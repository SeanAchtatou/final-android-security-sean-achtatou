package com.immomo.momo.android.activity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.b.a;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.view.EmoteEditeText;
import com.immomo.momo.android.view.HeaderLayout;
import com.immomo.momo.android.view.a.n;
import com.immomo.momo.android.view.bi;
import com.immomo.momo.b;
import com.immomo.momo.g;
import com.immomo.momo.service.aq;
import com.immomo.momo.service.bean.am;
import com.immomo.momo.service.bean.au;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.util.ao;
import com.immomo.momo.util.e;
import com.immomo.momo.util.h;
import com.immomo.momo.util.k;
import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import mm.purchasesdk.PurchaseCode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class EditUserProfileActivity extends js {
    private View A;
    private TextView B;
    private TextView C;
    private ImageView D;
    /* access modifiers changed from: private */
    public HashMap E = new HashMap();
    /* access modifiers changed from: private */
    public HashMap F = new HashMap();
    /* access modifiers changed from: private */
    public boolean G = false;
    /* access modifiers changed from: private */
    public Date H = null;
    /* access modifiers changed from: private */
    public Date I = null;
    private String J = PoiTypeDef.All;
    private File K = null;
    private View.OnClickListener L = new cr(this);
    private View.OnClickListener M = new cu(this);
    private View.OnLongClickListener N = new cw(this);
    private Runnable O = new cy(this);
    /* access modifiers changed from: private */
    public Handler P = new da(this);
    private bf l;
    private String m;
    private bi n;
    private HeaderLayout o = null;
    /* access modifiers changed from: private */
    public aq p = null;
    private View q;
    private View r;
    /* access modifiers changed from: private */
    public TextView s;
    /* access modifiers changed from: private */
    public TextView t;
    private EmoteEditeText u;
    private EmoteEditeText v;
    private EmoteEditeText w;
    private EmoteEditeText x;
    private EmoteEditeText y;
    private EmoteEditeText z;

    static /* synthetic */ void a(EditUserProfileActivity editUserProfileActivity, String str, Bitmap bitmap) {
        int i = 0;
        if (!a.a((CharSequence) str) && bitmap != null && editUserProfileActivity.i != null) {
            int length = editUserProfileActivity.i.length;
            for (int i2 = 0; i2 < editUserProfileActivity.h.size(); i2++) {
                au auVar = (au) editUserProfileActivity.h.get(i2);
                if (!auVar.d && auVar.b.equals(str)) {
                    auVar.f2997a = bitmap;
                }
            }
            while (i < length) {
                jt jtVar = (jt) editUserProfileActivity.i[i].findViewById(R.id.avatar_cover).getTag();
                if (jtVar == null || !((au) jtVar.f1774a).b.equals(str)) {
                    i++;
                } else {
                    ((ImageView) editUserProfileActivity.i[i].findViewById(R.id.avatar_imageview)).setImageBitmap(bitmap);
                    return;
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.b.a(java.lang.String, boolean):android.graphics.Bitmap
     arg types: [java.lang.String, int]
     candidates:
      com.immomo.momo.b.a(java.lang.String, android.graphics.Bitmap):android.graphics.Bitmap
      com.immomo.momo.b.a(java.lang.String, boolean):android.graphics.Bitmap */
    private void a(String str, String str2) {
        if (str != null) {
            this.A.setVisibility(0);
            this.B.setText(str);
        }
        if (!a.a((CharSequence) str2)) {
            am c = b.c(str2);
            if (c != null) {
                this.C.setText(c.b);
                if (a.a((CharSequence) c.d)) {
                    this.D.setImageBitmap(null);
                    this.D.setVisibility(8);
                    return;
                }
                this.D.setVisibility(0);
                this.D.setImageBitmap(b.a(c.f2991a, false));
                return;
            }
            this.C.setText(str);
            this.D.setVisibility(8);
            return;
        }
        this.C.setText("请选择你的行业");
        this.D.setVisibility(8);
    }

    private void a(List list, JSONArray jSONArray) {
        int i = 0;
        if (list != null && list.size() > 0) {
            while (true) {
                int i2 = i;
                if (i2 < list.size()) {
                    au auVar = (au) list.get(i2);
                    if (!auVar.d) {
                        JSONObject jSONObject = new JSONObject();
                        try {
                            if (auVar.c) {
                                jSONObject.put("upload", "YES");
                                jSONObject.put("guid", auVar.b);
                            } else {
                                jSONObject.put("upload", "NO");
                                jSONObject.put("key", "photo_" + i2);
                                File file = new File(new File(com.immomo.momo.a.g(), auVar.b.substring(0, 1)), String.valueOf(auVar.b) + ".jpg_");
                                this.e.a((Object) ("fiel == null? false, path: " + file.getPath() + ", size: " + file.length()));
                                this.F.put("photo_" + i2, file);
                            }
                            jSONArray.put(jSONObject);
                        } catch (JSONException e) {
                            this.e.a((Throwable) e);
                            a((CharSequence) "保存资料失败");
                            return;
                        }
                    }
                    i = i2 + 1;
                } else {
                    return;
                }
            }
        }
    }

    static /* synthetic */ void b(EditUserProfileActivity editUserProfileActivity) {
        String charSequence = editUserProfileActivity.t.getText().toString();
        if (a.a((CharSequence) charSequence)) {
            charSequence = "1980-1-1";
        }
        String[] split = charSequence.split("-");
        String[] split2 = split.length < 3 ? "1980-1-1".split("-") : split;
        new DatePickerDialog(editUserProfileActivity, new dc(editUserProfileActivity), Integer.parseInt(split2[0]), Integer.parseInt(split2[1]) - 1, Integer.parseInt(split2[2])).show();
    }

    static /* synthetic */ void c(EditUserProfileActivity editUserProfileActivity) {
        Intent intent = new Intent(editUserProfileActivity, EditIndustryActivity.class);
        intent.putExtra("job_name", editUserProfileActivity.B.getText().toString());
        intent.putExtra("industry_id", editUserProfileActivity.m);
        editUserProfileActivity.startActivityForResult(intent, 18);
    }

    static /* synthetic */ void d(EditUserProfileActivity editUserProfileActivity) {
        Intent intent = new Intent("android.intent.action.GET_CONTENT");
        intent.setType("image/*");
        editUserProfileActivity.startActivityForResult(Intent.createChooser(intent, "本地图片"), 11);
    }

    static /* synthetic */ void e(EditUserProfileActivity editUserProfileActivity) {
        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("immomo_");
        stringBuffer.append(a.c(new Date()));
        stringBuffer.append("_" + UUID.randomUUID());
        stringBuffer.append(".jpg");
        editUserProfileActivity.J = stringBuffer.toString();
        intent.putExtra("output", Uri.fromFile(new File(com.immomo.momo.a.i(), editUserProfileActivity.J)));
        editUserProfileActivity.startActivityForResult(intent, 12);
    }

    static /* synthetic */ boolean f(EditUserProfileActivity editUserProfileActivity) {
        for (int i = 0; i < editUserProfileActivity.h.size(); i++) {
            if (((au) editUserProfileActivity.h.get(i)).d) {
                return true;
            }
        }
        return false;
    }

    static /* synthetic */ void o(EditUserProfileActivity editUserProfileActivity) {
        new k("C", "C6401").e();
        JSONArray jSONArray = new JSONArray();
        editUserProfileActivity.a(editUserProfileActivity.h, jSONArray);
        editUserProfileActivity.E.put("photos", jSONArray.toString());
        editUserProfileActivity.E.put("momoid", editUserProfileActivity.l.h);
        editUserProfileActivity.e.a((Object) ("photos = " + jSONArray.toString()));
        editUserProfileActivity.a(new df(editUserProfileActivity, editUserProfileActivity)).execute(new Object[0]);
    }

    /* access modifiers changed from: private */
    public void v() {
        if (!this.v.getText().toString().equals(this.l.l())) {
            this.G = true;
            this.E.put("sign", this.v.getText().toString().trim());
        }
        if (!this.u.getText().toString().equals(this.l.L)) {
            this.G = true;
            this.E.put("interest", this.u.getText().toString().trim());
        }
        if (!this.w.getText().toString().equals(this.l.C)) {
            this.G = true;
            this.E.put("company", this.w.getText().toString().trim());
        }
        if (!this.x.getText().toString().equals(this.l.Q)) {
            this.G = true;
            this.E.put("school", this.x.getText().toString().trim());
        }
        if (!this.y.getText().toString().equals(this.l.F)) {
            this.G = true;
            this.E.put("hangout", this.y.getText().toString().trim());
        }
        if (!this.z.getText().toString().equals(this.l.D)) {
            this.G = true;
            this.E.put("website", this.z.getText().toString().trim());
        }
        if (!this.s.getText().toString().equals(this.l.i)) {
            this.G = true;
            this.E.put("name", this.s.getText().toString().trim());
        }
        if (!this.t.getText().toString().equals(this.l.J)) {
            this.G = true;
            this.E.put("birthday", this.t.getText().toString().trim());
        }
        if (!this.B.getText().toString().equals(this.l.N)) {
            this.G = true;
            this.E.put("job", this.B.getText().toString().trim());
        }
        if (this.m != this.l.M && !new StringBuilder(String.valueOf(this.m)).toString().equals(this.l.M)) {
            this.G = true;
            if (this.m == null) {
                this.E.put("industry", PoiTypeDef.All);
            } else {
                this.E.put("industry", this.m);
            }
        }
    }

    /* access modifiers changed from: private */
    public void w() {
        if (this.h != null && this.h.size() > 0) {
            int size = this.h.size();
            if (size <= 4) {
                this.j.setVisibility(0);
                this.k.setVisibility(8);
            } else if (size > 4 && size <= 8) {
                this.j.setVisibility(0);
                this.k.setVisibility(0);
            }
            for (int i = 0; i < size; i++) {
                au auVar = (au) this.h.get(i);
                this.i[i].setVisibility(0);
                if (auVar.d) {
                    ImageView imageView = (ImageView) this.i[i].findViewById(R.id.avatar_cover);
                    imageView.setVisibility(0);
                    imageView.setTag(new jt(auVar, i));
                    imageView.setOnClickListener(this.M);
                    ((LinearLayout) this.i[i].findViewById(R.id.avatar_add)).setVisibility(0);
                } else {
                    ImageView imageView2 = (ImageView) this.i[i].findViewById(R.id.avatar_cover);
                    imageView2.setVisibility(0);
                    imageView2.setTag(new jt(auVar, i));
                    imageView2.setOnClickListener(this.M);
                    imageView2.setOnLongClickListener(this.N);
                    ((ImageView) this.i[i].findViewById(R.id.avatar_imageview)).setImageBitmap(auVar.f2997a);
                    ((LinearLayout) this.i[i].findViewById(R.id.avatar_add)).setVisibility(4);
                }
            }
            if (size < 8) {
                for (int i2 = size; i2 < 8; i2++) {
                    this.i[i2].setVisibility(4);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        super.a(bundle);
        setContentView((int) R.layout.activity_edit_userprofile);
        u();
        d();
        this.o.a(this.n, new db(this));
        EmoteEditeText emoteEditeText = this.u;
        EmoteEditeText emoteEditeText2 = this.u;
        emoteEditeText.addTextChangedListener(new com.immomo.momo.util.aq(PurchaseCode.QUERY_NO_APP));
        EmoteEditeText emoteEditeText3 = this.w;
        EmoteEditeText emoteEditeText4 = this.w;
        emoteEditeText3.addTextChangedListener(new com.immomo.momo.util.aq(PurchaseCode.QUERY_NO_APP));
        EmoteEditeText emoteEditeText5 = this.x;
        EmoteEditeText emoteEditeText6 = this.x;
        emoteEditeText5.addTextChangedListener(new com.immomo.momo.util.aq(PurchaseCode.QUERY_NO_APP));
        EmoteEditeText emoteEditeText7 = this.y;
        EmoteEditeText emoteEditeText8 = this.y;
        emoteEditeText7.addTextChangedListener(new com.immomo.momo.util.aq(PurchaseCode.QUERY_NO_APP));
        EmoteEditeText emoteEditeText9 = this.z;
        EmoteEditeText emoteEditeText10 = this.z;
        emoteEditeText9.addTextChangedListener(new com.immomo.momo.util.aq(PurchaseCode.QUERY_NO_APP));
        EmoteEditeText emoteEditeText11 = this.v;
        EmoteEditeText emoteEditeText12 = this.v;
        emoteEditeText11.addTextChangedListener(new com.immomo.momo.util.aq(PurchaseCode.QUERY_NO_APP));
        this.q.setOnClickListener(this.L);
        this.r.setOnClickListener(this.L);
        this.A.setOnClickListener(this.L);
    }

    /* access modifiers changed from: protected */
    public final void d() {
        Calendar instance = Calendar.getInstance();
        Calendar instance2 = Calendar.getInstance();
        instance2.set(1, instance.get(1) - 12);
        this.I = instance2.getTime();
        instance2.set(1, instance.get(1) - 100);
        this.H = instance2.getTime();
        this.p = new aq();
        this.l = this.f;
        if (this.l == null) {
            a((CharSequence) "当前用户资料不存在");
            finish();
        }
        this.m = this.l.M;
        if (this.l != null) {
            int length = this.l.ae != null ? this.l.ae.length > 8 ? 8 : this.l.ae.length : 0;
            this.h.clear();
            for (int i = 0; i < length; i++) {
                au auVar = new au();
                auVar.b = this.l.ae[i];
                auVar.d = false;
                auVar.c = true;
                this.h.add(auVar);
            }
            if (this.h.size() < 8) {
                au auVar2 = new au();
                auVar2.d = true;
                auVar2.c = false;
                this.h.add(auVar2);
            }
        }
        w();
        this.s.setText(this.l.i);
        this.t.setText(this.l.J);
        try {
            if (this.l.L.getBytes("GBK").length > 512) {
                this.l.L = g.a(this.l.L, (int) PurchaseCode.QUERY_NO_APP);
            }
            this.u.setText(this.l.L);
        } catch (Exception e) {
            this.e.a((Object) e);
        }
        try {
            if (this.l.l().getBytes("GBK").length > 512) {
                this.l.b(g.a(this.l.l(), (int) PurchaseCode.QUERY_NO_APP));
            }
            this.v.setText(this.l.l());
        } catch (Exception e2) {
            this.e.a((Object) e2);
        }
        try {
            if (this.l.C.getBytes("GBK").length > 512) {
                this.l.C = g.a(this.l.C, (int) PurchaseCode.QUERY_NO_APP);
            }
            this.w.setText(this.l.C);
        } catch (Exception e3) {
            this.e.a((Object) e3);
        }
        try {
            if (this.l.Q.getBytes("GBK").length > 512) {
                this.l.Q = g.a(this.l.Q, (int) PurchaseCode.QUERY_NO_APP);
            }
            this.x.setText(this.l.Q);
        } catch (Exception e4) {
            this.e.a((Object) e4);
        }
        try {
            if (this.l.F.getBytes("GBK").length > 512) {
                this.l.F = g.a(this.l.F, (int) PurchaseCode.QUERY_NO_APP);
            }
            this.y.setText(this.l.F);
        } catch (Exception e5) {
            this.e.a((Object) e5);
        }
        try {
            if (this.l.D.getBytes("GBK").length > 512) {
                this.l.D = g.a(this.l.D, (int) PurchaseCode.QUERY_NO_APP);
            }
            this.z.setText(this.l.D);
        } catch (Exception e6) {
            this.e.a((Object) e6);
        }
        if (!a.a((CharSequence) this.l.N)) {
            this.A.setVisibility(0);
            e.a(this.B, this.l.N, this);
        }
        a(this.l.N, this.l.M);
        if (this.l.ae != null) {
            ArrayList arrayList = new ArrayList();
            for (String str : this.l.ae) {
                au auVar3 = new au();
                auVar3.b = str;
                auVar3.c = true;
                arrayList.add(auVar3);
            }
            if (arrayList.size() < 8) {
                au auVar4 = new au();
                auVar4.c = false;
                auVar4.d = true;
                arrayList.add(auVar4);
            }
            new Thread(this.O).start();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.h.a(java.lang.String, android.graphics.Bitmap, int, boolean):java.io.File
     arg types: [java.lang.String, android.graphics.Bitmap, int, int]
     candidates:
      com.immomo.momo.util.h.a(java.lang.String, java.lang.String, int, boolean):void
      com.immomo.momo.util.h.a(java.lang.String, android.graphics.Bitmap, int, boolean):java.io.File */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.b.a.a(android.graphics.Bitmap, float, boolean):android.graphics.Bitmap
     arg types: [android.graphics.Bitmap, int, int]
     candidates:
      android.support.v4.b.a.a(android.graphics.Bitmap, int, int):android.graphics.Bitmap
      android.support.v4.b.a.a(java.io.File, int, int):android.graphics.Bitmap
      android.support.v4.b.a.a(java.lang.String, java.lang.String, java.lang.String):java.lang.String
      android.support.v4.b.a.a(java.lang.Object[], java.lang.String, java.lang.String):java.lang.String
      android.support.v4.b.a.a(android.graphics.Bitmap, float, boolean):android.graphics.Bitmap */
    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        Uri fromFile;
        switch (i) {
            case 11:
                if (i2 == -1 && intent != null) {
                    Uri data = intent.getData();
                    Intent intent2 = new Intent(this, ImageFactoryActivity.class);
                    intent2.setData(data);
                    intent2.putExtra("aspectY", 1);
                    intent2.putExtra("aspectX", 1);
                    intent2.putExtra("minsize", 320);
                    this.K = new File(com.immomo.momo.a.g(), String.valueOf(com.immomo.a.a.f.b.a()) + ".jpg_");
                    intent2.putExtra("outputFilePath", this.K.getAbsolutePath());
                    startActivityForResult(intent2, 13);
                    return;
                }
                return;
            case 12:
                if (i2 == -1 && !a.a((CharSequence) this.J) && (fromFile = Uri.fromFile(new File(com.immomo.momo.a.i(), this.J))) != null) {
                    Intent intent3 = new Intent(this, ImageFactoryActivity.class);
                    intent3.setData(fromFile);
                    intent3.putExtra("aspectY", 1);
                    intent3.putExtra("aspectX", 1);
                    intent3.putExtra("minsize", 320);
                    this.K = new File(com.immomo.momo.a.g(), String.valueOf(com.immomo.a.a.f.b.a()) + ".jpg_");
                    intent3.putExtra("outputFilePath", this.K.getAbsolutePath());
                    startActivityForResult(intent3, 13);
                    return;
                }
                return;
            case 13:
                if (i2 == -1 && intent != null) {
                    if (!a.a((CharSequence) this.J)) {
                        File file = new File(com.immomo.momo.a.i(), this.J);
                        if (file.exists()) {
                            file.delete();
                        }
                    }
                    if (this.K != null) {
                        String absolutePath = this.K.getAbsolutePath();
                        String substring = this.K.getName().substring(0, this.K.getName().lastIndexOf("."));
                        this.e.a((Object) ("filename=" + substring));
                        Bitmap l2 = a.l(absolutePath);
                        if (l2 != null) {
                            this.e.a((Object) ("save large to " + h.a(substring, l2, 2, false).getPath()));
                            Bitmap a2 = a.a(l2, 150.0f, true);
                            this.e.a((Object) ("save small to " + h.a(substring, a2, 3, false).getPath()));
                            au auVar = new au();
                            auVar.b = substring;
                            auVar.f2997a = a2;
                            auVar.d = false;
                            auVar.c = false;
                            if (this.h.size() < 8) {
                                this.h.add(this.h.size() - 1, auVar);
                            } else if (this.h.size() >= 8) {
                                this.h.remove(this.h.size() - 1);
                                this.h.add(auVar);
                            }
                            w();
                            this.G = true;
                            if (this.K != null && this.K.exists()) {
                                this.K.delete();
                                return;
                            }
                            return;
                        }
                        return;
                    }
                    return;
                } else if (i2 == 1003) {
                    ao.d(R.string.cropimage_error_size);
                    return;
                } else if (i2 == 1000) {
                    ao.d(R.string.cropimage_error_other);
                    return;
                } else if (i2 == 1002) {
                    ao.d(R.string.cropimage_error_store);
                    return;
                } else if (i2 == 1001) {
                    ao.d(R.string.cropimage_error_filenotfound);
                    return;
                } else {
                    return;
                }
            case com.immomo.momo.h.DragSortListView_drag_handle_id /*14*/:
            case 15:
            case 16:
            case 17:
            default:
                return;
            case 18:
                if (i2 == -1 && intent != null) {
                    String stringExtra = intent.getStringExtra("job_name");
                    String stringExtra2 = intent.getStringExtra("industry_id");
                    this.m = stringExtra2;
                    a(stringExtra, stringExtra2);
                    return;
                }
                return;
        }
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i == 4) {
            v();
            if (this.G) {
                n nVar = new n(this);
                nVar.a();
                nVar.setTitle((int) R.string.dialog_exit_editprofile_title);
                nVar.a((int) R.string.dialog_exit_editprofile_msg);
                nVar.a(0, "保存", new dd(this));
                nVar.a(1, "不保存", new de(this));
                nVar.a(2, "取消", new ct());
                nVar.show();
                return true;
            }
        }
        return super.onKeyDown(i, keyEvent);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        new k("PO", "P911").e();
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Bundle bundle) {
        super.onRestoreInstanceState(bundle);
        this.e.a((Object) "---------------onRestoreInstanceState");
        if (bundle.containsKey("camera_filename")) {
            this.J = bundle.getString("camera_filename");
        }
        if (bundle.containsKey("avatorFile")) {
            this.K = new File(bundle.getString("avatorFile"));
        }
        if (bundle.containsKey("sign")) {
            this.v.setText(bundle.getString("sign"));
        }
        if (bundle.containsKey("interest")) {
            this.u.setText(bundle.getString("interest"));
        }
        if (bundle.containsKey("company")) {
            this.w.setText(bundle.getString("company"));
        }
        if (bundle.containsKey("school")) {
            this.x.setText(bundle.getString("school"));
        }
        if (bundle.containsKey("hangout")) {
            this.y.setText(bundle.getString("hangout"));
        }
        if (bundle.containsKey("website")) {
            this.z.setText(bundle.getString("website"));
        }
        if (bundle.containsKey("name")) {
            this.s.setText(bundle.getString("name"));
        }
        if (bundle.containsKey("birthday")) {
            this.t.setText(bundle.getString("birthday"));
        }
        if (bundle.containsKey("industry") && bundle.containsKey("job")) {
            a(bundle.getString("job"), bundle.getString("industry"));
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        new k("PI", "P911").e();
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        if (!a.a((CharSequence) this.J)) {
            bundle.putString("camera_filename", this.J);
        }
        if (this.K != null) {
            bundle.putString("avatorFile", this.K.getPath());
        }
        if (this.v.getText().toString().length() > 0) {
            bundle.putString("sign", this.v.getText().toString());
        }
        if (this.u.getText().toString().length() > 0) {
            bundle.putString("interest", this.u.getText().toString().trim());
        }
        if (this.w.getText().toString().length() > 0) {
            bundle.putString("company", this.w.getText().toString().trim());
        }
        if (this.x.getText().toString().length() > 0) {
            bundle.putString("school", this.x.getText().toString().trim());
        }
        if (this.y.getText().toString().length() > 0) {
            bundle.putString("hangout", this.y.getText().toString().trim());
        }
        if (this.z.getText().toString().length() > 0) {
            bundle.putString("website", this.z.getText().toString().trim());
        }
        if (this.s.getText().toString().length() > 0) {
            bundle.putString("name", this.s.getText().toString().trim());
        }
        if (this.t.getText().toString().length() > 0) {
            bundle.putString("birthday", this.t.getText().toString().trim());
        }
        if (this.m != null) {
            bundle.putString("industry", this.m);
            bundle.putString("job", this.B.getText().toString());
        }
    }

    /* access modifiers changed from: protected */
    public final void u() {
        super.u();
        this.o = (HeaderLayout) findViewById(R.id.layout_header);
        this.n = new bi(this);
        this.n.a((int) R.drawable.ic_topbar_confirm_white);
        this.n.setBackgroundResource(R.drawable.bg_header_submit);
        this.n.setMarginRight(8);
        this.o.setTitleText("编辑资料");
        this.q = findViewById(R.id.layout_name);
        this.r = findViewById(R.id.layout_birthday);
        this.s = (TextView) findViewById(R.id.profile_tv_name);
        this.t = (TextView) findViewById(R.id.profile_tv_birthday);
        this.u = (EmoteEditeText) findViewById(R.id.profile_tv_interest);
        this.v = (EmoteEditeText) findViewById(R.id.profile_tv_sign);
        this.w = (EmoteEditeText) findViewById(R.id.profile_tv_company);
        this.x = (EmoteEditeText) findViewById(R.id.profile_tv_school);
        this.y = (EmoteEditeText) findViewById(R.id.profile_tv_hangout);
        this.z = (EmoteEditeText) findViewById(R.id.profile_tv_web);
        this.B = (TextView) findViewById(R.id.tv_industry_job);
        this.C = (TextView) findViewById(R.id.tv_industry);
        this.D = (ImageView) findViewById(R.id.icon_industry);
        this.A = findViewById(R.id.layout_industry);
    }
}
