package com.immomo.momo.android.a;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.immomo.momo.android.view.AltImageView;
import com.immomo.momo.android.view.EmoteTextView;
import com.immomo.momo.android.view.HandyTextView;

final class ih {

    /* renamed from: a  reason: collision with root package name */
    public ImageView f877a;
    public EmoteTextView b;
    public TextView c;
    public TextView d;
    public View e;
    public ImageView f;
    public HandyTextView g;
    public ImageView h;
    public View i;
    public EmoteTextView j;
    public EmoteTextView k;
    public AltImageView l;
    public View m;
    public EmoteTextView n;
    public EmoteTextView o;
    public View p;
    public TextView q;
    public View r;

    private ih() {
    }

    /* synthetic */ ih(byte b2) {
        this();
    }
}
