package com.immomo.momo.android.activity;

import android.view.View;

final class kd implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ SetHiddenActivity f1783a;

    kd(SetHiddenActivity setHiddenActivity) {
        this.f1783a = setHiddenActivity;
    }

    public final void onClick(View view) {
        if (SetHiddenActivity.c(this.f1783a)) {
            this.f1783a.a(new kf(this.f1783a, this.f1783a)).execute(new Object[0]);
        }
    }
}
