package com.immomo.momo.android.activity;

import android.content.Intent;
import android.os.AsyncTask;
import com.immomo.momo.R;
import com.immomo.momo.a.a;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.w;
import java.io.InterruptedIOException;

final class go extends AsyncTask {

    /* renamed from: a  reason: collision with root package name */
    private int f1522a;
    private /* synthetic */ OnlineSettingActivity b;

    public go(OnlineSettingActivity onlineSettingActivity, int i) {
        this.b = onlineSettingActivity;
        this.f1522a = i;
    }

    private Integer a() {
        try {
            return w.a().a(this.f1522a);
        } catch (InterruptedIOException e) {
            this.b.e.a((Throwable) e);
            this.b.b((int) R.string.errormsg_network_timeout);
        } catch (com.immomo.momo.a.w e2) {
            this.b.e.a((Throwable) e2);
            this.b.b((CharSequence) e2.getMessage());
        } catch (a e3) {
            this.b.e.a((Throwable) e3);
            this.b.b((CharSequence) e3.getMessage());
        } catch (Exception e4) {
            this.b.e.a((Throwable) e4);
            this.b.b((int) R.string.errormsg_server);
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object doInBackground(Object... objArr) {
        return a();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.android.activity.OnlineSettingActivity.a(com.immomo.momo.android.activity.OnlineSettingActivity, boolean):void
     arg types: [com.immomo.momo.android.activity.OnlineSettingActivity, int]
     candidates:
      com.immomo.momo.android.activity.OnlineSettingActivity.a(com.immomo.momo.android.activity.OnlineSettingActivity, com.immomo.momo.android.view.a.v):void
      com.immomo.momo.android.activity.ah.a(android.os.Bundle, java.lang.String):boolean
      com.immomo.momo.android.activity.ao.a(int, java.lang.String[]):com.immomo.momo.protocol.imjson.c.a
      com.immomo.momo.android.activity.ao.a(com.immomo.momo.android.view.bi, android.view.View$OnClickListener):void
      com.immomo.momo.android.activity.ao.a(java.lang.CharSequence, int):void
      com.immomo.momo.android.activity.ao.a(android.os.Bundle, java.lang.String):boolean
      com.immomo.momo.android.activity.OnlineSettingActivity.a(com.immomo.momo.android.activity.OnlineSettingActivity, boolean):void */
    /* access modifiers changed from: protected */
    public final /* synthetic */ void onPostExecute(Object obj) {
        Integer num = (Integer) obj;
        if (num != null) {
            if (num.intValue() == 0) {
                this.b.g.l = 0;
            } else if (num.intValue() == 1) {
                this.b.g.l = 1;
            } else if (num.intValue() == 2) {
                this.b.g.l = 2;
            }
            this.b.g.a("hiddenmode", Integer.valueOf(this.b.g.l));
            this.b.sendBroadcast(new Intent(com.immomo.momo.android.broadcast.w.c));
            OnlineSettingActivity.b(this.b);
        } else {
            this.b.l = true;
            OnlineSettingActivity.b(this.b);
            this.b.l = false;
        }
        if (this.b.m != null) {
            this.b.m.dismiss();
            this.b.m = (v) null;
        }
    }

    /* access modifiers changed from: protected */
    public final void onPreExecute() {
        this.b.m = new v(this.b, "请稍候，正在提交...");
        this.b.m.setOnCancelListener(new gp(this));
        this.b.m.show();
    }
}
