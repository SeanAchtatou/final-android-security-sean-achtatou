package com.immomo.momo.android.activity;

import android.graphics.Bitmap;
import com.immomo.momo.R;
import com.immomo.momo.android.c.g;
import com.immomo.momo.util.j;

final class iv implements g {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ OtherProfileV2Activity f1750a;

    iv(OtherProfileV2Activity otherProfileV2Activity) {
        this.f1750a = otherProfileV2Activity;
    }

    public final /* synthetic */ void a(Object obj) {
        Bitmap bitmap = (Bitmap) obj;
        this.f1750a.t.setImageLoading(false);
        if (bitmap != null && this.f1750a.t.af.equals(this.f1750a.aO.getTag(R.id.tag_item_imageid))) {
            j.a(this.f1750a.t.af, bitmap);
            this.f1750a.runOnUiThread(new iw(this, bitmap));
        }
    }
}
