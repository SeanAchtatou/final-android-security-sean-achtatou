package com.immomo.momo.android.activity.setting;

import android.view.View;

final class a implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ CommunityBindActivity f2125a;

    a(CommunityBindActivity communityBindActivity) {
        this.f2125a = communityBindActivity;
    }

    public final void onClick(View view) {
        switch (this.f2125a.o) {
            case 0:
                this.f2125a.finish();
                return;
            case 1:
                this.f2125a.b(new i(this.f2125a, this.f2125a));
                return;
            case 2:
                CommunityBindActivity.q(this.f2125a);
                return;
            case 3:
                CommunityBindActivity.p(this.f2125a);
                return;
            default:
                return;
        }
    }
}
