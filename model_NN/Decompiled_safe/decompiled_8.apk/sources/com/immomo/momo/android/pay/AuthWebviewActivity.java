package com.immomo.momo.android.pay;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.b.a;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.WebView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.a.a.f.b;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.ah;
import com.immomo.momo.android.view.bi;
import com.immomo.momo.g;
import com.immomo.momo.util.jni.Codec;
import org.apache.http.util.EncodingUtils;

public class AuthWebviewActivity extends ah {
    public static final String h = (String.valueOf(g.h()) + "_url");
    public static final String i = (String.valueOf(g.h()) + "_title");
    public static final String j = (String.valueOf(g.h()) + "_ispostdata");
    private String k = PoiTypeDef.All;
    private String l = PoiTypeDef.All;
    private boolean m = true;
    private WebView n = null;
    /* access modifiers changed from: private */
    public View o = null;

    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        super.a(bundle);
        setContentView((int) R.layout.activity_mypayrecord);
        d();
        if (!a.a((CharSequence) this.l)) {
            m().setTitleText(this.l);
        }
        this.n = (WebView) findViewById(R.id.webview);
        this.n.getSettings().setSupportZoom(true);
        this.n.getSettings().setBuiltInZoomControls(true);
        this.n.getSettings().setUseWideViewPort(true);
        if (g.a()) {
            this.n.getSettings().setDisplayZoomControls(false);
        }
        this.n.setOnTouchListener(new b());
        this.o = findViewById(R.id.loading_indicator);
        this.n.setWebViewClient(new a(this));
        this.n.getSettings().setCacheMode(2);
        this.n.getSettings().setJavaScriptEnabled(true);
        CookieSyncManager.createInstance(this);
        CookieSyncManager.getInstance().startSync();
        CookieManager.getInstance().removeSessionCookie();
        this.e.b((Object) ("url= " + this.k));
        if (this.m) {
            String a2 = b.a();
            this.n.postUrl(this.k, EncodingUtils.getBytes("random=" + a2 + "&token=" + a.n(String.valueOf("android") + this.f.h + a2 + g.s() + g.z() + Codec.gvk()) + "&version=" + g.z() + "&client=" + "android", "UTF-8"));
            return;
        }
        this.n.loadUrl(this.k);
    }

    /* access modifiers changed from: protected */
    public final void d() {
        Intent intent = getIntent();
        this.k = intent.getStringExtra(h);
        this.l = intent.getStringExtra(i);
        if (intent.hasExtra(j)) {
            this.m = intent.getBooleanExtra(j, this.m);
        }
        bi biVar = new bi(getApplicationContext());
        biVar.a("关闭");
        m().a(biVar, new c(this));
    }

    public void onBackPressed() {
        if (this.n.canGoBack()) {
            this.n.goBack();
        } else {
            super.onBackPressed();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }
}
