package com.immomo.momo.android.a;

import android.view.View;
import android.widget.AdapterView;
import com.immomo.momo.R;
import com.immomo.momo.android.view.a.n;
import com.immomo.momo.android.view.a.o;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.a.e;
import com.immomo.momo.util.ao;

final class el implements AdapterView.OnItemClickListener {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public /* synthetic */ ek f799a;
    private final /* synthetic */ String[] b;
    private final /* synthetic */ e c;

    el(ek ekVar, String[] strArr, e eVar) {
        this.f799a = ekVar;
        this.b = strArr;
        this.c = eVar;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        if ("复制文本".equals(this.b[i])) {
            g.a((CharSequence) this.c.a());
            ao.b("已成功复制文本");
        } else if ("删除".equals(this.b[i])) {
            n.a(this.f799a.d(), "确定要删除该留言？", new em(this, this.c)).show();
        } else if ("举报".equals(this.b[i])) {
            o oVar = new o(this.f799a.e, (int) R.array.reportfeed_items);
            oVar.setTitle((int) R.string.report_dialog_title_feed);
            oVar.a();
            oVar.a(new en(this, this.c));
            oVar.show();
        }
    }
}
