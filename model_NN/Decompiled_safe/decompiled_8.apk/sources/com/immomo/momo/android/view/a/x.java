package com.immomo.momo.android.view.a;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.b.a;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.b;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.am;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.util.k;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

public final class x extends aw implements View.OnClickListener, AdapterView.OnItemClickListener, CompoundButton.OnCheckedChangeListener {
    private static /* synthetic */ int[] I;
    private static /* synthetic */ int[] J;
    private static /* synthetic */ int[] K;
    /* access modifiers changed from: private */
    public static String[] v = {"不限", "白羊座", "金牛座", "双子座", "巨蟹座", "狮子座", "处女座", "天秤座", "天蝎座", "射手座", "摩羯座", "水瓶座", "双鱼座"};
    /* access modifiers changed from: private */
    public static String[] w = {"不限", "18-22岁", "23-26岁", "27-35岁", "35岁以上"};
    /* access modifiers changed from: private */
    public String A = PoiTypeDef.All;
    private bf B = null;
    private ListView C = null;
    private View D = null;
    private View E = null;
    /* access modifiers changed from: private */
    public boolean F = true;
    private ag G = null;
    private al H = null;

    /* renamed from: a  reason: collision with root package name */
    int f2708a = -1;
    BaseAdapter b;
    private RadioButton e = null;
    private RadioButton f = null;
    private RadioButton g = null;
    private RadioButton h = null;
    private RadioButton i = null;
    private RadioButton j = null;
    private RadioButton k = null;
    private TextView l;
    private ImageView m;
    /* access modifiers changed from: private */
    public TextView n;
    /* access modifiers changed from: private */
    public TextView o;
    private TextView p;
    private ImageView q;
    private ah r;
    private ak s;
    private ah t;
    private ak u;
    /* access modifiers changed from: private */
    public int x = 0;
    /* access modifiers changed from: private */
    public int y = 0;
    /* access modifiers changed from: private */
    public aj z = aj.ALL;

    public x(Context context, ah ahVar, ak akVar, int i2, int i3, aj ajVar, String str) {
        super(context, R.layout.include_diloag_nearby_filter);
        b((int) R.style.Popup_Animation_PushDownUp);
        this.B = g.q();
        this.r = ahVar;
        this.s = akVar;
        this.x = i2;
        this.y = i3;
        this.z = ajVar;
        this.A = str;
        this.t = ahVar;
        this.u = akVar;
        this.e = (RadioButton) c((int) R.id.filter_radiobutton_genderAll);
        this.f = (RadioButton) c((int) R.id.filter_radiobutton_genderMale);
        this.g = (RadioButton) c((int) R.id.filter_radiobutton_genderFemale);
        this.h = (RadioButton) c((int) R.id.filter_radiobutton_time15);
        this.i = (RadioButton) c((int) R.id.filter_radiobutton_time60);
        this.j = (RadioButton) c((int) R.id.filter_radiobutton_time1440);
        this.k = (RadioButton) c((int) R.id.filter_radiobutton_time4320);
        this.l = (TextView) c((int) R.id.filter_tv_industry);
        this.m = (ImageView) c((int) R.id.filter_iv_industry);
        this.o = (TextView) c((int) R.id.filter_tv_age);
        this.p = (TextView) c((int) R.id.filter_tv_bind);
        this.q = (ImageView) c((int) R.id.filter_iv_bind);
        this.n = (TextView) c((int) R.id.filter_tv_constellation);
        this.C = (ListView) c((int) R.id.listview);
        this.C.setOnItemClickListener(this);
        this.D = c((int) R.id.neabyfilter_layout_options);
        this.E = c((int) R.id.neabyfileter_layout_listview);
        this.h.setOnCheckedChangeListener(this);
        this.i.setOnCheckedChangeListener(this);
        this.j.setOnCheckedChangeListener(this);
        this.k.setOnCheckedChangeListener(this);
        this.e.setOnCheckedChangeListener(this);
        this.f.setOnCheckedChangeListener(this);
        this.g.setOnCheckedChangeListener(this);
        c((int) R.id.btn_ok).setOnClickListener(this);
        c((int) R.id.btn_cancle).setOnClickListener(this);
        c((int) R.id.filter_layout_constellation).setOnClickListener(this);
        c((int) R.id.filter_layout_age).setOnClickListener(this);
        c((int) R.id.filter_layout_bind).setOnClickListener(this);
        c((int) R.id.filter_layout_industry).setOnClickListener(this);
        switch (h()[this.r.ordinal()]) {
            case 1:
                this.e.setChecked(true);
                break;
            case 2:
                this.f.setChecked(true);
                break;
            case 3:
                this.g.setChecked(true);
                break;
        }
        switch (i()[this.s.ordinal()]) {
            case 1:
                this.h.setChecked(true);
                break;
            case 2:
                this.i.setChecked(true);
                break;
            case 3:
                this.j.setChecked(true);
                break;
            case 4:
                this.k.setChecked(true);
                break;
        }
        this.o.setText(w[this.x]);
        this.n.setText(v[this.y]);
        a(this.A);
        a(this.z);
    }

    /* access modifiers changed from: private */
    public void a(aj ajVar) {
        switch (j()[ajVar.ordinal()]) {
            case 1:
                this.p.setText("不限");
                this.p.setVisibility(0);
                this.q.setVisibility(8);
                return;
            case 2:
                this.q.setImageResource(R.drawable.ic_setting_weibo);
                this.q.setVisibility(0);
                this.p.setVisibility(8);
                return;
            case 3:
                this.q.setImageResource(R.drawable.ic_setting_tweibo);
                this.q.setVisibility(0);
                this.p.setVisibility(8);
                return;
            case 4:
                this.q.setImageResource(R.drawable.ic_setting_renren);
                this.q.setVisibility(0);
                this.p.setVisibility(8);
                return;
            default:
                return;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.b.a(java.lang.String, boolean):android.graphics.Bitmap
     arg types: [java.lang.String, int]
     candidates:
      com.immomo.momo.b.a(java.lang.String, android.graphics.Bitmap):android.graphics.Bitmap
      com.immomo.momo.b.a(java.lang.String, boolean):android.graphics.Bitmap */
    /* access modifiers changed from: private */
    public void a(String str) {
        if (a.a((CharSequence) str)) {
            this.l.setText("不限");
            this.l.setVisibility(0);
            this.m.setVisibility(8);
            return;
        }
        this.l.setVisibility(8);
        this.m.setVisibility(0);
        this.m.setImageBitmap(b.a(str, false));
    }

    private void a(String[] strArr, int i2) {
        this.f2708a = i2;
        ListView listView = this.C;
        ai aiVar = new ai(this, this.d, Arrays.asList(strArr));
        this.b = aiVar;
        listView.setAdapter((ListAdapter) aiVar);
    }

    private void f() {
        Animation loadAnimation = AnimationUtils.loadAnimation(this.d, R.anim.push_right_in);
        loadAnimation.setDuration(600);
        Animation loadAnimation2 = AnimationUtils.loadAnimation(this.d, R.anim.push_right_out);
        loadAnimation2.setDuration(600);
        this.H = null;
        this.f2708a = -1;
        this.E.setAnimation(loadAnimation2);
        this.D.setAnimation(loadAnimation);
        this.D.setVisibility(0);
        this.E.setVisibility(8);
    }

    private void g() {
        Animation loadAnimation = AnimationUtils.loadAnimation(this.d, R.anim.push_left_out);
        loadAnimation.setDuration(600);
        Animation loadAnimation2 = AnimationUtils.loadAnimation(this.d, R.anim.push_left_in);
        loadAnimation2.setDuration(600);
        this.E.setAnimation(loadAnimation2);
        this.D.setAnimation(loadAnimation);
        this.E.setVisibility(0);
        this.D.setVisibility(8);
    }

    private static /* synthetic */ int[] h() {
        int[] iArr = I;
        if (iArr == null) {
            iArr = new int[ah.values().length];
            try {
                iArr[ah.ALL.ordinal()] = 1;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[ah.FEMALE.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[ah.MALE.ordinal()] = 2;
            } catch (NoSuchFieldError e4) {
            }
            I = iArr;
        }
        return iArr;
    }

    private static /* synthetic */ int[] i() {
        int[] iArr = J;
        if (iArr == null) {
            iArr = new int[ak.values().length];
            try {
                iArr[ak.MINUTE_1440.ordinal()] = 3;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[ak.MINUTE_15.ordinal()] = 1;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[ak.MINUTE_4320.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[ak.MINUTE_60.ordinal()] = 2;
            } catch (NoSuchFieldError e5) {
            }
            J = iArr;
        }
        return iArr;
    }

    private static /* synthetic */ int[] j() {
        int[] iArr = K;
        if (iArr == null) {
            iArr = new int[aj.values().length];
            try {
                iArr[aj.ALL.ordinal()] = 1;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[aj.RENREN.ordinal()] = 4;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[aj.SINA.ordinal()] = 2;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[aj.TENGXUN.ordinal()] = 3;
            } catch (NoSuchFieldError e5) {
            }
            K = iArr;
        }
        return iArr;
    }

    public final void a(View view) {
        a(view, 17);
    }

    public final void a(ag agVar) {
        this.G = agVar;
    }

    /* access modifiers changed from: protected */
    public final boolean a() {
        if (!this.E.isShown()) {
            return super.a();
        }
        f();
        return true;
    }

    public final void onCheckedChanged(CompoundButton compoundButton, boolean z2) {
        if (z2) {
            switch (compoundButton.getId()) {
                case R.id.filter_radiobutton_genderAll /*2131166163*/:
                    this.t = ah.ALL;
                    return;
                case R.id.filter_radiobutton_genderMale /*2131166164*/:
                    this.t = ah.MALE;
                    return;
                case R.id.filter_radiobutton_genderFemale /*2131166165*/:
                    this.t = ah.FEMALE;
                    return;
                case R.id.filter_rg_time /*2131166166*/:
                default:
                    return;
                case R.id.filter_radiobutton_time15 /*2131166167*/:
                    this.u = ak.MINUTE_15;
                    return;
                case R.id.filter_radiobutton_time60 /*2131166168*/:
                    this.u = ak.MINUTE_60;
                    return;
                case R.id.filter_radiobutton_time1440 /*2131166169*/:
                    this.u = ak.MINUTE_1440;
                    return;
                case R.id.filter_radiobutton_time4320 /*2131166170*/:
                    this.u = ak.MINUTE_4320;
                    return;
            }
        }
    }

    public final void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_ok /*2131165289*/:
                if (this.G != null) {
                    this.G.a(this.t, this.u, this.z, this.x, this.A, this.y);
                }
                try {
                    JSONObject jSONObject = new JSONObject();
                    jSONObject.put("type", 1);
                    jSONObject.put("sex", this.t.a());
                    jSONObject.put("time", this.u.a());
                    new k("C", "C2021").e();
                } catch (JSONException e2) {
                }
                e();
                return;
            case R.id.btn_cancle /*2131165290*/:
                e();
                new k("C", "C2022").e();
                return;
            case R.id.filter_layout_industry /*2131166171*/:
                if (this.B.b()) {
                    ArrayList arrayList = new ArrayList();
                    List b2 = b.b();
                    int i2 = 0;
                    for (int i3 = 0; i3 < b2.size(); i3++) {
                        am amVar = (am) b2.get(i3);
                        arrayList.add(amVar);
                        if (amVar.f2991a != null && amVar.f2991a.equals(this.A)) {
                            i2 = i3;
                        }
                    }
                    arrayList.add(0, new am(PoiTypeDef.All, "不限", PoiTypeDef.All, PoiTypeDef.All));
                    this.f2708a = i2;
                    ListView listView = this.C;
                    ai aiVar = new ai(this, this.d, arrayList);
                    this.b = aiVar;
                    listView.setAdapter((ListAdapter) aiVar);
                    this.H = new ae(this);
                    g();
                    return;
                }
                n.a(this.d, "高级筛选仅限会员使用，是否开通会员？", "开通会员", "取消", new af(this), (DialogInterface.OnClickListener) null).show();
                return;
            case R.id.filter_layout_bind /*2131166174*/:
                if (this.B.b()) {
                    aj[] values = aj.values();
                    String[] strArr = new String[values.length];
                    for (int i4 = 0; i4 < values.length; i4++) {
                        strArr[i4] = aj.a(values[i4]);
                    }
                    a(strArr, this.z.ordinal());
                    this.H = new aa(this);
                    g();
                    return;
                }
                n.a(this.d, "高级筛选仅限会员使用，是否开通会员？", "开通会员", "取消", new ab(this), (DialogInterface.OnClickListener) null).show();
                return;
            case R.id.filter_layout_age /*2131166177*/:
                if (this.B.b()) {
                    a(w, this.x);
                    this.H = new y(this);
                    g();
                    return;
                }
                n.a(this.d, "高级筛选仅限会员使用，是否开通会员？", "开通会员", "取消", new z(this), (DialogInterface.OnClickListener) null).show();
                return;
            case R.id.filter_layout_constellation /*2131166179*/:
                if (this.B.b()) {
                    a(v, this.y);
                    this.H = new ac(this);
                    g();
                    return;
                }
                n.a(this.d, "高级筛选仅限会员使用，是否开通会员？", "开通会员", "取消", new ad(this), (DialogInterface.OnClickListener) null).show();
                return;
            default:
                return;
        }
    }

    public final void onItemClick(AdapterView adapterView, View view, int i2, long j2) {
        if (this.H != null) {
            this.H.a(i2);
        }
        f();
    }
}
