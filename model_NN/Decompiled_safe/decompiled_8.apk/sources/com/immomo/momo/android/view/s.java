package com.immomo.momo.android.view;

final class s implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ r f2827a;

    s(r rVar) {
        this.f2827a = rVar;
    }

    public final void run() {
        if (!this.f2827a.f2826a.s) {
            if (this.f2827a.f2826a.getVisibility() != 0) {
                this.f2827a.f2826a.setVisibility(0);
                this.f2827a.f2826a.p.setPadding(this.f2827a.f2826a.d, this.f2827a.f2826a.b, this.f2827a.f2826a.e, this.f2827a.f2826a.c);
                this.f2827a.f2826a.q.setVisibility(0);
                if (this.f2827a.b.j) {
                    this.f2827a.f2826a.o.setVisibility(0);
                } else {
                    this.f2827a.f2826a.o.setVisibility(8);
                }
                this.f2827a.f2826a.x.setVisibility(0);
                if (this.f2827a.f2826a.h == null) {
                    this.f2827a.f2826a.h = new q(this.f2827a.f2826a, (byte) 0);
                }
                this.f2827a.f2826a.setAdapter(this.f2827a.f2826a.h);
            }
            if (!this.f2827a.f2826a.n.hasMessages(435)) {
                this.f2827a.f2826a.h.c();
                this.f2827a.f2826a.h();
            }
        }
    }
}
