package com.immomo.momo.android.activity;

import android.content.DialogInterface;

final class br implements DialogInterface.OnCancelListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ bq f1050a;

    br(bq bqVar) {
        this.f1050a = bqVar;
    }

    public final void onCancel(DialogInterface dialogInterface) {
        this.f1050a.cancel(true);
        this.f1050a.c.finish();
    }
}
