package com.immomo.momo.android.c;

import com.immomo.momo.util.aj;
import org.json.JSONObject;

final class am implements aj {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ StringBuilder f2378a;
    private final /* synthetic */ Object b;

    am(StringBuilder sb, Object obj) {
        this.f2378a = sb;
        this.b = obj;
    }

    public final void a(JSONObject jSONObject) {
        if (jSONObject != null) {
            this.f2378a.append(jSONObject.toString());
        }
        synchronized (this.b) {
            this.b.notifyAll();
        }
    }
}
