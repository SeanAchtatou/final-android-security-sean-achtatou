package com.immomo.momo.android.pay;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.a;
import com.immomo.momo.service.aq;
import com.immomo.momo.service.bean.bg;
import java.util.ArrayList;
import java.util.List;

final class p extends d {

    /* renamed from: a  reason: collision with root package name */
    private v f2567a = null;
    private String c = null;
    private boolean d = false;
    /* access modifiers changed from: private */
    public /* synthetic */ BuyMemberActivity e;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public p(BuyMemberActivity buyMemberActivity, Context context, String str, boolean z) {
        super(context);
        this.e = buyMemberActivity;
        this.f2567a = new v(context);
        this.c = str;
        this.d = z;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        bg a2;
        boolean z = false;
        if (this.d) {
            a a3 = a.a();
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            ArrayList arrayList3 = new ArrayList();
            ArrayList arrayList4 = new ArrayList();
            ArrayList arrayList5 = new ArrayList();
            ArrayList arrayList6 = new ArrayList();
            long r = this.e.f.r();
            if (!this.e.v) {
                z = true;
            }
            a2 = a3.a(arrayList, arrayList2, arrayList3, arrayList4, arrayList5, arrayList6, r, z);
        } else {
            BuyMemberActivity.s(this.e);
            a a4 = a.a();
            List t = this.e.n;
            List u = this.e.o;
            List v = this.e.p;
            List w = this.e.q;
            List x = this.e.r;
            List y = this.e.s;
            long r2 = this.e.f.r();
            if (!this.e.v) {
                z = true;
            }
            a2 = a4.a(t, u, v, w, x, y, r2, z);
        }
        if (a2 != null) {
            this.e.f.b(a2.b);
            this.e.f.ad = a2.e;
            this.e.f.ai = a2.d;
            this.e.f.aj = android.support.v4.b.a.a(a2.c);
            new aq().b(this.e.f);
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
        this.f2567a.setCancelable(true);
        this.f2567a.setOnCancelListener(new q(this));
        this.f2567a.a(this.c);
        this.e.a(this.f2567a);
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        super.a(exc);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        this.e.x();
        if (this.d) {
            return;
        }
        if (!this.e.n.isEmpty()) {
            this.e.i.setVisibility(0);
            BuyMemberActivity.a(this.e, this.e.n);
            return;
        }
        this.e.i.setVisibility(8);
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.e.p();
    }
}
