package com.immomo.momo.android.activity.feed;

import android.view.View;
import android.widget.ImageView;
import com.immomo.momo.R;

final class bh {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ PublishFeedActivity f1465a;
    private View b;
    private ImageView c;
    private int d;
    private int e;
    /* access modifiers changed from: private */
    public boolean f = false;
    /* access modifiers changed from: private */
    public boolean g = false;
    private int h = -1;
    /* access modifiers changed from: private */
    public int i;

    public bh(PublishFeedActivity publishFeedActivity, View view, int i2) {
        boolean z = true;
        this.f1465a = publishFeedActivity;
        this.b = view;
        this.c = (ImageView) this.b.findViewById(R.id.signeditor_iv_icon);
        this.h = i2;
        switch (i2) {
            case 1:
                this.i = 11;
                this.g = publishFeedActivity.f.q();
                this.d = R.drawable.ic_publish_weibo_normal;
                this.e = R.drawable.ic_publish_weibo_selected;
                this.f = (!((Boolean) publishFeedActivity.g.b("publishfeed_sync_weibo", false)).booleanValue() || !this.g) ? false : z;
                break;
            case 2:
                this.i = 13;
                this.g = publishFeedActivity.f.at;
                this.d = R.drawable.ic_publish_tweibo_normal;
                this.e = R.drawable.ic_publish_tweibo_selected;
                this.f = (!((Boolean) publishFeedActivity.g.b("publishfeed_sync_tx", false)).booleanValue() || !this.g) ? false : z;
                break;
            case 3:
                this.i = 12;
                this.g = publishFeedActivity.f.ar;
                this.d = R.drawable.ic_publish_renren_normal;
                this.e = R.drawable.ic_publish_renren_selected;
                this.f = ((Boolean) publishFeedActivity.g.b("publishfeed_sync_renren", false)).booleanValue() && this.g;
                break;
            case 6:
                this.d = R.drawable.ic_publish_weixin_normal;
                this.e = R.drawable.ic_publish_weixin_selected;
                this.f = ((Boolean) publishFeedActivity.g.b("publishfeed_sync_weinxin", true)).booleanValue();
                break;
        }
        a(this.f);
        this.b.setOnClickListener(new bi(this, i2));
    }

    public final void a(boolean z) {
        this.f = z;
        if (z) {
            this.c.setImageResource(this.e);
        } else {
            this.c.setImageResource(this.d);
        }
    }

    public final boolean a() {
        return this.f;
    }

    public final void b() {
        if (this.g) {
            switch (this.h) {
                case 1:
                    this.f1465a.g.c("publishfeed_sync_weibo", Boolean.valueOf(this.f));
                    break;
                case 2:
                    this.f1465a.g.c("publishfeed_sync_tx", Boolean.valueOf(this.f));
                    break;
                case 3:
                    this.f1465a.g.c("publishfeed_sync_renren", Boolean.valueOf(this.f));
                    break;
            }
        }
        if (this.h == 6) {
            this.f1465a.g.c("publishfeed_sync_weinxin", Boolean.valueOf(this.f));
        }
    }
}
