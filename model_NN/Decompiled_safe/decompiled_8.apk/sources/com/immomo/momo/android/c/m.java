package com.immomo.momo.android.c;

import android.support.v4.b.a;
import com.immomo.momo.protocol.a.d;
import com.immomo.momo.service.bean.Message;
import com.immomo.momo.util.h;
import com.immomo.momo.util.jni.LocalAudioHolder;
import java.io.File;
import java.io.IOException;

public final class m extends t {
    private static com.immomo.momo.util.m c = new com.immomo.momo.util.m("test_momo", "[ from LoadAudioFileThread ]");

    /* renamed from: a  reason: collision with root package name */
    private Message f2387a = null;
    private g b;

    public m(Message message, g gVar) {
        super(gVar);
        this.b = gVar;
        this.f2387a = message;
    }

    public final void a() {
    }

    public final void run() {
        c.b((Object) ("filepath:" + this.f2387a.fileName + " fileName:" + a.a(this.f2387a)));
        try {
            File b2 = h.b(a.a(this.f2387a));
            if (b2.exists()) {
                this.f2387a.tempFile = b2;
                c.a((Object) ("LoadAudioThread ---has load a audio:" + b2.getAbsolutePath()));
                this.b.a(b2);
                return;
            }
        } catch (IOException e) {
            c.a((Throwable) e);
        }
        File file = null;
        try {
            String a2 = a.a(this.f2387a);
            file = d.a(a2, this.f2387a.chatType);
            file = h.b(a2);
            LocalAudioHolder.decodeAMR2WAV(file.getPath(), file.getPath());
            file.delete();
            this.f2387a.tempFile = file;
        } catch (Exception e2) {
            c.a((Throwable) e2);
        } finally {
            this.b.a(file);
        }
    }
}
