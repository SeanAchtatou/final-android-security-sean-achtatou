package com.immomo.momo.android.activity.feed;

import android.content.DialogInterface;

final class c implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ CorrectSiteActivity f1469a;

    c(CorrectSiteActivity correctSiteActivity) {
        this.f1469a = correctSiteActivity;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        dialogInterface.dismiss();
        this.f1469a.finish();
    }
}
