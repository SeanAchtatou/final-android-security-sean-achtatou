package com.immomo.momo.android.activity.emotestore;

import android.content.Context;
import android.content.Intent;
import com.immomo.momo.a.a;
import com.immomo.momo.android.broadcast.z;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.i;
import com.immomo.momo.service.aq;
import com.immomo.momo.util.k;

final class u extends d {

    /* renamed from: a  reason: collision with root package name */
    private String f1332a;
    /* access modifiers changed from: private */
    public /* synthetic */ EmotionProfileActivity c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public u(EmotionProfileActivity emotionProfileActivity, Context context, String str) {
        super(context);
        this.c = emotionProfileActivity;
        this.f1332a = str;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        String a2 = i.a().a(this.c.j, this.f1332a, this.c.f);
        this.c.h.q = true;
        this.c.h.u = true;
        this.c.i.b(this.c.h);
        this.c.i.a(this.c.h);
        new aq().b(this.c.f);
        return a2;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.c.a(new v(f(), this));
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        new k("U", "U62").e();
        if (!(exc instanceof a) || ((a) exc).f670a != 20405) {
            super.a(exc);
        } else {
            EmotionProfileActivity.d(this.c);
        }
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        String str = (String) obj;
        new k("U", "U61").e();
        com.immomo.momo.android.c.u.b().execute(new v(this));
        this.c.o.sendEmptyMessageDelayed(45, 100);
        this.c.u();
        Intent intent = new Intent(z.f2368a);
        intent.putExtra("eid", this.c.j);
        this.c.sendBroadcast(intent);
        if (!android.support.v4.b.a.a((CharSequence) str)) {
            a(str);
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.c.p();
    }
}
