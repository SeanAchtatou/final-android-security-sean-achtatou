package com.immomo.momo.android.activity.common;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.b.a;
import android.view.View;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.WelcomeActivity;
import com.immomo.momo.android.activity.lh;
import com.immomo.momo.g;
import com.immomo.momo.util.ao;

public class ShareToMomoActivity extends j {
    private static String i = (String.valueOf(g.h()) + "share_has_group");
    private int j = 1;
    private String k;
    private boolean l = true;
    /* access modifiers changed from: private */
    public String m = PoiTypeDef.All;
    /* access modifiers changed from: private */
    public String n = PoiTypeDef.All;
    /* access modifiers changed from: private */
    public String o = PoiTypeDef.All;
    /* access modifiers changed from: private */
    public String p = PoiTypeDef.All;

    private void b(String str, int i2) {
        Uri data = getIntent().getData();
        if (data != null) {
            a(new bu(this, this, i2, data, str)).execute(new Object[0]);
        } else {
            a(new bs(this, this, i2, str, PoiTypeDef.All)).execute(new Object[0]);
        }
    }

    /* access modifiers changed from: protected */
    public final String A() {
        return PoiTypeDef.All;
    }

    /* access modifiers changed from: protected */
    public final void B() {
        if (this.f == null) {
            ao.e(R.string.feed_publish_dialog_content_unlogin);
            Intent intent = new Intent(getApplicationContext(), WelcomeActivity.class);
            intent.addFlags(268435456);
            getApplicationContext().startActivity(intent);
            Intent intent2 = new Intent();
            intent2.putExtra("emsg", "客户端没有登录");
            setResult(40102, intent2);
            finish();
        }
        Bundle extras = getIntent().getExtras();
        if (extras == null) {
            finish();
            return;
        }
        this.e.b((Object) ("--------imageuri=" + getIntent().getData()));
        if (extras.containsKey("title")) {
            this.k = extras.getString("title");
        }
        if (extras.containsKey(i)) {
            this.l = extras.getBoolean(i);
        }
        if (extras.containsKey("appid")) {
            this.m = extras.getString("appid");
        }
        if (extras.containsKey("token")) {
            this.n = extras.getString("token");
        }
        if (extras.containsKey("callback")) {
            this.o = extras.getString("callback");
        }
        if (extras.containsKey("content")) {
            this.p = extras.getString("content");
        }
    }

    /* access modifiers changed from: protected */
    public final void C() {
        int i2 = 0;
        if (this.l) {
            this.j = 2;
            a(ay.class, a.class, ba.class);
            findViewById(R.id.contact_tab_group).setVisibility(0);
        } else {
            this.j = 1;
            a(ay.class, a.class);
            findViewById(R.id.contact_tab_group).setVisibility(8);
        }
        findViewById(R.id.contact_tab_both).setOnClickListener(this);
        findViewById(R.id.contact_tab_follows).setOnClickListener(this);
        findViewById(R.id.contact_tab_group).setOnClickListener(this);
        int intExtra = getIntent().getIntExtra("showindex", 0);
        if (intExtra >= 0) {
            i2 = intExtra > this.j ? this.j : intExtra;
        }
        c(i2);
    }

    /* access modifiers changed from: protected */
    public final void a(int i2, int i3) {
    }

    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        super.a(bundle);
        x();
    }

    /* access modifiers changed from: protected */
    public final void a(Fragment fragment, int i2) {
        switch (i2) {
            case 0:
                findViewById(R.id.contact_tab_both).setSelected(true);
                findViewById(R.id.contact_tab_follows).setSelected(false);
                findViewById(R.id.contact_tab_group).setSelected(false);
                break;
            case 1:
                findViewById(R.id.contact_tab_both).setSelected(false);
                findViewById(R.id.contact_tab_follows).setSelected(true);
                findViewById(R.id.contact_tab_group).setSelected(false);
                break;
            case 2:
                findViewById(R.id.contact_tab_both).setSelected(false);
                findViewById(R.id.contact_tab_follows).setSelected(false);
                findViewById(R.id.contact_tab_group).setSelected(true);
                break;
        }
        ((lh) fragment).a(this, m());
        a(fragment);
    }

    /* access modifiers changed from: protected */
    public final void a(String str, int i2) {
        if (!a.a((CharSequence) str)) {
            switch (i2) {
                case 0:
                    b(str, 1);
                    return;
                case 1:
                    b(str, 2);
                    return;
                case 2:
                    b(str, 3);
                    return;
                default:
                    return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.contact_tab_both /*2131165444*/:
                c(0);
                return;
            case R.id.contact_tab_follows /*2131165445*/:
                c(1);
                return;
            case R.id.contact_tab_fans /*2131165446*/:
            default:
                return;
            case R.id.contact_tab_group /*2131165447*/:
                c(2);
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
    }

    /* access modifiers changed from: protected */
    public final void x() {
        super.x();
        if (!a.a((CharSequence) this.k)) {
            setTitle(this.k);
        }
    }

    /* access modifiers changed from: protected */
    public final boolean y() {
        return true;
    }

    /* access modifiers changed from: protected */
    public final int z() {
        return 1;
    }
}
