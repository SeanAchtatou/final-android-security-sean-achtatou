package com.immomo.momo.android.activity.feed;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.protocol.a.k;
import com.immomo.momo.service.bean.ab;
import java.util.ArrayList;
import java.util.List;

final class an extends d {

    /* renamed from: a  reason: collision with root package name */
    private List f1445a = new ArrayList();
    private /* synthetic */ OtherFeedListActivity c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public an(OtherFeedListActivity otherFeedListActivity, Context context) {
        super(context);
        this.c = otherFeedListActivity;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        boolean a2 = k.a().a(this.f1445a, this.c.u, this.c.r, OtherFeedListActivity.n(this.c));
        this.c.m.a(this.f1445a);
        if (this.f1445a != null && this.f1445a.size() > 0) {
            OtherFeedListActivity otherFeedListActivity = this.c;
            otherFeedListActivity.u = otherFeedListActivity.u + this.f1445a.size();
            this.b.a((Object) ("downloadOtherFeedList success, lastindex:" + this.c.u));
        }
        return Boolean.valueOf(a2);
    }

    /* access modifiers changed from: protected */
    public final void a() {
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        Boolean bool = (Boolean) obj;
        if (this.f1445a.isEmpty() || !bool.booleanValue()) {
            this.b.a((Object) "remove...");
            this.c.h.k();
        }
        for (ab abVar : this.f1445a) {
            abVar.b = this.c.s;
            if (!this.c.k.contains(abVar)) {
                this.c.p.b(abVar);
            }
        }
        this.c.p.notifyDataSetChanged();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.c.w();
    }
}
