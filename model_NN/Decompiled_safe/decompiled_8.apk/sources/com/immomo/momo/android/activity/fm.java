package com.immomo.momo.android.activity;

import android.graphics.Bitmap;
import android.support.v4.view.u;
import android.view.View;
import android.view.ViewGroup;
import com.immomo.momo.R;
import com.immomo.momo.android.view.photoview.PhotoView;
import com.immomo.momo.g;

final class fm extends u {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ImageBrowserActivity f1495a;

    fm(ImageBrowserActivity imageBrowserActivity) {
        this.f1495a = imageBrowserActivity;
    }

    public final /* synthetic */ Object a(ViewGroup viewGroup, int i) {
        int a2 = this.f1495a.c(i);
        this.f1495a.i.a((Object) ("------------------instantiateItem " + a2));
        View inflate = g.o().inflate((int) R.layout.include_imagebrower_item, (ViewGroup) null);
        PhotoView photoView = (PhotoView) inflate.findViewById(R.id.imageview);
        fn fnVar = (fn) this.f1495a.h.get(a2);
        inflate.setId(a2);
        Bitmap a3 = fnVar.g;
        if (a3 != null) {
            photoView.setImageBitmap(a3);
        } else {
            ImageBrowserActivity.a(this.f1495a, fnVar, photoView);
        }
        viewGroup.addView(inflate);
        return inflate;
    }

    public final void a(ViewGroup viewGroup, int i, Object obj) {
        viewGroup.removeView((View) obj);
    }

    public final boolean a(View view, Object obj) {
        return view == obj;
    }

    public final int b() {
        if (this.f1495a.h.size() > 1) {
            return Integer.MAX_VALUE;
        }
        return this.f1495a.h.size();
    }
}
