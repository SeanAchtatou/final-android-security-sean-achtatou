package com.immomo.momo.android.game;

import android.content.Context;
import com.immomo.momo.android.activity.ao;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.m;

final class r extends d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ k f2442a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public r(k kVar, Context context) {
        super(context);
        this.f2442a = kVar;
    }

    /* access modifiers changed from: protected */
    public final Object a(Object... objArr) {
        m.a().b(this.f2442a.U, this.f2442a.V, this.f2442a.O, this.f2442a.W);
        return null;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.f2442a.a(new v(this.f2442a.c(), "正在获取支付信息..."));
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        super.a(exc);
        this.f2442a.X = false;
    }

    /* access modifiers changed from: protected */
    public final void a(Object obj) {
        ((ao) this.f2442a.c()).b(new t(this.f2442a, this.f2442a.c()));
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.f2442a.N();
    }
}
