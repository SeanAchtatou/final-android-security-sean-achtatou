package com.immomo.momo.android.activity;

import android.support.v4.b.a;
import android.view.View;
import com.immomo.momo.util.k;

final class fh extends a {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ImageBrowserActivity f1490a;

    fh(ImageBrowserActivity imageBrowserActivity) {
        this.f1490a = imageBrowserActivity;
    }

    public final void a_(int i) {
        if (this.f1490a.v.equals("avator")) {
            new k("S", "S2101").e();
        }
        int a2 = this.f1490a.c(i);
        if (this.f1490a.h.size() == 2) {
            for (int i2 = 0; i2 < this.f1490a.k.getChildCount(); i2++) {
                View childAt = this.f1490a.k.getChildAt(i2);
                if (childAt.getId() == a2) {
                    this.f1490a.a(childAt, a2);
                }
            }
            return;
        }
        this.f1490a.a(this.f1490a.k.findViewById(a2), a2);
    }
}
