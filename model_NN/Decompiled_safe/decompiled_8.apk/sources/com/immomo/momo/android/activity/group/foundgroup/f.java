package com.immomo.momo.android.activity.group.foundgroup;

import android.support.v4.b.a;
import android.view.View;
import android.widget.RadioGroup;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.util.k;

public final class f extends a implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private FoundGroupActivity f1665a;
    /* access modifiers changed from: private */
    public TextView b;
    private View c;
    private RadioGroup d;
    /* access modifiers changed from: private */
    public boolean e = false;
    /* access modifiers changed from: private */
    public s f;

    public f(View view, FoundGroupActivity foundGroupActivity, s sVar) {
        super(view);
        this.f1665a = foundGroupActivity;
        this.f = sVar;
        this.b = (TextView) a((int) R.id.creategroup_tv_sitename);
        this.d = (RadioGroup) a((int) R.id.creategroup_rg_sitetype);
        this.c = a((int) R.id.layout_choose_location);
        if (!a.a((CharSequence) this.f.d)) {
            this.b.setText(this.f.d);
        }
        f();
        this.c.setOnClickListener(this);
        this.d.setOnCheckedChangeListener(new g(this));
    }

    /* access modifiers changed from: package-private */
    public final void a(String str) {
        this.b.setText(str);
    }

    public final void a(boolean z) {
        this.e = z;
    }

    public final void b() {
    }

    /* access modifiers changed from: protected */
    public final boolean c() {
        if (this.b.getText().toString().trim().length() != 0) {
            return true;
        }
        this.f1665a.a((int) R.string.str_edit_groupinfo_chooseplace);
        return false;
    }

    /* access modifiers changed from: protected */
    public final void d() {
        new k("PI", "P642").e();
    }

    /* access modifiers changed from: protected */
    public final void e() {
        new k("PO", "P642").e();
    }

    public final void f() {
        int i = R.id.creategroup_rb_residential;
        switch (this.f.g) {
            case 2:
                i = R.id.creategroup_rb_office;
                break;
            case 3:
                i = R.id.creategroup_rb_unit;
                break;
        }
        this.d.check(i);
    }

    public final void onClick(View view) {
        switch (view.getId()) {
            case R.id.layout_choose_location /*2131166141*/:
                this.f1665a.onClick(view);
                return;
            default:
                return;
        }
    }
}
