package com.immomo.momo.android.activity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.android.view.HeaderLayout;
import com.immomo.momo.g;
import com.immomo.momo.util.ao;
import com.immomo.momo.util.k;
import java.util.regex.Pattern;

public class PhoneNumberBindEmailActivity extends ah {
    private HeaderLayout h = null;
    private Button i;
    /* access modifiers changed from: private */
    public EditText j = null;
    /* access modifiers changed from: private */
    public EditText k = null;

    private static boolean a(TextView textView) {
        String trim = textView.getText().toString().trim();
        if (trim != null && trim.length() > 0) {
            return false;
        }
        textView.requestFocus();
        return true;
    }

    static /* synthetic */ boolean c(PhoneNumberBindEmailActivity phoneNumberBindEmailActivity) {
        boolean z;
        boolean z2;
        if (a((TextView) phoneNumberBindEmailActivity.j)) {
            ao.e(R.string.reg_email_empty);
            z = false;
        } else {
            if (!Pattern.compile("\\w[\\w.-]*@[\\w.]+\\.\\w+").matcher(phoneNumberBindEmailActivity.j.getText().toString().trim()).matches()) {
                new k("U", "U13").e();
                phoneNumberBindEmailActivity.j.requestFocus();
                ao.e(R.string.reg_email_formaterror);
                z = false;
            } else {
                z = true;
            }
        }
        if (z) {
            if (a((TextView) phoneNumberBindEmailActivity.k)) {
                ao.e(R.string.reg_pwd_empty);
                z2 = false;
            } else {
                String trim = phoneNumberBindEmailActivity.k.getText().toString().trim();
                if (trim.length() < 6) {
                    phoneNumberBindEmailActivity.k.requestFocus();
                    ao.b(String.format(g.a((int) R.string.reg_pwd_sizemin), "6"));
                    z2 = false;
                } else if (trim.length() > 16) {
                    phoneNumberBindEmailActivity.k.requestFocus();
                    ao.b(String.format(g.a((int) R.string.reg_pwd_sizemax), "16"));
                    z2 = false;
                } else {
                    z2 = true;
                }
            }
            if (z2) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        super.a(bundle);
        setContentView((int) R.layout.activity_phonenumber_checkemail);
        this.h = (HeaderLayout) findViewById(R.id.layout_header);
        this.h.setTitleText("绑定邮箱");
        this.i = (Button) findViewById(R.id.send_button);
        this.j = (EditText) findViewById(R.id.et_email);
        this.k = (EditText) findViewById(R.id.et_pwd);
        this.i.setText("发送验证邮件");
        this.i.setEnabled(true);
        this.i.setOnClickListener(new jp(this));
    }

    /* access modifiers changed from: protected */
    public final void d() {
    }
}
