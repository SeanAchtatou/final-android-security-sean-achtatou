package com.immomo.momo.android.activity.event;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.protocol.a.a.b;
import com.immomo.momo.protocol.a.j;
import com.immomo.momo.service.r;
import com.immomo.momo.util.u;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

final class t extends d {

    /* renamed from: a  reason: collision with root package name */
    private boolean f1422a = false;
    private /* synthetic */ EventFeedProfileActivity c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public t(EventFeedProfileActivity eventFeedProfileActivity, Context context, boolean z) {
        super(context);
        this.c = eventFeedProfileActivity;
        this.f1422a = z;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        b a2;
        if (this.f1422a) {
            a2 = j.a().b(this.c.j);
            r unused = this.c.n;
            u.a(String.valueOf(this.c.j) + "eventfeedcomment");
        } else {
            a2 = j.a().a(this.c.j, this.c.m.getCount(), EventFeedProfileActivity.e(this.c));
        }
        r unused2 = this.c.n;
        String a3 = this.c.j;
        List list = a2.c;
        Object b = r.b(a3);
        if (b != null) {
            b.addAll(list);
        } else {
            b = new ArrayList();
            b.addAll(list);
        }
        u.a(String.valueOf(a3) + "eventfeedcomment", b);
        return a2;
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        EventFeedProfileActivity.a(this.c, this.c.l == null ? 0 : this.c.l.g);
        super.a(exc);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        b bVar = (b) obj;
        if (this.f1422a) {
            this.c.m.a().clear();
        }
        this.c.m.b((Collection) bVar.c);
        EventFeedProfileActivity.a(this.c, bVar.b);
        if (bVar.f2881a > 0) {
            this.c.u.setVisibility(0);
        } else {
            this.c.s.removeFooterView(this.c.u);
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.c.t.e();
        super.b();
    }
}
