package com.immomo.momo.android.activity;

import android.content.DialogInterface;

final class v implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ AlipayUserWelcomeActivity f2307a;

    v(AlipayUserWelcomeActivity alipayUserWelcomeActivity) {
        this.f2307a = alipayUserWelcomeActivity;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        dialogInterface.dismiss();
        this.f2307a.finish();
    }
}
