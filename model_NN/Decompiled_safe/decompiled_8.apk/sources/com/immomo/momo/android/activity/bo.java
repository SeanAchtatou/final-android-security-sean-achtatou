package com.immomo.momo.android.activity;

import android.content.DialogInterface;
import android.content.Intent;

final class bo implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ CheckStatusActivity f1048a;

    bo(CheckStatusActivity checkStatusActivity) {
        this.f1048a = checkStatusActivity;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f1048a.startActivity(new Intent("android.settings.WIRELESS_SETTINGS"));
        dialogInterface.dismiss();
    }
}
