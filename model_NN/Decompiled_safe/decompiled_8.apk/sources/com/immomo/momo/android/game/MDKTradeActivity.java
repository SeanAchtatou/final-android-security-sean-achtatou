package com.immomo.momo.android.game;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.w;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.immomo.a.a.f.a;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.ao;
import com.immomo.momo.android.activity.lh;
import com.immomo.momo.g;

public class MDKTradeActivity extends ao {
    /* access modifiers changed from: private */
    public ViewGroup h = null;
    /* access modifiers changed from: private */
    public String i;
    /* access modifiers changed from: private */
    public String j;
    private String k;
    /* access modifiers changed from: private */
    public ai l = new ai();
    private g m;
    private ac n;
    private k o;
    private ax p;
    private y q;
    private lh r;

    private void a(lh lhVar, boolean z) {
        if (lhVar != this.r) {
            w a2 = c().a();
            lh lhVar2 = this.r;
            if (z) {
                a2.a(R.id.tabcontent, lhVar, lhVar.getClass().getName());
                if (lhVar2 != null) {
                    lhVar.U();
                }
            }
            this.r = lhVar;
            this.r.d(true);
            if (lhVar2 != null) {
                lhVar2.d(false);
                a2.a();
                a2.c(lhVar2);
                m().a();
                this.r.a(this, m());
            }
            a2.d(this.r);
            a2.c();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    static /* synthetic */ void e(MDKTradeActivity mDKTradeActivity) {
        View inflate = g.o().inflate((int) R.layout.listitem_opentrade_tab, mDKTradeActivity.h, false);
        inflate.setOnClickListener(new ap(mDKTradeActivity));
        ((TextView) inflate.findViewById(R.id.opentrade_tab_tv_name)).setText("支付宝");
        mDKTradeActivity.h.addView(inflate);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    static /* synthetic */ void f(MDKTradeActivity mDKTradeActivity) {
        View inflate = g.o().inflate((int) R.layout.listitem_opentrade_tab, mDKTradeActivity.h, false);
        inflate.setOnClickListener(new aq(mDKTradeActivity));
        ((TextView) inflate.findViewById(R.id.opentrade_tab_tv_name)).setText("陌陌币");
        mDKTradeActivity.h.addView(inflate);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    static /* synthetic */ void g(MDKTradeActivity mDKTradeActivity) {
        View inflate = g.o().inflate((int) R.layout.listitem_opentrade_tab, mDKTradeActivity.h, false);
        inflate.setOnClickListener(new ar(mDKTradeActivity));
        ((TextView) inflate.findViewById(R.id.opentrade_tab_tv_name)).setText("充值卡");
        mDKTradeActivity.h.addView(inflate);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    static /* synthetic */ void h(MDKTradeActivity mDKTradeActivity) {
        View inflate = g.o().inflate((int) R.layout.listitem_opentrade_tab, mDKTradeActivity.h, false);
        inflate.setOnClickListener(new as(mDKTradeActivity));
        ((TextView) inflate.findViewById(R.id.opentrade_tab_tv_name)).setText("联通短信");
        mDKTradeActivity.h.addView(inflate);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    static /* synthetic */ void i(MDKTradeActivity mDKTradeActivity) {
        View inflate = g.o().inflate((int) R.layout.listitem_opentrade_tab, mDKTradeActivity.h, false);
        inflate.setOnClickListener(new at(mDKTradeActivity));
        ((TextView) inflate.findViewById(R.id.opentrade_tab_tv_name)).setText("移动短信");
        mDKTradeActivity.h.addView(inflate);
    }

    static /* synthetic */ void j(MDKTradeActivity mDKTradeActivity) {
        boolean z = false;
        if (mDKTradeActivity.m == null) {
            mDKTradeActivity.m = new g(mDKTradeActivity.l, mDKTradeActivity.j, mDKTradeActivity.i);
            z = true;
        }
        mDKTradeActivity.a(mDKTradeActivity.m, z);
    }

    static /* synthetic */ void k(MDKTradeActivity mDKTradeActivity) {
        boolean z = false;
        if (mDKTradeActivity.n == null) {
            mDKTradeActivity.n = new ac(mDKTradeActivity.l, mDKTradeActivity.j, mDKTradeActivity.i);
            z = true;
        }
        mDKTradeActivity.a(mDKTradeActivity.n, z);
    }

    static /* synthetic */ void l(MDKTradeActivity mDKTradeActivity) {
        boolean z = false;
        if (mDKTradeActivity.o == null) {
            mDKTradeActivity.o = new k(mDKTradeActivity.l, mDKTradeActivity.j, mDKTradeActivity.i);
            z = true;
        }
        mDKTradeActivity.a(mDKTradeActivity.o, z);
    }

    static /* synthetic */ void m(MDKTradeActivity mDKTradeActivity) {
        boolean z = false;
        if (mDKTradeActivity.p == null) {
            mDKTradeActivity.p = new ax(mDKTradeActivity.l, mDKTradeActivity.j, mDKTradeActivity.i);
            z = true;
        }
        mDKTradeActivity.a(mDKTradeActivity.p, z);
    }

    static /* synthetic */ void n(MDKTradeActivity mDKTradeActivity) {
        boolean z = false;
        if (mDKTradeActivity.q == null) {
            mDKTradeActivity.q = new y(mDKTradeActivity.l, mDKTradeActivity.j, mDKTradeActivity.i);
            z = true;
        }
        mDKTradeActivity.a(mDKTradeActivity.q, z);
    }

    /* access modifiers changed from: protected */
    public final void d() {
        b(new au(this, this));
    }

    public void onBackPressed() {
        if (this.r == null || !this.r.G()) {
            super.onBackPressed();
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_open_trade);
        this.h = (ViewGroup) findViewById(R.id.opentrade_layout_leftmenu);
        this.i = getIntent().getStringExtra("appid");
        this.j = getIntent().getStringExtra("token");
        this.k = getIntent().getStringExtra("trade_extendnumber");
        String stringExtra = getIntent().getStringExtra("product_id");
        this.l.i = stringExtra;
        this.l.j = this.k;
        if (a.a(this.i) || a.a(this.j) || a.a(stringExtra)) {
            Intent intent = new Intent();
            intent.putExtra("emsg", "参数错误");
            setResult(40105, intent);
            finish();
        }
        d();
    }
}
