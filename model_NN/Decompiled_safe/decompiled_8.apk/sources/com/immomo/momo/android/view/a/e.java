package com.immomo.momo.android.view.a;

import android.content.Context;
import android.database.DataSetObserver;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import com.immomo.momo.R;
import com.immomo.momo.android.c.z;
import com.immomo.momo.service.bean.bf;
import java.lang.reflect.Method;

public class e {

    /* renamed from: a  reason: collision with root package name */
    int f2693a = Integer.MAX_VALUE;
    private Context b;
    /* access modifiers changed from: private */
    public PopupWindow c;
    private ListAdapter d;
    /* access modifiers changed from: private */
    public h e;
    private int f = -2;
    private int g = -2;
    private int h;
    private DataSetObserver i;
    private View j;
    private AdapterView.OnItemClickListener k;
    /* access modifiers changed from: private */
    public final m l = new m(this, (byte) 0);
    private final l m = new l(this, (byte) 0);
    private final k n = new k(this, (byte) 0);
    private final i o = new i(this, (byte) 0);
    /* access modifiers changed from: private */
    public Handler p = new Handler();
    private Rect q = new Rect();
    private boolean r;
    private Drawable[] s = new Drawable[2];

    public e(Context context) {
        this.b = context;
        this.c = new PopupWindow(context);
        this.c.setInputMethodMode(1);
    }

    private static Method a(Class cls, String str, Class... clsArr) {
        while (true) {
            try {
                Method declaredMethod = cls.getDeclaredMethod(str, clsArr);
                if (declaredMethod != null) {
                    declaredMethod.setAccessible(true);
                    return declaredMethod;
                }
            } catch (Exception e2) {
            }
            if (cls.getSuperclass() == null) {
                return null;
            }
            cls = cls.getSuperclass();
        }
    }

    public static void a(Context context, bf bfVar, bf bfVar2, z zVar) {
        o oVar = new o(context, (int) R.array.report_items);
        oVar.setTitle((int) R.string.report_dialog_title);
        oVar.a();
        oVar.a(new t(context, bfVar, bfVar2, zVar));
        oVar.show();
    }

    private int j() {
        int i2;
        int i3;
        int i4;
        boolean z = true;
        int i5 = 0;
        if (this.e == null) {
            Context context = this.b;
            new f(this);
            this.e = new h(context, !this.r);
            this.e.setAdapter(this.d);
            this.e.setOnItemClickListener(this.k);
            this.e.setFocusable(true);
            this.e.setFocusableInTouchMode(true);
            this.e.setOnItemSelectedListener(new g(this));
            this.e.setOnScrollListener(this.n);
            this.c.setContentView(this.e);
        } else {
            this.c.getContentView();
        }
        Drawable background = this.c.getBackground();
        if (background != null) {
            background.getPadding(this.q);
            int i6 = this.q.top + this.q.bottom;
            this.h = -this.q.top;
            i2 = i6;
        } else {
            i2 = 0;
        }
        if (this.c.getInputMethodMode() != 2) {
            z = false;
        }
        try {
            i3 = ((Integer) a(this.c.getClass(), "getMaxAvailableHeight", View.class, Integer.TYPE, Boolean.TYPE).invoke(this.c, this.j, Integer.valueOf(this.h), Boolean.valueOf(z))).intValue();
        } catch (Exception e2) {
            i3 = 0;
        }
        if (this.f == -1) {
            return i3 + i2;
        }
        try {
            i4 = ((Integer) a(ListView.class, "measureHeightOfChildren", Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE).invoke(this.e, 0, 0, -1, Integer.valueOf(i3 + 0), -1)).intValue();
        } catch (Exception e3) {
            i4 = 0;
        }
        if (i4 > 0) {
            i5 = i2 + 0;
        }
        return i4 + i5;
    }

    public final void a() {
        this.r = true;
        this.c.setFocusable(true);
    }

    public final void a(int i2) {
        this.g = i2;
    }

    public final void a(View view) {
        this.j = view;
    }

    public void a(AdapterView.OnItemClickListener onItemClickListener) {
        this.k = onItemClickListener;
    }

    public final void a(ListAdapter listAdapter) {
        if (this.i == null) {
            this.i = new j(this, (byte) 0);
        } else if (this.d != null) {
            this.d.unregisterDataSetObserver(this.i);
        }
        this.d = listAdapter;
        if (this.d != null) {
            listAdapter.registerDataSetObserver(this.i);
        }
        if (this.e != null) {
            this.e.setAdapter(this.d);
        }
    }

    public void a(PopupWindow.OnDismissListener onDismissListener) {
        this.c.setOnDismissListener(onDismissListener);
    }

    public final void a(Drawable[] drawableArr) {
        if (drawableArr != null && drawableArr.length > 0) {
            if (drawableArr.length < 2) {
                Drawable drawable = drawableArr[0];
                this.c.setBackgroundDrawable(drawable);
                this.s[0] = drawable;
                this.s[1] = null;
                return;
            }
            this.s[0] = drawableArr[0];
            this.s[1] = drawableArr[1];
            this.c.setBackgroundDrawable(this.s[0]);
        }
    }

    public final View b() {
        return this.j;
    }

    public final void c() {
        this.f = -2;
    }

    /* JADX WARNING: Removed duplicated region for block: B:64:0x016d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void d() {
        /*
            r12 = this;
            r11 = 2
            r10 = -2
            r0 = -1
            r6 = 1
            r2 = 0
            int r5 = r12.j()
            boolean r1 = r12.h()
            android.widget.PopupWindow r3 = r12.c     // Catch:{ Exception -> 0x01c5 }
            java.lang.Class r3 = r3.getClass()     // Catch:{ Exception -> 0x01c5 }
            java.lang.String r4 = "setAllowScrollingAnchorParent"
            r7 = 1
            java.lang.Class[] r7 = new java.lang.Class[r7]     // Catch:{ Exception -> 0x01c5 }
            r8 = 0
            java.lang.Class r9 = java.lang.Boolean.TYPE     // Catch:{ Exception -> 0x01c5 }
            r7[r8] = r9     // Catch:{ Exception -> 0x01c5 }
            java.lang.reflect.Method r3 = a(r3, r4, r7)     // Catch:{ Exception -> 0x01c5 }
            android.widget.PopupWindow r4 = r12.c     // Catch:{ Exception -> 0x01c5 }
            r7 = 1
            java.lang.Object[] r7 = new java.lang.Object[r7]     // Catch:{ Exception -> 0x01c5 }
            r8 = 0
            r9 = 0
            java.lang.Boolean r9 = java.lang.Boolean.valueOf(r9)     // Catch:{ Exception -> 0x01c5 }
            r7[r8] = r9     // Catch:{ Exception -> 0x01c5 }
            r3.invoke(r4, r7)     // Catch:{ Exception -> 0x01c5 }
        L_0x0031:
            android.widget.PopupWindow r3 = r12.c
            boolean r3 = r3.isShowing()
            if (r3 == 0) goto L_0x009b
            int r3 = r12.g
            if (r3 != r0) goto L_0x0075
            r4 = r0
        L_0x003e:
            int r3 = r12.f
            if (r3 != r0) goto L_0x0094
            if (r1 == 0) goto L_0x0083
        L_0x0044:
            if (r1 == 0) goto L_0x0087
            android.widget.PopupWindow r1 = r12.c
            int r3 = r12.g
            if (r3 != r0) goto L_0x0085
        L_0x004c:
            r1.setWindowLayoutMode(r0, r2)
        L_0x004f:
            android.widget.PopupWindow r0 = r12.c
            r0.setOutsideTouchable(r6)
            android.widget.PopupWindow r0 = r12.c
            android.view.View r1 = r12.j
            int r3 = r12.h
            r0.update(r1, r2, r3, r4, r5)
        L_0x005d:
            android.view.View r0 = r12.j
            if (r0 == 0) goto L_0x0074
            java.lang.Class<android.widget.PopupWindow> r0 = android.widget.PopupWindow.class
            java.lang.String r1 = "unregisterForScrollChanged"
            r2 = 0
            java.lang.Class[] r2 = new java.lang.Class[r2]     // Catch:{ Exception -> 0x01bf }
            java.lang.reflect.Method r0 = a(r0, r1, r2)     // Catch:{ Exception -> 0x01bf }
            android.widget.PopupWindow r1 = r12.c     // Catch:{ Exception -> 0x01bf }
            r2 = 0
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ Exception -> 0x01bf }
            r0.invoke(r1, r2)     // Catch:{ Exception -> 0x01bf }
        L_0x0074:
            return
        L_0x0075:
            int r3 = r12.g
            if (r3 != r10) goto L_0x0080
            android.view.View r3 = r12.j
            int r4 = r3.getWidth()
            goto L_0x003e
        L_0x0080:
            int r4 = r12.g
            goto L_0x003e
        L_0x0083:
            r5 = r0
            goto L_0x0044
        L_0x0085:
            r0 = r2
            goto L_0x004c
        L_0x0087:
            android.widget.PopupWindow r3 = r12.c
            int r1 = r12.g
            if (r1 != r0) goto L_0x0092
            r1 = r0
        L_0x008e:
            r3.setWindowLayoutMode(r1, r0)
            goto L_0x004f
        L_0x0092:
            r1 = r2
            goto L_0x008e
        L_0x0094:
            int r0 = r12.f
            if (r0 == r10) goto L_0x004f
            int r5 = r12.f
            goto L_0x004f
        L_0x009b:
            int r1 = r12.g
            if (r1 != r0) goto L_0x0176
            r1 = r0
        L_0x00a0:
            int r3 = r12.f
            if (r3 != r0) goto L_0x0192
            r3 = r0
        L_0x00a5:
            android.widget.PopupWindow r4 = r12.c
            r4.setWindowLayoutMode(r1, r3)
            android.widget.PopupWindow r1 = r12.c     // Catch:{ Exception -> 0x01c2 }
            java.lang.Class r1 = r1.getClass()     // Catch:{ Exception -> 0x01c2 }
            java.lang.String r3 = "setClipToScreenEnabled"
            r4 = 1
            java.lang.Class[] r4 = new java.lang.Class[r4]     // Catch:{ Exception -> 0x01c2 }
            r5 = 0
            java.lang.Class r7 = java.lang.Boolean.TYPE     // Catch:{ Exception -> 0x01c2 }
            r4[r5] = r7     // Catch:{ Exception -> 0x01c2 }
            java.lang.reflect.Method r1 = a(r1, r3, r4)     // Catch:{ Exception -> 0x01c2 }
            android.widget.PopupWindow r3 = r12.c     // Catch:{ Exception -> 0x01c2 }
            r4 = 1
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ Exception -> 0x01c2 }
            r5 = 0
            r7 = 1
            java.lang.Boolean r7 = java.lang.Boolean.valueOf(r7)     // Catch:{ Exception -> 0x01c2 }
            r4[r5] = r7     // Catch:{ Exception -> 0x01c2 }
            r1.invoke(r3, r4)     // Catch:{ Exception -> 0x01c2 }
        L_0x00ce:
            android.widget.PopupWindow r1 = r12.c
            r1.setOutsideTouchable(r6)
            android.widget.PopupWindow r1 = r12.c
            com.immomo.momo.android.view.a.l r3 = r12.m
            r1.setTouchInterceptor(r3)
            android.view.View r1 = r12.j
            android.view.View r3 = r1.getRootView()
            int r4 = r1.getHeight()
            android.widget.PopupWindow r5 = r12.c
            int r5 = r5.getHeight()
            android.widget.PopupWindow r7 = r12.c
            int r7 = r7.getWidth()
            int[] r8 = new int[r11]
            int[] r9 = new int[r11]
            android.graphics.Rect r10 = new android.graphics.Rect
            r10.<init>()
            r1.getLocationInWindow(r8)
            r1.getLocationOnScreen(r9)
            r1.getWindowVisibleDisplayFrame(r10)
            r8 = r8[r2]
            int r8 = r8 + 0
            r11 = r9[r6]
            int r4 = r4 + r11
            int r4 = r4 + 0
            int r4 = r4 + r5
            int r5 = r10.bottom
            if (r4 > r5) goto L_0x011a
            int r4 = r8 + r7
            int r3 = r3.getWidth()
            int r3 = r4 - r3
            if (r3 <= 0) goto L_0x01c8
        L_0x011a:
            int r3 = r10.bottom
            r4 = r9[r6]
            int r3 = r3 - r4
            int r1 = r1.getHeight()
            int r1 = r3 - r1
            int r1 = r1 + 0
            r3 = r9[r6]
            int r3 = r3 + 0
            int r4 = r10.top
            int r3 = r3 - r4
            if (r1 >= r3) goto L_0x01a8
            r1 = r6
        L_0x0131:
            if (r1 == 0) goto L_0x01aa
            com.immomo.momo.android.view.a.h r1 = r12.e
            r1.setStackFromBottom(r6)
            android.graphics.drawable.Drawable[] r1 = r12.s
            int r1 = r1.length
            if (r1 <= r6) goto L_0x01aa
            android.graphics.drawable.Drawable[] r1 = r12.s
            r1 = r1[r6]
            if (r1 == 0) goto L_0x01aa
            android.widget.PopupWindow r1 = r12.c
            android.graphics.drawable.Drawable[] r3 = r12.s
            r3 = r3[r6]
            r1.setBackgroundDrawable(r3)
        L_0x014c:
            android.widget.PopupWindow r1 = r12.c
            android.view.View r3 = r12.j
            int r4 = r12.h
            r1.showAsDropDown(r3, r2, r4)
            com.immomo.momo.android.view.a.h r1 = r12.e
            r1.setSelection(r0)
            boolean r0 = r12.r
            if (r0 == 0) goto L_0x0166
            com.immomo.momo.android.view.a.h r0 = r12.e
            boolean r0 = r0.isInTouchMode()
            if (r0 == 0) goto L_0x0169
        L_0x0166:
            r12.f()
        L_0x0169:
            boolean r0 = r12.r
            if (r0 != 0) goto L_0x005d
            android.os.Handler r0 = r12.p
            com.immomo.momo.android.view.a.i r1 = r12.o
            r0.post(r1)
            goto L_0x005d
        L_0x0176:
            int r1 = r12.g
            if (r1 != r10) goto L_0x0188
            android.widget.PopupWindow r1 = r12.c
            android.view.View r3 = r12.j
            int r3 = r3.getWidth()
            r1.setWidth(r3)
            r1 = r2
            goto L_0x00a0
        L_0x0188:
            android.widget.PopupWindow r1 = r12.c
            int r3 = r12.g
            r1.setWidth(r3)
            r1 = r2
            goto L_0x00a0
        L_0x0192:
            int r3 = r12.f
            if (r3 != r10) goto L_0x019e
            android.widget.PopupWindow r3 = r12.c
            r3.setHeight(r5)
            r3 = r2
            goto L_0x00a5
        L_0x019e:
            android.widget.PopupWindow r3 = r12.c
            int r4 = r12.f
            r3.setHeight(r4)
            r3 = r2
            goto L_0x00a5
        L_0x01a8:
            r1 = r2
            goto L_0x0131
        L_0x01aa:
            android.graphics.drawable.Drawable[] r1 = r12.s
            int r1 = r1.length
            if (r1 <= 0) goto L_0x014c
            android.graphics.drawable.Drawable[] r1 = r12.s
            r1 = r1[r2]
            if (r1 == 0) goto L_0x014c
            android.widget.PopupWindow r1 = r12.c
            android.graphics.drawable.Drawable[] r3 = r12.s
            r3 = r3[r2]
            r1.setBackgroundDrawable(r3)
            goto L_0x014c
        L_0x01bf:
            r0 = move-exception
            goto L_0x0074
        L_0x01c2:
            r1 = move-exception
            goto L_0x00ce
        L_0x01c5:
            r3 = move-exception
            goto L_0x0031
        L_0x01c8:
            r1 = r2
            goto L_0x0131
        */
        throw new UnsupportedOperationException("Method not decompiled: com.immomo.momo.android.view.a.e.d():void");
    }

    public final void e() {
        this.c.dismiss();
        this.c.setContentView(null);
        this.e = null;
        this.p.removeCallbacks(this.l);
    }

    public final void f() {
        h hVar = this.e;
        if (hVar != null) {
            hVar.f2696a = true;
            try {
                a(hVar.getClass(), "hideSelector", new Class[0]).invoke(hVar, new Object[0]);
            } catch (Exception e2) {
            }
            hVar.requestLayout();
        }
    }

    public final boolean g() {
        return this.c.isShowing();
    }

    public final boolean h() {
        return this.c.getInputMethodMode() == 2;
    }

    public final ListView i() {
        return this.e;
    }
}
