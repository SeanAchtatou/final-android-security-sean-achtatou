package com.immomo.momo.android.map;

final class q implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ GeoAMapActivity f2511a;

    q(GeoAMapActivity geoAMapActivity) {
        this.f2511a = geoAMapActivity;
    }

    public final void run() {
        if (!this.f2511a.isFinishing() && this.f2511a.b != null) {
            this.f2511a.b.dismiss();
            this.f2511a.b = null;
        }
    }
}
