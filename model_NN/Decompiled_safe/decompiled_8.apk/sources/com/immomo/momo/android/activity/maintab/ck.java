package com.immomo.momo.android.activity.maintab;

import android.graphics.Bitmap;
import android.widget.ImageView;
import com.immomo.momo.R;
import com.immomo.momo.android.c.g;
import com.immomo.momo.util.j;

final class ck implements g {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ cd f1882a;
    private final /* synthetic */ String b;
    private final /* synthetic */ ImageView c;

    ck(cd cdVar, String str, ImageView imageView) {
        this.f1882a = cdVar;
        this.b = str;
        this.c = imageView;
    }

    public final /* synthetic */ void a(Object obj) {
        Bitmap bitmap = (Bitmap) obj;
        j.a(this.b, bitmap);
        if (this.b.equals(this.c.getTag(R.id.tag_item_imageid))) {
            this.f1882a.aA.post(new cl(this.c, bitmap));
        }
    }
}
