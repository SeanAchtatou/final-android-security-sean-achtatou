package com.immomo.momo.android.c;

import android.graphics.Bitmap;
import com.immomo.momo.g;
import java.util.concurrent.BlockingQueue;

final class l implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private BlockingQueue f2386a = null;
    private boolean b = true;

    public l(BlockingQueue blockingQueue) {
        this.f2386a = blockingQueue;
    }

    public final void a() {
        this.b = false;
        try {
            this.f2386a.put(g.u());
        } catch (Throwable th) {
        }
    }

    public final void run() {
        while (true) {
            try {
                if (this.b || this.f2386a.size() > 0) {
                    Bitmap bitmap = (Bitmap) this.f2386a.take();
                    if (bitmap != null) {
                        bitmap.recycle();
                    }
                } else {
                    return;
                }
            } catch (InterruptedException e) {
                return;
            }
        }
    }
}
