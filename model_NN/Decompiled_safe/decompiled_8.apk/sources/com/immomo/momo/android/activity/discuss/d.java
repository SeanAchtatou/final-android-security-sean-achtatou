package com.immomo.momo.android.activity.discuss;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import com.immomo.momo.android.activity.OtherProfileActivity;
import com.immomo.momo.service.bean.p;

final class d implements AdapterView.OnItemClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ DiscussMemberListActivity f1230a;

    d(DiscussMemberListActivity discussMemberListActivity) {
        this.f1230a = discussMemberListActivity;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        Intent intent = new Intent(this.f1230a.getApplicationContext(), OtherProfileActivity.class);
        intent.putExtra("tag", "local");
        intent.putExtra("momoid", ((p) this.f1230a.m.getItem(i)).f3034a);
        this.f1230a.startActivity(intent);
    }
}
