package com.immomo.momo.android.view.a;

import android.view.MotionEvent;
import android.view.View;

final class l implements View.OnTouchListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ e f2700a;

    private l(e eVar) {
        this.f2700a = eVar;
    }

    /* synthetic */ l(e eVar, byte b) {
        this(eVar);
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        int action = motionEvent.getAction();
        int x = (int) motionEvent.getX();
        int y = (int) motionEvent.getY();
        if (action == 0 && this.f2700a.c != null && this.f2700a.c.isShowing() && x >= 0 && x < this.f2700a.c.getWidth() && y >= 0 && y < this.f2700a.c.getHeight()) {
            this.f2700a.p.postDelayed(this.f2700a.l, 250);
            return false;
        } else if (action != 1) {
            return false;
        } else {
            this.f2700a.p.removeCallbacks(this.f2700a.l);
            return false;
        }
    }
}
