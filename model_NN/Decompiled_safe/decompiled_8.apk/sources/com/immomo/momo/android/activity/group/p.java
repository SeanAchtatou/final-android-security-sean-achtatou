package com.immomo.momo.android.activity.group;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.b.a;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.g;
import com.immomo.momo.protocol.a.n;

final class p extends d {

    /* renamed from: a  reason: collision with root package name */
    private v f1690a;
    private /* synthetic */ EditGroupPartyActivity c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public p(EditGroupPartyActivity editGroupPartyActivity, Context context) {
        super(context);
        this.c = editGroupPartyActivity;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        return n.a().b(this.c.x);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.f1690a = new v(this.c);
        this.f1690a.a("请求提交中...");
        this.f1690a.setCancelable(true);
        this.f1690a.setOnCancelListener(new q(this));
        this.f1690a.show();
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        this.b.a((Throwable) exc);
        a(exc.getMessage());
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        String str = (String) obj;
        if (!a.a((CharSequence) str)) {
            a(str);
            this.c.x.o = 2;
            this.c.x.i = 1;
            this.c.A.a(this.c.x);
            this.c.getIntent().putExtra("pid", this.c.x.b);
            this.c.setResult(-1, this.c.getIntent());
        }
        g.d().a(new Bundle(), "actions.groupfeedchanged");
        this.c.finish();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        if (this.f1690a != null) {
            this.f1690a.dismiss();
        }
    }
}
