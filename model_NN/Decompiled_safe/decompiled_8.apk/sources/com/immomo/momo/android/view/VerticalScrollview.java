package com.immomo.momo.android.view;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.ScrollView;

public class VerticalScrollview extends ScrollView {
    public VerticalScrollview(Context context) {
        super(context);
    }

    public VerticalScrollview(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public VerticalScrollview(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        int action = motionEvent.getAction();
        switch (action) {
            case 0:
                Log.i("VerticalScrollview", "onInterceptTouchEvent: DOWN super false");
                super.onTouchEvent(motionEvent);
                break;
            case 1:
                Log.i("VerticalScrollview", "onInterceptTouchEvent: UP super false");
                break;
            case 2:
                break;
            case 3:
                Log.i("VerticalScrollview", "onInterceptTouchEvent: CANCEL super false");
                super.onTouchEvent(motionEvent);
                break;
            default:
                Log.i("VerticalScrollview", "onInterceptTouchEvent: " + action);
                break;
        }
        return false;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        super.onTouchEvent(motionEvent);
        Log.i("VerticalScrollview", "onTouchEvent. action: " + motionEvent.getAction());
        return true;
    }
}
