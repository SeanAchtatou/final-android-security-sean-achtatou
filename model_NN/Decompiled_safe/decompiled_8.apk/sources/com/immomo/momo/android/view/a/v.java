package com.immomo.momo.android.view.a;

import android.app.Dialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.TextView;
import com.immomo.momo.R;

public final class v extends Dialog {

    /* renamed from: a  reason: collision with root package name */
    AsyncTask f2706a;

    public v(Context context) {
        super(context, 2131558450);
        this.f2706a = null;
        setCancelable(true);
        setContentView((int) R.layout.common_flipping_loading_diloag);
        ((TextView) findViewById(R.id.textview)).setText((int) R.string.press);
        setOnCancelListener(new w(this));
    }

    public v(Context context, int i) {
        this(context);
        ((TextView) findViewById(R.id.textview)).setText(i);
    }

    public v(Context context, AsyncTask asyncTask) {
        this(context);
        this.f2706a = asyncTask;
    }

    public v(Context context, CharSequence charSequence) {
        this(context);
        ((TextView) findViewById(R.id.textview)).setText(charSequence);
    }

    public v(Context context, CharSequence charSequence, AsyncTask asyncTask) {
        this(context, charSequence);
        this.f2706a = asyncTask;
    }

    public final void a(CharSequence charSequence) {
        ((TextView) findViewById(R.id.textview)).setText(charSequence);
    }
}
