package com.immomo.momo.android.activity;

import android.content.Context;
import android.content.Intent;
import com.immomo.momo.android.c.d;
import com.immomo.momo.protocol.a.w;
import java.util.ArrayList;

final class jh extends d {

    /* renamed from: a  reason: collision with root package name */
    private long f1763a;
    private /* synthetic */ OtherProfileV2Activity c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public jh(OtherProfileV2Activity otherProfileV2Activity, Context context) {
        super(context);
        this.c = otherProfileV2Activity;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        this.f1763a = this.c.t.X;
        this.c.k = new ArrayList();
        w.a().a(this.c.t, this.c.k);
        this.c.s.d(this.c.t.h);
        this.c.q.b(this.c.t);
        if (this.c.t.ah != null) {
            this.c.r.a(this.c.t.ah);
        }
        if (this.f1763a != this.c.t.X) {
            Intent intent = new Intent(com.immomo.momo.android.broadcast.w.f2366a);
            intent.putExtra("momoid", this.c.t.h);
            this.c.sendBroadcast(new Intent(intent));
        }
        this.b.b((Object) ("user isBind=" + this.c.t.ac));
        return 1;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.b.a((Object) "downloadUserProfile~~~~~~~~~~~~~~");
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        this.c.v();
        this.c.w();
    }
}
