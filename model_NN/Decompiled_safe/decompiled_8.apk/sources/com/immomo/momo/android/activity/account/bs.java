package com.immomo.momo.android.activity.account;

import android.content.Intent;
import android.support.v4.b.a;

final class bs implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ br f972a;

    bs(br brVar) {
        this.f972a = brVar;
    }

    public final void run() {
        Intent intent = new Intent(this.f972a.f971a.q, RegisterActivity.class);
        intent.putExtra("registInterfacetype", this.f972a.f971a.q.o);
        intent.putExtra("timestamp", this.f972a.f971a.q.A);
        intent.putExtra("starttime", this.f972a.f971a.q.B);
        if (a.f(this.f972a.f971a.q.w)) {
            intent.putExtra("alipay_user_id", this.f972a.f971a.q.w);
        }
        this.f972a.f971a.q.startActivity(intent);
        this.f972a.f971a.q.finish();
        this.f972a.f971a.q.overridePendingTransition(0, 0);
    }
}
