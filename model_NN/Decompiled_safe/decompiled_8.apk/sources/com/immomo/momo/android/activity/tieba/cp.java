package com.immomo.momo.android.activity.tieba;

import android.content.Context;
import android.support.v4.b.a;
import com.immomo.momo.R;
import com.immomo.momo.android.c.d;
import com.immomo.momo.protocol.a.v;
import com.immomo.momo.service.bean.c.b;
import mm.purchasesdk.PurchaseCode;

final class cp extends d {

    /* renamed from: a  reason: collision with root package name */
    private int f2217a;
    private b c;
    private String d;
    private /* synthetic */ TieDetailActivity e;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public cp(TieDetailActivity tieDetailActivity, Context context, int i, b bVar, String str) {
        super(context);
        this.e = tieDetailActivity;
        this.f2217a = i;
        this.c = bVar;
        this.d = str;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        switch (this.f2217a) {
            case PurchaseCode.QUERY_OK /*101*/:
                return v.a().c(this.c.f3017a, this.e.O);
            case PurchaseCode.ORDER_OK /*102*/:
                return v.a().j(this.c.f3017a);
            case PurchaseCode.UNSUB_OK /*103*/:
                return v.a().m(this.c.f3017a);
            case PurchaseCode.AUTH_OK /*104*/:
                return v.a().n(this.c.f3017a);
            case 105:
                return v.a().o(this.c.f3017a);
            case 106:
                return v.a().p(this.c.f3017a);
            case 107:
                return v.a().q(this.c.f3017a);
            case 108:
                return v.a().d(this.c.f3017a, this.d);
            case 109:
                return v.a().a(this.c.c, this.c.f3017a, (String) null, this.c.f, this.d);
            default:
                return null;
        }
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.e.a(new com.immomo.momo.android.view.a.v(this.e, this));
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        String str = (String) obj;
        if (!a.a((CharSequence) str)) {
            a(str);
        }
        switch (this.f2217a) {
            case PurchaseCode.QUERY_OK /*101*/:
            default:
                return;
            case PurchaseCode.ORDER_OK /*102*/:
                this.b.b((Object) "话题已经置顶");
                this.c.m = true;
                this.e.o.a(this.c);
                TieDetailActivity tieDetailActivity = this.e;
                b bVar = this.c;
                tieDetailActivity.z.findViewById(R.id.iv_tie_icon_top).setVisibility(0);
                TieDetailActivity.f(this.e, this.c);
                return;
            case PurchaseCode.UNSUB_OK /*103*/:
                this.b.b((Object) "话题取消置顶");
                this.c.m = false;
                this.e.o.a(this.c);
                TieDetailActivity tieDetailActivity2 = this.e;
                b bVar2 = this.c;
                tieDetailActivity2.z.findViewById(R.id.iv_tie_icon_top).setVisibility(8);
                TieDetailActivity.f(this.e, this.c);
                return;
            case PurchaseCode.AUTH_OK /*104*/:
                this.b.b((Object) "话题已经加精");
                this.c.p = true;
                this.e.o.a(this.c);
                TieDetailActivity tieDetailActivity3 = this.e;
                b bVar3 = this.c;
                tieDetailActivity3.z.findViewById(R.id.iv_tie_icon_elite).setVisibility(0);
                TieDetailActivity.f(this.e, this.c);
                return;
            case 105:
                this.b.b((Object) "话题取消加精");
                this.c.p = false;
                this.e.o.a(this.c);
                TieDetailActivity tieDetailActivity4 = this.e;
                b bVar4 = this.c;
                tieDetailActivity4.z.findViewById(R.id.iv_tie_icon_elite).setVisibility(8);
                TieDetailActivity.f(this.e, this.c);
                return;
            case 106:
                this.b.b((Object) "话题已经锁定");
                this.c.o = 4;
                this.e.o.a(this.c.f3017a, 4);
                TieDetailActivity tieDetailActivity5 = this.e;
                b bVar5 = this.c;
                tieDetailActivity5.x();
                return;
            case 107:
                this.b.b((Object) "话题取消锁定");
                this.c.o = 1;
                this.e.o.a(this.c.f3017a, 1);
                this.e.d(this.c);
                return;
            case 108:
            case 109:
                this.b.b((Object) "话题已经删除或禁言");
                this.e.o.a(this.c.f3017a, 2);
                TieDetailActivity.f(this.e, this.c);
                this.e.finish();
                return;
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.e.p();
    }
}
