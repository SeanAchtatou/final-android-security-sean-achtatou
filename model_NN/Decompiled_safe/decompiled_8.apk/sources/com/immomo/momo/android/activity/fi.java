package com.immomo.momo.android.activity;

import android.support.v4.b.a;
import android.view.View;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.android.c.r;
import java.lang.ref.WeakReference;

final class fi implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ImageBrowserActivity f1491a;
    private final /* synthetic */ View b;
    private final /* synthetic */ fn c;

    fi(ImageBrowserActivity imageBrowserActivity, View view, fn fnVar) {
        this.f1491a = imageBrowserActivity;
        this.b = view;
        this.c = fnVar;
    }

    public final void run() {
        View findViewById = this.b.findViewById(R.id.progress_layout);
        this.f1491a.i.a((Object) ("imageItem.isFinish=" + this.c.f));
        if (this.c.f) {
            this.c.b = new fp(this.f1491a.w);
            this.c.b.f = new WeakReference((TextView) this.b.findViewById(R.id.progress_text_percent));
            this.c.b.e = new WeakReference((TextView) this.b.findViewById(R.id.progress_text_size));
            this.c.a(findViewById);
            this.c.m = this.f1491a.w;
            r rVar = null;
            if (this.f1491a.r.equals("URL") && !a.a((CharSequence) this.c.d)) {
                rVar = new r(this.c.d, this.c, this.f1491a.p, this.c.b);
                rVar.a(this.c.d());
            } else if (!a.a((CharSequence) this.c.c)) {
                rVar = new r(this.c.c, this.c, this.f1491a.p, this.c.b);
            }
            this.c.f = false;
            if (rVar.c()) {
                findViewById.setVisibility(8);
            } else {
                findViewById.setVisibility(0);
            }
            ImageBrowserActivity imageBrowserActivity = this.f1491a;
            ImageBrowserActivity.a(rVar);
        } else if (this.c.b != null) {
            this.c.b.f = new WeakReference((TextView) this.b.findViewById(R.id.progress_text_percent));
            this.c.b.e = new WeakReference((TextView) this.b.findViewById(R.id.progress_text_size));
            this.c.b.a();
            this.c.a(findViewById);
            findViewById.setVisibility(0);
        } else {
            findViewById.setVisibility(8);
        }
    }
}
