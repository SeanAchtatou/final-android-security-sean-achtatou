package com.immomo.momo.android.activity.contacts;

import com.immomo.momo.android.a.hf;

final class bv implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ bu f1176a;
    private final /* synthetic */ String b;

    bv(bu buVar, String str) {
        this.f1176a = buVar;
        this.b = str;
    }

    public final void run() {
        int a2 = this.f1176a.f1175a.P.a(this.b, hf.f857a);
        if (a2 < 0 || a2 > this.f1176a.f1175a.P.getCount()) {
            this.f1176a.f1175a.R = this.f1176a.f1175a.U.c();
            this.f1176a.f1175a.P.b(this.f1176a.f1175a.R);
        } else {
            this.f1176a.f1175a.P.b(this.f1176a.f1175a.P.a(this.b, hf.f857a));
            bk.a(this.f1176a.f1175a, this.f1176a.f1175a.P.e());
        }
        this.f1176a.f1175a.P.notifyDataSetChanged();
    }
}
