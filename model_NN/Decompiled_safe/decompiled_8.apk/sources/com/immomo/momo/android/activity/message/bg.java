package com.immomo.momo.android.activity.message;

import android.content.Intent;
import com.immomo.momo.android.activity.group.GroupMemberListActivity;
import com.immomo.momo.android.view.bl;

final class bg implements bl {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ GroupChatActivity f1940a;

    bg(GroupChatActivity groupChatActivity) {
        this.f1940a = groupChatActivity;
    }

    public final void u() {
        this.f1940a.Z.getFooterViewButton().e();
        Intent intent = new Intent(this.f1940a.n(), GroupMemberListActivity.class);
        intent.putExtra("gid", this.f1940a.i);
        intent.putExtra("count", this.f1940a.Q.k);
        this.f1940a.startActivity(intent);
    }
}
