package com.immomo.momo.android.activity.retrieve;

import android.content.DialogInterface;

final class g implements DialogInterface.OnDismissListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ InputPhoneNumberActivity f2113a;

    g(InputPhoneNumberActivity inputPhoneNumberActivity) {
        this.f2113a = inputPhoneNumberActivity;
    }

    public final void onDismiss(DialogInterface dialogInterface) {
        if (this.f2113a.r != null) {
            this.f2113a.r.cancel(true);
            this.f2113a.r = (m) null;
        }
    }
}
