package com.immomo.momo.android.activity.tieba;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.b.a;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.ah;
import com.immomo.momo.android.view.a.n;
import com.immomo.momo.android.view.bi;
import com.immomo.momo.util.ao;
import com.immomo.momo.util.h;
import java.io.File;
import java.util.Date;
import java.util.UUID;
import mm.purchasesdk.PurchaseCode;

public class TiebaManagerApplyActivity extends ah implements View.OnClickListener {
    private EditText h = null;
    private EditText i = null;
    private EditText j = null;
    private ImageView k = null;
    /* access modifiers changed from: private */
    public TextView l = null;
    private bi m = null;
    private String n = null;
    /* access modifiers changed from: private */
    public String o = null;
    /* access modifiers changed from: private */
    public String p = null;
    /* access modifiers changed from: private */
    public String q = null;
    /* access modifiers changed from: private */
    public String r = null;
    private File s = null;
    /* access modifiers changed from: private */
    public File t = null;
    private Bitmap u = null;
    private Handler v = new Handler();

    static /* synthetic */ void f(TiebaManagerApplyActivity tiebaManagerApplyActivity) {
        if (!((Boolean) tiebaManagerApplyActivity.g.b("key_tieba_agree_bazhu_rule", false)).booleanValue()) {
            n a2 = n.a(tiebaManagerApplyActivity, PoiTypeDef.All, "我已阅读并同意", new ei(tiebaManagerApplyActivity));
            a2.setTitle("吧主申请协议");
            View inflate = tiebaManagerApplyActivity.getLayoutInflater().inflate((int) R.layout.dialog_tieba_protocol, (ViewGroup) null);
            WebView webView = (WebView) inflate.findViewById(R.id.webview);
            View findViewById = inflate.findViewById(R.id.loading_indicator);
            WebSettings settings = webView.getSettings();
            settings.setCacheMode(2);
            settings.setJavaScriptEnabled(true);
            settings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
            webView.setWebChromeClient(new ek());
            webView.setDownloadListener(new el(tiebaManagerApplyActivity));
            webView.setWebViewClient(new em(tiebaManagerApplyActivity, findViewById));
            webView.loadUrl("file:///android_asset/tiebamanger.html");
            a2.setOnCancelListener(new ej(tiebaManagerApplyActivity));
            a2.setContentView(inflate);
            a2.show();
        }
    }

    static /* synthetic */ void i(TiebaManagerApplyActivity tiebaManagerApplyActivity) {
        boolean z = false;
        tiebaManagerApplyActivity.p = tiebaManagerApplyActivity.h.getText().toString().trim();
        tiebaManagerApplyActivity.q = tiebaManagerApplyActivity.i.getText().toString().trim();
        tiebaManagerApplyActivity.r = tiebaManagerApplyActivity.j.getText().toString().trim();
        if (!a.f(tiebaManagerApplyActivity.p)) {
            ao.b("请输入姓名", 1);
        } else if (!a.f(tiebaManagerApplyActivity.q)) {
            ao.b("请输入身份证号", 1);
        } else if (tiebaManagerApplyActivity.q.length() < 15) {
            ao.b("请输入正确身份证号", 1);
        } else if (!a.f(tiebaManagerApplyActivity.r)) {
            ao.b("请输入申请原因", 1);
        } else if (tiebaManagerApplyActivity.r.length() < 25) {
            ao.b("内容不能少于25字", 1);
        } else if (tiebaManagerApplyActivity.s == null || !tiebaManagerApplyActivity.s.exists()) {
            ao.b("请拍身份证照片", 1);
        } else {
            z = true;
        }
        if (z) {
            tiebaManagerApplyActivity.b(new eo(tiebaManagerApplyActivity, tiebaManagerApplyActivity));
        }
    }

    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        super.a(bundle);
        setContentView((int) R.layout.activity_bazhuinfo);
        m().setTitleText("吧主申请");
        this.o = getIntent().getStringExtra("apply_tieba_manager_tid");
        this.h = (EditText) findViewById(R.id.bazhu_name);
        this.i = (EditText) findViewById(R.id.bazhu_card_id);
        this.j = (EditText) findViewById(R.id.bazhu_apply_content);
        this.k = (ImageView) findViewById(R.id.iv_camera);
        this.l = (TextView) findViewById(R.id.bazhu_reason_count);
        this.m = new bi(this);
        this.m.a((int) R.drawable.ic_topbar_confirm_white);
        this.m.setMarginRight(8);
        this.m.setBackgroundResource(R.drawable.bg_header_submit);
        m().a(this.m, new en(this));
        findViewById(R.id.iv_camera_cover).setOnClickListener(this);
        this.j.addTextChangedListener(new eh(this));
        this.v.postDelayed(new eg(this), 200);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.h.a(java.lang.String, android.graphics.Bitmap, int, boolean):java.io.File
     arg types: [java.lang.String, android.graphics.Bitmap, int, int]
     candidates:
      com.immomo.momo.util.h.a(java.lang.String, java.lang.String, int, boolean):void
      com.immomo.momo.util.h.a(java.lang.String, android.graphics.Bitmap, int, boolean):java.io.File */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.b.a.a(android.graphics.Bitmap, float, boolean):android.graphics.Bitmap
     arg types: [android.graphics.Bitmap, int, int]
     candidates:
      android.support.v4.b.a.a(android.graphics.Bitmap, int, int):android.graphics.Bitmap
      android.support.v4.b.a.a(java.io.File, int, int):android.graphics.Bitmap
      android.support.v4.b.a.a(java.lang.String, java.lang.String, java.lang.String):java.lang.String
      android.support.v4.b.a.a(java.lang.Object[], java.lang.String, java.lang.String):java.lang.String
      android.support.v4.b.a.a(android.graphics.Bitmap, float, boolean):android.graphics.Bitmap */
    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        switch (i2) {
            case PurchaseCode.UNSUB_OK /*103*/:
                if (i3 == -1) {
                    if (!a.a((CharSequence) this.n)) {
                        this.s = new File(com.immomo.momo.a.i(), this.n);
                        String absolutePath = this.s.getAbsolutePath();
                        String substring = this.s.getName().substring(0, this.s.getName().lastIndexOf("."));
                        Bitmap l2 = a.l(absolutePath);
                        if (l2 != null) {
                            this.t = h.a(substring, l2, 16, false);
                            this.u = a.a(l2, 150.0f, true);
                            h.a(substring, this.u, 15, false);
                            this.k.setImageBitmap(this.u);
                        }
                        findViewById(R.id.tv_card).setVisibility(8);
                        getWindow().getDecorView().requestFocus();
                        return;
                    }
                    return;
                } else if (i3 != 0) {
                    ao.b("拍照失败，请检查", 1);
                    return;
                } else {
                    return;
                }
            default:
                return;
        }
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_camera_cover /*2131165403*/:
                Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
                StringBuffer stringBuffer = new StringBuffer();
                stringBuffer.append("immomo_");
                stringBuffer.append(a.c(new Date()));
                stringBuffer.append("_" + UUID.randomUUID());
                stringBuffer.append(".jpg");
                this.n = stringBuffer.toString();
                intent.putExtra("output", Uri.fromFile(new File(com.immomo.momo.a.i(), this.n)));
                startActivityForResult(intent, PurchaseCode.UNSUB_OK);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (!a.a((CharSequence) this.n)) {
            File file = new File(com.immomo.momo.a.i(), this.n);
            if (file.exists()) {
                file.delete();
            }
            this.n = null;
        }
        super.onDestroy();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.b.a.a(android.graphics.Bitmap, float, boolean):android.graphics.Bitmap
     arg types: [android.graphics.Bitmap, int, int]
     candidates:
      android.support.v4.b.a.a(android.graphics.Bitmap, int, int):android.graphics.Bitmap
      android.support.v4.b.a.a(java.io.File, int, int):android.graphics.Bitmap
      android.support.v4.b.a.a(java.lang.String, java.lang.String, java.lang.String):java.lang.String
      android.support.v4.b.a.a(java.lang.Object[], java.lang.String, java.lang.String):java.lang.String
      android.support.v4.b.a.a(android.graphics.Bitmap, float, boolean):android.graphics.Bitmap */
    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Bundle bundle) {
        super.onRestoreInstanceState(bundle);
        this.n = bundle.getString("camera_file_name");
        if (bundle.containsKey("uploadFile")) {
            try {
                this.t = new File(bundle.getString("uploadFile"));
            } catch (Exception e) {
            }
        }
        if (this.t != null) {
            File file = this.t;
            String absolutePath = file.getAbsolutePath();
            file.getName().substring(0, file.getName().lastIndexOf("."));
            Bitmap l2 = a.l(absolutePath);
            if (l2 != null) {
                this.u = a.a(l2, 150.0f, true);
                this.k.setImageBitmap(this.u);
            }
            findViewById(R.id.tv_card).setVisibility(8);
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putString("camera_file_name", this.n);
        if (this.t != null) {
            bundle.putString("uploadFile", this.t.getAbsoluteFile().toString());
        }
    }
}
