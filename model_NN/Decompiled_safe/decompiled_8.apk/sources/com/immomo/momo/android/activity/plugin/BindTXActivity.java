package com.immomo.momo.android.activity.plugin;

import android.os.Bundle;
import android.webkit.WebView;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.ah;
import com.immomo.momo.android.view.HeaderLayout;
import com.immomo.momo.android.view.a.v;

public class BindTXActivity extends ah {
    private HeaderLayout h = null;
    /* access modifiers changed from: private */
    public String i = "http://www.immomo.com/api/weibo/aouth_callback";
    /* access modifiers changed from: private */
    public WebView j = null;
    private TextView k = null;
    /* access modifiers changed from: private */
    public v l = null;
    /* access modifiers changed from: private */
    public String m;
    /* access modifiers changed from: private */
    public String n = PoiTypeDef.All;
    /* access modifiers changed from: private */
    public String o = PoiTypeDef.All;
    /* access modifiers changed from: private */
    public boolean p = true;

    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        super.a(bundle);
        setContentView((int) R.layout.activity_bind_api);
        this.j = (WebView) findViewById(R.id.web);
        this.j.getSettings().setJavaScriptEnabled(true);
        this.j.setWebViewClient(new ai(this, (byte) 0));
        this.h = (HeaderLayout) findViewById(R.id.layout_header);
        this.h.setTitleText("绑定腾讯微博");
        this.k = (TextView) findViewById(R.id.header_stv_title);
        this.k.setFocusableInTouchMode(false);
        new ac(this, this).execute(new Object[0]);
    }
}
