package com.immomo.momo.android.activity.event;

import android.content.Intent;
import android.view.View;
import android.widget.ExpandableListView;
import com.immomo.momo.service.bean.y;
import com.immomo.momo.service.bean.z;

final class b implements ExpandableListView.OnChildClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ a f1370a;

    b(a aVar) {
        this.f1370a = aVar;
    }

    public final boolean onChildClick(ExpandableListView expandableListView, View view, int i, int i2, long j) {
        a aVar = this.f1370a;
        Intent intent = new Intent(a.H(), EventProfileActivity.class);
        intent.putExtra("eventid", ((y) ((z) this.f1370a.O.get(i)).f3043a.get(i2)).h());
        this.f1370a.a(intent);
        return true;
    }
}
