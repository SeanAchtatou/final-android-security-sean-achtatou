package com.immomo.momo.android.activity.event;

import android.content.Context;
import com.immomo.momo.protocol.a.j;
import java.util.ArrayList;
import java.util.List;

final class d extends com.immomo.momo.android.c.d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ a f1407a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public d(a aVar, Context context) {
        super(context);
        this.f1407a = aVar;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        ArrayList arrayList = new ArrayList();
        j.a().a(arrayList, this.f1407a.S.b());
        this.f1407a.S.d(arrayList);
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public final void a() {
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        super.a(exc);
        this.f1407a.v();
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        this.f1407a.Q();
        this.f1407a.T.setLastFlushTime(this.f1407a.Q);
        this.f1407a.N.a("eventfeed_lasttime_success", this.f1407a.Q);
        this.f1407a.O.clear();
        this.f1407a.O.addAll((List) obj);
        this.f1407a.P.notifyDataSetChanged();
        this.f1407a.P.a();
        this.f1407a.P();
        ((MainEventActivity) this.f1407a.c()).y();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.f1407a.R = null;
    }
}
