package com.immomo.momo.android.activity.contacts;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.g;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

final class av extends d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ an f1153a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public av(an anVar, Context context) {
        super(context);
        this.f1153a = anVar;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        ArrayList arrayList = new ArrayList();
        an.a(this.f1153a, arrayList);
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        List list = (List) obj;
        super.a(list);
        if (list != null && list.size() > 0) {
            if (list.size() > 0) {
                an anVar = this.f1153a;
                anVar.U = anVar.U + list.size();
                this.f1153a.P.b((Collection) list);
                if (this.f1153a.P.getCount() < g.q().w) {
                    return;
                }
            }
            this.f1153a.O.k();
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
        super.b();
        this.f1153a.Y = null;
        this.f1153a.Q.e();
    }
}
