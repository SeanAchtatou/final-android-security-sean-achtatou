package com.immomo.momo.android.game;

import android.os.Bundle;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import com.immomo.momo.android.activity.as;
import com.immomo.momo.android.view.bi;
import com.immomo.momo.util.jni.Codec;
import org.apache.http.util.EncodingUtils;

public class RechargeWebviewActivity extends as {
    private String i;
    private String j;

    /* access modifiers changed from: protected */
    public final int f() {
        return super.f();
    }

    /* access modifiers changed from: protected */
    public final void g() {
        super.g();
        i();
        this.h.getSettings().setSupportZoom(true);
        CookieSyncManager.createInstance(this);
        CookieSyncManager.getInstance().startSync();
        CookieManager.getInstance().removeSessionCookie();
        a(new bi(getApplicationContext()).a("关闭"), new bh(this));
    }

    public void onBackPressed() {
        if (this.h.canGoBack()) {
            this.h.goBack();
        } else {
            super.onBackPressed();
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.i = getIntent().getStringExtra("token");
        this.j = getIntent().getStringExtra("appid");
        this.h.postUrl(Codec.dwf4(), EncodingUtils.getBytes(Codec.weeryt(this.i, this.j), "UTF-8"));
    }
}
