package com.immomo.momo.android.activity.tieba;

import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.b.a;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.ah;
import com.immomo.momo.android.view.a.ao;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.aj;
import com.immomo.momo.util.j;
import java.util.List;

public class TiebaCreateActivity extends ah {
    public static String h = "value_count";
    public static String i = "value_name";
    public static String j = "value_support";
    public static String k = "value_id";
    public static String l = "value_getdata";
    /* access modifiers changed from: private */
    public dk A;
    private View[] B;
    private TextView m;
    private TextView n;
    private TextView o;
    private View p;
    private Button q;
    private Button r;
    /* access modifiers changed from: private */
    public String s = null;
    /* access modifiers changed from: private */
    public dr t;
    /* access modifiers changed from: private */
    public String u;
    /* access modifiers changed from: private */
    public int v;
    /* access modifiers changed from: private */
    public boolean w = true;
    /* access modifiers changed from: private */
    public dn x;
    /* access modifiers changed from: private */
    public dl y;
    /* access modifiers changed from: private */
    public dp z;

    static /* synthetic */ void a(TiebaCreateActivity tiebaCreateActivity, List list) {
        int i2 = 0;
        while (i2 < list.size() && i2 <= 8) {
            tiebaCreateActivity.B[i2].setVisibility(0);
            j.a((aj) list.get(i2), (ImageView) tiebaCreateActivity.B[i2].findViewById(R.id.avatar_imageview), (ViewGroup) null, 3);
            i2++;
        }
    }

    static /* synthetic */ void m(TiebaCreateActivity tiebaCreateActivity) {
        ao aoVar = new ao(tiebaCreateActivity, tiebaCreateActivity.f);
        aoVar.setTitle("邀请更多人分享");
        aoVar.a(tiebaCreateActivity.getResources().getString(R.string.tie_create_dialoginfo, tiebaCreateActivity.u, Integer.valueOf(tiebaCreateActivity.v)), 0);
        aoVar.a(new dj(tiebaCreateActivity, aoVar));
        aoVar.d();
        tiebaCreateActivity.a(aoVar);
    }

    /* access modifiers changed from: private */
    public void u() {
        this.m.setVisibility(0);
        this.q.setVisibility(0);
        this.r.setVisibility(0);
        v();
    }

    /* access modifiers changed from: private */
    public void v() {
        String string = getResources().getString(R.string.tie_create_support, String.valueOf(this.v));
        if (this.w) {
            this.m.setText(getResources().getString(R.string.tie_create_hassupportinfo, this.u));
            this.n.setText((int) R.string.tie_hascreate_str);
            this.n.setVisibility(0);
            this.q.setText("取消支持");
        } else {
            this.m.setText(getResources().getString(R.string.tie_create_info, this.u));
            this.n.setText((int) R.string.tie_create_str);
            this.n.setVisibility(8);
            this.q.setText("支持");
        }
        this.o.setText(string);
        setTitle(String.valueOf(this.u) + "(待创建)");
    }

    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        super.a(bundle);
        setContentView((int) R.layout.activity_tiebacreate);
        this.s = getIntent().getExtras().getString(k);
        if (a.a((CharSequence) this.s)) {
            finish();
        }
        this.m = (TextView) findViewById(R.id.createtieba_txt_info);
        this.n = (TextView) findViewById(R.id.createtieba_txt_info2);
        this.q = (Button) findViewById(R.id.createtieba_btn_support);
        this.r = (Button) findViewById(R.id.createtieba_btn_share);
        this.o = (TextView) findViewById(R.id.createtieba_tv_membercount);
        this.p = findViewById(R.id.createtieba_view_showmemberlist);
        this.B = new View[8];
        this.B[0] = findViewById(R.id.member_avatar_block0);
        this.B[1] = findViewById(R.id.member_avatar_block1);
        this.B[2] = findViewById(R.id.member_avatar_block2);
        this.B[3] = findViewById(R.id.member_avatar_block3);
        this.B[4] = findViewById(R.id.member_avatar_block4);
        this.B[5] = findViewById(R.id.member_avatar_block5);
        this.B[6] = findViewById(R.id.member_avatar_block6);
        this.B[7] = findViewById(R.id.member_avatar_block7);
        int round = Math.round(((((((float) g.J()) - (g.l().getDimension(R.dimen.pic_item_margin) * 16.0f)) - (g.l().getDimension(R.dimen.style_patterns) * 2.0f)) - (((float) BitmapFactory.decodeResource(getResources(), R.drawable.ic_common_arrow_right).getWidth()) + g.l().getDimension(R.dimen.member_arrow_margin_left))) - (g.l().getDimension(R.dimen.pic_layout_margin) * 2.0f)) / 8.0f);
        for (int i2 = 0; i2 < this.B.length; i2++) {
            ViewGroup.LayoutParams layoutParams = this.B[i2].getLayoutParams();
            layoutParams.height = round;
            layoutParams.width = round;
            this.B[i2].setLayoutParams(layoutParams);
        }
        d();
        this.q.setOnClickListener(new dg(this));
        this.r.setOnClickListener(new dh(this));
        this.p.setOnClickListener(new di(this));
    }

    /* access modifiers changed from: protected */
    public final void d() {
        super.d();
        setTitle("申请创建");
        this.u = getIntent().getExtras().getString(i);
        this.v = getIntent().getExtras().getInt(h, 0);
        this.w = getIntent().getExtras().getBoolean(j, true);
        b(new dk(this, this));
        if (getIntent().getExtras().getBoolean(l, false)) {
            b(new dl(this, this));
        } else {
            u();
        }
    }
}
