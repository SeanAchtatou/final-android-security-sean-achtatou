package com.immomo.momo.android.activity.plugin;

import android.graphics.Bitmap;
import com.immomo.momo.R;

final class bx implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ bw f2078a;
    private final /* synthetic */ Bitmap b;

    bx(bw bwVar, Bitmap bitmap) {
        this.f2078a = bwVar;
        this.b = bitmap;
    }

    public final void run() {
        if (this.b != null) {
            this.f2078a.f2077a.q.setEnabled(true);
            this.f2078a.f2077a.q.setImageBitmap(this.b);
            return;
        }
        this.f2078a.f2077a.q.setEnabled(false);
        this.f2078a.f2077a.q.setImageResource(R.drawable.ic_common_def_header);
    }
}
