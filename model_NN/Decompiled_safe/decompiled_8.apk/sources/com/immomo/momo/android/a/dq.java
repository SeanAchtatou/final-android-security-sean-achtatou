package com.immomo.momo.android.a;

import android.view.View;
import com.immomo.momo.R;

final class dq implements View.OnLongClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ dn f780a;

    dq(dn dnVar) {
        this.f780a = dnVar;
    }

    public final boolean onLongClick(View view) {
        return this.f780a.e.getOnItemLongClickListenerInWrapper().onItemLongClick(this.f780a.e, null, ((Integer) view.getTag(R.id.tag_item_position)).intValue(), (long) view.getId());
    }
}
