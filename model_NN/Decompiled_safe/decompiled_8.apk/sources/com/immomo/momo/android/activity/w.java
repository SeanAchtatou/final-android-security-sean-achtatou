package com.immomo.momo.android.activity;

import android.content.DialogInterface;
import android.support.v4.b.a;
import com.immomo.momo.android.view.EmoteEditeText;

final class w implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ AlipayUserWelcomeActivity f2308a;
    private final /* synthetic */ EmoteEditeText b;

    w(AlipayUserWelcomeActivity alipayUserWelcomeActivity, EmoteEditeText emoteEditeText) {
        this.f2308a = alipayUserWelcomeActivity;
        this.b = emoteEditeText;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        String trim = this.b.getText().toString().trim();
        if (a.a((CharSequence) trim)) {
            this.f2308a.a((CharSequence) "请输入密码");
        } else {
            this.f2308a.a(new aa(this.f2308a, this.f2308a, a.n(trim), this.f2308a.f.h)).execute(new Object[0]);
        }
    }
}
