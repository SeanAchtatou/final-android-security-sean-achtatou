package com.immomo.momo.android.activity;

import android.graphics.Bitmap;
import android.widget.ImageView;
import android.widget.TextView;
import com.immomo.momo.android.c.n;

final class ed implements Runnable {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public eb f1270a;
    /* access modifiers changed from: private */
    public ImageView b;
    /* access modifiers changed from: private */
    public TextView c;
    /* access modifiers changed from: private */
    public /* synthetic */ FilterActivity d;

    public ed(FilterActivity filterActivity, eb ebVar, ImageView imageView, TextView textView) {
        this.d = filterActivity;
        this.f1270a = ebVar;
        this.b = imageView;
        this.c = textView;
    }

    public final void run() {
        ee eeVar = new ee(this);
        Bitmap f = this.d.d;
        String str = this.f1270a.c;
        new Thread(new n(eeVar, f, this.f1270a.f1268a, true)).start();
    }
}
