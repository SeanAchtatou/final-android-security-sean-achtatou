package com.immomo.momo.android.activity.common;

import android.os.Bundle;
import android.os.Handler;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.android.a.c;
import com.immomo.momo.android.activity.lh;
import com.immomo.momo.android.broadcast.d;
import com.immomo.momo.android.broadcast.j;
import com.immomo.momo.android.broadcast.w;
import com.immomo.momo.android.view.FriendRrefreshView;
import com.immomo.momo.android.view.LoadingButton;
import com.immomo.momo.android.view.bl;
import com.immomo.momo.android.view.bu;
import com.immomo.momo.android.view.cv;
import com.immomo.momo.g;
import com.immomo.momo.service.aq;
import com.immomo.momo.service.bean.bf;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class a extends lh implements AdapterView.OnItemClickListener, l, bl, bu, cv {
    /* access modifiers changed from: private */
    public FriendRrefreshView O;
    /* access modifiers changed from: private */
    public c P;
    /* access modifiers changed from: private */
    public LoadingButton Q;
    /* access modifiers changed from: private */
    public List R;
    private j S;
    private w T = null;
    /* access modifiers changed from: private */
    public Date U = null;
    /* access modifiers changed from: private */
    public aq V;
    /* access modifiers changed from: private */
    public h W;
    /* access modifiers changed from: private */
    public i X;
    /* access modifiers changed from: private */
    public Handler Y = new Handler();
    /* access modifiers changed from: private */
    public View Z;
    /* access modifiers changed from: private */
    public EditText aa;
    private TextWatcher ab = new b(this);
    private int ac = 2;
    private d ad = new c(this);

    /* access modifiers changed from: private */
    public void P() {
        this.P = new c(c(), this.R, this.O, ((j) c()).y());
        this.O.setAdapter((ListAdapter) this.P);
        if (this.P.getCount() < 500) {
            this.O.k();
        }
    }

    /* access modifiers changed from: private */
    public List Q() {
        this.R = this.V.c();
        List list = this.R;
        al();
        return this.R;
    }

    /* access modifiers changed from: private */
    public void al() {
        for (Map.Entry value : ((j) c()).E().entrySet()) {
            bf bfVar = (bf) value.getValue();
            if (bfVar != null && this.R.contains(bfVar)) {
                this.R.remove(bfVar);
            }
        }
    }

    static /* synthetic */ j h(a aVar) {
        return (j) aVar.c();
    }

    static /* synthetic */ void q(a aVar) {
        aVar.P = new c(aVar.c(), aVar.R, aVar.O, ((j) aVar.c()).y());
        aVar.O.setAdapter((ListAdapter) aVar.P);
        aVar.O();
    }

    /* access modifiers changed from: protected */
    public final int B() {
        return R.layout.layout_relation_both;
    }

    /* access modifiers changed from: protected */
    public final void C() {
        this.O = (FriendRrefreshView) c((int) R.id.both_listview);
        this.O.setEnableLoadMoreFoolter(true);
        View inflate = g.o().inflate((int) R.layout.listitem_relation_empty, (ViewGroup) null);
        ((TextView) inflate.findViewById(R.id.tv_tip)).setText((int) R.string.bothlist_empty_tip);
        this.O.a(inflate);
        this.O.getSortView().setVisibility(8);
        this.Q = this.O.getFooterViewButton();
        this.Z = this.O.getClearButton();
        this.aa = this.O.getSearchEdit();
    }

    /* access modifiers changed from: protected */
    public final void E() {
        super.E();
        this.O.o();
        if (this.Q.d()) {
            this.Q.e();
        }
        this.aa.getText().clear();
        this.P.a(true);
    }

    public final void O() {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.R.size()) {
                this.P.notifyDataSetChanged();
                return;
            }
            bf bfVar = (bf) this.R.get(i2);
            if (((j) c()).D().containsKey(bfVar.h)) {
                if (!this.P.b(bfVar)) {
                    this.P.a(bfVar);
                }
            } else if (this.P.b(bfVar)) {
                this.P.a(bfVar);
            }
            i = i2 + 1;
        }
    }

    public final void S() {
        super.S();
    }

    public final void ae() {
        super.ae();
    }

    public final void ag() {
        super.ag();
    }

    public final void ai() {
        this.O.i();
    }

    public final void aj() {
        this.O.setLastFlushTime(this.N.b("lasttime_bothlist_success"));
        this.U = this.N.b("lasttime_bothlist_success");
        Q();
        P();
        this.S = new j(c());
        this.S.a(new g(this));
    }

    public final void b_() {
        if (this.W != null && !this.W.isCancelled()) {
            this.W.cancel(true);
        }
        this.W = new h(this, c());
        this.W.execute(new Object[0]);
    }

    /* access modifiers changed from: protected */
    public final void g(Bundle bundle) {
        this.V = new aq();
        this.T = new w(c());
        this.T.a(this.ad);
        try {
            this.ac = ((Integer) this.N.b("sorttype_realtion_both", 2)).intValue();
        } catch (Exception e) {
        }
        if (this.ac != 1) {
            int i = this.ac;
        }
        this.O.setOnPullToRefreshListener$42b903f6(this);
        this.O.setOnCancelListener$135502(this);
        this.O.setOnItemClickListener(this);
        this.Q.setOnProcessListener(this);
        this.Z.setOnClickListener(new f(this));
        this.aa.addTextChangedListener(this.ab);
        aj();
    }

    public final void m() {
        if (this.W == null || !this.W.isCancelled()) {
            if (this.U != null && new Date().getTime() - this.U.getTime() > 900000) {
                this.O.b();
            } else if (this.R.size() <= 0) {
                this.O.b();
            }
        }
        super.m();
    }

    public void onItemClick(AdapterView adapterView, View view, int i, long j) {
        if (((j) c()).y()) {
            ((j) c()).a(this.P.getItem(i).h, 0);
        } else if (this.P.b(this.P.getItem(i)) || ((j) c()).D().size() + 1 <= ((j) c()).z()) {
            if (this.P.a(this.P.getItem(i))) {
                ((j) c()).a(this.P.getItem(i));
            } else {
                ((j) c()).b(this.P.getItem(i));
            }
            this.P.notifyDataSetChanged();
            ((j) c()).a(((j) c()).D().size(), ((j) c()).z());
        } else {
            a(((j) c()).A());
        }
    }

    public final void p() {
        super.p();
        if (this.S != null) {
            a(this.S);
            this.S = null;
        }
        if (this.T != null) {
            a(this.T);
            this.T = null;
        }
        if (this.W != null && !this.W.isCancelled()) {
            this.W.cancel(true);
            this.W = null;
        }
        if (this.X != null && !this.X.isCancelled()) {
            this.X.cancel(true);
            this.X = null;
        }
    }

    public final void u() {
        this.Q.f();
        this.X = new i(this, c());
        this.X.execute(new Object[0]);
    }

    public final void v() {
        this.U = new Date();
        this.N.a("lasttime_bothlist", this.U);
        this.O.n();
        this.Q.e();
        if (this.W != null && !this.W.isCancelled()) {
            this.W.cancel(true);
        }
    }
}
