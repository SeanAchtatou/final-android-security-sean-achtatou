package com.immomo.momo.android.activity.group;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.plugin.g.a;
import com.immomo.momo.protocol.a.n;
import java.io.File;

final class eb extends d {

    /* renamed from: a  reason: collision with root package name */
    private v f1625a = null;
    private boolean c = false;
    private boolean d = false;
    private boolean e = false;
    private boolean f = false;
    private String g;
    private File h;
    /* access modifiers changed from: private */
    public /* synthetic */ GroupProfileActivity i;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public eb(GroupProfileActivity groupProfileActivity, Context context, boolean z, boolean z2, boolean z3, boolean z4, String str, File file) {
        super(context);
        this.i = groupProfileActivity;
        this.c = z;
        this.d = z2;
        this.e = z3;
        this.f = z4;
        this.g = str;
        this.h = file;
        this.f1625a = new v(context);
        this.f1625a.a("请求提交中");
        this.f1625a.setCancelable(true);
        this.f1625a.setOnCancelListener(new ec(this));
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        return n.a().a(this.i.l, this.c, this.d, this.e, this.f);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.b.a((Object) "ShareWeiboTask~~~~~~~~~~~~~~");
        if (this.f1625a != null) {
            this.f1625a.show();
        }
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        super.a(exc);
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void a(Object obj) {
        String str = (String) obj;
        if (this.f) {
            a.a().a(str, this.g, this.h);
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
        if (this.f1625a != null) {
            this.f1625a.dismiss();
            this.f1625a = null;
        }
    }
}
