package com.immomo.momo.android.activity.retrieve;

import android.os.Bundle;
import android.support.v4.b.a;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.ao;
import com.immomo.momo.android.broadcast.y;
import com.immomo.momo.android.view.HeaderLayout;
import com.immomo.momo.g;
import com.immomo.momo.util.k;

public class ResetPswByPhoneActivity extends ao implements View.OnClickListener {
    private HeaderLayout h = null;
    private TextView i = null;
    private TextView j = null;
    /* access modifiers changed from: private */
    public EditText k = null;
    /* access modifiers changed from: private */
    public EditText l = null;
    private EditText m = null;
    /* access modifiers changed from: private */
    public String n = PoiTypeDef.All;
    /* access modifiers changed from: private */
    public String o = PoiTypeDef.All;
    /* access modifiers changed from: private */
    public String p = PoiTypeDef.All;
    private String q = PoiTypeDef.All;
    private String r = PoiTypeDef.All;
    private y s = null;

    private static boolean a(EditText editText) {
        String trim = editText.getText().toString().trim();
        return trim == null || trim.length() <= 0;
    }

    /* access modifiers changed from: protected */
    public final void d() {
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0037, code lost:
        if (r2 != false) goto L_0x0039;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onClick(android.view.View r6) {
        /*
            r5 = this;
            r0 = 1
            r1 = 0
            int r2 = r6.getId()
            switch(r2) {
                case 2131165286: goto L_0x000a;
                case 2131165289: goto L_0x000e;
                case 2131165846: goto L_0x010d;
                default: goto L_0x0009;
            }
        L_0x0009:
            return
        L_0x000a:
            r5.finish()
            goto L_0x0009
        L_0x000e:
            android.widget.EditText r2 = r5.k
            boolean r2 = a(r2)
            if (r2 == 0) goto L_0x0044
            java.lang.String r2 = "请输入验证码"
            com.immomo.momo.util.ao.b(r2)
            android.widget.EditText r2 = r5.k
            r2.requestFocus()
            r2 = r1
        L_0x0021:
            if (r2 == 0) goto L_0x010a
            android.widget.EditText r2 = r5.l
            boolean r2 = a(r2)
            if (r2 == 0) goto L_0x0052
            r2 = 2131493103(0x7f0c00ef, float:1.8609677E38)
            com.immomo.momo.util.ao.e(r2)
            android.widget.EditText r2 = r5.l
            r2.requestFocus()
            r2 = r1
        L_0x0037:
            if (r2 == 0) goto L_0x010a
        L_0x0039:
            if (r0 == 0) goto L_0x0009
            com.immomo.momo.android.activity.retrieve.p r0 = new com.immomo.momo.android.activity.retrieve.p
            r0.<init>(r5, r5)
            r5.b(r0)
            goto L_0x0009
        L_0x0044:
            android.widget.EditText r2 = r5.k
            android.text.Editable r2 = r2.getText()
            java.lang.String r2 = r2.toString()
            r5.p = r2
            r2 = r0
            goto L_0x0021
        L_0x0052:
            android.widget.EditText r2 = r5.l
            android.text.Editable r2 = r2.getText()
            java.lang.String r2 = r2.toString()
            java.lang.String r2 = r2.trim()
            int r3 = r2.length()
            r4 = 6
            if (r3 >= r4) goto L_0x0087
            android.widget.EditText r2 = r5.l
            r2.requestFocus()
            android.widget.EditText r2 = r5.l
            r2.selectAll()
            r2 = 2131493111(0x7f0c00f7, float:1.8609693E38)
            java.lang.String r2 = com.immomo.momo.g.a(r2)
            java.lang.Object[] r3 = new java.lang.Object[r0]
            java.lang.String r4 = "6"
            r3[r1] = r4
            java.lang.String r2 = java.lang.String.format(r2, r3)
            com.immomo.momo.util.ao.b(r2)
            r2 = r1
            goto L_0x0037
        L_0x0087:
            int r3 = r2.length()
            r4 = 16
            if (r3 <= r4) goto L_0x00af
            android.widget.EditText r2 = r5.l
            r2.requestFocus()
            android.widget.EditText r2 = r5.l
            r2.selectAll()
            r2 = 2131493112(0x7f0c00f8, float:1.8609695E38)
            java.lang.String r2 = com.immomo.momo.g.a(r2)
            java.lang.Object[] r3 = new java.lang.Object[r0]
            java.lang.String r4 = "16"
            r3[r1] = r4
            java.lang.String r2 = java.lang.String.format(r2, r3)
            com.immomo.momo.util.ao.b(r2)
            r2 = r1
            goto L_0x0037
        L_0x00af:
            android.widget.EditText r3 = r5.m
            boolean r3 = a(r3)
            if (r3 == 0) goto L_0x00c5
            r2 = 2131493104(0x7f0c00f0, float:1.8609679E38)
            com.immomo.momo.util.ao.e(r2)
            android.widget.EditText r2 = r5.m
            r2.requestFocus()
            r2 = r1
            goto L_0x0037
        L_0x00c5:
            android.widget.EditText r3 = r5.m
            android.text.Editable r3 = r3.getText()
            java.lang.String r3 = r3.toString()
            java.lang.String r3 = r3.trim()
            if (r3 == 0) goto L_0x00db
            boolean r3 = r3.equals(r2)
            if (r3 != 0) goto L_0x00ee
        L_0x00db:
            r2 = 2131493105(0x7f0c00f1, float:1.860968E38)
            com.immomo.momo.util.ao.e(r2)
            android.widget.EditText r2 = r5.m
            r2.requestFocus()
            android.widget.EditText r2 = r5.m
            r2.selectAll()
            r2 = r1
            goto L_0x0037
        L_0x00ee:
            boolean r2 = android.support.v4.b.a.g(r2)
            if (r2 == 0) goto L_0x0107
            r2 = 2131493113(0x7f0c00f9, float:1.8609697E38)
            com.immomo.momo.util.ao.e(r2)
            android.widget.EditText r2 = r5.l
            r2.requestFocus()
            android.widget.EditText r2 = r5.l
            r2.selectAll()
            r2 = r1
            goto L_0x0037
        L_0x0107:
            r2 = r0
            goto L_0x0037
        L_0x010a:
            r0 = r1
            goto L_0x0039
        L_0x010d:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r1 = "smsto:"
            r0.<init>(r1)
            java.lang.String r1 = r5.r
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            android.net.Uri r0 = android.net.Uri.parse(r0)
            android.content.Intent r1 = new android.content.Intent
            java.lang.String r2 = "android.intent.action.SENDTO"
            r1.<init>(r2, r0)
            java.lang.String r0 = "sms_body"
            java.lang.String r2 = r5.q
            r1.putExtra(r0, r2)
            r5.startActivity(r1)     // Catch:{ Exception -> 0x0135 }
            goto L_0x0009
        L_0x0135:
            r0 = move-exception
            java.lang.String r0 = "该设备不支持短信息功能"
            r5.a(r0)
            goto L_0x0009
        */
        throw new UnsupportedOperationException("Method not decompiled: com.immomo.momo.android.activity.retrieve.ResetPswByPhoneActivity.onClick(android.view.View):void");
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_resetpwd_phone);
        this.h = (HeaderLayout) findViewById(R.id.layout_header);
        this.h.setTitleText("密码重置");
        this.k = (EditText) findViewById(R.id.rg_et_verifycode);
        this.i = (TextView) findViewById(R.id.rg_tv_phonenumber);
        this.j = (TextView) findViewById(R.id.rg_tv_info);
        this.l = (EditText) findViewById(R.id.rg_et_pwd);
        this.m = (EditText) findViewById(R.id.rg_et_pwd_confim);
        this.m.setOnEditorActionListener(new n(this));
        findViewById(R.id.btn_ok).setOnClickListener(this);
        findViewById(R.id.btn_back).setOnClickListener(this);
        findViewById(R.id.rg_bt_sendmsg).setOnClickListener(this);
        this.n = getIntent().getStringExtra("areacode");
        this.o = getIntent().getStringExtra("phonenumber");
        String c = a.c(this.o);
        this.q = getIntent().getStringExtra("code");
        this.r = getIntent().getStringExtra("interfacenumber");
        if (a.f(this.q) && a.f(this.r)) {
            String str = "使用已绑定的手机号" + c + "\n编辑短信内容" + this.q + "发送到" + this.r + "\n收到短信验证码后，继续以下操作\n此操作仅扣取正常短信费用。";
            TextView textView = this.j;
            String str2 = this.q;
            String str3 = this.r;
            int indexOf = str.indexOf(c);
            int indexOf2 = str.indexOf(str3);
            int indexOf3 = str.indexOf(str2);
            SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(str);
            spannableStringBuilder.setSpan(new ForegroundColorSpan(g.c((int) R.color.blue)), indexOf, c.length() + indexOf, 33);
            spannableStringBuilder.setSpan(new ForegroundColorSpan(g.c((int) R.color.blue)), indexOf2, str3.length() + indexOf2, 33);
            spannableStringBuilder.setSpan(new ForegroundColorSpan(g.c((int) R.color.blue)), indexOf3, str2.length() + indexOf3, 33);
            textView.setText(spannableStringBuilder);
            this.i.setVisibility(8);
        } else if (!"+86".equals(this.n)) {
            this.j.setText("包含验证码的短信已发送到");
            TextView textView2 = this.i;
            String str4 = "(" + this.n + ")" + c;
            String str5 = "(" + this.n + ")" + this.o;
            int indexOf4 = str5.indexOf(str4);
            SpannableStringBuilder spannableStringBuilder2 = new SpannableStringBuilder(str5);
            spannableStringBuilder2.setSpan(new ForegroundColorSpan(g.c((int) R.color.blue)), indexOf4, str4.length() + indexOf4, 33);
            textView2.setText(spannableStringBuilder2);
            findViewById(R.id.rg_bt_sendmsg).setVisibility(8);
            ((TextView) findViewById(R.id.rg_tv_title1)).setText("第一步：获取验证码");
        } else {
            finish();
        }
        this.s = new y(this);
        this.s.a(new o(this));
        this.e.a((Object) "onCreate...");
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.s != null) {
            unregisterReceiver(this.s);
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        new k("PO", "P133");
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        new k("PI", "P133");
    }
}
