package com.immomo.momo.android.activity;

import android.content.Intent;
import android.os.AsyncTask;
import com.immomo.momo.R;
import com.immomo.momo.a.e;
import com.immomo.momo.a.g;
import com.immomo.momo.a.h;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.w;
import com.immomo.momo.service.ai;
import com.immomo.momo.service.aq;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.service.y;
import java.io.InterruptedIOException;
import java.util.List;

final class gg extends AsyncTask {

    /* renamed from: a  reason: collision with root package name */
    private v f1514a = null;
    private /* synthetic */ OldAddFriendActivity b;

    public gg(OldAddFriendActivity oldAddFriendActivity) {
        this.b = oldAddFriendActivity;
        this.f1514a = new v(oldAddFriendActivity.c());
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public String doInBackground(String... strArr) {
        try {
            this.b.O = new bf(strArr[0]);
            w.a().a(this.b.O, (List) null);
            return "yes";
        } catch (com.immomo.momo.a.w e) {
            this.b.L.a((Throwable) e);
            this.b.e((int) R.string.errormsg_network_unfind);
            return "error";
        } catch (InterruptedIOException e2) {
            this.b.L.a((Throwable) e2);
            this.b.e((int) R.string.errormsg_network_timeout);
            return "error";
        } catch (e e3) {
            this.b.L.a((Throwable) e3);
            this.b.e((int) R.string.errormsg_network_normal400);
            return "error";
        } catch (g e4) {
            this.b.L.a((Throwable) e4);
            this.b.e((int) R.string.errormsg_network_normal403);
            return "error";
        } catch (h e5) {
            this.b.L.a((Throwable) e5);
            this.b.e((int) R.string.find_no_momoid);
            return "error";
        } catch (Exception e6) {
            this.b.e((int) R.string.errormsg_server);
            this.b.L.a((Throwable) e6);
            return "error";
        }
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void onPostExecute(Object obj) {
        String str = (String) obj;
        if (!this.b.c().isFinishing()) {
            this.f1514a.dismiss();
        }
        if (str.equals("yes")) {
            new y().d(this.b.O.h);
            new aq().b(this.b.O);
            if (this.b.O.ah != null) {
                new ai().a(this.b.O.ah);
            }
            Intent intent = new Intent(com.immomo.momo.android.broadcast.w.f2366a);
            intent.putExtra("momoid", this.b.O.h);
            OldAddFriendActivity oldAddFriendActivity = this.b;
            OldAddFriendActivity.b(intent);
            Intent intent2 = new Intent(this.b.c(), OtherProfileActivity.class);
            intent2.putExtra("momoid", this.b.O.h);
            intent2.putExtra("tag", "notreflsh");
            this.b.a(intent2);
        } else {
            str.equals("error");
        }
        this.b.R = (AsyncTask) null;
    }

    /* access modifiers changed from: protected */
    public final void onPreExecute() {
        super.onPreExecute();
        this.f1514a.setCancelable(true);
        this.f1514a.a("正在查找,请稍候...");
        this.f1514a.setOnCancelListener(new gh(this));
        this.f1514a.show();
    }
}
