package com.immomo.momo.android.activity.event;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.protocol.a.j;
import com.immomo.momo.service.bean.u;
import java.util.ArrayList;
import java.util.List;

final class cl extends d {

    /* renamed from: a  reason: collision with root package name */
    private List f1405a = new ArrayList();
    private /* synthetic */ UsersEventListActivity c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public cl(UsersEventListActivity usersEventListActivity, Context context) {
        super(context);
        this.c = usersEventListActivity;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        return Boolean.valueOf(j.a().a(this.f1405a, this.c.l.getCount()));
    }

    /* access modifiers changed from: protected */
    public final void a() {
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        if (((Boolean) obj).booleanValue()) {
            this.c.m.setVisibility(0);
        } else {
            this.c.m.setVisibility(8);
        }
        for (u uVar : this.f1405a) {
            if (!this.c.p.contains(uVar)) {
                this.c.l.b(uVar);
                this.c.p.add(uVar);
            }
        }
        this.c.l.notifyDataSetChanged();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.c.m.e();
    }
}
