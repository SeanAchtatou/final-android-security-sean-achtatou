package com.immomo.momo.android.activity.feed;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.protocol.a.a.c;
import com.immomo.momo.protocol.a.k;
import java.util.Collection;

final class t extends d {

    /* renamed from: a  reason: collision with root package name */
    private boolean f1485a = false;
    private /* synthetic */ FeedProfileActivity c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public t(FeedProfileActivity feedProfileActivity, Context context, boolean z) {
        super(context);
        this.c = feedProfileActivity;
        this.f1485a = z;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        c a2;
        if (this.f1485a) {
            a2 = k.a().e(this.c.j);
            this.c.m.d(this.c.j);
        } else {
            a2 = k.a().a(this.c.j, this.c.l.getCount(), FeedProfileActivity.d(this.c));
        }
        this.c.m.d(a2.c);
        return a2;
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        FeedProfileActivity.a(this.c, this.c.k.g);
        super.a(exc);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        c cVar = (c) obj;
        if (this.f1485a) {
            this.c.l.a((Collection) cVar.c);
        } else {
            this.c.l.b((Collection) cVar.c);
        }
        FeedProfileActivity.a(this.c, cVar.b);
        if (cVar.f2882a > 0) {
            this.c.t.setVisibility(0);
        } else {
            this.c.r.removeFooterView(this.c.t);
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.c.s.e();
        super.b();
    }
}
