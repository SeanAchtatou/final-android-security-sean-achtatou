package com.immomo.momo.android.activity;

import android.content.Context;
import com.immomo.momo.android.c.d;
import java.util.concurrent.atomic.AtomicBoolean;

final class bs extends d {

    /* renamed from: a  reason: collision with root package name */
    private StringBuilder f1051a = new StringBuilder();
    private /* synthetic */ CheckStatusActivity c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public bs(CheckStatusActivity checkStatusActivity, Context context) {
        super(context);
        this.c = checkStatusActivity;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        AtomicBoolean atomicBoolean = new AtomicBoolean();
        com.immomo.momo.protocol.imjson.d.a(this.f1051a, atomicBoolean);
        return Boolean.valueOf(atomicBoolean.get());
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        this.c.k.setText("通讯服务器连接失败");
        this.c.m.setVisibility(0);
        this.c.o = false;
        this.b.a((Throwable) exc);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        this.c.o = ((Boolean) obj).booleanValue();
        this.c.k.setText(this.c.o ? "通讯服务器连接成功" : "通讯服务器连接失败");
        this.c.m.setVisibility(this.c.o ? 4 : 0);
    }
}
