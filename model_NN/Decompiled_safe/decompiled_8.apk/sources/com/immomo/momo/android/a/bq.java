package com.immomo.momo.android.a;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.view.HandyListView;
import com.immomo.momo.service.bean.q;
import com.immomo.momo.util.j;
import com.immomo.momo.util.m;
import java.util.List;

public final class bq extends a {
    private HandyListView d = null;

    public bq(Context context, List list, HandyListView handyListView) {
        super(context, list);
        new m(this);
        this.d = handyListView;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        br brVar;
        if (view == null) {
            view = a((int) R.layout.listitem_emotion);
            brVar = new br((byte) 0);
            view.setTag(brVar);
            brVar.c = (ImageView) view.findViewById(R.id.emotionitem_iv_bag);
            brVar.d = (ImageView) view.findViewById(R.id.emotionitem_iv_cover);
            brVar.f742a = (TextView) view.findViewById(R.id.emotionitem_tv_name);
            brVar.b = (TextView) view.findViewById(R.id.emotionitem_tv_name_flag);
            brVar.e = (TextView) view.findViewById(R.id.emotionitem_tv_lable);
            brVar.f = (TextView) view.findViewById(R.id.emotionitem_tv_price_first);
            brVar.g = (TextView) view.findViewById(R.id.emotionitem_tv_price_second);
        } else {
            brVar = (br) view.getTag();
        }
        q qVar = (q) getItem(i);
        brVar.f742a.setText(qVar.b);
        j.b(qVar.b(), brVar.d, this.d, 18, true);
        brVar.f.setText(qVar.l);
        if (qVar.i == 3 || qVar.i == 4) {
            brVar.g.setText(qVar.m);
            brVar.g.setVisibility(0);
            brVar.g.getPaint().setFlags(16);
        } else {
            brVar.g.setVisibility(8);
        }
        if (qVar.q) {
            brVar.c.setImageResource(R.drawable.bg_eshop_listitem_have2);
        } else {
            brVar.c.setImageResource(0);
        }
        if (qVar.i != 0) {
            brVar.e.setText(qVar.j);
            brVar.e.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        }
        if (qVar.i == 1) {
            brVar.e.setBackgroundResource(R.drawable.round_eshop_lable1);
        } else if (qVar.i == 2) {
            brVar.e.setBackgroundResource(R.drawable.round_eshop_lable2);
        } else if (qVar.i == 3) {
            brVar.e.setBackgroundResource(R.drawable.round_eshop_lable3);
        } else if (qVar.i == 4) {
            brVar.e.setBackgroundResource(R.drawable.round_eshop_lable4);
        } else {
            brVar.e.setText(PoiTypeDef.All);
            brVar.e.setBackgroundResource(0);
            brVar.e.setCompoundDrawablesWithIntrinsicBounds(0, 0, (int) R.drawable.ic_common_arrow_right, 0);
        }
        if (qVar.c != 0) {
            brVar.b.setText(qVar.d);
            brVar.b.setVisibility(0);
        }
        if (qVar.c == 1) {
            brVar.b.setBackgroundResource(R.drawable.round_eshop_lable1);
        } else if (qVar.c == 2) {
            brVar.b.setBackgroundResource(R.drawable.round_eshop_lable2);
        } else if (qVar.c == 3) {
            brVar.b.setBackgroundResource(R.drawable.round_eshop_lable3);
        } else if (qVar.c == 4) {
            brVar.b.setBackgroundResource(R.drawable.round_eshop_lable4);
        } else {
            brVar.b.setVisibility(8);
        }
        return view;
    }
}
