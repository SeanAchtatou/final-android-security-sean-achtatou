package com.immomo.momo.android.c;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import com.immomo.momo.android.activity.NewVersionActivity;
import com.immomo.momo.android.service.Initializer;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.b;
import com.immomo.momo.g;
import com.immomo.momo.protocol.a.d;
import com.immomo.momo.service.ad;
import com.immomo.momo.service.an;
import com.immomo.momo.service.bean.ar;
import com.immomo.momo.util.ak;
import com.immomo.momo.util.ao;
import com.immomo.momo.util.m;

public final class h extends AsyncTask {

    /* renamed from: a  reason: collision with root package name */
    private v f2383a;
    private Context b;
    private m c;
    private boolean d = false;

    public h(Context context, boolean z) {
        this.b = context;
        this.d = z;
        this.c = new m("CheckNewVersionTask");
        this.f2383a = new v(this.b);
        new an();
        this.f2383a.setCancelable(true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.ak.a(java.lang.String, java.lang.Integer):int
     arg types: [java.lang.String, int]
     candidates:
      com.immomo.momo.util.ak.a(android.content.Context, java.lang.String):com.immomo.momo.util.ak
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Long):long
      com.immomo.momo.util.ak.a(java.lang.String, int):void
      com.immomo.momo.util.ak.a(java.lang.String, long):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.String):void
      com.immomo.momo.util.ak.a(java.lang.String, boolean):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Boolean):boolean
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Integer):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.Long]
     candidates:
      com.immomo.momo.util.ak.a(android.content.Context, java.lang.String):com.immomo.momo.util.ak
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Integer):int
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Long):long
      com.immomo.momo.util.ak.a(java.lang.String, int):void
      com.immomo.momo.util.ak.a(java.lang.String, long):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.String):void
      com.immomo.momo.util.ak.a(java.lang.String, boolean):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Boolean):boolean
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.immomo.momo.util.ak.a(android.content.Context, java.lang.String):com.immomo.momo.util.ak
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Integer):int
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Long):long
      com.immomo.momo.util.ak.a(java.lang.String, int):void
      com.immomo.momo.util.ak.a(java.lang.String, long):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.String):void
      com.immomo.momo.util.ak.a(java.lang.String, boolean):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Boolean):boolean
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.ak.a(java.lang.String, java.lang.Long):long
     arg types: [java.lang.String, long]
     candidates:
      com.immomo.momo.util.ak.a(android.content.Context, java.lang.String):com.immomo.momo.util.ak
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Integer):int
      com.immomo.momo.util.ak.a(java.lang.String, int):void
      com.immomo.momo.util.ak.a(java.lang.String, long):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.String):void
      com.immomo.momo.util.ak.a(java.lang.String, boolean):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Boolean):boolean
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Long):long */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.Integer]
     candidates:
      com.immomo.momo.util.ak.a(android.content.Context, java.lang.String):com.immomo.momo.util.ak
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Integer):int
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Long):long
      com.immomo.momo.util.ak.a(java.lang.String, int):void
      com.immomo.momo.util.ak.a(java.lang.String, long):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.String):void
      com.immomo.momo.util.ak.a(java.lang.String, boolean):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Boolean):boolean
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.m.a(java.lang.String, java.lang.Throwable):void
     arg types: [java.lang.String, java.lang.Exception]
     candidates:
      com.immomo.momo.util.m.a(java.lang.Appendable, java.lang.Throwable):void
      com.immomo.momo.util.m.a(java.lang.Object, java.lang.Throwable):void
      com.immomo.momo.util.m.a(java.lang.String, java.io.File):void
      com.immomo.momo.util.m.a(java.lang.String, java.lang.Throwable):void */
    private ar a() {
        try {
            int a2 = g.d().f668a.a("industry_version_count", (Integer) 0);
            this.c.a((Object) ("industry version: " + a2));
            ar a3 = d.a(a2);
            if (a3 == null) {
                return a3;
            }
            ak akVar = g.d().f668a;
            akVar.a("momo_deny_count", (Object) Long.valueOf(a3.f));
            akVar.a("momo_report_count", (Object) Long.valueOf(a3.g));
            akVar.a("momo_deny_info", (Object) a3.h);
            this.c.a((Object) ("momo_deny_count: " + akVar.a("momo_deny_count", (Long) 0L)));
            this.c.a((Object) ("momo_report_count: " + akVar.a("momo_report_count", (Long) 0L)));
            if (a3.e != null) {
                new ad().a(a3.e);
                akVar.a("industry_version_count", (Object) Integer.valueOf(a3.d));
                b.a();
                Intent intent = new Intent(this.b, Initializer.class);
                intent.putExtra("init_param", "param_icon");
                this.b.startService(intent);
            }
            if (g.z() < a3.c) {
                this.c.a((Object) ("getClass().getName()=" + this.b.getClass().getName()));
                Intent intent2 = new Intent(this.b, NewVersionActivity.class);
                intent2.putExtra("url_download", a3.b);
                intent2.putExtra("version_name", a3.f2994a);
                intent2.addFlags(268435456);
                this.b.getApplicationContext().startActivity(intent2);
                return a3;
            } else if (!this.d) {
                return a3;
            } else {
                ao.a((CharSequence) "当前已是最新版");
                return a3;
            }
        } catch (Exception e) {
            this.c.a("CheckNewVersionTask", (Throwable) e);
            if (this.d) {
                ao.a((CharSequence) e.getMessage());
                if (this.f2383a != null) {
                    this.f2383a.cancel();
                }
            }
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object doInBackground(Object... objArr) {
        return a();
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void onPostExecute(Object obj) {
        if ((this.f2383a != null && this.d) || !(this.b instanceof Activity) || !((Activity) this.b).isFinishing()) {
            this.f2383a.dismiss();
        }
    }

    /* access modifiers changed from: protected */
    public final void onPreExecute() {
        if (this.f2383a != null && this.d) {
            this.f2383a.a("请求提交中");
            this.f2383a.show();
        }
        super.onPreExecute();
    }
}
