package com.immomo.momo.android.view;

import android.view.GestureDetector;
import android.view.MotionEvent;

final class bc extends GestureDetector.SimpleOnGestureListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ az f2737a;

    private bc(az azVar) {
        this.f2737a = azVar;
    }

    /* synthetic */ bc(az azVar, byte b) {
        this(azVar);
    }

    public final boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
        this.f2737a.m = f2;
        return false;
    }
}
