package com.immomo.momo.android.activity.message;

import android.content.Intent;
import android.view.View;
import com.immomo.momo.android.activity.group.GroupFeedsActivity;

final class bn implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ GroupChatActivity f1947a;

    bn(GroupChatActivity groupChatActivity) {
        this.f1947a = groupChatActivity;
    }

    public final void onClick(View view) {
        Intent intent = new Intent(this.f1947a, GroupFeedsActivity.class);
        intent.putExtra("gid", this.f1947a.u());
        this.f1947a.startActivity(intent);
        this.f1947a.Q.u = false;
        this.f1947a.ag();
    }
}
