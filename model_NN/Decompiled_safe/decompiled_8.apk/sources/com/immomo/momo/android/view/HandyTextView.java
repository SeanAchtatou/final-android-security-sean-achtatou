package com.immomo.momo.android.view;

import android.content.Context;
import android.graphics.Canvas;
import android.support.v4.b.a;
import android.text.SpannableStringBuilder;
import android.util.AttributeSet;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.plugin.b.e;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HandyTextView extends TextView {
    public HandyTextView(Context context) {
        super(context, null);
    }

    public HandyTextView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public HandyTextView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    /* access modifiers changed from: protected */
    public CharSequence b(CharSequence charSequence) {
        boolean z = false;
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(charSequence);
        Matcher matcher = Pattern.compile("(\\[[.[^\\[\\]]]*?\\|et\\|([.[^\\[\\]]]*?\\|)*[.[^\\[\\]]]*?\\])").matcher(charSequence);
        while (matcher.find()) {
            z = true;
            getContext();
            spannableStringBuilder.setSpan(new e(matcher.group()), matcher.start(), matcher.end(), 33);
        }
        return z ? spannableStringBuilder : charSequence;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        try {
            super.onDraw(canvas);
        } catch (Throwable th) {
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        try {
            super.onMeasure(i, i2);
        } catch (Exception e) {
            setText(a.p(getText().toString()));
            super.onMeasure(i, i2);
        }
    }

    public void setText(CharSequence charSequence, TextView.BufferType bufferType) {
        if (charSequence == null) {
            charSequence = PoiTypeDef.All;
        }
        try {
            super.setText(charSequence, bufferType);
        } catch (Exception e) {
        }
    }
}
