package com.immomo.momo.android.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.immomo.momo.R;
import com.immomo.momo.g;

public class PointLayout extends LinearLayout {

    /* renamed from: a  reason: collision with root package name */
    private int f2644a = 10;
    private int b = 0;
    private int c = R.drawable.ic_welcome_steppoint_white;
    private int d = R.drawable.ic_welcome_steppoint_gray;
    private ImageView[] e;

    public PointLayout(Context context) {
        super(context);
        setOrientation(0);
    }

    public PointLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        setOrientation(0);
    }

    public final void a() {
        this.c = R.drawable.index_step_arrow;
    }

    public void setCurentPoint(int i) {
        if (i - 1 <= this.b) {
            for (int i2 = 0; i2 < this.b; i2++) {
                if (i2 == i - 1) {
                    this.e[i2].setImageResource(this.c);
                } else {
                    this.e[i2].setImageResource(this.d);
                }
            }
        }
    }

    public void setPointCount(int i) {
        if (i > 0 && this.b != i) {
            removeAllViews();
            this.e = new ImageView[i];
            for (int i2 = 0; i2 < i; i2++) {
                ImageView[] imageViewArr = this.e;
                int i3 = this.d;
                ImageView imageView = new ImageView(getContext());
                imageView.setLayoutParams(new LinearLayout.LayoutParams(-2, -2));
                imageView.setImageResource(i3);
                imageViewArr[i2] = imageView;
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-2, -2);
                layoutParams.rightMargin = (int) (((float) this.f2644a) * g.k());
                addView(this.e[i2], layoutParams);
            }
            this.b = i;
        }
    }

    public void setPointPading(int i) {
        if (i >= 0) {
            this.f2644a = i;
        }
    }
}
