package com.immomo.momo.android.game;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.Button;
import android.widget.TextView;
import com.immomo.a.a.f.a;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.lh;
import mm.purchasesdk.Purchase;

@SuppressLint({"ValidFragment"})
public final class y extends lh {
    ai O;
    String P;
    String Q;
    al R = null;
    Purchase S;
    ab T;
    private TextView U;
    private TextView V;
    private TextView W;
    private TextView X;
    private TextView Y;
    private Button Z;

    public y(ai aiVar, String str, String str2) {
        this.O = aiVar;
        this.P = str;
        this.Q = str2;
        this.R = aiVar.d;
        new Handler();
        this.S = Purchase.getInstance();
        c();
        this.T = new ab(this);
    }

    /* access modifiers changed from: private */
    public void O() {
        this.L.a((Object) "paying");
        this.S.order(c(), this.R.f2408a, this.R.b, this.R.h, this.T);
        this.L.a((Object) "paying success");
    }

    static /* synthetic */ void c(y yVar) {
        Intent intent = new Intent();
        intent.putExtra("product_id", yVar.O.i);
        intent.putExtra("trade_number", yVar.R.h);
        intent.putExtra("trade_sign", yVar.R.g);
        if (!a.a(yVar.O.j)) {
            intent.putExtra("trade_extendnumber", yVar.O.j);
        }
        yVar.c().setResult(30210, intent);
        yVar.c().finish();
    }

    static /* synthetic */ void d(y yVar) {
        if (!yVar.R.i) {
            yVar.a(new aa(yVar, yVar.c()));
        } else {
            yVar.O();
        }
    }

    /* access modifiers changed from: protected */
    public final int B() {
        return R.layout.fragment_mdkpay;
    }

    /* access modifiers changed from: protected */
    public final void C() {
        this.U = (TextView) c((int) R.id.opentrade_tv_fee);
        this.V = (TextView) c((int) R.id.opentrade_tv_product);
        this.W = (TextView) c((int) R.id.opentrade_tv_username);
        this.X = (TextView) c((int) R.id.opentrade_tv_momoid);
        this.Z = (Button) c((int) R.id.opentrade_btn_confim);
        this.Y = (TextView) c((int) R.id.opentrade_tv_desc);
        this.Z.setText("付款");
        this.Y.setText("适用于中国移动的手机用户");
    }

    public final boolean F() {
        return false;
    }

    public final void aj() {
        this.U.setText("支付金额: " + android.support.v4.b.a.a(this.R.e) + "元");
        this.V.setText("商品: " + this.O.h);
        if (this.O.g != null) {
            this.W.setText("用户: " + this.O.g.h());
            this.X.setText("陌陌号: " + this.O.g.h);
            return;
        }
        this.W.setText("用户: ");
        this.X.setText("陌陌号: ");
    }

    public final void g(Bundle bundle) {
        this.Z.setOnClickListener(new z(this));
        aj();
    }

    public final void p() {
        super.p();
    }
}
