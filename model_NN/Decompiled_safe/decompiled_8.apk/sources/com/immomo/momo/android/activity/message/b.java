package com.immomo.momo.android.activity.message;

import com.immomo.momo.android.a.a.ab;
import com.immomo.momo.plugin.audio.e;
import com.immomo.momo.service.bean.Message;

final class b implements e {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ a f1933a;

    b(a aVar) {
        this.f1933a = aVar;
    }

    public final void a() {
        this.f1933a.v().a();
    }

    public final void a(int i) {
        if (i == 0) {
            this.f1933a.j.sendEmptyMessage(10019);
        } else {
            this.f1933a.j.sendEmptyMessage(10020);
        }
    }

    public final void a(Message message) {
        ab.j.remove(message.msgId);
        this.f1933a.Y();
        if (!this.f1933a.a(message)) {
            this.f1933a.ai();
        }
    }

    public final void b() {
        this.f1933a.Y();
    }

    public final void c() {
        this.f1933a.ai();
        this.f1933a.Y();
    }
}
