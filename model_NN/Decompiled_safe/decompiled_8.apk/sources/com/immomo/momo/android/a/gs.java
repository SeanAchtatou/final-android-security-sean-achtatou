package com.immomo.momo.android.a;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.a.a;
import com.immomo.momo.service.bean.ay;
import com.immomo.momo.util.j;
import com.immomo.momo.util.m;
import java.util.HashMap;
import java.util.List;

public final class gs extends b {

    /* renamed from: a  reason: collision with root package name */
    private List f847a = null;
    private ExpandableListView b = null;

    public gs(List list, ExpandableListView expandableListView) {
        new m(this);
        new HashMap();
        this.f847a = list;
        this.b = expandableListView;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public a getChild(int i, int i2) {
        return (a) ((ay) this.f847a.get(i)).g.get(i2);
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public ay getGroup(int i) {
        return (ay) this.f847a.get(i);
    }

    public final void a() {
        for (int i = 0; i < getGroupCount(); i++) {
            this.b.expandGroup(i);
        }
    }

    public final void a(View view, int i) {
        if (i >= 0) {
            ay a2 = getGroup(i);
            ((TextView) view.findViewById(R.id.sitelist_tv_name)).setText(String.valueOf(a2.f) + "  " + a2.e);
            ((TextView) view.findViewById(R.id.tv_groupcount)).setText(String.valueOf(a2.c) + "个群组");
        }
    }

    public final int b(int i, int i2) {
        if (i2 < 0 && i < 0) {
            return 0;
        }
        if (i2 != -1 || this.b.isGroupExpanded(i)) {
            return i2 == getChildrenCount(i) + -1 ? 2 : 1;
        }
        return 0;
    }

    public final long getChildId(int i, int i2) {
        return (long) i2;
    }

    public final View getChildView(int i, int i2, boolean z, View view, ViewGroup viewGroup) {
        if (view == null) {
            gt gtVar = new gt((byte) 0);
            view = g.o().inflate((int) R.layout.listitem_nearbygroup, (ViewGroup) null);
            gtVar.f848a = (ImageView) view.findViewById(R.id.userlist_item_iv_face);
            gtVar.b = (TextView) view.findViewById(R.id.userlist_item_tv_name);
            gtVar.c = (TextView) view.findViewById(R.id.userlist_item_tv_sign);
            gtVar.d = (TextView) view.findViewById(R.id.userlist_item_tv_count);
            view.findViewById(R.id.userlist_item_layout_membercount_backgroud);
            view.findViewById(R.id.userlist_item_iv_person_icon);
            gtVar.e = (ImageView) view.findViewById(R.id.userlist_item_pic_iv_party);
            gtVar.f = (ImageView) view.findViewById(R.id.iv_level_icon);
            gtVar.g = (ImageView) view.findViewById(R.id.userlist_item_tv_new);
            view.setTag(R.id.tag_userlist_item, gtVar);
        }
        a a2 = getChild(i, i2);
        gt gtVar2 = (gt) view.getTag(R.id.tag_userlist_item);
        if (android.support.v4.b.a.a((CharSequence) a2.c)) {
            a2.c = a2.b;
        }
        gtVar2.b.setText(a2.c);
        if (a2.a()) {
            gtVar2.b.setTextColor(g.c((int) R.color.font_vip_name));
        } else {
            gtVar2.b.setTextColor(g.c((int) R.color.text_color));
        }
        gtVar2.e.setVisibility(a2.C ? 0 : 8);
        if (a2.h != null && a2.h.length() > 70) {
            a2.h = a2.h.substring(0, 70);
        }
        gtVar2.c.setText(a2.h);
        int a3 = a.a(a2.A, a2.a());
        if (a3 != -1) {
            gtVar2.f.setVisibility(0);
            gtVar2.f.setImageResource(a3);
        } else {
            gtVar2.f.setVisibility(8);
        }
        gtVar2.d.setText(String.valueOf(a2.k) + "/" + a2.j);
        j.a(a2, gtVar2.f848a, this.b, 3);
        if (a2.c()) {
            gtVar2.g.setVisibility(0);
        } else {
            gtVar2.g.setVisibility(8);
        }
        return view;
    }

    public final int getChildrenCount(int i) {
        if (i < 0 || i >= this.f847a.size()) {
            return 0;
        }
        if (((ay) this.f847a.get(i)).g == null) {
            return 0;
        }
        return ((ay) this.f847a.get(i)).g.size();
    }

    public final int getGroupCount() {
        return this.f847a.size();
    }

    public final long getGroupId(int i) {
        return (long) i;
    }

    public final View getGroupView(int i, boolean z, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = g.o().inflate((int) R.layout.listitem_groupsite, (ViewGroup) null);
            gu guVar = new gu();
            guVar.f849a = (TextView) view.findViewById(R.id.sitelist_tv_name);
            guVar.b = (TextView) view.findViewById(R.id.tv_groupcount);
            view.findViewById(R.id.sitelist_tv_arrow);
            view.findViewById(R.id.layout_root);
            view.setTag(R.id.tag_userlist_item, guVar);
        }
        ay a2 = getGroup(i);
        gu guVar2 = (gu) view.getTag(R.id.tag_userlist_item);
        guVar2.f849a.setText(String.valueOf(a2.f) + "  " + a2.e);
        guVar2.b.setText(String.valueOf(a2.c) + "个群组");
        return view;
    }

    public final boolean hasStableIds() {
        return true;
    }

    public final boolean isChildSelectable(int i, int i2) {
        return true;
    }
}
