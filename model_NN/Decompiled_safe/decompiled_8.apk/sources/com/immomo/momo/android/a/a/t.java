package com.immomo.momo.android.a.a;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import mm.purchasesdk.PurchaseCode;

final class t extends Handler {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ s f702a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    t(s sVar, Looper looper) {
        super(looper);
        this.f702a = sVar;
    }

    public final void handleMessage(Message message) {
        switch (message.what) {
            case 395:
                s.a(this.f702a);
                return;
            case 396:
                if (this.f702a.e() != null) {
                    this.f702a.e().Y();
                    return;
                }
                return;
            case 397:
            case 399:
            default:
                return;
            case 398:
                this.f702a.d();
                return;
            case PurchaseCode.BILL_DYMARK_CREATE_ERROR /*400*/:
                this.f702a.k();
                return;
            case PurchaseCode.BILL_CANCEL_FAIL /*401*/:
                this.f702a.c();
                return;
        }
    }
}
