package com.immomo.momo.android.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.widget.ImageButton;
import com.immomo.momo.util.m;

public class CustomLongClickImageButton extends ImageButton {

    /* renamed from: a  reason: collision with root package name */
    private int f2625a = 2;
    private int b;
    private boolean c = false;
    private Runnable d = null;
    private m e = new m(this);

    public CustomLongClickImageButton(Context context) {
        super(context);
        a();
    }

    public CustomLongClickImageButton(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a();
    }

    public CustomLongClickImageButton(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        a();
    }

    private void a() {
        setLongClickable(false);
        this.b = ViewConfiguration.get(getContext()).getScaledTouchSlop();
        this.e.a((Object) ("!!!!!!!!!!!!!!1touchSlop=" + this.b));
    }

    private void b() {
        removeCallbacks(this.d);
        this.d = null;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (!this.c) {
            return super.onTouchEvent(motionEvent);
        }
        switch (motionEvent.getAction()) {
            case 0:
                this.f2625a = 1;
                if (this.d != null) {
                    b();
                }
                this.d = new z(this);
                postDelayed(this.d, 100);
                break;
            case 1:
            case 3:
                b();
                this.f2625a = 2;
                break;
            case 2:
                float x = (float) ((int) motionEvent.getX());
                float y = (float) ((int) motionEvent.getY());
                float f = (float) this.b;
                if (!(x >= (-f) && y >= (-f) && x < ((float) (getRight() - getLeft())) + f && y < ((float) (getBottom() - getTop())) + f) && this.f2625a == 1) {
                    b();
                    this.f2625a = 2;
                    return true;
                }
                break;
        }
        return super.onTouchEvent(motionEvent);
    }

    public void setOnLongClickListener(View.OnLongClickListener onLongClickListener) {
        super.setOnLongClickListener(onLongClickListener);
        this.c = onLongClickListener != null;
        setLongClickable(false);
    }
}
