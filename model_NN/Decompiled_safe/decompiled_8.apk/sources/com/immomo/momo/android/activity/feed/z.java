package com.immomo.momo.android.activity.feed;

import android.content.Intent;
import com.immomo.a.a.f.a;
import com.immomo.momo.android.broadcast.d;
import com.immomo.momo.android.broadcast.f;
import com.immomo.momo.android.broadcast.p;
import com.immomo.momo.service.bean.ab;

final class z implements d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ y f1487a;

    z(y yVar) {
        this.f1487a = yVar;
    }

    public final void a(Intent intent) {
        ab a2;
        if (f.f2349a.equals(intent.getAction())) {
            String stringExtra = intent.getStringExtra("feedid");
            String stringExtra2 = intent.getStringExtra("userid");
            if (!a.a(stringExtra) && this.f1487a.M.h.equals(stringExtra2)) {
                this.f1487a.Q.a(stringExtra);
            }
        } else if (p.b.equals(intent.getAction()) && (a2 = this.f1487a.R.a(intent.getStringExtra("feedid"))) != null && !this.f1487a.Q.a().contains(a2)) {
            this.f1487a.Q.b(0, a2);
        }
    }
}
