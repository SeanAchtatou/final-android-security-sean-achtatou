package com.immomo.momo.android.game;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.b.a;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.ao;
import com.immomo.momo.android.activity.lh;

@SuppressLint({"ValidFragment"})
public final class k extends lh {
    ai O;
    ViewGroup P;
    EditText Q;
    EditText R;
    Button S;
    Button T;
    String U;
    String V;
    ak W = null;
    boolean X = false;
    private TextView Y;
    private TextView Z;
    private TextView aa;
    private TextView ab;
    /* access modifiers changed from: private */
    public View.OnClickListener ac = new l(this);

    public k(ai aiVar, String str, String str2) {
        this.O = aiVar;
        this.U = str;
        this.V = str2;
        this.W = aiVar.e;
    }

    /* access modifiers changed from: private */
    public void O() {
        Intent intent = new Intent();
        intent.putExtra("product_id", this.O.i);
        intent.putExtra("trade_number", this.W.h);
        intent.putExtra("trade_sign", this.W.g);
        if (!a.a((CharSequence) this.O.j)) {
            intent.putExtra("trade_extendnumber", this.O.j);
        }
        c().setResult(30210, intent);
        c().finish();
    }

    static /* synthetic */ void d(k kVar) {
        if (kVar.W.f2407a.isEmpty()) {
            ((ao) kVar.c()).b(new s(kVar, kVar.c()));
        } else if (!kVar.X) {
            String trim = kVar.Q.getText().toString().trim();
            String trim2 = kVar.R.getText().toString().trim();
            if (a.a((CharSequence) trim)) {
                kVar.a("请输入充值卡号");
                return;
            }
            kVar.W.b.e = trim;
            if (a.a((CharSequence) trim2)) {
                kVar.a("请输入充值卡密码");
                return;
            }
            kVar.W.b.f = trim2;
            if (kVar.W.b.d <= 0) {
                kVar.a("请选择充值卡面额");
                return;
            }
            kVar.X = true;
            ((ao) kVar.c()).b(new r(kVar, kVar.c()));
        } else {
            kVar.a("支付已在进行中...");
        }
    }

    /* access modifiers changed from: protected */
    public final int B() {
        return R.layout.fragment_mdkcardpay;
    }

    /* access modifiers changed from: protected */
    public final void C() {
        this.Y = (TextView) c((int) R.id.opentrade_tv_fee);
        this.Z = (TextView) c((int) R.id.opentrade_tv_product);
        this.aa = (TextView) c((int) R.id.opentrade_tv_username);
        this.ab = (TextView) c((int) R.id.opentrade_tv_momoid);
        this.T = (Button) c((int) R.id.opentrade_btn_confim);
        c((int) R.id.opentrade_tv_desc);
        this.P = (ViewGroup) c((int) R.id.opentrade_layout_cardchannel);
        this.Q = (EditText) c((int) R.id.opentrade_et_cardnumber);
        this.R = (EditText) c((int) R.id.opentrade_et_cardpwd);
        this.S = (Button) c((int) R.id.opentrade_btn_cardamount);
    }

    public final boolean F() {
        return false;
    }

    public final boolean G() {
        if (!this.X) {
            return super.G();
        }
        O();
        return true;
    }

    public final void aj() {
        this.Y.setText("支付金额: " + a.a(this.W.e) + "元");
        this.Z.setText("商品: " + this.O.h);
        if (this.O.g != null) {
            this.aa.setText("用户: " + this.O.g.h());
            this.ab.setText("陌陌号: " + this.O.g.h);
        } else {
            this.aa.setText("用户: ");
            this.ab.setText("陌陌号: ");
        }
        ((ao) c()).b(new s(this, c()));
    }

    public final void g(Bundle bundle) {
        this.T.setOnClickListener(new m(this));
        this.Q.addTextChangedListener(new n(this));
        this.R.addTextChangedListener(new o(this));
        this.S.setOnClickListener(new p(this));
        aj();
    }
}
