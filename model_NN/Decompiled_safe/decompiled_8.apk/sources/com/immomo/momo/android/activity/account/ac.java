package com.immomo.momo.android.activity.account;

import android.content.DialogInterface;
import android.support.v4.b.a;
import android.text.SpannableStringBuilder;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.android.view.a.n;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.util.ao;
import com.immomo.momo.util.e;
import com.immomo.momo.util.k;
import com.immomo.momo.util.m;
import java.util.regex.Pattern;

public final class ac extends ab {

    /* renamed from: a  reason: collision with root package name */
    private EditText f931a = null;
    private EditText b = null;
    private EditText c = null;
    private TextView d = null;
    private bf e = null;
    /* access modifiers changed from: private */
    public RegisterActivity f = null;
    /* access modifiers changed from: private */
    public am g = null;
    /* access modifiers changed from: private */
    public boolean h = false;
    /* access modifiers changed from: private */
    public String i = null;
    private TextView j = null;
    /* access modifiers changed from: private */
    public ImageView k = null;
    /* access modifiers changed from: private */
    public EditText l = null;
    /* access modifiers changed from: private */
    public n m = null;

    public ac(bf bfVar, View view, RegisterActivity registerActivity) {
        super(view);
        new m("RegisterStep1");
        this.e = bfVar;
        this.f = registerActivity;
        this.f931a = (EditText) a((int) R.id.rg_et_email);
        this.b = (EditText) a((int) R.id.rg_et_pwd);
        this.d = (TextView) a((int) R.id.rg_link_phone);
        this.c = (EditText) a((int) R.id.rg_et_pwd_confim);
        this.c.setOnEditorActionListener(new ad(this));
        String charSequence = this.d.getText().toString();
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(this.d.getText());
        spannableStringBuilder.setSpan(new ae(this), 0, charSequence.length(), 33);
        this.d.setMovementMethod(LinkMovementMethod.getInstance());
        this.d.setText(spannableStringBuilder);
        e.a(this.d, 0, charSequence.length());
        TextView textView = (TextView) a((int) R.id.rg_tv_note);
        String charSequence2 = textView.getText().toString();
        SpannableStringBuilder spannableStringBuilder2 = new SpannableStringBuilder(charSequence2);
        spannableStringBuilder2.setSpan(new ag(this), charSequence2.indexOf("陌陌用户协议"), charSequence2.indexOf("陌陌用户协议") + 6, 33);
        textView.setMovementMethod(LinkMovementMethod.getInstance());
        textView.setText(spannableStringBuilder2);
        e.a(textView, charSequence2.indexOf("陌陌用户协议"), charSequence2.indexOf("陌陌用户协议") + 6);
        if (!a.a((CharSequence) this.e.G)) {
            this.f931a.setText(this.e.G);
        }
        if (!a.a((CharSequence) this.e.f3011a)) {
            this.b.setText(this.e.f3011a);
        }
    }

    public final boolean a() {
        boolean z;
        boolean z2;
        if (a(this.f931a)) {
            ao.e(R.string.reg_email_empty);
            z = false;
        } else {
            String trim = this.f931a.getText().toString().trim();
            if (!Pattern.compile("\\w[\\w.-]*@[\\w.-]+\\.\\w+").matcher(trim).matches()) {
                this.f931a.requestFocus();
                ao.e(R.string.reg_email_formaterror);
                z = false;
            } else {
                this.e.G = trim;
                z = true;
            }
        }
        if (z) {
            if (a(this.b)) {
                ao.e(R.string.reg_pwd_empty);
                z2 = false;
            } else {
                String trim2 = this.b.getText().toString().trim();
                if (trim2.length() < 6) {
                    this.b.requestFocus();
                    ao.b(String.format(g.a((int) R.string.reg_pwd_sizemin), "6"));
                    z2 = false;
                } else if (trim2.length() > 16) {
                    this.b.requestFocus();
                    ao.b(String.format(g.a((int) R.string.reg_pwd_sizemax), "16"));
                    z2 = false;
                } else if (a(this.c)) {
                    ao.e(R.string.reg_pwd_confim_empty);
                    z2 = false;
                } else {
                    String trim3 = this.c.getText().toString().trim();
                    if (trim3 == null || !trim3.equals(trim2)) {
                        ao.e(R.string.reg_pwd_confim_notmather);
                        z2 = false;
                    } else {
                        this.e.f3011a = trim2;
                        z2 = true;
                    }
                }
            }
            if (z2) {
                if (!a.a((CharSequence) this.f.l)) {
                    return true;
                }
                g();
                return false;
            }
        }
        return false;
    }

    public final void e() {
        new k("PI", "P123").e();
    }

    public final void f() {
        new k("PO", "P123").e();
    }

    public final void g() {
        if (this.m != null) {
            this.m.dismiss();
        }
        this.h = false;
        View inflate = g.o().inflate((int) R.layout.include_register_1_scode, (ViewGroup) null);
        this.m = new n(this.f);
        this.m.a(0, (int) R.string.dialog_btn_confim, new ah(this));
        this.m.a(1, (int) R.string.dialog_btn_cancel, (DialogInterface.OnClickListener) null);
        this.m.setTitle("输入验证码");
        this.m.a();
        this.m.setContentView(inflate);
        this.m.show();
        this.m.setOnDismissListener(new ai(this));
        this.j = (TextView) inflate.findViewById(R.id.scode_tv_reload);
        this.k = (ImageView) inflate.findViewById(R.id.scode_iv_code);
        this.l = (EditText) inflate.findViewById(R.id.scode_et_inputcode);
        e.a(this.j, 0, this.j.getText().length());
        this.j.setOnClickListener(new aj(this));
        this.f.b(new am(this, this.f));
    }

    public final void h() {
        this.d.setVisibility(8);
    }
}
