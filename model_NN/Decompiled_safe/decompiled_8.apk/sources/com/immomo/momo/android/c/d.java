package com.immomo.momo.android.c;

import android.content.Context;
import android.os.AsyncTask;
import com.immomo.momo.R;
import com.immomo.momo.a.a;
import com.immomo.momo.android.activity.ao;
import com.immomo.momo.util.m;
import org.json.JSONException;

public abstract class d extends AsyncTask {

    /* renamed from: a  reason: collision with root package name */
    private Context f2381a = null;
    public m b = new m(getClass().getSimpleName());

    public d(Context context) {
        this.f2381a = context;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void onPostExecute(a aVar) {
        b();
        if (aVar.b == null) {
            a(aVar.f2369a);
        } else if (aVar.b instanceof Exception) {
            a((Exception) aVar.b);
        } else {
            a(new Exception(aVar.b));
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public a doInBackground(Object... objArr) {
        a aVar = new a();
        try {
            aVar.f2369a = a(objArr);
        } catch (Throwable th) {
            aVar.b = th;
        }
        return aVar;
    }

    /* access modifiers changed from: protected */
    public abstract Object a(Object... objArr);

    /* access modifiers changed from: protected */
    public void a() {
    }

    public final void a(int i) {
        if (this.f2381a instanceof ao) {
            ((ao) this.f2381a).a(i);
        } else {
            com.immomo.momo.util.ao.e(i);
        }
    }

    /* access modifiers changed from: protected */
    public void a(Exception exc) {
        if (exc != null) {
            this.b.a((Throwable) exc);
            if (exc instanceof a) {
                if (!android.support.v4.b.a.a((CharSequence) exc.getMessage())) {
                    a(exc.getMessage());
                } else {
                    a((int) R.string.errormsg_server);
                }
            } else if (exc instanceof com.immomo.a.a.b.d) {
                a(exc.getMessage());
            } else if (exc instanceof JSONException) {
                a((int) R.string.errormsg_dataerror);
            } else {
                a((int) R.string.errormsg_client);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void a(Object obj) {
    }

    public final void a(String str) {
        if (this.f2381a instanceof ao) {
            ((ao) this.f2381a).a(str, 0);
        } else {
            com.immomo.momo.util.ao.b(str, 0);
        }
    }

    /* access modifiers changed from: protected */
    public void b() {
    }

    public final boolean e() {
        return getStatus() == AsyncTask.Status.RUNNING;
    }

    public final Context f() {
        return this.f2381a;
    }

    /* access modifiers changed from: protected */
    public final void onPreExecute() {
        if (this.f2381a == null) {
            cancel(true);
            a aVar = new a();
            aVar.b = new Exception();
            onPostExecute(aVar);
            return;
        }
        a();
    }
}
