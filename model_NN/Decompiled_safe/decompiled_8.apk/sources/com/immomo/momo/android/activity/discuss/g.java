package com.immomo.momo.android.activity.discuss;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import com.immomo.momo.R;
import com.immomo.momo.android.view.cg;
import java.util.Random;

final class g implements cg {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ DiscussMemberListActivity f1233a;

    g(DiscussMemberListActivity discussMemberListActivity) {
        this.f1233a = discussMemberListActivity;
    }

    public final void a() {
        this.f1233a.j.setPadding(0, 0, 0, com.immomo.momo.g.a(0.0f));
        for (int firstVisiblePosition = this.f1233a.j.getFirstVisiblePosition(); firstVisiblePosition <= this.f1233a.j.getLastVisiblePosition(); firstVisiblePosition++) {
            View findViewById = this.f1233a.j.getChildAt(firstVisiblePosition - this.f1233a.j.getFirstVisiblePosition()).findViewById(R.id.triangle_zone);
            if (findViewById != null) {
                Animation loadAnimation = AnimationUtils.loadAnimation(this.f1233a.getApplicationContext(), R.anim.push_left_in);
                loadAnimation.setStartOffset((long) new Random().nextInt(100));
                findViewById.setAnimation(loadAnimation);
                findViewById.setVisibility(0);
            }
        }
    }

    public final void b() {
        this.f1233a.j.setPadding(0, 0, 0, com.immomo.momo.g.a(50.0f));
        for (int firstVisiblePosition = this.f1233a.j.getFirstVisiblePosition(); firstVisiblePosition <= this.f1233a.j.getLastVisiblePosition(); firstVisiblePosition++) {
            View findViewById = this.f1233a.j.getChildAt(firstVisiblePosition - this.f1233a.j.getFirstVisiblePosition()).findViewById(R.id.triangle_zone);
            if (findViewById != null) {
                Animation loadAnimation = AnimationUtils.loadAnimation(this.f1233a.getApplicationContext(), R.anim.push_right_out);
                loadAnimation.setStartOffset((long) new Random().nextInt(100));
                findViewById.setAnimation(loadAnimation);
                findViewById.setVisibility(8);
            }
        }
    }
}
