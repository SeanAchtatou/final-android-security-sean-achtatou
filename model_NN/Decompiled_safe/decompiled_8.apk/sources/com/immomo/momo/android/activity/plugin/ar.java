package com.immomo.momo.android.activity.plugin;

import android.support.v4.b.a;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.util.b;
import com.immomo.momo.util.m;
import java.net.URLEncoder;
import java.util.Date;
import java.util.Random;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

public final class ar {

    /* renamed from: a  reason: collision with root package name */
    private static m f2045a = new m("test_momo");

    public static String a() {
        Random random = new Random();
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < 18; i++) {
            stringBuffer.append("abcdefghijklmnopqrstuvwxyz0123456789".charAt(random.nextInt("abcdefghijklmnopqrstuvwxyz0123456789".length())));
        }
        return stringBuffer.toString();
    }

    public static String a(String str, String str2) {
        try {
            Mac instance = Mac.getInstance("HmacSHA1");
            instance.init(new SecretKeySpec(str2.getBytes("UTF-8"), "HmacSHA1"));
            return new String(b.a(instance.doFinal(str.getBytes("UTF-8"))));
        } catch (Exception e) {
            f2045a.a((Throwable) e);
            return PoiTypeDef.All;
        }
    }

    public static String a(String str, String str2, String str3, String str4, String str5, String str6) {
        String str7 = String.valueOf(str) + "&" + URLEncoder.encode(str2, "utf-8") + "&";
        String str8 = "oauth_consumer_key=" + str3 + "&oauth_nonce=" + str5 + "&oauth_signature_method=HMAC-SHA1&oauth_timestamp=" + str4;
        if (a.f(str6)) {
            str8 = String.valueOf(str8) + "&oauth_token=" + str6;
        }
        return String.valueOf(str7) + URLEncoder.encode(str8, "utf-8");
    }

    public static String b() {
        return new StringBuilder(String.valueOf(new Date().getTime())).toString().substring(0, 10);
    }

    public static String c() {
        return "HMAC-SHA1";
    }
}
