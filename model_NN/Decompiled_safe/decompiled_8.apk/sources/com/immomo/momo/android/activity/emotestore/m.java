package com.immomo.momo.android.activity.emotestore;

import android.content.Intent;
import com.immomo.momo.android.broadcast.d;
import com.immomo.momo.android.broadcast.g;

final class m implements d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ EmotionProfileActivity f1325a;

    m(EmotionProfileActivity emotionProfileActivity) {
        this.f1325a = emotionProfileActivity;
    }

    public final void a(Intent intent) {
        String action = intent.getAction();
        if (this.f1325a.h.f3035a.equals(intent.getStringExtra("eid"))) {
            if (g.c.equals(action)) {
                this.f1325a.a((CharSequence) (String.valueOf(this.f1325a.h.b) + "下载失败"));
            } else if (g.f2350a.equals(action)) {
                this.f1325a.a((CharSequence) (String.valueOf(this.f1325a.h.b) + "下载完成"));
                this.f1325a.h.t = true;
                this.f1325a.h.u = true;
            }
            this.f1325a.u();
        }
    }
}
