package com.immomo.momo.android.activity;

import android.view.View;
import com.immomo.momo.R;

final class fu implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ LanguageSettingsActivity f1502a;

    private fu(LanguageSettingsActivity languageSettingsActivity) {
        this.f1502a = languageSettingsActivity;
    }

    /* synthetic */ fu(LanguageSettingsActivity languageSettingsActivity, byte b) {
        this(languageSettingsActivity);
    }

    public final void onClick(View view) {
        switch (view.getId()) {
            case R.id.language_layout_en /*2131165880*/:
                this.f1502a.t().a("en");
                break;
            case R.id.language_layout_zh /*2131165881*/:
                this.f1502a.t().a("zh");
                break;
        }
        this.f1502a.finish();
    }
}
