package com.immomo.momo.android.map;

import android.view.View;

final class k implements View.OnFocusChangeListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ CreateSiteActivity f2505a;

    k(CreateSiteActivity createSiteActivity) {
        this.f2505a = createSiteActivity;
    }

    public final void onFocusChange(View view, boolean z) {
        if (!z) {
            this.f2505a.h.setText(this.f2505a.h.getText().toString().trim());
        } else if (this.f2505a.m != null) {
            this.f2505a.h.setSelection(this.f2505a.h.getText().toString().length());
        }
    }
}
