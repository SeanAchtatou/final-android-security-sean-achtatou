package com.immomo.momo.android.a;

import android.app.Activity;
import android.support.v4.b.a;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.d;
import com.immomo.momo.android.view.HandyListView;
import com.immomo.momo.android.view.ce;
import com.immomo.momo.service.bean.ag;
import com.immomo.momo.service.bean.ah;
import com.immomo.momo.service.bean.aj;
import com.immomo.momo.util.j;
import com.immomo.momo.util.m;
import java.util.List;

public final class da extends ce {
    /* access modifiers changed from: private */
    public Activity d = null;
    /* access modifiers changed from: private */
    public HandyListView e;

    public da(HandyListView handyListView, Activity activity, List list) {
        super(activity, list);
        new m(this);
        this.e = null;
        this.d = activity;
        this.e = handyListView;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.b.a.a(java.util.Date, boolean):java.lang.String
     arg types: [java.util.Date, int]
     candidates:
      android.support.v4.b.a.a(android.graphics.Bitmap, float):android.graphics.Bitmap
      android.support.v4.b.a.a(android.graphics.Bitmap, java.io.File):android.graphics.Bitmap
      android.support.v4.b.a.a(int, int):java.lang.String
      android.support.v4.b.a.a(java.util.Collection, java.lang.String):java.lang.String
      android.support.v4.b.a.a(java.lang.Object[], java.lang.String):java.lang.String
      android.support.v4.b.a.a(java.lang.String, java.lang.String):org.json.JSONObject
      android.support.v4.b.a.a(float, int[]):void
      android.support.v4.b.a.a(android.app.Activity, int):void
      android.support.v4.b.a.a(android.view.View, java.lang.Runnable):void
      android.support.v4.b.a.a(java.io.File, java.io.File):void
      android.support.v4.b.a.a(java.io.File, java.util.zip.ZipInputStream):void
      android.support.v4.b.a.a(java.lang.Object, java.lang.StringBuilder):void
      android.support.v4.b.a.a(android.content.Context, java.lang.String):boolean
      android.support.v4.b.a.a(com.immomo.a.a.d.b, com.immomo.momo.service.bean.Message):boolean
      android.support.v4.b.a.a(byte[], java.lang.String):byte[]
      android.support.v4.b.a.a(byte[], byte[]):byte[]
      android.support.v4.b.a.a(int[], boolean):byte[]
      android.support.v4.b.a.a(byte[], boolean):int[]
      android.support.v4.b.a.a(int, float):void
      android.support.v4.b.a.a(java.lang.Object, com.immomo.a.a.f):void
      android.support.v4.view.bf.a(int, float):void
      com.immomo.a.a.f.a(java.lang.Object, com.immomo.a.a.f):void
      android.support.v4.b.a.a(java.util.Date, boolean):java.lang.String */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup, int, boolean, boolean, int):void
     arg types: [com.immomo.momo.service.bean.bf, android.widget.ImageView, com.immomo.momo.android.view.HandyListView, int, int, int, int]
     candidates:
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, int, android.view.View, android.view.ViewGroup, boolean, int, boolean):com.immomo.momo.android.c.o
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup, int, boolean, boolean, int):void */
    public final View a(int i, View view) {
        dg dgVar;
        boolean z = false;
        if (view == null) {
            dg dgVar2 = new dg((byte) 0);
            view = LayoutInflater.from(this.d).inflate((int) R.layout.listitem_frienddistancenotice, (ViewGroup) null);
            dgVar2.h = view.findViewById(R.id.frienddis_layout_button);
            dgVar2.i[0] = (Button) dgVar2.h.findViewById(R.id.button1);
            dgVar2.i[1] = (Button) dgVar2.h.findViewById(R.id.button2);
            dgVar2.i[2] = (Button) dgVar2.h.findViewById(R.id.button3);
            dgVar2.i[3] = (Button) dgVar2.h.findViewById(R.id.button4);
            dgVar2.c = (ImageView) view.findViewById(R.id.frienddis_iv_avatar);
            dgVar2.d = (TextView) view.findViewById(R.id.frienddis_tv_username);
            dgVar2.e = (TextView) view.findViewById(R.id.frienddis_tv_content);
            dgVar2.b = (TextView) view.findViewById(R.id.frienddis_tv_time);
            dgVar2.f = (TextView) view.findViewById(R.id.frienddis_tv_distance);
            dgVar2.f773a = view.findViewById(R.id.item_layout);
            dgVar2.g = (TextView) view.findViewById(R.id.frienddis_tv_title);
            view.setTag(dgVar2);
            dgVar = dgVar2;
        } else {
            dgVar = (dg) view.getTag();
        }
        ag agVar = (ag) this.f671a.get(i);
        dgVar.f773a.setOnLongClickListener(new db(this, i));
        dgVar.f773a.setOnClickListener(new dc(this, i));
        if (agVar.l()) {
            List k = agVar.k();
            int size = k.size();
            int dimensionPixelSize = this.b.getResources().getDimensionPixelSize(R.dimen.button_padding);
            dgVar.h.setVisibility(0);
            for (int i2 = 0; i2 < 4; i2++) {
                if (i2 >= size) {
                    dgVar.i[i2].setVisibility(8);
                } else if (((ah) k.get(i2)).e()) {
                    dgVar.i[i2].setVisibility(0);
                    dgVar.i[i2].setText("已处理");
                    dgVar.i[i2].setClickable(false);
                    dgVar.i[i2].setEnabled(false);
                    dgVar.i[i2].setBackgroundResource(R.drawable.btn_big_normal_disable);
                    dgVar.i[i2].setPadding(dimensionPixelSize, 0, dimensionPixelSize, 0);
                } else {
                    if (!d.a(((ah) k.get(i2)).b())) {
                        String c = ((ah) k.get(i2)).c();
                        if (!(c.startsWith("/api/") || c.startsWith("/game/"))) {
                            dgVar.i[i2].setVisibility(8);
                        }
                    }
                    dgVar.i[i2].setVisibility(0);
                    dgVar.i[i2].setText(((ah) k.get(i2)).a());
                    dgVar.i[i2].setEnabled(true);
                    if (((ah) k.get(i2)).b().contains("accept")) {
                        dgVar.i[i2].setBackgroundResource(R.drawable.btn_default_popsubmit);
                    } else {
                        dgVar.i[i2].setBackgroundResource(R.drawable.btn_default);
                    }
                    dgVar.i[i2].setPadding(dimensionPixelSize, 0, dimensionPixelSize, 0);
                    if (this.e.f()) {
                        dgVar.i[i2].setClickable(false);
                    } else {
                        dgVar.i[i2].setClickable(true);
                        dgVar.i[i2].setOnClickListener(new de(this, agVar, (ah) k.get(i2)));
                    }
                }
            }
        } else {
            dgVar.h.setVisibility(8);
        }
        if (!a.f(agVar.f())) {
            dgVar.c.setVisibility(8);
        } else {
            dgVar.c.setVisibility(0);
            j.a((aj) agVar.g(), dgVar.c, (ViewGroup) this.e, 3, false, true, 0);
            dgVar.c.setOnClickListener(new dd(this, agVar));
        }
        dgVar.d.setVisibility(0);
        dgVar.d.setText(agVar.g().h());
        dgVar.e.setText(agVar.h());
        dgVar.b.setText(a.a(agVar.b(), false));
        ImageView imageView = dgVar.c;
        if (!this.e.f()) {
            z = true;
        }
        imageView.setClickable(z);
        dgVar.f.setText("(" + agVar.n() + ")");
        dgVar.g.setText(agVar.c());
        return view;
    }
}
