package com.immomo.momo.android.c;

import android.content.Context;
import com.immomo.momo.a.a;
import com.immomo.momo.android.activity.ao;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.c;
import com.immomo.momo.util.w;

public final class al extends d {

    /* renamed from: a  reason: collision with root package name */
    private Context f2377a;

    public al(Context context) {
        super(context);
        this.f2377a = context;
    }

    /* access modifiers changed from: protected */
    public final Object a(Object... objArr) {
        Object obj = new Object();
        StringBuilder sb = new StringBuilder();
        new w(f(), new am(sb, obj)).a();
        synchronized (obj) {
            obj.wait();
        }
        if (sb.length() > 0) {
            c.a().a(sb.toString());
            return null;
        }
        throw new a("网络信息收集失败，请联系客服");
    }

    /* access modifiers changed from: protected */
    public final void a() {
        if (this.f2377a instanceof ao) {
            ((ao) this.f2377a).a(new v(this.f2377a, "正在检测网络...", this));
        }
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        if (this.f2377a instanceof ao) {
            super.a(exc);
        }
    }

    /* access modifiers changed from: protected */
    public final void a(Object obj) {
        if (this.f2377a instanceof ao) {
            ((ao) this.f2377a).a((CharSequence) "提交成功，感谢你的反馈");
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
        if (this.f2377a instanceof ao) {
            ((ao) this.f2377a).p();
        }
    }
}
