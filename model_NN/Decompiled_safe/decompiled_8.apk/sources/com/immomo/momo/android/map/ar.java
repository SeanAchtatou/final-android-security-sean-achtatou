package com.immomo.momo.android.map;

import android.content.Intent;
import android.view.View;
import com.amap.mapapi.poisearch.PoiTypeDef;

final class ar implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ aq f2470a;
    private final /* synthetic */ CharSequence b;

    ar(aq aqVar, CharSequence charSequence) {
        this.f2470a = aqVar;
        this.b = charSequence;
    }

    public final void onClick(View view) {
        Intent intent = new Intent();
        intent.putExtra("siteid", PoiTypeDef.All);
        intent.putExtra("sitename", this.b.toString().trim());
        intent.putExtra("sitetype", this.f2470a.f2469a.u);
        intent.putExtra("lat", this.f2470a.f2469a.n.getLatitude());
        intent.putExtra("lng", this.f2470a.f2469a.n.getLongitude());
        intent.putExtra("loctype", this.f2470a.f2469a.p);
        this.f2470a.f2469a.setResult(-1, intent);
        this.f2470a.f2469a.finish();
    }
}
