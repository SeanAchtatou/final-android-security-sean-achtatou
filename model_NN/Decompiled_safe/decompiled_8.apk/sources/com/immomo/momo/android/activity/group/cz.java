package com.immomo.momo.android.activity.group;

import android.graphics.Bitmap;
import com.immomo.momo.android.c.g;

final class cz implements g {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ GroupProfileActivity f1597a;
    private final /* synthetic */ String b;
    private final /* synthetic */ String c;

    cz(GroupProfileActivity groupProfileActivity, String str, String str2) {
        this.f1597a = groupProfileActivity;
        this.b = str;
        this.c = str2;
    }

    public final /* synthetic */ void a(Object obj) {
        this.f1597a.ab.post(new da(this, (Bitmap) obj, this.b, this.c));
    }
}
