package com.immomo.momo.android.activity.account;

import android.content.Context;
import android.graphics.Bitmap;
import com.immomo.momo.android.c.d;
import com.immomo.momo.protocol.a.w;

final class j extends d {

    /* renamed from: a  reason: collision with root package name */
    private StringBuilder f1004a = new StringBuilder();
    private /* synthetic */ LoginActivity c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public j(LoginActivity loginActivity, Context context) {
        super(context);
        this.c = loginActivity;
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ Object a(Object... objArr) {
        w.a();
        return w.a(this.f1004a);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        if (this.c.r != null) {
            this.c.r.cancel(true);
        }
        this.c.r = this;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        this.c.v.setImageBitmap((Bitmap) obj);
        this.c.s = true;
        this.c.t = this.f1004a.toString();
        this.b.a((Object) ("cookie:" + this.c.t));
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.c.r = (j) null;
    }
}
