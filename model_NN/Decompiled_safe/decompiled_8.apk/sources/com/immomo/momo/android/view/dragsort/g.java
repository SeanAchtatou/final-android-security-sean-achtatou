package com.immomo.momo.android.view.dragsort;

import android.database.DataSetObserver;

final class g extends DataSetObserver {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ f f2794a;

    g(f fVar) {
        this.f2794a = fVar;
    }

    public final void onChanged() {
        this.f2794a.notifyDataSetChanged();
    }

    public final void onInvalidated() {
        this.f2794a.notifyDataSetInvalidated();
    }
}
