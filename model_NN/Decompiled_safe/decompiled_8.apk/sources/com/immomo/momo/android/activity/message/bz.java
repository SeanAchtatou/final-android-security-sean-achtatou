package com.immomo.momo.android.activity.message;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import com.immomo.momo.R;
import com.immomo.momo.android.view.cg;
import com.immomo.momo.g;
import java.util.Random;

final class bz implements cg {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ HiSessionListActivity f1959a;

    bz(HiSessionListActivity hiSessionListActivity) {
        this.f1959a = hiSessionListActivity;
    }

    public final void a() {
        this.f1959a.l.setPadding(0, 0, 0, 0);
        for (int firstVisiblePosition = this.f1959a.l.getFirstVisiblePosition(); firstVisiblePosition <= this.f1959a.l.getLastVisiblePosition(); firstVisiblePosition++) {
            View childAt = this.f1959a.l.getChildAt(firstVisiblePosition - this.f1959a.l.getFirstVisiblePosition());
            View findViewById = childAt.findViewById(R.id.triangle_zone);
            if (findViewById != null) {
                Animation loadAnimation = AnimationUtils.loadAnimation(this.f1959a.getApplicationContext(), R.anim.push_left_in);
                loadAnimation.setStartOffset((long) new Random().nextInt(100));
                findViewById.setAnimation(loadAnimation);
                findViewById.setVisibility(0);
            }
            View findViewById2 = childAt.findViewById(R.id.userlist_item_iv_face);
            if (findViewById2 != null) {
                findViewById2.setClickable(true);
            }
        }
    }

    public final void b() {
        this.f1959a.l.setPadding(0, 0, 0, g.a(50.0f));
        for (int firstVisiblePosition = this.f1959a.l.getFirstVisiblePosition(); firstVisiblePosition <= this.f1959a.l.getLastVisiblePosition(); firstVisiblePosition++) {
            View childAt = this.f1959a.l.getChildAt(firstVisiblePosition - this.f1959a.l.getFirstVisiblePosition());
            View findViewById = childAt.findViewById(R.id.triangle_zone);
            if (findViewById != null) {
                Animation loadAnimation = AnimationUtils.loadAnimation(this.f1959a.getApplicationContext(), R.anim.push_right_out);
                loadAnimation.setStartOffset((long) new Random().nextInt(100));
                findViewById.setAnimation(loadAnimation);
                findViewById.setVisibility(8);
            }
            View findViewById2 = childAt.findViewById(R.id.userlist_item_iv_face);
            if (findViewById2 != null) {
                findViewById2.setClickable(false);
            }
        }
    }
}
