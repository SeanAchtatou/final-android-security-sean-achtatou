package com.immomo.momo.android.activity.retrieve;

import android.content.DialogInterface;
import android.support.v4.b.a;
import com.immomo.momo.android.view.a.n;
import com.immomo.momo.util.ao;

final class f implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ InputPhoneNumberActivity f2112a;

    f(InputPhoneNumberActivity inputPhoneNumberActivity) {
        this.f2112a = inputPhoneNumberActivity;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        String trim = this.f2112a.y.getText().toString().trim();
        if (a.a((CharSequence) trim)) {
            ao.b("验证码不可为空，请重试");
            this.f2112a.y.requestFocus();
        } else if (this.f2112a.t) {
            this.f2112a.b(new i(this.f2112a, this.f2112a, trim));
        }
        ((n) dialogInterface).c();
    }
}
