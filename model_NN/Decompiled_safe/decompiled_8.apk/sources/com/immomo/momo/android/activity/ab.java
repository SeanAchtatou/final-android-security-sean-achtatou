package com.immomo.momo.android.activity;

import android.content.DialogInterface;

final class ab implements DialogInterface.OnCancelListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ aa f926a;

    ab(aa aaVar) {
        this.f926a = aaVar;
    }

    public final void onCancel(DialogInterface dialogInterface) {
        this.f926a.cancel(true);
        this.f926a.e.finish();
    }
}
