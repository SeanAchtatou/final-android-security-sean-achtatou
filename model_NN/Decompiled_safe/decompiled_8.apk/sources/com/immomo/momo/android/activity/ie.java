package com.immomo.momo.android.activity;

import android.content.DialogInterface;
import com.immomo.momo.R;
import com.immomo.momo.android.view.a.al;
import com.immomo.momo.android.view.a.e;
import com.immomo.momo.android.view.a.n;

final class ie implements al {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ OtherProfileV2Activity f1736a;

    ie(OtherProfileV2Activity otherProfileV2Activity) {
        this.f1736a = otherProfileV2Activity;
    }

    public final void a(int i) {
        switch (i) {
            case 0:
                n nVar = new n(this.f1736a);
                nVar.setTitle("提醒");
                nVar.a("确定拉黑此用户吗");
                nVar.a();
                nVar.a(0, (int) R.string.dialog_btn_confim, new Cif(this));
                nVar.a(1, (int) R.string.dialog_btn_cancel, (DialogInterface.OnClickListener) null);
                nVar.show();
                return;
            case 1:
                e.a(this.f1736a, this.f1736a.f, this.f1736a.t, new ii());
                return;
            default:
                return;
        }
    }
}
