package com.immomo.momo.android.map;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import com.google.android.maps.MapView;
import com.google.android.maps.MyLocationOverlay;
import com.immomo.momo.R;

final class af extends MyLocationOverlay {

    /* renamed from: a  reason: collision with root package name */
    private Bitmap f2459a;
    private /* synthetic */ GoogleMapActivity b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public af(GoogleMapActivity googleMapActivity, Context context, MapView mapView) {
        super(context, mapView);
        this.b = googleMapActivity;
        if (googleMapActivity.i) {
            this.f2459a = GoogleMapActivity.b(googleMapActivity);
        } else {
            this.f2459a = BitmapFactory.decodeResource(googleMapActivity.getResources(), R.drawable.ic_map_pin);
        }
    }

    public final synchronized boolean draw(Canvas canvas, MapView mapView, boolean z, long j) {
        af.super.draw(canvas, mapView, z, j);
        Point point = new Point();
        mapView.getProjection().toPixels(this.b.b, point);
        canvas.drawBitmap(this.f2459a, (float) (point.x - (this.f2459a.getWidth() / 2)), (float) (point.y - (this.f2459a.getHeight() / 2)), (Paint) null);
        return true;
    }
}
