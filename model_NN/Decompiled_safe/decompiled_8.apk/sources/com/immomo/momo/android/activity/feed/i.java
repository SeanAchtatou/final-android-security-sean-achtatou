package com.immomo.momo.android.activity.feed;

import com.immomo.momo.android.view.cx;

final class i implements cx {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ FeedProfileActivity f1474a;
    private int b = 0;

    i(FeedProfileActivity feedProfileActivity) {
        this.f1474a = feedProfileActivity;
    }

    public final void a(int i, int i2) {
        if (this.b == 0) {
            this.b = i;
        }
        if (i != this.b || this.f1474a.af.isShown()) {
            this.f1474a.G.postDelayed(new j(this), 100);
            this.f1474a.h = true;
            return;
        }
        this.f1474a.G.setVisibility(8);
        this.f1474a.h = false;
    }
}
