package com.immomo.momo.android.activity.contacts;

import android.support.v4.b.a;
import android.text.Editable;
import android.text.TextWatcher;
import java.util.Collection;

final class ba implements TextWatcher {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ aw f1158a;

    ba(aw awVar) {
        this.f1158a = awVar;
    }

    public final void afterTextChanged(Editable editable) {
        if (!this.f1158a.ad) {
            this.f1158a.L.a((Object) "afterTextChanged");
            String trim = editable.toString().trim();
            if (a.a((CharSequence) trim)) {
                this.f1158a.P();
                this.f1158a.O();
                this.f1158a.O.setVisibility(4);
                this.f1158a.P.requestFocus();
                return;
            }
            this.f1158a.S.a(false);
            this.f1158a.S.b((Collection) this.f1158a.Z.d(trim));
            this.f1158a.R.k();
            this.f1158a.O.setVisibility(0);
        }
    }

    public final void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public final void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }
}
