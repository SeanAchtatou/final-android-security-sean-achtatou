package com.immomo.momo.android.view;

import android.graphics.Camera;
import android.graphics.Matrix;
import android.view.animation.Animation;
import android.view.animation.Transformation;

public final class at extends Animation {

    /* renamed from: a  reason: collision with root package name */
    private Camera f2728a;
    private final float b;
    private final float c;
    private int d;

    public at(float f, float f2, int i) {
        this.b = f;
        this.c = f2;
        this.d = i;
    }

    public final void a(int i) {
        this.d = i;
    }

    /* access modifiers changed from: protected */
    public final void applyTransformation(float f, Transformation transformation) {
        float f2 = (360.0f * f) + 0.0f;
        float f3 = this.b;
        float f4 = this.c;
        Camera camera = this.f2728a;
        Matrix matrix = transformation.getMatrix();
        camera.save();
        if ((this.d & 1) == 1) {
            camera.rotateX(f2);
        }
        if ((this.d & 2) == 2) {
            camera.rotateY(f2);
        }
        if ((this.d & 4) == 4) {
            camera.rotateZ(f2);
        }
        camera.getMatrix(matrix);
        camera.restore();
        matrix.preTranslate(-f3, -f4);
        matrix.postTranslate(f3, f4);
    }

    public final void initialize(int i, int i2, int i3, int i4) {
        super.initialize(i, i2, i3, i4);
        this.f2728a = new Camera();
    }
}
