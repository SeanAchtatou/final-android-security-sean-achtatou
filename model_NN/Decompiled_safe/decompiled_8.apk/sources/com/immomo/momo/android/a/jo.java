package com.immomo.momo.android.a;

import android.app.Activity;
import android.support.v4.b.a;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.view.HandyListView;
import com.immomo.momo.g;
import com.immomo.momo.plugin.e.c;
import com.immomo.momo.util.e;
import com.immomo.momo.util.j;
import com.immomo.momo.util.m;
import java.util.ArrayList;

public final class jo extends a {
    /* access modifiers changed from: private */
    public m d = new m("WeiboAdapter");
    private HandyListView e = null;
    /* access modifiers changed from: private */
    public Activity f = null;

    public jo(Activity activity, HandyListView handyListView) {
        super(activity, new ArrayList());
        this.f = activity;
        this.e = handyListView;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        jr jrVar;
        c cVar = (c) getItem(i);
        if (view == null) {
            view = g.o().inflate((int) R.layout.listitem_weibo, (ViewGroup) null);
            jr jrVar2 = new jr((byte) 0);
            view.setTag(R.id.tag_userlist_item, jrVar2);
            jrVar2.f905a = (TextView) view.findViewById(R.id.weiboitem_tv_textcontent);
            jrVar2.c = (ImageView) view.findViewById(R.id.weiboitem_iv_imagecontent);
            jrVar2.b = (TextView) view.findViewById(R.id.weiboitem_iv_posttime);
            jrVar2.g = view.findViewById(R.id.weiboitem_layout_repost);
            jrVar2.f = (TextView) jrVar2.g.findViewById(R.id.weiboitem_tv_name);
            jrVar2.e = (TextView) jrVar2.g.findViewById(R.id.weiboitem_tv_textcontent);
            jrVar2.d = (ImageView) jrVar2.g.findViewById(R.id.weiboitem_repost_iv_imagecontent);
            jrVar2.g.findViewById(R.id.weiboitem_repost_iv_posttime);
            jrVar = jrVar2;
        } else {
            jrVar = (jr) view.getTag(R.id.tag_userlist_item);
        }
        jrVar.c.setOnClickListener(new jp(this, i));
        jrVar.d.setOnClickListener(new jq(this, i));
        e.b(jrVar.f905a, a.a(cVar.b) ? "转发微博" : cVar.b, d());
        jrVar.c.setVisibility(8);
        jrVar.d.setVisibility(8);
        if (!com.immomo.a.a.f.a.a(cVar.c)) {
            jrVar.g.setVisibility(0);
            e.b(jrVar.e, a.a(cVar.d) ? PoiTypeDef.All : cVar.d, d());
            jrVar.f.setText("@" + cVar.c);
            if (cVar.h) {
                jrVar.d.setVisibility(0);
                j.a(cVar, jrVar.d, this.e, 5);
            }
        } else {
            jrVar.g.setVisibility(8);
            if (cVar.h) {
                jrVar.c.setVisibility(0);
                j.a(cVar, jrVar.c, this.e, 5);
            }
        }
        if (cVar.e != 0) {
            jrVar.b.setText(a.e(a.a(cVar.e)));
        } else {
            jrVar.b.setText("未知");
        }
        return view;
    }
}
