package com.immomo.momo.android.view;

final class bm implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ MGifImageView f2746a;
    private final /* synthetic */ au b;

    bm(MGifImageView mGifImageView, au auVar) {
        this.f2746a = mGifImageView;
        this.b = auVar;
    }

    public final void run() {
        this.f2746a.requestLayout();
        this.b.i();
        if (this.b.b() == 1 && !this.b.d && this.b.h()) {
            bo c = this.f2746a.e;
            if (c == null || !c.f2747a) {
                this.f2746a.e = new bo(this.f2746a, (byte) 0);
                MGifImageView.h.execute(this.f2746a.e);
            }
        }
    }
}
