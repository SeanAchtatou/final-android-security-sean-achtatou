package com.immomo.momo.android.activity.maintab;

import android.content.Context;
import android.content.Intent;
import com.immomo.momo.android.c.d;
import com.immomo.momo.protocol.a.w;
import com.immomo.momo.service.ai;
import com.immomo.momo.service.aq;

final class j extends d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ MaintabActivity f1892a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public j(MaintabActivity maintabActivity, Context context) {
        super(context);
        this.f1892a = maintabActivity;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        if (!w.a().a(this.f1892a.f)) {
            return false;
        }
        new aq().b(this.f1892a.f);
        if (this.f1892a.f.ah != null) {
            new ai().a(this.f1892a.f.ah);
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        Intent intent = new Intent(com.immomo.momo.android.broadcast.w.f2366a);
        intent.putExtra("momoid", this.f1892a.f.h);
        this.f1892a.sendBroadcast(intent);
    }
}
