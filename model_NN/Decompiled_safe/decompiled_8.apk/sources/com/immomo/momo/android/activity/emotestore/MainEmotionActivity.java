package com.immomo.momo.android.activity.emotestore;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.b.a;
import android.view.View;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.ka;
import com.immomo.momo.android.view.bi;

public class MainEmotionActivity extends ka implements View.OnClickListener {
    public final void a(Intent intent, int i, Bundle bundle, String str) {
        if (EmotionProfileActivity.class.getName().equals(intent.getComponent().getClassName())) {
            intent.putExtra("gremoveid", getIntent().getStringExtra("giftremoteid"));
            i = 123;
        }
        super.a(intent, i, bundle, str);
    }

    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        int i = 0;
        super.a(bundle);
        setContentView((int) R.layout.activity_emotestore_tabs);
        super.x();
        a(am.class, bg.class, ae.class);
        findViewById(R.id.emotionmain_layout_tab1).setOnClickListener(this);
        findViewById(R.id.emotionmain_layout_tab2).setOnClickListener(this);
        findViewById(R.id.emotionmain_layout_tab3).setOnClickListener(this);
        int intExtra = getIntent().getIntExtra("showindex", 0);
        if (intExtra >= 0) {
            i = intExtra > 2 ? 2 : intExtra;
        }
        c(i);
    }

    /* access modifiers changed from: protected */
    public final void a(Fragment fragment, int i) {
        super.a(fragment, i);
        switch (i) {
            case 0:
                findViewById(R.id.emotionmain_layout_tab1).setSelected(true);
                findViewById(R.id.emotionmain_layout_tab2).setSelected(false);
                findViewById(R.id.emotionmain_layout_tab3).setSelected(false);
                return;
            case 1:
                findViewById(R.id.emotionmain_layout_tab2).setSelected(true);
                findViewById(R.id.emotionmain_layout_tab1).setSelected(false);
                findViewById(R.id.emotionmain_layout_tab3).setSelected(false);
                return;
            case 2:
                findViewById(R.id.emotionmain_layout_tab3).setSelected(true);
                findViewById(R.id.emotionmain_layout_tab2).setSelected(false);
                findViewById(R.id.emotionmain_layout_tab1).setSelected(false);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public final void e() {
        super.e();
        bi biVar = new bi(this);
        biVar.a("我的表情");
        biVar.setOnClickListener(new au(this));
        a(biVar);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i == 123 && i2 == -1 && !a.a((CharSequence) getIntent().getStringExtra("giftremoteid"))) {
            finish();
        }
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.emotionmain_layout_tab1 /*2131165586*/:
                c(0);
                return;
            case R.id.emotionmain_tab_label /*2131165587*/:
            case R.id.emotionmain_tab_count /*2131165588*/:
            default:
                return;
            case R.id.emotionmain_layout_tab2 /*2131165589*/:
                c(1);
                return;
            case R.id.emotionmain_layout_tab3 /*2131165590*/:
                c(2);
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        int i = 0;
        int intExtra = intent.getIntExtra("showindex", 0);
        if (intExtra >= 0) {
            i = intExtra > 2 ? 2 : intExtra;
        }
        if (i != w()) {
            c(i);
        }
    }

    public void startActivity(Intent intent, Bundle bundle) {
        super.startActivity(intent, bundle);
        this.e.a((Object) "giftAction, startActivity 2");
    }

    /* access modifiers changed from: protected */
    public final void x() {
        super.x();
    }
}
