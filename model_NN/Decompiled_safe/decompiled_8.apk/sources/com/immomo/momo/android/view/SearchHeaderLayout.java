package com.immomo.momo.android.view;

import android.content.Context;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.util.aq;

public class SearchHeaderLayout extends HeaderLayout {

    /* renamed from: a  reason: collision with root package name */
    private EmoteEditeText f2656a;
    private View b;
    private View c;
    private Animation d = null;

    public SearchHeaderLayout(Context context) {
        super(context);
        d();
    }

    public SearchHeaderLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        d();
    }

    private void d() {
        this.f2656a = (EmoteEditeText) findViewById(R.id.et_newsearchbar);
        this.b = findViewById(R.id.btn_newclear);
        this.c = findViewById(R.id.iv_newloading);
        EmoteEditeText emoteEditeText = this.f2656a;
        EmoteEditeText emoteEditeText2 = this.f2656a;
        emoteEditeText.addTextChangedListener(new aq(20));
        post(new dd(this));
    }

    public final void a(TextWatcher textWatcher) {
        this.f2656a.addTextChangedListener(textWatcher);
    }

    public final void a(View.OnClickListener onClickListener) {
        this.b.setOnClickListener(onClickListener);
    }

    public final void b() {
        this.b.setVisibility(8);
        this.c.setVisibility(0);
        if (this.d == null) {
            this.d = AnimationUtils.loadAnimation(getContext(), R.anim.loading);
        }
        this.c.startAnimation(this.d);
    }

    public final void c() {
        this.b.setVisibility(0);
        this.c.clearAnimation();
        this.c.setVisibility(4);
    }

    public View getClearButton() {
        return this.b;
    }

    /* access modifiers changed from: protected */
    public int getLayout() {
        return R.layout.common_headerbar_search;
    }

    public View getLoadingView() {
        return this.c;
    }

    public EmoteEditeText getSerachEditeText() {
        return this.f2656a;
    }

    public void setEditer(TextView.OnEditorActionListener onEditorActionListener) {
        this.f2656a.setOnEditorActionListener(onEditorActionListener);
    }
}
