package com.immomo.momo.android.a;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.service.bean.g;
import com.immomo.momo.service.bean.h;
import com.immomo.momo.util.m;
import java.util.HashMap;
import java.util.List;

public final class ar extends gj {

    /* renamed from: a  reason: collision with root package name */
    private List f725a = null;
    private ExpandableListView b = null;

    public ar(List list, ExpandableListView expandableListView) {
        new m(this);
        new HashMap();
        this.f725a = list;
        this.b = expandableListView;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public h getGroup(int i) {
        return (h) this.f725a.get(i);
    }

    /* access modifiers changed from: private */
    /* renamed from: c */
    public g getChild(int i, int i2) {
        return (g) ((h) this.f725a.get(i)).b.get(i2);
    }

    public final void a() {
        for (int i = 0; i < getGroupCount(); i++) {
            this.b.expandGroup(i);
        }
    }

    public final void a(int i, int i2) {
        g c = getChild(i, i2);
        if (c != null) {
            c.g = true;
        }
        notifyDataSetChanged();
    }

    public final void a(View view, int i) {
        if (i >= 0) {
            ((TextView) view.findViewById(R.id.sitelist_tv_name)).setText(getGroup(i).f3026a);
        }
    }

    public final int b(int i, int i2) {
        if (i2 < 0 && i < 0) {
            return 0;
        }
        if (i2 != -1 || this.b.isGroupExpanded(i)) {
            return i2 == getChildrenCount(i) + -1 ? 2 : 1;
        }
        return 0;
    }

    public final long getChildId(int i, int i2) {
        return (long) i2;
    }

    public final View getChildView(int i, int i2, boolean z, View view, ViewGroup viewGroup) {
        if (view == null) {
            as asVar = new as((byte) 0);
            view = com.immomo.momo.g.o().inflate((int) R.layout.listitem_contactchild, (ViewGroup) null);
            asVar.f726a = (TextView) view.findViewById(R.id.tv_name);
            asVar.b = (TextView) view.findViewById(R.id.tv_operate);
            view.setTag(R.id.tag_userlist_item, asVar);
        }
        g c = getChild(i, i2);
        as asVar2 = (as) view.getTag(R.id.tag_userlist_item);
        asVar2.f726a.setText(c.e);
        switch (c.b) {
            case 1:
                asVar2.b.setText(c.g ? R.string.contact_relation11 : R.string.contact_relation1);
                break;
            case 2:
                asVar2.b.setText((int) R.string.contact_relation2);
                break;
            case 3:
                asVar2.b.setText((int) R.string.contact_relation3);
                break;
        }
        return view;
    }

    public final int getChildrenCount(int i) {
        if (i < 0 || i >= this.f725a.size()) {
            return 0;
        }
        if (((h) this.f725a.get(i)).b == null) {
            return 0;
        }
        return ((h) this.f725a.get(i)).b.size();
    }

    public final int getGroupCount() {
        return this.f725a.size();
    }

    public final long getGroupId(int i) {
        return (long) i;
    }

    public final View getGroupView(int i, boolean z, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = com.immomo.momo.g.o().inflate((int) R.layout.listitem_contactgroup, (ViewGroup) null);
            at atVar = new at();
            atVar.f727a = (TextView) view.findViewById(R.id.sitelist_tv_name);
            view.setTag(R.id.tag_userlist_item, atVar);
        }
        ((at) view.getTag(R.id.tag_userlist_item)).f727a.setText(getGroup(i).f3026a);
        return view;
    }

    public final boolean hasStableIds() {
        return true;
    }

    public final boolean isChildSelectable(int i, int i2) {
        return true;
    }
}
