package com.immomo.momo.android.view;

import android.content.Context;
import android.text.SpannableStringBuilder;
import android.util.AttributeSet;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.plugin.b.a;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ChatEmoteTextView extends MEmoteTextView {
    public ChatEmoteTextView(Context context) {
        super(context);
    }

    public ChatEmoteTextView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public ChatEmoteTextView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public final CharSequence a(CharSequence charSequence) {
        if (charSequence == null) {
            return PoiTypeDef.All;
        }
        boolean z = false;
        CharSequence a2 = super.a(charSequence);
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(a2);
        Matcher matcher = Pattern.compile("(\\[[.[^\\[\\]]]*?\\|et\\|([.[^\\[\\]]]*?\\|)*[.[^\\[\\]]]*?\\])").matcher(a2);
        while (matcher.find()) {
            z = true;
            getContext();
            spannableStringBuilder.setSpan(new a(matcher.group()), matcher.start(), matcher.end(), 33);
        }
        return z ? spannableStringBuilder : a2;
    }

    /* access modifiers changed from: protected */
    public final CharSequence b(CharSequence charSequence) {
        return charSequence;
    }
}
