package com.immomo.momo.android.view.a;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.android.a.a;
import com.immomo.momo.b;
import com.immomo.momo.service.bean.am;
import java.util.List;

final class ai extends a {
    private /* synthetic */ x d;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ai(x xVar, Context context, List list) {
        super(context, list);
        this.d = xVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.b.a(java.lang.String, boolean):android.graphics.Bitmap
     arg types: [java.lang.String, int]
     candidates:
      com.immomo.momo.b.a(java.lang.String, android.graphics.Bitmap):android.graphics.Bitmap
      com.immomo.momo.b.a(java.lang.String, boolean):android.graphics.Bitmap */
    public final View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = a((int) R.layout.listitem_nearbyfilter);
        }
        if (this.d.F) {
            Object item = getItem(i);
            TextView textView = (TextView) view.findViewById(R.id.neabyfilter_item_tv_name);
            textView.setText(item.toString());
            ImageView imageView = (ImageView) view.findViewById(R.id.neabyfilter_item_iv_icon);
            if (item instanceof am) {
                am amVar = (am) item;
                textView.setText(amVar.b);
                if (!android.support.v4.b.a.a((CharSequence) amVar.f2991a)) {
                    imageView.setImageBitmap(b.a(amVar.f2991a, false));
                    imageView.setVisibility(0);
                } else {
                    imageView.setVisibility(4);
                }
            } else {
                imageView.setVisibility(8);
            }
            if (this.d.f2708a == i) {
                view.findViewById(R.id.neabyfilter_item_iv_selected).setVisibility(0);
            } else {
                view.findViewById(R.id.neabyfilter_item_iv_selected).setVisibility(8);
            }
        }
        return view;
    }
}
