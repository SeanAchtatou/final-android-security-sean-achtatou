package com.immomo.momo.android.activity.group;

import com.immomo.momo.android.view.cx;

final class aw implements cx {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ GroupFeedProfileActivity f1547a;
    private int b = 0;

    aw(GroupFeedProfileActivity groupFeedProfileActivity) {
        this.f1547a = groupFeedProfileActivity;
    }

    public final void a(int i, int i2) {
        if (this.b == 0) {
            this.b = i;
        }
        if (i != this.b || this.f1547a.ab.isShown()) {
            this.f1547a.G.postDelayed(new ax(this), 100);
            this.f1547a.h = true;
            return;
        }
        this.f1547a.G.setVisibility(8);
        this.f1547a.h = false;
    }
}
