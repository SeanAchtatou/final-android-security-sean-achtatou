package com.immomo.momo.android.map;

final class ag implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ RomaAMapActivity f2460a;

    ag(RomaAMapActivity romaAMapActivity) {
        this.f2460a = romaAMapActivity;
    }

    public final void run() {
        try {
            this.f2460a.i = ((double) this.f2460a.f.getMapCenter().getLatitudeE6()) / 1000000.0d;
            this.f2460a.j = ((double) this.f2460a.f.getMapCenter().getLongitudeE6()) / 1000000.0d;
            this.f2460a.h.a((Object) ("result latlng: " + this.f2460a.i + "," + this.f2460a.j));
        } catch (Exception e) {
            this.f2460a.h.a((Throwable) e);
        }
    }
}
