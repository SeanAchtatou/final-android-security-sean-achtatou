package com.immomo.momo.android.activity.event;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import com.immomo.momo.service.bean.u;

final class cj implements AdapterView.OnItemClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ UsersEventListActivity f1403a;

    cj(UsersEventListActivity usersEventListActivity) {
        this.f1403a = usersEventListActivity;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        Intent intent = new Intent(this.f1403a, EventProfileActivity.class);
        intent.putExtra("eventid", ((u) this.f1403a.l.getItem(i)).f3038a);
        this.f1403a.startActivity(intent);
    }
}
