package com.immomo.momo.android.activity.tieba;

import android.graphics.Bitmap;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.immomo.momo.R;

final class s extends WebViewClient {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ EditTieActivity f2299a;
    private final /* synthetic */ View b;

    s(EditTieActivity editTieActivity, View view) {
        this.f2299a = editTieActivity;
        this.b = view;
    }

    public final void onPageFinished(WebView webView, String str) {
        super.onPageFinished(webView, str);
        if (this.b != null) {
            this.b.setVisibility(8);
        }
    }

    public final void onPageStarted(WebView webView, String str, Bitmap bitmap) {
        super.onPageStarted(webView, str, bitmap);
        if (this.b != null) {
            this.b.setVisibility(0);
        }
    }

    public final void onReceivedError(WebView webView, int i, String str, String str2) {
        if (i == -2) {
            this.f2299a.a((int) R.string.errormsg_network_unfind);
        } else {
            this.f2299a.a((int) R.string.errormsg_server);
        }
        if (this.b != null) {
            this.b.setVisibility(8);
        }
    }
}
