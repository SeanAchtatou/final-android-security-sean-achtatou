package com.immomo.momo.android.view;

import android.content.Context;
import android.graphics.PointF;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Scroller;
import com.baidu.location.LocationClientOption;
import com.immomo.momo.g;
import com.immomo.momo.util.m;

public class ScrollLayout extends ViewGroup {

    /* renamed from: a  reason: collision with root package name */
    private int f2654a;
    private Scroller b;
    private VelocityTracker c;
    private float d;
    private int e;
    private int f;
    private boolean g;
    private m h;
    private db i;
    private int j;
    private boolean k;
    private da l;

    public ScrollLayout(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public ScrollLayout(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.f2654a = 0;
        this.g = true;
        this.h = new m("ScrollLayout");
        this.i = null;
        this.j = 0;
        this.l = null;
        this.b = new Scroller(context);
        this.e = 0;
        this.f = g.a(5.0f);
        this.h.a((Object) ("!!!!!!!!!!!!!!1touchSlop=" + this.f));
    }

    private static int a(int i2, int i3, int i4) {
        int mode = View.MeasureSpec.getMode(i3);
        int size = View.MeasureSpec.getSize(i3);
        switch (mode) {
            case Integer.MIN_VALUE:
                if (size < i2) {
                    i2 = size | 16777216;
                    break;
                }
                break;
            case 1073741824:
                i2 = size;
                break;
        }
        return (-16777216 & i4) | i2;
    }

    public final void a() {
        this.f2654a = 3;
        if (!this.b.isFinished()) {
            this.b.abortAnimation();
        }
        if (this.c != null) {
            this.c.recycle();
            this.c = null;
        }
    }

    public final void a(int i2) {
        boolean z = false;
        int i3 = this.e;
        int max = Math.max(0, Math.min(i2, getChildCount() - 1));
        if (getScrollX() != getWidth() * max) {
            int width = (getWidth() * max) - getScrollX();
            this.b.startScroll(getScrollX(), 0, width, 0, Math.abs(width));
            this.e = max;
            invalidate();
        }
        if (i3 != this.e) {
            z = true;
        }
        this.k = z;
        if (this.k && this.i != null) {
            this.i.a(this.e);
            if (g.a()) {
                requestLayout();
            }
        }
    }

    public void computeScroll() {
        if (this.b.computeScrollOffset()) {
            scrollTo(this.b.getCurrX(), this.b.getCurrY());
            postInvalidate();
        }
    }

    public int getCurrentScreen() {
        return this.e;
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        if (!this.g) {
            return false;
        }
        int action = motionEvent.getAction();
        if (action == 2 && this.f2654a != 0) {
            return true;
        }
        float x = motionEvent.getX();
        switch (action) {
            case 0:
                this.d = x;
                this.f2654a = this.b.isFinished() ? 0 : 1;
                break;
            case 1:
            case 3:
                this.f2654a = 0;
                break;
            case 2:
                if (((int) Math.abs(this.d - x)) > this.f) {
                    this.f2654a = 1;
                    break;
                }
                break;
        }
        return this.f2654a != 0;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        int i6 = 0;
        int childCount = getChildCount();
        int bottom = getBottom() - getTop();
        for (int i7 = 0; i7 < childCount; i7++) {
            View childAt = getChildAt(i7);
            if (childAt.getVisibility() != 8) {
                int measuredHeight = childAt.getMeasuredHeight();
                int i8 = bottom - measuredHeight;
                int measuredWidth = childAt.getMeasuredWidth();
                childAt.layout(i6, i8, i6 + measuredWidth, measuredHeight + i8);
                i6 += measuredWidth;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        int size = View.MeasureSpec.getSize(i2);
        if (View.MeasureSpec.getMode(i2) != 1073741824) {
            throw new IllegalStateException("ScrollLayout only can run at EXACTLY mode!");
        }
        int i4 = -1;
        int childCount = getChildCount();
        int i5 = 0;
        int i6 = 0;
        while (i5 < childCount) {
            View childAt = getChildAt(i5);
            childAt.measure(i2, i3);
            int measuredHeight = childAt.getMeasuredHeight();
            if (i5 != this.e) {
                measuredHeight = i4;
            }
            i6 |= (childAt.getMeasuredWidth() & -16777216) | ((childAt.getMeasuredHeight() >> 16) & -256);
            i5++;
            i4 = measuredHeight;
        }
        scrollTo(this.e * size, 0);
        this.j = size;
        setMeasuredDimension(a(getSuggestedMinimumWidth(), i2, 0) | (i6 & -16777216), a(i4, i3, i6 << 16));
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        boolean z;
        if (this.c == null) {
            this.c = VelocityTracker.obtain();
        }
        this.c.addMovement(motionEvent);
        int action = motionEvent.getAction();
        float x = motionEvent.getX();
        switch (action) {
            case 0:
                if (!this.b.isFinished()) {
                    this.b.abortAnimation();
                }
                this.d = x;
                z = true;
                break;
            case 1:
                if (this.f2654a != 3) {
                    VelocityTracker velocityTracker = this.c;
                    velocityTracker.computeCurrentVelocity(LocationClientOption.MIN_SCAN_SPAN);
                    int xVelocity = (int) velocityTracker.getXVelocity();
                    if (xVelocity > 600 && this.e > 0) {
                        a(this.e - 1);
                    } else if (xVelocity >= -600 || this.e >= getChildCount() - 1) {
                        int width = getWidth();
                        a((getScrollX() + ((int) (((float) width) * 0.4f))) / width);
                    } else {
                        a(this.e + 1);
                    }
                    if (this.c != null) {
                        this.c.recycle();
                        this.c = null;
                    }
                    this.f2654a = 0;
                    z = true;
                    break;
                } else {
                    if (this.l != null) {
                        this.l.b(new PointF(motionEvent.getRawX(), motionEvent.getRawY()));
                    }
                    this.f2654a = 0;
                    return true;
                }
                break;
            case 2:
                if (this.f2654a != 3) {
                    int i2 = (int) (this.d - x);
                    this.d = x;
                    if (i2 < 0) {
                        int scrollX = getScrollX();
                        if (scrollX <= 0) {
                            z = false;
                            break;
                        } else if (scrollX + i2 < 0) {
                            i2 = -scrollX;
                        }
                    }
                    if (i2 > 0) {
                        int scrollX2 = getScrollX();
                        if (scrollX2 >= this.j) {
                            z = false;
                            break;
                        } else if (scrollX2 + i2 > this.j) {
                            i2 = this.j - scrollX2;
                        }
                    }
                    scrollBy(i2, 0);
                    z = true;
                    break;
                } else if (this.l == null) {
                    return true;
                } else {
                    this.l.a(new PointF(motionEvent.getRawX(), motionEvent.getRawY()));
                    return true;
                }
            case 3:
                this.f2654a = 0;
            default:
                z = true;
                break;
        }
        return z;
    }

    public void setOnTouchListener(da daVar) {
        this.l = daVar;
    }

    public void setSelectedListener(db dbVar) {
        this.i = dbVar;
        if (dbVar != null) {
            dbVar.a(this.e);
        }
    }

    public void setToScreen(int i2) {
        int i3 = this.e;
        int max = Math.max(0, Math.min(i2, getChildCount() - 1));
        this.e = max;
        scrollTo(max * getWidth(), 0);
        if (i3 != this.e && this.i != null) {
            this.i.a(this.e);
            if (g.a()) {
                requestLayout();
            }
        }
    }
}
