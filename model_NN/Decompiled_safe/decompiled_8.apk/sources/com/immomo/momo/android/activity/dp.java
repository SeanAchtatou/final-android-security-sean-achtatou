package com.immomo.momo.android.activity;

import com.immomo.momo.android.view.cp;

final class dp implements cp {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ EditVipProfileActivity f1257a;

    dp(EditVipProfileActivity editVipProfileActivity) {
        this.f1257a = editVipProfileActivity;
    }

    public final void a(int i) {
        if (this.f1257a.S == 0) {
            this.f1257a.S = i;
            this.f1257a.R = i;
        } else if (i != 0) {
            int round = Math.round(((float) (i - this.f1257a.R)) * 0.5f) + this.f1257a.D.getScrollY();
            if (round > 0 && round < this.f1257a.N) {
                this.f1257a.D.scrollTo(0, round);
            }
            this.f1257a.R = i;
        } else {
            this.f1257a.R = 0;
            this.f1257a.S = 0;
        }
    }
}
