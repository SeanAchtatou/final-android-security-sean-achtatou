package com.immomo.momo.android.view.a;

import android.content.DialogInterface;
import android.support.v4.b.a;
import com.immomo.momo.R;
import com.immomo.momo.util.ao;

final class bd implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ az f2684a;

    bd(az azVar) {
        this.f2684a = azVar;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        if (this.f2684a.k == 0) {
            this.f2684a.c();
            if (this.f2684a.g.getSelectCount() == 0) {
                ao.b("请先选择分类后再进行下一步");
                return;
            }
            this.f2684a.i.setText("上一步");
            this.f2684a.j.setText("完成");
            this.f2684a.f.setInAnimation(this.f2684a.getContext(), R.anim.push_left_in);
            this.f2684a.f.setOutAnimation(this.f2684a.getContext(), R.anim.push_left_out);
            new bg(this.f2684a, this.f2684a.getContext()).execute(new Object[0]);
            this.f2684a.f.showNext();
            az azVar = this.f2684a;
            azVar.k = azVar.k + 1;
        } else if (this.f2684a.k == 1 && this.f2684a.n != null && !a.a((CharSequence) this.f2684a.h.getSelectIds())) {
            this.f2684a.n.a(this.f2684a.h.getSelectIds());
        }
        this.f2684a.d();
    }
}
