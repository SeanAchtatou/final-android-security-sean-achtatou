package com.immomo.momo.android.activity.event;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.protocol.a.j;
import com.immomo.momo.service.bean.aa;
import com.immomo.momo.service.r;

final class u extends d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ EventFeedProfileActivity f1423a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public u(EventFeedProfileActivity eventFeedProfileActivity, Context context) {
        super(context);
        this.f1423a = eventFeedProfileActivity;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        aa a2 = j.a().a(this.f1423a.j);
        if (a2.i == 2) {
            this.f1423a.n.a(a2, this.f1423a.k);
        } else {
            r unused = this.f1423a.n;
            String unused2 = this.f1423a.k;
        }
        return a2;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        aa aaVar = (aa) obj;
        this.f1423a.l = aaVar;
        if (aaVar.i == 2) {
            a("该留言已经被删除");
            this.f1423a.finish();
            return;
        }
        this.f1423a.a(aaVar);
    }
}
