package com.immomo.momo.android.a;

import android.app.Activity;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.immomo.a.a.f.a;
import com.immomo.momo.R;
import com.immomo.momo.android.view.EmoteTextView;
import com.immomo.momo.android.view.HandyListView;
import com.immomo.momo.b;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.util.j;
import java.util.List;

public final class ch extends a {
    private Activity d = null;
    /* access modifiers changed from: private */
    public HandyListView e = null;

    public ch(Activity activity, List list, HandyListView handyListView) {
        super(activity, list);
        new ci();
        this.d = activity;
        this.e = handyListView;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.b.a(java.lang.String, boolean):android.graphics.Bitmap
     arg types: [java.lang.String, int]
     candidates:
      com.immomo.momo.b.a(java.lang.String, android.graphics.Bitmap):android.graphics.Bitmap
      com.immomo.momo.b.a(java.lang.String, boolean):android.graphics.Bitmap */
    public final View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            cl clVar = new cl((byte) 0);
            view = LayoutInflater.from(this.d).inflate((int) R.layout.listitem_groupuser, (ViewGroup) null);
            clVar.i = view.findViewById(R.id.layout_time_container);
            clVar.f757a = view.findViewById(R.id.layout_item_container);
            clVar.b = (ImageView) view.findViewById(R.id.userlist_item_iv_face);
            clVar.c = (TextView) view.findViewById(R.id.userlist_item_tv_name);
            clVar.d = (TextView) view.findViewById(R.id.userlist_item_tv_age);
            clVar.e = (TextView) view.findViewById(R.id.userlist_item_tv_distance);
            clVar.f = (TextView) view.findViewById(R.id.profile_tv_time);
            clVar.g = (EmoteTextView) view.findViewById(R.id.userlist_item_tv_sign);
            clVar.j = (ImageView) view.findViewById(R.id.userlist_item_iv_gender);
            clVar.h = view.findViewById(R.id.userlist_item_layout_genderbackgroud);
            clVar.k = (ImageView) view.findViewById(R.id.userlist_item_pic_iv_weibo);
            clVar.l = (ImageView) view.findViewById(R.id.userlist_item_pic_iv_tweibo);
            clVar.m = (ImageView) view.findViewById(R.id.userlist_item_pic_iv_renren);
            clVar.n = (ImageView) view.findViewById(R.id.userlist_item_pic_iv_vip);
            view.findViewById(R.id.userlist_item_pic_iv_douban);
            clVar.o = (ImageView) view.findViewById(R.id.userlist_item_pic_iv_relation);
            view.findViewById(R.id.userlist_item_pic_iv_device);
            clVar.p = (ImageView) view.findViewById(R.id.userlist_item_pic_iv_multipic);
            clVar.q = (ImageView) view.findViewById(R.id.userlist_item_pic_iv_industry);
            clVar.r = view.findViewById(R.id.triangle_zone);
            clVar.s = (ImageView) view.findViewById(R.id.userlist_item_pic_sign);
            view.setTag(R.id.tag_userlist_item, clVar);
        }
        bf bfVar = (bf) getItem(i);
        cl clVar2 = (cl) view.getTag(R.id.tag_userlist_item);
        clVar2.f757a.setOnClickListener(new cj(this, i));
        clVar2.f757a.setOnLongClickListener(new ck(this, i));
        clVar2.r.setVisibility(8);
        if (bfVar != null) {
            clVar2.e.setText(bfVar.Z);
            if (g.a((int) R.string.profile_distance_hide).equals(bfVar.Z) || g.a((int) R.string.profile_distance_unknown).equals(bfVar.Z)) {
                clVar2.i.setVisibility(8);
            } else {
                clVar2.i.setVisibility(0);
            }
            if (!a.a(bfVar.aa)) {
                clVar2.f.setText(" | " + bfVar.aa);
            }
            clVar2.d.setText(new StringBuilder(String.valueOf(bfVar.I)).toString());
            clVar2.c.setText(bfVar.h());
            if (bfVar.b()) {
                clVar2.c.setTextColor(g.c((int) R.color.font_vip_name));
            } else {
                clVar2.c.setTextColor(g.c((int) R.color.text_color));
            }
            clVar2.g.setText(bfVar.n());
            if (!a.a(bfVar.R)) {
                Bitmap b = b.b(bfVar.R);
                if (b != null) {
                    clVar2.s.setVisibility(0);
                    clVar2.s.setImageBitmap(b);
                } else {
                    clVar2.s.setVisibility(8);
                }
            } else {
                clVar2.s.setVisibility(8);
            }
            if ("F".equals(bfVar.H)) {
                clVar2.h.setBackgroundResource(R.drawable.bg_gender_famal);
                clVar2.j.setImageResource(R.drawable.ic_user_famale);
            } else {
                clVar2.h.setBackgroundResource(R.drawable.bg_gender_male);
                clVar2.j.setImageResource(R.drawable.ic_user_male);
            }
            if (bfVar.j()) {
                clVar2.p.setVisibility(0);
            } else {
                clVar2.p.setVisibility(8);
            }
            if ("both".equals(bfVar.P)) {
                clVar2.o.setVisibility(0);
            } else {
                clVar2.o.setVisibility(8);
            }
            if (bfVar.an) {
                clVar2.k.setVisibility(0);
                clVar2.k.setImageResource(bfVar.ap ? R.drawable.ic_userinfo_weibov : R.drawable.ic_userinfo_weibo);
            } else {
                clVar2.k.setVisibility(8);
            }
            if (bfVar.at) {
                clVar2.l.setVisibility(0);
                clVar2.l.setImageResource(bfVar.au ? R.drawable.ic_userinfo_tweibov : R.drawable.ic_userinfo_tweibo);
            } else {
                clVar2.l.setVisibility(8);
            }
            if (bfVar.b()) {
                if (bfVar.c()) {
                    clVar2.n.setImageResource(R.drawable.ic_userinfo_vip_year);
                } else {
                    clVar2.n.setImageResource(R.drawable.ic_userinfo_vip);
                }
                clVar2.n.setVisibility(0);
            } else {
                clVar2.n.setVisibility(8);
            }
            if (bfVar.ar) {
                clVar2.m.setVisibility(0);
            } else {
                clVar2.m.setVisibility(8);
            }
            if (!a.a(bfVar.M)) {
                clVar2.q.setVisibility(0);
                clVar2.q.setImageBitmap(b.a(bfVar.M, true));
            } else {
                clVar2.q.setVisibility(8);
            }
            j.a(bfVar, clVar2.b, this.e, 3);
        }
        return view;
    }
}
