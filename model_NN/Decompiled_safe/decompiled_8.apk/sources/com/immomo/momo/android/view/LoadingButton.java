package com.immomo.momo.android.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import com.immomo.momo.R;

public class LoadingButton extends bj implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private String f2634a = "查看更多";
    private String b = "正在加载";
    private boolean c = false;
    private int d = 0;
    private int e = R.drawable.ic_loading;
    private bl f = null;

    public LoadingButton(Context context) {
        super(context);
        h();
    }

    public LoadingButton(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        h();
    }

    private void h() {
        a(this.d);
        setButtonOnClickListener(this);
        a(this.f2634a);
    }

    public final boolean d() {
        return this.c;
    }

    public final void e() {
        this.c = false;
        a();
        a(this.d);
        a(this.f2634a);
        setEnabled(true);
    }

    public final void f() {
        this.c = true;
        a(this.e);
        b();
        a(this.b);
        setEnabled(false);
    }

    public final void g() {
        this.c = true;
        a(this.e);
        b();
        c();
        setEnabled(false);
    }

    public String getLoadingText() {
        return this.b;
    }

    public String getNormalText() {
        return this.f2634a;
    }

    public void onClick(View view) {
        if (!this.c && this.f != null) {
            f();
            if (this.f != null) {
                this.f.u();
            }
        }
    }

    public void setLoadingText(int i) {
        setLoadingText(getContext().getString(i));
    }

    public void setLoadingText(String str) {
        this.b = str;
        if (this.c) {
            a(str);
        }
    }

    public void setNormalIconResId(int i) {
        this.d = i;
        if (i < 0) {
            i = 0;
        }
        if (!this.c) {
            a(i);
        }
    }

    public void setNormalText(int i) {
        setNormalText(getContext().getString(i));
    }

    public void setNormalText(String str) {
        this.f2634a = str;
        if (!this.c) {
            a(this.f2634a);
        }
    }

    public void setOnProcessListener(bl blVar) {
        this.f = blVar;
    }
}
