package com.immomo.momo.android.activity.emotestore;

import android.graphics.Bitmap;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;

final class l implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ k f1324a;
    private final /* synthetic */ Bitmap b;

    l(k kVar, Bitmap bitmap) {
        this.f1324a = kVar;
        this.b = bitmap;
    }

    public final void run() {
        if (this.b != null) {
            BitmapDrawable bitmapDrawable = new BitmapDrawable(this.b);
            bitmapDrawable.setTileModeXY(Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);
            bitmapDrawable.setDither(true);
            this.f1324a.f1323a.n.setBackgroundDrawable(bitmapDrawable);
        }
    }
}
