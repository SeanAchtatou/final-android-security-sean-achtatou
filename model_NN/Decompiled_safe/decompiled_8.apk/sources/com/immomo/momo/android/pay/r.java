package com.immomo.momo.android.pay;

import android.content.Context;
import android.os.Message;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.a;

final class r extends d {

    /* renamed from: a  reason: collision with root package name */
    private v f2569a = null;
    private Message c;
    private /* synthetic */ BuyMemberActivity d;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public r(BuyMemberActivity buyMemberActivity, Context context, Message message) {
        super(context);
        this.d = buyMemberActivity;
        this.c = message;
        this.f2569a = new v(context);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        return a.a().b();
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
        this.f2569a.setCancelable(true);
        this.f2569a.setOnCancelListener(new s(this));
        this.f2569a.a("生成订单...");
        this.d.a(this.f2569a);
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        super.a(exc);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        this.d.y = (String) obj;
        this.d.F.sendMessage(this.c);
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.d.p();
    }
}
