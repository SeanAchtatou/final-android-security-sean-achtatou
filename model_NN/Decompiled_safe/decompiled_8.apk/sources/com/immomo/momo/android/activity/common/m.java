package com.immomo.momo.android.activity.common;

import android.os.Handler;
import android.os.Message;

final class m extends Handler {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public /* synthetic */ CloudMessageTabsActivity f1124a;

    m(CloudMessageTabsActivity cloudMessageTabsActivity) {
        this.f1124a = cloudMessageTabsActivity;
    }

    public final void handleMessage(Message message) {
        if (message.what == this.f1124a.t) {
            this.f1124a.r.setText(String.valueOf(this.f1124a.w) + "/" + this.f1124a.x);
            this.f1124a.s.setText(String.valueOf((int) ((((float) this.f1124a.w) / ((float) (this.f1124a.x + 2))) * 100.0f)) + "%");
            this.f1124a.q.setMax(this.f1124a.x);
            this.f1124a.q.setProgress(this.f1124a.w);
        } else if (message.what == this.f1124a.u) {
            this.f1124a.p.setText("正在更新数据库... ");
            this.f1124a.q.setVisibility(8);
            this.f1124a.r.setVisibility(8);
            this.f1124a.s.setVisibility(8);
        } else if (message.what == this.f1124a.v) {
            this.f1124a.o.a(0, "完成", new n(this));
        }
    }
}
