package com.immomo.momo.android.activity.event;

import com.immomo.momo.android.view.cx;

final class i implements cx {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ EventFeedProfileActivity f1411a;
    private int b = 0;

    i(EventFeedProfileActivity eventFeedProfileActivity) {
        this.f1411a = eventFeedProfileActivity;
    }

    public final void a(int i, int i2) {
        if (this.b == 0) {
            this.b = i;
        }
        if (i != this.b || this.f1411a.af.isShown()) {
            this.f1411a.G.postDelayed(new j(this), 100);
            this.f1411a.h = true;
            return;
        }
        this.f1411a.G.setVisibility(8);
        this.f1411a.h = false;
    }
}
