package com.immomo.momo.android.activity.tieba;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.b.a;
import com.immomo.momo.android.c.d;
import com.immomo.momo.service.bean.av;
import com.immomo.momo.util.j;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import mm.purchasesdk.PurchaseCode;

final class v extends d {

    /* renamed from: a  reason: collision with root package name */
    boolean f2302a = false;
    private List c = null;
    private com.immomo.momo.android.view.a.v d = null;
    /* access modifiers changed from: private */
    public /* synthetic */ EditTieActivity e;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public v(EditTieActivity editTieActivity, Context context, List list, boolean z) {
        super(context);
        this.e = editTieActivity;
        this.c = list;
        this.f2302a = z;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        ArrayList arrayList = new ArrayList();
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.c.size()) {
                return arrayList;
            }
            av avVar = (av) this.c.get(i2);
            if (avVar != null) {
                av avVar2 = (av) this.e.K.get(avVar.f2998a);
                if (avVar2 == null) {
                    File file = new File(avVar.f2998a);
                    if (file.exists()) {
                        Bitmap a2 = a.a(file, (int) PurchaseCode.LOADCHANNEL_ERR, (int) PurchaseCode.LOADCHANNEL_ERR);
                        if (a2 != null) {
                            avVar.c = a2;
                            j.a(avVar.f2998a, a2);
                        }
                        avVar.b = file;
                        this.e.K.put(avVar.f2998a, avVar);
                        avVar2 = avVar;
                    } else {
                        avVar2 = null;
                    }
                }
                if (avVar2 != null) {
                    arrayList.add(avVar2);
                }
            }
            i = i2 + 1;
        }
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.d = new com.immomo.momo.android.view.a.v(this.e.n(), "正在处理...");
        this.d.setCancelable(false);
        this.e.a(this.d);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        this.e.z.post(new w(this, (ArrayList) obj));
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.e.p();
    }
}
