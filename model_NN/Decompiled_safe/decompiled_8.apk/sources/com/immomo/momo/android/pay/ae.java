package com.immomo.momo.android.pay;

import android.graphics.Bitmap;
import com.immomo.momo.R;
import com.immomo.momo.a;
import com.immomo.momo.android.view.a.ao;
import com.immomo.momo.util.h;
import java.io.File;

final class ae implements Runnable {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public /* synthetic */ ad f2524a;
    private final /* synthetic */ Bitmap b;
    private final /* synthetic */ String c;
    private final /* synthetic */ String d;

    ae(ad adVar, Bitmap bitmap, String str, String str2) {
        this.f2524a = adVar;
        this.b = bitmap;
        this.c = str;
        this.d = str2;
    }

    public final void run() {
        if (!this.f2524a.f2523a.isFinishing()) {
            ao aoVar = new ao(this.f2524a.f2523a, this.f2524a.f2523a.f);
            aoVar.setTitle((int) R.string.vip_sharedialog_title);
            aoVar.a(this.c.trim(), (int) R.drawable.round_shareobox);
            aoVar.a(this.b);
            File file = new File(a.b(), "vipshare.png_");
            h.a(this.b, file);
            aoVar.a(new af(this, aoVar, this.d, file));
            aoVar.show();
            this.f2524a.f2523a.g.c("show_vipdialog", false);
        } else if (this.b != null && !this.b.isRecycled()) {
            this.b.recycle();
        }
    }
}
