package com.immomo.momo.android.activity.group;

import android.content.Intent;
import com.immomo.momo.android.activity.emotestore.EmotionProfileActivity;
import com.immomo.momo.android.view.a.al;
import com.immomo.momo.android.view.a.n;
import com.immomo.momo.g;
import com.immomo.momo.plugin.b.a;
import com.immomo.momo.service.bean.a.f;

final class be implements al {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ GroupFeedProfileActivity f1556a;
    private final /* synthetic */ String[] b;
    private final /* synthetic */ f c;

    be(GroupFeedProfileActivity groupFeedProfileActivity, String[] strArr, f fVar) {
        this.f1556a = groupFeedProfileActivity;
        this.b = strArr;
        this.c = fVar;
    }

    public final void a(int i) {
        if ("回复".equals(this.b[i])) {
            this.f1556a.a(this.c);
        } else if ("查看表情".equals(this.b[i])) {
            GroupFeedProfileActivity groupFeedProfileActivity = this.f1556a;
            GroupFeedProfileActivity groupFeedProfileActivity2 = this.f1556a;
            a aVar = new a(GroupFeedProfileActivity.c(this.c.g));
            Intent intent = new Intent(this.f1556a, EmotionProfileActivity.class);
            intent.putExtra("eid", aVar.h());
            this.f1556a.startActivity(intent);
        } else if ("复制文本".equals(this.b[i])) {
            g.a((CharSequence) this.c.g);
            this.f1556a.a((CharSequence) "已成功复制文本");
        } else if ("删除".equals(this.b[i])) {
            n.a(this.f1556a, "确定要删除该评论？", new bf(this, this.c)).show();
        }
    }
}
