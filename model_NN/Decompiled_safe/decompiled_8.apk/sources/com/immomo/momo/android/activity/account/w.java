package com.immomo.momo.android.activity.account;

import android.content.Context;
import android.content.Intent;
import com.immomo.momo.android.activity.maintab.MaintabActivity;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.g;
import com.immomo.momo.service.a.ar;
import com.immomo.momo.service.aq;
import com.immomo.momo.service.bean.at;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.util.ak;
import com.immomo.momo.util.ao;
import com.immomo.momo.util.jni.Codec;
import com.immomo.momo.util.k;
import org.json.JSONObject;

final class w extends d {

    /* renamed from: a  reason: collision with root package name */
    private bf f1017a;
    private /* synthetic */ RegisterActivityWithP c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    private w(RegisterActivityWithP registerActivityWithP, Context context, bf bfVar) {
        super(context);
        this.c = registerActivityWithP;
        this.f1017a = null;
        this.f1017a = bfVar;
    }

    /* synthetic */ w(RegisterActivityWithP registerActivityWithP, Context context, bf bfVar, byte b) {
        this(registerActivityWithP, context, bfVar);
    }

    /* access modifiers changed from: protected */
    public final Object a(Object... objArr) {
        aq aqVar = new aq(this.f1017a.h);
        try {
            com.immomo.momo.protocol.a.w.a().a(this.f1017a, this.f1017a.W);
            aqVar.b(this.f1017a);
            aqVar.a();
            return null;
        } catch (Exception e) {
            this.b.a((Throwable) e);
            throw e;
        } catch (Throwable th) {
            aqVar.a();
            throw th;
        }
    }

    /* access modifiers changed from: protected */
    public final void a() {
        v vVar = new v(this.c, "正在初始化，请稍候...");
        vVar.setOnCancelListener(new x(this));
        this.c.a(vVar);
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("error", exc.toString());
            new k("U", "U93", jSONObject).e();
        } catch (Exception e) {
        }
        this.b.a((Throwable) exc);
        this.c.p();
        ao.b("注册成功，请登录", 1);
        Intent intent = new Intent(this.c.getApplicationContext(), LoginActivity.class);
        intent.putExtra("account", this.f1017a.G);
        this.c.startActivity(intent);
        this.c.finish();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.service.bean.at.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, boolean]
     candidates:
      com.immomo.momo.service.bean.at.a(android.content.Context, java.lang.String):com.immomo.momo.service.bean.at
      com.immomo.momo.service.bean.at.a(java.lang.Integer, java.lang.Integer):void
      com.immomo.momo.service.bean.at.a(java.lang.String, java.util.Date):void
      com.immomo.momo.service.bean.at.a(java.lang.String, java.lang.Object):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.immomo.momo.util.ak.a(android.content.Context, java.lang.String):com.immomo.momo.util.ak
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Integer):int
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Long):long
      com.immomo.momo.util.ak.a(java.lang.String, int):void
      com.immomo.momo.util.ak.a(java.lang.String, long):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.String):void
      com.immomo.momo.util.ak.a(java.lang.String, boolean):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Boolean):boolean
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void */
    /* access modifiers changed from: protected */
    public final void a(Object obj) {
        at a2 = at.a(this.c.getApplicationContext(), this.f1017a.h);
        a2.a("newuser", (Object) true);
        this.c.t().a(this.f1017a, a2);
        ak akVar = g.d().f668a;
        akVar.a("account", (Object) this.f1017a.h);
        akVar.a("momoid", (Object) this.f1017a.h);
        akVar.a("cookie", (Object) Codec.c(this.f1017a.W));
        ar.b();
        this.c.startActivity(new Intent(this.c.getApplicationContext(), MaintabActivity.class));
        this.c.finish();
    }
}
