package com.immomo.momo.android.view.a;

import android.view.MotionEvent;
import android.view.View;

final class bk implements View.OnTouchListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ bj f2690a;

    bk(bj bjVar) {
        this.f2690a = bjVar;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        return this.f2690a.f2689a.onTouchEvent(motionEvent);
    }
}
