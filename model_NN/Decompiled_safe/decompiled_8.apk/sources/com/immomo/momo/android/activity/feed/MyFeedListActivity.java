package com.immomo.momo.android.activity.feed;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import com.immomo.momo.R;
import com.immomo.momo.android.a.cs;
import com.immomo.momo.android.activity.ah;
import com.immomo.momo.android.broadcast.d;
import com.immomo.momo.android.broadcast.f;
import com.immomo.momo.android.broadcast.p;
import com.immomo.momo.android.view.LoadingButton;
import com.immomo.momo.android.view.MomoRefreshListView;
import com.immomo.momo.android.view.bi;
import com.immomo.momo.android.view.bl;
import com.immomo.momo.android.view.bu;
import com.immomo.momo.android.view.cv;
import com.immomo.momo.g;
import com.immomo.momo.service.ai;
import com.immomo.momo.service.bean.ab;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class MyFeedListActivity extends ah implements View.OnClickListener, AdapterView.OnItemClickListener, bl, bu, cv {
    /* access modifiers changed from: private */
    public MomoRefreshListView h = null;
    /* access modifiers changed from: private */
    public LoadingButton i;
    /* access modifiers changed from: private */
    public List j = null;
    /* access modifiers changed from: private */
    public Set k = new HashSet();
    /* access modifiers changed from: private */
    public ai l = null;
    /* access modifiers changed from: private */
    public aj m = null;
    /* access modifiers changed from: private */
    public ai n = null;
    /* access modifiers changed from: private */
    public cs o = null;
    /* access modifiers changed from: private */
    public int p;
    /* access modifiers changed from: private */
    public int q;
    private f r;
    private p s;
    private d t;

    public MyFeedListActivity() {
        new Handler();
        this.p = 0;
        this.q = 0;
        this.r = null;
        this.s = null;
        this.t = new af(this);
    }

    static /* synthetic */ ab l(MyFeedListActivity myFeedListActivity) {
        if (myFeedListActivity.o.getCount() <= 0) {
            return null;
        }
        return (ab) myFeedListActivity.o.getItem(myFeedListActivity.o.getCount() - 1);
    }

    /* access modifiers changed from: private */
    public void w() {
        this.h.n();
        this.i.e();
        x();
    }

    private void x() {
        if (this.n != null && !this.n.isCancelled()) {
            this.n.cancel(true);
        }
        if (this.m != null && !this.m.isCancelled()) {
            this.m.cancel(true);
        }
    }

    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        super.a(bundle);
        setContentView((int) R.layout.activity_myfeedlist);
        m().setTitleText((int) R.string.my_feedlist_title);
        bi a2 = new bi(this).a((int) R.drawable.ic_topbar_addfeed_white);
        a2.setMarginRight(8);
        a2.setBackgroundResource(R.drawable.bg_header_submit);
        a(a2, new ah(this));
        this.h = (MomoRefreshListView) findViewById(R.id.listview);
        this.h.setTimeEnable(false);
        this.h.setCompleteScrollTop(false);
        this.h.setEnableLoadMoreFoolter(true);
        this.i = this.h.getFooterViewButton();
        this.h.addHeaderView(g.o().inflate((int) R.layout.listitem_blank, (ViewGroup) null));
        this.h.a(g.o().inflate((int) R.layout.include_feed_listempty, (ViewGroup) null));
        this.i.setOnProcessListener(this);
        this.h.setOnPullToRefreshListener$42b903f6(this);
        this.h.setOnCancelListener$135502(new ag(this));
        d();
        this.r = new f(this);
        this.r.a(this.t);
        this.s = new p(this);
        this.s.a(this.t);
    }

    public final void b_() {
        if (this.m != null && !this.m.isCancelled()) {
            this.m.cancel(true);
        }
        a(new aj(this, this)).execute(new Object[0]);
    }

    /* access modifiers changed from: protected */
    public final void d() {
        super.d();
        this.l = new ai();
        this.j = this.l.a(this.f.h, 20);
        MomoRefreshListView momoRefreshListView = this.h;
        cs csVar = new cs(this, this.j, this.h);
        this.o = csVar;
        momoRefreshListView.setAdapter((ListAdapter) csVar);
        for (ab add : this.j) {
            this.k.add(add);
        }
        this.p += this.o.getCount();
        if (this.j.size() < 20) {
            this.h.k();
        }
        this.h.l();
    }

    public void onClick(View view) {
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        x();
        if (this.r != null) {
            unregisterReceiver(this.r);
            this.r = null;
        }
        if (this.s != null) {
            unregisterReceiver(this.s);
            this.s = null;
        }
        super.onDestroy();
    }

    public void onItemClick(AdapterView adapterView, View view, int i2, long j2) {
    }

    /* access modifiers changed from: protected */
    public final void s() {
        if (this.o.getCount() < 20) {
            this.h.l();
        }
    }

    public final void u() {
        this.i.f();
        this.n = new ai(this, this);
        this.n.execute(new Object[0]);
    }

    public final void v() {
        this.p = this.q;
        w();
    }
}
