package com.immomo.momo.android.activity.group;

import android.os.Bundle;
import android.support.v4.b.a;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.ah;
import com.immomo.momo.android.view.HeaderLayout;
import com.immomo.momo.service.bean.a.g;
import com.immomo.momo.service.x;
import com.immomo.momo.service.y;
import com.immomo.momo.util.h;
import java.io.File;

public class ShareGroupPartyActivity extends ah implements View.OnClickListener {
    private HeaderLayout h = null;
    private Button i;
    private View j;
    /* access modifiers changed from: private */
    public String k = PoiTypeDef.All;
    private ImageView l;
    private ImageView m;
    private ImageView n;
    private ImageView o;
    /* access modifiers changed from: private */
    public boolean p = false;
    /* access modifiers changed from: private */
    public boolean q = false;
    /* access modifiers changed from: private */
    public boolean r = false;
    /* access modifiers changed from: private */
    public boolean s = false;
    /* access modifiers changed from: private */
    public int t = -1;

    public static File c(String str) {
        g gVar = new g(str);
        new x().a(gVar, str);
        File a2 = h.a(new y().e(gVar.c).getLoadImageId(), 3);
        if (a2 == null || !a2.exists()) {
            return null;
        }
        return a2;
    }

    /* access modifiers changed from: private */
    public void u() {
        if (this.p) {
            this.l.setImageResource(R.drawable.ic_publish_weibo_selected);
        } else {
            this.l.setImageResource(R.drawable.ic_publish_weibo_normal);
        }
    }

    /* access modifiers changed from: private */
    public void v() {
        if (this.q) {
            this.o.setImageResource(R.drawable.ic_publish_renren_selected);
        } else {
            this.o.setImageResource(R.drawable.ic_publish_renren_normal);
        }
    }

    /* access modifiers changed from: private */
    public void w() {
        if (this.s) {
            this.n.setImageResource(R.drawable.ic_publish_weixin_selected);
        } else {
            this.n.setImageResource(R.drawable.ic_publish_weixin_normal);
        }
    }

    /* access modifiers changed from: private */
    public void x() {
        if (this.r) {
            this.m.setImageResource(R.drawable.ic_publish_tweibo_selected);
        } else {
            this.m.setImageResource(R.drawable.ic_publish_tweibo_normal);
        }
    }

    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        super.a(bundle);
        setContentView((int) R.layout.activity_shareparty);
        d();
        this.h = (HeaderLayout) findViewById(R.id.layout_header);
        this.h.setTitleText((int) R.string.sharegroup_title);
        this.i = (Button) findViewById(R.id.share_button);
        this.j = findViewById(R.id.share_layout);
        this.l = (ImageView) findViewById(R.id.iv_sina);
        this.o = (ImageView) findViewById(R.id.iv_renren);
        this.n = (ImageView) findViewById(R.id.iv_weixin);
        this.m = (ImageView) findViewById(R.id.iv_tx);
        this.j.setVisibility(0);
        this.i.setOnClickListener(this);
        this.l.setOnClickListener(new fc(this));
        this.o.setOnClickListener(new fd(this));
        this.m.setOnClickListener(new fe(this));
        this.n.setOnClickListener(new ff(this));
        if (this.p) {
            this.l.setVisibility(0);
            u();
        } else {
            this.l.setVisibility(8);
        }
        if (this.r) {
            this.m.setVisibility(0);
            x();
        } else {
            this.m.setVisibility(8);
        }
        if (this.q) {
            this.o.setVisibility(0);
            v();
        } else {
            this.o.setVisibility(8);
        }
        if (this.s) {
            this.n.setVisibility(0);
            w();
            return;
        }
        this.n.setVisibility(8);
    }

    /* access modifiers changed from: protected */
    public final void d() {
        this.t = getIntent().getIntExtra("share_type", -1);
        this.k = getIntent().getStringExtra("pid");
        this.q = this.f.ar;
        this.p = this.f.an;
        this.r = this.f.at;
        this.s = true;
        if (!a.f(this.k) || this.t == -1) {
            this.e.a((Object) "ShareGroupPartyActivity 初始化数据错误 --- 需要参数 1：sharetype 2：partyId");
            finish();
        }
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.share_button /*2131165926*/:
                if (this.f != null) {
                    new fg(this, this).execute(new Object[0]);
                    return;
                }
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
    }
}
