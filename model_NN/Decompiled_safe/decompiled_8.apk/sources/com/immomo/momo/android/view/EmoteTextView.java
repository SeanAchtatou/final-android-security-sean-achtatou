package com.immomo.momo.android.view;

import android.content.Context;
import android.support.v4.b.a;
import android.text.Editable;
import android.text.SpannableStringBuilder;
import android.util.AttributeSet;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EmoteTextView extends HandyTextView {

    /* renamed from: a  reason: collision with root package name */
    private boolean f2627a = true;

    public EmoteTextView(Context context) {
        super(context);
    }

    public EmoteTextView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public EmoteTextView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public CharSequence a(CharSequence charSequence) {
        if (charSequence == null) {
            return PoiTypeDef.All;
        }
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(charSequence);
        Matcher matcher = Pattern.compile("([-])").matcher(charSequence);
        boolean z = false;
        while (matcher.find()) {
            z = true;
            spannableStringBuilder.setSpan(new ad(getContext(), getResources().getIdentifier("zemoji_" + a.m(matcher.group()).substring(2), "drawable", getContext().getPackageName()), (byte) 0), matcher.start(), matcher.end(), 33);
        }
        if (z) {
            charSequence = spannableStringBuilder;
        }
        return b(charSequence);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.text.Editable.append(java.lang.CharSequence, int, int):android.text.Editable}
     arg types: [java.lang.CharSequence, int, int]
     candidates:
      ClspMth{android.text.Editable.append(java.lang.CharSequence, int, int):java.lang.Appendable throws java.io.IOException}
      ClspMth{java.lang.Appendable.append(java.lang.CharSequence, int, int):java.lang.Appendable throws java.io.IOException}
      ClspMth{android.text.Editable.append(java.lang.CharSequence, int, int):android.text.Editable} */
    public void append(CharSequence charSequence, int i, int i2) {
        if (getText() instanceof Editable) {
            ((Editable) getText()).append(a(charSequence), i, i2);
        } else {
            super.append(a(charSequence), i, i2);
        }
    }

    public void setText(ao aoVar) {
        if (aoVar.f2723a) {
            this.f2627a = false;
            setText(aoVar.c);
            this.f2627a = true;
            return;
        }
        aoVar.b(a(aoVar.b));
        setText(aoVar);
    }

    public void setText(CharSequence charSequence, TextView.BufferType bufferType) {
        if (this.f2627a) {
            super.setText(a(charSequence), bufferType);
        } else {
            super.setText(charSequence, bufferType);
        }
    }
}
