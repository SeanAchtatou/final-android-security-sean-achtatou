package com.immomo.momo.android.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.ScrollView;

final class co extends ScrollView {

    /* renamed from: a  reason: collision with root package name */
    private float f2765a;
    private float b;
    private float c;
    private float d;

    public co(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public final void fling(int i) {
        super.fling(i);
    }

    public final boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case 0:
                this.b = 0.0f;
                this.f2765a = 0.0f;
                this.c = motionEvent.getX();
                this.d = motionEvent.getY();
                break;
            case 2:
                float x = motionEvent.getX();
                float y = motionEvent.getY();
                this.f2765a += Math.abs(x - this.c);
                this.b += Math.abs(y - this.d);
                this.c = x;
                this.d = y;
                if (this.f2765a > this.b) {
                    return false;
                }
                break;
        }
        return super.onInterceptTouchEvent(motionEvent);
    }

    /* access modifiers changed from: protected */
    public final void onScrollChanged(int i, int i2, int i3, int i4) {
        super.onScrollChanged(i, i2, i3, i4);
    }
}
