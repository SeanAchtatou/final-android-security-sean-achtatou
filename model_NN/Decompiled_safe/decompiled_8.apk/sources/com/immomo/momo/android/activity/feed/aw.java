package com.immomo.momo.android.activity.feed;

import android.net.Uri;
import java.util.ArrayList;

final class aw implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ PublishFeedActivity f1453a;
    private final /* synthetic */ Uri b;

    aw(PublishFeedActivity publishFeedActivity, Uri uri) {
        this.f1453a = publishFeedActivity;
        this.b = uri;
    }

    public final void run() {
        this.f1453a.x.a();
        this.f1453a.E.setSelected(true);
        this.f1453a.F.setSelected(false);
        PublishFeedActivity.l(this.f1453a);
        this.f1453a.D();
        this.f1453a.A();
        ArrayList arrayList = new ArrayList();
        arrayList.add(this.f1453a.a(this.b));
        this.f1453a.v = 2;
        this.f1453a.b(new bf(this.f1453a, this.f1453a.n(), arrayList));
    }
}
