package com.immomo.momo.android.activity.common;

import android.content.Intent;
import android.support.v4.b.a;
import com.immomo.momo.android.a.hp;
import com.immomo.momo.android.broadcast.d;
import com.immomo.momo.android.broadcast.t;

final class bk implements d {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ba f1104a;

    bk(ba baVar) {
        this.f1104a = baVar;
    }

    public final void a(Intent intent) {
        if (t.f2363a.equals(intent.getAction())) {
            if (this.f1104a.X == null) {
                ba baVar = this.f1104a;
                ba baVar2 = this.f1104a;
                ba baVar3 = this.f1104a;
                baVar.X = new bp(baVar2, ba.H());
            } else {
                this.f1104a.X.cancel(true);
                ba baVar4 = this.f1104a;
                ba baVar5 = this.f1104a;
                ba baVar6 = this.f1104a;
                baVar4.X = new bp(baVar5, ba.H());
            }
            this.f1104a.X.execute(new Object[0]);
        } else if (t.b.equals(intent.getAction())) {
            String stringExtra = intent.getStringExtra("disid");
            if (!a.a((CharSequence) stringExtra)) {
                this.f1104a.aa.post(new bl(this, stringExtra));
            }
        } else if (t.c.equals(intent.getAction())) {
            String stringExtra2 = intent.getStringExtra("disid");
            if (!a.a((CharSequence) stringExtra2)) {
                this.f1104a.aa.post(new bm(this, stringExtra2));
            }
        } else if (t.d.equals(intent.getAction())) {
            String str = intent.getExtras() != null ? (String) intent.getExtras().get("disid") : null;
            if (!a.a((CharSequence) str)) {
                this.f1104a.aa.post(new bn(this, this.f1104a.P.a(str, hp.f862a), str));
            }
        }
    }
}
