package com.immomo.momo.android.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.b.a;
import com.immomo.momo.android.activity.AboutTabsActivity;
import com.immomo.momo.g;
import com.immomo.momo.util.jni.Codec;

public class NewVersionReceiver extends BroadcastReceiver {

    /* renamed from: a  reason: collision with root package name */
    private static String f2344a = (String.valueOf(g.h()) + ".action.hasnewversion");

    public void onReceive(Context context, Intent intent) {
        Intent intent2;
        if (intent.getAction().equals(f2344a)) {
            String stringExtra = intent.getExtras() != null ? intent.getStringExtra("activity") : null;
            if (!a.a((CharSequence) stringExtra) && !a.a((CharSequence) intent.getStringExtra("key"))) {
                try {
                    Class<?> cls = Class.forName(Codec.b(stringExtra));
                    if (cls != null && cls.getName().contains("tivity")) {
                        intent2 = new Intent(context, cls);
                        intent2.setFlags(268435456);
                        context.startActivity(intent2);
                    }
                } catch (Exception e) {
                }
            }
            intent2 = new Intent(context, AboutTabsActivity.class);
            intent2.putExtra("showindex", 0);
            intent2.setFlags(268435456);
            context.startActivity(intent2);
        }
    }
}
