package com.immomo.momo.android.activity.message;

import android.graphics.Bitmap;
import android.net.Uri;
import android.support.v4.b.a;
import com.immomo.momo.MomoApplication;
import com.immomo.momo.android.b.z;
import com.immomo.momo.g;
import com.immomo.momo.service.af;
import com.immomo.momo.service.bean.Message;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.util.h;
import com.immomo.momo.util.m;
import java.io.File;
import java.io.IOException;

public final class ce {

    /* renamed from: a  reason: collision with root package name */
    private static ce f1965a;
    /* access modifiers changed from: private */
    public m b = new m("test_momo", "[ --- from MessageHelper ---]");

    private ce() {
    }

    public static ce a() {
        if (f1965a == null) {
            f1965a = new ce();
        }
        return f1965a;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.service.bean.Message.<init>(boolean, int):void
     arg types: [int, int]
     candidates:
      com.immomo.momo.service.bean.Message.<init>(int, boolean):void
      com.immomo.momo.service.bean.Message.<init>(boolean, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.h.a(java.lang.String, android.graphics.Bitmap, int, boolean):java.io.File
     arg types: [java.lang.String, android.graphics.Bitmap, int, int]
     candidates:
      com.immomo.momo.util.h.a(java.lang.String, java.lang.String, int, boolean):void
      com.immomo.momo.util.h.a(java.lang.String, android.graphics.Bitmap, int, boolean):java.io.File */
    public static Message a(File file, bf bfVar, String str, int i, int i2) {
        File a2;
        Message message = new Message(true, 1);
        message.remoteId = bfVar.h;
        message.status = 7;
        message.distance = bfVar.d();
        message.chatType = i;
        message.bubbleStyle = i2;
        if (i == 2) {
            message.groupId = str;
        } else if (i == 3) {
            message.discussId = str;
        }
        Bitmap l = a.l(file.getPath());
        if (l != null) {
            file.delete();
            a2 = h.a(message.msgId, l, 0, true);
            l.recycle();
        } else {
            a2 = h.a(message.msgId, 0);
            file.renameTo(a2);
        }
        message.fileName = Uri.fromFile(a2).toString();
        message.fileSize = a2.length();
        return message;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.service.bean.Message.<init>(boolean, int):void
     arg types: [int, int]
     candidates:
      com.immomo.momo.service.bean.Message.<init>(int, boolean):void
      com.immomo.momo.service.bean.Message.<init>(boolean, int):void */
    public static Message a(String str, bf bfVar, String str2, int i, int i2) {
        int i3 = 1;
        Message message = new Message(true, 0);
        message.setContent(str);
        message.distance = bfVar.d();
        message.remoteId = bfVar.h;
        message.bubbleStyle = i2;
        if (!g.q().b()) {
            i3 = 0;
        }
        message.bubbleStyle = i3;
        message.chatType = i;
        if (i == 2) {
            message.groupId = str2;
        } else if (i == 3) {
            message.discussId = str2;
        }
        return message;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.service.bean.Message.<init>(boolean, int):void
     arg types: [int, int]
     candidates:
      com.immomo.momo.service.bean.Message.<init>(int, boolean):void
      com.immomo.momo.service.bean.Message.<init>(boolean, int):void */
    public static Message b(String str, bf bfVar, String str2, int i, int i2) {
        Message message = new Message(true, 6);
        message.setContent(str);
        message.distance = bfVar.d();
        message.remoteId = bfVar.h;
        message.chatType = i;
        message.bubbleStyle = i2;
        if (i == 2) {
            message.groupId = str2;
        } else if (i == 3) {
            message.discussId = str2;
        }
        return message;
    }

    public static void b() {
        f1965a = null;
    }

    public static void b(Message message) {
        g.d().c().post(new cg(message));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.service.bean.Message.<init>(boolean, int):void
     arg types: [int, int]
     candidates:
      com.immomo.momo.service.bean.Message.<init>(int, boolean):void
      com.immomo.momo.service.bean.Message.<init>(boolean, int):void */
    public final Message a(String str, int i, bf bfVar, String str2, int i2, int i3) {
        Message message = null;
        try {
            File b2 = h.b(str);
            if (b2 != null && b2.exists()) {
                if (b2.length() < 6 || b2.length() > 5242880) {
                    b2.delete();
                    throw new RuntimeException("异常文件: 语音文件小于6B或者大于5M");
                }
                this.b.a((Object) ("sendAudioMessage, audioFile=" + b2));
                message = new Message(true, 4);
                message.msgId = str;
                message.remoteId = bfVar.h;
                message.status = 7;
                message.fileSize = b2.length();
                message.fileName = Uri.fromFile(b2).toString();
                message.audiotime = i;
                message.expandedName = Message.EXPAND_MESSAGE_AUDIO;
                message.distance = bfVar.d();
                message.chatType = i2;
                message.bubbleStyle = i3;
                if (i2 == 2) {
                    message.groupId = str2;
                } else if (i2 == 3) {
                    message.discussId = str2;
                }
            }
        } catch (IOException e) {
            this.b.a((Throwable) e);
        }
        return message;
    }

    public final void a(Message message) {
        this.b.a((Object) "resending meaasge");
        if (message.contentType == 1 || message.contentType == 6) {
            message.status = 7;
        } else if (message.contentType == 4) {
            message.status = 7;
        } else {
            message.status = 1;
        }
        af.c().a(message.msgId, message.status, message.chatType);
        g.d();
        MomoApplication.a(message);
    }

    public final void a(Message message, com.immomo.momo.android.c.g gVar, String str, int i, int i2) {
        message.status = 8;
        message.chatType = i;
        message.bubbleStyle = i2;
        if (i == 2) {
            message.groupId = str;
        } else if (i == 3) {
            message.discussId = str;
        }
        com.immomo.momo.android.b.m a2 = com.immomo.momo.android.b.m.a(message.msgId);
        a2.a(gVar);
        z.a(new cf(this, a2, message));
    }
}
