package com.immomo.momo.android.view;

import android.view.View;
import com.immomo.momo.service.bean.av;

final class cq implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ PublishSelectPhotoView f2766a;
    private final /* synthetic */ av b;

    cq(PublishSelectPhotoView publishSelectPhotoView, av avVar) {
        this.f2766a = publishSelectPhotoView;
        this.b = avVar;
    }

    public final void onClick(View view) {
        this.f2766a.a(this.b);
        this.f2766a.setData(this.f2766a.getDatalist());
    }
}
