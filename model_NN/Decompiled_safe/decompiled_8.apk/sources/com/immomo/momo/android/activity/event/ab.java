package com.immomo.momo.android.activity.event;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import com.immomo.momo.service.bean.aa;

final class ab implements AdapterView.OnItemClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ EventFeedsActivity f1347a;

    ab(EventFeedsActivity eventFeedsActivity) {
        this.f1347a = eventFeedsActivity;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        String str = ((aa) this.f1347a.j.get(i)).h;
        Intent intent = new Intent(this.f1347a, EventFeedProfileActivity.class);
        intent.putExtra("eventfeedid", str);
        intent.putExtra("eventid", this.f1347a.h);
        this.f1347a.startActivity(intent);
    }
}
