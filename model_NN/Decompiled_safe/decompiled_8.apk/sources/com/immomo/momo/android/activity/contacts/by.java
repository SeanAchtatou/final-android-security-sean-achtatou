package com.immomo.momo.android.activity.contacts;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import com.immomo.momo.android.a.hf;
import com.immomo.momo.android.activity.discuss.DiscussProfileActivity;
import com.immomo.momo.android.activity.group.GroupProfileActivity;

final class by implements AdapterView.OnItemClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ bk f1179a;

    by(bk bkVar) {
        this.f1179a = bkVar;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        if (i < this.f1179a.P.getCount()) {
            hf hfVar = (hf) this.f1179a.P.getItem(i);
            Intent intent = new Intent();
            if (hfVar.h == 0) {
                intent.setClass(this.f1179a.c(), GroupProfileActivity.class);
                intent.putExtra("tag", "local");
                intent.putExtra("gid", hfVar.e);
                this.f1179a.a(intent);
            } else if (hfVar.h == hf.f857a) {
                intent.setClass(this.f1179a.c(), DiscussProfileActivity.class);
                intent.putExtra("tag", "local");
                intent.putExtra("did", hfVar.e);
                this.f1179a.a(intent);
            }
        }
    }
}
