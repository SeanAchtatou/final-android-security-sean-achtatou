package com.immomo.momo.android.activity;

import android.support.v4.b.a;
import android.view.View;
import com.immomo.momo.R;

final class l implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ AddGroupActivity f1804a;

    l(AddGroupActivity addGroupActivity) {
        this.f1804a = addGroupActivity;
    }

    public final void onClick(View view) {
        String editable = this.f1804a.Q.getText().toString();
        if (a.a((CharSequence) editable)) {
            this.f1804a.d((int) R.string.find_empty_gid);
            return;
        }
        this.f1804a.L.a((Object) ("find gid :" + editable));
        this.f1804a.R = new m(this.f1804a, this.f1804a.c());
        this.f1804a.R.execute(new String[]{editable});
    }
}
