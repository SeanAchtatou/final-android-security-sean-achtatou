package com.immomo.momo.android.a;

import android.content.Context;
import android.support.v4.b.a;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.c;
import com.immomo.momo.protocol.a.p;
import com.immomo.momo.service.bean.i;
import com.immomo.momo.service.bean.j;
import java.util.Map;
import org.json.JSONObject;

final class ap extends d {

    /* renamed from: a  reason: collision with root package name */
    private j f723a = null;
    private i c = null;
    private /* synthetic */ ae d;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ap(ae aeVar, Context context, j jVar, i iVar) {
        super(context);
        this.d = aeVar;
        this.f723a = jVar;
        this.c = iVar;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        String b = c.a().b(String.valueOf(p.f2900a) + this.f723a.c(), (Map) null);
        this.f723a.d();
        this.c.b(true);
        this.d.d.a(this.c);
        return new JSONObject(b).optString("msg");
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.d.e.a(new v(this.d.e, this));
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        String str = (String) obj;
        if (this.f723a.b().startsWith("block")) {
            this.d.c(this.c);
        }
        this.d.notifyDataSetChanged();
        if (!a.a((CharSequence) str)) {
            a(str);
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.d.e.p();
    }
}
