package com.immomo.momo.android.view.a;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.b.a;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.g;
import com.immomo.momo.util.m;
import java.util.ArrayList;
import java.util.List;

public class n extends Dialog implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    protected List f2702a;
    private boolean b = true;
    private View c = null;
    private ViewGroup d = null;
    private View e = null;
    private View f = null;
    private boolean g = true;
    private boolean h = false;
    private boolean i = true;
    private m j = new m(this);

    public n(Context context) {
        super(context, 2131558452);
        super.setContentView((int) R.layout.common_dialog_generic);
        this.f2702a = new ArrayList();
        Button button = (Button) findViewById(R.id.dilaog_button1);
        button.setOnClickListener(this);
        this.f2702a.add(button);
        Button button2 = (Button) findViewById(R.id.dilaog_button2);
        button2.setOnClickListener(this);
        this.f2702a.add(button2);
        Button button3 = (Button) findViewById(R.id.dilaog_button3);
        button3.setOnClickListener(this);
        this.f2702a.add(button3);
        this.c = findViewById(R.id.dialog_layout_title);
        this.d = (ViewGroup) findViewById(R.id.dialog_layout_content);
        this.e = findViewById(R.id.dialog_layout_button);
        this.f = findViewById(R.id.dialog_title_divider);
        setCancelable(true);
        setCanceledOnTouchOutside(true);
        setTitle((int) R.string.dialog_title_alert);
    }

    public static n a(Context context, int i2, int i3, int i4, DialogInterface.OnClickListener onClickListener, DialogInterface.OnClickListener onClickListener2) {
        return a(context, context.getString(i2), g.a(i3), g.a(i4), onClickListener, onClickListener2);
    }

    public static n a(Context context, int i2, DialogInterface.OnClickListener onClickListener) {
        return a(context, i2, (int) R.string.dialog_btn_confim, (int) R.string.dialog_btn_cancel, onClickListener, (DialogInterface.OnClickListener) null);
    }

    public static n a(Context context, int i2, DialogInterface.OnClickListener onClickListener, DialogInterface.OnClickListener onClickListener2) {
        return a(context, g.a(i2), context.getString(R.string.dialog_btn_confim), context.getString(R.string.dialog_btn_cancel), onClickListener, onClickListener2);
    }

    public static n a(Context context, CharSequence charSequence, DialogInterface.OnClickListener onClickListener) {
        return a(context, charSequence, context.getString(R.string.dialog_btn_confim), context.getString(R.string.dialog_btn_cancel), onClickListener, (DialogInterface.OnClickListener) null);
    }

    public static n a(Context context, CharSequence charSequence, DialogInterface.OnClickListener onClickListener, DialogInterface.OnClickListener onClickListener2) {
        return a(context, charSequence, context.getString(R.string.dialog_btn_confim), context.getString(R.string.dialog_btn_cancel), onClickListener, onClickListener2);
    }

    public static n a(Context context, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, DialogInterface.OnClickListener onClickListener, DialogInterface.OnClickListener onClickListener2) {
        n nVar = new n(context);
        nVar.a(charSequence);
        nVar.a(0, charSequence2, onClickListener);
        nVar.a(1, charSequence3, onClickListener2);
        nVar.a();
        return nVar;
    }

    public static n a(Context context, CharSequence charSequence, String str, DialogInterface.OnClickListener onClickListener) {
        n nVar = new n(context);
        nVar.a(charSequence);
        nVar.a(0, str, onClickListener);
        return nVar;
    }

    public static n b(Context context, int i2, DialogInterface.OnClickListener onClickListener) {
        return b(context, context.getString(i2), onClickListener);
    }

    public static n b(Context context, CharSequence charSequence, DialogInterface.OnClickListener onClickListener) {
        n nVar = new n(context);
        nVar.a(charSequence);
        nVar.a(0, context.getString(R.string.dialog_btn_confim), onClickListener);
        return nVar;
    }

    public final Button a(int i2, int i3, DialogInterface.OnClickListener onClickListener) {
        return a(i2, getContext().getString(i3), onClickListener);
    }

    public final Button a(int i2, CharSequence charSequence, DialogInterface.OnClickListener onClickListener) {
        Button button = (Button) this.f2702a.get(i2);
        button.setText(charSequence);
        button.setVisibility(0);
        ((Button) this.f2702a.get(i2)).setTag(onClickListener);
        this.e.setVisibility(0);
        return button;
    }

    public final n a() {
        Button button = (Button) this.f2702a.get(0);
        button.setBackgroundResource(R.drawable.btn_default_popsubmit);
        button.setTextColor(g.c((int) R.color.blue));
        return this;
    }

    public final void a(int i2) {
        a(getContext().getString(i2));
    }

    public final void a(CharSequence charSequence) {
        ((TextView) findViewById(R.id.dialog_tv_message)).setText(charSequence);
    }

    public final void b() {
        this.b = false;
        if (this.c.getVisibility() == 0 && !this.c.isShown()) {
            this.f.setVisibility(8);
        }
    }

    public final void c() {
        this.i = false;
    }

    public void onClick(View view) {
        DialogInterface.OnClickListener onClickListener = (DialogInterface.OnClickListener) view.getTag();
        if (onClickListener != null) {
            int i2 = 0;
            while (true) {
                if (i2 < this.f2702a.size()) {
                    if (this.f2702a.get(i2) == view) {
                        break;
                    }
                    i2++;
                } else {
                    i2 = -1;
                    break;
                }
            }
            onClickListener.onClick(this, i2);
        }
        if (this.i) {
            dismiss();
        } else {
            this.i = true;
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        this.j.a((Object) "onTouchEvent");
        if (!this.g || !this.h || motionEvent.getAction() != 0) {
            return super.onTouchEvent(motionEvent);
        }
        cancel();
        return true;
    }

    public void setCancelable(boolean z) {
        super.setCancelable(z);
        this.g = z;
    }

    public void setCanceledOnTouchOutside(boolean z) {
        super.setCanceledOnTouchOutside(z);
        this.h = z;
    }

    public void setContentView(int i2) {
        setContentView(g.o().inflate(i2, (ViewGroup) null));
    }

    public void setContentView(View view) {
        if (this.d.getChildCount() > 0) {
            this.d.removeAllViews();
        }
        this.d.addView(view);
    }

    public void setContentView(View view, ViewGroup.LayoutParams layoutParams) {
        if (this.d.getChildCount() > 0) {
            this.d.removeAllViews();
        }
        this.d.addView(view, layoutParams);
    }

    public void setTitle(int i2) {
        setTitle(getContext().getString(i2));
    }

    public void setTitle(CharSequence charSequence) {
        if (charSequence == null || a.a((CharSequence) charSequence.toString())) {
            this.c.setVisibility(8);
            return;
        }
        ((TextView) findViewById(R.id.dialog_tv_title)).setText(charSequence);
        this.c.setVisibility(0);
        if (!this.b) {
            this.f.setVisibility(8);
        } else {
            this.f.setVisibility(0);
        }
    }
}
