package com.immomo.momo.android.a;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.b.a;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.CheckBox;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.OtherProfileActivity;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.util.j;
import com.immomo.momo.util.m;
import java.util.List;

public final class c extends a implements View.OnClickListener, Filterable {
    private Activity d = null;
    /* access modifiers changed from: private */
    public List e = null;
    /* access modifiers changed from: private */
    public List f = null;
    private AbsListView g = null;
    private m h = new m("MultiSelectFriendAdapter");
    /* access modifiers changed from: private */
    public Object i = new Object();
    private e j;
    private boolean k = false;
    private StringBuilder l;

    public c(Activity activity, List list, AbsListView absListView, boolean z) {
        super(activity, list);
        new d();
        this.d = activity;
        this.f = list;
        this.e = list;
        this.g = absListView;
        this.k = z;
        this.l = new StringBuilder();
    }

    public final boolean a(bf bfVar) {
        int length;
        int indexOf = this.l.toString().indexOf(bfVar.h);
        this.h.b((Object) ("proClick=========================:" + this.l.toString()));
        this.h.b((Object) ("index=:" + indexOf));
        if (indexOf >= 0) {
            String sb = this.l.toString();
            this.h.b((Object) ("sss:" + sb));
            if (indexOf == 0) {
                if (this.l == null) {
                    length = 0;
                } else {
                    String sb2 = this.l.toString();
                    length = PoiTypeDef.All.equals(sb2.trim()) ? 0 : sb2.split(",").length;
                }
                if (length > 1) {
                    this.l = new StringBuilder(sb.replaceFirst(String.valueOf(bfVar.h) + ",", PoiTypeDef.All));
                    return false;
                }
                String replaceFirst = sb.replaceFirst(bfVar.h, PoiTypeDef.All);
                this.h.b((Object) ("newvalue:" + replaceFirst));
                this.l = new StringBuilder(replaceFirst);
                return false;
            }
            this.l = new StringBuilder(sb.replaceFirst("," + bfVar.h, PoiTypeDef.All));
            return false;
        }
        if (a.a((CharSequence) this.l.toString())) {
            this.l.append(bfVar.h);
        } else {
            this.l.append(",");
            this.l.append(bfVar.h);
        }
        return true;
    }

    public final boolean b(bf bfVar) {
        return this.l.toString().indexOf(bfVar.h) >= 0;
    }

    /* renamed from: d */
    public final bf getItem(int i2) {
        if (this.f == null || this.f.size() <= 0) {
            return null;
        }
        return (bf) this.f.get(i2);
    }

    public final int getCount() {
        return this.f.size();
    }

    public final Filter getFilter() {
        if (this.j == null) {
            this.j = new e(this, (byte) 0);
        }
        return this.j;
    }

    public final long getItemId(int i2) {
        return (long) i2;
    }

    public final View getView(int i2, View view, ViewGroup viewGroup) {
        if (view == null) {
            f fVar = new f((byte) 0);
            view = LayoutInflater.from(this.b).inflate((int) R.layout.listitem_user_select, (ViewGroup) null);
            fVar.f811a = (ImageView) view.findViewById(R.id.userlist_item_iv_face);
            fVar.b = (TextView) view.findViewById(R.id.userlist_item_tv_name);
            fVar.c = (CheckBox) view.findViewById(16908289);
            view.setTag(R.id.tag_userlist_item, fVar);
            fVar.f811a.setOnClickListener(this);
        }
        bf d2 = getItem(i2);
        f fVar2 = (f) view.getTag(R.id.tag_userlist_item);
        fVar2.b.setText(d2.h().trim());
        if (this.k) {
            fVar2.c.setVisibility(4);
        } else {
            fVar2.c.setVisibility(0);
        }
        fVar2.c.setChecked(b(d2));
        j.a(d2, fVar2.f811a, this.g, 3);
        fVar2.f811a.setTag(R.id.tag_item_position, Integer.valueOf(i2));
        return view;
    }

    public final void onClick(View view) {
        int intValue = ((Integer) view.getTag(R.id.tag_item_position)).intValue();
        Intent intent = new Intent(d(), OtherProfileActivity.class);
        intent.putExtra("momoid", getItem(intValue).h);
        this.d.startActivity(intent);
    }
}
