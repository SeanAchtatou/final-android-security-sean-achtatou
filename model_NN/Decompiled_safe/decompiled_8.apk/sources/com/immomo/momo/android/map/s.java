package com.immomo.momo.android.map;

import android.location.Location;
import com.amap.mapapi.core.GeoPoint;

final class s implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ r f2513a;
    private final /* synthetic */ Location b;

    s(r rVar, Location location) {
        this.f2513a = rVar;
        this.b = location;
    }

    public final void run() {
        if (!this.f2513a.f2512a.isFinishing() && this.f2513a.f2512a.b != null) {
            this.f2513a.f2512a.b.dismiss();
            this.f2513a.f2512a.b = null;
        }
        this.f2513a.f2512a.i = this.b.getLatitude();
        this.f2513a.f2512a.j = this.b.getLongitude();
        this.f2513a.f2512a.c = new GeoPoint((int) (this.f2513a.f2512a.i * 1000000.0d), (int) (this.f2513a.f2512a.j * 1000000.0d));
        this.f2513a.f2512a.f.getController().animateTo(this.f2513a.f2512a.c);
        this.f2513a.f2512a.f.getController().setCenter(this.f2513a.f2512a.c);
        GeoAMapActivity.e(this.f2513a.f2512a);
    }
}
