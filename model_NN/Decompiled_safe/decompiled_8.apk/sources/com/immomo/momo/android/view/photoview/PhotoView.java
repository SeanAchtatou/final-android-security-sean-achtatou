package com.immomo.momo.android.view.photoview;

import android.content.Context;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import com.immomo.momo.android.plugin.cropimage.q;

public class PhotoView extends ImageView {

    /* renamed from: a  reason: collision with root package name */
    private final a f2816a;
    private ImageView.ScaleType b;

    public PhotoView(Context context) {
        this(context, null);
    }

    public PhotoView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public PhotoView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        super.setScaleType(ImageView.ScaleType.MATRIX);
        this.f2816a = new a(this);
        if (this.b != null) {
            setScaleType(this.b);
            this.b = null;
        }
    }

    public RectF getDisplayRect() {
        return this.f2816a.b();
    }

    public float getMaxScale() {
        return this.f2816a.f();
    }

    public float getMidScale() {
        return this.f2816a.e();
    }

    public float getMinScale() {
        return this.f2816a.d();
    }

    public float getScale() {
        return this.f2816a.g();
    }

    public ImageView.ScaleType getScaleType() {
        return this.f2816a.h();
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        this.f2816a.a();
        super.onDetachedFromWindow();
    }

    public void setAllowParentInterceptOnEdge(boolean z) {
        this.f2816a.a(z);
    }

    public void setImageDrawable(Drawable drawable) {
        super.setImageDrawable(drawable);
        if (this.f2816a != null) {
            this.f2816a.i();
        }
    }

    public void setImageResource(int i) {
        super.setImageResource(i);
        if (this.f2816a != null) {
            this.f2816a.i();
        }
    }

    public void setImageURI(Uri uri) {
        super.setImageURI(uri);
        if (this.f2816a != null) {
            this.f2816a.i();
        }
    }

    public void setMaxScale(float f) {
        this.f2816a.c(f);
    }

    public void setMidScale(float f) {
        this.f2816a.b(f);
    }

    public void setMinScale(float f) {
        this.f2816a.a(f);
    }

    public void setOnLongClickListener(View.OnLongClickListener onLongClickListener) {
        this.f2816a.a(onLongClickListener);
    }

    public void setOnMatrixChangeListener$158a2467(q qVar) {
        this.f2816a.a(qVar);
    }

    public void setOnPhotoTapListener$42d9b47d(q qVar) {
        this.f2816a.b(qVar);
    }

    public void setOnViewTapListener$63a93b04(q qVar) {
        this.f2816a.c(qVar);
    }

    public void setScaleType(ImageView.ScaleType scaleType) {
        if (this.f2816a != null) {
            this.f2816a.a(scaleType);
        } else {
            this.b = scaleType;
        }
    }

    public void setZoomable(boolean z) {
        this.f2816a.b(z);
    }
}
