package com.immomo.momo.android.activity.message;

import android.location.Location;
import com.immomo.momo.MomoApplication;
import com.immomo.momo.android.b.n;
import com.immomo.momo.android.b.z;
import com.immomo.momo.android.c.g;
import com.immomo.momo.service.bean.Message;

public final class x implements g {

    /* renamed from: a  reason: collision with root package name */
    private Message f1996a = null;
    private /* synthetic */ a b;

    public x(a aVar, Message message) {
        this.b = aVar;
        this.f1996a = message;
    }

    public final /* synthetic */ void a(Object obj) {
        Location location = ((n) obj).f2332a;
        if (!z.a(location)) {
            this.f1996a.status = 3;
            this.b.c(this.f1996a);
            ce.a();
            ce.b(this.f1996a);
            return;
        }
        new ah(this.b, this.b).execute(new Object[0]);
        this.f1996a.convertLat = location.getLatitude();
        this.f1996a.convertLng = location.getLongitude();
        this.f1996a.convertAcc = location.getAccuracy();
        this.f1996a.status = 1;
        this.b.d(this.f1996a);
        com.immomo.momo.g.d();
        MomoApplication.a(this.f1996a);
        this.b.Y();
    }
}
