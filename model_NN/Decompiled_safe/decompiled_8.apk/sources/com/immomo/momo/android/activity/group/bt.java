package com.immomo.momo.android.activity.group;

import android.content.Intent;
import android.net.Uri;
import com.immomo.momo.a;
import com.immomo.momo.android.view.a.al;
import java.io.File;

final class bt implements al {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ GroupFeedsActivity f1567a;

    bt(GroupFeedsActivity groupFeedsActivity) {
        this.f1567a = groupFeedsActivity;
    }

    public final void a(int i) {
        switch (i) {
            case 0:
                Intent intent = new Intent("android.intent.action.GET_CONTENT");
                intent.setType("image/*");
                this.f1567a.startActivityForResult(Intent.createChooser(intent, "本地图片"), 11);
                return;
            case 1:
                Intent intent2 = new Intent("android.media.action.IMAGE_CAPTURE");
                this.f1567a.h = new File(a.i(), new StringBuilder(String.valueOf(System.currentTimeMillis())).toString());
                intent2.putExtra("output", Uri.fromFile(this.f1567a.h));
                this.f1567a.startActivityForResult(intent2, 12);
                return;
            default:
                return;
        }
    }
}
