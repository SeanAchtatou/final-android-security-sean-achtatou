package com.immomo.momo.android.activity.feed;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import com.immomo.momo.R;
import com.immomo.momo.android.a.gk;
import com.immomo.momo.android.activity.lh;
import com.immomo.momo.android.view.HeaderLayout;
import com.immomo.momo.android.view.LoadingButton;
import com.immomo.momo.android.view.MomoRefreshListView;
import com.immomo.momo.android.view.bl;
import com.immomo.momo.android.view.bu;
import com.immomo.momo.android.view.cv;
import com.immomo.momo.g;
import com.immomo.momo.service.af;
import com.immomo.momo.service.ai;
import com.immomo.momo.service.bean.ae;
import com.immomo.momo.util.k;
import java.util.Date;
import java.util.List;

public class ad extends lh implements AdapterView.OnItemClickListener, bl, bu, cv {
    /* access modifiers changed from: private */
    public gk O = null;
    /* access modifiers changed from: private */
    public ai P;
    /* access modifiers changed from: private */
    public MomoRefreshListView Q = null;
    /* access modifiers changed from: private */
    public LoadingButton R = null;
    /* access modifiers changed from: private */
    public ae S;
    /* access modifiers changed from: private */
    public ae T = null;

    static /* synthetic */ ae e(ad adVar) {
        if (adVar.O == null || adVar.O.getCount() <= 0) {
            return null;
        }
        return (ae) adVar.O.getItem(adVar.O.getCount() - 1);
    }

    /* access modifiers changed from: protected */
    public final int B() {
        return R.layout.activity_friendsfeedlist;
    }

    public final void C() {
        this.Q = (MomoRefreshListView) c((int) R.id.lv_feed);
        this.Q.setEnableLoadMoreFoolter(true);
        this.R = this.Q.getFooterViewButton();
        this.R.setOnProcessListener(this);
        this.Q.setLastFlushTime(new Date());
        this.Q.setListPaddingBottom(-3);
        this.Q.a(g.o().inflate((int) R.layout.include_mycomment_listempty, (ViewGroup) null));
        this.Q.setOnPullToRefreshListener$42b903f6(this);
        this.Q.setOnCancelListener$135502(this);
        this.Q.setOnItemClickListener(this);
        this.Q.addHeaderView(g.o().inflate((int) R.layout.listitem_blank, (ViewGroup) null));
    }

    /* access modifiers changed from: protected */
    public final void E() {
        super.E();
        this.Q.o();
        if (this.R.d()) {
            this.R.e();
        }
    }

    public final void S() {
        super.S();
        if (this.O.isEmpty() || af.c().j() > 0) {
            this.Q.l();
        }
    }

    public final void a(Context context, HeaderLayout headerLayout) {
        headerLayout.setTitleText((int) R.string.feedtabs_comments);
    }

    public final void ae() {
        super.ae();
        new k("PI", "P541").e();
    }

    public final void ag() {
        super.ag();
        new k("PO", "P541").e();
    }

    public final void ai() {
        this.Q.i();
    }

    public final void aj() {
        List d = this.P.d();
        this.L.b((Object) ("!!!!!!!!!!!!!!!! comments count:" + d.size()));
        this.O = new gk(this, d, this.Q);
        this.Q.setAdapter((ListAdapter) this.O);
        if (this.O.getCount() < 20) {
            this.R.setVisibility(8);
        } else {
            this.R.setVisibility(0);
        }
        this.Q.setLastFlushTime(this.N.b("prf_time_my_comment"));
    }

    public final void b_() {
        this.Q.setLoadingVisible(true);
        a(new ae(this, g.c(), true));
    }

    /* access modifiers changed from: protected */
    public final void g(Bundle bundle) {
        this.P = new ai();
        aj();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.android.activity.feed.FeedProfileActivity.a(android.content.Context, java.lang.String, boolean):void
     arg types: [android.support.v4.app.g, java.lang.String, int]
     candidates:
      com.immomo.momo.android.activity.feed.FeedProfileActivity.a(com.immomo.momo.android.activity.feed.FeedProfileActivity, java.lang.String, boolean):void
      com.immomo.momo.android.activity.ao.a(android.support.v4.app.Fragment, android.content.Intent, int):void
      android.support.v4.app.g.a(java.lang.String, java.io.PrintWriter, android.view.View):void
      android.support.v4.app.g.a(android.support.v4.app.Fragment, android.content.Intent, int):void
      com.immomo.momo.android.activity.feed.FeedProfileActivity.a(android.content.Context, java.lang.String, boolean):void */
    public void onItemClick(AdapterView adapterView, View view, int i, long j) {
        FeedProfileActivity.a((Context) c(), ((ae) this.O.getItem(i)).h, false);
    }

    public final void p() {
        super.p();
    }

    public final void u() {
        this.L.b((Object) "+++++++++++++++ load more buttom onProcess");
        this.R.f();
        a(new ae(this, g.c(), false));
    }

    public final void v() {
        this.Q.n();
        this.R.e();
    }
}
