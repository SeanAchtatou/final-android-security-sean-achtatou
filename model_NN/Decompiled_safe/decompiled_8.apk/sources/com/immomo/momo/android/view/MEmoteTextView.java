package com.immomo.momo.android.view;

import android.content.Context;
import android.util.AttributeSet;

public class MEmoteTextView extends EmoteTextView {
    public MEmoteTextView(Context context) {
        super(context);
    }

    public MEmoteTextView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public MEmoteTextView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public CharSequence a(CharSequence charSequence) {
        return bp.a(super.a(charSequence));
    }
}
