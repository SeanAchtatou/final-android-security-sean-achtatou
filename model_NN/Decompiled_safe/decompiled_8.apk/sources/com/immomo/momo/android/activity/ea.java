package com.immomo.momo.android.activity;

import android.graphics.Bitmap;

final class ea implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ dz f1267a;
    private final /* synthetic */ Bitmap b;

    ea(dz dzVar, Bitmap bitmap) {
        this.f1267a = dzVar;
        this.b = bitmap;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.android.activity.FilterActivity.a(com.immomo.momo.android.activity.FilterActivity, boolean):void
     arg types: [com.immomo.momo.android.activity.FilterActivity, int]
     candidates:
      com.immomo.momo.android.activity.FilterActivity.a(com.immomo.momo.android.activity.FilterActivity, android.net.Uri):android.graphics.Bitmap
      com.immomo.momo.android.activity.FilterActivity.a(com.immomo.momo.android.activity.FilterActivity, int):void
      com.immomo.momo.android.activity.FilterActivity.a(com.immomo.momo.android.activity.FilterActivity, android.graphics.Bitmap):void
      com.immomo.momo.android.activity.FilterActivity.a(com.immomo.momo.android.activity.FilterActivity, com.immomo.momo.android.activity.eb):void
      com.immomo.momo.android.activity.FilterActivity.a(com.immomo.momo.android.activity.FilterActivity, boolean):void */
    public final void run() {
        if (this.b != null && !this.b.isRecycled()) {
            this.f1267a.f1266a.c.setImageBitmap(this.b);
        }
        this.f1267a.f1266a.a(true);
    }
}
