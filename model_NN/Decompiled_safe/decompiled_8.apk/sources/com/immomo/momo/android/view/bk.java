package com.immomo.momo.android.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.Log;

public class bk extends aa {

    /* renamed from: a  reason: collision with root package name */
    private Drawable f2745a;
    private int b;
    private Context c;

    @Deprecated
    public bk() {
        this((byte) 0);
    }

    private bk(byte b2) {
        this.c = null;
        this.f2745a = new BitmapDrawable((Bitmap) null);
        int intrinsicWidth = this.f2745a.getIntrinsicWidth();
        int intrinsicHeight = this.f2745a.getIntrinsicHeight();
        this.f2745a.setBounds(0, 0, intrinsicWidth <= 0 ? 0 : intrinsicWidth, intrinsicHeight <= 0 ? 0 : intrinsicHeight);
    }

    public bk(Context context, int i) {
        this.c = context;
        this.b = i;
    }

    public Drawable a() {
        Drawable drawable = null;
        if (this.f2745a != null) {
            return this.f2745a;
        }
        try {
            drawable = this.c.getResources().getDrawable(this.b);
            drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
            return drawable;
        } catch (Throwable th) {
            Log.e("sms", "Unable to find resource: " + this.b);
            return drawable;
        }
    }
}
