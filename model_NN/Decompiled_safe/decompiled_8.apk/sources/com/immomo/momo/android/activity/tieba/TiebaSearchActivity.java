package com.immomo.momo.android.activity.tieba;

import android.os.Bundle;
import android.support.v4.b.a;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.ah;
import com.immomo.momo.android.view.Cdo;
import com.immomo.momo.android.view.SearchHeaderLayout;
import com.immomo.momo.android.view.TiebaHotWordView;
import com.immomo.momo.android.view.TiebaSearchView;
import com.immomo.momo.android.view.f;
import java.util.Arrays;
import java.util.List;

public class TiebaSearchActivity extends ah implements Cdo, f {
    /* access modifiers changed from: private */
    public SearchHeaderLayout h;
    /* access modifiers changed from: private */
    public TiebaHotWordView i;
    private View j;
    /* access modifiers changed from: private */
    public fg k;
    /* access modifiers changed from: private */
    public TiebaSearchView l;
    /* access modifiers changed from: private */
    public View m;

    static /* synthetic */ void a(TiebaSearchActivity tiebaSearchActivity, List list) {
        if (list != null && tiebaSearchActivity.g != null) {
            tiebaSearchActivity.g.c("key_tieba_history", a.a(list, ","));
        }
    }

    static /* synthetic */ List c(TiebaSearchActivity tiebaSearchActivity) {
        if (tiebaSearchActivity.g == null || a.a((CharSequence) tiebaSearchActivity.g.b("key_tieba_history", PoiTypeDef.All))) {
            return null;
        }
        return Arrays.asList(a.b((String) tiebaSearchActivity.g.b("key_tieba_history", PoiTypeDef.All), ","));
    }

    /* access modifiers changed from: private */
    public void x() {
        View currentFocus = getCurrentFocus();
        if (currentFocus != null) {
            ((InputMethodManager) getApplicationContext().getSystemService("input_method")).hideSoftInputFromWindow(currentFocus.getWindowToken(), 2);
        }
    }

    public final void a(int i2, View view) {
        this.h.getSerachEditeText().setText((CharSequence) this.i.a(i2));
        this.h.getSerachEditeText().setSelection(((String) this.i.a(i2)).length());
        this.l.a((String) this.i.a(i2));
        this.l.setVisibility(0);
        this.j.setVisibility(8);
    }

    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        super.a(bundle);
        setContentView((int) R.layout.activity_tiebasearch);
        this.h = (SearchHeaderLayout) m();
        this.h.getTitileView().setVisibility(8);
        this.m = this.h.getClearButton();
        this.m.setVisibility(8);
        this.i = (TiebaHotWordView) findViewById(R.id.layout_hot_gridview);
        this.i.setItemViewWeith(70);
        this.i.setMinPading(5);
        this.h.getSerachEditeText().setHint("搜索或创建感兴趣的陌陌吧");
        this.l = (TiebaSearchView) findViewById(R.id.layout_search);
        this.j = findViewById(R.id.layout_hot);
        d();
        this.i.setOnMomoGridViewItemClickListener(this);
        this.h.setEditer(new fd(this));
        this.h.a(new fe(this));
        this.h.a(new ff(this));
        this.l.setTiebaSearchViewListener(this);
    }

    /* access modifiers changed from: protected */
    public final void d() {
        super.d();
        this.i.post(new fc(this));
        b(new fg(this, this));
    }

    public final void u() {
        this.j.setVisibility(8);
        this.l.setVisibility(0);
        this.h.b();
        x();
    }

    public final void v() {
        this.h.c();
    }

    public final void w() {
        this.j.setVisibility(0);
        this.l.setVisibility(8);
        this.h.c();
        this.h.getSerachEditeText().setText(PoiTypeDef.All);
    }
}
