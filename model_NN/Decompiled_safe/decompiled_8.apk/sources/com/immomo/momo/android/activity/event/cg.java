package com.immomo.momo.android.activity.event;

import android.view.View;
import android.widget.ImageView;
import com.immomo.momo.R;

final class cg {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ PublishEventFeedActivity f1400a;
    private View b;
    private ImageView c;
    private int d;
    private int e;
    /* access modifiers changed from: private */
    public boolean f = false;
    /* access modifiers changed from: private */
    public boolean g = false;
    private int h = -1;
    /* access modifiers changed from: private */
    public int i;

    public cg(PublishEventFeedActivity publishEventFeedActivity, View view, int i2) {
        boolean z = true;
        this.f1400a = publishEventFeedActivity;
        this.b = view;
        this.c = (ImageView) this.b.findViewById(R.id.signeditor_iv_icon);
        this.h = i2;
        switch (i2) {
            case 1:
                this.i = 11;
                this.g = publishEventFeedActivity.f.q();
                this.d = R.drawable.ic_publish_weibo_normal;
                this.e = R.drawable.ic_publish_weibo_selected;
                this.f = (!((Boolean) publishEventFeedActivity.g.b("publishfeed_sync_weibo", false)).booleanValue() || !this.g) ? false : z;
                break;
            case 2:
                this.i = 13;
                this.g = publishEventFeedActivity.f.at;
                this.d = R.drawable.ic_publish_tweibo_normal;
                this.e = R.drawable.ic_publish_tweibo_selected;
                this.f = (!((Boolean) publishEventFeedActivity.g.b("publishfeed_sync_tx", false)).booleanValue() || !this.g) ? false : z;
                break;
            case 3:
                this.i = 12;
                this.g = publishEventFeedActivity.f.ar;
                this.d = R.drawable.ic_publish_renren_normal;
                this.e = R.drawable.ic_publish_renren_selected;
                this.f = ((Boolean) publishEventFeedActivity.g.b("publishfeed_sync_renren", false)).booleanValue() && this.g;
                break;
            case 5:
                this.d = R.drawable.ic_publish_feed_normal;
                this.e = R.drawable.ic_publish_feed_selected;
                this.f = ((Boolean) publishEventFeedActivity.g.b("publishfeed_sync_feed", false)).booleanValue();
                break;
        }
        a(this.f);
        this.b.setOnClickListener(new ch(this, i2));
    }

    public final void a(boolean z) {
        this.f = z;
        if (z) {
            this.c.setImageResource(this.e);
        } else {
            this.c.setImageResource(this.d);
        }
    }

    public final boolean a() {
        return this.f;
    }

    public final void b() {
        if (this.g) {
            switch (this.h) {
                case 1:
                    this.f1400a.g.c("publishfeed_sync_weibo", Boolean.valueOf(this.f));
                    break;
                case 2:
                    this.f1400a.g.c("publishfeed_sync_tx", Boolean.valueOf(this.f));
                    break;
                case 3:
                    this.f1400a.g.c("publishfeed_sync_renren", Boolean.valueOf(this.f));
                    break;
            }
        }
        this.f1400a.g.c("publishfeed_sync_feed", Boolean.valueOf(this.f));
    }
}
