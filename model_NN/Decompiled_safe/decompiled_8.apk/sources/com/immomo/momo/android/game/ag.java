package com.immomo.momo.android.game;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.m;

final class ag extends d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ac f2403a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ag(ac acVar, Context context) {
        super(context);
        this.f2403a = acVar;
    }

    /* access modifiers changed from: protected */
    public final Object a(Object... objArr) {
        m.a().b(this.f2403a.Q, this.f2403a.R, this.f2403a.O, this.f2403a.S);
        this.f2403a.S.i = true;
        return null;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.f2403a.a(new v(this.f2403a.c(), "正在获取支付信息...", this));
    }

    /* access modifiers changed from: protected */
    public final void a(Object obj) {
        this.f2403a.O();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.f2403a.N();
    }
}
