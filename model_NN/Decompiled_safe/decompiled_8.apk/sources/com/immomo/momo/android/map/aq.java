package com.immomo.momo.android.map;

import android.support.v4.b.a;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.android.b.z;

final class aq implements TextWatcher {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public /* synthetic */ SelectSiteAMapActivity f2469a;

    aq(SelectSiteAMapActivity selectSiteAMapActivity) {
        this.f2469a = selectSiteAMapActivity;
    }

    public final void afterTextChanged(Editable editable) {
    }

    public final void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public final void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        if (!this.f2469a.s) {
            if (!a.a((CharSequence) charSequence.toString().trim())) {
                this.f2469a.q.b();
                this.f2469a.b.removeFooterView(this.f2469a.l);
                ((TextView) this.f2469a.l.findViewById(R.id.textview)).setText("添加地点'" + charSequence.toString().trim() + "' ");
                this.f2469a.l.findViewById(R.id.layout_footer).setOnClickListener(new ar(this, charSequence));
                if (z.a(this.f2469a.n)) {
                    this.f2469a.b.addFooterView(this.f2469a.l);
                    this.f2469a.l.requestFocus();
                    return;
                }
                this.f2469a.b.removeFooterView(this.f2469a.l);
            } else if (this.f2469a.q.getCount() > 0) {
                this.f2469a.b.removeFooterView(this.f2469a.l);
                this.f2469a.q.b();
            }
        } else if (!charSequence.toString().trim().equals(this.f2469a.h)) {
            if (this.f2469a.r != null && !this.f2469a.r.isCancelled()) {
                this.f2469a.r.cancel(true);
                this.f2469a.r = (az) null;
            }
            if (this.f2469a.y) {
                this.f2469a.h = charSequence.toString().trim();
                this.f2469a.u = 0;
                this.f2469a.v.setText(this.f2469a.d());
                this.f2469a.r = new az(this.f2469a, this.f2469a, this.f2469a.h);
                this.f2469a.r.execute(new Object[0]);
            }
        }
    }
}
