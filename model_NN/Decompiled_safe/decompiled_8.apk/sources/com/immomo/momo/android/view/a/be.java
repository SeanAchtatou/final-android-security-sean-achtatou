package com.immomo.momo.android.view.a;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.protocol.a.v;
import java.util.List;

final class be extends d {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public /* synthetic */ az f2685a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public be(az azVar, Context context) {
        super(context);
        this.f2685a = azVar;
        if (azVar.l != null && !azVar.l.isCancelled()) {
            azVar.l.cancel(true);
        }
        azVar.l = this;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        return v.a().c();
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.f2685a.a(this.f2685a.d);
        this.f2685a.g.setVisibility(4);
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        super.a(exc);
        az azVar = this.f2685a;
        az.b(this.f2685a.d);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        az azVar = this.f2685a;
        az.b(this.f2685a.d);
        this.f2685a.g.setVisibility(0);
        this.f2685a.g.post(new bf(this, (List) obj));
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.f2685a.l = (be) null;
    }
}
