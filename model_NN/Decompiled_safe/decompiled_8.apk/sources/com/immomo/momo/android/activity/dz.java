package com.immomo.momo.android.activity;

import android.graphics.Bitmap;
import com.immomo.momo.android.c.g;
import com.immomo.momo.util.i;

final class dz implements g {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ FilterActivity f1266a;
    private final /* synthetic */ eb b;

    dz(FilterActivity filterActivity, eb ebVar) {
        this.f1266a = filterActivity;
        this.b = ebVar;
    }

    public final /* synthetic */ void a(Object obj) {
        Bitmap bitmap = (Bitmap) obj;
        if (i.a(this.b.d) == null && bitmap != null) {
            i.a(this.b.d, bitmap);
        }
        this.f1266a.f922a.post(new ea(this, bitmap));
    }
}
