package com.immomo.momo.android.b;

import android.location.Location;
import com.amap.mapapi.location.LocationManagerProxy;
import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;

final class j implements BDLocationListener {

    /* renamed from: a  reason: collision with root package name */
    private p f2328a;
    private /* synthetic */ h b;

    public j(h hVar, p pVar) {
        this.b = hVar;
        this.f2328a = pVar;
    }

    public final void onReceiveLocation(BDLocation bDLocation) {
        if (bDLocation != null) {
            this.b.b.a((Object) ("baidu receive a location errcode: " + bDLocation.getLocType()));
        }
        if (bDLocation != null && bDLocation.getLocType() == 61) {
            this.b.b.a((Object) ("baidu gps location succeed: " + bDLocation.getLatitude() + ", " + bDLocation.getLongitude() + ", " + bDLocation.getRadius() + ", " + bDLocation.getLocType()));
            Location location = new Location(LocationManagerProxy.GPS_PROVIDER);
            location.setLatitude(bDLocation.getLatitude());
            location.setLongitude(bDLocation.getLongitude());
            location.setAccuracy(bDLocation.getRadius());
            if (this.f2328a != null) {
                this.f2328a.a(location, 1, 100, 201);
            }
        }
    }

    public final void onReceivePoi(BDLocation bDLocation) {
    }
}
