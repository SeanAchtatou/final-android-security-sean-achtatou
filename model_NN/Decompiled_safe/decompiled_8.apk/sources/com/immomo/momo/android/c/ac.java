package com.immomo.momo.android.c;

import android.content.Context;
import com.immomo.momo.R;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.n;
import com.immomo.momo.util.ao;

public final class ac extends d {

    /* renamed from: a  reason: collision with root package name */
    private v f2370a = null;

    public ac(Context context) {
        super(context);
        this.f2370a = new v(context);
        this.f2370a.setCancelable(true);
        this.f2370a.setOnCancelListener(new ad(this));
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        String[] strArr = (String[]) objArr;
        n.a().d(strArr[0], strArr[1]);
        return true;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
        if (this.f2370a != null) {
            this.f2370a.a("请求提交中");
            this.f2370a.show();
        }
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        super.a(exc);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        Boolean bool = (Boolean) obj;
        super.a(bool);
        if (bool.booleanValue()) {
            ao.g(R.string.report_result_success);
        } else {
            ao.g(R.string.report_result_failed);
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
        super.b();
        if (this.f2370a != null) {
            this.f2370a.dismiss();
            this.f2370a = null;
        }
    }
}
