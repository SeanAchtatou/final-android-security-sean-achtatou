package com.immomo.momo.android.activity.maintab;

import android.location.Location;
import com.immomo.momo.R;
import com.immomo.momo.android.b.p;
import com.immomo.momo.android.b.z;
import com.immomo.momo.util.ao;

final class ay extends p {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ aq f1848a;

    ay(aq aqVar) {
        this.f1848a = aqVar;
    }

    public final void a(Location location, int i, int i2, int i3) {
        if (this.b) {
            if (z.a(location)) {
                this.f1848a.M.S = location.getLatitude();
                this.f1848a.M.T = location.getLongitude();
                this.f1848a.M.U = (double) location.getAccuracy();
                this.f1848a.M.aH = i;
                this.f1848a.M.aI = i3;
                this.f1848a.M.a(System.currentTimeMillis());
                this.f1848a.S.a(this.f1848a.M);
                this.f1848a.ad.post(new az(this));
                return;
            }
            this.f1848a.al();
            if (i2 == 104) {
                ao.g(R.string.errormsg_network_unfind);
            } else if (i2 == 105) {
                this.f1848a.ad.post(new ba(this.f1848a));
            } else {
                ao.g(R.string.errormsg_location_nearby_failed);
            }
        }
    }
}
