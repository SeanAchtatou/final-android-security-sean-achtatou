package com.immomo.momo.android.activity.account;

import android.support.v4.b.a;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.util.k;
import com.immomo.momo.util.m;
import java.util.Calendar;
import java.util.Date;

public final class ao extends ab implements DatePicker.OnDateChangedListener {

    /* renamed from: a  reason: collision with root package name */
    private bf f943a = null;
    private m b = null;
    private DatePicker c = null;
    private TextView d = null;
    private TextView e = null;
    private Date f = null;
    private Date g = null;
    private Date h = a.i("19900101");

    public ao(bf bfVar, View view) {
        super(view);
        this.f943a = bfVar;
        this.b = new m("RegisterStep1");
        this.c = (DatePicker) a((int) R.id.rg_timepicker_birthday);
        this.d = (TextView) a((int) R.id.rg_tv_conste);
        this.e = (TextView) a((int) R.id.rg_tv_age);
        if (g.a()) {
            this.c.setLayerType(1, null);
        }
        try {
            if (!a.a((CharSequence) this.f943a.J)) {
                this.h = a.h(this.f943a.J);
            }
        } catch (Exception e2) {
        }
        Calendar instance = Calendar.getInstance();
        Calendar instance2 = Calendar.getInstance();
        instance2.set(1, instance.get(1) - 12);
        this.g = instance2.getTime();
        this.b.a((Object) ("minDate:" + a.e(this.g)));
        instance2.set(1, instance.get(1) - 100);
        this.f = instance2.getTime();
        this.b.a((Object) ("maxDate:" + a.e(this.f)));
        Calendar instance3 = Calendar.getInstance();
        instance3.setTime(this.h);
        a(instance3);
        this.c.init(instance3.get(1), instance3.get(2), instance3.get(5), this);
    }

    private void a(Calendar calendar) {
        this.b.a((Object) ("flushBrithday:" + a.e(calendar.getTime())));
        this.d.setText(a.a(calendar.get(2) + 1, calendar.get(5)));
        this.e.setText(new StringBuilder(String.valueOf(a.h(calendar.getTime()))).toString());
        this.h = calendar.getTime();
    }

    public final boolean a() {
        Calendar instance = Calendar.getInstance();
        instance.set(this.c.getYear(), this.c.getMonth(), this.c.getDayOfMonth());
        a(instance);
        if (this.h.after(this.g) || this.h.before(this.f)) {
            com.immomo.momo.util.ao.b(String.format(g.a((int) R.string.reg_age_range), 12, 100));
            return false;
        }
        this.f943a.J = a.d(this.h);
        return true;
    }

    public final void e() {
        new k("PI", "P125").e();
    }

    public final void f() {
        new k("PO", "P125").e();
    }

    public final void onDateChanged(DatePicker datePicker, int i, int i2, int i3) {
        Calendar instance = Calendar.getInstance();
        instance.set(i, i2, i3);
        this.b.a((Object) ("change date:" + a.e(instance.getTime())));
        this.b.a((Object) ("minDate:" + this.g + ", maxDate:" + this.f));
        if (instance.getTime().after(this.g) || instance.getTime().before(this.f)) {
            instance.setTime(this.h);
            datePicker.init(instance.get(1), instance.get(2), instance.get(5), this);
            return;
        }
        a(instance);
    }
}
