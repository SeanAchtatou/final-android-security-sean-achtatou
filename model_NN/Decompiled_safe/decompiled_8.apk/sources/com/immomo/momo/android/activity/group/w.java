package com.immomo.momo.android.activity.group;

import android.content.Intent;
import android.support.v4.b.a;
import android.view.View;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.ImageBrowserActivity;
import com.immomo.momo.android.activity.jt;
import com.immomo.momo.android.view.a.o;
import com.immomo.momo.service.bean.au;
import java.util.ArrayList;

final class w implements View.OnClickListener {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public /* synthetic */ EditGroupProfileActivity f1696a;

    w(EditGroupProfileActivity editGroupProfileActivity) {
        this.f1696a = editGroupProfileActivity;
    }

    public final void onClick(View view) {
        this.f1696a.e.a((Object) "avatar cover clickded");
        jt jtVar = (jt) view.getTag();
        if (jtVar == null) {
            return;
        }
        if (((au) jtVar.f1774a).d) {
            o oVar = new o(this.f1696a, (int) R.array.editprofile_add_photo);
            oVar.setTitle((int) R.string.dialog_title_add_photo);
            oVar.a(new x(this));
            oVar.show();
            return;
        }
        ArrayList arrayList = new ArrayList();
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.f1696a.h.size()) {
                break;
            }
            au auVar = (au) this.f1696a.h.get(i2);
            if (!auVar.d) {
                arrayList.add(auVar.b);
            }
            i = i2 + 1;
        }
        if (arrayList.size() > 0) {
            String a2 = a.a(arrayList, "<!>");
            this.f1696a.e.a((Object) ("result = " + a2));
            String[] b = a.b(a2, "<!>");
            this.f1696a.e.a((Object) ("arrs length = " + b.length));
            Intent intent = new Intent(this.f1696a, ImageBrowserActivity.class);
            intent.putExtra("array", b);
            intent.putExtra("imagetype", "avator");
            intent.putExtra("index", jtVar.b);
            this.f1696a.startActivity(intent);
        }
    }
}
