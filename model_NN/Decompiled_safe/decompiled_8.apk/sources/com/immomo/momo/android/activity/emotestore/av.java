package com.immomo.momo.android.activity.emotestore;

import android.content.Context;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.ListAdapter;
import com.immomo.momo.R;
import com.immomo.momo.android.a.gh;
import com.immomo.momo.android.activity.lh;
import com.immomo.momo.android.broadcast.g;
import com.immomo.momo.android.c.u;
import com.immomo.momo.android.view.HeaderLayout;
import com.immomo.momo.android.view.bi;
import com.immomo.momo.android.view.dragsort.DragSortListView;
import com.immomo.momo.service.o;
import com.immomo.momo.util.k;
import java.util.ArrayList;
import java.util.List;

public class av extends lh {
    DragSortListView O = null;
    o P = new o();
    gh Q = null;
    be R = null;
    bi S;
    List T = null;
    List U = null;
    private g V = null;

    /* access modifiers changed from: protected */
    public final int B() {
        return R.layout.activity_emotestore_mine;
    }

    public final void C() {
        this.O = (DragSortListView) c((int) R.id.listview);
        this.O.setCacheColorHint(d().getColor(R.color.background_undercard));
        this.O.setLastFlushTime(this.N.b("mineem_reflush"));
        this.O.setEnableLoadMoreFoolter(false);
        this.O.setCompleteScrollTop(false);
        this.S = new bi(com.immomo.momo.g.c()).a((int) R.drawable.ic_topbar_addfeed).a("编辑");
        this.O.setListPaddingBottom(-3);
    }

    /* access modifiers changed from: protected */
    public final void E() {
        super.E();
        ViewParent parent = this.S.getParent();
        if (parent != null) {
            ((ViewGroup) parent).removeView(this.S);
        }
        this.S.a((int) R.drawable.ic_topbar_addfeed).a("编辑");
    }

    public final void a(Context context, HeaderLayout headerLayout) {
        headerLayout.a(this.S, null);
    }

    public final void ae() {
        new k("PO", "P943").e();
    }

    public final void ag() {
        new k("PI", "P943").e();
    }

    public final void ai() {
        this.O.i();
    }

    public final void aj() {
        this.T = new ArrayList(this.P.e());
        this.U = new ArrayList(this.P.d());
        this.Q = new gh(c(), this.T, this.U, this.O);
        this.O.setAdapter((ListAdapter) this.Q);
        if (com.immomo.momo.g.f()) {
            u.b().execute(new bd(this));
        }
        a(new be(this, c()));
    }

    public final boolean b(int i, KeyEvent keyEvent) {
        return (i != 4 || !this.Q.e()) ? super.b(i, keyEvent) : this.S.performClick();
    }

    /* access modifiers changed from: protected */
    public final void g(Bundle bundle) {
        this.O.setOnCancelListener$135502(new ax(this));
        this.O.setOnPullToRefreshListener$42b903f6(new ay(this));
        this.O.setDropListener(new az(this));
        this.O.setCanDropListener(new ba());
        this.O.setOnItemClickListener(new bb(this));
        this.S.setOnClickListener(new bc(this));
        aj();
        this.V = new g(c(), g.c, g.f2350a, g.b);
        this.V.a(new aw(this));
    }

    public final void m() {
        super.m();
        if (this.T.size() == this.P.e().size() && this.U.size() == this.P.d().size()) {
            this.Q.notifyDataSetChanged();
        } else if (!this.Q.e()) {
            this.U.clear();
            this.T.clear();
            this.U.addAll(this.P.d());
            this.T.addAll(this.P.e());
        }
    }

    public final void p() {
        super.p();
        a(this.V);
    }
}
