package com.immomo.momo.android.activity.plugin;

import android.content.Context;
import com.immomo.momo.R;
import com.immomo.momo.a.a;
import com.immomo.momo.android.c.d;
import com.immomo.momo.plugin.f.b;
import com.immomo.momo.util.ao;

final class bz extends d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ TxWeiboActivity f2080a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public bz(TxWeiboActivity txWeiboActivity, Context context) {
        super(context);
        this.f2080a = txWeiboActivity;
        if (txWeiboActivity.G != null) {
            txWeiboActivity.G.cancel(true);
        }
        txWeiboActivity.G = this;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        return com.immomo.momo.protocol.a.d.d(this.f2080a.j, this.f2080a.k);
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        this.f2080a.findViewById(R.id.process_layout_root).setVisibility(8);
        if (exc instanceof a) {
            super.a(exc);
            return;
        }
        this.b.a((Throwable) exc);
        ao.g(R.string.plus_error_profile);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        b bVar = (b) obj;
        super.a(bVar);
        if (bVar != null) {
            this.f2080a.m = bVar;
            this.f2080a.v();
        }
    }
}
