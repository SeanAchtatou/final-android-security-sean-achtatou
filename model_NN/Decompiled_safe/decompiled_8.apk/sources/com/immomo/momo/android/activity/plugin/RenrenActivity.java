package com.immomo.momo.android.activity.plugin;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.b.a;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.a.hh;
import com.immomo.momo.android.activity.ah;
import com.immomo.momo.android.view.HeaderLayout;
import com.immomo.momo.g;
import com.immomo.momo.plugin.d.c;

public class RenrenActivity extends ah {
    /* access modifiers changed from: private */
    public ImageView h = null;
    private TextView i = null;
    private TextView j = null;
    private ImageView k = null;
    private TextView l = null;
    private View m = null;
    private TextView n = null;
    private View o = null;
    private TextView p = null;
    private View q = null;
    private TextView r = null;
    private View s = null;
    /* access modifiers changed from: private */
    public c t = null;
    private ListView u = null;
    private hh v;
    /* access modifiers changed from: private */
    public Handler w = new Handler();
    /* access modifiers changed from: private */
    public String x;
    /* access modifiers changed from: private */
    public String y;
    private AsyncTask z = null;

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0087  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x00b4  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00ca  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x011d  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x013c  */
    /* JADX WARNING: Removed duplicated region for block: B:42:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void u() {
        /*
            r5 = this;
            r3 = 0
            r4 = 8
            com.immomo.momo.plugin.d.c r0 = r5.t
            if (r0 != 0) goto L_0x0008
        L_0x0007:
            return
        L_0x0008:
            com.immomo.momo.util.m r0 = r5.e
            com.immomo.momo.plugin.d.c r1 = r5.t
            r0.a(r1)
            android.widget.TextView r0 = r5.i
            com.immomo.momo.plugin.d.c r1 = r5.t
            java.lang.String r1 = r1.f2870a
            r0.setText(r1)
            android.widget.TextView r0 = r5.j
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            com.immomo.momo.plugin.d.c r2 = r5.t
            int r2 = r2.i
            java.lang.String r2 = java.lang.String.valueOf(r2)
            r1.<init>(r2)
            java.lang.String r1 = r1.toString()
            r0.setText(r1)
            android.widget.TextView r0 = r5.l
            com.immomo.momo.plugin.d.c r1 = r5.t
            java.lang.String r1 = r1.e
            r0.setText(r1)
            com.immomo.momo.plugin.d.c r0 = r5.t
            java.lang.String[] r0 = r0.f
            if (r0 == 0) goto L_0x0128
            com.immomo.momo.plugin.d.c r0 = r5.t
            java.lang.String[] r0 = r0.f
            int r0 = r0.length
            if (r0 <= 0) goto L_0x0128
            java.lang.String r0 = ""
            com.immomo.momo.plugin.d.c r1 = r5.t
            java.lang.String[] r1 = r1.f
            java.lang.String r2 = ","
            java.lang.String r1 = android.support.v4.b.a.a(r1, r2)
            java.lang.String r2 = r1.trim()
            boolean r0 = r0.equals(r2)
            if (r0 != 0) goto L_0x0128
            android.view.View r0 = r5.m
            r0.setVisibility(r3)
            android.widget.TextView r0 = r5.n
            r0.setText(r1)
        L_0x0064:
            com.immomo.momo.plugin.d.c r0 = r5.t
            java.lang.String[] r0 = r0.g
            if (r0 == 0) goto L_0x012f
            com.immomo.momo.plugin.d.c r0 = r5.t
            java.lang.String[] r0 = r0.g
            int r0 = r0.length
            if (r0 <= 0) goto L_0x012f
            java.lang.String r0 = ""
            com.immomo.momo.plugin.d.c r1 = r5.t
            java.lang.String[] r1 = r1.g
            java.lang.String r2 = ","
            java.lang.String r1 = android.support.v4.b.a.a(r1, r2)
            java.lang.String r2 = r1.trim()
            boolean r0 = r0.equals(r2)
            if (r0 != 0) goto L_0x012f
            android.view.View r0 = r5.o
            r0.setVisibility(r3)
            android.widget.TextView r0 = r5.p
            r0.setText(r1)
        L_0x0091:
            com.immomo.momo.plugin.d.c r0 = r5.t
            java.lang.String[] r0 = r0.h
            if (r0 == 0) goto L_0x0136
            com.immomo.momo.plugin.d.c r0 = r5.t
            java.lang.String[] r0 = r0.h
            int r0 = r0.length
            if (r0 <= 0) goto L_0x0136
            java.lang.String r0 = ""
            com.immomo.momo.plugin.d.c r1 = r5.t
            java.lang.String[] r1 = r1.h
            java.lang.String r2 = ","
            java.lang.String r1 = android.support.v4.b.a.a(r1, r2)
            java.lang.String r2 = r1.trim()
            boolean r0 = r0.equals(r2)
            if (r0 != 0) goto L_0x0136
            android.view.View r0 = r5.q
            r0.setVisibility(r3)
            android.widget.TextView r0 = r5.r
            r0.setText(r1)
        L_0x00be:
            java.lang.String r0 = "M"
            com.immomo.momo.plugin.d.c r1 = r5.t
            java.lang.String r1 = r1.b
            boolean r0 = r0.equalsIgnoreCase(r1)
            if (r0 == 0) goto L_0x013c
            android.widget.ImageView r0 = r5.k
            r1 = 2130837635(0x7f020083, float:1.728023E38)
            r0.setBackgroundResource(r1)
            android.widget.ImageView r0 = r5.k
            r1 = 2130838284(0x7f02030c, float:1.7281546E38)
            r0.setImageResource(r1)
        L_0x00da:
            com.immomo.momo.plugin.d.c r0 = r5.t
            java.lang.String r0 = r0.d
            boolean r0 = android.support.v4.b.a.a(r0)
            if (r0 != 0) goto L_0x0112
            com.immomo.momo.plugin.d.c r0 = r5.t
            java.lang.String r0 = r0.d
            java.lang.String r1 = "/"
            int r0 = r0.indexOf(r1)
            if (r0 <= 0) goto L_0x0112
            com.immomo.momo.plugin.d.c r0 = r5.t
            java.lang.String r0 = r0.d
            java.lang.String r0 = android.support.v4.b.a.n(r0)
            com.immomo.momo.android.c.r r1 = new com.immomo.momo.android.c.r
            com.immomo.momo.android.activity.plugin.bk r2 = new com.immomo.momo.android.activity.plugin.bk
            r2.<init>(r5)
            r3 = 0
            r1.<init>(r0, r2, r4, r3)
            com.immomo.momo.plugin.d.c r0 = r5.t
            java.lang.String r0 = r0.d
            r1.a(r0)
            java.lang.Thread r0 = new java.lang.Thread
            r0.<init>(r1)
            r0.start()
        L_0x0112:
            com.immomo.momo.android.a.hh r0 = r5.v
            r0.b()
            com.immomo.momo.plugin.d.c r0 = r5.t
            java.util.List r0 = r0.k
            if (r0 == 0) goto L_0x0007
            com.immomo.momo.android.a.hh r0 = r5.v
            com.immomo.momo.plugin.d.c r1 = r5.t
            java.util.List r1 = r1.k
            r0.b(r1)
            goto L_0x0007
        L_0x0128:
            android.view.View r0 = r5.m
            r0.setVisibility(r4)
            goto L_0x0064
        L_0x012f:
            android.view.View r0 = r5.o
            r0.setVisibility(r4)
            goto L_0x0091
        L_0x0136:
            android.view.View r0 = r5.q
            r0.setVisibility(r4)
            goto L_0x00be
        L_0x013c:
            java.lang.String r0 = "F"
            com.immomo.momo.plugin.d.c r1 = r5.t
            java.lang.String r1 = r1.b
            boolean r0 = r0.equalsIgnoreCase(r1)
            if (r0 == 0) goto L_0x0159
            android.widget.ImageView r0 = r5.k
            r1 = 2130837633(0x7f020081, float:1.7280226E38)
            r0.setBackgroundResource(r1)
            android.widget.ImageView r0 = r5.k
            r1 = 2130838280(0x7f020308, float:1.7281538E38)
            r0.setImageResource(r1)
            goto L_0x00da
        L_0x0159:
            android.widget.ImageView r0 = r5.k
            r0.setVisibility(r4)
            goto L_0x00da
        */
        throw new UnsupportedOperationException("Method not decompiled: com.immomo.momo.android.activity.plugin.RenrenActivity.u():void");
    }

    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        super.a(bundle);
        setContentView((int) R.layout.activity_plus_renren);
        this.s = g.o().inflate((int) R.layout.include_renren_prifile, (ViewGroup) null);
        this.h = (ImageView) this.s.findViewById(R.id.renren_iv_avator);
        this.i = (TextView) this.s.findViewById(R.id.renren_tv_name);
        this.j = (TextView) this.s.findViewById(R.id.renren_tv_friendcount);
        this.k = (ImageView) this.s.findViewById(R.id.renren_iv_gender);
        this.l = (TextView) this.s.findViewById(R.id.renren_tv_location);
        this.m = this.s.findViewById(R.id.renren_layout_university);
        this.n = (TextView) this.s.findViewById(R.id.renren_tv_university);
        this.o = this.s.findViewById(R.id.renren_layout_work);
        this.p = (TextView) this.s.findViewById(R.id.renren_tv_work);
        this.q = this.s.findViewById(R.id.renren_layout_hs);
        this.r = (TextView) this.s.findViewById(R.id.renren_tv_hs);
        this.u = (ListView) findViewById(R.id.renren_list);
        ((HeaderLayout) findViewById(R.id.layout_header)).setTitleText((int) R.string.plus_renren);
        d();
    }

    /* access modifiers changed from: protected */
    public final void d() {
        this.u.addHeaderView(this.s);
        this.v = new hh(this);
        this.u.setAdapter((ListAdapter) this.v);
        if (getIntent().getExtras() != null) {
            if (getIntent().getExtras().get("profile") != null) {
                this.t = (c) getIntent().getExtras().get("profile");
                u();
                findViewById(R.id.process_layout_root).setVisibility(8);
                return;
            }
            this.x = getIntent().getExtras() == null ? PoiTypeDef.All : (String) getIntent().getExtras().get("momoid");
            this.y = getIntent().getExtras() == null ? PoiTypeDef.All : (String) getIntent().getExtras().get("renrenuid");
            if (!a.a((CharSequence) this.x) || !a.a((CharSequence) this.y)) {
                this.z = new bm(this, this).execute(new Object[0]);
                return;
            }
        }
        finish();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.z != null && !this.z.isCancelled()) {
            this.z.cancel(true);
        }
    }
}
