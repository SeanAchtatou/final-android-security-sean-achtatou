package com.immomo.momo.android.view.photoview;

import android.annotation.TargetApi;
import android.content.Context;
import android.view.MotionEvent;
import mm.purchasesdk.PurchaseCode;

@TargetApi(5)
class j extends i {
    private int d = -1;
    private int e = 0;

    public j(Context context) {
        super(context);
    }

    public boolean a(MotionEvent motionEvent) {
        int i = 0;
        switch (motionEvent.getAction() & PurchaseCode.AUTH_INVALID_APP) {
            case 0:
                this.d = motionEvent.getPointerId(0);
                break;
            case 1:
            case 3:
                this.d = -1;
                break;
            case 6:
                int action = (motionEvent.getAction() & 65280) >> 8;
                if (motionEvent.getPointerId(action) == this.d) {
                    int i2 = action == 0 ? 1 : 0;
                    this.d = motionEvent.getPointerId(i2);
                    this.b = motionEvent.getX(i2);
                    this.c = motionEvent.getY(i2);
                    break;
                }
                break;
        }
        if (this.d != -1) {
            i = this.d;
        }
        this.e = motionEvent.findPointerIndex(i);
        return super.a(motionEvent);
    }

    /* access modifiers changed from: package-private */
    public final float b(MotionEvent motionEvent) {
        try {
            return motionEvent.getX(this.e);
        } catch (Exception e2) {
            return motionEvent.getX();
        }
    }

    /* access modifiers changed from: package-private */
    public final float c(MotionEvent motionEvent) {
        try {
            return motionEvent.getY(this.e);
        } catch (Exception e2) {
            return motionEvent.getY();
        }
    }
}
