package com.immomo.momo.android.activity.message;

import android.content.DialogInterface;
import android.support.v4.b.a;
import com.immomo.momo.android.view.EmoteEditeText;

final class cl implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ MultiChatActivity f1972a;
    private final /* synthetic */ EmoteEditeText b;

    cl(MultiChatActivity multiChatActivity, EmoteEditeText emoteEditeText) {
        this.f1972a = multiChatActivity;
        this.b = emoteEditeText;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        if (a.a((CharSequence) this.b.getText().toString().trim())) {
            this.f1972a.a((CharSequence) "名称不能为空");
            return;
        }
        this.f1972a.b(new cp(this.f1972a, this.f1972a, this.b.getText().toString().trim()));
        dialogInterface.dismiss();
    }
}
