package com.immomo.momo.android.plugin.cropimage;

import android.graphics.Bitmap;
import android.graphics.Matrix;

public final class t {

    /* renamed from: a  reason: collision with root package name */
    private Bitmap f2597a;
    private int b = 0;

    public t(Bitmap bitmap) {
        this.f2597a = bitmap;
    }

    private boolean f() {
        return (this.b / 90) % 2 != 0;
    }

    public final int a() {
        return this.b;
    }

    public final void a(int i) {
        this.b = i;
    }

    public final void a(Bitmap bitmap) {
        this.f2597a = bitmap;
    }

    public final Bitmap b() {
        return this.f2597a;
    }

    public final Matrix c() {
        Matrix matrix = new Matrix();
        if (this.b != 0) {
            matrix.preTranslate((float) (-(this.f2597a.getWidth() / 2)), (float) (-(this.f2597a.getHeight() / 2)));
            matrix.postRotate((float) this.b);
            matrix.postTranslate((float) (e() / 2), (float) (d() / 2));
        }
        return matrix;
    }

    public final int d() {
        return f() ? this.f2597a.getWidth() : this.f2597a.getHeight();
    }

    public final int e() {
        return f() ? this.f2597a.getHeight() : this.f2597a.getWidth();
    }
}
