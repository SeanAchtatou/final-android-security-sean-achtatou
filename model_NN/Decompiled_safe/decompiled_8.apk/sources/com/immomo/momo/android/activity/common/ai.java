package com.immomo.momo.android.activity.common;

import android.content.Context;
import android.widget.ListAdapter;
import com.immomo.momo.android.a.fz;
import com.immomo.momo.android.c.d;
import com.immomo.momo.protocol.a.w;
import java.util.ArrayList;
import java.util.List;

final class ai extends d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ah f1079a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ai(ah ahVar, Context context) {
        super(context);
        this.f1079a = ahVar;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        ArrayList arrayList = new ArrayList();
        w.a().d(arrayList, 1);
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        super.a(exc);
        this.f1079a.O.n();
        if (this.f1079a.P != null && !this.f1079a.P.isCancelled()) {
            this.f1079a.P.cancel(true);
        }
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        List list = (List) obj;
        super.a(list);
        this.f1079a.T = new fz(this.f1079a.c(), list, this.f1079a.O, this.f1079a);
        this.f1079a.O.setAdapter((ListAdapter) this.f1079a.T);
    }

    /* access modifiers changed from: protected */
    public final void b() {
        super.b();
        this.f1079a.P = null;
        this.f1079a.O.n();
    }
}
