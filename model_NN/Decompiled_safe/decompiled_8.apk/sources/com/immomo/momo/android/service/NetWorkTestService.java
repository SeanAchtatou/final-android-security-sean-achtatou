package com.immomo.momo.android.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import com.immomo.momo.util.m;
import java.util.Random;

public class NetWorkTestService extends Service {

    /* renamed from: a  reason: collision with root package name */
    public static boolean f2601a = false;
    /* access modifiers changed from: private */
    public Random b = null;
    /* access modifiers changed from: private */
    public m c = new m(this);

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        super.onCreate();
        this.b = new Random();
        f2601a = true;
        new o(this, (byte) 0).start();
    }

    public void onDestroy() {
        f2601a = false;
    }
}
