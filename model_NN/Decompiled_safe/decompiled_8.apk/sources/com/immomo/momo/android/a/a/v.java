package com.immomo.momo.android.a.a;

import android.graphics.Bitmap;
import android.support.v4.b.a;
import com.immomo.momo.android.c.g;
import com.immomo.momo.service.bean.Message;
import mm.purchasesdk.PurchaseCode;

final class v implements g {

    /* renamed from: a  reason: collision with root package name */
    private Message f704a;
    private /* synthetic */ s b;

    public v(s sVar, Message message) {
        this.b = sVar;
        this.f704a = message;
    }

    public final /* synthetic */ void a(Object obj) {
        Bitmap bitmap = (Bitmap) obj;
        s.j.remove(this.f704a.msgId);
        this.f704a.isLoadingResourse = false;
        if (bitmap != null) {
            s.a(this.f704a.fileName, a.a(bitmap, 6.0f));
            this.f704a.setImageLoadFailed(false);
            this.b.u.sendEmptyMessage(396);
            return;
        }
        this.f704a.setImageLoadFailed(true);
        this.b.u.sendEmptyMessage(PurchaseCode.BILL_CANCEL_FAIL);
    }
}
