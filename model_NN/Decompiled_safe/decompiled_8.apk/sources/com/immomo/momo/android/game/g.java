package com.immomo.momo.android.game;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.Button;
import android.widget.TextView;
import com.immomo.a.a.f.a;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.ao;
import com.immomo.momo.android.activity.lh;
import com.immomo.momo.android.pay.al;
import com.immomo.momo.android.pay.ap;

@SuppressLint({"ValidFragment"})
public final class g extends lh {
    ai O;
    String P;
    String Q;
    am R = null;
    private TextView S;
    private TextView T;
    private TextView U;
    private TextView V;
    private TextView W;
    private Button X;
    private Handler Y = new h(this);

    public g(ai aiVar, String str, String str2) {
        this.O = aiVar;
        this.P = str;
        this.Q = str2;
        this.R = aiVar.f2405a;
    }

    /* access modifiers changed from: private */
    public void O() {
        if (new al(c()).a() && !new ap().a(this.R.f, this.Y, c())) {
            a("支付宝启动失败");
        }
    }

    static /* synthetic */ void a(g gVar) {
        Intent intent = new Intent();
        intent.putExtra("product_id", gVar.O.i);
        intent.putExtra("trade_number", gVar.R.h);
        intent.putExtra("trade_sign", gVar.R.g);
        if (!a.a(gVar.O.j)) {
            intent.putExtra("trade_extendnumber", gVar.O.j);
        }
        gVar.c().setResult(30210, intent);
        gVar.c().finish();
    }

    static /* synthetic */ void d(g gVar) {
        if (!gVar.R.i) {
            ((ao) gVar.c()).b(new j(gVar, gVar.c()));
        } else {
            gVar.O();
        }
    }

    /* access modifiers changed from: protected */
    public final int B() {
        return R.layout.fragment_mdkpay;
    }

    /* access modifiers changed from: protected */
    public final void C() {
        this.S = (TextView) c((int) R.id.opentrade_tv_fee);
        this.T = (TextView) c((int) R.id.opentrade_tv_product);
        this.U = (TextView) c((int) R.id.opentrade_tv_username);
        this.V = (TextView) c((int) R.id.opentrade_tv_momoid);
        this.X = (Button) c((int) R.id.opentrade_btn_confim);
        this.W = (TextView) c((int) R.id.opentrade_tv_desc);
        this.X.setText("去支付宝支付");
        this.W.setText("请确认支付后登录支付宝支付");
    }

    public final boolean F() {
        return false;
    }

    public final void aj() {
        this.S.setText("支付金额: " + android.support.v4.b.a.a(this.R.e) + "元");
        this.T.setText("商品: " + this.O.h);
        if (this.O.g != null) {
            this.U.setText("用户: " + this.O.g.h());
            this.V.setText("陌陌号: " + this.O.g.h);
            return;
        }
        this.U.setText("用户: ");
        this.V.setText("陌陌号: ");
    }

    public final void g(Bundle bundle) {
        this.X.setOnClickListener(new i(this));
        aj();
    }
}
