package com.immomo.momo.android.activity.plugin;

import android.os.Bundle;
import android.support.v4.b.a;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.ah;
import com.immomo.momo.android.view.HeaderLayout;
import com.immomo.momo.plugin.d.c;
import com.immomo.momo.plugin.e.e;
import com.immomo.momo.plugin.f.b;

public class CommunityStatusActivity extends ah {
    /* access modifiers changed from: private */
    public int h;
    private HeaderLayout i = null;
    private ImageView j;
    /* access modifiers changed from: private */
    public TextView k;
    private TextView l;
    private TextView m;
    private View n;
    private View o;
    private View p;
    /* access modifiers changed from: private */
    public c q = null;
    /* access modifiers changed from: private */
    public e r = null;
    /* access modifiers changed from: private */
    public b s = null;
    /* access modifiers changed from: private */
    public ax t;
    /* access modifiers changed from: private */
    public aw u;
    /* access modifiers changed from: private */
    public ay v;
    /* access modifiers changed from: private */
    public az w;

    static /* synthetic */ String l(CommunityStatusActivity communityStatusActivity) {
        String str = PoiTypeDef.All;
        switch (communityStatusActivity.h) {
            case 1:
                str = "新浪微博";
                break;
            case 2:
                str = "腾讯微博";
                break;
            case 3:
                str = "人人网";
                break;
        }
        return String.format(communityStatusActivity.getResources().getString(R.string.communitystatus_unbind_info_dialog), str);
    }

    static /* synthetic */ void m(CommunityStatusActivity communityStatusActivity) {
        switch (communityStatusActivity.h) {
            case 1:
                communityStatusActivity.b(new az(communityStatusActivity, communityStatusActivity, 1));
                return;
            case 2:
                communityStatusActivity.b(new az(communityStatusActivity, communityStatusActivity, 6));
                return;
            case 3:
                communityStatusActivity.b(new az(communityStatusActivity, communityStatusActivity, 3));
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        super.a(bundle);
        setContentView((int) R.layout.activity_communitystatus);
        this.i = (HeaderLayout) findViewById(R.id.layout_header);
        this.j = (ImageView) findViewById(R.id.image_icon);
        this.k = (TextView) findViewById(R.id.txt_weiboname);
        this.l = (TextView) findViewById(R.id.txt_showweibo);
        this.m = (TextView) findViewById(R.id.txt_addweibo);
        this.n = findViewById(R.id.layout_showweibo);
        this.o = findViewById(R.id.layout_unbind);
        this.p = findViewById(R.id.layout_addweibo);
        this.h = getIntent().getIntExtra("type", 0);
        switch (this.h) {
            case 0:
                finish();
                break;
            case 1:
                b(new ax(this, this));
                this.j.setImageResource(R.drawable.ic_setting_bind_intro_weibo);
                this.l.setText((int) R.string.community_sina_tv_info_show);
                this.m.setText((int) R.string.community_sina_tv_info_add);
                this.i.setTitleText((int) R.string.community_sina_title);
                if (!a.a((CharSequence) this.f.p)) {
                    this.k.setText(this.f.p);
                    break;
                }
                break;
            case 2:
                ay ayVar = new ay(this, this);
                this.v = ayVar;
                b(ayVar);
                this.j.setImageResource(R.drawable.ic_setting_bind_intro_tweibo);
                this.l.setText((int) R.string.community_tx_tv_info_show);
                this.m.setText((int) R.string.community_tx_tv_info_add);
                this.i.setTitleText((int) R.string.community_tx_title);
                if (!a.a((CharSequence) this.f.q)) {
                    this.k.setText(this.f.q);
                    break;
                }
                break;
            case 3:
                b(new aw(this, this));
                this.j.setImageResource(R.drawable.ic_setting_bind_intro_renren);
                this.l.setText((int) R.string.community_renren_tv_info_show);
                this.m.setText((int) R.string.community_renren_tv_info_add);
                this.i.setTitleText((int) R.string.community_renren_title);
                if (!a.a((CharSequence) this.f.r)) {
                    this.k.setText(this.f.r);
                    break;
                }
                break;
        }
        this.p.setOnClickListener(new as(this));
        this.n.setOnClickListener(new at(this));
        this.o.setOnClickListener(new au(this));
    }
}
