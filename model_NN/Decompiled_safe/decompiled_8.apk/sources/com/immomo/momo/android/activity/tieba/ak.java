package com.immomo.momo.android.activity.tieba;

import com.immomo.momo.android.view.MGifImageView;
import com.immomo.momo.android.view.au;
import java.io.File;

final class ak implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ aj f2162a;
    private final /* synthetic */ File b;
    private final /* synthetic */ boolean c;
    private final /* synthetic */ MGifImageView d;

    ak(aj ajVar, File file, boolean z, MGifImageView mGifImageView) {
        this.f2162a = ajVar;
        this.b = file;
        this.c = z;
        this.d = mGifImageView;
    }

    public final void run() {
        if (this.b != null && this.b.exists()) {
            this.f2162a.f2161a.J = new au(this.c ? 1 : 2);
            this.f2162a.f2161a.J.a(this.b, this.d);
            this.f2162a.f2161a.J.b(20);
            this.d.setGifDecoder(this.f2162a.f2161a.J);
        }
    }
}
