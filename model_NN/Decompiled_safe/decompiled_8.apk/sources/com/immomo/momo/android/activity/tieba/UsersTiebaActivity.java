package com.immomo.momo.android.activity.tieba;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.b.a;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import com.immomo.momo.R;
import com.immomo.momo.android.a.ji;
import com.immomo.momo.android.activity.ah;
import com.immomo.momo.android.broadcast.aa;
import com.immomo.momo.android.view.MomoRefreshExpandableListView;
import com.immomo.momo.android.view.bu;
import com.immomo.momo.android.view.cv;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.bb;
import com.immomo.momo.service.bean.c.d;
import java.util.ArrayList;

public class UsersTiebaActivity extends ah implements ExpandableListView.OnChildClickListener, bu, cv {
    public static String h = "key_momoid";
    public static String i = "key_name";
    /* access modifiers changed from: private */
    public MomoRefreshExpandableListView j;
    /* access modifiers changed from: private */
    public ji k = null;
    /* access modifiers changed from: private */
    public fl l;
    /* access modifiers changed from: private */
    public String m;
    private String n;
    private aa o = null;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, com.immomo.momo.android.view.MomoRefreshExpandableListView, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        super.a(bundle);
        setContentView((int) R.layout.activity_tiebacreating);
        this.m = getIntent().getStringExtra(h);
        if (a.a((CharSequence) this.m)) {
            finish();
        }
        this.n = getIntent().getStringExtra(i);
        if (a.a((CharSequence) this.n)) {
            this.n = this.m;
        } else if (this.m.equals(this.f.h)) {
            this.n = "我";
        }
        this.j = (MomoRefreshExpandableListView) findViewById(R.id.listview);
        this.j.setListPaddingBottom(-3);
        this.j.setMMHeaderView(g.o().inflate((int) R.layout.listitem_groupsite, (ViewGroup) this.j, false));
        this.j.setTimeEnable(false);
        this.j.setOnGroupClickListener(new fk());
        setTitle(String.valueOf(this.n) + "加入的陌陌吧");
        d();
        this.j.setOnPullToRefreshListener(this);
        this.j.setOnCancelListener(this);
        this.j.setOnChildClickListener(this);
        this.o = new aa(this);
        this.o.a(new fj(this));
    }

    public final void b_() {
        b(new fl(this, this));
    }

    /* access modifiers changed from: protected */
    public final void d() {
        super.d();
        this.k = new ji(new ArrayList(), this.j);
        this.j.setAdapter(this.k);
        this.k.b();
        this.j.h();
    }

    public boolean onChildClick(ExpandableListView expandableListView, View view, int i2, int i3, long j2) {
        bb a2 = this.k.getGroup(i2);
        if (a2.a() == 3) {
            d a3 = this.k.getChild(i2, i3);
            Intent intent = new Intent(this, TiebaCreateActivity.class);
            intent.putExtra(TiebaCreateActivity.i, a3.b);
            intent.putExtra(TiebaCreateActivity.h, a3.g);
            intent.putExtra(TiebaCreateActivity.k, a3.f3019a);
            intent.putExtra(TiebaCreateActivity.j, a3.m);
            startActivity(intent);
            return true;
        } else if (a2.a() != 0) {
            return true;
        } else {
            Intent intent2 = new Intent(this, TiebaProfileActivity.class);
            intent2.putExtra("tiebaid", this.k.getChild(i2, i3).f3019a);
            startActivity(intent2);
            return true;
        }
    }

    public final void v() {
        this.j.i();
    }
}
