package com.immomo.momo.android.view.dragsort;

import android.content.Context;
import android.content.res.TypedArray;
import android.database.DataSetObserver;
import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ListAdapter;
import com.immomo.momo.android.view.MomoRefreshListView;
import com.immomo.momo.h;
import mm.purchasesdk.PurchaseCode;

public class DragSortListView extends MomoRefreshListView {
    /* access modifiers changed from: private */
    public int A = 0;
    /* access modifiers changed from: private */
    public int B = 1;
    /* access modifiers changed from: private */
    public int C;
    /* access modifiers changed from: private */
    public int D;
    private int E = 0;
    private View[] F = new View[1];
    private k G;
    private float H = 0.33333334f;
    private float I = 0.33333334f;
    private int J;
    private int K;
    /* access modifiers changed from: private */
    public float L;
    /* access modifiers changed from: private */
    public float M;
    /* access modifiers changed from: private */
    public float N;
    /* access modifiers changed from: private */
    public float O;
    /* access modifiers changed from: private */
    public float P = 0.5f;
    /* access modifiers changed from: private */
    public j Q = new j(this);
    private int R;
    /* access modifiers changed from: private */
    public int S;
    /* access modifiers changed from: private */
    public int T;
    private int U = 0;
    private boolean V = false;
    private boolean W = false;
    private p Z = null;
    private MotionEvent aa;
    private int ab = 0;
    private float ac = 0.25f;
    private float ad = 0.0f;
    private f ae;
    private boolean af = false;
    private m ag;
    /* access modifiers changed from: private */
    public boolean ah = false;
    private boolean ai = false;
    private q aj = new q();
    private r ak;
    private n al;
    /* access modifiers changed from: private */
    public boolean am;
    /* access modifiers changed from: private */
    public float an = 0.0f;
    private boolean ao = false;
    private View g;
    /* access modifiers changed from: private */
    public Point h = new Point();
    private Point i = new Point();
    /* access modifiers changed from: private */
    public int j;
    private boolean k = false;
    private DataSetObserver l;
    private float m = 1.0f;
    private float n = 1.0f;
    /* access modifiers changed from: private */
    public int o;
    /* access modifiers changed from: private */
    public int p;
    /* access modifiers changed from: private */
    public int q;
    private boolean r = false;
    /* access modifiers changed from: private */
    public int s;
    private int t;
    private int u;
    private i v;
    private o w;
    private s x;
    private h y;
    private boolean z = true;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(float, float):float} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    public DragSortListView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        int i2 = 150;
        int i3 = 150;
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(attributeSet, h.DragSortListView, 0, 0);
            this.B = Math.max(1, obtainStyledAttributes.getDimensionPixelSize(0, 1));
            this.af = obtainStyledAttributes.getBoolean(5, false);
            if (this.af) {
                this.ag = new m(this);
            }
            this.m = obtainStyledAttributes.getFloat(6, this.m);
            this.n = this.m;
            this.z = obtainStyledAttributes.getBoolean(10, this.z);
            this.ac = Math.max(0.0f, Math.min(1.0f, 1.0f - obtainStyledAttributes.getFloat(7, 0.75f)));
            this.r = this.ac > 0.0f;
            setDragScrollStart(obtainStyledAttributes.getFloat(1, this.H));
            this.P = obtainStyledAttributes.getFloat(2, this.P);
            int i4 = obtainStyledAttributes.getInt(8, 150);
            int i5 = obtainStyledAttributes.getInt(9, 150);
            if (obtainStyledAttributes.getBoolean(17, true)) {
                boolean z2 = obtainStyledAttributes.getBoolean(12, false);
                int i6 = obtainStyledAttributes.getInt(4, 1);
                boolean z3 = obtainStyledAttributes.getBoolean(11, true);
                int i7 = obtainStyledAttributes.getInt(13, 0);
                int resourceId = obtainStyledAttributes.getResourceId(14, 0);
                int resourceId2 = obtainStyledAttributes.getResourceId(15, 0);
                int resourceId3 = obtainStyledAttributes.getResourceId(16, 0);
                int color = obtainStyledAttributes.getColor(3, -16777216);
                a aVar = new a(this, resourceId, i7, i6, resourceId3, resourceId2);
                aVar.b(z2);
                aVar.a(z3);
                aVar.b(color);
                this.Z = aVar;
                setOnTouchListener(aVar);
            }
            obtainStyledAttributes.recycle();
            i3 = i5;
            i2 = i4;
        }
        this.G = new k(this);
        if (i2 > 0) {
            this.ak = new r(this, i2);
        }
        if (i3 > 0) {
            this.al = new n(this, i3);
        }
        this.aa = MotionEvent.obtain(0, 0, 3, 0.0f, 0.0f, 0.0f, 0.0f, 0, 0.0f, 0.0f, 0, 0);
        this.l = new e(this);
    }

    private void A() {
        if (this.g != null) {
            b(this.g);
            this.C = this.g.getMeasuredHeight();
            this.D = this.C / 2;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.android.view.dragsort.DragSortListView.c(int, android.view.View, boolean):void
     arg types: [int, android.view.View, int]
     candidates:
      com.immomo.momo.android.view.dragsort.DragSortListView.c(com.immomo.momo.android.view.dragsort.DragSortListView, int, android.view.View):void
      com.immomo.momo.android.view.dragsort.DragSortListView.c(int, android.view.View, boolean):void */
    /* access modifiers changed from: private */
    public void B() {
        int firstVisiblePosition = getFirstVisiblePosition() + (getChildCount() / 2);
        View childAt = getChildAt(getChildCount() / 2);
        if (childAt != null) {
            c(firstVisiblePosition, childAt, true);
        }
    }

    /* access modifiers changed from: private */
    public void C() {
        if (this.g != null) {
            this.g.setVisibility(8);
            if (this.Z != null) {
                this.Z.a(this.g);
            }
            this.g = null;
            invalidate();
        }
    }

    private void a(int i2, float f) {
        if (this.A == 0 || this.A == 4) {
            if (this.A == 0) {
                this.s = getHeaderViewsCount() + i2;
                this.p = this.s;
                this.q = this.s;
                this.o = this.s;
                View childAt = getChildAt(this.s - getFirstVisiblePosition());
                if (childAt != null) {
                    childAt.setVisibility(4);
                }
            }
            this.A = 1;
            this.an = f;
            if (this.W) {
                switch (this.ab) {
                    case 1:
                        super.onTouchEvent(this.aa);
                        break;
                    case 2:
                        super.onInterceptTouchEvent(this.aa);
                        break;
                }
            }
            if (this.ak != null) {
                this.ak.c();
            } else {
                v();
            }
        }
    }

    private void a(int i2, Canvas canvas) {
        ViewGroup viewGroup;
        int bottom;
        int i3;
        Drawable divider = getDivider();
        int dividerHeight = getDividerHeight();
        if (divider != null && dividerHeight != 0 && (viewGroup = (ViewGroup) getChildAt(i2 - getFirstVisiblePosition())) != null) {
            int paddingLeft = getPaddingLeft();
            int width = getWidth() - getPaddingRight();
            int height = viewGroup.getChildAt(0).getHeight();
            if (i2 > this.s) {
                i3 = height + viewGroup.getTop();
                bottom = i3 + dividerHeight;
            } else {
                bottom = viewGroup.getBottom() - height;
                i3 = bottom - dividerHeight;
            }
            canvas.save();
            canvas.clipRect(paddingLeft, i3, width, bottom);
            divider.setBounds(paddingLeft, i3, width, bottom);
            divider.draw(canvas);
            canvas.restore();
        }
    }

    /* access modifiers changed from: private */
    public void a(int i2, View view, boolean z2) {
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        int c = (i2 == this.s || i2 == this.p || i2 == this.q) ? c(i2, b(i2, view, z2)) : -2;
        if (c != layoutParams.height) {
            layoutParams.height = c;
            view.setLayoutParams(layoutParams);
        }
        if (i2 == this.p || i2 == this.q) {
            if (i2 < this.s) {
                if (this.y == null || this.y.a(i2 - getHeaderViewsCount())) {
                    ((c) view).setGravity(80);
                }
            } else if (i2 > this.s) {
                if (this.y != null && !this.y.a(i2 - getHeaderViewsCount())) {
                    ((c) view).setGravity(80);
                }
            }
            ((c) view).setGravity(48);
        }
        int visibility = view.getVisibility();
        int i3 = 0;
        if (i2 == this.s && this.g != null) {
            i3 = 4;
        }
        if (i3 != visibility) {
            view.setVisibility(i3);
        }
    }

    private void a(MotionEvent motionEvent) {
        int action = motionEvent.getAction() & PurchaseCode.AUTH_INVALID_APP;
        if (action != 0) {
            int i2 = this.R;
            this.T = this.S;
        }
        this.R = (int) motionEvent.getX();
        this.S = (int) motionEvent.getY();
        if (action == 0) {
            int i3 = this.R;
            this.T = this.S;
        }
        motionEvent.getRawX();
        int i4 = this.R;
        motionEvent.getRawY();
        int i5 = this.S;
    }

    private boolean a(boolean z2, float f) {
        if (this.g == null) {
            return false;
        }
        this.G.c();
        if (z2) {
            a(this.s - getHeaderViewsCount(), f);
        } else if (this.al != null) {
            this.al.c();
        } else {
            u();
        }
        if (this.af) {
            this.ag.c();
        }
        return true;
    }

    /* access modifiers changed from: private */
    public int b(int i2, int i3) {
        int headerViewsCount = getHeaderViewsCount();
        int footerViewsCount = getFooterViewsCount();
        if (i2 <= headerViewsCount || i2 >= getCount() - footerViewsCount) {
            return i3;
        }
        int dividerHeight = getDividerHeight();
        int i4 = this.C - this.B;
        int d = d(i2);
        int c = c(i2);
        if (this.q <= this.s) {
            if (i2 == this.q && this.p != this.q) {
                i3 = i2 == this.s ? (i3 + c) - this.C : ((c - d) + i3) - i4;
            } else if (i2 > this.q && i2 <= this.s) {
                i3 -= i4;
            }
        } else if (i2 > this.s && i2 <= this.p) {
            i3 += i4;
        } else if (i2 == this.q && this.p != this.q) {
            i3 += c - d;
        }
        return i2 <= this.s ? (((this.C - dividerHeight) - d(i2 - 1)) / 2) + i3 : (((d - dividerHeight) - this.C) / 2) + i3;
    }

    /* access modifiers changed from: private */
    public int b(int i2, View view, boolean z2) {
        if (i2 == this.s) {
            return 0;
        }
        if (i2 >= getHeaderViewsCount() && i2 < getCount() - getFooterViewsCount()) {
            view = ((ViewGroup) view).getChildAt(0);
        }
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        if (layoutParams != null && layoutParams.height > 0) {
            return layoutParams.height;
        }
        int height = view.getHeight();
        if (height != 0 && !z2) {
            return height;
        }
        b(view);
        return view.getMeasuredHeight();
    }

    private void b(View view) {
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        if (layoutParams == null) {
            layoutParams = new AbsListView.LayoutParams(-1, -2);
            view.setLayoutParams(layoutParams);
        }
        view.measure(ViewGroup.getChildMeasureSpec(this.E, getListPaddingLeft() + getListPaddingRight(), layoutParams.width), layoutParams.height > 0 ? View.MeasureSpec.makeMeasureSpec(layoutParams.height, 1073741824) : View.MeasureSpec.makeMeasureSpec(0, 0));
    }

    /* access modifiers changed from: private */
    public int c(int i2) {
        View childAt = getChildAt(i2 - getFirstVisiblePosition());
        return childAt != null ? childAt.getHeight() : c(i2, d(i2));
    }

    private int c(int i2, int i3) {
        getDividerHeight();
        boolean z2 = this.r && this.p != this.q;
        int i4 = this.C - this.B;
        int i5 = (int) (this.ad * ((float) i4));
        return i2 == this.s ? this.s == this.p ? z2 ? i5 + this.B : this.C : this.s == this.q ? this.C - i5 : this.B : i2 == this.p ? z2 ? i3 + i5 : i3 + i4 : i2 == this.q ? (i3 + i4) - i5 : i3;
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:100:0x02d9  */
    /* JADX WARNING: Removed duplicated region for block: B:105:0x02fb  */
    /* JADX WARNING: Removed duplicated region for block: B:126:0x0353  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x018d  */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x01c1  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void c(int r18, android.view.View r19, boolean r20) {
        /*
            r17 = this;
            r2 = 1
            r0 = r17
            r0.ah = r2
            r0 = r17
            int r9 = r0.p
            r0 = r17
            int r10 = r0.q
            r0 = r17
            com.immomo.momo.android.view.dragsort.p r2 = r0.Z
            if (r2 == 0) goto L_0x0035
            r0 = r17
            android.graphics.Point r2 = r0.i
            r0 = r17
            int r3 = r0.R
            r0 = r17
            int r4 = r0.S
            r2.set(r3, r4)
            r0 = r17
            com.immomo.momo.android.view.dragsort.p r2 = r0.Z
            r0 = r17
            android.view.View r3 = r0.g
            r0 = r17
            android.graphics.Point r3 = r0.h
            r0 = r17
            android.graphics.Point r4 = r0.i
            r2.a(r3)
        L_0x0035:
            r0 = r17
            android.graphics.Point r2 = r0.h
            int r2 = r2.x
            r0 = r17
            android.graphics.Point r3 = r0.h
            int r4 = r3.y
            int r3 = r17.getPaddingLeft()
            r0 = r17
            int r5 = r0.U
            r5 = r5 & 1
            if (r5 != 0) goto L_0x022f
            if (r2 <= r3) goto L_0x022f
            r0 = r17
            android.graphics.Point r2 = r0.h
            r2.x = r3
        L_0x0055:
            int r3 = r17.getHeaderViewsCount()
            int r5 = r17.getFooterViewsCount()
            int r6 = r17.getFirstVisiblePosition()
            int r7 = r17.getLastVisiblePosition()
            int r2 = r17.getPaddingTop()
            if (r6 >= r3) goto L_0x0079
            int r2 = r3 - r6
            int r2 = r2 + -1
            r0 = r17
            android.view.View r2 = r0.getChildAt(r2)
            int r2 = r2.getBottom()
        L_0x0079:
            r0 = r17
            int r3 = r0.U
            r3 = r3 & 8
            if (r3 != 0) goto L_0x009a
            r0 = r17
            int r3 = r0.s
            if (r6 > r3) goto L_0x009a
            r0 = r17
            int r3 = r0.s
            int r3 = r3 - r6
            r0 = r17
            android.view.View r3 = r0.getChildAt(r3)
            int r3 = r3.getTop()
            int r2 = java.lang.Math.max(r3, r2)
        L_0x009a:
            int r3 = r17.getHeight()
            int r8 = r17.getPaddingBottom()
            int r3 = r3 - r8
            int r8 = r17.getCount()
            int r8 = r8 - r5
            int r8 = r8 + -1
            if (r7 < r8) goto L_0x00be
            int r3 = r17.getCount()
            int r3 = r3 - r5
            int r3 = r3 + -1
            int r3 = r3 - r6
            r0 = r17
            android.view.View r3 = r0.getChildAt(r3)
            int r3 = r3.getBottom()
        L_0x00be:
            r0 = r17
            int r5 = r0.U
            r5 = r5 & 4
            if (r5 != 0) goto L_0x00df
            r0 = r17
            int r5 = r0.s
            if (r7 < r5) goto L_0x00df
            r0 = r17
            int r5 = r0.s
            int r5 = r5 - r6
            r0 = r17
            android.view.View r5 = r0.getChildAt(r5)
            int r5 = r5.getBottom()
            int r3 = java.lang.Math.min(r5, r3)
        L_0x00df:
            if (r4 >= r2) goto L_0x0241
            r0 = r17
            android.graphics.Point r3 = r0.h
            r3.y = r2
        L_0x00e7:
            r0 = r17
            android.graphics.Point r2 = r0.h
            int r2 = r2.y
            r0 = r17
            int r3 = r0.D
            int r2 = r2 + r3
            r0 = r17
            r0.j = r2
            int r4 = r17.getFirstVisiblePosition()
            r0 = r17
            int r3 = r0.p
            int r2 = r3 - r4
            r0 = r17
            android.view.View r2 = r0.getChildAt(r2)
            if (r2 != 0) goto L_0x0118
            int r2 = r17.getChildCount()
            int r2 = r2 / 2
            int r3 = r4 + r2
            int r2 = r3 - r4
            r0 = r17
            android.view.View r2 = r0.getChildAt(r2)
        L_0x0118:
            int r5 = r2.getTop()
            int r2 = r2.getHeight()
            r0 = r17
            int r4 = r0.b(r3, r5)
            int r7 = r17.getDividerHeight()
            r0 = r17
            int r6 = r0.j
            if (r6 >= r4) goto L_0x0275
            r2 = r3
            r3 = r4
        L_0x0132:
            if (r2 >= 0) goto L_0x0255
        L_0x0134:
            int r5 = r17.getHeaderViewsCount()
            int r7 = r17.getFooterViewsCount()
            r6 = 0
            r0 = r17
            int r8 = r0.p
            r0 = r17
            int r11 = r0.q
            r0 = r17
            float r12 = r0.ad
            r0 = r17
            boolean r13 = r0.r
            if (r13 == 0) goto L_0x02cf
            int r13 = r4 - r3
            int r13 = java.lang.Math.abs(r13)
            r0 = r17
            int r14 = r0.j
            if (r14 >= r4) goto L_0x02a5
        L_0x015b:
            r14 = 1056964608(0x3f000000, float:0.5)
            r0 = r17
            float r15 = r0.ac
            float r14 = r14 * r15
            float r13 = (float) r13
            float r13 = r13 * r14
            int r13 = (int) r13
            float r14 = (float) r13
            int r3 = r3 + r13
            int r13 = r4 - r13
            r0 = r17
            int r15 = r0.j
            if (r15 >= r3) goto L_0x02ac
            int r4 = r2 + -1
            r0 = r17
            r0.p = r4
            r0 = r17
            r0.q = r2
            r4 = 1056964608(0x3f000000, float:0.5)
            r0 = r17
            int r13 = r0.j
            int r3 = r3 - r13
            float r3 = (float) r3
            float r3 = r3 * r4
            float r3 = r3 / r14
            r0 = r17
            r0.ad = r3
        L_0x0187:
            r0 = r17
            int r3 = r0.p
            if (r3 >= r5) goto L_0x02d9
            r0 = r17
            r0.p = r5
            r0 = r17
            r0.q = r5
            r2 = r5
        L_0x0196:
            r0 = r17
            int r3 = r0.p
            if (r3 != r8) goto L_0x01aa
            r0 = r17
            int r3 = r0.q
            if (r3 != r11) goto L_0x01aa
            r0 = r17
            float r3 = r0.ad
            int r3 = (r3 > r12 ? 1 : (r3 == r12 ? 0 : -1))
            if (r3 == 0) goto L_0x0356
        L_0x01aa:
            r3 = 1
        L_0x01ab:
            r0 = r17
            com.immomo.momo.android.view.dragsort.h r4 = r0.y
            if (r4 == 0) goto L_0x02f5
            r0 = r17
            com.immomo.momo.android.view.dragsort.h r4 = r0.y
            int r5 = r2 - r5
            boolean r4 = r4.a(r5)
            if (r4 != 0) goto L_0x02f5
            r3 = 0
            r8 = r3
        L_0x01bf:
            if (r8 == 0) goto L_0x0222
            r17.z()
            r6 = 0
            int r4 = r17.d(r18)
            int r3 = r19.getHeight()
            r0 = r17
            r1 = r18
            int r5 = r0.c(r1, r4)
            r0 = r17
            int r2 = r0.s
            r0 = r18
            if (r0 == r2) goto L_0x034f
            int r2 = r3 - r4
            int r4 = r5 - r4
        L_0x01e1:
            r0 = r17
            int r7 = r0.C
            r0 = r17
            int r11 = r0.s
            r0 = r17
            int r12 = r0.p
            if (r11 == r12) goto L_0x01fe
            r0 = r17
            int r11 = r0.s
            r0 = r17
            int r12 = r0.q
            if (r11 == r12) goto L_0x01fe
            r0 = r17
            int r11 = r0.B
            int r7 = r7 - r11
        L_0x01fe:
            r0 = r18
            if (r0 > r9) goto L_0x0311
            r0 = r17
            int r2 = r0.p
            r0 = r18
            if (r0 <= r2) goto L_0x034c
            int r2 = r7 - r4
            int r2 = r2 + 0
        L_0x020e:
            int r3 = r19.getTop()
            int r2 = r2 + r3
            int r3 = r17.getPaddingTop()
            int r2 = r2 - r3
            r0 = r17
            r1 = r18
            r0.setSelectionFromTop(r1, r2)
            r17.layoutChildren()
        L_0x0222:
            if (r8 != 0) goto L_0x0226
            if (r20 == 0) goto L_0x0229
        L_0x0226:
            r17.invalidate()
        L_0x0229:
            r2 = 0
            r0 = r17
            r0.ah = r2
            return
        L_0x022f:
            r0 = r17
            int r5 = r0.U
            r5 = r5 & 2
            if (r5 != 0) goto L_0x0055
            if (r2 >= r3) goto L_0x0055
            r0 = r17
            android.graphics.Point r2 = r0.h
            r2.x = r3
            goto L_0x0055
        L_0x0241:
            r0 = r17
            int r2 = r0.C
            int r2 = r2 + r4
            if (r2 <= r3) goto L_0x00e7
            r0 = r17
            android.graphics.Point r2 = r0.h
            r0 = r17
            int r4 = r0.C
            int r3 = r3 - r4
            r2.y = r3
            goto L_0x00e7
        L_0x0255:
            int r2 = r2 + -1
            r0 = r17
            int r4 = r0.c(r2)
            if (r2 != 0) goto L_0x0264
            int r5 = r5 - r7
            int r4 = r5 - r4
            goto L_0x0134
        L_0x0264:
            int r4 = r4 + r7
            int r5 = r5 - r4
            r0 = r17
            int r4 = r0.b(r2, r5)
            r0 = r17
            int r6 = r0.j
            if (r6 >= r4) goto L_0x0134
            r3 = r4
            goto L_0x0132
        L_0x0275:
            int r8 = r17.getCount()
            r6 = r5
            r5 = r2
            r2 = r3
            r3 = r4
        L_0x027d:
            if (r2 >= r8) goto L_0x0134
            int r4 = r8 + -1
            if (r2 != r4) goto L_0x0288
            int r4 = r6 + r7
            int r4 = r4 + r5
            goto L_0x0134
        L_0x0288:
            int r4 = r7 + r5
            int r6 = r6 + r4
            int r4 = r2 + 1
            r0 = r17
            int r5 = r0.c(r4)
            int r4 = r2 + 1
            r0 = r17
            int r4 = r0.b(r4, r6)
            r0 = r17
            int r11 = r0.j
            if (r11 < r4) goto L_0x0134
            int r2 = r2 + 1
            r3 = r4
            goto L_0x027d
        L_0x02a5:
            r16 = r3
            r3 = r4
            r4 = r16
            goto L_0x015b
        L_0x02ac:
            r0 = r17
            int r3 = r0.j
            if (r3 < r13) goto L_0x02cf
            r0 = r17
            r0.p = r2
            int r3 = r2 + 1
            r0 = r17
            r0.q = r3
            r3 = 1056964608(0x3f000000, float:0.5)
            r13 = 1065353216(0x3f800000, float:1.0)
            r0 = r17
            int r15 = r0.j
            int r4 = r4 - r15
            float r4 = (float) r4
            float r4 = r4 / r14
            float r4 = r4 + r13
            float r3 = r3 * r4
            r0 = r17
            r0.ad = r3
            goto L_0x0187
        L_0x02cf:
            r0 = r17
            r0.p = r2
            r0 = r17
            r0.q = r2
            goto L_0x0187
        L_0x02d9:
            r0 = r17
            int r3 = r0.q
            int r4 = r17.getCount()
            int r4 = r4 - r7
            if (r3 < r4) goto L_0x0196
            int r2 = r17.getCount()
            int r2 = r2 - r7
            int r2 = r2 + -1
            r0 = r17
            r0.p = r2
            r0 = r17
            r0.q = r2
            goto L_0x0196
        L_0x02f5:
            r0 = r17
            int r4 = r0.o
            if (r2 == r4) goto L_0x0353
            r0 = r17
            com.immomo.momo.android.view.dragsort.i r3 = r0.v
            if (r3 == 0) goto L_0x0309
            r0 = r17
            com.immomo.momo.android.view.dragsort.i r3 = r0.v
            r0 = r17
            int r3 = r0.o
        L_0x0309:
            r0 = r17
            r0.o = r2
            r3 = 1
            r8 = r3
            goto L_0x01bf
        L_0x0311:
            r0 = r18
            if (r0 != r10) goto L_0x0334
            r0 = r17
            int r4 = r0.p
            r0 = r18
            if (r0 > r4) goto L_0x0322
            int r2 = r2 - r7
            int r2 = r2 + 0
            goto L_0x020e
        L_0x0322:
            r0 = r17
            int r4 = r0.q
            r0 = r18
            if (r0 != r4) goto L_0x0330
            int r2 = r3 - r5
            int r2 = r2 + 0
            goto L_0x020e
        L_0x0330:
            int r2 = r2 + 0
            goto L_0x020e
        L_0x0334:
            r0 = r17
            int r2 = r0.p
            r0 = r18
            if (r0 > r2) goto L_0x0340
            int r2 = 0 - r7
            goto L_0x020e
        L_0x0340:
            r0 = r17
            int r2 = r0.q
            r0 = r18
            if (r0 != r2) goto L_0x034c
            int r2 = 0 - r4
            goto L_0x020e
        L_0x034c:
            r2 = r6
            goto L_0x020e
        L_0x034f:
            r2 = r3
            r4 = r5
            goto L_0x01e1
        L_0x0353:
            r8 = r3
            goto L_0x01bf
        L_0x0356:
            r3 = r6
            goto L_0x01ab
        */
        throw new UnsupportedOperationException("Method not decompiled: com.immomo.momo.android.view.dragsort.DragSortListView.c(int, android.view.View, boolean):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.android.view.dragsort.DragSortListView.b(int, android.view.View, boolean):int
     arg types: [int, android.view.View, int]
     candidates:
      com.immomo.momo.android.view.dragsort.DragSortListView.b(com.immomo.momo.android.view.dragsort.DragSortListView, int, android.view.View):int
      com.immomo.momo.android.view.dragsort.DragSortListView.b(int, android.view.View, boolean):int */
    /* access modifiers changed from: private */
    public int d(int i2) {
        View view;
        if (i2 == this.s) {
            return 0;
        }
        View childAt = getChildAt(i2 - getFirstVisiblePosition());
        if (childAt != null) {
            return b(i2, childAt, false);
        }
        int a2 = this.aj.a(i2);
        if (a2 != -1) {
            return a2;
        }
        ListAdapter adapter = getAdapter();
        int itemViewType = adapter.getItemViewType(i2);
        int viewTypeCount = adapter.getViewTypeCount();
        if (viewTypeCount != this.F.length) {
            this.F = new View[viewTypeCount];
        }
        if (itemViewType < 0) {
            view = adapter.getView(i2, null, this);
        } else if (this.F[itemViewType] == null) {
            view = adapter.getView(i2, null, this);
            this.F[itemViewType] = view;
        } else {
            view = adapter.getView(i2, this.F[itemViewType], this);
        }
        int b = b(i2, view, true);
        this.aj.a(i2, b);
        return b;
    }

    static /* synthetic */ void q(DragSortListView dragSortListView) {
        int i2 = dragSortListView.s;
        dragSortListView.getHeaderViewsCount();
        dragSortListView.v();
    }

    private void t() {
        this.s = -1;
        this.p = -1;
        this.q = -1;
        this.o = -1;
    }

    /* access modifiers changed from: private */
    public void u() {
        this.A = 2;
        if (this.w != null && this.o >= 0 && this.o < getCount()) {
            int headerViewsCount = getHeaderViewsCount();
            this.w.a(this.s - headerViewsCount, this.o - headerViewsCount);
        }
        C();
        w();
        t();
        z();
        if (this.W) {
            this.A = 3;
        } else {
            this.A = 0;
        }
    }

    private void v() {
        this.A = 1;
        if (this.x != null) {
            s sVar = this.x;
        }
        C();
        w();
        t();
        if (this.W) {
            this.A = 3;
        } else {
            this.A = 0;
        }
    }

    private void w() {
        int i2 = 0;
        int firstVisiblePosition = getFirstVisiblePosition();
        if (this.s < firstVisiblePosition) {
            View childAt = getChildAt(0);
            if (childAt != null) {
                i2 = childAt.getTop();
            }
            setSelectionFromTop(firstVisiblePosition - 1, i2 - getPaddingTop());
        }
    }

    private void x() {
        this.ab = 0;
        this.W = false;
        if (this.A == 3) {
            this.A = 0;
        }
        this.n = this.m;
        this.ao = false;
        this.aj.a();
    }

    private void y() {
        int paddingTop = getPaddingTop();
        int height = (getHeight() - paddingTop) - getPaddingBottom();
        float f = (float) height;
        this.M = ((float) paddingTop) + (this.H * f);
        this.L = (f * (1.0f - this.I)) + ((float) paddingTop);
        this.J = (int) this.M;
        this.K = (int) this.L;
        this.N = this.M - ((float) paddingTop);
        this.O = ((float) (paddingTop + height)) - this.L;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.android.view.dragsort.DragSortListView.a(int, android.view.View, boolean):void
     arg types: [int, android.view.View, int]
     candidates:
      com.immomo.momo.android.view.dragsort.DragSortListView.a(com.immomo.momo.android.view.dragsort.DragSortListView, int, int):int
      com.immomo.momo.android.view.dragsort.DragSortListView.a(com.immomo.momo.android.view.dragsort.DragSortListView, int, android.view.View):void
      com.immomo.momo.android.view.dragsort.DragSortListView.a(int, android.view.View, boolean):void */
    private void z() {
        int firstVisiblePosition = getFirstVisiblePosition();
        int lastVisiblePosition = getLastVisiblePosition();
        int min = Math.min(lastVisiblePosition - firstVisiblePosition, ((getCount() - 1) - getFooterViewsCount()) - firstVisiblePosition);
        for (int max = Math.max(0, getHeaderViewsCount() - firstVisiblePosition); max <= min; max++) {
            View childAt = getChildAt(max);
            if (childAt != null) {
                a(firstVisiblePosition + max, childAt, false);
            }
        }
    }

    public final void a() {
        if (this.A == 4) {
            this.G.c();
            C();
            t();
            z();
            if (this.W) {
                this.A = 3;
            } else {
                this.A = 0;
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.android.view.dragsort.DragSortListView.a(boolean, float):boolean
     arg types: [int, float]
     candidates:
      com.immomo.momo.android.view.dragsort.DragSortListView.a(int, float):void
      com.immomo.momo.android.view.dragsort.DragSortListView.a(int, android.graphics.Canvas):void
      com.immomo.momo.android.view.dragsort.DragSortListView.a(com.immomo.momo.android.view.dragsort.DragSortListView, float):void
      com.immomo.momo.android.view.dragsort.DragSortListView.a(com.immomo.momo.android.view.dragsort.DragSortListView, int):void
      com.immomo.momo.android.view.dragsort.DragSortListView.a(com.immomo.momo.android.view.dragsort.DragSortListView, boolean):void
      com.immomo.momo.android.view.MomoRefreshListView.a(int, int):void
      com.immomo.momo.android.view.RefreshOnOverScrollListView.a(android.view.View, int):void
      com.immomo.momo.android.view.HandyListView.a(com.immomo.momo.android.view.HandyListView, float):void
      com.immomo.momo.android.view.HandyListView.a(com.immomo.momo.android.view.HandyListView, boolean):void
      com.immomo.momo.android.view.cw.a(int, int):void
      com.immomo.momo.android.view.dragsort.DragSortListView.a(boolean, float):boolean */
    public final boolean a(float f) {
        this.am = true;
        return a(true, f);
    }

    public final boolean a(int i2, int i3, int i4, int i5) {
        View a2;
        if (!this.W || this.Z == null || (a2 = this.Z.a(i2)) == null || this.A != 0 || !this.W || this.g != null || a2 == null || !this.z) {
            return false;
        }
        h canDropListener = getCanDropListener();
        if (canDropListener != null && !canDropListener.a(i2)) {
            return false;
        }
        if (getParent() != null) {
            getParent().requestDisallowInterceptTouchEvent(true);
        }
        int headerViewsCount = getHeaderViewsCount() + i2;
        this.p = headerViewsCount;
        this.q = headerViewsCount;
        this.s = headerViewsCount;
        this.o = headerViewsCount;
        this.A = 4;
        this.U = 0;
        this.U |= i3;
        this.g = a2;
        A();
        this.t = i4;
        this.u = i5;
        int i6 = this.S;
        this.h.x = this.R - this.t;
        this.h.y = this.S - this.u;
        View childAt = getChildAt(this.s - getFirstVisiblePosition());
        if (childAt != null) {
            childAt.setVisibility(4);
        }
        if (this.af) {
            this.ag.a();
        }
        switch (this.ab) {
            case 1:
                super.onTouchEvent(this.aa);
                break;
            case 2:
                super.onInterceptTouchEvent(this.aa);
                break;
        }
        requestLayout();
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.android.view.dragsort.DragSortListView.a(int, float):void
     arg types: [int, int]
     candidates:
      com.immomo.momo.android.view.dragsort.DragSortListView.a(int, android.graphics.Canvas):void
      com.immomo.momo.android.view.dragsort.DragSortListView.a(com.immomo.momo.android.view.dragsort.DragSortListView, float):void
      com.immomo.momo.android.view.dragsort.DragSortListView.a(com.immomo.momo.android.view.dragsort.DragSortListView, int):void
      com.immomo.momo.android.view.dragsort.DragSortListView.a(com.immomo.momo.android.view.dragsort.DragSortListView, boolean):void
      com.immomo.momo.android.view.dragsort.DragSortListView.a(boolean, float):boolean
      com.immomo.momo.android.view.MomoRefreshListView.a(int, int):void
      com.immomo.momo.android.view.RefreshOnOverScrollListView.a(android.view.View, int):void
      com.immomo.momo.android.view.HandyListView.a(com.immomo.momo.android.view.HandyListView, float):void
      com.immomo.momo.android.view.HandyListView.a(com.immomo.momo.android.view.HandyListView, boolean):void
      com.immomo.momo.android.view.cw.a(int, int):void
      com.immomo.momo.android.view.dragsort.DragSortListView.a(int, float):void */
    public final void b(int i2) {
        this.am = false;
        a(i2, 0.0f);
    }

    public final boolean c() {
        return this.ao;
    }

    /* access modifiers changed from: protected */
    public void dispatchDraw(Canvas canvas) {
        float f;
        super.dispatchDraw(canvas);
        if (this.A != 0) {
            if (this.p != this.s) {
                a(this.p, canvas);
            }
            if (!(this.q == this.p || this.q == this.s)) {
                a(this.q, canvas);
            }
        }
        if (this.g != null) {
            int width = this.g.getWidth();
            int height = this.g.getHeight();
            int i2 = this.h.x;
            int width2 = getWidth();
            if (i2 < 0) {
                i2 = -i2;
            }
            if (i2 < width2) {
                float f2 = ((float) (width2 - i2)) / ((float) width2);
                f = f2 * f2;
            } else {
                f = 0.0f;
            }
            int i3 = (int) (f * 255.0f * this.n);
            canvas.save();
            canvas.translate((float) this.h.x, (float) this.h.y);
            canvas.clipRect(0, 0, width, height);
            canvas.saveLayerAlpha(0.0f, 0.0f, (float) width, (float) height, i3, 31);
            this.g.draw(canvas);
            canvas.restore();
            canvas.restore();
        }
    }

    public h getCanDropListener() {
        return this.y;
    }

    public float getFloatAlpha() {
        return this.n;
    }

    public ListAdapter getInputAdapter() {
        if (this.ae == null) {
            return null;
        }
        return this.ae.a();
    }

    /* access modifiers changed from: protected */
    public void layoutChildren() {
        super.layoutChildren();
        if (this.g != null) {
            if (this.g.isLayoutRequested() && !this.k) {
                A();
            }
            this.g.layout(0, 0, this.g.getMeasuredWidth(), this.g.getMeasuredHeight());
            this.k = false;
        }
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (this.af) {
            this.ag.b();
        }
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        boolean z2;
        if (!this.z) {
            return super.onInterceptTouchEvent(motionEvent);
        }
        a(motionEvent);
        this.V = true;
        int action = motionEvent.getAction() & PurchaseCode.AUTH_INVALID_APP;
        if (action == 0) {
            if (this.A != 0) {
                this.ai = true;
                return true;
            }
            this.W = true;
        }
        if (this.g == null) {
            if (super.onInterceptTouchEvent(motionEvent)) {
                this.ao = true;
                z2 = true;
            } else {
                z2 = false;
            }
            switch (action) {
                case 1:
                case 3:
                    x();
                    break;
                case 2:
                default:
                    if (!z2) {
                        this.ab = 2;
                        break;
                    } else {
                        this.ab = 1;
                        break;
                    }
            }
        } else {
            z2 = true;
        }
        if (action == 1 || action == 3) {
            this.W = false;
        }
        return z2;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        super.onMeasure(i2, i3);
        if (this.g != null) {
            if (this.g.isLayoutRequested()) {
                A();
            }
            this.k = true;
        }
        this.E = i2;
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i2, int i3, int i4, int i5) {
        super.onSizeChanged(i2, i3, i4, i5);
        y();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.android.view.dragsort.DragSortListView.a(boolean, float):boolean
     arg types: [int, int]
     candidates:
      com.immomo.momo.android.view.dragsort.DragSortListView.a(int, float):void
      com.immomo.momo.android.view.dragsort.DragSortListView.a(int, android.graphics.Canvas):void
      com.immomo.momo.android.view.dragsort.DragSortListView.a(com.immomo.momo.android.view.dragsort.DragSortListView, float):void
      com.immomo.momo.android.view.dragsort.DragSortListView.a(com.immomo.momo.android.view.dragsort.DragSortListView, int):void
      com.immomo.momo.android.view.dragsort.DragSortListView.a(com.immomo.momo.android.view.dragsort.DragSortListView, boolean):void
      com.immomo.momo.android.view.MomoRefreshListView.a(int, int):void
      com.immomo.momo.android.view.RefreshOnOverScrollListView.a(android.view.View, int):void
      com.immomo.momo.android.view.HandyListView.a(com.immomo.momo.android.view.HandyListView, float):void
      com.immomo.momo.android.view.HandyListView.a(com.immomo.momo.android.view.HandyListView, boolean):void
      com.immomo.momo.android.view.cw.a(int, int):void
      com.immomo.momo.android.view.dragsort.DragSortListView.a(boolean, float):boolean */
    public boolean onTouchEvent(MotionEvent motionEvent) {
        boolean z2 = false;
        if (this.ai) {
            this.ai = false;
            return false;
        } else if (!this.z) {
            return super.onTouchEvent(motionEvent);
        } else {
            boolean z3 = this.V;
            this.V = false;
            if (!z3) {
                a(motionEvent);
            }
            if (this.A == 4) {
                motionEvent.getAction();
                switch (motionEvent.getAction() & PurchaseCode.AUTH_INVALID_APP) {
                    case 1:
                        if (this.A == 4) {
                            this.am = false;
                            a(false, 0.0f);
                        }
                        x();
                        break;
                    case 2:
                        int y2 = (int) motionEvent.getY();
                        this.h.x = ((int) motionEvent.getX()) - this.t;
                        this.h.y = y2 - this.u;
                        B();
                        int min = Math.min(y2, this.j + this.D);
                        int max = Math.max(y2, this.j - this.D);
                        int b = this.G.b();
                        if (min <= this.T || min <= this.K || b == 1) {
                            if (max >= this.T || max >= this.J || b == 0) {
                                if (max >= this.J && min <= this.K && this.G.a()) {
                                    this.G.c();
                                    break;
                                }
                            } else {
                                if (b != -1) {
                                    this.G.c();
                                }
                                this.G.a(0);
                                break;
                            }
                        } else {
                            if (b != -1) {
                                this.G.c();
                            }
                            this.G.a(1);
                            break;
                        }
                        break;
                    case 3:
                        if (this.A == 4) {
                            a();
                        }
                        x();
                        break;
                }
                return true;
            }
            if (this.A == 0 && super.onTouchEvent(motionEvent)) {
                z2 = true;
            }
            switch (motionEvent.getAction() & PurchaseCode.AUTH_INVALID_APP) {
                case 1:
                case 3:
                    x();
                    return z2;
                case 2:
                default:
                    if (!z2) {
                        return z2;
                    }
                    this.ab = 1;
                    return z2;
            }
        }
    }

    public void requestLayout() {
        if (!this.ah) {
            super.requestLayout();
        }
    }

    public final boolean s() {
        return this.z;
    }

    public void setAdapter(ListAdapter listAdapter) {
        if (listAdapter != null) {
            this.ae = new f(this, listAdapter);
            listAdapter.registerDataSetObserver(this.l);
            if (listAdapter instanceof o) {
                setDropListener((o) listAdapter);
            }
            if (listAdapter instanceof i) {
                setDragListener((i) listAdapter);
            }
            if (listAdapter instanceof s) {
                setRemoveListener((s) listAdapter);
            }
        } else {
            this.ae = null;
        }
        super.setAdapter((ListAdapter) this.ae);
    }

    public void setCanDropListener(h hVar) {
        this.y = hVar;
    }

    public void setDragEnabled(boolean z2) {
        this.z = z2;
    }

    public void setDragListener(i iVar) {
        this.v = iVar;
    }

    public void setDragScrollProfile(j jVar) {
        if (jVar != null) {
            this.Q = jVar;
        }
    }

    public void setDragScrollStart(float f) {
        if (f > 0.5f) {
            this.I = 0.5f;
        } else {
            this.I = f;
        }
        if (f > 0.5f) {
            this.H = 0.5f;
        } else {
            this.H = f;
        }
        if (getHeight() != 0) {
            y();
        }
    }

    public void setDragSortListener(l lVar) {
        setDropListener(lVar);
        setDragListener(lVar);
        setRemoveListener(lVar);
    }

    public void setDropListener(o oVar) {
        this.w = oVar;
    }

    public void setFloatAlpha(float f) {
        this.n = f;
    }

    public void setFloatViewManager(p pVar) {
        this.Z = pVar;
    }

    public void setMaxScrollSpeed(float f) {
        this.P = f;
    }

    public void setRemoveListener(s sVar) {
        this.x = sVar;
    }
}
