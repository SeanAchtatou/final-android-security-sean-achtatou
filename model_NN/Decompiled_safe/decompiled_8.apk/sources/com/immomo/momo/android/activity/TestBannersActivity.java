package com.immomo.momo.android.activity;

import android.os.Bundle;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.immomo.momo.R;
import com.immomo.momo.android.view.g;

public class TestBannersActivity extends ao {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_testbanner);
        ListView listView = (ListView) findViewById(R.id.listview);
        listView.addHeaderView(new g(this, 1).getWappview());
        listView.setAdapter((ListAdapter) new lm(this));
        listView.setOnItemClickListener(new ln());
    }
}
