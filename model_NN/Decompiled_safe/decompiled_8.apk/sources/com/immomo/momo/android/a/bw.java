package com.immomo.momo.android.a;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.b.a;
import android.view.View;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.ImageBrowserActivity;
import com.immomo.momo.android.activity.emotestore.EmotionProfileActivity;
import com.immomo.momo.service.bean.aa;

final class bw implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ bv f746a;

    bw(bv bvVar) {
        this.f746a = bvVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public final void onClick(View view) {
        int intValue = ((Integer) view.getTag(R.id.tag_item_position)).intValue();
        String str = ((aa) this.f746a.getItem(intValue)).k;
        if (a.f(str)) {
            Intent intent = new Intent(this.f746a.d(), EmotionProfileActivity.class);
            intent.putExtra("eid", str);
            this.f746a.d().startActivity(intent);
            return;
        }
        Intent intent2 = new Intent(this.f746a.d(), ImageBrowserActivity.class);
        intent2.putExtra("array", new String[]{((aa) this.f746a.getItem(intValue)).getLoadImageId()});
        intent2.putExtra("imagetype", "feed");
        intent2.putExtra("autohide_header", true);
        this.f746a.d().startActivity(intent2);
        if (((Activity) this.f746a.d()).getParent() != null) {
            ((Activity) this.f746a.d()).getParent().overridePendingTransition(R.anim.zoom_enter, R.anim.normal);
        } else {
            ((Activity) this.f746a.d()).overridePendingTransition(R.anim.zoom_enter, R.anim.normal);
        }
    }
}
