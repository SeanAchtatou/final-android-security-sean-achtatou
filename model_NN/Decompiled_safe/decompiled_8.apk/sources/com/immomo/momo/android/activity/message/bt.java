package com.immomo.momo.android.activity.message;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.protocol.a.a.i;
import com.immomo.momo.protocol.a.o;
import java.util.Collection;
import java.util.Date;

final class bt extends d {

    /* renamed from: a  reason: collision with root package name */
    private i f1953a;
    private /* synthetic */ GroupChatActivity c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public bt(GroupChatActivity groupChatActivity, Context context) {
        super(context);
        this.c = groupChatActivity;
        if (groupChatActivity.ai != null) {
            groupChatActivity.ai.cancel(true);
        }
        groupChatActivity.ai = this;
    }

    /* access modifiers changed from: protected */
    public final Object a(Object... objArr) {
        this.f1953a = o.a().a(this.c.i);
        this.c.P.a(this.c.i, this.f1953a.f2888a);
        this.c.ab = new Date();
        this.c.o().a("gmemberlist_lasttime_success" + this.c.i, this.c.ab);
        return null;
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
    }

    /* access modifiers changed from: protected */
    public final void a(Object obj) {
        this.c.R.a((Collection) this.f1953a.f2888a);
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.c.Z.n();
    }
}
