package com.immomo.momo.android.activity.group;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.protocol.a.a.i;
import com.immomo.momo.protocol.a.o;
import java.util.Date;

final class bu extends d {

    /* renamed from: a  reason: collision with root package name */
    private i f1568a;
    private /* synthetic */ GroupFeedsActivity c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public bu(GroupFeedsActivity groupFeedsActivity, Context context) {
        super(context);
        this.c = groupFeedsActivity;
    }

    /* access modifiers changed from: protected */
    public final Object a(Object... objArr) {
        this.f1568a = o.a().a(this.c.u);
        this.c.C.a(this.c.u, this.f1568a.f2888a);
        this.c.t = new Date();
        this.c.o().a("gmemberlist_lasttime_success" + this.c.u, this.c.t);
        return null;
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        super.a(exc);
    }

    /* access modifiers changed from: protected */
    public final void a(Object obj) {
        this.c.w();
    }
}
