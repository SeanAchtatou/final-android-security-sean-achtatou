package com.immomo.momo.android.activity.group;

import android.content.Context;
import android.support.v4.b.a;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.n;
import com.immomo.momo.service.bean.a.g;
import com.immomo.momo.service.bean.as;

final class co extends d {

    /* renamed from: a  reason: collision with root package name */
    private v f1588a;
    private /* synthetic */ GroupPartyActivity c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public co(GroupPartyActivity groupPartyActivity, Context context) {
        super(context);
        this.c = groupPartyActivity;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        return n.a().g(this.c.u, "quit");
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.f1588a = new v(this.c);
        this.f1588a.a("请求提交中");
        this.f1588a.setCancelable(true);
        this.f1588a.setOnCancelListener(new cp(this));
        this.f1588a.show();
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        String str = (String) obj;
        if (!a.a((CharSequence) str)) {
            a(str);
            this.c.t.o = 0;
            if (this.c.t.p != null) {
                this.c.t.p.remove(new as(this.c.f.h));
                g b = this.c.t;
                b.n--;
            }
            this.c.A();
            this.c.w.a(this.c.t);
        }
        GroupPartyActivity.g(this.c);
    }

    /* access modifiers changed from: protected */
    public final void b() {
        if (this.f1588a != null) {
            this.f1588a.dismiss();
        }
    }
}
