package com.immomo.momo.android.activity.feed;

import com.immomo.momo.android.view.cx;

final class az implements cx {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ PublishFeedActivity f1456a;

    az(PublishFeedActivity publishFeedActivity) {
        this.f1456a = publishFeedActivity;
    }

    public final void a(int i, int i2) {
        if (i < i2) {
            this.f1456a.e.a((Object) ("OnResize--------------h < oldh=" + i + "<" + i2));
            if (((double) i) <= ((double) this.f1456a.w) * 0.8d) {
                this.f1456a.h = true;
            }
        } else if (((double) i2) <= ((double) this.f1456a.w) * 0.8d && this.f1456a.h && !this.f1456a.x.isShown()) {
            this.f1456a.h = false;
        }
    }
}
