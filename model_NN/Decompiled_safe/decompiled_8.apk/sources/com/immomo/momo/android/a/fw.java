package com.immomo.momo.android.a;

import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.widget.ImageView;

final class fw {

    /* renamed from: a  reason: collision with root package name */
    private Handler f830a = null;
    /* access modifiers changed from: private */
    public ImageView b = null;

    public fw(Looper looper, ImageView imageView) {
        this.b = imageView;
        this.f830a = new fx(this, looper);
    }

    public final void a(Bitmap bitmap) {
        Message message = new Message();
        message.what = 1;
        message.obj = bitmap;
        this.f830a.sendMessage(message);
    }
}
