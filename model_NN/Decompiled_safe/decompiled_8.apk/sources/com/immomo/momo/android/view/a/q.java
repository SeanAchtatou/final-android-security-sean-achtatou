package com.immomo.momo.android.view.a;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.b.a;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import com.immomo.momo.R;
import com.immomo.momo.g;

public final class q extends n {
    private WebView b = null;
    /* access modifiers changed from: private */
    public View c = null;

    private q(Context context) {
        super(context);
        View inflate = g.o().inflate((int) R.layout.include_dialog_webview, (ViewGroup) null);
        setContentView(inflate);
        this.b = (WebView) inflate.findViewById(R.id.webview);
        this.c = inflate.findViewById(R.id.loading_indicator);
        WebSettings settings = this.b.getSettings();
        settings.setCacheMode(2);
        settings.setJavaScriptEnabled(true);
        settings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        this.b.setWebChromeClient(new r());
        this.b.setWebViewClient(new s(this));
        b(170);
    }

    public static q a(Context context) {
        q qVar = new q(context);
        qVar.a(0, context.getString(R.string.dialog_btn_confim), (DialogInterface.OnClickListener) null);
        if (!a.a((CharSequence) null)) {
            qVar.a((String) null);
        }
        return qVar;
    }

    public final void a(String str) {
        this.b.loadUrl(a.a(str, "type", "dialog"));
    }

    public final void b(int i) {
        findViewById(R.id.dialog_webview_layout_root).setMinimumHeight(g.a((float) i));
    }
}
