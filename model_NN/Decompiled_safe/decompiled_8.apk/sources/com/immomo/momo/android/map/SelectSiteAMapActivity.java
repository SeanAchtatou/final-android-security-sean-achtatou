package com.immomo.momo.android.map;

import android.content.Intent;
import android.graphics.Bitmap;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.text.InputFilter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.amap.mapapi.core.GeoPoint;
import com.amap.mapapi.map.MapActivity;
import com.amap.mapapi.map.MapView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.a.hj;
import com.immomo.momo.android.b.p;
import com.immomo.momo.android.b.z;
import com.immomo.momo.android.view.HeaderLayout;
import com.immomo.momo.android.view.ResizeListenerLayout;
import com.immomo.momo.android.view.a.n;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.util.ao;
import com.immomo.momo.util.m;
import java.util.ArrayList;
import java.util.List;
import mm.purchasesdk.PurchaseCode;

public class SelectSiteAMapActivity extends MapActivity implements View.OnClickListener {
    ListView b = null;
    EditText c = null;
    p d = null;
    Handler e = new Handler();
    GeoPoint f = null;
    Bitmap g = null;
    String h = PoiTypeDef.All;
    boolean i;
    /* access modifiers changed from: private */
    public m j = new m(getClass().getSimpleName());
    private ResizeListenerLayout k = null;
    /* access modifiers changed from: private */
    public View l = null;
    private MapView m = null;
    /* access modifiers changed from: private */
    public Location n = null;
    /* access modifiers changed from: private */
    public Location o = null;
    /* access modifiers changed from: private */
    public int p = 0;
    /* access modifiers changed from: private */
    public hj q = null;
    /* access modifiers changed from: private */
    public az r = null;
    /* access modifiers changed from: private */
    public boolean s = false;
    /* access modifiers changed from: private */
    public List t = new ArrayList();
    /* access modifiers changed from: private */
    public int u = 0;
    /* access modifiers changed from: private */
    public TextView v = null;
    private String w = PoiTypeDef.All;
    /* access modifiers changed from: private */
    public v x = null;
    /* access modifiers changed from: private */
    public boolean y = true;

    /* access modifiers changed from: private */
    public void a() {
        if (z.a(this.n)) {
            b();
            return;
        }
        this.x = new v(this, (int) R.string.getting_loation);
        this.x.show();
        this.x.setOnCancelListener(new at(this));
        this.d = new au(this);
        try {
            z.a(new aw(this));
        } catch (Exception e2) {
            ao.g(R.string.errormsg_location_nearby_failed);
            c();
        }
    }

    /* access modifiers changed from: private */
    public void b() {
        if (this.o != null) {
            this.f = new GeoPoint((int) (this.o.getLatitude() * 1000000.0d), (int) (this.o.getLongitude() * 1000000.0d));
            if (this.f != null) {
                this.m.getController().animateTo(this.f);
                this.m.getController().setCenter(this.f);
            }
        }
        if (this.r != null && !this.r.isCancelled()) {
            this.r.cancel(true);
        }
        this.r = new az(this, this, null);
        this.r.execute(new Object[0]);
    }

    /* access modifiers changed from: private */
    public void c() {
        this.e.post(new ax(this));
        z.b(this.d);
        if (this.d != null) {
            this.d.b = false;
        }
        if (this.r != null && !this.r.isCancelled()) {
            this.r.cancel(true);
        }
    }

    /* access modifiers changed from: private */
    public String d() {
        switch (this.u) {
            case 0:
                this.w = getString(R.string.editsite_sitename0);
                break;
            case 1:
                this.w = getString(R.string.editsite_sitename1);
                break;
            case 2:
                this.w = getString(R.string.editsite_sitename2);
                break;
            case 3:
                this.w = getString(R.string.editsite_sitename3);
                break;
            case 4:
                this.w = getString(R.string.editsite_sitename4);
                break;
            default:
                this.w = getString(R.string.editsite_defaultbtn_text);
                break;
        }
        return this.w;
    }

    /* access modifiers changed from: protected */
    public boolean isRouteDisplayed() {
        return false;
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        switch (i2) {
            case PurchaseCode.INIT_OK /*100*/:
                if (i3 == -1) {
                    Intent intent2 = new Intent();
                    intent2.putExtra("siteid", PoiTypeDef.All);
                    intent2.putExtra("sitename", intent.getStringExtra("sitename"));
                    intent2.putExtra("sitetype", intent.getIntExtra("sitetype", -1));
                    intent2.putExtra("lat", this.n.getLatitude());
                    intent2.putExtra("lng", this.n.getLongitude());
                    intent2.putExtra("loctype", this.p);
                    setResult(-1, intent2);
                    finish();
                    return;
                }
                return;
            default:
                return;
        }
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.layout_sitename /*2131165794*/:
                ay ayVar = new ay(this);
                n nVar = new n(this);
                an anVar = new an(this, ayVar, nVar);
                nVar.setTitle((int) R.string.editsite_dialog_title);
                View inflate = getLayoutInflater().inflate((int) R.layout.dialog_select_sitetype, (ViewGroup) null);
                inflate.findViewById(R.id.layout_type_all).setOnClickListener(anVar);
                inflate.findViewById(R.id.layout_type_department).setOnClickListener(anVar);
                inflate.findViewById(R.id.layout_type_school).setOnClickListener(anVar);
                inflate.findViewById(R.id.layout_type_restaurant).setOnClickListener(anVar);
                inflate.findViewById(R.id.layout_type_office).setOnClickListener(anVar);
                inflate.findViewById(R.id.layout_type_trafic).setOnClickListener(anVar);
                inflate.findViewById(R.id.layout_type_entertainment).setOnClickListener(anVar);
                nVar.setContentView(inflate);
                nVar.setOnCancelListener(new ao(ayVar));
                nVar.show();
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_selectsite_autonavi);
        this.u = getIntent().getIntExtra("sitetype", 1);
        ((HeaderLayout) findViewById(R.id.layout_header)).setTitleText((int) R.string.editsite_header_title);
        this.k = (ResizeListenerLayout) findViewById(R.id.rootlayout);
        this.v = (TextView) findViewById(R.id.tv_sitename);
        this.v.setText(d());
        findViewById(R.id.mapcover);
        this.l = getLayoutInflater().inflate((int) R.layout.listitem_searchsite_footer, (ViewGroup) null);
        this.l.setClickable(true);
        this.l.requestFocus();
        this.b = (ListView) findViewById(R.id.listview);
        this.b.addFooterView(this.l);
        this.c = (EditText) findViewById(R.id.search_edittext);
        this.c.setHint((int) R.string.selectsite_search);
        findViewById(R.id.search_btn_clear);
        this.m = (MapView) findViewById(R.id.mapview);
        this.m.setBuiltInZoomControls(true);
        this.m.displayZoomControls(true);
        this.m.setEnabled(true);
        this.m.setClickable(true);
        this.m.setFocusable(false);
        this.m.setBuiltInZoomControls(false);
        this.m.getController().setZoom(19);
        ListView listView = this.b;
        hj hjVar = new hj(this);
        this.q = hjVar;
        listView.setAdapter((ListAdapter) hjVar);
        this.b.removeFooterView(this.l);
        this.m.getOverlays().add(new bb(this, this, this.m));
        findViewById(R.id.layout_sitename).setOnClickListener(this);
        this.k.setOnResizeListener(new am());
        ap apVar = new ap(this);
        this.c.setFilters(new InputFilter[]{apVar});
        this.c.addTextChangedListener(new aq(this));
        this.b.setOnItemClickListener(new as(this));
        a();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.r != null && !this.r.isCancelled()) {
            this.r.cancel(true);
            this.r = null;
        }
        c();
    }
}
