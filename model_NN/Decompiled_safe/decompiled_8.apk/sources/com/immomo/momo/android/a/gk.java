package com.immomo.momo.android.a;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.ImageBrowserActivity;
import com.immomo.momo.android.activity.OtherProfileActivity;
import com.immomo.momo.android.activity.emotestore.EmotionProfileActivity;
import com.immomo.momo.android.activity.feed.ad;
import com.immomo.momo.android.c.u;
import com.immomo.momo.android.view.HandyListView;
import com.immomo.momo.android.view.a.o;
import com.immomo.momo.g;
import com.immomo.momo.plugin.b.a;
import com.immomo.momo.service.bean.ae;
import com.immomo.momo.service.bean.aj;
import com.immomo.momo.service.bean.q;
import com.immomo.momo.util.j;
import com.immomo.momo.util.m;
import java.io.File;
import java.lang.ref.SoftReference;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class gk extends a implements View.OnClickListener {
    private static Map f = new HashMap(24);
    /* access modifiers changed from: private */
    public ad d = null;
    private HandyListView e = null;

    public gk(ad adVar, List list, HandyListView handyListView) {
        super(ad.H(), list);
        new m("test_momo", "[ -- FriendFeedAdapter -- ]");
        this.d = adVar;
        this.e = handyListView;
    }

    private static String a(String str) {
        Matcher matcher = Pattern.compile("(\\[[.[^\\[\\]]]*?\\|et\\|([.[^\\[\\]]]*?\\|)*[.[^\\[\\]]]*?\\])").matcher(str);
        if (matcher.find()) {
            return matcher.group();
        }
        return null;
    }

    private void a(ae aeVar, a aVar, ImageView imageView) {
        Bitmap t;
        SoftReference softReference = (SoftReference) f.get(aVar.g());
        Bitmap bitmap = softReference == null ? null : (Bitmap) softReference.get();
        if (bitmap != null && bitmap.isRecycled()) {
            f.remove(bitmap);
            bitmap = null;
        }
        if (bitmap != null) {
            t = (Bitmap) ((SoftReference) f.get(aVar.g())).get();
        } else {
            t = g.t();
            File a2 = q.a(aVar.g(), aVar.h());
            if (a2.exists()) {
                t = android.support.v4.b.a.l(a2.getPath());
                if (t != null) {
                    f.put(aVar.g(), new SoftReference(t));
                }
            } else if (!aeVar.isImageLoading()) {
                aeVar.setImageLoading(true);
                u.b().execute(new gm(this, aVar));
            }
        }
        imageView.setImageBitmap(t);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, com.immomo.momo.android.view.HandyListView):void
     arg types: [com.immomo.momo.service.bean.bf, android.widget.ImageView, com.immomo.momo.android.view.HandyListView]
     candidates:
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, int):void
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup):void
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, com.immomo.momo.android.view.HandyListView):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup, int, boolean, boolean, int):void
     arg types: [com.immomo.momo.service.bean.ab, android.widget.ImageView, com.immomo.momo.android.view.HandyListView, int, int, int, int]
     candidates:
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, int, android.view.View, android.view.ViewGroup, boolean, int, boolean):com.immomo.momo.android.c.o
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup, int, boolean, boolean, int):void */
    public final View getView(int i, View view, ViewGroup viewGroup) {
        go goVar;
        if (view == null) {
            view = g.o().inflate((int) R.layout.listitem_feed_mycomment, (ViewGroup) null);
            goVar = new go((byte) 0);
            view.setTag(R.id.tag_userlist_item, goVar);
            goVar.f844a = (TextView) view.findViewById(R.id.tv_comment_time);
            goVar.b = (TextView) view.findViewById(R.id.tv_comment_content);
            goVar.c = (TextView) view.findViewById(R.id.tv_comment_replycontent);
            goVar.d = (ImageView) view.findViewById(R.id.iv_comment_replyimage);
            goVar.e = (ImageView) view.findViewById(R.id.iv_comment_emotion);
            goVar.f = (ImageView) view.findViewById(R.id.iv_comment_emotion_src);
            goVar.g = (ImageView) view.findViewById(R.id.iv_comment_photo);
            goVar.h = (TextView) view.findViewById(R.id.tv_comment_name);
            goVar.i = view.findViewById(R.id.layout_comment_content);
            goVar.g.setOnClickListener(this);
            goVar.d.setOnClickListener(this);
            goVar.i.setOnClickListener(this);
        } else {
            goVar = (go) view.getTag(R.id.tag_userlist_item);
        }
        ae aeVar = (ae) getItem(i);
        goVar.g.setTag(R.id.tag_item_position, Integer.valueOf(i));
        goVar.d.setTag(R.id.tag_item_position, Integer.valueOf(i));
        goVar.i.setTag(R.id.tag_item_position, Integer.valueOf(i));
        goVar.e.setTag(R.id.tag_item_position, Integer.valueOf(i));
        goVar.f.setTag(R.id.tag_item_position, Integer.valueOf(i));
        if (aeVar.f2985a != null) {
            goVar.h.setText(aeVar.f2985a.h());
            if (aeVar.f2985a.b()) {
                goVar.h.setTextColor(g.c((int) R.color.font_vip_name));
            } else {
                goVar.h.setTextColor(g.c((int) R.color.font_value));
            }
        } else {
            goVar.h.setText(aeVar.b);
        }
        j.a((aj) aeVar.f2985a, goVar.g, this.e);
        goVar.f844a.setText(aeVar.e);
        goVar.b.setText(aeVar.f);
        goVar.c.setText(aeVar.j);
        if (aeVar.n == 1) {
            String a2 = a(aeVar.f);
            String str = null;
            if (android.support.v4.b.a.f(a2)) {
                goVar.e.setVisibility(0);
                str = aeVar.f.replace(a2, PoiTypeDef.All);
                Context context = this.b;
                a aVar = new a(a2);
                a(aeVar, aVar, goVar.e);
                goVar.e.setTag(R.id.tag_item_emotionspan, aVar);
            }
            if (android.support.v4.b.a.f(str)) {
                goVar.b.setVisibility(0);
                goVar.b.setText(str);
            } else {
                goVar.b.setVisibility(8);
            }
        } else {
            goVar.e.setVisibility(8);
            goVar.b.setVisibility(0);
            goVar.b.setText(aeVar.f);
        }
        String a3 = a(aeVar.j);
        if (android.support.v4.b.a.f(a3)) {
            goVar.f.setVisibility(0);
            goVar.c.setVisibility(8);
            aeVar.f.replace(a3, PoiTypeDef.All);
            Context context2 = this.b;
            a aVar2 = new a(a3);
            a(aeVar, aVar2, goVar.f);
            goVar.f.setTag(R.id.tag_item_emotionspan, aVar2);
        } else {
            goVar.f.setVisibility(8);
            goVar.c.setVisibility(0);
            goVar.c.setText(aeVar.j);
        }
        if (aeVar.o != 1 ? false : aeVar.g == null ? false : !android.support.v4.b.a.f(aeVar.g.getLoadImageId()) ? false : aeVar.g.i != 2) {
            goVar.d.setVisibility(0);
            j.a((aj) aeVar.g, goVar.d, (ViewGroup) this.e, 15, false, true, g.a(8.0f));
        } else {
            goVar.d.setVisibility(8);
        }
        return view;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public final void onClick(View view) {
        int intValue = ((Integer) view.getTag(R.id.tag_item_position)).intValue();
        switch (view.getId()) {
            case R.id.iv_comment_photo /*2131166104*/:
                Intent intent = new Intent(this.d.c(), OtherProfileActivity.class);
                intent.putExtra("momoid", ((ae) getItem(intValue)).b);
                this.d.a(intent);
                return;
            case R.id.layout_comment_content /*2131166667*/:
                ae aeVar = (ae) getItem(intValue);
                o oVar = new o(this.d.c(), aeVar.o == 2 ? R.array.event_comments : R.array.feed_comments);
                oVar.a(new gl(this, aeVar));
                oVar.show();
                return;
            case R.id.iv_comment_replyimage /*2131166670*/:
                String str = ((ae) getItem(intValue)).g.k;
                if (android.support.v4.b.a.f(str)) {
                    Intent intent2 = new Intent(this.d.c(), EmotionProfileActivity.class);
                    intent2.putExtra("eid", str);
                    this.d.a(intent2);
                    return;
                }
                Intent intent3 = new Intent(this.d.c(), ImageBrowserActivity.class);
                intent3.putExtra("array", new String[]{((ae) getItem(intValue)).g.getLoadImageId()});
                intent3.putExtra("imagetype", "feed");
                intent3.putExtra("autohide_header", true);
                this.d.a(intent3);
                this.d.b((int) R.anim.normal);
                return;
            default:
                return;
        }
    }
}
