package com.immomo.momo.android.activity.group;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.protocol.a.a.h;
import com.immomo.momo.protocol.a.o;
import com.immomo.momo.service.bean.a.e;

final class bv extends d {

    /* renamed from: a  reason: collision with root package name */
    private h f1569a;
    private /* synthetic */ GroupFeedsActivity c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public bv(GroupFeedsActivity groupFeedsActivity, Context context) {
        super(context);
        this.c = groupFeedsActivity;
    }

    /* access modifiers changed from: protected */
    public final Object a(Object... objArr) {
        this.f1569a = o.a().a(this.c.u, this.c.A.getCount());
        this.c.B.a(this.f1569a.e, this.c.u, false);
        return null;
    }

    /* access modifiers changed from: protected */
    public final void a() {
    }

    /* access modifiers changed from: protected */
    public final void a(Object obj) {
        if (this.f1569a.e.isEmpty() || !this.f1569a.b) {
            this.c.j.setVisibility(8);
        } else {
            this.c.j.setVisibility(0);
        }
        for (e eVar : this.f1569a.e) {
            if (!this.c.y.contains(eVar)) {
                this.c.A.b(eVar);
                this.c.y.add(eVar);
            }
        }
        this.c.A.notifyDataSetChanged();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.c.j.e();
    }
}
