package com.immomo.momo.android.view.photoview;

import android.annotation.TargetApi;
import android.content.Context;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;

@TargetApi(8)
final class k extends j {
    private final ScaleGestureDetector d;
    private final ScaleGestureDetector.OnScaleGestureListener e = new l(this);

    public k(Context context) {
        super(context);
        this.d = new ScaleGestureDetector(context, this.e);
    }

    public final boolean a() {
        return this.d.isInProgress();
    }

    public final boolean a(MotionEvent motionEvent) {
        this.d.onTouchEvent(motionEvent);
        return super.a(motionEvent);
    }
}
