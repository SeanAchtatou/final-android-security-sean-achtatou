package com.immomo.momo.android.activity.event;

import android.support.v4.app.g;
import android.view.View;
import com.immomo.momo.R;
import com.immomo.momo.android.view.RotatingImageView;
import com.immomo.momo.android.view.a.am;
import com.immomo.momo.service.bean.v;
import com.immomo.momo.service.bean.w;
import com.immomo.momo.service.bean.x;
import mm.purchasesdk.PurchaseCode;

final class be implements View.OnClickListener {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public /* synthetic */ bd f1374a;

    be(bd bdVar) {
        this.f1374a = bdVar;
    }

    public final void onClick(View view) {
        am amVar;
        switch (view.getId()) {
            case R.id.nearbyevent_layout_time /*2131165764*/:
                g c = this.f1374a.c();
                String[] c2 = v.c();
                this.f1374a.Y.ordinal();
                am amVar2 = new am(c, c2, (byte) 0);
                amVar2.a(new bg(this));
                amVar2.a(view);
                amVar = amVar2;
                break;
            case R.id.nearbyevent_tv_time /*2131165765*/:
            case R.id.imageview /*2131165766*/:
            case R.id.nearbyevent_tv_type /*2131165768*/:
            default:
                return;
            case R.id.nearbyevent_layout_type /*2131165767*/:
                g c3 = this.f1374a.c();
                String[] c4 = x.c();
                this.f1374a.Z.ordinal();
                am amVar3 = new am(c3, c4, (byte) 0);
                amVar3.a(new bh(this));
                amVar3.a(view);
                amVar = amVar3;
                break;
            case R.id.nearbyevent_layout_order /*2131165769*/:
                g c5 = this.f1374a.c();
                String[] c6 = w.c();
                this.f1374a.aa.ordinal();
                am amVar4 = new am(c5, c6, (byte) 0);
                amVar4.a(new bf(this));
                amVar4.a(view);
                amVar = amVar4;
                break;
        }
        RotatingImageView rotatingImageView = (RotatingImageView) view.findViewById(R.id.imageview);
        rotatingImageView.a(0, -180, PurchaseCode.UNSUPPORT_ENCODING_ERR);
        amVar.a(new bi(rotatingImageView));
    }
}
