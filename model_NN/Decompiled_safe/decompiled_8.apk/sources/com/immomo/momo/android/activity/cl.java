package com.immomo.momo.android.activity;

import android.graphics.RectF;
import android.view.MotionEvent;
import android.view.View;
import com.immomo.momo.R;
import com.immomo.momo.g;

final class cl implements View.OnTouchListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ EditUserPhotoActivity f1069a;

    cl(EditUserPhotoActivity editUserPhotoActivity) {
        this.f1069a = editUserPhotoActivity;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        if (this.f1069a.h.getDraggedPosition() == -1) {
            if (motionEvent.getAction() == 0) {
                if (RectF.intersects(new RectF(motionEvent.getRawX() - 2.0f, motionEvent.getRawY() - 2.0f, motionEvent.getRawX() + 2.0f, motionEvent.getRawY() + 2.0f), g.a(this.f1069a.k))) {
                    this.f1069a.l.setBackgroundResource(R.drawable.bg_multiselect_press);
                    this.f1069a.n = true;
                }
            } else if ((motionEvent.getAction() == 1 || motionEvent.getAction() == 3) && this.f1069a.n) {
                this.f1069a.l.setBackgroundResource(R.drawable.bg_multiselect);
                this.f1069a.a((CharSequence) "拖拽图片到这里删除");
                this.f1069a.n = false;
            }
        }
        return false;
    }
}
