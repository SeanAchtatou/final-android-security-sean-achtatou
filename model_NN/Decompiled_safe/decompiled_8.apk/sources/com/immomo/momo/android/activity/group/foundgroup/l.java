package com.immomo.momo.android.activity.group.foundgroup;

import android.graphics.Bitmap;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import com.immomo.momo.R;
import com.immomo.momo.service.bean.al;
import com.immomo.momo.util.j;
import com.immomo.momo.util.k;
import java.io.File;

public final class l extends a implements View.OnClickListener {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public FoundGroupActivity f1670a;
    private ImageView b = null;
    /* access modifiers changed from: private */
    public File c = null;
    /* access modifiers changed from: private */
    public s d;
    private CheckBox e;
    private CheckBox f;

    public l(View view, FoundGroupActivity foundGroupActivity, s sVar) {
        super(view);
        this.f1670a = foundGroupActivity;
        this.d = sVar;
        this.b = (ImageView) a((int) R.id.iv_groupphoto);
        this.f = (CheckBox) a((int) R.id.cb_choosepic);
        this.e = (CheckBox) a((int) R.id.cb_defaultpic);
        a((int) R.id.layout_choosepic).setOnClickListener(this);
        a((int) R.id.layout_defaultpic).setOnClickListener(this);
        if (this.d.b) {
            this.f.setChecked(false);
            this.e.setChecked(true);
            g();
            return;
        }
        if (this.d.f1677a != null) {
            this.c = this.d.f1677a;
            a(j.a(this.d.f1677a));
        }
        this.f.setChecked(true);
        this.e.setChecked(false);
    }

    private void g() {
        j.a(new al(this.f1670a.c(this.d.g)), this.b, (ViewGroup) null, 3);
    }

    public final void a(Bitmap bitmap) {
        if (bitmap == null) {
            this.b.setImageResource(R.drawable.ic_common_def_header);
        } else {
            this.b.setImageBitmap(bitmap);
        }
    }

    public final void a(File file) {
        this.c = file;
    }

    public final void b() {
    }

    /* access modifiers changed from: protected */
    public final boolean c() {
        new m(this).execute(new s[]{this.d});
        return false;
    }

    /* access modifiers changed from: protected */
    public final void d() {
        new k("PI", "P645").e();
    }

    /* access modifiers changed from: protected */
    public final void e() {
        new k("PO", "P645").e();
    }

    public final void f() {
        this.e.setChecked(false);
        this.f.setChecked(true);
        this.d.b = false;
    }

    public final void onClick(View view) {
        switch (view.getId()) {
            case R.id.layout_defaultpic /*2131166148*/:
                this.d.b = true;
                this.e.setChecked(true);
                this.f.setChecked(false);
                g();
                return;
            case R.id.cb_defaultpic /*2131166149*/:
            default:
                return;
            case R.id.layout_choosepic /*2131166150*/:
                this.f1670a.u();
                new k("C", "C64501").e();
                return;
        }
    }
}
