package com.immomo.momo.android.activity.tieba;

import android.content.Context;
import com.immomo.momo.a.h;
import com.immomo.momo.android.c.d;
import com.immomo.momo.protocol.a.v;

final class ez extends d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ TiebaProfileActivity f2276a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ez(TiebaProfileActivity tiebaProfileActivity, Context context) {
        super(context);
        this.f2276a = tiebaProfileActivity;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        com.immomo.momo.service.bean.c.d f = v.a().f(this.f2276a.k);
        this.f2276a.m.a(f);
        return f;
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        if (exc instanceof h) {
            a(exc.getMessage());
            this.f2276a.finish();
            return;
        }
        super.a(exc);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        this.f2276a.l = (com.immomo.momo.service.bean.c.d) obj;
        this.f2276a.v();
        this.f2276a.setTitle(this.f2276a.l.b);
    }
}
