package com.immomo.momo.android.pay;

import android.content.DialogInterface;
import android.os.Message;

final class g implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ BuyMemberActivity f2558a;

    g(BuyMemberActivity buyMemberActivity) {
        this.f2558a = buyMemberActivity;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        if (!this.f2558a.A) {
            Message message = new Message();
            message.what = 124;
            this.f2558a.b(new r(this.f2558a, this.f2558a, message));
            return;
        }
        this.f2558a.b(new j(this.f2558a, this.f2558a));
    }
}
