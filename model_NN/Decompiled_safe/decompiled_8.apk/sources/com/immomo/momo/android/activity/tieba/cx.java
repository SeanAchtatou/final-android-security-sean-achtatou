package com.immomo.momo.android.activity.tieba;

import android.content.Context;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.android.c.d;
import com.immomo.momo.protocol.a.v;
import com.immomo.momo.service.bean.c.e;

final class cx extends d {

    /* renamed from: a  reason: collision with root package name */
    private e f2225a;
    private int c;
    private String d;
    private /* synthetic */ TiebaAdminActivity e;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public cx(TiebaAdminActivity tiebaAdminActivity, Context context, e eVar, int i, String str) {
        super(context);
        this.e = tiebaAdminActivity;
        this.f2225a = eVar;
        this.c = i;
        this.d = str;
    }

    /* access modifiers changed from: protected */
    public final Object a(Object... objArr) {
        String str = PoiTypeDef.All;
        switch (this.c) {
            case 1:
                v.a().a(this.f2225a.c, (String) null, this.f2225a.f, this.f2225a.o, this.d);
                str = "删除并禁言";
                break;
            case 2:
                str = "删除";
                v.a().f(this.f2225a.f, this.d);
                this.f2225a.n = true;
                break;
            case 3:
                v.a().a((String) null, this.f2225a.f, this.f2225a.o);
                str = "请求封禁";
                break;
            case 4:
                v.a().b((String) null, this.f2225a.f);
                this.e.k.a().remove(this.f2225a);
                break;
        }
        this.f2225a.i++;
        this.f2225a.h = "吧主" + this.e.f.h() + "操作:" + str;
        if (this.e.k.getCount() > 20) {
            this.e.j.b(this.e.k.a().subList(0, 20));
        } else {
            this.e.j.b(this.e.k.a());
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
        this.e.a(new com.immomo.momo.android.view.a.v(f(), this));
    }

    /* access modifiers changed from: protected */
    public final void a(Object obj) {
        a("处理成功");
        this.e.k.notifyDataSetChanged();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        super.b();
        this.e.p();
    }
}
