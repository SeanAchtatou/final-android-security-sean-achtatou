package com.immomo.momo.android.pay;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.b.a;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.android.a.gf;
import com.immomo.momo.android.activity.ah;
import com.immomo.momo.android.activity.common.SelectSingleTabsActivity;
import com.immomo.momo.android.c.r;
import com.immomo.momo.android.view.FillWidthImageView;
import com.immomo.momo.service.aq;
import com.immomo.momo.service.bean.aj;
import com.immomo.momo.service.bean.an;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.util.h;
import com.immomo.momo.util.j;
import com.immomo.momo.util.k;
import java.io.File;
import java.io.IOException;
import mm.purchasesdk.PurchaseCode;
import org.json.JSONException;

public class MemberCenterActivity extends ah implements View.OnClickListener, AdapterView.OnItemClickListener {
    private TextView h = null;
    private TextView i = null;
    private aq j;
    private Button k;
    private Button l;
    private ListView m;
    private View n;
    private View o;
    /* access modifiers changed from: private */
    public gf p;
    /* access modifiers changed from: private */
    public ai q;
    private FillWidthImageView r;
    private bf s = null;

    static /* synthetic */ void a(MemberCenterActivity memberCenterActivity, String str, String str2, String str3) {
        if (!memberCenterActivity.isFinishing()) {
            r rVar = new r(new StringBuilder().append(System.currentTimeMillis()).toString(), new ad(memberCenterActivity, str2, str3), 17, null);
            rVar.a(str);
            new Thread(rVar).start();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup, int, boolean, boolean, int):void
     arg types: [com.immomo.momo.service.bean.al, com.immomo.momo.android.view.FillWidthImageView, ?[OBJECT, ARRAY], int, int, int, int]
     candidates:
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, int, android.view.View, android.view.ViewGroup, boolean, int, boolean):com.immomo.momo.android.c.o
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup, int, boolean, boolean, int):void */
    /* access modifiers changed from: private */
    public void u() {
        if (this.f.b()) {
            this.o.setVisibility(0);
            this.r.setVisibility(8);
            this.k.setText("续费");
            this.h.setText(this.f.b() ? this.f.c() ? R.string.payyearvip_member : R.string.payvip_member : R.string.payvip_notmember);
            if (this.f.aj != null) {
                this.e.b((Object) ("currentUser.expireTime=" + this.f.aj));
                this.i.setText(a.d(this.f.aj));
                return;
            }
            return;
        }
        this.k.setText("开通会员");
        this.r.setVisibility(0);
        this.o.setVisibility(8);
        if (this.r != null && this.q != null) {
            j.a((aj) this.q.a(), (ImageView) this.r, (ViewGroup) null, 18, true, false, 0);
        }
    }

    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        super.a(bundle);
        setContentView((int) R.layout.activity_member_center);
        this.m = (ListView) findViewById(R.id.membericenter_listview);
        this.n = LayoutInflater.from(this).inflate((int) R.layout.include_membercentervipheader, (ViewGroup) null);
        this.o = this.n.findViewById(R.id.layout_vip);
        this.h = (TextView) this.n.findViewById(R.id.tv_member_status);
        this.i = (TextView) this.n.findViewById(R.id.tv_expiredtime);
        this.r = (FillWidthImageView) this.n.findViewById(R.id.layout_image_header);
        this.m.addHeaderView(this.n);
        this.m.addFooterView(LayoutInflater.from(this).inflate((int) R.layout.include_emotion_footer, (ViewGroup) null));
        this.k = (Button) findViewById(R.id.btn_submit);
        this.l = (Button) findViewById(R.id.btn_gift);
        setTitle("会员介绍");
        this.p = new gf(this);
        this.m.setAdapter((ListAdapter) this.p);
        d();
        this.k.setOnClickListener(this);
        this.l.setOnClickListener(this);
        this.m.setOnItemClickListener(this);
        if (((Boolean) this.g.b("show_vipdialog", true)).booleanValue() && this.f.b()) {
            new ag(this, this).execute(new String[0]);
        }
    }

    /* access modifiers changed from: protected */
    public final void d() {
        this.j = new aq();
        try {
            String a2 = h.a(new File(com.immomo.momo.a.u(), "membercenter_file"));
            if (!a.a((CharSequence) a2)) {
                com.immomo.momo.protocol.a.a.a();
                this.q = com.immomo.momo.protocol.a.a.a(a2);
                if (!(this.q == null || this.q.c == null)) {
                    this.p.a(this.q.c);
                }
            }
        } catch (IOException e) {
            this.e.a((Throwable) e);
        } catch (JSONException e2) {
            this.e.a((Throwable) e2);
        }
        new ah(this, this).execute(new Object[0]);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        if (i2 == 312 && i3 == -1) {
            String stringExtra = intent.getStringExtra("smomoid");
            if (!a.a((CharSequence) stringExtra)) {
                this.s = this.j.b(stringExtra);
                if (this.s == null) {
                    this.s = new bf(stringExtra);
                }
                Intent intent2 = new Intent(this, BuyMemberActivity.class);
                intent2.putExtra("key_isGift", true);
                intent2.putExtra("key_remoteid", this.s.h);
                startActivity(intent2);
            }
        } else if (i2 == 200) {
            u();
        }
        super.onActivityResult(i2, i3, intent);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_submit /*2131165424*/:
                new k("C", "C9302").e();
                Intent intent = new Intent(this, BuyMemberActivity.class);
                if (!this.f.b()) {
                    intent.putExtra("key_is_openmember", true);
                }
                startActivityForResult(intent, PurchaseCode.LOADCHANNEL_ERR);
                return;
            case R.id.btn_gift /*2131165760*/:
                new k("C", "C9303").e();
                Intent intent2 = new Intent(this, SelectSingleTabsActivity.class);
                intent2.putExtra("title", "选择赠送好友");
                startActivityForResult(intent2, 312);
                return;
            default:
                return;
        }
    }

    public void onItemClick(AdapterView adapterView, View view, int i2, long j2) {
        an a2;
        if (i2 != 0 && i2 <= this.p.getCount() && (a2 = this.p.getItem(i2 - 1)) != null && a2.d == 2) {
            Intent intent = new Intent(this, MemIntroductionDetailActivity.class);
            intent.putExtra("webview_title", a2.f2992a);
            intent.putExtra("webview_url", a2.c);
            startActivity(intent);
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        new k("PO", "P93").e();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        u();
        new k("PI", "P93").e();
    }
}
