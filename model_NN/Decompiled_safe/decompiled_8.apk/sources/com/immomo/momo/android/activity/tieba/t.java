package com.immomo.momo.android.activity.tieba;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.service.j;

final class t extends d {

    /* renamed from: a  reason: collision with root package name */
    private v f2300a = null;
    private /* synthetic */ EditTieActivity c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public t(EditTieActivity editTieActivity, Context context) {
        super(context);
        this.c = editTieActivity;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        return com.immomo.momo.protocol.a.v.a().a(this.c.p.getText().toString().trim(), this.c.o.getText().toString().trim(), this.c.y.c, this.c.u);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.f2300a = new v(this.c.n(), "请稍候，正在提交...");
        this.f2300a.setOnCancelListener(new u(this));
        this.c.a(this.f2300a);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        EditTieActivity.h(this.c);
        EditTieActivity.i(this.c);
        j.a().a(this.c);
        this.c.finish();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.c.p();
    }
}
