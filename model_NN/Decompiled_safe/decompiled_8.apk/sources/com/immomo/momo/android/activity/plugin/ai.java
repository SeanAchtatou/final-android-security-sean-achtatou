package com.immomo.momo.android.activity.plugin;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.immomo.momo.R;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.util.ao;

final class ai extends WebViewClient {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ BindTXActivity f2036a;

    private ai(BindTXActivity bindTXActivity) {
        this.f2036a = bindTXActivity;
    }

    /* synthetic */ ai(BindTXActivity bindTXActivity, byte b) {
        this(bindTXActivity);
    }

    public final void onLoadResource(WebView webView, String str) {
        super.onLoadResource(webView, str);
    }

    public final void onPageFinished(WebView webView, String str) {
        this.f2036a.p();
        if (Build.VERSION.SDK_INT > 10 || str.indexOf("code=") < 0) {
            super.onPageFinished(webView, str);
            return;
        }
        this.f2036a.m = Uri.parse(str).getQueryParameter("code");
        new ae(this.f2036a, this.f2036a, this.f2036a.m, "0").execute(new Object[0]);
    }

    public final void onPageStarted(WebView webView, String str, Bitmap bitmap) {
        super.onPageStarted(webView, str, bitmap);
        if (this.f2036a.l == null) {
            this.f2036a.l = new v(this.f2036a);
        }
        this.f2036a.l.a("正在加载，请稍候...");
        this.f2036a.a(this.f2036a.l);
    }

    public final void onReceivedError(WebView webView, int i, String str, String str2) {
        if (i == -2) {
            ao.e(R.string.errormsg_network_unfind);
        } else {
            ao.e(R.string.errormsg_server);
        }
        this.f2036a.p();
    }

    public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
        if (Build.VERSION.SDK_INT <= 10) {
            if (str.indexOf(this.f2036a.i) >= 0) {
                return true;
            }
        } else if (str.indexOf("code=") >= 0) {
            this.f2036a.m = Uri.parse(str).getQueryParameter("code");
            new ae(this.f2036a, this.f2036a, this.f2036a.m, "0").execute(new Object[0]);
            return true;
        }
        return super.shouldOverrideUrlLoading(webView, str);
    }
}
