package com.immomo.momo.android.activity.account;

import android.content.DialogInterface;
import android.support.v4.b.a;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.SpannableStringBuilder;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.TranslateAnimation;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.android.view.a.n;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.util.ao;
import com.immomo.momo.util.e;
import com.immomo.momo.util.k;
import com.immomo.momo.util.m;

public final class bn extends ab implements TextWatcher, TextView.OnEditorActionListener {

    /* renamed from: a  reason: collision with root package name */
    private EditText f967a = null;
    private TextView b = null;
    /* access modifiers changed from: private */
    public TextView c = null;
    /* access modifiers changed from: private */
    public TextView d = null;
    private TextView e = null;
    private View f = null;
    /* access modifiers changed from: private */
    public bf g = null;
    /* access modifiers changed from: private */
    public String[] h = null;
    /* access modifiers changed from: private */
    public cb i = null;
    /* access modifiers changed from: private */
    public boolean j = false;
    /* access modifiers changed from: private */
    public String k = null;
    private TextView l = null;
    /* access modifiers changed from: private */
    public ImageView m = null;
    /* access modifiers changed from: private */
    public EditText n = null;
    /* access modifiers changed from: private */
    public n o = null;
    /* access modifiers changed from: private */
    public String p = "empty";
    /* access modifiers changed from: private */
    public RegisterActivityWithP q = null;

    public bn(bf bfVar, View view, RegisterActivityWithP registerActivityWithP) {
        super(view);
        new m("test_momo", "[ -- StepPhone -- ]");
        this.g = bfVar;
        this.q = registerActivityWithP;
        this.q.c(RegisterActivityWithP.h);
        this.h = a.d();
        if (!a.f(this.g.b)) {
            String line1Number = ((TelephonyManager) this.q.getSystemService("phone")).getLine1Number();
            if (a.f(line1Number)) {
                String e2 = a.e(line1Number);
                if (a.f(e2)) {
                    this.g.c = e2;
                    this.g.b = line1Number.substring(e2.length());
                    this.q.v = this.g.b;
                }
            }
        }
        if (a.a((CharSequence) this.g.c)) {
            this.g.c = "+86";
        }
        new k("PI", "P121").e();
        this.f = a((int) R.id.layout_button_content);
        this.d = (TextView) a((int) R.id.rg_tv_phone_notice);
        this.f967a = (EditText) a((int) R.id.rg_et_phone);
        this.f967a.addTextChangedListener(this);
        this.f967a.setOnEditorActionListener(this);
        this.f967a.setText(this.g.b);
        this.b = (TextView) a((int) R.id.rg_link_email);
        this.c = (TextView) a((int) R.id.rg_et_areacode);
        this.c.setText(this.g.c);
        this.c.setOnClickListener(new bo(this));
        if (this.f967a.getText().length() > 0) {
            this.d.setVisibility(0);
            this.f.setPadding(0, g.a(58.0f), 0, 0);
        }
        this.e = (TextView) a((int) R.id.rg_tv_note);
        String charSequence = this.e.getText().toString();
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(charSequence);
        spannableStringBuilder.setSpan(new bq(this), charSequence.indexOf("陌陌用户协议"), charSequence.indexOf("陌陌用户协议") + 6, 33);
        this.e.setMovementMethod(LinkMovementMethod.getInstance());
        this.e.setText(spannableStringBuilder);
        e.a(this.e, charSequence.indexOf("陌陌用户协议"), charSequence.indexOf("陌陌用户协议") + 6);
        this.b.setText("使用电子邮箱注册");
        this.c.setVisibility(0);
        this.f967a.setVisibility(0);
        SpannableStringBuilder spannableStringBuilder2 = new SpannableStringBuilder(this.b.getText());
        spannableStringBuilder2.setSpan(new br(this), 0, 8, 33);
        this.b.setMovementMethod(LinkMovementMethod.getInstance());
        this.b.setText(spannableStringBuilder2);
        e.a(this.b, 0, 8);
    }

    public final boolean a() {
        if (a(this.f967a)) {
            ao.b("请填写手机号码");
            this.q.a((TextView) this.f967a);
            return false;
        }
        String trim = this.f967a.getText().toString().trim();
        if (trim.length() < 3) {
            new k("U", "U11").e();
            ao.e(R.string.reg_phone_formaterror);
            return false;
        } else if (!"+86".equals(this.g.c) || (trim.length() == 11 && "1".equals(trim.substring(0, 1)))) {
            this.g.b = trim;
            if (!a.a((CharSequence) this.q.x) && this.g.b.equals(this.q.x) && this.g.c.equals(this.q.y)) {
                this.g.f = true;
                this.q.z++;
            }
            return true;
        } else {
            ao.e(R.string.reg_phone_formaterror);
            return false;
        }
    }

    public final void afterTextChanged(Editable editable) {
        if (editable.length() > 0) {
            if (this.d.isShown() || this.f.getAnimation() != null) {
                this.d.setVisibility(0);
                this.f.setPadding(0, g.a(58.0f), 0, 0);
            } else {
                this.f.setPadding(0, this.d.getHeight() + g.a(8.0f), 0, 0);
                TranslateAnimation translateAnimation = new TranslateAnimation(1, 0.0f, 1, 0.0f, 0, (float) (-g.a(58.0f)), 2, 0.0f);
                translateAnimation.setDuration(100);
                translateAnimation.setAnimationListener(new bt(this));
                this.f.setAnimation(translateAnimation);
            }
            String editable2 = editable.toString();
            StringBuilder sb = new StringBuilder();
            for (int i2 = 0; i2 < editable2.length(); i2++) {
                sb.append(editable2.charAt(i2));
                if (i2 == 2) {
                    sb.append(" ");
                } else if ((i2 - 2) % 4 == 0) {
                    sb.append(" ");
                }
            }
            this.d.setText(sb);
            return;
        }
        this.d.setVisibility(4);
        TranslateAnimation translateAnimation2 = new TranslateAnimation(1, 0.0f, 1, 0.0f, 0, (float) g.a(58.0f), 2, 0.0f);
        translateAnimation2.setDuration(100);
        this.f.setPadding(0, 0, 0, g.a(58.0f));
        this.f.setAnimation(translateAnimation2);
    }

    public final void b() {
        if (this.f967a.getText().length() > 0) {
            this.d.setVisibility(0);
            this.f.setPadding(0, g.a(58.0f), 0, 0);
            return;
        }
        this.d.setVisibility(4);
        this.f.setPadding(0, 0, 0, g.a(58.0f));
    }

    public final void beforeTextChanged(CharSequence charSequence, int i2, int i3, int i4) {
    }

    public final void c() {
        boolean z = false;
        new k("PO", "P121");
        if (a()) {
            if (this.q.z == 1) {
                this.p = String.valueOf(this.g.c) + this.g.b;
                z = true;
            } else if (!a.f(this.p) || !this.p.equals(String.valueOf(this.g.c) + this.g.b)) {
                this.g.f = false;
                this.g.e = null;
                this.q.s = false;
                this.q.t = false;
                this.q.b(new bx(this, this.q));
                this.q.u = true;
            } else {
                z = true;
            }
            if (!z) {
                return;
            }
            if (this.q.z == 1) {
                this.q.z++;
                this.q.d(3);
            } else if (this.q.t || this.q.s) {
                this.q.d(2);
            } else {
                this.q.u();
            }
        }
    }

    public final void d() {
        super.d();
        this.q.i();
    }

    public final void e() {
        new k("PI", "P121").e();
    }

    public final void f() {
        new k("PO", "P121").e();
    }

    public final void g() {
        this.b.setVisibility(8);
    }

    public final void h() {
        if (this.o != null) {
            this.o.dismiss();
        }
        this.j = false;
        View inflate = g.o().inflate((int) R.layout.include_register_1_scode, (ViewGroup) null);
        this.o = new n(this.q);
        this.o.a(0, (int) R.string.dialog_btn_confim, new bu(this));
        this.o.a(1, (int) R.string.dialog_btn_cancel, (DialogInterface.OnClickListener) null);
        this.o.setTitle("输入验证码");
        this.o.a();
        this.o.setContentView(inflate);
        this.o.show();
        this.o.setOnDismissListener(new bv(this));
        this.l = (TextView) inflate.findViewById(R.id.scode_tv_reload);
        this.m = (ImageView) inflate.findViewById(R.id.scode_iv_code);
        this.n = (EditText) inflate.findViewById(R.id.scode_et_inputcode);
        e.a(this.l, 0, this.l.getText().length());
        this.l.setOnClickListener(new bw(this));
        this.q.b(new cb(this, this.q));
    }

    public final boolean onEditorAction(TextView textView, int i2, KeyEvent keyEvent) {
        if (textView.getId() != R.id.rg_et_phone || 5 != i2) {
            return false;
        }
        c();
        return true;
    }

    public final void onTextChanged(CharSequence charSequence, int i2, int i3, int i4) {
    }
}
