package com.immomo.momo.android.map;

import android.content.Intent;
import android.view.View;
import com.amap.mapapi.poisearch.PoiTypeDef;

final class bh implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ bg f2487a;
    private final /* synthetic */ CharSequence b;

    bh(bg bgVar, CharSequence charSequence) {
        this.f2487a = bgVar;
        this.b = charSequence;
    }

    public final void onClick(View view) {
        this.f2487a.f2486a.k.a((Object) "footview clicked");
        if (this.f2487a.f2486a.n != null) {
            Intent intent = new Intent();
            intent.putExtra("siteid", PoiTypeDef.All);
            intent.putExtra("sitename", this.b.toString().trim());
            intent.putExtra("sitetype", this.f2487a.f2486a.u);
            intent.putExtra("lat", this.f2487a.f2486a.n.getLatitude());
            intent.putExtra("lng", this.f2487a.f2486a.n.getLongitude());
            intent.putExtra("loctype", this.f2487a.f2486a.p);
            this.f2487a.f2486a.setResult(-1, intent);
            this.f2487a.f2486a.finish();
        }
    }
}
