package com.immomo.momo.android.activity.plugin;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.support.v4.b.a;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.immomo.momo.R;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.util.ao;

final class u extends WebViewClient {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ BindRenrenActivity f2099a;

    private u(BindRenrenActivity bindRenrenActivity) {
        this.f2099a = bindRenrenActivity;
    }

    /* synthetic */ u(BindRenrenActivity bindRenrenActivity, byte b) {
        this(bindRenrenActivity);
    }

    public final void onPageFinished(WebView webView, String str) {
        super.onPageFinished(webView, str);
        this.f2099a.p();
        if (Build.VERSION.SDK_INT <= 10 && str.indexOf(this.f2099a.i) >= 0) {
            String queryParameter = Uri.parse(str.replace("#", "?")).getQueryParameter("code");
            if (a.f(queryParameter)) {
                this.f2099a.m = new q(this.f2099a, this.f2099a, queryParameter, false);
                this.f2099a.m.execute(new Object[0]);
            }
        }
    }

    public final void onPageStarted(WebView webView, String str, Bitmap bitmap) {
        super.onPageStarted(webView, str, bitmap);
        if (this.f2099a.l == null) {
            this.f2099a.l = new v(this.f2099a);
        }
        this.f2099a.l.a("正在加载，请稍候...");
        this.f2099a.a(this.f2099a.l);
    }

    public final void onReceivedError(WebView webView, int i, String str, String str2) {
        if (i == -2) {
            ao.e(R.string.errormsg_network_unfind);
        } else {
            ao.e(R.string.errormsg_server);
        }
        this.f2099a.p();
    }

    public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
        if (Build.VERSION.SDK_INT <= 10) {
            if (str.indexOf(this.f2099a.i) >= 0) {
                return true;
            }
        } else if (str.indexOf(this.f2099a.i) >= 0) {
            str = str.replace("#", "?");
            String queryParameter = Uri.parse(str).getQueryParameter("code");
            if (a.f(queryParameter)) {
                this.f2099a.m = new q(this.f2099a, this.f2099a, queryParameter, false);
                this.f2099a.m.execute(new Object[0]);
            }
        }
        return super.shouldOverrideUrlLoading(webView, str);
    }
}
