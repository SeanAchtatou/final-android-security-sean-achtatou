package com.immomo.momo.android.activity;

import android.content.DialogInterface;

final class kq implements DialogInterface.OnCancelListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ kp f1795a;

    kq(kp kpVar) {
        this.f1795a = kpVar;
    }

    public final void onCancel(DialogInterface dialogInterface) {
        this.f1795a.cancel(true);
    }
}
