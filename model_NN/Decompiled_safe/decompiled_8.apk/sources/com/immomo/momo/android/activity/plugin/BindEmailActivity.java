package com.immomo.momo.android.activity.plugin;

import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.ah;
import com.immomo.momo.android.view.HeaderLayout;

public class BindEmailActivity extends ah {
    private HeaderLayout h = null;
    private TextView i;
    private Button j;
    private boolean k = false;
    /* access modifiers changed from: private */
    public g l;

    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        super.a(bundle);
        setContentView((int) R.layout.activity_mailbox_checkemail);
        d();
        this.h = (HeaderLayout) findViewById(R.id.layout_header);
        this.h.setTitleText("绑定邮箱");
        this.i = (TextView) findViewById(R.id.tv_register_email);
        this.j = (Button) findViewById(R.id.send_button);
        if (this.k) {
            this.j.setText("邮箱已验证");
            this.j.setEnabled(false);
        } else {
            this.j.setText("发送验证邮件");
            this.j.setEnabled(true);
        }
        this.j.setOnClickListener(new f(this));
        this.i.setText(this.f.G);
    }

    /* access modifiers changed from: protected */
    public final void d() {
        this.k = getIntent().getBooleanExtra("bind_email", false);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.l != null) {
            this.l.cancel(true);
        }
    }
}
