package com.immomo.momo.android.activity;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.g;
import android.view.View;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.MomoApplication;
import com.immomo.momo.R;
import com.immomo.momo.android.view.HeaderLayout;
import com.immomo.momo.android.view.bi;
import com.immomo.momo.protocol.imjson.c.a;
import com.immomo.momo.service.bean.at;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.util.m;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class ao extends g {
    /* access modifiers changed from: protected */
    public m e = new m(getClass().getSimpleName());
    /* access modifiers changed from: protected */
    public bf f = null;
    /* access modifiers changed from: protected */
    public at g = null;
    private Map h = new HashMap();
    private boolean i = false;
    /* access modifiers changed from: private */
    public boolean j = false;
    private HeaderLayout k = null;
    private List l = null;
    private Dialog m = null;
    private String n = PoiTypeDef.All;
    private List o = new ArrayList();
    /* access modifiers changed from: private */
    public boolean p = false;
    private int q = 0;

    public final AsyncTask a(AsyncTask asyncTask) {
        this.l.add(asyncTask);
        return asyncTask;
    }

    /* access modifiers changed from: protected */
    public final a a(int i2, String... strArr) {
        ap apVar = new ap(this, i2, strArr);
        this.o.add(apVar);
        t().i().a(apVar);
        return apVar;
    }

    public final void a(int i2) {
        if (this.i) {
            com.immomo.momo.util.ao.d(i2);
        }
    }

    public final synchronized void a(Dialog dialog) {
        p();
        this.m = dialog;
        try {
            if (!isFinishing()) {
                dialog.show();
            }
        } catch (Throwable th) {
        }
    }

    public void a(Intent intent, int i2, Bundle bundle, String str) {
        if (intent.getExtras() == null || !intent.getExtras().containsKey("afrom")) {
            intent.putExtra("afrom", str);
        }
        if (com.immomo.momo.g.b()) {
            super.startActivityForResult(intent, i2, bundle);
        } else {
            super.startActivityForResult(intent, i2);
        }
    }

    public final void a(Fragment fragment, Intent intent, int i2) {
        this.e.a((Object) ("~~~~~~~~KEY_FROM=" + fragment.getClass().getName()));
        intent.putExtra("afrom", fragment.getClass().getName());
        super.a(fragment, intent, i2);
    }

    public final void a(bi biVar) {
        a(biVar, (View.OnClickListener) null);
    }

    public final void a(bi biVar, View.OnClickListener onClickListener) {
        m().a(biVar, onClickListener);
    }

    public final void a(CharSequence charSequence) {
        a(charSequence, 1);
    }

    public final void a(CharSequence charSequence, int i2) {
        if (this.i) {
            com.immomo.momo.util.ao.a(charSequence, i2);
        }
    }

    /* access modifiers changed from: protected */
    public boolean a(Bundle bundle, String str) {
        return false;
    }

    public final void b(int i2) {
        if (this.i) {
            com.immomo.momo.util.ao.f(i2);
        }
    }

    public final void b(AsyncTask asyncTask) {
        a(asyncTask).execute(new Object[0]);
    }

    public final void b(CharSequence charSequence) {
        if (this.i) {
            com.immomo.momo.util.ao.b(charSequence);
        }
    }

    /* access modifiers changed from: protected */
    public void d() {
    }

    /* access modifiers changed from: protected */
    public void e() {
    }

    public View findViewById(int i2) {
        View view = this.h.get(Integer.valueOf(i2)) != null ? (View) ((WeakReference) this.h.get(Integer.valueOf(i2))).get() : null;
        if (view != null) {
            return view;
        }
        View findViewById = super.findViewById(i2);
        this.h.put(Integer.valueOf(i2), new WeakReference(findViewById));
        return findViewById;
    }

    public bf h() {
        return this.f;
    }

    public boolean isDestroyed() {
        return this.p || isFinishing();
    }

    public boolean isFinishing() {
        return super.isFinishing() || this.p;
    }

    public final String l() {
        return this.n;
    }

    public final HeaderLayout m() {
        if (this.k == null) {
            this.k = (HeaderLayout) findViewById(R.id.layout_header);
            if (this.k != null && this.k.getParent() != null && (this.k.getParent() instanceof ao) && !this.k.getHeaderSpinner().isShown()) {
                this.k = ((ao) getParent()).m();
            }
        }
        return this.k;
    }

    public final ao n() {
        return this;
    }

    public final at o() {
        return this.g;
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.j = false;
        this.l = new ArrayList();
        this.n = getIntent().getStringExtra("afrom");
        this.f = t().a();
        this.g = t().b();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.p = true;
        super.onDestroy();
        for (AsyncTask asyncTask : this.l) {
            if (asyncTask != null && !asyncTask.isCancelled()) {
                asyncTask.cancel(true);
            }
        }
        if (this.o.size() > 0) {
            for (a aVar : this.o) {
                this.e.a((Object) ("unregister " + aVar.a() + ", b=" + t().i().b(aVar)));
            }
        }
        this.o.clear();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.e.a((Object) "onPause");
        this.i = false;
        d.b();
    }

    /* access modifiers changed from: protected */
    public void onPostCreate(Bundle bundle) {
        super.onPostCreate(bundle);
        if (!(m() == null || !android.support.v4.b.a.a(m().getTitleText()) || m().getHeaderSpinner().getVisibility() == 0)) {
            m().setTitleText(getTitle());
        }
        new Handler().postDelayed(new aq(this), 100);
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        super.onRestart();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.i = true;
        d.a();
        this.q++;
        if (this.q == 1) {
            s();
        } else {
            int i2 = this.q;
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        try {
            super.onSaveInstanceState(bundle);
        } catch (IllegalStateException e2) {
            this.e.a((Throwable) e2);
        }
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }

    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void p() {
        /*
            r1 = this;
            monitor-enter(r1)
            android.app.Dialog r0 = r1.m     // Catch:{ Throwable -> 0x0020, all -> 0x001d }
            if (r0 == 0) goto L_0x001b
            android.app.Dialog r0 = r1.m     // Catch:{ Throwable -> 0x0020, all -> 0x001d }
            boolean r0 = r0.isShowing()     // Catch:{ Throwable -> 0x0020, all -> 0x001d }
            if (r0 == 0) goto L_0x001b
            boolean r0 = r1.isFinishing()     // Catch:{ Throwable -> 0x0020, all -> 0x001d }
            if (r0 != 0) goto L_0x001b
            android.app.Dialog r0 = r1.m     // Catch:{ Throwable -> 0x0020, all -> 0x001d }
            r0.dismiss()     // Catch:{ Throwable -> 0x0020, all -> 0x001d }
            r0 = 0
            r1.m = r0     // Catch:{ Throwable -> 0x0020, all -> 0x001d }
        L_0x001b:
            monitor-exit(r1)
            return
        L_0x001d:
            r0 = move-exception
            monitor-exit(r1)
            throw r0
        L_0x0020:
            r0 = move-exception
            goto L_0x001b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.immomo.momo.android.activity.ao.p():void");
    }

    public final boolean q() {
        return this.i;
    }

    /* access modifiers changed from: protected */
    public final boolean r() {
        return this.j;
    }

    /* access modifiers changed from: protected */
    public void s() {
    }

    public void setTitle(int i2) {
        if (m() != null) {
            m().setTitleText(i2);
        }
        super.setTitle(i2);
    }

    public void setTitle(CharSequence charSequence) {
        if (m() != null) {
            m().setTitleText(charSequence);
        }
        super.setTitle(charSequence);
    }

    public void startActivity(Intent intent, Bundle bundle) {
        super.startActivity(intent, bundle);
    }

    public final void startActivityForResult(Intent intent, int i2) {
        a(intent, i2, null, getClass().getName());
    }

    public final void startActivityForResult(Intent intent, int i2, Bundle bundle) {
        a(intent, i2, bundle, getClass().getName());
    }

    /* access modifiers changed from: protected */
    public MomoApplication t() {
        return (MomoApplication) getApplication();
    }

    public void unregisterReceiver(BroadcastReceiver broadcastReceiver) {
        if (broadcastReceiver != null) {
            super.unregisterReceiver(broadcastReceiver);
        }
    }
}
