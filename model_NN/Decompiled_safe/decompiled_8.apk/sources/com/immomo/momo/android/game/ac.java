package com.immomo.momo.android.game;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;
import com.immomo.a.a.f.a;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.ao;
import com.immomo.momo.android.activity.lh;
import com.immomo.momo.android.pay.RechargeActivity;
import com.immomo.momo.g;

@SuppressLint({"ValidFragment"})
public final class ac extends lh {
    ai O;
    TextView P;
    String Q;
    String R;
    am S;
    boolean T;
    private TextView U;
    private TextView V;
    private TextView W;
    private TextView X;
    private Button Y;
    private Button Z;

    public ac(ai aiVar, String str, String str2) {
        this.O = aiVar;
        this.Q = str;
        this.R = str2;
        this.S = aiVar.b;
    }

    /* access modifiers changed from: private */
    public void O() {
        if (!g.y()) {
            a("当前没有网络，请确认网络环境");
            return;
        }
        this.T = true;
        ((ao) c()).b(new ah(this, c()));
    }

    static /* synthetic */ void b(ac acVar) {
        if (!acVar.S.i) {
            ((ao) acVar.c()).b(new ag(acVar, acVar.c()));
        } else {
            acVar.O();
        }
    }

    static /* synthetic */ void c(ac acVar) {
        if (acVar.M == null || acVar.O.g == null || !acVar.M.h.equals(acVar.O.g.h)) {
            Intent intent = new Intent(g.c(), RechargeWebviewActivity.class);
            intent.putExtra("appid", acVar.R);
            intent.putExtra("token", acVar.Q);
            acVar.a(intent, 16);
            return;
        }
        Intent intent2 = new Intent(g.c(), RechargeActivity.class);
        intent2.putExtra("model", 1);
        acVar.a(intent2, 15);
    }

    /* access modifiers changed from: private */
    public void e(boolean z) {
        Intent intent = new Intent();
        intent.putExtra("product_id", this.O.i);
        intent.putExtra("trade_number", this.S.h);
        intent.putExtra("trade_sign", this.S.g);
        if (!a.a(this.O.j)) {
            intent.putExtra("trade_extendnumber", this.O.j);
        }
        if (z) {
            c().setResult(-1, intent);
        } else {
            c().setResult(30210, intent);
        }
        c().finish();
    }

    /* access modifiers changed from: protected */
    public final int B() {
        return R.layout.fragment_mdkmomopay;
    }

    /* access modifiers changed from: protected */
    public final void C() {
        this.U = (TextView) c((int) R.id.opentrade_tv_fee);
        this.V = (TextView) c((int) R.id.opentrade_tv_product);
        this.W = (TextView) c((int) R.id.opentrade_tv_username);
        this.X = (TextView) c((int) R.id.opentrade_tv_momoid);
        this.Y = (Button) c((int) R.id.opentrade_btn_confim);
        this.Z = (Button) c((int) R.id.opentrade_btn_recharge);
        this.P = (TextView) c((int) R.id.opentrade_tv_balance);
    }

    public final boolean F() {
        return false;
    }

    public final boolean G() {
        if (!this.T) {
            return false;
        }
        e(false);
        return true;
    }

    public final void aj() {
        this.U.setText("支付金额: " + ((long) this.S.e) + "陌陌币");
        this.V.setText("商品: " + this.O.h);
        this.P.setText("陌陌账号余额: " + ((long) this.O.f) + "陌陌币");
        if (this.O.g != null) {
            this.W.setText("用户: " + this.O.g.h());
            this.X.setText("陌陌号: " + this.O.g.h);
            return;
        }
        this.W.setText("用户: ");
        this.X.setText("陌陌号: ");
    }

    public final void b(int i, int i2, Intent intent) {
        if (i == 15 && i2 == -1) {
            long longExtra = intent.getLongExtra("balance", -1);
            this.L.a((Object) ("onActivityResult, balance=" + longExtra));
            if (longExtra > 0) {
                this.O.f = (double) longExtra;
                this.P.setText("陌陌账号余额: " + longExtra + "陌陌币");
            }
        } else if (i == 16) {
            ((ao) c()).b(new af(this, c()));
        }
    }

    public final void g(Bundle bundle) {
        this.Y.setOnClickListener(new ad(this));
        this.Z.setOnClickListener(new ae(this));
        aj();
    }
}
