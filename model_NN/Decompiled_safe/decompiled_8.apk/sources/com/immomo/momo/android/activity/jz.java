package com.immomo.momo.android.activity;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.t;
import android.support.v4.view.ViewPager;
import android.support.v4.view.bf;
import android.view.ViewGroup;
import java.util.ArrayList;
import java.util.Iterator;

public final class jz extends t implements bf {

    /* renamed from: a  reason: collision with root package name */
    private final Context f1779a;
    private final ViewPager b;
    private ArrayList c;
    private boolean d;
    private /* synthetic */ jx e;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public jz(jx jxVar, Fragment fragment, ViewPager viewPager, ArrayList arrayList) {
        super(fragment.e());
        this.e = jxVar;
        this.c = null;
        this.d = true;
        this.c = new ArrayList();
        this.f1779a = fragment.c();
        this.b = viewPager;
        if (arrayList != null) {
            Iterator it = arrayList.iterator();
            while (it.hasNext()) {
                this.c.add((jy) it.next());
            }
        }
        this.b.setOnPageChangeListener(this);
        this.b.setAdapter(this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.android.activity.lh.a(android.content.Context, java.lang.Class, boolean):com.immomo.momo.android.activity.lh
     arg types: [android.content.Context, java.lang.Class, int]
     candidates:
      com.immomo.momo.android.activity.lh.a(java.lang.String, int, int):void
      com.immomo.momo.android.activity.ar.a(int, int, android.content.Intent):void
      android.support.v4.app.Fragment.a(android.content.Context, java.lang.String, android.os.Bundle):android.support.v4.app.Fragment
      android.support.v4.app.Fragment.a(int, int, android.content.Intent):void
      com.immomo.momo.android.activity.lh.a(android.content.Context, java.lang.Class, boolean):com.immomo.momo.android.activity.lh */
    public final Fragment a(int i) {
        return lh.a(this.f1779a, ((jy) this.c.get(i)).f1778a, true);
    }

    public final Object a(ViewGroup viewGroup, int i) {
        Object a2 = super.a(viewGroup, i);
        this.e.R.put(Integer.valueOf(i), (lh) a2);
        return a2;
    }

    public final void a(int i, float f) {
    }

    public final void a(ViewGroup viewGroup) {
        super.a(viewGroup);
        if (this.d) {
            this.d = false;
            a_(this.b.getCurrentItem());
        }
    }

    public final void a(ViewGroup viewGroup, int i, Object obj) {
    }

    public final void a_(int i) {
        if (!(this.e.S < 0 || this.e.S == i || this.e.R.get(Integer.valueOf(this.e.S)) == null)) {
            ((lh) this.e.R.get(Integer.valueOf(this.e.S))).af();
        }
        lh lhVar = (lh) this.e.R.get(Integer.valueOf(i));
        if (lhVar != null) {
            if (this.e.M()) {
                lhVar.ad();
            }
            this.e.a(lhVar, i);
            this.e.S = i;
        }
    }

    public final int b() {
        return this.c.size();
    }

    public final void b(int i) {
    }
}
