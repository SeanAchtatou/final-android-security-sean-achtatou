package com.immomo.momo.android.activity.contacts;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.g;
import com.immomo.momo.protocol.a.w;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

final class l extends d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ a f1192a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public l(a aVar, Context context) {
        super(context);
        this.f1192a = aVar;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        ArrayList arrayList = new ArrayList();
        int a2 = w.a().a(arrayList, 0, this.f1192a.N);
        this.f1192a.V.c(arrayList);
        g.q().y = a2;
        this.f1192a.V.a(g.q().y, g.q().h);
        List unused = this.f1192a.P();
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
        if (this.f1192a.X != null && !this.f1192a.X.isCancelled()) {
            this.f1192a.X.cancel(true);
        }
        this.f1192a.Q.g();
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        super.a(exc);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.android.activity.contacts.a.a(com.immomo.momo.android.activity.contacts.a, boolean):void
     arg types: [com.immomo.momo.android.activity.contacts.a, int]
     candidates:
      com.immomo.momo.android.activity.contacts.a.a(com.immomo.momo.android.activity.contacts.a, int):void
      com.immomo.momo.android.activity.contacts.a.a(com.immomo.momo.android.activity.contacts.a, java.util.Date):void
      com.immomo.momo.android.activity.contacts.a.a(android.content.Context, com.immomo.momo.android.view.HeaderLayout):void
      com.immomo.momo.android.activity.lh.a(int, java.lang.String[]):com.immomo.momo.protocol.imjson.c.a
      com.immomo.momo.android.activity.lh.a(android.content.Context, com.immomo.momo.android.view.HeaderLayout):void
      com.immomo.momo.android.activity.lh.a(int, android.view.KeyEvent):boolean
      com.immomo.momo.android.activity.lh.a(android.os.Bundle, java.lang.String):boolean
      com.immomo.momo.android.activity.ar.a(android.view.LayoutInflater, android.view.ViewGroup):android.view.View
      android.support.v4.app.Fragment.a(android.content.Context, java.lang.String):android.support.v4.app.Fragment
      android.support.v4.app.Fragment.a(android.view.LayoutInflater, android.view.ViewGroup):android.view.View
      android.support.v4.app.Fragment.a(int, android.support.v4.app.Fragment):void
      android.support.v4.app.Fragment.a(android.content.Intent, int):void
      android.support.v4.app.Fragment.a(android.view.Menu, android.view.MenuInflater):boolean
      com.immomo.momo.android.activity.contacts.a.a(com.immomo.momo.android.activity.contacts.a, boolean):void */
    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        List list = (List) obj;
        super.a(list);
        if (list != null) {
            this.f1192a.O.setLastFlushTime(this.f1192a.U);
            this.f1192a.N.a("lasttime_bothlist_success", this.f1192a.U);
            a.o(this.f1192a);
            this.f1192a.ac = true;
            this.f1192a.aa.getText().clear();
            this.f1192a.Z.setVisibility(4);
            this.f1192a.ac = false;
            if (list.size() >= g.q().y) {
                this.f1192a.O.k();
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
        super.b();
        this.f1192a.W = null;
        this.f1192a.U = new Date();
        this.f1192a.N.a("lasttime_bothlist", this.f1192a.U);
        this.f1192a.O.n();
        this.f1192a.Q.e();
    }
}
