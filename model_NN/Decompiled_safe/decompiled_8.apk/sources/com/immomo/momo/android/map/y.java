package com.immomo.momo.android.map;

import android.location.Location;

final class y implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ x f2519a;
    private final /* synthetic */ Location b;

    y(x xVar, Location location) {
        this.f2519a = xVar;
        this.b = location;
    }

    public final void run() {
        if (!this.f2519a.f2518a.isFinishing() && this.f2519a.f2518a.f2450a != null) {
            this.f2519a.f2518a.f2450a.dismiss();
            this.f2519a.f2518a.f2450a = null;
        }
        this.f2519a.f2518a.f = this.b.getLatitude();
        this.f2519a.f2518a.g = this.b.getLongitude();
        this.f2519a.f2518a.b();
        GeoGoogleMapActivity.e(this.f2519a.f2518a);
    }
}
