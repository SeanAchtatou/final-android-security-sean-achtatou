package com.immomo.momo.android.activity.tieba;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.ka;
import com.immomo.momo.android.activity.lh;
import com.immomo.momo.g;
import com.immomo.momo.protocol.imjson.d;
import com.immomo.momo.service.af;

public class MainTiebaActivity extends ka implements View.OnClickListener {
    private TextView h;

    private boolean z() {
        int k = af.c().k();
        if (k <= 0) {
            this.h.setVisibility(8);
        } else if (k < 100) {
            this.h.setVisibility(0);
            this.h.setText(new StringBuilder(String.valueOf(k)).toString());
        } else {
            this.h.setVisibility(0);
            this.h.setText("99+");
        }
        if (!g() && k > 0) {
            c(1);
        }
        return k > 0;
    }

    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        setContentView((int) R.layout.activity_tiebamain);
        a(dw.class, z.class);
        findViewById(R.id.tiebamain_layout_tab1).setOnClickListener(this);
        findViewById(R.id.tiebamain_layout_tab2).setOnClickListener(this);
        this.h = (TextView) findViewById(R.id.tiebamain_layout_tab2).findViewById(R.id.tiebamain_tab_count);
        if (z()) {
            t().p();
        }
        a(800, "actions.tieba");
    }

    /* access modifiers changed from: protected */
    public final void a(Fragment fragment, int i) {
        switch (i) {
            case 0:
                findViewById(R.id.tiebamain_layout_tab1).setSelected(true);
                findViewById(R.id.tiebamain_layout_tab2).setSelected(false);
                break;
            case 1:
                findViewById(R.id.tiebamain_layout_tab2).setSelected(true);
                findViewById(R.id.tiebamain_layout_tab1).setSelected(false);
                break;
        }
        m().a();
        ((lh) fragment).a(this, m());
        if (!((lh) fragment).T()) {
            ((lh) fragment).S();
        }
    }

    /* access modifiers changed from: protected */
    public final void a(lh lhVar, int i) {
        if (i == 0) {
            lhVar.a(findViewById(R.id.tiebamain_layout_tab1));
        } else if (i == 1) {
            lhVar.a(findViewById(R.id.tiebamain_layout_tab2));
        }
    }

    /* access modifiers changed from: protected */
    public final boolean a(Bundle bundle, String str) {
        if ("actions.tieba".equals(str)) {
            z();
        }
        return super.a(bundle, str);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tiebamain_layout_tab1 /*2131165960*/:
                c(0);
                return;
            case R.id.tiebamain_tab_label /*2131165961*/:
            case R.id.tiebamain_tab_count /*2131165962*/:
            default:
                return;
            case R.id.tiebamain_layout_tab2 /*2131165963*/:
                c(1);
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        g.d().a(new Bundle(), "actions.tiebachanged");
    }

    public final void y() {
        if (this.h.isShown()) {
            t().p();
        }
        this.h.setVisibility(8);
        af.c().c(0);
        d.e();
    }
}
