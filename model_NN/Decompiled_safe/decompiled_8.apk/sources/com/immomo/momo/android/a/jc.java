package com.immomo.momo.android.a;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.c.a;
import com.immomo.momo.util.j;
import java.util.List;

public final class jc extends a {
    public jc(Context context, List list) {
        super(context, list);
    }

    private static String d(int i) {
        return i > 9999999 ? String.valueOf(i / 10000000) + "千万" : i > 9999 ? String.valueOf(i / 10000) + "万" : new StringBuilder(String.valueOf(i)).toString();
    }

    public final int getItemViewType(int i) {
        return ((a) getItem(i)).f3016a;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        a aVar = (a) getItem(i);
        int itemViewType = getItemViewType(i);
        if (itemViewType == 1) {
            if (view == null) {
                je jeVar = new je((byte) 0);
                view = g.o().inflate((int) R.layout.listitem_tieba, (ViewGroup) null);
                jeVar.f895a = (ImageView) view.findViewById(R.id.teibalist_item_iv_face);
                jeVar.f = (ImageView) view.findViewById(R.id.teibalist_item_iv_hot);
                jeVar.e = (ImageView) view.findViewById(R.id.tiebalist_item_iv_recommend);
                jeVar.b = (TextView) view.findViewById(R.id.tiebalist_item_tv_name);
                jeVar.d = (TextView) view.findViewById(R.id.tiebalist_item_tv_sign);
                jeVar.c = (TextView) view.findViewById(R.id.tiebalist_item_tv_attribute);
                view.setTag(R.id.tag_userlist_item, jeVar);
            }
            je jeVar2 = (je) view.getTag(R.id.tag_userlist_item);
            if (android.support.v4.b.a.a((CharSequence) aVar.c.b)) {
                jeVar2.b.setText(aVar.c.f3019a);
            } else {
                jeVar2.b.setText(aVar.c.b);
            }
            jeVar2.f.setVisibility(8);
            jeVar2.e.setVisibility(8);
            if (aVar.c.i > 0) {
                jeVar2.c.setText("成员 " + d(aVar.c.g) + " | 今日话题 " + d(aVar.c.i));
            } else {
                jeVar2.c.setText("成员 " + d(aVar.c.g));
            }
            if (aVar.c.n) {
                jeVar2.f.setVisibility(0);
            } else {
                jeVar2.f.setVisibility(8);
            }
            if (aVar.c.o) {
                jeVar2.e.setVisibility(0);
            } else {
                jeVar2.e.setVisibility(8);
            }
            if (aVar.c.j != null && aVar.c.j.length() > 70) {
                aVar.c.j = aVar.c.j.substring(0, 70);
            }
            jeVar2.d.setText(aVar.c.j);
            j.a(aVar.c, jeVar2.f895a, (ViewGroup) null, 3);
        } else if (itemViewType == 2) {
            if (view == null) {
                jf jfVar = new jf((byte) 0);
                view = g.o().inflate((int) R.layout.listitem_tiebacreating, (ViewGroup) null);
                jfVar.f896a = (TextView) view.findViewById(R.id.tiebacreating_item_tv_name);
                jfVar.b = (TextView) view.findViewById(R.id.tiebacreating_item_tv_des);
                view.setTag(R.id.tag_userlist_item, jfVar);
            }
            jf jfVar2 = (jf) view.getTag(R.id.tag_userlist_item);
            if (android.support.v4.b.a.a((CharSequence) aVar.c.b)) {
                jfVar2.f896a.setText(aVar.c.f3019a);
            } else {
                jfVar2.f896a.setText(aVar.c.b);
            }
            jfVar2.b.setText(String.valueOf(d(aVar.c.g)) + "人支持");
        } else if (itemViewType == 3) {
            if (view == null) {
                view = g.o().inflate((int) R.layout.listitem_groupsite, (ViewGroup) null);
                jd jdVar = new jd((byte) 0);
                jdVar.f894a = (TextView) view.findViewById(R.id.sitelist_tv_name);
                view.setTag(R.id.tag_userlist_item, jdVar);
            }
            ((jd) view.getTag(R.id.tag_userlist_item)).f894a.setText(aVar.b);
        }
        return view;
    }

    public final int getViewTypeCount() {
        return 4;
    }
}
