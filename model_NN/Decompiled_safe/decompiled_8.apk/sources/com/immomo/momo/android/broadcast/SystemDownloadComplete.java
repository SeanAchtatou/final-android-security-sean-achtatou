package com.immomo.momo.android.broadcast;

import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.support.v4.b.a;

public class SystemDownloadComplete extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("android.intent.action.DOWNLOAD_COMPLETE")) {
            long longExtra = intent.getLongExtra("extra_download_id", -1);
            DownloadManager.Query query = new DownloadManager.Query();
            query.setFilterById(longExtra);
            Cursor query2 = ((DownloadManager) context.getSystemService("download")).query(query);
            if (query2.moveToFirst()) {
                String string = query2.getString(query2.getColumnIndex("local_uri"));
                String string2 = query2.getString(query2.getColumnIndex("media_type"));
                if ("application/vnd.android.package-archive".equals(string2) && !a.a((CharSequence) string)) {
                    Intent intent2 = new Intent();
                    intent2.addFlags(268435456);
                    intent2.setAction("android.intent.action.VIEW");
                    intent2.setDataAndType(Uri.parse(string), string2);
                    context.startActivity(intent2);
                }
            }
            query2.close();
        } else if (intent.getAction().equals("android.intent.action.DOWNLOAD_NOTIFICATION_CLICKED")) {
            Intent createChooser = Intent.createChooser(new Intent("android.intent.action.VIEW_DOWNLOADS", intent.getData()), "下载");
            createChooser.addFlags(268435456);
            context.startActivity(createChooser);
        }
    }
}
