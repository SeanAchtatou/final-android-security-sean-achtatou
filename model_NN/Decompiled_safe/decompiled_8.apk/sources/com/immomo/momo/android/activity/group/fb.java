package com.immomo.momo.android.activity.group;

import android.content.Context;
import android.widget.ListAdapter;
import com.immomo.momo.android.a.gy;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.RefreshOnOverScrollListView;
import com.immomo.momo.protocol.a.n;
import com.immomo.momo.service.bean.a.a;
import java.util.ArrayList;
import java.util.List;

final class fb extends d {

    /* renamed from: a  reason: collision with root package name */
    private String f1650a;
    private List c = new ArrayList();
    private /* synthetic */ SearchGroupActivity d;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public fb(SearchGroupActivity searchGroupActivity, Context context, String str) {
        super(context);
        this.d = searchGroupActivity;
        this.f1650a = str;
        if (searchGroupActivity.v != null && !searchGroupActivity.v.isCancelled()) {
            searchGroupActivity.v.cancel(true);
        }
        searchGroupActivity.v = this;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        this.d.u = this.f1650a;
        return Boolean.valueOf(n.a().a(this.c, this.d.f.S, this.d.f.T, this.d.f.aH, this.f1650a, 0));
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
        this.d.h.b();
        this.d.w();
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        super.a(exc);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        this.d.s.setVisibility(0);
        if (((Boolean) obj).booleanValue()) {
            this.d.n.setVisibility(0);
        } else {
            this.d.n.setVisibility(8);
        }
        this.d.l.setVisibility(this.c.isEmpty() ? 0 : 8);
        if (!this.c.isEmpty()) {
            this.d.p.clear();
            for (a aVar : this.c) {
                this.d.p.put(aVar.b, aVar);
            }
            this.d.k.setVisibility(0);
            RefreshOnOverScrollListView l = this.d.k;
            SearchGroupActivity searchGroupActivity = this.d;
            gy gyVar = new gy(this.d, this.c, this.d.k);
            searchGroupActivity.o = gyVar;
            l.setAdapter((ListAdapter) gyVar);
            return;
        }
        this.d.k.setVisibility(8);
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.d.h.c();
    }
}
