package com.immomo.momo.android.activity.event;

import com.immomo.momo.android.c.i;
import com.immomo.momo.android.c.u;
import com.immomo.momo.android.view.an;
import com.immomo.momo.plugin.b.a;
import com.immomo.momo.service.bean.q;
import java.io.File;

final class ca implements an {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ PublishEventFeedActivity f1394a;

    ca(PublishEventFeedActivity publishEventFeedActivity) {
        this.f1394a = publishEventFeedActivity;
    }

    public final void a(CharSequence charSequence, int i) {
        this.f1394a.e.b((Object) ("+++++++++++++++++++++ onEmoteSelected:" + ((Object) charSequence) + "  type:" + i));
        if (i == 2) {
            this.f1394a.e.a((Object) ("emoteString=" + ((Object) charSequence)));
            PublishEventFeedActivity publishEventFeedActivity = this.f1394a;
            PublishEventFeedActivity publishEventFeedActivity2 = this.f1394a;
            publishEventFeedActivity.I = new a(charSequence.toString());
            this.f1394a.H = null;
            File a2 = q.a(this.f1394a.I.g(), this.f1394a.I.h());
            if (a2 == null || !a2.exists()) {
                u.b().execute(new i(this.f1394a.I.g(), this.f1394a.I.h(), new cb(this), (byte) 0));
                return;
            }
            this.f1394a.s.setImageBitmap(android.support.v4.b.a.l(a2.getAbsolutePath()));
        }
    }
}
