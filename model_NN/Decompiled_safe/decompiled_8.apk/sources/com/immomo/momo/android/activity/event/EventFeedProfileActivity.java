package com.immomo.momo.android.activity.event;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.b.a;
import android.view.Display;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.a.dh;
import com.immomo.momo.android.activity.ImageBrowserActivity;
import com.immomo.momo.android.activity.OtherProfileActivity;
import com.immomo.momo.android.activity.ah;
import com.immomo.momo.android.activity.d;
import com.immomo.momo.android.activity.emotestore.EmotionProfileActivity;
import com.immomo.momo.android.c.i;
import com.immomo.momo.android.view.EmoteInputView;
import com.immomo.momo.android.view.EmoteTextView;
import com.immomo.momo.android.view.HandyListView;
import com.immomo.momo.android.view.HeaderLayout;
import com.immomo.momo.android.view.LoadingButton;
import com.immomo.momo.android.view.MGifImageView;
import com.immomo.momo.android.view.MSmallEmoteEditeText;
import com.immomo.momo.android.view.ResizeListenerLayout;
import com.immomo.momo.android.view.a.at;
import com.immomo.momo.android.view.a.o;
import com.immomo.momo.android.view.au;
import com.immomo.momo.android.view.bi;
import com.immomo.momo.android.view.bl;
import com.immomo.momo.b;
import com.immomo.momo.g;
import com.immomo.momo.service.aq;
import com.immomo.momo.service.bean.GameApp;
import com.immomo.momo.service.bean.aa;
import com.immomo.momo.service.bean.ae;
import com.immomo.momo.service.bean.aj;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.service.bean.q;
import com.immomo.momo.service.bean.u;
import com.immomo.momo.service.r;
import com.immomo.momo.util.j;
import com.immomo.momo.util.k;
import java.io.File;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EventFeedProfileActivity extends ah implements View.OnClickListener, View.OnTouchListener, AdapterView.OnItemClickListener, bl {
    private EmoteTextView A;
    private ImageView B;
    /* access modifiers changed from: private */
    public MGifImageView C;
    private ImageView D;
    private TextView E;
    private TextView F;
    /* access modifiers changed from: private */
    public View G;
    private ImageView H;
    private ImageView I;
    private ImageView J;
    private ImageView K;
    private ImageView L;
    private ImageView M;
    private ImageView N;
    private ImageView O;
    private View P;
    private TextView Q;
    private View R;
    private View S;
    private View T;
    private View U;
    private TextView V;
    private TextView W;
    private ImageView X;
    private View Y;
    /* access modifiers changed from: private */
    public MSmallEmoteEditeText Z;
    private ImageView aa;
    /* access modifiers changed from: private */
    public TextView ab;
    private View ac;
    private Button ad;
    private ImageView ae;
    /* access modifiers changed from: private */
    public EmoteInputView af = null;
    private Animation ag = null;
    /* access modifiers changed from: private */
    public au ah;
    /* access modifiers changed from: private */
    public boolean ai = true;
    /* access modifiers changed from: private */
    public ae aj;
    boolean h = false;
    private ResizeListenerLayout i = null;
    /* access modifiers changed from: private */
    public String j = PoiTypeDef.All;
    /* access modifiers changed from: private */
    public String k = PoiTypeDef.All;
    /* access modifiers changed from: private */
    public aa l = null;
    /* access modifiers changed from: private */
    public dh m = null;
    /* access modifiers changed from: private */
    public r n;
    /* access modifiers changed from: private */
    public InputMethodManager o = null;
    private Handler p = new Handler();
    private HeaderLayout q = null;
    private bi r = null;
    /* access modifiers changed from: private */
    public HandyListView s = null;
    /* access modifiers changed from: private */
    public LoadingButton t = null;
    /* access modifiers changed from: private */
    public View u = null;
    private View v;
    private TextView w;
    private TextView x;
    private TextView y;
    private View z;

    /* access modifiers changed from: private */
    public void a(int i2, String str) {
        String str2;
        if (this.l != null) {
            if (i2 != 1) {
                str2 = this.Z.getText().toString().trim();
                if (a.a((CharSequence) str2)) {
                    a((CharSequence) "请输入评论内容");
                    return;
                }
            } else {
                str2 = str;
            }
            if (this.ai) {
                a(new v(this, this, this.j, this.j, 1, i2, str2, this.l.c, this.l.b.i())).execute(new Object[0]);
            } else if (this.aj != null) {
                int i3 = i2;
                a(new v(this, this, this.j, this.aj.k, 2, i3, "回复" + this.aj.f2985a.i() + ":" + str2, this.aj.b, this.aj.f2985a.i())).execute(new Object[0]);
            }
        }
    }

    public static void a(Context context, ae aeVar) {
        Intent intent = new Intent(context, EventFeedProfileActivity.class);
        intent.putExtra("eventfeedid", aeVar.h);
        intent.putExtra("eventid", aeVar.i);
        intent.putExtra("key_commentid", aeVar.k);
        intent.putExtra("key_owner_id", aeVar.b);
        intent.putExtra("key_comment_content", aeVar.f);
        if (aeVar.f2985a != null) {
            intent.putExtra("key_owner_name", aeVar.f2985a.i());
        }
        context.startActivity(intent);
    }

    static /* synthetic */ void a(EventFeedProfileActivity eventFeedProfileActivity, int i2) {
        if (eventFeedProfileActivity.l != null) {
            eventFeedProfileActivity.l.g = i2;
            eventFeedProfileActivity.F.setText(new StringBuilder(String.valueOf(i2)).toString());
            if (eventFeedProfileActivity.l.g > 0 || !eventFeedProfileActivity.m.isEmpty()) {
                eventFeedProfileActivity.ac.setVisibility(8);
                return;
            }
            eventFeedProfileActivity.aa.clearAnimation();
            eventFeedProfileActivity.aa.setVisibility(8);
            eventFeedProfileActivity.x.setText("暂无评论");
            eventFeedProfileActivity.x.setVisibility(0);
            eventFeedProfileActivity.ac.setVisibility(0);
        }
    }

    static /* synthetic */ void a(EventFeedProfileActivity eventFeedProfileActivity, String str, boolean z2) {
        o oVar = new o(eventFeedProfileActivity, (int) R.array.reportfeed_items);
        if (z2) {
            oVar.setTitle((int) R.string.report_dialog_title_comment);
        } else {
            oVar.setTitle((int) R.string.report_dialog_title_feed);
        }
        oVar.a();
        oVar.a(new s(eventFeedProfileActivity, z2, str));
        oVar.show();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.b.a(java.lang.String, boolean):android.graphics.Bitmap
     arg types: [java.lang.String, int]
     candidates:
      com.immomo.momo.b.a(java.lang.String, android.graphics.Bitmap):android.graphics.Bitmap
      com.immomo.momo.b.a(java.lang.String, boolean):android.graphics.Bitmap */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup, int, boolean, boolean, int):void
     arg types: [com.immomo.momo.service.bean.bf, android.widget.ImageView, ?[OBJECT, ARRAY], int, int, int, int]
     candidates:
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, int, android.view.View, android.view.ViewGroup, boolean, int, boolean):com.immomo.momo.android.c.o
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup, int, boolean, boolean, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup, int, boolean, boolean, int):void
     arg types: [com.immomo.momo.service.bean.al, android.widget.ImageView, com.immomo.momo.android.view.HandyListView, int, int, int, int]
     candidates:
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, int, android.view.View, android.view.ViewGroup, boolean, int, boolean):com.immomo.momo.android.c.o
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup, int, boolean, boolean, int):void */
    /* access modifiers changed from: private */
    public void a(aa aaVar) {
        if (aaVar != null) {
            this.w.setText(aaVar.f);
            this.z.setEnabled((aaVar.d == null || aaVar.d.o == 0) ? false : true);
            this.y.setText(aaVar.g());
            if (a.f(aaVar.b())) {
                this.A.setText(aaVar.c());
                this.A.setVisibility(0);
            } else {
                this.A.setVisibility(8);
            }
            if (a.f(aaVar.k) && a.f(aaVar.j)) {
                this.B.setVisibility(8);
                this.C.setVisibility(0);
                this.C.setAlt(aaVar.j);
                boolean endsWith = this.l.j.endsWith(".gif");
                if (this.ah == null) {
                    File a2 = q.a(this.l.j, this.l.k);
                    this.e.b((Object) ("emoteFile:" + (a2 == null ? "null" : a2.getAbsolutePath())));
                    if (a2 == null || !a2.exists()) {
                        new i(this.l.j, this.l.k, new k(this, endsWith)).a();
                    } else {
                        this.ah = new au(endsWith ? 1 : 2);
                        this.ah.a(a2, this.C);
                        this.ah.b(20);
                        this.C.setGifDecoder(this.ah);
                    }
                } else {
                    this.C.setGifDecoder(this.ah);
                    if ((this.ah.g() == 4 || this.ah.g() == 2) && this.ah.f() != null && this.ah.f().exists()) {
                        this.ah.a(this.ah.f(), this.C);
                    }
                }
            } else if (a.f(aaVar.getLoadImageId())) {
                this.B.setVisibility(0);
                this.C.setVisibility(8);
                j.a(aaVar, this.B, (ViewGroup) null, 15);
            } else {
                this.B.setVisibility(8);
                this.C.setVisibility(8);
            }
            if (aaVar.b != null) {
                this.E.setText(aaVar.b.h());
                this.R.setVisibility(0);
                this.Q.setText(new StringBuilder(String.valueOf(aaVar.b.I)).toString());
                if (aaVar.b.b()) {
                    this.E.setTextColor(g.c((int) R.color.font_vip_name));
                } else {
                    this.E.setTextColor(g.c((int) R.color.font_value));
                }
                if ("F".equals(aaVar.b.H)) {
                    this.P.setBackgroundResource(R.drawable.bg_gender_famal);
                    this.H.setImageResource(R.drawable.ic_user_famale);
                } else {
                    this.P.setBackgroundResource(R.drawable.bg_gender_male);
                    this.H.setImageResource(R.drawable.ic_user_male);
                }
                if (aaVar.b.j()) {
                    this.M.setVisibility(0);
                } else {
                    this.M.setVisibility(8);
                }
                if (!a.a((CharSequence) aaVar.b.M)) {
                    this.N.setVisibility(0);
                    this.N.setImageBitmap(b.a(aaVar.b.M, true));
                } else {
                    this.N.setVisibility(8);
                }
                if (aaVar.b.an) {
                    this.I.setVisibility(0);
                    this.I.setImageResource(aaVar.b.ap ? R.drawable.ic_userinfo_weibov : R.drawable.ic_userinfo_weibo);
                } else {
                    this.I.setVisibility(8);
                }
                if (aaVar.b.at) {
                    this.J.setVisibility(0);
                    this.J.setImageResource(aaVar.b.au ? R.drawable.ic_userinfo_tweibov : R.drawable.ic_userinfo_tweibo);
                } else {
                    this.J.setVisibility(8);
                }
                if (aaVar.b.aJ == 1 || aaVar.b.aJ == 3) {
                    this.O.setVisibility(0);
                    this.O.setImageResource(aaVar.b.aJ == 1 ? R.drawable.ic_userinfo_groupowner : R.drawable.ic_userinfo_group);
                } else {
                    this.O.setVisibility(8);
                }
                if (aaVar.b.ar) {
                    this.K.setVisibility(0);
                } else {
                    this.K.setVisibility(8);
                }
                if (aaVar.b.b()) {
                    this.L.setVisibility(0);
                } else {
                    this.L.setVisibility(8);
                }
            } else {
                this.R.setVisibility(0);
                this.E.setText(aaVar.c);
            }
            j.a((aj) aaVar.b, this.D, (ViewGroup) null, 3, false, true, g.a(8.0f));
            if (aaVar == null || aaVar.g <= this.m.getCount()) {
                this.F.setText(new StringBuilder(String.valueOf(this.m.getCount())).toString());
            } else {
                this.F.setText(new StringBuilder(String.valueOf(aaVar.g)).toString());
            }
            if (aaVar.n != null) {
                GameApp gameApp = aaVar.n;
                this.S.setVisibility(0);
                this.T.setVisibility(0);
                this.U.setVisibility(0);
                this.V.setTextColor(g.c((int) R.color.text_feedapp_game));
                this.W.setTextColor(g.c((int) R.color.text_feedapp_game));
                this.S.setBackgroundColor(g.c((int) R.color.bg_feedapp_game_border));
                this.T.setBackgroundColor(g.c((int) R.color.bg_feedapp_game_border));
                this.U.setBackgroundColor(g.c((int) R.color.bg_feedapp_game));
                this.V.setText(gameApp.appdesc);
                this.W.setText(gameApp.appname);
                j.a((aj) gameApp.appIconLoader(), this.X, (ViewGroup) this.s, 18, false, true, 0);
            } else if (aaVar.o != null) {
                u uVar = aaVar.o;
                this.S.setVisibility(0);
                this.T.setVisibility(0);
                this.U.setVisibility(0);
                this.V.setTextColor(g.c((int) R.color.text_feedapp_event));
                this.W.setTextColor(g.c((int) R.color.text_feedapp_event));
                this.S.setBackgroundColor(g.c((int) R.color.bg_feedapp_event_border));
                this.T.setBackgroundColor(g.c((int) R.color.bg_feedapp_event_border));
                this.U.setBackgroundColor(g.c((int) R.color.bg_feedapp_event));
                this.V.setText(String.valueOf(uVar.d) + "  " + uVar.f() + "人参加");
                this.W.setText(uVar.b);
                this.X.setImageResource(R.drawable.ic_discover_event);
            } else {
                this.S.setVisibility(8);
                this.T.setVisibility(8);
                this.U.setVisibility(8);
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(ae aeVar) {
        this.ai = false;
        this.aj = aeVar;
        this.ab.setVisibility(0);
        this.ab.setText(a.f(aeVar.f2985a.l) ? "回复" + aeVar.f2985a.i() + "(" + aeVar.f2985a.h() + ")：" + d(aeVar.f) : "回复" + aeVar.f2985a.i() + ":" + d(aeVar.f));
        y();
    }

    static /* synthetic */ String c(String str) {
        Matcher matcher = Pattern.compile("(\\[[.[^\\[\\]]]*?\\|et\\|([.[^\\[\\]]]*?\\|)*[.[^\\[\\]]]*?\\])").matcher(str);
        if (matcher.find()) {
            return matcher.group();
        }
        return null;
    }

    private static String d(String str) {
        return str.toString().indexOf("回复") == 0 ? str.substring(str.indexOf(":") + 1) : str;
    }

    static /* synthetic */ Date e(EventFeedProfileActivity eventFeedProfileActivity) {
        if (eventFeedProfileActivity.m == null || eventFeedProfileActivity.m.getCount() <= 0) {
            return null;
        }
        return ((ae) eventFeedProfileActivity.m.getItem(eventFeedProfileActivity.m.getCount() - 1)).a();
    }

    static /* synthetic */ void q(EventFeedProfileActivity eventFeedProfileActivity) {
        if (eventFeedProfileActivity.l != null) {
            String[] strArr = a.f(eventFeedProfileActivity.l.b()) ? eventFeedProfileActivity.f.h.equals(eventFeedProfileActivity.l.c) ? new String[]{"复制文本", "删除"} : new String[]{"复制文本", "举报"} : eventFeedProfileActivity.f.h.equals(eventFeedProfileActivity.l.c) ? new String[]{"删除"} : new String[]{"举报"};
            at atVar = new at(eventFeedProfileActivity, eventFeedProfileActivity.r, strArr);
            atVar.a(new o(eventFeedProfileActivity, strArr));
            atVar.d();
        }
    }

    private void w() {
        Display defaultDisplay = getWindowManager().getDefaultDisplay();
        defaultDisplay.getWidth();
        defaultDisplay.getHeight();
    }

    /* access modifiers changed from: private */
    public void x() {
        this.ae.setImageResource(R.drawable.ic_publish_emote);
        this.af.a();
    }

    private void y() {
        this.p.postDelayed(new n(this), 200);
    }

    private void z() {
        this.ai = true;
        this.ab.setVisibility(8);
        y();
    }

    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        super.a(bundle);
        setContentView((int) R.layout.activity_feedprofile);
        this.j = getIntent().getStringExtra("eventfeedid");
        this.k = getIntent().getStringExtra("eventid");
        if (!a.f(this.j)) {
            this.e.b((Object) "++++++++++++++++++++ empty key when start FriendFeedActivity!");
            finish();
            return;
        }
        this.i = (ResizeListenerLayout) findViewById(R.id.layout_root);
        this.q = (HeaderLayout) findViewById(R.id.layout_header);
        this.q.setTitleText("活动讨论回复");
        this.u = g.o().inflate((int) R.layout.common_list_loadmore, (ViewGroup) null);
        this.t = (LoadingButton) this.u.findViewById(R.id.btn_loadmore);
        this.t.setNormalIconResId(R.drawable.ic_btn_inner_clock);
        this.s = (HandyListView) findViewById(R.id.lv_feed);
        this.s.addFooterView(this.u);
        this.G = findViewById(R.id.layout_cover);
        this.Y = findViewById(R.id.layout_feed_comment);
        this.Z = (MSmallEmoteEditeText) this.Y.findViewById(R.id.tv_feed_editer);
        this.ab = (TextView) this.Y.findViewById(R.id.tv_feed_editertitle);
        this.ad = (Button) this.Y.findViewById(R.id.bt_feed_send);
        this.ae = (ImageView) this.Y.findViewById(R.id.iv_feed_emote);
        this.af = (EmoteInputView) this.Y.findViewById(R.id.emoteview);
        this.af.setEditText(this.Z);
        this.af.setEmoteFlag(5);
        this.af.setOnEmoteSelectedListener(new g(this));
        this.r = new bi(this);
        this.r.a((int) R.drawable.ic_topbar_more);
        this.q.a(this.r, new h(this));
        this.v = g.o().inflate((int) R.layout.include_feedprofile_feed, (ViewGroup) null);
        this.w = (TextView) this.v.findViewById(R.id.tv_feed_time);
        this.y = (TextView) this.v.findViewById(R.id.tv_feed_site);
        this.z = this.v.findViewById(R.id.layout_feed_site);
        this.A = (EmoteTextView) this.v.findViewById(R.id.tv_feed_content);
        this.B = (ImageView) this.v.findViewById(R.id.iv_feed_content);
        this.C = (MGifImageView) this.v.findViewById(R.id.gv_feed_content);
        this.D = (ImageView) this.v.findViewById(R.id.iv_feed_photo);
        this.E = (TextView) this.v.findViewById(R.id.tv_feed_name);
        this.F = (TextView) this.v.findViewById(R.id.tv_feed_commentcount);
        this.v.findViewById(R.id.layout_feed_commentcount);
        this.ac = this.v.findViewById(R.id.layout_feed_titlecomment);
        this.x = (TextView) this.ac.findViewById(R.id.tv_feed_titlecomment);
        this.aa = (ImageView) this.ac.findViewById(R.id.iv_feed_titleanim);
        this.R = this.v.findViewById(R.id.userlist_item_layout_badgeContainer);
        this.H = (ImageView) this.R.findViewById(R.id.userlist_item_iv_gender);
        this.P = this.R.findViewById(R.id.userlist_item_layout_genderbackgroud);
        this.I = (ImageView) this.R.findViewById(R.id.userlist_item_pic_iv_weibo);
        this.J = (ImageView) this.R.findViewById(R.id.userlist_item_pic_iv_tweibo);
        this.O = (ImageView) this.R.findViewById(R.id.userlist_item_pic_iv_group_role);
        this.K = (ImageView) this.R.findViewById(R.id.userlist_item_pic_iv_renren);
        this.L = (ImageView) this.R.findViewById(R.id.userlist_item_pic_iv_vip);
        this.M = (ImageView) this.R.findViewById(R.id.userlist_item_pic_iv_multipic);
        this.N = (ImageView) this.R.findViewById(R.id.userlist_item_pic_iv_industry);
        this.Q = (TextView) this.R.findViewById(R.id.userlist_item_tv_age);
        this.S = this.v.findViewById(R.id.feed_view_app_border_bottom);
        this.T = this.v.findViewById(R.id.feed_view_app_border_top);
        this.U = this.v.findViewById(R.id.feed_layout_app);
        this.V = (TextView) this.v.findViewById(R.id.feed_tv_appdesc);
        this.W = (TextView) this.v.findViewById(R.id.feed_tv_apptitle);
        this.X = (ImageView) this.v.findViewById(R.id.feed_iv_appicon);
        this.s.addHeaderView(this.v);
        this.s.setOnItemClickListener(this);
        this.t.setOnProcessListener(this);
        this.Z.setOnTouchListener(this);
        this.B.setOnClickListener(this);
        this.C.setOnClickListener(this);
        this.D.setOnClickListener(this);
        this.G.setOnClickListener(this);
        this.ae.setOnClickListener(this);
        this.ad.setOnClickListener(this);
        this.U.setOnClickListener(this);
        this.i.setOnResizeListener(new i(this));
        this.n = new r();
        this.o = (InputMethodManager) getSystemService("input_method");
        this.m = new dh(this, this.s);
        this.s.setAdapter((ListAdapter) this.m);
        w();
        d();
    }

    /* access modifiers changed from: protected */
    public final void d() {
        int indexOf;
        aa aaVar = null;
        r rVar = this.n;
        String str = this.j;
        String str2 = this.k;
        aa aaVar2 = new aa(str);
        List a2 = rVar.a(str2);
        if (a2 != null && (indexOf = a2.indexOf(aaVar2)) >= 0) {
            aaVar = (aa) a2.get(indexOf);
        }
        this.l = aaVar;
        dh dhVar = this.m;
        r rVar2 = this.n;
        dhVar.b((Collection) r.b(this.j));
        this.n.c(this.k);
        if (this.m.getCount() < 20) {
            this.u.setVisibility(8);
        } else {
            this.u.setVisibility(0);
        }
        a(this.l);
    }

    /* access modifiers changed from: protected */
    public final void e() {
        super.e();
        if (this.l == null || this.l.i != 2) {
            if (getIntent().getBooleanExtra("key_show_inputmethod", false)) {
                z();
            }
            if (this.aa.getDrawable() != null) {
                if (this.ag == null) {
                    this.ag = AnimationUtils.loadAnimation(this, R.anim.loading);
                }
                this.aa.startAnimation(this.ag);
            }
            if (a.f(getIntent().getStringExtra("key_commentid"))) {
                ae aeVar = new ae();
                aeVar.k = getIntent().getStringExtra("key_commentid");
                aeVar.h = this.j;
                aeVar.g = this.l;
                aeVar.b = getIntent().getStringExtra("key_owner_id");
                aeVar.f2985a = new aq().b(aeVar.b);
                if (aeVar.f2985a == null) {
                    aeVar.f2985a = new bf(aeVar.b);
                    aeVar.f2985a.i = getIntent().getStringExtra("key_owner_name");
                }
                aeVar.f = getIntent().getStringExtra("key_comment_content");
                a(aeVar);
            }
            a(new u(this, this)).execute(new Object[0]);
            a(new t(this, this, true)).execute(new Object[0]);
            return;
        }
        a((CharSequence) "该留言已经被删除");
        finish();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.bt_feed_send /*2131165595*/:
                new k("S", "S4202").e();
                a(0, (String) null);
                return;
            case R.id.iv_feed_emote /*2131165596*/:
                if (this.af.isShown()) {
                    x();
                    y();
                    return;
                }
                v();
                this.ae.setImageResource(R.drawable.ic_publish_keyboard);
                if (this.h) {
                    this.p.postDelayed(new m(this), 300);
                } else {
                    this.af.b();
                }
                this.G.setVisibility(0);
                this.Z.requestFocus();
                return;
            case R.id.layout_cover /*2131165601*/:
                v();
                x();
                this.h = false;
                this.G.setVisibility(8);
                return;
            case R.id.iv_feed_photo /*2131166248*/:
                if (this.l != null) {
                    Intent intent = new Intent(this, OtherProfileActivity.class);
                    intent.putExtra("momoid", this.l.c);
                    startActivity(intent);
                    return;
                }
                return;
            case R.id.iv_feed_content /*2131166257*/:
                Intent intent2 = new Intent(this, ImageBrowserActivity.class);
                intent2.putExtra("array", new String[]{this.l.getLoadImageId()});
                intent2.putExtra("imagetype", "feed");
                intent2.putExtra("autohide_header", true);
                startActivity(intent2);
                overridePendingTransition(R.anim.zoom_enter, R.anim.normal);
                return;
            case R.id.gv_feed_content /*2131166259*/:
                Intent intent3 = new Intent(this, EmotionProfileActivity.class);
                intent3.putExtra("eid", this.l.k);
                startActivity(intent3);
                return;
            case R.id.feed_layout_app /*2131166261*/:
                GameApp gameApp = this.l.n;
                u uVar = this.l.o;
                if (gameApp != null) {
                    if (gameApp.action != null) {
                        this.e.a((Object) gameApp.action);
                        d.a(gameApp.action, this);
                        return;
                    }
                    return;
                } else if (uVar != null && uVar != null) {
                    this.e.a((Object) uVar.w);
                    d.a(uVar.w, this);
                    return;
                } else {
                    return;
                }
            case R.id.layout_feed_commentcount /*2131166270*/:
                z();
                return;
            default:
                return;
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        w();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.ah != null) {
            this.ah.l();
        }
    }

    public void onItemClick(AdapterView adapterView, View view, int i2, long j2) {
        if (this.l != null) {
            ae aeVar = (ae) this.m.getItem(i2);
            String[] strArr = aeVar.n == 1 ? (this.f.h.equals(this.l.c) || this.f.h.equals(aeVar.b)) ? new String[]{"回复", "查看表情", "删除"} : new String[]{"回复", "查看表情"} : this.f.h.equals(aeVar.b) ? new String[]{"回复", "复制文本", "删除"} : this.f.h.equals(this.l.c) ? new String[]{"回复", "复制文本", "举报", "删除"} : new String[]{"回复", "复制文本", "举报"};
            o oVar = new o(this, strArr);
            oVar.a(new q(this, strArr, aeVar));
            oVar.show();
        }
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 != 4 || !this.af.isShown()) {
            return super.onKeyDown(i2, keyEvent);
        }
        x();
        this.G.setVisibility(8);
        this.h = false;
        return true;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        new k("PO", "P42").e();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        new k("PI", "P42").e();
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        switch (view.getId()) {
            case R.id.tv_feed_editer /*2131165597*/:
                x();
                return false;
            default:
                return false;
        }
    }

    public final void u() {
        this.t.f();
        a(new t(this, this, false)).execute(new Object[0]);
    }

    /* access modifiers changed from: protected */
    public final void v() {
        View currentFocus = getCurrentFocus();
        if (currentFocus != null) {
            this.o.hideSoftInputFromWindow(currentFocus.getWindowToken(), 2);
        }
    }
}
