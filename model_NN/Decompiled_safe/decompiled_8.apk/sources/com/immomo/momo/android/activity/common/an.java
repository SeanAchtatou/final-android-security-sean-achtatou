package com.immomo.momo.android.activity.common;

import android.content.Context;
import com.immomo.momo.R;
import com.immomo.momo.a.a;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.n;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.w;
import com.immomo.momo.service.bean.e;
import com.immomo.momo.util.ao;

final class an extends d {

    /* renamed from: a  reason: collision with root package name */
    private v f1083a = null;
    private int c = 0;
    /* access modifiers changed from: private */
    public /* synthetic */ al d;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public an(al alVar, Context context, int i) {
        super(context);
        this.d = alVar;
        this.c = i;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        w a2 = w.a();
        String str = this.d.M.h;
        a2.c(((e) this.d.T.getItem(this.c)).c, ((InviteSNSActivity) this.d.c()).z(), ((InviteSNSActivity) this.d.c()).y());
        return null;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
        this.f1083a = new v(this.d.c());
        this.f1083a.a("请求提交中...");
        this.f1083a.setCancelable(true);
        this.f1083a.setOnCancelListener(new ao(this));
        this.d.a(this.f1083a);
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        this.d.N();
        if (!(exc instanceof a) || ((a) exc).f670a != 407) {
            super.a(exc);
            return;
        }
        n a2 = n.a(this.d.c(), (int) R.string.invite_sinarebind_into, (int) R.string.invite_sinarebind_btn, (int) R.string.dialog_btn_cancel, new ap(this), new aq(this));
        a2.setTitle((int) R.string.invite_sinarebind_title);
        this.d.a(a2);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        String str = (String) obj;
        if (!android.support.v4.b.a.a((CharSequence) str)) {
            ao.a((CharSequence) str);
        }
        this.d.T.d(this.c);
        this.d.T.notifyDataSetChanged();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        super.b();
        this.d.N();
    }
}
