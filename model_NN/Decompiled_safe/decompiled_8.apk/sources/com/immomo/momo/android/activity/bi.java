package com.immomo.momo.android.activity;

import android.content.DialogInterface;
import android.content.Intent;

final class bi implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ CheckStatusActivity f1045a;

    bi(CheckStatusActivity checkStatusActivity) {
        this.f1045a = checkStatusActivity;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f1045a.startActivity(new Intent("android.settings.WIRELESS_SETTINGS"));
        dialogInterface.dismiss();
    }
}
