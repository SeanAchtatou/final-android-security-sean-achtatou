package com.immomo.momo.android.activity.group;

import android.content.Context;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.android.c.b;
import com.immomo.momo.protocol.a.a.o;
import com.immomo.momo.service.bean.a.e;

final class bi extends b {
    private String c;
    private String d;
    private String e;
    private String f;
    private String g;
    private int h;
    private int i;
    private /* synthetic */ GroupFeedProfileActivity j;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public bi(GroupFeedProfileActivity groupFeedProfileActivity, Context context, String str, String str2, int i2, int i3, String str3, String str4, String str5) {
        super(context);
        this.j = groupFeedProfileActivity;
        this.c = str;
        this.d = str2;
        this.h = i2;
        this.i = i3;
        this.e = str3;
        this.f = str4;
        this.g = str5;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        this.e = this.e.replaceAll("\n{2,}", "\n");
        o a2 = com.immomo.momo.protocol.a.o.a().a(this.c, this.d, this.h, this.i, this.e, this.f, this.g);
        this.j.m.a(a2.f2894a);
        return a2;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
        this.j.y();
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        o oVar = (o) obj;
        oVar.f2894a.f2975a = this.j.f;
        GroupFeedProfileActivity groupFeedProfileActivity = this.j;
        e e2 = this.j.k;
        int i2 = e2.e + 1;
        e2.e = i2;
        GroupFeedProfileActivity.a(groupFeedProfileActivity, i2);
        this.j.l.b(0, oVar.f2894a);
        this.j.V.setText(PoiTypeDef.All);
        this.j.af = true;
        this.j.y();
        this.j.v();
        this.j.ag = null;
        this.j.X.setText(PoiTypeDef.All);
        this.j.X.setVisibility(8);
        super.a(oVar);
    }
}
