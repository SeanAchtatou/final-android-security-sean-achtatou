package com.immomo.momo.android.activity.emotestore;

import android.content.DialogInterface;
import android.content.Intent;
import com.immomo.momo.android.pay.RechargeActivity;
import com.immomo.momo.util.k;

final class t implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ EmotionProfileActivity f1331a;

    t(EmotionProfileActivity emotionProfileActivity) {
        this.f1331a = emotionProfileActivity;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        new k("C", "C94905").e();
        this.f1331a.startActivity(new Intent(this.f1331a, RechargeActivity.class));
    }
}
