package com.immomo.momo.android.a;

import android.view.View;

final class w implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ q f917a;
    private final /* synthetic */ aa b;
    private final /* synthetic */ int c;
    private final /* synthetic */ int d;

    w(q qVar, aa aaVar, int i, int i2) {
        this.f917a = qVar;
        this.b = aaVar;
        this.c = i;
        this.d = i2;
    }

    public final void onClick(View view) {
        this.f917a.b.getOnChildClickListener().onChildClick(this.f917a.b, this.b.d, this.c, this.d, 0);
    }
}
