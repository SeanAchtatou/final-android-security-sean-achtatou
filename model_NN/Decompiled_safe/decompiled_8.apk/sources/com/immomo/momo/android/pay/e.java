package com.immomo.momo.android.pay;

import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.view.View;
import com.immomo.momo.android.view.a.q;
import mm.purchasesdk.PurchaseCode;

final class e extends ClickableSpan {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ BuyMemberActivity f2557a;

    e(BuyMemberActivity buyMemberActivity) {
        this.f2557a = buyMemberActivity;
    }

    public final void onClick(View view) {
        q a2 = q.a(this.f2557a);
        a2.b(PurchaseCode.BILL_DYMARK_CREATE_ERROR);
        a2.setTitle("会员协议");
        a2.a(BuyMemberActivity.h);
        a2.show();
    }

    public final void updateDrawState(TextPaint textPaint) {
    }
}
