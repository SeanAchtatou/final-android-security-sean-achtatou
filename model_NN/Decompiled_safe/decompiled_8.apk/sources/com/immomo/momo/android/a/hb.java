package com.immomo.momo.android.a;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.a.a;
import com.immomo.momo.service.bean.a.i;
import com.immomo.momo.service.bean.at;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.service.bean.n;
import com.immomo.momo.service.bean.o;
import com.immomo.momo.util.j;
import com.immomo.momo.util.m;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public final class hb extends a {
    /* access modifiers changed from: private */
    public Context d = null;
    private List e;
    private List f;
    private bf g;
    private AbsListView h = null;

    public hb(Context context, List list, List list2, bf bfVar, AbsListView absListView) {
        super(context, new ArrayList());
        new hc();
        this.d = context;
        a(list, list2);
        this.d = context;
        this.h = absListView;
        this.g = bfVar;
        new m("GroupListAdapter").a();
    }

    public final int a(String str, int i) {
        hf hfVar = new hf();
        hfVar.h = i;
        hfVar.e = str;
        return e(hfVar);
    }

    public final void a(a aVar) {
        this.e.add(0, aVar);
        hf hfVar = new hf();
        hfVar.h = 0;
        hfVar.e = aVar.b;
        hfVar.f = aVar;
        this.f671a.add(1, hfVar);
        notifyDataSetChanged();
    }

    public final void a(List list) {
        if (this.e != null) {
            this.e.clear();
        }
        a(list, this.f);
    }

    public final void a(List list, List list2) {
        this.e = list;
        this.f = list2;
        this.f671a.clear();
        hf hfVar = new hf();
        hfVar.h = hf.b;
        this.f671a.add(hfVar);
        if (list == null || list.size() <= 0) {
            hf hfVar2 = new hf();
            hfVar2.h = hf.c;
            this.f671a.add(hfVar2);
        } else {
            Iterator it = list.iterator();
            while (it.hasNext()) {
                a aVar = (a) it.next();
                hf hfVar3 = new hf();
                hfVar3.h = 0;
                hfVar3.f = aVar;
                hfVar3.e = aVar.b;
                this.f671a.add(hfVar3);
            }
        }
        hf hfVar4 = new hf();
        hfVar4.h = hf.b;
        this.f671a.add(hfVar4);
        if (list2 == null || list2.size() <= 0) {
            hf hfVar5 = new hf();
            hfVar5.h = hf.d;
            this.f671a.add(hfVar5);
        } else {
            Iterator it2 = list2.iterator();
            while (it2.hasNext()) {
                n nVar = (n) it2.next();
                hf hfVar6 = new hf();
                hfVar6.h = hf.f857a;
                hfVar6.g = nVar;
                hfVar6.e = nVar.f;
                this.f671a.add(hfVar6);
            }
        }
        notifyDataSetChanged();
    }

    public final void b(int i) {
        hf hfVar = (hf) this.f671a.get(i);
        if (hfVar.h == hf.f857a) {
            this.f.remove(new n(hfVar.e));
        } else if (hfVar.h == 0) {
            this.e.remove(new a(hfVar.e));
        }
        super.b(i);
    }

    public final void b(List list) {
        if (this.f != null) {
            this.f.clear();
        }
        a(this.e, list);
    }

    public final /* synthetic */ boolean c(Object obj) {
        hf hfVar = (hf) obj;
        if (hfVar.h == hf.f857a) {
            this.f.remove(new n(hfVar.e));
        } else if (hfVar.h == 0) {
            this.e.remove(new a(hfVar.e));
        }
        return super.c(hfVar);
    }

    public final int e() {
        if (this.f == null) {
            return 0;
        }
        return this.f.size();
    }

    public final int getItemViewType(int i) {
        return ((hf) getItem(i)).h;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        String str;
        int i2 = 0;
        getItem(i);
        if (getItemViewType(i) == hf.f857a) {
            n nVar = ((hf) getItem(i)).g;
            if (view == null) {
                he heVar = new he((byte) 0);
                view = LayoutInflater.from(this.d).inflate((int) R.layout.listitem_discuss_relation, (ViewGroup) null);
                heVar.f856a = (ImageView) view.findViewById(R.id.userlist_item_iv_face);
                heVar.b = (TextView) view.findViewById(R.id.userlist_item_tv_name);
                heVar.c = (TextView) view.findViewById(R.id.userlist_item_tv_sign);
                heVar.e = (TextView) view.findViewById(R.id.userlist_item_tv_role);
                view.findViewById(R.id.userlist_item_tv_status);
                heVar.f = (TextView) view.findViewById(R.id.userlist_item_tv_discount);
                heVar.d = view.findViewById(R.id.userlist_item_mute);
                heVar.g = view.findViewById(R.id.discusslist_item_line);
                view.setTag(R.id.tag_userlist_item, heVar);
            }
            he heVar2 = (he) view.getTag(R.id.tag_userlist_item);
            if (!android.support.v4.b.a.a((CharSequence) nVar.b)) {
                heVar2.b.setText(nVar.b);
            } else {
                heVar2.b.setText(nVar.f);
            }
            heVar2.f.setText(String.valueOf(nVar.j));
            if (nVar.e != null) {
                heVar2.c.setText(nVar.m);
            } else {
                heVar2.c.setText(PoiTypeDef.All);
            }
            if (this.g.h.equals(nVar.c)) {
                heVar2.e.setVisibility(0);
            } else {
                heVar2.e.setVisibility(8);
            }
            at r = g.r();
            if (r != null) {
                o d2 = r.d(nVar.f);
                if (d2 == null || d2.f3033a) {
                    heVar2.d.setVisibility(8);
                } else {
                    heVar2.d.setVisibility(0);
                }
            } else {
                heVar2.d.setVisibility(8);
            }
            if (i == getCount() - 1) {
                heVar2.g.setVisibility(8);
            } else {
                heVar2.g.setVisibility(0);
            }
            j.a(nVar, heVar2.f856a, this.h, 3);
            return view;
        } else if (getItemViewType(i) == hf.b) {
            View inflate = LayoutInflater.from(this.d).inflate((int) R.layout.listitem_groupdiscusstitle, (ViewGroup) null);
            TextView textView = (TextView) inflate.findViewById(16908310);
            TextView textView2 = (TextView) inflate.findViewById(16908313);
            if (i == 0) {
                StringBuilder sb = new StringBuilder("群组 (");
                if (this.e != null) {
                    i2 = this.e.size();
                }
                String sb2 = sb.append(i2).append(")").toString();
                textView2.setText("创建群组");
                str = sb2;
            } else {
                StringBuilder sb3 = new StringBuilder("多人对话 (");
                if (this.f != null) {
                    i2 = this.f.size();
                }
                String sb4 = sb3.append(i2).append(")").toString();
                textView2.setText("创建多人对话");
                str = sb4;
            }
            inflate.setOnClickListener(new hd(this, i));
            textView.setText(str);
            return inflate;
        } else if (getItemViewType(i) == 0) {
            a aVar = ((hf) getItem(i)).f;
            if (view == null) {
                hg hgVar = new hg((byte) 0);
                view = LayoutInflater.from(this.d).inflate((int) R.layout.listitem_group_relation, (ViewGroup) null);
                hgVar.f858a = (ImageView) view.findViewById(R.id.userlist_item_iv_face);
                hgVar.b = (TextView) view.findViewById(R.id.userlist_item_tv_name);
                hgVar.c = (TextView) view.findViewById(R.id.userlist_item_tv_distance);
                hgVar.d = (TextView) view.findViewById(R.id.userlist_item_tv_sign);
                hgVar.e = (TextView) view.findViewById(R.id.userlist_item_tv_sitename);
                hgVar.g = (TextView) view.findViewById(R.id.userlist_item_tv_role);
                hgVar.h = (TextView) view.findViewById(R.id.userlist_item_tv_status);
                hgVar.i = (ImageView) view.findViewById(R.id.userlist_item_pic_iv_party);
                view.findViewById(R.id.userlist_item_layout_membercount_backgroud);
                view.findViewById(R.id.userlist_item_iv_person_icon);
                hgVar.f = view.findViewById(R.id.userlist_item_mute);
                hgVar.j = view.findViewById(R.id.grouplist_item_line);
                view.setTag(R.id.tag_userlist_item, hgVar);
            }
            hg hgVar2 = (hg) view.getTag(R.id.tag_userlist_item);
            hgVar2.c.setText(aVar.p);
            if (aVar.a()) {
                hgVar2.b.setTextColor(g.c((int) R.color.font_vip_name));
            } else {
                hgVar2.b.setTextColor(g.c((int) R.color.text_color));
            }
            if (!android.support.v4.b.a.a((CharSequence) aVar.c)) {
                hgVar2.b.setText(aVar.c);
            } else {
                hgVar2.b.setText(aVar.b);
            }
            if (aVar.h != null) {
                hgVar2.d.setText(aVar.h);
            } else {
                hgVar2.d.setText(PoiTypeDef.All);
            }
            if (this.g.h.equals(aVar.g)) {
                hgVar2.g.setVisibility(0);
            } else {
                hgVar2.g.setVisibility(8);
            }
            if (aVar.C) {
                hgVar2.i.setVisibility(0);
            } else {
                hgVar2.i.setVisibility(8);
            }
            if (aVar.G == 1) {
                hgVar2.h.setVisibility(0);
                hgVar2.h.setText((int) R.string.grouplist_group_status_waiting);
            } else if (aVar.G == 4) {
                hgVar2.h.setVisibility(0);
                hgVar2.h.setText((int) R.string.grouplist_group_status_baned);
            } else if (aVar.G == 3) {
                hgVar2.h.setVisibility(0);
                hgVar2.h.setText((int) R.string.grouplist_group_status_notpass);
            } else {
                hgVar2.h.setVisibility(8);
            }
            if (!android.support.v4.b.a.a((CharSequence) aVar.K)) {
                hgVar2.e.setVisibility(0);
                hgVar2.e.setText(aVar.K);
            } else {
                hgVar2.e.setVisibility(8);
            }
            at r2 = g.r();
            if (r2 != null) {
                i c = r2.c(aVar.b);
                if (c == null || c.f2978a) {
                    hgVar2.f.setVisibility(8);
                } else {
                    hgVar2.f.setVisibility(0);
                }
            } else {
                hgVar2.f.setVisibility(8);
            }
            if (((hf) getItem(i + 1)).h != ((hf) getItem(i)).h) {
                hgVar2.j.setVisibility(8);
            } else {
                hgVar2.j.setVisibility(0);
            }
            j.a(aVar, hgVar2.f858a, this.h, 3);
            return view;
        } else if (getItemViewType(i) == hf.c) {
            View inflate2 = LayoutInflater.from(this.d).inflate((int) R.layout.listitem_relation_empty, (ViewGroup) null);
            ((TextView) inflate2.findViewById(R.id.tv_tip)).setText((int) R.string.grouplist_empty_tip);
            return inflate2;
        } else if (getItemViewType(i) != hf.d) {
            return view;
        } else {
            View inflate3 = LayoutInflater.from(this.d).inflate((int) R.layout.listitem_relation_empty, (ViewGroup) null);
            ((TextView) inflate3.findViewById(R.id.tv_tip)).setText((int) R.string.discusslist_empty_tip);
            return inflate3;
        }
    }

    public final int getViewTypeCount() {
        return 5;
    }
}
