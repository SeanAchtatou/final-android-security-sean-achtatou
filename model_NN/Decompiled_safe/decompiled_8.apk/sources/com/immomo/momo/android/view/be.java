package com.immomo.momo.android.view;

import android.widget.AbsListView;
import android.widget.BaseAdapter;

final class be implements AbsListView.OnScrollListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ HandyListView f2739a;

    be(HandyListView handyListView) {
        this.f2739a = handyListView;
    }

    public final void onScroll(AbsListView absListView, int i, int i2, int i3) {
        if (this.f2739a.k != null) {
            this.f2739a.k.onScroll(absListView, i, i2, i3);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.android.view.HandyListView.a(com.immomo.momo.android.view.HandyListView, boolean):void
     arg types: [com.immomo.momo.android.view.HandyListView, int]
     candidates:
      com.immomo.momo.android.view.HandyListView.a(com.immomo.momo.android.view.HandyListView, float):void
      com.immomo.momo.android.view.HandyListView.a(com.immomo.momo.android.view.HandyListView, boolean):void */
    public final void onScrollStateChanged(AbsListView absListView, int i) {
        if (this.f2739a.k != null) {
            this.f2739a.k.onScrollStateChanged(absListView, i);
        }
        switch (i) {
            case 0:
                this.f2739a.g = false;
                this.f2739a.o = 0.0f;
                if (this.f2739a.d() && this.f2739a.j != null && (this.f2739a.j instanceof BaseAdapter)) {
                    ((BaseAdapter) this.f2739a.j).notifyDataSetChanged();
                    return;
                }
                return;
            case 1:
                this.f2739a.g = true;
                return;
            case 2:
                this.f2739a.g = true;
                return;
            default:
                return;
        }
    }
}
