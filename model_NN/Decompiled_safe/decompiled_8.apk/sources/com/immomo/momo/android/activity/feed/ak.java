package com.immomo.momo.android.activity.feed;

import android.content.Intent;
import android.support.v4.b.a;
import com.immomo.momo.android.broadcast.d;
import com.immomo.momo.android.broadcast.f;

final class ak implements d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ OtherFeedListActivity f1442a;

    ak(OtherFeedListActivity otherFeedListActivity) {
        this.f1442a = otherFeedListActivity;
    }

    public final void a(Intent intent) {
        if (f.f2349a.equals(intent.getAction())) {
            String str = intent.getExtras() != null ? (String) intent.getExtras().get("feedid") : null;
            String str2 = intent.getExtras() != null ? (String) intent.getExtras().get("userid") : null;
            if (!a.a((CharSequence) str) && this.f1442a.f.h.equals(str2)) {
                this.f1442a.p.a(str);
            }
        }
    }
}
