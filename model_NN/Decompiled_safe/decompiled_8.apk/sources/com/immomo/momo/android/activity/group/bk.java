package com.immomo.momo.android.activity.group;

import android.content.Context;
import android.os.Bundle;
import com.immomo.momo.android.c.b;
import com.immomo.momo.g;
import com.immomo.momo.protocol.a.o;
import com.immomo.momo.service.bean.a.e;

final class bk extends b {
    private e c;
    private /* synthetic */ GroupFeedProfileActivity d;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public bk(GroupFeedProfileActivity groupFeedProfileActivity, Context context, e eVar) {
        super(context);
        this.d = groupFeedProfileActivity;
        this.c = eVar;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        String b = o.a().b(this.c.f);
        this.d.m.f(this.c.f);
        Bundle bundle = new Bundle();
        bundle.putString("groupfeedid", this.c.f);
        g.d().a(bundle, "actions.groupfeeddelete");
        return b;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        a((String) obj);
        this.c.h = 2;
        this.d.finish();
    }
}
