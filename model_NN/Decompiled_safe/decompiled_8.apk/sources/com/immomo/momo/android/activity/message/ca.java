package com.immomo.momo.android.activity.message;

import android.view.View;
import com.immomo.momo.android.view.a.n;
import java.util.List;

final class ca implements View.OnClickListener {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public /* synthetic */ HiSessionListActivity f1961a;

    ca(HiSessionListActivity hiSessionListActivity) {
        this.f1961a = hiSessionListActivity;
    }

    public final void onClick(View view) {
        List d = this.f1961a.v.d();
        if (!d.isEmpty()) {
            n.a(this.f1961a, "确认删除所选的招呼吗？", new cb(this, d)).show();
        }
    }
}
