package com.immomo.momo.android.map;

import android.location.Location;
import com.amap.mapapi.core.GeoPoint;

final class ak implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ aj f2464a;
    private final /* synthetic */ Location b;

    ak(aj ajVar, Location location) {
        this.f2464a = ajVar;
        this.b = location;
    }

    public final void run() {
        if (!this.f2464a.f2463a.isFinishing() && this.f2464a.f2463a.b != null) {
            this.f2464a.f2463a.b.dismiss();
            this.f2464a.f2463a.b = null;
        }
        this.f2464a.f2463a.i = this.b.getLatitude();
        this.f2464a.f2463a.j = this.b.getLongitude();
        this.f2464a.f2463a.c = new GeoPoint((int) (this.f2464a.f2463a.i * 1000000.0d), (int) (this.f2464a.f2463a.j * 1000000.0d));
        this.f2464a.f2463a.f.getController().animateTo(this.f2464a.f2463a.c);
        this.f2464a.f2463a.f.getController().setCenter(this.f2464a.f2463a.c);
        RomaAMapActivity.e(this.f2464a.f2463a);
    }
}
