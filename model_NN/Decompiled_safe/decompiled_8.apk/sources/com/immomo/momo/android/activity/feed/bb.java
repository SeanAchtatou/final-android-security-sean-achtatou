package com.immomo.momo.android.activity.feed;

import com.immomo.momo.android.c.g;
import com.immomo.momo.android.view.MGifImageView;
import java.io.File;

final class bb implements g {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ PublishFeedActivity f1459a;
    private final /* synthetic */ MGifImageView b;
    private final /* synthetic */ boolean c;

    bb(PublishFeedActivity publishFeedActivity, MGifImageView mGifImageView, boolean z) {
        this.f1459a = publishFeedActivity;
        this.b = mGifImageView;
        this.c = z;
    }

    public final /* synthetic */ void a(Object obj) {
        this.b.post(new bc(this, (File) obj, this.c, this.b));
    }
}
