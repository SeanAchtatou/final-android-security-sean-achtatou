package com.immomo.momo.android.activity.message;

import android.location.Location;
import com.immomo.momo.R;
import com.immomo.momo.android.b.l;
import com.immomo.momo.android.b.m;
import com.immomo.momo.android.b.p;
import com.immomo.momo.android.b.z;
import com.immomo.momo.service.af;
import com.immomo.momo.service.bean.Message;
import com.immomo.momo.util.ao;

final class cf extends p {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ce f1966a;
    private final /* synthetic */ m c;
    private final /* synthetic */ Message d;

    cf(ce ceVar, m mVar, Message message) {
        this.f1966a = ceVar;
        this.c = mVar;
        this.d = message;
    }

    public final void a(Location location, int i, int i2, int i3) {
        if (!z.a(location)) {
            this.c.a(location, i, i2, i3);
            this.d.status = 3;
            af.c().a(this.d.msgId, 3, this.d.chatType);
            ce ceVar = this.f1966a;
            ce.b(this.d);
            ao.g(R.string.errormsg_location_failed);
            return;
        }
        this.f1966a.b.a((Object) ("type: " + i));
        if (i == 1) {
            this.c.a(location, i, i2, i3);
            return;
        }
        new l(this.c, 0).execute(location);
    }
}
