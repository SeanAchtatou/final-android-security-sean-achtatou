package com.immomo.momo.android.view;

import android.content.Context;
import android.content.res.Resources;
import android.support.v4.b.a;
import android.text.SpannableStringBuilder;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.g;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class bp {

    /* renamed from: a  reason: collision with root package name */
    private static final Map f2748a;
    private static final String[] b = {"[微笑]", "[哈哈]", "[嘻嘻]", "[偷笑]", "[吐舌头]", "[飞吻]", "[挖鼻孔]", "[不开心]", "[无聊]", "[惊讶]", "[疑问]", "[委屈]", "[害羞]", "[汗]", "[斜眼]", "[尴尬]", "[抓狂]", "[衰]", "[泪]", "[仰慕]", "[拜拜]", "[愤怒]", "[鄙视]", "[鼓掌]", "[得意]", "[恶魔]", "[酷]", "[摊手]", "[砸死你]", "[囧]", "[花心]", "[财迷]", "[奋斗]", "[No]", "[瞌睡]", "[咆哮]", "[闭嘴]", "[晕]", "[呕吐]", "[悲剧]", "[坏]", "[中指]", "[饿]", "[GOOD]", "[BAD]", "[勾引]", "[OK]", "[握手]", "[拉勾]", "[猪头]", "[喵]", "[咖啡]", "[蛋糕]", "[寿司]", "[西餐]", "[碰杯]", "[礼物]", "[路过]", "[大便]", "[心]", "[心碎]", "[花]", "[车]", "[晚安]"};
    private static final String[] c = {"zem1", "zem2", "zem3", "zem4", "zem5", "zem6", "zem7", "zem8", "zem9", "zem10", "zem11", "zem12", "zem13", "zem14", "zem15", "zem16", "zem17", "zem18", "zem19", "zem20", "zem21", "zem22", "zem23", "zem24", "zem25", "zem26", "zem27", "zem28", "zem29", "zem30", "zem31", "zem32", "zem33", "zem34", "zem35", "zem36", "zem37", "zem38", "zem39", "zem40", "zem41", "zem42", "zem43", "zem44", "zem45", "zem46", "zem47", "zem48", "zem64", "zem49", "zem50", "zem51", "zem52", "zem53", "zem54", "zem55", "zem56", "zem57", "zem58", "zem59", "zem60", "zem61", "zem62", "zem63"};
    private static final String d = ("(" + a.a(b, "|").replaceAll("\\[", "\\\\[").replaceAll("\\]", "\\\\]") + ")");

    static {
        HashMap hashMap = new HashMap();
        for (int i = 0; i < b.length; i++) {
            hashMap.put(b[i], c[i]);
        }
        f2748a = hashMap;
    }

    public static final CharSequence a(CharSequence charSequence) {
        if (charSequence == null) {
            return PoiTypeDef.All;
        }
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(charSequence);
        Matcher matcher = Pattern.compile(d).matcher(charSequence);
        String h = g.h();
        Resources l = g.l();
        Context c2 = g.c();
        boolean z = false;
        while (matcher.find()) {
            z = true;
            String a2 = a(matcher.group());
            if (!a.a((CharSequence) a2)) {
                spannableStringBuilder.setSpan(new ad(c2, l.getIdentifier(a2, "drawable", h)), matcher.start(), matcher.end(), 33);
            }
        }
        return z ? spannableStringBuilder : charSequence;
    }

    public static final String a(String str) {
        return (String) f2748a.get(str);
    }

    public static final String[] a() {
        return b;
    }

    public static final CharSequence b(CharSequence charSequence) {
        if (charSequence == null) {
            return PoiTypeDef.All;
        }
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(charSequence);
        Matcher matcher = Pattern.compile(d).matcher(charSequence);
        boolean z = false;
        String h = g.h();
        Resources l = g.l();
        Context c2 = g.c();
        while (matcher.find()) {
            z = true;
            String a2 = a(matcher.group());
            if (!a.a((CharSequence) a2)) {
                spannableStringBuilder.setSpan(new de(c2, l.getIdentifier(a2, "drawable", h)), matcher.start(), matcher.end(), 33);
            }
        }
        return z ? spannableStringBuilder : charSequence;
    }

    public static final String b() {
        return d;
    }
}
