package com.immomo.momo.android.activity.maintab;

import android.graphics.Bitmap;
import android.widget.ImageView;

final class cl implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ ImageView f1883a;
    private final /* synthetic */ Bitmap b;

    cl(ImageView imageView, Bitmap bitmap) {
        this.f1883a = imageView;
        this.b = bitmap;
    }

    public final void run() {
        this.f1883a.setImageBitmap(this.b);
    }
}
