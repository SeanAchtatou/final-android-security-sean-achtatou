package com.immomo.momo.android.activity;

import android.content.res.TypedArray;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.View;
import com.immomo.momo.R;
import com.immomo.momo.a;
import com.immomo.momo.android.activity.maintab.MaintabActivity;
import com.immomo.momo.android.view.ScrollViewPager;
import com.immomo.momo.android.view.a.ao;
import com.immomo.momo.g;

public class UserGuideActivity extends ao implements View.OnClickListener {
    TypedValue h = new TypedValue();
    private ScrollViewPager i;
    /* access modifiers changed from: private */
    public TypedArray j = null;
    /* access modifiers changed from: private */
    public TypedArray k = null;
    /* access modifiers changed from: private */
    public TypedArray l = null;

    public void onClick(View view) {
        boolean z = true;
        if (!MaintabActivity.class.getName().equals(l())) {
            finish();
            return;
        }
        if (!((this.f != null && (this.f.an || this.f.at || this.f.ar)) && a.q) || !g.y()) {
            z = false;
        }
        if (z) {
            ao aoVar = new ao(this, this.f);
            aoVar.b((int) R.string.newversion_share);
            aoVar.setTitle((int) R.string.newversion_share_title);
            aoVar.d();
            aoVar.a(new lq(this, aoVar));
            aoVar.setOnDismissListener(new ls(this));
            aoVar.show();
            return;
        }
        finish();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_usergui);
        this.i = (ScrollViewPager) findViewById(R.id.usergui_scrollviewpager);
        this.j = getResources().obtainTypedArray(R.array.guide_content);
        this.k = getResources().obtainTypedArray(R.array.guide_bottom);
        this.l = getResources().obtainTypedArray(R.array.guide_backgroud);
        this.i.setAdapter(new lt(this, (byte) 0));
    }
}
