package com.immomo.momo.android.activity.contacts;

import android.content.Context;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.w;
import com.immomo.momo.service.bean.e;
import com.immomo.momo.service.bean.f;

final class t extends d {

    /* renamed from: a  reason: collision with root package name */
    private v f1197a = null;
    private String c = PoiTypeDef.All;
    private int d = 0;
    private int e = 0;
    private /* synthetic */ CommunityPeopleActivity f;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public t(CommunityPeopleActivity communityPeopleActivity, Context context, String str, int i, int i2) {
        super(context);
        this.f = communityPeopleActivity;
        if (communityPeopleActivity.o != null) {
            communityPeopleActivity.o.cancel(true);
        }
        communityPeopleActivity.o = this;
        this.c = str;
        this.d = i;
        this.e = i2;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        switch (this.f.i) {
            case 1:
                return w.a().f(this.c, ((e) ((f) this.f.m.get(this.d)).b.get(this.e)).c);
            case 2:
                return w.a().g(this.c, ((e) ((f) this.f.m.get(this.d)).b.get(this.e)).c);
            case 3:
                return w.a().h(this.c, ((e) ((f) this.f.m.get(this.d)).b.get(this.e)).c);
            default:
                return null;
        }
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
        this.f1197a = new v(this.f);
        this.f1197a.a("请求提交中...");
        this.f1197a.setCancelable(true);
        this.f1197a.setOnCancelListener(new u(this));
        this.f.a(this.f1197a);
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        super.a(exc);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        String str = (String) obj;
        if (str != null) {
            this.f.b((CharSequence) str);
        }
        this.f.k.a(this.d, this.e);
    }

    /* access modifiers changed from: protected */
    public final void b() {
        super.b();
        this.f.p();
    }
}
