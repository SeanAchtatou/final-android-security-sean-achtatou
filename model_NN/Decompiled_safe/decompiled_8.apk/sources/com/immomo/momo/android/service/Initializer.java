package com.immomo.momo.android.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import com.immomo.momo.android.broadcast.a;
import com.immomo.momo.g;
import com.immomo.momo.util.ak;
import com.immomo.momo.util.m;
import java.io.File;
import java.util.Date;

public class Initializer extends Service {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public ak f2599a = null;
    private boolean b = false;
    /* access modifiers changed from: private */
    public boolean c = false;
    /* access modifiers changed from: private */
    public boolean d = false;
    /* access modifiers changed from: private */
    public boolean e = false;
    private h f = null;
    private boolean g = false;
    private m h = new m(this);

    private void a() {
        this.f = new h(this, this);
        this.f.execute(new Object[0]);
    }

    static /* synthetic */ void a(File file) {
        for (int i = 0; i < 10; i++) {
            File file2 = new File(file, new StringBuilder(String.valueOf(i)).toString());
            if (!file2.exists()) {
                file2.mkdirs();
            }
        }
        for (char c2 = 'a'; c2 <= 'f'; c2 = (char) (c2 + 1)) {
            File file3 = new File(file, new StringBuilder(String.valueOf(c2)).toString());
            if (!file3.exists()) {
                file3.mkdirs();
            }
        }
    }

    /* access modifiers changed from: private */
    public synchronized void b() {
        if (this.b && this.d && this.c && this.e) {
            stopSelf();
        }
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.ak.a(java.lang.String, java.lang.Boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.immomo.momo.util.ak.a(android.content.Context, java.lang.String):com.immomo.momo.util.ak
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Integer):int
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Long):long
      com.immomo.momo.util.ak.a(java.lang.String, int):void
      com.immomo.momo.util.ak.a(java.lang.String, long):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.String):void
      com.immomo.momo.util.ak.a(java.lang.String, boolean):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, boolean]
     candidates:
      com.immomo.momo.util.ak.a(android.content.Context, java.lang.String):com.immomo.momo.util.ak
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Integer):int
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Long):long
      com.immomo.momo.util.ak.a(java.lang.String, int):void
      com.immomo.momo.util.ak.a(java.lang.String, long):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.String):void
      com.immomo.momo.util.ak.a(java.lang.String, boolean):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Boolean):boolean
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void */
    public void onCreate() {
        Date b2;
        super.onCreate();
        this.h.a((Object) "oncreate~~~");
        this.f2599a = g.d().f668a;
        if (!this.f2599a.a("app_inited_" + g.z(), (Boolean) false)) {
            this.f2599a.a("app_inited_" + g.z(), (Object) true);
        }
        sendBroadcast(new Intent(a.f2345a));
        this.b = true;
        b();
        if (this.f2599a.a("imagecache_inited", (Boolean) false)) {
            this.d = true;
            b();
        } else {
            new f(this, this).execute(new Object[0]);
        }
        a();
        if (g.q() == null || !g.r().c || ((b2 = g.r().b("contact_uploadtime")) != null && Math.abs(System.currentTimeMillis() - b2.getTime()) < 86400000)) {
            this.e = true;
            b();
        } else {
            new j(this, getApplicationContext()).execute(new Object[0]);
        }
        this.g = true;
    }

    public void onDestroy() {
        super.onDestroy();
        sendBroadcast(new Intent(a.f2345a));
    }

    public int onStartCommand(Intent intent, int i, int i2) {
        if (intent != null && !this.g && "param_icon".equals(intent.getStringExtra("init_param"))) {
            if (this.f != null) {
                this.f.cancel(true);
            }
            a();
        }
        this.g = false;
        return super.onStartCommand(intent, i, i2);
    }
}
