package com.immomo.momo.android.a.a;

import android.graphics.drawable.AnimationDrawable;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.message.a;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.Message;
import com.immomo.momo.util.m;
import mm.purchasesdk.PurchaseCode;

public final class d extends ab implements View.OnClickListener, View.OnLongClickListener {

    /* renamed from: a  reason: collision with root package name */
    private static m f689a = new m("test_momo", "[ --- AudioMessageItem ---]");
    private com.immomo.momo.plugin.audio.d o = null;
    /* access modifiers changed from: private */
    public AnimationDrawable p = null;
    private ImageView q;
    private ImageView r;
    private TextView s = null;
    private Handler t = new e(this, e().getMainLooper());

    protected d(a aVar) {
        super(aVar);
    }

    public static void a(Message message, a aVar) {
        if (aVar != null && message.tempFile != null && message.tempFile.exists()) {
            aVar.g(message);
        } else if (!message.isLoadingResourse) {
            message.isLoadingResourse = true;
            j.add(message.msgId);
            f fVar = new f(message, aVar);
            com.immomo.momo.a.C();
            new Thread(new com.immomo.momo.android.c.m(message, fVar)).start();
            if (aVar != null) {
                aVar.Y();
            }
        }
    }

    /* access modifiers changed from: private */
    public void c() {
        System.out.println("MOMO MOMO AudioMessageItem.stopAnimation()");
        if (this.p != null) {
            this.p.stop();
        }
        this.r.setVisibility(8);
        this.q.setVisibility(0);
        this.q.setImageResource(R.drawable.ic_chat_audio_play);
    }

    /* access modifiers changed from: private */
    public void d() {
        this.p = new AnimationDrawable();
        this.p.addFrame(g.b((int) R.drawable.ic_chat_audio_playing0), PurchaseCode.UNSUPPORT_ENCODING_ERR);
        this.p.addFrame(g.b((int) R.drawable.ic_chat_audio_playing1), PurchaseCode.UNSUPPORT_ENCODING_ERR);
        this.p.addFrame(g.b((int) R.drawable.ic_chat_audio_playing2), PurchaseCode.UNSUPPORT_ENCODING_ERR);
        this.p.addFrame(g.b((int) R.drawable.ic_chat_audio_playing3), PurchaseCode.UNSUPPORT_ENCODING_ERR);
        this.p.setOneShot(false);
        this.q.setImageDrawable(this.p);
        this.r.setVisibility(8);
        this.q.setVisibility(0);
        this.t.sendEmptyMessage(398);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        View inflate = this.k.inflate((int) R.layout.message_audio, (ViewGroup) null);
        this.i.addView(inflate, 0);
        this.q = (ImageView) inflate.findViewById(R.id.message_iv_playimage);
        this.s = (TextView) inflate.findViewById(R.id.message_tv_audiotime);
        this.r = (ImageView) inflate.findViewById(R.id.download_view);
        this.i.setOnLongClickListener(this);
        this.i.setOnClickListener(this);
    }

    /* access modifiers changed from: protected */
    public final void b() {
        System.out.println("MOMO MOMO AudioMessageItem.onFillMessage()");
        c();
        this.o = com.immomo.momo.plugin.audio.d.a();
        this.s.setText(String.valueOf(Math.round(((float) this.h.audiotime) / 10.0f)) + " ''");
        if (this.o.a(this.h)) {
            this.o.a(e().w());
            d();
        } else if (j != null && j.contains(this.h.msgId)) {
            this.p = new AnimationDrawable();
            this.p.addFrame(g.b((int) R.drawable.ic_loading_msgplus_01), PurchaseCode.UNSUPPORT_ENCODING_ERR);
            this.p.addFrame(g.b((int) R.drawable.ic_loading_msgplus_02), PurchaseCode.UNSUPPORT_ENCODING_ERR);
            this.p.addFrame(g.b((int) R.drawable.ic_loading_msgplus_03), PurchaseCode.UNSUPPORT_ENCODING_ERR);
            this.p.addFrame(g.b((int) R.drawable.ic_loading_msgplus_04), PurchaseCode.UNSUPPORT_ENCODING_ERR);
            this.p.setOneShot(false);
            this.r.setVisibility(0);
            this.r.setImageDrawable(this.p);
            this.q.setVisibility(8);
            this.t.sendEmptyMessage(398);
        }
    }

    public final void onClick(View view) {
        f689a.a("+++++ onclick:" + this.h, (Throwable) null);
        if (this.o.a(this.h)) {
            a e = e();
            Message message = this.h;
            e.af();
            j.remove(this.h.msgId);
            e().Y();
            return;
        }
        a(this.h, e());
    }

    public final boolean onLongClick(View view) {
        a(this.h.status == 3 ? b : c);
        return true;
    }
}
