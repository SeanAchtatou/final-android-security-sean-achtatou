package com.immomo.momo.android.c;

import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.android.activity.fs;
import com.immomo.momo.util.h;
import com.immomo.momo.util.m;
import java.io.File;

public class o extends t {

    /* renamed from: a  reason: collision with root package name */
    protected String f2389a = null;
    protected File b = null;
    protected int c;
    private String d;
    private m e = new m(getClass().getSimpleName());
    private fs f;

    static {
        new m("rlog");
    }

    public o(String str, g gVar, int i, fs fsVar) {
        super(gVar);
        if (str == null || PoiTypeDef.All.equals(str)) {
            throw new RuntimeException(new NullPointerException("imageId == null || \"\".equals(imageId)"));
        }
        this.f2389a = str;
        this.c = i;
        this.f = fsVar;
        this.b = h.a(str, i);
    }

    public final void a() {
        if (this.b.exists()) {
            u.e().execute(this);
        } else {
            u.d().execute(this);
        }
    }

    public final void a(String str) {
        this.d = str;
    }

    public final String b() {
        return this.f2389a;
    }

    public final boolean c() {
        return this.b.exists();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0097, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0098, code lost:
        r8 = r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x00b2, code lost:
        r0 = th;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00b2 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:1:0x0001] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r9 = this;
            r1 = 0
            java.lang.String r0 = r9.d     // Catch:{ Throwable -> 0x00ba, all -> 0x00b2 }
            boolean r0 = android.support.v4.b.a.a(r0)     // Catch:{ Throwable -> 0x00ba, all -> 0x00b2 }
            if (r0 == 0) goto L_0x006e
            java.lang.String r0 = r9.f2389a     // Catch:{ Throwable -> 0x00ba, all -> 0x00b2 }
            int r2 = r9.c     // Catch:{ Throwable -> 0x00ba, all -> 0x00b2 }
            com.immomo.momo.android.activity.fs r3 = r9.f     // Catch:{ Throwable -> 0x00ba, all -> 0x00b2 }
            com.immomo.momo.util.v r0 = com.immomo.momo.protocol.a.p.a(r0, r2, r3)     // Catch:{ Throwable -> 0x00ba, all -> 0x00b2 }
        L_0x0013:
            android.graphics.Bitmap r1 = r0.b     // Catch:{ Throwable -> 0x00ba, all -> 0x00b2 }
            if (r1 == 0) goto L_0x006a
            java.io.File r2 = r9.b     // Catch:{ Throwable -> 0x0097, all -> 0x00b2 }
            if (r2 == 0) goto L_0x006a
            java.lang.String r2 = "image/png"
            java.lang.String r0 = r0.f3099a     // Catch:{ Throwable -> 0x0097, all -> 0x00b2 }
            boolean r0 = r2.equals(r0)     // Catch:{ Throwable -> 0x0097, all -> 0x00b2 }
            if (r0 == 0) goto L_0x0077
            android.graphics.Bitmap$CompressFormat r0 = android.graphics.Bitmap.CompressFormat.PNG     // Catch:{ Throwable -> 0x0097, all -> 0x00b2 }
        L_0x0027:
            java.io.File r2 = r9.b     // Catch:{ Throwable -> 0x0097, all -> 0x00b2 }
            java.io.File r0 = com.immomo.momo.util.h.a(r1, r2, r0)     // Catch:{ Throwable -> 0x0097, all -> 0x00b2 }
            com.immomo.momo.service.bean.ak r2 = new com.immomo.momo.service.bean.ak     // Catch:{ Throwable -> 0x0097, all -> 0x00b2 }
            r2.<init>()     // Catch:{ Throwable -> 0x0097, all -> 0x00b2 }
            java.lang.String r3 = r9.f2389a     // Catch:{ Throwable -> 0x0097, all -> 0x00b2 }
            r2.f2990a = r3     // Catch:{ Throwable -> 0x0097, all -> 0x00b2 }
            java.lang.String r0 = r0.getAbsolutePath()     // Catch:{ Throwable -> 0x0097, all -> 0x00b2 }
            r2.b = r0     // Catch:{ Throwable -> 0x0097, all -> 0x00b2 }
            java.util.Date r0 = new java.util.Date     // Catch:{ Throwable -> 0x0097, all -> 0x00b2 }
            r0.<init>()     // Catch:{ Throwable -> 0x0097, all -> 0x00b2 }
            r2.e = r0     // Catch:{ Throwable -> 0x0097, all -> 0x00b2 }
            int r0 = r9.c     // Catch:{ Throwable -> 0x0097, all -> 0x00b2 }
            r2.d = r0     // Catch:{ Throwable -> 0x0097, all -> 0x00b2 }
            int r0 = r9.c     // Catch:{ Throwable -> 0x0097, all -> 0x00b2 }
            r3 = 3
            if (r0 != r3) goto L_0x007a
            java.lang.String r0 = r2.f2990a     // Catch:{ Throwable -> 0x0097, all -> 0x00b2 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0097, all -> 0x00b2 }
            java.lang.String r0 = java.lang.String.valueOf(r0)     // Catch:{ Throwable -> 0x0097, all -> 0x00b2 }
            r3.<init>(r0)     // Catch:{ Throwable -> 0x0097, all -> 0x00b2 }
            java.lang.String r0 = "_s"
            java.lang.StringBuilder r0 = r3.append(r0)     // Catch:{ Throwable -> 0x0097, all -> 0x00b2 }
            java.lang.String r0 = r0.toString()     // Catch:{ Throwable -> 0x0097, all -> 0x00b2 }
            r2.f2990a = r0     // Catch:{ Throwable -> 0x0097, all -> 0x00b2 }
        L_0x0063:
            com.immomo.momo.service.a.ad r0 = com.immomo.momo.service.a.ad.g()     // Catch:{ Throwable -> 0x0097, all -> 0x00b2 }
            r0.b(r2)     // Catch:{ Throwable -> 0x0097, all -> 0x00b2 }
        L_0x006a:
            r9.a(r1)
        L_0x006d:
            return
        L_0x006e:
            java.lang.String r0 = r9.d     // Catch:{ Throwable -> 0x00ba, all -> 0x00b2 }
            com.immomo.momo.android.activity.fs r2 = r9.f     // Catch:{ Throwable -> 0x00ba, all -> 0x00b2 }
            com.immomo.momo.util.v r0 = com.immomo.momo.protocol.a.p.a(r0, r2)     // Catch:{ Throwable -> 0x00ba, all -> 0x00b2 }
            goto L_0x0013
        L_0x0077:
            android.graphics.Bitmap$CompressFormat r0 = android.graphics.Bitmap.CompressFormat.JPEG     // Catch:{ Throwable -> 0x0097, all -> 0x00b2 }
            goto L_0x0027
        L_0x007a:
            int r0 = r9.c     // Catch:{ Throwable -> 0x0097, all -> 0x00b2 }
            r3 = 2
            if (r0 != r3) goto L_0x0063
            java.lang.String r0 = r2.f2990a     // Catch:{ Throwable -> 0x0097, all -> 0x00b2 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0097, all -> 0x00b2 }
            java.lang.String r0 = java.lang.String.valueOf(r0)     // Catch:{ Throwable -> 0x0097, all -> 0x00b2 }
            r3.<init>(r0)     // Catch:{ Throwable -> 0x0097, all -> 0x00b2 }
            java.lang.String r0 = "_l"
            java.lang.StringBuilder r0 = r3.append(r0)     // Catch:{ Throwable -> 0x0097, all -> 0x00b2 }
            java.lang.String r0 = r0.toString()     // Catch:{ Throwable -> 0x0097, all -> 0x00b2 }
            r2.f2990a = r0     // Catch:{ Throwable -> 0x0097, all -> 0x00b2 }
            goto L_0x0063
        L_0x0097:
            r0 = move-exception
            r8 = r1
        L_0x0099:
            com.immomo.momo.util.m r1 = r9.e     // Catch:{ all -> 0x00b7 }
            r1.a(r0)     // Catch:{ all -> 0x00b7 }
            com.immomo.momo.android.activity.fs r0 = r9.f     // Catch:{ all -> 0x00b7 }
            if (r0 == 0) goto L_0x00ae
            com.immomo.momo.android.activity.fs r0 = r9.f     // Catch:{ all -> 0x00b7 }
            r1 = -1
            r2 = -1
            r4 = -1
            r6 = -1
            r0.a(r1, r2, r4, r6)     // Catch:{ all -> 0x00b7 }
        L_0x00ae:
            r9.a(r8)
            goto L_0x006d
        L_0x00b2:
            r0 = move-exception
        L_0x00b3:
            r9.a(r1)
            throw r0
        L_0x00b7:
            r0 = move-exception
            r1 = r8
            goto L_0x00b3
        L_0x00ba:
            r0 = move-exception
            r8 = r1
            goto L_0x0099
        */
        throw new UnsupportedOperationException("Method not decompiled: com.immomo.momo.android.c.o.run():void");
    }
}
