package com.immomo.momo.android.activity;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Environment;
import com.immomo.momo.a;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.plugin.cropimage.m;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.util.ao;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

final class fl extends d {

    /* renamed from: a  reason: collision with root package name */
    private fn f1494a = null;
    private /* synthetic */ ImageBrowserActivity c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public fl(ImageBrowserActivity imageBrowserActivity, Context context, fn fnVar) {
        super(context);
        this.c = imageBrowserActivity;
        this.f1494a = fnVar;
    }

    /* access modifiers changed from: protected */
    public final Object a(Object... objArr) {
        FileOutputStream fileOutputStream;
        boolean z = true;
        if (!Environment.getExternalStorageState().equals("mounted")) {
            ao.a((CharSequence) "存储卡不可用, 图片保存失败");
            cancel(true);
        } else {
            Bitmap b = this.f1494a.b();
            File file = new File(a.f(), String.valueOf(this.f1494a.c) + ".jpg");
            if (file.exists()) {
                z = false;
            }
            try {
                fileOutputStream = new FileOutputStream(file);
                try {
                    b.compress(Bitmap.CompressFormat.JPEG, 85, fileOutputStream);
                    if (z) {
                        m.a(this.c.getApplicationContext(), file);
                    }
                    android.support.v4.b.a.a(fileOutputStream);
                    return true;
                } catch (FileNotFoundException e) {
                    e = e;
                    try {
                        this.b.a((Throwable) e);
                        android.support.v4.b.a.a(fileOutputStream);
                        return null;
                    } catch (Throwable th) {
                        th = th;
                        android.support.v4.b.a.a(fileOutputStream);
                        throw th;
                    }
                }
            } catch (FileNotFoundException e2) {
                e = e2;
                fileOutputStream = null;
                this.b.a((Throwable) e);
                android.support.v4.b.a.a(fileOutputStream);
                return null;
            } catch (Throwable th2) {
                th = th2;
                fileOutputStream = null;
                android.support.v4.b.a.a(fileOutputStream);
                throw th;
            }
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.c.a(new v(this.c, "正在保存图片..."));
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        a("图片保存失败");
    }

    /* access modifiers changed from: protected */
    public final void a(Object obj) {
        a("图片保存成功：immomo/camera");
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.c.p();
    }
}
