package com.immomo.momo.android.view.a;

import android.content.DialogInterface;

final class w implements DialogInterface.OnCancelListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ v f2707a;

    w(v vVar) {
        this.f2707a = vVar;
    }

    public final void onCancel(DialogInterface dialogInterface) {
        if (this.f2707a.f2706a != null) {
            this.f2707a.f2706a.cancel(true);
        }
    }
}
