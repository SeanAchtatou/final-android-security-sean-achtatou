package com.immomo.momo.android.activity.setting;

import android.widget.CompoundButton;

final class u implements CompoundButton.OnCheckedChangeListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ SettingMutetimeActivity f2144a;

    u(SettingMutetimeActivity settingMutetimeActivity) {
        this.f2144a = settingMutetimeActivity;
    }

    public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
        this.f2144a.p.h = z;
        this.f2144a.p.a("open_mutetime", Boolean.valueOf(this.f2144a.p.h));
        this.f2144a.q.setVisibility(z ? 0 : 8);
    }
}
