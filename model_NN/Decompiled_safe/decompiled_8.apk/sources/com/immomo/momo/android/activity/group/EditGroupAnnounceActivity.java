package com.immomo.momo.android.activity.group;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.ah;
import com.immomo.momo.android.view.HeaderLayout;
import com.immomo.momo.android.view.a.n;
import com.immomo.momo.android.view.bi;
import com.immomo.momo.service.bean.a.a;
import com.immomo.momo.service.y;

public class EditGroupAnnounceActivity extends ah implements View.OnClickListener {
    private HeaderLayout h = null;
    private bi i = null;
    private String j = PoiTypeDef.All;
    private e k = null;
    private TextView l;
    private String m;
    /* access modifiers changed from: private */
    public a n;
    /* access modifiers changed from: private */
    public y o;

    static /* synthetic */ void c(EditGroupAnnounceActivity editGroupAnnounceActivity) {
        boolean z;
        String charSequence = editGroupAnnounceActivity.l.getText().toString();
        if (charSequence.length() < 0) {
            editGroupAnnounceActivity.a((CharSequence) "公告描述至少10个字");
            editGroupAnnounceActivity.l.requestFocus();
            z = false;
        } else if (charSequence.length() > 500) {
            editGroupAnnounceActivity.a((CharSequence) "公告描述不能超过500个字");
            editGroupAnnounceActivity.l.requestFocus();
            z = false;
        } else {
            editGroupAnnounceActivity.n.f = editGroupAnnounceActivity.l.getText().toString();
            z = true;
        }
        if (z) {
            editGroupAnnounceActivity.k = new e(editGroupAnnounceActivity, editGroupAnnounceActivity);
            editGroupAnnounceActivity.k.execute(new Object[0]);
        }
    }

    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        super.a(bundle);
        setContentView((int) R.layout.activity_edit_groupannounce);
        this.o = new y();
        m().setTitleText("编辑群公告");
        this.h = (HeaderLayout) findViewById(R.id.layout_header);
        this.i = new bi(this);
        this.i.a("提交").a((int) R.drawable.ic_topbar_confirm);
        this.h.a(this.i, new a(this));
        this.l = (TextView) findViewById(R.id.tv_desc);
        if (bundle == null || !bundle.getBoolean("from_saveinstance", false)) {
            this.j = getIntent().getStringExtra("gid");
            this.n = this.o.e(this.j);
        } else {
            this.j = bundle.getString("gid");
            this.n = this.o.e(this.j);
        }
        this.e.b((Object) ("gid=" + this.j));
        this.e.b((Object) ("group null? " + (this.n == null)));
        if (this.n != null) {
            this.m = this.n.f;
            this.l.setText(android.support.v4.b.a.a(this.n.f) ? PoiTypeDef.All : this.n.f);
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
    }

    public void onClick(View view) {
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (this.k != null && !this.k.isCancelled()) {
            this.k.cancel(true);
        }
        super.onDestroy();
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 == 4) {
            if (this.n != null && !android.support.v4.b.a.a(this.n.f) && !this.n.f.equals(this.m)) {
                n nVar = new n(this);
                nVar.a();
                nVar.setTitle("退出编辑群公告");
                nVar.a("群公告已经修改，退出前要提交吗？");
                nVar.a(0, "提交", new b(this));
                nVar.a(1, "不提交", new c(this));
                nVar.a(2, "取消", new d());
                nVar.show();
                return true;
            }
        }
        return super.onKeyDown(i2, keyEvent);
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        bundle.putBoolean("from_saveinstance", true);
        bundle.putString("gid", this.j);
        super.onSaveInstanceState(bundle);
    }
}
