package com.immomo.momo.android.activity;

import android.view.View;
import com.immomo.momo.android.view.a.n;
import java.util.List;

final class en implements View.OnClickListener {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public /* synthetic */ FriendDistanceActivity f1338a;

    en(FriendDistanceActivity friendDistanceActivity) {
        this.f1338a = friendDistanceActivity;
    }

    public final void onClick(View view) {
        List d = this.f1338a.k.d();
        if (!d.isEmpty()) {
            n.a(this.f1338a.n(), "确认删除所选的内容吗？", new eo(this, d)).show();
        }
    }
}
