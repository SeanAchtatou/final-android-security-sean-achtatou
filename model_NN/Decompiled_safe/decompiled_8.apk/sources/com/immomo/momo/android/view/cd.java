package com.immomo.momo.android.view;

import android.content.Context;
import android.view.View;
import android.widget.AdapterView;
import android.widget.PopupWindow;
import com.immomo.momo.R;
import com.immomo.momo.util.m;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class cd implements AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener {

    /* renamed from: a  reason: collision with root package name */
    private HandyListView f2759a = null;
    private boolean b = false;
    private PopupWindow c = null;
    private PopupWindow d = null;
    private Context e = null;
    private PopupActionBar f = null;
    private HeaderLayout g = null;
    private int h = R.string.multiple_choiced_count;
    private int i = -1;
    private int j = R.color.background_normal_selected;
    private int k = R.drawable.bglistitem_multipleselecting;
    private int l = R.drawable.bglistitem_selector_user;
    private int m = 0;
    private List n = new ArrayList();
    private Map o = new HashMap();
    private AdapterView.OnItemClickListener p = null;
    private cg q = null;
    private cf r = null;
    private m s = new m(this);
    private ce t = null;

    public cd(Context context, HandyListView handyListView) {
        this.f2759a = handyListView;
        this.e = context;
        this.f = new PopupActionBar(this.e);
        this.g = new HeaderLayout(context);
        this.g.a(new bi(context).b(R.string.quit).a((int) R.drawable.ic_topbar_cancel), new ch(this, (byte) 0));
    }

    private boolean a(int i2, boolean z) {
        if (i2 < 0) {
            return false;
        }
        Object item = this.t.getItem(i2);
        if (this.r != null && !this.r.a(item)) {
            return false;
        }
        this.o.put(item, Boolean.valueOf(z));
        if (z) {
            this.n.add(item);
        } else {
            this.n.remove(item);
        }
        return true;
    }

    private boolean a(Object obj) {
        if (this.o.get(obj) != null) {
            return ((Boolean) this.o.get(obj)).booleanValue();
        }
        return false;
    }

    public final void a() {
        this.j = R.drawable.bg_common_card_full_press;
        this.l = R.drawable.bg_card_full;
        this.k = R.drawable.bg_card_full;
    }

    public final void a(int i2) {
        this.i = i2;
    }

    /* access modifiers changed from: package-private */
    public final void a(View view, int i2) {
        if (view != null && i2 >= 0 && i2 < this.t.getCount()) {
            int paddingLeft = view.getPaddingLeft();
            int paddingTop = view.getPaddingTop();
            int paddingRight = view.getPaddingRight();
            int paddingBottom = view.getPaddingBottom();
            if (a(this.t.getItem(i2))) {
                if (this.j != -1) {
                    view.setBackgroundResource(this.j);
                }
            } else if (this.b) {
                if (this.k != -1) {
                    view.setBackgroundResource(this.k);
                }
            } else if (this.l != -1) {
                view.setBackgroundResource(this.l);
            }
            view.setPadding(paddingLeft, paddingTop, paddingRight, paddingBottom);
        }
    }

    public final void a(a aVar, View.OnClickListener onClickListener) {
        this.f.a(aVar, onClickListener);
    }

    public final void a(ce ceVar) {
        e();
        if (ceVar != null) {
            ceVar.a(this);
        }
        this.t = ceVar;
    }

    public final void a(cf cfVar) {
        this.r = cfVar;
    }

    public final void a(cg cgVar) {
        this.q = cgVar;
    }

    public final int b() {
        return this.i;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.widget.PopupWindow.<init>(android.view.View, int, int, boolean):void}
     arg types: [com.immomo.momo.android.view.PopupActionBar, int, int, int]
     candidates:
      ClspMth{android.widget.PopupWindow.<init>(android.content.Context, android.util.AttributeSet, int, int):void}
      ClspMth{android.widget.PopupWindow.<init>(android.view.View, int, int, boolean):void} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.widget.PopupWindow.<init>(android.view.View, int, int, boolean):void}
     arg types: [com.immomo.momo.android.view.HeaderLayout, int, int, int]
     candidates:
      ClspMth{android.widget.PopupWindow.<init>(android.content.Context, android.util.AttributeSet, int, int):void}
      ClspMth{android.widget.PopupWindow.<init>(android.view.View, int, int, boolean):void} */
    /* access modifiers changed from: package-private */
    public final void c() {
        this.f2759a.setOnItemLongClickListener(this);
        this.c = new PopupWindow((View) this.f, -1, -2, false);
        this.d = new PopupWindow((View) this.g, -1, -2, false);
    }

    public final List d() {
        return this.n;
    }

    public final void e() {
        int headerViewsCount;
        if (this.b) {
            this.b = false;
            this.o.clear();
            this.n.clear();
            this.m = 0;
            for (int firstVisiblePosition = this.f2759a.getFirstVisiblePosition(); firstVisiblePosition <= this.f2759a.getLastVisiblePosition(); firstVisiblePosition++) {
                View childAt = this.f2759a.getChildAt(firstVisiblePosition - this.f2759a.getFirstVisiblePosition());
                if (childAt != null && (headerViewsCount = firstVisiblePosition - this.f2759a.getHeaderViewsCount()) >= 0) {
                    if (this.i != -1) {
                        a(childAt.findViewById(this.i), headerViewsCount);
                    } else {
                        a(childAt, headerViewsCount);
                    }
                }
            }
            this.f2759a.setOnItemClickListener(this.p);
            this.f2759a.setLongClickable(true);
            this.c.dismiss();
            this.d.dismiss();
            if (this.q != null) {
                this.q.a();
            }
        }
    }

    public final void f() {
        e();
        this.f2759a.setOnItemLongClickListener(null);
        this.f2759a.setLongClickable(false);
    }

    public final boolean g() {
        return this.b;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i2, long j2) {
        int i3;
        if (this.b) {
            boolean a2 = a(this.t.getItem(i2));
            if (a(i2, !a2)) {
                if (a2) {
                    i3 = this.m - 1;
                    this.m = i3;
                } else {
                    i3 = this.m + 1;
                    this.m = i3;
                }
                if (this.m == 0) {
                    e();
                    return;
                }
                a(view, i2);
                this.g.setTitleText(this.e.getString(this.h, Integer.valueOf(i3)));
                return;
            }
            return;
        }
        this.s.b((Object) "ERROR. request on !selecting");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.android.view.cd.a(int, boolean):boolean
     arg types: [int, int]
     candidates:
      com.immomo.momo.android.view.cd.a(android.view.View, int):void
      com.immomo.momo.android.view.cd.a(com.immomo.momo.android.view.a, android.view.View$OnClickListener):void
      com.immomo.momo.android.view.cd.a(int, boolean):boolean */
    public final boolean onItemLongClick(AdapterView adapterView, View view, int i2, long j2) {
        if (this.b || !a(i2, true)) {
            return false;
        }
        this.b = true;
        this.c.showAtLocation(this.f2759a, 80, 0, 0);
        this.d.showAtLocation(this.f2759a, 48, 0, 0);
        this.f2759a.setLongClickable(false);
        this.m++;
        this.g.setTitleText(this.e.getString(this.h, Integer.valueOf(this.m)));
        this.t.notifyDataSetChanged();
        this.p = this.f2759a.getOnItemClickListener();
        this.f2759a.setOnItemClickListener(this);
        if (this.q != null) {
            this.q.b();
        }
        this.s.a((Object) ("onItemLongClick, position=" + i2));
        return true;
    }
}
