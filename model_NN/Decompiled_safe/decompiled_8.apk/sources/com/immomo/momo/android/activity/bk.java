package com.immomo.momo.android.activity;

import android.content.DialogInterface;
import android.content.Intent;

final class bk implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ CheckStatusActivity f1046a;

    bk(CheckStatusActivity checkStatusActivity) {
        this.f1046a = checkStatusActivity;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f1046a.startActivity(new Intent("android.settings.WIRELESS_SETTINGS"));
        dialogInterface.dismiss();
    }
}
