package com.immomo.momo.android.activity.feed;

import android.content.Context;
import com.immomo.momo.android.c.b;
import com.immomo.momo.protocol.a.k;
import com.immomo.momo.service.bean.ab;
import com.immomo.momo.service.bean.ae;

final class w extends b {
    private ae c;
    private /* synthetic */ FeedProfileActivity d;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public w(FeedProfileActivity feedProfileActivity, Context context, ae aeVar) {
        super(context);
        this.d = feedProfileActivity;
        this.c = aeVar;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        return k.a().d(this.c.k);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        String str = (String) obj;
        a(str);
        this.d.l.c(this.c);
        FeedProfileActivity feedProfileActivity = this.d;
        ab h = this.d.k;
        int i = h.g - 1;
        h.g = i;
        FeedProfileActivity.a(feedProfileActivity, i);
        this.d.m.e(this.c.k);
        super.a((Object) str);
    }
}
