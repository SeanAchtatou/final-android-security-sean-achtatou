package com.immomo.momo.android.activity;

import android.content.Context;
import android.graphics.Canvas;
import com.amap.mapapi.map.MapView;
import com.amap.mapapi.map.MyLocationOverlay;

final class mj extends MyLocationOverlay {
    public mj(Context context, MapView mapView) {
        super(context, mapView);
    }

    public final boolean draw(Canvas canvas, MapView mapView, boolean z, long j) {
        super.draw(canvas, mapView, z, j);
        return false;
    }
}
