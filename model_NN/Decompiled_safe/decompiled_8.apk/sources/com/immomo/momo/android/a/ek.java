package com.immomo.momo.android.a;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v4.b.a;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.ImageBrowserActivity;
import com.immomo.momo.android.activity.OtherProfileActivity;
import com.immomo.momo.android.activity.ao;
import com.immomo.momo.android.activity.emotestore.EmotionProfileActivity;
import com.immomo.momo.android.activity.feed.FeedProfileActivity;
import com.immomo.momo.android.activity.group.GroupFeedProfileActivity;
import com.immomo.momo.android.activity.group.GroupPartyActivity;
import com.immomo.momo.android.c.u;
import com.immomo.momo.android.view.AltImageView;
import com.immomo.momo.android.view.EmoteTextView;
import com.immomo.momo.android.view.HandyListView;
import com.immomo.momo.android.view.MultiImageView;
import com.immomo.momo.android.view.a.at;
import com.immomo.momo.android.view.cb;
import com.immomo.momo.b;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.a.e;
import com.immomo.momo.service.bean.aj;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.service.bean.q;
import com.immomo.momo.service.v;
import com.immomo.momo.util.j;
import com.immomo.momo.util.m;
import java.io.File;
import java.lang.ref.SoftReference;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class ek extends a implements View.OnClickListener, cb {
    private static Map i = new HashMap(24);
    /* access modifiers changed from: private */
    public v d = null;
    /* access modifiers changed from: private */
    public ao e = null;
    private HandyListView f = null;
    private int g = 0;
    private String h;

    public ek(ao aoVar, List list, HandyListView handyListView) {
        super(aoVar, list);
        new m(this);
        this.h = g.q().h;
        this.e = aoVar;
        this.g = g.n();
        this.d = new v();
        this.f = handyListView;
    }

    private void a(e eVar, ImageView imageView) {
        SoftReference softReference = (SoftReference) i.get(eVar.i);
        Bitmap bitmap = softReference == null ? null : (Bitmap) softReference.get();
        if (bitmap != null && bitmap.isRecycled()) {
            i.remove(bitmap);
            bitmap = null;
        }
        if (bitmap != null) {
            bitmap = (Bitmap) ((SoftReference) i.get(eVar.i)).get();
        } else if (!this.f.g()) {
            File a2 = q.a(eVar.i, eVar.j);
            if (a2.exists() && (bitmap = a.l(a2.getPath())) != null) {
                i.put(eVar.i, new SoftReference(bitmap));
            }
            if (bitmap == null && !eVar.isImageLoading()) {
                eVar.setImageLoading(true);
                u.b().execute(new eo(this, eVar));
            }
        }
        if (bitmap != null) {
            imageView.setImageBitmap(bitmap);
        } else {
            imageView.setImageDrawable(null);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public final void b(int i2, String[] strArr) {
        Intent intent = new Intent(this.e, ImageBrowserActivity.class);
        intent.putExtra("array", strArr);
        intent.putExtra("imagetype", "feed");
        intent.putExtra("autohide_header", true);
        intent.putExtra("index", i2);
        this.e.startActivity(intent);
        if (this.e.getParent() != null) {
            this.e.getParent().overridePendingTransition(R.anim.zoom_enter, R.anim.normal);
        } else {
            this.e.overridePendingTransition(R.anim.zoom_enter, R.anim.normal);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.b.a(java.lang.String, boolean):android.graphics.Bitmap
     arg types: [java.lang.String, int]
     candidates:
      com.immomo.momo.b.a(java.lang.String, android.graphics.Bitmap):android.graphics.Bitmap
      com.immomo.momo.b.a(java.lang.String, boolean):android.graphics.Bitmap */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, com.immomo.momo.android.view.HandyListView):void
     arg types: [com.immomo.momo.service.bean.bf, android.widget.ImageView, com.immomo.momo.android.view.HandyListView]
     candidates:
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, int):void
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup):void
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, com.immomo.momo.android.view.HandyListView):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup, int, boolean, boolean, int):void
     arg types: [com.immomo.momo.service.bean.a.e, com.immomo.momo.android.view.AltImageView, com.immomo.momo.android.view.HandyListView, int, int, int, int]
     candidates:
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, int, android.view.View, android.view.ViewGroup, boolean, int, boolean):com.immomo.momo.android.c.o
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup, int, boolean, boolean, int):void */
    public final View getView(int i2, View view, ViewGroup viewGroup) {
        er erVar;
        if (view == null) {
            view = g.o().inflate((int) R.layout.listitem_groupfeed, (ViewGroup) null);
            er erVar2 = new er((byte) 0);
            view.setTag(R.id.tag_userlist_item, erVar2);
            erVar2.f804a = (TextView) view.findViewById(R.id.tv_feed_time);
            erVar2.b = (TextView) view.findViewById(R.id.tv_feed_site);
            erVar2.c = view.findViewById(R.id.layout_feed_site);
            erVar2.d = (EmoteTextView) view.findViewById(R.id.tv_feed_content);
            erVar2.e = (AltImageView) view.findViewById(R.id.iv_feed_content);
            erVar2.f = (MultiImageView) view.findViewById(R.id.mv_feed_content);
            erVar2.g = (ImageView) view.findViewById(R.id.iv_feed_photo);
            erVar2.h = (TextView) view.findViewById(R.id.tv_feed_name);
            erVar2.j = (TextView) view.findViewById(R.id.tv_feed_commentcount);
            erVar2.k = (ImageView) view.findViewById(R.id.iv_feed_commentcountic);
            erVar2.l = view.findViewById(R.id.layout_feed_commentcount);
            erVar2.m = view.findViewById(R.id.bt_feed_more);
            erVar2.i = view.findViewById(R.id.layout_feed_content);
            erVar2.n = view.findViewById(R.id.groupfeed_iv_top);
            erVar2.o = view.findViewById(R.id.groupfeed_iv_party);
            erVar2.i = view.findViewById(R.id.layout_feed_content);
            erVar2.z = view.findViewById(R.id.userlist_item_layout_badgeContainer);
            erVar2.p = (ImageView) view.findViewById(R.id.userlist_item_iv_gender);
            erVar2.x = view.findViewById(R.id.userlist_item_layout_genderbackgroud);
            erVar2.q = (ImageView) view.findViewById(R.id.userlist_item_pic_iv_weibo);
            erVar2.r = (ImageView) view.findViewById(R.id.userlist_item_pic_iv_tweibo);
            erVar2.w = (ImageView) view.findViewById(R.id.userlist_item_pic_iv_group_role);
            erVar2.s = (ImageView) view.findViewById(R.id.userlist_item_pic_iv_renren);
            erVar2.t = (ImageView) view.findViewById(R.id.userlist_item_pic_iv_vip);
            erVar2.u = (ImageView) view.findViewById(R.id.userlist_item_pic_iv_multipic);
            erVar2.v = (ImageView) view.findViewById(R.id.userlist_item_pic_iv_industry);
            erVar2.y = (TextView) view.findViewById(R.id.userlist_item_tv_age);
            erVar2.g.setOnClickListener(this);
            erVar2.i.setOnClickListener(this);
            erVar2.e.setOnClickListener(this);
            erVar = erVar2;
        } else {
            erVar = (er) view.getTag(R.id.tag_userlist_item);
        }
        e eVar = (e) getItem(i2);
        erVar.g.setTag(R.id.tag_item_position, Integer.valueOf(i2));
        erVar.e.setTag(R.id.tag_item_position, Integer.valueOf(i2));
        erVar.i.setTag(R.id.tag_item_position, Integer.valueOf(i2));
        erVar.l.setTag(R.id.tag_item_position, Integer.valueOf(i2));
        erVar.m.setTag(R.id.tag_item_position, Integer.valueOf(i2));
        erVar.b.setTag(R.id.tag_item_position, Integer.valueOf(i2));
        erVar.c.setTag(R.id.tag_item_position, Integer.valueOf(i2));
        if (eVar.b != null) {
            erVar.z.setVisibility(0);
            erVar.y.setText(new StringBuilder(String.valueOf(eVar.b.I)).toString());
            if ("F".equals(eVar.b.H)) {
                erVar.x.setBackgroundResource(R.drawable.bg_gender_famal);
                erVar.p.setImageResource(R.drawable.ic_user_famale);
            } else {
                erVar.x.setBackgroundResource(R.drawable.bg_gender_male);
                erVar.p.setImageResource(R.drawable.ic_user_male);
            }
            if (eVar.b.j()) {
                erVar.u.setVisibility(0);
            } else {
                erVar.u.setVisibility(8);
            }
            if (!com.immomo.a.a.f.a.a(eVar.b.M)) {
                erVar.v.setVisibility(0);
                erVar.v.setImageBitmap(b.a(eVar.b.M, true));
            } else {
                erVar.v.setVisibility(8);
            }
            if (eVar.b.an) {
                erVar.q.setVisibility(0);
                erVar.q.setImageResource(eVar.b.ap ? R.drawable.ic_userinfo_weibov : R.drawable.ic_userinfo_weibo);
            } else {
                erVar.q.setVisibility(8);
            }
            if (eVar.b.at) {
                erVar.r.setVisibility(0);
                erVar.r.setImageResource(eVar.b.au ? R.drawable.ic_userinfo_tweibov : R.drawable.ic_userinfo_tweibo);
            } else {
                erVar.r.setVisibility(8);
            }
            if (eVar.b.aJ == 1 || eVar.b.aJ == 3) {
                erVar.w.setVisibility(0);
                erVar.w.setImageResource(eVar.b.aJ == 1 ? R.drawable.ic_userinfo_groupowner : R.drawable.ic_userinfo_group);
            } else {
                erVar.w.setVisibility(8);
            }
            if (eVar.b.ar) {
                erVar.s.setVisibility(0);
            } else {
                erVar.s.setVisibility(8);
            }
            if (eVar.b.b()) {
                erVar.t.setVisibility(0);
                if (eVar.b.c()) {
                    erVar.t.setImageResource(R.drawable.ic_userinfo_vip_year);
                } else {
                    erVar.t.setImageResource(R.drawable.ic_userinfo_vip);
                }
            } else {
                erVar.t.setVisibility(8);
            }
        } else {
            erVar.z.setVisibility(8);
        }
        TextView textView = erVar.h;
        bf bfVar = eVar.b == null ? new bf(eVar.c) : eVar.b;
        String h2 = bfVar.h();
        String str = eVar.o == 0 ? "(已退出)" : PoiTypeDef.All;
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(String.valueOf(h2) + str);
        if (bfVar.b()) {
            spannableStringBuilder.setSpan(new ForegroundColorSpan(g.c((int) R.color.font_vip_name)), 0, h2.length(), 33);
        } else {
            spannableStringBuilder.setSpan(new ForegroundColorSpan(g.c((int) R.color.font_value)), 0, h2.length(), 33);
        }
        if (eVar.o == 0) {
            spannableStringBuilder.setSpan(new ForegroundColorSpan(g.c((int) R.color.font_light)), h2.length(), str.length() + h2.length(), 33);
        }
        textView.setText(spannableStringBuilder);
        j.a((aj) eVar.b, erVar.g, this.f);
        erVar.b.setText(eVar.l);
        if (a.f(eVar.a())) {
            erVar.d.setText(eVar.c());
            erVar.d.setVisibility(0);
        } else {
            erVar.d.setVisibility(8);
        }
        erVar.f804a.setText(eVar.d);
        erVar.j.setText(new StringBuilder().append(eVar.e).toString());
        if (eVar.n) {
            erVar.n.setVisibility(0);
        } else {
            erVar.n.setVisibility(8);
        }
        erVar.f.setVisibility(8);
        erVar.e.setVisibility(8);
        if (eVar.k != null) {
            ViewGroup.LayoutParams layoutParams = erVar.e.getLayoutParams();
            layoutParams.height = this.g;
            layoutParams.width = (int) ((((float) this.g) / ((float) eVar.k.k())) * ((float) eVar.k.j()));
            erVar.e.setLayoutParams(layoutParams);
            erVar.e.setAlt(eVar.i);
            a(eVar, erVar.e);
            erVar.e.setAlt(eVar.i);
            erVar.e.setVisibility(0);
        } else if (a.f(eVar.i) && a.f(eVar.j)) {
            erVar.e.setVisibility(0);
            ViewGroup.LayoutParams layoutParams2 = erVar.e.getLayoutParams();
            layoutParams2.height = this.g;
            layoutParams2.width = this.g;
            erVar.e.setLayoutParams(layoutParams2);
            erVar.e.setAlt(eVar.i);
            a(eVar, erVar.e);
            erVar.e.setAlt(eVar.i);
        } else if (eVar.f() > 1) {
            erVar.f.setVisibility(0);
            erVar.f.setImage(eVar.g());
            erVar.f.setOnclickHandler(this);
        } else if (a.f(eVar.getLoadImageId())) {
            ViewGroup.LayoutParams layoutParams3 = erVar.e.getLayoutParams();
            layoutParams3.height = this.g;
            layoutParams3.width = this.g;
            erVar.e.setLayoutParams(layoutParams3);
            erVar.e.setAlt(PoiTypeDef.All);
            erVar.e.setVisibility(0);
            j.a((aj) eVar, (ImageView) erVar.e, (ViewGroup) this.f, 15, false, false, 0);
        }
        if (eVar.m == 1) {
            erVar.o.setVisibility(0);
            erVar.k.setImageResource(R.drawable.ic_user_person);
        } else {
            erVar.o.setVisibility(8);
            erVar.k.setImageResource(R.drawable.ic_feed_comment);
        }
        return view;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.android.activity.feed.FeedProfileActivity.a(android.content.Context, java.lang.String, boolean):void
     arg types: [com.immomo.momo.android.activity.ao, java.lang.String, int]
     candidates:
      com.immomo.momo.android.activity.feed.FeedProfileActivity.a(com.immomo.momo.android.activity.feed.FeedProfileActivity, java.lang.String, boolean):void
      com.immomo.momo.android.activity.ao.a(android.support.v4.app.Fragment, android.content.Intent, int):void
      android.support.v4.app.g.a(java.lang.String, java.io.PrintWriter, android.view.View):void
      android.support.v4.app.g.a(android.support.v4.app.Fragment, android.content.Intent, int):void
      com.immomo.momo.android.activity.feed.FeedProfileActivity.a(android.content.Context, java.lang.String, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public final void onClick(View view) {
        int intValue = ((Integer) view.getTag(R.id.tag_item_position)).intValue();
        switch (view.getId()) {
            case R.id.layout_feed_content /*2131166245*/:
                if (((e) getItem(intValue)).m == 1) {
                    GroupPartyActivity.a(this.e, ((e) getItem(intValue)).g);
                    return;
                } else {
                    GroupFeedProfileActivity.a(this.e, ((e) getItem(intValue)).f);
                    return;
                }
            case R.id.iv_feed_photo /*2131166248*/:
                Intent intent = new Intent(this.e, OtherProfileActivity.class);
                intent.putExtra("momoid", ((e) getItem(intValue)).c);
                this.e.startActivity(intent);
                return;
            case R.id.iv_feed_content /*2131166257*/:
                String str = ((e) getItem(intValue)).j;
                if (a.f(str)) {
                    Intent intent2 = new Intent(this.e, EmotionProfileActivity.class);
                    intent2.putExtra("eid", str);
                    this.e.startActivity(intent2);
                    return;
                }
                Intent intent3 = new Intent(this.e, ImageBrowserActivity.class);
                intent3.putExtra("array", new String[]{((e) getItem(intValue)).getLoadImageId()});
                intent3.putExtra("imagetype", "feed");
                intent3.putExtra("autohide_header", true);
                this.e.startActivity(intent3);
                if (this.e.getParent() != null) {
                    this.e.getParent().overridePendingTransition(R.anim.zoom_enter, R.anim.normal);
                    return;
                } else {
                    this.e.overridePendingTransition(R.anim.zoom_enter, R.anim.normal);
                    return;
                }
            case R.id.layout_feed_commentcount /*2131166270*/:
                FeedProfileActivity.a((Context) this.e, ((e) getItem(intValue)).f, true);
                return;
            case R.id.bt_feed_more /*2131166665*/:
                e eVar = (e) getItem(intValue);
                String[] strArr = a.f(eVar.a()) ? this.h.equals(eVar.c) ? new String[]{"复制文本", "删除"} : new String[]{"复制文本", "举报"} : this.h.equals(eVar.c) ? new String[]{"删除"} : new String[]{"举报"};
                at atVar = new at(this.e, view, strArr);
                atVar.a(new el(this, strArr, eVar));
                atVar.d();
                return;
            default:
                return;
        }
    }
}
