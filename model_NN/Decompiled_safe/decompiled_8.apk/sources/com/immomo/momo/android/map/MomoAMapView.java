package com.immomo.momo.android.map;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import com.amap.mapapi.map.MapView;
import com.immomo.momo.util.m;

public class MomoAMapView extends MapView {
    private aa k = null;
    private boolean l = false;

    public MomoAMapView(Context context) {
        super(context);
        new m(getClass().getSimpleName());
    }

    public MomoAMapView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        new m(getClass().getSimpleName());
    }

    public MomoAMapView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        new m(getClass().getSimpleName());
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case 1:
                if (this.l && this.k != null) {
                    this.k.a(1);
                }
                this.l = false;
                break;
            case 2:
                if (!this.l && this.k != null) {
                    this.k.a(2);
                }
                this.l = true;
                break;
        }
        return super.onTouchEvent(motionEvent);
    }

    public void setGetAddressListener(aa aaVar) {
        this.k = aaVar;
    }
}
