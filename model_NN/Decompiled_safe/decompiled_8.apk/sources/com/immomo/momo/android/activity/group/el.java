package com.immomo.momo.android.activity.group;

import com.immomo.momo.android.view.cx;

final class el implements cx {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ PublishGroupFeedActivity f1633a;

    el(PublishGroupFeedActivity publishGroupFeedActivity) {
        this.f1633a = publishGroupFeedActivity;
    }

    public final void a(int i, int i2) {
        if (i < i2) {
            this.f1633a.e.a((Object) ("OnResize--------------h < oldh=" + i + "<" + i2));
            if (((double) i) <= ((double) this.f1633a.p) * 0.8d) {
                this.f1633a.h = true;
            }
        } else if (((double) i2) <= ((double) this.f1633a.p) * 0.8d && this.f1633a.h && !this.f1633a.q.isShown()) {
            this.f1633a.h = false;
        }
    }
}
