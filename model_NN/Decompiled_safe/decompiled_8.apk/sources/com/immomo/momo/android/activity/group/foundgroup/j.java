package com.immomo.momo.android.activity.group.foundgroup;

import android.support.v4.b.a;
import android.view.View;
import com.immomo.momo.R;
import com.immomo.momo.android.view.EmoteEditeText;
import com.immomo.momo.util.aq;
import com.immomo.momo.util.k;
import mm.purchasesdk.PurchaseCode;

public final class j extends a implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private FoundGroupActivity f1669a;
    private EmoteEditeText b = ((EmoteEditeText) a(R.id.et_group_introduction));
    private s c;

    public j(View view, FoundGroupActivity foundGroupActivity, s sVar) {
        super(view);
        this.f1669a = foundGroupActivity;
        this.c = sVar;
        if (!a.a((CharSequence) this.c.e)) {
            this.b.setText(this.c.e);
        }
        EmoteEditeText emoteEditeText = this.b;
        EmoteEditeText emoteEditeText2 = this.b;
        emoteEditeText.addTextChangedListener(new aq(PurchaseCode.QUERY_NO_APP));
        EmoteEditeText emoteEditeText3 = this.b;
        aq aqVar = new aq(20);
        new k();
        aqVar.a();
    }

    public final void b() {
    }

    /* access modifiers changed from: protected */
    public final boolean c() {
        String trim = this.b.getText().toString().trim();
        if (trim.length() == 0) {
            this.f1669a.a((int) R.string.str_group_introduction_tip);
            return false;
        } else if (trim.length() < 15) {
            this.f1669a.a((int) R.string.str_edit_groupintroduction);
            return false;
        } else {
            this.c.e = trim;
            return true;
        }
    }

    /* access modifiers changed from: protected */
    public final void d() {
        new k("PI", "P644").e();
    }

    /* access modifiers changed from: protected */
    public final void e() {
        new k("PO", "P644").e();
    }

    public final void onClick(View view) {
        view.getId();
    }
}
