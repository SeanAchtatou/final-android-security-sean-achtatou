package com.immomo.momo.android.activity;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.w;
import com.immomo.momo.service.bean.at;

final class jn extends d {

    /* renamed from: a  reason: collision with root package name */
    private v f1769a;
    private Boolean c = false;
    private /* synthetic */ PhoneBoxActivity d;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public jn(PhoneBoxActivity phoneBoxActivity, Context context) {
        super(context);
        this.d = phoneBoxActivity;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        return Boolean.valueOf(w.a().a(this.c.booleanValue()));
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.f1769a = new v(this.d);
        this.f1769a.a("请求提交中");
        this.f1769a.setCancelable(true);
        this.f1769a.setOnCancelListener(new jo(this));
        this.d.a(this.f1769a);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        at atVar = this.d.g;
        at atVar2 = this.d.g;
        boolean booleanValue = ((Boolean) obj).booleanValue();
        atVar2.c = booleanValue;
        atVar.a("phonebook_syn", Boolean.valueOf(booleanValue));
        this.d.finish();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.d.p();
    }
}
