package com.immomo.momo.android.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.b.a;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.plugin.cropimage.q;
import com.immomo.momo.android.view.a.al;
import com.immomo.momo.h;

public class HeaderLayout extends FrameLayout {

    /* renamed from: a  reason: collision with root package name */
    private View f2632a = null;
    private LinearLayout b = null;
    private LinearLayout c = null;
    private LinearLayout d = null;
    private LinearLayout e = null;
    private ImageView f = null;
    private ImageView g = null;
    private ImageView h = null;
    private TextView i = null;
    private TextView j = null;
    private HeaderSpinner k = null;
    private View l = null;
    private CharSequence m = PoiTypeDef.All;

    public HeaderLayout(Context context) {
        super(context);
        b();
    }

    public HeaderLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        b();
        String string = context.obtainStyledAttributes(attributeSet, h.HeaderBar).getString(0);
        if (!a.a((CharSequence) string)) {
            setTitleText(string);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, com.immomo.momo.android.view.HeaderLayout, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    private void b() {
        ((LayoutInflater) getContext().getSystemService("layout_inflater")).inflate(getLayout(), (ViewGroup) this, true);
        this.e = (LinearLayout) findViewById(R.id.header_layout_holder);
        this.f2632a = findViewById(R.id.header_layout_icon_container);
        this.b = (LinearLayout) findViewById(R.id.header_layout_button_container);
        this.c = (LinearLayout) findViewById(R.id.header_layout_rightview_container);
        this.d = (LinearLayout) findViewById(R.id.header_layout_leftview_container);
        findViewById(R.id.header_layout_middle);
        this.f = (ImageView) findViewById(R.id.header_iv_avatar);
        this.g = (ImageView) findViewById(R.id.header_iv_logo);
        this.i = (TextView) findViewById(R.id.header_stv_title);
        this.j = (TextView) findViewById(R.id.header_tv_subtitle);
        this.h = (ImageView) findViewById(R.id.header_iv_icon);
        this.k = (HeaderSpinner) findViewById(R.id.header_spinner);
        this.l = findViewById(R.id.layout_searchlayout);
    }

    public final void a() {
        this.b.removeAllViews();
        this.b.setVisibility(8);
        this.c.removeAllViews();
        this.d.removeAllViews();
    }

    public final void a(View view) {
        if (view != null) {
            this.c.addView(view);
            this.c.setVisibility(0);
        }
    }

    public final void a(bi biVar, View.OnClickListener onClickListener) {
        if (biVar != null && biVar != null) {
            if (onClickListener != null) {
                biVar.setOnClickListener(onClickListener);
            }
            this.b.setVisibility(0);
            this.b.addView(biVar, 0);
        }
    }

    public final void b(View view) {
        if (view != null) {
            this.d.addView(view);
            this.d.setVisibility(0);
        }
    }

    public HeaderSpinner getHeaderSpinner() {
        return this.k;
    }

    /* access modifiers changed from: protected */
    public int getLayout() {
        return R.layout.common_headerbar;
    }

    public ImageView getLeftLogoView() {
        return (ImageView) findViewById(R.id.header_iv_logo);
    }

    public LinearLayout getRootLayout() {
        return this.e;
    }

    public View getSearchLayout() {
        return this.l;
    }

    public int getSpinnerSelectedIndex() {
        return this.k.getSelectedIndex();
    }

    public TextView getTitileView() {
        return this.i;
    }

    public CharSequence getTitleText() {
        return this.m;
    }

    public void setAvatar(int i2) {
        if (i2 <= 0) {
            this.f.setVisibility(8);
            return;
        }
        this.f.setImageResource(i2);
        this.f.setVisibility(0);
    }

    public void setLeftIcon(int i2) {
        if (i2 <= 0) {
            this.f2632a.setVisibility(8);
            return;
        }
        this.f2632a.setVisibility(0);
        this.h.setImageResource(i2);
    }

    public void setLeftIconViewOnClickListener(View.OnClickListener onClickListener) {
        this.h.setOnClickListener(onClickListener);
    }

    public void setLogoView(int i2) {
        this.g.setImageResource(i2);
    }

    public void setLogoView(Bitmap bitmap) {
        this.g.setImageBitmap(bitmap);
    }

    public void setLogoViewOnClickListener(View.OnClickListener onClickListener) {
        this.g.setOnClickListener(onClickListener);
    }

    public void setLogoVisibility(boolean z) {
        if (z) {
            this.g.setVisibility(0);
        } else {
            this.g.setVisibility(8);
        }
    }

    public void setSpinnerDropdownListener$28bc66b(q qVar) {
        this.k.setOnDropdownListener$28bc66b(qVar);
    }

    public void setSpinnerSelectedListener(al alVar) {
        this.k.setOnItemClickListener(alVar);
    }

    public void setSpinnerText(String str) {
        this.k.setText(str);
    }

    public void setSubTitleText(CharSequence charSequence) {
        this.j.setText(charSequence);
        if (charSequence.toString().trim().length() > 0) {
            this.j.setVisibility(0);
        } else {
            this.j.setVisibility(8);
        }
    }

    public void setTitleText(int i2) {
        setTitleText(getResources().getString(i2));
    }

    public void setTitleText(CharSequence charSequence) {
        if (charSequence == null) {
            charSequence = PoiTypeDef.All;
        }
        this.m = charSequence;
        this.i.setText(this.m);
        if (charSequence.toString().trim().length() > 0) {
            this.i.setVisibility(0);
        } else {
            this.i.setVisibility(4);
        }
    }

    public void setTitleViewOnClickListener(View.OnClickListener onClickListener) {
        this.i.setOnClickListener(onClickListener);
    }
}
