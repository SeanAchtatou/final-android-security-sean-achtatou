package com.immomo.momo.android.activity.plugin;

import android.content.Context;
import com.immomo.momo.R;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.w;

final class bt extends d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ SubmitVerifyCodeActivity f2074a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public bt(SubmitVerifyCodeActivity submitVerifyCodeActivity, Context context) {
        super(context);
        this.f2074a = submitVerifyCodeActivity;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        w.a().b(this.f2074a.k, this.f2074a.l, this.f2074a.m, this.f2074a.n);
        return true;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.f2074a.o = new v(this.f2074a, (int) R.string.press);
        this.f2074a.o.setCancelable(true);
        this.f2074a.o.setOnCancelListener(new bu(this));
        this.f2074a.o.show();
        super.a();
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        a("绑定成功");
        this.f2074a.setResult(-1, this.f2074a.getIntent());
        this.f2074a.finish();
        super.a((Boolean) obj);
    }

    /* access modifiers changed from: protected */
    public final void b() {
        if (this.f2074a.o != null) {
            this.f2074a.o.dismiss();
            this.f2074a.o = (v) null;
        }
        super.b();
    }
}
