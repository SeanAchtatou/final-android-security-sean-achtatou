package com.immomo.momo.android.activity;

import android.graphics.Bitmap;
import com.immomo.momo.R;
import com.immomo.momo.android.c.g;
import com.immomo.momo.util.j;

final class hl implements g {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ OtherProfileActivity f1717a;

    hl(OtherProfileActivity otherProfileActivity) {
        this.f1717a = otherProfileActivity;
    }

    public final /* synthetic */ void a(Object obj) {
        Bitmap bitmap = (Bitmap) obj;
        this.f1717a.t.setImageLoading(false);
        if (bitmap != null && this.f1717a.t.af.equals(this.f1717a.aM.getTag(R.id.tag_item_imageid))) {
            j.a(this.f1717a.t.af, bitmap);
            this.f1717a.runOnUiThread(new hm(this, bitmap));
        }
    }
}
