package com.immomo.momo.android.activity.discuss;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Message;
import com.immomo.momo.android.c.g;
import com.immomo.momo.service.bean.au;

final class o implements g {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ DiscussProfileActivity f1241a;
    private final /* synthetic */ au b;

    o(DiscussProfileActivity discussProfileActivity, au auVar) {
        this.f1241a = discussProfileActivity;
        this.b = auVar;
    }

    public final /* synthetic */ void a(Object obj) {
        Message message = new Message();
        Bundle bundle = new Bundle();
        bundle.putString("guid", this.b.b);
        message.setData(bundle);
        message.obj = (Bitmap) obj;
        message.what = 12;
        this.f1241a.z.sendMessage(message);
    }
}
