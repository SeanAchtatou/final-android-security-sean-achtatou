package com.immomo.momo.android.map;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import com.google.android.maps.MapView;
import com.immomo.momo.util.m;

public class MomoGoogleMapView extends MapView {

    /* renamed from: a  reason: collision with root package name */
    private aa f2452a = null;
    private boolean b = false;

    public MomoGoogleMapView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        new m(getClass().getSimpleName());
    }

    public MomoGoogleMapView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        new m(getClass().getSimpleName());
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case 1:
                if (this.b && this.f2452a != null) {
                    this.f2452a.a(1);
                }
                this.b = false;
                break;
            case 2:
                if (!this.b) {
                    this.f2452a.a(2);
                }
                this.b = true;
                break;
        }
        return MomoGoogleMapView.super.onTouchEvent(motionEvent);
    }

    public void setGetAddressListener(aa aaVar) {
        this.f2452a = aaVar;
    }
}
