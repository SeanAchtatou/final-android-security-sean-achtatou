package com.immomo.momo.android.game;

import android.view.View;

class aw implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ MDKTradeActivity f2417a;

    private aw(MDKTradeActivity mDKTradeActivity) {
        this.f2417a = mDKTradeActivity;
    }

    /* synthetic */ aw(MDKTradeActivity mDKTradeActivity, byte b) {
        this(mDKTradeActivity);
    }

    public void onClick(View view) {
        view.setSelected(true);
        for (int i = 0; i < this.f2417a.h.getChildCount(); i++) {
            View childAt = this.f2417a.h.getChildAt(i);
            if (childAt != view) {
                childAt.setSelected(false);
            }
        }
    }
}
