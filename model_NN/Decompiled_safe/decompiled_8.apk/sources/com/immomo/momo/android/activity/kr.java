package com.immomo.momo.android.activity;

import android.os.AsyncTask;
import com.immomo.momo.R;
import com.immomo.momo.a.a;
import com.immomo.momo.a.w;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.d;

final class kr extends AsyncTask {

    /* renamed from: a  reason: collision with root package name */
    private v f1796a;
    private /* synthetic */ SharePageActivity b;

    public kr(SharePageActivity sharePageActivity) {
        this.b = sharePageActivity;
        this.f1796a = new v(sharePageActivity);
        this.f1796a.a("请求提交中");
        this.f1796a.setCancelable(true);
    }

    private String a() {
        try {
            d.b(this.b.f, this.b.k.isChecked());
            return "yes";
        } catch (w e) {
            this.b.e.a((Throwable) e);
            this.b.b((int) R.string.errormsg_network_unfind);
            return "no";
        } catch (a e2) {
            this.b.e.a((Throwable) e2);
            this.b.b((CharSequence) e2.getMessage());
            return "no";
        } catch (Exception e3) {
            this.b.e.a((Throwable) e3);
            this.b.b((int) R.string.errormsg_server);
            return "no";
        }
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object doInBackground(Object... objArr) {
        return a();
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void onPostExecute(Object obj) {
        String str = (String) obj;
        super.onPostExecute(str);
        if (this.f1796a != null) {
            this.f1796a.dismiss();
        }
        if (str.equals("yes")) {
            this.b.a((CharSequence) "分享成功");
            this.b.setResult(-1, this.b.v());
            this.b.finish();
        }
    }

    /* access modifiers changed from: protected */
    public final void onPreExecute() {
        if (this.f1796a != null) {
            this.f1796a.setOnCancelListener(new ks(this));
            this.f1796a.show();
        }
        super.onPreExecute();
    }
}
