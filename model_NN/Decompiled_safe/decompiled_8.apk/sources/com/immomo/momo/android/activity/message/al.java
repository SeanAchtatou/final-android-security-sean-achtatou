package com.immomo.momo.android.activity.message;

import android.content.Intent;
import com.immomo.momo.android.a.a.ab;
import com.immomo.momo.android.broadcast.d;

final class al implements d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ChatActivity f1919a;

    al(ChatActivity chatActivity) {
        this.f1919a = chatActivity;
    }

    public final void a(Intent intent) {
        String stringExtra = intent.getStringExtra("key_message_id");
        long longExtra = intent.getLongExtra("key_upload_progress", 0);
        ab abVar = (ab) this.f1919a.T.e.get(stringExtra);
        if (abVar != null) {
            long j = abVar.h.fileSize;
            if (longExtra >= 0) {
                abVar.h.fileUploadedLength = longExtra;
                abVar.a((((float) longExtra) * 100.0f) / ((float) j));
                if (longExtra < j) {
                    return;
                }
            }
            this.f1919a.T.e.remove(stringExtra);
            this.f1919a.J();
        }
    }
}
