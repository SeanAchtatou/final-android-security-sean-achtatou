package com.immomo.momo.android.map;

import android.location.Location;
import com.immomo.momo.android.b.l;
import com.immomo.momo.android.b.p;
import com.immomo.momo.android.b.z;

final class bm extends p {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ SelectSiteGoogleActivity f2492a;

    bm(SelectSiteGoogleActivity selectSiteGoogleActivity) {
        this.f2492a = selectSiteGoogleActivity;
    }

    public final void a(Location location, int i, int i2, int i3) {
        if (this.f2492a.c.b) {
            this.f2492a.n = location;
            this.f2492a.p = i;
            if (i != 0) {
                this.f2492a.c.a(location, i, i2, i3);
            } else if (z.a(location)) {
                new l(this.f2492a.c, 0).execute(location);
            } else {
                this.f2492a.c.a(location, i, i2, i3);
            }
        }
    }
}
