package com.immomo.momo.android.game;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.m;
import com.unicom.dcLoader.Utils;

final class az extends d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ax f2419a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public az(ax axVar, Context context) {
        super(context);
        this.f2419a = axVar;
    }

    /* access modifiers changed from: protected */
    public final Object a(Object... objArr) {
        ao aoVar = this.f2419a.R;
        ax axVar = this.f2419a;
        aoVar.o = ax.Q();
        m.a().a(this.f2419a.P, this.f2419a.Q, this.f2419a.O, this.f2419a.R);
        return null;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.f2419a.a(new v(this.f2419a.c(), "正在获取支付信息...", this));
    }

    /* access modifiers changed from: protected */
    public final void a(Object obj) {
        Utils.a().a(this.f2419a.c(), this.f2419a.R.n, this.f2419a.R.f2409a, this.f2419a.R.d, this.f2419a.R.j, this.f2419a.R.l, this.f2419a.R.k, new ba(this.f2419a, (byte) 0));
        Utils.a().a(this.f2419a.c(), this.f2419a.R.m);
        this.f2419a.R.i = true;
        this.f2419a.P();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.f2419a.N();
    }
}
