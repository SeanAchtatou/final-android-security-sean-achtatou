package com.immomo.momo.android.game;

import android.content.Context;
import com.immomo.momo.a.a;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.m;

final class t extends d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ k f2444a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public t(k kVar, Context context) {
        super(context);
        this.f2444a = kVar;
    }

    /* access modifiers changed from: protected */
    public final Object a(Object... objArr) {
        m a2 = m.a();
        String str = this.f2444a.U;
        String str2 = this.f2444a.V;
        ai aiVar = this.f2444a.O;
        a2.a(str, str2, this.f2444a.W);
        return null;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.f2444a.a(new v(this.f2444a.c(), "正在支付...", this));
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        if (!(exc instanceof a) || ((a) exc).f670a != 30210) {
            a("支付失败，请重试");
        } else {
            this.f2444a.O();
        }
    }

    /* access modifiers changed from: protected */
    public final void a(Object obj) {
        this.f2444a.O();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.f2444a.N();
        this.f2444a.X = false;
    }
}
