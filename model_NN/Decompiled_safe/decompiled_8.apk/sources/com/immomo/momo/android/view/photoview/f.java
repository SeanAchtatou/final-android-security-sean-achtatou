package com.immomo.momo.android.view.photoview;

import android.annotation.TargetApi;
import android.content.Context;
import android.widget.OverScroller;

@TargetApi(9)
final class f extends e {

    /* renamed from: a  reason: collision with root package name */
    private OverScroller f2821a;

    public f(Context context) {
        this.f2821a = new OverScroller(context);
    }

    public final void a(int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
        this.f2821a.fling(i, i2, i3, i4, i5, i6, i7, i8, 0, 0);
    }

    public final boolean a() {
        return this.f2821a.computeScrollOffset();
    }

    public final void b() {
        this.f2821a.forceFinished(true);
    }

    public final int c() {
        return this.f2821a.getCurrX();
    }

    public final int d() {
        return this.f2821a.getCurrY();
    }
}
