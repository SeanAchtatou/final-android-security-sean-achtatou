package com.immomo.momo.android.activity.group;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import com.immomo.momo.service.bean.a.a;

final class fh implements AdapterView.OnItemClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ SiteGroupsActivity f1656a;

    fh(SiteGroupsActivity siteGroupsActivity) {
        this.f1656a = siteGroupsActivity;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        Intent intent = new Intent(this.f1656a.getApplicationContext(), GroupProfileActivity.class);
        intent.putExtra("gid", ((a) this.f1656a.l.getItem(i)).b);
        this.f1656a.startActivity(intent);
    }
}
