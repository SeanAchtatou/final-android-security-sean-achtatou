package com.immomo.momo.android.activity.emotestore;

import android.content.Context;
import android.widget.ListAdapter;
import com.immomo.momo.android.c.d;
import com.immomo.momo.protocol.a.i;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

final class at extends d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ am f1297a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public at(am amVar, Context context) {
        super(context);
        this.f1297a = amVar;
        if (amVar.S != null) {
            amVar.S.cancel(true);
        }
        amVar.S = this;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        ArrayList arrayList = new ArrayList();
        i.a().a(arrayList, 0);
        this.f1297a.Q.a(arrayList);
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public final void a() {
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        super.a(exc);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        List list = (List) obj;
        Date date = new Date();
        this.f1297a.O.setLastFlushTime(date);
        this.f1297a.N.a("hotem_reflush", date);
        if (list.size() < 30) {
            this.f1297a.P.setVisibility(8);
        } else {
            this.f1297a.P.setVisibility(0);
        }
        this.f1297a.R.b();
        this.f1297a.R.b((Collection) list);
        this.f1297a.O.setAdapter((ListAdapter) this.f1297a.R);
        this.f1297a.U.j();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.f1297a.O.n();
        this.f1297a.S = null;
    }
}
