package com.immomo.momo.android.map;

import android.location.Location;
import com.immomo.momo.R;
import com.immomo.momo.android.b.p;
import com.immomo.momo.android.b.z;
import com.immomo.momo.util.ao;

final class x extends p {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ GeoGoogleMapActivity f2518a;

    x(GeoGoogleMapActivity geoGoogleMapActivity) {
        this.f2518a = geoGoogleMapActivity;
    }

    public final void a(Location location, int i, int i2, int i3) {
        if (this.b) {
            if (z.a(location)) {
                this.f2518a.m.post(new y(this, location));
                return;
            }
            this.f2518a.a();
            if (i2 == 104) {
                ao.g(R.string.errormsg_network_unfind);
            } else {
                ao.g(R.string.errormsg_location_nearby_failed);
            }
        }
    }
}
