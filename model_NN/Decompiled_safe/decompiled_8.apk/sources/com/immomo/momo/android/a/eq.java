package com.immomo.momo.android.a;

import android.content.Context;
import android.content.Intent;
import com.immomo.momo.android.broadcast.f;
import com.immomo.momo.android.c.b;
import com.immomo.momo.g;
import com.immomo.momo.protocol.a.k;
import com.immomo.momo.service.bean.a.e;
import com.immomo.momo.service.v;

final class eq extends b {
    private e c;
    private /* synthetic */ ek d;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public eq(ek ekVar, Context context) {
        super(context);
        this.d = ekVar;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        this.c = ((e[]) objArr)[0];
        String b = k.a().b(this.c.f);
        new v().f(this.c.f);
        Intent intent = new Intent(f.f2349a);
        intent.putExtra("feedid", this.c.f);
        intent.putExtra("userid", g.q().h);
        this.d.e.sendBroadcast(intent);
        return b;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        String str = (String) obj;
        a(str);
        this.d.c(this.c);
        super.a((Object) str);
    }
}
