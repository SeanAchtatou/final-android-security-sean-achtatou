package com.immomo.momo.android.activity.event;

import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.a.a.f.b;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.ImageFactoryActivity;
import com.immomo.momo.android.activity.ah;
import com.immomo.momo.android.broadcast.w;
import com.immomo.momo.android.view.EmoteInputView;
import com.immomo.momo.android.view.HeaderLayout;
import com.immomo.momo.android.view.MEmoteEditeText;
import com.immomo.momo.android.view.ResizeListenerLayout;
import com.immomo.momo.android.view.a.n;
import com.immomo.momo.android.view.a.o;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.android.view.bi;
import com.immomo.momo.plugin.b.a;
import com.immomo.momo.service.ai;
import com.immomo.momo.service.aq;
import com.immomo.momo.service.bean.aa;
import com.immomo.momo.service.bean.ay;
import com.immomo.momo.service.bean.q;
import com.immomo.momo.service.r;
import com.immomo.momo.util.ao;
import com.immomo.momo.util.h;
import com.immomo.momo.util.j;
import java.io.File;
import java.util.Date;
import java.util.UUID;
import mm.purchasesdk.PurchaseCode;

public class PublishEventFeedActivity extends ah implements View.OnClickListener, View.OnTouchListener {
    private String A;
    /* access modifiers changed from: private */
    public int B;
    /* access modifiers changed from: private */
    public EmoteInputView C = null;
    private Handler D = new Handler();
    /* access modifiers changed from: private */
    public v E = null;
    private String F = PoiTypeDef.All;
    private File G = null;
    /* access modifiers changed from: private */
    public File H = null;
    /* access modifiers changed from: private */
    public a I = null;
    private Bitmap J = null;
    protected boolean h = false;
    /* access modifiers changed from: private */
    public cg i = null;
    /* access modifiers changed from: private */
    public cg j = null;
    /* access modifiers changed from: private */
    public cg k = null;
    /* access modifiers changed from: private */
    public cg l = null;
    /* access modifiers changed from: private */
    public MEmoteEditeText m = null;
    private TextView n = null;
    /* access modifiers changed from: private */
    public aa o = null;
    /* access modifiers changed from: private */
    public ai p = null;
    /* access modifiers changed from: private */
    public aq q = null;
    /* access modifiers changed from: private */
    public r r = null;
    /* access modifiers changed from: private */
    public ImageView s = null;
    /* access modifiers changed from: private */
    public TextView t = null;
    private ResizeListenerLayout u = null;
    private boolean v = false;
    private boolean w = false;
    private boolean x = false;
    /* access modifiers changed from: private */
    public String y;
    private String z;

    static /* synthetic */ boolean r(PublishEventFeedActivity publishEventFeedActivity) {
        String trim = publishEventFeedActivity.m.getText().toString().trim();
        if (trim.length() > 120) {
            publishEventFeedActivity.a((int) R.string.feed_publish_toast_long);
            return false;
        } else if (publishEventFeedActivity.I != null || publishEventFeedActivity.H != null || !android.support.v4.b.a.a((CharSequence) trim)) {
            return true;
        } else {
            publishEventFeedActivity.a((int) R.string.feed_publish_toast_nofeed);
            return false;
        }
    }

    private void u() {
        Display defaultDisplay = getWindowManager().getDefaultDisplay();
        defaultDisplay.getWidth();
        this.B = defaultDisplay.getHeight();
    }

    private void v() {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService("input_method");
        View currentFocus = getCurrentFocus();
        if (currentFocus != null) {
            inputMethodManager.hideSoftInputFromWindow(currentFocus.getWindowToken(), 2);
        }
    }

    static /* synthetic */ void w(PublishEventFeedActivity publishEventFeedActivity) {
        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("immomo_");
        stringBuffer.append(android.support.v4.b.a.c(new Date()));
        stringBuffer.append("_" + UUID.randomUUID());
        stringBuffer.append(".jpg");
        publishEventFeedActivity.F = stringBuffer.toString();
        intent.putExtra("output", Uri.fromFile(new File(com.immomo.momo.a.i(), publishEventFeedActivity.F)));
        publishEventFeedActivity.startActivityForResult(intent, PurchaseCode.UNSUB_OK);
    }

    static /* synthetic */ void x(PublishEventFeedActivity publishEventFeedActivity) {
        Intent intent = new Intent("android.intent.action.GET_CONTENT");
        intent.setType("image/*");
        publishEventFeedActivity.startActivityForResult(intent, PurchaseCode.ORDER_OK);
    }

    static /* synthetic */ void y(PublishEventFeedActivity publishEventFeedActivity) {
        publishEventFeedActivity.v();
        if (publishEventFeedActivity.h) {
            publishEventFeedActivity.D.postDelayed(new bu(publishEventFeedActivity), 300);
        } else {
            publishEventFeedActivity.C.b();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        super.a(bundle);
        setContentView((int) R.layout.activity_publish_eventfeed);
        this.p = new ai();
        this.q = new aq();
        this.r = new r();
        u();
        Intent intent = getIntent();
        if (bundle != null) {
            this.x = bundle.getBoolean("from_share", false);
            this.f = this.q.b(this.f.h);
        }
        if (intent.getExtras() != null) {
            Uri data = intent.getData();
            if (!intent.getExtras().getBoolean("is_read") && data != null) {
                this.f = this.q.b(this.f.h);
                this.x = true;
                Intent intent2 = new Intent(this, ImageFactoryActivity.class);
                intent2.setData(data);
                intent2.putExtra("aspectY", 1);
                intent2.putExtra("aspectX", 1);
                intent2.putExtra("minsize", 320);
                intent2.putExtra("process_model", "filter");
                this.G = new File(com.immomo.momo.a.g(), "temp_" + b.a() + ".jpg_");
                intent2.putExtra("outputFilePath", this.G.getAbsolutePath());
                intent.putExtra("is_read", true);
                startActivityForResult(intent2, PurchaseCode.QUERY_OK);
            }
        }
        this.u = (ResizeListenerLayout) findViewById(R.id.rootlayout);
        this.m = (MEmoteEditeText) findViewById(R.id.signeditor_tv_text);
        this.n = (TextView) findViewById(R.id.tv_sitename);
        this.s = (ImageView) findViewById(R.id.iv_camera);
        this.t = (TextView) findViewById(R.id.tv_textcount);
        this.C = (EmoteInputView) findViewById(R.id.emoteview);
        this.C.setEditText(this.m);
        this.C.setOnEmoteSelectedListener(new ca(this));
        this.m.setOnTouchListener(this);
        this.m.addTextChangedListener(new bx(this));
        bi a2 = new bi(getApplicationContext()).a((int) R.drawable.ic_topbar_confirm_white);
        a2.setMarginRight(8);
        a2.setBackgroundResource(R.drawable.bg_header_submit);
        m().a(a2, new by(this));
        findViewById(R.id.iv_camera_cover).setOnClickListener(this);
        this.u.setOnResizeListener(new bz(this));
        this.o = new aa();
        this.o.d = new ay();
        if (bundle == null || !bundle.getBoolean("from_saveinstance", false)) {
            Intent intent3 = getIntent();
            if (!(intent3 == null || intent3.getExtras() == null)) {
                this.w = Boolean.valueOf(intent3.getBooleanExtra("is_from_nearbyfeed", false)).booleanValue();
                this.o.d.f3001a = intent3.getStringExtra("siteid");
                this.o.d.f = intent3.getStringExtra("sitename");
                this.o.d.d = intent3.getIntExtra("sitetype", 0);
                this.o.d.h = intent3.getDoubleExtra("lat", 0.0d);
                this.o.d.i = intent3.getDoubleExtra("lng", 0.0d);
                this.y = intent3.getStringExtra("eventid");
                this.z = intent3.getStringExtra("eventname");
                this.A = intent3.getStringExtra("eventdes");
            }
        } else {
            this.o.d.f3001a = (String) bundle.get("siteid");
            this.o.d.f = (String) bundle.get("sitename");
            this.F = bundle.getString("camera_filename");
            this.w = bundle.getBoolean("is_from_nearbyfeed");
            String string = bundle.getString("avatorFilePath");
            String string2 = bundle.getString("save_uploadguid");
            String string3 = bundle.getString("save_emote");
            this.o.d.h = bundle.getDouble("lat");
            this.o.d.i = bundle.getDouble("lng");
            this.o.d.d = bundle.getInt("sitetype");
            this.y = bundle.getString("eventid");
            if (!android.support.v4.b.a.a((CharSequence) string)) {
                this.G = new File(string);
            }
            if (!android.support.v4.b.a.a((CharSequence) string2)) {
                this.H = new File(string2);
            }
            if (!android.support.v4.b.a.a((CharSequence) string3)) {
                this.I = new a(string3.toString());
            }
            String str = bundle.get("save_feedcontent") == null ? PoiTypeDef.All : (String) bundle.get("save_feedcontent");
            if (!android.support.v4.b.a.a((CharSequence) str)) {
                this.m.setText(str);
                this.m.setSelection(str.length());
            }
            if (this.H != null) {
                this.s.setImageBitmap(j.a(this.H));
            }
            if (this.I != null) {
                this.s.setImageBitmap(android.support.v4.b.a.l(q.a(this.I.g(), this.I.h()).getAbsolutePath()));
            }
        }
        this.n.setText(this.o.d.f);
        if (android.support.v4.b.a.a((CharSequence) this.y)) {
            a((CharSequence) "没有指定活动ID");
            finish();
        }
        m().setTitleText("发布活动讨论");
        String str2 = String.valueOf(android.support.v4.b.a.a(this.z) ? PoiTypeDef.All : "[" + this.z + "]") + (android.support.v4.b.a.a(this.A) ? PoiTypeDef.All : this.A);
        HeaderLayout m2 = m();
        if (android.support.v4.b.a.a((CharSequence) str2)) {
            str2 = PoiTypeDef.All;
        }
        m2.setSubTitleText(str2);
        this.i = new cg(this, findViewById(R.id.signeditor_layout_syncto_sinaweibo), 1);
        this.k = new cg(this, findViewById(R.id.signeditor_layout_syncto_tx), 2);
        this.j = new cg(this, findViewById(R.id.signeditor_layout_syncto_renren), 3);
        this.l = new cg(this, findViewById(R.id.signeditor_layout_syncto_feed), 5);
        if (bundle == null) {
            String stringExtra = getIntent().getStringExtra("toptic");
            if (!android.support.v4.b.a.a((CharSequence) stringExtra)) {
                this.m.setText(stringExtra);
                this.m.setSelection(stringExtra.length());
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.h.a(java.lang.String, android.graphics.Bitmap, int, boolean):java.io.File
     arg types: [java.lang.String, android.graphics.Bitmap, int, int]
     candidates:
      com.immomo.momo.util.h.a(java.lang.String, java.lang.String, int, boolean):void
      com.immomo.momo.util.h.a(java.lang.String, android.graphics.Bitmap, int, boolean):java.io.File */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.b.a.a(android.graphics.Bitmap, float, boolean):android.graphics.Bitmap
     arg types: [android.graphics.Bitmap, int, int]
     candidates:
      android.support.v4.b.a.a(android.graphics.Bitmap, int, int):android.graphics.Bitmap
      android.support.v4.b.a.a(java.io.File, int, int):android.graphics.Bitmap
      android.support.v4.b.a.a(java.lang.String, java.lang.String, java.lang.String):java.lang.String
      android.support.v4.b.a.a(java.lang.Object[], java.lang.String, java.lang.String):java.lang.String
      android.support.v4.b.a.a(android.graphics.Bitmap, float, boolean):android.graphics.Bitmap */
    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        switch (i2) {
            case 11:
                if (i3 == -1) {
                    this.f.an = true;
                    new aq().b(this.f);
                    this.i.g = true;
                    this.i.a(true);
                    Intent intent2 = new Intent(w.f2366a);
                    intent2.putExtra("momoid", this.f.h);
                    sendBroadcast(intent2);
                    return;
                }
                return;
            case 12:
                if (i3 == -1) {
                    this.f.ar = true;
                    this.j.g = true;
                    this.j.a(true);
                    Intent intent3 = new Intent(w.f2366a);
                    intent3.putExtra("momoid", this.f.h);
                    sendBroadcast(intent3);
                    return;
                }
                return;
            case 13:
                if (i3 == -1) {
                    this.f.at = true;
                    new aq().b(this.f);
                    this.k.g = true;
                    this.k.a(true);
                    Intent intent4 = new Intent(w.f2366a);
                    intent4.putExtra("momoid", this.f.h);
                    sendBroadcast(intent4);
                    return;
                }
                return;
            case PurchaseCode.QUERY_OK /*101*/:
                if (i3 == -1 && intent != null) {
                    if (!android.support.v4.b.a.a((CharSequence) this.F)) {
                        File file = new File(com.immomo.momo.a.i(), this.F);
                        if (file.exists()) {
                            file.delete();
                        }
                        this.F = null;
                    }
                    if (this.G != null) {
                        String absolutePath = this.G.getAbsolutePath();
                        String substring = this.G.getName().substring(0, this.G.getName().lastIndexOf("."));
                        Bitmap l2 = android.support.v4.b.a.l(absolutePath);
                        if (l2 != null) {
                            this.H = h.a(substring, l2, 16, false);
                            this.e.a((Object) ("scaleAndSavePhoto, uploadFile=" + this.H.getPath()));
                            this.J = android.support.v4.b.a.a(l2, 150.0f, true);
                            h.a(substring, this.J, 15, false);
                            this.s.setImageBitmap(this.J);
                        }
                        try {
                            this.G.delete();
                            this.G = null;
                        } catch (Exception e) {
                        }
                        getWindow().getDecorView().requestFocus();
                        return;
                    }
                    return;
                } else if (i3 == 1003) {
                    ao.b("图片尺寸太小，请重新选择", 1);
                    return;
                } else if (i3 == 1000) {
                    ao.d(R.string.cropimage_error_other);
                    return;
                } else if (i3 == 1002) {
                    ao.d(R.string.cropimage_error_store);
                    return;
                } else if (i3 == 1001) {
                    ao.d(R.string.cropimage_error_filenotfound);
                    return;
                } else if (i3 == 0 && this.x) {
                    finish();
                    return;
                } else {
                    return;
                }
            case PurchaseCode.ORDER_OK /*102*/:
                if (i3 == -1 && intent != null) {
                    Uri data = intent.getData();
                    Intent intent5 = new Intent(this, ImageFactoryActivity.class);
                    intent5.setData(data);
                    intent5.putExtra("aspectY", 1);
                    intent5.putExtra("aspectX", 1);
                    intent5.putExtra("minsize", 320);
                    intent5.putExtra("process_model", "filter");
                    this.G = new File(com.immomo.momo.a.g(), "temp_" + b.a() + ".jpg_");
                    intent5.putExtra("outputFilePath", this.G.getAbsolutePath());
                    startActivityForResult(intent5, PurchaseCode.QUERY_OK);
                    return;
                }
                return;
            case PurchaseCode.UNSUB_OK /*103*/:
                if (i3 == -1 && !android.support.v4.b.a.a((CharSequence) this.F)) {
                    this.m.requestFocus();
                    Uri fromFile = Uri.fromFile(new File(com.immomo.momo.a.i(), this.F));
                    if (fromFile != null) {
                        Intent intent6 = new Intent(this, ImageFactoryActivity.class);
                        intent6.setData(fromFile);
                        intent6.putExtra("minsize", 320);
                        intent6.putExtra("process_model", "filter");
                        intent6.putExtra("maxwidth", 720);
                        intent6.putExtra("maxheight", 3000);
                        this.G = new File(com.immomo.momo.a.g(), "temp_" + b.a() + ".jpg_");
                        intent6.putExtra("outputFilePath", this.G.getAbsolutePath());
                        startActivityForResult(intent6, PurchaseCode.QUERY_OK);
                        return;
                    }
                    return;
                }
                return;
            default:
                return;
        }
    }

    public void onBackPressed() {
        if (r()) {
            if (this.C.isShown()) {
                this.C.a();
                this.h = false;
                return;
            } else if (!(this.I == null && this.H == null && !android.support.v4.b.a.f(this.m.getText().toString().trim()))) {
                n nVar = new n(this);
                nVar.a();
                nVar.setTitle((int) R.string.feed_publish_dialog_title);
                nVar.a((int) R.string.eventfeed_publish_dialog_content);
                nVar.a(0, (int) R.string.dialog_btn_confim, new bv(this));
                nVar.a(1, (int) R.string.dialog_btn_cancel, new bw());
                nVar.show();
                return;
            }
        }
        super.onBackPressed();
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_camera_cover /*2131165403*/:
                this.C.a();
                v();
                o oVar = new o(this, (int) R.array.editprofile_add_pic);
                oVar.setTitle((int) R.string.dialog_title_add_pic);
                oVar.a(new cd(this));
                oVar.show();
                return;
            default:
                return;
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        u();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.i.b();
        this.j.b();
        this.k.b();
        this.l.b();
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Bundle bundle) {
        super.onRestoreInstanceState(bundle);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (!this.v) {
            this.v = true;
        }
        this.h = false;
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        String editable = this.m.getText().toString();
        if (!android.support.v4.b.a.a((CharSequence) editable)) {
            bundle.putString("save_feedcontent", editable);
        }
        if (this.H != null) {
            bundle.putString("save_uploadguid", this.H.getAbsolutePath());
        }
        if (this.I != null) {
            bundle.putString("save_emote", this.I.toString());
        }
        if (!android.support.v4.b.a.a((CharSequence) this.o.d.f3001a)) {
            bundle.putString("siteid", this.o.d.f3001a);
        }
        if (!android.support.v4.b.a.a((CharSequence) this.o.d.f)) {
            bundle.putString("sitename", this.o.d.f);
        }
        bundle.putDouble("lat", this.o.d.h);
        bundle.putDouble("lng", this.o.d.i);
        bundle.putInt("sitetype", this.o.d.d);
        bundle.putBoolean("is_from_nearbyfeed", this.w);
        bundle.putString("eventid", this.y);
        if (!android.support.v4.b.a.a((CharSequence) this.F)) {
            bundle.putString("camera_filename", this.F);
        }
        if (this.G != null) {
            bundle.putString("avatorFilePath", this.G.getAbsolutePath());
        }
        bundle.putBoolean("from_saveinstance", true);
        bundle.putBoolean("from_share", this.x);
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        switch (view.getId()) {
            case R.id.layout_content /*2131165199*/:
                if ((this.C.isShown() || this.h) && 1 == motionEvent.getAction() && motionEvent.getEventTime() - motionEvent.getDownTime() < 100) {
                    if (this.C.isShown()) {
                        this.C.a();
                    } else {
                        v();
                    }
                    this.h = false;
                    break;
                }
            case R.id.signeditor_tv_text /*2131165795*/:
                if (motionEvent.getAction() == 1) {
                    this.C.a();
                    break;
                }
                break;
        }
        return false;
    }
}
