package com.immomo.momo.android.activity.event;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.protocol.a.j;
import com.immomo.momo.service.bean.bf;
import java.util.ArrayList;
import java.util.List;

final class ag extends d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ EventMemberListActivity f1351a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ag(EventMemberListActivity eventMemberListActivity, Context context) {
        super(context);
        this.f1351a = eventMemberListActivity;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        ArrayList<bf> arrayList = new ArrayList<>();
        this.f1351a.s = j.a().b(arrayList, this.f1351a.n, 0);
        if (arrayList.size() > 0) {
            this.f1351a.k.a(this.f1351a.n, arrayList);
        }
        this.f1351a.r.clear();
        for (bf bfVar : arrayList) {
            this.f1351a.r.put(bfVar.h, bfVar);
        }
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        if (this.f1351a.p != null && !this.f1351a.p.isCancelled()) {
            this.f1351a.p.cancel(true);
        }
        this.f1351a.q.g();
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        super.a(exc);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        List list = (List) obj;
        if (list != null) {
            this.f1351a.j.clear();
            this.f1351a.j.addAll(list);
            this.f1351a.w();
            EventMemberListActivity.j(this.f1351a);
            return;
        }
        a("获取活动成员列表失败");
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.f1351a.o = null;
        this.f1351a.x();
    }
}
