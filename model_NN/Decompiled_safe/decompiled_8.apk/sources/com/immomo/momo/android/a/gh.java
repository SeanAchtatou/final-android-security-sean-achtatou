package com.immomo.momo.android.a;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.view.HandyListView;
import com.immomo.momo.protocol.a.i;
import com.immomo.momo.service.bean.q;
import com.immomo.momo.util.j;
import java.util.List;

public final class gh extends a implements View.OnClickListener {
    private HandyListView d = null;
    private boolean e;
    private List f = null;
    private List g = null;

    public gh(Context context, List list, List list2, HandyListView handyListView) {
        super(context, list);
        this.d = handyListView;
        this.f = list;
        this.g = list2;
    }

    private boolean e(int i) {
        return i > 0 && i <= this.f.size();
    }

    private boolean f(int i) {
        return i > this.f.size() + 1 && i <= (this.f.size() + this.g.size()) + 1;
    }

    public final boolean areAllItemsEnabled() {
        return false;
    }

    public final void b(boolean z) {
        this.e = z;
    }

    /* renamed from: d */
    public final q getItem(int i) {
        if (e(i)) {
            return (q) this.f.get(i - 1);
        } else if (!f(i)) {
            return null;
        } else {
            return (q) this.g.get((i - this.f.size()) - 2);
        }
    }

    public final boolean e() {
        return this.e;
    }

    public final int getCount() {
        return (this.e || this.g.isEmpty()) ? this.f.size() + 1 : this.f.size() + this.g.size() + 2;
    }

    public final int getItemViewType(int i) {
        return (!e(i) && !f(i)) ? 0 : 1;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        gi giVar;
        int i2 = 0;
        if (getItemViewType(i) == 1) {
            if (view == null) {
                view = a((int) R.layout.listitem_minemotion);
                gi giVar2 = new gi((byte) 0);
                view.setTag(giVar2);
                giVar2.g = (Button) view.findViewById(R.id.mineemotion_btn);
                giVar2.d = (ImageView) view.findViewById(R.id.mineemotion_iv_cover);
                giVar2.f840a = (TextView) view.findViewById(R.id.mineemotion_tv_name);
                giVar2.b = (TextView) view.findViewById(R.id.emotionitem_tv_name_flag);
                giVar2.c = (TextView) view.findViewById(R.id.mineemotion_tv_status);
                giVar2.h = view.findViewById(R.id.layout_content);
                giVar2.e = (ImageView) view.findViewById(R.id.mineemotion_iv_right);
                giVar2.f = (ImageView) view.findViewById(R.id.mineemotion_iv_drop);
                giVar2.g.setOnClickListener(this);
                giVar2.h.setOnClickListener(this);
                giVar = giVar2;
            } else {
                giVar = (gi) view.getTag();
            }
            q d2 = getItem(i);
            j.b(d2.b(), giVar.d, this.d, 18, true);
            giVar.f840a.setText(d2.b);
            giVar.g.setTag(Integer.valueOf(i));
            giVar.h.setTag(Integer.valueOf(i));
            giVar.h.setClickable(!this.e);
            if (d2.c != 0) {
                giVar.b.setText(d2.d);
                giVar.b.setVisibility(0);
            }
            if (d2.c == 1) {
                giVar.b.setBackgroundResource(R.drawable.round_eshop_lable1);
            } else if (d2.c == 2) {
                giVar.b.setBackgroundResource(R.drawable.round_eshop_lable2);
            } else if (d2.c == 3) {
                giVar.b.setBackgroundResource(R.drawable.round_eshop_lable3);
            } else if (d2.c == 4) {
                giVar.b.setBackgroundResource(R.drawable.round_eshop_lable4);
            } else {
                giVar.b.setVisibility(8);
            }
            if (this.e) {
                giVar.g.setText("删除");
                giVar.g.setVisibility(0);
                giVar.c.setVisibility(8);
                giVar.e.setVisibility(8);
                giVar.f.setVisibility(0);
            } else {
                giVar.e.setImageResource(R.drawable.ic_common_arrow_right);
                giVar.e.setVisibility(0);
                giVar.f.setVisibility(8);
                giVar.g.setVisibility(8);
                giVar.c.setVisibility(0);
                if (d2.t) {
                    giVar.c.setSelected(false);
                    giVar.c.setText(PoiTypeDef.All);
                } else {
                    i.a();
                    if (i.b(d2.f3035a)) {
                        giVar.c.setSelected(false);
                        giVar.c.setText("下载中");
                    } else {
                        giVar.c.setSelected(true);
                        giVar.c.setText("未下载");
                    }
                }
            }
        } else {
            if (view == null) {
                view = a((int) R.layout.listitem_mineemotion_title);
            }
            TextView textView = (TextView) view.findViewById(R.id.minemotion_tv_title);
            if (i == 0) {
                if (this.f.isEmpty()) {
                    i2 = 8;
                }
                textView.setVisibility(i2);
                if (this.e) {
                    textView.setText("按住拖拽图标可调整顺序");
                } else {
                    textView.setText("当前使用表情");
                }
            } else {
                textView.setVisibility(0);
                textView.setText("可使用表情");
            }
        }
        return view;
    }

    public final int getViewTypeCount() {
        return 2;
    }

    public final boolean isEnabled(int i) {
        return getItemViewType(i) != 0;
    }

    public final void onClick(View view) {
        AdapterView.OnItemClickListener onItemClickListener;
        int intValue = ((Integer) view.getTag()).intValue();
        if (this.d != null && (onItemClickListener = this.d.getOnItemClickListener()) != null) {
            onItemClickListener.onItemClick(this.d, view, intValue, (long) view.getId());
        }
    }
}
