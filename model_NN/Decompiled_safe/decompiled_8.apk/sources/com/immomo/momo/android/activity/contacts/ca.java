package com.immomo.momo.android.activity.contacts;

import android.content.Context;
import android.support.v4.b.a;
import com.immomo.momo.android.c.d;
import com.immomo.momo.protocol.a.n;
import java.util.ArrayList;
import java.util.Date;

final class ca extends d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ bk f1182a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ca(bk bkVar, Context context) {
        super(context);
        this.f1182a = bkVar;
    }

    /* access modifiers changed from: protected */
    public final Object a(Object... objArr) {
        this.f1182a.Q = new ArrayList();
        this.f1182a.R = new ArrayList();
        n.a().a(this.f1182a.Q, this.f1182a.R);
        this.f1182a.U.a(this.f1182a.R);
        this.f1182a.T.b(this.f1182a.Q);
        bk.a(this.f1182a, this.f1182a.R.size());
        bk.b(this.f1182a, this.f1182a.Q.size());
        return null;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
    }

    /* access modifiers changed from: protected */
    public final void a(Object obj) {
        super.a(obj);
        this.f1182a.O.setLastFlushTime(this.f1182a.S);
        this.f1182a.N.c("lasttime_mygroups_success", a.c(this.f1182a.S));
        this.f1182a.P.a(this.f1182a.Q, this.f1182a.R);
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.f1182a.S = new Date();
        this.f1182a.N.c("lasttime_my_grouplist", a.c(this.f1182a.S));
        this.f1182a.O.n();
    }
}
