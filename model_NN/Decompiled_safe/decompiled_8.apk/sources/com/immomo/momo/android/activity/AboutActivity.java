package com.immomo.momo.android.activity;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;
import com.immomo.momo.R;

public class AboutActivity extends ao {
    private Button h;
    private Button i;
    private Button j;

    static /* synthetic */ void a(AboutActivity aboutActivity) {
        String str = "market://details?id=" + aboutActivity.getPackageName();
        try {
            Intent intent = new Intent("android.intent.action.VIEW");
            intent.setData(Uri.parse(str));
            aboutActivity.startActivity(intent);
        } catch (Exception e) {
            aboutActivity.b((CharSequence) "找不到应用市场，无法对陌陌评分");
            aboutActivity.e.a((Throwable) e);
        }
    }

    /* access modifiers changed from: protected */
    public final void d() {
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_aboutmomo);
        TextView textView = (TextView) findViewById(R.id.about_tv_versionname);
        getPackageManager();
        try {
            PackageInfo packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            textView.setText("版本: Android " + packageInfo.versionName);
            String str = packageInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            textView.setVisibility(8);
        }
        this.h = (Button) findViewById(R.id.about_btn_checknewversion);
        this.i = (Button) findViewById(R.id.about_btn_introduction);
        this.j = (Button) findViewById(R.id.go_official_website);
        this.h.setOnClickListener(new a(this));
        this.i.setOnClickListener(new b(this));
        this.j.setOnClickListener(new c(this));
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        AboutTabsActivity.f921a.setTitleText((int) R.string.abouts_aboutmomo);
    }
}
