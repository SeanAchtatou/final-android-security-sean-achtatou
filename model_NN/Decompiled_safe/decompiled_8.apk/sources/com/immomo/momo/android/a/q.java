package com.immomo.momo.android.a;

import android.support.v4.b.a;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.android.c.r;
import com.immomo.momo.android.c.u;
import com.immomo.momo.android.view.MomoUnrefreshExpandableListView;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.e;
import com.immomo.momo.service.bean.f;
import com.immomo.momo.util.m;
import java.util.HashMap;
import java.util.List;

public final class q extends gj {

    /* renamed from: a  reason: collision with root package name */
    private List f911a = null;
    /* access modifiers changed from: private */
    public MomoUnrefreshExpandableListView b = null;
    private int c;

    public q(List list, MomoUnrefreshExpandableListView momoUnrefreshExpandableListView, int i) {
        new m(this);
        new HashMap();
        this.f911a = list;
        this.b = momoUnrefreshExpandableListView;
        this.c = i;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public f getGroup(int i) {
        return (f) this.f911a.get(i);
    }

    /* access modifiers changed from: private */
    /* renamed from: c */
    public e getChild(int i, int i2) {
        return (e) ((f) this.f911a.get(i)).b.get(i2);
    }

    public final void a() {
        for (int i = 0; i < getGroupCount(); i++) {
            this.b.expandGroup(i);
        }
    }

    public final void a(int i, int i2) {
        e c2 = getChild(i, i2);
        if (c2 != null) {
            c2.f = true;
        }
        notifyDataSetChanged();
    }

    public final void a(View view, int i) {
        if (i >= 0) {
            ((TextView) view.findViewById(R.id.sitelist_tv_name)).setText(getGroup(i).f3024a);
        }
    }

    public final int b(int i, int i2) {
        if (i2 < 0 && i < 0) {
            return 0;
        }
        if (i2 != -1 || this.b.isGroupExpanded(i)) {
            return i2 == getChildrenCount(i) + -1 ? 2 : 1;
        }
        return 0;
    }

    public final long getChildId(int i, int i2) {
        return (long) i2;
    }

    public final View getChildView(int i, int i2, boolean z, View view, ViewGroup viewGroup) {
        if (view == null) {
            aa aaVar = new aa((byte) 0);
            view = g.o().inflate((int) R.layout.listitem_sinacontactchild, (ViewGroup) null);
            aaVar.f709a = (TextView) view.findViewById(R.id.tv_name);
            aaVar.b = (TextView) view.findViewById(R.id.tv_operate);
            aaVar.c = (ImageView) view.findViewById(R.id.userlist_item_iv_face);
            aaVar.e = view.findViewById(R.id.iv_arrow);
            aaVar.f = (Button) view.findViewById(R.id.btn_operate);
            aaVar.d = (ImageView) view.findViewById(R.id.userlist_item_iv_face_cover_click);
            view.setTag(R.id.tag_userlist_item, aaVar);
        }
        e c2 = getChild(i, i2);
        aa aaVar2 = (aa) view.getTag(R.id.tag_userlist_item);
        aaVar2.f709a.setText(c2.d);
        ImageView imageView = aaVar2.c;
        String str = c2.g;
        String str2 = c2.c;
        imageView.setTag(R.id.tag_item_imageid, str2);
        if (c2.h == null && !a.a((CharSequence) str2) && !a.a((CharSequence) str) && !this.b.c()) {
            r rVar = new r(str2, new ab(this, c2), 6, null);
            rVar.a(str);
            u.b().execute(rVar);
        }
        imageView.setImageBitmap(c2.h == null ? g.t() : c2.h);
        if (this.c != 11) {
            if (this.c == 22) {
                switch (c2.b) {
                    case 1:
                        aaVar2.d.setOnClickListener(new v(this, aaVar2, i, i2));
                        aaVar2.e.setVisibility(0);
                        aaVar2.b.setVisibility(8);
                        aaVar2.f.setVisibility(0);
                        aaVar2.f.setFocusable(false);
                        if (c2.f) {
                            aaVar2.f.setEnabled(false);
                            aaVar2.f.setText("已邀请");
                        } else {
                            aaVar2.f.setEnabled(true);
                            aaVar2.f.setText("邀请");
                        }
                        aaVar2.d.setOnClickListener(new w(this, aaVar2, i, i2));
                        aaVar2.f.setOnClickListener(new x(this, aaVar2, i, i2));
                        break;
                    case 2:
                        aaVar2.e.setVisibility(0);
                        aaVar2.b.setVisibility(8);
                        aaVar2.f.setVisibility(0);
                        aaVar2.f.setFocusable(false);
                        if (c2.f) {
                            aaVar2.f.setEnabled(false);
                            aaVar2.f.setText("已邀请");
                        } else {
                            aaVar2.f.setEnabled(true);
                            aaVar2.f.setText("邀请");
                        }
                        aaVar2.d.setOnClickListener(new y(this, aaVar2, i, i2));
                        aaVar2.f.setOnClickListener(new z(this, aaVar2, i, i2));
                        break;
                    case 3:
                        aaVar2.f.setVisibility(8);
                        aaVar2.f.setFocusable(false);
                        aaVar2.e.setVisibility(0);
                        aaVar2.b.setVisibility(0);
                        aaVar2.d.setClickable(false);
                        aaVar2.b.setText((int) R.string.contact_relation3);
                        break;
                }
            }
        } else {
            switch (c2.b) {
                case 1:
                    aaVar2.b.setVisibility(8);
                    aaVar2.f.setVisibility(0);
                    aaVar2.f.setFocusable(false);
                    aaVar2.e.setVisibility(0);
                    if (c2.f) {
                        aaVar2.f.setEnabled(false);
                        aaVar2.f.setText((int) R.string.contact_relation11);
                    } else {
                        aaVar2.f.setEnabled(true);
                        aaVar2.f.setText((int) R.string.contact_relation1);
                    }
                    aaVar2.f.setOnClickListener(new r(this, aaVar2, i, i2));
                    aaVar2.d.setOnClickListener(new s(this, aaVar2, i, i2));
                    break;
                case 2:
                    aaVar2.e.setVisibility(0);
                    aaVar2.b.setVisibility(8);
                    aaVar2.f.setVisibility(0);
                    aaVar2.f.setFocusable(false);
                    if (c2.f) {
                        aaVar2.f.setEnabled(false);
                        aaVar2.f.setText("已邀请");
                    } else {
                        aaVar2.f.setEnabled(true);
                        aaVar2.f.setText("邀请");
                    }
                    aaVar2.d.setOnClickListener(new t(this, aaVar2, i, i2));
                    aaVar2.f.setOnClickListener(new u(this, aaVar2, i, i2));
                    break;
                case 3:
                    aaVar2.f.setVisibility(8);
                    aaVar2.f.setFocusable(false);
                    aaVar2.e.setVisibility(0);
                    aaVar2.b.setVisibility(0);
                    aaVar2.d.setClickable(false);
                    aaVar2.b.setText((int) R.string.contact_relation3);
                    break;
            }
        }
        aaVar2.d.setClickable(false);
        return view;
    }

    public final int getChildrenCount(int i) {
        if (i < 0 || i >= this.f911a.size()) {
            return 0;
        }
        if (((f) this.f911a.get(i)).b == null) {
            return 0;
        }
        return ((f) this.f911a.get(i)).b.size();
    }

    public final int getGroupCount() {
        return this.f911a.size();
    }

    public final long getGroupId(int i) {
        return (long) i;
    }

    public final View getGroupView(int i, boolean z, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = g.o().inflate((int) R.layout.listitem_contactgroup, (ViewGroup) null);
            ad adVar = new ad();
            adVar.f712a = (TextView) view.findViewById(R.id.sitelist_tv_name);
            view.setTag(R.id.tag_userlist_item, adVar);
        }
        ((ad) view.getTag(R.id.tag_userlist_item)).f712a.setText(getGroup(i).f3024a);
        return view;
    }

    public final boolean hasStableIds() {
        return true;
    }

    public final boolean isChildSelectable(int i, int i2) {
        return true;
    }
}
