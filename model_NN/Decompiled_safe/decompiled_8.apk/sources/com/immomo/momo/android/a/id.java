package com.immomo.momo.android.a;

import android.content.Intent;
import android.support.v4.b.a;
import com.immomo.momo.android.activity.tieba.PublishTieCommentActivity;
import com.immomo.momo.android.view.a.al;
import com.immomo.momo.android.view.a.n;
import com.immomo.momo.service.bean.c.c;
import mm.purchasesdk.PurchaseCode;

final class id implements al {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ hy f873a;
    private final /* synthetic */ String[] b;
    private final /* synthetic */ c c;

    id(hy hyVar, String[] strArr, c cVar) {
        this.f873a = hyVar;
        this.b = strArr;
        this.c = cVar;
    }

    public final void a(int i) {
        if ("回复".equals(this.b[i])) {
            Intent intent = new Intent(this.f873a.f, PublishTieCommentActivity.class);
            intent.putExtra("tiezi_id", this.c.i.f3017a);
            intent.putExtra("tiezi_name", this.c.i.d);
            intent.putExtra("tiecomment_tomomoid", this.c.c);
            intent.putExtra("tiecommen_tocommentid", this.c.f3018a);
            intent.putExtra("tiecommen_tousername", this.c.b.h());
            if (!a.a((CharSequence) this.c.getLoadImageId())) {
                intent.putExtra("tiecommen_tousercontent", String.valueOf(this.c.d) + "[图片]");
            } else {
                intent.putExtra("tiecommen_tousercontent", this.c.d);
            }
            this.f873a.f.startActivityForResult(intent, PurchaseCode.ORDER_OK);
        } else if ("举报".equals(this.b[i])) {
            hy.a(this.f873a, this.c, (int) PurchaseCode.QUERY_OK);
        } else if ("删除".equals(this.b[i])) {
            if (this.f873a.d.equals(this.c.c) || !this.f873a.e) {
                n.a(this.f873a.f, "确定删除评论吗？", new ie(this.f873a, this.c)).show();
            } else {
                hy.a(this.f873a, this.c, (int) PurchaseCode.ORDER_OK);
            }
        } else if ("删除并禁言".equals(this.b[i])) {
            hy.a(this.f873a, this.c, (int) PurchaseCode.UNSUB_OK);
        }
    }
}
