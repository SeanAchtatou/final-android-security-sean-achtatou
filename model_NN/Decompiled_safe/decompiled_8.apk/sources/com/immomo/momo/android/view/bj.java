package com.immomo.momo.android.view;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;

public class bj extends LinearLayout {

    /* renamed from: a  reason: collision with root package name */
    private int f2744a = R.layout.common_btn_loadmore;
    private CharSequence b = PoiTypeDef.All;
    private ImageView c = null;
    private TextView d = null;
    private LinearLayout e = null;
    private Animation f = null;

    public bj(Context context) {
        super(context);
        d();
    }

    public bj(Context context, int i) {
        super(context);
        this.f2744a = i;
        d();
    }

    public bj(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        d();
    }

    public bj(Context context, AttributeSet attributeSet, int i) {
        super(context);
        this.f2744a = i;
        d();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, com.immomo.momo.android.view.bj, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    private void d() {
        ((LayoutInflater) getContext().getSystemService("layout_inflater")).inflate(this.f2744a, (ViewGroup) this, true);
        this.c = (ImageView) findViewById(R.id.btn_iv_lefticon);
        findViewById(R.id.btn_iv_righticon);
        this.d = (TextView) findViewById(R.id.btn_tv_text);
        this.e = (LinearLayout) findViewById(R.id.btn_layout_container);
    }

    public final bj a(int i) {
        if (i <= 0) {
            this.c.setVisibility(8);
        } else {
            this.c.setVisibility(0);
            this.c.setImageResource(i);
        }
        return this;
    }

    public final bj a(CharSequence charSequence) {
        if (charSequence == null) {
            this.d.setVisibility(8);
        } else {
            this.b = charSequence;
            this.d.setVisibility(0);
            this.d.setText(this.b);
        }
        return this;
    }

    public final void a() {
        if (this.c != null) {
            ImageView imageView = this.c;
            if (imageView.getDrawable() != null) {
                imageView.clearAnimation();
            }
        }
    }

    public final void b() {
        if (this.c != null) {
            ImageView imageView = this.c;
            if (imageView.getDrawable() != null) {
                if (this.f == null) {
                    this.f = AnimationUtils.loadAnimation(getContext(), R.anim.loading);
                }
                imageView.startAnimation(this.f);
            }
        }
    }

    public final bj c() {
        this.b = getContext().getString(R.string.pull_to_refresh_refreshing_label);
        this.d.setVisibility(0);
        this.d.setText(this.b);
        return this;
    }

    public CharSequence getText() {
        return this.b;
    }

    public void setBackgroundColor(int i) {
        this.e.setBackgroundColor(i);
    }

    public void setBackgroundDrawable(Drawable drawable) {
        this.e.setBackgroundDrawable(drawable);
    }

    public void setBackgroundResource(int i) {
        this.e.setBackgroundResource(i);
    }

    public void setButtonOnClickListener(View.OnClickListener onClickListener) {
        this.e.setOnClickListener(onClickListener);
    }

    public void setEnabled(boolean z) {
        this.e.setEnabled(z);
    }

    public void setLeftVisibility(int i) {
        this.c.setVisibility(i);
    }

    public void setRightVisibility(int i) {
        this.c.setVisibility(i);
    }

    public void setTextColor(int i) {
        this.d.setTextColor(i);
    }
}
