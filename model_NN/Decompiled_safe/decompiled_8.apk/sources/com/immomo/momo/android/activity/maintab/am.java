package com.immomo.momo.android.activity.maintab;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.protocol.a.n;
import com.immomo.momo.service.bean.ay;
import java.util.List;

final class am extends d {

    /* renamed from: a  reason: collision with root package name */
    private double f1838a;
    private double c;
    private /* synthetic */ x d;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public am(x xVar, Context context, double d2, double d3) {
        super(context);
        this.d = xVar;
        this.f1838a = d2;
        this.c = d3;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        List<ay> a2 = n.a().a(this.d.R.getGroupCount(), this.f1838a, this.c, this.d.M.aH);
        if (a2 != null) {
            for (ay ayVar : a2) {
                this.d.U.a(ayVar);
                if (ayVar.g != null) {
                    this.d.T.a(ayVar.g);
                }
            }
        }
        return a2;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        List<ay> list = (List) obj;
        if (list == null) {
            return;
        }
        if (!list.isEmpty()) {
            for (ay ayVar : list) {
                if (this.d.O.get(ayVar.f3001a) == null) {
                    this.d.Q.add(ayVar);
                    this.d.O.put(ayVar.f3001a, ayVar);
                } else {
                    this.b.b((Object) ("重复..." + ayVar.f3001a));
                }
            }
            this.d.R.notifyDataSetChanged();
            this.d.R.a();
            this.d.af.setVisibility(0);
            return;
        }
        this.d.af.setVisibility(8);
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.d.X = (d) null;
        this.d.af.e();
    }
}
