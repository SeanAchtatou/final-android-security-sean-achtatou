package com.immomo.momo.android.activity.common;

import android.content.Context;
import android.support.v4.b.a;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.h;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

final class aw extends d {

    /* renamed from: a  reason: collision with root package name */
    private v f1091a = null;
    private String c;
    private String d;
    private HashMap e;
    private /* synthetic */ InviteToDiscussTabsActivity f;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public aw(InviteToDiscussTabsActivity inviteToDiscussTabsActivity, Context context, HashMap hashMap, String str) {
        super(context);
        this.f = inviteToDiscussTabsActivity;
        this.d = str;
        this.e = hashMap;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        ArrayList arrayList = new ArrayList();
        for (Map.Entry key : this.e.entrySet()) {
            arrayList.add((String) key.getKey());
        }
        this.c = a.a(arrayList, ",");
        h.a().b(this.c, this.d);
        return null;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
        this.f1091a = new v(this.f);
        this.f1091a.a("请求提交中...");
        this.f1091a.setCancelable(true);
        this.f1091a.setOnCancelListener(new ax(this));
        this.f.a(this.f1091a);
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        super.a(exc);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        this.f.setResult(-1);
        this.f.finish();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        super.b();
        this.f.p();
    }
}
