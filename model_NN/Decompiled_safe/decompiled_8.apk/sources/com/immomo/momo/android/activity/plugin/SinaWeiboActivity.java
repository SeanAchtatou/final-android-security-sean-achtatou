package com.immomo.momo.android.activity.plugin;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.b.a;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.a.jo;
import com.immomo.momo.android.activity.ah;
import com.immomo.momo.android.c.r;
import com.immomo.momo.android.view.HandyListView;
import com.immomo.momo.android.view.HeaderLayout;
import com.immomo.momo.android.view.LoadingButton;
import com.immomo.momo.android.view.bi;
import com.immomo.momo.android.view.bl;
import com.immomo.momo.g;
import com.immomo.momo.plugin.e.e;

public class SinaWeiboActivity extends ah implements View.OnClickListener, bl {
    private View A = null;
    private TextView B = null;
    /* access modifiers changed from: private */
    public View C = null;
    /* access modifiers changed from: private */
    public LoadingButton D = null;
    private bi E = null;
    private AsyncTask F = null;
    private AsyncTask G = null;
    /* access modifiers changed from: private */
    public Handler H = new Handler();
    /* access modifiers changed from: private */
    public int h = 1;
    /* access modifiers changed from: private */
    public jo i = null;
    /* access modifiers changed from: private */
    public String j = PoiTypeDef.All;
    /* access modifiers changed from: private */
    public String k = PoiTypeDef.All;
    /* access modifiers changed from: private */
    public e l = null;
    private View m = null;
    private HeaderLayout n = null;
    /* access modifiers changed from: private */
    public HandyListView o = null;
    /* access modifiers changed from: private */
    public ImageView p = null;
    private TextView q = null;
    private TextView r = null;
    private TextView s = null;
    private View t = null;
    private TextView u = null;
    private View v = null;
    private ImageView w = null;
    private View x = null;
    private TextView y = null;
    private TextView z = null;

    /* access modifiers changed from: private */
    public void v() {
        if (this.l != null) {
            if (this.l != null) {
                this.e.a(this.l);
                this.j = new StringBuilder(String.valueOf(this.l.f2875a)).toString();
                this.q.setText(this.l.b);
                this.y.setText(this.l.d);
                this.z.setText(this.l.f);
                if (this.l.n) {
                    this.A.setVisibility(0);
                    this.B.setText(this.l.o);
                    this.q.setCompoundDrawablesWithIntrinsicBounds(0, 0, (int) R.drawable.ic_user_sinav, 0);
                } else {
                    this.A.setVisibility(8);
                    this.q.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                }
                if ("f".equalsIgnoreCase(this.l.j)) {
                    this.w.setBackgroundResource(R.drawable.bg_gender_famal_weibo);
                    this.w.setImageResource(R.drawable.ic_user_famale);
                } else if ("m".equalsIgnoreCase(this.l.j)) {
                    this.w.setBackgroundResource(R.drawable.bg_gender_male_weibo);
                    this.w.setImageResource(R.drawable.ic_user_male);
                } else {
                    this.w.setVisibility(8);
                }
                this.r.setText(new StringBuilder(String.valueOf(this.l.l)).toString());
                if (this.l.m < 100000) {
                    this.u.setText(new StringBuilder(String.valueOf(this.l.m)).toString());
                    this.v.setVisibility(8);
                } else {
                    this.u.setText(new StringBuilder(String.valueOf(this.l.m / 10000)).toString());
                    this.v.setVisibility(0);
                }
                if (this.l.k < 100000) {
                    this.s.setText(new StringBuilder(String.valueOf(this.l.k)).toString());
                    this.t.setVisibility(8);
                } else {
                    this.s.setText(new StringBuilder(String.valueOf(this.l.k / 10000)).toString());
                    this.t.setVisibility(0);
                }
                this.n.a(this.E, new bn(this));
                if (!a.a((CharSequence) this.l.h) && this.l.h.indexOf("/") > 0) {
                    r rVar = new r(a.n(this.l.h), new bo(this), 5, null);
                    rVar.a(this.l.h.replace("/50/", "/180/"));
                    new Thread(rVar).start();
                    this.x.setOnClickListener(new bq(this));
                }
            }
            this.G = new bs(this, this).execute(new Object[0]);
            return;
        }
        findViewById(R.id.process_layout_root).setVisibility(8);
    }

    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        super.a(bundle);
        setContentView((int) R.layout.activity_plus_sinaweibo);
        this.n = (HeaderLayout) findViewById(R.id.layout_header);
        this.n.setTitleText((int) R.string.plus_sinaweibo);
        this.E = new bi(this);
        this.E.a("访问微博");
        this.E.a((int) R.drawable.ic_topbar_link);
        this.o = (HandyListView) findViewById(R.id.weibo_list);
        this.C = g.o().inflate((int) R.layout.common_list_loadmore, (ViewGroup) null);
        this.C.setVisibility(8);
        this.D = (LoadingButton) this.C.findViewById(R.id.btn_loadmore);
        this.D.setNormalIconResId(R.drawable.ic_btn_inner_clock);
        this.m = g.o().inflate((int) R.layout.include_weibo_prifile, (ViewGroup) null);
        this.p = (ImageView) this.m.findViewById(R.id.weibo_iv_avator);
        this.p.setEnabled(false);
        this.q = (TextView) this.m.findViewById(R.id.weibo_tv_name);
        this.r = (TextView) this.m.findViewById(R.id.weibo_tv_followcount);
        this.s = (TextView) this.m.findViewById(R.id.weibo_tv_fanscount);
        this.t = this.m.findViewById(R.id.weibo_tv_fanscount_w);
        this.u = (TextView) this.m.findViewById(R.id.weibo_tv_weibocount);
        this.v = this.m.findViewById(R.id.weibo_tv_weibocount_w);
        this.w = (ImageView) this.m.findViewById(R.id.weibo_iv_gender);
        this.x = this.m.findViewById(R.id.weibo_iv_cover);
        this.y = (TextView) this.m.findViewById(R.id.weibo_tv_location);
        this.z = (TextView) this.m.findViewById(R.id.weibo_tv_sign);
        this.A = this.m.findViewById(R.id.weibo_layout_vip);
        this.B = (TextView) this.m.findViewById(R.id.weibo_tv_vip);
        this.p.setOnClickListener(this);
        this.D.setOnProcessListener(this);
        d();
    }

    /* access modifiers changed from: protected */
    public final void d() {
        this.o.addHeaderView(this.m);
        this.o.addFooterView(this.C);
        this.C.setVisibility(8);
        this.i = new jo(this, this.o);
        this.o.setAdapter((ListAdapter) this.i);
        if (getIntent().getExtras() == null) {
            finish();
        } else if (getIntent().getExtras().get("momoid") != null) {
            this.k = (String) getIntent().getExtras().get("momoid");
            this.F = new br(this, this).execute(new Object[0]);
        } else if (getIntent().getExtras().get("uid") != null) {
            this.j = (String) getIntent().getExtras().get("uid");
            this.F = new br(this, this).execute(new Object[0]);
        } else if (((e) getIntent().getExtras().get("profile")) != null) {
            this.l = (e) getIntent().getExtras().get("profile");
            v();
        } else {
            finish();
        }
    }

    public void onClick(View view) {
        switch (view.getId()) {
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.F != null && !this.F.isCancelled()) {
            this.F.cancel(true);
        }
        if (this.G != null && !this.G.isCancelled()) {
            this.G.cancel(true);
        }
    }

    public final void u() {
        new bs(this, this).execute(new Object[0]);
    }
}
