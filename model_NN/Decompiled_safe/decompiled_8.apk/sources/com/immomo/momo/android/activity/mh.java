package com.immomo.momo.android.activity;

import android.content.Context;
import com.baidu.location.LocationClientOption;
import com.immomo.momo.android.c.d;
import com.immomo.momo.protocol.a.w;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import mm.purchasesdk.PurchaseCode;

final class mh extends d {

    /* renamed from: a  reason: collision with root package name */
    private long f2000a = 0;
    private StringBuilder c = null;
    private StringBuilder d = null;
    private /* synthetic */ UserRoamActivity e;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public mh(UserRoamActivity userRoamActivity, Context context, long j) {
        super(context);
        this.e = userRoamActivity;
        if (userRoamActivity.A != null) {
            userRoamActivity.A.cancel(true);
        }
        userRoamActivity.A = this;
        this.f2000a = j;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        ArrayList arrayList = new ArrayList();
        this.c = new StringBuilder();
        this.d = new StringBuilder();
        this.e.s = w.a().a(arrayList, ((int) this.f2000a) / LocationClientOption.MIN_SCAN_SPAN, this.c, this.d, this.e.e.n.a(), this.e.e.o.a());
        this.e.r.a(arrayList);
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        super.a(exc);
        this.e.g();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.android.a.a.a(java.util.Collection, boolean):void
     arg types: [java.util.List, int]
     candidates:
      com.immomo.momo.android.a.a.a(int, java.lang.Object):void
      com.immomo.momo.android.a.a.a(int, java.util.Collection):void
      com.immomo.momo.android.a.a.a(java.lang.Object, int):void
      com.immomo.momo.android.a.a.a(java.util.Collection, boolean):void */
    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        List list = (List) obj;
        if (!this.e.C && this.e.h.isShown()) {
            this.e.E.removeMessages(100);
            this.e.z = 0;
            this.e.a(this.e.s);
            this.e.v = this.c.toString();
            this.e.w = this.d.toString();
            this.e.E.sendEmptyMessageDelayed(PurchaseCode.QUERY_OK, 300);
            this.e.q.a(false);
            this.e.q.a((Collection) list, false);
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.e.A = (mh) null;
    }
}
