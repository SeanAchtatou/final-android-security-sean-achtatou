package com.immomo.momo.android.activity.message;

import android.content.BroadcastReceiver;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Bundle;
import android.support.v4.b.a;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ListAdapter;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.a.a.af;
import com.immomo.momo.android.a.a.d;
import com.immomo.momo.android.activity.discuss.DiscussProfileActivity;
import com.immomo.momo.android.broadcast.i;
import com.immomo.momo.android.broadcast.t;
import com.immomo.momo.android.broadcast.w;
import com.immomo.momo.android.c.u;
import com.immomo.momo.android.view.EmoteEditeText;
import com.immomo.momo.android.view.a.at;
import com.immomo.momo.service.ag;
import com.immomo.momo.service.aq;
import com.immomo.momo.service.bean.Message;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.service.bean.n;
import com.immomo.momo.service.bean.o;
import com.immomo.momo.service.g;
import com.immomo.momo.service.h;
import com.immomo.momo.util.k;
import com.immomo.momo.util.m;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ThreadPoolExecutor;
import mm.purchasesdk.PurchaseCode;
import org.json.JSONObject;

public class MultiChatActivity extends a {
    protected m L = new m("test_momo", "[--- from MultiChatActivity ---]");
    /* access modifiers changed from: private */
    public aq M;
    private g N;
    /* access modifiers changed from: private */
    public h O;
    /* access modifiers changed from: private */
    public n P;
    private w Q = null;
    private i R = null;
    private t S = null;
    /* access modifiers changed from: private */
    public af T;
    private ThreadPoolExecutor U = new u(1, 2);
    /* access modifiers changed from: private */
    public o V = null;
    /* access modifiers changed from: private */
    public boolean W = false;
    private at X = null;

    public MultiChatActivity() {
        this.L.a((Object) "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~MessageActivity created....");
    }

    private void a(BroadcastReceiver broadcastReceiver) {
        if (broadcastReceiver != null) {
            unregisterReceiver(broadcastReceiver);
        }
    }

    /* access modifiers changed from: private */
    public void ag() {
        setTitle(this.P.a());
    }

    /* access modifiers changed from: private */
    public List ah() {
        ArrayList arrayList = (ArrayList) this.N.b(this.P.f, this.T.getCount(), 21);
        if (arrayList.size() > 20) {
            arrayList.remove(0);
            this.W = true;
        } else {
            this.W = false;
        }
        Iterator it = arrayList.iterator();
        boolean z = false;
        while (it.hasNext()) {
            Message message = (Message) it.next();
            if (message.receive) {
                if (message.status == 5 || message.status == 9 || message.status == 13) {
                    this.F.add(message.msgId);
                    if (message.status == 5) {
                        z = true;
                    }
                }
                i(message);
                message.status = 4;
            } else if (message.status == 8) {
                com.immomo.momo.android.b.m.a(message.msgId).a(new x(this, message));
            }
        }
        if (this.T.isEmpty() && z) {
            com.immomo.momo.g.d().r();
        }
        ai();
        return arrayList;
    }

    private void ai() {
        if (this.F != null && this.F.size() > 0) {
            String[] strArr = (String[]) this.F.toArray(new String[0]);
            this.N.a(strArr);
            com.immomo.momo.g.d().a(this.P.f, strArr, 3);
            this.F.clear();
        }
    }

    private void h(Message message) {
        if (message != null) {
            this.L.b((Object) ("dealMesssage message:" + message));
            if (message.receive) {
                message.status = 4;
            }
            i(message);
        }
    }

    static /* synthetic */ void i(MultiChatActivity multiChatActivity) {
        View inflate = com.immomo.momo.g.o().inflate((int) R.layout.dialog_contactpeople_apply, (ViewGroup) null);
        EmoteEditeText emoteEditeText = (EmoteEditeText) inflate.findViewById(R.id.edittext_reason);
        if (multiChatActivity.P != null) {
            emoteEditeText.setText(multiChatActivity.P.a());
        } else {
            emoteEditeText.setText(multiChatActivity.i);
        }
        emoteEditeText.addTextChangedListener(new com.immomo.momo.util.aq(24));
        emoteEditeText.setHint((int) R.string.dprofile_editname_hint);
        emoteEditeText.setSelection(emoteEditeText.getText().toString().length());
        com.immomo.momo.android.view.a.n nVar = new com.immomo.momo.android.view.a.n(multiChatActivity);
        nVar.setTitle("修改名称");
        nVar.setContentView(inflate);
        nVar.a();
        nVar.a(0, multiChatActivity.getString(R.string.dialog_btn_confim), new cl(multiChatActivity, emoteEditeText));
        nVar.a(1, multiChatActivity.getString(R.string.dialog_btn_cancel), new cm());
        nVar.getWindow().setSoftInputMode(4);
        nVar.show();
    }

    private void i(Message message) {
        if (message.remoteUser == null) {
            if (!a.a((CharSequence) message.remoteId)) {
                message.remoteUser = this.M.b(message.remoteId);
                if (message.remoteUser == null) {
                    message.remoteUser = new bf(message.remoteId);
                    this.U.execute(new cs(this, message));
                }
            } else {
                message.remoteUser = new bf();
            }
        }
        message.remoteUser.setImageMultipleDiaplay(true);
    }

    /* access modifiers changed from: protected */
    public final void O() {
        D();
        this.k = new ag();
        this.M = new aq();
        this.N = new g();
        this.O = new h();
        this.z = (InputMethodManager) getSystemService("input_method");
        this.A = (AudioManager) getSystemService("audio");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.service.h.a(java.lang.String, boolean):com.immomo.momo.service.bean.n
     arg types: [java.lang.String, int]
     candidates:
      com.immomo.momo.service.h.a(com.immomo.momo.service.bean.n, java.lang.String):void
      com.immomo.momo.service.h.a(java.lang.String, int):void
      com.immomo.momo.service.h.a(java.lang.String, java.lang.String):void
      com.immomo.momo.service.h.a(java.lang.String, java.lang.String[]):void
      com.immomo.momo.service.h.a(java.util.List, java.lang.String):void
      com.immomo.momo.service.h.a(java.lang.String, boolean):com.immomo.momo.service.bean.n */
    /* access modifiers changed from: protected */
    public final void P() {
        this.f.setImageMultipleDiaplay(true);
        this.P = this.O.a(u(), false);
        if (this.P == null) {
            this.P = new n(getIntent().getStringExtra("remoteDiscussID"));
            this.P.b = this.P.f;
            new Thread(new cj(this)).start();
        }
        ag();
        this.V = this.g.d(this.P.f);
    }

    /* access modifiers changed from: protected */
    public final void Q() {
        this.T = new af(this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.android.a.a.aa.a(int, java.util.Collection):void
     arg types: [int, java.util.List]
     candidates:
      com.immomo.momo.android.a.a.a(int, java.lang.Object):void
      com.immomo.momo.android.a.a.a(java.lang.Object, int):void
      com.immomo.momo.android.a.a.a(java.util.Collection, boolean):void
      com.immomo.momo.android.a.a.aa.a(int, java.util.Collection):void */
    /* access modifiers changed from: protected */
    public final void R() {
        aa();
        this.T.b();
        this.T.a(0, (Collection) ah());
        if (!this.W) {
            this.l.a();
        }
        this.N.a(this.P.f);
        this.l.setAdapter((ListAdapter) this.T);
    }

    /* access modifiers changed from: protected */
    public final void S() {
        a(800, "actions.discuss", "actions.message.status", "actions.emoteupdates", "actions.dlocalmsg");
        this.R = new i(this);
        this.Q = new w(this);
        this.S = new t(this);
        this.S.a(new ch(this));
        this.R.a(new ci(this));
    }

    /* access modifiers changed from: protected */
    public final void T() {
        ai();
        com.immomo.momo.g.d().r();
    }

    /* access modifiers changed from: protected */
    public final int U() {
        return R.layout.activity_multi_chat;
    }

    /* access modifiers changed from: protected */
    public final void V() {
        if (this.X == null || !this.X.g()) {
            A();
            z();
            String[] strArr = this.f.h.equals(this.P.c) ? new String[]{PoiTypeDef.All, "语音收听方式", "邀请好友加入", "修改对话名称", "设置聊天背景"} : new String[]{PoiTypeDef.All, "语音收听方式", "邀请好友加入", "设置聊天背景"};
            if (this.V == null || this.V.f3033a) {
                strArr[0] = "关闭提醒";
            } else {
                strArr[0] = "开启提醒";
            }
            this.X = new at(this, this.n, strArr);
            this.X.a(at.b);
            this.X.a(new ck(this, strArr));
            this.X.d();
        }
    }

    /* access modifiers changed from: protected */
    public final void W() {
        Intent intent = new Intent();
        intent.setClass(this, DiscussProfileActivity.class);
        intent.putExtra("tag", "local");
        intent.putExtra("did", getIntent().getStringExtra("remoteDiscussID"));
        intent.setFlags(603979776);
        startActivity(intent);
    }

    /* access modifiers changed from: protected */
    public final void X() {
        this.W = false;
        b(new cr(this, this));
    }

    public final void Y() {
        this.j.sendEmptyMessage(PurchaseCode.BILL_DIALOG_SHOWERROR);
    }

    /* access modifiers changed from: protected */
    public final void Z() {
        this.E = this.O.d(this.f.h, this.P.f) && this.P.g != 3;
        this.L.b((Object) ("bothRelation:" + this.E));
        if (this.E) {
            M();
            return;
        }
        L();
        new Thread(new cn(this)).start();
    }

    /* access modifiers changed from: protected */
    public final Message a(File file) {
        ce.a();
        return ce.a(file, this.f, this.P.f, 3, this.f.b() ? 1 : 0);
    }

    public final boolean a(Bundle bundle, String str) {
        if (!"actions.discuss".equals(str)) {
            if ("actions.message.status".equals(str)) {
                if (bundle.getInt("chattype") != 3) {
                    return false;
                }
                if (!this.P.f.equals(bundle.getString("discussid"))) {
                    this.L.b((Object) "return false");
                    return false;
                }
                String string = bundle.getString("msgid");
                String string2 = bundle.getString("stype");
                int f = this.T.f(new Message(string));
                this.L.a((Object) ("position:" + f));
                if (f >= 0) {
                    Message message = (Message) this.T.getItem(f);
                    if ("msgsuccess".equals(string2)) {
                        message.status = 2;
                    } else if ("msgsending".equals(string2)) {
                        message.status = 1;
                        message.fileName = this.N.c(string).fileName;
                    } else if ("msgfailed".equals(string2)) {
                        message.status = 3;
                    }
                    Y();
                }
            } else if (str.equals("actions.emoteupdates")) {
                Y();
            } else if (str.equals("actions.dlocalmsg")) {
                if (!this.P.f.equals(bundle.getString("discussid"))) {
                    return false;
                }
                Message message2 = (Message) bundle.getSerializable("messageobj");
                if (message2.receive) {
                    h(message2);
                }
                a(this.T, message2);
                return true;
            }
            return super.a(bundle, str);
        } else if (!this.P.f.equals(bundle.getString("discussid"))) {
            return false;
        } else {
            List<Message> list = (List) bundle.getSerializable("messagearray");
            if (list == null || list.isEmpty()) {
                return false;
            }
            for (Message message3 : list) {
                String str2 = message3.msgId;
                if (message3.contentType != 5 && message3.receive) {
                    this.F.add(str2);
                }
                h(message3);
            }
            if (q()) {
                ai();
            }
            a(this.T, list);
            return q();
        }
    }

    /* access modifiers changed from: protected */
    public final boolean a(Message message) {
        this.L.b((Object) ("onAudioCompleted " + message.msgId));
        int f = this.T.f(message) + 1;
        if (f < this.T.getCount()) {
            Message message2 = (Message) this.T.getItem(f);
            if (message2.receive && message2.contentType == 4 && !message2.isAudioPlayed) {
                d.a(message2, this);
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public final void aa() {
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("discuss_id", u());
            jSONObject.put("number", this.N.f(this.i));
            new k("PI", "P2d", jSONObject).e();
        } catch (Exception e) {
        }
    }

    /* access modifiers changed from: protected */
    public final void ab() {
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("discuss_id", u());
            jSONObject.put("number", this.N.f(this.i));
            new k("PO", "P2d", jSONObject).e();
        } catch (Exception e) {
        }
    }

    /* access modifiers changed from: protected */
    public final List ac() {
        return this.N.h(this.P.f);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.service.bean.Message.<init>(boolean, int):void
     arg types: [int, int]
     candidates:
      com.immomo.momo.service.bean.Message.<init>(int, boolean):void
      com.immomo.momo.service.bean.Message.<init>(boolean, int):void */
    /* access modifiers changed from: protected */
    public final Message ad() {
        int i = 1;
        Message message = new Message(true, 2);
        message.remoteId = this.f.h;
        message.distance = this.f.d();
        ce a2 = ce.a();
        x xVar = new x(this, message);
        String str = this.P.f;
        if (!this.f.b()) {
            i = 0;
        }
        a2.a(message, xVar, str, 3, i);
        return message;
    }

    public final void b(Message message) {
        this.T.c(message);
        this.N.c(message);
    }

    /* access modifiers changed from: protected */
    public final Message c(int i) {
        return ce.a().a(this.H, i, this.f, this.P.f, 3, this.f.b() ? 1 : 0);
    }

    /* access modifiers changed from: protected */
    public final Message c(String str) {
        ce.a();
        return ce.a(str, this.f, this.P.f, 3, this.f.b() ? 1 : 0);
    }

    /* access modifiers changed from: protected */
    public final void c(Message message) {
        this.N.a(message.msgId, message.status);
    }

    /* access modifiers changed from: protected */
    public final Message d(String str) {
        ce.a();
        return ce.b(str, this.f, this.P.f, 3, this.f.b() ? 1 : 0);
    }

    /* access modifiers changed from: protected */
    public final void d() {
        super.d();
        e(this.P.l);
    }

    /* access modifiers changed from: protected */
    public final void d(Message message) {
        this.N.b(message);
    }

    /* access modifiers changed from: protected */
    public final void e() {
        super.e();
        this.L.a((Object) "~~~~~~~~~~~~~~~~~~~~~~onInitialize!!!");
    }

    /* access modifiers changed from: protected */
    public final void f(Message message) {
        if (message != null) {
            if (this.K) {
                this.K = false;
                new k("C", "C5301").e();
            }
            this.T.a(message);
            super.f(message);
        }
    }

    public boolean handleMessage(android.os.Message message) {
        switch (message.what) {
            case PurchaseCode.BILL_DIALOG_SHOWERROR /*402*/:
                this.T.notifyDataSetChanged();
                return true;
            case 10002:
                return true;
            default:
                return super.handleMessage(message);
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i2 == -1 && i == 264) {
            String stringExtra = intent.getStringExtra("key_resourseid");
            e(stringExtra);
            this.P.l = stringExtra;
            this.O.a(stringExtra, this.i);
            return;
        }
        super.onActivityResult(i, i2, intent);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        ab();
        if (r()) {
            a(this.R);
            a(this.Q);
            a(this.S);
        }
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        this.L.a((Object) "onStart");
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        this.L.a((Object) "onStop...");
        if (r()) {
            Bundle bundle = new Bundle();
            bundle.putString("sessionid", this.P.f);
            bundle.putInt("sessiontype", 6);
            com.immomo.momo.g.d().a(bundle, "action.sessionchanged");
        }
    }

    /* access modifiers changed from: protected */
    public final String u() {
        return getIntent().getStringExtra("remoteDiscussID");
    }
}
