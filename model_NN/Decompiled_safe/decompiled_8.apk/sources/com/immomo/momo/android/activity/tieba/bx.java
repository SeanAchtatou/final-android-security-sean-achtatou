package com.immomo.momo.android.activity.tieba;

import com.immomo.momo.android.view.MGifImageView;
import com.immomo.momo.android.view.au;
import java.io.File;

final class bx implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ bw f2198a;
    private final /* synthetic */ File b;
    private final /* synthetic */ boolean c;
    private final /* synthetic */ MGifImageView d;

    bx(bw bwVar, File file, boolean z, MGifImageView mGifImageView) {
        this.f2198a = bwVar;
        this.b = file;
        this.c = z;
        this.d = mGifImageView;
    }

    public final void run() {
        if (this.b != null && this.b.exists()) {
            this.f2198a.f2197a.R = new au(this.c ? 1 : 2);
            this.f2198a.f2197a.R.a(this.b, this.d);
            this.f2198a.f2197a.R.b(20);
            this.d.setGifDecoder(this.f2198a.f2197a.R);
        }
    }
}
