package com.immomo.momo.android.activity.plugin;

import android.graphics.Bitmap;
import com.immomo.momo.R;

final class bp implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ bo f2070a;
    private final /* synthetic */ Bitmap b;

    bp(bo boVar, Bitmap bitmap) {
        this.f2070a = boVar;
        this.b = bitmap;
    }

    public final void run() {
        if (this.b != null) {
            this.f2070a.f2069a.p.setEnabled(true);
            this.f2070a.f2069a.p.setImageBitmap(this.b);
            return;
        }
        this.f2070a.f2069a.p.setEnabled(false);
        this.f2070a.f2069a.p.setImageResource(R.drawable.ic_common_def_header);
    }
}
