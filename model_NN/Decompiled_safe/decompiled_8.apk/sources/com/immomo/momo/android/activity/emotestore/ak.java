package com.immomo.momo.android.activity.emotestore;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.protocol.a.i;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

final class ak extends d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ae f1289a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ak(ae aeVar, Context context) {
        super(context);
        this.f1289a = aeVar;
        if (aeVar.T != null) {
            aeVar.T.cancel(true);
        }
        aeVar.T = this;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        ArrayList arrayList = new ArrayList();
        i.a().c(arrayList, this.f1289a.R.getCount());
        this.f1289a.Q.b(arrayList);
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public final void a() {
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        List list = (List) obj;
        if (list.size() < 30) {
            this.f1289a.P.setVisibility(8);
        } else {
            this.f1289a.P.setVisibility(0);
        }
        this.f1289a.R.b((Collection) list);
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.f1289a.P.e();
    }
}
