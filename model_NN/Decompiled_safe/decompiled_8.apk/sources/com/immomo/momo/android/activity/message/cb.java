package com.immomo.momo.android.activity.message;

import android.content.DialogInterface;
import java.util.List;

final class cb implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ca f1962a;
    private final /* synthetic */ List b;

    cb(ca caVar, List list) {
        this.f1962a = caVar;
        this.b = list;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f1962a.f1961a.q.a().removeAll(this.b);
        this.f1962a.f1961a.q.notifyDataSetChanged();
        this.f1962a.f1961a.k.a(this.b);
        this.f1962a.f1961a.v.e();
    }
}
