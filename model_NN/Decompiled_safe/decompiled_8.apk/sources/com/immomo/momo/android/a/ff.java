package com.immomo.momo.android.a;

import android.content.Context;
import android.support.v4.b.a;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.n;
import com.immomo.momo.service.bean.a.j;

final class ff extends d {

    /* renamed from: a  reason: collision with root package name */
    private v f817a = null;
    private j c;
    private String d;
    private /* synthetic */ es e;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ff(es esVar, Context context, j jVar, String str) {
        super(context);
        this.e = esVar;
        this.c = jVar;
        this.d = str;
        this.f817a = new v(context);
        this.f817a.setCancelable(true);
        this.f817a.setOnCancelListener(new fg(this));
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        return n.a().b(this.d, this.c.f2979a);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
        this.e.d.a(this.f817a);
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        super.a(exc);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        String str = (String) obj;
        if (!a.a((CharSequence) str)) {
            a(str);
        }
        this.e.b.h();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        super.b();
        this.e.d.p();
    }
}
