package com.immomo.momo.android.a;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.service.bean.GameApp;
import com.immomo.momo.service.bean.aj;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.util.j;
import java.util.ArrayList;
import java.util.List;

public final class gv extends BaseAdapter {

    /* renamed from: a  reason: collision with root package name */
    private List f850a = new ArrayList();
    private Context b;
    private bf c;

    public gv(Context context) {
        this.b = context;
    }

    /* renamed from: a */
    public final gw getItem(int i) {
        if (this.f850a == null) {
            return null;
        }
        return (gw) this.f850a.get(i);
    }

    public final void a(bf bfVar) {
        this.f850a.clear();
        this.c = bfVar;
        if (bfVar != null) {
            if (bfVar.an) {
                gw gwVar = new gw();
                gwVar.b = 1;
                this.f850a.add(gwVar);
            }
            if (bfVar.at) {
                gw gwVar2 = new gw();
                gwVar2.b = 2;
                this.f850a.add(gwVar2);
            }
            if (bfVar.ar) {
                gw gwVar3 = new gw();
                gwVar3.b = 3;
                this.f850a.add(gwVar3);
            }
        }
        if (bfVar.O != null) {
            for (GameApp gameApp : bfVar.O) {
                gw gwVar4 = new gw();
                gwVar4.b = 4;
                gwVar4.f851a = gameApp;
                this.f850a.add(gwVar4);
            }
        }
        notifyDataSetChanged();
    }

    public final int getCount() {
        if (this.f850a == null) {
            return 0;
        }
        return this.f850a.size();
    }

    public final long getItemId(int i) {
        return (long) i;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup, int, boolean, boolean, int):void
     arg types: [com.immomo.momo.service.bean.al, android.widget.ImageView, ?[OBJECT, ARRAY], int, int, int, int]
     candidates:
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, int, android.view.View, android.view.ViewGroup, boolean, int, boolean):com.immomo.momo.android.c.o
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup, int, boolean, boolean, int):void */
    public final View getView(int i, View view, ViewGroup viewGroup) {
        gx gxVar;
        if (view == null || view.getTag() == null) {
            gxVar = new gx();
            view = LayoutInflater.from(this.b).inflate((int) R.layout.listitem_icon, (ViewGroup) null);
            gxVar.f852a = (ImageView) view.findViewById(R.id.profileicon_image_icon);
            gxVar.b = (TextView) view.findViewById(R.id.profileicon_tv_name);
            view.setTag(gxVar);
        } else {
            gxVar = (gx) view.getTag();
        }
        gw a2 = getItem(i);
        if (a2.b == 1) {
            if (this.c == null || !this.c.ap) {
                gxVar.f852a.setImageResource(R.drawable.ic_userpro_sina_large);
            } else {
                gxVar.f852a.setImageResource(R.drawable.ic_userpro_sinav_large);
            }
            gxVar.b.setText("新浪微博");
        } else if (a2.b == 2) {
            if (this.c == null || !this.c.au) {
                gxVar.f852a.setImageResource(R.drawable.ic_userpro_tweibo_large);
            } else {
                gxVar.f852a.setImageResource(R.drawable.ic_userpro_tweibov_large);
            }
            gxVar.b.setText("腾讯微博");
        } else if (a2.b == 3) {
            gxVar.f852a.setImageResource(R.drawable.ic_userpro_renren_large);
            gxVar.b.setText("人人网");
        } else {
            gxVar.b.setText(a2.f851a.appname);
            j.a((aj) a2.f851a.appIconLoader(), gxVar.f852a, (ViewGroup) null, 5, false, true, 5);
        }
        return view;
    }
}
