package com.immomo.momo.android.activity.emotestore;

import android.graphics.Matrix;

final class o implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ EmotionProfileActivity f1327a;

    o(EmotionProfileActivity emotionProfileActivity) {
        this.f1327a = emotionProfileActivity;
    }

    public final void run() {
        Matrix matrix = new Matrix();
        matrix.postTranslate((float) this.f1327a.m.getMeasuredWidth(), 300.0f);
        this.f1327a.m.setImageMatrix(matrix);
    }
}
