package com.immomo.momo.android.a.a;

import com.immomo.momo.android.activity.message.a;
import com.immomo.momo.android.c.g;
import com.immomo.momo.plugin.audio.e;
import com.immomo.momo.service.bean.Message;
import java.io.File;

final class f implements g {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ Message f691a;
    private final /* synthetic */ a b;

    f(Message message, a aVar) {
        this.f691a = message;
        this.b = aVar;
    }

    public final /* synthetic */ void a(Object obj) {
        File file = (File) obj;
        this.f691a.isLoadingResourse = false;
        d.j.remove(this.f691a.msgId);
        if (this.b == null || this.b.isFinishing()) {
            this.f691a.tempFile = null;
            return;
        }
        if (file == null || !file.exists()) {
            this.f691a.tempFile = null;
            this.b.b((CharSequence) "语音下载失败");
            e w = this.b.w();
            Message message = this.f691a;
            w.c();
        } else {
            this.b.B().post(new g(this.b, this.f691a));
        }
        this.b.Y();
    }
}
