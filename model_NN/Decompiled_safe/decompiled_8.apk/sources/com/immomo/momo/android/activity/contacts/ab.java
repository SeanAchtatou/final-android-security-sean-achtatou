package com.immomo.momo.android.activity.contacts;

import android.support.v4.b.a;
import com.immomo.momo.service.bean.g;
import com.immomo.momo.util.d;
import java.util.Comparator;

final class ab implements Comparator {
    ab() {
    }

    public final /* synthetic */ int compare(Object obj, Object obj2) {
        g gVar = (g) obj;
        g gVar2 = (g) obj2;
        if (a.a((CharSequence) gVar.f)) {
            gVar.f = d.a(gVar.e);
        }
        if (a.a((CharSequence) gVar2.f)) {
            gVar2.f = d.a(gVar2.e);
        }
        return gVar.f.compareTo(gVar2.f);
    }
}
