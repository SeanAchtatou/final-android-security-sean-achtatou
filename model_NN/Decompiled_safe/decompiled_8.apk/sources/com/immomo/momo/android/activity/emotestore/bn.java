package com.immomo.momo.android.activity.emotestore;

import android.content.Context;
import android.widget.ListAdapter;
import com.immomo.momo.android.c.d;
import com.immomo.momo.protocol.a.i;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

final class bn extends d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ bg f1315a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public bn(bg bgVar, Context context) {
        super(context);
        this.f1315a = bgVar;
        if (bgVar.S != null) {
            bgVar.S.cancel(true);
        }
        bgVar.S = this;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        ArrayList arrayList = new ArrayList();
        i.a().b(arrayList, 0);
        this.f1315a.Q.c(arrayList);
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public final void a() {
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        super.a(exc);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        List list = (List) obj;
        Date date = new Date();
        this.f1315a.O.setLastFlushTime(date);
        this.f1315a.N.a("newem_reflush", date);
        if (list.size() < 30) {
            this.f1315a.P.setVisibility(8);
        } else {
            this.f1315a.P.setVisibility(0);
        }
        this.f1315a.R.b();
        this.f1315a.R.b((Collection) list);
        this.f1315a.O.setAdapter((ListAdapter) this.f1315a.R);
        this.f1315a.U.j();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.f1315a.O.n();
        this.f1315a.S = null;
    }
}
