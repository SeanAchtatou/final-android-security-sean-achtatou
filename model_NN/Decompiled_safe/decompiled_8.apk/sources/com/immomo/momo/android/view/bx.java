package com.immomo.momo.android.view;

final class bx implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ MomoRefreshListView f2754a;

    bx(MomoRefreshListView momoRefreshListView) {
        this.f2754a = momoRefreshListView;
    }

    public final void run() {
        if (!this.f2754a.f2639a) {
            if (this.f2754a.getFirstVisiblePosition() > 0) {
                this.f2754a.i();
            }
            this.f2754a.a(-this.f2754a.d);
            return;
        }
        if (this.f2754a.x != null) {
            this.f2754a.x.v();
        }
        this.f2754a.i();
        if (this.f2754a.h != null) {
            this.f2754a.h.b_();
        }
    }
}
