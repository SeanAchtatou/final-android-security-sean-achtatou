package com.immomo.momo.android.map;

import android.location.Location;
import com.immomo.momo.android.b.l;
import com.immomo.momo.android.b.p;
import com.immomo.momo.android.b.z;

final class aw extends p {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ SelectSiteAMapActivity f2475a;

    aw(SelectSiteAMapActivity selectSiteAMapActivity) {
        this.f2475a = selectSiteAMapActivity;
    }

    public final void a(Location location, int i, int i2, int i3) {
        if (this.f2475a.d.b) {
            this.f2475a.n = location;
            this.f2475a.p = i;
            if (i != 0) {
                this.f2475a.d.a(location, i, i2, i3);
            } else if (z.a(location)) {
                new l(this.f2475a.d, 0).execute(location);
            } else {
                this.f2475a.d.a(location, i, i2, i3);
            }
        }
    }
}
