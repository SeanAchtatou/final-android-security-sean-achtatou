package com.immomo.momo.android.activity.tieba;

import android.content.Context;
import android.content.Intent;
import com.immomo.momo.android.broadcast.aa;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;

final class dn extends d {

    /* renamed from: a  reason: collision with root package name */
    private v f2242a;
    private /* synthetic */ TiebaCreateActivity c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public dn(TiebaCreateActivity tiebaCreateActivity, Context context) {
        super(context);
        this.c = tiebaCreateActivity;
        if (tiebaCreateActivity.x != null && !tiebaCreateActivity.x.isCancelled()) {
            tiebaCreateActivity.x.cancel(true);
        }
        tiebaCreateActivity.x = this;
        this.f2242a = new v(f());
        this.f2242a.a("请求提交中");
        this.f2242a.setCancelable(true);
        this.f2242a.setOnCancelListener(new Cdo(this));
        tiebaCreateActivity.a(this.f2242a);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        com.immomo.momo.protocol.a.v.a().c(this.c.s);
        return null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.android.activity.tieba.TiebaCreateActivity.a(com.immomo.momo.android.activity.tieba.TiebaCreateActivity, boolean):void
     arg types: [com.immomo.momo.android.activity.tieba.TiebaCreateActivity, int]
     candidates:
      com.immomo.momo.android.activity.tieba.TiebaCreateActivity.a(com.immomo.momo.android.activity.tieba.TiebaCreateActivity, int):void
      com.immomo.momo.android.activity.tieba.TiebaCreateActivity.a(com.immomo.momo.android.activity.tieba.TiebaCreateActivity, com.immomo.momo.android.activity.tieba.dk):void
      com.immomo.momo.android.activity.tieba.TiebaCreateActivity.a(com.immomo.momo.android.activity.tieba.TiebaCreateActivity, com.immomo.momo.android.activity.tieba.dl):void
      com.immomo.momo.android.activity.tieba.TiebaCreateActivity.a(com.immomo.momo.android.activity.tieba.TiebaCreateActivity, com.immomo.momo.android.activity.tieba.dn):void
      com.immomo.momo.android.activity.tieba.TiebaCreateActivity.a(com.immomo.momo.android.activity.tieba.TiebaCreateActivity, com.immomo.momo.android.activity.tieba.dp):void
      com.immomo.momo.android.activity.tieba.TiebaCreateActivity.a(com.immomo.momo.android.activity.tieba.TiebaCreateActivity, com.immomo.momo.android.activity.tieba.dr):void
      com.immomo.momo.android.activity.tieba.TiebaCreateActivity.a(com.immomo.momo.android.activity.tieba.TiebaCreateActivity, java.lang.String):void
      com.immomo.momo.android.activity.tieba.TiebaCreateActivity.a(com.immomo.momo.android.activity.tieba.TiebaCreateActivity, java.util.List):void
      com.immomo.momo.android.activity.ah.a(android.os.Bundle, java.lang.String):boolean
      com.immomo.momo.android.activity.ao.a(int, java.lang.String[]):com.immomo.momo.protocol.imjson.c.a
      com.immomo.momo.android.activity.ao.a(com.immomo.momo.android.view.bi, android.view.View$OnClickListener):void
      com.immomo.momo.android.activity.ao.a(java.lang.CharSequence, int):void
      com.immomo.momo.android.activity.ao.a(android.os.Bundle, java.lang.String):boolean
      com.immomo.momo.android.activity.tieba.TiebaCreateActivity.a(com.immomo.momo.android.activity.tieba.TiebaCreateActivity, boolean):void */
    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        this.c.w = true;
        TiebaCreateActivity tiebaCreateActivity = this.c;
        tiebaCreateActivity.v = tiebaCreateActivity.v + 1;
        Intent intent = new Intent(aa.f2346a);
        intent.putExtra("tiebaid", this.c.s);
        intent.putExtra("type", "support");
        this.c.sendBroadcast(intent);
        this.c.v();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.c.t = (dr) null;
        this.c.p();
    }
}
