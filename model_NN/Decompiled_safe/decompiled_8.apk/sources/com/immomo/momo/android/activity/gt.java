package com.immomo.momo.android.activity;

import com.immomo.momo.android.view.cp;

final class gt implements cp {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ OtherProfileActivity f1701a;

    gt(OtherProfileActivity otherProfileActivity) {
        this.f1701a = otherProfileActivity;
    }

    public final void a(int i) {
        if (this.f1701a.bb == 0) {
            this.f1701a.bb = i;
            this.f1701a.ba = i;
        } else if (i != 0) {
            int round = Math.round(((float) (i - this.f1701a.ba)) * 0.5f) + this.f1701a.aN.getScrollY();
            if (round > 0 && round < this.f1701a.aW) {
                this.f1701a.aN.scrollTo(0, round);
            }
            this.f1701a.ba = i;
        } else {
            this.f1701a.ba = 0;
            this.f1701a.bb = 0;
        }
    }
}
