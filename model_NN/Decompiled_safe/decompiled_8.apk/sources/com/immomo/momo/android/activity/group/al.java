package com.immomo.momo.android.activity.group;

import android.content.DialogInterface;

final class al implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ aj f1536a;

    al(aj ajVar) {
        this.f1536a = ajVar;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        if (dialogInterface != null && !this.f1536a.c.isFinishing()) {
            dialogInterface.dismiss();
        }
        this.f1536a.c.finish();
    }
}
