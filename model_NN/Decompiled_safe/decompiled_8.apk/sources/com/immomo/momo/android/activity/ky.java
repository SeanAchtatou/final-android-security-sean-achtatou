package com.immomo.momo.android.activity;

import android.graphics.Bitmap;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.immomo.momo.R;

final class ky extends WebViewClient {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ShareWebviewActivity f1802a;

    private ky(ShareWebviewActivity shareWebviewActivity) {
        this.f1802a = shareWebviewActivity;
    }

    /* synthetic */ ky(ShareWebviewActivity shareWebviewActivity, byte b) {
        this(shareWebviewActivity);
    }

    public final void onPageFinished(WebView webView, String str) {
        super.onPageFinished(webView, str);
        if (this.f1802a.h != null) {
            this.f1802a.h.setVisibility(8);
        }
    }

    public final void onPageStarted(WebView webView, String str, Bitmap bitmap) {
        super.onPageStarted(webView, str, bitmap);
        if (this.f1802a.h != null) {
            this.f1802a.h.setVisibility(0);
        }
    }

    public final void onReceivedError(WebView webView, int i, String str, String str2) {
        this.f1802a.n.a((Object) ("errorcode: " + i));
        this.f1802a.n.a((Object) ("description: " + str));
        this.f1802a.n.a((Object) ("failingUrl: " + str2));
        if (i == -2) {
            this.f1802a.a((int) R.string.errormsg_network_unfind);
        } else {
            this.f1802a.a((int) R.string.errormsg_server);
        }
        if (this.f1802a.h != null) {
            this.f1802a.h.setVisibility(8);
        }
    }
}
