package com.immomo.momo.android.activity.event;

import com.immomo.momo.android.view.cv;

final class ba implements cv {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ HistoryEventListActivity f1371a;

    ba(HistoryEventListActivity historyEventListActivity) {
        this.f1371a = historyEventListActivity;
    }

    public final void v() {
        this.f1371a.h.n();
        if (this.f1371a.k != null) {
            this.f1371a.k.cancel(true);
            this.f1371a.k = (bc) null;
        }
    }
}
