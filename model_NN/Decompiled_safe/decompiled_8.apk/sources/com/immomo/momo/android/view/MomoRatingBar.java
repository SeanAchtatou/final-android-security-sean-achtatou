package com.immomo.momo.android.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.RatingBar;

public class MomoRatingBar extends RatingBar {

    /* renamed from: a  reason: collision with root package name */
    private RatingBar.OnRatingBarChangeListener f2638a;
    private int b = 0;

    public MomoRatingBar(Context context) {
        super(context);
    }

    public MomoRatingBar(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public MomoRatingBar(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        int x = ((int) ((motionEvent.getX() / ((float) getWidth())) * 5.0f)) + 1;
        if (x > getNumStars()) {
            x = getNumStars();
        }
        if (this.f2638a != null) {
            this.f2638a.onRatingChanged(this, (float) x, true);
        }
        setRating((float) x);
        if (x == this.b) {
            return true;
        }
        return super.onTouchEvent(motionEvent);
    }

    public void setMinRating(int i) {
        this.b = i;
    }

    public void setOnRatingBarChangeListener(RatingBar.OnRatingBarChangeListener onRatingBarChangeListener) {
        this.f2638a = onRatingBarChangeListener;
    }
}
