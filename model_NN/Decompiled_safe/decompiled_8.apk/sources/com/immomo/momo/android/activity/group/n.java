package com.immomo.momo.android.activity.group;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.b.a;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.g;

final class n extends d {

    /* renamed from: a  reason: collision with root package name */
    private v f1688a;
    private /* synthetic */ EditGroupPartyActivity c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public n(EditGroupPartyActivity editGroupPartyActivity, Context context) {
        super(context);
        this.c = editGroupPartyActivity;
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ Object a(Object... objArr) {
        return com.immomo.momo.protocol.a.n.a().a(this.c.x);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.f1688a = new v(this.c);
        this.f1688a.a("请求提交中...");
        this.f1688a.setCancelable(true);
        this.f1688a.setOnCancelListener(new o(this));
        this.f1688a.show();
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        this.b.a((Throwable) exc);
        a(exc.getMessage());
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        a((String) obj);
        this.c.x.o = 2;
        this.c.x.i = 1;
        if (a.f(this.c.y)) {
            this.c.A.a(this.c.y);
        }
        this.b.b((Object) ("++++++++++++ partyid:" + this.c.y));
        this.c.A.a(this.c.x);
        this.c.setResult(-1, new Intent().putExtra("pid", this.c.x.b));
        g.d().a(new Bundle(), "actions.groupfeedchanged");
        this.c.finish();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        if (this.f1688a != null) {
            this.f1688a.dismiss();
        }
    }
}
