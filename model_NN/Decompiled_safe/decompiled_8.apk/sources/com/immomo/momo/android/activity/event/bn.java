package com.immomo.momo.android.activity.event;

import com.immomo.momo.R;

final class bn implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ bm f1383a;

    bn(bm bmVar) {
        this.f1383a = bmVar;
    }

    public final void run() {
        if (!this.f1383a.f1382a.f()) {
            this.f1383a.f1382a.O.setLoadingViewText(R.string.pull_to_refresh_refreshing_label);
            this.f1383a.f1382a.a(new bt(this.f1383a.f1382a, this.f1383a.f1382a.c()));
        }
    }
}
