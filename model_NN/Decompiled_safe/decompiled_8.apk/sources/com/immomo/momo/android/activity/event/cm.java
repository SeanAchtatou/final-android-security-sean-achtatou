package com.immomo.momo.android.activity.event;

import android.content.Context;
import android.widget.ListAdapter;
import com.immomo.momo.android.a.cf;
import com.immomo.momo.android.c.d;
import com.immomo.momo.protocol.a.j;
import java.util.ArrayList;
import java.util.List;

final class cm extends d {

    /* renamed from: a  reason: collision with root package name */
    private List f1406a = new ArrayList();
    private /* synthetic */ UsersEventListActivity c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public cm(UsersEventListActivity usersEventListActivity, Context context) {
        super(context);
        this.c = usersEventListActivity;
        if (usersEventListActivity.n != null) {
            usersEventListActivity.n.cancel(true);
        }
        usersEventListActivity.n = this;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        boolean a2 = j.a().a(this.f1406a, this.c.q, 0);
        this.c.p.clear();
        this.c.p.addAll(this.f1406a);
        return Boolean.valueOf(a2);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        this.c.o = this.f1406a;
        this.c.l = new cf(this.c, this.c.o, this.c.k);
        this.c.k.setAdapter((ListAdapter) this.c.l);
        if (((Boolean) obj).booleanValue()) {
            this.c.m.setVisibility(0);
        } else {
            this.c.m.setVisibility(8);
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.c.k.n();
        this.c.n = (cm) null;
    }
}
