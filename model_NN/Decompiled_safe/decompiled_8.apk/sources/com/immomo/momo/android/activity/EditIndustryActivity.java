package com.immomo.momo.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.b.a;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.view.HeaderLayout;
import com.immomo.momo.android.view.a.n;
import com.immomo.momo.b;
import com.immomo.momo.g;
import com.immomo.momo.service.aq;
import com.immomo.momo.service.bean.am;
import java.util.ArrayList;
import java.util.List;

public class EditIndustryActivity extends ah implements AdapterView.OnItemClickListener {
    private HeaderLayout h;
    private ListView i;
    private BaseAdapter j;
    private EditText k;
    /* access modifiers changed from: private */
    public int l = 0;
    private List m = null;
    private String n;
    private String o = PoiTypeDef.All;

    public EditIndustryActivity() {
        new cb();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.widget.ListView, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        super.a(bundle);
        setContentView((int) R.layout.activity_edit_industry);
        new aq();
        Intent intent = getIntent();
        if (intent != null) {
            String stringExtra = intent.getStringExtra("industry_id");
            if (a.a((CharSequence) stringExtra)) {
                stringExtra = PoiTypeDef.All;
            }
            this.o = stringExtra;
            this.n = intent.getStringExtra("job_name");
        }
        d();
        this.h = (HeaderLayout) findViewById(R.id.layout_header);
        this.h.setTitleText("编辑职业");
        this.i = (ListView) findViewById(R.id.listview_industry);
        LinearLayout linearLayout = (LinearLayout) g.o().inflate((int) R.layout.include_editindustry_headerview, (ViewGroup) this.i, false);
        this.k = (EditText) linearLayout.findViewById(R.id.edittext_job);
        if (!a.a((CharSequence) this.n)) {
            this.k.setText(this.n);
        }
        this.i.addHeaderView(linearLayout);
        List list = this.m;
        ListView listView = this.i;
        this.j = new cd(this, list);
        this.i.setAdapter((ListAdapter) this.j);
        this.i.setOnItemClickListener(this);
    }

    /* access modifiers changed from: protected */
    public final void d() {
        this.m = new ArrayList();
        ArrayList arrayList = new ArrayList(b.b());
        for (int i2 = 0; i2 < arrayList.size(); i2++) {
            ce ceVar = new ce((byte) 0);
            ceVar.f1062a = ((am) arrayList.get(i2)).b;
            ceVar.c = ((am) arrayList.get(i2)).d;
            ceVar.b = ((am) arrayList.get(i2)).f2991a;
            if (this.o.equals(ceVar.b)) {
                this.l = i2;
            }
            this.m.add(ceVar);
        }
        ce ceVar2 = new ce((byte) 0);
        ceVar2.f1062a = "无";
        ceVar2.b = PoiTypeDef.All;
        this.m.add(ceVar2);
        if (PoiTypeDef.All.equals(this.o)) {
            this.l = this.m.size() - 1;
        }
    }

    public void onItemClick(AdapterView adapterView, View view, int i2, long j2) {
        if (i2 <= this.m.size()) {
            this.l = i2 - 1;
            this.o = ((ce) this.j.getItem(this.l)).b;
            this.j.notifyDataSetChanged();
        }
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 == 4) {
            if (this.k == null) {
                setResult(0);
                finish();
            } else if (this.k != null && !this.o.equals(PoiTypeDef.All) && a.a((CharSequence) this.k.getText().toString())) {
                n nVar = new n(this);
                nVar.a();
                nVar.setTitle("提醒");
                nVar.a("职业信息不能为空");
                nVar.a(1, getString(R.string.dialog_btn_cancel), new cc());
                nVar.show();
            } else if (this.k != null) {
                Intent intent = new Intent();
                intent.putExtra("job_name", this.k.getText().toString().trim());
                intent.putExtra("industry_id", this.o);
                setResult(-1, intent);
                finish();
            }
        }
        return super.onKeyDown(i2, keyEvent);
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Bundle bundle) {
        super.onRestoreInstanceState(bundle);
        this.o = bundle.get("industry_id") == null ? PoiTypeDef.All : (String) bundle.get("industry_id");
        String str = bundle.get("job_name") == null ? PoiTypeDef.All : (String) bundle.get("job_name");
        if (!a.a((CharSequence) str)) {
            this.k.setText(str);
            this.k.setSelection(str.length());
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        bundle.putString("industry_id", this.o);
        bundle.putString("job_name", this.k.getText().toString());
    }
}
