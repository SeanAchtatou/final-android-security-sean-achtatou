package com.immomo.momo.android.pay;

import android.os.Handler;
import android.os.Message;
import com.immomo.momo.R;

final class av extends Handler {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ RechargeActivity f2540a;

    av(RechargeActivity rechargeActivity) {
        this.f2540a = rechargeActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.android.pay.RechargeActivity.a(com.immomo.momo.android.pay.RechargeActivity, boolean):void
     arg types: [com.immomo.momo.android.pay.RechargeActivity, int]
     candidates:
      com.immomo.momo.android.pay.RechargeActivity.a(com.immomo.momo.android.pay.RechargeActivity, com.immomo.momo.android.pay.ay):void
      com.immomo.momo.android.pay.RechargeActivity.a(com.immomo.momo.android.pay.RechargeActivity, com.immomo.momo.android.pay.bk):void
      com.immomo.momo.android.pay.RechargeActivity.a(com.immomo.momo.android.pay.RechargeActivity, com.immomo.momo.android.view.a.v):void
      com.immomo.momo.android.pay.RechargeActivity.a(com.immomo.momo.android.pay.RechargeActivity, java.lang.String):void
      com.immomo.momo.android.pay.RechargeActivity.a(com.immomo.momo.android.pay.RechargeActivity, java.util.List):void
      com.immomo.momo.android.pay.RechargeActivity.a(java.lang.String, int):void
      com.immomo.momo.android.activity.ao.a(int, java.lang.String[]):com.immomo.momo.protocol.imjson.c.a
      com.immomo.momo.android.activity.ao.a(com.immomo.momo.android.view.bi, android.view.View$OnClickListener):void
      com.immomo.momo.android.activity.ao.a(java.lang.CharSequence, int):void
      com.immomo.momo.android.activity.ao.a(android.os.Bundle, java.lang.String):boolean
      com.immomo.momo.android.pay.RechargeActivity.a(com.immomo.momo.android.pay.RechargeActivity, boolean):void */
    public final void handleMessage(Message message) {
        try {
            this.f2540a.y = (String) message.obj;
            this.f2540a.e.b((Object) ("strRet: " + this.f2540a.y));
            switch (message.what) {
                case 1:
                    RechargeActivity.c(this.f2540a);
                    try {
                        if ("9000".equals(this.f2540a.y.substring("resultStatus={".length() + this.f2540a.y.indexOf("resultStatus="), this.f2540a.y.indexOf("};memo=")))) {
                            try {
                                this.f2540a.k();
                                this.f2540a.t = true;
                                this.f2540a.s.setText((int) R.string.payvip_btn_recheck);
                                this.f2540a.a(this.f2540a.y, 0);
                            } catch (Exception e) {
                                this.f2540a.e.a((Throwable) e);
                            }
                        }
                    } catch (Exception e2) {
                        this.f2540a.e.a((Throwable) e2);
                        this.f2540a.a((CharSequence) "支付失败");
                    }
                    super.handleMessage(message);
                    return;
                case 2:
                    RechargeActivity.c(this.f2540a);
                    this.f2540a.a((CharSequence) "支付失败，请稍后重试。");
                    super.handleMessage(message);
                    return;
                default:
                    super.handleMessage(message);
                    return;
            }
        } catch (Exception e3) {
            this.f2540a.e.a((Throwable) e3);
        }
    }
}
