package com.immomo.momo.android.activity.maintab;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import com.immomo.a.a.f.a;
import com.immomo.momo.android.activity.UserRoamActivity;
import com.immomo.momo.android.activity.d;
import com.immomo.momo.android.activity.emotestore.MainEmotionActivity;
import com.immomo.momo.android.activity.event.MainEventActivity;
import com.immomo.momo.android.activity.feed.MainFeedActivity;
import com.immomo.momo.android.activity.group.MainGroupActivity;
import com.immomo.momo.android.activity.tieba.MainTiebaActivity;
import com.immomo.momo.android.game.GameProfileActivity;
import com.immomo.momo.service.bean.GameApp;
import com.immomo.momo.service.bean.m;

final class b implements AdapterView.OnItemClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ a f1850a;

    b(a aVar) {
        this.f1850a = aVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.service.bean.at.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, boolean]
     candidates:
      com.immomo.momo.service.bean.at.a(android.content.Context, java.lang.String):com.immomo.momo.service.bean.at
      com.immomo.momo.service.bean.at.a(java.lang.Integer, java.lang.Integer):void
      com.immomo.momo.service.bean.at.a(java.lang.String, java.util.Date):void
      com.immomo.momo.service.bean.at.a(java.lang.String, java.lang.Object):void */
    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        m d = this.f1850a.R.d(i);
        if (d.f3031a == 1) {
            a aVar = this.f1850a;
            a aVar2 = this.f1850a;
            aVar.a(new Intent(a.H(), MainGroupActivity.class));
            this.f1850a.R.notifyDataSetChanged();
            this.f1850a.Q.b(this.f1850a.P);
        } else if (d.f3031a == 2) {
            a aVar3 = this.f1850a;
            a aVar4 = this.f1850a;
            aVar3.a(new Intent(a.H(), MainFeedActivity.class));
        } else if (d.f3031a == 4) {
            a aVar5 = this.f1850a;
            Intent intent = new Intent(a.H(), GameProfileActivity.class);
            intent.putExtra("appid", ((GameApp) d.c).appid);
            this.f1850a.a(intent);
            if (d.d) {
                d.d = false;
                this.f1850a.R.notifyDataSetChanged();
            }
        } else if (d.f3031a == 3) {
            a aVar6 = this.f1850a;
            a aVar7 = this.f1850a;
            aVar6.a(new Intent(a.H(), MainEventActivity.class));
        } else if (d.f3031a == 5) {
            a aVar8 = this.f1850a;
            a aVar9 = this.f1850a;
            aVar8.a(new Intent(a.H(), MainTiebaActivity.class));
            if (d.d) {
                d.d = false;
                this.f1850a.R.notifyDataSetChanged();
                this.f1850a.Q.b(this.f1850a.P);
            }
            if (d.h) {
                d.h = false;
                this.f1850a.N.a("newtieba", (Object) false);
                this.f1850a.R.notifyDataSetChanged();
            }
        } else if (d.f3031a == 6) {
            a aVar10 = this.f1850a;
            a aVar11 = this.f1850a;
            aVar10.a(new Intent(a.H(), UserRoamActivity.class));
        } else if (d.f3031a == 7) {
            if (d.d) {
                d.d = false;
                this.f1850a.R.notifyDataSetChanged();
                this.f1850a.Q.b(this.f1850a.P);
                if (!a.a(d.e) && d.a(d.e, this.f1850a.c())) {
                    return;
                }
            }
            a aVar12 = this.f1850a;
            a aVar13 = this.f1850a;
            aVar12.a(new Intent(a.H(), MainEmotionActivity.class));
        }
    }
}
