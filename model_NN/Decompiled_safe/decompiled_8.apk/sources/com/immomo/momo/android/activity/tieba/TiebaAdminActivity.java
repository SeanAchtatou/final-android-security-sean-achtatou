package com.immomo.momo.android.activity.tieba;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.android.a.ii;
import com.immomo.momo.android.activity.ah;
import com.immomo.momo.android.view.LoadingButton;
import com.immomo.momo.android.view.MomoRefreshListView;
import com.immomo.momo.android.view.a.ay;
import com.immomo.momo.android.view.a.o;
import com.immomo.momo.g;
import com.immomo.momo.service.af;
import com.immomo.momo.service.ao;
import com.immomo.momo.service.bean.c.e;

public class TiebaAdminActivity extends ah {
    /* access modifiers changed from: private */
    public MomoRefreshListView h;
    /* access modifiers changed from: private */
    public LoadingButton i;
    /* access modifiers changed from: private */
    public ao j = null;
    /* access modifiers changed from: private */
    public ii k = null;
    private TextView l = null;

    static /* synthetic */ void a(TiebaAdminActivity tiebaAdminActivity, e eVar) {
        o oVar = new o(tiebaAdminActivity, eVar.n ? eVar.m ? new String[]{"禁言", "忽略举报"} : new String[]{"禁言", "忽略举报"} : eVar.m ? new String[]{"解锁话题", "删除话题", "删除并禁言", "忽略举报"} : new String[]{"锁定话题", "删除话题", "删除并禁言", "忽略举报"});
        oVar.a(new ct(tiebaAdminActivity, eVar));
        oVar.show();
    }

    static /* synthetic */ void a(TiebaAdminActivity tiebaAdminActivity, e eVar, int i2) {
        ay ayVar = new ay(tiebaAdminActivity);
        ayVar.a(new cu(tiebaAdminActivity, eVar, i2, ayVar));
        tiebaAdminActivity.a(ayVar);
    }

    static /* synthetic */ void b(TiebaAdminActivity tiebaAdminActivity, e eVar) {
        o oVar = new o(tiebaAdminActivity, eVar.n ? new String[]{"禁言", "忽略举报"} : new String[]{"删除此评论", "删除并禁言", "忽略举报"});
        oVar.a(new cw(tiebaAdminActivity, eVar));
        oVar.show();
    }

    static /* synthetic */ void b(TiebaAdminActivity tiebaAdminActivity, e eVar, int i2) {
        ay ayVar = new ay(tiebaAdminActivity);
        ayVar.a(new cv(tiebaAdminActivity, eVar, i2, ayVar));
        tiebaAdminActivity.a(ayVar);
    }

    /* access modifiers changed from: private */
    public void c(int i2) {
        if (i2 > 0) {
            this.l.setVisibility(0);
            this.l.setText(String.valueOf(i2) + "条新举报");
            return;
        }
        this.l.setVisibility(8);
    }

    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        super.a(bundle);
        setContentView((int) R.layout.activity_tiebaadmin);
        this.h = (MomoRefreshListView) findViewById(R.id.listview);
        this.h.addHeaderView(g.o().inflate((int) R.layout.listitem_blank, (ViewGroup) null));
        this.h.setEnableLoadMoreFoolter(true);
        this.i = this.h.getFooterViewButton();
        this.i.setVisibility(8);
        setTitle("吧主管理");
        this.h.setLastFlushTime(this.g.b("tiebaadminlasttime"));
        this.l = (TextView) LayoutInflater.from(this).inflate((int) R.layout.include_righttext_hi, (ViewGroup) null);
        m().a(this.l);
        this.h.setOnItemClickListener(new cq(this));
        this.i.setOnProcessListener(new cr(this));
        this.h.setOnPullToRefreshListener$42b903f6(new cs(this));
        d();
    }

    /* access modifiers changed from: protected */
    public final boolean a(Bundle bundle, String str) {
        if (str.equals("actions.tiebareport")) {
            c(bundle.getInt("tiebareport"));
        }
        return super.a(bundle, str);
    }

    /* access modifiers changed from: protected */
    public final void d() {
        this.j = new ao();
        this.k = new ii(this, this.j.a(), this.h);
        this.h.setAdapter((ListAdapter) this.k);
        int l2 = af.c().l();
        if (l2 > 0) {
            t().p();
            this.h.l();
        } else {
            b(new cz(this, this));
        }
        c(l2);
        a(800, "actions.tiebareport");
    }
}
