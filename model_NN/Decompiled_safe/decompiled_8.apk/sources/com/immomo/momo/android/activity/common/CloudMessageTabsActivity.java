package com.immomo.momo.android.activity.common;

import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.b.a;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.view.MomoProgressbar;
import com.immomo.momo.android.view.a.n;
import com.immomo.momo.android.view.bi;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.util.k;
import mm.purchasesdk.PurchaseCode;

public class CloudMessageTabsActivity extends j {
    Handler i = new m(this);
    /* access modifiers changed from: private */
    public n j;
    /* access modifiers changed from: private */
    public EditText k;
    /* access modifiers changed from: private */
    public String l = PoiTypeDef.All;
    private String m = PoiTypeDef.All;
    /* access modifiers changed from: private */
    public q n;
    /* access modifiers changed from: private */
    public n o;
    /* access modifiers changed from: private */
    public TextView p = null;
    /* access modifiers changed from: private */
    public MomoProgressbar q = null;
    /* access modifiers changed from: private */
    public TextView r = null;
    /* access modifiers changed from: private */
    public TextView s = null;
    /* access modifiers changed from: private */
    public int t = PurchaseCode.CERT_REQUEST_CANCEL;
    /* access modifiers changed from: private */
    public int u = 223;
    /* access modifiers changed from: private */
    public int v = 224;
    /* access modifiers changed from: private */
    public long w = 0;
    /* access modifiers changed from: private */
    public long x = 0;

    static /* synthetic */ void o(CloudMessageTabsActivity cloudMessageTabsActivity) {
        cloudMessageTabsActivity.j = n.a(cloudMessageTabsActivity, (CharSequence) null, "确定", "取消", new w(cloudMessageTabsActivity, (byte) 0), (DialogInterface.OnClickListener) null);
        cloudMessageTabsActivity.k = (EditText) g.o().inflate((int) R.layout.common_dialog_password, (ViewGroup) null);
        cloudMessageTabsActivity.j.setTitle("验证身份");
        cloudMessageTabsActivity.j.setContentView(cloudMessageTabsActivity.k);
        cloudMessageTabsActivity.k.requestFocus();
        cloudMessageTabsActivity.j.setCanceledOnTouchOutside(false);
        cloudMessageTabsActivity.j.show();
        cloudMessageTabsActivity.k.postDelayed(new p(cloudMessageTabsActivity), 200);
    }

    /* access modifiers changed from: protected */
    public final String A() {
        return getString(R.string.discuss_select_cloudmessage_much);
    }

    /* access modifiers changed from: protected */
    public final void B() {
    }

    /* access modifiers changed from: protected */
    public final void C() {
        int i2 = 0;
        a(ay.class, a.class);
        findViewById(R.id.contact_tab_both).setOnClickListener(this);
        findViewById(R.id.contact_tab_follows).setOnClickListener(this);
        int intExtra = getIntent().getIntExtra("showindex", 0);
        if (intExtra >= 0) {
            i2 = intExtra > 1 ? 1 : intExtra;
        }
        c(i2);
    }

    /* access modifiers changed from: protected */
    public final void a(int i2, int i3) {
        m().setTitleText(getResources().getString(R.string.discuss_select_createtitle, Integer.valueOf(i2), Integer.valueOf(i3)));
    }

    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        super.a(bundle);
        this.h = 20;
        if (getIntent().getStringExtra("invite_user_id") != null) {
            this.m = getIntent().getStringExtra("invite_user_id");
        }
        if (!a.a((CharSequence) this.m)) {
            D().put(this.m, new bf(this.m));
        }
        a(D().size(), this.h);
        bi biVar = new bi(this);
        biVar.a("提交");
        biVar.a((int) R.drawable.ic_topbar_confirm);
        a(biVar, new o(this));
        this.h = 20;
    }

    /* access modifiers changed from: protected */
    public final void a(String str, int i2) {
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        new k("PO", "P932").e();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        new k("PI", "P932").e();
    }

    /* access modifiers changed from: protected */
    public final boolean y() {
        return false;
    }

    /* access modifiers changed from: protected */
    public final int z() {
        return this.h;
    }
}
