package com.immomo.momo.android.activity.event;

import android.view.View;
import android.widget.AdapterView;
import com.immomo.momo.android.view.a.n;
import com.immomo.momo.g;

final class o implements AdapterView.OnItemClickListener {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public /* synthetic */ EventFeedProfileActivity f1417a;
    private final /* synthetic */ String[] b;

    o(EventFeedProfileActivity eventFeedProfileActivity, String[] strArr) {
        this.f1417a = eventFeedProfileActivity;
        this.b = strArr;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.android.activity.event.EventFeedProfileActivity.a(com.immomo.momo.android.activity.event.EventFeedProfileActivity, java.lang.String, boolean):void
     arg types: [com.immomo.momo.android.activity.event.EventFeedProfileActivity, java.lang.String, int]
     candidates:
      com.immomo.momo.android.activity.ao.a(android.support.v4.app.Fragment, android.content.Intent, int):void
      android.support.v4.app.g.a(java.lang.String, java.io.PrintWriter, android.view.View):void
      android.support.v4.app.g.a(android.support.v4.app.Fragment, android.content.Intent, int):void
      com.immomo.momo.android.activity.event.EventFeedProfileActivity.a(com.immomo.momo.android.activity.event.EventFeedProfileActivity, java.lang.String, boolean):void */
    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        if (this.f1417a.l != null) {
            if ("复制文本".equals(this.b[i])) {
                g.a((CharSequence) this.f1417a.l.b());
                this.f1417a.a((CharSequence) "已成功复制文本");
            } else if ("删除".equals(this.b[i])) {
                n.a(this.f1417a, "确定要删除该留言？", new p(this)).show();
            } else if ("举报".equals(this.b[i])) {
                EventFeedProfileActivity.a(this.f1417a, this.f1417a.l.h, false);
            }
        }
    }
}
