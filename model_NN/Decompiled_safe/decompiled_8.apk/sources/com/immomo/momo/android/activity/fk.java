package com.immomo.momo.android.activity;

import com.immomo.momo.android.view.photoview.PhotoView;

final class fk implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ PhotoView f1493a;
    private final /* synthetic */ fn b;

    fk(PhotoView photoView, fn fnVar) {
        this.f1493a = photoView;
        this.b = fnVar;
    }

    public final void run() {
        this.f1493a.setImageBitmap(this.b.g);
    }
}
