package com.immomo.momo.android.activity.tieba;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;

final class dr extends d {

    /* renamed from: a  reason: collision with root package name */
    private v f2246a;
    private boolean c;
    private boolean d;
    private boolean e;
    private /* synthetic */ TiebaCreateActivity f;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public dr(TiebaCreateActivity tiebaCreateActivity, Context context, boolean z, boolean z2, boolean z3) {
        super(context);
        this.f = tiebaCreateActivity;
        if (tiebaCreateActivity.t != null && !tiebaCreateActivity.t.isCancelled()) {
            tiebaCreateActivity.t.cancel(true);
        }
        tiebaCreateActivity.t = this;
        this.c = z;
        this.d = z2;
        this.e = z3;
        this.f2246a = new v(f());
        this.f2246a.a("请求提交中");
        this.f2246a.setCancelable(true);
        this.f2246a.setOnCancelListener(new ds(this));
        tiebaCreateActivity.a(this.f2246a);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        com.immomo.momo.protocol.a.v.a().a(this.f.u, this.c, this.e, this.d);
        return null;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        a("邀请成功");
        this.f.finish();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.f.t = (dr) null;
        this.f.p();
    }
}
