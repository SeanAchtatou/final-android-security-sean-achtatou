package com.immomo.momo.android.activity.account;

import android.support.v4.b.a;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.util.ao;
import com.immomo.momo.util.k;
import com.immomo.momo.util.m;
import java.util.Calendar;
import java.util.Date;

public final class bb extends ab implements DatePicker.OnDateChangedListener {

    /* renamed from: a  reason: collision with root package name */
    private bf f955a = null;
    private m b = null;
    private RegisterActivityWithP c = null;
    private DatePicker d = null;
    private TextView e = null;
    private TextView f = null;
    private Date g = null;
    private Date h = null;
    private Date i = a.i("19900101");

    public bb(bf bfVar, View view, RegisterActivityWithP registerActivityWithP) {
        super(view);
        this.f955a = bfVar;
        this.c = registerActivityWithP;
        registerActivityWithP.c(RegisterActivityWithP.m);
        this.b = new m("RegisterStep1");
        this.d = (DatePicker) a((int) R.id.rg_timepicker_birthday);
        this.e = (TextView) a((int) R.id.rg_tv_conste);
        this.f = (TextView) a((int) R.id.rg_tv_age);
        if (g.a()) {
            this.d.setLayerType(1, null);
        }
        try {
            if (!a.a((CharSequence) this.f955a.J)) {
                this.i = a.h(this.f955a.J);
            }
        } catch (Exception e2) {
        }
        Calendar instance = Calendar.getInstance();
        Calendar instance2 = Calendar.getInstance();
        instance2.set(1, instance.get(1) - 12);
        this.h = instance2.getTime();
        this.b.a((Object) ("minDate:" + a.e(this.h)));
        instance2.set(1, instance.get(1) - 100);
        this.g = instance2.getTime();
        this.b.a((Object) ("maxDate:" + a.e(this.g)));
        Calendar instance3 = Calendar.getInstance();
        instance3.setTime(this.i);
        a(instance3);
        this.d.init(instance3.get(1), instance3.get(2), instance3.get(5), this);
    }

    private void a(Calendar calendar) {
        this.b.a((Object) ("flushBrithday:" + a.e(calendar.getTime())));
        this.e.setText(a.a(calendar.get(2) + 1, calendar.get(5)));
        this.f.setText(new StringBuilder(String.valueOf(a.h(calendar.getTime()))).toString());
        this.i = calendar.getTime();
    }

    public final boolean a() {
        Calendar instance = Calendar.getInstance();
        instance.set(this.d.getYear(), this.d.getMonth(), this.d.getDayOfMonth());
        a(instance);
        if (this.i.after(this.h) || this.i.before(this.g)) {
            ao.b(String.format(g.a((int) R.string.reg_age_range), 12, 100));
            return false;
        }
        this.f955a.J = a.d(this.i);
        return true;
    }

    public final void c() {
        if (a()) {
            this.c.u();
        }
    }

    public final void d() {
        super.d();
        this.c.i();
    }

    public final void e() {
        new k("PI", "P125").e();
    }

    public final void f() {
        new k("PO", "P125").e();
    }

    public final void onDateChanged(DatePicker datePicker, int i2, int i3, int i4) {
        Calendar instance = Calendar.getInstance();
        instance.set(i2, i3, i4);
        this.b.a((Object) ("change date:" + a.e(instance.getTime())));
        this.b.a((Object) ("minDate:" + this.h + ", maxDate:" + this.g));
        if (instance.getTime().after(this.h) || instance.getTime().before(this.g)) {
            instance.setTime(this.i);
            datePicker.init(instance.get(1), instance.get(2), instance.get(5), this);
            return;
        }
        a(instance);
    }
}
