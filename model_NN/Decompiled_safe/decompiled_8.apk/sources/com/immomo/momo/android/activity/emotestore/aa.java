package com.immomo.momo.android.activity.emotestore;

import android.content.Context;
import android.content.Intent;
import com.immomo.momo.android.broadcast.o;
import com.immomo.momo.android.c.d;
import com.immomo.momo.protocol.a.i;

final class aa extends d {

    /* renamed from: a  reason: collision with root package name */
    private int f1281a;
    private /* synthetic */ EmotionProfileActivity c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public aa(EmotionProfileActivity emotionProfileActivity, Context context) {
        super(context);
        this.c = emotionProfileActivity;
        this.f1281a = emotionProfileActivity.h.A != null ? emotionProfileActivity.h.A.c : -1;
    }

    /* access modifiers changed from: protected */
    public final Object a(Object... objArr) {
        i.a().a(this.c.h);
        this.c.i.b(this.c.h);
        if (!this.c.h.q) {
            return null;
        }
        this.c.i.a(this.c.h);
        return null;
    }

    /* access modifiers changed from: protected */
    public final void a(Object obj) {
        this.c.u();
        if (this.c.h.A == null) {
            return;
        }
        if (this.f1281a != -1 && this.f1281a != this.c.h.A.c) {
            a("完成任务，现在可以使用新表情");
            this.b.a((Object) ("emotion.enable=" + this.c.h.u + ", emotion.downloaded=" + this.c.h.t + ",ownNumber=" + this.c.h.A.c));
            if (this.c.h.u && this.c.h.t) {
                Intent intent = new Intent(o.f2358a);
                intent.putExtra("event", "task");
                this.c.sendBroadcast(intent);
            }
        } else if (this.f1281a == -1 && this.c.h.u && this.c.h.t) {
            Intent intent2 = new Intent(o.f2358a);
            intent2.putExtra("event", "task");
            this.c.sendBroadcast(intent2);
        }
    }
}
