package com.immomo.momo.android.activity.group;

import com.immomo.momo.android.view.MGifImageView;
import com.immomo.momo.android.view.au;
import java.io.File;

final class eo implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ en f1636a;
    private final /* synthetic */ File b;
    private final /* synthetic */ boolean c;
    private final /* synthetic */ MGifImageView d;

    eo(en enVar, File file, boolean z, MGifImageView mGifImageView) {
        this.f1636a = enVar;
        this.b = file;
        this.c = z;
        this.d = mGifImageView;
    }

    public final void run() {
        if (this.b != null && this.b.exists()) {
            this.f1636a.f1635a.A = new au(this.c ? 1 : 2);
            this.f1636a.f1635a.A.a(this.b, this.d);
            this.f1636a.f1635a.A.b(20);
            this.d.setGifDecoder(this.f1636a.f1635a.A);
        }
    }
}
