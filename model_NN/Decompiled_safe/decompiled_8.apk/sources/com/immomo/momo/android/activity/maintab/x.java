package com.immomo.momo.android.activity.maintab;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.b.a;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ListAdapter;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.a.gs;
import com.immomo.momo.android.a.gy;
import com.immomo.momo.android.activity.lh;
import com.immomo.momo.android.b.p;
import com.immomo.momo.android.b.z;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.EmoteEditeText;
import com.immomo.momo.android.view.HeaderLayout;
import com.immomo.momo.android.view.LoadingButton;
import com.immomo.momo.android.view.MomoRefreshExpandableListView;
import com.immomo.momo.android.view.RefreshOnOverScrollListView;
import com.immomo.momo.android.view.bi;
import com.immomo.momo.android.view.bl;
import com.immomo.momo.android.view.bu;
import com.immomo.momo.android.view.cv;
import com.immomo.momo.android.view.dp;
import com.immomo.momo.g;
import com.immomo.momo.service.ag;
import com.immomo.momo.service.am;
import com.immomo.momo.service.aq;
import com.immomo.momo.service.bean.ay;
import com.immomo.momo.service.y;
import com.immomo.momo.util.ao;
import com.immomo.momo.util.k;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class x extends lh implements View.OnClickListener, bl, bu, cv {
    /* access modifiers changed from: private */
    public Map O = new HashMap();
    /* access modifiers changed from: private */
    public Map P = new HashMap();
    /* access modifiers changed from: private */
    public List Q = null;
    /* access modifiers changed from: private */
    public gs R = null;
    /* access modifiers changed from: private */
    public gy S = null;
    /* access modifiers changed from: private */
    public y T = null;
    /* access modifiers changed from: private */
    public am U = null;
    /* access modifiers changed from: private */
    public aq V = null;
    /* access modifiers changed from: private */
    public Date W = null;
    /* access modifiers changed from: private */
    public d X = null;
    /* access modifiers changed from: private */
    public d Y = null;
    private View Z = null;
    private View aa = null;
    /* access modifiers changed from: private */
    public LoadingButton ab = null;
    /* access modifiers changed from: private */
    public MomoRefreshExpandableListView ac = null;
    /* access modifiers changed from: private */
    public RefreshOnOverScrollListView ad = null;
    /* access modifiers changed from: private */
    public View ae = null;
    /* access modifiers changed from: private */
    public LoadingButton af = null;
    private p ag = null;
    /* access modifiers changed from: private */
    public Handler ah = new Handler();
    private bi ai = null;
    private EmoteEditeText aj = null;
    private Button ak = null;
    private View al = null;
    /* access modifiers changed from: private */
    public View am = null;
    /* access modifiers changed from: private */
    public ao an = null;
    /* access modifiers changed from: private */
    public String ao = PoiTypeDef.All;
    private ap ap = null;
    private Animation aq = null;

    static /* synthetic */ void E(x xVar) {
        Intent intent = new Intent();
        intent.setAction("com.android.settings.APPLICATION_DEVELOPMENT_SETTINGS");
        intent.setFlags(268435456);
        try {
            xVar.a(intent);
        } catch (Throwable th) {
            xVar.L.a(th);
            intent.setAction("android.settings.SETTINGS");
            try {
                xVar.a(intent);
            } catch (Exception e) {
                xVar.L.a((Throwable) e);
            }
        }
    }

    public static void O() {
    }

    /* access modifiers changed from: private */
    public void P() {
        View currentFocus = c().getCurrentFocus();
        if (currentFocus != null) {
            ((InputMethodManager) g.c().getSystemService("input_method")).hideSoftInputFromWindow(currentFocus.getWindowToken(), 2);
        }
    }

    /* access modifiers changed from: private */
    public void Q() {
        this.W = new Date();
        this.N.a("ng_latttime_reflush", this.W);
        this.ac.i();
        this.af.e();
    }

    static /* synthetic */ void a(x xVar, String str) {
        if (!a.a((CharSequence) str) && xVar.N != null) {
            ArrayList arrayList = new ArrayList();
            String[] b = a.b(xVar.N.m, ",");
            if (b == null) {
                xVar.N.m = str;
                xVar.N.a();
                return;
            }
            arrayList.addAll(Arrays.asList(b));
            if (!arrayList.contains(str)) {
                arrayList.add(0, str);
            }
            if (arrayList.size() > 5) {
                arrayList.remove(arrayList.size() - 1);
            }
            xVar.N.m = a.a(arrayList, ",");
            xVar.N.a();
        }
    }

    /* access modifiers changed from: private */
    public void al() {
        if (this.Q.size() <= 0) {
            this.af.setVisibility(8);
        } else {
            this.af.setVisibility(0);
        }
    }

    private void am() {
        this.ac.setLoadingViewText(R.string.pull_to_refresh_locate_label);
        this.ag = new z(this);
        try {
            z.a(this.ag);
        } catch (Exception e) {
            this.L.a((Throwable) e);
            ao.g(R.string.errormsg_location_nearby_failed);
            an();
        }
    }

    /* access modifiers changed from: private */
    public void an() {
        this.ah.post(new ae(this));
    }

    private void ao() {
        z.b(this.ag);
        if (this.ag != null) {
            this.ag.b = false;
        }
    }

    private void ap() {
        this.ai.setVisibility(0);
        K().getTitileView().setVisibility(0);
        this.am.setVisibility(8);
        this.aa.setVisibility(8);
        this.ae.setVisibility(8);
        this.ab.setVisibility(8);
        this.aj.setText(PoiTypeDef.All);
        this.ad.setVisibility(8);
        this.S.b();
        aq();
        P();
        ar();
    }

    /* access modifiers changed from: private */
    public void aq() {
        if (this.ap != null && !this.ap.isCancelled()) {
            this.ap.cancel(true);
            this.ap = null;
        }
        if (this.an != null && !this.an.isCancelled()) {
            this.an.cancel(true);
            this.an = null;
        }
    }

    /* access modifiers changed from: private */
    public void ar() {
        this.ak.setVisibility(0);
        this.al.clearAnimation();
        this.al.setVisibility(4);
        this.ab.e();
    }

    static /* synthetic */ void n(x xVar) {
        xVar.ak.setVisibility(8);
        xVar.al.setVisibility(0);
        if (xVar.aq == null) {
            xVar.aq = AnimationUtils.loadAnimation(g.c(), R.anim.loading);
        }
        xVar.al.startAnimation(xVar.aq);
    }

    static /* synthetic */ void y(x xVar) {
        if (!a.a((CharSequence) xVar.aj.getText().toString().trim())) {
            xVar.aq();
            ap apVar = new ap(xVar, xVar.c(), xVar.aj.getText().toString().trim());
            xVar.ap = apVar;
            apVar.execute(new Object[0]);
        }
    }

    /* access modifiers changed from: protected */
    public final int B() {
        return R.layout.fragment_nearby_group;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, com.immomo.momo.android.view.MomoRefreshExpandableListView, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* access modifiers changed from: protected */
    public final void C() {
        this.am = K().getSearchLayout();
        this.aj = (EmoteEditeText) this.am.findViewById(R.id.et_searchbar);
        this.aj.setHint((int) R.string.searchgroup_hint);
        this.ak = (Button) this.am.findViewById(R.id.btn_clear);
        this.al = this.am.findViewById(R.id.iv_loading);
        this.ad = (RefreshOnOverScrollListView) c((int) R.id.listview_search);
        this.ae = c((int) R.id.layout_empty);
        this.ad.setListPaddingBottom(-3);
        RefreshOnOverScrollListView refreshOnOverScrollListView = this.ad;
        gy gyVar = new gy(g.c(), new ArrayList(), this.ad);
        this.S = gyVar;
        refreshOnOverScrollListView.setAdapter((ListAdapter) gyVar);
        this.ad.setVisibility(8);
        this.Z = g.o().inflate((int) R.layout.common_list_loadmore, (ViewGroup) null);
        this.ad.addFooterView(this.Z);
        this.ab = (LoadingButton) this.Z.findViewById(R.id.btn_loadmore);
        this.ab.setNormalIconResId(R.drawable.ic_btn_inner_clock);
        this.ab.setVisibility(8);
        this.aa = c((int) R.id.view_cover);
        Date b = this.N.b("ng_lasttime_success");
        this.ac = (MomoRefreshExpandableListView) c((int) R.id.listview);
        this.ac.setLastFlushTime(b);
        this.ac.setEnableLoadMoreFoolter(true);
        this.ac.setFastScrollEnabled(false);
        this.ac.setMMHeaderView(g.o().inflate((int) R.layout.listitem_groupsite, (ViewGroup) this.ac, false));
        this.af = this.ac.getFooterViewButton();
        if (dp.a(2)) {
            this.ac.addHeaderView(new dp(c(), 2));
        }
        View inflate = g.o().inflate((int) R.layout.include_nearby_listempty, (ViewGroup) null);
        this.ac.a(inflate);
        inflate.findViewById(R.id.nearby_btn_empty_location).setOnClickListener(new ak(this));
        this.ac.setListPaddingBottom(-3);
    }

    public final boolean G() {
        return false;
    }

    public final void S() {
        super.S();
        if (this.R.isEmpty()) {
            this.ac.h();
        } else if (this.W == null || System.currentTimeMillis() - this.W.getTime() > 900000) {
            am();
        }
    }

    public final void a(Context context, HeaderLayout headerLayout) {
        super.a(context, headerLayout);
        headerLayout.setTitleText((int) R.string.nearby_group);
        bi a2 = new bi(g.c()).a((int) R.drawable.ic_topbar_search);
        this.ai = a2;
        headerLayout.a(a2, new al(this));
    }

    public final void ae() {
        super.ae();
        new k("PI", "P3").e();
    }

    public final void ag() {
        super.ag();
        new k("PO", "P3").e();
    }

    public final void ah() {
        super.ah();
        ap();
    }

    public final void ai() {
        this.ac.e();
    }

    public final void aj() {
        this.aa.setOnClickListener(this);
        this.ak.setOnClickListener(this);
        this.ab.setOnProcessListener(new y(this));
        this.aj.addTextChangedListener(new af(this));
        EmoteEditeText emoteEditeText = this.aj;
        EmoteEditeText emoteEditeText2 = this.aj;
        emoteEditeText.addTextChangedListener(new com.immomo.momo.util.aq(20));
        this.aj.setOnEditorActionListener(new ag(this));
        this.ac.setOnPullToRefreshListener(this);
        this.ac.setOnCancelListener(this);
        this.af.setOnProcessListener(this);
        this.ac.setOnChildClickListener(new ah(this));
        this.ac.setOnGroupClickListener(new ai());
        this.ad.setOnItemClickListener(new aj(this));
        this.O.clear();
        this.W = this.N.b("ng_latttime_reflush");
        this.Q = this.U.c();
        for (ay ayVar : this.Q) {
            this.O.put(ayVar.f3001a, ayVar);
        }
        this.R = new gs(this.Q, this.ac);
        this.ac.setAdapter(this.R);
        this.R.a();
        al();
    }

    public final void b_() {
        if (this.X != null && !this.X.isCancelled()) {
            this.X.cancel(true);
        }
        am();
        new k("S", "S301").e();
    }

    public final void g(Bundle bundle) {
        this.T = new y();
        this.V = new aq();
        this.U = new am();
        new ag();
        aj();
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.view_cover /*2131165852*/:
                ap();
                return;
            case R.id.btn_clear /*2131166036*/:
                this.aj.setText(PoiTypeDef.All);
                return;
            default:
                return;
        }
    }

    public final void p() {
        super.p();
        if (this.X != null && !this.X.isCancelled()) {
            this.X.cancel(true);
            this.X = null;
        }
        if (this.Y != null && !this.Y.isCancelled()) {
            this.Y.cancel(true);
            this.Y = null;
        }
        ao();
    }

    public final void u() {
        new k("C", "C301").e();
        this.af.f();
        this.Y = new am(this, c(), this.M.S, this.M.T);
        this.Y.execute(new Object[0]);
    }

    public final void v() {
        Q();
        if (this.X != null && !this.X.isCancelled()) {
            this.X.cancel(true);
        }
        ao();
    }
}
