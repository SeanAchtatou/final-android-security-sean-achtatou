package com.immomo.momo.android.activity.common;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.ka;

public class InviteSNSActivity extends ka implements View.OnClickListener {
    private String h;
    private int i = -1;

    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        super.a(bundle);
        setContentView((int) R.layout.activity_invitesns_tabs);
        this.h = getIntent().getStringExtra("invite_id");
        this.i = getIntent().getIntExtra("invite_type", -1);
        a(al.class, ar.class, ah.class);
        findViewById(R.id.contact_tab_sina).setOnClickListener(this);
        findViewById(R.id.contact_tab_tencent).setOnClickListener(this);
        findViewById(R.id.contact_tab_renren).setOnClickListener(this);
        m().setTitleText("邀请外部好友");
    }

    /* access modifiers changed from: protected */
    public final void a(Fragment fragment, int i2) {
        switch (i2) {
            case 0:
                findViewById(R.id.contact_tab_sina).setSelected(true);
                findViewById(R.id.contact_tab_tencent).setSelected(false);
                findViewById(R.id.contact_tab_renren).setSelected(false);
                return;
            case 1:
                findViewById(R.id.contact_tab_sina).setSelected(false);
                findViewById(R.id.contact_tab_tencent).setSelected(true);
                findViewById(R.id.contact_tab_renren).setSelected(false);
                return;
            case 2:
                findViewById(R.id.contact_tab_sina).setSelected(false);
                findViewById(R.id.contact_tab_tencent).setSelected(false);
                findViewById(R.id.contact_tab_renren).setSelected(true);
                return;
            default:
                return;
        }
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.contact_tab_sina /*2131165735*/:
                c(0);
                return;
            case R.id.contact_tab_tencent /*2131165736*/:
                c(1);
                return;
            case R.id.contact_tab_renren /*2131165737*/:
                c(2);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    public final String y() {
        return this.h;
    }

    public final int z() {
        return this.i;
    }
}
