package com.immomo.momo.android.activity.group;

import android.view.View;

final class af implements View.OnFocusChangeListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ EditGroupProfileActivity f1530a;

    af(EditGroupProfileActivity editGroupProfileActivity) {
        this.f1530a = editGroupProfileActivity;
    }

    public final void onFocusChange(View view, boolean z) {
        if (!z || this.f1530a.m.c == null) {
            this.f1530a.r.setText(this.f1530a.r.getText().toString().trim());
        } else {
            this.f1530a.r.setSelection(this.f1530a.r.getText().toString().length());
        }
    }
}
