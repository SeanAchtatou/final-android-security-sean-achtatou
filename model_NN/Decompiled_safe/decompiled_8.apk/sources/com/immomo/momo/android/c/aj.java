package com.immomo.momo.android.c;

import android.content.Context;
import com.immomo.momo.protocol.a.o;
import com.immomo.momo.service.bean.a.a;
import com.immomo.momo.service.y;
import com.immomo.momo.util.h;

public final class aj extends d {

    /* renamed from: a  reason: collision with root package name */
    private static aj f2375a;
    private String c;
    private String d;
    private String e;

    private aj(Context context, a aVar) {
        super(context);
        this.c = aVar.E;
        this.e = aVar.b;
    }

    public static void a(Context context, a aVar) {
        if (f2375a != null) {
            f2375a.cancel(true);
        }
        aj ajVar = new aj(context, aVar);
        f2375a = ajVar;
        ajVar.execute(new Object[0]);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.service.y.a(java.lang.String, boolean, java.lang.String):void
     arg types: [java.lang.String, int, java.lang.String]
     candidates:
      com.immomo.momo.service.y.a(java.lang.String, java.lang.String, boolean):com.immomo.momo.service.bean.a.j
      com.immomo.momo.service.y.a(int, java.lang.String, java.lang.String):void
      com.immomo.momo.service.y.a(java.lang.String, java.lang.String, int):void
      com.immomo.momo.service.y.a(java.lang.String, boolean, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.h.a(java.lang.String, java.lang.String, int, boolean):void
     arg types: [java.lang.String, java.lang.String, int, int]
     candidates:
      com.immomo.momo.util.h.a(java.lang.String, android.graphics.Bitmap, int, boolean):java.io.File
      com.immomo.momo.util.h.a(java.lang.String, java.lang.String, int, boolean):void */
    /* access modifiers changed from: protected */
    public final Object a(Object... objArr) {
        this.d = o.a().a(this.e, h.a(this.c, 2));
        new y().a(this.e, true, this.d);
        h.a(this.c, this.d, 2, false);
        return null;
    }

    /* access modifiers changed from: protected */
    public final void a(Object obj) {
    }
}
