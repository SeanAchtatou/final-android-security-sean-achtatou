package com.immomo.momo.android.activity.group;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.w;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.ah;
import com.immomo.momo.android.activity.lh;
import com.immomo.momo.android.activity.maintab.aq;
import com.immomo.momo.android.activity.maintab.x;

public class NearbyGroupActivity extends ah {
    private lh h = null;

    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        super.a(bundle);
        setContentView((int) R.layout.activity_nearbygroup);
        if (bundle != null) {
            Fragment a2 = c().a(aq.class.getName());
            Fragment a3 = c().a(x.class.getName());
            w a4 = c().a();
            if (a2 != null) {
                a4.b(a2);
            }
            if (a3 != null) {
                a4.b(a3);
            }
            a4.c();
        }
        this.h = new x();
        w a5 = c().a();
        a5.a(this.h);
        a5.c();
    }

    /* access modifiers changed from: protected */
    public final void e() {
        super.e();
        this.h.aj();
    }

    public void onBackPressed() {
        if (!this.h.G()) {
            super.onBackPressed();
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.h.af();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.h.ad();
    }
}
