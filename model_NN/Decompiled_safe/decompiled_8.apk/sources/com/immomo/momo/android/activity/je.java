package com.immomo.momo.android.activity;

import android.content.Context;
import com.immomo.momo.R;
import com.immomo.momo.a.e;
import com.immomo.momo.a.g;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.w;

final class je extends d {

    /* renamed from: a  reason: collision with root package name */
    private v f1760a;
    private /* synthetic */ OtherProfileV2Activity c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public je(OtherProfileV2Activity otherProfileV2Activity, Context context) {
        super(context);
        this.c = otherProfileV2Activity;
    }

    /* access modifiers changed from: protected */
    public final Object a(Object... objArr) {
        w.a().b(this.c.t.h);
        return null;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.f1760a = new v(this.c);
        this.f1760a.a("请求提交中 ");
        this.f1760a.setCancelable(true);
        this.f1760a.setOnCancelListener(new jf(this));
        this.f1760a.show();
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        if (exc instanceof e) {
            this.b.a((Throwable) exc);
            a((int) R.string.errormsg_network_normal400);
        } else if (exc instanceof g) {
            this.b.a((Throwable) exc);
            a((int) R.string.errormsg_network_normal403);
        } else {
            super.a(exc);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.android.activity.OtherProfileV2Activity.a(com.immomo.momo.android.activity.OtherProfileV2Activity, boolean):void
     arg types: [com.immomo.momo.android.activity.OtherProfileV2Activity, int]
     candidates:
      com.immomo.momo.android.activity.OtherProfileV2Activity.a(com.immomo.momo.android.activity.OtherProfileV2Activity, int):void
      com.immomo.momo.android.activity.OtherProfileV2Activity.a(com.immomo.momo.android.activity.OtherProfileV2Activity, com.immomo.momo.android.a.gv):void
      com.immomo.momo.android.activity.OtherProfileV2Activity.a(com.immomo.momo.android.activity.OtherProfileV2Activity, com.immomo.momo.android.activity.ja):void
      com.immomo.momo.android.activity.OtherProfileV2Activity.a(com.immomo.momo.android.activity.OtherProfileV2Activity, com.immomo.momo.android.activity.je):void
      com.immomo.momo.android.activity.OtherProfileV2Activity.a(com.immomo.momo.android.activity.OtherProfileV2Activity, com.immomo.momo.android.view.EmoteEditeText):void
      com.immomo.momo.android.activity.OtherProfileV2Activity.a(com.immomo.momo.android.activity.OtherProfileV2Activity, java.util.List):void
      com.immomo.momo.android.activity.ah.a(android.os.Bundle, java.lang.String):boolean
      com.immomo.momo.android.activity.ao.a(int, java.lang.String[]):com.immomo.momo.protocol.imjson.c.a
      com.immomo.momo.android.activity.ao.a(com.immomo.momo.android.view.bi, android.view.View$OnClickListener):void
      com.immomo.momo.android.activity.ao.a(java.lang.CharSequence, int):void
      com.immomo.momo.android.activity.ao.a(android.os.Bundle, java.lang.String):boolean
      com.immomo.momo.android.activity.OtherProfileV2Activity.a(com.immomo.momo.android.activity.OtherProfileV2Activity, boolean):void */
    /* access modifiers changed from: protected */
    public final void a(Object obj) {
        this.c.m = false;
        a("取消关注成功");
        OtherProfileV2Activity.a(this.c, 1);
    }

    /* access modifiers changed from: protected */
    public final void b() {
        if (this.f1760a != null) {
            this.f1760a.dismiss();
        }
    }
}
