package com.immomo.momo.android.view.dragsort;

import android.os.SystemClock;
import android.view.View;
import android.view.ViewGroup;

final class r extends t {
    private float b;
    private float c;
    private float d;
    private int e = -1;
    private int f = -1;
    private int g;
    private int h;
    private /* synthetic */ DragSortListView i;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public r(DragSortListView dragSortListView, int i2) {
        super(dragSortListView, i2);
        this.i = dragSortListView;
    }

    public final void a() {
        int i2 = -1;
        this.e = -1;
        this.f = -1;
        this.g = this.i.p;
        this.h = this.i.q;
        int unused = this.i.s;
        this.i.A = 1;
        this.b = (float) this.i.h.x;
        if (this.i.am) {
            float width = ((float) this.i.getWidth()) * 2.0f;
            if (this.i.an == 0.0f) {
                DragSortListView dragSortListView = this.i;
                if (this.b >= 0.0f) {
                    i2 = 1;
                }
                dragSortListView.an = ((float) i2) * width;
                return;
            }
            float f2 = width * 2.0f;
            if (this.i.an < 0.0f && this.i.an > (-f2)) {
                this.i.an = -f2;
            } else if (this.i.an > 0.0f && this.i.an < f2) {
                this.i.an = f2;
            }
        } else {
            this.i.C();
        }
    }

    public final void a(float f2) {
        View childAt;
        float f3 = 1.0f - f2;
        int firstVisiblePosition = this.i.getFirstVisiblePosition();
        View childAt2 = this.i.getChildAt(this.g - firstVisiblePosition);
        if (this.i.am) {
            float uptimeMillis = ((float) (SystemClock.uptimeMillis() - this.f2800a)) / 1000.0f;
            if (uptimeMillis != 0.0f) {
                float o = this.i.an * uptimeMillis;
                int width = this.i.getWidth();
                DragSortListView dragSortListView = this.i;
                dragSortListView.an = (((float) (this.i.an > 0.0f ? 1 : -1)) * uptimeMillis * ((float) width)) + dragSortListView.an;
                this.b += o;
                this.i.h.x = (int) this.b;
                if (this.b < ((float) width) && this.b > ((float) (-width))) {
                    this.f2800a = SystemClock.uptimeMillis();
                    this.i.B();
                    return;
                }
            } else {
                return;
            }
        }
        if (childAt2 != null) {
            if (this.e == -1) {
                this.e = this.i.b(this.g, childAt2, false);
                this.c = (float) (childAt2.getHeight() - this.e);
            }
            int max = Math.max((int) (this.c * f3), 1);
            ViewGroup.LayoutParams layoutParams = childAt2.getLayoutParams();
            layoutParams.height = max + this.e;
            childAt2.setLayoutParams(layoutParams);
        }
        if (this.h != this.g && (childAt = this.i.getChildAt(this.h - firstVisiblePosition)) != null) {
            if (this.f == -1) {
                this.f = this.i.b(this.h, childAt, false);
                this.d = (float) (childAt.getHeight() - this.f);
            }
            int max2 = Math.max((int) (this.d * f3), 1);
            ViewGroup.LayoutParams layoutParams2 = childAt.getLayoutParams();
            layoutParams2.height = max2 + this.f;
            childAt.setLayoutParams(layoutParams2);
        }
    }

    public final void b() {
        DragSortListView.q(this.i);
    }
}
