package com.immomo.momo.android.b;

import android.content.DialogInterface;
import android.content.Intent;
import com.immomo.momo.android.activity.ao;

final class ad implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ ao f2316a;

    ad(ao aoVar) {
        this.f2316a = aoVar;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        Intent intent = new Intent();
        intent.setAction("com.android.settings.APPLICATION_DEVELOPMENT_SETTINGS");
        intent.setFlags(268435456);
        try {
            this.f2316a.startActivity(intent);
        } catch (Throwable th) {
            z.d.a(th);
            intent.setAction("android.settings.SETTINGS");
            try {
                this.f2316a.startActivity(intent);
            } catch (Exception e) {
                z.d.a((Throwable) e);
            }
        }
    }
}
