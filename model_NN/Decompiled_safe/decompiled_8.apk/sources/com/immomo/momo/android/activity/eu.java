package com.immomo.momo.android.activity;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.n;

final class eu extends d {

    /* renamed from: a  reason: collision with root package name */
    private v f1344a;
    private /* synthetic */ GroupLevelActivity c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public eu(GroupLevelActivity groupLevelActivity, Context context) {
        super(context);
        this.c = groupLevelActivity;
        if (groupLevelActivity.w != null) {
            groupLevelActivity.w.cancel(true);
        }
        groupLevelActivity.w = this;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        return n.a().e(this.c.r.b);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.f1344a = new v(this.c);
        this.f1344a.a("请求提交中");
        this.f1344a.setCancelable(true);
        this.f1344a.setOnCancelListener(new ev(this));
        this.c.a(this.f1344a);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        this.f1344a.dismiss();
        this.c.setResult(-1);
        a("升级成功！");
        this.c.finish();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.c.p();
    }
}
