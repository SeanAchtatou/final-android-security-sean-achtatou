package com.immomo.momo.android.activity.common;

import android.content.DialogInterface;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.util.k;
import org.json.JSONObject;

final class w implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ CloudMessageTabsActivity f1134a;

    private w(CloudMessageTabsActivity cloudMessageTabsActivity) {
        this.f1134a = cloudMessageTabsActivity;
    }

    /* synthetic */ w(CloudMessageTabsActivity cloudMessageTabsActivity, byte b) {
        this(cloudMessageTabsActivity);
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f1134a.l = this.f1134a.k.getText().toString();
        if (this.f1134a.l.length() < 4 || this.f1134a.l.length() > 16) {
            this.f1134a.j.c();
            this.f1134a.k.setText(PoiTypeDef.All);
            this.f1134a.a((CharSequence) "密码无效");
            return;
        }
        this.f1134a.j.dismiss();
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("number", this.f1134a.D().size());
            new k("C", "C93201", jSONObject).e();
        } catch (Throwable th) {
        }
        this.f1134a.b(new q(this.f1134a, this.f1134a));
    }
}
