package com.immomo.momo.android.activity.group;

import android.view.View;
import android.widget.AdapterView;
import com.immomo.momo.android.view.a.n;
import com.immomo.momo.g;

final class bc implements AdapterView.OnItemClickListener {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public /* synthetic */ GroupFeedProfileActivity f1554a;

    bc(GroupFeedProfileActivity groupFeedProfileActivity) {
        this.f1554a = groupFeedProfileActivity;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        if (this.f1554a.k != null) {
            if ("复制文本".equals(this.f1554a.o.get(i))) {
                g.a((CharSequence) this.f1554a.k.a());
                this.f1554a.a((CharSequence) "已成功复制文本");
            } else if ("置顶".equals(this.f1554a.o.get(i))) {
                this.f1554a.b(new bl(this.f1554a, this.f1554a.n()));
            } else if ("取消置顶".equals(this.f1554a.o.get(i))) {
                this.f1554a.b(new bl(this.f1554a, this.f1554a.n()));
            } else if ("删除".equals(this.f1554a.o.get(i))) {
                n.a(this.f1554a, "确定要删除该留言？", new bd(this)).show();
            }
        }
    }
}
