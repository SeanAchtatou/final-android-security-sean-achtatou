package com.immomo.momo.android.a;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.b;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.az;
import com.immomo.momo.util.m;
import java.util.List;

public final class au extends a {
    public au(Context context, List list) {
        super(context, list);
        new m("DialogSelectSiteAdapter");
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = g.o().inflate((int) R.layout.griditem_dialog_selectsite, (ViewGroup) null);
            view.setTag(R.id.tv_sitetype_name, view.findViewById(R.id.tv_sitetype_name));
            view.setTag(R.id.iv_sitetype_icon, view.findViewById(R.id.iv_sitetype_icon));
        }
        ImageView imageView = (ImageView) view.getTag(R.id.iv_sitetype_icon);
        az azVar = (az) getItem(i);
        ((TextView) view.getTag(R.id.tv_sitetype_name)).setText(azVar.b);
        Bitmap a2 = b.a(((az) getItem(i)).a());
        if (a2 == null) {
            imageView.setImageResource(R.drawable.ic_sitetype_none);
        } else {
            imageView.setImageBitmap(a2);
        }
        view.setTag(azVar);
        return view;
    }
}
