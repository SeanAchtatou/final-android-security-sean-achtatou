package com.immomo.momo.android.activity.event;

import android.content.Intent;
import android.view.View;
import com.immomo.momo.android.activity.setting.CommunityBindActivity;

final class ch implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ cg f1401a;
    private final /* synthetic */ int b;

    ch(cg cgVar, int i) {
        this.f1401a = cgVar;
        this.b = i;
    }

    public final void onClick(View view) {
        boolean z = false;
        if (this.b == 5) {
            cg cgVar = this.f1401a;
            if (!this.f1401a.f) {
                z = true;
            }
            cgVar.a(z);
        } else if (this.f1401a.g) {
            cg cgVar2 = this.f1401a;
            if (!this.f1401a.f) {
                z = true;
            }
            cgVar2.a(z);
        } else {
            Intent intent = new Intent(this.f1401a.f1400a, CommunityBindActivity.class);
            intent.putExtra("type", this.b);
            this.f1401a.f1400a.startActivityForResult(intent, this.f1401a.i);
        }
    }
}
