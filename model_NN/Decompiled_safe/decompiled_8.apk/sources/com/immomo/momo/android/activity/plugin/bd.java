package com.immomo.momo.android.activity.plugin;

import android.content.Context;
import com.immomo.momo.R;
import com.immomo.momo.a.a;
import com.immomo.momo.android.c.d;

final class bd extends d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ DoubanActivity f2058a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public bd(DoubanActivity doubanActivity, Context context) {
        super(context);
        this.f2058a = doubanActivity;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        return com.immomo.momo.protocol.a.d.c(this.f2058a.u);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
        this.f2058a.findViewById(R.id.process_layout_root).setVisibility(0);
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        if (exc instanceof a) {
            super.a(exc);
        } else {
            a((int) R.string.plus_error_profile);
        }
        this.f2058a.findViewById(R.id.process_layout_root).setVisibility(8);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        com.immomo.momo.plugin.a.d dVar = (com.immomo.momo.plugin.a.d) obj;
        super.a(dVar);
        if (dVar != null) {
            this.f2058a.o = dVar;
            DoubanActivity.b(this.f2058a);
            this.f2058a.t = new be(this.f2058a, this.f2058a).execute(new Object[0]);
            return;
        }
        this.f2058a.findViewById(R.id.process_layout_root).setVisibility(8);
    }

    /* access modifiers changed from: protected */
    public final void b() {
        super.b();
    }
}
