package com.immomo.momo.android.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.WebView;
import android.widget.ImageView;
import com.baidu.location.LocationClientOption;
import com.immomo.a.a.f.a;
import com.immomo.momo.R;
import com.immomo.momo.android.c.o;
import com.immomo.momo.android.service.Initializer;
import com.immomo.momo.g;
import com.immomo.momo.util.m;
import java.io.FileInputStream;
import java.util.concurrent.ThreadPoolExecutor;

public final class lb {

    /* renamed from: a  reason: collision with root package name */
    m f1806a = new m(this);
    private View b = null;
    private ImageView c = null;
    private ImageView d = null;
    private ao e;
    private WebView f = null;
    private boolean g = false;
    private int h = LocationClientOption.MIN_SCAN_SPAN;

    /* JADX WARNING: Removed duplicated region for block: B:25:0x013a  */
    /* JADX WARNING: Removed duplicated region for block: B:53:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public lb(com.immomo.momo.android.activity.ao r11) {
        /*
            r10 = this;
            r2 = 1
            r1 = 0
            r3 = 0
            r10.<init>()
            r10.b = r3
            r10.c = r3
            r10.d = r3
            r10.f = r3
            com.immomo.momo.util.m r0 = new com.immomo.momo.util.m
            r0.<init>(r10)
            r10.f1806a = r0
            r10.g = r1
            r0 = 1000(0x3e8, float:1.401E-42)
            r10.h = r0
            r10.e = r11
            r10.g = r1
            r0 = 2131165980(0x7f07031c, float:1.7946192E38)
            android.view.View r0 = r10.a(r0)
            android.widget.ImageView r0 = (android.widget.ImageView) r0
            r10.d = r0
            r0 = 2131165983(0x7f07031f, float:1.7946199E38)
            android.view.View r0 = r10.a(r0)
            android.widget.ImageView r0 = (android.widget.ImageView) r0
            r10.c = r0
            r0 = 2131165979(0x7f07031b, float:1.794619E38)
            android.view.View r0 = r10.a(r0)
            r10.b = r0
            r0 = 2131165984(0x7f070320, float:1.79462E38)
            android.view.View r0 = r10.a(r0)
            android.webkit.WebView r0 = (android.webkit.WebView) r0
            r10.f = r0
            com.immomo.momo.service.an r0 = new com.immomo.momo.service.an
            r0.<init>()
            java.util.Date r4 = new java.util.Date
            r4.<init>()
            com.immomo.momo.service.bean.ba r5 = r0.a(r4)
            com.immomo.momo.util.m r0 = r10.f1806a
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            java.lang.String r6 = "----------splashscreen:"
            r4.<init>(r6)
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.String r4 = r4.toString()
            r0.a(r4)
            if (r5 == 0) goto L_0x0206
            java.lang.String r0 = r5.a()     // Catch:{ Throwable -> 0x01ed }
            boolean r0 = android.support.v4.b.a.a(r0)     // Catch:{ Throwable -> 0x01ed }
            if (r0 != 0) goto L_0x0209
            com.immomo.momo.android.activity.ao r0 = r10.e     // Catch:{ Throwable -> 0x01ed }
            java.lang.String r4 = r5.i()     // Catch:{ Throwable -> 0x01ed }
            java.io.FileInputStream r0 = r0.openFileInput(r4)     // Catch:{ Throwable -> 0x01ed }
            r4 = 2130838362(0x7f02035a, float:1.7281704E38)
            android.graphics.Bitmap r0 = com.immomo.momo.g.a(r0, r4)     // Catch:{ Throwable -> 0x01ed }
            if (r0 != 0) goto L_0x0099
            java.util.concurrent.ThreadPoolExecutor r4 = com.immomo.momo.android.c.u.b()     // Catch:{ Throwable -> 0x01ed }
            java.lang.String r6 = r5.i()     // Catch:{ Throwable -> 0x01ed }
            java.lang.String r7 = r5.a()     // Catch:{ Throwable -> 0x01ed }
            b(r4, r6, r7)     // Catch:{ Throwable -> 0x01ed }
        L_0x0099:
            com.immomo.momo.util.m r4 = r10.f1806a     // Catch:{ Throwable -> 0x01ed }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x01ed }
            java.lang.String r7 = "---------------------splashscreen,backgroudBitmap="
            r6.<init>(r7)     // Catch:{ Throwable -> 0x01ed }
            java.lang.StringBuilder r6 = r6.append(r0)     // Catch:{ Throwable -> 0x01ed }
            java.lang.String r6 = r6.toString()     // Catch:{ Throwable -> 0x01ed }
            r4.a(r6)     // Catch:{ Throwable -> 0x01ed }
            r4 = r0
        L_0x00ae:
            if (r4 == 0) goto L_0x0206
            java.lang.String r0 = r5.d()     // Catch:{ Throwable -> 0x01ed }
            boolean r0 = android.support.v4.b.a.a(r0)     // Catch:{ Throwable -> 0x01ed }
            if (r0 != 0) goto L_0x0203
            com.immomo.momo.android.activity.ao r0 = r10.e     // Catch:{ Throwable -> 0x01ed }
            java.lang.String r6 = r5.j()     // Catch:{ Throwable -> 0x01ed }
            java.io.FileInputStream r0 = r0.openFileInput(r6)     // Catch:{ Throwable -> 0x01ed }
            r6 = 2130838364(0x7f02035c, float:1.7281708E38)
            android.graphics.Bitmap r0 = com.immomo.momo.g.a(r0, r6)     // Catch:{ Throwable -> 0x01ed }
            if (r0 != 0) goto L_0x00dc
            java.util.concurrent.ThreadPoolExecutor r6 = com.immomo.momo.android.c.u.b()     // Catch:{ Throwable -> 0x01ed }
            java.lang.String r7 = r5.j()     // Catch:{ Throwable -> 0x01ed }
            java.lang.String r8 = r5.d()     // Catch:{ Throwable -> 0x01ed }
            b(r6, r7, r8)     // Catch:{ Throwable -> 0x01ed }
        L_0x00dc:
            com.immomo.momo.util.m r6 = r10.f1806a     // Catch:{ Throwable -> 0x01ed }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x01ed }
            java.lang.String r8 = "---------------------splashscreen,frontBitmap="
            r7.<init>(r8)     // Catch:{ Throwable -> 0x01ed }
            java.lang.StringBuilder r7 = r7.append(r0)     // Catch:{ Throwable -> 0x01ed }
            java.lang.String r7 = r7.toString()     // Catch:{ Throwable -> 0x01ed }
            r6.a(r7)     // Catch:{ Throwable -> 0x01ed }
        L_0x00f0:
            if (r0 != 0) goto L_0x0183
            r4.recycle()     // Catch:{ Throwable -> 0x01ed }
            r0 = r1
        L_0x00f6:
            java.util.Date r3 = new java.util.Date
            r3.<init>()
            com.immomo.momo.android.activity.ao r4 = r10.e
            com.immomo.momo.service.bean.at r4 = r4.o()
            java.lang.String r6 = "splashdownload"
            java.util.Date r4 = r4.b(r6)
            if (r4 == 0) goto L_0x011d
            long r6 = r3.getTime()
            long r8 = r4.getTime()
            long r6 = r6 - r8
            long r6 = java.lang.Math.abs(r6)
            r8 = 3600000(0x36ee80, double:1.7786363E-317)
            int r4 = (r6 > r8 ? 1 : (r6 == r8 ? 0 : -1))
            if (r4 <= 0) goto L_0x0138
        L_0x011d:
            com.immomo.momo.android.activity.le r4 = new com.immomo.momo.android.activity.le
            com.immomo.momo.android.activity.ao r6 = r10.e
            android.content.Context r6 = r6.getApplicationContext()
            r4.<init>(r6)
            java.lang.Object[] r6 = new java.lang.Object[r1]
            r4.execute(r6)
            com.immomo.momo.android.activity.ao r4 = r10.e
            com.immomo.momo.service.bean.at r4 = r4.o()
            java.lang.String r6 = "splashdownload"
            r4.a(r6, r3)
        L_0x0138:
            if (r0 == 0) goto L_0x0182
            java.lang.String r0 = r5.f()
            boolean r0 = com.immomo.a.a.f.a.a(r0)
            if (r0 != 0) goto L_0x01fb
            android.webkit.WebView r0 = r10.f
            r0.setVisibility(r1)
            android.webkit.WebView r0 = r10.f
            android.webkit.WebViewClient r1 = new android.webkit.WebViewClient
            r1.<init>()
            r0.setWebViewClient(r1)
            java.lang.String r0 = r5.f()
            java.lang.String r1 = ""
            java.lang.String r0 = com.immomo.momo.service.bean.d.a(r0, r1)
            com.immomo.momo.util.m r1 = r10.f1806a
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r4 = "url="
            r3.<init>(r4)
            java.lang.StringBuilder r3 = r3.append(r0)
            java.lang.String r3 = r3.toString()
            r1.a(r3)
            android.webkit.WebView r1 = r10.f
            r1.loadUrl(r0)
        L_0x0176:
            java.util.concurrent.ThreadPoolExecutor r0 = com.immomo.momo.android.c.u.b()
            com.immomo.momo.android.activity.ld r1 = new com.immomo.momo.android.activity.ld
            r1.<init>(r10, r2)
            r0.execute(r1)
        L_0x0182:
            return
        L_0x0183:
            android.widget.ImageView r6 = r10.d     // Catch:{ Throwable -> 0x01ed }
            r6.setImageBitmap(r0)     // Catch:{ Throwable -> 0x01ed }
            android.view.View r0 = r10.b     // Catch:{ Throwable -> 0x01ed }
            android.graphics.drawable.BitmapDrawable r6 = new android.graphics.drawable.BitmapDrawable     // Catch:{ Throwable -> 0x01ed }
            com.immomo.momo.android.activity.ao r7 = r10.e     // Catch:{ Throwable -> 0x01ed }
            android.content.res.Resources r7 = r7.getResources()     // Catch:{ Throwable -> 0x01ed }
            r6.<init>(r7, r4)     // Catch:{ Throwable -> 0x01ed }
            r0.setBackgroundDrawable(r6)     // Catch:{ Throwable -> 0x01ed }
            com.immomo.momo.util.m r0 = r10.f1806a     // Catch:{ Throwable -> 0x01ed }
            java.lang.String r4 = "parse success"
            r0.a(r4)     // Catch:{ Throwable -> 0x01ed }
            r0 = 2000(0x7d0, float:2.803E-42)
            r10.h = r0     // Catch:{ Throwable -> 0x01ed }
            java.lang.String r0 = r5.e()     // Catch:{ Throwable -> 0x01f7 }
            boolean r0 = android.support.v4.b.a.a(r0)     // Catch:{ Throwable -> 0x01f7 }
            if (r0 != 0) goto L_0x0201
            com.immomo.momo.android.activity.ao r0 = r10.e     // Catch:{ Throwable -> 0x01f7 }
            java.lang.String r3 = r5.k()     // Catch:{ Throwable -> 0x01f7 }
            java.io.FileInputStream r0 = r0.openFileInput(r3)     // Catch:{ Throwable -> 0x01f7 }
            r3 = 2130838363(0x7f02035b, float:1.7281706E38)
            android.graphics.Bitmap r0 = com.immomo.momo.g.a(r0, r3)     // Catch:{ Throwable -> 0x01f7 }
            if (r0 != 0) goto L_0x01cf
            java.util.concurrent.ThreadPoolExecutor r3 = com.immomo.momo.android.c.u.b()     // Catch:{ Throwable -> 0x01f7 }
            java.lang.String r4 = r5.k()     // Catch:{ Throwable -> 0x01f7 }
            java.lang.String r6 = r5.e()     // Catch:{ Throwable -> 0x01f7 }
            b(r3, r4, r6)     // Catch:{ Throwable -> 0x01f7 }
        L_0x01cf:
            com.immomo.momo.util.m r3 = r10.f1806a     // Catch:{ Throwable -> 0x01f7 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x01f7 }
            java.lang.String r6 = "---------------------splashscreen,copyrightBitmap="
            r4.<init>(r6)     // Catch:{ Throwable -> 0x01f7 }
            java.lang.StringBuilder r4 = r4.append(r0)     // Catch:{ Throwable -> 0x01f7 }
            java.lang.String r4 = r4.toString()     // Catch:{ Throwable -> 0x01f7 }
            r3.a(r4)     // Catch:{ Throwable -> 0x01f7 }
        L_0x01e3:
            if (r0 == 0) goto L_0x01fe
            android.widget.ImageView r3 = r10.c     // Catch:{ Throwable -> 0x01f7 }
            r3.setImageBitmap(r0)     // Catch:{ Throwable -> 0x01f7 }
            r0 = r2
            goto L_0x00f6
        L_0x01ed:
            r0 = move-exception
            r3 = r0
            r0 = r1
        L_0x01f0:
            com.immomo.momo.util.m r4 = r10.f1806a
            r4.a(r3)
            goto L_0x00f6
        L_0x01f7:
            r0 = move-exception
            r3 = r0
            r0 = r2
            goto L_0x01f0
        L_0x01fb:
            r2 = r1
            goto L_0x0176
        L_0x01fe:
            r0 = r2
            goto L_0x00f6
        L_0x0201:
            r0 = r3
            goto L_0x01e3
        L_0x0203:
            r0 = r3
            goto L_0x00f0
        L_0x0206:
            r0 = r1
            goto L_0x00f6
        L_0x0209:
            r4 = r3
            goto L_0x00ae
        */
        throw new UnsupportedOperationException("Method not decompiled: com.immomo.momo.android.activity.lb.<init>(com.immomo.momo.android.activity.ao):void");
    }

    private View a(int i) {
        return this.e.findViewById(i);
    }

    /* access modifiers changed from: private */
    public static void b(ThreadPoolExecutor threadPoolExecutor, String str, String str2) {
        boolean z = false;
        if (!a.a(str2)) {
            try {
                FileInputStream openFileInput = g.c().openFileInput(str);
                Bitmap decodeStream = BitmapFactory.decodeStream(openFileInput);
                if (decodeStream != null) {
                    decodeStream.recycle();
                    z = true;
                }
                openFileInput.close();
            } catch (Throwable th) {
            }
            if (!z) {
                Bitmap.CompressFormat compressFormat = Bitmap.CompressFormat.PNG;
                if (str2 != null && str2.endsWith(".jpg")) {
                    compressFormat = Bitmap.CompressFormat.JPEG;
                }
                o oVar = new o("temp", new lf(str, compressFormat), -1, null);
                oVar.a(str2);
                threadPoolExecutor.execute(oVar);
            }
        }
    }

    public final void a() {
        this.b.setVisibility(0);
        this.e.startService(new Intent(this.e.getApplicationContext(), Initializer.class));
        if ("10".equals(g.g())) {
            this.b.findViewById(R.id.splash_layout_market).setVisibility(0);
        }
    }

    public final void a(Runnable runnable) {
        this.b.post(runnable);
    }

    public final boolean a(Runnable runnable, long j) {
        return this.b.postDelayed(runnable, j);
    }

    public final int b() {
        return this.h;
    }

    public final void c() {
        Animation loadAnimation = AnimationUtils.loadAnimation(this.e, R.anim.layout_alpha_out);
        loadAnimation.setDuration(300);
        this.b.setAnimation(loadAnimation);
        this.b.setVisibility(8);
        this.b.postDelayed(new lc(this), 1000);
    }

    /* access modifiers changed from: package-private */
    public final void d() {
        if (!this.g) {
            this.g = true;
            ((ViewGroup) this.b.getParent()).removeView(this.b);
            Drawable background = this.b.getBackground();
            if (background instanceof BitmapDrawable) {
                Bitmap bitmap = ((BitmapDrawable) background).getBitmap();
                this.b.setBackgroundResource(0);
                if (bitmap != null) {
                    bitmap.recycle();
                }
            }
        }
    }
}
