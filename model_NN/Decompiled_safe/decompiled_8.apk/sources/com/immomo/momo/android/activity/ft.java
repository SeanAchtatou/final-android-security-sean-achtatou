package com.immomo.momo.android.activity;

import android.content.Intent;
import android.view.View;
import com.immomo.momo.R;
import com.immomo.momo.android.broadcast.m;

final class ft implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ KickOffActivity f1501a;

    ft(KickOffActivity kickOffActivity) {
        this.f1501a = kickOffActivity;
    }

    public final void onClick(View view) {
        this.f1501a.t().l();
        Intent intent = new Intent(this.f1501a.getApplicationContext(), WelcomeActivity.class);
        intent.putExtra("model", 0);
        this.f1501a.startActivity(intent);
        this.f1501a.overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
        this.f1501a.finish();
        this.f1501a.sendOrderedBroadcast(new Intent(m.f2356a), null);
    }
}
