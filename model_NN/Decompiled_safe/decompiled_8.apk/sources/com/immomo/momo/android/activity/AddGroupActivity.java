package com.immomo.momo.android.activity;

import android.content.Context;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import com.immomo.momo.R;
import com.immomo.momo.android.view.HeaderLayout;
import com.immomo.momo.service.bean.a.a;

public class AddGroupActivity extends lh {
    /* access modifiers changed from: private */
    public a O;
    private Button P;
    /* access modifiers changed from: private */
    public EditText Q;
    /* access modifiers changed from: private */
    public m R = null;

    /* access modifiers changed from: protected */
    public final int B() {
        return R.layout.activity_addgroup;
    }

    /* access modifiers changed from: protected */
    public final void C() {
        this.Q = (EditText) c((int) R.id.friend_edit_momoid);
        this.Q.requestFocus();
        this.P = (Button) c((int) R.id.friend_btn_find);
    }

    public final void a(Context context, HeaderLayout headerLayout) {
        headerLayout.setTitleText((int) R.string.addtabs_group);
    }

    /* access modifiers changed from: protected */
    public final void g(Bundle bundle) {
        this.P.setOnClickListener(new l(this));
        aj();
    }

    public final void p() {
        super.p();
        if (this.R != null && !this.R.isCancelled()) {
            this.R.cancel(true);
        }
    }
}
