package com.immomo.momo.android.view;

import android.view.View;

final class bv implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ MomoRefreshListView f2752a;

    bv(MomoRefreshListView momoRefreshListView) {
        this.f2752a = momoRefreshListView;
    }

    public final void onClick(View view) {
        if (this.f2752a.x != null) {
            this.f2752a.setLoadingVisible(false);
            this.f2752a.x.v();
        }
    }
}
