package com.immomo.momo.android.activity.maintab;

import android.content.Intent;
import com.immomo.momo.android.broadcast.d;
import com.immomo.momo.android.broadcast.p;
import com.immomo.momo.service.bean.ab;

final class m implements d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ l f1893a;

    m(l lVar) {
        this.f1893a = lVar;
    }

    public final void a(Intent intent) {
        ab a2;
        if (intent != null && p.b.equals(intent.getAction()) && (a2 = this.f1893a.X.a(intent.getStringExtra("feedid"))) != null && !this.f1893a.S.contains(a2)) {
            this.f1893a.S.add(a2);
            this.f1893a.W.b(0, a2);
        }
    }
}
