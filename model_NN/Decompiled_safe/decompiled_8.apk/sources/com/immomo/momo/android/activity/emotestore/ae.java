package com.immomo.momo.android.activity.emotestore;

import android.content.Context;
import android.os.Bundle;
import android.widget.ListAdapter;
import com.immomo.momo.R;
import com.immomo.momo.android.a.bq;
import com.immomo.momo.android.activity.ao;
import com.immomo.momo.android.activity.lh;
import com.immomo.momo.android.broadcast.d;
import com.immomo.momo.android.broadcast.z;
import com.immomo.momo.android.view.HeaderLayout;
import com.immomo.momo.android.view.LoadingButton;
import com.immomo.momo.android.view.MomoRefreshListView;
import com.immomo.momo.android.view.g;
import com.immomo.momo.service.o;
import com.immomo.momo.util.k;
import java.util.Date;

public class ae extends lh {
    MomoRefreshListView O = null;
    LoadingButton P;
    o Q = new o();
    bq R = null;
    al S = null;
    ak T = null;
    g U = null;
    private z V = null;
    private d W = new af(this);

    /* access modifiers changed from: protected */
    public final int B() {
        return R.layout.activity_emotestore_hot;
    }

    public final void C() {
        this.O = (MomoRefreshListView) c((int) R.id.listview);
        this.O.setEnableLoadMoreFoolter(true);
        this.O.setCompleteScrollTop(false);
        this.P = this.O.getFooterViewButton();
        this.U = new g(c(), 7);
        this.O.addHeaderView(this.U.getWappview());
        this.O.setListPaddingBottom(-3);
    }

    /* access modifiers changed from: protected */
    public final void E() {
        super.E();
        this.O.o();
        if (this.P.d()) {
            this.P.e();
        }
        this.U.setContext(c());
    }

    public final void a(Context context, HeaderLayout headerLayout) {
    }

    public final void ae() {
        new k("PO", "P941").e();
        this.U.g();
    }

    public final void ag() {
        new k("PI", "P941").e();
        this.U.f();
    }

    public final void ai() {
        this.O.i();
    }

    public final void aj() {
        this.R = new bq(c(), this.Q.b(), this.O);
        this.O.setAdapter((ListAdapter) this.R);
        if (this.R.getCount() < 30) {
            this.P.setVisibility(8);
        } else {
            this.P.setVisibility(0);
        }
        Date b = this.N.b("freeem_reflush");
        this.O.setLastFlushTime(b);
        if (this.R.isEmpty()) {
            this.O.l();
        } else if (b == null || Math.abs(b.getTime() - System.currentTimeMillis()) > 900000) {
            ((ao) c()).b(new al(this, c()));
        }
    }

    /* access modifiers changed from: protected */
    public final void g(Bundle bundle) {
        this.V = new z(c());
        this.V.a(this.W);
        this.O.setOnCancelListener$135502(new ag(this));
        this.O.setOnPullToRefreshListener$42b903f6(new ah(this));
        this.O.setOnItemClickListener(new ai(this));
        this.P.setOnProcessListener(new aj(this));
        aj();
        this.L.b((Object) "onCreatedonCreatedonCreatedonCreatedonCreatedonCreatedonCreated");
    }

    public final void p() {
        super.p();
        if (this.V != null) {
            a(this.V);
        }
    }
}
