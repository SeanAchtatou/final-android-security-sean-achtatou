package com.immomo.momo.android.activity.discuss;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.b.a;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.OtherProfileActivity;
import com.immomo.momo.android.activity.ah;
import com.immomo.momo.android.activity.common.InviteToDiscussTabsActivity;
import com.immomo.momo.android.activity.message.MultiChatActivity;
import com.immomo.momo.android.c.r;
import com.immomo.momo.android.c.u;
import com.immomo.momo.android.view.EmoteEditeText;
import com.immomo.momo.android.view.HeaderLayout;
import com.immomo.momo.android.view.bi;
import com.immomo.momo.g;
import com.immomo.momo.service.aq;
import com.immomo.momo.service.bean.au;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.service.bean.n;
import com.immomo.momo.service.bean.o;
import com.immomo.momo.service.h;
import java.io.File;
import java.util.ArrayList;
import mm.purchasesdk.PurchaseCode;

public class DiscussProfileActivity extends ah implements View.OnClickListener {
    private View.OnClickListener A = new n(this);
    private aa h;
    /* access modifiers changed from: private */
    public boolean i = false;
    /* access modifiers changed from: private */
    public h j = null;
    private aq k = null;
    /* access modifiers changed from: private */
    public boolean l = false;
    private View m;
    private View[] n;
    private ArrayList o = new ArrayList();
    private String p;
    /* access modifiers changed from: private */
    public String q;
    /* access modifiers changed from: private */
    public HeaderLayout r = null;
    private bi s = null;
    private TextView t = null;
    private TextView u = null;
    private TextView v = null;
    private View w;
    /* access modifiers changed from: private */
    public o x = null;
    /* access modifiers changed from: private */
    public n y = null;
    /* access modifiers changed from: private */
    public Handler z = new m(this);

    private void A() {
        if (this.h != null && !this.h.isCancelled()) {
            this.h.cancel(true);
        }
        this.h = new aa(this, this);
        this.h.execute(new String[0]);
    }

    static /* synthetic */ void a(DiscussProfileActivity discussProfileActivity, String str, Bitmap bitmap) {
        if (!a.a((CharSequence) str) && bitmap != null && discussProfileActivity.n != null) {
            int length = discussProfileActivity.n.length;
            int i2 = 0;
            while (i2 < length) {
                v vVar = (v) discussProfileActivity.n[i2].findViewById(R.id.avatar_cover).getTag();
                if (vVar == null || a.a((CharSequence) ((au) vVar.f1247a).b) || !((au) vVar.f1247a).b.equals(str)) {
                    i2++;
                } else {
                    ((ImageView) discussProfileActivity.n[i2].findViewById(R.id.avatar_imageview)).setImageBitmap(bitmap);
                    return;
                }
            }
        }
    }

    static /* synthetic */ void c(DiscussProfileActivity discussProfileActivity) {
        String[] strArr = new String[3];
        strArr[0] = (discussProfileActivity.x == null || !discussProfileActivity.x.f3033a) ? discussProfileActivity.getString(R.string.dprofile_owner_op0) : discussProfileActivity.getString(R.string.dprofile_owner_op01);
        strArr[1] = discussProfileActivity.getString(R.string.dprofile_owner_op1);
        strArr[2] = discussProfileActivity.getString(R.string.dprofile_owner_op2);
        com.immomo.momo.android.view.a.o oVar = new com.immomo.momo.android.view.a.o(discussProfileActivity, strArr);
        oVar.setTitle((int) R.string.dialog_title_avatar_long_press);
        oVar.a();
        oVar.a(new p(discussProfileActivity));
        oVar.show();
    }

    static /* synthetic */ void e(DiscussProfileActivity discussProfileActivity) {
        String[] strArr = new String[2];
        strArr[0] = (discussProfileActivity.x == null || !discussProfileActivity.x.f3033a) ? discussProfileActivity.getString(R.string.dprofile_member_op0) : discussProfileActivity.getString(R.string.dprofile_member_op01);
        strArr[1] = discussProfileActivity.getString(R.string.dprofile_member_op1);
        com.immomo.momo.android.view.a.o oVar = new com.immomo.momo.android.view.a.o(discussProfileActivity, strArr);
        oVar.setTitle((int) R.string.dialog_title_avatar_long_press);
        oVar.a();
        oVar.a(new t(discussProfileActivity));
        oVar.show();
    }

    static /* synthetic */ void k(DiscussProfileActivity discussProfileActivity) {
        if (discussProfileActivity.y != null) {
            discussProfileActivity.z();
            discussProfileActivity.v();
        }
    }

    static /* synthetic */ void n(DiscussProfileActivity discussProfileActivity) {
        View inflate = g.o().inflate((int) R.layout.dialog_contactpeople_apply, (ViewGroup) null);
        EmoteEditeText emoteEditeText = (EmoteEditeText) inflate.findViewById(R.id.edittext_reason);
        if (discussProfileActivity.y != null && !a.a((CharSequence) discussProfileActivity.y.b)) {
            emoteEditeText.setText(discussProfileActivity.y.b);
        }
        emoteEditeText.addTextChangedListener(new com.immomo.momo.util.aq(24));
        emoteEditeText.setHint((int) R.string.dprofile_editname_hint);
        emoteEditeText.setSelection(emoteEditeText.getText().toString().length());
        com.immomo.momo.android.view.a.n nVar = new com.immomo.momo.android.view.a.n(discussProfileActivity);
        nVar.setTitle("修改名称");
        nVar.setContentView(inflate);
        nVar.a();
        nVar.a(0, discussProfileActivity.getString(R.string.dialog_btn_confim), new r(discussProfileActivity, emoteEditeText));
        nVar.a(1, discussProfileActivity.getString(R.string.dialog_btn_cancel), new s());
        nVar.getWindow().setSoftInputMode(4);
        nVar.show();
    }

    static /* synthetic */ void u() {
    }

    private void v() {
        boolean z2;
        if (this.o != null && this.o.size() > 0) {
            int size = this.o.size();
            for (int i2 = 0; i2 < size; i2++) {
                au auVar = (au) this.o.get(i2);
                String str = auVar.b;
                this.e.b((Object) ("downloadMemberPhotos: " + str));
                if (a.a((CharSequence) str)) {
                    Message message = new Message();
                    Bundle bundle = new Bundle();
                    bundle.putString("guid", auVar.b);
                    message.setData(bundle);
                    message.obj = null;
                    message.what = 12;
                    this.z.sendMessage(message);
                } else {
                    try {
                        z2 = new File(new File(com.immomo.momo.a.d(), str.substring(0, 1)), String.valueOf(str) + ".jpg_").exists();
                        if (!z2) {
                            try {
                                z2 = new File(com.immomo.momo.a.d(), String.valueOf(str) + ".jpg_").exists();
                            } catch (Exception e) {
                            }
                        }
                    } catch (Exception e2) {
                        z2 = false;
                    }
                    o oVar = new o(this, auVar);
                    if (z2) {
                        u.f().execute(new r(str, oVar, 3, null));
                    } else {
                        u.g().execute(new com.immomo.momo.android.c.o(str, oVar, 3, null));
                    }
                }
            }
        }
    }

    private void w() {
        findViewById(R.id.layout_owner).setOnClickListener(this);
        findViewById(R.id.layout_inviteothers).setOnClickListener(this);
        this.w.setOnClickListener(this);
        this.m.setOnClickListener(this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.service.h.a(java.lang.String, boolean):com.immomo.momo.service.bean.n
     arg types: [java.lang.String, int]
     candidates:
      com.immomo.momo.service.h.a(com.immomo.momo.service.bean.n, java.lang.String):void
      com.immomo.momo.service.h.a(java.lang.String, int):void
      com.immomo.momo.service.h.a(java.lang.String, java.lang.String):void
      com.immomo.momo.service.h.a(java.lang.String, java.lang.String[]):void
      com.immomo.momo.service.h.a(java.util.List, java.lang.String):void
      com.immomo.momo.service.h.a(java.lang.String, boolean):com.immomo.momo.service.bean.n */
    private void x() {
        if (!a.a((CharSequence) this.q)) {
            this.x = this.g.d(this.q);
            this.y = this.j.a(this.q, false);
            if (this.y != null) {
                this.i = false;
                this.f.h.equals(this.y.c);
                this.l = this.j.d(this.f.h, this.q);
                y();
                z();
                return;
            }
            this.i = true;
            this.y = new n(this.q);
            this.r.setTitleText(this.y.f);
        }
    }

    /* access modifiers changed from: private */
    public void y() {
        if (!(this.y == null || this.y.e == null || this.y.e.length <= 0)) {
            aq aqVar = new aq();
            int length = this.y.e.length > 8 ? 8 : this.y.e.length;
            this.o.clear();
            for (int i2 = 0; i2 < length; i2++) {
                au auVar = new au();
                auVar.b = aqVar.a(this.y.e[i2]);
                this.o.add(auVar);
            }
        }
        if (this.o != null && this.o.size() > 0) {
            int size = this.o.size();
            this.e.a((Object) ("length: " + size));
            for (int i3 = 0; i3 < size; i3++) {
                this.n[i3].setVisibility(0);
                ((ImageView) this.n[i3].findViewById(R.id.avatar_cover)).setTag(new v(this.o.get(i3)));
            }
            if (size < 8) {
                for (int i4 = size; i4 < 8; i4++) {
                    this.n[i4].setVisibility(4);
                }
            }
        }
    }

    private void z() {
        if (this.y != null) {
            this.v.setText(this.q);
            if (!a.a((CharSequence) this.y.b)) {
                this.r.setTitleText(this.y.b);
            } else {
                this.r.setTitleText(this.y.f);
            }
            String a2 = g.a((int) R.string.discussprofile_member_title);
            this.t.setText(String.format(a2, Integer.valueOf(this.y.j), Integer.valueOf(this.y.k)));
            bf b = this.k.b(this.y.c);
            if (b != null) {
                this.u.setText(b.h());
            } else {
                this.u.setText(this.y.c);
            }
            if (this.y == null) {
                return;
            }
            if (this.f.h.equals(this.y.c) || this.l) {
                this.w.setVisibility(0);
                this.s.setVisibility(0);
                return;
            }
            this.w.setVisibility(8);
            this.s.setVisibility(8);
        }
    }

    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        super.a(bundle);
        setContentView((int) R.layout.activity_discussprofile);
        this.j = new h();
        this.k = new aq();
        this.r = (HeaderLayout) findViewById(R.id.layout_header);
        this.t = (TextView) findViewById(R.id.tv_member_count);
        this.u = (TextView) findViewById(R.id.tv_ownername);
        this.v = (TextView) findViewById(R.id.tv_discussid);
        this.w = (LinearLayout) findViewById(R.id.profile_layout_chat);
        ((TextView) this.w.findViewById(R.id.tv_chat)).setText((int) R.string.dprofile_setting_startchat);
        this.s = new bi(this);
        this.s.a((int) R.drawable.ic_topbar_more);
        this.s.setVisibility(8);
        this.r.a(this.s, this.A);
        this.m = findViewById(R.id.view_showmemberlist);
        this.n = new View[8];
        this.n[0] = findViewById(R.id.member_avatar_block0);
        this.n[1] = findViewById(R.id.member_avatar_block1);
        this.n[2] = findViewById(R.id.member_avatar_block2);
        this.n[3] = findViewById(R.id.member_avatar_block3);
        this.n[4] = findViewById(R.id.member_avatar_block4);
        this.n[5] = findViewById(R.id.member_avatar_block5);
        this.n[6] = findViewById(R.id.member_avatar_block6);
        this.n[7] = findViewById(R.id.member_avatar_block7);
        int round = Math.round((((((float) g.J()) - (g.l().getDimension(R.dimen.pic_item_margin) * 16.0f)) - (g.l().getDimension(R.dimen.style_patterns) * 2.0f)) - (((float) BitmapFactory.decodeResource(getResources(), R.drawable.ic_common_arrow_right).getWidth()) + g.l().getDimension(R.dimen.member_arrow_margin_left))) / 8.0f);
        for (int i2 = 0; i2 < this.n.length; i2++) {
            ViewGroup.LayoutParams layoutParams = this.n[i2].getLayoutParams();
            layoutParams.height = round;
            layoutParams.width = round;
            this.n[i2].setLayoutParams(layoutParams);
        }
        w();
        if (bundle == null || !bundle.getBoolean("from_saveinstance", false)) {
            Intent intent = getIntent();
            this.p = intent.getStringExtra("tag");
            this.q = intent.getStringExtra("did");
        } else {
            this.q = (String) bundle.get("did");
            this.p = (String) bundle.get("tag");
            this.p = this.p == null ? "local" : this.p;
        }
        x();
    }

    /* access modifiers changed from: protected */
    public final void e() {
        super.e();
        v();
        w();
        if (!"notreflsh".equals(this.p)) {
            A();
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        if (i2 == 100 || i3 == -1) {
            A();
        } else if (i2 == 101 || i3 == -1) {
            x();
        }
        super.onActivityResult(i2, i3, intent);
    }

    public void onClick(View view) {
        boolean z2 = false;
        switch (view.getId()) {
            case R.id.layout_owner /*2131165489*/:
                Intent intent = new Intent(this, OtherProfileActivity.class);
                intent.putExtra("tag", "internet");
                intent.putExtra("momoid", this.y.c);
                startActivity(intent);
                return;
            case R.id.layout_inviteothers /*2131165492*/:
                if (!a.a((CharSequence) this.q)) {
                    Intent intent2 = new Intent(this, InviteToDiscussTabsActivity.class);
                    intent2.putExtra("did", this.q);
                    startActivityForResult(intent2, 100);
                    return;
                }
                return;
            case R.id.view_showmemberlist /*2131165497*/:
                Intent intent3 = new Intent(this, DiscussMemberListActivity.class);
                intent3.putExtra("did", this.y == null ? this.q : this.y.f);
                intent3.putExtra("count", this.y == null ? 0 : this.y.j);
                if (this.y != null) {
                    z2 = this.f.h.equals(this.y.c);
                }
                intent3.putExtra("is_owner", z2);
                startActivityForResult(intent3, PurchaseCode.QUERY_OK);
                return;
            case R.id.profile_layout_chat /*2131166283*/:
                if (!a.a((CharSequence) this.q)) {
                    Intent intent4 = new Intent(this, MultiChatActivity.class);
                    intent4.putExtra("remoteDiscussID", this.q);
                    startActivity(intent4);
                    return;
                }
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (this.h != null && !this.h.isCancelled()) {
            this.h.cancel(true);
        }
        super.onDestroy();
    }
}
