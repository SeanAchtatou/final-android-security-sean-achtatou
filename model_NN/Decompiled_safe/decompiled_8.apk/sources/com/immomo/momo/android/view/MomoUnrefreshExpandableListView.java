package com.immomo.momo.android.view;

import android.content.Context;
import android.graphics.Canvas;
import android.os.Handler;
import android.support.v4.b.a;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.AbsListView;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.android.a.gj;
import com.immomo.momo.android.plugin.cropimage.q;
import com.immomo.momo.g;
import java.util.Date;

public class MomoUnrefreshExpandableListView extends cu {
    private int d;
    private LoadingButton e = null;
    private boolean f = false;
    private ExpandableListView.OnChildClickListener g;
    private cv h = null;
    private Handler i = new by(this, getContext().getMainLooper());
    private gj j;
    private View k;
    private boolean l = false;
    private int m;
    private int n;
    private boolean o = true;
    private int p = -1;
    private float q;
    private float r;

    public MomoUnrefreshExpandableListView(Context context) {
        super(context);
        g();
        h();
    }

    public MomoUnrefreshExpandableListView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        g();
        h();
    }

    public MomoUnrefreshExpandableListView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        g();
        h();
    }

    private void g() {
        View inflate = g.o().inflate((int) R.layout.common_list_loadmore, (ViewGroup) null);
        this.c = inflate;
        this.e = (LoadingButton) inflate.findViewById(R.id.btn_loadmore);
        AnimationUtils.loadAnimation(getContext(), R.anim.loading);
        setFooterView(inflate);
        if (this.f) {
            j();
        }
    }

    private void h() {
        setOnScrollListener(new bz(this));
    }

    public final void a(int i2, int i3) {
        if (g.p() && this.k != null && this.j != null && this.j.getGroupCount() != 0) {
            switch (this.j.b(i2, i3)) {
                case 0:
                    this.l = false;
                    return;
                case 1:
                    this.j.a(this.k, i2);
                    if (this.k.getTop() != 0) {
                        this.k.layout(0, 0, this.m, this.n);
                    }
                    this.l = true;
                    if (this.o) {
                        requestLayout();
                        this.o = false;
                        return;
                    }
                    return;
                case 2:
                    int bottom = getChildAt(0).getBottom();
                    int height = this.k.getHeight();
                    int i4 = bottom < height ? bottom - height : 0;
                    this.j.a(this.k, i2);
                    if (this.k.getTop() != i4) {
                        this.k.layout(0, i4, this.m, this.n + i4);
                    }
                    this.l = true;
                    if (this.o) {
                        requestLayout();
                        this.o = false;
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
        LinearLayout linearLayout = null;
        this.i.removeMessages(156);
        linearLayout.setVisibility(8);
        linearLayout.setLayoutParams(new LinearLayout.LayoutParams(-1, 0));
        if (l()) {
            setPreventOverScroll(false);
        }
        setTipVisible(true);
    }

    /* access modifiers changed from: protected */
    public void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);
        if (g.p() && this.l) {
            drawChild(canvas, this.k, getDrawingTime());
        }
    }

    public LoadingButton getFooterViewButton() {
        return this.e;
    }

    public LinearLayout getLoadingContainer() {
        return null;
    }

    public cv getOnCancelListener() {
        return this.h;
    }

    public ExpandableListView.OnChildClickListener getOnChildClickListener() {
        return this.g;
    }

    /* access modifiers changed from: protected */
    public int getRefreshLayout() {
        return R.layout.include_pull_to_refresh;
    }

    /* access modifiers changed from: protected */
    public int getRefreshingLayout() {
        return R.layout.include_pull_to_refreshing_header;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        super.onLayout(z, i2, i3, i4, i5);
        if (g.p()) {
            long expandableListPosition = getExpandableListPosition(getFirstVisiblePosition());
            int packedPositionGroup = ExpandableListView.getPackedPositionGroup(expandableListPosition);
            int packedPositionChild = ExpandableListView.getPackedPositionChild(expandableListPosition);
            if (this.j != null) {
                int b = this.j.b(packedPositionGroup, packedPositionChild);
                if (!(this.k == null || b == this.p)) {
                    this.p = b;
                    this.k.layout(0, 0, this.m, this.n);
                }
            }
            a(packedPositionGroup, packedPositionChild);
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        super.onMeasure(i2, i3);
        if (this.k != null) {
            measureChild(this.k, i2, i3);
            this.m = this.k.getMeasuredWidth();
            this.n = this.k.getMeasuredHeight();
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (this.l) {
            switch (motionEvent.getAction()) {
                case 0:
                    this.q = motionEvent.getX();
                    this.r = motionEvent.getY();
                    if (this.q <= ((float) this.m) && this.r <= ((float) this.n)) {
                        return true;
                    }
                case 1:
                    float x = motionEvent.getX();
                    float y = motionEvent.getY();
                    float abs = Math.abs(x - this.q);
                    float abs2 = Math.abs(y - this.r);
                    if (x <= ((float) this.m) && y <= ((float) this.n) && abs <= ((float) this.m) && abs2 <= ((float) this.n)) {
                        View view = this.k;
                        return true;
                    }
            }
        }
        return super.onTouchEvent(motionEvent);
    }

    public void setAdapter(ExpandableListAdapter expandableListAdapter) {
        super.setAdapter(expandableListAdapter);
        removeFooterView(this.c);
        if (this.f) {
            addFooterView(this.c);
        }
        this.j = (gj) expandableListAdapter;
    }

    public void setEnableLoadMoreFoolter(boolean z) {
        this.f = z;
    }

    public void setLastFlushTime(Date date) {
        TextView textView = null;
        String str = String.valueOf(getContext().getString(R.string.pull_to_refresh_lastTime)) + "：";
        if (date != null) {
            str = String.valueOf(str) + a.a(date);
        }
        textView.setText(str);
        textView.setText(str);
    }

    public void setLoadingViewText(int i2) {
        TextView textView = null;
        textView.setText(i2);
    }

    public void setLoadingVisible(boolean z) {
    }

    public void setMMHeaderView(View view) {
        this.k = view;
        view.setLayoutParams(new AbsListView.LayoutParams(-1, -2));
        if (this.k != null) {
            setFadingEdgeLength(0);
        }
        requestLayout();
    }

    public void setNoMoreRefresh(boolean z) {
    }

    public void setOnCancelListener(cv cvVar) {
        this.h = cvVar;
    }

    public void setOnChildClickListener(ExpandableListView.OnChildClickListener onChildClickListener) {
        super.setOnChildClickListener(onChildClickListener);
        this.g = onChildClickListener;
    }

    public void setOnPullToRefreshListener$13bd663b(q qVar) {
    }

    /* access modifiers changed from: protected */
    public void setOverScrollState(int i2) {
        TextView textView = null;
        if (i2 != this.d) {
            switch (i2) {
                case 1:
                    textView.setText((int) R.string.pull_to_refresh_pull_label);
                    break;
                case 2:
                    textView.setText((int) R.string.pull_to_refresh_release_label);
                    break;
                case 3:
                    textView.setText((int) R.string.pull_to_refresh_refreshing_label);
                    break;
                case 4:
                    textView.setText((int) R.string.pull_to_refresh_pull_label);
                    break;
            }
            this.d = i2;
        }
    }

    public void setTipVisible(boolean z) {
    }
}
