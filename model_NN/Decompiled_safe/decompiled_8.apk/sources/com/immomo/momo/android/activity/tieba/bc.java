package com.immomo.momo.android.activity.tieba;

import android.content.Intent;
import android.view.View;
import com.immomo.momo.android.activity.setting.CommunityBindActivity;

final class bc implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ bb f2179a;
    private final /* synthetic */ int b;

    bc(bb bbVar, int i) {
        this.f2179a = bbVar;
        this.b = i;
    }

    public final void onClick(View view) {
        boolean z = false;
        if (this.b == 5 || this.b == 6) {
            bb bbVar = this.f2179a;
            if (!this.f2179a.f) {
                z = true;
            }
            bbVar.a(z);
        } else if (this.f2179a.g) {
            bb bbVar2 = this.f2179a;
            if (!this.f2179a.f) {
                z = true;
            }
            bbVar2.a(z);
        } else {
            Intent intent = new Intent(this.f2179a.f2178a, CommunityBindActivity.class);
            intent.putExtra("type", this.b);
            this.f2179a.f2178a.startActivityForResult(intent, this.f2179a.i);
        }
    }
}
