package com.immomo.momo.android.map;

final class o implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ GeoAMapActivity f2509a;

    o(GeoAMapActivity geoAMapActivity) {
        this.f2509a = geoAMapActivity;
    }

    public final void run() {
        try {
            this.f2509a.i = ((double) this.f2509a.f.getMapCenter().getLatitudeE6()) / 1000000.0d;
            this.f2509a.j = ((double) this.f2509a.f.getMapCenter().getLongitudeE6()) / 1000000.0d;
            this.f2509a.h.a((Object) ("result latlng: " + this.f2509a.i + "," + this.f2509a.j));
        } catch (Exception e) {
            this.f2509a.h.a((Throwable) e);
        }
    }
}
