package com.immomo.momo.android.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.b.a;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.WebView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.a.a.f.b;
import com.immomo.momo.R;
import com.immomo.momo.android.c.u;
import com.immomo.momo.g;
import com.immomo.momo.util.ay;
import com.immomo.momo.util.jni.Codec;
import java.util.List;
import org.apache.http.util.EncodingUtils;

public class WebviewActivity extends ao {
    private WebView h = null;
    private String i;
    /* access modifiers changed from: private */
    public String j;
    private long k = 0;
    private ay l;

    @SuppressLint({"NewApi"})
    public final boolean a(WebView webView, String str) {
        Uri parse = Uri.parse(str);
        if (parse == null || a.a((CharSequence) parse.getScheme()) || parse.getScheme().equals("http") || parse.getScheme().equals("https") || parse.getScheme().equals("ftp")) {
            return false;
        }
        if (!isDestroyed()) {
            this.e.a((Object) ("intercept -> " + str));
            if (!"immomo.com".equals(parse.getHost())) {
                startActivity(Intent.createChooser(new Intent("android.intent.action.VIEW", parse), this.j == null ? "打开应用" : this.j));
            } else if ("momochat".equals(parse.getScheme())) {
                String queryParameter = parse.getQueryParameter("goto");
                if (!a.a((CharSequence) queryParameter)) {
                    d.a(queryParameter, this);
                }
            } else {
                String queryParameter2 = parse.getQueryParameter("appid");
                Intent intent = new Intent("android.intent.action.VIEW", parse);
                intent.addFlags(268435456);
                List<ResolveInfo> queryIntentActivities = getPackageManager().queryIntentActivities(intent, 32);
                String queryParameter3 = parse.getQueryParameter("apkurl");
                if ((a.a((CharSequence) queryParameter3) || queryIntentActivities != null) && !queryIntentActivities.isEmpty()) {
                    if (System.currentTimeMillis() - this.k < 500) {
                        new Handler().postDelayed(new mv(this, intent), 500 - (System.currentTimeMillis() - this.k));
                    } else {
                        startActivity(Intent.createChooser(intent, this.j == null ? "打开应用" : this.j));
                        finish();
                    }
                    if (!a.a((CharSequence) queryParameter2)) {
                        u.b().execute(new mx(this, queryParameter2));
                    }
                } else {
                    this.e.a((Object) ("downloadUrl=" + queryParameter3));
                    webView.loadUrl(queryParameter3);
                    if (!a.a((CharSequence) queryParameter2)) {
                        u.b().execute(new mw(this, queryParameter2));
                    }
                }
            }
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public final void d() {
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        this.l.a(i2, i3, intent);
    }

    public void onBackPressed() {
        if (this.h.canGoBack()) {
            this.h.goBack();
        } else {
            super.onBackPressed();
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_webview);
        this.k = System.currentTimeMillis();
        Intent intent = getIntent();
        Uri data = intent.getData();
        if (data == null) {
            this.i = intent.getStringExtra("webview_url");
        } else {
            this.i = data.toString();
        }
        this.j = intent.getStringExtra("webview_title");
        if (!a.a((CharSequence) this.j)) {
            setTitle(this.j);
        }
        this.h = (WebView) findViewById(R.id.webview);
        this.h.getSettings().setJavaScriptEnabled(true);
        WebView webView = this.h;
        ay ayVar = new ay(this, this.h);
        this.l = ayVar;
        webView.addJavascriptInterface(ayVar, "aobj");
        this.h.getSettings().setSupportZoom(true);
        this.h.getSettings().setBuiltInZoomControls(true);
        this.h.getSettings().setUseWideViewPort(true);
        if (g.a()) {
            this.h.getSettings().setDisplayZoomControls(false);
        }
        this.h.setOnTouchListener(new my());
        this.h.setDownloadListener(new ms(this));
        this.h.setWebChromeClient(new mt(this));
        this.h.setWebViewClient(new mu(this));
        if (!a.a((CharSequence) this.i)) {
            if (a(this.h, this.i)) {
                if (this.f == null) {
                    startActivity(new Intent(getApplicationContext(), WelcomeActivity.class));
                }
                finish();
            } else if (this.i.startsWith("http://www.immomo.com/api/")) {
                CookieSyncManager.createInstance(this);
                CookieSyncManager.getInstance().startSync();
                CookieManager.getInstance().removeSessionCookie();
                String str = this.f != null ? this.f.h : PoiTypeDef.All;
                String a2 = b.a();
                String gvk = Codec.gvk();
                int z = g.z();
                this.h.postUrl(this.i, EncodingUtils.getBytes("random=" + a2 + "&token=" + a.n(String.valueOf("android") + str + a2 + (a.a(g.s()) ? PoiTypeDef.All : g.s()) + z + gvk) + "&version=" + z + "&client=" + "android" + "&momoid=" + str, "UTF-8"));
            } else {
                this.h.loadUrl(this.i);
            }
            this.e.a((Object) ("url=" + this.i));
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        ay ayVar = this.l;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        ay ayVar = this.l;
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Bundle bundle) {
        super.onRestoreInstanceState(bundle);
        this.l.b(bundle);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        ay ayVar = this.l;
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        this.l.a(bundle);
    }
}
