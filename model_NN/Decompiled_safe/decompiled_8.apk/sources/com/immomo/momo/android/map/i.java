package com.immomo.momo.android.map;

import android.app.Dialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Looper;
import android.support.v4.b.a;
import android.view.View;
import com.amap.mapapi.map.MapActivity;
import com.amap.mapapi.map.MapView;
import com.immomo.momo.MomoApplication;
import com.immomo.momo.R;
import com.immomo.momo.android.view.HeaderLayout;
import com.immomo.momo.service.bean.at;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.util.ao;
import com.immomo.momo.util.m;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class i extends MapActivity {
    private Map b = new HashMap();
    private boolean c = false;
    protected bf d = null;
    /* access modifiers changed from: protected */
    public at e = null;
    private List f = null;
    private Dialog g = null;
    private HeaderLayout h = null;

    public i() {
        new m(getClass().getSimpleName());
    }

    protected static void a(MapView mapView) {
        mapView.setBuiltInZoomControls(true);
        mapView.displayZoomControls(true);
        mapView.setEnabled(true);
        mapView.setClickable(true);
        mapView.getController().setZoom(7);
        mapView.setFocusable(true);
        mapView.setDoubleClickZooming(false);
    }

    protected static void e() {
    }

    static /* synthetic */ void f() {
    }

    public final void a(int i) {
        if (this.c) {
            ao.g(i);
        }
    }

    public final synchronized void a(Dialog dialog) {
        b();
        this.g = dialog;
        if (!isFinishing()) {
            dialog.show();
        }
    }

    public final void a(AsyncTask asyncTask) {
        this.f.add(asyncTask);
        asyncTask.execute(new Object[0]);
    }

    public final synchronized void b() {
        if (this.g != null && this.g.isShowing() && !isFinishing()) {
            this.g.dismiss();
            this.g = null;
        }
    }

    public final boolean c() {
        return this.c;
    }

    public final HeaderLayout d() {
        if (this.h == null) {
            this.h = (HeaderLayout) findViewById(R.id.layout_header);
            if (this.h != null && this.h.getParent() != null && (this.h.getParent() instanceof com.immomo.momo.android.activity.ao) && !this.h.getHeaderSpinner().isShown()) {
                this.h = ((com.immomo.momo.android.activity.ao) getParent()).m();
            }
        }
        return this.h;
    }

    public View findViewById(int i) {
        View view = this.b.get(Integer.valueOf(i)) != null ? (View) ((WeakReference) this.b.get(Integer.valueOf(i))).get() : null;
        if (view != null) {
            return view;
        }
        View findViewById = super.findViewById(i);
        this.b.put(Integer.valueOf(i), new WeakReference(findViewById));
        return findViewById;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.d = ((MomoApplication) getApplication()).a();
        this.e = ((MomoApplication) getApplication()).b();
        this.f = new ArrayList();
        Looper.myQueue().addIdleHandler(new j(this));
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        for (AsyncTask asyncTask : this.f) {
            if (asyncTask != null && !asyncTask.isCancelled()) {
                asyncTask.cancel(true);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.c = false;
    }

    /* access modifiers changed from: protected */
    public void onPostCreate(Bundle bundle) {
        super.onPostCreate(bundle);
        if (d() != null && a.a(d().getTitleText()) && d().getHeaderSpinner().getVisibility() != 0) {
            d().setTitleText(getTitle());
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.c = true;
    }

    public void startActivityForResult(Intent intent, int i) {
        super.startActivityForResult(intent, i);
    }
}
