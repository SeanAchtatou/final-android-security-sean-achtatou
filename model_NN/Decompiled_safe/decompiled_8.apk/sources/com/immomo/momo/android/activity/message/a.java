package com.immomo.momo.android.activity.message;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.view.Display;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.a.a.f.b;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.ImageFactoryActivity;
import com.immomo.momo.android.activity.ah;
import com.immomo.momo.android.view.ChatListView;
import com.immomo.momo.android.view.EmoteInputView;
import com.immomo.momo.android.view.HandyListView;
import com.immomo.momo.android.view.HeaderLayout;
import com.immomo.momo.android.view.MEmoteEditeText;
import com.immomo.momo.android.view.ResizeListenerLayout;
import com.immomo.momo.android.view.ScrollLayout;
import com.immomo.momo.android.view.a.o;
import com.immomo.momo.android.view.au;
import com.immomo.momo.android.view.bi;
import com.immomo.momo.android.view.bn;
import com.immomo.momo.g;
import com.immomo.momo.plugin.audio.d;
import com.immomo.momo.plugin.audio.e;
import com.immomo.momo.plugin.audio.i;
import com.immomo.momo.plugin.audio.j;
import com.immomo.momo.plugin.audio.k;
import com.immomo.momo.plugin.audio.l;
import com.immomo.momo.service.af;
import com.immomo.momo.service.ag;
import com.immomo.momo.service.bean.Message;
import com.immomo.momo.service.bean.bh;
import com.immomo.momo.util.am;
import com.immomo.momo.util.ao;
import com.immomo.momo.util.h;
import com.immomo.momo.util.m;
import com.immomo.momo.util.t;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import mm.purchasesdk.PurchaseCode;

public abstract class a extends ah implements Handler.Callback, View.OnClickListener, View.OnTouchListener, bn {
    protected AudioManager A;
    protected EmoteInputView B = null;
    protected int C;
    protected boolean D = false;
    protected boolean E = true;
    protected List F = new ArrayList();
    protected String G = null;
    protected String H = null;
    protected long I;
    protected File J = null;
    protected boolean K = true;
    private Button L = null;
    private View M;
    private View N = null;
    private MEmoteEditeText O = null;
    private View P;
    private View Q;
    private View R;
    private View S;
    private View T;
    private View U = null;
    private View V = null;
    private View W = null;
    private ResizeListenerLayout X = null;
    private Animation Y = null;
    private ao Z = null;
    private i aa = null;
    private j ab = null;
    private k ac;
    private e ad;
    private aa ae = null;
    private List af = new ArrayList();
    private boolean ag = false;
    private File ah = null;
    private File ai = null;
    private String aj = null;
    private long ak;
    private int al;
    /* access modifiers changed from: private */
    public Runnable am = null;
    private boolean an = false;
    private bh ao;
    protected m h = new m("test_momo", "[--- from BaseMessageActivity ---]");
    protected String i = PoiTypeDef.All;
    protected Handler j = new Handler(this);
    protected ag k;
    protected ChatListView l = null;
    protected HeaderLayout m = null;
    protected bi n;
    protected View o = null;
    protected View p;
    protected View q = null;
    protected View r = null;
    protected View s = null;
    protected ImageView t = null;
    protected ScrollLayout u = null;
    protected LinearLayout v = null;
    protected com.immomo.momo.android.view.a.a w = null;
    protected Bitmap x;
    protected Bitmap y = null;
    protected InputMethodManager z = null;

    private void a(Uri uri) {
        System.gc();
        Intent intent = new Intent(this, ImageFactoryActivity.class);
        intent.setData(uri);
        this.aj = b.a();
        this.ai = new File(com.immomo.momo.a.j(), String.valueOf(this.aj) + ".jpg_");
        intent.putExtra("outputFilePath", this.ai.getAbsolutePath());
        intent.putExtra("process_model", "filter");
        intent.putExtra("maxwidth", 720);
        intent.putExtra("maxheight", 3000);
        startActivityForResult(intent, 12);
    }

    private void ag() {
        String f = this.k.f(this.i);
        this.h.b((Object) ("get a draft : " + f));
        if (android.support.v4.b.a.f(f)) {
            this.O.setText(f);
            d(0);
        }
        if (this.k.g(this.i) == 1) {
            this.u.setToScreen(1);
        }
    }

    private void ah() {
        if (!this.D && !this.B.isShown() && !this.o.isShown()) {
            if (this.Y == null) {
                this.Y = AnimationUtils.loadAnimation(this, R.anim.buttomtip_in);
            }
            this.o.setVisibility(0);
            this.o.setAnimation(this.Y);
            this.Y.start();
        }
    }

    /* access modifiers changed from: private */
    public void ai() {
        v().b();
        v().c();
        this.j.sendEmptyMessage(10020);
    }

    private void b(boolean z2) {
        if (!z2) {
            try {
                Intent intent = new Intent("android.intent.action.GET_CONTENT");
                intent.setType("image/*");
                startActivityForResult(intent, 13);
            } catch (ActivityNotFoundException e) {
                a((CharSequence) "你的手机上未发现相册应用，建议到应用市场安装");
            }
        } else if (Environment.getExternalStorageState().equals("mounted")) {
            this.aj = b.a();
            this.ah = new File(com.immomo.momo.a.j(), String.valueOf(this.aj) + ".jpg_");
            Intent intent2 = new Intent("android.media.action.IMAGE_CAPTURE");
            intent2.putExtra("output", Uri.fromFile(this.ah));
            startActivityForResult(intent2, 11);
        } else {
            a((CharSequence) "手机存储卡不可用,无法发送图片");
        }
    }

    /* access modifiers changed from: private */
    public void d(int i2) {
        if (i2 == 0) {
            this.L.setVisibility(0);
            this.M.setVisibility(8);
            return;
        }
        this.L.setVisibility(8);
        this.M.setVisibility(0);
    }

    static /* synthetic */ void e(a aVar) {
        System.out.println("BaseMessageActivity.sendAudio()");
        aVar.ak = System.currentTimeMillis() - 500;
        long j2 = aVar.ak - aVar.I;
        if (j2 < 1000) {
            aVar.F();
            aVar.a((CharSequence) "录音时长不足1秒");
            return;
        }
        aVar.al = Math.round(((float) j2) / 100.0f);
        String str = aVar.H;
        aVar.f(aVar.c(aVar.al));
        am.a().a(R.raw.ms_voice_stoped);
    }

    /* access modifiers changed from: protected */
    public final void A() {
        this.B.a();
    }

    public final Handler B() {
        return this.j;
    }

    /* access modifiers changed from: protected */
    public final void C() {
        this.P.setEnabled(false);
        this.S.setEnabled(false);
        this.R.setEnabled(false);
        this.T.setEnabled(false);
        this.U.setVisibility(8);
        Animation loadAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.controller_exit);
        loadAnimation.setInterpolator(AnimationUtils.loadInterpolator(this, 17432583));
        this.P.setAnimation(loadAnimation);
        this.P.setVisibility(8);
    }

    /* access modifiers changed from: protected */
    public final void D() {
        Display defaultDisplay = getWindowManager().getDefaultDisplay();
        defaultDisplay.getWidth();
        this.C = defaultDisplay.getHeight();
    }

    /* access modifiers changed from: protected */
    public void E() {
        this.W.setVisibility(8);
        this.A.setMode(0);
        this.A.setSpeakerphoneOn(false);
        Y();
        System.out.println("MOMO BaseMessageActivity.onRecordStart()");
        d.a().b();
        am.a().a(R.raw.ms_voice_stoped);
        try {
            this.H = b.a();
            this.J = h.a(this.H);
            com.immomo.momo.a.C();
            this.aa = l.f();
            this.aa.a(this.J);
            i iVar = this.aa;
            if (this.ab == null) {
                com.immomo.momo.a.C();
                this.ab = new f(this);
            }
            iVar.a(this.ab);
            this.aa.a();
        } catch (IOException e) {
            a((CharSequence) "存储卡不可用，录音失败");
            if (this.w != null) {
                this.w.e();
            }
            this.h.a((Throwable) e);
        }
    }

    /* access modifiers changed from: protected */
    public final void F() {
        if (this.aa != null) {
            this.aa.c();
        }
    }

    /* access modifiers changed from: protected */
    public final void G() {
        if (this.aa != null) {
            this.aa.b();
        }
    }

    /* access modifiers changed from: protected */
    public final void H() {
        System.out.println("BaseMessageActivity.recordAudio()");
        if (this.aa == null || !this.aa.d()) {
            this.h.a((Object) "recordAudio-----------");
            this.t.setImageResource(R.drawable.ic_chatbar_audiobtn_press);
            this.r.setBackgroundResource(R.drawable.bg_chatbar_audio_press);
            this.u.a();
            if (this.w == null || !this.w.d()) {
                this.w = new com.immomo.momo.android.view.a.a(this);
            }
            this.w.a(new d(this));
            this.w.a(new e(this));
            E();
            return;
        }
        this.aa.b();
    }

    public final List I() {
        if (!this.ag) {
            this.ag = true;
            this.af.addAll(ac());
        }
        return this.af;
    }

    /* access modifiers changed from: protected */
    public final void J() {
        if (this.ag) {
            this.ag = false;
            this.af.clear();
        }
    }

    public final void K() {
        this.o.setVisibility(8);
    }

    /* access modifiers changed from: protected */
    public final void L() {
        this.r.setVisibility(8);
    }

    /* access modifiers changed from: protected */
    public final void M() {
        this.r.setVisibility(0);
    }

    public final boolean N() {
        return this.an;
    }

    /* access modifiers changed from: protected */
    public abstract void O();

    /* access modifiers changed from: protected */
    public abstract void P();

    /* access modifiers changed from: protected */
    public abstract void Q();

    /* access modifiers changed from: protected */
    public abstract void R();

    /* access modifiers changed from: protected */
    public abstract void S();

    /* access modifiers changed from: protected */
    public abstract void T();

    /* access modifiers changed from: protected */
    public abstract int U();

    /* access modifiers changed from: protected */
    public abstract void V();

    /* access modifiers changed from: protected */
    public abstract void W();

    /* access modifiers changed from: protected */
    public abstract void X();

    public abstract void Y();

    /* access modifiers changed from: protected */
    public abstract void Z();

    /* access modifiers changed from: protected */
    public abstract Message a(File file);

    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        super.a(bundle);
        setContentView(U());
        this.h.a((Object) "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~MessageActivity created....");
        O();
        x();
        P();
        this.i = u();
        ag();
        Z();
        Q();
        this.ae = new aa(this);
        this.ae.start();
        if (bundle != null) {
            try {
                Object obj = bundle.get("cameraPic");
                if (obj != null) {
                    this.ah = new File(obj.toString());
                }
            } catch (Exception e) {
            }
            try {
                Object obj2 = bundle.get("imageprocessPic");
                if (obj2 != null) {
                    this.ai = new File(obj2.toString());
                }
            } catch (Exception e2) {
            }
            try {
                Object obj3 = bundle.get("picName");
                if (obj3 != null) {
                    this.aj = obj3.toString();
                    this.ai = new File(com.immomo.momo.a.j(), String.valueOf(this.aj) + ".jpg_");
                }
            } catch (Exception e3) {
            }
        }
    }

    public final void a(com.immomo.momo.android.a.a aVar, Message message) {
        if (aVar.f(message) == -1) {
            boolean z2 = this.l.getLastVisiblePosition() < aVar.getCount() + -2;
            aVar.a(message);
            if (z2) {
                ah();
            } else {
                this.l.c();
            }
            if (this.ag && message.contentType == 1) {
                this.af.add(0, message);
            }
        }
    }

    public final void a(com.immomo.momo.android.a.a aVar, List list) {
        if (!list.isEmpty()) {
            long currentTimeMillis = System.currentTimeMillis();
            this.h.a((Object) ("list.size=" + list.size()));
            boolean z2 = this.l.getLastVisiblePosition() < aVar.getCount() + -2;
            Iterator it = list.iterator();
            while (it.hasNext()) {
                Message message = (Message) it.next();
                if (aVar.f(message) == -1) {
                    aVar.b(message);
                    if (this.ag && message.contentType == 1) {
                        this.af.add(0, message);
                    }
                }
            }
            this.h.a((Object) ("list.size=, +" + (System.currentTimeMillis() - currentTimeMillis)));
            aVar.notifyDataSetChanged();
            if (z2) {
                ah();
            } else {
                this.l.c();
            }
        }
    }

    /* access modifiers changed from: protected */
    public abstract boolean a(Message message);

    /* access modifiers changed from: protected */
    public void aa() {
    }

    /* access modifiers changed from: protected */
    public void ab() {
    }

    /* access modifiers changed from: protected */
    public abstract List ac();

    /* access modifiers changed from: protected */
    public abstract Message ad();

    /* access modifiers changed from: protected */
    public final void ae() {
        o oVar = new o(this);
        String[] stringArray = getResources().getStringArray(R.array.chat_audio_type);
        ArrayList arrayList = new ArrayList();
        int i2 = 0;
        while (i2 < stringArray.length) {
            z zVar = new z();
            zVar.f1998a = stringArray[i2];
            zVar.b = i2 == this.g.i;
            arrayList.add(zVar);
            i2++;
        }
        oVar.a(new v(this, arrayList));
        oVar.setTitle((int) R.string.header_audio_type);
        oVar.a();
        oVar.a(new h(this));
        oVar.show();
    }

    public final void af() {
        System.out.println("MOMO BaseMessageActivity.stopAudio()");
        d.a().b();
        new i(this).start();
    }

    public abstract void b(Message message);

    /* access modifiers changed from: protected */
    public abstract Message c(int i2);

    /* access modifiers changed from: protected */
    public abstract Message c(String str);

    /* access modifiers changed from: protected */
    public abstract void c(Message message);

    /* access modifiers changed from: protected */
    public abstract Message d(String str);

    /* access modifiers changed from: protected */
    public void d() {
        R();
    }

    /* access modifiers changed from: protected */
    public abstract void d(Message message);

    /* access modifiers changed from: protected */
    public void e() {
        super.e();
        this.X = (ResizeListenerLayout) findViewById(R.id.layout_root);
        this.l = (ChatListView) findViewById(R.id.chat_listview);
        this.l.setCacheColorHint(0);
        this.P = findViewById(R.id.message_layout_plusbar);
        this.R = findViewById(R.id.message_plusbar_sendpicture);
        this.S = findViewById(R.id.message_plusbar_sendlocation);
        this.T = findViewById(R.id.message_plusbar_sendcamera);
        this.o = findViewById(R.id.message_layout_mask);
        this.U = findViewById(R.id.layout_mask);
        this.Q = this.V.findViewById(R.id.message_btn_openplus);
        this.p = this.V.findViewById(R.id.message_btn_openkeybord);
        this.q = this.V.findViewById(R.id.message_btn_openemotes);
        this.t = (ImageView) findViewById(R.id.message_ib_audiorecord);
        this.W = findViewById(R.id.coverLayout);
        this.B = (EmoteInputView) findViewById(R.id.message_emoteview);
        this.B.setEditText(this.O);
        this.L.setOnClickListener(this);
        this.M.setOnClickListener(this);
        this.N.setOnClickListener(this);
        this.Q.setOnClickListener(this);
        this.p.setOnClickListener(this);
        this.q.setOnClickListener(this);
        this.o.setOnTouchListener(this);
        this.R.setOnClickListener(this);
        this.S.setOnClickListener(this);
        this.T.setOnClickListener(this);
        this.U.setOnTouchListener(this);
        this.l.setOnTouchListener(this);
        this.l.setOnScrollListener(new y(this));
        this.O.setOnTouchListener(this);
        this.O.addTextChangedListener(new k(this));
        findViewById(R.id.message_btn_audio_openplus).setOnClickListener(this);
        findViewById(R.id.message_layout_audiocontainer).setOnTouchListener(this);
        this.u.setOnTouchListener(new ag(this));
        this.u.setSelectedListener(new ac(this));
        this.l.setOnPullToRefreshListener$42b903f6(new l(this));
        this.W.setOnTouchListener(new m());
        this.s.setOnTouchListener(new n());
        findViewById(R.id.message_layout_audiocontainer).setOnLongClickListener(new o(this));
        this.X.setOnResizeListener(new p(this));
        this.B.setOnEmoteSelectedListener(new q(this));
        S();
        d();
        this.G = getIntent().getStringExtra("from");
    }

    public final void e(Message message) {
        String str = "@" + message.remoteUser.i();
        if (!this.O.getText().toString().contains(str)) {
            this.O.append(String.valueOf(str) + " ");
        }
    }

    /* access modifiers changed from: protected */
    public final void e(String str) {
        this.ao = new bh(str);
        b(new r(this, this, this.ao));
    }

    /* access modifiers changed from: protected */
    public void f(Message message) {
        if (message != null) {
            if (this.ag && message.contentType == 1) {
                this.af.add(0, message);
            }
            try {
                if (this.l != null) {
                    this.l.c();
                }
                this.ae.a(message);
            } catch (Throwable th) {
                this.h.a(th);
            }
        }
    }

    public final void g(Message message) {
        System.out.println("BaseMessageActivity.playAudio()");
        if (this.aa == null || !this.aa.d()) {
            if (message.receive && !message.isAudioPlayed) {
                message.isAudioPlayed = true;
                af.c().b(message);
            }
            d a2 = d.a();
            a2.a(message, w());
            a2.a(0);
            Y();
        }
    }

    public boolean handleMessage(android.os.Message message) {
        switch (message.what) {
            case 330:
                this.B.a();
                this.p.setVisibility(8);
                this.q.setVisibility(0);
                return true;
            case 331:
                z();
                return true;
            case 332:
                this.p.setVisibility(8);
                this.q.setVisibility(0);
                return true;
            case 333:
                this.p.setVisibility(8);
                this.q.setVisibility(0);
                return true;
            case 338:
                if (!this.O.isFocused()) {
                    this.O.requestFocus();
                    this.O.setSelection(this.O.getText().length());
                }
                this.O.dispatchKeyEvent(new KeyEvent(0, 67));
                return true;
            case PurchaseCode.BILL_DIALOG_SHOWERROR /*402*/:
                Y();
                return true;
            case 10002:
                if (this.u.getCurrentScreen() != 1) {
                    return true;
                }
                this.j.sendEmptyMessageDelayed(10025, 700);
                if (this.Z == null) {
                    return true;
                }
                this.Z.a(getString(R.string.message_not_bothfollow));
                return true;
            case 10019:
                this.W.setVisibility(0);
                return true;
            case 10020:
                this.W.setVisibility(8);
                return true;
            case 10021:
                System.out.println("BaseMessageActivity.handleMessage()");
                if (this.aa == null || !this.aa.d()) {
                    return true;
                }
                this.w.a(this.aa.e());
                this.j.sendEmptyMessageDelayed(10021, 70);
                return true;
            case 10025:
                this.E = false;
                this.u.a(0);
                return true;
            default:
                return false;
        }
    }

    /* access modifiers changed from: protected */
    public final boolean k() {
        return true;
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        Uri data;
        super.onActivityResult(i2, i3, intent);
        this.h.a((Object) ("onActivityResult------------requestCode=" + i2 + ", resultCode=" + i3 + ", data=" + intent));
        if (i3 != -1) {
            a((CharSequence) ("图片处理失败[" + i2 + "-" + i3 + "]"));
        } else if (i2 == 13) {
            if (intent != null && (data = intent.getData()) != null) {
                a(data);
            }
        } else if (i2 == 11) {
            this.h.a((Object) ("cameraPic=" + this.ah));
            if (this.ah == null || !this.ah.exists()) {
                a((CharSequence) "拍照程序错误");
            } else {
                a(Uri.fromFile(this.ah));
            }
        } else if (i2 == 12) {
            if (this.ah != null && this.ah.exists()) {
                this.ah.delete();
                this.ah = null;
            }
            if (this.ai != null && this.ai.exists()) {
                f(a(this.ai));
            }
            getWindow().getDecorView().requestFocus();
        }
    }

    public void onBackPressed() {
        if (!r()) {
            super.onBackPressed();
        } else if (this.W.isShown()) {
            this.W.setVisibility(8);
        } else if (this.P.isShown()) {
            C();
        } else if (this.B.isShown()) {
            this.B.a();
            this.p.setVisibility(8);
            this.q.setVisibility(0);
            this.D = false;
        } else if (this.w == null || !this.w.d()) {
            super.onBackPressed();
        } else {
            this.w.e();
            F();
        }
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.message_btn_sendtext /*2131165307*/:
                String trim = this.O.getText().toString().trim();
                this.O.getText().clear();
                if (android.support.v4.b.a.f(trim)) {
                    f(c(trim));
                    return;
                }
                return;
            case R.id.message_btn_openplus /*2131165308*/:
            case R.id.message_btn_audio_openplus /*2131166303*/:
                if (this.P.getVisibility() == 0) {
                    C();
                    return;
                }
                this.P.setEnabled(true);
                this.S.setEnabled(true);
                this.R.setEnabled(true);
                this.T.setEnabled(true);
                this.P.setAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.controller_enter));
                this.P.setVisibility(0);
                this.U.setVisibility(0);
                return;
            case R.id.message_btn_openemotes /*2131165309*/:
                this.h.a((Object) "showEmoteLayout.....");
                z();
                if (this.D) {
                    this.j.postDelayed(new c(this), 300);
                } else {
                    this.B.b();
                    K();
                }
                this.p.setVisibility(0);
                this.q.setVisibility(8);
                return;
            case R.id.message_btn_openkeybord /*2131165310*/:
                this.O.requestFocus();
                this.z.showSoftInput(this.O, 1);
                this.B.a();
                this.p.setVisibility(8);
                this.q.setVisibility(0);
                return;
            case R.id.message_plusbar_sendlocation /*2131165315*/:
                C();
                f(ad());
                return;
            case R.id.message_plusbar_sendpicture /*2131165317*/:
                C();
                b(false);
                return;
            case R.id.message_plusbar_sendcamera /*2131165318*/:
                C();
                b(true);
                return;
            case R.id.message_btn_audio_back /*2131166304*/:
                this.u.a(0);
                return;
            case R.id.message_btn_gotoaudio /*2131166313*/:
                this.u.a(1);
                return;
            default:
                return;
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        D();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.an = true;
        this.ae.a(new af());
        au.m();
        ImageView imageView = (ImageView) findViewById(R.id.chat_iv_background);
        Drawable drawable = imageView.getDrawable();
        if (drawable instanceof BitmapDrawable) {
            Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
            imageView.setImageBitmap(null);
            if (bitmap != null) {
                com.immomo.momo.util.j.a(b.a(), bitmap);
            }
        }
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        int i3 = 0;
        if (r()) {
            switch (i2) {
                case 24:
                    AudioManager audioManager = this.A;
                    if (this.A.getMode() != 2) {
                        i3 = 3;
                    }
                    audioManager.adjustStreamVolume(i3, 1, 1);
                    return true;
                case 25:
                    AudioManager audioManager2 = this.A;
                    if (this.A.getMode() != 2) {
                        i3 = 3;
                    }
                    audioManager2.adjustStreamVolume(i3, -1, 1);
                    return true;
            }
        }
        return super.onKeyDown(i2, keyEvent);
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        this.K = true;
        if (!android.support.v4.b.a.f(this.i) || !this.i.equals(u())) {
            try {
                P();
                this.i = u();
                ag();
                Z();
                Q();
                d();
            } catch (Throwable th) {
                this.h.a(th);
                finish();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.Z = null;
        String trim = this.O.getText().toString().trim();
        int currentScreen = this.u.getCurrentScreen();
        if (r()) {
            this.W.setVisibility(8);
            this.A.setMode(0);
            this.A.setSpeakerphoneOn(false);
            d.a().b();
            d.a().d();
            ai();
            if (this.aa != null && this.aa.d()) {
                this.aa.b();
            }
            if (this.w != null && this.w.d()) {
                this.w.e();
                F();
            }
            if (this.w != null && this.w.d()) {
                this.w.e();
                F();
            }
            new Thread(new j(this, trim, currentScreen)).start();
        }
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Bundle bundle) {
        super.onRestoreInstanceState(bundle);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (r()) {
            Y();
            this.D = this.B.isShown();
            z();
            getWindow().getDecorView().requestFocus();
            if (this.F.size() > 0) {
                T();
            }
        } else {
            this.D = false;
        }
        this.Z = t.a(g.a(75.0f));
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        if (this.ah != null) {
            bundle.putString("cameraPic", this.ah.getAbsolutePath());
        }
        if (this.ai != null) {
            bundle.putString("imageprocessPic", this.ai.getAbsolutePath());
        }
        if (!android.support.v4.b.a.a((CharSequence) this.aj)) {
            bundle.putString("picName", this.aj);
        }
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        switch (view.getId()) {
            case R.id.layout_mask /*2131165200*/:
                if (motionEvent.getAction() == 0) {
                    C();
                    return true;
                }
                break;
            case R.id.message_ed_msgeditor /*2131165306*/:
                if (motionEvent.getAction() == 1) {
                    this.B.a();
                    this.q.setVisibility(0);
                    this.p.setVisibility(8);
                    this.u.requestLayout();
                    break;
                }
                break;
            case R.id.chat_listview /*2131165426*/:
                if ((this.B.isShown() || this.D) && 1 == motionEvent.getAction() && motionEvent.getEventTime() - motionEvent.getDownTime() < 100) {
                    if (this.B.isShown()) {
                        this.B.a();
                    } else {
                        z();
                    }
                    this.p.setVisibility(8);
                    this.q.setVisibility(0);
                    this.D = false;
                    break;
                }
            case R.id.message_layout_audiocontainer /*2131166302*/:
                if (motionEvent.getAction() == 1) {
                    if (this.w == null || !this.w.d()) {
                        return false;
                    }
                    this.w.e();
                    G();
                    return true;
                }
                break;
            case R.id.message_layout_mask /*2131166311*/:
                if (motionEvent.getAction() == 0) {
                    this.l.c();
                    K();
                    return true;
                }
                break;
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public abstract String u();

    public final k v() {
        if (this.ac == null) {
            this.ac = new k();
        }
        return this.ac;
    }

    public final e w() {
        if (this.ad == null) {
            this.ad = new b(this);
        }
        return this.ad;
    }

    /* access modifiers changed from: protected */
    public void x() {
        this.u = (ScrollLayout) findViewById(R.id.message_editor_gallery);
        this.V = findViewById(R.id.message_layout_editor_text);
        this.O = (MEmoteEditeText) this.V.findViewById(R.id.message_ed_msgeditor);
        this.r = findViewById(R.id.message_layout_editor);
        this.s = findViewById(R.id.layout_cover_record);
        this.L = (Button) this.V.findViewById(R.id.message_btn_sendtext);
        this.M = this.V.findViewById(R.id.message_btn_gotoaudio);
        this.N = findViewById(R.id.message_btn_audio_back);
        this.x = android.support.v4.b.a.d(getResources().getColor(R.color.msg_short_line_normal));
        this.y = android.support.v4.b.a.d(getResources().getColor(R.color.msg_short_line_selected));
        this.v = (LinearLayout) findViewById(R.id.message_layout_rounds);
        for (int i2 = 0; i2 < this.u.getChildCount(); i2++) {
            ImageView imageView = (ImageView) g.o().inflate((int) R.layout.include_message_shortline, (ViewGroup) null);
            imageView.setImageBitmap(this.x);
            this.v.addView(imageView);
        }
        this.m = (HeaderLayout) findViewById(R.id.layout_header);
        HeaderLayout headerLayout = this.m;
        bi a2 = new bi(this).a((int) R.drawable.ic_topbar_more);
        this.n = a2;
        headerLayout.a(a2, new ab(this, (byte) 0));
        this.m.a(new bi(this).a((int) R.drawable.ic_topbar_profile), new ae(this, (byte) 0));
        this.G = getIntent().getStringExtra("from");
    }

    public final HandyListView y() {
        return this.l;
    }

    /* access modifiers changed from: protected */
    public final void z() {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService("input_method");
        View currentFocus = getCurrentFocus();
        if (currentFocus != null) {
            inputMethodManager.hideSoftInputFromWindow(currentFocus.getWindowToken(), 2);
        }
    }
}
