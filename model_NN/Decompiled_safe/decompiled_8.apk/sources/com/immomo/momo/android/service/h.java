package com.immomo.momo.android.service;

import android.content.Context;
import com.immomo.momo.a;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.c.u;
import com.immomo.momo.b;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.am;
import java.io.File;

final class h extends d {

    /* renamed from: a  reason: collision with root package name */
    private u f2608a = null;
    private /* synthetic */ Initializer c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public h(Initializer initializer, Context context) {
        super(context);
        this.c = initializer;
    }

    private void a(String str, int i) {
        String str2 = i == 11 ? String.valueOf(str) + "_s2" : String.valueOf(str) + "_l";
        if (b.a(str2) == null && !new File(a.z(), String.valueOf(str2) + ".png_").exists()) {
            this.f2608a.execute(new i(str, i));
        }
    }

    /* access modifiers changed from: protected */
    public final Object a(Object... objArr) {
        for (am amVar : b.b()) {
            a(amVar.d, 11);
            a(amVar.d, 12);
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        if ("wifi".equals(g.Y())) {
            this.f2608a = new u(5, 5);
        } else {
            this.f2608a = new u(2, 2);
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.c.c = true;
        this.c.b();
    }
}
