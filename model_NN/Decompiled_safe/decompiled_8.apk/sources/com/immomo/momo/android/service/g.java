package com.immomo.momo.android.service;

import com.immomo.momo.service.bean.ak;
import java.io.File;
import java.io.FileFilter;
import java.util.Calendar;
import java.util.Date;

final class g implements FileFilter {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ f f2607a;
    private final /* synthetic */ Date b;
    private final /* synthetic */ File c;
    private final /* synthetic */ boolean d;

    g(f fVar, Date date, File file, boolean z) {
        this.f2607a = fVar;
        this.b = date;
        this.c = file;
        this.d = z;
    }

    public final boolean accept(File file) {
        if (file.isFile() && file.getName().endsWith(".jpg_")) {
            Date date = null;
            ak akVar = (ak) this.f2607a.f2606a.get(file.getAbsolutePath());
            if (akVar == null) {
                if (new Date(file.lastModified()).before(this.b)) {
                    file.delete();
                } else {
                    Calendar instance = Calendar.getInstance();
                    instance.add(6, -12);
                    date = instance.getTime();
                }
            }
            String name = file.getName();
            File file2 = new File(this.c, name.substring(0, 1));
            if (!file2.exists()) {
                file2.mkdirs();
            }
            File file3 = new File(file2, name);
            if (file.renameTo(file3)) {
                if (akVar != null) {
                    akVar.b = file3.getAbsolutePath();
                    if ((this.d && akVar.f2990a.endsWith("_s")) || (!this.d && akVar.f2990a.endsWith("_l"))) {
                        this.f2607a.c.add(akVar.f2990a);
                    }
                } else {
                    akVar = new ak();
                    akVar.b = file.getAbsolutePath();
                    akVar.e = date;
                    if (this.d) {
                        akVar.f2990a = String.valueOf(akVar.f2990a) + "_s";
                    } else {
                        akVar.f2990a = String.valueOf(akVar.f2990a) + "_l";
                    }
                }
                akVar.f2990a = name.substring(0, name.lastIndexOf(".jpg_"));
                if (this.d) {
                    akVar.f2990a = String.valueOf(akVar.f2990a) + "_s";
                } else {
                    akVar.f2990a = String.valueOf(akVar.f2990a) + "_l";
                }
                this.f2607a.e.b(akVar);
            } else {
                file.delete();
            }
        }
        return false;
    }
}
