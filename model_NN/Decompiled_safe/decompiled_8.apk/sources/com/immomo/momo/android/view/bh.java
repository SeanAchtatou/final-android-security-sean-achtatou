package com.immomo.momo.android.view;

import android.view.View;
import android.widget.AdapterView;

public final class bh implements AdapterView.OnItemLongClickListener {

    /* renamed from: a  reason: collision with root package name */
    private AdapterView.OnItemLongClickListener f2742a = null;
    private /* synthetic */ HandyListView b;

    public bh(HandyListView handyListView, AdapterView.OnItemLongClickListener onItemLongClickListener) {
        this.b = handyListView;
        this.f2742a = onItemLongClickListener;
    }

    public final boolean onItemLongClick(AdapterView adapterView, View view, int i, long j) {
        if (this.f2742a == null) {
            return false;
        }
        int headerViewsCount = i - this.b.getHeaderViewsCount();
        if (this.b.t || this.b.j == null || headerViewsCount < 0 || headerViewsCount >= this.b.j.getCount()) {
            return true;
        }
        return this.f2742a.onItemLongClick(adapterView, view, headerViewsCount, j);
    }
}
