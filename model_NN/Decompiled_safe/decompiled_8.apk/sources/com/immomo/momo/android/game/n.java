package com.immomo.momo.android.game;

import android.text.Editable;
import android.text.TextWatcher;

final class n implements TextWatcher {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ k f2438a;

    n(k kVar) {
        this.f2438a = kVar;
    }

    public final void afterTextChanged(Editable editable) {
    }

    public final void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public final void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        this.f2438a.W.b.e = charSequence.toString();
    }
}
