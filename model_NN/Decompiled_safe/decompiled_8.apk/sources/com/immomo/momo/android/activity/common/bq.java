package com.immomo.momo.android.activity.common;

import android.content.Context;
import android.support.v4.b.a;
import com.immomo.momo.android.c.d;
import com.immomo.momo.protocol.a.n;
import java.util.ArrayList;
import java.util.Date;

final class bq extends d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ba f1110a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public bq(ba baVar, Context context) {
        super(context);
        this.f1110a = baVar;
    }

    /* access modifiers changed from: protected */
    public final Object a(Object... objArr) {
        this.f1110a.Q = new ArrayList();
        this.f1110a.R = new ArrayList();
        n.a().a(this.f1110a.Q, this.f1110a.R);
        this.f1110a.U.a(this.f1110a.R);
        ba.a(this.f1110a, this.f1110a.R.size());
        ba.b(this.f1110a, this.f1110a.Q.size());
        return null;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
    }

    /* access modifiers changed from: protected */
    public final void a(Object obj) {
        super.a(obj);
        if (!(this.f1110a.Q.size() == 0 && this.f1110a.R.size() == 0)) {
            this.f1110a.O.setLastFlushTime(this.f1110a.S);
            this.f1110a.N.c("lasttime_mygroups_success", a.c(this.f1110a.S));
        }
        this.f1110a.P.a(this.f1110a.Q, this.f1110a.R);
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.f1110a.S = new Date();
        this.f1110a.N.c("lasttime_my_grouplist", a.c(this.f1110a.S));
        this.f1110a.O.n();
    }
}
