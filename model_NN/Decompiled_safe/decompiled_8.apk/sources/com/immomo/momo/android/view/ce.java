package com.immomo.momo.android.view;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import com.immomo.momo.android.a.a;
import java.util.List;

public abstract class ce extends a {
    private cd d = null;

    public ce(Context context, List list) {
        super(context, list);
    }

    public abstract View a(int i, View view);

    /* access modifiers changed from: package-private */
    public final void a(cd cdVar) {
        this.d = cdVar;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        View a2 = a(i, view);
        if (this.d != null) {
            if (this.d.b() != -1) {
                this.d.a(a2.findViewById(this.d.b()), i);
            } else {
                this.d.a(a2, i);
            }
        }
        return a2;
    }
}
