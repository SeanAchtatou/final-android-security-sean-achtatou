package com.immomo.momo.android.activity.contacts;

import android.content.Context;
import com.immomo.momo.R;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.w;
import com.immomo.momo.service.bean.e;
import com.immomo.momo.service.bean.f;
import java.util.ArrayList;
import java.util.Collections;

final class z extends d {

    /* renamed from: a  reason: collision with root package name */
    private v f1203a = null;
    /* access modifiers changed from: private */
    public /* synthetic */ CommunityPeopleActivity c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public z(CommunityPeopleActivity communityPeopleActivity, Context context) {
        super(context);
        this.c = communityPeopleActivity;
        if (communityPeopleActivity.n != null) {
            communityPeopleActivity.n.cancel(true);
        }
        communityPeopleActivity.n = this;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        ArrayList<e> arrayList = new ArrayList<>();
        switch (this.c.i) {
            case 1:
                if (this.c.j != 11) {
                    if (this.c.j == 22) {
                        w.a().b(arrayList, 2);
                        break;
                    }
                } else {
                    w.a().b(arrayList, 1);
                    break;
                }
                break;
            case 2:
                if (this.c.j != 11) {
                    if (this.c.j == 22) {
                        w.a().c(arrayList, 2);
                        break;
                    }
                } else {
                    w.a().c(arrayList, 1);
                    break;
                }
                break;
            case 3:
                if (this.c.j != 11) {
                    if (this.c.j == 22) {
                        w.a().d(arrayList, 2);
                        break;
                    }
                } else {
                    w.a().d(arrayList, 1);
                    break;
                }
                break;
        }
        f fVar = new f();
        f fVar2 = new f();
        f fVar3 = new f();
        for (e eVar : arrayList) {
            switch (eVar.b) {
                case 1:
                    fVar.b.add(eVar);
                    break;
                case 2:
                    fVar3.b.add(eVar);
                    break;
                case 3:
                    fVar2.b.add(eVar);
                    break;
            }
        }
        fVar.f3024a = String.valueOf(fVar.b.size()) + this.c.getString(R.string.contact_grouptitle1);
        fVar2.f3024a = String.valueOf(fVar2.b.size()) + this.c.getString(R.string.contact_grouptitle3);
        Collections.sort(fVar.b, this.c.h);
        Collections.sort(fVar2.b, this.c.h);
        this.c.m.add(fVar);
        if (this.c.j == 11) {
            this.c.m.add(fVar2);
        }
        fVar3.f3024a = String.valueOf(fVar3.b.size()) + this.c.getString(R.string.contact_grouptitle2);
        Collections.sort(fVar3.b, this.c.h);
        this.c.m.add(fVar3);
        return null;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
        this.f1203a = new v(this.c);
        this.f1203a.a("请求提交中...");
        this.f1203a.setCancelable(true);
        this.f1203a.setOnCancelListener(new aa(this));
        this.c.a(this.f1203a);
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        super.a(exc);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        CommunityPeopleActivity.e(this.c);
    }

    /* access modifiers changed from: protected */
    public final void b() {
        super.b();
        this.c.p();
    }
}
