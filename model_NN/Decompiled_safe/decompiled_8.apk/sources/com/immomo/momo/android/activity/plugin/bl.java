package com.immomo.momo.android.activity.plugin;

import android.graphics.Bitmap;
import com.immomo.momo.R;

final class bl implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ bk f2066a;
    private final /* synthetic */ Bitmap b;

    bl(bk bkVar, Bitmap bitmap) {
        this.f2066a = bkVar;
        this.b = bitmap;
    }

    public final void run() {
        if (this.b != null) {
            this.f2066a.f2065a.h.setImageBitmap(this.b);
        } else {
            this.f2066a.f2065a.h.setImageResource(R.drawable.ic_common_def_header);
        }
    }
}
