package com.immomo.momo.android.activity;

import android.os.AsyncTask;
import com.immomo.momo.R;
import com.immomo.momo.a.a;
import com.immomo.momo.a.w;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.d;

final class kp extends AsyncTask {

    /* renamed from: a  reason: collision with root package name */
    private v f1794a;
    private /* synthetic */ SharePageActivity b;

    public kp(SharePageActivity sharePageActivity) {
        this.b = sharePageActivity;
        this.f1794a = new v(sharePageActivity);
        this.f1794a.a("请求提交中");
        this.f1794a.setCancelable(true);
    }

    private String a() {
        try {
            d.a(this.b.f);
            return "yes";
        } catch (w e) {
            this.b.e.a((Throwable) e);
            this.b.b((int) R.string.errormsg_network_unfind);
            return "no";
        } catch (a e2) {
            this.b.e.a((Throwable) e2);
            this.b.b((CharSequence) e2.getMessage());
            return "no";
        } catch (Exception e3) {
            this.b.e.a((Throwable) e3);
            this.b.b((int) R.string.errormsg_server);
            return "no";
        }
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object doInBackground(Object... objArr) {
        return a();
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void onPostExecute(Object obj) {
        String str = (String) obj;
        super.onPostExecute(str);
        if (this.f1794a != null) {
            this.f1794a.dismiss();
        }
        if (str.equals("yes")) {
            this.b.a((CharSequence) "分享成功");
            this.b.setResult(-1, this.b.v());
            this.b.finish();
        }
    }

    /* access modifiers changed from: protected */
    public final void onPreExecute() {
        if (this.f1794a != null) {
            this.f1794a.setOnCancelListener(new kq(this));
            this.f1794a.show();
        }
        super.onPreExecute();
    }
}
