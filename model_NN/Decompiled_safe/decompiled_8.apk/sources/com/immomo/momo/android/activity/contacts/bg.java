package com.immomo.momo.android.activity.contacts;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.android.a.a;
import java.util.List;

final class bg extends a {
    public bg(Context context, List list) {
        super(context, list);
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = this.c.inflate((int) R.layout.listitem_dialog, (ViewGroup) null);
        }
        ((TextView) view.findViewById(R.id.textview)).setText(((bj) getItem(i)).f1165a);
        view.findViewById(R.id.imageview).setVisibility(((bj) getItem(i)).b ? 0 : 8);
        return view;
    }
}
