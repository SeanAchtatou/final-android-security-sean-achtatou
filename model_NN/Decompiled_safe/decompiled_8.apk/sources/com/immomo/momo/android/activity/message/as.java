package com.immomo.momo.android.activity.message;

import com.immomo.momo.protocol.a.w;
import com.immomo.momo.service.bean.bf;

final class as implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ChatActivity f1926a;
    private final /* synthetic */ bf b;

    as(ChatActivity chatActivity, bf bfVar) {
        this.f1926a = chatActivity;
        this.b = bfVar;
    }

    public final void run() {
        try {
            w.a().b(this.b, this.b.h);
            this.f1926a.O.d(this.b);
            this.f1926a.E = "both".equals(this.b.P);
            this.f1926a.ag();
            this.f1926a.Y();
            if (!this.f1926a.E) {
                this.f1926a.j.sendEmptyMessage(10002);
            }
        } catch (Exception e) {
            this.f1926a.L.a((Throwable) e);
        }
    }
}
