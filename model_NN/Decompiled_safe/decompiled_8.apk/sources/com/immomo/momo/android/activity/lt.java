package com.immomo.momo.android.activity;

import android.support.v4.view.u;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.immomo.momo.R;
import com.immomo.momo.g;

final class lt extends u {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ UserGuideActivity f1819a;

    private lt(UserGuideActivity userGuideActivity) {
        this.f1819a = userGuideActivity;
    }

    /* synthetic */ lt(UserGuideActivity userGuideActivity, byte b) {
        this(userGuideActivity);
    }

    public final Object a(ViewGroup viewGroup, int i) {
        View inflate = g.o().inflate((int) R.layout.include_usergui_item, (ViewGroup) null);
        ImageView imageView = (ImageView) inflate.findViewById(R.id.userguiitem_iv_top);
        ImageView imageView2 = (ImageView) inflate.findViewById(R.id.userguiitem_iv_below);
        if (this.f1819a.j.getValue(i, this.f1819a.h) && this.f1819a.h.resourceId > 0) {
            imageView.setImageDrawable(this.f1819a.j.getDrawable(i));
        }
        if (i >= this.f1819a.k.length()) {
            imageView2.setVisibility(8);
        } else if (this.f1819a.k.getValue(i, this.f1819a.h) && this.f1819a.h.resourceId > 0) {
            imageView2.setImageDrawable(this.f1819a.k.getDrawable(i));
        }
        if (i < this.f1819a.l.length() && this.f1819a.l.getValue(i, this.f1819a.h) && this.f1819a.h.resourceId > 0) {
            inflate.setBackgroundDrawable(this.f1819a.l.getDrawable(i));
        }
        View findViewById = inflate.findViewById(R.id.userguiitem_btn_enter);
        if (i == b() - 1) {
            findViewById.setVisibility(0);
            findViewById.setOnClickListener(this.f1819a);
        } else {
            findViewById.setVisibility(8);
        }
        viewGroup.addView(inflate);
        return inflate;
    }

    public final void a(ViewGroup viewGroup, int i, Object obj) {
        viewGroup.removeView((View) obj);
    }

    public final boolean a(View view, Object obj) {
        return view == obj;
    }

    public final int b() {
        return this.f1819a.j.length();
    }
}
