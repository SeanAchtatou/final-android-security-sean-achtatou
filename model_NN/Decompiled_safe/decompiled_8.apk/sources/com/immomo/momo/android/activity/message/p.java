package com.immomo.momo.android.activity.message;

import com.immomo.momo.android.view.cx;

final class p implements cx {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ a f1989a;

    p(a aVar) {
        this.f1989a = aVar;
    }

    public final void a(int i, int i2) {
        this.f1989a.h.a((Object) ("OnResize..  h:" + i + ", oldh:" + i2));
        if (i < i2) {
            if (((double) i) <= ((double) this.f1989a.C) * 0.8d) {
                this.f1989a.D = true;
            }
        } else if (((double) i2) <= ((double) this.f1989a.C) * 0.8d && this.f1989a.D && !this.f1989a.B.isShown()) {
            this.f1989a.D = false;
        }
    }
}
