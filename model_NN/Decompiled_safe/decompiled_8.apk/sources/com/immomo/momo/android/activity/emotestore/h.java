package com.immomo.momo.android.activity.emotestore;

import android.content.DialogInterface;

final class h implements DialogInterface.OnCancelListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ g f1321a;

    h(g gVar) {
        this.f1321a = gVar;
    }

    public final void onCancel(DialogInterface dialogInterface) {
        this.f1321a.cancel(true);
        this.f1321a.f.finish();
    }
}
