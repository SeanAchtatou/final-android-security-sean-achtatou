package com.immomo.momo.android.view;

import android.os.Build;
import android.view.View;
import com.immomo.momo.R;
import com.immomo.momo.service.bean.c.f;

final class df implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ TiebaCategoryGuidView f2775a;
    private final /* synthetic */ View b;
    private final /* synthetic */ int c;

    df(TiebaCategoryGuidView tiebaCategoryGuidView, View view, int i) {
        this.f2775a = tiebaCategoryGuidView;
        this.b = view;
        this.c = i;
    }

    public final void onClick(View view) {
        View findViewById = this.b.findViewById(R.id.tiebacategory_item_img_click);
        View findViewById2 = this.b.findViewById(R.id.tiebacategory_item_img_layout);
        if (this.f2775a.b.contains(((f) this.f2775a.a(this.c)).f3021a)) {
            this.f2775a.b.remove(((f) this.f2775a.a(this.c)).f3021a);
            if (Build.VERSION.SDK_INT > 10) {
                TiebaCategoryGuidView tiebaCategoryGuidView = this.f2775a;
                TiebaCategoryGuidView.a(findViewById, findViewById2);
            } else {
                findViewById.setVisibility(8);
                findViewById2.setBackgroundResource(R.drawable.bglistitem_selector_tiebaguide_select);
            }
            this.f2775a.b.remove(((f) this.f2775a.a(this.c)).f3021a);
            return;
        }
        if (Build.VERSION.SDK_INT > 10) {
            TiebaCategoryGuidView tiebaCategoryGuidView2 = this.f2775a;
            TiebaCategoryGuidView.b(findViewById, findViewById2);
        } else {
            findViewById.setVisibility(0);
            findViewById2.setBackgroundResource(R.drawable.bg_cover_tiebaguide_outerpress);
        }
        this.f2775a.b.add(((f) this.f2775a.a(this.c)).f3021a);
    }
}
