package com.immomo.momo.android.a.a;

import android.view.View;
import android.view.ViewGroup;
import com.immomo.momo.R;
import com.immomo.momo.android.a.a;
import com.immomo.momo.service.bean.Message;
import com.immomo.momo.util.m;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class aa extends a {
    private WeakReference d;
    public Map e;
    protected Map f;
    private Date g = null;
    private Message h;

    public aa(com.immomo.momo.android.activity.message.a aVar) {
        super(aVar, new ArrayList());
        new m("MessageAdapter");
        this.e = null;
        this.h = null;
        ab.m = null;
        this.d = new WeakReference(aVar);
        new HashMap();
        this.e = new HashMap();
        ab.j = new ArrayList();
        this.f = new HashMap();
    }

    private void a(List list) {
        if ((list != null) && (list.size() > 0)) {
            this.h = (Message) list.get(0);
            if (this.h.timestamp == null) {
                this.h.timestamp = new Date(0);
            }
            this.f.put(this.h.msgId, Float.valueOf(this.h.getDiatance()));
            for (int i = 1; i < list.size(); i++) {
                Message message = (Message) list.get(i);
                if (message.timestamp != null && message.timestamp.getTime() - this.h.timestamp.getTime() >= 300000) {
                    this.h = message;
                }
                this.f.put(this.h.msgId, Float.valueOf(message.getDiatance()));
            }
        }
    }

    private void c(Message message) {
        if (getCount() == 0) {
            this.h = message;
            if (this.h.timestamp == null) {
                this.h.timestamp = new Date();
            }
            this.f.put(this.h.msgId, Float.valueOf(message.getDiatance()));
            return;
        }
        if (message.timestamp != null && message.timestamp.getTime() - this.h.timestamp.getTime() >= 300000) {
            this.h = message;
        }
        this.f.put(this.h.msgId, Float.valueOf(message.getDiatance()));
    }

    private void d(Message message) {
        if (this.f.get(message.msgId) != null) {
            if (getCount() > f(message) + 1) {
                if (this.f.get(((Message) getItem(f(message) + 1)).msgId) == null) {
                    this.f.put(((Message) getItem(f(message) + 1)).msgId, (Float) this.f.remove(message.msgId));
                }
            } else if (getCount() != f(message) + 1 || getCount() <= 1) {
                this.f.remove(message.msgId);
            } else {
                this.f.remove(message.msgId);
                this.h = (Message) getItem(f(message) - 1);
            }
        }
    }

    public final void a(int i, Collection collection) {
        a((List) collection);
        super.a(i, collection);
    }

    public final void a(Message message) {
        c(message);
        super.a((Object) message);
    }

    public final /* bridge */ /* synthetic */ void a(Object... objArr) {
        super.a((Object[]) ((Message[]) objArr));
    }

    public final void b(int i) {
        d((Message) getItem(i));
        super.b(i);
    }

    public final /* synthetic */ void b(int i, Object obj) {
        Message message = (Message) obj;
        c(message);
        super.b(i, message);
    }

    public final /* synthetic */ void b(Object obj) {
        Message message = (Message) obj;
        c(message);
        super.b((Object) message);
    }

    public final void b(Collection collection) {
        a((List) collection);
        super.b(collection);
    }

    /* renamed from: b */
    public final boolean c(Message message) {
        d(message);
        return super.c((Object) message);
    }

    public final /* synthetic */ Object c(int i) {
        d((Message) getItem(i));
        return (Message) super.c(i);
    }

    public final com.immomo.momo.android.activity.message.a e() {
        if (this.d != null) {
            return (com.immomo.momo.android.activity.message.a) this.d.get();
        }
        return null;
    }

    public int getItemViewType(int i) {
        Message message = (Message) getItem(i);
        int i2 = message.receive ? 0 : 8;
        switch (message.contentType) {
            case 0:
                return i2 + 0;
            case 1:
                return i2 + 3;
            case 2:
                return i2 + 4;
            case 3:
                return i2 + 6;
            case 4:
                return i2 + 1;
            case 5:
                return i2 + 5;
            case 6:
                return i2 + 2;
            case 7:
                return i2 + 7;
            default:
                return i2 + 0;
        }
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        ab abVar;
        Message message = (Message) getItem(i);
        if (this.g == null) {
            this.g = ((Message) this.f671a.get(0)).timestamp;
        }
        if (view == null) {
            abVar = ab.a(message.chatType, message.contentType, message.receive, e());
            view = abVar.g;
            view.setTag(R.id.tag_messageadapter, abVar);
        } else {
            abVar = (ab) view.getTag(R.id.tag_messageadapter);
        }
        abVar.a(message);
        if (message.status == 7) {
            this.e.put(message.msgId, abVar);
        }
        Float f2 = (Float) this.f.get(message.msgId);
        if (f2 == null) {
            abVar.h();
        } else if (f2.floatValue() >= 0.0f) {
            abVar.a(f2);
        } else if (f2.floatValue() == -2.0f) {
            abVar.a("隐身");
        } else {
            abVar.a("未知");
        }
        return view;
    }

    public int getViewTypeCount() {
        return 16;
    }
}
