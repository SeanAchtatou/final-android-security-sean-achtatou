package com.immomo.momo.android.activity.group;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.n;

final class ah extends d {

    /* renamed from: a  reason: collision with root package name */
    private v f1532a = null;
    /* access modifiers changed from: private */
    public /* synthetic */ EditGroupProfileActivity c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ah(EditGroupProfileActivity editGroupProfileActivity, Context context) {
        super(context);
        this.c = editGroupProfileActivity;
        this.f1532a = new v(context);
        this.f1532a.a("正在获取群资料");
        this.f1532a.setCancelable(true);
        this.f1532a.setOnCancelListener(new ai(this));
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        n.a().a(this.c.m, this.c.C);
        this.b.a((Object) ("downloadGroupProfile" + this.c.m + "result --->1"));
        return 1;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        if (this.f1532a != null) {
            this.f1532a.show();
        }
        this.b.a((Object) "downloadTempGroupProfile~~~~~~~~~~~~~~");
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        this.c.m = this.c.p.e(this.c.C);
        super.a(exc);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        if (((Integer) obj).intValue() >= 0) {
            this.c.w();
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
        if (this.f1532a != null) {
            this.f1532a.dismiss();
            this.f1532a = null;
        }
    }
}
