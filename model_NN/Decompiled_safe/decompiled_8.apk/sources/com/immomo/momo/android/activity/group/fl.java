package com.immomo.momo.android.activity.group;

import android.content.Context;
import android.widget.ListAdapter;
import com.immomo.momo.android.a.hr;
import com.immomo.momo.android.c.d;
import com.immomo.momo.protocol.a.n;
import com.immomo.momo.service.bean.a.a;
import java.util.List;

final class fl extends d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ SiteGroupsActivity f1660a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public fl(SiteGroupsActivity siteGroupsActivity, Context context) {
        super(context);
        this.f1660a = siteGroupsActivity;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        List b = n.a().b(this.f1660a.h, 0);
        this.f1660a.j.a(b);
        return b;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        if (this.f1660a.n != null && !this.f1660a.n.isCancelled()) {
            this.f1660a.n.cancel(true);
        }
        this.f1660a.m.g();
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        List<a> list = (List) obj;
        if (list != null) {
            this.f1660a.l = new hr(this.f1660a.getApplicationContext(), list, this.f1660a.k);
            this.f1660a.k.setAdapter((ListAdapter) this.f1660a.l);
            this.f1660a.i.clear();
            for (a aVar : list) {
                this.f1660a.i.put(aVar.b, aVar);
            }
            if (list.size() < 30) {
                this.f1660a.k.k();
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.f1660a.o = null;
        this.f1660a.k.n();
        this.f1660a.m.e();
    }
}
