package com.immomo.momo.android.a;

import android.content.Context;
import android.support.v4.b.a;
import com.immomo.momo.android.activity.tieba.TieDetailActivity;
import com.immomo.momo.android.c.d;
import com.immomo.momo.protocol.a.v;
import com.immomo.momo.service.bean.c.c;
import mm.purchasesdk.PurchaseCode;

final class ig extends d {

    /* renamed from: a  reason: collision with root package name */
    private int f876a;
    private c c;
    private String d;
    private /* synthetic */ hy e;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ig(hy hyVar, Context context, int i, c cVar, String str) {
        super(context);
        this.e = hyVar;
        this.f876a = i;
        this.c = cVar;
        this.d = str;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        switch (this.f876a) {
            case PurchaseCode.QUERY_OK /*101*/:
                return v.a().e(this.c.f3018a, this.d);
            case PurchaseCode.ORDER_OK /*102*/:
                return v.a().f(this.c.f3018a, this.d);
            case PurchaseCode.UNSUB_OK /*103*/:
                return v.a().a(this.c.i.c, (String) null, this.c.f3018a, this.c.c, this.d);
            default:
                return null;
        }
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.e.f.a(new com.immomo.momo.android.view.a.v(this.e.f, this));
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        String str = (String) obj;
        if (!a.a((CharSequence) str)) {
            a(str);
        }
        switch (this.f876a) {
            case PurchaseCode.QUERY_OK /*101*/:
            default:
                return;
            case PurchaseCode.ORDER_OK /*102*/:
                TieDetailActivity a2 = this.e.f;
                c cVar = this.c;
                a2.u();
                return;
            case PurchaseCode.UNSUB_OK /*103*/:
                TieDetailActivity a3 = this.e.f;
                c cVar2 = this.c;
                a3.v();
                return;
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.e.f.p();
    }
}
