package com.immomo.momo.android.a;

import android.view.View;

final class v implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ q f916a;
    private final /* synthetic */ aa b;
    private final /* synthetic */ int c;
    private final /* synthetic */ int d;

    v(q qVar, aa aaVar, int i, int i2) {
        this.f916a = qVar;
        this.b = aaVar;
        this.c = i;
        this.d = i2;
    }

    public final void onClick(View view) {
        this.f916a.b.getOnChildClickListener().onChildClick(this.f916a.b, this.b.d, this.c, this.d, 0);
    }
}
