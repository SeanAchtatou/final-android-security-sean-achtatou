package com.immomo.momo.android.a;

import android.view.View;

final class az implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ax f729a;
    private final /* synthetic */ int b;

    az(ax axVar, int i) {
        this.f729a = axVar;
        this.b = i;
    }

    public final void onClick(View view) {
        this.f729a.f.getOnItemClickListener().onItemClick(this.f729a.f, view, this.b, (long) view.getId());
    }
}
