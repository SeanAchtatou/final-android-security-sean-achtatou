package com.immomo.momo.android.c;

import com.immomo.momo.util.m;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public final class ag {

    /* renamed from: a  reason: collision with root package name */
    private static BlockingQueue f2373a = null;
    private static m b = new m("ImageGC");
    private static ai c = null;

    public static void a() {
        if (c != null) {
            c.a();
            c = null;
        }
        f2373a = new LinkedBlockingQueue();
        c = new ai(f2373a);
        new Thread(c).start();
    }

    public static void a(t tVar) {
        if (tVar != null) {
            if (f2373a == null) {
                a();
            }
            try {
                f2373a.put(tVar);
            } catch (InterruptedException e) {
                b.a((Throwable) e);
            }
        }
    }
}
