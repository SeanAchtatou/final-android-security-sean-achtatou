package com.immomo.momo.android.activity.account;

import android.content.DialogInterface;
import android.support.v4.b.a;
import com.immomo.momo.android.view.a.n;
import com.immomo.momo.util.ao;

final class bu implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ bn f974a;

    bu(bn bnVar) {
        this.f974a = bnVar;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        String trim = this.f974a.n.getText().toString().trim();
        if (a.a((CharSequence) trim)) {
            ao.b("验证码不可为空，请重试");
            this.f974a.n.requestFocus();
        } else if (this.f974a.j) {
            this.f974a.q.b(new bz(this.f974a, this.f974a.q, trim));
        }
        ((n) dialogInterface).c();
    }
}
