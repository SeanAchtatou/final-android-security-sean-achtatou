package com.immomo.momo.android.activity.tieba;

import android.content.Intent;
import android.net.Uri;
import android.webkit.DownloadListener;

final class el implements DownloadListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ TiebaManagerApplyActivity f2263a;

    el(TiebaManagerApplyActivity tiebaManagerApplyActivity) {
        this.f2263a = tiebaManagerApplyActivity;
    }

    public final void onDownloadStart(String str, String str2, String str3, String str4, long j) {
        this.f2263a.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
    }
}
