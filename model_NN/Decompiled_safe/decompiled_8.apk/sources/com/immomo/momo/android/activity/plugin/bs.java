package com.immomo.momo.android.activity.plugin;

import android.content.Context;
import com.immomo.momo.R;
import com.immomo.momo.android.c.d;
import com.immomo.momo.plugin.e.c;
import java.util.Collection;
import java.util.List;

final class bs extends d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ SinaWeiboActivity f2073a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public bs(SinaWeiboActivity sinaWeiboActivity, Context context) {
        super(context);
        this.f2073a = sinaWeiboActivity;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        List a2 = this.f2073a.h == 1 ? com.immomo.momo.protocol.a.d.a(this.f2073a.k, this.f2073a.j, "0") : com.immomo.momo.protocol.a.d.a(this.f2073a.k, this.f2073a.j, ((c) this.f2073a.i.getItem(this.f2073a.i.getCount() - 1)).f2873a);
        if (a2.size() > 0) {
            SinaWeiboActivity sinaWeiboActivity = this.f2073a;
            sinaWeiboActivity.h = sinaWeiboActivity.h + 1;
        }
        return a2;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        List list = (List) obj;
        this.f2073a.i.b((Collection) list);
        if ((list.size() > this.f2073a.h * 20 || list.size() < 20) && this.f2073a.o.getFooterViewsCount() > 0) {
            this.f2073a.o.removeFooterView(this.f2073a.C);
        }
        super.a(list);
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.f2073a.findViewById(R.id.process_layout_root).setVisibility(8);
        this.f2073a.D.e();
        if (this.f2073a.h > 5) {
            this.f2073a.o.removeFooterView(this.f2073a.C);
        } else {
            this.f2073a.C.setVisibility(0);
        }
        super.b();
    }
}
