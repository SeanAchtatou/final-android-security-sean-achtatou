package com.immomo.momo.android.view;

import android.content.DialogInterface;
import android.view.View;
import android.widget.AdapterView;
import com.immomo.momo.R;
import com.immomo.momo.android.view.a.n;
import com.immomo.momo.service.bean.bd;
import com.immomo.momo.service.bean.q;
import com.immomo.momo.service.bean.r;

final class ah implements AdapterView.OnItemClickListener {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public /* synthetic */ EmoteInputView f2717a;

    ah(EmoteInputView emoteInputView) {
        this.f2717a = emoteInputView;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        if (this.f2717a.i == this.f2717a.j) {
            if (this.f2717a.f != null) {
                this.f2717a.f.a((String) view.getTag(), this.f2717a.f.getSelectionStart());
            }
            if (this.f2717a.o != null) {
                this.f2717a.o.a((String) view.getTag(), 1);
            }
        } else if (this.f2717a.i == this.f2717a.k) {
            r rVar = (r) view.getTag();
            if (this.f2717a.o != null) {
                this.f2717a.o.a(rVar.toString(), 2);
            }
            bd.b().a(rVar);
            EmoteInputView.g(this.f2717a);
        } else {
            r rVar2 = (r) view.getTag();
            if (((Boolean) view.getTag(R.id.tag_item_boolean)).booleanValue()) {
                bd.b().a(rVar2);
                if (this.f2717a.o != null) {
                    this.f2717a.o.a(rVar2.toString(), 2);
                    return;
                }
                return;
            }
            try {
                n.a(this.f2717a.getContext(), "需要完成相应任务才可激活此表情", "查看表情", "取消", new ai(this, ((q) this.f2717a.i.getTag()).f3035a), (DialogInterface.OnClickListener) null).show();
            } catch (Exception e) {
                this.f2717a.t.a((Throwable) e);
            }
        }
    }
}
