package com.immomo.momo.android.activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.b.a;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;

public class ShowLocationDiloagActivity extends Activity {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public String f923a;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_showlocation);
        TextView textView = (TextView) findViewById(R.id.dilog_tv_content);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            this.f923a = extras.getString("message");
        }
        this.f923a = a.a(this.f923a) ? PoiTypeDef.All : this.f923a;
        textView.setText(this.f923a);
        findViewById(R.id.btn_ok).setOnClickListener(new kz(this));
        findViewById(R.id.btn_copy).setOnClickListener(new la(this));
    }
}
