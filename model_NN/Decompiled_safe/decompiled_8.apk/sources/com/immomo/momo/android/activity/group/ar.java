package com.immomo.momo.android.activity.group;

import com.immomo.momo.protocol.a.w;
import com.immomo.momo.service.bean.a.b;
import com.immomo.momo.service.bean.bf;
import java.util.ArrayList;
import java.util.List;

final class ar implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ GroupActionListFragment f1542a;
    private List b;

    public ar(GroupActionListFragment groupActionListFragment, List list) {
        this.f1542a = groupActionListFragment;
        this.b = null;
        this.b = new ArrayList();
        this.b.addAll(list);
        list.clear();
    }

    public final void run() {
        try {
            for (bf bfVar : w.a().a((String[]) this.b.toArray(new String[this.b.size()]))) {
                if (bfVar != null) {
                    ((b) this.f1542a.ad.remove(bfVar.h)).a(bfVar);
                    this.f1542a.X.d(bfVar);
                }
            }
            if (!this.f1542a.f()) {
                this.f1542a.P.post(new as(this));
            }
        } catch (Exception e) {
            this.f1542a.O.a((Throwable) e);
        }
    }
}
