package com.immomo.momo.android.game;

import android.support.v4.b.a;
import android.view.View;
import android.view.ViewGroup;
import com.amap.mapapi.poisearch.PoiTypeDef;

final class l implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ k f2436a;

    l(k kVar) {
        this.f2436a = kVar;
    }

    public final void onClick(View view) {
        for (int i = 0; i < this.f2436a.P.getChildCount(); i++) {
            ViewGroup viewGroup = (ViewGroup) this.f2436a.P.getChildAt(i);
            View childAt = viewGroup.getChildAt(0);
            View childAt2 = viewGroup.getChildAt(1);
            if (childAt != view) {
                childAt.setSelected(false);
            }
            if (childAt2 != view) {
                childAt2.setSelected(false);
            }
        }
        view.setSelected(true);
        this.f2436a.W.b = (aj) view.getTag();
        if (!a.a((CharSequence) this.f2436a.W.b.e)) {
            this.f2436a.Q.setText(this.f2436a.W.b.e);
            if (this.f2436a.Q.isFocused()) {
                this.f2436a.Q.setSelection(this.f2436a.Q.getText().length());
            }
        } else {
            this.f2436a.Q.setText(PoiTypeDef.All);
        }
        if (!a.a((CharSequence) this.f2436a.W.b.f)) {
            this.f2436a.R.setText(this.f2436a.W.b.f);
            if (this.f2436a.R.isFocused()) {
                this.f2436a.R.setSelection(this.f2436a.Q.getText().length());
            }
        } else {
            this.f2436a.R.setText(PoiTypeDef.All);
        }
        if (this.f2436a.W.b.d > 0) {
            this.f2436a.S.setText("已选择面额:" + this.f2436a.W.b.d + "元");
        } else {
            this.f2436a.S.setText("点击选择面额");
        }
    }
}
