package com.immomo.momo.android.a;

import android.support.v4.b.a;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.aj;
import com.immomo.momo.service.bean.al;
import com.immomo.momo.service.bean.y;
import com.immomo.momo.service.bean.z;
import com.immomo.momo.util.j;
import com.immomo.momo.util.m;
import java.util.HashMap;
import java.util.List;

public final class bs extends b {

    /* renamed from: a  reason: collision with root package name */
    private List f743a = null;
    private ExpandableListView b = null;

    public bs(List list, ExpandableListView expandableListView) {
        new m(this);
        new HashMap();
        this.f743a = list;
        this.b = expandableListView;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public y getChild(int i, int i2) {
        return (y) ((z) this.f743a.get(i)).f3043a.get(i2);
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public z getGroup(int i) {
        return (z) this.f743a.get(i);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, int, boolean, boolean):void
     arg types: [com.immomo.momo.service.bean.al, android.widget.ImageView, int, int, int]
     candidates:
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup, int, boolean):void
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, int, boolean, boolean):void */
    private static void a(y yVar, bt btVar) {
        if (yVar.o().isEmpty()) {
            btVar.j.setVisibility(8);
            return;
        }
        btVar.j.setVisibility(0);
        for (int size = yVar.o().size(); size < 6; size++) {
            btVar.m[size].setVisibility(8);
        }
        int i = 0;
        while (i < yVar.o().size() && i < 6) {
            btVar.m[i].setVisibility(0);
            btVar.n[i].setImageId((String) yVar.o().get(i));
            btVar.n[i].setImageLoadFailed(false);
            j.a((aj) btVar.n[i], btVar.l[i], 3, true, false);
            i++;
        }
    }

    public final void a() {
        for (int i = 0; i < getGroupCount(); i++) {
            this.b.expandGroup(i);
        }
    }

    public final void a(View view, int i) {
        if (i >= 0) {
            z a2 = getGroup(i);
            ((TextView) view.findViewById(R.id.sitelist_tv_name)).setTextColor(g.c((int) R.color.blue));
            ((TextView) view.findViewById(R.id.sitelist_tv_name)).setText(a2.a());
        }
    }

    public final int b(int i, int i2) {
        if (i2 < 0 && i < 0) {
            return 0;
        }
        if (i2 != -1 || this.b.isGroupExpanded(i)) {
            return i2 == getChildrenCount(i) + -1 ? 2 : 1;
        }
        return 0;
    }

    public final long getChildId(int i, int i2) {
        return (long) i2;
    }

    public final View getChildView(int i, int i2, boolean z, View view, ViewGroup viewGroup) {
        int i3 = 8;
        if (view == null) {
            bt btVar = new bt((byte) 0);
            view = g.o().inflate((int) R.layout.listitem_eventdynamic, (ViewGroup) null);
            btVar.l = new ImageView[6];
            btVar.m = new View[6];
            btVar.n = new al[6];
            for (int i4 = 0; i4 < btVar.n.length; i4++) {
                btVar.n[i4] = new al();
            }
            btVar.l[0] = (ImageView) view.findViewById(R.id.iv_avatar_0);
            btVar.l[1] = (ImageView) view.findViewById(R.id.iv_avatar_1);
            btVar.l[2] = (ImageView) view.findViewById(R.id.iv_avatar_2);
            btVar.l[3] = (ImageView) view.findViewById(R.id.iv_avatar_3);
            btVar.l[4] = (ImageView) view.findViewById(R.id.iv_avatar_4);
            btVar.l[5] = (ImageView) view.findViewById(R.id.iv_avatar_5);
            btVar.m[0] = view.findViewById(R.id.layout_avatar_0);
            btVar.m[1] = view.findViewById(R.id.layout_avatar_1);
            btVar.m[2] = view.findViewById(R.id.layout_avatar_2);
            btVar.m[3] = view.findViewById(R.id.layout_avatar_3);
            btVar.m[4] = view.findViewById(R.id.layout_avatar_4);
            btVar.m[5] = view.findViewById(R.id.layout_avatar_5);
            btVar.c = (TextView) view.findViewById(R.id.tv_timedes);
            btVar.d = (TextView) view.findViewById(R.id.tv_avatardes);
            btVar.k = (ImageView) view.findViewById(R.id.iv_readedtip);
            btVar.h = (TextView) view.findViewById(R.id.tv_commentcount);
            btVar.e = (TextView) view.findViewById(R.id.tv_joincount);
            btVar.b = (TextView) view.findViewById(R.id.tv_ismine);
            btVar.f744a = (TextView) view.findViewById(R.id.tv_name);
            btVar.i = view.findViewById(R.id.layout_count);
            btVar.j = view.findViewById(R.id.layout_joinmember);
            btVar.g = (TextView) view.findViewById(R.id.tv_newcommentcount);
            btVar.f = (TextView) view.findViewById(R.id.tv_newjoincount);
            view.setTag(R.id.tag_userlist_item, btVar);
        }
        y a2 = getChild(i, i2);
        bt btVar2 = (bt) view.getTag(R.id.tag_userlist_item);
        if (a.a((CharSequence) a2.i())) {
            a2.e(a2.h());
        }
        btVar2.f744a.setText(a2.i());
        btVar2.c.setText(a2.n());
        btVar2.d.setText(a.a(a2.g()) ? PoiTypeDef.All : a2.g());
        btVar2.h.setText(String.valueOf(a2.k()) + "留言");
        btVar2.e.setText(String.valueOf(a2.j()) + "人参加");
        btVar2.i.setVisibility(a2.m() ? 0 : 8);
        btVar2.g.setText(a2.c() > 0 ? " (+" + a2.c() + ")" : PoiTypeDef.All);
        btVar2.f.setText(a2.b() > 0 ? " (+" + a2.b() + ")" : PoiTypeDef.All);
        btVar2.b.setVisibility(a2.m() ? 0 : 8);
        ImageView imageView = btVar2.k;
        if (!a2.a()) {
            i3 = 0;
        }
        imageView.setVisibility(i3);
        a(a2, btVar2);
        return view;
    }

    public final int getChildrenCount(int i) {
        if (i < 0 || i >= this.f743a.size()) {
            return 0;
        }
        if (((z) this.f743a.get(i)).f3043a == null) {
            return 0;
        }
        return ((z) this.f743a.get(i)).f3043a.size();
    }

    public final int getGroupCount() {
        return this.f743a.size();
    }

    public final long getGroupId(int i) {
        return (long) i;
    }

    public final View getGroupView(int i, boolean z, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = g.o().inflate((int) R.layout.listitem_eventdynamic_group, (ViewGroup) null);
            bu buVar = new bu();
            buVar.f745a = (TextView) view.findViewById(R.id.sitelist_tv_name);
            view.setTag(R.id.tag_userlist_item, buVar);
        }
        ((bu) view.getTag(R.id.tag_userlist_item)).f745a.setText(getGroup(i).a());
        return view;
    }

    public final boolean hasStableIds() {
        return true;
    }

    public final boolean isChildSelectable(int i, int i2) {
        return true;
    }
}
