package com.immomo.momo.android.activity.plugin;

import a.a.a.a;
import a.a.a.b;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.webkit.WebView;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.a.e;
import com.immomo.momo.a.w;
import com.immomo.momo.android.activity.ah;
import com.immomo.momo.android.view.HeaderLayout;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.g;
import com.immomo.momo.protocol.a.d;
import com.immomo.momo.service.bean.c;
import com.immomo.momo.util.ao;
import com.immomo.momo.util.m;
import org.apache.http.conn.ConnectTimeoutException;

public class BindTwitter2Activity extends ah {
    /* access modifiers changed from: private */
    public m h = new m("test_momo");
    private HeaderLayout i = null;
    /* access modifiers changed from: private */
    public a j;
    /* access modifiers changed from: private */
    public b k;
    /* access modifiers changed from: private */
    public String l = "http://test.wdy/success";
    /* access modifiers changed from: private */
    public WebView m;
    private TextView n;
    /* access modifiers changed from: private */
    public v o = null;
    private String p;
    /* access modifiers changed from: private */
    public c q;
    private ak r;
    /* access modifiers changed from: private */
    public Handler s = new aj(this);

    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        super.a(bundle);
        setContentView((int) R.layout.activity_bind_api);
        this.m = (WebView) findViewById(R.id.web);
        this.m.getSettings().setJavaScriptEnabled(true);
        this.m.setWebViewClient(new am(this, (byte) 0));
        this.i = (HeaderLayout) findViewById(R.id.layout_header);
        this.i.setTitleText("绑定twitter");
        this.n = (TextView) findViewById(R.id.header_stv_title);
        this.n.setFocusableInTouchMode(false);
        this.r = new ak(this, this);
        a(this.r).execute(new Object[0]);
    }

    /* access modifiers changed from: protected */
    public final void c(String str) {
        boolean z;
        Uri parse = Uri.parse(str);
        this.h.a((Object) ("onNewIntent intent.getData():" + parse));
        String queryParameter = parse.getQueryParameter("oauth_verifier");
        try {
            this.k.b();
            this.k.b(this.j, queryParameter);
        } catch (Exception e) {
            this.h.a((Throwable) e);
        }
        String str2 = (String) this.k.a().get((Object) "user_id").first();
        String a2 = this.j.a();
        this.p = this.j.b();
        try {
            this.h.b((Object) ("get result from BindSinaActivity : userId=" + str2 + " oauthToken=" + a2 + " oauthTokenSecret=" + this.p + " verifier=" + queryParameter));
            d.a(str2, a2, this.p, queryParameter);
            z = true;
        } catch (e e2) {
            this.h.a((Throwable) e2);
            ao.g(R.string.errormsg_network_normal400);
            z = false;
        } catch (w e3) {
            this.h.a((Throwable) e3);
            ao.g(R.string.errormsg_network_unfind);
            z = false;
        } catch (ConnectTimeoutException e4) {
            this.h.a((Throwable) e4);
            ao.g(R.string.errormsg_network_timeout);
            z = false;
        } catch (Exception e5) {
            this.h.a((Throwable) e5);
            z = false;
        }
        if (!z) {
            ao.a((CharSequence) "绑定失败，请稍后再试");
            return;
        }
        ao.a((CharSequence) "新浪微博绑定成功");
        g.q().an = true;
        setResult(-1, getIntent());
        finish();
    }

    /* access modifiers changed from: protected */
    public final void d() {
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (this.r != null) {
            this.o = null;
            this.r.cancel(true);
        }
        super.onDestroy();
    }
}
