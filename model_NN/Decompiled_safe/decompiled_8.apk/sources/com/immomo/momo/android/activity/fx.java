package com.immomo.momo.android.activity;

import android.view.View;
import android.widget.AdapterView;
import com.immomo.momo.service.bean.b.b;
import com.immomo.momo.util.ao;

final class fx implements AdapterView.OnItemClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ MulImagePickerActivity f1505a;

    fx(MulImagePickerActivity mulImagePickerActivity) {
        this.f1505a = mulImagePickerActivity;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        b b = this.f1505a.k.b(i);
        if (b.d) {
            this.f1505a.k.b(b);
            b.d = false;
            this.f1505a.q.a(i);
            this.f1505a.k.a(b);
        } else if (this.f1505a.k.d()) {
            b.d = true;
            this.f1505a.k.c(b);
            this.f1505a.q.a(i);
            this.f1505a.k.a(b);
        } else {
            ao.b("最多可以发布" + this.f1505a.k.b() + "张图");
        }
        this.f1505a.g();
    }
}
