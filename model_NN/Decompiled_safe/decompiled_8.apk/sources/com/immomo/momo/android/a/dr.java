package com.immomo.momo.android.a;

import android.view.View;

final class dr implements View.OnLongClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ dn f781a;
    private final /* synthetic */ int b;

    dr(dn dnVar, int i) {
        this.f781a = dnVar;
        this.b = i;
    }

    public final boolean onLongClick(View view) {
        return this.f781a.e.getOnItemLongClickListenerInWrapper().onItemLongClick(this.f781a.e, view, this.b, (long) view.getId());
    }
}
