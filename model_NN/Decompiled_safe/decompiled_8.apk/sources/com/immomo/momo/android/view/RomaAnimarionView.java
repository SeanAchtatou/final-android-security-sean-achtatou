package com.immomo.momo.android.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import com.immomo.momo.R;
import com.immomo.momo.util.m;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class RomaAnimarionView extends SurfaceView implements SurfaceHolder.Callback {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static final int[] f2652a = {R.drawable.bg_roma_flowcity_1, R.drawable.bg_roma_flowcity_2, R.drawable.bg_roma_flowcity_3, R.drawable.bg_roma_flowcity_4};
    /* access modifiers changed from: private */
    public cz b;
    private SurfaceHolder c = null;
    /* access modifiers changed from: private */
    public boolean d;
    /* access modifiers changed from: private */
    public boolean e;
    /* access modifiers changed from: private */
    public Bitmap f;
    /* access modifiers changed from: private */
    public Bitmap g;
    private Paint h;
    private int i;
    /* access modifiers changed from: private */
    public boolean j;
    /* access modifiers changed from: private */
    public Lock k = null;
    /* access modifiers changed from: private */
    public Condition l = null;
    /* access modifiers changed from: private */
    public m m = new m(this);

    public RomaAnimarionView(Context context) {
        super(context);
        g();
    }

    public RomaAnimarionView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        g();
    }

    public RomaAnimarionView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        g();
    }

    static /* synthetic */ void f() {
    }

    private void g() {
        this.h = new Paint();
        getHolder().setType(0);
        getHolder().addCallback(this);
        this.k = new ReentrantLock();
        this.l = this.k.newCondition();
    }

    private boolean h() {
        Canvas lockCanvas;
        if (this.c == null || (lockCanvas = this.c.lockCanvas()) == null) {
            return false;
        }
        try {
            int i2 = this.i / 4;
            if (i2 >= this.f.getWidth()) {
                i2 %= this.f.getWidth();
            }
            int measuredWidth = getMeasuredWidth() + i2;
            if (measuredWidth <= this.f.getWidth()) {
                lockCanvas.drawBitmap(this.f, new Rect(i2, 0, measuredWidth, this.f.getHeight()), new Rect(0, 0, getMeasuredWidth(), getMeasuredHeight()), this.h);
            } else {
                int width = measuredWidth - this.f.getWidth();
                lockCanvas.drawBitmap(this.f, new Rect(i2, 0, this.f.getWidth(), this.f.getHeight()), new Rect(0, 0, getMeasuredWidth() - width, getMeasuredHeight()), this.h);
                lockCanvas.drawBitmap(this.f, new Rect(0, 0, width, this.f.getHeight()), new Rect(getMeasuredWidth() - width, 0, getMeasuredWidth(), getMeasuredHeight()), this.h);
            }
            int i3 = this.i;
            if (i3 >= this.g.getWidth()) {
                i3 %= this.g.getWidth();
            }
            int measuredWidth2 = getMeasuredWidth() + i3;
            int round = Math.round(((float) this.g.getHeight()) * (((float) getMeasuredHeight()) / ((float) this.f.getHeight())));
            if (measuredWidth2 <= this.g.getWidth()) {
                lockCanvas.drawBitmap(this.g, new Rect(i3, 0, measuredWidth2, this.g.getHeight()), new Rect(0, 0, getMeasuredWidth(), round), this.h);
            } else {
                int width2 = measuredWidth2 - this.g.getWidth();
                lockCanvas.drawBitmap(this.g, new Rect(i3, 0, this.g.getWidth(), this.g.getHeight()), new Rect(0, 0, getMeasuredWidth() - width2, round), this.h);
                lockCanvas.drawBitmap(this.g, new Rect(0, 0, width2, this.g.getHeight()), new Rect(getMeasuredWidth() - width2, 0, getMeasuredWidth(), round), this.h);
            }
            this.c.unlockCanvasAndPost(lockCanvas);
            return true;
        } catch (Exception e2) {
            this.m.a((Throwable) e2);
            this.c.unlockCanvasAndPost(lockCanvas);
            return false;
        } catch (Throwable th) {
            this.c.unlockCanvasAndPost(lockCanvas);
            throw th;
        }
    }

    public final void a() {
        if (!this.j) {
            this.k.lock();
            this.j = true;
            this.l.signal();
            this.k.unlock();
        }
    }

    public final void b() {
        this.j = false;
    }

    public final void c() {
        this.e = true;
        this.d = false;
        this.k.lock();
        this.l.signal();
        this.k.unlock();
        if (this.f != null) {
            this.f.recycle();
        }
        if (this.g != null) {
            this.g.recycle();
        }
        this.f = null;
        this.g = null;
        getHolder().removeCallback(this);
    }

    /* access modifiers changed from: package-private */
    public final void d() {
        if (h() && this.j) {
            this.i += 8;
        }
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        super.onMeasure(i2, i3);
    }

    public void setStatusListener(cz czVar) {
        this.b = czVar;
    }

    public void surfaceChanged(SurfaceHolder surfaceHolder, int i2, int i3, int i4) {
        this.c = surfaceHolder;
        this.m.a((Object) ("surfaceChanged, width=" + i3 + ", height=" + i4 + ", format=" + i2));
    }

    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        this.c = surfaceHolder;
        this.d = true;
        this.e = false;
        new Thread(new cy(this)).start();
    }

    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        this.c = null;
        this.e = true;
    }
}
