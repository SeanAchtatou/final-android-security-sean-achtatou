package com.immomo.momo.android.a;

import android.content.Intent;
import android.view.View;
import com.immomo.momo.android.activity.group.GroupProfileActivity;

/* renamed from: com.immomo.momo.android.a.do  reason: invalid class name */
final class Cdo implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ dn f778a;

    Cdo(dn dnVar) {
        this.f778a = dnVar;
    }

    public final void onClick(View view) {
        Intent intent = new Intent();
        intent.setClass(this.f778a.d, GroupProfileActivity.class);
        intent.putExtra("tag", "local");
        intent.putExtra("gid", (String) view.getTag());
        this.f778a.d.startActivity(intent);
    }
}
