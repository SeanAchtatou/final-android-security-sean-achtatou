package com.immomo.momo.android.activity.event;

import android.content.Intent;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.b.a;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.amap.mapapi.location.LocationManagerProxy;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.ImageBrowserActivity;
import com.immomo.momo.android.activity.WebviewActivity;
import com.immomo.momo.android.activity.ah;
import com.immomo.momo.android.activity.common.CommonShareActivity;
import com.immomo.momo.android.b.z;
import com.immomo.momo.android.map.AMapActivity;
import com.immomo.momo.android.map.GoogleMapActivity;
import com.immomo.momo.android.view.a.ao;
import com.immomo.momo.android.view.a.at;
import com.immomo.momo.android.view.a.n;
import com.immomo.momo.android.view.a.o;
import com.immomo.momo.android.view.bi;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.ab;
import com.immomo.momo.service.bean.al;
import com.immomo.momo.service.bean.au;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.service.bean.u;
import com.immomo.momo.service.bean.x;
import com.immomo.momo.service.r;
import com.immomo.momo.util.j;
import com.immomo.momo.util.k;
import java.util.ArrayList;
import java.util.List;

public class EventProfileActivity extends ah implements View.OnClickListener {
    private TextView A;
    private TextView B;
    private TextView C;
    private TextView D;
    private TextView E;
    private LinearLayout F;
    private LinearLayout G;
    private RelativeLayout H;
    private TextView I;
    private LinearLayout J;
    private List K;
    private at L = null;
    private bi M = null;
    private ImageView N;
    private ImageView O;
    private ImageView P;
    private ap h;
    private ArrayList i = new ArrayList();
    private View[] j;
    /* access modifiers changed from: private */
    public boolean k = false;
    private String l;
    /* access modifiers changed from: private */
    public String m;
    /* access modifiers changed from: private */
    public u n;
    /* access modifiers changed from: private */
    public r o;
    private View p;
    private View q;
    private View r;
    private View s;
    private TextView t;
    private TextView u;
    private TextView v;
    private TextView w;
    private TextView x;
    private TextView y;
    private TextView z;

    static /* synthetic */ void g(EventProfileActivity eventProfileActivity) {
        if (eventProfileActivity.n != null) {
            eventProfileActivity.w();
            eventProfileActivity.u();
            eventProfileActivity.y();
        }
    }

    static /* synthetic */ boolean j(EventProfileActivity eventProfileActivity) {
        return eventProfileActivity.f.at || eventProfileActivity.f.ar || eventProfileActivity.f.an;
    }

    static /* synthetic */ void k(EventProfileActivity eventProfileActivity) {
        ao aoVar = new ao(eventProfileActivity, eventProfileActivity.f);
        aoVar.setTitle((int) R.string.event_profile_sharedialog_title);
        aoVar.b((int) R.string.event_profile_sharedialog_content);
        aoVar.a(new an(eventProfileActivity, aoVar));
        eventProfileActivity.a(aoVar);
    }

    static /* synthetic */ void l(EventProfileActivity eventProfileActivity) {
        n nVar = new n(eventProfileActivity);
        nVar.setTitle("提示");
        nVar.a();
        nVar.a(g.a((int) R.string.event_profile_quitdialog_content));
        nVar.a(0, (int) R.string.dialog_btn_confim, new ak(eventProfileActivity));
        nVar.a(1, (int) R.string.dialog_btn_cancel, new al());
        eventProfileActivity.a(nVar);
    }

    static /* synthetic */ void m(EventProfileActivity eventProfileActivity) {
        if (eventProfileActivity.n != null) {
            String[] strArr = {"分享给好友", "分享到社交网络"};
            o oVar = new o(eventProfileActivity, strArr);
            oVar.a(new ao(eventProfileActivity, strArr));
            oVar.b();
            oVar.show();
        }
    }

    static /* synthetic */ void n(EventProfileActivity eventProfileActivity) {
        if (eventProfileActivity.L == null || !eventProfileActivity.L.g()) {
            String[] strArr = eventProfileActivity.n.t ? new String[]{"退出活动", "分享"} : new String[]{"分享"};
            eventProfileActivity.L = new at(eventProfileActivity, eventProfileActivity.M, strArr);
            eventProfileActivity.L.a(at.b);
            eventProfileActivity.L.a(new ai(eventProfileActivity, strArr));
            eventProfileActivity.L.d();
        }
    }

    static /* synthetic */ void o(EventProfileActivity eventProfileActivity) {
        Intent intent = new Intent(eventProfileActivity, CommonShareActivity.class);
        intent.putExtra("from_type", 2);
        intent.putExtra("from_id", eventProfileActivity.m);
        eventProfileActivity.startActivity(intent);
    }

    private void u() {
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 < this.i.size()) {
                j.a(new al(((au) this.i.get(i3)).b), (ImageView) this.j[i3].findViewById(R.id.avatar_imageview), (ViewGroup) null, 3);
                i2 = i3 + 1;
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: private */
    public void v() {
        if (!"notreflsh".equals(this.l)) {
            this.h = new ap(this, this);
            this.h.execute(new String[]{this.m});
        }
    }

    private void w() {
        int i2 = 3;
        if (this.n != null) {
            m().setTitleText("活动详情");
            this.r.setEnabled(!a.a(this.n.s));
            this.s.setVisibility(!a.a(this.n.s) ? 0 : 8);
            this.t.setText(a.a(this.n.b) ? PoiTypeDef.All : this.n.b);
            this.v.setText(a.a(this.n.e) ? PoiTypeDef.All : this.n.e);
            this.y.setText(a.a(this.n.k) ? PoiTypeDef.All : this.n.k);
            this.w.setText(a.a(this.n.h) ? PoiTypeDef.All : this.n.h);
            this.u.setText(a.a(this.n.d) ? PoiTypeDef.All : this.n.d);
            this.B.setText(a.a(this.n.p) ? PoiTypeDef.All : this.n.p);
            this.x.setText(x.a(this.n.i).b());
            String a2 = g.a((int) R.string.event_profile_membercount);
            String a3 = g.a((int) R.string.event_profile_friendscount);
            if (this.n.e() > 0) {
                this.z.setText(String.valueOf(String.format(a2, Integer.valueOf(this.n.f()))) + "/" + String.format(a3, Integer.valueOf(this.n.e())));
            } else {
                this.z.setText(String.format(a2, Integer.valueOf(this.n.f())));
            }
            this.A.setText(String.format(g.a((int) R.string.event_profile_commentcount), Integer.valueOf(this.n.d())));
            this.I.setText("活动讨论(" + this.n.d() + ")");
            List b = this.n.b();
            this.K = new ArrayList();
            this.J.removeAllViews();
            if (b != null && b.size() > 0) {
                if (b.size() <= 3) {
                    i2 = b.size();
                }
                for (int i3 = 0; i3 < i2; i3++) {
                    this.K.add((ab) b.get(i3));
                    ab abVar = (ab) b.get(i3);
                    View inflate = g.o().inflate((int) R.layout.eventprofile_listcomments, (ViewGroup) null);
                    ax axVar = new ax((byte) 0);
                    axVar.c = (TextView) inflate.findViewById(R.id.tv_comment_name);
                    axVar.b = (TextView) inflate.findViewById(R.id.tv_comment_content);
                    axVar.f1367a = (TextView) inflate.findViewById(R.id.tv_comment_time);
                    axVar.d = (ImageView) inflate.findViewById(R.id.iv_comment_photo);
                    if (abVar.b != null) {
                        axVar.c.setText(abVar.b.h());
                    } else {
                        axVar.c.setText(abVar.c);
                    }
                    if (a.f(abVar.b())) {
                        axVar.b.setText(abVar.b());
                    } else if (a.f(abVar.getLoadImageId())) {
                        axVar.b.setText("[图片]");
                    }
                    j.a(abVar.b, axVar.d, (ViewGroup) null);
                    axVar.f1367a.setText(abVar.f);
                    axVar.d.setOnClickListener(new am(this, abVar));
                    this.J.addView(inflate);
                }
            }
            j.a(this.n, this.N, (ViewGroup) null, 20);
            j.a(new al(this.n.q), this.O, (ViewGroup) null);
            if (this.n.m) {
                this.C.setVisibility(0);
            } else {
                this.C.setVisibility(8);
            }
            if (this.n.n) {
                this.D.setVisibility(0);
            } else {
                this.D.setVisibility(8);
            }
            if (this.n.o) {
                this.E.setVisibility(0);
            } else {
                this.E.setVisibility(8);
            }
        }
    }

    /* access modifiers changed from: private */
    public void x() {
        if (!(this.n == null || this.n.l == null || this.n.l.size() <= 0)) {
            int size = this.n.l.size() > 8 ? 8 : this.n.l.size();
            this.i.clear();
            for (int i2 = 0; i2 < size; i2++) {
                au auVar = new au();
                bf bfVar = (bf) this.n.l.get(i2);
                if (!(bfVar == null || bfVar.ae == null)) {
                    auVar.b = bfVar.ae[0];
                }
                this.i.add(auVar);
            }
        }
        if (this.i != null && this.i.size() > 0) {
            int size2 = this.i.size();
            this.e.a((Object) ("length: " + size2));
            for (int i3 = 0; i3 < size2; i3++) {
                this.j[i3].setVisibility(0);
                ((ImageView) this.j[i3].findViewById(R.id.avatar_cover)).setTag(this.i.get(i3));
            }
            if (size2 < 8) {
                for (int i4 = size2; i4 < 8; i4++) {
                    this.j[i4].setVisibility(4);
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void y() {
        if (this.n.t) {
            this.F.setVisibility(8);
            this.H.setPadding(0, 0, 0, 0);
            return;
        }
        this.F.setVisibility(0);
        this.H.setPadding(5, 0, 0, 0);
        if (this.n.j == 1) {
            this.F.setEnabled(true);
            return;
        }
        this.F.setEnabled(false);
        ((TextView) this.F.getChildAt(0)).setText("活动已过期");
    }

    public final void a(Bundle bundle) {
        super.a(bundle);
        setContentView((int) R.layout.activity_eventprofile);
        this.o = new r();
        this.t = (TextView) findViewById(R.id.eventprofile_tv_name);
        this.v = (TextView) findViewById(R.id.eventprofile_tv_adress);
        this.y = (TextView) findViewById(R.id.eventprofile_tv_desc);
        this.w = (TextView) findViewById(R.id.eventprofile_tv_distance);
        this.z = (TextView) findViewById(R.id.eventprofile_tv_totalcount);
        this.u = (TextView) findViewById(R.id.eventprofile_tv_holdtime);
        this.B = (TextView) findViewById(R.id.eventprofile_tv_hostname);
        this.A = (TextView) findViewById(R.id.eventprofile_tv_msgcount);
        this.x = (TextView) findViewById(R.id.eventprofile_tv_subject);
        this.x = (TextView) findViewById(R.id.eventprofile_tv_subject);
        findViewById(R.id.tv_comment_name);
        findViewById(R.id.tv_comment_time);
        findViewById(R.id.iv_comment_photo);
        this.N = (ImageView) findViewById(R.id.eventprofile_iv_avatar);
        this.P = (ImageView) findViewById(R.id.eventprofile_iv_avatarcover);
        this.O = (ImageView) findViewById(R.id.eventprofile_iv_hostlogo);
        findViewById(R.id.eventprofile_layout_tag);
        this.p = findViewById(R.id.eventprofile_layout_members);
        this.F = (LinearLayout) findViewById(R.id.profile_layout_bottom_join);
        this.G = (LinearLayout) findViewById(R.id.profile_layout_bottom_feeds);
        this.H = (RelativeLayout) findViewById(R.id.profile_layout_bottom_feeds_wrap);
        this.I = (TextView) findViewById(R.id.profile_bottom_text_feeds);
        this.q = findViewById(R.id.eventprofile_layout_desc);
        this.p = findViewById(R.id.eventprofile_layout_members);
        this.r = findViewById(R.id.eventprofile_layout_host);
        this.s = findViewById(R.id.eventprofile_iv_hostarrow);
        this.C = (TextView) findViewById(R.id.eventprofile_tv_hot);
        this.D = (TextView) findViewById(R.id.eventprofile_tv_today);
        this.E = (TextView) findViewById(R.id.eventprofile_tv_free);
        this.J = (LinearLayout) findViewById(R.id.eventprofile_layout_commentcontainer);
        this.j = new View[8];
        this.j[0] = findViewById(R.id.member_avatar_block0);
        this.j[1] = findViewById(R.id.member_avatar_block1);
        this.j[2] = findViewById(R.id.member_avatar_block2);
        this.j[3] = findViewById(R.id.member_avatar_block3);
        this.j[4] = findViewById(R.id.member_avatar_block4);
        this.j[5] = findViewById(R.id.member_avatar_block5);
        this.j[6] = findViewById(R.id.member_avatar_block6);
        this.j[7] = findViewById(R.id.member_avatar_block7);
        int round = Math.round(((((((float) g.J()) - (g.l().getDimension(R.dimen.pic_item_margin) * 16.0f)) - (g.l().getDimension(R.dimen.style_patterns) * 2.0f)) - (((float) BitmapFactory.decodeResource(getResources(), R.drawable.ic_common_arrow_right).getWidth()) + g.l().getDimension(R.dimen.member_arrow_margin_left))) - (g.l().getDimension(R.dimen.pic_layout_margin) * 2.0f)) / 8.0f);
        for (int i2 = 0; i2 < this.j.length; i2++) {
            ViewGroup.LayoutParams layoutParams = this.j[i2].getLayoutParams();
            layoutParams.height = round;
            layoutParams.width = round;
            this.j[i2].setLayoutParams(layoutParams);
        }
        this.M = new bi(this).a((int) R.drawable.ic_topbar_arrow_down);
        m().a(this.M, new aj(this));
        if (bundle == null || !bundle.getBoolean("from_saveinstance", false)) {
            Intent intent = getIntent();
            this.l = intent.getStringExtra("tag");
            this.m = intent.getStringExtra("eventid");
        } else {
            this.m = (String) bundle.get("eventid");
            this.l = (String) bundle.get("tag");
            this.l = this.l == null ? "local" : this.l;
        }
        if (!a.a((CharSequence) this.m)) {
            this.n = this.o.c(this.m);
            if (this.n != null) {
                this.k = false;
                x();
                y();
                return;
            }
            this.k = true;
            this.n = new u(this.m);
        }
    }

    /* access modifiers changed from: protected */
    public final void e() {
        super.e();
        u();
        this.F.setOnClickListener(this);
        this.G.setOnClickListener(this);
        this.p.setOnClickListener(this);
        this.q.setOnClickListener(this);
        this.r.setOnClickListener(this);
        this.P.setOnClickListener(this);
        this.v.setOnClickListener(this);
        findViewById(R.id.eventprofile_layout_commentcontainer).setOnClickListener(this);
        v();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.eventprofile_layout_desc /*2131165608*/:
                if (!a.a((CharSequence) this.n.u)) {
                    new k("C", "C71102").e();
                    Intent intent = new Intent(this, WebviewActivity.class);
                    intent.putExtra("webview_title", "活动介绍");
                    intent.putExtra("webview_url", this.n.u);
                    startActivity(intent);
                    return;
                }
                return;
            case R.id.eventprofile_layout_members /*2131165612*/:
                Intent intent2 = new Intent(this, EventMemberListActivity.class);
                intent2.putExtra("eventid", this.m);
                intent2.putExtra("count", this.n.f());
                startActivity(intent2);
                return;
            case R.id.eventprofile_layout_host /*2131165613*/:
                if (!a.a((CharSequence) this.n.s)) {
                    Intent intent3 = new Intent(this, WebviewActivity.class);
                    intent3.putExtra("webview_title", "主办方");
                    intent3.putExtra("webview_url", this.n.s);
                    startActivity(intent3);
                    return;
                }
                return;
            case R.id.eventprofile_layout_commentcontainer /*2131165618*/:
            case R.id.profile_layout_bottom_feeds /*2131166211*/:
                Intent intent4 = new Intent(this, EventFeedsActivity.class);
                intent4.putExtra("eventid", this.m);
                intent4.putExtra("eventname", this.n.b);
                intent4.putExtra("eventdes", this.n.k);
                startActivity(intent4);
                return;
            case R.id.profile_layout_bottom_join /*2131166209*/:
                if (this.n.j == 1) {
                    new k("C", "C71103").e();
                    new ar(this, this).execute(new Object[0]);
                    return;
                }
                return;
            case R.id.eventprofile_iv_avatarcover /*2131166223*/:
                Intent intent5 = new Intent(this, ImageBrowserActivity.class);
                intent5.putExtra("array", new String[]{this.n.c});
                intent5.putExtra("imagetype", "event");
                intent5.putExtra("index", 0);
                startActivity(intent5);
                overridePendingTransition(R.anim.zoom_enter, R.anim.normal);
                return;
            case R.id.eventprofile_tv_adress /*2131166227*/:
                new k("C", "C71101").e();
                if (this.n != null) {
                    try {
                        Class.forName("com.google.android.maps.MapActivity");
                        Intent intent6 = new Intent(this, GoogleMapActivity.class);
                        intent6.putExtra("latitude", this.n.y);
                        intent6.putExtra("longitude", this.n.z);
                        intent6.putExtra("is_receive", true);
                        intent6.putExtra("is_show_add", true);
                        intent6.putExtra("add_title", this.n.b);
                        intent6.putExtra("add_info", this.n.f);
                        startActivity(intent6);
                        return;
                    } catch (Exception e) {
                        Location location = new Location(LocationManagerProxy.GPS_PROVIDER);
                        location.setLatitude(this.n.y);
                        location.setLongitude(this.n.z);
                        if (z.a(location)) {
                            Intent intent7 = new Intent(this, AMapActivity.class);
                            intent7.putExtra("latitude", this.n.y);
                            intent7.putExtra("longitude", this.n.z);
                            intent7.putExtra("is_receive", true);
                            intent7.putExtra("is_show_add", true);
                            intent7.putExtra("add_title", this.n.b);
                            intent7.putExtra("add_info", this.n.f);
                            startActivity(intent7);
                            return;
                        }
                        startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://maps.google.com/maps?q=" + this.n.y + "," + this.n.z)));
                        return;
                    }
                } else {
                    return;
                }
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        new k("PO", "P711").e();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        new k("PI", "P711").e();
        if (!this.k) {
            w();
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        bundle.putBoolean("from_saveinstance", true);
        bundle.putString("eventid", this.m);
        bundle.putString("tag", this.l);
        super.onSaveInstanceState(bundle);
    }
}
