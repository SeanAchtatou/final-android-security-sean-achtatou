package com.immomo.momo.android.activity.discuss;

import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Message;

final class m extends Handler {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ DiscussProfileActivity f1239a;

    m(DiscussProfileActivity discussProfileActivity) {
        this.f1239a = discussProfileActivity;
    }

    public final void handleMessage(Message message) {
        switch (message.what) {
            case 12:
                String string = message.getData().getString("guid");
                DiscussProfileActivity.a(this.f1239a, string, (Bitmap) message.obj);
                break;
        }
        super.handleMessage(message);
    }
}
