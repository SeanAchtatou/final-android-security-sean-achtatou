package com.immomo.momo.android.game;

import android.widget.RatingBar;
import android.widget.TextView;

final class u implements RatingBar.OnRatingBarChangeListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ MDKFeedBackActivity f2445a;

    u(MDKFeedBackActivity mDKFeedBackActivity) {
        this.f2445a = mDKFeedBackActivity;
    }

    public final void onRatingChanged(RatingBar ratingBar, float f, boolean z) {
        this.f2445a.j.setVisibility(0);
        TextView c = this.f2445a.j;
        MDKFeedBackActivity mDKFeedBackActivity = this.f2445a;
        c.setText(MDKFeedBackActivity.c((int) f));
        this.f2445a.q = (int) f;
    }
}
