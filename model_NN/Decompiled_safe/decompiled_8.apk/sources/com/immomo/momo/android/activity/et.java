package com.immomo.momo.android.activity;

import android.graphics.Bitmap;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.immomo.momo.R;
import com.immomo.momo.util.ao;

final class et extends WebViewClient {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ GroupLevelActivity f1343a;

    et(GroupLevelActivity groupLevelActivity) {
        this.f1343a = groupLevelActivity;
    }

    public final void onPageFinished(WebView webView, String str) {
        super.onPageFinished(webView, str);
        if (this.f1343a.j != null) {
            this.f1343a.j.setVisibility(8);
        }
    }

    public final void onPageStarted(WebView webView, String str, Bitmap bitmap) {
        super.onPageStarted(webView, str, bitmap);
        if (this.f1343a.j != null) {
            this.f1343a.j.setVisibility(0);
        }
    }

    public final void onReceivedError(WebView webView, int i, String str, String str2) {
        if (i == -2) {
            ao.e(R.string.errormsg_network_unfind);
        } else {
            ao.e(R.string.errormsg_server);
        }
        if (this.f1343a.j != null) {
            this.f1343a.j.setVisibility(8);
        }
    }
}
