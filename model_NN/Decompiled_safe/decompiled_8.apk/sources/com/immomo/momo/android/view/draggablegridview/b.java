package com.immomo.momo.android.view.draggablegridview;

final class b implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ DraggableGridView f2788a;

    b(DraggableGridView draggableGridView) {
        this.f2788a = draggableGridView;
    }

    public final void run() {
        if (this.f2788a.e != -1) {
            if (this.f2788a.f < this.f2788a.f2787a * 3 && this.f2788a.b > 0) {
                DraggableGridView draggableGridView = this.f2788a;
                draggableGridView.b -= 20;
            } else if (this.f2788a.f > (this.f2788a.getBottom() - this.f2788a.getTop()) - (this.f2788a.f2787a * 3) && this.f2788a.b < this.f2788a.getMaxScroll()) {
                this.f2788a.b += 20;
            }
        } else if (this.f2788a.c != 0.0f && !this.f2788a.g) {
            DraggableGridView draggableGridView2 = this.f2788a;
            draggableGridView2.b = (int) (((float) draggableGridView2.b) + this.f2788a.c);
            DraggableGridView draggableGridView3 = this.f2788a;
            draggableGridView3.c = (float) (((double) draggableGridView3.c) * 0.9d);
            if (((double) Math.abs(this.f2788a.c)) < 0.25d) {
                this.f2788a.c = 0.0f;
            }
        }
        this.f2788a.a();
        this.f2788a.onLayout(true, this.f2788a.getLeft(), this.f2788a.getTop(), this.f2788a.getRight(), this.f2788a.getBottom());
        this.f2788a.d.postDelayed(this, 25);
    }
}
