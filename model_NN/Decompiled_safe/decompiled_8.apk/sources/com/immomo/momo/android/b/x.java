package com.immomo.momo.android.b;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.v4.b.a;
import com.amap.mapapi.location.LocationManagerProxy;
import com.immomo.momo.util.m;
import java.util.HashMap;
import java.util.Map;

public final class x extends k {
    private static x d;
    private LocationManager b;
    /* access modifiers changed from: private */
    public m c = new m("GoogleLocater");
    private HashMap e = new HashMap();

    private x(Context context) {
        this.b = (LocationManager) context.getSystemService(LocationManagerProxy.KEY_LOCATION_CHANGED);
    }

    public static x a(Context context) {
        if (d == null) {
            d = new x(context);
        }
        return d;
    }

    /* access modifiers changed from: protected */
    public final void a(String str) {
        y yVar;
        if (!a.a((CharSequence) str)) {
            this.c.b((Object) ("Google locater cancel " + str));
            if (this.b != null && (yVar = (y) this.e.get(str)) != null) {
                this.b.removeUpdates(yVar);
                this.e.remove(str);
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void a(String str, p pVar) {
        y yVar = new y(this, pVar);
        this.e.put(str, yVar);
        this.b.requestLocationUpdates(LocationManagerProxy.GPS_PROVIDER, 0, 0.0f, yVar);
    }

    public final boolean a() {
        return this.b.isProviderEnabled(LocationManagerProxy.GPS_PROVIDER);
    }

    /* access modifiers changed from: protected */
    public final void b() {
        LocationListener locationListener;
        if (this.b != null) {
            this.c.a((Object) "manager is not null");
            for (Map.Entry entry : this.e.entrySet()) {
                if (!(entry == null || (locationListener = (LocationListener) entry.getValue()) == null)) {
                    this.c.a((Object) ("l is " + locationListener.toString()));
                    this.b.removeUpdates((LocationListener) entry.getValue());
                }
            }
            this.e.clear();
        }
    }

    /* access modifiers changed from: protected */
    public final void b(String str, p pVar) {
        y yVar = new y(this, pVar);
        this.e.put(str, yVar);
        this.b.requestLocationUpdates(LocationManagerProxy.NETWORK_PROVIDER, 0, 0.0f, yVar);
    }

    /* access modifiers changed from: protected */
    public final Location c() {
        boolean z = true;
        this.c.a((Object) "getLastKnowLocation called");
        Location lastKnownLocation = this.b.getLastKnownLocation(LocationManagerProxy.GPS_PROVIDER);
        Location lastKnownLocation2 = this.b.getLastKnownLocation(LocationManagerProxy.NETWORK_PROVIDER);
        this.c.a((Object) ("getLastKnowLocation gps location is null? : " + (lastKnownLocation == null)));
        m mVar = this.c;
        StringBuilder sb = new StringBuilder("getLastKnowLocation network location is null? : ");
        if (lastKnownLocation2 != null) {
            z = false;
        }
        mVar.a((Object) sb.append(z).toString());
        if (lastKnownLocation == null && lastKnownLocation2 == null) {
            return null;
        }
        if (lastKnownLocation == null && lastKnownLocation2 != null) {
            return lastKnownLocation2;
        }
        if (lastKnownLocation != null && lastKnownLocation2 == null) {
            return lastKnownLocation;
        }
        if (lastKnownLocation == null || lastKnownLocation2 == null) {
            return null;
        }
        this.c.a((Object) "getBetterLocation called");
        if (lastKnownLocation.getTime() > lastKnownLocation2.getTime()) {
            this.c.a((Object) "getBetterLocation-> gps is better");
        } else if (lastKnownLocation.getTime() < lastKnownLocation2.getTime()) {
            this.c.a((Object) "getBetterLocation-> network is better");
            return lastKnownLocation2;
        }
        return lastKnownLocation;
    }
}
