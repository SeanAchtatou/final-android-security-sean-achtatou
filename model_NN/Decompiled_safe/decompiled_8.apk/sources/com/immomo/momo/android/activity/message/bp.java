package com.immomo.momo.android.activity.message;

import android.content.Intent;
import com.immomo.momo.android.a.a.ab;
import com.immomo.momo.android.broadcast.d;

final class bp implements d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ GroupChatActivity f1949a;

    bp(GroupChatActivity groupChatActivity) {
        this.f1949a = groupChatActivity;
    }

    public final void a(Intent intent) {
        String stringExtra = intent.getStringExtra("key_message_id");
        long longExtra = intent.getLongExtra("key_upload_progress", 0);
        ab abVar = (ab) this.f1949a.ac.d.get(stringExtra);
        if (abVar != null) {
            long j = abVar.h.fileSize;
            if (longExtra >= 0) {
                abVar.h.fileUploadedLength = longExtra;
                abVar.a((((float) longExtra) * 100.0f) / ((float) j));
                if (longExtra < j) {
                    return;
                }
            }
            this.f1949a.ac.d.remove(stringExtra);
            this.f1949a.J();
        }
    }
}
