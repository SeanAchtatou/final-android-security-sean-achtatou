package com.immomo.momo.android.activity.message;

import com.immomo.momo.protocol.a.w;
import com.immomo.momo.service.bean.Message;
import com.immomo.momo.service.bean.bf;

final class bv implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private Message f1955a;
    private /* synthetic */ GroupChatActivity b;

    public bv(GroupChatActivity groupChatActivity, Message message) {
        this.b = groupChatActivity;
        this.f1955a = message;
        if (message.remoteUser == null) {
            message.remoteUser = new bf(message.remoteId);
        }
    }

    public final void run() {
        try {
            w.a().c(this.f1955a.remoteUser, this.f1955a.remoteId);
            this.b.N.d(this.f1955a.remoteUser);
            this.b.Y();
        } catch (Exception e) {
        }
    }
}
