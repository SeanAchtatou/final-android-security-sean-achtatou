package com.immomo.momo.android.a;

import android.view.View;
import com.immomo.momo.android.activity.ao;
import com.immomo.momo.android.activity.d;
import com.immomo.momo.service.bean.ag;
import com.immomo.momo.service.bean.ah;

final class de implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private ah f771a;
    private ag b;
    private /* synthetic */ da c;

    public de(da daVar, ag agVar, ah ahVar) {
        this.c = daVar;
        this.f771a = ahVar;
        this.b = agVar;
    }

    public final void onClick(View view) {
        if (d.a(this.f771a.b())) {
            d.a(this.f771a.f(), this.c.d);
        } else {
            ((ao) this.c.d).b(new df(this.c, this.c.d, this.b, this.f771a));
        }
    }
}
