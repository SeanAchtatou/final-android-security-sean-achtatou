package com.immomo.momo.android.activity.event;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.plugin.g.a;
import com.immomo.momo.protocol.a.j;
import com.immomo.momo.util.h;
import java.io.File;

final class av extends d {

    /* renamed from: a  reason: collision with root package name */
    private v f1365a = null;
    private boolean c = false;
    private boolean d = false;
    private boolean e = false;
    private boolean f = false;
    /* access modifiers changed from: private */
    public /* synthetic */ EventProfileActivity g;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public av(EventProfileActivity eventProfileActivity, Context context, boolean z, boolean z2, boolean z3, boolean z4) {
        super(context);
        this.g = eventProfileActivity;
        this.c = z;
        this.d = z2;
        this.e = z3;
        this.f = z4;
        this.f1365a = new v(context);
        this.f1365a.a("请求提交中");
        this.f1365a.setCancelable(true);
        this.f1365a.setOnCancelListener(new aw(this));
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        return j.a().a(this.g.m, this.c, this.d, this.e, this.f);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.g.a(this.f1365a);
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        super.a(exc);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        String str = (String) obj;
        this.g.b((CharSequence) "活动分享成功");
        File a2 = h.a(this.g.n.c, 20);
        if (this.f) {
            a.a().a(str, "我参加了陌陌活动：" + this.g.n.b, a2);
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.g.p();
    }
}
