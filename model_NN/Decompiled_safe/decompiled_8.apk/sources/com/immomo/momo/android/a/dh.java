package com.immomo.momo.android.a;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v4.b.a;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.OtherProfileActivity;
import com.immomo.momo.android.activity.emotestore.EmotionProfileActivity;
import com.immomo.momo.android.c.u;
import com.immomo.momo.android.view.AltImageView;
import com.immomo.momo.android.view.HandyListView;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.ae;
import com.immomo.momo.service.bean.aj;
import com.immomo.momo.service.bean.q;
import com.immomo.momo.util.j;
import com.immomo.momo.util.m;
import java.io.File;
import java.lang.ref.SoftReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class dh extends a implements View.OnClickListener {
    private static Map h = new HashMap(24);
    private m d;
    private int e;
    private Activity f;
    private HandyListView g;

    public dh(Activity activity, HandyListView handyListView) {
        super(activity, new ArrayList());
        this.d = new m("test_momo", "[ -- FriendsFeedCommentAdapter -- ]");
        this.e = 0;
        this.f = null;
        this.g = null;
        this.e = g.n();
        this.g = handyListView;
        this.f = activity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, com.immomo.momo.android.view.HandyListView):void
     arg types: [com.immomo.momo.service.bean.bf, android.widget.ImageView, com.immomo.momo.android.view.HandyListView]
     candidates:
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, int):void
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup):void
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, com.immomo.momo.android.view.HandyListView):void */
    public final View getView(int i, View view, ViewGroup viewGroup) {
        dk dkVar;
        String str = null;
        if (view == null) {
            view = g.o().inflate((int) R.layout.listitem_feedcomment, (ViewGroup) null);
            dk dkVar2 = new dk((byte) 0);
            view.setTag(R.id.tag_userlist_item, dkVar2);
            dkVar2.c = (TextView) view.findViewById(R.id.tv_comment_name);
            dkVar2.e = (AltImageView) view.findViewById(R.id.iv_comment_emotion);
            dkVar2.b = (TextView) view.findViewById(R.id.tv_comment_content);
            dkVar2.f776a = (TextView) view.findViewById(R.id.tv_comment_time);
            dkVar2.d = (ImageView) view.findViewById(R.id.iv_comment_photo);
            dkVar2.d.setOnClickListener(this);
            dkVar = dkVar2;
        } else {
            dkVar = (dk) view.getTag(R.id.tag_userlist_item);
        }
        dkVar.d.setTag(R.id.tag_item_position, Integer.valueOf(i));
        ae aeVar = (ae) getItem(i);
        if (aeVar.f2985a != null) {
            dkVar.c.setText(aeVar.f2985a.h());
        } else {
            dkVar.c.setText(aeVar.b);
        }
        if (aeVar.n == 1) {
            Matcher matcher = Pattern.compile("(\\[[.[^\\[\\]]]*?\\|et\\|([.[^\\[\\]]]*?\\|)*[.[^\\[\\]]]*?\\])").matcher(aeVar.f);
            String group = matcher.find() ? matcher.group() : null;
            if (a.f(group)) {
                dkVar.e.setVisibility(0);
                String replace = aeVar.f.replace(group, PoiTypeDef.All);
                Context context = this.b;
                com.immomo.momo.plugin.b.a aVar = new com.immomo.momo.plugin.b.a(group);
                dkVar.e.setTag(R.id.tag_item_emotionspan, aVar);
                dkVar.e.setAlt(aVar.i());
                AltImageView altImageView = dkVar.e;
                SoftReference softReference = (SoftReference) h.get(aVar.g());
                Bitmap bitmap = softReference == null ? null : (Bitmap) softReference.get();
                if (bitmap == null && !this.g.g()) {
                    File a2 = q.a(aVar.g(), aVar.h());
                    if (a2.exists()) {
                        this.d.a((Object) "---------------load emotion");
                        bitmap = a.l(a2.getPath());
                        if (bitmap != null) {
                            h.put(aVar.g(), new SoftReference(bitmap));
                        }
                        this.d.a((Object) "---------------load emotion end");
                    } else if (!aeVar.isImageLoading()) {
                        aeVar.setImageLoading(true);
                        u.b().execute(new di(this, aVar));
                    }
                }
                altImageView.setImageBitmap(bitmap);
                ViewGroup.LayoutParams layoutParams = dkVar.e.getLayoutParams();
                layoutParams.height = this.e;
                layoutParams.width = (int) ((((float) this.e) / ((float) aVar.k())) * ((float) aVar.j()));
                dkVar.e.setLayoutParams(layoutParams);
                str = replace;
            } else {
                ViewGroup.LayoutParams layoutParams2 = dkVar.e.getLayoutParams();
                layoutParams2.height = this.e;
                layoutParams2.width = this.e;
                dkVar.e.setLayoutParams(layoutParams2);
            }
            if (a.f(str)) {
                dkVar.b.setVisibility(0);
                dkVar.b.setText(str);
            } else {
                dkVar.b.setVisibility(8);
            }
        } else {
            dkVar.e.setVisibility(8);
            dkVar.b.setVisibility(0);
            dkVar.b.setText(aeVar.f);
        }
        j.a((aj) aeVar.f2985a, dkVar.d, this.g);
        dkVar.f776a.setText(aeVar.e);
        return view;
    }

    public final void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_comment_photo /*2131166104*/:
                Intent intent = new Intent();
                intent.setClass(this.f, OtherProfileActivity.class);
                intent.putExtra("tag", "local");
                intent.putExtra("momoid", ((ae) getItem(((Integer) view.getTag(R.id.tag_item_position)).intValue())).b);
                this.f.startActivity(intent);
                return;
            case R.id.iv_comment_emotion /*2131166669*/:
                Intent intent2 = new Intent(this.f, EmotionProfileActivity.class);
                intent2.putExtra("eid", ((com.immomo.momo.plugin.b.a) view.getTag(R.id.tag_item_emotionspan)).h());
                this.f.startActivity(intent2);
                return;
            default:
                return;
        }
    }
}
