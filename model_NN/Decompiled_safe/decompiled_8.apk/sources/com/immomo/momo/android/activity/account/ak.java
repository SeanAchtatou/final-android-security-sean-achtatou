package com.immomo.momo.android.activity.account;

import android.content.Context;
import com.immomo.momo.R;
import com.immomo.momo.a.g;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.w;
import com.immomo.momo.util.ao;

final class ak extends d {

    /* renamed from: a  reason: collision with root package name */
    private v f939a = null;
    private String c = null;
    private /* synthetic */ ac d;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ak(ac acVar, Context context, String str) {
        super(context);
        this.d = acVar;
        this.c = str;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        this.b.a((Object) ("校验：" + this.c));
        return w.a().c(this.c, this.d.i);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.f939a = new v(this.d.f, "请稍候，正在校验....");
        this.f939a.setOnCancelListener(new al(this));
        this.f939a.show();
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        if (exc instanceof g) {
            this.d.f.b(new am(this.d, this.d.f));
            this.d.k.setImageBitmap(null);
            ao.d(R.string.reg_scode_timeout);
            return;
        }
        super.a(exc);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        String str = (String) obj;
        if (this.d.m != null && this.d.m.isShowing()) {
            this.d.m.dismiss();
        }
        this.d.f.l = str;
        this.d.f.b(false);
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.f939a.dismiss();
        this.f939a = null;
    }
}
