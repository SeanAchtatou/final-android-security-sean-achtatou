package com.immomo.momo.android.activity;

import android.content.DialogInterface;

final class o implements DialogInterface.OnCancelListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ AlipayUserWelcomeActivity f2026a;

    o(AlipayUserWelcomeActivity alipayUserWelcomeActivity) {
        this.f2026a = alipayUserWelcomeActivity;
    }

    public final void onCancel(DialogInterface dialogInterface) {
        dialogInterface.dismiss();
        this.f2026a.finish();
    }
}
