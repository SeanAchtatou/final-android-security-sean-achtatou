package com.immomo.momo.android.activity;

import android.content.Intent;
import android.view.View;
import java.util.ArrayList;

final class fy implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ MulImagePickerActivity f1506a;

    fy(MulImagePickerActivity mulImagePickerActivity) {
        this.f1506a = mulImagePickerActivity;
    }

    public final void onClick(View view) {
        Intent intent = new Intent();
        intent.putStringArrayListExtra("select_images_path", (ArrayList) this.f1506a.k.f2956a);
        this.f1506a.setResult(-1, intent);
        this.f1506a.finish();
    }
}
