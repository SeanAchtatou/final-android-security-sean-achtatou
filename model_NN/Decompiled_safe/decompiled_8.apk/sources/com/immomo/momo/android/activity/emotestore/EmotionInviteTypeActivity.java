package com.immomo.momo.android.activity.emotestore;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.ah;
import com.immomo.momo.android.activity.contacts.CommunityPeopleActivity;
import com.immomo.momo.android.activity.setting.CommunityBindActivity;
import com.immomo.momo.g;
import com.sina.weibo.sdk.constant.Constants;

public class EmotionInviteTypeActivity extends ah implements View.OnClickListener {
    private Button h;
    private ImageView i;
    private ImageView j;
    private ImageView k;
    private TextView l;
    /* access modifiers changed from: private */
    public boolean m = true;
    /* access modifiers changed from: private */
    public boolean n = true;
    /* access modifiers changed from: private */
    public boolean o = true;

    /* access modifiers changed from: private */
    public void u() {
        if (this.m) {
            this.i.setImageResource(R.drawable.ic_publish_weibo_selected);
        } else {
            this.i.setImageResource(R.drawable.ic_publish_weibo_normal);
        }
    }

    /* access modifiers changed from: private */
    public void v() {
        if (this.n) {
            this.j.setImageResource(R.drawable.ic_publish_renren_selected);
        } else {
            this.j.setImageResource(R.drawable.ic_publish_renren_normal);
        }
    }

    /* access modifiers changed from: private */
    public void w() {
        if (this.o) {
            this.k.setImageResource(R.drawable.ic_publish_tweibo_selected);
        } else {
            this.k.setImageResource(R.drawable.ic_publish_tweibo_normal);
        }
    }

    private boolean x() {
        return this.m && this.f.an;
    }

    private boolean y() {
        return this.n && this.f.ar;
    }

    private boolean z() {
        return this.o && this.f.at;
    }

    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        super.a(bundle);
        setContentView((int) R.layout.activity_emotestore_invitetype);
        this.i = (ImageView) findViewById(R.id.iv_sina);
        this.j = (ImageView) findViewById(R.id.iv_renren);
        this.k = (ImageView) findViewById(R.id.iv_tx);
        this.h = (Button) findViewById(R.id.btn_share);
        this.l = (TextView) findViewById(R.id.tv_content);
        String a2 = g.a((int) R.string.emotion_invitetype_str2);
        this.l.setText(String.format(a2, this.f.h));
        if (this.f.an) {
            this.i.setVisibility(0);
            u();
        } else {
            this.i.setVisibility(8);
        }
        if (this.f.at) {
            this.k.setVisibility(0);
            w();
        } else {
            this.k.setVisibility(8);
        }
        if (this.f.ar) {
            this.j.setVisibility(0);
            v();
        } else {
            this.j.setVisibility(8);
        }
        this.h.setOnClickListener(this);
        findViewById(R.id.layout_sms_invite).setOnClickListener(this);
        findViewById(R.id.layout_renren_invite).setOnClickListener(this);
        findViewById(R.id.layout_txweibo_invite).setOnClickListener(this);
        findViewById(R.id.layout_weibo_invite).setOnClickListener(this);
        this.i.setOnClickListener(new d(this));
        this.j.setOnClickListener(new e(this));
        this.k.setOnClickListener(new f(this));
        setTitle("邀请方式");
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        Intent intent2 = new Intent();
        switch (i2) {
            case 21:
                if (i3 == -1) {
                    intent2.setClass(this, CommunityPeopleActivity.class);
                    intent2.putExtra("type", 3);
                    intent2.putExtra("from", 22);
                    startActivity(intent2);
                    return;
                }
                return;
            case Constants.WEIBO_SDK_VERSION /*22*/:
                if (i3 == -1) {
                    intent2.setClass(this, CommunityPeopleActivity.class);
                    intent2.putExtra("type", 2);
                    intent2.putExtra("from", 22);
                    startActivity(intent2);
                    return;
                }
                return;
            case 23:
                if (i3 == -1) {
                    intent2.setClass(this, CommunityPeopleActivity.class);
                    intent2.putExtra("type", 1);
                    intent2.putExtra("from", 22);
                    startActivity(intent2);
                    return;
                }
                return;
            default:
                return;
        }
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_share /*2131165558*/:
                if (x() || y() || z()) {
                    new g(this, this, x(), y(), z()).execute(new String[0]);
                    return;
                } else if (this.f.ar || this.f.an || this.f.at) {
                    a((CharSequence) "请至少选择一种社交网络");
                    return;
                } else {
                    a((CharSequence) "当前没有绑定社交网络，无法分享");
                    return;
                }
            case R.id.layout_sms_invite /*2131165559*/:
                try {
                    Intent intent = new Intent("android.intent.action.SENDTO", Uri.parse("smsto:"));
                    intent.putExtra("sms_body", String.format(g.a((int) R.string.emotion_invitetype_str2), this.f.h));
                    startActivity(intent);
                    return;
                } catch (Exception e) {
                    a((CharSequence) "请确认系统是否安装短信应用");
                    return;
                }
            case R.id.layout_weibo_invite /*2131165560*/:
                if (this.f.an) {
                    Intent intent2 = new Intent();
                    intent2.setClass(this, CommunityPeopleActivity.class);
                    intent2.putExtra("type", 1);
                    intent2.putExtra("from", 22);
                    startActivity(intent2);
                    return;
                }
                Intent intent3 = new Intent(this, CommunityBindActivity.class);
                intent3.putExtra("type", 1);
                startActivityForResult(intent3, 23);
                return;
            case R.id.layout_txweibo_invite /*2131165561*/:
                if (this.f.at) {
                    Intent intent4 = new Intent();
                    intent4.setClass(this, CommunityPeopleActivity.class);
                    intent4.putExtra("type", 2);
                    intent4.putExtra("from", 22);
                    startActivity(intent4);
                    return;
                }
                Intent intent5 = new Intent(this, CommunityBindActivity.class);
                intent5.putExtra("type", 2);
                startActivityForResult(intent5, 22);
                return;
            case R.id.layout_renren_invite /*2131165562*/:
                if (this.f.ar) {
                    Intent intent6 = new Intent();
                    intent6.setClass(this, CommunityPeopleActivity.class);
                    intent6.putExtra("type", 3);
                    intent6.putExtra("from", 22);
                    startActivity(intent6);
                    return;
                }
                Intent intent7 = new Intent(this, CommunityBindActivity.class);
                intent7.putExtra("type", 3);
                startActivityForResult(intent7, 21);
                return;
            default:
                return;
        }
    }
}
