package com.immomo.momo.android.broadcast;

import android.content.Context;
import com.immomo.momo.g;

public final class p extends b {

    /* renamed from: a  reason: collision with root package name */
    public static final String f2359a = (String.valueOf(g.h()) + ".action.feed.publish.sitefeed");
    public static final String b = (String.valueOf(g.h()) + ".action.feed.publish.nearbyfeed");

    public p(Context context) {
        super(context);
        a(b, f2359a);
    }
}
