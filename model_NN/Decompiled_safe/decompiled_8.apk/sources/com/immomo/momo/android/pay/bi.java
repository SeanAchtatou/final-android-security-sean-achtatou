package com.immomo.momo.android.pay;

import android.content.Context;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.g;
import com.immomo.momo.protocol.a.a;
import com.unicom.dcLoader.Utils;
import com.unicom.dcLoader.c;
import java.util.Map;

final class bi extends d {

    /* renamed from: a  reason: collision with root package name */
    private v f2552a = null;
    private Map c;
    private /* synthetic */ RechargeActivity d;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public bi(RechargeActivity rechargeActivity, Context context, Map map) {
        super(context);
        this.d = rechargeActivity;
        this.c = map;
        this.f2552a = new v(context);
        rechargeActivity.w = new bk();
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        return a.a().a(this.d.w, g.E(), g.D(), this.d.g(), this.c);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
        this.f2552a.setCancelable(true);
        this.f2552a.setOnCancelListener(new bj(this));
        this.f2552a.a("请求提交中...");
        this.d.a(this.f2552a);
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        super.a(exc);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        if (!this.d.u) {
            Utils.a().a(this.d, this.d.w.l, this.d.w.f2554a, this.d.w.e, this.d.w.h, this.d.w.j, this.d.w.k, new bd(this.d));
            this.d.u = true;
        }
        Utils.a().a(this.d.n(), this.d.w.f);
        Utils.a().a(this.d.n(), this.d.w.c, PoiTypeDef.All, (String) this.c.get("subject"), this.d.w.g, this.d.w.b, g.I(), c.single, new bd(this.d));
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.d.p();
    }
}
