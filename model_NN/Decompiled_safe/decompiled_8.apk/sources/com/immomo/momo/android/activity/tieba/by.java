package com.immomo.momo.android.activity.tieba;

import android.content.DialogInterface;
import com.immomo.momo.android.view.a.ao;
import com.immomo.momo.service.bean.c.b;

final class by implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ TieDetailActivity f2199a;
    private final /* synthetic */ ao b;
    private final /* synthetic */ b c;

    by(TieDetailActivity tieDetailActivity, ao aoVar, b bVar) {
        this.f2199a = tieDetailActivity;
        this.b = aoVar;
        this.c = bVar;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        if (this.b.e() || this.b.f() || this.b.g() || this.b.h()) {
            this.f2199a.b(new cn(this.f2199a, this.f2199a, this.c, this.b.e(), this.b.f(), this.b.g(), this.b.h()));
            dialogInterface.dismiss();
        }
    }
}
