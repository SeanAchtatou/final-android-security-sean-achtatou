package com.immomo.momo.android.activity.common;

import com.immomo.momo.android.a.hp;

final class bh implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ bb f1101a;
    private final /* synthetic */ String b;

    bh(bb bbVar, String str) {
        this.f1101a = bbVar;
        this.b = str;
    }

    public final void run() {
        int a2 = this.f1101a.f1095a.P.a(this.b, 0);
        if (a2 < 0 || a2 > this.f1101a.f1095a.P.getCount()) {
            this.f1101a.f1095a.Q = this.f1101a.f1095a.T.c();
            this.f1101a.f1095a.aa.post(new bi(this));
            return;
        }
        hp hpVar = (hp) this.f1101a.f1095a.P.getItem(this.f1101a.f1095a.P.a(this.b, 0));
        if (hpVar != null && hpVar.f != null) {
            this.f1101a.f1095a.T.a(hpVar.f, this.b);
            this.f1101a.f1095a.aa.post(new bj(this));
        }
    }
}
