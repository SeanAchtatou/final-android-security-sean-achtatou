package com.immomo.momo.android.activity.account;

import android.content.Intent;
import android.support.v4.b.a;

final class af implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ae f934a;

    af(ae aeVar) {
        this.f934a = aeVar;
    }

    public final void run() {
        Intent intent = new Intent(this.f934a.f933a.f, RegisterActivityWithP.class);
        intent.putExtra("registInterfacetype", this.f934a.f933a.f.k);
        intent.putExtra("timestamp", this.f934a.f933a.f.i);
        intent.putExtra("starttime", this.f934a.f933a.f.j);
        if (a.f(this.f934a.f933a.f.h)) {
            intent.putExtra("alipay_user_id", this.f934a.f933a.f.h);
        }
        this.f934a.f933a.f.startActivity(intent);
        this.f934a.f933a.f.finish();
        this.f934a.f933a.f.overridePendingTransition(0, 0);
    }
}
