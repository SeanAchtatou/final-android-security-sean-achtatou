package com.immomo.momo.android.activity.feed;

import com.immomo.momo.plugin.g.a;
import com.immomo.momo.service.bean.av;
import com.immomo.momo.service.bean.q;
import java.io.File;

final class at implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ PublishFeedActivity f1451a;

    at(PublishFeedActivity publishFeedActivity) {
        this.f1451a = publishFeedActivity;
    }

    public final void run() {
        File file = null;
        if (this.f1451a.v == 2) {
            if (this.f1451a.A.getDatalist() != null && this.f1451a.A.getDatalist().size() > 0) {
                file = ((av) this.f1451a.A.getDatalist().get(0)).b;
            }
        } else if (this.f1451a.v == 1 && this.f1451a.M != null) {
            file = q.a(this.f1451a.M.g(), this.f1451a.M.h());
        }
        a.a().a(this.f1451a.S, this.f1451a.p.getText().toString().trim(), file);
    }
}
