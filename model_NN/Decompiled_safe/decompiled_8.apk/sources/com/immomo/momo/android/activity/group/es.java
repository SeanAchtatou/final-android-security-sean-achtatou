package com.immomo.momo.android.activity.group;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.b.a;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.service.bean.av;
import com.immomo.momo.util.j;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import mm.purchasesdk.PurchaseCode;

final class es extends d {

    /* renamed from: a  reason: collision with root package name */
    private List f1640a = null;
    private v c = null;
    /* access modifiers changed from: private */
    public /* synthetic */ PublishGroupFeedActivity d;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public es(PublishGroupFeedActivity publishGroupFeedActivity, Context context, List list) {
        super(context);
        this.d = publishGroupFeedActivity;
        this.f1640a = list;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        ArrayList arrayList = new ArrayList();
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.f1640a.size()) {
                return arrayList;
            }
            av avVar = new av();
            if (!a.a((CharSequence) this.f1640a.get(i2))) {
                avVar.f2998a = (String) this.f1640a.get(i2);
                av avVar2 = (av) this.d.B.get(avVar.f2998a);
                if (avVar2 == null) {
                    File file = new File(avVar.f2998a);
                    if (file.exists()) {
                        Bitmap a2 = a.a(file, (int) PurchaseCode.LOADCHANNEL_ERR, (int) PurchaseCode.LOADCHANNEL_ERR);
                        if (a2 != null) {
                            avVar.c = a2;
                            j.a(avVar.f2998a, a2);
                        }
                        avVar.b = file;
                        this.d.B.put(avVar.f2998a, avVar);
                        avVar2 = avVar;
                    } else {
                        avVar2 = null;
                    }
                }
                if (avVar2 != null) {
                    arrayList.add(avVar2);
                }
            }
            i = i2 + 1;
        }
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.c = new v(this.d.n(), "正在处理...");
        this.c.setCancelable(false);
        this.d.a(this.c);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        this.d.r.post(new et(this, (ArrayList) obj));
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.d.p();
    }
}
