package com.immomo.momo.android.broadcast;

import android.content.Context;
import android.content.IntentFilter;
import com.immomo.momo.g;

public final class i extends b {

    /* renamed from: a  reason: collision with root package name */
    public static final String f2352a = (String.valueOf(g.h()) + ".action.fileuploadprogress");

    public i(Context context) {
        super(context);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(f2352a);
        a(intentFilter);
    }
}
