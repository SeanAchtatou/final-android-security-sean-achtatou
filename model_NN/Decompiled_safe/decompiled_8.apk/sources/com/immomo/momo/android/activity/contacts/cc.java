package com.immomo.momo.android.activity.contacts;

import android.content.Context;
import android.content.Intent;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.w;
import com.immomo.momo.service.bean.at;

final class cc extends d {

    /* renamed from: a  reason: collision with root package name */
    private v f1184a = null;
    private boolean c = false;
    private /* synthetic */ OpenContactActivity d;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public cc(OpenContactActivity openContactActivity, Context context) {
        super(context);
        this.d = openContactActivity;
        if (openContactActivity.i != null) {
            openContactActivity.i.cancel(true);
        }
        openContactActivity.i = this;
        this.c = true;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        return Boolean.valueOf(w.a().a(this.c));
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
        this.f1184a = new v(this.d);
        this.f1184a.a("请求提交中...");
        this.f1184a.setCancelable(true);
        this.f1184a.setOnCancelListener(new cd(this));
        this.d.a(this.f1184a);
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        super.a(exc);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.service.bean.at.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, boolean]
     candidates:
      com.immomo.momo.service.bean.at.a(android.content.Context, java.lang.String):com.immomo.momo.service.bean.at
      com.immomo.momo.service.bean.at.a(java.lang.Integer, java.lang.Integer):void
      com.immomo.momo.service.bean.at.a(java.lang.String, java.util.Date):void
      com.immomo.momo.service.bean.at.a(java.lang.String, java.lang.Object):void */
    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        if (((Boolean) obj).booleanValue()) {
            if (this.d.g != null) {
                at b = this.d.g;
                this.d.g.c = true;
                b.a("phonebook_syn", (Object) true);
            }
            this.d.finish();
            this.d.startActivity(new Intent(this.d, ContactPeopleActivity.class));
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
        super.b();
        this.d.p();
    }
}
