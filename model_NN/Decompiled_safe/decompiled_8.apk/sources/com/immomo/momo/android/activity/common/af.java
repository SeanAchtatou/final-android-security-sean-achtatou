package com.immomo.momo.android.activity.common;

import android.content.Context;
import android.content.Intent;
import android.support.v4.b.a;
import com.immomo.momo.android.activity.message.MultiChatActivity;
import com.immomo.momo.android.broadcast.t;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.h;
import com.immomo.momo.service.bean.n;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

final class af extends d {

    /* renamed from: a  reason: collision with root package name */
    private v f1077a = null;
    private String c;
    private Map d = null;
    private /* synthetic */ CreateDiscussTabsActivity e;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public af(CreateDiscussTabsActivity createDiscussTabsActivity, Context context, HashMap hashMap) {
        super(context);
        this.e = createDiscussTabsActivity;
        this.d = hashMap;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        ArrayList arrayList = new ArrayList();
        for (Map.Entry key : this.d.entrySet()) {
            arrayList.add((String) key.getKey());
        }
        this.c = a.a(arrayList, ",");
        n d2 = h.a().d(this.c);
        com.immomo.momo.service.h hVar = new com.immomo.momo.service.h();
        hVar.a(d2);
        hVar.b(d2);
        return d2.f;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
        this.f1077a = new v(this.e);
        this.f1077a.a("请求提交中...");
        this.f1077a.setCancelable(true);
        this.f1077a.setOnCancelListener(new ag(this));
        this.e.a(this.f1077a);
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        super.a(exc);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        Intent intent = new Intent();
        intent.setAction(t.f2363a);
        this.e.sendBroadcast(intent);
        Intent intent2 = new Intent(this.e, MultiChatActivity.class);
        intent2.putExtra("remoteDiscussID", (String) obj);
        this.e.startActivity(intent2);
        this.e.finish();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        super.b();
        this.e.p();
    }
}
