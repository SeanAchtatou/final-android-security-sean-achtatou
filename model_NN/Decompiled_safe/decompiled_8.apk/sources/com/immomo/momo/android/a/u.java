package com.immomo.momo.android.a;

import android.view.View;

final class u implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ q f915a;
    private final /* synthetic */ aa b;
    private final /* synthetic */ int c;
    private final /* synthetic */ int d;

    u(q qVar, aa aaVar, int i, int i2) {
        this.f915a = qVar;
        this.b = aaVar;
        this.c = i;
        this.d = i2;
    }

    public final void onClick(View view) {
        this.f915a.b.getOnChildClickListener().onChildClick(this.f915a.b, this.b.f, this.c, this.d, 0);
    }
}
