package com.immomo.momo.android.map;

import android.location.Location;
import com.immomo.momo.R;
import com.immomo.momo.android.b.p;
import com.immomo.momo.android.b.z;
import com.immomo.momo.util.ao;

final class aj extends p {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ RomaAMapActivity f2463a;

    aj(RomaAMapActivity romaAMapActivity) {
        this.f2463a = romaAMapActivity;
    }

    public final void a(Location location, int i, int i2, int i3) {
        if (this.b) {
            if (z.a(location)) {
                this.f2463a.q.post(new ak(this, location));
                return;
            }
            this.f2463a.a();
            if (i2 == 104) {
                ao.g(R.string.errormsg_network_unfind);
            } else {
                ao.g(R.string.errormsg_location_nearby_failed);
            }
        }
    }
}
