package com.immomo.momo.android.activity;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.android.a.a;

final class lp extends a {
    lp(Context context) {
        super(context);
    }

    public final int getCount() {
        return 30;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = this.c.inflate((int) R.layout.listitem_textview, (ViewGroup) null);
        }
        ((TextView) view.findViewById(R.id.textview)).setText(new StringBuilder(String.valueOf(i)).toString());
        return view;
    }
}
