package com.immomo.momo.android.a.a;

import android.graphics.drawable.AnimationDrawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.maintab.bh;
import com.immomo.momo.android.view.HandyListView;
import com.immomo.momo.android.view.ce;
import com.immomo.momo.g;
import com.immomo.momo.protocol.imjson.util.c;
import com.immomo.momo.service.bean.Message;
import com.immomo.momo.service.bean.a.a;
import com.immomo.momo.service.bean.a.i;
import com.immomo.momo.service.bean.at;
import com.immomo.momo.service.bean.ax;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.service.bean.n;
import com.immomo.momo.service.bean.o;
import com.immomo.momo.util.j;
import com.immomo.momo.util.m;
import java.util.ArrayList;

public final class ah extends ce {
    /* access modifiers changed from: private */
    public bh d = null;
    /* access modifiers changed from: private */
    public HandyListView e = null;

    public ah(bh bhVar, ArrayList arrayList, HandyListView handyListView) {
        super(bh.H(), arrayList);
        new ai(this);
        this.d = bhVar;
        new m("test_momo", "[ --- from SessionListViewAdapter --- ]");
        this.e = handyListView;
    }

    static /* synthetic */ void e() {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.b.a.a(java.util.Date, boolean):java.lang.String
     arg types: [java.util.Date, int]
     candidates:
      android.support.v4.b.a.a(android.graphics.Bitmap, float):android.graphics.Bitmap
      android.support.v4.b.a.a(android.graphics.Bitmap, java.io.File):android.graphics.Bitmap
      android.support.v4.b.a.a(int, int):java.lang.String
      android.support.v4.b.a.a(java.util.Collection, java.lang.String):java.lang.String
      android.support.v4.b.a.a(java.lang.Object[], java.lang.String):java.lang.String
      android.support.v4.b.a.a(java.lang.String, java.lang.String):org.json.JSONObject
      android.support.v4.b.a.a(float, int[]):void
      android.support.v4.b.a.a(android.app.Activity, int):void
      android.support.v4.b.a.a(android.view.View, java.lang.Runnable):void
      android.support.v4.b.a.a(java.io.File, java.io.File):void
      android.support.v4.b.a.a(java.io.File, java.util.zip.ZipInputStream):void
      android.support.v4.b.a.a(java.lang.Object, java.lang.StringBuilder):void
      android.support.v4.b.a.a(android.content.Context, java.lang.String):boolean
      android.support.v4.b.a.a(com.immomo.a.a.d.b, com.immomo.momo.service.bean.Message):boolean
      android.support.v4.b.a.a(byte[], java.lang.String):byte[]
      android.support.v4.b.a.a(byte[], byte[]):byte[]
      android.support.v4.b.a.a(int[], boolean):byte[]
      android.support.v4.b.a.a(byte[], boolean):int[]
      android.support.v4.b.a.a(int, float):void
      android.support.v4.b.a.a(java.lang.Object, com.immomo.a.a.f):void
      android.support.v4.view.bf.a(int, float):void
      com.immomo.a.a.f.a(java.lang.Object, com.immomo.a.a.f):void
      android.support.v4.b.a.a(java.util.Date, boolean):java.lang.String */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.j.a(android.widget.ImageView, boolean, boolean, int):void
     arg types: [android.widget.ImageView, int, int, int]
     candidates:
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup, int):void
      com.immomo.momo.util.j.a(android.widget.ImageView, boolean, boolean, int):void */
    public final View a(int i, View view) {
        ap apVar;
        String str;
        if (view == null) {
            ap apVar2 = new ap((byte) 0);
            view = g.o().inflate((int) R.layout.listitem_session, (ViewGroup) null);
            apVar2.f685a = (ImageView) view.findViewById(R.id.chatlist_item_iv_face);
            apVar2.c = (TextView) view.findViewById(R.id.chatlist_item_tv_name);
            apVar2.d = (TextView) view.findViewById(R.id.chatlist_item_tv_status);
            apVar2.e = (TextView) view.findViewById(R.id.chatlist_item_tv_status_new);
            apVar2.f = (ImageView) view.findViewById(R.id.chatlist_item_iv_status);
            apVar2.g = (TextView) view.findViewById(R.id.chatlist_item_tv_timestamp);
            apVar2.h = (TextView) view.findViewById(R.id.chatlist_item_tv_content);
            apVar2.i = view.findViewById(R.id.item_layout);
            apVar2.j = view.findViewById(R.id.avatar_layout);
            apVar2.m = view.findViewById(R.id.userlist_item_mute);
            apVar2.l = apVar2.j.findViewById(R.id.chatlist_item_iv_face_cover_click);
            apVar2.k = apVar2.j.findViewById(R.id.chatlist_item_iv_face_cover_select);
            apVar2.b = (ImageView) view.findViewById(R.id.chatlist_item_iv_status_point);
            view.setTag(R.id.tag_item, apVar2);
            apVar = apVar2;
        } else {
            apVar = (ap) view.getTag(R.id.tag_item);
        }
        ax axVar = (ax) this.f671a.get(i);
        if (axVar != null && axVar.o == 2) {
            at r = g.r();
            if (r != null) {
                i c = r.c(axVar.f3000a);
                if (c == null || c.f2978a) {
                    apVar.m.setVisibility(8);
                } else {
                    apVar.m.setVisibility(0);
                }
            } else {
                apVar.m.setVisibility(8);
            }
        } else if (axVar == null || axVar.o != 6) {
            apVar.m.setVisibility(8);
        } else {
            at r2 = g.r();
            if (r2 != null) {
                o d2 = r2.d(axVar.f3000a);
                if (d2 == null || d2.f3033a) {
                    apVar.m.setVisibility(8);
                } else {
                    apVar.m.setVisibility(0);
                }
            }
        }
        if (axVar.o == 1) {
            apVar.c.setText("陌生人向你打招呼");
            apVar.f685a.setImageResource(R.drawable.ic_header_sayhi);
        } else if (axVar.o == 5) {
            apVar.c.setText("好友推荐");
            apVar.f685a.setImageResource(R.drawable.ic_header_contacts);
        } else if (axVar.o == 8) {
            apVar.c.setText("好友出没");
            apVar.f685a.setImageResource(R.drawable.ic_header_fdiscover);
        } else if (axVar.o == 2) {
            if (axVar.c == null) {
                axVar.c = new a(axVar.f3000a);
                apVar.c.setText(axVar.f3000a);
                j.a(apVar.f685a, false, false, -1);
            } else {
                apVar.c.setText(axVar.c.d());
                j.a(axVar.c, apVar.f685a, this.e, 3);
            }
        } else if (axVar.o == 6) {
            if (axVar.d == null) {
                axVar.d = new n(axVar.f3000a);
                apVar.c.setText(axVar.f3000a);
                j.a(apVar.f685a, false, false, -1);
            } else {
                apVar.c.setText(axVar.d.a());
                j.a(axVar.d, apVar.f685a, this.e, 3);
            }
        } else if (axVar.o == 0) {
            if (axVar.b == null) {
                axVar.b = new bf(axVar.f3000a);
                apVar.c.setText(axVar.f3000a);
            } else {
                apVar.c.setText(axVar.b.h());
            }
            j.a(axVar.b, apVar.f685a, this.e, 3);
        } else if (axVar.o == 7 && axVar.e != null) {
            TextView textView = apVar.c;
            c cVar = axVar.e;
            textView.setText(c.g());
            j.a(axVar.e, apVar.f685a, this.e, 3);
        }
        if (axVar.o == 0) {
            apVar.l.setVisibility(0);
            apVar.l.setBackgroundResource(R.drawable.bglistitem_selector_session_avatar);
            apVar.k.setBackgroundResource(R.drawable.bglistitem_selector_session_select);
            apVar.l.setOnClickListener(new al(this, axVar));
        } else if (axVar.o == 2) {
            apVar.l.setVisibility(0);
            apVar.l.setBackgroundResource(R.drawable.bglistitem_selector_session_avatar);
            apVar.k.setBackgroundResource(R.drawable.bglistitem_selector_session_select);
            apVar.l.setOnClickListener(new am(this, axVar));
        } else if (axVar.o == 6) {
            apVar.l.setVisibility(0);
            apVar.l.setBackgroundResource(R.drawable.bglistitem_selector_session_avatar);
            apVar.k.setBackgroundResource(R.drawable.bglistitem_selector_session_select);
            apVar.l.setOnClickListener(new an(this, axVar));
        } else {
            apVar.l.setVisibility(8);
            apVar.k.setBackgroundResource(R.drawable.bglistitem_selector_session_select);
        }
        apVar.l.setClickable(!this.e.f());
        apVar.l.setEnabled(!this.e.f());
        if (axVar.k == null) {
            axVar.k = new Message(PoiTypeDef.All);
            axVar.k.receive = true;
            axVar.k.contentType = 0;
            axVar.k.setContent(PoiTypeDef.All);
            axVar.k.timestamp = null;
        }
        if (axVar.k.timestamp != null) {
            apVar.g.setText(android.support.v4.b.a.a(axVar.k.timestamp, false));
        } else {
            apVar.g.setText(PoiTypeDef.All);
        }
        apVar.d.setVisibility(8);
        apVar.e.setVisibility(8);
        apVar.f.setVisibility(8);
        apVar.b.setVisibility(8);
        if (axVar.a() || axVar.f > 0) {
            if (axVar.a()) {
                apVar.b.setVisibility(0);
            } else {
                apVar.e.setVisibility(0);
                apVar.e.setBackgroundResource(R.drawable.bg_chat_newmsg);
                apVar.e.setText(String.valueOf(axVar.f) + "条未读消息");
            }
        } else if (!axVar.k.receive) {
            switch (axVar.k.status) {
                case 1:
                    apVar.f.setVisibility(0);
                    apVar.f.setBackgroundResource(R.drawable.bg_msgbox_state_sending);
                    apVar.f.setImageResource(R.anim.message_sending);
                    this.d.P().post(new ao((AnimationDrawable) apVar.f.getDrawable()));
                    break;
                case 2:
                    if (axVar.o != 2 && axVar.o != 6) {
                        apVar.d.setVisibility(0);
                        apVar.d.setBackgroundResource(R.drawable.bg_msgbox_state_send);
                        apVar.d.setText((int) R.string.msg_status_sended);
                        break;
                    } else {
                        apVar.d.setVisibility(8);
                        break;
                    }
                case 3:
                    apVar.d.setVisibility(0);
                    apVar.d.setBackgroundResource(R.drawable.bg_msgbox_state_failure);
                    apVar.d.setText((int) R.string.msg_status_failed);
                    break;
                case 4:
                case 5:
                case 9:
                default:
                    apVar.d.setVisibility(8);
                    break;
                case 6:
                    if (axVar.o != 2 && axVar.o != 6) {
                        apVar.d.setVisibility(0);
                        apVar.d.setBackgroundResource(R.drawable.bg_msgbox_state_read);
                        apVar.d.setText((int) R.string.msg_status_readed);
                        break;
                    } else {
                        apVar.d.setVisibility(8);
                        break;
                    }
                    break;
                case 7:
                    apVar.d.setVisibility(0);
                    apVar.d.setBackgroundResource(R.drawable.bg_msgbox_state_processing);
                    apVar.d.setText((int) R.string.msg_status_uploading);
                    break;
                case 8:
                    apVar.d.setVisibility(0);
                    apVar.d.setBackgroundResource(R.drawable.bg_msgbox_state_processing);
                    apVar.d.setText((int) R.string.msg_status_positioning);
                    break;
                case 10:
                    apVar.d.setVisibility(0);
                    apVar.d.setBackgroundResource(R.drawable.bg_msgbox_state_cloud);
                    apVar.d.setText((int) R.string.msg_status_cloud);
                    break;
            }
        } else if (axVar.k.status == 10) {
            apVar.d.setVisibility(0);
            apVar.d.setBackgroundResource(R.drawable.bg_msgbox_state_cloud);
            apVar.d.setText((int) R.string.msg_status_cloud);
        }
        Message message = axVar.k;
        if (message != null) {
            StringBuilder sb = new StringBuilder();
            if (axVar.o == 5) {
                str = PoiTypeDef.All;
            } else if (axVar.o == 8) {
                str = PoiTypeDef.All;
            } else if (axVar.o != 2 && axVar.o != 6) {
                str = message.getDiatance() < 0.0f ? PoiTypeDef.All : "[" + android.support.v4.b.a.a(message.getDiatance() / 1000.0f) + "km] ";
            } else if (!message.receive) {
                str = PoiTypeDef.All;
            } else if (android.support.v4.b.a.a((CharSequence) message.remoteId)) {
                str = PoiTypeDef.All;
            } else if (message.getDiatance() >= 0.0f) {
                str = "[" + android.support.v4.b.a.a(message.getDiatance() / 1000.0f) + "km] " + (message.remoteUser == null ? message.remoteId : message.remoteUser.h()) + ": ";
            } else {
                str = message.remoteUser == null ? message.remoteId : String.valueOf(message.remoteUser.h()) + ": ";
            }
            sb.append(str);
            if (message.contentType == 1) {
                sb.append("图片消息");
            } else if (message.contentType == 2) {
                sb.append("位置信息");
            } else if (message.contentType == 4) {
                sb.append("语音消息");
            } else if (message.contentType == 7) {
                sb.append(message.getContent());
            } else {
                sb.append(message.getContent());
            }
            apVar.h.setText(String.valueOf(sb.toString()) + " ");
        } else {
            apVar.h.setText(PoiTypeDef.All);
        }
        apVar.i.setOnClickListener(new ak(this, i));
        apVar.i.setOnLongClickListener(new aj(this, i));
        return view;
    }
}
