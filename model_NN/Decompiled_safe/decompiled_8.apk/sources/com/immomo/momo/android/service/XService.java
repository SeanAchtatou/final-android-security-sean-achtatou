package com.immomo.momo.android.service;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.IBinder;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.a.a.a;
import com.immomo.a.a.b;
import com.immomo.a.a.d;
import com.immomo.momo.MomoApplication;
import com.immomo.momo.g;
import com.immomo.momo.protocol.imjson.a.c;
import com.immomo.momo.protocol.imjson.a.e;
import com.immomo.momo.protocol.imjson.a.f;
import com.immomo.momo.protocol.imjson.a.h;
import com.immomo.momo.protocol.imjson.a.i;
import com.immomo.momo.protocol.imjson.a.j;
import com.immomo.momo.protocol.imjson.a.q;
import com.immomo.momo.protocol.imjson.a.r;
import com.immomo.momo.protocol.imjson.a.s;
import com.immomo.momo.protocol.imjson.k;
import com.immomo.momo.protocol.imjson.l;
import com.immomo.momo.protocol.imjson.n;
import com.immomo.momo.protocol.imjson.o;
import com.immomo.momo.protocol.imjson.p;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.util.ak;
import com.immomo.momo.util.jni.Codec;
import com.immomo.momo.util.m;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import mm.purchasesdk.PurchaseCode;

public class XService extends Service {
    public static final String c = (String.valueOf(g.h()) + ".action.xmpp.disconnect");
    public static final String d = (String.valueOf(g.h()) + ".action.xmpp.xmppinfo.clear");
    public static final String e = (String.valueOf(g.h()) + ".action.xmpp.xmppinfo.login");
    public static final String f = (String.valueOf(g.h()) + ".action.xmpp.xmppinfo.ping");
    private static String g = (String.valueOf(g.h()) + ".action.stopxmpp");

    /* renamed from: a  reason: collision with root package name */
    public p f2602a = null;
    public a b;
    /* access modifiers changed from: private */
    public com.immomo.momo.protocol.imjson.g h = null;
    private r i = null;
    private BroadcastReceiver j = null;
    /* access modifiers changed from: private */
    public boolean k = false;
    /* access modifiers changed from: private */
    public String l = PoiTypeDef.All;
    /* access modifiers changed from: private */
    public m m = new m("Xservice");
    /* access modifiers changed from: private */
    public Lock n = new ReentrantLock();

    static /* synthetic */ void e(XService xService) {
        if (xService.f2602a.b()) {
            xService.f2602a.g();
        } else {
            xService.f2602a.c();
        }
        xService.h.c();
        l.c = true;
        l.f2926a = true;
    }

    static /* synthetic */ MomoApplication h(XService xService) {
        return (MomoApplication) xService.getApplication();
    }

    public final o a() {
        ak a2 = ak.a(getApplicationContext(), String.valueOf(((MomoApplication) getApplication()).a().h) + "_appconfig");
        String b2 = a2.b("appconfigs_0", PoiTypeDef.All);
        String b3 = a2.b("appconfigs_1", PoiTypeDef.All);
        if (!android.support.v4.b.a.a((CharSequence) b2) && !android.support.v4.b.a.a((CharSequence) b3)) {
            try {
                return new o(Codec.b(b2), Integer.parseInt(Codec.b(b3)));
            } catch (Exception e2) {
            }
        }
        return null;
    }

    public final void a(String str, int i2) {
        b b2 = this.b.b();
        b2.a(str);
        b2.a(i2);
    }

    public final void a(boolean z) {
        l.c = false;
        this.f2602a.e();
        this.b.k();
        if (z) {
            this.h.d();
        }
    }

    public final void b() {
        stopSelf();
    }

    public final void b(String str, int i2) {
        if (((MomoApplication) getApplication()).a() != null) {
            SharedPreferences.Editor edit = ak.a(getApplicationContext(), String.valueOf(((MomoApplication) getApplication()).a().h) + "_appconfig").a().edit();
            if (!str.equals(com.immomo.momo.a.f669a) || i2 != com.immomo.momo.a.b) {
                edit.putString("appconfigs_0", Codec.c(str));
                edit.putString("appconfigs_1", Codec.c(String.valueOf(i2)));
            } else {
                edit.remove("appconfigs_0");
                edit.remove("appconfigs_1");
            }
            edit.commit();
        }
    }

    public final void c() {
        bf a2 = ((MomoApplication) getApplication()).a();
        if (a2 == null || !((MomoApplication) getApplication()).j() || !g.y() || this.b.m() || this.k) {
            this.m.b((Object) ("-----------ignoreLogin, " + ("loginSuccess=" + this.b.m() + ", logining=" + this.k + ", username=" + (a2 == null ? "null" : a2.h))));
            if (this.b.m() && this.f2602a.b()) {
                this.f2602a.g();
                return;
            }
            return;
        }
        this.k = true;
        new Thread(new t(this)).start();
    }

    public final p d() {
        return this.f2602a;
    }

    public final void e() {
        this.f2602a.d();
    }

    public final String f() {
        return this.b.b().a();
    }

    public final int g() {
        return this.b.b().b();
    }

    public IBinder onBind(Intent intent) {
        this.m.a((Object) "onbind");
        return new s(this, (byte) 0);
    }

    public void onCreate() {
        super.onCreate();
        if (((MomoApplication) getApplication()).a() == null || !((MomoApplication) getApplication()).j()) {
            stopSelf();
            return;
        }
        new Thread(new p()).start();
        l.h = true;
        a.a(new com.immomo.momo.protocol.imjson.util.b());
        b bVar = new b();
        bVar.f("android");
        bVar.a(com.immomo.momo.a.f669a);
        bVar.a(com.immomo.momo.a.b);
        bVar.d(15);
        bVar.c(g.z());
        bVar.b(((MomoApplication) getApplication()).a().h);
        bVar.c(((MomoApplication) getApplication()).a().W);
        bVar.d(g.I());
        this.b = new d(bVar);
        this.b.a(new n());
        this.b.a(new k(this.b));
        a aVar = this.b;
        com.immomo.momo.protocol.imjson.a.l lVar = new com.immomo.momo.protocol.imjson.a.l();
        a aVar2 = this.b;
        com.immomo.momo.protocol.imjson.a.k kVar = new com.immomo.momo.protocol.imjson.a.k();
        this.b.b("msg", lVar);
        this.b.b("gmsg", lVar);
        this.b.b("dmsg", lVar);
        this.b.b("msgst", kVar);
        a aVar3 = this.b;
        q qVar = new q();
        this.b.b("set", qVar);
        h hVar = new h();
        qVar.a("gnotice", hVar);
        qVar.a("gnotification", hVar);
        qVar.a("gradar-notice", hVar);
        qVar.a("gevent", new e());
        qVar.a("devent", new com.immomo.momo.protocol.imjson.a.b());
        com.immomo.momo.protocol.imjson.a.p pVar = new com.immomo.momo.protocol.imjson.a.p();
        qVar.a("follow", pVar);
        qVar.a("unfollow", pVar);
        a aVar4 = this.b;
        qVar.a("distance", new j());
        qVar.a("profile", new com.immomo.momo.protocol.imjson.a.o());
        qVar.a("feed-notice", new c(this.b));
        qVar.a("event-notice", new com.immomo.momo.protocol.imjson.c(this.b));
        qVar.a("tieba-notice", new r(this.b));
        com.immomo.momo.protocol.imjson.a.a aVar5 = new com.immomo.momo.protocol.imjson.a.a();
        qVar.a("contacts-notice", aVar5);
        qVar.a("momocontacts-notice", aVar5);
        qVar.a("wbcontacts-notice", aVar5);
        qVar.a("qqwbcontacts-notice", aVar5);
        qVar.a("rrcontacts-notice", aVar5);
        qVar.a("appcontacts-notice", aVar5);
        qVar.a("friendfeedcount", aVar5);
        qVar.a("fradar-notice", new com.immomo.momo.protocol.imjson.a.d());
        qVar.a("tieba-report", new s(this.b));
        qVar.a("gzone", new i());
        a aVar6 = this.b;
        f fVar = new f();
        this.b.b("get", fVar);
        fVar.a("net_status", new com.immomo.momo.protocol.imjson.a.g());
        this.f2602a = new p(this.b);
        this.h = new com.immomo.momo.protocol.imjson.g(this);
        this.b.a(this.h);
        this.f2602a.f();
        if (((MomoApplication) getApplication()).a() != null && ((MomoApplication) getApplication()).j()) {
            ak a2 = ak.a(getApplicationContext(), String.valueOf(((MomoApplication) getApplication()).a().h) + "_appconfig");
            String b2 = a2.b("appconfigs_0", PoiTypeDef.All);
            String b3 = a2.b("appconfigs_1", PoiTypeDef.All);
            String str = PoiTypeDef.All;
            int i2 = -1;
            if (!android.support.v4.b.a.a((CharSequence) b2) && !android.support.v4.b.a.a((CharSequence) b3)) {
                try {
                    str = Codec.b(b2);
                    i2 = Integer.parseInt(Codec.b(b3));
                } catch (Exception e2) {
                }
            }
            if (android.support.v4.b.a.a((CharSequence) str) || i2 < 0) {
                str = com.immomo.momo.a.f669a;
                i2 = com.immomo.momo.a.b;
            }
            b b4 = this.b.b();
            b4.a(str);
            b4.a(i2);
            l.d = b4.a();
            l.e = b4.b();
            this.l = android.support.v4.b.a.n(String.valueOf(g.h()) + g.I() + g.B() + "android:" + g.z()).substring(0, 3);
            b4.e(this.l);
        }
        this.i = new r(this, this);
        this.j = new q(this);
        IntentFilter intentFilter = new IntentFilter(g);
        intentFilter.setPriority(PurchaseCode.QUERY_FROZEN);
        registerReceiver(this.j, intentFilter);
    }

    public void onDestroy() {
        super.onDestroy();
        this.m.a((Object) "onDestroy....");
        if (this.f2602a != null) {
            this.f2602a.d();
        }
        if (this.h != null) {
            this.h.b();
        }
        if (this.b != null) {
            this.b.b(this.h);
            this.b.k();
        }
        if (this.i != null) {
            unregisterReceiver(this.i);
            this.i = null;
        }
        if (this.j != null) {
            unregisterReceiver(this.j);
            this.j = null;
        }
        l.c = false;
        l.f2926a = false;
        l.b = "通讯服务被关闭";
        l.h = false;
    }

    public int onStartCommand(Intent intent, int i2, int i3) {
        if (((MomoApplication) getApplication()).a() == null || !((MomoApplication) getApplication()).j() || android.support.v4.b.a.a((CharSequence) ((MomoApplication) getApplication()).a().h) || android.support.v4.b.a.a((CharSequence) ((MomoApplication) getApplication()).a().W)) {
            stopSelf();
            return super.onStartCommand(intent, i2, i3);
        }
        this.b.b().b(((MomoApplication) getApplication()).a().h);
        this.b.b().c(((MomoApplication) getApplication()).a().W);
        this.h.g();
        c();
        return super.onStartCommand(intent, i2, i3);
    }
}
