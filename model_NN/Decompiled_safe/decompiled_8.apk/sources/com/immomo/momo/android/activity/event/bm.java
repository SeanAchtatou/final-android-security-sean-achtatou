package com.immomo.momo.android.activity.event;

import android.location.Location;
import com.immomo.momo.R;
import com.immomo.momo.android.b.p;
import com.immomo.momo.android.b.z;
import com.immomo.momo.util.ao;

final class bm extends p {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ bd f1382a;

    bm(bd bdVar) {
        this.f1382a = bdVar;
    }

    public final void a(Location location, int i, int i2, int i3) {
        if (this.b) {
            if (z.a(location)) {
                this.f1382a.M.S = location.getLatitude();
                this.f1382a.M.T = location.getLongitude();
                this.f1382a.M.aH = i;
                this.f1382a.M.aI = i3;
                this.f1382a.M.a(System.currentTimeMillis());
                this.f1382a.X.a(this.f1382a.M);
                this.f1382a.U.post(new bn(this));
                return;
            }
            this.f1382a.P();
            if (i2 == 104) {
                ao.g(R.string.errormsg_network_unfind);
            } else if (i2 == 105) {
                this.f1382a.U.post(new bp(this.f1382a));
            } else {
                ao.g(R.string.errormsg_location_nearby_failed);
            }
        }
    }
}
