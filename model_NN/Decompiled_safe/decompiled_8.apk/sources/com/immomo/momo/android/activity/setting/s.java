package com.immomo.momo.android.activity.setting;

import android.content.Intent;
import android.view.View;
import com.immomo.momo.android.activity.AboutTabsActivity;

final class s implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ FeedBackListActivity f2142a;

    s(FeedBackListActivity feedBackListActivity) {
        this.f2142a = feedBackListActivity;
    }

    public final void onClick(View view) {
        Intent intent = new Intent(this.f2142a, AboutTabsActivity.class);
        intent.putExtra("showindex", 1);
        this.f2142a.startActivity(intent);
    }
}
