package com.immomo.momo.android.activity;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import com.immomo.momo.R;
import com.immomo.momo.android.view.HeaderLayout;
import com.immomo.momo.service.bean.bf;

public class OldAddFriendActivity extends lh {
    /* access modifiers changed from: private */
    public bf O;
    private Button P;
    /* access modifiers changed from: private */
    public EditText Q;
    /* access modifiers changed from: private */
    public AsyncTask R = null;

    /* access modifiers changed from: protected */
    public final int B() {
        return R.layout.activity_oldaddfriend;
    }

    public final void C() {
        this.P = (Button) c((int) R.id.friend_btn_find);
        this.Q = (EditText) c((int) R.id.friend_edit_momoid);
        this.Q.requestFocus();
    }

    public final void a(Context context, HeaderLayout headerLayout) {
        headerLayout.setTitleText((int) R.string.addtabs_friend);
    }

    /* access modifiers changed from: protected */
    public final void g(Bundle bundle) {
        super.a(bundle);
        this.P.setOnClickListener(new gf(this));
        aj();
    }

    public final void m() {
        super.m();
    }

    public final void p() {
        super.p();
        if (this.R != null && !this.R.isCancelled()) {
            this.R.cancel(true);
        }
    }
}
