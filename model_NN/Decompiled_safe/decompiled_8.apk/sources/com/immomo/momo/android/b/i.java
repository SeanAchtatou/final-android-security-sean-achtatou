package com.immomo.momo.android.b;

import android.location.Location;
import com.amap.mapapi.location.LocationManagerProxy;
import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;

final class i implements BDLocationListener {

    /* renamed from: a  reason: collision with root package name */
    private p f2327a;
    private /* synthetic */ h b;

    public i(h hVar, p pVar) {
        this.b = hVar;
        this.f2327a = pVar;
    }

    public final void onReceiveLocation(BDLocation bDLocation) {
        if (bDLocation != null) {
            this.b.b.a((Object) ("baidu received a location, errcode=: " + bDLocation.getLocType()));
        }
        if (bDLocation == null) {
            return;
        }
        if (bDLocation.getLocType() == 61 || bDLocation.getLocType() == 161) {
            this.b.b.a((Object) ("baidu gps&both location succeed: " + bDLocation.getLatitude() + ", " + bDLocation.getLongitude() + ", " + bDLocation.getRadius() + ", " + bDLocation.getLocType()));
            Location location = new Location(LocationManagerProxy.NETWORK_PROVIDER);
            location.setLatitude(bDLocation.getLatitude());
            location.setLongitude(bDLocation.getLongitude());
            location.setAccuracy(bDLocation.getRadius());
            if (this.f2327a != null) {
                this.f2327a.a(location, 1, 100, 201);
            }
        }
    }

    public final void onReceivePoi(BDLocation bDLocation) {
    }
}
