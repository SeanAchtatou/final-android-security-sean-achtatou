package com.immomo.momo.android.activity.group.foundgroup;

import android.content.Intent;
import com.immomo.momo.android.broadcast.u;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.g;
import com.immomo.momo.protocol.a.n;
import com.immomo.momo.service.bean.a.a;
import com.immomo.momo.service.bean.ay;
import com.immomo.momo.service.y;

final class m extends d {

    /* renamed from: a  reason: collision with root package name */
    private v f1671a;
    private a c = new a();
    /* access modifiers changed from: private */
    public /* synthetic */ l d;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public m(l lVar) {
        super(lVar.f1670a);
        this.d = lVar;
        this.f1671a = new v(lVar.f1670a, "群组创建中请稍等...");
        this.f1671a.setOnCancelListener(new n(this));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.service.y.a(com.immomo.momo.service.bean.a.a, boolean):void
     arg types: [com.immomo.momo.service.bean.a.a, int]
     candidates:
      com.immomo.momo.service.y.a(com.immomo.momo.service.bean.a.a, java.lang.String):void
      com.immomo.momo.service.y.a(java.lang.String, int):void
      com.immomo.momo.service.y.a(java.lang.String, java.lang.String):void
      com.immomo.momo.service.y.a(java.lang.String, java.util.List):void
      com.immomo.momo.service.y.a(java.lang.String, boolean):void
      com.immomo.momo.service.y.a(java.lang.String, java.lang.String[]):void
      com.immomo.momo.service.y.a(java.util.List, java.lang.String):void
      com.immomo.momo.service.y.a(com.immomo.momo.service.bean.a.a, boolean):void */
    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        s[] sVarArr = (s[]) objArr;
        ay ayVar = new ay();
        ayVar.f = sVarArr[0].d;
        ayVar.d = sVarArr[0].g;
        ayVar.f3001a = sVarArr[0].f;
        String a2 = n.a().a(this.d.d.c, this.d.d.e, ayVar, this.c, this.d.d.h, this.d.d.i, this.d.d.j, this.d.c);
        this.b.a(this.c);
        if (!android.support.v4.b.a.a((CharSequence) a2)) {
            y yVar = new y();
            yVar.a(this.c, false);
            yVar.a(g.q().h, this.c.b, 1);
            Intent intent = new Intent(u.f2364a);
            intent.putExtra("gid", this.c.b);
            this.d.f1670a.sendBroadcast(intent);
        }
        return a2;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.f1671a.show();
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        String str = (String) obj;
        if (!android.support.v4.b.a.a((CharSequence) str)) {
            com.immomo.momo.android.view.a.n b = com.immomo.momo.android.view.a.n.b(this.d.f1670a, str, new o(this));
            b.setCancelable(false);
            b.show();
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.f1671a.dismiss();
    }
}
