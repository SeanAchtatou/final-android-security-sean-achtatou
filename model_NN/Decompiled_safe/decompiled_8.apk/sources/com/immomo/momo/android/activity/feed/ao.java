package com.immomo.momo.android.activity.feed;

import android.content.Context;
import android.widget.ListAdapter;
import com.immomo.momo.android.a.cs;
import com.immomo.momo.android.c.d;
import com.immomo.momo.protocol.a.k;
import com.immomo.momo.service.bean.ab;
import java.util.ArrayList;
import java.util.List;

final class ao extends d {

    /* renamed from: a  reason: collision with root package name */
    private List f1446a = new ArrayList();
    private /* synthetic */ OtherFeedListActivity c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ao(OtherFeedListActivity otherFeedListActivity, Context context) {
        super(context);
        this.c = otherFeedListActivity;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        this.c.v = this.c.u;
        this.c.u = 0;
        boolean a2 = k.a().a(this.f1446a, this.c.u, this.c.r);
        this.c.m.g(this.c.r);
        this.c.m.a(this.f1446a);
        this.c.k.clear();
        for (ab abVar : this.f1446a) {
            abVar.b = this.c.s;
            this.c.k.add(abVar);
        }
        if (this.f1446a != null && this.f1446a.size() > 0) {
            OtherFeedListActivity otherFeedListActivity = this.c;
            otherFeedListActivity.u = otherFeedListActivity.u + this.f1446a.size();
            this.b.a((Object) ("downloadOtherFeedList success, lastindex:" + this.c.u));
        }
        return Boolean.valueOf(a2);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        if (this.c.o != null && !this.c.o.isCancelled()) {
            this.c.o.cancel(true);
        }
        this.c.i.g();
        if (this.c.n != null) {
            this.c.n.cancel(true);
        }
        this.c.n = this;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        this.c.j = this.f1446a;
        this.c.p = new cs(this.c, this.c.j, this.c.h);
        this.c.h.setAdapter((ListAdapter) this.c.p);
        if (!((Boolean) obj).booleanValue()) {
            this.c.h.k();
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.c.w();
        this.c.n = (ao) null;
    }
}
