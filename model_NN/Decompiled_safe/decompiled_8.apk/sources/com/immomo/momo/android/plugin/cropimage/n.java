package com.immomo.momo.android.plugin.cropimage;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.widget.ImageView;

abstract class n extends ImageView {

    /* renamed from: a  reason: collision with root package name */
    private Matrix f2593a = new Matrix();
    private Matrix b = new Matrix();
    protected final t c = new t(null);
    protected Handler d = new Handler();
    private final Matrix e = new Matrix();
    private final float[] f = new float[9];
    private int g = -1;
    private int h = -1;
    private float i;
    private Runnable j = null;

    public n(Context context) {
        super(context);
        setScaleType(ImageView.ScaleType.MATRIX);
    }

    public n(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        setScaleType(ImageView.ScaleType.MATRIX);
    }

    private Matrix a() {
        this.e.set(this.f2593a);
        this.e.postConcat(this.b);
        return this.e;
    }

    private void a(Bitmap bitmap, int i2) {
        super.setImageBitmap(bitmap);
        Drawable drawable = getDrawable();
        if (drawable != null) {
            drawable.setDither(true);
        }
        this.c.b();
        this.c.a(bitmap);
        this.c.a(i2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [float, int]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    private void a(t tVar, Matrix matrix) {
        float width = (float) getWidth();
        float height = (float) getHeight();
        float e2 = (float) tVar.e();
        float d2 = (float) tVar.d();
        matrix.reset();
        float min = Math.min(Math.min(width / e2, 3.0f), Math.min(height / d2, 3.0f));
        matrix.postConcat(tVar.c());
        matrix.postScale(min, min);
        matrix.postTranslate((width - (e2 * min)) / 2.0f, (height - (d2 * min)) / 2.0f);
    }

    /* access modifiers changed from: protected */
    public void a(float f2, float f3) {
        this.b.postTranslate(f2, f3);
    }

    /* access modifiers changed from: protected */
    public void a(float f2, float f3, float f4) {
        if (f2 > this.i) {
            f2 = this.i;
        }
        float f5 = f2 / f();
        this.b.postScale(f5, f5, f3, f4);
        setImageMatrix(a());
        e();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.android.plugin.cropimage.n.a(com.immomo.momo.android.plugin.cropimage.t, boolean):void
     arg types: [com.immomo.momo.android.plugin.cropimage.t, int]
     candidates:
      com.immomo.momo.android.plugin.cropimage.n.a(android.graphics.Bitmap, int):void
      com.immomo.momo.android.plugin.cropimage.n.a(com.immomo.momo.android.plugin.cropimage.t, android.graphics.Matrix):void
      com.immomo.momo.android.plugin.cropimage.n.a(float, float):void
      com.immomo.momo.android.plugin.cropimage.n.a(com.immomo.momo.android.plugin.cropimage.t, boolean):void */
    public final void a(Bitmap bitmap) {
        a(new t(bitmap), true);
    }

    public final void a(t tVar, boolean z) {
        if (getWidth() <= 0) {
            this.j = new o(this, tVar, z);
            return;
        }
        if (tVar.b() != null) {
            a(tVar, this.f2593a);
            a(tVar.b(), tVar.a());
        } else {
            this.f2593a.reset();
            setImageBitmap(null);
        }
        if (z) {
            this.b.reset();
        }
        setImageMatrix(a());
        this.i = this.c.b() == null ? 1.0f : Math.max(((float) this.c.e()) / ((float) this.g), ((float) this.c.d()) / ((float) this.h)) * 4.0f;
    }

    /* access modifiers changed from: protected */
    public final void b(float f2, float f3) {
        a(f2, f3);
        setImageMatrix(a());
    }

    /* access modifiers changed from: protected */
    public final void b(float f2, float f3, float f4) {
        float f5 = f();
        this.d.post(new p(this, System.currentTimeMillis(), f5, (f2 - f()) / 300.0f, f3, f4));
    }

    public final void d() {
        a(null);
    }

    /* access modifiers changed from: protected */
    public final void e() {
        float f2 = 0.0f;
        if (this.c.b() != null) {
            Matrix a2 = a();
            RectF rectF = new RectF(0.0f, 0.0f, (float) this.c.b().getWidth(), (float) this.c.b().getHeight());
            a2.mapRect(rectF);
            float height = rectF.height();
            float width = rectF.width();
            int height2 = getHeight();
            float height3 = height < ((float) height2) ? ((((float) height2) - height) / 2.0f) - rectF.top : rectF.top > 0.0f ? -rectF.top : rectF.bottom < ((float) height2) ? ((float) getHeight()) - rectF.bottom : 0.0f;
            int width2 = getWidth();
            if (width < ((float) width2)) {
                f2 = ((((float) width2) - width) / 2.0f) - rectF.left;
            } else if (rectF.left > 0.0f) {
                f2 = -rectF.left;
            } else if (rectF.right < ((float) width2)) {
                f2 = ((float) width2) - rectF.right;
            }
            a(f2, height3);
            setImageMatrix(a());
        }
    }

    /* access modifiers changed from: protected */
    public final float f() {
        this.b.getValues(this.f);
        return this.f[0];
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 == 4 && keyEvent.getRepeatCount() == 0) {
            return true;
        }
        return super.onKeyDown(i2, keyEvent);
    }

    public boolean onKeyUp(int i2, KeyEvent keyEvent) {
        if (i2 != 4 || f() <= 1.0f) {
            return super.onKeyUp(i2, keyEvent);
        }
        a(1.0f, ((float) getWidth()) / 2.0f, ((float) getHeight()) / 2.0f);
        return true;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        super.onLayout(z, i2, i3, i4, i5);
        this.g = i4 - i2;
        this.h = i5 - i3;
        Runnable runnable = this.j;
        if (runnable != null) {
            this.j = null;
            runnable.run();
        }
        if (this.c.b() != null) {
            a(this.c, this.f2593a);
            setImageMatrix(a());
        }
    }

    public void setImageBitmap(Bitmap bitmap) {
        a(bitmap, 0);
    }
}
