package com.immomo.momo.android.view.photoview;

import android.support.v4.b.a;
import android.widget.ImageView;

final class c implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final float f2819a;
    private final float b;
    private final float c;
    private final float d;
    private /* synthetic */ a e;

    public c(a aVar, float f, float f2, float f3, float f4) {
        this.e = aVar;
        this.c = f2;
        this.f2819a = f3;
        this.b = f4;
        if (f < f2) {
            this.d = 1.07f;
        } else {
            this.d = 0.93f;
        }
    }

    public final void run() {
        ImageView c2 = this.e.c();
        if (c2 != null) {
            this.e.m.postScale(this.d, this.d, this.f2819a, this.b);
            this.e.k();
            float g = this.e.g();
            if ((this.d <= 1.0f || g >= this.c) && (this.d >= 1.0f || this.c >= g)) {
                float f = this.c / g;
                this.e.m.postScale(f, f, this.f2819a, this.b);
                this.e.k();
                return;
            }
            a.a(c2, this);
        }
    }
}
