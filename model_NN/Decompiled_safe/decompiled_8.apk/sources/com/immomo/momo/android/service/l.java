package com.immomo.momo.android.service;

import android.location.Location;
import com.immomo.momo.android.b.p;
import com.immomo.momo.android.b.z;
import com.immomo.momo.g;

final class l extends p {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ k f2612a;

    l(k kVar) {
        this.f2612a = kVar;
    }

    public final void a(Location location, int i, int i2, int i3) {
        if (z.a(location) && g.q() != null && this.f2612a.f2611a.c != null) {
            g.q().S = location.getLatitude();
            g.q().T = location.getLongitude();
            g.q().U = (double) location.getAccuracy();
            g.q().aH = i;
            g.q().aI = i3;
            new Thread(new m(this)).start();
        }
    }
}
