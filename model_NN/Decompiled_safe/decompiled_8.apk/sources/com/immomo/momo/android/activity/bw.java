package com.immomo.momo.android.activity;

import android.view.View;
import com.immomo.momo.android.view.a.n;
import java.util.List;

final class bw implements View.OnClickListener {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public /* synthetic */ ContactNoticeListActivity f1055a;

    bw(ContactNoticeListActivity contactNoticeListActivity) {
        this.f1055a = contactNoticeListActivity;
    }

    public final void onClick(View view) {
        List d = this.f1055a.p.d();
        if (!d.isEmpty()) {
            n.a(this.f1055a, "确认删除所选的邀请吗？", new bx(this, d)).show();
        }
    }
}
