package com.immomo.momo.android.activity.tieba;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.a.iw;
import com.immomo.momo.android.activity.ah;
import com.immomo.momo.android.view.LoadingButton;
import com.immomo.momo.android.view.MomoRefreshExpandableListView;
import com.immomo.momo.android.view.bl;
import com.immomo.momo.android.view.bu;
import com.immomo.momo.android.view.cv;
import com.immomo.momo.g;
import com.immomo.momo.service.ao;
import com.immomo.momo.service.bean.bc;
import com.immomo.momo.service.bean.c.d;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class TiebaMemberListActivity extends ah implements View.OnClickListener, bl, bu, cv {
    Set h = new HashSet();
    /* access modifiers changed from: private */
    public int i = 30;
    /* access modifiers changed from: private */
    public String j = null;
    private d k = null;
    /* access modifiers changed from: private */
    public List l = null;
    /* access modifiers changed from: private */
    public iw m = null;
    private MomoRefreshExpandableListView n = null;
    /* access modifiers changed from: private */
    public LoadingButton o = null;

    static /* synthetic */ void d(TiebaMemberListActivity tiebaMemberListActivity) {
        if (tiebaMemberListActivity.l.size() == 0) {
            tiebaMemberListActivity.l.add(new bc());
            tiebaMemberListActivity.l.add(new bc());
            ((bc) tiebaMemberListActivity.l.get(0)).b = "吧主";
            ((bc) tiebaMemberListActivity.l.get(1)).b = "附近成员";
        }
    }

    /* access modifiers changed from: private */
    public void w() {
        this.n.i();
        this.o.e();
    }

    public final void a(Intent intent, int i2, Bundle bundle, String str) {
        if (intent.getComponent() != null && com.immomo.momo.android.activity.d.g(intent.getComponent().getClassName())) {
            intent.putExtra("afromname", this.k != null ? this.k.b : PoiTypeDef.All);
        }
        super.a(intent, i2, bundle, str);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, com.immomo.momo.android.view.MomoRefreshExpandableListView, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        super.a(bundle);
        setContentView((int) R.layout.activity_tieba_members);
        this.j = getIntent().getStringExtra("tiebaid");
        getIntent().getIntExtra("type", 0);
        this.n = (MomoRefreshExpandableListView) findViewById(R.id.listview);
        this.n.setEnableLoadMoreFoolter(true);
        this.n.setFastScrollEnabled(false);
        this.n.setMMHeaderView(g.o().inflate((int) R.layout.listitem_tiebamember_groupsite, (ViewGroup) this.n, false));
        this.n.setGroupIndicator(null);
        this.n.setTimeEnable(false);
        this.o = this.n.getFooterViewButton();
        this.o.a("更多成员");
        m().setTitleText("附近成员");
        d();
        this.n.setOnCancelListener(this);
        this.n.setOnPullToRefreshListener(this);
        this.o.setOnProcessListener(this);
        this.n.setOnChildClickListener(new eq(this));
    }

    public final void b_() {
        this.h.clear();
        this.n.setLoadingViewText(R.string.pull_to_refresh_refreshing_label);
        b(new er(this, this, true));
    }

    /* access modifiers changed from: protected */
    public final void d() {
        super.d();
        this.l = new ArrayList();
        this.k = new ao().a(this.j);
        this.m = new iw(this, this.l, this.n);
        this.n.setAdapter(this.m);
        this.m.a();
        this.n.h();
        this.o.setVisibility(8);
    }

    /* access modifiers changed from: protected */
    public final void e() {
        super.e();
    }

    public void onClick(View view) {
    }

    public final void u() {
        this.n.setLoadingViewText(R.string.pull_to_refresh_refreshing_label);
        b(new er(this, this, false));
    }

    public final void v() {
        w();
    }
}
