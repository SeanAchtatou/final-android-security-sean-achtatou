package com.immomo.momo.android.map;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import com.immomo.momo.service.bean.ay;

final class bi implements AdapterView.OnItemClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ SelectSiteGoogleActivity f2488a;

    bi(SelectSiteGoogleActivity selectSiteGoogleActivity) {
        this.f2488a = selectSiteGoogleActivity;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        ay ayVar = (ay) this.f2488a.q.getItem(i);
        Intent intent = new Intent();
        intent.putExtra("siteid", ayVar.f3001a);
        intent.putExtra("sitename", ayVar.f);
        intent.putExtra("sitetype", ayVar.d);
        this.f2488a.setResult(-1, intent);
        this.f2488a.finish();
    }
}
