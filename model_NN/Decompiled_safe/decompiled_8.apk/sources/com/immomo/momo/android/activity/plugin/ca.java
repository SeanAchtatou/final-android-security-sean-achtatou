package com.immomo.momo.android.activity.plugin;

import android.content.Context;
import com.immomo.momo.R;
import com.immomo.momo.android.c.d;
import com.immomo.momo.plugin.f.c;
import java.util.Collection;
import java.util.List;

final class ca extends d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ TxWeiboActivity f2082a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ca(TxWeiboActivity txWeiboActivity, Context context) {
        super(context);
        this.f2082a = txWeiboActivity;
        if (txWeiboActivity.H != null) {
            txWeiboActivity.H.cancel(true);
        }
        txWeiboActivity.H = this;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        List a2 = this.f2082a.h == 1 ? com.immomo.momo.protocol.a.d.a(this.f2082a.j, this.f2082a.k, 0, 0, 0) : com.immomo.momo.protocol.a.d.a(this.f2082a.j, this.f2082a.k, 1, ((c) this.f2082a.i.getItem(this.f2082a.i.getCount() - 1)).e, ((c) this.f2082a.i.getItem(this.f2082a.i.getCount() - 1)).f2878a);
        TxWeiboActivity txWeiboActivity = this.f2082a;
        txWeiboActivity.h = txWeiboActivity.h + 1;
        return a2;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        List list = (List) obj;
        this.f2082a.i.b((Collection) list);
        if (list.size() < 10 && this.f2082a.p.getFooterViewsCount() > 0) {
            this.f2082a.p.removeFooterView(this.f2082a.D);
        }
        this.f2082a.findViewById(R.id.process_layout_root).setVisibility(8);
        super.a(list);
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.f2082a.findViewById(R.id.process_layout_root).setVisibility(8);
        this.f2082a.E.e();
        if (this.f2082a.h > 5) {
            this.f2082a.p.removeFooterView(this.f2082a.D);
        } else {
            this.f2082a.D.setVisibility(0);
        }
        super.b();
    }
}
