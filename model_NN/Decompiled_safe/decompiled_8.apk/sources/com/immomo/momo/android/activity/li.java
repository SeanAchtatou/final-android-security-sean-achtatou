package com.immomo.momo.android.activity;

import android.view.View;
import com.immomo.momo.R;
import com.immomo.momo.android.view.a.n;

final class li implements View.OnClickListener {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public /* synthetic */ lh f1812a;

    li(lh lhVar) {
        this.f1812a = lhVar;
    }

    public final void onClick(View view) {
        if (view == this.f1812a.R) {
            lh lhVar = this.f1812a;
            lh.Y();
            an anVar = (an) view.getTag(R.id.tag_item);
            if (anVar != null && anVar.b == 1008) {
                this.f1812a.a(n.a(this.f1812a.c(), (int) R.string.dialog_upload_network, new lj(this)));
            }
            this.f1812a.a((an) view.getTag(R.id.tag_item));
        }
    }
}
