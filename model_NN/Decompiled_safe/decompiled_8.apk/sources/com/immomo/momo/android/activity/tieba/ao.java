package com.immomo.momo.android.activity.tieba;

import android.content.Intent;
import com.immomo.momo.android.broadcast.x;

final class ao implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ PublishTieActivity f2166a;

    ao(PublishTieActivity publishTieActivity) {
        this.f2166a = publishTieActivity;
    }

    public final void run() {
        Intent intent = new Intent(x.f2367a);
        intent.putExtra("key_pid", this.f2166a.z.f3017a);
        intent.putExtra("key_tiebaid", this.f2166a.u);
        this.f2166a.sendBroadcast(intent);
    }
}
