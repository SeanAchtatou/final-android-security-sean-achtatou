package com.immomo.momo.android.activity.retrieve;

import android.content.Context;
import android.graphics.Bitmap;
import com.immomo.momo.android.c.d;
import com.immomo.momo.protocol.a.w;

final class m extends d {

    /* renamed from: a  reason: collision with root package name */
    private StringBuilder f2119a = new StringBuilder();
    private /* synthetic */ InputPhoneNumberActivity c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public m(InputPhoneNumberActivity inputPhoneNumberActivity, Context context) {
        super(context);
        this.c = inputPhoneNumberActivity;
        if (inputPhoneNumberActivity.r != null) {
            inputPhoneNumberActivity.r.cancel(true);
            inputPhoneNumberActivity.r = (m) null;
        }
        inputPhoneNumberActivity.r = this;
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ Object a(Object... objArr) {
        w.a();
        return w.a(this.f2119a);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        this.c.x.setImageBitmap((Bitmap) obj);
        this.c.t = true;
        this.c.u = this.f2119a.toString();
        this.b.a((Object) ("cookie:" + this.c.u));
    }
}
