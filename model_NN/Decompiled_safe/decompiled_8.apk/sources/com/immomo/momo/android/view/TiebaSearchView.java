package com.immomo.momo.android.view;

import android.content.Context;
import android.content.Intent;
import android.support.v4.b.a;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.android.a.jc;
import com.immomo.momo.android.activity.tieba.TiebaCreateActivity;
import com.immomo.momo.android.activity.tieba.TiebaProfileActivity;
import com.immomo.momo.g;
import com.immomo.momo.util.ao;
import java.util.ArrayList;

public class TiebaSearchView extends LinearLayout implements View.OnClickListener, AdapterView.OnItemClickListener {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public Button f2658a;
    /* access modifiers changed from: private */
    public RefreshOnOverScrollListView b;
    /* access modifiers changed from: private */
    public jc c = new jc(getContext(), new ArrayList());
    /* access modifiers changed from: private */
    public dn d;
    /* access modifiers changed from: private */
    public dl e;
    /* access modifiers changed from: private */
    public String f;
    /* access modifiers changed from: private */
    public Cdo g;
    private View h;
    /* access modifiers changed from: private */
    public View i;
    /* access modifiers changed from: private */
    public TextView j;

    public TiebaSearchView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        View inflate = inflate(getContext(), R.layout.tieba_searchview, null);
        addView(inflate, new LinearLayout.LayoutParams(-1, -1));
        this.h = inflate.findViewById(R.id.layout_cover);
        this.b = (RefreshOnOverScrollListView) inflate.findViewById(R.id.listview_search);
        View inflate2 = g.o().inflate((int) R.layout.searchview_foot, (ViewGroup) null);
        this.i = inflate2.findViewById(R.id.layout_root);
        this.j = (TextView) inflate2.findViewById(R.id.text_info);
        this.f2658a = (Button) inflate2.findViewById(R.id.btn_create);
        this.b.addFooterView(inflate2);
        this.b.setAdapter((ListAdapter) this.c);
        this.b.setVisibility(8);
        this.i.setVisibility(8);
        this.f2658a.setOnClickListener(this);
        this.b.setOnItemClickListener(this);
        this.h.setOnClickListener(this);
    }

    public final void a() {
        this.i.setVisibility(8);
        this.b.setVisibility(8);
        this.g.w();
        if (this.d != null && !this.d.isCancelled()) {
            this.d.cancel(true);
            this.d = null;
        }
    }

    public final void a(String str) {
        this.i.setVisibility(8);
        this.b.setVisibility(8);
        if (a.a((CharSequence) str.trim())) {
            ao.b("搜索内容不为空");
            return;
        }
        this.c.b();
        this.f = str;
        new dn(this, getContext()).execute(new Object[0]);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.layout_cover /*2131165601*/:
                break;
            default:
                return;
            case R.id.btn_create /*2131166901*/:
                new dl(this, getContext()).execute(new Object[0]);
                break;
        }
        this.g.w();
    }

    public void onItemClick(AdapterView adapterView, View view, int i2, long j2) {
        if (i2 <= this.c.getCount()) {
            com.immomo.momo.service.bean.c.a aVar = (com.immomo.momo.service.bean.c.a) this.c.getItem(i2);
            if (aVar.f3016a == 1) {
                Intent intent = new Intent(getContext(), TiebaProfileActivity.class);
                intent.putExtra("tiebaid", aVar.c.f3019a);
                intent.addFlags(268435456);
                getContext().startActivity(intent);
            } else if (aVar.f3016a == 2) {
                Intent intent2 = new Intent(getContext(), TiebaCreateActivity.class);
                intent2.putExtra(TiebaCreateActivity.i, aVar.c.b);
                intent2.putExtra(TiebaCreateActivity.h, aVar.c.g);
                intent2.putExtra(TiebaCreateActivity.k, aVar.c.f3019a);
                intent2.putExtra(TiebaCreateActivity.j, aVar.c.m);
                intent2.addFlags(268435456);
                getContext().startActivity(intent2);
            }
        }
    }

    public void setTiebaSearchViewListener(Cdo doVar) {
        this.g = doVar;
    }

    public void setVisibility(int i2) {
        if (i2 == 8 && this.d != null && !this.d.isCancelled()) {
            this.d.cancel(true);
            this.d = null;
        }
        super.setVisibility(i2);
    }
}
