package com.immomo.momo.android.pay;

import android.content.Context;
import com.immomo.momo.R;
import com.immomo.momo.a.r;
import com.immomo.momo.a.s;
import com.immomo.momo.a.t;
import com.immomo.momo.a.w;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.a;
import com.immomo.momo.service.aq;
import com.immomo.momo.service.bean.bg;
import com.immomo.momo.util.ao;
import java.util.Map;

final class l extends d {

    /* renamed from: a  reason: collision with root package name */
    private v f2563a = null;
    private Map c;
    private Context d;
    private int e = 0;
    private /* synthetic */ BuyMemberActivity f;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public l(BuyMemberActivity buyMemberActivity, Context context, Map map, int i) {
        super(context);
        this.f = buyMemberActivity;
        this.c = map;
        this.d = context;
        this.e = i;
        this.f2563a = new v(context);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        bg bgVar = new bg();
        a.a().c(this.c, bgVar);
        if (bgVar.f3012a) {
            this.f.f.b(bgVar.b);
            aq aqVar = new aq();
            aqVar.a(android.support.v4.b.a.a(bgVar.c), this.f.f.h);
            aqVar.a(bgVar.b, this.f.f.h);
        }
        return bgVar;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
        this.f2563a.setCancelable(false);
        this.f2563a.a("正在验证...");
        this.f.a(this.f2563a);
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        super.a(exc);
        if (!(exc instanceof t) && !(exc instanceof r) && !(exc instanceof s) && !(exc instanceof w) && (exc instanceof com.immomo.momo.a.a)) {
            BuyMemberActivity.p(this.f);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.android.pay.BuyMemberActivity.a(com.immomo.momo.android.pay.BuyMemberActivity, boolean):void
     arg types: [com.immomo.momo.android.pay.BuyMemberActivity, int]
     candidates:
      com.immomo.momo.android.pay.BuyMemberActivity.a(com.immomo.momo.android.pay.BuyMemberActivity, com.immomo.momo.android.pay.bk):void
      com.immomo.momo.android.pay.BuyMemberActivity.a(com.immomo.momo.android.pay.BuyMemberActivity, com.immomo.momo.android.pay.m):void
      com.immomo.momo.android.pay.BuyMemberActivity.a(com.immomo.momo.android.pay.BuyMemberActivity, com.immomo.momo.android.view.a.v):void
      com.immomo.momo.android.pay.BuyMemberActivity.a(com.immomo.momo.android.pay.BuyMemberActivity, java.lang.String):void
      com.immomo.momo.android.pay.BuyMemberActivity.a(com.immomo.momo.android.pay.BuyMemberActivity, java.util.List):void
      com.immomo.momo.android.pay.BuyMemberActivity.a(java.lang.String, int):void
      com.immomo.momo.android.activity.ah.a(android.os.Bundle, java.lang.String):boolean
      com.immomo.momo.android.activity.ao.a(int, java.lang.String[]):com.immomo.momo.protocol.imjson.c.a
      com.immomo.momo.android.activity.ao.a(com.immomo.momo.android.view.bi, android.view.View$OnClickListener):void
      com.immomo.momo.android.activity.ao.a(java.lang.CharSequence, int):void
      com.immomo.momo.android.activity.ao.a(android.os.Bundle, java.lang.String):boolean
      com.immomo.momo.android.pay.BuyMemberActivity.a(com.immomo.momo.android.pay.BuyMemberActivity, boolean):void */
    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        bg bgVar = (bg) obj;
        this.f.x();
        if (bgVar.f3012a) {
            BuyMemberActivity.q(this.f);
            this.f.H = false;
            this.f.t.setText((int) R.string.payvip_buy);
            if (!com.immomo.a.a.f.a.a(bgVar.f)) {
                ao.a((CharSequence) bgVar.f);
                return;
            }
            return;
        }
        BuyMemberActivity.p(this.f);
        if (this.e < 4) {
            try {
                Thread.sleep(2000);
            } catch (Throwable th) {
            }
            this.c.put("verify", "1");
            this.e++;
            a("验证，第" + this.e + "次请求");
            this.f.b(new l(this.f, this.d, this.c, this.e));
            return;
        }
        a("验证失败，请稍候重新验证。");
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.f.p();
    }
}
