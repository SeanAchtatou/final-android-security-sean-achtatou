package com.immomo.momo.android.activity.maintab;

import android.content.DialogInterface;
import android.view.View;
import com.immomo.momo.android.view.a.n;
import java.util.List;

final class bw implements View.OnClickListener {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public /* synthetic */ bh f1869a;

    bw(bh bhVar) {
        this.f1869a = bhVar;
    }

    public final void onClick(View view) {
        List d = this.f1869a.O.d();
        if (!d.isEmpty()) {
            n nVar = new n(this.f1869a.c());
            nVar.a("删除所选对话后，是否清空消息记录？");
            nVar.a(0, "清空", new bx(this, d));
            nVar.a(1, "不清空", new by(this, d));
            nVar.a(2, "取消", (DialogInterface.OnClickListener) null);
            nVar.show();
        }
    }
}
