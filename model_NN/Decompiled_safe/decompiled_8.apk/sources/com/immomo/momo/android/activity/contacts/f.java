package com.immomo.momo.android.activity.contacts;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import com.immomo.momo.android.activity.OtherProfileActivity;
import com.immomo.momo.service.bean.bf;

final class f implements AdapterView.OnItemClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ a f1188a;

    f(a aVar) {
        this.f1188a = aVar;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        Intent intent = new Intent(this.f1188a.c(), OtherProfileActivity.class);
        intent.putExtra("tag", "local");
        intent.putExtra("momoid", ((bf) this.f1188a.R.get(i)).h);
        this.f1188a.a(intent);
    }
}
