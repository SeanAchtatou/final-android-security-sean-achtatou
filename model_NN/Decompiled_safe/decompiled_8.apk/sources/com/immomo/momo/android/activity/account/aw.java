package com.immomo.momo.android.activity.account;

import com.immomo.momo.protocol.a.w;
import java.util.TimerTask;

final class aw extends TimerTask {

    /* renamed from: a  reason: collision with root package name */
    private int f950a = 0;
    /* access modifiers changed from: private */
    public /* synthetic */ SecurityCheckActivity b;

    aw(SecurityCheckActivity securityCheckActivity) {
        this.b = securityCheckActivity;
    }

    public final void run() {
        if (this.f950a > 240) {
            this.b.n.cancel();
            this.b.n = null;
            cancel();
            return;
        }
        try {
            this.b.e.b((Object) "!!! timer run");
            this.f950a++;
            w.a().h(this.b.m);
            this.b.n.cancel();
            this.b.n = null;
            this.b.runOnUiThread(new ax(this));
        } catch (Exception e) {
            this.b.e.a((Throwable) e);
        }
    }
}
