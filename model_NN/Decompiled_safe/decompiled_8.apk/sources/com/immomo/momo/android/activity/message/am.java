package com.immomo.momo.android.activity.message;

import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import com.immomo.momo.android.activity.common.CloudMessageTabsActivity;
import com.immomo.momo.android.activity.common.CreateDiscussTabsActivity;
import com.immomo.momo.android.view.a.n;
import mm.purchasesdk.PurchaseCode;

final class am implements AdapterView.OnItemClickListener {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public /* synthetic */ ChatActivity f1920a;
    private final /* synthetic */ String[] b;

    am(ChatActivity chatActivity, String[] strArr) {
        this.f1920a = chatActivity;
        this.b = strArr;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        if ("语音收听方式".equals(this.b[i])) {
            this.f1920a.ae();
        } else if ("创建多人对话".equals(this.b[i])) {
            Intent intent = new Intent(this.f1920a, CreateDiscussTabsActivity.class);
            intent.putExtra("invite_user_id", this.f1920a.i);
            this.f1920a.startActivity(intent);
        } else if ("同步聊天消息".equals(this.b[i])) {
            if (this.f1920a.f.b()) {
                Intent intent2 = new Intent(this.f1920a, CloudMessageTabsActivity.class);
                intent2.putExtra("invite_user_id", this.f1920a.i);
                this.f1920a.startActivityForResult(intent2, PurchaseCode.AUTH_SDK_ERROR);
                return;
            }
            n.a(this.f1920a, "成为陌陌会员即可同步好友聊天记录", "查看详情", "取消", new an(this), (DialogInterface.OnClickListener) null).show();
        } else if ("设置聊天背景".equals(this.b[i])) {
            ChatBGSettingActivity.a(this.f1920a, this.f1920a.Q.aR);
        }
    }
}
