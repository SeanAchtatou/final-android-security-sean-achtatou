package com.immomo.momo.android.b;

import android.location.Location;
import java.util.List;

final class v extends p {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ q f2340a;
    private final /* synthetic */ List c;
    private final /* synthetic */ String d;
    private final /* synthetic */ Object e;

    v(q qVar, List list, String str, Object obj) {
        this.f2340a = qVar;
        this.c = list;
        this.d = str;
        this.e = obj;
    }

    public final void a(Location location, int i, int i2, int i3) {
        this.c.add(location);
        if (this.c.size() > 0) {
            this.f2340a.f2335a.a(this.d);
            synchronized (this.e) {
                this.e.notify();
            }
        }
    }
}
