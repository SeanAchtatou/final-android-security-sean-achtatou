package com.immomo.momo.android.activity;

import android.content.Context;
import android.content.Intent;
import com.immomo.momo.android.activity.group.GroupProfileActivity;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.n;
import com.immomo.momo.service.bean.a.a;
import com.immomo.momo.service.y;

final class h extends d {

    /* renamed from: a  reason: collision with root package name */
    private v f1707a = null;
    private /* synthetic */ AddFriendActivity c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public h(AddFriendActivity addFriendActivity, Context context) {
        super(context);
        this.c = addFriendActivity;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        this.c.i = new a(((String[]) objArr)[0]);
        n.a().a(this.c.i.b, this.c.i);
        return "yes";
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.f1707a = new v(this.c);
        this.f1707a.setCancelable(true);
        this.f1707a.a("正在查找,请稍候...");
        this.f1707a.setOnCancelListener(new i(this));
        this.c.a(this.f1707a);
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        super.a(exc);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.service.y.a(com.immomo.momo.service.bean.a.a, boolean):void
     arg types: [com.immomo.momo.service.bean.a.a, int]
     candidates:
      com.immomo.momo.service.y.a(com.immomo.momo.service.bean.a.a, java.lang.String):void
      com.immomo.momo.service.y.a(java.lang.String, int):void
      com.immomo.momo.service.y.a(java.lang.String, java.lang.String):void
      com.immomo.momo.service.y.a(java.lang.String, java.util.List):void
      com.immomo.momo.service.y.a(java.lang.String, boolean):void
      com.immomo.momo.service.y.a(java.lang.String, java.lang.String[]):void
      com.immomo.momo.service.y.a(java.util.List, java.lang.String):void
      com.immomo.momo.service.y.a(com.immomo.momo.service.bean.a.a, boolean):void */
    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        if (((String) obj).equals("yes")) {
            new y().a(this.c.i, false);
            Intent intent = new Intent(this.c, GroupProfileActivity.class);
            intent.putExtra("gid", this.c.i.b);
            this.c.startActivity(intent);
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.c.p();
    }
}
