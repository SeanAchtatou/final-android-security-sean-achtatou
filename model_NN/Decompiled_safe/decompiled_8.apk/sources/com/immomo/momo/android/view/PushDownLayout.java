package com.immomo.momo.android.view;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;

public class PushDownLayout extends ViewGroup {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public float f2648a = 1.0f;
    /* access modifiers changed from: private */
    public boolean b = false;
    /* access modifiers changed from: private */
    public boolean c = false;
    /* access modifiers changed from: private */
    public boolean d = true;
    private Handler e = new ct(this);

    public PushDownLayout(Context context) {
        super(context);
    }

    public PushDownLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public PushDownLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    private static int a(int i, int i2, int i3) {
        int mode = View.MeasureSpec.getMode(i2);
        int size = View.MeasureSpec.getSize(i2);
        switch (mode) {
            case Integer.MIN_VALUE:
                if (size < i) {
                    i = size | 16777216;
                    break;
                }
                break;
            case 1073741824:
                i = size;
                break;
        }
        return (-16777216 & i3) | i;
    }

    public final void a() {
        this.d = false;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int i5 = 0;
        int childCount = getChildCount();
        int bottom = getBottom() - getTop();
        for (int i6 = 0; i6 < childCount; i6++) {
            View childAt = getChildAt(i6);
            if (childAt.getVisibility() != 8) {
                int measuredHeight = childAt.getMeasuredHeight();
                int i7 = bottom - measuredHeight;
                int measuredWidth = childAt.getMeasuredWidth();
                childAt.layout(i5, i7, i5 + measuredWidth, measuredHeight + i7);
                i5 += measuredWidth;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int i3 = -1;
        int childCount = getChildCount();
        int i4 = 0;
        int i5 = 0;
        while (i4 < childCount) {
            View childAt = getChildAt(i4);
            childAt.measure(i, i2);
            int measuredHeight = childAt.getMeasuredHeight();
            if (i3 >= measuredHeight) {
                measuredHeight = i3;
            }
            i5 |= (childAt.getMeasuredWidth() & -16777216) | ((childAt.getMeasuredHeight() >> 16) & -256);
            i4++;
            i3 = measuredHeight;
        }
        setMeasuredDimension(a(getSuggestedMinimumWidth(), i, 0) | (i5 & -16777216), a(Math.round(((float) i3) * this.f2648a), i2, i5 << 16));
    }

    public void setVisibility(int i) {
        if (getVisibility() != 0 && i == 0) {
            this.f2648a = 0.0f;
            this.b = false;
            this.e.removeMessages(11);
            this.e.sendEmptyMessage(12);
        } else if (!(getVisibility() == 0 && i == 8)) {
            this.f2648a = 1.0f;
        }
        super.setVisibility(i);
    }
}
