package com.immomo.momo.android.activity.feed;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import com.immomo.momo.R;
import com.immomo.momo.android.a.cs;
import com.immomo.momo.android.activity.lh;
import com.immomo.momo.android.broadcast.d;
import com.immomo.momo.android.broadcast.f;
import com.immomo.momo.android.broadcast.p;
import com.immomo.momo.android.view.HeaderLayout;
import com.immomo.momo.android.view.LoadingButton;
import com.immomo.momo.android.view.MomoRefreshListView;
import com.immomo.momo.android.view.bl;
import com.immomo.momo.android.view.bu;
import com.immomo.momo.android.view.cv;
import com.immomo.momo.g;
import com.immomo.momo.service.af;
import com.immomo.momo.service.ai;
import com.immomo.momo.service.bean.ab;
import com.immomo.momo.util.k;
import java.util.Date;

public class y extends lh implements AdapterView.OnItemClickListener, bl, bu, cv {
    /* access modifiers changed from: private */
    public int O = 0;
    /* access modifiers changed from: private */
    public Date P = null;
    /* access modifiers changed from: private */
    public cs Q = null;
    /* access modifiers changed from: private */
    public ai R;
    /* access modifiers changed from: private */
    public MomoRefreshListView S = null;
    /* access modifiers changed from: private */
    public LoadingButton T = null;
    /* access modifiers changed from: private */
    public ab U;
    /* access modifiers changed from: private */
    public ab V = null;
    private f W = null;
    private p X = null;
    private d Y = new z(this);

    static /* synthetic */ ab e(y yVar) {
        if (yVar.Q == null || yVar.Q.getCount() <= 0) {
            return null;
        }
        return (ab) yVar.Q.getItem(yVar.Q.getCount() - 1);
    }

    /* access modifiers changed from: protected */
    public final int B() {
        return R.layout.activity_friendsfeedlist;
    }

    /* access modifiers changed from: protected */
    public final void C() {
        this.S = (MomoRefreshListView) c((int) R.id.lv_feed);
        this.S.setEnableLoadMoreFoolter(true);
        this.T = this.S.getFooterViewButton();
        this.T.setOnProcessListener(this);
        this.S.a(g.o().inflate((int) R.layout.include_friendsfeed_listempty, (ViewGroup) null));
        this.S.setListPaddingBottom(-3);
        this.S.setOnPullToRefreshListener$42b903f6(this);
        this.S.setOnCancelListener$135502(this);
        this.S.setOnItemClickListener(this);
        this.S.addHeaderView(g.o().inflate((int) R.layout.listitem_blank, (ViewGroup) null));
        this.S.setOnTouchListener(new aa());
    }

    /* access modifiers changed from: protected */
    public final void E() {
        super.E();
        this.S.o();
        if (this.T.d()) {
            this.T.e();
        }
    }

    public final void S() {
        super.S();
        if (this.Q.isEmpty() || af.c().i() > 0) {
            this.S.l();
        }
    }

    public final void a(Context context, HeaderLayout headerLayout) {
        headerLayout.setTitleText((int) R.string.feedtabs_feeds);
    }

    public final void ae() {
        super.ae();
        new k("PI", "P54").e();
    }

    public final void ag() {
        super.ag();
        new k("PO", "P54").e();
    }

    public final void ai() {
        this.S.i();
    }

    public final void aj() {
        this.Q = new cs(c(), this.R.c(), this.S);
        this.S.setAdapter((ListAdapter) this.Q);
        if (this.Q.getCount() < 20) {
            this.T.setVisibility(8);
        } else {
            this.O++;
            this.T.setVisibility(0);
        }
        this.P = this.N.b("prf_time_frients_feed");
        this.S.setLastFlushTime(this.P);
    }

    public final void b_() {
        this.S.setLoadingVisible(true);
        if (this.U != null && !this.U.isCancelled()) {
            this.U.cancel(true);
        }
        this.U = new ab(this, c(), true);
        this.U.execute(new Object[0]);
    }

    /* access modifiers changed from: protected */
    public final void g(Bundle bundle) {
        this.W = new f(c());
        this.W.a(this.Y);
        this.X = new p(c());
        this.X.a(this.Y);
        this.R = new ai();
        aj();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.android.activity.feed.FeedProfileActivity.a(android.content.Context, java.lang.String, boolean):void
     arg types: [android.support.v4.app.g, java.lang.String, int]
     candidates:
      com.immomo.momo.android.activity.feed.FeedProfileActivity.a(com.immomo.momo.android.activity.feed.FeedProfileActivity, java.lang.String, boolean):void
      com.immomo.momo.android.activity.ao.a(android.support.v4.app.Fragment, android.content.Intent, int):void
      android.support.v4.app.g.a(java.lang.String, java.io.PrintWriter, android.view.View):void
      android.support.v4.app.g.a(android.support.v4.app.Fragment, android.content.Intent, int):void
      com.immomo.momo.android.activity.feed.FeedProfileActivity.a(android.content.Context, java.lang.String, boolean):void */
    public void onItemClick(AdapterView adapterView, View view, int i, long j) {
        FeedProfileActivity.a((Context) c(), ((ab) this.Q.getItem(i)).h, false);
    }

    public final void p() {
        super.p();
        if (this.U != null && !this.U.isCancelled()) {
            this.U.cancel(true);
        }
        if (this.V != null && !this.V.isCancelled()) {
            this.V.cancel(true);
        }
        if (this.W != null) {
            a(this.W);
            this.W = null;
        }
        if (this.X != null) {
            a(this.X);
            this.X = null;
        }
    }

    public final void u() {
        this.T.f();
        if (this.V != null) {
            this.V.cancel(true);
        }
        this.V = new ab(this, g.c(), false);
        this.V.execute(new Object[0]);
    }

    public final void v() {
        this.S.n();
        this.T.e();
    }
}
