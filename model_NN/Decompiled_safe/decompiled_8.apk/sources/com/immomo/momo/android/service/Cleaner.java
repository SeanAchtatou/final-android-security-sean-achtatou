package com.immomo.momo.android.service;

import android.app.Service;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;
import android.os.IBinder;
import com.immomo.a.a.f.b;
import com.immomo.momo.MomoApplication;
import com.immomo.momo.a;
import com.immomo.momo.service.a.at;
import com.immomo.momo.service.a.au;
import com.immomo.momo.service.a.ba;
import com.immomo.momo.service.a.p;
import com.immomo.momo.util.ak;
import com.immomo.momo.util.m;
import java.io.File;
import java.util.Calendar;

public class Cleaner extends Service {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public m f2598a = new m(this);

    private static File a(String str) {
        while (true) {
            File file = new File(a.b(), str.substring(0, 1));
            if (!file.exists()) {
                file.mkdirs();
            }
            File file2 = new File(file, str);
            if (!file2.exists()) {
                file2.createNewFile();
                return file2;
            }
            str = b.a(16);
        }
    }

    static /* synthetic */ MomoApplication b(Cleaner cleaner) {
        return (MomoApplication) cleaner.getApplication();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.service.a.b.b(java.lang.String, java.lang.Object[]):void
     arg types: [java.lang.String, java.lang.String[]]
     candidates:
      com.immomo.momo.service.a.b.b(android.database.Cursor, java.lang.String):long
      com.immomo.momo.service.a.b.b(java.lang.String, java.lang.Object):java.lang.Object
      com.immomo.momo.service.a.b.b(java.lang.String[], java.lang.String[]):java.lang.Object
      com.immomo.momo.service.a.b.b(java.lang.String, java.lang.String[]):java.util.List
      com.immomo.momo.service.a.b.b(java.lang.String[], java.lang.Object[]):void
      com.immomo.momo.service.a.b.b(java.lang.String, java.lang.Object[]):void */
    /* JADX WARNING: Removed duplicated region for block: B:134:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0082  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x00ae A[Catch:{ Exception -> 0x0322 }] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0133  */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x028e A[SYNTHETIC, Splitter:B:72:0x028e] */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x029b A[Catch:{ Exception -> 0x02a5, all -> 0x02b6 }, LOOP:6: B:64:0x0272->B:74:0x029b, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:94:0x032c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static /* synthetic */ void c(com.immomo.momo.android.service.Cleaner r19) {
        /*
            com.immomo.momo.service.a.ag r7 = new com.immomo.momo.service.a.ag
            android.app.Application r2 = r19.getApplication()
            com.immomo.momo.MomoApplication r2 = (com.immomo.momo.MomoApplication) r2
            android.database.sqlite.SQLiteDatabase r2 = r2.e()
            r7.<init>(r2)
            com.immomo.momo.service.a.af r8 = new com.immomo.momo.service.a.af
            android.app.Application r2 = r19.getApplication()
            com.immomo.momo.MomoApplication r2 = (com.immomo.momo.MomoApplication) r2
            android.database.sqlite.SQLiteDatabase r2 = r2.e()
            r8.<init>(r2)
            java.lang.String r2 = "select m_remoteid from messages group by m_remoteid having count(m_remoteid)>600"
            r3 = 0
            java.lang.String[] r3 = new java.lang.String[r3]
            android.database.Cursor r4 = r7.a(r2, r3)
            com.immomo.momo.service.ae r9 = new com.immomo.momo.service.ae
            r9.<init>()
        L_0x002c:
            boolean r2 = r4.moveToNext()
            if (r2 != 0) goto L_0x0137
            if (r4 == 0) goto L_0x03a1
            r4.close()
            r2 = 0
        L_0x0038:
            r0 = r19
            com.immomo.momo.util.m r3 = r0.f2598a
            java.lang.String r4 = "delete usermessages finished"
            r3.a(r4)
            com.immomo.momo.service.w r4 = new com.immomo.momo.service.w     // Catch:{ Exception -> 0x038f }
            r4.<init>()     // Catch:{ Exception -> 0x038f }
            com.immomo.momo.service.a.z r5 = new com.immomo.momo.service.a.z     // Catch:{ Exception -> 0x038f }
            com.immomo.momo.MomoApplication r3 = com.immomo.momo.g.d()     // Catch:{ Exception -> 0x038f }
            android.database.sqlite.SQLiteDatabase r3 = r3.e()     // Catch:{ Exception -> 0x038f }
            r5.<init>(r3)     // Catch:{ Exception -> 0x038f }
            java.lang.String r3 = "select field4 from gmessages group by field4 having count(field4)>1000"
            r6 = 0
            java.lang.String[] r6 = new java.lang.String[r6]     // Catch:{ Exception -> 0x038f }
            android.database.Cursor r3 = r5.a(r3, r6)     // Catch:{ Exception -> 0x038f }
            android.database.sqlite.SQLiteDatabase r2 = r4.b()     // Catch:{ Exception -> 0x0318 }
            r2.beginTransaction()     // Catch:{ Exception -> 0x0318 }
        L_0x0063:
            boolean r2 = r3.moveToNext()     // Catch:{ Exception -> 0x0318 }
            if (r2 != 0) goto L_0x02bf
            android.database.sqlite.SQLiteDatabase r2 = r4.b()     // Catch:{ Exception -> 0x0318 }
            r2.setTransactionSuccessful()     // Catch:{ Exception -> 0x0318 }
            android.database.sqlite.SQLiteDatabase r2 = r4.b()     // Catch:{ Exception -> 0x0318 }
            r2.endTransaction()     // Catch:{ Exception -> 0x0318 }
            r0 = r19
            com.immomo.momo.util.m r2 = r0.f2598a     // Catch:{ Exception -> 0x0318 }
            java.lang.String r4 = "delelte groupmessage finished"
            r2.a(r4)     // Catch:{ Exception -> 0x0318 }
        L_0x0080:
            if (r3 == 0) goto L_0x0086
            r3.close()
            r3 = 0
        L_0x0086:
            com.immomo.momo.service.ag r4 = new com.immomo.momo.service.ag     // Catch:{ Exception -> 0x0322 }
            r4.<init>()     // Catch:{ Exception -> 0x0322 }
            android.database.sqlite.SQLiteDatabase r2 = r4.b()     // Catch:{ Exception -> 0x0322 }
            r2.beginTransaction()     // Catch:{ Exception -> 0x0322 }
            int r2 = r4.g()     // Catch:{ Exception -> 0x0322 }
            if (r2 != 0) goto L_0x00db
            int r2 = r4.h()     // Catch:{ Exception -> 0x0322 }
            r5 = 1000(0x3e8, float:1.401E-42)
            if (r2 <= r5) goto L_0x00db
            r2 = 1000(0x3e8, float:1.401E-42)
            r5 = 1001(0x3e9, float:1.403E-42)
            java.util.List r2 = r4.b(r2, r5)     // Catch:{ Exception -> 0x0322 }
            boolean r5 = r2.isEmpty()     // Catch:{ Exception -> 0x0322 }
            if (r5 != 0) goto L_0x00db
            r5 = 0
            java.lang.Object r2 = r2.get(r5)     // Catch:{ Exception -> 0x0322 }
            com.immomo.momo.service.bean.a.b r2 = (com.immomo.momo.service.bean.a.b) r2     // Catch:{ Exception -> 0x0322 }
            com.immomo.momo.service.a.v r5 = new com.immomo.momo.service.a.v     // Catch:{ Exception -> 0x0322 }
            com.immomo.momo.MomoApplication r6 = com.immomo.momo.g.d()     // Catch:{ Exception -> 0x0322 }
            android.database.sqlite.SQLiteDatabase r6 = r6.e()     // Catch:{ Exception -> 0x0322 }
            r5.<init>(r6)     // Catch:{ Exception -> 0x0322 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0322 }
            java.lang.String r7 = "_id<="
            r6.<init>(r7)     // Catch:{ Exception -> 0x0322 }
            int r2 = r2.j()     // Catch:{ Exception -> 0x0322 }
            java.lang.StringBuilder r2 = r6.append(r2)     // Catch:{ Exception -> 0x0322 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0322 }
            r6 = 0
            java.lang.String[] r6 = new java.lang.String[r6]     // Catch:{ Exception -> 0x0322 }
            r5.b(r2, r6)     // Catch:{ Exception -> 0x0322 }
        L_0x00db:
            android.database.sqlite.SQLiteDatabase r2 = r4.b()     // Catch:{ Exception -> 0x0322 }
            r2.setTransactionSuccessful()     // Catch:{ Exception -> 0x0322 }
            android.database.sqlite.SQLiteDatabase r2 = r4.b()     // Catch:{ Exception -> 0x0322 }
            r2.endTransaction()     // Catch:{ Exception -> 0x0322 }
            r0 = r19
            com.immomo.momo.util.m r2 = r0.f2598a     // Catch:{ Exception -> 0x0322 }
            java.lang.String r4 = "delelte groupaction finished"
            r2.a(r4)     // Catch:{ Exception -> 0x0322 }
        L_0x00f2:
            com.immomo.momo.service.g r4 = new com.immomo.momo.service.g     // Catch:{ Throwable -> 0x0385 }
            r4.<init>()     // Catch:{ Throwable -> 0x0385 }
            com.immomo.momo.service.a.i r5 = new com.immomo.momo.service.a.i     // Catch:{ Throwable -> 0x0385 }
            com.immomo.momo.MomoApplication r2 = com.immomo.momo.g.d()     // Catch:{ Throwable -> 0x0385 }
            android.database.sqlite.SQLiteDatabase r2 = r2.e()     // Catch:{ Throwable -> 0x0385 }
            r5.<init>(r2)     // Catch:{ Throwable -> 0x0385 }
            java.lang.String r2 = "select field4 from gmessages group by field4 having count(field4)>1000"
            r6 = 0
            java.lang.String[] r6 = new java.lang.String[r6]     // Catch:{ Throwable -> 0x0385 }
            android.database.Cursor r3 = r5.a(r2, r6)     // Catch:{ Throwable -> 0x0385 }
            android.database.sqlite.SQLiteDatabase r2 = r4.b()     // Catch:{ Throwable -> 0x0385 }
            r2.beginTransaction()     // Catch:{ Throwable -> 0x0385 }
        L_0x0114:
            boolean r2 = r3.moveToNext()     // Catch:{ Throwable -> 0x0385 }
            if (r2 != 0) goto L_0x032c
            android.database.sqlite.SQLiteDatabase r2 = r4.b()     // Catch:{ Throwable -> 0x0385 }
            r2.setTransactionSuccessful()     // Catch:{ Throwable -> 0x0385 }
            android.database.sqlite.SQLiteDatabase r2 = r4.b()     // Catch:{ Throwable -> 0x0385 }
            r2.endTransaction()     // Catch:{ Throwable -> 0x0385 }
            r0 = r19
            com.immomo.momo.util.m r2 = r0.f2598a     // Catch:{ Throwable -> 0x0385 }
            java.lang.String r4 = "delelte discussmessage finished"
            r2.a(r4)     // Catch:{ Throwable -> 0x0385 }
        L_0x0131:
            if (r3 == 0) goto L_0x0136
            r3.close()
        L_0x0136:
            return
        L_0x0137:
            r2 = 0
            java.lang.String r10 = r4.getString(r2)
            boolean r2 = r9.j(r10)
            if (r2 != 0) goto L_0x002c
            r2 = 500(0x1f4, float:7.0E-43)
            java.util.ArrayList r11 = new java.util.ArrayList
            r11.<init>()
            java.util.ArrayList r12 = new java.util.ArrayList
            r12.<init>()
        L_0x014e:
            r3 = 100
            java.util.List r13 = r9.a(r10, r2, r3)
            boolean r3 = r13.isEmpty()
            if (r3 != 0) goto L_0x025e
            int r3 = r13.size()
            int r3 = r3 + r2
            r0 = r19
            com.immomo.momo.util.m r2 = r0.f2598a
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            java.lang.String r6 = "delete "
            r5.<init>(r6)
            java.lang.StringBuilder r5 = r5.append(r10)
            java.lang.String r5 = r5.toString()
            r2.a(r5)
            org.json.JSONArray r14 = new org.json.JSONArray
            r14.<init>()
            java.util.Iterator r5 = r13.iterator()
        L_0x017e:
            boolean r2 = r5.hasNext()
            if (r2 != 0) goto L_0x01cb
            r6 = 0
            r2 = 16
            java.lang.String r2 = com.immomo.a.a.f.b.a(r2)     // Catch:{ Exception -> 0x0252, all -> 0x0288 }
            java.io.File r2 = a(r2)     // Catch:{ Exception -> 0x0252, all -> 0x0288 }
            java.io.FileOutputStream r5 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x0252, all -> 0x0288 }
            r5.<init>(r2)     // Catch:{ Exception -> 0x0252, all -> 0x0288 }
            java.lang.String r6 = r14.toString()     // Catch:{ Exception -> 0x039d, all -> 0x0396 }
            byte[] r6 = r6.getBytes()     // Catch:{ Exception -> 0x039d, all -> 0x0396 }
            byte[] r6 = com.immomo.momo.util.jni.Codec.a(r6, r10)     // Catch:{ Exception -> 0x039d, all -> 0x0396 }
            r5.write(r6)     // Catch:{ Exception -> 0x039d, all -> 0x0396 }
            r5.flush()     // Catch:{ Exception -> 0x039d, all -> 0x0396 }
            com.immomo.momo.service.bean.ap r6 = new com.immomo.momo.service.bean.ap     // Catch:{ Exception -> 0x039d, all -> 0x0396 }
            r6.<init>()     // Catch:{ Exception -> 0x039d, all -> 0x0396 }
            int r14 = r13.size()     // Catch:{ Exception -> 0x039d, all -> 0x0396 }
            r6.e = r14     // Catch:{ Exception -> 0x039d, all -> 0x0396 }
            java.util.Date r14 = new java.util.Date     // Catch:{ Exception -> 0x039d, all -> 0x0396 }
            r14.<init>()     // Catch:{ Exception -> 0x039d, all -> 0x0396 }
            r6.d = r14     // Catch:{ Exception -> 0x039d, all -> 0x0396 }
            java.lang.String r2 = r2.getPath()     // Catch:{ Exception -> 0x039d, all -> 0x0396 }
            r6.f2993a = r2     // Catch:{ Exception -> 0x039d, all -> 0x0396 }
            r6.b = r10     // Catch:{ Exception -> 0x039d, all -> 0x0396 }
            r12.add(r6)     // Catch:{ Exception -> 0x039d, all -> 0x0396 }
            r11.addAll(r13)     // Catch:{ Exception -> 0x039d, all -> 0x0396 }
            android.support.v4.b.a.a(r5)
            r2 = r3
            goto L_0x014e
        L_0x01cb:
            java.lang.Object r2 = r5.next()
            com.immomo.momo.service.bean.Message r2 = (com.immomo.momo.service.bean.Message) r2
            org.json.JSONObject r6 = new org.json.JSONObject     // Catch:{ Exception -> 0x0248 }
            r6.<init>()     // Catch:{ Exception -> 0x0248 }
            java.lang.String r15 = "remoteid"
            java.lang.String r0 = r2.remoteId     // Catch:{ Exception -> 0x0248 }
            r16 = r0
            r0 = r16
            r6.put(r15, r0)     // Catch:{ Exception -> 0x0248 }
            java.lang.String r15 = "status"
            int r0 = r2.status     // Catch:{ Exception -> 0x0248 }
            r16 = r0
            r0 = r16
            r6.put(r15, r0)     // Catch:{ Exception -> 0x0248 }
            java.lang.String r15 = "type"
            int r0 = r2.contentType     // Catch:{ Exception -> 0x0248 }
            r16 = r0
            r0 = r16
            r6.put(r15, r0)     // Catch:{ Exception -> 0x0248 }
            java.lang.String r15 = "timestamp"
            java.util.Date r0 = r2.timestamp     // Catch:{ Exception -> 0x0248 }
            r16 = r0
            long r16 = com.immomo.momo.service.a.b.a(r16)     // Catch:{ Exception -> 0x0248 }
            r0 = r16
            r6.put(r15, r0)     // Catch:{ Exception -> 0x0248 }
            java.lang.String r15 = "receive"
            boolean r0 = r2.receive     // Catch:{ Exception -> 0x0248 }
            r16 = r0
            r0 = r16
            r6.put(r15, r0)     // Catch:{ Exception -> 0x0248 }
            java.lang.String r15 = "msginfo"
            java.lang.String r16 = com.immomo.momo.util.a.a.b(r2)     // Catch:{ Exception -> 0x0248 }
            r0 = r16
            r6.put(r15, r0)     // Catch:{ Exception -> 0x0248 }
            java.lang.String r15 = "msgid"
            java.lang.String r0 = r2.msgId     // Catch:{ Exception -> 0x0248 }
            r16 = r0
            r0 = r16
            r6.put(r15, r0)     // Catch:{ Exception -> 0x0248 }
            int r15 = r2.contentType     // Catch:{ Exception -> 0x0248 }
            r16 = 2
            r0 = r16
            if (r15 != r0) goto L_0x0243
            java.lang.String r15 = "map_location"
            java.lang.String r16 = r2.getDbLocationjson()     // Catch:{ Exception -> 0x0248 }
            r0 = r16
            r6.put(r15, r0)     // Catch:{ Exception -> 0x0248 }
            java.lang.String r15 = "map_convertlocation"
            java.lang.String r2 = r2.getDbConverLocationJson()     // Catch:{ Exception -> 0x0248 }
            r6.put(r15, r2)     // Catch:{ Exception -> 0x0248 }
        L_0x0243:
            r14.put(r6)     // Catch:{ Exception -> 0x0248 }
            goto L_0x017e
        L_0x0248:
            r2 = move-exception
            r0 = r19
            com.immomo.momo.util.m r6 = r0.f2598a
            r6.a(r2)
            goto L_0x017e
        L_0x0252:
            r2 = move-exception
            r3 = r6
        L_0x0254:
            r0 = r19
            com.immomo.momo.util.m r5 = r0.f2598a     // Catch:{ all -> 0x0399 }
            r5.a(r2)     // Catch:{ all -> 0x0399 }
            android.support.v4.b.a.a(r3)
        L_0x025e:
            android.database.sqlite.SQLiteDatabase r2 = r7.a()
            r2.beginTransaction()
            int r2 = r12.size()     // Catch:{ Exception -> 0x02a5 }
            int r2 = r2 + -1
            r3 = r2
        L_0x026c:
            if (r3 >= 0) goto L_0x028e
            java.util.Iterator r3 = r11.iterator()     // Catch:{ Exception -> 0x02a5 }
        L_0x0272:
            boolean r2 = r3.hasNext()     // Catch:{ Exception -> 0x02a5 }
            if (r2 != 0) goto L_0x029b
            android.database.sqlite.SQLiteDatabase r2 = r7.a()     // Catch:{ Exception -> 0x02a5 }
            r2.setTransactionSuccessful()     // Catch:{ Exception -> 0x02a5 }
            android.database.sqlite.SQLiteDatabase r2 = r7.a()
            r2.endTransaction()
            goto L_0x002c
        L_0x0288:
            r2 = move-exception
            r5 = r6
        L_0x028a:
            android.support.v4.b.a.a(r5)
            throw r2
        L_0x028e:
            java.lang.Object r2 = r12.get(r3)     // Catch:{ Exception -> 0x02a5 }
            com.immomo.momo.service.bean.ap r2 = (com.immomo.momo.service.bean.ap) r2     // Catch:{ Exception -> 0x02a5 }
            r8.a(r2)     // Catch:{ Exception -> 0x02a5 }
            int r2 = r3 + -1
            r3 = r2
            goto L_0x026c
        L_0x029b:
            java.lang.Object r2 = r3.next()     // Catch:{ Exception -> 0x02a5 }
            com.immomo.momo.service.bean.Message r2 = (com.immomo.momo.service.bean.Message) r2     // Catch:{ Exception -> 0x02a5 }
            r7.c(r2)     // Catch:{ Exception -> 0x02a5 }
            goto L_0x0272
        L_0x02a5:
            r2 = move-exception
            r0 = r19
            com.immomo.momo.util.m r3 = r0.f2598a     // Catch:{ all -> 0x02b6 }
            r3.a(r2)     // Catch:{ all -> 0x02b6 }
            android.database.sqlite.SQLiteDatabase r2 = r7.a()
            r2.endTransaction()
            goto L_0x002c
        L_0x02b6:
            r2 = move-exception
            android.database.sqlite.SQLiteDatabase r3 = r7.a()
            r3.endTransaction()
            throw r2
        L_0x02bf:
            r2 = 0
            java.lang.String r6 = r3.getString(r2)     // Catch:{ Exception -> 0x0318 }
            r0 = r19
            com.immomo.momo.util.m r2 = r0.f2598a     // Catch:{ Exception -> 0x0318 }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0318 }
            java.lang.String r8 = "delelte group "
            r7.<init>(r8)     // Catch:{ Exception -> 0x0318 }
            java.lang.StringBuilder r7 = r7.append(r6)     // Catch:{ Exception -> 0x0318 }
            java.lang.String r7 = r7.toString()     // Catch:{ Exception -> 0x0318 }
            r2.a(r7)     // Catch:{ Exception -> 0x0318 }
            boolean r2 = r4.d(r6)     // Catch:{ Exception -> 0x0318 }
            if (r2 != 0) goto L_0x0063
            r2 = 1000(0x3e8, float:1.401E-42)
            r7 = 1001(0x3e9, float:1.403E-42)
            java.util.List r2 = r4.b(r6, r2, r7)     // Catch:{ Exception -> 0x0318 }
            boolean r7 = r2.isEmpty()     // Catch:{ Exception -> 0x0318 }
            if (r7 != 0) goto L_0x0063
            r7 = 0
            java.lang.Object r2 = r2.get(r7)     // Catch:{ Exception -> 0x0318 }
            com.immomo.momo.service.bean.Message r2 = (com.immomo.momo.service.bean.Message) r2     // Catch:{ Exception -> 0x0318 }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0318 }
            java.lang.String r8 = "_id<="
            r7.<init>(r8)     // Catch:{ Exception -> 0x0318 }
            int r2 = r2.id     // Catch:{ Exception -> 0x0318 }
            java.lang.StringBuilder r2 = r7.append(r2)     // Catch:{ Exception -> 0x0318 }
            java.lang.String r7 = " and field4="
            java.lang.StringBuilder r2 = r2.append(r7)     // Catch:{ Exception -> 0x0318 }
            java.lang.StringBuilder r2 = r2.append(r6)     // Catch:{ Exception -> 0x0318 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0318 }
            r6 = 0
            java.lang.String[] r6 = new java.lang.String[r6]     // Catch:{ Exception -> 0x0318 }
            r5.b(r2, r6)     // Catch:{ Exception -> 0x0318 }
            goto L_0x0063
        L_0x0318:
            r2 = move-exception
        L_0x0319:
            r0 = r19
            com.immomo.momo.util.m r4 = r0.f2598a
            r4.a(r2)
            goto L_0x0080
        L_0x0322:
            r2 = move-exception
            r0 = r19
            com.immomo.momo.util.m r4 = r0.f2598a
            r4.a(r2)
            goto L_0x00f2
        L_0x032c:
            r2 = 0
            java.lang.String r6 = r3.getString(r2)     // Catch:{ Throwable -> 0x0385 }
            r0 = r19
            com.immomo.momo.util.m r2 = r0.f2598a     // Catch:{ Throwable -> 0x0385 }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0385 }
            java.lang.String r8 = "delelte discuss "
            r7.<init>(r8)     // Catch:{ Throwable -> 0x0385 }
            java.lang.StringBuilder r7 = r7.append(r6)     // Catch:{ Throwable -> 0x0385 }
            java.lang.String r7 = r7.toString()     // Catch:{ Throwable -> 0x0385 }
            r2.a(r7)     // Catch:{ Throwable -> 0x0385 }
            boolean r2 = r4.e(r6)     // Catch:{ Throwable -> 0x0385 }
            if (r2 != 0) goto L_0x0114
            r2 = 1000(0x3e8, float:1.401E-42)
            r7 = 1001(0x3e9, float:1.403E-42)
            java.util.List r2 = r4.b(r6, r2, r7)     // Catch:{ Throwable -> 0x0385 }
            boolean r7 = r2.isEmpty()     // Catch:{ Throwable -> 0x0385 }
            if (r7 != 0) goto L_0x0114
            r7 = 0
            java.lang.Object r2 = r2.get(r7)     // Catch:{ Throwable -> 0x0385 }
            com.immomo.momo.service.bean.Message r2 = (com.immomo.momo.service.bean.Message) r2     // Catch:{ Throwable -> 0x0385 }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0385 }
            java.lang.String r8 = "_id<="
            r7.<init>(r8)     // Catch:{ Throwable -> 0x0385 }
            int r2 = r2.id     // Catch:{ Throwable -> 0x0385 }
            java.lang.StringBuilder r2 = r7.append(r2)     // Catch:{ Throwable -> 0x0385 }
            java.lang.String r7 = " and field4="
            java.lang.StringBuilder r2 = r2.append(r7)     // Catch:{ Throwable -> 0x0385 }
            java.lang.StringBuilder r2 = r2.append(r6)     // Catch:{ Throwable -> 0x0385 }
            java.lang.String r2 = r2.toString()     // Catch:{ Throwable -> 0x0385 }
            r6 = 0
            java.lang.String[] r6 = new java.lang.String[r6]     // Catch:{ Throwable -> 0x0385 }
            r5.b(r2, r6)     // Catch:{ Throwable -> 0x0385 }
            goto L_0x0114
        L_0x0385:
            r2 = move-exception
            r0 = r19
            com.immomo.momo.util.m r4 = r0.f2598a
            r4.a(r2)
            goto L_0x0131
        L_0x038f:
            r3 = move-exception
            r18 = r3
            r3 = r2
            r2 = r18
            goto L_0x0319
        L_0x0396:
            r2 = move-exception
            goto L_0x028a
        L_0x0399:
            r2 = move-exception
            r5 = r3
            goto L_0x028a
        L_0x039d:
            r2 = move-exception
            r3 = r5
            goto L_0x0254
        L_0x03a1:
            r2 = r4
            goto L_0x0038
        */
        throw new UnsupportedOperationException("Method not decompiled: com.immomo.momo.android.service.Cleaner.c(com.immomo.momo.android.service.Cleaner):void");
    }

    static /* synthetic */ void d(Cleaner cleaner) {
        try {
            ba baVar = new ba(((MomoApplication) cleaner.getApplication()).e());
            Calendar instance = Calendar.getInstance();
            instance.add(6, -5);
            baVar.b("field44<=? and field23='none'", new Object[]{Long.valueOf(ba.a(instance.getTime()))});
            cleaner.f2598a.a((Object) "删除过期的用户资料完成");
        } catch (Throwable th) {
            cleaner.f2598a.a(th);
        }
    }

    static /* synthetic */ void e(Cleaner cleaner) {
        try {
            p pVar = new p(((MomoApplication) cleaner.getApplication()).h());
            Calendar instance = Calendar.getInstance();
            instance.add(6, -5);
            pVar.b("field9<=?", new Object[]{Long.valueOf(com.immomo.momo.service.a.b.a(instance.getTime()))});
        } catch (Throwable th) {
            cleaner.f2598a.a(th);
        }
        try {
            au auVar = new au(((MomoApplication) cleaner.getApplication()).h());
            Calendar instance2 = Calendar.getInstance();
            instance2.add(6, -5);
            auVar.b("field11<=?", new Object[]{Long.valueOf(com.immomo.momo.service.a.b.a(instance2.getTime()))});
        } catch (Throwable th2) {
            cleaner.f2598a.a(th2);
        }
    }

    static /* synthetic */ void f(Cleaner cleaner) {
        try {
            at atVar = new at(((MomoApplication) cleaner.getApplication()).h());
            Calendar instance = Calendar.getInstance();
            instance.add(6, -5);
            atVar.b("field16<=?", new Object[]{Long.valueOf(com.immomo.momo.service.a.b.a(instance.getTime()))});
        } catch (Throwable th) {
            cleaner.f2598a.a(th);
        }
    }

    static /* synthetic */ void g(Cleaner cleaner) {
        try {
            com.immomo.momo.service.a.m mVar = new com.immomo.momo.service.a.m(((MomoApplication) cleaner.getApplication()).h());
            Calendar instance = Calendar.getInstance();
            instance.add(6, -7);
            mVar.b("field20<=?", new Object[]{Long.valueOf(com.immomo.momo.service.a.b.a(instance.getTime()))});
        } catch (Throwable th) {
            cleaner.f2598a.a(th);
        }
    }

    static /* synthetic */ void h(Cleaner cleaner) {
        try {
            SQLiteDatabase e = ((MomoApplication) cleaner.getApplication()).e();
            e.execSQL("VACUUM");
            e.execSQL("ANALYZE");
            SQLiteDatabase h = ((MomoApplication) cleaner.getApplication()).h();
            h.execSQL("VACUUM");
            h.execSQL("ANALYZE");
            SQLiteDatabase g = ((MomoApplication) cleaner.getApplication()).g();
            g.execSQL("VACUUM");
            g.execSQL("ANALYZE");
        } catch (Throwable th) {
            cleaner.f2598a.a(th);
        }
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.Long]
     candidates:
      com.immomo.momo.util.ak.a(android.content.Context, java.lang.String):com.immomo.momo.util.ak
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Integer):int
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Long):long
      com.immomo.momo.util.ak.a(java.lang.String, int):void
      com.immomo.momo.util.ak.a(java.lang.String, long):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.String):void
      com.immomo.momo.util.ak.a(java.lang.String, boolean):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Boolean):boolean
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void */
    public void onCreate() {
        super.onCreate();
        ak.a(this, "app_preference").a("last_cleartime", (Object) Long.valueOf(System.currentTimeMillis()));
        new Thread(new a(this)).start();
        if (Environment.getExternalStorageState().equals("mounted")) {
            new Thread(new b(this)).start();
        }
        new Thread(new c(this)).start();
        stopSelf();
    }

    public void onDestroy() {
        super.onDestroy();
    }
}
