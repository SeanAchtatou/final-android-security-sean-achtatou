package com.immomo.momo.android.activity.account;

import android.content.Context;
import com.immomo.momo.R;
import com.immomo.momo.a.g;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.w;
import com.immomo.momo.util.ao;

final class bz extends d {

    /* renamed from: a  reason: collision with root package name */
    private v f979a = null;
    private String c = null;
    private /* synthetic */ bn d;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public bz(bn bnVar, Context context, String str) {
        super(context);
        this.d = bnVar;
        this.c = str;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        this.b.a((Object) ("校验：" + this.c));
        return w.a().c(this.c, this.d.k);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.f979a = new v(this.d.q, "请稍候，正在校验....");
        this.f979a.setOnCancelListener(new ca(this));
        this.f979a.show();
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        if (exc instanceof g) {
            this.d.q.b(new cb(this.d, this.d.q));
            this.d.m.setImageBitmap(null);
            ao.d(R.string.reg_scode_timeout);
            return;
        }
        super.a(exc);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        String str = (String) obj;
        if (this.d.o != null && this.d.o.isShowing()) {
            this.d.o.dismiss();
        }
        this.d.q.r = str;
        this.d.c();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.f979a.dismiss();
        this.f979a = null;
    }
}
