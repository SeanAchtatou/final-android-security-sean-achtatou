package com.immomo.momo.android.activity.group;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.ka;
import com.immomo.momo.android.activity.lh;
import com.immomo.momo.android.activity.maintab.x;
import com.immomo.momo.service.ag;

public class MainGroupActivity extends ka implements View.OnClickListener {
    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        setContentView((int) R.layout.activity_groupmain);
        a(x.class, GroupActionListFragment.class);
        findViewById(R.id.groupmain_layout_tab1).setOnClickListener(this);
        findViewById(R.id.groupmain_layout_tab2).setOnClickListener(this);
        if (new ag().g() > 0) {
            c(1);
        }
    }

    /* access modifiers changed from: protected */
    public final void a(Fragment fragment, int i) {
        switch (i) {
            case 0:
                findViewById(R.id.groupmain_layout_tab1).setSelected(true);
                findViewById(R.id.groupmain_layout_tab2).setSelected(false);
                break;
            case 1:
                findViewById(R.id.groupmain_layout_tab2).setSelected(true);
                findViewById(R.id.groupmain_layout_tab1).setSelected(false);
                break;
            case 2:
                findViewById(R.id.groupmain_layout_tab2).setSelected(false);
                findViewById(R.id.groupmain_layout_tab1).setSelected(false);
                break;
        }
        m().a();
        ((lh) fragment).a(this, m());
        if (!((lh) fragment).T()) {
            ((lh) fragment).S();
        }
    }

    /* access modifiers changed from: protected */
    public final void a(lh lhVar, int i) {
        if (i == 0) {
            lhVar.a(findViewById(R.id.groupmain_layout_tab1));
        } else if (i == 1) {
            lhVar.a(findViewById(R.id.groupmain_layout_tab2));
        }
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.groupmain_layout_tab1 /*2131165680*/:
                c(0);
                return;
            case R.id.groupmain_tab_label /*2131165681*/:
            case R.id.groupmain_tab_count /*2131165682*/:
            default:
                return;
            case R.id.groupmain_layout_tab2 /*2131165683*/:
                c(1);
                return;
        }
    }
}
