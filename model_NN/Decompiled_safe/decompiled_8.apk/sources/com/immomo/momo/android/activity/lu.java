package com.immomo.momo.android.activity;

import android.os.Handler;
import android.os.Message;
import android.widget.TextSwitcher;
import mm.purchasesdk.PurchaseCode;

final class lu extends Handler {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ UserRoamActivity f1820a;

    lu(UserRoamActivity userRoamActivity) {
        this.f1820a = userRoamActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.android.activity.UserRoamActivity.a(com.immomo.momo.android.activity.UserRoamActivity, boolean):java.lang.String
     arg types: [com.immomo.momo.android.activity.UserRoamActivity, int]
     candidates:
      com.immomo.momo.android.activity.UserRoamActivity.a(com.immomo.momo.android.activity.UserRoamActivity, int):void
      com.immomo.momo.android.activity.UserRoamActivity.a(com.immomo.momo.android.activity.UserRoamActivity, android.location.Location):void
      com.immomo.momo.android.activity.UserRoamActivity.a(com.immomo.momo.android.activity.UserRoamActivity, com.immomo.momo.android.activity.mh):void
      com.immomo.momo.android.activity.UserRoamActivity.a(com.immomo.momo.android.activity.UserRoamActivity, java.lang.String):void
      com.immomo.momo.android.activity.UserRoamActivity.a(com.immomo.momo.android.activity.UserRoamActivity, boolean):java.lang.String */
    public final void handleMessage(Message message) {
        boolean z = false;
        switch (message.what) {
            case PurchaseCode.INIT_OK /*100*/:
                this.f1820a.n.setText(UserRoamActivity.a(this.f1820a, false));
                this.f1820a.E.sendEmptyMessageDelayed(100, 100);
                return;
            case PurchaseCode.QUERY_OK /*101*/:
                if (!this.f1820a.C && this.f1820a.h.isShown()) {
                    if (this.f1820a.z > 3) {
                        UserRoamActivity.f(this.f1820a);
                        return;
                    }
                    TextSwitcher a2 = this.f1820a.n;
                    UserRoamActivity userRoamActivity = this.f1820a;
                    if (this.f1820a.z == 3) {
                        z = true;
                    }
                    a2.setText(UserRoamActivity.a(userRoamActivity, z));
                    UserRoamActivity userRoamActivity2 = this.f1820a;
                    userRoamActivity2.z = userRoamActivity2.z + 1;
                    this.f1820a.E.sendEmptyMessageDelayed(PurchaseCode.QUERY_OK, (long) ((this.f1820a.z + 1) * PurchaseCode.UNSUPPORT_ENCODING_ERR));
                    return;
                }
                return;
            default:
                return;
        }
    }
}
