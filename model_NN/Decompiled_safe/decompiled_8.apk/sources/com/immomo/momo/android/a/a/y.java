package com.immomo.momo.android.a.a;

import android.graphics.Bitmap;
import com.immomo.momo.android.c.g;

final class y implements g {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ w f707a;

    y(w wVar) {
        this.f707a = wVar;
    }

    public final /* synthetic */ void a(Object obj) {
        Bitmap bitmap = (Bitmap) obj;
        w.j.remove(this.f707a.h.msgId);
        if (bitmap != null) {
            this.f707a.h.setImageLoadFailed(false);
            w.a(this.f707a.h.getLoadImageId(), bitmap);
        } else {
            this.f707a.h.setImageLoadFailed(true);
        }
        this.f707a.h.isLoadingResourse = false;
        this.f707a.d();
    }
}
