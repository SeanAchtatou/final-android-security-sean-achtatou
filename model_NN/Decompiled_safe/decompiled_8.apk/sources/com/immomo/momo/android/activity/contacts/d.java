package com.immomo.momo.android.activity.contacts;

import com.immomo.momo.service.bean.bf;

final class d implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ c f1186a;
    private final /* synthetic */ String b;

    d(c cVar, String str) {
        this.f1186a = cVar;
        this.b = str;
    }

    public final void run() {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < this.f1186a.f1181a.P.getCount()) {
                bf bfVar = (bf) this.f1186a.f1181a.P.getItem(i2);
                if (bfVar.h.equals(this.b)) {
                    this.f1186a.f1181a.V.a(bfVar, this.b);
                    this.f1186a.f1181a.Y.post(new e(this));
                    return;
                }
                i = i2 + 1;
            } else {
                return;
            }
        }
    }
}
