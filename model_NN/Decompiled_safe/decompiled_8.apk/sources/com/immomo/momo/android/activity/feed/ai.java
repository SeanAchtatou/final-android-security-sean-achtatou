package com.immomo.momo.android.activity.feed;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.protocol.a.k;
import com.immomo.momo.service.bean.ab;
import java.util.ArrayList;
import java.util.List;

final class ai extends d {

    /* renamed from: a  reason: collision with root package name */
    private List f1440a = new ArrayList();
    private /* synthetic */ MyFeedListActivity c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ai(MyFeedListActivity myFeedListActivity, Context context) {
        super(context);
        this.c = myFeedListActivity;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        boolean a2 = k.a().a(this.f1440a, this.c.p, this.c.f.h, MyFeedListActivity.l(this.c));
        for (ab abVar : this.f1440a) {
            abVar.b = this.c.f;
        }
        this.c.l.a(this.f1440a);
        if (this.f1440a != null && this.f1440a.size() > 0) {
            MyFeedListActivity myFeedListActivity = this.c;
            myFeedListActivity.p = myFeedListActivity.p + this.f1440a.size();
            this.b.a((Object) ("downloadOtherFeedList success, lastindex:" + this.c.p));
        }
        return Boolean.valueOf(a2);
    }

    /* access modifiers changed from: protected */
    public final void a() {
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        Boolean bool = (Boolean) obj;
        if (this.f1440a.isEmpty() || !bool.booleanValue()) {
            this.b.a((Object) "remove...");
            this.c.h.k();
        }
        for (ab abVar : this.f1440a) {
            if (!this.c.k.contains(abVar)) {
                this.c.k.add(abVar);
                this.c.o.b(abVar);
            }
        }
        this.c.o.notifyDataSetChanged();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.c.w();
    }
}
