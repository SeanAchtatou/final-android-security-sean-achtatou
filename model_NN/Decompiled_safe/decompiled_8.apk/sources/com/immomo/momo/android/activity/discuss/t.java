package com.immomo.momo.android.activity.discuss;

import android.os.Bundle;
import com.immomo.momo.R;
import com.immomo.momo.android.view.a.al;
import com.immomo.momo.android.view.a.n;
import com.immomo.momo.service.g;

final class t implements al {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ DiscussProfileActivity f1245a;

    t(DiscussProfileActivity discussProfileActivity) {
        this.f1245a = discussProfileActivity;
    }

    public final void a(int i) {
        switch (i) {
            case 0:
                if (this.f1245a.x != null) {
                    this.f1245a.x.f3033a = !this.f1245a.x.f3033a;
                    this.f1245a.x.a("discuss_ispush", Boolean.valueOf(this.f1245a.x.f3033a));
                    g gVar = new g();
                    if (gVar.e(this.f1245a.q)) {
                        if (this.f1245a.x.f3033a) {
                            gVar.a(this.f1245a.q, 5, 13);
                        } else {
                            gVar.a(this.f1245a.q, 13, 5);
                        }
                    }
                    Bundle bundle = new Bundle();
                    bundle.putString("sessionid", this.f1245a.q);
                    bundle.putInt("sessiontype", 6);
                    com.immomo.momo.g.d().a(bundle, "action.sessionchanged");
                    return;
                }
                return;
            case 1:
                n.a(this.f1245a, this.f1245a.getString(R.string.dprofile_setting_quit_tip), new u(this.f1245a)).show();
                return;
            default:
                return;
        }
    }
}
