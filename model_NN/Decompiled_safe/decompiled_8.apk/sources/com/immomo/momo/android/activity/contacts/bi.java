package com.immomo.momo.android.activity.contacts;

import com.immomo.momo.android.view.cv;
import java.util.Date;

final class bi implements cv {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ aw f1164a;

    private bi(aw awVar) {
        this.f1164a = awVar;
    }

    /* synthetic */ bi(aw awVar, byte b) {
        this(awVar);
    }

    public final void v() {
        this.f1164a.Y = new Date();
        this.f1164a.R.n();
        this.f1164a.W = this.f1164a.X;
        if (this.f1164a.aa != null && !this.f1164a.aa.isCancelled()) {
            this.f1164a.aa.cancel(true);
        }
    }
}
