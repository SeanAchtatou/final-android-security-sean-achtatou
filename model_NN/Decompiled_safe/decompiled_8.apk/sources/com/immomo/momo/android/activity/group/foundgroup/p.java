package com.immomo.momo.android.activity.group.foundgroup;

import android.content.Intent;
import android.view.View;
import com.immomo.momo.android.activity.WebviewActivity;
import com.immomo.momo.g;
import com.immomo.momo.util.k;

final class p implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ FoundGroupActivity f1674a;

    p(FoundGroupActivity foundGroupActivity) {
        this.f1674a = foundGroupActivity;
    }

    public final void onClick(View view) {
        Intent intent = new Intent(this.f1674a, WebviewActivity.class);
        new k("C", "C64604").e();
        intent.putExtra("webview_title", "群组介绍");
        intent.putExtra("webview_url", "http://m.immomo.com/inc/android/group.html?v=" + g.z());
        this.f1674a.startActivity(intent);
    }
}
