package com.immomo.momo.android.activity.plugin;

import android.content.Intent;
import android.view.View;
import com.immomo.momo.android.activity.contacts.CommunityPeopleActivity;

final class as implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ CommunityStatusActivity f2046a;

    as(CommunityStatusActivity communityStatusActivity) {
        this.f2046a = communityStatusActivity;
    }

    public final void onClick(View view) {
        Intent intent = new Intent();
        switch (this.f2046a.h) {
            case 1:
                intent.setClass(this.f2046a, CommunityPeopleActivity.class);
                intent.putExtra("type", 1);
                intent.putExtra("from", 11);
                this.f2046a.startActivity(intent);
                return;
            case 2:
                intent.setClass(this.f2046a, CommunityPeopleActivity.class);
                intent.putExtra("type", 2);
                intent.putExtra("from", 11);
                this.f2046a.startActivity(intent);
                return;
            case 3:
                intent.setClass(this.f2046a, CommunityPeopleActivity.class);
                intent.putExtra("type", 3);
                intent.putExtra("from", 11);
                this.f2046a.startActivity(intent);
                return;
            default:
                return;
        }
    }
}
