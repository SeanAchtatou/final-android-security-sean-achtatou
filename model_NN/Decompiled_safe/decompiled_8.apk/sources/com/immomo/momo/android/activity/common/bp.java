package com.immomo.momo.android.activity.common;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.protocol.a.h;
import java.util.List;

final class bp extends d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ba f1109a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public bp(ba baVar, Context context) {
        super(context);
        this.f1109a = baVar;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        List b = h.a().b();
        ba.a(this.f1109a, b.size());
        return b;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        List list = (List) obj;
        super.a(list);
        if (list != null) {
            this.f1109a.R = list;
            this.f1109a.P.b(list);
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
    }
}
