package com.immomo.momo.android.view.dragsort;

import android.database.DataSetObserver;

final class e extends DataSetObserver {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ DragSortListView f2792a;

    e(DragSortListView dragSortListView) {
        this.f2792a = dragSortListView;
    }

    private void a() {
        if (this.f2792a.A == 4) {
            this.f2792a.a();
        }
    }

    public final void onChanged() {
        a();
    }

    public final void onInvalidated() {
        a();
    }
}
