package com.immomo.momo.android.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import com.immomo.momo.g;
import java.util.List;

public abstract class d extends LinearLayout {

    /* renamed from: a  reason: collision with root package name */
    protected f f2771a;
    private int b = 0;
    private List c;
    private int d = 0;
    private int e = 0;
    private int f = 0;

    public d(Context context) {
        super(context);
        setOrientation(1);
    }

    public d(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        setOrientation(1);
    }

    public abstract View a(Object obj);

    public final Object a(int i) {
        if (this.c == null) {
            return null;
        }
        return this.c.get(i);
    }

    /* access modifiers changed from: protected */
    public void b(int i, View view) {
        view.setOnClickListener(new e(this, i));
    }

    public List getDatalist() {
        return this.c;
    }

    public int getItemCount() {
        if (this.c == null) {
            return 0;
        }
        return this.c.size();
    }

    public void setData(List list) {
        int i;
        removeAllViews();
        if (this.b != 0 && list != null && list.size() != 0) {
            this.c = list;
            this.d = getMeasuredWidth() / (this.b + this.f);
            this.e = ((getMeasuredWidth() % (this.b + this.f)) / this.d) + (this.f / 2);
            int size = list.size() / this.d;
            if (list.size() < this.d) {
                this.d = getItemCount();
                size = 0;
            }
            int i2 = 0;
            while (i2 <= size) {
                LinearLayout linearLayout = new LinearLayout(getContext());
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2);
                layoutParams.topMargin = this.e;
                addView(linearLayout, layoutParams);
                int i3 = 0;
                while (i3 < this.d && (i = (this.d * i2) + i3) < list.size()) {
                    View a2 = a(list.get(i));
                    LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(this.b, -2);
                    layoutParams2.leftMargin = this.e;
                    linearLayout.setOrientation(0);
                    linearLayout.addView(a2, layoutParams2);
                    b(i, a2);
                    i3++;
                }
                i2++;
            }
        }
    }

    public void setItemViewWeith(int i) {
        this.b = (int) (((float) i) * g.k());
    }

    public void setMinPading(int i) {
        this.f = (int) (((float) i) * g.k());
    }

    public void setOnMomoGridViewItemClickListener(f fVar) {
        this.f2771a = fVar;
    }
}
