package com.immomo.momo.android.activity.tieba;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.util.ao;

final class eo extends d {

    /* renamed from: a  reason: collision with root package name */
    private v f2266a = null;
    private /* synthetic */ TiebaManagerApplyActivity c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public eo(TiebaManagerApplyActivity tiebaManagerApplyActivity, Context context) {
        super(context);
        this.c = tiebaManagerApplyActivity;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        return com.immomo.momo.protocol.a.v.a().a(this.c.o, this.c.p, this.c.q, this.c.r, this.c.t);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
        this.f2266a = new v(this.c, "请稍候，正在提交...");
        this.f2266a.setOnCancelListener(new ep(this));
        this.c.a(this.f2266a);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        String str = (String) obj;
        super.a((Object) str);
        ao.b(str);
        this.c.finish();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        super.b();
        this.c.p();
    }
}
