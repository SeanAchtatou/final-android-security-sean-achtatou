package com.immomo.momo.android.activity.group;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.a.a.f.b;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.ImageFactoryActivity;
import com.immomo.momo.android.activity.js;
import com.immomo.momo.android.activity.jt;
import com.immomo.momo.android.view.EmoteEditeText;
import com.immomo.momo.android.view.HeaderLayout;
import com.immomo.momo.android.view.a.n;
import com.immomo.momo.android.view.bi;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.a.a;
import com.immomo.momo.service.bean.au;
import com.immomo.momo.service.y;
import com.immomo.momo.util.ak;
import com.immomo.momo.util.ao;
import com.immomo.momo.util.aq;
import com.immomo.momo.util.h;
import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import mm.purchasesdk.PurchaseCode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class EditGroupProfileActivity extends js {
    private File A = null;
    private View.OnClickListener B = new r(this);
    /* access modifiers changed from: private */
    public String C = null;
    /* access modifiers changed from: private */
    public boolean D = false;
    private View.OnClickListener E = new w(this);
    private View.OnLongClickListener F = new y(this);
    private Runnable G = new aa(this);
    /* access modifiers changed from: private */
    public Handler H = new ac(this);
    boolean l = true;
    /* access modifiers changed from: private */
    public a m;
    private bi n;
    private HeaderLayout o = null;
    /* access modifiers changed from: private */
    public y p = null;
    /* access modifiers changed from: private */
    public EmoteEditeText q;
    /* access modifiers changed from: private */
    public EmoteEditeText r;
    /* access modifiers changed from: private */
    public TextView s;
    private View t;
    private RadioGroup u;
    /* access modifiers changed from: private */
    public boolean v = false;
    /* access modifiers changed from: private */
    public HashMap w = new HashMap();
    /* access modifiers changed from: private */
    public HashMap x = new HashMap();
    /* access modifiers changed from: private */
    public boolean y = false;
    private String z = PoiTypeDef.All;

    static /* synthetic */ void a(EditGroupProfileActivity editGroupProfileActivity, String str, Bitmap bitmap) {
        int i = 0;
        if (!android.support.v4.b.a.a((CharSequence) str) && bitmap != null && editGroupProfileActivity.i != null) {
            int length = editGroupProfileActivity.i.length;
            for (int i2 = 0; i2 < editGroupProfileActivity.h.size(); i2++) {
                au auVar = (au) editGroupProfileActivity.h.get(i2);
                if (!auVar.d && auVar.b.equals(str)) {
                    auVar.f2997a = bitmap;
                }
            }
            while (i < length) {
                jt jtVar = (jt) editGroupProfileActivity.i[i].findViewById(R.id.avatar_cover).getTag();
                if (jtVar == null || !((au) jtVar.f1774a).b.equals(str)) {
                    i++;
                } else {
                    ((ImageView) editGroupProfileActivity.i[i].findViewById(R.id.avatar_imageview)).setImageBitmap(bitmap);
                    return;
                }
            }
        }
    }

    private void a(List list, JSONArray jSONArray) {
        int i = 0;
        if (list != null && list.size() > 0) {
            while (true) {
                int i2 = i;
                if (i2 < list.size()) {
                    au auVar = (au) list.get(i2);
                    if (!auVar.d) {
                        JSONObject jSONObject = new JSONObject();
                        try {
                            if (auVar.c) {
                                jSONObject.put("upload", "YES");
                                jSONObject.put("guid", auVar.b);
                            } else {
                                jSONObject.put("upload", "NO");
                                jSONObject.put("key", "photo_" + i2);
                                this.x.put("photo_" + i2, new File(new File(com.immomo.momo.a.g(), auVar.b.substring(0, 1)), String.valueOf(auVar.b) + ".jpg_"));
                            }
                            jSONArray.put(jSONObject);
                        } catch (JSONException e) {
                            this.e.a((Throwable) e);
                            a((CharSequence) "保存资料失败");
                            return;
                        }
                    }
                    i = i2 + 1;
                } else {
                    return;
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.h.a(java.lang.String, java.lang.String, int, boolean):void
     arg types: [java.lang.String, java.lang.String, int, int]
     candidates:
      com.immomo.momo.util.h.a(java.lang.String, android.graphics.Bitmap, int, boolean):java.io.File
      com.immomo.momo.util.h.a(java.lang.String, java.lang.String, int, boolean):void */
    static /* synthetic */ void b(EditGroupProfileActivity editGroupProfileActivity, a aVar) {
        int size = editGroupProfileActivity.h.size();
        for (int i = 0; i < size; i++) {
            au auVar = (au) editGroupProfileActivity.h.get(i);
            if (!auVar.c && !auVar.d && aVar.F != null && aVar.F.length >= size) {
                h.a(auVar.b, aVar.F[i], 2, true);
            }
        }
    }

    static /* synthetic */ void c(EditGroupProfileActivity editGroupProfileActivity) {
        Intent intent = new Intent("android.intent.action.GET_CONTENT");
        intent.setType("image/*");
        editGroupProfileActivity.startActivityForResult(Intent.createChooser(intent, "本地图片"), 11);
    }

    private void c(String str) {
        String[] b;
        if (!android.support.v4.b.a.a((CharSequence) str) && (b = android.support.v4.b.a.b(str, ",")) != null) {
            this.h.clear();
            for (String str2 : b) {
                au auVar = new au();
                auVar.b = str2;
                if (auVar.b.startsWith("temp_")) {
                    auVar.c = false;
                } else {
                    auVar.c = true;
                }
                auVar.d = false;
                this.h.add(auVar);
            }
            if (this.h.size() < 8) {
                au auVar2 = new au();
                auVar2.d = true;
                auVar2.c = false;
                this.h.add(auVar2);
            }
        }
    }

    static /* synthetic */ void d(EditGroupProfileActivity editGroupProfileActivity) {
        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("immomo_");
        stringBuffer.append(android.support.v4.b.a.c(new Date()));
        stringBuffer.append("_" + UUID.randomUUID());
        stringBuffer.append(".jpg");
        editGroupProfileActivity.z = stringBuffer.toString();
        intent.putExtra("output", Uri.fromFile(new File(com.immomo.momo.a.i(), editGroupProfileActivity.z)));
        editGroupProfileActivity.startActivityForResult(intent, 12);
    }

    static /* synthetic */ boolean f(EditGroupProfileActivity editGroupProfileActivity) {
        for (int i = 0; i < editGroupProfileActivity.h.size(); i++) {
            if (((au) editGroupProfileActivity.h.get(i)).d) {
                return true;
            }
        }
        return false;
    }

    static /* synthetic */ boolean r(EditGroupProfileActivity editGroupProfileActivity) {
        if (android.support.v4.b.a.a((CharSequence) editGroupProfileActivity.q.getText().toString().trim())) {
            editGroupProfileActivity.a((int) R.string.str_edit_groupinfo_sign);
            editGroupProfileActivity.q.setSelection(0);
            return false;
        } else if (editGroupProfileActivity.q.getText().toString().trim().length() < 15) {
            editGroupProfileActivity.a((int) R.string.str_edit_groupintroduction);
            editGroupProfileActivity.q.setSelection(0);
            return false;
        } else if (android.support.v4.b.a.a((CharSequence) editGroupProfileActivity.s.getText().toString().trim())) {
            editGroupProfileActivity.a((int) R.string.str_edit_groupinfo_chooseplace);
            return false;
        } else if (!android.support.v4.b.a.a((CharSequence) editGroupProfileActivity.r.getText().toString().trim())) {
            return true;
        } else {
            editGroupProfileActivity.a((int) R.string.str_edit_groupinfo_groupname);
            editGroupProfileActivity.r.setSelection(0);
            return false;
        }
    }

    static /* synthetic */ void u(EditGroupProfileActivity editGroupProfileActivity) {
        JSONArray jSONArray = new JSONArray();
        editGroupProfileActivity.a(editGroupProfileActivity.h, jSONArray);
        editGroupProfileActivity.w.put("photos", jSONArray.toString());
        editGroupProfileActivity.a(new aj(editGroupProfileActivity, editGroupProfileActivity)).execute(new Object[0]);
    }

    /* access modifiers changed from: private */
    public void v() {
        if (!this.r.getText().toString().equals(this.m.c)) {
            this.y = true;
        }
        this.m.c = this.r.getText().toString().trim();
        this.w.put("name", this.m.c);
        if (!this.q.getText().toString().equals(this.m.h)) {
            this.y = true;
        }
        this.m.h = this.q.getText().toString().trim();
        this.w.put("sign", this.m.h);
        if (android.support.v4.b.a.a((CharSequence) this.m.J)) {
            this.w.put("type", String.valueOf(this.m.L));
            this.w.put("sname", this.m.K);
            this.w.put("lat", new StringBuilder(String.valueOf(this.m.l)).toString());
            this.w.put("lng", new StringBuilder(String.valueOf(this.m.m)).toString());
            return;
        }
        this.w.put("sid", this.m.J);
        this.w.remove("sname");
        this.w.remove("lat");
        this.w.remove("lng");
    }

    /* access modifiers changed from: private */
    public void w() {
        if (!(this.m == null || this.m.F == null)) {
            this.h.clear();
            for (String str : this.m.F) {
                au auVar = new au();
                auVar.b = str;
                auVar.d = false;
                auVar.c = true;
                this.h.add(auVar);
            }
            if (this.h.size() < 8) {
                au auVar2 = new au();
                auVar2.d = true;
                auVar2.c = false;
                this.h.add(auVar2);
            }
        }
        x();
        z();
        if (this.m.F != null) {
            ArrayList arrayList = new ArrayList();
            for (String str2 : this.m.F) {
                au auVar3 = new au();
                auVar3.b = str2;
                auVar3.c = true;
                arrayList.add(auVar3);
            }
            if (arrayList.size() < 8) {
                au auVar4 = new au();
                auVar4.c = false;
                auVar4.d = true;
                arrayList.add(auVar4);
            }
            new Thread(this.G).start();
        }
    }

    /* access modifiers changed from: private */
    public void x() {
        if (this.h != null && this.h.size() > 0) {
            int size = this.h.size();
            if (size <= 4) {
                this.j.setVisibility(0);
                this.k.setVisibility(8);
            } else if (size > 4 && size <= 8) {
                this.j.setVisibility(0);
                this.k.setVisibility(0);
            }
            for (int i = 0; i < size; i++) {
                au auVar = (au) this.h.get(i);
                this.i[i].setVisibility(0);
                if (auVar.d) {
                    ImageView imageView = (ImageView) this.i[i].findViewById(R.id.avatar_cover);
                    imageView.setVisibility(0);
                    imageView.setTag(new jt(auVar, i));
                    imageView.setOnClickListener(this.E);
                    ((LinearLayout) this.i[i].findViewById(R.id.avatar_add)).setVisibility(0);
                } else {
                    ImageView imageView2 = (ImageView) this.i[i].findViewById(R.id.avatar_cover);
                    imageView2.setVisibility(0);
                    imageView2.setTag(new jt(auVar, i));
                    imageView2.setOnClickListener(this.E);
                    imageView2.setOnLongClickListener(this.F);
                    ((ImageView) this.i[i].findViewById(R.id.avatar_imageview)).setImageBitmap(auVar.f2997a);
                    ((LinearLayout) this.i[i].findViewById(R.id.avatar_add)).setVisibility(4);
                }
            }
            if (size < 8) {
                for (int i2 = size; i2 < 8; i2++) {
                    this.i[i2].setVisibility(4);
                }
            }
        }
    }

    private void y() {
        int i = R.id.creategroup_rb_residential;
        switch (this.m.L) {
            case 2:
                i = R.id.creategroup_rb_office;
                break;
            case 3:
                i = R.id.creategroup_rb_unit;
                break;
        }
        this.l = false;
        ((RadioButton) this.u.findViewById(i)).setChecked(true);
        this.l = true;
    }

    private void z() {
        if (this.m != null) {
            y();
            if (!android.support.v4.b.a.a((CharSequence) this.m.c)) {
                try {
                    if (this.m.c.getBytes("GBK").length > 20) {
                        String a2 = g.a(this.m.c, 20);
                        this.e.a((Object) ("string-result" + a2));
                        this.m.c = a2;
                    }
                    this.r.setText(this.m.c);
                } catch (Exception e) {
                    this.e.a((Object) e);
                }
            } else {
                this.r.setText(PoiTypeDef.All);
            }
            if (!android.support.v4.b.a.a((CharSequence) this.m.K)) {
                try {
                    if (this.m.K.getBytes("GBK").length > 20) {
                        String a3 = g.a(this.m.K, 20);
                        this.e.a((Object) ("string-result" + a3));
                        this.m.K = a3;
                    }
                    this.s.setText(this.m.K);
                } catch (Exception e2) {
                    this.e.a((Object) e2);
                }
            } else {
                this.s.setText(PoiTypeDef.All);
            }
            if (!android.support.v4.b.a.a((CharSequence) this.m.h)) {
                try {
                    if (this.m.h.getBytes("GBK").length > 512) {
                        String a4 = g.a(this.m.h, (int) PurchaseCode.QUERY_NO_APP);
                        this.e.a((Object) ("sign-result" + a4));
                        this.m.h = a4;
                    }
                    this.q.setText(this.m.h);
                } catch (Exception e3) {
                    this.e.a((Object) e3);
                }
            } else {
                this.q.setText(PoiTypeDef.All);
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        super.a(bundle);
        setContentView((int) R.layout.activity_edit_groupprofile);
        u();
        this.o.a(this.n, new ag(this));
        this.t.setOnClickListener(this.B);
        this.u.setOnCheckedChangeListener(new s(this));
        EmoteEditeText emoteEditeText = this.q;
        EmoteEditeText emoteEditeText2 = this.q;
        emoteEditeText.addTextChangedListener(new aq(PurchaseCode.QUERY_NO_APP));
        EmoteEditeText emoteEditeText3 = this.r;
        aq aqVar = new aq(20);
        new ad();
        aqVar.a();
        this.r.addTextChangedListener(aqVar);
        this.q.setOnFocusChangeListener(new ae(this));
        this.r.setOnFocusChangeListener(new af(this));
        if (bundle == null || !bundle.getBoolean("from_saveinstance", false)) {
            this.e.a((Object) "EditGroupProfile data is inited by intent");
            Intent intent = getIntent();
            this.C = intent.getStringExtra("gid");
            this.D = intent.getBooleanExtra("key_group_is_pass", this.D);
            this.p = new y();
            this.m = this.p.e(this.C);
            this.e.a((Object) ("EditGroupProfileActivity initData findGroup  Group----> " + this.m));
            if (this.m == null) {
                a((CharSequence) "当前群资料不存在");
                finish();
            }
            w();
            new ah(this, this).execute(new String[0]);
            return;
        }
        this.e.a((Object) "EditGroupProfile data is inited by saveInstance");
        this.C = (String) bundle.get("gid");
        this.D = ((Boolean) bundle.get("key_group_is_pass")).booleanValue();
        this.z = bundle.getString("camera_filename");
        String string = bundle.getString("avatorFilePath");
        if (!android.support.v4.b.a.a((CharSequence) string)) {
            this.A = new File(string);
        }
        this.p = new y();
        this.m = this.p.e(this.C);
        if (this.m != null) {
            this.m.c = bundle.getString("group_name") == null ? PoiTypeDef.All : bundle.getString("group_name");
            this.m.h = bundle.getString("group_sign") == null ? PoiTypeDef.All : bundle.getString("group_sign");
            this.m.K = bundle.getString("site_name") == null ? PoiTypeDef.All : bundle.getString("site_name");
            this.m.J = bundle.getString("site_id") == null ? PoiTypeDef.All : bundle.getString("site_id");
            this.m.L = bundle.getInt("site_type") == 0 ? this.m.L : bundle.getInt("site_type");
        }
        c(bundle.getString("group_photos"));
        x();
        z();
        new Thread(this.G).start();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.h.a(java.lang.String, android.graphics.Bitmap, int, boolean):java.io.File
     arg types: [java.lang.String, android.graphics.Bitmap, int, int]
     candidates:
      com.immomo.momo.util.h.a(java.lang.String, java.lang.String, int, boolean):void
      com.immomo.momo.util.h.a(java.lang.String, android.graphics.Bitmap, int, boolean):java.io.File */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.b.a.a(android.graphics.Bitmap, float, boolean):android.graphics.Bitmap
     arg types: [android.graphics.Bitmap, int, int]
     candidates:
      android.support.v4.b.a.a(android.graphics.Bitmap, int, int):android.graphics.Bitmap
      android.support.v4.b.a.a(java.io.File, int, int):android.graphics.Bitmap
      android.support.v4.b.a.a(java.lang.String, java.lang.String, java.lang.String):java.lang.String
      android.support.v4.b.a.a(java.lang.Object[], java.lang.String, java.lang.String):java.lang.String
      android.support.v4.b.a.a(android.graphics.Bitmap, float, boolean):android.graphics.Bitmap */
    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        Uri fromFile;
        switch (i) {
            case 11:
                if (i2 == -1 && intent != null) {
                    Uri data = intent.getData();
                    Intent intent2 = new Intent(this, ImageFactoryActivity.class);
                    intent2.setData(data);
                    intent2.putExtra("aspectY", 1);
                    intent2.putExtra("aspectX", 1);
                    intent2.putExtra("minsize", 320);
                    this.A = new File(com.immomo.momo.a.g(), "temp_" + b.a() + ".jpg_");
                    intent2.putExtra("outputFilePath", this.A.getAbsolutePath());
                    startActivityForResult(intent2, 13);
                    return;
                }
                return;
            case 12:
                if (i2 == -1 && !android.support.v4.b.a.a((CharSequence) this.z) && (fromFile = Uri.fromFile(new File(com.immomo.momo.a.i(), this.z))) != null) {
                    Intent intent3 = new Intent(this, ImageFactoryActivity.class);
                    intent3.setData(fromFile);
                    intent3.putExtra("aspectY", 1);
                    intent3.putExtra("aspectX", 1);
                    intent3.putExtra("minsize", 320);
                    this.A = new File(com.immomo.momo.a.g(), "temp_" + b.a() + ".jpg_");
                    intent3.putExtra("outputFilePath", this.A.getAbsolutePath());
                    startActivityForResult(intent3, 13);
                    return;
                }
                return;
            case 13:
                if (i2 == -1 && intent != null) {
                    if (!android.support.v4.b.a.a((CharSequence) this.z)) {
                        File file = new File(com.immomo.momo.a.i(), this.z);
                        if (file.exists()) {
                            file.delete();
                        }
                    }
                    if (this.A != null) {
                        String absolutePath = this.A.getAbsolutePath();
                        String substring = this.A.getName().substring(0, this.A.getName().lastIndexOf("."));
                        this.e.a((Object) ("filename=" + substring));
                        Bitmap l2 = android.support.v4.b.a.l(absolutePath);
                        if (l2 != null) {
                            this.e.a((Object) ("save large to " + h.a(substring, l2, 2, false).getPath()));
                            Bitmap a2 = android.support.v4.b.a.a(l2, 150.0f, true);
                            this.e.a((Object) ("save small to " + h.a(substring, a2, 3, false).getPath()));
                            au auVar = new au();
                            auVar.b = substring;
                            auVar.f2997a = a2;
                            auVar.d = false;
                            auVar.c = false;
                            if (this.h.size() < 8) {
                                this.h.add(this.h.size() - 1, auVar);
                            } else if (this.h.size() >= 8) {
                                this.h.remove(this.h.size() - 1);
                                this.h.add(auVar);
                            }
                            x();
                            this.y = true;
                            if (this.A != null && this.A.exists()) {
                                this.A.delete();
                                return;
                            }
                            return;
                        }
                        return;
                    }
                    return;
                } else if (i2 == 1003) {
                    ao.d(R.string.cropimage_error_size);
                    return;
                } else if (i2 == 1000) {
                    ao.d(R.string.cropimage_error_other);
                    return;
                } else if (i2 == 1002) {
                    ao.d(R.string.cropimage_error_store);
                    return;
                } else if (i2 == 1001) {
                    ao.d(R.string.cropimage_error_filenotfound);
                    return;
                } else {
                    return;
                }
            case 151:
                if (i2 == -1) {
                    String stringExtra = intent.getStringExtra("siteid");
                    this.e.a((Object) ("siteId" + stringExtra));
                    String stringExtra2 = intent.getStringExtra("sitename");
                    this.m.L = intent.getIntExtra("sitetype", this.m.L);
                    if (android.support.v4.b.a.a((CharSequence) stringExtra)) {
                        this.m.J = PoiTypeDef.All;
                        this.w.remove("sid");
                        this.v = this.m.L == 0;
                        this.w.put("type", new StringBuilder(String.valueOf(this.m.L)).toString());
                        this.w.put("sname", stringExtra2);
                        this.w.put("lat", new StringBuilder().append(intent.getDoubleExtra("lat", this.m.l)).toString());
                        this.w.put("lng", new StringBuilder().append(intent.getDoubleExtra("lng", this.m.m)).toString());
                        this.w.put("loctype", new StringBuilder().append(intent.getIntExtra("loctype", 0)).toString());
                    } else {
                        this.w.remove("sname");
                        this.w.remove("type");
                        this.w.remove("lat");
                        this.w.remove("lng");
                        this.w.remove("loctype");
                        this.w.put("sid", stringExtra);
                        this.v = false;
                    }
                    if (this.m.L == 0) {
                        this.m.L = 1;
                    }
                    this.m.J = stringExtra;
                    this.m.K = stringExtra2;
                    this.s.setText(stringExtra2);
                    this.y = true;
                    y();
                    return;
                }
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i == 4) {
            v();
            if (this.y) {
                n nVar = new n(this);
                nVar.a();
                nVar.setTitle((int) R.string.dialog_exit_editgroup_title);
                nVar.a((int) R.string.dialog_exit_editgroup_msg);
                nVar.a(0, "保存", new t(this));
                nVar.a(1, "不保存", new u(this));
                nVar.a(2, "取消", new v());
                nVar.show();
                return true;
            }
        }
        return super.onKeyDown(i, keyEvent);
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Bundle bundle) {
        super.onRestoreInstanceState(bundle);
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        ak.a(this, "edite_group_profile").b();
        if (!android.support.v4.b.a.a((CharSequence) this.z)) {
            bundle.putString("camera_filename", this.z);
        }
        if (this.A != null) {
            bundle.putString("avatorFilePath", this.A.getAbsolutePath());
        }
        bundle.putBoolean("from_saveinstance", true);
        bundle.putString("gid", this.C);
        bundle.putBoolean("key_group_is_pass", this.D);
        if (this.w.get("name") != null) {
            bundle.putString("group_name", (String) this.w.get("name"));
        } else {
            bundle.putString("group_name", this.m.c);
        }
        if (this.w.get("sign") != null) {
            bundle.putString("group_sign", (String) this.w.get("sign"));
        } else {
            bundle.putString("group_sign", this.m.h);
        }
        if (this.w.get("apply_desc") != null) {
            bundle.putString("group_applydes", (String) this.w.get("apply_desc"));
        } else {
            bundle.putString("group_applydes", this.m.i);
        }
        if (this.w.get("sitename") != null) {
            bundle.putString("site_name", (String) this.w.get("sitename"));
        } else {
            bundle.putString("site_name", this.m.K);
        }
        if (this.w.get("siteid") != null) {
            bundle.putString("site_id", (String) this.w.get("siteid"));
        } else {
            bundle.putString("site_id", this.m.J);
        }
        if (this.w.get("sitetype") != null) {
            bundle.putInt("site_type", Integer.parseInt((String) this.w.get("sitetype")));
        } else {
            bundle.putInt("site_type", this.m.L);
        }
        ArrayList arrayList = new ArrayList();
        if (this.h != null) {
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 >= this.h.size()) {
                    break;
                }
                String str = ((au) this.h.get(i2)).b;
                if (!android.support.v4.b.a.a((CharSequence) str)) {
                    arrayList.add(str);
                }
                i = i2 + 1;
            }
        }
        String a2 = android.support.v4.b.a.a(arrayList, ",");
        this.e.a((Object) ("guidsString: " + a2));
        if (!android.support.v4.b.a.a((CharSequence) a2)) {
            bundle.putString("group_photos", a2);
        } else {
            bundle.putString("group_photos", android.support.v4.b.a.a(this.m.F, ","));
        }
        super.onSaveInstanceState(bundle);
    }

    /* access modifiers changed from: protected */
    public final void u() {
        super.u();
        this.o = (HeaderLayout) findViewById(R.id.layout_header);
        this.n = new bi(this);
        this.n.a("保存").a((int) R.drawable.ic_topbar_confirm);
        this.o.setTitleText("编辑群资料");
        findViewById(R.id.layout_name);
        this.t = findViewById(R.id.profile_layout_sitename);
        this.s = (TextView) this.t.findViewById(R.id.profile_tv_sitename);
        this.u = (RadioGroup) findViewById(R.id.creategroup_rg_sitetype);
        this.q = (EmoteEditeText) findViewById(R.id.profile_tv_sign);
        this.r = (EmoteEditeText) findViewById(R.id.profile_tv_name);
    }
}
