package com.immomo.momo.android.activity.group;

import android.view.View;

final class ae implements View.OnFocusChangeListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ EditGroupProfileActivity f1529a;

    ae(EditGroupProfileActivity editGroupProfileActivity) {
        this.f1529a = editGroupProfileActivity;
    }

    public final void onFocusChange(View view, boolean z) {
        if (!z) {
            this.f1529a.q.setText(this.f1529a.q.getText().toString().trim());
        } else if (this.f1529a.m.h != null) {
            this.f1529a.q.setSelection(this.f1529a.q.getText().toString().length());
        }
    }
}
