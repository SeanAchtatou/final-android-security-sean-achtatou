package com.immomo.momo.android.activity.event;

import com.immomo.momo.R;
import com.immomo.momo.android.c.aa;
import com.immomo.momo.android.c.ab;
import com.immomo.momo.android.view.a.al;
import com.immomo.momo.g;

final class s implements al {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ EventFeedProfileActivity f1421a;
    private final /* synthetic */ boolean b;
    private final /* synthetic */ String c;

    s(EventFeedProfileActivity eventFeedProfileActivity, boolean z, String str) {
        this.f1421a = eventFeedProfileActivity;
        this.b = z;
        this.c = str;
    }

    public final void a(int i) {
        String[] stringArray = g.l().getStringArray(R.array.reportfeed_items);
        if (i >= stringArray.length) {
            return;
        }
        if (this.b) {
            this.f1421a.a(new aa(this.f1421a)).execute(stringArray[i], this.c);
            return;
        }
        this.f1421a.a(new ab(this.f1421a)).execute(stringArray[i], this.c);
    }
}
