package com.immomo.momo.android.pay;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.plugin.g.a;
import java.io.File;

final class aj extends d {

    /* renamed from: a  reason: collision with root package name */
    private v f2529a = null;
    private boolean c = false;
    private boolean d = false;
    private boolean e = false;
    private boolean f = false;
    private String g;
    private File h;
    /* access modifiers changed from: private */
    public /* synthetic */ MemberCenterActivity i;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public aj(MemberCenterActivity memberCenterActivity, Context context, boolean z, boolean z2, boolean z3, boolean z4, String str, File file) {
        super(context);
        this.i = memberCenterActivity;
        this.c = z;
        this.d = z2;
        this.e = z3;
        this.f = z4;
        this.g = str;
        this.h = file;
        this.f2529a = new v(context);
        this.f2529a.a("请求提交中");
        this.f2529a.setCancelable(true);
        this.f2529a.setOnCancelListener(new ak(this));
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ Object a(Object... objArr) {
        return com.immomo.momo.protocol.a.d.a(this.c, this.d, this.e, this.f);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.i.a(this.f2529a);
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        super.a(exc);
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void a(Object obj) {
        String str = (String) obj;
        if (this.f) {
            a.a().a(str, this.g, this.h);
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.i.p();
    }
}
