package com.immomo.momo.android.a;

import android.content.Context;
import com.immomo.momo.R;
import com.immomo.momo.a.n;
import com.immomo.momo.android.activity.ao;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.p;
import com.immomo.momo.service.ag;
import com.immomo.momo.service.bean.a.b;
import com.immomo.momo.service.bean.a.c;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

final class ea extends d {

    /* renamed from: a  reason: collision with root package name */
    private b f791a;
    private c c;
    /* access modifiers changed from: private */
    public /* synthetic */ dn d;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ea(dn dnVar, Context context, b bVar, c cVar) {
        super(context);
        this.d = dnVar;
        this.c = cVar;
        this.f791a = bVar;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        return com.immomo.momo.protocol.a.c.a().b(String.valueOf(p.f2900a) + this.c.c(), (Map) null);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        ((ao) this.d.d).a(new v(this.d.d, this));
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        ao aoVar = (ao) this.d.d;
        if (!(exc instanceof n) || !this.c.a().trim().equals("同意") || aoVar.h().b()) {
            super.a(exc);
        } else {
            aoVar.a(com.immomo.momo.android.view.a.n.a(aoVar, (int) R.string.nonvip__followgroup_dialogconfirm_msg, (int) R.string.nonvip__dialog_enter, (int) R.string.nonvip__dialog_cancel, new eb(this), new ec(this)));
        }
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        try {
            String string = new JSONObject((String) obj).getString("msg");
            this.c.d();
            new ag().a(this.f791a);
            a(string);
        } catch (JSONException e) {
            this.b.a((Throwable) e);
        }
        this.d.notifyDataSetChanged();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        ((ao) this.d.d).p();
    }
}
