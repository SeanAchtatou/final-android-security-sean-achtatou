package com.immomo.momo.android.activity.tieba;

import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.ImageFactoryActivity;
import com.immomo.momo.android.activity.MulImagePickerActivity;
import com.immomo.momo.android.activity.ah;
import com.immomo.momo.android.broadcast.w;
import com.immomo.momo.android.c.i;
import com.immomo.momo.android.view.EmoteInputView;
import com.immomo.momo.android.view.HeaderLayout;
import com.immomo.momo.android.view.MEmoteEditeText;
import com.immomo.momo.android.view.MGifImageView;
import com.immomo.momo.android.view.PublishButton;
import com.immomo.momo.android.view.PublishSelectPhotoView;
import com.immomo.momo.android.view.ResizeListenerLayout;
import com.immomo.momo.android.view.a.n;
import com.immomo.momo.android.view.au;
import com.immomo.momo.android.view.bi;
import com.immomo.momo.android.view.cs;
import com.immomo.momo.android.view.f;
import com.immomo.momo.g;
import com.immomo.momo.plugin.b.a;
import com.immomo.momo.protocol.a.v;
import com.immomo.momo.service.aq;
import com.immomo.momo.service.bean.av;
import com.immomo.momo.service.bean.c.b;
import com.immomo.momo.service.bean.q;
import com.immomo.momo.service.l;
import com.immomo.momo.service.m;
import com.immomo.momo.util.ao;
import com.immomo.momo.util.h;
import com.immomo.momo.util.j;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import mm.purchasesdk.PurchaseCode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class PublishTieActivity extends ah implements View.OnClickListener, View.OnTouchListener, cs, f, l {
    public static final String h = ("http://m.immomo.com/inc/android/tieba/agreement.html?v=" + g.z());
    /* access modifiers changed from: private */
    public Handler A = new Handler();
    private MGifImageView B;
    /* access modifiers changed from: private */
    public PublishSelectPhotoView C;
    private View D;
    private View E;
    private View F;
    /* access modifiers changed from: private */
    public PublishButton G;
    /* access modifiers changed from: private */
    public PublishButton H;
    private TextView I;
    /* access modifiers changed from: private */
    public au J;
    /* access modifiers changed from: private */
    public HashMap K = new HashMap();
    private String L = PoiTypeDef.All;
    private File M = null;
    private File N = null;
    /* access modifiers changed from: private */
    public a O = null;
    private Bitmap P = null;
    private int Q = 0;
    private HashMap R = new HashMap();
    private HashMap S = new HashMap();
    private String T = PoiTypeDef.All;
    /* access modifiers changed from: private */
    public String U = null;
    private m V = new m();
    protected boolean i = false;
    private bb j = null;
    private bb k = null;
    private bb l = null;
    private bb m = null;
    private bb n = null;
    /* access modifiers changed from: private */
    public MEmoteEditeText o = null;
    /* access modifiers changed from: private */
    public EditText p;
    /* access modifiers changed from: private */
    public TextView q = null;
    private ResizeListenerLayout r = null;
    private boolean s = false;
    /* access modifiers changed from: private */
    public int t = 0;
    /* access modifiers changed from: private */
    public String u;
    private String v;
    private int w;
    /* access modifiers changed from: private */
    public int x;
    /* access modifiers changed from: private */
    public EmoteInputView y = null;
    /* access modifiers changed from: private */
    public b z;

    /* access modifiers changed from: private */
    public void A() {
        findViewById(R.id.btn_localphoto).setEnabled(false);
        findViewById(R.id.btn_camera).setEnabled(false);
        findViewById(R.id.btn_emote).setEnabled(true);
        findViewById(R.id.layout_tip).setVisibility(8);
        D();
        F();
        this.y.a();
        D();
        this.B.setAlt(this.O.g());
        if (this.J != null) {
            this.J.l();
            this.J = null;
        }
        String g = this.O.g();
        String h2 = this.O.h();
        MGifImageView mGifImageView = this.B;
        boolean endsWith = g.endsWith(".gif");
        if (this.J == null) {
            File a2 = q.a(g, h2);
            this.e.b((Object) ("emoteFile:" + (a2 == null ? "null" : a2.getAbsolutePath())));
            if (a2 == null || !a2.exists()) {
                new i(g, h2, new aj(this, mGifImageView, endsWith)).a();
                return;
            }
            this.J = new au(endsWith ? 1 : 2);
            this.J.a(a2, mGifImageView);
            this.J.b(20);
            mGifImageView.setGifDecoder(this.J);
            return;
        }
        mGifImageView.setGifDecoder(this.J);
        if ((this.J.g() == 4 || this.J.g() == 2) && this.J.f() != null && this.J.f().exists()) {
            this.J.a(this.J.f(), mGifImageView);
        }
    }

    private ArrayList B() {
        ArrayList arrayList = new ArrayList();
        if (this.C.getDatalist() != null) {
            int i2 = 0;
            while (true) {
                int i3 = i2;
                if (i3 >= this.C.getDatalist().size()) {
                    break;
                }
                arrayList.add(((av) this.C.getDatalist().get(i3)).f2998a);
                i2 = i3 + 1;
            }
        }
        return arrayList;
    }

    private void C() {
        this.D.setVisibility(8);
    }

    private void D() {
        this.D.setVisibility(0);
        this.F.setVisibility(8);
    }

    private void E() {
        this.E.setVisibility(8);
        this.G.setSelected(false);
        this.H.setSelected(false);
    }

    private void F() {
        this.F.setVisibility(8);
    }

    private void G() {
        this.t = 0;
        findViewById(R.id.btn_localphoto).setEnabled(true);
        findViewById(R.id.btn_camera).setEnabled(true);
        findViewById(R.id.btn_emote).setEnabled(true);
        findViewById(R.id.layout_tip).setVisibility(0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.h.a(java.lang.String, java.lang.String, int, boolean):void
     arg types: [java.lang.String, java.lang.String, int, int]
     candidates:
      com.immomo.momo.util.h.a(java.lang.String, android.graphics.Bitmap, int, boolean):java.io.File
      com.immomo.momo.util.h.a(java.lang.String, java.lang.String, int, boolean):void */
    private void a(b bVar) {
        try {
            if (this.t == 2) {
                int b = bVar.b();
                for (int i2 = 0; i2 < b; i2++) {
                    String str = (String) this.S.get("photo_" + i2);
                    if (!android.support.v4.b.a.a((CharSequence) str)) {
                        h.a(str, bVar.c()[i2], 16, false);
                    }
                }
            }
        } catch (Throwable th) {
            this.e.a(th);
        }
    }

    private void a(List list) {
        ArrayList arrayList = new ArrayList();
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= list.size()) {
                this.C.a();
                this.C.a(arrayList);
                this.C.setData(this.C.getDatalist());
                return;
            }
            av avVar = new av();
            if (!android.support.v4.b.a.a((CharSequence) list.get(i3))) {
                avVar.f2998a = (String) list.get(i3);
                av avVar2 = (av) this.K.get(avVar.f2998a);
                if (avVar2 == null) {
                    File file = new File(avVar.f2998a);
                    if (file.exists()) {
                        Bitmap a2 = android.support.v4.b.a.a(file, (int) PurchaseCode.LOADCHANNEL_ERR, (int) PurchaseCode.LOADCHANNEL_ERR);
                        if (a2 != null) {
                            avVar.c = a2;
                            j.a(avVar.f2998a, a2);
                        }
                        avVar.b = file;
                        this.K.put(avVar.f2998a, avVar);
                        avVar2 = avVar;
                    } else {
                        avVar2 = null;
                    }
                }
                if (avVar2 != null) {
                    arrayList.add(avVar2);
                }
            }
            i2 = i3 + 1;
        }
    }

    private void c(int i2) {
        u();
        this.y.setEmoteFlag(i2);
        if (this.i) {
            this.A.postDelayed(new ad(this), 300);
        } else {
            this.y.b();
        }
    }

    /* access modifiers changed from: private */
    public void d(int i2) {
        if (i2 <= 0) {
            this.I.setVisibility(8);
            return;
        }
        this.I.setVisibility(0);
        this.I.setText(new StringBuilder().append(i2).toString());
    }

    static /* synthetic */ void g(PublishTieActivity publishTieActivity) {
        publishTieActivity.R.clear();
        publishTieActivity.S.clear();
        try {
            JSONArray jSONArray = new JSONArray();
            if (publishTieActivity.C.getDatalist() != null) {
                int i2 = 0;
                while (true) {
                    int i3 = i2;
                    if (i3 >= publishTieActivity.C.getDatalist().size()) {
                        publishTieActivity.T = jSONArray.toString();
                        return;
                    }
                    JSONObject jSONObject = new JSONObject();
                    jSONObject.put("upload", "NO");
                    jSONObject.put("key", "photo_" + i3);
                    publishTieActivity.R.put("photo_" + i3, ((av) publishTieActivity.C.getDatalist().get(i3)).b);
                    jSONArray.put(jSONObject);
                    i2 = i3 + 1;
                }
            }
        } catch (Exception e) {
            publishTieActivity.e.a((Throwable) e);
        }
    }

    static /* synthetic */ void h(PublishTieActivity publishTieActivity) {
        publishTieActivity.V.e = publishTieActivity.p.getText().toString().trim();
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("selectmode", publishTieActivity.t);
            jSONObject.put("tiebaid", publishTieActivity.u);
            jSONObject.put("tiebaname", publishTieActivity.v);
            jSONObject.put("content", publishTieActivity.o.getText().toString().trim());
            jSONObject.put("title", publishTieActivity.p.getText().toString().trim());
            jSONObject.put("emotionbody", publishTieActivity.O == null ? PoiTypeDef.All : publishTieActivity.O.toString());
            jSONObject.put("pathlist", android.support.v4.b.a.a(publishTieActivity.B(), ","));
            publishTieActivity.V.f3050a = jSONObject.toString();
        } catch (JSONException e) {
            publishTieActivity.e.a((Throwable) e);
        }
    }

    static /* synthetic */ void l(PublishTieActivity publishTieActivity) {
        if (!publishTieActivity.g.z) {
            n a2 = n.a(publishTieActivity, PoiTypeDef.All, "接受协议", new as(publishTieActivity));
            a2.setTitle("陌陌吧用户协议");
            View inflate = publishTieActivity.getLayoutInflater().inflate((int) R.layout.dialog_tieba_protocol, (ViewGroup) null);
            WebView webView = (WebView) inflate.findViewById(R.id.webview);
            View findViewById = inflate.findViewById(R.id.loading_indicator);
            WebSettings settings = webView.getSettings();
            settings.setCacheMode(2);
            settings.setJavaScriptEnabled(true);
            settings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
            webView.setWebChromeClient(new au());
            webView.setDownloadListener(new av(publishTieActivity));
            webView.setWebViewClient(new aw(publishTieActivity, findViewById));
            webView.loadUrl(h);
            a2.setOnCancelListener(new at(publishTieActivity));
            a2.setContentView(inflate);
            a2.show();
        }
    }

    static /* synthetic */ boolean n(PublishTieActivity publishTieActivity) {
        String trim = publishTieActivity.p.getText().toString().trim();
        if (trim.length() > 20) {
            publishTieActivity.a((CharSequence) "标题不能超过20个字");
            return false;
        } else if (trim.length() < 4) {
            publishTieActivity.a((CharSequence) "标题不能少于4个字");
            return false;
        } else {
            String trim2 = publishTieActivity.o.getText().toString().trim();
            if (trim2.length() > 1000) {
                publishTieActivity.a((CharSequence) "内容不能超过1000个字");
                return false;
            } else if (trim2.length() >= 10) {
                return true;
            } else {
                publishTieActivity.a((CharSequence) "内容不能少于10个字");
                return false;
            }
        }
    }

    private void y() {
        Display defaultDisplay = getWindowManager().getDefaultDisplay();
        this.w = defaultDisplay.getWidth();
        this.x = defaultDisplay.getHeight();
    }

    static /* synthetic */ void y(PublishTieActivity publishTieActivity) {
        publishTieActivity.E.setVisibility(0);
        if (publishTieActivity.t == 0) {
            publishTieActivity.D.setVisibility(8);
            publishTieActivity.F.setVisibility(8);
        } else if (publishTieActivity.t == 1) {
            publishTieActivity.D.setVisibility(0);
            publishTieActivity.F.setVisibility(8);
            publishTieActivity.A();
        } else if (publishTieActivity.t == 2) {
            publishTieActivity.D.setVisibility(8);
            publishTieActivity.F.setVisibility(0);
        }
    }

    private void z() {
        m().setTitleText("发布话题");
        String str = android.support.v4.b.a.a(this.v) ? PoiTypeDef.All : this.v;
        HeaderLayout m2 = m();
        if (android.support.v4.b.a.a((CharSequence) str)) {
            str = PoiTypeDef.All;
        }
        m2.setSubTitleText(str);
    }

    public final void a(int i2, View view) {
        Uri fromFile;
        if (this.C.getDatalist() != null && i2 < this.C.getDatalist().size() && (fromFile = Uri.fromFile(((av) this.C.getDatalist().get(i2)).b)) != null) {
            this.Q = i2;
            Intent intent = new Intent(this, ImageFactoryActivity.class);
            intent.setData(fromFile);
            intent.putExtra("minsize", 320);
            intent.putExtra("process_model", "filter");
            intent.putExtra("maxwidth", 720);
            intent.putExtra("maxheight", 3000);
            this.N = new File(com.immomo.momo.a.g(), "temp_" + com.immomo.a.a.f.b.a() + ".jpg_");
            intent.putExtra("outputFilePath", this.N.getAbsolutePath());
            startActivityForResult(intent, 105);
        }
    }

    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        String[] b;
        List asList;
        String[] b2;
        List asList2;
        super.a(bundle);
        setContentView((int) R.layout.activity_publish_tie);
        y();
        this.r = (ResizeListenerLayout) findViewById(R.id.rootlayout);
        this.o = (MEmoteEditeText) findViewById(R.id.signeditor_tv_text);
        this.q = (TextView) findViewById(R.id.tv_textcount);
        this.p = (EditText) findViewById(R.id.et_title);
        this.y = (EmoteInputView) findViewById(R.id.emoteview);
        this.y.setEditText(this.o);
        this.B = (MGifImageView) findViewById(R.id.iv_selected_emote);
        this.E = findViewById(R.id.layout_selected);
        this.D = findViewById(R.id.layout_selected_emote);
        this.F = findViewById(R.id.layout_selected_photo);
        this.C = (PublishSelectPhotoView) findViewById(R.id.list_selectphotos);
        this.G = (PublishButton) findViewById(R.id.btn_selectpic);
        this.H = (PublishButton) findViewById(R.id.btn_selectemote);
        this.G.setIcon(R.drawable.ic_publish_selectpic);
        this.H.setIcon(R.drawable.ic_publish_selectemote);
        this.I = (TextView) findViewById(R.id.tv_selectpic_count);
        this.G.setSelected(true);
        this.H.setSelected(false);
        this.o.setOnTouchListener(this);
        this.p.setOnTouchListener(this);
        this.o.addTextChangedListener(new af(this));
        bi a2 = new bi(getApplicationContext()).a((int) R.drawable.ic_topbar_confirm_white);
        a2.setMarginRight(8);
        a2.setBackgroundResource(R.drawable.bg_header_submit);
        m().a(a2, new ag(this));
        this.r.setOnResizeListener(new ah(this));
        this.y.setOnEmoteSelectedListener(new ai(this));
        findViewById(R.id.iv_delete_emote).setOnClickListener(this);
        findViewById(R.id.btn_localphoto).setOnClickListener(this);
        findViewById(R.id.btn_camera).setOnClickListener(this);
        findViewById(R.id.btn_emote).setOnClickListener(this);
        this.C.setOnMomoGridViewItemClickListener(this);
        this.C.setRefreshListener(this);
        this.G.setOnClickListener(this);
        this.H.setOnClickListener(this);
        if (getIntent().getExtras().containsKey("ddraft")) {
            this.V.b = getIntent().getIntExtra("ddraftid", 0);
            try {
                JSONObject jSONObject = new JSONObject(getIntent().getStringExtra("ddraft"));
                this.p.setText(jSONObject.optString("title", PoiTypeDef.All));
                this.p.setSelection(this.p.getText().toString().length());
                this.o.setText(jSONObject.optString("content", PoiTypeDef.All));
                this.t = jSONObject.optInt("selectmode", 0);
                if (this.t == 1 && !android.support.v4.b.a.a((CharSequence) jSONObject.optString("emotionbody", PoiTypeDef.All))) {
                    this.O = new a(jSONObject.optString("emotionbody", PoiTypeDef.All));
                    d(1);
                }
                this.u = jSONObject.optString("tiebaid", PoiTypeDef.All);
                this.v = jSONObject.optString("tiebaname", PoiTypeDef.All);
                if (!android.support.v4.b.a.a((CharSequence) jSONObject.optString("pathlist", PoiTypeDef.All)) && this.t == 2 && (b2 = android.support.v4.b.a.b(jSONObject.optString("pathlist", PoiTypeDef.All), ",")) != null && (asList2 = Arrays.asList(b2)) != null) {
                    b(new az(this, this, asList2));
                }
                z();
            } catch (JSONException e) {
                this.e.a((Throwable) e);
            }
        } else {
            if (bundle == null || !bundle.getBoolean("from_saveinstance", false)) {
                Intent intent = getIntent();
                if (!(intent == null || intent.getExtras() == null)) {
                    this.u = intent.getStringExtra("tieba_id") == null ? PoiTypeDef.All : intent.getStringExtra("tieba_id");
                    this.v = intent.getStringExtra("tieba_name") == null ? PoiTypeDef.All : intent.getStringExtra("tieba_name");
                }
            } else {
                this.u = bundle.getString("tieba_id");
                this.v = bundle.getString("tieba_name");
                String str = bundle.get("save_tiecontent") == null ? PoiTypeDef.All : (String) bundle.get("save_tiecontent");
                if (!android.support.v4.b.a.a((CharSequence) str)) {
                    this.o.setText(str);
                    this.o.setSelection(str.length());
                }
                String str2 = bundle.get("save_tietile") == null ? PoiTypeDef.All : (String) bundle.get("save_tietile");
                if (!android.support.v4.b.a.a((CharSequence) str2)) {
                    this.p.setText(str2);
                    this.p.setSelection(str2.length());
                }
                if (bundle.containsKey("camera_filename")) {
                    this.L = bundle.getString("camera_filename");
                }
                if (bundle.containsKey("camera_filepath")) {
                    this.M = new File(bundle.getString("camera_filepath"));
                }
                if (bundle.containsKey("local_filepath")) {
                    this.N = new File(bundle.getString("local_filepath"));
                }
                this.Q = bundle.getInt("posFilter");
                this.t = bundle.getInt("selectMode");
                if (this.t == 1 && bundle.get("emotionbody") != null) {
                    this.O = new a((String) bundle.get("emotionbody"));
                    d(1);
                } else if (!(this.t != 2 || bundle.get("pathlist") == null || (b = android.support.v4.b.a.b((String) bundle.get("pathlist"), ",")) == null || (asList = Arrays.asList(b)) == null)) {
                    a(asList);
                }
            }
            if (android.support.v4.b.a.a((CharSequence) this.u)) {
                a((CharSequence) "没有指定陌陌吧ID");
                finish();
            }
            z();
            this.A.postDelayed(new ar(this), 200);
        }
        this.j = new bb(this, findViewById(R.id.signeditor_layout_syncto_sinaweibo), 1);
        this.l = new bb(this, findViewById(R.id.signeditor_layout_syncto_tx), 2);
        this.k = new bb(this, findViewById(R.id.signeditor_layout_syncto_renren), 3);
        this.m = new bb(this, findViewById(R.id.signeditor_layout_syncto_feed), 5);
        this.n = new bb(this, findViewById(R.id.signeditor_layout_syncto_weixin), 6);
        findViewById(R.id.tv_syntip).setVisibility(this.w <= 480 ? 8 : 0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.h.a(java.lang.String, android.graphics.Bitmap, int, boolean):java.io.File
     arg types: [java.lang.String, android.graphics.Bitmap, int, int]
     candidates:
      com.immomo.momo.util.h.a(java.lang.String, java.lang.String, int, boolean):void
      com.immomo.momo.util.h.a(java.lang.String, android.graphics.Bitmap, int, boolean):java.io.File */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.b.a.a(android.graphics.Bitmap, float, boolean):android.graphics.Bitmap
     arg types: [android.graphics.Bitmap, int, int]
     candidates:
      android.support.v4.b.a.a(android.graphics.Bitmap, int, int):android.graphics.Bitmap
      android.support.v4.b.a.a(java.io.File, int, int):android.graphics.Bitmap
      android.support.v4.b.a.a(java.lang.String, java.lang.String, java.lang.String):java.lang.String
      android.support.v4.b.a.a(java.lang.Object[], java.lang.String, java.lang.String):java.lang.String
      android.support.v4.b.a.a(android.graphics.Bitmap, float, boolean):android.graphics.Bitmap */
    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        switch (i2) {
            case 11:
                if (i3 == -1) {
                    this.f.an = true;
                    new aq().b(this.f);
                    this.j.g = true;
                    this.j.a(true);
                    Intent intent2 = new Intent(w.f2366a);
                    intent2.putExtra("momoid", this.f.h);
                    sendBroadcast(intent2);
                    return;
                }
                return;
            case 12:
                if (i3 == -1) {
                    this.f.ar = true;
                    this.k.g = true;
                    this.k.a(true);
                    Intent intent3 = new Intent(w.f2366a);
                    intent3.putExtra("momoid", this.f.h);
                    sendBroadcast(intent3);
                    return;
                }
                return;
            case 13:
                if (i3 == -1) {
                    this.f.at = true;
                    new aq().b(this.f);
                    this.l.g = true;
                    this.l.a(true);
                    Intent intent4 = new Intent(w.f2366a);
                    intent4.putExtra("momoid", this.f.h);
                    sendBroadcast(intent4);
                    return;
                }
                return;
            case PurchaseCode.QUERY_OK /*101*/:
                if (i3 == -1 && intent != null) {
                    this.t = 2;
                    if (!android.support.v4.b.a.a((CharSequence) this.L)) {
                        File file = new File(com.immomo.momo.a.i(), this.L);
                        if (file.exists()) {
                            file.delete();
                        }
                        this.L = null;
                    }
                    if (this.M != null) {
                        String absolutePath = this.M.getAbsolutePath();
                        String substring = this.M.getName().substring(0, this.M.getName().lastIndexOf("."));
                        Bitmap l2 = android.support.v4.b.a.l(absolutePath);
                        if (l2 != null) {
                            File a2 = h.a(substring, l2, 16, false);
                            this.e.a((Object) ("scaleAndSavePhoto, uploadFile=" + a2.getPath()));
                            this.P = android.support.v4.b.a.a(l2, 150.0f, true);
                            h.a(substring, this.P, 15, false);
                            av avVar = new av();
                            avVar.b = a2;
                            avVar.f2998a = a2.getAbsolutePath();
                            avVar.c = this.P;
                            this.K.put(avVar.f2998a, avVar);
                            this.C.b(avVar);
                            this.C.setData(this.C.getDatalist());
                            l2.recycle();
                        }
                        try {
                            this.M.delete();
                            this.M = null;
                        } catch (Exception e) {
                        }
                        getWindow().getDecorView().requestFocus();
                        return;
                    }
                    return;
                } else if (i3 == 1003) {
                    ao.b("图片尺寸太小，请重新选择", 1);
                    return;
                } else if (i3 == 1000) {
                    ao.d(R.string.cropimage_error_other);
                    return;
                } else if (i3 == 1002) {
                    ao.d(R.string.cropimage_error_store);
                    return;
                } else if (i3 == 1001) {
                    ao.d(R.string.cropimage_error_filenotfound);
                    return;
                } else {
                    return;
                }
            case PurchaseCode.UNSUB_OK /*103*/:
                if (i3 == -1 && !android.support.v4.b.a.a((CharSequence) this.L)) {
                    this.o.requestFocus();
                    Uri fromFile = Uri.fromFile(new File(com.immomo.momo.a.i(), this.L));
                    if (fromFile != null) {
                        Intent intent5 = new Intent(this, ImageFactoryActivity.class);
                        intent5.setData(fromFile);
                        intent5.putExtra("minsize", 320);
                        intent5.putExtra("process_model", "filter");
                        intent5.putExtra("maxwidth", 720);
                        intent5.putExtra("maxheight", 3000);
                        this.M = new File(com.immomo.momo.a.g(), "temp_" + com.immomo.a.a.f.b.a() + ".jpg_");
                        intent5.putExtra("outputFilePath", this.M.getAbsolutePath());
                        startActivityForResult(intent5, PurchaseCode.QUERY_OK);
                        return;
                    }
                    return;
                }
                return;
            case PurchaseCode.AUTH_OK /*104*/:
                if (i3 == -1 && intent != null) {
                    this.t = 2;
                    b(new az(this, this, intent.getStringArrayListExtra("select_images_path")));
                    return;
                }
                return;
            case 105:
                if (i3 != -1 || intent == null) {
                    if (i3 == 1003) {
                        ao.b("图片尺寸太小，请重新选择", 1);
                        return;
                    } else if (i3 == 1000) {
                        ao.d(R.string.cropimage_error_other);
                        return;
                    } else if (i3 == 1002) {
                        ao.d(R.string.cropimage_error_store);
                        return;
                    } else if (i3 == 1001) {
                        ao.d(R.string.cropimage_error_filenotfound);
                        return;
                    } else {
                        return;
                    }
                } else if (this.N != null) {
                    String absolutePath2 = this.N.getAbsolutePath();
                    String substring2 = this.N.getName().substring(0, this.N.getName().lastIndexOf("."));
                    Bitmap l3 = android.support.v4.b.a.l(absolutePath2);
                    if (l3 != null) {
                        File a3 = h.a(substring2, l3, 16, false);
                        this.e.a((Object) ("scaleAndSavePhoto, uploadFile=" + a3.getPath()));
                        Bitmap a4 = android.support.v4.b.a.a(l3, 150.0f, true);
                        h.a(substring2, a4, 15, false);
                        av avVar2 = (av) this.C.getDatalist().get(this.Q);
                        avVar2.b = a3;
                        avVar2.f2998a = a3.getAbsolutePath();
                        avVar2.c = a4;
                        this.K.put(avVar2.f2998a, avVar2);
                        this.C.a(this.Q, avVar2);
                        this.C.setData(this.C.getDatalist());
                        l3.recycle();
                    }
                    try {
                        this.N.delete();
                        this.N = null;
                    } catch (Exception e2) {
                    }
                    getWindow().getDecorView().requestFocus();
                    return;
                } else {
                    return;
                }
            default:
                return;
        }
    }

    public void onBackPressed() {
        if (r()) {
            if (this.y.isShown()) {
                this.y.a();
                this.i = false;
                return;
            }
            if (this.O != null || (this.C.getDatalist() != null && this.C.getDatalist().size() > 0) || android.support.v4.b.a.f(this.o.getText().toString().trim()) || android.support.v4.b.a.f(this.p.getText().toString().trim())) {
                n nVar = new n(this);
                nVar.a();
                nVar.setTitle((int) R.string.feed_publish_dialog_title);
                nVar.a((int) R.string.publishtie_quit_tip);
                nVar.a(0, (int) R.string.dialog_btn_confim, new ap(this));
                nVar.a(1, (int) R.string.dialog_btn_cancel, new aq());
                nVar.show();
                return;
            }
        }
        super.onBackPressed();
    }

    public void onClick(View view) {
        int i2 = 0;
        switch (view.getId()) {
            case R.id.btn_localphoto /*2131165805*/:
                C();
                ArrayList arrayList = new ArrayList();
                if (this.C.getDatalist() != null) {
                    while (true) {
                        int i3 = i2;
                        if (i3 < this.C.getDatalist().size()) {
                            arrayList.add(((av) this.C.getDatalist().get(i3)).f2998a);
                            i2 = i3 + 1;
                        }
                    }
                }
                Intent intent = new Intent(this, MulImagePickerActivity.class);
                intent.putStringArrayListExtra("select_images_path_results", arrayList);
                intent.putExtra("max_select_images_num", 6);
                startActivityForResult(intent, PurchaseCode.AUTH_OK);
                return;
            case R.id.btn_camera /*2131165806*/:
                C();
                if (this.C.getDatalist() == null) {
                    i2 = 1;
                } else if (this.C.getDatalist() != null && this.C.getDatalist().size() < 6) {
                    i2 = 1;
                }
                if (i2 == 0) {
                    a((CharSequence) "最多选择6张图片");
                    return;
                }
                Intent intent2 = new Intent("android.media.action.IMAGE_CAPTURE");
                StringBuffer stringBuffer = new StringBuffer();
                stringBuffer.append("immomo_");
                stringBuffer.append(android.support.v4.b.a.c(new Date()));
                stringBuffer.append("_" + UUID.randomUUID());
                stringBuffer.append(".jpg");
                this.L = stringBuffer.toString();
                intent2.putExtra("output", Uri.fromFile(new File(com.immomo.momo.a.i(), this.L)));
                startActivityForResult(intent2, PurchaseCode.UNSUB_OK);
                return;
            case R.id.btn_emote /*2131165807*/:
                c(4);
                return;
            case R.id.layout_selected_photo /*2131165808*/:
            case R.id.list_selectphotos /*2131165809*/:
            case R.id.layout_selected_emote /*2131165810*/:
            case R.id.iv_selected_emote /*2131165811*/:
            case R.id.tv_tip /*2131165813*/:
            case R.id.layout_line /*2131165814*/:
            case R.id.signeditor_layout_syncto_weixin /*2131165815*/:
            default:
                return;
            case R.id.iv_delete_emote /*2131165812*/:
                C();
                G();
                d(0);
                this.O = null;
                return;
            case R.id.btn_selectpic /*2131165816*/:
                u();
                this.A.postDelayed(new al(this), 300);
                return;
            case R.id.btn_selectemote /*2131165817*/:
                E();
                this.y.a();
                this.G.setSelected(false);
                this.H.setSelected(true);
                c(1);
                return;
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        y();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.j.b();
        this.k.b();
        this.l.b();
        this.m.b();
        this.n.b();
        if (this.J != null) {
            this.J.l();
        }
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Bundle bundle) {
        super.onRestoreInstanceState(bundle);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.s) {
            this.A.postDelayed(new ae(this), 200);
            getWindow().getDecorView().requestFocus();
            this.i = this.y.isShown();
        }
        if (!this.s) {
            this.s = true;
        }
        this.i = false;
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        String trim = this.o.getText().toString().trim();
        if (!android.support.v4.b.a.a((CharSequence) trim)) {
            bundle.putString("save_tiecontent", trim);
        }
        String trim2 = this.p.getText().toString().trim();
        if (!android.support.v4.b.a.a((CharSequence) trim2)) {
            bundle.putString("save_tietile", trim2);
        }
        bundle.putInt("selectMode", this.t);
        if (this.O != null && this.t == 1) {
            bundle.putString("emotionbody", this.O.toString());
        }
        if (this.C.getDatalist() != null && this.t == 2) {
            bundle.putString("pathlist", android.support.v4.b.a.a(B(), ","));
        }
        bundle.putString("tieba_id", this.u);
        bundle.putString("tieba_name", this.v);
        bundle.putBoolean("from_saveinstance", true);
        if (!android.support.v4.b.a.a((CharSequence) this.L)) {
            bundle.putString("camera_filename", this.L);
        }
        if (this.M != null) {
            bundle.putString("camera_filepath", this.M.getPath());
        }
        if (this.N != null) {
            bundle.putString("local_filepath", this.N.getPath());
        }
        bundle.putInt("posFilter", this.Q);
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        switch (view.getId()) {
            case R.id.layout_content /*2131165199*/:
                if ((this.y.isShown() || this.i) && 1 == motionEvent.getAction() && motionEvent.getEventTime() - motionEvent.getDownTime() < 100) {
                    if (this.y.isShown()) {
                        this.y.a();
                    } else {
                        u();
                    }
                    this.i = false;
                    break;
                }
            case R.id.signeditor_tv_text /*2131165795*/:
            case R.id.et_title /*2131165818*/:
                if (motionEvent.getAction() == 1) {
                    this.y.a();
                    E();
                    break;
                }
                break;
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public final void u() {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService("input_method");
        View currentFocus = getCurrentFocus();
        if (currentFocus != null) {
            inputMethodManager.hideSoftInputFromWindow(currentFocus.getWindowToken(), 2);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.h.a(java.lang.String, android.graphics.Bitmap, int, boolean):java.io.File
     arg types: [java.lang.String, android.graphics.Bitmap, int, int]
     candidates:
      com.immomo.momo.util.h.a(java.lang.String, java.lang.String, int, boolean):void
      com.immomo.momo.util.h.a(java.lang.String, android.graphics.Bitmap, int, boolean):java.io.File */
    public final void v() {
        boolean a2 = this.k.a();
        boolean a3 = this.j.a();
        boolean a4 = this.l.a();
        boolean a5 = this.m.a();
        boolean a6 = this.n.a();
        this.z = new b();
        switch (this.t) {
            case 0:
                this.U = v.a().a(this.p.getText().toString().trim(), this.o.getText().toString().trim(), new HashMap(), this.T, null, a3, a2, a4, a5, a6, this.z, this.u);
                break;
            case 1:
                this.U = v.a().a(this.p.getText().toString().trim(), this.o.getText().toString().trim(), new HashMap(), this.T, this.O, a3, a2, a4, a5, a6, this.z, this.u);
                break;
            case 2:
                for (Map.Entry entry : this.R.entrySet()) {
                    File file = (File) entry.getValue();
                    if (file == null || !file.exists()) {
                        this.A.post(new am(this));
                        throw new Exception();
                    }
                    String substring = file.getName().substring(0, file.getName().lastIndexOf("."));
                    entry.setValue(h.a(substring, android.support.v4.b.a.a(file, 720, 3000), 16, false));
                    this.S.put((String) entry.getKey(), substring);
                }
                this.U = v.a().a(this.p.getText().toString().trim(), this.o.getText().toString().trim(), this.R, this.T, null, a3, a2, a4, a5, a6, this.z, this.u);
                break;
        }
        new com.immomo.momo.service.ao().a(this.z);
        a(this.z);
        if (this.n.a()) {
            this.A.post(new an(this));
        }
        this.A.post(new ao(this));
    }

    public final m w() {
        return this.V;
    }

    public final void x() {
        if (this.C.getDatalist() != null) {
            this.I.setText(new StringBuilder().append(this.C.getDatalist().size()).toString());
            if (this.C.getDatalist().size() > 0) {
                this.F.setVisibility(0);
                findViewById(R.id.btn_localphoto).setEnabled(true);
                findViewById(R.id.btn_camera).setEnabled(true);
                findViewById(R.id.btn_emote).setEnabled(false);
                findViewById(R.id.layout_tip).setVisibility(8);
                this.I.setVisibility(0);
                RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) this.E.getLayoutParams();
                layoutParams.height = -2;
                this.E.setLayoutParams(layoutParams);
                return;
            }
            G();
            this.I.setVisibility(8);
            RelativeLayout.LayoutParams layoutParams2 = (RelativeLayout.LayoutParams) this.E.getLayoutParams();
            layoutParams2.height = (int) (266.0f * g.k());
            this.E.setLayoutParams(layoutParams2);
            F();
            return;
        }
        this.I.setVisibility(8);
        F();
    }
}
