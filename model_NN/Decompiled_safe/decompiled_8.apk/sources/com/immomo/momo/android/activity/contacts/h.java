package com.immomo.momo.android.activity.contacts;

import android.content.Intent;
import android.support.v4.b.a;
import com.immomo.momo.android.broadcast.d;
import com.immomo.momo.android.broadcast.j;
import com.immomo.momo.service.bean.bf;

final class h implements d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ a f1190a;

    h(a aVar) {
        this.f1190a = aVar;
    }

    public final void a(Intent intent) {
        bf b;
        if (intent.getAction().equals(j.c) || intent.getAction().equals(j.b)) {
            String stringExtra = intent.getStringExtra("key_momoid");
            if (!a.a((CharSequence) stringExtra)) {
                this.f1190a.P.c(new bf(stringExtra));
            }
        } else if (intent.getAction().equals(j.d) || intent.getAction().equals(j.f2353a)) {
            String stringExtra2 = intent.getStringExtra("key_momoid");
            if (!a.a((CharSequence) stringExtra2) && "both".equals(intent.getStringExtra("relation")) && (b = this.f1190a.V.b(stringExtra2)) != null && !this.f1190a.R.contains(b)) {
                this.f1190a.R.add(0, b);
                this.f1190a.P.notifyDataSetChanged();
            }
        }
    }
}
