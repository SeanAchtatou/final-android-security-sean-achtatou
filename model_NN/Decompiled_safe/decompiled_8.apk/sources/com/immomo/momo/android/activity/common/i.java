package com.immomo.momo.android.activity.common;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.protocol.a.w;
import java.util.ArrayList;

final class i extends d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ a f1122a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public i(a aVar, Context context) {
        super(context);
        this.f1122a = aVar;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        ArrayList arrayList = new ArrayList();
        int unused = w.a().a(arrayList, this.f1122a.P.getCount(), this.f1122a.N);
        return arrayList;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0026, code lost:
        if (r2.f1122a.P.getCount() >= com.immomo.momo.g.q().y) goto L_0x0028;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ void a(java.lang.Object r3) {
        /*
            r2 = this;
            java.util.List r3 = (java.util.List) r3
            super.a(r3)
            if (r3 == 0) goto L_0x0036
            int r0 = r3.size()
            if (r0 <= 0) goto L_0x0028
            com.immomo.momo.android.activity.common.a r0 = r2.f1122a
            com.immomo.momo.android.a.c r0 = r0.P
            r0.b(r3)
            com.immomo.momo.android.activity.common.a r0 = r2.f1122a
            com.immomo.momo.android.a.c r0 = r0.P
            int r0 = r0.getCount()
            com.immomo.momo.service.bean.bf r1 = com.immomo.momo.g.q()
            int r1 = r1.y
            if (r0 < r1) goto L_0x0031
        L_0x0028:
            com.immomo.momo.android.activity.common.a r0 = r2.f1122a
            com.immomo.momo.android.view.FriendRrefreshView r0 = r0.O
            r0.k()
        L_0x0031:
            com.immomo.momo.android.activity.common.a r0 = r2.f1122a
            r0.O()
        L_0x0036:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.immomo.momo.android.activity.common.i.a(java.lang.Object):void");
    }

    /* access modifiers changed from: protected */
    public final void b() {
        super.b();
        this.f1122a.W = null;
        this.f1122a.Q.e();
    }
}
