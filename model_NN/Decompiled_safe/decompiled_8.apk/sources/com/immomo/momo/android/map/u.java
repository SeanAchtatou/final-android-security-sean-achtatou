package com.immomo.momo.android.map;

final class u implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ GeoGoogleMapActivity f2515a;

    u(GeoGoogleMapActivity geoGoogleMapActivity) {
        this.f2515a = geoGoogleMapActivity;
    }

    public final void run() {
        try {
            this.f2515a.f = ((double) this.f2515a.c.getMapCenter().getLatitudeE6()) / 1000000.0d;
            this.f2515a.g = ((double) this.f2515a.c.getMapCenter().getLongitudeE6()) / 1000000.0d;
            this.f2515a.e.a((Object) ("result latlng: " + this.f2515a.f + "," + this.f2515a.g));
        } catch (Exception e) {
            this.f2515a.e.a((Throwable) e);
        }
    }
}
