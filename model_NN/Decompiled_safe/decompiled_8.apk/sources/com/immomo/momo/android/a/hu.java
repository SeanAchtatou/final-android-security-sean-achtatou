package com.immomo.momo.android.a;

import android.app.Activity;
import android.support.v4.b.a;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.g;
import com.immomo.momo.plugin.f.c;
import com.immomo.momo.util.e;
import com.immomo.momo.util.j;
import com.immomo.momo.util.m;
import java.util.ArrayList;

public final class hu extends a {
    /* access modifiers changed from: private */
    public m d = new m("WeiboAdapter");
    /* access modifiers changed from: private */
    public Activity e = null;

    public hu(Activity activity) {
        super(activity, new ArrayList());
        this.e = activity;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        hx hxVar;
        c cVar = (c) getItem(i);
        if (view == null) {
            view = g.o().inflate((int) R.layout.listitem_weibo, (ViewGroup) null);
            hx hxVar2 = new hx((byte) 0);
            view.setTag(R.id.tag_userlist_item, hxVar2);
            hxVar2.f867a = (TextView) view.findViewById(R.id.weiboitem_tv_textcontent);
            hxVar2.c = (ImageView) view.findViewById(R.id.weiboitem_iv_imagecontent);
            hxVar2.b = (TextView) view.findViewById(R.id.weiboitem_iv_posttime);
            hxVar2.g = view.findViewById(R.id.weiboitem_layout_repost);
            hxVar2.f = (TextView) hxVar2.g.findViewById(R.id.weiboitem_tv_name);
            hxVar2.e = (TextView) hxVar2.g.findViewById(R.id.weiboitem_tv_textcontent);
            hxVar2.d = (ImageView) hxVar2.g.findViewById(R.id.weiboitem_repost_iv_imagecontent);
            hxVar2.g.findViewById(R.id.weiboitem_repost_iv_posttime);
            hxVar = hxVar2;
        } else {
            hxVar = (hx) view.getTag(R.id.tag_userlist_item);
        }
        hxVar.c.setOnClickListener(new hv(this, i));
        hxVar.d.setOnClickListener(new hw(this, i));
        e.b(hxVar.f867a, a.a(cVar.b) ? "转发微博" : cVar.b, d());
        hxVar.c.setVisibility(8);
        hxVar.d.setVisibility(8);
        if (!com.immomo.a.a.f.a.a(cVar.c)) {
            hxVar.g.setVisibility(0);
            e.b(hxVar.e, a.a(cVar.d) ? PoiTypeDef.All : cVar.d, d());
            hxVar.f.setText("@" + cVar.c);
            if (cVar.h) {
                hxVar.d.setVisibility(0);
                j.a(cVar, hxVar.d, (ViewGroup) null, 5);
            }
        } else {
            hxVar.g.setVisibility(8);
            if (cVar.h) {
                hxVar.c.setVisibility(0);
                j.a(cVar, hxVar.c, (ViewGroup) null, 5);
            }
        }
        if (cVar.e != 0) {
            hxVar.b.setText(a.e(a.a(cVar.e)));
        } else {
            hxVar.b.setText("未知");
        }
        return view;
    }
}
