package com.immomo.momo.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import com.immomo.momo.R;
import com.immomo.momo.android.a.bk;
import com.immomo.momo.android.broadcast.d;
import com.immomo.momo.android.broadcast.q;
import com.immomo.momo.android.view.HandyListView;
import com.immomo.momo.g;
import com.immomo.momo.service.j;
import com.immomo.momo.service.m;
import java.util.List;

public class DraftPublishActivity extends ah implements d {
    bk h = null;
    private HandyListView i = null;
    private j j = null;
    private q k = null;

    public final void a(Intent intent) {
        m a2;
        int i2 = 0;
        if (q.b.equals(intent.getAction())) {
            m a3 = this.j.a(intent.getIntExtra("draftid", 0));
            if (a3 != null) {
                List a4 = this.h.a();
                while (i2 < a4.size()) {
                    if (a3.equals(a4.get(i2))) {
                        this.h.a(i2, a3);
                        return;
                    }
                    i2++;
                }
            }
        } else if (q.f2360a.equals(intent.getAction())) {
            int intExtra = intent.getIntExtra("draftid", 0);
            List a5 = this.h.a();
            while (true) {
                int i3 = i2;
                if (i3 >= a5.size()) {
                    return;
                }
                if (intExtra == ((m) a5.get(i3)).b) {
                    this.h.b(i3);
                    return;
                }
                i2 = i3 + 1;
            }
        } else if (q.c.equals(intent.getAction()) && (a2 = this.j.a(intent.getIntExtra("draftid", 0))) != null) {
            List a6 = this.h.a();
            while (i2 < a6.size()) {
                if (a2.equals(a6.get(i2))) {
                    this.h.a(i2, a2);
                    return;
                }
                i2++;
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        super.a(bundle);
        setContentView((int) R.layout.activity_draftlist);
        this.i = (HandyListView) findViewById(R.id.listview);
        this.i.addHeaderView(g.o().inflate((int) R.layout.listitem_blank, (ViewGroup) null));
        this.i.setOnItemClickListener(new ca(this));
        d();
    }

    /* access modifiers changed from: protected */
    public final void d() {
        this.j = j.a();
        this.h = new bk(this, this.j.b());
        this.i.setAdapter((ListAdapter) this.h);
        this.k = new q(this);
        this.k.a(this);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.k != null) {
            unregisterReceiver(this.k);
        }
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        d();
    }
}
