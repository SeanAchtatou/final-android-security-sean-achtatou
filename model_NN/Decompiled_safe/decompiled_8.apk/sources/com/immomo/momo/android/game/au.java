package com.immomo.momo.android.game;

import android.content.Context;
import android.content.Intent;
import com.immomo.momo.a.a;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.m;

final class au extends d {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public /* synthetic */ MDKTradeActivity f2415a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public au(MDKTradeActivity mDKTradeActivity, Context context) {
        super(context);
        this.f2415a = mDKTradeActivity;
    }

    /* access modifiers changed from: protected */
    public final Object a(Object... objArr) {
        m.a().b(this.f2415a.i, this.f2415a.j, this.f2415a.l);
        return null;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        v vVar = new v(this.f2415a, "正在获取支付信息...");
        vVar.setOnCancelListener(new av(this));
        this.f2415a.a(vVar);
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        String str;
        int i;
        if (exc instanceof a) {
            a aVar = (a) exc;
            i = aVar.f670a > 0 ? aVar.f670a : 40100;
            str = exc.getMessage();
        } else {
            str = "支付失败";
            i = 40100;
        }
        Intent intent = new Intent();
        intent.putExtra("emsg", str);
        intent.putExtra("product_id", this.f2415a.l.i);
        this.f2415a.setResult(i, intent);
        this.f2415a.finish();
    }

    /* access modifiers changed from: protected */
    public final void a(Object obj) {
        this.f2415a.h.removeAllViews();
        if (this.f2415a.l.f2405a != null) {
            MDKTradeActivity.e(this.f2415a);
        }
        if (this.f2415a.l.b != null) {
            MDKTradeActivity.f(this.f2415a);
        }
        if (this.f2415a.l.e != null) {
            MDKTradeActivity.g(this.f2415a);
        }
        if (this.f2415a.l.c != null) {
            MDKTradeActivity.h(this.f2415a);
        }
        if (this.f2415a.l.d != null) {
            MDKTradeActivity.i(this.f2415a);
        }
        if (this.f2415a.h.getChildCount() > 0) {
            this.f2415a.h.getChildAt(0).performClick();
            return;
        }
        Intent intent = new Intent();
        intent.putExtra("emsg", "支付失败，支付渠道已被关闭");
        intent.putExtra("product_id", this.f2415a.l.i);
        this.f2415a.setResult(30215, intent);
        this.f2415a.finish();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.f2415a.p();
    }
}
