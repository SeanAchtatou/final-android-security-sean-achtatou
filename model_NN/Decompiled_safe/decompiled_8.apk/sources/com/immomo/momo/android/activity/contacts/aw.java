package com.immomo.momo.android.activity.contacts;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.b.a;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.a.dl;
import com.immomo.momo.android.activity.lh;
import com.immomo.momo.android.activity.maintab.MaintabActivity;
import com.immomo.momo.android.broadcast.d;
import com.immomo.momo.android.broadcast.j;
import com.immomo.momo.android.broadcast.w;
import com.immomo.momo.android.view.FriendRrefreshView;
import com.immomo.momo.android.view.HeaderLayout;
import com.immomo.momo.android.view.a.o;
import com.immomo.momo.android.view.bl;
import com.immomo.momo.android.view.bu;
import com.immomo.momo.g;
import com.immomo.momo.service.aq;
import com.immomo.momo.util.k;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class aw extends lh implements View.OnClickListener, bl, bu {
    /* access modifiers changed from: private */
    public View O;
    /* access modifiers changed from: private */
    public EditText P;
    private View Q;
    /* access modifiers changed from: private */
    public FriendRrefreshView R;
    /* access modifiers changed from: private */
    public dl S;
    /* access modifiers changed from: private */
    public List T;
    private j U;
    private w V = null;
    /* access modifiers changed from: private */
    public int W = 0;
    /* access modifiers changed from: private */
    public int X = 0;
    /* access modifiers changed from: private */
    public Date Y = null;
    /* access modifiers changed from: private */
    public aq Z;
    /* access modifiers changed from: private */
    public bh aa;
    /* access modifiers changed from: private */
    public Handler ab = new Handler();
    /* access modifiers changed from: private */
    public int ac = 1;
    /* access modifiers changed from: private */
    public boolean ad = false;
    private d ae = new ax(this);
    private TextWatcher af = new ba(this);

    /* access modifiers changed from: private */
    public void O() {
        Q();
        this.W += this.S.getCount();
        if (this.S.getCount() < 400) {
            this.R.k();
        }
    }

    /* access modifiers changed from: private */
    public void P() {
        this.T = this.Z.b(this.ac);
    }

    /* access modifiers changed from: private */
    public void Q() {
        this.S = new dl(c(), this.T, this.R, false);
        this.R.setAdapter((ListAdapter) this.S);
    }

    static /* synthetic */ void q(aw awVar) {
        awVar.ad = true;
        awVar.P.getText().clear();
        awVar.O.setVisibility(4);
        awVar.ad = false;
    }

    /* access modifiers changed from: protected */
    public final int B() {
        return R.layout.layout_relation_friend;
    }

    /* access modifiers changed from: protected */
    public final void C() {
        this.R = (FriendRrefreshView) c((int) R.id.friends_listview);
        this.R.setEnableLoadMoreFoolter(false);
        this.R.k();
        this.R.setListPaddingBottom(MaintabActivity.h);
        View inflate = g.o().inflate((int) R.layout.listitem_relation_empty, (ViewGroup) null);
        ((TextView) inflate.findViewById(R.id.tv_tip)).setText((int) R.string.friendlist_empty_tip);
        this.R.a(inflate);
        this.O = this.R.getClearButton();
        this.P = this.R.getSearchEdit();
        this.Q = this.R.getSortView();
    }

    /* access modifiers changed from: protected */
    public final void E() {
        super.E();
        this.R.o();
        this.ad = true;
        this.P.getText().clear();
        if (this.O.isShown()) {
            this.O.setVisibility(4);
        }
        this.ad = false;
    }

    public final void S() {
        super.S();
        if (this.Y == null && (this.aa == null || !this.aa.isCancelled())) {
            this.R.l();
        } else if (this.Y == null) {
        } else {
            if (this.aa != null && this.aa.isCancelled()) {
                return;
            }
            if (new Date().getTime() - this.Y.getTime() > 900000 || this.T.size() <= 0) {
                this.R.l();
            }
        }
    }

    public final void a(Context context, HeaderLayout headerLayout) {
    }

    public final void ae() {
        super.ae();
        new k("PI", "P62").e();
    }

    public final void ag() {
        super.ag();
        new k("PO", "P62").e();
    }

    public final void ai() {
        this.R.i();
    }

    public final void aj() {
        Date date;
        P();
        try {
            String str = (String) this.N.b("lasttime_friends_success", PoiTypeDef.All);
            date = !a.a(str) ? a.k(str) : null;
        } catch (Exception e) {
            date = null;
        }
        this.R.setLastFlushTime(date);
        try {
            String str2 = (String) this.N.b("lasttime_friends", PoiTypeDef.All);
            if (!a.a((CharSequence) str2)) {
                this.Y = a.k(str2);
            }
        } catch (Exception e2) {
        }
        O();
        if (this.S.getCount() <= 0) {
            this.R.l();
        }
    }

    public final void b_() {
        if (this.aa != null && !this.aa.isCancelled()) {
            this.aa.cancel(true);
        }
        this.aa = new bh(this, c());
        this.aa.execute(new Object[0]);
    }

    /* access modifiers changed from: protected */
    public final void g(Bundle bundle) {
        this.Z = new aq();
        this.V = new w(c());
        this.V.a(this.ae);
        try {
            this.ac = ((Integer) this.N.b("sorttype_realtion_friend", 2)).intValue();
        } catch (Exception e) {
        }
        this.O.setOnClickListener(new bb(this));
        this.P.addTextChangedListener(this.af);
        this.R.setOnPullToRefreshListener$42b903f6(this);
        this.R.setOnCancelListener$135502(new bi(this, (byte) 0));
        this.R.setOnItemClickListener(new bc(this));
        this.Q.setOnClickListener(this);
        aj();
        this.U = new j(c());
        this.U.a(new bd(this));
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.layout_sort /*2131166400*/:
                o oVar = new o(c());
                String[] stringArray = d().getStringArray(R.array.order_friend_list);
                ArrayList arrayList = new ArrayList();
                int i = 0;
                while (i < stringArray.length) {
                    bj bjVar = new bj((byte) 0);
                    bjVar.f1165a = stringArray[i];
                    bjVar.b = i == this.ac;
                    arrayList.add(bjVar);
                    i++;
                }
                oVar.a(new bg(c(), arrayList));
                oVar.setTitle((int) R.string.header_order);
                oVar.a();
                oVar.a(new be(this));
                oVar.setOnCancelListener(new bf());
                oVar.show();
                return;
            default:
                return;
        }
    }

    public final void p() {
        super.p();
        if (this.U != null) {
            a(this.U);
            this.U = null;
        }
        if (this.V != null) {
            a(this.V);
            this.V = null;
        }
        if (this.aa != null && !this.aa.isCancelled()) {
            this.aa.cancel(true);
            this.aa = null;
        }
    }

    public final void u() {
    }
}
