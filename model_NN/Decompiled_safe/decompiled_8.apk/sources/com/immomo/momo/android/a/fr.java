package com.immomo.momo.android.a;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.immomo.momo.android.view.EmoteTextView;

final class fr {

    /* renamed from: a  reason: collision with root package name */
    public ImageView f825a;
    public TextView b;
    public TextView c;
    public TextView d;
    public TextView e;
    public EmoteTextView f;
    public ImageView g;
    public View h;
    public ImageView i;
    public ImageView j;
    public ImageView k;
    public ImageView l;
    public ImageView m;
    public ImageView n;
    public ImageView o;
    public ImageView p;
    public View q;

    private fr() {
    }

    /* synthetic */ fr(byte b2) {
        this();
    }
}
