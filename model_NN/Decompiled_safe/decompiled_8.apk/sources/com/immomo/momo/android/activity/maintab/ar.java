package com.immomo.momo.android.activity.maintab;

import android.content.Intent;
import android.support.v4.b.a;
import com.immomo.momo.android.broadcast.d;
import com.immomo.momo.android.broadcast.w;
import com.immomo.momo.service.bean.bf;

final class ar implements d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ aq f1842a;

    ar(aq aqVar) {
        this.f1842a = aqVar;
    }

    public final void a(Intent intent) {
        if (intent.getAction().equals(w.f2366a)) {
            String str = intent.getExtras() != null ? (String) intent.getExtras().get("momoid") : null;
            if (!a.a((CharSequence) str) && this.f1842a.P.containsKey(str)) {
                this.f1842a.S.a((bf) this.f1842a.P.get(str), str);
                this.f1842a.Q.notifyDataSetChanged();
            }
        } else if (intent.getAction().equals(w.c)) {
            this.f1842a.P();
        }
    }
}
