package com.immomo.momo.android.activity.maintab;

import android.content.Intent;
import android.support.v4.b.a;
import com.immomo.momo.android.broadcast.d;
import com.immomo.momo.android.broadcast.w;

final class bp implements d {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ bh f1864a;

    bp(bh bhVar) {
        this.f1864a = bhVar;
    }

    public final void a(Intent intent) {
        if (w.f2366a.equals(intent.getAction())) {
            String str = intent.getExtras() != null ? (String) intent.getExtras().get("momoid") : null;
            if (!a.a((CharSequence) str) && this.f1864a.X != null) {
                new Thread(new bq(this, str)).start();
            }
        }
    }
}
