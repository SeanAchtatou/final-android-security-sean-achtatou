package com.immomo.momo.android.b;

import com.immomo.momo.util.ak;
import mm.purchasesdk.PurchaseCode;

final class ab implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ Object f2314a;
    private final /* synthetic */ p b;
    private final /* synthetic */ p c;
    private final /* synthetic */ p d;
    private final /* synthetic */ p e;
    private final /* synthetic */ ak f;

    ab(Object obj, p pVar, p pVar2, p pVar3, p pVar4, ak akVar) {
        this.f2314a = obj;
        this.b = pVar;
        this.c = pVar2;
        this.d = pVar3;
        this.e = pVar4;
        this.f = akVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, int]
     candidates:
      com.immomo.momo.util.ak.a(android.content.Context, java.lang.String):com.immomo.momo.util.ak
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Integer):int
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Long):long
      com.immomo.momo.util.ak.a(java.lang.String, int):void
      com.immomo.momo.util.ak.a(java.lang.String, long):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.String):void
      com.immomo.momo.util.ak.a(java.lang.String, boolean):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Boolean):boolean
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void */
    public final void run() {
        synchronized (this.f2314a) {
            try {
                this.f2314a.wait(z.d() ? 60000 : 15000);
                z.c();
                if (!this.b.b() && !this.c.b() && !this.d.b()) {
                    this.e.a(null, 0, PurchaseCode.QUERY_OK, PurchaseCode.LOADCHANNEL_ERR);
                    this.f.a("momolocate_locater_type", (Object) 203);
                }
            } catch (Exception e2) {
                z.d.a((Throwable) e2);
                this.e.a(null, 0, PurchaseCode.QUERY_OK, PurchaseCode.LOADCHANNEL_ERR);
            }
        }
    }
}
