package com.immomo.momo.android.activity.group;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.b.a;
import android.view.View;
import com.immomo.momo.android.activity.OtherProfileActivity;

final class cf implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ GroupPartyActivity f1579a;

    cf(GroupPartyActivity groupPartyActivity) {
        this.f1579a = groupPartyActivity;
    }

    public final void onClick(View view) {
        Bundle bundle = (Bundle) view.getTag();
        if (bundle != null && !a.a((CharSequence) bundle.getString("momoid"))) {
            Intent intent = new Intent(this.f1579a.getApplicationContext(), OtherProfileActivity.class);
            intent.putExtra("tag", "local");
            intent.putExtra("momoid", bundle.getString("momoid"));
            this.f1579a.startActivity(intent);
        }
    }
}
