package com.immomo.momo.android.map;

import android.graphics.Bitmap;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.b.a;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.amap.mapapi.core.GeoPoint;
import com.amap.mapapi.location.LocationManagerProxy;
import com.amap.mapapi.map.MapView;
import com.amap.mapapi.map.MyLocationOverlay;
import com.amap.mapapi.map.RouteMessageHandler;
import com.amap.mapapi.map.RouteOverlay;
import com.amap.mapapi.route.Route;
import com.immomo.momo.R;
import com.immomo.momo.android.b.z;
import com.immomo.momo.android.view.HeaderLayout;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.android.view.bi;
import com.immomo.momo.g;
import com.immomo.momo.util.ao;
import com.immomo.momo.util.m;
import java.util.List;

public class AMapActivity extends i implements RouteMessageHandler {
    List b = null;
    public int c = 10;
    private MyLocationOverlay f = null;
    /* access modifiers changed from: private */
    public GeoPoint g = null;
    private Location h = null;
    /* access modifiers changed from: private */
    public MapView i = null;
    private boolean j = false;
    /* access modifiers changed from: private */
    public m k = new m(getClass().getSimpleName());
    private String l = null;
    private String m = null;
    /* access modifiers changed from: private */
    public boolean n = false;
    /* access modifiers changed from: private */
    public RouteOverlay o = null;
    private Handler p = new a(this);

    static /* synthetic */ void a(AMapActivity aMapActivity, GeoPoint geoPoint) {
        Route.FromAndTo fromAndTo = new Route.FromAndTo(geoPoint, aMapActivity.g);
        try {
            aMapActivity.p.post(new g(aMapActivity));
            aMapActivity.b = Route.calculateRoute(aMapActivity, fromAndTo, aMapActivity.c);
            aMapActivity.p.sendEmptyMessage(100);
        } catch (Exception e) {
            aMapActivity.k.a((Throwable) e);
            aMapActivity.b();
            aMapActivity.a((int) R.string.errormsg_route_failed);
        }
    }

    static /* synthetic */ Bitmap e(AMapActivity aMapActivity) {
        View inflate = LayoutInflater.from(aMapActivity).inflate((int) R.layout.map_addressinfo, (ViewGroup) null);
        if (!a.a((CharSequence) aMapActivity.l)) {
            ((TextView) inflate.findViewById(R.id.mapview_title)).setText(aMapActivity.l);
        } else {
            ((TextView) inflate.findViewById(R.id.mapview_title)).setVisibility(8);
        }
        if (!a.a((CharSequence) aMapActivity.m)) {
            ((TextView) inflate.findViewById(R.id.mapview_info)).setText(aMapActivity.m);
        } else {
            ((TextView) inflate.findViewById(R.id.mapview_info)).setVisibility(8);
        }
        inflate.measure(View.MeasureSpec.makeMeasureSpec(0, 0), View.MeasureSpec.makeMeasureSpec(0, 0));
        inflate.layout(0, 0, inflate.getMeasuredWidth(), inflate.getMeasuredHeight());
        inflate.buildDrawingCache();
        return inflate.getDrawingCache();
    }

    static /* synthetic */ void g(AMapActivity aMapActivity) {
        v vVar = new v(aMapActivity);
        vVar.setCancelable(true);
        vVar.a("正在获取您的当前位置......");
        aMapActivity.a(vVar);
        e eVar = new e(aMapActivity);
        try {
            g.R();
            z.a(eVar);
        } catch (Exception e) {
            aMapActivity.k.a((Throwable) e);
            ao.g(R.string.errormsg_location_failed);
        }
    }

    /* access modifiers changed from: protected */
    public boolean isRouteDisplayed() {
        return false;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        double d = -1.0d;
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_baidumap);
        Bundle extras = getIntent().getExtras();
        if (extras == null) {
            ao.e(R.string.map_location_error);
        } else {
            this.j = extras.getBoolean("is_receive", false);
            double doubleValue = extras.get("latitude") == null ? -1.0d : Double.valueOf(extras.get("latitude").toString()).doubleValue();
            if (extras.get("longitude") != null) {
                d = Double.valueOf(extras.get("longitude").toString()).doubleValue();
            }
            this.h = new Location(LocationManagerProxy.NETWORK_PROVIDER);
            this.h.setLatitude(doubleValue);
            this.h.setLongitude(d);
            this.n = extras.getBoolean("is_show_add", false);
            if (this.n) {
                this.l = extras.getString("add_title");
                this.m = extras.getString("add_info");
            }
            if (!z.a(this.h)) {
                ao.e(R.string.map_location_error);
                finish();
            } else {
                this.k.a((Object) ("src lat=" + doubleValue + ", lnt=" + d));
                this.g = new GeoPoint((int) (doubleValue * 1000000.0d), (int) (d * 1000000.0d));
                if (this.g != null) {
                    this.k.a((Object) ("baidu lat=" + (((double) this.g.getLatitudeE6()) / 1000000.0d) + ", lnt=" + (((double) this.g.getLongitudeE6()) / 1000000.0d)));
                }
            }
        }
        this.i = (MapView) findViewById(R.id.mapview);
        a(this.i);
        HeaderLayout headerLayout = (HeaderLayout) findViewById(R.id.layout_header);
        headerLayout.setTitleText((int) R.string.map);
        headerLayout.a(new bi(this).a((int) R.drawable.ic_topbar_locate), new b(this));
        if (this.j) {
            headerLayout.a(new bi(this).a("导航"), new c(this));
        }
        if (this.g != null) {
            this.i.getController().animateTo(this.g);
            this.i.getController().setCenter(this.g);
        }
        this.f = new h(this, this, this.i);
        this.i.getOverlays().add(this.f);
    }

    public void onDrag(MapView mapView, RouteOverlay routeOverlay, int i2, GeoPoint geoPoint) {
    }

    public void onDragBegin(MapView mapView, RouteOverlay routeOverlay, int i2, GeoPoint geoPoint) {
    }

    public void onDragEnd(MapView mapView, RouteOverlay routeOverlay, int i2, GeoPoint geoPoint) {
        b();
    }

    public boolean onRouteEvent(MapView mapView, RouteOverlay routeOverlay, int i2, int i3) {
        return false;
    }
}
