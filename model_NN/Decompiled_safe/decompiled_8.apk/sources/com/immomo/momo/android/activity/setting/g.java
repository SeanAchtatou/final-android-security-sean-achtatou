package com.immomo.momo.android.activity.setting;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;

final class g extends d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ CommunityBindActivity f2130a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public g(CommunityBindActivity communityBindActivity, Context context) {
        super(context);
        this.f2130a = communityBindActivity;
        if (communityBindActivity.p != null) {
            communityBindActivity.p.cancel(true);
        }
        communityBindActivity.p = this;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        return com.immomo.momo.protocol.a.d.b("renren");
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.f2130a.r = new v(this.f2130a, "正在请求数据，请稍候...");
        this.f2130a.r.setOnCancelListener(new h(this));
        this.f2130a.a(this.f2130a.r);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        String[] strArr = (String[]) obj;
        if (strArr == null || strArr.length != 3) {
            a("绑定失败");
            return;
        }
        this.f2130a.h = strArr[0];
        this.f2130a.i = strArr[1];
        this.f2130a.j = strArr[2];
        CommunityBindActivity.c(this.f2130a);
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.f2130a.p();
    }
}
