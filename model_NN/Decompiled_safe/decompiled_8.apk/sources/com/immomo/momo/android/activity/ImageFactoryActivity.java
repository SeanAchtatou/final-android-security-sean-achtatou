package com.immomo.momo.android.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.b.a;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.plugin.cropimage.CropImageActivity;
import com.immomo.momo.android.view.HeaderLayout;
import com.immomo.momo.android.view.bi;

public class ImageFactoryActivity extends lg {
    private static fr b = null;

    public static fr a() {
        return b;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_imagefactory);
        b = new fr(this);
        this.f1811a.a((Object) "onCreate~~~~~~~~~~~~~");
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        if (extras != null) {
            b.q = (Bitmap) extras.getParcelable("data");
            b.e = extras.getInt("aspectX");
            b.f = extras.getInt("aspectY");
            b.g = extras.getInt("outputX");
            b.h = extras.getInt("outputY");
            b.i = extras.getBoolean("scale", true);
            b.j = extras.getBoolean("scaleUpIfNeeded", true);
            b.k = extras.getInt("saveQuality", 85);
            b.r = (String) extras.get("outputFilePath");
            b.l = extras.getInt("minsize", 0);
            b.m = extras.getInt("maxwidth", 960);
            b.n = extras.getInt("maxheight", 960);
            if (b.l < 0) {
                b.l = 0;
            }
            b.o = intent.getData();
            b.s = (String) extras.get("process_model");
        }
        if (a.a((CharSequence) b.s)) {
            b.s = "crop_and_filter";
        }
        b.p = b.o;
        if (!(bundle == null || bundle == null || !bundle.getBoolean("from_saveinstance", false))) {
            b.e = bundle.getInt("aspectX");
            b.f = bundle.getInt("aspectY");
            b.g = bundle.getInt("mOutputX");
            b.h = bundle.getInt("mOutputY");
            b.i = bundle.getBoolean("scale");
            b.j = bundle.getBoolean("scaleUp");
            b.k = bundle.getInt("saveQuality");
            b.l = bundle.getInt("minPix");
            b.m = bundle.getInt("maxWidth");
            b.n = bundle.getInt("maxHeight");
            String string = bundle.getString("originalBitmapUri");
            if (!a.a((CharSequence) string)) {
                b.o = Uri.parse(string);
            }
            String string2 = bundle.getString("filterImageUri");
            if (!a.a((CharSequence) string2)) {
                b.p = Uri.parse(string2);
            }
            b.r = bundle.getString("outputFilePath");
        }
        b.f1500a = (HeaderLayout) findViewById(R.id.layout_header);
        b.b = new bi(this).a((int) R.drawable.ic_topbar_rotation);
        b.f1500a.a(b.b, null);
        b.c = (Button) findViewById(R.id.imagefactory_btn1);
        b.d = (Button) findViewById(R.id.imagefactory_btn2);
        a(CropImageActivity.class, FilterActivity.class);
        if ("filter".equals(b.s)) {
            this.f1811a.a((Object) ("~~~~~~~~~~~~~~~~~imageFactoryHandler.filterImageUri=" + b.p));
            a(1);
            return;
        }
        Animation loadAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.push_left_in);
        loadAnimation.setInterpolator(new LinearInterpolator());
        loadAnimation.setDuration(500);
        Animation loadAnimation2 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.push_left_out);
        loadAnimation2.setInterpolator(new LinearInterpolator());
        loadAnimation2.setDuration(500);
        a(loadAnimation, loadAnimation2);
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        if (b != null) {
            bundle.putBoolean("from_saveinstance", true);
            bundle.putInt("aspectX", b.e);
            bundle.putInt("aspectY", b.f);
            bundle.putInt("mOutputX", b.g);
            bundle.putInt("mOutputY", b.h);
            bundle.putBoolean("scale", b.i);
            bundle.putBoolean("scaleUp", b.j);
            bundle.putInt("saveQuality", b.k);
            bundle.putInt("minPix", b.l);
            bundle.putInt("maxWidth", b.m);
            bundle.putInt("maxHeight", b.n);
            bundle.putString("originalBitmapUri", b.o != null ? b.o.getPath() : PoiTypeDef.All);
            bundle.putString("filterImageUri", b.p != null ? b.p.getPath() : PoiTypeDef.All);
            bundle.putString("outputFilePath", b.r);
        }
    }
}
