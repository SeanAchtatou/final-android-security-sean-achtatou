package com.immomo.momo.android.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.util.j;
import java.util.List;

public class WelcomeView extends LinearLayout {

    /* renamed from: a  reason: collision with root package name */
    private static int f2661a = 10;
    private static int b = 4;
    private static int c;
    private static int d;
    private static int e;
    private LinearLayout[] f;
    private RelativeLayout[] g;

    public WelcomeView(Context context) {
        super(context);
        a();
    }

    public WelcomeView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a();
    }

    private void a() {
        setOrientation(1);
        postDelayed(new dw(this), 10);
    }

    /* access modifiers changed from: private */
    public void b() {
        if (c == 0) {
            getCount();
        }
        if (c > 0) {
            this.g = new RelativeLayout[(b * c)];
            this.f = new LinearLayout[c];
            for (int i = 0; i < c; i++) {
                this.f[i] = new LinearLayout(getContext());
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, e);
                layoutParams.topMargin = (int) (((float) f2661a) * g.k());
                addView(this.f[i], layoutParams);
                for (int i2 = 0; i2 < b; i2++) {
                    LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(d, e);
                    layoutParams2.leftMargin = (int) (((float) f2661a) * g.k());
                    this.g[(b * i) + i2] = getImageLayout();
                    this.f[i].addView(this.g[(b * i) + i2], layoutParams2);
                }
            }
        }
    }

    public static int getCount() {
        int J = ((int) ((((float) g.J()) - (20.0f * g.k())) - ((((float) f2661a) * g.k()) * 3.0f))) / b;
        d = J;
        e = J;
        int L = (int) (((float) g.L()) / (((float) e) + (((float) f2661a) * g.k())));
        c = L;
        return L * b;
    }

    private RelativeLayout getImageLayout() {
        RelativeLayout relativeLayout = (RelativeLayout) inflate(getContext(), R.layout.include_welcome_photo, null);
        relativeLayout.setLayoutParams(new RelativeLayout.LayoutParams(d, e));
        return relativeLayout;
    }

    public final void a(List list) {
        if (this.g == null || this.g.length <= 0) {
            b();
        }
        int i = 0;
        while (i < list.size() && i <= this.g.length) {
            bf bfVar = (bf) list.get(i);
            ImageView imageView = (ImageView) this.g[i].findViewById(R.id.welcomephoto_imge_sex);
            j.b(bfVar, (ImageView) this.g[i].findViewById(R.id.welcomephoto_img_photo), null, 3, true);
            ((TextView) this.g[i].findViewById(R.id.welcomephoto_tv_position)).setText(bfVar.Z);
            this.g[i].findViewById(R.id.welcomephoto_layout_info).setVisibility(0);
            if ("F".equals(bfVar.H)) {
                imageView.setImageResource(R.drawable.ic_user_famale2);
            } else if ("M".equals(bfVar.H)) {
                imageView.setImageResource(R.drawable.ic_user_male2);
            }
            i++;
        }
    }
}
