package com.immomo.momo.android.activity.plugin;

import android.os.Handler;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.c.a.c;
import com.immomo.momo.R;
import com.immomo.momo.plugin.c.b;
import com.immomo.momo.plugin.e.e;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class bh implements c {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ FacebookActivity f2062a;
    private String b = PoiTypeDef.All;

    public bh(FacebookActivity facebookActivity, String str) {
        this.f2062a = facebookActivity;
        this.b = str;
    }

    public final void a() {
        this.f2062a.findViewById(R.id.process_layout_root).setVisibility(8);
    }

    public final void a(String str) {
        this.f2062a.h.b((Object) ("facebookAct query resule : " + str));
        if (this.b.equals("request_type_get_profile")) {
            try {
                this.f2062a.k = new e();
                JSONObject jSONObject = new JSONObject(str.substring(1, str.length() - 1));
                this.f2062a.k.b = jSONObject.optString("name");
                this.f2062a.k.f = jSONObject.optString("about_me");
                this.f2062a.k.g = jSONObject.optString("profile_url");
                this.f2062a.k.d = jSONObject.optString("hometown_location");
                this.f2062a.k.k = jSONObject.optInt("friend_count");
                this.f2062a.k.l = jSONObject.optInt("likes_count");
                this.f2062a.k.h = jSONObject.optString("pic_small");
                this.f2062a.x.post(new bi(this));
            } catch (JSONException e) {
                this.f2062a.h.b((Object) e.getMessage());
            }
        } else if (this.b.equals("request_type_get_message")) {
            try {
                List a2 = b.a(new JSONArray(str));
                if (a2 != null) {
                    Iterator it = a2.iterator();
                    while (it.hasNext()) {
                        it.next();
                        e unused = this.f2062a.k;
                    }
                    new Handler(this.f2062a.getMainLooper()).post(new bj(this, a2));
                }
            } catch (JSONException e2) {
                this.f2062a.h.a((Throwable) e2);
            }
        }
    }

    public final void b() {
        this.f2062a.findViewById(R.id.process_layout_root).setVisibility(8);
    }
}
