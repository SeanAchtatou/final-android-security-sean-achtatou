package com.immomo.momo.android.a;

import android.view.View;
import com.immomo.momo.R;
import com.immomo.momo.android.view.a.at;

final class bb implements View.OnClickListener {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public /* synthetic */ ax f731a;
    private final /* synthetic */ bg b;
    private final /* synthetic */ int c;

    bb(ax axVar, bg bgVar, int i) {
        this.f731a = axVar;
        this.b = bgVar;
        this.c = i;
    }

    public final void onClick(View view) {
        at atVar = new at(this.f731a.d, this.b.r, this.f731a.d.getResources().getStringArray(R.array.discussmemberlist_remove_items));
        atVar.a(new bc(this, this.c));
        atVar.d();
    }
}
