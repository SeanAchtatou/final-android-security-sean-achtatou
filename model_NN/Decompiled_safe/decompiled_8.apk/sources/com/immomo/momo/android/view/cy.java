package com.immomo.momo.android.view;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import com.immomo.momo.R;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import mm.purchasesdk.PurchaseCode;

final class cy implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ RomaAnimarionView f2770a;

    cy(RomaAnimarionView romaAnimarionView) {
        this.f2770a = romaAnimarionView;
    }

    public final void run() {
        if (this.f2770a.b != null) {
            cz unused = this.f2770a.b;
        }
        try {
            this.f2770a.f = BitmapFactory.decodeResource(this.f2770a.getResources(), R.drawable.bg_roma_sky);
            Random random = new Random();
            for (int i = 0; i < RomaAnimarionView.f2652a.length / 2; i++) {
                int nextInt = random.nextInt(RomaAnimarionView.f2652a.length);
                if (nextInt != i) {
                    int i2 = RomaAnimarionView.f2652a[nextInt];
                    RomaAnimarionView.f2652a[nextInt] = RomaAnimarionView.f2652a[i];
                    RomaAnimarionView.f2652a[i] = i2;
                }
            }
            Bitmap[] bitmapArr = new Bitmap[RomaAnimarionView.f2652a.length];
            int i3 = 0;
            int i4 = 0;
            for (int i5 = 0; i5 < RomaAnimarionView.f2652a.length; i5++) {
                Bitmap decodeResource = BitmapFactory.decodeResource(this.f2770a.getResources(), RomaAnimarionView.f2652a[i5]);
                bitmapArr[i5] = decodeResource;
                i4 += decodeResource.getWidth() + PurchaseCode.LOADCHANNEL_ERR;
                if (decodeResource.getHeight() > i3) {
                    i3 = decodeResource.getHeight();
                }
            }
            Bitmap createBitmap = Bitmap.createBitmap(i4, i3, Bitmap.Config.ARGB_4444);
            Paint paint = new Paint();
            Canvas canvas = new Canvas(createBitmap);
            int i6 = 0;
            for (Bitmap bitmap : bitmapArr) {
                canvas.drawBitmap(bitmap, (float) i6, 0.0f, paint);
                i6 += bitmap.getWidth() + PurchaseCode.LOADCHANNEL_ERR;
                bitmap.recycle();
            }
            this.f2770a.g = createBitmap;
            RomaAnimarionView romaAnimarionView = this.f2770a;
            if (this.f2770a.g != null) {
                Bitmap unused2 = this.f2770a.f;
            }
            RomaAnimarionView.f();
            if (this.f2770a.b != null) {
                this.f2770a.b.a(true);
            }
            this.f2770a.m.a((Object) "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~surface initresource");
            int max = Math.max(80 / Runtime.getRuntime().availableProcessors(), 20);
            while (this.f2770a.d && !this.f2770a.e) {
                this.f2770a.k.lock();
                try {
                    this.f2770a.d();
                    if (this.f2770a.j) {
                        this.f2770a.l.await((long) max, TimeUnit.MILLISECONDS);
                    } else {
                        this.f2770a.l.await(10000, TimeUnit.MILLISECONDS);
                    }
                } catch (InterruptedException e) {
                    this.f2770a.m.a((Throwable) e);
                }
                this.f2770a.k.unlock();
            }
        } catch (OutOfMemoryError e2) {
            this.f2770a.m.a((Throwable) e2);
            if (this.f2770a.b != null) {
                this.f2770a.b.a(false);
            }
        } catch (Exception e3) {
            this.f2770a.m.a((Throwable) e3);
            if (this.f2770a.b != null) {
                this.f2770a.b.a(false);
            }
        }
    }
}
