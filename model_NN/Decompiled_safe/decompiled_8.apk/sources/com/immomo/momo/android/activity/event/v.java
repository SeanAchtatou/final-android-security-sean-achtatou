package com.immomo.momo.android.activity.event;

import android.content.Context;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.android.c.b;
import com.immomo.momo.protocol.a.a.n;
import com.immomo.momo.protocol.a.j;
import com.immomo.momo.service.bean.aa;
import com.immomo.momo.service.bean.ae;
import com.immomo.momo.service.r;
import java.util.List;

final class v extends b {
    private String c;
    private String d;
    private String e;
    private String f;
    private String g;
    private int h;
    private int i;
    private /* synthetic */ EventFeedProfileActivity j;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public v(EventFeedProfileActivity eventFeedProfileActivity, Context context, String str, String str2, int i2, int i3, String str3, String str4, String str5) {
        super(context);
        this.j = eventFeedProfileActivity;
        this.c = str;
        this.d = str2;
        this.h = i2;
        this.i = i3;
        this.e = str3;
        this.f = str4;
        this.g = str5;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        this.e = this.e.replaceAll("\n{2,}", "\n");
        n a2 = j.a().a(this.c, this.d, this.h, this.i, this.e, this.f, this.g);
        r unused = this.j.n;
        String a3 = this.j.j;
        ae aeVar = a2.f2893a;
        List b = r.b(a3);
        b.add(0, aeVar);
        r.b(a3, b);
        return a2;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
        this.j.x();
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        n nVar = (n) obj;
        nVar.f2893a.f2985a = this.j.f;
        EventFeedProfileActivity eventFeedProfileActivity = this.j;
        aa i2 = this.j.l;
        int i3 = i2.g + 1;
        i2.g = i3;
        EventFeedProfileActivity.a(eventFeedProfileActivity, i3);
        this.j.m.b(0, nVar.f2893a);
        this.j.Z.setText(PoiTypeDef.All);
        this.j.ai = true;
        this.j.x();
        this.j.v();
        this.j.aj = null;
        this.j.ab.setText(PoiTypeDef.All);
        this.j.ab.setVisibility(8);
        super.a(nVar);
    }
}
