package com.immomo.momo.android.activity.tieba;

import android.content.Intent;
import android.view.View;

final class db implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ TiebaCategoryActivity f2230a;

    db(TiebaCategoryActivity tiebaCategoryActivity) {
        this.f2230a = tiebaCategoryActivity;
    }

    public final void onClick(View view) {
        this.f2230a.startActivity(new Intent(this.f2230a, TiebaSearchActivity.class));
    }
}
