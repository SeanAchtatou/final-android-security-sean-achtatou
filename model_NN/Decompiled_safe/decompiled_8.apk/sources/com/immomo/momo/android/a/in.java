package com.immomo.momo.android.a;

import android.content.Context;
import android.support.v4.b.a;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.c.d;
import com.immomo.momo.util.j;
import java.util.List;

public final class in extends a {
    private ListView d;

    public in(Context context, List list, ListView listView) {
        super(context, list);
        this.d = listView;
    }

    private static String d(int i) {
        return i > 9999999 ? String.valueOf(i / 10000000) + "千万" : i > 9999 ? String.valueOf(i / 10000) + "万" : new StringBuilder(String.valueOf(i)).toString();
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        d dVar = (d) getItem(i);
        if (view == null) {
            io ioVar = new io((byte) 0);
            view = g.o().inflate((int) R.layout.listitem_tieba, (ViewGroup) null);
            ioVar.f881a = (ImageView) view.findViewById(R.id.teibalist_item_iv_face);
            ioVar.f = (ImageView) view.findViewById(R.id.teibalist_item_iv_hot);
            ioVar.e = (ImageView) view.findViewById(R.id.tiebalist_item_iv_recommend);
            ioVar.b = (TextView) view.findViewById(R.id.tiebalist_item_tv_name);
            ioVar.d = (TextView) view.findViewById(R.id.tiebalist_item_tv_sign);
            ioVar.c = (TextView) view.findViewById(R.id.tiebalist_item_tv_attribute);
            view.setTag(R.id.tag_userlist_item, ioVar);
        }
        io ioVar2 = (io) view.getTag(R.id.tag_userlist_item);
        if (a.a((CharSequence) dVar.b)) {
            ioVar2.b.setText(dVar.f3019a);
        } else {
            ioVar2.b.setText(dVar.b);
        }
        if (dVar.n) {
            ioVar2.f.setVisibility(0);
        } else {
            ioVar2.f.setVisibility(8);
        }
        if (dVar.o) {
            ioVar2.e.setVisibility(0);
        } else {
            ioVar2.e.setVisibility(8);
        }
        if (dVar.i > 0) {
            ioVar2.c.setText("成员 " + d(dVar.g) + " | 今日话题 " + d(dVar.i));
        } else {
            ioVar2.c.setText("成员 " + d(dVar.g));
        }
        if (dVar.j != null && dVar.j.length() > 70) {
            dVar.j = dVar.j.substring(0, 70);
        }
        ioVar2.d.setText(dVar.j);
        j.b(dVar, ioVar2.f881a, this.d, 3, false);
        return view;
    }
}
