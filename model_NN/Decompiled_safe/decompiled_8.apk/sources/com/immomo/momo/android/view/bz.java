package com.immomo.momo.android.view;

import android.widget.AbsListView;
import android.widget.ExpandableListView;
import com.immomo.momo.g;

final class bz implements AbsListView.OnScrollListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ MomoUnrefreshExpandableListView f2756a;

    bz(MomoUnrefreshExpandableListView momoUnrefreshExpandableListView) {
        this.f2756a = momoUnrefreshExpandableListView;
    }

    public final void onScroll(AbsListView absListView, int i, int i2, int i3) {
        if (g.p()) {
            long expandableListPosition = this.f2756a.getExpandableListPosition(i);
            this.f2756a.a(ExpandableListView.getPackedPositionGroup(expandableListPosition), ExpandableListView.getPackedPositionChild(expandableListPosition));
        }
    }

    public final void onScrollStateChanged(AbsListView absListView, int i) {
    }
}
