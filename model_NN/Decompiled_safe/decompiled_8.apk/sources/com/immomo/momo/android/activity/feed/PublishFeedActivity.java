package com.immomo.momo.android.activity.feed;

import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.a.a.f.b;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.ImageFactoryActivity;
import com.immomo.momo.android.activity.MulImagePickerActivity;
import com.immomo.momo.android.activity.ah;
import com.immomo.momo.android.broadcast.p;
import com.immomo.momo.android.broadcast.w;
import com.immomo.momo.android.c.i;
import com.immomo.momo.android.view.EmoteInputView;
import com.immomo.momo.android.view.MEmoteEditeText;
import com.immomo.momo.android.view.MGifImageView;
import com.immomo.momo.android.view.PublishButton;
import com.immomo.momo.android.view.PublishSelectPhotoView;
import com.immomo.momo.android.view.ResizeListenerLayout;
import com.immomo.momo.android.view.a.n;
import com.immomo.momo.android.view.au;
import com.immomo.momo.android.view.bi;
import com.immomo.momo.android.view.cs;
import com.immomo.momo.android.view.f;
import com.immomo.momo.g;
import com.immomo.momo.plugin.b.a;
import com.immomo.momo.protocol.a.k;
import com.immomo.momo.service.ai;
import com.immomo.momo.service.aq;
import com.immomo.momo.service.bean.ab;
import com.immomo.momo.service.bean.av;
import com.immomo.momo.service.bean.ay;
import com.immomo.momo.service.bean.q;
import com.immomo.momo.service.l;
import com.immomo.momo.service.m;
import com.immomo.momo.util.ao;
import com.immomo.momo.util.h;
import com.immomo.momo.util.j;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import mm.purchasesdk.PurchaseCode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class PublishFeedActivity extends ah implements View.OnClickListener, View.OnTouchListener, cs, f, l {
    /* access modifiers changed from: private */
    public PublishSelectPhotoView A;
    private View B;
    private View C;
    private View D;
    /* access modifiers changed from: private */
    public PublishButton E;
    /* access modifiers changed from: private */
    public PublishButton F;
    private TextView G;
    /* access modifiers changed from: private */
    public au H;
    /* access modifiers changed from: private */
    public HashMap I = new HashMap();
    private String J = PoiTypeDef.All;
    private File K = null;
    private File L = null;
    /* access modifiers changed from: private */
    public a M = null;
    private Bitmap N = null;
    private int O = 0;
    private HashMap P = new HashMap();
    private HashMap Q = new HashMap();
    private String R = PoiTypeDef.All;
    /* access modifiers changed from: private */
    public String S = PoiTypeDef.All;
    private m T = new m();
    protected boolean h = false;
    private ab i = null;
    private ai j = null;
    private aq k = null;
    private bh l = null;
    private bh m = null;
    private bh n = null;
    private bh o = null;
    /* access modifiers changed from: private */
    public MEmoteEditeText p = null;
    /* access modifiers changed from: private */
    public TextView q = null;
    private ResizeListenerLayout r = null;
    private boolean s = false;
    private boolean t = false;
    private boolean u = false;
    /* access modifiers changed from: private */
    public int v = 0;
    /* access modifiers changed from: private */
    public int w;
    /* access modifiers changed from: private */
    public EmoteInputView x = null;
    /* access modifiers changed from: private */
    public Handler y = new Handler();
    private MGifImageView z;

    /* access modifiers changed from: private */
    public void A() {
        this.B.setVisibility(8);
    }

    private void B() {
        this.B.setVisibility(0);
        this.D.setVisibility(8);
    }

    private void C() {
        this.C.setVisibility(8);
        this.E.setSelected(false);
        this.F.setSelected(false);
    }

    /* access modifiers changed from: private */
    public void D() {
        this.D.setVisibility(0);
    }

    private void E() {
        this.D.setVisibility(8);
    }

    private void F() {
        this.v = 0;
        findViewById(R.id.btn_localphoto).setEnabled(true);
        findViewById(R.id.btn_camera).setEnabled(true);
        findViewById(R.id.btn_emote).setEnabled(true);
        findViewById(R.id.layout_tip).setVisibility(0);
    }

    private ArrayList G() {
        ArrayList arrayList = new ArrayList();
        if (this.A.getDatalist() != null) {
            int i2 = 0;
            while (true) {
                int i3 = i2;
                if (i3 >= this.A.getDatalist().size()) {
                    break;
                }
                arrayList.add(((av) this.A.getDatalist().get(i3)).f2998a);
                i2 = i3 + 1;
            }
        }
        return arrayList;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.h.a(java.lang.String, java.lang.String, int, boolean):void
     arg types: [java.lang.String, java.lang.String, int, int]
     candidates:
      com.immomo.momo.util.h.a(java.lang.String, android.graphics.Bitmap, int, boolean):java.io.File
      com.immomo.momo.util.h.a(java.lang.String, java.lang.String, int, boolean):void */
    private void a(ab abVar) {
        try {
            if (this.v == 2) {
                int i2 = abVar.i();
                for (int i3 = 0; i3 < i2; i3++) {
                    String str = (String) this.Q.get("photo_" + i3);
                    this.e.b((Object) ("asdf feed.getImages()[i]=" + abVar.j()[i3] + ", oldname=" + str));
                    if (!android.support.v4.b.a.a((CharSequence) str)) {
                        h.a(str, abVar.j()[i3], 16, false);
                    }
                }
            }
        } catch (Throwable th) {
            this.e.a(th);
        }
    }

    private void a(List list) {
        ArrayList arrayList = new ArrayList();
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= list.size()) {
                this.A.a();
                this.A.a(arrayList);
                this.A.setData(this.A.getDatalist());
                return;
            }
            av avVar = new av();
            if (!android.support.v4.b.a.a((CharSequence) list.get(i3))) {
                avVar.f2998a = (String) list.get(i3);
                av avVar2 = (av) this.I.get(avVar.f2998a);
                if (avVar2 == null) {
                    File file = new File(avVar.f2998a);
                    if (file.exists()) {
                        Bitmap a2 = android.support.v4.b.a.a(file, (int) PurchaseCode.LOADCHANNEL_ERR, (int) PurchaseCode.LOADCHANNEL_ERR);
                        if (a2 != null) {
                            avVar.c = a2;
                            j.a(avVar.f2998a, a2);
                        }
                        avVar.b = file;
                        this.I.put(avVar.f2998a, avVar);
                        avVar2 = avVar;
                    } else {
                        avVar2 = null;
                    }
                }
                if (avVar2 != null) {
                    arrayList.add(avVar2);
                }
            }
            i2 = i3 + 1;
        }
    }

    static /* synthetic */ void b(PublishFeedActivity publishFeedActivity) {
        publishFeedActivity.P.clear();
        publishFeedActivity.Q.clear();
        try {
            JSONArray jSONArray = new JSONArray();
            if (publishFeedActivity.A.getDatalist() != null) {
                int i2 = 0;
                while (true) {
                    int i3 = i2;
                    if (i3 >= publishFeedActivity.A.getDatalist().size()) {
                        publishFeedActivity.R = jSONArray.toString();
                        return;
                    }
                    JSONObject jSONObject = new JSONObject();
                    jSONObject.put("upload", "NO");
                    jSONObject.put("key", "photo_" + i3);
                    publishFeedActivity.P.put("photo_" + i3, ((av) publishFeedActivity.A.getDatalist().get(i3)).b);
                    jSONArray.put(jSONObject);
                    i2 = i3 + 1;
                }
            }
        } catch (Exception e) {
            publishFeedActivity.e.a((Throwable) e);
        }
    }

    private void c(int i2) {
        u();
        this.x.setEmoteFlag(i2);
        if (this.h) {
            this.y.postDelayed(new aq(this), 300);
        } else {
            this.x.b();
        }
    }

    static /* synthetic */ void c(PublishFeedActivity publishFeedActivity) {
        publishFeedActivity.T.e = publishFeedActivity.p.getText().toString().trim();
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("selectMode", publishFeedActivity.v);
            jSONObject.put("content", publishFeedActivity.p.getText().toString().trim());
            jSONObject.put("emotionbody", publishFeedActivity.M == null ? PoiTypeDef.All : publishFeedActivity.M.toString());
            jSONObject.put("pathlist", android.support.v4.b.a.a(publishFeedActivity.G(), ","));
            publishFeedActivity.T.f3050a = jSONObject.toString();
        } catch (JSONException e) {
            publishFeedActivity.e.a((Throwable) e);
        }
    }

    /* access modifiers changed from: private */
    public void d(int i2) {
        if (i2 <= 0) {
            this.G.setVisibility(8);
            return;
        }
        this.G.setVisibility(0);
        this.G.setText(new StringBuilder().append(i2).toString());
    }

    static /* synthetic */ void l(PublishFeedActivity publishFeedActivity) {
        publishFeedActivity.C.setVisibility(0);
        if (publishFeedActivity.v == 0) {
            publishFeedActivity.B.setVisibility(8);
            publishFeedActivity.D.setVisibility(8);
        } else if (publishFeedActivity.v == 1) {
            publishFeedActivity.B.setVisibility(0);
            publishFeedActivity.D.setVisibility(8);
            publishFeedActivity.z();
        } else if (publishFeedActivity.v == 2) {
            publishFeedActivity.B.setVisibility(8);
            publishFeedActivity.D.setVisibility(0);
        }
    }

    static /* synthetic */ boolean p(PublishFeedActivity publishFeedActivity) {
        if (publishFeedActivity.M != null || (publishFeedActivity.A.getDatalist() != null && (publishFeedActivity.A.getDatalist() == null || publishFeedActivity.A.getDatalist().size() > 0))) {
            String trim = publishFeedActivity.p.getText().toString().trim();
            if (android.support.v4.b.a.a((CharSequence) trim)) {
                publishFeedActivity.a((int) R.string.feed_publish_toast_nofeed);
                return false;
            } else if (trim.length() <= 120) {
                return true;
            } else {
                publishFeedActivity.a((int) R.string.feed_publish_toast_long);
                return false;
            }
        } else {
            publishFeedActivity.a((int) R.string.feed_publish_toast_nopic);
            return false;
        }
    }

    private void y() {
        Display defaultDisplay = getWindowManager().getDefaultDisplay();
        defaultDisplay.getWidth();
        this.w = defaultDisplay.getHeight();
    }

    /* access modifiers changed from: private */
    public void z() {
        findViewById(R.id.btn_localphoto).setEnabled(false);
        findViewById(R.id.btn_camera).setEnabled(false);
        findViewById(R.id.btn_emote).setEnabled(true);
        findViewById(R.id.layout_tip).setVisibility(8);
        B();
        E();
        this.x.a();
        B();
        this.z.setAlt(this.M.g());
        if (this.H != null) {
            this.H.l();
            this.H = null;
        }
        String g = this.M.g();
        String h2 = this.M.h();
        MGifImageView mGifImageView = this.z;
        boolean endsWith = g.endsWith(".gif");
        if (this.H == null) {
            File a2 = q.a(g, h2);
            this.e.b((Object) ("emoteFile:" + (a2 == null ? "null" : a2.getAbsolutePath())));
            if (a2 == null || !a2.exists()) {
                new i(g, h2, new bb(this, mGifImageView, endsWith)).a();
                return;
            }
            this.H = new au(endsWith ? 1 : 2);
            this.H.a(a2, mGifImageView);
            this.H.b(20);
            mGifImageView.setGifDecoder(this.H);
            return;
        }
        mGifImageView.setGifDecoder(this.H);
        if ((this.H.g() == 4 || this.H.g() == 2) && this.H.f() != null && this.H.f().exists()) {
            this.H.a(this.H.f(), mGifImageView);
        }
    }

    /* access modifiers changed from: protected */
    public final String a(Uri uri) {
        Cursor managedQuery = managedQuery(uri, new String[]{"_data"}, null, null, null);
        if (managedQuery == null) {
            return uri.getPath();
        }
        int columnIndexOrThrow = managedQuery.getColumnIndexOrThrow("_data");
        managedQuery.moveToFirst();
        return managedQuery.getString(columnIndexOrThrow);
    }

    public final void a(int i2, View view) {
        Uri fromFile;
        if (this.A.getDatalist() != null && i2 < this.A.getDatalist().size() && (fromFile = Uri.fromFile(((av) this.A.getDatalist().get(i2)).b)) != null) {
            this.O = i2;
            Intent intent = new Intent(this, ImageFactoryActivity.class);
            intent.setData(fromFile);
            intent.putExtra("minsize", 320);
            intent.putExtra("process_model", "filter");
            intent.putExtra("maxwidth", 720);
            intent.putExtra("maxheight", 3000);
            this.L = new File(com.immomo.momo.a.g(), "temp_" + b.a() + ".jpg_");
            intent.putExtra("outputFilePath", this.L.getAbsolutePath());
            startActivityForResult(intent, 105);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        String[] b;
        List asList;
        String[] b2;
        List asList2;
        super.a(bundle);
        setContentView((int) R.layout.activity_publish_feed);
        this.j = new ai();
        this.k = new aq();
        this.i = new ab();
        this.i.d = new ay();
        y();
        Intent intent = getIntent();
        if (bundle != null) {
            this.u = bundle.getBoolean("from_share", false);
            this.f = this.k.b(this.f.h);
        }
        if (intent.getExtras() != null) {
            Uri data = intent.getData();
            if (!intent.getExtras().getBoolean("is_read") && data != null) {
                this.f = this.k.b(this.f.h);
                this.u = true;
                u();
                this.y.postDelayed(new aw(this, data), 300);
                intent.putExtra("is_read", true);
            }
        }
        m().setTitleText((int) R.string.feed_publishfeed_title);
        this.r = (ResizeListenerLayout) findViewById(R.id.rootlayout);
        this.p = (MEmoteEditeText) findViewById(R.id.signeditor_tv_text);
        this.q = (TextView) findViewById(R.id.tv_textcount);
        this.x = (EmoteInputView) findViewById(R.id.emoteview);
        this.x.setEditText(this.p);
        this.z = (MGifImageView) findViewById(R.id.iv_selected_emote);
        this.C = findViewById(R.id.layout_selected);
        this.B = findViewById(R.id.layout_selected_emote);
        this.D = findViewById(R.id.layout_selected_photo);
        this.A = (PublishSelectPhotoView) findViewById(R.id.list_selectphotos);
        this.E = (PublishButton) findViewById(R.id.btn_selectpic);
        this.F = (PublishButton) findViewById(R.id.btn_selectemote);
        this.E.setIcon(R.drawable.ic_publish_selectpic);
        this.F.setIcon(R.drawable.ic_publish_selectemote);
        this.G = (TextView) findViewById(R.id.tv_selectpic_count);
        this.E.setSelected(true);
        this.F.setSelected(false);
        this.p.setOnTouchListener(this);
        this.p.addTextChangedListener(new ax(this));
        bi a2 = new bi(getApplicationContext()).a((int) R.drawable.ic_topbar_confirm_white);
        a2.setMarginRight(8);
        a2.setBackgroundResource(R.drawable.bg_header_submit);
        m().a(a2, new ay(this));
        this.r.setOnResizeListener(new az(this));
        this.x.setOnEmoteSelectedListener(new ba(this));
        findViewById(R.id.iv_delete_emote).setOnClickListener(this);
        findViewById(R.id.btn_localphoto).setOnClickListener(this);
        findViewById(R.id.btn_camera).setOnClickListener(this);
        findViewById(R.id.btn_emote).setOnClickListener(this);
        this.A.setOnMomoGridViewItemClickListener(this);
        this.A.setRefreshListener(this);
        this.E.setOnClickListener(this);
        this.F.setOnClickListener(this);
        this.l = new bh(this, findViewById(R.id.signeditor_layout_syncto_sinaweibo), 1);
        this.n = new bh(this, findViewById(R.id.signeditor_layout_syncto_tx), 2);
        this.m = new bh(this, findViewById(R.id.signeditor_layout_syncto_renren), 3);
        this.o = new bh(this, findViewById(R.id.signeditor_layout_syncto_weixin), 6);
        if (getIntent().getExtras().containsKey("ddraft")) {
            this.T.b = getIntent().getIntExtra("ddraftid", 0);
            try {
                JSONObject jSONObject = new JSONObject(getIntent().getStringExtra("ddraft"));
                this.p.setText(jSONObject.optString("content", PoiTypeDef.All));
                this.p.setSelection(this.p.getText().toString().length());
                this.v = jSONObject.optInt("selectMode", 0);
                if (this.v == 1 && !android.support.v4.b.a.a((CharSequence) jSONObject.optString("emotionbody", PoiTypeDef.All))) {
                    this.M = new a(jSONObject.optString("emotionbody", PoiTypeDef.All));
                    d(1);
                }
                if (!android.support.v4.b.a.a((CharSequence) jSONObject.optString("pathlist", PoiTypeDef.All)) && this.v == 2 && (b2 = android.support.v4.b.a.b(jSONObject.optString("pathlist", PoiTypeDef.All), ",")) != null && (asList2 = Arrays.asList(b2)) != null) {
                    b(new bf(this, this, asList2));
                }
            } catch (JSONException e) {
                this.e.a((Throwable) e);
            }
        } else {
            if (bundle == null || !bundle.getBoolean("from_saveinstance", false)) {
                Intent intent2 = getIntent();
                if (!(intent2 == null || intent2.getExtras() == null)) {
                    this.t = Boolean.valueOf(intent2.getBooleanExtra("is_from_nearbyfeed", false)).booleanValue();
                }
            } else {
                this.t = bundle.getBoolean("is_from_nearbyfeed");
                String str = bundle.get("save_feedcontent") == null ? PoiTypeDef.All : (String) bundle.get("save_feedcontent");
                if (!android.support.v4.b.a.a((CharSequence) str)) {
                    this.p.setText(str);
                    this.p.setSelection(str.length());
                }
                if (bundle.containsKey("camera_filename")) {
                    this.J = bundle.getString("camera_filename");
                }
                if (bundle.containsKey("camera_filepath")) {
                    this.K = new File(bundle.getString("camera_filepath"));
                }
                if (bundle.containsKey("local_filepath")) {
                    this.L = new File(bundle.getString("local_filepath"));
                }
                this.O = bundle.getInt("posFilter");
                this.v = bundle.getInt("selectMode");
                if (this.v == 1 && bundle.get("emotionbody") != null) {
                    this.M = new a((String) bundle.get("emotionbody"));
                    d(1);
                } else if (!(this.v != 2 || bundle.get("pathlist") == null || (b = android.support.v4.b.a.b((String) bundle.get("pathlist"), ",")) == null || (asList = Arrays.asList(b)) == null)) {
                    a(asList);
                }
            }
            this.l = new bh(this, findViewById(R.id.signeditor_layout_syncto_sinaweibo), 1);
            this.n = new bh(this, findViewById(R.id.signeditor_layout_syncto_tx), 2);
            this.m = new bh(this, findViewById(R.id.signeditor_layout_syncto_renren), 3);
        }
        if (bundle == null) {
            String stringExtra = getIntent().getStringExtra("toptic");
            if (!android.support.v4.b.a.a((CharSequence) stringExtra)) {
                this.p.setText(stringExtra);
                this.p.setSelection(stringExtra.length());
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.h.a(java.lang.String, android.graphics.Bitmap, int, boolean):java.io.File
     arg types: [java.lang.String, android.graphics.Bitmap, int, int]
     candidates:
      com.immomo.momo.util.h.a(java.lang.String, java.lang.String, int, boolean):void
      com.immomo.momo.util.h.a(java.lang.String, android.graphics.Bitmap, int, boolean):java.io.File */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.b.a.a(android.graphics.Bitmap, float, boolean):android.graphics.Bitmap
     arg types: [android.graphics.Bitmap, int, int]
     candidates:
      android.support.v4.b.a.a(android.graphics.Bitmap, int, int):android.graphics.Bitmap
      android.support.v4.b.a.a(java.io.File, int, int):android.graphics.Bitmap
      android.support.v4.b.a.a(java.lang.String, java.lang.String, java.lang.String):java.lang.String
      android.support.v4.b.a.a(java.lang.Object[], java.lang.String, java.lang.String):java.lang.String
      android.support.v4.b.a.a(android.graphics.Bitmap, float, boolean):android.graphics.Bitmap */
    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        switch (i2) {
            case 11:
                if (i3 == -1) {
                    this.f.an = true;
                    new aq().b(this.f);
                    this.l.g = true;
                    this.l.a(true);
                    Intent intent2 = new Intent(w.f2366a);
                    intent2.putExtra("momoid", this.f.h);
                    sendBroadcast(intent2);
                    return;
                }
                return;
            case 12:
                if (i3 == -1) {
                    this.f.ar = true;
                    this.m.g = true;
                    this.m.a(true);
                    Intent intent3 = new Intent(w.f2366a);
                    intent3.putExtra("momoid", this.f.h);
                    sendBroadcast(intent3);
                    return;
                }
                return;
            case 13:
                if (i3 == -1) {
                    this.f.at = true;
                    new aq().b(this.f);
                    this.n.g = true;
                    this.n.a(true);
                    Intent intent4 = new Intent(w.f2366a);
                    intent4.putExtra("momoid", this.f.h);
                    sendBroadcast(intent4);
                    return;
                }
                return;
            case PurchaseCode.QUERY_OK /*101*/:
                if (i3 == -1 && intent != null) {
                    this.v = 2;
                    if (!android.support.v4.b.a.a((CharSequence) this.J)) {
                        File file = new File(com.immomo.momo.a.i(), this.J);
                        if (file.exists()) {
                            file.delete();
                        }
                        this.J = null;
                    }
                    if (this.K != null) {
                        String absolutePath = this.K.getAbsolutePath();
                        String substring = this.K.getName().substring(0, this.K.getName().lastIndexOf("."));
                        Bitmap l2 = android.support.v4.b.a.l(absolutePath);
                        if (l2 != null) {
                            File a2 = h.a(substring, l2, 16, false);
                            this.e.a((Object) ("scaleAndSavePhoto, uploadFile=" + a2.getPath()));
                            this.N = android.support.v4.b.a.a(l2, 150.0f, true);
                            h.a(substring, this.N, 15, false);
                            av avVar = new av();
                            avVar.b = a2;
                            avVar.f2998a = a2.getAbsolutePath();
                            avVar.c = this.N;
                            this.I.put(avVar.f2998a, avVar);
                            this.A.b(avVar);
                            this.A.setData(this.A.getDatalist());
                            l2.recycle();
                        }
                        try {
                            this.K.delete();
                            this.K = null;
                        } catch (Exception e) {
                        }
                        getWindow().getDecorView().requestFocus();
                        return;
                    }
                    return;
                } else if (i3 == 1003) {
                    ao.b("图片尺寸太小，请重新选择", 1);
                    return;
                } else if (i3 == 1000) {
                    ao.d(R.string.cropimage_error_other);
                    return;
                } else if (i3 == 1002) {
                    ao.d(R.string.cropimage_error_store);
                    return;
                } else if (i3 == 1001) {
                    ao.d(R.string.cropimage_error_filenotfound);
                    return;
                } else {
                    return;
                }
            case PurchaseCode.UNSUB_OK /*103*/:
                if (i3 == -1 && !android.support.v4.b.a.a((CharSequence) this.J)) {
                    this.p.requestFocus();
                    Uri fromFile = Uri.fromFile(new File(com.immomo.momo.a.i(), this.J));
                    if (fromFile != null) {
                        Intent intent5 = new Intent(this, ImageFactoryActivity.class);
                        intent5.setData(fromFile);
                        intent5.putExtra("minsize", 320);
                        intent5.putExtra("process_model", "filter");
                        intent5.putExtra("maxwidth", 720);
                        intent5.putExtra("maxheight", 3000);
                        this.K = new File(com.immomo.momo.a.g(), "temp_" + b.a() + ".jpg_");
                        intent5.putExtra("outputFilePath", this.K.getAbsolutePath());
                        startActivityForResult(intent5, PurchaseCode.QUERY_OK);
                        return;
                    }
                    return;
                }
                return;
            case PurchaseCode.AUTH_OK /*104*/:
                if (i3 == -1 && intent != null) {
                    this.v = 2;
                    b(new bf(this, this, intent.getStringArrayListExtra("select_images_path")));
                    return;
                }
                return;
            case 105:
                if (i3 != -1 || intent == null) {
                    if (i3 == 1003) {
                        ao.b("图片尺寸太小，请重新选择", 1);
                        return;
                    } else if (i3 == 1000) {
                        ao.d(R.string.cropimage_error_other);
                        return;
                    } else if (i3 == 1002) {
                        ao.d(R.string.cropimage_error_store);
                        return;
                    } else if (i3 == 1001) {
                        ao.d(R.string.cropimage_error_filenotfound);
                        return;
                    } else {
                        return;
                    }
                } else if (this.L != null) {
                    String absolutePath2 = this.L.getAbsolutePath();
                    String substring2 = this.L.getName().substring(0, this.L.getName().lastIndexOf("."));
                    Bitmap l3 = android.support.v4.b.a.l(absolutePath2);
                    if (l3 != null) {
                        File a3 = h.a(substring2, l3, 16, false);
                        this.e.a((Object) ("scaleAndSavePhoto, uploadFile=" + a3.getPath()));
                        Bitmap a4 = android.support.v4.b.a.a(l3, 150.0f, true);
                        h.a(substring2, a4, 15, false);
                        av avVar2 = (av) this.A.getDatalist().get(this.O);
                        avVar2.b = a3;
                        avVar2.f2998a = a3.getAbsolutePath();
                        avVar2.c = a4;
                        this.I.put(avVar2.f2998a, avVar2);
                        this.A.a(this.O, avVar2);
                        this.A.setData(this.A.getDatalist());
                        l3.recycle();
                    }
                    try {
                        this.L.delete();
                        this.L = null;
                    } catch (Exception e2) {
                    }
                    getWindow().getDecorView().requestFocus();
                    return;
                } else {
                    return;
                }
            default:
                return;
        }
    }

    public void onBackPressed() {
        if (r()) {
            if (this.x.isShown()) {
                this.x.a();
                this.h = false;
                return;
            }
            if (this.M != null || (this.A.getDatalist() != null && this.A.getDatalist().size() > 0) || android.support.v4.b.a.f(this.p.getText().toString().trim())) {
                n nVar = new n(this);
                nVar.a();
                nVar.setTitle((int) R.string.feed_publish_dialog_title);
                nVar.a((int) R.string.feed_publish_dialog_content);
                nVar.a(0, (int) R.string.dialog_btn_confim, new ap(this));
                nVar.a(1, (int) R.string.dialog_btn_cancel, new au());
                nVar.show();
                return;
            }
        }
        super.onBackPressed();
    }

    public void onClick(View view) {
        int i2 = 0;
        switch (view.getId()) {
            case R.id.btn_localphoto /*2131165805*/:
                A();
                ArrayList arrayList = new ArrayList();
                if (this.A.getDatalist() != null) {
                    while (true) {
                        int i3 = i2;
                        if (i3 < this.A.getDatalist().size()) {
                            arrayList.add(((av) this.A.getDatalist().get(i3)).f2998a);
                            i2 = i3 + 1;
                        }
                    }
                }
                Intent intent = new Intent(this, MulImagePickerActivity.class);
                intent.putStringArrayListExtra("select_images_path_results", arrayList);
                intent.putExtra("max_select_images_num", 6);
                startActivityForResult(intent, PurchaseCode.AUTH_OK);
                return;
            case R.id.btn_camera /*2131165806*/:
                A();
                if (this.A.getDatalist() == null) {
                    i2 = 1;
                } else if (this.A.getDatalist() != null && this.A.getDatalist().size() < 6) {
                    i2 = 1;
                }
                if (i2 == 0) {
                    a((CharSequence) "最多选择6张图片");
                    return;
                }
                Intent intent2 = new Intent("android.media.action.IMAGE_CAPTURE");
                StringBuffer stringBuffer = new StringBuffer();
                stringBuffer.append("immomo_");
                stringBuffer.append(android.support.v4.b.a.c(new Date()));
                stringBuffer.append("_" + UUID.randomUUID());
                stringBuffer.append(".jpg");
                this.J = stringBuffer.toString();
                intent2.putExtra("output", Uri.fromFile(new File(com.immomo.momo.a.i(), this.J)));
                startActivityForResult(intent2, PurchaseCode.UNSUB_OK);
                return;
            case R.id.btn_emote /*2131165807*/:
                c(4);
                return;
            case R.id.layout_selected_photo /*2131165808*/:
            case R.id.list_selectphotos /*2131165809*/:
            case R.id.layout_selected_emote /*2131165810*/:
            case R.id.iv_selected_emote /*2131165811*/:
            case R.id.tv_tip /*2131165813*/:
            case R.id.layout_line /*2131165814*/:
            case R.id.signeditor_layout_syncto_weixin /*2131165815*/:
            default:
                return;
            case R.id.iv_delete_emote /*2131165812*/:
                A();
                F();
                d(0);
                this.M = null;
                return;
            case R.id.btn_selectpic /*2131165816*/:
                u();
                this.y.postDelayed(new ar(this), 300);
                return;
            case R.id.btn_selectemote /*2131165817*/:
                C();
                this.x.a();
                this.E.setSelected(false);
                this.F.setSelected(true);
                c(1);
                return;
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        y();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.l.b();
        this.m.b();
        this.n.b();
        this.o.b();
        if (this.H != null) {
            this.H.l();
        }
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Bundle bundle) {
        super.onRestoreInstanceState(bundle);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.s) {
            this.y.postDelayed(new av(this), 200);
            getWindow().getDecorView().requestFocus();
            this.h = this.x.isShown();
        } else {
            this.h = false;
        }
        if (!this.s) {
            this.s = true;
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        String editable = this.p.getText().toString();
        if (!android.support.v4.b.a.a((CharSequence) editable)) {
            bundle.putString("save_feedcontent", editable);
        }
        bundle.putInt("selectMode", this.v);
        if (this.M != null && this.v == 1) {
            bundle.putString("emotionbody", this.M.toString());
        }
        if (this.A.getDatalist() != null && this.v == 2) {
            bundle.putString("pathlist", android.support.v4.b.a.a(G(), ","));
        }
        bundle.putBoolean("is_from_nearbyfeed", this.t);
        bundle.putBoolean("from_saveinstance", true);
        bundle.putBoolean("from_share", this.u);
        if (!android.support.v4.b.a.a((CharSequence) this.J)) {
            bundle.putString("camera_filename", this.J);
        }
        if (this.K != null) {
            bundle.putString("camera_filepath", this.K.getPath());
        }
        if (this.L != null) {
            bundle.putString("local_filepath", this.L.getPath());
        }
        bundle.putInt("posFilter", this.O);
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        switch (view.getId()) {
            case R.id.layout_content /*2131165199*/:
                if ((this.x.isShown() || this.h) && 1 == motionEvent.getAction() && motionEvent.getEventTime() - motionEvent.getDownTime() < 100) {
                    if (this.x.isShown()) {
                        this.x.a();
                    } else {
                        u();
                    }
                    this.h = false;
                    break;
                }
            case R.id.signeditor_tv_text /*2131165795*/:
            case R.id.et_title /*2131165818*/:
                if (motionEvent.getAction() == 1) {
                    this.x.a();
                    C();
                    break;
                }
                break;
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public final void u() {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService("input_method");
        View currentFocus = getCurrentFocus();
        if (currentFocus != null) {
            inputMethodManager.hideSoftInputFromWindow(currentFocus.getWindowToken(), 2);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.h.a(java.lang.String, android.graphics.Bitmap, int, boolean):java.io.File
     arg types: [java.lang.String, android.graphics.Bitmap, int, int]
     candidates:
      com.immomo.momo.util.h.a(java.lang.String, java.lang.String, int, boolean):void
      com.immomo.momo.util.h.a(java.lang.String, android.graphics.Bitmap, int, boolean):java.io.File */
    public final void v() {
        boolean a2 = this.m.a();
        boolean a3 = this.l.a();
        boolean a4 = this.n.a();
        boolean a5 = this.o.a();
        double d = 0.0d;
        double d2 = 0.0d;
        int i2 = 0;
        this.f = this.k.b(this.f.h);
        if (this.f != null) {
            i2 = this.f.aH;
            d = this.f.S;
            d2 = this.f.T;
        }
        switch (this.v) {
            case 0:
                this.S = k.a().a(this.p.getText().toString().trim(), new HashMap(), this.R, null, a3, a2, a4, a5, this.i, i2, d, d2);
                break;
            case 1:
                this.S = k.a().a(this.p.getText().toString().trim(), new HashMap(), this.R, this.M, a3, a2, a4, a5, this.i, i2, d, d2);
                break;
            case 2:
                for (Map.Entry entry : this.P.entrySet()) {
                    File file = (File) entry.getValue();
                    if (file == null || !file.exists()) {
                        this.y.post(new as(this));
                        throw new Exception();
                    }
                    String substring = file.getName().substring(0, file.getName().lastIndexOf("."));
                    Bitmap a6 = android.support.v4.b.a.a(file, 720, 3000);
                    entry.setValue(h.a(substring, a6, 16, false));
                    this.Q.put((String) entry.getKey(), substring);
                    a6.recycle();
                }
                this.S = k.a().a(this.p.getText().toString().trim(), this.P, this.R, null, a3, a2, a4, a5, this.i, i2, d, d2);
                break;
        }
        this.j.a(this.i);
        this.k.a(this.i.h, this.f.h);
        a(this.i);
        Intent intent = new Intent(p.f2359a);
        intent.putExtra("feedid", this.i.h);
        intent.putExtra("userid", this.f.h);
        sendBroadcast(intent);
        if (this.t || this.u) {
            Intent intent2 = new Intent(p.b);
            intent2.putExtra("feedid", this.i.h);
            sendBroadcast(intent2);
        }
        if (this.o.a()) {
            this.y.post(new at(this));
        }
    }

    public final m w() {
        return this.T;
    }

    public final void x() {
        if (this.A.getDatalist() != null) {
            this.G.setText(new StringBuilder().append(this.A.getDatalist().size()).toString());
            if (this.A.getDatalist().size() > 0) {
                D();
                findViewById(R.id.btn_localphoto).setEnabled(true);
                findViewById(R.id.btn_camera).setEnabled(true);
                findViewById(R.id.btn_emote).setEnabled(false);
                findViewById(R.id.layout_tip).setVisibility(8);
                this.G.setVisibility(0);
                RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) this.C.getLayoutParams();
                layoutParams.height = -2;
                this.C.setLayoutParams(layoutParams);
                return;
            }
            F();
            this.G.setVisibility(8);
            RelativeLayout.LayoutParams layoutParams2 = (RelativeLayout.LayoutParams) this.C.getLayoutParams();
            layoutParams2.height = (int) (266.0f * g.k());
            this.C.setLayoutParams(layoutParams2);
            E();
            return;
        }
        this.G.setVisibility(8);
        E();
    }
}
