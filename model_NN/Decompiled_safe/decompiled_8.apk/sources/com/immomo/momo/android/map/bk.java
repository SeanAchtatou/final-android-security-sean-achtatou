package com.immomo.momo.android.map;

import android.location.Location;
import com.immomo.momo.R;
import com.immomo.momo.android.b.p;
import com.immomo.momo.android.b.z;
import com.immomo.momo.util.ao;

final class bk extends p {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SelectSiteGoogleActivity f2490a;

    bk(SelectSiteGoogleActivity selectSiteGoogleActivity) {
        this.f2490a = selectSiteGoogleActivity;
    }

    public final void a(Location location, int i, int i2, int i3) {
        if (this.b) {
            if (z.a(location)) {
                this.f2490a.d.post(new bl(this, location));
                return;
            }
            this.f2490a.c();
            if (i2 == 104) {
                ao.g(R.string.errormsg_network_unfind);
            } else {
                ao.g(R.string.errormsg_location_nearby_failed);
            }
        }
    }
}
