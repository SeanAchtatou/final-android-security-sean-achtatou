package com.immomo.momo.android.activity;

import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ListAdapter;
import com.immomo.momo.R;
import com.immomo.momo.android.a.fs;
import com.immomo.momo.android.a.i;
import com.immomo.momo.g;
import com.immomo.momo.service.aa;
import com.immomo.momo.service.bean.b.a;
import com.immomo.momo.util.ao;
import java.util.ArrayList;

public class MulImagePickerActivity extends ao {
    /* access modifiers changed from: private */
    public static int i = 1;
    private static int j = 2;
    /* access modifiers changed from: private */
    public int h = -1;
    /* access modifiers changed from: private */
    public aa k = null;
    /* access modifiers changed from: private */
    public GridView l = null;
    /* access modifiers changed from: private */
    public GridView m = null;
    private Button n = null;
    private boolean o = false;
    /* access modifiers changed from: private */
    public i p = null;
    /* access modifiers changed from: private */
    public fs q = null;
    private fz r = null;

    /* access modifiers changed from: private */
    public void g() {
        int b = this.k.b();
        int c = this.k.c();
        this.n.setText("确定(" + c + "/" + b + ")");
        if (c == 0) {
            this.n.setEnabled(this.o);
        } else {
            this.n.setEnabled(true);
        }
    }

    public final void c(int i2) {
        this.q = new fs(this, ((a) this.k.e().get(i2)).f3004a, this.m);
        this.m.setAdapter((ListAdapter) this.q);
        this.l.setVisibility(8);
        this.m.setVisibility(0);
        setTitle("选择图片");
        this.h = j;
    }

    /* access modifiers changed from: protected */
    public final void d() {
        super.d();
        if (!g.f()) {
            ao.b("请插入SD卡");
        }
        GridView gridView = this.l;
        this.p = new i(this);
        this.l.setAdapter((ListAdapter) this.p);
        this.h = i;
    }

    public void onBackPressed() {
        if (j == this.h) {
            this.m.setVisibility(8);
            this.p.notifyDataSetChanged();
            this.l.setVisibility(0);
            setTitle("选择相册");
            this.h = i;
            return;
        }
        super.onBackPressed();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_mulimage_picker);
        this.k = aa.a(this);
        Intent intent = getIntent();
        if (intent != null) {
            this.k.a(intent.getIntExtra("max_select_images_num", 6));
            ArrayList<String> stringArrayListExtra = intent.getStringArrayListExtra("select_images_path_results");
            if (stringArrayListExtra != null) {
                this.k.f2956a = stringArrayListExtra;
                if (stringArrayListExtra.size() > 0) {
                    this.o = true;
                }
            } else {
                this.k.f2956a.clear();
            }
        }
        setTitle("选择相册");
        this.n = (Button) findViewById(R.id.btn_pickok);
        g();
        this.l = (GridView) findViewById(R.id.gv_buckets);
        this.m = (GridView) findViewById(R.id.gv_images);
        d();
        this.m.setOnItemClickListener(new fx(this));
        this.n.setOnClickListener(new fy(this));
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("org.piaozhiye.demo.sdcardLinsener.receiver");
        intentFilter.addAction("android.intent.action.MEDIA_BAD_REMOVAL");
        intentFilter.addAction("android.intent.action.MEDIA_MOUNTED");
        intentFilter.addAction("android.intent.action.MEDIA_REMOVED");
        intentFilter.addAction("android.intent.action.MEDIA_UNMOUNTED");
        intentFilter.addDataScheme("file");
        this.r = new fz(this, (byte) 0);
        registerReceiver(this.r, intentFilter);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (this.r != null) {
            unregisterReceiver(this.r);
        }
        super.onDestroy();
    }
}
