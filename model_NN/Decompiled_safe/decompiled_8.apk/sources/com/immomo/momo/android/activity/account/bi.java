package com.immomo.momo.android.activity.account;

import android.content.Context;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import com.immomo.momo.R;
import com.immomo.momo.a.a;
import com.immomo.momo.a.l;
import com.immomo.momo.a.o;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.n;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.g;
import com.immomo.momo.protocol.a.w;
import org.json.JSONException;

final class bi extends d {

    /* renamed from: a  reason: collision with root package name */
    private v f962a = null;
    /* access modifiers changed from: private */
    public /* synthetic */ bc c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public bi(bc bcVar, Context context) {
        super(context);
        this.c = bcVar;
    }

    /* access modifiers changed from: protected */
    public final Object a(Object... objArr) {
        w.a().b(this.c.d.c, this.c.d.b, this.c.d.e);
        return null;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.f962a = new v(this.c.j, "正在验证，请稍候...");
        this.f962a.setOnCancelListener(new bj(this));
        this.f962a.show();
        super.a();
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        if (exc instanceof com.immomo.momo.a.w) {
            this.c.j.a((CharSequence) exc.getMessage());
        } else if ((exc instanceof l) && !this.c.j.isFinishing()) {
            n b = n.b(this.c.j, exc.getMessage(), new bk(this));
            b.setCancelable(false);
            b.show();
        } else if (exc instanceof o) {
            this.c.j.a((int) R.string.reg_pwd_email_checkerror);
        } else if (exc instanceof JSONException) {
            this.c.j.a((int) R.string.errormsg_dataerror);
        } else if (exc instanceof a) {
            this.c.j.a((CharSequence) exc.getMessage());
        } else if (!"mobile".equals(g.Y()) || !g.ac()) {
            this.c.j.a((int) R.string.errormsg_server);
        } else {
            this.c.j.g();
        }
    }

    /* access modifiers changed from: protected */
    public final void a(Object obj) {
        this.c.d.f = true;
        this.c.g.setEnabled(false);
        this.c.g.setWidth(-1);
        this.c.g.setText((int) R.string.reg_verify_passed);
        String str = "(" + this.c.d.c + ")" + this.c.d.b;
        String str2 = "你的手机号 " + str + "已通过验证";
        this.c.e.setText(str2);
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(str2);
        spannableStringBuilder.setSpan(new ForegroundColorSpan(g.c((int) R.color.notice_font)), str2.indexOf(str), str.length() + str2.indexOf(str), 34);
        this.c.e.setText(spannableStringBuilder);
        this.c.f.setEnabled(false);
        this.c.k.removeMessages(2245);
        this.c.c();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.f962a.dismiss();
        this.f962a = null;
    }
}
