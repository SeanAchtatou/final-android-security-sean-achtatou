package com.immomo.momo.android.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ListAdapter;
import com.immomo.momo.h;

public class MultiImageView extends GridView implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private int f2640a = 3;
    private int b = this.f2640a;
    private int c = 0;
    private int d = 0;
    /* access modifiers changed from: private */
    public String[] e = new String[0];
    private cb f;
    private ca g;

    public MultiImageView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context, attributeSet);
    }

    public MultiImageView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        a(context, attributeSet);
    }

    private void a(Context context, AttributeSet attributeSet) {
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, h.MultiImageView);
        setNumColumns(obtainStyledAttributes.getInteger(0, this.f2640a));
        setColumnWidth(obtainStyledAttributes.getDimensionPixelSize(2, this.c));
        setHorizontalSpacing(obtainStyledAttributes.getDimensionPixelSize(3, this.d));
        setVerticalSpacing(getHorizontalSpacing());
        obtainStyledAttributes.recycle();
    }

    public int getColumnWidth() {
        return this.c;
    }

    public int getHorizontalSpacing() {
        return this.d;
    }

    public int getNumColumns() {
        return this.b;
    }

    public void onClick(View view) {
        if (this.f != null) {
            this.f.b(((Integer) view.getTag()).intValue(), this.e);
        }
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
    }

    public void onMeasure(int i, int i2) {
        int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(536870911, Integer.MIN_VALUE);
        if (this.e != null && this.e.length > 0) {
            this.g = new ca(this, getContext());
            setAdapter((ListAdapter) this.g);
        }
        super.onMeasure(i, makeMeasureSpec);
    }

    public void setColumnWidth(int i) {
        this.c = i;
        super.setColumnWidth(i);
    }

    public void setHorizontalSpacing(int i) {
        this.d = i;
        super.setHorizontalSpacing(i);
    }

    public void setImage(String[] strArr) {
        if (strArr == null || strArr.length == 0) {
            setVisibility(8);
        }
        this.e = strArr;
        if (strArr.length <= this.f2640a) {
            setNumColumns(strArr.length);
        } else if ((this.f2640a + 1) / 2 == (strArr.length + 1) / 2) {
            setNumColumns(strArr.length / 2);
        } else {
            setNumColumns(this.f2640a);
        }
        if (this.c > 0) {
            ViewGroup.LayoutParams layoutParams = getLayoutParams();
            layoutParams.width = (this.c * this.b) + ((this.b - 1) * this.d);
            layoutParams.height = this.c;
            setLayoutParams(layoutParams);
        }
        if (this.g == null) {
            this.g = new ca(this, getContext());
            setAdapter((ListAdapter) this.g);
        }
        this.g.notifyDataSetChanged();
    }

    public void setMaxChildInLine(int i) {
        this.f2640a = i;
        setNumColumns(i);
    }

    public void setNumColumns(int i) {
        this.b = i;
        super.setNumColumns(i);
    }

    public void setOnclickHandler(cb cbVar) {
        this.f = cbVar;
    }
}
