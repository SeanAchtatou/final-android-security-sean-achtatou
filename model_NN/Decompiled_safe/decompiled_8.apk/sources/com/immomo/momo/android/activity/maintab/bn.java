package com.immomo.momo.android.activity.maintab;

import com.immomo.momo.protocol.a.w;
import com.immomo.momo.service.bean.Message;
import com.immomo.momo.service.bean.bf;

final class bn implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ bh f1862a;
    private final /* synthetic */ Message b;

    bn(bh bhVar, Message message) {
        this.f1862a = bhVar;
        this.b = message;
    }

    public final void run() {
        try {
            bf bfVar = new bf(this.b.remoteId);
            w.a().b(bfVar, bfVar.h);
            this.b.remoteUser = bfVar;
            this.f1862a.Y.d(bfVar);
            this.f1862a.ah.sendEmptyMessage(7438);
        } catch (Exception e) {
            this.f1862a.P.a((Object) "[error][from chatListViewAdapter]downloadOtherProfile exception");
        }
    }
}
