package com.immomo.momo.android.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.av;
import java.util.ArrayList;
import java.util.List;

public class PublishSelectPhotoView extends LinearLayout {

    /* renamed from: a  reason: collision with root package name */
    private static int f2647a = 30;
    private static int b = 3;
    private static int c = 2;
    private static int d;
    private static int e;
    private LinearLayout[] f;
    private RelativeLayout[] g;
    private List h = null;
    /* access modifiers changed from: private */
    public f i;
    private cs j;

    public PublishSelectPhotoView(Context context) {
        super(context);
        b();
    }

    public PublishSelectPhotoView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        b();
    }

    private void b() {
        setOrientation(1);
        int J = ((int) ((((float) g.J()) - (20.0f * g.k())) - ((((float) f2647a) * g.k()) * 3.0f))) / b;
        d = J;
        e = J;
        c = 2;
    }

    public final void a() {
        if (this.h != null) {
            this.h.clear();
        }
    }

    public final void a(int i2, av avVar) {
        if (this.h == null) {
            this.h = new ArrayList();
        }
        this.h.set(i2, avVar);
    }

    public final void a(av avVar) {
        if (this.h != null) {
            this.h.remove(avVar);
        }
    }

    public final void a(List list) {
        if (this.h == null) {
            this.h = new ArrayList();
        }
        this.h.addAll(list);
    }

    public final void b(av avVar) {
        if (this.h == null) {
            this.h = new ArrayList();
        }
        this.h.add(avVar);
    }

    public List getDatalist() {
        return this.h;
    }

    public int getItemCount() {
        if (this.h == null) {
            return 0;
        }
        return this.h.size();
    }

    /* JADX WARN: Type inference failed for: r2v39, types: [android.view.View] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup, int, boolean, boolean, int):void
     arg types: [com.immomo.momo.service.bean.al, android.widget.ImageView, ?[OBJECT, ARRAY], int, int, int, int]
     candidates:
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, int, android.view.View, android.view.ViewGroup, boolean, int, boolean):com.immomo.momo.android.c.o
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup, int, boolean, boolean, int):void */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void setData(java.util.List r19) {
        /*
            r18 = this;
            if (r19 != 0) goto L_0x0003
        L_0x0002:
            return
        L_0x0003:
            r18.removeAllViews()
            r0 = r18
            com.immomo.momo.android.view.cs r2 = r0.j
            if (r2 == 0) goto L_0x0013
            r0 = r18
            com.immomo.momo.android.view.cs r2 = r0.j
            r2.x()
        L_0x0013:
            r0 = r19
            r1 = r18
            r1.h = r0
            int r2 = com.immomo.momo.android.view.PublishSelectPhotoView.b
            int r3 = com.immomo.momo.android.view.PublishSelectPhotoView.c
            int r2 = r2 * r3
            android.widget.RelativeLayout[] r2 = new android.widget.RelativeLayout[r2]
            r0 = r18
            r0.g = r2
            int r2 = com.immomo.momo.android.view.PublishSelectPhotoView.c
            android.widget.LinearLayout[] r2 = new android.widget.LinearLayout[r2]
            r0 = r18
            r0.f = r2
            android.widget.LinearLayout$LayoutParams r14 = new android.widget.LinearLayout$LayoutParams
            r2 = -1
            int r3 = com.immomo.momo.android.view.PublishSelectPhotoView.e
            r14.<init>(r2, r3)
            r2 = 1065353216(0x3f800000, float:1.0)
            float r3 = com.immomo.momo.g.k()
            float r2 = r2 * r3
            int r2 = (int) r2
            r14.topMargin = r2
            r2 = 0
            r12 = r2
        L_0x0040:
            int r2 = com.immomo.momo.android.view.PublishSelectPhotoView.c
            if (r12 < r2) goto L_0x005b
            r0 = r18
            android.widget.LinearLayout[] r2 = r0.f
            r3 = 1
            r3 = r2[r3]
            r0 = r18
            java.util.List r2 = r0.h
            int r2 = r2.size()
            r4 = 3
            if (r2 <= r4) goto L_0x0141
            r2 = 0
        L_0x0057:
            r3.setVisibility(r2)
            goto L_0x0002
        L_0x005b:
            r0 = r18
            android.widget.LinearLayout[] r2 = r0.f
            android.widget.LinearLayout r3 = new android.widget.LinearLayout
            android.content.Context r4 = r18.getContext()
            r3.<init>(r4)
            r2[r12] = r3
            r0 = r18
            android.widget.LinearLayout[] r2 = r0.f
            r2 = r2[r12]
            r0 = r18
            r0.addView(r2, r14)
            r2 = 0
            r13 = r2
        L_0x0077:
            int r2 = com.immomo.momo.android.view.PublishSelectPhotoView.b
            if (r13 < r2) goto L_0x007f
            int r2 = r12 + 1
            r12 = r2
            goto L_0x0040
        L_0x007f:
            int r2 = com.immomo.momo.android.view.PublishSelectPhotoView.b
            int r2 = r2 * r12
            int r15 = r2 + r13
            r0 = r18
            java.util.List r2 = r0.h
            int r2 = r2.size()
            if (r15 >= r2) goto L_0x0136
            android.widget.LinearLayout$LayoutParams r16 = new android.widget.LinearLayout$LayoutParams
            int r2 = com.immomo.momo.android.view.PublishSelectPhotoView.d
            int r3 = com.immomo.momo.android.view.PublishSelectPhotoView.e
            r0 = r16
            r0.<init>(r2, r3)
            int r2 = com.immomo.momo.android.view.PublishSelectPhotoView.f2647a
            float r2 = (float) r2
            float r3 = com.immomo.momo.g.k()
            float r2 = r2 * r3
            int r2 = (int) r2
            r0 = r16
            r0.leftMargin = r2
            r0 = r18
            android.widget.RelativeLayout[] r0 = r0.g
            r17 = r0
            r0 = r18
            java.util.List r2 = r0.h
            java.lang.Object r2 = r2.get(r15)
            r9 = r2
            com.immomo.momo.service.bean.av r9 = (com.immomo.momo.service.bean.av) r9
            android.content.Context r2 = r18.getContext()
            r3 = 2130903530(0x7f0301ea, float:1.741388E38)
            r4 = 0
            android.view.View r2 = inflate(r2, r3, r4)
            r10 = r2
            android.widget.RelativeLayout r10 = (android.widget.RelativeLayout) r10
            android.widget.RelativeLayout$LayoutParams r2 = new android.widget.RelativeLayout$LayoutParams
            int r3 = com.immomo.momo.android.view.PublishSelectPhotoView.d
            int r4 = com.immomo.momo.android.view.PublishSelectPhotoView.e
            r2.<init>(r3, r4)
            r10.setLayoutParams(r2)
            r2 = 2131166771(0x7f070633, float:1.7947797E38)
            android.view.View r3 = r10.findViewById(r2)
            android.widget.ImageView r3 = (android.widget.ImageView) r3
            r2 = 2131166772(0x7f070634, float:1.7947799E38)
            android.view.View r2 = r10.findViewById(r2)
            r11 = r2
            android.widget.ImageView r11 = (android.widget.ImageView) r11
            boolean r2 = r9.d
            if (r2 == 0) goto L_0x013b
            r2 = 2130838058(0x7f02022a, float:1.7281088E38)
            r3.setImageResource(r2)
            com.immomo.momo.service.bean.al r2 = new com.immomo.momo.service.bean.al
            java.lang.String r4 = r9.e
            r2.<init>(r4)
            r4 = 0
            r5 = 15
            r6 = 0
            r7 = 0
            r8 = 0
            com.immomo.momo.util.j.a(r2, r3, r4, r5, r6, r7, r8)
        L_0x00ff:
            com.immomo.momo.android.view.cq r2 = new com.immomo.momo.android.view.cq
            r0 = r18
            r2.<init>(r0, r9)
            r11.setOnClickListener(r2)
            r17[r15] = r10
            r0 = r18
            android.widget.RelativeLayout[] r2 = r0.g
            r2 = r2[r15]
            r3 = 0
            r2.setVisibility(r3)
            r0 = r18
            android.widget.RelativeLayout[] r2 = r0.g
            r2 = r2[r15]
            com.immomo.momo.android.view.cr r3 = new com.immomo.momo.android.view.cr
            r0 = r18
            r3.<init>(r0, r15)
            r2.setOnClickListener(r3)
            r0 = r18
            android.widget.LinearLayout[] r2 = r0.f
            r2 = r2[r12]
            r0 = r18
            android.widget.RelativeLayout[] r3 = r0.g
            r3 = r3[r15]
            r0 = r16
            r2.addView(r3, r0)
        L_0x0136:
            int r2 = r13 + 1
            r13 = r2
            goto L_0x0077
        L_0x013b:
            android.graphics.Bitmap r2 = r9.c
            r3.setImageBitmap(r2)
            goto L_0x00ff
        L_0x0141:
            r2 = 4
            goto L_0x0057
        */
        throw new UnsupportedOperationException("Method not decompiled: com.immomo.momo.android.view.PublishSelectPhotoView.setData(java.util.List):void");
    }

    public void setOnMomoGridViewItemClickListener(f fVar) {
        this.i = fVar;
    }

    public void setRefreshListener(cs csVar) {
        this.j = csVar;
    }
}
