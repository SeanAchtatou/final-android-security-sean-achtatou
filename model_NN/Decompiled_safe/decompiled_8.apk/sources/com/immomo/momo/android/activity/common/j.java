package com.immomo.momo.android.activity.common;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.View;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.ka;
import com.immomo.momo.android.activity.lh;
import com.immomo.momo.service.bean.bf;
import java.util.HashMap;

public abstract class j extends ka implements View.OnClickListener {
    protected int h;
    private HashMap i = new HashMap();
    private HashMap j = new HashMap();
    private Handler k = new Handler();

    /* access modifiers changed from: protected */
    public abstract String A();

    /* access modifiers changed from: protected */
    public abstract void B();

    /* access modifiers changed from: protected */
    public abstract void C();

    /* access modifiers changed from: protected */
    public final HashMap D() {
        return this.i;
    }

    /* access modifiers changed from: protected */
    public final HashMap E() {
        return this.j;
    }

    /* access modifiers changed from: protected */
    public abstract void a(int i2, int i3);

    /* access modifiers changed from: protected */
    public void a(Bundle bundle) {
        super.a(bundle);
        setContentView((int) R.layout.activity_selectfriend_tabs);
        B();
        C();
    }

    /* access modifiers changed from: protected */
    public final void a(Fragment fragment) {
        if (fragment != null) {
            this.k.post(new k(fragment));
        }
    }

    /* access modifiers changed from: protected */
    public void a(Fragment fragment, int i2) {
        super.a(fragment, i2);
        switch (i2) {
            case 0:
                findViewById(R.id.contact_tab_both).setSelected(true);
                findViewById(R.id.contact_tab_follows).setSelected(false);
                break;
            case 1:
                findViewById(R.id.contact_tab_both).setSelected(false);
                findViewById(R.id.contact_tab_follows).setSelected(true);
                break;
        }
        ((lh) fragment).a(this, m());
        a(fragment);
    }

    /* access modifiers changed from: protected */
    public final void a(bf bfVar) {
        if (bfVar != null) {
            this.i.put(bfVar.h, bfVar);
            a(v());
        }
    }

    /* access modifiers changed from: protected */
    public abstract void a(String str, int i2);

    /* access modifiers changed from: protected */
    public final void b(bf bfVar) {
        if (bfVar != null) {
            this.i.remove(bfVar.h);
            a(v());
        }
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.contact_tab_both /*2131165444*/:
                c(0);
                return;
            case R.id.contact_tab_follows /*2131165445*/:
                c(1);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (this.i != null) {
            this.i.clear();
        }
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void x() {
        super.x();
    }

    /* access modifiers changed from: protected */
    public abstract boolean y();

    /* access modifiers changed from: protected */
    public abstract int z();
}
