package com.immomo.momo.android.activity.common;

import android.view.inputmethod.InputMethodManager;

final class p implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ CloudMessageTabsActivity f1127a;

    p(CloudMessageTabsActivity cloudMessageTabsActivity) {
        this.f1127a = cloudMessageTabsActivity;
    }

    public final void run() {
        ((InputMethodManager) this.f1127a.getSystemService("input_method")).showSoftInput(this.f1127a.k, 1);
        this.f1127a.k.requestFocus();
    }
}
