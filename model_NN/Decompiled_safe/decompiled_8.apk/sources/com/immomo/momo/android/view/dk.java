package com.immomo.momo.android.view;

import android.view.View;
import android.view.animation.Animation;
import com.immomo.momo.R;

final class dk implements Animation.AnimationListener {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ View f2780a;
    private final /* synthetic */ View b;

    dk(View view, View view2) {
        this.f2780a = view;
        this.b = view2;
    }

    public final void onAnimationEnd(Animation animation) {
        this.b.setVisibility(8);
    }

    public final void onAnimationRepeat(Animation animation) {
    }

    public final void onAnimationStart(Animation animation) {
        this.f2780a.setBackgroundResource(R.drawable.bglistitem_selector_tiebaguide_select);
    }
}
