package com.immomo.momo.android.activity.message;

import android.content.BroadcastReceiver;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Bundle;
import android.support.v4.b.a;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.a.a.aa;
import com.immomo.momo.android.activity.OtherProfileActivity;
import com.immomo.momo.android.broadcast.d;
import com.immomo.momo.android.broadcast.i;
import com.immomo.momo.android.broadcast.j;
import com.immomo.momo.android.broadcast.w;
import com.immomo.momo.android.view.a.at;
import com.immomo.momo.android.view.a.n;
import com.immomo.momo.g;
import com.immomo.momo.protocol.imjson.util.c;
import com.immomo.momo.service.ae;
import com.immomo.momo.service.ag;
import com.immomo.momo.service.aq;
import com.immomo.momo.service.bean.Message;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.util.k;
import com.immomo.momo.util.m;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import mm.purchasesdk.PurchaseCode;

public class ChatActivity extends a {
    protected m L = new m("test_momo", "[--- from ChatActivity ---]" + hashCode());
    private w M;
    private i N;
    /* access modifiers changed from: private */
    public aq O;
    /* access modifiers changed from: private */
    public ae P;
    /* access modifiers changed from: private */
    public bf Q;
    private boolean R = false;
    private j S = null;
    /* access modifiers changed from: private */
    public aa T;
    private View U;
    /* access modifiers changed from: private */
    public ImageView V;
    private ImageView W;
    /* access modifiers changed from: private */
    public ImageView X;
    private Timer Y;
    private TimerTask Z;
    private d aa = new aj(this);
    private at ab = null;
    /* access modifiers changed from: private */
    public boolean ac = false;
    /* access modifiers changed from: private */
    public boolean ad = true;
    /* access modifiers changed from: private */
    public int ae = 0;

    private void a(float f, long j) {
        if (f >= 0.0f && System.currentTimeMillis() - j <= 900000) {
            AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
            alphaAnimation.setDuration(1000);
            alphaAnimation.setRepeatCount(2);
            alphaAnimation.setRepeatMode(2);
            this.X.setVisibility(0);
            this.X.setAnimation(alphaAnimation);
            alphaAnimation.setAnimationListener(new ax(this));
            alphaAnimation.start();
        }
    }

    private void a(BroadcastReceiver broadcastReceiver) {
        if (broadcastReceiver != null) {
            unregisterReceiver(broadcastReceiver);
        }
    }

    /* access modifiers changed from: private */
    public void ag() {
        setTitle("与" + this.Q.h() + "对话");
    }

    /* access modifiers changed from: private */
    public void ah() {
        if (!a.f(this.Q.Z)) {
            this.m.setSubTitleText("未知");
        } else if (this.Q.d() < 0.0f) {
            this.m.setSubTitleText(this.Q.Z);
        } else {
            this.m.setSubTitleText(String.valueOf(this.Q.Z) + (this.Q.V ? "(误差大)" : PoiTypeDef.All) + " " + this.Q.f());
        }
        ImageView imageView = this.W;
        float d = this.Q.d();
        imageView.setImageResource(d == -2.0f ? R.drawable.ic_chat_dis_f2 : d < 0.0f ? R.drawable.ic_chat_dis_f1 : d < 2000.0f ? R.drawable.ic_chat_dis_1 : d < 10000.0f ? R.drawable.ic_chat_dis_2 : d < 100000.0f ? R.drawable.ic_chat_dis_3 : d < 1000000.0f ? R.drawable.ic_chat_dis_4 : d < 5000000.0f ? R.drawable.ic_chat_dis_5 : R.drawable.ic_chat_dis_6);
        this.X.setVisibility(8);
        this.V.setImageResource(a.b(this.Q.e(), this.Q.d() < 0.0f));
    }

    /* access modifiers changed from: private */
    public List ai() {
        ArrayList arrayList = (ArrayList) this.P.a(this.Q.h, this.T.getCount(), 21);
        if (arrayList.size() > 20) {
            arrayList.remove(0);
            this.ac = true;
        } else {
            this.ac = false;
        }
        Iterator it = arrayList.iterator();
        boolean z = false;
        while (it.hasNext()) {
            Message message = (Message) it.next();
            if (message.receive) {
                if (message.status == 5 || message.status == 9 || message.status == 13) {
                    this.F.add(message.msgId);
                    if (message.status == 5) {
                        z = true;
                    }
                }
                message.remoteUser = this.Q;
                if (message.status != 10) {
                    message.status = 4;
                }
            } else if (message.status == 8) {
                com.immomo.momo.android.b.m.a(message.msgId).a(new x(this, message));
            }
        }
        this.L.a((Object) ("--------hasUnreaded=" + z));
        if (this.T.isEmpty() && z) {
            this.R = this.k.b(this.Q.h);
            if (this.R) {
                g.d().q();
            } else {
                g.d().n();
            }
        }
        aj();
        return arrayList;
    }

    private void aj() {
        if (this.F != null && this.F.size() > 0) {
            String[] strArr = (String[]) this.F.toArray(new String[0]);
            this.P.f(strArr);
            g.d().a(this.Q.h, strArr, 1);
            this.F.clear();
        }
    }

    private boolean ak() {
        return this.T.isEmpty() && !this.E;
    }

    private Message h(Message message) {
        if (message == null) {
            return null;
        }
        if (!(message.distanceTime == null || this.Q.e() == null || !message.receive)) {
            if (message.distanceTime.after(this.Q.e())) {
                this.Q.a(message.getDiatance());
                this.Q.a(message.distanceTime);
                a(this.Q.d(), message.distanceTime.getTime());
                ah();
            } else if (message.distanceTime.getTime() == this.Q.e().getTime()) {
                this.Q.a(message.getDiatance());
            }
        }
        if (message.receive) {
            message.status = 4;
        }
        message.remoteUser = this.Q;
        return message;
    }

    static /* synthetic */ void i(ChatActivity chatActivity) {
        n nVar = new n(chatActivity);
        nVar.setTitle("距离图标说明");
        nVar.a(0, "确认", new aw());
        nVar.setContentView((int) R.layout.dialog_distancedialog_des);
        nVar.show();
    }

    private void i(Message message) {
        this.L.a((Object) ("chatFrom=" + this.G));
        String stringExtra = getIntent().getStringExtra("afromname");
        if (com.immomo.momo.android.activity.d.j(this.G) || com.immomo.momo.android.activity.d.l(this.G)) {
            if (!a.a((CharSequence) stringExtra)) {
                message.source = "群组" + stringExtra;
            }
        } else if (com.immomo.momo.android.activity.d.c(this.G) || com.immomo.momo.android.activity.d.b(this.G)) {
            if (!a.a((CharSequence) stringExtra)) {
                message.source = "陌陌" + stringExtra + "吧";
            }
        } else if (com.immomo.momo.android.activity.d.d(this.G)) {
            message.source = "查看附近的人";
        } else if (com.immomo.momo.android.activity.d.i(this.G)) {
            message.source = "陌陌留言板";
        } else if (com.immomo.momo.android.activity.d.k(this.G)) {
            message.source = "地点漫游";
        } else if (com.immomo.momo.android.activity.d.m(this.G)) {
            if (!a.a((CharSequence) stringExtra)) {
                message.source = "游戏 " + stringExtra;
            }
        } else if (com.immomo.momo.android.activity.d.n(this.G)) {
            message.source = "附近活动";
        }
    }

    /* access modifiers changed from: protected */
    public final void E() {
        if (this.E) {
            super.E();
        }
    }

    /* access modifiers changed from: protected */
    public final void O() {
        D();
        this.z = (InputMethodManager) getSystemService("input_method");
        this.A = (AudioManager) getSystemService("audio");
        this.k = new ag();
        this.O = new aq();
        this.P = new ae();
    }

    /* access modifiers changed from: protected */
    public final void P() {
        this.f.setImageMultipleDiaplay(true);
        this.Q = this.O.c(u());
        if (this.Q == null) {
            this.Q = new bf(u());
            this.Q.i = this.Q.h;
            new Thread(new as(this, this.Q)).start();
        }
        this.Q.setImageMultipleDiaplay(true);
        String str = this.Q.h;
        ag();
        ah();
    }

    /* access modifiers changed from: protected */
    public final void Q() {
        this.T = new aa(this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.android.a.a.aa.a(int, java.util.Collection):void
     arg types: [int, java.util.List]
     candidates:
      com.immomo.momo.android.a.a.a(int, java.lang.Object):void
      com.immomo.momo.android.a.a.a(java.lang.Object, int):void
      com.immomo.momo.android.a.a.a(java.util.Collection, boolean):void
      com.immomo.momo.android.a.a.aa.a(int, java.util.Collection):void */
    /* access modifiers changed from: protected */
    public final void R() {
        this.T.b();
        this.T.a(0, (Collection) ai());
        if (!this.ac) {
            this.l.a();
        }
        this.P.c(this.Q.h);
        this.l.setAdapter((ListAdapter) this.T);
    }

    /* access modifiers changed from: protected */
    public final void S() {
        a(800, "actions.usermessage", "actions.himessage", "actions.userlocalmsg", "actions.message.status", "actions.emoteupdates", "actions.logger");
        this.S = new j(this);
        this.N = new i(this);
        this.M = new w(this);
        this.S.a(new ak(this));
        this.N.a(new al(this));
        this.M.a(this.aa);
    }

    /* access modifiers changed from: protected */
    public final void T() {
        aj();
        if (this.R) {
            g.d().q();
        } else {
            g.d().n();
        }
    }

    /* access modifiers changed from: protected */
    public final int U() {
        return R.layout.activity_chat;
    }

    /* access modifiers changed from: protected */
    public final void V() {
        String[] strArr;
        if (this.ab == null || !this.ab.g()) {
            A();
            z();
            if (this.E) {
                strArr = new String[]{"语音收听方式", "创建多人对话", "同步聊天消息", "设置聊天背景"};
                this.ab = new at(this, this.n, strArr);
            } else {
                strArr = new String[]{"语音收听方式", "设置聊天背景"};
                this.ab = new at(this, this.n, strArr);
            }
            this.ab.a(new am(this, strArr));
            this.ab.d();
        }
    }

    /* access modifiers changed from: protected */
    public final void W() {
        new k("C", "C5103").e();
        Intent intent = new Intent();
        intent.setClass(this, OtherProfileActivity.class);
        intent.putExtra("tag", "local");
        intent.putExtra("momoid", getIntent().getStringExtra("remoteUserID"));
        intent.setFlags(603979776);
        startActivity(intent);
    }

    /* access modifiers changed from: protected */
    public final void X() {
        this.l.setLoadingVisible(true);
        b(new ay(this, this));
    }

    public final void Y() {
        this.j.sendEmptyMessage(PurchaseCode.BILL_DIALOG_SHOWERROR);
    }

    /* access modifiers changed from: protected */
    public final void Z() {
        this.E = "both".equals(this.Q.P);
        if (!c.c() || !this.Q.h.equals("1602")) {
            new at(this).start();
            new au(this).start();
        }
    }

    /* access modifiers changed from: protected */
    public final Message a(File file) {
        ce.a();
        Message a2 = ce.a(file, this.Q, (String) null, 1, this.f.b() ? 1 : 0);
        if (ak()) {
            i(a2);
        }
        return a2;
    }

    /* JADX WARN: Type inference failed for: r1v0 */
    /* JADX WARN: Type inference failed for: r1v20, types: [boolean] */
    /* JADX WARN: Type inference failed for: r1v23 */
    /* JADX WARN: Type inference failed for: r1v26, types: [int] */
    /* JADX WARN: Type inference failed for: r1v27 */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean a(android.os.Bundle r13, java.lang.String r14) {
        /*
            r12 = this;
            r11 = 2
            r9 = 0
            r8 = 6
            r2 = 1
            r1 = 0
            java.lang.String r0 = "actions.usermessage"
            boolean r0 = r14.equals(r0)
            if (r0 != 0) goto L_0x001e
            java.lang.String r0 = "actions.himessage"
            boolean r0 = r14.equals(r0)
            if (r0 != 0) goto L_0x001e
            java.lang.String r0 = "actions.logger"
            boolean r0 = r14.equals(r0)
            if (r0 == 0) goto L_0x007b
        L_0x001e:
            java.lang.String r0 = "remoteuserid"
            java.lang.String r0 = r13.getString(r0)
            com.immomo.momo.service.bean.bf r2 = r12.Q
            java.lang.String r2 = r2.h
            boolean r0 = r2.equals(r0)
            if (r0 != 0) goto L_0x002f
        L_0x002e:
            return r1
        L_0x002f:
            java.lang.String r0 = "messagearray"
            java.io.Serializable r0 = r13.getSerializable(r0)
            java.util.List r0 = (java.util.List) r0
            if (r0 == 0) goto L_0x002e
            boolean r2 = r0.isEmpty()
            if (r2 != 0) goto L_0x002e
            java.util.Iterator r2 = r0.iterator()
        L_0x0043:
            boolean r1 = r2.hasNext()
            if (r1 != 0) goto L_0x005c
            com.immomo.momo.android.a.a.aa r1 = r12.T
            r12.a(r1, r0)
            boolean r0 = r12.q()
            if (r0 == 0) goto L_0x0057
            r12.aj()
        L_0x0057:
            boolean r1 = r12.q()
            goto L_0x002e
        L_0x005c:
            java.lang.Object r1 = r2.next()
            com.immomo.momo.service.bean.Message r1 = (com.immomo.momo.service.bean.Message) r1
            java.lang.String r3 = r1.msgId
            int r4 = r1.contentType
            r5 = 5
            if (r4 == r5) goto L_0x0077
            int r4 = r1.status
            r5 = 4
            if (r4 == r5) goto L_0x0077
            boolean r4 = r1.receive
            if (r4 == 0) goto L_0x0077
            java.util.List r4 = r12.F
            r4.add(r3)
        L_0x0077:
            r12.h(r1)
            goto L_0x0043
        L_0x007b:
            java.lang.String r0 = "actions.message.status"
            boolean r0 = r14.equals(r0)
            if (r0 == 0) goto L_0x01d3
            java.lang.String r0 = "chattype"
            int r0 = r13.getInt(r0)
            if (r0 != r2) goto L_0x0217
            java.lang.String r0 = "remoteuserid"
            java.lang.String r0 = r13.getString(r0)
            com.immomo.momo.service.bean.bf r3 = r12.Q
            java.lang.String r3 = r3.h
            boolean r0 = r3.equals(r0)
            if (r0 == 0) goto L_0x002e
            java.lang.String r0 = "stype"
            java.lang.String r3 = r13.getString(r0)
            java.lang.String r0 = "msgreaded"
            boolean r0 = r0.equals(r3)
            if (r0 == 0) goto L_0x00fe
            java.lang.String r0 = "msgid"
            java.lang.String[] r3 = r13.getStringArray(r0)
            boolean r0 = android.support.v4.b.a.a(r3)
            if (r0 != 0) goto L_0x00dc
            com.immomo.momo.android.a.a.aa r0 = r12.T
            java.util.List r0 = r0.a()
            java.util.Iterator r1 = r0.iterator()
        L_0x00bf:
            boolean r0 = r1.hasNext()
            if (r0 != 0) goto L_0x00cb
        L_0x00c5:
            r12.Y()
        L_0x00c8:
            r1 = r2
            goto L_0x002e
        L_0x00cb:
            java.lang.Object r0 = r1.next()
            com.immomo.momo.service.bean.Message r0 = (com.immomo.momo.service.bean.Message) r0
            boolean r3 = r0.receive
            if (r3 != 0) goto L_0x00bf
            int r3 = r0.status
            if (r3 != r11) goto L_0x00bf
            r0.status = r8
            goto L_0x00bf
        L_0x00dc:
            com.immomo.momo.android.a.a.aa r0 = r12.T
            java.util.List r4 = r0.a()
        L_0x00e2:
            int r0 = r3.length
            if (r1 >= r0) goto L_0x00c5
            com.immomo.momo.service.bean.Message r0 = new com.immomo.momo.service.bean.Message
            r5 = r3[r1]
            r0.<init>(r5)
            int r0 = r4.indexOf(r0)
            if (r0 < 0) goto L_0x00fa
            java.lang.Object r0 = r4.get(r0)
            com.immomo.momo.service.bean.Message r0 = (com.immomo.momo.service.bean.Message) r0
            r0.status = r8
        L_0x00fa:
            int r0 = r1 + 1
            r1 = r0
            goto L_0x00e2
        L_0x00fe:
            java.lang.String r0 = "msgid"
            java.lang.String r4 = r13.getString(r0)
            boolean r0 = android.support.v4.b.a.f(r4)
            if (r0 == 0) goto L_0x00c8
            com.immomo.momo.android.a.a.aa r0 = r12.T
            com.immomo.momo.service.bean.Message r5 = new com.immomo.momo.service.bean.Message
            r5.<init>(r4)
            int r0 = r0.f(r5)
            com.immomo.momo.util.m r5 = r12.L
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            java.lang.String r7 = "position:"
            r6.<init>(r7)
            java.lang.StringBuilder r6 = r6.append(r0)
            java.lang.String r7 = "  serverType:"
            java.lang.StringBuilder r6 = r6.append(r7)
            java.lang.StringBuilder r6 = r6.append(r3)
            java.lang.String r6 = r6.toString()
            r5.a(r6)
            if (r0 < 0) goto L_0x00c8
            com.immomo.momo.android.a.a.aa r5 = r12.T
            java.lang.Object r0 = r5.getItem(r0)
            com.immomo.momo.service.bean.Message r0 = (com.immomo.momo.service.bean.Message) r0
            java.lang.String r5 = "msgsuccess"
            boolean r5 = r5.equals(r3)
            if (r5 == 0) goto L_0x0150
            int r1 = r0.status
            if (r1 == r8) goto L_0x014b
            r0.status = r11
        L_0x014b:
            r12.Y()
            goto L_0x00c8
        L_0x0150:
            java.lang.String r5 = "msgdistance"
            boolean r5 = r5.equals(r3)
            if (r5 == 0) goto L_0x01b1
            java.lang.String r3 = "distance"
            r4 = -1
            int r3 = r13.getInt(r3, r4)
            float r3 = (float) r3
            r0.distance = r3
            java.lang.String r3 = "dtime"
            r4 = -1
            long r4 = r13.getLong(r3, r4)
            int r3 = (r4 > r9 ? 1 : (r4 == r9 ? 0 : -1))
            if (r3 <= 0) goto L_0x01af
            java.util.Date r3 = new java.util.Date     // Catch:{ Exception -> 0x021d }
            r3.<init>(r4)     // Catch:{ Exception -> 0x021d }
        L_0x0173:
            r0.distanceTime = r3     // Catch:{ Exception -> 0x021d }
        L_0x0175:
            com.immomo.momo.service.bean.bf r3 = r12.Q
            long r6 = r3.a()
            int r3 = (r6 > r4 ? 1 : (r6 == r4 ? 0 : -1))
            if (r3 < 0) goto L_0x018d
            com.immomo.momo.service.bean.bf r3 = r12.Q
            long r6 = r3.a()
            int r3 = (r6 > r9 ? 1 : (r6 == r9 ? 0 : -1))
            if (r3 == 0) goto L_0x014b
            int r3 = (r4 > r9 ? 1 : (r4 == r9 ? 0 : -1))
            if (r3 != 0) goto L_0x014b
        L_0x018d:
            com.immomo.momo.service.bean.bf r3 = r12.Q
            java.lang.String r6 = "deviation"
            int r6 = r13.getInt(r6, r1)
            if (r6 != r2) goto L_0x0198
            r1 = r2
        L_0x0198:
            r3.V = r1
            com.immomo.momo.service.bean.bf r1 = r12.Q
            float r3 = r0.distance
            r1.a(r3)
            com.immomo.momo.service.bean.bf r1 = r12.Q
            r1.a(r4)
            r12.ah()
            float r0 = r0.distance
            r12.a(r0, r4)
            goto L_0x014b
        L_0x01af:
            r3 = 0
            goto L_0x0173
        L_0x01b1:
            java.lang.String r1 = "msgsending"
            boolean r1 = r1.equals(r3)
            if (r1 == 0) goto L_0x01c6
            r0.status = r2
            com.immomo.momo.service.ae r1 = r12.P
            com.immomo.momo.service.bean.Message r1 = r1.h(r4)
            java.lang.String r1 = r1.fileName
            r0.fileName = r1
            goto L_0x014b
        L_0x01c6:
            java.lang.String r1 = "msgfailed"
            boolean r1 = r1.equals(r3)
            if (r1 == 0) goto L_0x014b
            r1 = 3
            r0.status = r1
            goto L_0x014b
        L_0x01d3:
            java.lang.String r0 = "actions.emoteupdates"
            boolean r0 = r14.equals(r0)
            if (r0 == 0) goto L_0x01e8
            com.immomo.momo.util.m r0 = r12.L
            java.lang.String r1 = "Action_EmoteUpdates---------------"
            r0.a(r1)
            r12.Y()
            r1 = r2
            goto L_0x002e
        L_0x01e8:
            java.lang.String r0 = "actions.userlocalmsg"
            boolean r0 = r14.equals(r0)
            if (r0 == 0) goto L_0x0217
            java.lang.String r0 = "remoteuserid"
            java.lang.String r0 = r13.getString(r0)
            com.immomo.momo.service.bean.bf r3 = r12.Q
            java.lang.String r3 = r3.h
            boolean r0 = r3.equals(r0)
            if (r0 == 0) goto L_0x002e
            java.lang.String r0 = "messageobj"
            java.io.Serializable r0 = r13.getSerializable(r0)
            com.immomo.momo.service.bean.Message r0 = (com.immomo.momo.service.bean.Message) r0
            boolean r1 = r0.receive
            if (r1 == 0) goto L_0x020f
            r12.h(r0)
        L_0x020f:
            com.immomo.momo.android.a.a.aa r1 = r12.T
            r12.a(r1, r0)
            r1 = r2
            goto L_0x002e
        L_0x0217:
            boolean r1 = super.a(r13, r14)
            goto L_0x002e
        L_0x021d:
            r3 = move-exception
            goto L_0x0175
        */
        throw new UnsupportedOperationException("Method not decompiled: com.immomo.momo.android.activity.message.ChatActivity.a(android.os.Bundle, java.lang.String):boolean");
    }

    /* access modifiers changed from: protected */
    public final boolean a(Message message) {
        int f = this.T.f(message) + 1;
        if (f < this.T.getCount()) {
            Message message2 = (Message) this.T.getItem(f);
            if (message2.receive && message2.contentType == 4 && !message2.isAudioPlayed) {
                com.immomo.momo.android.a.a.d.a(message2, this);
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public final List ac() {
        return this.P.g(this.Q.h);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.service.bean.Message.<init>(boolean, int):void
     arg types: [int, int]
     candidates:
      com.immomo.momo.service.bean.Message.<init>(int, boolean):void
      com.immomo.momo.service.bean.Message.<init>(boolean, int):void */
    /* access modifiers changed from: protected */
    public final Message ad() {
        Message message = new Message(true, 2);
        message.remoteId = this.Q.h;
        message.distance = this.Q.d();
        ce.a().a(message, new x(this, message), (String) null, 1, this.f.b() ? 1 : 0);
        if (ak()) {
            i(message);
        }
        return message;
    }

    public final void b(Message message) {
        this.P.c(message);
        this.T.c(message);
    }

    /* access modifiers changed from: protected */
    public final Message c(int i) {
        return ce.a().a(this.H, i, this.Q, null, 1, this.f.b() ? 1 : 0);
    }

    /* access modifiers changed from: protected */
    public final Message c(String str) {
        ce.a();
        Message a2 = ce.a(str, this.Q, (String) null, 1, this.f.b() ? 1 : 0);
        if (ak()) {
            i(a2);
        }
        return a2;
    }

    /* access modifiers changed from: protected */
    public final void c(Message message) {
        this.P.a(message.msgId, message.status);
    }

    /* access modifiers changed from: protected */
    public final Message d(String str) {
        ce.a();
        Message b = ce.b(str, this.Q, null, 1, this.f.b() ? 1 : 0);
        if (ak()) {
            i(b);
        }
        return b;
    }

    /* access modifiers changed from: protected */
    public final void d() {
        super.d();
        if (!this.O.m(this.Q.h)) {
            this.O.n(this.Q.h);
            if (this.T.getCount() <= 0 && !this.E) {
                n nVar = new n(this);
                nVar.setTitle((int) R.string.dialog_title_newchat_waring);
                nVar.a(0, (int) R.string.dialog_button_newchat_waring, (DialogInterface.OnClickListener) null);
                View inflate = g.o().inflate((int) R.layout.dialog_new_chat_waring, (ViewGroup) null);
                nVar.setContentView(inflate);
                ((TextView) inflate.findViewById(R.id.dialog_msg_newchat_waring_info_textview)).setText(g.d().f668a.b("momo_deny_info", getString(R.string.dialog_msg_newchat_waring_text_default)));
                nVar.setCanceledOnTouchOutside(false);
                nVar.show();
            }
        }
        e(this.Q.aR);
    }

    /* access modifiers changed from: protected */
    public final void d(Message message) {
        this.P.b(message);
    }

    /* access modifiers changed from: protected */
    public final void e() {
        super.e();
        this.L.a((Object) "onInitialize!!!");
    }

    /* access modifiers changed from: protected */
    public final void f(Message message) {
        if (message != null) {
            if (this.K) {
                this.K = false;
                new k("C", "C5101").e();
            }
            this.T.a(message);
            super.f(message);
        }
    }

    public boolean handleMessage(android.os.Message message) {
        switch (message.what) {
            case PurchaseCode.BILL_DIALOG_SHOWERROR /*402*/:
                this.T.notifyDataSetChanged();
                return true;
            case 1600021:
                ah();
                return true;
            default:
                return super.handleMessage(message);
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i2 == -1) {
            if (i == 263) {
                d();
                return;
            } else if (i == 264) {
                String stringExtra = intent.getStringExtra("key_resourseid");
                e(stringExtra);
                this.Q.aR = stringExtra;
                this.O.c(stringExtra, this.i);
                return;
            }
        }
        super.onActivityResult(i, i2, intent);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        a(this.S);
        a(this.N);
        a(this.M);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (this.Z != null) {
            this.Z.cancel();
        }
        if (this.Y != null) {
            this.Y.purge();
            this.Y.cancel();
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.L.a((Object) "onResume");
        this.Y = new Timer();
        this.Z = new ar(this);
        this.Y.schedule(this.Z, 60000, 60000);
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        this.L.a((Object) "onStart");
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        if (r()) {
            Bundle bundle = new Bundle();
            bundle.putString("sessionid", this.Q.h);
            bundle.putInt("sessiontype", 0);
            g.d().a(bundle, "action.sessionchanged");
            if (this.R && !"from_hiactivity".equals(this.G)) {
                Bundle bundle2 = new Bundle();
                bundle2.putString("sessionid", "-2222");
                bundle2.putInt("sessiontype", 1);
                g.d().a(bundle2, "action.sessionchanged");
            }
        }
        this.L.a((Object) "onStop...");
    }

    /* access modifiers changed from: protected */
    public final String u() {
        return getIntent().getStringExtra("remoteUserID");
    }

    /* access modifiers changed from: protected */
    public final void x() {
        super.x();
        this.U = g.o().inflate((int) R.layout.common_distanceaction, (ViewGroup) null);
        this.V = (ImageView) this.U.findViewById(R.id.iv_background);
        this.W = (ImageView) this.U.findViewById(R.id.iv_distanceic);
        this.X = (ImageView) this.U.findViewById(R.id.iv_animationbg);
        this.V.setOnClickListener(new ao(this));
        this.m.b(this.U);
        this.m.getLeftLogoView().setVisibility(8);
        findViewById(R.id.message_plusbar_gift).setVisibility(0);
        findViewById(R.id.message_plusbar_gift).setOnClickListener(new ap(this));
    }
}
