package com.immomo.momo.android.activity;

import android.app.ActivityGroup;
import android.app.LocalActivityManager;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import com.immomo.momo.R;
import com.immomo.momo.util.m;
import java.util.ArrayList;
import java.util.List;

public class lg extends ActivityGroup {

    /* renamed from: a  reason: collision with root package name */
    protected m f1811a = new m(getClass().getSimpleName());
    private List b = null;
    private ViewGroup c = null;
    private int d = -1;
    private LocalActivityManager e = null;
    private View f = null;
    private Animation g = null;
    private Animation h = null;

    public final void a(int i) {
        if (this.c == null) {
            this.c = (ViewGroup) findViewById(R.id.tabcontent);
        }
        if (i != this.d && !this.b.isEmpty()) {
            if (i < 0) {
                i = 0;
            } else if (i >= this.b.size()) {
                i = this.b.size() - 1;
            }
            Class cls = (Class) this.b.get(i);
            if (cls != null) {
                Intent intent = new Intent(getApplicationContext(), cls);
                intent.addFlags(67108864);
                Window startActivity = this.e.startActivity(cls.getName(), intent);
                View decorView = startActivity != null ? startActivity.getDecorView() : null;
                View view = this.f;
                this.f = decorView;
                if (this.f != null) {
                    this.f.setVisibility(0);
                    this.f.setFocusableInTouchMode(true);
                    ((ViewGroup) this.f).setDescendantFocusability(262144);
                    this.c.addView(this.f, new ViewGroup.LayoutParams(-1, -1));
                }
                if (!(view == decorView || view == null)) {
                    if (this.h != null) {
                        view.startAnimation(this.h);
                    }
                    this.c.removeView(view);
                    if (this.g != null) {
                        this.f.startAnimation(this.g);
                    }
                }
                this.f.requestFocus();
                this.d = i;
            }
        }
    }

    public final void a(Animation animation, Animation animation2) {
        this.g = animation;
        this.h = animation2;
    }

    /* access modifiers changed from: protected */
    public final void a(Class... clsArr) {
        for (Class add : clsArr) {
            this.b.add(add);
        }
    }

    public void onContentChanged() {
        super.onContentChanged();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.b = new ArrayList();
        this.e = getLocalActivityManager();
    }

    /* access modifiers changed from: protected */
    public void onPostCreate(Bundle bundle) {
        super.onPostCreate(bundle);
        if (bundle != null) {
            a(bundle.getInt("currentTab", 0));
        } else if (this.d == -1) {
            a(0);
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        bundle.putInt("currentTab", this.d);
        super.onSaveInstanceState(bundle);
    }
}
