package com.immomo.momo.android.view;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.HeaderViewListAdapter;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.immomo.momo.R;
import com.immomo.momo.android.plugin.cropimage.q;
import com.immomo.momo.g;
import com.immomo.momo.util.m;

public class HandyListView extends ListView {

    /* renamed from: a  reason: collision with root package name */
    private View f2631a = null;
    private View b = null;
    private View c = null;
    private q d = null;
    private int e = -1;
    private int f = 8;
    /* access modifiers changed from: private */
    public boolean g = false;
    private boolean h = true;
    private m i = new m("HandyListView");
    /* access modifiers changed from: private */
    public ListAdapter j = null;
    /* access modifiers changed from: private */
    public AbsListView.OnScrollListener k = null;
    private AdapterView.OnItemLongClickListener l = null;
    private cd m = null;
    private GestureDetector n = null;
    /* access modifiers changed from: private */
    public float o = 0.0f;
    private int p = 0;
    private int q = 0;
    private View r = null;
    private Drawable s = null;
    /* access modifiers changed from: private */
    public boolean t = false;
    private q u = null;

    public HandyListView(Context context) {
        super(context);
        a();
    }

    public HandyListView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a();
    }

    public HandyListView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        a();
    }

    private void a() {
        this.n = new GestureDetector(getContext(), new bg(this, (byte) 0));
        super.setOnScrollListener(new be(this));
    }

    public final void a(View view) {
        this.f2631a = view;
        if (this.f2631a != null) {
            this.f2631a.setVisibility(8);
            LinearLayout linearLayout = new LinearLayout(getContext());
            linearLayout.setLayoutParams(new AbsListView.LayoutParams(-1, -2));
            linearLayout.setGravity(17);
            linearLayout.addView(this.f2631a);
            linearLayout.setBackgroundDrawable(view.getBackground());
            addHeaderView(linearLayout);
        }
    }

    /* access modifiers changed from: protected */
    public final void a(boolean z) {
        if (this.f2631a == null) {
            return;
        }
        if (z) {
            this.f2631a.setVisibility(0);
        } else {
            this.f2631a.setVisibility(8);
        }
    }

    public void addFooterView(View view) {
        super.addFooterView(view);
    }

    public final boolean d() {
        return this.h;
    }

    /* access modifiers changed from: protected */
    public boolean e() {
        return true;
    }

    public final boolean f() {
        return this.m != null && this.m.g();
    }

    public final boolean g() {
        return this.g && Math.abs(this.o) >= ((float) this.p);
    }

    public ListAdapter getBaseAdapter() {
        ListAdapter adapter = super.getAdapter();
        return adapter instanceof HeaderViewListAdapter ? ((HeaderViewListAdapter) adapter).getWrappedAdapter() : adapter;
    }

    public View getEmptyView() {
        return this.b != null ? this.b : super.getEmptyView();
    }

    public int getFastVelocity() {
        return this.p;
    }

    public int getListPaddingBottom() {
        return this.q;
    }

    public cd getMultipleSelector() {
        return this.m;
    }

    public AdapterView.OnItemLongClickListener getOnItemLongClickListenerInWrapper() {
        return this.l;
    }

    public float getScrollVelocity() {
        return this.o;
    }

    public int getSolidColor() {
        return this.e == -1 ? super.getSolidColor() : this.e;
    }

    public final boolean h() {
        return this.g;
    }

    public final void i() {
        if (getAdapter() != null && getAdapter().getCount() > 0) {
            if (Build.VERSION.SDK_INT < 8) {
                setSelection(0);
                return;
            }
            if (getFirstVisiblePosition() > 5) {
                setSelection(5);
            }
            if (g.a()) {
                smoothScrollToPositionFromTop(0, 0);
            } else {
                smoothScrollToPosition(0);
            }
        }
    }

    public final void j() {
        if (this.r == null) {
            this.r = inflate(getContext(), R.layout.listitem_blank, null);
            this.r.setLayoutParams(new AbsListView.LayoutParams(-1, this.q));
            addFooterView(this.r);
            if (this.s != null) {
                this.r.setBackgroundDrawable(this.s);
                return;
            }
            return;
        }
        removeFooterView(this.r);
        addFooterView(this.r);
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        return this.u != null ? this.u.a() : super.onInterceptTouchEvent(motionEvent);
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i2, int i3, int i4, int i5) {
        super.onSizeChanged(i2, i3, i4, i5);
        if (this.d != null) {
            q qVar = this.d;
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        this.n.onTouchEvent(motionEvent);
        return super.onTouchEvent(motionEvent);
    }

    public boolean performItemClick(View view, int i2, long j2) {
        int headerViewsCount = i2 - getHeaderViewsCount();
        if (this.t || this.j == null || headerViewsCount < 0 || headerViewsCount >= this.j.getCount()) {
            return true;
        }
        return super.performItemClick(view, headerViewsCount, j2);
    }

    public void setAdapter(ListAdapter listAdapter) {
        if (this.c != null) {
            this.c.setVisibility(8);
        }
        if (this.m != null) {
            this.m.a((ce) listAdapter);
        }
        this.g = false;
        setVisibility(0);
        super.setEmptyView(this.b);
        this.b = null;
        super.setAdapter(listAdapter);
        a(listAdapter == null || listAdapter.isEmpty());
        if (listAdapter != null) {
            listAdapter.registerDataSetObserver(new bf(this));
        }
        this.j = listAdapter;
        if (this.q > 0 && e()) {
            this.i.a((Object) "addPaddingBottomView~~~~~~~~~~~~~~~~~~~~~~~~~~");
            j();
        }
    }

    public void setEmptyView(View view) {
        this.b = view;
        if (this.b != null) {
            this.b.setVisibility(8);
        }
    }

    public void setEmptyViewVisible(boolean z) {
        if (this.f2631a != null) {
            this.f2631a.setVisibility(z ? 0 : 8);
        }
    }

    public void setFadingEdgeColor(int i2) {
        this.e = i2;
    }

    public void setFastVelocity(int i2) {
        this.p = i2;
    }

    public void setInterceptItemClick(boolean z) {
        this.t = z;
    }

    public void setListPaddingBackground(Drawable drawable) {
        this.s = drawable;
        if (this.r != null && this.s != null) {
            this.r.setBackgroundDrawable(drawable);
        }
    }

    public void setListPaddingBottom(int i2) {
        if (i2 == -3) {
            this.q = (int) getContext().getResources().getDimension(R.dimen.bottomtabbar);
        } else {
            this.q = i2;
        }
    }

    public void setListViewHiddenValue(int i2) {
        this.f = i2;
    }

    public void setLoadingListView(View view) {
        this.c = view;
        if (this.c != null) {
            setVisibility(this.f);
            this.c.setVisibility(0);
        }
    }

    public void setMultipleSelector(cd cdVar) {
        if (cdVar == null || this.j == null || (this.j instanceof ce)) {
            if (this.m != cdVar) {
                if (this.m != null) {
                    this.m.f();
                }
                if (cdVar != null) {
                    cdVar.c();
                    cdVar.a((ce) this.j);
                }
            }
            this.m = cdVar;
            return;
        }
        throw new RuntimeException(new IllegalStateException("Adater mast be extends of MultipleSelectionAdapter."));
    }

    public void setOnInterceptTouchListener$43dcc6cd(q qVar) {
        this.u = qVar;
    }

    public void setOnItemLongClickListener(AdapterView.OnItemLongClickListener onItemLongClickListener) {
        this.l = onItemLongClickListener;
        super.setOnItemLongClickListener(new bh(this, this.l));
    }

    public void setOnScrollListener(AbsListView.OnScrollListener onScrollListener) {
        this.k = onScrollListener;
    }

    public void setOnSizeChangedListener$3d25535(q qVar) {
        this.d = qVar;
    }

    public void setScorllEndReflush(boolean z) {
        this.h = z;
    }

    public void setScrolling(boolean z) {
        this.g = z;
    }
}
