package com.immomo.momo.android.c;

import java.util.concurrent.BlockingQueue;

final class ai implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private BlockingQueue f2374a = null;
    private boolean b = true;

    public ai(BlockingQueue blockingQueue) {
        this.f2374a = blockingQueue;
    }

    public final void a() {
        this.b = false;
        try {
            this.f2374a.put(new ah());
        } catch (InterruptedException e) {
        }
    }

    public final void run() {
        while (true) {
            try {
                if (this.b || this.f2374a.size() > 0) {
                    t tVar = (t) this.f2374a.take();
                    if (tVar instanceof ah) {
                        this.b = false;
                    } else {
                        tVar.a();
                    }
                } else {
                    return;
                }
            } catch (InterruptedException e) {
                return;
            }
        }
    }
}
