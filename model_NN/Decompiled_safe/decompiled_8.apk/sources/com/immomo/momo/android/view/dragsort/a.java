package com.immomo.momo.android.view.dragsort;

import android.graphics.Point;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import mm.purchasesdk.PurchaseCode;

public final class a extends p implements GestureDetector.OnGestureListener, View.OnTouchListener {

    /* renamed from: a  reason: collision with root package name */
    private int f2789a = 0;
    private boolean b = true;
    private int c;
    /* access modifiers changed from: private */
    public boolean d = false;
    /* access modifiers changed from: private */
    public boolean e = false;
    private GestureDetector f;
    private GestureDetector g;
    private int h;
    private int i = -1;
    private int j = -1;
    private int k = -1;
    private int[] l = new int[2];
    private int m;
    private int n;
    private int o;
    private int p;
    private boolean q = false;
    /* access modifiers changed from: private */
    public float r = 500.0f;
    private int s;
    private int t;
    private int u;
    private boolean v;
    /* access modifiers changed from: private */
    public DragSortListView w;
    /* access modifiers changed from: private */
    public int x;
    private GestureDetector.OnGestureListener y = new b(this);

    public a(DragSortListView dragSortListView, int i2, int i3, int i4, int i5, int i6) {
        super(dragSortListView);
        this.w = dragSortListView;
        this.f = new GestureDetector(dragSortListView.getContext(), this);
        this.g = new GestureDetector(dragSortListView.getContext(), this.y);
        this.g.setIsLongpressEnabled(false);
        this.h = ViewConfiguration.get(dragSortListView.getContext()).getScaledTouchSlop();
        this.s = i2;
        this.t = i5;
        this.u = i6;
        this.c = i4;
        this.f2789a = i3;
    }

    private int a(MotionEvent motionEvent, int i2) {
        int pointToPosition = this.w.pointToPosition((int) motionEvent.getX(), (int) motionEvent.getY());
        int headerViewsCount = this.w.getHeaderViewsCount();
        int footerViewsCount = this.w.getFooterViewsCount();
        int count = this.w.getCount();
        if (pointToPosition != -1 && pointToPosition >= headerViewsCount && pointToPosition < count - footerViewsCount) {
            View childAt = this.w.getChildAt(pointToPosition - this.w.getFirstVisiblePosition());
            int rawX = (int) motionEvent.getRawX();
            int rawY = (int) motionEvent.getRawY();
            View findViewById = i2 == 0 ? childAt : childAt.findViewById(i2);
            if (findViewById != null) {
                findViewById.getLocationOnScreen(this.l);
                if (rawX > this.l[0] && rawY > this.l[1] && rawX < this.l[0] + findViewById.getWidth()) {
                    if (rawY < findViewById.getHeight() + this.l[1]) {
                        this.m = childAt.getLeft();
                        this.n = childAt.getTop();
                        return pointToPosition;
                    }
                }
            }
        }
        return -1;
    }

    private boolean a(int i2, int i3, int i4) {
        int i5 = 0;
        if (this.b && !this.e) {
            i5 = 12;
        }
        if (this.d && this.e) {
            i5 = i5 | 1 | 2;
        }
        this.q = this.w.a(i2 - this.w.getHeaderViewsCount(), i5, i3, i4);
        return this.q;
    }

    public final void a(Point point) {
        if (this.d && this.e) {
            this.x = point.x;
        }
    }

    public final void a(boolean z) {
        this.b = z;
    }

    public final void b(boolean z) {
        this.d = z;
    }

    public final boolean onDown(MotionEvent motionEvent) {
        if (this.d && this.c == 0) {
            this.k = a(motionEvent, this.t);
        }
        this.i = a(motionEvent, this.s);
        boolean a2 = (this.i == -1 || this.f2789a != 0) ? false : a(this.i, ((int) motionEvent.getX()) - this.m, (((int) motionEvent.getY()) - this.n) - 20);
        this.e = false;
        this.v = true;
        this.x = 0;
        this.j = this.c == 1 ? a(motionEvent, this.u) : -1;
        return a2;
    }

    public final boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f2, float f3) {
        return false;
    }

    public final void onLongPress(MotionEvent motionEvent) {
        if (this.i != -1 && this.f2789a == 2) {
            this.w.performHapticFeedback(0);
            a(this.i, this.o - this.m, this.p - this.n);
        }
    }

    public final boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent2, float f2, float f3) {
        if (motionEvent == null || motionEvent2 == null) {
            return false;
        }
        int x2 = (int) motionEvent.getX();
        int y2 = (int) motionEvent.getY();
        int x3 = (int) motionEvent2.getX();
        int y3 = (int) motionEvent2.getY();
        int i2 = x3 - this.m;
        int i3 = y3 - this.n;
        if (!this.v || this.q) {
            return false;
        }
        if (this.i == -1 && this.j == -1) {
            return false;
        }
        if (this.i != -1) {
            if (this.f2789a == 1 && Math.abs(y3 - y2) > this.h && this.b) {
                return a(this.i, i2, i3);
            }
            if (this.f2789a == 0 || Math.abs(x3 - x2) <= this.h || !this.d) {
                return false;
            }
            this.e = true;
            return a(this.j, i2, i3);
        } else if (this.j == -1) {
            return false;
        } else {
            if (Math.abs(x3 - x2) > this.h && this.d) {
                this.e = true;
                return a(this.j, i2, i3);
            } else if (Math.abs(y3 - y2) <= this.h) {
                return false;
            } else {
                this.v = false;
                return false;
            }
        }
    }

    public final void onShowPress(MotionEvent motionEvent) {
    }

    public final boolean onSingleTapUp(MotionEvent motionEvent) {
        if (!this.d || this.c != 0 || this.k == -1) {
            return true;
        }
        this.w.b(this.k - this.w.getHeaderViewsCount());
        return true;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public final boolean onTouch(View view, MotionEvent motionEvent) {
        if (!this.w.s() || this.w.c()) {
            return false;
        }
        if (this.f.onTouchEvent(motionEvent)) {
            return true;
        }
        if (this.d && this.q && this.c == 1 && this.g.onTouchEvent(motionEvent)) {
            return true;
        }
        switch (motionEvent.getAction() & PurchaseCode.AUTH_INVALID_APP) {
            case 0:
                this.o = (int) motionEvent.getX();
                this.p = (int) motionEvent.getY();
                break;
            case 1:
                if (this.d && this.e) {
                    if ((this.x >= 0 ? this.x : -this.x) > this.w.getWidth() / 2) {
                        this.w.a(0.0f);
                    }
                }
                this.e = false;
                this.q = false;
                break;
            case 3:
                this.e = false;
                this.q = false;
                break;
        }
        return false;
    }
}
