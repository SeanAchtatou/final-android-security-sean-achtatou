package com.immomo.momo.android.map;

import android.location.Location;
import com.immomo.momo.R;
import com.immomo.momo.android.b.p;
import com.immomo.momo.android.b.z;
import com.immomo.momo.util.ao;

final class r extends p {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ GeoAMapActivity f2512a;

    r(GeoAMapActivity geoAMapActivity) {
        this.f2512a = geoAMapActivity;
    }

    public final void a(Location location, int i, int i2, int i3) {
        if (this.b) {
            if (z.a(location)) {
                this.f2512a.p.post(new s(this, location));
                return;
            }
            this.f2512a.a();
            if (i2 == 104) {
                ao.g(R.string.errormsg_network_unfind);
            } else {
                ao.g(R.string.errormsg_location_nearby_failed);
            }
        }
    }
}
