package com.immomo.momo.android.activity.group;

import android.view.View;
import android.widget.AdapterView;

final class bq implements AdapterView.OnItemClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ GroupFeedsActivity f1564a;
    private final /* synthetic */ String[] b;

    bq(GroupFeedsActivity groupFeedsActivity, String[] strArr) {
        this.f1564a = groupFeedsActivity;
        this.b = strArr;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        if ("群活动".equals(this.b[i])) {
            GroupFeedsActivity.q(this.f1564a);
        } else if ("群留言".equals(this.b[i])) {
            GroupFeedsActivity.m(this.f1564a);
        }
    }
}
