package com.immomo.momo.android.activity;

import android.view.View;
import com.immomo.momo.R;
import com.immomo.momo.android.view.a.n;

final class ai implements View.OnClickListener {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public /* synthetic */ ah f1025a;

    ai(ah ahVar) {
        this.f1025a = ahVar;
    }

    public final void onClick(View view) {
        if (view == this.f1025a.h) {
            ah ahVar = this.f1025a;
            ah.i();
            an anVar = (an) view.getTag(R.id.tag_item);
            if (anVar != null && anVar.b == 1008) {
                this.f1025a.a(n.a(this.f1025a, (int) R.string.dialog_upload_network, new aj(this)));
            }
            this.f1025a.a(anVar);
        }
    }
}
