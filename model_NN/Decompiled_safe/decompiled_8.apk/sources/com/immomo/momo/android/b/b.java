package com.immomo.momo.android.b;

import android.os.Handler;
import android.os.Message;
import mm.purchasesdk.PurchaseCode;

final class b extends Handler {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ a f2321a;

    b(a aVar) {
        this.f2321a = aVar;
    }

    public final void handleMessage(Message message) {
        switch (message.what) {
            case 1:
                this.f2321a.f2312a.a((Object) "cdma by Google");
                String string = message.getData().getString("listener_id");
                p pVar = (p) message.obj;
                try {
                    this.f2321a.b(string, pVar);
                    return;
                } catch (Exception e) {
                    pVar.a(null, 1, PurchaseCode.QUERY_OK, 201);
                    this.f2321a.f2312a.a((Throwable) e);
                    return;
                }
            case 2:
                String string2 = message.getData().getString("listener_id");
                p pVar2 = (p) message.obj;
                try {
                    this.f2321a.a(string2, pVar2);
                    return;
                } catch (Exception e2) {
                    pVar2.a(null, 1, PurchaseCode.QUERY_OK, 201);
                    this.f2321a.f2312a.a((Throwable) e2);
                    return;
                }
            default:
                return;
        }
    }
}
