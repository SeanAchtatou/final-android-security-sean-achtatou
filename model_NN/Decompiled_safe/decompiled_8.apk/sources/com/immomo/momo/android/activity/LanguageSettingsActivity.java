package com.immomo.momo.android.activity;

import android.os.Bundle;
import com.immomo.momo.R;
import com.immomo.momo.android.view.HeaderLayout;

public class LanguageSettingsActivity extends ah {
    /* access modifiers changed from: protected */
    public final void b(Bundle bundle) {
        super.b(bundle);
        setContentView((int) R.layout.activity_setting_language);
        ((HeaderLayout) findViewById(R.id.layout_header)).setTitleText("语言");
        fu fuVar = new fu(this, (byte) 0);
        findViewById(R.id.language_layout_en).setOnClickListener(fuVar);
        findViewById(R.id.language_layout_zh).setOnClickListener(fuVar);
    }
}
