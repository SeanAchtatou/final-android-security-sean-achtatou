package com.immomo.momo.android.view;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.WebviewActivity;
import com.immomo.momo.g;

public final class dp extends FrameLayout {

    /* renamed from: a  reason: collision with root package name */
    private View f2784a;
    private Button b;
    private Button c;
    private int d;
    private String e;
    private String f;
    private String g;
    private String h;
    private int i;
    private TextView j;
    private TextView k;
    private ImageView l;

    public dp(Context context, int i2) {
        super(context);
        this.d = i2;
        switch (this.d) {
            case 1:
                this.e = getContext().getString(R.string.userguid_nearbyuser_title);
                this.f = getContext().getString(R.string.userguid_nearbyuser_msg);
                this.h = getContext().getString(R.string.userguid_nearbyuser_button);
                this.i = R.drawable.ic_userguide_welcome;
                this.g = "http://m.immomo.com/inc/android/newuserguide.html";
                break;
            case 2:
                this.e = getContext().getString(R.string.userguid_nearbygroup_title);
                this.f = getContext().getString(R.string.userguid_nearbygroup_msg);
                this.h = getContext().getString(R.string.userguid_nearbygroup_button);
                this.i = R.drawable.ic_userguide_groupintro;
                this.g = "http://m.immomo.com/inc/android/groupintro.html";
                break;
            case 3:
                this.e = getContext().getString(R.string.userguid_session_title);
                this.f = getContext().getString(R.string.userguid_session_msg);
                this.h = getContext().getString(R.string.userguid_session_button);
                this.i = R.drawable.ic_userguide_addfriend;
                this.g = "http://m.immomo.com/inc/android/addfriends.html";
                break;
            default:
                return;
        }
        inflate(getContext(), R.layout.include_listheader_userguide, this);
        this.f2784a = findViewById(R.id.layout_root);
        this.b = (Button) findViewById(R.id.userguide_btn_1);
        this.c = (Button) findViewById(R.id.userguide_btn_2);
        this.j = (TextView) findViewById(R.id.userguide_tv_title);
        this.k = (TextView) findViewById(R.id.userguide_tv_msg);
        this.l = (ImageView) findViewById(R.id.userguide_iv_icon);
        this.j.setText(this.e);
        this.k.setText(this.f);
        this.b.setText(this.h);
        this.l.setImageResource(this.i);
        dq dqVar = new dq(this);
        this.f2784a.setOnClickListener(dqVar);
        this.b.setOnClickListener(dqVar);
        this.c.setOnClickListener(new dr(this));
    }

    static /* synthetic */ void a(dp dpVar) {
        Intent intent = new Intent(dpVar.getContext(), WebviewActivity.class);
        intent.putExtra("webview_title", dpVar.e);
        intent.putExtra("webview_url", dpVar.g);
        dpVar.getContext().startActivity(intent);
    }

    public static boolean a(int i2) {
        switch (i2) {
            case 1:
            case 2:
            case 3:
                boolean booleanValue = ((Boolean) g.r().b("guide_" + i2, false)).booleanValue();
                if (((Boolean) g.r().b("newuser", false)).booleanValue() && !booleanValue) {
                    return true;
                }
        }
        return false;
    }

    static /* synthetic */ void b(dp dpVar) {
        dpVar.f2784a.setVisibility(8);
        g.r().c("guide_" + dpVar.d, true);
    }
}
