package com.immomo.momo.android.activity.group.foundgroup;

import android.widget.RadioGroup;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;

final class g implements RadioGroup.OnCheckedChangeListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ f f1666a;

    g(f fVar) {
        this.f1666a = fVar;
    }

    public final void onCheckedChanged(RadioGroup radioGroup, int i) {
        switch (radioGroup.getCheckedRadioButtonId()) {
            case R.id.creategroup_rb_residential /*2131166122*/:
                this.f1666a.f.g = 1;
                break;
            case R.id.creategroup_rb_office /*2131166123*/:
                this.f1666a.f.g = 2;
                break;
            case R.id.creategroup_rb_unit /*2131166124*/:
                this.f1666a.f.g = 3;
                break;
        }
        if (!this.f1666a.e) {
            this.f1666a.b.setText(PoiTypeDef.All);
            this.f1666a.f.d = null;
        }
    }
}
