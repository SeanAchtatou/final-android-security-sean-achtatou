package com.immomo.momo.android.a;

import com.immomo.momo.R;
import com.immomo.momo.android.c.ab;
import com.immomo.momo.android.view.a.al;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.a.e;

final class en implements al {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ el f801a;
    private final /* synthetic */ e b;

    en(el elVar, e eVar) {
        this.f801a = elVar;
        this.b = eVar;
    }

    public final void a(int i) {
        String[] stringArray = g.l().getStringArray(R.array.reportfeed_items);
        if (i < stringArray.length) {
            this.f801a.f799a.e.a(new ab(this.f801a.f799a.e)).execute(stringArray[i], this.b.f);
        }
    }
}
