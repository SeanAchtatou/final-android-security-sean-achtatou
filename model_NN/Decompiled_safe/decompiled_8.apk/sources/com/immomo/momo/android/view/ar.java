package com.immomo.momo.android.view;

import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

final class ar {

    /* renamed from: a  reason: collision with root package name */
    private int f2726a;
    private FrameLayout b;
    private FrameLayout c = null;
    private LinearLayout d = null;
    /* access modifiers changed from: private */
    public View e = null;
    private /* synthetic */ FlingGallery f;

    public ar(FlingGallery flingGallery, int i, FrameLayout frameLayout) {
        this.f = flingGallery;
        this.f2726a = i;
        this.b = frameLayout;
        this.c = new FrameLayout(flingGallery.l);
        this.c.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        this.d = new LinearLayout(flingGallery.l);
        this.d.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        this.b.addView(this.d);
    }

    public final int a() {
        return this.d.getScrollX();
    }

    public final View a(int i) {
        if (this.e != null) {
            this.d.removeView(this.e);
        }
        if (this.f.m != null) {
            if (i < this.f.getFirstPosition() || i > this.f.getLastPosition()) {
                this.e = this.c;
            } else {
                this.e = this.f.m.getView(i, this.e, this.d);
            }
        }
        if (this.e != null) {
            this.d.addView(this.e, new LinearLayout.LayoutParams(-1, -1));
        }
        return this.e;
    }

    public final void a(int i, int i2) {
        this.d.scrollTo(FlingGallery.a(this.f, this.f2726a, i2) + i, 0);
    }

    public final void b() {
        this.d.requestFocus();
    }
}
