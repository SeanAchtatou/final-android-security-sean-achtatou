package com.immomo.momo.android.map;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import com.amap.mapapi.map.MapView;
import com.amap.mapapi.map.MyLocationOverlay;
import com.immomo.momo.R;

final class h extends MyLocationOverlay {

    /* renamed from: a  reason: collision with root package name */
    private Bitmap f2503a;
    private /* synthetic */ AMapActivity b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public h(AMapActivity aMapActivity, Context context, MapView mapView) {
        super(context, mapView);
        this.b = aMapActivity;
        if (aMapActivity.n) {
            this.f2503a = AMapActivity.e(aMapActivity);
        } else {
            this.f2503a = BitmapFactory.decodeResource(aMapActivity.getResources(), R.drawable.ic_map_pin);
        }
    }

    public final boolean draw(Canvas canvas, MapView mapView, boolean z, long j) {
        super.draw(canvas, mapView, z, j);
        Point pixels = mapView.getProjection().toPixels(this.b.g, null);
        canvas.drawBitmap(this.f2503a, (float) (pixels.x - (this.f2503a.getWidth() / 2)), (float) (pixels.y - (this.f2503a.getHeight() / 2)), (Paint) null);
        return true;
    }
}
