package com.immomo.momo.android.plugin.cropimage;

import android.app.ProgressDialog;
import android.os.Handler;

final class h extends s implements Runnable {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public final r f2587a;
    /* access modifiers changed from: private */
    public final ProgressDialog b;
    private final Runnable c;
    private final Handler d;
    private final Runnable e = new i(this);

    public h(r rVar, Runnable runnable, ProgressDialog progressDialog, Handler handler) {
        this.f2587a = rVar;
        this.b = progressDialog;
        this.c = runnable;
        this.f2587a.a(this);
        this.d = handler;
    }

    public final void a() {
        this.e.run();
        this.d.removeCallbacks(this.e);
    }

    public final void b() {
        this.b.hide();
    }

    public final void c() {
        this.b.show();
    }

    public final void run() {
        try {
            this.c.run();
        } finally {
            this.d.post(this.e);
        }
    }
}
