package com.immomo.momo.android.map;

import android.location.Location;
import com.immomo.momo.android.b.l;
import com.immomo.momo.android.b.p;
import com.immomo.momo.android.b.z;

final class al extends p {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ RomaAMapActivity f2465a;

    al(RomaAMapActivity romaAMapActivity) {
        this.f2465a = romaAMapActivity;
    }

    public final void a(Location location, int i, int i2, int i3) {
        if (i != 0) {
            this.f2465a.k.a(location, i, i2, i3);
        } else if (z.a(location)) {
            new l(this.f2465a.k, 0).execute(location);
        } else {
            this.f2465a.k.a(location, i, i2, i3);
        }
    }
}
