package com.immomo.momo.android.view;

import android.view.View;
import android.view.animation.Animation;
import com.immomo.momo.R;

final class dg implements Animation.AnimationListener {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ View f2776a;
    private final /* synthetic */ View b;

    dg(View view, View view2) {
        this.f2776a = view;
        this.b = view2;
    }

    public final void onAnimationEnd(Animation animation) {
    }

    public final void onAnimationRepeat(Animation animation) {
    }

    public final void onAnimationStart(Animation animation) {
        this.f2776a.setVisibility(0);
        this.b.setBackgroundResource(R.drawable.bg_cover_tiebaguide_outerpress);
    }
}
