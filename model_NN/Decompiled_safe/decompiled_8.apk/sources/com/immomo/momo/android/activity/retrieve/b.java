package com.immomo.momo.android.activity.retrieve;

import android.content.Context;
import com.immomo.momo.R;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.n;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.w;

final class b extends d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ InputEmailAddressActivity f2108a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public b(InputEmailAddressActivity inputEmailAddressActivity, Context context) {
        super(context);
        this.f2108a = inputEmailAddressActivity;
    }

    /* access modifiers changed from: protected */
    public final Object a(Object... objArr) {
        w.a().a(this.f2108a.l);
        return null;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.f2108a.k = new v(this.f2108a, (int) R.string.press);
        this.f2108a.k.setCancelable(true);
        this.f2108a.k.setOnCancelListener(new c(this));
        this.f2108a.k.show();
    }

    /* access modifiers changed from: protected */
    public final void a(Object obj) {
        n.b(this.f2108a, "重置密码的链接稍后将发送到您的邮箱，请注意查收", new a(this.f2108a)).show();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        if (this.f2108a.k != null) {
            this.f2108a.k.dismiss();
            this.f2108a.k = (v) null;
        }
    }
}
