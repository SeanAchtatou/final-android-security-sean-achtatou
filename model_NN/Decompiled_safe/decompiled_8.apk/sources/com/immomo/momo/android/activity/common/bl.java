package com.immomo.momo.android.activity.common;

import com.immomo.momo.android.a.hp;

final class bl implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ bk f1105a;
    private final /* synthetic */ String b;

    bl(bk bkVar, String str) {
        this.f1105a = bkVar;
        this.b = str;
    }

    public final void run() {
        int a2 = this.f1105a.f1104a.P.a(this.b, hp.f862a);
        if (a2 < 0 || a2 > this.f1105a.f1104a.P.getCount()) {
            this.f1105a.f1104a.R = this.f1105a.f1104a.U.c();
            this.f1105a.f1104a.P.b(this.f1105a.f1104a.R);
        } else {
            this.f1105a.f1104a.P.b(this.f1105a.f1104a.P.a(this.b, hp.f862a));
            ba.a(this.f1105a.f1104a, this.f1105a.f1104a.P.e());
        }
        this.f1105a.f1104a.P.notifyDataSetChanged();
    }
}
