package com.immomo.momo.android.activity;

import android.content.Intent;
import android.view.View;

final class ho implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ OtherProfileActivity f1720a;

    ho(OtherProfileActivity otherProfileActivity) {
        this.f1720a = otherProfileActivity;
    }

    public final void onClick(View view) {
        this.f1720a.startActivity(this.f1720a.f.b() ? new Intent(this.f1720a, EditVipProfileActivity.class) : new Intent(this.f1720a, EditUserProfileActivity.class));
    }
}
