package com.immomo.momo.android.activity.message;

import android.content.Intent;
import com.immomo.momo.android.broadcast.d;

final class ak implements d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ChatActivity f1918a;

    ak(ChatActivity chatActivity) {
        this.f1918a = chatActivity;
    }

    public final void a(Intent intent) {
        String stringExtra = intent.getStringExtra("key_momoid");
        this.f1918a.L.b((Object) ("got a friendListBroadcast momoid:" + stringExtra));
        if (this.f1918a.Q.h.equals(stringExtra)) {
            this.f1918a.E = "both".equalsIgnoreCase(this.f1918a.O.g(this.f1918a.Q.h));
            if (this.f1918a.E) {
                this.f1918a.E = true;
            } else {
                this.f1918a.j.sendEmptyMessage(10002);
            }
        }
    }
}
