package com.immomo.momo.android.activity.group;

import com.immomo.momo.protocol.a.n;
import com.immomo.momo.service.bean.a.a;

final class ap implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ GroupActionListFragment f1540a;
    private final /* synthetic */ a b;

    ap(GroupActionListFragment groupActionListFragment, a aVar) {
        this.f1540a = groupActionListFragment;
        this.b = aVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.service.y.a(com.immomo.momo.service.bean.a.a, boolean):void
     arg types: [com.immomo.momo.service.bean.a.a, int]
     candidates:
      com.immomo.momo.service.y.a(com.immomo.momo.service.bean.a.a, java.lang.String):void
      com.immomo.momo.service.y.a(java.lang.String, int):void
      com.immomo.momo.service.y.a(java.lang.String, java.lang.String):void
      com.immomo.momo.service.y.a(java.lang.String, java.util.List):void
      com.immomo.momo.service.y.a(java.lang.String, boolean):void
      com.immomo.momo.service.y.a(java.lang.String, java.lang.String[]):void
      com.immomo.momo.service.y.a(java.util.List, java.lang.String):void
      com.immomo.momo.service.y.a(com.immomo.momo.service.bean.a.a, boolean):void */
    public final void run() {
        try {
            n.a().a(this.b.b, this.b);
            this.f1540a.Y.a(this.b, false);
            if (!this.f1540a.f()) {
                this.f1540a.P.post(new aq(this));
            }
        } catch (Exception e) {
            this.f1540a.O.a((Object) "[error][from chatListViewAdapter]downloadOtherProfile exception");
            this.f1540a.O.a((Throwable) e);
        }
    }
}
