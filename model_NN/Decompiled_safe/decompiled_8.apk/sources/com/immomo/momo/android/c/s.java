package com.immomo.momo.android.c;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import com.immomo.momo.a;
import com.immomo.momo.protocol.a.d;
import com.immomo.momo.util.h;
import com.immomo.momo.util.m;
import java.io.File;

public final class s extends t {
    private static m e = new m("LoadMapImageThread");

    /* renamed from: a  reason: collision with root package name */
    private File f2392a = null;
    private Double b;
    private Double c;
    private g d;

    public s(Double d2, Double d3, g gVar) {
        super(gVar);
        this.d = gVar;
        this.f2392a = a.l();
        this.b = d2;
        this.c = d3;
    }

    private void a(Bitmap bitmap) {
        if (bitmap != null) {
            try {
                bitmap = android.support.v4.b.a.a(bitmap, 6.0f);
            } catch (Throwable th) {
            }
        }
        this.d.a(bitmap);
    }

    public final void a() {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.h.a(java.lang.String, android.graphics.Bitmap, int, boolean):java.io.File
     arg types: [java.lang.String, android.graphics.Bitmap, int, int]
     candidates:
      com.immomo.momo.util.h.a(java.lang.String, java.lang.String, int, boolean):void
      com.immomo.momo.util.h.a(java.lang.String, android.graphics.Bitmap, int, boolean):java.io.File */
    public final void run() {
        Bitmap bitmap = null;
        try {
            String sb = new StringBuilder().append(this.b).toString();
            File file = new File(this.f2392a, String.valueOf(sb) + new StringBuilder().append(this.c).toString() + ".jpg_");
            e.a((Object) ("load map sdcard  -- > path=" + file.getAbsolutePath()));
            if (file.exists()) {
                e.a((Object) ("load map sdcard !!success!!! -- > path=" + file.getAbsolutePath()));
                bitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
            }
            if (bitmap != null) {
                e.a((Object) ("LoadMapImageThread ---has load a map:" + this.f2392a.getAbsolutePath()));
                a(bitmap);
                return;
            }
        } catch (Throwable th) {
        }
        try {
            Bitmap a2 = d.a(this.b.doubleValue(), this.c.doubleValue());
            e.a((Object) "LoadMapImageThread --- load a map from HTTP");
            if (a2 != null) {
                h.a(new StringBuilder().append(this.b).append(this.c).toString(), a2, 4, false);
            }
            a(a2);
        } catch (Throwable th2) {
            Throwable th3 = th2;
            Bitmap bitmap2 = bitmap;
            Throwable th4 = th3;
            a(bitmap2);
            throw th4;
        }
    }
}
