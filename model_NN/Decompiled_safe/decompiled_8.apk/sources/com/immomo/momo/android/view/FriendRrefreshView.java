package com.immomo.momo.android.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import com.immomo.momo.R;
import com.immomo.momo.g;

public class FriendRrefreshView extends MomoRefreshListView {
    private EditText g;
    private View h;
    private View i = null;

    public FriendRrefreshView(Context context) {
        super(context);
        a();
    }

    public FriendRrefreshView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a();
    }

    public FriendRrefreshView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        a();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, com.immomo.momo.android.view.FriendRrefreshView, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    private void a() {
        LinearLayout linearLayout = (LinearLayout) g.o().inflate((int) R.layout.include_relationfriend_searchbar, (ViewGroup) this, false);
        this.g = (EditText) linearLayout.findViewById(R.id.search_edittext);
        this.h = linearLayout.findViewById(R.id.search_btn_clear);
        this.i = linearLayout.findViewById(R.id.layout_sort);
        addHeaderView(linearLayout);
    }

    public View getClearButton() {
        return this.h;
    }

    public EditText getSearchEdit() {
        return this.g;
    }

    public View getSortView() {
        return this.i;
    }
}
