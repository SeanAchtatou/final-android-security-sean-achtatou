package com.immomo.momo.android.activity.feed;

import android.content.Context;
import android.widget.ListAdapter;
import com.immomo.momo.android.a.cs;
import com.immomo.momo.android.c.d;
import com.immomo.momo.protocol.a.k;
import com.immomo.momo.service.bean.ab;
import java.util.ArrayList;
import java.util.List;

final class aj extends d {

    /* renamed from: a  reason: collision with root package name */
    private List f1441a = new ArrayList();
    private /* synthetic */ MyFeedListActivity c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public aj(MyFeedListActivity myFeedListActivity, Context context) {
        super(context);
        this.c = myFeedListActivity;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        this.c.q = this.c.p;
        this.c.p = 0;
        boolean a2 = k.a().a(this.f1441a, this.c.p, this.c.f.h);
        for (ab abVar : this.f1441a) {
            abVar.b = this.c.f;
        }
        this.c.l.g(this.c.f.h);
        this.c.l.a(this.f1441a);
        this.c.k.clear();
        for (ab add : this.f1441a) {
            this.c.k.add(add);
        }
        if (this.f1441a != null && this.f1441a.size() > 0) {
            MyFeedListActivity myFeedListActivity = this.c;
            myFeedListActivity.p = myFeedListActivity.p + this.f1441a.size();
            this.b.a((Object) ("downloadOtherFeedList success, lastindex:" + this.c.p));
        }
        return Boolean.valueOf(a2);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        if (this.c.n != null && !this.c.n.isCancelled()) {
            this.c.n.cancel(true);
        }
        this.c.i.g();
        if (this.c.m != null) {
            this.c.m.cancel(true);
        }
        this.c.m = this;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        this.c.j = this.f1441a;
        this.c.o = new cs(this.c, this.c.j, this.c.h);
        this.c.h.setAdapter((ListAdapter) this.c.o);
        if (!((Boolean) obj).booleanValue()) {
            this.c.h.k();
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.c.w();
        this.c.m = (aj) null;
    }
}
