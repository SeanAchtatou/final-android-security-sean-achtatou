package com.immomo.momo.android.activity.feed;

import com.immomo.momo.android.view.au;
import java.io.File;

final class l implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ k f1477a;
    private final /* synthetic */ File b;
    private final /* synthetic */ boolean c;

    l(k kVar, File file, boolean z) {
        this.f1477a = kVar;
        this.b = file;
        this.c = z;
    }

    public final void run() {
        if (this.b != null && this.b.exists()) {
            this.f1477a.f1476a.ah = new au(this.c ? 1 : 2);
            this.f1477a.f1476a.ah.a(this.b, this.f1477a.f1476a.C);
            this.f1477a.f1476a.ah.b(20);
            this.f1477a.f1476a.C.setGifDecoder(this.f1477a.f1476a.ah);
        }
    }
}
