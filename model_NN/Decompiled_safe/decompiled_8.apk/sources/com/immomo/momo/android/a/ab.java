package com.immomo.momo.android.a;

import android.graphics.Bitmap;
import android.os.Handler;
import com.immomo.momo.android.c.g;
import com.immomo.momo.service.bean.e;

final class ab implements g {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ q f710a;
    private e b = null;
    /* access modifiers changed from: private */
    public Bitmap c = null;
    private Handler d = null;

    public ab(q qVar, e eVar) {
        this.f710a = qVar;
        this.b = eVar;
        this.d = new ac(this, qVar.b.getContext().getMainLooper());
    }

    public final /* synthetic */ void a(Object obj) {
        this.c = (Bitmap) obj;
        if (!(this.c == null || this.b == null)) {
            this.b.h = this.c;
        }
        if (this.d != null) {
            this.d.sendEmptyMessage(1);
        }
    }
}
