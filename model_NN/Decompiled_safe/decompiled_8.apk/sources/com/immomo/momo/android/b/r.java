package com.immomo.momo.android.b;

import android.os.Handler;
import android.os.Message;
import mm.purchasesdk.PurchaseCode;

final class r extends Handler {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ q f2336a;

    r(q qVar) {
        this.f2336a = qVar;
    }

    public final void handleMessage(Message message) {
        switch (message.what) {
            case 1:
                this.f2336a.b.a((Object) "cdma by Google");
                String string = message.getData().getString("listener_id");
                boolean z = message.getData().getBoolean("isConvert");
                p pVar = (p) message.obj;
                try {
                    this.f2336a.b(string, pVar, z);
                    return;
                } catch (Exception e) {
                    pVar.a(null, 0, PurchaseCode.QUERY_OK, PurchaseCode.LOADCHANNEL_ERR);
                    this.f2336a.b.a((Throwable) e);
                    return;
                }
            case 2:
                String string2 = message.getData().getString("listener_id");
                boolean z2 = message.getData().getBoolean("isConvert");
                p pVar2 = (p) message.obj;
                try {
                    this.f2336a.a(string2, pVar2, z2);
                    return;
                } catch (Exception e2) {
                    pVar2.a(null, 0, PurchaseCode.QUERY_OK, PurchaseCode.LOADCHANNEL_ERR);
                    this.f2336a.b.a((Throwable) e2);
                    return;
                }
            default:
                return;
        }
    }
}
