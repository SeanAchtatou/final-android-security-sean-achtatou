package com.immomo.momo.android.activity.setting;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.support.v4.b.a;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.g;
import com.sina.sdk.api.message.InviteApi;
import java.io.File;

final class w extends Handler {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ SettingShareActivity f2146a;

    w(SettingShareActivity settingShareActivity) {
        this.f2146a = settingShareActivity;
    }

    public final void handleMessage(Message message) {
        Bitmap bitmap = (Bitmap) message.obj;
        String string = message.getData().getString(InviteApi.KEY_URL);
        if (this.f2146a.j != null) {
            this.f2146a.j.dismiss();
        }
        String str = String.valueOf(this.f2146a.getString(R.string.share_momocard_content)) + (a.a(string) ? PoiTypeDef.All : "(苹果、安卓手机下载:" + string + " )");
        if (bitmap != null) {
            bitmap.recycle();
            Intent intent = new Intent("android.intent.action.SEND");
            intent.putExtra("android.intent.extra.STREAM", Uri.fromFile(new File(com.immomo.momo.a.y().getAbsoluteFile() + "/" + (String.valueOf(g.c("momoshared_" + this.f2146a.f.h)) + ".jpg_"))));
            intent.setType("image/jpeg");
            intent.putExtra("android.intent.extra.SUBJECT", "分享");
            intent.putExtra("android.intent.extra.TEXT", str);
            this.f2146a.startActivity(Intent.createChooser(intent, "分享"));
            return;
        }
        Intent intent2 = new Intent("android.intent.action.SEND");
        intent2.setType("text/plain");
        intent2.putExtra("android.intent.extra.SUBJECT", "分享");
        intent2.putExtra("android.intent.extra.TEXT", str);
        this.f2146a.startActivity(Intent.createChooser(intent2, "分享"));
    }
}
