package com.immomo.momo.android.activity.contacts;

import com.immomo.momo.service.bean.bf;

final class ap implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ao f1147a;
    private final /* synthetic */ String b;

    ap(ao aoVar, String str) {
        this.f1147a = aoVar;
        this.b = str;
    }

    public final void run() {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < this.f1147a.f1146a.P.getCount()) {
                bf bfVar = (bf) this.f1147a.f1146a.P.getItem(i2);
                if (bfVar.h.equals(this.b)) {
                    this.f1147a.f1146a.X.a(bfVar, this.b);
                    this.f1147a.f1146a.aa.post(new aq(this));
                    return;
                }
                i = i2 + 1;
            } else {
                return;
            }
        }
    }
}
