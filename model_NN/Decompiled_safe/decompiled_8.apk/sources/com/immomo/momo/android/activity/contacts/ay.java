package com.immomo.momo.android.activity.contacts;

import com.immomo.momo.service.bean.bf;

final class ay implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ax f1155a;
    private final /* synthetic */ String b;

    ay(ax axVar, String str) {
        this.f1155a = axVar;
        this.b = str;
    }

    public final void run() {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < this.f1155a.f1154a.S.getCount()) {
                bf bfVar = (bf) this.f1155a.f1154a.S.getItem(i2);
                if (bfVar.h.equals(this.b)) {
                    this.f1155a.f1154a.Z.a(bfVar, this.b);
                    this.f1155a.f1154a.ab.post(new az(this));
                    return;
                }
                i = i2 + 1;
            } else {
                return;
            }
        }
    }
}
