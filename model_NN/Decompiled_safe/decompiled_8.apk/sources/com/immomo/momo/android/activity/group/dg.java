package com.immomo.momo.android.activity.group;

import android.content.Intent;
import android.support.v4.b.a;
import com.immomo.momo.android.broadcast.d;
import com.immomo.momo.android.broadcast.u;

final class dg implements d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ GroupProfileActivity f1604a;

    dg(GroupProfileActivity groupProfileActivity) {
        this.f1604a = groupProfileActivity;
    }

    public final void a(Intent intent) {
        this.f1604a.e.a((Object) "onReceive");
        if (u.f2364a.equals(intent.getAction())) {
            a.a((CharSequence) intent.getStringExtra("gid"));
        } else if (u.b.equals(intent.getAction())) {
            if (!a.a((CharSequence) intent.getStringExtra("gid"))) {
                this.f1604a.finish();
            }
        } else if (u.e.equals(intent.getAction())) {
            String stringExtra = intent.getStringExtra("gid");
            this.f1604a.e.a((Object) ("broadcast gid:" + stringExtra));
            if (!a.a((CharSequence) stringExtra) && stringExtra.equals(this.f1604a.r.b)) {
                this.f1604a.r = this.f1604a.o.e(stringExtra);
                this.f1604a.u();
                GroupProfileActivity.k(this.f1604a);
            }
        } else {
            u.f.equals(intent.getAction());
        }
    }
}
