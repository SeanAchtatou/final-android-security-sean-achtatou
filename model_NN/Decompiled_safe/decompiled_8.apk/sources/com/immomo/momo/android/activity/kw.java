package com.immomo.momo.android.activity;

import android.os.AsyncTask;
import com.immomo.momo.R;
import com.immomo.momo.a.a;
import com.immomo.momo.a.w;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.d;
import java.net.URLEncoder;

final class kw extends AsyncTask {

    /* renamed from: a  reason: collision with root package name */
    private v f1800a;
    private /* synthetic */ ShareWebviewActivity b;

    public kw(ShareWebviewActivity shareWebviewActivity) {
        this.b = shareWebviewActivity;
        this.f1800a = new v(shareWebviewActivity);
        this.f1800a.setCancelable(true);
        this.f1800a.setOnDismissListener(new kx(this));
    }

    private String a() {
        try {
            return d.h();
        } catch (w e) {
            this.b.n.a((Throwable) e);
            this.b.b((int) R.string.errormsg_network_unfind);
        } catch (a e2) {
            this.b.n.a((Throwable) e2);
            this.b.b((CharSequence) e2.getMessage());
        } catch (Exception e3) {
            this.b.n.a((Throwable) e3);
            this.b.b((int) R.string.errormsg_server);
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object doInBackground(Object... objArr) {
        return a();
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void onPostExecute(Object obj) {
        String str = (String) obj;
        if (this.f1800a != null) {
            this.f1800a.cancel();
        }
        if (!android.support.v4.b.a.a((CharSequence) str)) {
            this.b.d(this.b.c(URLEncoder.encode(str)));
        }
    }

    /* access modifiers changed from: protected */
    public final void onPreExecute() {
        super.onPreExecute();
        this.f1800a.a("请求提交中...");
        this.f1800a.show();
    }
}
