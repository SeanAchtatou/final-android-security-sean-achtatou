package com.immomo.momo.android.a.a;

import com.immomo.momo.android.activity.message.ce;
import com.immomo.momo.android.view.a.al;
import com.immomo.momo.g;
import com.immomo.momo.util.ao;

final class ae implements al {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ab f676a;
    private final /* synthetic */ String[] b;

    ae(ab abVar, String[] strArr) {
        this.f676a = abVar;
        this.b = strArr;
    }

    public final void a(int i) {
        if ("重新发送".equals(this.b[i])) {
            ce.a().a(this.f676a.h);
            this.f676a.e().Y();
        } else if ("复制文本".equals(this.b[i])) {
            g.a((CharSequence) this.f676a.h.getContent());
            ao.b("已复制消息文本");
        } else if ("删除消息".equals(this.b[i])) {
            this.f676a.e().b(this.f676a.h);
        } else if ("@ TA".equals(this.b[i])) {
            this.f676a.e().e(this.f676a.h);
        }
    }
}
