package com.immomo.momo.android.activity.contacts;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import com.immomo.momo.R;
import com.immomo.momo.android.a.q;
import com.immomo.momo.android.activity.ah;
import com.immomo.momo.android.activity.plugin.RenrenActivity;
import com.immomo.momo.android.activity.plugin.SinaWeiboActivity;
import com.immomo.momo.android.activity.plugin.TxWeiboActivity;
import com.immomo.momo.android.view.EmoteEditeText;
import com.immomo.momo.android.view.MomoUnrefreshExpandableListView;
import com.immomo.momo.android.view.a.n;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.e;
import com.immomo.momo.util.aq;
import com.immomo.momo.util.k;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class CommunityPeopleActivity extends ah {
    Comparator h = new o();
    /* access modifiers changed from: private */
    public int i;
    /* access modifiers changed from: private */
    public int j;
    /* access modifiers changed from: private */
    public q k = null;
    private MomoUnrefreshExpandableListView l = null;
    /* access modifiers changed from: private */
    public List m;
    /* access modifiers changed from: private */
    public z n;
    /* access modifiers changed from: private */
    public t o;
    /* access modifiers changed from: private */
    public v p;

    static /* synthetic */ void a(CommunityPeopleActivity communityPeopleActivity, int i2, int i3) {
        View inflate = g.o().inflate((int) R.layout.dialog_contactpeople_apply, (ViewGroup) null);
        EmoteEditeText emoteEditeText = (EmoteEditeText) inflate.findViewById(R.id.edittext_reason);
        emoteEditeText.addTextChangedListener(new aq(24));
        n nVar = new n(communityPeopleActivity);
        nVar.setTitle("好友验证");
        nVar.setContentView(inflate);
        nVar.a();
        nVar.a(0, communityPeopleActivity.getString(R.string.dialog_btn_confim), new r(communityPeopleActivity, emoteEditeText, i2, i3));
        nVar.a(1, communityPeopleActivity.getString(R.string.dialog_btn_cancel), new s());
        nVar.getWindow().setSoftInputMode(4);
        nVar.show();
    }

    static /* synthetic */ void a(CommunityPeopleActivity communityPeopleActivity, e eVar) {
        switch (communityPeopleActivity.i) {
            case 1:
                Intent intent = new Intent(communityPeopleActivity, SinaWeiboActivity.class);
                intent.putExtra("uid", eVar.c);
                communityPeopleActivity.startActivity(intent);
                return;
            case 2:
                Intent intent2 = new Intent(communityPeopleActivity, TxWeiboActivity.class);
                intent2.putExtra("keyType", 2);
                intent2.putExtra("uid", eVar.c);
                communityPeopleActivity.startActivity(intent2);
                return;
            case 3:
                Intent intent3 = new Intent(communityPeopleActivity, RenrenActivity.class);
                intent3.putExtra("renrenuid", eVar.c);
                communityPeopleActivity.startActivity(intent3);
                return;
            default:
                return;
        }
    }

    static /* synthetic */ void e(CommunityPeopleActivity communityPeopleActivity) {
        if (communityPeopleActivity.m != null) {
            communityPeopleActivity.k = new q(communityPeopleActivity.m, communityPeopleActivity.l, communityPeopleActivity.j);
            communityPeopleActivity.l.setAdapter(communityPeopleActivity.k);
            communityPeopleActivity.k.a();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, com.immomo.momo.android.view.MomoUnrefreshExpandableListView, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        int i2;
        super.a(bundle);
        setContentView((int) R.layout.activity_sinacontactpeople);
        if (getIntent().getExtras() == null || getIntent().getExtras().getInt("type", 0) == 0) {
            finish();
            return;
        }
        this.i = getIntent().getExtras().getInt("type", 0);
        this.j = getIntent().getExtras().getInt("from", 0);
        if (this.j == 0) {
            finish();
        }
        switch (this.i) {
            case 1:
                i2 = R.string.sinaweibocontact_title;
                break;
            case 2:
                i2 = R.string.txweibocontact_title;
                break;
            case 3:
                i2 = R.string.renrencontact_title;
                break;
            default:
                i2 = 0;
                break;
        }
        if (i2 == 0) {
            finish();
        }
        m().setTitleText(i2);
        this.l = (MomoUnrefreshExpandableListView) findViewById(R.id.listview_contact);
        this.l.setMMHeaderView(g.o().inflate((int) R.layout.listitem_contactgroup, (ViewGroup) this.l, false));
        this.l.setOnChildClickListener(new p(this));
        this.l.setOnGroupClickListener(new q());
        d();
    }

    /* access modifiers changed from: protected */
    public final void d() {
        this.m = new ArrayList();
        new z(this, this).execute(new Object[0]);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        if (i2 == 23) {
            if (i3 == -1) {
                a((CharSequence) "绑定成功");
            } else {
                a((CharSequence) "绑定失败");
            }
        }
        super.onActivityResult(i2, i3, intent);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        switch (this.i) {
            case 1:
                new k("PO", "P693").e();
                return;
            case 2:
                new k("PO", "P695").e();
                return;
            case 3:
                new k("PO", "P694").e();
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        switch (this.i) {
            case 1:
                new k("PI", "P693").e();
                return;
            case 2:
                new k("PI", "P695").e();
                return;
            case 3:
                new k("PI", "P694").e();
                return;
            default:
                return;
        }
    }
}
