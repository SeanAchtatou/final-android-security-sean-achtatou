package com.immomo.momo.android.activity.emotestore;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.android.a.a;
import com.immomo.momo.g;
import org.json.JSONArray;
import org.json.JSONObject;

final class ac extends a {
    private JSONArray d = null;

    public ac(Context context, JSONArray jSONArray) {
        super(context);
        this.d = jSONArray;
    }

    public final int getCount() {
        if (this.d == null) {
            return 0;
        }
        return this.d.length();
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = g.o().inflate((int) R.layout.listitem_task, (ViewGroup) null);
        }
        ImageView imageView = (ImageView) view.findViewById(R.id.task_iv_status);
        JSONObject optJSONObject = this.d.optJSONObject(i);
        ((TextView) view.findViewById(R.id.task_tv_title)).setText(optJSONObject.optString("desc"));
        if (optJSONObject.optInt("state") == 1) {
            imageView.setVisibility(0);
        } else {
            imageView.setVisibility(8);
        }
        return view;
    }
}
