package com.immomo.momo.android.activity.tieba;

import android.support.v4.b.a;
import android.text.Editable;
import android.text.TextWatcher;
import com.amap.mapapi.poisearch.PoiTypeDef;

final class c implements TextWatcher {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ EditTieActivity f2201a;

    c(EditTieActivity editTieActivity) {
        this.f2201a = editTieActivity;
    }

    public final void afterTextChanged(Editable editable) {
        if (a.a((CharSequence) editable.toString())) {
            this.f2201a.q.setText(PoiTypeDef.All);
        } else {
            this.f2201a.q.setText(String.valueOf(editable.length()) + "/1000字");
        }
    }

    public final void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public final void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }
}
