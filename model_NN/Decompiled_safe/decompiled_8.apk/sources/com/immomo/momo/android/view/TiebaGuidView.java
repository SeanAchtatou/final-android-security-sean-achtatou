package com.immomo.momo.android.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.AnimationSet;
import android.view.animation.OvershootInterpolator;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import com.immomo.momo.R;
import com.immomo.momo.service.bean.c.d;
import com.immomo.momo.util.j;
import java.util.ArrayList;
import java.util.List;

public class TiebaGuidView extends d {
    /* access modifiers changed from: private */
    public List b = new ArrayList();
    /* access modifiers changed from: private */
    public f c;

    public TiebaGuidView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        setItemViewWeith(55);
        setMinPading(5);
    }

    static /* synthetic */ void a(View view, View view2) {
        AnimationSet animationSet = new AnimationSet(true);
        ScaleAnimation scaleAnimation = new ScaleAnimation(1.0f, 0.4f, 1.0f, 0.4f, (float) (view2.getMeasuredWidth() / 4), (float) (view2.getMeasuredHeight() / 4));
        AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
        animationSet.addAnimation(scaleAnimation);
        animationSet.addAnimation(alphaAnimation);
        animationSet.setDuration(600);
        animationSet.setAnimationListener(new dk(view2, view));
        view.clearAnimation();
        view.startAnimation(animationSet);
    }

    static /* synthetic */ void b(View view, View view2) {
        AnimationSet animationSet = new AnimationSet(true);
        ScaleAnimation scaleAnimation = new ScaleAnimation(0.3f, 1.0f, 0.3f, 1.0f, (float) (view2.getMeasuredWidth() / 4), (float) (view2.getMeasuredHeight() / 4));
        AlphaAnimation alphaAnimation = new AlphaAnimation(0.9f, 1.0f);
        animationSet.addAnimation(scaleAnimation);
        animationSet.addAnimation(alphaAnimation);
        animationSet.setDuration(600);
        animationSet.setInterpolator(new OvershootInterpolator(4.0f));
        animationSet.setAnimationListener(new dj(view, view2));
        view.clearAnimation();
        view.startAnimation(animationSet);
    }

    public final /* synthetic */ View a(Object obj) {
        d dVar = (d) obj;
        View inflate = inflate(getContext(), R.layout.listitem_tiebaguidcategory, null);
        j.a(dVar, (ImageView) inflate.findViewById(R.id.tiebacategory_item_img_category), (ViewGroup) null, 3);
        ((EmoteTextView) inflate.findViewById(R.id.tiebacategory_item_text_name)).setText(dVar.b);
        return inflate;
    }

    public final void a() {
        this.b.clear();
    }

    /* access modifiers changed from: protected */
    public final void b(int i, View view) {
        view.findViewById(R.id.tiebacategory_item_img_layout).setOnClickListener(new di(this, view, i));
    }

    public int getSelectCount() {
        return this.b.size();
    }

    public String getSelectIds() {
        StringBuilder sb = new StringBuilder();
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.b.size()) {
                return sb.toString();
            }
            sb.append((String) this.b.get(i2));
            if (i2 != this.b.size() - 1) {
                sb.append(",");
            }
            i = i2 + 1;
        }
    }

    public void setOnMomoGridViewItemClickListener(f fVar) {
        this.c = fVar;
    }
}
