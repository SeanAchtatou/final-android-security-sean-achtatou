package com.immomo.momo.android.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AbsListView;
import android.widget.RelativeLayout;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.baidu.location.LocationClientOption;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.WebviewActivity;
import com.immomo.momo.android.c.u;
import com.immomo.momo.protocol.a.c;
import com.immomo.momo.service.a;
import com.immomo.momo.service.bean.at;
import com.immomo.momo.service.bean.d;
import com.immomo.momo.util.j;
import com.immomo.momo.util.m;
import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

public final class g extends ViewPager {
    /* access modifiers changed from: private */
    public View.OnClickListener A = new h(this);

    /* renamed from: a  reason: collision with root package name */
    int f2806a = -1;
    int b;
    int c;
    int d;
    int e = 0;
    int f = 0;
    List g = new ArrayList();
    q h = null;
    a i = null;
    boolean j = false;
    /* access modifiers changed from: private */
    public m k = new m(this);
    private int l;
    private int m;
    /* access modifiers changed from: private */
    public Handler n = null;
    /* access modifiers changed from: private */
    public View o = null;
    /* access modifiers changed from: private */
    public View p = null;
    /* access modifiers changed from: private */
    public View q = null;
    /* access modifiers changed from: private */
    public Date r = new Date();
    /* access modifiers changed from: private */
    public boolean s = false;
    private boolean t = false;
    private at u = null;
    /* access modifiers changed from: private */
    public ThreadPoolExecutor v = new u(2, 2);
    private boolean w = true;
    /* access modifiers changed from: private */
    public WebView x;
    private boolean y = false;
    private Context z = null;

    public g(Context context, int i2) {
        super(context);
        this.f2806a = i2;
        this.i = new a();
        l();
    }

    public g(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        l();
    }

    private void b(d dVar) {
        if (dVar.l == null || this.r.getTime() - dVar.l.getTime() > 86400000) {
            dVar.l = this.r;
            new v(this, dVar, getContext()).execute(new Object[0]);
        }
        if (!dVar.k) {
            dVar.k = true;
            new w(this, dVar, getContext()).execute(new Object[0]);
        }
        if (!android.support.v4.b.a.a((CharSequence) dVar.m)) {
            String a2 = d.a(dVar.m, PoiTypeDef.All);
            this.k.a((Object) ("url=" + a2));
            this.x.loadUrl(a2);
        }
        if (!android.support.v4.b.a.a((CharSequence) dVar.n)) {
            new x(dVar, getContext()).execute(new Object[0]);
        }
    }

    private Context getContext2() {
        return this.z == null ? super.getContext() : this.z;
    }

    static /* synthetic */ void j(g gVar) {
        boolean z2 = false;
        AtomicInteger atomicInteger = new AtomicInteger();
        atomicInteger.set(gVar.f);
        AtomicBoolean atomicBoolean = new AtomicBoolean();
        try {
            Date date = new Date();
            List<d> a2 = c.a().a(gVar.f2806a, atomicInteger, atomicBoolean, date);
            gVar.k.a((Object) ("serverTime=" + android.support.v4.b.a.e(date)));
            gVar.f = atomicInteger.get();
            gVar.u.c("bannerversion" + gVar.f2806a, Integer.valueOf(gVar.f));
            gVar.u.a("bannerreflushtime" + gVar.f2806a, new Date());
            gVar.u.a("bannertime" + gVar.f2806a, date);
            if (a2 == null) {
                Date date2 = gVar.r;
                if (date2 != null && date2.getYear() == date.getYear() && date2.getDate() == date.getDate() && date2.getMonth() == date.getMonth()) {
                    z2 = true;
                }
                if (z2) {
                    gVar.r = date;
                    return;
                }
                gVar.r = date;
            } else {
                gVar.u.a("bannerclosetime" + gVar.f2806a, (Date) null);
                gVar.u.a("bannerbegintime" + gVar.f2806a, new Date());
                gVar.r = date;
                for (d dVar : a2) {
                    if (!(dVar.f == null || dVar.e == null)) {
                        d a3 = gVar.i.a(dVar.f3022a, dVar.h);
                        if (a3 != null) {
                            dVar.o = a3.o;
                            dVar.p = a3.p;
                        }
                        File file = new File(com.immomo.momo.a.c(), android.support.v4.b.a.n(dVar.d));
                        if (file.exists()) {
                            dVar.i = file;
                        } else if (gVar.r.getTime() < dVar.e.getTime()) {
                            gVar.v.execute(new t(dVar));
                        }
                        if (gVar.r.getTime() < dVar.e.getTime()) {
                            gVar.v.execute(new t(dVar));
                        }
                    }
                }
                gVar.i.a(a2, gVar.f2806a);
            }
            gVar.n.post(new p(gVar));
        } catch (Exception e2) {
            gVar.k.a((Throwable) e2);
        }
    }

    public static void k() {
    }

    private void l() {
        this.u = com.immomo.momo.g.r();
        com.immomo.momo.g.q();
        this.l = com.immomo.momo.g.J();
        this.m = Math.round(((float) this.l) / 6.4f);
        int a2 = com.immomo.momo.g.a(5.0f);
        this.d = a2;
        this.e = a2;
        this.c = a2;
        this.b = a2;
        ViewGroup viewGroup = (ViewGroup) com.immomo.momo.g.o().inflate((int) R.layout.common_banner, (ViewGroup) null);
        viewGroup.setLayoutParams(new AbsListView.LayoutParams(-1, -2));
        this.p = viewGroup;
        this.q = viewGroup.findViewById(R.id.banner_layout_corners);
        this.q.setLayoutParams(new RelativeLayout.LayoutParams(-1, this.m));
        if (this.f2806a == 3 || this.f2806a == 4) {
            this.q.setBackgroundResource(R.drawable.bg_cover_undercard);
        }
        setLayoutParams(new ViewGroup.LayoutParams(-1, this.m));
        viewGroup.addView(this, 0);
        this.o = viewGroup.findViewById(R.id.banner_iv_allowclose);
        this.x = (WebView) viewGroup.findViewById(R.id.webview);
        this.x.setWebViewClient(new WebViewClient());
        this.n = new i(this);
        this.f = ((Integer) this.u.b("bannerversion" + this.f2806a, 0)).intValue();
        Date b2 = this.u.b("bannerreflushtime" + this.f2806a);
        Date b3 = this.u.b("bannertime" + this.f2806a);
        if (b3 == null || b2 == null || Math.abs(this.r.getTime() - b2.getTime()) >= 900000) {
            new Thread(new j(this)).start();
        } else if (b3 != null) {
            this.r = b3;
        }
        d();
        this.o.setOnClickListener(new k(this));
    }

    private boolean m() {
        if (this.g.isEmpty()) {
            this.k.a((Object) ("isDisplayable,   bannerList.isEmpty()=" + this.g.isEmpty()));
            return false;
        } else if (!Environment.getExternalStorageState().equals("mounted")) {
            return false;
        } else {
            for (d dVar : this.g) {
                this.k.a((Object) ("isDisplayable, banner=" + dVar.b()));
                if (dVar.b()) {
                    return true;
                }
            }
            return false;
        }
    }

    private void n() {
        this.n.removeMessages(435);
    }

    /* access modifiers changed from: protected */
    public final void a(d dVar) {
        if (dVar.b == 1) {
            if (!android.support.v4.b.a.a((CharSequence) dVar.g)) {
                Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(dVar.g));
                intent.addFlags(268435456);
                intent.putExtra("com.android.browser.application_id", com.immomo.momo.g.h());
                getContext2().startActivity(intent);
            }
        } else if (dVar.b == 4 || dVar.b == 5) {
            this.k.a((Object) ("banner.url=" + dVar.g));
            com.immomo.momo.android.activity.d.a(dVar.g, getContext2());
            new u(this, dVar, getContext()).execute(new Object[0]);
        } else if (dVar.b != 2) {
            this.k.c("banner.linktype=" + dVar.b);
        } else if (!android.support.v4.b.a.a((CharSequence) dVar.g)) {
            Intent intent2 = new Intent(getContext(), WebviewActivity.class);
            intent2.putExtra("webview_title", "网页");
            intent2.putExtra("webview_url", dVar.g);
            if (getContext2() instanceof Activity) {
                getContext2().startActivity(intent2);
                return;
            }
            intent2.addFlags(268435456);
            getContext2().startActivity(intent2);
        }
    }

    public final int b(int i2) {
        int i3 = i2;
        do {
            int i4 = (i3 >= this.g.size() || i3 < 0) ? 0 : i3;
            d dVar = (d) this.g.get(i4);
            if (dVar.a() != null || dVar.b()) {
                return i4;
            }
            if (!dVar.isImageLoading()) {
                this.v.execute(new r(this, dVar));
            }
            i3 = i4 + 1;
        } while (i3 != i2);
        return i2;
    }

    public final void d() {
        List<d> a2 = this.i.a(this.r, this.f2806a);
        this.t = true;
        this.g.clear();
        for (d dVar : a2) {
            if (!dVar.o || Math.abs(this.r.getTime() - dVar.p) > 86400000) {
                this.g.add(dVar);
                if (!dVar.b() && !dVar.isImageLoading()) {
                    this.v.execute(new r(this, dVar));
                }
                this.t = this.t && dVar.j;
            } else {
                this.k.a((Object) ("closed banner, id=" + dVar.f3022a));
            }
        }
        if (m()) {
            setVisibility(0);
            this.p.setPadding(this.d, this.b, this.e, this.c);
            this.x.setVisibility(0);
            this.q.setVisibility(0);
            if (this.t) {
                this.o.setVisibility(0);
            } else {
                this.o.setVisibility(8);
            }
            this.h = new q(this, (byte) 0);
            h();
            if (this.u.b("bannerbegintime" + this.f2806a) == null) {
                this.u.a("bannerbegintime" + this.f2806a, new Date());
            }
        } else {
            this.h = null;
            this.x.setVisibility(8);
            setVisibility(8);
            this.o.setVisibility(8);
            this.q.setVisibility(8);
            this.p.setPadding(0, 0, 0, 0);
            n();
        }
        setAdapter(this.h);
    }

    public final void e() {
        int i2;
        if (this.w) {
            try {
                a(getCurrentItem() + 1);
                int currentPosition = getCurrentPosition();
                if (currentPosition < 0 || currentPosition >= this.g.size()) {
                    i2 = 3000;
                } else {
                    int i3 = ((d) this.g.get(currentPosition)).c * LocationClientOption.MIN_SCAN_SPAN;
                    b((d) this.g.get(currentPosition));
                    i2 = i3;
                }
                this.n.sendEmptyMessageDelayed(435, (long) i2);
            } catch (Exception e2) {
                this.k.a((Throwable) e2);
                this.n.sendEmptyMessageDelayed(435, 5000);
            }
        }
    }

    public final void f() {
        this.w = false;
        this.k.a((Object) ("~~~~~~~~~~~~~~~~~onWindowHidden, pos=" + this.f2806a));
        n();
    }

    public final void g() {
        this.w = true;
        if (!this.n.hasMessages(435)) {
            this.k.a((Object) ("~~~~~~~~~~~~~~~~~onWindowShown, pos=" + this.f2806a));
            h();
        }
    }

    public final int getCurrentPosition() {
        int i2 = 0;
        while (i2 < getChildCount()) {
            try {
                Object tag = getChildAt(i2).getTag(R.id.tag_item_position);
                if (tag != null && ((Integer) tag).intValue() == getCurrentItem() && getChildAt(i2).getTag() != null) {
                    return ((Integer) getChildAt(i2).getTag()).intValue();
                }
                i2++;
            } catch (Exception e2) {
            }
        }
        return 0;
    }

    public final View getWappview() {
        return this.p;
    }

    public final void h() {
        int i2;
        if (this.w) {
            this.k.a((Object) "startPlay, removeMessages");
            this.n.removeMessages(435);
            int i3 = 0;
            if (!this.g.isEmpty()) {
                Iterator it = this.g.iterator();
                while (true) {
                    i2 = i3;
                    if (!it.hasNext()) {
                        break;
                    }
                    i3 = ((d) it.next()).b() ? i2 + 1 : i2;
                    if (i3 == 2) {
                        i2 = i3;
                        break;
                    }
                }
                int currentPosition = getCurrentPosition();
                if (currentPosition >= 0 && currentPosition < this.g.size()) {
                    d dVar = (d) this.g.get(currentPosition);
                    if (i2 >= 2) {
                        long j2 = (long) (dVar.c * LocationClientOption.MIN_SCAN_SPAN);
                        this.k.a((Object) ("startPlay, sendEmptyMessageDelayed, delayed=" + j2));
                        this.n.sendEmptyMessageDelayed(435, j2);
                    } else {
                        this.k.a((Object) ("startPlay, cachedSize=" + i2));
                    }
                    b(dVar);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void i() {
        this.s = true;
        n();
        this.o.setVisibility(8);
        setVisibility(8);
        this.q.setVisibility(8);
        this.p.setPadding(0, 0, 0, 0);
        this.x.setVisibility(8);
        this.u.a("bannerclosetime" + this.f2806a, this.r);
        Date b2 = this.u.b("bannerbegintime" + this.f2806a);
        Date date = new Date();
        String[] strArr = new String[this.g.size()];
        int i2 = 0;
        for (int i3 = 0; i3 < this.g.size(); i3++) {
            d dVar = (d) this.g.get(i3);
            dVar.o = true;
            dVar.p = this.r.getTime();
            strArr[i3] = dVar.f3022a;
            if (dVar.k) {
                i2++;
            }
        }
        int size = this.g.size();
        if (b2 != null && b2.before(date)) {
            new l(this, strArr, (date.getTime() - b2.getTime()) / 1000, i2, size).start();
            this.u.a("bannerbegintime" + this.f2806a, (Date) null);
        }
        new Thread(new m(this)).start();
    }

    public final boolean j() {
        if (this.j) {
            return false;
        }
        this.j = true;
        this.n.postDelayed(new n(this), 1000);
        return true;
    }

    /* access modifiers changed from: protected */
    public final void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.y) {
            this.y = false;
            h();
        }
    }

    /* access modifiers changed from: protected */
    public final void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        n();
        this.y = true;
        if (this.g != null && !this.g.isEmpty()) {
            for (d dVar : this.g) {
                if (dVar.a() != null) {
                    j.a(dVar.getLoadImageId(), dVar.a());
                }
            }
        }
    }

    public final boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        return false;
    }

    public final void setBannerPadding$3b4dfe4b(int i2) {
        this.b = 0;
        if (i2 != -1) {
            this.c = i2;
        }
        if (getVisibility() == 0) {
            this.p.setPadding(this.d, this.b, this.e, this.c);
        }
    }

    public final void setContext(Context context) {
        this.z = context;
    }
}
