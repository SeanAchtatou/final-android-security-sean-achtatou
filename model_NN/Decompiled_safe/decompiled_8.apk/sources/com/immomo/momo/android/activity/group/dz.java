package com.immomo.momo.android.activity.group;

import android.content.Context;
import android.content.Intent;
import com.immomo.momo.R;
import com.immomo.momo.android.broadcast.u;
import com.immomo.momo.android.broadcast.w;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.n;
import com.immomo.momo.service.y;
import com.immomo.momo.util.ao;

final class dz extends d {

    /* renamed from: a  reason: collision with root package name */
    private v f1622a = null;
    private /* synthetic */ GroupProfileActivity c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public dz(GroupProfileActivity groupProfileActivity, Context context) {
        super(context);
        this.c = groupProfileActivity;
        this.f1622a = new v(context);
        this.f1622a.setCancelable(true);
        this.f1622a.setOnCancelListener(new ea(this));
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        n.a().b(this.c.l);
        new y().b(this.c.f.h, this.c.l);
        return true;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.c.a(this.f1622a);
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        super.a(exc);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        Boolean bool = (Boolean) obj;
        super.a(bool);
        if (bool.booleanValue()) {
            ao.g(R.string.group_setting_quit_success);
            Intent intent = new Intent(u.b);
            intent.putExtra("gid", this.c.l);
            this.c.sendBroadcast(intent);
            this.c.sendBroadcast(new Intent(w.b));
            return;
        }
        ao.g(R.string.group_setting_quit_failed);
    }

    /* access modifiers changed from: protected */
    public final void b() {
        super.b();
        this.c.p();
    }
}
