package com.immomo.momo.android.a;

import android.content.Context;
import android.support.v4.b.a;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.android.view.MomoProgressbar;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.aj;
import com.immomo.momo.service.bean.bh;
import com.immomo.momo.util.aw;
import com.immomo.momo.util.j;
import com.immomo.momo.util.m;
import java.util.List;

public final class o extends a {
    private int d = 0;

    public o(Context context, List list, bh bhVar) {
        super(context, list);
        new m(this);
        a(bhVar);
    }

    public final void a(bh bhVar) {
        this.d = e(bhVar);
        notifyDataSetChanged();
    }

    public final void d(int i) {
        this.d = i;
        notifyDataSetChanged();
    }

    public final bh e() {
        if (this.d < 0) {
            return null;
        }
        return (bh) getItem(this.d);
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        p pVar;
        bh bhVar = (bh) getItem(i);
        if (view == null) {
            view = a((int) R.layout.listitem_chatbackground);
            p pVar2 = new p((byte) 0);
            view.setTag(pVar2);
            pVar2.f = (ImageView) view.findViewById(R.id.chatbackground_iv_pic);
            pVar2.f910a = view.findViewById(R.id.chatbackground_iv_add);
            pVar2.b = view.findViewById(R.id.chatbackground_iv_selected);
            pVar2.d = view.findViewById(R.id.chatbackground_iv_download);
            pVar2.c = view.findViewById(R.id.chatbackground_layout_content);
            pVar2.e = (TextView) view.findViewById(R.id.chatbackground_tv_desc);
            pVar2.g = (MomoProgressbar) view.findViewById(R.id.chatbackground_mp_progress);
            pVar2.g.setBackgroud(R.color.dark_gray);
            pVar2.g.setInnderDrawable(R.color.blue);
            pVar2.g.setProgressHeight(g.a(30.0f));
            ViewGroup.LayoutParams layoutParams = pVar2.c.getLayoutParams();
            int J = (g.J() - (g.a(5.0f) * 4)) / 3;
            layoutParams.height = J;
            layoutParams.width = J;
            pVar2.c.setLayoutParams(layoutParams);
            pVar = pVar2;
        } else {
            pVar = (p) view.getTag();
        }
        if (i == this.d) {
            pVar.b.setVisibility(0);
        } else {
            pVar.b.setVisibility(8);
        }
        if (i == 0) {
            pVar.e.setText("默认");
            pVar.e.setVisibility(0);
            pVar.g.setVisibility(8);
            pVar.d.setVisibility(8);
        } else if (i == getCount() - 1) {
            pVar.e.setText("自定义");
            pVar.e.setVisibility(0);
            pVar.g.setVisibility(8);
            pVar.d.setVisibility(8);
        } else if (bhVar.isImageLoading()) {
            pVar.e.setVisibility(8);
            pVar.g.setVisibility(0);
            pVar.g.setMax(bhVar.c);
            pVar.g.setProgress(bhVar.b);
            pVar.d.setVisibility(8);
        } else if (!aw.a(bhVar)) {
            pVar.d.setVisibility(0);
            pVar.g.setVisibility(8);
            pVar.e.setVisibility(8);
        } else {
            pVar.e.setVisibility(8);
            pVar.g.setVisibility(8);
            pVar.d.setVisibility(8);
        }
        if (i != getCount() - 1) {
            j.a((aj) getItem(i), pVar.f, (ViewGroup) null, 18);
            pVar.f910a.setVisibility(8);
        } else if (!a.f(((bh) getItem(i)).f3013a) || i != this.d) {
            pVar.f.setImageResource(g.a("drawable/" + getItem(0)));
            pVar.f910a.setVisibility(0);
        } else {
            pVar.f910a.setVisibility(8);
            pVar.f.setImageBitmap(a.l(com.immomo.momo.a.g() + "/t/" + ((bh) getItem(i)).f3013a + ".jpg_"));
        }
        return view;
    }
}
