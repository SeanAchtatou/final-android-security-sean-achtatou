package com.immomo.momo.android.view.a;

import android.content.Context;
import android.widget.ListView;

final class h extends ListView {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public boolean f2696a;
    private boolean b;

    public h(Context context, boolean z) {
        super(context, null, 0);
        this.b = z;
        setCacheColorHint(0);
    }

    public final boolean hasFocus() {
        return this.b || super.hasFocus();
    }

    public final boolean hasWindowFocus() {
        return this.b || super.hasWindowFocus();
    }

    public final boolean isFocused() {
        return this.b || super.isFocused();
    }

    public final boolean isInTouchMode() {
        return (this.b && this.f2696a) || super.isInTouchMode();
    }
}
