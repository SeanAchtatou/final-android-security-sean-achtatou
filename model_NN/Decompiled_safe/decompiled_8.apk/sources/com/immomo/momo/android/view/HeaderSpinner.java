package com.immomo.momo.android.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.android.plugin.cropimage.q;
import com.immomo.momo.android.view.a.al;
import com.immomo.momo.util.m;
import mm.purchasesdk.PurchaseCode;

public class HeaderSpinner extends FrameLayout implements View.OnClickListener, AdapterView.OnItemClickListener, PopupWindow.OnDismissListener {

    /* renamed from: a  reason: collision with root package name */
    private TextView f2633a = null;
    private int b = -1;
    private View c = null;
    private RotatingImageView d = null;
    private dc e = null;
    private al f;

    public HeaderSpinner(Context context) {
        super(context);
        new m(this);
        this.f = null;
        a();
    }

    public HeaderSpinner(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        new m(this);
        this.f = null;
        a();
    }

    public HeaderSpinner(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        new m(this);
        this.f = null;
        a();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, com.immomo.momo.android.view.HeaderSpinner, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    private void a() {
        ((LayoutInflater) getContext().getSystemService("layout_inflater")).inflate((int) R.layout.common_headerbar_spinner, (ViewGroup) this, true);
        this.f2633a = (TextView) findViewById(R.id.spinner_stv_text);
        this.c = findViewById(R.id.root_layout);
        this.c.setOnClickListener(this);
        this.d = (RotatingImageView) findViewById(R.id.spinner_iv_rotaing);
        findViewById(R.id.spinner_iv_tips);
        this.e = new dc(getContext(), new LinearInterpolator(), (byte) 0);
    }

    public void computeScroll() {
        if (this.e.d()) {
            this.d.setDegress(this.e.b());
            postInvalidate();
        }
    }

    public String[] getOptions() {
        return null;
    }

    public int getSelectedIndex() {
        return this.b;
    }

    public TextView getTextView() {
        return this.f2633a;
    }

    public boolean isSelected() {
        return this.c.isSelected();
    }

    public boolean isShown() {
        return super.isShown();
    }

    public void onClick(View view) {
    }

    public void onDismiss() {
        setSelected(false);
    }

    public void onItemClick(AdapterView adapterView, View view, int i, long j) {
        this.b = i;
        setText((CharSequence) null);
        if (this.f != null) {
            this.f.a(i);
        }
    }

    public void setOnClickListener(View.OnClickListener onClickListener) {
        this.c.setOnClickListener(onClickListener);
    }

    public void setOnDropdownListener$28bc66b(q qVar) {
    }

    public void setOnItemClickListener(al alVar) {
        this.f = alVar;
    }

    public void setSelected(boolean z) {
        this.c.setSelected(z);
        if (!this.e.a()) {
            this.e.e();
        }
        if (z) {
            this.e.a(0, 0, -180, 0, PurchaseCode.UNSUPPORT_ENCODING_ERR);
        } else {
            this.e.a(-180, 0, 180, 0, PurchaseCode.UNSUPPORT_ENCODING_ERR);
        }
        invalidate();
    }

    public void setSelectedIndex(int i) {
        this.b = i;
    }

    public void setText(int i) {
        this.f2633a.setText(getContext().getString(i));
    }

    public void setText(CharSequence charSequence) {
        this.f2633a.setText(charSequence);
    }
}
