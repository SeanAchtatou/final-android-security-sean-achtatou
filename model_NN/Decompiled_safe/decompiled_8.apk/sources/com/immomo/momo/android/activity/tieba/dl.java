package com.immomo.momo.android.activity.tieba;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;

final class dl extends d {

    /* renamed from: a  reason: collision with root package name */
    private v f2240a;
    /* access modifiers changed from: private */
    public /* synthetic */ TiebaCreateActivity c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public dl(TiebaCreateActivity tiebaCreateActivity, Context context) {
        super(context);
        this.c = tiebaCreateActivity;
        if (tiebaCreateActivity.y != null && !tiebaCreateActivity.y.isCancelled()) {
            tiebaCreateActivity.t.cancel(true);
        }
        tiebaCreateActivity.y = this;
        this.f2240a = new v(f());
        this.f2240a.a("请求提交中");
        this.f2240a.setCancelable(true);
        this.f2240a.setOnCancelListener(new dm(this));
        tiebaCreateActivity.a(this.f2240a);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        return com.immomo.momo.protocol.a.v.a().f(this.c.s);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        com.immomo.momo.service.bean.c.d dVar = (com.immomo.momo.service.bean.c.d) obj;
        this.c.v = dVar.g;
        this.c.w = dVar.m;
        this.c.u = dVar.b;
        this.c.u();
        this.c.v();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.c.t = (dr) null;
        this.c.p();
    }
}
