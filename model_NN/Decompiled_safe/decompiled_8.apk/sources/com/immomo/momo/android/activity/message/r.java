package com.immomo.momo.android.activity.message;

import android.content.Context;
import android.support.v4.b.a;
import android.widget.ImageView;
import com.immomo.momo.R;
import com.immomo.momo.android.c.d;
import com.immomo.momo.service.bean.bh;
import com.immomo.momo.util.aw;

final class r extends d {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public bh f1991a;
    private /* synthetic */ a c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public r(a aVar, Context context, bh bhVar) {
        super(context);
        this.c = aVar;
        this.f1991a = bhVar;
    }

    /* access modifiers changed from: protected */
    public final Object a(Object... objArr) {
        ImageView imageView = (ImageView) this.c.findViewById(R.id.chat_iv_background);
        try {
            if (!a.f(this.f1991a.f3013a)) {
                this.f1991a.f3013a = (String) this.c.o().b("chatbg_resourseid", "bg_chat_preview_001");
            }
            if ("bg_chat_preview_001".equals(this.f1991a.f3013a)) {
                imageView.post(new s(imageView));
            } else if (aw.a(this.f1991a)) {
                imageView.post(new t(this, imageView));
            } else if (this.f1991a.a().indexOf("bg_chat_") < 0) {
                imageView.post(new u(imageView));
            } else {
                aw.a().a(this.f1991a, null, new w(this.c, this.f1991a));
            }
        } catch (Throwable th) {
            this.b.a(th);
        }
        return null;
    }
}
