package com.immomo.momo.android.activity.contacts;

import android.support.v4.b.a;
import com.immomo.momo.android.view.cv;
import java.util.Date;

final class cb implements cv {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ bk f1183a;

    private cb(bk bkVar) {
        this.f1183a = bkVar;
    }

    /* synthetic */ cb(bk bkVar, byte b) {
        this(bkVar);
    }

    public final void v() {
        this.f1183a.S = new Date();
        this.f1183a.N.c("lasttime_my_grouplist", a.c(this.f1183a.S));
        this.f1183a.O.n();
        if (this.f1183a.W != null && !this.f1183a.W.isCancelled()) {
            this.f1183a.W.cancel(true);
        }
    }
}
