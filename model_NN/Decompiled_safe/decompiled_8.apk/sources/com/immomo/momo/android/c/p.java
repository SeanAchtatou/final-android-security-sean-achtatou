package com.immomo.momo.android.c;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.support.v4.b.a;
import android.view.View;
import android.widget.ImageView;
import com.immomo.momo.R;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.aj;
import com.immomo.momo.util.j;
import com.immomo.momo.util.m;
import java.lang.ref.WeakReference;

public final class p implements g {

    /* renamed from: a  reason: collision with root package name */
    private aj f2390a = null;
    /* access modifiers changed from: private */
    public Bitmap b = null;
    private Handler c = null;
    private WeakReference d = null;
    /* access modifiers changed from: private */
    public String e = null;
    private boolean f = false;
    private int g = -1;
    private boolean h = false;
    private long i = 0;
    /* access modifiers changed from: private */
    public m j = new m(getClass().getSimpleName()).b();

    public p(aj ajVar, View view, boolean z, int i2) {
        this.f2390a = ajVar;
        if (view != null) {
            this.d = new WeakReference(view);
            this.c = new q(this, view.getContext().getMainLooper());
        }
        this.e = ajVar.getLoadImageId();
        this.f = z;
        this.g = i2;
    }

    public final View a() {
        if (this.d != null) {
            return (View) this.d.get();
        }
        return null;
    }

    public final void a(Bitmap bitmap) {
        j.f3089a.remove(this.e);
        this.f2390a.setImageLoading(false);
        this.f2390a.setImageCallback(null);
        this.b = bitmap;
        if (this.b != null) {
            j.a(this.e, this.b);
            if (this.f) {
                try {
                    this.b = a.a(this.b, (float) this.g);
                    j.a(String.valueOf(this.e) + "_r" + this.g, this.b);
                } catch (Throwable th) {
                }
            }
            if (this.h) {
                try {
                    Bitmap a2 = a.a(this.b, g.a(36.0f), g.a(36.0f));
                    j.b.put(this.e, a2);
                    this.b = a2;
                } catch (Throwable th2) {
                }
            }
            if (this.c != null) {
                try {
                    WeakReference weakReference = this.d;
                } catch (Exception e2) {
                }
                if (this.i > 0) {
                    try {
                        Thread.sleep(this.i);
                    } catch (InterruptedException e3) {
                    }
                }
                View a3 = a();
                if (a3 == null || !this.e.equals(a3.getTag(R.id.tag_item_imageid))) {
                    this.j.a((Object) "miss-------------------222");
                    return;
                }
                Drawable drawable = ((ImageView) a3).getDrawable();
                if (drawable == null || !(drawable instanceof BitmapDrawable) || ((BitmapDrawable) drawable).getBitmap() != this.b) {
                    this.c.sendEmptyMessageDelayed(35, 0);
                } else {
                    this.j.a((Object) "miss-------------------111");
                }
            }
        }
    }

    public final void a(View view) {
        if (view != null) {
            this.d = new WeakReference(view);
        } else {
            this.d = null;
        }
    }

    public final void a(boolean z) {
        this.h = z;
    }

    public final void b() {
        this.i = 100;
    }
}
