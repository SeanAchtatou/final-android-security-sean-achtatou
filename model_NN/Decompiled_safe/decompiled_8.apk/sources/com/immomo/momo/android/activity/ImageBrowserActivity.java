package com.immomo.momo.android.activity;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.b.a;
import android.text.SpannableString;
import android.text.style.RelativeSizeSpan;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;
import com.baidu.location.LocationClientOption;
import com.immomo.momo.R;
import com.immomo.momo.android.c.k;
import com.immomo.momo.android.c.o;
import com.immomo.momo.android.c.r;
import com.immomo.momo.android.c.t;
import com.immomo.momo.android.c.u;
import com.immomo.momo.android.view.ScrollViewPager;
import com.immomo.momo.android.view.photoview.PhotoView;
import com.immomo.momo.g;
import com.immomo.momo.util.m;
import java.util.ArrayList;
import java.util.List;

public class ImageBrowserActivity extends ao {
    private static u s = new u(2, 2);
    private static u t = new u(2, 2);
    private static u u = new u(3, 3);
    List h = null;
    /* access modifiers changed from: private */
    public m i = new m("ImageBrowserActivity");
    /* access modifiers changed from: private */
    public fm j = null;
    /* access modifiers changed from: private */
    public ScrollViewPager k = null;
    private TextView l = null;
    private View m = null;
    private View n = null;
    private int o = -1;
    /* access modifiers changed from: private */
    public int p = -1;
    private boolean q = false;
    /* access modifiers changed from: private */
    public String r = "mimageid";
    /* access modifiers changed from: private */
    public String v;
    /* access modifiers changed from: private */
    public Handler w = new Handler();

    static /* synthetic */ void a(ImageBrowserActivity imageBrowserActivity, fn fnVar, PhotoView photoView) {
        if (imageBrowserActivity.o < 0) {
            return;
        }
        if (fnVar.b() != null) {
            photoView.setImageBitmap(fnVar.b());
        } else if (fnVar.a() != null) {
            photoView.setImageBitmap(fnVar.a());
        } else if (!fnVar.f1496a) {
            fj fjVar = new fj(imageBrowserActivity, fnVar, photoView);
            if (!a.a((CharSequence) fnVar.c)) {
                r rVar = new r(fnVar.c, fjVar, imageBrowserActivity.o, null);
                if (imageBrowserActivity.r.equals("URL") && !a.a((CharSequence) fnVar.c())) {
                    rVar.a(fnVar.c());
                }
                if (imageBrowserActivity.h.size() != 2) {
                    fnVar.f1496a = true;
                }
                a(rVar);
            }
        }
    }

    public static void a(o oVar) {
        u uVar = oVar.c() ? s : g.aa() ? u : t;
        if (!uVar.getQueue().isEmpty()) {
            while (true) {
                Runnable poll = uVar.getQueue().poll();
                if (poll == null) {
                    break;
                }
                try {
                    ((t) poll).a((Object) null);
                } catch (Exception e) {
                }
            }
        }
        uVar.execute(oVar);
    }

    /* access modifiers changed from: private */
    public int c(int i2) {
        return i2 < this.h.size() ? i2 : i2 % this.h.size();
    }

    public final void a(View view, int i2) {
        int c = c(i2);
        this.i.a((Object) ("position=" + c));
        if (this.q) {
            String str = String.valueOf(c + 1) + "/" + this.h.size();
            SpannableString spannableString = new SpannableString(str);
            spannableString.setSpan(new RelativeSizeSpan(1.5f), 0, str.indexOf(47), 17);
            this.l.setText(spannableString);
        }
        if (view != null) {
            fn fnVar = (fn) this.h.get(c);
            PhotoView photoView = (PhotoView) view.findViewById(R.id.imageview);
            fnVar.a(photoView);
            Bitmap b = fnVar.b();
            if (b != null) {
                this.i.a((Object) ("largeBitmap =" + b));
                Drawable drawable = photoView.getDrawable();
                if (drawable == null || !(drawable instanceof BitmapDrawable)) {
                    photoView.setImageBitmap(b);
                    return;
                }
                Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
                if (bitmap == null || bitmap != b) {
                    photoView.setImageBitmap(b);
                } else {
                    this.i.a((Object) ("b=" + bitmap));
                }
            } else {
                this.w.postDelayed(new fi(this, view, fnVar), 300);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ViewPager.a(int, boolean):void
     arg types: [int, int]
     candidates:
      android.support.v4.view.ViewPager.a(android.graphics.Rect, android.view.View):android.graphics.Rect
      android.support.v4.view.ViewPager.a(int, int):android.support.v4.view.bb
      android.support.v4.view.ViewPager.a(int, float):void
      android.support.v4.view.ViewPager.a(int, boolean):void */
    public void onCreate(Bundle bundle) {
        String string;
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_imagebrowser);
        this.k = (ScrollViewPager) findViewById(R.id.viewpager);
        this.l = (TextView) findViewById(R.id.imagebrower_tv_page);
        this.m = findViewById(R.id.imagebrower_layout_pagelayout);
        this.n = findViewById(R.id.imagebrower_iv_save);
        this.n.setOnClickListener(new fg(this));
        this.k.setOnPageChangeListener(new fh(this));
        ArrayList arrayList = new ArrayList();
        Bundle extras = getIntent().getExtras();
        if (!(extras == null || (string = extras.getString("imagetype")) == null)) {
            this.v = string;
            if (string.equals("avator")) {
                this.o = 3;
                this.p = 2;
                this.m.setVisibility(0);
                this.n.setVisibility(8);
                this.q = true;
            } else if (string.equals("chat")) {
                this.o = 1;
                this.p = 0;
                this.m.setVisibility(0);
                this.q = true;
            } else if (string.equals("gchat")) {
                this.o = 14;
                this.p = 13;
                this.m.setVisibility(0);
                this.q = true;
            } else if (string.equals("weibo")) {
                this.o = 5;
                this.p = 6;
            } else if (string.equals("feed")) {
                this.o = 15;
                this.p = 16;
                this.m.setVisibility(0);
                this.n.setVisibility(8);
                this.q = true;
            } else if (string.equals("event")) {
                this.o = 20;
                this.p = 21;
            }
            this.r = a.a(extras.getString("model")) ? "mimageid" : extras.getString("model");
            if (this.r.equals("mimageid")) {
                String[] stringArray = extras.getStringArray("array");
                if (stringArray != null) {
                    for (String fnVar : stringArray) {
                        arrayList.add(new fn(fnVar));
                    }
                }
            } else if (this.r.equals("URL")) {
                String[] stringArray2 = extras.getStringArray("thumb_url_array");
                String[] stringArray3 = extras.getStringArray("large_url_array");
                if (!(stringArray2 == null || stringArray3 == null)) {
                    for (int i2 = 0; i2 < stringArray2.length; i2++) {
                        fn fnVar2 = new fn();
                        fnVar2.a(stringArray2[i2]);
                        fnVar2.e = this.p;
                        if (i2 < stringArray3.length) {
                            fnVar2.b(stringArray3[i2]);
                        }
                        arrayList.add(fnVar2);
                    }
                }
            }
        }
        int i3 = extras.getInt("index", 0);
        if (i3 >= arrayList.size()) {
            i3 = arrayList.size() - 1;
        }
        if (i3 < 0) {
            i3 = 0;
        }
        this.h = arrayList;
        if (this.h.size() > 1) {
            i3 += this.h.size() * LocationClientOption.MIN_SCAN_SPAN;
        } else if (this.h.size() == 0) {
            finish();
            return;
        }
        this.j = new fm(this);
        this.k.setAdapter(this.j);
        this.k.a(i3, false);
        this.w.postDelayed(new ff(this, i3), 100);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.j != null) {
            for (fn fnVar : this.h) {
                k.a(fnVar.b());
                k.a(fnVar.a());
            }
        }
        t.getQueue().clear();
        s.getQueue().clear();
        u.getQueue().clear();
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 == 4) {
            overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
        }
        return super.onKeyDown(i2, keyEvent);
    }
}
