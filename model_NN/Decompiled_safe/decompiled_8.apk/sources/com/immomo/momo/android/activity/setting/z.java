package com.immomo.momo.android.activity.setting;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.b.a;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.android.c.g;
import com.sina.sdk.api.message.InviteApi;

final class z implements g {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ x f2149a;
    private final /* synthetic */ String b;

    z(x xVar, String str) {
        this.f2149a = xVar;
        this.b = str;
    }

    public final /* synthetic */ void a(Object obj) {
        Message message = new Message();
        message.obj = (Bitmap) obj;
        Bundle bundle = new Bundle();
        bundle.putString(InviteApi.KEY_URL, a.a(this.b) ? PoiTypeDef.All : this.b);
        message.setData(bundle);
        this.f2149a.f2147a.h.sendMessage(message);
    }
}
