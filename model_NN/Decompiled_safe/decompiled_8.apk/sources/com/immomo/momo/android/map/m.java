package com.immomo.momo.android.map;

import android.view.View;
import android.widget.AdapterView;
import com.immomo.momo.android.view.a.n;
import com.immomo.momo.service.bean.az;

final class m implements AdapterView.OnItemClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ CreateSiteActivity f2507a;
    private final /* synthetic */ n b;

    m(CreateSiteActivity createSiteActivity, n nVar) {
        this.f2507a = createSiteActivity;
        this.b = nVar;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        az azVar = (az) view.getTag();
        if (azVar != null) {
            try {
                this.f2507a.k = Integer.parseInt(azVar.c);
                this.f2507a.l = azVar.b;
                this.f2507a.i.setText(this.f2507a.l);
                this.b.dismiss();
            } catch (Throwable th) {
                this.f2507a.e.a(th);
            }
        }
    }
}
