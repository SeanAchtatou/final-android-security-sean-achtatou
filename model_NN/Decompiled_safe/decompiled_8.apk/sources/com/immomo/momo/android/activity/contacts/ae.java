package com.immomo.momo.android.activity.contacts;

import android.content.DialogInterface;
import com.immomo.momo.android.view.EmoteEditeText;
import com.immomo.momo.service.bean.g;
import com.immomo.momo.service.bean.h;

final class ae implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ContactPeopleActivity f1139a;
    private final /* synthetic */ EmoteEditeText b;
    private final /* synthetic */ int c;
    private final /* synthetic */ int d;

    ae(ContactPeopleActivity contactPeopleActivity, EmoteEditeText emoteEditeText, int i, int i2) {
        this.f1139a = contactPeopleActivity;
        this.b = emoteEditeText;
        this.c = i;
        this.d = i2;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        new ah(this.f1139a, this.f1139a, this.b.getText().toString().trim(), ((g) ((h) this.f1139a.k.get(this.c)).b.get(this.d)).d, this.c, this.d).execute(new Object[0]);
        dialogInterface.dismiss();
    }
}
