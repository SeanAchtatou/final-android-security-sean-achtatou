package com.immomo.momo.android.activity.tieba;

import android.content.Intent;
import android.view.View;

final class dd implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ TiebaCategoryDetailActivity f2232a;

    dd(TiebaCategoryDetailActivity tiebaCategoryDetailActivity) {
        this.f2232a = tiebaCategoryDetailActivity;
    }

    public final void onClick(View view) {
        this.f2232a.startActivity(new Intent(this.f2232a, TiebaSearchActivity.class));
    }
}
