package com.immomo.momo.android.activity.tieba;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.protocol.a.v;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

final class fi extends d {

    /* renamed from: a  reason: collision with root package name */
    private List f2286a = new ArrayList();
    private /* synthetic */ TiebaVerifyActivity c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public fi(TiebaVerifyActivity tiebaVerifyActivity, Context context) {
        super(context);
        this.c = tiebaVerifyActivity;
        if (tiebaVerifyActivity.j != null) {
            tiebaVerifyActivity.j.cancel(true);
        }
        if (tiebaVerifyActivity.k != null && tiebaVerifyActivity.k.isCancelled()) {
            tiebaVerifyActivity.k.cancel(true);
        }
        tiebaVerifyActivity.j = this;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        return Boolean.valueOf(v.a().a(this.c.f.h, 0, this.f2286a));
    }

    /* access modifiers changed from: protected */
    public final void a() {
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        this.c.i.b();
        this.c.i.b((Collection) this.f2286a);
        this.c.i.notifyDataSetChanged();
        this.c.m.clear();
        this.c.m.addAll(this.f2286a);
        if (((Boolean) obj).booleanValue()) {
            this.c.l.setVisibility(0);
        } else {
            this.c.l.setVisibility(8);
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.c.h.n();
        this.c.j = (fi) null;
    }
}
