package com.immomo.momo.android.activity;

import android.content.DialogInterface;

final class z implements DialogInterface.OnCancelListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ y f2311a;

    z(y yVar) {
        this.f2311a = yVar;
    }

    public final void onCancel(DialogInterface dialogInterface) {
        this.f2311a.cancel(true);
        this.f2311a.c.finish();
    }
}
