package com.immomo.momo.android.activity.common;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.m;

final class bs extends d {

    /* renamed from: a  reason: collision with root package name */
    private int f1112a;
    private String c;
    private String d;
    private v e;
    private /* synthetic */ ShareToMomoActivity f;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public bs(ShareToMomoActivity shareToMomoActivity, Context context, int i, String str, String str2) {
        super(context);
        this.f = shareToMomoActivity;
        this.f1112a = i;
        this.c = str;
        this.d = str2;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        switch (this.f1112a) {
            case 1:
                m.a().a(this.f.n, this.f.m, this.c, this.f.p, this.d, this.f.o);
                return null;
            case 2:
                m.a().b(this.f.n, this.f.m, this.c, this.f.p, this.d, this.f.o);
                return null;
            case 3:
                m.a().c(this.f.n, this.f.m, this.c, this.f.p, this.d, this.f.o);
                return null;
            default:
                return null;
        }
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.e = new v(this.f);
        this.e.a("请求提交中");
        this.e.setCancelable(true);
        this.e.setOnCancelListener(new bt(this));
        this.f.a(this.e);
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        super.a(exc);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        a("分享成功");
        this.f.setResult(-1);
        this.f.finish();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.f.p();
    }
}
