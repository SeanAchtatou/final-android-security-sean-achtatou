package com.immomo.momo.android.activity;

import android.content.DialogInterface;
import com.immomo.momo.android.view.EmoteEditeText;

final class il implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ OtherProfileV2Activity f1742a;
    private final /* synthetic */ EmoteEditeText b;

    il(OtherProfileV2Activity otherProfileV2Activity, EmoteEditeText emoteEditeText) {
        this.f1742a = otherProfileV2Activity;
        this.b = emoteEditeText;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        OtherProfileV2Activity.a(this.f1742a, this.b);
        dialogInterface.dismiss();
    }
}
