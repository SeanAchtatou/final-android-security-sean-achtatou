package com.immomo.momo.android.view.a;

import android.content.Context;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Gallery;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import com.immomo.momo.R;

public final class bj extends aw implements View.OnClickListener, AdapterView.OnItemSelectedListener {

    /* renamed from: a  reason: collision with root package name */
    Gallery f2689a;
    private bl b;
    /* access modifiers changed from: private */
    public int e = 1;
    /* access modifiers changed from: private */
    public int f;
    private int g;
    private TextView h;
    private TextView i;
    private Button j;
    private Button k;
    private View l;
    private al m;

    public bj(Context context, int i2, int i3) {
        super(context, R.layout.common_pager);
        b((int) R.style.Popup_Animation_Alpha);
        this.f = i2;
        this.g = i3;
        this.f2689a = (Gallery) c(R.id.tiebapager_gallery);
        this.h = (TextView) c(R.id.tiebapager_tv_select);
        this.i = (TextView) c(R.id.tiebapager_tv_pageindex);
        this.j = (Button) c(R.id.tiebapager_btn_submit);
        this.k = (Button) c(R.id.tiebapager_btn_cancel);
        this.l = c(R.id.layout_root);
        this.b = new bl(this, context);
        this.f2689a.setAdapter((SpinnerAdapter) new bl(this, context));
        this.f2689a.setOnItemSelectedListener(this);
        this.f2689a.setSelection(this.g - 1);
        this.j.setOnClickListener(this);
        this.k.setOnClickListener(this);
        this.l.setOnTouchListener(new bk(this));
    }

    public final void a(View view) {
        super.a(view);
    }

    public final void a(al alVar) {
        this.m = alVar;
    }

    public final void onClick(View view) {
        switch (view.getId()) {
            case R.id.tiebapager_btn_submit /*2131166054*/:
                if (this.m != null) {
                    this.m.a(this.g);
                }
                e();
                return;
            case R.id.tiebapager_btn_cancel /*2131166055*/:
                e();
                return;
            default:
                return;
        }
    }

    public final void onItemSelected(AdapterView adapterView, View view, int i2, long j2) {
        this.g = i2 + 1;
        this.h.setText(new StringBuilder(String.valueOf(this.g)).toString());
        this.i.setText(String.valueOf(this.g) + "/" + this.b.getCount());
    }

    public final void onNothingSelected(AdapterView adapterView) {
    }
}
