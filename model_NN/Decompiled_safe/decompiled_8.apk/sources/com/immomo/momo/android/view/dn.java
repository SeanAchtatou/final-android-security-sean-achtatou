package com.immomo.momo.android.view;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.protocol.a.v;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

final class dn extends d {

    /* renamed from: a  reason: collision with root package name */
    private List f2783a;
    private /* synthetic */ TiebaSearchView c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public dn(TiebaSearchView tiebaSearchView, Context context) {
        super(context);
        this.c = tiebaSearchView;
        if (tiebaSearchView.d != null && !tiebaSearchView.d.isCancelled()) {
            tiebaSearchView.d.cancel(true);
        }
        tiebaSearchView.d = this;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        this.f2783a = new ArrayList();
        return Boolean.valueOf(v.a().a(this.c.f, this.f2783a));
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.c.g.u();
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        Boolean bool = (Boolean) obj;
        this.c.c.b((Collection) this.f2783a);
        this.c.b.setVisibility(0);
        if (bool.booleanValue() || this.f2783a.size() == 0) {
            this.c.i.setVisibility(0);
            if (this.f2783a.size() == 0) {
                this.c.j.setVisibility(0);
            } else {
                this.c.j.setVisibility(8);
            }
            if (bool.booleanValue()) {
                this.c.f2658a.setText("你可以申请创建\"" + this.c.f + "\"陌陌吧");
                this.c.f2658a.setVisibility(0);
                return;
            }
            this.c.f2658a.setVisibility(8);
            return;
        }
        this.c.i.setVisibility(8);
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.c.d = (dn) null;
        this.c.g.v();
    }
}
