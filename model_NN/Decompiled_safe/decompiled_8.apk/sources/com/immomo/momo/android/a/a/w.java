package com.immomo.momo.android.a.a;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.net.Uri;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.amap.mapapi.location.LocationManagerProxy;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.message.a;
import com.immomo.momo.android.b.z;
import com.immomo.momo.android.c.s;
import com.immomo.momo.android.c.u;
import com.immomo.momo.android.map.AMapActivity;
import com.immomo.momo.android.map.GoogleMapActivity;
import com.immomo.momo.android.view.a.n;
import com.immomo.momo.g;
import java.io.File;
import java.util.concurrent.ThreadPoolExecutor;
import mm.purchasesdk.PurchaseCode;

public final class w extends ab implements View.OnClickListener {
    private static ThreadPoolExecutor r = new u(3, 3);

    /* renamed from: a  reason: collision with root package name */
    private ImageView f705a;
    private ImageView o;
    /* access modifiers changed from: private */
    public AnimationDrawable p = null;
    /* access modifiers changed from: private */
    public View q;
    private Handler s = new x(this, e().getMainLooper());

    protected w(a aVar) {
        super(aVar);
    }

    private void k() {
        this.p = new AnimationDrawable();
        this.p.addFrame(g.b((int) R.drawable.ic_loading_msgplus_01), PurchaseCode.UNSUPPORT_ENCODING_ERR);
        this.p.addFrame(g.b((int) R.drawable.ic_loading_msgplus_02), PurchaseCode.UNSUPPORT_ENCODING_ERR);
        this.p.addFrame(g.b((int) R.drawable.ic_loading_msgplus_03), PurchaseCode.UNSUPPORT_ENCODING_ERR);
        this.p.addFrame(g.b((int) R.drawable.ic_loading_msgplus_04), PurchaseCode.UNSUPPORT_ENCODING_ERR);
        this.p.setOneShot(false);
        this.q.setVisibility(0);
        this.o.setImageDrawable(this.p);
        this.s.sendEmptyMessage(398);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        View inflate = this.k.inflate((int) R.layout.message_map, (ViewGroup) null);
        this.i.addView(inflate, 0);
        this.f705a = (ImageView) inflate.findViewById(R.id.message_iv_msgimage);
        this.q = inflate.findViewById(R.id.layer_download);
        this.o = (ImageView) inflate.findViewById(R.id.download_view);
        this.i.setOnClickListener(this);
        this.i.setOnLongClickListener(this);
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.q.setVisibility(8);
        this.f705a.setImageBitmap(g.w());
        if (this.h.status == 8) {
            k();
            return;
        }
        this.h.setImageId(String.valueOf(this.h.convertLat) + this.h.convertLng + ".jpg_");
        Bitmap b = b(this.h.getLoadImageId());
        if (!android.support.v4.b.a.a(Double.valueOf(this.h.convertLat)) && !android.support.v4.b.a.a(Double.valueOf(this.h.convertLng))) {
            this.f705a.setImageBitmap(g.w());
            Drawable drawable = ((ImageView) this.q.findViewById(R.id.download_view)).getDrawable();
            if (drawable != null && (drawable instanceof AnimationDrawable)) {
                ((AnimationDrawable) drawable).stop();
            }
        } else if (b != null) {
            this.f705a.setImageBitmap(b);
        } else if (this.h.isImageLoadingFailed()) {
            this.f705a.setImageBitmap(g.w());
        } else if (this.h.isLoadingResourse || e().y().g()) {
            k();
        } else {
            this.h.isLoadingResourse = true;
            y yVar = new y(this);
            if (!new File(com.immomo.momo.a.l(), String.valueOf(this.h.convertLat) + this.h.convertLng + ".jpg_").exists()) {
                j.add(this.h.msgId);
            }
            r.execute(new s(Double.valueOf(this.h.convertLat), Double.valueOf(this.h.convertLng), yVar));
        }
    }

    /* access modifiers changed from: protected */
    public final void c() {
        if (this.p != null) {
            this.p.stop();
        }
        this.q.setVisibility(8);
    }

    /* access modifiers changed from: protected */
    public final void d() {
        this.s.sendEmptyMessage(396);
    }

    public final void onClick(View view) {
        if (this.h.status != 8) {
            Location location = new Location(LocationManagerProxy.GPS_PROVIDER);
            location.setLatitude(this.h.convertLat);
            location.setLongitude(this.h.convertLng);
            location.setAccuracy(this.h.convertAcc);
            if (z.a(location)) {
                try {
                    Class.forName("com.google.android.maps.MapActivity");
                    Intent intent = new Intent(e(), GoogleMapActivity.class);
                    intent.putExtra("latitude", this.h.convertLat);
                    intent.putExtra("longitude", this.h.convertLng);
                    intent.putExtra("is_receive", this.h.receive);
                    e().startActivity(intent);
                } catch (Exception e) {
                    Location location2 = new Location(LocationManagerProxy.GPS_PROVIDER);
                    location2.setLatitude(this.h.convertLat);
                    location2.setLongitude(this.h.convertLng);
                    location2.setAccuracy(this.h.convertAcc);
                    if (z.a(location2)) {
                        Intent intent2 = new Intent(e(), AMapActivity.class);
                        intent2.putExtra("latitude", this.h.convertLat);
                        intent2.putExtra("longitude", this.h.convertLng);
                        intent2.putExtra("is_receive", this.h.receive);
                        e().startActivity(intent2);
                        return;
                    }
                    e().startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://maps.google.com/maps?q=" + this.h.convertLat + "," + this.h.convertLng)));
                }
            }
        }
    }

    public final boolean onLongClick(View view) {
        n a2 = n.a(e(), "删除消息", new z(this));
        a2.setTitle("操作");
        a2.show();
        return true;
    }
}
