package com.immomo.momo.android.map;

import android.location.Location;
import com.immomo.momo.android.b.l;
import com.immomo.momo.android.b.p;
import com.immomo.momo.android.b.z;

final class t extends p {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ GeoAMapActivity f2514a;

    t(GeoAMapActivity geoAMapActivity) {
        this.f2514a = geoAMapActivity;
    }

    public final void a(Location location, int i, int i2, int i3) {
        if (i != 0) {
            this.f2514a.k.a(location, i, i2, i3);
        } else if (z.a(location)) {
            new l(this.f2514a.k, 0).execute(location);
        } else {
            this.f2514a.k.a(location, i, i2, i3);
        }
    }
}
