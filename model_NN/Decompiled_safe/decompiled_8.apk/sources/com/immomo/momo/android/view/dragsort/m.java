package com.immomo.momo.android.view.dragsort;

import android.os.Environment;
import android.util.Log;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

final class m {

    /* renamed from: a  reason: collision with root package name */
    private StringBuilder f2797a = new StringBuilder();
    private File b = new File(Environment.getExternalStorageDirectory(), "dslv_state.txt");
    private int c = 0;
    private int d = 0;
    private boolean e = false;
    private /* synthetic */ DragSortListView f;

    public m(DragSortListView dragSortListView) {
        this.f = dragSortListView;
        if (!this.b.exists()) {
            try {
                this.b.createNewFile();
                Log.d("mobeta", "file created");
            } catch (IOException e2) {
                Log.w("mobeta", "Could not create dslv_state.txt");
                Log.d("mobeta", e2.getMessage());
            }
        }
    }

    private void d() {
        boolean z = false;
        if (this.e) {
            try {
                if (this.d != 0) {
                    z = true;
                }
                FileWriter fileWriter = new FileWriter(this.b, z);
                fileWriter.write(this.f2797a.toString());
                this.f2797a.delete(0, this.f2797a.length());
                fileWriter.flush();
                fileWriter.close();
                this.d++;
            } catch (IOException e2) {
            }
        }
    }

    public final void a() {
        this.f2797a.append("<DSLVStates>\n");
        this.d = 0;
        this.e = true;
    }

    public final void b() {
        if (this.e) {
            this.f2797a.append("<DSLVState>\n");
            int childCount = this.f.getChildCount();
            int firstVisiblePosition = this.f.getFirstVisiblePosition();
            this.f2797a.append("    <Positions>");
            for (int i = 0; i < childCount; i++) {
                this.f2797a.append(firstVisiblePosition + i).append(",");
            }
            this.f2797a.append("</Positions>\n");
            this.f2797a.append("    <Tops>");
            for (int i2 = 0; i2 < childCount; i2++) {
                this.f2797a.append(this.f.getChildAt(i2).getTop()).append(",");
            }
            this.f2797a.append("</Tops>\n");
            this.f2797a.append("    <Bottoms>");
            for (int i3 = 0; i3 < childCount; i3++) {
                this.f2797a.append(this.f.getChildAt(i3).getBottom()).append(",");
            }
            this.f2797a.append("</Bottoms>\n");
            this.f2797a.append("    <FirstExpPos>").append(this.f.p).append("</FirstExpPos>\n");
            this.f2797a.append("    <FirstExpBlankHeight>").append(this.f.c(this.f.p) - this.f.d(this.f.p)).append("</FirstExpBlankHeight>\n");
            this.f2797a.append("    <SecondExpPos>").append(this.f.q).append("</SecondExpPos>\n");
            this.f2797a.append("    <SecondExpBlankHeight>").append(this.f.c(this.f.q) - this.f.d(this.f.q)).append("</SecondExpBlankHeight>\n");
            this.f2797a.append("    <SrcPos>").append(this.f.s).append("</SrcPos>\n");
            this.f2797a.append("    <SrcHeight>").append(this.f.C + this.f.getDividerHeight()).append("</SrcHeight>\n");
            this.f2797a.append("    <ViewHeight>").append(this.f.getHeight()).append("</ViewHeight>\n");
            this.f2797a.append("    <LastY>").append(this.f.T).append("</LastY>\n");
            this.f2797a.append("    <FloatY>").append(this.f.j).append("</FloatY>\n");
            this.f2797a.append("    <ShuffleEdges>");
            for (int i4 = 0; i4 < childCount; i4++) {
                this.f2797a.append(this.f.b(firstVisiblePosition + i4, this.f.getChildAt(i4).getTop())).append(",");
            }
            this.f2797a.append("</ShuffleEdges>\n");
            this.f2797a.append("</DSLVState>\n");
            this.c++;
            if (this.c > 1000) {
                d();
                this.c = 0;
            }
        }
    }

    public final void c() {
        if (this.e) {
            this.f2797a.append("</DSLVStates>\n");
            d();
            this.e = false;
        }
    }
}
