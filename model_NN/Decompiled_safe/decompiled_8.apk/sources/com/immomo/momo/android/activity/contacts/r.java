package com.immomo.momo.android.activity.contacts;

import android.content.DialogInterface;
import com.immomo.momo.android.view.EmoteEditeText;

final class r implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ CommunityPeopleActivity f1196a;
    private final /* synthetic */ EmoteEditeText b;
    private final /* synthetic */ int c;
    private final /* synthetic */ int d;

    r(CommunityPeopleActivity communityPeopleActivity, EmoteEditeText emoteEditeText, int i, int i2) {
        this.f1196a = communityPeopleActivity;
        this.b = emoteEditeText;
        this.c = i;
        this.d = i2;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f1196a.b(new t(this.f1196a, this.f1196a, this.b.getText().toString().trim(), this.c, this.d));
        dialogInterface.dismiss();
    }
}
