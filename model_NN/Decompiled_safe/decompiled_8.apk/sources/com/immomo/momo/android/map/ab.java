package com.immomo.momo.android.map;

import android.view.View;
import com.immomo.momo.R;
import com.immomo.momo.util.ao;

final class ab implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ GoogleMapActivity f2455a;

    ab(GoogleMapActivity googleMapActivity) {
        this.f2455a = googleMapActivity;
    }

    public final void onClick(View view) {
        if (this.f2455a.b != null) {
            this.f2455a.d.getController().animateTo(this.f2455a.b);
            this.f2455a.d.getController().setZoom(18);
            return;
        }
        ao.e(R.string.map_location_error);
    }
}
