package com.immomo.momo.android.view;

import android.content.Context;
import android.graphics.drawable.Drawable;
import com.immomo.momo.R;
import com.immomo.momo.g;

public class de extends bk {

    /* renamed from: a  reason: collision with root package name */
    private int f2774a = 50;

    @Deprecated
    public de() {
    }

    public de(Context context, int i) {
        super(context, i);
    }

    public final Drawable a() {
        int i;
        int round;
        Drawable e = e();
        if (e != null) {
            if (e.getIntrinsicWidth() * e.getIntrinsicHeight() > this.f2774a * this.f2774a) {
                if (e.getIntrinsicWidth() > e.getIntrinsicHeight()) {
                    round = this.f2774a;
                    i = Math.round((((float) round) / ((float) e.getIntrinsicWidth())) * ((float) e.getIntrinsicHeight()));
                } else {
                    i = this.f2774a;
                    round = Math.round((((float) i) / ((float) e.getIntrinsicHeight())) * ((float) e.getIntrinsicWidth()));
                }
                e.setBounds(0, 0, round, i);
            } else {
                e.setBounds(0, 0, e.getIntrinsicWidth(), e.getIntrinsicHeight());
            }
        }
        return e;
    }

    public final void d() {
        this.f2774a = 60;
    }

    /* access modifiers changed from: protected */
    public Drawable e() {
        Drawable a2 = super.a();
        return a2 == null ? g.b((int) R.drawable.zemoji_error) : a2;
    }
}
