package com.immomo.momo.android.activity.emotestore;

import android.content.Context;
import android.content.Intent;
import com.immomo.momo.android.broadcast.o;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.i;
import com.immomo.momo.service.bean.q;

final class bf extends d {

    /* renamed from: a  reason: collision with root package name */
    private q f1308a = null;
    private boolean c = false;
    private /* synthetic */ av d;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public bf(av avVar, Context context, q qVar) {
        super(context);
        this.d = avVar;
        if (qVar != null) {
            this.f1308a = qVar;
            this.c = qVar.t;
            qVar.u = false;
            qVar.t = false;
            avVar.T.remove(qVar);
            avVar.U.add(0, qVar);
        }
    }

    /* access modifiers changed from: protected */
    public final Object a(Object... objArr) {
        if (this.f1308a != null) {
            this.d.P.h(this.d.U);
            this.d.P.i(this.d.T);
        }
        try {
            i.a().a(this.d.T);
        } catch (Exception e) {
        }
        try {
            if (this.f1308a == null || !this.c) {
                return null;
            }
            Runtime.getRuntime().exec("rm -r " + q.a(this.f1308a.f3035a));
            return null;
        } catch (Exception e2) {
            this.b.a((Throwable) e2);
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public final void a() {
        if (this.f1308a != null) {
            this.d.a(new v(this.d.c(), this));
        }
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        this.b.a((Throwable) exc);
    }

    /* access modifiers changed from: protected */
    public final void a(Object obj) {
        if (this.f1308a != null) {
            Intent intent = new Intent(o.f2358a);
            intent.putExtra("event", "disable");
            av avVar = this.d;
            av.b(intent);
            a("表情删除成功");
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
        if (this.f1308a != null) {
            this.d.Q.notifyDataSetChanged();
            this.d.N();
        }
    }

    /* access modifiers changed from: protected */
    public final void onCancelled() {
        if (this.f1308a != null) {
            this.d.Q.notifyDataSetChanged();
        }
    }
}
