package com.immomo.momo.android.a;

import android.support.v4.b.a;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.tieba.dw;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.bb;
import com.immomo.momo.service.bean.c.d;
import com.immomo.momo.util.j;
import java.util.HashMap;
import java.util.List;

public final class is extends b {

    /* renamed from: a  reason: collision with root package name */
    private List f885a = null;
    private ExpandableListView b = null;
    /* access modifiers changed from: private */
    public dw c;
    private int d = 0;
    private int e = 0;
    private int f = 0;
    private boolean g = false;
    private String h = "进入举报中心";
    private int i = 0;

    public is(dw dwVar, List list, ExpandableListView expandableListView, int i2, int i3, int i4, boolean z) {
        new HashMap();
        this.f885a = list;
        this.b = expandableListView;
        this.c = dwVar;
        this.e = i3;
        this.d = i2;
        this.f = i4;
        this.g = z;
    }

    private static boolean a(bb bbVar) {
        return bbVar.a() == 1;
    }

    private static boolean b(bb bbVar) {
        return bbVar.a() == 0;
    }

    /* access modifiers changed from: private */
    /* renamed from: c */
    public bb getGroup(int i2) {
        return b(i2) ? new bb(4) : this.g ? (bb) this.f885a.get(i2 - 1) : (bb) this.f885a.get(i2);
    }

    private String c() {
        return this.e > 0 ? "我的陌陌吧(" + this.e + ")" : "我的陌陌吧";
    }

    private static String d(int i2) {
        return i2 > 9999999 ? String.valueOf(i2 / 10000000) + "千万" : i2 > 9999 ? String.valueOf(i2 / 10000) + "万" : new StringBuilder(String.valueOf(i2)).toString();
    }

    /* access modifiers changed from: private */
    /* renamed from: f */
    public d getChild(int i2, int i3) {
        if (b(i2) || g(i2, i3)) {
            return null;
        }
        return (d) ((bb) this.f885a.get(i2 - (this.g ? 1 : 0))).c().get(i3);
    }

    private boolean g(int i2, int i3) {
        return i3 >= getGroup(i2).b();
    }

    public final void a() {
        for (int i2 = 0; i2 < getGroupCount(); i2++) {
            this.b.expandGroup(i2);
        }
    }

    public final void a(int i2) {
        this.i = i2;
        notifyDataSetChanged();
    }

    public final void a(View view, int i2) {
        if (i2 >= 0) {
            bb c2 = getGroup(i2);
            if (b(i2)) {
                ((TextView) view.findViewById(R.id.sitelist_tv_name)).setText("陌陌吧管理");
            } else if (b(c2)) {
                ((TextView) view.findViewById(R.id.sitelist_tv_name)).setText(c());
            } else if (a(c2)) {
                ((TextView) view.findViewById(R.id.sitelist_tv_name)).setText("推荐陌陌吧");
            } else {
                ((TextView) view.findViewById(R.id.sitelist_tv_name)).setText(PoiTypeDef.All);
            }
        }
    }

    public final void a(String str) {
        if (str == null) {
            str = PoiTypeDef.All;
        }
        this.h = str;
        notifyDataSetChanged();
    }

    public final void a(String str, int i2) {
        if (str == null) {
            str = PoiTypeDef.All;
        }
        this.h = str;
        this.i = i2;
        notifyDataSetChanged();
    }

    public final boolean a(int i2, int i3) {
        return a(getGroup(i3)) && getGroup(i3).b() == 0 && i2 == 0;
    }

    public final int b(int i2, int i3) {
        if (i3 < 0 && i2 < 0) {
            return 0;
        }
        if (i3 != -1 || this.b.isGroupExpanded(i2)) {
            return i3 == getChildrenCount(i2) + -1 ? 2 : 1;
        }
        return 0;
    }

    public final bb b() {
        for (bb bbVar : this.f885a) {
            if (a(bbVar)) {
                return bbVar;
            }
        }
        bb bbVar2 = new bb(1);
        this.f885a.add(bbVar2);
        return bbVar2;
    }

    public final boolean b(int i2) {
        return this.g && i2 == 0;
    }

    public final boolean c(int i2, int i3) {
        if (b(getGroup(i3))) {
            if ((getGroup(i3).b() == 0 ? 1 : 0) + getGroup(i3).b() == i2) {
                return true;
            }
        }
        return false;
    }

    public final boolean d(int i2, int i3) {
        return b(getGroup(i3)) && getGroup(i3).b() == 0 && i2 == 0;
    }

    public final d e(int i2, int i3) {
        if (g(i2, i3)) {
            return null;
        }
        return (d) getGroup(i2).c().get(i3);
    }

    public final long getChildId(int i2, int i3) {
        return (long) i3;
    }

    public final int getChildType(int i2, int i3) {
        if (!d(i3, i2) && !a(i3, i2)) {
            return c(i3, i2) ? 1 : 2;
        }
        return 0;
    }

    public final int getChildTypeCount() {
        return 3;
    }

    public final View getChildView(int i2, int i3, boolean z, View view, ViewGroup viewGroup) {
        if (d(i3, i2)) {
            if (view != null) {
                return view;
            }
            View inflate = g.o().inflate((int) R.layout.listitem_tieba_myempty, (ViewGroup) null);
            inflate.findViewById(R.id.tieba_tv_join).setOnClickListener(new it(this));
            return inflate;
        } else if (c(i3, i2)) {
            if (view == null) {
                view = g.o().inflate((int) R.layout.include_tiebaindex_actionbutton, (ViewGroup) null);
                view.setTag(R.id.tag_tieba_index0, (Button) view.findViewById(R.id.tiebaindex_bt_action));
                view.setTag(R.id.tag_tieba_index1, (Button) view.findViewById(R.id.tiebaindex_bt_action2));
            }
            Button button = (Button) view.getTag(R.id.tag_tieba_index0);
            Button button2 = (Button) view.getTag(R.id.tag_tieba_index1);
            if (this.d > 0) {
                button2.setText("申请中的");
                button2.setVisibility(0);
            } else {
                button2.setVisibility(8);
            }
            button.setText("待创建的陌陌吧");
            button.setVisibility(0);
            return view;
        } else if (a(i3, i2)) {
            if (view == null) {
                view = g.o().inflate((int) R.layout.listitem_relation_empty, (ViewGroup) null);
                view.setTag((TextView) view.findViewById(R.id.tv_tip));
            }
            ((TextView) view.getTag()).setText("暂无推荐的陌陌吧");
            return view;
        } else {
            if (view == null) {
                iv ivVar = new iv((byte) 0);
                view = g.o().inflate((int) R.layout.listitem_tieba, (ViewGroup) null);
                ivVar.f888a = (ImageView) view.findViewById(R.id.teibalist_item_iv_face);
                ivVar.f = (ImageView) view.findViewById(R.id.teibalist_item_iv_hot);
                ivVar.e = (ImageView) view.findViewById(R.id.tiebalist_item_iv_recommend);
                ivVar.b = (TextView) view.findViewById(R.id.tiebalist_item_tv_name);
                ivVar.d = (TextView) view.findViewById(R.id.tiebalist_item_tv_sign);
                ivVar.c = (TextView) view.findViewById(R.id.tiebalist_item_tv_attribute);
                ivVar.g = (ImageView) view.findViewById(R.id.tiebalist_item_iv_newpoint);
                ivVar.h = (TextView) view.findViewById(R.id.tiebalist_item_tv_unread);
                view.setTag(R.id.tag_userlist_item, ivVar);
            }
            iv ivVar2 = (iv) view.getTag(R.id.tag_userlist_item);
            if (b(i2)) {
                ivVar2.b.setText("举报处理");
                ivVar2.f.setVisibility(8);
                ivVar2.e.setVisibility(8);
                ivVar2.c.setText(this.h);
                ivVar2.d.setVisibility(8);
                ivVar2.g.setVisibility(8);
                ivVar2.f888a.setImageResource(R.drawable.ic_tieba_report);
                if (this.i > 0) {
                    ivVar2.h.setText(new StringBuilder(String.valueOf(this.i)).toString());
                    ivVar2.h.setVisibility(0);
                    return view;
                }
                ivVar2.h.setVisibility(8);
                return view;
            }
            d f2 = getChild(i2, i3);
            if (a.a((CharSequence) f2.b)) {
                ivVar2.b.setText(f2.f3019a);
            } else {
                ivVar2.b.setText(f2.b);
            }
            ivVar2.f.setVisibility(8);
            ivVar2.e.setVisibility(8);
            if (f2.i > 0) {
                ivVar2.c.setText("成员 " + d(f2.g) + " | 今日话题 " + d(f2.i));
            } else {
                ivVar2.c.setText("成员 " + d(f2.g));
            }
            if (f2.j != null && f2.j.length() > 70) {
                f2.j = f2.j.substring(0, 70);
            }
            ivVar2.d.setText(f2.j);
            ivVar2.d.setVisibility(0);
            ivVar2.g.setVisibility(f2.q ? 0 : 8);
            ivVar2.h.setVisibility(8);
            j.a(f2, ivVar2.f888a, (ViewGroup) null, 3);
            return view;
        }
    }

    public final int getChildrenCount(int i2) {
        boolean z = false;
        boolean z2 = true;
        if (b(i2)) {
            return 1;
        }
        bb c2 = getGroup(i2);
        if (i2 < 0 || i2 >= getGroupCount()) {
            return 0;
        }
        if (c2.c() == null) {
            return 0;
        }
        if (b(c2)) {
            int b2 = c2.b() > 0 ? c2.b() : 1;
            if (this.f + this.d <= 0) {
                z2 = false;
            }
            if (z2) {
                b2++;
            }
            return b2;
        } else if (!a(c2)) {
            return ((bb) this.f885a.get(i2)).c().size();
        } else {
            if (c2.b() == 0) {
                z = true;
            }
            if (!z) {
                return c2.b();
            }
            return 1;
        }
    }

    public final int getGroupCount() {
        return (this.g ? 1 : 0) + this.f885a.size();
    }

    public final long getGroupId(int i2) {
        return (long) i2;
    }

    public final View getGroupView(int i2, boolean z, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = g.o().inflate((int) R.layout.listitem_groupsite, (ViewGroup) null);
            iu iuVar = new iu();
            iuVar.f887a = (TextView) view.findViewById(R.id.sitelist_tv_name);
            view.findViewById(R.id.tv_groupcount);
            view.findViewById(R.id.sitelist_tv_arrow);
            view.findViewById(R.id.layout_root);
            view.setTag(R.id.tag_userlist_item, iuVar);
        }
        bb c2 = getGroup(i2);
        iu iuVar2 = (iu) view.getTag(R.id.tag_userlist_item);
        if (b(i2)) {
            iuVar2.f887a.setText("陌陌吧管理");
        } else if (b(c2)) {
            iuVar2.f887a.setText(c());
        } else if (a(c2)) {
            iuVar2.f887a.setText("推荐陌陌吧");
        } else {
            iuVar2.f887a.setText(PoiTypeDef.All);
        }
        return view;
    }

    public final boolean hasStableIds() {
        return true;
    }

    public final boolean isChildSelectable(int i2, int i3) {
        return true;
    }
}
