package com.immomo.momo.android.view;

import android.widget.AbsListView;
import android.widget.ExpandableListView;
import com.immomo.momo.g;

final class bs implements AbsListView.OnScrollListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ MomoRefreshExpandableListView f2751a;

    bs(MomoRefreshExpandableListView momoRefreshExpandableListView) {
        this.f2751a = momoRefreshExpandableListView;
    }

    public final void onScroll(AbsListView absListView, int i, int i2, int i3) {
        if (g.p()) {
            long expandableListPosition = this.f2751a.getExpandableListPosition(i);
            this.f2751a.b(ExpandableListView.getPackedPositionGroup(expandableListPosition), ExpandableListView.getPackedPositionChild(expandableListPosition));
        }
    }

    public final void onScrollStateChanged(AbsListView absListView, int i) {
    }
}
