package com.immomo.momo.android.activity.account;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.b.a;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.ViewFlipper;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.a.a.f.b;
import com.immomo.momo.MomoApplication;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.ImageFactoryActivity;
import com.immomo.momo.android.activity.ao;
import com.immomo.momo.android.view.HeaderLayout;
import com.immomo.momo.android.view.a.n;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.util.ak;
import com.immomo.momo.util.h;
import com.immomo.momo.util.m;
import java.io.File;
import java.util.Date;
import java.util.UUID;

public class RegisterActivityWithP extends ao implements View.OnClickListener {
    private static String V = PoiTypeDef.All;
    public static String h = "1";
    public static String i = "2";
    public static String j = "3";
    public static String k = "4";
    public static String l = "5";
    public static String m = "6";
    public static String n = "7";
    public int A = 0;
    public long B = 0;
    private m C = new m("RegisterActivityWithP");
    private int D = 0;
    private bf E = null;
    private ab F = null;
    private bn G = null;
    private bc H = null;
    private ck I = null;
    private cr J = null;
    private ba K = null;
    private bb L = null;
    private cc M = null;
    /* access modifiers changed from: private */
    public View N = null;
    private ViewFlipper O = null;
    private HeaderLayout P = null;
    private TextView Q = null;
    private Button R = null;
    private Button S = null;
    private InputMethodManager T = null;
    private String U = null;
    public int[] o = null;
    public String p = null;
    public String q = null;
    public String r = null;
    public boolean s = false;
    public boolean t = false;
    public boolean u = false;
    public String v;
    public String w = PoiTypeDef.All;
    public String x = PoiTypeDef.All;
    public String y = PoiTypeDef.All;
    public int z = 0;

    private void b(boolean z2) {
        if (z2) {
            this.O.showNext();
            this.D++;
            this.F = v();
            switch (this.D) {
                case 2:
                    this.S.setText((int) R.string.reg_prestep);
                    return;
                case 6:
                    this.R.setText((int) R.string.reg);
                    return;
                default:
                    return;
            }
        } else if (this.D < 7) {
            if (this.D == 6) {
                this.R.setText((int) R.string.reg);
            }
            this.F.c();
        } else if (this.F.a()) {
            b(new z(this, this, this.E, (byte) 0));
        }
    }

    private ab v() {
        ab abVar = null;
        switch (this.D) {
            case 1:
                if (this.G == null) {
                    this.G = new bn(this.E, this.O.getCurrentView(), this);
                }
                abVar = this.G;
                this.P.setTitleText("注册新账号");
                this.Q.setText("1/6");
                break;
            case 2:
                if (this.H == null) {
                    this.H = new bc(this.E, this.O.getCurrentView(), this);
                }
                abVar = this.H;
                this.P.setTitleText("填写验证码");
                this.Q.setText("2/6");
                break;
            case 3:
                if (this.I == null) {
                    this.I = new ck(this.E, this.O.getCurrentView(), this);
                }
                abVar = this.I;
                this.P.setTitleText("验证手机号码");
                this.Q.setText("2/6");
                break;
            case 4:
                if (this.J == null) {
                    this.J = new cr(this.E, this.O.getCurrentView(), this);
                }
                abVar = this.J;
                this.P.setTitleText("设置密码");
                this.Q.setText("3/6");
                break;
            case 5:
                if (this.K == null) {
                    this.K = new ba(this.E, this.O.getCurrentView(), this);
                }
                abVar = this.K;
                this.P.setTitleText("填写基本资料");
                this.Q.setText("4/6");
                break;
            case 6:
                if (this.L == null) {
                    this.L = new bb(this.E, this.O.getCurrentView(), this);
                }
                abVar = this.L;
                this.P.setTitleText("您的生日");
                this.Q.setText("5/6");
                break;
            case 7:
                if (this.M == null) {
                    this.M = new cc(this.E, this.O.getCurrentView(), this);
                }
                abVar = this.M;
                this.P.setTitleText("设置头像");
                this.Q.setText("6/6");
                break;
        }
        abVar.b();
        return abVar;
    }

    public final void a(TextView textView) {
        textView.requestFocus();
        this.T.showSoftInput(textView, 1);
    }

    public final void c(int i2) {
        f();
        if (this.D <= 1) {
            n.a(this, "确认要放弃注册么？", new t(this)).show();
            return;
        }
        this.O.setInAnimation(getApplicationContext(), R.anim.push_right_in);
        this.O.setOutAnimation(getApplicationContext(), R.anim.push_right_out);
        this.O.setDisplayedChild(this.O.getDisplayedChild() - i2);
        this.D -= i2;
        this.F.f();
        this.F = v();
        this.F.e();
        switch (this.D) {
            case 1:
                this.S.setText("返回");
                return;
            case 6:
                this.R.setText("下一步");
                return;
            default:
                return;
        }
    }

    public final void c(String str) {
        b(new y(this, str));
    }

    public final void d() {
        this.o = getIntent().getIntArrayExtra("registInterfacetype");
        this.A = getIntent().getIntExtra("timestamp", 0);
        this.B = getIntent().getLongExtra("starttime", System.currentTimeMillis());
        this.w = getIntent().getStringExtra("alipay_user_id");
        String stringExtra = getIntent().getStringExtra("alipay_user_phonenumber");
        if (a.f(stringExtra)) {
            String e = a.e(stringExtra);
            if (a.f(e)) {
                this.y = e;
                this.E.c = this.y;
                this.x = stringExtra.substring(e.length());
                this.E.b = this.x;
            } else {
                this.y = "+86";
                this.E.c = this.y;
                this.x = stringExtra;
                this.E.b = stringExtra;
            }
        }
        if ("F".equalsIgnoreCase(getIntent().getStringExtra("alipay_user_gender"))) {
            this.E.H = "F";
        } else if ("M".equalsIgnoreCase(getIntent().getStringExtra("alipay_user_gender"))) {
            this.E.H = "M";
        }
    }

    public final void d(int i2) {
        f();
        this.O.setInAnimation(getApplicationContext(), R.anim.push_left_in);
        this.O.setOutAnimation(getApplicationContext(), R.anim.push_left_out);
        this.O.setDisplayedChild(this.O.getDisplayedChild() + i2);
        this.D += i2;
        this.F.f();
        this.F = v();
        this.F.e();
        this.S.setText((int) R.string.reg_prestep);
    }

    /* access modifiers changed from: protected */
    public final void e() {
        super.e();
        this.C.a((Object) "onInitialize~~~~~~~~~~~~~~~~");
        if (this.o == null) {
            b(new u(this, this));
        } else {
            this.N.setVisibility(0);
        }
    }

    public final void f() {
        View currentFocus = getCurrentFocus();
        if (currentFocus != null) {
            this.T.hideSoftInputFromWindow(currentFocus.getWindowToken(), 2);
        }
    }

    public final void g() {
        n.b(this, (int) R.string.errormsg_net_cmwap, (DialogInterface.OnClickListener) null).show();
    }

    public final void i() {
        c(1);
    }

    public final void j() {
        Intent intent = new Intent("android.intent.action.GET_CONTENT");
        intent.setType("image/*");
        startActivityForResult(Intent.createChooser(intent, "本地图片"), 11);
    }

    public final void k() {
        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("immomo_");
        stringBuffer.append(a.c(new Date()));
        stringBuffer.append("_" + UUID.randomUUID());
        stringBuffer.append(".jpg");
        V = stringBuffer.toString();
        this.C.a((Object) ("camera_filename=" + V));
        intent.putExtra("output", Uri.fromFile(new File(com.immomo.momo.a.i(), V)));
        startActivityForResult(intent, 12);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.b.a.a(android.graphics.Bitmap, float, boolean):android.graphics.Bitmap
     arg types: [android.graphics.Bitmap, int, int]
     candidates:
      android.support.v4.b.a.a(android.graphics.Bitmap, int, int):android.graphics.Bitmap
      android.support.v4.b.a.a(java.io.File, int, int):android.graphics.Bitmap
      android.support.v4.b.a.a(java.lang.String, java.lang.String, java.lang.String):java.lang.String
      android.support.v4.b.a.a(java.lang.Object[], java.lang.String, java.lang.String):java.lang.String
      android.support.v4.b.a.a(android.graphics.Bitmap, float, boolean):android.graphics.Bitmap */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.h.a(java.lang.String, android.graphics.Bitmap, int, boolean):java.io.File
     arg types: [java.lang.String, android.graphics.Bitmap, int, int]
     candidates:
      com.immomo.momo.util.h.a(java.lang.String, java.lang.String, int, boolean):void
      com.immomo.momo.util.h.a(java.lang.String, android.graphics.Bitmap, int, boolean):java.io.File */
    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        Uri fromFile;
        File a2;
        Bitmap a3;
        switch (i2) {
            case 11:
                if (i3 == -1 && intent != null) {
                    Uri data = intent.getData();
                    Intent intent2 = new Intent(getApplicationContext(), ImageFactoryActivity.class);
                    intent2.setData(data);
                    intent2.putExtra("aspectY", 1);
                    intent2.putExtra("aspectX", 1);
                    intent2.putExtra("minsize", 320);
                    this.U = b.a(8);
                    intent2.putExtra("outputFilePath", h.a(this.U, 2).getAbsolutePath());
                    startActivityForResult(intent2, 13);
                    return;
                }
                return;
            case 12:
                this.C.a((Object) ("camera_filename:" + V + ", resultCode:" + i3 + ", data:" + intent));
                if (i3 == -1 && !a.a((CharSequence) V) && (fromFile = Uri.fromFile(new File(com.immomo.momo.a.i(), V))) != null) {
                    Intent intent3 = new Intent(getApplicationContext(), ImageFactoryActivity.class);
                    intent3.setData(fromFile);
                    intent3.putExtra("aspectY", 1);
                    intent3.putExtra("aspectX", 1);
                    intent3.putExtra("minsize", 320);
                    this.U = b.a(8);
                    intent3.putExtra("outputFilePath", h.a(this.U, 2).getAbsolutePath());
                    startActivityForResult(intent3, 13);
                    return;
                }
                return;
            case 13:
                if (i3 == -1) {
                    this.C.a((Object) ("resultCode=" + i3));
                    if (!a.a((CharSequence) V)) {
                        File file = new File(com.immomo.momo.a.i(), V);
                        if (file.exists()) {
                            file.delete();
                        }
                        V = PoiTypeDef.All;
                    }
                    this.C.a((Object) ("avatorGUID=" + this.U));
                    if (this.U != null && (a2 = h.a(this.U, 2)) != null && a2.exists()) {
                        this.C.a((Object) ("avatorFile=" + a2));
                        Bitmap l2 = a.l(a2.getAbsolutePath());
                        if (!(l2 == null || (a3 = a.a(l2, 150.0f, true)) == null)) {
                            h.a(this.U, a3, 3, false);
                            l2.recycle();
                            this.E.ae = new String[]{this.U};
                            if (this.M != null) {
                                this.M.a(a3);
                            }
                        }
                        this.U = null;
                        return;
                    }
                    return;
                } else if (i3 == 1003) {
                    com.immomo.momo.util.ao.d(R.string.cropimage_error_size);
                    return;
                } else if (i3 == 1000) {
                    com.immomo.momo.util.ao.d(R.string.cropimage_error_other);
                    return;
                } else if (i3 == 1001) {
                    com.immomo.momo.util.ao.d(R.string.cropimage_error_store);
                    return;
                } else if (i3 == 1002) {
                    com.immomo.momo.util.ao.d(R.string.cropimage_error_filenotfound);
                    return;
                } else {
                    return;
                }
            default:
                return;
        }
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_back /*2131165286*/:
                this.F.d();
                return;
            case R.id.btn_login /*2131165287*/:
            case R.id.btn_register /*2131165288*/:
            default:
                return;
            case R.id.btn_ok /*2131165289*/:
                b(false);
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.C.a((Object) "onCreate~~~~~~~~~~~~~~~~");
        setContentView((int) R.layout.activity_register_p);
        this.E = new bf();
        this.T = g.j();
        d();
        this.N = findViewById(R.id.layout_content);
        this.R = (Button) findViewById(R.id.btn_ok);
        this.S = (Button) findViewById(R.id.btn_back);
        this.P = (HeaderLayout) findViewById(R.id.layout_header);
        this.Q = (TextView) g.o().inflate((int) R.layout.include_headerbar_righttext, (ViewGroup) null);
        this.P.a(this.Q);
        this.O = (ViewFlipper) findViewById(R.id.rg_viewflipper);
        b(true);
        this.R.setOnClickListener(this);
        this.S.setOnClickListener(this);
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 != 4) {
            return super.onKeyDown(i2, keyEvent);
        }
        this.F.d();
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.ak.a(java.lang.String, java.lang.Integer):int
     arg types: [java.lang.String, int]
     candidates:
      com.immomo.momo.util.ak.a(android.content.Context, java.lang.String):com.immomo.momo.util.ak
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Long):long
      com.immomo.momo.util.ak.a(java.lang.String, int):void
      com.immomo.momo.util.ak.a(java.lang.String, long):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.String):void
      com.immomo.momo.util.ak.a(java.lang.String, boolean):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Boolean):boolean
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Integer):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.ak.a(java.lang.String, java.lang.Boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.immomo.momo.util.ak.a(android.content.Context, java.lang.String):com.immomo.momo.util.ak
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Integer):int
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Long):long
      com.immomo.momo.util.ak.a(java.lang.String, int):void
      com.immomo.momo.util.ak.a(java.lang.String, long):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.String):void
      com.immomo.momo.util.ak.a(java.lang.String, boolean):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Boolean):boolean */
    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Bundle bundle) {
        super.onRestoreInstanceState(bundle);
        this.C.a((Object) "onRestoreInstanceState~~~~~~~~~~~~");
        ak a2 = ak.a(getApplicationContext(), "tmp_reg");
        this.E.f3011a = a2.b("password", PoiTypeDef.All);
        this.w = a2.b("alipay_user_id", PoiTypeDef.All);
        this.E.i = a2.b("name", PoiTypeDef.All);
        this.E.H = a2.b("sex", PoiTypeDef.All);
        this.E.I = a2.a("age", (Integer) 0);
        this.E.J = a2.b("birthday", PoiTypeDef.All);
        this.U = a2.b("avatorGUID", PoiTypeDef.All);
        V = a2.b("camera_filename", PoiTypeDef.All);
        this.E.c = a2.b("areaCode", PoiTypeDef.All);
        this.E.b = a2.b("phoneNumber", PoiTypeDef.All);
        this.E.e = a2.b("verificationCode", PoiTypeDef.All);
        this.E.f = a2.a("hasVerification", (Boolean) false);
        this.q = a2.b("sp_msg", PoiTypeDef.All);
        this.A = a2.a("timestamp", (Integer) 0);
        this.B = a2.a("starttime", Long.valueOf(System.currentTimeMillis()));
        this.p = a2.b("sp_num", PoiTypeDef.All);
        this.r = a2.b("accessToken", PoiTypeDef.All);
        this.z = a2.a("needSkipVerify", (Integer) 0);
        this.u = a2.a("needRecheckVerify", (Boolean) false);
        this.E.s = a2.b("inviterid", PoiTypeDef.All);
        if (!a.a((CharSequence) a2.b("photos", PoiTypeDef.All))) {
            try {
                this.E.ae = a.b(a2.b("photos", PoiTypeDef.All), ",");
            } catch (Exception e) {
            }
        }
        int a3 = a2.a("index", (Integer) 0);
        this.C.a((Object) ("pre step index=" + a3));
        for (int i2 = this.D; i2 < a3; i2++) {
            this.C.a((Object) ("renext...index=" + this.D));
            b(true);
        }
        a2.b();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.immomo.momo.util.ak.a(android.content.Context, java.lang.String):com.immomo.momo.util.ak
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Integer):int
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Long):long
      com.immomo.momo.util.ak.a(java.lang.String, int):void
      com.immomo.momo.util.ak.a(java.lang.String, long):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.String):void
      com.immomo.momo.util.ak.a(java.lang.String, boolean):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Boolean):boolean
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.Boolean]
     candidates:
      com.immomo.momo.util.ak.a(android.content.Context, java.lang.String):com.immomo.momo.util.ak
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Integer):int
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Long):long
      com.immomo.momo.util.ak.a(java.lang.String, int):void
      com.immomo.momo.util.ak.a(java.lang.String, long):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.String):void
      com.immomo.momo.util.ak.a(java.lang.String, boolean):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Boolean):boolean
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.Integer]
     candidates:
      com.immomo.momo.util.ak.a(android.content.Context, java.lang.String):com.immomo.momo.util.ak
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Integer):int
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Long):long
      com.immomo.momo.util.ak.a(java.lang.String, int):void
      com.immomo.momo.util.ak.a(java.lang.String, long):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.String):void
      com.immomo.momo.util.ak.a(java.lang.String, boolean):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Boolean):boolean
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.Long]
     candidates:
      com.immomo.momo.util.ak.a(android.content.Context, java.lang.String):com.immomo.momo.util.ak
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Integer):int
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Long):long
      com.immomo.momo.util.ak.a(java.lang.String, int):void
      com.immomo.momo.util.ak.a(java.lang.String, long):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.String):void
      com.immomo.momo.util.ak.a(java.lang.String, boolean):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Boolean):boolean
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void */
    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        ak a2 = ak.a(getApplicationContext(), "tmp_reg");
        a2.b();
        if (!a.a((CharSequence) this.E.b)) {
            a2.a("phoneNumber", (Object) this.E.b);
        }
        if (!a.a((CharSequence) this.E.c)) {
            a2.a("areaCode", (Object) this.E.c);
        }
        if (!a.a((CharSequence) this.E.e)) {
            a2.a("verificationCode", (Object) this.E.e);
        }
        if (this.E.f) {
            a2.a("hasVerification", (Object) Boolean.valueOf(this.E.f));
        }
        if (a.f(this.q)) {
            a2.a("sp_msg", (Object) this.q);
        }
        if (a.f(this.p)) {
            a2.a("sp_num", (Object) this.p);
        }
        a2.a("timestamp", (Object) Integer.valueOf(this.A));
        a2.a("starttime", (Object) Long.valueOf(this.B));
        if (!a.a((CharSequence) this.E.f3011a)) {
            a2.a("password", (Object) this.E.f3011a);
        }
        if (!a.a((CharSequence) this.w)) {
            a2.a("alipay_user_id", (Object) this.w);
        }
        if (!a.a((CharSequence) this.E.i)) {
            a2.a("name", (Object) this.E.i);
        }
        if (!a.a((CharSequence) this.E.H)) {
            a2.a("sex", (Object) this.E.H);
        }
        if (this.E.I > 0) {
            a2.a("age", (Object) Integer.valueOf(this.E.I));
        }
        if (!a.a((CharSequence) this.E.J)) {
            a2.a("birthday", (Object) this.E.J);
        }
        if (!a.a((CharSequence) this.U)) {
            a2.a("avatorGUID", (Object) this.U);
        }
        if (!a.a((CharSequence) V)) {
            a2.a("camera_filename", (Object) V);
        }
        if (a.f(this.E.s)) {
            a2.a("inviterid", (Object) this.E.s);
        }
        if (!a.a((CharSequence) this.r)) {
            a2.a("accessToken", (Object) this.r);
        }
        if (this.D >= 0) {
            a2.a("index", (Object) Integer.valueOf(this.D));
        }
        if (this.E.ae != null && this.E.ae.length > 0) {
            a2.a("photos", (Object) a.a(this.E.ae, ","));
        }
        a2.a("needRecheckVerify", this.u);
        a2.a("needSkipVerify", this.z);
        this.C.a((Object) "onSaveInstanceState~~~~~~~~~~~~~~~~");
    }

    /* access modifiers changed from: protected */
    public final MomoApplication t() {
        return (MomoApplication) getApplication();
    }

    public final void u() {
        d(1);
    }
}
