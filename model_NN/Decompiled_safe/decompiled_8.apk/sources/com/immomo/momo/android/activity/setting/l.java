package com.immomo.momo.android.activity.setting;

import android.view.View;
import com.immomo.a.a.f.a;

final class l implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ FeedBackActivity f2135a;

    l(FeedBackActivity feedBackActivity) {
        this.f2135a = feedBackActivity;
    }

    public final void onClick(View view) {
        if (a.a(this.f2135a.h.getText().toString().trim())) {
            this.f2135a.a((CharSequence) "填写意见不能为空");
        } else if (this.f2135a.h.getText().toString().trim().length() < 2) {
            this.f2135a.a((CharSequence) "字数不能少于2字");
        } else {
            this.f2135a.b(new o(this.f2135a, this.f2135a, this.f2135a.h.getText().toString(), this.f2135a.p));
        }
    }
}
