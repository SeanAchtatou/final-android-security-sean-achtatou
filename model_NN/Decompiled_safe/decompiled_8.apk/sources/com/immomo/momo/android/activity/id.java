package com.immomo.momo.android.activity;

import com.immomo.momo.android.view.cp;

final class id implements cp {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ OtherProfileV2Activity f1735a;

    id(OtherProfileV2Activity otherProfileV2Activity) {
        this.f1735a = otherProfileV2Activity;
    }

    public final void a(int i) {
        if (this.f1735a.bd == 0) {
            this.f1735a.bd = i;
            this.f1735a.bc = i;
        } else if (i != 0) {
            int round = Math.round(((float) (i - this.f1735a.bc)) * 0.5f) + this.f1735a.aP.getScrollY();
            if (round > 0 && round < this.f1735a.aY) {
                this.f1735a.aP.scrollTo(0, round);
            }
            this.f1735a.bc = i;
        } else {
            this.f1735a.bc = 0;
            this.f1735a.bd = 0;
        }
    }
}
