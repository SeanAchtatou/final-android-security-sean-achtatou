package com.immomo.momo.android.a.a;

import android.os.Bundle;
import com.immomo.momo.android.c.g;
import com.immomo.momo.service.bean.aj;
import java.io.File;

final class k implements g {

    /* renamed from: a  reason: collision with root package name */
    private aj f696a;
    private /* synthetic */ h b;

    public k(h hVar, aj ajVar) {
        this.b = hVar;
        this.f696a = ajVar;
    }

    public final /* synthetic */ void a(Object obj) {
        File file = (File) obj;
        this.f696a.setImageLoading(false);
        if (file != null) {
            com.immomo.momo.g.d().a(new Bundle(), "actions.emoteupdates");
            this.f696a.setImageLoadFailed(false);
        } else {
            this.f696a.setImageLoadFailed(true);
        }
        h.f693a.remove(this);
        this.b.e().Y();
    }
}
