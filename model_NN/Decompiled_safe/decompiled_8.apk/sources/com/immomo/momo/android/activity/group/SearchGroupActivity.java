package com.immomo.momo.android.activity.group;

import android.os.Bundle;
import android.support.v4.b.a;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ListAdapter;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.a.gy;
import com.immomo.momo.android.activity.ah;
import com.immomo.momo.android.view.EmoteEditeText;
import com.immomo.momo.android.view.LoadingButton;
import com.immomo.momo.android.view.RefreshOnOverScrollListView;
import com.immomo.momo.android.view.SearchHeaderLayout;
import com.immomo.momo.android.view.TiebaHotWordView;
import com.immomo.momo.android.view.f;
import com.immomo.momo.g;
import com.immomo.momo.util.aq;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SearchGroupActivity extends ah implements View.OnClickListener, f {
    /* access modifiers changed from: private */
    public SearchHeaderLayout h;
    /* access modifiers changed from: private */
    public TiebaHotWordView i;
    /* access modifiers changed from: private */
    public ez j;
    /* access modifiers changed from: private */
    public RefreshOnOverScrollListView k = null;
    /* access modifiers changed from: private */
    public View l = null;
    private View m = null;
    /* access modifiers changed from: private */
    public LoadingButton n = null;
    /* access modifiers changed from: private */
    public gy o = null;
    /* access modifiers changed from: private */
    public Map p = new HashMap();
    private EmoteEditeText q = null;
    /* access modifiers changed from: private */
    public View r = null;
    /* access modifiers changed from: private */
    public View s;
    /* access modifiers changed from: private */
    public fa t = null;
    /* access modifiers changed from: private */
    public String u = PoiTypeDef.All;
    /* access modifiers changed from: private */
    public fb v = null;

    static /* synthetic */ void a(SearchGroupActivity searchGroupActivity, List list) {
        if (list != null && searchGroupActivity.g != null) {
            searchGroupActivity.g.c("key_group_searchhotword", a.a(list, ","));
        }
    }

    static /* synthetic */ List o(SearchGroupActivity searchGroupActivity) {
        if (searchGroupActivity.g == null || a.a((CharSequence) searchGroupActivity.g.b("key_group_searchhotword", PoiTypeDef.All))) {
            return null;
        }
        return Arrays.asList(a.b((String) searchGroupActivity.g.b("key_group_searchhotword", PoiTypeDef.All), ","));
    }

    /* access modifiers changed from: private */
    public void u() {
        if (a.a((CharSequence) this.q.getText().toString().trim())) {
            a((CharSequence) "搜索内容不为空");
        } else {
            b(new fb(this, this, this.q.getText().toString().trim()));
        }
    }

    /* access modifiers changed from: private */
    public void v() {
        if (this.v != null && !this.v.isCancelled()) {
            this.v.cancel(true);
            this.v = null;
        }
        if (this.t != null && !this.t.isCancelled()) {
            this.t.cancel(true);
            this.t = null;
        }
    }

    /* access modifiers changed from: private */
    public void w() {
        View currentFocus = getCurrentFocus();
        if (currentFocus != null) {
            ((InputMethodManager) getSystemService("input_method")).hideSoftInputFromWindow(currentFocus.getWindowToken(), 2);
        }
    }

    public final void a(int i2, View view) {
        this.h.getSerachEditeText().setText((CharSequence) this.i.a(i2));
        this.h.getSerachEditeText().setSelection(((String) this.i.a(i2)).length());
        u();
    }

    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        super.a(bundle);
        setContentView((int) R.layout.activity_search_group);
        this.i = (TiebaHotWordView) findViewById(R.id.layout_hot);
        this.i.setItemViewWeith(80);
        this.i.setMinPading(5);
        this.h = (SearchHeaderLayout) findViewById(R.id.layout_header);
        this.h.getTitileView().setVisibility(8);
        this.q = this.h.getSerachEditeText();
        this.q.setHint((int) R.string.searchgroup_hint);
        this.r = this.h.getClearButton();
        this.r.setVisibility(4);
        this.k = (RefreshOnOverScrollListView) findViewById(R.id.listview_search);
        this.l = findViewById(R.id.layout_empty);
        this.k.setListPaddingBottom(-3);
        RefreshOnOverScrollListView refreshOnOverScrollListView = this.k;
        gy gyVar = new gy(this, new ArrayList(), this.k);
        this.o = gyVar;
        refreshOnOverScrollListView.setAdapter((ListAdapter) gyVar);
        this.k.setVisibility(8);
        this.m = g.o().inflate((int) R.layout.common_list_loadmore, (ViewGroup) null);
        this.k.addFooterView(this.m);
        this.n = (LoadingButton) this.m.findViewById(R.id.btn_loadmore);
        this.n.setNormalIconResId(R.drawable.ic_btn_inner_clock);
        this.n.setVisibility(8);
        this.s = findViewById(R.id.layout_searchview);
        d();
        this.i.setOnMomoGridViewItemClickListener(this);
        this.r.setOnClickListener(this);
        this.n.setOnProcessListener(new ev(this));
        this.q.addTextChangedListener(new ew(this));
        EmoteEditeText emoteEditeText = this.q;
        EmoteEditeText emoteEditeText2 = this.q;
        emoteEditeText.addTextChangedListener(new aq(20));
        this.q.setOnEditorActionListener(new ex(this));
        this.k.setOnItemClickListener(new ey(this));
    }

    /* access modifiers changed from: protected */
    public final void d() {
        super.d();
        this.i.post(new eu(this));
        b(new ez(this, this));
    }

    public void finish() {
        super.finish();
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_newclear /*2131166041*/:
                this.q.setText(PoiTypeDef.All);
                this.l.setVisibility(8);
                this.n.setVisibility(8);
                this.k.setVisibility(8);
                this.o.b();
                v();
                w();
                this.s.setVisibility(8);
                this.h.c();
                this.r.setVisibility(8);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }
}
