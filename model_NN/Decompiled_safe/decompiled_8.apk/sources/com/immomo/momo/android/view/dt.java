package com.immomo.momo.android.view;

import android.support.v4.view.u;
import android.view.View;
import android.view.ViewGroup;
import java.util.List;

final class dt extends u {

    /* renamed from: a  reason: collision with root package name */
    private List f2802a;

    public dt(List list) {
        this.f2802a = list;
    }

    public final Object a(ViewGroup viewGroup, int i) {
        viewGroup.addView((View) this.f2802a.get(i), 0);
        return this.f2802a.get(i);
    }

    public final void a(ViewGroup viewGroup, int i, Object obj) {
        viewGroup.removeView((View) this.f2802a.get(i));
    }

    public final boolean a(View view, Object obj) {
        return view == obj;
    }

    public final int b() {
        return this.f2802a.size();
    }
}
