package com.immomo.momo.android.activity.setting;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.SharePageActivity;
import com.immomo.momo.android.activity.ah;
import com.immomo.momo.android.view.HeaderLayout;
import com.immomo.momo.android.view.a.v;

public class SettingShareActivity extends ah implements View.OnClickListener {
    Handler h = new w(this);
    private HeaderLayout i = null;
    /* access modifiers changed from: private */
    public v j = null;

    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        super.a(bundle);
        setContentView((int) R.layout.activity_setting_share);
        this.i = (HeaderLayout) findViewById(R.id.layout_header);
        this.i.setTitleText("分享给好友");
        findViewById(R.id.setting_layout_share_more).setOnClickListener(this);
        findViewById(R.id.setting_layout_share_renren).setOnClickListener(this);
        findViewById(R.id.setting_layout_share_tencentweibo).setOnClickListener(this);
        findViewById(R.id.setting_layout_share_sinaweibo).setOnClickListener(this);
    }

    /* access modifiers changed from: protected */
    public final void d() {
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.setting_layout_share_sinaweibo /*2131165916*/:
                Intent intent = new Intent(this, SharePageActivity.class);
                intent.putExtra("share_type", 0);
                intent.putExtra("title_str", "分享给好友");
                intent.putExtra("content_str", "分享资料卡到新浪微博");
                intent.putExtra("show_checkbox", "关注@陌陌科技");
                startActivity(intent);
                return;
            case R.id.sina_icon /*2131165917*/:
            case R.id.renren_icon /*2131165920*/:
            default:
                return;
            case R.id.setting_layout_share_tencentweibo /*2131165918*/:
                Intent intent2 = new Intent(this, SharePageActivity.class);
                intent2.putExtra("share_type", 2);
                intent2.putExtra("title_str", "分享给好友");
                intent2.putExtra("content_str", "分享资料卡到腾讯微博");
                intent2.putExtra("show_checkbox", "关注@陌陌科技");
                startActivity(intent2);
                return;
            case R.id.setting_layout_share_renren /*2131165919*/:
                Intent intent3 = new Intent(this, SharePageActivity.class);
                intent3.putExtra("share_type", 3);
                intent3.putExtra("title_str", "分享给好友");
                intent3.putExtra("content_str", "分享资料卡到人人网");
                startActivity(intent3);
                return;
            case R.id.setting_layout_share_more /*2131165921*/:
                if (this.f != null) {
                    new x(this).execute(new String[0]);
                    return;
                }
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
    }
}
