package com.immomo.momo.android.activity.tieba;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.protocol.a.v;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

final class cy extends d {

    /* renamed from: a  reason: collision with root package name */
    private boolean f2226a = false;
    private /* synthetic */ TiebaAdminActivity c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public cy(TiebaAdminActivity tiebaAdminActivity, Context context) {
        super(context);
        this.c = tiebaAdminActivity;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        ArrayList arrayList = new ArrayList();
        this.f2226a = v.a().a(arrayList, this.c.k.getCount());
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        this.c.k.b((Collection) ((List) obj));
        this.c.i.setVisibility(this.f2226a ? 0 : 8);
    }

    /* access modifiers changed from: protected */
    public final void b() {
        super.b();
        this.c.i.e();
    }
}
