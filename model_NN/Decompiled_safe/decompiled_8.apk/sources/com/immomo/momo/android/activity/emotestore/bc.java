package com.immomo.momo.android.activity.emotestore;

import android.content.Intent;
import android.view.View;
import com.immomo.momo.R;
import com.immomo.momo.android.broadcast.o;
import com.immomo.momo.android.view.dragsort.DragSortListView;

final class bc implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ av f1305a;

    bc(av avVar) {
        this.f1305a = avVar;
    }

    public final void onClick(View view) {
        boolean z = false;
        this.f1305a.Q.b(!this.f1305a.Q.e());
        if (this.f1305a.Q.e()) {
            this.f1305a.S.a((int) R.drawable.ic_topbar_confirm).a("完成");
        } else {
            this.f1305a.S.a((int) R.drawable.ic_topbar_addfeed).a("编辑");
            this.f1305a.P.i(this.f1305a.T);
            Intent intent = new Intent(o.f2358a);
            intent.putExtra("event", "sort");
            av avVar = this.f1305a;
            av.b(intent);
            this.f1305a.D().b(new bf(this.f1305a, this.f1305a.c(), null));
        }
        this.f1305a.Q.notifyDataSetChanged();
        this.f1305a.O.setDragEnabled(this.f1305a.Q.e());
        DragSortListView dragSortListView = this.f1305a.O;
        if (!this.f1305a.Q.e()) {
            z = true;
        }
        dragSortListView.setEnableOverscroll(z);
    }
}
