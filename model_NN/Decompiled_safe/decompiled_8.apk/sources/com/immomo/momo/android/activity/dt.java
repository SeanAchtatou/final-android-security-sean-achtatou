package com.immomo.momo.android.activity;

import android.content.Context;
import android.content.Intent;
import com.immomo.momo.R;
import com.immomo.momo.a.e;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.w;
import com.immomo.momo.util.ao;
import com.immomo.momo.util.h;

final class dt extends d {

    /* renamed from: a  reason: collision with root package name */
    private v f1261a;
    private /* synthetic */ EditVipProfileActivity c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public dt(EditVipProfileActivity editVipProfileActivity, Context context) {
        super(context);
        this.c = editVipProfileActivity;
    }

    /* access modifiers changed from: protected */
    public final Object a(Object... objArr) {
        this.c.n.a(this.c.f, this.c.f.h);
        w.a().a(this.c.f, this.c.I, this.c.J, this.c.i);
        this.c.f.X++;
        this.c.n.b(this.c.f);
        if (!(this.c.f.af == null || this.c.i == null)) {
            this.c.i.renameTo(h.a(this.c.f.af, 2));
        }
        this.b.a((Object) ("user.photos=" + this.c.f.ae));
        this.b.a((Object) ("user.name=" + this.c.f.i));
        Intent intent = new Intent(com.immomo.momo.android.broadcast.w.f2366a);
        intent.putExtra("momoid", this.c.f.h);
        this.c.sendBroadcast(intent);
        return null;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
        this.f1261a = new v(this.c);
        this.f1261a.a("资料提交中");
        this.f1261a.setCancelable(true);
        this.f1261a.setOnCancelListener(new du(this));
        this.c.a(this.f1261a);
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        if (exc instanceof e) {
            this.b.a((Throwable) exc);
            ao.g(R.string.errormsg_network_normal400);
            return;
        }
        super.a(exc);
    }

    /* access modifiers changed from: protected */
    public final void a(Object obj) {
        this.c.setResult(-1);
        this.c.finish();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        super.b();
        this.c.p();
    }
}
