package com.immomo.momo.android.b;

import android.location.Location;
import java.util.List;
import mm.purchasesdk.PurchaseCode;

final class e implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ a f2324a;
    private final /* synthetic */ Object b;
    private final /* synthetic */ String c;
    private final /* synthetic */ String d;
    private final /* synthetic */ List e;
    private final /* synthetic */ List f;
    private final /* synthetic */ p g;

    e(a aVar, Object obj, String str, String str2, List list, List list2, p pVar) {
        this.f2324a = aVar;
        this.b = obj;
        this.c = str;
        this.d = str2;
        this.e = list;
        this.f = list2;
        this.g = pVar;
    }

    public final void run() {
        synchronized (this.b) {
            try {
                this.b.wait(60000);
                this.f2324a.b.a(this.c);
                this.f2324a.b.a(this.d);
                this.f2324a.b.a();
                if (this.e.size() > 0 || this.f.size() > 0) {
                    this.g.a(this.f.size() > 0 ? (Location) this.f.get(this.f.size() - 1) : (Location) this.e.get(this.e.size() - 1), 1, 100, 201);
                } else {
                    this.g.a(null, 1, PurchaseCode.QUERY_OK, 201);
                }
            } catch (Exception e2) {
                this.f2324a.f2312a.a((Throwable) e2);
                this.g.a(null, 1, PurchaseCode.QUERY_OK, 201);
            }
        }
        return;
    }
}
