package com.immomo.momo.android.activity.discuss;

import android.content.DialogInterface;
import android.support.v4.b.a;
import com.immomo.momo.android.view.EmoteEditeText;

final class r implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ DiscussProfileActivity f1244a;
    private final /* synthetic */ EmoteEditeText b;

    r(DiscussProfileActivity discussProfileActivity, EmoteEditeText emoteEditeText) {
        this.f1244a = discussProfileActivity;
        this.b = emoteEditeText;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        if (a.a((CharSequence) this.b.getText().toString().trim())) {
            this.f1244a.a((CharSequence) "名称不能为空");
            return;
        }
        this.f1244a.a(new y(this.f1244a, this.f1244a, this.b.getText().toString().trim())).execute(new Object[0]);
        dialogInterface.dismiss();
    }
}
