package com.immomo.momo.android.a;

import android.view.View;

final class du implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ dn f784a;
    private final /* synthetic */ int b;

    du(dn dnVar, int i) {
        this.f784a = dnVar;
        this.b = i;
    }

    public final void onClick(View view) {
        if (this.f784a.e.getOnItemClickListener() != null) {
            this.f784a.e.getOnItemClickListener().onItemClick(this.f784a.e, view, this.b, (long) view.getId());
        }
    }
}
