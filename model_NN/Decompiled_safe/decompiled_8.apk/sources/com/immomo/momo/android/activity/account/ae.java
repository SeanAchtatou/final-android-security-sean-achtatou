package com.immomo.momo.android.activity.account;

import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.view.View;

final class ae extends ClickableSpan {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public /* synthetic */ ac f933a;

    ae(ac acVar) {
        this.f933a = acVar;
    }

    public final void onClick(View view) {
        view.post(new af(this));
    }

    public final void updateDrawState(TextPaint textPaint) {
    }
}
