package com.immomo.momo.android.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

public class b extends BroadcastReceiver {

    /* renamed from: a  reason: collision with root package name */
    private final Context f2347a;
    private d b = new c();

    public b(Context context) {
        this.f2347a = context;
    }

    public b(Context context, String str) {
        this.f2347a = context;
        a(str);
    }

    public void a(Intent intent) {
    }

    public final void a(IntentFilter intentFilter) {
        this.f2347a.registerReceiver(this, intentFilter);
    }

    public final void a(d dVar) {
        this.b = dVar;
    }

    public final void a(String... strArr) {
        IntentFilter intentFilter = new IntentFilter();
        for (String addAction : strArr) {
            intentFilter.addAction(addAction);
        }
        a(intentFilter);
    }

    public final void onReceive(Context context, Intent intent) {
        this.b.a(intent);
        a(intent);
    }
}
