package com.immomo.momo.android.a.a;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

final class e extends Handler {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ d f690a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    e(d dVar, Looper looper) {
        super(looper);
        this.f690a = dVar;
    }

    public final void handleMessage(Message message) {
        switch (message.what) {
            case 394:
                if (this.f690a.p != null) {
                    this.f690a.p.stop();
                }
                this.f690a.d();
                return;
            case 395:
                this.f690a.c();
                return;
            case 396:
                d.a(this.f690a.h, this.f690a.e());
                return;
            case 397:
            default:
                return;
            case 398:
                this.f690a.p.start();
                return;
        }
    }
}
