package com.immomo.momo.android.a;

import android.view.View;

final class ai implements View.OnLongClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ae f716a;
    private final /* synthetic */ int b;

    ai(ae aeVar, int i) {
        this.f716a = aeVar;
        this.b = i;
    }

    public final boolean onLongClick(View view) {
        return this.f716a.f.getOnItemLongClickListenerInWrapper().onItemLongClick(this.f716a.f, view, this.b, (long) view.getId());
    }
}
