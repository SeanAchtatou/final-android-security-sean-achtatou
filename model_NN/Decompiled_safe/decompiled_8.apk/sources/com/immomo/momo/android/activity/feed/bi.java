package com.immomo.momo.android.activity.feed;

import android.content.Intent;
import android.view.View;
import com.immomo.momo.android.activity.setting.CommunityBindActivity;

final class bi implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ bh f1466a;
    private final /* synthetic */ int b;

    bi(bh bhVar, int i) {
        this.f1466a = bhVar;
        this.b = i;
    }

    public final void onClick(View view) {
        boolean z = false;
        if (this.b == 6) {
            bh bhVar = this.f1466a;
            if (!this.f1466a.f) {
                z = true;
            }
            bhVar.a(z);
        } else if (this.f1466a.g) {
            bh bhVar2 = this.f1466a;
            if (!this.f1466a.f) {
                z = true;
            }
            bhVar2.a(z);
        } else {
            Intent intent = new Intent(this.f1466a.f1465a.n(), CommunityBindActivity.class);
            intent.putExtra("type", this.b);
            this.f1466a.f1465a.startActivityForResult(intent, this.f1466a.i);
        }
    }
}
