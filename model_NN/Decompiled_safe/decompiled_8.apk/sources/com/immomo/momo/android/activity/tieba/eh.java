package com.immomo.momo.android.activity.tieba;

import android.support.v4.b.a;
import android.text.Editable;
import android.text.TextWatcher;
import com.amap.mapapi.poisearch.PoiTypeDef;

final class eh implements TextWatcher {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ TiebaManagerApplyActivity f2260a;

    eh(TiebaManagerApplyActivity tiebaManagerApplyActivity) {
        this.f2260a = tiebaManagerApplyActivity;
    }

    public final void afterTextChanged(Editable editable) {
        if (a.a((CharSequence) editable.toString())) {
            this.f2260a.l.setText(PoiTypeDef.All);
        } else {
            this.f2260a.l.setText(String.valueOf(editable.length()) + "/500字");
        }
    }

    public final void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public final void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }
}
