package com.immomo.momo.android.view.a;

import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.service.bean.bf;

public final class ao extends n {
    private View b;
    private ImageView c;
    private ImageView d;
    private ImageView e;
    private ImageView f;
    private ImageView g;
    private TextView h;
    private TextView i;
    private bf j;
    private View k;
    /* access modifiers changed from: private */
    public boolean l = true;
    /* access modifiers changed from: private */
    public boolean m = true;
    /* access modifiers changed from: private */
    public boolean n = true;
    /* access modifiers changed from: private */
    public boolean o = true;

    public ao(com.immomo.momo.android.activity.ao aoVar, bf bfVar) {
        super(aoVar);
        this.b = LayoutInflater.from(aoVar).inflate((int) R.layout.dialog_share, (ViewGroup) null);
        this.j = bfVar;
        setContentView(this.b);
        this.g = (ImageView) this.b.findViewById(R.id.iv_pic);
        this.h = (TextView) this.b.findViewById(R.id.text_content);
        this.c = (ImageView) this.b.findViewById(R.id.iv_sina);
        this.d = (ImageView) this.b.findViewById(R.id.iv_renren);
        this.f = (ImageView) this.b.findViewById(R.id.iv_weixin);
        this.e = (ImageView) this.b.findViewById(R.id.iv_tx);
        this.k = this.b.findViewById(R.id.layout_content);
        this.i = (TextView) this.b.findViewById(R.id.text_content_outimg);
        a();
        if (this.j.an) {
            this.c.setVisibility(0);
            i();
        } else {
            this.c.setVisibility(8);
        }
        if (this.j.at) {
            this.e.setVisibility(0);
            k();
        } else {
            this.e.setVisibility(8);
        }
        if (this.j.ar) {
            this.d.setVisibility(0);
            j();
        } else {
            this.d.setVisibility(8);
        }
        l();
        this.c.setOnClickListener(new ap(this));
        this.d.setOnClickListener(new aq(this));
        this.e.setOnClickListener(new ar(this));
        this.f.setOnClickListener(new as(this));
    }

    /* access modifiers changed from: private */
    public void i() {
        if (this.l) {
            this.c.setImageResource(R.drawable.ic_publish_weibo_selected);
        } else {
            this.c.setImageResource(R.drawable.ic_publish_weibo_normal);
        }
    }

    /* access modifiers changed from: private */
    public void j() {
        if (this.m) {
            this.d.setImageResource(R.drawable.ic_publish_renren_selected);
        } else {
            this.d.setImageResource(R.drawable.ic_publish_renren_normal);
        }
    }

    /* access modifiers changed from: private */
    public void k() {
        if (this.n) {
            this.e.setImageResource(R.drawable.ic_publish_tweibo_selected);
        } else {
            this.e.setImageResource(R.drawable.ic_publish_tweibo_normal);
        }
    }

    /* access modifiers changed from: private */
    public void l() {
        if (this.o) {
            this.f.setImageResource(R.drawable.ic_publish_weixin_selected);
        } else {
            this.f.setImageResource(R.drawable.ic_publish_weixin_normal);
        }
    }

    public final void a(DialogInterface.OnClickListener onClickListener) {
        a(0, (int) R.string.dialog_btn_confim, onClickListener);
        a(1, (int) R.string.dialog_btn_cancel, (DialogInterface.OnClickListener) null);
    }

    public final void a(Bitmap bitmap) {
        this.g.setVisibility(0);
        this.g.setImageBitmap(bitmap);
    }

    public final void a(String str, int i2) {
        if (i2 != 0) {
            this.h.setText(str);
            this.k.setBackgroundResource(i2);
            this.k.setVisibility(0);
            this.i.setVisibility(8);
            return;
        }
        this.i.setVisibility(0);
        this.i.setText(str);
        this.k.setVisibility(8);
    }

    public final void b(int i2) {
        this.i.setVisibility(0);
        this.i.setText(i2);
        this.k.setVisibility(8);
    }

    public final void d() {
        this.f.setVisibility(8);
    }

    public final boolean e() {
        return this.l && this.j.an;
    }

    public final boolean f() {
        return this.m && this.j.ar;
    }

    public final boolean g() {
        return this.n && this.j.at;
    }

    public final boolean h() {
        return this.o;
    }

    public final void show() {
        super.show();
    }
}
