package com.immomo.momo.android.activity;

import android.content.Intent;
import android.view.View;
import com.immomo.momo.R;

final class hi implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ OtherProfileActivity f1714a;

    hi(OtherProfileActivity otherProfileActivity) {
        this.f1714a = otherProfileActivity;
    }

    public final void onClick(View view) {
        int intValue = ((Integer) view.getTag(R.id.tag_item_position)).intValue();
        Intent intent = new Intent(this.f1714a, ImageBrowserActivity.class);
        intent.putExtra("array", this.f1714a.t.ae);
        intent.putExtra("imagetype", "avator");
        intent.putExtra("index", intValue);
        this.f1714a.startActivity(intent);
        this.f1714a.overridePendingTransition(R.anim.zoom_enter, R.anim.normal);
    }
}
