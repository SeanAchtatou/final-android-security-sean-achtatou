package com.immomo.momo.android.view.a;

public enum aj {
    ALL(0, "不限"),
    SINA(1, "新浪微博"),
    TENGXUN(2, "腾讯微博"),
    RENREN(4, "人人网");
    
    private final int e;
    private final String f;

    private aj(int i, String str) {
        this.e = i;
        this.f = str;
    }

    public final int a() {
        return this.e;
    }
}
