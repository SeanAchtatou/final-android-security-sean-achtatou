package com.immomo.momo.android.activity;

import android.content.Intent;
import android.support.v4.b.a;
import com.immomo.momo.android.broadcast.d;
import com.immomo.momo.service.bean.bf;

final class ba implements d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ BlacklistActivity f1038a;

    ba(BlacklistActivity blacklistActivity) {
        this.f1038a = blacklistActivity;
    }

    public final void a(Intent intent) {
        String stringExtra = intent.getStringExtra("momoid");
        if (!a.a((CharSequence) stringExtra) && this.f1038a.l != null) {
            for (int i = 0; i < this.f1038a.l.getCount(); i++) {
                bf a2 = this.f1038a.l.getItem(i);
                if (a2.h.equals(stringExtra)) {
                    this.f1038a.k.a(a2, stringExtra);
                    this.f1038a.l.notifyDataSetChanged();
                    return;
                }
            }
        }
    }
}
