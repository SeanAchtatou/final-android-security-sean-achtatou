package com.immomo.momo.android.view.a;

public enum ak {
    MINUTE_15(15),
    MINUTE_60(60),
    MINUTE_1440(1440),
    MINUTE_4320(4320);
    
    private final int e;

    private ak(int i) {
        this.e = i;
    }

    public final int a() {
        return this.e;
    }
}
