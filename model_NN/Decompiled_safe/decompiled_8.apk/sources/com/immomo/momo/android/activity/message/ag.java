package com.immomo.momo.android.activity.message;

import android.graphics.PointF;
import com.immomo.momo.android.view.da;

public final class ag implements da {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ a f1914a;

    public ag(a aVar) {
        this.f1914a = aVar;
    }

    public final void a(PointF pointF) {
        if (this.f1914a.w != null && this.f1914a.w.d()) {
            this.f1914a.w.a(pointF);
        }
    }

    public final void b(PointF pointF) {
        if (this.f1914a.w != null && this.f1914a.w.d()) {
            this.f1914a.w.e();
            if (!this.f1914a.w.a(pointF)) {
                this.f1914a.G();
            } else {
                this.f1914a.F();
            }
        }
    }
}
