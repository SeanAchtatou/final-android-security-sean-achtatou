package com.immomo.momo.android.activity.discuss;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.a.b;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ListAdapter;
import com.immomo.momo.R;
import com.immomo.momo.android.a.ax;
import com.immomo.momo.android.activity.ah;
import com.immomo.momo.android.broadcast.d;
import com.immomo.momo.android.broadcast.r;
import com.immomo.momo.android.view.HeaderLayout;
import com.immomo.momo.android.view.RefreshOnOverScrollListView;
import com.immomo.momo.android.view.a;
import com.immomo.momo.android.view.a.o;
import com.immomo.momo.android.view.bi;
import com.immomo.momo.android.view.cd;
import com.immomo.momo.android.view.cg;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.n;
import com.immomo.momo.service.h;
import java.util.List;

public class DiscussMemberListActivity extends ah implements b, View.OnClickListener {
    cd h = null;
    private HeaderLayout i;
    /* access modifiers changed from: private */
    public RefreshOnOverScrollListView j;
    private List k = null;
    /* access modifiers changed from: private */
    public h l;
    /* access modifiers changed from: private */
    public ax m;
    private r n;
    /* access modifiers changed from: private */
    public String o;
    private boolean p = false;
    /* access modifiers changed from: private */
    public int q = 1;
    /* access modifiers changed from: private */
    public bi r = null;
    /* access modifiers changed from: private */
    public Handler s = new Handler();
    private d t = new a(this);
    /* access modifiers changed from: private */
    public boolean u = false;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.service.h.a(java.lang.String, boolean):com.immomo.momo.service.bean.n
     arg types: [java.lang.String, int]
     candidates:
      com.immomo.momo.service.h.a(com.immomo.momo.service.bean.n, java.lang.String):void
      com.immomo.momo.service.h.a(java.lang.String, int):void
      com.immomo.momo.service.h.a(java.lang.String, java.lang.String):void
      com.immomo.momo.service.h.a(java.lang.String, java.lang.String[]):void
      com.immomo.momo.service.h.a(java.util.List, java.lang.String):void
      com.immomo.momo.service.h.a(java.lang.String, boolean):com.immomo.momo.service.bean.n */
    /* access modifiers changed from: private */
    public void a(List list) {
        if (list != null && list.size() > 0) {
            this.k = list;
            n a2 = this.l.a(this.o, false);
            if (a2 != null) {
                this.m = new ax(this, this.k, this.j, a2);
                this.j.setAdapter((ListAdapter) this.m);
                String a3 = g.a((int) R.string.groupmember_list_header_title);
                this.i.setTitleText(String.format(a3, Integer.valueOf(this.k.size())));
            }
        }
    }

    static /* synthetic */ void h(DiscussMemberListActivity discussMemberListActivity) {
        o oVar = new o(discussMemberListActivity, (int) R.array.order_groupmember_list);
        oVar.setTitle((int) R.string.header_order);
        oVar.a();
        oVar.a(new c(discussMemberListActivity));
        oVar.show();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.service.h.a(java.lang.String, boolean):com.immomo.momo.service.bean.n
     arg types: [java.lang.String, int]
     candidates:
      com.immomo.momo.service.h.a(com.immomo.momo.service.bean.n, java.lang.String):void
      com.immomo.momo.service.h.a(java.lang.String, int):void
      com.immomo.momo.service.h.a(java.lang.String, java.lang.String):void
      com.immomo.momo.service.h.a(java.lang.String, java.lang.String[]):void
      com.immomo.momo.service.h.a(java.util.List, java.lang.String):void
      com.immomo.momo.service.h.a(java.lang.String, boolean):com.immomo.momo.service.bean.n */
    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        super.a(bundle);
        setContentView((int) R.layout.activity_discussmemberlist);
        this.l = new h();
        this.n = new r(this);
        this.n.a(this.t);
        this.j = (RefreshOnOverScrollListView) findViewById(R.id.listview);
        this.i = (HeaderLayout) findViewById(R.id.layout_header);
        String a2 = g.a((int) R.string.groupmember_list_header_title);
        this.i.setTitleText(String.format(a2, Integer.valueOf(getIntent().getIntExtra("count", 0))));
        this.r = new bi(this).b(R.string.header_order).a((int) R.drawable.ic_topbar_showbylist);
        m().a(this.r, new b(this));
        this.r.setVisibility(8);
        if (bundle == null || !bundle.getBoolean("from_saveinstance", false)) {
            Intent intent = getIntent();
            this.o = intent.getStringExtra("did");
            this.p = Boolean.valueOf(intent.getBooleanExtra("is_owner", this.p)).booleanValue();
        } else {
            this.o = (String) bundle.get("did");
            this.p = bundle.getBoolean("is_owner");
        }
        a(this.l.c(this.o, this.q));
        this.r.setVisibility(0);
        new h(this, this).execute(new Object[0]);
        this.j.setOnItemClickListener(new d(this));
        n a3 = this.l.a(this.o, true);
        if (a3 != null && g.q().h.equals(a3.c)) {
            this.h = new cd(this, this.j);
            this.h.a((int) R.id.layout_item_container);
            this.j.setMultipleSelector(this.h);
            this.h.a(new a(this).a(), new e(this));
            this.h.a((cg) new g(this));
        }
    }

    /* access modifiers changed from: protected */
    public final void d() {
    }

    public void onBackPressed() {
        if (this.h == null || !this.h.g()) {
            super.onBackPressed();
        } else {
            this.h.e();
        }
    }

    public void onClick(View view) {
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.n != null) {
            unregisterReceiver(this.n);
            this.n = null;
        }
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 == 4 && this.u) {
            setResult(-1);
            finish();
        }
        return super.onKeyDown(i2, keyEvent);
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        bundle.putBoolean("from_saveinstance", true);
        bundle.putString("did", this.o);
        bundle.putBoolean("is_owner", this.p);
        super.onSaveInstanceState(bundle);
    }
}
