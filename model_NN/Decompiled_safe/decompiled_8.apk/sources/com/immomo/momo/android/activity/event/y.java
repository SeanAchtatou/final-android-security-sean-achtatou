package com.immomo.momo.android.activity.event;

import android.content.Intent;
import android.view.View;

final class y implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ EventFeedsActivity f1424a;

    y(EventFeedsActivity eventFeedsActivity) {
        this.f1424a = eventFeedsActivity;
    }

    public final void onClick(View view) {
        Intent intent = new Intent(this.f1424a, PublishEventFeedActivity.class);
        intent.putExtra("eventid", this.f1424a.h);
        intent.putExtra("eventname", this.f1424a.o);
        intent.putExtra("eventdes", this.f1424a.p);
        this.f1424a.startActivity(intent);
    }
}
