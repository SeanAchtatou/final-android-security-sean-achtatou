package com.immomo.momo.android.activity;

import android.view.View;
import android.widget.AdapterView;
import com.immomo.momo.R;
import com.immomo.momo.android.view.a.o;
import com.immomo.momo.service.bean.bf;

final class av implements AdapterView.OnItemLongClickListener {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public /* synthetic */ BlacklistActivity f1033a;

    av(BlacklistActivity blacklistActivity) {
        this.f1033a = blacklistActivity;
    }

    public final boolean onItemLongClick(AdapterView adapterView, View view, int i, long j) {
        bf a2 = this.f1033a.l.getItem(i);
        o oVar = new o(this.f1033a, (int) R.array.blacklist_dialog_item);
        oVar.setTitle("请选择操作");
        oVar.a(new aw(this, a2));
        oVar.show();
        return true;
    }
}
