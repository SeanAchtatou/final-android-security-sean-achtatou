package com.immomo.momo.android.activity.feed;

import android.view.View;
import android.widget.AdapterView;
import com.immomo.momo.android.view.a.n;
import com.immomo.momo.g;

final class o implements AdapterView.OnItemClickListener {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public /* synthetic */ FeedProfileActivity f1480a;
    private final /* synthetic */ String[] b;

    o(FeedProfileActivity feedProfileActivity, String[] strArr) {
        this.f1480a = feedProfileActivity;
        this.b = strArr;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.android.activity.feed.FeedProfileActivity.a(com.immomo.momo.android.activity.feed.FeedProfileActivity, java.lang.String, boolean):void
     arg types: [com.immomo.momo.android.activity.feed.FeedProfileActivity, java.lang.String, int]
     candidates:
      com.immomo.momo.android.activity.feed.FeedProfileActivity.a(android.content.Context, java.lang.String, boolean):void
      com.immomo.momo.android.activity.ao.a(android.support.v4.app.Fragment, android.content.Intent, int):void
      android.support.v4.app.g.a(java.lang.String, java.io.PrintWriter, android.view.View):void
      android.support.v4.app.g.a(android.support.v4.app.Fragment, android.content.Intent, int):void
      com.immomo.momo.android.activity.feed.FeedProfileActivity.a(com.immomo.momo.android.activity.feed.FeedProfileActivity, java.lang.String, boolean):void */
    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        if (this.f1480a.k != null) {
            if ("复制文本".equals(this.b[i])) {
                g.a((CharSequence) this.f1480a.k.b());
                this.f1480a.a((CharSequence) "已成功复制文本");
            } else if ("删除".equals(this.b[i])) {
                n.a(this.f1480a, "确定要删除该留言？", new p(this)).show();
            } else if ("举报".equals(this.b[i])) {
                FeedProfileActivity.a(this.f1480a, this.f1480a.k.h, false);
            }
        }
    }
}
