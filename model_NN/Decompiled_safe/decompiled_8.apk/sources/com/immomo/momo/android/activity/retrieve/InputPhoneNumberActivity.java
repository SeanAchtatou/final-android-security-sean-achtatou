package com.immomo.momo.android.activity.retrieve;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.b.a;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.ao;
import com.immomo.momo.android.view.a.n;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.g;
import com.immomo.momo.util.e;
import com.immomo.momo.util.k;

public class InputPhoneNumberActivity extends ao implements View.OnClickListener {
    /* access modifiers changed from: private */
    public v A = null;
    /* access modifiers changed from: private */
    public TextView h = null;
    private TextView i = null;
    private EditText j = null;
    private View k = null;
    /* access modifiers changed from: private */
    public String l = PoiTypeDef.All;
    /* access modifiers changed from: private */
    public String m = PoiTypeDef.All;
    /* access modifiers changed from: private */
    public boolean n = false;
    private boolean o = false;
    private boolean p = false;
    /* access modifiers changed from: private */
    public String[] q = null;
    /* access modifiers changed from: private */
    public m r = null;
    /* access modifiers changed from: private */
    public i s = null;
    /* access modifiers changed from: private */
    public boolean t = false;
    /* access modifiers changed from: private */
    public String u = null;
    /* access modifiers changed from: private */
    public String v = null;
    private TextView w = null;
    /* access modifiers changed from: private */
    public ImageView x = null;
    /* access modifiers changed from: private */
    public EditText y = null;
    /* access modifiers changed from: private */
    public n z = null;

    static /* synthetic */ void a(InputPhoneNumberActivity inputPhoneNumberActivity, String[] strArr) {
        Intent intent = new Intent(inputPhoneNumberActivity, ResetPswByPhoneActivity.class);
        intent.putExtra("areacode", inputPhoneNumberActivity.l);
        intent.putExtra("phonenumber", inputPhoneNumberActivity.m);
        if (strArr != null) {
            intent.putExtra("interfacenumber", strArr[0]);
            intent.putExtra("code", strArr[1]);
        }
        inputPhoneNumberActivity.startActivityForResult(intent, 33);
    }

    /* access modifiers changed from: private */
    public void g() {
        boolean z2 = false;
        String trim = this.j.getText().toString().trim();
        if (trim == null || trim.length() <= 0) {
            com.immomo.momo.util.ao.b("请输入手机号码");
            this.j.requestFocus();
        } else if (this.j.getText().length() < 3) {
            a((CharSequence) "手机号码不得小于三位数");
            this.j.requestFocus();
        } else if (!"+86".equals(this.l) || this.j.getText().length() == 11) {
            if (this.j.isEnabled()) {
                this.m = this.j.getText().toString();
            }
            z2 = true;
        } else {
            a((CharSequence) "手机号码格式错误");
            this.j.requestFocus();
        }
        if (z2) {
            boolean z3 = this.n;
            b(new k(this, this));
        }
    }

    /* access modifiers changed from: protected */
    public final void d() {
    }

    public final void f() {
        if (this.z != null) {
            this.z.dismiss();
        }
        this.t = false;
        View inflate = g.o().inflate((int) R.layout.include_register_1_scode, (ViewGroup) null);
        this.z = new n(this);
        this.z.a(0, (int) R.string.dialog_btn_confim, new f(this));
        this.z.a(1, (int) R.string.dialog_btn_cancel, (DialogInterface.OnClickListener) null);
        this.z.setTitle("输入验证码");
        this.z.a();
        this.z.setContentView(inflate);
        this.z.show();
        this.z.setOnDismissListener(new g(this));
        this.w = (TextView) inflate.findViewById(R.id.scode_tv_reload);
        this.x = (ImageView) inflate.findViewById(R.id.scode_iv_code);
        this.y = (EditText) inflate.findViewById(R.id.scode_et_inputcode);
        e.a(this.w, 0, this.w.getText().length());
        this.w.setOnClickListener(new h(this));
        b(new m(this, this));
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        finish();
        super.onActivityResult(i2, i3, intent);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_back /*2131165286*/:
                finish();
                return;
            case R.id.btn_next /*2131165417*/:
                g();
                return;
            default:
                return;
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_inputphoneno);
        this.q = a.d();
        this.h = (TextView) findViewById(R.id.input_areacode);
        this.j = (EditText) findViewById(R.id.input_phonenumber);
        this.i = (TextView) findViewById(R.id.text_notice);
        this.k = findViewById(R.id.btn_next);
        this.k.setOnClickListener(this);
        findViewById(R.id.btn_back).setOnClickListener(this);
        this.h.setOnClickListener(new d(this));
        if (getIntent().getBooleanExtra("phonedisable", false)) {
            findViewById(R.id.disable_cover).setVisibility(0);
        }
        this.m = getIntent().getStringExtra("phonenumber");
        this.l = a.f(getIntent().getStringExtra("areacode")) ? getIntent().getStringExtra("areacode") : "+86";
        this.o = getIntent().getBooleanExtra("islogin", false);
        this.p = a.f(this.m) && a.f(this.l);
        this.h.setText(this.l);
        if (!this.o) {
            this.i.setText("请输入注册时使用的手机号");
        } else if (this.p) {
            this.h.setEnabled(false);
            this.h.setFocusable(false);
            this.j.setEnabled(false);
            this.j.setFocusable(false);
            this.j.setText(a.c(this.m));
            this.i.setText("将通过当前账号的绑定手机找回密码");
        } else {
            findViewById(R.id.disable_cover).setVisibility(0);
            findViewById(R.id.layout_content).setVisibility(8);
            this.k.setEnabled(false);
        }
        this.e.a((Object) "onCreate...");
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        new k("PO", "P132");
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        new k("PI", "P132");
    }
}
