package com.immomo.momo.android.activity.account;

import android.os.Handler;
import android.os.Message;
import android.widget.Button;

final class bd extends Handler {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ bc f957a;

    bd(bc bcVar) {
        this.f957a = bcVar;
    }

    public final void handleMessage(Message message) {
        if (message.what == 2245) {
            this.f957a.f956a.b((Object) ("!!! resend count " + this.f957a.b));
            if (this.f957a.b <= 0 || this.f957a.d.f) {
                this.f957a.b = 60;
                this.f957a.g.setEnabled(true);
                this.f957a.g.setText("重发");
                return;
            }
            this.f957a.g.setEnabled(false);
            Button d = this.f957a.g;
            StringBuilder sb = new StringBuilder("重发(");
            bc bcVar = this.f957a;
            int b = bcVar.b;
            bcVar.b = b - 1;
            d.setText(sb.append(b).append(")").toString());
            if (!this.f957a.j.isDestroyed()) {
                this.f957a.k.sendEmptyMessageDelayed(2245, 1000);
            }
        }
    }
}
