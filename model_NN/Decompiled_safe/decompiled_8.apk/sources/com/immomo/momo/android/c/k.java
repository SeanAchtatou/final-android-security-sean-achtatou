package com.immomo.momo.android.c;

import android.graphics.Bitmap;
import com.immomo.momo.util.m;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public final class k {

    /* renamed from: a  reason: collision with root package name */
    private static BlockingQueue f2385a = null;
    private static m b = new m("ImageGC");
    private static Thread c = null;
    private static l d = null;

    public static void a() {
        b();
        f2385a = new LinkedBlockingQueue();
        d = new l(f2385a);
        Thread thread = new Thread(d);
        c = thread;
        thread.start();
    }

    public static void a(Bitmap bitmap) {
        if (bitmap != null) {
            if (f2385a == null) {
                a();
            }
            try {
                f2385a.put(bitmap);
            } catch (InterruptedException e) {
                b.a((Throwable) e);
            }
        }
    }

    public static void b() {
        if (d != null) {
            d.a();
            d = null;
        }
        if (c != null) {
            try {
                c.interrupt();
            } catch (Exception e) {
            }
        }
    }
}
