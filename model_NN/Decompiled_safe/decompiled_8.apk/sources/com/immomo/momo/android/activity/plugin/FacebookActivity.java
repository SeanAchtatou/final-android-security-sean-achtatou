package com.immomo.momo.android.activity.plugin;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.c.a.a;
import com.c.a.d;
import com.immomo.momo.R;
import com.immomo.momo.android.a.cm;
import com.immomo.momo.android.activity.ah;
import com.immomo.momo.android.view.HeaderLayout;
import com.immomo.momo.g;
import com.immomo.momo.plugin.e.e;
import com.immomo.momo.util.m;

public class FacebookActivity extends ah implements View.OnClickListener {
    public m h = new m("test_momo");
    /* access modifiers changed from: private */
    public cm i = null;
    private String j = null;
    /* access modifiers changed from: private */
    public e k = null;
    private View l = null;
    private HeaderLayout m = null;
    private ListView n = null;
    /* access modifiers changed from: private */
    public ImageView o = null;
    private TextView p = null;
    private TextView q = null;
    private TextView r = null;
    private TextView s = null;
    private TextView t = null;
    private TextView u = null;
    private View v = null;
    private TextView w = null;
    /* access modifiers changed from: private */
    public Handler x = new Handler();

    /* JADX WARN: Type inference failed for: r4v0, types: [com.immomo.momo.android.activity.fs, android.view.View] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    static /* synthetic */ void c(com.immomo.momo.android.activity.plugin.FacebookActivity r5) {
        /*
            r4 = 0
            r3 = 0
            com.immomo.momo.plugin.e.e r0 = r5.k
            if (r0 == 0) goto L_0x0124
            com.immomo.momo.util.m r0 = r5.h
            com.immomo.momo.plugin.e.e r1 = r5.k
            r0.a(r1)
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            com.immomo.momo.plugin.e.e r1 = r5.k
            java.lang.String r1 = r1.f2875a
            java.lang.String r1 = java.lang.String.valueOf(r1)
            r0.<init>(r1)
            r0.toString()
            r0 = 1
            r4.setEnabled(r0)
            android.widget.TextView r0 = r5.p
            com.immomo.momo.plugin.e.e r1 = r5.k
            java.lang.String r1 = r1.b
            r0.setText(r1)
            android.widget.TextView r0 = r5.t
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            com.immomo.momo.plugin.e.e r2 = r5.k
            java.lang.String r2 = r2.d
            java.lang.String r2 = java.lang.String.valueOf(r2)
            r1.<init>(r2)
            java.lang.String r2 = "-locationView"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.setText(r1)
            android.widget.TextView r0 = r5.u
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            com.immomo.momo.plugin.e.e r2 = r5.k
            java.lang.String r2 = r2.f
            java.lang.String r2 = java.lang.String.valueOf(r2)
            r1.<init>(r2)
            java.lang.String r2 = "-signView"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.setText(r1)
            com.immomo.momo.plugin.e.e r0 = r5.k
            boolean r0 = r0.n
            if (r0 == 0) goto L_0x0125
            android.view.View r0 = r5.v
            r0.setVisibility(r3)
            android.widget.TextView r0 = r5.w
            com.immomo.momo.plugin.e.e r1 = r5.k
            java.lang.String r1 = r1.o
            r0.setText(r1)
            android.widget.TextView r0 = r5.p
            r1 = 2130838288(0x7f020310, float:1.7281554E38)
            r0.setCompoundDrawablesWithIntrinsicBounds(r3, r3, r1, r3)
        L_0x007e:
            android.widget.TextView r0 = r5.r
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            com.immomo.momo.plugin.e.e r2 = r5.k
            int r2 = r2.k
            java.lang.String r2 = java.lang.String.valueOf(r2)
            r1.<init>(r2)
            java.lang.String r1 = r1.toString()
            r0.setText(r1)
            android.widget.TextView r0 = r5.q
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            com.immomo.momo.plugin.e.e r2 = r5.k
            int r2 = r2.l
            java.lang.String r2 = java.lang.String.valueOf(r2)
            r1.<init>(r2)
            java.lang.String r1 = r1.toString()
            r0.setText(r1)
            android.widget.TextView r0 = r5.s
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            com.immomo.momo.plugin.e.e r2 = r5.k
            int r2 = r2.m
            java.lang.String r2 = java.lang.String.valueOf(r2)
            r1.<init>(r2)
            java.lang.String r1 = r1.toString()
            r0.setText(r1)
            com.immomo.momo.plugin.e.e r0 = r5.k
            java.lang.String r0 = r0.h
            boolean r0 = android.support.v4.b.a.a(r0)
            if (r0 != 0) goto L_0x0124
            com.immomo.momo.plugin.e.e r0 = r5.k
            java.lang.String r0 = r0.h
            java.lang.String r1 = "/"
            int r0 = r0.indexOf(r1)
            if (r0 <= 0) goto L_0x0124
            com.immomo.momo.util.m r0 = r5.h
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r2 = "weiboUser.profileImageUrl : "
            r1.<init>(r2)
            com.immomo.momo.plugin.e.e r2 = r5.k
            java.lang.String r2 = r2.h
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.b(r1)
            com.immomo.momo.plugin.e.e r0 = r5.k
            java.lang.String r0 = r0.h
            com.immomo.momo.plugin.e.e r1 = r5.k
            java.lang.String r1 = r1.h
            java.lang.String r2 = ".php"
            int r1 = r1.indexOf(r2)
            int r1 = r1 + 4
            java.lang.String r0 = r0.substring(r1)
            java.lang.String r1 = "/"
            java.lang.String r2 = ""
            java.lang.String r0 = r0.replaceAll(r1, r2)
            com.immomo.momo.android.c.r r1 = new com.immomo.momo.android.c.r
            com.immomo.momo.android.activity.plugin.bf r2 = new com.immomo.momo.android.activity.plugin.bf
            r2.<init>(r5)
            r3 = 5
            r1.<init>(r0, r2, r3, r4)
            com.immomo.momo.plugin.e.e r0 = r5.k
            java.lang.String r0 = r0.h
            r1.a(r0)
            java.lang.Thread r0 = new java.lang.Thread
            r0.<init>(r1)
            r0.start()
        L_0x0124:
            return
        L_0x0125:
            android.view.View r0 = r5.v
            r1 = 8
            r0.setVisibility(r1)
            android.widget.TextView r0 = r5.p
            r0.setCompoundDrawablesWithIntrinsicBounds(r3, r3, r3, r3)
            goto L_0x007e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.immomo.momo.android.activity.plugin.FacebookActivity.c(com.immomo.momo.android.activity.plugin.FacebookActivity):void");
    }

    static /* synthetic */ void d(FacebookActivity facebookActivity) {
        d dVar = new d("255035774601283");
        dVar.a("255035774601283|Y5Fvf-e4HFoLFILgKY1gU-xt47I");
        Bundle bundle = new Bundle();
        bundle.putString("method", "fql.query");
        bundle.putString("query", "SELECT post_id,viewer_id,app_id,filter_key,attribution,app_data,action_links,attachment,permalink,description,type, actor_id, target_id, message,created_time FROM stream  WHERE source_id = 100001683241625 AND type in (46,247)");
        new a(dVar).a(bundle, new bh(facebookActivity, "request_type_get_message"));
    }

    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        ViewGroup viewGroup = null;
        super.a(bundle);
        setContentView((int) R.layout.activity_plus_facebook);
        this.j = getIntent().getExtras() == null ? PoiTypeDef.All : (String) getIntent().getExtras().get("momoid");
        this.m = (HeaderLayout) findViewById(R.id.layout_header);
        this.m.setTitleText("Facebook");
        this.n = (ListView) findViewById(R.id.weibo_list);
        this.l = g.o().inflate((int) R.layout.include_weibo_prifile, viewGroup);
        viewGroup.setEnabled(false);
        this.o = (ImageView) this.l.findViewById(R.id.weibo_iv_avator);
        this.o.setEnabled(false);
        this.p = (TextView) this.l.findViewById(R.id.weibo_tv_name);
        this.q = (TextView) this.l.findViewById(R.id.weibo_tv_followcount);
        this.r = (TextView) this.l.findViewById(R.id.weibo_tv_fanscount);
        this.s = (TextView) this.l.findViewById(R.id.weibo_tv_weibocount);
        this.t = (TextView) this.l.findViewById(R.id.weibo_tv_location);
        this.u = (TextView) this.l.findViewById(R.id.weibo_tv_sign);
        this.v = this.l.findViewById(R.id.weibo_layout_vip);
        this.w = (TextView) this.l.findViewById(R.id.weibo_tv_vip);
        viewGroup.setOnClickListener(this);
        this.o.setOnClickListener(this);
        d();
    }

    /* access modifiers changed from: protected */
    public final void d() {
        this.n.addHeaderView(this.l);
        this.i = new cm(this, this.n);
        this.n.setAdapter((ListAdapter) this.i);
        if (!android.support.v4.b.a.a((CharSequence) this.j)) {
            d dVar = new d("255035774601283");
            dVar.a("255035774601283|Y5Fvf-e4HFoLFILgKY1gU-xt47I");
            Bundle bundle = new Bundle();
            bundle.putString("method", "fql.query");
            bundle.putString("query", "SELECT uid,name,username,wall_count,pic_small,verified,hometown_location,about_me,notes_count,wall_count,profile_url,profile_blurb,website,friend_count,likes_count,mutual_friend_count FROM user WHERE uid=100001683241625");
            new a(dVar).a(bundle, new bh(this, "request_type_get_profile"));
        }
    }

    public void onClick(View view) {
        switch (view.getId()) {
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }
}
