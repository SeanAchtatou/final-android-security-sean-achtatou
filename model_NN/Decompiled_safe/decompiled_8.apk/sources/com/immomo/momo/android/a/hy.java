package com.immomo.momo.android.a;

import android.content.Intent;
import android.support.v4.b.a;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.ImageBrowserActivity;
import com.immomo.momo.android.activity.OtherProfileActivity;
import com.immomo.momo.android.activity.emotestore.EmotionProfileActivity;
import com.immomo.momo.android.activity.tieba.TieDetailActivity;
import com.immomo.momo.android.view.AltImageView;
import com.immomo.momo.android.view.EmoteTextView;
import com.immomo.momo.android.view.HandyListView;
import com.immomo.momo.android.view.HandyTextView;
import com.immomo.momo.android.view.a.ay;
import com.immomo.momo.android.view.a.o;
import com.immomo.momo.service.bean.aj;
import com.immomo.momo.service.bean.c.c;
import com.immomo.momo.service.bean.c.g;
import com.immomo.momo.util.j;
import java.util.ArrayList;

public final class hy extends a implements View.OnClickListener {
    private static final String[] h = {"删除"};
    private static final String[] i = {"回复", "删除", "删除并禁言"};
    private static final String[] j = {"删除"};
    private static final String[] k = {"回复", "举报"};
    private static final String[] l = {"回复", "举报", "删除"};
    /* access modifiers changed from: private */
    public String d;
    /* access modifiers changed from: private */
    public boolean e;
    /* access modifiers changed from: private */
    public TieDetailActivity f;
    private HandyListView g;

    static {
        String[] strArr = {"垃圾广告", "色情信息", "敏感内容", "不实信息"};
    }

    public hy(TieDetailActivity tieDetailActivity, HandyListView handyListView) {
        super(tieDetailActivity, new ArrayList());
        this.f = tieDetailActivity;
        this.g = handyListView;
    }

    static /* synthetic */ void a(hy hyVar, c cVar) {
        Intent intent = new Intent(hyVar.f, OtherProfileActivity.class);
        intent.putExtra("momoid", cVar.b.h);
        hyVar.f.startActivity(intent);
    }

    static /* synthetic */ void a(hy hyVar, c cVar, int i2) {
        ay ayVar = new ay(hyVar.f);
        ayVar.a(new Cif(hyVar, i2, cVar, ayVar));
        ayVar.show();
    }

    static /* synthetic */ void b(hy hyVar, c cVar) {
        if (cVar != null && cVar.o != 2) {
            String[] strArr = hyVar.d.equals(cVar.c) ? hyVar.e ? h : j : hyVar.e ? i : hyVar.d.equals(cVar.i.f) ? l : k;
            o oVar = new o(hyVar.f, strArr);
            oVar.a(new id(hyVar, strArr, cVar));
            oVar.b();
            oVar.show();
        }
    }

    public final void a(String str, boolean z) {
        this.d = str;
        this.e = z;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup, int, boolean, boolean, int):void
     arg types: [com.immomo.momo.service.bean.c.g, android.widget.ImageView, com.immomo.momo.android.view.HandyListView, int, int, int, int]
     candidates:
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, int, android.view.View, android.view.ViewGroup, boolean, int, boolean):com.immomo.momo.android.c.o
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup, int, boolean, boolean, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.b.a.a(java.util.Date, boolean):java.lang.String
     arg types: [java.util.Date, int]
     candidates:
      android.support.v4.b.a.a(android.graphics.Bitmap, float):android.graphics.Bitmap
      android.support.v4.b.a.a(android.graphics.Bitmap, java.io.File):android.graphics.Bitmap
      android.support.v4.b.a.a(int, int):java.lang.String
      android.support.v4.b.a.a(java.util.Collection, java.lang.String):java.lang.String
      android.support.v4.b.a.a(java.lang.Object[], java.lang.String):java.lang.String
      android.support.v4.b.a.a(java.lang.String, java.lang.String):org.json.JSONObject
      android.support.v4.b.a.a(float, int[]):void
      android.support.v4.b.a.a(android.app.Activity, int):void
      android.support.v4.b.a.a(android.view.View, java.lang.Runnable):void
      android.support.v4.b.a.a(java.io.File, java.io.File):void
      android.support.v4.b.a.a(java.io.File, java.util.zip.ZipInputStream):void
      android.support.v4.b.a.a(java.lang.Object, java.lang.StringBuilder):void
      android.support.v4.b.a.a(android.content.Context, java.lang.String):boolean
      android.support.v4.b.a.a(com.immomo.a.a.d.b, com.immomo.momo.service.bean.Message):boolean
      android.support.v4.b.a.a(byte[], java.lang.String):byte[]
      android.support.v4.b.a.a(byte[], byte[]):byte[]
      android.support.v4.b.a.a(int[], boolean):byte[]
      android.support.v4.b.a.a(byte[], boolean):int[]
      android.support.v4.b.a.a(int, float):void
      android.support.v4.b.a.a(java.lang.Object, com.immomo.a.a.f):void
      android.support.v4.view.bf.a(int, float):void
      com.immomo.a.a.f.a(java.lang.Object, com.immomo.a.a.f):void
      android.support.v4.b.a.a(java.util.Date, boolean):java.lang.String */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup, int, boolean, boolean, int):void
     arg types: [com.immomo.momo.service.bean.c.c, com.immomo.momo.android.view.AltImageView, com.immomo.momo.android.view.HandyListView, int, int, int, int]
     candidates:
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, int, android.view.View, android.view.ViewGroup, boolean, int, boolean):com.immomo.momo.android.c.o
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup, int, boolean, boolean, int):void */
    public final View getView(int i2, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = LayoutInflater.from(this.f).inflate((int) R.layout.listitem_tie_detail, (ViewGroup) null);
            ih ihVar = new ih((byte) 0);
            ihVar.f877a = (ImageView) view.findViewById(R.id.iv_tie_userheader);
            ihVar.b = (EmoteTextView) view.findViewById(R.id.tv_tie_username);
            ihVar.c = (TextView) view.findViewById(R.id.tv_tie_floor);
            ihVar.d = (TextView) view.findViewById(R.id.tv_tie_time);
            ihVar.e = view.findViewById(R.id.userlist_item_layout_genderbackgroud);
            ihVar.f = (ImageView) view.findViewById(R.id.userlist_item_iv_gender);
            ihVar.g = (HandyTextView) view.findViewById(R.id.userlist_item_tv_age);
            ihVar.h = (ImageView) view.findViewById(R.id.userlist_item_pic_iv_weibo);
            ihVar.i = view.findViewById(R.id.layout_tie_content);
            ihVar.j = (EmoteTextView) view.findViewById(R.id.tv_tie_content);
            ihVar.k = (EmoteTextView) view.findViewById(R.id.tv_tie_statusmsg);
            ihVar.l = (AltImageView) view.findViewById(R.id.iv_tie_content);
            ihVar.m = view.findViewById(R.id.tie_layout_comment);
            ihVar.n = (EmoteTextView) view.findViewById(R.id.tie_tv_comment_user);
            ihVar.o = (EmoteTextView) view.findViewById(R.id.tie_tv_comment_content);
            ihVar.p = view.findViewById(R.id.layout_tie_distance);
            ihVar.q = (TextView) view.findViewById(R.id.tv_tie_distance);
            ihVar.r = view.findViewById(R.id.layout_tie_usersinfo);
            view.setTag(R.id.tag_userlist_item, ihVar);
        }
        ih ihVar2 = (ih) view.getTag(R.id.tag_userlist_item);
        c cVar = (c) this.f671a.get(i2);
        g gVar = cVar.b;
        j.a((aj) gVar, ihVar2.f877a, (ViewGroup) this.g, 3, false, true, 0);
        ihVar2.b.setText(cVar.b.h());
        if (gVar.b()) {
            ihVar2.b.setTextColor(com.immomo.momo.g.c((int) R.color.font_vip_name));
        } else {
            ihVar2.b.setTextColor(com.immomo.momo.g.c((int) R.color.text_color));
        }
        ihVar2.c.setText(cVar.h);
        ihVar2.d.setText(a.a(cVar.g, false));
        if ("F".equals(gVar.H)) {
            ihVar2.e.setBackgroundResource(R.drawable.bg_gender_famal);
            ihVar2.f.setImageResource(R.drawable.ic_user_famale);
        } else {
            ihVar2.e.setBackgroundResource(R.drawable.bg_gender_male);
            ihVar2.f.setImageResource(R.drawable.ic_user_male);
        }
        ihVar2.g.setText(new StringBuilder(String.valueOf(gVar.I)).toString());
        if (gVar.an) {
            ihVar2.h.setVisibility(0);
            ihVar2.h.setImageResource(gVar.ap ? R.drawable.ic_userinfo_weibov : R.drawable.ic_userinfo_weibo);
        } else if (gVar.at) {
            ihVar2.h.setVisibility(0);
            ihVar2.h.setImageResource(gVar.au ? R.drawable.ic_userinfo_tweibov : R.drawable.ic_userinfo_tweibo);
        } else if (gVar.ar) {
            ihVar2.h.setVisibility(0);
        } else {
            ihVar2.h.setVisibility(8);
        }
        if (cVar.o == 1) {
            ihVar2.k.setVisibility(8);
            if (a.a((CharSequence) cVar.d)) {
                ihVar2.j.setVisibility(8);
            } else {
                ihVar2.j.setVisibility(0);
                ihVar2.j.setText(cVar.d);
            }
            if (a.a((CharSequence) cVar.getLoadImageId())) {
                ihVar2.l.setVisibility(8);
            } else {
                ihVar2.l.setAlt(PoiTypeDef.All);
                ihVar2.l.setVisibility(0);
                ihVar2.l.setOnClickListener(this);
                ihVar2.l.setTag(R.id.tag_item_position, Integer.valueOf(i2));
                j.a((aj) cVar, (ImageView) ihVar2.l, (ViewGroup) this.g, 15, false, false, 0);
            }
            if (cVar.k == null) {
                ihVar2.m.setVisibility(8);
            } else {
                ihVar2.m.setVisibility(0);
                ihVar2.n.setText("回复" + cVar.k.h + " " + cVar.k.b.h() + ":");
                ihVar2.o.setText(cVar.k.d);
            }
        } else if (cVar.o == 2) {
            ihVar2.j.setVisibility(8);
            ihVar2.k.setVisibility(0);
            ihVar2.k.setText(cVar.d);
            ihVar2.l.setVisibility(8);
            ihVar2.m.setVisibility(8);
        }
        if (a.a((CharSequence) cVar.f)) {
            ihVar2.p.setVisibility(8);
        } else {
            ihVar2.p.setVisibility(0);
            ihVar2.q.setText(cVar.f);
        }
        ihVar2.f877a.setOnClickListener(new hz(this, cVar));
        ihVar2.r.setOnClickListener(new ia(this, cVar));
        ihVar2.e.setOnClickListener(new ib(this, cVar));
        ihVar2.i.setOnClickListener(new ic(this, cVar));
        return view;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public final void onClick(View view) {
        int intValue = ((Integer) view.getTag(R.id.tag_item_position)).intValue();
        switch (view.getId()) {
            case R.id.iv_tie_content /*2131166457*/:
                String str = ((c) getItem(intValue)).s;
                if (!a.a((CharSequence) ((c) getItem(intValue)).getLoadImageId())) {
                    Intent intent = new Intent(this.f, ImageBrowserActivity.class);
                    intent.putExtra("array", new String[]{((c) getItem(intValue)).getLoadImageId()});
                    intent.putExtra("imagetype", "feed");
                    intent.putExtra("autohide_header", true);
                    this.f.startActivity(intent);
                    if (this.f.getParent() != null) {
                        this.f.overridePendingTransition(R.anim.zoom_enter, R.anim.normal);
                        return;
                    } else {
                        this.f.overridePendingTransition(R.anim.zoom_enter, R.anim.normal);
                        return;
                    }
                } else if (a.f(str)) {
                    Intent intent2 = new Intent(this.f, EmotionProfileActivity.class);
                    intent2.putExtra("eid", str);
                    this.f.startActivity(intent2);
                    return;
                } else {
                    return;
                }
            default:
                return;
        }
    }
}
