package com.immomo.momo.android.a;

import com.immomo.momo.service.bean.a.a;
import com.immomo.momo.service.bean.n;

public final class hf {

    /* renamed from: a  reason: collision with root package name */
    public static int f857a = 1;
    public static int b = 2;
    public static int c = 3;
    public static int d = 4;
    public String e;
    public a f;
    public n g;
    public int h;

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        hf hfVar = (hf) obj;
        return this.e == null ? hfVar.e == null : this.e.equals(hfVar.e) && this.h == hfVar.h;
    }

    public final int hashCode() {
        return (this.e == null ? 0 : this.e.hashCode()) + 31;
    }

    public final String toString() {
        return "GroupDiscuss [id=" + this.e + ", group=" + this.f + ", discuss=" + this.g + ", type=" + this.h + "]";
    }
}
