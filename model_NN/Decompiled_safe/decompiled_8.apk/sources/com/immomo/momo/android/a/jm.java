package com.immomo.momo.android.a;

import android.content.Context;
import android.support.v4.b.a;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.view.EmoteTextView;
import com.immomo.momo.b;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.util.j;
import java.util.List;

public final class jm extends a {
    private Context d = null;
    private AbsListView e = null;

    public jm(Context context, List list, AbsListView absListView) {
        super(context, list);
        this.d = context;
        this.e = absListView;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.b.a(java.lang.String, boolean):android.graphics.Bitmap
     arg types: [java.lang.String, int]
     candidates:
      com.immomo.momo.b.a(java.lang.String, android.graphics.Bitmap):android.graphics.Bitmap
      com.immomo.momo.b.a(java.lang.String, boolean):android.graphics.Bitmap */
    public final View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            jn jnVar = new jn((byte) 0);
            view = LayoutInflater.from(this.d).inflate((int) R.layout.listitem_user, (ViewGroup) null);
            jnVar.f902a = (ImageView) view.findViewById(R.id.userlist_item_iv_face);
            jnVar.b = (TextView) view.findViewById(R.id.userlist_item_tv_name);
            jnVar.c = (TextView) view.findViewById(R.id.userlist_item_tv_age);
            jnVar.d = (TextView) view.findViewById(R.id.userlist_item_tv_distance);
            jnVar.e = (TextView) view.findViewById(R.id.userlist_tv_time);
            jnVar.f = (EmoteTextView) view.findViewById(R.id.userlist_item_tv_sign);
            jnVar.i = (ImageView) view.findViewById(R.id.userlist_item_iv_gender);
            jnVar.h = view.findViewById(R.id.userlist_item_layout_genderbackgroud);
            jnVar.j = (ImageView) view.findViewById(R.id.userlist_item_pic_iv_weibo);
            jnVar.k = (ImageView) view.findViewById(R.id.userlist_item_pic_iv_txweibo);
            jnVar.p = (ImageView) view.findViewById(R.id.userlist_item_pic_iv_group_role);
            jnVar.l = (ImageView) view.findViewById(R.id.userlist_item_pic_iv_renren);
            jnVar.m = (ImageView) view.findViewById(R.id.userlist_item_pic_iv_vip);
            jnVar.n = (ImageView) view.findViewById(R.id.userlist_item_pic_iv_multipic);
            jnVar.o = (ImageView) view.findViewById(R.id.userlist_item_pic_iv_industry);
            jnVar.g = (ImageView) view.findViewById(R.id.userlist_item_pic_sign);
            jnVar.g.setVisibility(8);
            jnVar.q = view.findViewById(R.id.userlist_tv_timedriver);
            view.setTag(R.id.tag_userlist_item, jnVar);
        }
        bf bfVar = (bf) getItem(i);
        jn jnVar2 = (jn) view.getTag(R.id.tag_userlist_item);
        jnVar2.d.setText(bfVar.Z);
        if (bfVar.d() < 0.0f) {
            jnVar2.e.setVisibility(8);
            jnVar2.q.setVisibility(8);
        } else {
            jnVar2.e.setVisibility(0);
            jnVar2.q.setVisibility(0);
            jnVar2.e.setText(bfVar.aa);
        }
        jnVar2.c.setText(new StringBuilder(String.valueOf(bfVar.I)).toString());
        jnVar2.b.setText(bfVar.h());
        if (bfVar.b()) {
            jnVar2.b.setTextColor(g.c((int) R.color.font_vip_name));
        } else {
            jnVar2.b.setTextColor(g.c((int) R.color.text_color));
        }
        if (bfVar.aF != null) {
            jnVar2.f.setText("访问时间: " + a.a(bfVar.aF));
        } else {
            jnVar2.f.setText(PoiTypeDef.All);
        }
        if ("F".equals(bfVar.H)) {
            jnVar2.h.setBackgroundResource(R.drawable.bg_gender_famal);
            jnVar2.i.setImageResource(R.drawable.ic_user_famale);
        } else {
            jnVar2.h.setBackgroundResource(R.drawable.bg_gender_male);
            jnVar2.i.setImageResource(R.drawable.ic_user_male);
        }
        if (bfVar.j()) {
            jnVar2.n.setVisibility(0);
        } else {
            jnVar2.n.setVisibility(8);
        }
        if (!a.a((CharSequence) bfVar.M)) {
            jnVar2.o.setVisibility(0);
            jnVar2.o.setImageBitmap(b.a(bfVar.M, true));
        } else {
            jnVar2.o.setVisibility(8);
        }
        if (bfVar.an) {
            jnVar2.j.setVisibility(0);
            jnVar2.j.setImageResource(bfVar.ap ? R.drawable.ic_userinfo_weibov : R.drawable.ic_userinfo_weibo);
        } else {
            jnVar2.j.setVisibility(8);
        }
        if (bfVar.at) {
            jnVar2.k.setVisibility(0);
            jnVar2.k.setImageResource(bfVar.au ? R.drawable.ic_userinfo_tweibov : R.drawable.ic_userinfo_tweibo);
        } else {
            jnVar2.k.setVisibility(8);
        }
        if (bfVar.aJ == 1 || bfVar.aJ == 3) {
            jnVar2.p.setVisibility(0);
            jnVar2.p.setImageResource(bfVar.aJ == 1 ? R.drawable.ic_userinfo_groupowner : R.drawable.ic_userinfo_group);
        } else {
            jnVar2.p.setVisibility(8);
        }
        if (bfVar.ar) {
            jnVar2.l.setVisibility(0);
        } else {
            jnVar2.l.setVisibility(8);
        }
        if (bfVar.b()) {
            jnVar2.m.setVisibility(0);
        } else {
            jnVar2.m.setVisibility(8);
        }
        j.a(bfVar, jnVar2.f902a, this.e, 3);
        return view;
    }
}
