package com.immomo.momo.android.activity.event;

import android.os.Bundle;
import android.support.v4.b.a;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import com.immomo.momo.R;
import com.immomo.momo.android.a.cf;
import com.immomo.momo.android.activity.ah;
import com.immomo.momo.android.view.LoadingButton;
import com.immomo.momo.android.view.MomoRefreshListView;
import com.immomo.momo.android.view.bu;
import com.immomo.momo.g;
import com.immomo.momo.util.k;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class UsersEventListActivity extends ah implements bu {
    public static String h = "value_name";
    public static String i = "value_momoid";
    public static String j = "value_count";
    /* access modifiers changed from: private */
    public MomoRefreshListView k = null;
    /* access modifiers changed from: private */
    public cf l = null;
    /* access modifiers changed from: private */
    public LoadingButton m;
    /* access modifiers changed from: private */
    public cm n = null;
    /* access modifiers changed from: private */
    public List o = null;
    /* access modifiers changed from: private */
    public Set p = new HashSet();
    /* access modifiers changed from: private */
    public String q = null;
    private String r = null;
    private int s;

    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        super.a(bundle);
        setContentView((int) R.layout.activity_historyevents);
        if (getIntent().getExtras() != null) {
            this.q = getIntent().getExtras().getString(i);
            this.r = getIntent().getExtras().getString(h);
            this.s = getIntent().getExtras().getInt(j);
        }
        if (a.a((CharSequence) this.q)) {
            finish();
        }
        this.k = (MomoRefreshListView) findViewById(R.id.listview);
        this.k.setFastScrollEnabled(false);
        this.k.setTimeEnable(false);
        this.k.setEnableLoadMoreFoolter(true);
        this.m = this.k.getFooterViewButton();
        this.k.addHeaderView(g.o().inflate((int) R.layout.listitem_blank, (ViewGroup) null));
        this.m.setVisibility(8);
        setTitle(this.r == null ? this.q : String.valueOf(this.r) + "参加的活动(" + this.s + ")");
        this.m.setOnProcessListener(new ci(this));
        this.k.setOnPullToRefreshListener$42b903f6(this);
        this.k.setOnItemClickListener(new cj(this));
        this.k.setOnCancelListener$135502(new ck(this));
        d();
    }

    public final void b_() {
        this.k.setLoadingViewText(R.string.pull_to_refresh_refreshing_label);
        b(new cm(this, this));
    }

    /* access modifiers changed from: protected */
    public final void d() {
        MomoRefreshListView momoRefreshListView = this.k;
        cf cfVar = new cf(this, new ArrayList(), this.k);
        this.l = cfVar;
        momoRefreshListView.setAdapter((ListAdapter) cfVar);
        this.k.l();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        new k("PO", "P71113").e();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        new k("PI", "P71113").e();
        this.l.notifyDataSetChanged();
    }
}
