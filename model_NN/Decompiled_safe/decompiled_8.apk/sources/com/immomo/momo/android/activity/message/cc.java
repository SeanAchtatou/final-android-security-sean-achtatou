package com.immomo.momo.android.activity.message;

import com.immomo.momo.protocol.a.w;
import com.immomo.momo.service.bean.aw;
import com.immomo.momo.service.bean.bf;

final class cc implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private String[] f1963a;
    private /* synthetic */ HiSessionListActivity b;

    public cc(HiSessionListActivity hiSessionListActivity, String[] strArr) {
        this.b = hiSessionListActivity;
        this.f1963a = strArr;
    }

    public final void run() {
        try {
            for (bf bfVar : w.a().a(this.f1963a)) {
                if (bfVar != null) {
                    ((aw) this.b.w.remove(bfVar.h)).a(bfVar);
                    this.b.r.d(bfVar);
                }
            }
            this.b.h.sendEmptyMessage(7438);
        } catch (Exception e) {
            this.b.e.a((Throwable) e);
        }
    }
}
