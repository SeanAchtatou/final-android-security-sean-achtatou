package com.immomo.momo.android.b;

import android.location.Location;
import java.util.List;

final class t extends p {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ q f2338a;
    private final /* synthetic */ List c;
    private final /* synthetic */ String d;
    private final /* synthetic */ String e;
    private final /* synthetic */ Object f;

    t(q qVar, List list, String str, String str2, Object obj) {
        this.f2338a = qVar;
        this.c = list;
        this.d = str;
        this.e = str2;
        this.f = obj;
    }

    public final void a(Location location, int i, int i2, int i3) {
        this.c.add(location);
        if (this.c.size() > 0) {
            this.f2338a.f2335a.a(this.d);
            this.f2338a.f2335a.a(this.e);
            synchronized (this.f) {
                this.f.notify();
            }
        }
    }
}
