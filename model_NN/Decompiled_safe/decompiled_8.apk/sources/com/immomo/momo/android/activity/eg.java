package com.immomo.momo.android.activity;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.util.i;

final class eg extends d {

    /* renamed from: a  reason: collision with root package name */
    private v f1273a = null;
    private /* synthetic */ FilterActivity c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public eg(FilterActivity filterActivity, Context context) {
        super(context);
        this.c = filterActivity;
        this.f1273a = new v(context);
        this.f1273a.a("正在处理请稍等...");
        this.f1273a.setCancelable(true);
        this.f1273a.setOnCancelListener(new eh(this));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    private String c() {
        this.c.b.lock();
        try {
            FilterActivity filterActivity = this.c;
            filterActivity.m = filterActivity.m + 90;
            if (this.c.m >= 360) {
                this.c.m = 0;
            }
            this.b.a((Object) ("rotate degree: " + this.c.m));
            Matrix matrix = new Matrix();
            matrix.setRotate(90.0f, 0.5f, 0.5f);
            this.c.e = Bitmap.createBitmap(this.c.e, 0, 0, this.c.e.getWidth(), this.c.e.getHeight(), matrix, true);
            this.c.b.unlock();
            return null;
        } catch (Exception e) {
            throw e;
        } catch (Throwable th) {
            this.c.b.unlock();
            throw th;
        }
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        return c();
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
        if (this.f1273a != null) {
            this.f1273a.show();
        }
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        this.b.a((Throwable) exc);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        super.a((Object) ((String) obj));
        i.a();
        FilterActivity.a(this.c, (eb) this.c.k.get(this.c.h));
    }

    /* access modifiers changed from: protected */
    public final void b() {
        if (this.f1273a != null) {
            this.f1273a.dismiss();
            this.f1273a = null;
        }
    }
}
