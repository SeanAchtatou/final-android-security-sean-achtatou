package com.immomo.momo.android.activity;

import android.os.Handler;
import android.widget.TextView;
import java.lang.ref.WeakReference;

final class fp implements fs {

    /* renamed from: a  reason: collision with root package name */
    private long f1498a = 0;
    private long b = 0;
    private int c = 4;
    private Handler d = null;
    /* access modifiers changed from: private */
    public WeakReference e = null;
    /* access modifiers changed from: private */
    public WeakReference f = null;

    public fp(Handler handler) {
        this.d = handler;
    }

    public final void a() {
        TextView textView = this.e != null ? (TextView) this.e.get() : null;
        TextView textView2 = this.f != null ? (TextView) this.f.get() : null;
        if (textView != null && textView2 != null) {
            if (this.c == -1) {
                textView2.setText("下载失败");
                textView.setVisibility(8);
            } else if (this.c == 4) {
                textView2.setText("0%");
                textView.setVisibility(8);
            } else if ((this.c == 1 || this.c == 2) && this.b > 0) {
                textView2.setText(String.valueOf((this.f1498a * 100) / this.b) + "%");
                textView.setVisibility(0);
                textView.setText(String.valueOf(this.f1498a / 1024) + "K/" + (this.b / 1024) + "K");
            }
        }
    }

    public final void a(int i, long j, long j2, long j3) {
        this.f1498a = j2;
        this.b = j;
        this.c = i;
        this.d.post(new fq(this));
    }
}
