package com.immomo.momo.android.activity.plugin;

import android.graphics.Bitmap;
import com.immomo.momo.R;

final class bc implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ bb f2057a;
    private final /* synthetic */ Bitmap b;

    bc(bb bbVar, Bitmap bitmap) {
        this.f2057a = bbVar;
        this.b = bitmap;
    }

    public final void run() {
        if (this.b != null) {
            this.f2057a.f2056a.h.setImageBitmap(this.b);
        } else {
            this.f2057a.f2056a.h.setImageResource(R.drawable.ic_common_def_header);
        }
    }
}
