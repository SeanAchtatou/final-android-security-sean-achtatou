package com.immomo.momo.android.activity.account;

import android.content.DialogInterface;
import android.support.v4.b.a;
import com.immomo.momo.android.view.a.n;
import com.immomo.momo.util.ao;

final class ah implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ac f936a;

    ah(ac acVar) {
        this.f936a = acVar;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        String trim = this.f936a.l.getText().toString().trim();
        if (a.a((CharSequence) trim)) {
            ao.b("验证码不可为空，请重试");
            this.f936a.l.requestFocus();
        } else if (this.f936a.h) {
            this.f936a.f.b(new ak(this.f936a, this.f936a.f, trim));
        }
        ((n) dialogInterface).c();
    }
}
