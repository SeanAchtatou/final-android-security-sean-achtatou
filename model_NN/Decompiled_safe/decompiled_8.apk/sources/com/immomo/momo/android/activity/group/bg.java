package com.immomo.momo.android.activity.group;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.protocol.a.a.g;
import com.immomo.momo.protocol.a.o;
import java.util.Collection;

final class bg extends d {

    /* renamed from: a  reason: collision with root package name */
    private boolean f1558a = false;
    private /* synthetic */ GroupFeedProfileActivity c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public bg(GroupFeedProfileActivity groupFeedProfileActivity, Context context, boolean z) {
        super(context);
        this.c = groupFeedProfileActivity;
        this.f1558a = z;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        g a2;
        if (this.f1558a) {
            a2 = o.a().f(this.c.j);
            this.c.m.d(this.c.j);
        } else {
            a2 = o.a().a(this.c.j, this.c.l.getCount(), GroupFeedProfileActivity.h(this.c));
        }
        this.c.m.a(a2.c, this.c.j);
        return a2;
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        GroupFeedProfileActivity.a(this.c, 0);
        super.a(exc);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        g gVar = (g) obj;
        if (this.f1558a) {
            this.c.l.a().clear();
        }
        this.c.l.b((Collection) gVar.c);
        GroupFeedProfileActivity.a(this.c, gVar.b);
        if (gVar.f2886a > 0) {
            this.c.u.setVisibility(0);
        } else {
            this.c.s.removeFooterView(this.c.u);
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.c.t.e();
        super.b();
    }
}
