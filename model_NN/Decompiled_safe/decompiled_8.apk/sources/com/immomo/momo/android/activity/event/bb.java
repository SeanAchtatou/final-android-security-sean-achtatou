package com.immomo.momo.android.activity.event;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.protocol.a.j;
import com.immomo.momo.service.bean.u;
import java.util.ArrayList;
import java.util.List;

final class bb extends d {

    /* renamed from: a  reason: collision with root package name */
    private List f1372a = new ArrayList();
    private /* synthetic */ HistoryEventListActivity c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public bb(HistoryEventListActivity historyEventListActivity, Context context) {
        super(context);
        this.c = historyEventListActivity;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        boolean a2 = j.a().a(this.f1372a, this.c.i.getCount());
        this.c.o.e(this.f1372a);
        return Boolean.valueOf(a2);
    }

    /* access modifiers changed from: protected */
    public final void a() {
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        Boolean bool = (Boolean) obj;
        if (this.f1372a.isEmpty() || !bool.booleanValue()) {
            this.c.h.k();
        }
        for (u uVar : this.f1372a) {
            if (!this.c.n.contains(uVar)) {
                this.c.i.b(uVar);
                this.c.n.add(uVar);
            }
        }
        this.c.i.notifyDataSetChanged();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.c.j.e();
    }
}
