package com.immomo.momo.android.activity.message;

import android.content.Context;
import com.immomo.momo.R;
import com.immomo.momo.android.c.d;
import com.immomo.momo.service.bean.Message;
import com.immomo.momo.util.ao;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

final class ay extends d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ChatActivity f1931a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ay(ChatActivity chatActivity, Context context) {
        super(context);
        this.f1931a = chatActivity;
    }

    private List c() {
        int i;
        List<Message> d = this.f1931a.ac ? this.f1931a.ai() : new ArrayList();
        if (d.isEmpty() && !this.f1931a.ac && this.f1931a.ad) {
            this.b.b((Object) "load message from cachefile");
            int i2 = 0;
            while (true) {
                i = i2 + 1;
                if (i2 >= 2) {
                    break;
                }
                try {
                    this.f1931a.ad = this.f1931a.P.a(this.f1931a.ae, this.f1931a.Q, d);
                    ChatActivity chatActivity = this.f1931a;
                    chatActivity.ae = chatActivity.ae + 1;
                    this.b.a((Object) ("load success. size=" + d.size()));
                    break;
                } catch (Exception e) {
                    this.b.a((Throwable) e);
                    i2 = i;
                }
            }
            if (i == 2) {
                ao.g(R.string.errormsg_cachemessage_parsefailed);
                ChatActivity chatActivity2 = this.f1931a;
                chatActivity2.ae = chatActivity2.ae + 1;
            } else if (d.size() > 0) {
                for (Message message : d) {
                    if (message.receive) {
                        message.remoteUser = this.f1931a.Q;
                        if (message.status != 10) {
                            message.status = 4;
                        }
                    }
                }
            }
        }
        return d;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        return c();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.android.a.a.aa.a(int, java.util.Collection):void
     arg types: [int, java.util.List]
     candidates:
      com.immomo.momo.android.a.a.a(int, java.lang.Object):void
      com.immomo.momo.android.a.a.a(java.lang.Object, int):void
      com.immomo.momo.android.a.a.a(java.util.Collection, boolean):void
      com.immomo.momo.android.a.a.aa.a(int, java.util.Collection):void */
    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        List list = (List) obj;
        this.f1931a.T.a(0, (Collection) list);
        this.f1931a.l.setSelectionFromTop(list.size() + 2, this.f1931a.l.getLoadingHeigth());
    }

    /* access modifiers changed from: protected */
    public final void b() {
        if (!this.f1931a.ac && !this.f1931a.ad) {
            this.f1931a.l.a();
        }
        this.f1931a.l.n();
    }
}
