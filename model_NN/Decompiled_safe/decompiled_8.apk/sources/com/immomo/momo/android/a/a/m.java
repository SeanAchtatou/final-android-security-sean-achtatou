package com.immomo.momo.android.a.a;

import android.support.v4.b.a;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.message.HiSessionListActivity;
import com.immomo.momo.android.view.EmoteTextView;
import com.immomo.momo.android.view.RefreshOnOverScrollListView;
import com.immomo.momo.android.view.ce;
import com.immomo.momo.b;
import com.immomo.momo.g;
import com.immomo.momo.service.ag;
import com.immomo.momo.service.bean.Message;
import com.immomo.momo.service.bean.aj;
import com.immomo.momo.service.bean.aw;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.util.j;
import java.util.List;

public final class m extends ce {
    /* access modifiers changed from: private */
    public HiSessionListActivity d = null;
    /* access modifiers changed from: private */
    public RefreshOnOverScrollListView e = null;

    public m(RefreshOnOverScrollListView refreshOnOverScrollListView, HiSessionListActivity hiSessionListActivity, List list) {
        super(refreshOnOverScrollListView.getContext(), list);
        new ag();
        this.d = hiSessionListActivity;
        this.f671a = list;
        this.e = refreshOnOverScrollListView;
    }

    private static void a(r rVar, aw awVar) {
        boolean z = false;
        String str = PoiTypeDef.All;
        for (Message message : awVar.c()) {
            switch (message.contentType) {
                case 1:
                    str = "[图片]";
                    break;
                case 2:
                    str = "[地图]";
                    break;
                case 3:
                case 5:
                default:
                    z = true;
                    break;
                case 4:
                    str = "[语音]";
                    break;
                case 6:
                    str = "[表情]";
                    break;
            }
            if (z) {
                rVar.o.setText(message.getEmoteContent());
            } else {
                rVar.o.setText(str);
            }
            if (awVar.b() == 0) {
                rVar.p.setVisibility(8);
            } else {
                rVar.p.setVisibility(0);
                rVar.p.setText(new StringBuilder().append(awVar.b()).toString());
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.b.a.a(java.util.Date, boolean):java.lang.String
     arg types: [java.util.Date, int]
     candidates:
      android.support.v4.b.a.a(android.graphics.Bitmap, float):android.graphics.Bitmap
      android.support.v4.b.a.a(android.graphics.Bitmap, java.io.File):android.graphics.Bitmap
      android.support.v4.b.a.a(int, int):java.lang.String
      android.support.v4.b.a.a(java.util.Collection, java.lang.String):java.lang.String
      android.support.v4.b.a.a(java.lang.Object[], java.lang.String):java.lang.String
      android.support.v4.b.a.a(java.lang.String, java.lang.String):org.json.JSONObject
      android.support.v4.b.a.a(float, int[]):void
      android.support.v4.b.a.a(android.app.Activity, int):void
      android.support.v4.b.a.a(android.view.View, java.lang.Runnable):void
      android.support.v4.b.a.a(java.io.File, java.io.File):void
      android.support.v4.b.a.a(java.io.File, java.util.zip.ZipInputStream):void
      android.support.v4.b.a.a(java.lang.Object, java.lang.StringBuilder):void
      android.support.v4.b.a.a(android.content.Context, java.lang.String):boolean
      android.support.v4.b.a.a(com.immomo.a.a.d.b, com.immomo.momo.service.bean.Message):boolean
      android.support.v4.b.a.a(byte[], java.lang.String):byte[]
      android.support.v4.b.a.a(byte[], byte[]):byte[]
      android.support.v4.b.a.a(int[], boolean):byte[]
      android.support.v4.b.a.a(byte[], boolean):int[]
      android.support.v4.b.a.a(int, float):void
      android.support.v4.b.a.a(java.lang.Object, com.immomo.a.a.f):void
      android.support.v4.view.bf.a(int, float):void
      com.immomo.a.a.f.a(java.lang.Object, com.immomo.a.a.f):void
      android.support.v4.b.a.a(java.util.Date, boolean):java.lang.String */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.b.a(java.lang.String, boolean):android.graphics.Bitmap
     arg types: [java.lang.String, int]
     candidates:
      com.immomo.momo.b.a(java.lang.String, android.graphics.Bitmap):android.graphics.Bitmap
      com.immomo.momo.b.a(java.lang.String, boolean):android.graphics.Bitmap */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup, int, boolean, boolean, int):void
     arg types: [com.immomo.momo.service.bean.bf, android.widget.ImageView, com.immomo.momo.android.view.RefreshOnOverScrollListView, int, int, int, int]
     candidates:
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, int, android.view.View, android.view.ViewGroup, boolean, int, boolean):com.immomo.momo.android.c.o
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup, int, boolean, boolean, int):void */
    public final View a(int i, View view) {
        if (view == null) {
            r rVar = new r((byte) 0);
            view = LayoutInflater.from(this.d).inflate((int) R.layout.listitem_hi, (ViewGroup) null);
            rVar.b = (ImageView) view.findViewById(R.id.userlist_item_iv_face);
            rVar.c = (TextView) view.findViewById(R.id.userlist_item_tv_name);
            rVar.g = (TextView) view.findViewById(R.id.userlist_item_tv_age);
            rVar.f = (ImageView) view.findViewById(R.id.userlist_item_iv_gender);
            rVar.e = view.findViewById(R.id.userlist_item_layout_genderbackgroud);
            rVar.l = (ImageView) view.findViewById(R.id.userlist_item_pic_iv_industry);
            rVar.h = (ImageView) view.findViewById(R.id.userlist_item_pic_iv_weibo);
            rVar.i = (ImageView) view.findViewById(R.id.userlist_item_pic_iv_tweibo);
            rVar.j = (ImageView) view.findViewById(R.id.userlist_item_pic_iv_renren);
            rVar.k = (ImageView) view.findViewById(R.id.userlist_item_pic_iv_multipic);
            rVar.d = (TextView) view.findViewById(R.id.hi_message_timestamp);
            rVar.o = (EmoteTextView) view.findViewById(R.id.message_content);
            rVar.p = (TextView) view.findViewById(R.id.tv_msg_count);
            rVar.m = (ImageView) view.findViewById(R.id.userlist_item_pic_iv_vip);
            rVar.n = (ImageView) view.findViewById(R.id.userlist_item_pic_iv_group_role);
            rVar.f700a = view.findViewById(R.id.item_layout);
            view.setTag(R.id.tag_userlist_item, rVar);
            rVar.f700a.setOnLongClickListener(new n());
        }
        aw awVar = (aw) this.f671a.get(i);
        bf e2 = awVar.e();
        r rVar2 = (r) view.getTag(R.id.tag_userlist_item);
        bf bfVar = e2 == null ? new bf(awVar.d()) : e2;
        rVar2.f700a.setOnLongClickListener(new o(this, i));
        if (a.f(bfVar.Z)) {
            bfVar.d();
            rVar2.e.setVisibility(0);
            if ("F".equals(bfVar.H)) {
                rVar2.e.setBackgroundResource(R.drawable.bg_gender_famal);
                rVar2.f.setImageResource(R.drawable.ic_user_famale);
            } else {
                rVar2.e.setBackgroundResource(R.drawable.bg_gender_male);
                rVar2.f.setImageResource(R.drawable.ic_user_male);
            }
        }
        rVar2.c.setText(bfVar.h());
        if (bfVar.b()) {
            rVar2.c.setTextColor(g.c((int) R.color.font_vip_name));
        } else {
            rVar2.c.setTextColor(g.c((int) R.color.text_color));
        }
        rVar2.g.setText(new StringBuilder(String.valueOf(bfVar.I)).toString());
        rVar2.d.setText(a.a(awVar.a(), false));
        if (bfVar.j()) {
            rVar2.k.setVisibility(0);
        } else {
            rVar2.k.setVisibility(8);
        }
        if (!a.a((CharSequence) bfVar.M)) {
            rVar2.l.setVisibility(0);
            rVar2.l.setImageBitmap(b.a(bfVar.M, true));
        } else {
            rVar2.l.setVisibility(8);
        }
        if (bfVar.an) {
            rVar2.h.setVisibility(0);
            rVar2.h.setImageResource(bfVar.ap ? R.drawable.ic_userinfo_weibov : R.drawable.ic_userinfo_weibo);
        } else {
            rVar2.h.setVisibility(8);
        }
        if (bfVar.at) {
            rVar2.i.setVisibility(0);
            rVar2.i.setImageResource(bfVar.au ? R.drawable.ic_userinfo_tweibov : R.drawable.ic_userinfo_tweibo);
        } else {
            rVar2.i.setVisibility(8);
        }
        if (bfVar.ar) {
            rVar2.j.setVisibility(0);
        } else {
            rVar2.j.setVisibility(8);
        }
        if (bfVar.b()) {
            rVar2.m.setVisibility(0);
        } else {
            rVar2.m.setVisibility(8);
        }
        if (bfVar.aJ == 1 || bfVar.aJ == 3) {
            rVar2.n.setVisibility(0);
            rVar2.n.setImageResource(bfVar.aJ == 1 ? R.drawable.ic_userinfo_groupowner : R.drawable.ic_userinfo_group);
        } else {
            rVar2.n.setVisibility(8);
        }
        a(rVar2, awVar);
        rVar2.b.setOnClickListener(new p(this, awVar));
        rVar2.f700a.setOnClickListener(new q(this, i));
        rVar2.b.setClickable(!this.e.f());
        j.a((aj) bfVar, rVar2.b, (ViewGroup) this.e, 3, false, true, g.a(8.0f));
        return view;
    }
}
