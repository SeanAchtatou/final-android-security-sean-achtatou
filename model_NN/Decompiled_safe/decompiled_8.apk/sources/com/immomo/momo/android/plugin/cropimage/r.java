package com.immomo.momo.android.plugin.cropimage;

import android.app.Activity;
import android.os.Bundle;
import java.util.ArrayList;
import java.util.Iterator;

public class r extends Activity {

    /* renamed from: a  reason: collision with root package name */
    private final ArrayList f2596a = new ArrayList();

    public final void a(s sVar) {
        if (!this.f2596a.contains(sVar)) {
            this.f2596a.add(sVar);
        }
    }

    public final void b(s sVar) {
        this.f2596a.remove(sVar);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Iterator it = this.f2596a.iterator();
        while (it.hasNext()) {
            it.next();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        Iterator it = this.f2596a.iterator();
        while (it.hasNext()) {
            ((s) it.next()).a();
        }
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        Iterator it = this.f2596a.iterator();
        while (it.hasNext()) {
            ((s) it.next()).c();
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        Iterator it = this.f2596a.iterator();
        while (it.hasNext()) {
            ((s) it.next()).b();
        }
    }
}
