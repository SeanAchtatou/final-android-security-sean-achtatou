package com.immomo.momo.android.activity.contacts;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.b.a;
import android.view.View;
import android.widget.Button;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.ah;
import com.immomo.momo.android.activity.plugin.BindPhoneActivity;
import com.immomo.momo.android.broadcast.w;
import com.immomo.momo.g;
import com.immomo.momo.service.aq;
import com.immomo.momo.util.k;

public class OpenContactActivity extends ah implements View.OnClickListener {
    private Button h;
    /* access modifiers changed from: private */
    public cc i;

    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        super.a(bundle);
        setContentView((int) R.layout.activity_opencontact);
        m().setTitleText((int) R.string.contact_opencontact_title);
        this.h = (Button) findViewById(R.id.btn_open);
        this.h.setText((int) R.string.contact_opencontact_btn);
        this.h.setOnClickListener(this);
        findViewById(R.id.btn_cancel).setOnClickListener(this);
    }

    /* access modifiers changed from: protected */
    public final void d() {
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        switch (i2) {
            case 18:
                if (i3 == -1) {
                    String stringExtra = intent.getStringExtra("phonenumber");
                    String stringExtra2 = intent.getStringExtra("areacode");
                    if (a.f(stringExtra) && a.f(stringExtra2)) {
                        g.q().b = stringExtra;
                        g.q().c = stringExtra2;
                        g.q().g = true;
                        new aq().b(g.q());
                        Intent intent2 = new Intent(w.f2366a);
                        intent2.putExtra("momoid", this.f.h);
                        sendBroadcast(intent2);
                        new cc(this, this).execute(new Object[0]);
                        return;
                    }
                    return;
                }
                return;
            default:
                return;
        }
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_open /*2131165776*/:
                new k("C", "C95102").e();
                if (h().g) {
                    b(new cc(this, this));
                    return;
                } else {
                    startActivityForResult(new Intent(this, BindPhoneActivity.class), 18);
                    return;
                }
            case R.id.btn_cancel /*2131165777*/:
                new k("C", "C95101").e();
                finish();
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        new k("PO", "P951").e();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        new k("PI", "P951").e();
    }
}
