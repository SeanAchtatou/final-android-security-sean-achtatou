package com.immomo.momo.android.view;

import android.widget.AbsListView;
import android.widget.BaseAdapter;

final class ak implements AbsListView.OnScrollListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ EmoteInputView f2720a;

    ak(EmoteInputView emoteInputView) {
        this.f2720a = emoteInputView;
    }

    public final void onScroll(AbsListView absListView, int i, int i2, int i3) {
    }

    public final void onScrollStateChanged(AbsListView absListView, int i) {
        if (i == 0 && this.f2720a.e.getAdapter() != null && (this.f2720a.e.getAdapter() instanceof BaseAdapter)) {
            this.f2720a.t.a((Object) "notifyDataSetChanged----------------------------");
            ((BaseAdapter) this.f2720a.e.getAdapter()).notifyDataSetChanged();
        }
    }
}
