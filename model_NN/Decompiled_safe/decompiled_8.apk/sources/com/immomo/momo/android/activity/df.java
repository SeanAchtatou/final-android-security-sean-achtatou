package com.immomo.momo.android.activity;

import android.content.Context;
import android.content.Intent;
import com.immomo.momo.R;
import com.immomo.momo.a.e;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.w;
import com.immomo.momo.service.bean.au;
import com.immomo.momo.util.ao;
import com.immomo.momo.util.h;

final class df extends d {

    /* renamed from: a  reason: collision with root package name */
    private v f1220a;
    private /* synthetic */ EditUserProfileActivity c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public df(EditUserProfileActivity editUserProfileActivity, Context context) {
        super(context);
        this.c = editUserProfileActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.h.a(java.lang.String, java.lang.String, int, boolean):void
     arg types: [java.lang.String, java.lang.String, int, int]
     candidates:
      com.immomo.momo.util.h.a(java.lang.String, android.graphics.Bitmap, int, boolean):java.io.File
      com.immomo.momo.util.h.a(java.lang.String, java.lang.String, int, boolean):void */
    /* access modifiers changed from: protected */
    public final Object a(Object... objArr) {
        this.c.p.a(this.c.f, this.c.f.h);
        w.a().a(this.c.f, this.c.E, this.c.F);
        this.c.f.X++;
        this.c.p.b(this.c.f);
        int size = this.c.h.size();
        for (int i = 0; i < size; i++) {
            au auVar = (au) this.c.h.get(i);
            if (!auVar.c && !auVar.d) {
                h.a(auVar.b, this.c.f.ae[i], 2, true);
            }
        }
        this.b.a((Object) ("user.photos=" + this.c.f.ae));
        this.b.a((Object) ("user.name=" + this.c.f.i));
        Intent intent = new Intent(com.immomo.momo.android.broadcast.w.f2366a);
        intent.putExtra("momoid", this.c.f.h);
        this.c.sendBroadcast(intent);
        return null;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
        this.f1220a = new v(this.c);
        this.f1220a.a("资料提交中");
        this.f1220a.setCancelable(true);
        this.f1220a.setOnCancelListener(new dg(this));
        this.f1220a.show();
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        if (exc instanceof e) {
            this.b.a((Throwable) exc);
            ao.g(R.string.errormsg_network_normal400);
            return;
        }
        super.a(exc);
    }

    /* access modifiers changed from: protected */
    public final void a(Object obj) {
        this.c.setResult(-1);
        this.c.finish();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        super.b();
        if (this.f1220a != null && !this.c.isFinishing()) {
            this.f1220a.dismiss();
            this.f1220a = null;
        }
    }
}
