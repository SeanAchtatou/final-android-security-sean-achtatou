package com.immomo.momo.android.map;

import android.content.DialogInterface;

final class ao implements DialogInterface.OnCancelListener {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ ay f2467a;

    ao(ay ayVar) {
        this.f2467a = ayVar;
    }

    public final void onCancel(DialogInterface dialogInterface) {
        if (this.f2467a != null) {
            this.f2467a.a(true);
        }
    }
}
