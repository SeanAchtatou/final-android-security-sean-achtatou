package com.immomo.momo.android.activity.maintab;

import android.os.Bundle;
import android.view.View;
import android.widget.ListAdapter;
import com.immomo.momo.R;
import com.immomo.momo.android.a.av;
import com.immomo.momo.android.b.z;
import com.immomo.momo.android.view.HandyListView;
import com.immomo.momo.g;
import com.immomo.momo.service.af;
import com.immomo.momo.service.ag;
import com.immomo.momo.service.aq;
import com.immomo.momo.service.bean.l;
import com.immomo.momo.service.bean.m;
import com.immomo.momo.service.e;
import com.immomo.momo.util.k;
import java.util.Date;
import mm.purchasesdk.PurchaseCode;

public class a extends k {
    String O = null;
    l P = null;
    e Q = null;
    av R = null;
    private HandyListView S = null;
    private View T;
    private ag U = null;

    private void P() {
        m a2 = this.P.a(2);
        m a3 = this.P.a(1);
        m a4 = this.P.a(3);
        m a5 = this.P.a(5);
        if (a2.a() > 0 || a3.a() > 0 || a4.a() > 0 || a5.a() > 0) {
            this.T.setVisibility(0);
        } else {
            this.T.setVisibility(8);
        }
    }

    /* access modifiers changed from: protected */
    public final int B() {
        return R.layout.activity_discover;
    }

    /* access modifiers changed from: protected */
    public final void C() {
        this.S = (HandyListView) c((int) R.id.listview);
        a((CharSequence) "发现");
        this.S.setListPaddingBottom(MaintabActivity.h);
    }

    public final void a(View view) {
        super.a(view);
        if (view != null) {
            this.T = view.findViewById(R.id.tabitem_prifile_iv_badge);
        }
    }

    /* access modifiers changed from: protected */
    public final boolean a(Bundle bundle, String str) {
        boolean z = false;
        if (str.equals("actions.feeds") || str.equals("actions.feedchanged")) {
            m a2 = this.P.a(2);
            int i = bundle.getInt("feedunreaded", -1);
            if (i == -1) {
                i = af.c().s();
            }
            if (!this.N.u) {
                z = true;
            }
            a2.a(i, z);
            this.R.notifyDataSetChanged();
            P();
        } else if (str.equals("actions.tieba") || str.equals("actions.tiebachanged") || str.equals("actions.tiebareport")) {
            m a3 = this.P.a(5);
            int i2 = bundle.getInt("tiebaunreaded", -1);
            if (i2 == -1) {
                i2 = af.c().t();
            }
            if (!this.N.w) {
                z = true;
            }
            a3.a(i2, z);
            this.R.notifyDataSetChanged();
            P();
        } else if (str.equals("actions.groupaction") || str.equals("actions.groupnoticechanged")) {
            m a4 = this.P.a(1);
            int i3 = bundle.getInt("gaunreaded", -1);
            if (i3 == -1) {
                i3 = this.U.g();
            }
            if (!this.N.t) {
                z = true;
            }
            a4.a(i3, z);
            this.R.notifyDataSetChanged();
            P();
        } else if (str.equals("actions.eventdynamics") || str.equals("actions.eventstatuschanged")) {
            m a5 = this.P.a(3);
            int i4 = bundle.getInt("eventtotalcount", -1);
            if (i4 == -1) {
                i4 = af.c().n() + af.c().o();
            }
            if (!this.N.v) {
                z = true;
            }
            a5.a(i4, z);
            this.R.notifyDataSetChanged();
            P();
        }
        return super.a(bundle, str);
    }

    public final void ae() {
        new k("PI", "P7").e();
    }

    public final void ag() {
        new k("PO", "P7").e();
    }

    public final void ai() {
        this.S.i();
    }

    public final void aj() {
        super.aj();
        new aq();
        this.Q = new e();
        this.U = new ag();
        e eVar = this.Q;
        this.P = e.a();
        this.R = new av(g.c(), this.P, this.S);
        this.S.setAdapter((ListAdapter) this.R);
        P();
        Date b = this.N.b("lasttime_discover");
        if (b == null || Math.abs(new Date().getTime() - b.getTime()) > 7200000) {
            a(new c(this, c()));
        }
        m a2 = this.P.a(5);
        if (((Boolean) this.N.b("newtieba", true)).booleanValue()) {
            a2.h = true;
        }
    }

    /* access modifiers changed from: protected */
    public final void g(Bundle bundle) {
        this.S.setOnItemClickListener(new b(this));
        aj();
        a((int) PurchaseCode.QUERY_FROZEN, "actions.feeds", "actions.feedchanged");
        a((int) PurchaseCode.QUERY_FROZEN, "actions.tieba", "actions.tiebachanged", "actions.tiebareport");
        a((int) PurchaseCode.QUERY_FROZEN, "actions.groupaction", "actions.groupnoticechanged");
        a((int) PurchaseCode.QUERY_FROZEN, "actions.eventdynamics", "actions.eventstatuschanged");
    }

    public final void p() {
        super.p();
        if (this.O != null) {
            z.a(this.O);
            this.O = null;
        }
    }
}
