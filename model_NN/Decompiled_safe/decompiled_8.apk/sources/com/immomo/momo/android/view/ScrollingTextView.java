package com.immomo.momo.android.view;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;

public class ScrollingTextView extends EmoteTextView implements View.OnClickListener {
    public ScrollingTextView(Context context) {
        super(context);
        a();
    }

    public ScrollingTextView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a();
    }

    public ScrollingTextView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        a();
    }

    private void a() {
        setLines(1);
        setFocusable(true);
        setFocusableInTouchMode(true);
        setHorizontallyScrolling(true);
        setEllipsize(TextUtils.TruncateAt.MARQUEE);
        setOnClickListener(this);
    }

    public boolean isFocused() {
        return true;
    }

    public void onClick(View view) {
        setEllipsize(TextUtils.TruncateAt.MARQUEE);
        invalidate();
    }
}
