package com.immomo.momo.android.activity.setting;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.immomo.a.a.f.a;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.ah;
import com.immomo.momo.android.activity.plugin.BindRenrenActivity;
import com.immomo.momo.android.activity.plugin.BindSinaActivity;
import com.immomo.momo.android.activity.plugin.BindTXActivity;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.at;
import com.immomo.momo.util.e;

public class CommunityBindActivity extends ah {
    /* access modifiers changed from: private */
    public String h = null;
    /* access modifiers changed from: private */
    public String i = null;
    /* access modifiers changed from: private */
    public String j = null;
    /* access modifiers changed from: private */
    public TextView k;
    private TextView l;
    private ImageView m;
    private Button n;
    /* access modifiers changed from: private */
    public int o;
    /* access modifiers changed from: private */
    public g p;
    /* access modifiers changed from: private */
    public boolean q = true;
    /* access modifiers changed from: private */
    public v r = null;
    /* access modifiers changed from: private */
    public i s;
    /* access modifiers changed from: private */
    public k t;
    /* access modifiers changed from: private */
    public c u;
    /* access modifiers changed from: private */
    public boolean v;
    private at w;
    private int x = 0;

    private static void a(TextView textView, String str, String str2) {
        if (str != null) {
            textView.setText(str);
            e.a(textView, str.indexOf(str2), str.indexOf(str2) + str2.length(), R.style.Style_Text_Communitybind);
        }
    }

    /* access modifiers changed from: private */
    public void a(int[] iArr) {
        if (iArr != null) {
            this.w.c("key_communtyno_sina", String.valueOf(iArr[0]));
            this.w.c("key_communtyno_renren", String.valueOf(iArr[1]));
            this.w.c("key_communtyno_tx", String.valueOf(iArr[2]));
        }
        switch (this.o) {
            case 0:
                finish();
                return;
            case 1:
                a(this.l, "已有 " + ((String) this.w.b("key_communtyno_sina", "   ")) + " 人绑定新浪微博", (String) this.w.b("key_communtyno_sina", (String) this.w.b("key_communtyno_sina", "   ")));
                return;
            case 2:
                a(this.l, "已有 " + ((String) this.w.b("key_communtyno_tx", "   ")) + " 人绑定腾讯微博", (String) this.w.b("key_communtyno_tx", (String) this.w.b("key_communtyno_tx", "   ")));
                return;
            case 3:
                a(this.l, "已有 " + ((String) this.w.b("key_communtyno_renren", "   ")) + " 人绑定人人网", (String) this.w.b("key_communtyno_renren", (String) this.w.b("key_communtyno_renren", "   ")));
                return;
            default:
                return;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    static /* synthetic */ void c(CommunityBindActivity communityBindActivity) {
        Intent intent = new Intent(communityBindActivity, BindRenrenActivity.class);
        intent.putExtra("show_toast", false);
        intent.putExtra("value_appid", communityBindActivity.j);
        intent.putExtra("value_key", communityBindActivity.h);
        intent.putExtra("value_secret", communityBindActivity.i);
        communityBindActivity.startActivityForResult(intent, 3);
    }

    static /* synthetic */ void p(CommunityBindActivity communityBindActivity) {
        g gVar = new g(communityBindActivity, communityBindActivity);
        communityBindActivity.p = gVar;
        communityBindActivity.b(gVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    static /* synthetic */ void q(CommunityBindActivity communityBindActivity) {
        Intent intent = new Intent(communityBindActivity, BindTXActivity.class);
        intent.putExtra("show_toast", false);
        communityBindActivity.startActivityForResult(intent, 2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    /* access modifiers changed from: private */
    public void u() {
        this.v = false;
        Intent intent = new Intent(this, BindSinaActivity.class);
        intent.putExtra("show_toast", false);
        intent.putExtra("value_key", this.h);
        intent.putExtra("value_secret", this.i);
        intent.putExtra("value_enforce", this.x);
        startActivityForResult(intent, 1);
    }

    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        super.a(bundle);
        setContentView((int) R.layout.activity_communitybind);
        this.k = (TextView) findViewById(R.id.communitybind_txt_info);
        this.l = (TextView) findViewById(R.id.communitybind_txt_no);
        this.m = (ImageView) findViewById(R.id.communitybind_img);
        this.n = (Button) findViewById(R.id.communitybind_btn);
        this.w = g.r();
        this.o = getIntent().getIntExtra("type", 0);
        this.x = getIntent().getIntExtra("value_enforce", 0);
        this.q = getIntent().getBooleanExtra("show_toast", true);
        switch (this.o) {
            case 0:
                finish();
                break;
            case 1:
                this.m.setImageResource(R.drawable.ic_setting_bind_intro_weibo);
                this.k.setText((int) R.string.communitybind_sina_info);
                setTitle("绑定新浪微博");
                break;
            case 2:
                this.m.setImageResource(R.drawable.ic_setting_bind_intro_tweibo);
                this.k.setText((int) R.string.communitybind_tx_info);
                setTitle("绑定腾讯微博");
                break;
            case 3:
                this.m.setImageResource(R.drawable.ic_setting_bind_intro_renren);
                this.k.setText((int) R.string.communitybind_renren_info);
                setTitle("绑定人人网");
                break;
        }
        a((int[]) null);
        b(new k(this, this));
        this.n.setOnClickListener(new a(this));
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        if (!this.v) {
            setResult(i3);
            finish();
        } else if (intent == null || a.a(intent.getStringExtra("code"))) {
            u();
            this.v = false;
        } else {
            b(new c(this, this, null, intent.getStringExtra("code"), this.x));
        }
    }
}
