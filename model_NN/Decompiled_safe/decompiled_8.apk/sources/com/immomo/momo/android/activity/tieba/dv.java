package com.immomo.momo.android.activity.tieba;

import android.content.Context;
import com.immomo.momo.android.a.ip;
import com.immomo.momo.android.c.d;
import com.immomo.momo.protocol.a.v;
import com.immomo.momo.service.ao;
import com.immomo.momo.util.u;
import java.util.List;

final class dv extends d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ TiebaCreatingActivity f2249a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public dv(TiebaCreatingActivity tiebaCreatingActivity, Context context) {
        super(context);
        this.f2249a = tiebaCreatingActivity;
        if (tiebaCreatingActivity.j != null) {
            tiebaCreatingActivity.j.cancel(true);
        }
        tiebaCreatingActivity.j = this;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        List f = v.a().f();
        ao unused = this.f2249a.k;
        u.a("tiebarecreating", f);
        return f;
    }

    /* access modifiers changed from: protected */
    public final void a() {
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        this.f2249a.i = new ip((List) obj, this.f2249a.h);
        this.f2249a.h.setAdapter(this.f2249a.i);
        this.f2249a.i.a();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.f2249a.h.i();
    }
}
