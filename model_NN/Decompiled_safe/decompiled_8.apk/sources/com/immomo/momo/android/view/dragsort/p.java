package com.immomo.momo.android.view.dragsort;

import android.graphics.Bitmap;
import android.graphics.Point;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;

public class p {

    /* renamed from: a  reason: collision with root package name */
    private Bitmap f2798a;
    private ImageView b;
    private int c = -16777216;
    private ListView d;

    public p(ListView listView) {
        this.d = listView;
    }

    public final View a(int i) {
        View childAt = this.d.getChildAt((this.d.getHeaderViewsCount() + i) - this.d.getFirstVisiblePosition());
        if (childAt == null) {
            return null;
        }
        childAt.setPressed(false);
        childAt.setDrawingCacheEnabled(true);
        this.f2798a = Bitmap.createBitmap(childAt.getDrawingCache());
        childAt.setDrawingCacheEnabled(false);
        if (this.b == null) {
            this.b = new ImageView(this.d.getContext());
        }
        this.b.setBackgroundColor(this.c);
        this.b.setPadding(0, 0, 0, 0);
        this.b.setImageBitmap(this.f2798a);
        this.b.setLayoutParams(new ViewGroup.LayoutParams(childAt.getWidth(), childAt.getHeight()));
        return this.b;
    }

    public void a(Point point) {
    }

    public final void a(View view) {
        ((ImageView) view).setImageDrawable(null);
        this.f2798a.recycle();
        this.f2798a = null;
    }

    public final void b(int i) {
        this.c = i;
    }
}
