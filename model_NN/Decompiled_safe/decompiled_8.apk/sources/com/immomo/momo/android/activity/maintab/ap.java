package com.immomo.momo.android.activity.maintab;

import android.content.Context;
import android.widget.ListAdapter;
import com.immomo.momo.android.a.gy;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.RefreshOnOverScrollListView;
import com.immomo.momo.protocol.a.n;
import com.immomo.momo.service.bean.a.a;
import java.util.ArrayList;
import java.util.List;

final class ap extends d {

    /* renamed from: a  reason: collision with root package name */
    private String f1841a;
    private List c = new ArrayList();
    private /* synthetic */ x d;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ap(x xVar, Context context, String str) {
        super(context);
        this.d = xVar;
        this.f1841a = str;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        x.a(this.d, this.f1841a);
        this.d.ao = this.f1841a;
        return Boolean.valueOf(n.a().a(this.c, this.d.M.S, this.d.M.T, this.d.M.aH, this.f1841a, 0));
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
        x.n(this.d);
        this.d.P();
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        super.a(exc);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        if (((Boolean) obj).booleanValue()) {
            this.d.ab.setVisibility(0);
        } else {
            this.d.ab.setVisibility(8);
        }
        this.d.ae.setVisibility(this.c.isEmpty() ? 0 : 8);
        if (!this.c.isEmpty()) {
            this.d.P.clear();
            for (a aVar : this.c) {
                this.d.P.put(aVar.b, aVar);
            }
            this.d.ad.setVisibility(0);
            RefreshOnOverScrollListView v = this.d.ad;
            x xVar = this.d;
            gy gyVar = new gy(this.d.c(), this.c, this.d.ad);
            xVar.S = gyVar;
            v.setAdapter((ListAdapter) gyVar);
            return;
        }
        this.d.ad.setVisibility(8);
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.d.ar();
    }
}
