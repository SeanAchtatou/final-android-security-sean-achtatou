package com.immomo.momo.android.activity.maintab;

import android.content.DialogInterface;
import java.util.List;

final class by implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ bw f1871a;
    private final /* synthetic */ List b;

    by(bw bwVar, List list) {
        this.f1871a = bwVar;
        this.b = list;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.service.ag.a(java.util.List, boolean):void
     arg types: [java.util.List, int]
     candidates:
      com.immomo.momo.service.ag.a(com.immomo.momo.service.bean.ax, boolean):void
      com.immomo.momo.service.ag.a(int, int):void
      com.immomo.momo.service.ag.a(java.lang.String, int):void
      com.immomo.momo.service.ag.a(java.lang.String, java.lang.String):void
      com.immomo.momo.service.ag.a(java.util.List, boolean):void */
    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f1871a.f1869a.X.a().removeAll(this.b);
        this.f1871a.f1869a.X.notifyDataSetChanged();
        this.f1871a.f1869a.Q.a(this.b, false);
        this.f1871a.f1869a.O.e();
        this.f1871a.f1869a.f(-1);
    }
}
