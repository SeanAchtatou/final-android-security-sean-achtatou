package com.immomo.momo.android.b;

import android.location.Location;
import com.immomo.momo.util.ak;

final class ag extends p {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ p f2317a;
    private final /* synthetic */ ak c;

    ag(p pVar, ak akVar) {
        this.f2317a = pVar;
        this.c = akVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, int]
     candidates:
      com.immomo.momo.util.ak.a(android.content.Context, java.lang.String):com.immomo.momo.util.ak
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Integer):int
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Long):long
      com.immomo.momo.util.ak.a(java.lang.String, int):void
      com.immomo.momo.util.ak.a(java.lang.String, long):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.String):void
      com.immomo.momo.util.ak.a(java.lang.String, boolean):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Boolean):boolean
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void */
    public final void a(Location location, int i, int i2, int i3) {
        if (z.a(location)) {
            this.f2317a.a(location, i, i2, i3);
            return;
        }
        this.c.a("momolocate_locater_type", (Object) 203);
        this.f2317a.a(null, i, i2, i3);
    }
}
