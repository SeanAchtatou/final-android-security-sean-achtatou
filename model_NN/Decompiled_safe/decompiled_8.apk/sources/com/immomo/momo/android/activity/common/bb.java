package com.immomo.momo.android.activity.common;

import android.content.Intent;
import com.immomo.momo.android.a.hp;
import com.immomo.momo.android.broadcast.d;
import com.immomo.momo.android.broadcast.u;
import com.immomo.momo.service.bean.a.a;

final class bb implements d {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ba f1095a;

    bb(ba baVar) {
        this.f1095a = baVar;
    }

    public final void a(Intent intent) {
        a e;
        if (u.f2364a.equals(intent.getAction())) {
            String stringExtra = intent.getStringExtra("gid");
            if (!android.support.v4.b.a.a((CharSequence) stringExtra) && (e = this.f1095a.T.e(stringExtra)) != null && !this.f1095a.Q.contains(e)) {
                this.f1095a.P.a(e);
                this.f1095a.aa.post(new bc(this));
            }
        } else if (u.b.equals(intent.getAction())) {
            if (!android.support.v4.b.a.a((CharSequence) intent.getStringExtra("gid"))) {
                this.f1095a.Q = this.f1095a.T.c();
                if (this.f1095a.P == null) {
                    this.f1095a.P();
                    return;
                }
                this.f1095a.P.a(this.f1095a.Q);
                this.f1095a.aa.post(new bd(this));
            }
        } else if (u.c.equals(intent.getAction())) {
            String stringExtra2 = intent.getStringExtra("gid");
            if (!android.support.v4.b.a.a((CharSequence) stringExtra2)) {
                int a2 = this.f1095a.P.a(stringExtra2, 0);
                if (a2 < 0 || a2 > this.f1095a.P.getCount()) {
                    this.f1095a.Q = this.f1095a.T.c();
                    this.f1095a.aa.post(new be(this));
                    return;
                }
                ((hp) this.f1095a.P.getItem(a2)).f.G = 4;
                this.f1095a.aa.post(new bf(this));
            }
        } else if (u.d.equals(intent.getAction())) {
            String stringExtra3 = intent.getStringExtra("gid");
            if (!android.support.v4.b.a.a((CharSequence) stringExtra3)) {
                int a3 = this.f1095a.P.a(stringExtra3, 0);
                if (a3 < 0 || a3 > this.f1095a.P.getCount()) {
                    this.f1095a.Q = this.f1095a.T.c();
                    this.f1095a.P.a(this.f1095a.Q);
                    return;
                }
                ((hp) this.f1095a.P.getItem(a3)).f.G = 2;
                this.f1095a.aa.post(new bg(this));
            }
        } else if (u.g.equals(intent.getAction())) {
            String str = intent.getExtras() != null ? (String) intent.getExtras().get("gid") : null;
            if (!android.support.v4.b.a.a((CharSequence) str) && this.f1095a.P != null) {
                new Thread(new bh(this, str)).start();
            }
        }
    }
}
