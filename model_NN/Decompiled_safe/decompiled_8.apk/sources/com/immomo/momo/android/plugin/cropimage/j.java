package com.immomo.momo.android.plugin.cropimage;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.net.Uri;
import android.os.AsyncTask;
import com.baidu.location.LocationClientOption;
import com.immomo.a.a.f.b;
import com.immomo.momo.R;
import com.immomo.momo.android.view.a.v;
import java.io.File;
import java.io.FileOutputStream;

final class j extends AsyncTask {

    /* renamed from: a  reason: collision with root package name */
    private Uri f2589a;
    private File b;
    private v c;
    private /* synthetic */ CropImageActivity d;

    private j(CropImageActivity cropImageActivity) {
        this.d = cropImageActivity;
        this.f2589a = null;
        this.b = null;
        this.c = null;
    }

    /* synthetic */ j(CropImageActivity cropImageActivity, byte b2) {
        this(cropImageActivity);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    /* access modifiers changed from: protected */
    public final Object doInBackground(Object... objArr) {
        Bitmap createBitmap;
        boolean z = true;
        try {
            this.d.l = this.d.c();
            com.immomo.momo.util.j.a(b.a(), this.d.l);
            if (this.d.c > 0.0f) {
                Matrix matrix = new Matrix();
                matrix.setRotate(this.d.c, 0.5f, 0.5f);
                Bitmap b2 = this.d.l;
                Bitmap createBitmap2 = Bitmap.createBitmap(this.d.l, 0, 0, this.d.l.getWidth(), this.d.l.getHeight(), matrix, true);
                b2.recycle();
                this.d.l = createBitmap2;
                com.immomo.momo.util.j.a(b.a(), this.d.l);
                System.gc();
            }
            if (this.d.l == null) {
                this.d.k.a(LocationClientOption.MIN_SCAN_SPAN, new Intent());
            } else {
                if (this.d.k.g == 0 || this.d.k.h == 0 || this.d.k.i) {
                    Rect a2 = this.d.b.a();
                    a2.set(a2.left / 1, a2.top / 1, a2.right / 1, a2.bottom / 1);
                    int width = a2.width();
                    int height = a2.height();
                    createBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
                    new Canvas(createBitmap).drawBitmap(this.d.l, a2, new Rect(0, 0, width, height), (Paint) null);
                    this.d.l.recycle();
                    System.gc();
                    if (!(this.d.k.g == 0 || this.d.k.h == 0 || !this.d.k.i)) {
                        createBitmap = CropImageActivity.a(new Matrix(), createBitmap, this.d.k.g, this.d.k.h, this.d.k.j);
                    }
                } else {
                    createBitmap = Bitmap.createBitmap(this.d.k.g, this.d.k.h, Bitmap.Config.RGB_565);
                    Canvas canvas = new Canvas(createBitmap);
                    Rect a3 = this.d.b.a();
                    Rect rect = new Rect(0, 0, this.d.k.g, this.d.k.h);
                    int width2 = (a3.width() - rect.width()) / 2;
                    int height2 = (a3.height() - rect.height()) / 2;
                    a3.inset(Math.max(0, width2), Math.max(0, height2));
                    rect.inset(Math.max(0, -width2), Math.max(0, -height2));
                    canvas.drawBitmap(this.d.l, a3, rect, (Paint) null);
                    this.d.l.recycle();
                    System.gc();
                }
                if (this.d.k.r != null) {
                    z = false;
                }
                if (z) {
                    try {
                        this.f2589a = m.a(this.d, createBitmap);
                    } catch (Exception e) {
                        this.d.m.a((Throwable) e);
                        try {
                            createBitmap.recycle();
                            System.gc();
                        } catch (Exception e2) {
                        }
                    }
                } else {
                    File file = new File(this.d.k.r);
                    this.d.m.a((Object) ("croppedImage.getWidth()=" + createBitmap.getWidth() + ",croppedImage.getHeight()=" + createBitmap.getHeight()));
                    FileOutputStream fileOutputStream = new FileOutputStream(file);
                    createBitmap.compress(Bitmap.CompressFormat.JPEG, this.d.k.k, fileOutputStream);
                    fileOutputStream.close();
                    createBitmap.recycle();
                    this.b = file;
                }
                System.gc();
            }
        } catch (Throwable th) {
            this.d.k.a(LocationClientOption.MIN_SCAN_SPAN);
            this.d.k.a();
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public final void onPostExecute(Object obj) {
        try {
            if (this.c != null && !this.d.isFinishing()) {
                this.c.dismiss();
            }
        } catch (Exception e) {
        }
        Intent intent = new Intent();
        boolean z = true;
        if (this.f2589a != null) {
            intent.setData(this.f2589a);
            this.d.k.a(-1, intent);
        } else if (this.b != null) {
            intent.putExtra("outputFilePath", this.b.getAbsolutePath());
            this.d.k.a(-1, intent);
        } else {
            this.d.k.a(1001, intent);
            z = false;
        }
        if (this.b == null || !z || !"crop_and_filter".equals(this.d.k.s)) {
            this.d.k.a();
        } else {
            this.d.k.p = Uri.fromFile(this.b);
            this.d.k.a(0);
            this.d.k.b();
        }
        super.onPostExecute(obj);
    }

    /* access modifiers changed from: protected */
    public final void onPreExecute() {
        this.d.j.d();
        this.d.j.f2579a.clear();
        this.d.l.recycle();
        this.d.l = null;
        System.gc();
        this.c = new v(this.d, (int) R.string.progress_cropping);
        this.c.show();
    }
}
