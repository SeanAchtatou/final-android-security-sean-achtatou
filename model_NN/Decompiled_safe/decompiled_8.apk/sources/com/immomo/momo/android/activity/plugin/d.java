package com.immomo.momo.android.activity.plugin;

import android.graphics.Bitmap;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.immomo.momo.R;
import com.immomo.momo.util.ao;

final class d extends WebViewClient {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public /* synthetic */ BindDoubanActivity f2083a;

    private d(BindDoubanActivity bindDoubanActivity) {
        this.f2083a = bindDoubanActivity;
    }

    /* synthetic */ d(BindDoubanActivity bindDoubanActivity, byte b) {
        this(bindDoubanActivity);
    }

    public final void doUpdateVisitedHistory(WebView webView, String str, boolean z) {
        if (str.indexOf("login") < 0) {
            webView.loadUrl(this.f2083a.s);
        }
        super.doUpdateVisitedHistory(webView, str, z);
    }

    public final void onLoadResource(WebView webView, String str) {
        super.onLoadResource(webView, str);
    }

    public final void onPageFinished(WebView webView, String str) {
        super.onPageFinished(webView, str);
        if (this.f2083a.l != null && this.f2083a.l.isShowing()) {
            this.f2083a.l.dismiss();
        }
    }

    public final void onPageStarted(WebView webView, String str, Bitmap bitmap) {
        super.onPageStarted(webView, str, bitmap);
        if (this.f2083a.l != null) {
            this.f2083a.l.a("正在加载，请稍候...");
            this.f2083a.l.show();
        }
    }

    public final void onReceivedError(WebView webView, int i, String str, String str2) {
        if (i == -2) {
            ao.e(R.string.errormsg_network_unfind);
        } else {
            ao.e(R.string.errormsg_server);
        }
        if (this.f2083a.l.isShowing()) {
            this.f2083a.l.dismiss();
        }
    }

    public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
        if (str.indexOf(this.f2083a.i) < 0) {
            return super.shouldOverrideUrlLoading(webView, str);
        }
        BindDoubanActivity bindDoubanActivity = this.f2083a;
        BindDoubanActivity bindDoubanActivity2 = this.f2083a;
        bindDoubanActivity.q = BindDoubanActivity.c(str);
        new e(this).start();
        return true;
    }
}
