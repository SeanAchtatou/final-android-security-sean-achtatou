package com.immomo.momo.android.a;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.a.a;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.service.bean.n;
import com.immomo.momo.util.j;
import com.immomo.momo.util.m;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public final class hl extends a {
    private Context d = null;
    private List e;
    private List f;
    private bf g;
    private AbsListView h = null;

    public hl(Context context, List list, List list2, bf bfVar, AbsListView absListView) {
        super(context, new ArrayList());
        new hm();
        this.d = context;
        a(list, list2);
        this.d = context;
        this.h = absListView;
        this.g = bfVar;
        new m("GroupListAdapter").a();
    }

    public final int a(String str, int i) {
        hp hpVar = new hp();
        hpVar.h = i;
        hpVar.e = str;
        return e(hpVar);
    }

    public final void a(a aVar) {
        this.e.add(0, aVar);
        hp hpVar = new hp();
        hpVar.h = 0;
        hpVar.e = aVar.b;
        hpVar.f = aVar;
        this.f671a.add(1, hpVar);
        notifyDataSetChanged();
    }

    public final void a(List list) {
        if (this.e != null) {
            this.e.clear();
        }
        a(list, this.f);
    }

    public final void a(List list, List list2) {
        this.e = list;
        this.f = list2;
        this.f671a.clear();
        hp hpVar = new hp();
        hpVar.h = hp.b;
        this.f671a.add(hpVar);
        if (list == null || list.size() <= 0) {
            hp hpVar2 = new hp();
            hpVar2.h = hp.c;
            this.f671a.add(hpVar2);
        } else {
            Iterator it = list.iterator();
            while (it.hasNext()) {
                a aVar = (a) it.next();
                hp hpVar3 = new hp();
                hpVar3.h = 0;
                hpVar3.f = aVar;
                hpVar3.e = aVar.b;
                this.f671a.add(hpVar3);
            }
        }
        hp hpVar4 = new hp();
        hpVar4.h = hp.b;
        this.f671a.add(hpVar4);
        if (list2 == null || list2.size() <= 0) {
            hp hpVar5 = new hp();
            hpVar5.h = hp.d;
            this.f671a.add(hpVar5);
        } else {
            Iterator it2 = list2.iterator();
            while (it2.hasNext()) {
                n nVar = (n) it2.next();
                hp hpVar6 = new hp();
                hpVar6.h = hp.f862a;
                hpVar6.g = nVar;
                hpVar6.e = nVar.f;
                this.f671a.add(hpVar6);
            }
        }
        notifyDataSetChanged();
    }

    public final void b(int i) {
        hp hpVar = (hp) this.f671a.get(i);
        if (hpVar.h == hp.f862a) {
            this.f.remove(new n(hpVar.e));
        } else if (hpVar.h == 0) {
            this.e.remove(new a(hpVar.e));
        }
        super.b(i);
    }

    public final void b(List list) {
        if (this.f != null) {
            this.f.clear();
        }
        a(this.e, list);
    }

    public final /* synthetic */ boolean c(Object obj) {
        hp hpVar = (hp) obj;
        if (hpVar.h == hp.f862a) {
            this.f.remove(new n(hpVar.e));
        } else if (hpVar.h == 0) {
            this.e.remove(new a(hpVar.e));
        }
        return super.c(hpVar);
    }

    public final int e() {
        if (this.f == null) {
            return 0;
        }
        return this.f.size();
    }

    public final int getItemViewType(int i) {
        return ((hp) getItem(i)).h;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        String str;
        getItem(i);
        if (getItemViewType(i) == hp.f862a) {
            n nVar = ((hp) getItem(i)).g;
            if (view == null) {
                ho hoVar = new ho((byte) 0);
                view = LayoutInflater.from(this.d).inflate((int) R.layout.listitem_sharediscuss_relation, (ViewGroup) null);
                hoVar.f861a = (ImageView) view.findViewById(R.id.userlist_item_iv_face);
                hoVar.b = (TextView) view.findViewById(R.id.userlist_item_tv_name);
                hoVar.c = view.findViewById(R.id.discusslist_item_line);
                view.setTag(R.id.tag_userlist_item, hoVar);
            }
            ho hoVar2 = (ho) view.getTag(R.id.tag_userlist_item);
            if (!android.support.v4.b.a.a((CharSequence) nVar.b)) {
                hoVar2.b.setText(nVar.b);
            } else {
                hoVar2.b.setText(nVar.f);
            }
            if (i == getCount() - 1) {
                hoVar2.c.setVisibility(8);
            } else {
                hoVar2.c.setVisibility(0);
            }
            j.a(nVar, hoVar2.f861a, this.h, 3);
            return view;
        } else if (getItemViewType(i) == hp.b) {
            View inflate = LayoutInflater.from(this.d).inflate((int) R.layout.listitem_groupdiscusstitle, (ViewGroup) null);
            TextView textView = (TextView) inflate.findViewById(16908310);
            TextView textView2 = (TextView) inflate.findViewById(16908313);
            if (i == 0) {
                textView2.setVisibility(8);
                str = "群组 (" + this.g.z + ")";
            } else {
                textView2.setVisibility(8);
                str = "多人对话 (" + this.g.A + ")";
            }
            inflate.setOnClickListener(new hn());
            textView.setText(str);
            return inflate;
        } else if (getItemViewType(i) == 0) {
            a aVar = ((hp) getItem(i)).f;
            if (view == null) {
                hq hqVar = new hq((byte) 0);
                view = LayoutInflater.from(this.d).inflate((int) R.layout.listitem_sharegroup_relation, (ViewGroup) null);
                hqVar.f863a = (ImageView) view.findViewById(R.id.userlist_item_iv_face);
                hqVar.b = (TextView) view.findViewById(R.id.userlist_item_tv_name);
                hqVar.c = view.findViewById(R.id.grouplist_item_line);
                view.setTag(R.id.tag_userlist_item, hqVar);
            }
            hq hqVar2 = (hq) view.getTag(R.id.tag_userlist_item);
            if (aVar.a()) {
                hqVar2.b.setTextColor(g.c((int) R.color.font_vip_name));
            } else {
                hqVar2.b.setTextColor(g.c((int) R.color.text_color));
            }
            if (!android.support.v4.b.a.a((CharSequence) aVar.c)) {
                hqVar2.b.setText(aVar.c);
            } else {
                hqVar2.b.setText(aVar.b);
            }
            if (((hp) getItem(i + 1)).h != ((hp) getItem(i)).h) {
                hqVar2.c.setVisibility(8);
            } else {
                hqVar2.c.setVisibility(0);
            }
            j.a(aVar, hqVar2.f863a, this.h, 3);
            return view;
        } else if (getItemViewType(i) == hp.c) {
            View inflate2 = LayoutInflater.from(this.d).inflate((int) R.layout.listitem_relation_empty, (ViewGroup) null);
            ((TextView) inflate2.findViewById(R.id.tv_tip)).setText((int) R.string.grouplist_empty_tip);
            return inflate2;
        } else if (getItemViewType(i) != hp.d) {
            return view;
        } else {
            View inflate3 = LayoutInflater.from(this.d).inflate((int) R.layout.listitem_relation_empty, (ViewGroup) null);
            ((TextView) inflate3.findViewById(R.id.tv_tip)).setText((int) R.string.discusslist_empty_tip);
            return inflate3;
        }
    }

    public final int getViewTypeCount() {
        return 5;
    }
}
