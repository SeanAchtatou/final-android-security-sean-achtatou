package com.immomo.momo.android.game;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.g;
import android.support.v4.b.a;
import android.widget.Button;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.lh;
import com.unicom.dcLoader.Utils;
import com.unicom.dcLoader.c;

@SuppressLint({"ValidFragment"})
public final class ax extends lh {
    ai O;
    String P;
    String Q;
    ao R = null;
    Handler S = null;
    private TextView T;
    private TextView U;
    private TextView V;
    private TextView W;
    private TextView X;
    private Button Y;

    public ax(ai aiVar, String str, String str2) {
        this.O = aiVar;
        this.P = str;
        this.Q = str2;
        this.R = aiVar.c;
        this.S = new Handler();
    }

    /* access modifiers changed from: private */
    public void P() {
        c cVar;
        Utils a2 = Utils.a();
        g c = c();
        String str = this.R.b;
        String str2 = this.R.l;
        String str3 = this.O.h;
        String sb = new StringBuilder(String.valueOf(this.R.e)).toString();
        String str4 = this.R.h;
        String I = com.immomo.momo.g.I();
        String str5 = this.R.c;
        if (!"single".equals(str5)) {
            if ("sub".equals(str5)) {
                cVar = c.sub;
            } else if ("unsub".equals(str5)) {
                cVar = c.unsub;
            }
            a2.a(c, str, str2, str3, sb, str4, I, cVar, new ba(this, (byte) 0));
        }
        cVar = c.single;
        a2.a(c, str, str2, str3, sb, str4, I, cVar, new ba(this, (byte) 0));
    }

    /* access modifiers changed from: private */
    public static String Q() {
        try {
            return new String(a.a(com.immomo.momo.g.c().getAssets().open("premessable.txt")));
        } catch (Exception e) {
            throw new com.immomo.momo.a.a("读取联通支付配置失败", e);
        }
    }

    static /* synthetic */ void c(ax axVar) {
        Intent intent = new Intent();
        intent.putExtra("product_id", axVar.O.i);
        intent.putExtra("trade_number", axVar.R.h);
        intent.putExtra("trade_sign", axVar.R.g);
        if (!com.immomo.a.a.f.a.a(axVar.O.j)) {
            intent.putExtra("trade_extendnumber", axVar.O.j);
        }
        axVar.c().setResult(30210, intent);
        axVar.c().finish();
    }

    static /* synthetic */ void d(ax axVar) {
        if (!axVar.R.i) {
            axVar.a(new az(axVar, axVar.c()));
        } else {
            axVar.P();
        }
    }

    /* access modifiers changed from: protected */
    public final int B() {
        return R.layout.fragment_mdkpay;
    }

    /* access modifiers changed from: protected */
    public final void C() {
        this.T = (TextView) c((int) R.id.opentrade_tv_fee);
        this.U = (TextView) c((int) R.id.opentrade_tv_product);
        this.V = (TextView) c((int) R.id.opentrade_tv_username);
        this.W = (TextView) c((int) R.id.opentrade_tv_momoid);
        this.Y = (Button) c((int) R.id.opentrade_btn_confim);
        this.X = (TextView) c((int) R.id.opentrade_tv_desc);
        this.Y.setText("付款");
        this.X.setText("适用于中国联通的手机用户");
    }

    public final boolean F() {
        return false;
    }

    public final void aj() {
        this.T.setText("支付金额: " + a.a(this.R.e) + "元");
        this.U.setText("商品: " + this.O.h);
        if (this.O.g != null) {
            this.V.setText("用户: " + this.O.g.h());
            this.W.setText("陌陌号: " + this.O.g.h);
            return;
        }
        this.V.setText("用户: ");
        this.W.setText("陌陌号: ");
    }

    public final void g(Bundle bundle) {
        this.Y.setOnClickListener(new ay(this));
        aj();
    }

    public final void p() {
        super.p();
    }
}
