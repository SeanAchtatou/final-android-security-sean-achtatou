package com.immomo.momo.android.activity.event;

import com.immomo.momo.android.view.au;
import java.io.File;

final class l implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ k f1414a;
    private final /* synthetic */ File b;
    private final /* synthetic */ boolean c;

    l(k kVar, File file, boolean z) {
        this.f1414a = kVar;
        this.b = file;
        this.c = z;
    }

    public final void run() {
        if (this.b != null && this.b.exists()) {
            this.f1414a.f1413a.ah = new au(this.c ? 1 : 2);
            this.f1414a.f1413a.ah.a(this.b, this.f1414a.f1413a.C);
            this.f1414a.f1413a.ah.b(20);
            this.f1414a.f1413a.C.setGifDecoder(this.f1414a.f1413a.ah);
        }
    }
}
