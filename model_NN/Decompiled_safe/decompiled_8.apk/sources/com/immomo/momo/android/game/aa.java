package com.immomo.momo.android.game;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.m;

final class aa extends d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ y f2398a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public aa(y yVar, Context context) {
        super(context);
        this.f2398a = yVar;
    }

    /* access modifiers changed from: protected */
    public final Object a(Object... objArr) {
        m.a().a(this.f2398a.P, this.f2398a.Q, this.f2398a.O, this.f2398a.R);
        return null;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.f2398a.a(new v(this.f2398a.c(), "正在获取支付信息...", this));
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        super.a(exc);
        this.f2398a.N();
    }

    /* access modifiers changed from: protected */
    public final void a(Object obj) {
        this.f2398a.S.setAppInfo(this.f2398a.R.c, this.f2398a.R.d);
        this.f2398a.S.init(this.f2398a.c(), this.f2398a.T);
        this.f2398a.R.i = true;
    }
}
