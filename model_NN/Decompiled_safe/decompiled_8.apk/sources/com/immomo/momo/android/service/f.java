package com.immomo.momo.android.service;

import android.content.Context;
import com.immomo.momo.a;
import com.immomo.momo.android.c.d;
import com.immomo.momo.service.a.ad;
import com.immomo.momo.service.bean.ak;
import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

final class f extends d {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public Map f2606a = null;
    /* access modifiers changed from: private */
    public List c = null;
    private Set d = null;
    /* access modifiers changed from: private */
    public ad e = null;
    private /* synthetic */ Initializer f;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public f(Initializer initializer, Context context) {
        super(context);
        this.f = initializer;
    }

    private void a(File file) {
        File file2;
        boolean startsWith = file.getAbsolutePath().startsWith(a.d().getAbsolutePath());
        boolean startsWith2 = file.getAbsolutePath().startsWith(a.g().getAbsolutePath());
        this.e.a().beginTransaction();
        try {
            File[] listFiles = file.listFiles();
            int length = listFiles.length;
            for (int i = 0; i < length; i++) {
                file2 = listFiles[i];
                String name = file2.getName();
                if (file2.isFile() && name.endsWith(".jpg_") && !this.d.contains(file2.getAbsolutePath())) {
                    ak akVar = new ak();
                    akVar.f2990a = name.substring(0, name.lastIndexOf(".jpg_"));
                    akVar.b = file2.getAbsolutePath();
                    akVar.e = new Date(file2.lastModified());
                    if (startsWith) {
                        akVar.d = 3;
                        akVar.f2990a = String.valueOf(akVar.f2990a) + "_s";
                    } else if (startsWith2) {
                        akVar.f2990a = String.valueOf(akVar.f2990a) + "_l";
                        akVar.d = 2;
                    }
                    this.e.b(akVar);
                } else if (file2.isDirectory()) {
                    a(file2);
                }
            }
            this.e.a().setTransactionSuccessful();
            this.e.a().endTransaction();
        } catch (Exception e2) {
            file2.delete();
        } catch (Throwable th) {
            this.e.a().endTransaction();
            throw th;
        }
    }

    private void b(int i) {
        File g;
        boolean z = true;
        if (i == 1) {
            g = a.d();
        } else if (i == 0) {
            g = a.g();
            z = false;
        } else {
            return;
        }
        if (g != null) {
            Initializer initializer = this.f;
            Initializer.a(g);
            Calendar instance = Calendar.getInstance();
            instance.add(6, -30);
            g.listFiles(new g(this, instance.getTime(), g, z));
        }
    }

    /* access modifiers changed from: protected */
    public final Object a(Object... objArr) {
        b(1);
        b(0);
        if (this.c.size() > 0) {
            this.e.c("i_imageid", this.c.toArray(new String[this.c.size()]));
            this.c.clear();
        }
        Collections.addAll(this.d, this.e.a("i_path", new String[0], new String[0]));
        a(a.a());
        return null;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.c = new ArrayList();
        this.f2606a = new HashMap();
        this.d = new HashSet();
        this.e = ad.g();
        List<ak> b = this.e.b();
        if (b != null) {
            for (ak akVar : b) {
                this.f2606a.put(akVar.b, akVar);
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, boolean]
     candidates:
      com.immomo.momo.util.ak.a(android.content.Context, java.lang.String):com.immomo.momo.util.ak
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Integer):int
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Long):long
      com.immomo.momo.util.ak.a(java.lang.String, int):void
      com.immomo.momo.util.ak.a(java.lang.String, long):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.String):void
      com.immomo.momo.util.ak.a(java.lang.String, boolean):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Boolean):boolean
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void */
    /* access modifiers changed from: protected */
    public final void a(Object obj) {
        this.f.f2599a.a("imagecache_inited", (Object) true);
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.f.d = true;
        this.f.b();
    }
}
