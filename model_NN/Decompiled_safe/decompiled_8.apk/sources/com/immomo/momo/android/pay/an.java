package com.immomo.momo.android.pay;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import java.io.IOException;

final class an implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ String f2533a;
    private final /* synthetic */ Context b;

    an(String str, Context context) {
        this.f2533a = str;
        this.b = context;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        try {
            Runtime.getRuntime().exec("chmod " + "777" + " " + this.f2533a);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.addFlags(268435456);
        intent.setDataAndType(Uri.parse("file://" + this.f2533a), "application/vnd.android.package-archive");
        this.b.startActivity(intent);
    }
}
