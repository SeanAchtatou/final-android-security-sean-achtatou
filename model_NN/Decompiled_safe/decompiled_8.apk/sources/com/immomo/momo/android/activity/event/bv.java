package com.immomo.momo.android.activity.event;

import android.content.DialogInterface;

final class bv implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ PublishEventFeedActivity f1390a;

    bv(PublishEventFeedActivity publishEventFeedActivity) {
        this.f1390a = publishEventFeedActivity;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f1390a.finish();
    }
}
