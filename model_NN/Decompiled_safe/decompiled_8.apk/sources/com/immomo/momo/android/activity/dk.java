package com.immomo.momo.android.activity;

import android.view.View;
import android.view.ViewGroup;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.view.EmoteEditeText;
import com.immomo.momo.android.view.a.n;
import com.immomo.momo.g;
import com.immomo.momo.util.aq;

final class dk implements View.OnClickListener {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public /* synthetic */ EditVipProfileActivity f1253a;

    dk(EditVipProfileActivity editVipProfileActivity) {
        this.f1253a = editVipProfileActivity;
    }

    public final void onClick(View view) {
        switch (view.getId()) {
            case R.id.layout_name /*2131165525*/:
                View inflate = g.o().inflate((int) R.layout.dialog_editprofile_name, (ViewGroup) null);
                EmoteEditeText emoteEditeText = (EmoteEditeText) inflate.findViewById(R.id.edittext_name);
                n a2 = n.a(this.f1253a, PoiTypeDef.All, new dl(this, emoteEditeText));
                a2.setTitle((int) R.string.dialog_title_editprofile_name);
                a2.setContentView(inflate);
                String trim = this.f1253a.s.getText().toString().trim();
                emoteEditeText.setText(trim);
                emoteEditeText.setSelection(trim.length());
                emoteEditeText.addTextChangedListener(new aq(24));
                a2.show();
                return;
            case R.id.layout_birthday /*2131165532*/:
                EditVipProfileActivity.f(this.f1253a);
                return;
            case R.id.layout_industry /*2131166202*/:
                EditVipProfileActivity.g(this.f1253a);
                return;
            case R.id.vip_avatar_container /*2131166551*/:
                EditVipProfileActivity.d(this.f1253a);
                return;
            default:
                return;
        }
    }
}
