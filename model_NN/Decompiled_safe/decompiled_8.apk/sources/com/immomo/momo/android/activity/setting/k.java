package com.immomo.momo.android.activity.setting;

import android.content.Context;
import com.immomo.momo.android.c.d;

final class k extends d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ CommunityBindActivity f2134a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public k(CommunityBindActivity communityBindActivity, Context context) {
        super(context);
        this.f2134a = communityBindActivity;
        if (communityBindActivity.t != null) {
            communityBindActivity.t.cancel(true);
        }
        communityBindActivity.t = this;
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ Object a(Object... objArr) {
        return com.immomo.momo.protocol.a.d.a();
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void a(Object obj) {
        int[] iArr = (int[]) obj;
        super.a(iArr);
        if (iArr != null) {
            this.f2134a.a(iArr);
        }
    }
}
