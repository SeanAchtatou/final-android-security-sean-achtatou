package com.immomo.momo.android.activity.contacts;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.jx;
import com.immomo.momo.android.activity.lh;
import com.immomo.momo.android.view.HeaderLayout;
import com.immomo.momo.android.view.bi;
import com.immomo.momo.g;

public class al extends jx implements View.OnClickListener {
    private void al() {
        String str = PoiTypeDef.All;
        if (Q() == 0) {
            str = String.valueOf(a((int) R.string.relation_both)) + " (" + this.M.y + ")";
        } else if (Q() == 1) {
            str = String.valueOf(a((int) R.string.relation_follow)) + " (" + this.M.x + ")";
        } else if (Q() == 2) {
            str = String.valueOf(a((int) R.string.relation_fans)) + " (" + this.M.w + ")";
        } else if (Q() == 3) {
            str = a((int) R.string.relation_group);
        }
        a((CharSequence) str);
    }

    /* access modifiers changed from: protected */
    public final int B() {
        return R.layout.activity_contact_tabs;
    }

    /* access modifiers changed from: protected */
    public final void C() {
        K().a(new bi(g.c()).a((int) R.drawable.ic_topbar_adduser), new am(this));
    }

    public final HeaderLayout K() {
        return (HeaderLayout) c((int) R.id.layout_header);
    }

    /* access modifiers changed from: protected */
    public final void a(Fragment fragment, int i) {
        switch (i) {
            case 0:
                c((int) R.id.contact_tab_both).setSelected(true);
                c((int) R.id.contact_tab_follows).setSelected(false);
                c((int) R.id.contact_tab_fans).setSelected(false);
                c((int) R.id.contact_tab_group).setSelected(false);
                break;
            case 1:
                c((int) R.id.contact_tab_both).setSelected(false);
                c((int) R.id.contact_tab_follows).setSelected(true);
                c((int) R.id.contact_tab_fans).setSelected(false);
                c((int) R.id.contact_tab_group).setSelected(false);
                break;
            case 2:
                c((int) R.id.contact_tab_both).setSelected(false);
                c((int) R.id.contact_tab_follows).setSelected(false);
                c((int) R.id.contact_tab_fans).setSelected(true);
                c((int) R.id.contact_tab_group).setSelected(false);
                break;
            case 3:
                c((int) R.id.contact_tab_both).setSelected(false);
                c((int) R.id.contact_tab_follows).setSelected(false);
                c((int) R.id.contact_tab_fans).setSelected(false);
                c((int) R.id.contact_tab_group).setSelected(true);
                break;
        }
        ((lh) fragment).a(c(), K());
        if (M() && !((lh) fragment).T()) {
            if (((lh) fragment).J()) {
                ((lh) fragment).S();
            } else {
                ((lh) fragment).U();
            }
        }
        al();
    }

    /* access modifiers changed from: protected */
    public final void ae() {
        al();
        lh P = P();
        if (P != null) {
            if (P.J()) {
                P.ad();
            }
            if (M() && !P.T()) {
                if (P.J()) {
                    P.S();
                } else {
                    P.U();
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void ag() {
        super.ag();
        if (P() != null && P().J()) {
            P().af();
        }
    }

    public final void ai() {
        lh P = P();
        if (P != null) {
            P.ai();
        }
    }

    /* access modifiers changed from: protected */
    public final void g(Bundle bundle) {
        c((int) R.id.contact_tab_both).setOnClickListener(this);
        c((int) R.id.contact_tab_follows).setOnClickListener(this);
        c((int) R.id.contact_tab_fans).setOnClickListener(this);
        c((int) R.id.contact_tab_group).setOnClickListener(this);
        a(a.class, aw.class, an.class, bk.class);
        O();
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.contact_tab_both /*2131165444*/:
                f(0);
                return;
            case R.id.contact_tab_follows /*2131165445*/:
                f(1);
                return;
            case R.id.contact_tab_fans /*2131165446*/:
                f(2);
                return;
            case R.id.contact_tab_group /*2131165447*/:
                f(3);
                return;
            default:
                return;
        }
    }
}
