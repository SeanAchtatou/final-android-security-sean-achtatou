package com.immomo.momo.android.activity.tieba;

import android.content.Context;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.service.j;

final class ax extends d {

    /* renamed from: a  reason: collision with root package name */
    private v f2173a = null;
    private /* synthetic */ PublishTieActivity c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ax(PublishTieActivity publishTieActivity, Context context) {
        super(context);
        this.c = publishTieActivity;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        return com.immomo.momo.protocol.a.v.a().a(this.c.p.getText().toString().trim(), this.c.o.getText().toString().trim(), this.c.u, PoiTypeDef.All);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.f2173a = new v(this.c, "请稍候，正在提交...");
        this.f2173a.setOnCancelListener(new ay(this));
        this.c.a(this.f2173a);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        PublishTieActivity.g(this.c);
        PublishTieActivity.h(this.c);
        j.a().b(this.c);
        this.c.finish();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.c.p();
    }
}
