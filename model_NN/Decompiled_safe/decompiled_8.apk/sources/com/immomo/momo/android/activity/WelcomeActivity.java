package com.immomo.momo.android.activity;

import android.content.Intent;
import android.content.res.TypedArray;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.util.TypedValue;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.maintab.MaintabActivity;
import com.immomo.momo.android.b.z;
import com.immomo.momo.android.broadcast.b;
import com.immomo.momo.android.broadcast.d;
import com.immomo.momo.android.broadcast.l;
import com.immomo.momo.android.c.h;
import com.immomo.momo.android.service.Initializer;
import com.immomo.momo.android.view.PointLayout;
import com.immomo.momo.android.view.ScrollViewPager;
import com.immomo.momo.android.view.WelcomeView;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.bj;
import com.immomo.momo.util.ak;
import com.immomo.momo.util.k;
import java.util.List;

public class WelcomeActivity extends ao implements d {
    public static List i;
    private String A;
    private String B;
    TypedValue h = new TypedValue();
    private b j = null;
    private ScrollViewPager k;
    /* access modifiers changed from: private */
    public PointLayout l;
    private nh m;
    /* access modifiers changed from: private */
    public TypedArray n = null;
    /* access modifiers changed from: private */
    public TypedArray o = null;
    /* access modifiers changed from: private */
    public String[] p;
    /* access modifiers changed from: private */
    public String[] q;
    /* access modifiers changed from: private */
    public String[] r;
    /* access modifiers changed from: private */
    public String[] s;
    /* access modifiers changed from: private */
    public Location t;
    /* access modifiers changed from: private */
    public ng u;
    /* access modifiers changed from: private */
    public bj v;
    /* access modifiers changed from: private */
    public Handler w = new Handler();
    private String x;
    private String y;
    private String z;

    static /* synthetic */ void b(WelcomeActivity welcomeActivity, bj bjVar) {
        welcomeActivity.v = bjVar;
        i = bjVar.b;
        welcomeActivity.m.a(bjVar);
        welcomeActivity.l.setPointCount(5);
        welcomeActivity.l.setCurentPoint(1);
    }

    private boolean b(Intent intent) {
        this.x = intent.getStringExtra("alipay_user_id");
        this.y = intent.getStringExtra("auth_code");
        this.z = intent.getStringExtra("app_id");
        intent.getStringExtra("version");
        this.A = intent.getStringExtra("alipay_client_version");
        this.B = intent.getStringExtra("source");
        return (this.A != null && this.A.startsWith("7.1")) || (this.B != null && this.B.equalsIgnoreCase("alipay_wallet"));
    }

    private void f() {
        setContentView((int) R.layout.activity_welcome_login);
        this.k = (ScrollViewPager) findViewById(R.id.welcome_login_scrollviewpager);
        this.l = (PointLayout) findViewById(R.id.welcome_login_ponint);
        this.l.a();
        this.n = getResources().obtainTypedArray(R.array.welcome_bg);
        this.o = getResources().obtainTypedArray(R.array.welcome_photo);
        this.p = getResources().getStringArray(R.array.welcome_job);
        this.q = getResources().getStringArray(R.array.welcome_no);
        this.r = getResources().getStringArray(R.array.welcome_info);
        this.s = getResources().getStringArray(R.array.welcome_blue_text);
        this.m = new nh(this, (byte) 0);
        this.l.setPointCount(5);
        this.k.setAdapter(this.m);
        this.k.setOnPageChangeListener(new nc(this));
        this.t = z.b();
        if (z.a(this.t)) {
            this.u = new ng(this, this, this.t, WelcomeView.getCount());
            this.u.execute(new Object[0]);
        } else {
            this.e.a((Object) "-welcome - yes");
            try {
                z.c(new nd(this));
            } catch (Exception e) {
                this.e.a((Throwable) e);
            }
        }
        findViewById(R.id.btn_login).setOnClickListener(new mz(this));
        findViewById(R.id.btn_about).setOnClickListener(new na(this));
        findViewById(R.id.btn_register).setOnClickListener(new nb(this));
        this.j = new l(this);
        this.j.a(this);
    }

    private void g() {
        Intent intent = new Intent(this, AlipayUserWelcomeActivity.class);
        intent.putExtra("alipay_user_id", this.x);
        intent.putExtra("alipay_user_authcode", this.y);
        intent.putExtra("alipay_user_appid", this.z);
        startActivity(intent);
        finish();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.ak.a(java.lang.String, java.lang.Long):long
     arg types: [java.lang.String, long]
     candidates:
      com.immomo.momo.util.ak.a(android.content.Context, java.lang.String):com.immomo.momo.util.ak
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Integer):int
      com.immomo.momo.util.ak.a(java.lang.String, int):void
      com.immomo.momo.util.ak.a(java.lang.String, long):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.String):void
      com.immomo.momo.util.ak.a(java.lang.String, boolean):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Boolean):boolean
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Long):long */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.Long]
     candidates:
      com.immomo.momo.util.ak.a(android.content.Context, java.lang.String):com.immomo.momo.util.ak
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Integer):int
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Long):long
      com.immomo.momo.util.ak.a(java.lang.String, int):void
      com.immomo.momo.util.ak.a(java.lang.String, long):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.String):void
      com.immomo.momo.util.ak.a(java.lang.String, boolean):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Boolean):boolean
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, long]
     candidates:
      com.immomo.momo.util.ak.a(android.content.Context, java.lang.String):com.immomo.momo.util.ak
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Integer):int
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Long):long
      com.immomo.momo.util.ak.a(java.lang.String, int):void
      com.immomo.momo.util.ak.a(java.lang.String, long):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.String):void
      com.immomo.momo.util.ak.a(java.lang.String, boolean):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Boolean):boolean
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void */
    /* access modifiers changed from: private */
    public void i() {
        this.e.a((Object) "checkUpdateVersion~~~~~~~~~~~~~~~~~");
        ak akVar = g.d().f668a;
        try {
            long a2 = akVar.a("last_checkversion_time", (Long) 0L);
            long currentTimeMillis = System.currentTimeMillis() / 1000;
            if (Math.abs(a2 - currentTimeMillis) > 86400) {
                new h(this, false).execute(new String[0]);
                akVar.a("last_checkversion_time", (Object) Long.valueOf(currentTimeMillis));
            }
        } catch (Exception e) {
            akVar.a("last_checkversion_time", (Object) 0L);
        }
    }

    public final void a(Intent intent) {
        if (intent.getAction().equals(l.f2355a)) {
            finish();
        }
    }

    public void onBackPressed() {
        finish();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        new k("M", "M1").e();
        if (getIntent().getIntExtra("model", 1) == 0) {
            f();
        } else if (b(getIntent())) {
            g();
        } else if (this.f != null) {
            Intent intent = new Intent(getApplicationContext(), MaintabActivity.class);
            intent.setFlags(335544320);
            getApplicationContext().startActivity(intent);
            finish();
            this.w.post(new nf(this));
        } else {
            f();
            i();
            startService(new Intent(this, Initializer.class));
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.j != null) {
            unregisterReceiver(this.j);
            this.j = null;
        }
        if (this.n != null) {
            this.n.recycle();
        }
        if (this.o != null) {
            this.o.recycle();
        }
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (b(intent)) {
            g();
        }
    }
}
