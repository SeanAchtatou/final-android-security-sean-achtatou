package com.immomo.momo.android.plugin.cropimage;

import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Region;
import android.graphics.drawable.Drawable;
import android.view.View;
import com.immomo.momo.R;

public final class k {

    /* renamed from: a  reason: collision with root package name */
    boolean f2590a;
    Rect b;
    RectF c;
    Matrix d;
    private View e;
    private l f = l.None;
    private RectF g;
    private boolean h = false;
    private float i;
    private boolean j = false;
    private Drawable k;
    private Drawable l;
    private Drawable m;
    private final Paint n = new Paint();
    private final Paint o = new Paint();
    private final Paint p = new Paint();

    public k(View view) {
        this.e = view;
    }

    private Rect c() {
        RectF rectF = new RectF(this.c.left, this.c.top, this.c.right, this.c.bottom);
        this.d.mapRect(rectF);
        return new Rect(Math.round(rectF.left), Math.round(rectF.top), Math.round(rectF.right), Math.round(rectF.bottom));
    }

    public final int a(float f2, float f3) {
        boolean z = false;
        Rect c2 = c();
        if (this.j) {
            float centerX = f2 - ((float) c2.centerX());
            float centerY = f3 - ((float) c2.centerY());
            int sqrt = (int) Math.sqrt((double) ((centerX * centerX) + (centerY * centerY)));
            int width = this.b.width() / 2;
            return ((float) Math.abs(sqrt - width)) <= 20.0f ? Math.abs(centerY) > Math.abs(centerX) ? centerY < 0.0f ? 8 : 16 : centerX < 0.0f ? 2 : 4 : sqrt < width ? 32 : 1;
        }
        boolean z2 = f3 >= ((float) c2.top) - 20.0f && f3 < ((float) c2.bottom) + 20.0f;
        if (f2 >= ((float) c2.left) - 20.0f && f2 < ((float) c2.right) + 20.0f) {
            z = true;
        }
        int i2 = (Math.abs(((float) c2.left) - f2) >= 20.0f || !z2) ? 1 : 3;
        if (Math.abs(((float) c2.right) - f2) < 20.0f && z2) {
            i2 |= 4;
        }
        if (Math.abs(((float) c2.top) - f3) < 20.0f && z) {
            i2 |= 8;
        }
        int i3 = (Math.abs(((float) c2.bottom) - f3) >= 20.0f || !z) ? i2 : i2 | 16;
        if (i3 != 1 || !c2.contains((int) f2, (int) f3)) {
            return i3;
        }
        return 32;
    }

    public final Rect a() {
        return new Rect((int) this.c.left, (int) this.c.top, (int) this.c.right, (int) this.c.bottom);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(float, float):float} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    /* access modifiers changed from: package-private */
    public final void a(int i2, float f2, float f3) {
        int i3 = 1;
        Rect c2 = c();
        if (i2 != 1) {
            if (i2 == 32) {
                Rect rect = new Rect(this.b);
                this.c.offset((this.c.width() / ((float) c2.width())) * f2, (this.c.height() / ((float) c2.height())) * f3);
                this.c.offset(Math.max(0.0f, this.g.left - this.c.left), Math.max(0.0f, this.g.top - this.c.top));
                this.c.offset(Math.min(0.0f, this.g.right - this.c.right), Math.min(0.0f, this.g.bottom - this.c.bottom));
                this.b = c();
                rect.union(this.b);
                rect.inset(-10, -10);
                this.e.invalidate(rect);
                return;
            }
            if ((i2 & 6) == 0) {
                f2 = 0.0f;
            }
            if ((i2 & 24) == 0) {
                f3 = 0.0f;
            }
            float width = (this.c.width() / ((float) c2.width())) * f2;
            float height = f3 * (this.c.height() / ((float) c2.height()));
            float f4 = ((float) ((i2 & 2) != 0 ? -1 : 1)) * width;
            if ((i2 & 8) != 0) {
                i3 = -1;
            }
            float f5 = ((float) i3) * height;
            if (this.h) {
                if (f4 != 0.0f) {
                    f5 = f4 / this.i;
                } else if (f5 != 0.0f) {
                    f4 = this.i * f5;
                }
            }
            RectF rectF = new RectF(this.c);
            if (f4 > 0.0f && rectF.width() + (2.0f * f4) > this.g.width()) {
                f4 = (this.g.width() - rectF.width()) / 2.0f;
                if (this.h) {
                    f5 = f4 / this.i;
                }
            }
            if (f5 > 0.0f && rectF.height() + (2.0f * f5) > this.g.height()) {
                f5 = (this.g.height() - rectF.height()) / 2.0f;
                if (this.h) {
                    f4 = this.i * f5;
                }
            }
            rectF.inset(-f4, -f5);
            if (rectF.width() < 25.0f) {
                rectF.inset((-(25.0f - rectF.width())) / 2.0f, 0.0f);
            }
            float f6 = this.h ? 25.0f / this.i : 25.0f;
            if (rectF.height() < f6) {
                rectF.inset(0.0f, (-(f6 - rectF.height())) / 2.0f);
            }
            if (rectF.left < this.g.left) {
                rectF.offset(this.g.left - rectF.left, 0.0f);
            } else if (rectF.right > this.g.right) {
                rectF.offset(-(rectF.right - this.g.right), 0.0f);
            }
            if (rectF.top < this.g.top) {
                rectF.offset(0.0f, this.g.top - rectF.top);
            } else if (rectF.bottom > this.g.bottom) {
                rectF.offset(0.0f, -(rectF.bottom - this.g.bottom));
            }
            this.c.set(rectF);
            this.b = c();
            this.e.invalidate();
        }
    }

    /* access modifiers changed from: protected */
    public final void a(Canvas canvas) {
        canvas.save();
        Path path = new Path();
        if (!this.f2590a) {
            this.p.setColor(-16777216);
            canvas.drawRect(this.b, this.p);
            return;
        }
        Rect rect = new Rect();
        this.e.getDrawingRect(rect);
        if (this.j) {
            float width = (float) this.b.width();
            path.addCircle(((float) this.b.left) + (width / 2.0f), (((float) this.b.height()) / 2.0f) + ((float) this.b.top), width / 2.0f, Path.Direction.CW);
            this.p.setColor(-1112874);
        } else {
            path.addRect(new RectF(this.b), Path.Direction.CW);
            this.p.setColor(-15889209);
        }
        try {
            canvas.clipPath(path, Region.Op.DIFFERENCE);
            canvas.drawRect(rect, this.f2590a ? this.n : this.o);
        } catch (Exception e2) {
            canvas.drawRect(rect, this.o);
        }
        canvas.restore();
        canvas.drawPath(path, this.p);
        if (this.f != l.Grow) {
            return;
        }
        if (this.j) {
            int intrinsicWidth = this.m.getIntrinsicWidth();
            int intrinsicHeight = this.m.getIntrinsicHeight();
            int round = (int) Math.round(Math.cos(0.7853981633974483d) * (((double) this.b.width()) / 2.0d));
            int width2 = ((this.b.left + (this.b.width() / 2)) + round) - (intrinsicWidth / 2);
            int height = ((this.b.top + (this.b.height() / 2)) - round) - (intrinsicHeight / 2);
            this.m.setBounds(width2, height, this.m.getIntrinsicWidth() + width2, this.m.getIntrinsicHeight() + height);
            this.m.draw(canvas);
            return;
        }
        int i2 = this.b.left + 1;
        int i3 = this.b.right + 1;
        int i4 = this.b.top + 4;
        int i5 = this.b.bottom + 3;
        int intrinsicWidth2 = this.k.getIntrinsicWidth() / 2;
        int intrinsicHeight2 = this.k.getIntrinsicHeight() / 2;
        int intrinsicHeight3 = this.l.getIntrinsicHeight() / 2;
        int intrinsicWidth3 = this.l.getIntrinsicWidth() / 2;
        int i6 = this.b.left + ((this.b.right - this.b.left) / 2);
        int i7 = this.b.top + ((this.b.bottom - this.b.top) / 2);
        this.k.setBounds(i2 - intrinsicWidth2, i7 - intrinsicHeight2, i2 + intrinsicWidth2, i7 + intrinsicHeight2);
        this.k.draw(canvas);
        this.k.setBounds(i3 - intrinsicWidth2, i7 - intrinsicHeight2, i3 + intrinsicWidth2, i7 + intrinsicHeight2);
        this.k.draw(canvas);
        this.l.setBounds(i6 - intrinsicWidth3, i4 - intrinsicHeight3, i6 + intrinsicWidth3, i4 + intrinsicHeight3);
        this.l.draw(canvas);
        this.l.setBounds(i6 - intrinsicWidth3, i5 - intrinsicHeight3, i6 + intrinsicWidth3, i5 + intrinsicHeight3);
        this.l.draw(canvas);
    }

    public final void a(Matrix matrix, Rect rect, RectF rectF, boolean z, boolean z2) {
        if (z) {
            z2 = true;
        }
        this.d = new Matrix(matrix);
        this.c = rectF;
        this.g = new RectF(rect);
        this.h = z2;
        this.j = z;
        this.i = this.c.width() / this.c.height();
        this.b = c();
        this.n.setARGB(165, 0, 0, 0);
        this.o.setARGB(0, 0, 0, 0);
        this.p.setStrokeWidth(3.0f);
        this.p.setStyle(Paint.Style.STROKE);
        this.p.setAntiAlias(true);
        this.f = l.None;
        Resources resources = this.e.getResources();
        this.k = resources.getDrawable(R.drawable.ic_cropphoto_buoy2);
        this.l = resources.getDrawable(R.drawable.ic_cropphoto_buoy);
        this.m = resources.getDrawable(R.drawable.ic_cropphoto_buoy);
    }

    public final void a(l lVar) {
        if (lVar != this.f) {
            this.f = lVar;
            this.e.invalidate();
        }
    }

    public final void b() {
        this.b = c();
    }
}
