package com.immomo.momo.android.activity;

import android.content.Intent;
import android.view.View;

final class iy implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ OtherProfileV2Activity f1753a;

    iy(OtherProfileV2Activity otherProfileV2Activity) {
        this.f1753a = otherProfileV2Activity;
    }

    public final void onClick(View view) {
        this.f1753a.startActivity(this.f1753a.f.b() ? new Intent(this.f1753a, EditVipProfileActivity.class) : new Intent(this.f1753a, EditUserProfileActivity.class));
    }
}
