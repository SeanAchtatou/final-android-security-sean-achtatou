package com.immomo.momo.android.plugin.cropimage;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import com.baidu.location.LocationClientOption;
import com.immomo.a.a.f.b;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.ImageFactoryActivity;
import com.immomo.momo.android.activity.fr;
import com.immomo.momo.g;
import com.immomo.momo.util.j;
import com.immomo.momo.util.m;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public class CropImageActivity extends r {

    /* renamed from: a  reason: collision with root package name */
    boolean f2578a;
    k b;
    float c = 0.0f;
    Runnable d = new a(this);
    private int e = (g.J() * 2);
    private int f = this.e;
    /* access modifiers changed from: private */
    public final Handler g = new Handler();
    private int h;
    private int i = 0;
    /* access modifiers changed from: private */
    public CropImageView j;
    /* access modifiers changed from: private */
    public fr k = null;
    /* access modifiers changed from: private */
    public Bitmap l;
    /* access modifiers changed from: private */
    public m m = new m("CropImageActivity");

    static {
        CropImageActivity.class.getSimpleName();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    static /* synthetic */ Bitmap a(Matrix matrix, Bitmap bitmap, int i2, int i3, boolean z) {
        Bitmap bitmap2;
        Matrix matrix2 = null;
        int width = bitmap.getWidth() - i2;
        int height = bitmap.getHeight() - i3;
        if (z || (width >= 0 && height >= 0)) {
            float width2 = (float) bitmap.getWidth();
            float height2 = (float) bitmap.getHeight();
            if (width2 / height2 > ((float) i2) / ((float) i3)) {
                float f2 = ((float) i3) / height2;
                if (f2 < 0.9f || f2 > 1.0f) {
                    matrix.setScale(f2, f2);
                    matrix2 = matrix;
                }
            } else {
                float f3 = ((float) i2) / width2;
                if (f3 < 0.9f || f3 > 1.0f) {
                    matrix.setScale(f3, f3);
                    matrix2 = matrix;
                }
            }
            if (matrix2 != null) {
                bitmap2 = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix2, true);
            } else {
                bitmap2 = bitmap;
            }
            if (bitmap2 != bitmap) {
                bitmap.recycle();
            }
            Bitmap createBitmap = Bitmap.createBitmap(bitmap2, Math.max(0, bitmap2.getWidth() - i2) / 2, Math.max(0, bitmap2.getHeight() - i3) / 2, i2, i3);
            if (createBitmap == bitmap2) {
                return createBitmap;
            }
            bitmap2.recycle();
            return createBitmap;
        }
        Bitmap createBitmap2 = Bitmap.createBitmap(i2, i3, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(createBitmap2);
        int max = Math.max(0, width / 2);
        int max2 = Math.max(0, height / 2);
        Rect rect = new Rect(max, max2, Math.min(i2, bitmap.getWidth()) + max, Math.min(i3, bitmap.getHeight()) + max2);
        int width3 = (i2 - rect.width()) / 2;
        int height3 = (i3 - rect.height()) / 2;
        canvas.drawBitmap(bitmap, rect, new Rect(width3, height3, i2 - width3, i3 - height3), (Paint) null);
        bitmap.recycle();
        System.gc();
        return createBitmap2;
    }

    static /* synthetic */ boolean b() {
        return false;
    }

    /* access modifiers changed from: private */
    public Bitmap c() {
        Bitmap bitmap;
        InputStream inputStream = null;
        try {
            String path = this.k.o.getPath();
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            while (true) {
                inputStream = path.startsWith("/asset/") ? getAssets().open(path.substring(path.indexOf("/asset/") + 7, path.length())) : getContentResolver().openInputStream(this.k.o);
                bitmap = BitmapFactory.decodeStream(inputStream, null, options);
                if (!options.inJustDecodeBounds) {
                    break;
                }
                int min = Math.min(this.f, this.k.m);
                int min2 = Math.min(this.e, this.k.n);
                this.m.a((Object) ("tWidth=" + min + ", tHeight=" + min2 + ", srcWidth=" + options.outWidth + ", srcHeight=" + options.outHeight));
                boolean z = Math.abs(options.outHeight - min2) >= Math.abs(options.outWidth - min);
                options.inJustDecodeBounds = false;
                if (options.outHeight * options.outWidth >= min * min2) {
                    options.inSampleSize = (int) ((double) (z ? options.outHeight / min2 : options.outWidth / min));
                }
                inputStream.close();
            }
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e2) {
                }
            }
        } catch (Throwable th) {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e3) {
                }
            }
            throw th;
        }
        return bitmap;
    }

    private void d() {
        if (!isFinishing()) {
            this.i = this.l.getHeight();
            this.h = this.l.getWidth();
            CropImageView cropImageView = this.j;
            int i2 = this.h;
            int i3 = this.i;
            CropImageView.b();
            this.j.a(this.k.l);
            this.m.a((Object) ("srcHeight=" + this.i + ", srcWidth=" + this.h));
            if (this.i < this.j.c() || this.h < this.j.c()) {
                this.k.a(1003);
                this.k.a();
                return;
            }
            this.j.a(this.l);
            String string = getResources().getString(R.string.running_face_detection);
            new Thread(new h(this, new f(this), ProgressDialog.show(this, null, string, true, false), this.g)).start();
        }
    }

    static /* synthetic */ void g(CropImageActivity cropImageActivity) {
        if (cropImageActivity.b != null && !cropImageActivity.f2578a) {
            cropImageActivity.f2578a = true;
            new j(cropImageActivity, (byte) 0).execute(new Object[0]);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public final void a() {
        this.c += 90.0f;
        if (this.c >= 360.0f) {
            this.c = 0.0f;
        }
        Matrix matrix = new Matrix();
        matrix.setRotate(90.0f, 0.5f, 0.5f);
        Bitmap createBitmap = Bitmap.createBitmap(this.l, 0, 0, this.l.getWidth(), this.l.getHeight(), matrix, true);
        Bitmap bitmap = this.l;
        this.l = createBitmap;
        j.a(b.a(), this.l);
        this.j.a();
        d();
        bitmap.recycle();
        System.gc();
        this.k.b.setEnabled(true);
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_imagefactory_crop);
        System.gc();
        this.k = ImageFactoryActivity.a();
        this.j = (CropImageView) findViewById(R.id.image);
        this.j.b = this;
        this.k.f1500a.setTitleText("裁切图片");
        this.k.b.setOnClickListener(new c(this));
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.j.d();
        this.j.f2579a.clear();
        if (this.l != null) {
            this.l.recycle();
            this.l = null;
        }
        System.gc();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    public void onStart() {
        super.onStart();
        this.l = this.k.q;
        if (this.l == null) {
            try {
                this.l = c();
                j.a(b.a(), this.l);
            } catch (FileNotFoundException e2) {
                this.m.a((Throwable) e2);
                this.k.a(1002);
                this.k.a();
                return;
            } catch (Throwable th) {
                this.m.a(th);
                this.k.a(LocationClientOption.MIN_SCAN_SPAN);
                this.k.a();
                return;
            }
        }
        if (this.l == null) {
            this.k.a(1002);
            this.k.a();
            return;
        }
        this.k.c.setText((int) R.string.crop_discard_text);
        this.k.c.setOnClickListener(new d(this));
        this.k.d.setText((int) R.string.crop_save_text);
        this.k.d.setOnClickListener(new e(this));
        this.c = 0.0f;
        d();
    }

    public void onStop() {
        super.onStop();
        if (this.l != null) {
            this.l.recycle();
        }
    }
}
