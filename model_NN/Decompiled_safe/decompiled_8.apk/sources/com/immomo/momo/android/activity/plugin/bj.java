package com.immomo.momo.android.activity.plugin;

import com.immomo.momo.R;
import java.util.Collection;
import java.util.List;

final class bj implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ bh f2064a;
    private final /* synthetic */ List b;

    bj(bh bhVar, List list) {
        this.f2064a = bhVar;
        this.b = list;
    }

    public final void run() {
        this.f2064a.f2062a.i.b((Collection) this.b);
        this.f2064a.f2062a.findViewById(R.id.process_layout_root).setVisibility(8);
    }
}
