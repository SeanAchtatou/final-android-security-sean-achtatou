package com.immomo.momo.android.a;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

final class ac extends Handler {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ab f711a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ac(ab abVar, Looper looper) {
        super(looper);
        this.f711a = abVar;
    }

    public final void handleMessage(Message message) {
        if (message.what == 1 && this.f711a.c != null) {
            this.f711a.f710a.notifyDataSetChanged();
        }
    }
}
