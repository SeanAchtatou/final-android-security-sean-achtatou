package com.immomo.momo.android.activity;

import android.content.Intent;
import com.immomo.momo.android.view.a.al;
import com.immomo.momo.android.view.a.e;
import com.immomo.momo.service.bean.bf;

final class aw implements al {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ av f1034a;
    private final /* synthetic */ bf b;

    aw(av avVar, bf bfVar) {
        this.f1034a = avVar;
        this.b = bfVar;
    }

    public final void a(int i) {
        switch (i) {
            case 0:
                Intent intent = new Intent(this.f1034a.f1033a.getApplicationContext(), OtherProfileActivity.class);
                intent.putExtra("tag", "local");
                intent.putExtra("momoid", this.b.h);
                this.f1034a.f1033a.startActivity(intent);
                return;
            case 1:
                e.a(this.f1034a.f1033a, this.f1034a.f1033a.f, this.b, new ax());
                return;
            case 2:
                new bc(this.f1034a.f1033a, this.b).execute(new Object[0]);
                return;
            default:
                return;
        }
    }
}
