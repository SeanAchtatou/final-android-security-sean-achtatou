package com.immomo.momo.android.activity;

import android.graphics.Bitmap;
import android.support.v4.b.a;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.immomo.momo.R;
import com.immomo.momo.util.ao;

final class mu extends WebViewClient {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ WebviewActivity f2012a;

    mu(WebviewActivity webviewActivity) {
        this.f2012a = webviewActivity;
    }

    public final void onLoadResource(WebView webView, String str) {
        this.f2012a.e.a((Object) ("onLoadResource=" + str));
        super.onLoadResource(webView, str);
    }

    public final void onPageFinished(WebView webView, String str) {
        super.onPageFinished(webView, str);
    }

    public final void onPageStarted(WebView webView, String str, Bitmap bitmap) {
        super.onPageStarted(webView, str, bitmap);
        this.f2012a.e.a((Object) ("open -> " + str));
    }

    public final void onReceivedError(WebView webView, int i, String str, String str2) {
        this.f2012a.e.a((Object) ("errorCode=" + i));
        ao.b(i == -2 ? this.f2012a.getString(R.string.errormsg_network_unfind) : this.f2012a.getString(R.string.errormsg_server));
    }

    public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
        this.f2012a.e.a((Object) ("shouldOverrideUrlLoading->" + str));
        if (!a.a((CharSequence) str)) {
            return this.f2012a.a(webView, str);
        }
        ao.b("网络地址错误");
        return true;
    }
}
