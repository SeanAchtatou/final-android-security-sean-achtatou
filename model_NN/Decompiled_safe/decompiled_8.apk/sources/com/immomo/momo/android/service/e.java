package com.immomo.momo.android.service;

import android.os.Binder;
import android.os.IBinder;
import android.os.Parcel;

public abstract class e extends Binder implements d {
    public e() {
        attachInterface(this, "com.immomo.momo.android.service.GetXMPPStateAIDL");
    }

    public IBinder asBinder() {
        return this;
    }

    public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) {
        int i3 = 0;
        switch (i) {
            case 1:
                parcel.enforceInterface("com.immomo.momo.android.service.GetXMPPStateAIDL");
                boolean a2 = a();
                parcel2.writeNoException();
                if (a2) {
                    i3 = 1;
                }
                parcel2.writeInt(i3);
                return true;
            case 2:
                parcel.enforceInterface("com.immomo.momo.android.service.GetXMPPStateAIDL");
                boolean b = b();
                parcel2.writeNoException();
                if (b) {
                    i3 = 1;
                }
                parcel2.writeInt(i3);
                return true;
            case 3:
                parcel.enforceInterface("com.immomo.momo.android.service.GetXMPPStateAIDL");
                boolean c = c();
                parcel2.writeNoException();
                if (c) {
                    i3 = 1;
                }
                parcel2.writeInt(i3);
                return true;
            case 4:
                parcel.enforceInterface("com.immomo.momo.android.service.GetXMPPStateAIDL");
                String d = d();
                parcel2.writeNoException();
                parcel2.writeString(d);
                return true;
            case 5:
                parcel.enforceInterface("com.immomo.momo.android.service.GetXMPPStateAIDL");
                String e = e();
                parcel2.writeNoException();
                parcel2.writeString(e);
                return true;
            case 1598968902:
                parcel2.writeString("com.immomo.momo.android.service.GetXMPPStateAIDL");
                return true;
            default:
                return super.onTransact(i, parcel, parcel2, i2);
        }
    }
}
