package com.immomo.momo.android.activity.plugin;

import android.content.Context;
import com.immomo.momo.R;
import com.immomo.momo.android.c.d;
import com.immomo.momo.plugin.a.a;
import java.util.Collection;
import java.util.List;

final class be extends d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ DoubanActivity f2059a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public be(DoubanActivity doubanActivity, Context context) {
        super(context);
        this.f2059a = doubanActivity;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        a.a();
        return a.a(this.f2059a.o.c);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        if (exc instanceof com.immomo.momo.a.a) {
            super.a(exc);
            return;
        }
        this.b.a((Throwable) exc);
        a((int) R.string.plus_error_douban_message);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        List list = (List) obj;
        if (list != null) {
            this.f2059a.r.b((Collection) list);
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.f2059a.findViewById(R.id.process_layout_root).setVisibility(8);
    }
}
