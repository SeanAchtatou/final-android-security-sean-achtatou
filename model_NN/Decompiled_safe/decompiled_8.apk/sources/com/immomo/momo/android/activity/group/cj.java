package com.immomo.momo.android.activity.group;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import com.immomo.momo.android.view.a.n;

final class cj implements AdapterView.OnItemClickListener {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public /* synthetic */ GroupPartyActivity f1583a;

    cj(GroupPartyActivity groupPartyActivity) {
        this.f1583a = groupPartyActivity;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        String str = (String) this.f1583a.h.get(i);
        if ("置顶".equals(str)) {
            this.f1583a.b(new cs(this.f1583a, this.f1583a));
        } else if ("取消置顶".equals(str)) {
            this.f1583a.b(new cs(this.f1583a, this.f1583a));
        } else if ("编辑活动".equals(str)) {
            Intent intent = new Intent(this.f1583a, EditGroupPartyActivity.class);
            intent.putExtra("pid", this.f1583a.t.b);
            intent.putExtra("gid", this.f1583a.t.c);
            intent.putExtra("modify_type", 2);
            this.f1583a.startActivityForResult(intent, 33);
        } else if ("删除活动".equals(str)) {
            n.a(this.f1583a, "确定要删除活动吗？该操作不可恢复", new ck(this)).show();
        } else if ("通知群成员".equals(str)) {
            this.f1583a.c(2);
        }
    }
}
