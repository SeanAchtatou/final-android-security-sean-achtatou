package com.immomo.momo.android.activity.common;

import android.content.Context;
import android.widget.ListAdapter;
import com.immomo.momo.android.a.fz;
import com.immomo.momo.android.c.d;
import com.immomo.momo.protocol.a.w;
import java.util.ArrayList;
import java.util.List;

final class am extends d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ al f1082a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public am(al alVar, Context context) {
        super(context);
        this.f1082a = alVar;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        ArrayList arrayList = new ArrayList();
        w.a().b(arrayList, 1);
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        super.a(exc);
        this.f1082a.O.n();
        if (this.f1082a.P != null && !this.f1082a.P.isCancelled()) {
            this.f1082a.P.cancel(true);
        }
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        List list = (List) obj;
        super.a(list);
        this.f1082a.T = new fz(this.f1082a.c(), list, this.f1082a.O, this.f1082a);
        this.f1082a.O.setAdapter((ListAdapter) this.f1082a.T);
    }

    /* access modifiers changed from: protected */
    public final void b() {
        super.b();
        this.f1082a.P = null;
        this.f1082a.O.n();
    }
}
