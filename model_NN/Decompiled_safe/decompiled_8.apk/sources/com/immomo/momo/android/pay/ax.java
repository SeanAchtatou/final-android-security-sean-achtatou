package com.immomo.momo.android.pay;

import android.content.Context;
import android.content.Intent;
import com.immomo.momo.R;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.g;
import com.immomo.momo.protocol.a.a;
import com.immomo.momo.service.aq;
import com.immomo.momo.service.bean.bg;
import com.immomo.momo.util.ao;
import com.immomo.momo.util.k;
import java.util.Map;

final class ax extends d {

    /* renamed from: a  reason: collision with root package name */
    private v f2541a = null;
    private Map c;
    private Context d;
    private int e = 0;
    private /* synthetic */ RechargeActivity f;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ax(RechargeActivity rechargeActivity, Context context, Map map, int i) {
        super(context);
        this.f = rechargeActivity;
        this.c = map;
        this.d = context;
        this.e = i;
        this.f2541a = new v(context);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        bg bgVar = new bg();
        a.a().a(this.c, bgVar);
        if (bgVar.f3012a) {
            this.f.f.b(bgVar.b);
            new aq().a(bgVar.b, this.f.f.h);
        }
        return bgVar;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
        this.f2541a.setCancelable(false);
        this.f2541a.a("正在验证...");
        this.f.a(this.f2541a);
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        super.a(exc);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.android.pay.RechargeActivity.a(com.immomo.momo.android.pay.RechargeActivity, boolean):void
     arg types: [com.immomo.momo.android.pay.RechargeActivity, int]
     candidates:
      com.immomo.momo.android.pay.RechargeActivity.a(com.immomo.momo.android.pay.RechargeActivity, com.immomo.momo.android.pay.ay):void
      com.immomo.momo.android.pay.RechargeActivity.a(com.immomo.momo.android.pay.RechargeActivity, com.immomo.momo.android.pay.bk):void
      com.immomo.momo.android.pay.RechargeActivity.a(com.immomo.momo.android.pay.RechargeActivity, com.immomo.momo.android.view.a.v):void
      com.immomo.momo.android.pay.RechargeActivity.a(com.immomo.momo.android.pay.RechargeActivity, java.lang.String):void
      com.immomo.momo.android.pay.RechargeActivity.a(com.immomo.momo.android.pay.RechargeActivity, java.util.List):void
      com.immomo.momo.android.pay.RechargeActivity.a(java.lang.String, int):void
      com.immomo.momo.android.activity.ao.a(int, java.lang.String[]):com.immomo.momo.protocol.imjson.c.a
      com.immomo.momo.android.activity.ao.a(com.immomo.momo.android.view.bi, android.view.View$OnClickListener):void
      com.immomo.momo.android.activity.ao.a(java.lang.CharSequence, int):void
      com.immomo.momo.android.activity.ao.a(android.os.Bundle, java.lang.String):boolean
      com.immomo.momo.android.pay.RechargeActivity.a(com.immomo.momo.android.pay.RechargeActivity, boolean):void */
    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        bg bgVar = (bg) obj;
        this.f.l.setText(String.valueOf(this.f.f.r()) + " " + g.a((int) R.string.gold_str));
        if (bgVar.f3012a) {
            RechargeActivity.m(this.f);
            this.f.t = false;
            this.f.s.setText((int) R.string.payvip_buy);
            ao.a((CharSequence) bgVar.f);
            new k("U", "U51").e();
            if (this.f.h == 1) {
                this.b.a((Object) ("----------------------------------finish, currentUser.getBalance()=" + this.f.f.r()));
                Intent intent = new Intent();
                intent.putExtra("balance", this.f.f.r());
                this.f.setResult(-1, intent);
                this.f.finish();
                return;
            }
            return;
        }
        new k("U", "U52").e();
        if (this.e < 4) {
            try {
                Thread.sleep(2000);
            } catch (Throwable th) {
            }
            this.c.put("verify", "1");
            this.e++;
            a("验证，第" + this.e + "次请求");
            new ax(this.f, this.d, this.c, this.e).execute(new Object[0]);
            return;
        }
        a("验证失败，请稍候重新验证。");
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.f.p();
    }
}
