package com.immomo.momo.android.activity.group;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.protocol.a.n;
import com.immomo.momo.service.bean.a.a;
import java.util.List;

final class fk extends d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ SiteGroupsActivity f1659a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public fk(SiteGroupsActivity siteGroupsActivity, Context context) {
        super(context);
        this.f1659a = siteGroupsActivity;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        List b = n.a().b(this.f1659a.h, this.f1659a.l.getCount());
        this.f1659a.j.a(b);
        return b;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        List<a> list = (List) obj;
        if (list != null) {
            for (a aVar : list) {
                if (!this.f1659a.i.containsKey(aVar.b)) {
                    this.f1659a.l.b(aVar);
                    this.f1659a.i.put(aVar.b, aVar);
                }
            }
            this.f1659a.l.notifyDataSetChanged();
            if (list.size() < 30) {
                this.f1659a.k.k();
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.f1659a.n = null;
        this.f1659a.m.e();
    }
}
