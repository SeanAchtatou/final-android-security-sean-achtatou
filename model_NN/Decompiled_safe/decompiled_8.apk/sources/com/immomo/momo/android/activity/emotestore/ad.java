package com.immomo.momo.android.activity.emotestore;

import android.content.Context;
import android.widget.ListAdapter;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.i;
import org.json.JSONObject;

final class ad extends d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ EmotionSetTaskActivity f1283a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ad(EmotionSetTaskActivity emotionSetTaskActivity, Context context) {
        super(context);
        this.f1283a = emotionSetTaskActivity;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        return i.a().e(this.f1283a.h);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.f1283a.a(new v(f(), "请稍候，正在获取数据...", this));
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        JSONObject jSONObject = (JSONObject) obj;
        this.f1283a.setTitle(jSONObject.optString("title"));
        this.f1283a.j = jSONObject.optJSONArray("list");
        this.f1283a.i.setAdapter((ListAdapter) new ac(f(), this.f1283a.j));
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.f1283a.p();
    }
}
