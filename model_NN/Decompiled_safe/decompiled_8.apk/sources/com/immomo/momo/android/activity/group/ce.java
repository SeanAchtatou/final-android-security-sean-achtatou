package com.immomo.momo.android.activity.group;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.protocol.a.n;
import java.util.ArrayList;
import java.util.List;

final class ce extends d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ GroupMemberListActivity f1578a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ce(GroupMemberListActivity groupMemberListActivity, Context context) {
        super(context);
        this.f1578a = groupMemberListActivity;
        if (groupMemberListActivity.s != null && !groupMemberListActivity.s.isCancelled()) {
            groupMemberListActivity.s.cancel(true);
        }
        groupMemberListActivity.s = this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.service.y.a(com.immomo.momo.service.bean.a.a, boolean):void
     arg types: [com.immomo.momo.service.bean.a.a, int]
     candidates:
      com.immomo.momo.service.y.a(com.immomo.momo.service.bean.a.a, java.lang.String):void
      com.immomo.momo.service.y.a(java.lang.String, int):void
      com.immomo.momo.service.y.a(java.lang.String, java.lang.String):void
      com.immomo.momo.service.y.a(java.lang.String, java.util.List):void
      com.immomo.momo.service.y.a(java.lang.String, boolean):void
      com.immomo.momo.service.y.a(java.lang.String, java.lang.String[]):void
      com.immomo.momo.service.y.a(java.util.List, java.lang.String):void
      com.immomo.momo.service.y.a(com.immomo.momo.service.bean.a.a, boolean):void */
    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        ArrayList arrayList3 = new ArrayList();
        n.a().a(this.f1578a.q, arrayList, arrayList2, arrayList3);
        this.f1578a.l.a(this.f1578a.q, false);
        ArrayList arrayList4 = new ArrayList();
        arrayList4.addAll(arrayList);
        arrayList4.addAll(arrayList2);
        arrayList4.addAll(arrayList3);
        this.f1578a.l.a(arrayList4, this.f1578a.q.b);
        GroupMemberListActivity groupMemberListActivity = this.f1578a;
        return GroupMemberListActivity.a(arrayList, arrayList2, arrayList3);
    }

    /* access modifiers changed from: protected */
    public final void a() {
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        super.a(exc);
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void a(Object obj) {
        List list = (List) obj;
        if (list != null) {
            this.f1578a.a(list);
        } else {
            a("获取群组列表失败");
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.f1578a.i.i();
        this.f1578a.w();
    }
}
