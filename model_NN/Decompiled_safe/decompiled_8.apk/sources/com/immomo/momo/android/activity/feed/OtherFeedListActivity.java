package com.immomo.momo.android.activity.feed;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.b.a;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.a.cs;
import com.immomo.momo.android.activity.ah;
import com.immomo.momo.android.broadcast.d;
import com.immomo.momo.android.broadcast.f;
import com.immomo.momo.android.view.HeaderLayout;
import com.immomo.momo.android.view.LoadingButton;
import com.immomo.momo.android.view.MomoRefreshListView;
import com.immomo.momo.android.view.bi;
import com.immomo.momo.android.view.bl;
import com.immomo.momo.android.view.bu;
import com.immomo.momo.android.view.cv;
import com.immomo.momo.g;
import com.immomo.momo.service.ai;
import com.immomo.momo.service.aq;
import com.immomo.momo.service.bean.ab;
import com.immomo.momo.service.bean.bf;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class OtherFeedListActivity extends ah implements View.OnClickListener, AdapterView.OnItemClickListener, bl, bu, cv {
    /* access modifiers changed from: private */
    public MomoRefreshListView h = null;
    /* access modifiers changed from: private */
    public LoadingButton i;
    /* access modifiers changed from: private */
    public List j = null;
    /* access modifiers changed from: private */
    public Set k = new HashSet();
    private aq l = null;
    /* access modifiers changed from: private */
    public ai m = null;
    /* access modifiers changed from: private */
    public ao n = null;
    /* access modifiers changed from: private */
    public an o = null;
    /* access modifiers changed from: private */
    public cs p = null;
    private HeaderLayout q = null;
    /* access modifiers changed from: private */
    public String r;
    /* access modifiers changed from: private */
    public bf s;
    private f t;
    /* access modifiers changed from: private */
    public int u;
    /* access modifiers changed from: private */
    public int v;
    private d w;

    public OtherFeedListActivity() {
        new Handler();
        this.r = PoiTypeDef.All;
        this.s = null;
        this.t = null;
        this.u = 0;
        this.v = 0;
        this.w = new ak(this);
    }

    static /* synthetic */ ab n(OtherFeedListActivity otherFeedListActivity) {
        if (otherFeedListActivity.p.getCount() <= 0) {
            return null;
        }
        return (ab) otherFeedListActivity.p.getItem(otherFeedListActivity.p.getCount() - 1);
    }

    /* access modifiers changed from: private */
    public void w() {
        this.h.n();
        this.i.e();
        x();
    }

    private void x() {
        if (this.o != null && !this.o.isCancelled()) {
            this.o.cancel(true);
        }
        if (this.n != null && !this.n.isCancelled()) {
            this.n.cancel(true);
        }
    }

    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        super.a(bundle);
        setContentView((int) R.layout.activity_otherfeedlist);
        Intent intent = getIntent();
        if (intent != null) {
            String str = (String) intent.getExtras().get("other_momoid");
            this.r = str;
            if (!a.a((CharSequence) str)) {
                this.m = new ai();
                this.l = new aq();
            }
        }
        this.q = (HeaderLayout) findViewById(R.id.layout_header);
        this.h = (MomoRefreshListView) findViewById(R.id.listview);
        this.h.setEnableLoadMoreFoolter(true);
        this.h.setTimeEnable(false);
        this.h.setCompleteScrollTop(false);
        this.i = this.h.getFooterViewButton();
        this.h.addHeaderView(g.o().inflate((int) R.layout.listitem_blank, (ViewGroup) null));
        this.h.a(g.o().inflate((int) R.layout.include_feed_listempty, (ViewGroup) null));
        this.i.setOnProcessListener(this);
        this.h.setOnPullToRefreshListener$42b903f6(this);
        this.h.setOnCancelListener$135502(new am(this));
        d();
        this.t = new f(this);
        this.t.a(this.w);
    }

    public final void b_() {
        if (this.n != null && !this.n.isCancelled()) {
            this.n.cancel(true);
        }
        a(new ao(this, this)).execute(new Object[0]);
    }

    /* access modifiers changed from: protected */
    public final void d() {
        super.d();
        this.j = this.m.a(this.r, 20);
        this.s = this.l.b(this.r);
        MomoRefreshListView momoRefreshListView = this.h;
        cs csVar = new cs(this, this.j, this.h);
        this.p = csVar;
        momoRefreshListView.setAdapter((ListAdapter) csVar);
        for (ab add : this.j) {
            this.k.add(add);
        }
        this.u += this.p.getCount();
        if (this.j.size() < 20) {
            this.h.k();
        }
        this.q.setTitleText(String.valueOf(this.s == null ? this.r : this.s.h()) + "的动态");
        this.h.l();
        bi a2 = new bi(getApplicationContext()).a((int) R.drawable.ic_topbar_addfeed_white);
        if (this.f.h.equals(this.r)) {
            a2.setMarginRight(8);
            a2.setBackgroundResource(R.drawable.bg_header_submit);
            a(a2, new al(this));
            return;
        }
        a2.setVisibility(8);
    }

    public void onClick(View view) {
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        x();
        if (this.t != null) {
            unregisterReceiver(this.t);
            this.t = null;
        }
        super.onDestroy();
    }

    public void onItemClick(AdapterView adapterView, View view, int i2, long j2) {
    }

    /* access modifiers changed from: protected */
    public final void s() {
        if (this.p.getCount() < 20) {
            this.h.l();
        }
    }

    public final void u() {
        this.i.f();
        this.o = new an(this, this);
        this.o.execute(new Object[0]);
    }

    public final void v() {
        this.u = this.v;
        w();
    }
}
