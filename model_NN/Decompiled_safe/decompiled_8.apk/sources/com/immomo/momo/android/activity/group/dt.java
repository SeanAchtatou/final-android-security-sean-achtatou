package com.immomo.momo.android.activity.group;

import android.content.Context;
import com.immomo.momo.R;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.n;

final class dt extends d {

    /* renamed from: a  reason: collision with root package name */
    private v f1616a = null;
    private int c = 0;
    private /* synthetic */ GroupProfileActivity d;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public dt(GroupProfileActivity groupProfileActivity, Context context) {
        super(context);
        this.d = groupProfileActivity;
        this.f1616a = new v(context);
        this.f1616a.setOnCancelListener(new du(this));
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        n.a().a(this.d.l, this.c);
        this.b.a((Object) ("upload model: " + this.c));
        this.d.o.a(this.d.l, this.c);
        this.d.r.f2970a = this.c;
        return true;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
        this.d.a(this.f1616a);
        this.c = this.d.o.a(this.d.l) ? 0 : 1;
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        super.a(exc);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        Boolean bool = (Boolean) obj;
        super.a(bool);
        if (bool.booleanValue()) {
            a((int) R.string.group_setting_hide_sucess);
        } else {
            a((int) R.string.group_setting_quit_failed);
        }
        this.d.x();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        super.b();
        this.d.p();
    }
}
