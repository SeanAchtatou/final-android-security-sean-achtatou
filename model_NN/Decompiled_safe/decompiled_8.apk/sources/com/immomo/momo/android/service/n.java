package com.immomo.momo.android.service;

import android.os.Message;
import com.immomo.momo.android.b.z;
import com.immomo.momo.g;
import java.util.TimerTask;

final class n extends TimerTask {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ LService f2614a;

    n(LService lService) {
        this.f2614a = lService;
    }

    public final void run() {
        this.f2614a.d.a((Object) "lService timer start to run");
        if (g.q() != null && z.a(g.q().a())) {
            this.f2614a.f.sendMessage(new Message());
        }
    }
}
