package com.immomo.momo.android.service;

import android.graphics.Bitmap;
import com.immomo.momo.R;
import com.immomo.momo.a;
import com.immomo.momo.android.activity.fs;
import com.immomo.momo.b;
import com.immomo.momo.g;
import com.immomo.momo.protocol.a.p;
import com.immomo.momo.util.v;
import java.io.File;
import java.io.FileOutputStream;

final class i implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private String f2609a = null;
    private int b;

    public i(String str, int i) {
        int i2 = 11;
        this.f2609a = str;
        this.b = i != 11 ? 12 : i2;
    }

    public final void run() {
        try {
            v a2 = p.a(this.f2609a, this.b, (fs) null);
            if (a2 != null && a2.b != null) {
                Bitmap bitmap = a2.b;
                String str = this.b == 12 ? String.valueOf(this.f2609a) + "_l" : String.valueOf(this.f2609a) + "_s2";
                File file = new File(a.z(), String.valueOf(str) + ".png_");
                FileOutputStream fileOutputStream = new FileOutputStream(file);
                bitmap.compress(Bitmap.CompressFormat.PNG, 85, fileOutputStream);
                fileOutputStream.close();
                bitmap.recycle();
                b.a(str, g.a(file, (int) R.drawable.ic_userinfo_renren));
            }
        } catch (Throwable th) {
        }
    }
}
