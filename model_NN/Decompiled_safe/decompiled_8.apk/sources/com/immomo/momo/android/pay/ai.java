package com.immomo.momo.android.pay;

import com.immomo.momo.protocol.a.p;
import com.immomo.momo.service.bean.al;
import java.util.List;

public final class ai {
    private static final String d = (String.valueOf(p.c) + "/m/inc/images/vip/");

    /* renamed from: a  reason: collision with root package name */
    public int f2528a = 0;
    public String b;
    public List c;
    private al e;

    public final al a() {
        if (this.e == null || !this.e.getLoadImageId().equals(String.valueOf(d) + this.b)) {
            if (this.b != null) {
                this.e = new al(String.valueOf(d) + this.b);
                this.e.setImageUrl(true);
            } else {
                this.e = null;
            }
        }
        return this.e;
    }
}
