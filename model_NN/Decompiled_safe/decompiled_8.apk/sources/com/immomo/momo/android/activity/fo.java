package com.immomo.momo.android.activity;

import android.graphics.Bitmap;
import android.view.View;
import com.immomo.momo.android.view.photoview.PhotoView;

final class fo implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ fn f1497a;
    private final /* synthetic */ Bitmap b;
    private final /* synthetic */ boolean c;

    fo(fn fnVar, Bitmap bitmap, boolean z) {
        this.f1497a = fnVar;
        this.b = bitmap;
        this.c = z;
    }

    public final void run() {
        PhotoView photoView;
        if (!(this.b == null || this.f1497a.k == null || (photoView = (PhotoView) this.f1497a.k.get()) == null)) {
            photoView.setImageBitmap(this.b);
        }
        if (this.c) {
            View view = null;
            if (this.f1497a.l != null) {
                view = (View) this.f1497a.l.get();
            }
            if (view != null) {
                view.setVisibility(8);
            }
        }
    }
}
