package com.immomo.momo.android.activity;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.android.b.z;
import com.immomo.momo.android.view.HeaderLayout;
import com.immomo.momo.android.view.a.n;
import com.immomo.momo.g;

public class CheckStatusActivity extends ah implements View.OnClickListener {
    private HeaderLayout h = null;
    private TextView i;
    private TextView j;
    /* access modifiers changed from: private */
    public TextView k;
    private View l;
    /* access modifiers changed from: private */
    public View m;
    /* access modifiers changed from: private */
    public boolean n = true;
    /* access modifiers changed from: private */
    public boolean o = true;
    private boolean p = true;
    private boolean q = true;

    public CheckStatusActivity() {
        new bh();
    }

    static /* synthetic */ void a(CheckStatusActivity checkStatusActivity) {
        checkStatusActivity.j.setText(checkStatusActivity.n ? "数据服务器连接成功" : "数据服务器连接失败");
        checkStatusActivity.l.setVisibility(checkStatusActivity.n ? 4 : 0);
        checkStatusActivity.a(new bs(checkStatusActivity, checkStatusActivity)).execute(new Object[0]);
    }

    static /* synthetic */ void f(CheckStatusActivity checkStatusActivity) {
        Intent intent = new Intent();
        intent.setAction("android.settings.LOCATION_SOURCE_SETTINGS");
        intent.setFlags(268435456);
        try {
            checkStatusActivity.startActivity(intent);
        } catch (ActivityNotFoundException e) {
            checkStatusActivity.e.a((Throwable) e);
            intent.setAction("android.settings.SETTINGS");
            try {
                checkStatusActivity.startActivity(intent);
            } catch (Exception e2) {
                checkStatusActivity.e.a((Throwable) e2);
            }
        }
    }

    static /* synthetic */ void u() {
    }

    static /* synthetic */ void v() {
    }

    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        super.a(bundle);
        setContentView((int) R.layout.activity_setting_checkstatus);
        this.h = (HeaderLayout) findViewById(R.id.layout_header);
        this.h.setTitleText("网络检测");
        this.l = findViewById(R.id.layout_http_failed);
        this.m = findViewById(R.id.layout_xmpp_failed);
        findViewById(R.id.layout_gps_failed);
        findViewById(R.id.layout_network_failed);
        this.i = (TextView) findViewById(R.id.tv_phone_type);
        TextView textView = this.i;
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(g.U());
        stringBuffer.append("/");
        stringBuffer.append(g.T());
        textView.setText(stringBuffer.toString());
        this.j = (TextView) findViewById(R.id.text_http_failed);
        this.k = (TextView) findViewById(R.id.text_xmpp_failed);
        findViewById(R.id.text_gps_failed);
        findViewById(R.id.text_network_failed);
        findViewById(R.id.layout_http_connection).setOnClickListener(this);
        findViewById(R.id.layout_xmpp_connection).setOnClickListener(this);
        findViewById(R.id.layout_gps).setOnClickListener(this);
        findViewById(R.id.layout_network).setOnClickListener(this);
        d();
    }

    /* access modifiers changed from: protected */
    public final void d() {
        new bq(this, this).execute(new String[0]);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.layout_http_connection /*2131165868*/:
                if (!this.n) {
                    n nVar = new n(this);
                    nVar.setContentView((int) R.layout.dialog_locationfailed_http);
                    nVar.a();
                    nVar.setTitle("解决办法");
                    nVar.a(0, "进入网络设置", new bi(this));
                    nVar.a(1, getString(R.string.dialog_btn_cancel), new bj());
                    nVar.show();
                    return;
                }
                return;
            case R.id.layout_xmpp_connection /*2131165871*/:
                if (!this.o) {
                    n nVar2 = new n(this);
                    nVar2.setContentView((int) R.layout.dialog_locationfailed_xmpp);
                    nVar2.setTitle("解决办法");
                    nVar2.a();
                    nVar2.a(0, "进入网络设置", new bk(this));
                    nVar2.a(1, getString(R.string.dialog_btn_cancel), new bl());
                    nVar2.show();
                    return;
                }
                return;
            case R.id.layout_gps /*2131165874*/:
                if (!this.p) {
                    n nVar3 = new n(this);
                    nVar3.setContentView((int) R.layout.dialog_locationfailed_gps);
                    nVar3.setTitle("解决办法");
                    nVar3.a();
                    nVar3.a(0, "进入定位设置", new bm(this));
                    nVar3.a(1, getString(R.string.dialog_btn_cancel), new bn());
                    nVar3.show();
                    return;
                }
                return;
            case R.id.layout_network /*2131165877*/:
                if (!this.q) {
                    n nVar4 = new n(this);
                    nVar4.setContentView((int) R.layout.dialog_locationfailed_networklocation);
                    nVar4.setTitle("解决办法");
                    nVar4.a();
                    nVar4.a(0, "进入网络设置", new bo(this));
                    nVar4.a(1, getString(R.string.dialog_btn_cancel), new bp());
                    nVar4.show();
                    return;
                }
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        z.b("location_always_failed_gps");
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
    }
}
