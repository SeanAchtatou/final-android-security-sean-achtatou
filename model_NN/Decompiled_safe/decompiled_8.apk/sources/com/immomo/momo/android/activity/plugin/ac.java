package com.immomo.momo.android.activity.plugin;

import android.content.Context;
import com.immomo.momo.R;
import com.immomo.momo.a.a;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.util.ao;

final class ac extends d {

    /* renamed from: a  reason: collision with root package name */
    private v f2031a;
    private /* synthetic */ BindTXActivity c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ac(BindTXActivity bindTXActivity, Context context) {
        super(context);
        this.c = bindTXActivity;
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ Object a(Object... objArr) {
        return com.immomo.momo.protocol.a.d.a(this.c.f.h);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
        this.f2031a = new v(this.c);
        this.f2031a.a("请求提交中");
        this.f2031a.setCancelable(true);
        this.f2031a.setOnCancelListener(new ad(this));
        this.c.a(this.f2031a);
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        this.f2031a.dismiss();
        if (exc instanceof a) {
            ao.a((CharSequence) exc.getMessage());
        } else {
            ao.g(R.string.errormsg_server);
        }
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        String[] strArr = (String[]) obj;
        super.a((Object) strArr);
        this.c.p();
        this.c.n = strArr[0];
        this.c.o = strArr[1];
        if (com.immomo.a.a.f.a.a(this.c.n) || com.immomo.a.a.f.a.a(this.c.o)) {
            ao.b("绑定失败，请稍后再试");
        } else {
            this.c.j.post(new ab(this.c, "http://open.t.qq.com/cgi-bin/oauth2/authorize?client_id=" + this.c.n + "&response_type=code&redirect_uri=" + this.c.i));
        }
    }
}
