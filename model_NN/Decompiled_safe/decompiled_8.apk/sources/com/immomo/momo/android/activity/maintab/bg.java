package com.immomo.momo.android.activity.maintab;

import android.content.Context;
import android.support.v4.b.a;
import com.immomo.momo.R;
import com.immomo.momo.android.c.d;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.util.am;
import java.util.Date;
import java.util.List;

final class bg extends d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ aq f1856a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public bg(aq aqVar, Context context) {
        super(context);
        this.f1856a = aqVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.android.activity.maintab.aq.b(com.immomo.momo.android.activity.maintab.aq, boolean):java.util.List
     arg types: [com.immomo.momo.android.activity.maintab.aq, int]
     candidates:
      com.immomo.momo.android.activity.lh.b(int, android.view.KeyEvent):boolean
      android.support.v4.app.Fragment.b(android.view.LayoutInflater, android.view.ViewGroup):android.view.View
      com.immomo.momo.android.activity.maintab.aq.b(com.immomo.momo.android.activity.maintab.aq, boolean):java.util.List */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.android.activity.maintab.aq.a(com.immomo.momo.android.activity.maintab.aq, boolean):void
     arg types: [com.immomo.momo.android.activity.maintab.aq, int]
     candidates:
      com.immomo.momo.android.activity.maintab.aq.a(com.immomo.momo.android.activity.maintab.aq, double):void
      com.immomo.momo.android.activity.maintab.aq.a(com.immomo.momo.android.activity.maintab.aq, int):void
      com.immomo.momo.android.activity.maintab.aq.a(com.immomo.momo.android.activity.maintab.aq, com.immomo.momo.android.c.d):void
      com.immomo.momo.android.activity.maintab.aq.a(com.immomo.momo.android.activity.maintab.aq, java.util.Date):void
      com.immomo.momo.android.activity.lh.a(int, java.lang.String[]):com.immomo.momo.protocol.imjson.c.a
      com.immomo.momo.android.activity.lh.a(android.content.Context, com.immomo.momo.android.view.HeaderLayout):void
      com.immomo.momo.android.activity.lh.a(int, android.view.KeyEvent):boolean
      com.immomo.momo.android.activity.lh.a(android.os.Bundle, java.lang.String):boolean
      com.immomo.momo.android.activity.ar.a(android.view.LayoutInflater, android.view.ViewGroup):android.view.View
      android.support.v4.app.Fragment.a(android.content.Context, java.lang.String):android.support.v4.app.Fragment
      android.support.v4.app.Fragment.a(android.view.LayoutInflater, android.view.ViewGroup):android.view.View
      android.support.v4.app.Fragment.a(int, android.support.v4.app.Fragment):void
      android.support.v4.app.Fragment.a(android.content.Intent, int):void
      android.support.v4.app.Fragment.a(android.view.Menu, android.view.MenuInflater):boolean
      com.immomo.momo.android.activity.maintab.aq.a(com.immomo.momo.android.activity.maintab.aq, boolean):void */
    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        this.f1856a.W = this.f1856a.V;
        this.f1856a.V = 0;
        List<bf> b = aq.b(this.f1856a, true);
        if (b != null && !b.isEmpty()) {
            this.f1856a.S.h(b);
        }
        this.f1856a.Z = new Date();
        this.f1856a.N.c("lasttime_neayby_success", a.c(this.f1856a.Z));
        if (this.f1856a.aa) {
            this.f1856a.aa = false;
            this.f1856a.N.a("neayby_filter_timeline", Integer.valueOf(this.f1856a.N.o.ordinal()));
            this.f1856a.N.a("neayby_filter_gender", Integer.valueOf(this.f1856a.N.n.ordinal()));
            this.f1856a.N.a("neayby_filter_gender", Integer.valueOf(this.f1856a.N.n.ordinal()));
            this.f1856a.N.a("neayby_filter_timeline", Integer.valueOf(this.f1856a.N.o.ordinal()));
            this.f1856a.N.a("neayby_filter_bind", Integer.valueOf(this.f1856a.N.p.ordinal()));
            this.f1856a.N.a("neayby_filter_age", Integer.valueOf(this.f1856a.N.q));
            this.f1856a.N.a("neayby_filter_constellation", Integer.valueOf(this.f1856a.N.r));
            this.f1856a.N.a("neayby_filter_industry", this.f1856a.N.s);
        }
        this.f1856a.P.clear();
        for (bf bfVar : b) {
            this.f1856a.P.put(bfVar.h, bfVar);
        }
        return b;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        if (this.f1856a.Y != null && !this.f1856a.Y.isCancelled()) {
            this.f1856a.Y.cancel(true);
        }
        this.f1856a.U.g();
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        super.a(exc);
        this.f1856a.v();
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        this.f1856a.O.clear();
        this.f1856a.O.addAll((List) obj);
        this.f1856a.Q.notifyDataSetChanged();
        this.f1856a.Q();
        this.b.a((Object) ("MomoKit.getAudioManager().getRingerMode()=" + g.i().getRingerMode()));
        if (g.i().getRingerMode() == 2) {
            am.a().a(R.raw.ref_success);
        }
        this.f1856a.T.setLastFlushTime(this.f1856a.Z);
        this.f1856a.am();
        this.f1856a.ac.j();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.f1856a.X = (d) null;
    }
}
