package com.immomo.momo.android.a;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import com.immomo.momo.R;
import com.immomo.momo.service.aa;
import com.immomo.momo.service.bean.b.b;
import java.util.List;

public final class fs extends BaseAdapter {

    /* renamed from: a  reason: collision with root package name */
    private Context f826a = null;
    /* access modifiers changed from: private */
    public aa b = null;
    /* access modifiers changed from: private */
    public List c = null;
    private String d = null;
    private GridView e = null;
    /* access modifiers changed from: private */
    public Handler f = new ft(this);
    private int g = 80;

    public fs(Context context, String str, GridView gridView) {
        this.b = aa.a(context);
        this.f826a = context;
        this.d = str;
        this.e = gridView;
        aa aaVar = this.b;
        aa.a();
        this.c = this.b.a(this.d, new fu(this, (byte) 0));
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) this.f826a).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        this.g = (displayMetrics.widthPixels / 4) - 16;
    }

    public final void a(int i) {
        int firstVisiblePosition = this.e.getFirstVisiblePosition();
        if (i - firstVisiblePosition >= 0) {
            fy fyVar = (fy) this.e.getChildAt(i - firstVisiblePosition).getTag();
            ViewGroup.LayoutParams layoutParams = fyVar.f832a.getLayoutParams();
            layoutParams.height = this.g;
            layoutParams.width = this.g;
            fyVar.f832a.setLayoutParams(layoutParams);
            fyVar.b.setLayoutParams(layoutParams);
            fyVar.b.setVisibility(((b) this.c.get(i)).d ? 0 : 8);
        }
    }

    public final int getCount() {
        return this.c.size();
    }

    public final /* synthetic */ Object getItem(int i) {
        return (b) this.c.get(i);
    }

    public final long getItemId(int i) {
        return (long) i;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        fy fyVar;
        if (view == null) {
            view = LayoutInflater.from(this.f826a).inflate((int) R.layout.include_select_image, (ViewGroup) null);
            fyVar = new fy((byte) 0);
            fyVar.f832a = (ImageView) view.findViewById(R.id.image_select);
            fyVar.b = (ImageView) view.findViewById(R.id.image_select_flag);
            view.setTag(fyVar);
        } else {
            fyVar = (fy) view.getTag();
        }
        ViewGroup.LayoutParams layoutParams = fyVar.f832a.getLayoutParams();
        layoutParams.height = this.g;
        layoutParams.width = this.g;
        fyVar.f832a.setLayoutParams(layoutParams);
        fyVar.b.setLayoutParams(layoutParams);
        fyVar.b.setVisibility(((b) this.c.get(i)).d ? 0 : 8);
        new fv(this, new fw(this.f826a.getMainLooper(), fyVar.f832a), ((b) this.c.get(i)).b).start();
        return view;
    }
}
