package com.immomo.momo.android.view;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.g;

public final class bi extends LinearLayout {

    /* renamed from: a  reason: collision with root package name */
    private int f2743a = R.layout.common_headerbar_button;
    private CharSequence b = PoiTypeDef.All;
    private View c = null;
    private TextView d = null;
    private View e = null;
    private View f = null;

    public bi(Context context) {
        super(context);
        a();
    }

    public bi(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, com.immomo.momo.android.view.bi, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    private void a() {
        ((LayoutInflater) getContext().getSystemService("layout_inflater")).inflate(this.f2743a, (ViewGroup) this, true);
        this.f = findViewById(R.id.header_btn_container);
        this.c = findViewById(R.id.header_btn_img);
        this.e = findViewById(R.id.iv_rightline);
        this.d = (TextView) findViewById(R.id.header_btn_text);
    }

    public final bi a(int i) {
        if (i <= 0) {
            this.c.setVisibility(8);
        } else {
            this.c.setVisibility(0);
            this.c.setBackgroundResource(i);
        }
        return this;
    }

    public final bi a(CharSequence charSequence) {
        if (charSequence == null) {
            this.d.setVisibility(8);
        } else {
            this.b = charSequence;
            this.d.setVisibility(0);
            this.d.setText(this.b);
        }
        return this;
    }

    public final bi b(int i) {
        if (i <= 0) {
            this.d.setVisibility(8);
        } else {
            this.b = getContext().getString(i);
            this.d.setVisibility(0);
            this.d.setText(this.b);
        }
        return this;
    }

    public final Drawable getBackground() {
        return this.f.getBackground();
    }

    public final View getIconView() {
        return this.c;
    }

    public final TextView getLabelView() {
        return this.d;
    }

    public final CharSequence getText() {
        return this.b;
    }

    public final boolean performClick() {
        return this.f.performClick();
    }

    public final boolean performLongClick() {
        return this.f.performLongClick();
    }

    public final void setBackgroundDrawable(Drawable drawable) {
        int paddingLeft = this.f.getPaddingLeft();
        int paddingTop = this.f.getPaddingTop();
        int paddingRight = this.f.getPaddingRight();
        int paddingBottom = this.f.getPaddingBottom();
        this.f.setBackgroundDrawable(drawable);
        this.f.setPadding(paddingLeft, paddingTop, paddingRight, paddingBottom);
    }

    public final void setBackgroundResource(int i) {
        int paddingLeft = this.f.getPaddingLeft();
        int paddingTop = this.f.getPaddingTop();
        int paddingRight = this.f.getPaddingRight();
        int paddingBottom = this.f.getPaddingBottom();
        this.f.setBackgroundResource(i);
        this.f.setPadding(paddingLeft, paddingTop, paddingRight, paddingBottom);
    }

    public final void setEnabled(boolean z) {
        this.f.setEnabled(z);
    }

    public final void setMarginLeft(int i) {
        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) this.f.getLayoutParams();
        layoutParams.leftMargin = g.a((float) i);
        this.f.setLayoutParams(layoutParams);
    }

    public final void setMarginRight(int i) {
        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) this.f.getLayoutParams();
        layoutParams.rightMargin = g.a((float) i);
        this.f.setLayoutParams(layoutParams);
    }

    public final void setOnClickListener(View.OnClickListener onClickListener) {
        if (this.f != null) {
            this.f.setOnClickListener(onClickListener);
        }
    }

    public final void setRightLineVisible(boolean z) {
        this.e.setVisibility(z ? 0 : 8);
    }

    public final void setSelected(boolean z) {
        this.f.setSelected(z);
    }
}
