package com.immomo.momo.android.activity.tieba;

import android.content.Context;
import android.support.v4.b.a;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.service.bean.c.b;
import com.immomo.momo.util.h;
import java.io.File;

final class cn extends d {

    /* renamed from: a  reason: collision with root package name */
    private v f2215a = null;
    private b c;
    private boolean d = false;
    private boolean e = false;
    private boolean f = false;
    private boolean g = false;
    /* access modifiers changed from: private */
    public /* synthetic */ TieDetailActivity h;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public cn(TieDetailActivity tieDetailActivity, Context context, b bVar, boolean z, boolean z2, boolean z3, boolean z4) {
        super(context);
        this.h = tieDetailActivity;
        this.c = bVar;
        this.d = z;
        this.e = z2;
        this.f = z3;
        this.g = z4;
        this.f2215a = new v(context);
        this.f2215a.a("请求提交中");
        this.f2215a.setCancelable(true);
        this.f2215a.setOnCancelListener(new co(this));
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ Object a(Object... objArr) {
        return com.immomo.momo.protocol.a.v.a().a(this.c.f3017a, this.d, this.e, this.f, this.g);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.h.a(this.f2215a);
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        super.a(exc);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        String str = (String) obj;
        this.h.b((CharSequence) "话题分享成功");
        if (this.g) {
            File file = null;
            if (this.c.c() != null && this.c.c().length > 0) {
                file = h.a(this.c.c()[0], 15);
            } else if (this.c.b != null && !a.a((CharSequence) this.c.b.getLoadImageId())) {
                file = h.a(this.c.b.getLoadImageId(), 3);
            }
            com.immomo.momo.plugin.g.a.a().a(str, String.format(this.h.T, this.c.d), file);
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.h.p();
    }
}
