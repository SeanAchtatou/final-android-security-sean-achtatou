package com.immomo.momo.android.activity.plugin;

import android.os.Bundle;
import android.webkit.WebView;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.ah;
import com.immomo.momo.android.view.HeaderLayout;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.util.ao;

public class BindRenrenActivity extends ah {
    private HeaderLayout h = null;
    /* access modifiers changed from: private */
    public String i = "http://widget.renren.com/callback.html";
    /* access modifiers changed from: private */
    public WebView j = null;
    private TextView k = null;
    /* access modifiers changed from: private */
    public v l = null;
    /* access modifiers changed from: private */
    public q m;
    private boolean n = true;
    private String o = null;

    static /* synthetic */ void e(BindRenrenActivity bindRenrenActivity) {
        if (bindRenrenActivity.n) {
            ao.a((CharSequence) "绑定成功");
        }
        bindRenrenActivity.setResult(-1, bindRenrenActivity.getIntent());
        bindRenrenActivity.finish();
    }

    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        super.a(bundle);
        setContentView((int) R.layout.activity_bind_api);
        this.j = (WebView) findViewById(R.id.web);
        this.j.getSettings().setJavaScriptEnabled(true);
        this.h = (HeaderLayout) findViewById(R.id.layout_header);
        this.h.setTitleText("绑定人人账号");
        this.k = (TextView) findViewById(R.id.header_stv_title);
        this.k.setFocusableInTouchMode(false);
        this.j.setWebViewClient(new u(this, (byte) 0));
        d();
    }

    /* access modifiers changed from: protected */
    public final void d() {
        this.n = getIntent().getBooleanExtra("show_toast", true);
        this.o = getIntent().getStringExtra("value_key");
        getIntent().getStringExtra("value_key");
        this.j.setVisibility(0);
        this.j.post(new p(this, "https://graph.renren.com/oauth/authorize?client_id=" + this.o + "&scope=read_user_status read_user_photo read_user_album status_update operate_like photo_upload&display=touch&response_type=code&redirect_uri=" + this.i));
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.l = null;
        if (this.m != null && this.m.isCancelled()) {
            this.m.cancel(true);
        }
        super.onDestroy();
    }
}
