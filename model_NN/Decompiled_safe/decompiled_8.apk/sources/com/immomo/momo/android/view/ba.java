package com.immomo.momo.android.view;

import android.widget.AbsListView;
import android.widget.BaseExpandableListAdapter;

final class ba implements AbsListView.OnScrollListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ az f2735a;

    ba(az azVar) {
        this.f2735a = azVar;
    }

    public final void onScroll(AbsListView absListView, int i, int i2, int i3) {
        if (this.f2735a.k != null) {
            this.f2735a.k.onScroll(absListView, i, i2, i3);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.android.view.az.a(com.immomo.momo.android.view.az, boolean):void
     arg types: [com.immomo.momo.android.view.az, int]
     candidates:
      com.immomo.momo.android.view.az.a(com.immomo.momo.android.view.az, float):void
      com.immomo.momo.android.view.az.a(com.immomo.momo.android.view.az, boolean):void */
    public final void onScrollStateChanged(AbsListView absListView, int i) {
        if (this.f2735a.k != null) {
            this.f2735a.k.onScrollStateChanged(absListView, i);
        }
        switch (i) {
            case 0:
                this.f2735a.g = false;
                if (this.f2735a.a() && this.f2735a.j != null && (this.f2735a.j instanceof BaseExpandableListAdapter)) {
                    ((BaseExpandableListAdapter) this.f2735a.j).notifyDataSetChanged();
                    return;
                }
                return;
            case 1:
                this.f2735a.g = true;
                return;
            case 2:
                this.f2735a.g = true;
                return;
            default:
                return;
        }
    }
}
