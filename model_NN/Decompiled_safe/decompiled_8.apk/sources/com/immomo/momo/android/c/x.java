package com.immomo.momo.android.c;

import android.content.Context;
import android.content.Intent;
import com.immomo.momo.android.broadcast.j;
import com.immomo.momo.service.aq;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.util.m;

public class x extends d {

    /* renamed from: a  reason: collision with root package name */
    protected Context f2396a;
    protected bf c;
    protected aq d;
    protected m e;
    protected z f = new y();
    private bf g;

    public x(Context context, bf bfVar, bf bfVar2, z zVar) {
        super(context);
        this.f2396a = context;
        this.g = bfVar;
        this.c = bfVar2;
        this.f = zVar;
        this.d = new aq();
        this.e = new m("BlockTask");
    }

    /* access modifiers changed from: protected */
    public Object a(Object... objArr) {
        return null;
    }

    /* access modifiers changed from: protected */
    public final void c() {
        bf j = this.d.j(this.c.h);
        if (j != null) {
            this.d.i(j.h);
            if (this.g.x > 0) {
                bf bfVar = this.g;
                bfVar.x--;
                this.d.b(this.g);
            }
            Intent intent = new Intent(j.b);
            intent.putExtra("key_momoid", this.c.h);
            intent.putExtra("newfollower", this.g.v);
            intent.putExtra("followercount", this.g.w);
            intent.putExtra("total_friends", this.g.x);
            this.f2396a.sendBroadcast(intent);
        }
    }

    /* access modifiers changed from: protected */
    public final void d() {
        aq aqVar = this.d;
        String str = this.c.h;
        bf bfVar = null;
        if (aqVar.o(str)) {
            bf bfVar2 = new bf(str);
            bfVar = aqVar.b(str);
            if (bfVar == null) {
                bfVar = bfVar2;
            }
        }
        if (bfVar != null) {
            this.d.l(bfVar.h);
            if (this.g.w > 0) {
                bf bfVar3 = this.g;
                bfVar3.w--;
                this.d.b(this.g);
            }
        }
        Intent intent = new Intent(j.c);
        intent.putExtra("key_momoid", this.c.h);
        intent.putExtra("newfollower", this.g.v);
        intent.putExtra("followercount", this.g.w);
        intent.putExtra("total_friends", this.g.x);
        this.f2396a.sendBroadcast(intent);
    }
}
