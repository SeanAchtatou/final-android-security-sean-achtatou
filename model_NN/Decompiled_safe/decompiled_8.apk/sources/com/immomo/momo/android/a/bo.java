package com.immomo.momo.android.a;

import com.immomo.momo.g;
import com.immomo.momo.protocol.a.i;
import com.immomo.momo.service.bean.r;
import java.io.File;

final class bo implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ bm f740a;
    private final /* synthetic */ r b;
    private final /* synthetic */ String c;
    private final /* synthetic */ File d;

    bo(bm bmVar, r rVar, String str, File file) {
        this.f740a = bmVar;
        this.b = rVar;
        this.c = str;
        this.d = file;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.protocol.a.i.a(java.lang.String, java.lang.String, java.io.File, boolean):void
     arg types: [java.lang.String, java.lang.String, java.io.File, int]
     candidates:
      com.immomo.momo.protocol.a.p.a(java.lang.String, java.util.Map, com.immomo.momo.protocol.a.l[], java.util.Map):java.lang.String
      com.immomo.momo.protocol.a.i.a(java.lang.String, java.lang.String, java.io.File, boolean):void */
    public final void run() {
        try {
            i.a().a(this.b.d(), String.valueOf(this.c) + ".png", this.d, true);
            this.b.setImageLoading(false);
            g.d().c().post(new bp(this));
        } catch (Exception e) {
        }
    }
}
