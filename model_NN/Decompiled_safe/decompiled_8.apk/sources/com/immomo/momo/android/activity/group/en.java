package com.immomo.momo.android.activity.group;

import com.immomo.momo.android.c.g;
import com.immomo.momo.android.view.MGifImageView;
import java.io.File;

final class en implements g {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ PublishGroupFeedActivity f1635a;
    private final /* synthetic */ MGifImageView b;
    private final /* synthetic */ boolean c;

    en(PublishGroupFeedActivity publishGroupFeedActivity, MGifImageView mGifImageView, boolean z) {
        this.f1635a = publishGroupFeedActivity;
        this.b = mGifImageView;
        this.c = z;
    }

    public final /* synthetic */ void a(Object obj) {
        this.b.post(new eo(this, (File) obj, this.c, this.b));
    }
}
