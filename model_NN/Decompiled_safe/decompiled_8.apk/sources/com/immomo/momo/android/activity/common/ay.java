package com.immomo.momo.android.activity.common;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import com.immomo.momo.R;
import com.immomo.momo.android.a.c;
import com.immomo.momo.android.activity.lh;
import com.immomo.momo.android.broadcast.d;
import com.immomo.momo.android.broadcast.v;
import com.immomo.momo.android.view.HeaderLayout;
import com.immomo.momo.android.view.RefreshOnOverScrollListView;
import com.immomo.momo.service.ag;
import com.immomo.momo.service.bean.bf;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ay extends lh implements AdapterView.OnItemClickListener, l {
    /* access modifiers changed from: private */
    public RefreshOnOverScrollListView O;
    /* access modifiers changed from: private */
    public c P;
    private v Q;
    /* access modifiers changed from: private */
    public List R = new ArrayList();
    private d S = new az(this);

    static /* synthetic */ j c(ay ayVar) {
        return (j) ayVar.c();
    }

    /* access modifiers changed from: protected */
    public final int B() {
        return R.layout.activity_select_recentcontact;
    }

    /* access modifiers changed from: protected */
    public final void C() {
        this.O = (RefreshOnOverScrollListView) c((int) R.id.listview);
    }

    /* access modifiers changed from: protected */
    public final void E() {
        super.E();
        this.P.a(true);
    }

    public final void O() {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.R.size()) {
                this.P.notifyDataSetChanged();
                return;
            }
            bf bfVar = (bf) this.R.get(i2);
            if (((j) c()).D().containsKey(bfVar.h)) {
                if (!this.P.b(bfVar)) {
                    this.P.a(bfVar);
                }
            } else if (this.P.b(bfVar)) {
                this.P.a(bfVar);
            }
            i = i2 + 1;
        }
    }

    public final void S() {
        this.L.b((Object) "RecentContactHandler onFirstResume");
        super.S();
    }

    public final void a(Context context, HeaderLayout headerLayout) {
    }

    public final void ai() {
        this.O.i();
    }

    /* access modifiers changed from: protected */
    public final void g(Bundle bundle) {
        this.Q = new v(c());
        this.Q.a(this.S);
        this.R.clear();
        this.R.addAll(new ag().c());
        for (Map.Entry value : ((j) c()).E().entrySet()) {
            bf bfVar = (bf) value.getValue();
            if (bfVar != null && this.R.contains(bfVar)) {
                this.R.remove(bfVar);
            }
        }
        this.P = new c(c(), this.R, this.O, ((j) c()).y());
        this.O.setAdapter((ListAdapter) this.P);
        if (this.P.getCount() <= 0) {
            ((j) c()).c(1);
        }
        this.O.setOnItemClickListener(this);
    }

    public final void m() {
        super.m();
    }

    public void onItemClick(AdapterView adapterView, View view, int i, long j) {
        if (((j) c()).y()) {
            ((j) c()).a(this.P.getItem(i).h, 0);
        } else if (this.P.b(this.P.getItem(i)) || ((j) c()).D().size() + 1 < ((j) c()).z()) {
            if (this.P.a(this.P.getItem(i))) {
                ((j) c()).a(this.P.getItem(i));
            } else {
                ((j) c()).b(this.P.getItem(i));
            }
            this.P.notifyDataSetChanged();
            ((j) c()).a(((j) c()).D().size(), ((j) c()).z());
        } else {
            a(((j) c()).A());
        }
    }

    public final void p() {
        super.p();
        if (this.Q != null) {
            c().unregisterReceiver(this.Q);
            this.Q = null;
        }
    }
}
