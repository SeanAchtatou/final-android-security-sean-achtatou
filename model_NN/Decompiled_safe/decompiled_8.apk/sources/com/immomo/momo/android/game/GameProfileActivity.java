package com.immomo.momo.android.game;

import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.b.a;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.ah;
import com.immomo.momo.android.activity.d;
import com.immomo.momo.service.bean.GameApp;
import com.immomo.momo.service.bean.aj;
import com.immomo.momo.service.bean.al;
import com.immomo.momo.service.u;
import com.immomo.momo.util.g;
import com.immomo.momo.util.j;
import java.util.List;

public class GameProfileActivity extends ah implements View.OnClickListener {
    private String h;
    /* access modifiers changed from: private */
    public u i;
    /* access modifiers changed from: private */
    public GameApp j;
    private TextView k;
    private TextView l;
    private ImageView m;
    private Button n;
    private Button o;
    private boolean p;
    /* access modifiers changed from: private */
    public boolean q;
    private ImageView[] r;
    private TextView s;
    private TextView t;
    private TextView u;
    private TextView v;
    private TextView w;
    private TextView x;
    private TextView y;
    /* access modifiers changed from: private */
    public int z = 0;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, int, boolean, boolean):void
     arg types: [com.immomo.momo.service.bean.al, android.widget.ImageView, int, int, int]
     candidates:
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup, int, boolean):void
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, int, boolean, boolean):void */
    /* access modifiers changed from: private */
    public void u() {
        List<ResolveInfo> queryIntentActivities;
        boolean z2 = false;
        setTitle(this.j.appname);
        this.k.setText(this.j.appname);
        this.l.setText(this.j.mcount);
        j.a(this.j.appIconLoader(), this.m, 18);
        this.p = false;
        if (!a.a((CharSequence) this.j.appURI) && (queryIntentActivities = getPackageManager().queryIntentActivities(new Intent("android.intent.action.VIEW", Uri.parse(this.j.appURI)), 32)) != null && !queryIntentActivities.isEmpty()) {
            this.p = true;
        }
        if (this.p) {
            this.n.setEnabled(true);
            this.n.setText("打开");
        } else {
            this.n.setText("下载");
            this.n.setEnabled(!a.a(this.j.appdownload));
        }
        String[] strArr = this.j.picArray;
        for (int i2 = 0; i2 < this.r.length; i2++) {
            if (strArr == null || i2 >= strArr.length) {
                this.r[i2].setVisibility(8);
            } else {
                this.r[i2].setVisibility(0);
                j.a((aj) new al(this.j.picArray[i2], true), this.r[i2], 18, false, false);
            }
        }
        if (a.a((CharSequence) this.j.eventNotice)) {
            this.s.setVisibility(8);
        } else {
            this.s.setVisibility(0);
            this.s.setText(this.j.eventNotice);
        }
        this.t.setText(this.j.appdesc);
        this.v.setText(this.j.developers);
        this.x.setText(this.j.size);
        this.w.setText(this.j.updateTimeString);
        this.y.setText(this.j.updateNotice);
        this.u.setText(this.j.versionName);
        Button button = this.o;
        if (!a.a((CharSequence) this.j.buttonLabel)) {
            z2 = true;
        }
        button.setEnabled(z2);
        this.o.setText(this.j.buttonLabel);
    }

    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        super.a(bundle);
        setContentView((int) R.layout.activity_gameprofile);
        if (d.e(l())) {
            this.z = 2;
        } else if (d.i(l())) {
            this.z = 5;
        } else if (d.f(l())) {
            this.z = 1;
        } else if (d.g(l())) {
            this.z = 3;
        } else if (d.h(l())) {
            this.z = 4;
        } else {
            this.z = 0;
        }
        this.e.a((Object) ("referer=" + this.z));
        this.k = (TextView) findViewById(R.id.gameprofile_tv_appname);
        this.l = (TextView) findViewById(R.id.gameprofile_tv_mcount);
        this.m = (ImageView) findViewById(R.id.gameprofile_iv_appicon);
        this.n = (Button) findViewById(R.id.gameprofile_btn_open);
        this.o = (Button) findViewById(R.id.gameprofile_btn_nearby);
        this.r = new ImageView[5];
        this.r[0] = (ImageView) findViewById(R.id.gameprofile_ic_cover_1);
        this.r[1] = (ImageView) findViewById(R.id.gameprofile_ic_cover_2);
        this.r[2] = (ImageView) findViewById(R.id.gameprofile_ic_cover_3);
        this.r[3] = (ImageView) findViewById(R.id.gameprofile_ic_cover_4);
        this.r[4] = (ImageView) findViewById(R.id.gameprofile_ic_cover_5);
        this.s = (TextView) findViewById(R.id.gameprofile_tv_eventnotice);
        this.t = (TextView) findViewById(R.id.gameprofile_tv_appdesc);
        this.v = (TextView) findViewById(R.id.gameprofile_tv_developer);
        this.w = (TextView) findViewById(R.id.gameprofile_tv_updatetime);
        this.x = (TextView) findViewById(R.id.gameprofile_tv_size);
        this.y = (TextView) findViewById(R.id.gameprofile_tv_updatedesc);
        this.u = (TextView) findViewById(R.id.gameprofile_tv_versionname);
        this.n.setOnClickListener(this);
        this.o.setOnClickListener(this);
        d();
    }

    /* access modifiers changed from: protected */
    public final void d() {
        this.i = new u();
        this.h = getIntent().getStringExtra("appid");
        this.j = this.i.a(this.h);
        if (this.j == null) {
            this.j = new GameApp();
            this.j.appid = this.h;
        } else {
            this.q = true;
            u();
        }
        b(new e(this, this));
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.gameprofile_btn_open /*2131165642*/:
                if (this.p) {
                    Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(this.j.appURI));
                    intent.addFlags(268435456);
                    startActivity(Intent.createChooser(intent, this.j.appname == null ? "游戏" : this.j.appname));
                    com.immomo.momo.android.c.u.b().execute(new d(this));
                    return;
                }
                this.e.a((Object) ("gameApp.appdownload=" + this.j.appdownload));
                if (g.a(getApplicationContext(), this.j.appdownload, this.j.appname == null ? "游戏" : this.j.appname, "application/vnd.android.package-archive")) {
                    a((CharSequence) "下载中...");
                } else {
                    a((CharSequence) "游戏数据有误，下载失败");
                }
                com.immomo.momo.android.c.u.b().execute(new c(this));
                return;
            case R.id.gameprofile_btn_nearby /*2131165655*/:
                Intent intent2 = new Intent(getApplicationContext(), NearbyPlayersActivity.class);
                intent2.putExtra("appid", this.h);
                intent2.putExtra("title", this.j.buttonLabel);
                startActivity(intent2);
                return;
            default:
                return;
        }
    }
}
