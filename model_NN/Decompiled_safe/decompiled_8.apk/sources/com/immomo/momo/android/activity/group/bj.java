package com.immomo.momo.android.activity.group;

import android.content.Context;
import com.immomo.momo.android.c.b;
import com.immomo.momo.protocol.a.o;
import com.immomo.momo.service.bean.a.e;
import com.immomo.momo.service.bean.a.f;

final class bj extends b {
    private f c;
    private /* synthetic */ GroupFeedProfileActivity d;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public bj(GroupFeedProfileActivity groupFeedProfileActivity, Context context, f fVar) {
        super(context);
        this.d = groupFeedProfileActivity;
        this.c = fVar;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        return o.a().d(this.c.i);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        String str = (String) obj;
        a(str);
        this.d.l.c(this.c);
        GroupFeedProfileActivity groupFeedProfileActivity = this.d;
        e e = this.d.k;
        int i = e.e - 1;
        e.e = i;
        GroupFeedProfileActivity.a(groupFeedProfileActivity, i);
        this.d.m.e(this.c.i);
        super.a((Object) str);
    }
}
