package com.immomo.momo.android.view;

import android.view.GestureDetector;
import android.view.MotionEvent;

final class bg extends GestureDetector.SimpleOnGestureListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ HandyListView f2741a;

    private bg(HandyListView handyListView) {
        this.f2741a = handyListView;
    }

    /* synthetic */ bg(HandyListView handyListView, byte b) {
        this(handyListView);
    }

    public final boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
        this.f2741a.o = f2;
        return false;
    }
}
