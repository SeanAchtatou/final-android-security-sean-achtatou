package com.immomo.momo.android.activity.contacts;

import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.ExpandableListView;
import com.immomo.momo.android.activity.OtherProfileActivity;
import com.immomo.momo.service.bean.e;
import com.immomo.momo.service.bean.f;

final class p implements ExpandableListView.OnChildClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ CommunityPeopleActivity f1195a;

    p(CommunityPeopleActivity communityPeopleActivity) {
        this.f1195a = communityPeopleActivity;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public final boolean onChildClick(ExpandableListView expandableListView, View view, int i, int i2, long j) {
        e eVar = (e) ((f) this.f1195a.m.get(i)).b.get(i2);
        switch (eVar.b) {
            case 1:
                if (view instanceof Button) {
                    if (this.f1195a.j != 11) {
                        if (this.f1195a.j == 22) {
                            this.f1195a.b(new v(this.f1195a, this.f1195a, i, i2));
                            break;
                        }
                    } else {
                        CommunityPeopleActivity.a(this.f1195a, i, i2);
                        break;
                    }
                }
                CommunityPeopleActivity.a(this.f1195a, eVar);
                break;
            case 2:
                if (view instanceof Button) {
                    this.f1195a.b(new v(this.f1195a, this.f1195a, i, i2));
                    break;
                }
                CommunityPeopleActivity.a(this.f1195a, eVar);
                break;
            case 3:
                Intent intent = new Intent(this.f1195a, OtherProfileActivity.class);
                intent.putExtra("momoid", ((e) ((f) this.f1195a.m.get(i)).b.get(i2)).f3023a);
                intent.putExtra("tag", "local");
                this.f1195a.startActivity(intent);
                break;
            default:
                CommunityPeopleActivity.a(this.f1195a, eVar);
                break;
        }
        return true;
    }
}
