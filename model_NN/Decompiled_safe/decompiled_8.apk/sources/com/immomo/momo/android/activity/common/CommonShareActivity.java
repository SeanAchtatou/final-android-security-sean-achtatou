package com.immomo.momo.android.activity.common;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.b.a;
import android.view.View;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.WelcomeActivity;
import com.immomo.momo.android.activity.lh;
import com.immomo.momo.android.view.a.n;
import com.immomo.momo.g;
import com.immomo.momo.service.aq;
import com.immomo.momo.service.h;
import com.immomo.momo.service.y;
import com.immomo.momo.util.ao;

public class CommonShareActivity extends j {
    private static String i = (String.valueOf(g.h()) + "share_has_group");
    private int j = 1;
    private boolean k = true;
    /* access modifiers changed from: private */
    public String l;
    /* access modifiers changed from: private */
    public int m;
    private aq n;
    private y o;
    private h p;

    static /* synthetic */ void a(CommonShareActivity commonShareActivity, String str, int i2) {
        Uri data = commonShareActivity.getIntent().getData();
        if (data != null) {
            commonShareActivity.a(new ab(commonShareActivity, commonShareActivity, i2, data, str)).execute(new Object[0]);
        } else {
            commonShareActivity.a(new z(commonShareActivity, commonShareActivity, i2, str)).execute(new Object[0]);
        }
    }

    /* access modifiers changed from: protected */
    public final String A() {
        return PoiTypeDef.All;
    }

    /* access modifiers changed from: protected */
    public final void B() {
        if (this.f == null) {
            ao.e(R.string.feed_publish_dialog_content_unlogin);
            Intent intent = new Intent(getApplicationContext(), WelcomeActivity.class);
            intent.addFlags(268435456);
            getApplicationContext().startActivity(intent);
            finish();
        }
        Bundle extras = getIntent().getExtras();
        if (extras == null) {
            finish();
            return;
        }
        if (extras.containsKey(i)) {
            this.k = extras.getBoolean(i);
        }
        if (extras.containsKey("from_id")) {
            this.l = extras.getString("from_id");
        }
        if (extras.containsKey("from_type")) {
            this.m = extras.getInt("from_type");
        }
        this.n = new aq();
        this.o = new y();
        this.p = new h();
    }

    /* access modifiers changed from: protected */
    public final void C() {
        int i2 = 0;
        if (this.k) {
            this.j = 2;
            a(ay.class, a.class, ba.class);
            findViewById(R.id.contact_tab_group).setVisibility(0);
        } else {
            this.j = 1;
            a(ay.class, a.class);
            findViewById(R.id.contact_tab_group).setVisibility(8);
        }
        findViewById(R.id.contact_tab_both).setOnClickListener(this);
        findViewById(R.id.contact_tab_follows).setOnClickListener(this);
        findViewById(R.id.contact_tab_group).setOnClickListener(this);
        int intExtra = getIntent().getIntExtra("showindex", 0);
        if (intExtra >= 0) {
            i2 = intExtra > this.j ? this.j : intExtra;
        }
        c(i2);
    }

    /* access modifiers changed from: protected */
    public final void a(int i2, int i3) {
    }

    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        super.a(bundle);
        x();
        u();
    }

    /* access modifiers changed from: protected */
    public final void a(Fragment fragment, int i2) {
        switch (i2) {
            case 0:
                findViewById(R.id.contact_tab_both).setSelected(true);
                findViewById(R.id.contact_tab_follows).setSelected(false);
                findViewById(R.id.contact_tab_group).setSelected(false);
                break;
            case 1:
                findViewById(R.id.contact_tab_both).setSelected(false);
                findViewById(R.id.contact_tab_follows).setSelected(true);
                findViewById(R.id.contact_tab_group).setSelected(false);
                break;
            case 2:
                findViewById(R.id.contact_tab_both).setSelected(false);
                findViewById(R.id.contact_tab_follows).setSelected(false);
                findViewById(R.id.contact_tab_group).setSelected(true);
                break;
        }
        ((lh) fragment).a(this, m());
        a(fragment);
    }

    /* access modifiers changed from: protected */
    public final void a(String str, int i2) {
        if (!a.a((CharSequence) str)) {
            String str2 = PoiTypeDef.All;
            switch (i2) {
                case 0:
                    if (this.n.b(str) != null) {
                        str2 = this.n.b(str).h();
                        break;
                    }
                    break;
                case 1:
                    str2 = this.o.f(str);
                    break;
                case 2:
                    str2 = this.p.a(str);
                    break;
            }
            n nVar = new n(this);
            nVar.setTitle("提示");
            nVar.a();
            nVar.a("将内容分享给:" + str2 + "?");
            nVar.a(0, (int) R.string.dialog_btn_confim, new x(this, str, i2));
            nVar.a(1, (int) R.string.dialog_btn_cancel, new y());
            nVar.show();
            return;
        }
        a((CharSequence) "分享参数错误");
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.contact_tab_both /*2131165444*/:
                c(0);
                return;
            case R.id.contact_tab_follows /*2131165445*/:
                c(1);
                return;
            case R.id.contact_tab_fans /*2131165446*/:
            default:
                return;
            case R.id.contact_tab_group /*2131165447*/:
                c(2);
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
    }

    /* access modifiers changed from: protected */
    public final void x() {
        super.x();
        switch (this.m) {
            case 1:
                setTitle("分享话题");
                return;
            case 2:
                setTitle("分享活动");
                return;
            case 3:
                setTitle("分享陌陌吧");
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public final boolean y() {
        return true;
    }

    /* access modifiers changed from: protected */
    public final int z() {
        return 1;
    }
}
