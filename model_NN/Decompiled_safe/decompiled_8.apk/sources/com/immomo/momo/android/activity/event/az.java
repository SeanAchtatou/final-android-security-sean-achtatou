package com.immomo.momo.android.activity.event;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import com.immomo.momo.service.bean.u;

final class az implements AdapterView.OnItemClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ HistoryEventListActivity f1369a;

    az(HistoryEventListActivity historyEventListActivity) {
        this.f1369a = historyEventListActivity;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        Intent intent = new Intent(this.f1369a, EventProfileActivity.class);
        intent.putExtra("eventid", ((u) this.f1369a.i.getItem(i)).f3038a);
        this.f1369a.startActivity(intent);
    }
}
