package com.immomo.momo.android.c;

import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.util.m;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadPoolExecutor;

final class v implements RejectedExecutionHandler {
    private v() {
    }

    /* synthetic */ v(byte b) {
        this();
    }

    public final void rejectedExecution(Runnable runnable, ThreadPoolExecutor threadPoolExecutor) {
        m mVar = new m(this);
        String str = PoiTypeDef.All;
        if (runnable instanceof o) {
            o oVar = (o) runnable;
            str = oVar.b();
            oVar.a((Object) null);
        }
        mVar.a((Object) ("rejectedExecution, 一个线程被取消, " + str));
    }
}
