package com.immomo.momo.android.activity;

import android.content.DialogInterface;

final class ls implements DialogInterface.OnDismissListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ UserGuideActivity f1818a;

    ls(UserGuideActivity userGuideActivity) {
        this.f1818a = userGuideActivity;
    }

    public final void onDismiss(DialogInterface dialogInterface) {
        this.f1818a.finish();
    }
}
