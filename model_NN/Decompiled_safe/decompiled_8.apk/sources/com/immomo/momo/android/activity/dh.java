package com.immomo.momo.android.activity;

import android.content.Intent;
import android.support.v4.b.a;
import com.immomo.momo.android.broadcast.d;
import com.immomo.momo.android.broadcast.w;
import com.immomo.momo.service.bean.bf;

final class dh implements d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ EditVipProfileActivity f1222a;

    dh(EditVipProfileActivity editVipProfileActivity) {
        this.f1222a = editVipProfileActivity;
    }

    public final void a(Intent intent) {
        bf b;
        if (w.f2366a.equals(intent.getAction())) {
            String stringExtra = intent.getStringExtra("momoid");
            if (!a.a((CharSequence) stringExtra) && this.f1222a.j.h.equals(stringExtra) && (b = this.f1222a.n.b(this.f1222a.f.h)) != null) {
                this.f1222a.j.ae = b.ae;
                this.f1222a.v();
            }
        }
    }
}
