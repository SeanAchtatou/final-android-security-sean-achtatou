package com.immomo.momo.android.activity.feed;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.k;
import com.immomo.momo.service.j;

final class bd extends d {

    /* renamed from: a  reason: collision with root package name */
    private v f1461a = null;
    private /* synthetic */ PublishFeedActivity c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public bd(PublishFeedActivity publishFeedActivity, Context context) {
        super(context);
        this.c = publishFeedActivity;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        return k.a().a(this.c.p.getText().toString().trim());
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.f1461a = new v(this.c.n(), "请稍候，正在提交...");
        this.f1461a.setOnCancelListener(new be(this));
        this.c.a(this.f1461a);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        PublishFeedActivity.b(this.c);
        PublishFeedActivity.c(this.c);
        j.a().c(this.c);
        this.c.finish();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.c.p();
    }
}
