package com.immomo.momo.android.activity.plugin;

import android.content.Context;
import com.immomo.momo.R;
import com.immomo.momo.a.a;
import com.immomo.momo.android.c.d;
import com.immomo.momo.plugin.d.c;
import com.immomo.momo.util.ao;

final class bm extends d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ RenrenActivity f2067a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public bm(RenrenActivity renrenActivity, Context context) {
        super(context);
        this.f2067a = renrenActivity;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        return com.immomo.momo.protocol.a.d.c(this.f2067a.x, this.f2067a.y);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
        this.f2067a.findViewById(R.id.process_layout_root).setVisibility(0);
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        if (exc instanceof a) {
            super.a(exc);
            return;
        }
        this.b.a((Throwable) exc);
        ao.g(R.string.plus_error_profile);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        c cVar = (c) obj;
        super.a(cVar);
        if (cVar != null) {
            this.f2067a.t = cVar;
            this.f2067a.u();
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
        super.b();
        this.f2067a.findViewById(R.id.process_layout_root).setVisibility(8);
    }
}
