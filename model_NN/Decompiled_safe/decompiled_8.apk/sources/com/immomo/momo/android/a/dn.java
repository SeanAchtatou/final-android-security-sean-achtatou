package com.immomo.momo.android.a;

import android.app.Activity;
import android.support.v4.b.a;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.d;
import com.immomo.momo.android.view.HandyListView;
import com.immomo.momo.android.view.ce;
import com.immomo.momo.service.bean.a.b;
import com.immomo.momo.service.bean.a.c;
import com.immomo.momo.service.bean.aj;
import com.immomo.momo.util.j;
import com.immomo.momo.util.m;
import java.util.List;

public final class dn extends ce {
    /* access modifiers changed from: private */
    public Activity d = null;
    /* access modifiers changed from: private */
    public HandyListView e = null;
    private View.OnClickListener f = new Cdo(this);
    private View.OnLongClickListener g = new dq(this);

    public dn(HandyListView handyListView, Activity activity, List list) {
        super(activity, list);
        this.d = activity;
        this.f671a = list;
        this.e = handyListView;
        new m("test_momo", "[ --- from GroupActionAdapter --- ]").b();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.b.a.a(java.util.Date, boolean):java.lang.String
     arg types: [java.util.Date, int]
     candidates:
      android.support.v4.b.a.a(android.graphics.Bitmap, float):android.graphics.Bitmap
      android.support.v4.b.a.a(android.graphics.Bitmap, java.io.File):android.graphics.Bitmap
      android.support.v4.b.a.a(int, int):java.lang.String
      android.support.v4.b.a.a(java.util.Collection, java.lang.String):java.lang.String
      android.support.v4.b.a.a(java.lang.Object[], java.lang.String):java.lang.String
      android.support.v4.b.a.a(java.lang.String, java.lang.String):org.json.JSONObject
      android.support.v4.b.a.a(float, int[]):void
      android.support.v4.b.a.a(android.app.Activity, int):void
      android.support.v4.b.a.a(android.view.View, java.lang.Runnable):void
      android.support.v4.b.a.a(java.io.File, java.io.File):void
      android.support.v4.b.a.a(java.io.File, java.util.zip.ZipInputStream):void
      android.support.v4.b.a.a(java.lang.Object, java.lang.StringBuilder):void
      android.support.v4.b.a.a(android.content.Context, java.lang.String):boolean
      android.support.v4.b.a.a(com.immomo.a.a.d.b, com.immomo.momo.service.bean.Message):boolean
      android.support.v4.b.a.a(byte[], java.lang.String):byte[]
      android.support.v4.b.a.a(byte[], byte[]):byte[]
      android.support.v4.b.a.a(int[], boolean):byte[]
      android.support.v4.b.a.a(byte[], boolean):int[]
      android.support.v4.b.a.a(int, float):void
      android.support.v4.b.a.a(java.lang.Object, com.immomo.a.a.f):void
      android.support.v4.view.bf.a(int, float):void
      com.immomo.a.a.f.a(java.lang.Object, com.immomo.a.a.f):void
      android.support.v4.b.a.a(java.util.Date, boolean):java.lang.String */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup):void
     arg types: [com.immomo.momo.service.bean.a.a, android.widget.ImageView, com.immomo.momo.android.view.HandyListView]
     candidates:
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, int):void
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, com.immomo.momo.android.view.HandyListView):void
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup, int, boolean, boolean, int):void
     arg types: [com.immomo.momo.service.bean.a.a, android.widget.ImageView, com.immomo.momo.android.view.HandyListView, int, int, int, int]
     candidates:
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, int, android.view.View, android.view.ViewGroup, boolean, int, boolean):com.immomo.momo.android.c.o
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup, int, boolean, boolean, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup, int, boolean, boolean, int):void
     arg types: [com.immomo.momo.service.bean.bf, android.widget.ImageView, com.immomo.momo.android.view.HandyListView, int, int, int, int]
     candidates:
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, int, android.view.View, android.view.ViewGroup, boolean, int, boolean):com.immomo.momo.android.c.o
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup, int, boolean, boolean, int):void */
    public final View a(int i, View view) {
        dz dzVar;
        boolean z = false;
        if (getItemViewType(i) == 1) {
            if (view == null) {
                dz dzVar2 = new dz((byte) 0);
                view = LayoutInflater.from(this.d).inflate((int) R.layout.listitem_groupaction_recomm, (ViewGroup) null);
                dzVar2.f789a = view.findViewById(R.id.item_layout);
                dzVar2.b[0] = (ViewGroup) view.findViewById(R.id.groupaction_layout_recommend_group1);
                dzVar2.b[1] = (ViewGroup) view.findViewById(R.id.groupaction_layout_recommend_group2);
                dzVar2.b[2] = (ViewGroup) view.findViewById(R.id.groupaction_layout_recommend_group3);
                dzVar2.b[3] = (ViewGroup) view.findViewById(R.id.groupaction_layout_recommend_group4);
                dzVar2.b[4] = (ViewGroup) view.findViewById(R.id.groupaction_layout_recommend_group5);
                dzVar2.c[0] = view.findViewById(R.id.groupaction_view_recommend_divide1);
                dzVar2.c[1] = view.findViewById(R.id.groupaction_view_recommend_divide2);
                dzVar2.c[2] = view.findViewById(R.id.groupaction_view_recommend_divide3);
                dzVar2.c[3] = view.findViewById(R.id.groupaction_view_recommend_divide4);
                dzVar2.d = (TextView) view.findViewById(R.id.groupaction_tv_recommend_time);
                view.setTag(R.id.tag_userlist_item, dzVar2);
                dzVar2.b[0].setOnClickListener(this.f);
                dzVar2.b[1].setOnClickListener(this.f);
                dzVar2.b[2].setOnClickListener(this.f);
                dzVar2.b[3].setOnClickListener(this.f);
                dzVar2.b[4].setOnClickListener(this.f);
                dzVar2.b[0].setOnLongClickListener(this.g);
                dzVar2.b[1].setOnLongClickListener(this.g);
                dzVar2.b[2].setOnLongClickListener(this.g);
                dzVar2.b[3].setOnLongClickListener(this.g);
                dzVar2.b[4].setOnLongClickListener(this.g);
                dzVar = dzVar2;
            } else {
                dzVar = (dz) view.getTag(R.id.tag_userlist_item);
            }
            b bVar = (b) this.f671a.get(i);
            dzVar.f789a.setOnLongClickListener(new dr(this, i));
            dzVar.f789a.setOnClickListener(new ds(this, i));
            dzVar.d.setText(a.a(bVar.f(), false));
            for (int i2 = 0; i2 < dzVar.b.length; i2++) {
                int i3 = i2 - 1;
                View view2 = null;
                if (i3 >= 0 && i3 < dzVar.c.length) {
                    view2 = dzVar.c[i3];
                }
                if (i2 >= bVar.h().size()) {
                    dzVar.b[i2].setVisibility(8);
                    if (view2 != null) {
                        view2.setVisibility(8);
                    }
                } else {
                    if (view2 != null) {
                        view2.setVisibility(0);
                    }
                    dzVar.b[i2].setVisibility(0);
                    com.immomo.momo.service.bean.a.a aVar = (com.immomo.momo.service.bean.a.a) bVar.h().get(i2);
                    dzVar.b[i2].setTag(aVar.b);
                    dzVar.b[i2].setClickable(!this.e.f());
                    dzVar.b[i2].setLongClickable(!this.e.f());
                    dzVar.b[i2].setPressed(false);
                    dzVar.b[i2].setTag(R.id.tag_item_position, Integer.valueOf(i));
                    TextView textView = (TextView) dzVar.b[i2].findViewById(R.id.groupaction_tv_recommend_distance);
                    ImageView imageView = (ImageView) dzVar.b[i2].findViewById(R.id.groupaction_iv_recommend_avatar);
                    ((TextView) dzVar.b[i2].findViewById(R.id.groupaction_tv_recommend_title)).setText(aVar.d());
                    if (aVar.b() > 0.0f) {
                        textView.setText(aVar.p);
                    } else {
                        textView.setText(PoiTypeDef.All);
                    }
                    j.a((aj) aVar, imageView, (ViewGroup) this.e);
                }
            }
        } else {
            if (view == null) {
                ed edVar = new ed((byte) 0);
                view = LayoutInflater.from(this.d).inflate((int) R.layout.listitem_groupaction, (ViewGroup) null);
                edVar.i = view.findViewById(R.id.layout_command_button);
                edVar.j[0] = (Button) edVar.i.findViewById(R.id.button1);
                edVar.j[0].setVisibility(0);
                edVar.j[1] = (Button) edVar.i.findViewById(R.id.button2);
                edVar.j[2] = (Button) edVar.i.findViewById(R.id.button3);
                edVar.j[3] = (Button) edVar.i.findViewById(R.id.button4);
                edVar.c = (ImageView) view.findViewById(R.id.iv_header_main);
                edVar.f = (ImageView) view.findViewById(R.id.iv_header_sub);
                edVar.g = (TextView) view.findViewById(R.id.tv_user);
                edVar.h = (TextView) view.findViewById(R.id.tv_content);
                edVar.e = (TextView) view.findViewById(R.id.tv_timestamp);
                edVar.d = (TextView) view.findViewById(R.id.hilist_tv_groupname);
                edVar.f794a = view.findViewById(R.id.item_layout);
                edVar.b = view.findViewById(R.id.item_layout_top);
                view.setTag(R.id.tag_userlist_item, edVar);
            }
            ed edVar2 = (ed) view.getTag(R.id.tag_userlist_item);
            b bVar2 = (b) this.f671a.get(i);
            edVar2.f794a.setOnLongClickListener(new dt(this, i));
            edVar2.f794a.setOnClickListener(new du(this, i));
            edVar2.b.setOnClickListener(new dv(this, i, bVar2));
            edVar2.b.setOnLongClickListener(new dw(this, i));
            if (bVar2.o()) {
                List n = bVar2.n();
                int size = n.size();
                int dimensionPixelSize = this.b.getResources().getDimensionPixelSize(R.dimen.button_padding);
                edVar2.i.setVisibility(0);
                for (int i4 = 0; i4 < 4; i4++) {
                    if (i4 >= size) {
                        edVar2.j[i4].setVisibility(8);
                    } else if (((c) n.get(i4)).e()) {
                        edVar2.j[i4].setVisibility(0);
                        edVar2.j[i4].setText("已处理");
                        edVar2.j[i4].setClickable(false);
                        edVar2.j[i4].setEnabled(false);
                        edVar2.j[i4].setBackgroundResource(R.drawable.btn_big_normal_disable);
                        edVar2.j[i4].setPadding(dimensionPixelSize, 0, dimensionPixelSize, 0);
                    } else {
                        if (!d.a(((c) n.get(i4)).b())) {
                            String c = ((c) n.get(i4)).c();
                            if (!(c.startsWith("/api/") || c.startsWith("/game/"))) {
                                edVar2.j[i4].setVisibility(8);
                            }
                        }
                        edVar2.j[i4].setVisibility(0);
                        edVar2.j[i4].setText(((c) n.get(i4)).a());
                        edVar2.j[i4].setEnabled(true);
                        if (((c) n.get(i4)).b().contains("accept")) {
                            edVar2.j[i4].setBackgroundResource(R.drawable.btn_default_popsubmit);
                        } else {
                            edVar2.j[i4].setBackgroundResource(R.drawable.btn_default);
                        }
                        edVar2.j[i4].setPadding(dimensionPixelSize, 0, dimensionPixelSize, 0);
                        if (this.e.f()) {
                            edVar2.j[i4].setClickable(false);
                        } else {
                            edVar2.j[i4].setClickable(true);
                            edVar2.j[i4].setOnClickListener(new dy(this, bVar2, (c) n.get(i4)));
                        }
                    }
                }
            } else {
                edVar2.i.setVisibility(8);
            }
            if (!a.f(bVar2.g())) {
                edVar2.f.setVisibility(8);
            } else {
                edVar2.f.setVisibility(0);
                j.a((aj) bVar2.e(), edVar2.f, (ViewGroup) this.e, 3, false, true, 0);
                edVar2.f.setOnClickListener(new dx(this, bVar2));
            }
            if (bVar2.d().b() >= 0.0f) {
                edVar2.d.setText(String.valueOf(bVar2.d().d()) + " (" + bVar2.d().p + ")");
            } else {
                edVar2.d.setText(bVar2.d().d());
            }
            edVar2.c.setClickable(!this.e.f());
            j.a((aj) bVar2.d(), edVar2.c, (ViewGroup) this.e, 3, false, true, 0);
            edVar2.c.setOnClickListener(new dp(this, bVar2));
            if (bVar2.e() == null) {
                edVar2.g.setVisibility(8);
            } else {
                edVar2.g.setVisibility(0);
                edVar2.g.setText(bVar2.e().i);
            }
            edVar2.h.setText(bVar2.k());
            edVar2.e.setText(a.a(bVar2.f(), false));
            edVar2.f.setClickable(!this.e.f());
            ImageView imageView2 = edVar2.c;
            if (!this.e.f()) {
                z = true;
            }
            imageView2.setClickable(z);
        }
        return view;
    }

    public final int getItemViewType(int i) {
        return ((b) getItem(i)).a() == 1000 ? 1 : 0;
    }

    public final int getViewTypeCount() {
        return 2;
    }
}
