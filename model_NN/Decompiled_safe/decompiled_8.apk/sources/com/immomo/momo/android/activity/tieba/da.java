package com.immomo.momo.android.activity.tieba;

import android.content.Context;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.android.c.d;
import com.immomo.momo.protocol.a.v;
import com.immomo.momo.service.bean.c.e;

final class da extends d {

    /* renamed from: a  reason: collision with root package name */
    private e f2229a;
    private int c;
    private String d;
    private /* synthetic */ TiebaAdminActivity e;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public da(TiebaAdminActivity tiebaAdminActivity, Context context, e eVar, int i, String str) {
        super(context);
        this.e = tiebaAdminActivity;
        this.f2229a = eVar;
        this.c = i;
        this.d = str;
    }

    /* access modifiers changed from: protected */
    public final Object a(Object... objArr) {
        String str;
        boolean z = true;
        switch (this.c) {
            case 1:
                v.a().a(this.f2229a.c, this.f2229a.e, (String) null, this.f2229a.o, this.d);
                str = "删除并禁言";
                break;
            case 2:
                v.a().d(this.f2229a.e, this.d);
                this.f2229a.n = true;
                str = "删除";
                break;
            case 3:
                v.a().a(this.f2229a.e, (String) null, this.f2229a.o);
                str = "请求封禁";
                break;
            case 4:
                if (this.f2229a.m) {
                    v.a().q(this.f2229a.e);
                } else {
                    v.a().p(this.f2229a.e);
                }
                e eVar = this.f2229a;
                if (this.f2229a.m) {
                    z = false;
                }
                eVar.m = z;
                if (!this.f2229a.m) {
                    str = "解锁";
                    break;
                } else {
                    str = "锁定";
                    break;
                }
            case 5:
                v.a().b(this.f2229a.e, (String) null);
                this.e.k.a().remove(this.f2229a);
            default:
                str = PoiTypeDef.All;
                break;
        }
        this.f2229a.i++;
        this.f2229a.h = "吧主" + this.e.f.h() + "操作:" + str;
        if (this.e.k.getCount() > 20) {
            this.e.j.b(this.e.k.a().subList(0, 20));
        } else {
            this.e.j.b(this.e.k.a());
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
        this.e.a(new com.immomo.momo.android.view.a.v(f(), this));
    }

    /* access modifiers changed from: protected */
    public final void a(Object obj) {
        a("处理成功");
        this.e.k.notifyDataSetChanged();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        super.b();
        this.e.p();
    }
}
