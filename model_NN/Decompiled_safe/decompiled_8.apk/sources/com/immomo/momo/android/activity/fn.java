package com.immomo.momo.android.activity;

import android.graphics.Bitmap;
import android.os.Handler;
import android.support.v4.b.a;
import android.view.View;
import com.immomo.momo.android.c.g;
import com.immomo.momo.android.view.photoview.PhotoView;
import com.immomo.momo.util.h;
import com.immomo.momo.util.m;
import java.lang.ref.SoftReference;
import java.lang.ref.WeakReference;

final class fn implements g {

    /* renamed from: a  reason: collision with root package name */
    boolean f1496a;
    fp b;
    String c;
    String d;
    int e;
    boolean f;
    /* access modifiers changed from: private */
    public Bitmap g;
    /* access modifiers changed from: private */
    public SoftReference h;
    private String i;
    private String j;
    /* access modifiers changed from: private */
    public WeakReference k;
    /* access modifiers changed from: private */
    public WeakReference l;
    /* access modifiers changed from: private */
    public Handler m;

    public fn() {
        this.b = null;
        this.g = null;
        this.h = null;
        this.k = null;
        this.l = null;
        this.m = null;
        this.f = true;
        this.g = null;
        this.h = null;
    }

    public fn(String str) {
        this();
        this.c = str;
    }

    public final Bitmap a() {
        return this.g;
    }

    public final void a(View view) {
        this.l = new WeakReference(view);
    }

    public final void a(PhotoView photoView) {
        this.k = new WeakReference(photoView);
    }

    public final /* synthetic */ void a(Object obj) {
        int i2;
        int i3 = 4096;
        boolean z = true;
        Bitmap bitmap = (Bitmap) obj;
        this.f = true;
        boolean z2 = bitmap != null;
        if (bitmap != null && (((float) bitmap.getWidth()) > 4096.0f || ((float) bitmap.getHeight()) > 4096.0f)) {
            bitmap.recycle();
            if (bitmap.getHeight() <= bitmap.getWidth()) {
                z = false;
            }
            if (z) {
                i2 = Math.round((4096.0f / ((float) bitmap.getHeight())) * ((float) bitmap.getWidth()));
            } else {
                i2 = 4096;
                i3 = Math.round((4096.0f / ((float) bitmap.getWidth())) * ((float) bitmap.getHeight()));
            }
            try {
                bitmap = a.a(h.a(this.c, this.e), i2, i3);
            } catch (Throwable th) {
                bitmap.recycle();
                new m(this).a(th);
                bitmap = null;
            }
        }
        if (bitmap != null) {
            this.h = new SoftReference(bitmap);
        }
        this.m.post(new fo(this, bitmap, z2));
    }

    public final void a(String str) {
        if (str != null) {
            this.c = a.n(str);
            this.i = str;
        }
    }

    public final Bitmap b() {
        if (this.h != null) {
            return (Bitmap) this.h.get();
        }
        return null;
    }

    public final void b(String str) {
        if (str != null) {
            this.d = a.n(str);
            this.j = str;
        }
    }

    public final String c() {
        return this.i;
    }

    public final String d() {
        return this.j;
    }
}
