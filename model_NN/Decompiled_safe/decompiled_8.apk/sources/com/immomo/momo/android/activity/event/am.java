package com.immomo.momo.android.activity.event;

import android.content.Intent;
import android.view.View;
import com.immomo.momo.android.activity.OtherProfileActivity;
import com.immomo.momo.service.bean.ab;

final class am implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ EventProfileActivity f1356a;
    private final /* synthetic */ ab b;

    am(EventProfileActivity eventProfileActivity, ab abVar) {
        this.f1356a = eventProfileActivity;
        this.b = abVar;
    }

    public final void onClick(View view) {
        Intent intent = new Intent();
        intent.setClass(this.f1356a, OtherProfileActivity.class);
        intent.putExtra("tag", "local");
        intent.putExtra("momoid", this.b.c);
        this.f1356a.startActivity(intent);
    }
}
