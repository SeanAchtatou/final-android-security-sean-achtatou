package com.immomo.momo.android.pay;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import com.immomo.momo.R;

final class am extends Handler {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ al f2532a;

    am(al alVar) {
        this.f2532a = alVar;
    }

    public final void handleMessage(Message message) {
        try {
            switch (message.what) {
                case 3:
                    al alVar = this.f2532a;
                    al alVar2 = this.f2532a;
                    Context context = this.f2532a.f2531a;
                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setTitle(context.getResources().getString(R.string.dialog_title_alert));
                    builder.setMessage(context.getResources().getString(R.string.payvip_alipay_plugin_installmsg));
                    builder.setPositiveButton((int) R.string.dialog_btn_confim, new an((String) message.obj, context));
                    builder.setNegativeButton(context.getResources().getString(R.string.dialog_btn_cancel), new ao());
                    builder.show();
                    break;
            }
            super.handleMessage(message);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
