package com.immomo.momo.android.view.a;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.android.a.a;
import java.util.List;

final class p extends a {
    private /* synthetic */ o d;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public p(o oVar, Context context, List list) {
        super(context, list);
        this.d = oVar;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = this.c.inflate((int) R.layout.listitem_dialog, (ViewGroup) null);
        }
        ((TextView) view.findViewById(R.id.textview)).setText(getItem(i).toString());
        if (this.d.b == i) {
            view.findViewById(R.id.imageview).setVisibility(0);
        } else {
            view.findViewById(R.id.imageview).setVisibility(8);
        }
        return view;
    }
}
