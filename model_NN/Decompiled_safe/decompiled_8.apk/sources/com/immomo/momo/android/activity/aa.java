package com.immomo.momo.android.activity;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.a;

final class aa extends d {

    /* renamed from: a  reason: collision with root package name */
    private String f925a;
    private String c;
    private v d;
    /* access modifiers changed from: private */
    public /* synthetic */ AlipayUserWelcomeActivity e;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public aa(AlipayUserWelcomeActivity alipayUserWelcomeActivity, Context context, String str, String str2) {
        super(context);
        this.e = alipayUserWelcomeActivity;
        this.f925a = str2;
        this.c = str;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        a.a().c(this.f925a, this.e.h, this.c);
        return false;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.d = new v(this.e);
        this.d.a("请求提交中");
        this.d.setCancelable(true);
        this.d.setOnCancelListener(new ab(this));
        this.e.a(this.d);
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        super.a(exc);
        this.e.f();
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        a("绑定成功");
        AlipayUserWelcomeActivity.d(this.e);
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.e.p();
    }
}
