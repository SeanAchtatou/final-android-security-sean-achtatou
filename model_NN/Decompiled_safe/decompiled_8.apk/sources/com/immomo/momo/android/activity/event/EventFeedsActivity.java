package com.immomo.momo.android.activity.event;

import android.content.Intent;
import android.os.Bundle;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import com.immomo.momo.R;
import com.immomo.momo.android.a.bv;
import com.immomo.momo.android.activity.ah;
import com.immomo.momo.android.view.LoadingButton;
import com.immomo.momo.android.view.MomoRefreshListView;
import com.immomo.momo.android.view.bi;
import com.immomo.momo.android.view.bu;
import com.immomo.momo.g;
import com.immomo.momo.service.r;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class EventFeedsActivity extends ah implements bu {
    /* access modifiers changed from: private */
    public String h = null;
    /* access modifiers changed from: private */
    public MomoRefreshListView i = null;
    /* access modifiers changed from: private */
    public List j = new ArrayList();
    /* access modifiers changed from: private */
    public bv k = null;
    /* access modifiers changed from: private */
    public LoadingButton l = null;
    /* access modifiers changed from: private */
    public Date m = null;
    /* access modifiers changed from: private */
    public ad n = null;
    /* access modifiers changed from: private */
    public String o = null;
    /* access modifiers changed from: private */
    public String p = null;
    /* access modifiers changed from: private */
    public r q = null;

    static /* synthetic */ void l(EventFeedsActivity eventFeedsActivity) {
        eventFeedsActivity.i.n();
        if (eventFeedsActivity.n != null) {
            eventFeedsActivity.n.cancel(true);
            eventFeedsActivity.n = null;
        }
    }

    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        super.a(bundle);
        setContentView((int) R.layout.activity_eventfeeds);
        Intent intent = getIntent();
        this.h = intent.getStringExtra("eventid");
        this.o = intent.getStringExtra("eventname");
        this.p = intent.getStringExtra("eventdes");
        m().setTitleText("活动讨论");
        bi a2 = new bi(getApplicationContext()).a((int) R.drawable.ic_topbar_addfeed_white);
        a2.setMarginRight(8);
        a2.setBackgroundResource(R.drawable.bg_header_submit);
        a(a2, new y(this));
        this.i = (MomoRefreshListView) findViewById(R.id.listview);
        this.i.setFastScrollEnabled(false);
        this.i.addHeaderView(g.o().inflate((int) R.layout.listitem_blank, (ViewGroup) null));
        this.i.a(g.o().inflate((int) R.layout.include_eventfeeds_listempty, (ViewGroup) null));
        this.i.setLastFlushTime(this.g.b("neventfeeds_lasttime_success"));
        this.i.setEnableLoadMoreFoolter(true);
        this.l = this.i.getFooterViewButton();
    }

    public final void b_() {
        this.i.setLoadingViewText(R.string.pull_to_refresh_refreshing_label);
        b(new ad(this, this));
    }

    /* access modifiers changed from: protected */
    public final void d() {
        this.q = new r();
        this.j = this.q.a(this.h);
        this.k = new bv(this, this.j, this.i);
        this.m = this.g.b("neventfeeds_latttime_reflush");
        this.i.setAdapter((ListAdapter) this.k);
        if (this.j.size() <= 0 || !((Boolean) this.g.b("neventfeeds_remain", false)).booleanValue()) {
            this.i.k();
        }
        this.i.l();
    }

    /* access modifiers changed from: protected */
    public final void e() {
        super.e();
        this.l.setOnProcessListener(new z(this));
        this.i.setOnPullToRefreshListener$42b903f6(this);
        this.i.setOnCancelListener$135502(new aa(this));
        this.i.setOnItemClickListener(new ab(this));
        d();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.k != null) {
            this.k.notifyDataSetChanged();
        }
    }
}
