package com.immomo.momo.android.activity.group;

import android.content.DialogInterface;
import com.immomo.momo.android.view.EmoteEditeText;

final class cu implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ GroupProfileActivity f1593a;
    private final /* synthetic */ EmoteEditeText b;

    cu(GroupProfileActivity groupProfileActivity, EmoteEditeText emoteEditeText) {
        this.f1593a = groupProfileActivity;
        this.b = emoteEditeText;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        GroupProfileActivity.a(this.f1593a, this.b);
        dialogInterface.dismiss();
    }
}
