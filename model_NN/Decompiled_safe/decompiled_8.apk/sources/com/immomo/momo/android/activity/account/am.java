package com.immomo.momo.android.activity.account;

import android.content.Context;
import android.graphics.Bitmap;
import com.immomo.momo.android.c.d;
import com.immomo.momo.protocol.a.w;

final class am extends d {

    /* renamed from: a  reason: collision with root package name */
    private StringBuilder f941a = new StringBuilder();
    private /* synthetic */ ac c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public am(ac acVar, Context context) {
        super(context);
        this.c = acVar;
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ Object a(Object... objArr) {
        w.a();
        return w.a(this.f941a);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        if (this.c.g != null) {
            this.c.g.cancel(true);
        }
        this.c.g = this;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        this.c.k.setImageBitmap((Bitmap) obj);
        this.c.h = true;
        this.c.i = this.f941a.toString();
        this.b.a((Object) ("cookie:" + this.c.i));
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.c.g = (am) null;
    }
}
