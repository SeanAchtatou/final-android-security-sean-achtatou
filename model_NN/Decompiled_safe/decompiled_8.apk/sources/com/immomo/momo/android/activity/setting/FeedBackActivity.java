package com.immomo.momo.android.activity.setting;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.b.a;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.a.a.f.b;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.ImageFactoryActivity;
import com.immomo.momo.android.activity.ah;
import com.immomo.momo.android.view.HeaderLayout;
import com.immomo.momo.android.view.a.o;
import com.immomo.momo.android.view.bi;
import com.immomo.momo.util.ao;
import com.immomo.momo.util.h;
import java.io.File;
import java.util.Date;
import java.util.UUID;
import mm.purchasesdk.PurchaseCode;

public class FeedBackActivity extends ah implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {
    /* access modifiers changed from: private */
    public EditText h;
    private bi i;
    private HeaderLayout j;
    private RadioButton k = null;
    private RadioButton l = null;
    private RadioButton m = null;
    private RadioButton n = null;
    private RadioButton o = null;
    /* access modifiers changed from: private */
    public int p = 1;
    /* access modifiers changed from: private */
    public o q;
    private ImageView r = null;
    private String s = PoiTypeDef.All;
    private File t = null;
    /* access modifiers changed from: private */
    public File u = null;
    private Bitmap v = null;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.b.a.a(android.graphics.Bitmap, float, boolean):android.graphics.Bitmap
     arg types: [android.graphics.Bitmap, int, int]
     candidates:
      android.support.v4.b.a.a(android.graphics.Bitmap, int, int):android.graphics.Bitmap
      android.support.v4.b.a.a(java.io.File, int, int):android.graphics.Bitmap
      android.support.v4.b.a.a(java.lang.String, java.lang.String, java.lang.String):java.lang.String
      android.support.v4.b.a.a(java.lang.Object[], java.lang.String, java.lang.String):java.lang.String
      android.support.v4.b.a.a(android.graphics.Bitmap, float, boolean):android.graphics.Bitmap */
    private void a(File file) {
        String absolutePath = file.getAbsolutePath();
        file.getName().substring(0, file.getName().lastIndexOf("."));
        Bitmap l2 = a.l(absolutePath);
        if (l2 != null) {
            this.v = a.a(l2, 150.0f, true);
            this.r.setImageBitmap(this.v);
        }
    }

    static /* synthetic */ void e(FeedBackActivity feedBackActivity) {
        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("immomo_");
        stringBuffer.append(a.c(new Date()));
        stringBuffer.append("_" + UUID.randomUUID());
        stringBuffer.append(".jpg");
        feedBackActivity.s = stringBuffer.toString();
        intent.putExtra("output", Uri.fromFile(new File(com.immomo.momo.a.i(), feedBackActivity.s)));
        feedBackActivity.startActivityForResult(intent, PurchaseCode.UNSUB_OK);
    }

    static /* synthetic */ void f(FeedBackActivity feedBackActivity) {
        Intent intent = new Intent("android.intent.action.GET_CONTENT");
        intent.setType("image/*");
        feedBackActivity.startActivityForResult(intent, PurchaseCode.ORDER_OK);
    }

    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        super.a(bundle);
        setContentView((int) R.layout.activity_feedback);
        this.h = (EditText) findViewById(R.id.feedback_edit);
        this.j = (HeaderLayout) findViewById(R.id.layout_header);
        this.i = new bi(this);
        this.i.a((int) R.drawable.ic_topbar_confirm_white);
        this.i.setBackgroundResource(R.drawable.bg_header_submit);
        this.i.setMarginRight(8);
        this.k = (RadioButton) findViewById(R.id.feedback_filter_radiobutton_advice);
        this.l = (RadioButton) findViewById(R.id.feedback_filter_radiobutton_suggest);
        this.m = (RadioButton) findViewById(R.id.feedback_filter_radiobutton_error);
        this.n = (RadioButton) findViewById(R.id.feedback_filter_radiobutton_complaints);
        this.o = (RadioButton) findViewById(R.id.feedback_filter_radiobutton_game);
        this.j.setTitleText((int) R.string.feedback_title);
        this.r = (ImageView) findViewById(R.id.iv_camera);
        this.j.a(this.i, new l(this));
        this.k.setOnCheckedChangeListener(this);
        this.l.setOnCheckedChangeListener(this);
        this.m.setOnCheckedChangeListener(this);
        this.n.setOnCheckedChangeListener(this);
        this.o.setOnCheckedChangeListener(this);
        findViewById(R.id.feedback_layout_netcheck).setOnClickListener(new m(this));
        findViewById(R.id.iv_camera_cover).setOnClickListener(this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.h.a(java.lang.String, android.graphics.Bitmap, int, boolean):java.io.File
     arg types: [java.lang.String, android.graphics.Bitmap, int, int]
     candidates:
      com.immomo.momo.util.h.a(java.lang.String, java.lang.String, int, boolean):void
      com.immomo.momo.util.h.a(java.lang.String, android.graphics.Bitmap, int, boolean):java.io.File */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.b.a.a(android.graphics.Bitmap, float, boolean):android.graphics.Bitmap
     arg types: [android.graphics.Bitmap, int, int]
     candidates:
      android.support.v4.b.a.a(android.graphics.Bitmap, int, int):android.graphics.Bitmap
      android.support.v4.b.a.a(java.io.File, int, int):android.graphics.Bitmap
      android.support.v4.b.a.a(java.lang.String, java.lang.String, java.lang.String):java.lang.String
      android.support.v4.b.a.a(java.lang.Object[], java.lang.String, java.lang.String):java.lang.String
      android.support.v4.b.a.a(android.graphics.Bitmap, float, boolean):android.graphics.Bitmap */
    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        Uri fromFile;
        switch (i2) {
            case PurchaseCode.QUERY_OK /*101*/:
                if (i3 == -1 && intent != null) {
                    if (!com.immomo.a.a.f.a.a(this.s)) {
                        File file = new File(com.immomo.momo.a.i(), this.s);
                        if (file.exists()) {
                            file.delete();
                        }
                        this.s = null;
                    }
                    if (this.t != null) {
                        String absolutePath = this.t.getAbsolutePath();
                        String substring = this.t.getName().substring(0, this.t.getName().lastIndexOf("."));
                        Bitmap l2 = a.l(absolutePath);
                        if (l2 != null) {
                            this.u = h.a(substring, l2, 16, false);
                            this.e.a((Object) ("scaleAndSavePhoto, uploadFile=" + this.u.getPath()));
                            this.v = a.a(l2, 150.0f, true);
                            h.a(substring, this.v, 15, false);
                            this.r.setImageBitmap(this.v);
                        }
                        try {
                            this.t.delete();
                            this.t = null;
                        } catch (Exception e) {
                        }
                        getWindow().getDecorView().requestFocus();
                        return;
                    }
                    return;
                } else if (i3 == 1003) {
                    ao.b("图片尺寸太小，请重新选择", 1);
                    return;
                } else if (i3 == 1000) {
                    ao.d(R.string.cropimage_error_other);
                    return;
                } else if (i3 == 1002) {
                    ao.d(R.string.cropimage_error_store);
                    return;
                } else if (i3 == 1001) {
                    ao.d(R.string.cropimage_error_filenotfound);
                    return;
                } else if (i3 == 0) {
                    finish();
                    return;
                } else {
                    return;
                }
            case PurchaseCode.ORDER_OK /*102*/:
                if (i3 == -1 && intent != null) {
                    Uri data = intent.getData();
                    Intent intent2 = new Intent(this, ImageFactoryActivity.class);
                    intent2.setData(data);
                    intent2.putExtra("aspectY", 1);
                    intent2.putExtra("aspectX", 1);
                    intent2.putExtra("maxwidth", 720);
                    intent2.putExtra("maxheight", 3000);
                    intent2.putExtra("minsize", 320);
                    intent2.putExtra("process_model", "filter");
                    this.t = new File(com.immomo.momo.a.g(), "temp_" + b.a() + ".jpg_");
                    intent2.putExtra("outputFilePath", this.t.getAbsolutePath());
                    startActivityForResult(intent2, PurchaseCode.QUERY_OK);
                    return;
                }
                return;
            case PurchaseCode.UNSUB_OK /*103*/:
                if (i3 == -1 && !com.immomo.a.a.f.a.a(this.s) && (fromFile = Uri.fromFile(new File(com.immomo.momo.a.i(), this.s))) != null) {
                    Intent intent3 = new Intent(this, ImageFactoryActivity.class);
                    intent3.setData(fromFile);
                    intent3.putExtra("minsize", 320);
                    intent3.putExtra("process_model", "filter");
                    intent3.putExtra("maxwidth", 720);
                    intent3.putExtra("maxheight", 3000);
                    this.t = new File(com.immomo.momo.a.g(), "temp_" + b.a() + ".jpg_");
                    intent3.putExtra("outputFilePath", this.t.getAbsolutePath());
                    startActivityForResult(intent3, PurchaseCode.QUERY_OK);
                    return;
                }
                return;
            default:
                return;
        }
    }

    public void onCheckedChanged(CompoundButton compoundButton, boolean z) {
        if (z) {
            switch (compoundButton.getId()) {
                case R.id.feedback_filter_radiobutton_advice /*2131165621*/:
                    this.p = 1;
                    return;
                case R.id.feedback_filter_radiobutton_suggest /*2131165622*/:
                    this.p = 2;
                    return;
                case R.id.feedback_filter_radiobutton_error /*2131165623*/:
                    this.p = 3;
                    return;
                case R.id.feedback_filter_radiobutton_complaints /*2131165624*/:
                    this.p = 4;
                    return;
                case R.id.feedback_filter_radiobutton_game /*2131165625*/:
                    this.p = 5;
                    return;
                default:
                    return;
            }
        }
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_camera_cover /*2131165403*/:
                o oVar = new o(this, (int) R.array.publishtiecomment_add_pic);
                oVar.setTitle((int) R.string.dialog_title_add_pic);
                oVar.a(new n(this));
                oVar.show();
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Bundle bundle) {
        super.onRestoreInstanceState(bundle);
        this.s = bundle.getString("camera_file_name");
        if (bundle.containsKey("avatorFile")) {
            try {
                this.t = new File(bundle.getString("avatorFile"));
            } catch (Exception e) {
            }
        }
        if (bundle.containsKey("uploadFile")) {
            try {
                this.u = new File(bundle.getString("uploadFile"));
            } catch (Exception e2) {
            }
        }
        if (this.t != null) {
            a(this.t);
        } else if (this.u != null) {
            a(this.u);
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putString("camera_file_name", this.s);
        if (this.t != null) {
            bundle.putString("avatorFile", this.t.getAbsoluteFile().toString());
        }
        if (this.u != null) {
            bundle.putString("uploadFile", this.u.getAbsoluteFile().toString());
        }
    }
}
