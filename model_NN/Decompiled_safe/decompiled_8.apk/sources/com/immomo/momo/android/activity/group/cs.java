package com.immomo.momo.android.activity.group;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.b.a;
import com.immomo.momo.android.c.b;
import com.immomo.momo.g;
import com.immomo.momo.protocol.a.o;

final class cs extends b {
    private /* synthetic */ GroupPartyActivity c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public cs(GroupPartyActivity groupPartyActivity, Context context) {
        super(context);
        this.c = groupPartyActivity;
        if (groupPartyActivity.y != null && !groupPartyActivity.y.isCancelled()) {
            groupPartyActivity.y.cancel(true);
        }
        groupPartyActivity.y = this;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        return o.a().a(this.c.u, !this.c.t.m);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        String str = (String) obj;
        this.c.t.m = !this.c.t.m;
        boolean unused = this.c.y();
        if (a.f(str)) {
            a(str);
        }
        g.d().a(new Bundle(), "actions.groupfeedchanged");
    }

    /* access modifiers changed from: protected */
    public final void b() {
        super.b();
        this.c.y = (cs) null;
    }
}
