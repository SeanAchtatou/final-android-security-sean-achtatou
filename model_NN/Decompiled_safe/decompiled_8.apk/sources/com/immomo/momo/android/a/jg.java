package com.immomo.momo.android.a;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.service.bean.c.d;
import java.util.List;

public final class jg extends a {
    public jg(Context context, List list) {
        super(context, list);
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        jh jhVar;
        d dVar = (d) getItem(i);
        if (view == null || view.getTag() == null) {
            jh jhVar2 = new jh((byte) 0);
            view = a((int) R.layout.listitem_tiebaverify);
            jhVar2.f897a = (TextView) view.findViewById(R.id.tiebaverify_item_tx_name);
            jhVar2.b = (TextView) view.findViewById(R.id.tiebaverify_item_tx_count);
            view.setTag(jhVar2);
            jhVar = jhVar2;
        } else {
            jhVar = (jh) view.getTag();
        }
        jhVar.f897a.setText(dVar.b);
        jhVar.b.setText(String.valueOf(dVar.g) + "人");
        return view;
    }
}
