package com.immomo.momo.android.activity.plugin;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.a.a;
import com.immomo.momo.android.a.bh;
import com.immomo.momo.android.activity.ah;
import com.immomo.momo.android.c.r;
import com.immomo.momo.android.view.HeaderLayout;
import com.immomo.momo.g;
import com.immomo.momo.plugin.a.d;

public class DoubanActivity extends ah {
    /* access modifiers changed from: private */
    public ImageView h = null;
    private TextView i = null;
    private TextView j = null;
    private TextView k = null;
    private TextView l = null;
    private TextView m = null;
    private View n = null;
    /* access modifiers changed from: private */
    public d o = null;
    /* access modifiers changed from: private */
    public Handler p = new Handler();
    private ListView q = null;
    /* access modifiers changed from: private */
    public a r;
    private AsyncTask s = null;
    /* access modifiers changed from: private */
    public AsyncTask t = null;
    /* access modifiers changed from: private */
    public String u;

    static /* synthetic */ void b(DoubanActivity doubanActivity) {
        if (doubanActivity.o != null) {
            doubanActivity.i.setText(doubanActivity.o.b);
            doubanActivity.j.setText(new StringBuilder(String.valueOf(doubanActivity.o.f)).toString());
            doubanActivity.k.setText(new StringBuilder(String.valueOf(doubanActivity.o.g)).toString());
            doubanActivity.m.setText(doubanActivity.o.d == null ? PoiTypeDef.All : doubanActivity.o.d);
            doubanActivity.l.setText(doubanActivity.o.f2844a);
            if (!android.support.v4.b.a.a((CharSequence) doubanActivity.o.e) && doubanActivity.o.e.indexOf("/") > 0) {
                r rVar = new r(doubanActivity.o.e.substring(doubanActivity.o.e.lastIndexOf("/") + 1, doubanActivity.o.e.lastIndexOf(".")), new bb(doubanActivity), 9, null);
                rVar.a(doubanActivity.o.e);
                new Thread(rVar).start();
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        super.a(bundle);
        setContentView((int) R.layout.activity_plus_douban);
        this.u = getIntent().getExtras() == null ? PoiTypeDef.All : (String) getIntent().getExtras().get("momoid");
        this.n = g.o().inflate((int) R.layout.include_douban_prifile, (ViewGroup) null);
        this.h = (ImageView) this.n.findViewById(R.id.douban_iv_avator);
        this.i = (TextView) this.n.findViewById(R.id.douban_tv_name);
        this.j = (TextView) this.n.findViewById(R.id.douban_tv_friendcount);
        this.k = (TextView) this.n.findViewById(R.id.douban_tv_constanccount);
        this.l = (TextView) this.n.findViewById(R.id.douban_tv_location);
        this.m = (TextView) this.n.findViewById(R.id.douban_tv_content);
        this.q = (ListView) findViewById(R.id.douban_list);
        ((HeaderLayout) findViewById(R.id.layout_header)).setTitleText((int) R.string.plus_douban);
        d();
    }

    /* access modifiers changed from: protected */
    public final void d() {
        this.q.addHeaderView(this.n);
        this.r = new bh(this);
        this.q.setAdapter((ListAdapter) this.r);
        if (!android.support.v4.b.a.a((CharSequence) this.u)) {
            this.s = new bd(this, this).execute(new Object[0]);
        } else {
            this.e.a((Throwable) new NullPointerException("momoid is null"));
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.s != null && !this.s.isCancelled()) {
            this.s.cancel(true);
        }
        if (this.t != null && !this.t.isCancelled()) {
            this.t.cancel(true);
        }
    }
}
