package com.immomo.momo.android.service;

import android.os.Handler;
import android.os.Message;
import com.immomo.momo.android.b.z;

final class k extends Handler {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public /* synthetic */ LService f2611a;

    k(LService lService) {
        this.f2611a = lService;
    }

    public final void handleMessage(Message message) {
        try {
            this.f2611a.e = new l(this);
            z.a(this.f2611a.e);
        } catch (Exception e) {
            this.f2611a.d.a((Throwable) e);
            z.b(this.f2611a.e);
        }
    }
}
