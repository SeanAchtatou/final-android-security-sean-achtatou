package com.immomo.momo.android.activity.tieba;

import com.immomo.momo.android.view.cx;

final class ah implements cx {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ PublishTieActivity f2159a;

    ah(PublishTieActivity publishTieActivity) {
        this.f2159a = publishTieActivity;
    }

    public final void a(int i, int i2) {
        if (i < i2) {
            this.f2159a.e.a((Object) ("OnResize--------------h < oldh=" + i + "<" + i2));
            if (((double) i) <= ((double) this.f2159a.x) * 0.8d) {
                this.f2159a.i = true;
            }
        } else if (((double) i2) <= ((double) this.f2159a.x) * 0.8d && this.f2159a.i && !this.f2159a.y.isShown()) {
            this.f2159a.i = false;
        }
    }
}
