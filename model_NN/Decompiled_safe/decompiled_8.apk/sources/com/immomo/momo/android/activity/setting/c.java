package com.immomo.momo.android.activity.setting;

import android.content.Context;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.a.o;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.n;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.plugin.e.a;
import com.immomo.momo.plugin.e.b;
import com.immomo.momo.service.aq;
import com.immomo.momo.util.ao;

final class c extends d {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public String f2127a;
    private int c;
    private v d;
    /* access modifiers changed from: private */
    public a e;
    /* access modifiers changed from: private */
    public /* synthetic */ CommunityBindActivity f;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public c(CommunityBindActivity communityBindActivity, Context context, a aVar, String str, int i) {
        super(context);
        this.f = communityBindActivity;
        if (communityBindActivity.u != null) {
            communityBindActivity.u.cancel(true);
        }
        communityBindActivity.u = this;
        this.e = aVar;
        this.c = i;
        this.f2127a = str;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        if (this.e == null) {
            this.e = com.immomo.momo.protocol.a.d.b(this.f.h, this.f.i, this.f2127a, b.f2872a);
        }
        return Integer.valueOf(com.immomo.momo.protocol.a.d.a(this.e.c(), this.e.b(), this.e.d(), this.e.a(), this.c));
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
        this.d = new v(this.f);
        this.d.a("请求提交中");
        this.d.setCancelable(true);
        this.d.setOnCancelListener(new d(this));
        this.d.show();
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        this.d.dismiss();
        if (exc instanceof o) {
            n.a(this.f, (int) R.string.bingsina_dialog_msg, new e(this), new f()).show();
        } else if (exc instanceof com.immomo.momo.a.a) {
            ao.a((CharSequence) exc.getMessage());
        } else {
            ao.g(R.string.errormsg_server);
        }
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        Integer num = (Integer) obj;
        super.a(num);
        this.d.dismiss();
        this.f.f.an = true;
        this.f.f.am = this.e.b();
        this.b.a((Object) ("remain day after bind: " + this.f.f.aG));
        if (num.intValue() == 1) {
            this.f.f.ao = "vip";
            this.f.f.ap = true;
        } else {
            this.f.f.ao = PoiTypeDef.All;
            this.f.f.ap = false;
        }
        new aq().b(this.f.f);
        if (this.f.q) {
            ao.a((CharSequence) "绑定成功");
        }
        this.f.setResult(-1);
        this.f.finish();
    }
}
