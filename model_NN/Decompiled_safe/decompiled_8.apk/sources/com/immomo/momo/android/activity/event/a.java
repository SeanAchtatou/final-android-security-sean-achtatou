package com.immomo.momo.android.activity.event;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import com.immomo.momo.R;
import com.immomo.momo.android.a.bs;
import com.immomo.momo.android.activity.lh;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.HeaderLayout;
import com.immomo.momo.android.view.LoadingButton;
import com.immomo.momo.android.view.MomoRefreshExpandableListView;
import com.immomo.momo.android.view.bl;
import com.immomo.momo.android.view.bu;
import com.immomo.momo.android.view.cv;
import com.immomo.momo.g;
import com.immomo.momo.service.af;
import com.immomo.momo.service.bean.y;
import com.immomo.momo.service.bean.z;
import com.immomo.momo.service.r;
import java.util.Date;
import java.util.List;

public class a extends lh implements View.OnClickListener, bl, bu, cv {
    /* access modifiers changed from: private */
    public List O = null;
    /* access modifiers changed from: private */
    public bs P = null;
    /* access modifiers changed from: private */
    public Date Q = null;
    /* access modifiers changed from: private */
    public d R = null;
    /* access modifiers changed from: private */
    public r S;
    /* access modifiers changed from: private */
    public MomoRefreshExpandableListView T = null;
    private LoadingButton U = null;

    public a() {
        new Handler();
    }

    private void O() {
        this.T.setOnPullToRefreshListener(this);
        this.T.setOnCancelListener(this);
        this.U.setOnProcessListener(this);
        this.T.setOnChildClickListener(new b(this));
        this.T.setOnGroupClickListener(new c());
    }

    /* access modifiers changed from: private */
    public void P() {
        if (this.O.size() <= 0) {
            this.U.setVisibility(8);
        } else {
            this.U.setVisibility(0);
        }
    }

    /* access modifiers changed from: private */
    public void Q() {
        this.Q = new Date();
        this.N.a("eventfeed_latttime_reflush", this.Q);
        this.T.i();
    }

    /* access modifiers changed from: protected */
    public final int B() {
        return R.layout.fragment_eventfeed_list;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, com.immomo.momo.android.view.MomoRefreshExpandableListView, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* access modifiers changed from: protected */
    public final void C() {
        Date b = this.N.b("eventfeed_lasttime_success");
        this.T = (MomoRefreshExpandableListView) c((int) R.id.listview);
        this.T.setLastFlushTime(b);
        this.T.setEnableLoadMoreFoolter(true);
        this.T.setFastScrollEnabled(false);
        this.T.setMMHeaderView(g.o().inflate((int) R.layout.listitem_groupsite, (ViewGroup) this.T, false));
        this.T.setGroupIndicator(null);
        this.U = this.T.getFooterViewButton();
        this.U.a("更多历史活动");
        this.T.setListPaddingBottom(-3);
        this.T.a(g.o().inflate((int) R.layout.include_eventdynamic_listempty, (ViewGroup) null));
    }

    public final boolean G() {
        return super.G();
    }

    public final void a(Context context, HeaderLayout headerLayout) {
        super.a(context, headerLayout);
        headerLayout.setTitleText("活动动态");
    }

    /* access modifiers changed from: protected */
    public final boolean a(Bundle bundle, String str) {
        return super.a(bundle, str);
    }

    public final void ae() {
        super.ae();
    }

    public final void ag() {
        super.ag();
    }

    public final void ai() {
        this.T.e();
    }

    public final void aj() {
        O();
        this.Q = this.N.b("eventfeed_latttime_reflush");
        this.O = this.S.b();
        this.P = new bs(this.O, this.T);
        this.T.setAdapter(this.P);
        this.P.a();
        if (this.P.isEmpty() || af.c().n() > 0) {
            this.T.h();
        }
        P();
    }

    public final void b_() {
        if (this.R != null && !this.R.isCancelled()) {
            this.R.cancel(true);
        }
        this.T.setLoadingViewText(R.string.pull_to_refresh_refreshing_label);
        this.R = new d(this, c());
        this.R.execute(new Object[0]);
    }

    public final void g(Bundle bundle) {
        this.S = new r();
        O();
        aj();
    }

    public final void o() {
        super.o();
        for (z zVar : this.O) {
            if (zVar.f3043a != null) {
                for (y yVar : zVar.f3043a) {
                    yVar.a(true);
                    yVar.b(0);
                    yVar.a(0);
                }
            }
        }
        try {
            this.S.d(this.O);
        } catch (Exception e) {
            this.L.a((Throwable) e);
        }
    }

    public void onClick(View view) {
    }

    public final void p() {
        super.p();
        if (this.R != null && !this.R.isCancelled()) {
            this.R.cancel(true);
            this.R = null;
        }
    }

    public final void u() {
        this.U.e();
        this.U.a("更多历史活动");
        a(new Intent(c(), HistoryEventListActivity.class));
    }

    public final void v() {
        Q();
        if (this.R != null && !this.R.isCancelled()) {
            this.R.cancel(true);
        }
    }
}
