package com.immomo.momo.android.a;

import android.view.View;

final class ds implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ dn f782a;
    private final /* synthetic */ int b;

    ds(dn dnVar, int i) {
        this.f782a = dnVar;
        this.b = i;
    }

    public final void onClick(View view) {
        if (this.f782a.e.getOnItemClickListener() != null) {
            this.f782a.e.getOnItemClickListener().onItemClick(this.f782a.e, view, this.b, (long) view.getId());
        }
    }
}
