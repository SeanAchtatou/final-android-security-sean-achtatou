package com.immomo.momo.android.a;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.b.a;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.ImageBrowserActivity;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.ad;
import com.immomo.momo.service.bean.aj;
import com.immomo.momo.service.bean.al;
import com.immomo.momo.util.e;
import com.immomo.momo.util.j;
import java.util.List;

public final class cq extends a implements View.OnClickListener {
    private Context d = null;
    private ListView e = null;
    private List f = null;

    public cq(Context context, List list, ListView listView) {
        super(context, list);
        this.d = context;
        this.f = list;
        this.e = listView;
    }

    /* access modifiers changed from: private */
    /* renamed from: d */
    public ad getItem(int i) {
        return (ad) this.f.get(i);
    }

    public final int getCount() {
        return this.f.size();
    }

    public final long getItemId(int i) {
        return (long) i;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.b.a.a(java.util.Date, boolean):java.lang.String
     arg types: [java.util.Date, int]
     candidates:
      android.support.v4.b.a.a(android.graphics.Bitmap, float):android.graphics.Bitmap
      android.support.v4.b.a.a(android.graphics.Bitmap, java.io.File):android.graphics.Bitmap
      android.support.v4.b.a.a(int, int):java.lang.String
      android.support.v4.b.a.a(java.util.Collection, java.lang.String):java.lang.String
      android.support.v4.b.a.a(java.lang.Object[], java.lang.String):java.lang.String
      android.support.v4.b.a.a(java.lang.String, java.lang.String):org.json.JSONObject
      android.support.v4.b.a.a(float, int[]):void
      android.support.v4.b.a.a(android.app.Activity, int):void
      android.support.v4.b.a.a(android.view.View, java.lang.Runnable):void
      android.support.v4.b.a.a(java.io.File, java.io.File):void
      android.support.v4.b.a.a(java.io.File, java.util.zip.ZipInputStream):void
      android.support.v4.b.a.a(java.lang.Object, java.lang.StringBuilder):void
      android.support.v4.b.a.a(android.content.Context, java.lang.String):boolean
      android.support.v4.b.a.a(com.immomo.a.a.d.b, com.immomo.momo.service.bean.Message):boolean
      android.support.v4.b.a.a(byte[], java.lang.String):byte[]
      android.support.v4.b.a.a(byte[], byte[]):byte[]
      android.support.v4.b.a.a(int[], boolean):byte[]
      android.support.v4.b.a.a(byte[], boolean):int[]
      android.support.v4.b.a.a(int, float):void
      android.support.v4.b.a.a(java.lang.Object, com.immomo.a.a.f):void
      android.support.v4.view.bf.a(int, float):void
      com.immomo.a.a.f.a(java.lang.Object, com.immomo.a.a.f):void
      android.support.v4.b.a.a(java.util.Date, boolean):java.lang.String */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup, int, boolean, boolean, int):void
     arg types: [com.immomo.momo.service.bean.al, android.widget.ImageView, android.widget.ListView, int, int, int, int]
     candidates:
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, int, android.view.View, android.view.ViewGroup, boolean, int, boolean):com.immomo.momo.android.c.o
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup, int, boolean, boolean, int):void */
    public final View getView(int i, View view, ViewGroup viewGroup) {
        cr crVar;
        if (view == null) {
            view = g.o().inflate((int) R.layout.listitem_feedback, (ViewGroup) null);
            cr crVar2 = new cr((byte) 0);
            crVar2.f761a = (ImageView) view.findViewById(R.id.iv_user_image);
            crVar2.b = (TextView) view.findViewById(R.id.tv_user_name);
            crVar2.c = (TextView) view.findViewById(R.id.tv_create_time);
            crVar2.d = (TextView) view.findViewById(R.id.tv_feedback_content);
            crVar2.e = (ImageView) view.findViewById(R.id.iv_feedback_content);
            crVar2.f = (LinearLayout) view.findViewById(R.id.layout_feedback_comment);
            crVar2.i = new cr[5];
            for (int i2 = 0; i2 < 5; i2++) {
                crVar2.i[i2] = new cr((byte) 0);
            }
            crVar2.i[0].g = view.findViewById(R.id.list_feedback0);
            crVar2.i[1].g = view.findViewById(R.id.list_feedback1);
            crVar2.i[2].g = view.findViewById(R.id.list_feedback2);
            crVar2.i[3].g = view.findViewById(R.id.list_feedback3);
            crVar2.i[4].g = view.findViewById(R.id.list_feedback4);
            int i3 = 0;
            while (true) {
                int i4 = i3;
                if (i4 >= 5) {
                    break;
                }
                View view2 = crVar2.i[i4].g;
                crVar2.i[i4].f761a = (ImageView) view2.findViewById(R.id.iv_user_image);
                crVar2.i[i4].b = (TextView) view2.findViewById(R.id.tv_reply_name);
                crVar2.i[i4].c = (TextView) view2.findViewById(R.id.tv_reply_time);
                crVar2.i[i4].d = (TextView) view2.findViewById(R.id.tv_content_text);
                crVar2.i[i4].e = (ImageView) view2.findViewById(R.id.iv_content_image);
                crVar2.i[i4].h = view2.findViewById(R.id.list_divideline);
                i3 = i4 + 1;
            }
            view.setTag(crVar2);
            crVar = crVar2;
        } else {
            crVar = (cr) view.getTag();
        }
        ad d2 = getItem(i);
        crVar.b.setText(String.valueOf(d2.e) + ":");
        crVar.c.setText(a.a(d2.f2984a, false));
        crVar.d.setText(d2.b);
        e.a(crVar.d, d2.b, this.d);
        if (a.f(d2.d)) {
            crVar.f761a.setVisibility(0);
            j.a(new al(d2.d), crVar.f761a, this.e);
        } else {
            crVar.f761a.setVisibility(8);
        }
        if (a.f(d2.c)) {
            crVar.e.setVisibility(0);
            j.a((aj) new al(d2.c), crVar.e, (ViewGroup) this.e, 1, false, false, 0);
            crVar.e.setTag(R.id.tag_item_imageid, d2.c);
            crVar.e.setOnClickListener(this);
        } else {
            crVar.e.setVisibility(8);
        }
        for (int i5 = 0; i5 < 5; i5++) {
            crVar.i[i5].g.setVisibility(8);
        }
        if (d2.f == null || d2.f.size() <= 0) {
            crVar.f.setVisibility(8);
        } else {
            crVar.f.setVisibility(0);
            crVar.i[0].h.setVisibility(8);
            int i6 = 0;
            while (true) {
                int i7 = i6;
                if (i7 >= d2.f.size()) {
                    break;
                }
                crVar.i[i7].g.setVisibility(0);
                ad adVar = (ad) d2.f.get(i7);
                crVar.i[i7].b.setText(String.valueOf(adVar.e) + ":");
                crVar.i[i7].c.setText(a.a(adVar.f2984a, false));
                crVar.i[i7].d.setText(adVar.b);
                e.a(crVar.i[i7].d, adVar.b, this.d);
                if (a.f(adVar.c)) {
                    crVar.i[i7].e.setVisibility(0);
                    j.a((aj) new al(adVar.c), crVar.i[i7].e, (ViewGroup) this.e, 1, false, false, 0);
                    crVar.i[i7].e.setTag(R.id.tag_item_imageid, adVar.c);
                    crVar.i[i7].e.setOnClickListener(this);
                } else {
                    crVar.i[i7].e.setVisibility(8);
                }
                i6 = i7 + 1;
            }
        }
        return view;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public final void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_content_image /*2131166239*/:
            case R.id.iv_feedback_content /*2131166676*/:
                String str = (String) view.getTag(R.id.tag_item_imageid);
                if (str != null) {
                    Intent intent = new Intent(d(), ImageBrowserActivity.class);
                    intent.putExtra("array", new String[]{str});
                    intent.putExtra("imagetype", "chat");
                    intent.putExtra("autohide_header", true);
                    d().startActivity(intent);
                    if (((Activity) d()).getParent() != null) {
                        ((Activity) d()).getParent().overridePendingTransition(R.anim.zoom_enter, R.anim.normal);
                        return;
                    } else {
                        ((Activity) d()).overridePendingTransition(R.anim.zoom_enter, R.anim.normal);
                        return;
                    }
                } else {
                    return;
                }
            default:
                return;
        }
    }
}
