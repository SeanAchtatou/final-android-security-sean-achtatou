package com.immomo.momo.android.pay;

public final class bk {

    /* renamed from: a  reason: collision with root package name */
    public String f2554a;
    public String b;
    public String c;
    public String d;
    public String e;
    public String f;
    public String g;
    public String h;
    public String i;
    public String j;
    public String k;
    public String l;

    public final String toString() {
        return "UniPayBean [cpCode=" + this.f2554a + ", orderid=" + this.b + ", vacCode=" + this.c + ", cpId=" + this.e + ", notifyUrl=" + this.f + ", money=" + this.g + ", company=" + this.h + ", sign=" + this.i + ", phone=" + this.j + ", game=" + this.k + ", appid=" + this.l + "]";
    }
}
