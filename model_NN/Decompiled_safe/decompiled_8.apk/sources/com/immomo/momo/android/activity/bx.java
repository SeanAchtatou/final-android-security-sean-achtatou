package com.immomo.momo.android.activity;

import android.content.DialogInterface;
import java.util.List;

final class bx implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ bw f1056a;
    private final /* synthetic */ List b;

    bx(bw bwVar, List list) {
        this.f1056a = bwVar;
        this.b = list;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f1056a.f1055a.l.a().removeAll(this.b);
        this.f1056a.f1055a.l.notifyDataSetChanged();
        this.f1056a.f1055a.n.b(this.b);
        this.f1056a.f1055a.p.e();
    }
}
