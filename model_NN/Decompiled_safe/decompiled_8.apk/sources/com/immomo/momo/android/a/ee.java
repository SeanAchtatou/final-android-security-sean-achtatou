package com.immomo.momo.android.a;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.aj;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.util.j;
import java.util.List;

public final class ee extends a {
    public ee(Activity activity, List list) {
        super(activity, list);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup, int, boolean, boolean, int):void
     arg types: [com.immomo.momo.service.bean.bf, android.widget.ImageView, ?[OBJECT, ARRAY], int, int, int, int]
     candidates:
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, int, android.view.View, android.view.ViewGroup, boolean, int, boolean):com.immomo.momo.android.c.o
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup, int, boolean, boolean, int):void */
    public final View getView(int i, View view, ViewGroup viewGroup) {
        ef efVar;
        if (view == null) {
            view = g.o().inflate((int) R.layout.listitem_groupchat_memberlist, (ViewGroup) null);
            ef efVar2 = new ef((byte) 0);
            view.setTag(R.id.tag_userlist_item, efVar2);
            efVar2.f795a = (TextView) view.findViewById(R.id.groupchat_tv_name);
            efVar2.b = (TextView) view.findViewById(R.id.groupchat_tv_distance);
            efVar2.c = (ImageView) view.findViewById(R.id.groupchat_iv_photo);
            efVar = efVar2;
        } else {
            efVar = (ef) view.getTag(R.id.tag_userlist_item);
        }
        bf bfVar = (bf) getItem(i);
        j.a((aj) bfVar, efVar.c, (ViewGroup) null, 3, false, true, 10);
        efVar.f795a.setText(bfVar.h());
        if (bfVar.d() < 0.0f) {
            efVar.b.setText(bfVar.Z);
        } else {
            efVar.b.setText(String.valueOf(bfVar.Z) + " | " + bfVar.f());
        }
        if (bfVar.b()) {
            efVar.f795a.setTextColor(g.c((int) R.color.font_vip_name));
        } else {
            efVar.f795a.setTextColor(g.c((int) R.color.font_value));
        }
        return view;
    }
}
