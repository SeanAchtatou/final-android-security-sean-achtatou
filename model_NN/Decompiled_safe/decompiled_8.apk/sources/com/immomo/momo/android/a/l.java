package com.immomo.momo.android.a;

import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.widget.ImageView;

final class l {

    /* renamed from: a  reason: collision with root package name */
    private Handler f907a = null;
    /* access modifiers changed from: private */
    public ImageView b = null;

    public l(Looper looper, ImageView imageView) {
        this.b = imageView;
        this.f907a = new m(this, looper);
    }

    public final void a(Bitmap bitmap) {
        Message message = new Message();
        message.what = 1;
        message.obj = bitmap;
        this.f907a.sendMessage(message);
    }
}
