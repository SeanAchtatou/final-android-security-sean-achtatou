package com.immomo.momo.android.a;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.b.a;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.android.view.EmoteTextView;
import com.immomo.momo.android.view.MomoRefreshExpandableListView;
import com.immomo.momo.b;
import com.immomo.momo.service.bean.bc;
import com.immomo.momo.service.bean.c.g;
import com.immomo.momo.util.j;
import java.util.List;

public final class iw extends b {

    /* renamed from: a  reason: collision with root package name */
    private List f889a = null;
    private MomoRefreshExpandableListView b = null;
    private LayoutInflater c = null;

    public iw(Context context, List list, MomoRefreshExpandableListView momoRefreshExpandableListView) {
        this.f889a = list;
        this.b = momoRefreshExpandableListView;
        this.c = (LayoutInflater) context.getSystemService("layout_inflater");
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public bc getGroup(int i) {
        return (bc) this.f889a.get(i);
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public g getChild(int i, int i2) {
        return (g) ((bc) this.f889a.get(i)).f3008a.get(i2);
    }

    public final void a() {
        for (int i = 0; i < getGroupCount(); i++) {
            this.b.expandGroup(i);
        }
    }

    public final void a(View view, int i) {
    }

    public final int b(int i, int i2) {
        return 0;
    }

    public final long getChildId(int i, int i2) {
        return (long) i2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.b.a(java.lang.String, boolean):android.graphics.Bitmap
     arg types: [java.lang.String, int]
     candidates:
      com.immomo.momo.b.a(java.lang.String, android.graphics.Bitmap):android.graphics.Bitmap
      com.immomo.momo.b.a(java.lang.String, boolean):android.graphics.Bitmap */
    public final View getChildView(int i, int i2, boolean z, View view, ViewGroup viewGroup) {
        if (view == null) {
            iy iyVar = new iy((byte) 0);
            view = this.c.inflate((int) R.layout.listitem_user, (ViewGroup) null);
            iyVar.f891a = (ImageView) view.findViewById(R.id.userlist_item_iv_face);
            iyVar.b = (TextView) view.findViewById(R.id.userlist_item_tv_name);
            iyVar.c = (TextView) view.findViewById(R.id.userlist_item_tv_age);
            iyVar.d = (TextView) view.findViewById(R.id.userlist_item_tv_distance);
            iyVar.e = (TextView) view.findViewById(R.id.userlist_tv_time);
            iyVar.f = (EmoteTextView) view.findViewById(R.id.userlist_item_tv_sign);
            iyVar.i = (ImageView) view.findViewById(R.id.userlist_item_iv_gender);
            iyVar.h = view.findViewById(R.id.userlist_item_layout_genderbackgroud);
            iyVar.p = (ImageView) view.findViewById(R.id.userlist_item_pic_iv_group_role);
            iyVar.j = (ImageView) view.findViewById(R.id.userlist_item_pic_iv_weibo);
            iyVar.k = (ImageView) view.findViewById(R.id.userlist_item_pic_iv_txweibo);
            iyVar.l = (ImageView) view.findViewById(R.id.userlist_item_pic_iv_renren);
            iyVar.m = (ImageView) view.findViewById(R.id.userlist_item_pic_iv_vip);
            view.findViewById(R.id.userlist_item_pic_iv_relation);
            iyVar.n = (ImageView) view.findViewById(R.id.userlist_item_pic_iv_multipic);
            iyVar.o = (ImageView) view.findViewById(R.id.userlist_item_pic_iv_industry);
            iyVar.g = (ImageView) view.findViewById(R.id.userlist_item_pic_sign);
            iyVar.q = view.findViewById(R.id.userlist_tv_timedriver);
            view.setTag(R.id.tag_userlist_item, iyVar);
        }
        g a2 = getChild(i, i2);
        iy iyVar2 = (iy) view.getTag(R.id.tag_userlist_item);
        iyVar2.d.setText(a2.Z);
        if (a2.d() < 0.0f) {
            iyVar2.e.setVisibility(8);
            iyVar2.q.setVisibility(8);
        } else {
            iyVar2.e.setVisibility(0);
            iyVar2.q.setVisibility(0);
            iyVar2.e.setText(a2.aa);
        }
        iyVar2.c.setText(new StringBuilder(String.valueOf(a2.I)).toString());
        iyVar2.b.setText(a2.h());
        if (a2.b()) {
            iyVar2.b.setTextColor(com.immomo.momo.g.c((int) R.color.font_vip_name));
        } else {
            iyVar2.b.setTextColor(com.immomo.momo.g.c((int) R.color.text_color));
        }
        iyVar2.f.setText(a2.n());
        if (!a.a((CharSequence) a2.R)) {
            Bitmap b2 = b.b(a2.R);
            if (b2 != null) {
                iyVar2.g.setVisibility(0);
                iyVar2.g.setImageBitmap(b2);
            } else {
                iyVar2.g.setVisibility(8);
            }
        } else {
            iyVar2.g.setVisibility(8);
        }
        if ("F".equals(a2.H)) {
            iyVar2.h.setBackgroundResource(R.drawable.bg_gender_famal);
            iyVar2.i.setImageResource(R.drawable.ic_user_famale);
        } else {
            iyVar2.h.setBackgroundResource(R.drawable.bg_gender_male);
            iyVar2.i.setImageResource(R.drawable.ic_user_male);
        }
        if (a2.j()) {
            iyVar2.n.setVisibility(0);
        } else {
            iyVar2.n.setVisibility(8);
        }
        if (a2.aJ == 1 || a2.aJ == 3) {
            iyVar2.p.setVisibility(0);
            iyVar2.p.setImageResource(a2.aJ == 1 ? R.drawable.ic_userinfo_groupowner : R.drawable.ic_userinfo_group);
        } else {
            iyVar2.p.setVisibility(8);
        }
        if (a2.an) {
            iyVar2.j.setVisibility(0);
            iyVar2.j.setImageResource(a2.ap ? R.drawable.ic_userinfo_weibov : R.drawable.ic_userinfo_weibo);
        } else {
            iyVar2.j.setVisibility(8);
        }
        if (a2.at) {
            iyVar2.k.setVisibility(0);
            iyVar2.k.setImageResource(a2.au ? R.drawable.ic_userinfo_tweibov : R.drawable.ic_userinfo_tweibo);
        } else {
            iyVar2.k.setVisibility(8);
        }
        if (a2.ar) {
            iyVar2.l.setVisibility(0);
        } else {
            iyVar2.l.setVisibility(8);
        }
        if (a2.b()) {
            iyVar2.m.setVisibility(0);
            if (a2.c()) {
                iyVar2.m.setImageResource(R.drawable.ic_userinfo_vip_year);
            } else {
                iyVar2.m.setImageResource(R.drawable.ic_userinfo_vip);
            }
        } else {
            iyVar2.m.setVisibility(8);
        }
        if (!a.a((CharSequence) a2.M)) {
            iyVar2.o.setVisibility(0);
            iyVar2.o.setImageBitmap(b.a(a2.M, true));
        } else {
            iyVar2.o.setVisibility(8);
        }
        j.a(a2, iyVar2.f891a, this.b, 3);
        return view;
    }

    public final int getChildrenCount(int i) {
        if (i < 0 || i >= this.f889a.size()) {
            return 0;
        }
        if (((bc) this.f889a.get(i)).f3008a == null) {
            return 0;
        }
        return ((bc) this.f889a.get(i)).f3008a.size();
    }

    public final int getGroupCount() {
        return this.f889a.size();
    }

    public final long getGroupId(int i) {
        return (long) i;
    }

    public final View getGroupView(int i, boolean z, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = com.immomo.momo.g.o().inflate((int) R.layout.listitem_tiebamember_groupsite, (ViewGroup) null);
            ix ixVar = new ix();
            ixVar.f890a = (TextView) view.findViewById(R.id.sitelist_tv_name);
            view.setTag(R.id.tag_userlist_item, ixVar);
        }
        ((ix) view.getTag(R.id.tag_userlist_item)).f890a.setText(getGroup(i).b);
        return view;
    }

    public final boolean hasStableIds() {
        return true;
    }

    public final boolean isChildSelectable(int i, int i2) {
        return true;
    }
}
