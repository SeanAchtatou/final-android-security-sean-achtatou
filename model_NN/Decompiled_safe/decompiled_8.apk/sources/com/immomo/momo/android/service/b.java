package com.immomo.momo.android.service;

import com.immomo.momo.service.a.ad;
import com.immomo.momo.service.bean.ak;
import java.io.File;
import java.util.List;

final class b implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ Cleaner f2604a;

    b(Cleaner cleaner) {
        this.f2604a = cleaner;
    }

    public final void run() {
        ad g;
        try {
            g = ad.g();
            List<ak> h = g.h();
            if (h != null && h.size() > 0) {
                g.a().beginTransaction();
                for (ak akVar : h) {
                    File file = new File(akVar.b);
                    if (file.exists()) {
                        file.delete();
                    }
                    g.a(akVar);
                }
                g.a().setTransactionSuccessful();
                g.a().endTransaction();
            }
        } catch (Exception e) {
            this.f2604a.f2598a.a((Throwable) e);
            g.a().setTransactionSuccessful();
            g.a().endTransaction();
        } catch (Throwable th) {
            this.f2604a.f2598a.a(th);
        }
    }
}
