package com.immomo.momo.android.activity;

import android.location.Location;
import com.immomo.momo.android.b.p;
import com.immomo.momo.android.b.z;

final class nd extends p {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ WelcomeActivity f2021a;

    nd(WelcomeActivity welcomeActivity) {
        this.f2021a = welcomeActivity;
    }

    public final void a(Location location, int i, int i2, int i3) {
        this.f2021a.e.a((Object) ("-welcome - loc=" + location));
        if (z.a(location)) {
            this.f2021a.w.post(new ne(this, location));
        }
    }
}
