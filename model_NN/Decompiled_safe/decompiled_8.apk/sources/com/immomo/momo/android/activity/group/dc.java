package com.immomo.momo.android.activity.group;

import android.content.Intent;
import android.view.View;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.GroupLevelActivity;
import com.immomo.momo.android.activity.OtherProfileActivity;
import com.immomo.momo.android.activity.message.GroupChatActivity;
import com.immomo.momo.android.view.a.n;
import com.immomo.momo.util.k;

final class dc implements View.OnClickListener {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public /* synthetic */ GroupProfileActivity f1600a;

    dc(GroupProfileActivity groupProfileActivity) {
        this.f1600a = groupProfileActivity;
    }

    public final void onClick(View view) {
        switch (view.getId()) {
            case R.id.view_showmemberlist /*2131165497*/:
                GroupProfileActivity.h(this.f1600a);
                return;
            case R.id.groupfeed_layout /*2131165697*/:
                Intent intent = new Intent(this.f1600a, GroupFeedsActivity.class);
                intent.putExtra("gid", this.f1600a.l);
                this.f1600a.startActivity(intent);
                return;
            case R.id.layout_level /*2131165705*/:
                Intent intent2 = new Intent(this.f1600a, GroupLevelActivity.class);
                intent2.putExtra("gid", this.f1600a.l);
                this.f1600a.startActivityForResult(intent2, 14);
                return;
            case R.id.layout_site /*2131165707*/:
                Intent intent3 = new Intent(this.f1600a, SiteGroupsActivity.class);
                intent3.putExtra("siteid", this.f1600a.r.J);
                intent3.putExtra("sitename", this.f1600a.r.K);
                this.f1600a.startActivity(intent3);
                return;
            case R.id.layout_ower /*2131165710*/:
                Intent intent4 = new Intent(this.f1600a, OtherProfileActivity.class);
                intent4.putExtra("tag", "internet");
                intent4.putExtra("momoid", this.f1600a.r.g);
                this.f1600a.startActivity(intent4);
                return;
            case R.id.profile_layout_chat /*2131166283*/:
                new k("C", "C3104").e();
                Intent intent5 = new Intent(this.f1600a.getApplicationContext(), GroupChatActivity.class);
                intent5.putExtra("remoteGroupID", this.f1600a.r == null ? this.f1600a.l : this.f1600a.r.b);
                this.f1600a.startActivity(intent5);
                return;
            case R.id.profile_layout_apply /*2131166285*/:
                new k("C", "C3103").e();
                new dv(this.f1600a, this.f1600a).execute(new Object[]{this.f1600a.l});
                return;
            case R.id.profile_layout_cancel_create /*2131166286*/:
                new k("C", "C64602").e();
                n.a(this.f1600a, this.f1600a.getString(R.string.group_profile_cancel_create_tip), new dd(this)).show();
                return;
            default:
                return;
        }
    }
}
