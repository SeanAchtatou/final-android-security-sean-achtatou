package com.immomo.momo.android.activity;

import android.view.MotionEvent;
import android.view.View;

final class my implements View.OnTouchListener {
    my() {
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case 0:
            case 1:
                if (view == null || view.hasFocus()) {
                    return false;
                }
                view.requestFocus();
                return false;
            default:
                return false;
        }
    }
}
