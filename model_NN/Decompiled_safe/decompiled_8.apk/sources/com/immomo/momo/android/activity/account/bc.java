package com.immomo.momo.android.activity.account;

import android.os.Handler;
import android.text.SpannableStringBuilder;
import android.text.method.LinkMovementMethod;
import android.text.style.ForegroundColorSpan;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.broadcast.y;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.util.ao;
import com.immomo.momo.util.e;
import com.immomo.momo.util.k;
import com.immomo.momo.util.m;

public final class bc extends ab implements TextView.OnEditorActionListener {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public m f956a = new m("test_momo", "[ -- StepCheckVerify -- ]");
    /* access modifiers changed from: private */
    public int b = 60;
    private y c = null;
    /* access modifiers changed from: private */
    public bf d = null;
    /* access modifiers changed from: private */
    public TextView e = null;
    /* access modifiers changed from: private */
    public EditText f = null;
    /* access modifiers changed from: private */
    public Button g = null;
    private TextView h = null;
    private View i = null;
    /* access modifiers changed from: private */
    public RegisterActivityWithP j = null;
    /* access modifiers changed from: private */
    public Handler k = new bd(this);
    private Animation l = null;
    /* access modifiers changed from: private */
    public bm m;

    public bc(bf bfVar, View view, RegisterActivityWithP registerActivityWithP) {
        super(view);
        this.d = bfVar;
        this.j = registerActivityWithP;
        registerActivityWithP.c(RegisterActivityWithP.i);
        this.f = (EditText) a((int) R.id.rg_et_verifycode);
        this.f.setOnEditorActionListener(this);
        this.e = (TextView) a((int) R.id.rg_tv_phonenumber);
        this.h = (TextView) a((int) R.id.rg_link_nocode);
        this.i = a((int) R.id.rg_layout_loading);
        ImageView imageView = (ImageView) this.i.findViewById(R.id.rg_iv_loading);
        if (imageView.getDrawable() != null && this.l == null) {
            this.l = AnimationUtils.loadAnimation(this.j, R.anim.loading);
            imageView.startAnimation(this.l);
        }
        this.g = (Button) a((int) R.id.rg_et_resend);
        this.g.setOnClickListener(new bf(this));
    }

    static /* synthetic */ void a(bc bcVar, String str) {
        bcVar.d.e = str;
        bcVar.f.setText(str);
        bcVar.c();
    }

    public final boolean a() {
        if (a(this.f)) {
            ao.b("请输入验证码");
            this.j.a((TextView) this.f);
            return false;
        }
        this.d.e = this.f.getText().toString();
        return true;
    }

    public final void b() {
        this.h.setText("总是收不到验证码？");
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(this.h.getText());
        spannableStringBuilder.setSpan(new bg(this), 0, 9, 33);
        this.h.setMovementMethod(LinkMovementMethod.getInstance());
        this.h.setText(spannableStringBuilder);
        e.a(this.h, 0, 9);
        if ("+86".equals(this.d.c)) {
            this.h.setVisibility(0);
        } else {
            this.h.setVisibility(8);
            this.h.setText(PoiTypeDef.All);
        }
        if (!this.d.f) {
            if (this.c == null) {
                this.c = new y(this.j);
            }
            this.c.a(new be(this));
        }
        if (this.d.f) {
            this.f.setEnabled(false);
            this.g.setEnabled(false);
            this.g.setWidth(-1);
            this.g.setText((int) R.string.reg_verify_passed);
            String str = "(" + this.d.c + ")" + this.d.b;
            String str2 = "你的手机号 " + str + "已通过验证";
            this.e.setText(str2);
            SpannableStringBuilder spannableStringBuilder2 = new SpannableStringBuilder(str2);
            spannableStringBuilder2.setSpan(new ForegroundColorSpan(g.c((int) R.color.notice_font)), str2.indexOf(str), str.length() + str2.indexOf(str), 34);
            this.e.setText(spannableStringBuilder2);
            this.f.setVisibility(8);
            this.h.setVisibility(8);
            this.i.setVisibility(8);
            return;
        }
        this.g.setWidth(g.a(180.0f));
        this.f.setEnabled(true);
        this.f.setVisibility(0);
        this.h.setVisibility(0);
        this.i.setVisibility(0);
        String str3 = "(" + this.d.c + ")" + this.d.b;
        String str4 = "验证码已经发送到 (" + this.d.c + ")" + this.d.b;
        this.e.setText(str4);
        SpannableStringBuilder spannableStringBuilder3 = new SpannableStringBuilder(str4);
        spannableStringBuilder3.setSpan(new ForegroundColorSpan(g.c((int) R.color.notice_font)), str4.indexOf(str3), str3.length() + str4.indexOf(str3), 34);
        this.e.setText(spannableStringBuilder3);
        if (this.j.u) {
            this.j.u = false;
            this.b = 60;
            this.f.setText(PoiTypeDef.All);
            if (!this.k.hasMessages(2245)) {
                this.k.sendEmptyMessage(2245);
            }
        }
    }

    public final void c() {
        if (this.d.f) {
            if (this.c != null) {
                this.j.unregisterReceiver(this.c);
                this.c = null;
            }
            this.j.d(2);
        } else if (a()) {
            this.j.b(new bi(this, this.j));
        }
    }

    public final void d() {
        if (this.c != null) {
            this.j.unregisterReceiver(this.c);
            this.c = null;
        }
        this.j.i();
    }

    public final void e() {
        new k("PI", "P122").e();
    }

    public final void f() {
        new k("PO", "P122").e();
    }

    public final boolean onEditorAction(TextView textView, int i2, KeyEvent keyEvent) {
        if (textView.getId() != R.id.rg_et_verifycode || 5 != i2) {
            return false;
        }
        c();
        return true;
    }
}
