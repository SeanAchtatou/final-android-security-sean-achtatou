package com.immomo.momo.android.activity;

import android.graphics.Bitmap;

final class dx implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ Bitmap f1264a;

    dx(Bitmap bitmap) {
        this.f1264a = bitmap;
    }

    public final void run() {
        if (this.f1264a != null && !this.f1264a.isRecycled()) {
            this.f1264a.recycle();
        }
    }
}
