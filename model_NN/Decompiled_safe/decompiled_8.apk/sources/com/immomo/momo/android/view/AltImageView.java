package com.immomo.momo.android.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.support.v4.b.a;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.widget.ImageView;
import com.immomo.momo.R;
import com.immomo.momo.g;

public class AltImageView extends ImageView {

    /* renamed from: a  reason: collision with root package name */
    private String f2621a;
    private TextPaint b = null;
    private int c;
    private int d = 0;

    public AltImageView(Context context) {
        super(context);
        setSizeWithSp(13);
        this.d = getResources().getColor(R.color.font_value);
    }

    public AltImageView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        setSizeWithSp(13);
        this.d = getResources().getColor(R.color.font_value);
    }

    public AltImageView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        setSizeWithSp(13);
        this.d = getResources().getColor(R.color.font_value);
    }

    public String getAlt() {
        return this.f2621a;
    }

    public int getColor() {
        return this.d;
    }

    public int getSize() {
        return this.c;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        int i;
        if (getDrawable() != null || a.a((CharSequence) this.f2621a)) {
            super.onDraw(canvas);
            return;
        }
        if (this.b == null) {
            this.b = new TextPaint();
            this.b.setTextSize((float) this.c);
            this.b.setAntiAlias(true);
            this.b.setColor(this.d);
        }
        String str = this.f2621a;
        float measureText = this.b.measureText("[" + str + "]");
        while (true) {
            i = (int) measureText;
            if (i > getMeasuredWidth()) {
                if (str.length() <= 2) {
                    str = "图片";
                    break;
                } else {
                    str = this.f2621a.substring(0, str.length() - 1);
                    measureText = this.b.measureText("[" + str + "]");
                }
            } else {
                break;
            }
        }
        canvas.drawText("[" + str + "]", (float) ((getMeasuredWidth() - i) / 2), (((float) getMeasuredHeight()) - this.b.getTextSize()) / 2.0f, this.b);
    }

    public void setAlt(String str) {
        this.f2621a = str;
    }

    public void setColor(int i) {
        this.d = i;
    }

    public void setColorWithResouse(int i) {
        this.d = getResources().getColor(i);
    }

    public void setImageBitmap(Bitmap bitmap) {
        if (bitmap == null) {
            setImageDrawable(null);
        } else {
            super.setImageBitmap(bitmap);
        }
    }

    public void setSize(int i) {
        this.c = i;
    }

    public void setSizeWithSp(int i) {
        this.c = g.b((float) i);
    }
}
