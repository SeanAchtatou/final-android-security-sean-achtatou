package com.immomo.momo.android.broadcast;

import android.content.Context;
import android.content.IntentFilter;
import com.baidu.location.LocationClientOption;

public final class y extends b {
    public y(Context context) {
        super(context);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.provider.Telephony.SMS_RECEIVED");
        intentFilter.setPriority(LocationClientOption.MIN_SCAN_SPAN);
        a(intentFilter);
    }
}
