package com.immomo.momo.android.activity.common;

import android.content.DialogInterface;
import android.content.Intent;
import com.immomo.momo.android.activity.setting.CommunityBindActivity;

final class ap implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ an f1085a;

    ap(an anVar) {
        this.f1085a = anVar;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        Intent intent = new Intent(this.f1085a.f(), CommunityBindActivity.class);
        intent.putExtra("type", 1);
        intent.putExtra("value_enforce", 1);
        this.f1085a.d.a(intent, 23);
    }
}
