package com.immomo.momo.android.activity;

import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Message;

final class da extends Handler {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ EditUserProfileActivity f1215a;

    da(EditUserProfileActivity editUserProfileActivity) {
        this.f1215a = editUserProfileActivity;
    }

    public final void handleMessage(Message message) {
        switch (message.what) {
            case 11:
                String string = message.getData().getString("guid");
                EditUserProfileActivity.a(this.f1215a, string, (Bitmap) message.obj);
                break;
        }
        super.handleMessage(message);
    }
}
