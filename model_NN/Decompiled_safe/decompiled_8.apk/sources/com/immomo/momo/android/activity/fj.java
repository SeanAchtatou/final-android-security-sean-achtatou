package com.immomo.momo.android.activity;

import android.graphics.Bitmap;
import com.immomo.momo.android.c.g;
import com.immomo.momo.android.view.photoview.PhotoView;

final class fj implements g {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ImageBrowserActivity f1492a;
    private final /* synthetic */ fn b;
    private final /* synthetic */ PhotoView c;

    fj(ImageBrowserActivity imageBrowserActivity, fn fnVar, PhotoView photoView) {
        this.f1492a = imageBrowserActivity;
        this.b = fnVar;
        this.c = photoView;
    }

    public final /* synthetic */ void a(Object obj) {
        Bitmap bitmap = (Bitmap) obj;
        if (bitmap != null) {
            this.b.g = bitmap;
            if (this.b.b() == null) {
                this.f1492a.w.post(new fk(this.c, this.b));
                return;
            }
            return;
        }
        this.b.f1496a = false;
    }
}
