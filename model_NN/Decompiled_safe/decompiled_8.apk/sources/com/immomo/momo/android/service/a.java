package com.immomo.momo.android.service;

import com.immomo.momo.service.an;
import com.immomo.momo.service.bean.ba;
import java.io.File;
import java.util.HashSet;
import java.util.List;

final class a implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ Cleaner f2603a;

    a(Cleaner cleaner) {
        this.f2603a = cleaner;
    }

    public final void run() {
        File fileStreamPath;
        List<ba> c = new an().c();
        long currentTimeMillis = System.currentTimeMillis();
        HashSet<String> hashSet = new HashSet<>();
        HashSet hashSet2 = new HashSet();
        for (ba baVar : c) {
            try {
                if (baVar.h() == null || currentTimeMillis > baVar.h().getTime()) {
                    if (!com.immomo.a.a.f.a.a(baVar.i())) {
                        hashSet.add(baVar.i());
                    }
                    if (!com.immomo.a.a.f.a.a(baVar.k())) {
                        hashSet.add(baVar.k());
                    }
                    if (!com.immomo.a.a.f.a.a(baVar.j())) {
                        hashSet.add(baVar.j());
                    }
                } else {
                    if (!com.immomo.a.a.f.a.a(baVar.i())) {
                        hashSet2.add(baVar.i());
                    }
                    if (!com.immomo.a.a.f.a.a(baVar.k())) {
                        hashSet2.add(baVar.k());
                    }
                    if (!com.immomo.a.a.f.a.a(baVar.j())) {
                        hashSet2.add(baVar.j());
                    }
                }
            } catch (Exception e) {
            }
        }
        if (!hashSet.isEmpty()) {
            for (String str : hashSet) {
                if (!hashSet2.contains(str) && (fileStreamPath = this.f2603a.getFileStreamPath(str)) != null && fileStreamPath.exists()) {
                    fileStreamPath.delete();
                }
            }
        }
        for (File file : this.f2603a.getFilesDir().listFiles()) {
            if (file.isFile()) {
                String name = file.getName();
                if (name.startsWith("cp_") && !hashSet2.contains(name) && !hashSet.contains(name)) {
                    file.delete();
                }
            }
        }
    }
}
