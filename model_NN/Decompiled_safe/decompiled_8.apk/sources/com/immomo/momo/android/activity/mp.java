package com.immomo.momo.android.activity;

import android.content.Context;
import com.immomo.momo.a;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.g;
import com.immomo.momo.protocol.a.w;
import com.immomo.momo.service.aq;
import com.immomo.momo.util.u;
import java.io.File;

final class mp extends d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ VisitorListActivity f2007a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public mp(VisitorListActivity visitorListActivity, Context context) {
        super(context);
        this.f2007a = visitorListActivity;
        visitorListActivity.a(new v(context, this));
    }

    /* access modifiers changed from: protected */
    public final Object a(Object... objArr) {
        w.a().b();
        aq unused = this.f2007a.k;
        if (u.c("uservisitorlist")) {
            u.a("uservisitorlist");
        }
        File file = new File(a.u(), "visitors");
        if (file.exists()) {
            file.delete();
        }
        if (u.c("uservisitorcount")) {
            u.a("uservisitorcount");
        }
        g.r().a("uservisitorcount");
        return null;
    }

    /* access modifiers changed from: protected */
    public final void a(Object obj) {
        this.f2007a.p = true;
        if (this.f2007a.j != null) {
            this.f2007a.j.b();
        }
        this.f2007a.h.k();
        this.f2007a.c(0);
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.f2007a.p();
    }
}
