package com.immomo.momo.android.pay;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.SpannableStringBuilder;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.a.a.f.a;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.ah;
import com.immomo.momo.android.view.PaymentButton;
import com.immomo.momo.android.view.a.n;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.g;
import com.immomo.momo.service.aq;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.util.e;
import com.immomo.momo.util.k;
import com.unicom.dcLoader.c;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import mm.purchasesdk.PurchaseCode;
import org.json.JSONObject;

public class BuyMemberActivity extends ah implements View.OnClickListener {
    public static final String h = ("http://m.immomo.com/inc/android/vipterms.html?v=" + g.z());
    /* access modifiers changed from: private */
    public boolean A = false;
    /* access modifiers changed from: private */
    public boolean B = false;
    /* access modifiers changed from: private */
    public boolean C = false;
    /* access modifiers changed from: private */
    public bk D;
    /* access modifiers changed from: private */
    public m E = null;
    /* access modifiers changed from: private */
    public Handler F = new d(this);
    /* access modifiers changed from: private */
    public v G = null;
    /* access modifiers changed from: private */
    public boolean H = false;
    /* access modifiers changed from: private */
    public String I;
    private List J = new ArrayList();
    private List K = new ArrayList();
    /* access modifiers changed from: private */
    public View i = null;
    private LinearLayout j = null;
    private LinearLayout k = null;
    private TextView l = null;
    private TextView m = null;
    /* access modifiers changed from: private */
    public List n = new ArrayList();
    /* access modifiers changed from: private */
    public List o = new ArrayList();
    /* access modifiers changed from: private */
    public List p = new ArrayList();
    /* access modifiers changed from: private */
    public List q = new ArrayList();
    /* access modifiers changed from: private */
    public List r = new ArrayList();
    /* access modifiers changed from: private */
    public List s = new ArrayList();
    /* access modifiers changed from: private */
    public Button t = null;
    /* access modifiers changed from: private */
    public String u;
    /* access modifiers changed from: private */
    public boolean v = false;
    private boolean w = false;
    /* access modifiers changed from: private */
    public String x;
    /* access modifiers changed from: private */
    public String y;
    /* access modifiers changed from: private */
    public String z;

    static /* synthetic */ void a(BuyMemberActivity buyMemberActivity) {
        try {
            if (buyMemberActivity.G != null && buyMemberActivity.G.isShowing() && !buyMemberActivity.isFinishing()) {
                buyMemberActivity.G.dismiss();
            }
        } catch (Throwable th) {
        }
    }

    static /* synthetic */ void a(BuyMemberActivity buyMemberActivity, List list) {
        buyMemberActivity.j.removeAllViews();
        int c = c(list.size());
        for (int i2 = 0; i2 < c; i2++) {
            View inflate = buyMemberActivity.getLayoutInflater().inflate((int) R.layout.listitem_recharge, (ViewGroup) null);
            buyMemberActivity.j.addView(inflate);
            buyMemberActivity.J.add((PaymentButton) inflate.findViewById(R.id.btn_left));
            if (i2 != c - 1 || list.size() == c * 2) {
                buyMemberActivity.J.add((PaymentButton) inflate.findViewById(R.id.btn_right));
            }
        }
        for (int i3 = 0; i3 < list.size(); i3++) {
            PaymentButton paymentButton = (PaymentButton) buyMemberActivity.J.get(i3);
            paymentButton.setVisibility(0);
            paymentButton.setOnClickListener(buyMemberActivity);
            at atVar = (at) list.get(i3);
            paymentButton.setLeftText(atVar.b);
            paymentButton.setTag(atVar);
            paymentButton.setSelected(atVar.f2538a);
            if (atVar.f2538a) {
                buyMemberActivity.a(atVar);
            }
        }
    }

    private void a(at atVar) {
        ArrayList arrayList;
        this.k.removeAllViews();
        this.K.clear();
        switch (atVar.c) {
            case 0:
                arrayList = this.o;
                break;
            case 1:
                arrayList = this.p;
                break;
            case 2:
            default:
                arrayList = new ArrayList();
                break;
            case 3:
                arrayList = this.q;
                break;
            case 4:
                arrayList = this.r;
                break;
            case 5:
                arrayList = this.s;
                break;
        }
        int c = c(arrayList.size());
        for (int i2 = 0; i2 < c; i2++) {
            View inflate = getLayoutInflater().inflate((int) R.layout.listitem_recharge, (ViewGroup) null);
            this.k.addView(inflate);
            this.K.add((PaymentButton) inflate.findViewById(R.id.btn_left));
            if (i2 != c - 1 || arrayList.size() == c * 2) {
                this.K.add((PaymentButton) inflate.findViewById(R.id.btn_right));
            }
        }
        for (int i3 = 0; i3 < arrayList.size(); i3++) {
            PaymentButton paymentButton = (PaymentButton) this.K.get(i3);
            paymentButton.setVisibility(0);
            paymentButton.setOnClickListener(this);
            au auVar = (au) arrayList.get(i3);
            paymentButton.setLeftText(auVar.c);
            paymentButton.setPromotionText(a.a(auVar.f) ? null : auVar.f);
            paymentButton.setTag(auVar);
            paymentButton.setSelected(auVar.e);
            if (auVar.e) {
                this.m.setText(auVar.d);
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(String str, int i2) {
        if (this.A) {
            b(str, i2);
            return;
        }
        Message message = new Message();
        message.what = 125;
        HashMap hashMap = new HashMap();
        hashMap.put("data", str);
        hashMap.put("count", Integer.valueOf(i2));
        message.obj = hashMap;
        b(new r(this, this, message));
    }

    /* access modifiers changed from: private */
    public void b(String str, int i2) {
        HashMap hashMap = new HashMap();
        String string = android.support.v4.b.a.a(str, ";").getString("result");
        String substring = string.substring(1, string.length() - 1);
        String substring2 = substring.substring(0, substring.indexOf("&sign_type="));
        JSONObject a2 = android.support.v4.b.a.a(substring, "&");
        a2.getString("sign_type").replace("\"", PoiTypeDef.All);
        String replace = a2.getString("sign").replace("\"", PoiTypeDef.All);
        String replace2 = a2.getString("out_trade_no").replace("\"", PoiTypeDef.All);
        hashMap.put("sign", replace);
        hashMap.put("content", substring2);
        hashMap.put("out_trade_no", replace2);
        hashMap.put("product_id", this.x);
        hashMap.put("in_trade_no", this.A ? this.z : this.y);
        if (this.v) {
            hashMap.put("remoteid", this.u);
            b(new l(this, this, hashMap, i2));
            return;
        }
        b(new k(this, this, hashMap, i2));
    }

    private static int c(int i2) {
        return i2 % 2 == 0 ? i2 / 2 : (i2 / 2) + 1;
    }

    static /* synthetic */ c c(String str) {
        if (!"single".equals(str)) {
            if ("sub".equals(str)) {
                return c.sub;
            }
            if ("unsub".equals(str)) {
                return c.unsub;
            }
        }
        return c.single;
    }

    /* access modifiers changed from: private */
    public void d(String str) {
        n.b(this, str, new f()).show();
    }

    static /* synthetic */ void p(BuyMemberActivity buyMemberActivity) {
        new k("U", "U72").e();
        buyMemberActivity.y();
        if (!buyMemberActivity.A) {
            buyMemberActivity.A = true;
            buyMemberActivity.z = buyMemberActivity.y;
        }
    }

    static /* synthetic */ void q(BuyMemberActivity buyMemberActivity) {
        new k("U", "U71").e();
        buyMemberActivity.A = false;
        buyMemberActivity.z();
        buyMemberActivity.x();
        if (buyMemberActivity.w) {
            buyMemberActivity.sendBroadcast(new Intent(com.immomo.momo.android.broadcast.n.f2357a));
            buyMemberActivity.startActivity(new Intent(buyMemberActivity, MemberCenterActivity.class));
            buyMemberActivity.finish();
        }
    }

    static /* synthetic */ void s(BuyMemberActivity buyMemberActivity) {
        buyMemberActivity.n.clear();
        buyMemberActivity.o.clear();
        buyMemberActivity.p.clear();
        buyMemberActivity.q.clear();
        buyMemberActivity.r.clear();
        buyMemberActivity.J.clear();
        buyMemberActivity.s.clear();
        buyMemberActivity.K.clear();
    }

    private Map u() {
        au auVar;
        HashMap hashMap = new HashMap();
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= this.K.size()) {
                return hashMap;
            }
            if (!((PaymentButton) this.K.get(i3)).isSelected() || (auVar = (au) ((PaymentButton) this.K.get(i3)).getTag()) == null) {
                i2 = i3 + 1;
            } else {
                hashMap.put("body", auVar.d);
                hashMap.put("subject", auVar.c);
                hashMap.put("product_id", auVar.f2539a);
                hashMap.put("total_fee", android.support.v4.b.a.b(auVar.b));
                this.x = auVar.f2539a;
                if (this.v) {
                    hashMap.put("purpose", "3");
                    hashMap.put("remoteid", this.u);
                } else {
                    hashMap.put("purpose", "2");
                }
                return hashMap;
            }
        }
    }

    /* access modifiers changed from: private */
    public String v() {
        try {
            InputStream open = getAssets().open("premessable.txt");
            if (open != null) {
                return new String(android.support.v4.b.a.a(open));
            }
        } catch (Exception e) {
            this.e.a((Throwable) e);
        }
        return PoiTypeDef.All;
    }

    private int w() {
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= this.J.size()) {
                return -1;
            }
            if (((PaymentButton) this.J.get(i3)).isSelected()) {
                return ((at) ((PaymentButton) this.J.get(i3)).getTag()).e;
            }
            i2 = i3 + 1;
        }
    }

    /* access modifiers changed from: private */
    public void x() {
        this.l.setText(String.valueOf(this.f.r()) + g.a((int) R.string.gold_str));
    }

    /* access modifiers changed from: private */
    public void y() {
        for (int i2 = 0; i2 < this.J.size(); i2++) {
            ((PaymentButton) this.J.get(i2)).setEnabled(((PaymentButton) this.J.get(i2)).isSelected());
        }
        for (int i3 = 0; i3 < this.K.size(); i3++) {
            ((PaymentButton) this.K.get(i3)).setEnabled(((PaymentButton) this.K.get(i3)).isSelected());
        }
    }

    /* access modifiers changed from: private */
    public void z() {
        for (int i2 = 0; i2 < this.J.size(); i2++) {
            ((PaymentButton) this.J.get(i2)).setEnabled(true);
        }
        for (int i3 = 0; i3 < this.K.size(); i3++) {
            ((PaymentButton) this.K.get(i3)).setEnabled(true);
        }
    }

    /* access modifiers changed from: protected */
    public final void b(Bundle bundle) {
        int i2 = R.string.giftmember_title;
        super.b(bundle);
        setContentView((int) R.layout.activity_buymember);
        setTitle(this.v ? R.string.giftmember_title : R.string.buymember_title);
        this.l = (TextView) findViewById(R.id.tv_mybalance);
        this.m = (TextView) findViewById(R.id.tv_paybody);
        this.l.setText(this.f.r() + " " + g.a((int) R.string.gold_str));
        this.i = findViewById(R.id.layout_pay);
        this.i.setVisibility(8);
        this.j = (LinearLayout) findViewById(R.id.layout_payment_container);
        this.k = (LinearLayout) findViewById(R.id.layout_payprice_container);
        this.t = (Button) findViewById(R.id.btn_submit);
        if (bundle == null || !bundle.getBoolean("from_saveinstance", false)) {
            this.e.b((Object) "adsf getIntent---->");
            Bundle extras = getIntent().getExtras();
            if (extras != null) {
                this.u = (String) extras.get("key_remoteid");
                if (extras.containsKey("key_is_openmember")) {
                    this.w = ((Boolean) extras.get("key_is_openmember")).booleanValue();
                }
                if (this.u != null) {
                    this.v = ((Boolean) extras.get("key_isGift")).booleanValue();
                }
            }
        } else {
            this.e.b((Object) "adsf savedInstanceState---->");
            this.u = (String) bundle.get("key_remoteid");
            this.v = ((Boolean) bundle.get("key_isGift")).booleanValue();
            this.w = ((Boolean) bundle.get("key_is_openmember")).booleanValue();
        }
        if (!this.v) {
            i2 = R.string.buymember_title;
        }
        setTitle(i2);
        this.t.setText(this.v ? R.string.payvip_gift : R.string.payvip_buy);
        b(new p(this, this, "正在初始化信息....", false));
        this.t.setOnClickListener(this);
        TextView textView = (TextView) findViewById(R.id.buymenmber_tv_protocol);
        String charSequence = textView.getText().toString();
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(charSequence);
        spannableStringBuilder.setSpan(new e(this), charSequence.indexOf("陌陌会员协议"), charSequence.indexOf("陌陌会员协议") + 6, 33);
        textView.setMovementMethod(LinkMovementMethod.getInstance());
        textView.setText(spannableStringBuilder);
        e.a(textView, charSequence.indexOf("陌陌会员协议"), charSequence.indexOf("陌陌会员协议") + 6);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        switch (i2) {
            case PurchaseCode.QUERY_OK /*101*/:
                b(new p(this, this, "正在更新数据", true));
                return;
            default:
                return;
        }
    }

    public void onClick(View view) {
        int i2;
        switch (view.getId()) {
            case R.id.btn_submit /*2131165424*/:
                if (!this.H) {
                    int i3 = 0;
                    while (true) {
                        if (i3 >= this.J.size()) {
                            i2 = -1;
                        } else if (((PaymentButton) this.J.get(i3)).isSelected()) {
                            i2 = ((at) ((PaymentButton) this.J.get(i3)).getTag()).c;
                        } else {
                            i3++;
                        }
                    }
                    switch (i2) {
                        case 0:
                            Map u2 = u();
                            if (this.v) {
                                if (!a.a(this.u)) {
                                    bf b = new aq().b(this.u);
                                    if (b == null) {
                                        b = new bf(this.u);
                                    }
                                    n a2 = n.a(this, "你将向好友" + b.h() + "赠送会员" + ((String) u2.get("subject")) + ", 消耗" + ((String) u2.get("total_fee")) + g.a((int) R.string.gold_str), new g(this));
                                    a2.setTitle("付费提示");
                                    a(a2);
                                    break;
                                }
                            } else {
                                n a3 = n.a(this, "你购买" + ((String) u2.get("subject")) + ", 消耗" + ((String) u2.get("total_fee")) + g.a((int) R.string.gold_str), new h(this));
                                a3.setTitle("付费提示");
                                a(a3);
                                break;
                            }
                            break;
                        case 1:
                            if (new al(this).a()) {
                                try {
                                    b(new w(this, this, u()));
                                    break;
                                } catch (Exception e) {
                                    break;
                                }
                            }
                            break;
                        case 3:
                        case 4:
                            try {
                                Map u3 = u();
                                u3.put("cashier", new StringBuilder(String.valueOf(w())).toString());
                                b(new n(this, this, u3));
                                break;
                            } catch (Exception e2) {
                                break;
                            }
                        case 5:
                            if (!g.p()) {
                                d("该支付选项需要安卓系统2.2及以上");
                                break;
                            } else {
                                y();
                                this.t.setText((int) R.string.payvip_btn_recheck);
                                if (!this.C) {
                                    b(new y(this, this, u()));
                                    break;
                                } else {
                                    b(new m(this, this, this.D.b, 4));
                                    break;
                                }
                            }
                    }
                } else {
                    try {
                        a(this.I, 4);
                        break;
                    } catch (Exception e3) {
                        this.e.a((Throwable) e3);
                        break;
                    }
                }
                break;
        }
        if (view.getTag() instanceof at) {
            at atVar = (at) view.getTag();
            if (!atVar.f2538a) {
                if (atVar.c != 2 || android.support.v4.b.a.a((CharSequence) atVar.d)) {
                    for (int i4 = 0; i4 < this.J.size(); i4++) {
                        PaymentButton paymentButton = (PaymentButton) this.J.get(i4);
                        at atVar2 = (at) paymentButton.getTag();
                        atVar2.f2538a = atVar2.c == atVar.c;
                        paymentButton.setSelected(atVar2.f2538a);
                        if (atVar2.f2538a) {
                            a(atVar2);
                        }
                    }
                    return;
                }
                Intent intent = new Intent(this, AuthWebviewActivity.class);
                intent.putExtra(AuthWebviewActivity.h, atVar.d);
                intent.putExtra(AuthWebviewActivity.i, "购买会员");
                startActivityForResult(intent, PurchaseCode.QUERY_OK);
            }
        } else if (view.getTag() instanceof au) {
            au auVar = (au) view.getTag();
            if (!auVar.e) {
                for (int i5 = 0; i5 < this.K.size(); i5++) {
                    PaymentButton paymentButton2 = (PaymentButton) this.K.get(i5);
                    au auVar2 = (au) paymentButton2.getTag();
                    auVar2.e = auVar2.f2539a.equals(auVar.f2539a);
                    paymentButton2.setSelected(auVar2.e);
                    if (auVar2.e) {
                        this.m.setText(auVar2.d);
                    }
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        new k("PO", "P933").e();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        new k("PI", "P333").e();
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        bundle.putBoolean("from_saveinstance", true);
        bundle.putString("key_remoteid", this.u);
        bundle.putBoolean("key_isGift", this.v);
        bundle.putBoolean("key_is_openmember", this.w);
        super.onSaveInstanceState(bundle);
    }
}
