package com.immomo.momo.android.activity;

import android.os.Bundle;
import android.widget.ListAdapter;
import com.immomo.momo.R;
import com.immomo.momo.android.a.jm;
import com.immomo.momo.android.view.LoadingButton;
import com.immomo.momo.android.view.MomoRefreshListView;
import com.immomo.momo.android.view.bi;
import com.immomo.momo.android.view.bu;
import com.immomo.momo.g;
import com.immomo.momo.service.aq;
import com.immomo.momo.util.u;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class VisitorListActivity extends ah implements bu {
    /* access modifiers changed from: private */
    public MomoRefreshListView h = null;
    /* access modifiers changed from: private */
    public LoadingButton i;
    /* access modifiers changed from: private */
    public jm j = null;
    /* access modifiers changed from: private */
    public aq k = null;
    /* access modifiers changed from: private */
    public mr l = null;
    /* access modifiers changed from: private */
    public Set m = null;
    /* access modifiers changed from: private */
    public int n = 0;
    private bi o;
    /* access modifiers changed from: private */
    public boolean p = false;

    /* access modifiers changed from: private */
    public void c(int i2) {
        if (i2 > 0) {
            setTitle("谁看过我 (" + i2 + ")");
        } else {
            setTitle("谁看过我");
        }
    }

    /* access modifiers changed from: protected */
    public final void b(Bundle bundle) {
        super.b(bundle);
        setContentView((int) R.layout.activity_visitorlist);
        this.h = (MomoRefreshListView) findViewById(R.id.listview);
        this.h.setLastFlushTime(this.g.b("visitor_lasttime_success"));
        this.h.setEnableLoadMoreFoolter(true);
        this.i = this.h.getFooterViewButton();
        this.o = new bi(this);
        this.o.a("清空");
        a(this.o, new mk(this));
        this.i.setOnProcessListener(new mm(this));
        this.h.setOnPullToRefreshListener$42b903f6(this);
        this.h.setOnItemClickListener(new mn(this));
        this.h.setOnCancelListener$135502(new mo(this));
        this.k = new aq();
        aq aqVar = this.k;
        c(u.c("uservisitorcount") ? ((Integer) u.b("uservisitorcount")).intValue() : ((Integer) g.r().b("uservisitorcount", 0)).intValue());
    }

    public final void b_() {
        b(new mr(this, this));
    }

    /* access modifiers changed from: protected */
    public final void d() {
        super.d();
        List d = this.k.d();
        this.j = new jm(this, d, this.h);
        this.h.setAdapter((ListAdapter) this.j);
        this.h.l();
        this.m = new HashSet(d);
        if (d.size() < 30) {
            this.i.setVisibility(8);
        }
    }

    /* access modifiers changed from: protected */
    public final void e() {
        super.e();
        d();
    }
}
