package com.immomo.momo.android.activity.plugin;

import android.content.Context;
import com.immomo.momo.R;
import com.immomo.momo.a.a;
import com.immomo.momo.android.c.d;
import com.immomo.momo.plugin.f.b;
import com.immomo.momo.service.aq;
import com.immomo.momo.util.ao;

final class ay extends d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ CommunityStatusActivity f2052a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ay(CommunityStatusActivity communityStatusActivity, Context context) {
        super(context);
        this.f2052a = communityStatusActivity;
        if (communityStatusActivity.v != null) {
            communityStatusActivity.v.cancel(true);
        }
        communityStatusActivity.v = this;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        return com.immomo.momo.protocol.a.d.d(this.f2052a.f.h, null);
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        if (exc instanceof a) {
            super.a(exc);
            return;
        }
        this.b.a((Throwable) exc);
        ao.g(R.string.plus_error_profile);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        b bVar = (b) obj;
        super.a(bVar);
        if (bVar != null) {
            this.f2052a.s = bVar;
            if (!android.support.v4.b.a.a((CharSequence) bVar.b)) {
                this.f2052a.k.setText(bVar.b);
                this.f2052a.f.q = bVar.b;
                new aq().b(this.f2052a.f);
            }
        }
    }
}
