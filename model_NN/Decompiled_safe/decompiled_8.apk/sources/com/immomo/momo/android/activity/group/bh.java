package com.immomo.momo.android.activity.group;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.protocol.a.o;
import com.immomo.momo.service.bean.a.e;

final class bh extends d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ GroupFeedProfileActivity f1559a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public bh(GroupFeedProfileActivity groupFeedProfileActivity, Context context) {
        super(context);
        this.f1559a = groupFeedProfileActivity;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        e c = o.a().c(this.f1559a.j);
        if (c.h == 2) {
            this.f1559a.m.f(this.f1559a.j);
        } else {
            this.f1559a.m.b(c);
        }
        return c;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        e eVar = (e) obj;
        this.f1559a.k = eVar;
        if (eVar.h == 2) {
            a("该留言已经被删除");
            this.f1559a.finish();
            return;
        }
        this.f1559a.w();
        this.f1559a.a(eVar);
    }
}
