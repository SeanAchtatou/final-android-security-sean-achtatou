package com.immomo.momo.android.activity.plugin;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.ao;
import com.immomo.momo.android.view.HeaderLayout;
import com.immomo.momo.android.view.a.v;

public class SubmitVerifyCodeActivity extends ao implements View.OnClickListener {
    private EditText h = null;
    private TextView i = null;
    private HeaderLayout j = null;
    /* access modifiers changed from: private */
    public String k = "+86";
    /* access modifiers changed from: private */
    public String l = PoiTypeDef.All;
    /* access modifiers changed from: private */
    public String m = PoiTypeDef.All;
    /* access modifiers changed from: private */
    public String n = PoiTypeDef.All;
    /* access modifiers changed from: private */
    public v o = null;

    /* access modifiers changed from: protected */
    public final void d() {
    }

    public void onClick(View view) {
        boolean z = true;
        switch (view.getId()) {
            case R.id.btn_back /*2131165286*/:
                finish();
                return;
            case R.id.btn_next /*2131165417*/:
                String trim = this.h.getText().toString().trim();
                if (trim == null || trim.length() <= 0) {
                    a((CharSequence) "请输入验证码");
                    this.h.requestFocus();
                    z = false;
                } else {
                    this.m = this.h.getText().toString();
                }
                if (z) {
                    new bt(this, this).execute(new String[0]);
                    return;
                }
                return;
            default:
                return;
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_submitverifycode);
        this.h = (EditText) findViewById(R.id.input_verifycode);
        this.i = (TextView) findViewById(R.id.rg_tv_phonenumber);
        this.j = (HeaderLayout) findViewById(R.id.layout_header);
        this.j.setTitleText("验证手机");
        findViewById(R.id.btn_next).setOnClickListener(this);
        findViewById(R.id.btn_back).setOnClickListener(this);
        this.k = getIntent().getStringExtra("areacode");
        this.l = getIntent().getStringExtra("phonenumber");
        this.n = getIntent().getStringExtra("password");
        this.i.setText("(" + this.k + ")" + this.l);
        this.e.a((Object) "onCreate...");
    }
}
