package com.immomo.momo.android.activity.emotestore;

import android.content.Intent;
import android.support.v4.b.a;
import android.view.View;
import com.immomo.momo.android.activity.OtherProfileActivity;

final class c implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ a f1316a;
    private final /* synthetic */ String b;

    c(a aVar, String str) {
        this.f1316a = aVar;
        this.b = str;
    }

    public final void onClick(View view) {
        if (!a.a((CharSequence) this.b)) {
            Intent intent = new Intent(this.f1316a.f1280a.getApplicationContext(), OtherProfileActivity.class);
            intent.putExtra("momoid", this.b);
            this.f1316a.f1280a.startActivity(intent);
        }
    }
}
