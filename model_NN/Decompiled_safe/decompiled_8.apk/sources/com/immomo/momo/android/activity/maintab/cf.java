package com.immomo.momo.android.activity.maintab;

import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Message;

final class cf extends Handler {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ cd f1878a;

    cf(cd cdVar) {
        this.f1878a = cdVar;
    }

    public final void handleMessage(Message message) {
        switch (message.what) {
            case 11:
                String string = message.getData().getString("guid");
                cd.a(this.f1878a, string, (Bitmap) message.obj);
                break;
        }
        super.handleMessage(message);
    }
}
