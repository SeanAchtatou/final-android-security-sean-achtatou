package com.immomo.momo.android.activity;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;
import com.immomo.momo.R;
import com.immomo.momo.android.view.HeaderLayout;

public class LocationServiceSettingActivity extends ah implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {
    private HeaderLayout h = null;
    private View i;
    private View j;

    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        super.a(bundle);
        setContentView((int) R.layout.activity_setting_locationservice);
        this.h = (HeaderLayout) findViewById(R.id.layout_header);
        this.h.setTitleText("定位服务");
        this.i = findViewById(R.id.setting_layout_location_system_setting);
        this.j = findViewById(R.id.layout_always_locationfailed);
        this.i.setOnClickListener(this);
        this.j.setOnClickListener(this);
    }

    /* access modifiers changed from: protected */
    public final void d() {
    }

    public void onCheckedChanged(CompoundButton compoundButton, boolean z) {
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.layout_always_locationfailed /*2131165866*/:
                startActivity(new Intent(this, CheckStatusActivity.class));
                return;
            case R.id.setting_layout_location_system_setting /*2131165882*/:
                Intent intent = new Intent();
                intent.setAction("android.settings.LOCATION_SOURCE_SETTINGS");
                intent.setFlags(268435456);
                try {
                    startActivity(intent);
                    return;
                } catch (ActivityNotFoundException e) {
                    this.e.a((Throwable) e);
                    intent.setAction("android.settings.SETTINGS");
                    try {
                        startActivity(intent);
                        return;
                    } catch (Exception e2) {
                        this.e.a((Throwable) e2);
                        return;
                    }
                }
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
    }
}
