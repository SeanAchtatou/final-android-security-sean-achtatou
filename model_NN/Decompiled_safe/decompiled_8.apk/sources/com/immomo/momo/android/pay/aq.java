package com.immomo.momo.android.pay;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import com.alipay.android.app.IAlixPay;

final class aq implements ServiceConnection {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ap f2535a;

    aq(ap apVar) {
        this.f2535a = apVar;
    }

    public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        synchronized (this.f2535a.f2534a) {
            this.f2535a.b = IAlixPay.Stub.asInterface(iBinder);
            this.f2535a.f2534a.notify();
        }
    }

    public final void onServiceDisconnected(ComponentName componentName) {
        this.f2535a.b = null;
    }
}
