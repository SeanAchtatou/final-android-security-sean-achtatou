package com.immomo.momo.android.c;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import com.immomo.momo.android.activity.fs;
import com.immomo.momo.service.a.ad;
import com.immomo.momo.service.bean.ak;
import java.io.File;
import java.util.Date;

public final class r extends o {
    public r(String str, g gVar, int i, fs fsVar) {
        super(str, gVar, i, fsVar);
    }

    public final void run() {
        try {
            if (this.b != null) {
                File file = this.b;
                Bitmap decodeFile = file.exists() ? BitmapFactory.decodeFile(file.getAbsolutePath()) : null;
                if (decodeFile != null) {
                    if (this.c == 3 || this.c == 15) {
                        this.f2389a = String.valueOf(this.f2389a) + "_s";
                    } else if (this.c == 2 || this.c == 16) {
                        this.f2389a = String.valueOf(this.f2389a) + "_l";
                    }
                    if (ad.g().a("i_imageid", this.f2389a)) {
                        ad.g().a(this.f2389a, new Date());
                    } else {
                        ak akVar = new ak();
                        akVar.f2990a = this.f2389a;
                        akVar.b = this.b.getAbsolutePath();
                        akVar.e = new Date();
                        akVar.d = this.c;
                        ad.g().b(akVar);
                    }
                    a(decodeFile);
                    return;
                }
            }
        } catch (Throwable th) {
        }
        super.run();
    }
}
