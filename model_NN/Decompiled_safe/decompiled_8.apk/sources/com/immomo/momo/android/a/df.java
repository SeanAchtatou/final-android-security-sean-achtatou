package com.immomo.momo.android.a;

import android.content.Context;
import com.immomo.momo.android.activity.ao;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.c;
import com.immomo.momo.protocol.a.p;
import com.immomo.momo.service.bean.ag;
import com.immomo.momo.service.bean.ah;
import com.immomo.momo.service.t;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

final class df extends d {

    /* renamed from: a  reason: collision with root package name */
    private ag f772a;
    private ah c;
    private /* synthetic */ da d;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public df(da daVar, Context context, ag agVar, ah ahVar) {
        super(context);
        this.d = daVar;
        this.c = ahVar;
        this.f772a = agVar;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        return c.a().b(String.valueOf(p.f2900a) + this.c.c(), (Map) null);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        ((ao) this.d.d).a(new v(this.d.d, this));
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        super.a(exc);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        try {
            String string = new JSONObject((String) obj).getString("msg");
            this.c.d();
            new t().a(this.f772a);
            a(string);
        } catch (JSONException e) {
            this.b.a((Throwable) e);
        }
        this.d.notifyDataSetChanged();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        ((ao) this.d.d).p();
    }
}
