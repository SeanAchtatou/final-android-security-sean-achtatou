package com.immomo.momo.android.a;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public abstract class a extends BaseAdapter {

    /* renamed from: a  reason: collision with root package name */
    protected List f671a;
    protected Context b;
    protected LayoutInflater c;
    private boolean d;

    public a(Context context) {
        this.d = true;
        this.b = null;
        this.c = null;
        this.b = context;
        this.c = (LayoutInflater) context.getSystemService("layout_inflater");
        this.f671a = new ArrayList();
    }

    public a(Context context, List list) {
        this(context);
        this.f671a = list;
    }

    public final View a(int i) {
        return this.c.inflate(i, (ViewGroup) null);
    }

    public final List a() {
        return this.f671a;
    }

    public final void a(int i, Object obj) {
        this.f671a.remove(i);
        this.f671a.add(i, obj);
        if (this.d) {
            notifyDataSetChanged();
        }
    }

    public void a(int i, Collection collection) {
        this.f671a.addAll(i, collection);
        if (this.d) {
            notifyDataSetChanged();
        }
    }

    public void a(Object obj) {
        this.f671a.add(obj);
        if (this.d) {
            notifyDataSetChanged();
        }
    }

    public final void a(Object obj, int i) {
        this.f671a.add(i, obj);
        if (this.d) {
            notifyDataSetChanged();
        }
    }

    public final void a(Collection collection) {
        this.f671a.clear();
        b(collection);
    }

    public final void a(Collection collection, boolean z) {
        this.f671a.addAll(collection);
        if (z) {
            notifyDataSetChanged();
        }
    }

    public final void a(boolean z) {
        this.f671a.clear();
        if (z) {
            notifyDataSetChanged();
        }
    }

    public void a(Object... objArr) {
        for (Object add : objArr) {
            this.f671a.add(add);
        }
        if (this.d) {
            notifyDataSetChanged();
        }
    }

    public final void b() {
        a(this.d);
    }

    public void b(int i) {
        this.f671a.remove(i);
        if (this.d) {
            notifyDataSetChanged();
        }
    }

    public void b(int i, Object obj) {
        this.f671a.add(i, obj);
        if (this.d) {
            notifyDataSetChanged();
        }
    }

    public void b(Object obj) {
        this.f671a.add(obj);
    }

    public void b(Collection collection) {
        a(collection, this.d);
    }

    public Object c(int i) {
        return this.f671a.remove(i);
    }

    public final void c() {
        this.d = false;
    }

    public boolean c(Object obj) {
        boolean remove = this.f671a.remove(obj);
        if (this.d) {
            notifyDataSetChanged();
        }
        return remove;
    }

    public final Context d() {
        return this.b;
    }

    public final void d(Object obj) {
        this.f671a.remove(obj);
    }

    public final int e(Object obj) {
        return this.f671a.indexOf(obj);
    }

    public final int f(Object obj) {
        return this.f671a.indexOf(obj);
    }

    public int getCount() {
        return this.f671a.size();
    }

    public Object getItem(int i) {
        return this.f671a.get(i);
    }

    public long getItemId(int i) {
        return (long) i;
    }

    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
        this.d = true;
    }
}
