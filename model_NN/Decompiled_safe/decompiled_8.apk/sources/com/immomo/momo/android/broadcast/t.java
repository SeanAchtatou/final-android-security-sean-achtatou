package com.immomo.momo.android.broadcast;

import android.content.Context;
import com.immomo.momo.g;

public final class t extends b {

    /* renamed from: a  reason: collision with root package name */
    public static final String f2363a = (String.valueOf(g.h()) + ".action.discusslist.add");
    public static final String b = (String.valueOf(g.h()) + ".action.discusslist.delete");
    public static final String c = (String.valueOf(g.h()) + ".action.discusslist.banded");
    public static final String d = (String.valueOf(g.h()) + ".action.discusslist.reflush.item");
    private static String e = (String.valueOf(g.h()) + ".action.discusslist.reflush.profile");

    public t(Context context) {
        super(context);
        a(b, f2363a, e, d, c);
    }
}
