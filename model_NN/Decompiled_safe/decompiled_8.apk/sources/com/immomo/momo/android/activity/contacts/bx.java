package com.immomo.momo.android.activity.contacts;

import com.immomo.momo.android.a.hf;

final class bx implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ bu f1178a;
    private final /* synthetic */ int b;
    private final /* synthetic */ String c;

    bx(bu buVar, int i, String str) {
        this.f1178a = buVar;
        this.b = i;
        this.c = str;
    }

    public final void run() {
        if (this.b < 0 || this.b > this.f1178a.f1175a.P.getCount()) {
            this.f1178a.f1175a.R = this.f1178a.f1175a.U.c();
            this.f1178a.f1175a.P.b(this.f1178a.f1175a.R);
        } else {
            this.f1178a.f1175a.U.a(((hf) this.f1178a.f1175a.P.getItem(this.b)).g, this.c);
        }
        this.f1178a.f1175a.P.notifyDataSetChanged();
    }
}
