package com.immomo.momo.android.pay;

import android.content.Context;
import com.immomo.momo.R;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.g;
import com.immomo.momo.protocol.a.a;
import com.immomo.momo.service.aq;
import java.util.ArrayList;

final class bb extends d {

    /* renamed from: a  reason: collision with root package name */
    private v f2545a = null;
    private String c = null;
    private boolean d = false;
    /* access modifiers changed from: private */
    public /* synthetic */ RechargeActivity e;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public bb(RechargeActivity rechargeActivity, Context context, String str, boolean z) {
        super(context);
        this.e = rechargeActivity;
        this.f2545a = new v(context);
        this.c = str;
        this.d = z;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        if (this.d) {
            this.e.f.b(a.a().a(new ArrayList(), new ArrayList(), new ArrayList(), new ArrayList(), new ArrayList(), this.e.f.r()));
        } else {
            RechargeActivity.p(this.e);
            this.e.f.b(a.a().a(this.e.n, this.e.o, this.e.p, this.e.q, this.e.r, this.e.f.r()));
        }
        new aq().a(this.e.f.r(), this.e.f.h);
        return true;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
        this.f2545a.setCancelable(true);
        this.f2545a.setOnCancelListener(new bc(this));
        this.f2545a.a(this.c);
        this.e.a(this.f2545a);
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        super.a(exc);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        this.e.l.setText(String.valueOf(this.e.f.r()) + " " + g.a((int) R.string.gold_str));
        if (this.d) {
            return;
        }
        if (!this.e.n.isEmpty()) {
            this.e.i.setVisibility(0);
            RechargeActivity.a(this.e, this.e.n);
            return;
        }
        this.e.i.setVisibility(8);
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.e.p();
    }
}
