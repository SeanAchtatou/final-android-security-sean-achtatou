package com.immomo.momo.android.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.maintab.MaintabActivity;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.g;
import com.immomo.momo.protocol.a.w;
import com.immomo.momo.service.aq;
import com.immomo.momo.service.bean.at;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.util.jni.Codec;
import com.immomo.momo.util.k;
import org.json.JSONObject;

final class ae extends d {

    /* renamed from: a  reason: collision with root package name */
    private bf f1022a = null;
    private aq c = null;
    private v d = null;
    private /* synthetic */ AlipayUserWelcomeActivity e;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ae(AlipayUserWelcomeActivity alipayUserWelcomeActivity, Context context, bf bfVar) {
        super(context);
        this.e = alipayUserWelcomeActivity;
        this.f1022a = bfVar;
        this.c = new aq(this.f1022a.h);
        this.f1022a.aE = at.a(g.c(), this.f1022a.h);
    }

    /* access modifiers changed from: protected */
    public final Object a(Object... objArr) {
        w.a().a(this.f1022a, this.f1022a.W);
        this.c.b(this.f1022a);
        SharedPreferences.Editor edit = g.d().f668a.a().edit();
        edit.putString("momoid", this.f1022a.h);
        edit.putString("cookie", Codec.c(this.f1022a.W));
        edit.commit();
        this.e.t().a(this.f1022a, this.f1022a.aE);
        this.f1022a = null;
        return null;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.d = new v(this.e, (int) R.string.login_success_init);
        this.d.setOnCancelListener(new af(this));
        this.e.a(this.d);
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        this.e.p();
        super.a(exc);
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("error", exc.toString());
            new k("U", "U95", jSONObject).e();
        } catch (Exception e2) {
        }
    }

    /* access modifiers changed from: protected */
    public final void a(Object obj) {
        Intent intent = new Intent(this.e.getApplicationContext(), MaintabActivity.class);
        intent.setFlags(268435456);
        this.e.getApplicationContext().startActivity(intent);
        this.e.finish();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.e.p();
        this.c.a();
    }
}
