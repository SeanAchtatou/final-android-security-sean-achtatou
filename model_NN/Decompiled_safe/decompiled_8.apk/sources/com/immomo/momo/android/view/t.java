package com.immomo.momo.android.view;

import android.graphics.Bitmap;
import com.immomo.momo.a;
import com.immomo.momo.android.activity.fs;
import com.immomo.momo.protocol.a.p;
import com.immomo.momo.service.bean.d;
import com.immomo.momo.util.h;
import com.immomo.momo.util.m;
import java.io.File;

public class t implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private m f2828a = new m(this);
    d b;
    private File c = null;

    public t(d dVar) {
        this.b = dVar;
        dVar.setImageLoading(true);
        this.c = new File(a.c(), android.support.v4.b.a.n(dVar.d));
    }

    private Bitmap a() {
        try {
            if (this.c.exists()) {
                this.f2828a.a((Object) ("loadFromLocal ->  " + this.c.getPath()));
                return android.support.v4.b.a.l(this.c.getPath());
            }
        } catch (Throwable th) {
            this.f2828a.a(th);
        }
        return null;
    }

    public void a(Bitmap bitmap) {
        if (bitmap != null) {
            try {
                new com.immomo.momo.service.a().a(this.b);
                bitmap.recycle();
            } catch (Exception e) {
            }
        }
    }

    public void run() {
        try {
            Bitmap a2 = a();
            if (a2 == null) {
                this.f2828a.a((Object) ("download banner->" + this.b.d));
                a2 = p.a(this.b.d, (fs) null).b;
                if (!this.c.exists()) {
                    this.c.createNewFile();
                }
                h.a(a2, this.c);
            }
            if (a2 != null) {
                this.b.a(a2);
                this.b.i = this.c;
            }
        } catch (Throwable th) {
            this.f2828a.a(th);
        } finally {
            this.b.setImageLoading(false);
            a(this.b.a());
        }
    }
}
