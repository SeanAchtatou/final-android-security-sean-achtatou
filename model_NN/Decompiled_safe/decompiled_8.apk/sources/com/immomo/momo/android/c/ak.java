package com.immomo.momo.android.c;

import android.content.Context;
import com.immomo.momo.a;
import com.immomo.momo.service.bean.bf;
import java.io.File;

public final class ak extends d {

    /* renamed from: a  reason: collision with root package name */
    public static boolean f2376a = false;
    private String c = null;
    private String d = null;
    private String e = null;

    public ak(Context context, bf bfVar) {
        super(context);
        this.c = bfVar.getLoadImageId();
        this.d = bfVar.W;
        this.e = bfVar.h;
    }

    public static boolean b(String str) {
        return new File(a.a(str), "avatarupload").exists();
    }

    public static void c(String str) {
        File file = new File(a.a(str), "avatarupload");
        if (file.exists()) {
            file.delete();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.h.a(java.lang.String, java.lang.String, int, boolean):void
     arg types: [java.lang.String, java.lang.String, int, int]
     candidates:
      com.immomo.momo.util.h.a(java.lang.String, android.graphics.Bitmap, int, boolean):java.io.File
      com.immomo.momo.util.h.a(java.lang.String, java.lang.String, int, boolean):void */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0043  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x004f  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0056  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x005c  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00c3  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x00d6  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object a(java.lang.Object... r15) {
        /*
            r14 = this;
            r12 = 3
            r10 = 2
            r5 = 1
            r1 = 0
            r3 = 0
            java.io.File r9 = new java.io.File
            java.lang.String r0 = r14.e
            java.io.File r0 = com.immomo.momo.a.a(r0)
            java.lang.String r2 = "avatarupload"
            r9.<init>(r0, r2)
            boolean r0 = r9.exists()
            if (r0 == 0) goto L_0x00de
            java.lang.String r0 = com.immomo.momo.util.h.a(r9)     // Catch:{ Exception -> 0x008f }
            org.json.JSONObject r7 = new org.json.JSONObject     // Catch:{ Exception -> 0x008f }
            r7.<init>(r0)     // Catch:{ Exception -> 0x008f }
            java.lang.String r0 = "avatarfile"
            java.lang.String r4 = r7.getString(r0)     // Catch:{ Exception -> 0x008f }
            java.lang.String r0 = "count"
            int r0 = r7.getInt(r0)     // Catch:{ Exception -> 0x008f }
            java.io.File r2 = new java.io.File     // Catch:{ Exception -> 0x00d0 }
            r2.<init>(r4)     // Catch:{ Exception -> 0x00d0 }
            r4 = 10
            if (r0 <= r4) goto L_0x00db
            r4 = r5
        L_0x0037:
            java.lang.String r6 = "guid"
            java.lang.String r6 = r7.optString(r6)     // Catch:{ Exception -> 0x00d4 }
            r8 = r7
            r7 = r0
            r0 = r2
            r2 = r6
        L_0x0041:
            if (r0 != 0) goto L_0x0049
            java.lang.String r0 = r14.c
            java.io.File r0 = com.immomo.momo.util.h.a(r0, r10)
        L_0x0049:
            boolean r6 = android.support.v4.b.a.a(r2)
            if (r6 == 0) goto L_0x00d6
            java.lang.String r2 = r14.c
            r13 = r2
            r2 = r4
            r4 = r13
        L_0x0054:
            if (r2 != 0) goto L_0x005a
            int r6 = r1 + 1
            if (r1 < r12) goto L_0x0098
        L_0x005a:
            if (r2 != 0) goto L_0x00c3
            boolean r1 = r9.exists()
            if (r1 == 0) goto L_0x0064
            if (r8 != 0) goto L_0x00b4
        L_0x0064:
            org.json.JSONObject r1 = new org.json.JSONObject
            r1.<init>()
            java.lang.String r2 = "avatarfile"
            java.lang.String r0 = r0.getAbsolutePath()
            r1.put(r2, r0)
            java.lang.String r0 = "time"
            long r4 = java.lang.System.currentTimeMillis()
            r1.put(r0, r4)
            java.lang.String r0 = "count"
            r1.put(r0, r12)
            java.lang.String r0 = "guid"
            java.lang.String r2 = r14.c
            r1.put(r0, r2)
            java.lang.String r0 = r1.toString()
            com.immomo.momo.util.h.a(r9, r0)
        L_0x008e:
            return r3
        L_0x008f:
            r0 = move-exception
            r0 = r1
            r2 = r3
            r4 = r1
        L_0x0093:
            r7 = r0
            r8 = r3
            r0 = r2
            r2 = r3
            goto L_0x0041
        L_0x0098:
            com.immomo.momo.protocol.a.w r1 = com.immomo.momo.protocol.a.w.a()     // Catch:{ Exception -> 0x00ac }
            java.lang.String r10 = r14.d     // Catch:{ Exception -> 0x00ac }
            java.lang.String r1 = r1.a(r0, r4, r10)     // Catch:{ Exception -> 0x00ac }
            java.lang.String r2 = r14.c     // Catch:{ Exception -> 0x00cd }
            r10 = 2
            r11 = 1
            com.immomo.momo.util.h.a(r2, r1, r10, r11)     // Catch:{ Exception -> 0x00cd }
            r1 = r6
            r2 = r5
            goto L_0x0054
        L_0x00ac:
            r1 = move-exception
        L_0x00ad:
            com.immomo.momo.util.m r10 = r14.b
            r10.a(r1)
            r1 = r6
            goto L_0x0054
        L_0x00b4:
            java.lang.String r0 = "count"
            int r1 = r7 + 3
            r8.put(r0, r1)
            java.lang.String r0 = r8.toString()
            com.immomo.momo.util.h.a(r9, r0)
            goto L_0x008e
        L_0x00c3:
            boolean r0 = r9.exists()
            if (r0 == 0) goto L_0x008e
            r9.delete()
            goto L_0x008e
        L_0x00cd:
            r1 = move-exception
            r2 = r5
            goto L_0x00ad
        L_0x00d0:
            r2 = move-exception
            r2 = r3
            r4 = r1
            goto L_0x0093
        L_0x00d4:
            r6 = move-exception
            goto L_0x0093
        L_0x00d6:
            r13 = r2
            r2 = r4
            r4 = r13
            goto L_0x0054
        L_0x00db:
            r4 = r1
            goto L_0x0037
        L_0x00de:
            r2 = r3
            r7 = r1
            r0 = r3
            r8 = r3
            r4 = r1
            goto L_0x0041
        */
        throw new UnsupportedOperationException("Method not decompiled: com.immomo.momo.android.c.ak.a(java.lang.Object[]):java.lang.Object");
    }

    /* access modifiers changed from: protected */
    public final void a() {
        f2376a = true;
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        this.b.a((Throwable) exc);
    }

    /* access modifiers changed from: protected */
    public final void b() {
        f2376a = false;
    }
}
