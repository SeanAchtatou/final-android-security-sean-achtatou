package com.immomo.momo.android.activity.account;

import android.text.style.ClickableSpan;
import android.view.View;

final class co extends ClickableSpan {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ck f994a;

    co(ck ckVar) {
        this.f994a = ckVar;
    }

    public final void onClick(View view) {
        this.f994a.f.setEnabled(false);
        if (this.f994a.b != null) {
            this.f994a.b.cancel();
            this.f994a.b = null;
        }
        this.f994a.h.t = false;
        this.f994a.h.i();
    }
}
