package com.immomo.momo.android.activity;

import android.widget.ListAdapter;

final class lw implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ UserRoamActivity f1822a;

    lw(UserRoamActivity userRoamActivity) {
        this.f1822a = userRoamActivity;
    }

    public final void run() {
        if (this.f1822a.p.isShown() && !this.f1822a.C) {
            this.f1822a.b.setText(this.f1822a.v);
            this.f1822a.f.setText(this.f1822a.v);
            this.f1822a.g.setAnimation(this.f1822a.h());
            this.f1822a.g.setVisibility(0);
            this.f1822a.m.setAdapter((ListAdapter) this.f1822a.q);
            this.f1822a.a();
        }
    }
}
