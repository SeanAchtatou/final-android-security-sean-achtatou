package com.immomo.momo.android.activity.event;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import com.immomo.momo.android.activity.OtherProfileActivity;
import com.immomo.momo.service.bean.bf;

final class af implements AdapterView.OnItemClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ EventMemberListActivity f1350a;

    af(EventMemberListActivity eventMemberListActivity) {
        this.f1350a = eventMemberListActivity;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        if (i < this.f1350a.j.size()) {
            Intent intent = new Intent(this.f1350a.getApplicationContext(), OtherProfileActivity.class);
            intent.putExtra("tag", "local");
            intent.putExtra("momoid", ((bf) this.f1350a.l.getItem(i)).h);
            this.f1350a.startActivity(intent);
        }
    }
}
