package com.immomo.momo.android.view.a;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.android.a.a;
import java.util.List;

final class au extends a {
    /* access modifiers changed from: private */
    public /* synthetic */ at d;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public au(at atVar, Context context, List list) {
        super(context, list);
        this.d = atVar;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = this.c.inflate((int) R.layout.listitem_smartbox, (ViewGroup) null);
        }
        ((TextView) view.findViewById(R.id.menu_tv_text)).setText((CharSequence) getItem(i));
        ImageView imageView = (ImageView) view.findViewById(R.id.menu_iv_status);
        if (this.d.f == i) {
            imageView.setVisibility(0);
        } else {
            imageView.setVisibility(8);
        }
        view.findViewById(R.id.menu_layout_content).setOnClickListener(new av(this, i));
        if (i == getCount() - 1) {
            view.findViewById(R.id.menu_driver).setVisibility(8);
        } else {
            view.findViewById(R.id.menu_driver).setVisibility(0);
        }
        return view;
    }
}
