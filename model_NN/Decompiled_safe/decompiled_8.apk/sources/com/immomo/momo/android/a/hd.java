package com.immomo.momo.android.a;

import android.content.Intent;
import android.view.View;
import com.immomo.momo.android.activity.common.CreateDiscussTabsActivity;
import com.immomo.momo.android.activity.group.foundgroup.FoundGroupActivity;

final class hd implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ hb f855a;
    private final /* synthetic */ int b;

    hd(hb hbVar, int i) {
        this.f855a = hbVar;
        this.b = i;
    }

    public final void onClick(View view) {
        if (this.b == 0) {
            this.f855a.d.startActivity(new Intent(this.f855a.d, FoundGroupActivity.class).addFlags(268435456));
        } else {
            this.f855a.d.startActivity(new Intent(this.f855a.d, CreateDiscussTabsActivity.class).addFlags(268435456));
        }
    }
}
