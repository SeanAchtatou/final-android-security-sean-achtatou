package com.immomo.momo.android.activity.group;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.android.a.dn;
import com.immomo.momo.android.activity.lh;
import com.immomo.momo.android.c.u;
import com.immomo.momo.android.view.HeaderLayout;
import com.immomo.momo.android.view.LoadingButton;
import com.immomo.momo.android.view.RefreshOnOverScrollListView;
import com.immomo.momo.android.view.bl;
import com.immomo.momo.android.view.cd;
import com.immomo.momo.android.view.cg;
import com.immomo.momo.g;
import com.immomo.momo.service.ag;
import com.immomo.momo.service.aq;
import com.immomo.momo.service.bean.a.a;
import com.immomo.momo.service.bean.a.b;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.service.y;
import com.immomo.momo.util.k;
import com.immomo.momo.util.m;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadPoolExecutor;

public class GroupActionListFragment extends lh implements bl {
    public m O = new m("test_momo", "[ --- from GroupActionListActivity ---]");
    Handler P = new Handler();
    private int Q = 0;
    private boolean R = true;
    /* access modifiers changed from: private */
    public ag S = null;
    /* access modifiers changed from: private */
    public RefreshOnOverScrollListView T = null;
    private View U = null;
    private ThreadPoolExecutor V = null;
    /* access modifiers changed from: private */
    public dn W = null;
    /* access modifiers changed from: private */
    public aq X = null;
    /* access modifiers changed from: private */
    public y Y = null;
    /* access modifiers changed from: private */
    public LoadingButton Z = null;
    /* access modifiers changed from: private */
    public cd aa = null;
    private TextView ab = null;
    private List ac = new ArrayList();
    /* access modifiers changed from: private */
    public Map ad = new HashMap();
    private List ae = new ArrayList();

    private void O() {
        if (this.Q > 0) {
            this.ab.setText(String.valueOf(this.Q) + "条新动态");
            this.ab.setVisibility(0);
            return;
        }
        this.ab.setVisibility(8);
    }

    private void P() {
        if (!this.ad.isEmpty() && !this.ac.isEmpty()) {
            this.V.execute(new ar(this, this.ac));
        }
    }

    private void Q() {
        if (!this.ae.isEmpty()) {
            for (a apVar : this.ae) {
                this.V.execute(new ap(this, apVar));
            }
            this.ae.clear();
        }
    }

    private void a(a aVar) {
        this.ae.remove(aVar);
        this.ae.add(aVar);
        if (this.ae.size() >= 5) {
            Q();
        }
    }

    private void a(b bVar, boolean z) {
        if (z) {
            bVar.a(this.X.b(bVar.g()));
        }
        if (android.support.v4.b.a.f(bVar.g()) && bVar.e() == null) {
            bVar.a(new bf(bVar.g()));
            this.ac.remove(bVar.g());
            this.ac.add(bVar.g());
            this.ad.put(bVar.g(), bVar);
            if (this.ac.size() >= 5) {
                P();
            }
        }
    }

    private void b(b bVar, boolean z) {
        if (z && !android.support.v4.b.a.a((CharSequence) bVar.c())) {
            bVar.a(this.Y.e(bVar.c()));
        }
        if (!android.support.v4.b.a.a((CharSequence) bVar.c()) && bVar.d() == null) {
            a aVar = new a(bVar.c());
            bVar.a(aVar);
            a(aVar);
        }
        if (bVar.a() == 1000) {
            ArrayList arrayList = new ArrayList();
            bVar.a(arrayList);
            for (String str : bVar.i()) {
                a e = this.Y.e(str);
                if (e == null) {
                    e = new a(str);
                    a(e);
                }
                arrayList.add(e);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.android.activity.group.GroupActionListFragment.b(com.immomo.momo.service.bean.a.b, boolean):void
     arg types: [com.immomo.momo.service.bean.a.b, int]
     candidates:
      com.immomo.momo.android.activity.lh.b(int, android.view.KeyEvent):boolean
      android.support.v4.app.Fragment.b(android.view.LayoutInflater, android.view.ViewGroup):android.view.View
      com.immomo.momo.android.activity.group.GroupActionListFragment.b(com.immomo.momo.service.bean.a.b, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.android.activity.group.GroupActionListFragment.a(com.immomo.momo.service.bean.a.b, boolean):void
     arg types: [com.immomo.momo.service.bean.a.b, int]
     candidates:
      com.immomo.momo.android.activity.group.GroupActionListFragment.a(android.content.Context, com.immomo.momo.android.view.HeaderLayout):void
      com.immomo.momo.android.activity.group.GroupActionListFragment.a(android.os.Bundle, java.lang.String):boolean
      com.immomo.momo.android.activity.lh.a(int, java.lang.String[]):com.immomo.momo.protocol.imjson.c.a
      com.immomo.momo.android.activity.lh.a(android.content.Context, com.immomo.momo.android.view.HeaderLayout):void
      com.immomo.momo.android.activity.lh.a(int, android.view.KeyEvent):boolean
      com.immomo.momo.android.activity.lh.a(android.os.Bundle, java.lang.String):boolean
      com.immomo.momo.android.activity.ar.a(android.view.LayoutInflater, android.view.ViewGroup):android.view.View
      android.support.v4.app.Fragment.a(android.content.Context, java.lang.String):android.support.v4.app.Fragment
      android.support.v4.app.Fragment.a(android.view.LayoutInflater, android.view.ViewGroup):android.view.View
      android.support.v4.app.Fragment.a(int, android.support.v4.app.Fragment):void
      android.support.v4.app.Fragment.a(android.content.Intent, int):void
      android.support.v4.app.Fragment.a(android.view.Menu, android.view.MenuInflater):boolean
      com.immomo.momo.android.activity.group.GroupActionListFragment.a(com.immomo.momo.service.bean.a.b, boolean):void */
    static /* synthetic */ ArrayList d(GroupActionListFragment groupActionListFragment) {
        ArrayList arrayList = (ArrayList) groupActionListFragment.S.b(groupActionListFragment.W.getCount(), 21);
        if (arrayList.size() > 20) {
            arrayList.remove(arrayList.size() - 1);
        } else {
            groupActionListFragment.T.removeFooterView(groupActionListFragment.U);
        }
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            b bVar = (b) it.next();
            groupActionListFragment.b(bVar, false);
            groupActionListFragment.a(bVar, false);
        }
        groupActionListFragment.P();
        groupActionListFragment.Q();
        groupActionListFragment.W.b((Collection) arrayList);
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public final int B() {
        return R.layout.activity_groupaction_list;
    }

    /* access modifiers changed from: protected */
    public final void C() {
        this.T = (RefreshOnOverScrollListView) c((int) R.id.listview);
        this.U = g.o().inflate((int) R.layout.common_list_loadmore, (ViewGroup) null);
        this.U.setVisibility(8);
        this.Z = (LoadingButton) this.U.findViewById(R.id.btn_loadmore);
        this.Z.setNormalIconResId(R.drawable.ic_btn_inner_clock);
        this.T.addHeaderView(g.o().inflate((int) R.layout.listitem_blank, (ViewGroup) null));
        this.T.setListPaddingBottom(-3);
        this.ab = (TextView) LayoutInflater.from(c()).inflate((int) R.layout.include_righttext_hi, (ViewGroup) null);
    }

    public final boolean G() {
        if (!this.aa.g()) {
            return false;
        }
        this.aa.e();
        return true;
    }

    public final void a(Context context, HeaderLayout headerLayout) {
        headerLayout.setTitleText("群组动态");
        headerLayout.a(this.ab);
    }

    public final void a(View view) {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.android.activity.group.GroupActionListFragment.b(com.immomo.momo.service.bean.a.b, boolean):void
     arg types: [com.immomo.momo.service.bean.a.b, int]
     candidates:
      com.immomo.momo.android.activity.lh.b(int, android.view.KeyEvent):boolean
      android.support.v4.app.Fragment.b(android.view.LayoutInflater, android.view.ViewGroup):android.view.View
      com.immomo.momo.android.activity.group.GroupActionListFragment.b(com.immomo.momo.service.bean.a.b, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.android.activity.group.GroupActionListFragment.a(com.immomo.momo.service.bean.a.b, boolean):void
     arg types: [com.immomo.momo.service.bean.a.b, int]
     candidates:
      com.immomo.momo.android.activity.group.GroupActionListFragment.a(android.content.Context, com.immomo.momo.android.view.HeaderLayout):void
      com.immomo.momo.android.activity.group.GroupActionListFragment.a(android.os.Bundle, java.lang.String):boolean
      com.immomo.momo.android.activity.lh.a(int, java.lang.String[]):com.immomo.momo.protocol.imjson.c.a
      com.immomo.momo.android.activity.lh.a(android.content.Context, com.immomo.momo.android.view.HeaderLayout):void
      com.immomo.momo.android.activity.lh.a(int, android.view.KeyEvent):boolean
      com.immomo.momo.android.activity.lh.a(android.os.Bundle, java.lang.String):boolean
      com.immomo.momo.android.activity.ar.a(android.view.LayoutInflater, android.view.ViewGroup):android.view.View
      android.support.v4.app.Fragment.a(android.content.Context, java.lang.String):android.support.v4.app.Fragment
      android.support.v4.app.Fragment.a(android.view.LayoutInflater, android.view.ViewGroup):android.view.View
      android.support.v4.app.Fragment.a(int, android.support.v4.app.Fragment):void
      android.support.v4.app.Fragment.a(android.content.Intent, int):void
      android.support.v4.app.Fragment.a(android.view.Menu, android.view.MenuInflater):boolean
      com.immomo.momo.android.activity.group.GroupActionListFragment.a(com.immomo.momo.service.bean.a.b, boolean):void */
    /* access modifiers changed from: protected */
    public final boolean a(Bundle bundle, String str) {
        if (!str.equals("actions.groupaction")) {
            return false;
        }
        b i = this.S.i(bundle.getString("msgid"));
        if (i != null) {
            b(i, true);
            a(i, true);
            Q();
            P();
            this.W.b(0, i);
        }
        int i2 = bundle.getInt("gaunreaded", -1);
        if (i2 < 0) {
            this.Q = this.S.g();
        } else {
            this.Q = i2;
        }
        O();
        return !this.R;
    }

    /* access modifiers changed from: protected */
    public final boolean aa() {
        return true;
    }

    /* access modifiers changed from: protected */
    public final void ae() {
        this.R = false;
        if (this.Q > 0) {
            g.d().p();
        }
        new k("PI", "P55").e();
    }

    /* access modifiers changed from: protected */
    public final void ag() {
        super.ag();
        this.R = true;
        this.S.i();
        new k("PO", "P55").e();
        Bundle bundle = new Bundle();
        bundle.putInt("gaunreaded", 0);
        g.d().a(bundle, "actions.groupnoticechanged");
    }

    public final void ai() {
        this.T.i();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.android.activity.group.GroupActionListFragment.b(com.immomo.momo.service.bean.a.b, boolean):void
     arg types: [com.immomo.momo.service.bean.a.b, int]
     candidates:
      com.immomo.momo.android.activity.lh.b(int, android.view.KeyEvent):boolean
      android.support.v4.app.Fragment.b(android.view.LayoutInflater, android.view.ViewGroup):android.view.View
      com.immomo.momo.android.activity.group.GroupActionListFragment.b(com.immomo.momo.service.bean.a.b, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.android.activity.group.GroupActionListFragment.a(com.immomo.momo.service.bean.a.b, boolean):void
     arg types: [com.immomo.momo.service.bean.a.b, int]
     candidates:
      com.immomo.momo.android.activity.group.GroupActionListFragment.a(android.content.Context, com.immomo.momo.android.view.HeaderLayout):void
      com.immomo.momo.android.activity.group.GroupActionListFragment.a(android.os.Bundle, java.lang.String):boolean
      com.immomo.momo.android.activity.lh.a(int, java.lang.String[]):com.immomo.momo.protocol.imjson.c.a
      com.immomo.momo.android.activity.lh.a(android.content.Context, com.immomo.momo.android.view.HeaderLayout):void
      com.immomo.momo.android.activity.lh.a(int, android.view.KeyEvent):boolean
      com.immomo.momo.android.activity.lh.a(android.os.Bundle, java.lang.String):boolean
      com.immomo.momo.android.activity.ar.a(android.view.LayoutInflater, android.view.ViewGroup):android.view.View
      android.support.v4.app.Fragment.a(android.content.Context, java.lang.String):android.support.v4.app.Fragment
      android.support.v4.app.Fragment.a(android.view.LayoutInflater, android.view.ViewGroup):android.view.View
      android.support.v4.app.Fragment.a(int, android.support.v4.app.Fragment):void
      android.support.v4.app.Fragment.a(android.content.Intent, int):void
      android.support.v4.app.Fragment.a(android.view.Menu, android.view.MenuInflater):boolean
      com.immomo.momo.android.activity.group.GroupActionListFragment.a(com.immomo.momo.service.bean.a.b, boolean):void */
    public final void aj() {
        this.Q = this.S.g();
        O();
        if (this.Q > 0) {
            g.d().p();
        }
        ArrayList arrayList = (ArrayList) this.S.b(0, 21);
        if (!arrayList.isEmpty()) {
            if (arrayList.size() > 20) {
                arrayList.remove(arrayList.size() - 1);
                this.U.setVisibility(0);
            } else {
                this.T.removeFooterView(this.U);
            }
            Iterator it = arrayList.iterator();
            while (it.hasNext()) {
                b bVar = (b) it.next();
                b(bVar, false);
                a(bVar, false);
            }
            P();
            Q();
            this.W = new dn(this.T, c(), arrayList);
            this.T.addFooterView(this.U);
            this.T.setAdapter((ListAdapter) this.W);
        }
    }

    public final void g(Bundle bundle) {
        this.M = g.q();
        this.S = new ag();
        this.Y = new y();
        this.X = new aq();
        this.V = u.a();
        this.Z.setOnProcessListener(this);
        this.aa = new cd(c(), this.T);
        this.aa.a();
        this.T.setMultipleSelector(this.aa);
        this.aa.a((int) R.id.item_layout);
        this.aa.a((cg) new am(this));
        this.aa.a(new com.immomo.momo.android.view.a(g.c()).a(), new an(this));
        aj();
        a(800, "actions.groupaction");
    }

    public final void u() {
        a(new at(this, (byte) 0));
    }
}
