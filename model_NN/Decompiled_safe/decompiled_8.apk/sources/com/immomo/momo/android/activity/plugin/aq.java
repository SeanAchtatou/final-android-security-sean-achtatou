package com.immomo.momo.android.activity.plugin;

import android.graphics.Bitmap;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.immomo.momo.R;
import com.immomo.momo.util.ao;

final class aq extends WebViewClient {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ BindTwitterActivity f2044a;

    private aq(BindTwitterActivity bindTwitterActivity) {
        this.f2044a = bindTwitterActivity;
    }

    /* synthetic */ aq(BindTwitterActivity bindTwitterActivity, byte b) {
        this(bindTwitterActivity);
    }

    public final void doUpdateVisitedHistory(WebView webView, String str, boolean z) {
        str.indexOf("login");
        super.doUpdateVisitedHistory(webView, str, z);
    }

    public final void onLoadResource(WebView webView, String str) {
        super.onLoadResource(webView, str);
    }

    public final void onPageFinished(WebView webView, String str) {
        super.onPageFinished(webView, str);
        if (this.f2044a.l != null && this.f2044a.l.isShowing()) {
            this.f2044a.l.dismiss();
        }
    }

    public final void onPageStarted(WebView webView, String str, Bitmap bitmap) {
        super.onPageStarted(webView, str, bitmap);
        this.f2044a.t.b((Object) ("onPageStarted : " + str));
        if (this.f2044a.l != null) {
            this.f2044a.l.a("正在加载，请稍候...");
            this.f2044a.l.show();
        }
    }

    public final void onReceivedError(WebView webView, int i, String str, String str2) {
        if (i == -2) {
            ao.e(R.string.errormsg_network_unfind);
        } else {
            ao.e(R.string.errormsg_server);
        }
        if (this.f2044a.l.isShowing()) {
            this.f2044a.l.dismiss();
        }
    }

    public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
        this.f2044a.t.b((Object) ("shouldOverrideUrlLoading : " + str));
        if (str.indexOf(this.f2044a.i) == 0) {
            BindTwitterActivity bindTwitterActivity = this.f2044a;
            String[] c = BindTwitterActivity.c(str);
            if (c != null && this.f2044a.d(String.valueOf(c[0]) + "&oauth_verifier" + c[1])) {
                BindTwitterActivity.h(this.f2044a);
                return true;
            }
        }
        return super.shouldOverrideUrlLoading(webView, str);
    }
}
