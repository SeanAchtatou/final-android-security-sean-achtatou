package com.immomo.momo.android.activity.event;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.j;
import com.immomo.momo.util.ao;

final class at extends d {

    /* renamed from: a  reason: collision with root package name */
    private v f1363a = null;
    private /* synthetic */ EventProfileActivity c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public at(EventProfileActivity eventProfileActivity, Context context) {
        super(context);
        this.c = eventProfileActivity;
        this.f1363a = new v(context);
        this.f1363a.setCancelable(true);
        this.f1363a.setOnCancelListener(new au(this));
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        String e = j.a().e(this.c.m);
        this.c.n.t = false;
        this.c.o.a(this.c.n);
        return e;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.c.a(this.f1363a);
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        super.a(exc);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        String str = (String) obj;
        super.a((Object) str);
        ao.a((CharSequence) str);
        this.c.y();
        this.c.v();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        super.b();
        this.c.p();
    }
}
