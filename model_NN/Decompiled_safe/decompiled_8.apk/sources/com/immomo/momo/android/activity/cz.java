package com.immomo.momo.android.activity;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Message;
import com.immomo.momo.android.c.g;
import com.immomo.momo.service.bean.au;
import com.immomo.momo.util.j;

final class cz implements g {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ cy f1213a;
    private final /* synthetic */ au b;

    cz(cy cyVar, au auVar) {
        this.f1213a = cyVar;
        this.b = auVar;
    }

    public final /* synthetic */ void a(Object obj) {
        Bitmap bitmap = (Bitmap) obj;
        if (j.a(this.b.b) == null && bitmap != null) {
            j.a(this.b.b, bitmap);
        }
        Message message = new Message();
        Bundle bundle = new Bundle();
        bundle.putString("guid", this.b.b);
        message.setData(bundle);
        message.obj = bitmap;
        message.what = 11;
        this.f1213a.f1212a.P.sendMessage(message);
    }
}
