package com.immomo.momo.android.a;

import android.view.View;

final class dt implements View.OnLongClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ dn f783a;
    private final /* synthetic */ int b;

    dt(dn dnVar, int i) {
        this.f783a = dnVar;
        this.b = i;
    }

    public final boolean onLongClick(View view) {
        return this.f783a.e.getOnItemLongClickListenerInWrapper().onItemLongClick(this.f783a.e, view, this.b, (long) view.getId());
    }
}
