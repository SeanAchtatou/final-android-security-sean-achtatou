package com.immomo.momo.android.activity.maintab;

import android.location.Location;
import com.immomo.momo.R;

final class aa implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ z f1828a;
    private final /* synthetic */ Location b;

    aa(z zVar, Location location) {
        this.f1828a = zVar;
        this.b = location;
    }

    public final void run() {
        this.f1828a.f1904a.ac.setLoadingViewText(R.string.pull_to_refresh_refreshing_label);
        this.f1828a.f1904a.X = new an(this.f1828a.f1904a, this.f1828a.f1904a.c(), this.b.getLatitude(), this.b.getLongitude());
        this.f1828a.f1904a.X.execute(new Object[0]);
    }
}
