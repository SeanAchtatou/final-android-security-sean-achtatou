package com.immomo.momo.android.activity.emotestore;

import android.content.Intent;
import com.immomo.momo.a;
import com.immomo.momo.android.broadcast.o;
import com.immomo.momo.service.bean.q;
import java.io.File;
import java.util.List;

final class bd implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ av f1306a;

    bd(av avVar) {
        this.f1306a = avVar;
    }

    public final void run() {
        int i = 0;
        boolean z = false;
        while (true) {
            int i2 = i + 1;
            if (i >= 2) {
                break;
            }
            List<q> list = i2 == 0 ? this.f1306a.U : this.f1306a.T;
            boolean z2 = z;
            boolean z3 = false;
            for (q qVar : list) {
                if (qVar.t) {
                    File file = new File(a.v(), String.valueOf(qVar.f3035a) + "/middle");
                    if (!file.exists() || !file.isDirectory() || file.list().length == 0) {
                        qVar.t = false;
                        if (file.exists()) {
                            file.delete();
                        }
                        z3 = true;
                        z2 = true;
                    }
                }
            }
            if (z3) {
                if (i2 == 0) {
                    this.f1306a.P.h(list);
                    i = i2;
                    z = z2;
                } else {
                    this.f1306a.P.i(list);
                }
            }
            i = i2;
            z = z2;
        }
        if (z) {
            this.f1306a.Q.notifyDataSetInvalidated();
            Intent intent = new Intent(o.f2358a);
            intent.putExtra("event", "check");
            av avVar = this.f1306a;
            av.b(intent);
        }
    }
}
