package com.immomo.momo.android.activity.message;

import android.view.View;
import android.widget.AdapterView;
import com.immomo.momo.service.bean.a.i;

final class bs implements AdapterView.OnItemClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ GroupChatActivity f1952a;

    bs(GroupChatActivity groupChatActivity) {
        this.f1952a = groupChatActivity;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        switch (i) {
            case 0:
                if (this.f1952a.ae != null) {
                    i o = this.f1952a.ae;
                    i o2 = this.f1952a.ae;
                    boolean z = !this.f1952a.ae.f2978a;
                    o2.f2978a = z;
                    o.a("group_ispush", Boolean.valueOf(z));
                    return;
                }
                return;
            case 1:
                this.f1952a.ae();
                return;
            case 2:
                ChatBGSettingActivity.a(this.f1952a, this.f1952a.Q.O);
                return;
            default:
                return;
        }
    }
}
