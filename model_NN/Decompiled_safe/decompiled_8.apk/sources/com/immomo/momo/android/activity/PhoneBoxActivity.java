package com.immomo.momo.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.b.a;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.contacts.ContactPeopleActivity;
import com.immomo.momo.android.view.HeaderLayout;
import com.immomo.momo.android.view.a.n;
import com.immomo.momo.util.k;

public class PhoneBoxActivity extends ah implements View.OnClickListener {
    private HeaderLayout h = null;
    private TextView i = null;
    private Button j;
    private Button k;
    private Button l;
    /* access modifiers changed from: private */
    public jl m;

    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        super.a(bundle);
        setContentView((int) R.layout.activity_phonebox);
        this.i = (TextView) findViewById(R.id.tv_register_phone);
        this.h = (HeaderLayout) findViewById(R.id.layout_header);
        this.h.setTitleText("绑定手机");
        this.j = (Button) findViewById(R.id.btn_phone_addfriend);
        this.k = (Button) findViewById(R.id.btn_phone_stopsyn);
        this.l = (Button) findViewById(R.id.btn_phone_open);
        if (o().c) {
            this.j.setVisibility(0);
            this.k.setVisibility(0);
            this.l.setVisibility(8);
        } else {
            this.j.setVisibility(8);
            this.k.setVisibility(8);
            this.l.setVisibility(0);
        }
        d();
        this.j.setOnClickListener(this);
        this.k.setOnClickListener(this);
        this.l.setOnClickListener(this);
    }

    /* access modifiers changed from: protected */
    public final void d() {
        this.i.setText(a.c(this.f.b));
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_phone_addfriend /*2131165785*/:
                startActivity(new Intent(this, ContactPeopleActivity.class));
                return;
            case R.id.btn_phone_stopsyn /*2131165786*/:
                a(n.a(this, "确定停止通讯录好友推荐", new jk(this)));
                return;
            case R.id.btn_phone_open /*2131165787*/:
                b(new jl(this, this));
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        new k("PO", "P95").e();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        new k("PI", "P95").e();
    }
}
