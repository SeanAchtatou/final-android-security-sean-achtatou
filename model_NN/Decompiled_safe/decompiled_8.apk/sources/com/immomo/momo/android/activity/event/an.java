package com.immomo.momo.android.activity.event;

import android.content.DialogInterface;
import com.immomo.momo.android.view.a.ao;
import com.immomo.momo.util.k;
import org.json.JSONObject;

final class an implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ EventProfileActivity f1357a;
    private final /* synthetic */ ao b;

    an(EventProfileActivity eventProfileActivity, ao aoVar) {
        this.f1357a = eventProfileActivity;
        this.b = aoVar;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        int i2 = 1;
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("sina_weibo", this.b.e() ? 1 : 0);
            jSONObject.put("renren", this.b.f() ? 1 : 0);
            if (!this.b.g()) {
                i2 = 0;
            }
            jSONObject.put("tencent_weibo", i2);
            new k("C", "C71106", jSONObject).e();
        } catch (Exception e) {
        }
        if (this.b.e() || this.b.f() || this.b.g() || this.b.h()) {
            new av(this.f1357a, this.f1357a, this.b.e(), this.b.f(), this.b.g(), this.b.h()).execute(new String[0]);
        }
        dialogInterface.dismiss();
    }
}
