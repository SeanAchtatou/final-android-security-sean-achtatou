package com.immomo.momo.android.activity.group;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.ah;
import com.immomo.momo.android.broadcast.d;
import com.immomo.momo.android.broadcast.u;
import com.immomo.momo.android.c.r;
import com.immomo.momo.android.view.EmoteEditeText;
import com.immomo.momo.android.view.HeaderLayout;
import com.immomo.momo.android.view.UserPhotosView;
import com.immomo.momo.android.view.a.n;
import com.immomo.momo.android.view.a.o;
import com.immomo.momo.android.view.bi;
import com.immomo.momo.h;
import com.immomo.momo.service.aq;
import com.immomo.momo.service.bean.a.a;
import com.immomo.momo.service.bean.a.e;
import com.immomo.momo.service.bean.a.g;
import com.immomo.momo.service.bean.a.i;
import com.immomo.momo.service.bean.au;
import com.immomo.momo.service.v;
import com.immomo.momo.service.y;
import com.immomo.momo.util.j;
import com.immomo.momo.util.k;
import java.io.File;
import java.util.ArrayList;
import org.json.JSONException;
import org.json.JSONObject;

public class GroupProfileActivity extends ah {
    private TextView A = null;
    private TextView B = null;
    private TextView C = null;
    private TextView D = null;
    private ImageView E = null;
    private ImageView F = null;
    private TextView G = null;
    private View H = null;
    private ImageView I = null;
    /* access modifiers changed from: private */
    public boolean J = false;
    /* access modifiers changed from: private */
    public boolean K = false;
    /* access modifiers changed from: private */
    public boolean L = false;
    private View M;
    private View N;
    private TextView O;
    private TextView P;
    private ImageView Q;
    private View R;
    private View S;
    private View T;
    private View[] U;
    private ScrollView V = null;
    private UserPhotosView W;
    private ArrayList X = new ArrayList();
    /* access modifiers changed from: private */
    public i Y = null;
    private View.OnClickListener Z;
    private View.OnClickListener aa;
    /* access modifiers changed from: private */
    public Handler ab;
    private d ac;
    private dq h;
    /* access modifiers changed from: private */
    public ds i;
    private u j;
    private String k;
    /* access modifiers changed from: private */
    public String l;
    private HeaderLayout m = null;
    private bi n = null;
    /* access modifiers changed from: private */
    public y o = null;
    private v p = null;
    private aq q = null;
    /* access modifiers changed from: private */
    public a r = null;
    private View s;
    private View t;
    private LinearLayout u;
    private LinearLayout v;
    private LinearLayout w;
    private TextView x = null;
    private TextView y = null;
    private TextView z = null;

    public GroupProfileActivity() {
        new g();
        this.Z = new ct(this);
        this.aa = new dc(this);
        new de(this);
        this.ab = new df(this);
        this.ac = new dg(this);
    }

    private void A() {
        if (this.X != null && this.X.size() > 0) {
            int size = this.X.size();
            this.e.a((Object) ("length: " + size));
            for (int i2 = 0; i2 < size; i2++) {
                this.U[i2].setVisibility(0);
                ((ImageView) this.U[i2].findViewById(R.id.avatar_cover)).setTag(new dl(this.X.get(i2), i2));
            }
            if (size < 8) {
                for (int i3 = size; i3 < 8; i3++) {
                    this.U[i3].setVisibility(4);
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void B() {
        this.e.b((Object) ("@@@@@@@@@@@@@ group :" + this.r));
        this.N.setFocusable(false);
        this.N.setClickable(false);
        if (this.r != null) {
            if (this.r.G == 4 || this.r.G == 3 || this.r.G == 1) {
                this.N.setVisibility(8);
            } else if (this.K) {
                this.N.setFocusable(true);
                this.N.setOnClickListener(this.aa);
                e b = this.r.N != null ? this.r.N.isEmpty() ? null : (e) this.r.N.get(0) : this.p.b(this.l);
                if (b == null) {
                    this.O.setText("来发表第一条留言吧");
                    this.P.setVisibility(8);
                    this.Q.setVisibility(8);
                    return;
                }
                this.S.setVisibility(0);
                this.P.setVisibility(0);
                this.O.setText(b.a());
                this.P.setText(b.d);
                if (android.support.v4.b.a.f(b.getLoadImageId())) {
                    this.Q.setVisibility(0);
                    j.a(b, this.Q, (ViewGroup) null, 15);
                } else {
                    this.Q.setVisibility(8);
                }
                if (b.m == 1) {
                    this.R.setVisibility(0);
                } else {
                    this.R.setVisibility(8);
                }
            } else if (this.r.C) {
                this.O.setText("群活动正在进行中...");
                this.P.setVisibility(8);
                this.R.setVisibility(0);
                this.N.setVisibility(0);
                this.S.setVisibility(8);
            } else {
                this.O.setText("加入群后可以查看群空间");
                this.P.setVisibility(8);
                this.R.setVisibility(8);
                this.S.setVisibility(8);
                this.N.setVisibility(0);
            }
        }
    }

    static /* synthetic */ void a(GroupProfileActivity groupProfileActivity, EmoteEditeText emoteEditeText) {
        InputMethodManager inputMethodManager = (InputMethodManager) groupProfileActivity.getSystemService("input_method");
        if (inputMethodManager != null && emoteEditeText != null) {
            inputMethodManager.hideSoftInputFromWindow(emoteEditeText.getWindowToken(), 0);
        }
    }

    static /* synthetic */ void a(GroupProfileActivity groupProfileActivity, String str, Bitmap bitmap) {
        if (!android.support.v4.b.a.a((CharSequence) str) && bitmap != null && groupProfileActivity.U != null) {
            int length = groupProfileActivity.U.length;
            int i2 = 0;
            while (i2 < length) {
                dl dlVar = (dl) groupProfileActivity.U[i2].findViewById(R.id.avatar_cover).getTag();
                if (dlVar == null || android.support.v4.b.a.a((CharSequence) ((au) dlVar.f1608a).b) || !((au) dlVar.f1608a).b.equals(str)) {
                    i2++;
                } else {
                    ((ImageView) groupProfileActivity.U[i2].findViewById(R.id.avatar_imageview)).setImageBitmap(bitmap);
                    return;
                }
            }
        }
    }

    static /* synthetic */ void a(GroupProfileActivity groupProfileActivity, String str, String str2, String str3) {
        if (!groupProfileActivity.isFinishing()) {
            r rVar = new r(new StringBuilder().append(System.currentTimeMillis()).toString(), new cz(groupProfileActivity, str2, str3), 17, null);
            rVar.a(str);
            new Thread(rVar).start();
        }
    }

    private void c(Bundle bundle) {
        if (bundle == null || !bundle.getBoolean("from_saveinstance", false)) {
            Intent intent = getIntent();
            this.k = intent.getStringExtra("tag");
            this.l = intent.getStringExtra("gid");
        } else {
            this.l = (String) bundle.get("gid");
            this.k = (String) bundle.get("tag");
            this.k = this.k == null ? "local" : this.k;
        }
        this.Y = this.g.c(this.l);
        this.K = this.o.c(this.f.h, this.l);
        this.r = this.o.e(this.l);
        if (this.r != null) {
            this.J = false;
            this.L = this.f.h.equals(this.r.g);
            u();
            x();
            y();
        } else {
            this.J = true;
            this.r = new a(this.l);
            this.m.setTitleText(this.r.b);
        }
        if (((Boolean) this.g.b(String.valueOf(this.r.b) + "_alertshared", false)).booleanValue() && this.L) {
            this.i = new ds(this, this);
            this.i.execute(new String[0]);
        }
        B();
        v();
    }

    static /* synthetic */ void d(GroupProfileActivity groupProfileActivity) {
        String[] strArr = null;
        if (groupProfileActivity.o.d(groupProfileActivity.r.b, groupProfileActivity.f.h) == 1) {
            String[] strArr2 = new String[6];
            strArr2[0] = groupProfileActivity.getString(R.string.gprofile_owner_op0);
            strArr2[1] = groupProfileActivity.o.a(groupProfileActivity.l) ? groupProfileActivity.getString(R.string.f312gprofile_owner_op11) : groupProfileActivity.getString(R.string.f313gprofile_owner_op12);
            strArr2[2] = (groupProfileActivity.Y == null || !groupProfileActivity.Y.f2978a) ? groupProfileActivity.getString(R.string.f316gprofile_member_op01) : groupProfileActivity.getString(R.string.f317gprofile_member_op02);
            strArr2[3] = groupProfileActivity.getString(R.string.gprofile_owner_op3);
            strArr2[4] = groupProfileActivity.getString(R.string.gprofile_owner_op4);
            strArr2[5] = groupProfileActivity.getString(R.string.gprofile_member_op3);
            strArr = strArr2;
        } else if (groupProfileActivity.o.d(groupProfileActivity.r.b, groupProfileActivity.f.h) == 2) {
            String[] strArr3 = new String[4];
            strArr3[0] = (groupProfileActivity.Y == null || !groupProfileActivity.Y.f2978a) ? groupProfileActivity.getString(R.string.f316gprofile_member_op01) : groupProfileActivity.getString(R.string.f317gprofile_member_op02);
            strArr3[1] = groupProfileActivity.getString(R.string.gprofile_owner_op3);
            strArr3[2] = groupProfileActivity.getString(R.string.gprofile_member_op2);
            strArr3[3] = groupProfileActivity.getString(R.string.gprofile_member_op3);
            strArr = strArr3;
        }
        if (strArr != null) {
            o oVar = new o(groupProfileActivity, strArr);
            oVar.setTitle((int) R.string.dialog_title_avatar_long_press);
            oVar.a();
            oVar.a(new dj(groupProfileActivity, strArr));
            oVar.show();
        }
    }

    static /* synthetic */ void f(GroupProfileActivity groupProfileActivity) {
        String[] strArr = new String[4];
        strArr[0] = (groupProfileActivity.Y == null || !groupProfileActivity.Y.f2978a) ? groupProfileActivity.getString(R.string.f316gprofile_member_op01) : groupProfileActivity.getString(R.string.f317gprofile_member_op02);
        strArr[1] = groupProfileActivity.getString(R.string.gprofile_member_op1);
        strArr[2] = groupProfileActivity.getString(R.string.gprofile_member_op2);
        strArr[3] = groupProfileActivity.getString(R.string.gprofile_member_op3);
        o oVar = new o(groupProfileActivity, strArr);
        oVar.setTitle((int) R.string.dialog_title_avatar_long_press);
        oVar.a();
        oVar.a(new cv(groupProfileActivity));
        oVar.show();
    }

    static /* synthetic */ void h(GroupProfileActivity groupProfileActivity) {
        Intent intent = new Intent(groupProfileActivity.getApplicationContext(), GroupMemberListActivity.class);
        intent.putExtra("gid", groupProfileActivity.r == null ? groupProfileActivity.l : groupProfileActivity.r.b);
        intent.putExtra("count", groupProfileActivity.r == null ? 0 : groupProfileActivity.r.k);
        groupProfileActivity.startActivity(intent);
    }

    static /* synthetic */ void k(GroupProfileActivity groupProfileActivity) {
        if (groupProfileActivity.r != null) {
            groupProfileActivity.x();
            groupProfileActivity.v();
            groupProfileActivity.y();
            groupProfileActivity.w();
        }
    }

    static /* synthetic */ void o(GroupProfileActivity groupProfileActivity) {
        if (groupProfileActivity.r.s) {
            groupProfileActivity.r.s = false;
            groupProfileActivity.o.j(groupProfileActivity.r.b);
        }
        if (groupProfileActivity.r.t) {
            groupProfileActivity.r.t = false;
            groupProfileActivity.o.i(groupProfileActivity.r.b);
        }
    }

    static /* synthetic */ void s(GroupProfileActivity groupProfileActivity) {
        View inflate = com.immomo.momo.g.o().inflate((int) R.layout.dialog_groupprofile_dismiss, (ViewGroup) null);
        EmoteEditeText emoteEditeText = (EmoteEditeText) inflate.findViewById(R.id.edittext_pwd);
        emoteEditeText.requestFocus();
        InputMethodManager inputMethodManager = (InputMethodManager) groupProfileActivity.getSystemService("input_method");
        if (!(inputMethodManager == null || emoteEditeText == null)) {
            inputMethodManager.showSoftInput(emoteEditeText, 2);
            inputMethodManager.toggleSoftInput(2, 1);
        }
        n nVar = new n(groupProfileActivity);
        nVar.setTitle("解散群组");
        nVar.setContentView(inflate);
        nVar.a();
        nVar.a(0, groupProfileActivity.getString(R.string.dialog_btn_confim), new dk(groupProfileActivity, emoteEditeText));
        nVar.a(1, groupProfileActivity.getString(R.string.dialog_btn_cancel), new cu(groupProfileActivity, emoteEditeText));
        nVar.show();
    }

    /* access modifiers changed from: private */
    public void u() {
        int i2 = 8;
        if (!(this.r == null || this.r.I == null || this.r.I.length <= 0)) {
            aq aqVar = new aq();
            if (this.r.I.length <= 8) {
                i2 = this.r.I.length;
            }
            this.X.clear();
            for (int i3 = 0; i3 < i2; i3++) {
                au auVar = new au();
                String str = this.r.I[i3];
                String a2 = aqVar.a(str);
                this.e.a((Object) ("---init momoid=" + str + ", avatar=" + a2));
                auVar.b = a2;
                this.X.add(auVar);
            }
        }
        A();
    }

    static /* synthetic */ void u(GroupProfileActivity groupProfileActivity) {
        o oVar = new o(groupProfileActivity, (int) R.array.reportgroup_items);
        oVar.setTitle((int) R.string.report_dialog_title_group);
        oVar.a();
        oVar.a(new cw(groupProfileActivity));
        oVar.show();
    }

    private void v() {
        this.W.a(this.r.F, false, false);
    }

    private void w() {
        boolean z2;
        if (this.X != null && this.X.size() > 0) {
            int size = this.X.size();
            for (int i2 = 0; i2 < size; i2++) {
                au auVar = (au) this.X.get(i2);
                String str = auVar.b;
                if (android.support.v4.b.a.a((CharSequence) str)) {
                    Message message = new Message();
                    Bundle bundle = new Bundle();
                    bundle.putString("guid", auVar.b);
                    message.setData(bundle);
                    message.obj = null;
                    message.what = 12;
                    this.ab.sendMessage(message);
                } else {
                    try {
                        z2 = new File(new File(com.immomo.momo.a.d(), str.substring(0, 1)), String.valueOf(str) + ".jpg_").exists();
                        if (!z2) {
                            try {
                                z2 = new File(com.immomo.momo.a.d(), String.valueOf(str) + ".jpg_").exists();
                            } catch (Exception e) {
                            }
                        }
                    } catch (Exception e2) {
                        z2 = false;
                    }
                    dh dhVar = new dh(this, auVar);
                    if (z2) {
                        com.immomo.momo.android.c.u.f().execute(new r(str, dhVar, 3, null));
                    } else {
                        com.immomo.momo.android.c.u.g().execute(new com.immomo.momo.android.c.o(str, dhVar, 3, null));
                    }
                }
            }
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:87:0x027a, code lost:
        if (r9.r.G == 5) goto L_0x027c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void x() {
        /*
            r9 = this;
            r8 = 3
            r7 = 2
            r6 = 1
            r5 = 8
            r4 = 0
            com.immomo.momo.service.bean.a.a r0 = r9.r
            if (r0 != 0) goto L_0x000b
        L_0x000a:
            return
        L_0x000b:
            com.immomo.momo.service.bean.a.a r0 = r9.r
            java.lang.String r0 = r0.c
            boolean r0 = android.support.v4.b.a.a(r0)
            if (r0 != 0) goto L_0x0179
            com.immomo.momo.android.view.HeaderLayout r0 = r9.m
            com.immomo.momo.service.bean.a.a r1 = r9.r
            java.lang.String r1 = r1.c
            r0.setTitleText(r1)
        L_0x001e:
            com.immomo.momo.service.bean.a.a r0 = r9.r
            java.lang.String r0 = r0.K
            boolean r0 = android.support.v4.b.a.a(r0)
            if (r0 != 0) goto L_0x0031
            com.immomo.momo.android.view.HeaderLayout r0 = r9.m
            com.immomo.momo.service.bean.a.a r1 = r9.r
            java.lang.String r1 = r1.K
            r0.setSubTitleText(r1)
        L_0x0031:
            com.immomo.momo.service.bean.a.a r0 = r9.r
            int r0 = r0.G
            r1 = 4
            if (r0 != r1) goto L_0x0184
            android.widget.TextView r0 = r9.G
            r0.setVisibility(r4)
            android.widget.TextView r0 = r9.G
            r1 = 2131493246(0x7f0c017e, float:1.8609967E38)
            r0.setText(r1)
            android.view.View r0 = r9.M
            r0.setVisibility(r4)
        L_0x004a:
            android.widget.TextView r1 = r9.x
            com.immomo.momo.service.bean.a.a r0 = r9.r
            java.lang.String r0 = r0.b
            boolean r0 = android.support.v4.b.a.a(r0)
            if (r0 == 0) goto L_0x0204
            java.lang.String r0 = ""
        L_0x0058:
            r1.setText(r0)
            com.immomo.momo.service.bean.a.a r0 = r9.r
            com.immomo.momo.service.bean.a.a r0 = r9.r
            int r0 = r0.A
            com.immomo.momo.service.bean.a.a r1 = r9.r
            boolean r1 = r1.a()
            int r0 = com.immomo.momo.service.bean.a.a.a(r0, r1)
            r1 = -1
            if (r0 == r1) goto L_0x020a
            android.widget.ImageView r1 = r9.I
            r1.setVisibility(r4)
            android.widget.ImageView r1 = r9.I
            r1.setImageResource(r0)
        L_0x0078:
            android.widget.TextView r1 = r9.y
            com.immomo.momo.service.bean.a.a r0 = r9.r
            java.lang.String r0 = r0.p
            boolean r0 = android.support.v4.b.a.a(r0)
            if (r0 == 0) goto L_0x0211
            java.lang.String r0 = ""
        L_0x0086:
            r1.setText(r0)
            com.immomo.momo.service.bean.a.a r0 = r9.r
            java.util.Date r0 = r0.d
            if (r0 == 0) goto L_0x0217
            android.widget.TextView r0 = r9.z
            com.immomo.momo.service.bean.a.a r1 = r9.r
            java.util.Date r1 = r1.d
            java.lang.String r1 = android.support.v4.b.a.d(r1)
            r0.setText(r1)
        L_0x009c:
            android.widget.TextView r1 = r9.A
            com.immomo.momo.service.bean.a.a r0 = r9.r
            java.lang.String r0 = r0.h
            boolean r0 = android.support.v4.b.a.a(r0)
            if (r0 == 0) goto L_0x0220
            java.lang.String r0 = ""
        L_0x00aa:
            r1.setText(r0)
            r0 = 2131493234(0x7f0c0172, float:1.8609942E38)
            java.lang.String r0 = com.immomo.momo.g.a(r0)
            android.widget.TextView r1 = r9.B
            java.lang.Object[] r2 = new java.lang.Object[r7]
            com.immomo.momo.service.bean.a.a r3 = r9.r
            int r3 = r3.k
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)
            r2[r4] = r3
            com.immomo.momo.service.bean.a.a r3 = r9.r
            int r3 = r3.j
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)
            r2[r6] = r3
            java.lang.String r0 = java.lang.String.format(r0, r2)
            r1.setText(r0)
            com.immomo.momo.service.aq r0 = r9.q
            com.immomo.momo.service.bean.a.a r1 = r9.r
            java.lang.String r1 = r1.g
            com.immomo.momo.service.bean.bf r0 = r0.b(r1)
            if (r0 == 0) goto L_0x0226
            android.widget.TextView r1 = r9.C
            java.lang.String r2 = r0.h()
            r1.setText(r2)
            java.lang.String[] r1 = r0.ae
            if (r1 == 0) goto L_0x00fb
            com.immomo.momo.service.bean.al r1 = new com.immomo.momo.service.bean.al
            java.lang.String[] r0 = r0.ae
            r0 = r0[r4]
            r1.<init>(r0)
            android.widget.ImageView r0 = r9.F
            r2 = 0
            com.immomo.momo.util.j.a(r1, r0, r2, r8)
        L_0x00fb:
            com.immomo.momo.service.bean.a.a r0 = r9.r
            java.lang.String r0 = r0.K
            if (r0 == 0) goto L_0x0238
            android.widget.TextView r0 = r9.D
            com.immomo.momo.service.bean.a.a r1 = r9.r
            java.lang.String r1 = r1.K
            r0.setText(r1)
        L_0x010a:
            com.immomo.momo.service.bean.a.a r0 = r9.r
            java.lang.String r0 = r0.J
            boolean r0 = android.support.v4.b.a.a(r0)
            if (r0 != 0) goto L_0x0241
            android.view.View r0 = r9.t
            r0.setClickable(r6)
            android.view.View r0 = r9.t
            r0.setFocusable(r6)
            android.widget.ImageView r0 = r9.E
            r0.setVisibility(r4)
        L_0x0123:
            com.immomo.momo.service.bean.a.a r0 = r9.r
            if (r0 == 0) goto L_0x015c
            com.immomo.momo.service.bean.bf r0 = r9.f
            java.lang.String r0 = r0.h
            com.immomo.momo.service.bean.a.a r1 = r9.r
            java.lang.String r1 = r1.g
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x0283
            com.immomo.momo.util.m r0 = r9.e
            java.lang.String r1 = "relation with this group: owner"
            r0.a(r1)
            com.immomo.momo.service.bean.a.a r0 = r9.r
            int r0 = r0.G
            if (r0 == r8) goto L_0x0148
            com.immomo.momo.service.bean.a.a r0 = r9.r
            int r0 = r0.G
            if (r0 != r6) goto L_0x0252
        L_0x0148:
            android.view.View r0 = r9.s
            r0.setVisibility(r4)
            android.widget.LinearLayout r0 = r9.w
            r0.setVisibility(r4)
            android.widget.LinearLayout r0 = r9.u
            r0.setVisibility(r5)
            android.widget.LinearLayout r0 = r9.v
            r0.setVisibility(r5)
        L_0x015c:
            com.immomo.momo.service.bean.a.a r0 = r9.r
            if (r0 == 0) goto L_0x000a
            com.immomo.momo.service.bean.bf r0 = r9.f
            java.lang.String r0 = r0.h
            com.immomo.momo.service.bean.a.a r1 = r9.r
            java.lang.String r1 = r1.g
            boolean r0 = r0.equals(r1)
            if (r0 != 0) goto L_0x0172
            boolean r0 = r9.K
            if (r0 == 0) goto L_0x02db
        L_0x0172:
            com.immomo.momo.android.view.bi r0 = r9.n
            r0.setVisibility(r4)
            goto L_0x000a
        L_0x0179:
            com.immomo.momo.android.view.HeaderLayout r0 = r9.m
            com.immomo.momo.service.bean.a.a r1 = r9.r
            java.lang.String r1 = r1.b
            r0.setTitleText(r1)
            goto L_0x001e
        L_0x0184:
            com.immomo.momo.service.bean.a.a r0 = r9.r
            int r0 = r0.G
            if (r0 != r8) goto L_0x01a2
            boolean r0 = r9.L
            if (r0 == 0) goto L_0x01a2
            android.widget.TextView r0 = r9.G
            r0.setVisibility(r4)
            android.widget.TextView r0 = r9.G
            r1 = 2131493250(0x7f0c0182, float:1.8609975E38)
            r0.setText(r1)
            android.view.View r0 = r9.M
            r0.setVisibility(r5)
            goto L_0x004a
        L_0x01a2:
            com.immomo.momo.service.bean.a.a r0 = r9.r
            int r0 = r0.G
            if (r0 != r6) goto L_0x01c0
            boolean r0 = r9.L
            if (r0 == 0) goto L_0x01c0
            android.widget.TextView r0 = r9.G
            r0.setVisibility(r4)
            android.widget.TextView r0 = r9.G
            r1 = 2131493249(0x7f0c0181, float:1.8609973E38)
            r0.setText(r1)
            android.view.View r0 = r9.M
            r0.setVisibility(r5)
            goto L_0x004a
        L_0x01c0:
            com.immomo.momo.service.bean.a.a r0 = r9.r
            int r0 = r0.q
            if (r0 != r6) goto L_0x01de
            boolean r0 = r9.L
            if (r0 == 0) goto L_0x01de
            android.widget.TextView r0 = r9.G
            r0.setVisibility(r4)
            android.widget.TextView r0 = r9.G
            r1 = 2131493247(0x7f0c017f, float:1.8609969E38)
            r0.setText(r1)
            android.view.View r0 = r9.M
            r0.setVisibility(r4)
            goto L_0x004a
        L_0x01de:
            com.immomo.momo.service.bean.a.a r0 = r9.r
            int r0 = r0.f2970a
            if (r0 != r6) goto L_0x01f8
            android.widget.TextView r0 = r9.G
            r0.setVisibility(r4)
            android.widget.TextView r0 = r9.G
            r1 = 2131493248(0x7f0c0180, float:1.860997E38)
            r0.setText(r1)
            android.view.View r0 = r9.M
            r0.setVisibility(r4)
            goto L_0x004a
        L_0x01f8:
            android.widget.TextView r0 = r9.G
            r0.setVisibility(r5)
            android.view.View r0 = r9.M
            r0.setVisibility(r4)
            goto L_0x004a
        L_0x0204:
            com.immomo.momo.service.bean.a.a r0 = r9.r
            java.lang.String r0 = r0.b
            goto L_0x0058
        L_0x020a:
            android.widget.ImageView r0 = r9.I
            r0.setVisibility(r5)
            goto L_0x0078
        L_0x0211:
            com.immomo.momo.service.bean.a.a r0 = r9.r
            java.lang.String r0 = r0.p
            goto L_0x0086
        L_0x0217:
            android.widget.TextView r0 = r9.z
            java.lang.String r1 = ""
            r0.setText(r1)
            goto L_0x009c
        L_0x0220:
            com.immomo.momo.service.bean.a.a r0 = r9.r
            java.lang.String r0 = r0.h
            goto L_0x00aa
        L_0x0226:
            android.widget.TextView r0 = r9.C
            com.immomo.momo.service.bean.a.a r1 = r9.r
            java.lang.String r1 = r1.g
            r0.setText(r1)
            r0 = 0
            android.widget.ImageView r1 = r9.F
            r2 = 0
            com.immomo.momo.util.j.a(r0, r1, r2, r8)
            goto L_0x00fb
        L_0x0238:
            android.widget.TextView r0 = r9.D
            java.lang.String r1 = ""
            r0.setText(r1)
            goto L_0x010a
        L_0x0241:
            android.view.View r0 = r9.t
            r0.setClickable(r4)
            android.view.View r0 = r9.t
            r0.setFocusable(r4)
            android.widget.ImageView r0 = r9.E
            r0.setVisibility(r5)
            goto L_0x0123
        L_0x0252:
            com.immomo.momo.service.bean.a.a r0 = r9.r
            int r0 = r0.G
            if (r0 == r7) goto L_0x025f
            com.immomo.momo.service.bean.a.a r0 = r9.r
            int r0 = r0.G
            r1 = 4
            if (r0 != r1) goto L_0x0275
        L_0x025f:
            android.view.View r0 = r9.s
            r0.setVisibility(r4)
            android.widget.LinearLayout r0 = r9.w
            r0.setVisibility(r5)
            android.widget.LinearLayout r0 = r9.u
            r0.setVisibility(r4)
            android.widget.LinearLayout r0 = r9.v
            r0.setVisibility(r5)
            goto L_0x015c
        L_0x0275:
            com.immomo.momo.service.bean.a.a r0 = r9.r
            int r0 = r0.G
            r1 = 5
            if (r0 != r1) goto L_0x015c
        L_0x027c:
            android.view.View r0 = r9.s
            r0.setVisibility(r5)
            goto L_0x015c
        L_0x0283:
            boolean r0 = r9.K
            if (r0 == 0) goto L_0x02b8
            com.immomo.momo.util.m r0 = r9.e
            java.lang.String r1 = "relation with this group: member"
            r0.a(r1)
            com.immomo.momo.service.bean.a.a r0 = r9.r
            int r0 = r0.G
            if (r0 == r7) goto L_0x029b
            com.immomo.momo.service.bean.a.a r0 = r9.r
            int r0 = r0.G
            r1 = 4
            if (r0 != r1) goto L_0x02b1
        L_0x029b:
            android.view.View r0 = r9.s
            r0.setVisibility(r4)
            android.widget.LinearLayout r0 = r9.w
            r0.setVisibility(r5)
            android.widget.LinearLayout r0 = r9.u
            r0.setVisibility(r4)
            android.widget.LinearLayout r0 = r9.v
            r0.setVisibility(r5)
            goto L_0x015c
        L_0x02b1:
            android.view.View r0 = r9.s
            r0.setVisibility(r5)
            goto L_0x015c
        L_0x02b8:
            com.immomo.momo.util.m r0 = r9.e
            java.lang.String r1 = "relation with this group: nothing"
            r0.a(r1)
            com.immomo.momo.service.bean.a.a r0 = r9.r
            int r0 = r0.G
            if (r0 != r7) goto L_0x027c
            android.view.View r0 = r9.s
            r0.setVisibility(r4)
            android.widget.LinearLayout r0 = r9.w
            r0.setVisibility(r5)
            android.widget.LinearLayout r0 = r9.u
            r0.setVisibility(r5)
            android.widget.LinearLayout r0 = r9.v
            r0.setVisibility(r4)
            goto L_0x015c
        L_0x02db:
            com.immomo.momo.android.view.bi r0 = r9.n
            r0.setVisibility(r5)
            goto L_0x000a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.immomo.momo.android.activity.group.GroupProfileActivity.x():void");
    }

    private void y() {
        int i2 = 8;
        this.T.setVisibility((this.r.I == null || this.r.I.length <= 0) ? 8 : 0);
        View view = this.H;
        if (this.r.I == null || this.r.I.length <= 0) {
            i2 = 0;
        }
        view.setVisibility(i2);
    }

    private void z() {
        this.h = new dq(this, this);
        this.h.execute(new String[]{this.l});
    }

    public final void a(Bundle bundle) {
        super.a(bundle);
        setContentView((int) R.layout.activity_groupprofile);
        this.o = new y();
        this.q = new aq();
        this.p = new v();
        this.j = new u(this);
        this.j.a(this.ac);
        this.m = (HeaderLayout) findViewById(R.id.layout_header);
        this.n = new bi(this);
        this.W = (UserPhotosView) findViewById(R.id.normal_photoview);
        this.n.a((int) R.drawable.ic_topbar_more);
        this.n.setVisibility(8);
        this.m.a(this.n, this.Z);
        this.s = findViewById(R.id.profile_layout_bottom);
        this.u = (LinearLayout) findViewById(R.id.profile_layout_chat);
        this.v = (LinearLayout) findViewById(R.id.profile_layout_apply);
        this.w = (LinearLayout) findViewById(R.id.profile_layout_cancel_create);
        this.x = (TextView) findViewById(R.id.profile_tv_gid);
        this.y = (TextView) findViewById(R.id.profile_tv_distance);
        this.z = (TextView) findViewById(R.id.profile_tv_createtime);
        this.A = (TextView) findViewById(R.id.profile_tv_sign);
        this.B = (TextView) findViewById(R.id.tv_member_count);
        this.C = (TextView) findViewById(R.id.tv_ownername);
        this.D = (TextView) findViewById(R.id.tv_sitename);
        this.E = (ImageView) findViewById(R.id.img_site_arrow);
        this.F = (ImageView) findViewById(R.id.iv_oweravatar);
        this.G = (TextView) findViewById(R.id.profile_baned_tip);
        this.T = findViewById(R.id.view_showmemberlist);
        this.N = findViewById(R.id.groupfeed_layout);
        this.Q = (ImageView) this.N.findViewById(R.id.groupfeed_iv_pic);
        this.O = (TextView) this.N.findViewById(R.id.groupfeed_tv_content);
        this.P = (TextView) this.N.findViewById(R.id.groupfeed_tv_distance);
        this.S = this.N.findViewById(R.id.groupfeed_layout_pic);
        this.R = this.N.findViewById(R.id.groupfeed_iv_party);
        this.t = findViewById(R.id.layout_site);
        this.I = (ImageView) findViewById(R.id.iv_levelicon);
        this.U = new View[8];
        this.U[0] = findViewById(R.id.member_avatar_block0);
        this.U[1] = findViewById(R.id.member_avatar_block1);
        this.U[2] = findViewById(R.id.member_avatar_block2);
        this.U[3] = findViewById(R.id.member_avatar_block3);
        this.U[4] = findViewById(R.id.member_avatar_block4);
        this.U[5] = findViewById(R.id.member_avatar_block5);
        this.U[6] = findViewById(R.id.member_avatar_block6);
        this.U[7] = findViewById(R.id.member_avatar_block7);
        int round = Math.round(((((((float) com.immomo.momo.g.J()) - (com.immomo.momo.g.l().getDimension(R.dimen.pic_item_margin) * 16.0f)) - (com.immomo.momo.g.l().getDimension(R.dimen.style_patterns) * 2.0f)) - (((float) BitmapFactory.decodeResource(getResources(), R.drawable.ic_common_arrow_right).getWidth()) + com.immomo.momo.g.l().getDimension(R.dimen.member_arrow_margin_left))) - (com.immomo.momo.g.l().getDimension(R.dimen.pic_layout_margin) * 2.0f)) / 8.0f);
        for (int i2 = 0; i2 < this.U.length; i2++) {
            ViewGroup.LayoutParams layoutParams = this.U[i2].getLayoutParams();
            layoutParams.height = round;
            layoutParams.width = round;
            this.U[i2].setLayoutParams(layoutParams);
        }
        this.V = (ScrollView) findViewById(R.id.scrollview_content);
        this.M = findViewById(R.id.layout_gid);
        this.H = findViewById(R.id.profile_layout_hidemember);
        this.T.setVisibility(4);
        c(bundle);
    }

    /* access modifiers changed from: protected */
    public final void d() {
    }

    /* access modifiers changed from: protected */
    public final void e() {
        super.e();
        w();
        this.u.setOnClickListener(this.aa);
        this.v.setOnClickListener(this.aa);
        this.w.setOnClickListener(this.aa);
        this.T.setOnClickListener(this.aa);
        findViewById(R.id.layout_ower).setOnClickListener(this.aa);
        findViewById(R.id.layout_site).setOnClickListener(this.aa);
        findViewById(R.id.layout_level).setOnClickListener(this.aa);
        di diVar = new di();
        diVar.a(false);
        this.W.setAvatarClickListener(diVar);
        z();
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        switch (i2) {
            case 13:
                if (i3 == -1) {
                    n.b(this, (int) R.string.str_join_group_apply_tip, new cy()).show();
                    return;
                }
                return;
            case h.DragSortListView_drag_handle_id /*14*/:
                if (i3 == -1) {
                    this.h = new dq(this, this);
                    this.h.execute(new String[]{this.l});
                    return;
                }
                return;
            default:
                return;
        }
    }

    public void onBindIconClicked(View view) {
        b(view.getId());
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (this.h != null && !this.h.isCancelled()) {
            this.h.cancel(true);
        }
        if (this.i != null && !this.i.isCancelled()) {
            this.i.cancel(true);
        }
        if (this.j != null) {
            unregisterReceiver(this.j);
            this.j = null;
        }
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        this.e.a((Object) "onNewIntent");
        String str = intent.getExtras() != null ? (String) intent.getExtras().get("gid") : null;
        if (!android.support.v4.b.a.a((CharSequence) str) && !this.l.equals(str)) {
            Bundle bundle = new Bundle();
            bundle.putBoolean("from_saveinstance", true);
            bundle.putString("gid", str);
            c(bundle);
            v();
            w();
            z();
            this.V.smoothScrollTo(0, 0);
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("group_id", this.l);
            new k("PO", "P31", jSONObject).e();
        } catch (JSONException e) {
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("group_id", this.l);
            new k("PI", "P31", jSONObject).e();
        } catch (JSONException e) {
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        bundle.putBoolean("from_saveinstance", true);
        bundle.putString("gid", this.l);
        bundle.putString("tag", this.k);
        super.onSaveInstanceState(bundle);
    }
}
