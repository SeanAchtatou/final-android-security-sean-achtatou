package com.immomo.momo.android.view;

import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.util.AttributeSet;
import com.immomo.momo.R;

public class ChatListView extends MomoRefreshListView {
    private boolean g = false;
    private Runnable h;

    public ChatListView(Context context) {
        super(context);
        new Handler(Looper.getMainLooper());
        this.h = null;
        s();
    }

    public ChatListView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        new Handler(Looper.getMainLooper());
        this.h = null;
        s();
    }

    public ChatListView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        new Handler(Looper.getMainLooper());
        this.h = null;
        s();
    }

    private void s() {
        setStackFromBottom(true);
        setFastScrollEnabled(false);
        setFastVelocity(0);
        setCompleteScrollTop(false);
    }

    public final void a() {
        this.g = true;
        setOverScrollView(null);
        setOverScrollListener$2880bfd9(null);
    }

    public final void b() {
        if (!this.g) {
            super.b();
        }
    }

    public final void c() {
        int count = getCount() - 1;
        if (count < 0) {
            return;
        }
        if (Build.VERSION.SDK_INT <= 7) {
            setSelectionFromTop(count, -55536);
            return;
        }
        Runnable runnable = this.h;
        if (runnable != null) {
            removeCallbacks(runnable);
        } else {
            runnable = new y(this, (byte) 0);
        }
        postDelayed(runnable, 30);
        this.h = runnable;
    }

    /* access modifiers changed from: protected */
    public int getRefreshLayout() {
        return R.layout.include_pull_to_load;
    }

    /* access modifiers changed from: protected */
    public int getRefreshingLayout() {
        return R.layout.include_pull_to_loading_header;
    }

    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        if (i2 < i4) {
            setSelection(getLastVisiblePosition());
        }
    }

    /* access modifiers changed from: protected */
    public void setOverScrollState(int i) {
        if (i != this.b) {
            this.f.a((Object) ("change state:" + i));
            switch (i) {
                case 1:
                    this.c.setText("下拉加载更多");
                    break;
                case 2:
                    this.c.setText("松开加载更多");
                    break;
                case 3:
                    this.c.setText("正在加载...");
                    break;
                case 4:
                    this.c.setText("下拉加载更多");
                    break;
            }
            this.b = i;
        }
    }
}
