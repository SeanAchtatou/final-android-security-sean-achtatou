package com.immomo.momo.android.b;

import android.location.Location;
import java.util.List;
import mm.purchasesdk.PurchaseCode;

final class g implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ a f2326a;
    private final /* synthetic */ Object b;
    private final /* synthetic */ List c;
    private final /* synthetic */ String d;
    private final /* synthetic */ p e;

    g(a aVar, Object obj, List list, String str, p pVar) {
        this.f2326a = aVar;
        this.b = obj;
        this.c = list;
        this.d = str;
        this.e = pVar;
    }

    public final void run() {
        synchronized (this.b) {
            try {
                this.b.wait(60000);
                if (this.c.size() <= 0) {
                    this.f2326a.b.a(this.d);
                    this.e.a(null, 1, PurchaseCode.QUERY_OK, 201);
                } else {
                    this.e.a((Location) this.c.get(this.c.size() - 1), 1, 100, 201);
                }
            } catch (Exception e2) {
                this.f2326a.f2312a.a((Throwable) e2);
                this.e.a(null, 1, PurchaseCode.QUERY_OK, 201);
            }
        }
        return;
    }
}
