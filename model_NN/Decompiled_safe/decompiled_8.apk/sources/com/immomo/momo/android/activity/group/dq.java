package com.immomo.momo.android.activity.group;

import android.content.Context;
import android.content.Intent;
import com.immomo.momo.R;
import com.immomo.momo.a.e;
import com.immomo.momo.a.g;
import com.immomo.momo.android.broadcast.u;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.n;

final class dq extends d {

    /* renamed from: a  reason: collision with root package name */
    private v f1613a = null;
    /* access modifiers changed from: private */
    public /* synthetic */ GroupProfileActivity c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public dq(GroupProfileActivity groupProfileActivity, Context context) {
        super(context);
        this.c = groupProfileActivity;
        this.f1613a = new v(context);
        this.f1613a.a("请求提交中");
        this.f1613a.setCancelable(true);
        this.f1613a.setOnCancelListener(new dr(this));
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        GroupProfileActivity.o(this.c);
        n.a().a(this.c.r.b, this.c.r);
        this.c.K = this.c.o.c(this.c.f.h, this.c.l);
        this.c.L = this.c.f.h.equals(this.c.r.g);
        return 1;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.b.a((Object) "downloadGroupProfile~~~~~~~~~~~~~~");
        if (this.c.J && this.f1613a != null) {
            this.f1613a.show();
        }
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        this.c.r = this.c.o.e(this.c.l);
        if (exc instanceof e) {
            this.b.a((Throwable) exc);
            a((int) R.string.errormsg_network_normal400);
        } else if (exc instanceof g) {
            this.b.a((Throwable) exc);
            a((int) R.string.errormsg_network_normal403);
        } else {
            super.a(exc);
            if (this.c.r == null || (exc instanceof com.immomo.momo.a.n)) {
                this.c.finish();
            }
        }
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        this.c.u();
        this.c.B();
        GroupProfileActivity.k(this.c);
        Intent intent = new Intent(u.g);
        intent.putExtra("gid", this.c.l);
        this.c.sendBroadcast(intent);
    }

    /* access modifiers changed from: protected */
    public final void b() {
        if (this.c.J && this.f1613a != null) {
            this.f1613a.dismiss();
            this.f1613a = null;
        }
    }
}
