package com.immomo.momo.android.activity.emotestore;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import com.immomo.momo.R;

final class bb implements AdapterView.OnItemClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ av f1304a;

    bb(av avVar) {
        this.f1304a = avVar;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        if (view.getId() == R.id.mineemotion_btn) {
            this.f1304a.D().b(new bf(this.f1304a, this.f1304a.c(), this.f1304a.Q.getItem(i)));
            return;
        }
        av avVar = this.f1304a;
        Intent intent = new Intent(av.H(), EmotionProfileActivity.class);
        intent.putExtra("eid", this.f1304a.Q.getItem(i).f3035a);
        this.f1304a.a(intent);
    }
}
