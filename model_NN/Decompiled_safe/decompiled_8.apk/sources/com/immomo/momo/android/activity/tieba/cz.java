package com.immomo.momo.android.activity.tieba;

import android.content.Context;
import android.os.Bundle;
import com.immomo.momo.android.c.d;
import com.immomo.momo.protocol.a.v;
import com.immomo.momo.service.af;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

final class cz extends d {

    /* renamed from: a  reason: collision with root package name */
    private boolean f2227a = false;
    private /* synthetic */ TiebaAdminActivity c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public cz(TiebaAdminActivity tiebaAdminActivity, Context context) {
        super(context);
        this.c = tiebaAdminActivity;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        ArrayList arrayList = new ArrayList();
        this.f2227a = v.a().a(arrayList, 0);
        this.c.j.c(arrayList);
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        this.c.k.a((Collection) ((List) obj));
        this.c.i.setVisibility(this.f2227a ? 0 : 8);
        Date date = new Date();
        this.c.g.a("tiebaadminlasttime", date);
        this.c.h.setLastFlushTime(date);
        af.c().d(0);
        com.immomo.momo.protocol.imjson.d.b();
        this.c.c(0);
        this.c.t().a(new Bundle(), "actions.tiebareportchanged");
    }

    /* access modifiers changed from: protected */
    public final void b() {
        super.b();
        this.c.h.n();
    }
}
