package com.immomo.momo.android.activity.maintab;

import android.content.DialogInterface;
import android.content.Intent;
import com.immomo.momo.android.activity.EditUserProfileActivity;
import com.immomo.momo.android.activity.EditVipProfileActivity;

final class ci implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ cd f1881a;

    ci(cd cdVar) {
        this.f1881a = cdVar;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f1881a.a(this.f1881a.M.b() ? new Intent(this.f1881a.c(), EditVipProfileActivity.class) : new Intent(this.f1881a.c(), EditUserProfileActivity.class));
    }
}
