package com.immomo.momo.android.activity.maintab;

import android.content.Intent;
import android.view.View;
import android.widget.ExpandableListView;
import com.immomo.momo.android.activity.group.GroupProfileActivity;
import com.immomo.momo.service.bean.a.a;
import com.immomo.momo.service.bean.ay;

final class ah implements ExpandableListView.OnChildClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ x f1834a;

    ah(x xVar) {
        this.f1834a = xVar;
    }

    public final boolean onChildClick(ExpandableListView expandableListView, View view, int i, int i2, long j) {
        x xVar = this.f1834a;
        Intent intent = new Intent(x.H(), GroupProfileActivity.class);
        intent.putExtra("gid", ((a) ((ay) this.f1834a.Q.get(i)).g.get(i2)).b);
        intent.putExtra("tag", "local");
        this.f1834a.a(intent);
        return true;
    }
}
