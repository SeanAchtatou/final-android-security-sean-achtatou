package com.immomo.momo.android.a;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.g;
import com.immomo.momo.plugin.d.b;
import com.immomo.momo.util.e;
import java.util.ArrayList;

public final class hh extends a {
    public hh(Context context) {
        super(context, new ArrayList());
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = g.o().inflate((int) R.layout.listitem_renren, (ViewGroup) null);
            hi hiVar = new hi((byte) 0);
            view.setTag(hiVar);
            hiVar.f859a = (TextView) view.findViewById(R.id.renrenitem_tv_textcontent);
            hiVar.b = (TextView) view.findViewById(R.id.renrenitem_iv_posttime);
        }
        b bVar = (b) getItem(i);
        hi hiVar2 = (hi) view.getTag();
        e.a(hiVar2.f859a, bVar.f2869a == null ? PoiTypeDef.All : bVar.f2869a, d());
        hiVar2.b.setText(bVar.b == null ? "未知" : bVar.b);
        return view;
    }
}
