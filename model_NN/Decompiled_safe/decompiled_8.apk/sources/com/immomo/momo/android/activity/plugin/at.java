package com.immomo.momo.android.activity.plugin;

import android.content.Intent;
import android.view.View;

final class at implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ CommunityStatusActivity f2047a;

    at(CommunityStatusActivity communityStatusActivity) {
        this.f2047a = communityStatusActivity;
    }

    public final void onClick(View view) {
        Intent intent;
        switch (this.f2047a.h) {
            case 1:
                intent = new Intent(this.f2047a, SinaWeiboActivity.class);
                if (this.f2047a.r == null) {
                    intent.putExtra("uid", this.f2047a.f.am);
                    intent.putExtra("momoid", this.f2047a.f.h);
                } else {
                    intent.putExtra("profile", this.f2047a.r);
                }
                intent.putExtra("profile", this.f2047a.r);
                break;
            case 2:
                intent = new Intent(this.f2047a, TxWeiboActivity.class);
                if (this.f2047a.s != null) {
                    intent.putExtra("profile", this.f2047a.s);
                    break;
                } else {
                    intent.putExtra("uid", this.f2047a.f.h);
                    break;
                }
            case 3:
                Intent intent2 = new Intent(this.f2047a, RenrenActivity.class);
                if (this.f2047a.q == null) {
                    intent2.putExtra("momoid", this.f2047a.f.h);
                } else {
                    intent2.putExtra("profile", this.f2047a.q);
                }
                this.f2047a.startActivity(intent2);
                return;
            default:
                return;
        }
        this.f2047a.startActivity(intent);
    }
}
