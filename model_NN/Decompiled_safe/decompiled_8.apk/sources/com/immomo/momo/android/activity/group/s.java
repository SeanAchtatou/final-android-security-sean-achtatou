package com.immomo.momo.android.activity.group;

import android.widget.RadioGroup;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;

final class s implements RadioGroup.OnCheckedChangeListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ EditGroupProfileActivity f1693a;

    s(EditGroupProfileActivity editGroupProfileActivity) {
        this.f1693a = editGroupProfileActivity;
    }

    public final void onCheckedChanged(RadioGroup radioGroup, int i) {
        if (this.f1693a.l) {
            switch (radioGroup.getCheckedRadioButtonId()) {
                case R.id.creategroup_rb_residential /*2131166122*/:
                    this.f1693a.m.L = 1;
                    break;
                case R.id.creategroup_rb_office /*2131166123*/:
                    this.f1693a.m.L = 2;
                    break;
                case R.id.creategroup_rb_unit /*2131166124*/:
                    this.f1693a.m.L = 3;
                    break;
            }
            if (this.f1693a.v) {
                this.f1693a.w.put("type", String.valueOf(this.f1693a.m.L));
            } else {
                this.f1693a.w.put("type", String.valueOf(this.f1693a.m.L));
                this.f1693a.s.setText(PoiTypeDef.All);
                this.f1693a.m.K = null;
            }
            this.f1693a.y = true;
        }
    }
}
