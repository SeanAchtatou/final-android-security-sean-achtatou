package com.immomo.momo.android.a;

import android.app.Activity;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.immomo.a.a.f.a;
import com.immomo.momo.R;
import com.immomo.momo.android.view.EmoteTextView;
import com.immomo.momo.android.view.HandyListView;
import com.immomo.momo.android.view.ce;
import com.immomo.momo.b;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.n;
import com.immomo.momo.service.bean.p;
import com.immomo.momo.util.j;
import java.util.List;

public final class ax extends ce {
    /* access modifiers changed from: private */
    public Activity d = null;
    /* access modifiers changed from: private */
    public List e = null;
    /* access modifiers changed from: private */
    public HandyListView f = null;
    /* access modifiers changed from: private */
    public n g = null;

    public ax(Activity activity, List list, HandyListView handyListView, n nVar) {
        super(activity, list);
        new ay();
        this.d = activity;
        this.e = list;
        this.f = handyListView;
        this.g = nVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.b.a(java.lang.String, boolean):android.graphics.Bitmap
     arg types: [java.lang.String, int]
     candidates:
      com.immomo.momo.b.a(java.lang.String, android.graphics.Bitmap):android.graphics.Bitmap
      com.immomo.momo.b.a(java.lang.String, boolean):android.graphics.Bitmap */
    public final View a(int i, View view) {
        if (view == null) {
            bg bgVar = new bg((byte) 0);
            view = LayoutInflater.from(this.d).inflate((int) R.layout.listitem_groupuser, (ViewGroup) null);
            bgVar.i = view.findViewById(R.id.layout_time_container);
            bgVar.f736a = view.findViewById(R.id.layout_item_container);
            bgVar.b = (ImageView) view.findViewById(R.id.userlist_item_iv_face);
            bgVar.c = (TextView) view.findViewById(R.id.userlist_item_tv_name);
            bgVar.d = (TextView) view.findViewById(R.id.userlist_item_tv_age);
            bgVar.e = (TextView) view.findViewById(R.id.userlist_item_tv_distance);
            bgVar.f = (TextView) view.findViewById(R.id.profile_tv_time);
            bgVar.g = (EmoteTextView) view.findViewById(R.id.userlist_item_tv_sign);
            bgVar.j = (ImageView) view.findViewById(R.id.userlist_item_iv_gender);
            bgVar.h = view.findViewById(R.id.userlist_item_layout_genderbackgroud);
            bgVar.k = (ImageView) view.findViewById(R.id.userlist_item_pic_iv_weibo);
            bgVar.l = (ImageView) view.findViewById(R.id.userlist_item_pic_iv_tweibo);
            bgVar.m = (ImageView) view.findViewById(R.id.userlist_item_pic_iv_renren);
            bgVar.n = (ImageView) view.findViewById(R.id.userlist_item_pic_iv_vip);
            view.findViewById(R.id.userlist_item_pic_iv_douban);
            bgVar.o = (ImageView) view.findViewById(R.id.userlist_item_pic_iv_relation);
            view.findViewById(R.id.userlist_item_pic_iv_device);
            bgVar.p = (ImageView) view.findViewById(R.id.userlist_item_pic_iv_multipic);
            bgVar.q = (ImageView) view.findViewById(R.id.userlist_item_pic_iv_industry);
            bgVar.r = view.findViewById(R.id.triangle_zone);
            bgVar.s = (ImageView) view.findViewById(R.id.userlist_item_pic_sign);
            view.setTag(R.id.tag_userlist_item, bgVar);
        }
        p pVar = (p) getItem(i);
        bg bgVar2 = (bg) view.getTag(R.id.tag_userlist_item);
        bgVar2.f736a.setOnClickListener(new az(this, i));
        bgVar2.f736a.setOnLongClickListener(new ba(this, i));
        bgVar2.r.setOnClickListener(new bb(this, bgVar2, i));
        if (this.g != null) {
            if (!g.q().h.equals(this.g.c) || this.f.f()) {
                bgVar2.r.setVisibility(8);
            } else {
                bgVar2.r.setVisibility(0);
            }
        }
        if (pVar.h != null) {
            bgVar2.e.setText(pVar.h.Z);
            if (g.a((int) R.string.profile_distance_hide).equals(pVar.h.Z) || g.a((int) R.string.profile_distance_unknown).equals(pVar.h.Z)) {
                bgVar2.i.setVisibility(8);
            } else {
                bgVar2.i.setVisibility(0);
            }
            if (!a.a(pVar.h.aa)) {
                bgVar2.f.setText(" | " + pVar.h.aa);
            }
            bgVar2.d.setText(new StringBuilder(String.valueOf(pVar.h.I)).toString());
            bgVar2.c.setText(pVar.h.h());
            if (pVar.h.b()) {
                bgVar2.c.setTextColor(g.c((int) R.color.font_vip_name));
            } else {
                bgVar2.c.setTextColor(g.c((int) R.color.text_color));
            }
            bgVar2.g.setText(pVar.h.n());
            if (!a.a(pVar.h.R)) {
                Bitmap b = b.b(pVar.h.R);
                if (b != null) {
                    bgVar2.s.setVisibility(0);
                    bgVar2.s.setImageBitmap(b);
                } else {
                    bgVar2.s.setVisibility(8);
                }
            } else {
                bgVar2.s.setVisibility(8);
            }
            if ("F".equals(pVar.h.H)) {
                bgVar2.h.setBackgroundResource(R.drawable.bg_gender_famal);
                bgVar2.j.setImageResource(R.drawable.ic_user_famale);
            } else {
                bgVar2.h.setBackgroundResource(R.drawable.bg_gender_male);
                bgVar2.j.setImageResource(R.drawable.ic_user_male);
            }
            if (pVar.h.j()) {
                bgVar2.p.setVisibility(0);
            } else {
                bgVar2.p.setVisibility(8);
            }
            if ("both".equals(pVar.h.P)) {
                bgVar2.o.setVisibility(0);
            } else {
                bgVar2.o.setVisibility(8);
            }
            if (pVar.h.an) {
                bgVar2.k.setVisibility(0);
                bgVar2.k.setImageResource(pVar.h.ap ? R.drawable.ic_userinfo_weibov : R.drawable.ic_userinfo_weibo);
            } else {
                bgVar2.k.setVisibility(8);
            }
            if (pVar.h.at) {
                bgVar2.l.setVisibility(0);
                bgVar2.l.setImageResource(pVar.h.au ? R.drawable.ic_userinfo_tweibov : R.drawable.ic_userinfo_tweibo);
            } else {
                bgVar2.l.setVisibility(8);
            }
            if (pVar.h.ar) {
                bgVar2.m.setVisibility(0);
            } else {
                bgVar2.m.setVisibility(8);
            }
            if (pVar.h.b()) {
                bgVar2.n.setVisibility(0);
                if (pVar.h.c()) {
                    bgVar2.n.setImageResource(R.drawable.ic_userinfo_vip_year);
                } else {
                    bgVar2.n.setImageResource(R.drawable.ic_userinfo_vip);
                }
            } else {
                bgVar2.n.setVisibility(8);
            }
            if (!a.a(pVar.h.M)) {
                bgVar2.q.setVisibility(0);
                bgVar2.q.setImageBitmap(b.a(pVar.h.M, true));
            } else {
                bgVar2.q.setVisibility(8);
            }
            j.a(pVar.h, bgVar2.b, this.f, 3);
        }
        return view;
    }
}
