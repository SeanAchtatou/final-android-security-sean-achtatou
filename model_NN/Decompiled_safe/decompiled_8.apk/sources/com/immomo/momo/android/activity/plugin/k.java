package com.immomo.momo.android.activity.plugin;

import android.graphics.Bitmap;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.immomo.momo.R;
import com.immomo.momo.util.ao;

final class k extends WebViewClient {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ BindFaceBookActivity f2090a;

    private k(BindFaceBookActivity bindFaceBookActivity) {
        this.f2090a = bindFaceBookActivity;
    }

    /* synthetic */ k(BindFaceBookActivity bindFaceBookActivity, byte b) {
        this(bindFaceBookActivity);
    }

    public final void doUpdateVisitedHistory(WebView webView, String str, boolean z) {
        if (str.indexOf("login") < 0) {
            webView.loadUrl(this.f2090a.n);
        }
        super.doUpdateVisitedHistory(webView, str, z);
    }

    public final void onLoadResource(WebView webView, String str) {
        super.onLoadResource(webView, str);
    }

    public final void onPageFinished(WebView webView, String str) {
        super.onPageFinished(webView, str);
        if (this.f2090a.l != null && this.f2090a.l.isShowing()) {
            this.f2090a.l.dismiss();
        }
    }

    public final void onPageStarted(WebView webView, String str, Bitmap bitmap) {
        super.onPageStarted(webView, str, bitmap);
        if (this.f2090a.l != null) {
            this.f2090a.l.a("正在加载，请稍候...");
            this.f2090a.l.show();
        }
    }

    public final void onReceivedError(WebView webView, int i, String str, String str2) {
        if (i == -2) {
            ao.e(R.string.errormsg_network_unfind);
        } else {
            ao.e(R.string.errormsg_server);
        }
        if (this.f2090a.l.isShowing()) {
            this.f2090a.l.dismiss();
        }
    }

    public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
        this.f2090a.e.b((Object) ("shouldOverrideUrlLoading:" + str));
        if (str.indexOf(this.f2090a.i) < 0) {
            return super.shouldOverrideUrlLoading(webView, str);
        }
        BindFaceBookActivity bindFaceBookActivity = this.f2090a;
        this.f2090a.c(str);
        BindFaceBookActivity.g(bindFaceBookActivity);
        return true;
    }
}
