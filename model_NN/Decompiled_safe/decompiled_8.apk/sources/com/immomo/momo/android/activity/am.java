package com.immomo.momo.android.activity;

import java.util.ArrayList;
import java.util.Collection;

public final class am extends ArrayList {
    private synchronized void b() {
        an[] anVarArr = (an[]) toArray(new an[size()]);
        for (int i = 1; i < anVarArr.length; i++) {
            an anVar = anVarArr[i];
            int i2 = i;
            while (i2 > 0 && anVarArr[i2 - 1].f1029a < anVar.f1029a) {
                anVarArr[i2] = anVarArr[i2 - 1];
                i2--;
            }
            anVarArr[i2] = anVar;
        }
        clear();
        for (an add : anVarArr) {
            super.add(add);
        }
    }

    public final an a() {
        if (size() > 0) {
            return (an) get(0);
        }
        return null;
    }

    /* renamed from: a */
    public final boolean add(an anVar) {
        boolean add = super.add(anVar);
        b();
        return add;
    }

    public final /* synthetic */ void add(int i, Object obj) {
        throw new UnsupportedOperationException();
    }

    public final boolean addAll(int i, Collection collection) {
        throw new UnsupportedOperationException();
    }

    public final boolean addAll(Collection collection) {
        boolean addAll = super.addAll(collection);
        b();
        return addAll;
    }
}
