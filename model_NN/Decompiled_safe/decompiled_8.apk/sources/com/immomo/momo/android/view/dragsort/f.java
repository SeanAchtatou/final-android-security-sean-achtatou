package com.immomo.momo.android.view.dragsort;

import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.Checkable;
import android.widget.ListAdapter;

final class f extends BaseAdapter {

    /* renamed from: a  reason: collision with root package name */
    private ListAdapter f2793a;
    private /* synthetic */ DragSortListView b;

    public f(DragSortListView dragSortListView, ListAdapter listAdapter) {
        this.b = dragSortListView;
        this.f2793a = listAdapter;
        this.f2793a.registerDataSetObserver(new g(this));
    }

    public final ListAdapter a() {
        return this.f2793a;
    }

    public final boolean areAllItemsEnabled() {
        return this.f2793a.areAllItemsEnabled();
    }

    public final int getCount() {
        return this.f2793a.getCount();
    }

    public final Object getItem(int i) {
        return this.f2793a.getItem(i);
    }

    public final long getItemId(int i) {
        return this.f2793a.getItemId(i);
    }

    public final int getItemViewType(int i) {
        return this.f2793a.getItemViewType(i);
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        c cVar;
        if (view != null) {
            cVar = (c) view;
            View childAt = cVar.getChildAt(0);
            View view2 = this.f2793a.getView(i, childAt, this.b);
            if (view2 != childAt) {
                if (childAt != null) {
                    cVar.removeViewAt(0);
                }
                cVar.addView(view2);
            }
        } else {
            View view3 = this.f2793a.getView(i, null, this.b);
            c dVar = view3 instanceof Checkable ? new d(this.b.getContext()) : new c(this.b.getContext());
            dVar.setLayoutParams(new AbsListView.LayoutParams(-1, -2));
            dVar.addView(view3);
            cVar = dVar;
        }
        this.b.a(this.b.getHeaderViewsCount() + i, cVar, true);
        return cVar;
    }

    public final int getViewTypeCount() {
        return this.f2793a.getViewTypeCount();
    }

    public final boolean hasStableIds() {
        return this.f2793a.hasStableIds();
    }

    public final boolean isEmpty() {
        return this.f2793a.isEmpty();
    }

    public final boolean isEnabled(int i) {
        return this.f2793a.isEnabled(i);
    }
}
