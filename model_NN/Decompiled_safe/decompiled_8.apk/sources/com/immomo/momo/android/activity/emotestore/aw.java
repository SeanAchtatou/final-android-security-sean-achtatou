package com.immomo.momo.android.activity.emotestore;

import android.content.Intent;
import com.immomo.momo.android.broadcast.d;
import com.immomo.momo.android.broadcast.g;
import com.immomo.momo.service.bean.q;

final class aw implements d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ av f1299a;

    aw(av avVar) {
        this.f1299a = avVar;
    }

    public final void a(Intent intent) {
        String action = intent.getAction();
        q a2 = this.f1299a.P.a(intent.getStringExtra("eid"));
        if (a2 != null) {
            if (g.c.equals(action)) {
                this.f1299a.a(String.valueOf(a2.b) + "下载失败");
            } else if (g.f2350a.equals(action)) {
                this.f1299a.a(String.valueOf(a2.b) + "下载完成");
                a2.t = true;
                a2.u = true;
                int indexOf = this.f1299a.T.indexOf(a2);
                if (indexOf >= 0) {
                    this.f1299a.T.remove(indexOf);
                }
                this.f1299a.T.add(0, a2);
                int indexOf2 = this.f1299a.U.indexOf(a2);
                if (indexOf2 >= 0) {
                    this.f1299a.U.remove(indexOf2);
                }
            }
            this.f1299a.Q.notifyDataSetChanged();
        }
    }
}
