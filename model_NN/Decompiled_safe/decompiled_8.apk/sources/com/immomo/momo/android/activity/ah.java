package com.immomo.momo.android.activity;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.b.a;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.broadcast.b;
import com.immomo.momo.android.broadcast.m;
import com.immomo.momo.android.view.PushDownLayout;
import com.immomo.momo.g;
import com.immomo.momo.protocol.imjson.d;
import com.immomo.momo.service.aq;
import com.immomo.momo.service.bean.bf;
import mm.purchasesdk.PurchaseCode;

public abstract class ah extends ao {
    /* access modifiers changed from: private */
    public View h = null;
    private TextView i = null;
    private ImageView j = null;
    private ImageView k = null;
    private boolean l = false;
    private b m = null;
    private am n = new am();
    private View.OnClickListener o = new ai(this);
    private Handler p = new ak(this);
    private int q = 1;

    protected static void i() {
    }

    private boolean u() {
        return this.h != null && this.h.getVisibility() == 0;
    }

    private void v() {
        if (this.h == null) {
            this.e.a((Object) "topTipView==null");
            return;
        }
        an a2 = this.n.a();
        if (a2 != null) {
            String str = a2.c;
            Bitmap decodeResource = (a2.d ? R.drawable.ic_common_arrow_right : 0) > 0 ? BitmapFactory.decodeResource(getResources(), R.drawable.ic_common_arrow_right) : null;
            if (this.h != null) {
                if (this.i != null) {
                    TextView textView = this.i;
                    if (str == null) {
                        str = PoiTypeDef.All;
                    }
                    textView.setText(str);
                }
                if (this.j != null) {
                    this.j.setVisibility(8);
                    this.j.setOnClickListener(this.o);
                }
                if (this.k != null) {
                    if (decodeResource != null) {
                        this.k.setImageBitmap(decodeResource);
                        this.k.setVisibility(0);
                    } else {
                        this.k.setVisibility(8);
                    }
                    this.k.setOnClickListener(this.o);
                }
                this.h.setTag(R.id.tag_item, null);
                if (this.h != null) {
                    if (!this.h.isShown()) {
                        this.h.setVisibility(0);
                    }
                    this.p.removeMessages(123);
                }
            }
            boolean z = a2.d;
            if (this.h != null) {
                this.h.setClickable(z);
            }
            this.h.setTag(R.id.tag_item, a2);
            return;
        }
        this.p.sendEmptyMessageDelayed(123, 1000);
    }

    /* access modifiers changed from: protected */
    public void a(Bundle bundle) {
    }

    /* access modifiers changed from: protected */
    public void a(an anVar) {
    }

    /* access modifiers changed from: protected */
    public boolean a(Bundle bundle, String str) {
        if ("actions.removeimjwarning".equals(str)) {
            if (this.h != null && u()) {
                c(new an(1008));
            }
        } else if ("actions.imjwarning".equals(str) && this.h != null) {
            String string = bundle.getString("imwmsg");
            String string2 = bundle.getString("imwtype");
            if (string != null) {
                if ("XMPP_AUTHFAILED".equals(string2)) {
                    b(new an(1007, string, Integer.MAX_VALUE));
                } else {
                    b(new an(string));
                }
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public void b(Bundle bundle) {
    }

    public final void b(an anVar) {
        this.e.a((Object) ("message=" + anVar));
        if (!((Boolean) this.g.b("tips_" + anVar.b, false)).booleanValue()) {
            if (anVar.f1029a <= 0) {
                int i2 = this.q;
                this.q = i2 + 1;
                anVar.f1029a = i2;
            }
            if (this.n.indexOf(anVar) < 0) {
                this.n.add(anVar);
            } else {
                this.n.remove(anVar);
                b(anVar);
            }
            v();
            return;
        }
        this.e.a((Object) ("miss, " + anVar));
    }

    public final void c(an anVar) {
        this.n.remove(anVar);
        v();
    }

    /* access modifiers changed from: protected */
    public void e() {
        super.e();
        if (this.h != null) {
            this.i = (TextView) this.h.findViewById(R.id.toptip_text);
            this.j = (ImageView) this.h.findViewById(R.id.toptip_icon_left);
            this.k = (ImageView) this.h.findViewById(R.id.toptip_icon_right);
            this.h.setOnClickListener(this.o);
            this.i.setClickable(false);
        }
        if (k()) {
            StringBuilder sb = new StringBuilder();
            try {
                boolean a2 = d.a(sb, null);
                String sb2 = a.a(sb) ? "通讯服务已经断开" : sb.toString();
                if (!a2) {
                    if (!u() && (this.h instanceof PushDownLayout)) {
                        ((PushDownLayout) this.h).a();
                    }
                    b(new an(sb2));
                } else if (u()) {
                    c(new an(1008));
                }
            } catch (Exception e) {
            }
            a((int) PurchaseCode.QUERY_FROZEN, "actions.removeimjwarning", "actions.imjwarning");
        }
    }

    /* access modifiers changed from: protected */
    public final void f() {
        finish();
    }

    public final boolean g() {
        return this.l;
    }

    public final bf h() {
        return this.f;
    }

    /* access modifiers changed from: protected */
    public final void j() {
        this.h.setVisibility(8);
    }

    /* access modifiers changed from: protected */
    public boolean k() {
        return false;
    }

    /* access modifiers changed from: protected */
    public final void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        b(bundle);
        if (this.f == null || !t().j()) {
            finish();
            return;
        }
        a(bundle);
        if (bundle != null) {
            try {
                new aq().a(this.f, this.f.h);
            } catch (Exception e) {
                finish();
                return;
            }
        }
        this.m = new m(this);
        this.m.a(new al(this));
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.m != null) {
            unregisterReceiver(this.m);
            this.m = null;
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onPostCreate(Bundle bundle) {
        super.onPostCreate(bundle);
        this.l = true;
        if (findViewById(R.id.layout_content) != null) {
            this.h = g.o().inflate((int) R.layout.common_toptip, (ViewGroup) null);
            try {
                ViewGroup viewGroup = (ViewGroup) findViewById(R.id.layout_content);
                if (viewGroup == null || this.h == null) {
                    this.e.a((Object) "onFillTopTip, false");
                    v();
                }
                if (viewGroup instanceof LinearLayout) {
                    ((LinearLayout) viewGroup).setOrientation(1);
                }
                viewGroup.addView(this.h, 0);
                this.e.a((Object) "onFillTopTip, true");
                v();
            } catch (Exception e) {
                this.e.a((Throwable) e);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }
}
