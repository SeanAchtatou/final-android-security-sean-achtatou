package com.immomo.momo.android.activity.group;

import android.view.View;

final class g implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ EditGroupPartyActivity f1682a;

    g(EditGroupPartyActivity editGroupPartyActivity) {
        this.f1682a = editGroupPartyActivity;
    }

    public final void onClick(View view) {
        if (this.f1682a.u() || this.f1682a.B == 3) {
            EditGroupPartyActivity.f(this.f1682a);
        } else {
            this.f1682a.finish();
        }
    }
}
