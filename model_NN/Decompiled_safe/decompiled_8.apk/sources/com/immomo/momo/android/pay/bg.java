package com.immomo.momo.android.pay;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.a;
import java.util.Map;

final class bg extends d {

    /* renamed from: a  reason: collision with root package name */
    private v f2550a = null;
    private Map c;
    private /* synthetic */ RechargeActivity d;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public bg(RechargeActivity rechargeActivity, Context context, Map map) {
        super(context);
        this.d = rechargeActivity;
        this.c = map;
        this.f2550a = new v(context);
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ Object a(Object... objArr) {
        return a.a().a(this.c);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
        this.f2550a.setCancelable(true);
        this.f2550a.setOnCancelListener(new bh(this));
        this.f2550a.a("请求提交中...");
        this.d.a(this.f2550a);
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        super.a(exc);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        String str = (String) obj;
        this.b.b((Object) ("sign: " + str));
        if (new ap().a(str, this.d.z, this.d)) {
            this.d.A = new v(this.d);
            this.d.A.setCancelable(false);
            this.d.A.a("正在启动支付宝....");
            this.d.A.show();
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.d.p();
    }
}
