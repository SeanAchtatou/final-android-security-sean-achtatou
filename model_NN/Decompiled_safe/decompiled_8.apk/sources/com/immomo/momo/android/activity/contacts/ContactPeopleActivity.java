package com.immomo.momo.android.activity.contacts;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.b.a;
import android.view.View;
import android.view.ViewGroup;
import com.immomo.momo.R;
import com.immomo.momo.android.a.ar;
import com.immomo.momo.android.activity.ah;
import com.immomo.momo.android.view.EmoteEditeText;
import com.immomo.momo.android.view.MomoUnrefreshExpandableListView;
import com.immomo.momo.android.view.a.n;
import com.immomo.momo.g;
import com.immomo.momo.service.c;
import com.immomo.momo.util.aq;
import com.immomo.momo.util.k;
import com.immomo.momo.util.u;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class ContactPeopleActivity extends ah {
    Comparator h;
    /* access modifiers changed from: private */
    public ar i = null;
    private MomoUnrefreshExpandableListView j = null;
    /* access modifiers changed from: private */
    public List k;
    /* access modifiers changed from: private */
    public c l = null;
    /* access modifiers changed from: private */
    public boolean m = true;

    public ContactPeopleActivity() {
        new Handler();
        this.h = new ab();
    }

    static /* synthetic */ void a(ContactPeopleActivity contactPeopleActivity, int i2, int i3) {
        View inflate = g.o().inflate((int) R.layout.dialog_contactpeople_apply, (ViewGroup) null);
        EmoteEditeText emoteEditeText = (EmoteEditeText) inflate.findViewById(R.id.edittext_reason);
        emoteEditeText.addTextChangedListener(new aq(24));
        n nVar = new n(contactPeopleActivity);
        nVar.setTitle("好友验证");
        nVar.setContentView(inflate);
        nVar.a();
        nVar.a(0, contactPeopleActivity.getString(R.string.dialog_btn_confim), new ae(contactPeopleActivity, emoteEditeText, i2, i3));
        nVar.a(1, contactPeopleActivity.getString(R.string.dialog_btn_cancel), new af());
        nVar.getWindow().setSoftInputMode(4);
        nVar.show();
    }

    static /* synthetic */ void a(ContactPeopleActivity contactPeopleActivity, String str, String str2) {
        if (!a.a((CharSequence) str)) {
            Intent intent = new Intent("android.intent.action.SENDTO", Uri.parse("smsto:" + contactPeopleActivity.l.b(str)));
            intent.putExtra("sms_body", str2);
            contactPeopleActivity.startActivity(intent);
        }
    }

    /* access modifiers changed from: private */
    public void u() {
        if (this.k != null) {
            this.i = new ar(this.k, this.j);
            this.j.setAdapter(this.i);
            this.i.a();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, com.immomo.momo.android.view.MomoUnrefreshExpandableListView, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        super.a(bundle);
        setContentView((int) R.layout.activity_contactpeople);
        m().setTitleText((int) R.string.contact_title);
        this.j = (MomoUnrefreshExpandableListView) findViewById(R.id.listview_contact);
        this.j.setMMHeaderView(g.o().inflate((int) R.layout.listitem_contactgroup, (ViewGroup) this.j, false));
        this.j.setOnChildClickListener(new ac(this));
        this.j.setOnGroupClickListener(new ad());
        d();
    }

    public final void c(String str) {
        n a2 = n.a(this, "请求已发送成功，该好友可能不能及时收到添加好友消息，是否用短信立即通知他？", "短信通知", "取消", new ag(this, str), (DialogInterface.OnClickListener) null);
        a2.setCanceledOnTouchOutside(false);
        a2.show();
    }

    /* access modifiers changed from: protected */
    public final void d() {
        this.l = new c();
        if (u.c("contactpeopleCache")) {
            this.k = (ArrayList) u.b("contactpeopleCache");
        }
        u();
        new aj(this, this).execute(new Object[0]);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        new k("PO", "P692").e();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        new k("PI", "P692").e();
    }
}
