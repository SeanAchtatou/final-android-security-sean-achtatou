package com.immomo.momo.android.activity;

import android.content.Intent;
import android.support.v4.b.a;
import com.immomo.momo.android.broadcast.d;
import com.immomo.momo.service.bean.bf;

final class fc implements d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ HiddenlistActivity f1431a;

    fc(HiddenlistActivity hiddenlistActivity) {
        this.f1431a = hiddenlistActivity;
    }

    public final void a(Intent intent) {
        String stringExtra = intent.getStringExtra("momoid");
        if (!a.a((CharSequence) stringExtra) && this.f1431a.l != null) {
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 < this.f1431a.l.getCount()) {
                    bf bfVar = (bf) this.f1431a.l.getItem(i2);
                    if (bfVar.h.equals(stringExtra)) {
                        this.f1431a.k.a(bfVar, stringExtra);
                        this.f1431a.l.notifyDataSetChanged();
                        return;
                    }
                    i = i2 + 1;
                } else {
                    return;
                }
            }
        }
    }
}
