package com.immomo.momo.android.a;

import android.content.Context;
import android.content.Intent;
import com.immomo.momo.android.broadcast.f;
import com.immomo.momo.android.c.b;
import com.immomo.momo.protocol.a.k;
import com.immomo.momo.service.ai;
import com.immomo.momo.service.bean.ab;

final class cy extends b {
    private ab c;
    private /* synthetic */ cs d;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public cy(cs csVar, Context context) {
        super(context);
        this.d = csVar;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        this.c = ((ab[]) objArr)[0];
        String b = k.a().b(this.c.h);
        new ai().f(this.c.h);
        Intent intent = new Intent(f.f2349a);
        intent.putExtra("feedid", this.c.h);
        intent.putExtra("siteid", this.c.e);
        intent.putExtra("userid", this.d.e.h().h);
        this.d.e.sendBroadcast(intent);
        return b;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        String str = (String) obj;
        a(str);
        this.d.c(this.c);
        super.a((Object) str);
    }
}
