package com.immomo.momo.android.activity;

import android.content.Context;
import android.support.v4.b.a;
import com.immomo.momo.R;
import com.immomo.momo.a.n;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.w;

final class ja extends d {

    /* renamed from: a  reason: collision with root package name */
    private v f1756a;
    /* access modifiers changed from: private */
    public /* synthetic */ OtherProfileV2Activity c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ja(OtherProfileV2Activity otherProfileV2Activity, Context context) {
        super(context);
        this.c = otherProfileV2Activity;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        return w.a().c(this.c.t.h);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.f1756a = new v(this.c);
        this.f1756a.a("请求提交中");
        this.f1756a.setCancelable(true);
        this.f1756a.setOnCancelListener(new jb(this));
        this.f1756a.show();
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        if (!(exc instanceof n) || this.c.f.b()) {
            super.a(exc);
            return;
        }
        this.c.a(com.immomo.momo.android.view.a.n.a(this.c, (int) R.string.nonvip_followuser_dialog_msg, (int) R.string.nonvip__dialog_enter, (int) R.string.nonvip__dialog_cancel, new jc(this), new jd(this)));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.android.activity.OtherProfileV2Activity.a(com.immomo.momo.android.activity.OtherProfileV2Activity, boolean):void
     arg types: [com.immomo.momo.android.activity.OtherProfileV2Activity, int]
     candidates:
      com.immomo.momo.android.activity.OtherProfileV2Activity.a(com.immomo.momo.android.activity.OtherProfileV2Activity, int):void
      com.immomo.momo.android.activity.OtherProfileV2Activity.a(com.immomo.momo.android.activity.OtherProfileV2Activity, com.immomo.momo.android.a.gv):void
      com.immomo.momo.android.activity.OtherProfileV2Activity.a(com.immomo.momo.android.activity.OtherProfileV2Activity, com.immomo.momo.android.activity.ja):void
      com.immomo.momo.android.activity.OtherProfileV2Activity.a(com.immomo.momo.android.activity.OtherProfileV2Activity, com.immomo.momo.android.activity.je):void
      com.immomo.momo.android.activity.OtherProfileV2Activity.a(com.immomo.momo.android.activity.OtherProfileV2Activity, com.immomo.momo.android.view.EmoteEditeText):void
      com.immomo.momo.android.activity.OtherProfileV2Activity.a(com.immomo.momo.android.activity.OtherProfileV2Activity, java.util.List):void
      com.immomo.momo.android.activity.ah.a(android.os.Bundle, java.lang.String):boolean
      com.immomo.momo.android.activity.ao.a(int, java.lang.String[]):com.immomo.momo.protocol.imjson.c.a
      com.immomo.momo.android.activity.ao.a(com.immomo.momo.android.view.bi, android.view.View$OnClickListener):void
      com.immomo.momo.android.activity.ao.a(java.lang.CharSequence, int):void
      com.immomo.momo.android.activity.ao.a(android.os.Bundle, java.lang.String):boolean
      com.immomo.momo.android.activity.OtherProfileV2Activity.a(com.immomo.momo.android.activity.OtherProfileV2Activity, boolean):void */
    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        String str = (String) obj;
        if (!a.a((CharSequence) str)) {
            this.c.m = true;
            a(str);
            OtherProfileV2Activity.a(this.c, 0);
            OtherProfileV2Activity.n(this.c);
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
        if (this.f1756a != null && this.f1756a.isShowing() && !this.c.isFinishing()) {
            this.f1756a.dismiss();
        }
    }
}
