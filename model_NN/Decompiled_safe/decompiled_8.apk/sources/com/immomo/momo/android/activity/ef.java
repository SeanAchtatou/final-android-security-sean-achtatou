package com.immomo.momo.android.activity;

import android.graphics.Bitmap;
import com.immomo.a.a.f.b;
import com.immomo.momo.util.i;

final class ef implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ee f1272a;
    private final /* synthetic */ Bitmap b;

    ef(ee eeVar, Bitmap bitmap) {
        this.f1272a = eeVar;
        this.b = bitmap;
    }

    public final void run() {
        this.f1272a.f1271a.b.setImageBitmap(this.b);
        this.f1272a.f1271a.c.setText(this.f1272a.f1271a.f1270a.b);
        i.a(b.a(), this.b);
    }
}
