package com.immomo.momo.android.activity.contacts;

import android.content.Intent;
import android.support.v4.b.a;
import com.immomo.momo.android.broadcast.d;

final class ao implements d {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ an f1146a;

    ao(an anVar) {
        this.f1146a = anVar;
    }

    public final void a(Intent intent) {
        String str = intent.getExtras() != null ? (String) intent.getExtras().get("momoid") : null;
        if (!a.a((CharSequence) str) && this.f1146a.P != null) {
            new Thread(new ap(this, str)).start();
        }
    }
}
