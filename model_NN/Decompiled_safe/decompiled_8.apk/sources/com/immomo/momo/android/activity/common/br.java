package com.immomo.momo.android.activity.common;

import android.support.v4.b.a;
import com.immomo.momo.android.view.cv;
import java.util.Date;

final class br implements cv {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ba f1111a;

    private br(ba baVar) {
        this.f1111a = baVar;
    }

    /* synthetic */ br(ba baVar, byte b) {
        this(baVar);
    }

    public final void v() {
        this.f1111a.S = new Date();
        this.f1111a.N.c("lasttime_my_grouplist", a.c(this.f1111a.S));
        this.f1111a.O.n();
        if (this.f1111a.W != null && !this.f1111a.W.isCancelled()) {
            this.f1111a.W.cancel(true);
        }
    }
}
