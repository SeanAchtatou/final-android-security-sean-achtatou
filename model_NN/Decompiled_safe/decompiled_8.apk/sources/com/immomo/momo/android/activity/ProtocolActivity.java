package com.immomo.momo.android.activity;

import android.os.Bundle;
import android.webkit.WebSettings;
import com.immomo.momo.R;
import com.immomo.momo.g;

public class ProtocolActivity extends as {
    public static final String i = ("http://m.immomo.com/inc/android/agreement.html?v=" + g.z());

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.h.loadUrl(i);
        this.h.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        AboutTabsActivity.f921a.setTitleText((int) R.string.abouts_protocol);
    }
}
