package com.immomo.momo.android.activity.emotestore;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.w;
import android.view.KeyEvent;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.ah;
import com.immomo.momo.android.activity.lh;

public class MineEmotesActivity extends ah {
    private lh h = null;

    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        super.a(bundle);
        setContentView((int) R.layout.activity_common_activtytofragment);
        if (bundle != null) {
            Fragment a2 = c().a(av.class.getName());
            w a3 = c().a();
            if (a2 != null) {
                a3.b(a2);
            }
            a3.c();
        }
        this.h = new av();
        w a4 = c().a();
        a4.a(this.h);
        a4.c();
    }

    /* access modifiers changed from: protected */
    public final void e() {
        super.e();
        this.h.a(this, m());
    }

    public void onBackPressed() {
        if (!this.h.G()) {
            super.onBackPressed();
        }
    }

    public boolean onKeyUp(int i, KeyEvent keyEvent) {
        if (this.h == null || !(this.h instanceof lh) || !this.h.b(i, keyEvent)) {
            return super.onKeyUp(i, keyEvent);
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.h.af();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.h.ad();
    }
}
