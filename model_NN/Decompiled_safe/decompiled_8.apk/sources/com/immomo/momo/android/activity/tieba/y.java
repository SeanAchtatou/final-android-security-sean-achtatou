package com.immomo.momo.android.activity.tieba;

import android.content.Intent;
import android.view.View;
import com.immomo.momo.android.activity.setting.CommunityBindActivity;

final class y implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ x f2305a;
    private final /* synthetic */ int b;

    y(x xVar, int i) {
        this.f2305a = xVar;
        this.b = i;
    }

    public final void onClick(View view) {
        boolean z = false;
        if (this.b == 5 || this.b == 6) {
            x xVar = this.f2305a;
            if (!this.f2305a.f) {
                z = true;
            }
            xVar.a(z);
        } else if (this.f2305a.g) {
            x xVar2 = this.f2305a;
            if (!this.f2305a.f) {
                z = true;
            }
            xVar2.a(z);
        } else {
            Intent intent = new Intent(this.f2305a.f2304a.n(), CommunityBindActivity.class);
            intent.putExtra("type", this.b);
            this.f2305a.f2304a.startActivityForResult(intent, this.f2305a.i);
        }
    }
}
