package com.immomo.momo.android.activity.emotestore;

import android.content.Context;
import android.widget.ListAdapter;
import com.immomo.momo.android.c.d;
import com.immomo.momo.protocol.a.i;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

final class al extends d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ae f1290a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public al(ae aeVar, Context context) {
        super(context);
        this.f1290a = aeVar;
        if (aeVar.S != null) {
            aeVar.S.cancel(true);
        }
        aeVar.S = this;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        ArrayList arrayList = new ArrayList();
        i.a().c(arrayList, 0);
        this.f1290a.Q.d(arrayList);
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public final void a() {
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        super.a(exc);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        List list = (List) obj;
        Date date = new Date();
        this.f1290a.O.setLastFlushTime(date);
        this.f1290a.N.a("freeem_reflush", date);
        if (list.size() < 30) {
            this.f1290a.P.setVisibility(8);
        } else {
            this.f1290a.P.setVisibility(0);
        }
        this.f1290a.R.b();
        this.f1290a.R.b((Collection) list);
        this.f1290a.O.setAdapter((ListAdapter) this.f1290a.R);
        this.f1290a.U.j();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.f1290a.O.n();
        this.f1290a.S = null;
    }
}
