package com.immomo.momo.android.activity.contacts;

import com.immomo.momo.android.a.hf;

final class bw implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ bu f1177a;
    private final /* synthetic */ String b;

    bw(bu buVar, String str) {
        this.f1177a = buVar;
        this.b = str;
    }

    public final void run() {
        int a2 = this.f1177a.f1175a.P.a(this.b, hf.f857a);
        if (a2 < 0 || a2 > this.f1177a.f1175a.P.getCount()) {
            this.f1177a.f1175a.R = this.f1177a.f1175a.U.c();
            this.f1177a.f1175a.P.b(this.f1177a.f1175a.R);
        } else {
            ((hf) this.f1177a.f1175a.P.getItem(a2)).g.g = 3;
        }
        this.f1177a.f1175a.P.notifyDataSetChanged();
    }
}
