package com.immomo.momo.android.activity.event;

import android.os.Bundle;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import com.immomo.momo.R;
import com.immomo.momo.android.a.cf;
import com.immomo.momo.android.activity.ah;
import com.immomo.momo.android.view.LoadingButton;
import com.immomo.momo.android.view.MomoRefreshListView;
import com.immomo.momo.android.view.bu;
import com.immomo.momo.g;
import com.immomo.momo.service.r;
import com.immomo.momo.util.k;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class HistoryEventListActivity extends ah implements bu {
    /* access modifiers changed from: private */
    public MomoRefreshListView h = null;
    /* access modifiers changed from: private */
    public cf i = null;
    /* access modifiers changed from: private */
    public LoadingButton j;
    /* access modifiers changed from: private */
    public bc k = null;
    /* access modifiers changed from: private */
    public Date l = null;
    /* access modifiers changed from: private */
    public List m = null;
    /* access modifiers changed from: private */
    public Set n = new HashSet();
    /* access modifiers changed from: private */
    public r o;

    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        super.a(bundle);
        setContentView((int) R.layout.activity_historyevents);
        m().getHeaderSpinner().setVisibility(8);
        this.h = (MomoRefreshListView) findViewById(R.id.listview);
        this.h.setFastScrollEnabled(false);
        this.h.setLastFlushTime(this.g.b("nevents_lasttime_success"));
        this.h.setEnableLoadMoreFoolter(true);
        this.j = this.h.getFooterViewButton();
        this.h.addHeaderView(g.o().inflate((int) R.layout.listitem_blank, (ViewGroup) null));
        this.h.a(g.o().inflate((int) R.layout.include_nearbyevent_listempty, (ViewGroup) null));
        m().setTitleText("历史活动");
        this.j.setOnProcessListener(new ay(this));
        this.h.setOnPullToRefreshListener$42b903f6(this);
        this.h.setOnItemClickListener(new az(this));
        this.h.setOnCancelListener$135502(new ba(this));
        d();
    }

    public final void b_() {
        this.h.setLoadingViewText(R.string.pull_to_refresh_refreshing_label);
        a(new bc(this, this)).execute(new Object[0]);
    }

    /* access modifiers changed from: protected */
    public final void d() {
        this.o = new r();
        this.m = this.o.d();
        this.n.addAll(this.m);
        MomoRefreshListView momoRefreshListView = this.h;
        cf cfVar = new cf(this, this.m, this.h);
        this.i = cfVar;
        momoRefreshListView.setAdapter((ListAdapter) cfVar);
        this.l = this.g.b("nevents_latttime_reflush");
        if (this.m.size() <= 0 || !((Boolean) this.g.b("nefilter_remain", false)).booleanValue()) {
            this.h.k();
        }
        this.h.l();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        new k("PO", "P71113").e();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        new k("PI", "P71113").e();
        this.i.notifyDataSetChanged();
    }
}
