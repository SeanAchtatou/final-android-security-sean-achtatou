package com.immomo.momo.android.activity.group;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import com.immomo.momo.service.bean.a.a;

final class ey implements AdapterView.OnItemClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ SearchGroupActivity f1646a;

    ey(SearchGroupActivity searchGroupActivity) {
        this.f1646a = searchGroupActivity;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        Intent intent = new Intent(this.f1646a.n(), GroupProfileActivity.class);
        intent.putExtra("gid", ((a) this.f1646a.o.getItem(i)).b);
        intent.putExtra("tag", "local");
        this.f1646a.startActivity(intent);
    }
}
