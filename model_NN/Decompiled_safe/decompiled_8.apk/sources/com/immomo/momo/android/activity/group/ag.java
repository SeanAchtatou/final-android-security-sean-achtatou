package com.immomo.momo.android.activity.group;

import android.view.View;

final class ag implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ EditGroupProfileActivity f1531a;

    ag(EditGroupProfileActivity editGroupProfileActivity) {
        this.f1531a = editGroupProfileActivity;
    }

    public final void onClick(View view) {
        if (EditGroupProfileActivity.r(this.f1531a)) {
            this.f1531a.v();
            if (this.f1531a.y) {
                EditGroupProfileActivity.u(this.f1531a);
            } else {
                this.f1531a.finish();
            }
        }
    }
}
