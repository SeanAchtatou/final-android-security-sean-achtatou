package com.immomo.momo.android.service;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.g;
import com.immomo.momo.protocol.a.w;
import com.immomo.momo.service.c;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

final class j extends d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ Initializer f2610a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public j(Initializer initializer, Context context) {
        super(context);
        this.f2610a = initializer;
    }

    /* access modifiers changed from: protected */
    public final Object a(Object... objArr) {
        try {
            Thread.sleep(10000);
        } catch (Exception e) {
        }
        c cVar = new c();
        if (cVar.f()) {
            Collection e2 = cVar.e();
            if (e2.size() > 0) {
                w.a().a(e2, 1);
                ArrayList arrayList = new ArrayList();
                arrayList.addAll(e2);
                cVar.a((List) arrayList);
                return null;
            }
            g.r().a("contact_uploadtime", new Date());
            return null;
        }
        g.r().a("contact_uploadtime", new Date());
        this.b.a((Object) "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
        return null;
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.f2610a.e = true;
        this.f2610a.b();
    }
}
