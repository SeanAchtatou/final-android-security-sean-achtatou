package com.immomo.momo.android.a;

import android.view.View;

final class ag implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ae f714a;
    private final /* synthetic */ int b;

    ag(ae aeVar, int i) {
        this.f714a = aeVar;
        this.b = i;
    }

    public final void onClick(View view) {
        if (this.f714a.f.getOnItemClickListener() != null) {
            this.f714a.f.getOnItemClickListener().onItemClick(this.f714a.f, view, this.b, (long) view.getId());
        }
    }
}
