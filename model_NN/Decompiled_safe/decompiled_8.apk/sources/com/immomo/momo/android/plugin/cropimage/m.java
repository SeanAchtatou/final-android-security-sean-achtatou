package com.immomo.momo.android.plugin.cropimage;

import android.content.ContentValues;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import java.io.File;

public final class m {

    /* renamed from: a  reason: collision with root package name */
    private static final Uri f2592a = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;

    static {
        String.valueOf(Environment.getExternalStorageDirectory().getAbsolutePath()) + "/.tmp";
    }

    public static Uri a(Context context, Bitmap bitmap) {
        if (bitmap == null) {
            return null;
        }
        Uri parse = Uri.parse(MediaStore.Images.Media.insertImage(context.getContentResolver(), bitmap, (String) null, (String) null));
        if (parse != null) {
            ContentValues contentValues = new ContentValues();
            contentValues.put("datetaken", Long.valueOf(System.currentTimeMillis()));
            contentValues.put("date_added", Long.valueOf(System.currentTimeMillis()));
            context.getContentResolver().update(parse, contentValues, null, null);
        }
        bitmap.recycle();
        return parse;
    }

    public static Uri a(Context context, File file) {
        if (file.exists() && file.isFile()) {
            try {
                ContentValues contentValues = new ContentValues();
                contentValues.put("mime_type", "image/jpeg");
                contentValues.put("_data", file.getAbsolutePath());
                contentValues.put("datetaken", Long.valueOf(System.currentTimeMillis()));
                contentValues.put("date_added", Long.valueOf(System.currentTimeMillis()));
                return context.getContentResolver().insert(f2592a, contentValues);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}
