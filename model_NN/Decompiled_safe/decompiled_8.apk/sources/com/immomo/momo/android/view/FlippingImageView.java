package com.immomo.momo.android.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;

public class FlippingImageView extends ImageView {

    /* renamed from: a  reason: collision with root package name */
    private at f2630a = null;
    private boolean b = false;
    private int c = 2;

    public FlippingImageView(Context context) {
        super(context);
    }

    public FlippingImageView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public FlippingImageView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    private void a() {
        if (!this.b && getWidth() > 0 && getVisibility() == 0) {
            this.b = true;
            if (this.f2630a == null) {
                this.f2630a = new at(((float) getWidth()) / 2.0f, ((float) getHeight()) / 2.0f, this.c);
                this.f2630a.setDuration(1000);
                this.f2630a.setInterpolator(new LinearInterpolator());
                this.f2630a.setRepeatCount(-1);
                this.f2630a.setRepeatMode(1);
            }
            startAnimation(this.f2630a);
        }
    }

    private void b() {
        if (this.b) {
            this.b = false;
            setAnimation(null);
            this.f2630a = null;
        }
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        a();
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        b();
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        if (i > 0) {
            a();
        }
    }

    /* access modifiers changed from: protected */
    public void onVisibilityChanged(View view, int i) {
        if (i == 4 || i == 8) {
            b();
        } else {
            a();
        }
    }

    public void setRotationFlags(int i) {
        this.c = i;
        if (this.f2630a != null) {
            this.f2630a.a(this.c);
        }
    }

    public void setVisibility(int i) {
        if (i == 4 || i == 8) {
            b();
        } else {
            a();
        }
        setVisibility(i);
    }
}
