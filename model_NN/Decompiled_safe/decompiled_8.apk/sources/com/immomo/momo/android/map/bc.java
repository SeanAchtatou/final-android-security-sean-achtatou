package com.immomo.momo.android.map;

import com.immomo.momo.android.view.cx;
import com.immomo.momo.g;

final class bc implements cx {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ SelectSiteGoogleActivity f2482a;

    bc(SelectSiteGoogleActivity selectSiteGoogleActivity) {
        this.f2482a = selectSiteGoogleActivity;
    }

    public final void a(int i, int i2) {
        if (!this.f2482a.s) {
            return;
        }
        if (i2 > i) {
            this.f2482a.e.getLayoutParams().height = 1;
            this.f2482a.f.setVisibility(0);
        } else if (i2 < i) {
            this.f2482a.e.getLayoutParams().height = g.a(150.0f);
            this.f2482a.f.setVisibility(8);
        }
    }
}
