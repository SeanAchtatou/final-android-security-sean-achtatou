package com.immomo.momo.android.activity.maintab;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.protocol.a.n;
import com.immomo.momo.service.bean.a.a;
import java.util.ArrayList;
import java.util.List;

final class ao extends d {

    /* renamed from: a  reason: collision with root package name */
    private List f1840a = new ArrayList();
    private /* synthetic */ x c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ao(x xVar, Context context) {
        super(context);
        this.c = xVar;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        return Boolean.valueOf(n.a().a(this.f1840a, this.c.M.S, this.c.M.T, this.c.M.aH, this.c.ao, this.c.S.getCount()));
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
        x.n(this.c);
        this.c.P();
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        super.a(exc);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        if (((Boolean) obj).booleanValue()) {
            this.c.ab.setVisibility(0);
        } else {
            this.c.ab.setVisibility(8);
        }
        if (!this.f1840a.isEmpty()) {
            for (a aVar : this.f1840a) {
                if (this.c.P.get(aVar.b) == null) {
                    this.c.P.put(aVar.b, aVar);
                    this.c.S.a((Object[]) new a[]{aVar});
                } else {
                    this.b.b((Object) "搜索更多有重复");
                }
            }
            this.c.S.notifyDataSetChanged();
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.c.ar();
    }
}
