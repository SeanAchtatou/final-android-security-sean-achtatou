package com.immomo.momo.android.a;

import android.content.Context;
import android.support.v4.b.a;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.android.view.EmoteTextView;
import com.immomo.momo.b;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.util.j;
import java.util.List;

public final class fq extends a {
    private Context d = null;
    private AbsListView e = null;

    public fq(Context context, List list, AbsListView absListView) {
        super(context, list);
        this.d = context;
        this.e = absListView;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.b.a(java.lang.String, boolean):android.graphics.Bitmap
     arg types: [java.lang.String, int]
     candidates:
      com.immomo.momo.b.a(java.lang.String, android.graphics.Bitmap):android.graphics.Bitmap
      com.immomo.momo.b.a(java.lang.String, boolean):android.graphics.Bitmap */
    public final View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            fr frVar = new fr((byte) 0);
            view = LayoutInflater.from(this.d).inflate((int) R.layout.listitem_user, (ViewGroup) null);
            frVar.f825a = (ImageView) view.findViewById(R.id.userlist_item_iv_face);
            frVar.b = (TextView) view.findViewById(R.id.userlist_item_tv_name);
            frVar.c = (TextView) view.findViewById(R.id.userlist_item_tv_age);
            frVar.d = (TextView) view.findViewById(R.id.userlist_item_tv_distance);
            frVar.e = (TextView) view.findViewById(R.id.userlist_tv_time);
            frVar.f = (EmoteTextView) view.findViewById(R.id.userlist_item_tv_sign);
            frVar.i = (ImageView) view.findViewById(R.id.userlist_item_iv_gender);
            frVar.h = view.findViewById(R.id.userlist_item_layout_genderbackgroud);
            frVar.j = (ImageView) view.findViewById(R.id.userlist_item_pic_iv_weibo);
            frVar.k = (ImageView) view.findViewById(R.id.userlist_item_pic_iv_txweibo);
            frVar.p = (ImageView) view.findViewById(R.id.userlist_item_pic_iv_group_role);
            frVar.l = (ImageView) view.findViewById(R.id.userlist_item_pic_iv_renren);
            frVar.m = (ImageView) view.findViewById(R.id.userlist_item_pic_iv_vip);
            frVar.n = (ImageView) view.findViewById(R.id.userlist_item_pic_iv_multipic);
            frVar.o = (ImageView) view.findViewById(R.id.userlist_item_pic_iv_industry);
            frVar.g = (ImageView) view.findViewById(R.id.userlist_item_pic_sign);
            frVar.g.setVisibility(8);
            frVar.q = view.findViewById(R.id.userlist_tv_timedriver);
            view.setTag(R.id.tag_userlist_item, frVar);
        }
        bf bfVar = (bf) getItem(i);
        fr frVar2 = (fr) view.getTag(R.id.tag_userlist_item);
        frVar2.d.setText(bfVar.Z);
        if (bfVar.d() < 0.0f) {
            frVar2.e.setVisibility(8);
            frVar2.q.setVisibility(8);
        } else {
            frVar2.e.setVisibility(0);
            frVar2.q.setVisibility(0);
            frVar2.e.setText(bfVar.aa);
        }
        frVar2.c.setText(new StringBuilder(String.valueOf(bfVar.I)).toString());
        frVar2.b.setText(bfVar.h());
        if (bfVar.b()) {
            frVar2.b.setTextColor(g.c((int) R.color.font_vip_name));
        } else {
            frVar2.b.setTextColor(g.c((int) R.color.text_color));
        }
        frVar2.f.setText("对其隐身");
        if ("F".equals(bfVar.H)) {
            frVar2.h.setBackgroundResource(R.drawable.bg_gender_famal);
            frVar2.i.setImageResource(R.drawable.ic_user_famale);
        } else {
            frVar2.h.setBackgroundResource(R.drawable.bg_gender_male);
            frVar2.i.setImageResource(R.drawable.ic_user_male);
        }
        if (bfVar.j()) {
            frVar2.n.setVisibility(0);
        } else {
            frVar2.n.setVisibility(8);
        }
        if (!a.a((CharSequence) bfVar.M)) {
            frVar2.o.setVisibility(0);
            frVar2.o.setImageBitmap(b.a(bfVar.M, true));
        } else {
            frVar2.o.setVisibility(8);
        }
        if (bfVar.an) {
            frVar2.j.setVisibility(0);
            frVar2.j.setImageResource(bfVar.ap ? R.drawable.ic_userinfo_weibov : R.drawable.ic_userinfo_weibo);
        } else {
            frVar2.j.setVisibility(8);
        }
        if (bfVar.at) {
            frVar2.k.setVisibility(0);
            frVar2.k.setImageResource(bfVar.au ? R.drawable.ic_userinfo_tweibov : R.drawable.ic_userinfo_tweibo);
        } else {
            frVar2.k.setVisibility(8);
        }
        if (bfVar.aJ == 1 || bfVar.aJ == 3) {
            frVar2.p.setVisibility(0);
            frVar2.p.setImageResource(bfVar.aJ == 1 ? R.drawable.ic_userinfo_groupowner : R.drawable.ic_userinfo_group);
        } else {
            frVar2.p.setVisibility(8);
        }
        if (bfVar.ar) {
            frVar2.l.setVisibility(0);
        } else {
            frVar2.l.setVisibility(8);
        }
        if (bfVar.b()) {
            frVar2.m.setVisibility(0);
            if (bfVar.c()) {
                frVar2.m.setImageResource(R.drawable.ic_userinfo_vip_year);
            } else {
                frVar2.m.setImageResource(R.drawable.ic_userinfo_vip);
            }
        } else {
            frVar2.m.setVisibility(8);
        }
        j.a(bfVar, frVar2.f825a, this.e, 3);
        return view;
    }
}
