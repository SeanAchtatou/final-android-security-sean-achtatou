package com.immomo.momo.android.view.a;

import android.content.DialogInterface;

final class bb implements DialogInterface.OnDismissListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ az f2682a;

    bb(az azVar) {
        this.f2682a = azVar;
    }

    public final void onDismiss(DialogInterface dialogInterface) {
        if (this.f2682a.l != null) {
            this.f2682a.l.cancel(true);
        }
        if (this.f2682a.m != null) {
            this.f2682a.m.cancel(true);
        }
    }
}
