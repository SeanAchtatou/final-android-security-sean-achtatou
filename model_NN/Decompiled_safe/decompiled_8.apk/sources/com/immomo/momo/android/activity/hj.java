package com.immomo.momo.android.activity;

import android.content.Intent;
import android.view.View;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.group.GroupProfileActivity;
import com.immomo.momo.util.k;
import org.json.JSONObject;

final class hj implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ OtherProfileActivity f1715a;

    hj(OtherProfileActivity otherProfileActivity) {
        this.f1715a = otherProfileActivity;
    }

    public final void onClick(View view) {
        String str = (String) view.getTag(R.id.tag_item);
        Intent intent = new Intent(this.f1715a, GroupProfileActivity.class);
        intent.putExtra("gid", str);
        intent.putExtra("tag", "internet");
        this.f1715a.startActivity(intent);
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("group_id", str);
            new k("C", "C2103", jSONObject).e();
        } catch (Throwable th) {
        }
    }
}
