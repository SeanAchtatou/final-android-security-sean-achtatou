package com.immomo.momo.android.activity.event;

import android.content.DialogInterface;

final class aq implements DialogInterface.OnCancelListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ap f1360a;

    aq(ap apVar) {
        this.f1360a = apVar;
    }

    public final void onCancel(DialogInterface dialogInterface) {
        this.f1360a.cancel(true);
        this.f1360a.c.finish();
    }
}
