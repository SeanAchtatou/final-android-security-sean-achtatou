package com.immomo.momo.android.activity.tieba;

import android.content.Context;
import android.os.AsyncTask;
import com.immomo.momo.android.c.d;
import com.immomo.momo.protocol.a.v;
import java.util.List;

final class cl extends d {

    /* renamed from: a  reason: collision with root package name */
    private String f2213a;
    private int c;
    private int d;
    private /* synthetic */ TieDetailActivity e;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public cl(TieDetailActivity tieDetailActivity, Context context, String str, int i) {
        super(context);
        this.e = tieDetailActivity;
        if (tieDetailActivity.q != null && !tieDetailActivity.q.isCancelled()) {
            tieDetailActivity.q.cancel(true);
        }
        tieDetailActivity.q = this;
        this.f2213a = str;
        this.c = i;
        this.d = 20;
    }

    private boolean c() {
        if (!this.e.r) {
            return false;
        }
        TieDetailActivity tieDetailActivity = this.e;
        tieDetailActivity.s = tieDetailActivity.s | 16;
        TieDetailActivity.d(this.e);
        return true;
    }

    private List d() {
        this.b.b((Object) "加载评论 - executeTask");
        List a2 = v.a().a(this.f2213a, this.c * 20, this.d);
        this.e.P.lock();
        while (this.e.i == null) {
            try {
                this.e.Q.await();
            } finally {
                this.e.P.unlock();
            }
        }
        return a2;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        return d();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.android.activity.tieba.TieDetailActivity.a(com.immomo.momo.android.activity.tieba.TieDetailActivity, android.os.AsyncTask):void
     arg types: [com.immomo.momo.android.activity.tieba.TieDetailActivity, com.immomo.momo.android.activity.tieba.cl]
     candidates:
      com.immomo.momo.android.activity.tieba.TieDetailActivity.a(com.immomo.momo.android.activity.tieba.TieDetailActivity, int):void
      com.immomo.momo.android.activity.tieba.TieDetailActivity.a(com.immomo.momo.android.activity.tieba.TieDetailActivity, com.immomo.momo.android.activity.tieba.cl):void
      com.immomo.momo.android.activity.tieba.TieDetailActivity.a(com.immomo.momo.android.activity.tieba.TieDetailActivity, com.immomo.momo.android.activity.tieba.cm):void
      com.immomo.momo.android.activity.tieba.TieDetailActivity.a(com.immomo.momo.android.activity.tieba.TieDetailActivity, com.immomo.momo.android.view.au):void
      com.immomo.momo.android.activity.tieba.TieDetailActivity.a(com.immomo.momo.android.activity.tieba.TieDetailActivity, com.immomo.momo.service.bean.c.b):void
      com.immomo.momo.android.activity.tieba.TieDetailActivity.a(com.immomo.momo.android.activity.tieba.TieDetailActivity, java.lang.String):void
      com.immomo.momo.android.activity.tieba.TieDetailActivity.a(com.immomo.momo.android.activity.tieba.TieDetailActivity, java.util.List):void
      com.immomo.momo.android.activity.tieba.TieDetailActivity.a(com.immomo.momo.android.activity.tieba.TieDetailActivity, boolean):void
      com.immomo.momo.android.activity.ah.a(android.os.Bundle, java.lang.String):boolean
      com.immomo.momo.android.activity.ao.a(int, java.lang.String[]):com.immomo.momo.protocol.imjson.c.a
      com.immomo.momo.android.activity.ao.a(com.immomo.momo.android.view.bi, android.view.View$OnClickListener):void
      com.immomo.momo.android.activity.ao.a(java.lang.CharSequence, int):void
      com.immomo.momo.android.activity.ao.a(android.os.Bundle, java.lang.String):boolean
      com.immomo.momo.android.activity.tieba.TieDetailActivity.a(com.immomo.momo.android.activity.tieba.TieDetailActivity, android.os.AsyncTask):void */
    /* access modifiers changed from: protected */
    public final void a() {
        this.b.b((Object) "加载评论 - onPreTask");
        if (!this.e.r) {
            TieDetailActivity.a(this.e, (AsyncTask) this);
        }
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        this.b.c("加载评论 - onTaskError");
        if (this.e.p != null && !this.e.p.isCancelled()) {
            this.e.p.cancel(true);
        }
        this.e.y();
        c();
        super.a(exc);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.android.activity.tieba.TieDetailActivity.a(com.immomo.momo.android.activity.tieba.TieDetailActivity, boolean):void
     arg types: [com.immomo.momo.android.activity.tieba.TieDetailActivity, int]
     candidates:
      com.immomo.momo.android.activity.tieba.TieDetailActivity.a(com.immomo.momo.android.activity.tieba.TieDetailActivity, int):void
      com.immomo.momo.android.activity.tieba.TieDetailActivity.a(com.immomo.momo.android.activity.tieba.TieDetailActivity, android.os.AsyncTask):void
      com.immomo.momo.android.activity.tieba.TieDetailActivity.a(com.immomo.momo.android.activity.tieba.TieDetailActivity, com.immomo.momo.android.activity.tieba.cl):void
      com.immomo.momo.android.activity.tieba.TieDetailActivity.a(com.immomo.momo.android.activity.tieba.TieDetailActivity, com.immomo.momo.android.activity.tieba.cm):void
      com.immomo.momo.android.activity.tieba.TieDetailActivity.a(com.immomo.momo.android.activity.tieba.TieDetailActivity, com.immomo.momo.android.view.au):void
      com.immomo.momo.android.activity.tieba.TieDetailActivity.a(com.immomo.momo.android.activity.tieba.TieDetailActivity, com.immomo.momo.service.bean.c.b):void
      com.immomo.momo.android.activity.tieba.TieDetailActivity.a(com.immomo.momo.android.activity.tieba.TieDetailActivity, java.lang.String):void
      com.immomo.momo.android.activity.tieba.TieDetailActivity.a(com.immomo.momo.android.activity.tieba.TieDetailActivity, java.util.List):void
      com.immomo.momo.android.activity.ah.a(android.os.Bundle, java.lang.String):boolean
      com.immomo.momo.android.activity.ao.a(int, java.lang.String[]):com.immomo.momo.protocol.imjson.c.a
      com.immomo.momo.android.activity.ao.a(com.immomo.momo.android.view.bi, android.view.View$OnClickListener):void
      com.immomo.momo.android.activity.ao.a(java.lang.CharSequence, int):void
      com.immomo.momo.android.activity.ao.a(android.os.Bundle, java.lang.String):boolean
      com.immomo.momo.android.activity.tieba.TieDetailActivity.a(com.immomo.momo.android.activity.tieba.TieDetailActivity, boolean):void */
    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        List list = (List) obj;
        this.b.b((Object) "加载评论 - onTaskSuccess");
        if (list.size() != 0) {
            TieDetailActivity.a(this.e, false);
            this.e.t.put(Integer.valueOf(this.c), list);
            TieDetailActivity tieDetailActivity = this.e;
            int i = this.c;
            tieDetailActivity.a(list);
            this.e.c(this.c);
        } else if (this.e.r) {
            TieDetailActivity.a(this.e, true);
        } else if (this.e.u == 0) {
            this.e.d(0);
            this.e.N.b();
            this.e.N.notifyDataSetChanged();
            TieDetailActivity.a(this.e, true);
        }
        c();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.b.b((Object) "加载评论 - onTaskFinish");
        this.e.q = (cl) null;
        if (!this.e.r) {
            this.e.p();
        }
    }

    /* access modifiers changed from: protected */
    public final void onCancelled() {
        this.b.b((Object) "加载评论 - onCancelled");
        c();
    }
}
