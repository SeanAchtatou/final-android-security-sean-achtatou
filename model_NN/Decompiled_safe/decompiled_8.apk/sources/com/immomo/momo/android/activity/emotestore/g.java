package com.immomo.momo.android.activity.emotestore;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.i;

final class g extends d {

    /* renamed from: a  reason: collision with root package name */
    private v f1320a = null;
    private boolean c = false;
    private boolean d = false;
    private boolean e = false;
    /* access modifiers changed from: private */
    public /* synthetic */ EmotionInviteTypeActivity f;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public g(EmotionInviteTypeActivity emotionInviteTypeActivity, Context context, boolean z, boolean z2, boolean z3) {
        super(context);
        this.f = emotionInviteTypeActivity;
        this.c = z;
        this.d = z2;
        this.e = z3;
        this.f1320a = new v(context);
        this.f1320a.a("请求提交中");
        this.f1320a.setCancelable(true);
        this.f1320a.setOnCancelListener(new h(this));
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ Object a(Object... objArr) {
        return i.a().a(this.c, this.d, this.e);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.f.a(this.f1320a);
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        super.a(exc);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        this.f.b((CharSequence) ((String) obj));
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.f.p();
    }
}
