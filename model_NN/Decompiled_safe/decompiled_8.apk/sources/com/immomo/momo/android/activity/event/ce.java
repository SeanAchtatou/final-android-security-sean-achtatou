package com.immomo.momo.android.activity.event;

import android.content.Context;
import android.content.Intent;
import android.support.v4.b.a;
import com.immomo.momo.android.broadcast.p;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.j;
import com.immomo.momo.service.bean.aa;
import com.immomo.momo.service.bean.u;
import com.immomo.momo.service.r;
import com.immomo.momo.util.ao;
import com.immomo.momo.util.h;
import java.util.ArrayList;

final class ce extends d {

    /* renamed from: a  reason: collision with root package name */
    private boolean f1398a;
    private boolean c;
    private boolean d;
    private boolean e;
    private /* synthetic */ PublishEventFeedActivity f;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ce(PublishEventFeedActivity publishEventFeedActivity, Context context) {
        super(context);
        this.f = publishEventFeedActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.h.a(java.lang.String, java.lang.String, int, boolean):void
     arg types: [java.lang.String, java.lang.String, int, int]
     candidates:
      com.immomo.momo.util.h.a(java.lang.String, android.graphics.Bitmap, int, boolean):java.io.File
      com.immomo.momo.util.h.a(java.lang.String, java.lang.String, int, boolean):void */
    private String c() {
        this.f.f = this.f.q.b(this.f.f.h);
        String a2 = j.a().a(this.f.o, this.f.H, this.f.I, this.c, this.f1398a, this.d, this.e, this.f.f.aH, this.f.f.S, this.f.f.T, this.f.y);
        this.f.o.b = this.f.f;
        this.f.o.c = this.f.f.h;
        if (this.e) {
            this.f.p.a(this.f.o);
            this.f.q.a(this.f.o.h, this.f.f.h);
            Intent intent = new Intent(p.f2359a);
            intent.putExtra("feedid", this.f.o.h);
            intent.putExtra("siteid", this.f.o.e);
            intent.putExtra("userid", this.f.f.h);
            this.f.sendBroadcast(intent);
        }
        r n = this.f.r;
        aa a3 = this.f.o;
        String l = this.f.y;
        Object a4 = n.a(l);
        if (a4 == null) {
            a4 = new ArrayList();
        }
        a4.add(0, a3);
        u c2 = n.c(l);
        if (c2 != null) {
            c2.a(c2.d() + 1);
        }
        try {
            if (!a.a((CharSequence) this.f.o.getLoadImageId())) {
                h.a(this.f.H.getName().substring(0, this.f.H.getName().lastIndexOf(".")), this.f.o.getLoadImageId(), 16, true);
            }
        } catch (Exception e2) {
            this.b.a((Throwable) e2);
        }
        return a2;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        return c();
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.f.o.b(this.f.m.getText().toString().trim());
        this.f1398a = this.f.j.a();
        this.c = this.f.i.a();
        this.d = this.f.k.a();
        this.e = this.f.l.a();
        this.f.E = new v(this.f, "请稍候，正在提交...");
        this.f.E.setOnCancelListener(new cf(this));
        this.f.E.show();
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        String str = (String) obj;
        this.f.setResult(-1);
        if (a.f(str)) {
            ao.b(str);
        }
        this.f.finish();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        if (this.f.E != null && !this.f.isFinishing()) {
            this.f.E.dismiss();
            this.f.E = (v) null;
        }
    }
}
