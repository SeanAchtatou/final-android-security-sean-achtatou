package com.immomo.momo.android.activity.contacts;

import android.content.Intent;
import android.support.v4.b.a;
import com.immomo.momo.android.broadcast.d;
import com.immomo.momo.android.broadcast.j;
import com.immomo.momo.service.bean.bf;

final class bd implements d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ aw f1161a;

    bd(aw awVar) {
        this.f1161a = awVar;
    }

    public final void a(Intent intent) {
        if (intent.getAction().equals(j.f2353a)) {
            String stringExtra = intent.getStringExtra("key_momoid");
            if (!a.a((CharSequence) stringExtra)) {
                bf j = this.f1161a.Z.j(stringExtra);
                if (!this.f1161a.T.contains(j)) {
                    this.f1161a.S.b(0, j);
                }
            }
        } else if (intent.getAction().equals(j.b)) {
            String stringExtra2 = intent.getStringExtra("key_momoid");
            if (!a.a((CharSequence) stringExtra2)) {
                this.f1161a.S.c(new bf(stringExtra2));
            }
        }
    }
}
