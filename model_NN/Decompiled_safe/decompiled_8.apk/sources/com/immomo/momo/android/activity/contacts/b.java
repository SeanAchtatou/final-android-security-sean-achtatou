package com.immomo.momo.android.activity.contacts;

import android.support.v4.b.a;
import android.text.Editable;
import android.text.TextWatcher;
import java.util.Collection;
import java.util.List;

final class b implements TextWatcher {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ a f1157a;

    b(a aVar) {
        this.f1157a = aVar;
    }

    public final void afterTextChanged(Editable editable) {
        if (!this.f1157a.ac) {
            String trim = editable.toString().trim();
            if (a.a((CharSequence) trim)) {
                List unused = this.f1157a.P();
                this.f1157a.O();
                this.f1157a.Z.setVisibility(4);
                this.f1157a.aa.requestFocus();
                return;
            }
            this.f1157a.P.a(false);
            this.f1157a.P.b((Collection) this.f1157a.V.e(trim));
            this.f1157a.O.k();
            this.f1157a.Z.setVisibility(0);
        }
    }

    public final void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public final void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }
}
