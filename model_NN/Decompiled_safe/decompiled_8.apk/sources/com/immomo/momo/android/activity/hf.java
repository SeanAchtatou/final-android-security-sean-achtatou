package com.immomo.momo.android.activity;

import android.content.Intent;
import android.view.View;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.event.UsersEventListActivity;
import com.immomo.momo.android.activity.feed.OtherFeedListActivity;
import com.immomo.momo.android.activity.message.ChatActivity;
import com.immomo.momo.android.activity.tieba.UsersTiebaActivity;
import com.immomo.momo.android.pay.MemberCenterActivity;
import com.immomo.momo.util.k;

final class hf implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ OtherProfileActivity f1712a;

    hf(OtherProfileActivity otherProfileActivity) {
        this.f1712a = otherProfileActivity;
    }

    public final void onClick(View view) {
        switch (view.getId()) {
            case R.id.profile_layout_start_chat /*2131165347*/:
                new k("C", "C2104").e();
                String e = this.f1712a.t == null ? this.f1712a.o : this.f1712a.t.h;
                Intent intent = new Intent(this.f1712a.getApplicationContext(), ChatActivity.class);
                intent.putExtra("remoteUserID", e);
                this.f1712a.startActivity(intent);
                return;
            case R.id.profile_layout_unfollow /*2131165348*/:
                new k("C", "C2106").e();
                OtherProfileActivity.j(this.f1712a);
                return;
            case R.id.profile_layout_follow /*2131165349*/:
                new k("C", "C2105").e();
                OtherProfileActivity.i(this.f1712a);
                return;
            case R.id.profile_layout_report /*2131165350*/:
                new k("C", "C2107").e();
                OtherProfileActivity.k(this.f1712a);
                return;
            case R.id.layout_vip /*2131166297*/:
                this.f1712a.startActivity(new Intent(this.f1712a, MemberCenterActivity.class));
                return;
            case R.id.layout_feed /*2131166331*/:
                new k("C", "C2102").e();
                Intent intent2 = new Intent(this.f1712a, OtherFeedListActivity.class);
                intent2.putExtra("other_momoid", this.f1712a.t.h);
                this.f1712a.startActivity(intent2);
                return;
            case R.id.layout_more_tieba /*2131166344*/:
                Intent intent3 = new Intent(this.f1712a, UsersTiebaActivity.class);
                intent3.putExtra(UsersTiebaActivity.h, this.f1712a.t.h);
                intent3.putExtra(UsersTiebaActivity.i, this.f1712a.t.h());
                this.f1712a.startActivity(intent3);
                return;
            case R.id.layout_join_envent /*2131166348*/:
                Intent intent4 = new Intent(this.f1712a, UsersEventListActivity.class);
                intent4.putExtra(UsersEventListActivity.j, this.f1712a.t.aP);
                intent4.putExtra(UsersEventListActivity.i, this.f1712a.o);
                intent4.putExtra(UsersEventListActivity.h, this.f1712a.t.h());
                this.f1712a.startActivity(intent4);
                return;
            default:
                return;
        }
    }
}
