package com.immomo.momo.android.view;

import android.content.Context;
import android.util.AttributeSet;

public class MEmoteEditeText extends EmoteEditeText {
    public MEmoteEditeText(Context context) {
        this(context, null);
    }

    public MEmoteEditeText(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 16842862);
    }

    public MEmoteEditeText(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public final CharSequence a(CharSequence charSequence) {
        return bp.a(super.a(charSequence));
    }
}
