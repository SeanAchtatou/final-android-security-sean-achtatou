package com.immomo.momo.android.view.photoview;

import android.view.ScaleGestureDetector;

final class l implements ScaleGestureDetector.OnScaleGestureListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ k f2824a;

    l(k kVar) {
        this.f2824a = kVar;
    }

    public final boolean onScale(ScaleGestureDetector scaleGestureDetector) {
        this.f2824a.f2823a.a(scaleGestureDetector.getScaleFactor(), scaleGestureDetector.getFocusX(), scaleGestureDetector.getFocusY());
        return true;
    }

    public final boolean onScaleBegin(ScaleGestureDetector scaleGestureDetector) {
        return true;
    }

    public final void onScaleEnd(ScaleGestureDetector scaleGestureDetector) {
    }
}
