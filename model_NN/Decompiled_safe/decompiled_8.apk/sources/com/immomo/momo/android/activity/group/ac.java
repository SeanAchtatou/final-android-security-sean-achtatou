package com.immomo.momo.android.activity.group;

import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Message;

final class ac extends Handler {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ EditGroupProfileActivity f1528a;

    ac(EditGroupProfileActivity editGroupProfileActivity) {
        this.f1528a = editGroupProfileActivity;
    }

    public final void handleMessage(Message message) {
        switch (message.what) {
            case 11:
                String string = message.getData().getString("guid");
                EditGroupProfileActivity.a(this.f1528a, string, (Bitmap) message.obj);
                break;
        }
        super.handleMessage(message);
    }
}
