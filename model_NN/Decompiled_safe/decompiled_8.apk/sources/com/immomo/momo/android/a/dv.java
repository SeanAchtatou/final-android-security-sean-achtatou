package com.immomo.momo.android.a;

import android.content.Intent;
import android.view.View;
import com.immomo.momo.android.activity.group.GroupProfileActivity;
import com.immomo.momo.service.bean.a.b;

final class dv implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ dn f785a;
    private final /* synthetic */ int b;
    private final /* synthetic */ b c;

    dv(dn dnVar, int i, b bVar) {
        this.f785a = dnVar;
        this.b = i;
        this.c = bVar;
    }

    public final void onClick(View view) {
        if (!this.f785a.e.f() || this.f785a.e.getOnItemClickListener() == null) {
            Intent intent = new Intent();
            intent.setClass(this.f785a.d, GroupProfileActivity.class);
            intent.putExtra("tag", "local");
            intent.putExtra("gid", this.c.c());
            this.f785a.d.startActivity(intent);
            return;
        }
        this.f785a.e.getOnItemClickListener().onItemClick(this.f785a.e, null, this.b, (long) view.getId());
    }
}
