package com.immomo.momo.android.activity.tieba;

import android.content.Intent;
import android.view.View;

final class di implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ TiebaCreateActivity f2237a;

    di(TiebaCreateActivity tiebaCreateActivity) {
        this.f2237a = tiebaCreateActivity;
    }

    public final void onClick(View view) {
        Intent intent = new Intent(this.f2237a, TiebaMemberListActivity.class);
        intent.putExtra("tiebaid", this.f2237a.s);
        this.f2237a.startActivity(intent);
    }
}
