package com.immomo.momo.android.activity.setting;

import android.os.AsyncTask;
import com.immomo.momo.R;
import com.immomo.momo.a.a;
import com.immomo.momo.android.activity.SharePageActivity;
import com.immomo.momo.android.c.r;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.g;
import com.immomo.momo.protocol.a.d;
import java.util.Calendar;

final class x extends AsyncTask {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public /* synthetic */ SettingShareActivity f2147a;

    public x(SettingShareActivity settingShareActivity) {
        this.f2147a = settingShareActivity;
        settingShareActivity.j = new v(settingShareActivity);
        settingShareActivity.j.setCancelable(true);
        settingShareActivity.j.setOnDismissListener(new y(this));
    }

    private String a() {
        try {
            return d.h();
        } catch (a e) {
            this.f2147a.e.a((Throwable) e);
            this.f2147a.b((CharSequence) e.getMessage());
        } catch (Exception e2) {
            this.f2147a.e.a((Throwable) e2);
            this.f2147a.b((int) R.string.errormsg_server);
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object doInBackground(Object... objArr) {
        return a();
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void onPostExecute(Object obj) {
        String str = (String) obj;
        super.onPostExecute(str);
        r rVar = new r(g.c("momoshared_" + this.f2147a.f.h), new z(this, str), 17, null);
        rVar.a((String.valueOf(SharePageActivity.h) + "/" + this.f2147a.f.h + ".jpg?day=" + Calendar.getInstance().get(5)).replaceAll("https", "http"));
        new Thread(rVar).start();
    }

    /* access modifiers changed from: protected */
    public final void onPreExecute() {
        super.onPreExecute();
        this.f2147a.j.a("请求提交中...");
        this.f2147a.j.show();
    }
}
