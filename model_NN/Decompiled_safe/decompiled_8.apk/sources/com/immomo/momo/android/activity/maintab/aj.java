package com.immomo.momo.android.activity.maintab;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import com.immomo.momo.android.activity.group.GroupProfileActivity;
import com.immomo.momo.service.bean.a.a;

final class aj implements AdapterView.OnItemClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ x f1835a;

    aj(x xVar) {
        this.f1835a = xVar;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        x xVar = this.f1835a;
        Intent intent = new Intent(x.H(), GroupProfileActivity.class);
        intent.putExtra("gid", ((a) this.f1835a.S.getItem(i)).b);
        intent.putExtra("tag", "local");
        this.f1835a.a(intent);
    }
}
