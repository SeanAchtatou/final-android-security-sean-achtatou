package com.immomo.momo.android.activity.contacts;

import android.content.DialogInterface;

final class ak implements DialogInterface.OnCancelListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ aj f1144a;

    ak(aj ajVar) {
        this.f1144a = ajVar;
    }

    public final void onCancel(DialogInterface dialogInterface) {
        this.f1144a.cancel(true);
        this.f1144a.c.finish();
    }
}
