package com.immomo.momo.android.service;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import com.immomo.momo.android.b.p;
import com.immomo.momo.android.b.z;
import com.immomo.momo.g;
import com.immomo.momo.service.aq;
import com.immomo.momo.util.m;
import java.util.Timer;
import java.util.TimerTask;

public class LService extends Service {

    /* renamed from: a  reason: collision with root package name */
    private Timer f2600a;
    private TimerTask b;
    /* access modifiers changed from: private */
    public aq c;
    /* access modifiers changed from: private */
    public m d = new m("LService");
    /* access modifiers changed from: private */
    public p e = null;
    /* access modifiers changed from: private */
    public Handler f = new k(this);

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        super.onCreate();
        if (g.q() != null) {
            this.c = new aq();
        }
        this.f2600a = new Timer();
        this.b = new n(this);
        this.f2600a.schedule(this.b, 2000, 180000);
        this.d.a((Object) "on created");
    }

    public void onDestroy() {
        super.onDestroy();
        this.b.cancel();
        this.f2600a.purge();
        z.b(this.e);
        this.d.a((Object) "onDestroy");
    }

    public void onStart(Intent intent, int i) {
        super.onStart(intent, i);
    }

    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }
}
