package com.immomo.momo.android.activity;

import android.content.Context;
import android.content.Intent;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.a.g;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.w;

final class hy extends d {

    /* renamed from: a  reason: collision with root package name */
    private v f1730a = null;
    private String c = PoiTypeDef.All;
    private /* synthetic */ OtherProfileActivity d;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public hy(OtherProfileActivity otherProfileActivity, Context context, String str) {
        super(context);
        this.d = otherProfileActivity;
        this.c = str;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        w.a().d(this.d.t.h, this.c);
        return true;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
        this.f1730a = new v(this.d);
        this.f1730a.a("请求提交中...");
        this.f1730a.setCancelable(true);
        this.f1730a.setOnCancelListener(new hz(this));
        this.f1730a.show();
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        if (exc instanceof g) {
            this.b.a((Throwable) exc);
            this.d.b((CharSequence) "你没有关注对方，不可以进行备注");
            return;
        }
        super.a(exc);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        Boolean bool = (Boolean) obj;
        super.a(bool);
        if (bool.booleanValue()) {
            this.d.t.l = this.c;
            this.d.q.b(this.d.t);
            this.d.C();
            Intent intent = new Intent(com.immomo.momo.android.broadcast.w.f2366a);
            intent.putExtra("momoid", this.d.t.h);
            this.d.sendBroadcast(intent);
            return;
        }
        a("备注修改失败");
    }

    /* access modifiers changed from: protected */
    public final void b() {
        super.b();
        if (this.f1730a != null && this.f1730a.isShowing() && !this.d.isFinishing()) {
            this.f1730a.dismiss();
            this.f1730a = null;
        }
    }
}
