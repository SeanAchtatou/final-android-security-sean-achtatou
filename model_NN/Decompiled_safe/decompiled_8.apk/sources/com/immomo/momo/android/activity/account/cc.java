package com.immomo.momo.android.activity.account;

import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.support.v4.b.a;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.android.view.a.n;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.aj;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.util.ao;
import com.immomo.momo.util.j;
import com.immomo.momo.util.k;
import com.immomo.momo.util.m;
import java.util.List;

public final class cc extends ab implements View.OnClickListener {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public bf f983a = null;
    /* access modifiers changed from: private */
    public m b = null;
    /* access modifiers changed from: private */
    public int c = -1;
    /* access modifiers changed from: private */
    public View d = null;
    /* access modifiers changed from: private */
    public View e = null;
    private View f = null;
    private ImageView g = null;
    /* access modifiers changed from: private */
    public RegisterActivityWithP h = null;
    /* access modifiers changed from: private */
    public List i = null;
    private View[] j;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public cc(bf bfVar, View view, RegisterActivityWithP registerActivityWithP) {
        super(view);
        int i2 = 0;
        this.f983a = bfVar;
        this.b = new m("RegisterStep1");
        this.j = new View[6];
        this.j[0] = a((int) R.id.member_avatar_block0);
        this.j[1] = a((int) R.id.member_avatar_block1);
        this.j[2] = a((int) R.id.member_avatar_block2);
        this.j[3] = a((int) R.id.member_avatar_block3);
        this.j[4] = a((int) R.id.member_avatar_block4);
        this.j[5] = a((int) R.id.member_avatar_block5);
        int round = Math.round(((((float) g.J()) - (g.l().getDimension(R.dimen.pic_item_margin) * 16.0f)) - (g.l().getDimension(R.dimen.welcome_avatar_margin) * 2.0f)) / 6.0f);
        while (true) {
            int i3 = i2;
            if (i3 >= this.j.length) {
                break;
            }
            ViewGroup.LayoutParams layoutParams = this.j[i3].findViewById(R.id.avatar_imageview).getLayoutParams();
            layoutParams.height = round;
            layoutParams.width = round;
            this.j[i3].findViewById(R.id.avatar_imageview).setLayoutParams(layoutParams);
            ((TextView) this.j[i3].findViewById(R.id.userlist_item_pic_tv_distance)).setTextColor(g.c((int) R.color.font_value));
            i2 = i3 + 1;
        }
        this.d = a((int) R.id.rg_btn_takepicture);
        this.e = a((int) R.id.rg_btn_selectimage);
        this.f = a((int) R.id.rg_tv_recommendation);
        this.g = (ImageView) a((int) R.id.rg_iv_userphoto);
        this.d.setOnClickListener(this);
        this.e.setOnClickListener(this);
        this.f.setOnClickListener(this);
        a((int) R.id.rg_layout_nearusers).post(new cd(this));
        if (!a.a((CharSequence) this.f983a.getLoadImageId())) {
            j.a(this.f983a, this.g, (ViewGroup) null, 3);
        }
        this.h = registerActivityWithP;
        registerActivityWithP.c(RegisterActivityWithP.n);
    }

    static /* synthetic */ void c(cc ccVar) {
        if (ccVar.i != null && ccVar.i.size() > 0) {
            for (int i2 = 0; i2 < ccVar.j.length; i2++) {
                if (i2 >= ccVar.i.size()) {
                    ccVar.j[i2].setVisibility(4);
                    return;
                }
                ccVar.j[i2].setVisibility(0);
                ccVar.j[i2].setOnClickListener(new ci());
                ((TextView) ccVar.j[i2].findViewById(R.id.userlist_item_pic_tv_distance)).setText(((bf) ccVar.i.get(i2)).Z);
                j.b((aj) ccVar.i.get(i2), (ImageView) ccVar.j[i2].findViewById(R.id.avatar_imageview), null, 3, true);
            }
        }
    }

    public final void a(Bitmap bitmap) {
        if (bitmap == null) {
            this.g.setImageResource(R.drawable.ic_common_def_header);
        } else {
            this.g.setImageBitmap(bitmap);
        }
    }

    public final boolean a() {
        if (this.f983a.ae != null && this.f983a.ae.length != 0) {
            return true;
        }
        ao.b("请设置头像");
        return false;
    }

    public final void c() {
        if (a()) {
            this.h.u();
        }
    }

    public final void d() {
        super.d();
        this.h.i();
    }

    public final void e() {
        new k("PI", "P126").e();
    }

    public final void f() {
        new k("PO", "P126").e();
    }

    public final void onClick(View view) {
        switch (view.getId()) {
            case R.id.rg_tv_recommendation /*2131166377*/:
                View inflate = g.o().inflate((int) R.layout.common_dialog_confirm_phonenumber, (ViewGroup) null);
                EditText editText = (EditText) inflate.findViewById(R.id.et_confirm_phonenumber);
                n a2 = n.a(this.h, (CharSequence) null, "确认", "取消", new cg(this, editText), (DialogInterface.OnClickListener) null);
                a2.setTitle("填写推荐人");
                a2.setContentView(inflate);
                a2.setCanceledOnTouchOutside(false);
                if (a.f(this.f983a.s)) {
                    editText.setText(this.f983a.s);
                    editText.setSelection(editText.getText().length());
                } else {
                    editText.postDelayed(new ch(this, editText), 100);
                }
                a2.show();
                return;
            case R.id.rg_layout_nearusers /*2131166378*/:
            case R.id.rg_iv_userphoto /*2131166379*/:
            default:
                return;
            case R.id.rg_btn_selectimage /*2131166380*/:
                new k("C", "C12602").e();
                this.h.j();
                this.e.setEnabled(false);
                this.e.postDelayed(new ce(this), 1000);
                return;
            case R.id.rg_btn_takepicture /*2131166381*/:
                new k("C", "C12603").e();
                this.h.k();
                this.d.setEnabled(false);
                this.d.postDelayed(new cf(this), 1000);
                return;
        }
    }
}
