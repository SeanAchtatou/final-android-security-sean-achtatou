package com.immomo.momo.android.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.b.a;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.a.gv;
import com.immomo.momo.android.broadcast.d;
import com.immomo.momo.android.broadcast.f;
import com.immomo.momo.android.broadcast.w;
import com.immomo.momo.android.view.AppIconMomoGridView;
import com.immomo.momo.android.view.EmoteEditeText;
import com.immomo.momo.android.view.EmoteTextView;
import com.immomo.momo.android.view.HeaderLayout;
import com.immomo.momo.android.view.ProfilePullScrollView;
import com.immomo.momo.android.view.UserPhotosView;
import com.immomo.momo.android.view.a.n;
import com.immomo.momo.android.view.a.o;
import com.immomo.momo.android.view.bi;
import com.immomo.momo.android.view.du;
import com.immomo.momo.b;
import com.immomo.momo.g;
import com.immomo.momo.service.ai;
import com.immomo.momo.service.aq;
import com.immomo.momo.service.bean.aj;
import com.immomo.momo.service.bean.am;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.service.y;
import com.immomo.momo.util.e;
import com.immomo.momo.util.j;
import java.util.List;

public class OtherProfileV2Activity extends ah implements du {
    private LinearLayout A;
    private View B;
    private View C;
    private View D;
    private View E;
    private View F;
    private View G;
    private View H;
    private View I;
    private LinearLayout J;
    private LinearLayout K;
    private LinearLayout L;
    private LinearLayout M;
    private LinearLayout N;
    /* access modifiers changed from: private */
    public LinearLayout O;
    private LinearLayout P;
    private LinearLayout Q;
    private LinearLayout R;
    private LinearLayout S;
    private LinearLayout T;
    private LinearLayout U;
    private View V;
    private ImageView W;
    private TextView X;
    private TextView Y;
    private TextView Z;
    private TextView aA;
    private ImageView aB;
    private ImageView aC;
    private View aD;
    /* access modifiers changed from: private */
    public AppIconMomoGridView aE;
    /* access modifiers changed from: private */
    public gv aF;
    private LinearLayout aG;
    private LinearLayout aH;
    private LinearLayout aI;
    private LinearLayout aJ;
    private ProfilePullScrollView aK = null;
    private View aL = null;
    private UserPhotosView aM;
    private UserPhotosView aN;
    /* access modifiers changed from: private */
    public ImageView aO;
    /* access modifiers changed from: private */
    public View aP;
    private ImageView aQ;
    private ImageView aR;
    private ImageView aS;
    private TextView aT;
    private TextView aU;
    private TextView aV;
    private TextView aW;
    private w aX;
    /* access modifiers changed from: private */
    public int aY = 0;
    private boolean aZ = false;
    private TextView aa;
    private TextView ab;
    private TextView ac;
    private TextView ad;
    private TextView ae;
    /* access modifiers changed from: private */
    public TextView af;
    private TextView ag;
    private TextView ah;
    private TextView ai;
    private TextView aj;
    private TextView ak;
    private TextView al;
    private TextView am;
    private TextView an;
    private TextView ao;
    private TextView ap;
    private View aq;
    private TextView ar;
    private TextView as;
    private TextView at;
    private TextView au;
    private TextView av;
    private TextView aw;
    /* access modifiers changed from: private */
    public TextView ax;
    private TextView ay;
    private TextView az;
    /* access modifiers changed from: private */
    public AnimationDrawable ba = null;
    private d bb = new ia(this);
    /* access modifiers changed from: private */
    public int bc = 0;
    /* access modifiers changed from: private */
    public int bd = 0;
    private View.OnClickListener be = new ip(this);
    private View.OnClickListener bf;
    private View.OnClickListener bg;
    Handler h = new Handler();
    /* access modifiers changed from: private */
    public ja i;
    /* access modifiers changed from: private */
    public je j;
    /* access modifiers changed from: private */
    public List k = null;
    /* access modifiers changed from: private */
    public boolean l = false;
    /* access modifiers changed from: private */
    public boolean m = false;
    private String n;
    /* access modifiers changed from: private */
    public String o;
    private HeaderLayout p = null;
    /* access modifiers changed from: private */
    public aq q = null;
    /* access modifiers changed from: private */
    public ai r = null;
    /* access modifiers changed from: private */
    public y s = null;
    /* access modifiers changed from: private */
    public bf t = null;
    private bi u;
    private bi v;
    private LinearLayout w;
    private LinearLayout x;
    private LinearLayout y;
    private LinearLayout z;

    public OtherProfileV2Activity() {
        new is(this);
        this.bf = new it(this);
        this.bg = new iu(this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup, int, boolean, boolean, int):void
     arg types: [com.immomo.momo.service.bean.ab, android.widget.ImageView, ?[OBJECT, ARRAY], int, int, int, int]
     candidates:
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, int, android.view.View, android.view.ViewGroup, boolean, int, boolean):com.immomo.momo.android.c.o
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup, int, boolean, boolean, int):void */
    private void A() {
        if (this.t.ah != null) {
            this.aL.setVisibility(0);
            this.at.setText(this.t.ah.b());
            if (this.t.u != 0) {
                this.ay.setText(String.valueOf(this.t.u));
            }
            if (this.t.ah.d != null) {
                this.au.setText(String.valueOf(this.t.ah.d.f) + "(" + this.t.ah.m + ")");
            } else {
                this.au.setText(this.t.ah.m);
            }
            if (a.a((CharSequence) this.t.ah.getLoadImageId())) {
                this.aC.setVisibility(8);
                return;
            }
            j.a((aj) this.t.ah, this.aC, (ViewGroup) null, 15, true, true, g.a(8.0f));
            this.aC.setVisibility(0);
            return;
        }
        this.aL.setVisibility(8);
    }

    private void B() {
        if (this.t != null) {
            if (!a.a((CharSequence) this.t.L)) {
                this.J.setVisibility(0);
                e.a(this.aj, this.t.L, this);
            } else {
                this.J.setVisibility(8);
            }
            if (!a.a((CharSequence) this.t.F)) {
                this.M.setVisibility(0);
                e.a(this.am, this.t.F, this);
            } else {
                this.M.setVisibility(8);
            }
            if (!a.a((CharSequence) this.t.D)) {
                this.N.setVisibility(0);
                e.a(this.an, this.t.D, this);
            } else {
                this.N.setVisibility(8);
            }
            if (!a.a((CharSequence) this.t.L) || !a.a((CharSequence) this.t.F) || !a.a((CharSequence) this.t.D)) {
                this.B.setVisibility(0);
            } else {
                this.B.setVisibility(8);
            }
            if (this.t.aM > 0 || this.t.aP > 0 || this.t.ah != null || C() || (this.k != null && this.k.size() > 0)) {
                if (this.t.aM > 0) {
                    this.G.setVisibility(0);
                    List list = this.t.aN;
                    if (list != null && list.size() > 0) {
                        this.aJ.removeAllViews();
                        int size = list.size() / 2;
                        int i2 = 0;
                        int i3 = 0;
                        loop0:
                        while (i2 <= size && i2 < 3 && i3 < list.size()) {
                            View inflate = LayoutInflater.from(this).inflate((int) R.layout.listitem_jointieba_v2, (ViewGroup) null);
                            if (i2 == 0) {
                                inflate.setPadding(0, 0, 0, 0);
                            }
                            this.aJ.addView(inflate);
                            int i4 = 0;
                            int i5 = i3;
                            while (i4 < 2) {
                                if (i5 >= list.size()) {
                                    break loop0;
                                }
                                TextView textView = (TextView) inflate.findViewById(i4 == 0 ? R.id.tv_left : R.id.tv_right);
                                if (textView != null) {
                                    textView.setVisibility(0);
                                    textView.setText(((com.immomo.momo.service.bean.c.d) list.get(i5)).b);
                                    textView.setTag(R.id.tag_item, list.get(i5));
                                    textView.setOnClickListener(this.bg);
                                }
                                i4++;
                                i5++;
                            }
                            i2++;
                            i3 = i5;
                        }
                    }
                    this.aw.setText(String.valueOf(this.t.aM));
                    this.I.setVisibility(this.t.aM > 0 ? 0 : 8);
                    this.H.setVisibility(0);
                } else {
                    this.G.setVisibility(8);
                }
                if (this.t.aP > 0) {
                    this.T.setVisibility(0);
                    this.U.setVisibility(0);
                    this.av.setText(String.valueOf(this.t.aP));
                    this.az.setText(this.t.aQ);
                } else {
                    this.U.setVisibility(8);
                }
                A();
                if (C()) {
                    this.T.setVisibility(0);
                    this.aG.setVisibility(0);
                    this.aE.postDelayed(new ix(this), 100);
                } else {
                    this.aG.setVisibility(8);
                }
                y();
                return;
            }
            this.T.setVisibility(8);
        }
    }

    static /* synthetic */ void C(OtherProfileV2Activity otherProfileV2Activity) {
        View inflate = g.o().inflate((int) R.layout.dialog_otherprofile_remark, (ViewGroup) null);
        EmoteEditeText emoteEditeText = (EmoteEditeText) inflate.findViewById(R.id.edittext_remark);
        EmoteTextView emoteTextView = (EmoteTextView) inflate.findViewById(R.id.tv_original_name);
        if (otherProfileV2Activity.t != null) {
            if (!a.a((CharSequence) otherProfileV2Activity.t.l)) {
                emoteEditeText.setText(otherProfileV2Activity.t.l);
                emoteEditeText.setSelection(otherProfileV2Activity.t.l.length());
            }
            emoteTextView.setText("原用户名： " + otherProfileV2Activity.t.i);
            emoteEditeText.requestFocus();
            InputMethodManager inputMethodManager = (InputMethodManager) otherProfileV2Activity.getSystemService("input_method");
            if (!(inputMethodManager == null || emoteEditeText == null)) {
                inputMethodManager.showSoftInput(emoteEditeText, 2);
                inputMethodManager.toggleSoftInput(2, 1);
            }
            emoteEditeText.addTextChangedListener(new com.immomo.momo.util.aq(24));
            n nVar = new n(otherProfileV2Activity);
            nVar.setTitle("修改备注");
            nVar.setContentView(inflate);
            nVar.a();
            nVar.a(0, otherProfileV2Activity.getString(R.string.dialog_btn_confim), new ik(otherProfileV2Activity, emoteEditeText));
            nVar.a(1, otherProfileV2Activity.getString(R.string.dialog_btn_cancel), new il(otherProfileV2Activity, emoteEditeText));
            nVar.show();
        }
    }

    private boolean C() {
        return this.t.ar || this.t.an || this.t.at || (this.t.O != null && this.t.O.size() > 0);
    }

    /* access modifiers changed from: private */
    public void D() {
        if (this.l) {
            this.ae.setText("自己");
        } else if (this.t.P != null) {
            if (this.t.P.equals("none")) {
                this.ae.setText((int) R.string.relation_stanger);
            } else if (this.t.P.equals("follow")) {
                this.ae.setText((int) R.string.relation_follow);
            } else if (this.t.P.equals("fans")) {
                this.ae.setText((int) R.string.relation_fans);
            } else if (this.t.P.equals("both")) {
                this.ae.setText((int) R.string.relation_both);
            }
        }
        if (this.t != null) {
            if (a.a((CharSequence) this.t.P) || this.t.P.equals("none")) {
                this.m = false;
            } else if (this.t.P.equals("fans")) {
                this.m = false;
            } else if (this.t.P.equals("follow")) {
                this.m = true;
            } else if (this.t.P.equals("both")) {
                this.m = true;
            }
        }
        if (this.m) {
            this.y.setVisibility(8);
            this.x.setVisibility(8);
            this.z.setVisibility(8);
            E();
        } else {
            this.y.setVisibility(8);
            this.x.setVisibility(0);
            this.z.setVisibility(0);
        }
        if (this.g == null || !"both".equals(this.t.P) || this.l) {
            this.O.setVisibility(8);
            this.P.setVisibility(8);
            this.Q.setVisibility(8);
            this.R.setVisibility(8);
            return;
        }
        if (this.t.d == null || !this.g.c) {
            this.O.setVisibility(8);
        } else if (!a.a((CharSequence) this.t.o)) {
            this.O.setVisibility(0);
            this.af.setText(this.t.o);
        } else {
            this.O.setVisibility(8);
            b(new jg(this, this, this.t.d));
        }
        if (!this.f.an || a.a((CharSequence) this.t.p)) {
            this.P.setVisibility(8);
        } else {
            this.ag.setText(this.t.p);
            this.P.setVisibility(0);
        }
        if (!this.f.at || a.a((CharSequence) this.t.q)) {
            this.Q.setVisibility(8);
        } else {
            this.ah.setText(this.t.q);
            this.Q.setVisibility(0);
        }
        if (!this.f.ar || a.a((CharSequence) this.t.r)) {
            this.R.setVisibility(8);
            return;
        }
        this.ai.setText(this.t.r);
        this.R.setVisibility(0);
    }

    /* access modifiers changed from: private */
    public void E() {
        if (this.m) {
            this.u.setVisibility(0);
        } else {
            this.u.setVisibility(8);
        }
        if (this.t.b()) {
            this.aV.setText(this.t.h());
            this.p.setTitleText("会员资料");
            return;
        }
        this.p.setTitleText(this.t.h());
    }

    private void F() {
        if (!"notreflsh".equals(this.n)) {
            b(new jh(this, this));
        }
    }

    static /* synthetic */ void a(OtherProfileV2Activity otherProfileV2Activity, int i2) {
        if (otherProfileV2Activity.t != null) {
            if (i2 == 0) {
                if ("none".equals(otherProfileV2Activity.t.P)) {
                    otherProfileV2Activity.t.P = "follow";
                } else if ("fans".equals(otherProfileV2Activity.t.P)) {
                    otherProfileV2Activity.t.P = "both";
                }
                otherProfileV2Activity.q.h(otherProfileV2Activity.t);
                otherProfileV2Activity.f.x++;
                Intent intent = new Intent(com.immomo.momo.android.broadcast.j.f2353a);
                intent.putExtra("key_momoid", otherProfileV2Activity.t.h);
                intent.putExtra("newfollower", otherProfileV2Activity.f.v);
                intent.putExtra("followercount", otherProfileV2Activity.f.w);
                intent.putExtra("total_friends", otherProfileV2Activity.f.x);
                intent.putExtra("relation", otherProfileV2Activity.t.P);
                otherProfileV2Activity.sendBroadcast(intent);
            } else if (i2 == 1) {
                if ("both".equals(otherProfileV2Activity.t.P)) {
                    otherProfileV2Activity.t.P = "fans";
                } else if ("follow".equals(otherProfileV2Activity.t.P)) {
                    otherProfileV2Activity.t.P = "none";
                }
                otherProfileV2Activity.q.i(otherProfileV2Activity.t.h);
                if (otherProfileV2Activity.f.x > 0) {
                    bf bfVar = otherProfileV2Activity.f;
                    bfVar.x--;
                }
                Intent intent2 = new Intent(com.immomo.momo.android.broadcast.j.b);
                intent2.putExtra("key_momoid", otherProfileV2Activity.t.h);
                intent2.putExtra("newfollower", otherProfileV2Activity.f.v);
                intent2.putExtra("followercount", otherProfileV2Activity.f.w);
                intent2.putExtra("total_friends", otherProfileV2Activity.f.x);
                intent2.putExtra("relation", otherProfileV2Activity.t.P);
                otherProfileV2Activity.sendBroadcast(intent2);
            }
            otherProfileV2Activity.q.d(otherProfileV2Activity.f.x, otherProfileV2Activity.f.h);
            otherProfileV2Activity.q.d(otherProfileV2Activity.t.h, otherProfileV2Activity.t.P);
            otherProfileV2Activity.D();
            otherProfileV2Activity.E();
        }
    }

    static /* synthetic */ void a(OtherProfileV2Activity otherProfileV2Activity, EmoteEditeText emoteEditeText) {
        InputMethodManager inputMethodManager = (InputMethodManager) otherProfileV2Activity.getSystemService("input_method");
        if (inputMethodManager != null && emoteEditeText != null) {
            inputMethodManager.hideSoftInputFromWindow(emoteEditeText.getWindowToken(), 0);
        }
    }

    private void c(Bundle bundle) {
        if (bundle == null || !bundle.getBoolean("from_saveinstance", false)) {
            Intent intent = getIntent();
            this.n = intent.getStringExtra("tag");
            this.o = intent.getStringExtra("momoid");
        } else {
            this.o = (String) bundle.get("momoid");
            this.n = (String) bundle.get("tag");
            this.n = this.n == null ? "local" : this.n;
        }
        if (!a.a((CharSequence) this.o)) {
            this.l = this.f != null && this.o.equals(this.f.h);
            if (this.l) {
                this.t = this.f;
                this.q.a(this.f, this.o);
            } else {
                this.t = this.q.b(this.o);
            }
            if (this.t == null) {
                this.t = new bf(this.o);
                this.p.setTitleText(this.t.h);
                this.ac.setText(this.t.h);
            } else if (!a.a((CharSequence) this.t.ag)) {
                this.t.ah = this.r.a(this.t.ag);
            }
            if (this.l) {
                this.A.setVisibility(8);
                this.u.setVisibility(8);
            } else {
                this.A.setVisibility(0);
            }
            x();
            z();
            D();
            w();
        }
    }

    static /* synthetic */ void i(OtherProfileV2Activity otherProfileV2Activity) {
        n nVar = new n(otherProfileV2Activity);
        nVar.setTitle("提示");
        nVar.a();
        nVar.a(g.a((int) R.string.dialog_follow_tip));
        nVar.a(0, (int) R.string.dialog_btn_confim, new im(otherProfileV2Activity));
        nVar.a(1, (int) R.string.dialog_btn_cancel, new in());
        nVar.show();
    }

    static /* synthetic */ void j(OtherProfileV2Activity otherProfileV2Activity) {
        n nVar = new n(otherProfileV2Activity);
        nVar.setTitle("提示");
        nVar.a();
        nVar.a(g.a((int) R.string.dialog_unfollow_tip));
        nVar.a(0, (int) R.string.dialog_btn_confim, new io(otherProfileV2Activity));
        nVar.a(1, (int) R.string.dialog_btn_cancel, new iq());
        nVar.show();
    }

    static /* synthetic */ void k(OtherProfileV2Activity otherProfileV2Activity) {
        o oVar = new o(otherProfileV2Activity, (int) R.array.report_block_items);
        oVar.a(new ie(otherProfileV2Activity));
        oVar.show();
    }

    static /* synthetic */ void n(OtherProfileV2Activity otherProfileV2Activity) {
        if (otherProfileV2Activity.t != null && !a.a((CharSequence) otherProfileV2Activity.t.h)) {
            otherProfileV2Activity.q.p(otherProfileV2Activity.t.h);
            Intent intent = new Intent(com.immomo.momo.android.broadcast.e.f2348a);
            intent.putExtra("key_momoid", otherProfileV2Activity.t.h);
            otherProfileV2Activity.sendBroadcast(intent);
        }
    }

    /* access modifiers changed from: private */
    public void v() {
        if (this.t != null) {
            x();
            z();
            B();
        }
    }

    /* access modifiers changed from: private */
    public void w() {
        int J2 = g.J();
        int i2 = (J2 * 7) / 8;
        ViewGroup.LayoutParams layoutParams = this.D.getLayoutParams();
        layoutParams.width = J2;
        layoutParams.height = i2;
        this.D.setLayoutParams(layoutParams);
        ViewGroup.LayoutParams layoutParams2 = this.aP.getLayoutParams();
        this.aY = Math.round(((float) i2) * 0.2f);
        layoutParams2.width = J2;
        layoutParams2.height = i2;
        this.aP.setLayoutParams(layoutParams2);
        if (!this.aZ) {
            this.aP.scrollTo(0, this.aY);
            this.aZ = true;
        }
        int photoHeight = this.aN.getPhotoHeight();
        if (photoHeight < this.aY) {
            photoHeight = this.aY;
        }
        if (this.t.b()) {
            this.aK.setMaxScroll(photoHeight);
        } else {
            this.aK.setMaxScroll(0);
        }
    }

    static /* synthetic */ void w(OtherProfileV2Activity otherProfileV2Activity) {
        o oVar = new o(otherProfileV2Activity, new String[]{"取消关注", "备注", "拉黑/举报"});
        oVar.setTitle((int) R.string.dialog_title_avatar_long_press);
        oVar.a();
        oVar.a(new ij(otherProfileV2Activity));
        oVar.show();
    }

    private void x() {
        if (this.t != null) {
            if (this.t.b()) {
                this.F.setVisibility(8);
                this.E.setVisibility(8);
                this.e.a((Object) ("-fillVipAvatarBg--------------------------------user.backgroud=" + this.t.af));
                if (!a.a((CharSequence) this.t.af)) {
                    iv ivVar = new iv(this);
                    if (this.aO.getTag(R.id.tag_item_imageid) == null || !this.aO.getTag(R.id.tag_item_imageid).equals(this.t.af)) {
                        com.immomo.momo.android.c.o oVar = new com.immomo.momo.android.c.o(this.t.af, ivVar, 2, null);
                        Bitmap a2 = j.a(this.t.af);
                        this.aO.setTag(R.id.tag_item_imageid, this.t.af);
                        if (a2 != null) {
                            this.aO.setImageBitmap(a2);
                        } else {
                            this.t.setImageLoading(true);
                            oVar.a();
                        }
                    }
                }
                this.aP.setVisibility(0);
                this.D.setVisibility(0);
                this.aN.a(this.t.ae, true, true);
                this.C.setVisibility(0);
                if (this.t.ae == null || this.t.ae.length <= 8) {
                    this.aS.setVisibility(4);
                } else {
                    this.aS.setVisibility(0);
                    this.aS.setBackgroundResource(R.anim.avatar_flip_tip);
                    this.ba = (AnimationDrawable) this.aS.getBackground();
                    this.aS.setBackgroundDrawable(this.ba);
                    this.h.post(new ir(this));
                }
                this.aV.setText(this.t.h());
                if (this.t.H != null) {
                    if (this.t.H.equals("F")) {
                        this.aQ.setImageResource(R.drawable.ic_user_famale2);
                    } else if (this.t.H.equals("M")) {
                        this.aQ.setImageResource(R.drawable.ic_user_male2);
                    }
                }
                this.aU.setText(new StringBuilder(String.valueOf(this.t.I)).toString());
                if (this.t.K != null) {
                    this.aT.setText(this.t.K);
                }
                if (this.t.ac) {
                    this.ap.setVisibility(0);
                    this.ap.setText((int) R.string.user_profile_baned_tip);
                } else {
                    this.ap.setVisibility(8);
                }
                if (this.t.d() < 0.0f) {
                    this.aW.setText(this.t.Z);
                } else {
                    this.aW.setText(String.valueOf(this.t.Z) + (this.t.V ? "(误差大)" : PoiTypeDef.All) + (!a.a(this.t.aa) ? "/" + this.t.aa : PoiTypeDef.All));
                }
            } else {
                this.F.setVisibility(0);
                this.E.setVisibility(0);
                this.C.setVisibility(4);
                this.aP.setVisibility(8);
                this.D.setVisibility(8);
                this.aM.a(this.t.ae, false, false);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.service.y.a(java.lang.String, java.lang.String, boolean):com.immomo.momo.service.bean.a.j
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      com.immomo.momo.service.y.a(int, java.lang.String, java.lang.String):void
      com.immomo.momo.service.y.a(java.lang.String, java.lang.String, int):void
      com.immomo.momo.service.y.a(java.lang.String, boolean, java.lang.String):void
      com.immomo.momo.service.y.a(java.lang.String, java.lang.String, boolean):com.immomo.momo.service.bean.a.j */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup, int, boolean, boolean, int):void
     arg types: [com.immomo.momo.service.bean.a.a, android.widget.ImageView, ?[OBJECT, ARRAY], int, int, int, int]
     candidates:
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, int, android.view.View, android.view.ViewGroup, boolean, int, boolean):com.immomo.momo.android.c.o
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup, int, boolean, boolean, int):void */
    /* access modifiers changed from: private */
    public void y() {
        if (this.k == null || this.k.size() <= 0) {
            this.aH.setVisibility(8);
            return;
        }
        this.T.setVisibility(0);
        this.aH.setVisibility(0);
        this.aA.setText(String.valueOf(this.k.size()));
        this.aI.removeAllViews();
        for (int i2 = 0; i2 < this.k.size(); i2++) {
            View inflate = LayoutInflater.from(this).inflate((int) R.layout.otherprofile_joingroup_item_v2, (ViewGroup) null);
            if (i2 == this.k.size() - 1) {
                inflate.findViewById(R.id.group_line).setVisibility(8);
            }
            this.aI.addView(inflate);
            com.immomo.momo.service.bean.a.a aVar = (com.immomo.momo.service.bean.a.a) this.k.get(i2);
            ((TextView) inflate.findViewById(R.id.tv_groupname)).setText(aVar.c);
            com.immomo.momo.service.bean.a.j a2 = this.s.a(aVar.b, this.o, false);
            if (a2 != null) {
                if (a2.g == 1) {
                    inflate.findViewById(R.id.group_owner).setVisibility(0);
                } else {
                    inflate.findViewById(R.id.group_owner).setVisibility(8);
                }
            }
            inflate.setTag(R.id.tag_item, aVar.b);
            inflate.setOnClickListener(this.bf);
            j.a((aj) aVar, (ImageView) inflate.findViewById(R.id.avatar_imageview), (ViewGroup) null, 3, false, true, g.a(8.0f));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.b.a(java.lang.String, boolean):android.graphics.Bitmap
     arg types: [java.lang.String, int]
     candidates:
      com.immomo.momo.b.a(java.lang.String, android.graphics.Bitmap):android.graphics.Bitmap
      com.immomo.momo.b.a(java.lang.String, boolean):android.graphics.Bitmap */
    /* access modifiers changed from: private */
    public void z() {
        if (this.t != null) {
            this.v.setVisibility(this.f.h.equals(this.t.h) ? 0 : 8);
            if (this.t.b()) {
                this.p.setTitleText("会员资料");
            } else {
                this.p.setTitleText(this.t.h());
            }
            if (this.t.H != null) {
                if (this.t.H.equals("F")) {
                    this.V.setBackgroundResource(R.drawable.bg_gender_famal);
                    this.W.setImageResource(R.drawable.ic_user_famale);
                } else if (this.t.H.equals("M")) {
                    this.V.setBackgroundResource(R.drawable.bg_gender_male);
                    this.W.setImageResource(R.drawable.ic_user_male);
                }
            }
            this.Y.setText(new StringBuilder(String.valueOf(this.t.I)).toString());
            if (this.t.K != null) {
                this.X.setText(this.t.K);
            }
            if (this.t.ac) {
                this.ao.setVisibility(0);
            } else {
                this.ao.setVisibility(8);
            }
            if (this.t.d() < 0.0f) {
                this.aD.setVisibility(8);
                this.aa.setText(this.t.Z);
            } else {
                this.aD.setVisibility(0);
                this.aa.setText(String.valueOf(this.t.Z) + (this.t.V ? "(误差大)" : PoiTypeDef.All));
            }
            if (!a.a((CharSequence) this.t.aa)) {
                this.Z.setText(this.t.aa);
            }
            if (!a.a((CharSequence) this.t.l())) {
                findViewById(R.id.layout_editsign).setVisibility(0);
                e.a(this.ab, this.t.l(), this);
            } else {
                findViewById(R.id.layout_editsign).setVisibility(8);
            }
            if (this.t.h != null) {
                this.ac.setText(this.t.h);
            }
            if (this.t.ay != null) {
                this.ad.setText(a.d(this.t.ay));
            }
            if (!a.a((CharSequence) this.t.N)) {
                e.a(this.ar, this.t.N, this);
            } else {
                this.ar.setText(PoiTypeDef.All);
            }
            if (!a.a((CharSequence) this.t.C)) {
                this.K.setVisibility(0);
                e.a(this.ak, this.t.C, this);
            } else {
                this.K.setVisibility(8);
            }
            if (!a.a((CharSequence) this.t.Q)) {
                this.L.setVisibility(0);
                e.a(this.al, this.t.Q, this);
            } else {
                this.L.setVisibility(8);
            }
            if (!a.a((CharSequence) this.t.M) || !a.a((CharSequence) this.t.N)) {
                this.aq.setVisibility(0);
            } else {
                this.aq.setVisibility(8);
            }
            if (this.t.b()) {
                this.S.setVisibility(0);
                if (this.t.c()) {
                    this.aR.setImageResource(R.drawable.ic_profile_vip_year);
                } else {
                    this.aR.setImageResource(R.drawable.ic_profile_vip);
                }
            } else {
                this.S.setVisibility(8);
            }
            if (!a.a((CharSequence) this.t.M)) {
                am c = b.c(this.t.M);
                this.e.a((Object) ("displayIndustry, getIndustry=" + c));
                if (c != null) {
                    this.aq.setVisibility(0);
                    this.e.a((Object) ("displayIndustry, item.name=" + c.b));
                    this.as.setText(a.a(c.b) ? PoiTypeDef.All : c.b);
                    this.e.a((Object) ("displayIndustry, item.icon=" + c.d));
                    if (a.a((CharSequence) c.d)) {
                        this.aB.setImageBitmap(null);
                    } else {
                        this.aB.setImageBitmap(b.a(c.f2991a, false));
                    }
                } else {
                    this.as.setText(PoiTypeDef.All);
                    this.aB.setImageBitmap(null);
                }
            } else {
                this.aq.setVisibility(8);
            }
            D();
            E();
            y();
            A();
        }
    }

    public final void a(Intent intent, int i2, Bundle bundle, String str) {
        if (intent.getComponent() != null && d.e(intent.getComponent().getClassName())) {
            if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("afromname")) {
                intent.putExtra("afromname", getIntent().getStringExtra("afromname"));
            }
            intent.putExtra("from", l());
        }
        super.a(intent, i2, bundle, str);
    }

    public final void a(Bundle bundle) {
        super.a(bundle);
        setContentView((int) R.layout.activity_otherprofile_v2);
        this.p = (HeaderLayout) findViewById(R.id.layout_header);
        this.v = new bi(this);
        this.v.a((int) R.drawable.ic_topbar_editor);
        this.u = new bi(getApplicationContext());
        this.u.a((int) R.drawable.ic_topbar_arrow_down);
        this.A = (LinearLayout) findViewById(R.id.profile_layout_bottom);
        this.w = (LinearLayout) findViewById(R.id.profile_layout_start_chat);
        this.x = (LinearLayout) findViewById(R.id.profile_layout_follow);
        this.y = (LinearLayout) findViewById(R.id.profile_layout_unfollow);
        this.z = (LinearLayout) findViewById(R.id.profile_layout_report);
        this.O = (LinearLayout) findViewById(R.id.profile_layout_phonesrc);
        this.P = (LinearLayout) findViewById(R.id.profile_layout_weibosrc);
        this.Q = (LinearLayout) findViewById(R.id.profile_layout_tweibosrc);
        this.R = (LinearLayout) findViewById(R.id.profile_layout_renrensrc);
        this.C = findViewById(R.id.layout_vip_userinfo);
        this.F = findViewById(R.id.layout_avatar_normal);
        this.E = findViewById(R.id.profile_layout_sex_sign);
        this.D = findViewById(R.id.layout_avatar_vip);
        this.aM = (UserPhotosView) this.F.findViewById(R.id.normal_photoview);
        this.aN = (UserPhotosView) this.D.findViewById(R.id.vip_photoview);
        this.aD = findViewById(R.id.layout_time_container);
        this.W = (ImageView) findViewById(R.id.profile_iv_gender);
        this.V = findViewById(R.id.profile_sex_background);
        this.Y = (TextView) findViewById(R.id.profile_tv_age);
        this.X = (TextView) findViewById(R.id.profile_tv_constellation);
        this.Z = (TextView) findViewById(R.id.profile_tv_time);
        this.aa = (TextView) findViewById(R.id.profile_tv_distance);
        this.aU = (TextView) findViewById(R.id.vip_profile_tv_age);
        this.aT = (TextView) findViewById(R.id.vip_profile_tv_constellation);
        this.aW = (TextView) findViewById(R.id.vip_profile_tv_timedistance);
        this.aV = (TextView) findViewById(R.id.vip_tv_name);
        this.aQ = (ImageView) findViewById(R.id.vip_profile_iv_gender);
        this.aS = (ImageView) findViewById(R.id.vip_iv_flip_tip);
        this.aO = (ImageView) findViewById(R.id.vip_iv_avatar_bg);
        findViewById(R.id.vip_iv_avatar_bg_cover);
        this.aP = findViewById(R.id.vip_iv_avatar_bglayout);
        this.ab = (TextView) findViewById(R.id.profile_tv_sign);
        this.ac = (TextView) findViewById(R.id.profile_tv_momoid);
        this.ad = (TextView) findViewById(R.id.profile_tv_regist_time);
        this.ae = (TextView) findViewById(R.id.profile_tv_relation);
        this.af = (TextView) findViewById(R.id.profile_tv_phonesource);
        this.ag = (TextView) findViewById(R.id.profile_tv_weibosource);
        this.ah = (TextView) findViewById(R.id.profile_tv_tweibosource);
        this.ai = (TextView) findViewById(R.id.profile_tv_renrensource);
        this.aq = findViewById(R.id.layout_industry);
        this.ar = (TextView) findViewById(R.id.tv_industry_job);
        this.as = (TextView) findViewById(R.id.tv_industry);
        this.aB = (ImageView) findViewById(R.id.icon_industry);
        this.ao = (TextView) findViewById(R.id.profile_baned_tip);
        this.ap = (TextView) findViewById(R.id.tv_editavatar_tip);
        this.K = (LinearLayout) findViewById(R.id.layout_company);
        this.S = (LinearLayout) findViewById(R.id.layout_vip);
        this.aR = (ImageView) findViewById(R.id.icon_vip);
        this.ak = (TextView) findViewById(R.id.profile_tv_company);
        this.L = (LinearLayout) findViewById(R.id.layout_school);
        this.al = (TextView) findViewById(R.id.profile_tv_school);
        this.aK = (ProfilePullScrollView) findViewById(R.id.scrollview_content);
        this.aL = findViewById(R.id.layout_feed);
        this.ay = (TextView) findViewById(R.id.txt_join_feed_count);
        this.at = (TextView) findViewById(R.id.tv_feeddes);
        this.au = (TextView) findViewById(R.id.tv_placedistance);
        this.aC = (ImageView) findViewById(R.id.iv_feedimg);
        this.B = findViewById(R.id.layout_self_introduction);
        this.J = (LinearLayout) findViewById(R.id.layout_interest);
        this.aj = (TextView) findViewById(R.id.profile_tv_interest);
        this.I = findViewById(R.id.layout_moretieba_container);
        this.M = (LinearLayout) findViewById(R.id.layout_hangout);
        this.N = (LinearLayout) findViewById(R.id.layout_website);
        this.am = (TextView) findViewById(R.id.profile_tv_hangout);
        this.an = (TextView) findViewById(R.id.profile_tv_web);
        this.aG = (LinearLayout) findViewById(R.id.profile_layout_bind_info);
        this.aH = (LinearLayout) findViewById(R.id.layout_join_group);
        this.aI = (LinearLayout) findViewById(R.id.group_container);
        this.T = (LinearLayout) findViewById(R.id.profile_layout_joins);
        this.U = (LinearLayout) findViewById(R.id.layout_join_envent);
        this.G = findViewById(R.id.profile_layout_jointieba);
        this.H = findViewById(R.id.layout_moretieba_container);
        this.av = (TextView) findViewById(R.id.txt_join_event_count);
        this.aw = (TextView) findViewById(R.id.txt_join_tieba_count);
        this.ax = (TextView) findViewById(R.id.txt_join_bind_count);
        this.az = (TextView) findViewById(R.id.txt_join_eventdesc);
        this.aA = (TextView) findViewById(R.id.txt_join_group_count);
        this.aJ = (LinearLayout) findViewById(R.id.tieba_container);
        this.q = new aq();
        this.s = new y();
        this.r = new ai();
        this.aX = new w(this);
        this.aX.a(this.bb);
        this.aX.a(f.f2349a);
        this.aX.a(this.bb);
        c(bundle);
    }

    /* access modifiers changed from: protected */
    public final void d() {
    }

    /* access modifiers changed from: protected */
    public final void e() {
        super.e();
        this.k = this.s.d(this.o);
        this.aE = (AppIconMomoGridView) findViewById(R.id.layout_app_icon);
        this.aF = new gv(this);
        if (this.t != null && this.t.h.equals(this.f.h)) {
            this.p.a(this.v, new iy(this));
        }
        this.p.a(this.u, new iz(this));
        this.w.setOnClickListener(this.be);
        this.x.setOnClickListener(this.be);
        this.y.setOnClickListener(this.be);
        this.z.setOnClickListener(this.be);
        this.S.setOnClickListener(this.be);
        findViewById(R.id.layout_feed).setOnClickListener(this.be);
        this.U.setOnClickListener(this.be);
        this.aN.setPageSelectedListener(this);
        findViewById(R.id.layout_more_tieba).setOnClickListener(this.be);
        ib ibVar = new ib();
        ibVar.a(false);
        this.aM.setAvatarClickListener(ibVar);
        this.aN.setAvatarClickListener(ibVar);
        this.aE.setOnMomoGridViewItemClickListener(new ic(this));
        this.aK.setOnPullScrollChangedListener(new id(this));
        B();
        F();
    }

    public void onBindIconClicked(View view) {
        b(view.getId());
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (this.i != null && !this.i.isCancelled()) {
            this.i.cancel(true);
        }
        if (this.j != null && !this.j.isCancelled()) {
            this.j.cancel(true);
        }
        if (this.aX != null) {
            unregisterReceiver(this.aX);
            this.aX = null;
        }
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        String str = intent.getExtras() != null ? (String) intent.getExtras().get("momoid") : null;
        if (!a.a((CharSequence) str) && !this.o.equals(str)) {
            Bundle bundle = new Bundle();
            bundle.putBoolean("from_saveinstance", true);
            bundle.putString("momoid", str);
            c(bundle);
            if (r()) {
                v();
                this.aK.d();
                F();
                return;
            }
            e();
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        bundle.putBoolean("from_saveinstance", true);
        bundle.putString("momoid", this.o);
        bundle.putString("tag", this.n);
        super.onSaveInstanceState(bundle);
    }

    public final void u() {
        if (this.ba != null && this.t.b() && this.ba.isVisible() && this.ba.isRunning()) {
            this.ba.stop();
            this.aS.setVisibility(4);
        }
    }
}
