package com.immomo.momo.android.activity.discuss;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import java.util.List;

final class h extends d {

    /* renamed from: a  reason: collision with root package name */
    private v f1234a = null;
    /* access modifiers changed from: private */
    public /* synthetic */ DiscussMemberListActivity c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public h(DiscussMemberListActivity discussMemberListActivity, Context context) {
        super(context);
        this.c = discussMemberListActivity;
        this.f1234a = new v(context);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        List b = com.immomo.momo.protocol.a.h.a().b(this.c.o);
        if (b != null && b.size() > 0) {
            this.c.l.a(b, this.c.o);
            this.c.s.post(new j(this));
        }
        return b;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
        this.f1234a.setCancelable(true);
        this.f1234a.setOnCancelListener(new i(this));
        this.f1234a.a("正在获取最新成员列表...");
        this.f1234a.show();
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        super.a(exc);
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void a(Object obj) {
        List list = (List) obj;
        if (list != null) {
            this.c.a(list);
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
        if (this.f1234a != null) {
            this.f1234a.dismiss();
            this.f1234a = null;
        }
    }
}
