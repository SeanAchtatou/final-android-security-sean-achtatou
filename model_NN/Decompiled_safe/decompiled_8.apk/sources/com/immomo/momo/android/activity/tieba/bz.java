package com.immomo.momo.android.activity.tieba;

import com.immomo.momo.android.view.a.al;
import com.immomo.momo.service.bean.c.b;
import mm.purchasesdk.PurchaseCode;

final class bz implements al {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ TieDetailActivity f2200a;
    private final /* synthetic */ String[] b;
    private final /* synthetic */ b c;

    bz(TieDetailActivity tieDetailActivity, String[] strArr, b bVar) {
        this.f2200a = tieDetailActivity;
        this.b = strArr;
        this.c = bVar;
    }

    public final void a(int i) {
        if ("评论".equals(this.b[i])) {
            this.f2200a.M.performClick();
        } else if ("分享给好友".equals(this.b[i])) {
            TieDetailActivity.k(this.f2200a, this.c);
        } else if ("分享到社交网络".equals(this.b[i])) {
            TieDetailActivity.l(this.f2200a, this.c);
        } else if ("举报".equals(this.b[i])) {
            TieDetailActivity.m(this.f2200a, this.c);
        } else if ("编辑话题".equals(this.b[i])) {
            TieDetailActivity.n(this.f2200a, this.c);
        } else if ("删除话题".equals(this.b[i])) {
            TieDetailActivity.a(this.f2200a, this.c, 108);
        } else if ("删除并禁言".equals(this.b[i])) {
            TieDetailActivity.a(this.f2200a, this.c, 109);
        } else if ("置顶话题".equals(this.b[i])) {
            this.f2200a.b(new cp(this.f2200a, this.f2200a, PurchaseCode.ORDER_OK, this.c, null));
        } else if ("取消置顶".equals(this.b[i])) {
            this.f2200a.b(new cp(this.f2200a, this.f2200a, PurchaseCode.UNSUB_OK, this.c, null));
        } else if ("设为精华".equals(this.b[i])) {
            this.f2200a.b(new cp(this.f2200a, this.f2200a, PurchaseCode.AUTH_OK, this.c, null));
        } else if ("取消精华".equals(this.b[i])) {
            this.f2200a.b(new cp(this.f2200a, this.f2200a, 105, this.c, null));
        } else if ("锁定话题".equals(this.b[i])) {
            this.f2200a.b(new cp(this.f2200a, this.f2200a, 106, this.c, null));
        } else if ("取消锁定".equals(this.b[i])) {
            this.f2200a.b(new cp(this.f2200a, this.f2200a, 107, this.c, null));
        }
    }
}
