package com.immomo.momo.android.activity.maintab;

import android.content.Intent;
import android.support.v4.b.a;
import com.immomo.momo.android.broadcast.d;
import com.immomo.momo.android.broadcast.f;
import com.immomo.momo.android.broadcast.p;
import com.immomo.momo.android.broadcast.w;
import com.immomo.momo.android.broadcast.z;
import com.immomo.momo.service.ai;
import com.immomo.momo.service.bean.ab;

final class cg implements d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ cd f1879a;

    cg(cd cdVar) {
        this.f1879a = cdVar;
    }

    public final void a(Intent intent) {
        ab a2;
        if (w.f2366a.equals(intent.getAction())) {
            String str = intent.getExtras() != null ? (String) intent.getExtras().get("momoid") : null;
            if (!a.a((CharSequence) str) && this.f1879a.M.h.equals(str)) {
                this.f1879a.aj();
                this.f1879a.ao();
                this.f1879a.ai();
            }
        } else if (p.f2359a.equals(intent.getAction())) {
            String str2 = intent.getExtras() != null ? (String) intent.getExtras().get("feedid") : null;
            String str3 = intent.getExtras() != null ? (String) intent.getExtras().get("userid") : null;
            if (!a.a((CharSequence) str2) && this.f1879a.M.h.equals(str3) && (a2 = new ai().a(str2)) != null) {
                this.f1879a.M.ah = a2;
            }
            this.f1879a.am();
        } else if (f.f2349a.equals(intent.getAction())) {
            String str4 = intent.getExtras() != null ? (String) intent.getExtras().get("userid") : null;
            if (this.f1879a.M.h.equals(str4)) {
                this.f1879a.M.ah = new ai().b(str4);
            }
            this.f1879a.am();
        } else if (z.f2368a.equals(intent.getAction()) || z.b.equals(intent.getAction())) {
            this.f1879a.Q();
        }
    }
}
