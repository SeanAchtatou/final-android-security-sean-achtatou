package com.immomo.momo.android.activity.message;

import android.os.Message;
import com.immomo.momo.android.activity.fs;
import com.immomo.momo.android.c.g;
import com.immomo.momo.service.bean.bh;

final class bc implements fs, g {

    /* renamed from: a  reason: collision with root package name */
    private bh f1936a;
    private /* synthetic */ ChatBGSettingActivity b;

    public bc(ChatBGSettingActivity chatBGSettingActivity, bh bhVar) {
        this.b = chatBGSettingActivity;
        this.f1936a = bhVar;
    }

    public final void a(int i, long j, long j2, long j3) {
        this.b.e.b((Object) ("@@@@@@@@@@@@@@ LoadLargeWallpaperCallback status:" + i + "  max:" + j + "  process:" + j2 + "  nowStamp:" + j3));
        this.f1936a.c = j;
        this.f1936a.b = j2;
        Message message = new Message();
        if (i == 0) {
            message.what = 20;
            this.b.t.sendMessageDelayed(message, 100);
        } else if (i == -1) {
            this.f1936a.setImageLoading(false);
            this.f1936a.setImageLoadFailed(true);
            message.what = 21;
            this.b.t.sendMessage(message);
        }
    }

    public final /* synthetic */ void a(Object obj) {
        this.f1936a.setImageLoading(false);
        this.f1936a.setImageLoadFailed(false);
        this.b.t.sendEmptyMessage(22);
    }
}
