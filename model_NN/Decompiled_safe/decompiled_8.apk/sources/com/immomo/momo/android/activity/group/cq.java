package com.immomo.momo.android.activity.group;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.b.a;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.g;
import com.immomo.momo.protocol.a.n;

final class cq extends d {

    /* renamed from: a  reason: collision with root package name */
    private v f1590a;
    private /* synthetic */ GroupPartyActivity c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public cq(GroupPartyActivity groupPartyActivity, Context context) {
        super(context);
        this.c = groupPartyActivity;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        return n.a().d(this.c.u);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.f1590a = new v(this.c);
        this.f1590a.a("请求提交中");
        this.f1590a.setCancelable(true);
        this.f1590a.setOnCancelListener(new cr(this));
        this.f1590a.show();
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        String str = (String) obj;
        if (!a.a((CharSequence) str)) {
            a(str);
        }
        this.c.t.o = 0;
        this.c.t.i = 3;
        new com.immomo.momo.service.v().f(this.c.t.f2976a);
        this.c.w.a(this.c.t);
        Bundle bundle = new Bundle();
        bundle.putString("groupfeedid", this.c.t.f2976a);
        g.d().a(bundle, "actions.groupfeeddelete");
        this.c.finish();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        if (this.f1590a != null) {
            this.f1590a.dismiss();
        }
    }
}
