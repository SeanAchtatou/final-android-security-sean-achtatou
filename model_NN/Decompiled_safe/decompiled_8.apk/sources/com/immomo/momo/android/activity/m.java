package com.immomo.momo.android.activity;

import android.content.Context;
import android.content.Intent;
import com.immomo.momo.R;
import com.immomo.momo.a.h;
import com.immomo.momo.android.activity.group.GroupProfileActivity;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.n;
import com.immomo.momo.service.bean.a.a;
import com.immomo.momo.service.y;

final class m extends d {

    /* renamed from: a  reason: collision with root package name */
    private v f1826a = null;
    private /* synthetic */ AddGroupActivity c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public m(AddGroupActivity addGroupActivity, Context context) {
        super(context);
        this.c = addGroupActivity;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        this.c.O = new a(((String[]) objArr)[0]);
        n.a().a(this.c.O.b, this.c.O);
        return "yes";
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
        this.f1826a = new v(this.c.c());
        this.f1826a.setCancelable(true);
        this.f1826a.a("正在查找,请稍候...");
        this.f1826a.setOnCancelListener(new n(this));
        this.f1826a.show();
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        if (exc instanceof h) {
            this.c.e((int) R.string.find_no_gid);
            this.b.a((Throwable) exc);
            return;
        }
        super.a(exc);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.service.y.a(com.immomo.momo.service.bean.a.a, boolean):void
     arg types: [com.immomo.momo.service.bean.a.a, int]
     candidates:
      com.immomo.momo.service.y.a(com.immomo.momo.service.bean.a.a, java.lang.String):void
      com.immomo.momo.service.y.a(java.lang.String, int):void
      com.immomo.momo.service.y.a(java.lang.String, java.lang.String):void
      com.immomo.momo.service.y.a(java.lang.String, java.util.List):void
      com.immomo.momo.service.y.a(java.lang.String, boolean):void
      com.immomo.momo.service.y.a(java.lang.String, java.lang.String[]):void
      com.immomo.momo.service.y.a(java.util.List, java.lang.String):void
      com.immomo.momo.service.y.a(com.immomo.momo.service.bean.a.a, boolean):void */
    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        if (((String) obj).equals("yes")) {
            new y().a(this.c.O, false);
            Intent intent = new Intent(this.c.c(), GroupProfileActivity.class);
            intent.putExtra("gid", this.c.O.b);
            this.c.a(intent);
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
        super.b();
        if (this.f1826a != null && !this.c.c().isFinishing()) {
            this.f1826a.dismiss();
            this.f1826a = null;
        }
    }
}
