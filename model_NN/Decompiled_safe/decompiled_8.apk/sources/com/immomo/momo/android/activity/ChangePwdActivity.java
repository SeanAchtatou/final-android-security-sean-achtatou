package com.immomo.momo.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.b.a;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.retrieve.RetrievePWDActivity;
import com.immomo.momo.android.view.HeaderLayout;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.g;
import com.immomo.momo.util.ao;

public class ChangePwdActivity extends ah implements View.OnClickListener {
    private TextView h = null;
    /* access modifiers changed from: private */
    public EditText i = null;
    /* access modifiers changed from: private */
    public EditText j = null;
    private EditText k = null;
    private Button l = null;
    private Button m = null;
    private HeaderLayout n = null;
    /* access modifiers changed from: private */
    public v o = null;

    private static boolean a(EditText editText) {
        String trim = editText.getText().toString().trim();
        return trim == null || trim.length() <= 0;
    }

    /* access modifiers changed from: private */
    public void u() {
        boolean z = true;
        if (a(this.i)) {
            ao.e(R.string.updatepwd_oldpwd_empty);
            this.i.requestFocus();
            z = false;
        } else if (a(this.j)) {
            ao.e(R.string.updatepwd_newpwd_empty);
            this.j.requestFocus();
            z = false;
        } else if (a(this.k)) {
            ao.e(R.string.updatepwd_newpwd_confim_empty);
            this.k.requestFocus();
            z = false;
        } else {
            String trim = this.i.getText().toString().trim();
            this.e.a((Object) ("pwd.length=" + trim.length()));
            if (trim.length() < 4) {
                ao.b(String.format(g.a((int) R.string.updatepwd_oldpwd_sizemin), 4));
                this.i.requestFocus();
                this.i.selectAll();
                z = false;
            } else {
                String trim2 = this.j.getText().toString().trim();
                if (trim2.length() < 6) {
                    ao.b(String.format(g.a((int) R.string.updatepwd_newpwd_sizemin), 6));
                    this.j.requestFocus();
                    this.j.selectAll();
                    z = false;
                } else if (trim2.length() > 16) {
                    ao.b(String.format(g.a((int) R.string.updatepwd_newpwd_sizemax), 16));
                    this.j.requestFocus();
                    this.j.selectAll();
                    z = false;
                } else {
                    String trim3 = this.k.getText().toString().trim();
                    if (trim3 == null || !trim3.equals(trim2)) {
                        ao.e(R.string.updatepwd_newpwd_confim_notmather);
                        this.k.requestFocus();
                        this.k.selectAll();
                        z = false;
                    } else if (a.g(trim2)) {
                        ao.e(R.string.reg_pwd_isweak);
                        this.k.setText(PoiTypeDef.All);
                        this.j.requestFocus();
                        this.j.selectAll();
                        z = false;
                    } else if (trim.equals(trim2)) {
                        ao.e(R.string.changepwd_oldpwd_newpwd_equals);
                        this.j.setText(PoiTypeDef.All);
                        this.k.setText(PoiTypeDef.All);
                        this.j.requestFocus();
                        z = false;
                    }
                }
            }
        }
        if (z) {
            new bf(this, this).execute(new String[0]);
        }
    }

    public final void a(Bundle bundle) {
        super.a(bundle);
        setContentView((int) R.layout.activity_setpwd);
        this.h = (TextView) findViewById(R.id.login_tv_forgotpassword);
        this.h.getPaint().setFlags(8);
        this.h.getPaint().setAntiAlias(true);
        this.i = (EditText) findViewById(R.id.setpwd_et_pwd_old);
        this.j = (EditText) findViewById(R.id.setpwd_et_pwd_new);
        this.k = (EditText) findViewById(R.id.setpwd_et_pwd_new_confim);
        this.l = (Button) findViewById(R.id.btn_back);
        this.m = (Button) findViewById(R.id.btn_ok);
        this.n = (HeaderLayout) findViewById(R.id.layout_header);
        this.n.setTitleText((int) R.string.changepwd);
        this.k.setOnEditorActionListener(new be(this));
        this.l.setOnClickListener(this);
        this.m.setOnClickListener(this);
        this.h.setOnClickListener(this);
    }

    /* access modifiers changed from: protected */
    public final void d() {
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        finish();
        super.onActivityResult(i2, i3, intent);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_back /*2131165286*/:
                finish();
                return;
            case R.id.btn_ok /*2131165289*/:
                u();
                return;
            case R.id.login_tv_forgotpassword /*2131165356*/:
                Intent intent = new Intent(this, RetrievePWDActivity.class);
                if (this.f.aw) {
                    intent.putExtra("isbindemail", true);
                    intent.putExtra("email", this.f.G);
                }
                if (this.f.g) {
                    intent.putExtra("phonenumber", this.f.b);
                    intent.putExtra("areacode", this.f.c);
                }
                intent.putExtra("islogin", true);
                startActivityForResult(intent, 66);
                return;
            default:
                return;
        }
    }
}
