package com.immomo.momo.android.c;

import android.content.Context;
import android.content.Intent;
import com.immomo.momo.R;
import com.immomo.momo.a.a;
import com.immomo.momo.android.broadcast.e;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.w;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.util.ao;
import java.util.Date;

public final class ae extends x {
    private v g = new v(this.f2396a);

    public ae(Context context, bf bfVar, bf bfVar2, z zVar) {
        super(context, bfVar, bfVar2, zVar);
        this.g.setCancelable(true);
        this.g.setOnCancelListener(new af(this));
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        w.a().b(this.c.h, ((String[]) objArr)[0]);
        return true;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
        if (this.g != null) {
            this.g.a("请求提交中");
            this.g.show();
        }
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        if (exc instanceof a) {
            this.e.a((Throwable) exc);
            ao.b(exc.getMessage());
            return;
        }
        this.e.a((Throwable) exc);
        ao.g(R.string.report_result_failed);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        bf b;
        Boolean bool = (Boolean) obj;
        super.a(bool);
        if (bool.booleanValue()) {
            ao.g(R.string.report_result_success);
            if (!this.c.h.equals("10000") && (b = this.d.b(this.c.h)) != null) {
                b.P = "none";
                this.c.P = "none";
                b.Y = new Date();
                this.d.k(b);
                c();
                d();
                if (this.f != null) {
                    this.f.a();
                }
                Intent intent = new Intent(e.b);
                intent.putExtra("key_momoid", this.c.h);
                this.f2396a.sendBroadcast(intent);
                return;
            }
            return;
        }
        ao.g(R.string.report_result_failed);
    }

    /* access modifiers changed from: protected */
    public final void b() {
        super.b();
        if (this.g != null) {
            this.g.dismiss();
            this.g = null;
        }
    }
}
