package com.immomo.momo.android.b;

import android.location.Location;
import com.immomo.momo.android.c.g;
import java.util.HashMap;
import java.util.Map;

public final class m extends p {

    /* renamed from: a  reason: collision with root package name */
    private static Map f2331a = new HashMap();
    private String c = null;
    private g d = null;
    private Object e = new Object();

    private m(String str) {
        this.c = str;
    }

    public static m a(String str) {
        m mVar = (m) f2331a.get(str);
        if (mVar != null) {
            return mVar;
        }
        m mVar2 = new m(str);
        f2331a.put(str, mVar2);
        return mVar2;
    }

    public final void a(Location location, int i, int i2, int i3) {
        synchronized (this.e) {
            n nVar = new n(location);
            if (this.d != null) {
                this.d.a(nVar);
            }
            f2331a.remove(this.c);
        }
    }

    public final void a(g gVar) {
        synchronized (this.e) {
            this.d = gVar;
        }
    }
}
