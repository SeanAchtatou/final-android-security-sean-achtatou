package com.immomo.momo.android.pay;

import android.content.Context;
import com.immomo.momo.a;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.util.h;
import java.io.File;
import java.util.List;

final class ah extends d {

    /* renamed from: a  reason: collision with root package name */
    private v f2527a = null;
    private /* synthetic */ MemberCenterActivity c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ah(MemberCenterActivity memberCenterActivity, Context context) {
        super(context);
        this.c = memberCenterActivity;
        this.f2527a = new v(context);
        this.f2527a.a("数据加载中");
        this.f2527a.setCancelable(true);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        File u = a.u();
        String a2 = com.immomo.momo.protocol.a.a.a().a(this.c.q);
        com.immomo.momo.protocol.a.a.a();
        ai a3 = com.immomo.momo.protocol.a.a.a(a2);
        if (this.c.q != null && a3.f2528a <= this.c.q.f2528a) {
            return null;
        }
        h.a(new File(u, "membercenter_file"), a2);
        this.c.q = a3;
        return this.c.q.c;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
        if (this.c.f.b()) {
            return;
        }
        if (this.c.p == null || this.c.p.getCount() == 0) {
            this.c.a(this.f2527a);
        }
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        List list = (List) obj;
        super.a(list);
        if (list != null) {
            this.c.u();
            this.c.p.a(list);
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
        super.b();
        this.c.p();
    }
}
