package com.immomo.momo.android.activity.message;

import android.content.Intent;
import android.support.v4.b.a;
import com.immomo.momo.android.broadcast.d;

final class aj implements d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ChatActivity f1917a;

    aj(ChatActivity chatActivity) {
        this.f1917a = chatActivity;
    }

    public final void a(Intent intent) {
        String str = intent.getExtras() != null ? (String) intent.getExtras().get("momoid") : null;
        if (!a.a((CharSequence) str) && this.f1917a.Q.h.equals(str)) {
            this.f1917a.O.a(this.f1917a.Q, str);
            this.f1917a.Y();
        }
    }
}
