package com.immomo.momo.android.view;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.text.style.ReplacementSpan;
import java.lang.ref.WeakReference;

public abstract class aa extends ReplacementSpan {

    /* renamed from: a  reason: collision with root package name */
    private WeakReference f2711a;

    protected aa() {
    }

    public abstract Drawable a();

    /* access modifiers changed from: protected */
    public Drawable b() {
        WeakReference weakReference = this.f2711a;
        Drawable drawable = null;
        if (weakReference != null) {
            drawable = (Drawable) weakReference.get();
        }
        if (drawable != null) {
            return drawable;
        }
        Drawable a2 = a();
        this.f2711a = new WeakReference(a2);
        return a2;
    }

    /* access modifiers changed from: protected */
    public final void c() {
        this.f2711a = null;
    }

    public void draw(Canvas canvas, CharSequence charSequence, int i, int i2, float f, int i3, int i4, int i5, Paint paint) {
        Drawable b = b();
        canvas.save();
        canvas.translate(f, (float) (i5 - b.getBounds().bottom));
        b.draw(canvas);
        canvas.restore();
    }

    public int getSize(Paint paint, CharSequence charSequence, int i, int i2, Paint.FontMetricsInt fontMetricsInt) {
        Rect bounds = b().getBounds();
        if (fontMetricsInt != null) {
            fontMetricsInt.ascent = -bounds.bottom;
            fontMetricsInt.descent = 0;
            fontMetricsInt.top = fontMetricsInt.ascent;
            fontMetricsInt.bottom = 0;
        }
        return bounds.right;
    }
}
