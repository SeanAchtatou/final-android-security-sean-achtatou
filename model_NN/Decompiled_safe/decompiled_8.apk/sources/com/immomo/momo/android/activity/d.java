package com.immomo.momo.android.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.immomo.a.a.f.a;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.common.InviteSNSActivity;
import com.immomo.momo.android.activity.emotestore.EmotionInviteTypeActivity;
import com.immomo.momo.android.activity.emotestore.EmotionProfileActivity;
import com.immomo.momo.android.activity.emotestore.MainEmotionActivity;
import com.immomo.momo.android.activity.emotestore.MineEmotesActivity;
import com.immomo.momo.android.activity.event.EventFeedProfileActivity;
import com.immomo.momo.android.activity.event.EventFeedsActivity;
import com.immomo.momo.android.activity.event.EventMemberListActivity;
import com.immomo.momo.android.activity.event.EventProfileActivity;
import com.immomo.momo.android.activity.event.MainEventActivity;
import com.immomo.momo.android.activity.feed.FeedProfileActivity;
import com.immomo.momo.android.activity.feed.MainFeedActivity;
import com.immomo.momo.android.activity.feed.OtherFeedListActivity;
import com.immomo.momo.android.activity.feed.PublishFeedActivity;
import com.immomo.momo.android.activity.group.GroupFeedProfileActivity;
import com.immomo.momo.android.activity.group.GroupFeedsActivity;
import com.immomo.momo.android.activity.group.GroupMemberListActivity;
import com.immomo.momo.android.activity.group.GroupPartyActivity;
import com.immomo.momo.android.activity.group.GroupProfileActivity;
import com.immomo.momo.android.activity.maintab.MaintabActivity;
import com.immomo.momo.android.activity.maintab.aq;
import com.immomo.momo.android.activity.message.ChatActivity;
import com.immomo.momo.android.activity.message.GroupChatActivity;
import com.immomo.momo.android.activity.message.MultiChatActivity;
import com.immomo.momo.android.activity.setting.FeedBackListActivity;
import com.immomo.momo.android.activity.tieba.MainTiebaActivity;
import com.immomo.momo.android.activity.tieba.TieDetailActivity;
import com.immomo.momo.android.activity.tieba.TiebaCategoryActivity;
import com.immomo.momo.android.activity.tieba.TiebaMemberListActivity;
import com.immomo.momo.android.activity.tieba.TiebaProfileActivity;
import com.immomo.momo.android.game.GameProfileActivity;
import com.immomo.momo.android.game.NearbyPlayersActivity;
import com.immomo.momo.android.pay.BuyMemberActivity;
import com.immomo.momo.android.pay.MemIntroductionDetailActivity;
import com.immomo.momo.android.pay.MemberCenterActivity;
import com.immomo.momo.util.ao;
import com.sina.sdk.api.message.InviteApi;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.regex.Pattern;

public final class d {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static Map f1214a = new HashMap();
    /* access modifiers changed from: private */
    public static e b = null;
    private static Timer c = new Timer();
    /* access modifiers changed from: private */
    public static boolean d = true;

    static void a() {
        if (!d) {
            d = true;
            for (f b2 : f1214a.values()) {
                b2.b();
            }
        }
        g();
    }

    public static void a(String str, f fVar) {
        f1214a.put(str, fVar);
    }

    public static boolean a(String str) {
        return "goto_profile".equals(str) || "goto_group_profile".equals(str) || "goto_group_party".equals(str) || "goto_chat".equals(str) || "goto_group_chat".equals(str) || "goto_discuss_chat".equals(str) || "goto_emoteshop".equals(str) || "goto_myemote".equals(str) || "goto_vipcenter".equals(str) || "goto_emote".equals(str) || InviteApi.KEY_URL.equals(str) || "goto_releasefeed".equals(str) || "goto_setting".equals(str) || "goto_url".equals(str) || "goto_event".equals(str) || "goto_emotioninvite".equals(str) || "goto_vipbuy".equals(str) || "goto_vipurl".equals(str) || "goto_eventlist".equals(str) || "goto_groupinvite".equals(str) || "goto_tieba".equals(str) || "goto_tieba_category".equals(str) || "goto_tieba_tielist".equals(str) || "goto_tieba_tie".equals(str) || "goto_roaming".equals(str) || "goto_gameprofile".equals(str) || "goto_group_space".equals(str) || "goto_group_feed".equals(str) || "goto_feedback".equals(str);
    }

    public static boolean a(String str, Context context) {
        Class cls;
        if (a.a(str) || !Pattern.compile("\\[.*?\\|.*?\\|.*?\\]").matcher(str).matches()) {
            return false;
        }
        String[] split = str.substring(1, str.length() - 1).split("\\|");
        if (split.length <= 1) {
            return false;
        }
        String str2 = split[1];
        Bundle bundle = new Bundle();
        if (split.length > 2) {
            if ("goto_profile".equals(str2)) {
                bundle.putString("momoid", split[2]);
            } else if ("goto_group_profile".equals(str2)) {
                bundle.putString("gid", split[2]);
            } else if ("goto_group_party".equals(str2)) {
                bundle.putString("pid", split[2]);
            } else if ("goto_chat".equals(str2)) {
                bundle.putString("remoteUserID", split[2]);
            } else if ("goto_group_chat".equals(str2)) {
                bundle.putString("remoteGroupID", split[2]);
            } else if ("goto_discuss_chat".equals(str2)) {
                bundle.putString("remoteDiscussID", split[2]);
            } else if ("goto_emote".equals(str2)) {
                bundle.putString("eid", split[2]);
            } else if (InviteApi.KEY_URL.equals(str2) || "goto_url".equals(str2)) {
                if (a.a(split[2]) || !split[2].startsWith("http")) {
                    return false;
                }
                bundle.putString("webview_url", split[2]);
                bundle.putString("webview_title", split[0]);
            } else if ("goto_vipurl".equals(str2)) {
                if (a.a(split[2]) || !split[2].startsWith("http")) {
                    return false;
                }
                bundle.putString("webview_url", split[2]);
                bundle.putString("webview_title", split[0]);
            } else if ("goto_app".equals(str2)) {
                try {
                    Intent intent = new Intent(context.getApplicationContext(), Class.forName(split[2]));
                    intent.addFlags(268435456);
                    context.startActivity(intent);
                    return true;
                } catch (ClassNotFoundException e) {
                }
            } else if ("goto_event".equals(str2)) {
                bundle.putString("eventid", split[2]);
            } else if ("goto_groupinvite".equals(str2)) {
                bundle.putString("invite_id", split[2]);
                bundle.putInt("invite_type", 2);
            } else if ("goto_tieba_tielist".equals(str2)) {
                bundle.putString("tiebaid", split[2]);
            } else if ("goto_tieba_tie".equals(str2)) {
                bundle.putString("key_tieid", split[2]);
            } else if ("goto_gameprofile".equals(str2)) {
                bundle.putString("appid", split[2]);
            } else if ("goto_emoteshop".equals(str2)) {
                if (!a.a(split[2])) {
                    try {
                        int parseInt = Integer.parseInt(split[2]);
                        if (parseInt == 1) {
                            bundle.putInt("showindex", 0);
                        } else if (parseInt == 2) {
                            bundle.putInt("showindex", 1);
                        } else if (parseInt == 3) {
                            bundle.putInt("showindex", 2);
                        }
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                }
            } else if ("goto_group_feed".equals(str2)) {
                bundle.putString("key_groupfeedid", split[2]);
            } else if ("goto_group_space".equals(str2)) {
                bundle.putString("gid", split[2]);
            }
        }
        if ("goto_releasefeed".equals(str2)) {
            bundle.putString("toptic", split[0]);
        }
        Intent intent2 = new Intent();
        if (!(context instanceof Activity)) {
            intent2.setFlags(268435456);
        }
        if ("goto_profile".equals(str2)) {
            cls = OtherProfileActivity.class;
        } else if ("goto_group_profile".equals(str2)) {
            cls = GroupProfileActivity.class;
        } else if ("goto_group_party".equals(str2)) {
            cls = GroupPartyActivity.class;
        } else if ("goto_chat".equals(str2)) {
            cls = ChatActivity.class;
        } else if ("goto_group_chat".equals(str2)) {
            cls = GroupChatActivity.class;
        } else if ("goto_discuss_chat".equals(str2)) {
            cls = MultiChatActivity.class;
        } else if ("goto_emoteshop".equals(str2)) {
            cls = MainEmotionActivity.class;
        } else if ("goto_myemote".equals(str2)) {
            cls = MineEmotesActivity.class;
        } else if ("goto_vipcenter".equals(str2)) {
            cls = MemberCenterActivity.class;
        } else if ("goto_vipbuy".equals(str2)) {
            cls = BuyMemberActivity.class;
        } else if ("goto_emote".equals(str2)) {
            cls = EmotionProfileActivity.class;
        } else if ("goto_releasefeed".equals(str2)) {
            cls = PublishFeedActivity.class;
        } else if ("goto_setting".equals(str2)) {
            cls = MaintabActivity.class;
            intent2.addFlags(67108864);
            intent2.putExtra("tabindex", 4);
        } else if ("goto_event".equals(str2)) {
            cls = EventProfileActivity.class;
        } else if ("goto_emotioninvite".equals(str2)) {
            cls = EmotionInviteTypeActivity.class;
        } else if (InviteApi.KEY_URL.equals(str2) || "goto_url".equals(str2)) {
            cls = WebviewActivity.class;
        } else if ("goto_vipurl".equals(str2)) {
            cls = MemIntroductionDetailActivity.class;
        } else if ("goto_eventlist".equals(str2)) {
            cls = MainEventActivity.class;
        } else if ("goto_groupinvite".equals(str2)) {
            cls = InviteSNSActivity.class;
        } else if ("goto_tieba".equals(str2)) {
            cls = MainTiebaActivity.class;
        } else if ("goto_tieba_category".equals(str2)) {
            cls = TiebaCategoryActivity.class;
        } else if ("goto_tieba_tielist".equals(str2)) {
            cls = TiebaProfileActivity.class;
        } else if ("goto_tieba_tie".equals(str2)) {
            cls = TieDetailActivity.class;
        } else if ("goto_roaming".equals(str2)) {
            cls = UserRoamActivity.class;
        } else if ("goto_gameprofile".equals(str2)) {
            cls = GameProfileActivity.class;
        } else if ("goto_group_space".equals(str2)) {
            cls = GroupFeedsActivity.class;
        } else if ("goto_group_feed".equals(str2)) {
            cls = GroupFeedProfileActivity.class;
        } else if ("goto_feedback".equals(str2)) {
            cls = FeedBackListActivity.class;
        } else {
            ao.g(R.string.errormsg_downversion);
            return false;
        }
        intent2.setClass(context.getApplicationContext(), cls);
        intent2.putExtras(bundle);
        context.startActivity(intent2);
        return true;
    }

    static void b() {
        e eVar = new e((byte) 0);
        g();
        b = eVar;
        c.schedule(b, 10000);
    }

    public static boolean b(String str) {
        return TieDetailActivity.class.getName().equals(str);
    }

    public static void c() {
        f1214a.clear();
        g();
    }

    public static boolean c(String str) {
        return TiebaMemberListActivity.class.getName().equals(str);
    }

    public static boolean d(String str) {
        return aq.class.getName().equals(str);
    }

    public static boolean e(String str) {
        return ChatActivity.class.getName().equals(str);
    }

    public static boolean f(String str) {
        return com.immomo.momo.android.activity.maintab.a.class.getName().equals(str);
    }

    private static void g() {
        if (b != null) {
            b.cancel();
            b = null;
            c.purge();
        }
    }

    public static boolean g(String str) {
        return OtherProfileActivity.class.getName().equals(str);
    }

    public static boolean h(String str) {
        return MainEmotionActivity.class.getName().equals(str) || EmotionProfileActivity.class.getName().equals(str);
    }

    public static boolean i(String str) {
        return MainFeedActivity.class.getName().equals(str) || FeedProfileActivity.class.getName().equals(str) || OtherFeedListActivity.class.getName().equals(str);
    }

    public static boolean j(String str) {
        return GroupMemberListActivity.class.getName().equals(str);
    }

    public static boolean k(String str) {
        return UserRoamActivity.class.getName().equals(str);
    }

    public static boolean l(String str) {
        return GroupChatActivity.class.getName().equals(str);
    }

    public static boolean m(String str) {
        return NearbyPlayersActivity.class.getName().equals(str);
    }

    public static boolean n(String str) {
        return EventMemberListActivity.class.getName().equals(str) || EventFeedsActivity.class.getName().equals(str) || EventFeedProfileActivity.class.getName().equals(str);
    }
}
