package com.immomo.momo.android.activity;

import android.os.AsyncTask;
import com.immomo.momo.R;
import com.immomo.momo.a.a;
import com.immomo.momo.protocol.a.w;
import java.util.Date;
import java.util.List;

final class bb extends AsyncTask {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ BlacklistActivity f1039a;

    private bb(BlacklistActivity blacklistActivity) {
        this.f1039a = blacklistActivity;
    }

    /* synthetic */ bb(BlacklistActivity blacklistActivity, byte b) {
        this(blacklistActivity);
    }

    private List a() {
        try {
            List g = w.a().g();
            this.f1039a.k.h();
            this.f1039a.k.g(g);
            return g;
        } catch (a e) {
            this.f1039a.b((CharSequence) e.getMessage());
            this.f1039a.e.a((Throwable) e);
        } catch (Exception e2) {
            this.f1039a.b((int) R.string.errormsg_server);
            this.f1039a.e.a((Throwable) e2);
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object doInBackground(Object... objArr) {
        return a();
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void onPostExecute(Object obj) {
        List list = (List) obj;
        this.f1039a.n = new Date();
        this.f1039a.g.a(this.f1039a.n);
        if (list != null) {
            if (list.size() > 0) {
                BlacklistActivity.a(this.f1039a, list);
            } else {
                this.f1039a.l.a();
            }
            this.f1039a.i.setLastFlushTime(this.f1039a.n);
            this.f1039a.g.b(this.f1039a.n);
        } else {
            this.f1039a.a((CharSequence) "获取黑名单失败");
        }
        this.f1039a.i.n();
    }
}
