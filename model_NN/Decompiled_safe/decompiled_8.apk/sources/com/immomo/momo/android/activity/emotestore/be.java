package com.immomo.momo.android.activity.emotestore;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.protocol.a.i;
import com.immomo.momo.service.bean.q;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

final class be extends d {

    /* renamed from: a  reason: collision with root package name */
    private List f1307a = null;
    private List c = null;
    private /* synthetic */ av d;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public be(av avVar, Context context) {
        super(context);
        this.d = avVar;
        if (avVar.R != null) {
            avVar.R.cancel(true);
        }
        avVar.R = this;
    }

    /* access modifiers changed from: protected */
    public final Object a(Object... objArr) {
        this.f1307a = new ArrayList();
        this.c = new ArrayList();
        ArrayList<q> arrayList = new ArrayList<>();
        i.a().a((Collection) arrayList);
        for (q qVar : arrayList) {
            if (qVar.u) {
                this.f1307a.add(qVar);
            } else {
                this.c.add(qVar);
            }
        }
        this.d.P.f(this.f1307a);
        this.d.P.g(this.c);
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public final void a(Object obj) {
        Date date = new Date();
        this.d.O.setLastFlushTime(date);
        this.d.N.a("mineem_reflush", date);
        if (!this.d.Q.e()) {
            this.d.T.clear();
            this.d.U.clear();
            this.d.U.addAll(this.c);
            this.d.T.addAll(this.f1307a);
            this.d.Q.notifyDataSetChanged();
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.d.O.n();
        this.d.R = null;
    }
}
