package com.immomo.momo.android.view;

import java.io.File;
import java.io.InputStream;

final class ax implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    au f2732a = null;
    private InputStream b;
    private File c = null;
    private /* synthetic */ au d;

    public ax(au auVar, File file, au auVar2) {
        this.d = auVar;
        this.f2732a = auVar2;
        this.c = file;
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x0055 A[Catch:{ IOException -> 0x0083, Throwable -> 0x00ac, all -> 0x0097 }] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0062 A[Catch:{ IOException -> 0x0083, Throwable -> 0x00ac, all -> 0x0097 }] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x006d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
            r5 = this;
            r0 = 1
            r1 = 0
            com.immomo.momo.android.view.au r2 = r5.d     // Catch:{ InterruptedException -> 0x007f }
            int r2 = r2.H     // Catch:{ InterruptedException -> 0x007f }
            if (r2 != r0) goto L_0x0028
            com.immomo.momo.android.view.au r2 = r5.d     // Catch:{ InterruptedException -> 0x007f }
            com.immomo.momo.util.m r2 = r2.F     // Catch:{ InterruptedException -> 0x007f }
            java.lang.String r3 = "----gif acquire"
            r2.a(r3)     // Catch:{ InterruptedException -> 0x007f }
            java.util.concurrent.Semaphore r2 = com.immomo.momo.android.view.au.D     // Catch:{ InterruptedException -> 0x007f }
            r2.acquire()     // Catch:{ InterruptedException -> 0x007f }
            com.immomo.momo.android.view.au r1 = r5.d     // Catch:{ InterruptedException -> 0x00c5, all -> 0x00c0 }
            com.immomo.momo.util.m r1 = r1.F     // Catch:{ InterruptedException -> 0x00c5, all -> 0x00c0 }
            java.lang.String r2 = "----gif acquire success"
            r1.a(r2)     // Catch:{ InterruptedException -> 0x00c5, all -> 0x00c0 }
            r1 = r0
        L_0x0028:
            java.io.InputStream r0 = r5.b     // Catch:{ IOException -> 0x0083, Throwable -> 0x00ac }
            if (r0 != 0) goto L_0x0039
            java.io.File r0 = r5.c     // Catch:{ IOException -> 0x0083, Throwable -> 0x00ac }
            if (r0 == 0) goto L_0x0039
            java.io.FileInputStream r0 = new java.io.FileInputStream     // Catch:{ IOException -> 0x0083, Throwable -> 0x00ac }
            java.io.File r2 = r5.c     // Catch:{ IOException -> 0x0083, Throwable -> 0x00ac }
            r0.<init>(r2)     // Catch:{ IOException -> 0x0083, Throwable -> 0x00ac }
            r5.b = r0     // Catch:{ IOException -> 0x0083, Throwable -> 0x00ac }
        L_0x0039:
            com.immomo.momo.android.view.au r0 = r5.d     // Catch:{ IOException -> 0x0083, Throwable -> 0x00ac }
            java.io.InputStream r2 = r5.b     // Catch:{ IOException -> 0x0083, Throwable -> 0x00ac }
            r0.a(r2)     // Catch:{ IOException -> 0x0083, Throwable -> 0x00ac }
            com.immomo.momo.android.view.au r0 = r5.d     // Catch:{ IOException -> 0x0083, Throwable -> 0x00ac }
            int r0 = r0.f2729a     // Catch:{ IOException -> 0x0083, Throwable -> 0x00ac }
            if (r0 == 0) goto L_0x004f
            java.io.File r0 = r5.c     // Catch:{ IOException -> 0x0083, Throwable -> 0x00ac }
            if (r0 == 0) goto L_0x004f
            java.io.File r0 = r5.c     // Catch:{ IOException -> 0x0083, Throwable -> 0x00ac }
            r0.delete()     // Catch:{ IOException -> 0x0083, Throwable -> 0x00ac }
        L_0x004f:
            com.immomo.momo.android.view.au r0 = r5.d     // Catch:{ all -> 0x0097 }
            boolean r0 = r0.d     // Catch:{ all -> 0x0097 }
            if (r0 == 0) goto L_0x005a
            com.immomo.momo.android.view.au r0 = r5.d     // Catch:{ all -> 0x0097 }
            r0.c()     // Catch:{ all -> 0x0097 }
        L_0x005a:
            com.immomo.momo.android.view.au r0 = r5.d     // Catch:{ all -> 0x0097 }
            com.immomo.momo.android.view.ay r0 = r0.Y     // Catch:{ all -> 0x0097 }
            if (r0 == 0) goto L_0x006b
            com.immomo.momo.android.view.au r0 = r5.d     // Catch:{ all -> 0x0097 }
            com.immomo.momo.android.view.ay r0 = r0.Y     // Catch:{ all -> 0x0097 }
            r0.a()     // Catch:{ all -> 0x0097 }
        L_0x006b:
            if (r1 == 0) goto L_0x0079
            r0 = 100
            java.lang.Thread.sleep(r0)     // Catch:{ InterruptedException -> 0x00be }
        L_0x0072:
            java.util.concurrent.Semaphore r0 = com.immomo.momo.android.view.au.D
            r0.release()
        L_0x0079:
            java.io.InputStream r0 = r5.b
            android.support.v4.b.a.a(r0)
            return
        L_0x007f:
            r0 = move-exception
            r0 = r1
        L_0x0081:
            r1 = r0
            goto L_0x0028
        L_0x0083:
            r0 = move-exception
            com.immomo.momo.android.view.au r2 = r5.d     // Catch:{ all -> 0x0097 }
            com.immomo.momo.util.m r2 = r2.F     // Catch:{ all -> 0x0097 }
            r2.a(r0)     // Catch:{ all -> 0x0097 }
            java.io.File r0 = r5.c     // Catch:{ all -> 0x0097 }
            if (r0 == 0) goto L_0x004f
            java.io.File r0 = r5.c     // Catch:{ all -> 0x0097 }
            r0.delete()     // Catch:{ all -> 0x0097 }
            goto L_0x004f
        L_0x0097:
            r0 = move-exception
        L_0x0098:
            if (r1 == 0) goto L_0x00a6
            r1 = 100
            java.lang.Thread.sleep(r1)     // Catch:{ InterruptedException -> 0x00bc }
        L_0x009f:
            java.util.concurrent.Semaphore r1 = com.immomo.momo.android.view.au.D
            r1.release()
        L_0x00a6:
            java.io.InputStream r1 = r5.b
            android.support.v4.b.a.a(r1)
            throw r0
        L_0x00ac:
            r0 = move-exception
            com.immomo.momo.android.view.au r2 = r5.d     // Catch:{ all -> 0x0097 }
            com.immomo.momo.util.m r2 = r2.F     // Catch:{ all -> 0x0097 }
            r2.a(r0)     // Catch:{ all -> 0x0097 }
            com.immomo.momo.android.view.au r0 = r5.d     // Catch:{ all -> 0x0097 }
            r2 = 2
            r0.f2729a = r2     // Catch:{ all -> 0x0097 }
            goto L_0x004f
        L_0x00bc:
            r1 = move-exception
            goto L_0x009f
        L_0x00be:
            r0 = move-exception
            goto L_0x0072
        L_0x00c0:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
            goto L_0x0098
        L_0x00c5:
            r1 = move-exception
            goto L_0x0081
        */
        throw new UnsupportedOperationException("Method not decompiled: com.immomo.momo.android.view.ax.run():void");
    }
}
