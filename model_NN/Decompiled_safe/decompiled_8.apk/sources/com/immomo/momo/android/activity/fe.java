package com.immomo.momo.android.activity;

import android.content.Context;
import com.immomo.momo.android.c.b;
import com.immomo.momo.protocol.a.w;
import com.immomo.momo.service.aq;
import com.immomo.momo.service.bean.bf;

final class fe extends b {
    private bf c = null;
    private /* synthetic */ HiddenlistActivity d;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public fe(HiddenlistActivity hiddenlistActivity, Context context) {
        super(context);
        this.d = hiddenlistActivity;
    }

    /* access modifiers changed from: protected */
    public final Object a(Object... objArr) {
        this.c = (bf) objArr[0];
        w.a().f(this.c.h);
        this.d.l.d(this.c);
        aq unused = this.d.k;
        aq.f();
        this.d.k.f(this.d.l.a());
        return null;
    }

    /* access modifiers changed from: protected */
    public final void a(Object obj) {
        a("已取消对其隐身");
        this.d.l.notifyDataSetChanged();
    }
}
