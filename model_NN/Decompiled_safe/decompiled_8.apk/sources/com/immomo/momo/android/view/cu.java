package com.immomo.momo.android.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.widget.AbsListView;
import com.baidu.location.LocationClientOption;
import com.immomo.momo.g;
import com.immomo.momo.util.m;

public class cu extends az implements View.OnTouchListener {

    /* renamed from: a  reason: collision with root package name */
    protected boolean f2769a;
    protected int b;
    protected View c = null;
    private boolean d;
    private boolean e;
    private boolean f;
    private float g;
    private float h;
    private View i;
    private cw j;
    private boolean k;
    private boolean l;
    private int m;
    private dc n;
    private float o;
    private View.OnTouchListener p;
    private int q;
    private VelocityTracker r;
    private boolean s;
    private View.OnTouchListener t;
    private float u;
    private boolean v;
    private boolean w;

    public cu(Context context) {
        super(context);
        new m(getClass().getSimpleName());
        this.t = null;
        this.u = 0.0f;
        this.v = false;
        this.w = false;
        this.p = this;
        this.m = 0;
        b();
    }

    public cu(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        new m(getClass().getSimpleName());
        this.t = null;
        this.u = 0.0f;
        this.v = false;
        this.w = false;
        this.p = this;
        this.m = 0;
        b();
    }

    public cu(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        new m(getClass().getSimpleName());
        this.t = null;
        this.u = 0.0f;
        this.v = false;
        this.w = false;
        this.p = this;
        this.m = 0;
        b();
    }

    private void b() {
        setFadingEdgeColor(0);
        setFadingEdgeLength(0);
        setCacheColorHint(-1);
        ViewConfiguration viewConfiguration = ViewConfiguration.get(getContext());
        this.q = viewConfiguration.getScaledTouchSlop();
        viewConfiguration.getScaledMinimumFlingVelocity();
        this.n = new dc(getContext());
        this.h = 0.0f;
        this.k = false;
        this.d = false;
        this.b = g.a(0.0f);
        this.i = null;
        this.f2769a = false;
        this.s = false;
        this.l = false;
        View view = new View(getContext());
        view.setLayoutParams(new AbsListView.LayoutParams(-1, -2));
        addHeaderView(view);
        super.setOnTouchListener(this.p);
    }

    private void b(int i2) {
        int scrollX = 0 - getScrollX();
        int customScrollY = i2 - getCustomScrollY();
        if (!this.n.a()) {
            this.n.e();
        }
        this.n.a(getScrollX(), getCustomScrollY(), scrollX, customScrollY);
        invalidate();
    }

    private void g() {
        if (!this.f2769a && getCustomScrollY() <= 0 && this.j != null) {
            this.j.a(-getCustomScrollY(), this.b);
            if ((-getCustomScrollY()) < this.b || this.d) {
                this.f2769a = false;
            } else {
                this.f2769a = true;
            }
            if (this.f2769a) {
                b(-this.b);
                this.j.b();
            }
        }
    }

    public final void a(int i2) {
        int scrollX = 0 - getScrollX();
        int customScrollY = i2 - getCustomScrollY();
        if (!this.n.a()) {
            this.n.e();
        }
        this.n.a(getScrollX(), getCustomScrollY(), scrollX, customScrollY, 1500);
        invalidate();
    }

    public final void a(View view, int i2) {
        this.b = g.a((float) i2);
        this.i = view;
    }

    public void computeScroll() {
        if (!this.e) {
            if (this.n.d()) {
                int c2 = this.n.c();
                setPreventInvalidate(true);
                scrollTo(0, c2);
                setPreventInvalidate(false);
                postInvalidate();
            } else if (!this.d && !this.f2769a && getCustomScrollY() < 0) {
                b(0);
            }
        }
    }

    public int getCustomScrollY() {
        return this.m;
    }

    public void invalidate() {
        if (!this.k) {
            super.invalidate();
        }
    }

    public void invalidate(int i2, int i3, int i4, int i5) {
        if (!this.k) {
            super.invalidate(i2, i3, i4, i5);
        }
    }

    public void invalidate(Rect rect) {
        if (!this.k) {
            super.invalidate(rect);
        }
    }

    public void invalidateDrawable(Drawable drawable) {
        if (!this.k) {
            super.invalidateDrawable(drawable);
        }
    }

    public final void j() {
        if (this.c != null) {
            addFooterView(this.c);
        }
    }

    public final boolean k() {
        return this.d;
    }

    public final boolean l() {
        return this.l;
    }

    public final void m() {
        if (this.f2769a) {
            this.f2769a = false;
            this.s = true;
            if (getCustomScrollY() < 0) {
                b(0);
            } else {
                invalidate();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (!this.e) {
            if (this.m < 0) {
                canvas.translate(0.0f, (float) (-this.m));
            }
            if (getFirstVisiblePosition() == 0) {
                this.f = true;
            } else {
                this.f = false;
            }
            if (!this.f2769a && this.f && getCustomScrollY() <= 0) {
                getCustomScrollY();
                if (this.s) {
                    this.s = false;
                }
                if (this.i != null && !this.l) {
                    this.i.draw(canvas);
                }
                g();
            } else if (this.f2769a && !this.f) {
                scrollTo(0, 0);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        if (this.i != null) {
            this.i.layout(0, this.b, i4, 0);
        }
        super.onLayout(z, i2, i3, i4, i5);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        if (this.i != null) {
            this.i.measure(i2, View.MeasureSpec.makeMeasureSpec(this.b, 1073741824));
        }
        super.onMeasure(i2, i3);
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (this.t != null) {
            this.t.onTouch(view, motionEvent);
        }
        if (this.l) {
            return false;
        }
        if (getFirstVisiblePosition() == 0) {
            this.f = true;
        } else {
            this.f = false;
        }
        if (this.r == null) {
            this.r = VelocityTracker.obtain();
        }
        this.r.addMovement(motionEvent);
        switch (motionEvent.getAction()) {
            case 0:
                this.d = this.f;
                this.w = false;
                if (!this.n.a()) {
                    this.n.e();
                }
                this.h = motionEvent.getY();
                this.g = motionEvent.getY();
                this.o = 0.0f;
                return false;
            case 1:
            case 3:
                if (this.w) {
                    this.w = false;
                    return false;
                }
                this.o = 0.0f;
                this.g = -1.0f;
                this.h = 0.0f;
                this.r.computeCurrentVelocity(LocationClientOption.MIN_SCAN_SPAN);
                this.u = 0.0f;
                this.v = false;
                setInterceptItemClick(false);
                this.r.getYVelocity();
                if (this.r != null) {
                    this.r.recycle();
                    this.r = null;
                }
                if (this.d) {
                    this.d = false;
                    if (this.f2769a) {
                        return false;
                    }
                    g();
                    b(0);
                    return false;
                } else if (this.n.a()) {
                    return false;
                } else {
                    this.n.e();
                    return false;
                }
            case 2:
                float y = this.h - motionEvent.getY();
                int i2 = (int) y;
                if (this.d || this.h != 0.0f) {
                    this.h = motionEvent.getY();
                    if (this.d) {
                        if (!this.f || getCustomScrollY() + i2 >= 0) {
                            this.d = false;
                            if (!this.f2769a && getCustomScrollY() < 0) {
                                scrollTo(0, 0);
                                setSelection(0);
                            }
                            this.u = 0.0f;
                        } else {
                            boolean z = getCustomScrollY() + i2 >= (-this.b);
                            if (!this.f2769a || z) {
                                if (i2 >= 0 && this.o == 0.0f) {
                                    this.o = motionEvent.getY();
                                }
                                int i3 = (int) (y / 2.0f);
                                if (this.g < 0.0f || !this.f) {
                                    scrollBy(0, i3);
                                } else if (Math.abs(this.g - motionEvent.getY()) <= ((float) this.q)) {
                                    scrollBy(0, i3);
                                } else {
                                    this.g = -1.0f;
                                    scrollBy(0, i3);
                                    if (this.u < -3.0f) {
                                        this.u = -3.0f;
                                    }
                                    if (!this.v) {
                                        this.v = true;
                                        if (pointToPosition((int) motionEvent.getX(), (int) motionEvent.getY()) >= 0) {
                                            this.w = true;
                                            setInterceptItemClick(true);
                                            dispatchTouchEvent(MotionEvent.obtain(motionEvent.getDownTime(), motionEvent.getDownTime() + 100, 3, motionEvent.getX(), motionEvent.getY(), 1));
                                        }
                                    }
                                }
                            } else {
                                scrollTo(0, -this.b);
                            }
                        }
                        if (this.u < -3.0f) {
                            this.u = -3.0f;
                        }
                        return true;
                    } else if (!this.f || i2 >= 0) {
                        return this.f && this.o > 0.0f;
                    } else {
                        this.d = true;
                        return true;
                    }
                } else {
                    this.h = motionEvent.getY();
                    return false;
                }
            default:
                return false;
        }
    }

    public void scrollBy(int i2, int i3) {
        scrollTo(0, getCustomScrollY() + i3);
    }

    public void scrollTo(int i2, int i3) {
        this.m = i3;
        invalidate();
    }

    public void setAutoOverScrollMultiplier(float f2) {
    }

    public void setFooterView(View view) {
        this.c = view;
    }

    public void setIsGingerbread(boolean z) {
        this.e = z;
    }

    public void setOnTouchListener(View.OnTouchListener onTouchListener) {
        this.t = onTouchListener;
    }

    public void setOverScrollListener(cw cwVar) {
        this.j = cwVar;
    }

    public void setOverScrollView(View view) {
        a(view, 0);
    }

    public void setPreventInvalidate(boolean z) {
        this.k = z;
    }

    public void setPreventOverScroll(boolean z) {
        this.l = z;
        this.f2769a = false;
        if (getCustomScrollY() != 0) {
            b(0);
        }
    }
}
