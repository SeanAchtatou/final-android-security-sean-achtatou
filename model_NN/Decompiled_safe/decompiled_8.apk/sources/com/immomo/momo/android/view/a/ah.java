package com.immomo.momo.android.view.a;

import com.amap.mapapi.poisearch.PoiTypeDef;

public enum ah {
    ALL(PoiTypeDef.All),
    MALE("M"),
    FEMALE("F");
    
    private String d;

    private ah(String str) {
        this.d = str;
    }

    public final String a() {
        return this.d;
    }
}
