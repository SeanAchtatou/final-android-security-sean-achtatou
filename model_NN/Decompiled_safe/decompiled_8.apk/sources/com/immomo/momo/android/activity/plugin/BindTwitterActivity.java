package com.immomo.momo.android.activity.plugin;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.b.a;
import android.webkit.WebView;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.a.e;
import com.immomo.momo.a.w;
import com.immomo.momo.android.activity.ah;
import com.immomo.momo.android.view.HeaderLayout;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.g;
import com.immomo.momo.protocol.a.d;
import com.immomo.momo.util.ao;
import com.immomo.momo.util.m;
import java.net.URLEncoder;
import org.apache.http.conn.ConnectTimeoutException;

public class BindTwitterActivity extends ah {
    private HeaderLayout h = null;
    /* access modifiers changed from: private */
    public String i = "http://test.wdy/success";
    /* access modifiers changed from: private */
    public WebView j;
    private TextView k;
    /* access modifiers changed from: private */
    public v l = null;
    /* access modifiers changed from: private */
    public String m = "XXX-XXXX-XXXX-XXXX";
    /* access modifiers changed from: private */
    public String n = "XXX-XXXX-XXXX-XXXX";
    /* access modifiers changed from: private */
    public String o = null;
    private String p = null;
    private String q = null;
    /* access modifiers changed from: private */
    public String r;
    private an s;
    /* access modifiers changed from: private */
    public m t = new m("test_momo");

    private String a(String str, String str2, String str3, String str4, String str5) {
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append("?oauth_consumer_key=").append(this.m);
        sb.append("&oauth_signature_method=").append(ar.c());
        sb.append("&oauth_signature=").append(URLEncoder.encode(str4));
        sb.append("&oauth_timestamp=").append(str2);
        sb.append("&oauth_nonce=").append(str3);
        if (a.f(str5)) {
            sb.append("&oauth_token=").append(str5);
        }
        return sb.toString();
    }

    protected static String[] c(String str) {
        Uri parse = Uri.parse(str);
        String[] strArr = {parse.getQueryParameter("oauth_token"), parse.getQueryParameter("oauth_verifier")};
        if (!a.f(strArr[0]) || !a.f(strArr[1])) {
            return null;
        }
        return strArr;
    }

    private boolean e(String str) {
        if (!a.f(str)) {
            return false;
        }
        Uri parse = Uri.parse(String.valueOf(this.i) + "?" + str);
        this.o = parse.getQueryParameter("oauth_token");
        this.p = parse.getQueryParameter("oauth_token_secret");
        this.q = parse.getQueryParameter("user_id");
        return a.f(this.o) && a.f(this.p);
    }

    static /* synthetic */ void h(BindTwitterActivity bindTwitterActivity) {
        boolean z;
        try {
            d.b(bindTwitterActivity.q, bindTwitterActivity.o, bindTwitterActivity.p);
            z = true;
        } catch (e e) {
            bindTwitterActivity.t.a((Throwable) e);
            ao.g(R.string.errormsg_network_normal400);
            z = false;
        } catch (w e2) {
            bindTwitterActivity.t.a((Throwable) e2);
            ao.g(R.string.errormsg_network_unfind);
            z = false;
        } catch (ConnectTimeoutException e3) {
            bindTwitterActivity.t.a((Throwable) e3);
            ao.g(R.string.errormsg_network_timeout);
            z = false;
        } catch (Exception e4) {
            bindTwitterActivity.t.a((Throwable) e4);
            z = false;
        }
        if (!z) {
            ao.a((CharSequence) "绑定失败，请稍后再试");
            return;
        }
        ao.a((CharSequence) "豆瓣账号绑定成功");
        g.q().an = true;
        bindTwitterActivity.setResult(-1, bindTwitterActivity.getIntent());
        bindTwitterActivity.finish();
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x00a2 A[SYNTHETIC, Splitter:B:13:0x00a2] */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0094 A[Catch:{ Exception -> 0x00bf }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean u() {
        /*
            r8 = this;
            r6 = 0
            java.lang.String r3 = com.immomo.momo.android.activity.plugin.ar.b()
            java.lang.String r4 = com.immomo.momo.android.activity.plugin.ar.a()
            java.lang.String r7 = ""
            java.lang.String r0 = "GET"
            java.lang.String r1 = "https://api.twitter.com/oauth/request_token"
            java.lang.String r2 = r8.m     // Catch:{ UnsupportedEncodingException -> 0x0099 }
            r5 = 0
            java.lang.String r0 = com.immomo.momo.android.activity.plugin.ar.a(r0, r1, r2, r3, r4, r5)     // Catch:{ UnsupportedEncodingException -> 0x0099 }
            com.immomo.momo.util.m r1 = r8.t     // Catch:{ UnsupportedEncodingException -> 0x00c7 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ UnsupportedEncodingException -> 0x00c7 }
            java.lang.String r5 = "request_token[BaseString]:"
            r2.<init>(r5)     // Catch:{ UnsupportedEncodingException -> 0x00c7 }
            java.lang.StringBuilder r2 = r2.append(r0)     // Catch:{ UnsupportedEncodingException -> 0x00c7 }
            java.lang.String r2 = r2.toString()     // Catch:{ UnsupportedEncodingException -> 0x00c7 }
            r1.a(r2)     // Catch:{ UnsupportedEncodingException -> 0x00c7 }
        L_0x002a:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r2 = r8.n
            java.lang.String r2 = java.lang.String.valueOf(r2)
            r1.<init>(r2)
            java.lang.String r2 = "&"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            java.lang.String r5 = com.immomo.momo.android.activity.plugin.ar.a(r0, r1)
            java.lang.String r2 = "https://api.twitter.com/oauth/request_token"
            r1 = r8
            java.lang.String r0 = r1.a(r2, r3, r4, r5, r6)
            com.immomo.momo.util.m r1 = r8.t
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "request_token[url]:"
            r2.<init>(r3)
            java.lang.StringBuilder r2 = r2.append(r0)
            java.lang.String r2 = r2.toString()
            r1.a(r2)
            org.apache.http.client.methods.HttpGet r1 = new org.apache.http.client.methods.HttpGet
            r1.<init>(r0)
            org.apache.http.impl.client.DefaultHttpClient r0 = new org.apache.http.impl.client.DefaultHttpClient
            r0.<init>()
            org.apache.http.HttpResponse r0 = r0.execute(r1)     // Catch:{ Exception -> 0x00bf }
            org.apache.http.HttpEntity r1 = r0.getEntity()     // Catch:{ Exception -> 0x00bf }
            java.lang.String r1 = org.apache.http.util.EntityUtils.toString(r1)     // Catch:{ Exception -> 0x00bf }
            com.immomo.momo.util.m r2 = r8.t     // Catch:{ Exception -> 0x00bf }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00bf }
            java.lang.String r4 = "request_token[result]:"
            r3.<init>(r4)     // Catch:{ Exception -> 0x00bf }
            java.lang.StringBuilder r3 = r3.append(r1)     // Catch:{ Exception -> 0x00bf }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x00bf }
            r2.a(r3)     // Catch:{ Exception -> 0x00bf }
            org.apache.http.StatusLine r2 = r0.getStatusLine()     // Catch:{ Exception -> 0x00bf }
            int r2 = r2.getStatusCode()     // Catch:{ Exception -> 0x00bf }
            r3 = 200(0xc8, float:2.8E-43)
            if (r2 != r3) goto L_0x00a2
            boolean r0 = r8.e(r1)     // Catch:{ Exception -> 0x00bf }
        L_0x0098:
            return r0
        L_0x0099:
            r0 = move-exception
            r1 = r0
            r0 = r7
        L_0x009c:
            com.immomo.momo.util.m r2 = r8.t
            r2.a(r1)
            goto L_0x002a
        L_0x00a2:
            com.immomo.momo.a.a r1 = new com.immomo.momo.a.a     // Catch:{ Exception -> 0x00bf }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00bf }
            java.lang.String r3 = "网络请求失败:"
            r2.<init>(r3)     // Catch:{ Exception -> 0x00bf }
            org.apache.http.StatusLine r0 = r0.getStatusLine()     // Catch:{ Exception -> 0x00bf }
            int r0 = r0.getStatusCode()     // Catch:{ Exception -> 0x00bf }
            java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ Exception -> 0x00bf }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x00bf }
            r1.<init>(r0)     // Catch:{ Exception -> 0x00bf }
            throw r1     // Catch:{ Exception -> 0x00bf }
        L_0x00bf:
            r0 = move-exception
            com.immomo.momo.util.m r1 = r8.t
            r1.a(r0)
            r0 = 0
            goto L_0x0098
        L_0x00c7:
            r1 = move-exception
            goto L_0x009c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.immomo.momo.android.activity.plugin.BindTwitterActivity.u():boolean");
    }

    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        super.a(bundle);
        setContentView((int) R.layout.activity_bind_api);
        g.q();
        this.j = (WebView) findViewById(R.id.web);
        this.j.getSettings().setJavaScriptEnabled(true);
        this.j.getSettings().setUserAgentString("Android");
        this.j.getSettings().setAppCacheEnabled(false);
        this.j.getSettings().setDatabaseEnabled(false);
        this.j.getSettings().setSavePassword(false);
        this.j.setWebViewClient(new aq(this, (byte) 0));
        this.h = (HeaderLayout) findViewById(R.id.layout_header);
        this.h.setTitleText("绑定豆瓣账号");
        this.k = (TextView) findViewById(R.id.header_stv_title);
        this.k.setFocusableInTouchMode(false);
        this.t.b();
        this.s = new an(this, this);
        this.s.execute(new Object[0]);
    }

    /* access modifiers changed from: protected */
    public final void d() {
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x00bd A[SYNTHETIC, Splitter:B:13:0x00bd] */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0092 A[Catch:{ Exception -> 0x00da }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean d(java.lang.String r8) {
        /*
            r7 = this;
            java.lang.String r3 = com.immomo.momo.android.activity.plugin.ar.b()
            java.lang.String r4 = com.immomo.momo.android.activity.plugin.ar.a()
            java.lang.String r6 = ""
            java.lang.String r0 = "GET"
            java.lang.String r1 = "https://api.twitter.com/oauth/access_token"
            java.lang.String r2 = r7.m     // Catch:{ UnsupportedEncodingException -> 0x00b3 }
            r5 = r8
            java.lang.String r0 = com.immomo.momo.android.activity.plugin.ar.a(r0, r1, r2, r3, r4, r5)     // Catch:{ UnsupportedEncodingException -> 0x00b3 }
            com.immomo.momo.util.m r1 = r7.t     // Catch:{ UnsupportedEncodingException -> 0x00e2 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ UnsupportedEncodingException -> 0x00e2 }
            java.lang.String r5 = "access_token[BaseString]:"
            r2.<init>(r5)     // Catch:{ UnsupportedEncodingException -> 0x00e2 }
            java.lang.StringBuilder r2 = r2.append(r0)     // Catch:{ UnsupportedEncodingException -> 0x00e2 }
            java.lang.String r2 = r2.toString()     // Catch:{ UnsupportedEncodingException -> 0x00e2 }
            r1.a(r2)     // Catch:{ UnsupportedEncodingException -> 0x00e2 }
        L_0x0029:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r2 = r7.n
            java.lang.String r2 = java.lang.String.valueOf(r2)
            r1.<init>(r2)
            java.lang.String r2 = "&"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = r7.p
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            com.immomo.momo.util.m r2 = r7.t
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            java.lang.String r6 = "access_token[signKey]:"
            r5.<init>(r6)
            java.lang.StringBuilder r5 = r5.append(r1)
            java.lang.String r5 = r5.toString()
            r2.a(r5)
            java.lang.String r5 = com.immomo.momo.android.activity.plugin.ar.a(r0, r1)
            java.lang.String r2 = "https://api.twitter.com/oauth/access_token"
            r1 = r7
            r6 = r8
            java.lang.String r0 = r1.a(r2, r3, r4, r5, r6)
            com.immomo.momo.util.m r1 = r7.t
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "access_token[url]:"
            r2.<init>(r3)
            java.lang.StringBuilder r2 = r2.append(r0)
            java.lang.String r2 = r2.toString()
            r1.a(r2)
            org.apache.http.client.methods.HttpGet r1 = new org.apache.http.client.methods.HttpGet
            r1.<init>(r0)
            org.apache.http.impl.client.DefaultHttpClient r0 = new org.apache.http.impl.client.DefaultHttpClient
            r0.<init>()
            org.apache.http.HttpResponse r0 = r0.execute(r1)     // Catch:{ Exception -> 0x00da }
            org.apache.http.StatusLine r1 = r0.getStatusLine()     // Catch:{ Exception -> 0x00da }
            int r1 = r1.getStatusCode()     // Catch:{ Exception -> 0x00da }
            r2 = 200(0xc8, float:2.8E-43)
            if (r1 != r2) goto L_0x00bd
            org.apache.http.HttpEntity r0 = r0.getEntity()     // Catch:{ Exception -> 0x00da }
            java.lang.String r0 = org.apache.http.util.EntityUtils.toString(r0)     // Catch:{ Exception -> 0x00da }
            com.immomo.momo.util.m r1 = r7.t     // Catch:{ Exception -> 0x00da }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00da }
            java.lang.String r3 = "getAccessToken responce : "
            r2.<init>(r3)     // Catch:{ Exception -> 0x00da }
            java.lang.StringBuilder r2 = r2.append(r0)     // Catch:{ Exception -> 0x00da }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x00da }
            r1.b(r2)     // Catch:{ Exception -> 0x00da }
            boolean r0 = r7.e(r0)     // Catch:{ Exception -> 0x00da }
        L_0x00b2:
            return r0
        L_0x00b3:
            r0 = move-exception
            r1 = r0
            r0 = r6
        L_0x00b6:
            com.immomo.momo.util.m r2 = r7.t
            r2.a(r1)
            goto L_0x0029
        L_0x00bd:
            com.immomo.momo.a.a r1 = new com.immomo.momo.a.a     // Catch:{ Exception -> 0x00da }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00da }
            java.lang.String r3 = "网络请求失败:"
            r2.<init>(r3)     // Catch:{ Exception -> 0x00da }
            org.apache.http.StatusLine r0 = r0.getStatusLine()     // Catch:{ Exception -> 0x00da }
            int r0 = r0.getStatusCode()     // Catch:{ Exception -> 0x00da }
            java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ Exception -> 0x00da }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x00da }
            r1.<init>(r0)     // Catch:{ Exception -> 0x00da }
            throw r1     // Catch:{ Exception -> 0x00da }
        L_0x00da:
            r0 = move-exception
            com.immomo.momo.util.m r1 = r7.t
            r1.a(r0)
            r0 = 0
            goto L_0x00b2
        L_0x00e2:
            r1 = move-exception
            goto L_0x00b6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.immomo.momo.android.activity.plugin.BindTwitterActivity.d(java.lang.String):boolean");
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.l = null;
        this.s.cancel(true);
        super.onDestroy();
    }
}
