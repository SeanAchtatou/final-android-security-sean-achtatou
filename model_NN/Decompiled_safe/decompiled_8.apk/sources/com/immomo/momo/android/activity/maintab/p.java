package com.immomo.momo.android.activity.maintab;

import android.location.Location;
import com.immomo.momo.R;
import com.immomo.momo.android.b.z;
import com.immomo.momo.util.ao;

final class p extends com.immomo.momo.android.b.p {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ l f1896a;

    p(l lVar) {
        this.f1896a = lVar;
    }

    public final void a(Location location, int i, int i2, int i3) {
        if (this.b) {
            if (z.a(location)) {
                this.f1896a.M.S = location.getLatitude();
                this.f1896a.M.T = location.getLongitude();
                this.f1896a.M.aH = i;
                this.f1896a.M.aI = i3;
                this.f1896a.M.a(System.currentTimeMillis());
                this.f1896a.T.a(this.f1896a.M);
                this.f1896a.V.post(new q(this));
                return;
            }
            this.f1896a.P();
            if (i2 == 104) {
                ao.g(R.string.errormsg_network_unfind);
            } else if (i2 == 105) {
                this.f1896a.V.post(new s(this.f1896a));
            } else {
                ao.g(R.string.errormsg_location_nearby_failed);
            }
        }
    }
}
