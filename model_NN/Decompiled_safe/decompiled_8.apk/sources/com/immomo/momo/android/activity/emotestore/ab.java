package com.immomo.momo.android.activity.emotestore;

import android.content.Intent;
import android.support.v4.b.a;
import android.view.View;
import android.widget.AdapterView;
import com.immomo.momo.android.activity.WebviewActivity;
import com.sina.sdk.api.message.InviteApi;
import org.json.JSONObject;

final class ab implements AdapterView.OnItemClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ EmotionSetTaskActivity f1282a;

    ab(EmotionSetTaskActivity emotionSetTaskActivity) {
        this.f1282a = emotionSetTaskActivity;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        JSONObject optJSONObject = this.f1282a.j.optJSONObject(i);
        if (optJSONObject != null && !a.a((CharSequence) optJSONObject.optString(InviteApi.KEY_URL))) {
            Intent intent = new Intent(this.f1282a.getApplicationContext(), WebviewActivity.class);
            intent.putExtra("webview_title", optJSONObject.optString("desc"));
            intent.putExtra("webview_url", optJSONObject.optString(InviteApi.KEY_URL));
            this.f1282a.startActivity(intent);
        }
    }
}
