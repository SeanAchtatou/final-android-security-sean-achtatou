package com.immomo.momo.android.activity.maintab;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import com.immomo.momo.android.activity.ContactNoticeListActivity;
import com.immomo.momo.android.activity.FriendDistanceActivity;
import com.immomo.momo.android.activity.message.ChatActivity;
import com.immomo.momo.android.activity.message.GroupChatActivity;
import com.immomo.momo.android.activity.message.HiSessionListActivity;
import com.immomo.momo.android.activity.message.MultiChatActivity;
import com.immomo.momo.service.bean.ax;
import com.immomo.momo.util.k;
import org.json.JSONObject;

final class bz implements AdapterView.OnItemClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ bh f1872a;

    bz(bh bhVar) {
        this.f1872a = bhVar;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        ax axVar = (ax) this.f1872a.X.getItem(i);
        int i2 = 0;
        if (axVar.o == 0) {
            bh bhVar = this.f1872a;
            Intent intent = new Intent(bh.H(), ChatActivity.class);
            intent.putExtra("remoteUserID", axVar.f3000a);
            this.f1872a.a(intent);
        } else if (axVar.o == 1) {
            bh bhVar2 = this.f1872a;
            this.f1872a.a(new Intent(bh.H(), HiSessionListActivity.class));
            i2 = 1;
        } else if (axVar.o == 2) {
            bh bhVar3 = this.f1872a;
            Intent intent2 = new Intent(bh.H(), GroupChatActivity.class);
            intent2.putExtra("remoteGroupID", axVar.f3000a);
            this.f1872a.a(intent2);
        } else if (axVar.o == 6) {
            bh bhVar4 = this.f1872a;
            Intent intent3 = new Intent(bh.H(), MultiChatActivity.class);
            intent3.putExtra("remoteDiscussID", axVar.f3000a);
            this.f1872a.a(intent3);
        } else if (axVar.o == 5) {
            i2 = 4;
            bh bhVar5 = this.f1872a;
            this.f1872a.a(new Intent(bh.H(), ContactNoticeListActivity.class));
        } else if (axVar.o == 8) {
            bh bhVar6 = this.f1872a;
            this.f1872a.a(new Intent(bh.H(), FriendDistanceActivity.class));
        } else if (axVar.o == 7) {
            bh bhVar7 = this.f1872a;
            Intent intent4 = new Intent(bh.H(), ChatActivity.class);
            intent4.putExtra("remoteUserID", axVar.f3000a);
            this.f1872a.a(intent4);
        }
        if (i2 != 0) {
            try {
                JSONObject jSONObject = new JSONObject();
                jSONObject.put("type", i2);
                jSONObject.put("number", axVar.f);
                new k("C", "C503", jSONObject).e();
            } catch (Exception e) {
            }
        }
    }
}
