package com.immomo.momo.android.activity.tieba;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import com.immomo.momo.R;
import com.immomo.momo.android.a.iz;
import com.immomo.momo.android.activity.ah;
import com.immomo.momo.android.activity.common.CommonShareActivity;
import com.immomo.momo.android.broadcast.d;
import com.immomo.momo.android.broadcast.x;
import com.immomo.momo.android.view.HandyTextView;
import com.immomo.momo.android.view.LoadingButton;
import com.immomo.momo.android.view.MomoRefreshListView;
import com.immomo.momo.android.view.a.at;
import com.immomo.momo.android.view.a.n;
import com.immomo.momo.android.view.a.o;
import com.immomo.momo.android.view.bi;
import com.immomo.momo.android.view.bu;
import com.immomo.momo.g;
import com.immomo.momo.service.ao;
import com.immomo.momo.service.bean.c.b;
import com.immomo.momo.util.j;
import com.immomo.momo.util.m;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class TiebaProfileActivity extends ah implements View.OnClickListener, AdapterView.OnItemClickListener, d, bu {
    private Button A = null;
    private bi B = null;
    private at C = null;
    private x D = null;
    private boolean E;
    Set h = new HashSet();
    int i = 0;
    /* access modifiers changed from: private */
    public Date j = null;
    /* access modifiers changed from: private */
    public String k = null;
    /* access modifiers changed from: private */
    public com.immomo.momo.service.bean.c.d l = null;
    /* access modifiers changed from: private */
    public ao m = null;
    /* access modifiers changed from: private */
    public MomoRefreshListView n = null;
    /* access modifiers changed from: private */
    public iz o = null;
    /* access modifiers changed from: private */
    public List p = null;
    /* access modifiers changed from: private */
    public List q = null;
    /* access modifiers changed from: private */
    public LoadingButton r = null;
    private View s = null;
    private ImageView t = null;
    private HandyTextView u = null;
    private HandyTextView v = null;
    private HandyTextView w = null;
    private HandyTextView x = null;
    private Button y = null;
    private Button z = null;

    public TiebaProfileActivity() {
        new m();
    }

    /* access modifiers changed from: private */
    public static void b(List list, List list2) {
        Iterator it = list2.iterator();
        while (it.hasNext()) {
            b bVar = (b) it.next();
            if (bVar.p) {
                list.add(bVar);
            }
        }
    }

    private static String c(int i2) {
        return i2 >= 10000 ? String.valueOf(i2 / 10000) + "万" : new StringBuilder(String.valueOf(i2)).toString();
    }

    static /* synthetic */ void l(TiebaProfileActivity tiebaProfileActivity) {
        Intent intent = new Intent(tiebaProfileActivity, TiebaManagerApplyActivity.class);
        intent.putExtra("apply_tieba_manager_tid", tiebaProfileActivity.k);
        tiebaProfileActivity.startActivity(intent);
    }

    static /* synthetic */ void m(TiebaProfileActivity tiebaProfileActivity) {
        if (tiebaProfileActivity.C == null || !tiebaProfileActivity.C.g()) {
            tiebaProfileActivity.C = new at(tiebaProfileActivity, tiebaProfileActivity.B, tiebaProfileActivity.l.m ? tiebaProfileActivity.l.l ? new String[]{"分享给好友", "退出陌陌吧"} : new String[]{"分享给好友", "申请吧主", "投诉", "退出陌陌吧"} : new String[]{"分享给好友", "加入陌陌吧", "投诉"});
            tiebaProfileActivity.C.a(at.b);
            tiebaProfileActivity.C.a(new ev(tiebaProfileActivity));
            tiebaProfileActivity.C.d();
        }
    }

    static /* synthetic */ void n(TiebaProfileActivity tiebaProfileActivity) {
        Intent intent = new Intent(tiebaProfileActivity.getApplicationContext(), CommonShareActivity.class);
        intent.putExtra("from_type", 3);
        intent.putExtra("from_id", tiebaProfileActivity.k);
        tiebaProfileActivity.startActivity(intent);
    }

    static /* synthetic */ void o(TiebaProfileActivity tiebaProfileActivity) {
        n nVar = new n(tiebaProfileActivity);
        nVar.setTitle("提示");
        nVar.a();
        if (tiebaProfileActivity.l.l) {
            nVar.a("退出陌陌吧后，你的吧主权限也会消失。此操作不可恢复，确定要继续吗?");
        } else {
            nVar.a("确定要退出陌陌吧吗?");
        }
        nVar.a(0, (int) R.string.dialog_btn_confim, new ew(tiebaProfileActivity));
        nVar.a(1, (int) R.string.dialog_btn_cancel, new ex());
        tiebaProfileActivity.a(nVar);
    }

    static /* synthetic */ void p(TiebaProfileActivity tiebaProfileActivity) {
        o oVar = new o(tiebaProfileActivity, (int) R.array.report_tieba_items);
        oVar.setTitle("投诉");
        oVar.a();
        oVar.a(new ey(tiebaProfileActivity));
        oVar.show();
    }

    /* access modifiers changed from: private */
    public void v() {
        if (this.l.m) {
            this.z.setVisibility(8);
        } else {
            this.z.setVisibility(0);
        }
        j.a(this.l, this.t, (ViewGroup) null);
        this.u.setText("成员 " + c(this.l.g));
        this.v.setText("话题 " + c(this.l.h));
        this.w.setText("今日话题 " + c(this.l.i));
        this.x.setText(this.l.j);
    }

    public final void a(Intent intent) {
        if (x.f2367a.equals(intent.getAction())) {
            if (this.k.equals(intent.getStringExtra("key_tiebaid"))) {
                this.E = true;
            }
        }
        if (x.b.equals(intent.getAction())) {
            String stringExtra = intent.getStringExtra("key_tiebaid");
            String stringExtra2 = intent.getStringExtra("key_pid");
            if (this.k.equals(stringExtra) && stringExtra2 != null) {
                for (int i2 = 0; i2 < this.o.getCount(); i2++) {
                    b d = this.o.getItem(i2);
                    if (d.f3017a.equalsIgnoreCase(stringExtra2)) {
                        b c = this.m.c(stringExtra2);
                        if (c != null) {
                            if (2 == c.o) {
                                if (d.l) {
                                    com.immomo.momo.service.bean.c.d dVar = this.l;
                                    dVar.i--;
                                }
                                this.o.c(i2);
                            } else {
                                d.p = c.p;
                                d.m = c.m;
                            }
                            this.o.notifyDataSetChanged();
                            return;
                        }
                        return;
                    }
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        super.a(bundle);
        setContentView((int) R.layout.activity_tiebaprofile);
        this.k = getIntent().getStringExtra("tiebaid");
        this.n = (MomoRefreshListView) findViewById(R.id.listitem_tieba);
        this.B = new bi(this).a((int) R.drawable.ic_topbar_more);
        a(this.B, new eu(this));
        this.n.setEnableLoadMoreFoolter(true);
        this.r = this.n.getFooterViewButton();
        this.s = g.o().inflate((int) R.layout.include_tiebaprofile_top, (ViewGroup) null);
        this.t = (ImageView) this.s.findViewById(R.id.tieba_photo);
        this.u = (HandyTextView) this.s.findViewById(R.id.tieba_joinnum);
        this.v = (HandyTextView) this.s.findViewById(R.id.tieba_tiesnum);
        this.w = (HandyTextView) this.s.findViewById(R.id.tieba_newtiesnum);
        this.x = (HandyTextView) this.s.findViewById(R.id.tieba_sign);
        this.y = (Button) this.s.findViewById(R.id.tieba_members);
        this.z = (Button) this.s.findViewById(R.id.tieba_join);
        this.A = (Button) this.s.findViewById(R.id.tieba_publish);
        this.n.addHeaderView(this.s);
        d();
        this.r.setOnProcessListener(new es(this));
        this.n.setOnItemClickListener(this);
        this.n.setOnPullToRefreshListener$42b903f6(this);
        this.n.setOnCancelListener$135502(new et(this));
        this.y.setOnClickListener(this);
        this.z.setOnClickListener(this);
        this.A.setOnClickListener(this);
        this.D = new x(this);
        this.D.a(this);
    }

    public final void b_() {
        this.n.setLoadingViewText(R.string.pull_to_refresh_refreshing_label);
        this.n.setLastFlushTime(this.j);
        this.h.clear();
        this.i = 0;
        b(new ez(this, this));
        b(new fa(this, this, this.i));
    }

    /* access modifiers changed from: protected */
    public final void d() {
        super.d();
        this.E = false;
        this.j = this.g.b("tieba_profile_lasttime_success");
        this.n.setLastFlushTime(this.j);
        this.m = new ao();
        this.l = this.m.a(this.k);
        if (this.l == null) {
            this.l = new com.immomo.momo.service.bean.c.d();
        } else {
            setTitle(this.l.b);
        }
        v();
        this.p = this.m.f(this.k);
        this.q = new ArrayList();
        b(this.q, this.p);
        List list = this.p;
        MomoRefreshListView momoRefreshListView = this.n;
        this.o = new iz(this, list);
        List list2 = this.q;
        MomoRefreshListView momoRefreshListView2 = this.n;
        new iz(this, list2);
        this.n.setAdapter((ListAdapter) this.o);
        if (this.o.getCount() < 20) {
            this.r.setVisibility(8);
        } else {
            this.r.setVisibility(0);
        }
        if (this.l.q) {
            this.l.q = false;
            this.m.b(this.k);
        }
    }

    /* access modifiers changed from: protected */
    public final void e() {
        super.e();
        this.n.l();
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tieba_members /*2131166474*/:
                Intent intent = new Intent(this, TiebaMemberListActivity.class);
                intent.putExtra("tiebaid", this.k);
                startActivity(intent);
                return;
            case R.id.tieba_join /*2131166475*/:
                b(new fb(this, this, 1));
                return;
            case R.id.tieba_publish /*2131166476*/:
                Intent intent2 = new Intent(this, PublishTieActivity.class);
                intent2.putExtra("tieba_id", this.k);
                intent2.putExtra("tieba_name", this.l.b);
                startActivity(intent2);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(this.D);
    }

    public void onItemClick(AdapterView adapterView, View view, int i2, long j2) {
        this.p.get(i2);
        this.m.d(((b) this.p.get(i2)).f3017a);
        Intent intent = new Intent(this, TieDetailActivity.class);
        intent.putExtra("key_tieid", ((b) this.p.get(i2)).f3017a);
        intent.putExtra("key_tietitle", ((b) this.p.get(i2)).d);
        startActivity(intent);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.E) {
            this.n.l();
            this.E = false;
        }
    }
}
