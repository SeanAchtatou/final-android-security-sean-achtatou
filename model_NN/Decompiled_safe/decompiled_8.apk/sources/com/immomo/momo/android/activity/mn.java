package com.immomo.momo.android.activity;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import com.immomo.momo.service.bean.bf;

final class mn implements AdapterView.OnItemClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ VisitorListActivity f2005a;

    mn(VisitorListActivity visitorListActivity) {
        this.f2005a = visitorListActivity;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        Intent intent = new Intent(this.f2005a.getApplicationContext(), OtherProfileActivity.class);
        intent.putExtra("momoid", ((bf) this.f2005a.j.getItem(i)).h);
        this.f2005a.startActivity(intent);
    }
}
