package com.immomo.momo.android.a.a;

import android.content.Intent;
import android.view.View;
import com.immomo.momo.android.activity.discuss.DiscussProfileActivity;
import com.immomo.momo.android.activity.maintab.bh;
import com.immomo.momo.service.bean.ax;

final class an implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ah f683a;
    private final /* synthetic */ ax b;

    an(ah ahVar, ax axVar) {
        this.f683a = ahVar;
        this.b = axVar;
    }

    public final void onClick(View view) {
        Intent intent = new Intent();
        bh unused = this.f683a.d;
        intent.setClass(bh.H(), DiscussProfileActivity.class);
        intent.putExtra("tag", "local");
        intent.putExtra("did", this.b.f3000a);
        this.f683a.d.a(intent);
    }
}
