package com.immomo.momo.android.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;
import com.immomo.momo.R;

public class TiebaHotWordView extends d {
    public TiebaHotWordView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public final /* synthetic */ View a(Object obj) {
        View inflate = inflate(getContext(), R.layout.listitem_tiebarecommend, null);
        ((TextView) inflate.findViewById(R.id.tv_name)).setText((String) obj);
        return inflate;
    }
}
