package com.immomo.momo.android.plugin.cropimage;

import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.RectF;

final class a implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ CropImageActivity f2580a;
    private float b = 1.0f;
    private Matrix c;

    a(CropImageActivity cropImageActivity) {
        this.f2580a = cropImageActivity;
    }

    static /* synthetic */ void a(a aVar) {
        int i;
        int i2;
        boolean z = false;
        k kVar = new k(aVar.f2580a.j);
        int width = aVar.f2580a.l.getWidth();
        int height = aVar.f2580a.l.getHeight();
        Rect rect = new Rect(0, 0, width, height);
        int min = (Math.min(width, height) * 4) / 5;
        if (aVar.f2580a.k.e == 0 || aVar.f2580a.k.f == 0) {
            i = min;
            i2 = min;
        } else if (aVar.f2580a.k.e > aVar.f2580a.k.f) {
            i = (aVar.f2580a.k.f * min) / aVar.f2580a.k.e;
            i2 = min;
        } else {
            i2 = (aVar.f2580a.k.e * min) / aVar.f2580a.k.f;
            i = min;
        }
        int i3 = (width - i2) / 2;
        int i4 = (height - i) / 2;
        RectF rectF = new RectF((float) i3, (float) i4, (float) (i2 + i3), (float) (i + i4));
        Matrix matrix = aVar.c;
        CropImageActivity cropImageActivity = aVar.f2580a;
        boolean b2 = CropImageActivity.b();
        if (!(aVar.f2580a.k.e == 0 || aVar.f2580a.k.f == 0)) {
            z = true;
        }
        kVar.a(matrix, rect, rectF, b2, z);
        aVar.f2580a.j.a(kVar);
    }

    public final void run() {
        this.c = this.f2580a.j.getImageMatrix();
        this.b = 1.0f / this.b;
        this.f2580a.g.post(new b(this));
    }
}
