package com.immomo.momo.android.map;

import android.text.InputFilter;
import android.text.Spanned;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.util.ao;
import java.io.UnsupportedEncodingException;

final class bf implements InputFilter {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ SelectSiteGoogleActivity f2485a;

    bf(SelectSiteGoogleActivity selectSiteGoogleActivity) {
        this.f2485a = selectSiteGoogleActivity;
    }

    public final CharSequence filter(CharSequence charSequence, int i, int i2, Spanned spanned, int i3, int i4) {
        try {
            if ((String.valueOf(this.f2485a.b.getText().toString().trim()) + charSequence.toString().trim()).getBytes("GBK").length > 20) {
                ao.a((CharSequence) "地点名称过长");
                return PoiTypeDef.All;
            }
        } catch (UnsupportedEncodingException e) {
            this.f2485a.k.a((Throwable) e);
        }
        return null;
    }
}
