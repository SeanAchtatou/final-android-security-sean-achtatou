package com.immomo.momo.android.map;

import android.view.View;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.view.a.n;

final class an implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ SelectSiteAMapActivity f2466a;
    private final /* synthetic */ ay b;
    private final /* synthetic */ n c;

    an(SelectSiteAMapActivity selectSiteAMapActivity, ay ayVar, n nVar) {
        this.f2466a = selectSiteAMapActivity;
        this.b = ayVar;
        this.c = nVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.android.map.SelectSiteAMapActivity.a(com.immomo.momo.android.map.SelectSiteAMapActivity, boolean):void
     arg types: [com.immomo.momo.android.map.SelectSiteAMapActivity, int]
     candidates:
      com.immomo.momo.android.map.SelectSiteAMapActivity.a(com.immomo.momo.android.map.SelectSiteAMapActivity, int):void
      com.immomo.momo.android.map.SelectSiteAMapActivity.a(com.immomo.momo.android.map.SelectSiteAMapActivity, android.location.Location):void
      com.immomo.momo.android.map.SelectSiteAMapActivity.a(com.immomo.momo.android.map.SelectSiteAMapActivity, com.immomo.momo.android.map.az):void
      com.immomo.momo.android.map.SelectSiteAMapActivity.a(com.immomo.momo.android.map.SelectSiteAMapActivity, boolean):void */
    public final void onClick(View view) {
        switch (view.getId()) {
            case R.id.layout_type_all /*2131166087*/:
                this.f2466a.y = true;
                this.f2466a.u = 0;
                break;
            case R.id.layout_type_department /*2131166088*/:
                this.f2466a.y = false;
                this.f2466a.u = 1;
                break;
            case R.id.layout_type_office /*2131166089*/:
                this.f2466a.y = false;
                this.f2466a.u = 2;
                break;
            case R.id.layout_type_school /*2131166090*/:
                this.f2466a.y = false;
                this.f2466a.u = 3;
                break;
            case R.id.layout_type_entertainment /*2131166091*/:
                this.f2466a.y = false;
                this.f2466a.u = 4;
                break;
        }
        this.f2466a.v.setText(this.f2466a.d());
        this.f2466a.c.setText(PoiTypeDef.All);
        this.f2466a.b.removeFooterView(this.f2466a.l);
        this.b.a(false);
        this.c.dismiss();
    }
}
