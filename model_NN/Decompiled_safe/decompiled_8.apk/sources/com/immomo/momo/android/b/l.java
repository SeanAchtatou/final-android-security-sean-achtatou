package com.immomo.momo.android.b;

import android.location.Location;
import android.os.AsyncTask;
import com.amap.mapapi.location.LocationManagerProxy;
import com.immomo.momo.protocol.a.d;
import com.immomo.momo.util.m;
import mm.purchasesdk.PurchaseCode;

public final class l extends AsyncTask {

    /* renamed from: a  reason: collision with root package name */
    private static m f2330a = new m("ConvertGPSTask");
    private p b;
    private int c;

    public l(p pVar, int i) {
        this.b = pVar;
        this.c = i;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public String doInBackground(Location... locationArr) {
        try {
            Location location = new Location(LocationManagerProxy.GPS_PROVIDER);
            int a2 = d.a(location, locationArr[0].getLatitude(), locationArr[0].getLongitude(), locationArr[0].getAccuracy());
            this.b.a(location, a2, 100, this.c);
            f2330a.a((Object) ("convertGPS location finished. loctype: " + a2));
        } catch (Exception e) {
            f2330a.a((Throwable) e);
            this.b.a(null, 0, PurchaseCode.QUERY_OK, this.c);
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
        super.onPostExecute((String) obj);
    }
}
