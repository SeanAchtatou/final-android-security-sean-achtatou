package com.immomo.momo.android.activity.event;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import com.immomo.momo.R;
import com.immomo.momo.android.a.ca;
import com.immomo.momo.android.activity.lh;
import com.immomo.momo.android.view.HeaderLayout;
import com.immomo.momo.android.view.LoadingButton;
import com.immomo.momo.android.view.MomoRefreshListView;
import com.immomo.momo.android.view.bl;
import com.immomo.momo.android.view.bu;
import com.immomo.momo.android.view.cv;
import com.immomo.momo.g;
import com.immomo.momo.service.af;
import com.immomo.momo.service.r;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class e extends lh implements AdapterView.OnItemClickListener, bl, bu, cv {
    /* access modifiers changed from: private */
    public MomoRefreshListView O = null;
    /* access modifiers changed from: private */
    public ca P = null;
    /* access modifiers changed from: private */
    public List Q = null;
    /* access modifiers changed from: private */
    public r R = null;
    /* access modifiers changed from: private */
    public LoadingButton S = null;
    /* access modifiers changed from: private */
    public f T;
    /* access modifiers changed from: private */
    public f U = null;

    /* access modifiers changed from: protected */
    public final int B() {
        return R.layout.activity_layoutlist;
    }

    /* access modifiers changed from: protected */
    public final void C() {
        this.O = (MomoRefreshListView) c((int) R.id.listview);
        this.O.setFastScrollEnabled(false);
        this.O.setEnableLoadMoreFoolter(true);
        this.S = this.O.getFooterViewButton();
        this.S.setOnProcessListener(this);
        this.O.setLastFlushTime(new Date());
        this.O.setOnPullToRefreshListener$42b903f6(this);
        this.O.setOnCancelListener$135502(this);
        this.O.setOnItemClickListener(this);
        this.O.addHeaderView(g.o().inflate((int) R.layout.listitem_blank, (ViewGroup) null));
        this.O.a(g.o().inflate((int) R.layout.include_mycomment_listempty, (ViewGroup) null));
        this.O.setListPaddingBottom(-3);
        this.S.setVisibility(8);
    }

    public final void a(Context context, HeaderLayout headerLayout) {
        super.a(context, headerLayout);
        headerLayout.setTitleText("评论我的");
    }

    public final void ai() {
        this.O.i();
    }

    public final void aj() {
        super.aj();
        this.R = new r();
        this.Q = this.R.a();
        if (this.Q == null) {
            this.Q = new ArrayList();
        }
        this.P = new ca(c(), this.Q, this.O);
        this.O.setAdapter((ListAdapter) this.P);
        if (this.P.getCount() < 20) {
            this.S.setVisibility(8);
        } else {
            this.S.setVisibility(0);
        }
        this.O.setLastFlushTime(g.r().b("prf_time_event_feed_my_comments"));
        if (this.P.isEmpty() || af.c().o() > 0) {
            this.O.l();
        }
    }

    public final void b_() {
        this.O.setLoadingVisible(true);
        if (this.T != null && !this.T.isCancelled()) {
            this.T.cancel(true);
        }
        this.O.setLoadingViewText(R.string.pull_to_refresh_refreshing_label);
        this.T = new f(this, c(), true);
        this.T.execute(new Object[0]);
    }

    /* access modifiers changed from: protected */
    public final void g(Bundle bundle) {
        aj();
    }

    public void onItemClick(AdapterView adapterView, View view, int i, long j) {
    }

    public final void p() {
        super.p();
        if (this.U != null) {
            this.U.cancel(true);
        }
        if (this.T != null) {
            this.T.cancel(true);
        }
    }

    public final void u() {
        this.S.f();
        if (this.U != null) {
            this.U.cancel(true);
        }
        this.U = new f(this, c(), false);
        this.U.execute(new Object[0]);
    }

    public final void v() {
        this.O.n();
        this.S.e();
    }
}
