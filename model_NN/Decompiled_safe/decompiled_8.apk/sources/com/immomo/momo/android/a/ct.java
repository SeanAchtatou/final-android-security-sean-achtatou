package com.immomo.momo.android.a;

import android.view.View;
import android.widget.AdapterView;
import com.immomo.momo.R;
import com.immomo.momo.android.view.a.n;
import com.immomo.momo.android.view.a.o;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.ab;
import com.immomo.momo.util.ao;

final class ct implements AdapterView.OnItemClickListener {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public /* synthetic */ cs f762a;
    private final /* synthetic */ String[] b;
    private final /* synthetic */ ab c;

    ct(cs csVar, String[] strArr, ab abVar) {
        this.f762a = csVar;
        this.b = strArr;
        this.c = abVar;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        if ("复制文本".equals(this.b[i])) {
            g.a((CharSequence) this.c.b());
            ao.b("已成功复制文本");
        } else if ("删除".equals(this.b[i])) {
            n.a(this.f762a.d(), "确定要删除该留言？", new cu(this, this.c)).show();
        } else if ("举报".equals(this.b[i])) {
            o oVar = new o(this.f762a.e, (int) R.array.reportfeed_items);
            oVar.setTitle((int) R.string.report_dialog_title_feed);
            oVar.a();
            oVar.a(new cv(this, this.c));
            oVar.show();
        }
    }
}
