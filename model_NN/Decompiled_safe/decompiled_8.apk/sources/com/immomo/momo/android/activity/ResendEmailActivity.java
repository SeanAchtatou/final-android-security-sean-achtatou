package com.immomo.momo.android.activity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.android.view.HeaderLayout;

public class ResendEmailActivity extends ah {
    private HeaderLayout h = null;
    private TextView i;
    private Button j;
    /* access modifiers changed from: private */
    public String k;
    /* access modifiers changed from: private */
    public String l;

    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        super.a(bundle);
        setContentView((int) R.layout.activity_resend_checkemail);
        d();
        this.h = (HeaderLayout) findViewById(R.id.layout_header);
        this.h.setTitleText("绑定邮箱");
        this.i = (TextView) findViewById(R.id.tv_register_email);
        this.j = (Button) findViewById(R.id.send_button);
        this.j.setOnClickListener(new ju(this));
        this.i.setText(this.k);
    }

    /* access modifiers changed from: protected */
    public final void d() {
        this.k = getIntent().getStringExtra("email");
        this.l = getIntent().getStringExtra("password");
    }
}
