package com.immomo.momo.android.view.a;

import android.graphics.RectF;

final class c implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ a f2692a;

    c(a aVar) {
        this.f2692a = aVar;
    }

    public final void run() {
        int[] iArr = new int[2];
        this.f2692a.b.getLocationOnScreen(iArr);
        int i = iArr[0];
        int i2 = iArr[1];
        this.f2692a.h = new RectF((float) i, (float) i2, (float) (i + this.f2692a.b.getWidth()), (float) (i2 + this.f2692a.b.getHeight()));
    }
}
