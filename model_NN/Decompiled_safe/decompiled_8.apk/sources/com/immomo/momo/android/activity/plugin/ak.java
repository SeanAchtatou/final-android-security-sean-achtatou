package com.immomo.momo.android.activity.plugin;

import a.a.a.a;
import a.a.a.b;
import android.content.Context;
import android.os.Message;
import com.immomo.momo.R;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.service.bean.c;
import com.immomo.momo.util.ao;

final class ak extends d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ BindTwitter2Activity f2038a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ak(BindTwitter2Activity bindTwitter2Activity, Context context) {
        super(context);
        this.f2038a = bindTwitter2Activity;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        this.f2038a.q = new c();
        this.f2038a.q.a("gBxG1ywhkOWGdSJy67zqAg");
        this.f2038a.q.b("DZPi4Nlkl4NtoNkzOIyF81WfCxeYxlcNeaeLbzCTgZw");
        this.b.b((Object) "get apikey");
        if (this.f2038a.q != null) {
            this.f2038a.j = new a(this.f2038a.q.f3015a, this.f2038a.q.b);
            this.f2038a.k = new b("http://api.twitter.com/oauth/request_token", "http://api.twitter.com/oauth/access_token", "http://api.twitter.com/oauth/authorize");
            String a2 = this.f2038a.k.a(this.f2038a.j, this.f2038a.l);
            this.b.b((Object) ("get authUrl : " + a2));
            Message message = new Message();
            message.what = 100;
            message.obj = a2;
            this.f2038a.s.sendMessage(message);
            return true;
        }
        ao.g(R.string.errormsg_server);
        return false;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.f2038a.o = new v(this.f2038a, "正在请求数据，请稍候...");
        this.f2038a.o.setOnCancelListener(new al(this));
        this.f2038a.o.show();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        if (this.f2038a.o != null && this.f2038a.o.isShowing()) {
            this.f2038a.o.dismiss();
        }
    }
}
