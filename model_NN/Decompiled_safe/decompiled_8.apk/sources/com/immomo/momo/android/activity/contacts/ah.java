package com.immomo.momo.android.activity.contacts;

import android.content.Context;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.a.a;
import com.immomo.momo.protocol.a.w;

final class ah extends d {

    /* renamed from: a  reason: collision with root package name */
    private a f1141a;
    private v c = null;
    private String d = PoiTypeDef.All;
    private String e = PoiTypeDef.All;
    private int f = 0;
    private int g = 0;
    private /* synthetic */ ContactPeopleActivity h;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ah(ContactPeopleActivity contactPeopleActivity, Context context, String str, String str2, int i, int i2) {
        super(context);
        this.h = contactPeopleActivity;
        this.d = str;
        this.e = str2;
        this.f = i;
        this.g = i2;
    }

    /* access modifiers changed from: protected */
    public final Object a(Object... objArr) {
        this.f1141a = w.a().e(this.d, this.e);
        return null;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
        this.c = new v(this.h);
        this.c.a("请求提交中...");
        this.c.setCancelable(true);
        this.c.setOnCancelListener(new ai(this));
        this.h.a(this.c);
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        super.a(exc);
    }

    /* access modifiers changed from: protected */
    public final void a(Object obj) {
        if (this.f1141a.b) {
            this.h.c(this.e);
        } else {
            a(this.f1141a.f2880a);
        }
        this.h.i.a(this.f, this.g);
    }

    /* access modifiers changed from: protected */
    public final void b() {
        super.b();
        this.h.p();
    }
}
