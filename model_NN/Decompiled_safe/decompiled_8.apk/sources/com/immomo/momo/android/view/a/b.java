package com.immomo.momo.android.view.a;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

final class b extends Handler {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ a f2680a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    b(a aVar, Looper looper) {
        super(looper);
        this.f2680a = aVar;
    }

    public final void handleMessage(Message message) {
        if (message.what == 146) {
            a aVar = this.f2680a;
            aVar.i = aVar.i + 1;
            if (this.f2680a.i == 50) {
                this.f2680a.f.setTextColor(-65536);
            }
            this.f2680a.f.setText(String.valueOf(this.f2680a.i) + " ''");
            if (this.f2680a.i < 60) {
                sendEmptyMessageDelayed(146, 1000);
            } else if (this.f2680a.k != null) {
                this.f2680a.k.a();
                this.f2680a.e();
            }
        }
    }
}
