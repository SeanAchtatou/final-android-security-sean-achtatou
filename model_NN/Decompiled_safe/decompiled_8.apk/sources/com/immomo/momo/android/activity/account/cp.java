package com.immomo.momo.android.activity.account;

import android.content.Context;
import com.immomo.momo.R;
import com.immomo.momo.a.a;
import com.immomo.momo.a.x;
import com.immomo.momo.android.c.b;
import com.immomo.momo.protocol.a.w;
import org.json.JSONException;

final class cp extends b {
    /* access modifiers changed from: private */
    public /* synthetic */ ck c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public cp(ck ckVar, Context context) {
        super(context);
        this.c = ckVar;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        return w.a().a(this.c.c.c, this.c.c.b, this.c.h.s);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
        this.f2379a.setCanceledOnTouchOutside(false);
        this.f2379a.setOnCancelListener(new cq(this));
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        this.b.a((Throwable) exc);
        if (exc instanceof a) {
            a(exc.getMessage());
        } else if (exc instanceof JSONException) {
            a((int) R.string.errormsg_dataerror);
        } else if (exc instanceof x) {
            a((int) R.string.errormsg_dataerror);
        } else {
            a((int) R.string.errormsg_server);
        }
        this.c.d();
    }

    /* access modifiers changed from: protected */
    public final void a(Object obj) {
        this.c.h.p = ((String[]) obj)[0];
        this.c.h.q = ((String[]) obj)[1];
        this.c.g();
    }

    /* access modifiers changed from: protected */
    public final String c() {
        return "正在读取注册配置，请稍候";
    }
}
