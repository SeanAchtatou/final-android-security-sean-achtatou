package com.immomo.momo.android.activity.group;

import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.a.a.f.b;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.ImageFactoryActivity;
import com.immomo.momo.android.activity.MulImagePickerActivity;
import com.immomo.momo.android.activity.ah;
import com.immomo.momo.android.c.i;
import com.immomo.momo.android.view.EmoteInputView;
import com.immomo.momo.android.view.MEmoteEditeText;
import com.immomo.momo.android.view.MGifImageView;
import com.immomo.momo.android.view.PublishButton;
import com.immomo.momo.android.view.PublishSelectPhotoView;
import com.immomo.momo.android.view.ResizeListenerLayout;
import com.immomo.momo.android.view.a.n;
import com.immomo.momo.android.view.au;
import com.immomo.momo.android.view.bi;
import com.immomo.momo.android.view.cs;
import com.immomo.momo.android.view.f;
import com.immomo.momo.g;
import com.immomo.momo.plugin.b.a;
import com.immomo.momo.protocol.a.o;
import com.immomo.momo.service.bean.a.e;
import com.immomo.momo.service.bean.av;
import com.immomo.momo.service.bean.q;
import com.immomo.momo.service.l;
import com.immomo.momo.service.m;
import com.immomo.momo.service.v;
import com.immomo.momo.util.ao;
import com.immomo.momo.util.h;
import com.immomo.momo.util.j;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import mm.purchasesdk.PurchaseCode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class PublishGroupFeedActivity extends ah implements View.OnClickListener, View.OnTouchListener, cs, f, l {
    /* access modifiers changed from: private */
    public au A;
    /* access modifiers changed from: private */
    public HashMap B = new HashMap();
    private HashMap C = new HashMap();
    private HashMap D = new HashMap();
    private String E = PoiTypeDef.All;
    private m F = new m();
    private String G = PoiTypeDef.All;
    private File H = null;
    private File I = null;
    /* access modifiers changed from: private */
    public a J = null;
    private Bitmap K = null;
    private int L = 0;
    protected boolean h = false;
    /* access modifiers changed from: private */
    public String i;
    private e j;
    /* access modifiers changed from: private */
    public MEmoteEditeText k = null;
    /* access modifiers changed from: private */
    public TextView l = null;
    private ResizeListenerLayout m = null;
    private boolean n = false;
    /* access modifiers changed from: private */
    public int o = 0;
    /* access modifiers changed from: private */
    public int p;
    /* access modifiers changed from: private */
    public EmoteInputView q = null;
    /* access modifiers changed from: private */
    public Handler r = new Handler();
    private MGifImageView s;
    /* access modifiers changed from: private */
    public PublishSelectPhotoView t;
    private View u;
    private View v;
    private View w;
    /* access modifiers changed from: private */
    public PublishButton x;
    /* access modifiers changed from: private */
    public PublishButton y;
    private TextView z;

    static {
        "http://m.immomo.com/inc/android/tieba/agreement.html?v=" + g.z();
    }

    private void A() {
        this.u.setVisibility(8);
    }

    private void B() {
        this.u.setVisibility(0);
        this.w.setVisibility(8);
    }

    private void C() {
        this.v.setVisibility(8);
        this.x.setSelected(false);
        this.y.setSelected(false);
    }

    private void D() {
        this.w.setVisibility(8);
    }

    private void E() {
        this.o = 0;
        findViewById(R.id.btn_localphoto).setEnabled(true);
        findViewById(R.id.btn_camera).setEnabled(true);
        findViewById(R.id.btn_emote).setEnabled(true);
        findViewById(R.id.layout_tip).setVisibility(0);
    }

    private ArrayList F() {
        ArrayList arrayList = new ArrayList();
        if (this.t.getDatalist() != null) {
            int i2 = 0;
            while (true) {
                int i3 = i2;
                if (i3 >= this.t.getDatalist().size()) {
                    break;
                }
                arrayList.add(((av) this.t.getDatalist().get(i3)).f2998a);
                i2 = i3 + 1;
            }
        }
        return arrayList;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.h.a(java.lang.String, java.lang.String, int, boolean):void
     arg types: [java.lang.String, java.lang.String, int, int]
     candidates:
      com.immomo.momo.util.h.a(java.lang.String, android.graphics.Bitmap, int, boolean):java.io.File
      com.immomo.momo.util.h.a(java.lang.String, java.lang.String, int, boolean):void */
    private void a(e eVar) {
        try {
            if (this.o == 2) {
                int f = eVar.f();
                for (int i2 = 0; i2 < f; i2++) {
                    String str = (String) this.D.get("photo_" + i2);
                    this.e.b((Object) ("asdf feed.getImages()[i]=" + eVar.g()[i2] + ", oldname=" + str));
                    if (!android.support.v4.b.a.a((CharSequence) str)) {
                        h.a(str, eVar.g()[i2], 16, false);
                    }
                }
            }
        } catch (Throwable th) {
            this.e.a(th);
        }
    }

    private void a(List list) {
        ArrayList arrayList = new ArrayList();
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= list.size()) {
                this.t.a();
                this.t.a(arrayList);
                this.t.setData(this.t.getDatalist());
                return;
            }
            av avVar = new av();
            if (!android.support.v4.b.a.a((CharSequence) list.get(i3))) {
                avVar.f2998a = (String) list.get(i3);
                av avVar2 = (av) this.B.get(avVar.f2998a);
                if (avVar2 == null) {
                    File file = new File(avVar.f2998a);
                    if (file.exists()) {
                        Bitmap a2 = android.support.v4.b.a.a(file, (int) PurchaseCode.LOADCHANNEL_ERR, (int) PurchaseCode.LOADCHANNEL_ERR);
                        if (a2 != null) {
                            avVar.c = a2;
                            j.a(avVar.f2998a, a2);
                        }
                        avVar.b = file;
                        this.B.put(avVar.f2998a, avVar);
                        avVar2 = avVar;
                    } else {
                        avVar2 = null;
                    }
                }
                if (avVar2 != null) {
                    arrayList.add(avVar2);
                }
            }
            i2 = i3 + 1;
        }
    }

    private void c(int i2) {
        u();
        this.q.setEmoteFlag(i2);
        if (this.h) {
            this.r.postDelayed(new ep(this), 300);
        } else {
            this.q.b();
        }
    }

    static /* synthetic */ void c(PublishGroupFeedActivity publishGroupFeedActivity) {
        publishGroupFeedActivity.C.clear();
        publishGroupFeedActivity.D.clear();
        try {
            JSONArray jSONArray = new JSONArray();
            if (publishGroupFeedActivity.t.getDatalist() != null) {
                int i2 = 0;
                while (true) {
                    int i3 = i2;
                    if (i3 >= publishGroupFeedActivity.t.getDatalist().size()) {
                        publishGroupFeedActivity.E = jSONArray.toString();
                        return;
                    }
                    JSONObject jSONObject = new JSONObject();
                    jSONObject.put("upload", "NO");
                    jSONObject.put("key", "photo_" + i3);
                    publishGroupFeedActivity.C.put("photo_" + i3, ((av) publishGroupFeedActivity.t.getDatalist().get(i3)).b);
                    jSONArray.put(jSONObject);
                    i2 = i3 + 1;
                }
            }
        } catch (Exception e) {
            publishGroupFeedActivity.e.a((Throwable) e);
        }
    }

    /* access modifiers changed from: private */
    public void d(int i2) {
        if (i2 <= 0) {
            this.z.setVisibility(8);
            return;
        }
        this.z.setVisibility(0);
        this.z.setText(new StringBuilder().append(i2).toString());
    }

    static /* synthetic */ void d(PublishGroupFeedActivity publishGroupFeedActivity) {
        publishGroupFeedActivity.F.e = publishGroupFeedActivity.k.getText().toString().trim();
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("selectmode", publishGroupFeedActivity.o);
            jSONObject.put("gid", publishGroupFeedActivity.i);
            jSONObject.put("content", publishGroupFeedActivity.k.getText().toString().trim());
            jSONObject.put("emotionbody", publishGroupFeedActivity.J == null ? PoiTypeDef.All : publishGroupFeedActivity.J.toString());
            jSONObject.put("pathlist", android.support.v4.b.a.a(publishGroupFeedActivity.F(), ","));
            publishGroupFeedActivity.F.f3050a = jSONObject.toString();
        } catch (JSONException e) {
            publishGroupFeedActivity.e.a((Throwable) e);
        }
    }

    static /* synthetic */ boolean i(PublishGroupFeedActivity publishGroupFeedActivity) {
        String trim = publishGroupFeedActivity.k.getText().toString().trim();
        if (trim.length() > 120) {
            publishGroupFeedActivity.a((CharSequence) "内容不能超过120个字");
            return false;
        } else if (!android.support.v4.b.a.a((CharSequence) trim)) {
            return true;
        } else {
            publishGroupFeedActivity.a((CharSequence) "内容不能为空");
            return false;
        }
    }

    static /* synthetic */ void u(PublishGroupFeedActivity publishGroupFeedActivity) {
        publishGroupFeedActivity.v.setVisibility(0);
        if (publishGroupFeedActivity.o == 0) {
            publishGroupFeedActivity.u.setVisibility(8);
            publishGroupFeedActivity.w.setVisibility(8);
        } else if (publishGroupFeedActivity.o == 1) {
            publishGroupFeedActivity.u.setVisibility(0);
            publishGroupFeedActivity.w.setVisibility(8);
            publishGroupFeedActivity.z();
        } else if (publishGroupFeedActivity.o == 2) {
            publishGroupFeedActivity.u.setVisibility(8);
            publishGroupFeedActivity.w.setVisibility(0);
        }
    }

    private void y() {
        Display defaultDisplay = getWindowManager().getDefaultDisplay();
        defaultDisplay.getWidth();
        this.p = defaultDisplay.getHeight();
    }

    /* access modifiers changed from: private */
    public void z() {
        findViewById(R.id.btn_localphoto).setEnabled(false);
        findViewById(R.id.btn_camera).setEnabled(false);
        findViewById(R.id.btn_emote).setEnabled(true);
        findViewById(R.id.layout_tip).setVisibility(8);
        B();
        D();
        this.q.a();
        B();
        this.s.setAlt(this.J.g());
        if (this.A != null) {
            this.A.l();
            this.A = null;
        }
        String g = this.J.g();
        String h2 = this.J.h();
        MGifImageView mGifImageView = this.s;
        boolean endsWith = g.endsWith(".gif");
        if (this.A == null) {
            File a2 = q.a(g, h2);
            this.e.b((Object) ("emoteFile:" + (a2 == null ? "null" : a2.getAbsolutePath())));
            if (a2 == null || !a2.exists()) {
                new i(g, h2, new en(this, mGifImageView, endsWith)).a();
                return;
            }
            this.A = new au(endsWith ? 1 : 2);
            this.A.a(a2, mGifImageView);
            this.A.b(20);
            mGifImageView.setGifDecoder(this.A);
            return;
        }
        mGifImageView.setGifDecoder(this.A);
        if ((this.A.g() == 4 || this.A.g() == 2) && this.A.f() != null && this.A.f().exists()) {
            this.A.a(this.A.f(), mGifImageView);
        }
    }

    public final void a(int i2, View view) {
        Uri fromFile;
        if (this.t.getDatalist() != null && i2 < this.t.getDatalist().size() && (fromFile = Uri.fromFile(((av) this.t.getDatalist().get(i2)).b)) != null) {
            this.L = i2;
            Intent intent = new Intent(this, ImageFactoryActivity.class);
            intent.setData(fromFile);
            intent.putExtra("minsize", 320);
            intent.putExtra("process_model", "filter");
            intent.putExtra("maxwidth", 720);
            intent.putExtra("maxheight", 3000);
            this.I = new File(com.immomo.momo.a.g(), "temp_" + b.a() + ".jpg_");
            intent.putExtra("outputFilePath", this.I.getAbsolutePath());
            startActivityForResult(intent, 105);
        }
    }

    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        String[] b;
        List asList;
        List asList2;
        super.a(bundle);
        setContentView((int) R.layout.activity_publish_groupfeed);
        y();
        this.m = (ResizeListenerLayout) findViewById(R.id.rootlayout);
        this.k = (MEmoteEditeText) findViewById(R.id.signeditor_tv_text);
        this.l = (TextView) findViewById(R.id.tv_textcount);
        this.q = (EmoteInputView) findViewById(R.id.emoteview);
        this.q.setEditText(this.k);
        this.s = (MGifImageView) findViewById(R.id.iv_selected_emote);
        this.v = findViewById(R.id.layout_selected);
        this.u = findViewById(R.id.layout_selected_emote);
        this.w = findViewById(R.id.layout_selected_photo);
        this.t = (PublishSelectPhotoView) findViewById(R.id.list_selectphotos);
        this.x = (PublishButton) findViewById(R.id.btn_selectpic);
        this.y = (PublishButton) findViewById(R.id.btn_selectemote);
        this.x.setIcon(R.drawable.ic_publish_selectpic);
        this.y.setIcon(R.drawable.ic_publish_selectemote);
        this.z = (TextView) findViewById(R.id.tv_selectpic_count);
        this.x.setSelected(true);
        this.y.setSelected(false);
        this.k.setOnTouchListener(this);
        this.k.addTextChangedListener(new ej(this));
        bi a2 = new bi(getApplicationContext()).a((int) R.drawable.ic_topbar_confirm_white);
        a2.setMarginRight(8);
        a2.setBackgroundResource(R.drawable.bg_header_submit);
        m().a(a2, new ek(this));
        this.m.setOnResizeListener(new el(this));
        this.q.setOnEmoteSelectedListener(new em(this));
        findViewById(R.id.iv_delete_emote).setOnClickListener(this);
        findViewById(R.id.btn_localphoto).setOnClickListener(this);
        findViewById(R.id.btn_camera).setOnClickListener(this);
        findViewById(R.id.btn_emote).setOnClickListener(this);
        this.t.setOnMomoGridViewItemClickListener(this);
        this.t.setRefreshListener(this);
        this.x.setOnClickListener(this);
        this.y.setOnClickListener(this);
        if (getIntent().getExtras().containsKey("ddraft")) {
            this.F.b = getIntent().getIntExtra("ddraftid", 0);
            try {
                JSONObject jSONObject = new JSONObject(getIntent().getStringExtra("ddraft"));
                this.k.setText(jSONObject.optString("content", PoiTypeDef.All));
                this.k.setSelection(this.k.getText().toString().length());
                this.o = jSONObject.optInt("selectmode", 0);
                this.i = jSONObject.optString("gid", PoiTypeDef.All);
                if (this.o == 1 && !android.support.v4.b.a.a((CharSequence) jSONObject.optString("emotionbody", PoiTypeDef.All))) {
                    this.J = new a(jSONObject.optString("emotionbody", PoiTypeDef.All));
                    d(1);
                }
                String[] b2 = android.support.v4.b.a.b(jSONObject.optString("pathlist", PoiTypeDef.All), ",");
                if (b2 != null && this.o == 2 && (asList2 = Arrays.asList(b2)) != null) {
                    b(new es(this, this, asList2));
                }
            } catch (JSONException e) {
                this.e.a((Throwable) e);
            }
        } else {
            if (bundle == null || !bundle.getBoolean("from_saveinstance", false)) {
                this.i = getIntent().getStringExtra("gid");
            } else {
                this.i = bundle.getString("gid");
                String str = bundle.get("save_tiecontent") == null ? PoiTypeDef.All : (String) bundle.get("save_tiecontent");
                if (!android.support.v4.b.a.a((CharSequence) str)) {
                    this.k.setText(str);
                    this.k.setSelection(str.length());
                }
                if (bundle.containsKey("camera_filename")) {
                    this.G = bundle.getString("camera_filename");
                }
                if (bundle.containsKey("camera_filepath")) {
                    this.H = new File(bundle.getString("camera_filepath"));
                }
                if (bundle.containsKey("local_filepath")) {
                    this.I = new File(bundle.getString("local_filepath"));
                }
                this.L = bundle.getInt("posFilter");
                this.o = bundle.getInt("selectMode");
                if (this.o == 1 && bundle.get("emotionbody") != null) {
                    this.J = new a((String) bundle.get("emotionbody"));
                    d(1);
                } else if (!(this.o != 2 || bundle.get("pathlist") == null || (b = android.support.v4.b.a.b((String) bundle.get("pathlist"), ",")) == null || (asList = Arrays.asList(b)) == null)) {
                    a(asList);
                }
            }
            if (android.support.v4.b.a.a((CharSequence) this.i)) {
                a((CharSequence) "没有指定的群ID");
                finish();
                return;
            }
            m().setTitleText("发布群留言");
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.h.a(java.lang.String, android.graphics.Bitmap, int, boolean):java.io.File
     arg types: [java.lang.String, android.graphics.Bitmap, int, int]
     candidates:
      com.immomo.momo.util.h.a(java.lang.String, java.lang.String, int, boolean):void
      com.immomo.momo.util.h.a(java.lang.String, android.graphics.Bitmap, int, boolean):java.io.File */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.b.a.a(android.graphics.Bitmap, float, boolean):android.graphics.Bitmap
     arg types: [android.graphics.Bitmap, int, int]
     candidates:
      android.support.v4.b.a.a(android.graphics.Bitmap, int, int):android.graphics.Bitmap
      android.support.v4.b.a.a(java.io.File, int, int):android.graphics.Bitmap
      android.support.v4.b.a.a(java.lang.String, java.lang.String, java.lang.String):java.lang.String
      android.support.v4.b.a.a(java.lang.Object[], java.lang.String, java.lang.String):java.lang.String
      android.support.v4.b.a.a(android.graphics.Bitmap, float, boolean):android.graphics.Bitmap */
    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        switch (i2) {
            case PurchaseCode.QUERY_OK /*101*/:
                if (i3 == -1 && intent != null) {
                    this.o = 2;
                    if (!android.support.v4.b.a.a((CharSequence) this.G)) {
                        File file = new File(com.immomo.momo.a.i(), this.G);
                        if (file.exists()) {
                            file.delete();
                        }
                        this.G = null;
                    }
                    if (this.H != null) {
                        String absolutePath = this.H.getAbsolutePath();
                        String substring = this.H.getName().substring(0, this.H.getName().lastIndexOf("."));
                        Bitmap l2 = android.support.v4.b.a.l(absolutePath);
                        if (l2 != null) {
                            File a2 = h.a(substring, l2, 16, false);
                            this.e.a((Object) ("scaleAndSavePhoto, uploadFile=" + a2.getPath()));
                            this.K = android.support.v4.b.a.a(l2, 150.0f, true);
                            h.a(substring, this.K, 15, false);
                            av avVar = new av();
                            avVar.b = a2;
                            avVar.f2998a = a2.getAbsolutePath();
                            avVar.c = this.K;
                            this.B.put(avVar.f2998a, avVar);
                            this.t.b(avVar);
                            this.t.setData(this.t.getDatalist());
                            l2.recycle();
                        }
                        try {
                            this.H.delete();
                            this.H = null;
                        } catch (Exception e) {
                        }
                        getWindow().getDecorView().requestFocus();
                        return;
                    }
                    return;
                } else if (i3 == 1003) {
                    ao.b("图片尺寸太小，请重新选择", 1);
                    return;
                } else if (i3 == 1000) {
                    ao.d(R.string.cropimage_error_other);
                    return;
                } else if (i3 == 1002) {
                    ao.d(R.string.cropimage_error_store);
                    return;
                } else if (i3 == 1001) {
                    ao.d(R.string.cropimage_error_filenotfound);
                    return;
                } else {
                    return;
                }
            case PurchaseCode.ORDER_OK /*102*/:
            default:
                return;
            case PurchaseCode.UNSUB_OK /*103*/:
                if (i3 == -1 && !android.support.v4.b.a.a((CharSequence) this.G)) {
                    this.k.requestFocus();
                    Uri fromFile = Uri.fromFile(new File(com.immomo.momo.a.i(), this.G));
                    if (fromFile != null) {
                        Intent intent2 = new Intent(this, ImageFactoryActivity.class);
                        intent2.setData(fromFile);
                        intent2.putExtra("minsize", 320);
                        intent2.putExtra("process_model", "filter");
                        intent2.putExtra("maxwidth", 720);
                        intent2.putExtra("maxheight", 3000);
                        this.H = new File(com.immomo.momo.a.g(), "temp_" + b.a() + ".jpg_");
                        intent2.putExtra("outputFilePath", this.H.getAbsolutePath());
                        startActivityForResult(intent2, PurchaseCode.QUERY_OK);
                        return;
                    }
                    return;
                }
                return;
            case PurchaseCode.AUTH_OK /*104*/:
                if (i3 == -1 && intent != null) {
                    this.o = 2;
                    b(new es(this, this, intent.getStringArrayListExtra("select_images_path")));
                    return;
                }
                return;
            case 105:
                if (i3 != -1 || intent == null) {
                    if (i3 == 1003) {
                        ao.b("图片尺寸太小，请重新选择", 1);
                        return;
                    } else if (i3 == 1000) {
                        ao.d(R.string.cropimage_error_other);
                        return;
                    } else if (i3 == 1002) {
                        ao.d(R.string.cropimage_error_store);
                        return;
                    } else if (i3 == 1001) {
                        ao.d(R.string.cropimage_error_filenotfound);
                        return;
                    } else {
                        return;
                    }
                } else if (this.I != null) {
                    String absolutePath2 = this.I.getAbsolutePath();
                    String substring2 = this.I.getName().substring(0, this.I.getName().lastIndexOf("."));
                    Bitmap l3 = android.support.v4.b.a.l(absolutePath2);
                    if (l3 != null) {
                        File a3 = h.a(substring2, l3, 16, false);
                        this.e.a((Object) ("scaleAndSavePhoto, uploadFile=" + a3.getPath()));
                        Bitmap a4 = android.support.v4.b.a.a(l3, 150.0f, true);
                        h.a(substring2, a4, 15, false);
                        av avVar2 = (av) this.t.getDatalist().get(this.L);
                        avVar2.b = a3;
                        avVar2.f2998a = a3.getAbsolutePath();
                        avVar2.c = a4;
                        this.B.put(avVar2.f2998a, avVar2);
                        this.t.a(this.L, avVar2);
                        this.t.setData(this.t.getDatalist());
                        l3.recycle();
                    }
                    try {
                        this.I.delete();
                        this.I = null;
                    } catch (Exception e2) {
                    }
                    getWindow().getDecorView().requestFocus();
                    return;
                } else {
                    return;
                }
        }
    }

    public void onBackPressed() {
        if (r()) {
            if (this.q.isShown()) {
                this.q.a();
                this.h = false;
                return;
            }
            if (this.J != null || (this.t.getDatalist() != null && this.t.getDatalist().size() > 0) || android.support.v4.b.a.f(this.k.getText().toString().trim())) {
                n nVar = new n(this);
                nVar.a();
                nVar.setTitle((int) R.string.feed_publish_dialog_title);
                nVar.a("确定放弃发布吗？");
                nVar.a(0, (int) R.string.dialog_btn_confim, new ed(this));
                nVar.a(1, (int) R.string.dialog_btn_cancel, new eh());
                nVar.show();
                return;
            }
        }
        super.onBackPressed();
    }

    public void onClick(View view) {
        int i2 = 0;
        switch (view.getId()) {
            case R.id.btn_localphoto /*2131165805*/:
                A();
                ArrayList arrayList = new ArrayList();
                if (this.t.getDatalist() != null) {
                    while (true) {
                        int i3 = i2;
                        if (i3 < this.t.getDatalist().size()) {
                            arrayList.add(((av) this.t.getDatalist().get(i3)).f2998a);
                            i2 = i3 + 1;
                        }
                    }
                }
                Intent intent = new Intent(this, MulImagePickerActivity.class);
                intent.putStringArrayListExtra("select_images_path_results", arrayList);
                intent.putExtra("max_select_images_num", 6);
                startActivityForResult(intent, PurchaseCode.AUTH_OK);
                return;
            case R.id.btn_camera /*2131165806*/:
                A();
                if (this.t.getDatalist() == null) {
                    i2 = 1;
                } else if (this.t.getDatalist() != null && this.t.getDatalist().size() < 6) {
                    i2 = 1;
                }
                if (i2 == 0) {
                    a((CharSequence) "最多选择6张图片");
                    return;
                }
                Intent intent2 = new Intent("android.media.action.IMAGE_CAPTURE");
                StringBuffer stringBuffer = new StringBuffer();
                stringBuffer.append("immomo_");
                stringBuffer.append(android.support.v4.b.a.c(new Date()));
                stringBuffer.append("_" + UUID.randomUUID());
                stringBuffer.append(".jpg");
                this.G = stringBuffer.toString();
                intent2.putExtra("output", Uri.fromFile(new File(com.immomo.momo.a.i(), this.G)));
                startActivityForResult(intent2, PurchaseCode.UNSUB_OK);
                return;
            case R.id.btn_emote /*2131165807*/:
                c(4);
                return;
            case R.id.layout_selected_photo /*2131165808*/:
            case R.id.list_selectphotos /*2131165809*/:
            case R.id.layout_selected_emote /*2131165810*/:
            case R.id.iv_selected_emote /*2131165811*/:
            case R.id.tv_tip /*2131165813*/:
            case R.id.layout_line /*2131165814*/:
            case R.id.signeditor_layout_syncto_weixin /*2131165815*/:
            default:
                return;
            case R.id.iv_delete_emote /*2131165812*/:
                A();
                E();
                d(0);
                this.J = null;
                return;
            case R.id.btn_selectpic /*2131165816*/:
                u();
                this.r.postDelayed(new ee(this), 300);
                return;
            case R.id.btn_selectemote /*2131165817*/:
                C();
                this.q.a();
                this.x.setSelected(false);
                this.y.setSelected(true);
                c(1);
                return;
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        y();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.A != null) {
            this.A.l();
        }
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Bundle bundle) {
        super.onRestoreInstanceState(bundle);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.n) {
            this.r.postDelayed(new ei(this), 200);
            getWindow().getDecorView().requestFocus();
            this.h = this.q.isShown();
        }
        if (!this.n) {
            this.n = true;
        }
        this.h = false;
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        String editable = this.k.getText().toString();
        if (!android.support.v4.b.a.a((CharSequence) editable)) {
            bundle.putString("save_tiecontent", editable);
        }
        bundle.putInt("selectMode", this.o);
        if (this.J != null && this.o == 1) {
            bundle.putString("emotionbody", this.J.toString());
        }
        if (this.t.getDatalist() != null && this.o == 2) {
            bundle.putString("pathlist", android.support.v4.b.a.a(F(), ","));
        }
        bundle.putString("gid", this.i);
        bundle.putBoolean("from_saveinstance", true);
        if (!android.support.v4.b.a.a((CharSequence) this.G)) {
            bundle.putString("camera_filename", this.G);
        }
        if (this.H != null) {
            bundle.putString("camera_filepath", this.H.getPath());
        }
        if (this.I != null) {
            bundle.putString("local_filepath", this.I.getPath());
        }
        bundle.putInt("posFilter", this.L);
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        switch (view.getId()) {
            case R.id.layout_content /*2131165199*/:
                if ((this.q.isShown() || this.h) && 1 == motionEvent.getAction() && motionEvent.getEventTime() - motionEvent.getDownTime() < 100) {
                    if (this.q.isShown()) {
                        this.q.a();
                    } else {
                        u();
                    }
                    this.h = false;
                    break;
                }
            case R.id.signeditor_tv_text /*2131165795*/:
            case R.id.et_title /*2131165818*/:
                if (motionEvent.getAction() == 1) {
                    this.q.a();
                    C();
                    break;
                }
                break;
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public final void u() {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService("input_method");
        View currentFocus = getCurrentFocus();
        if (currentFocus != null) {
            inputMethodManager.hideSoftInputFromWindow(currentFocus.getWindowToken(), 2);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.h.a(java.lang.String, android.graphics.Bitmap, int, boolean):java.io.File
     arg types: [java.lang.String, android.graphics.Bitmap, int, int]
     candidates:
      com.immomo.momo.util.h.a(java.lang.String, java.lang.String, int, boolean):void
      com.immomo.momo.util.h.a(java.lang.String, android.graphics.Bitmap, int, boolean):java.io.File */
    public final void v() {
        this.j = new e();
        switch (this.o) {
            case 0:
                o.a().a(this.j, this.k.getText().toString().trim(), new HashMap(), this.E, null, this.i);
                break;
            case 1:
                o.a().a(this.j, this.k.getText().toString().trim(), new HashMap(), this.E, this.J, this.i);
                break;
            case 2:
                for (Map.Entry entry : this.C.entrySet()) {
                    File file = (File) entry.getValue();
                    if (file == null || !file.exists()) {
                        this.r.post(new ef(this));
                        throw new Exception();
                    }
                    String substring = file.getName().substring(0, file.getName().lastIndexOf("."));
                    Bitmap a2 = android.support.v4.b.a.a(file, 720, 3000);
                    entry.setValue(h.a(substring, a2, 16, false));
                    this.D.put((String) entry.getKey(), substring);
                    a2.recycle();
                }
                o.a().a(this.j, this.k.getText().toString().trim(), this.C, this.E, null, this.i);
                break;
        }
        new v().a(this.j);
        a(this.j);
        this.r.post(new eg());
    }

    public final m w() {
        return this.F;
    }

    public final void x() {
        if (this.t.getDatalist() != null) {
            this.z.setText(new StringBuilder().append(this.t.getDatalist().size()).toString());
            if (this.t.getDatalist().size() > 0) {
                this.w.setVisibility(0);
                findViewById(R.id.btn_localphoto).setEnabled(true);
                findViewById(R.id.btn_camera).setEnabled(true);
                findViewById(R.id.btn_emote).setEnabled(false);
                findViewById(R.id.layout_tip).setVisibility(8);
                this.z.setVisibility(0);
                RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) this.v.getLayoutParams();
                layoutParams.height = -2;
                this.v.setLayoutParams(layoutParams);
                return;
            }
            E();
            this.z.setVisibility(8);
            RelativeLayout.LayoutParams layoutParams2 = (RelativeLayout.LayoutParams) this.v.getLayoutParams();
            layoutParams2.height = (int) (266.0f * g.k());
            this.v.setLayoutParams(layoutParams2);
            D();
            return;
        }
        this.z.setVisibility(8);
        D();
    }
}
