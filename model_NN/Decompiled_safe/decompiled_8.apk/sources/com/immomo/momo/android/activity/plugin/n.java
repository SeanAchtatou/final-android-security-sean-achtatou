package com.immomo.momo.android.activity.plugin;

import android.content.Context;
import com.immomo.momo.R;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.w;

final class n extends d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ BindPhoneActivity f2093a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public n(BindPhoneActivity bindPhoneActivity, Context context) {
        super(context);
        this.f2093a = bindPhoneActivity;
        if (bindPhoneActivity.r != null) {
            bindPhoneActivity.r.cancel(true);
        }
        bindPhoneActivity.r = this;
    }

    /* access modifiers changed from: protected */
    public final Object a(Object... objArr) {
        w.a().a(4, this.f2093a.l, this.f2093a.m, this.f2093a.n, (String) null);
        return null;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.f2093a.q = new v(this.f2093a, (int) R.string.press);
        this.f2093a.q.setCancelable(true);
        this.f2093a.q.setOnCancelListener(new o(this));
        this.f2093a.a(this.f2093a.q);
    }

    /* access modifiers changed from: protected */
    public final void a(Object obj) {
        this.f2093a.p = String.valueOf(this.f2093a.l) + this.f2093a.m + this.f2093a.n;
        this.f2093a.g();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.f2093a.p();
    }
}
