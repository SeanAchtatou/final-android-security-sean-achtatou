package com.immomo.momo.android.a;

import android.view.View;
import com.immomo.momo.android.activity.ao;
import com.immomo.momo.android.activity.d;
import com.immomo.momo.service.bean.a.b;
import com.immomo.momo.service.bean.a.c;

final class dy implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private c f788a;
    private b b;
    private /* synthetic */ dn c;

    public dy(dn dnVar, b bVar, c cVar) {
        this.c = dnVar;
        this.f788a = cVar;
        this.b = bVar;
    }

    public final void onClick(View view) {
        if (d.a(this.f788a.b())) {
            d.a(this.f788a.f(), this.c.d);
        } else {
            ((ao) this.c.d).b(new ea(this.c, this.c.d, this.b, this.f788a));
        }
    }
}
