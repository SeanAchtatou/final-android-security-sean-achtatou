package com.immomo.momo.android.activity.tieba;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.protocol.a.v;
import com.immomo.momo.service.bean.c.b;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

final class fa extends d {

    /* renamed from: a  reason: collision with root package name */
    private List f2278a = new ArrayList();
    private int c;
    private /* synthetic */ TiebaProfileActivity d;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public fa(TiebaProfileActivity tiebaProfileActivity, Context context, int i) {
        super(context);
        this.d = tiebaProfileActivity;
        this.c = i;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        StringBuilder sb = new StringBuilder();
        boolean a2 = v.a().a(this.f2278a, this.c, this.d.k, sb);
        TiebaProfileActivity tiebaProfileActivity = this.d;
        tiebaProfileActivity.i = Integer.parseInt(sb.toString()) + tiebaProfileActivity.i;
        for (b bVar : this.f2278a) {
            this.d.m.e(bVar.f3017a);
        }
        if (this.c == 0) {
            this.d.m.a(this.f2278a, this.d.k);
        } else {
            this.d.m.g(this.f2278a);
        }
        return Boolean.valueOf(a2);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        Boolean bool = (Boolean) obj;
        super.a(bool);
        Iterator it = this.f2278a.iterator();
        while (it.hasNext()) {
            if (!this.d.h.add(((b) it.next()).f3017a)) {
                it.remove();
                it = this.f2278a.iterator();
            }
        }
        if (this.c == 0) {
            this.d.p.clear();
            this.d.q.clear();
        }
        if (!(this.f2278a == null || this.f2278a.size() == 0)) {
            this.d.p.addAll(this.f2278a);
            ArrayList arrayList = new ArrayList();
            TiebaProfileActivity tiebaProfileActivity = this.d;
            TiebaProfileActivity.b(arrayList, this.f2278a);
            this.d.q.addAll(arrayList);
        }
        this.d.o.notifyDataSetChanged();
        this.d.r.setVisibility(bool.booleanValue() ? 0 : 8);
    }

    /* access modifiers changed from: protected */
    public final void b() {
        super.b();
        this.d.j = new Date();
        this.d.g.a("tieba_profile_lasttime_success", this.d.j);
        this.d.n.n();
        if (this.c != 0) {
            this.d.r.e();
        }
    }
}
