package com.immomo.momo.android.activity;

import android.support.v4.b.a;
import android.view.View;
import com.immomo.momo.R;
import com.immomo.momo.protocol.imjson.util.c;

final class gf implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ OldAddFriendActivity f1513a;

    gf(OldAddFriendActivity oldAddFriendActivity) {
        this.f1513a = oldAddFriendActivity;
    }

    public final void onClick(View view) {
        String editable = this.f1513a.Q.getText().toString();
        if (a.a((CharSequence) editable)) {
            this.f1513a.d((int) R.string.find_empty_momoid);
        } else if (editable.equals(this.f1513a.M.h)) {
            this.f1513a.d((int) R.string.find_your_momoid);
        } else if (!c.c() || !editable.equals("1602")) {
            this.f1513a.R = new gg(this.f1513a).execute(editable);
        } else {
            c.a().d();
        }
    }
}
