package com.immomo.momo.android.activity.contacts;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.android.a.dl;
import com.immomo.momo.android.activity.lh;
import com.immomo.momo.android.activity.maintab.MaintabActivity;
import com.immomo.momo.android.broadcast.d;
import com.immomo.momo.android.broadcast.j;
import com.immomo.momo.android.broadcast.w;
import com.immomo.momo.android.view.FriendRrefreshView;
import com.immomo.momo.android.view.HeaderLayout;
import com.immomo.momo.android.view.LoadingButton;
import com.immomo.momo.android.view.a.o;
import com.immomo.momo.android.view.bl;
import com.immomo.momo.android.view.bu;
import com.immomo.momo.android.view.cv;
import com.immomo.momo.g;
import com.immomo.momo.service.aq;
import com.immomo.momo.util.k;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class a extends lh implements View.OnClickListener, bl, bu, cv {
    /* access modifiers changed from: private */
    public FriendRrefreshView O;
    /* access modifiers changed from: private */
    public dl P;
    /* access modifiers changed from: private */
    public LoadingButton Q;
    /* access modifiers changed from: private */
    public List R;
    private j S;
    private w T = null;
    /* access modifiers changed from: private */
    public Date U = null;
    /* access modifiers changed from: private */
    public aq V;
    /* access modifiers changed from: private */
    public l W;
    /* access modifiers changed from: private */
    public m X;
    /* access modifiers changed from: private */
    public Handler Y = new Handler();
    /* access modifiers changed from: private */
    public View Z;
    /* access modifiers changed from: private */
    public EditText aa;
    private View ab;
    /* access modifiers changed from: private */
    public boolean ac = false;
    private TextWatcher ad = new b(this);
    /* access modifiers changed from: private */
    public int ae = 2;
    private d af = new c(this);

    /* access modifiers changed from: private */
    public void O() {
        this.P = new dl(g.c(), this.R, this.O, true);
        this.O.setAdapter((ListAdapter) this.P);
        if (this.P.getCount() < 500) {
            this.O.k();
        }
    }

    /* access modifiers changed from: private */
    public List P() {
        this.R = this.V.a(this.ae);
        return this.R;
    }

    static /* synthetic */ void o(a aVar) {
        aVar.P = new dl(aVar.c(), aVar.R, aVar.O, true);
        aVar.O.setAdapter((ListAdapter) aVar.P);
    }

    /* access modifiers changed from: protected */
    public final int B() {
        return R.layout.layout_relation_both;
    }

    /* access modifiers changed from: protected */
    public final void C() {
        this.O = (FriendRrefreshView) c((int) R.id.both_listview);
        this.O.setEnableLoadMoreFoolter(true);
        View inflate = g.o().inflate((int) R.layout.listitem_relation_empty, (ViewGroup) null);
        ((TextView) inflate.findViewById(R.id.tv_tip)).setText((int) R.string.bothlist_empty_tip);
        this.O.a(inflate);
        this.O.setListPaddingBottom(MaintabActivity.h);
        this.Q = this.O.getFooterViewButton();
        this.Z = this.O.getClearButton();
        this.aa = this.O.getSearchEdit();
        this.ab = this.O.getSortView();
    }

    /* access modifiers changed from: protected */
    public final void E() {
        super.E();
        this.O.o();
        if (this.Q.d()) {
            this.Q.e();
        }
        this.ac = true;
        this.aa.getText().clear();
        if (this.Z.isShown()) {
            this.Z.setVisibility(4);
        }
        this.ac = false;
        if (this.ad != null) {
            this.aa.removeTextChangedListener(this.ad);
        }
    }

    public final void S() {
        super.S();
        if (this.W != null && !this.W.isCancelled()) {
            return;
        }
        if (this.U == null) {
            this.O.l();
        } else if (new Date().getTime() - this.U.getTime() > 900000) {
            this.O.l();
        } else if (this.R.size() <= 0) {
            this.O.l();
        }
    }

    public final void a(Context context, HeaderLayout headerLayout) {
    }

    public final void ae() {
        super.ae();
        new k("PI", "P61").e();
    }

    public final void ag() {
        super.ag();
        new k("PO", "P61").e();
    }

    public final void ai() {
        this.O.i();
    }

    public final void aj() {
        this.O.setLastFlushTime(this.N.b("lasttime_bothlist_success"));
        this.U = this.N.b("lasttime_bothlist_success");
        P();
        O();
        this.S = new j(c());
        this.S.a(new h(this));
    }

    public final void b_() {
        if (this.W != null && !this.W.isCancelled()) {
            this.W.cancel(true);
        }
        this.W = new l(this, c());
        this.W.execute(new Object[0]);
    }

    /* access modifiers changed from: protected */
    public final void g(Bundle bundle) {
        this.V = new aq();
        this.T = new w(c());
        this.T.a(this.af);
        this.ae = ((Integer) this.N.b("sorttype_realtion_both", 2)).intValue();
        this.O.setOnPullToRefreshListener$42b903f6(this);
        this.O.setOnCancelListener$135502(this);
        this.O.setOnItemClickListener(new f(this));
        this.Q.setOnProcessListener(this);
        this.Z.setOnClickListener(new g(this));
        this.aa.addTextChangedListener(this.ad);
        this.ab.setOnClickListener(this);
        aj();
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.layout_sort /*2131166400*/:
                o oVar = new o(c());
                String[] stringArray = d().getStringArray(R.array.order_friend_list);
                ArrayList arrayList = new ArrayList();
                int i = 0;
                while (i < stringArray.length) {
                    n nVar = new n((byte) 0);
                    nVar.f1194a = stringArray[i];
                    nVar.b = i == this.ae;
                    arrayList.add(nVar);
                    i++;
                }
                oVar.a(new k(c(), arrayList));
                oVar.setTitle((int) R.string.header_order);
                oVar.a();
                oVar.a(new i(this));
                oVar.setOnCancelListener(new j());
                oVar.show();
                return;
            default:
                return;
        }
    }

    public final void p() {
        super.p();
        if (this.S != null) {
            a(this.S);
            this.S = null;
        }
        if (this.T != null) {
            a(this.T);
            this.T = null;
        }
        if (this.W != null && !this.W.isCancelled()) {
            this.W.cancel(true);
            this.W = null;
        }
        if (this.X != null && !this.X.isCancelled()) {
            this.X.cancel(true);
            this.X = null;
        }
    }

    public final void u() {
        this.Q.f();
        this.X = new m(this, c());
        this.X.execute(new Object[0]);
    }

    public final void v() {
        this.U = new Date();
        this.N.a("lasttime_bothlist", this.U);
        this.O.n();
        this.Q.e();
        if (this.W != null && !this.W.isCancelled()) {
            this.W.cancel(true);
        }
    }
}
