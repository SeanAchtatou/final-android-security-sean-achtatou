package com.immomo.momo.android.activity.maintab;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import com.immomo.momo.R;
import com.immomo.momo.android.a.cs;
import com.immomo.momo.android.activity.lh;
import com.immomo.momo.android.b.z;
import com.immomo.momo.android.broadcast.p;
import com.immomo.momo.android.view.HeaderLayout;
import com.immomo.momo.android.view.LoadingButton;
import com.immomo.momo.android.view.MomoRefreshListView;
import com.immomo.momo.android.view.bu;
import com.immomo.momo.g;
import com.immomo.momo.service.ai;
import com.immomo.momo.service.aq;
import com.immomo.momo.util.ao;
import com.immomo.momo.util.k;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class l extends lh implements bu {
    /* access modifiers changed from: private */
    public MomoRefreshListView O = null;
    /* access modifiers changed from: private */
    public LoadingButton P;
    /* access modifiers changed from: private */
    public Date Q = null;
    /* access modifiers changed from: private */
    public List R = null;
    /* access modifiers changed from: private */
    public Set S = new HashSet();
    /* access modifiers changed from: private */
    public aq T = null;
    /* access modifiers changed from: private */
    public w U = null;
    /* access modifiers changed from: private */
    public Handler V = new Handler();
    /* access modifiers changed from: private */
    public cs W = null;
    /* access modifiers changed from: private */
    public ai X;
    private p Y = null;
    private com.immomo.momo.android.b.p Z = null;

    private void O() {
        this.O.setLoadingViewText(R.string.pull_to_refresh_locate_label);
        this.Z = new p(this);
        try {
            z.a(this.Z);
        } catch (Exception e) {
            this.L.a((Throwable) e);
            ao.g(R.string.errormsg_location_nearby_failed);
            P();
        }
    }

    /* access modifiers changed from: private */
    public void P() {
        this.V.post(new r(this));
    }

    static /* synthetic */ void k(l lVar) {
        if (lVar.Z != null) {
            z.b(lVar.Z);
            lVar.Z.b = false;
        }
        lVar.O.n();
        if (lVar.U != null) {
            lVar.U.cancel(true);
            lVar.U = null;
        }
    }

    static /* synthetic */ void p(l lVar) {
        Intent intent = new Intent();
        intent.setAction("com.android.settings.APPLICATION_DEVELOPMENT_SETTINGS");
        intent.setFlags(268435456);
        try {
            lVar.a(intent);
        } catch (Throwable th) {
            lVar.L.a(th);
            intent.setAction("android.settings.SETTINGS");
            try {
                lVar.a(intent);
            } catch (Exception e) {
                lVar.L.a((Throwable) e);
            }
        }
    }

    /* access modifiers changed from: protected */
    public final int B() {
        return R.layout.activity_nearbyfeeds;
    }

    /* access modifiers changed from: protected */
    public final void C() {
        this.O = (MomoRefreshListView) c((int) R.id.listview);
        this.O.setFastScrollEnabled(false);
        this.O.setLastFlushTime(this.N.b("nfeeds_lasttime_success"));
        this.O.setEnableLoadMoreFoolter(true);
        this.P = this.O.getFooterViewButton();
        this.O.a(g.o().inflate((int) R.layout.include_nearbysite_listempty, (ViewGroup) null));
        this.O.addHeaderView(g.o().inflate((int) R.layout.listitem_blank, (ViewGroup) null));
        this.O.setListPaddingBottom(-3);
    }

    public final void S() {
        super.S();
        if (this.W.isEmpty()) {
            this.O.l();
        } else if (this.Q == null || System.currentTimeMillis() - this.Q.getTime() > 900000) {
            O();
        }
    }

    public final void a(Context context, HeaderLayout headerLayout) {
        headerLayout.setTitleText((int) R.string.nearbyfeeds);
    }

    /* access modifiers changed from: protected */
    public final void ae() {
        new k("PI", "P4").e();
    }

    /* access modifiers changed from: protected */
    public final void ag() {
        new k("PO", "P4").e();
    }

    public final void ai() {
        this.O.i();
    }

    public final void aj() {
        super.aj();
        this.T = new aq();
        this.X = new ai();
        this.R = this.X.e();
        this.S.clear();
        this.S.addAll(this.R);
        MomoRefreshListView momoRefreshListView = this.O;
        cs csVar = new cs(c(), this.R, this.O);
        this.W = csVar;
        momoRefreshListView.setAdapter((ListAdapter) csVar);
        this.Q = this.N.b("nfeeds_latttime_reflush");
        if (this.R.size() <= 0 || !((Boolean) this.N.b("nffilter_remain", false)).booleanValue()) {
            this.P.setVisibility(8);
        } else {
            this.P.setVisibility(0);
        }
    }

    public final void b_() {
        O();
        new k("S", "S401").e();
    }

    /* access modifiers changed from: protected */
    public final void g(Bundle bundle) {
        this.P.setOnProcessListener(new n(this));
        this.O.setOnPullToRefreshListener$42b903f6(this);
        this.O.setOnCancelListener$135502(new o(this));
        aj();
        this.Y = new p(c());
        this.Y.a(new m(this));
    }

    public final void p() {
        super.p();
        if (this.Y != null) {
            a(this.Y);
            this.Y = null;
        }
    }
}
