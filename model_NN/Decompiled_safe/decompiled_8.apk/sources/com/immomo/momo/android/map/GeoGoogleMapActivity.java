package com.immomo.momo.android.map;

import android.content.Intent;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.immomo.momo.R;
import com.immomo.momo.android.b.p;
import com.immomo.momo.android.b.z;
import com.immomo.momo.android.view.HeaderLayout;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.g;
import com.immomo.momo.util.m;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class GeoGoogleMapActivity extends MapActivity implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    v f2450a = null;
    private GeoPoint b = null;
    /* access modifiers changed from: private */
    public MomoGoogleMapView c = null;
    private HeaderLayout d = null;
    /* access modifiers changed from: private */
    public m e = new m(getClass().getSimpleName());
    /* access modifiers changed from: private */
    public double f;
    /* access modifiers changed from: private */
    public double g;
    /* access modifiers changed from: private */
    public p h = null;
    private ExecutorService i = Executors.newSingleThreadExecutor();
    private boolean j = false;
    private Button k;
    private Button l;
    /* access modifiers changed from: private */
    public Handler m = new Handler();

    /* access modifiers changed from: private */
    public void a() {
        this.m.post(new w(this));
        z.b(this.h);
        if (this.h != null) {
            this.h.b = false;
        }
    }

    /* access modifiers changed from: private */
    public void b() {
        this.b = new GeoPoint((int) (this.f * 1000000.0d), (int) (this.g * 1000000.0d));
        this.c.getController().animateTo(this.b);
        this.c.getController().setCenter(this.b);
    }

    static /* synthetic */ void e(GeoGoogleMapActivity geoGoogleMapActivity) {
        if (!geoGoogleMapActivity.i.isShutdown()) {
            geoGoogleMapActivity.i.execute(new Thread(new u(geoGoogleMapActivity)));
        }
    }

    /* access modifiers changed from: protected */
    public boolean isRouteDisplayed() {
        return true;
    }

    public void onBackPressed() {
        setResult(0);
        finish();
        GeoGoogleMapActivity.super.onBackPressed();
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_back /*2131165286*/:
                setResult(0);
                finish();
                return;
            case R.id.btn_login /*2131165287*/:
            case R.id.btn_register /*2131165288*/:
            default:
                return;
            case R.id.btn_ok /*2131165289*/:
                Intent intent = new Intent();
                intent.putExtra("latitude", this.f);
                intent.putExtra("longitude", this.g);
                intent.putExtra("geo_adress", z.a(this.f, this.g, g.q().S, g.q().T));
                setResult(-1, intent);
                finish();
                return;
        }
    }

    /* JADX WARN: Type inference failed for: r7v0, types: [android.content.Context, android.view.View$OnClickListener, com.immomo.momo.android.map.GeoGoogleMapActivity, com.google.android.maps.MapActivity] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x00b1  */
    /* JADX WARNING: Removed duplicated region for block: B:21:? A[RETURN, SYNTHETIC] */
    protected void onCreate(android.os.Bundle r8) {
        /*
            r7 = this;
            r6 = 0
            r2 = -4616189618054758400(0xbff0000000000000, double:-1.0)
            r5 = 1
            com.immomo.momo.android.map.GeoGoogleMapActivity.super.onCreate(r8)
            r0 = 2130903103(0x7f03003f, float:1.7413015E38)
            r7.setContentView(r0)
            r0 = 2131165197(0x7f07000d, float:1.7944604E38)
            android.view.View r0 = r7.findViewById(r0)
            com.immomo.momo.android.view.HeaderLayout r0 = (com.immomo.momo.android.view.HeaderLayout) r0
            r7.d = r0
            com.immomo.momo.android.view.HeaderLayout r0 = r7.d
            r1 = 2131493288(0x7f0c01a8, float:1.8610052E38)
            r0.setTitleText(r1)
            r0 = 2131166037(0x7f070355, float:1.7946308E38)
            r7.findViewById(r0)
            r0 = 2131165286(0x7f070066, float:1.7944785E38)
            android.view.View r0 = r7.findViewById(r0)
            android.widget.Button r0 = (android.widget.Button) r0
            r7.k = r0
            r0 = 2131165289(0x7f070069, float:1.794479E38)
            android.view.View r0 = r7.findViewById(r0)
            android.widget.Button r0 = (android.widget.Button) r0
            r7.l = r0
            r0 = 2131165395(0x7f0700d3, float:1.7945006E38)
            android.view.View r0 = r7.findViewById(r0)
            com.immomo.momo.android.map.MomoGoogleMapView r0 = (com.immomo.momo.android.map.MomoGoogleMapView) r0
            r7.c = r0
            com.immomo.momo.android.map.MomoGoogleMapView r0 = r7.c
            r0.setBuiltInZoomControls(r5)
            com.immomo.momo.android.map.MomoGoogleMapView r0 = r7.c
            r0.displayZoomControls(r5)
            com.immomo.momo.android.map.MomoGoogleMapView r0 = r7.c
            r0.setEnabled(r5)
            com.immomo.momo.android.map.MomoGoogleMapView r0 = r7.c
            r0.setClickable(r5)
            com.immomo.momo.android.map.MomoGoogleMapView r0 = r7.c
            com.google.android.maps.MapController r0 = r0.getController()
            r1 = 16
            r0.setZoom(r1)
            com.immomo.momo.android.map.MomoGoogleMapView r0 = r7.c
            r0.setSatellite(r6)
            com.immomo.momo.android.map.MomoGoogleMapView r0 = r7.c
            com.immomo.momo.android.map.v r1 = new com.immomo.momo.android.map.v
            r1.<init>(r7)
            r0.setGetAddressListener(r1)
            android.content.Intent r0 = r7.getIntent()
            android.os.Bundle r4 = r0.getExtras()
            if (r4 == 0) goto L_0x00f6
            java.lang.String r0 = "latitude"
            java.lang.Object r0 = r4.get(r0)
            if (r0 != 0) goto L_0x00d0
            r0 = r2
        L_0x0088:
            r7.f = r0
            java.lang.String r0 = "longitude"
            java.lang.Object r0 = r4.get(r0)
            if (r0 != 0) goto L_0x00e3
        L_0x0092:
            r7.g = r2
            double r0 = r7.f
            double r2 = r7.g
            boolean r0 = com.immomo.momo.android.b.z.a(r0, r2)
            if (r0 == 0) goto L_0x00f6
            r7.b()
            r7.j = r6
        L_0x00a3:
            android.widget.Button r0 = r7.k
            r0.setOnClickListener(r7)
            android.widget.Button r0 = r7.l
            r0.setOnClickListener(r7)
            boolean r0 = r7.j
            if (r0 == 0) goto L_0x00cf
            com.immomo.momo.android.view.a.v r0 = new com.immomo.momo.android.view.a.v
            r1 = 2131492910(0x7f0c002e, float:1.8609285E38)
            r0.<init>(r7, r1)
            r7.f2450a = r0
            com.immomo.momo.android.view.a.v r0 = r7.f2450a
            r0.show()
            com.immomo.momo.android.map.x r0 = new com.immomo.momo.android.map.x
            r0.<init>(r7)
            r7.h = r0
            com.immomo.momo.android.map.z r0 = new com.immomo.momo.android.map.z     // Catch:{ Exception -> 0x00f9 }
            r0.<init>(r7)     // Catch:{ Exception -> 0x00f9 }
            com.immomo.momo.android.b.z.a(r0)     // Catch:{ Exception -> 0x00f9 }
        L_0x00cf:
            return
        L_0x00d0:
            java.lang.String r0 = "latitude"
            java.lang.Object r0 = r4.get(r0)
            java.lang.String r0 = r0.toString()
            java.lang.Double r0 = java.lang.Double.valueOf(r0)
            double r0 = r0.doubleValue()
            goto L_0x0088
        L_0x00e3:
            java.lang.String r0 = "longitude"
            java.lang.Object r0 = r4.get(r0)
            java.lang.String r0 = r0.toString()
            java.lang.Double r0 = java.lang.Double.valueOf(r0)
            double r2 = r0.doubleValue()
            goto L_0x0092
        L_0x00f6:
            r7.j = r5
            goto L_0x00a3
        L_0x00f9:
            r0 = move-exception
            r0 = 2131493069(0x7f0c00cd, float:1.8609608E38)
            com.immomo.momo.util.ao.g(r0)
            r7.a()
            goto L_0x00cf
        */
        throw new UnsupportedOperationException("Method not decompiled: com.immomo.momo.android.map.GeoGoogleMapActivity.onCreate(android.os.Bundle):void");
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        try {
            this.i.shutdown();
        } catch (Exception e2) {
            this.e.a((Throwable) e2);
        }
        a();
        GeoGoogleMapActivity.super.onDestroy();
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        return GeoGoogleMapActivity.super.onKeyDown(i2, keyEvent);
    }
}
