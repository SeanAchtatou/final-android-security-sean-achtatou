package com.immomo.momo.android.activity.common;

import android.content.Intent;
import android.support.v4.b.a;
import com.immomo.momo.android.broadcast.d;

final class c implements d {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ a f1116a;

    c(a aVar) {
        this.f1116a = aVar;
    }

    public final void a(Intent intent) {
        String stringExtra = intent.getStringExtra("momoid");
        if (!a.a((CharSequence) stringExtra) && this.f1116a.P != null) {
            new Thread(new d(this, stringExtra)).start();
        }
    }
}
