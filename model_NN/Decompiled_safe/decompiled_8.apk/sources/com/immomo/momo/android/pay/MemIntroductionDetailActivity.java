package com.immomo.momo.android.pay;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.b.a;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.ah;
import com.immomo.momo.android.broadcast.d;
import com.immomo.momo.android.broadcast.n;

public class MemIntroductionDetailActivity extends ah implements View.OnClickListener {
    private WebView h = null;
    private Button i = null;
    private n j = null;
    private String k;
    private String l;
    private d m = new aa(this);

    /* access modifiers changed from: protected */
    public final void b(Bundle bundle) {
        super.b(bundle);
        setContentView((int) R.layout.activity_mem_introduction);
        this.k = getIntent().getStringExtra("webview_url");
        this.l = getIntent().getStringExtra("webview_title");
        this.h = (WebView) findViewById(R.id.webview);
        this.h.getSettings().setBuiltInZoomControls(true);
        this.h.getSettings().setJavaScriptEnabled(true);
        this.i = (Button) findViewById(R.id.btn_openmember);
        if (this.f.b()) {
            findViewById(R.id.layout_btn).setVisibility(8);
        } else {
            this.i.setText("开通会员");
            findViewById(R.id.layout_btn).setVisibility(0);
        }
        this.i.setOnClickListener(this);
        this.h.setWebChromeClient(new ab());
        this.h.setWebViewClient(new ac());
        setTitle(this.l);
        d();
        if (!a.a((CharSequence) this.k)) {
            this.h.loadUrl(this.k);
        }
    }

    /* access modifiers changed from: protected */
    public final void d() {
        this.j = new n(this);
        this.j.a(this.m);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void onClick(View view) {
        Intent intent;
        switch (view.getId()) {
            case R.id.btn_openmember /*2131165758*/:
                if (this.f.b()) {
                    intent = new Intent(this, MemberCenterActivity.class);
                } else {
                    intent = new Intent(this, BuyMemberActivity.class);
                    intent.putExtra("key_is_openmember", true);
                }
                startActivity(intent);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (this.j != null) {
            unregisterReceiver(this.j);
            this.j = null;
        }
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }
}
