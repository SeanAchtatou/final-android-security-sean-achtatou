package com.immomo.momo.android.a.a;

import android.graphics.Bitmap;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.d;
import com.immomo.momo.android.c.r;
import com.immomo.momo.android.c.u;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.Message;
import java.io.File;
import java.util.concurrent.ThreadPoolExecutor;

public final class a extends ab implements View.OnClickListener, View.OnFocusChangeListener {
    private static ThreadPoolExecutor o = new u(3, 3);

    /* renamed from: a  reason: collision with root package name */
    private ImageView f672a;
    private View p = null;
    private ImageView q = null;
    private ImageView r = null;
    private TextView s = null;
    private TextView t = null;
    private float u = 60.0f;
    private float v = ((float) g.a(this.u));
    /* access modifiers changed from: private */
    public Handler w = new b(this, e().getMainLooper());

    protected a(com.immomo.momo.android.activity.message.a aVar) {
        super(aVar);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        View inflate = this.k.inflate((int) R.layout.message_action, (ViewGroup) null);
        this.i.addView(inflate, 0);
        this.i.setOnLongClickListener(this);
        this.i.setOnFocusChangeListener(this);
        this.p = inflate.findViewById(R.id.layout_top);
        this.q = (ImageView) inflate.findViewById(R.id.iv_top);
        this.r = (ImageView) inflate.findViewById(R.id.iv_left);
        this.s = (TextView) inflate.findViewById(R.id.tv_content);
        this.t = (TextView) inflate.findViewById(R.id.tv_action);
        this.f672a = (ImageView) inflate.findViewById(R.id.message_iv_msgimage);
    }

    /* access modifiers changed from: protected */
    public final void b() {
        String n;
        boolean z = false;
        Message message = this.h;
        if (message.layout == 0) {
            this.p.setVisibility(8);
            this.r.setVisibility(4);
            this.f672a = this.r;
            float f = this.v / ((float) message.imageWidth);
            ViewGroup.LayoutParams layoutParams = this.f672a.getLayoutParams();
            layoutParams.width = (int) this.v;
            layoutParams.height = (int) (((float) message.imageHeight) * f);
            this.f672a.setLayoutParams(layoutParams);
        } else {
            this.p.setVisibility(0);
            this.q.setVisibility(4);
            this.r.setVisibility(8);
            this.f672a = this.q;
            float f2 = this.v / 110.0f;
            ViewGroup.LayoutParams layoutParams2 = this.f672a.getLayoutParams();
            layoutParams2.width = (int) (((float) message.imageWidth) * f2);
            layoutParams2.height = (int) (this.v * f2);
            this.f672a.setLayoutParams(layoutParams2);
        }
        if (this.h.action == null) {
            this.i.setOnClickListener(null);
            this.t.setVisibility(8);
        } else if (android.support.v4.b.a.f(this.h.action.f2969a)) {
            this.i.setOnClickListener(this);
            this.t.setVisibility(0);
            this.t.setText(this.h.action.f2969a);
        } else {
            this.i.setOnClickListener(this);
            this.t.setVisibility(8);
        }
        this.h.setImageUrl(true);
        this.h.setImageId(this.h.fileName);
        Bitmap b = b(this.h.getLoadImageId());
        if (b != null) {
            this.f672a.setVisibility(0);
            this.f672a.setImageBitmap(b);
        } else if (this.h.isImageLoadingFailed()) {
            this.f672a.setImageBitmap(g.t());
        } else if (this.h.isLoadingResourse || e().y().g()) {
            this.f672a.setVisibility(4);
        } else {
            this.h.isLoadingResourse = true;
            c cVar = new c(this, this.h);
            if (this.h.fileName.indexOf("://") < 0) {
                n = this.h.fileName;
                z = new File(com.immomo.momo.a.k(), n).exists();
            } else {
                n = android.support.v4.b.a.n(this.h.fileName);
            }
            if (!z) {
                j.add(this.h.msgId);
            }
            if (android.support.v4.b.a.f(n)) {
                r rVar = new r(n, cVar, 18, null);
                rVar.a(this.h.fileName);
                o.execute(rVar);
            } else {
                this.f672a.setImageBitmap(g.t());
            }
        }
        this.s.setText(this.h.getContent());
    }

    public final void onClick(View view) {
        d.a(this.h.action.toString(), e());
    }

    public final void onFocusChange(View view, boolean z) {
    }
}
