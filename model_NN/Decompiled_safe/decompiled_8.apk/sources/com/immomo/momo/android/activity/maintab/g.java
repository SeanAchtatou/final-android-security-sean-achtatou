package com.immomo.momo.android.activity.maintab;

import android.content.Intent;
import android.support.v4.app.w;
import com.immomo.momo.android.activity.UserGuideActivity;
import com.immomo.momo.android.activity.lh;
import com.immomo.momo.util.am;

final class g implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ f f1889a;

    g(f fVar) {
        this.f1889a = fVar;
    }

    public final void run() {
        if (!this.f1889a.f1888a.isDestroyed()) {
            w a2 = this.f1889a.f1888a.c().a();
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 >= this.f1889a.f1888a.k.length) {
                    break;
                }
                if (i2 != this.f1889a.f1888a.q) {
                    lh lhVar = (lh) this.f1889a.f1888a.k[i2][3];
                    this.f1889a.f1888a.e.a((Object) ("hide " + lhVar.getClass().getName()));
                    a2.c(lhVar);
                }
                i = i2 + 1;
            }
            am.b();
            MaintabActivity maintabActivity = this.f1889a.f1888a;
            if (com.immomo.momo.g.P()) {
                this.f1889a.f1888a.startActivityForResult(new Intent(this.f1889a.f1888a.getApplicationContext(), UserGuideActivity.class), 321);
                this.f1889a.f1888a.j.sendEmptyMessageDelayed(11, 500);
            } else {
                this.f1889a.f1888a.j.sendEmptyMessageDelayed(12, 200);
            }
            this.f1889a.f1888a.a((lh) this.f1889a.f1888a.k[this.f1889a.f1888a.q][3], a2);
        }
    }
}
