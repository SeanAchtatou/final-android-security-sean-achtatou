package com.immomo.momo.android.game;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.m;

final class e extends d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ GameProfileActivity f2431a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public e(GameProfileActivity gameProfileActivity, Context context) {
        super(context);
        this.f2431a = gameProfileActivity;
    }

    /* access modifiers changed from: protected */
    public final Object a(Object... objArr) {
        m.a().a(this.f2431a.j, this.f2431a.z);
        this.f2431a.i.a(this.f2431a.j);
        return null;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        if (!this.f2431a.q) {
            this.f2431a.a(new v(f()));
        }
    }

    /* access modifiers changed from: protected */
    public final void a(Object obj) {
        this.f2431a.u();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.f2431a.p();
    }
}
