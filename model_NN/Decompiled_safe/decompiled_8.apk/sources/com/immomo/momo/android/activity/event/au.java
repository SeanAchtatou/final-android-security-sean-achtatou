package com.immomo.momo.android.activity.event;

import android.content.DialogInterface;

final class au implements DialogInterface.OnCancelListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ at f1364a;

    au(at atVar) {
        this.f1364a = atVar;
    }

    public final void onCancel(DialogInterface dialogInterface) {
        this.f1364a.cancel(true);
    }
}
