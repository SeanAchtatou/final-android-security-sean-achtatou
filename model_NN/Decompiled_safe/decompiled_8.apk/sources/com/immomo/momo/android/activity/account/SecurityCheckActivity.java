package com.immomo.momo.android.activity.account;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.ao;
import com.immomo.momo.android.view.HeaderLayout;
import com.immomo.momo.g;
import com.immomo.momo.util.k;
import java.util.Timer;

public class SecurityCheckActivity extends ao implements View.OnClickListener {
    private HeaderLayout h = null;
    private TextView i = null;
    private View j = null;
    /* access modifiers changed from: private */
    public String k = PoiTypeDef.All;
    /* access modifiers changed from: private */
    public String l = PoiTypeDef.All;
    /* access modifiers changed from: private */
    public String m = PoiTypeDef.All;
    /* access modifiers changed from: private */
    public Timer n = null;
    private Animation o = null;

    static /* synthetic */ void b(SecurityCheckActivity securityCheckActivity) {
        String str = "使用注册时填写的手机，编辑短信内容 " + securityCheckActivity.k + " 发送到 " + securityCheckActivity.l;
        TextView textView = securityCheckActivity.i;
        String str2 = securityCheckActivity.l;
        String str3 = securityCheckActivity.k;
        int indexOf = str.indexOf(str2);
        int indexOf2 = str.indexOf(str3);
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(str);
        spannableStringBuilder.setSpan(new ForegroundColorSpan(g.c((int) R.color.blue)), indexOf, str2.length() + indexOf, 33);
        spannableStringBuilder.setSpan(new ForegroundColorSpan(g.c((int) R.color.blue)), indexOf2, str3.length() + indexOf2, 33);
        textView.setText(spannableStringBuilder);
        securityCheckActivity.j.setVisibility(0);
        ImageView imageView = (ImageView) securityCheckActivity.j.findViewById(R.id.rg_iv_loading);
        if (imageView.getDrawable() != null && securityCheckActivity.o == null) {
            securityCheckActivity.o = AnimationUtils.loadAnimation(securityCheckActivity, R.anim.loading);
            imageView.startAnimation(securityCheckActivity.o);
        }
    }

    /* access modifiers changed from: protected */
    public final void d() {
    }

    /* access modifiers changed from: protected */
    public final void f() {
        if (this.n == null) {
            this.n = new Timer();
            this.n.schedule(new aw(this), 0, 5000);
        }
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_back /*2131165286*/:
                finish();
                return;
            case R.id.btn_ok /*2131165289*/:
                f();
                a((CharSequence) "正在验证，请稍候...");
                return;
            case R.id.rg_bt_sendmsg /*2131165846*/:
                Intent intent = new Intent("android.intent.action.SENDTO", Uri.parse("smsto:" + this.l));
                intent.putExtra("sms_body", this.k);
                try {
                    startActivity(intent);
                    return;
                } catch (Exception e) {
                    com.immomo.momo.util.ao.b("该设备不支持短信息功能,请使用其他手机发送短信");
                    return;
                }
            default:
                return;
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_securitycheck);
        this.h = (HeaderLayout) findViewById(R.id.layout_header);
        this.h.setTitleText("账号安全验证");
        this.j = findViewById(R.id.rg_layout_loading);
        this.i = (TextView) findViewById(R.id.rg_tv_info);
        findViewById(R.id.btn_ok).setOnClickListener(this);
        findViewById(R.id.btn_back).setOnClickListener(this);
        findViewById(R.id.rg_bt_sendmsg).setOnClickListener(this);
        this.m = getIntent().getStringExtra("account");
        b(new ay(this, this));
        this.e.a((Object) "onCreate...");
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        new k("PO", "P133");
        if (this.n != null) {
            this.n.cancel();
            this.n = null;
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        new k("PI", "P133");
        f();
    }
}
