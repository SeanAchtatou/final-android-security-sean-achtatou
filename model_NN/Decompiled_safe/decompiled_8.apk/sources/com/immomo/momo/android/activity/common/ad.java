package com.immomo.momo.android.activity.common;

import android.support.v4.b.a;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.util.d;
import java.util.Comparator;

final class ad implements Comparator {
    ad() {
    }

    public final /* synthetic */ int compare(Object obj, Object obj2) {
        bf bfVar = (bf) obj;
        bf bfVar2 = (bf) obj2;
        if (a.a((CharSequence) bfVar.j)) {
            bfVar.j = d.a(bfVar.i.trim());
        }
        if (a.a((CharSequence) bfVar2.j)) {
            bfVar2.j = d.a(bfVar2.i.trim());
        }
        return bfVar.j.compareTo(bfVar2.j);
    }
}
