package com.immomo.momo.android.c;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.View;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import com.immomo.momo.R;
import com.immomo.momo.g;

final class q extends Handler {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ p f2391a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public q(p pVar, Looper looper) {
        super(looper);
        this.f2391a = pVar;
    }

    public final void handleMessage(Message message) {
        View a2;
        if (message.what == 35 && this.f2391a.b != null && !this.f2391a.b.isRecycled() && (a2 = this.f2391a.a()) != null) {
            Bitmap a3 = this.f2391a.b;
            if (!this.f2391a.e.equals(a2.getTag(R.id.tag_item_imageid))) {
                this.f2391a.j.a((Object) "miss-------------------555");
            } else if (a2 instanceof ImageSwitcher) {
                ((ImageSwitcher) a2).setImageDrawable(new BitmapDrawable(g.l(), a3));
            } else if (a2 instanceof ImageView) {
                Drawable drawable = ((ImageView) a2).getDrawable();
                if (drawable == null || !(drawable instanceof BitmapDrawable) || ((BitmapDrawable) drawable).getBitmap() != this.f2391a.b) {
                    ((ImageView) a2).setImageBitmap(a3);
                } else {
                    this.f2391a.j.a((Object) "miss-------------------444");
                }
            }
        }
    }
}
