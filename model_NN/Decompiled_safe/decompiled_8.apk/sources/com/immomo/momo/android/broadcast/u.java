package com.immomo.momo.android.broadcast;

import android.content.Context;
import com.immomo.momo.g;

public final class u extends b {

    /* renamed from: a  reason: collision with root package name */
    public static final String f2364a = (String.valueOf(g.h()) + ".action.grouplist.deletegroup");
    public static final String b = (String.valueOf(g.h()) + ".action.grouplist.addgroup");
    public static final String c = (String.valueOf(g.h()) + ".action.grouplist.banded");
    public static final String d = (String.valueOf(g.h()) + ".action.grouplist.pass");
    public static final String e = (String.valueOf(g.h()) + ".action.grouplist.reflush.profile");
    public static final String f = (String.valueOf(g.h()) + ".action.grouplist.reflush.party.info");
    public static final String g = (String.valueOf(g.h()) + ".action.grouplist.reflush.item");

    public u(Context context) {
        super(context);
        a(b, f2364a, e, g, f, c, d);
    }
}
