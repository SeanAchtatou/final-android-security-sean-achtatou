package com.immomo.momo.android.activity.maintab;

import android.content.DialogInterface;
import android.content.Intent;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.WelcomeActivity;
import com.immomo.momo.android.broadcast.m;

final class cm implements DialogInterface.OnClickListener {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public /* synthetic */ cd f1884a;

    cm(cd cdVar) {
        this.f1884a = cdVar;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        new Thread(new cn(this)).start();
        cd cdVar = this.f1884a;
        cd.L().l();
        cd cdVar2 = this.f1884a;
        Intent intent = new Intent(cd.H(), WelcomeActivity.class);
        intent.putExtra("model", 0);
        intent.addFlags(268435456);
        cd cdVar3 = this.f1884a;
        cd.H().startActivity(intent);
        this.f1884a.b((int) R.anim.zoom_exit);
        cd cdVar4 = this.f1884a;
        cd.c(new Intent(m.f2356a));
        this.f1884a.c().finish();
    }
}
