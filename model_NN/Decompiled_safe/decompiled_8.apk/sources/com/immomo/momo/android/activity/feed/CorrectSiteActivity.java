package com.immomo.momo.android.activity.feed;

import android.os.Bundle;
import android.view.KeyEvent;
import android.widget.EditText;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.ah;
import com.immomo.momo.android.view.HeaderLayout;
import com.immomo.momo.android.view.a.n;
import com.immomo.momo.android.view.bi;

public class CorrectSiteActivity extends ah {
    /* access modifiers changed from: private */
    public EditText h;
    private HeaderLayout i;
    private bi j;
    /* access modifiers changed from: private */
    public String k;

    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        super.a(bundle);
        setContentView((int) R.layout.activity_sitefeed_correctsite);
        this.i = (HeaderLayout) findViewById(R.id.layout_header);
        this.j = new bi(getApplicationContext()).b(R.string.submit);
        a(this.j, new a(this));
        this.i.setTitleText("地点纠错/反馈");
        this.h = (EditText) findViewById(R.id.correctsiteact_et);
        this.h.setFocusable(true);
        this.h.post(new b(this));
        this.k = getIntent().getStringExtra("sid");
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 != 4) {
            return super.onKeyDown(i2, keyEvent);
        }
        if (this.h.getText().toString().trim().equals(PoiTypeDef.All)) {
            finish();
        } else {
            n.a(this, (int) R.string.site_conrrection_dialog_content, new c(this), new d()).show();
        }
        return true;
    }
}
