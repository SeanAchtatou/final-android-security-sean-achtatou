package com.immomo.momo.android.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import com.immomo.momo.android.a.gv;
import com.immomo.momo.g;

public class AppIconMomoGridView extends LinearLayout {

    /* renamed from: a  reason: collision with root package name */
    protected f f2622a;
    private int b = 0;
    private gv c;
    private int d = 0;
    private int e = 0;
    private int f = ((int) (5.0f * g.k()));

    public AppIconMomoGridView(Context context) {
        super(context);
        setOrientation(1);
    }

    public AppIconMomoGridView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        setOrientation(1);
    }

    public int getItemCount() {
        if (this.c == null) {
            return 0;
        }
        return this.c.getCount();
    }

    public void setAdapter(gv gvVar) {
        int i;
        removeAllViews();
        this.f = (int) (5.0f * g.k());
        this.b = (int) (55.0f * g.k());
        this.d = (getMeasuredWidth() - this.f) / (this.b + this.f);
        this.e = (getMeasuredWidth() - ((this.b + this.f) * this.d)) / (this.d - 1);
        if (this.b != 0 && gvVar != null && gvVar.getCount() != 0) {
            this.c = gvVar;
            int count = gvVar.getCount() / this.d;
            int i2 = 0;
            while (i2 <= count && this.d * i2 < gvVar.getCount()) {
                LinearLayout linearLayout = new LinearLayout(getContext());
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2);
                if (i2 != 0) {
                    layoutParams.topMargin = (int) (10.0f * g.k());
                }
                addView(linearLayout, layoutParams);
                int i3 = 0;
                while (i3 < this.d && (i = (this.d * i2) + i3) < gvVar.getCount()) {
                    View view = gvVar.getView(i, null, null);
                    LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(this.b, -2);
                    if (i3 != 0) {
                        layoutParams2.leftMargin = this.e + this.f;
                    }
                    linearLayout.setOrientation(0);
                    linearLayout.addView(view, layoutParams2);
                    view.setOnClickListener(new b(this, i));
                    i3++;
                }
                i2++;
            }
        }
    }

    public void setOnMomoGridViewItemClickListener(f fVar) {
        this.f2622a = fVar;
    }
}
