package com.immomo.momo.android.view.a;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.android.a.a;
import java.util.List;

final class an extends a {
    public an(Context context, List list) {
        super(context, list);
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = this.c.inflate((int) R.layout.listitem_drakmenu, (ViewGroup) null);
        }
        ((TextView) view.findViewById(R.id.textview)).setText(getItem(i).toString());
        return view;
    }
}
