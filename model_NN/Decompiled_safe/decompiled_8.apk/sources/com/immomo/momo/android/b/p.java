package com.immomo.momo.android.b;

import android.location.Location;

public abstract class p {

    /* renamed from: a  reason: collision with root package name */
    private boolean f2334a = false;
    public boolean b = true;

    public final void a() {
        this.f2334a = true;
    }

    public abstract void a(Location location, int i, int i2, int i3);

    public final boolean b() {
        return this.f2334a;
    }
}
