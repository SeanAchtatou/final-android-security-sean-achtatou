package com.immomo.momo.android.a;

import android.view.View;
import android.widget.AdapterView;
import com.immomo.momo.R;
import com.immomo.momo.android.view.a.n;
import com.immomo.momo.service.bean.a.j;

final class ev implements AdapterView.OnItemClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ es f808a;
    private final /* synthetic */ String[] b;
    private final /* synthetic */ j c;

    ev(es esVar, String[] strArr, j jVar) {
        this.f808a = esVar;
        this.b = strArr;
        this.c = jVar;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        if ("设为管理员".equals(this.b[i])) {
            es.a(this.f808a, this.c);
        } else if ("撤销管理员".equals(this.b[i])) {
            es.b(this.f808a, this.c);
        } else if ("转让群组".equals(this.b[i])) {
            es.c(this.f808a, this.c);
        } else if ("移除".equals(this.b[i])) {
            n.a(this.f808a.d, this.f808a.d.getString(R.string.group_memberlist_delete_tip), new fc(this.f808a, this.c)).show();
        } else if ("移除并举报".equals(this.b[i])) {
            es.e(this.f808a, this.c);
        }
    }
}
