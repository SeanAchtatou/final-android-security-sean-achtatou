package com.immomo.momo.android.b;

import android.location.Location;
import java.util.List;
import mm.purchasesdk.PurchaseCode;

final class u implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ q f2339a;
    private final /* synthetic */ Object b;
    private final /* synthetic */ String c;
    private final /* synthetic */ String d;
    private final /* synthetic */ List e;
    private final /* synthetic */ List f;
    private final /* synthetic */ p g;
    private final /* synthetic */ boolean h;

    u(q qVar, Object obj, String str, String str2, List list, List list2, p pVar, boolean z) {
        this.f2339a = qVar;
        this.b = obj;
        this.c = str;
        this.d = str2;
        this.e = list;
        this.f = list2;
        this.g = pVar;
        this.h = z;
    }

    public final void run() {
        synchronized (this.b) {
            try {
                this.b.wait(60000);
                this.f2339a.f2335a.a(this.c);
                this.f2339a.f2335a.a(this.d);
                this.f2339a.f2335a.b();
                if (this.e.size() > 0 || this.f.size() > 0) {
                    Location location = this.f.size() > 0 ? (Location) this.f.get(this.f.size() - 1) : (Location) this.e.get(this.e.size() - 1);
                    if (this.h) {
                        new l(this.g, PurchaseCode.LOADCHANNEL_ERR).execute(location);
                    } else {
                        this.g.a(location, 0, 100, PurchaseCode.LOADCHANNEL_ERR);
                    }
                } else {
                    this.g.a(null, 0, PurchaseCode.QUERY_OK, PurchaseCode.LOADCHANNEL_ERR);
                }
            } catch (Exception e2) {
                this.f2339a.b.a((Throwable) e2);
                this.g.a(null, 0, PurchaseCode.QUERY_OK, PurchaseCode.LOADCHANNEL_ERR);
            }
        }
        return;
    }
}
