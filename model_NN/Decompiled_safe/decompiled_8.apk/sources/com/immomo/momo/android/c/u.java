package com.immomo.momo.android.c;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public final class u extends ThreadPoolExecutor {

    /* renamed from: a  reason: collision with root package name */
    private static final TimeUnit f2394a = TimeUnit.SECONDS;
    private static ThreadPoolExecutor b = null;
    private static ThreadPoolExecutor c = null;
    private static ExecutorService d = null;
    private static ThreadPoolExecutor e = null;
    private static ThreadPoolExecutor f = null;

    private u() {
        super(2, 10, 2, f2394a, new LinkedBlockingQueue(), new v((byte) 0));
    }

    public u(int i, int i2) {
        super(i, i2, 2, f2394a, new LinkedBlockingQueue(), new v((byte) 0));
    }

    public static ThreadPoolExecutor a() {
        return new u();
    }

    public static ThreadPoolExecutor b() {
        if (b == null) {
            b = new u(10, 10);
        }
        return b;
    }

    public static ThreadPoolExecutor c() {
        return new u(1, 1);
    }

    public static ThreadPoolExecutor d() {
        if (c == null) {
            c = new u(2, 2);
        }
        return c;
    }

    public static ExecutorService e() {
        if (d == null) {
            int availableProcessors = Runtime.getRuntime().availableProcessors();
            d = new u(availableProcessors, availableProcessors);
        }
        return d;
    }

    public static ThreadPoolExecutor f() {
        if (e == null) {
            e = new u(1, 1);
        }
        return e;
    }

    public static ThreadPoolExecutor g() {
        if (f == null) {
            f = new u(3, 3);
        }
        return f;
    }

    public static void h() {
        if (e != null) {
            try {
                e.shutdownNow();
            } catch (Exception e2) {
            }
            e = null;
        }
        if (f != null) {
            try {
                f.shutdownNow();
            } catch (Exception e3) {
            }
            f = null;
        }
        if (b != null) {
            try {
                b.shutdownNow();
            } catch (Exception e4) {
            }
            b = null;
        }
    }

    /* access modifiers changed from: protected */
    public final void finalize() {
        super.finalize();
    }
}
