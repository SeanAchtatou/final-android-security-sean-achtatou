package com.immomo.momo.android.activity.account;

import android.support.v4.b.a;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.util.ao;
import com.immomo.momo.util.k;
import com.immomo.momo.util.m;

public final class cr extends ab {

    /* renamed from: a  reason: collision with root package name */
    private bf f996a = null;
    private RegisterActivityWithP b = null;
    private EditText c = null;
    private EditText d = null;

    public cr(bf bfVar, View view, RegisterActivityWithP registerActivityWithP) {
        super(view);
        new m("test_momo", "[ -- StepSetPassword -- ]");
        this.f996a = bfVar;
        this.b = registerActivityWithP;
        registerActivityWithP.c(RegisterActivityWithP.k);
        this.c = (EditText) a((int) R.id.rg_et_pwd);
        this.d = (EditText) a((int) R.id.rg_et_pwd_confim);
        this.d.setOnEditorActionListener(new cs(this));
    }

    public final boolean a() {
        if (a(this.c)) {
            ao.e(R.string.reg_pwd_empty);
            this.b.a((TextView) this.c);
            return false;
        }
        String trim = this.c.getText().toString().trim();
        if (trim.length() < 6) {
            ao.b(String.format(g.a((int) R.string.reg_pwd_sizemin), "6"));
            this.b.a((TextView) this.c);
            this.c.selectAll();
            return false;
        } else if (trim.length() > 16) {
            ao.b(String.format(g.a((int) R.string.reg_pwd_sizemax), "16"));
            this.b.a((TextView) this.c);
            this.c.selectAll();
            return false;
        } else if (a(this.d)) {
            ao.e(R.string.reg_pwd_confim_empty);
            this.b.a((TextView) this.d);
            return false;
        } else {
            String trim2 = this.d.getText().toString().trim();
            if (trim2 == null || !trim2.equals(trim)) {
                ao.e(R.string.reg_pwd_confim_notmather);
                this.b.a((TextView) this.d);
                this.d.selectAll();
                return false;
            } else if (a.g(trim)) {
                ao.e(R.string.reg_pwd_isweak);
                this.b.a((TextView) this.c);
                this.c.selectAll();
                return false;
            } else {
                this.f996a.f3011a = trim;
                return true;
            }
        }
    }

    public final void c() {
        if (a()) {
            this.b.u();
        }
    }

    public final void d() {
        super.d();
        this.b.c(2);
    }

    public final void e() {
        new k("PI", "P123").e();
    }

    public final void f() {
        new k("PO", "P123").e();
    }
}
