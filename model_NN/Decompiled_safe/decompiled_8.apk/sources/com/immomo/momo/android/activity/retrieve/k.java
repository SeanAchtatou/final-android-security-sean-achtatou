package com.immomo.momo.android.activity.retrieve;

import android.content.Context;
import com.immomo.momo.R;
import com.immomo.momo.a.i;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.w;

final class k extends d {

    /* renamed from: a  reason: collision with root package name */
    private String[] f2117a;
    private /* synthetic */ InputPhoneNumberActivity c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public k(InputPhoneNumberActivity inputPhoneNumberActivity, Context context) {
        super(context);
        this.c = inputPhoneNumberActivity;
    }

    /* access modifiers changed from: protected */
    public final Object a(Object... objArr) {
        if ("+86".equals(this.c.l)) {
            this.f2117a = w.a().c(this.c.l, this.c.m, this.c.v);
        } else {
            w.a().a(3, this.c.l, this.c.m, (String) null, this.c.v);
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.c.A = new v(this.c, (int) R.string.press);
        this.c.A.setCancelable(true);
        this.c.A.setOnCancelListener(new l(this));
        this.c.A.show();
        super.a();
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        if (exc instanceof i) {
            this.c.f();
        } else {
            super.a(exc);
        }
    }

    /* access modifiers changed from: protected */
    public final void a(Object obj) {
        this.c.n = true;
        InputPhoneNumberActivity.a(this.c, this.f2117a);
        super.a(obj);
    }

    /* access modifiers changed from: protected */
    public final void b() {
        if (this.c.A != null) {
            this.c.A.dismiss();
            this.c.A = (v) null;
        }
        super.b();
    }
}
