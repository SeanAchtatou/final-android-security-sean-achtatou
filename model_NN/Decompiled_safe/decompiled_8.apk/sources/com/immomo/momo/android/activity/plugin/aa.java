package com.immomo.momo.android.activity.plugin;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.immomo.momo.R;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.util.ao;

final class aa extends WebViewClient {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ BindSinaActivity f2029a;

    private aa(BindSinaActivity bindSinaActivity) {
        this.f2029a = bindSinaActivity;
    }

    /* synthetic */ aa(BindSinaActivity bindSinaActivity, byte b) {
        this(bindSinaActivity);
    }

    public final void onLoadResource(WebView webView, String str) {
        super.onLoadResource(webView, str);
    }

    public final void onPageFinished(WebView webView, String str) {
        super.onPageFinished(webView, str);
        this.f2029a.p();
        if (Build.VERSION.SDK_INT <= 10 && str.indexOf("code=") >= 0) {
            this.f2029a.m = Uri.parse(str).getQueryParameter("code");
            this.f2029a.q = new w(this.f2029a, this.f2029a, null, this.f2029a.r);
            this.f2029a.q.execute(new Object[0]);
        }
    }

    public final void onPageStarted(WebView webView, String str, Bitmap bitmap) {
        super.onPageStarted(webView, str, bitmap);
        if (this.f2029a.l == null) {
            this.f2029a.l = new v(this.f2029a);
        }
        this.f2029a.l.a("正在加载，请稍候...");
        this.f2029a.a(this.f2029a.l);
    }

    public final void onReceivedError(WebView webView, int i, String str, String str2) {
        if (i == -2) {
            ao.e(R.string.errormsg_network_unfind);
        } else {
            ao.e(R.string.errormsg_server);
        }
        this.f2029a.p();
    }

    public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
        if (Build.VERSION.SDK_INT <= 10) {
            if (str.indexOf(this.f2029a.i) >= 0) {
                return true;
            }
        } else if (str.indexOf("code=") >= 0) {
            this.f2029a.m = Uri.parse(str).getQueryParameter("code");
            this.f2029a.q = new w(this.f2029a, this.f2029a, null, this.f2029a.r);
            this.f2029a.q.execute(new Object[0]);
            return true;
        }
        return super.shouldOverrideUrlLoading(webView, str);
    }
}
