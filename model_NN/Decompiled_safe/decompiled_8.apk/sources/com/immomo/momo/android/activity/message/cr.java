package com.immomo.momo.android.activity.message;

import android.content.Context;
import com.immomo.momo.android.c.d;
import java.util.Collection;
import java.util.List;

final class cr extends d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ MultiChatActivity f1977a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public cr(MultiChatActivity multiChatActivity, Context context) {
        super(context);
        this.f1977a = multiChatActivity;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        return this.f1977a.ah();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.android.a.a.aa.a(int, java.util.Collection):void
     arg types: [int, java.util.List]
     candidates:
      com.immomo.momo.android.a.a.a(int, java.lang.Object):void
      com.immomo.momo.android.a.a.a(java.lang.Object, int):void
      com.immomo.momo.android.a.a.a(java.util.Collection, boolean):void
      com.immomo.momo.android.a.a.aa.a(int, java.util.Collection):void */
    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        List list = (List) obj;
        this.f1977a.T.a(0, (Collection) list);
        this.f1977a.l.setSelectionFromTop(list.size() + 2, this.f1977a.l.getLoadingHeigth());
    }

    /* access modifiers changed from: protected */
    public final void b() {
        if (!this.f1977a.W) {
            this.f1977a.l.a();
        }
        this.f1977a.l.n();
    }
}
