package com.immomo.momo.android.activity;

import android.webkit.DownloadListener;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.util.g;

final class ms implements DownloadListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ WebviewActivity f2010a;

    ms(WebviewActivity webviewActivity) {
        this.f2010a = webviewActivity;
    }

    public final void onDownloadStart(String str, String str2, String str3, String str4, long j) {
        this.f2010a.e.a((Object) ("download -> " + str));
        g.a(this.f2010a, str, PoiTypeDef.All, str4);
    }
}
