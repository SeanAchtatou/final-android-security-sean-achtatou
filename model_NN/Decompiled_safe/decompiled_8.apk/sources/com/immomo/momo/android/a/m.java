package com.immomo.momo.android.a;

import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;

final class m extends Handler {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ l f908a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    m(l lVar, Looper looper) {
        super(looper);
        this.f908a = lVar;
    }

    public final void handleMessage(Message message) {
        this.f908a.b.setImageBitmap((Bitmap) message.obj);
    }
}
