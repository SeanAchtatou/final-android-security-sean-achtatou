package com.immomo.momo.android.activity.group;

import android.content.Intent;
import android.view.View;
import com.immomo.momo.android.activity.ImageBrowserActivity;

final class de implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ GroupProfileActivity f1602a;

    de(GroupProfileActivity groupProfileActivity) {
        this.f1602a = groupProfileActivity;
    }

    public final void onClick(View view) {
        this.f1602a.e.a((Object) "avatar cover clickded");
        dl dlVar = (dl) view.getTag();
        if (dlVar != null) {
            String[] strArr = this.f1602a.r.F;
            Intent intent = new Intent(this.f1602a, ImageBrowserActivity.class);
            intent.putExtra("array", strArr);
            intent.putExtra("imagetype", "avator");
            intent.putExtra("index", dlVar.b);
            this.f1602a.startActivity(intent);
        }
    }
}
