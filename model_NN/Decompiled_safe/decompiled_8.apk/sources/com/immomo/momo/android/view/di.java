package com.immomo.momo.android.view;

import android.os.Build;
import android.view.View;
import com.immomo.momo.R;
import com.immomo.momo.service.bean.c.d;
import com.immomo.momo.util.ao;

final class di implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ TiebaGuidView f2778a;
    private final /* synthetic */ View b;
    private final /* synthetic */ int c;

    di(TiebaGuidView tiebaGuidView, View view, int i) {
        this.f2778a = tiebaGuidView;
        this.b = view;
        this.c = i;
    }

    public final void onClick(View view) {
        View findViewById = this.b.findViewById(R.id.tiebacategory_item_img_click);
        View findViewById2 = this.b.findViewById(R.id.tiebacategory_item_img_layout);
        if (this.f2778a.b.contains(((d) this.f2778a.a(this.c)).f3019a)) {
            this.f2778a.b.remove(((d) this.f2778a.a(this.c)).f3019a);
            if (Build.VERSION.SDK_INT > 10) {
                TiebaGuidView tiebaGuidView = this.f2778a;
                TiebaGuidView.a(findViewById, findViewById2);
            } else {
                findViewById.setVisibility(8);
                findViewById2.setBackgroundResource(R.drawable.bglistitem_selector_tiebaguide_select);
            }
        } else if (this.f2778a.b.size() >= 20) {
            ao.b("最多可以加入20个陌陌吧");
            return;
        } else {
            this.f2778a.b.add(((d) this.f2778a.a(this.c)).f3019a);
            if (Build.VERSION.SDK_INT > 10) {
                TiebaGuidView tiebaGuidView2 = this.f2778a;
                TiebaGuidView.b(findViewById, findViewById2);
            } else {
                findViewById.setVisibility(0);
                findViewById2.setBackgroundResource(R.drawable.bg_cover_tiebaguide_outerpress);
            }
        }
        this.f2778a.c.a(this.c, this.b);
    }
}
