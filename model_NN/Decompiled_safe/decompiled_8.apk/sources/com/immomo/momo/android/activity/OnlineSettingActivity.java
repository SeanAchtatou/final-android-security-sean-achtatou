package com.immomo.momo.android.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import com.immomo.momo.R;
import com.immomo.momo.android.view.HeaderLayout;
import com.immomo.momo.android.view.a.n;
import com.immomo.momo.android.view.a.v;

public class OnlineSettingActivity extends ah implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {
    private HeaderLayout h = null;
    private RadioButton i = null;
    private RadioButton j = null;
    private RadioButton k = null;
    /* access modifiers changed from: private */
    public boolean l = false;
    /* access modifiers changed from: private */
    public v m = null;
    /* access modifiers changed from: private */
    public boolean n = false;

    static /* synthetic */ void b(OnlineSettingActivity onlineSettingActivity) {
        boolean z = true;
        onlineSettingActivity.i.setChecked(onlineSettingActivity.g.l == 0);
        onlineSettingActivity.j.setChecked(onlineSettingActivity.g.l == 1);
        RadioButton radioButton = onlineSettingActivity.k;
        if (onlineSettingActivity.g.l != 2) {
            z = false;
        }
        radioButton.setChecked(z);
    }

    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        super.a(bundle);
        setContentView((int) R.layout.activity_setting_onlinestatus);
        this.h = (HeaderLayout) findViewById(R.id.layout_header);
        this.h.setTitleText(getString(R.string.setting_hide_title));
        this.i = (RadioButton) findViewById(R.id.setting_rb_online);
        this.j = (RadioButton) findViewById(R.id.setting_rb_yinshen_stranger);
        this.k = (RadioButton) findViewById(R.id.setting_rb_yinshen_all);
        this.i.setOnCheckedChangeListener(this);
        this.j.setOnCheckedChangeListener(this);
        this.k.setOnCheckedChangeListener(this);
        findViewById(R.id.setting_layout_online).setOnClickListener(this);
        findViewById(R.id.setting_layout_yinshen_stranger).setOnClickListener(this);
        findViewById(R.id.setting_layout_yinshen_all).setOnClickListener(this);
    }

    /* access modifiers changed from: protected */
    public final void d() {
    }

    public void onCheckedChanged(CompoundButton compoundButton, boolean z) {
        if (!this.l) {
            switch (compoundButton.getId()) {
                case R.id.setting_rb_online /*2131165910*/:
                    boolean isChecked = compoundButton.isChecked();
                    if (this.n) {
                        this.n = false;
                        return;
                    } else if (isChecked) {
                        this.j.setChecked(false);
                        this.k.setChecked(false);
                        new go(this, 0).execute(new Object[0]);
                        return;
                    } else {
                        return;
                    }
                case R.id.setting_layout_yinshen_stranger /*2131165911*/:
                case R.id.setting_layout_yinshen_all /*2131165913*/:
                default:
                    return;
                case R.id.setting_rb_yinshen_stranger /*2131165912*/:
                    boolean isChecked2 = compoundButton.isChecked();
                    if (this.n) {
                        this.n = false;
                        return;
                    } else if (isChecked2) {
                        this.i.setChecked(false);
                        this.k.setChecked(false);
                        if (this.g.l == 0) {
                            n a2 = n.a(this, "\"" + getString(R.string.setting_hide_part) + "\"模式将使您不能出现在其他人的附近列表上，确定要这样做吗？", "确认", "取消", new gi(this), new gj(this));
                            a2.setOnCancelListener(new gk(this));
                            a2.show();
                            return;
                        }
                        new go(this, 1).execute(new Object[0]);
                        return;
                    } else {
                        return;
                    }
                case R.id.setting_rb_yinshen_all /*2131165914*/:
                    boolean isChecked3 = compoundButton.isChecked();
                    if (this.n) {
                        this.n = false;
                        return;
                    } else if (isChecked3) {
                        this.i.setChecked(false);
                        this.j.setChecked(false);
                        if (this.g.l == 0) {
                            n a3 = n.a(this, "\"" + getString(R.string.setting_hide_all) + "\"模式下所有人都不更新与你的距离，确定要这样做吗？", "确认", "取消", new gl(this), new gm(this));
                            a3.setOnCancelListener(new gn(this));
                            a3.show();
                            return;
                        }
                        new go(this, 2).execute(new Object[0]);
                        return;
                    } else {
                        return;
                    }
            }
        }
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.setting_layout_online /*2131165909*/:
                this.i.toggle();
                return;
            case R.id.setting_rb_online /*2131165910*/:
            case R.id.setting_rb_yinshen_stranger /*2131165912*/:
            default:
                return;
            case R.id.setting_layout_yinshen_stranger /*2131165911*/:
                this.j.toggle();
                return;
            case R.id.setting_layout_yinshen_all /*2131165913*/:
                this.k.toggle();
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        boolean z = true;
        super.onStart();
        this.l = true;
        this.i.setChecked(this.g.l == 0);
        this.j.setChecked(this.g.l == 1);
        RadioButton radioButton = this.k;
        if (this.g.l != 2) {
            z = false;
        }
        radioButton.setChecked(z);
        this.l = false;
    }
}
