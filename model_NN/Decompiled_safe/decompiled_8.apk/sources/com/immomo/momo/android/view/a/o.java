package com.immomo.momo.android.view.a;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.immomo.momo.R;
import com.immomo.momo.g;
import java.util.Arrays;

public class o extends n implements AdapterView.OnItemClickListener {
    int b;
    private ListView c;
    private al d;

    public o(Context context) {
        super(context);
        this.c = null;
        this.d = null;
        this.b = -1;
        b();
        View inflate = g.o().inflate((int) R.layout.include_dialog_simplelist, (ViewGroup) null);
        setContentView(inflate);
        this.c = (ListView) inflate.findViewById(R.id.listview);
        this.c.setOnItemClickListener(this);
    }

    public o(Context context, int i) {
        this(context);
        a(new p(this, getContext(), Arrays.asList(context.getResources().getStringArray(i))));
    }

    public o(Context context, CharSequence[] charSequenceArr, int i) {
        this(context, charSequenceArr);
        this.b = i;
    }

    public o(Context context, Object[] objArr) {
        this(context);
        a(new p(this, getContext(), Arrays.asList(objArr)));
    }

    public final void a(ListAdapter listAdapter) {
        this.c.setAdapter(listAdapter);
    }

    public final void a(al alVar) {
        this.d = alVar;
    }

    public void onItemClick(AdapterView adapterView, View view, int i, long j) {
        if (this.d != null) {
            this.d.a(i);
        }
        dismiss();
    }
}
