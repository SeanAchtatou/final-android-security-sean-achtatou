package com.immomo.momo.android.activity.tieba;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.protocol.a.a.q;
import com.immomo.momo.protocol.a.v;

final class ef extends d {

    /* renamed from: a  reason: collision with root package name */
    private q f2258a;
    private /* synthetic */ dw c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ef(dw dwVar, Context context) {
        super(context);
        this.c = dwVar;
    }

    /* access modifiers changed from: protected */
    public final Object a(Object... objArr) {
        this.f2258a = v.a().a(this.c.V.b().b());
        return null;
    }

    /* access modifiers changed from: protected */
    public final void a(Object obj) {
        this.c.V.b().b(this.f2258a.a());
        if (!this.f2258a.b()) {
            this.c.O.g();
        }
        this.c.V.notifyDataSetChanged();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.c.aa.e();
    }
}
