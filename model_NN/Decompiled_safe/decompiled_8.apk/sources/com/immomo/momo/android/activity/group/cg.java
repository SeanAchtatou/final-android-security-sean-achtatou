package com.immomo.momo.android.activity.group;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.b.a;
import android.view.View;

final class cg implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ GroupPartyActivity f1580a;

    cg(GroupPartyActivity groupPartyActivity) {
        this.f1580a = groupPartyActivity;
    }

    public final void onClick(View view) {
        Bundle bundle = (Bundle) view.getTag();
        if (bundle != null && !a.a((CharSequence) bundle.getString("phoneNum"))) {
            try {
                this.f1580a.startActivity(new Intent("android.intent.action.DIAL", Uri.parse("tel:" + bundle.getString("phoneNum"))));
            } catch (Exception e) {
                this.f1580a.e.a((Throwable) e);
            }
        }
    }
}
