package com.immomo.momo.android.activity.tieba;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.protocol.a.v;

final class ed extends d {

    /* renamed from: a  reason: collision with root package name */
    private String f2256a;
    private /* synthetic */ dw c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ed(dw dwVar, Context context, String str) {
        super(context);
        this.c = dwVar;
        if (dwVar.ab != null) {
            dwVar.ab.cancel(true);
        }
        dwVar.ab = this;
        this.f2256a = str;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        return v.a().b(this.f2256a);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        a((String) obj);
        this.c.O.h();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.c.ab = (ed) null;
        this.c.N();
    }
}
