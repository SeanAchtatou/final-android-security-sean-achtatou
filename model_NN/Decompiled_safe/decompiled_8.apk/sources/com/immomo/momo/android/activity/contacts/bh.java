package com.immomo.momo.android.activity.contacts;

import android.content.Context;
import android.support.v4.b.a;
import com.immomo.momo.android.c.d;
import com.immomo.momo.g;
import com.immomo.momo.protocol.a.w;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

final class bh extends d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ aw f1163a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public bh(aw awVar, Context context) {
        super(context);
        this.f1163a = awVar;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        this.f1163a.X = this.f1163a.W;
        this.f1163a.W = 0;
        ArrayList arrayList = new ArrayList();
        int a2 = w.a().a(this.f1163a.W, arrayList);
        this.f1163a.Z.b(arrayList);
        g.q().x = a2;
        this.f1163a.Z.d(g.q().x, g.q().h);
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        List list = (List) obj;
        super.a(list);
        this.f1163a.R.k();
        if (list.size() > 0) {
            this.f1163a.P();
            aw awVar = this.f1163a;
            awVar.W = awVar.W + this.f1163a.T.size();
            this.f1163a.R.setLastFlushTime(this.f1163a.Y);
            this.f1163a.N.c("lasttime_friends_success", a.c(this.f1163a.Y));
            this.f1163a.Q();
            aw awVar2 = this.f1163a;
            List unused = this.f1163a.T;
            aw.q(awVar2);
            return;
        }
        this.f1163a.W = this.f1163a.X;
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.f1163a.Y = new Date();
        this.f1163a.N.c("lasttime_friends", a.c(this.f1163a.Y));
        this.f1163a.aa = null;
        this.f1163a.R.n();
    }
}
