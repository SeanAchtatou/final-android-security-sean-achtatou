package com.immomo.momo.android.activity.event;

import android.content.DialogInterface;

final class aw implements DialogInterface.OnCancelListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ av f1366a;

    aw(av avVar) {
        this.f1366a = avVar;
    }

    public final void onCancel(DialogInterface dialogInterface) {
        this.f1366a.cancel(true);
        this.f1366a.g.finish();
    }
}
