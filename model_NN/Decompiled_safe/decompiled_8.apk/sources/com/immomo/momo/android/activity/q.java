package com.immomo.momo.android.activity;

import android.content.Intent;
import android.view.View;

final class q implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ AlipayUserWelcomeActivity f2104a;

    q(AlipayUserWelcomeActivity alipayUserWelcomeActivity) {
        this.f2104a = alipayUserWelcomeActivity;
    }

    public final void onClick(View view) {
        Intent intent = new Intent(this.f2104a, AboutTabsActivity.class);
        intent.putExtra("showindex", 0);
        this.f2104a.startActivity(intent);
    }
}
