package com.immomo.momo.android.activity.contacts;

import android.support.v4.b.a;
import com.immomo.momo.service.bean.e;
import com.immomo.momo.util.d;
import java.util.Comparator;

final class o implements Comparator {
    o() {
    }

    public final /* synthetic */ int compare(Object obj, Object obj2) {
        e eVar = (e) obj;
        e eVar2 = (e) obj2;
        if (a.a((CharSequence) eVar.e)) {
            eVar.e = d.a(eVar.d);
        }
        if (a.a((CharSequence) eVar2.e)) {
            eVar2.e = d.a(eVar2.d);
        }
        return eVar.e.compareTo(eVar2.e);
    }
}
