package com.immomo.momo.android.activity.message;

import android.content.BroadcastReceiver;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.RelativeLayout;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.a.a.l;
import com.immomo.momo.android.a.ee;
import com.immomo.momo.android.activity.an;
import com.immomo.momo.android.activity.d;
import com.immomo.momo.android.activity.group.GroupProfileActivity;
import com.immomo.momo.android.broadcast.i;
import com.immomo.momo.android.broadcast.u;
import com.immomo.momo.android.view.LoadingButton;
import com.immomo.momo.android.view.MomoRefreshListView;
import com.immomo.momo.android.view.a.at;
import com.immomo.momo.android.view.a.n;
import com.immomo.momo.g;
import com.immomo.momo.service.ag;
import com.immomo.momo.service.aq;
import com.immomo.momo.service.bean.Message;
import com.immomo.momo.service.bean.a.a;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.service.w;
import com.immomo.momo.service.y;
import com.immomo.momo.util.k;
import com.immomo.momo.util.m;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ThreadPoolExecutor;
import mm.purchasesdk.PurchaseCode;
import org.json.JSONObject;

public class GroupChatActivity extends a {
    protected m L = new m("test_momo", "[--- from GroupChatActivity ---]");
    private int M = 0;
    /* access modifiers changed from: private */
    public aq N;
    private w O;
    /* access modifiers changed from: private */
    public y P;
    /* access modifiers changed from: private */
    public a Q;
    /* access modifiers changed from: private */
    public ee R;
    private com.immomo.momo.android.broadcast.w S = null;
    private i T = null;
    private u U = null;
    private ImageView V;
    private View W;
    private ImageView X;
    /* access modifiers changed from: private */
    public View Y;
    /* access modifiers changed from: private */
    public MomoRefreshListView Z;
    /* access modifiers changed from: private */
    public RelativeLayout aa;
    /* access modifiers changed from: private */
    public Date ab;
    /* access modifiers changed from: private */
    public l ac;
    private ThreadPoolExecutor ad = new com.immomo.momo.android.c.u(1, 2);
    /* access modifiers changed from: private */
    public com.immomo.momo.service.bean.a.i ae = null;
    /* access modifiers changed from: private */
    public boolean af = false;
    private at ag = null;
    /* access modifiers changed from: private */
    public boolean ah = false;
    /* access modifiers changed from: private */
    public bt ai;

    public GroupChatActivity() {
        this.L.a((Object) "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~MessageActivity created....");
    }

    private void a(BroadcastReceiver broadcastReceiver) {
        if (broadcastReceiver != null) {
            unregisterReceiver(broadcastReceiver);
        }
    }

    /* access modifiers changed from: private */
    public List ah() {
        ArrayList arrayList = (ArrayList) this.O.b(this.Q.b, this.ac.getCount(), 21);
        if (arrayList.size() > 20) {
            arrayList.remove(0);
            this.af = true;
        } else {
            this.af = false;
        }
        Iterator it = arrayList.iterator();
        boolean z = false;
        while (it.hasNext()) {
            Message message = (Message) it.next();
            if (message.receive) {
                if (message.status == 5 || message.status == 9 || message.status == 13) {
                    this.F.add(message.msgId);
                    if (message.status == 5) {
                        z = true;
                    }
                }
                i(message);
                message.status = 4;
            } else if (message.status == 8) {
                com.immomo.momo.android.b.m.a(message.msgId).a(new x(this, message));
            }
        }
        if (this.ac.isEmpty() && z) {
            g.d().o();
        }
        ai();
        return arrayList;
    }

    private void ai() {
        if (this.F != null && this.F.size() > 0) {
            String[] strArr = (String[]) this.F.toArray(new String[0]);
            this.O.a(strArr);
            g.d().a(this.Q.b, strArr, 2);
            this.F.clear();
        }
    }

    /* access modifiers changed from: private */
    public void aj() {
        if (!this.ah) {
            b(false);
            AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
            alphaAnimation.setDuration(300);
            alphaAnimation.setFillAfter(false);
            this.ah = true;
            alphaAnimation.setAnimationListener(new bj(this));
            this.Y.startAnimation(alphaAnimation);
            TranslateAnimation translateAnimation = new TranslateAnimation(1, -0.5f, 1, 0.0f, 1, 0.0f, 1, 0.0f);
            translateAnimation.setDuration(300);
            translateAnimation.setFillAfter(true);
            translateAnimation.setInterpolator(this, 17432582);
            this.X.startAnimation(translateAnimation);
            TranslateAnimation translateAnimation2 = new TranslateAnimation(1, 0.0f, 1, -1.0f, 1, 0.0f, 1, 0.0f);
            translateAnimation2.setDuration(300);
            translateAnimation2.setFillAfter(false);
            translateAnimation2.setInterpolator(this, 17432582);
            translateAnimation2.setAnimationListener(new bk(this));
            this.aa.startAnimation(translateAnimation2);
        }
    }

    /* access modifiers changed from: private */
    public void b(boolean z) {
        if (!android.support.v4.b.a.a((CharSequence) this.Q.c)) {
            setTitle(this.Q.c);
        } else {
            setTitle(this.Q.b);
        }
        if (z) {
            this.m.setSubTitleText(String.valueOf(this.Q.k) + "位群成员");
        } else if (android.support.v4.b.a.f(this.Q.K)) {
            this.m.setSubTitleText(this.Q.K);
        } else {
            this.m.setSubTitleText(PoiTypeDef.All);
        }
    }

    private void h(Message message) {
        if (message != null) {
            if (message.receive) {
                message.status = 4;
            }
            i(message);
        }
    }

    private void i(Message message) {
        if (message.remoteUser == null) {
            if (!android.support.v4.b.a.a((CharSequence) message.remoteId)) {
                message.remoteUser = this.N.b(message.remoteId);
                if (message.remoteUser == null) {
                    message.remoteUser = new bf(message.remoteId);
                    this.ad.execute(new bv(this, message));
                }
            } else {
                message.remoteUser = new bf();
            }
        }
        message.remoteUser.setImageMultipleDiaplay(true);
    }

    static /* synthetic */ void l(GroupChatActivity groupChatActivity) {
        groupChatActivity.b(true);
        if (groupChatActivity.R == null) {
            groupChatActivity.ab = groupChatActivity.o().b("gmemberlist_lasttime_success" + groupChatActivity.i);
            groupChatActivity.R = new ee(groupChatActivity, groupChatActivity.P.b(groupChatActivity.i));
            groupChatActivity.Z.getFooterViewButton().setOnProcessListener(new bg(groupChatActivity));
            groupChatActivity.Z.setAdapter((ListAdapter) groupChatActivity.R);
            groupChatActivity.Z.setOnItemClickListener(new bh(groupChatActivity));
        }
        if (groupChatActivity.R.isEmpty() || groupChatActivity.ab == null || new Date().getTime() - groupChatActivity.ab.getTime() > 900000) {
            groupChatActivity.b(new bt(groupChatActivity, groupChatActivity));
        }
        AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
        alphaAnimation.setDuration(300);
        alphaAnimation.setFillAfter(true);
        groupChatActivity.Y.startAnimation(alphaAnimation);
        groupChatActivity.Y.setVisibility(0);
        TranslateAnimation translateAnimation = new TranslateAnimation(1, 0.0f, 1, -0.5f, 1, 0.0f, 1, 0.0f);
        translateAnimation.setDuration(300);
        translateAnimation.setFillAfter(true);
        translateAnimation.setInterpolator(groupChatActivity, 17432582);
        groupChatActivity.X.startAnimation(translateAnimation);
        TranslateAnimation translateAnimation2 = new TranslateAnimation(1, -1.0f, 1, 0.0f, 1, 0.0f, 1, 0.0f);
        translateAnimation2.setDuration(300);
        translateAnimation2.setFillAfter(true);
        translateAnimation2.setInterpolator(groupChatActivity, 17432582);
        groupChatActivity.aa.startAnimation(translateAnimation2);
        groupChatActivity.aa.setVisibility(0);
        groupChatActivity.aa.setFocusable(true);
        LoadingButton loadingButton = (LoadingButton) groupChatActivity.findViewById(R.id.groupchat_loadmore);
        loadingButton.setOnProcessListener(new bi(groupChatActivity, loadingButton));
    }

    /* access modifiers changed from: protected */
    public final void O() {
        D();
        this.k = new ag();
        this.N = new aq();
        this.O = new w();
        this.P = new y();
        this.z = (InputMethodManager) getSystemService("input_method");
        this.A = (AudioManager) getSystemService("audio");
    }

    /* access modifiers changed from: protected */
    public final void P() {
        this.f.setImageMultipleDiaplay(true);
        this.Q = this.P.e(u());
        if (this.Q == null) {
            this.Q = new a(getIntent().getStringExtra("remoteGroupID"));
            this.Q.c = this.Q.b;
            new Thread(new bq(this)).start();
        }
        if (android.support.v4.b.a.f(this.Q.K)) {
            this.m.setSubTitleText(this.Q.K);
        }
        b(false);
        this.L.a((Object) ("bothRelation=" + this.E));
        this.ae = this.g.c(this.Q.b);
    }

    /* access modifiers changed from: protected */
    public final void Q() {
        this.ac = new l(this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.android.a.a.aa.a(int, java.util.Collection):void
     arg types: [int, java.util.List]
     candidates:
      com.immomo.momo.android.a.a.a(int, java.lang.Object):void
      com.immomo.momo.android.a.a.a(java.lang.Object, int):void
      com.immomo.momo.android.a.a.a(java.util.Collection, boolean):void
      com.immomo.momo.android.a.a.aa.a(int, java.util.Collection):void */
    /* access modifiers changed from: protected */
    public final void R() {
        if (!((Boolean) this.g.b("tips_1001", false)).booleanValue() && this.ae.f2978a) {
            this.M = this.O.e(this.Q.b);
            aa();
            if (this.M >= 50) {
                an anVar = new an(1001, "群消息太多，可设置本群消息不提醒");
                anVar.b();
                b(anVar);
            }
        }
        this.ac.b();
        this.ac.a(0, (Collection) ah());
        if (!this.af) {
            this.l.a();
        }
        this.O.a(this.Q.b);
        this.l.setAdapter((ListAdapter) this.ac);
    }

    /* access modifiers changed from: protected */
    public final void S() {
        a(800, "actions.gmessage", "actions.message.status", "actions.glocalmsg", "actions.emoteupdates", "actions.groupfeed");
        this.T = new i(this);
        this.S = new com.immomo.momo.android.broadcast.w(this);
        this.U = new u(this);
        this.U.a(new bo(this));
        this.T.a(new bp(this));
    }

    /* access modifiers changed from: protected */
    public final void T() {
        ai();
        g.d().o();
    }

    /* access modifiers changed from: protected */
    public final int U() {
        return R.layout.activity_group_chat;
    }

    /* access modifiers changed from: protected */
    public final void V() {
        if (this.ag == null || !this.ag.g()) {
            A();
            z();
            String[] strArr = {PoiTypeDef.All, "语音收听方式", "设置聊天背景"};
            if (this.ae == null || this.ae.f2978a) {
                strArr[0] = "关闭提醒";
            } else {
                strArr[0] = "开启提醒";
            }
            this.ag = new at(this, this.n, strArr);
            this.ag.a(at.b);
            this.ag.a(new bs(this));
            this.ag.d();
        }
    }

    /* access modifiers changed from: protected */
    public final void W() {
        Intent intent = new Intent();
        intent.setClass(this, GroupProfileActivity.class);
        intent.putExtra("tag", "local");
        intent.putExtra("gid", getIntent().getStringExtra("remoteGroupID"));
        intent.setFlags(603979776);
        startActivity(intent);
    }

    /* access modifiers changed from: protected */
    public final void X() {
        this.af = false;
        b(new bu(this, this));
    }

    public final void Y() {
        this.j.sendEmptyMessage(PurchaseCode.BILL_DIALOG_SHOWERROR);
    }

    /* access modifiers changed from: protected */
    public final void Z() {
        this.E = this.P.c(this.f.h, this.Q.b) && this.Q.G != 4;
        this.L.b((Object) ("bothRelation:" + this.E));
        if (this.E) {
            M();
            ag();
            return;
        }
        L();
        ag();
        new Thread(new be(this)).start();
    }

    /* access modifiers changed from: protected */
    public final Message a(File file) {
        ce.a();
        return ce.a(file, this.f, this.Q.b, 2, this.f.b() ? 1 : 0);
    }

    public final void a(Intent intent, int i, Bundle bundle, String str) {
        if (intent.getComponent() != null && d.g(intent.getComponent().getClassName())) {
            intent.putExtra("afromname", this.Q.d());
        }
        super.a(intent, i, bundle, str);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.service.bean.at.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, boolean]
     candidates:
      com.immomo.momo.service.bean.at.a(android.content.Context, java.lang.String):com.immomo.momo.service.bean.at
      com.immomo.momo.service.bean.at.a(java.lang.Integer, java.lang.Integer):void
      com.immomo.momo.service.bean.at.a(java.lang.String, java.util.Date):void
      com.immomo.momo.service.bean.at.a(java.lang.String, java.lang.Object):void */
    /* access modifiers changed from: protected */
    public final void a(an anVar) {
        if (anVar != null && anVar.a() == 1001) {
            a(n.a(this, (int) R.string.tips_group, (int) R.string.tips_btn_goto, (int) R.string.tips_btn_nevermind, new br(this), (DialogInterface.OnClickListener) null));
            this.g.a("tips_" + anVar.a(), (Object) true);
            c(anVar);
        }
    }

    public final boolean a(Bundle bundle, String str) {
        if (!"actions.gmessage".equals(str)) {
            if ("actions.message.status".equals(str)) {
                if (bundle.getInt("chattype") != 2) {
                    return false;
                }
                if (!this.Q.b.equals(bundle.getString("groupid"))) {
                    return false;
                }
                String string = bundle.getString("msgid");
                String string2 = bundle.getString("stype");
                int f = this.ac.f(new Message(string));
                this.L.a((Object) ("position:" + f));
                if (f >= 0) {
                    Message message = (Message) this.ac.getItem(f);
                    if ("msgsuccess".equals(string2)) {
                        message.status = 2;
                    } else if ("msgsending".equals(string2)) {
                        message.status = 1;
                        message.fileName = this.O.c(string).fileName;
                    } else if ("msgfailed".equals(string2)) {
                        message.status = 3;
                    }
                    Y();
                }
            } else if (str.equals("actions.emoteupdates")) {
                Y();
            } else if (str.equals("actions.glocalmsg")) {
                if (!this.Q.b.equals(bundle.getString("groupid"))) {
                    return false;
                }
                Message message2 = (Message) bundle.getSerializable("messageobj");
                if (message2.receive) {
                    h(message2);
                }
                a(this.ac, message2);
                return true;
            } else if ("actions.groupfeed".equals(str)) {
                if (!this.Q.b.equals(bundle.getString("groupid"))) {
                    return false;
                }
                this.Q.u = true;
                ag();
            }
            return super.a(bundle, str);
        } else if (!this.Q.b.equals(bundle.getString("groupid"))) {
            return false;
        } else {
            List<Message> list = (List) bundle.getSerializable("messagearray");
            if (list == null || list.isEmpty()) {
                return false;
            }
            for (Message message3 : list) {
                String str2 = message3.msgId;
                if (message3.contentType != 5 && message3.receive) {
                    this.F.add(str2);
                }
                h(message3);
            }
            if (q()) {
                ai();
            }
            a(this.ac, list);
            return q();
        }
    }

    /* access modifiers changed from: protected */
    public final boolean a(Message message) {
        this.L.b((Object) ("onAudioCompleted " + message.msgId));
        int f = this.ac.f(message);
        this.L.b((Object) ("position:" + f + " adapterCount:" + this.ac.getCount()));
        int i = f + 1;
        if (i < this.ac.getCount()) {
            Message message2 = (Message) this.ac.getItem(i);
            if (message2.receive && message2.contentType == 4 && !message2.isAudioPlayed) {
                com.immomo.momo.android.a.a.d.a(message2, this);
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public final void aa() {
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("group_id", u());
            jSONObject.put("number", this.M);
            new k("PI", "P2g", jSONObject).e();
        } catch (Exception e) {
        }
    }

    /* access modifiers changed from: protected */
    public final void ab() {
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("group_id", u());
            jSONObject.put("number", this.M);
            new k("PO", "P2g", jSONObject).e();
        } catch (Exception e) {
        }
    }

    /* access modifiers changed from: protected */
    public final List ac() {
        return this.O.g(this.Q.b);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.service.bean.Message.<init>(boolean, int):void
     arg types: [int, int]
     candidates:
      com.immomo.momo.service.bean.Message.<init>(int, boolean):void
      com.immomo.momo.service.bean.Message.<init>(boolean, int):void */
    /* access modifiers changed from: protected */
    public final Message ad() {
        int i = 1;
        Message message = new Message(true, 2);
        message.remoteId = this.f.h;
        message.distance = this.f.d();
        ce a2 = ce.a();
        x xVar = new x(this, message);
        String str = this.Q.b;
        if (!this.f.b()) {
            i = 0;
        }
        a2.a(message, xVar, str, 2, i);
        return message;
    }

    /* access modifiers changed from: protected */
    public final void ag() {
        if (!this.E) {
            this.V.setVisibility(8);
            return;
        }
        if (this.Q.u) {
            this.V.setImageResource(R.drawable.ic_groupfeed_bullet_action);
        } else {
            this.V.setImageResource(R.drawable.ic_groupfeed_bullet);
        }
        this.V.setVisibility(0);
    }

    public final void b(Message message) {
        this.ac.c(message);
        this.O.c(message);
    }

    /* access modifiers changed from: protected */
    public final Message c(int i) {
        return ce.a().a(this.H, i, this.f, this.Q.b, 2, this.f.b() ? 1 : 0);
    }

    /* access modifiers changed from: protected */
    public final Message c(String str) {
        ce.a();
        return ce.a(str, this.f, this.Q.b, 2, this.f.b() ? 1 : 0);
    }

    /* access modifiers changed from: protected */
    public final void c(Message message) {
        this.O.a(message.msgId, message.status);
    }

    /* access modifiers changed from: protected */
    public final Message d(String str) {
        ce.a();
        return ce.b(str, this.f, this.Q.b, 2, this.f.b() ? 1 : 0);
    }

    /* access modifiers changed from: protected */
    public final void d() {
        super.d();
        e(this.Q.O);
    }

    /* access modifiers changed from: protected */
    public final void d(Message message) {
        this.O.b(message);
    }

    /* access modifiers changed from: protected */
    public final void e() {
        super.e();
        this.L.a((Object) "~~~~~~~~~~~~~~~~~~~~~~onInitialize!!!");
    }

    /* access modifiers changed from: protected */
    public final void f(Message message) {
        if (message != null) {
            if (this.K) {
                this.K = false;
                new k("C", "C5201").e();
            }
            this.ac.a(message);
            super.f(message);
        }
    }

    public boolean handleMessage(android.os.Message message) {
        switch (message.what) {
            case PurchaseCode.BILL_DIALOG_SHOWERROR /*402*/:
                this.ac.notifyDataSetChanged();
                return true;
            case 10002:
                return true;
            default:
                return super.handleMessage(message);
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i2 == -1 && i == 264) {
            String stringExtra = intent.getStringExtra("key_resourseid");
            e(stringExtra);
            this.Q.O = stringExtra;
            this.P.a(stringExtra, this.i);
            return;
        }
        super.onActivityResult(i, i2, intent);
    }

    public void onBackPressed() {
        if (this.aa.isShown()) {
            aj();
        } else {
            super.onBackPressed();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        ab();
        if (r()) {
            a(this.T);
            a(this.S);
            a(this.U);
        }
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        this.L.a((Object) "onStart");
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        this.L.a((Object) "onStop...");
        if (r()) {
            Bundle bundle = new Bundle();
            bundle.putString("sessionid", this.Q.b);
            bundle.putInt("sessiontype", 2);
            g.d().a(bundle, "action.sessionchanged");
        }
    }

    /* access modifiers changed from: protected */
    public final String u() {
        return getIntent().getStringExtra("remoteGroupID");
    }

    /* access modifiers changed from: protected */
    public final void x() {
        super.x();
        this.aa = (RelativeLayout) findViewById(R.id.groupchat_memberlist_contener);
        this.Z = (MomoRefreshListView) this.aa.findViewById(R.id.groupchat_lv_memberlist);
        this.Z.setTimeEnable(false);
        getLayoutInflater().inflate((int) R.layout.common_list_loadmore, (ViewGroup) null);
        this.Y = findViewById(R.id.coverLayout_withoutheader);
        this.Y.setOnClickListener(new bd(this));
        this.W = g.o().inflate((int) R.layout.common_menu_left, (ViewGroup) null);
        this.X = (ImageView) this.W.findViewById(R.id.commentmenu_iv_icon);
        this.m.getRootLayout().addView(this.W, 0);
        this.m.getTitileView().setClickable(false);
        this.m.setOnClickListener(new bl(this));
        this.Z.setOnPullToRefreshListener$42b903f6(new bm(this));
        this.V = (ImageView) findViewById(R.id.iv_newgroupbullet);
        this.V.setOnClickListener(new bn(this));
    }
}
