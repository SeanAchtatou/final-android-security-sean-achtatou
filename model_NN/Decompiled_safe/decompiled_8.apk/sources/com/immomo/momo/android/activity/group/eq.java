package com.immomo.momo.android.activity.group;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.o;
import com.immomo.momo.service.j;

final class eq extends d {

    /* renamed from: a  reason: collision with root package name */
    private v f1638a = null;
    private /* synthetic */ PublishGroupFeedActivity c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public eq(PublishGroupFeedActivity publishGroupFeedActivity, Context context) {
        super(context);
        this.c = publishGroupFeedActivity;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        return o.a().a(this.c.k.getText().toString().trim(), this.c.i);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.f1638a = new v(this.c.n(), "请稍候，正在提交...");
        this.f1638a.setOnCancelListener(new er(this));
        this.c.a(this.f1638a);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        PublishGroupFeedActivity.c(this.c);
        PublishGroupFeedActivity.d(this.c);
        j.a().d(this.c);
        this.c.finish();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.c.p();
    }
}
