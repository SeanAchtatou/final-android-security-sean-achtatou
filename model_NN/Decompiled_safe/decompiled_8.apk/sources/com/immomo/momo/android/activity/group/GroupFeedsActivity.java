package com.immomo.momo.android.activity.group;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;
import com.immomo.a.a.f.b;
import com.immomo.momo.R;
import com.immomo.momo.android.a.ek;
import com.immomo.momo.android.a.fp;
import com.immomo.momo.android.activity.ImageFactoryActivity;
import com.immomo.momo.android.activity.ah;
import com.immomo.momo.android.view.HeaderRefreshListView;
import com.immomo.momo.android.view.LoadingButton;
import com.immomo.momo.android.view.a.at;
import com.immomo.momo.android.view.a.n;
import com.immomo.momo.android.view.a.o;
import com.immomo.momo.android.view.bi;
import com.immomo.momo.android.view.bu;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.a.a;
import com.immomo.momo.service.bean.a.e;
import com.immomo.momo.service.bean.aj;
import com.immomo.momo.service.bean.al;
import com.immomo.momo.service.v;
import com.immomo.momo.service.y;
import com.immomo.momo.util.ao;
import com.immomo.momo.util.h;
import com.immomo.momo.util.j;
import java.io.File;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class GroupFeedsActivity extends ah implements bu {
    /* access modifiers changed from: private */
    public ek A;
    /* access modifiers changed from: private */
    public v B;
    /* access modifiers changed from: private */
    public y C;
    /* access modifiers changed from: private */
    public boolean D;
    private File E;
    File h = null;
    /* access modifiers changed from: private */
    public HeaderRefreshListView i = null;
    /* access modifiers changed from: private */
    public LoadingButton j;
    private GridView k;
    private ImageView l;
    private TextView m;
    private TextView n;
    private View o;
    private at p;
    private bi q;
    private View r;
    private Date s = null;
    /* access modifiers changed from: private */
    public Date t = null;
    /* access modifiers changed from: private */
    public String u;
    /* access modifiers changed from: private */
    public a v;
    private File w = null;
    /* access modifiers changed from: private */
    public List x = null;
    /* access modifiers changed from: private */
    public Set y = new HashSet();
    /* access modifiers changed from: private */
    public bw z = null;

    public GroupFeedsActivity() {
        new Handler();
        this.A = null;
        this.D = true;
        this.E = null;
    }

    static /* synthetic */ void m(GroupFeedsActivity groupFeedsActivity) {
        Intent intent = new Intent(groupFeedsActivity, PublishGroupFeedActivity.class);
        intent.putExtra("gid", groupFeedsActivity.u);
        groupFeedsActivity.startActivity(intent);
    }

    static /* synthetic */ void o(GroupFeedsActivity groupFeedsActivity) {
        if (groupFeedsActivity.p == null || !groupFeedsActivity.p.g()) {
            String[] strArr = {"群活动", "群留言"};
            groupFeedsActivity.p = new at(groupFeedsActivity, groupFeedsActivity.q, strArr);
            groupFeedsActivity.p.a(new bq(groupFeedsActivity, strArr));
            groupFeedsActivity.p.d();
        }
    }

    static /* synthetic */ void p(GroupFeedsActivity groupFeedsActivity) {
        groupFeedsActivity.i.n();
        if (groupFeedsActivity.z != null) {
            groupFeedsActivity.z.cancel(true);
            groupFeedsActivity.z = null;
        }
    }

    static /* synthetic */ void q(GroupFeedsActivity groupFeedsActivity) {
        Intent intent = new Intent(groupFeedsActivity, EditGroupPartyActivity.class);
        intent.putExtra("gid", groupFeedsActivity.v.b);
        intent.putExtra("modify_type", 1);
        groupFeedsActivity.startActivityForResult(intent, 14);
    }

    static /* synthetic */ void r(GroupFeedsActivity groupFeedsActivity) {
        String[] strArr = groupFeedsActivity.v.D ? new String[]{"使用照片墙", "更换封面", "取消"} : new String[]{"更换封面", "取消"};
        o oVar = new o(groupFeedsActivity, strArr);
        oVar.a(new bs(groupFeedsActivity, strArr));
        oVar.show();
    }

    static /* synthetic */ void s(GroupFeedsActivity groupFeedsActivity) {
        o oVar = new o(groupFeedsActivity, (int) R.array.editprofile_add_photo);
        oVar.a(new bt(groupFeedsActivity));
        oVar.show();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, int, boolean, boolean):void
     arg types: [com.immomo.momo.service.bean.al, android.widget.ImageView, int, int, int]
     candidates:
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup, int, boolean):void
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, int, boolean, boolean):void */
    /* access modifiers changed from: private */
    public void w() {
        this.m.setText(this.v.d());
        this.n.setText(String.valueOf(this.v.k) + "/" + this.v.j);
        if (this.v.D) {
            if (this.v.E.indexOf("_tempimage") >= 0) {
                x();
            }
            this.k.setVisibility(8);
            this.l.setVisibility(0);
            j.a((aj) new al(this.v.E), this.l, 2, false, false);
            return;
        }
        this.k.setVisibility(0);
        this.l.setVisibility(8);
        List b = this.C.b(this.u);
        if (b == null || b.isEmpty()) {
            b(new bu(this, this));
            return;
        }
        this.k.setAdapter((ListAdapter) new fp(this, b));
        if (b.size() < 20) {
            this.n.setText(String.valueOf(b.size()) + "/" + this.v.j);
        }
    }

    private void x() {
        com.immomo.momo.android.c.aj.a(this, this.v);
    }

    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        super.a(bundle);
        setContentView((int) R.layout.activity_groupfeeds);
        m().setTitleText("群空间");
        this.u = getIntent().getStringExtra("gid");
    }

    /* access modifiers changed from: protected */
    public final boolean a(Bundle bundle, String str) {
        if ("actions.groupfeedchanged".equals(str)) {
            this.i.m();
        } else if ("actions.groupfeeddelete".equals(str)) {
            this.A.c(new e(bundle.getString("groupfeedid")));
        }
        return super.a(bundle, str);
    }

    public final void b_() {
        b(new bw(this, this));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.service.y.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.immomo.momo.service.y.a(com.immomo.momo.service.bean.a.a, java.lang.String):void
      com.immomo.momo.service.y.a(com.immomo.momo.service.bean.a.a, boolean):void
      com.immomo.momo.service.y.a(java.lang.String, int):void
      com.immomo.momo.service.y.a(java.lang.String, java.lang.String):void
      com.immomo.momo.service.y.a(java.lang.String, java.util.List):void
      com.immomo.momo.service.y.a(java.lang.String, java.lang.String[]):void
      com.immomo.momo.service.y.a(java.util.List, java.lang.String):void
      com.immomo.momo.service.y.a(java.lang.String, boolean):void */
    public final void d() {
        boolean z2 = true;
        super.d();
        this.B = new v();
        this.C = new y();
        this.v = this.C.e(this.u);
        if (!(this.v.n == 2 || this.v.n == 1)) {
            z2 = false;
        }
        this.D = z2;
        this.x = this.B.a(this.u, 21);
        this.y.clear();
        this.y.addAll(this.x);
        HeaderRefreshListView headerRefreshListView = this.i;
        ek ekVar = new ek(this, this.x, this.i);
        this.A = ekVar;
        headerRefreshListView.setAdapter((ListAdapter) ekVar);
        this.s = o().b("gfeeds_lasttime_success" + this.u);
        if (this.x.size() <= 0 || !((Boolean) o().b("gffilter_remain" + this.u, false)).booleanValue()) {
            this.j.setVisibility(8);
        } else {
            this.j.setVisibility(0);
        }
        this.q = new bi(this).a((int) R.drawable.ic_topbar_addfeed_white);
        this.q.setMarginRight(8);
        this.q.setBackgroundResource(R.drawable.bg_header_submit);
        m().a(this.q, new bn(this));
        if (this.v.u) {
            this.C.a(this.u, false);
        } else if (!this.A.isEmpty() && this.s != null) {
            return;
        }
        this.i.l();
    }

    /* access modifiers changed from: protected */
    public final void e() {
        super.e();
        this.i = (HeaderRefreshListView) findViewById(R.id.listview);
        this.i.setFastScrollEnabled(false);
        this.i.setLastFlushTime(o().b("gfeeds_lasttime_success" + this.u));
        this.i.setEnableLoadMoreFoolter(true);
        this.j = this.i.getFooterViewButton();
        this.r = g.o().inflate((int) R.layout.include_groupfeeds_header, (ViewGroup) null);
        this.i.setHeaderView((ViewGroup) this.r);
        this.i.a(g.o().inflate((int) R.layout.include_groupfeed_listempty, (ViewGroup) null));
        this.i.addHeaderView(g.o().inflate((int) R.layout.listitem_blank, (ViewGroup) null));
        this.k = (GridView) this.r.findViewById(R.id.groupfeedlist_gridview_background);
        this.l = (ImageView) this.r.findViewById(R.id.groupfeedlist_iv_background);
        this.m = (TextView) this.r.findViewById(R.id.groupfeedlist_tv_groupname);
        this.n = (TextView) this.r.findViewById(R.id.groupfeedlist_tv_membercount);
        this.o = this.r.findViewById(R.id.groupfeedlist_view_cover);
        this.o.setOnClickListener(new br(this));
        this.j.setOnProcessListener(new bo(this));
        this.i.setOnPullToRefreshListener$42b903f6(this);
        this.i.setOnCancelListener$135502(new bp(this));
        d();
        if (((Boolean) o().b("pref_groupfeed_intro", true)).booleanValue()) {
            o().c("pref_groupfeed_intro", false);
            n nVar = new n(this);
            nVar.setTitle((int) R.string.dialog_groupfeed_introtitle0);
            nVar.a(0, (int) R.string.dialog_button_groupfeed_commend, new bm(this));
            nVar.a();
            nVar.a(1, (int) R.string.dialog_button_groupfeed_cancle, (DialogInterface.OnClickListener) null);
            nVar.setContentView(g.o().inflate((int) R.layout.dialog_groupfeed_intro, (ViewGroup) null));
            nVar.setCanceledOnTouchOutside(false);
            nVar.show();
        }
        a(800, "actions.groupfeedchanged", "actions.groupfeeddelete");
        w();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.h.a(java.lang.String, android.graphics.Bitmap, int, boolean):java.io.File
     arg types: [java.lang.String, android.graphics.Bitmap, int, int]
     candidates:
      com.immomo.momo.util.h.a(java.lang.String, java.lang.String, int, boolean):void
      com.immomo.momo.util.h.a(java.lang.String, android.graphics.Bitmap, int, boolean):java.io.File */
    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        switch (i2) {
            case 11:
                if (i3 == -1 && intent != null) {
                    Uri data = intent.getData();
                    Intent intent2 = new Intent(getApplicationContext(), ImageFactoryActivity.class);
                    intent2.setData(data);
                    intent2.putExtra("aspectY", 1);
                    intent2.putExtra("aspectX", 1);
                    intent2.putExtra("minsize", 320);
                    this.w = new File(com.immomo.momo.a.g(), String.valueOf(b.a()) + ".jpg_");
                    intent2.putExtra("outputFilePath", this.w.getAbsolutePath());
                    startActivityForResult(intent2, 13);
                    return;
                }
                return;
            case 12:
                if (i3 == -1 && this.h != null && this.h.exists()) {
                    Uri fromFile = Uri.fromFile(this.h);
                    this.w = new File(com.immomo.momo.a.g(), this.h.getName());
                    Intent intent3 = new Intent(getApplicationContext(), ImageFactoryActivity.class);
                    intent3.setData(fromFile);
                    intent3.putExtra("aspectY", 1);
                    intent3.putExtra("aspectX", 1);
                    intent3.putExtra("minsize", 320);
                    intent3.putExtra("outputFilePath", this.w.getAbsolutePath());
                    startActivityForResult(intent3, 13);
                    return;
                }
                return;
            case 13:
                if (i3 == -1 && intent != null) {
                    if (this.h != null) {
                        if (this.h.exists()) {
                            this.h.delete();
                        }
                        this.h = null;
                    }
                    if (this.w != null) {
                        String str = String.valueOf(b.a()) + "_tempimage";
                        Bitmap l2 = android.support.v4.b.a.l(this.w.getPath());
                        if (l2 != null) {
                            this.E = h.a(str, l2, 2, true);
                            this.e.a((Object) ("save file=" + this.E));
                            this.l.setImageBitmap(l2);
                            this.v.D = true;
                            this.v.E = str;
                            this.k.setVisibility(8);
                            this.l.setVisibility(0);
                            x();
                        } else {
                            this.E = null;
                            ao.b("发生未知错误，图片设置失败");
                        }
                        this.w = null;
                        return;
                    }
                    return;
                } else if (i3 == 1003) {
                    ao.d(R.string.cropimage_error_bg_size);
                    return;
                } else if (i3 == 1000) {
                    ao.d(R.string.cropimage_error_other);
                    return;
                } else if (i3 == 1002) {
                    ao.d(R.string.cropimage_error_store);
                    return;
                } else if (i3 == 1001) {
                    ao.d(R.string.cropimage_error_filenotfound);
                    return;
                } else {
                    return;
                }
            case com.immomo.momo.h.DragSortListView_drag_handle_id /*14*/:
                if (i3 == -1 && intent != null) {
                    String stringExtra = intent.getStringExtra("pid");
                    if (android.support.v4.b.a.f(stringExtra)) {
                        Intent intent4 = new Intent(this, ShareGroupPartyActivity.class);
                        intent4.putExtra("pid", stringExtra);
                        intent4.putExtra("share_type", 1);
                        startActivity(intent4);
                        this.i.requestFocus();
                        return;
                    }
                    return;
                }
                return;
            default:
                return;
        }
    }

    public void onDestroy() {
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public final void v() {
        b(new bx(this, this));
    }
}
