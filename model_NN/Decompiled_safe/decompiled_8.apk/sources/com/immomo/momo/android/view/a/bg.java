package com.immomo.momo.android.view.a;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.protocol.a.v;
import java.util.List;

final class bg extends d {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public /* synthetic */ az f2687a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public bg(az azVar, Context context) {
        super(context);
        this.f2687a = azVar;
        if (azVar.m != null && !azVar.m.isCancelled()) {
            azVar.m.cancel(true);
        }
        azVar.m = this;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        return v.a().a(this.f2687a.g.getSelectIds());
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.f2687a.a(this.f2687a.e);
        this.f2687a.h.setVisibility(4);
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        super.a(exc);
        az azVar = this.f2687a;
        az.b(this.f2687a.e);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        az azVar = this.f2687a;
        az.b(this.f2687a.e);
        this.f2687a.h.postDelayed(new bh(this, (List) obj), 1000);
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.f2687a.l = (be) null;
    }
}
