package com.immomo.momo.android.activity.message;

import android.text.Editable;
import android.text.TextWatcher;

final class k implements TextWatcher {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ a f1986a;

    k(a aVar) {
        this.f1986a = aVar;
    }

    public final void afterTextChanged(Editable editable) {
        if (editable.length() == 0) {
            this.f1986a.d(1);
        } else {
            this.f1986a.d(0);
        }
    }

    public final void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public final void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }
}
