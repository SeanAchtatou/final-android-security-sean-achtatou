package com.immomo.momo.android.activity.emotestore;

import android.os.Bundle;
import android.widget.Button;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.ah;

public class EmotionInviteTaskActivity extends ah {
    String h = PoiTypeDef.All;
    /* access modifiers changed from: private */
    public Button i;

    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        super.a(bundle);
        setContentView((int) R.layout.activity_emotestore_invitetask);
        this.i = (Button) findViewById(R.id.emotiontask_btn_nvite);
        setTitle("邀请任务");
        this.h = getIntent().getStringExtra("eid");
        b(new a(this, this));
    }
}
