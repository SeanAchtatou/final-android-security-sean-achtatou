package com.immomo.momo.android.activity;

import android.content.Intent;
import android.support.v4.b.a;
import com.immomo.momo.android.broadcast.d;
import com.immomo.momo.android.broadcast.f;
import com.immomo.momo.android.broadcast.w;

final class gq implements d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ OtherProfileActivity f1524a;

    gq(OtherProfileActivity otherProfileActivity) {
        this.f1524a = otherProfileActivity;
    }

    public final void a(Intent intent) {
        if (w.f2366a.equals(intent.getAction())) {
            String str = intent.getExtras() != null ? (String) intent.getExtras().get("momoid") : null;
            if (!a.a((CharSequence) str) && this.f1524a.t.h.equals(str) && this.f1524a.l && this.f1524a.r() && !this.f1524a.q()) {
                this.f1524a.v();
            }
        } else if (w.b.equals(intent.getAction())) {
            this.f1524a.k = this.f1524a.s.d(this.f1524a.o);
            this.f1524a.y();
        } else if (f.f2349a.equals(intent.getAction())) {
            if (this.f1524a.t.h.equals(intent.getStringExtra("userid"))) {
                this.f1524a.t.ah = this.f1524a.r.b(this.f1524a.t.h);
                this.f1524a.z();
            }
        }
    }
}
