package com.immomo.momo.android.activity.tieba;

import com.immomo.momo.android.view.cx;

final class e implements cx {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ EditTieActivity f2252a;

    e(EditTieActivity editTieActivity) {
        this.f2252a = editTieActivity;
    }

    public final void a(int i, int i2) {
        if (i < i2) {
            this.f2252a.e.a((Object) ("OnResize--------------h < oldh=" + i + "<" + i2));
            if (((double) i) <= ((double) this.f2252a.w) * 0.8d) {
                this.f2252a.h = true;
            }
        } else if (((double) i2) <= ((double) this.f2252a.w) * 0.8d && this.f2252a.h && !this.f2252a.x.isShown()) {
            this.f2252a.h = false;
        }
    }
}
