package com.immomo.momo.android.view;

import android.text.SpannableStringBuilder;

final class ac extends SpannableStringBuilder {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ab f2713a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    ac(ab abVar, CharSequence charSequence) {
        super(charSequence);
        this.f2713a = abVar;
    }

    public final SpannableStringBuilder append(char c) {
        return super.append(c);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.text.SpannableStringBuilder.replace(int, int, java.lang.CharSequence, int, int):android.text.SpannableStringBuilder}
     arg types: [int, int, java.lang.CharSequence, int, int]
     candidates:
      com.immomo.momo.android.view.ac.replace(int, int, java.lang.CharSequence, int, int):android.text.Editable
      ClspMth{android.text.SpannableStringBuilder.replace(int, int, java.lang.CharSequence, int, int):android.text.Editable}
      ClspMth{android.text.SpannableStringBuilder.replace(int, int, java.lang.CharSequence, int, int):android.text.SpannableStringBuilder} */
    public final SpannableStringBuilder replace(int i, int i2, CharSequence charSequence, int i3, int i4) {
        return super.replace(i, i2, (charSequence == null || charSequence.length() <= 0) ? charSequence : this.f2713a.f2712a.a(charSequence), i3, i4);
    }
}
