package com.immomo.momo.android.activity.tieba;

import android.content.Context;
import android.os.Bundle;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import com.immomo.momo.R;
import com.immomo.momo.android.a.gp;
import com.immomo.momo.android.activity.lh;
import com.immomo.momo.android.view.HeaderLayout;
import com.immomo.momo.android.view.LoadingButton;
import com.immomo.momo.android.view.MomoRefreshListView;
import com.immomo.momo.android.view.bl;
import com.immomo.momo.android.view.bu;
import com.immomo.momo.g;
import com.immomo.momo.service.af;
import com.immomo.momo.service.ao;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class z extends lh implements bl, bu {
    /* access modifiers changed from: private */
    public MomoRefreshListView O = null;
    /* access modifiers changed from: private */
    public LoadingButton P;
    /* access modifiers changed from: private */
    public Date Q = null;
    /* access modifiers changed from: private */
    public Set R = new HashSet();
    /* access modifiers changed from: private */
    public gp S = null;
    /* access modifiers changed from: private */
    public ao T = null;
    /* access modifiers changed from: private */
    public ab U = null;
    /* access modifiers changed from: private */
    public ac V = null;

    static /* synthetic */ void j(z zVar) {
        zVar.O.n();
        if (zVar.V != null) {
            zVar.V.cancel(true);
        }
    }

    /* access modifiers changed from: protected */
    public final int B() {
        return R.layout.activity_mytiecomments;
    }

    /* access modifiers changed from: protected */
    public final void C() {
        this.O = (MomoRefreshListView) c((int) R.id.listview);
        this.O.setFastScrollEnabled(false);
        this.O.setLastFlushTime(this.N.b("tieba_mycom_lasttime_success"));
        this.O.setEnableLoadMoreFoolter(true);
        this.P = this.O.getFooterViewButton();
        this.O.a(g.o().inflate((int) R.layout.include_mytiecomm_listempty, (ViewGroup) null));
        this.O.addHeaderView(g.o().inflate((int) R.layout.listitem_blank_2dp, (ViewGroup) null));
    }

    /* access modifiers changed from: protected */
    public final void E() {
        super.E();
        this.O.o();
        if (this.P.d()) {
            this.P.e();
        }
    }

    public final void S() {
        super.S();
        if (this.S.isEmpty() || af.c().k() > 0) {
            this.O.l();
        }
    }

    public final void a(Context context, HeaderLayout headerLayout) {
        super.a(context, headerLayout);
        headerLayout.setTitleText((int) R.string.tieba_title_index1);
    }

    /* access modifiers changed from: protected */
    public final void ae() {
        super.ae();
    }

    public final void ai() {
        this.O.i();
    }

    public final void aj() {
        super.aj();
        this.R.clear();
        this.T = new ao();
        List e = this.T.e();
        this.R.addAll(e);
        this.O.setListPaddingBottom(-3);
        this.S = new gp(c(), e, this.O);
        this.O.setAdapter((ListAdapter) this.S);
        this.Q = this.N.b("tieba_mycom_lasttime_success");
        if (e.size() < 20 || !((Boolean) this.N.b("mtyiecomments_remain", false)).booleanValue()) {
            this.O.k();
        }
    }

    public final void b_() {
        a(new ac(this, c()));
    }

    /* access modifiers changed from: protected */
    public final void g(Bundle bundle) {
        this.P.setOnProcessListener(this);
        this.O.setOnPullToRefreshListener$42b903f6(this);
        this.O.setOnCancelListener$135502(new aa(this));
        aj();
    }

    public final void p() {
        super.p();
    }

    public final void u() {
        a(new ab(this, c()));
    }
}
