package com.immomo.momo.android.activity.contacts;

import android.content.DialogInterface;
import android.content.Intent;
import com.immomo.momo.android.activity.setting.CommunityBindActivity;

final class x implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ v f1201a;

    x(v vVar) {
        this.f1201a = vVar;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        Intent intent = new Intent(this.f1201a.f(), CommunityBindActivity.class);
        intent.putExtra("type", 1);
        intent.putExtra("value_enforce", 1);
        this.f1201a.e.startActivityForResult(intent, 23);
    }
}
