package com.immomo.momo.android.activity.message;

import android.widget.ImageView;
import com.immomo.momo.android.view.db;

public final class ac implements db {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ a f1911a;

    public ac(a aVar) {
        this.f1911a = aVar;
    }

    public final void a(int i) {
        for (int i2 = 0; i2 < this.f1911a.u.getChildCount(); i2++) {
            ImageView imageView = (ImageView) this.f1911a.v.getChildAt(i2);
            if (i == i2) {
                imageView.setImageBitmap(this.f1911a.y);
            } else {
                imageView.setImageBitmap(this.f1911a.x);
            }
        }
        if (i == 1) {
            this.f1911a.A();
            if (this.f1911a.D) {
                this.f1911a.j.postDelayed(new ad(this), 500);
            }
            if (!this.f1911a.E) {
                this.f1911a.j.sendEmptyMessage(10002);
            }
        } else if (i == 0) {
            this.f1911a.p.setVisibility(0);
            this.f1911a.q.setVisibility(0);
        }
    }
}
