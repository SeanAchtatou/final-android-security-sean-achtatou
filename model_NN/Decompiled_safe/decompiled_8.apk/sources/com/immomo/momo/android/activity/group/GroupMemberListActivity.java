package com.immomo.momo.android.activity.group;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.a.b;
import android.view.View;
import android.view.ViewGroup;
import com.immomo.momo.R;
import com.immomo.momo.android.a.es;
import com.immomo.momo.android.activity.ah;
import com.immomo.momo.android.broadcast.s;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.BottomTipView;
import com.immomo.momo.android.view.HeaderLayout;
import com.immomo.momo.android.view.MomoRefreshExpandableListView;
import com.immomo.momo.android.view.a.o;
import com.immomo.momo.android.view.bi;
import com.immomo.momo.android.view.bu;
import com.immomo.momo.android.view.cv;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.a.a;
import com.immomo.momo.service.bean.a.j;
import com.immomo.momo.service.bean.a.k;
import com.immomo.momo.service.y;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

public class GroupMemberListActivity extends ah implements b, View.OnClickListener, bu, cv {
    private HeaderLayout h;
    /* access modifiers changed from: private */
    public MomoRefreshExpandableListView i;
    /* access modifiers changed from: private */
    public BottomTipView j;
    private List k = null;
    /* access modifiers changed from: private */
    public y l;
    /* access modifiers changed from: private */
    public es m;
    private s n;
    private String o;
    /* access modifiers changed from: private */
    public int p = 1;
    /* access modifiers changed from: private */
    public a q = null;
    private bi r = null;
    /* access modifiers changed from: private */
    public d s = null;
    private com.immomo.momo.android.broadcast.d t = new by(this);

    static /* synthetic */ List a(List list, List list2, List list3) {
        ArrayList arrayList = new ArrayList();
        k kVar = new k(1);
        kVar.f2980a = new ArrayList();
        kVar.f2980a = list;
        arrayList.add(kVar);
        k kVar2 = new k(2);
        kVar2.f2980a = list2;
        arrayList.add(kVar2);
        k kVar3 = new k(3);
        kVar3.f2980a = list3;
        arrayList.add(kVar3);
        return arrayList;
    }

    /* access modifiers changed from: private */
    public void a(List list) {
        if (list != null && list.size() > 0) {
            this.k = list;
            this.q = this.l.e(this.o);
            if (this.q != null) {
                this.m = new es(this.k, this.i, this, this.q, this.l.d(this.o, this.f.h));
                this.i.setAdapter(this.m);
                this.m.a();
                String a2 = g.a((int) R.string.groupmember_list_header_title);
                this.h.setTitleText(String.format(a2, Integer.valueOf(this.q.k)));
            }
        }
    }

    static /* synthetic */ void h(GroupMemberListActivity groupMemberListActivity) {
        o oVar = new o(groupMemberListActivity, (int) R.array.order_groupmember_list);
        oVar.setTitle((int) R.string.header_order);
        oVar.a();
        oVar.a(new ca(groupMemberListActivity));
        oVar.show();
    }

    /* access modifiers changed from: private */
    public void w() {
        if (this.l.d(this.o, this.f.h) == 2 || this.l.d(this.o, this.f.h) == 1) {
            new Handler().postDelayed(new cd(this), 1500);
        } else {
            this.j.setVisibility(8);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.service.y.a(java.lang.String, java.lang.String, boolean):com.immomo.momo.service.bean.a.j
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      com.immomo.momo.service.y.a(int, java.lang.String, java.lang.String):void
      com.immomo.momo.service.y.a(java.lang.String, java.lang.String, int):void
      com.immomo.momo.service.y.a(java.lang.String, boolean, java.lang.String):void
      com.immomo.momo.service.y.a(java.lang.String, java.lang.String, boolean):com.immomo.momo.service.bean.a.j */
    /* access modifiers changed from: private */
    public List x() {
        ArrayList arrayList = new ArrayList();
        k kVar = new k(1);
        kVar.f2980a = new ArrayList();
        j a2 = this.l.a(this.o, this.q.g, true);
        if (a2 != null) {
            kVar.f2980a.add(a2);
        }
        arrayList.add(kVar);
        k kVar2 = new k(2);
        kVar2.f2980a = this.l.g(this.o);
        arrayList.add(kVar2);
        k kVar3 = new k(3);
        kVar3.f2980a = this.l.b(this.o, this.p);
        arrayList.add(kVar3);
        return arrayList;
    }

    public final void a(Intent intent, int i2, Bundle bundle, String str) {
        if (intent.getComponent() != null && com.immomo.momo.android.activity.d.g(intent.getComponent().getClassName())) {
            intent.putExtra("afromname", this.q != null ? this.q.d() : this.o);
        }
        super.a(intent, i2, bundle, str);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, com.immomo.momo.android.view.MomoRefreshExpandableListView, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        super.a(bundle);
        setContentView((int) R.layout.activity_groupmemberlist);
        this.l = new y();
        this.n = new s(this);
        this.n.a(this.t);
        this.i = (MomoRefreshExpandableListView) findViewById(R.id.listview);
        this.i.setListPaddingBottom(-3);
        this.i.setMMHeaderView(g.o().inflate((int) R.layout.listitem_groupsite, (ViewGroup) this.i, false));
        this.i.setGroupIndicator(null);
        this.i.setTimeEnable(false);
        this.i.setFastScrollEnabled(false);
        this.h = (HeaderLayout) findViewById(R.id.layout_header);
        this.h.setTitleText(String.format(g.a((int) R.string.groupmember_list_header_title), Integer.valueOf(getIntent().getIntExtra("count", 0))));
        this.r = new bi(this).b(R.string.header_order).a((int) R.drawable.ic_topbar_showbylist);
        m().a(this.r, new bz(this));
        this.r.setVisibility(8);
        this.j = (BottomTipView) findViewById(R.id.layout_tip);
        this.j.setText("点击成员右下角操作图标,进行管理");
        if (bundle == null || !bundle.getBoolean("from_saveinstance", false)) {
            this.o = getIntent().getStringExtra("gid");
        } else {
            this.o = (String) bundle.get("gid");
        }
        this.q = this.l.e(this.o);
        if (this.q == null) {
            finish();
        }
        a(x());
        this.r.setVisibility(this.l.c(g.q().h, this.o) ? 0 : 8);
        w();
        b(new ce(this, this));
        this.i.setOnPullToRefreshListener(this);
        this.i.setOnCancelListener(this);
        this.i.setOnChildClickListener(new cb(this));
        this.i.setOnGroupClickListener(new cc());
        this.i.h();
    }

    public final void b_() {
        b(new ce(this, this));
    }

    /* access modifiers changed from: protected */
    public final void d() {
    }

    public void onClick(View view) {
        view.getId();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.n != null) {
            unregisterReceiver(this.n);
            this.n = null;
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("group_id", this.o);
            new com.immomo.momo.util.k("PO", "P311", jSONObject).e();
        } catch (JSONException e) {
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("group_id", this.o);
            new com.immomo.momo.util.k("PI", "P311", jSONObject).e();
        } catch (JSONException e) {
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        bundle.putBoolean("from_saveinstance", true);
        bundle.putString("gid", this.o);
        super.onSaveInstanceState(bundle);
    }

    public final void v() {
        this.i.i();
    }
}
