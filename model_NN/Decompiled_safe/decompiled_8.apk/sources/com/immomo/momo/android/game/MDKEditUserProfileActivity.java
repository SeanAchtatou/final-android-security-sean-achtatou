package com.immomo.momo.android.game;

import android.content.Intent;
import android.os.Bundle;
import com.immomo.momo.android.activity.EditUserProfileActivity;
import com.immomo.momo.android.activity.EditVipProfileActivity;
import com.immomo.momo.android.activity.WelcomeActivity;
import com.immomo.momo.android.activity.ao;
import com.immomo.momo.service.aq;

public class MDKEditUserProfileActivity extends ao {
    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i == 123) {
            setResult(i2);
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (this.f == null) {
            Intent intent = new Intent(getApplicationContext(), WelcomeActivity.class);
            intent.addFlags(268435456);
            getApplicationContext().startActivity(intent);
            Intent intent2 = new Intent();
            intent2.putExtra("emsg", "客户端没有登录");
            setResult(40102, intent2);
            finish();
            return;
        }
        new aq().a(this.f, this.f.h);
        if (this.f.b()) {
            startActivityForResult(new Intent(getApplicationContext(), EditVipProfileActivity.class), 123);
        } else {
            startActivityForResult(new Intent(getApplicationContext(), EditUserProfileActivity.class), 123);
        }
    }
}
