package com.immomo.momo.android.activity.tieba;

import android.content.Intent;
import android.net.Uri;
import android.webkit.DownloadListener;

final class br implements DownloadListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ PublishTieCommentActivity f2192a;

    br(PublishTieCommentActivity publishTieCommentActivity) {
        this.f2192a = publishTieCommentActivity;
    }

    public final void onDownloadStart(String str, String str2, String str3, String str4, long j) {
        this.f2192a.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
    }
}
