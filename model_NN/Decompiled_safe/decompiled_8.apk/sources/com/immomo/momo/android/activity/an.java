package com.immomo.momo.android.activity;

public final class an {

    /* renamed from: a  reason: collision with root package name */
    int f1029a;
    int b;
    String c;
    boolean d;

    public an(int i) {
        this.b = i;
    }

    public an(int i, String str) {
        this.c = str;
        this.b = i;
    }

    public an(int i, String str, int i2) {
        this.c = str;
        this.f1029a = i2;
        this.b = i;
    }

    public an(String str) {
        this.c = str;
        this.f1029a = Integer.MAX_VALUE;
        this.b = 1008;
        this.d = true;
    }

    public final int a() {
        return this.b;
    }

    public final void b() {
        this.d = true;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        return this.b == ((an) obj).b;
    }

    public final int hashCode() {
        return this.b + 31;
    }

    public final String toString() {
        return "TipsMessage [id=" + this.b + ", message=" + this.c + "]";
    }
}
