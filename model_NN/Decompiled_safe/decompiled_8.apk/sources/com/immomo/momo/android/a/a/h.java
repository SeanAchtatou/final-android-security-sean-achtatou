package com.immomo.momo.android.a.a;

import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.emotestore.EmotionProfileActivity;
import com.immomo.momo.android.activity.message.a;
import com.immomo.momo.android.c.i;
import com.immomo.momo.android.view.MGifImageView;
import com.immomo.momo.android.view.au;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.ao;
import com.immomo.momo.service.bean.q;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import mm.purchasesdk.PurchaseCode;

public final class h extends ab implements View.OnClickListener, View.OnTouchListener {

    /* renamed from: a  reason: collision with root package name */
    static Map f693a = new HashMap();
    private View o;
    private ImageView p;
    private ImageView q;
    /* access modifiers changed from: private */
    public AnimationDrawable r = null;
    private MGifImageView s = null;
    private Handler t = new i(this, e().getMainLooper());

    protected h(a aVar) {
        super(aVar);
        this.n.b();
    }

    /* access modifiers changed from: private */
    public void c() {
        this.q.clearAnimation();
        this.r = new AnimationDrawable();
        this.r.addFrame(g.b((int) R.drawable.ic_loading_msgplus_01), PurchaseCode.UNSUPPORT_ENCODING_ERR);
        this.r.addFrame(g.b((int) R.drawable.ic_loading_msgplus_02), PurchaseCode.UNSUPPORT_ENCODING_ERR);
        this.r.addFrame(g.b((int) R.drawable.ic_loading_msgplus_03), PurchaseCode.UNSUPPORT_ENCODING_ERR);
        this.r.addFrame(g.b((int) R.drawable.ic_loading_msgplus_04), PurchaseCode.UNSUPPORT_ENCODING_ERR);
        this.r.setOneShot(false);
        this.o.setVisibility(0);
        this.q.setImageDrawable(this.r);
        this.t.post(new j(this));
    }

    /* access modifiers changed from: protected */
    public final void a() {
        View inflate = this.k.inflate((int) R.layout.message_gif, (ViewGroup) null);
        this.i.addView(inflate);
        this.o = inflate.findViewById(R.id.layer_download);
        this.q = (ImageView) inflate.findViewById(R.id.download_view);
        this.p = (ImageView) inflate.findViewById(R.id.download_view_image);
        this.i.setBackgroundResource(0);
        this.s = (MGifImageView) inflate.findViewById(R.id.message_gv_msggif);
        this.s.setGifStatus(e());
        this.i.setOnLongClickListener(this);
        this.s.setOnLongClickListener(this);
        this.s.setOnTouchListener(this);
    }

    /* access modifiers changed from: protected */
    public final void b() {
        com.immomo.momo.plugin.b.a aVar;
        boolean z = true;
        ao aoVar = (ao) this.h.getEmoteContent();
        if (aoVar.b == null) {
            e();
            com.immomo.momo.plugin.b.a aVar2 = new com.immomo.momo.plugin.b.a(this.h.getContent());
            aoVar.b = aVar2;
            aoVar.c = aVar2.h();
            aVar = aVar2;
        } else {
            aVar = aoVar.b;
        }
        this.s.setTag(R.id.tag_item, aoVar.c);
        this.s.setOnClickListener(this);
        this.s.setWidth(aoVar.b.j());
        this.s.setHeight(aoVar.b.k());
        if (this.h.gifDecoder != null) {
            this.s.setVisibility(0);
            this.o.setVisibility(8);
            this.p.clearAnimation();
            this.s.setGifDecoder(this.h.gifDecoder);
            if (e().y().g()) {
                return;
            }
            if ((this.h.gifDecoder.g() == 4 || this.h.gifDecoder.g() == 2) && this.h.gifDecoder.f() != null && this.h.gifDecoder.f().exists()) {
                this.h.gifDecoder.a(this.h.gifDecoder.f(), this.s);
                return;
            }
            return;
        }
        com.immomo.momo.plugin.b.a aVar3 = ((ao) this.h.getEmoteContent()).b;
        String g = aVar3.g();
        String h = aVar3.h();
        File a2 = q.a(g, h);
        if (!a2.exists() || a2.length() <= 0) {
            if (!aVar3.isImageLoading() && !aVar3.isImageLoadingFailed()) {
                aVar3.setImageLoading(true);
                aVar3.setImageLoadFailed(false);
                i iVar = (i) f693a.get(String.valueOf(h) + g);
                if (iVar == null || iVar.d()) {
                    i iVar2 = new i(g, h, new k(this, aVar3));
                    f693a.put(String.valueOf(h) + g, iVar2);
                    iVar2.a();
                } else {
                    iVar.a((com.immomo.momo.android.c.g) new k(this, aVar3));
                }
            }
            a2 = null;
        }
        if (!e().y().g()) {
            this.n.a((Object) ("readFromFile=" + a2));
            if (a2 != null) {
                this.h.gifDecoder = new au(aVar3.f() ? 1 : 2);
                this.h.gifDecoder.a(a2, this.s);
                this.h.gifDecoder.b(10);
                this.s.setGifDecoder(this.h.gifDecoder);
            } else {
                this.s.setGifDecoder(null);
                z = false;
            }
        } else {
            this.s.setGifDecoder(null);
            z = false;
        }
        if (z) {
            this.n.b((Object) " !!!!!!!!!!!!!!!!! case1");
            this.s.setVisibility(0);
            this.o.setVisibility(8);
        } else if (aVar.isImageLoadingFailed()) {
            this.n.b((Object) " !!!!!!!!!!!!!!!!! case2");
            this.o.setVisibility(0);
            this.q.setVisibility(4);
            if (this.r != null) {
                this.r.stop();
            }
            this.p.setImageResource(R.drawable.ic_chat_def_emote_failure);
            this.o.setTag(R.id.tag_item, aoVar.c);
            this.o.setOnClickListener(this);
        } else {
            this.n.b((Object) " !!!!!!!!!!!!!!!!! case3");
            c();
        }
    }

    public final void onClick(View view) {
        String str = (String) view.getTag(R.id.tag_item);
        if (!android.support.v4.b.a.a((CharSequence) str)) {
            if (view.getId() == R.id.message_gv_msggif) {
                Intent intent = new Intent(e().getApplicationContext(), EmotionProfileActivity.class);
                intent.putExtra("eid", str);
                e().startActivity(intent);
            } else if (view.getId() == R.id.layer_download && ((ao) this.h.getEmoteContent()).b.isImageLoadingFailed()) {
                ((ao) this.h.getEmoteContent()).b.setImageLoadFailed(false);
                e().Y();
            }
        }
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        if (motionEvent.getAction() == 0) {
            this.i.setPressed(true);
        } else if (motionEvent.getAction() == 3 || motionEvent.getAction() == 4 || motionEvent.getAction() == 1) {
            this.i.setPressed(false);
        }
        return false;
    }
}
