package com.immomo.momo.android.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import com.immomo.a.a.f.b;
import com.immomo.momo.R;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.c.n;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.util.ao;
import com.immomo.momo.util.i;
import java.io.File;
import java.io.FileOutputStream;

final class ei extends d {

    /* renamed from: a  reason: collision with root package name */
    private v f1275a;
    private /* synthetic */ FilterActivity c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ei(FilterActivity filterActivity, Context context) {
        super(context);
        this.c = filterActivity;
        this.f1275a = new v(context, (int) R.string.progress_filtering);
        this.f1275a.setCancelable(true);
        this.f1275a.setOnCancelListener(new ej(this));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    private Boolean c() {
        Bitmap bitmap;
        Bitmap bitmap2;
        try {
            this.c.b.lock();
            Bitmap a2 = this.c.a(this.c.f);
            i.a(b.a(), a2);
            if (a2 != null) {
                if (this.c.m > 0) {
                    this.b.a((Object) ("save rotate: " + this.c.m));
                    Matrix matrix = new Matrix();
                    matrix.setRotate((float) this.c.m, 0.5f, 0.5f);
                    bitmap = Bitmap.createBitmap(a2, 0, 0, a2.getWidth(), a2.getHeight(), matrix, true);
                    i.a(b.a(), bitmap);
                } else {
                    bitmap = a2;
                }
                if (this.c.h == 0 || bitmap == null) {
                    bitmap2 = bitmap;
                } else {
                    this.b.a((Object) "save filter called");
                    eb ebVar = (eb) this.c.k.get(this.c.h);
                    String str = ebVar.d;
                    bitmap2 = new n(null, bitmap, ebVar.f1268a, false).b();
                }
                if (bitmap2 != null) {
                    File file = new File(this.c.g);
                    FileOutputStream fileOutputStream = new FileOutputStream(file);
                    bitmap2.compress(Bitmap.CompressFormat.JPEG, this.c.j.k, fileOutputStream);
                    fileOutputStream.close();
                    bitmap2.recycle();
                    this.b.a((Object) ("imageFactoryHandler.saveQuality=" + this.c.j.k + ",saveBitmap.getWidth()=" + bitmap2.getWidth() + ",saveBitmap.getHeight()=" + bitmap2.getHeight() + ",tmpFile.length=" + file.length()));
                    System.gc();
                    return true;
                }
            }
            this.c.b.unlock();
            return false;
        } catch (Exception e) {
            this.b.a((Throwable) e);
            return false;
        } finally {
            this.c.b.unlock();
        }
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        return c();
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
        if (this.f1275a != null) {
            this.f1275a.show();
        }
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        this.b.a((Throwable) exc);
        ao.e(R.string.filter_save_failed);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        if (((Boolean) obj).booleanValue()) {
            Intent intent = new Intent();
            intent.putExtra("outputFilePath", this.c.g);
            this.c.j.a(-1, intent);
            this.c.j.t.finish();
            i.a();
            return;
        }
        ao.e(R.string.filter_save_failed);
    }

    /* access modifiers changed from: protected */
    public final void b() {
        super.b();
        if (this.f1275a != null && !this.c.isFinishing()) {
            this.f1275a.dismiss();
            this.f1275a = null;
        }
    }
}
