package com.immomo.momo.android.activity;

import android.app.Activity;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.b.a;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.immomo.a.a.f.b;
import com.immomo.momo.R;
import com.immomo.momo.android.c.n;
import com.immomo.momo.g;
import com.immomo.momo.util.i;
import com.immomo.momo.util.m;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class FilterActivity extends Activity implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    Handler f922a = new dv();
    final Lock b = new ReentrantLock();
    /* access modifiers changed from: private */
    public ImageView c;
    /* access modifiers changed from: private */
    public Bitmap d;
    /* access modifiers changed from: private */
    public Bitmap e;
    /* access modifiers changed from: private */
    public Uri f;
    /* access modifiers changed from: private */
    public String g;
    /* access modifiers changed from: private */
    public int h = 0;
    private m i = new m(getClass().getSimpleName());
    /* access modifiers changed from: private */
    public fr j = null;
    /* access modifiers changed from: private */
    public List k = null;
    private View[] l = null;
    /* access modifiers changed from: private */
    public int m = 0;
    private View.OnClickListener n = new dw(this);

    /* access modifiers changed from: private */
    public Bitmap a(Uri uri) {
        return a.a(uri, this, this.j.m, this.j.n);
    }

    /* access modifiers changed from: private */
    public void a() {
        int i2 = 0;
        while (i2 < this.l.length) {
            this.l[i2].findViewById(R.id.iv_filter_cover).setSelected(i2 == this.h);
            i2++;
        }
    }

    static /* synthetic */ void a(FilterActivity filterActivity, eb ebVar) {
        if (filterActivity.h == 0) {
            filterActivity.c.setImageBitmap(filterActivity.e);
            filterActivity.a(true);
            return;
        }
        dz dzVar = new dz(filterActivity, ebVar);
        if (i.a(ebVar.d) != null) {
            dzVar.a(i.a(ebVar.d));
            return;
        }
        Bitmap bitmap = filterActivity.e;
        String str = ebVar.d;
        new Thread(new n(dzVar, bitmap, ebVar.f1268a, false)).start();
    }

    /* access modifiers changed from: private */
    public void a(boolean z) {
        for (View findViewById : this.l) {
            findViewById.findViewById(R.id.iv_filter_cover).setClickable(z);
        }
    }

    private void b() {
        this.l = new View[this.k.size()];
        this.l[0] = findViewById(R.id.filter0);
        this.l[1] = findViewById(R.id.filter1);
        this.l[2] = findViewById(R.id.filter2);
        this.l[3] = findViewById(R.id.filter3);
        this.l[4] = findViewById(R.id.filter4);
        this.l[5] = findViewById(R.id.filter5);
        this.l[6] = findViewById(R.id.filter6);
        this.l[7] = findViewById(R.id.filter7);
        int length = this.l.length;
        for (int i2 = 0; i2 < length; i2++) {
            View view = this.l[i2];
            if (view != null) {
                View findViewById = view.findViewById(R.id.iv_filter_cover);
                findViewById.setTag(new ec(this.k.get(i2), i2));
                findViewById.setOnClickListener(this.n);
            }
        }
    }

    private void c() {
        if (this.k != null && this.k.size() > 0) {
            int size = this.k.size();
            ((ImageView) this.l[0].findViewById(R.id.iv_filterImg)).setImageBitmap(this.d);
            ((TextView) this.l[0].findViewById(R.id.tv_filterName)).setText(((eb) this.k.get(0)).b);
            for (int i2 = 1; i2 < size; i2++) {
                eb ebVar = (eb) this.k.get(i2);
                if (ebVar != null) {
                    new Thread(new ed(this, ebVar, (ImageView) this.l[i2].findViewById(R.id.iv_filterImg), (TextView) this.l[i2].findViewById(R.id.tv_filterName))).start();
                }
            }
        }
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imagefactory_btn1 /*2131165720*/:
                this.j.a(0);
                this.j.t.finish();
                return;
            case R.id.imagefactory_btn2 /*2131165721*/:
                new ei(this, this).execute(new String[0]);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_imagefactory_filter);
        System.gc();
        this.j = ImageFactoryActivity.a();
        this.f = this.j.p;
        this.g = this.j.r;
        this.j.b.setOnClickListener(new dy(this));
        this.j.f1500a.setTitleText((int) R.string.filterimage);
        if (this.f == null) {
            this.j.a(1002);
            finish();
        }
        this.i.a((Object) ("initData~~~~~~~~~~~~~~inputFileUri=" + this.f.getPath()));
        this.k = new ArrayList();
        eb ebVar = new eb();
        ebVar.f1268a = 99;
        ebVar.c = "thumb_none" + this.f.toString();
        ebVar.d = "big_none" + this.f.toString();
        ebVar.b = "默认";
        this.k.add(ebVar);
        eb ebVar2 = new eb();
        ebVar2.f1268a = 100;
        ebVar2.c = "thumb_lomo" + this.f.toString();
        ebVar2.d = "big_lomo" + this.f.toString();
        ebVar2.b = "LOMO";
        this.k.add(ebVar2);
        eb ebVar3 = new eb();
        ebVar3.f1268a = 1012;
        ebVar3.c = "thumb_toyhipster" + this.f.toString();
        ebVar3.d = "big_toyhipster" + this.f.toString();
        ebVar3.b = "纯真";
        this.k.add(ebVar3);
        eb ebVar4 = new eb();
        ebVar4.f1268a = 1005;
        ebVar4.c = "thumb_c41" + this.f.toString();
        ebVar4.d = "big_c41" + this.f.toString();
        ebVar4.b = "重彩";
        this.k.add(ebVar4);
        eb ebVar5 = new eb();
        ebVar5.f1268a = 1001;
        ebVar5.c = "thumb_vienna" + this.f.toString();
        ebVar5.d = "big_vienna" + this.f.toString();
        ebVar5.b = "维也纳";
        this.k.add(ebVar5);
        eb ebVar6 = new eb();
        ebVar6.f1268a = 1003;
        ebVar6.c = "thumb_xprocess" + this.f.toString();
        ebVar6.d = "big_xprocess" + this.f.toString();
        ebVar6.b = "淡雅";
        this.k.add(ebVar6);
        eb ebVar7 = new eb();
        ebVar7.f1268a = 1013;
        ebVar7.c = "thumb_cover1" + this.f.toString();
        ebVar7.d = "big_cover1" + this.f.toString();
        ebVar7.b = "酷";
        this.k.add(ebVar7);
        eb ebVar8 = new eb();
        ebVar8.f1268a = 1014;
        ebVar8.c = "thumb_cover2" + this.f.toString();
        ebVar8.d = "big_cover2" + this.f.toString();
        ebVar8.b = "浓厚";
        this.k.add(ebVar8);
        this.i.a((Object) this.f.getPath());
        this.e = a(this.f);
        i.a(b.a(), this.e);
        int a2 = g.a(60.0f);
        Bitmap a3 = a.a(this.e, a2, a2);
        i.a(b.a(), a3);
        this.d = a.a(a3, (float) g.a(3.0f));
        i.a(b.a(), this.d);
        this.i.a((Object) ("initData~~~~~~~~~~~~~~thumbBitmap=" + this.d + ", bigBitmap=" + this.e));
        new Thread(new dx(a3)).start();
        if (this.d == null || this.e == null) {
            this.j.a(1002);
            finish();
        }
        this.i.a((Object) ("~~~~~~~~~~~~~~~~~~src size = " + this.e.getWidth() + "x" + this.e.getHeight()));
        System.gc();
        this.c = (ImageView) findViewById(R.id.imageview_filter);
        this.c.setImageBitmap(this.e);
        this.j.c.setText("取消");
        this.j.c.setOnClickListener(this);
        this.j.d.setText("完成");
        this.j.d.setOnClickListener(this);
        b();
        c();
        a();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        i.a();
        System.gc();
    }
}
