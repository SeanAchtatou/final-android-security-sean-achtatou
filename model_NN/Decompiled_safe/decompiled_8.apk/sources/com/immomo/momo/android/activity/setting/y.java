package com.immomo.momo.android.activity.setting;

import android.content.DialogInterface;

final class y implements DialogInterface.OnDismissListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ x f2148a;

    y(x xVar) {
        this.f2148a = xVar;
    }

    public final void onDismiss(DialogInterface dialogInterface) {
        this.f2148a.cancel(true);
        this.f2148a.f2147a.finish();
    }
}
