package com.immomo.momo.android.activity.tieba;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import com.immomo.momo.R;
import com.immomo.momo.android.a.ip;
import com.immomo.momo.android.activity.ah;
import com.immomo.momo.android.broadcast.aa;
import com.immomo.momo.android.view.MomoRefreshExpandableListView;
import com.immomo.momo.android.view.bu;
import com.immomo.momo.android.view.cv;
import com.immomo.momo.g;
import com.immomo.momo.service.ao;
import com.immomo.momo.service.bean.c.d;
import com.immomo.momo.util.u;
import java.util.ArrayList;
import java.util.List;

public class TiebaCreatingActivity extends ah implements ExpandableListView.OnChildClickListener, bu, cv {
    /* access modifiers changed from: private */
    public MomoRefreshExpandableListView h;
    /* access modifiers changed from: private */
    public ip i = null;
    /* access modifiers changed from: private */
    public dv j;
    /* access modifiers changed from: private */
    public ao k;
    private aa l = null;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, com.immomo.momo.android.view.MomoRefreshExpandableListView, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        super.a(bundle);
        setContentView((int) R.layout.activity_tiebacreating);
        this.h = (MomoRefreshExpandableListView) findViewById(R.id.listview);
        this.h.setListPaddingBottom(-3);
        this.h.setMMHeaderView(g.o().inflate((int) R.layout.listitem_groupsite, (ViewGroup) this.h, false));
        this.h.setTimeEnable(false);
        this.h.setOnGroupClickListener(new du());
        setTitle("待创建的陌陌吧");
        d();
        this.h.setOnPullToRefreshListener(this);
        this.h.setOnCancelListener(this);
        this.h.setOnChildClickListener(this);
        this.l = new aa(this);
        this.l.a(new dt(this));
    }

    public final void b_() {
        b(new dv(this, this));
    }

    /* access modifiers changed from: protected */
    public final void d() {
        super.d();
        this.k = new ao();
        ao aoVar = this.k;
        this.i = new ip(u.c("tiebarecreating") ? (List) u.b("tiebarecreating") : new ArrayList(), this.h);
        this.h.setAdapter(this.i);
        this.i.a();
        this.h.h();
    }

    public boolean onChildClick(ExpandableListView expandableListView, View view, int i2, int i3, long j2) {
        d a2 = this.i.getChild(i2, i3);
        Intent intent = new Intent(this, TiebaCreateActivity.class);
        intent.putExtra(TiebaCreateActivity.i, a2.b);
        intent.putExtra(TiebaCreateActivity.h, a2.g);
        intent.putExtra(TiebaCreateActivity.k, a2.f3019a);
        intent.putExtra(TiebaCreateActivity.j, a2.m);
        startActivity(intent);
        return true;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (this.l != null) {
            unregisterReceiver(this.l);
            this.l = null;
        }
        super.onDestroy();
    }

    public final void v() {
        this.h.i();
    }
}
