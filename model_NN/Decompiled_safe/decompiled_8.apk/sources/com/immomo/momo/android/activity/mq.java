package com.immomo.momo.android.activity;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.protocol.a.w;
import com.immomo.momo.service.aq;
import com.immomo.momo.service.bean.bf;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

final class mq extends d {

    /* renamed from: a  reason: collision with root package name */
    private boolean f2008a = false;
    private /* synthetic */ VisitorListActivity c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public mq(VisitorListActivity visitorListActivity, Context context) {
        super(context);
        this.c = visitorListActivity;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        ArrayList<bf> arrayList = new ArrayList<>();
        AtomicInteger atomicInteger = new AtomicInteger();
        this.f2008a = w.a().a(arrayList, atomicInteger, this.c.j.getCount());
        this.c.n = atomicInteger.get();
        aq unused = this.c.k;
        aq.c(this.c.n);
        ArrayList arrayList2 = new ArrayList();
        for (bf bfVar : arrayList) {
            if (!this.c.m.contains(bfVar)) {
                arrayList2.add(bfVar);
                this.c.m.add(bfVar);
            }
        }
        this.c.k.a(arrayList2);
        return arrayList2;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        List list = (List) obj;
        if (!this.c.p) {
            if (list == null || list.isEmpty() || !this.f2008a) {
                this.c.h.k();
            }
            this.c.j.b((Collection) list);
            this.c.c(this.c.n);
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.c.i.e();
    }
}
