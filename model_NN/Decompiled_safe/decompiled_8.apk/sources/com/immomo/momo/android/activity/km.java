package com.immomo.momo.android.activity;

import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Message;
import android.view.ViewGroup;
import com.immomo.momo.R;
import com.immomo.momo.g;

final class km extends Handler {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ SharePageActivity f1791a;

    km(SharePageActivity sharePageActivity) {
        this.f1791a = sharePageActivity;
    }

    public final void handleMessage(Message message) {
        Bitmap bitmap = (Bitmap) message.obj;
        if (bitmap != null) {
            int width = bitmap.getWidth();
            int height = bitmap.getHeight();
            int J = g.J() - g.a(44.0f);
            int i = (height * J) / width;
            ViewGroup.LayoutParams layoutParams = this.f1791a.findViewById(R.id.share_img_container).getLayoutParams();
            layoutParams.width = J;
            layoutParams.height = i;
            this.f1791a.findViewById(R.id.share_img_container).setLayoutParams(layoutParams);
            this.f1791a.o.setImageBitmap(bitmap);
        }
    }
}
