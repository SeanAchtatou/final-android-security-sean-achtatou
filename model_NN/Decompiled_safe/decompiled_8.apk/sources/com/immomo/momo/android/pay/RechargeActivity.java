package com.immomo.momo.android.pay;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.b.a;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.ao;
import com.immomo.momo.android.view.PaymentButton;
import com.immomo.momo.android.view.a.n;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.g;
import com.immomo.momo.util.k;
import com.unicom.dcLoader.Utils;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import mm.purchasesdk.PurchaseCode;
import org.json.JSONObject;

public class RechargeActivity extends ao implements View.OnClickListener {
    /* access modifiers changed from: private */
    public v A = null;
    private List B = new ArrayList();
    private List C = new ArrayList();
    int h = 0;
    /* access modifiers changed from: private */
    public View i = null;
    private LinearLayout j = null;
    private LinearLayout k = null;
    private TextView l = null;
    private TextView m = null;
    /* access modifiers changed from: private */
    public List n = new ArrayList();
    /* access modifiers changed from: private */
    public List o = new ArrayList();
    /* access modifiers changed from: private */
    public List p = new ArrayList();
    /* access modifiers changed from: private */
    public List q = new ArrayList();
    /* access modifiers changed from: private */
    public List r = new ArrayList();
    /* access modifiers changed from: private */
    public Button s = null;
    /* access modifiers changed from: private */
    public boolean t = false;
    /* access modifiers changed from: private */
    public boolean u = false;
    /* access modifiers changed from: private */
    public boolean v = false;
    /* access modifiers changed from: private */
    public bk w;
    /* access modifiers changed from: private */
    public ay x = null;
    /* access modifiers changed from: private */
    public String y;
    /* access modifiers changed from: private */
    public Handler z = new av(this);

    static /* synthetic */ void a(RechargeActivity rechargeActivity, List list) {
        rechargeActivity.j.removeAllViews();
        int d = d(list.size());
        for (int i2 = 0; i2 < d; i2++) {
            View inflate = rechargeActivity.getLayoutInflater().inflate((int) R.layout.listitem_recharge, (ViewGroup) null);
            rechargeActivity.j.addView(inflate);
            rechargeActivity.B.add((PaymentButton) inflate.findViewById(R.id.btn_left));
            if (i2 != d - 1 || list.size() == d * 2) {
                rechargeActivity.B.add((PaymentButton) inflate.findViewById(R.id.btn_right));
            }
        }
        for (int i3 = 0; i3 < list.size(); i3++) {
            PaymentButton paymentButton = (PaymentButton) rechargeActivity.B.get(i3);
            paymentButton.setVisibility(0);
            paymentButton.setOnClickListener(rechargeActivity);
            at atVar = (at) list.get(i3);
            paymentButton.setLeftText(atVar.b);
            paymentButton.setTag(atVar);
            paymentButton.setSelected(atVar.f2538a);
            if (atVar.f2538a) {
                rechargeActivity.k.removeAllViews();
                rechargeActivity.a(rechargeActivity.c(atVar.c));
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(String str, int i2) {
        HashMap hashMap = new HashMap();
        String string = a.a(str, ";").getString("result");
        String substring = string.substring(1, string.length() - 1);
        String substring2 = substring.substring(0, substring.indexOf("&sign_type="));
        JSONObject a2 = a.a(substring, "&");
        a2.getString("sign_type").replace("\"", PoiTypeDef.All);
        String replace = a2.getString("sign").replace("\"", PoiTypeDef.All);
        String replace2 = a2.getString("out_trade_no").replace("\"", PoiTypeDef.All);
        hashMap.put("sign", replace);
        hashMap.put("content", substring2);
        hashMap.put("out_trade_no", replace2);
        b(new ax(this, this, hashMap, i2));
    }

    private void a(List list) {
        this.k.removeAllViews();
        this.C.clear();
        int d = d(list.size());
        for (int i2 = 0; i2 < d; i2++) {
            View inflate = getLayoutInflater().inflate((int) R.layout.listitem_recharge, (ViewGroup) null);
            this.k.addView(inflate);
            this.C.add((PaymentButton) inflate.findViewById(R.id.btn_left));
            if (i2 != d - 1 || list.size() == d * 2) {
                this.C.add((PaymentButton) inflate.findViewById(R.id.btn_right));
            }
        }
        for (int i3 = 0; i3 < list.size(); i3++) {
            PaymentButton paymentButton = (PaymentButton) this.C.get(i3);
            paymentButton.setVisibility(0);
            paymentButton.setOnClickListener(this);
            au auVar = (au) list.get(i3);
            paymentButton.setLeftText(auVar.c);
            paymentButton.setPromotionText(com.immomo.a.a.f.a.a(auVar.f) ? null : auVar.f);
            paymentButton.setTag(auVar);
            paymentButton.setSelected(auVar.e);
            if (auVar.e) {
                this.m.setText(auVar.d);
            }
        }
    }

    private List c(int i2) {
        switch (i2) {
            case 1:
                return this.o;
            case 2:
            default:
                return new ArrayList();
            case 3:
                return this.p;
            case 4:
                return this.q;
            case 5:
                return this.r;
        }
    }

    static /* synthetic */ void c(RechargeActivity rechargeActivity) {
        try {
            if (rechargeActivity.A != null && rechargeActivity.A.isShowing() && !rechargeActivity.isFinishing()) {
                rechargeActivity.A.dismiss();
            }
        } catch (Throwable th) {
        }
    }

    /* access modifiers changed from: private */
    public void c(String str) {
        n.b(this, str, new aw()).show();
    }

    private static int d(int i2) {
        return i2 % 2 == 0 ? i2 / 2 : (i2 / 2) + 1;
    }

    private Map f() {
        au auVar;
        HashMap hashMap = new HashMap();
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= this.C.size()) {
                return hashMap;
            }
            if (!((PaymentButton) this.C.get(i3)).isSelected() || (auVar = (au) ((PaymentButton) this.C.get(i3)).getTag()) == null) {
                i2 = i3 + 1;
            } else {
                hashMap.put("body", auVar.d);
                hashMap.put("subject", auVar.c);
                hashMap.put("product_id", auVar.f2539a);
                hashMap.put("total_fee", new StringBuilder().append(auVar.b).toString());
                hashMap.put("purpose", "1");
                return hashMap;
            }
        }
    }

    /* access modifiers changed from: private */
    public String g() {
        try {
            InputStream open = getAssets().open("premessable.txt");
            if (open != null) {
                return new String(a.a(open));
            }
        } catch (Exception e) {
            this.e.a((Throwable) e);
        }
        return PoiTypeDef.All;
    }

    private int i() {
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= this.B.size()) {
                return -1;
            }
            if (((PaymentButton) this.B.get(i3)).isSelected()) {
                return ((at) ((PaymentButton) this.B.get(i3)).getTag()).c;
            }
            i2 = i3 + 1;
        }
    }

    private int j() {
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= this.B.size()) {
                return -1;
            }
            if (((PaymentButton) this.B.get(i3)).isSelected()) {
                return ((at) ((PaymentButton) this.B.get(i3)).getTag()).e;
            }
            i2 = i3 + 1;
        }
    }

    /* access modifiers changed from: private */
    public void k() {
        for (int i2 = 0; i2 < this.B.size(); i2++) {
            ((PaymentButton) this.B.get(i2)).setEnabled(((PaymentButton) this.B.get(i2)).isSelected());
        }
        for (int i3 = 0; i3 < this.C.size(); i3++) {
            ((PaymentButton) this.C.get(i3)).setEnabled(((PaymentButton) this.C.get(i3)).isSelected());
        }
    }

    static /* synthetic */ void m(RechargeActivity rechargeActivity) {
        for (int i2 = 0; i2 < rechargeActivity.B.size(); i2++) {
            ((PaymentButton) rechargeActivity.B.get(i2)).setEnabled(true);
        }
        for (int i3 = 0; i3 < rechargeActivity.C.size(); i3++) {
            ((PaymentButton) rechargeActivity.C.get(i3)).setEnabled(true);
        }
    }

    static /* synthetic */ void p(RechargeActivity rechargeActivity) {
        rechargeActivity.n.clear();
        rechargeActivity.o.clear();
        rechargeActivity.p.clear();
        rechargeActivity.q.clear();
        rechargeActivity.r.clear();
        rechargeActivity.B.clear();
        rechargeActivity.C.clear();
    }

    /* access modifiers changed from: protected */
    public final void d() {
        super.d();
        b(new bb(this, this, "正在初始化信息....", false));
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        switch (i2) {
            case PurchaseCode.INIT_OK /*100*/:
                b(new bb(this, this, "正在更新数据", true));
                return;
            default:
                return;
        }
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_submit /*2131165424*/:
                if (!this.t) {
                    switch (i()) {
                        case 1:
                            if (new al(this).a()) {
                                try {
                                    Map f = f();
                                    try {
                                        JSONObject jSONObject = new JSONObject();
                                        jSONObject.put("money", f.get("total_fee"));
                                        new k("C", "C9201", jSONObject).e();
                                    } catch (Throwable th) {
                                    }
                                    b(new bg(this, this, f));
                                    break;
                                } catch (Exception e) {
                                    break;
                                }
                            }
                            break;
                        case 3:
                        case 4:
                            try {
                                Map f2 = f();
                                f2.put("cashier", new StringBuilder(String.valueOf(j())).toString());
                                b(new az(this, this, f2));
                                break;
                            } catch (Exception e2) {
                                break;
                            }
                        case 5:
                            if (!g.p()) {
                                c("该支付选项需要安卓系统2.2及以上");
                                break;
                            } else {
                                k();
                                this.s.setText((int) R.string.payvip_btn_recheck);
                                if (!this.v) {
                                    b(new bi(this, this, f()));
                                    break;
                                } else {
                                    b(new ay(this, this, this.w.b, 4));
                                    break;
                                }
                            }
                    }
                } else {
                    try {
                        a(this.y, 4);
                        break;
                    } catch (Exception e3) {
                        this.e.a((Throwable) e3);
                        break;
                    }
                }
                break;
            case R.id.layout_mypayrecord /*2131165827*/:
                Intent intent = new Intent(this, AuthWebviewActivity.class);
                intent.putExtra(AuthWebviewActivity.h, "https://www.immomo.com/website/vip/account/bill?momoid=" + g.q().h);
                intent.putExtra(AuthWebviewActivity.i, "交易记录");
                startActivity(intent);
                break;
        }
        if (view.getTag() instanceof at) {
            at atVar = (at) view.getTag();
            if (atVar.c != 2 || a.a((CharSequence) atVar.d)) {
                for (int i2 = 0; i2 < this.B.size(); i2++) {
                    PaymentButton paymentButton = (PaymentButton) this.B.get(i2);
                    at atVar2 = (at) paymentButton.getTag();
                    atVar2.f2538a = atVar2.c == atVar.c;
                    paymentButton.setSelected(atVar2.f2538a);
                    if (atVar2.f2538a) {
                        a(c(atVar.c));
                    }
                }
                return;
            }
            Intent intent2 = new Intent(this, AuthWebviewActivity.class);
            intent2.putExtra(AuthWebviewActivity.h, atVar.d);
            intent2.putExtra(AuthWebviewActivity.i, "账户充值");
            startActivityForResult(intent2, 100);
        } else if (view.getTag() instanceof au) {
            au auVar = (au) view.getTag();
            for (int i3 = 0; i3 < this.C.size(); i3++) {
                PaymentButton paymentButton2 = (PaymentButton) this.C.get(i3);
                au auVar2 = (au) paymentButton2.getTag();
                auVar2.e = auVar2.f2539a.equals(auVar.f2539a);
                paymentButton2.setSelected(auVar2.e);
                if (auVar2.e) {
                    this.m.setText(auVar2.d);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_recharge);
        this.h = getIntent().getIntExtra("model", 0);
        m().setTitleText((int) R.string.recharge_title);
        this.l = (TextView) findViewById(R.id.tv_mybalance);
        this.m = (TextView) findViewById(R.id.tv_paybody);
        String string = getResources().getString(R.string.gold);
        this.l.setText(String.format(string, String.valueOf(this.f.r())));
        this.i = findViewById(R.id.layout_pay);
        this.i.setVisibility(8);
        this.j = (LinearLayout) findViewById(R.id.layout_payment_container);
        this.k = (LinearLayout) findViewById(R.id.layout_payprice_container);
        this.s = (Button) findViewById(R.id.btn_submit);
        d();
        findViewById(R.id.layout_mypayrecord).setOnClickListener(this);
        this.s.setOnClickListener(this);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (this.u) {
            Utils.a().a(this);
        }
        super.onDestroy();
    }
}
