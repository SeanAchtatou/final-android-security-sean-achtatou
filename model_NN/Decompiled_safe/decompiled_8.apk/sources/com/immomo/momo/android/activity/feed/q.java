package com.immomo.momo.android.activity.feed;

import android.content.Intent;
import com.immomo.momo.android.activity.emotestore.EmotionProfileActivity;
import com.immomo.momo.android.view.a.al;
import com.immomo.momo.android.view.a.n;
import com.immomo.momo.g;
import com.immomo.momo.plugin.b.a;
import com.immomo.momo.service.bean.ae;

final class q implements al {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ FeedProfileActivity f1482a;
    private final /* synthetic */ String[] b;
    private final /* synthetic */ ae c;

    q(FeedProfileActivity feedProfileActivity, String[] strArr, ae aeVar) {
        this.f1482a = feedProfileActivity;
        this.b = strArr;
        this.c = aeVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.android.activity.feed.FeedProfileActivity.a(com.immomo.momo.android.activity.feed.FeedProfileActivity, java.lang.String, boolean):void
     arg types: [com.immomo.momo.android.activity.feed.FeedProfileActivity, java.lang.String, int]
     candidates:
      com.immomo.momo.android.activity.feed.FeedProfileActivity.a(android.content.Context, java.lang.String, boolean):void
      com.immomo.momo.android.activity.ao.a(android.support.v4.app.Fragment, android.content.Intent, int):void
      android.support.v4.app.g.a(java.lang.String, java.io.PrintWriter, android.view.View):void
      android.support.v4.app.g.a(android.support.v4.app.Fragment, android.content.Intent, int):void
      com.immomo.momo.android.activity.feed.FeedProfileActivity.a(com.immomo.momo.android.activity.feed.FeedProfileActivity, java.lang.String, boolean):void */
    public final void a(int i) {
        if ("回复".equals(this.b[i])) {
            this.f1482a.a(this.c);
        } else if ("查看表情".equals(this.b[i])) {
            FeedProfileActivity feedProfileActivity = this.f1482a;
            FeedProfileActivity feedProfileActivity2 = this.f1482a;
            a aVar = new a(FeedProfileActivity.c(this.c.f));
            Intent intent = new Intent(this.f1482a, EmotionProfileActivity.class);
            intent.putExtra("eid", aVar.h());
            this.f1482a.startActivity(intent);
        } else if ("复制文本".equals(this.b[i])) {
            g.a((CharSequence) this.c.f);
            this.f1482a.a((CharSequence) "已成功复制文本");
        } else if ("删除".equals(this.b[i])) {
            n.a(this.f1482a, "确定要删除该评论？", new r(this, this.c)).show();
        } else if ("举报".equals(this.b[i])) {
            FeedProfileActivity.a(this.f1482a, this.c.k, true);
        }
    }
}
