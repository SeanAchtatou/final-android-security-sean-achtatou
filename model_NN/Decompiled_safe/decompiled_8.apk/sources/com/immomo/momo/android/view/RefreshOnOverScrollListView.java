package com.immomo.momo.android.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.animation.DecelerateInterpolator;
import android.widget.AbsListView;
import com.baidu.location.LocationClientOption;
import com.immomo.momo.g;
import com.immomo.momo.util.m;
import mm.purchasesdk.PurchaseCode;

public class RefreshOnOverScrollListView extends HandyListView implements View.OnTouchListener {

    /* renamed from: a  reason: collision with root package name */
    private boolean f2650a;
    private boolean b;
    private boolean c;
    protected int d;
    protected dc e;
    protected m f;
    private boolean g;
    private float h;
    private float i;
    private View j;
    private cw k;
    private boolean l;
    private boolean m;
    private boolean n;
    private int o;
    private float p;
    private View.OnTouchListener q;
    private int r;
    private VelocityTracker s;
    private boolean t;
    private View.OnTouchListener u;
    private float v;
    private boolean w;
    private boolean x;

    public RefreshOnOverScrollListView(Context context) {
        super(context);
        this.f = new m(getClass().getSimpleName());
        this.u = null;
        this.v = 0.0f;
        this.w = false;
        this.x = false;
    }

    public RefreshOnOverScrollListView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f = new m(getClass().getSimpleName());
        this.u = null;
        this.v = 0.0f;
        this.w = false;
        this.x = false;
        this.q = this;
        this.o = 0;
        a();
    }

    public RefreshOnOverScrollListView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.f = new m(getClass().getSimpleName());
        this.u = null;
        this.v = 0.0f;
        this.w = false;
        this.x = false;
        this.q = this;
        this.o = 0;
        a();
    }

    public RefreshOnOverScrollListView(Context context, View view) {
        super(context);
        this.f = new m(getClass().getSimpleName());
        this.u = null;
        this.v = 0.0f;
        this.w = false;
        this.x = false;
        this.q = this;
        this.o = 0;
        a();
        if (view != null) {
            addHeaderView(view);
        }
    }

    private void a() {
        setFadingEdgeColor(0);
        setFadingEdgeLength(0);
        setCacheColorHint(-1);
        ViewConfiguration viewConfiguration = ViewConfiguration.get(getContext());
        this.r = viewConfiguration.getScaledTouchSlop();
        viewConfiguration.getScaledMinimumFlingVelocity();
        this.e = new dc(getContext(), new DecelerateInterpolator());
        this.i = 0.0f;
        this.l = false;
        this.f2650a = false;
        this.d = g.a(0.0f);
        this.j = null;
        this.c = false;
        this.t = false;
        this.m = false;
        this.n = true;
        View view = new View(getContext());
        view.setLayoutParams(new AbsListView.LayoutParams(-1, -2));
        addHeaderView(view);
        super.setOnTouchListener(this.q);
    }

    private void b() {
        if (!this.c && getCustomScrollY() <= 0 && this.k != null) {
            this.k.a(-getCustomScrollY(), this.d);
            if ((-getCustomScrollY()) < this.d || this.f2650a) {
                this.c = false;
            } else {
                this.c = true;
            }
            if (this.c) {
                b(-this.d);
                this.k.b();
            }
        }
    }

    private void b(int i2) {
        int scrollX = 0 - getScrollX();
        int customScrollY = i2 - getCustomScrollY();
        if (!this.e.a()) {
            this.e.e();
        }
        this.e.a(getScrollX(), getCustomScrollY(), scrollX, customScrollY);
        invalidate();
    }

    public final void a(int i2) {
        int scrollX = 0 - getScrollX();
        int customScrollY = i2 - getCustomScrollY();
        if (!this.e.a()) {
            this.e.e();
        }
        this.e.a(getScrollX(), getCustomScrollY(), scrollX, customScrollY, PurchaseCode.UNSUPPORT_ENCODING_ERR);
        invalidate();
    }

    public final void a(View view, int i2) {
        this.d = g.a((float) i2);
        this.j = view;
    }

    public void computeScroll() {
        if (!this.b) {
            if (this.e.d()) {
                int c2 = this.e.c();
                setPreventInvalidate(true);
                scrollTo(0, c2);
                setPreventInvalidate(false);
                postInvalidate();
            } else if (!this.f2650a && !this.c && getCustomScrollY() < 0) {
                b(0);
            }
        }
    }

    public int getCustomScrollY() {
        return this.o;
    }

    public void invalidate() {
        if (!this.l) {
            super.invalidate();
        }
    }

    public void invalidate(int i2, int i3, int i4, int i5) {
        if (!this.l) {
            super.invalidate(i2, i3, i4, i5);
        }
    }

    public void invalidate(Rect rect) {
        if (!this.l) {
            super.invalidate(rect);
        }
    }

    public void invalidateDrawable(Drawable drawable) {
        if (!this.l) {
            super.invalidateDrawable(drawable);
        }
    }

    /* access modifiers changed from: protected */
    public void o() {
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (!this.b) {
            if (this.o < 0) {
                canvas.translate(0.0f, (float) (-this.o));
            }
            if (getFirstVisiblePosition() == 0) {
                this.g = true;
            } else {
                this.g = false;
            }
            if (!this.c && this.g && getCustomScrollY() <= 0) {
                getCustomScrollY();
                if (this.t) {
                    this.t = false;
                }
                if (this.j != null && !this.m) {
                    this.j.draw(canvas);
                }
                b();
            } else if (this.c && !this.g) {
                scrollTo(0, 0);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        if (this.j != null) {
            this.j.layout(0, this.d, i4, 0);
        }
        super.onLayout(z, i2, i3, i4, i5);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        if (this.j != null) {
            this.j.measure(i2, View.MeasureSpec.makeMeasureSpec(this.d, 1073741824));
        }
        super.onMeasure(i2, i3);
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (this.u != null) {
            this.u.onTouch(view, motionEvent);
        }
        if (this.m || !this.n) {
            return false;
        }
        if (getFirstVisiblePosition() == 0) {
            this.g = true;
        } else {
            this.g = false;
        }
        if (this.s == null) {
            this.s = VelocityTracker.obtain();
        }
        this.s.addMovement(motionEvent);
        switch (motionEvent.getAction()) {
            case 0:
                this.f2650a = this.g;
                this.x = false;
                if (!this.e.a()) {
                    this.e.e();
                }
                this.i = motionEvent.getY();
                this.h = motionEvent.getY();
                this.p = 0.0f;
                return false;
            case 1:
            case 3:
                if (this.x) {
                    this.x = false;
                    return false;
                }
                this.p = 0.0f;
                this.h = -1.0f;
                this.i = 0.0f;
                this.s.computeCurrentVelocity(LocationClientOption.MIN_SCAN_SPAN);
                this.v = 0.0f;
                this.w = false;
                setInterceptItemClick(false);
                this.s.getYVelocity();
                if (this.s != null) {
                    this.s.recycle();
                    this.s = null;
                }
                if (this.f2650a) {
                    this.f2650a = false;
                    if (this.c) {
                        return false;
                    }
                    b();
                    b(0);
                    return false;
                } else if (this.e.a()) {
                    return false;
                } else {
                    this.e.e();
                    return false;
                }
            case 2:
                float y = this.i - motionEvent.getY();
                int i2 = (int) y;
                if (this.f2650a || this.i != 0.0f) {
                    this.i = motionEvent.getY();
                    if (this.f2650a) {
                        if (!this.g || getCustomScrollY() + i2 >= 0) {
                            this.f2650a = false;
                            if (!this.c && getCustomScrollY() < 0) {
                                scrollTo(0, 0);
                                setSelection(0);
                            }
                            this.v = 0.0f;
                        } else {
                            boolean z = getCustomScrollY() + i2 >= (-this.d);
                            if (!this.c || z) {
                                if (i2 >= 0 && this.p == 0.0f) {
                                    this.p = motionEvent.getY();
                                }
                                int i3 = (int) (y / 2.0f);
                                if (this.h < 0.0f || !this.g) {
                                    scrollBy(0, i3);
                                } else if (Math.abs(this.h - motionEvent.getY()) <= ((float) this.r)) {
                                    scrollBy(0, i3);
                                } else {
                                    this.h = -1.0f;
                                    scrollBy(0, i3);
                                    if (this.v < -3.0f) {
                                        this.v = -3.0f;
                                    }
                                    if (!this.w) {
                                        this.w = true;
                                        if (pointToPosition((int) motionEvent.getX(), (int) motionEvent.getY()) >= 0) {
                                            this.x = true;
                                            setInterceptItemClick(true);
                                            dispatchTouchEvent(MotionEvent.obtain(motionEvent.getDownTime(), motionEvent.getDownTime() + 100, 3, motionEvent.getX(), motionEvent.getY(), 1));
                                        }
                                    }
                                }
                            } else {
                                scrollTo(0, -this.d);
                            }
                        }
                        if (this.v < -3.0f) {
                            this.v = -3.0f;
                        }
                        return true;
                    } else if (!this.g || i2 >= 0) {
                        return this.g && this.p > 0.0f;
                    } else {
                        this.f2650a = true;
                        return true;
                    }
                } else {
                    this.i = motionEvent.getY();
                    return false;
                }
            default:
                return false;
        }
    }

    public final boolean p() {
        return this.f2650a;
    }

    public final boolean q() {
        return this.m;
    }

    public final void r() {
        if (this.c) {
            this.c = false;
            this.t = true;
            if (getCustomScrollY() < 0) {
                b(0);
            } else {
                invalidate();
            }
        }
    }

    public void scrollBy(int i2, int i3) {
        scrollTo(0, getCustomScrollY() + i3);
    }

    public void scrollTo(int i2, int i3) {
        this.o = i3;
        invalidate();
    }

    public void setAutoOverScrollMultiplier(float f2) {
    }

    public void setEnableOverscroll(boolean z) {
        this.n = z;
    }

    public void setIsGingerbread(boolean z) {
        this.b = z;
    }

    public void setOnTouchListener(View.OnTouchListener onTouchListener) {
        this.u = onTouchListener;
    }

    public void setOverScrollListener$2880bfd9(cw cwVar) {
        this.k = cwVar;
    }

    public void setOverScrollView(View view) {
        a(view, 0);
    }

    public void setPreventInvalidate(boolean z) {
        this.l = z;
    }

    public void setPreventOverScroll(boolean z) {
        this.m = z;
        this.c = false;
        if (getCustomScrollY() != 0) {
            b(0);
        }
    }
}
