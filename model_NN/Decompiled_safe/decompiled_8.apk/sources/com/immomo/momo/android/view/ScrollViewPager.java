package com.immomo.momo.android.view;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.support.v4.view.u;
import android.util.AttributeSet;
import android.view.MotionEvent;

public class ScrollViewPager extends ViewPager {

    /* renamed from: a  reason: collision with root package name */
    private boolean f2655a = true;

    public ScrollViewPager(Context context) {
        super(context);
    }

    public ScrollViewPager(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        try {
            if (this.f2655a) {
                return super.onInterceptTouchEvent(motionEvent);
            }
        } catch (Exception e) {
        }
        return false;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (this.f2655a) {
            return super.onTouchEvent(motionEvent);
        }
        return false;
    }

    public void setAdapter(u uVar) {
        super.setAdapter(uVar);
    }

    public void setEnableTouchScroll(boolean z) {
        this.f2655a = z;
    }
}
