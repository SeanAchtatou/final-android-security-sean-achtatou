package com.immomo.momo.android.view;

final class ci implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ NumberPicker f2761a;

    ci(NumberPicker numberPicker) {
        this.f2761a = numberPicker;
    }

    public final void run() {
        if (this.f2761a.k) {
            this.f2761a.a(this.f2761a.b + 1);
            this.f2761a.c.postDelayed(this, this.f2761a.j);
        } else if (this.f2761a.l) {
            this.f2761a.a(this.f2761a.b - 1);
            this.f2761a.c.postDelayed(this, this.f2761a.j);
        }
    }
}
