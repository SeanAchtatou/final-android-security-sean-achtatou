package com.immomo.momo.android.a;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.ColorFilter;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.support.v4.b.a;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.c.p;
import com.immomo.momo.android.c.u;
import com.immomo.momo.android.view.bp;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.q;
import com.immomo.momo.service.bean.r;
import com.immomo.momo.util.c;
import com.immomo.momo.util.m;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import mm.purchasesdk.PurchaseCode;

public final class bm extends a {
    private static String[] d = {"zemoji_e415", "zemoji_e056", "zemoji_e057", "zemoji_e414", "zemoji_e405", "zemoji_e106", "zemoji_e418", "zemoji_e417", "zemoji_e40d", "zemoji_e40a", "zemoji_e404", "zemoji_e105", "zemoji_e409", "zemoji_e40e", "zemoji_e402", "zemoji_e108", "zemoji_e403", "zemoji_e058", "zemoji_e407", "zemoji_e401", "zemoji_e40f", "zemoji_e40b", "zemoji_e406", "zemoji_e413", "zemoji_e411", "zemoji_e412", "zemoji_e410", "zemoji_e107", "zemoji_e059", "zemoji_e416", "zemoji_e408", "zemoji_e40c", "zemoji_e11a", "zemoji_e10c", "zemoji_e32c", "zemoji_e32a", "zemoji_e32d", "zemoji_e328", "zemoji_e32b", "zemoji_e022", "zemoji_e023", "zemoji_e327", "zemoji_e329", "zemoji_e32e", "zemoji_e335", "zemoji_e334", "zemoji_e337", "zemoji_e336", "zemoji_e13c", "zemoji_e330", "zemoji_e331", "zemoji_e326", "zemoji_e03e", "zemoji_e11d", "zemoji_e05a", "zemoji_e00e", "zemoji_e421", "zemoji_e420", "zemoji_e00d", "zemoji_e010", "zemoji_e011", "zemoji_e41e", "zemoji_e012", "zemoji_e422", "zemoji_e22e", "zemoji_e22f", "zemoji_e231", "zemoji_e230", "zemoji_e427", "zemoji_e41d", "zemoji_e00f", "zemoji_e41f", "zemoji_e14c", "zemoji_e201", "zemoji_e115", "zemoji_e428", "zemoji_e51f", "zemoji_e429", "zemoji_e424", "zemoji_e423", "zemoji_e253", "zemoji_e426", "zemoji_e111", "zemoji_e425", "zemoji_e31e", "zemoji_e31f", "zemoji_e31d", "zemoji_e001", "zemoji_e002", "zemoji_e005", "zemoji_e004", "zemoji_e51a", "zemoji_e519", "zemoji_e518", "zemoji_e515", "zemoji_e516", "zemoji_e517", "zemoji_e51b", "zemoji_e152", "zemoji_e04e", "zemoji_e51c", "zemoji_e51e", "zemoji_e11c", "zemoji_e536", "zemoji_e003", "zemoji_e41c", "zemoji_e41b", "zemoji_e419", "zemoji_e41a"};
    private static String[][] g = {d};
    private int e = -1;
    private q f;

    public bm(Context context) {
        super(context, new ArrayList());
        new m("EmoteAdapter");
        this.f = null;
        this.e = 0;
        e();
    }

    public bm(Context context, q qVar) {
        super(context, new ArrayList());
        new m("EmoteAdapter");
        this.f = null;
        this.f = qVar;
        e();
    }

    /* access modifiers changed from: private */
    public void a(r rVar) {
        rVar.setImageLoading(true);
        File file = rVar.f3036a;
        u.b().execute(new bo(this, rVar, rVar.b(), file));
    }

    private void e() {
        int i = 0;
        if (this.e == 0) {
            String[] a2 = bp.a();
            int length = a2.length;
            while (i < length) {
                b(a2[i]);
                i++;
            }
        } else if (this.e == 1) {
            String[] strArr = g[0];
            int length2 = strArr.length;
            while (i < length2) {
                b(strArr[i]);
                i++;
            }
        } else {
            q qVar = this.f;
        }
        notifyDataSetChanged();
    }

    public final void a(q qVar) {
        this.f = qVar;
    }

    public final int getCount() {
        return this.f != null ? this.f.v.size() : super.getCount();
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        boolean z;
        if (view == null) {
            view = g.o().inflate((int) R.layout.message_emote_item, (ViewGroup) null);
            view.setTag(R.id.textview, view.findViewById(R.id.textview));
            view.setTag(R.id.imageview, view.findViewById(R.id.imageview));
        }
        TextView textView = (TextView) view.getTag(R.id.textview);
        ImageView imageView = (ImageView) view.getTag(R.id.imageview);
        textView.setVisibility(8);
        if (this.e == 1) {
            imageView.setImageResource(d().getResources().getIdentifier((String) getItem(i), "drawable", d().getPackageName()));
            view.setTag(getItem(i));
            imageView.setMinimumWidth(0);
            imageView.setMinimumHeight(0);
            z = true;
        } else {
            if (this.e == 0) {
                String str = (String) getItem(i);
                String a2 = bp.a(str);
                imageView.setMinimumHeight(0);
                imageView.setMinimumWidth(0);
                if (a2 != null) {
                    imageView.setImageResource(d().getResources().getIdentifier(a2, "drawable", d().getPackageName()));
                    view.setTag(str);
                    z = true;
                }
            } else if (this.f != null) {
                List list = this.f.v;
                textView.setVisibility(a.a(((r) list.get(i)).f()) ? 8 : 0);
                textView.setText(((r) list.get(i)).f());
                view.setTag(list.get(i));
                z = this.f.A == null || i < this.f.A.c;
                imageView.setMinimumWidth(g.a(56.0f));
                imageView.setMinimumHeight(g.a(56.0f));
                r rVar = (r) list.get(i);
                if (a.a((CharSequence) rVar.b())) {
                    imageView.setImageBitmap(null);
                    imageView.setTag(R.id.tag_item_imageid, PoiTypeDef.All);
                } else {
                    imageView.setTag(R.id.tag_item_imageid, rVar.b());
                    Bitmap bitmap = (Bitmap) c.a().a(rVar.b());
                    if (bitmap != null) {
                        imageView.setImageBitmap(bitmap);
                    } else {
                        imageView.setImageBitmap(null);
                        if (rVar.f3036a == null) {
                            rVar.f3036a = q.b(rVar.b(), rVar.d());
                        }
                        File file = rVar.f3036a;
                        if (file.exists()) {
                            if (rVar.isImageLoading()) {
                                ((p) rVar.getImageCallback()).a((View) imageView);
                            } else {
                                rVar.setImageLoading(true);
                                p pVar = new p(rVar, imageView, false, 0);
                                rVar.setImageCallback(pVar);
                                pVar.b();
                                u.e().execute(new bn(this, file, rVar, pVar));
                            }
                        } else if (!rVar.isImageLoading()) {
                            a(rVar);
                        }
                    }
                }
            }
            z = true;
        }
        if (z) {
            view.setTag(R.id.tag_item_boolean, true);
            imageView.setColorFilter((ColorFilter) null);
            if (g.a()) {
                imageView.setAlpha(1.0f);
            } else {
                imageView.setAlpha(PurchaseCode.AUTH_INVALID_APP);
            }
        } else {
            view.setTag(R.id.tag_item_boolean, false);
            ColorMatrix colorMatrix = new ColorMatrix();
            colorMatrix.setSaturation(0.0f);
            imageView.setColorFilter(new ColorMatrixColorFilter(colorMatrix));
            if (g.a()) {
                imageView.setAlpha(0.2f);
            } else {
                imageView.setAlpha(50);
            }
        }
        return view;
    }
}
