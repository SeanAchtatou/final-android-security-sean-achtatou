package com.immomo.momo.android.map;

import android.app.Activity;
import android.support.v4.b.a;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.android.b.z;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.n;
import com.immomo.momo.service.bean.ay;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

final class az extends d {

    /* renamed from: a  reason: collision with root package name */
    String f2478a = null;
    private boolean c = false;
    private v d = null;
    private Activity e;
    /* access modifiers changed from: private */
    public /* synthetic */ SelectSiteAMapActivity f;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public az(SelectSiteAMapActivity selectSiteAMapActivity, Activity activity, String str) {
        super(activity);
        this.f = selectSiteAMapActivity;
        this.e = activity;
        this.f2478a = str;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.android.map.SelectSiteAMapActivity.a(com.immomo.momo.android.map.SelectSiteAMapActivity, boolean):void
     arg types: [com.immomo.momo.android.map.SelectSiteAMapActivity, int]
     candidates:
      com.immomo.momo.android.map.SelectSiteAMapActivity.a(com.immomo.momo.android.map.SelectSiteAMapActivity, int):void
      com.immomo.momo.android.map.SelectSiteAMapActivity.a(com.immomo.momo.android.map.SelectSiteAMapActivity, android.location.Location):void
      com.immomo.momo.android.map.SelectSiteAMapActivity.a(com.immomo.momo.android.map.SelectSiteAMapActivity, com.immomo.momo.android.map.az):void
      com.immomo.momo.android.map.SelectSiteAMapActivity.a(com.immomo.momo.android.map.SelectSiteAMapActivity, boolean):void */
    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        this.f.y = true;
        ArrayList arrayList = new ArrayList();
        this.c = n.a().a(arrayList, this.f.n.getLatitude(), this.f.n.getLongitude(), this.f2478a, this.f.u, this.f.p, 1);
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        if (this.f.n == null) {
            cancel(true);
        } else if (!this.f.s && !this.e.isFinishing()) {
            this.d = new v(this.e, (int) R.string.downloading);
            this.d.show();
        }
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        boolean z;
        List list = (List) obj;
        if (a.a((CharSequence) this.f2478a)) {
            this.f.t.clear();
            this.f.t.addAll(list);
        }
        this.f.q.b();
        this.f.q.b((Collection) list);
        if (!a.a((CharSequence) this.f2478a)) {
            Iterator it = list.iterator();
            while (true) {
                if (it.hasNext()) {
                    if (this.f2478a.equals(((ay) it.next()).f)) {
                        z = true;
                        break;
                    }
                } else {
                    z = false;
                    break;
                }
            }
            if (!z) {
                this.f.b.removeFooterView(this.f.l);
                ((TextView) this.f.l.findViewById(R.id.textview)).setText("添加 '" + this.f2478a + "' ");
                this.f.l.findViewById(R.id.layout_footer).setOnClickListener(new ba(this));
                if (z.a(this.f.n)) {
                    this.f.b.addFooterView(this.f.l);
                    this.f.l.requestFocus();
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.f.r = (az) null;
        if (!this.f.s) {
            this.f.s = true;
            this.f.c.getEditableText().clear();
            this.f.i = this.c;
            if (this.d != null && !this.f.isFinishing()) {
                this.d.dismiss();
                this.d = null;
            }
        }
    }
}
