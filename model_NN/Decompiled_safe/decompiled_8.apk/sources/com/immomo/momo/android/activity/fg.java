package com.immomo.momo.android.activity;

import android.view.View;
import com.immomo.momo.util.ao;

final class fg implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ImageBrowserActivity f1489a;

    fg(ImageBrowserActivity imageBrowserActivity) {
        this.f1489a = imageBrowserActivity;
    }

    public final void onClick(View view) {
        int a2 = this.f1489a.c(this.f1489a.k.getCurrentItem());
        if (a2 >= 0 && a2 < this.f1489a.j.b()) {
            fn fnVar = (fn) this.f1489a.h.get(a2);
            if (fnVar.h == null) {
                ao.b("请稍候，图片正在加载中");
            } else {
                this.f1489a.b(new fl(this.f1489a, this.f1489a, fnVar));
            }
        }
    }
}
