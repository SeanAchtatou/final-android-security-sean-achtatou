package com.immomo.momo.android.pay;

import android.content.Context;
import com.immomo.momo.R;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.g;
import com.immomo.momo.protocol.a.a;
import com.immomo.momo.service.aq;
import com.immomo.momo.service.bean.bg;

final class ay extends d {

    /* renamed from: a  reason: collision with root package name */
    private v f2542a = null;
    private String c;
    private bg d = new bg();
    private int e = 0;
    private /* synthetic */ RechargeActivity f;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ay(RechargeActivity rechargeActivity, Context context, String str, int i) {
        super(context);
        this.f = rechargeActivity;
        this.c = str;
        this.f2542a = new v(context);
        this.e = i;
        if (rechargeActivity.x != null && !rechargeActivity.x.isCancelled()) {
            rechargeActivity.x.cancel(true);
        }
        rechargeActivity.x = this;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        String a2 = a.a().a(this.c, this.d);
        if (this.d.f3012a) {
            this.f.f.b(this.d.b);
            new aq().a(this.d.b, this.f.f.h);
        }
        return a2;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
        this.f2542a.setCancelable(false);
        this.f2542a.a("正在验证...");
        this.f.a(this.f2542a);
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        super.a(exc);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.android.pay.RechargeActivity.b(com.immomo.momo.android.pay.RechargeActivity, boolean):void
     arg types: [com.immomo.momo.android.pay.RechargeActivity, int]
     candidates:
      com.immomo.momo.android.pay.RechargeActivity.b(com.immomo.momo.android.pay.RechargeActivity, java.lang.String):void
      com.immomo.momo.android.pay.RechargeActivity.b(com.immomo.momo.android.pay.RechargeActivity, boolean):void */
    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        String str = (String) obj;
        this.f.l.setText(String.valueOf(this.f.f.r()) + " " + g.a((int) R.string.gold_str));
        if (this.d.f3012a) {
            RechargeActivity.m(this.f);
            this.f.v = false;
            this.f.s.setText((int) R.string.payvip_buy);
            a(str);
        } else if (this.e < 4) {
            try {
                Thread.sleep(2000);
            } catch (Throwable th) {
            }
            this.e++;
            a("验证，第" + this.e + "次请求");
            new ay(this.f, this.f.n(), this.c, this.e).execute(new Object[0]);
        } else {
            this.f.v = true;
            a("验证失败，请稍候重新验证。");
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.f.p();
    }
}
