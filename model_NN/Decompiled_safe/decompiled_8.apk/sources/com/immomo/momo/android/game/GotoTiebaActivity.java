package com.immomo.momo.android.game;

import android.content.Intent;
import android.os.Bundle;
import com.immomo.a.a.f.a;
import com.immomo.momo.android.activity.WelcomeActivity;
import com.immomo.momo.android.activity.ao;

public class GotoTiebaActivity extends ao {
    String h;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (this.f == null) {
            startActivity(new Intent(getApplicationContext(), WelcomeActivity.class));
            com.immomo.momo.util.ao.b("客户端没有登录");
            finish();
            return;
        }
        this.h = getIntent().getStringExtra("appid");
        getIntent().getStringExtra("packagename");
        if (!a.a(this.h)) {
            b(new f(this, this));
            return;
        }
        com.immomo.momo.util.ao.b("客户端参数错误");
        finish();
    }
}
