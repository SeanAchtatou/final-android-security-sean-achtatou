package com.immomo.momo.android.view;

import android.view.View;

final class b implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ AppIconMomoGridView f2734a;
    private final /* synthetic */ int b;

    b(AppIconMomoGridView appIconMomoGridView, int i) {
        this.f2734a = appIconMomoGridView;
        this.b = i;
    }

    public final void onClick(View view) {
        if (this.f2734a.f2622a != null) {
            this.f2734a.f2622a.a(this.b, view);
        }
    }
}
