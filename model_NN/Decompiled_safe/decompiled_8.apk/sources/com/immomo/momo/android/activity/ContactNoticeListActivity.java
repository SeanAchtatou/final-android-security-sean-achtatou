package com.immomo.momo.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.b.a;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import com.immomo.momo.R;
import com.immomo.momo.android.a.ae;
import com.immomo.momo.android.c.u;
import com.immomo.momo.android.view.HandyListView;
import com.immomo.momo.android.view.LoadingButton;
import com.immomo.momo.android.view.RefreshOnOverScrollListView;
import com.immomo.momo.android.view.bi;
import com.immomo.momo.android.view.bl;
import com.immomo.momo.android.view.cd;
import com.immomo.momo.android.view.cg;
import com.immomo.momo.g;
import com.immomo.momo.service.aq;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.service.bean.i;
import com.immomo.momo.service.c;
import com.immomo.momo.util.k;
import com.sina.weibo.sdk.constant.Constants;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadPoolExecutor;
import mm.purchasesdk.PurchaseCode;

public class ContactNoticeListActivity extends ah implements bl {
    Handler h = new bt(this);
    /* access modifiers changed from: private */
    public HandyListView i = null;
    private View j = null;
    /* access modifiers changed from: private */
    public LoadingButton k = null;
    /* access modifiers changed from: private */
    public ae l = null;
    /* access modifiers changed from: private */
    public aq m = null;
    /* access modifiers changed from: private */
    public c n = null;
    private ThreadPoolExecutor o = null;
    /* access modifiers changed from: private */
    public cd p = null;
    private boolean q = false;
    private Map r = new HashMap();

    private void a(i iVar) {
        String h2 = iVar.h();
        if (!a.a((CharSequence) h2)) {
            bf b = this.m.b(h2);
            if (b == null) {
                if (this.r.get(h2) != null) {
                    b = (bf) this.r.get(h2);
                } else {
                    b = new bf(h2);
                    this.r.put(h2, b);
                }
                b.setImageMultipleDiaplay(true);
            }
            iVar.a(b);
        }
    }

    /* access modifiers changed from: private */
    public void c(int i2) {
        List<i> a2 = this.n.a(i2);
        if (!a2.isEmpty()) {
            if (a2.size() > 20) {
                a2.remove(a2.size() - 1);
                this.j.setVisibility(0);
            } else {
                this.i.removeFooterView(this.j);
            }
            for (i a3 : a2) {
                a(a3);
            }
            v();
            this.l.b((Collection) a2);
        }
    }

    private void v() {
        if (this.r.size() > 0) {
            this.o.execute(new bz(this, this.r));
            this.r.clear();
        }
    }

    public final void a(Bundle bundle) {
        super.a(bundle);
        setContentView((int) R.layout.activity_contactnoticelist);
        this.m = new aq();
        this.n = new c();
        this.o = u.a();
        this.i = (RefreshOnOverScrollListView) findViewById(R.id.listview);
        setTitle("好友推荐");
        this.j = g.o().inflate((int) R.layout.common_list_loadmore, (ViewGroup) null);
        this.j.setVisibility(8);
        this.k = (LoadingButton) this.j.findViewById(R.id.btn_loadmore);
        this.k.setNormalIconResId(R.drawable.ic_btn_inner_clock);
        this.i.addFooterView(this.j);
        this.i.addHeaderView(g.o().inflate((int) R.layout.listitem_blank, (ViewGroup) null));
        this.i.a(g.o().inflate((int) R.layout.include_contact_listempty, (ViewGroup) null));
        a(new bi(getApplicationContext()).a((int) R.drawable.ic_topbar_adduser), new bu(this));
        this.k.setOnProcessListener(this);
        this.p = new cd(this, this.i);
        this.p.a();
        this.i.setMultipleSelector(this.p);
        this.p.a((int) R.id.item_layout);
        this.p.a((cg) new bv(this));
        this.p.a(new com.immomo.momo.android.view.a(this).a(), new bw(this));
        this.l = new ae(this.i, this);
    }

    /* access modifiers changed from: protected */
    public final boolean a(Bundle bundle, String str) {
        if (!"actions.contactnotice".equals(str)) {
            return super.a(bundle, str);
        }
        i c = this.n.c(bundle.getString("msgid"));
        if (c != null) {
            a(c);
            v();
            this.l.b(0, c);
        }
        if (!q()) {
            this.q = true;
        }
        return q();
    }

    /* access modifiers changed from: protected */
    public final void d() {
        if (this.n.g() > 0) {
            t().s();
        }
        c(0);
    }

    /* access modifiers changed from: protected */
    public final void e() {
        super.e();
        this.i.setAdapter((ListAdapter) this.l);
        d();
        a((int) PurchaseCode.QUERY_FROZEN, "actions.contactnotice");
    }

    /* access modifiers changed from: protected */
    public final boolean k() {
        return true;
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        if (i3 == -1) {
            switch (i2) {
                case 21:
                    a.a(this, 3);
                    return;
                case Constants.WEIBO_SDK_VERSION /*22*/:
                    a.a(this, 2);
                    return;
                case 23:
                    a.a(this, 1);
                    return;
                default:
                    return;
            }
        }
    }

    public void onBackPressed() {
        if (this.p.g()) {
            this.p.e();
        } else {
            super.onBackPressed();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        new k("PO", "P57").e();
        this.n.h();
        Bundle bundle = new Bundle();
        bundle.putString("sessionid", "-2230");
        bundle.putInt("sessiontype", 5);
        g.d().a(bundle, "action.sessionchanged");
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        new k("PI", "P57").e();
        if (this.q) {
            t().s();
            this.q = false;
        }
    }

    public final void u() {
        new by(this, (byte) 0).execute(new Object[0]);
    }
}
