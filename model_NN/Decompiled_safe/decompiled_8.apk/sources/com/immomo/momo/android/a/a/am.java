package com.immomo.momo.android.a.a;

import android.content.Intent;
import android.view.View;
import com.immomo.momo.android.activity.group.GroupProfileActivity;
import com.immomo.momo.android.activity.maintab.bh;
import com.immomo.momo.service.bean.ax;

final class am implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ah f682a;
    private final /* synthetic */ ax b;

    am(ah ahVar, ax axVar) {
        this.f682a = ahVar;
        this.b = axVar;
    }

    public final void onClick(View view) {
        Intent intent = new Intent();
        bh unused = this.f682a.d;
        intent.setClass(bh.H(), GroupProfileActivity.class);
        intent.putExtra("tag", "local");
        intent.putExtra("gid", this.b.f3000a);
        this.f682a.d.a(intent);
    }
}
