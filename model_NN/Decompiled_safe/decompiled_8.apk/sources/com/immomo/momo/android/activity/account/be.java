package com.immomo.momo.android.activity.account;

import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import com.immomo.momo.android.broadcast.d;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

final class be implements d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ bc f958a;

    be(bc bcVar) {
        this.f958a = bcVar;
    }

    public final void a(Intent intent) {
        Bundle extras = intent.getExtras();
        if (extras != null) {
            Object[] objArr = (Object[]) extras.get("pdus");
            SmsMessage[] smsMessageArr = new SmsMessage[objArr.length];
            for (int i = 0; i < objArr.length; i++) {
                smsMessageArr[i] = SmsMessage.createFromPdu((byte[]) objArr[i]);
            }
            for (SmsMessage messageBody : smsMessageArr) {
                String messageBody2 = messageBody.getMessageBody();
                if (messageBody2.indexOf("陌陌") >= 0 && messageBody2.indexOf("验证码") >= 0) {
                    Matcher matcher = Pattern.compile("\\d{6,}").matcher(messageBody2);
                    if (matcher.find()) {
                        bc.a(this.f958a, matcher.group());
                        return;
                    }
                }
            }
        }
    }
}
