package com.immomo.momo.android.activity.group.foundgroup;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.b.a;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.ViewFlipper;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.a.a.f.b;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.ImageFactoryActivity;
import com.immomo.momo.android.activity.ah;
import com.immomo.momo.android.map.SelectSiteAMapActivity;
import com.immomo.momo.android.view.HeaderLayout;
import com.immomo.momo.android.view.a.n;
import com.immomo.momo.android.view.a.o;
import com.immomo.momo.android.view.bi;
import com.immomo.momo.g;
import com.immomo.momo.util.ak;
import com.immomo.momo.util.ao;
import com.immomo.momo.util.h;
import com.immomo.momo.util.k;
import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.UUID;
import mm.purchasesdk.PurchaseCode;
import org.json.JSONException;
import org.json.JSONObject;

public class FoundGroupActivity extends ah implements View.OnClickListener, c {
    Button h;
    private a i;
    private b j;
    private f k;
    private h l;
    private j m;
    private l n;
    private s o;
    private View p;
    private Map q = null;
    private int r = 0;
    private Button s;
    private ViewFlipper t;
    private TextView u;
    private bi v;
    private boolean w = true;
    private String x = PoiTypeDef.All;
    private File y = null;

    private void b(boolean z) {
        if (z) {
            x();
            this.i.b();
        } else if (this.i.c()) {
            if (this.r == 4) {
                this.h.setText("提交");
            } else {
                this.h.setText("下一步");
            }
            this.t.setInAnimation(getApplicationContext(), R.anim.push_left_in);
            this.t.setOutAnimation(getApplicationContext(), R.anim.push_left_out);
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService("input_method");
            View currentFocus = getCurrentFocus();
            if (currentFocus != null) {
                inputMethodManager.hideSoftInputFromWindow(currentFocus.getWindowToken(), 2);
            }
            x();
        }
    }

    private void x() {
        if (this.i != null) {
            this.i.e();
        }
        this.t.showNext();
        this.r++;
        this.s.setText(this.r > 2 ? "上一步" : "返回");
        y();
        if (this.r <= 1) {
            this.p.setVisibility(8);
            this.u.setVisibility(8);
            this.v.setVisibility(0);
        } else {
            this.p.setVisibility(0);
            this.u.setVisibility(0);
            this.v.setVisibility(8);
            this.w = false;
        }
        if (this.i != null) {
            this.i.d();
        }
    }

    private void y() {
        this.u.setText(String.valueOf(this.r - 1) + "/4");
        switch (this.r) {
            case 1:
                if (this.j == null) {
                    b bVar = new b(this.t.getCurrentView(), this);
                    this.j = bVar;
                    this.i = bVar;
                } else {
                    this.i = this.j;
                }
                this.j.a(this);
                m().setTitleText("创建地点群");
                return;
            case 2:
                if (this.k == null) {
                    f fVar = new f(this.t.getCurrentView(), this, this.o);
                    this.k = fVar;
                    this.i = fVar;
                } else {
                    this.i = this.k;
                }
                m().setTitleText("选择群地点");
                return;
            case 3:
                if (this.l == null) {
                    h hVar = new h(this.t.getCurrentView(), this, this.o);
                    this.l = hVar;
                    this.i = hVar;
                } else {
                    this.i = this.l;
                }
                m().setTitleText("输入群名称");
                return;
            case 4:
                if (this.m == null) {
                    j jVar = new j(this.t.getCurrentView(), this, this.o);
                    this.m = jVar;
                    this.i = jVar;
                } else {
                    this.i = this.m;
                }
                m().setTitleText("输入群介绍");
                return;
            case 5:
                l lVar = new l(this.t.getCurrentView(), this, this.o);
                this.n = lVar;
                this.i = lVar;
                m().setTitleText("选择群头像");
                return;
            default:
                return;
        }
    }

    private void z() {
        if (this.r > 1) {
            if (this.i != null) {
                this.i.e();
            }
            this.t.setInAnimation(getApplicationContext(), R.anim.push_right_in);
            this.t.setOutAnimation(getApplicationContext(), R.anim.push_right_out);
            this.t.showPrevious();
            this.r--;
            y();
            if (this.i != null) {
                this.i.d();
            }
            if (this.r <= 1) {
                this.p.setVisibility(8);
                this.u.setVisibility(8);
                this.v.setVisibility(0);
            } else {
                this.p.setVisibility(0);
                this.u.setVisibility(0);
                this.v.setVisibility(8);
            }
            this.s.setText(this.r <= 2 ? "返回" : "上一步");
            switch (this.r) {
                case 4:
                    this.h.setText("下一步");
                    return;
                default:
                    return;
            }
        } else if (this.w) {
            finish();
        } else {
            n.a(this, (int) R.string.str_giveup_create_group, new q(this)).show();
        }
    }

    public final void a() {
        b(false);
    }

    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        super.a(bundle);
        setContentView((int) R.layout.activity_creategroup);
        setTitle((int) R.string.str_creategroup_title);
        this.h = (Button) findViewById(R.id.btn_ok);
        this.s = (Button) findViewById(R.id.btn_back);
        this.p = findViewById(R.id.layout_btn);
        this.t = (ViewFlipper) findViewById(R.id.cg_viewflipper);
        HeaderLayout headerLayout = (HeaderLayout) findViewById(R.id.layout_header);
        this.v = new bi(this);
        this.v.a((int) R.drawable.ic_topbar_about);
        this.v.setOnClickListener(new p(this));
        headerLayout.a(this.v);
        this.u = (TextView) g.o().inflate((int) R.layout.include_headerbar_righttext, (ViewGroup) null);
        headerLayout.a(this.u);
        this.o = new s();
        this.h.setOnClickListener(this);
        this.s.setOnClickListener(this);
        if (bundle == null) {
            b(true);
        }
    }

    public final void a(Map map) {
        this.q = map;
    }

    public final void b() {
        b(false);
    }

    public final String c(int i2) {
        return (String) this.q.get(Integer.valueOf(i2));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.b.a.a(android.graphics.Bitmap, float, boolean):android.graphics.Bitmap
     arg types: [android.graphics.Bitmap, int, int]
     candidates:
      android.support.v4.b.a.a(android.graphics.Bitmap, int, int):android.graphics.Bitmap
      android.support.v4.b.a.a(java.io.File, int, int):android.graphics.Bitmap
      android.support.v4.b.a.a(java.lang.String, java.lang.String, java.lang.String):java.lang.String
      android.support.v4.b.a.a(java.lang.Object[], java.lang.String, java.lang.String):java.lang.String
      android.support.v4.b.a.a(android.graphics.Bitmap, float, boolean):android.graphics.Bitmap */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public void onActivityResult(int i2, int i3, Intent intent) {
        Uri fromFile;
        float f;
        float f2;
        Bitmap createBitmap;
        boolean z = false;
        switch (i2) {
            case PurchaseCode.QUERY_OK /*101*/:
                if (i3 == -1) {
                    this.e.a((Object) ("resultCode=" + i3));
                    if (!a.a((CharSequence) this.x)) {
                        File file = new File(com.immomo.momo.a.i(), this.x);
                        if (file.exists()) {
                            file.delete();
                        }
                        this.x = PoiTypeDef.All;
                    }
                    this.e.a((Object) ("avatorFile=" + this.y));
                    if (this.y != null) {
                        String absolutePath = this.y.getAbsolutePath();
                        String name = new File(absolutePath).getName();
                        try {
                            Bitmap decodeFile = BitmapFactory.decodeFile(absolutePath);
                            if (decodeFile != null) {
                                if (decodeFile.getWidth() > 640 || decodeFile.getHeight() > 640) {
                                    int width = decodeFile.getWidth();
                                    int height = decodeFile.getHeight();
                                    if ((width > height ? width : height) == 640) {
                                        createBitmap = decodeFile;
                                    } else {
                                        float f3 = ((float) width) / ((float) height);
                                        if (width >= height) {
                                            f = 640.0f / ((float) width);
                                            f2 = ((float) ((int) (640.0f / f3))) / ((float) height);
                                        } else {
                                            f = ((float) ((int) (f3 * 640.0f))) / ((float) width);
                                            f2 = 640.0f / ((float) height);
                                        }
                                        Matrix matrix = new Matrix();
                                        matrix.postScale(f, f2);
                                        createBitmap = Bitmap.createBitmap(decodeFile, 0, 0, width, height, matrix, true);
                                        if (decodeFile != null) {
                                            decodeFile.recycle();
                                        }
                                    }
                                    h.a(createBitmap, this.y);
                                    decodeFile.recycle();
                                    decodeFile = createBitmap;
                                }
                                Bitmap a2 = a.a(decodeFile, 150.0f, true);
                                decodeFile.recycle();
                                s sVar = this.o;
                                new String[1][0] = name.substring(0, name.lastIndexOf("."));
                                this.o.f1677a = this.y;
                                if (this.n != null) {
                                    this.n.a(this.y);
                                    this.n.a(a2);
                                    this.n.f();
                                    return;
                                }
                                return;
                            }
                            return;
                        } catch (Throwable th) {
                            return;
                        }
                    } else {
                        return;
                    }
                } else if (i3 == 1003) {
                    ao.d(R.string.cropimage_error_size);
                    return;
                } else if (i3 == 1000) {
                    ao.d(R.string.cropimage_error_other);
                    return;
                } else if (i3 == 1001) {
                    ao.d(R.string.cropimage_error_store);
                    return;
                } else if (i3 == 1002) {
                    ao.d(R.string.cropimage_error_filenotfound);
                    return;
                } else {
                    return;
                }
            case PurchaseCode.ORDER_OK /*102*/:
                if (i3 == -1 && intent != null) {
                    Uri data = intent.getData();
                    Intent intent2 = new Intent(getApplicationContext(), ImageFactoryActivity.class);
                    intent2.setData(data);
                    intent2.putExtra("aspectY", 1);
                    intent2.putExtra("aspectX", 1);
                    intent2.putExtra("minsize", 320);
                    this.y = new File(com.immomo.momo.a.g(), String.valueOf(b.a()) + ".jpg_");
                    intent2.putExtra("outputFilePath", this.y.getAbsolutePath());
                    startActivityForResult(intent2, PurchaseCode.QUERY_OK);
                    return;
                }
                return;
            case PurchaseCode.UNSUB_OK /*103*/:
                this.e.a((Object) ("camera_filename:" + this.x + ", resultCode:" + i3 + ", data:" + intent));
                if (i3 == -1 && !a.a((CharSequence) this.x) && (fromFile = Uri.fromFile(new File(com.immomo.momo.a.i(), this.x))) != null) {
                    Intent intent3 = new Intent(getApplicationContext(), ImageFactoryActivity.class);
                    intent3.setData(fromFile);
                    intent3.putExtra("aspectY", 1);
                    intent3.putExtra("aspectX", 1);
                    intent3.putExtra("minsize", 320);
                    this.y = new File(com.immomo.momo.a.g(), String.valueOf(b.a()) + ".jpg_");
                    intent3.putExtra("outputFilePath", this.y.getAbsolutePath());
                    startActivityForResult(intent3, PurchaseCode.QUERY_OK);
                    return;
                }
                return;
            case 151:
                if (i3 == -1) {
                    String stringExtra = intent.getStringExtra("siteid");
                    String stringExtra2 = intent.getStringExtra("sitename");
                    this.o.g = intent.getIntExtra("sitetype", this.o.g);
                    this.o.h = intent.getDoubleExtra("lat", g.q().S);
                    this.o.i = intent.getDoubleExtra("lng", g.q().T);
                    this.o.j = intent.getIntExtra("loctype", g.q().aH);
                    if (a.a((CharSequence) stringExtra)) {
                        this.o.f = PoiTypeDef.All;
                    }
                    f fVar = this.k;
                    if (this.o.g == 0) {
                        z = true;
                    }
                    fVar.a(z);
                    if (this.o.g == 0) {
                        this.o.g = 1;
                    }
                    this.k.f();
                    this.o.f = stringExtra;
                    this.o.d = stringExtra2;
                    this.k.a(stringExtra2);
                    return;
                }
                return;
            default:
                return;
        }
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_back /*2131165286*/:
                z();
                return;
            case R.id.btn_ok /*2131165289*/:
                b(false);
                return;
            case R.id.layout_choose_location /*2131166141*/:
                Intent intent = new Intent(this, SelectSiteAMapActivity.class);
                intent.putExtra("sitetype", this.o.g);
                startActivityForResult(intent, 151);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 != 4) {
            return super.onKeyDown(i2, keyEvent);
        }
        z();
        return true;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        new k("PO", "P641").e();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.ak.a(java.lang.String, java.lang.Integer):int
     arg types: [java.lang.String, int]
     candidates:
      com.immomo.momo.util.ak.a(android.content.Context, java.lang.String):com.immomo.momo.util.ak
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Long):long
      com.immomo.momo.util.ak.a(java.lang.String, int):void
      com.immomo.momo.util.ak.a(java.lang.String, long):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.String):void
      com.immomo.momo.util.ak.a(java.lang.String, boolean):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Boolean):boolean
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Integer):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.ak.a(java.lang.String, java.lang.Boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.immomo.momo.util.ak.a(android.content.Context, java.lang.String):com.immomo.momo.util.ak
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Integer):int
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Long):long
      com.immomo.momo.util.ak.a(java.lang.String, int):void
      com.immomo.momo.util.ak.a(java.lang.String, long):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.String):void
      com.immomo.momo.util.ak.a(java.lang.String, boolean):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Boolean):boolean */
    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Bundle bundle) {
        super.onRestoreInstanceState(bundle);
        ak a2 = ak.a(getApplicationContext(), "tmp_creategroup");
        this.o.c = a2.b("group_name", PoiTypeDef.All);
        this.o.e = a2.b("group_introduction", PoiTypeDef.All);
        this.o.d = a2.b("group_location", PoiTypeDef.All);
        this.o.f = a2.b("group_siteid", PoiTypeDef.All);
        this.o.g = a2.a("group_sitetype", (Integer) 1);
        if (!a.a((CharSequence) a2.b("avatorFile", PoiTypeDef.All))) {
            this.y = new File(a2.b("avatorFile", PoiTypeDef.All));
            this.o.f1677a = this.y;
        }
        if (!a.a((CharSequence) a2.b("camera_filename", PoiTypeDef.All))) {
            this.x = a2.b("camera_filename", PoiTypeDef.All);
        }
        this.o.b = a2.a("use_default_avatar", (Boolean) true);
        String b = a2.b("defaultBgMap", PoiTypeDef.All);
        if (!a.a((CharSequence) b)) {
            try {
                HashMap hashMap = new HashMap();
                JSONObject jSONObject = new JSONObject(b);
                Iterator<String> keys = jSONObject.keys();
                while (keys.hasNext()) {
                    try {
                        String next = keys.next();
                        hashMap.put(Integer.valueOf(Integer.parseInt(next)), jSONObject.getString(next));
                    } catch (Exception e) {
                    }
                }
                this.q = hashMap;
            } catch (JSONException e2) {
            }
        }
        int a3 = a2.a("step_index", (Integer) 1);
        for (int i2 = this.r; i2 < a3; i2++) {
            this.e.a((Object) ("renext...index=" + this.r));
            b(true);
        }
        a2.b();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        new k("PI", "P641").e();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.Integer]
     candidates:
      com.immomo.momo.util.ak.a(android.content.Context, java.lang.String):com.immomo.momo.util.ak
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Integer):int
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Long):long
      com.immomo.momo.util.ak.a(java.lang.String, int):void
      com.immomo.momo.util.ak.a(java.lang.String, long):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.String):void
      com.immomo.momo.util.ak.a(java.lang.String, boolean):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Boolean):boolean
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.immomo.momo.util.ak.a(android.content.Context, java.lang.String):com.immomo.momo.util.ak
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Integer):int
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Long):long
      com.immomo.momo.util.ak.a(java.lang.String, int):void
      com.immomo.momo.util.ak.a(java.lang.String, long):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.String):void
      com.immomo.momo.util.ak.a(java.lang.String, boolean):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Boolean):boolean
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.Boolean]
     candidates:
      com.immomo.momo.util.ak.a(android.content.Context, java.lang.String):com.immomo.momo.util.ak
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Integer):int
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Long):long
      com.immomo.momo.util.ak.a(java.lang.String, int):void
      com.immomo.momo.util.ak.a(java.lang.String, long):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.String):void
      com.immomo.momo.util.ak.a(java.lang.String, boolean):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Boolean):boolean
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void */
    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        ak a2 = ak.a(getApplicationContext(), "tmp_creategroup");
        a2.b();
        a2.a("step_index", (Object) Integer.valueOf(this.r));
        if (!a.a((CharSequence) this.o.c)) {
            a2.a("group_name", (Object) this.o.c);
        }
        if (!a.a((CharSequence) this.o.e)) {
            a2.a("group_introduction", (Object) this.o.e);
        }
        if (!a.a((CharSequence) this.o.d)) {
            a2.a("group_location", (Object) this.o.d);
        }
        if (!a.a((CharSequence) this.o.f)) {
            a2.a("group_siteid", (Object) this.o.f);
        }
        a2.a("group_sitetype", (Object) Integer.valueOf(this.o.g));
        if (!a.a((CharSequence) this.o.f)) {
            a2.a("avatorFile", (Object) this.o.f);
        }
        if (this.y != null) {
            a2.a("avatorFile", this.y.getAbsoluteFile());
        }
        if (!a.a((CharSequence) this.x)) {
            a2.a("camera_filename", (Object) this.x);
        }
        if (this.q != null) {
            JSONObject jSONObject = new JSONObject();
            for (Map.Entry entry : this.q.entrySet()) {
                try {
                    jSONObject.put(new StringBuilder().append(entry.getKey()).toString(), entry.getValue());
                } catch (JSONException e) {
                }
            }
            a2.a("defaultBgMap", (Object) jSONObject.toString());
        }
        a2.a("use_default_avatar", (Object) Boolean.valueOf(this.o.b));
    }

    public final void u() {
        o oVar = new o(this, (int) R.array.editprofile_add_photo);
        oVar.setTitle((int) R.string.dialog_title_add_pic);
        oVar.a(new r(this));
        oVar.show();
    }

    public final void v() {
        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("immomo_");
        stringBuffer.append(a.c(new Date()));
        stringBuffer.append("_" + UUID.randomUUID());
        stringBuffer.append(".jpg");
        this.x = stringBuffer.toString();
        this.e.a((Object) ("camera_filename=" + this.x));
        intent.putExtra("output", Uri.fromFile(new File(com.immomo.momo.a.i(), this.x)));
        startActivityForResult(intent, PurchaseCode.UNSUB_OK);
    }

    public final void w() {
        Intent intent = new Intent("android.intent.action.GET_CONTENT");
        intent.setType("image/*");
        startActivityForResult(Intent.createChooser(intent, "本地图片"), PurchaseCode.ORDER_OK);
    }
}
