package com.immomo.momo.android.activity.message;

import android.widget.AbsListView;
import com.immomo.momo.android.view.au;

public final class y implements AbsListView.OnScrollListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ a f1997a;

    public y(a aVar) {
        this.f1997a = aVar;
    }

    public final void onScroll(AbsListView absListView, int i, int i2, int i3) {
        if (this.f1997a.r()) {
            Runnable c = this.f1997a.am;
            if (c != null) {
                absListView.removeCallbacks(c);
            } else {
                c = new ai(this.f1997a, absListView);
            }
            absListView.postDelayed(c, 50);
            this.f1997a.am = c;
            if (i == 0) {
                this.f1997a.l.b();
            }
        }
    }

    public final void onScrollStateChanged(AbsListView absListView, int i) {
        if (i == 0) {
            au.k();
        }
    }
}
