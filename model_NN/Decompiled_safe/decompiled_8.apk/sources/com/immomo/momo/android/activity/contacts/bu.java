package com.immomo.momo.android.activity.contacts;

import android.content.Intent;
import android.support.v4.b.a;
import com.immomo.momo.android.a.hf;
import com.immomo.momo.android.broadcast.d;
import com.immomo.momo.android.broadcast.t;

final class bu implements d {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ bk f1175a;

    bu(bk bkVar) {
        this.f1175a = bkVar;
    }

    public final void a(Intent intent) {
        if (t.f2363a.equals(intent.getAction())) {
            if (this.f1175a.X == null) {
                bk bkVar = this.f1175a;
                bk bkVar2 = this.f1175a;
                bk bkVar3 = this.f1175a;
                bkVar.X = new bz(bkVar2, bk.H());
            } else {
                this.f1175a.X.cancel(true);
                bk bkVar4 = this.f1175a;
                bk bkVar5 = this.f1175a;
                bk bkVar6 = this.f1175a;
                bkVar4.X = new bz(bkVar5, bk.H());
            }
            this.f1175a.X.execute(new Object[0]);
        } else if (t.b.equals(intent.getAction())) {
            String stringExtra = intent.getStringExtra("disid");
            if (!a.a((CharSequence) stringExtra)) {
                this.f1175a.aa.post(new bv(this, stringExtra));
            }
        } else if (t.c.equals(intent.getAction())) {
            String stringExtra2 = intent.getStringExtra("disid");
            if (!a.a((CharSequence) stringExtra2)) {
                this.f1175a.aa.post(new bw(this, stringExtra2));
            }
        } else if (t.d.equals(intent.getAction())) {
            String str = intent.getExtras() != null ? (String) intent.getExtras().get("disid") : null;
            if (!a.a((CharSequence) str)) {
                this.f1175a.aa.post(new bx(this, this.f1175a.P.a(str, hf.f857a), str));
            }
        }
    }
}
