package com.immomo.momo.android.map;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.b.a;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ListAdapter;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.a.au;
import com.immomo.momo.android.activity.ah;
import com.immomo.momo.android.view.a.n;
import com.immomo.momo.android.view.bi;
import com.immomo.momo.g;
import com.immomo.momo.service.am;
import java.util.HashMap;

public class CreateSiteActivity extends ah implements View.OnClickListener {
    /* access modifiers changed from: private */
    public EditText h;
    /* access modifiers changed from: private */
    public TextView i;
    private boolean j = false;
    /* access modifiers changed from: private */
    public int k = -1;
    /* access modifiers changed from: private */
    public String l = PoiTypeDef.All;
    /* access modifiers changed from: private */
    public String m = PoiTypeDef.All;

    public CreateSiteActivity() {
        new HashMap();
    }

    static /* synthetic */ boolean c(CreateSiteActivity createSiteActivity) {
        if (a.a((CharSequence) createSiteActivity.h.getText().toString().trim())) {
            createSiteActivity.a((CharSequence) "地点名称不能为空");
            return false;
        } else if (createSiteActivity.k != -1) {
            return true;
        } else {
            createSiteActivity.a((CharSequence) "请选择地点类型");
            return false;
        }
    }

    static /* synthetic */ String h(CreateSiteActivity createSiteActivity) {
        switch (createSiteActivity.k) {
            case 1:
                createSiteActivity.l = createSiteActivity.getString(R.string.editsite_sitename1);
                break;
            case 2:
                createSiteActivity.l = createSiteActivity.getString(R.string.editsite_sitename2);
                break;
            case 3:
                createSiteActivity.l = createSiteActivity.getString(R.string.editsite_sitename3);
                break;
            case 4:
                createSiteActivity.l = createSiteActivity.getString(R.string.editsite_sitename4);
                break;
            default:
                createSiteActivity.l = createSiteActivity.getString(R.string.editsite_defaultbtn_text);
                break;
        }
        return createSiteActivity.l;
    }

    /* access modifiers changed from: protected */
    public final void b(Bundle bundle) {
        super.b(bundle);
        setContentView((int) R.layout.activity_create_site);
        d();
        m().setTitleText((int) R.string.act_csite_titile);
        m().a(new bi(this).b(R.string.post).a((int) R.drawable.ic_topbar_confirm), new l(this));
        this.h = (EditText) findViewById(R.id.et_sitename);
        this.i = (TextView) findViewById(R.id.tv_sitetypename);
        findViewById(R.id.layout_sitetype).setOnClickListener(this);
        this.h.setOnFocusChangeListener(new k(this));
        this.h.setText(this.m);
    }

    /* access modifiers changed from: protected */
    public final void d() {
        Intent intent = getIntent();
        if (intent != null) {
            intent.getDoubleExtra("lat", g.q().S);
            intent.getDoubleExtra("lng", g.q().T);
            intent.getIntExtra("loctype", g.q().aH);
            this.m = intent.getStringExtra("sitename");
            this.j = intent.getBooleanExtra("key_issearchsite", false);
        }
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.layout_sitetype /*2131165470*/:
                if (this.j) {
                    n nVar = new n(this);
                    nVar.setTitle((int) R.string.editsite_dialog_title);
                    View inflate = getLayoutInflater().inflate((int) R.layout.dialog_selecttype_forsearch, (ViewGroup) null);
                    GridView gridView = (GridView) inflate.findViewById(R.id.gv_selectsite);
                    au auVar = new au(this, new am().c.b());
                    gridView.setColumnWidth(getResources().getDimensionPixelSize(R.dimen.selecttype_item_width));
                    gridView.setAdapter((ListAdapter) auVar);
                    gridView.setOnItemClickListener(new m(this, nVar));
                    nVar.setContentView(inflate);
                    nVar.show();
                    return;
                }
                n nVar2 = new n(this);
                n nVar3 = new n(this, nVar2);
                nVar2.setTitle((int) R.string.editsite_dialog_title);
                View inflate2 = getLayoutInflater().inflate((int) R.layout.dialog_select_sitetype, (ViewGroup) null);
                inflate2.findViewById(R.id.layout_type_department).setOnClickListener(nVar3);
                inflate2.findViewById(R.id.layout_type_school).setOnClickListener(nVar3);
                inflate2.findViewById(R.id.layout_type_restaurant).setOnClickListener(nVar3);
                inflate2.findViewById(R.id.layout_type_office).setOnClickListener(nVar3);
                inflate2.findViewById(R.id.layout_type_trafic).setOnClickListener(nVar3);
                inflate2.findViewById(R.id.layout_type_entertainment).setOnClickListener(nVar3);
                nVar2.setContentView(inflate2);
                nVar2.show();
                return;
            default:
                return;
        }
    }
}
