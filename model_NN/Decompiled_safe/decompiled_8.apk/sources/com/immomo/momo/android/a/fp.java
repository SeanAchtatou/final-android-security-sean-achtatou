package com.immomo.momo.android.a;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.immomo.momo.R;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.aj;
import com.immomo.momo.util.j;
import com.immomo.momo.util.m;
import java.util.List;

public final class fp extends a {
    public fp(Context context, List list) {
        super(context, list);
        new m(this);
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        ImageView imageView;
        if (view == null) {
            view = a((int) R.layout.listitem_groupfeeds_headerwall);
            ImageView imageView2 = (ImageView) view.findViewById(R.id.headerwall_iv_header);
            ViewGroup.LayoutParams layoutParams = imageView2.getLayoutParams();
            int J = g.J() / 5;
            layoutParams.height = J;
            layoutParams.width = J;
            imageView2.setLayoutParams(layoutParams);
            view.setTag(imageView2);
            imageView = imageView2;
        } else {
            imageView = (ImageView) view.getTag();
        }
        j.a((aj) getItem(i), imageView, (ViewGroup) null, 3);
        return view;
    }
}
