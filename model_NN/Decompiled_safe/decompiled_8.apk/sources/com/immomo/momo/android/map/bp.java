package com.immomo.momo.android.map;

import android.app.Activity;
import android.support.v4.b.a;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.android.b.z;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.n;
import com.immomo.momo.service.bean.ay;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

final class bp extends d {

    /* renamed from: a  reason: collision with root package name */
    String f2495a = null;
    private boolean c = false;
    private v d = null;
    private Activity e;
    private int f = 0;
    /* access modifiers changed from: private */
    public /* synthetic */ SelectSiteGoogleActivity g;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public bp(SelectSiteGoogleActivity selectSiteGoogleActivity, Activity activity, String str, int i) {
        super(activity);
        this.g = selectSiteGoogleActivity;
        this.e = activity;
        this.f2495a = str;
        this.f = i;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        ArrayList arrayList = new ArrayList();
        this.c = n.a().a(arrayList, this.g.n.getLatitude(), this.g.n.getLongitude(), this.f2495a, this.f, this.g.p, 3);
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        if (this.g.n == null) {
            cancel(true);
        } else if (!this.g.s && !this.e.isFinishing()) {
            this.d = new v(this.e, (int) R.string.downloading);
            this.d.show();
        }
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        boolean z;
        List list = (List) obj;
        if (a.a((CharSequence) this.f2495a)) {
            this.g.t.clear();
            this.g.t.addAll(list);
        }
        this.g.q.b();
        this.g.q.b((Collection) list);
        if (!a.a((CharSequence) this.f2495a)) {
            Iterator it = list.iterator();
            while (true) {
                if (it.hasNext()) {
                    if (this.f2495a.equals(((ay) it.next()).f)) {
                        z = true;
                        break;
                    }
                } else {
                    z = false;
                    break;
                }
            }
            if (!z) {
                this.g.f2453a.removeFooterView(this.g.m);
                ((TextView) this.g.m.findViewById(R.id.textview)).setText("添加 '" + this.f2495a + "' ");
                this.g.m.findViewById(R.id.layout_footer).setOnClickListener(new bq(this));
                if (z.a(this.g.n)) {
                    this.g.f2453a.addFooterView(this.g.m);
                    this.g.m.requestFocus();
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.g.r = (bp) null;
        if (!this.g.s) {
            this.g.s = true;
            this.g.b.getEditableText().clear();
            this.g.j = this.c;
            if (this.d != null && !this.g.isFinishing()) {
                this.d.dismiss();
                this.d = null;
            }
        }
    }
}
