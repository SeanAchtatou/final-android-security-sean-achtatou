package com.immomo.momo.android.game;

import android.view.View;
import com.immomo.momo.android.view.a.o;
import java.util.List;

final class p implements View.OnClickListener {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public /* synthetic */ k f2440a;

    p(k kVar) {
        this.f2440a = kVar;
    }

    public final void onClick(View view) {
        if (this.f2440a.W.b != null) {
            aj ajVar = this.f2440a.W.b;
            List list = ajVar.h;
            String[] strArr = new String[list.size()];
            int i = 0;
            int i2 = -1;
            while (true) {
                int i3 = i;
                if (i3 >= list.size()) {
                    o oVar = new o(this.f2440a.c(), strArr, i2);
                    oVar.a(new q(this, list, ajVar));
                    oVar.show();
                    return;
                }
                if (((Integer) list.get(i3)).intValue() == ajVar.d) {
                    i2 = i3;
                }
                strArr[i3] = list.get(i3) + "元";
                i = i3 + 1;
            }
        }
    }
}
