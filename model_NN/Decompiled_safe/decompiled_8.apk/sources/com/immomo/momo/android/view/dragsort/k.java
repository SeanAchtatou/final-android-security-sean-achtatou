package com.immomo.momo.android.view.dragsort;

import android.os.SystemClock;
import android.view.View;

final class k implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private long f2796a;
    private long b;
    private int c;
    private float d;
    private long e;
    private int f;
    private float g;
    private boolean h = false;
    private /* synthetic */ DragSortListView i;

    public k(DragSortListView dragSortListView) {
        this.i = dragSortListView;
    }

    public final void a(int i2) {
        if (!this.h) {
            this.h = true;
            this.e = SystemClock.uptimeMillis();
            this.f2796a = this.e;
            this.f = i2;
            this.i.post(this);
        }
    }

    public final boolean a() {
        return this.h;
    }

    public final int b() {
        if (this.h) {
            return this.f;
        }
        return -1;
    }

    public final void c() {
        this.i.removeCallbacks(this);
        this.h = false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.android.view.dragsort.DragSortListView.a(com.immomo.momo.android.view.dragsort.DragSortListView, boolean):void
     arg types: [com.immomo.momo.android.view.dragsort.DragSortListView, int]
     candidates:
      com.immomo.momo.android.view.dragsort.DragSortListView.a(int, float):void
      com.immomo.momo.android.view.dragsort.DragSortListView.a(int, android.graphics.Canvas):void
      com.immomo.momo.android.view.dragsort.DragSortListView.a(com.immomo.momo.android.view.dragsort.DragSortListView, float):void
      com.immomo.momo.android.view.dragsort.DragSortListView.a(com.immomo.momo.android.view.dragsort.DragSortListView, int):void
      com.immomo.momo.android.view.dragsort.DragSortListView.a(boolean, float):boolean
      com.immomo.momo.android.view.MomoRefreshListView.a(int, int):void
      com.immomo.momo.android.view.RefreshOnOverScrollListView.a(android.view.View, int):void
      com.immomo.momo.android.view.HandyListView.a(com.immomo.momo.android.view.HandyListView, float):void
      com.immomo.momo.android.view.HandyListView.a(com.immomo.momo.android.view.HandyListView, boolean):void
      com.immomo.momo.android.view.cw.a(int, int):void
      com.immomo.momo.android.view.dragsort.DragSortListView.a(com.immomo.momo.android.view.dragsort.DragSortListView, boolean):void */
    public final void run() {
        int i2;
        int firstVisiblePosition = this.i.getFirstVisiblePosition();
        int lastVisiblePosition = this.i.getLastVisiblePosition();
        int count = this.i.getCount();
        int paddingTop = this.i.getPaddingTop();
        int height = (this.i.getHeight() - paddingTop) - this.i.getPaddingBottom();
        int min = Math.min(this.i.S, this.i.j + this.i.D);
        int max = Math.max(this.i.S, this.i.j - this.i.D);
        this.b = SystemClock.uptimeMillis();
        this.d = (float) (this.b - this.f2796a);
        this.c = Math.round(this.g * this.d);
        if (this.c >= 0) {
            this.c = Math.min(height, this.c);
            i2 = firstVisiblePosition;
        } else {
            this.c = Math.max(-height, this.c);
            i2 = lastVisiblePosition;
        }
        if (this.f == 0) {
            View childAt = this.i.getChildAt(0);
            if (childAt == null) {
                this.h = false;
                return;
            } else if (firstVisiblePosition == 0 && childAt.getTop() == paddingTop) {
                this.h = false;
                return;
            } else {
                j s = this.i.Q;
                float t = (this.i.M - ((float) max)) / this.i.N;
                long j = this.f2796a;
                this.g = s.a(t);
            }
        } else {
            View childAt2 = this.i.getChildAt(lastVisiblePosition - firstVisiblePosition);
            if (childAt2 == null) {
                this.h = false;
                return;
            } else if (lastVisiblePosition != count - 1 || childAt2.getBottom() > height + paddingTop) {
                j s2 = this.i.Q;
                float v = (((float) min) - this.i.L) / this.i.O;
                long j2 = this.f2796a;
                this.g = -s2.a(v);
            } else {
                this.h = false;
                return;
            }
        }
        View childAt3 = this.i.getChildAt(i2 - firstVisiblePosition);
        int top = childAt3.getTop() + this.c;
        if (i2 == 0 && top > paddingTop) {
            top = paddingTop;
        }
        this.i.ah = true;
        this.i.setSelectionFromTop(i2, top - paddingTop);
        this.i.layoutChildren();
        this.i.invalidate();
        this.i.ah = false;
        this.i.c(i2, childAt3, false);
        this.f2796a = this.b;
        this.i.post(this);
    }
}
