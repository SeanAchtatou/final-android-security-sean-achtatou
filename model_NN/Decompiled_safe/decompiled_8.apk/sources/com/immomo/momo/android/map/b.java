package com.immomo.momo.android.map;

import android.view.View;
import com.immomo.momo.R;
import com.immomo.momo.util.ao;

final class b implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ AMapActivity f2479a;

    b(AMapActivity aMapActivity) {
        this.f2479a = aMapActivity;
    }

    public final void onClick(View view) {
        if (this.f2479a.g != null) {
            this.f2479a.i.getController().animateTo(this.f2479a.g);
            this.f2479a.i.getController().setZoom(16);
            return;
        }
        ao.e(R.string.map_location_error);
    }
}
