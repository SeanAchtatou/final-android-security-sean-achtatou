package com.immomo.momo.android.activity.tieba;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.protocol.a.v;
import java.util.Collection;
import java.util.Date;
import java.util.List;

final class dc extends d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ TiebaCategoryActivity f2231a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public dc(TiebaCategoryActivity tiebaCategoryActivity, Context context) {
        super(context);
        this.f2231a = tiebaCategoryActivity;
        if (tiebaCategoryActivity.l != null) {
            tiebaCategoryActivity.l.cancel(true);
        }
        tiebaCategoryActivity.l = this;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        List b = v.a().b();
        this.f2231a.k.f(b);
        return b;
    }

    /* access modifiers changed from: protected */
    public final void a() {
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        this.f2231a.g.a("category_latttime_reflush", this.f2231a.i);
        this.f2231a.j.a(false);
        this.f2231a.j.b((Collection) ((List) obj));
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.f2231a.h.n();
        this.f2231a.i = new Date();
        this.f2231a.h.setLastFlushTime(this.f2231a.i);
    }
}
