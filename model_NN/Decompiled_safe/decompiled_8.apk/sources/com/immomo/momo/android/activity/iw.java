package com.immomo.momo.android.activity;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.view.ViewPropertyAnimator;
import com.immomo.momo.g;

final class iw implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ iv f1751a;
    private final /* synthetic */ Bitmap b;

    iw(iv ivVar, Bitmap bitmap) {
        this.f1751a = ivVar;
        this.b = bitmap;
    }

    @SuppressLint({"NewApi"})
    public final void run() {
        if (g.a()) {
            this.f1751a.f1750a.aO.setLayerType(2, null);
            this.f1751a.f1750a.aO.setAlpha(0.0f);
            ViewPropertyAnimator animate = this.f1751a.f1750a.aO.animate();
            animate.alphaBy(1.0f);
            animate.setDuration(600);
            animate.start();
        }
        this.f1751a.f1750a.aO.setImageBitmap(this.b);
    }
}
