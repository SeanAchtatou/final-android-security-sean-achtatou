package com.immomo.momo.android.activity;

import android.graphics.RectF;
import com.immomo.momo.R;
import com.immomo.momo.android.view.draggablegridview.c;
import com.immomo.momo.g;

final class ck implements c {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ EditUserPhotoActivity f1068a;

    ck(EditUserPhotoActivity editUserPhotoActivity) {
        this.f1068a = editUserPhotoActivity;
    }

    public final void a() {
        RectF a2 = g.a(this.f1068a.k);
        RectF a3 = g.a(this.f1068a.h.getDraggedView());
        a3.set(a3.left, a3.top - 10.0f, a3.right, a3.bottom + 50.0f);
        if (RectF.intersects(a3, a2)) {
            this.f1068a.l.setBackgroundResource(R.drawable.bg_multiselect_press);
            this.f1068a.m = true;
            return;
        }
        this.f1068a.l.setBackgroundResource(R.drawable.bg_multiselect);
        this.f1068a.m = false;
    }

    public final void b() {
        this.f1068a.m = false;
    }

    public final void c() {
        if (this.f1068a.m) {
            this.f1068a.l.setBackgroundResource(R.drawable.bg_multiselect);
            int draggedPosition = this.f1068a.h.getDraggedPosition();
            if (draggedPosition >= 0 && draggedPosition < this.f1068a.i.size()) {
                this.f1068a.c(draggedPosition);
            }
        }
    }
}
