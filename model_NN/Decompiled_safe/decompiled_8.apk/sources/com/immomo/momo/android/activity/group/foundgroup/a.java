package com.immomo.momo.android.activity.group.foundgroup;

import android.view.View;

public abstract class a {

    /* renamed from: a  reason: collision with root package name */
    private View f1661a;

    public a(View view) {
        this.f1661a = view;
    }

    public final View a() {
        return this.f1661a;
    }

    /* access modifiers changed from: protected */
    public final View a(int i) {
        return this.f1661a.findViewById(i);
    }

    public void b() {
    }

    /* access modifiers changed from: protected */
    public boolean c() {
        return true;
    }

    /* access modifiers changed from: protected */
    public abstract void d();

    /* access modifiers changed from: protected */
    public abstract void e();
}
