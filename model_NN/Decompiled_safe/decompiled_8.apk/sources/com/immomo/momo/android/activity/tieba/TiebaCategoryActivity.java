package com.immomo.momo.android.activity.tieba;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import com.immomo.momo.R;
import com.immomo.momo.android.a.il;
import com.immomo.momo.android.activity.ah;
import com.immomo.momo.android.view.MomoRefreshListView;
import com.immomo.momo.android.view.bi;
import com.immomo.momo.android.view.bu;
import com.immomo.momo.android.view.cv;
import com.immomo.momo.service.ao;
import com.immomo.momo.service.bean.c.f;
import java.util.Date;

public class TiebaCategoryActivity extends ah implements AdapterView.OnItemClickListener, bu, cv {
    /* access modifiers changed from: private */
    public MomoRefreshListView h = null;
    /* access modifiers changed from: private */
    public Date i = null;
    /* access modifiers changed from: private */
    public il j = null;
    /* access modifiers changed from: private */
    public ao k;
    /* access modifiers changed from: private */
    public dc l;

    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        super.a(bundle);
        setContentView((int) R.layout.activity_tiebacategory);
        this.h = (MomoRefreshListView) findViewById(R.id.listview);
        this.h.setOnItemClickListener(this);
        this.h.setOnCancelListener$135502(this);
        d();
        this.h.setOnPullToRefreshListener$42b903f6(this);
        m().a(new bi(this).a((int) R.drawable.ic_topbar_search), new db(this));
    }

    public final void b_() {
        b(new dc(this, this));
    }

    /* access modifiers changed from: protected */
    public final void d() {
        super.d();
        this.k = new ao();
        this.i = this.g.b("category_latttime_reflush");
        if (this.i != null) {
            this.h.setLastFlushTime(this.i);
        }
        this.j = new il(this, this.k.d(), this.h);
        this.h.setAdapter((ListAdapter) this.j);
        if (this.j.isEmpty()) {
            this.h.l();
        } else {
            b(new dc(this, this));
        }
        setTitle("陌陌吧分类");
    }

    public void onItemClick(AdapterView adapterView, View view, int i2, long j2) {
        f fVar = (f) this.j.getItem(i2);
        Intent intent = new Intent(this, TiebaCategoryDetailActivity.class);
        intent.putExtra(TiebaCategoryDetailActivity.j, fVar.e);
        intent.putExtra(TiebaCategoryDetailActivity.i, fVar.f3021a);
        intent.putExtra(TiebaCategoryDetailActivity.h, fVar.b);
        startActivity(intent);
    }

    public final void v() {
        this.h.n();
    }
}
