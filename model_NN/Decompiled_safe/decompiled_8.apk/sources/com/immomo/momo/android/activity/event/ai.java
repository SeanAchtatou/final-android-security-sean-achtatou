package com.immomo.momo.android.activity.event;

import android.view.View;
import android.widget.AdapterView;

final class ai implements AdapterView.OnItemClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ EventProfileActivity f1353a;
    private final /* synthetic */ String[] b;

    ai(EventProfileActivity eventProfileActivity, String[] strArr) {
        this.f1353a = eventProfileActivity;
        this.b = strArr;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        if ("退出活动".equals(this.b[i])) {
            EventProfileActivity.l(this.f1353a);
        } else if ("分享".equals(this.b[i])) {
            EventProfileActivity.m(this.f1353a);
        }
    }
}
