package com.immomo.momo.android.activity.event;

import android.content.Intent;
import com.immomo.momo.android.activity.emotestore.EmotionProfileActivity;
import com.immomo.momo.android.view.a.al;
import com.immomo.momo.android.view.a.n;
import com.immomo.momo.g;
import com.immomo.momo.plugin.b.a;
import com.immomo.momo.service.bean.ae;

final class q implements al {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ EventFeedProfileActivity f1419a;
    private final /* synthetic */ String[] b;
    private final /* synthetic */ ae c;

    q(EventFeedProfileActivity eventFeedProfileActivity, String[] strArr, ae aeVar) {
        this.f1419a = eventFeedProfileActivity;
        this.b = strArr;
        this.c = aeVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.android.activity.event.EventFeedProfileActivity.a(com.immomo.momo.android.activity.event.EventFeedProfileActivity, java.lang.String, boolean):void
     arg types: [com.immomo.momo.android.activity.event.EventFeedProfileActivity, java.lang.String, int]
     candidates:
      com.immomo.momo.android.activity.ao.a(android.support.v4.app.Fragment, android.content.Intent, int):void
      android.support.v4.app.g.a(java.lang.String, java.io.PrintWriter, android.view.View):void
      android.support.v4.app.g.a(android.support.v4.app.Fragment, android.content.Intent, int):void
      com.immomo.momo.android.activity.event.EventFeedProfileActivity.a(com.immomo.momo.android.activity.event.EventFeedProfileActivity, java.lang.String, boolean):void */
    public final void a(int i) {
        if ("回复".equals(this.b[i])) {
            this.f1419a.a(this.c);
        } else if ("查看表情".equals(this.b[i])) {
            EventFeedProfileActivity eventFeedProfileActivity = this.f1419a;
            EventFeedProfileActivity eventFeedProfileActivity2 = this.f1419a;
            a aVar = new a(EventFeedProfileActivity.c(this.c.f));
            Intent intent = new Intent(this.f1419a, EmotionProfileActivity.class);
            intent.putExtra("eid", aVar.h());
            this.f1419a.startActivity(intent);
        } else if ("复制文本".equals(this.b[i])) {
            g.a((CharSequence) this.c.f);
            this.f1419a.a((CharSequence) "已成功复制文本");
        } else if ("删除".equals(this.b[i])) {
            n.a(this.f1419a, "确定要删除该评论？", new r(this, this.c)).show();
        } else if ("举报".equals(this.b[i])) {
            EventFeedProfileActivity.a(this.f1419a, this.c.k, true);
        }
    }
}
