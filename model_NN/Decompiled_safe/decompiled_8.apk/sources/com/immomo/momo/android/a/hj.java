package com.immomo.momo.android.a;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.b;
import com.immomo.momo.g;
import com.immomo.momo.service.am;
import com.immomo.momo.service.bean.ay;
import com.immomo.momo.service.bean.az;

public final class hj extends a {
    private am d = new am();

    public hj(Context context) {
        super(context);
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        Bitmap bitmap = null;
        int i2 = 8;
        if (view == null) {
            hk hkVar = new hk((byte) 0);
            view = g.o().inflate((int) R.layout.listitem_searchsite, (ViewGroup) null);
            hkVar.f860a = (ImageView) view.findViewById(R.id.iv_icon);
            hkVar.c = (TextView) view.findViewById(R.id.tv_distance);
            hkVar.d = (TextView) view.findViewById(R.id.tv_frequent);
            hkVar.b = (TextView) view.findViewById(R.id.tv_sitename);
            view.setTag(R.id.tag_userlist_item, hkVar);
        }
        hk hkVar2 = (hk) view.getTag(R.id.tag_userlist_item);
        ay ayVar = (ay) getItem(i);
        hkVar2.b.setText(ayVar.f);
        ImageView imageView = hkVar2.f860a;
        az a2 = this.d.a(new StringBuilder(String.valueOf(ayVar.d)).toString());
        if (a2 != null) {
            bitmap = b.a(a2.a());
        }
        imageView.setImageBitmap(bitmap);
        hkVar2.d.setVisibility(ayVar.q ? 0 : 8);
        TextView textView = hkVar2.c;
        if (!ayVar.q) {
            i2 = 0;
        }
        textView.setVisibility(i2);
        hkVar2.c.setText(ayVar.e);
        return view;
    }
}
