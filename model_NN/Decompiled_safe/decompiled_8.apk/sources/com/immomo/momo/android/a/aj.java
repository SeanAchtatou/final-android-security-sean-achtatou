package com.immomo.momo.android.a;

import android.content.Intent;
import android.support.v4.b.a;
import android.view.View;
import com.immomo.momo.android.activity.OtherProfileActivity;
import com.immomo.momo.android.activity.plugin.RenrenActivity;
import com.immomo.momo.android.activity.plugin.SinaWeiboActivity;
import com.immomo.momo.android.activity.plugin.TxWeiboActivity;
import com.immomo.momo.service.bean.i;

final class aj implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ae f717a;
    private final /* synthetic */ i b;

    aj(ae aeVar, i iVar) {
        this.f717a = aeVar;
        this.b = iVar;
    }

    public final void onClick(View view) {
        if (!a.a((CharSequence) this.b.h())) {
            Intent intent = new Intent(this.f717a.e, OtherProfileActivity.class);
            intent.putExtra("momoid", this.b.h());
            this.f717a.e.startActivity(intent);
        } else if (this.b.c() == null) {
        } else {
            if (this.b.b() == 2) {
                Intent intent2 = new Intent(this.f717a.e, SinaWeiboActivity.class);
                intent2.putExtra("uid", this.b.c().f2875a);
                this.f717a.e.startActivity(intent2);
            } else if (this.b.b() == 4) {
                Intent intent3 = new Intent(this.f717a.e, RenrenActivity.class);
                intent3.putExtra("renrenuid", this.b.c().f2875a);
                this.f717a.e.startActivity(intent3);
            } else if (this.b.b() == 3) {
                Intent intent4 = new Intent(this.f717a.e, TxWeiboActivity.class);
                intent4.putExtra("keyType", 2);
                intent4.putExtra("uid", this.b.c().f2875a);
                this.f717a.e.startActivity(intent4);
            }
        }
    }
}
