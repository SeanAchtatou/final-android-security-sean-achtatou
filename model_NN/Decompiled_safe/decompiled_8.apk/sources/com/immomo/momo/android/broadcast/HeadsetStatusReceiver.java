package com.immomo.momo.android.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.immomo.momo.g;

public class HeadsetStatusReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        if (!"android.intent.action.HEADSET_PLUG".equals(intent.getAction())) {
            return;
        }
        if (intent.getIntExtra("state", 0) != 0) {
            g.a(true);
        } else {
            g.a(false);
        }
    }
}
