package com.immomo.momo.android.activity.tieba;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import com.immomo.momo.R;
import com.immomo.momo.android.a.jg;
import com.immomo.momo.android.activity.ah;
import com.immomo.momo.android.view.LoadingButton;
import com.immomo.momo.android.view.MomoRefreshListView;
import com.immomo.momo.android.view.bl;
import com.immomo.momo.android.view.bu;
import com.immomo.momo.android.view.cv;
import com.immomo.momo.service.bean.c.d;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class TiebaVerifyActivity extends ah implements AdapterView.OnItemClickListener, bl, bu, cv {
    /* access modifiers changed from: private */
    public MomoRefreshListView h = null;
    /* access modifiers changed from: private */
    public jg i = null;
    /* access modifiers changed from: private */
    public fi j;
    /* access modifiers changed from: private */
    public fh k;
    /* access modifiers changed from: private */
    public LoadingButton l;
    /* access modifiers changed from: private */
    public Set m = new HashSet();

    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        super.a(bundle);
        setContentView((int) R.layout.activity_tiebacategorydetail);
        this.h = (MomoRefreshListView) findViewById(R.id.listview);
        this.h.setTimeEnable(true);
        this.h.setCompleteScrollTop(false);
        this.h.setEnableLoadMoreFoolter(true);
        this.h.setTimeEnable(false);
        this.l = this.h.getFooterViewButton();
        this.l.setVisibility(8);
        d();
        this.h.setOnPullToRefreshListener$42b903f6(this);
        this.h.setOnCancelListener$135502(this);
        this.l.setOnProcessListener(this);
        this.h.setOnItemClickListener(this);
    }

    public final void b_() {
        b(new fi(this, this));
    }

    /* access modifiers changed from: protected */
    public final void d() {
        super.d();
        ArrayList arrayList = new ArrayList();
        MomoRefreshListView momoRefreshListView = this.h;
        this.i = new jg(this, arrayList);
        this.h.setAdapter((ListAdapter) this.i);
        this.h.l();
        setTitle("创建中的陌陌吧");
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.j != null && !this.j.isCancelled()) {
            this.j.cancel(true);
        }
        if (this.k != null && !this.k.isCancelled()) {
            this.k.cancel(true);
        }
    }

    public void onItemClick(AdapterView adapterView, View view, int i2, long j2) {
        if (i2 <= this.i.getCount()) {
            Intent intent = new Intent(this, TiebaMemberListActivity.class);
            intent.putExtra("tiebaid", ((d) this.i.getItem(i2)).f3019a);
            startActivity(intent);
        }
    }

    public final void u() {
        this.k = new fh(this, this);
        this.k.execute(new Object[0]);
    }

    public final void v() {
        this.h.n();
        if (this.j != null) {
            this.j.cancel(true);
            this.j = null;
        }
    }
}
