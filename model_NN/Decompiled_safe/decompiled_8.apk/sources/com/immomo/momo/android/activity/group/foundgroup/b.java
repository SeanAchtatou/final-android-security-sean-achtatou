package com.immomo.momo.android.activity.group.foundgroup;

import android.content.Intent;
import android.support.v4.b.a;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.immomo.momo.MomoApplication;
import com.immomo.momo.R;
import com.immomo.momo.android.pay.MemberCenterActivity;
import com.immomo.momo.service.bean.a.h;
import com.immomo.momo.util.e;
import com.immomo.momo.util.k;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class b extends a implements View.OnClickListener {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public FoundGroupActivity f1662a;
    private Button b;
    private Button c;
    private TextView d;
    private TextView e;
    private TextView f;
    private TextView g;
    private TextView h;
    private c i;
    private boolean j;
    private MomoApplication k;
    private ArrayList l = new ArrayList();
    private ArrayList m = new ArrayList();
    private ArrayList n = new ArrayList();

    public b(View view, FoundGroupActivity foundGroupActivity) {
        super(view);
        this.f1662a = foundGroupActivity;
        this.d = (TextView) view.findViewById(R.id.surplus_group_tip_tx);
        this.e = (TextView) view.findViewById(R.id.surplus_group_viptip_tx);
        this.f = (TextView) view.findViewById(R.id.surplus_group_info_tx);
        this.g = (TextView) view.findViewById(R.id.surplus_group_vipinfo_tx);
        this.h = (TextView) view.findViewById(R.id.surplus_group_vipyearsinfo_tx);
        this.b = (Button) view.findViewById(R.id.surplus_group_btn);
        this.b.setOnClickListener(this);
        this.c = (Button) view.findViewById(R.id.surplus_vipgroup_btn);
        this.c.setOnClickListener(this);
        this.k = (MomoApplication) foundGroupActivity.getApplication();
        this.j = this.k.a().b();
    }

    private static void a(TextView textView, String str) {
        Matcher matcher = Pattern.compile("[\\d]+").matcher(textView.getText());
        while (matcher.find()) {
            String group = matcher.group();
            e.a(textView, str.indexOf(group), group.length() + str.indexOf(group), R.style.Style_Text_FoundGroup_Tip);
        }
    }

    static /* synthetic */ void a(b bVar, h hVar) {
        bVar.f1662a.a(hVar.n);
        bVar.j = bVar.k.a().b();
        List list = hVar.f2977a;
        List list2 = hVar.b;
        int size = list.size();
        int size2 = list2.size();
        int i2 = size + size2;
        LinearLayout.LayoutParams f2 = f();
        f2.leftMargin = (int) bVar.f1662a.getResources().getDimension(R.dimen.params_left);
        LinearLayout.LayoutParams f3 = f();
        f3.topMargin = (int) bVar.f1662a.getResources().getDimension(R.dimen.params2_top);
        if (!bVar.j) {
            f3.leftMargin = (int) bVar.f1662a.getResources().getDimension(R.dimen.params2_left);
        }
        LinearLayout.LayoutParams f4 = f();
        f4.topMargin = (int) bVar.f1662a.getResources().getDimension(R.dimen.params3_top);
        for (int i3 = 0; i3 < i2; i3++) {
            LinearLayout linearLayout = new LinearLayout(bVar.f1662a);
            linearLayout.setOrientation(0);
            linearLayout.setLayoutParams(f4);
            TextView textView = new TextView(bVar.f1662a);
            textView.setTextColor(bVar.f1662a.getResources().getColor(R.color.font_value));
            ImageView imageView = new ImageView(bVar.f1662a);
            textView.setTextSize(15.0f);
            textView.setLayoutParams(f3);
            f2.gravity = 16;
            imageView.setLayoutParams(f2);
            textView.setGravity(16);
            if (i3 == 0) {
                LinearLayout.LayoutParams f5 = f();
                f5.topMargin = (int) bVar.f1662a.getResources().getDimension(R.dimen.params1_top);
                linearLayout.setLayoutParams(f5);
            }
            bVar.l.add(textView);
            bVar.m.add(imageView);
            if (!bVar.j) {
                linearLayout.addView(imageView);
            }
            linearLayout.addView(textView);
            bVar.n.add(linearLayout);
        }
        if (a.a((CharSequence) hVar.d)) {
            bVar.d.setVisibility(8);
        } else {
            bVar.d.setText(hVar.d);
            bVar.d.setVisibility(0);
        }
        bVar.e.setVisibility(0);
        if (!a.a((CharSequence) hVar.e)) {
            bVar.e.setText(hVar.e);
        }
        bVar.f.setText(hVar.i);
        a(bVar.f, hVar.i);
        bVar.g.setText("会员" + hVar.j);
        a(bVar.g, "会员" + hVar.j);
        String str = "会员" + hVar.j;
        e.a(bVar.g, str.indexOf("会员"), str.indexOf("会员") + "会员".length(), R.style.Style_Text_blue);
        bVar.h.setText("年费会员" + hVar.k);
        a(bVar.h, "年费会员" + hVar.k);
        String str2 = "年费会员" + hVar.k;
        e.a(bVar.h, str2.indexOf("年费会员"), str2.indexOf("年费会员") + "年费会员".length(), R.style.Style_Text_FoundGroup_Tip);
        bVar.f1662a.h.setText(hVar.c ? R.string.creategroup_enable : R.string.creategroup_disable);
        bVar.f1662a.h.setEnabled(hVar.c);
        bVar.b.setText(hVar.c ? R.string.creategroup_enable : R.string.creategroup_disable);
        bVar.b.setEnabled(hVar.c);
        int i4 = 0;
        if (size != 0) {
            int i5 = 0;
            while (i5 < size) {
                ((TextView) bVar.l.get(i5)).setText((CharSequence) list.get(i5));
                ((ImageView) bVar.m.get(i5)).setBackgroundResource(R.drawable.ic_group_create_right);
                i5++;
            }
            i4 = i5;
        }
        if (size2 != 0) {
            int i6 = 0;
            int i7 = i4;
            while (i6 < size2) {
                ((TextView) bVar.l.get(i7)).setText((CharSequence) list2.get(i6));
                ((ImageView) bVar.m.get(i7)).setBackgroundResource(R.drawable.ic_group_create_wrong);
                i6++;
                i7++;
            }
        }
        LinearLayout linearLayout2 = (LinearLayout) bVar.a((int) R.id.group_permission_info);
        for (int i8 = 0; i8 < i2; i8++) {
            linearLayout2.addView((View) bVar.n.get(i8), i8 + 1, f4);
        }
        if (bVar.j) {
            bVar.b.setVisibility(8);
            if (hVar.c) {
                bVar.c.setText((int) R.string.creategroup_createvip);
                bVar.c.setEnabled(true);
                return;
            }
            bVar.c.setText((int) R.string.creategroup_full);
            bVar.c.setEnabled(false);
            return;
        }
        bVar.b.setVisibility(0);
        bVar.c.setText((int) R.string.creategroup_getvip);
        if (hVar.m >= hVar.l) {
            bVar.b.setEnabled(false);
            if (!hVar.c) {
                bVar.b.setText((int) R.string.creategroup_disable);
            } else {
                bVar.b.setText((int) R.string.creategroup_full);
            }
        } else if (!hVar.c) {
            bVar.b.setText((int) R.string.creategroup_disable);
        } else {
            bVar.b.setText((int) R.string.creategroup_create);
            bVar.b.setEnabled(true);
        }
    }

    private static LinearLayout.LayoutParams f() {
        return new LinearLayout.LayoutParams(-2, -2);
    }

    public final void a(c cVar) {
        this.i = cVar;
    }

    public final void b() {
        if (((LinearLayout) ((LinearLayout) a()).findViewById(R.id.group_permission_info)).getChildCount() == 3) {
            new d(this, this.f1662a).execute(new Object[0]);
        }
    }

    /* access modifiers changed from: protected */
    public final void d() {
    }

    /* access modifiers changed from: protected */
    public final void e() {
    }

    public final void onClick(View view) {
        if (view.equals(this.b)) {
            if (this.i != null) {
                this.i.a();
            }
        } else if (!view.equals(this.c)) {
        } else {
            if (!this.j) {
                new k("C6", "C64101").e();
                this.f1662a.startActivity(new Intent(this.f1662a, MemberCenterActivity.class));
            } else if (this.i != null) {
                this.i.b();
            }
        }
    }
}
