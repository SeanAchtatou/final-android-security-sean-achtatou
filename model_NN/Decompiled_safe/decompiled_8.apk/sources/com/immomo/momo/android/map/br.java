package com.immomo.momo.android.map;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import com.google.android.maps.MapView;
import com.google.android.maps.MyLocationOverlay;
import com.immomo.momo.R;

final class br extends MyLocationOverlay {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ SelectSiteGoogleActivity f2497a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public br(SelectSiteGoogleActivity selectSiteGoogleActivity, Context context, MapView mapView) {
        super(context, mapView);
        this.f2497a = selectSiteGoogleActivity;
    }

    public final boolean draw(Canvas canvas, MapView mapView, boolean z, long j) {
        br.super.draw(canvas, mapView, z, j);
        if (this.f2497a.g == null) {
            return false;
        }
        Point pixels = mapView.getProjection().toPixels(this.f2497a.g, (Point) null);
        if (this.f2497a.h == null || this.f2497a.h.isRecycled()) {
            this.f2497a.h = BitmapFactory.decodeResource(this.f2497a.getResources(), R.drawable.ic_map_pin);
        }
        canvas.drawBitmap(this.f2497a.h, (float) (pixels.x - (this.f2497a.h.getWidth() / 2)), (float) (pixels.y - (this.f2497a.h.getHeight() / 2)), (Paint) null);
        return true;
    }
}
