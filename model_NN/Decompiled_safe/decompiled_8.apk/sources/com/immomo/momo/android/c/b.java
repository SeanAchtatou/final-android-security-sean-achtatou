package com.immomo.momo.android.c;

import android.content.Context;
import com.immomo.momo.android.view.a.v;

public abstract class b extends d {

    /* renamed from: a  reason: collision with root package name */
    protected v f2379a = null;

    public b(Context context) {
        super(context);
    }

    protected static boolean d() {
        return true;
    }

    /* access modifiers changed from: protected */
    public void a() {
        this.f2379a = new v(f(), c());
        this.f2379a.setOnCancelListener(new c(this));
        this.f2379a.show();
    }

    /* access modifiers changed from: protected */
    public void b() {
        this.f2379a.dismiss();
        this.f2379a = null;
    }

    /* access modifiers changed from: protected */
    public String c() {
        return "正在提交，请稍候...";
    }
}
