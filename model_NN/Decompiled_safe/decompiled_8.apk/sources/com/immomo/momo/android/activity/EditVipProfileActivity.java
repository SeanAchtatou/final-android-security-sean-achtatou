package com.immomo.momo.android.activity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.b.a;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.broadcast.d;
import com.immomo.momo.android.broadcast.w;
import com.immomo.momo.android.view.EmoteEditeText;
import com.immomo.momo.android.view.HeaderLayout;
import com.immomo.momo.android.view.ProfilePullScrollView;
import com.immomo.momo.android.view.UserPhotosView;
import com.immomo.momo.android.view.a.n;
import com.immomo.momo.android.view.a.o;
import com.immomo.momo.android.view.bi;
import com.immomo.momo.b;
import com.immomo.momo.g;
import com.immomo.momo.service.aq;
import com.immomo.momo.service.bean.aj;
import com.immomo.momo.service.bean.al;
import com.immomo.momo.service.bean.am;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.util.ao;
import com.immomo.momo.util.e;
import com.immomo.momo.util.h;
import com.immomo.momo.util.j;
import com.immomo.momo.util.k;
import java.io.File;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import mm.purchasesdk.PurchaseCode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class EditVipProfileActivity extends ah {
    private UserPhotosView A;
    private ImageView B;
    private ProfilePullScrollView C = null;
    /* access modifiers changed from: private */
    public View D;
    private View E;
    private TextView F;
    private TextView G;
    private ImageView H;
    /* access modifiers changed from: private */
    public HashMap I = new HashMap();
    /* access modifiers changed from: private */
    public HashMap J = new HashMap();
    /* access modifiers changed from: private */
    public boolean K = false;
    /* access modifiers changed from: private */
    public Date L = null;
    /* access modifiers changed from: private */
    public Date M = null;
    /* access modifiers changed from: private */
    public int N = 0;
    private File O = null;
    private boolean P = false;
    private w Q;
    /* access modifiers changed from: private */
    public int R = 0;
    /* access modifiers changed from: private */
    public int S = 0;
    private d T = new dh(this);
    private View.OnClickListener U = new dk(this);
    File h = null;
    File i;
    /* access modifiers changed from: private */
    public bf j;
    private String k;
    private bi l;
    private HeaderLayout m = null;
    /* access modifiers changed from: private */
    public aq n = null;
    private View o;
    private View p;
    private View q;
    private TextView r;
    /* access modifiers changed from: private */
    public TextView s;
    /* access modifiers changed from: private */
    public TextView t;
    private EmoteEditeText u;
    private EmoteEditeText v;
    private EmoteEditeText w;
    private EmoteEditeText x;
    private EmoteEditeText y;
    private EmoteEditeText z;

    public EditVipProfileActivity() {
        new dm();
        this.i = null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.b.a(java.lang.String, boolean):android.graphics.Bitmap
     arg types: [java.lang.String, int]
     candidates:
      com.immomo.momo.b.a(java.lang.String, android.graphics.Bitmap):android.graphics.Bitmap
      com.immomo.momo.b.a(java.lang.String, boolean):android.graphics.Bitmap */
    private void a(String str, String str2) {
        if (str != null) {
            this.E.setVisibility(0);
            this.F.setText(str);
        }
        if (!a.a((CharSequence) str2)) {
            am c = b.c(str2);
            if (c != null) {
                this.G.setText(c.b);
                if (a.a((CharSequence) c.d)) {
                    this.H.setImageBitmap(null);
                    this.H.setVisibility(8);
                    return;
                }
                this.H.setVisibility(0);
                this.H.setImageBitmap(b.a(c.f2991a, false));
                return;
            }
            this.G.setText(str);
            this.H.setVisibility(8);
            return;
        }
        this.G.setText("请选择你的行业");
        this.H.setVisibility(8);
    }

    private void a(JSONArray jSONArray) {
        if (this.j.ae != null && this.j.ae.length > 0) {
            int i2 = 0;
            while (i2 < this.j.ae.length) {
                JSONObject jSONObject = new JSONObject();
                try {
                    jSONObject.put("upload", "YES");
                    jSONObject.put("guid", this.j.ae[i2]);
                    jSONArray.put(jSONObject);
                    i2++;
                } catch (JSONException e) {
                    this.e.a((Throwable) e);
                    return;
                }
            }
        }
    }

    static /* synthetic */ void d(EditVipProfileActivity editVipProfileActivity) {
        o oVar = new o(editVipProfileActivity, (int) R.array.editprofile_add_photo);
        oVar.setTitle((int) R.string.dialog_title_change_bg);
        oVar.a(new dj(editVipProfileActivity));
        oVar.show();
    }

    static /* synthetic */ void f(EditVipProfileActivity editVipProfileActivity) {
        String charSequence = editVipProfileActivity.t.getText().toString();
        if (a.a((CharSequence) charSequence)) {
            charSequence = "1980-1-1";
        }
        String[] split = charSequence.split("-");
        String[] split2 = split.length < 3 ? "1980-1-1".split("-") : split;
        new DatePickerDialog(editVipProfileActivity, new dq(editVipProfileActivity), Integer.parseInt(split2[0]), Integer.parseInt(split2[1]) - 1, Integer.parseInt(split2[2])).show();
    }

    static /* synthetic */ void g(EditVipProfileActivity editVipProfileActivity) {
        Intent intent = new Intent(editVipProfileActivity.getApplicationContext(), EditIndustryActivity.class);
        intent.putExtra("job_name", editVipProfileActivity.F.getText().toString());
        intent.putExtra("industry_id", editVipProfileActivity.k);
        editVipProfileActivity.startActivityForResult(intent, 18);
    }

    static /* synthetic */ void l(EditVipProfileActivity editVipProfileActivity) {
        new k("C", "C6401").e();
        JSONArray jSONArray = new JSONArray();
        editVipProfileActivity.a(jSONArray);
        editVipProfileActivity.I.put("photos", jSONArray.toString());
        editVipProfileActivity.I.put("momoid", editVipProfileActivity.j.h);
        editVipProfileActivity.e.a((Object) ("photos = " + jSONArray.toString()));
        editVipProfileActivity.a(new dt(editVipProfileActivity, editVipProfileActivity)).execute(new Object[0]);
    }

    /* access modifiers changed from: private */
    public void u() {
        if (!this.v.getText().toString().equals(this.j.l())) {
            this.K = true;
            this.I.put("sign", this.v.getText().toString().trim());
        }
        if (!this.u.getText().toString().equals(this.j.L)) {
            this.K = true;
            this.I.put("interest", this.u.getText().toString().trim());
        }
        if (!this.w.getText().toString().equals(this.j.C)) {
            this.K = true;
            this.I.put("company", this.w.getText().toString().trim());
        }
        if (!this.x.getText().toString().equals(this.j.Q)) {
            this.K = true;
            this.I.put("school", this.x.getText().toString().trim());
        }
        if (!this.y.getText().toString().equals(this.j.F)) {
            this.K = true;
            this.I.put("hangout", this.y.getText().toString().trim());
        }
        if (!this.z.getText().toString().equals(this.j.D)) {
            this.K = true;
            this.I.put("website", this.z.getText().toString().trim());
        }
        if (!this.s.getText().toString().equals(this.j.i)) {
            this.K = true;
            this.I.put("name", this.s.getText().toString().trim());
        }
        if (!this.t.getText().toString().equals(this.j.J)) {
            this.K = true;
            this.I.put("birthday", this.t.getText().toString().trim());
        }
        if (!this.F.getText().toString().equals(this.j.N)) {
            this.K = true;
            this.I.put("job", this.F.getText().toString().trim());
        }
        if (this.k != this.j.M && !new StringBuilder(String.valueOf(this.k)).toString().equals(this.j.M)) {
            this.K = true;
            if (this.k == null) {
                this.I.put("industry", PoiTypeDef.All);
            } else {
                this.I.put("industry", this.k);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, int, boolean, boolean):void
     arg types: [com.immomo.momo.service.bean.al, android.widget.ImageView, int, int, int]
     candidates:
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup, int, boolean):void
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, int, boolean, boolean):void */
    /* access modifiers changed from: private */
    public void v() {
        this.A.a(this.j.ae, true, true);
        int J2 = g.J();
        int i2 = (J2 * 7) / 8;
        ViewGroup.LayoutParams layoutParams = this.o.getLayoutParams();
        layoutParams.width = J2;
        layoutParams.height = i2;
        this.o.setLayoutParams(layoutParams);
        ViewGroup.LayoutParams layoutParams2 = this.D.getLayoutParams();
        this.N = Math.round(((float) i2) * 0.2f);
        layoutParams2.width = J2;
        layoutParams2.height = i2;
        this.D.setLayoutParams(layoutParams2);
        if (!this.P) {
            this.D.scrollTo(0, this.N);
            this.P = true;
        }
        int photoHeight = this.A.getPhotoHeight();
        if (photoHeight < this.N) {
            photoHeight = this.N;
        }
        this.C.setMaxScroll(photoHeight);
        j.a((aj) new al(this.j.af), this.B, 2, true, false);
    }

    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        super.a(bundle);
        setContentView((int) R.layout.activity_edit_vipprofile);
        this.m = (HeaderLayout) findViewById(R.id.layout_header);
        this.l = new bi(this);
        this.l.a((int) R.drawable.ic_topbar_confirm_white);
        this.l.setBackgroundResource(R.drawable.bg_header_submit);
        this.l.setMarginRight(8);
        this.m.setTitleText("编辑会员资料");
        this.A = (UserPhotosView) findViewById(R.id.vip_photoview);
        this.B = (ImageView) findViewById(R.id.vip_iv_avatar_bg);
        this.o = findViewById(R.id.vip_avatar_container);
        this.p = findViewById(R.id.layout_name);
        this.q = findViewById(R.id.layout_birthday);
        this.s = (TextView) findViewById(R.id.profile_tv_name);
        this.t = (TextView) findViewById(R.id.profile_tv_birthday);
        this.u = (EmoteEditeText) findViewById(R.id.profile_tv_interest);
        this.v = (EmoteEditeText) findViewById(R.id.profile_tv_sign);
        this.w = (EmoteEditeText) findViewById(R.id.profile_tv_company);
        this.x = (EmoteEditeText) findViewById(R.id.profile_tv_school);
        this.y = (EmoteEditeText) findViewById(R.id.profile_tv_hangout);
        this.z = (EmoteEditeText) findViewById(R.id.profile_tv_web);
        this.F = (TextView) findViewById(R.id.tv_industry_job);
        this.G = (TextView) findViewById(R.id.tv_industry);
        this.r = (TextView) findViewById(R.id.tv_editavatar_tip);
        this.H = (ImageView) findViewById(R.id.icon_industry);
        this.E = findViewById(R.id.layout_industry);
        this.C = (ProfilePullScrollView) findViewById(R.id.scrollview_content);
        this.D = findViewById(R.id.vip_iv_avatar_bglayout);
        d();
        this.m.a(this.l, new dn(this));
        Cdo doVar = new Cdo(this);
        doVar.a(true);
        this.A.setAvatarClickListener(doVar);
        this.o.setOnClickListener(this.U);
        EmoteEditeText emoteEditeText = this.u;
        EmoteEditeText emoteEditeText2 = this.u;
        emoteEditeText.addTextChangedListener(new com.immomo.momo.util.aq(PurchaseCode.QUERY_NO_APP));
        EmoteEditeText emoteEditeText3 = this.w;
        EmoteEditeText emoteEditeText4 = this.w;
        emoteEditeText3.addTextChangedListener(new com.immomo.momo.util.aq(PurchaseCode.QUERY_NO_APP));
        EmoteEditeText emoteEditeText5 = this.x;
        EmoteEditeText emoteEditeText6 = this.x;
        emoteEditeText5.addTextChangedListener(new com.immomo.momo.util.aq(PurchaseCode.QUERY_NO_APP));
        EmoteEditeText emoteEditeText7 = this.y;
        EmoteEditeText emoteEditeText8 = this.y;
        emoteEditeText7.addTextChangedListener(new com.immomo.momo.util.aq(PurchaseCode.QUERY_NO_APP));
        EmoteEditeText emoteEditeText9 = this.z;
        EmoteEditeText emoteEditeText10 = this.z;
        emoteEditeText9.addTextChangedListener(new com.immomo.momo.util.aq(PurchaseCode.QUERY_NO_APP));
        EmoteEditeText emoteEditeText11 = this.v;
        EmoteEditeText emoteEditeText12 = this.v;
        emoteEditeText11.addTextChangedListener(new com.immomo.momo.util.aq(PurchaseCode.QUERY_NO_APP));
        this.p.setOnClickListener(this.U);
        this.q.setOnClickListener(this.U);
        this.E.setOnClickListener(this.U);
        findViewById(R.id.vip_iv_avatar_bg).setOnClickListener(this.U);
        this.Q = new w(this);
        this.Q.a(this.T);
        this.C.setOnPullScrollChangedListener(new dp(this));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.b.a(java.lang.String, boolean):android.graphics.Bitmap
     arg types: [java.lang.String, int]
     candidates:
      com.immomo.momo.b.a(java.lang.String, android.graphics.Bitmap):android.graphics.Bitmap
      com.immomo.momo.b.a(java.lang.String, boolean):android.graphics.Bitmap */
    /* access modifiers changed from: protected */
    public final void d() {
        Calendar instance = Calendar.getInstance();
        Calendar instance2 = Calendar.getInstance();
        instance2.set(1, instance.get(1) - 12);
        this.M = instance2.getTime();
        instance2.set(1, instance.get(1) - 100);
        this.L = instance2.getTime();
        this.n = new aq();
        this.j = this.f;
        if (this.j == null) {
            a((CharSequence) "当前用户资料不存在");
            finish();
        }
        this.k = this.j.M;
        this.r.setVisibility(0);
        this.s.setText(this.j.i);
        this.t.setText(this.j.J);
        this.u.setText(this.j.L);
        this.v.setText(this.j.l());
        this.w.setText(this.j.C);
        this.x.setText(this.j.Q);
        this.y.setText(this.j.F);
        this.z.setText(this.j.D);
        if (!a.a((CharSequence) this.j.N)) {
            this.E.setVisibility(0);
            e.a(this.F, this.j.N, this);
        }
        if (!a.a((CharSequence) this.j.M)) {
            am c = b.c(this.j.M);
            if (c != null) {
                this.G.setText(c.b);
                if (a.a((CharSequence) c.d)) {
                    this.H.setImageBitmap(null);
                    this.H.setVisibility(8);
                } else {
                    this.H.setVisibility(0);
                    this.H.setImageBitmap(b.a(c.f2991a, false));
                    this.e.a((Object) "Userprofile load image thread called");
                }
            } else {
                this.G.setText(this.j.N);
                this.H.setVisibility(8);
            }
        } else {
            this.G.setText("请选择你的行业");
            this.H.setVisibility(8);
        }
        v();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.h.a(java.lang.String, android.graphics.Bitmap, int, boolean):java.io.File
     arg types: [java.lang.String, android.graphics.Bitmap, int, int]
     candidates:
      com.immomo.momo.util.h.a(java.lang.String, java.lang.String, int, boolean):void
      com.immomo.momo.util.h.a(java.lang.String, android.graphics.Bitmap, int, boolean):java.io.File */
    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        switch (i2) {
            case 11:
                if (i3 == -1 && intent != null) {
                    Uri data = intent.getData();
                    Intent intent2 = new Intent(getApplicationContext(), ImageFactoryActivity.class);
                    intent2.setData(data);
                    intent2.putExtra("aspectY", 1);
                    intent2.putExtra("aspectX", 1);
                    intent2.putExtra("minsize", 320);
                    this.O = new File(com.immomo.momo.a.g(), String.valueOf(com.immomo.a.a.f.b.a()) + ".jpg_");
                    intent2.putExtra("outputFilePath", this.O.getAbsolutePath());
                    startActivityForResult(intent2, 13);
                    return;
                }
                return;
            case 12:
                if (i3 == -1 && this.h != null && this.h.exists()) {
                    Uri fromFile = Uri.fromFile(this.h);
                    this.O = new File(com.immomo.momo.a.g(), this.h.getName());
                    Intent intent3 = new Intent(getApplicationContext(), ImageFactoryActivity.class);
                    intent3.setData(fromFile);
                    intent3.putExtra("aspectY", 1);
                    intent3.putExtra("aspectX", 1);
                    intent3.putExtra("minsize", 320);
                    intent3.putExtra("outputFilePath", this.O.getAbsolutePath());
                    startActivityForResult(intent3, 13);
                    return;
                }
                return;
            case 13:
                if (i3 == -1 && intent != null) {
                    if (this.h != null) {
                        if (this.h.exists()) {
                            this.h.delete();
                        }
                        this.h = null;
                    }
                    if (this.O != null) {
                        String a2 = com.immomo.a.a.f.b.a();
                        Bitmap l2 = a.l(this.O.getPath());
                        if (l2 != null) {
                            this.i = h.a(a2, l2, 2, true);
                            this.e.a((Object) ("save file=" + this.i));
                            this.B.setImageBitmap(l2);
                            this.K = true;
                        } else {
                            this.i = null;
                            ao.b("发生未知错误，图片添加失败");
                        }
                        this.O = null;
                        return;
                    }
                    return;
                } else if (i3 == 1003) {
                    ao.d(R.string.cropimage_error_bg_size);
                    return;
                } else if (i3 == 1000) {
                    ao.d(R.string.cropimage_error_other);
                    return;
                } else if (i3 == 1002) {
                    ao.d(R.string.cropimage_error_store);
                    return;
                } else if (i3 == 1001) {
                    ao.d(R.string.cropimage_error_filenotfound);
                    return;
                } else {
                    return;
                }
            case com.immomo.momo.h.DragSortListView_drag_handle_id /*14*/:
            case 15:
            case 16:
            case 17:
            default:
                return;
            case 18:
                if (i3 == -1 && intent != null) {
                    String stringExtra = intent.getStringExtra("job_name");
                    String stringExtra2 = intent.getStringExtra("industry_id");
                    this.k = stringExtra2;
                    a(stringExtra, stringExtra2);
                    return;
                }
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.Q != null) {
            unregisterReceiver(this.Q);
            this.Q = null;
        }
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 == 4) {
            u();
            if (this.K) {
                n nVar = new n(this);
                nVar.a();
                nVar.setTitle((int) R.string.dialog_exit_editprofile_title);
                nVar.a((int) R.string.dialog_exit_editprofile_msg);
                nVar.a(0, "保存", new dr(this));
                nVar.a(1, "不保存", new ds(this));
                nVar.a(2, "取消", new di());
                nVar.show();
                return true;
            }
        }
        return super.onKeyDown(i2, keyEvent);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        new k("PO", "P913").e();
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Bundle bundle) {
        super.onRestoreInstanceState(bundle);
        this.e.a((Object) "---------------onRestoreInstanceState");
        if (bundle.containsKey("camera_filename")) {
            this.h = new File(bundle.getString("camera_filename"));
        }
        if (bundle.containsKey("avatorFile")) {
            this.O = new File(bundle.getString("avatorFile"));
        }
        if (bundle.containsKey("sign")) {
            this.v.setText(bundle.getString("sign"));
        }
        if (bundle.containsKey("interest")) {
            this.u.setText(bundle.getString("interest"));
        }
        if (bundle.containsKey("company")) {
            this.w.setText(bundle.getString("company"));
        }
        if (bundle.containsKey("school")) {
            this.x.setText(bundle.getString("school"));
        }
        if (bundle.containsKey("hangout")) {
            this.y.setText(bundle.getString("hangout"));
        }
        if (bundle.containsKey("website")) {
            this.z.setText(bundle.getString("website"));
        }
        if (bundle.containsKey("name")) {
            this.s.setText(bundle.getString("name"));
        }
        if (bundle.containsKey("birthday")) {
            this.t.setText(bundle.getString("birthday"));
        }
        if (bundle.containsKey("industry") && bundle.containsKey("job")) {
            a(bundle.getString("job"), bundle.getString("industry"));
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        new k("PI", "P913").e();
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        if (this.h != null) {
            bundle.putString("camera_filename", this.h.getPath());
        }
        if (this.O != null) {
            bundle.putString("avatorFile", this.O.getPath());
        }
        if (this.v.getText().toString().length() > 0) {
            bundle.putString("sign", this.v.getText().toString());
        }
        if (this.u.getText().toString().length() > 0) {
            bundle.putString("interest", this.u.getText().toString().trim());
        }
        if (this.w.getText().toString().length() > 0) {
            bundle.putString("company", this.w.getText().toString().trim());
        }
        if (this.x.getText().toString().length() > 0) {
            bundle.putString("school", this.x.getText().toString().trim());
        }
        if (this.y.getText().toString().length() > 0) {
            bundle.putString("hangout", this.y.getText().toString().trim());
        }
        if (this.z.getText().toString().length() > 0) {
            bundle.putString("website", this.z.getText().toString().trim());
        }
        if (this.s.getText().toString().length() > 0) {
            bundle.putString("name", this.s.getText().toString().trim());
        }
        if (this.t.getText().toString().length() > 0) {
            bundle.putString("birthday", this.t.getText().toString().trim());
        }
        if (this.k != null) {
            bundle.putString("industry", this.k);
            bundle.putString("job", this.F.getText().toString());
        }
    }
}
