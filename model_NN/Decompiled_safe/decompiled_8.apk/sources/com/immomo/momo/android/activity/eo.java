package com.immomo.momo.android.activity;

import android.content.DialogInterface;
import java.util.List;

final class eo implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ en f1339a;
    private final /* synthetic */ List b;

    eo(en enVar, List list) {
        this.f1339a = enVar;
        this.b = list;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f1339a.f1338a.h.a().removeAll(this.b);
        this.f1339a.f1338a.h.notifyDataSetChanged();
        this.f1339a.f1338a.l.a(this.b);
        this.f1339a.f1338a.k.e();
        this.f1339a.f1338a.c(this.f1339a.f1338a.l.c());
    }
}
