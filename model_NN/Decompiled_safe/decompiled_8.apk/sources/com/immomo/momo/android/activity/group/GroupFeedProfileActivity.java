package com.immomo.momo.android.activity.group;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.b.a;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.a.eg;
import com.immomo.momo.android.activity.ImageBrowserActivity;
import com.immomo.momo.android.activity.OtherProfileActivity;
import com.immomo.momo.android.activity.ah;
import com.immomo.momo.android.activity.emotestore.EmotionProfileActivity;
import com.immomo.momo.android.broadcast.h;
import com.immomo.momo.android.c.i;
import com.immomo.momo.android.view.EmoteInputView;
import com.immomo.momo.android.view.EmoteTextView;
import com.immomo.momo.android.view.HandyListView;
import com.immomo.momo.android.view.HeaderLayout;
import com.immomo.momo.android.view.LoadingButton;
import com.immomo.momo.android.view.MGifImageView;
import com.immomo.momo.android.view.MSmallEmoteEditeText;
import com.immomo.momo.android.view.MultiImageView;
import com.immomo.momo.android.view.ResizeListenerLayout;
import com.immomo.momo.android.view.a.at;
import com.immomo.momo.android.view.a.o;
import com.immomo.momo.android.view.au;
import com.immomo.momo.android.view.bi;
import com.immomo.momo.android.view.bl;
import com.immomo.momo.android.view.cb;
import com.immomo.momo.b;
import com.immomo.momo.g;
import com.immomo.momo.service.aq;
import com.immomo.momo.service.bean.a.e;
import com.immomo.momo.service.bean.a.f;
import com.immomo.momo.service.bean.aj;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.service.bean.q;
import com.immomo.momo.service.v;
import com.immomo.momo.util.j;
import com.immomo.momo.util.k;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GroupFeedProfileActivity extends ah implements View.OnClickListener, View.OnTouchListener, AdapterView.OnItemClickListener, bl, cb {
    private ImageView A;
    private MultiImageView B;
    /* access modifiers changed from: private */
    public MGifImageView C;
    private ImageView D;
    private TextView E;
    private TextView F;
    /* access modifiers changed from: private */
    public View G;
    private View H;
    private View I;
    private ImageView J;
    private ImageView K;
    private ImageView L;
    private ImageView M;
    private ImageView N;
    private ImageView O;
    private ImageView P;
    private ImageView Q;
    private View R;
    private TextView S;
    private View T;
    private View U;
    /* access modifiers changed from: private */
    public MSmallEmoteEditeText V;
    private ImageView W;
    /* access modifiers changed from: private */
    public TextView X;
    private View Y;
    private Button Z;
    private ImageView aa;
    /* access modifiers changed from: private */
    public EmoteInputView ab = null;
    private Animation ac = null;
    /* access modifiers changed from: private */
    public au ad;
    /* access modifiers changed from: private */
    public bl ae;
    /* access modifiers changed from: private */
    public boolean af = true;
    /* access modifiers changed from: private */
    public f ag;
    boolean h = false;
    private ResizeListenerLayout i = null;
    /* access modifiers changed from: private */
    public String j = PoiTypeDef.All;
    /* access modifiers changed from: private */
    public e k = null;
    /* access modifiers changed from: private */
    public eg l = null;
    /* access modifiers changed from: private */
    public v m;
    /* access modifiers changed from: private */
    public InputMethodManager n = null;
    /* access modifiers changed from: private */
    public ArrayList o;
    private Handler p = new Handler();
    private HeaderLayout q = null;
    private bi r = null;
    /* access modifiers changed from: private */
    public HandyListView s = null;
    /* access modifiers changed from: private */
    public LoadingButton t = null;
    /* access modifiers changed from: private */
    public View u = null;
    private View v;
    private TextView w;
    private TextView x;
    private TextView y;
    private EmoteTextView z;

    private void A() {
        this.af = true;
        this.X.setVisibility(8);
        z();
    }

    /* access modifiers changed from: private */
    public void a(int i2, String str) {
        String str2;
        if (this.k != null) {
            if (i2 != 1) {
                str2 = this.V.getText().toString().trim();
                if (a.a((CharSequence) str2)) {
                    a((CharSequence) "请输入评论内容");
                    return;
                }
            } else {
                str2 = str;
            }
            if (this.af) {
                String str3 = this.j;
                String str4 = this.j;
                String str5 = this.k.c;
                e eVar = this.k;
                b(new bi(this, this, str3, str4, 1, i2, str2, str5, eVar.b != null ? eVar.b.h() : eVar.c));
            } else if (this.ag != null) {
                String str6 = "回复" + this.ag.f2975a.i + ":" + str2;
                String str7 = this.j;
                String str8 = this.ag.i;
                String str9 = this.ag.b;
                f fVar = this.ag;
                b(new bi(this, this, str7, str8, 2, i2, str6, str9, fVar.f2975a != null ? fVar.f2975a.h() : fVar.b));
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public static void a(Context context, String str) {
        Intent intent = new Intent(context, GroupFeedProfileActivity.class);
        intent.putExtra("key_groupfeedid", str);
        intent.putExtra("key_show_inputmethod", false);
        context.startActivity(intent);
    }

    static /* synthetic */ void a(GroupFeedProfileActivity groupFeedProfileActivity, int i2) {
        if (groupFeedProfileActivity.k != null) {
            groupFeedProfileActivity.k.e = i2;
            groupFeedProfileActivity.F.setText(new StringBuilder(String.valueOf(i2)).toString());
            if (groupFeedProfileActivity.k.e > 0 || !groupFeedProfileActivity.l.isEmpty()) {
                groupFeedProfileActivity.Y.setVisibility(8);
                return;
            }
            groupFeedProfileActivity.W.clearAnimation();
            groupFeedProfileActivity.W.setVisibility(8);
            groupFeedProfileActivity.x.setText("暂无评论");
            groupFeedProfileActivity.x.setVisibility(0);
            groupFeedProfileActivity.Y.setVisibility(0);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.b.a.a(java.util.Date, boolean):java.lang.String
     arg types: [java.util.Date, int]
     candidates:
      android.support.v4.b.a.a(android.graphics.Bitmap, float):android.graphics.Bitmap
      android.support.v4.b.a.a(android.graphics.Bitmap, java.io.File):android.graphics.Bitmap
      android.support.v4.b.a.a(int, int):java.lang.String
      android.support.v4.b.a.a(java.util.Collection, java.lang.String):java.lang.String
      android.support.v4.b.a.a(java.lang.Object[], java.lang.String):java.lang.String
      android.support.v4.b.a.a(java.lang.String, java.lang.String):org.json.JSONObject
      android.support.v4.b.a.a(float, int[]):void
      android.support.v4.b.a.a(android.app.Activity, int):void
      android.support.v4.b.a.a(android.view.View, java.lang.Runnable):void
      android.support.v4.b.a.a(java.io.File, java.io.File):void
      android.support.v4.b.a.a(java.io.File, java.util.zip.ZipInputStream):void
      android.support.v4.b.a.a(java.lang.Object, java.lang.StringBuilder):void
      android.support.v4.b.a.a(android.content.Context, java.lang.String):boolean
      android.support.v4.b.a.a(com.immomo.a.a.d.b, com.immomo.momo.service.bean.Message):boolean
      android.support.v4.b.a.a(byte[], java.lang.String):byte[]
      android.support.v4.b.a.a(byte[], byte[]):byte[]
      android.support.v4.b.a.a(int[], boolean):byte[]
      android.support.v4.b.a.a(byte[], boolean):int[]
      android.support.v4.b.a.a(int, float):void
      android.support.v4.b.a.a(java.lang.Object, com.immomo.a.a.f):void
      android.support.v4.view.bf.a(int, float):void
      com.immomo.a.a.f.a(java.lang.Object, com.immomo.a.a.f):void
      android.support.v4.b.a.a(java.util.Date, boolean):java.lang.String */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.b.a(java.lang.String, boolean):android.graphics.Bitmap
     arg types: [java.lang.String, int]
     candidates:
      com.immomo.momo.b.a(java.lang.String, android.graphics.Bitmap):android.graphics.Bitmap
      com.immomo.momo.b.a(java.lang.String, boolean):android.graphics.Bitmap */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup, int, boolean, boolean, int):void
     arg types: [com.immomo.momo.service.bean.bf, android.widget.ImageView, ?[OBJECT, ARRAY], int, int, int, int]
     candidates:
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, int, android.view.View, android.view.ViewGroup, boolean, int, boolean):com.immomo.momo.android.c.o
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup, int, boolean, boolean, int):void */
    /* access modifiers changed from: private */
    public void a(e eVar) {
        if (eVar != null) {
            this.w.setText(a.a(eVar.d(), true));
            this.y.setText(eVar.l);
            if (a.f(eVar.a())) {
                this.z.setText(eVar.c());
                this.z.setVisibility(0);
            } else {
                this.z.setVisibility(8);
            }
            this.A.setVisibility(8);
            this.C.setVisibility(8);
            this.B.setVisibility(8);
            if (a.f(eVar.j) && a.f(eVar.i)) {
                this.C.setVisibility(0);
                this.C.setAlt(eVar.i);
                boolean endsWith = this.k.i.endsWith(".gif");
                if (this.ad == null) {
                    File a2 = q.a(this.k.i, this.k.j);
                    this.e.b((Object) ("emoteFile:" + (a2 == null ? "null" : a2.getAbsolutePath())));
                    if (a2 == null || !a2.exists()) {
                        new i(this.k.i, this.k.j, new ay(this, endsWith)).a();
                    } else {
                        this.ad = new au(endsWith ? 1 : 2);
                        this.ad.a(a2, this.C);
                        this.ad.b(20);
                        this.C.setGifDecoder(this.ad);
                    }
                } else {
                    this.C.setGifDecoder(this.ad);
                    if ((this.ad.g() == 4 || this.ad.g() == 2) && this.ad.f() != null && this.ad.f().exists()) {
                        this.ad.a(this.ad.f(), this.C);
                    }
                }
            } else if (eVar.f() > 1) {
                this.B.setVisibility(0);
                this.B.setImage(eVar.g());
                this.B.setOnclickHandler(this);
            } else if (a.f(eVar.getLoadImageId())) {
                this.A.setVisibility(0);
                j.a(eVar, this.A, (ViewGroup) null, 15);
            }
            if (eVar.b != null) {
                this.T.setVisibility(0);
                this.S.setText(new StringBuilder(String.valueOf(eVar.b.I)).toString());
                if ("F".equals(eVar.b.H)) {
                    this.R.setBackgroundResource(R.drawable.bg_gender_famal);
                    this.J.setImageResource(R.drawable.ic_user_famale);
                } else {
                    this.R.setBackgroundResource(R.drawable.bg_gender_male);
                    this.J.setImageResource(R.drawable.ic_user_male);
                }
                if (eVar.b.j()) {
                    this.O.setVisibility(0);
                } else {
                    this.O.setVisibility(8);
                }
                if (!a.a((CharSequence) eVar.b.M)) {
                    this.P.setVisibility(0);
                    this.P.setImageBitmap(b.a(eVar.b.M, true));
                } else {
                    this.P.setVisibility(8);
                }
                if (eVar.b.an) {
                    this.K.setVisibility(0);
                    this.K.setImageResource(eVar.b.ap ? R.drawable.ic_userinfo_weibov : R.drawable.ic_userinfo_weibo);
                } else {
                    this.K.setVisibility(8);
                }
                if (eVar.b.at) {
                    this.L.setVisibility(0);
                    this.L.setImageResource(eVar.b.au ? R.drawable.ic_userinfo_tweibov : R.drawable.ic_userinfo_tweibo);
                } else {
                    this.L.setVisibility(8);
                }
                if (eVar.b.aJ == 1 || eVar.b.aJ == 3) {
                    this.Q.setVisibility(0);
                    this.Q.setImageResource(eVar.b.aJ == 1 ? R.drawable.ic_userinfo_groupowner : R.drawable.ic_userinfo_group);
                } else {
                    this.Q.setVisibility(8);
                }
                if (eVar.b.ar) {
                    this.M.setVisibility(0);
                } else {
                    this.M.setVisibility(8);
                }
                if (eVar.b.b()) {
                    this.N.setVisibility(0);
                    if (eVar.b.c()) {
                        this.N.setImageResource(R.drawable.ic_userinfo_vip_year);
                    } else {
                        this.N.setImageResource(R.drawable.ic_userinfo_vip);
                    }
                } else {
                    this.N.setVisibility(8);
                }
            } else {
                this.T.setVisibility(8);
            }
            TextView textView = this.E;
            bf bfVar = eVar.b == null ? new bf(eVar.c) : eVar.b;
            String h2 = bfVar.h();
            String str = eVar.o == 0 ? "(已退出)" : PoiTypeDef.All;
            SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(String.valueOf(h2) + str);
            if (bfVar.b()) {
                spannableStringBuilder.setSpan(new ForegroundColorSpan(g.c((int) R.color.font_vip_name)), 0, h2.length(), 33);
            } else {
                spannableStringBuilder.setSpan(new ForegroundColorSpan(g.c((int) R.color.font_value)), 0, h2.length(), 33);
            }
            if (eVar.o == 0) {
                spannableStringBuilder.setSpan(new ForegroundColorSpan(g.c((int) R.color.font_light)), h2.length(), str.length() + h2.length(), 33);
            }
            textView.setText(spannableStringBuilder);
            j.a((aj) eVar.b, this.D, (ViewGroup) null, 3, false, true, g.a(8.0f));
            if (eVar == null || eVar.e <= this.l.getCount()) {
                this.F.setText(new StringBuilder(String.valueOf(this.l.getCount())).toString());
            } else {
                this.F.setText(new StringBuilder(String.valueOf(eVar.e)).toString());
            }
            if (eVar.n) {
                this.H.setVisibility(0);
            } else {
                this.H.setVisibility(8);
            }
            if (eVar.m == 1) {
                this.I.setVisibility(0);
            } else {
                this.I.setVisibility(8);
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(f fVar) {
        this.af = false;
        this.ag = fVar;
        this.X.setVisibility(0);
        this.X.setText(a.f(fVar.f2975a.l) ? "回复" + fVar.f2975a.i + "(" + fVar.f2975a.h() + ")：" + d(fVar.g) : "回复" + fVar.f2975a.i + ":" + d(fVar.g));
        z();
    }

    static /* synthetic */ String c(String str) {
        Matcher matcher = Pattern.compile("(\\[[.[^\\[\\]]]*?\\|et\\|([.[^\\[\\]]]*?\\|)*[.[^\\[\\]]]*?\\])").matcher(str);
        if (matcher.find()) {
            return matcher.group();
        }
        return null;
    }

    private static String d(String str) {
        return str.toString().indexOf("回复") == 0 ? str.substring(str.indexOf(":") + 1) : str;
    }

    static /* synthetic */ f h(GroupFeedProfileActivity groupFeedProfileActivity) {
        if (groupFeedProfileActivity.l == null || groupFeedProfileActivity.l.getCount() <= 0) {
            return null;
        }
        return (f) groupFeedProfileActivity.l.getItem(groupFeedProfileActivity.l.getCount() - 1);
    }

    static /* synthetic */ void s(GroupFeedProfileActivity groupFeedProfileActivity) {
        at atVar = new at(groupFeedProfileActivity, groupFeedProfileActivity.r, (String[]) groupFeedProfileActivity.o.toArray(new String[0]));
        atVar.a(new bc(groupFeedProfileActivity));
        atVar.d();
    }

    /* access modifiers changed from: private */
    public void w() {
        if (this.k != null) {
            this.o = new ArrayList();
            if (this.k.p) {
                if (this.k.n) {
                    this.o.add("取消置顶");
                } else {
                    this.o.add("置顶");
                }
            }
            if (a.f(this.k.a())) {
                this.o.add("复制文本");
            }
            if (this.k.p || this.f.h.equals(this.k.c)) {
                this.o.add("删除");
            }
            if (!this.o.isEmpty() && this.r == null) {
                this.r = new bi(this);
                this.r.a((int) R.drawable.ic_topbar_more);
                this.q.a(this.r, new av(this));
            }
        }
    }

    /* access modifiers changed from: private */
    public void x() {
        this.v = g.o().inflate((int) R.layout.include_groupfeedprofile, (ViewGroup) null);
        this.w = (TextView) this.v.findViewById(R.id.tv_feed_time);
        this.y = (TextView) this.v.findViewById(R.id.tv_feed_site);
        this.z = (EmoteTextView) this.v.findViewById(R.id.tv_feed_content);
        this.A = (ImageView) this.v.findViewById(R.id.iv_feed_content);
        this.B = (MultiImageView) this.v.findViewById(R.id.mv_feed_content);
        this.C = (MGifImageView) this.v.findViewById(R.id.gv_feed_content);
        this.D = (ImageView) this.v.findViewById(R.id.iv_feed_photo);
        this.E = (TextView) this.v.findViewById(R.id.tv_feed_name);
        this.F = (TextView) this.v.findViewById(R.id.tv_feed_commentcount);
        this.Y = this.v.findViewById(R.id.layout_feed_titlecomment);
        this.x = (TextView) this.Y.findViewById(R.id.tv_feed_titlecomment);
        this.W = (ImageView) this.Y.findViewById(R.id.iv_feed_titleanim);
        this.T = this.v.findViewById(R.id.userlist_item_layout_badgeContainer);
        this.J = (ImageView) this.T.findViewById(R.id.userlist_item_iv_gender);
        this.R = this.T.findViewById(R.id.userlist_item_layout_genderbackgroud);
        this.K = (ImageView) this.T.findViewById(R.id.userlist_item_pic_iv_weibo);
        this.L = (ImageView) this.T.findViewById(R.id.userlist_item_pic_iv_tweibo);
        this.Q = (ImageView) this.T.findViewById(R.id.userlist_item_pic_iv_group_role);
        this.M = (ImageView) this.T.findViewById(R.id.userlist_item_pic_iv_renren);
        this.N = (ImageView) this.T.findViewById(R.id.userlist_item_pic_iv_vip);
        this.O = (ImageView) this.T.findViewById(R.id.userlist_item_pic_iv_multipic);
        this.P = (ImageView) this.T.findViewById(R.id.userlist_item_pic_iv_industry);
        this.S = (TextView) this.T.findViewById(R.id.userlist_item_tv_age);
        this.H = this.v.findViewById(R.id.groupfeed_iv_top);
        this.I = this.v.findViewById(R.id.groupfeed_iv_party);
    }

    /* access modifiers changed from: private */
    public void y() {
        this.aa.setImageResource(R.drawable.ic_publish_emote);
        this.ab.a();
    }

    private void z() {
        this.p.postDelayed(new bb(this), 200);
    }

    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        super.a(bundle);
        setContentView((int) R.layout.activity_feedprofile);
        this.j = getIntent().getStringExtra("key_groupfeedid");
        if (!a.f(this.j)) {
            this.e.b((Object) "++++++++++++++++++++ empty key when start FriendFeedActivity!");
            finish();
            return;
        }
        this.i = (ResizeListenerLayout) findViewById(R.id.layout_root);
        this.q = (HeaderLayout) findViewById(R.id.layout_header);
        this.q.setTitleText("群留言详情");
        this.u = g.o().inflate((int) R.layout.common_list_loadmore, (ViewGroup) null);
        this.t = (LoadingButton) this.u.findViewById(R.id.btn_loadmore);
        this.t.setNormalIconResId(R.drawable.ic_btn_inner_clock);
        this.s = (HandyListView) findViewById(R.id.lv_feed);
        this.s.addFooterView(this.u);
        this.G = findViewById(R.id.layout_cover);
        this.U = findViewById(R.id.layout_feed_comment);
        this.V = (MSmallEmoteEditeText) this.U.findViewById(R.id.tv_feed_editer);
        this.X = (TextView) this.U.findViewById(R.id.tv_feed_editertitle);
        this.Z = (Button) this.U.findViewById(R.id.bt_feed_send);
        this.aa = (ImageView) this.U.findViewById(R.id.iv_feed_emote);
        this.ab = (EmoteInputView) this.U.findViewById(R.id.emoteview);
        this.ab.setEditText(this.V);
        this.ab.setEmoteFlag(5);
        this.ab.setOnEmoteSelectedListener(new au(this));
        x();
        this.s.addHeaderView(this.v);
        this.s.setOnItemClickListener(this);
        this.t.setOnProcessListener(this);
        this.V.setOnTouchListener(this);
        this.A.setOnClickListener(this);
        this.C.setOnClickListener(this);
        this.D.setOnClickListener(this);
        this.G.setOnClickListener(this);
        this.aa.setOnClickListener(this);
        this.Z.setOnClickListener(this);
        this.i.setOnResizeListener(new aw(this));
        this.m = new v();
        this.n = (InputMethodManager) getSystemService("input_method");
        this.l = new eg(this, this.s);
        this.s.setAdapter((ListAdapter) this.l);
        d();
    }

    public final void b(int i2, String[] strArr) {
        Intent intent = new Intent(this, ImageBrowserActivity.class);
        intent.putExtra("array", strArr);
        intent.putExtra("imagetype", "feed");
        intent.putExtra("index", i2);
        startActivity(intent);
        if (getParent() != null) {
            getParent().overridePendingTransition(R.anim.zoom_enter, R.anim.normal);
        } else {
            overridePendingTransition(R.anim.zoom_enter, R.anim.normal);
        }
    }

    /* access modifiers changed from: protected */
    public final void d() {
        this.k = this.m.a(this.j);
        this.l.b((Collection) this.m.c(this.j));
        if (this.l.getCount() < 20) {
            this.u.setVisibility(8);
        } else {
            this.u.setVisibility(0);
        }
        w();
        a(this.k);
    }

    /* access modifiers changed from: protected */
    public final void e() {
        super.e();
        if (this.k == null || this.k.h != 2) {
            if (getIntent().getBooleanExtra("key_show_inputmethod", false)) {
                A();
            }
            if (this.W.getDrawable() != null) {
                if (this.ac == null) {
                    this.ac = AnimationUtils.loadAnimation(this, R.anim.loading);
                }
                this.W.startAnimation(this.ac);
            }
            if (a.f(getIntent().getStringExtra("key_commentid"))) {
                f fVar = new f();
                fVar.i = getIntent().getStringExtra("key_commentid");
                fVar.h = this.j;
                e eVar = this.k;
                fVar.b = getIntent().getStringExtra("key_owner_id");
                fVar.f2975a = new aq().b(fVar.b);
                if (fVar.f2975a == null) {
                    fVar.f2975a = new bf(fVar.b);
                }
                fVar.g = getIntent().getStringExtra("key_comment_content");
                a(fVar);
            }
            b(new bh(this, this));
            b(new bg(this, this, true));
            return;
        }
        a((CharSequence) "该留言已经被删除");
        finish();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.bt_feed_send /*2131165595*/:
                new k("S", "S4202").e();
                a(0, (String) null);
                return;
            case R.id.iv_feed_emote /*2131165596*/:
                if (this.ab.isShown()) {
                    y();
                    z();
                    return;
                }
                v();
                this.aa.setImageResource(R.drawable.ic_publish_keyboard);
                if (this.h) {
                    this.p.postDelayed(new ba(this), 300);
                } else {
                    this.ab.b();
                }
                this.G.setVisibility(0);
                this.V.requestFocus();
                return;
            case R.id.layout_cover /*2131165601*/:
                v();
                y();
                this.h = false;
                this.G.setVisibility(8);
                return;
            case R.id.iv_feed_photo /*2131166248*/:
                if (this.k != null) {
                    Intent intent = new Intent(this, OtherProfileActivity.class);
                    intent.putExtra("momoid", this.k.c);
                    startActivity(intent);
                    return;
                }
                return;
            case R.id.iv_feed_content /*2131166257*/:
                Intent intent2 = new Intent(this, ImageBrowserActivity.class);
                intent2.putExtra("array", new String[]{this.k.getLoadImageId()});
                intent2.putExtra("imagetype", "feed");
                intent2.putExtra("autohide_header", true);
                startActivity(intent2);
                overridePendingTransition(R.anim.zoom_enter, R.anim.normal);
                return;
            case R.id.gv_feed_content /*2131166259*/:
                Intent intent3 = new Intent(this, EmotionProfileActivity.class);
                intent3.putExtra("eid", this.k.j);
                startActivity(intent3);
                return;
            case R.id.layout_feed_commentcount /*2131166270*/:
                A();
                return;
            default:
                return;
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.ad != null) {
            this.ad.l();
        }
    }

    public void onItemClick(AdapterView adapterView, View view, int i2, long j2) {
        if (this.k != null) {
            f fVar = (f) this.l.getItem(i2);
            String[] strArr = fVar.l == 1 ? (this.k.p || this.f.h.equals(this.k.c) || this.f.h.equals(fVar.b)) ? new String[]{"回复", "查看表情", "删除"} : new String[]{"回复", "查看表情"} : (this.k.p || this.f.h.equals(fVar.b) || this.f.h.equals(this.k.c)) ? new String[]{"回复", "复制文本", "删除"} : new String[]{"回复", "复制文本"};
            o oVar = new o(this, strArr);
            oVar.a(new be(this, strArr, fVar));
            oVar.show();
        }
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 != 4 || !this.ab.isShown()) {
            return super.onKeyDown(i2, keyEvent);
        }
        y();
        this.G.setVisibility(8);
        this.h = false;
        return true;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        if (r() && this.k != null) {
            Intent intent = new Intent();
            intent.putExtra("feedid", this.j);
            intent.putExtra("comment_count", this.k.e);
            intent.setAction(h.f2351a);
            sendBroadcast(intent);
        }
        super.onStop();
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        switch (view.getId()) {
            case R.id.tv_feed_editer /*2131165597*/:
                y();
                return false;
            default:
                return false;
        }
    }

    public final void u() {
        this.t.f();
        b(new bg(this, this, false));
    }

    /* access modifiers changed from: protected */
    public final void v() {
        View currentFocus = getCurrentFocus();
        if (currentFocus != null) {
            this.n.hideSoftInputFromWindow(currentFocus.getWindowToken(), 2);
        }
    }
}
