package com.immomo.momo.android.activity.common;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.g;
import com.immomo.momo.protocol.a.w;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

final class h extends d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ a f1121a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public h(a aVar, Context context) {
        super(context);
        this.f1121a = aVar;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        ArrayList arrayList = new ArrayList();
        int a2 = w.a().a(arrayList, 0, this.f1121a.N);
        this.f1121a.V.c(arrayList);
        g.q().y = a2;
        this.f1121a.V.a(g.q().y, g.q().h);
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
        if (this.f1121a.X != null && !this.f1121a.X.isCancelled()) {
            this.f1121a.X.cancel(true);
        }
        this.f1121a.Q.g();
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        super.a(exc);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        List list = (List) obj;
        super.a(list);
        if (list != null) {
            this.f1121a.O.setLastFlushTime(this.f1121a.U);
            this.f1121a.N.a("lasttime_bothlist_success", this.f1121a.U);
            List unused = this.f1121a.Q();
            a.q(this.f1121a);
            if (list.size() >= g.q().y) {
                this.f1121a.O.k();
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
        super.b();
        this.f1121a.W = null;
        this.f1121a.U = new Date();
        this.f1121a.N.a("lasttime_bothlist", this.f1121a.U);
        this.f1121a.O.n();
        this.f1121a.Q.e();
    }
}
