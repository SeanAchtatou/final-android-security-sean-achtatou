package com.immomo.momo.android.a.a;

import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.message.a;
import com.immomo.momo.util.e;

public final class ag extends ab implements View.OnTouchListener {

    /* renamed from: a  reason: collision with root package name */
    private TextView f677a = null;

    protected ag(a aVar) {
        super(aVar);
    }

    /* access modifiers changed from: protected */
    public final void a() {
    }

    /* access modifiers changed from: protected */
    public final void a(View view) {
        ((LinearLayout) view.findViewById(R.id.message_layout_contentcontainer)).setVisibility(8);
        this.f677a = (TextView) view.findViewById(R.id.message_tv_noticemessage);
        this.f677a.setVisibility(0);
        super.a(view);
    }

    /* access modifiers changed from: protected */
    public final void b() {
        e.a(this.f677a, this.h.getContent(), e());
    }

    /* access modifiers changed from: protected */
    public final void f() {
        b();
    }

    public final boolean onLongClick(View view) {
        return false;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        if (motionEvent.getAction() == 0) {
            this.i.setPressed(true);
        } else if (motionEvent.getAction() == 3 || motionEvent.getAction() == 4 || motionEvent.getAction() == 1) {
            this.i.setPressed(false);
        }
        return false;
    }
}
