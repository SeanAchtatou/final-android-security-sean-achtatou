package com.immomo.momo.android.plugin.cropimage;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.MotionEvent;
import com.immomo.momo.g;
import com.immomo.momo.util.m;
import java.util.ArrayList;
import java.util.Iterator;

class CropImageView extends n {

    /* renamed from: a  reason: collision with root package name */
    ArrayList f2579a = new ArrayList();
    Context b;
    private k e = null;
    private float f;
    private float g;
    private int h;
    private int i = 0;

    public CropImageView(Context context) {
        super(context);
        if (g.a()) {
            setLayerType(1, null);
        }
    }

    public CropImageView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        if (g.a()) {
            setLayerType(1, null);
        }
    }

    public CropImageView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet);
        if (g.a()) {
            setLayerType(1, null);
        }
    }

    public static void b() {
    }

    private void b(k kVar) {
        Rect rect = kVar.b;
        int max = Math.max(0, getLeft() - rect.left);
        int min = Math.min(0, getRight() - rect.right);
        int max2 = Math.max(0, getTop() - rect.top);
        int min2 = Math.min(0, getBottom() - rect.bottom);
        if (max == 0) {
            max = min;
        }
        if (max2 == 0) {
            max2 = min2;
        }
        if (max != 0 || max2 != 0) {
            b((float) max, (float) max2);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(float, float):float} */
    private void c(k kVar) {
        Rect rect = kVar.b;
        float width = (float) rect.width();
        float height = (float) rect.height();
        Rect a2 = kVar.a();
        float width2 = (float) a2.width();
        float height2 = (float) a2.height();
        new m(this);
        if (width2 < ((float) this.i) && this.e != null) {
            int i2 = (int) (((float) this.i) - width2);
            this.h = 2;
            this.e.a(this.h, (float) (-i2), (float) (-i2));
            b(this.e);
            Rect rect2 = kVar.b;
            width = (float) rect2.width();
            height = (float) rect2.height();
        } else if (height2 < ((float) this.i) && this.e != null) {
            int i3 = (int) (((float) this.i) - height2);
            this.h = 16;
            this.e.a(this.h, (float) (-i3), (float) i3);
            b(this.e);
            Rect rect3 = kVar.b;
            width = (float) rect3.width();
            height = (float) rect3.height();
        }
        float max = Math.max(1.0f, Math.min((((float) getWidth()) / width) * 0.6f, (((float) getHeight()) / height) * 0.6f) * f());
        if (((double) (Math.abs(max - f()) / max)) > 0.1d) {
            float[] fArr = {kVar.c.centerX(), kVar.c.centerY()};
            getImageMatrix().mapPoints(fArr);
            b(max, fArr[0], fArr[1]);
        }
        b(kVar);
    }

    /* access modifiers changed from: package-private */
    public final void a() {
        this.f2579a.clear();
        this.e = null;
        this.f = 0.0f;
        this.g = 0.0f;
        this.h = 0;
        this.i = 0;
    }

    /* access modifiers changed from: protected */
    public final void a(float f2, float f3) {
        super.a(f2, f3);
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 < this.f2579a.size()) {
                k kVar = (k) this.f2579a.get(i3);
                kVar.d.postTranslate(f2, f3);
                kVar.b();
                i2 = i3 + 1;
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void a(float f2, float f3, float f4) {
        super.a(f2, f3, f4);
        Iterator it = this.f2579a.iterator();
        while (it.hasNext()) {
            k kVar = (k) it.next();
            kVar.d.set(getImageMatrix());
            kVar.b();
        }
    }

    public final void a(int i2) {
        this.i = i2;
    }

    public final void a(k kVar) {
        this.f2579a.add(kVar);
        invalidate();
    }

    public final int c() {
        return this.i;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        try {
            super.onDraw(canvas);
            int i2 = 0;
            while (true) {
                int i3 = i2;
                if (i3 < this.f2579a.size()) {
                    ((k) this.f2579a.get(i3)).a(canvas);
                    i2 = i3 + 1;
                } else {
                    return;
                }
            }
        } catch (Exception e2) {
        }
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        super.onLayout(z, i2, i3, i4, i5);
        if (this.c.b() != null) {
            Iterator it = this.f2579a.iterator();
            while (it.hasNext()) {
                k kVar = (k) it.next();
                kVar.d.set(getImageMatrix());
                kVar.b();
                if (kVar.f2590a) {
                    c(kVar);
                }
            }
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public boolean onTouchEvent(MotionEvent motionEvent) {
        int i2 = 0;
        if (((CropImageActivity) this.b).f2578a) {
            return false;
        }
        switch (motionEvent.getAction()) {
            case 0:
                while (true) {
                    if (i2 >= this.f2579a.size()) {
                        break;
                    } else {
                        k kVar = (k) this.f2579a.get(i2);
                        int a2 = kVar.a(motionEvent.getX(), motionEvent.getY());
                        if (a2 != 1) {
                            this.h = a2;
                            this.e = kVar;
                            this.f = motionEvent.getX();
                            this.g = motionEvent.getY();
                            this.e.a(a2 == 32 ? l.Move : l.Grow);
                            break;
                        } else {
                            i2++;
                        }
                    }
                }
            case 1:
                if (this.e != null) {
                    c(this.e);
                    this.e.a(l.None);
                }
                this.e = null;
                break;
            case 2:
                if (this.e != null) {
                    this.e.a(this.h, motionEvent.getX() - this.f, motionEvent.getY() - this.g);
                    this.f = motionEvent.getX();
                    this.g = motionEvent.getY();
                    b(this.e);
                    break;
                }
                break;
        }
        switch (motionEvent.getAction()) {
            case 1:
                e();
                break;
            case 2:
                if (f() == 1.0f) {
                    e();
                    break;
                }
                break;
        }
        return true;
    }
}
