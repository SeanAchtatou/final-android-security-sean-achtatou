package com.immomo.momo.android.activity.tieba;

import android.content.Intent;
import android.net.Uri;
import android.webkit.DownloadListener;

final class r implements DownloadListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ EditTieActivity f2298a;

    r(EditTieActivity editTieActivity) {
        this.f2298a = editTieActivity;
    }

    public final void onDownloadStart(String str, String str2, String str3, String str4, long j) {
        this.f2298a.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
    }
}
