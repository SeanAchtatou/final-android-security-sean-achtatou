package com.immomo.momo.android.activity.common;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.b.a;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.MomoProgressbar;
import com.immomo.momo.android.view.a.n;
import com.immomo.momo.g;
import com.immomo.momo.protocol.a.e;
import com.immomo.momo.service.ae;
import com.immomo.momo.service.bean.Message;
import com.immomo.momo.service.bean.bf;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

final class q extends d {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public /* synthetic */ CloudMessageTabsActivity f1128a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public q(CloudMessageTabsActivity cloudMessageTabsActivity, Context context) {
        super(context);
        this.f1128a = cloudMessageTabsActivity;
        if (cloudMessageTabsActivity.n != null) {
            cloudMessageTabsActivity.n.cancel(true);
        }
        cloudMessageTabsActivity.n = this;
    }

    private String c() {
        ArrayList arrayList = new ArrayList();
        for (Map.Entry key : this.f1128a.D().entrySet()) {
            arrayList.add((String) key.getKey());
        }
        HashMap a2 = e.a().a(this.f1128a.l, a.a(arrayList, ","), new v(this.f1128a, (byte) 0));
        this.f1128a.i.removeMessages(this.f1128a.t);
        this.f1128a.i.sendEmptyMessage(this.f1128a.u);
        ae aeVar = new ae();
        for (String str : a2.keySet()) {
            bf bfVar = new bf(str);
            boolean z = true;
            ArrayList<Message> arrayList2 = new ArrayList<>();
            int i = 0;
            int i2 = 0;
            while (z && i2 < 2) {
                try {
                    boolean a3 = aeVar.a(i, bfVar, arrayList2);
                    i++;
                    try {
                        this.b.a((Object) ("load success. size=" + arrayList2.size()));
                        break;
                    } catch (Exception e) {
                        e = e;
                        z = a3;
                        i2 = 0;
                    }
                } catch (Exception e2) {
                    e = e2;
                    i2++;
                    this.b.a((Throwable) e);
                }
            }
            if (arrayList2.size() > 0) {
                for (Message message : arrayList2) {
                    if (message.receive && message.status != 10) {
                        message.status = 4;
                    }
                }
                aeVar.a(arrayList2, str);
                aeVar.o(str);
            }
            if (((List) a2.get(str)).size() > 0) {
                aeVar.a((List) a2.get(str), str);
            }
        }
        this.f1128a.i.sendEmptyMessage(this.f1128a.v);
        return null;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        return c();
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
        this.f1128a.o = n.a(f(), "同步中.... 0%", "取消", new r(this));
        View inflate = g.o().inflate((int) R.layout.common_dialog_progress_cloudmsg, (ViewGroup) null);
        this.f1128a.s = (TextView) inflate.findViewById(R.id.tv_percent);
        this.f1128a.r = (TextView) inflate.findViewById(R.id.tv_postion);
        this.f1128a.p = (TextView) inflate.findViewById(R.id.tv_msg);
        this.f1128a.q = (MomoProgressbar) inflate.findViewById(R.id.progress_download);
        this.f1128a.q.setMax(100);
        this.f1128a.q.setProgress(0);
        this.f1128a.p.setText("消息同步中...");
        this.f1128a.s.setText("0%");
        this.f1128a.r.setText(PoiTypeDef.All);
        this.f1128a.o.setContentView(inflate);
        this.f1128a.o.setCancelable(true);
        this.f1128a.o.setCanceledOnTouchOutside(false);
        this.f1128a.o.setOnCancelListener(new s(this));
        this.f1128a.a(this.f1128a.o);
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        this.b.a((Throwable) exc);
        this.f1128a.q.setVisibility(8);
        this.f1128a.s.setVisibility(8);
        this.f1128a.r.setVisibility(8);
        if (exc instanceof com.immomo.momo.a.n) {
            this.f1128a.p.setText(exc.getMessage());
            this.f1128a.o.a(0, "确定", new t(this));
        } else if (exc instanceof com.immomo.momo.a.a) {
            this.f1128a.p.setText(exc.getMessage());
            this.f1128a.o.a(0, "确定", (DialogInterface.OnClickListener) null);
        } else if (exc instanceof com.immomo.a.a.b.d) {
            this.f1128a.p.setText(exc.getMessage());
            this.f1128a.o.a(0, "确定", (DialogInterface.OnClickListener) null);
        } else {
            this.f1128a.p.setText((int) R.string.errormsg_dataerror);
            this.f1128a.o.a(0, "确定", (DialogInterface.OnClickListener) null);
        }
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        g.d().a(new Bundle(), "action.syncfinished");
        this.f1128a.p.setText("同步已完成");
        this.f1128a.o.a(0, "确定", new u(this));
    }

    /* access modifiers changed from: protected */
    public final void onCancelled() {
        e.a().b();
    }
}
