package com.immomo.momo.android.view;

final class bo implements Runnable {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public boolean f2747a;
    private /* synthetic */ MGifImageView b;

    private bo(MGifImageView mGifImageView) {
        this.b = mGifImageView;
        this.f2747a = true;
    }

    /* synthetic */ bo(MGifImageView mGifImageView, byte b2) {
        this(mGifImageView);
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x0059 A[Catch:{ InterruptedException -> 0x007c, Exception -> 0x007a }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
            r5 = this;
            r1 = 0
        L_0x0001:
            boolean r0 = r5.f2747a
            if (r0 == 0) goto L_0x0019
            com.immomo.momo.android.view.MGifImageView r0 = r5.b
            com.immomo.momo.android.view.bn r0 = r0.d
            if (r0 == 0) goto L_0x003b
            com.immomo.momo.android.view.MGifImageView r0 = r5.b
            com.immomo.momo.android.view.bn r0 = r0.d
            boolean r0 = r0.N()
            if (r0 == 0) goto L_0x003b
        L_0x0019:
            r5.f2747a = r1
            com.immomo.momo.android.view.MGifImageView r0 = r5.b
            com.immomo.momo.android.view.au r0 = r0.f2635a
            if (r0 == 0) goto L_0x003a
            com.immomo.momo.android.view.MGifImageView r1 = r5.b
            com.immomo.momo.android.view.bn r1 = r1.d
            if (r1 == 0) goto L_0x003a
            com.immomo.momo.android.view.MGifImageView r1 = r5.b
            com.immomo.momo.android.view.bn r1 = r1.d
            boolean r1 = r1.N()
            if (r1 == 0) goto L_0x003a
            r0.c()
        L_0x003a:
            return
        L_0x003b:
            com.immomo.momo.android.view.MGifImageView r0 = r5.b
            com.immomo.momo.android.view.au r2 = r0.f2635a
            if (r2 == 0) goto L_0x007e
            boolean r0 = r2.h()     // Catch:{ InterruptedException -> 0x007c, Exception -> 0x007a }
            if (r0 == 0) goto L_0x007e
            com.immomo.momo.android.view.MGifImageView r0 = r5.b     // Catch:{ InterruptedException -> 0x007c, Exception -> 0x007a }
            com.immomo.momo.android.view.au r0 = r0.f2635a     // Catch:{ InterruptedException -> 0x007c, Exception -> 0x007a }
            int r0 = r0.a()     // Catch:{ InterruptedException -> 0x007c, Exception -> 0x007a }
            int r0 = r2.a(r0)     // Catch:{ InterruptedException -> 0x007c, Exception -> 0x007a }
        L_0x0057:
            if (r0 > 0) goto L_0x005b
            r0 = 150(0x96, float:2.1E-43)
        L_0x005b:
            long r3 = (long) r0     // Catch:{ InterruptedException -> 0x007c, Exception -> 0x007a }
            java.lang.Thread.sleep(r3)     // Catch:{ InterruptedException -> 0x007c, Exception -> 0x007a }
            boolean r0 = r5.f2747a     // Catch:{ InterruptedException -> 0x007c, Exception -> 0x007a }
            if (r0 == 0) goto L_0x006e
            if (r2 == 0) goto L_0x006e
            boolean r0 = r2.h()     // Catch:{ InterruptedException -> 0x007c, Exception -> 0x007a }
            if (r0 == 0) goto L_0x006e
            r2.e()     // Catch:{ InterruptedException -> 0x007c, Exception -> 0x007a }
        L_0x006e:
            com.immomo.momo.android.view.MGifImageView r0 = r5.b     // Catch:{ InterruptedException -> 0x007c, Exception -> 0x007a }
            r0.postInvalidate()     // Catch:{ InterruptedException -> 0x007c, Exception -> 0x007a }
            if (r2 == 0) goto L_0x0001
            boolean r0 = r2.d     // Catch:{ InterruptedException -> 0x007c, Exception -> 0x007a }
            if (r0 == 0) goto L_0x0001
            goto L_0x0019
        L_0x007a:
            r0 = move-exception
            goto L_0x0019
        L_0x007c:
            r0 = move-exception
            goto L_0x0001
        L_0x007e:
            r0 = r1
            goto L_0x0057
        */
        throw new UnsupportedOperationException("Method not decompiled: com.immomo.momo.android.view.bo.run():void");
    }
}
