package com.immomo.momo.android.activity;

import android.support.v4.b.a;
import android.view.View;
import com.immomo.momo.util.g;

final class ga implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ NewVersionActivity f1509a;
    private final /* synthetic */ String b;

    ga(NewVersionActivity newVersionActivity, String str) {
        this.f1509a = newVersionActivity;
        this.b = str;
    }

    public final void onClick(View view) {
        if (!a.a((CharSequence) this.b)) {
            g.a(this.f1509a, this.b, "陌陌升级", "application/vnd.android.package-archive");
        }
        this.f1509a.finish();
    }
}
