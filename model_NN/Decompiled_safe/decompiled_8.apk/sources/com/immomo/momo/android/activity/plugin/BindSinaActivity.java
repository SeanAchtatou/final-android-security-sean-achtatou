package com.immomo.momo.android.activity.plugin;

import android.os.Bundle;
import android.webkit.WebView;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.ah;
import com.immomo.momo.android.view.HeaderLayout;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.util.m;

public class BindSinaActivity extends ah {
    private HeaderLayout h = null;
    /* access modifiers changed from: private */
    public String i = "http://www.immomo.com/api/weibo/aouth_callback";
    /* access modifiers changed from: private */
    public WebView j = null;
    private TextView k = null;
    /* access modifiers changed from: private */
    public v l = null;
    /* access modifiers changed from: private */
    public String m;
    /* access modifiers changed from: private */
    public boolean n = true;
    /* access modifiers changed from: private */
    public String o;
    /* access modifiers changed from: private */
    public String p;
    /* access modifiers changed from: private */
    public w q;
    /* access modifiers changed from: private */
    public int r = 0;

    public BindSinaActivity() {
        new m("BindSina2Activity");
    }

    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        super.a(bundle);
        setContentView((int) R.layout.activity_bind_api);
        this.j = (WebView) findViewById(R.id.web);
        this.j.getSettings().setJavaScriptEnabled(true);
        this.j.setWebViewClient(new aa(this, (byte) 0));
        this.h = (HeaderLayout) findViewById(R.id.layout_header);
        this.h.setTitleText("绑定新浪微博");
        this.k = (TextView) findViewById(R.id.header_stv_title);
        this.k.setFocusableInTouchMode(false);
        d();
        this.j.post(new v(this, "https://api.weibo.com/oauth2/authorize?client_id=" + this.o + "&response_type=code&redirect_uri=" + this.i + "&display=mobile&scope=email,direct_messages_read,direct_messages_write,friendships_groups_read,friendships_groups_write,statuses_to_me_read,invitation_write"));
    }

    /* access modifiers changed from: protected */
    public final void d() {
        this.n = getIntent().getBooleanExtra("show_toast", true);
        this.o = getIntent().getStringExtra("value_key");
        this.p = getIntent().getStringExtra("value_secret");
        this.r = getIntent().getIntExtra("value_enforce", 0);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (this.q != null && this.q.isCancelled()) {
            this.q.cancel(true);
        }
        super.onDestroy();
    }
}
