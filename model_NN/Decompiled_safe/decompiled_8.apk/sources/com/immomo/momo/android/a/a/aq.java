package com.immomo.momo.android.a.a;

import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.message.a;
import com.immomo.momo.android.view.EmoteTextView;
import com.immomo.momo.util.e;

public final class aq extends ab implements View.OnTouchListener {

    /* renamed from: a  reason: collision with root package name */
    private EmoteTextView f686a = null;

    protected aq(a aVar) {
        super(aVar);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        View inflate = this.k.inflate((int) R.layout.message_text, (ViewGroup) null);
        this.i.addView(inflate, 0);
        this.f686a = (EmoteTextView) inflate.findViewById(R.id.message_tv_msgtext);
        this.i.setOnLongClickListener(this);
        this.f686a.setOnLongClickListener(this);
        this.f686a.setOnTouchListener(this);
    }

    /* access modifiers changed from: protected */
    public final void b() {
        e.a(this.f686a, this.h.getEmoteContent(), e());
    }

    /* access modifiers changed from: protected */
    public final String[] i() {
        return j() ? f : this.h.status == 3 ? d : e;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        if (motionEvent.getAction() == 0) {
            this.i.setPressed(true);
        } else if (motionEvent.getAction() == 3 || motionEvent.getAction() == 4 || motionEvent.getAction() == 1) {
            this.i.setPressed(false);
        }
        return false;
    }
}
