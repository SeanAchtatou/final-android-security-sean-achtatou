package com.immomo.momo.android.activity.tieba;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.protocol.a.v;
import com.immomo.momo.service.bean.c.f;
import java.util.Date;

final class de extends d {

    /* renamed from: a  reason: collision with root package name */
    private f f2233a = new f();
    private /* synthetic */ TiebaCategoryDetailActivity c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public de(TiebaCategoryDetailActivity tiebaCategoryDetailActivity, Context context) {
        super(context);
        this.c = tiebaCategoryDetailActivity;
        if (tiebaCategoryDetailActivity.p != null) {
            tiebaCategoryDetailActivity.p.cancel(true);
        }
        tiebaCategoryDetailActivity.p = this;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        return Boolean.valueOf(v.a().a(this.c.q, this.c.m.getCount(), this.f2233a));
    }

    /* access modifiers changed from: protected */
    public final void a() {
        if (this.c.o != null) {
            cancel(true);
            this.c.u.e();
            this.c.p = (de) null;
            return;
        }
        this.c.u.f();
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        Boolean bool = (Boolean) obj;
        this.c.g.a("categorydetail_latttime_reflush", this.c.l);
        this.c.s = String.valueOf(this.c.r) + "(" + this.f2233a.e + ")";
        this.c.setTitle(this.c.s);
        for (com.immomo.momo.service.bean.c.d dVar : this.f2233a.f) {
            if (!this.c.v.contains(dVar)) {
                this.c.m.a(dVar);
                this.c.v.add(dVar);
            }
        }
        if (bool.booleanValue()) {
            this.c.u.setVisibility(0);
        } else {
            this.c.u.setVisibility(8);
        }
        this.c.m.notifyDataSetChanged();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.c.k.n();
        this.c.u.e();
        this.c.l = new Date();
        this.c.p = (de) null;
        this.c.k.setLastFlushTime(this.c.l);
    }
}
