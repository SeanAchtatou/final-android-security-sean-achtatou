package com.immomo.momo.android.view;

import android.view.View;

final class bq implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ MomoRefreshExpandableListView f2749a;

    bq(MomoRefreshExpandableListView momoRefreshExpandableListView) {
        this.f2749a = momoRefreshExpandableListView;
    }

    public final void onClick(View view) {
        if (this.f2749a.u != null) {
            this.f2749a.setLoadingVisible(false);
            this.f2749a.u.v();
        }
    }
}
