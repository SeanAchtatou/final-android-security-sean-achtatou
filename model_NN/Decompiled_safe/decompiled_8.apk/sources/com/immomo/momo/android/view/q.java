package com.immomo.momo.android.view;

import android.support.v4.b.a;
import android.support.v4.view.u;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.immomo.momo.R;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.d;

final class q extends u {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ g f2825a;

    private q(g gVar) {
        this.f2825a = gVar;
    }

    /* synthetic */ q(g gVar, byte b) {
        this(gVar);
    }

    public final Object a(ViewGroup viewGroup, int i) {
        View inflate = g.o().inflate((int) R.layout.listitem_banner, (ViewGroup) null);
        int size = this.f2825a.g.size();
        if (size <= 0) {
            return inflate;
        }
        int i2 = i >= size ? i % size : i;
        d dVar = (d) this.f2825a.g.get(i2);
        if (dVar == null) {
            return inflate;
        }
        ImageView imageView = (ImageView) inflate.findViewById(R.id.imageview);
        int b = this.f2825a.b(i2);
        if (dVar.a() == null && dVar.b()) {
            dVar.a(a.l(dVar.i.getPath()));
        }
        if (dVar.a() == null) {
            this.f2825a.v.execute(new r(this.f2825a, dVar));
        }
        imageView.setImageBitmap(dVar.a());
        viewGroup.addView(inflate);
        inflate.setTag(Integer.valueOf(b));
        inflate.setTag(R.id.tag_item_position, Integer.valueOf(i));
        inflate.setOnClickListener(this.f2825a.A);
        return inflate;
    }

    public final void a(ViewGroup viewGroup, int i, Object obj) {
        viewGroup.removeView((View) obj);
    }

    public final boolean a(View view, Object obj) {
        return view == obj;
    }

    public final int b() {
        return Integer.MAX_VALUE;
    }
}
