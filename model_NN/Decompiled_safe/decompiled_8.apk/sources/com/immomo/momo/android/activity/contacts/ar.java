package com.immomo.momo.android.activity.contacts;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import com.immomo.momo.android.activity.OtherProfileActivity;
import com.immomo.momo.service.bean.bf;

final class ar implements AdapterView.OnItemClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ an f1149a;

    ar(an anVar) {
        this.f1149a = anVar;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        if (i < this.f1149a.R.size()) {
            Intent intent = new Intent(this.f1149a.c(), OtherProfileActivity.class);
            intent.putExtra("tag", "local");
            intent.putExtra("momoid", ((bf) this.f1149a.R.get(i)).h);
            this.f1149a.a(intent);
        }
    }
}
