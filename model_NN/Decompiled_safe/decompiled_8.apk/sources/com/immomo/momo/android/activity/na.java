package com.immomo.momo.android.activity;

import android.content.Intent;
import android.view.View;
import com.immomo.momo.util.k;

final class na implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ WelcomeActivity f2018a;

    na(WelcomeActivity welcomeActivity) {
        this.f2018a = welcomeActivity;
    }

    public final void onClick(View view) {
        new k("C", "C11101").e();
        this.f2018a.startActivity(new Intent(this.f2018a.getApplicationContext(), AboutTabsActivity.class));
    }
}
