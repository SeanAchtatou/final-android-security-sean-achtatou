package com.immomo.momo.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.a.b;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import com.immomo.momo.R;
import com.immomo.momo.android.a.fq;
import com.immomo.momo.android.broadcast.w;
import com.immomo.momo.android.view.HeaderLayout;
import com.immomo.momo.android.view.MomoRefreshListView;
import com.immomo.momo.android.view.bi;
import com.immomo.momo.android.view.bu;
import com.immomo.momo.android.view.cv;
import com.immomo.momo.g;
import com.immomo.momo.service.aq;
import com.immomo.momo.service.bean.bf;
import java.util.Date;
import java.util.List;

public class HiddenlistActivity extends ah implements b, bu, cv {
    private HeaderLayout h;
    /* access modifiers changed from: private */
    public MomoRefreshListView i;
    private List j = null;
    /* access modifiers changed from: private */
    public aq k;
    /* access modifiers changed from: private */
    public fq l;
    private fd m = null;
    /* access modifiers changed from: private */
    public Date n = null;
    private w o = null;

    static /* synthetic */ void a(HiddenlistActivity hiddenlistActivity, bf bfVar) {
        Intent intent = new Intent(hiddenlistActivity.getApplicationContext(), OtherProfileActivity.class);
        intent.putExtra("tag", "local");
        intent.putExtra("momoid", bfVar.h);
        hiddenlistActivity.startActivity(intent);
    }

    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        super.a(bundle);
        setContentView((int) R.layout.activity_hidelist);
        this.i = (MomoRefreshListView) findViewById(R.id.listview);
        this.h = (HeaderLayout) findViewById(R.id.layout_header);
        this.h.setTitleText("定向隐身");
        this.i.a(g.o().inflate((int) R.layout.include_hiddenlist_listempty, (ViewGroup) null));
        bi biVar = new bi(this);
        biVar.a("添加");
        this.h.a(biVar, new ex(this));
        this.i.setOnCancelListener$135502(this);
        this.i.setOnPullToRefreshListener$42b903f6(this);
        this.i.setOnItemLongClickListener(new ey(this));
        this.i.setOnItemClickListener(new fa(this));
        this.o = new w(this);
        this.o.a(new fc(this));
        d();
    }

    public final void b_() {
        this.m = new fd(this, this);
        this.m.execute(new Object[0]);
    }

    /* access modifiers changed from: protected */
    public final void d() {
        this.k = new aq();
        this.j = this.k.e();
        this.l = new fq(this, this.j, this.i);
        this.i.setAdapter((ListAdapter) this.l);
        if (this.j.size() <= 0) {
            this.i.l();
        }
        this.i.setLastFlushTime(this.g.f());
        this.n = this.g.f();
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        if (i3 == -1 && i2 == 6876) {
            intent.getStringExtra("result_userid");
            this.i.b();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (this.o != null) {
            unregisterReceiver(this.o);
            this.o = null;
        }
        if (this.m != null && !this.m.isCancelled()) {
            this.m.cancel(true);
            this.m = null;
        }
        super.onDestroy();
    }

    public final void v() {
        this.i.n();
        if (this.m != null && !this.m.isCancelled()) {
            this.m.cancel(true);
            this.m = null;
        }
    }

    /* access modifiers changed from: protected */
    public final void w() {
        startActivityForResult(new Intent(this, SetHiddenActivity.class), 6876);
    }
}
