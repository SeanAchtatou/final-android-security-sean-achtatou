package com.immomo.momo.android.activity.event;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.protocol.a.j;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

final class bt extends d {

    /* renamed from: a  reason: collision with root package name */
    private List f1388a = new ArrayList();
    private /* synthetic */ bd c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public bt(bd bdVar, Context context) {
        super(context);
        this.c = bdVar;
        if (bdVar.T != null) {
            bdVar.T.cancel(true);
        }
        bdVar.T = this;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        new AtomicInteger();
        boolean a2 = j.a().a(this.f1388a, 0, this.c.M.S, this.c.M.T, this.c.M.aH, this.c.Y.a(), this.c.Z.a(), this.c.aa.a());
        this.c.W.a(this.f1388a);
        this.c.S.clear();
        this.c.S.addAll(this.f1388a);
        return Boolean.valueOf(a2);
    }

    /* access modifiers changed from: protected */
    public final void a() {
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        Boolean bool = (Boolean) obj;
        this.c.O.setLastFlushTime(this.c.Q);
        this.c.N.a("nevents_lasttime_success", this.c.Q);
        this.c.N.c("nefilter_remain", bool);
        this.c.R = this.f1388a;
        this.c.V.a((Collection) this.c.R);
        if (!bool.booleanValue()) {
            this.c.P.setVisibility(8);
        } else {
            this.c.P.setVisibility(0);
        }
        this.c.ac.j();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.c.O.n();
        this.c.Q = new Date();
        this.c.N.a("nevents_latttime_reflush", this.c.Q);
        this.c.T = (bt) null;
    }
}
