package com.immomo.momo.android.activity.group;

import android.content.Intent;
import android.view.View;
import android.widget.ExpandableListView;
import com.immomo.momo.android.activity.OtherProfileActivity;
import com.immomo.momo.service.bean.a.j;

final class cb implements ExpandableListView.OnChildClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ GroupMemberListActivity f1576a;

    cb(GroupMemberListActivity groupMemberListActivity) {
        this.f1576a = groupMemberListActivity;
    }

    public final boolean onChildClick(ExpandableListView expandableListView, View view, int i, int i2, long j) {
        if (!this.f1576a.m.a(i) && !this.f1576a.m.b(i)) {
            j a2 = this.f1576a.m.getChild(i, i2);
            Intent intent = new Intent(this.f1576a.getApplicationContext(), OtherProfileActivity.class);
            intent.putExtra("tag", "local");
            intent.putExtra("momoid", a2.f2979a);
            this.f1576a.startActivity(intent);
        }
        return true;
    }
}
