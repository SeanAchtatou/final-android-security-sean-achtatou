package com.immomo.momo.android.activity.message;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.b.a;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListAdapter;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.a.a.f.b;
import com.immomo.momo.R;
import com.immomo.momo.android.a.o;
import com.immomo.momo.android.activity.ImageFactoryActivity;
import com.immomo.momo.android.activity.ah;
import com.immomo.momo.service.au;
import com.immomo.momo.service.bean.bh;
import com.immomo.momo.util.ao;
import com.immomo.momo.util.aw;
import com.immomo.momo.util.h;
import java.io.File;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import mm.purchasesdk.PurchaseCode;

public class ChatBGSettingActivity extends ah implements AdapterView.OnItemClickListener {
    private GridView h;
    private View i;
    /* access modifiers changed from: private */
    public o j;
    private String k;
    private int l = 800;
    private int m = 480;
    private boolean n = false;
    /* access modifiers changed from: private */
    public au o;
    private String p = PoiTypeDef.All;
    private File q = null;
    private File r = null;
    private Bitmap s = null;
    /* access modifiers changed from: private */
    public Handler t = new az(this);

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public static void a(Activity activity) {
        Intent intent = new Intent(activity, ChatBGSettingActivity.class);
        intent.putExtra("key_global", true);
        activity.startActivity(intent);
    }

    public static void a(Activity activity, String str) {
        Intent intent = new Intent(activity, ChatBGSettingActivity.class);
        intent.putExtra("key_resourseid", str);
        activity.startActivityForResult(intent, PurchaseCode.AUTH_NO_BUSINESS);
    }

    static /* synthetic */ void e(ChatBGSettingActivity chatBGSettingActivity) {
        Intent intent = new Intent("android.intent.action.GET_CONTENT");
        intent.setType("image/*");
        chatBGSettingActivity.startActivityForResult(intent, PurchaseCode.ORDER_OK);
    }

    static /* synthetic */ void f(ChatBGSettingActivity chatBGSettingActivity) {
        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("immomo_");
        stringBuffer.append(a.c(new Date()));
        stringBuffer.append("_" + UUID.randomUUID());
        stringBuffer.append(".jpg");
        chatBGSettingActivity.p = stringBuffer.toString();
        intent.putExtra("output", Uri.fromFile(new File(com.immomo.momo.a.i(), chatBGSettingActivity.p)));
        chatBGSettingActivity.startActivityForResult(intent, PurchaseCode.UNSUB_OK);
    }

    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        super.a(bundle);
        setContentView((int) R.layout.activity_setchatbackground);
    }

    /* access modifiers changed from: protected */
    public final void d() {
        this.n = getIntent().getBooleanExtra("key_global", this.n);
        String stringExtra = getIntent().getStringExtra("key_resourseid");
        this.k = (String) o().b("chatbg_resourseid", "bg_chat_preview_001");
        this.o = new au();
        List a2 = this.o.a();
        if (a2.isEmpty()) {
            a2.add(new bh("bg_chat_preview_001"));
            a2.add(new bh("bg_chat_preview_002"));
            a2.add(new bh("bg_chat_preview_003"));
            a2.add(new bh("bg_chat_preview_004"));
            a2.add(new bh("bg_chat_preview_005"));
            a2.add(new bh("bg_chat_preview_006"));
            a2.add(new bh("bg_chat_preview_007"));
            a2.add(new bh("bg_chat_preview_008"));
            a2.add(new bh("bg_chat_preview_009"));
        }
        this.m = this.i.getWidth();
        this.l = this.i.getHeight();
        String str = this.n ? this.k : stringExtra;
        if (!a.f(str)) {
            str = this.k;
        }
        bh bhVar = new bh();
        if (str.indexOf("bg_chat") < 0) {
            bhVar.f3013a = str;
            if (!aw.a(bhVar)) {
                str = "bg_chat_preview_001";
                bhVar.f3013a = null;
            }
        }
        a2.add(bhVar);
        this.j = new o(this, a2, new bh(str));
        this.h.setAdapter((ListAdapter) this.j);
        b(new bb(this, this));
    }

    /* access modifiers changed from: protected */
    public final void e() {
        this.i = findViewById(R.id.root_layout);
        this.h = (GridView) findViewById(R.id.setchatbg_gridview);
        this.h.setOnItemClickListener(this);
        m().setTitleText("聊天背景");
        d();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.h.a(java.lang.String, android.graphics.Bitmap, int, boolean):java.io.File
     arg types: [java.lang.String, android.graphics.Bitmap, int, int]
     candidates:
      com.immomo.momo.util.h.a(java.lang.String, java.lang.String, int, boolean):void
      com.immomo.momo.util.h.a(java.lang.String, android.graphics.Bitmap, int, boolean):java.io.File */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.b.a.a(android.graphics.Bitmap, float, boolean):android.graphics.Bitmap
     arg types: [android.graphics.Bitmap, int, int]
     candidates:
      android.support.v4.b.a.a(android.graphics.Bitmap, int, int):android.graphics.Bitmap
      android.support.v4.b.a.a(java.io.File, int, int):android.graphics.Bitmap
      android.support.v4.b.a.a(java.lang.String, java.lang.String, java.lang.String):java.lang.String
      android.support.v4.b.a.a(java.lang.Object[], java.lang.String, java.lang.String):java.lang.String
      android.support.v4.b.a.a(android.graphics.Bitmap, float, boolean):android.graphics.Bitmap */
    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        Uri fromFile;
        switch (i2) {
            case PurchaseCode.QUERY_OK /*101*/:
                if (i3 != -1 || intent == null) {
                    if (i3 == 1003) {
                        ao.b("图片尺寸太小，请重新选择", 1);
                    } else if (i3 == 1000) {
                        ao.d(R.string.cropimage_error_other);
                    } else if (i3 == 1002) {
                        ao.d(R.string.cropimage_error_store);
                    } else if (i3 == 1001) {
                        ao.d(R.string.cropimage_error_filenotfound);
                    }
                    if (this.q != null && this.q.exists()) {
                        try {
                            this.q.delete();
                            return;
                        } catch (Exception e) {
                            return;
                        }
                    } else {
                        return;
                    }
                } else {
                    getWindow().getDecorView().requestFocus();
                    if (!a.a((CharSequence) this.p)) {
                        File file = new File(com.immomo.momo.a.i(), this.p);
                        if (file.exists()) {
                            file.delete();
                        }
                        this.p = null;
                    }
                    if (this.q != null) {
                        String absolutePath = this.q.getAbsolutePath();
                        String substring = this.q.getName().substring(0, this.q.getName().lastIndexOf("."));
                        Bitmap l2 = a.l(absolutePath);
                        if (l2 != null) {
                            Bitmap a2 = a.a(l2, this.m, this.l);
                            this.r = h.a(substring, a2, 2, false);
                            this.e.a((Object) ("scaleAndSavePhoto, uploadFile=" + this.r.getPath()));
                            this.s = a.a(a2, 320.0f, true);
                            h.a(String.valueOf(substring) + "_preview", this.s, 2, false);
                            ((bh) this.j.getItem(this.j.getCount() - 1)).f3013a = String.valueOf(substring) + "_preview";
                            this.j.d(this.j.getCount() - 1);
                            getWindow().getDecorView().requestFocus();
                            l2 = a2;
                        }
                        try {
                            this.q.delete();
                            this.q = null;
                            l2.recycle();
                            this.s.recycle();
                            return;
                        } catch (Exception e2) {
                            return;
                        }
                    } else {
                        return;
                    }
                }
            case PurchaseCode.ORDER_OK /*102*/:
                if (i3 == -1 && intent != null) {
                    Uri data = intent.getData();
                    Intent intent2 = new Intent(this, ImageFactoryActivity.class);
                    intent2.setData(data);
                    intent2.putExtra("minsize", ((double) this.m) * 1.5d);
                    intent2.putExtra("aspectY", this.l);
                    intent2.putExtra("aspectX", this.m);
                    this.q = new File(com.immomo.momo.a.g(), "temp_" + b.a() + ".jpg_");
                    intent2.putExtra("outputFilePath", this.q.getAbsolutePath());
                    startActivityForResult(intent2, PurchaseCode.QUERY_OK);
                    return;
                }
                return;
            case PurchaseCode.UNSUB_OK /*103*/:
                if (i3 == -1 && !a.a((CharSequence) this.p) && (fromFile = Uri.fromFile(new File(com.immomo.momo.a.i(), this.p))) != null) {
                    Intent intent3 = new Intent(this, ImageFactoryActivity.class);
                    intent3.setData(fromFile);
                    intent3.putExtra("aspectY", this.l);
                    intent3.putExtra("aspectX", this.m);
                    this.q = new File(com.immomo.momo.a.g(), "temp_" + b.a() + ".jpg_");
                    intent3.putExtra("outputFilePath", this.q.getAbsolutePath());
                    startActivityForResult(intent3, PurchaseCode.QUERY_OK);
                    return;
                }
                return;
            default:
                return;
        }
    }

    public void onBackPressed() {
        String str = this.j.e().f3013a;
        if (!this.n && str.equals(this.k)) {
            str = PoiTypeDef.All;
        }
        if (this.n) {
            o().c("chatbg_resourseid", str);
        } else {
            Intent intent = new Intent();
            intent.putExtra("key_resourseid", str);
            setResult(-1, intent);
        }
        super.onBackPressed();
    }

    public void onItemClick(AdapterView adapterView, View view, int i2, long j2) {
        if (i2 == this.j.getCount() - 1) {
            com.immomo.momo.android.view.a.o oVar = new com.immomo.momo.android.view.a.o(this, (int) R.array.editprofile_add_photo);
            oVar.setTitle("选取照片");
            oVar.a(new ba(this));
            oVar.show();
            return;
        }
        bh bhVar = (bh) this.j.getItem(i2);
        if (i2 == 0 || aw.a(bhVar)) {
            this.j.d(i2);
            return;
        }
        bc bcVar = new bc(this, bhVar);
        aw.a().a(bhVar, bcVar, bcVar);
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Bundle bundle) {
        this.p = bundle.getString("camera_filename");
        this.n = bundle.getBoolean("global");
        if (bundle.containsKey("avatorFile")) {
            try {
                this.q = new File(bundle.getString("avatorFile"));
            } catch (Exception e) {
            }
        }
        if (bundle.containsKey("uploadFile")) {
            try {
                this.q = new File(bundle.getString("uploadFile"));
            } catch (Exception e2) {
            }
        }
        super.onRestoreInstanceState(bundle);
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        bundle.putString("camera_filename", this.p);
        bundle.putString("resourseid", this.j.e().f3013a);
        bundle.putInt("hight", this.l);
        bundle.putInt("width", this.m);
        bundle.putBoolean("global", this.n);
        if (this.q != null) {
            bundle.putString("avatorFile", this.q.getAbsoluteFile().toString());
        }
        if (this.r != null) {
            bundle.putString("uploadFile", this.r.getAbsoluteFile().toString());
        }
    }
}
