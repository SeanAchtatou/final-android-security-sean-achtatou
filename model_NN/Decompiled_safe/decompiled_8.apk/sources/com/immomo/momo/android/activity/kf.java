package com.immomo.momo.android.activity;

import android.content.Context;
import android.content.Intent;
import com.immomo.momo.android.c.b;
import com.immomo.momo.protocol.a.w;

final class kf extends b {
    private String c = null;
    private /* synthetic */ SetHiddenActivity d;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public kf(SetHiddenActivity setHiddenActivity, Context context) {
        super(context);
        this.d = setHiddenActivity;
    }

    /* access modifiers changed from: protected */
    public final Object a(Object... objArr) {
        w.a().g(this.c);
        return null;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
        this.c = this.d.i.getText().toString();
    }

    /* access modifiers changed from: protected */
    public final void a(Object obj) {
        a("设置成功");
        Intent intent = new Intent();
        intent.putExtra("result_userid", this.c);
        this.d.setResult(-1, intent);
        this.d.finish();
    }
}
