package com.immomo.momo.android.activity.tieba;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.protocol.a.v;
import com.immomo.momo.service.aq;
import com.immomo.momo.service.bean.bc;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.service.bean.c.g;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

final class er extends d {

    /* renamed from: a  reason: collision with root package name */
    private boolean f2269a;
    private List c = new ArrayList();
    private /* synthetic */ TiebaMemberListActivity d;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public er(TiebaMemberListActivity tiebaMemberListActivity, Context context, boolean z) {
        super(context);
        this.d = tiebaMemberListActivity;
        this.f2269a = z;
        this.c.add(new bc());
        this.c.add(new bc());
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        int i = 0;
        if (!this.f2269a) {
            i = this.d.l.size() == 1 ? ((bc) this.d.l.get(0)).f3008a.size() : ((bc) this.d.l.get(1)).f3008a.size();
        }
        boolean a2 = v.a().a(this.c, this.d.j, i, this.d.i);
        for (bc bcVar : this.c) {
            if (bcVar.f3008a != null) {
                aq aqVar = new aq();
                for (bf d2 : bcVar.f3008a) {
                    aqVar.d(d2);
                }
            }
        }
        return Boolean.valueOf(a2);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        Boolean bool = (Boolean) obj;
        super.a(bool);
        Iterator it = ((bc) this.c.get(1)).f3008a.iterator();
        while (it.hasNext()) {
            if (!this.d.h.add(((g) it.next()).h)) {
                it.remove();
                it = ((bc) this.c.get(1)).f3008a.iterator();
            }
        }
        if (this.f2269a) {
            TiebaMemberListActivity.d(this.d);
            if (((bc) this.c.get(0)).f3008a.size() == 0 && this.d.l.size() > 1) {
                this.d.l.remove(0);
            }
            for (int i = 0; i < this.d.l.size(); i++) {
                ((bc) this.d.l.get(i)).f3008a.clear();
            }
        }
        if (this.d.l.size() <= 1) {
            ((bc) this.d.l.get(0)).f3008a.addAll(((bc) this.c.get(1)).f3008a);
        } else {
            for (int i2 = 0; i2 < this.d.l.size(); i2++) {
                ((bc) this.d.l.get(i2)).f3008a.addAll(((bc) this.c.get(i2)).f3008a);
            }
        }
        if (bool.booleanValue()) {
            this.d.o.setVisibility(0);
        } else {
            this.d.o.setVisibility(8);
        }
        this.d.m.a();
        this.d.m.notifyDataSetChanged();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        super.b();
        this.d.w();
    }
}
