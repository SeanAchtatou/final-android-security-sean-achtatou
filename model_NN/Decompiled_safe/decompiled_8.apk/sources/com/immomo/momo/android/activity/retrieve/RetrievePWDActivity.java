package com.immomo.momo.android.activity.retrieve;

import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.b.a;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TabHost;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.android.view.HeaderLayout;
import com.immomo.momo.g;

public class RetrievePWDActivity extends TabActivity {

    /* renamed from: a  reason: collision with root package name */
    private TabHost f2106a = null;
    private int b = 0;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_abouttabs);
        this.f2106a = getTabHost();
        ((HeaderLayout) findViewById(R.id.layout_header)).setTitleText("找回密码");
        String stringExtra = getIntent().getStringExtra("phonenumber");
        getIntent().getStringExtra("areacode");
        String stringExtra2 = getIntent().getStringExtra("email");
        if (!a.f(stringExtra) && a.f(stringExtra2)) {
            this.b = 1;
        }
        LayoutInflater o = g.o();
        Class<InputPhoneNumberActivity> cls = InputPhoneNumberActivity.class;
        View inflate = o.inflate((int) R.layout.common_tabbar_item_lightblue, (ViewGroup) null);
        ((TextView) inflate.findViewById(R.id.tab_item_tv_label)).setText("手机号码");
        TabHost.TabSpec indicator = this.f2106a.newTabSpec(cls.getName()).setIndicator(inflate);
        indicator.setContent(getIntent().setClass(getApplicationContext(), cls));
        this.f2106a.addTab(indicator);
        Class<InputEmailAddressActivity> cls2 = InputEmailAddressActivity.class;
        View inflate2 = o.inflate((int) R.layout.common_tabbar_item_lightblue, (ViewGroup) null);
        inflate2.findViewById(R.id.tabitem_ligthblue_driver_left).setVisibility(0);
        ((TextView) inflate2.findViewById(R.id.tab_item_tv_label)).setText("电子邮箱");
        TabHost.TabSpec indicator2 = this.f2106a.newTabSpec(cls2.getName()).setIndicator(inflate2);
        indicator2.setContent(new Intent(getIntent().setClass(getApplicationContext(), cls2)));
        this.f2106a.addTab(indicator2);
        this.f2106a.setCurrentTab(this.b);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
    }
}
