package com.immomo.momo.android.activity.group;

import com.immomo.momo.R;
import com.immomo.momo.android.c.ac;
import com.immomo.momo.android.view.a.al;
import com.immomo.momo.g;

final class cw implements al {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ GroupProfileActivity f1595a;

    cw(GroupProfileActivity groupProfileActivity) {
        this.f1595a = groupProfileActivity;
    }

    public final void a(int i) {
        String[] stringArray = g.l().getStringArray(R.array.reportgroup_items);
        if (i < stringArray.length) {
            new ac(this.f1595a).execute(new String[]{this.f1595a.l, stringArray[i]});
        }
    }
}
