package com.immomo.momo.android.map;

import android.location.Location;
import com.immomo.momo.android.b.l;
import com.immomo.momo.android.b.p;
import com.immomo.momo.android.b.z;

final class ae extends p {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ p f2458a;

    ae(p pVar) {
        this.f2458a = pVar;
    }

    public final void a(Location location, int i, int i2, int i3) {
        if (i != 0) {
            this.f2458a.a(location, i, i2, i3);
        } else if (z.a(location)) {
            new l(this.f2458a, 0).execute(location);
        } else {
            this.f2458a.a(location, i, i2, i3);
        }
    }
}
