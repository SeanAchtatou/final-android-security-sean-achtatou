package com.immomo.momo.android.a;

import android.view.View;

final class dw implements View.OnLongClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ dn f786a;
    private final /* synthetic */ int b;

    dw(dn dnVar, int i) {
        this.f786a = dnVar;
        this.b = i;
    }

    public final boolean onLongClick(View view) {
        return this.f786a.e.getOnItemLongClickListenerInWrapper().onItemLongClick(this.f786a.e, null, this.b, (long) view.getId());
    }
}
