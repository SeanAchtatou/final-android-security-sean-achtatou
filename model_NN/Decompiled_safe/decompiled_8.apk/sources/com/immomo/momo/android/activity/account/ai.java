package com.immomo.momo.android.activity.account;

import android.content.DialogInterface;

final class ai implements DialogInterface.OnDismissListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ac f937a;

    ai(ac acVar) {
        this.f937a = acVar;
    }

    public final void onDismiss(DialogInterface dialogInterface) {
        if (this.f937a.g != null) {
            this.f937a.g.cancel(true);
            this.f937a.g = (am) null;
        }
    }
}
