package com.immomo.momo.android.activity.message;

import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.android.a.a.m;
import com.immomo.momo.android.activity.ah;
import com.immomo.momo.android.broadcast.e;
import com.immomo.momo.android.c.u;
import com.immomo.momo.android.view.HeaderLayout;
import com.immomo.momo.android.view.LoadingButton;
import com.immomo.momo.android.view.RefreshOnOverScrollListView;
import com.immomo.momo.android.view.a;
import com.immomo.momo.android.view.bl;
import com.immomo.momo.android.view.cd;
import com.immomo.momo.android.view.cg;
import com.immomo.momo.g;
import com.immomo.momo.service.ag;
import com.immomo.momo.service.aq;
import com.immomo.momo.service.bean.aw;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.util.k;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadPoolExecutor;
import org.json.JSONObject;

public class HiSessionListActivity extends ah implements bl {
    public Handler h = new bw(this);
    private int i = 0;
    private int j = 0;
    /* access modifiers changed from: private */
    public ag k = null;
    /* access modifiers changed from: private */
    public RefreshOnOverScrollListView l = null;
    private HeaderLayout m = null;
    private View n = null;
    private ThreadPoolExecutor o = null;
    private TextView p;
    /* access modifiers changed from: private */
    public m q = null;
    /* access modifiers changed from: private */
    public aq r = null;
    /* access modifiers changed from: private */
    public LoadingButton s = null;
    private Date t = new Date();
    private e u = null;
    /* access modifiers changed from: private */
    public cd v = null;
    /* access modifiers changed from: private */
    public Map w = new HashMap();

    static /* synthetic */ void a(HiSessionListActivity hiSessionListActivity, String str) {
        hiSessionListActivity.q.c(new aw(str));
        hiSessionListActivity.w();
    }

    private void a(List list) {
        ArrayList arrayList = new ArrayList();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            aw awVar = (aw) it.next();
            if (awVar.e() == null) {
                awVar.a(new bf(awVar.d()));
                arrayList.add(awVar.d());
                this.w.put(awVar.d(), awVar);
            }
        }
        if (arrayList.size() > 0) {
            this.o.execute(new cc(this, (String[]) arrayList.toArray(new String[arrayList.size()])));
        }
    }

    private void c(String str) {
        int i2;
        aw a2 = this.k.a(str);
        if (a2 != null) {
            int f = this.q.f(new aw(str));
            if (f >= 0) {
                this.q.b(f);
                i2 = ((aw) this.q.getItem(f)).a().after(this.t) ? 0 : f;
            } else {
                if (a2.e() == null) {
                    a2.a(this.r.b(a2.d()));
                    if (a2.e() == null) {
                        a2.a(new bf(a2.d()));
                        this.w.put(a2.d(), a2);
                        this.o.execute(new cc(this, new String[]{a2.d()}));
                    }
                }
                i2 = 0;
            }
            if (i2 == 0) {
                this.t = a2.a();
                this.l.i();
            }
            this.q.b(i2, a2);
        } else if (this.q.c(new aw(str))) {
            this.q.notifyDataSetChanged();
        }
    }

    static /* synthetic */ ArrayList f(HiSessionListActivity hiSessionListActivity) {
        ArrayList arrayList = (ArrayList) hiSessionListActivity.k.b(hiSessionListActivity.q.getCount());
        if (arrayList.size() > 20) {
            arrayList.remove(arrayList.size() - 1);
            hiSessionListActivity.n.setVisibility(0);
        } else {
            hiSessionListActivity.l.removeFooterView(hiSessionListActivity.n);
        }
        hiSessionListActivity.a(arrayList);
        hiSessionListActivity.q.b((Collection) arrayList);
        return arrayList;
    }

    /* access modifiers changed from: private */
    public void v() {
        if (this.i > 0) {
            this.p.setText(String.valueOf(this.i) + "条新招呼/共" + this.j + "条");
        } else {
            this.p.setText("共" + this.j + "条");
        }
    }

    private void w() {
        this.i = this.k.e();
        this.j = this.k.d();
        this.h.sendEmptyMessage(7168);
    }

    public final void a(Bundle bundle) {
        super.a(bundle);
        setContentView((int) R.layout.activity_hisessionlist);
        g.d().q();
        this.k = new ag();
        this.r = new aq();
        this.o = u.c();
        this.m = (HeaderLayout) findViewById(R.id.layout_header);
        this.p = (TextView) LayoutInflater.from(this).inflate((int) R.layout.include_righttext_hi, (ViewGroup) null);
        this.l = (RefreshOnOverScrollListView) findViewById(R.id.listview);
        this.m = (HeaderLayout) findViewById(R.id.layout_header);
        this.m.setTitleText("招呼");
        this.n = g.o().inflate((int) R.layout.common_list_loadmore, (ViewGroup) null);
        this.n.setVisibility(8);
        this.s = (LoadingButton) this.n.findViewById(R.id.btn_loadmore);
        this.s.setNormalIconResId(R.drawable.ic_btn_inner_clock);
        this.i = this.k.e();
        this.j = this.k.d();
        v();
        this.s.setOnProcessListener(this);
        this.l.setOnItemClickListener(new by(this));
        this.v = new cd(this, this.l);
        this.v.a();
        this.v.a((int) R.id.item_layout);
        this.l.setMultipleSelector(this.v);
        this.v.a((cg) new bz(this));
        this.v.a(new a(this).a(), new ca(this));
        this.q = new m(this.l, this, new ArrayList());
        this.l.setAdapter((ListAdapter) this.q);
        this.l.addFooterView(this.n);
        this.m.a(this.p);
    }

    /* access modifiers changed from: protected */
    public final boolean a(Bundle bundle, String str) {
        String string = bundle.getString("remoteuserid");
        if ("actions.himessage".equals(str)) {
            c(string);
            w();
            return q();
        } else if (!"action.sessionchanged".equals(str)) {
            return super.a(bundle, str);
        } else {
            String string2 = bundle.getString("sessionid");
            int i2 = bundle.getInt("sessiontype", 0);
            if ("-2222".equals(string2) || i2 == 1) {
                return false;
            }
            c(string2);
            w();
            return false;
        }
    }

    /* access modifiers changed from: protected */
    public final void d() {
        ArrayList arrayList = (ArrayList) this.k.b(0);
        if (arrayList.isEmpty()) {
            this.t = new Date();
            return;
        }
        this.t = ((aw) arrayList.get(0)).a();
        if (arrayList.size() > 20) {
            arrayList.remove(arrayList.size() - 1);
            this.n.setVisibility(0);
        } else {
            this.l.removeFooterView(this.n);
        }
        a(arrayList);
        this.q.b((Collection) arrayList);
    }

    /* access modifiers changed from: protected */
    public final void e() {
        super.e();
        d();
        a(700, "actions.himessage");
        a(700, "action.sessionchanged");
        this.u = new e(this);
        this.u.a(new bx(this));
    }

    /* access modifiers changed from: protected */
    public final boolean k() {
        return true;
    }

    public void onBackPressed() {
        if (this.v.g()) {
            this.v.e();
        } else {
            super.onBackPressed();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.u != null) {
            unregisterReceiver(this.u);
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        this.k.f();
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("number", this.j);
            new k("PO", "P511", jSONObject).e();
        } catch (Exception e) {
        }
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        g.d().q();
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("number", this.j);
            new k("PI", "P511", jSONObject).e();
        } catch (Exception e) {
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        Bundle bundle = new Bundle();
        bundle.putString("sessionid", "-2222");
        bundle.putInt("sessiontype", 1);
        g.d().a(bundle, "action.sessionchanged");
    }

    public final void u() {
        b(new cd(this, (byte) 0));
    }
}
