package com.immomo.momo.android.activity.common;

import android.view.View;
import android.widget.AdapterView;
import com.immomo.momo.android.a.hp;

final class bo implements AdapterView.OnItemClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ba f1108a;

    bo(ba baVar) {
        this.f1108a = baVar;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        if (i < this.f1108a.P.getCount()) {
            hp hpVar = (hp) this.f1108a.P.getItem(i);
            if (hpVar.h == 0) {
                ba.m(this.f1108a).a(hpVar.e, 1);
            } else if (hpVar.h == hp.f862a) {
                ba.m(this.f1108a).a(hpVar.e, 2);
            }
        }
    }
}
