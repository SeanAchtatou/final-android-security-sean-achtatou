package com.immomo.momo.android.game;

import android.content.Context;
import android.view.View;
import com.immomo.momo.R;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.PaymentButton;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.g;
import com.immomo.momo.protocol.a.m;

final class s extends d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ k f2443a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public s(k kVar, Context context) {
        super(context);
        this.f2443a = kVar;
    }

    /* access modifiers changed from: protected */
    public final Object a(Object... objArr) {
        m.a().a(this.f2443a.V, this.f2443a.U, this.f2443a.O, this.f2443a.W);
        return null;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.f2443a.a(new v(this.f2443a.c(), "正在初始化..."));
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        super.a(exc);
        this.f2443a.T.setText("重试");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* access modifiers changed from: protected */
    public final void a(Object obj) {
        PaymentButton paymentButton;
        if (this.f2443a.W.f2407a.isEmpty()) {
            this.f2443a.T.setText("重试");
            return;
        }
        this.f2443a.S.setVisibility(0);
        this.f2443a.Q.setVisibility(0);
        this.f2443a.R.setVisibility(0);
        this.f2443a.T.setText("付款");
        this.f2443a.P.removeAllViews();
        for (int i = 0; i < this.f2443a.W.f2407a.size(); i++) {
            aj ajVar = (aj) this.f2443a.W.f2407a.get(i);
            if (i % 2 == 0) {
                View inflate = g.o().inflate((int) R.layout.listitem_recharge, this.f2443a.P, false);
                this.f2443a.P.addView(inflate);
                paymentButton = (PaymentButton) inflate.findViewById(R.id.btn_left);
            } else {
                paymentButton = (PaymentButton) this.f2443a.P.getChildAt(this.f2443a.P.getChildCount() - 1).findViewById(R.id.btn_right);
            }
            paymentButton.setVisibility(0);
            paymentButton.setLeftText(ajVar.c);
            paymentButton.setTag(ajVar);
            paymentButton.setOnClickListener(this.f2443a.ac);
            if (i == 0) {
                this.f2443a.W.b = ajVar;
                paymentButton.setSelected(true);
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.f2443a.N();
    }
}
