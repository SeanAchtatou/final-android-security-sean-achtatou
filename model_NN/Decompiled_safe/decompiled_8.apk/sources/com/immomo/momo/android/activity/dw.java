package com.immomo.momo.android.activity;

import android.view.View;

final class dw implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ FilterActivity f1263a;

    dw(FilterActivity filterActivity) {
        this.f1263a = filterActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.android.activity.FilterActivity.a(com.immomo.momo.android.activity.FilterActivity, boolean):void
     arg types: [com.immomo.momo.android.activity.FilterActivity, int]
     candidates:
      com.immomo.momo.android.activity.FilterActivity.a(com.immomo.momo.android.activity.FilterActivity, android.net.Uri):android.graphics.Bitmap
      com.immomo.momo.android.activity.FilterActivity.a(com.immomo.momo.android.activity.FilterActivity, int):void
      com.immomo.momo.android.activity.FilterActivity.a(com.immomo.momo.android.activity.FilterActivity, android.graphics.Bitmap):void
      com.immomo.momo.android.activity.FilterActivity.a(com.immomo.momo.android.activity.FilterActivity, com.immomo.momo.android.activity.eb):void
      com.immomo.momo.android.activity.FilterActivity.a(com.immomo.momo.android.activity.FilterActivity, boolean):void */
    public final void onClick(View view) {
        eb ebVar;
        try {
            this.f1263a.b.lock();
            this.f1263a.a(false);
            ec ecVar = (ec) view.getTag();
            if (!(ecVar == null || (ebVar = (eb) ecVar.f1269a) == null)) {
                this.f1263a.h = ecVar.b;
                this.f1263a.a();
                FilterActivity.a(this.f1263a, ebVar);
            }
        } catch (Exception e) {
        } finally {
            this.f1263a.b.unlock();
        }
    }
}
