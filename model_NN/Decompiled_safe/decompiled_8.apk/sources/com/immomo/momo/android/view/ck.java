package com.immomo.momo.android.view;

import android.text.InputFilter;
import android.text.Spanned;

final class ck implements InputFilter {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ NumberPicker f2763a;

    private ck(NumberPicker numberPicker) {
        this.f2763a = numberPicker;
    }

    /* synthetic */ ck(NumberPicker numberPicker, byte b) {
        this(numberPicker);
    }

    public final CharSequence filter(CharSequence charSequence, int i, int i2, Spanned spanned, int i3, int i4) {
        NumberPicker numberPicker = this.f2763a;
        NumberPicker.d();
        return this.f2763a.f.filter(charSequence, i, i2, spanned, i3, i4);
    }
}
