package com.immomo.momo.android.a;

import android.content.Context;
import android.content.Intent;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.broadcast.s;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.n;
import com.immomo.momo.service.bean.a.j;
import com.immomo.momo.service.y;
import com.immomo.momo.util.ao;
import java.util.ArrayList;

final class fi extends d {

    /* renamed from: a  reason: collision with root package name */
    private v f820a = null;
    private j c;
    private String d;
    private int e = 0;
    private String f = PoiTypeDef.All;
    private /* synthetic */ es g;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public fi(es esVar, Context context, j jVar, String str, int i, String str2) {
        super(context);
        this.g = esVar;
        this.c = jVar;
        this.d = str;
        this.e = i;
        this.f = str2;
        this.f820a = new v(context);
        this.f820a.setCancelable(true);
        this.f820a.setOnCancelListener(new fj(this));
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(this.c.f2979a);
        n.a().a(this.d, arrayList, this.e, this.f);
        return true;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
        this.g.d.a(this.f820a);
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        super.a(exc);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        Boolean bool = (Boolean) obj;
        super.a(bool);
        if (bool.booleanValue()) {
            a("操作成功");
            new y().b(this.c.f2979a, this.d);
            this.g.a(this.c);
            Intent intent = new Intent(s.f2362a);
            intent.putExtra("gid", this.d);
            this.g.d.sendBroadcast(intent);
            return;
        }
        ao.g(R.string.group_setting_quit_failed);
    }

    /* access modifiers changed from: protected */
    public final void b() {
        super.b();
        this.g.d.p();
    }
}
