package com.immomo.momo.android.activity.event;

import android.content.DialogInterface;

final class as implements DialogInterface.OnCancelListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ar f1362a;

    as(ar arVar) {
        this.f1362a = arVar;
    }

    public final void onCancel(DialogInterface dialogInterface) {
        this.f1362a.cancel(true);
    }
}
