package com.immomo.momo.android.a;

import android.graphics.drawable.Drawable;
import android.text.Html;

final class bi implements Html.ImageGetter {
    bi() {
    }

    public final Drawable getDrawable(String str) {
        Drawable createFromPath = Drawable.createFromPath(str);
        createFromPath.setBounds(0, 0, createFromPath.getIntrinsicWidth(), createFromPath.getIntrinsicHeight());
        return createFromPath;
    }
}
