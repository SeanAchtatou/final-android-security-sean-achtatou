package com.immomo.momo.android.activity;

import android.support.v4.b.a;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.b;
import java.util.List;

final class cd extends BaseAdapter {

    /* renamed from: a  reason: collision with root package name */
    private List f1061a = null;
    private /* synthetic */ EditIndustryActivity b;

    public cd(EditIndustryActivity editIndustryActivity, List list) {
        this.b = editIndustryActivity;
        this.f1061a = list;
    }

    public final int getCount() {
        return this.f1061a.size();
    }

    public final Object getItem(int i) {
        if (this.f1061a == null || this.f1061a.size() <= 0) {
            return null;
        }
        return this.f1061a.get(i);
    }

    public final long getItemId(int i) {
        return (long) i;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.b.a(java.lang.String, boolean):android.graphics.Bitmap
     arg types: [java.lang.String, int]
     candidates:
      com.immomo.momo.b.a(java.lang.String, android.graphics.Bitmap):android.graphics.Bitmap
      com.immomo.momo.b.a(java.lang.String, boolean):android.graphics.Bitmap */
    public final View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            cf cfVar = new cf((byte) 0);
            view = LayoutInflater.from(this.b).inflate((int) R.layout.listitem_industry, (ViewGroup) null);
            cfVar.f1063a = (ImageView) view.findViewById(R.id.imageview_icon);
            cfVar.b = (TextView) view.findViewById(R.id.tv_industry);
            cfVar.c = (RadioButton) view.findViewById(R.id.rb_check);
            cfVar.d = view.findViewById(R.id.view_line);
            view.setTag(R.id.tag_userlist_item, cfVar);
        }
        ce ceVar = (ce) this.f1061a.get(i);
        cf cfVar2 = (cf) view.getTag(R.id.tag_userlist_item);
        cfVar2.b.setText(ceVar.f1062a);
        cfVar2.c.setChecked(this.b.l == i);
        if (a.a((CharSequence) ceVar.c)) {
            cfVar2.f1063a.setImageBitmap(null);
        } else {
            cfVar2.f1063a.setImageBitmap(b.a(ceVar.b, false));
        }
        if (i == this.f1061a.size() - 1) {
            cfVar2.d.setVisibility(8);
        } else {
            cfVar2.d.setVisibility(0);
        }
        return view;
    }
}
