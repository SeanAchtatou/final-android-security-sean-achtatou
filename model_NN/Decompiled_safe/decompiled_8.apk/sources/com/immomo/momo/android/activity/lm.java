package com.immomo.momo.android.activity;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.android.a.a;

final class lm extends a {
    lm(Context context) {
        super(context);
    }

    public final int getCount() {
        return 30;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = a((int) R.layout.listitem_user);
        }
        ((TextView) view.findViewById(R.id.userlist_item_tv_name)).setText(new StringBuilder(String.valueOf(i)).toString());
        return view;
    }
}
