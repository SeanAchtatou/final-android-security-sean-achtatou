package com.immomo.momo.android.activity.group;

import android.content.Context;
import android.content.Intent;
import com.immomo.momo.R;
import com.immomo.momo.a.e;
import com.immomo.momo.android.broadcast.u;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.n;
import com.immomo.momo.service.bean.a.a;
import com.immomo.momo.util.ao;

final class aj extends d {

    /* renamed from: a  reason: collision with root package name */
    private v f1534a;
    /* access modifiers changed from: private */
    public /* synthetic */ EditGroupProfileActivity c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public aj(EditGroupProfileActivity editGroupProfileActivity, Context context) {
        super(context);
        this.c = editGroupProfileActivity;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        a aVar = new a();
        String a2 = n.a().a(this.c.m.b, this.c.w, this.c.x, !this.c.D, aVar);
        this.b.a((Object) ("isPass: " + this.c.D));
        if (android.support.v4.b.a.a((CharSequence) a2)) {
            return null;
        }
        if (!this.c.D) {
            EditGroupProfileActivity.b(this.c, aVar);
            Intent intent = new Intent(u.g);
            intent.putExtra("gid", this.c.m.b);
            this.c.sendBroadcast(intent);
            Intent intent2 = new Intent(u.e);
            intent2.putExtra("gid", this.c.m.b);
            this.c.sendBroadcast(intent2);
        }
        this.c.p.k(this.c.m.b);
        return a2;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
        this.f1534a = new v(this.c);
        this.f1534a.a("资料提交中");
        this.f1534a.setCancelable(true);
        this.f1534a.setOnCancelListener(new ak(this));
        this.c.a(this.f1534a);
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        if (exc instanceof e) {
            this.b.a((Throwable) exc);
            ao.g(R.string.errormsg_network_normal400);
            return;
        }
        super.a(exc);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        String str = (String) obj;
        if (!android.support.v4.b.a.a((CharSequence) str)) {
            com.immomo.momo.android.view.a.n.b(this.c, str, new al(this)).show();
        } else {
            this.c.finish();
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
        super.b();
        this.c.p();
    }
}
