package com.immomo.momo.android.activity.group;

import android.content.Context;
import com.immomo.momo.a.h;
import com.immomo.momo.android.c.d;
import com.immomo.momo.protocol.a.n;

final class cl extends d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ GroupPartyActivity f1585a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public cl(GroupPartyActivity groupPartyActivity, Context context) {
        super(context);
        this.f1585a = groupPartyActivity;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        n.a().c(this.f1585a.t);
        this.f1585a.w.a(this.f1585a.t);
        return 0;
    }

    /* access modifiers changed from: protected */
    public final void a() {
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        if ((exc instanceof h) || (exc instanceof com.immomo.momo.a.n)) {
            a(exc.getMessage());
            this.f1585a.w.a(this.f1585a.u);
            this.f1585a.setResult(35);
            this.f1585a.finish();
            return;
        }
        super.a(exc);
    }

    /* access modifiers changed from: protected */
    public final void a(Object obj) {
        boolean unused = this.f1585a.y();
        this.f1585a.A();
        GroupPartyActivity.g(this.f1585a);
    }

    /* access modifiers changed from: protected */
    public final void b() {
        GroupPartyActivity groupPartyActivity = this.f1585a;
        GroupPartyActivity.w();
    }
}
