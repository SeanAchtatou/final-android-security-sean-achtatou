package com.immomo.momo.android.activity.contacts;

import android.content.Intent;
import android.support.v4.b.a;
import com.immomo.momo.android.broadcast.d;

final class c implements d {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ a f1181a;

    c(a aVar) {
        this.f1181a = aVar;
    }

    public final void a(Intent intent) {
        String str = intent.getExtras() != null ? (String) intent.getExtras().get("momoid") : null;
        if (!a.a((CharSequence) str) && this.f1181a.P != null) {
            new Thread(new d(this, str)).start();
        }
    }
}
