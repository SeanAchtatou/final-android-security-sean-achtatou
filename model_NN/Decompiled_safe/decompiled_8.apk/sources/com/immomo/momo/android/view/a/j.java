package com.immomo.momo.android.view.a;

import android.database.DataSetObserver;

final class j extends DataSetObserver {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ e f2698a;

    private j(e eVar) {
        this.f2698a = eVar;
    }

    /* synthetic */ j(e eVar, byte b) {
        this(eVar);
    }

    public final void onChanged() {
        if (this.f2698a.g()) {
            this.f2698a.d();
        }
    }

    public final void onInvalidated() {
        this.f2698a.e();
    }
}
