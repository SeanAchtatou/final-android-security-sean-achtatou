package com.immomo.momo.android.a.a;

import android.view.View;
import android.view.ViewGroup;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.message.a;
import com.immomo.momo.service.bean.Message;
import com.immomo.momo.util.m;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public final class af extends aa {
    public Map d;
    private Date g = null;
    private Map h;

    public af(a aVar) {
        super(aVar);
        new m("test_momo", "[ --- from MultiChatMessageAdapter --- ]").a();
        this.h = null;
        this.d = null;
        ab.m = null;
        new HashMap();
        this.h = new HashMap();
        this.d = new HashMap();
        ab.j = new ArrayList();
        this.f = new HashMap();
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        ab abVar;
        Message message = (Message) getItem(i);
        if (this.g == null) {
            this.g = ((Message) this.f671a.get(0)).timestamp;
        }
        if (view == null) {
            abVar = ab.a(message.chatType, message.contentType, message.receive, e());
            view = abVar.g;
            view.setTag(R.id.tag_messageadapter, abVar);
        } else {
            abVar = (ab) view.getTag(R.id.tag_messageadapter);
        }
        abVar.a(message);
        if (message.receive && message.contentType == 4 && !message.isAudioPlayed) {
            this.h.put(message.msgId, abVar);
        }
        if (message.status == 7) {
            this.d.put(message.msgId, abVar);
        }
        if (((Float) this.f.get(message.msgId)) != null) {
            abVar.g();
        } else {
            abVar.h();
        }
        return view;
    }
}
