package com.immomo.momo.android.pay;

import android.content.Context;
import com.immomo.momo.R;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.a;
import com.immomo.momo.service.aq;
import com.immomo.momo.service.bean.bg;

final class m extends d {

    /* renamed from: a  reason: collision with root package name */
    private v f2564a = null;
    private String c;
    private bg d = new bg();
    private int e = 0;
    private /* synthetic */ BuyMemberActivity f;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public m(BuyMemberActivity buyMemberActivity, Context context, String str, int i) {
        super(context);
        this.f = buyMemberActivity;
        this.c = str;
        this.f2564a = new v(context);
        this.e = i;
        if (buyMemberActivity.E != null && !buyMemberActivity.E.isCancelled()) {
            buyMemberActivity.E.cancel(true);
        }
        buyMemberActivity.E = this;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        String a2 = a.a().a(this.c, this.d);
        if (this.d.f3012a) {
            this.f.f.b(this.d.b);
            this.f.f.ad = this.d.e;
            this.f.f.ai = this.d.d;
            this.f.f.aj = android.support.v4.b.a.a(this.d.c);
            new aq().b(this.f.f);
        }
        return a2;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
        this.f2564a.setCancelable(false);
        this.f2564a.a("正在验证...");
        this.f.a(this.f2564a);
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        super.a(exc);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.android.pay.BuyMemberActivity.b(com.immomo.momo.android.pay.BuyMemberActivity, boolean):void
     arg types: [com.immomo.momo.android.pay.BuyMemberActivity, int]
     candidates:
      com.immomo.momo.android.pay.BuyMemberActivity.b(com.immomo.momo.android.pay.BuyMemberActivity, java.lang.String):void
      com.immomo.momo.android.pay.BuyMemberActivity.b(java.lang.String, int):void
      com.immomo.momo.android.pay.BuyMemberActivity.b(com.immomo.momo.android.pay.BuyMemberActivity, boolean):void */
    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        String str = (String) obj;
        this.f.x();
        if (this.d.f3012a) {
            this.f.z();
            this.f.C = false;
            this.f.t.setText((int) R.string.payvip_buy);
            a(str);
        } else if (this.e < 4) {
            try {
                Thread.sleep(2000);
            } catch (Throwable th) {
            }
            this.e++;
            a("验证，第" + this.e + "次请求");
            new m(this.f, this.f.n(), this.c, this.e).execute(new Object[0]);
        } else {
            this.f.C = true;
            a("验证失败，请稍候重新验证。");
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.f.p();
    }
}
