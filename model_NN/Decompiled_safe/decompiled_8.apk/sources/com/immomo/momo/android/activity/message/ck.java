package com.immomo.momo.android.activity.message;

import android.content.Intent;
import android.support.v4.b.a;
import android.view.View;
import android.widget.AdapterView;
import com.immomo.momo.android.activity.common.InviteToDiscussTabsActivity;
import com.immomo.momo.service.bean.o;

final class ck implements AdapterView.OnItemClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ MultiChatActivity f1971a;
    private final /* synthetic */ String[] b;

    ck(MultiChatActivity multiChatActivity, String[] strArr) {
        this.f1971a = multiChatActivity;
        this.b = strArr;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        if ("关闭提醒".equals(this.b[i]) || "开启提醒".equals(this.b[i])) {
            if (this.f1971a.V != null) {
                o h = this.f1971a.V;
                o h2 = this.f1971a.V;
                boolean z = !this.f1971a.V.f3033a;
                h2.f3033a = z;
                h.a("discuss_ispush", Boolean.valueOf(z));
            }
        } else if ("语音收听方式".equals(this.b[i])) {
            this.f1971a.ae();
        } else if ("邀请好友加入".equals(this.b[i])) {
            if (!a.a((CharSequence) this.f1971a.P.f)) {
                Intent intent = new Intent(this.f1971a, InviteToDiscussTabsActivity.class);
                intent.putExtra("did", this.f1971a.P.f);
                this.f1971a.startActivity(intent);
            }
        } else if ("修改对话名称".equals(this.b[i])) {
            MultiChatActivity.i(this.f1971a);
        } else if ("设置聊天背景".equals(this.b[i])) {
            ChatBGSettingActivity.a(this.f1971a, this.f1971a.P.l);
        }
    }
}
