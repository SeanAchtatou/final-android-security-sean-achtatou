package com.immomo.momo.android.activity;

import android.content.Intent;
import android.net.Uri;
import android.webkit.DownloadListener;

final class gd implements DownloadListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ NewVersionActivity f1511a;

    gd(NewVersionActivity newVersionActivity) {
        this.f1511a = newVersionActivity;
    }

    public final void onDownloadStart(String str, String str2, String str3, String str4, long j) {
        this.f1511a.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
    }
}
