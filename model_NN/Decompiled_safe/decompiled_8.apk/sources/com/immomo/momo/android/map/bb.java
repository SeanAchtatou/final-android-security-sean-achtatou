package com.immomo.momo.android.map;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import com.amap.mapapi.map.MapView;
import com.amap.mapapi.map.MyLocationOverlay;
import com.immomo.momo.R;

final class bb extends MyLocationOverlay {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ SelectSiteAMapActivity f2481a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public bb(SelectSiteAMapActivity selectSiteAMapActivity, Context context, MapView mapView) {
        super(context, mapView);
        this.f2481a = selectSiteAMapActivity;
    }

    public final boolean draw(Canvas canvas, MapView mapView, boolean z, long j) {
        super.draw(canvas, mapView, z, j);
        if (this.f2481a.f == null) {
            return false;
        }
        Point pixels = mapView.getProjection().toPixels(this.f2481a.f, null);
        if (this.f2481a.g == null || this.f2481a.g.isRecycled()) {
            this.f2481a.g = BitmapFactory.decodeResource(this.f2481a.getResources(), R.drawable.ic_map_pin);
        }
        canvas.drawBitmap(this.f2481a.g, (float) (pixels.x - (this.f2481a.g.getWidth() / 2)), (float) (pixels.y - (this.f2481a.g.getHeight() / 2)), (Paint) null);
        return true;
    }
}
