package com.immomo.momo.android.activity.tieba;

import android.content.Context;
import com.immomo.momo.a.h;
import com.immomo.momo.android.c.d;
import com.immomo.momo.protocol.a.v;
import com.immomo.momo.service.bean.c.b;
import com.immomo.momo.util.ao;

final class cm extends d {

    /* renamed from: a  reason: collision with root package name */
    private String f2214a;
    private /* synthetic */ TieDetailActivity c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public cm(TieDetailActivity tieDetailActivity, Context context, String str) {
        super(context);
        this.c = tieDetailActivity;
        this.f2214a = str;
        if (tieDetailActivity.p != null && !tieDetailActivity.p.isCancelled()) {
            tieDetailActivity.p.cancel(true);
        }
        tieDetailActivity.p = this;
    }

    private boolean c() {
        if (!this.c.r) {
            return false;
        }
        TieDetailActivity tieDetailActivity = this.c;
        tieDetailActivity.s = tieDetailActivity.s | 1;
        return TieDetailActivity.d(this.c);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        this.b.b((Object) "加载话题内容 - executeTask");
        return v.a().i(this.f2214a);
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        this.b.c("加载话题内容 - onTaskError");
        if (this.c.q != null && !this.c.q.isCancelled()) {
            this.c.q.cancel(true);
        }
        this.c.y();
        c();
        if (exc instanceof h) {
            ao.b(exc.getMessage());
            this.c.finish();
            return;
        }
        super.a(exc);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        b bVar = (b) obj;
        this.b.b((Object) "加载话题内容 - onTaskSuccess");
        this.c.o.a(bVar);
        this.c.N.a(this.c.m, this.c.n);
        if (bVar.o == 2) {
            a("该话题已经被删除");
            this.c.finish();
            return;
        }
        this.c.i = bVar;
        this.c.a(this.c.i);
        this.c.b(this.c.i);
        TieDetailActivity.d(this.c, this.c.i);
        this.c.c(this.c.i);
        c();
        this.c.P.lock();
        try {
            this.c.Q.signal();
        } finally {
            this.c.P.unlock();
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.b.b((Object) "加载话题内容 - onTaskFinish");
        this.c.p = (cm) null;
    }

    /* access modifiers changed from: protected */
    public final void onCancelled() {
        this.b.b((Object) "加载话题内容 - onCancelled");
        c();
    }
}
