package com.immomo.momo.android.activity.maintab;

import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.w;
import android.support.v4.b.a;
import android.view.View;
import android.view.ViewGroup;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.ah;
import com.immomo.momo.android.activity.contacts.OpenContactActivity;
import com.immomo.momo.android.activity.contacts.al;
import com.immomo.momo.android.activity.d;
import com.immomo.momo.android.activity.f;
import com.immomo.momo.android.activity.lb;
import com.immomo.momo.android.activity.lh;
import com.immomo.momo.android.b.z;
import com.immomo.momo.android.c.ag;
import com.immomo.momo.android.c.u;
import com.immomo.momo.android.service.Cleaner;
import com.immomo.momo.android.service.LService;
import com.immomo.momo.g;
import com.immomo.momo.service.ai;
import com.immomo.momo.service.aq;
import com.immomo.momo.util.k;

public class MaintabActivity extends ah implements f {
    public static final int h = ((int) g.l().getDimension(R.dimen.maintabbottomtabbar));
    private static boolean l = true;
    String i = PoiTypeDef.All;
    Handler j = new d(this);
    final Object[][] k;
    /* access modifiers changed from: private */
    public lb m = null;
    private ViewGroup n;
    /* access modifiers changed from: private */
    public lh o;
    private ServiceConnection p = null;
    /* access modifiers changed from: private */
    public int q = 0;
    private boolean r;
    private View.OnClickListener s;

    public MaintabActivity() {
        Object[] objArr = new Object[5];
        objArr[0] = aq.class;
        objArr[1] = true;
        objArr[2] = Integer.valueOf((int) R.id.maintab_layout_1);
        Object[] objArr2 = new Object[5];
        objArr2[0] = a.class;
        objArr2[1] = true;
        objArr2[2] = Integer.valueOf((int) R.id.maintab_layout_2);
        Object[] objArr3 = new Object[5];
        objArr3[0] = bh.class;
        objArr3[1] = true;
        objArr3[2] = Integer.valueOf((int) R.id.maintab_layout_3);
        Object[] objArr4 = new Object[5];
        objArr4[0] = al.class;
        objArr4[1] = false;
        objArr4[2] = Integer.valueOf((int) R.id.maintab_layout_4);
        Object[] objArr5 = new Object[5];
        objArr5[0] = cd.class;
        objArr5[1] = true;
        objArr5[2] = Integer.valueOf((int) R.id.maintab_layout_5);
        this.k = new Object[][]{objArr, objArr2, objArr3, objArr4, objArr5};
        this.s = new e(this);
    }

    private int a(Intent intent) {
        if (intent.getExtras() != null) {
            int i2 = intent.getExtras().getInt("tabindex", this.q);
            this.i = (String) intent.getExtras().get("source");
            intent.putExtra("source", PoiTypeDef.All);
            if (i2 != this.q && i2 < this.k.length && i2 >= 0) {
                return i2;
            }
        } else {
            this.i = null;
        }
        return this.q;
    }

    /* access modifiers changed from: private */
    public void a(lh lhVar, w wVar) {
        if (lhVar != this.o) {
            this.e.a((Object) ("showFragment, fragment=" + lhVar.getClass().getName()));
            for (int i2 = 0; i2 < this.k.length; i2++) {
                View view = (View) this.k[i2][4];
                if (lhVar == this.k[i2][3]) {
                    view.setSelected(true);
                    this.q = i2;
                } else {
                    view.setSelected(false);
                }
            }
            if (wVar == null) {
                wVar = c().a();
            }
            lh lhVar2 = this.o;
            this.o = lhVar;
            if (lhVar2 != null) {
                lhVar2.af();
                wVar.c(lhVar2);
            }
            if (lhVar.J()) {
                lhVar.ad();
            }
            if (!lhVar.T()) {
                if (lhVar.J()) {
                    lhVar.S();
                } else {
                    lhVar.U();
                }
            }
            wVar.d(this.o);
            wVar.c();
        }
    }

    /* access modifiers changed from: private */
    public void b(boolean z) {
        this.n = (ViewGroup) findViewById(R.id.tabwidget);
        w a2 = c().a();
        for (int length = this.k.length - 1; length >= 0; length--) {
            Class cls = (Class) this.k[length][0];
            int intValue = ((Integer) this.k[length][2]).intValue();
            lh a3 = lh.a(this, cls, ((Boolean) this.k[length][1]).booleanValue());
            a3.d(false);
            this.k[length][3] = a3;
            a2.a(R.id.tabcontent, a3, cls.getName());
            View findViewById = this.n.findViewById(intValue);
            this.k[length][4] = findViewById;
            a3.a(findViewById);
            findViewById.setOnClickListener(this.s);
            if (z) {
                a2.c(a3);
            }
        }
        a2.c();
        this.q = a(getIntent());
        this.r = true;
    }

    private boolean c(int i2) {
        if (!this.r || i2 < 0 || i2 >= this.k.length) {
            return false;
        }
        a((lh) this.k[i2][3], (w) null);
        return true;
    }

    private void u() {
        this.n = (ViewGroup) findViewById(R.id.tabwidget);
        w a2 = c().a();
        for (int length = this.k.length - 1; length >= 0; length--) {
            Class cls = (Class) this.k[length][0];
            int intValue = ((Integer) this.k[length][2]).intValue();
            lh lhVar = (lh) c().a(cls.getName());
            if (lhVar == null) {
                lhVar = lh.a(this, cls, ((Boolean) this.k[length][1]).booleanValue());
            }
            this.k[length][3] = lhVar;
            View findViewById = this.n.findViewById(intValue);
            this.k[length][4] = findViewById;
            lhVar.a(findViewById);
            findViewById.setOnClickListener(this.s);
            a2.c(lhVar);
        }
        a2.c();
        this.r = true;
    }

    /* access modifiers changed from: private */
    public void v() {
        if (!this.g.c && !((Boolean) this.g.b("alertcontact", false)).booleanValue()) {
            startActivity(new Intent(getApplicationContext(), OpenContactActivity.class));
            this.g.c("alertcontact", true);
        }
    }

    public final void a() {
        if (!isFinishing() && !isDestroyed()) {
            this.e.a((Object) "!!!!!!!!!!!!!!!!!onAppExit....");
            if (this.p != null) {
                try {
                    z.c();
                    unbindService(this.p);
                } catch (Exception e) {
                }
                stopService(new Intent(getApplicationContext(), LService.class));
            }
            new k("M", "M3").e();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.android.activity.d.a(java.lang.String, com.immomo.momo.android.activity.f):void
     arg types: [java.lang.String, com.immomo.momo.android.activity.maintab.MaintabActivity]
     candidates:
      com.immomo.momo.android.activity.d.a(java.lang.String, android.content.Context):boolean
      com.immomo.momo.android.activity.d.a(java.lang.String, com.immomo.momo.android.activity.f):void */
    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        setContentView((int) R.layout.activity_maintabs);
        new aq().a(this.f, this.f.h);
        if (!a.a((CharSequence) this.f.ag) && this.f.ah == null) {
            this.f.ah = new ai().a(this.f.ag);
        }
        b(new j(this, this));
        b(new i(this, (byte) 0));
        d.a(getClass().getName(), (f) this);
        ag.a();
        com.immomo.momo.android.c.k.a();
        this.p = new com.immomo.momo.util.al();
        bindService(new Intent(getApplicationContext(), LService.class), this.p, 1);
        t().d();
        this.e.a((Object) ("sessionid=" + this.f.W));
        if (bundle != null) {
            u();
            c(bundle.getInt("tabindex", 0));
        } else if (l) {
            l = false;
            this.m = new lb(this);
            this.m.a();
            this.m.a(new f(this, System.currentTimeMillis()));
        } else {
            b(true);
            c(this.q);
        }
    }

    public final void b() {
        this.e.a((Object) "onAppEnter....");
        this.p = new com.immomo.momo.util.al();
        bindService(new Intent(getApplicationContext(), LService.class), this.p, 1);
        t().d();
        new k("M", "M2").e();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.Integer]
     candidates:
      com.immomo.momo.util.ak.a(android.content.Context, java.lang.String):com.immomo.momo.util.ak
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Integer):int
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Long):long
      com.immomo.momo.util.ak.a(java.lang.String, int):void
      com.immomo.momo.util.ak.a(java.lang.String, long):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.String):void
      com.immomo.momo.util.ak.a(java.lang.String, boolean):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Boolean):boolean
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void */
    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        if (i2 == 321) {
            t().f668a.a("guide_", (Object) Integer.valueOf(g.Q()));
            v();
        }
    }

    public void onBackPressed() {
        if (this.o == null || !this.o.J() || !this.o.G()) {
            super.onBackPressed();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        new k("M", "M3").e();
        try {
            u.h();
            d.c();
            com.immomo.momo.android.c.k.b();
            System.gc();
            new Thread(new h(this)).start();
            z.a();
            unbindService(this.p);
        } catch (Exception e) {
            this.e.a((Throwable) e);
        }
        this.i = null;
        if (Math.abs(System.currentTimeMillis() - t().f668a.a("last_cleartime", 0L)) >= 86400000) {
            startService(new Intent(getApplicationContext(), Cleaner.class));
        }
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        this.q = a(intent);
        if (this.r) {
            c(this.q);
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (this.o != null && this.o.J()) {
            this.o.af();
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.o != null && this.o.J() && !this.o.M()) {
            this.o.ad();
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putInt("tabindex", this.q);
    }
}
