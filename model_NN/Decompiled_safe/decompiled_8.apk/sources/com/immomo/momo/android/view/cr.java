package com.immomo.momo.android.view;

import android.view.View;

final class cr implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ PublishSelectPhotoView f2767a;
    private final /* synthetic */ int b;

    cr(PublishSelectPhotoView publishSelectPhotoView, int i) {
        this.f2767a = publishSelectPhotoView;
        this.b = i;
    }

    public final void onClick(View view) {
        if (this.f2767a.i != null) {
            this.f2767a.i.a(this.b, view);
        }
    }
}
