package com.immomo.momo.android.broadcast;

import android.content.Context;
import com.immomo.momo.g;

public final class j extends b {

    /* renamed from: a  reason: collision with root package name */
    public static final String f2353a = (String.valueOf(g.h()) + ".action.friendlist.addfriend");
    public static final String b = (String.valueOf(g.h()) + ".action.friendlist.deletefriend");
    public static final String c = (String.valueOf(g.h()) + ".action.friendlist.deletefans");
    public static final String d = (String.valueOf(g.h()) + ".action.friendlist.addfans");
    private static String e = (String.valueOf(g.h()) + ".action.friendlist.reflushcount");
    private static String f = (String.valueOf(g.h()) + ".action.friendlist.refresh");

    public j(Context context) {
        super(context);
        a(f2353a, b, c, d, f, e);
    }
}
