package com.immomo.momo.android.activity.emotestore;

import android.content.Intent;
import android.view.View;

final class au implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ MainEmotionActivity f1298a;

    au(MainEmotionActivity mainEmotionActivity) {
        this.f1298a = mainEmotionActivity;
    }

    public final void onClick(View view) {
        this.f1298a.startActivity(new Intent(this.f1298a, MineEmotesActivity.class));
    }
}
