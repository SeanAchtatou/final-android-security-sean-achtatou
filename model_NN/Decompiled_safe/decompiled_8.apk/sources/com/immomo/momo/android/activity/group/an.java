package com.immomo.momo.android.activity.group;

import android.view.View;
import com.immomo.momo.android.view.a.n;
import java.util.List;

final class an implements View.OnClickListener {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public /* synthetic */ GroupActionListFragment f1538a;

    an(GroupActionListFragment groupActionListFragment) {
        this.f1538a = groupActionListFragment;
    }

    public final void onClick(View view) {
        List d = this.f1538a.aa.d();
        if (!d.isEmpty()) {
            n.a(this.f1538a.c(), "确认删除所选的动态吗？", new ao(this, d)).show();
        }
    }
}
