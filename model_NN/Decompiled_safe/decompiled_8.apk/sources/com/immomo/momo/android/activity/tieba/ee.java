package com.immomo.momo.android.activity.tieba;

import android.content.Context;
import com.immomo.momo.android.a.is;
import com.immomo.momo.android.c.d;
import com.immomo.momo.protocol.a.a.e;
import com.immomo.momo.protocol.a.v;

final class ee extends d {

    /* renamed from: a  reason: collision with root package name */
    private e f2257a;
    private /* synthetic */ dw c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ee(dw dwVar, Context context) {
        super(context);
        this.c = dwVar;
        if (dwVar.Y != null && !dwVar.Y.isCancelled()) {
            dwVar.Y.cancel(true);
        }
        dwVar.Y = this;
    }

    /* access modifiers changed from: protected */
    public final Object a(Object... objArr) {
        this.f2257a = v.a().e();
        dw.a(this.c, this.f2257a);
        return null;
    }

    /* access modifiers changed from: protected */
    public final void a(Object obj) {
        this.c.V = new is(this.c, this.c.W, this.c.O, this.c.Q, this.c.R, this.c.S, this.c.T);
        this.c.O.setAdapter(this.c.V);
        this.c.V.a();
        this.c.Z.j();
        dw.j(this.c);
        if (this.f2257a.c + this.f2257a.b == 0 && ((Boolean) this.c.N.b("key_tiebaguid", true)).booleanValue()) {
            this.c.P();
            this.c.N.c("key_tiebaguid", false);
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.c.O.i();
    }
}
