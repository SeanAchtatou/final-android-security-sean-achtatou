package com.immomo.momo.android.activity;

import android.text.Editable;
import android.text.TextWatcher;

final class g implements TextWatcher {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ AddFriendActivity f1508a;

    g(AddFriendActivity addFriendActivity) {
        this.f1508a = addFriendActivity;
    }

    public final void afterTextChanged(Editable editable) {
        if (this.f1508a.k.getText().toString().trim().length() >= 4) {
            this.f1508a.j.setEnabled(true);
        } else {
            this.f1508a.j.setEnabled(false);
        }
    }

    public final void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public final void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }
}
