package com.immomo.momo.android.a;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v4.b.a;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.ImageBrowserActivity;
import com.immomo.momo.android.activity.OtherProfileActivity;
import com.immomo.momo.android.activity.ao;
import com.immomo.momo.android.activity.d;
import com.immomo.momo.android.activity.emotestore.EmotionProfileActivity;
import com.immomo.momo.android.activity.feed.FeedProfileActivity;
import com.immomo.momo.android.c.u;
import com.immomo.momo.android.view.AltImageView;
import com.immomo.momo.android.view.EmoteTextView;
import com.immomo.momo.android.view.HandyListView;
import com.immomo.momo.android.view.MultiImageView;
import com.immomo.momo.android.view.a.at;
import com.immomo.momo.android.view.cb;
import com.immomo.momo.b;
import com.immomo.momo.g;
import com.immomo.momo.service.ai;
import com.immomo.momo.service.bean.GameApp;
import com.immomo.momo.service.bean.ab;
import com.immomo.momo.service.bean.ac;
import com.immomo.momo.service.bean.aj;
import com.immomo.momo.service.bean.q;
import com.immomo.momo.util.j;
import com.immomo.momo.util.k;
import com.immomo.momo.util.m;
import java.io.File;
import java.lang.ref.SoftReference;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class cs extends a implements View.OnClickListener, cb {
    private static Map j = new HashMap(24);
    /* access modifiers changed from: private */
    public ai d;
    /* access modifiers changed from: private */
    public ao e;
    private HandyListView f;
    private int g;
    private m h;
    private String i;

    public cs(Activity activity, List list, HandyListView handyListView) {
        super(activity, list);
        this.d = null;
        this.e = null;
        this.f = null;
        this.g = 0;
        this.h = new m(this);
        this.i = g.q().h;
        this.g = g.n();
        this.e = (ao) activity;
        this.d = new ai();
        this.f = handyListView;
    }

    private void a(ab abVar, ImageView imageView) {
        SoftReference softReference = (SoftReference) j.get(abVar.j);
        Bitmap bitmap = softReference == null ? null : (Bitmap) softReference.get();
        if (bitmap != null && bitmap.isRecycled()) {
            j.remove(bitmap);
            bitmap = null;
        }
        if (bitmap != null) {
            bitmap = (Bitmap) ((SoftReference) j.get(abVar.j)).get();
        } else if (!this.f.g()) {
            File a2 = q.a(abVar.j, abVar.k);
            if (a2.exists() && (bitmap = a.l(a2.getPath())) != null) {
                j.put(abVar.j, new SoftReference(bitmap));
            }
            if (bitmap == null && !abVar.isImageLoading()) {
                abVar.setImageLoading(true);
                u.b().execute(new cw(this, abVar));
            }
        }
        if (bitmap != null) {
            imageView.setImageBitmap(bitmap);
        } else {
            imageView.setImageDrawable(null);
        }
    }

    public final void a(String str) {
        if (!com.immomo.a.a.f.a.a(str)) {
            c(new ab(str));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public final void b(int i2, String[] strArr) {
        Intent intent = new Intent(this.e, ImageBrowserActivity.class);
        intent.putExtra("array", strArr);
        intent.putExtra("imagetype", "feed");
        intent.putExtra("autohide_header", true);
        intent.putExtra("index", i2);
        this.e.startActivity(intent);
        if (this.e.getParent() != null) {
            this.e.getParent().overridePendingTransition(R.anim.zoom_enter, R.anim.normal);
        } else {
            this.e.overridePendingTransition(R.anim.zoom_enter, R.anim.normal);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.b.a(java.lang.String, boolean):android.graphics.Bitmap
     arg types: [java.lang.String, int]
     candidates:
      com.immomo.momo.b.a(java.lang.String, android.graphics.Bitmap):android.graphics.Bitmap
      com.immomo.momo.b.a(java.lang.String, boolean):android.graphics.Bitmap */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, com.immomo.momo.android.view.HandyListView):void
     arg types: [com.immomo.momo.service.bean.bf, android.widget.ImageView, com.immomo.momo.android.view.HandyListView]
     candidates:
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, int):void
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup):void
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, com.immomo.momo.android.view.HandyListView):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup, int, boolean, boolean, int):void
     arg types: [com.immomo.momo.service.bean.al, android.widget.ImageView, com.immomo.momo.android.view.HandyListView, int, int, int, int]
     candidates:
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, int, android.view.View, android.view.ViewGroup, boolean, int, boolean):com.immomo.momo.android.c.o
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup, int, boolean, boolean, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup, int, boolean, boolean, int):void
     arg types: [com.immomo.momo.service.bean.ab, com.immomo.momo.android.view.AltImageView, com.immomo.momo.android.view.HandyListView, int, int, int, int]
     candidates:
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, int, android.view.View, android.view.ViewGroup, boolean, int, boolean):com.immomo.momo.android.c.o
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup, int, boolean, boolean, int):void */
    public final View getView(int i2, View view, ViewGroup viewGroup) {
        cz czVar;
        if (view == null) {
            view = g.o().inflate((int) R.layout.listitem_feed, (ViewGroup) null);
            cz czVar2 = new cz((byte) 0);
            view.setTag(R.id.tag_userlist_item, czVar2);
            czVar2.f767a = (TextView) view.findViewById(R.id.tv_feed_time);
            czVar2.b = (TextView) view.findViewById(R.id.tv_feed_site);
            czVar2.c = view.findViewById(R.id.layout_feed_site);
            czVar2.d = (EmoteTextView) view.findViewById(R.id.tv_feed_content);
            czVar2.e = (AltImageView) view.findViewById(R.id.iv_feed_content);
            czVar2.f = (MultiImageView) view.findViewById(R.id.mv_feed_content);
            czVar2.g = (ImageView) view.findViewById(R.id.iv_feed_photo);
            czVar2.h = (TextView) view.findViewById(R.id.tv_feed_name);
            czVar2.j = (TextView) view.findViewById(R.id.tv_feed_commentcount);
            czVar2.k = view.findViewById(R.id.layout_feed_commentcount);
            czVar2.l = view.findViewById(R.id.bt_feed_more);
            czVar2.i = view.findViewById(R.id.layout_feed_content);
            czVar2.C = view.findViewById(R.id.userlist_item_layout_badgeContainer);
            czVar2.s = (ImageView) view.findViewById(R.id.userlist_item_iv_gender);
            czVar2.A = view.findViewById(R.id.userlist_item_layout_genderbackgroud);
            czVar2.t = (ImageView) view.findViewById(R.id.userlist_item_pic_iv_weibo);
            czVar2.u = (ImageView) view.findViewById(R.id.userlist_item_pic_iv_tweibo);
            czVar2.z = (ImageView) view.findViewById(R.id.userlist_item_pic_iv_group_role);
            czVar2.v = (ImageView) view.findViewById(R.id.userlist_item_pic_iv_renren);
            czVar2.w = (ImageView) view.findViewById(R.id.userlist_item_pic_iv_vip);
            czVar2.x = (ImageView) view.findViewById(R.id.userlist_item_pic_iv_multipic);
            czVar2.y = (ImageView) view.findViewById(R.id.userlist_item_pic_iv_industry);
            czVar2.B = (TextView) view.findViewById(R.id.userlist_item_tv_age);
            czVar2.m = view.findViewById(R.id.feed_layout_app);
            czVar2.n = (ImageView) czVar2.m.findViewById(R.id.feed_iv_appicon);
            czVar2.p = (TextView) czVar2.m.findViewById(R.id.feed_tv_appdesc);
            czVar2.o = (TextView) czVar2.m.findViewById(R.id.feed_tv_apptitle);
            czVar2.r = view.findViewById(R.id.feed_view_app_border_bottom);
            czVar2.q = view.findViewById(R.id.feed_view_app_border_top);
            czVar2.D = view.findViewById(R.id.layout_feed_userinfo);
            czVar2.g.setOnClickListener(this);
            czVar2.i.setOnClickListener(this);
            czVar2.e.setOnClickListener(this);
            czVar2.D.setOnClickListener(this);
            czVar2.m.setOnClickListener(this);
            czVar = czVar2;
        } else {
            czVar = (cz) view.getTag(R.id.tag_userlist_item);
        }
        ab abVar = (ab) getItem(i2);
        czVar.g.setTag(R.id.tag_item_position, Integer.valueOf(i2));
        czVar.D.setTag(R.id.tag_item_position, Integer.valueOf(i2));
        czVar.e.setTag(R.id.tag_item_position, Integer.valueOf(i2));
        czVar.i.setTag(R.id.tag_item_position, Integer.valueOf(i2));
        czVar.k.setTag(R.id.tag_item_position, Integer.valueOf(i2));
        czVar.l.setTag(R.id.tag_item_position, Integer.valueOf(i2));
        czVar.b.setTag(R.id.tag_item_position, Integer.valueOf(i2));
        czVar.c.setTag(R.id.tag_item_position, Integer.valueOf(i2));
        czVar.m.setTag(R.id.tag_item_position, Integer.valueOf(i2));
        if (abVar.b != null) {
            czVar.h.setText(abVar.b.h());
            if (abVar.b.b()) {
                czVar.h.setTextColor(g.c((int) R.color.font_vip_name));
            } else {
                czVar.h.setTextColor(g.c((int) R.color.font_value));
            }
            czVar.C.setVisibility(0);
            czVar.B.setText(new StringBuilder(String.valueOf(abVar.b.I)).toString());
            if ("F".equals(abVar.b.H)) {
                czVar.A.setBackgroundResource(R.drawable.bg_gender_famal);
                czVar.s.setImageResource(R.drawable.ic_user_famale);
            } else {
                czVar.A.setBackgroundResource(R.drawable.bg_gender_male);
                czVar.s.setImageResource(R.drawable.ic_user_male);
            }
            if (abVar.b.j()) {
                czVar.x.setVisibility(0);
            } else {
                czVar.x.setVisibility(8);
            }
            if (!com.immomo.a.a.f.a.a(abVar.b.M)) {
                czVar.y.setVisibility(0);
                czVar.y.setImageBitmap(b.a(abVar.b.M, true));
            } else {
                czVar.y.setVisibility(8);
            }
            if (abVar.b.an) {
                czVar.t.setVisibility(0);
                czVar.t.setImageResource(abVar.b.ap ? R.drawable.ic_userinfo_weibov : R.drawable.ic_userinfo_weibo);
            } else {
                czVar.t.setVisibility(8);
            }
            if (abVar.b.at) {
                czVar.u.setVisibility(0);
                czVar.u.setImageResource(abVar.b.au ? R.drawable.ic_userinfo_tweibov : R.drawable.ic_userinfo_tweibo);
            } else {
                czVar.u.setVisibility(8);
            }
            if (abVar.b.aJ == 1 || abVar.b.aJ == 3) {
                czVar.z.setVisibility(0);
                czVar.z.setImageResource(abVar.b.aJ == 1 ? R.drawable.ic_userinfo_groupowner : R.drawable.ic_userinfo_group);
            } else {
                czVar.z.setVisibility(8);
            }
            if (abVar.b.ar) {
                czVar.v.setVisibility(0);
            } else {
                czVar.v.setVisibility(8);
            }
            if (abVar.b.b()) {
                czVar.w.setVisibility(0);
                if (abVar.b.c()) {
                    czVar.w.setImageResource(R.drawable.ic_userinfo_vip_year);
                } else {
                    czVar.w.setImageResource(R.drawable.ic_userinfo_vip);
                }
            } else {
                czVar.w.setVisibility(8);
            }
        } else {
            czVar.h.setText(abVar.c);
            czVar.C.setVisibility(8);
        }
        j.a((aj) abVar.b, czVar.g, this.f);
        czVar.c.setEnabled((abVar.d == null || abVar.d.o == 0) ? false : true);
        czVar.b.setText(abVar.g());
        if (a.f(abVar.b())) {
            czVar.d.setText(abVar.c());
            czVar.d.setVisibility(0);
        } else {
            czVar.d.setVisibility(8);
        }
        czVar.f767a.setText(abVar.f);
        czVar.e.setVisibility(8);
        czVar.f.setVisibility(8);
        if (abVar.l != null) {
            ViewGroup.LayoutParams layoutParams = czVar.e.getLayoutParams();
            layoutParams.height = this.g;
            layoutParams.width = (int) ((((float) this.g) / ((float) abVar.l.k())) * ((float) abVar.l.j()));
            czVar.e.setLayoutParams(layoutParams);
            czVar.e.setAlt(abVar.j);
            a(abVar, czVar.e);
            czVar.e.setAlt(abVar.j);
            czVar.e.setVisibility(0);
        } else if (a.f(abVar.j) && a.f(abVar.k)) {
            czVar.e.setVisibility(0);
            ViewGroup.LayoutParams layoutParams2 = czVar.e.getLayoutParams();
            layoutParams2.height = this.g;
            layoutParams2.width = this.g;
            czVar.e.setLayoutParams(layoutParams2);
            czVar.e.setAlt(abVar.j);
            a(abVar, czVar.e);
            czVar.e.setAlt(abVar.j);
        } else if (abVar.i() > 1) {
            czVar.f.setVisibility(0);
            czVar.f.setImage(abVar.j());
            czVar.f.setOnclickHandler(this);
        } else if (a.f(abVar.getLoadImageId())) {
            ViewGroup.LayoutParams layoutParams3 = czVar.e.getLayoutParams();
            layoutParams3.height = this.g;
            layoutParams3.width = this.g;
            czVar.e.setLayoutParams(layoutParams3);
            czVar.e.setAlt(PoiTypeDef.All);
            czVar.e.setVisibility(0);
            j.a((aj) abVar, (ImageView) czVar.e, (ViewGroup) this.f, 15, false, false, 0);
        } else {
            czVar.e.setVisibility(8);
        }
        czVar.j.setText(new StringBuilder().append(abVar.g).toString());
        if (abVar.n != null) {
            GameApp gameApp = abVar.n;
            czVar.r.setVisibility(0);
            czVar.q.setVisibility(0);
            czVar.m.setVisibility(0);
            czVar.p.setTextColor(g.c((int) R.color.text_feedapp_event));
            czVar.o.setTextColor(g.c((int) R.color.text_feedapp_event));
            czVar.r.setBackgroundColor(g.c((int) R.color.bg_feedapp_event_border));
            czVar.q.setBackgroundColor(g.c((int) R.color.bg_feedapp_event_border));
            czVar.m.setBackgroundColor(g.c((int) R.color.bg_feedapp_event));
            czVar.p.setText(gameApp.appdesc);
            czVar.o.setText(gameApp.appname);
            j.a((aj) gameApp.appIconLoader(), czVar.n, (ViewGroup) this.f, 18, false, true, 0);
        } else if (abVar.o != null) {
            com.immomo.momo.service.bean.u uVar = abVar.o;
            czVar.r.setVisibility(0);
            czVar.q.setVisibility(0);
            czVar.m.setVisibility(0);
            czVar.p.setTextColor(g.c((int) R.color.text_feedapp_event));
            czVar.o.setTextColor(g.c((int) R.color.text_feedapp_event));
            czVar.r.setBackgroundColor(g.c((int) R.color.bg_feedapp_event_border));
            czVar.q.setBackgroundColor(g.c((int) R.color.bg_feedapp_event_border));
            czVar.m.setBackgroundColor(g.c((int) R.color.bg_feedapp_event));
            czVar.p.setText(String.valueOf(uVar.d) + "  " + uVar.f() + "人参加");
            czVar.o.setText(uVar.b);
            czVar.n.setImageResource(R.drawable.ic_discover_event);
        } else if (abVar.p != null) {
            czVar.r.setVisibility(0);
            czVar.q.setVisibility(0);
            czVar.m.setVisibility(0);
            czVar.p.setTextColor(g.c((int) R.color.text_feedapp_event));
            czVar.o.setTextColor(g.c((int) R.color.text_feedapp_event));
            czVar.r.setBackgroundColor(g.c((int) R.color.bg_feedapp_event_border));
            czVar.q.setBackgroundColor(g.c((int) R.color.bg_feedapp_event_border));
            czVar.m.setBackgroundColor(g.c((int) R.color.bg_feedapp_event));
            czVar.p.setText(abVar.p.b);
            czVar.o.setText(abVar.p.f2983a);
            j.a(abVar.p, czVar.n, this.f, 18);
        } else {
            czVar.r.setVisibility(8);
            czVar.q.setVisibility(8);
            czVar.m.setVisibility(8);
        }
        return view;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.android.activity.feed.FeedProfileActivity.a(android.content.Context, java.lang.String, boolean):void
     arg types: [com.immomo.momo.android.activity.ao, java.lang.String, int]
     candidates:
      com.immomo.momo.android.activity.feed.FeedProfileActivity.a(com.immomo.momo.android.activity.feed.FeedProfileActivity, java.lang.String, boolean):void
      com.immomo.momo.android.activity.ao.a(android.support.v4.app.Fragment, android.content.Intent, int):void
      android.support.v4.app.g.a(java.lang.String, java.io.PrintWriter, android.view.View):void
      android.support.v4.app.g.a(android.support.v4.app.Fragment, android.content.Intent, int):void
      com.immomo.momo.android.activity.feed.FeedProfileActivity.a(android.content.Context, java.lang.String, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public final void onClick(View view) {
        int intValue = ((Integer) view.getTag(R.id.tag_item_position)).intValue();
        switch (view.getId()) {
            case R.id.layout_feed_content /*2131166245*/:
                FeedProfileActivity.a((Context) this.e, ((ab) getItem(intValue)).h, false);
                return;
            case R.id.iv_feed_photo /*2131166248*/:
                Intent intent = new Intent(this.e, OtherProfileActivity.class);
                intent.putExtra("momoid", ((ab) getItem(intValue)).c);
                this.e.startActivity(intent);
                return;
            case R.id.iv_feed_content /*2131166257*/:
                new k("C", "C402").e();
                String str = ((ab) getItem(intValue)).k;
                if (a.f(str)) {
                    Intent intent2 = new Intent(this.e, EmotionProfileActivity.class);
                    intent2.putExtra("eid", str);
                    this.e.startActivity(intent2);
                    return;
                }
                Intent intent3 = new Intent(this.e, ImageBrowserActivity.class);
                intent3.putExtra("array", new String[]{((ab) getItem(intValue)).getLoadImageId()});
                intent3.putExtra("imagetype", "feed");
                intent3.putExtra("autohide_header", true);
                this.e.startActivity(intent3);
                if (this.e.getParent() != null) {
                    this.e.getParent().overridePendingTransition(R.anim.zoom_enter, R.anim.normal);
                    return;
                } else {
                    this.e.overridePendingTransition(R.anim.zoom_enter, R.anim.normal);
                    return;
                }
            case R.id.feed_layout_app /*2131166261*/:
                GameApp gameApp = ((ab) getItem(intValue)).n;
                com.immomo.momo.service.bean.u uVar = ((ab) getItem(intValue)).o;
                ac acVar = ((ab) getItem(intValue)).p;
                if (gameApp != null) {
                    if (a.f(gameApp.action)) {
                        this.h.a((Object) gameApp.action);
                        d.a(gameApp.action, this.e);
                        return;
                    }
                    return;
                } else if (uVar != null) {
                    if (a.f(uVar.w)) {
                        this.h.a((Object) uVar.w);
                        d.a(uVar.w, this.e);
                        return;
                    }
                    return;
                } else if (acVar != null && a.f(acVar.d)) {
                    this.h.a((Object) acVar.d);
                    d.a(acVar.d, this.e);
                    return;
                } else {
                    return;
                }
            case R.id.layout_feed_commentcount /*2131166270*/:
                FeedProfileActivity.a((Context) this.e, ((ab) getItem(intValue)).h, true);
                return;
            case R.id.layout_feed_userinfo /*2131166640*/:
                Intent intent4 = new Intent(this.e, OtherProfileActivity.class);
                intent4.putExtra("momoid", ((ab) getItem(intValue)).c);
                this.e.startActivity(intent4);
                return;
            case R.id.bt_feed_more /*2131166665*/:
                ab abVar = (ab) getItem(intValue);
                String[] strArr = a.f(abVar.b()) ? this.i.equals(abVar.c) ? new String[]{"复制文本", "删除"} : new String[]{"复制文本", "举报"} : this.i.equals(abVar.c) ? new String[]{"删除"} : new String[]{"举报"};
                at atVar = new at(this.e, view, strArr);
                atVar.a(new ct(this, strArr, abVar));
                atVar.d();
                return;
            default:
                return;
        }
    }
}
