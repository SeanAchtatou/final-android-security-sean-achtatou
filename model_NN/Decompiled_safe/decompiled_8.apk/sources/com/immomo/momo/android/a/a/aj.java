package com.immomo.momo.android.a.a;

import android.view.View;

final class aj implements View.OnLongClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ah f679a;
    private final /* synthetic */ int b;

    aj(ah ahVar, int i) {
        this.f679a = ahVar;
        this.b = i;
    }

    public final boolean onLongClick(View view) {
        return this.f679a.e.getOnItemLongClickListenerInWrapper().onItemLongClick(this.f679a.e, view, this.b, (long) view.getId());
    }
}
