package com.immomo.momo.android.activity.common;

import android.content.Context;
import android.widget.ListAdapter;
import com.immomo.momo.android.a.fz;
import com.immomo.momo.android.c.d;
import com.immomo.momo.protocol.a.w;
import java.util.ArrayList;
import java.util.List;

final class as extends d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ar f1087a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public as(ar arVar, Context context) {
        super(context);
        this.f1087a = arVar;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        ArrayList arrayList = new ArrayList();
        w.a().c(arrayList, 1);
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        super.a(exc);
        this.f1087a.O.n();
        if (this.f1087a.P != null && !this.f1087a.P.isCancelled()) {
            this.f1087a.P.cancel(true);
        }
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        List list = (List) obj;
        super.a(list);
        this.f1087a.T = new fz(this.f1087a.c(), list, this.f1087a.O, this.f1087a);
        this.f1087a.O.setAdapter((ListAdapter) this.f1087a.T);
    }

    /* access modifiers changed from: protected */
    public final void b() {
        super.b();
        this.f1087a.P = null;
        this.f1087a.O.n();
    }
}
