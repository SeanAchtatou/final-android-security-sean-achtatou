package com.immomo.momo.android.pay;

import android.content.DialogInterface;
import android.os.Message;

final class h implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ BuyMemberActivity f2559a;

    h(BuyMemberActivity buyMemberActivity) {
        this.f2559a = buyMemberActivity;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        if (!this.f2559a.A) {
            Message message = new Message();
            message.what = 123;
            this.f2559a.b(new r(this.f2559a, this.f2559a, message));
            return;
        }
        this.f2559a.b(new i(this.f2559a, this.f2559a));
    }
}
