package com.immomo.momo.android.activity;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.view.View;
import com.immomo.momo.R;
import mm.purchasesdk.PurchaseCode;

final class gb implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ NewVersionActivity f1510a;
    private final /* synthetic */ String b;

    gb(NewVersionActivity newVersionActivity, String str) {
        this.f1510a = newVersionActivity;
        this.b = str;
    }

    public final void onClick(View view) {
        Intent intent = new Intent(this.f1510a.getApplicationContext(), NewVersionActivity.class);
        intent.putExtra("url_download", this.b);
        Notification notification = new Notification();
        notification.icon = R.drawable.ic_taskbar_icon;
        notification.when = System.currentTimeMillis();
        notification.flags |= 16;
        notification.tickerText = "陌陌升级提醒";
        notification.setLatestEventInfo(this.f1510a.getApplicationContext(), "陌陌升级提醒", "陌陌有新版本，点击了解详情", PendingIntent.getActivity(this.f1510a.getApplicationContext(), 1, intent, 134217728));
        ((NotificationManager) this.f1510a.getSystemService("notification")).notify(PurchaseCode.RESPONSE_ERR, notification);
        this.f1510a.finish();
    }
}
