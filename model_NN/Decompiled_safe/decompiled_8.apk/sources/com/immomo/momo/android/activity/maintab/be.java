package com.immomo.momo.android.activity.maintab;

import com.immomo.momo.android.view.a.ag;
import com.immomo.momo.android.view.a.ah;
import com.immomo.momo.android.view.a.aj;
import com.immomo.momo.android.view.a.ak;

final class be implements ag {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ aq f1854a;

    private be(aq aqVar) {
        this.f1854a = aqVar;
    }

    /* synthetic */ be(aq aqVar, byte b) {
        this(aqVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.android.activity.maintab.aq.a(com.immomo.momo.android.activity.maintab.aq, boolean):void
     arg types: [com.immomo.momo.android.activity.maintab.aq, int]
     candidates:
      com.immomo.momo.android.activity.maintab.aq.a(com.immomo.momo.android.activity.maintab.aq, double):void
      com.immomo.momo.android.activity.maintab.aq.a(com.immomo.momo.android.activity.maintab.aq, int):void
      com.immomo.momo.android.activity.maintab.aq.a(com.immomo.momo.android.activity.maintab.aq, com.immomo.momo.android.c.d):void
      com.immomo.momo.android.activity.maintab.aq.a(com.immomo.momo.android.activity.maintab.aq, java.util.Date):void
      com.immomo.momo.android.activity.lh.a(int, java.lang.String[]):com.immomo.momo.protocol.imjson.c.a
      com.immomo.momo.android.activity.lh.a(android.content.Context, com.immomo.momo.android.view.HeaderLayout):void
      com.immomo.momo.android.activity.lh.a(int, android.view.KeyEvent):boolean
      com.immomo.momo.android.activity.lh.a(android.os.Bundle, java.lang.String):boolean
      com.immomo.momo.android.activity.ar.a(android.view.LayoutInflater, android.view.ViewGroup):android.view.View
      android.support.v4.app.Fragment.a(android.content.Context, java.lang.String):android.support.v4.app.Fragment
      android.support.v4.app.Fragment.a(android.view.LayoutInflater, android.view.ViewGroup):android.view.View
      android.support.v4.app.Fragment.a(int, android.support.v4.app.Fragment):void
      android.support.v4.app.Fragment.a(android.content.Intent, int):void
      android.support.v4.app.Fragment.a(android.view.Menu, android.view.MenuInflater):boolean
      com.immomo.momo.android.activity.maintab.aq.a(com.immomo.momo.android.activity.maintab.aq, boolean):void */
    public final void a(ah ahVar, ak akVar, aj ajVar, int i, String str, int i2) {
        if (this.f1854a.X != null && !this.f1854a.X.isCancelled()) {
            this.f1854a.X.cancel(true);
        }
        this.f1854a.N.o = akVar;
        this.f1854a.N.n = ahVar;
        this.f1854a.N.p = ajVar;
        this.f1854a.N.s = str;
        this.f1854a.N.r = i2;
        this.f1854a.N.q = i;
        this.f1854a.aa = true;
        this.f1854a.T.l();
        this.f1854a.T.i();
    }
}
