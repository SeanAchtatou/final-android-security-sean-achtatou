package com.immomo.momo.android.activity.message;

import android.os.Handler;
import android.os.Message;
import com.mapabc.minimap.map.vmap.VMapProjection;
import com.sina.weibo.sdk.constant.Constants;

final class az extends Handler {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ChatBGSettingActivity f1932a;

    az(ChatBGSettingActivity chatBGSettingActivity) {
        this.f1932a = chatBGSettingActivity;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public final void handleMessage(Message message) {
        switch (message.what) {
            case VMapProjection.MAXZOOMLEVEL /*20*/:
                this.f1932a.j.notifyDataSetChanged();
                this.f1932a.t.sendEmptyMessageDelayed(20, 300);
                return;
            case 21:
                this.f1932a.a((CharSequence) "背景图下载失败，请重试");
                break;
            case Constants.WEIBO_SDK_VERSION /*22*/:
                break;
            default:
                return;
        }
        this.f1932a.j.notifyDataSetChanged();
    }
}
