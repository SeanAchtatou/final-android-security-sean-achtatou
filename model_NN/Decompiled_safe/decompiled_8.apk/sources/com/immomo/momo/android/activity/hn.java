package com.immomo.momo.android.activity;

import android.widget.ListAdapter;
import com.immomo.momo.android.a.gv;
import com.immomo.momo.g;

final class hn implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ OtherProfileActivity f1719a;

    hn(OtherProfileActivity otherProfileActivity) {
        this.f1719a = otherProfileActivity;
    }

    public final void run() {
        int measuredWidth = (int) (((float) this.f1719a.aC.getMeasuredWidth()) / (65.0f * g.k()));
        if (measuredWidth > 0) {
            this.f1719a.aC.setNumColumns(measuredWidth);
        }
        if (this.f1719a.aD == null) {
            this.f1719a.aD = new gv(this.f1719a);
            this.f1719a.aC.setAdapter((ListAdapter) this.f1719a.aD);
        }
        this.f1719a.aD.a(this.f1719a.t);
        this.f1719a.aE.setVisibility(0);
    }
}
