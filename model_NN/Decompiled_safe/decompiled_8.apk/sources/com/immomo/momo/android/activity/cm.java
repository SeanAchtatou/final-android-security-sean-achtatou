package com.immomo.momo.android.activity;

import android.view.View;

final class cm implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ EditUserPhotoActivity f1070a;

    cm(EditUserPhotoActivity editUserPhotoActivity) {
        this.f1070a = editUserPhotoActivity;
    }

    public final void onClick(View view) {
        if (this.f1070a.v()) {
            this.f1070a.b(new cq(this.f1070a, this.f1070a));
        } else {
            this.f1070a.finish();
        }
    }
}
