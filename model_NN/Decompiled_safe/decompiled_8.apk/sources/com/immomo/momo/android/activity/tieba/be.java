package com.immomo.momo.android.activity.tieba;

import android.support.v4.b.a;
import android.text.Editable;
import android.text.TextWatcher;
import com.amap.mapapi.poisearch.PoiTypeDef;

final class be implements TextWatcher {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ PublishTieCommentActivity f2181a;

    be(PublishTieCommentActivity publishTieCommentActivity) {
        this.f2181a = publishTieCommentActivity;
    }

    public final void afterTextChanged(Editable editable) {
        if (a.a((CharSequence) editable.toString())) {
            this.f2181a.n.setText(PoiTypeDef.All);
        } else {
            this.f2181a.n.setText(String.valueOf(editable.length()) + "/1000字");
        }
    }

    public final void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public final void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }
}
