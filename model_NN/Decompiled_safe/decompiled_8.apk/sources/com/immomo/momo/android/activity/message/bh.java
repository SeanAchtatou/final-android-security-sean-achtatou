package com.immomo.momo.android.activity.message;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import com.immomo.momo.android.activity.OtherProfileActivity;
import com.immomo.momo.service.bean.bf;

final class bh implements AdapterView.OnItemClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ GroupChatActivity f1941a;

    bh(GroupChatActivity groupChatActivity) {
        this.f1941a = groupChatActivity;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        Intent intent = new Intent();
        intent.setClass(this.f1941a.n(), OtherProfileActivity.class);
        intent.putExtra("tag", "local");
        intent.putExtra("momoid", ((bf) this.f1941a.R.getItem(i)).h);
        this.f1941a.startActivity(intent);
    }
}
