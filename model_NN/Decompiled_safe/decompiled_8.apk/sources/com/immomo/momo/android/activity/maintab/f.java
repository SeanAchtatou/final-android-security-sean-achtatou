package com.immomo.momo.android.activity.maintab;

import com.immomo.momo.util.am;

final class f implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ MaintabActivity f1888a;
    private final /* synthetic */ long b;

    f(MaintabActivity maintabActivity, long j) {
        this.f1888a = maintabActivity;
        this.b = j;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [long, int]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    public final void run() {
        if (!this.f1888a.isDestroyed()) {
            this.f1888a.b(false);
            am.b();
            this.f1888a.m.a(new g(this), Math.max(((long) this.f1888a.m.b()) - Math.abs(System.currentTimeMillis() - this.b), 0L));
        }
    }
}
