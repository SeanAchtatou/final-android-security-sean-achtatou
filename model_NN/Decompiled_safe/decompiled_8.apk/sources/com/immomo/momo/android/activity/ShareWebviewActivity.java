package com.immomo.momo.android.activity;

import android.os.Bundle;
import android.support.v4.b.a;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.view.HeaderLayout;
import com.immomo.momo.g;
import com.immomo.momo.util.m;
import java.util.Calendar;

public class ShareWebviewActivity extends ah {
    View h = null;
    private HeaderLayout i = null;
    private View j = null;
    private WebView k = null;
    private int l = -1;
    private String[] m;
    /* access modifiers changed from: private */
    public m n = new m("ShareWebviewActivity");

    /* access modifiers changed from: private */
    public String c(String str) {
        String str2 = String.valueOf(getString(R.string.share_momocard_content)) + "(苹果、安卓手机下载:" + str + " )";
        String str3 = String.valueOf(SharePageActivity.h) + "/" + this.f.h + ".jpg?day=" + Calendar.getInstance().get(5);
        String str4 = PoiTypeDef.All;
        switch (this.l) {
            case 0:
                str4 = "http://service.weibo.com/share/share.php?title=" + str2 + "&pic=" + str3 + "?url=" + str;
                break;
            case 1:
                str4 = "http://service.weibo.com/share/share.php?title=" + str + "&pic=" + this.m[0];
                break;
            case 2:
                str4 = "http://v.t.qq.com/share/share.php?title=" + str2 + "&pic=" + str3 + "?url=" + str;
                break;
            case 3:
                str4 = "http://widget.renren.com/dialog/feed?app_id=165934&action_name=访问&action_link=" + str + "&redirect_uri=http://apps.renren.com/demo_app&url=" + str + "&name=我在陌陌，你在哪里？&description=" + str2 + "&image=" + str3;
                break;
            case 4:
                str4 = "http://sns.qzone.qq.com/cgi-bin/qzshare/cgi_qzshare_onekey?url=" + str + "&title=赶快加我陌陌，让我看看你们在哪里？&desc=" + str2 + "&pics=" + str3;
                break;
        }
        this.n.a((Object) ("url: " + str4));
        return str4;
    }

    /* access modifiers changed from: private */
    public void d(String str) {
        this.k.getSettings().setCacheMode(2);
        this.k.getSettings().setJavaScriptEnabled(true);
        WebSettings settings = this.k.getSettings();
        settings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NORMAL);
        settings.setBuiltInZoomControls(true);
        settings.setSupportZoom(true);
        settings.setUseWideViewPort(true);
        settings.setDefaultZoom(WebSettings.ZoomDensity.FAR);
        this.k.setWebChromeClient(new kv());
        this.k.setWebViewClient(new ky(this, (byte) 0));
        this.k.getSettings().setUserAgentString(g.O());
        this.k.loadUrl(str);
    }

    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        super.a(bundle);
        setContentView((int) R.layout.activity_setting_sharewebview);
        d();
        this.i = (HeaderLayout) findViewById(R.id.layout_header);
        this.j = findViewById(R.id.sharewebview_rootview);
        this.k = (WebView) this.j.findViewById(R.id.webview);
        this.h = this.j.findViewById(R.id.loading_indicator);
        findViewById(R.id.header_stv_title).setFocusableInTouchMode(false);
        String str = PoiTypeDef.All;
        switch (this.l) {
            case 0:
            case 1:
                str = "新浪微博";
                break;
            case 2:
                str = "腾讯微博";
                break;
            case 3:
                str = "人人网";
                break;
            case 4:
                str = "QQ空间";
                break;
        }
        this.i.setTitleText("分享到" + str);
        if (this.f != null) {
            if (this.l == 1) {
                this.m = getIntent().getStringArrayExtra("share_content");
                if (!a.a(this.m)) {
                    this.n.a((Object) "启动Web分享群活动时，分享内容参数不能为空");
                    finish();
                }
                d(c(this.m[1]));
                return;
            }
            new kw(this).execute(new String[0]);
        }
    }

    /* access modifiers changed from: protected */
    public final void d() {
        this.l = getIntent().getIntExtra("share_type", -1);
        this.n.b((Object) ("shareType:" + this.l));
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
    }
}
