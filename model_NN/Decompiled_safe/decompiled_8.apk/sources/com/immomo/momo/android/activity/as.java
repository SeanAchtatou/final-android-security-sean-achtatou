package com.immomo.momo.android.activity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import com.immomo.momo.R;

@SuppressLint({"JavascriptInterface"})
public abstract class as extends ao {
    protected WebView h = null;
    /* access modifiers changed from: private */
    public View i = null;
    private ViewGroup j;

    /* access modifiers changed from: protected */
    public final void d() {
    }

    /* access modifiers changed from: protected */
    public int f() {
        return R.layout.activity_baseweb;
    }

    /* access modifiers changed from: protected */
    public void g() {
        this.h = (WebView) findViewById(R.id.webview);
        this.j = (ViewGroup) findViewById(R.id.layout_header_container);
        this.h.getSettings().setJavaScriptEnabled(true);
    }

    /* access modifiers changed from: protected */
    public final void i() {
        this.j.setVisibility(0);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(f());
        g();
        this.i = findViewById(R.id.loading_indicator);
        this.h.setWebChromeClient(new at());
        this.h.setWebViewClient(new au(this));
    }
}
