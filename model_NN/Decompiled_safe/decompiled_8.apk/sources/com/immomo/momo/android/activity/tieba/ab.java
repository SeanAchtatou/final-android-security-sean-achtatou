package com.immomo.momo.android.activity.tieba;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.protocol.a.a.l;
import com.immomo.momo.protocol.a.v;
import com.immomo.momo.service.bean.c.c;
import java.util.Iterator;

final class ab extends d {

    /* renamed from: a  reason: collision with root package name */
    private l f2153a = null;
    private /* synthetic */ z c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ab(z zVar, Context context) {
        super(context);
        this.c = zVar;
        if (zVar.U != null && !zVar.U.isCancelled()) {
            zVar.U.cancel(true);
        }
        zVar.U = this;
    }

    /* access modifiers changed from: protected */
    public final Object a(Object... objArr) {
        this.f2153a = v.a().b(this.c.S.getCount());
        return null;
    }

    /* access modifiers changed from: protected */
    public final void a(Object obj) {
        if (!this.f2153a.b()) {
            this.c.O.k();
        }
        Iterator it = this.f2153a.a().iterator();
        while (it.hasNext()) {
            c cVar = (c) it.next();
            if (!this.c.R.contains(cVar)) {
                this.c.S.b(cVar);
                this.c.R.add(cVar);
            }
        }
        this.c.S.notifyDataSetChanged();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.c.P.e();
    }
}
