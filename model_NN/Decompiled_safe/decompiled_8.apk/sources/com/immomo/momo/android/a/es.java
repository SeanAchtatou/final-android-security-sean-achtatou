package com.immomo.momo.android.a;

import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.ao;
import com.immomo.momo.android.view.EmoteTextView;
import com.immomo.momo.android.view.MomoRefreshExpandableListView;
import com.immomo.momo.android.view.a.at;
import com.immomo.momo.android.view.a.n;
import com.immomo.momo.android.view.a.o;
import com.immomo.momo.b;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.a.a;
import com.immomo.momo.service.bean.a.j;
import com.immomo.momo.service.bean.a.k;
import com.immomo.momo.util.m;
import java.util.HashMap;
import java.util.List;

public final class es extends b {
    private static final String[] f = {"设为管理员", "转让群组", "移除", "移除并举报"};
    private static final String[] g = {"撤销管理员", "转让群组", "移除", "移除并举报"};
    private static final String[] h = {"移除", "移除并举报"};

    /* renamed from: a  reason: collision with root package name */
    private List f805a = null;
    /* access modifiers changed from: private */
    public MomoRefreshExpandableListView b = null;
    /* access modifiers changed from: private */
    public a c = null;
    /* access modifiers changed from: private */
    public ao d = null;
    /* access modifiers changed from: private */
    public int e;

    public es(List list, MomoRefreshExpandableListView momoRefreshExpandableListView, ao aoVar, a aVar, int i) {
        new m(this);
        new HashMap();
        this.f805a = list;
        this.b = momoRefreshExpandableListView;
        this.d = aoVar;
        this.e = i;
        this.c = aVar;
    }

    static /* synthetic */ void a(es esVar, j jVar) {
        n nVar = new n(esVar.d);
        nVar.setTitle("设置为管理员");
        nVar.a();
        nVar.a("TA将有权限管理群成员和群空间");
        nVar.a(0, (int) R.string.dialog_btn_confim, new ew(esVar, jVar));
        nVar.a(1, (int) R.string.dialog_btn_cancel, new ex());
        nVar.show();
    }

    private static boolean a(k kVar) {
        return kVar.b() == 2;
    }

    static /* synthetic */ void b(es esVar, j jVar) {
        n nVar = new n(esVar.d);
        nVar.setTitle("撤销管理员");
        nVar.a();
        nVar.a("TA将失去管理群组的权限");
        nVar.a(0, (int) R.string.dialog_btn_confim, new ey(esVar, jVar));
        nVar.a(1, (int) R.string.dialog_btn_cancel, new ez());
        nVar.show();
    }

    private static boolean b(k kVar) {
        return kVar.b() == 3;
    }

    /* access modifiers changed from: private */
    /* renamed from: c */
    public k getGroup(int i) {
        return (k) this.f805a.get(i);
    }

    static /* synthetic */ void c(es esVar, j jVar) {
        EditText editText = (EditText) g.o().inflate((int) R.layout.common_dialog_transfergroup_pwd, (ViewGroup) null);
        n a2 = n.a(esVar.d, PoiTypeDef.All, "确定", "取消", new fa(esVar, editText, jVar), (DialogInterface.OnClickListener) null);
        a2.setTitle("验证身份");
        a2.setContentView(editText);
        editText.requestFocus();
        a2.setCanceledOnTouchOutside(false);
        a2.show();
        editText.postDelayed(new fb(esVar, editText), 200);
    }

    static /* synthetic */ void e(es esVar, j jVar) {
        o oVar = new o(esVar.d, (int) R.array.reportgroup_items);
        oVar.setTitle((int) R.string.report_dialog_title);
        oVar.a();
        oVar.a(new eu(esVar, jVar));
        oVar.show();
    }

    /* renamed from: a */
    public final j getChild(int i, int i2) {
        return (j) ((k) this.f805a.get(i)).f2980a.get(i2);
    }

    public final void a() {
        for (int i = 0; i < getGroupCount(); i++) {
            this.b.expandGroup(i);
        }
    }

    public final void a(View view, int i) {
        if (i >= 0) {
            ((TextView) view.findViewById(R.id.sitelist_tv_name)).setText(getGroup(i).a());
        }
    }

    public final void a(j jVar) {
        if (jVar != null && this.f805a != null) {
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 < this.f805a.size()) {
                    ((k) this.f805a.get(i2)).f2980a.remove(jVar);
                    i = i2 + 1;
                } else {
                    return;
                }
            }
        }
    }

    public final void a(j jVar, View view) {
        String[] strArr;
        switch (this.e) {
            case 1:
                if (jVar.g != 2) {
                    strArr = f;
                    break;
                } else {
                    strArr = g;
                    break;
                }
            case 2:
                strArr = h;
                break;
            default:
                strArr = new String[0];
                break;
        }
        if (strArr.length > 0) {
            at atVar = new at(this.d, view, strArr);
            atVar.a(new ev(this, strArr, jVar));
            atVar.d();
        }
    }

    public final boolean a(int i) {
        return a(getGroup(i)) && getGroup(i).f2980a.size() == 0;
    }

    public final int b(int i, int i2) {
        if (i2 < 0 && i < 0) {
            return 0;
        }
        if (i2 != -1 || this.b.isGroupExpanded(i)) {
            return i2 == getChildrenCount(i) + -1 ? 2 : 1;
        }
        return 0;
    }

    public final boolean b(int i) {
        return b(getGroup(i)) && getGroup(i).f2980a.size() == 0;
    }

    public final long getChildId(int i, int i2) {
        return (long) i2;
    }

    public final int getChildType(int i, int i2) {
        if (a(i)) {
            return 0;
        }
        return b(i) ? 1 : 2;
    }

    public final int getChildTypeCount() {
        return 3;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.b.a(java.lang.String, boolean):android.graphics.Bitmap
     arg types: [java.lang.String, int]
     candidates:
      com.immomo.momo.b.a(java.lang.String, android.graphics.Bitmap):android.graphics.Bitmap
      com.immomo.momo.b.a(java.lang.String, boolean):android.graphics.Bitmap */
    public final View getChildView(int i, int i2, boolean z, View view, ViewGroup viewGroup) {
        if (a(i)) {
            if (view == null || ((TextView) view.getTag(R.id.tag_tieba_index0)) == null) {
                view = g.o().inflate((int) R.layout.listitem_relation_empty, (ViewGroup) null);
                view.setTag((TextView) view.findViewById(R.id.tv_tip));
            }
            ((TextView) view.getTag()).setText("未设置管理员");
        } else if (b(i)) {
            if (view == null) {
                view = g.o().inflate((int) R.layout.listitem_relation_empty, (ViewGroup) null);
                view.setTag((TextView) view.findViewById(R.id.tv_tip));
            }
            ((TextView) view.getTag()).setText("还未有群成员");
        } else {
            if (view == null) {
                fk fkVar = new fk((byte) 0);
                view = g.o().inflate((int) R.layout.listitem_groupuser, (ViewGroup) null);
                fkVar.h = view.findViewById(R.id.layout_time_container);
                view.findViewById(R.id.layout_item_container);
                fkVar.f822a = (ImageView) view.findViewById(R.id.userlist_item_iv_face);
                fkVar.b = (TextView) view.findViewById(R.id.userlist_item_tv_name);
                fkVar.c = (TextView) view.findViewById(R.id.userlist_item_tv_age);
                fkVar.d = (TextView) view.findViewById(R.id.userlist_item_tv_distance);
                fkVar.e = (TextView) view.findViewById(R.id.profile_tv_time);
                fkVar.f = (EmoteTextView) view.findViewById(R.id.userlist_item_tv_sign);
                fkVar.i = (ImageView) view.findViewById(R.id.userlist_item_iv_gender);
                fkVar.g = view.findViewById(R.id.userlist_item_layout_genderbackgroud);
                fkVar.j = (ImageView) view.findViewById(R.id.userlist_item_pic_iv_weibo);
                fkVar.k = (ImageView) view.findViewById(R.id.userlist_item_pic_iv_tweibo);
                fkVar.l = (ImageView) view.findViewById(R.id.userlist_item_pic_iv_renren);
                fkVar.m = (ImageView) view.findViewById(R.id.userlist_item_pic_iv_vip);
                fkVar.n = (ImageView) view.findViewById(R.id.userlist_item_pic_iv_relation);
                fkVar.o = (ImageView) view.findViewById(R.id.userlist_item_pic_iv_multipic);
                fkVar.p = (ImageView) view.findViewById(R.id.userlist_item_pic_iv_industry);
                fkVar.q = view.findViewById(R.id.triangle_zone);
                fkVar.r = (ImageView) view.findViewById(R.id.userlist_item_pic_sign);
                view.setTag(R.id.tag_userlist_item, fkVar);
            }
            j a2 = getChild(i, i2);
            fk fkVar2 = (fk) view.getTag(R.id.tag_userlist_item);
            fkVar2.q.setOnClickListener(new et(this, a2, fkVar2));
            if (this.e == 1) {
                fkVar2.q.setVisibility(a2.f2979a.equals(g.q().h) ? 8 : 0);
            } else if (this.e == 2) {
                fkVar2.q.setVisibility((a2.g == 2 || a2.g == 1) ? 8 : 0);
            } else {
                fkVar2.q.setVisibility(8);
            }
            if (a2.h != null) {
                fkVar2.d.setText(a2.h.Z);
                if (g.a((int) R.string.profile_distance_hide).equals(a2.h.Z) || g.a((int) R.string.profile_distance_unknown).equals(a2.h.Z)) {
                    fkVar2.h.setVisibility(8);
                } else {
                    fkVar2.h.setVisibility(0);
                }
                if (!android.support.v4.b.a.a((CharSequence) a2.h.aa)) {
                    fkVar2.e.setText(" | " + a2.h.aa);
                }
                fkVar2.c.setText(new StringBuilder(String.valueOf(a2.h.I)).toString());
                fkVar2.b.setText(a2.h.h());
                if (a2.h.b()) {
                    fkVar2.b.setTextColor(g.c((int) R.color.font_vip_name));
                } else {
                    fkVar2.b.setTextColor(g.c((int) R.color.text_color));
                }
                fkVar2.f.setText(a2.h.n());
                if (!android.support.v4.b.a.a((CharSequence) a2.h.R)) {
                    Bitmap b2 = b.b(a2.h.R);
                    if (b2 != null) {
                        fkVar2.r.setVisibility(0);
                        fkVar2.r.setImageBitmap(b2);
                    } else {
                        fkVar2.r.setVisibility(8);
                    }
                } else {
                    fkVar2.r.setVisibility(8);
                }
                if ("F".equals(a2.h.H)) {
                    fkVar2.g.setBackgroundResource(R.drawable.bg_gender_famal);
                    fkVar2.i.setImageResource(R.drawable.ic_user_famale);
                } else {
                    fkVar2.g.setBackgroundResource(R.drawable.bg_gender_male);
                    fkVar2.i.setImageResource(R.drawable.ic_user_male);
                }
                if (a2.h.j()) {
                    fkVar2.o.setVisibility(0);
                } else {
                    fkVar2.o.setVisibility(8);
                }
                if ("both".equals(a2.h.P)) {
                    fkVar2.n.setVisibility(0);
                } else {
                    fkVar2.n.setVisibility(8);
                }
                if (a2.h.an) {
                    fkVar2.j.setVisibility(0);
                    fkVar2.j.setImageResource(a2.h.ap ? R.drawable.ic_userinfo_weibov : R.drawable.ic_userinfo_weibo);
                } else {
                    fkVar2.j.setVisibility(8);
                }
                if (a2.h.at) {
                    fkVar2.k.setVisibility(0);
                    fkVar2.k.setImageResource(a2.h.au ? R.drawable.ic_userinfo_tweibov : R.drawable.ic_userinfo_tweibo);
                } else {
                    fkVar2.k.setVisibility(8);
                }
                if (a2.h.b()) {
                    fkVar2.m.setVisibility(0);
                    if (a2.h.c()) {
                        fkVar2.m.setImageResource(R.drawable.ic_userinfo_vip_year);
                    } else {
                        fkVar2.m.setImageResource(R.drawable.ic_userinfo_vip);
                    }
                } else {
                    fkVar2.m.setVisibility(8);
                }
                if (a2.h.ar) {
                    fkVar2.l.setVisibility(0);
                } else {
                    fkVar2.l.setVisibility(8);
                }
                if (!android.support.v4.b.a.a((CharSequence) a2.h.M)) {
                    fkVar2.p.setVisibility(0);
                    fkVar2.p.setImageBitmap(b.a(a2.h.M, true));
                } else {
                    fkVar2.p.setVisibility(8);
                }
                com.immomo.momo.util.j.a(a2.h, fkVar2.f822a, this.b, 3);
            }
        }
        return view;
    }

    public final int getChildrenCount(int i) {
        if (i < 0 || i >= this.f805a.size()) {
            return 0;
        }
        k kVar = (k) this.f805a.get(i);
        if (kVar.f2980a == null) {
            return 0;
        }
        if (a(kVar) || b(kVar)) {
            if (kVar.f2980a.size() == 0) {
                return 1;
            }
            return kVar.f2980a.size();
        } else if (((k) this.f805a.get(i)).f2980a == null) {
            return 0;
        } else {
            return ((k) this.f805a.get(i)).f2980a.size();
        }
    }

    public final int getGroupCount() {
        return this.f805a.size();
    }

    public final long getGroupId(int i) {
        return (long) i;
    }

    public final View getGroupView(int i, boolean z, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = g.o().inflate((int) R.layout.listitem_groupsite, (ViewGroup) null);
            fh fhVar = new fh();
            fhVar.f819a = (TextView) view.findViewById(R.id.sitelist_tv_name);
            fhVar.b = (ImageView) view.findViewById(R.id.sitelist_tv_arrow);
            view.findViewById(R.id.layout_root);
            view.setTag(R.id.tag_userlist_item, fhVar);
        }
        k c2 = getGroup(i);
        fh fhVar2 = (fh) view.getTag(R.id.tag_userlist_item);
        fhVar2.f819a.setText(c2.a());
        fhVar2.b.setVisibility(8);
        return view;
    }

    public final boolean hasStableIds() {
        return true;
    }

    public final boolean isChildSelectable(int i, int i2) {
        return true;
    }
}
