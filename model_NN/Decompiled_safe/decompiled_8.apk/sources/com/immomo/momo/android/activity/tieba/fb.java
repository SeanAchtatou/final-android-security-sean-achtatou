package com.immomo.momo.android.activity.tieba;

import android.content.Context;
import android.content.Intent;
import com.immomo.momo.android.broadcast.aa;
import com.immomo.momo.android.c.d;
import com.immomo.momo.protocol.a.v;
import com.immomo.momo.util.ao;
import com.sina.weibo.sdk.constant.Constants;

final class fb extends d {

    /* renamed from: a  reason: collision with root package name */
    private int f2279a = 0;
    private StringBuilder c = null;
    private long d = 0;
    private /* synthetic */ TiebaProfileActivity e;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public fb(TiebaProfileActivity tiebaProfileActivity, Context context, int i) {
        super(context);
        this.e = tiebaProfileActivity;
        this.f2279a = i;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        Object[] objArr2 = objArr;
        String str = null;
        switch (this.f2279a) {
            case 1:
                str = v.a().c(this.e.k);
                this.e.l.m = true;
                this.e.l.g++;
                break;
            case 2:
                str = v.a().d(this.e.k);
                com.immomo.momo.service.bean.c.d d2 = this.e.l;
                d2.g--;
                this.e.l.m = false;
                this.e.l.l = false;
                break;
            case 4:
                str = v.a().a(this.e.k, (String) objArr2[0]);
                break;
            case 8:
                str = v.a().e(this.e.l.b);
                break;
            case Constants.WEIBO_SDK_VERSION /*22*/:
                this.c = new StringBuilder();
                str = v.a().a(this.e.k, this.c);
                break;
        }
        long currentTimeMillis = 1000 - (System.currentTimeMillis() - this.d);
        if (currentTimeMillis > 0) {
            Thread.sleep(currentTimeMillis);
        }
        return str;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        if (22 == this.f2279a) {
            com.immomo.momo.android.view.a.v vVar = new com.immomo.momo.android.view.a.v(f(), this);
            vVar.a("正在检查是否符合申请条件，请稍候...");
            this.e.a(vVar);
        } else {
            this.e.a(new com.immomo.momo.android.view.a.v(f(), this));
        }
        this.d = System.currentTimeMillis();
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        String str = (String) obj;
        super.a((Object) str);
        if (22 != this.f2279a) {
            ao.a((CharSequence) str);
        }
        switch (this.f2279a) {
            case 1:
                Intent intent = new Intent(aa.f2346a);
                intent.putExtra("tiebaid", this.e.k);
                intent.putExtra("type", "join");
                this.e.sendBroadcast(intent);
                break;
            case 2:
                Intent intent2 = new Intent(aa.f2346a);
                intent2.putExtra("tiebaid", this.e.k);
                intent2.putExtra("type", "quite");
                this.e.sendBroadcast(intent2);
                break;
            case Constants.WEIBO_SDK_VERSION /*22*/:
                if (Integer.parseInt(this.c.toString()) != 0) {
                    ao.a((CharSequence) str);
                    break;
                } else {
                    TiebaProfileActivity.l(this.e);
                    break;
                }
        }
        this.e.m.a(this.e.l);
        this.e.v();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.e.p();
    }
}
