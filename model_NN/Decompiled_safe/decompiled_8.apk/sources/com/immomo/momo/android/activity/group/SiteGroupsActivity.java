package com.immomo.momo.android.activity.group;

import android.os.Bundle;
import android.support.v4.b.a;
import android.widget.ListAdapter;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.a.hr;
import com.immomo.momo.android.activity.ah;
import com.immomo.momo.android.view.LoadingButton;
import com.immomo.momo.android.view.MomoRefreshListView;
import com.immomo.momo.android.view.bl;
import com.immomo.momo.android.view.bu;
import com.immomo.momo.android.view.cv;
import com.immomo.momo.service.am;
import com.immomo.momo.service.bean.ay;
import com.immomo.momo.service.y;
import com.immomo.momo.util.k;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class SiteGroupsActivity extends ah implements bl, bu, cv {
    String h = PoiTypeDef.All;
    Map i = new HashMap();
    y j = null;
    MomoRefreshListView k = null;
    hr l = null;
    LoadingButton m = null;
    fk n = null;
    fl o = null;
    private ay p = null;
    private am q = null;

    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        super.a(bundle);
        setContentView((int) R.layout.activity_sitegroups);
        this.h = getIntent().getStringExtra("siteid");
        if (a.a((CharSequence) this.h)) {
            finish();
            return;
        }
        this.j = new y();
        this.q = new am();
        String stringExtra = getIntent().getStringExtra("sitename");
        if (a.a((CharSequence) stringExtra)) {
            this.p = this.q.b(this.h);
            if (this.p != null) {
                setTitle(this.p.f);
            }
        } else {
            setTitle(stringExtra);
        }
        this.k = (MomoRefreshListView) findViewById(R.id.listview);
        this.k.setTimeEnable(false);
        this.k.setEnableLoadMoreFoolter(true);
        this.m = this.k.getFooterViewButton();
        this.m.setVisibility(8);
        this.m.setOnProcessListener(this);
        this.l = new hr(getApplicationContext(), new ArrayList(), this.k);
        this.k.setAdapter((ListAdapter) this.l);
        this.k.setOnPullToRefreshListener$42b903f6(this);
        this.k.setOnCancelListener$135502(this);
        this.k.setOnItemClickListener(new fh(this));
        a(new fi(this, this)).execute(new Object[0]);
    }

    public final void b_() {
        if (this.o != null && !this.o.isCancelled()) {
            this.o.cancel(true);
        }
        fl flVar = new fl(this, this);
        this.o = flVar;
        a(flVar).execute(new Object[0]);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("site_id", this.h);
            new k("PO", "P33", jSONObject).e();
        } catch (JSONException e) {
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("site_id", this.h);
            new k("PI", "P33", jSONObject).e();
        } catch (JSONException e) {
        }
    }

    public final void u() {
        this.m.f();
        fk fkVar = new fk(this, this);
        this.n = fkVar;
        a(fkVar).execute(new Object[0]);
    }

    public final void v() {
        this.k.n();
        if (this.o != null && !this.o.isCancelled()) {
            this.o.cancel(true);
            this.o = null;
            this.m.e();
        }
    }
}
