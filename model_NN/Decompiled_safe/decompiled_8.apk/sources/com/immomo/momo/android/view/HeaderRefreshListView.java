package com.immomo.momo.android.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Scroller;
import mm.purchasesdk.PurchaseCode;

public class HeaderRefreshListView extends MomoRefreshListView {
    private Scroller g;
    private int h = PurchaseCode.LOADCHANNEL_ERR;
    private boolean i = false;
    private View j;
    private float k;
    private int l = 0;

    public HeaderRefreshListView(Context context) {
        super(context);
        this.g = new Scroller(context);
    }

    public HeaderRefreshListView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.g = new Scroller(context);
    }

    public HeaderRefreshListView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.g = new Scroller(context);
    }

    public void computeScroll() {
        if (this.g.computeScrollOffset()) {
            int currX = this.g.getCurrX();
            int currY = this.g.getCurrY();
            this.j.layout(0, 0, currX + this.j.getWidth(), currY);
            if (!this.g.isFinished() && this.i && currY > this.l) {
                this.j.setLayoutParams(new LinearLayout.LayoutParams(this.j.getWidth(), currY));
            }
            invalidate();
        }
        super.computeScroll();
    }

    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        float y = motionEvent.getY();
        if (this.j == null) {
            return false;
        }
        switch (motionEvent.getAction()) {
            case 0:
                this.k = y;
                this.l = this.j.getBottom();
                break;
            case 1:
                this.i = true;
                this.g.startScroll(this.j.getLeft(), this.j.getBottom(), 0 - this.j.getLeft(), this.l - this.j.getBottom(), PurchaseCode.LOADCHANNEL_ERR);
                invalidate();
                break;
            case 2:
                if (this.j.isShown() && this.j.getTop() >= 0) {
                    int i2 = (int) (((y - this.k) / 2.5f) + ((float) this.l));
                    if (i2 < this.j.getBottom() + this.h && i2 >= this.l) {
                        this.j.setLayoutParams(new LinearLayout.LayoutParams((ViewGroup.MarginLayoutParams) new LinearLayout.LayoutParams(this.j.getWidth(), i2)));
                    }
                    this.i = false;
                    break;
                }
        }
        return super.dispatchTouchEvent(motionEvent);
    }

    public void setHeaderView(ViewGroup viewGroup) {
        addHeaderView(viewGroup);
        this.j = viewGroup.getChildAt(0);
    }
}
