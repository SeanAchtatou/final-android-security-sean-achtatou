package com.immomo.momo.android.activity.event;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.ka;
import com.immomo.momo.android.activity.lh;
import com.immomo.momo.g;
import com.immomo.momo.protocol.imjson.d;
import com.immomo.momo.service.af;

public class MainEventActivity extends ka implements View.OnClickListener {
    private TextView h;
    private TextView i;

    private boolean A() {
        int n = af.c().n();
        if (n <= 0) {
            this.h.setVisibility(8);
        } else if (n < 100) {
            this.h.setVisibility(0);
            this.h.setText(new StringBuilder(String.valueOf(n)).toString());
        } else {
            this.h.setVisibility(0);
            this.h.setText("99+");
        }
        int o = af.c().o();
        if (o <= 0) {
            this.i.setVisibility(8);
        } else if (o < 100) {
            this.i.setVisibility(0);
            this.i.setText(new StringBuilder(String.valueOf(o)).toString());
        } else {
            this.i.setVisibility(0);
            this.i.setText("99+");
        }
        if (!g()) {
            if (n > 0) {
                c(1);
            } else if (o > 0) {
                c(2);
            }
        }
        return n > 0 || o > 0;
    }

    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        setContentView((int) R.layout.activity_eventmain);
        a(bd.class, a.class, e.class);
        findViewById(R.id.eventmain_layout_tab1).setOnClickListener(this);
        findViewById(R.id.eventmain_layout_tab2).setOnClickListener(this);
        findViewById(R.id.eventmain_layout_tab3).setOnClickListener(this);
        this.h = (TextView) findViewById(R.id.eventmain_layout_tab2).findViewById(R.id.eventmain_tab_count);
        this.i = (TextView) findViewById(R.id.eventmain_layout_tab3).findViewById(R.id.eventmain_tab_count);
        if (A()) {
            t().p();
        }
        a(700, "actions.eventdynamics");
    }

    /* access modifiers changed from: protected */
    public final void a(Fragment fragment, int i2) {
        switch (i2) {
            case 0:
                findViewById(R.id.eventmain_layout_tab1).setSelected(true);
                findViewById(R.id.eventmain_layout_tab2).setSelected(false);
                findViewById(R.id.eventmain_layout_tab3).setSelected(false);
                break;
            case 1:
                findViewById(R.id.eventmain_layout_tab2).setSelected(true);
                findViewById(R.id.eventmain_layout_tab1).setSelected(false);
                findViewById(R.id.eventmain_layout_tab3).setSelected(false);
                break;
            case 2:
                findViewById(R.id.eventmain_layout_tab2).setSelected(false);
                findViewById(R.id.eventmain_layout_tab1).setSelected(false);
                findViewById(R.id.eventmain_layout_tab3).setSelected(true);
                break;
        }
        m().a();
        ((lh) fragment).a(this, m());
        if (!((lh) fragment).T()) {
            ((lh) fragment).S();
        }
    }

    /* access modifiers changed from: protected */
    public final boolean a(Bundle bundle, String str) {
        if ("actions.eventdynamics".equals(str)) {
            A();
        }
        return super.a(bundle, str);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.eventmain_layout_tab1 /*2131165603*/:
                c(0);
                return;
            case R.id.eventmain_tab_label /*2131165604*/:
            case R.id.eventmain_tab_count /*2131165605*/:
            default:
                return;
            case R.id.eventmain_layout_tab2 /*2131165606*/:
                c(1);
                return;
            case R.id.eventmain_layout_tab3 /*2131165607*/:
                c(2);
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        g.d().a(new Bundle(), "actions.feedchanged");
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
    }

    /* access modifiers changed from: protected */
    public final void s() {
        super.s();
    }

    public final void y() {
        if (this.h.isShown()) {
            t().p();
            this.h.setVisibility(8);
        }
        af.c().e(0);
        d.c();
        g.d().a(new Bundle(), "actions.eventstatuschanged");
    }

    public final void z() {
        if (this.i.isShown()) {
            t().p();
            this.i.setVisibility(8);
        }
        af.c().f(0);
        d.d();
    }
}
