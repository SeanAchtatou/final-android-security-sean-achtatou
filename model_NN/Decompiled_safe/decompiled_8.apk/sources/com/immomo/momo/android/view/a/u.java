package com.immomo.momo.android.view.a;

import android.content.Context;
import android.content.DialogInterface;
import com.immomo.momo.android.c.ae;
import com.immomo.momo.android.c.z;
import com.immomo.momo.service.bean.bf;

final class u implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ Context f2705a;
    private final /* synthetic */ bf b;
    private final /* synthetic */ bf c;
    private final /* synthetic */ z d;
    private final /* synthetic */ String[] e;
    private final /* synthetic */ int f;

    u(Context context, bf bfVar, bf bfVar2, z zVar, String[] strArr, int i) {
        this.f2705a = context;
        this.b = bfVar;
        this.c = bfVar2;
        this.d = zVar;
        this.e = strArr;
        this.f = i;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        new ae(this.f2705a, this.b, this.c, this.d).execute(new String[]{this.e[this.f]});
    }
}
