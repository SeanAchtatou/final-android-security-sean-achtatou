package com.immomo.momo.android.activity;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.protocol.a.w;
import com.immomo.momo.service.aq;
import com.immomo.momo.service.bean.bf;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

final class mr extends d {

    /* renamed from: a  reason: collision with root package name */
    private boolean f2009a = false;
    private /* synthetic */ VisitorListActivity c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public mr(VisitorListActivity visitorListActivity, Context context) {
        super(context);
        this.c = visitorListActivity;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        ArrayList<bf> arrayList = new ArrayList<>();
        AtomicInteger atomicInteger = new AtomicInteger();
        this.f2009a = w.a().a(arrayList, atomicInteger, 0);
        this.c.n = atomicInteger.get();
        aq unused = this.c.k;
        aq.c(this.c.n);
        this.c.k.e(arrayList);
        this.c.m.clear();
        for (bf add : arrayList) {
            this.c.m.add(add);
        }
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.c.l = this;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        List list = (List) obj;
        Date date = new Date();
        this.c.h.setLastFlushTime(date);
        this.c.g.a("visitor_lasttime_success", date);
        if (!this.c.p) {
            List a2 = this.c.j.a();
            a2.clear();
            a2.addAll(list);
            this.c.j.notifyDataSetChanged();
            this.c.c(this.c.n);
            if (!this.f2009a) {
                this.c.h.k();
            } else {
                this.c.i.setVisibility(0);
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.c.h.n();
        this.c.l = (mr) null;
    }
}
