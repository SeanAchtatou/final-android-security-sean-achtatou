package com.immomo.momo.android.activity.common;

import android.content.Context;
import android.net.Uri;
import android.support.v4.b.a;
import com.immomo.a.a.f.b;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.util.h;
import java.io.File;

final class ab extends d {

    /* renamed from: a  reason: collision with root package name */
    private int f1074a;
    private String c;
    private Uri d;
    private v e;
    private /* synthetic */ CommonShareActivity f;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ab(CommonShareActivity commonShareActivity, Context context, int i, Uri uri, String str) {
        super(context);
        this.f = commonShareActivity;
        this.f1074a = i;
        this.d = uri;
        this.c = str;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        if (h.a(a.a(this.d, this.f, 720, 3000), new File(com.immomo.momo.a.i(), String.valueOf(System.currentTimeMillis()) + "_" + b.a())) != null) {
            return null;
        }
        throw new Exception("获取图片失败");
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.e = new v(this.f);
        this.e.a("请求提交中");
        this.e.setCancelable(true);
        this.e.setOnCancelListener(new ac(this));
        this.f.a(this.e);
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        super.a(exc);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        if (!a.a((CharSequence) ((String) obj))) {
            this.f.a(new z(this.f, this.f, this.f1074a, this.c)).execute(new Object[0]);
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.f.p();
    }
}
