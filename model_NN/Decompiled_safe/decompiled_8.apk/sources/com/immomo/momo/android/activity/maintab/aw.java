package com.immomo.momo.android.activity.maintab;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import com.immomo.momo.android.activity.OtherProfileActivity;
import com.immomo.momo.service.bean.bf;

final class aw implements AdapterView.OnItemClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ aq f1846a;

    aw(aq aqVar) {
        this.f1846a = aqVar;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        aq aqVar = this.f1846a;
        Intent intent = new Intent(aq.H(), OtherProfileActivity.class);
        intent.putExtra("momoid", ((bf) this.f1846a.O.get(i)).h);
        intent.putExtra("tag", "local");
        this.f1846a.a(intent);
    }
}
