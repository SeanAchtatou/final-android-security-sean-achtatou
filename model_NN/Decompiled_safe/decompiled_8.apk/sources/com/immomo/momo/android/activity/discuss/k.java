package com.immomo.momo.android.activity.discuss;

import android.content.Context;
import android.content.Intent;
import com.immomo.momo.android.broadcast.r;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.h;
import com.immomo.momo.service.bean.n;
import com.immomo.momo.service.bean.p;
import java.util.ArrayList;
import java.util.List;

final class k extends d {

    /* renamed from: a  reason: collision with root package name */
    private v f1237a = null;
    private String c;
    private List d;
    private /* synthetic */ DiscussMemberListActivity e;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public k(DiscussMemberListActivity discussMemberListActivity, Context context, List list, String str) {
        super(context);
        this.e = discussMemberListActivity;
        this.c = str;
        this.d = list;
        this.f1237a = new v(context);
        this.f1237a.setCancelable(true);
        this.f1237a.setOnCancelListener(new l(this));
        this.f1237a.a("请求提交中");
        discussMemberListActivity.a(this.f1237a);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.service.h.a(java.lang.String, boolean):com.immomo.momo.service.bean.n
     arg types: [java.lang.String, int]
     candidates:
      com.immomo.momo.service.h.a(com.immomo.momo.service.bean.n, java.lang.String):void
      com.immomo.momo.service.h.a(java.lang.String, int):void
      com.immomo.momo.service.h.a(java.lang.String, java.lang.String):void
      com.immomo.momo.service.h.a(java.lang.String, java.lang.String[]):void
      com.immomo.momo.service.h.a(java.util.List, java.lang.String):void
      com.immomo.momo.service.h.a(java.lang.String, boolean):com.immomo.momo.service.bean.n */
    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        ArrayList<String> arrayList = new ArrayList<>();
        for (p pVar : this.d) {
            if (pVar != null) {
                arrayList.add(pVar.f3034a);
            }
        }
        h.a().a(this.c, arrayList);
        for (String c2 : arrayList) {
            this.e.l.c(c2, this.c);
        }
        int c3 = this.e.l.c(this.c);
        n a2 = this.e.l.a(this.c, false);
        if (a2 != null) {
            a2.j = c3;
            this.e.l.b(this.c, a2.j);
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        super.a(exc);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        this.e.u = true;
        this.e.m.a().removeAll(this.d);
        this.e.m.notifyDataSetChanged();
        if (this.e.h != null) {
            this.e.h.e();
        }
        this.e.a(this.e.l.c(this.c, this.e.q));
        this.e.sendBroadcast(new Intent(r.f2361a));
    }

    /* access modifiers changed from: protected */
    public final void b() {
        super.b();
        this.e.p();
    }
}
