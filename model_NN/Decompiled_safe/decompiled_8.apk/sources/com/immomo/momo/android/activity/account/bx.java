package com.immomo.momo.android.activity.account;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.b.a;
import com.immomo.momo.R;
import com.immomo.momo.a.g;
import com.immomo.momo.a.i;
import com.immomo.momo.a.j;
import com.immomo.momo.a.k;
import com.immomo.momo.a.l;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.n;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.w;
import com.immomo.momo.util.jni.Codec;
import org.json.JSONException;

final class bx extends d {

    /* renamed from: a  reason: collision with root package name */
    private v f977a = null;
    private /* synthetic */ bn c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public bx(bn bnVar, Context context) {
        super(context);
        this.c = bnVar;
    }

    /* access modifiers changed from: protected */
    public final Object a(Object... objArr) {
        long currentTimeMillis = System.currentTimeMillis();
        bn bnVar = this.c;
        bn bnVar2 = this.c;
        int i = (int) (((currentTimeMillis - 0) / 1000) + 0);
        w.a().b(this.c.g.c, this.c.g.b, this.c.q.r, i, a.b(Codec.gpi(this.c.q, i)));
        return null;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.f977a = new v(this.c.q, "正在验证手机号，请稍候...");
        this.f977a.setOnCancelListener(new by(this));
        this.f977a.show();
        super.a();
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        if (exc instanceof com.immomo.momo.a.w) {
            this.c.q.a((CharSequence) exc.getMessage());
        } else if ((exc instanceof l) && !this.c.q.isFinishing()) {
            n b = n.b(this.c.q, exc.getMessage(), (DialogInterface.OnClickListener) null);
            b.setCancelable(false);
            b.show();
        } else if (exc instanceof g) {
            this.c.q.a((CharSequence) exc.getMessage());
        } else if (exc instanceof j) {
            this.c.q.s = true;
            a((Object) null);
        } else if (exc instanceof i) {
            this.c.h();
        } else if (exc instanceof k) {
            this.c.h();
        } else if (exc instanceof JSONException) {
            this.c.q.a((int) R.string.errormsg_dataerror);
        } else if (exc instanceof com.immomo.momo.a.a) {
            this.c.q.a((CharSequence) exc.getMessage());
        } else if (!"mobile".equals(com.immomo.momo.g.Y()) || !com.immomo.momo.g.ac()) {
            this.c.q.a((int) R.string.errormsg_server);
        } else {
            this.c.q.g();
        }
    }

    /* access modifiers changed from: protected */
    public final void a(Object obj) {
        this.c.p = String.valueOf(this.c.g.c) + this.c.g.b;
        this.c.c();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.f977a.dismiss();
        this.f977a = null;
    }
}
