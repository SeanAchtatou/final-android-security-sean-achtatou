package com.immomo.momo.android.activity;

import android.content.DialogInterface;
import android.content.Intent;
import com.immomo.momo.android.pay.BuyMemberActivity;

final class lv implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ UserRoamActivity f1821a;

    lv(UserRoamActivity userRoamActivity) {
        this.f1821a = userRoamActivity;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f1821a.startActivity(new Intent(this.f1821a.getApplicationContext(), BuyMemberActivity.class));
    }
}
