package com.immomo.momo.android.activity.account;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.b.a;
import com.immomo.momo.R;
import com.immomo.momo.a.i;
import com.immomo.momo.a.j;
import com.immomo.momo.a.l;
import com.immomo.momo.a.u;
import com.immomo.momo.a.x;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.n;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.g;
import com.immomo.momo.protocol.a.w;
import com.immomo.momo.service.bean.bf;
import java.util.concurrent.atomic.AtomicInteger;
import org.json.JSONException;

final class k extends d {

    /* renamed from: a  reason: collision with root package name */
    private v f1005a = null;
    private AtomicInteger c;
    private /* synthetic */ LoginActivity d;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public k(LoginActivity loginActivity, Context context) {
        super(context);
        this.d = loginActivity;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        this.c = new AtomicInteger();
        return w.a().a(this.d.z, a.n(this.d.y), this.d.h, com.immomo.momo.a.B(), this.d.j, this.c);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.f1005a = new v(this.d, (int) R.string.login_posting);
        this.f1005a.setOnCancelListener(new l(this));
        this.f1005a.show();
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        this.b.a((Throwable) exc);
        if ((exc instanceof l) && !this.d.isFinishing()) {
            n b = n.b(this.d, exc.getMessage(), (DialogInterface.OnClickListener) null);
            b.setCancelable(false);
            b.show();
        } else if (exc instanceof i) {
            this.d.g();
        } else if (exc instanceof j) {
            this.d.f();
        } else if (exc instanceof com.immomo.momo.a.k) {
            this.d.f();
        } else if (exc instanceof com.immomo.momo.a.a) {
            a(exc.getMessage());
        } else if (exc instanceof JSONException) {
            a((int) R.string.errormsg_dataerror);
        } else if (exc instanceof u) {
            this.d.a(n.b(this.d, (int) R.string.errormsg_devices, (DialogInterface.OnClickListener) null));
        } else if (exc instanceof x) {
            a((int) R.string.errormsg_dataerror);
        } else if (!"mobile".equals(g.Y()) || !g.ac()) {
            a((int) R.string.errormsg_server);
        } else {
            this.d.a(n.b(this.d, (int) R.string.errormsg_net_cmwap, (DialogInterface.OnClickListener) null));
        }
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        LoginActivity.a(this.d, (bf) obj, this.c.get());
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.f1005a.dismiss();
        this.f1005a = null;
    }
}
