package com.immomo.momo.android.pay;

import android.content.Context;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.g;
import com.immomo.momo.protocol.a.a;
import com.unicom.dcLoader.Utils;
import java.util.Map;

final class y extends d {

    /* renamed from: a  reason: collision with root package name */
    private v f2576a = null;
    private Map c;
    private /* synthetic */ BuyMemberActivity d;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public y(BuyMemberActivity buyMemberActivity, Context context, Map map) {
        super(context);
        this.d = buyMemberActivity;
        this.c = map;
        this.f2576a = new v(context);
        buyMemberActivity.D = new bk();
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        return a.a().a(this.d.D, g.E(), g.D(), this.d.v(), this.c);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
        this.f2576a.setCancelable(true);
        this.f2576a.setOnCancelListener(new z(this));
        this.f2576a.a("请求提交中...");
        this.d.a(this.f2576a);
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        super.a(exc);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        if (!this.d.B) {
            Utils.a().a(this.d.n(), this.d.D.l, this.d.D.f2554a, this.d.D.e, this.d.D.h, this.d.D.j, this.d.D.k, new t(this.d));
            this.d.B = true;
        }
        Utils.a().a(this.d.n(), this.d.D.f);
        String str = this.d.D.g;
        String str2 = this.d.D.b;
        String I = g.I();
        BuyMemberActivity buyMemberActivity = this.d;
        Utils.a().a(this.d.n(), this.d.D.c, PoiTypeDef.All, (String) this.c.get("subject"), str, str2, I, BuyMemberActivity.c(this.d.D.d), new t(this.d));
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.d.p();
    }
}
