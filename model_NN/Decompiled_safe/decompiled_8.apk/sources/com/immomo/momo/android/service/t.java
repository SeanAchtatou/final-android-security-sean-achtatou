package com.immomo.momo.android.service;

import com.immomo.a.a.b;
import com.immomo.a.a.b.a;
import com.immomo.momo.protocol.imjson.l;
import com.immomo.momo.protocol.imjson.p;
import com.immomo.momo.protocol.imjson.task.DeviceSetTask;

final class t implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ XService f2619a;

    public t(XService xService) {
        this.f2619a = xService;
    }

    public final void run() {
        b b;
        this.f2619a.n.lock();
        long currentTimeMillis = System.currentTimeMillis();
        try {
            b = this.f2619a.b.b();
            this.f2619a.b.g();
            this.f2619a.b.h();
            XService.e(this.f2619a);
            l.d = b.a();
            l.e = b.b();
            if (!this.f2619a.l.equals(this.f2619a.b.b().e())) {
                p.a(new DeviceSetTask(this.f2619a.l));
                b.e(this.f2619a.l);
            }
        } catch (Exception e) {
            this.f2619a.m.a((Throwable) e);
            if (e instanceof a) {
                if (((a) e).a() == 403) {
                    this.f2619a.h.e();
                    this.f2619a.b.k();
                    this.f2619a.f2602a.d();
                } else if (((a) e).a() == 405) {
                    XService.h(this.f2619a).l();
                }
                return;
            }
            if (b.a().equals(com.immomo.momo.a.f669a) && b.b() == com.immomo.momo.a.b) {
                new Thread(new u(this)).start();
            }
            this.f2619a.a(true);
        } finally {
            this.f2619a.k = false;
            this.f2619a.n.unlock();
            l.l = System.currentTimeMillis() - currentTimeMillis;
        }
    }
}
