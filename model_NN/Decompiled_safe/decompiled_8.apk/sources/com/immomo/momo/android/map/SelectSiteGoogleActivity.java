package com.immomo.momo.android.map;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.location.Location;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapView;
import com.immomo.momo.R;
import com.immomo.momo.android.a.hj;
import com.immomo.momo.android.b.p;
import com.immomo.momo.android.b.z;
import com.immomo.momo.android.view.ResizeListenerLayout;
import com.immomo.momo.android.view.a.n;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.util.ao;
import com.immomo.momo.util.m;
import java.util.ArrayList;
import java.util.List;
import mm.purchasesdk.PurchaseCode;

public class SelectSiteGoogleActivity extends MapActivity implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    ListView f2453a = null;
    EditText b = null;
    p c = null;
    Handler d = new Handler();
    MapView e = null;
    View f = null;
    GeoPoint g = null;
    Bitmap h = null;
    String i = PoiTypeDef.All;
    boolean j;
    /* access modifiers changed from: private */
    public m k = new m(getClass().getSimpleName());
    private ResizeListenerLayout l = null;
    /* access modifiers changed from: private */
    public View m = null;
    /* access modifiers changed from: private */
    public Location n = null;
    /* access modifiers changed from: private */
    public Location o = null;
    /* access modifiers changed from: private */
    public int p = 0;
    /* access modifiers changed from: private */
    public hj q = null;
    /* access modifiers changed from: private */
    public bp r = null;
    /* access modifiers changed from: private */
    public boolean s = false;
    /* access modifiers changed from: private */
    public List t = new ArrayList();
    /* access modifiers changed from: private */
    public int u = 0;
    /* access modifiers changed from: private */
    public TextView v = null;
    private String w = PoiTypeDef.All;
    /* access modifiers changed from: private */
    public v x = null;

    /* JADX WARN: Type inference failed for: r2v0, types: [android.content.Context, com.immomo.momo.android.map.SelectSiteGoogleActivity] */
    /* access modifiers changed from: private */
    public void a() {
        if (z.a(this.n)) {
            b();
            return;
        }
        this.x = new v((Context) this, (int) R.string.getting_loation);
        this.x.show();
        this.x.setOnCancelListener(new bj(this));
        this.c = new bk(this);
        try {
            z.a(new bm(this));
        } catch (Exception e2) {
            ao.g(R.string.errormsg_location_nearby_failed);
            c();
        }
    }

    /* JADX WARN: Type inference failed for: r6v0, types: [com.immomo.momo.android.map.SelectSiteGoogleActivity, android.app.Activity] */
    /* access modifiers changed from: private */
    public void b() {
        if (this.o != null) {
            this.g = new GeoPoint((int) (this.o.getLatitude() * 1000000.0d), (int) (this.o.getLongitude() * 1000000.0d));
            if (this.g != null) {
                this.e.getController().animateTo(this.g);
                this.e.getController().setCenter(this.g);
            }
        }
        if (this.r != null && !this.r.isCancelled()) {
            this.r.cancel(true);
        }
        this.r = new bp(this, this, null, this.u);
        this.r.execute(new Object[0]);
    }

    /* access modifiers changed from: private */
    public void c() {
        this.d.post(new bn(this));
        z.b(this.c);
        if (this.c != null) {
            this.c.b = false;
        }
        if (this.r != null && !this.r.isCancelled()) {
            this.r.cancel(true);
        }
    }

    /* access modifiers changed from: private */
    public String d() {
        switch (this.u) {
            case 0:
                this.w = getString(R.string.editsite_sitename0);
                break;
            case 1:
                this.w = getString(R.string.editsite_sitename1);
                break;
            case 2:
                this.w = getString(R.string.editsite_sitename2);
                break;
            case 3:
                this.w = getString(R.string.editsite_sitename3);
                break;
            case 4:
                this.w = getString(R.string.editsite_sitename4);
                break;
            default:
                this.w = getString(R.string.editsite_defaultbtn_text);
                break;
        }
        return this.w;
    }

    /* access modifiers changed from: protected */
    public boolean isRouteDisplayed() {
        return false;
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        switch (i2) {
            case PurchaseCode.INIT_OK /*100*/:
                if (i3 == -1) {
                    Intent intent2 = new Intent();
                    intent2.putExtra("siteid", PoiTypeDef.All);
                    intent2.putExtra("sitename", intent.getStringExtra("sitename"));
                    intent2.putExtra("sitetype", intent.getIntExtra("sitetype", -1));
                    intent2.putExtra("lat", this.n.getLatitude());
                    intent2.putExtra("lng", this.n.getLongitude());
                    intent2.putExtra("loctype", this.p);
                    setResult(-1, intent2);
                    finish();
                    return;
                }
                return;
            default:
                return;
        }
    }

    /* JADX WARN: Type inference failed for: r6v0, types: [android.content.Context, com.immomo.momo.android.map.SelectSiteGoogleActivity] */
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.layout_sitename /*2131165794*/:
                bo boVar = new bo(this);
                n nVar = new n(this);
                bd bdVar = new bd(this, boVar, nVar);
                nVar.setTitle((int) R.string.editsite_dialog_title);
                View inflate = getLayoutInflater().inflate((int) R.layout.dialog_select_sitetype, (ViewGroup) null);
                inflate.findViewById(R.id.layout_type_all).setOnClickListener(bdVar);
                inflate.findViewById(R.id.layout_type_department).setOnClickListener(bdVar);
                inflate.findViewById(R.id.layout_type_school).setOnClickListener(bdVar);
                inflate.findViewById(R.id.layout_type_restaurant).setOnClickListener(bdVar);
                inflate.findViewById(R.id.layout_type_office).setOnClickListener(bdVar);
                inflate.findViewById(R.id.layout_type_trafic).setOnClickListener(bdVar);
                inflate.findViewById(R.id.layout_type_entertainment).setOnClickListener(bdVar);
                nVar.setContentView(inflate);
                nVar.setOnCancelListener(new be(boVar));
                nVar.show();
                return;
            default:
                return;
        }
    }

    /* JADX WARN: Type inference failed for: r5v0, types: [android.content.Context, com.immomo.momo.android.map.SelectSiteGoogleActivity, android.view.View$OnClickListener, com.google.android.maps.MapActivity] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    protected void onCreate(android.os.Bundle r6) {
        /*
            r5 = this;
            r4 = 0
            r3 = 1
            com.immomo.momo.android.map.SelectSiteGoogleActivity.super.onCreate(r6)
            r0 = 2130903186(0x7f030092, float:1.7413183E38)
            r5.setContentView(r0)
            android.content.Intent r0 = r5.getIntent()
            java.lang.String r1 = "sitetype"
            int r0 = r0.getIntExtra(r1, r3)
            r5.u = r0
            r0 = 2131165197(0x7f07000d, float:1.7944604E38)
            android.view.View r0 = r5.findViewById(r0)
            com.immomo.momo.android.view.HeaderLayout r0 = (com.immomo.momo.android.view.HeaderLayout) r0
            r1 = 2131493313(0x7f0c01c1, float:1.8610103E38)
            r0.setTitleText(r1)
            r0 = 2131165656(0x7f0701d8, float:1.7945535E38)
            android.view.View r0 = r5.findViewById(r0)
            com.immomo.momo.android.view.ResizeListenerLayout r0 = (com.immomo.momo.android.view.ResizeListenerLayout) r0
            r5.l = r0
            r0 = 2131165709(0x7f07020d, float:1.7945643E38)
            android.view.View r0 = r5.findViewById(r0)
            android.widget.TextView r0 = (android.widget.TextView) r0
            r5.v = r0
            android.widget.TextView r0 = r5.v
            java.lang.String r1 = r5.d()
            r0.setText(r1)
            r0 = 2131165658(0x7f0701da, float:1.794554E38)
            android.view.View r0 = r5.findViewById(r0)
            r5.f = r0
            android.view.LayoutInflater r0 = r5.getLayoutInflater()
            r1 = 2130903535(0x7f0301ef, float:1.741389E38)
            r2 = 0
            android.view.View r0 = r0.inflate(r1, r2)
            r5.m = r0
            android.view.View r0 = r5.m
            r0.setClickable(r3)
            android.view.View r0 = r5.m
            r0.requestFocus()
            r0 = 2131165255(0x7f070047, float:1.7944722E38)
            android.view.View r0 = r5.findViewById(r0)
            android.widget.ListView r0 = (android.widget.ListView) r0
            r5.f2453a = r0
            android.widget.ListView r0 = r5.f2453a
            android.view.View r1 = r5.m
            r0.addFooterView(r1)
            r0 = 2131166314(0x7f07046a, float:1.794687E38)
            android.view.View r0 = r5.findViewById(r0)
            android.widget.EditText r0 = (android.widget.EditText) r0
            r5.b = r0
            android.widget.EditText r0 = r5.b
            r1 = 2131493258(0x7f0c018a, float:1.8609991E38)
            r0.setHint(r1)
            r0 = 2131166315(0x7f07046b, float:1.7946872E38)
            r5.findViewById(r0)
            r0 = 2131165395(0x7f0700d3, float:1.7945006E38)
            android.view.View r0 = r5.findViewById(r0)
            com.google.android.maps.MapView r0 = (com.google.android.maps.MapView) r0
            r5.e = r0
            com.google.android.maps.MapView r0 = r5.e
            r0.setBuiltInZoomControls(r3)
            com.google.android.maps.MapView r0 = r5.e
            r0.displayZoomControls(r3)
            com.google.android.maps.MapView r0 = r5.e
            r0.setEnabled(r3)
            com.google.android.maps.MapView r0 = r5.e
            r0.setClickable(r3)
            com.google.android.maps.MapView r0 = r5.e
            r0.setFocusable(r4)
            com.google.android.maps.MapView r0 = r5.e
            r0.setBuiltInZoomControls(r4)
            com.google.android.maps.MapView r0 = r5.e
            com.google.android.maps.MapController r0 = r0.getController()
            r1 = 19
            r0.setZoom(r1)
            android.widget.ListView r0 = r5.f2453a
            com.immomo.momo.android.a.hj r1 = new com.immomo.momo.android.a.hj
            r1.<init>(r5)
            r5.q = r1
            r0.setAdapter(r1)
            android.widget.ListView r0 = r5.f2453a
            android.view.View r1 = r5.m
            r0.removeFooterView(r1)
            com.google.android.maps.MapView r0 = r5.e
            java.util.List r0 = r0.getOverlays()
            com.immomo.momo.android.map.br r1 = new com.immomo.momo.android.map.br
            com.google.android.maps.MapView r2 = r5.e
            r1.<init>(r5, r5, r2)
            r0.add(r1)
            r0 = 2131165794(0x7f070262, float:1.7945815E38)
            android.view.View r0 = r5.findViewById(r0)
            r0.setOnClickListener(r5)
            com.immomo.momo.android.view.ResizeListenerLayout r0 = r5.l
            com.immomo.momo.android.map.bc r1 = new com.immomo.momo.android.map.bc
            r1.<init>(r5)
            r0.setOnResizeListener(r1)
            com.immomo.momo.android.map.bf r0 = new com.immomo.momo.android.map.bf
            r0.<init>(r5)
            android.widget.EditText r1 = r5.b
            android.text.InputFilter[] r2 = new android.text.InputFilter[r3]
            r2[r4] = r0
            r1.setFilters(r2)
            android.widget.EditText r0 = r5.b
            com.immomo.momo.android.map.bg r1 = new com.immomo.momo.android.map.bg
            r1.<init>(r5)
            r0.addTextChangedListener(r1)
            android.widget.ListView r0 = r5.f2453a
            com.immomo.momo.android.map.bi r1 = new com.immomo.momo.android.map.bi
            r1.<init>(r5)
            r0.setOnItemClickListener(r1)
            r5.a()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.immomo.momo.android.map.SelectSiteGoogleActivity.onCreate(android.os.Bundle):void");
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        SelectSiteGoogleActivity.super.onDestroy();
        if (this.r != null && !this.r.isCancelled()) {
            this.r.cancel(true);
            this.r = null;
        }
        c();
    }
}
