package com.immomo.momo.android.view;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.view.View;

final class dv extends AnimatorListenerAdapter {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ VerticalContainer f2803a;
    private final /* synthetic */ View b;

    dv(VerticalContainer verticalContainer, View view) {
        this.f2803a = verticalContainer;
        this.b = view;
    }

    public final void onAnimationEnd(Animator animator) {
        dv.super.removeView(this.b);
        this.f2803a.b.remove(this.b);
        this.f2803a.requestLayout();
    }
}
