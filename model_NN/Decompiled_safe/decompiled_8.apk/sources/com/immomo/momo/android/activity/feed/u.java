package com.immomo.momo.android.activity.feed;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.protocol.a.k;
import com.immomo.momo.service.bean.ab;

final class u extends d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ FeedProfileActivity f1486a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public u(FeedProfileActivity feedProfileActivity, Context context) {
        super(context);
        this.f1486a = feedProfileActivity;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        ab c = k.a().c(this.f1486a.j);
        if (c.i == 2) {
            this.f1486a.m.f(this.f1486a.j);
        } else {
            this.f1486a.m.b(c);
        }
        return c;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        ab abVar = (ab) obj;
        this.f1486a.k = abVar;
        if (abVar.i == 2) {
            a("该留言已经被删除");
            this.f1486a.finish();
            return;
        }
        this.f1486a.a(abVar);
    }
}
