package com.immomo.momo.android.activity;

import android.content.Context;
import android.support.v4.b.a;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.android.c.d;
import com.immomo.momo.service.c;

final class hw extends d {

    /* renamed from: a  reason: collision with root package name */
    private String f1728a = PoiTypeDef.All;
    private /* synthetic */ OtherProfileActivity c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public hw(OtherProfileActivity otherProfileActivity, Context context, String str) {
        super(context);
        this.c = otherProfileActivity;
        this.f1728a = str;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        String a2 = c.c().a(this.f1728a);
        if (!a.a((CharSequence) a2)) {
            this.c.t.o = a2;
            this.c.q.b(a2, this.c.t.h);
        }
        return a2;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        String str = (String) obj;
        this.b.a((Object) ("GetPhoneContactNameTask, result=" + str));
        if (str != null) {
            this.c.N.setVisibility(0);
            this.c.ae.setText(str);
            return;
        }
        this.c.N.setVisibility(8);
    }
}
