package com.immomo.momo.android.plugin.a;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import com.immomo.momo.util.i;
import com.immomo.momo.util.jni.ImageFilter;
import java.io.InputStream;
import mm.purchasesdk.PurchaseCode;

public final class a {
    static {
        float[] fArr = new float[PurchaseCode.QUERY_OK];
        fArr[0] = 0.0f;
        fArr[1] = 0.01f;
        fArr[2] = 0.02f;
        fArr[3] = 0.04f;
        fArr[4] = 0.05f;
        fArr[5] = 0.06f;
        fArr[6] = 0.07f;
        fArr[7] = 0.08f;
        fArr[8] = 0.1f;
        fArr[9] = 0.11f;
        fArr[10] = 0.12f;
        fArr[11] = 0.14f;
        fArr[12] = 0.15f;
        fArr[13] = 0.16f;
        fArr[14] = 0.17f;
        fArr[15] = 0.18f;
        fArr[16] = 0.2f;
        fArr[17] = 0.21f;
        fArr[18] = 0.22f;
        fArr[19] = 0.24f;
        fArr[20] = 0.25f;
        fArr[21] = 0.27f;
        fArr[22] = 0.28f;
        fArr[23] = 0.3f;
        fArr[24] = 0.32f;
        fArr[25] = 0.34f;
        fArr[26] = 0.36f;
        fArr[27] = 0.38f;
        fArr[28] = 0.4f;
        fArr[29] = 0.42f;
        fArr[30] = 0.44f;
        fArr[31] = 0.46f;
        fArr[32] = 0.48f;
        fArr[33] = 0.5f;
        fArr[34] = 0.53f;
        fArr[35] = 0.56f;
        fArr[36] = 0.59f;
        fArr[37] = 0.62f;
        fArr[38] = 0.65f;
        fArr[39] = 0.68f;
        fArr[40] = 0.71f;
        fArr[41] = 0.74f;
        fArr[42] = 0.77f;
        fArr[43] = 0.8f;
        fArr[44] = 0.83f;
        fArr[45] = 0.86f;
        fArr[46] = 0.89f;
        fArr[47] = 0.92f;
        fArr[48] = 0.95f;
        fArr[49] = 0.98f;
        fArr[50] = 1.0f;
        fArr[51] = 1.06f;
        fArr[52] = 1.12f;
        fArr[53] = 1.18f;
        fArr[54] = 1.24f;
        fArr[55] = 1.3f;
        fArr[56] = 1.36f;
        fArr[57] = 1.42f;
        fArr[58] = 1.48f;
        fArr[59] = 1.54f;
        fArr[60] = 1.6f;
        fArr[61] = 1.66f;
        fArr[62] = 1.72f;
        fArr[63] = 1.78f;
        fArr[64] = 1.84f;
        fArr[65] = 1.9f;
        fArr[66] = 1.96f;
        fArr[67] = 2.0f;
        fArr[68] = 2.12f;
        fArr[69] = 2.25f;
        fArr[70] = 2.37f;
        fArr[71] = 2.5f;
        fArr[72] = 2.62f;
        fArr[73] = 2.75f;
        fArr[74] = 2.87f;
        fArr[75] = 3.0f;
        fArr[76] = 3.2f;
        fArr[77] = 3.4f;
        fArr[78] = 3.6f;
        fArr[79] = 3.8f;
        fArr[80] = 4.0f;
        fArr[81] = 4.3f;
        fArr[82] = 4.7f;
        fArr[83] = 4.9f;
        fArr[84] = 5.0f;
        fArr[85] = 5.5f;
        fArr[86] = 6.0f;
        fArr[87] = 6.5f;
        fArr[88] = 6.8f;
        fArr[89] = 7.0f;
        fArr[90] = 7.3f;
        fArr[91] = 7.5f;
        fArr[92] = 7.8f;
        fArr[93] = 8.0f;
        fArr[94] = 8.4f;
        fArr[95] = 8.7f;
        fArr[96] = 9.0f;
        fArr[97] = 9.4f;
        fArr[98] = 9.6f;
        fArr[99] = 9.8f;
        fArr[100] = 10.0f;
    }

    private static int a(int i) {
        return Math.min(100, Math.max(-100, i));
    }

    public static Bitmap a(Context context, Bitmap bitmap, float f, String str, String str2) {
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        Bitmap createBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(createBitmap);
        Paint paint = new Paint();
        paint.setFilterBitmap(false);
        canvas.drawBitmap(bitmap, 0.0f, 0.0f, paint);
        a(context, canvas, createBitmap, width, height, f, str, str2);
        return createBitmap;
    }

    private static Bitmap a(Context context, String str) {
        Bitmap bitmap = null;
        try {
            InputStream open = context.getResources().getAssets().open("filters/" + str);
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inScaled = false;
            bitmap = BitmapFactory.decodeStream(open, null, options);
            if (open != null) {
                open.close();
            }
        } catch (Throwable th) {
        }
        return bitmap;
    }

    public static Bitmap a(Bitmap bitmap) {
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        int[] iArr = new int[(width * height)];
        bitmap.getPixels(iArr, 0, width, 0, 0, width, height);
        int[] native_ImgToGray = ImageFilter.native_ImgToGray(iArr, width, height);
        Bitmap createBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
        createBitmap.setPixels(native_ImgToGray, 0, width, 0, 0, width, height);
        return createBitmap;
    }

    public static Bitmap a(Bitmap bitmap, float f, float f2) {
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        int[] iArr = new int[256];
        int[] iArr2 = new int[256];
        android.support.v4.b.a.a(f, iArr);
        android.support.v4.b.a.a(f2, iArr2);
        int i = (height + 4) / 4;
        int[] iArr3 = new int[(width * i)];
        Bitmap createBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 < 4 && i3 * i < height) {
                int i4 = (i3 * i) + i > height ? height - (i3 * i) : i;
                if (i4 <= 0) {
                    return createBitmap;
                }
                bitmap.getPixels(iArr3, 0, width, 0, i3 * i, width, i4);
                int i5 = 0;
                for (int i6 = 0; i6 < width; i6++) {
                    for (int i7 = 0; i7 < i4; i7++) {
                        iArr3[i5] = (iArr[(iArr3[i5] >> 16) & PurchaseCode.AUTH_INVALID_APP] << 16) | -16777216 | (iArr[(iArr3[i5] >> 8) & PurchaseCode.AUTH_INVALID_APP] << 8) | iArr2[iArr3[i5] & PurchaseCode.AUTH_INVALID_APP];
                        i5++;
                    }
                }
                createBitmap.setPixels(iArr3, 0, width, 0, i3 * i, width, i4);
                i2 = i3 + 1;
            }
        }
        return createBitmap;
    }

    private static void a(Context context, Canvas canvas, Bitmap bitmap, int i, int i2, float f, String str, String str2) {
        Bitmap createBitmap = Bitmap.createBitmap(i, i2, Bitmap.Config.ARGB_8888);
        Canvas canvas2 = new Canvas(createBitmap);
        Paint paint = new Paint();
        paint.setFilterBitmap(false);
        Bitmap a2 = a(context, str2);
        canvas2.drawBitmap(a2, (Rect) null, new Rect(0, 0, i, i2), paint);
        i.a(a2);
        a(bitmap, createBitmap, str, f);
        canvas.drawBitmap(createBitmap, 0.0f, 0.0f, paint);
    }

    private static void a(Bitmap bitmap, Bitmap bitmap2, String str, float f) {
        if (bitmap2 != null && bitmap != null) {
            int width = bitmap.getWidth();
            int height = bitmap.getHeight();
            if (bitmap2.getWidth() == width && bitmap2.getHeight() == height) {
                int i = height < 100 ? 2 : 8;
                int i2 = (height + i) / i;
                int[] iArr = new int[(width * i2)];
                int[] iArr2 = new int[(width * i2)];
                int i3 = 0;
                while (true) {
                    int i4 = i3;
                    if (i4 < i && i4 * i2 < height && i4 < i && i4 * i2 < height) {
                        int i5 = (i4 * i2) + i2 > height ? height - (i4 * i2) : i2;
                        if (i5 > 0) {
                            bitmap.getPixels(iArr, 0, width, 0, i4 * i2, width, i5);
                            bitmap2.getPixels(iArr2, 0, width, 0, i4 * i2, width, i5);
                            float f2 = f / 255.0f;
                            int i6 = 0;
                            if ("Overlay".equals(str)) {
                                int i7 = 0;
                                while (i7 < width) {
                                    int i8 = 0;
                                    int i9 = i6;
                                    while (i8 < i5) {
                                        int i10 = (iArr[i9] >> 16) & PurchaseCode.AUTH_INVALID_APP;
                                        int i11 = (iArr[i9] >> 8) & PurchaseCode.AUTH_INVALID_APP;
                                        int i12 = iArr[i9] & PurchaseCode.AUTH_INVALID_APP;
                                        int i13 = (iArr[i9] >> 24) & PurchaseCode.AUTH_INVALID_APP;
                                        int i14 = (iArr2[i9] >> 16) & PurchaseCode.AUTH_INVALID_APP;
                                        int i15 = (iArr2[i9] >> 8) & PurchaseCode.AUTH_INVALID_APP;
                                        int i16 = iArr2[i9] & PurchaseCode.AUTH_INVALID_APP;
                                        int i17 = (iArr2[i9] >> 24) & PurchaseCode.AUTH_INVALID_APP;
                                        int i18 = i10 < 128 ? (i14 * i10) >> 7 : 255 - (((255 - i14) * (255 - i10)) >> 7);
                                        int i19 = i11 < 128 ? (i15 * i11) >> 7 : 255 - (((255 - i15) * (255 - i11)) >> 7);
                                        int i20 = i12 < 128 ? (i16 * i12) >> 7 : 255 - (((255 - i16) * (255 - i12)) >> 7);
                                        float f3 = ((float) i17) * f2;
                                        float f4 = 1.0f - f3;
                                        iArr[i9] = ((int) ((((float) i20) * f3) + (((float) i12) * f4))) | (((int) ((((float) i19) * f3) + (((float) i11) * f4))) << 8) | (((int) ((((float) i18) * f3) + (((float) i10) * f4))) << 16) | (((int) ((((float) i17) * f) + (((float) i13) * f4))) << 24);
                                        i8++;
                                        i9++;
                                    }
                                    i7++;
                                    i6 = i9;
                                }
                            } else if ("hardlight".equals(str)) {
                                int i21 = 0;
                                while (i21 < width) {
                                    int i22 = 0;
                                    int i23 = i6;
                                    while (i22 < i5) {
                                        int i24 = (iArr[i23] >> 16) & PurchaseCode.AUTH_INVALID_APP;
                                        int i25 = (iArr[i23] >> 8) & PurchaseCode.AUTH_INVALID_APP;
                                        int i26 = iArr[i23] & PurchaseCode.AUTH_INVALID_APP;
                                        int i27 = (iArr[i23] >> 24) & PurchaseCode.AUTH_INVALID_APP;
                                        int i28 = (iArr2[i23] >> 16) & PurchaseCode.AUTH_INVALID_APP;
                                        int i29 = (iArr2[i23] >> 8) & PurchaseCode.AUTH_INVALID_APP;
                                        int i30 = iArr2[i23] & PurchaseCode.AUTH_INVALID_APP;
                                        int i31 = (iArr2[i23] >> 24) & PurchaseCode.AUTH_INVALID_APP;
                                        int i32 = i28 < 128 ? (i28 * i24) >> 7 : 255 - (((255 - i28) * (255 - i24)) >> 7);
                                        int i33 = i29 < 128 ? (i29 * i25) >> 7 : 255 - (((255 - i29) * (255 - i25)) >> 7);
                                        int i34 = i30 < 128 ? (i30 * i26) >> 7 : 255 - (((255 - i30) * (255 - i26)) >> 7);
                                        float f5 = ((float) i31) * f2;
                                        float f6 = 1.0f - f5;
                                        iArr[i23] = ((int) ((((float) i34) * f5) + (((float) i26) * f6))) | (((int) ((((float) i33) * f5) + (((float) i25) * f6))) << 8) | (((int) ((((float) i32) * f5) + (((float) i24) * f6))) << 16) | (((int) ((((float) i31) * f) + (((float) i27) * f6))) << 24);
                                        i22++;
                                        i23++;
                                    }
                                    i21++;
                                    i6 = i23;
                                }
                            } else if ("Multiply".equals(str)) {
                                for (int i35 = 0; i35 < width; i35++) {
                                    for (int i36 = 0; i36 < i5; i36++) {
                                        int i37 = (iArr[i6] >> 16) & PurchaseCode.AUTH_INVALID_APP;
                                        int i38 = (iArr[i6] >> 8) & PurchaseCode.AUTH_INVALID_APP;
                                        int i39 = iArr[i6] & PurchaseCode.AUTH_INVALID_APP;
                                        int i40 = (iArr[i6] >> 24) & PurchaseCode.AUTH_INVALID_APP;
                                        int i41 = (iArr2[i6] >> 16) & PurchaseCode.AUTH_INVALID_APP;
                                        int i42 = (iArr2[i6] >> 8) & PurchaseCode.AUTH_INVALID_APP;
                                        int i43 = iArr2[i6] & PurchaseCode.AUTH_INVALID_APP;
                                        int i44 = (iArr2[i6] >> 24) & PurchaseCode.AUTH_INVALID_APP;
                                        float f7 = ((float) i44) * f2;
                                        float f8 = 1.0f - f7;
                                        iArr[i6] = (((int) ((((float) i37) * f8) + (((float) ((i41 * i37) >> 8)) * f7))) << 16) | (((int) ((((float) i40) * f8) + (((float) i44) * f))) << 24) | (((int) ((((float) i38) * f8) + (((float) ((i42 * i38) >> 8)) * f7))) << 8) | ((int) ((((float) i39) * f8) + (((float) ((i43 * i39) >> 8)) * f7)));
                                        i6++;
                                    }
                                }
                            } else if ("colorDodge".equals(str)) {
                                for (int i45 = 0; i45 < width; i45++) {
                                    int i46 = 0;
                                    int i47 = i6;
                                    while (i46 < i5) {
                                        int i48 = (iArr[i47] >> 16) & PurchaseCode.AUTH_INVALID_APP;
                                        int i49 = (iArr[i47] >> 8) & PurchaseCode.AUTH_INVALID_APP;
                                        int i50 = iArr[i47] & PurchaseCode.AUTH_INVALID_APP;
                                        int i51 = (iArr[i47] >> 24) & PurchaseCode.AUTH_INVALID_APP;
                                        int i52 = (iArr2[i47] >> 16) & PurchaseCode.AUTH_INVALID_APP;
                                        int i53 = (iArr2[i47] >> 8) & PurchaseCode.AUTH_INVALID_APP;
                                        int i54 = iArr2[i47] & PurchaseCode.AUTH_INVALID_APP;
                                        int i55 = (iArr2[i47] >> 24) & PurchaseCode.AUTH_INVALID_APP;
                                        if (i52 != 255) {
                                            i52 = Math.min((int) PurchaseCode.AUTH_INVALID_APP, (i48 << 8) / (255 - i52));
                                        }
                                        if (i53 != 255) {
                                            i53 = Math.min((int) PurchaseCode.AUTH_INVALID_APP, (i49 << 8) / (255 - i53));
                                        }
                                        if (i54 != 255) {
                                            i54 = Math.min((int) PurchaseCode.AUTH_INVALID_APP, (i50 << 8) / (255 - i54));
                                        }
                                        if (i55 != 255) {
                                            i55 = Math.min((int) PurchaseCode.AUTH_INVALID_APP, (i51 << 8) / (255 - i55));
                                        }
                                        iArr[i47] = (i55 << 24) | (i52 << 16) | (i53 << 8) | i54;
                                        i46++;
                                        i47++;
                                    }
                                    i6 = i47;
                                }
                            } else if ("colorBurn".equals(str)) {
                                for (int i56 = 0; i56 < width; i56++) {
                                    int i57 = 0;
                                    int i58 = i6;
                                    while (i57 < i5) {
                                        int i59 = (iArr[i58] >> 16) & PurchaseCode.AUTH_INVALID_APP;
                                        int i60 = (iArr[i58] >> 8) & PurchaseCode.AUTH_INVALID_APP;
                                        int i61 = iArr[i58] & PurchaseCode.AUTH_INVALID_APP;
                                        int i62 = (iArr[i58] >> 24) & PurchaseCode.AUTH_INVALID_APP;
                                        int i63 = (iArr2[i58] >> 16) & PurchaseCode.AUTH_INVALID_APP;
                                        int i64 = (iArr2[i58] >> 8) & PurchaseCode.AUTH_INVALID_APP;
                                        int i65 = iArr2[i58] & PurchaseCode.AUTH_INVALID_APP;
                                        int i66 = (iArr2[i58] >> 24) & PurchaseCode.AUTH_INVALID_APP;
                                        if (i63 != 0) {
                                            i63 = Math.max(0, 255 - (((255 - i59) << 8) / i63));
                                        }
                                        if (i64 != 0) {
                                            i64 = Math.max(0, 255 - (((255 - i60) << 8) / i64));
                                        }
                                        if (i65 != 0) {
                                            i65 = Math.max(0, 255 - (((255 - i61) << 8) / i65));
                                        }
                                        if (i66 != 0) {
                                            i66 = Math.max(0, 255 - (((255 - i62) << 8) / i66));
                                        }
                                        iArr[i58] = (i66 << 24) | (i63 << 16) | (i64 << 8) | i65;
                                        i57++;
                                        i58++;
                                    }
                                    i6 = i58;
                                }
                            } else if ("linearColorDodge".equals(str)) {
                                for (int i67 = 0; i67 < width; i67++) {
                                    int i68 = 0;
                                    int i69 = i6;
                                    while (i68 < i5) {
                                        int i70 = (iArr[i69] >> 16) & PurchaseCode.AUTH_INVALID_APP;
                                        int i71 = (iArr[i69] >> 8) & PurchaseCode.AUTH_INVALID_APP;
                                        int i72 = iArr[i69] & PurchaseCode.AUTH_INVALID_APP;
                                        int i73 = (iArr[i69] >> 24) & PurchaseCode.AUTH_INVALID_APP;
                                        int i74 = (iArr2[i69] >> 16) & PurchaseCode.AUTH_INVALID_APP;
                                        int i75 = (iArr2[i69] >> 8) & PurchaseCode.AUTH_INVALID_APP;
                                        int i76 = iArr2[i69] & PurchaseCode.AUTH_INVALID_APP;
                                        int i77 = (iArr2[i69] >> 24) & PurchaseCode.AUTH_INVALID_APP;
                                        int i78 = i70 + i74;
                                        int i79 = i78 >= 255 ? 255 : i78;
                                        int i80 = i75 + i71;
                                        int i81 = i80 >= 255 ? 255 : i80;
                                        int i82 = i76 + i72;
                                        int i83 = i82 >= 255 ? 255 : i82;
                                        int i84 = i77 + i73;
                                        if (i84 >= 255) {
                                            i84 = PurchaseCode.AUTH_INVALID_APP;
                                        }
                                        iArr[i69] = (i84 << 24) | (i79 << 16) | (i81 << 8) | i83;
                                        i68++;
                                        i69++;
                                    }
                                    i6 = i69;
                                }
                            } else if ("linearColorBurn".equals(str)) {
                                for (int i85 = 0; i85 < width; i85++) {
                                    int i86 = 0;
                                    int i87 = i6;
                                    while (i86 < i5) {
                                        int i88 = (iArr[i87] >> 16) & PurchaseCode.AUTH_INVALID_APP;
                                        int i89 = (iArr[i87] >> 8) & PurchaseCode.AUTH_INVALID_APP;
                                        int i90 = iArr[i87] & PurchaseCode.AUTH_INVALID_APP;
                                        int i91 = (iArr[i87] >> 24) & PurchaseCode.AUTH_INVALID_APP;
                                        int i92 = (iArr2[i87] >> 16) & PurchaseCode.AUTH_INVALID_APP;
                                        int i93 = (iArr2[i87] >> 8) & PurchaseCode.AUTH_INVALID_APP;
                                        int i94 = iArr2[i87] & PurchaseCode.AUTH_INVALID_APP;
                                        int i95 = (iArr2[i87] >> 24) & PurchaseCode.AUTH_INVALID_APP;
                                        int i96 = i88 + i92;
                                        int i97 = i96 < 255 ? 0 : i96 - 255;
                                        int i98 = i93 + i89;
                                        int i99 = i98 < 255 ? 0 : i98 - 255;
                                        int i100 = i94 + i90;
                                        int i101 = i100 < 255 ? 0 : i100 - 255;
                                        int i102 = i95 + i91;
                                        iArr[i87] = ((i102 < 255 ? 0 : i102 - 255) << 24) | (i97 << 16) | (i99 << 8) | i101;
                                        i86++;
                                        i87++;
                                    }
                                    i6 = i87;
                                }
                            }
                            bitmap2.setPixels(iArr, 0, width, 0, i4 * i2, width, i5);
                            i3 = i4 + 1;
                        } else {
                            return;
                        }
                    } else {
                        return;
                    }
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(float, float):float} */
    private static void a(ColorMatrix colorMatrix, float f) {
        float min = 3.141593f * (Math.min(180.0f, Math.max(-180.0f, f)) / 180.0f);
        if (min != 0.0f) {
            float cos = (float) Math.cos((double) min);
            float sin = (float) Math.sin((double) min);
            colorMatrix.postConcat(new ColorMatrix(new float[]{0.213f + (0.787f * cos) + (sin * -0.213f), (cos * -0.715f) + 0.715f + (sin * -0.715f), (-0.072f * cos) + 0.072f + (0.928f * sin), 0.0f, 0.0f, 0.213f + (cos * -0.213f) + (0.143f * sin), (0.28500003f * cos) + 0.715f + (0.14f * sin), (-0.072f * cos) + 0.072f + (-0.283f * sin), 0.0f, 0.0f, 0.213f + (cos * -0.213f) + (-0.787f * sin), (cos * -0.715f) + 0.715f + (0.715f * sin), (sin * 0.072f) + (cos * 0.928f) + 0.072f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f}));
        }
    }

    private static void a(ColorMatrix colorMatrix, int i) {
        int a2 = a(i);
        if (a2 != 0) {
            colorMatrix.postConcat(new ColorMatrix(new float[]{1.0f, 0.0f, 0.0f, 0.0f, (float) a2, 0.0f, 1.0f, 0.0f, 0.0f, (float) a2, 0.0f, 0.0f, 1.0f, 0.0f, (float) a2, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f}));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.android.plugin.a.a.a(android.graphics.ColorMatrix, float):void
     arg types: [android.graphics.ColorMatrix, int]
     candidates:
      com.immomo.momo.android.plugin.a.a.a(android.content.Context, java.lang.String):android.graphics.Bitmap
      com.immomo.momo.android.plugin.a.a.a(android.graphics.ColorMatrix, int):void
      com.immomo.momo.android.plugin.a.a.a(android.graphics.ColorMatrix, float):void */
    public static Bitmap b(Context context, Bitmap bitmap, float f, String str, String str2) {
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        Bitmap createBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(createBitmap);
        ColorMatrix colorMatrix = new ColorMatrix();
        a(colorMatrix, -10);
        Paint paint = new Paint();
        paint.setFilterBitmap(false);
        paint.setColorFilter(new ColorMatrixColorFilter(colorMatrix));
        canvas.drawBitmap(bitmap, 0.0f, 0.0f, paint);
        colorMatrix.reset();
        a(colorMatrix, -100.0f);
        c(colorMatrix, -80);
        Paint paint2 = new Paint();
        paint2.setFilterBitmap(false);
        paint2.setColorFilter(new ColorMatrixColorFilter(colorMatrix));
        canvas.drawBitmap(createBitmap, 0.0f, 0.0f, paint2);
        a(context, canvas, createBitmap, width, height, f, str, str2);
        return createBitmap;
    }

    public static Bitmap b(Bitmap bitmap) {
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        int[] iArr = new int[(width * height)];
        bitmap.getPixels(iArr, 0, width, 0, 0, width, height);
        int[] native_ImgOldPhoto = ImageFilter.native_ImgOldPhoto(iArr, width, height);
        Bitmap createBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
        createBitmap.setPixels(native_ImgOldPhoto, 0, width, 0, 0, width, height);
        return createBitmap;
    }

    private static void b(ColorMatrix colorMatrix, int i) {
        int a2 = a(i);
        if (a2 != 0) {
            float f = ((((float) a2) / 100.0f) * 127.0f) + 127.0f;
            colorMatrix.postConcat(new ColorMatrix(new float[]{f / 127.0f, 0.0f, 0.0f, 0.0f, (127.0f - f) * 0.5f, 0.0f, f / 127.0f, 0.0f, 0.0f, (127.0f - f) * 0.5f, 0.0f, 0.0f, f / 127.0f, 0.0f, (127.0f - f) * 0.5f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f}));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.android.plugin.a.a.a(android.graphics.ColorMatrix, float):void
     arg types: [android.graphics.ColorMatrix, int]
     candidates:
      com.immomo.momo.android.plugin.a.a.a(android.content.Context, java.lang.String):android.graphics.Bitmap
      com.immomo.momo.android.plugin.a.a.a(android.graphics.ColorMatrix, int):void
      com.immomo.momo.android.plugin.a.a.a(android.graphics.ColorMatrix, float):void */
    public static Bitmap c(Context context, Bitmap bitmap, float f, String str, String str2) {
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        Bitmap createBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(createBitmap);
        ColorMatrix colorMatrix = new ColorMatrix();
        a(colorMatrix, -58.0f);
        c(colorMatrix, -80);
        Paint paint = new Paint();
        paint.setFilterBitmap(false);
        paint.setColorFilter(new ColorMatrixColorFilter(colorMatrix));
        canvas.drawBitmap(bitmap, 0.0f, 0.0f, paint);
        colorMatrix.reset();
        a(colorMatrix, 6);
        b(colorMatrix, 15);
        Paint paint2 = new Paint();
        paint2.setColorFilter(new ColorMatrixColorFilter(colorMatrix));
        canvas.drawBitmap(createBitmap, 0.0f, 0.0f, paint2);
        a(context, canvas, createBitmap, width, height, f, str, str2);
        return createBitmap;
    }

    public static Bitmap c(Bitmap bitmap) {
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        int[] iArr = new int[(width * height)];
        bitmap.getPixels(iArr, 0, width, 0, 0, width, height);
        int[] native_ImgSoftBY = ImageFilter.native_ImgSoftBY(iArr, width, height);
        Bitmap createBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
        createBitmap.setPixels(native_ImgSoftBY, 0, width, 0, 0, width, height);
        return createBitmap;
    }

    private static void c(ColorMatrix colorMatrix, int i) {
        int a2 = a(i);
        if (a2 != 0) {
            float f = (a2 > 0 ? (((float) a2) * 3.0f) / 100.0f : ((float) a2) / 100.0f) + 1.0f;
            colorMatrix.postConcat(new ColorMatrix(new float[]{((1.0f - f) * 0.3086f) + f, (1.0f - f) * 0.6094f, (1.0f - f) * 0.082f, 0.0f, 0.0f, (1.0f - f) * 0.3086f, ((1.0f - f) * 0.6094f) + f, (1.0f - f) * 0.082f, 0.0f, 0.0f, (1.0f - f) * 0.3086f, (1.0f - f) * 0.6094f, f + ((1.0f - f) * 0.082f), 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f}));
        }
    }

    public static Bitmap d(Context context, Bitmap bitmap, float f, String str, String str2) {
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        Bitmap createBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(createBitmap);
        ColorMatrix colorMatrix = new ColorMatrix();
        a(colorMatrix, 10);
        b(colorMatrix, 50);
        Paint paint = new Paint();
        paint.setFilterBitmap(false);
        paint.setColorFilter(new ColorMatrixColorFilter(colorMatrix));
        canvas.drawBitmap(bitmap, 0.0f, 0.0f, paint);
        a(context, canvas, createBitmap, width, height, f, str, str2);
        return createBitmap;
    }

    public static Bitmap d(Bitmap bitmap) {
        Bitmap createBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(createBitmap);
        ColorMatrix colorMatrix = new ColorMatrix();
        a(colorMatrix, -3);
        b(colorMatrix, 16);
        Paint paint = new Paint();
        paint.setFilterBitmap(false);
        paint.setColorFilter(new ColorMatrixColorFilter(colorMatrix));
        canvas.drawBitmap(bitmap, 0.0f, 0.0f, paint);
        Bitmap j = j(createBitmap);
        i.a(createBitmap);
        return j;
    }

    public static Bitmap e(Context context, Bitmap bitmap, float f, String str, String str2) {
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        Bitmap createBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(createBitmap);
        ColorMatrix colorMatrix = new ColorMatrix();
        a(colorMatrix, 5);
        b(colorMatrix, 20);
        Paint paint = new Paint();
        paint.setFilterBitmap(false);
        paint.setColorFilter(new ColorMatrixColorFilter(colorMatrix));
        canvas.drawBitmap(bitmap, 0.0f, 0.0f, paint);
        a(context, canvas, createBitmap, width, height, f, str, str2);
        return createBitmap;
    }

    public static Bitmap e(Bitmap bitmap) {
        Bitmap createBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(createBitmap);
        ColorMatrix colorMatrix = new ColorMatrix();
        a(colorMatrix, 20);
        c(colorMatrix, Math.abs(25));
        Paint paint = new Paint();
        paint.setFilterBitmap(false);
        paint.setColorFilter(new ColorMatrixColorFilter(colorMatrix));
        canvas.drawBitmap(bitmap, 0.0f, 0.0f, paint);
        Paint paint2 = new Paint();
        paint2.setFilterBitmap(false);
        colorMatrix.reset();
        a(colorMatrix, 5);
        b(colorMatrix, 21);
        paint2.setColorFilter(new ColorMatrixColorFilter(colorMatrix));
        canvas.drawBitmap(createBitmap, 0.0f, 0.0f, paint2);
        return createBitmap;
    }

    public static Bitmap f(Bitmap bitmap) {
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        Bitmap createBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(createBitmap);
        ColorMatrix colorMatrix = new ColorMatrix();
        a(colorMatrix, 20);
        b(colorMatrix, -20);
        Paint paint = new Paint();
        paint.setFilterBitmap(false);
        paint.setColorFilter(new ColorMatrixColorFilter(colorMatrix));
        canvas.drawBitmap(bitmap, 0.0f, 0.0f, paint);
        Bitmap j = j(createBitmap);
        Paint paint2 = new Paint();
        paint2.setFilterBitmap(false);
        canvas.drawBitmap(j, 0.0f, 0.0f, paint2);
        i.a(j);
        Paint paint3 = new Paint();
        paint3.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_OVER));
        paint3.setColor(Color.argb(25, (int) PurchaseCode.AUTH_INVALID_APP, (int) PurchaseCode.AUTH_INVALID_APP, (int) PurchaseCode.ORDER_OK));
        canvas.drawRect(new Rect(0, 0, width, height), paint3);
        return createBitmap;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.android.plugin.a.a.a(android.graphics.ColorMatrix, float):void
     arg types: [android.graphics.ColorMatrix, int]
     candidates:
      com.immomo.momo.android.plugin.a.a.a(android.content.Context, java.lang.String):android.graphics.Bitmap
      com.immomo.momo.android.plugin.a.a.a(android.graphics.ColorMatrix, int):void
      com.immomo.momo.android.plugin.a.a.a(android.graphics.ColorMatrix, float):void */
    public static Bitmap g(Bitmap bitmap) {
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        Bitmap createBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(createBitmap);
        ColorMatrix colorMatrix = new ColorMatrix();
        a(colorMatrix, 50);
        b(colorMatrix, -20);
        Paint paint = new Paint();
        paint.setFilterBitmap(false);
        paint.setColorFilter(new ColorMatrixColorFilter(colorMatrix));
        canvas.drawBitmap(bitmap, 0.0f, 0.0f, paint);
        colorMatrix.reset();
        a(colorMatrix, -10.0f);
        c(colorMatrix, 20);
        Paint paint2 = new Paint();
        paint2.setFilterBitmap(false);
        paint2.setColorFilter(new ColorMatrixColorFilter(colorMatrix));
        canvas.drawBitmap(createBitmap, 0.0f, 0.0f, paint2);
        Bitmap j = j(createBitmap);
        Paint paint3 = new Paint();
        paint3.setFilterBitmap(false);
        canvas.drawBitmap(j, 0.0f, 0.0f, paint3);
        i.a(j);
        Paint paint4 = new Paint();
        paint4.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_OVER));
        paint4.setColor(Color.argb(15, (int) PurchaseCode.AUTH_INVALID_APP, 0, 0));
        canvas.drawRect(new Rect(0, 0, width, height), paint4);
        return createBitmap;
    }

    public static Bitmap h(Bitmap bitmap) {
        Bitmap createBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(createBitmap);
        ColorMatrix colorMatrix = new ColorMatrix();
        colorMatrix.set(new float[]{0.393f, 0.769f, 0.189f, 0.0f, 0.0f, 0.349f, 0.686f, 0.168f, 0.0f, 0.0f, 0.272f, 0.534f, 0.131f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f});
        Paint paint = new Paint();
        paint.setFilterBitmap(false);
        paint.setColorFilter(new ColorMatrixColorFilter(colorMatrix));
        canvas.drawBitmap(bitmap, 0.0f, 0.0f, paint);
        return createBitmap;
    }

    public static Bitmap i(Bitmap bitmap) {
        Bitmap createBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(createBitmap);
        ColorMatrix colorMatrix = new ColorMatrix();
        a(colorMatrix, 15);
        b(colorMatrix, 15);
        Paint paint = new Paint();
        paint.setFilterBitmap(false);
        paint.setColorFilter(new ColorMatrixColorFilter(colorMatrix));
        canvas.drawBitmap(bitmap, 0.0f, 0.0f, paint);
        colorMatrix.reset();
        c(colorMatrix, -31);
        Paint paint2 = new Paint();
        paint2.setFilterBitmap(false);
        paint2.setColorFilter(new ColorMatrixColorFilter(colorMatrix));
        canvas.drawBitmap(createBitmap, 0.0f, 0.0f, paint2);
        Bitmap j = j(createBitmap);
        i.a(createBitmap);
        return j;
    }

    private static Bitmap j(Bitmap bitmap) {
        return a(bitmap, 40.0f, -20.0f);
    }
}
