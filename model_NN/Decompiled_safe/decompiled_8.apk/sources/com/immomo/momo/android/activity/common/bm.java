package com.immomo.momo.android.activity.common;

import com.immomo.momo.android.a.hp;

final class bm implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ bk f1106a;
    private final /* synthetic */ String b;

    bm(bk bkVar, String str) {
        this.f1106a = bkVar;
        this.b = str;
    }

    public final void run() {
        int a2 = this.f1106a.f1104a.P.a(this.b, hp.f862a);
        if (a2 < 0 || a2 > this.f1106a.f1104a.P.getCount()) {
            this.f1106a.f1104a.R = this.f1106a.f1104a.U.c();
            this.f1106a.f1104a.P.b(this.f1106a.f1104a.R);
        } else {
            ((hp) this.f1106a.f1104a.P.getItem(a2)).g.g = 3;
        }
        this.f1106a.f1104a.P.notifyDataSetChanged();
    }
}
