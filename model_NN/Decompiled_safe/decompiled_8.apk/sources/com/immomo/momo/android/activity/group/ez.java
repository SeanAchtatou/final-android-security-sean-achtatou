package com.immomo.momo.android.activity.group;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.protocol.a.n;
import java.util.List;

final class ez extends d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ SearchGroupActivity f1647a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ez(SearchGroupActivity searchGroupActivity, Context context) {
        super(context);
        this.f1647a = searchGroupActivity;
        if (searchGroupActivity.j != null) {
            searchGroupActivity.j.cancel(true);
        }
        searchGroupActivity.j = this;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        List c = n.a().c();
        SearchGroupActivity.a(this.f1647a, c);
        return c;
    }

    /* access modifiers changed from: protected */
    public final void a() {
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        this.f1647a.i.setData((List) obj);
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.f1647a.j = (ez) null;
    }
}
