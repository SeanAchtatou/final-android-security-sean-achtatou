package com.immomo.momo.android.view;

import com.amap.mapapi.poisearch.PoiTypeDef;

public class ao {

    /* renamed from: a  reason: collision with root package name */
    boolean f2723a = false;
    /* access modifiers changed from: private */
    public CharSequence b = PoiTypeDef.All;
    /* access modifiers changed from: private */
    public CharSequence c = PoiTypeDef.All;

    public final void a(CharSequence charSequence) {
        this.b = charSequence;
    }

    public final boolean a() {
        return this.f2723a;
    }

    public final void b(CharSequence charSequence) {
        this.c = charSequence;
        this.f2723a = true;
    }

    public String toString() {
        return "EmoteText [sourceText=" + ((Object) this.b) + ", emoteText=" + ((Object) this.c) + ", inited=" + this.f2723a + "]";
    }
}
