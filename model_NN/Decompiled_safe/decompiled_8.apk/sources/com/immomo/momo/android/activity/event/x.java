package com.immomo.momo.android.activity.event;

import android.content.Context;
import android.content.Intent;
import com.immomo.momo.android.broadcast.f;
import com.immomo.momo.android.c.b;
import com.immomo.momo.protocol.a.j;
import com.immomo.momo.service.bean.aa;

final class x extends b {
    private aa c;
    private /* synthetic */ EventFeedProfileActivity d;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public x(EventFeedProfileActivity eventFeedProfileActivity, Context context) {
        super(context);
        this.d = eventFeedProfileActivity;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        this.c = (aa) objArr[0];
        String c2 = j.a().c(this.c.h);
        this.d.n.a(this.c, this.d.k);
        Intent intent = new Intent(f.f2349a);
        intent.putExtra("feedid", this.c.h);
        intent.putExtra("siteid", this.c.e);
        intent.putExtra("userid", this.d.f.h);
        this.d.sendBroadcast(intent);
        return c2;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        a((String) obj);
        this.c.i = 2;
        this.d.finish();
    }
}
