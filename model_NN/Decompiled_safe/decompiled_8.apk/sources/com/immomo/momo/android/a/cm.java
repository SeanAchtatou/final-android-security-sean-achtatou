package com.immomo.momo.android.a;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.ImageBrowserActivity;
import com.immomo.momo.android.c.r;
import com.immomo.momo.android.c.u;
import com.immomo.momo.g;
import com.immomo.momo.plugin.c.a;
import com.immomo.momo.util.e;
import com.immomo.momo.util.m;
import java.util.ArrayList;

public final class cm extends a implements View.OnClickListener {
    private m d = new m("WeiboAdapter");
    /* access modifiers changed from: private */
    public AbsListView e = null;
    private Activity f = null;

    public cm(Activity activity, AbsListView absListView) {
        super(activity, new ArrayList());
        this.f = activity;
        this.e = absListView;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = g.o().inflate((int) R.layout.listitem_weibo, (ViewGroup) null);
            cp cpVar = new cp((byte) 0);
            view.setTag(R.id.tag_userlist_item, cpVar);
            cpVar.f760a = (TextView) view.findViewById(R.id.weiboitem_tv_textcontent);
            cpVar.c = (ImageView) view.findViewById(R.id.weiboitem_iv_imagecontent);
            cpVar.c.setOnClickListener(this);
            cpVar.b = (TextView) view.findViewById(R.id.weiboitem_iv_posttime);
            cpVar.d = view.findViewById(R.id.weiboitem_layout_repost);
            cp cpVar2 = new cp((byte) 0);
            cpVar2.f760a = (TextView) cpVar.d.findViewById(R.id.weiboitem_tv_textcontent);
            cpVar2.c = (ImageView) cpVar.d.findViewById(R.id.weiboitem_repost_iv_imagecontent);
            cpVar2.c.setOnClickListener(this);
            cpVar2.b = (TextView) cpVar.d.findViewById(R.id.weiboitem_repost_iv_posttime);
        }
        a aVar = (a) getItem(i);
        cp cpVar3 = (cp) view.getTag(R.id.tag_userlist_item);
        cpVar3.c.setTag(R.id.tag_item_position, Integer.valueOf(i));
        e.a(cpVar3.f760a, aVar.b() == null ? PoiTypeDef.All : aVar.b(), d());
        if (android.support.v4.b.a.a((CharSequence) aVar.c())) {
            cpVar3.c.setVisibility(8);
            view.setTag(PoiTypeDef.All);
        } else {
            cpVar3.c.setVisibility(0);
            ImageView imageView = cpVar3.c;
            String c = aVar.c();
            String str = PoiTypeDef.All;
            if (c != null && c.indexOf("/") > 0) {
                str = c.substring(c.lastIndexOf("/") + 1);
                view.setTag(str);
            }
            if (aVar.f2866a == null) {
                aVar.f2866a = g.t();
                if (!android.support.v4.b.a.a((CharSequence) str) && !android.support.v4.b.a.a((CharSequence) c)) {
                    r rVar = new r(str, new cn(this, aVar, str), 5, null);
                    rVar.a(c);
                    u.b().execute(rVar);
                }
            }
            imageView.setImageBitmap(aVar.f2866a);
        }
        if (aVar.a() != null) {
            cpVar3.b.setText(android.support.v4.b.a.e(aVar.a()));
        } else {
            cpVar3.b.setText("未知");
        }
        return view;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public final void onClick(View view) {
        switch (view.getId()) {
            case R.id.weiboitem_iv_imagecontent /*2131166859*/:
                try {
                    Integer num = (Integer) view.getTag(R.id.tag_item_position);
                    if (num.intValue() >= 0 && num.intValue() < getCount() && !android.support.v4.b.a.a((CharSequence) ((a) getItem(num.intValue())).c())) {
                        Intent intent = new Intent(this.f, ImageBrowserActivity.class);
                        intent.putExtra("thumb_url_array", new String[]{((a) getItem(num.intValue())).c()});
                        intent.putExtra("large_url_array", new String[]{((a) getItem(num.intValue())).c().replace("_s", "_b")});
                        intent.putExtra("model", "URL");
                        intent.putExtra("imagetype", "weibo");
                        intent.putExtra("autohide_header", true);
                        this.f.startActivity(intent);
                        this.f.overridePendingTransition(R.anim.zoom_enter, R.anim.normal);
                        return;
                    }
                    return;
                } catch (Exception e2) {
                    this.d.a((Throwable) e2);
                    return;
                }
            default:
                return;
        }
    }
}
