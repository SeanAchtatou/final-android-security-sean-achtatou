package com.immomo.momo.android.activity;

import android.os.AsyncTask;
import com.immomo.momo.R;
import com.immomo.momo.a.a;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.w;
import com.immomo.momo.service.bean.bf;

final class bc extends AsyncTask {

    /* renamed from: a  reason: collision with root package name */
    private bf f1040a;
    private v b;
    private /* synthetic */ BlacklistActivity c;

    public bc(BlacklistActivity blacklistActivity, bf bfVar) {
        this.c = blacklistActivity;
        this.f1040a = bfVar;
        this.b = new v(blacklistActivity);
        this.b.setCancelable(true);
    }

    private String a() {
        try {
            w.a().d(this.f1040a.h);
            return "yes";
        } catch (a e) {
            this.c.b((CharSequence) e.getMessage());
            return "no";
        } catch (Exception e2) {
            this.c.b((int) R.string.errormsg_server);
            return "no";
        }
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object doInBackground(Object... objArr) {
        return a();
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void onPostExecute(Object obj) {
        if (((String) obj).equals("yes")) {
            this.c.a((CharSequence) "移除成功");
            this.c.k.p(this.f1040a.h);
            this.c.l.a(this.f1040a);
        }
        this.b.cancel();
    }

    /* access modifiers changed from: protected */
    public final void onPreExecute() {
        if (this.b != null) {
            this.b.setOnCancelListener(new bd(this));
            this.b.a("请求提交中");
            this.b.show();
        }
    }
}
