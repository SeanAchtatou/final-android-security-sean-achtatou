package com.immomo.momo.android.activity.plugin;

import android.content.Context;
import com.immomo.momo.R;
import com.immomo.momo.a.a;
import com.immomo.momo.a.o;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.n;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.service.aq;
import com.immomo.momo.util.ao;

final class ae extends d {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public String f2033a;
    private String c;
    private v d;
    /* access modifiers changed from: private */
    public /* synthetic */ BindTXActivity e;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ae(BindTXActivity bindTXActivity, Context context, String str, String str2) {
        super(context);
        this.e = bindTXActivity;
        this.f2033a = str;
        this.c = str2;
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ Object a(Object... objArr) {
        return com.immomo.momo.protocol.a.d.a(this.f2033a, this.c);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
        this.d = new v(this.e);
        this.d.a("请求提交中");
        this.d.setCancelable(true);
        this.d.setOnCancelListener(new af(this));
        this.e.a(this.d);
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        this.e.p();
        if (exc instanceof o) {
            n.a(this.e, (int) R.string.bingsina_dialog_msg, new ag(this), new ah()).show();
            return;
        }
        if (exc instanceof a) {
            ao.a((CharSequence) exc.getMessage());
        } else {
            ao.g(R.string.errormsg_server);
        }
        this.e.finish();
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        String[] strArr = (String[]) obj;
        super.a((Object) strArr);
        this.e.p();
        this.e.f.at = true;
        this.e.f.as = strArr[0];
        if (!com.immomo.a.a.f.a.a(strArr[1])) {
            this.e.f.au = true;
        } else {
            this.e.f.au = false;
        }
        new aq().b(this.e.f);
        if (this.e.p) {
            ao.a((CharSequence) "绑定成功");
        }
        this.e.setResult(-1);
        this.e.finish();
    }
}
