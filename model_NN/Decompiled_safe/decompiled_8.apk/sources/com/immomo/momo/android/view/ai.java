package com.immomo.momo.android.view;

import android.content.DialogInterface;
import android.content.Intent;
import com.immomo.momo.android.activity.emotestore.EmotionProfileActivity;

final class ai implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ah f2718a;
    private final /* synthetic */ String b;

    ai(ah ahVar, String str) {
        this.f2718a = ahVar;
        this.b = str;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        Intent intent = new Intent(this.f2718a.f2717a.getContext(), EmotionProfileActivity.class);
        intent.putExtra("eid", this.b);
        this.f2718a.f2717a.getContext().startActivity(intent);
    }
}
