package com.immomo.momo.android.activity.maintab;

import android.content.Context;
import com.immomo.momo.android.c.d;
import java.util.List;

final class bf extends d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ aq f1855a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public bf(aq aqVar, Context context) {
        super(context);
        this.f1855a = aqVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.android.activity.maintab.aq.b(com.immomo.momo.android.activity.maintab.aq, boolean):java.util.List
     arg types: [com.immomo.momo.android.activity.maintab.aq, int]
     candidates:
      com.immomo.momo.android.activity.lh.b(int, android.view.KeyEvent):boolean
      android.support.v4.app.Fragment.b(android.view.LayoutInflater, android.view.ViewGroup):android.view.View
      com.immomo.momo.android.activity.maintab.aq.b(com.immomo.momo.android.activity.maintab.aq, boolean):java.util.List */
    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        return aq.b(this.f1855a, false);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        List<com.immomo.momo.service.bean.bf> list = (List) obj;
        if (list == null) {
            return;
        }
        if (list.size() > 0) {
            for (com.immomo.momo.service.bean.bf bfVar : list) {
                if (this.f1855a.P.get(bfVar.h) == null) {
                    this.f1855a.O.add(bfVar);
                    this.f1855a.P.put(bfVar.h, bfVar);
                }
                this.f1855a.Q.notifyDataSetChanged();
            }
            return;
        }
        this.f1855a.T.k();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.f1855a.X = (d) null;
        this.f1855a.U.e();
    }
}
