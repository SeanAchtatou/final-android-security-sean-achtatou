package com.immomo.momo.android.pay;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.os.Handler;
import android.os.Message;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public final class al {

    /* renamed from: a  reason: collision with root package name */
    Context f2531a = null;
    private Handler b = new am(this);

    public al(Context context) {
        this.f2531a = context;
    }

    private static boolean a(Context context, String str, String str2) {
        try {
            InputStream open = context.getAssets().open(str);
            File file = new File(str2);
            file.createNewFile();
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            byte[] bArr = new byte[1024];
            while (true) {
                int read = open.read(bArr);
                if (read <= 0) {
                    fileOutputStream.close();
                    open.close();
                    return true;
                }
                fileOutputStream.write(bArr, 0, read);
            }
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public final boolean a() {
        boolean z = false;
        List<PackageInfo> installedPackages = this.f2531a.getPackageManager().getInstalledPackages(0);
        int i = 0;
        while (true) {
            if (i < installedPackages.size()) {
                PackageInfo packageInfo = installedPackages.get(i);
                if (packageInfo.packageName.equalsIgnoreCase("com.eg.android.AlipayGphone") && packageInfo.versionCode > 37) {
                    z = true;
                    break;
                } else if (packageInfo.packageName.equalsIgnoreCase("com.alipay.android.app")) {
                    z = true;
                    break;
                } else {
                    i++;
                }
            } else {
                break;
            }
        }
        if (!z) {
            String str = String.valueOf(this.f2531a.getCacheDir().getAbsolutePath()) + "/temp.apk";
            a(this.f2531a, "alipay_msp_3.5.4_201306191652.mp3", str);
            Message message = new Message();
            message.what = 3;
            message.obj = str;
            this.b.sendMessage(message);
        }
        return z;
    }
}
