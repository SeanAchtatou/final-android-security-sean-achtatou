package com.immomo.momo.android.plugin.cropimage;

final class p implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ n f2595a;
    private final /* synthetic */ long b;
    private final /* synthetic */ float c;
    private final /* synthetic */ float d;
    private final /* synthetic */ float e;
    private final /* synthetic */ float f;

    p(n nVar, long j, float f2, float f3, float f4, float f5) {
        this.f2595a = nVar;
        this.b = j;
        this.c = f2;
        this.d = f3;
        this.e = f4;
        this.f = f5;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    public final void run() {
        float min = Math.min(300.0f, (float) (System.currentTimeMillis() - this.b));
        this.f2595a.a(this.c + (this.d * min), this.e, this.f);
        if (min < 300.0f) {
            this.f2595a.d.post(this);
        }
    }
}
