package com.immomo.momo.android.activity.account;

import android.content.DialogInterface;

final class bv implements DialogInterface.OnDismissListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ bn f975a;

    bv(bn bnVar) {
        this.f975a = bnVar;
    }

    public final void onDismiss(DialogInterface dialogInterface) {
        if (this.f975a.i != null) {
            this.f975a.i.cancel(true);
            this.f975a.i = (cb) null;
        }
    }
}
