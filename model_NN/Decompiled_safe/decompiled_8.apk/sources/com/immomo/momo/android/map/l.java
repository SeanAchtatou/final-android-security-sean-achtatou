package com.immomo.momo.android.map;

import android.content.Intent;
import android.view.View;

final class l implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ CreateSiteActivity f2506a;

    l(CreateSiteActivity createSiteActivity) {
        this.f2506a = createSiteActivity;
    }

    public final void onClick(View view) {
        if (CreateSiteActivity.c(this.f2506a)) {
            Intent intent = new Intent();
            intent.putExtra("sitename", this.f2506a.h.getText().toString().trim());
            intent.putExtra("sitetype", this.f2506a.k);
            this.f2506a.setResult(-1, intent);
            this.f2506a.finish();
        }
    }
}
