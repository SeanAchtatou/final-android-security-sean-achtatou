package com.immomo.momo.android.activity;

import android.content.Context;
import android.support.v4.b.a;
import com.immomo.momo.android.c.d;
import com.immomo.momo.g;
import com.immomo.momo.protocol.a.w;

final class ke extends d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ SetHiddenActivity f1784a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ke(SetHiddenActivity setHiddenActivity, Context context) {
        super(context);
        this.f1784a = setHiddenActivity;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        return w.a().e();
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        this.b.a((Throwable) exc);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        String str = (String) obj;
        if (a.f(str)) {
            this.f1784a.j.setText(str);
            g.d().f668a.a("hidden_setting_config", str);
        }
    }
}
