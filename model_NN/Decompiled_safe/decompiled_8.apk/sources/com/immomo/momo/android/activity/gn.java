package com.immomo.momo.android.activity;

import android.content.DialogInterface;

final class gn implements DialogInterface.OnCancelListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ OnlineSettingActivity f1521a;

    gn(OnlineSettingActivity onlineSettingActivity) {
        this.f1521a = onlineSettingActivity;
    }

    public final void onCancel(DialogInterface dialogInterface) {
        this.f1521a.n = true;
        OnlineSettingActivity.b(this.f1521a);
    }
}
