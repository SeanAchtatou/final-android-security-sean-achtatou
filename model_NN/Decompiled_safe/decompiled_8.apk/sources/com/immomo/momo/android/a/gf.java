package com.immomo.momo.android.a;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.service.bean.aj;
import com.immomo.momo.service.bean.an;
import com.immomo.momo.util.j;
import java.util.ArrayList;
import java.util.List;

public final class gf extends BaseAdapter {

    /* renamed from: a  reason: collision with root package name */
    private List f838a = new ArrayList();
    private Context b;

    public gf(Context context) {
        this.b = context;
    }

    /* renamed from: a */
    public final an getItem(int i) {
        if (this.f838a == null) {
            return null;
        }
        return (an) this.f838a.get(i);
    }

    public final void a(List list) {
        this.f838a.clear();
        this.f838a.addAll(list);
        notifyDataSetChanged();
    }

    public final int getCount() {
        if (this.f838a == null) {
            return 0;
        }
        return this.f838a.size();
    }

    public final long getItemId(int i) {
        return (long) i;
    }

    public final int getItemViewType(int i) {
        return getItem(i).d;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup, int, boolean, boolean, int):void
     arg types: [com.immomo.momo.service.bean.al, android.widget.ImageView, ?[OBJECT, ARRAY], int, int, int, int]
     candidates:
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, int, android.view.View, android.view.ViewGroup, boolean, int, boolean):com.immomo.momo.android.c.o
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup, int, boolean, boolean, int):void */
    public final View getView(int i, View view, ViewGroup viewGroup) {
        gg ggVar;
        if (getItemViewType(i) == 1) {
            an a2 = getItem(i);
            View inflate = LayoutInflater.from(this.b).inflate((int) R.layout.listitem_settingheader, (ViewGroup) null);
            ((TextView) inflate.findViewById(R.id.membercener_item_tv_header)).setText(a2.f2992a);
            return inflate;
        } else if (getItemViewType(i) != 2) {
            return null;
        } else {
            an a3 = getItem(i);
            if (view == null || view.getTag() == null || !(view.getTag() instanceof gg)) {
                ggVar = new gg((byte) 0);
                view = LayoutInflater.from(this.b).inflate((int) R.layout.listitem_settinginfo, (ViewGroup) null);
                ggVar.f839a = (TextView) view.findViewById(R.id.membercenter_item_tv_info);
                ggVar.b = (TextView) view.findViewById(R.id.membercenter_item_tv_new);
                ggVar.c = (ImageView) view.findViewById(R.id.membercenter_item_img_icon);
                ggVar.d = view.findViewById(R.id.membercenter_item_view_line);
                view.setTag(ggVar);
            } else {
                ggVar = (gg) view.getTag();
            }
            ggVar.f839a.setText(a3.f2992a);
            if (a3.e) {
                ggVar.b.setVisibility(0);
            } else {
                ggVar.b.setVisibility(8);
            }
            if (i == getCount() - 1 || a3.d != getItem(i + 1).d) {
                ggVar.d.setVisibility(8);
            } else {
                ggVar.d.setVisibility(0);
            }
            j.a((aj) a3.a(), ggVar.c, (ViewGroup) null, 18, true, false, 0);
            return view;
        }
    }

    public final int getViewTypeCount() {
        return 4;
    }
}
