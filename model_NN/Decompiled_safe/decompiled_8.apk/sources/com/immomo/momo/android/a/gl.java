package com.immomo.momo.android.a;

import android.content.Context;
import android.content.Intent;
import com.immomo.momo.android.activity.event.EventFeedProfileActivity;
import com.immomo.momo.android.activity.event.EventProfileActivity;
import com.immomo.momo.android.activity.feed.FeedProfileActivity;
import com.immomo.momo.android.view.a.al;
import com.immomo.momo.service.bean.ae;

final class gl implements al {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ gk f841a;
    private final /* synthetic */ ae b;

    gl(gk gkVar, ae aeVar) {
        this.f841a = gkVar;
        this.b = aeVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.android.activity.feed.FeedProfileActivity.a(android.content.Context, java.lang.String, boolean):void
     arg types: [android.support.v4.app.g, java.lang.String, int]
     candidates:
      com.immomo.momo.android.activity.feed.FeedProfileActivity.a(com.immomo.momo.android.activity.feed.FeedProfileActivity, java.lang.String, boolean):void
      com.immomo.momo.android.activity.ao.a(android.support.v4.app.Fragment, android.content.Intent, int):void
      android.support.v4.app.g.a(java.lang.String, java.io.PrintWriter, android.view.View):void
      android.support.v4.app.g.a(android.support.v4.app.Fragment, android.content.Intent, int):void
      com.immomo.momo.android.activity.feed.FeedProfileActivity.a(android.content.Context, java.lang.String, boolean):void */
    public final void a(int i) {
        switch (i) {
            case 0:
                if (this.b.o == 2) {
                    EventFeedProfileActivity.a(this.f841a.d.c(), this.b);
                    return;
                } else {
                    FeedProfileActivity.a(this.f841a.d.c(), this.b);
                    return;
                }
            case 1:
                if (this.b.o == 2) {
                    Intent intent = new Intent(this.f841a.d.c(), EventProfileActivity.class);
                    intent.putExtra("eventid", this.b.i);
                    this.f841a.d.a(intent);
                    return;
                }
                FeedProfileActivity.a((Context) this.f841a.d.c(), this.b.h, false);
                return;
            default:
                return;
        }
    }
}
