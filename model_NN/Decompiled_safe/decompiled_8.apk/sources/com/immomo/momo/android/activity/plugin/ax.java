package com.immomo.momo.android.activity.plugin;

import android.content.Context;
import com.immomo.momo.R;
import com.immomo.momo.a.a;
import com.immomo.momo.android.c.d;
import com.immomo.momo.plugin.e.e;
import com.immomo.momo.service.aq;

final class ax extends d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ CommunityStatusActivity f2051a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ax(CommunityStatusActivity communityStatusActivity, Context context) {
        super(context);
        this.f2051a = communityStatusActivity;
        if (communityStatusActivity.t != null) {
            communityStatusActivity.t.cancel(true);
        }
        communityStatusActivity.t = this;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        return com.immomo.momo.protocol.a.d.b(this.f2051a.f.am, this.f2051a.f.h);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        if (exc instanceof a) {
            super.a(exc);
            return;
        }
        this.b.a((Throwable) exc);
        a((int) R.string.plus_error_profile);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        e eVar = (e) obj;
        super.a(eVar);
        if (eVar != null) {
            this.f2051a.r = eVar;
            if (!android.support.v4.b.a.a((CharSequence) eVar.b)) {
                this.f2051a.k.setText(eVar.b);
                this.f2051a.f.p = eVar.b;
                new aq().b(this.f2051a.f);
            }
        }
    }
}
