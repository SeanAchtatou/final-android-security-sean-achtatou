package com.immomo.momo.android.c;

import com.immomo.momo.util.m;
import java.io.File;
import java.net.HttpURLConnection;

public class w {

    /* renamed from: a  reason: collision with root package name */
    public long f2395a = 0;
    public long b = 0;
    public File c = null;
    public boolean d = false;
    public boolean e = false;
    private HttpURLConnection f = null;

    public w() {
        new m(this);
    }

    public final void a() {
        HttpURLConnection httpURLConnection = this.f;
        if (httpURLConnection != null) {
            httpURLConnection.disconnect();
            this.f = null;
        }
        this.e = true;
        this.d = true;
    }

    public void a(long j, long j2, int i, HttpURLConnection httpURLConnection) {
        this.b = j;
        this.f2395a = j2;
        this.f = httpURLConnection;
        if (i == 4) {
            this.d = true;
        } else if (i == 2) {
            this.d = true;
        } else if (i == 5) {
            this.d = true;
            this.e = true;
        } else {
            this.d = false;
        }
    }
}
