package com.immomo.momo.android.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.GridView;

public class AvaterGridView extends GridView {
    public AvaterGridView(Context context) {
        super(context);
    }

    public AvaterGridView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public AvaterGridView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public void onMeasure(int i, int i2) {
        super.onMeasure(i, View.MeasureSpec.makeMeasureSpec(536870911, Integer.MIN_VALUE));
    }
}
