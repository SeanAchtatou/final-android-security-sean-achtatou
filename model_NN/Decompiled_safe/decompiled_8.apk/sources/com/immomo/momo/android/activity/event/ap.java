package com.immomo.momo.android.activity.event;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.j;

final class ap extends d {

    /* renamed from: a  reason: collision with root package name */
    private v f1359a = null;
    /* access modifiers changed from: private */
    public /* synthetic */ EventProfileActivity c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ap(EventProfileActivity eventProfileActivity, Context context) {
        super(context);
        this.c = eventProfileActivity;
        this.f1359a = new v(context);
        this.f1359a.a("正在获取资料，请稍候...");
        this.f1359a.setCancelable(true);
        this.f1359a.setOnCancelListener(new aq(this));
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        j.a().a(this.c.m, this.c.n);
        this.c.o.a(this.c.n);
        return 0;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.b.a((Object) "downloadEventProfile~~~~~~~~~~~~~~");
        if (this.c.k && this.f1359a != null) {
            this.c.a(this.f1359a);
        }
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        super.a(exc);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        this.c.k = false;
        this.c.x();
        EventProfileActivity.g(this.c);
    }

    /* access modifiers changed from: protected */
    public final void b() {
        if (this.c.k && this.f1359a != null) {
            this.c.p();
        }
    }
}
