package com.immomo.momo.android.broadcast;

import android.content.Context;
import android.content.IntentFilter;

public final class g extends b {

    /* renamed from: a  reason: collision with root package name */
    public static final String f2350a = (String.valueOf(com.immomo.momo.g.h()) + ".action.edown.success");
    public static final String b = (String.valueOf(com.immomo.momo.g.h()) + ".action.edown.begin");
    public static final String c = (String.valueOf(com.immomo.momo.g.h()) + ".action.edown.error");

    public g(Context context, String... strArr) {
        super(context);
        IntentFilter intentFilter = new IntentFilter();
        for (String addAction : strArr) {
            intentFilter.addAction(addAction);
        }
        a(intentFilter);
    }
}
