package com.immomo.momo.android.activity.discuss;

import com.immomo.momo.R;
import com.immomo.momo.android.view.a.al;
import com.immomo.momo.android.view.a.n;
import com.immomo.momo.service.bean.o;

final class p implements al {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ DiscussProfileActivity f1242a;

    p(DiscussProfileActivity discussProfileActivity) {
        this.f1242a = discussProfileActivity;
    }

    public final void a(int i) {
        switch (i) {
            case 0:
                if (this.f1242a.x != null) {
                    o m = this.f1242a.x;
                    o m2 = this.f1242a.x;
                    boolean z = !this.f1242a.x.f3033a;
                    m2.f3033a = z;
                    m.a("discuss_ispush", Boolean.valueOf(z));
                    return;
                }
                return;
            case 1:
                DiscussProfileActivity.n(this.f1242a);
                return;
            case 2:
                n.a(this.f1242a, this.f1242a.getString(R.string.dprofile_setting_dismiss_tip), new q(this.f1242a)).show();
                return;
            default:
                return;
        }
    }
}
