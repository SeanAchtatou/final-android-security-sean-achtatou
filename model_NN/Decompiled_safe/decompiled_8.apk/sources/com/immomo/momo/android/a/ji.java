package com.immomo.momo.android.a;

import android.support.v4.b.a;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.bb;
import com.immomo.momo.service.bean.c.d;
import com.immomo.momo.util.j;
import java.util.HashMap;
import java.util.List;

public final class ji extends b {

    /* renamed from: a  reason: collision with root package name */
    private List f898a = null;
    private ExpandableListView b = null;

    public ji(List list, ExpandableListView expandableListView) {
        new HashMap();
        this.f898a = list;
        this.b = expandableListView;
    }

    private static String b(int i) {
        return i > 9999999 ? String.valueOf(i / 10000000) + "千万" : i > 9999 ? String.valueOf(i / 10000) + "万" : new StringBuilder(String.valueOf(i)).toString();
    }

    /* renamed from: a */
    public final bb getGroup(int i) {
        return (bb) this.f898a.get(i);
    }

    /* renamed from: a */
    public final d getChild(int i, int i2) {
        if (i2 >= getGroup(i).b()) {
            return null;
        }
        return (d) ((bb) this.f898a.get(i)).c().get(i2);
    }

    public final void a() {
        if (this.f898a != null) {
            this.f898a.clear();
        }
    }

    public final void a(View view, int i) {
        if (i >= 0) {
            bb a2 = getGroup(i);
            if (a2.a() == 0) {
                ((TextView) view.findViewById(R.id.sitelist_tv_name)).setText("已创建(" + a2.b() + ")");
            } else if (a2.a() == 3) {
                ((TextView) view.findViewById(R.id.sitelist_tv_name)).setText("待创建(" + a2.b() + ")");
            } else {
                ((TextView) view.findViewById(R.id.sitelist_tv_name)).setText(PoiTypeDef.All);
            }
        }
    }

    public final void a(List list) {
        if (this.f898a != null) {
            this.f898a.addAll(list);
        }
    }

    public final int b(int i, int i2) {
        if (i2 < 0 && i < 0) {
            return 0;
        }
        if (i2 != -1 || this.b.isGroupExpanded(i)) {
            return i2 == getChildrenCount(i) + -1 ? 2 : 1;
        }
        return 0;
    }

    public final void b() {
        for (int i = 0; i < getGroupCount(); i++) {
            this.b.expandGroup(i);
        }
    }

    public final long getChildId(int i, int i2) {
        return (long) i2;
    }

    public final int getChildType(int i, int i2) {
        return ((bb) this.f898a.get(i)).a() == 0 ? 0 : 1;
    }

    public final int getChildTypeCount() {
        return 2;
    }

    public final View getChildView(int i, int i2, boolean z, View view, ViewGroup viewGroup) {
        d a2 = getChild(i, i2);
        int childType = getChildType(i, i2);
        if (childType == 0) {
            if (view == null) {
                jk jkVar = new jk((byte) 0);
                view = g.o().inflate((int) R.layout.listitem_tieba, (ViewGroup) null);
                jkVar.f900a = (ImageView) view.findViewById(R.id.teibalist_item_iv_face);
                jkVar.f = (ImageView) view.findViewById(R.id.teibalist_item_iv_hot);
                jkVar.e = (ImageView) view.findViewById(R.id.tiebalist_item_iv_recommend);
                jkVar.b = (TextView) view.findViewById(R.id.tiebalist_item_tv_name);
                jkVar.d = (TextView) view.findViewById(R.id.tiebalist_item_tv_sign);
                jkVar.c = (TextView) view.findViewById(R.id.tiebalist_item_tv_attribute);
                view.setTag(R.id.tag_userlist_item, jkVar);
            }
            jk jkVar2 = (jk) view.getTag(R.id.tag_userlist_item);
            if (a.a((CharSequence) a2.b)) {
                jkVar2.b.setText(a2.f3019a);
            } else {
                jkVar2.b.setText(a2.b);
            }
            jkVar2.f.setVisibility(8);
            jkVar2.e.setVisibility(8);
            if (a2.i > 0) {
                jkVar2.c.setText("成员 " + b(a2.g) + " | 今日话题 " + b(a2.i));
            } else {
                jkVar2.c.setText("成员 " + b(a2.g));
            }
            if (a2.j != null && a2.j.length() > 70) {
                a2.j = a2.j.substring(0, 70);
            }
            jkVar2.d.setText(a2.j);
            j.a(a2, jkVar2.f900a, (ViewGroup) null, 3);
        } else if (childType == 1) {
            if (view == null) {
                jl jlVar = new jl((byte) 0);
                view = g.o().inflate((int) R.layout.listitem_tiebacreating, (ViewGroup) null);
                jlVar.f901a = (TextView) view.findViewById(R.id.tiebacreating_item_tv_name);
                jlVar.b = (TextView) view.findViewById(R.id.tiebacreating_item_tv_des);
                view.setTag(R.id.tag_userlist_item, jlVar);
            }
            jl jlVar2 = (jl) view.getTag(R.id.tag_userlist_item);
            if (a.a((CharSequence) a2.b)) {
                jlVar2.f901a.setText(a2.f3019a);
            } else {
                jlVar2.f901a.setText(a2.b);
            }
            jlVar2.b.setText(String.valueOf(b(a2.g)) + "人支持");
        }
        return view;
    }

    public final int getChildrenCount(int i) {
        bb a2 = getGroup(i);
        if (i < 0 || i >= getGroupCount() || a2.c() == null) {
            return 0;
        }
        return ((bb) this.f898a.get(i)).c().size();
    }

    public final int getGroupCount() {
        return this.f898a.size();
    }

    public final long getGroupId(int i) {
        return (long) i;
    }

    public final View getGroupView(int i, boolean z, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = g.o().inflate((int) R.layout.listitem_groupsite, (ViewGroup) null);
            jj jjVar = new jj();
            jjVar.f899a = (TextView) view.findViewById(R.id.sitelist_tv_name);
            view.findViewById(R.id.tv_groupcount);
            view.findViewById(R.id.layout_root);
            view.setTag(R.id.tag_userlist_item, jjVar);
        }
        bb a2 = getGroup(i);
        jj jjVar2 = (jj) view.getTag(R.id.tag_userlist_item);
        if (a2.a() == 0) {
            jjVar2.f899a.setText("已创建(" + a2.b() + ")");
        } else if (a2.a() == 3) {
            jjVar2.f899a.setText("待创建(" + a2.b() + ")");
        } else {
            jjVar2.f899a.setText(PoiTypeDef.All);
        }
        return view;
    }

    public final boolean hasStableIds() {
        return true;
    }

    public final boolean isChildSelectable(int i, int i2) {
        return true;
    }
}
