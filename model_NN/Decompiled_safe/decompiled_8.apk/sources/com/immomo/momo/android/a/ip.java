package com.immomo.momo.android.a;

import android.support.v4.b.a;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.bb;
import com.immomo.momo.service.bean.c.d;
import java.util.HashMap;
import java.util.List;

public final class ip extends b {

    /* renamed from: a  reason: collision with root package name */
    private List f882a = null;
    private ExpandableListView b = null;

    public ip(List list, ExpandableListView expandableListView) {
        new HashMap();
        this.f882a = list;
        this.b = expandableListView;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public bb getGroup(int i) {
        return (bb) this.f882a.get(i);
    }

    /* renamed from: a */
    public final d getChild(int i, int i2) {
        if (i2 >= getGroup(i).b()) {
            return null;
        }
        return (d) ((bb) this.f882a.get(i)).c().get(i2);
    }

    public final void a() {
        for (int i = 0; i < getGroupCount(); i++) {
            this.b.expandGroup(i);
        }
    }

    public final void a(View view, int i) {
        if (i >= 0) {
            bb a2 = getGroup(i);
            if (a2.a() == 3) {
                ((TextView) view.findViewById(R.id.sitelist_tv_name)).setText("我支持的(" + a2.b() + ")");
            } else if (a2.a() == 2) {
                ((TextView) view.findViewById(R.id.sitelist_tv_name)).setText("其他热门待创建");
            } else {
                ((TextView) view.findViewById(R.id.sitelist_tv_name)).setText(PoiTypeDef.All);
            }
        }
    }

    public final int b(int i, int i2) {
        if (i2 < 0 && i < 0) {
            return 0;
        }
        if (i2 != -1 || this.b.isGroupExpanded(i)) {
            return i2 == getChildrenCount(i) + -1 ? 2 : 1;
        }
        return 0;
    }

    public final long getChildId(int i, int i2) {
        return (long) i2;
    }

    public final int getChildTypeCount() {
        return 2;
    }

    public final View getChildView(int i, int i2, boolean z, View view, ViewGroup viewGroup) {
        d a2 = getChild(i, i2);
        if (view == null) {
            ir irVar = new ir((byte) 0);
            view = g.o().inflate((int) R.layout.listitem_tiebacreating, (ViewGroup) null);
            irVar.f884a = (TextView) view.findViewById(R.id.tiebacreating_item_tv_name);
            irVar.b = (TextView) view.findViewById(R.id.tiebacreating_item_tv_des);
            view.setTag(R.id.tag_userlist_item, irVar);
        }
        ir irVar2 = (ir) view.getTag(R.id.tag_userlist_item);
        if (a.a((CharSequence) a2.b)) {
            irVar2.f884a.setText(a2.f3019a);
        } else {
            irVar2.f884a.setText(a2.b);
        }
        TextView textView = irVar2.b;
        int i3 = a2.g;
        textView.setText(String.valueOf(i3 > 9999999 ? String.valueOf(i3 / 10000000) + "千万" : i3 > 9999 ? String.valueOf(i3 / 10000) + "万" : new StringBuilder(String.valueOf(i3)).toString()) + "人支持");
        return view;
    }

    public final int getChildrenCount(int i) {
        bb a2 = getGroup(i);
        if (i < 0 || i >= getGroupCount() || a2.c() == null) {
            return 0;
        }
        return ((bb) this.f882a.get(i)).c().size();
    }

    public final int getGroupCount() {
        return this.f882a.size();
    }

    public final long getGroupId(int i) {
        return (long) i;
    }

    public final View getGroupView(int i, boolean z, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = g.o().inflate((int) R.layout.listitem_groupsite, (ViewGroup) null);
            iq iqVar = new iq();
            iqVar.f883a = (TextView) view.findViewById(R.id.sitelist_tv_name);
            view.findViewById(R.id.tv_groupcount);
            view.findViewById(R.id.layout_root);
            view.setTag(R.id.tag_userlist_item, iqVar);
        }
        bb a2 = getGroup(i);
        iq iqVar2 = (iq) view.getTag(R.id.tag_userlist_item);
        if (a2.a() == 3) {
            iqVar2.f883a.setText("我支持的(" + a2.b() + ")");
        } else if (a2.a() == 2) {
            iqVar2.f883a.setText("其他热门待创建");
        } else {
            iqVar2.f883a.setText(PoiTypeDef.All);
        }
        return view;
    }

    public final boolean hasStableIds() {
        return true;
    }

    public final boolean isChildSelectable(int i, int i2) {
        return true;
    }
}
