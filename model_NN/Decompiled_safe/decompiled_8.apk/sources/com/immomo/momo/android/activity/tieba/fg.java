package com.immomo.momo.android.activity.tieba;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.protocol.a.v;
import java.util.List;

final class fg extends d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ TiebaSearchActivity f2284a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public fg(TiebaSearchActivity tiebaSearchActivity, Context context) {
        super(context);
        this.f2284a = tiebaSearchActivity;
        if (tiebaSearchActivity.k != null) {
            tiebaSearchActivity.k.cancel(true);
        }
        tiebaSearchActivity.k = this;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        List d = v.a().d();
        TiebaSearchActivity.a(this.f2284a, d);
        return d;
    }

    /* access modifiers changed from: protected */
    public final void a() {
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        this.f2284a.i.setData((List) obj);
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.f2284a.k = (fg) null;
    }
}
