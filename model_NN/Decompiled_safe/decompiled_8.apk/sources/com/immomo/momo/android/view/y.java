package com.immomo.momo.android.view;

final class y implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ChatListView f2833a;

    private y(ChatListView chatListView) {
        this.f2833a = chatListView;
    }

    /* synthetic */ y(ChatListView chatListView, byte b) {
        this(chatListView);
    }

    public final void run() {
        if (this.f2833a.getLastVisiblePosition() < this.f2833a.getCount() - 5) {
            this.f2833a.setSelection(this.f2833a.getCount() - 5);
        }
        this.f2833a.smoothScrollToPosition(this.f2833a.getCount() - 1);
    }
}
