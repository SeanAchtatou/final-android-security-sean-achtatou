package com.immomo.momo.android.activity.plugin;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;

final class a extends d {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public /* synthetic */ BindDoubanActivity f2028a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public a(BindDoubanActivity bindDoubanActivity, Context context) {
        super(context);
        this.f2028a = bindDoubanActivity;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        String[] b = com.immomo.momo.protocol.a.d.b("douban");
        if (b != null && b.length == 2) {
            this.f2028a.m = b[0];
            this.f2028a.n = b[1];
            if (this.f2028a.u()) {
                this.f2028a.s = "http://www.douban.com/service/auth/authorize?oauth_token=" + this.f2028a.o + "&oauth_callback=" + this.f2028a.i;
                this.f2028a.j.post(new b(this));
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.f2028a.l = new v(this.f2028a, "正在请求数据，请稍候...");
        this.f2028a.l.setOnCancelListener(new c(this));
        this.f2028a.l.show();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        if (this.f2028a.l != null && this.f2028a.l.isShowing()) {
            this.f2028a.l.dismiss();
        }
    }
}
