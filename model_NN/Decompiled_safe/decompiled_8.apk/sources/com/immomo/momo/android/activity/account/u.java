package com.immomo.momo.android.activity.account;

import android.content.Context;
import android.content.Intent;
import android.support.v4.b.a;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.a.p;
import com.immomo.momo.protocol.a.w;

final class u extends d {

    /* renamed from: a  reason: collision with root package name */
    private v f1015a = null;
    private /* synthetic */ RegisterActivityWithP c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public u(RegisterActivityWithP registerActivityWithP, Context context) {
        super(context);
        this.c = registerActivityWithP;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        return w.a().f();
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.f1015a = new v(this.c, "正在读取注册配置，请稍候");
        this.f1015a.setOnCancelListener(new v(this));
        this.c.a(this.f1015a);
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        this.c.N.setVisibility(0);
        this.c.G.g();
        super.a(exc);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        p pVar = (p) obj;
        this.c.o = pVar.b;
        this.c.A = pVar.f2895a;
        this.c.B = System.currentTimeMillis();
        if (this.c.o.length == 0) {
            this.c.G.g();
        }
        if (this.c.o.length == 1 && this.c.o[0] == 1) {
            this.c.G.g();
        }
        if (this.c.o[0] == 2) {
            Intent intent = new Intent(this.c, RegisterActivity.class);
            intent.putExtra("registInterfacetype", this.c.o);
            intent.putExtra("timestamp", this.c.A);
            if (a.f(this.c.w)) {
                intent.putExtra("alipay_user_id", this.c.w);
            }
            this.c.startActivity(intent);
            this.c.overridePendingTransition(0, 0);
            this.c.finish();
            return;
        }
        this.c.N.setVisibility(0);
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.c.p();
    }
}
