package com.immomo.momo.android.a;

import android.view.View;

final class db implements View.OnLongClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ da f768a;
    private final /* synthetic */ int b;

    db(da daVar, int i) {
        this.f768a = daVar;
        this.b = i;
    }

    public final boolean onLongClick(View view) {
        return this.f768a.e.getOnItemLongClickListenerInWrapper().onItemLongClick(this.f768a.e, view, this.b, (long) view.getId());
    }
}
