package com.immomo.momo.android.activity;

import android.os.Bundle;
import android.support.v4.b.a;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.android.view.HeaderLayout;
import com.immomo.momo.g;

public class SetHiddenActivity extends ah {
    private HeaderLayout h = null;
    /* access modifiers changed from: private */
    public EditText i;
    /* access modifiers changed from: private */
    public TextView j;
    private Button k;

    static /* synthetic */ boolean c(SetHiddenActivity setHiddenActivity) {
        String trim = setHiddenActivity.i.getText().toString().trim();
        if (!a.f(trim)) {
            setHiddenActivity.a((CharSequence) "请输入陌陌号");
            setHiddenActivity.i.requestFocus();
            return false;
        } else if (trim.length() < 4) {
            setHiddenActivity.a((CharSequence) "无效陌陌号");
            setHiddenActivity.i.requestFocus();
            return false;
        } else if (!setHiddenActivity.f.h.equals(trim)) {
            return true;
        } else {
            setHiddenActivity.a((CharSequence) "不能对自己隐身");
            setHiddenActivity.i.requestFocus();
            return false;
        }
    }

    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        super.a(bundle);
        setContentView((int) R.layout.activity_sethideuser);
        d();
        this.h = (HeaderLayout) findViewById(R.id.layout_header);
        this.h.setTitleText("定向隐身");
        this.i = (EditText) findViewById(R.id.sethideuser_et_momoid);
        this.j = (TextView) findViewById(R.id.hiddensetting_tv_desc);
        this.k = (Button) findViewById(R.id.send_button);
        this.k.setOnClickListener(new kd(this));
        this.j.setText(g.d().f668a.b("hidden_setting_config", getString(R.string.hidden_setting_config)));
        a(new ke(this, this)).execute(new Object[0]);
    }
}
