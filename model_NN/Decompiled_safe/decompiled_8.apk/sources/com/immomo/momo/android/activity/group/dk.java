package com.immomo.momo.android.activity.group;

import android.content.DialogInterface;
import android.support.v4.b.a;
import com.immomo.momo.android.view.EmoteEditeText;

final class dk implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ GroupProfileActivity f1607a;
    private final /* synthetic */ EmoteEditeText b;

    dk(GroupProfileActivity groupProfileActivity, EmoteEditeText emoteEditeText) {
        this.f1607a = groupProfileActivity;
        this.b = emoteEditeText;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        GroupProfileActivity.a(this.f1607a, this.b);
        String trim = this.b.getText().toString().trim();
        if (a.a((CharSequence) trim)) {
            this.f1607a.a((CharSequence) "请输入密码");
        } else {
            this.f1607a.b(new Cdo(this.f1607a, this.f1607a, a.n(trim)));
        }
    }
}
