package com.immomo.momo.android.view.a;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.RectF;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.android.view.AudioView;
import com.immomo.momo.g;
import com.immomo.momo.util.m;

public final class a extends aw {

    /* renamed from: a  reason: collision with root package name */
    private m f2663a = new m("AudioRecordingBox").a();
    /* access modifiers changed from: private */
    public View b = null;
    private TextView e = null;
    /* access modifiers changed from: private */
    public TextView f = null;
    private AudioView g = null;
    /* access modifiers changed from: private */
    public RectF h = null;
    /* access modifiers changed from: private */
    public int i = 0;
    private int j = 0;
    /* access modifiers changed from: private */
    public d k = null;
    private Handler l = new b(this, g.c().getMainLooper());

    public a(Context context) {
        super(context, R.layout.include_message_diloag_audiorecord);
        this.c.setAnimationStyle(R.style.Popup_Animation_ScaleScale);
        int a2 = g.a(10.0f);
        int color = g.l().getColor(R.color.red);
        Bitmap createBitmap = Bitmap.createBitmap(a2, a2, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(createBitmap);
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setColor(color);
        canvas.drawCircle(((float) a2) / 2.0f, ((float) a2) / 2.0f, ((float) a2) / 2.0f, paint);
        ((ImageView) c((int) R.id.audiorecod_iv_round)).setImageBitmap(createBitmap);
        this.b = c((int) R.id.audiorecod_layout_container);
        this.e = (TextView) c((int) R.id.audiorecod_tv_note);
        this.f = (TextView) c((int) R.id.audiorecod_tv_time);
        this.g = (AudioView) c((int) R.id.audiorecod_audioview);
    }

    public final void a(int i2) {
        int i3 = 7;
        int i4 = (i2 * 7) / 32768;
        int round = this.j < i4 ? i4 + Math.round(((float) (i4 - this.j)) * 2.0f) : i4 + (Math.round(((float) this.j) - (((float) i4) * 1.4f)) - 1);
        if (round < 0) {
            i3 = 0;
        } else if (round <= 7) {
            i3 = round;
        }
        this.j = i3;
        this.g.a(i3);
    }

    public final void a(View view) {
        super.a(view);
        this.i = 0;
        this.f.setText(String.valueOf(this.i) + " ''");
        this.l.sendEmptyMessageDelayed(146, 1000);
        this.b.post(new c(this));
    }

    public final void a(d dVar) {
        this.k = dVar;
    }

    public final boolean a(PointF pointF) {
        this.f2663a.a((Object) ("touch point : [" + pointF.x + ", " + pointF.y + "]"));
        this.f2663a.a((Object) ("viewRectF :" + this.h));
        new RectF();
        boolean intersects = this.h != null ? RectF.intersects(new RectF(pointF.x - 2.0f, pointF.y - 2.0f, pointF.x + 2.0f, pointF.y + 2.0f), this.h) : false;
        if (intersects) {
            this.e.setText((int) R.string.audiorecod_note_cancel);
            this.g.a();
        } else {
            this.e.setText((int) R.string.audiorecod_note_scroll);
            this.g.b();
        }
        return intersects;
    }

    public final void onDismiss() {
        super.onDismiss();
        this.l.removeMessages(146);
    }

    public final boolean onKey(View view, int i2, KeyEvent keyEvent) {
        if (i2 == 4 && keyEvent.getAction() == 1) {
            return true;
        }
        return super.onKey(view, i2, keyEvent);
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        return false;
    }
}
