package com.immomo.momo.android.activity.event;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.protocol.a.j;
import com.immomo.momo.service.r;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

final class ac extends d {

    /* renamed from: a  reason: collision with root package name */
    private List f1348a = new ArrayList();
    private /* synthetic */ EventFeedsActivity c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ac(EventFeedsActivity eventFeedsActivity, Context context) {
        super(context);
        this.c = eventFeedsActivity;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        boolean a2 = j.a().a(this.f1348a, this.c.k.getCount(), this.c.h);
        r unused = this.c.q;
        String unused2 = this.c.h;
        List list = this.f1348a;
        return Boolean.valueOf(a2);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        Boolean bool = (Boolean) obj;
        super.a(bool);
        if (this.f1348a.isEmpty() || !bool.booleanValue()) {
            this.c.i.k();
        }
        this.c.k.b((Collection) this.f1348a);
        this.c.k.notifyDataSetChanged();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        super.b();
        this.c.l.e();
    }
}
