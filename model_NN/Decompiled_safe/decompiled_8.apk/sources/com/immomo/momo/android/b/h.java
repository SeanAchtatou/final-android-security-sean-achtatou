package com.immomo.momo.android.b;

import android.content.Context;
import android.support.v4.b.a;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.immomo.momo.util.m;
import java.util.HashMap;
import java.util.Map;

public final class h extends k {
    private static h c;
    /* access modifiers changed from: private */
    public m b = new m("BaiduLocater");
    private HashMap d = new HashMap();
    private HashMap e = new HashMap();

    private h(Context context) {
        this.f2329a = context;
    }

    public static h a(Context context) {
        if (c == null) {
            c = new h(context);
        }
        return c;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        LocationClient locationClient;
        for (Map.Entry entry : this.d.entrySet()) {
            if (!(entry == null || (locationClient = (LocationClient) entry.getValue()) == null)) {
                this.b.a((Object) ("l is " + locationClient.toString()));
                try {
                    if (this.e.get(entry.getKey()) != null) {
                        locationClient.unRegisterLocationListener((BDLocationListener) this.e.get(entry.getKey()));
                    }
                    locationClient.stop();
                } catch (Throwable th) {
                }
            }
        }
        this.d.clear();
        this.e.clear();
    }

    /* access modifiers changed from: protected */
    public final void a(String str) {
        if (!a.a((CharSequence) str)) {
            this.b.b((Object) ("Baidu locater cancel " + str));
            LocationClient locationClient = (LocationClient) this.d.get(str);
            if (locationClient != null) {
                try {
                    if (this.e.get(str) != null) {
                        locationClient.unRegisterLocationListener((BDLocationListener) this.e.get(str));
                        this.e.remove(str);
                    }
                    locationClient.stop();
                    this.d.remove(str);
                } catch (Throwable th) {
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void a(String str, p pVar) {
        this.b.a((Object) ("getLocationByGPS called in" + Class.class.getSimpleName()));
        LocationClientOption locationClientOption = new LocationClientOption();
        locationClientOption.setOpenGps(true);
        locationClientOption.setCoorType("gcj02");
        locationClientOption.setPriority(1);
        locationClientOption.setScanSpan(5000);
        locationClientOption.disableCache(true);
        LocationClient locationClient = new LocationClient(this.f2329a);
        locationClient.setLocOption(locationClientOption);
        this.d.put(str, locationClient);
        j jVar = new j(this, pVar);
        locationClient.registerLocationListener(jVar);
        this.e.put(str, jVar);
        locationClient.start();
    }

    /* access modifiers changed from: protected */
    public final void b(String str, p pVar) {
        this.b.a((Object) ("getLocationByNetwork called in" + Class.class.getSimpleName()));
        LocationClientOption locationClientOption = new LocationClientOption();
        locationClientOption.setOpenGps(true);
        locationClientOption.setCoorType("gcj02");
        locationClientOption.setPriority(2);
        locationClientOption.setScanSpan(5000);
        locationClientOption.disableCache(true);
        LocationClient locationClient = new LocationClient(this.f2329a);
        locationClient.setLocOption(locationClientOption);
        this.d.put(str, locationClient);
        i iVar = new i(this, pVar);
        locationClient.registerLocationListener(iVar);
        this.e.put(str, iVar);
        locationClient.start();
    }
}
