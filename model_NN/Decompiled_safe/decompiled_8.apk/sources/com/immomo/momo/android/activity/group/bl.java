package com.immomo.momo.android.activity.group;

import android.content.Context;
import android.os.Bundle;
import com.immomo.momo.android.c.b;
import com.immomo.momo.g;
import com.immomo.momo.protocol.a.o;
import com.immomo.momo.service.bean.a.e;

final class bl extends b {
    private /* synthetic */ GroupFeedProfileActivity c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public bl(GroupFeedProfileActivity groupFeedProfileActivity, Context context) {
        super(context);
        this.c = groupFeedProfileActivity;
        if (groupFeedProfileActivity.ae != null) {
            groupFeedProfileActivity.ae.cancel(true);
        }
        groupFeedProfileActivity.ae = this;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        boolean z = false;
        String b = o.a().b(this.c.j, !this.c.k.n);
        e e = this.c.k;
        if (!this.c.k.n) {
            z = true;
        }
        e.n = z;
        this.c.m.a(this.c.k);
        return b;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        this.c.w();
        this.c.x();
        a((String) obj);
        g.d().a(new Bundle(), "actions.groupfeedchanged");
    }

    /* access modifiers changed from: protected */
    public final void b() {
        super.b();
        this.c.ae = (bl) null;
    }
}
