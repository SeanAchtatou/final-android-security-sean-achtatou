package com.immomo.momo.android.a;

import android.content.Context;
import android.support.v4.b.a;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.util.j;
import java.util.List;

public final class g extends BaseAdapter {

    /* renamed from: a  reason: collision with root package name */
    private Context f833a = null;
    private List b = null;
    private AbsListView c = null;

    public g(Context context, List list, AbsListView absListView) {
        this.f833a = context;
        this.b = list;
        this.c = absListView;
    }

    /* renamed from: a */
    public final bf getItem(int i) {
        if (i < 0 || i >= this.b.size()) {
            return new bf();
        }
        bf bfVar = (bf) this.b.get(i);
        return bfVar == null ? new bf() : bfVar;
    }

    public final void a() {
        this.b.clear();
        notifyDataSetChanged();
    }

    public final void a(bf bfVar) {
        this.b.remove(bfVar);
        notifyDataSetChanged();
    }

    public final int getCount() {
        return this.b.size();
    }

    public final long getItemId(int i) {
        return (long) i;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            h hVar = new h((byte) 0);
            view = LayoutInflater.from(this.f833a).inflate((int) R.layout.listitem_black, (ViewGroup) null);
            hVar.f853a = (ImageView) view.findViewById(R.id.blacklist_item_iv_face);
            hVar.b = (TextView) view.findViewById(R.id.blacklist_item_tv_name);
            hVar.c = (TextView) view.findViewById(R.id.blocklist_item_tv_blocktime);
            view.setTag(R.id.tag_userlist_item, hVar);
        }
        bf bfVar = (bf) this.b.get(i);
        h hVar2 = (h) view.getTag(R.id.tag_userlist_item);
        hVar2.b.setText(bfVar.h());
        if (bfVar.Y != null) {
            hVar2.c.setText(a.e(bfVar.Y));
        } else {
            hVar2.c.setText(PoiTypeDef.All);
        }
        j.a(bfVar, hVar2.f853a, this.c, 3);
        return view;
    }
}
