package com.immomo.momo.android.service;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import com.immomo.momo.a;
import com.immomo.momo.android.broadcast.b;
import com.immomo.momo.util.ak;

public final class r extends b {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ XService f2617a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public r(XService xService, Context context) {
        super(context);
        this.f2617a = xService;
        a(XService.c, XService.d, XService.e, XService.f);
    }

    public final void a(Intent intent) {
        if (intent.getAction().equals(XService.c)) {
            this.f2617a.b.k();
        } else if (intent.getAction().equals(XService.d)) {
            SharedPreferences.Editor edit = ak.a(this.f2617a.getApplicationContext(), String.valueOf(XService.h(this.f2617a).a().h) + "_appconfig").a().edit();
            edit.remove("appconfigs_0");
            edit.remove("appconfigs_1");
            edit.commit();
            com.immomo.a.a.b b = this.f2617a.b.b();
            b.a(a.f669a);
            b.a(a.b);
            this.f2617a.h.f();
            this.f2617a.b.k();
            this.f2617a.c();
        } else if (intent.getAction().equals(XService.e)) {
            new Thread(new t(this.f2617a)).start();
        } else if (intent.getAction().equals(XService.f)) {
            com.immomo.a.a.d.b bVar = new com.immomo.a.a.d.b();
            bVar.a("pi");
            try {
                this.f2617a.b.a(bVar);
            } catch (Exception e) {
                this.f2617a.m.a((Throwable) e);
            }
        }
    }
}
