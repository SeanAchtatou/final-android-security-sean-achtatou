package com.immomo.momo.android.game;

import android.content.Context;
import com.immomo.momo.R;
import com.immomo.momo.a.a;
import com.immomo.momo.android.c.d;
import com.immomo.momo.protocol.a.m;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

final class bf extends d {

    /* renamed from: a  reason: collision with root package name */
    private boolean f2426a = false;
    private /* synthetic */ NearbyPlayersActivity c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public bf(NearbyPlayersActivity nearbyPlayersActivity, Context context) {
        super(context);
        this.c = nearbyPlayersActivity;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        ArrayList arrayList = new ArrayList();
        if (this.c.n != null) {
            this.f2426a = m.a().a(arrayList, this.c.j.getCount(), this.c.n, this.c.o);
            this.c.m.a(arrayList);
        } else {
            new a(this.c.getString(R.string.errormsg_location_failed));
        }
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public final void a() {
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        this.c.j.b((Collection) ((List) obj));
        if (!this.f2426a) {
            this.c.i.setVisibility(8);
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.c.i.e();
    }
}
