package com.immomo.momo.android.activity.event;

import com.immomo.momo.android.c.g;
import java.io.File;

final class cb implements g {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ca f1395a;

    cb(ca caVar) {
        this.f1395a = caVar;
    }

    public final /* synthetic */ void a(Object obj) {
        File file = (File) obj;
        if (file != null && file.exists()) {
            this.f1395a.f1394a.s.post(new cc(this, file));
        }
    }
}
