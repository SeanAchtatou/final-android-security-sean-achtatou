package com.immomo.momo.android.a;

import android.graphics.Bitmap;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import com.immomo.momo.R;
import com.immomo.momo.android.c.g;
import com.immomo.momo.plugin.c.a;

final class cn implements g {

    /* renamed from: a  reason: collision with root package name */
    private a f758a = null;
    /* access modifiers changed from: private */
    public Bitmap b = null;
    private Handler c = null;
    /* access modifiers changed from: private */
    public ImageView d = null;
    private String e = null;
    private /* synthetic */ cm f;

    public cn(cm cmVar, a aVar, String str) {
        this.f = cmVar;
        this.f758a = aVar;
        this.e = str;
        this.c = new co(this, cmVar.d().getMainLooper());
    }

    public final /* synthetic */ void a(Object obj) {
        View findViewWithTag;
        this.b = (Bitmap) obj;
        if (!(this.b == null || this.f758a == null)) {
            this.f758a.f2866a = this.b;
        }
        if (this.c != null) {
            try {
                if (!(this.f.e == null || (findViewWithTag = this.f.e.findViewWithTag(this.e)) == null)) {
                    this.d = (ImageView) findViewWithTag.findViewById(R.id.weiboitem_iv_imagecontent);
                }
            } catch (Exception e2) {
            }
            this.c.sendEmptyMessage(1);
        }
    }
}
