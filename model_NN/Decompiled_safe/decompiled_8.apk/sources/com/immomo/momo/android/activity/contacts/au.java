package com.immomo.momo.android.activity.contacts;

import android.content.Context;
import android.support.v4.b.a;
import com.immomo.momo.android.c.d;
import com.immomo.momo.g;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

final class au extends d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ an f1152a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public au(an anVar, Context context) {
        super(context);
        this.f1152a = anVar;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        this.f1152a.V = this.f1152a.U;
        this.f1152a.U = 0;
        ArrayList arrayList = new ArrayList();
        int a2 = an.a(this.f1152a, arrayList);
        this.f1152a.X.d(arrayList);
        g.q().v = 0;
        g.q().w = a2;
        this.f1152a.X.a(g.q().w, g.q().v, g.q().h);
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
        if (this.f1152a.Z != null && !this.f1152a.Z.isCancelled()) {
            this.f1152a.Z.cancel(true);
        }
        this.f1152a.Q.g();
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        List list = (List) obj;
        super.a(list);
        if (list == null || list.size() <= 0) {
            this.f1152a.U = this.f1152a.V;
            return;
        }
        this.f1152a.N.c("lasttime_fans_success", a.c(this.f1152a.W));
        this.f1152a.O.setLastFlushTime(this.f1152a.W);
        an anVar = this.f1152a;
        anVar.U = anVar.U + list.size();
        an.b(this.f1152a, list);
        if (list.size() >= g.q().w) {
            this.f1152a.O.k();
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
        super.b();
        this.f1152a.Y = null;
        this.f1152a.W = new Date();
        this.f1152a.N.c("lasttime_fans", a.c(this.f1152a.W));
        this.f1152a.O.n();
        this.f1152a.Q.e();
    }
}
