package com.immomo.momo.android.activity.contacts;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import com.immomo.momo.android.activity.OtherProfileActivity;
import com.immomo.momo.service.bean.bf;

final class bc implements AdapterView.OnItemClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ aw f1160a;

    bc(aw awVar) {
        this.f1160a = awVar;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        if (i < this.f1160a.T.size()) {
            Intent intent = new Intent(this.f1160a.c(), OtherProfileActivity.class);
            intent.putExtra("tag", "local");
            intent.putExtra("momoid", ((bf) this.f1160a.T.get(i)).h);
            this.f1160a.a(intent);
        }
    }
}
