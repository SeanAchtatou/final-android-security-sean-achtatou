package com.immomo.momo.android.a;

import android.view.View;

final class ij implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ii f878a;

    ij(ii iiVar) {
        this.f878a = iiVar;
    }

    public final void onClick(View view) {
        int intValue = ((Integer) view.getTag()).intValue();
        this.f878a.d.getOnItemClickListener().onItemClick(this.f878a.d, view, intValue, (long) view.getId());
    }
}
