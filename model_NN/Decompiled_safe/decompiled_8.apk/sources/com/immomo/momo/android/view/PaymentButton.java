package com.immomo.momo.android.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.immomo.momo.R;

public class PaymentButton extends LinearLayout {

    /* renamed from: a  reason: collision with root package name */
    private int f2643a = R.layout.common_payment_button;
    private TextView b;
    private TextView c;

    public PaymentButton(Context context) {
        super(context);
        a();
    }

    private PaymentButton(Context context, int i) {
        super(context);
        this.f2643a = i;
        a();
    }

    public PaymentButton(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, com.immomo.momo.android.view.PaymentButton, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    private void a() {
        ((LayoutInflater) getContext().getSystemService("layout_inflater")).inflate(this.f2643a, (ViewGroup) this, true);
        this.b = (TextView) findViewById(R.id.tv_left);
        findViewById(R.id.tv_right);
        this.c = (TextView) findViewById(R.id.tv_promotion);
    }

    public void setLeftText(int i) {
        if (i <= 0) {
            this.b.setVisibility(8);
            return;
        }
        this.b.setVisibility(0);
        this.b.setText(getContext().getString(i));
    }

    public void setLeftText(CharSequence charSequence) {
        if (charSequence == null) {
            this.b.setVisibility(8);
            return;
        }
        this.b.setVisibility(0);
        this.b.setText(charSequence);
    }

    public void setPromotionText(int i) {
        if (i <= 0) {
            this.c.setVisibility(8);
            return;
        }
        this.c.setVisibility(0);
        this.c.setText(getContext().getString(i));
    }

    public void setPromotionText(CharSequence charSequence) {
        if (charSequence == null) {
            this.c.setVisibility(8);
            return;
        }
        this.c.setVisibility(0);
        this.c.setText(charSequence);
    }
}
