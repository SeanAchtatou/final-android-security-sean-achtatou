package com.immomo.momo.android.activity.group;

import com.immomo.momo.android.view.au;
import java.io.File;

final class az implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ay f1550a;
    private final /* synthetic */ File b;
    private final /* synthetic */ boolean c;

    az(ay ayVar, File file, boolean z) {
        this.f1550a = ayVar;
        this.b = file;
        this.c = z;
    }

    public final void run() {
        if (this.b != null && this.b.exists()) {
            this.f1550a.f1549a.ad = new au(this.c ? 1 : 2);
            this.f1550a.f1549a.ad.a(this.b, this.f1550a.f1549a.C);
            this.f1550a.f1549a.ad.b(20);
            this.f1550a.f1549a.C.setGifDecoder(this.f1550a.f1549a.ad);
        }
    }
}
