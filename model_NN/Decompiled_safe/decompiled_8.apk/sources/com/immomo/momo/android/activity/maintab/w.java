package com.immomo.momo.android.activity.maintab;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.protocol.a.k;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

final class w extends d {

    /* renamed from: a  reason: collision with root package name */
    private List f1902a = new ArrayList();
    private /* synthetic */ l c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public w(l lVar, Context context) {
        super(context);
        this.c = lVar;
        if (lVar.U != null) {
            lVar.U.cancel(true);
        }
        lVar.U = this;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        boolean a2 = k.a().a(this.f1902a, 0, this.c.M.S, this.c.M.T, this.c.M.aH);
        this.c.X.e(this.f1902a);
        this.c.S.clear();
        this.c.S.addAll(this.f1902a);
        return Boolean.valueOf(a2);
    }

    /* access modifiers changed from: protected */
    public final void a() {
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        Boolean bool = (Boolean) obj;
        Date date = new Date();
        this.c.O.setLastFlushTime(date);
        this.c.N.a("nfeeds_lasttime_success", date);
        this.c.N.c("nffilter_remain", bool);
        this.c.R = this.f1902a;
        this.c.W.a((Collection) this.c.R);
        this.c.P.setVisibility(bool.booleanValue() ? 0 : 8);
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.c.O.n();
        this.c.Q = new Date();
        this.c.N.a("nfeeds_latttime_reflush", this.c.Q);
        this.c.U = (w) null;
    }
}
