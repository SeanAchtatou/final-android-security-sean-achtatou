package com.immomo.momo.android.activity.maintab;

import android.location.Location;
import com.immomo.momo.R;
import com.immomo.momo.android.b.p;
import com.immomo.momo.util.ao;

final class z extends p {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ x f1904a;

    z(x xVar) {
        this.f1904a = xVar;
    }

    public final void a(Location location, int i, int i2, int i3) {
        if (this.b) {
            if (com.immomo.momo.android.b.z.a(location)) {
                this.f1904a.M.S = location.getLatitude();
                this.f1904a.M.T = location.getLongitude();
                this.f1904a.M.U = (double) location.getAccuracy();
                this.f1904a.M.a(System.currentTimeMillis());
                this.f1904a.M.aH = i;
                this.f1904a.M.aI = i3;
                this.f1904a.V.a(this.f1904a.M);
                this.f1904a.ah.post(new aa(this, location));
                return;
            }
            this.f1904a.an();
            if (i2 == 104) {
                ao.g(R.string.errormsg_network_unfind);
            } else if (i2 == 105) {
                this.f1904a.ah.post(new ab(this.f1904a));
            } else {
                ao.g(R.string.errormsg_location_nearby_failed);
            }
        }
    }
}
