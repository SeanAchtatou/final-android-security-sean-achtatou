package com.immomo.momo.android.broadcast;

import android.content.Context;
import android.content.IntentFilter;
import com.immomo.momo.g;

public final class x extends b {

    /* renamed from: a  reason: collision with root package name */
    public static final String f2367a = (String.valueOf(g.h()) + ".action.refresh.tieba");
    public static final String b = (String.valueOf(g.h()) + ".action.update.tieba");

    public x(Context context) {
        super(context);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(f2367a);
        intentFilter.addAction(b);
        a(intentFilter);
    }
}
