package com.immomo.momo.android.activity.account;

import android.view.View;
import android.widget.TextView;

public abstract class ab {

    /* renamed from: a  reason: collision with root package name */
    private View f930a = null;

    public ab(View view) {
        this.f930a = view;
    }

    protected static boolean a(TextView textView) {
        String trim = textView.getText().toString().trim();
        if (trim != null && trim.length() > 0) {
            return false;
        }
        textView.requestFocus();
        return true;
    }

    /* access modifiers changed from: protected */
    public final View a(int i) {
        return this.f930a.findViewById(i);
    }

    public abstract boolean a();

    public void b() {
    }

    public void c() {
    }

    public void d() {
    }

    public void e() {
    }

    public void f() {
    }
}
