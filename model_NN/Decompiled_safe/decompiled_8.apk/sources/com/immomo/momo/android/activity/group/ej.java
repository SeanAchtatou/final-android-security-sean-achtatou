package com.immomo.momo.android.activity.group;

import android.support.v4.b.a;
import android.text.Editable;
import android.text.TextWatcher;
import com.amap.mapapi.poisearch.PoiTypeDef;

final class ej implements TextWatcher {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ PublishGroupFeedActivity f1631a;

    ej(PublishGroupFeedActivity publishGroupFeedActivity) {
        this.f1631a = publishGroupFeedActivity;
    }

    public final void afterTextChanged(Editable editable) {
        if (a.a((CharSequence) editable.toString())) {
            this.f1631a.l.setText(PoiTypeDef.All);
        } else {
            this.f1631a.l.setText(String.valueOf(editable.length()) + "/120字");
        }
    }

    public final void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public final void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }
}
