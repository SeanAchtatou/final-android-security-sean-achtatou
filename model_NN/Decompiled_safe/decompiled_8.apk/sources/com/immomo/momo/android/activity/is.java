package com.immomo.momo.android.activity;

import android.content.Intent;
import android.view.View;
import com.immomo.momo.R;

final class is implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ OtherProfileV2Activity f1747a;

    is(OtherProfileV2Activity otherProfileV2Activity) {
        this.f1747a = otherProfileV2Activity;
    }

    public final void onClick(View view) {
        int intValue = ((Integer) view.getTag(R.id.tag_item_position)).intValue();
        Intent intent = new Intent(this.f1747a, ImageBrowserActivity.class);
        intent.putExtra("array", this.f1747a.t.ae);
        intent.putExtra("imagetype", "avator");
        intent.putExtra("index", intValue);
        this.f1747a.startActivity(intent);
        this.f1747a.overridePendingTransition(R.anim.zoom_enter, R.anim.normal);
    }
}
