package com.immomo.momo.android.view.dragsort;

import android.os.SystemClock;

class t implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    protected long f2800a;
    private float b;
    private float c = 0.5f;
    private float d;
    private float e;
    private float f;
    private float g;
    private boolean h;
    private /* synthetic */ DragSortListView i;

    public t(DragSortListView dragSortListView, int i2) {
        this.i = dragSortListView;
        this.b = (float) i2;
        float f2 = 1.0f / ((this.c * 2.0f) * (1.0f - this.c));
        this.g = f2;
        this.d = f2;
        this.e = this.c / ((this.c - 1.0f) * 2.0f);
        this.f = 1.0f / (1.0f - this.c);
    }

    public void a() {
    }

    public void a(float f2) {
    }

    public void b() {
    }

    public final void c() {
        this.f2800a = SystemClock.uptimeMillis();
        this.h = false;
        a();
        this.i.post(this);
    }

    public final void d() {
        this.h = true;
    }

    public void run() {
        float f2;
        if (!this.h) {
            float uptimeMillis = ((float) (SystemClock.uptimeMillis() - this.f2800a)) / this.b;
            if (uptimeMillis >= 1.0f) {
                a(1.0f);
                b();
                return;
            }
            if (uptimeMillis < this.c) {
                f2 = uptimeMillis * this.d * uptimeMillis;
            } else if (uptimeMillis < 1.0f - this.c) {
                f2 = (uptimeMillis * this.f) + this.e;
            } else {
                f2 = 1.0f - ((uptimeMillis - 1.0f) * (this.g * (uptimeMillis - 1.0f)));
            }
            a(f2);
            this.i.post(this);
        }
    }
}
