package com.immomo.momo.android.activity.emotestore;

import android.content.Context;
import android.content.Intent;
import com.immomo.momo.android.broadcast.o;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.g;
import com.immomo.momo.protocol.a.i;

final class w extends d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ EmotionProfileActivity f1334a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public w(EmotionProfileActivity emotionProfileActivity, Context context) {
        super(context);
        this.f1334a = emotionProfileActivity;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        String d = i.a().d(this.f1334a.h.f3035a);
        if (this.f1334a.h.A != null) {
            this.f1334a.h.q = true;
            this.f1334a.h.A.f = true;
            this.f1334a.h.A.c = this.f1334a.h.A.b;
            this.f1334a.i.b(this.f1334a.h);
        }
        return d;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.f1334a.a(new v(f(), this));
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        String str = (String) obj;
        this.f1334a.u();
        if (str != null) {
            a(str);
        }
        if (this.f1334a.h.u && this.f1334a.h.t) {
            Intent intent = new Intent(o.f2358a);
            intent.putExtra("event", "task");
            g.c().sendBroadcast(intent);
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.f1334a.p();
    }
}
