package com.immomo.momo.android.activity.tieba;

import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.ImageFactoryActivity;
import com.immomo.momo.android.activity.MulImagePickerActivity;
import com.immomo.momo.android.activity.ah;
import com.immomo.momo.android.broadcast.w;
import com.immomo.momo.android.c.i;
import com.immomo.momo.android.view.EmoteInputView;
import com.immomo.momo.android.view.HeaderLayout;
import com.immomo.momo.android.view.MEmoteEditeText;
import com.immomo.momo.android.view.MGifImageView;
import com.immomo.momo.android.view.PublishButton;
import com.immomo.momo.android.view.PublishSelectPhotoView;
import com.immomo.momo.android.view.ResizeListenerLayout;
import com.immomo.momo.android.view.a.n;
import com.immomo.momo.android.view.au;
import com.immomo.momo.android.view.cs;
import com.immomo.momo.android.view.f;
import com.immomo.momo.g;
import com.immomo.momo.plugin.b.a;
import com.immomo.momo.protocol.a.v;
import com.immomo.momo.service.aq;
import com.immomo.momo.service.bean.av;
import com.immomo.momo.service.bean.c.b;
import com.immomo.momo.service.bean.q;
import com.immomo.momo.service.l;
import com.immomo.momo.service.m;
import com.immomo.momo.util.ao;
import com.immomo.momo.util.h;
import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import mm.purchasesdk.PurchaseCode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class EditTieActivity extends ah implements View.OnClickListener, View.OnTouchListener, cs, f, l {
    private static String i = ("http://m.immomo.com/inc/android/tieba/agreement.html?v=" + g.z());
    private MGifImageView A;
    /* access modifiers changed from: private */
    public PublishSelectPhotoView B;
    private View C;
    private View D;
    private View E;
    /* access modifiers changed from: private */
    public PublishButton F;
    /* access modifiers changed from: private */
    public PublishButton G;
    private TextView H;
    private List I = null;
    /* access modifiers changed from: private */
    public au J;
    /* access modifiers changed from: private */
    public HashMap K = new HashMap();
    private String L = PoiTypeDef.All;
    private File M = null;
    private File N = null;
    /* access modifiers changed from: private */
    public a O = null;
    private Bitmap P = null;
    private int Q = 0;
    private HashMap R = new HashMap();
    private HashMap S = new HashMap();
    private String T = PoiTypeDef.All;
    /* access modifiers changed from: private */
    public String U = null;
    private m V = new m();
    protected boolean h = false;
    private x j = null;
    private x k = null;
    private x l = null;
    private x m = null;
    private x n = null;
    /* access modifiers changed from: private */
    public MEmoteEditeText o = null;
    /* access modifiers changed from: private */
    public EditText p;
    /* access modifiers changed from: private */
    public TextView q = null;
    private ResizeListenerLayout r = null;
    private boolean s = false;
    /* access modifiers changed from: private */
    public int t = 0;
    /* access modifiers changed from: private */
    public String u;
    private String v;
    /* access modifiers changed from: private */
    public int w;
    /* access modifiers changed from: private */
    public EmoteInputView x = null;
    /* access modifiers changed from: private */
    public b y;
    /* access modifiers changed from: private */
    public Handler z = new Handler();

    private void A() {
        m().setTitleText("发布话题");
        String str = android.support.v4.b.a.a(this.v) ? PoiTypeDef.All : this.v;
        HeaderLayout m2 = m();
        if (android.support.v4.b.a.a((CharSequence) str)) {
            str = PoiTypeDef.All;
        }
        m2.setSubTitleText(str);
    }

    /* access modifiers changed from: private */
    public void B() {
        findViewById(R.id.btn_localphoto).setEnabled(false);
        findViewById(R.id.btn_camera).setEnabled(false);
        findViewById(R.id.btn_emote).setEnabled(true);
        findViewById(R.id.layout_tip).setVisibility(8);
        F();
        H();
        this.x.a();
        F();
        this.A.setAlt(this.O.g());
        if (this.J != null) {
            this.J.l();
            this.J = null;
        }
        String g = this.O.g();
        String h2 = this.O.h();
        MGifImageView mGifImageView = this.A;
        boolean endsWith = g.endsWith(".gif");
        if (this.J == null) {
            File a2 = q.a(g, h2);
            this.e.b((Object) ("emoteFile:" + (a2 == null ? "null" : a2.getAbsolutePath())));
            if (a2 == null || !a2.exists()) {
                new i(g, h2, new g(this, mGifImageView, endsWith)).a();
                return;
            }
            this.J = new au(endsWith ? 1 : 2);
            this.J.a(a2, mGifImageView);
            this.J.b(20);
            mGifImageView.setGifDecoder(this.J);
            return;
        }
        mGifImageView.setGifDecoder(this.J);
        if ((this.J.g() == 4 || this.J.g() == 2) && this.J.f() != null && this.J.f().exists()) {
            this.J.a(this.J.f(), mGifImageView);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{org.json.JSONObject.put(java.lang.String, double):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, long):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, int):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, java.lang.Object):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException} */
    private JSONArray C() {
        JSONArray jSONArray = new JSONArray();
        if (this.B.getDatalist() != null) {
            for (int i2 = 0; i2 < this.B.getDatalist().size(); i2++) {
                JSONObject jSONObject = new JSONObject();
                try {
                    av avVar = (av) this.B.getDatalist().get(i2);
                    if (avVar.d) {
                        jSONObject.put("isUploaded", true);
                        jSONObject.put("guid", avVar.e);
                    } else {
                        jSONObject.put("isUploaded", false);
                        jSONObject.put("path", avVar.f2998a);
                    }
                    jSONArray.put(jSONObject);
                } catch (Exception e) {
                    this.e.a((Throwable) e);
                }
            }
        }
        return jSONArray;
    }

    private boolean D() {
        if (this.B.getDatalist() == null) {
            return false;
        }
        for (int i2 = 0; i2 < this.B.getDatalist().size(); i2++) {
            if (((av) this.B.getDatalist().get(i2)).d) {
                return true;
            }
        }
        return false;
    }

    private void E() {
        this.C.setVisibility(8);
    }

    private void F() {
        this.C.setVisibility(0);
        this.E.setVisibility(8);
    }

    private void G() {
        this.D.setVisibility(8);
        this.F.setSelected(false);
        this.G.setSelected(false);
    }

    private void H() {
        this.E.setVisibility(8);
    }

    private void I() {
        this.t = 0;
        findViewById(R.id.btn_localphoto).setEnabled(true);
        findViewById(R.id.btn_camera).setEnabled(true);
        findViewById(R.id.btn_emote).setEnabled(true);
        findViewById(R.id.layout_tip).setVisibility(0);
    }

    private static List a(JSONArray jSONArray) {
        ArrayList arrayList = new ArrayList();
        if (jSONArray != null) {
            for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                try {
                    av avVar = new av();
                    JSONObject optJSONObject = jSONArray.optJSONObject(i2);
                    if (optJSONObject != null) {
                        avVar.d = optJSONObject.getBoolean("isUploaded");
                        avVar.e = optJSONObject.optString("guid", PoiTypeDef.All);
                        avVar.f2998a = optJSONObject.optString("path", PoiTypeDef.All);
                        if (avVar.d) {
                            avVar.f2998a = avVar.e;
                        }
                    }
                    arrayList.add(avVar);
                } catch (JSONException e) {
                }
            }
        }
        return arrayList;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.h.a(java.lang.String, java.lang.String, int, boolean):void
     arg types: [java.lang.String, java.lang.String, int, int]
     candidates:
      com.immomo.momo.util.h.a(java.lang.String, android.graphics.Bitmap, int, boolean):java.io.File
      com.immomo.momo.util.h.a(java.lang.String, java.lang.String, int, boolean):void */
    private void a(b bVar) {
        try {
            if (this.t == 2) {
                int b = bVar.b();
                for (int i2 = 0; i2 < b; i2++) {
                    String str = (String) this.S.get("photo_" + i2);
                    this.e.b((Object) ("asdf feed.getImages()[i]=" + bVar.c()[i2] + ", oldname=" + str));
                    if (!android.support.v4.b.a.a((CharSequence) str)) {
                        h.a(str, bVar.c()[i2], 16, false);
                    }
                }
            }
        } catch (Throwable th) {
            this.e.a(th);
        }
    }

    private void c(int i2) {
        u();
        this.x.setEmoteFlag(i2);
        if (this.h) {
            this.z.postDelayed(new a(this), 300);
        } else {
            this.x.b();
        }
    }

    /* access modifiers changed from: private */
    public void d(int i2) {
        if (i2 <= 0) {
            this.H.setVisibility(8);
            return;
        }
        this.H.setVisibility(0);
        this.H.setText(new StringBuilder().append(i2).toString());
    }

    static /* synthetic */ void h(EditTieActivity editTieActivity) {
        editTieActivity.R.clear();
        editTieActivity.S.clear();
        if (editTieActivity.B.getDatalist() != null && editTieActivity.B.getDatalist().size() > 0) {
            JSONArray jSONArray = new JSONArray();
            int i2 = 0;
            while (true) {
                int i3 = i2;
                if (i3 >= editTieActivity.B.getDatalist().size()) {
                    editTieActivity.T = jSONArray.toString();
                    return;
                }
                av avVar = (av) editTieActivity.B.getDatalist().get(i3);
                if (avVar != null) {
                    JSONObject jSONObject = new JSONObject();
                    try {
                        if (avVar.d) {
                            jSONObject.put("upload", "YES");
                            jSONObject.put("guid", avVar.e);
                        } else {
                            jSONObject.put("upload", "NO");
                            jSONObject.put("key", "photo_" + i3);
                            editTieActivity.R.put("photo_" + i3, avVar.b);
                        }
                        jSONArray.put(jSONObject);
                    } catch (JSONException e) {
                        editTieActivity.e.a((Throwable) e);
                        editTieActivity.a((CharSequence) "编辑失败");
                        return;
                    }
                }
                i2 = i3 + 1;
            }
        }
    }

    static /* synthetic */ void i(EditTieActivity editTieActivity) {
        editTieActivity.V.e = editTieActivity.p.getText().toString().trim();
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("selectmode", editTieActivity.t);
            jSONObject.put("postid", editTieActivity.u);
            jSONObject.put("tiebaname", editTieActivity.v);
            jSONObject.put("content", editTieActivity.o.getText().toString().trim());
            jSONObject.put("title", editTieActivity.p.getText().toString().trim());
            jSONObject.put("emotionbody", editTieActivity.O == null ? PoiTypeDef.All : editTieActivity.O.toString());
            jSONObject.put("pathArr", editTieActivity.C().toString());
            editTieActivity.V.f3050a = jSONObject.toString();
        } catch (JSONException e) {
            editTieActivity.e.a((Throwable) e);
        }
    }

    static /* synthetic */ void m(EditTieActivity editTieActivity) {
        if (!editTieActivity.g.z) {
            n a2 = n.a(editTieActivity, PoiTypeDef.All, "接受协议", new o(editTieActivity));
            a2.setTitle("陌陌吧用户协议");
            View inflate = editTieActivity.getLayoutInflater().inflate((int) R.layout.dialog_tieba_protocol, (ViewGroup) null);
            WebView webView = (WebView) inflate.findViewById(R.id.webview);
            View findViewById = inflate.findViewById(R.id.loading_indicator);
            WebSettings settings = webView.getSettings();
            settings.setCacheMode(2);
            settings.setJavaScriptEnabled(true);
            settings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
            webView.setWebChromeClient(new q());
            webView.setDownloadListener(new r(editTieActivity));
            webView.setWebViewClient(new s(editTieActivity, findViewById));
            webView.loadUrl(i);
            a2.setOnCancelListener(new p(editTieActivity));
            a2.setContentView(inflate);
            a2.show();
        }
    }

    static /* synthetic */ boolean o(EditTieActivity editTieActivity) {
        String trim = editTieActivity.p.getText().toString().trim();
        if (trim.length() > 20) {
            editTieActivity.a((CharSequence) "标题不能超过20个字");
            return false;
        } else if (trim.length() < 4) {
            editTieActivity.a((CharSequence) "标题不能少于4个字");
            return false;
        } else {
            String trim2 = editTieActivity.o.getText().toString().trim();
            if (trim2.length() > 1000) {
                editTieActivity.a((CharSequence) "内容不能超过1000个字");
                return false;
            } else if (trim2.length() >= 10) {
                return true;
            } else {
                editTieActivity.a((CharSequence) "内容不能少于10个字");
                return false;
            }
        }
    }

    private void y() {
        Display defaultDisplay = getWindowManager().getDefaultDisplay();
        defaultDisplay.getWidth();
        this.w = defaultDisplay.getHeight();
    }

    private void z() {
        String[] c = this.y.c();
        ArrayList arrayList = new ArrayList();
        if (c != null) {
            for (String str : c) {
                av avVar = new av();
                avVar.d = true;
                avVar.e = str;
                avVar.f2998a = avVar.e;
                arrayList.add(avVar);
            }
        }
        this.B.a();
        this.B.a(arrayList);
        this.B.setData(this.B.getDatalist());
    }

    static /* synthetic */ void z(EditTieActivity editTieActivity) {
        editTieActivity.D.setVisibility(0);
        if (editTieActivity.t == 0) {
            editTieActivity.C.setVisibility(8);
            editTieActivity.E.setVisibility(8);
        } else if (editTieActivity.t == 1) {
            editTieActivity.C.setVisibility(0);
            editTieActivity.E.setVisibility(8);
            editTieActivity.B();
        } else if (editTieActivity.t == 2) {
            editTieActivity.C.setVisibility(8);
            editTieActivity.E.setVisibility(0);
            if (editTieActivity.I != null && editTieActivity.B.getDatalist() == null) {
                editTieActivity.b(new v(editTieActivity, editTieActivity, editTieActivity.I, editTieActivity.D()));
            }
        }
    }

    public final void a(int i2, View view) {
        Uri fromFile;
        if (this.B.getDatalist() != null && i2 < this.B.getDatalist().size()) {
            if (((av) this.B.getDatalist().get(i2)).d) {
                a((CharSequence) "已经发布的图片不可编辑");
            } else if (((av) this.B.getDatalist().get(i2)).b != null && (fromFile = Uri.fromFile(((av) this.B.getDatalist().get(i2)).b)) != null) {
                this.Q = i2;
                Intent intent = new Intent(this, ImageFactoryActivity.class);
                intent.setData(fromFile);
                intent.putExtra("minsize", 320);
                intent.putExtra("process_model", "filter");
                intent.putExtra("maxwidth", 720);
                intent.putExtra("maxheight", 3000);
                this.N = new File(com.immomo.momo.a.g(), "temp_" + com.immomo.a.a.f.b.a() + ".jpg_");
                intent.putExtra("outputFilePath", this.N.getAbsolutePath());
                startActivityForResult(intent, 105);
            }
        }
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x02d1, code lost:
        if (r0 == null) goto L_0x02d3;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(android.os.Bundle r7) {
        /*
            r6 = this;
            r5 = 2
            r3 = 0
            r4 = 1
            super.a(r7)
            r0 = 2130903164(0x7f03007c, float:1.7413138E38)
            r6.setContentView(r0)
            r6.y()
            r0 = 2131165656(0x7f0701d8, float:1.7945535E38)
            android.view.View r0 = r6.findViewById(r0)
            com.immomo.momo.android.view.ResizeListenerLayout r0 = (com.immomo.momo.android.view.ResizeListenerLayout) r0
            r6.r = r0
            r0 = 2131165795(0x7f070263, float:1.7945817E38)
            android.view.View r0 = r6.findViewById(r0)
            com.immomo.momo.android.view.MEmoteEditeText r0 = (com.immomo.momo.android.view.MEmoteEditeText) r0
            r6.o = r0
            r0 = 2131165802(0x7f07026a, float:1.7945831E38)
            android.view.View r0 = r6.findViewById(r0)
            android.widget.TextView r0 = (android.widget.TextView) r0
            r6.q = r0
            r0 = 2131165818(0x7f07027a, float:1.7945864E38)
            android.view.View r0 = r6.findViewById(r0)
            android.widget.EditText r0 = (android.widget.EditText) r0
            r6.p = r0
            r0 = 2131165599(0x7f07019f, float:1.794542E38)
            android.view.View r0 = r6.findViewById(r0)
            com.immomo.momo.android.view.EmoteInputView r0 = (com.immomo.momo.android.view.EmoteInputView) r0
            r6.x = r0
            com.immomo.momo.android.view.EmoteInputView r0 = r6.x
            com.immomo.momo.android.view.MEmoteEditeText r1 = r6.o
            r0.setEditText(r1)
            r0 = 2131165811(0x7f070273, float:1.794585E38)
            android.view.View r0 = r6.findViewById(r0)
            com.immomo.momo.android.view.MGifImageView r0 = (com.immomo.momo.android.view.MGifImageView) r0
            r6.A = r0
            r0 = 2131165803(0x7f07026b, float:1.7945833E38)
            android.view.View r0 = r6.findViewById(r0)
            r6.D = r0
            r0 = 2131165810(0x7f070272, float:1.7945848E38)
            android.view.View r0 = r6.findViewById(r0)
            r6.C = r0
            r0 = 2131165808(0x7f070270, float:1.7945844E38)
            android.view.View r0 = r6.findViewById(r0)
            r6.E = r0
            r0 = 2131165809(0x7f070271, float:1.7945846E38)
            android.view.View r0 = r6.findViewById(r0)
            com.immomo.momo.android.view.PublishSelectPhotoView r0 = (com.immomo.momo.android.view.PublishSelectPhotoView) r0
            r6.B = r0
            r0 = 2131165816(0x7f070278, float:1.794586E38)
            android.view.View r0 = r6.findViewById(r0)
            com.immomo.momo.android.view.PublishButton r0 = (com.immomo.momo.android.view.PublishButton) r0
            r6.F = r0
            r0 = 2131165817(0x7f070279, float:1.7945862E38)
            android.view.View r0 = r6.findViewById(r0)
            com.immomo.momo.android.view.PublishButton r0 = (com.immomo.momo.android.view.PublishButton) r0
            r6.G = r0
            com.immomo.momo.android.view.PublishButton r0 = r6.F
            r1 = 2130838152(0x7f020288, float:1.7281278E38)
            r0.setIcon(r1)
            com.immomo.momo.android.view.PublishButton r0 = r6.G
            r1 = 2130838151(0x7f020287, float:1.7281276E38)
            r0.setIcon(r1)
            r0 = 2131166362(0x7f07049a, float:1.7946967E38)
            android.view.View r0 = r6.findViewById(r0)
            android.widget.TextView r0 = (android.widget.TextView) r0
            r6.H = r0
            com.immomo.momo.android.view.PublishButton r0 = r6.F
            r0.setSelected(r4)
            com.immomo.momo.android.view.PublishButton r0 = r6.G
            r0.setSelected(r3)
            com.immomo.momo.android.view.MEmoteEditeText r0 = r6.o
            r0.setOnTouchListener(r6)
            android.widget.EditText r0 = r6.p
            r0.setOnTouchListener(r6)
            com.immomo.momo.android.view.MEmoteEditeText r0 = r6.o
            com.immomo.momo.android.activity.tieba.c r1 = new com.immomo.momo.android.activity.tieba.c
            r1.<init>(r6)
            r0.addTextChangedListener(r1)
            com.immomo.momo.android.view.bi r0 = new com.immomo.momo.android.view.bi
            android.content.Context r1 = r6.getApplicationContext()
            r0.<init>(r1)
            r1 = 2130838261(0x7f0202f5, float:1.72815E38)
            com.immomo.momo.android.view.bi r0 = r0.a(r1)
            r1 = 8
            r0.setMarginRight(r1)
            r1 = 2130837644(0x7f02008c, float:1.7280248E38)
            r0.setBackgroundResource(r1)
            com.immomo.momo.android.view.HeaderLayout r1 = r6.m()
            com.immomo.momo.android.activity.tieba.d r2 = new com.immomo.momo.android.activity.tieba.d
            r2.<init>(r6)
            r1.a(r0, r2)
            com.immomo.momo.android.view.ResizeListenerLayout r0 = r6.r
            com.immomo.momo.android.activity.tieba.e r1 = new com.immomo.momo.android.activity.tieba.e
            r1.<init>(r6)
            r0.setOnResizeListener(r1)
            com.immomo.momo.android.view.EmoteInputView r0 = r6.x
            com.immomo.momo.android.activity.tieba.f r1 = new com.immomo.momo.android.activity.tieba.f
            r1.<init>(r6)
            r0.setOnEmoteSelectedListener(r1)
            r0 = 2131165812(0x7f070274, float:1.7945852E38)
            android.view.View r0 = r6.findViewById(r0)
            r0.setOnClickListener(r6)
            r0 = 2131165805(0x7f07026d, float:1.7945837E38)
            android.view.View r0 = r6.findViewById(r0)
            r0.setOnClickListener(r6)
            r0 = 2131165806(0x7f07026e, float:1.794584E38)
            android.view.View r0 = r6.findViewById(r0)
            r0.setOnClickListener(r6)
            r0 = 2131165807(0x7f07026f, float:1.7945842E38)
            android.view.View r0 = r6.findViewById(r0)
            r0.setOnClickListener(r6)
            com.immomo.momo.android.view.PublishSelectPhotoView r0 = r6.B
            r0.setOnMomoGridViewItemClickListener(r6)
            com.immomo.momo.android.view.PublishSelectPhotoView r0 = r6.B
            r0.setRefreshListener(r6)
            com.immomo.momo.android.view.PublishButton r0 = r6.F
            r0.setOnClickListener(r6)
            com.immomo.momo.android.view.PublishButton r0 = r6.G
            r0.setOnClickListener(r6)
            android.content.Intent r0 = r6.getIntent()
            android.os.Bundle r0 = r0.getExtras()
            java.lang.String r1 = "ddraft"
            boolean r0 = r0.containsKey(r1)
            if (r0 == 0) goto L_0x0242
            com.immomo.momo.service.m r0 = r6.V
            android.content.Intent r1 = r6.getIntent()
            java.lang.String r2 = "ddraftid"
            int r1 = r1.getIntExtra(r2, r3)
            r0.b = r1
            android.content.Intent r0 = r6.getIntent()
            java.lang.String r1 = "ddraft"
            java.lang.String r0 = r0.getStringExtra(r1)
            org.json.JSONObject r1 = new org.json.JSONObject     // Catch:{ JSONException -> 0x023b }
            r1.<init>(r0)     // Catch:{ JSONException -> 0x023b }
            android.widget.EditText r0 = r6.p     // Catch:{ JSONException -> 0x023b }
            java.lang.String r2 = "title"
            java.lang.String r3 = ""
            java.lang.String r2 = r1.optString(r2, r3)     // Catch:{ JSONException -> 0x023b }
            r0.setText(r2)     // Catch:{ JSONException -> 0x023b }
            com.immomo.momo.android.view.MEmoteEditeText r0 = r6.o     // Catch:{ JSONException -> 0x023b }
            java.lang.String r2 = "content"
            java.lang.String r3 = ""
            java.lang.String r2 = r1.optString(r2, r3)     // Catch:{ JSONException -> 0x023b }
            r0.setText(r2)     // Catch:{ JSONException -> 0x023b }
            java.lang.String r0 = "selectmode"
            r2 = 0
            int r0 = r1.optInt(r0, r2)     // Catch:{ JSONException -> 0x023b }
            r6.t = r0     // Catch:{ JSONException -> 0x023b }
            int r0 = r6.t     // Catch:{ JSONException -> 0x023b }
            if (r0 != r4) goto L_0x01b9
            java.lang.String r0 = "emotionbody"
            java.lang.String r2 = ""
            java.lang.String r0 = r1.optString(r0, r2)     // Catch:{ JSONException -> 0x023b }
            boolean r0 = android.support.v4.b.a.a(r0)     // Catch:{ JSONException -> 0x023b }
            if (r0 != 0) goto L_0x01b9
            com.immomo.momo.plugin.b.a r0 = new com.immomo.momo.plugin.b.a     // Catch:{ JSONException -> 0x023b }
            java.lang.String r2 = "emotionbody"
            java.lang.String r3 = ""
            java.lang.String r2 = r1.optString(r2, r3)     // Catch:{ JSONException -> 0x023b }
            r0.<init>(r2)     // Catch:{ JSONException -> 0x023b }
            r6.O = r0     // Catch:{ JSONException -> 0x023b }
            r0 = 1
            r6.d(r0)     // Catch:{ JSONException -> 0x023b }
        L_0x01b9:
            java.lang.String r0 = "postid"
            java.lang.String r2 = ""
            java.lang.String r0 = r1.optString(r0, r2)     // Catch:{ JSONException -> 0x023b }
            r6.u = r0     // Catch:{ JSONException -> 0x023b }
            java.lang.String r0 = "tiebaname"
            java.lang.String r2 = ""
            java.lang.String r0 = r1.optString(r0, r2)     // Catch:{ JSONException -> 0x023b }
            r6.v = r0     // Catch:{ JSONException -> 0x023b }
            java.lang.String r0 = "pathArr"
            java.lang.String r2 = ""
            java.lang.String r0 = r1.optString(r0, r2)     // Catch:{ JSONException -> 0x023b }
            boolean r0 = android.support.v4.b.a.a(r0)     // Catch:{ JSONException -> 0x023b }
            if (r0 != 0) goto L_0x01ee
            org.json.JSONArray r0 = new org.json.JSONArray     // Catch:{ JSONException -> 0x023b }
            java.lang.String r2 = "pathArr"
            java.lang.String r3 = ""
            java.lang.String r1 = r1.optString(r2, r3)     // Catch:{ JSONException -> 0x023b }
            r0.<init>(r1)     // Catch:{ JSONException -> 0x023b }
            java.util.List r0 = a(r0)     // Catch:{ JSONException -> 0x023b }
            r6.I = r0     // Catch:{ JSONException -> 0x023b }
        L_0x01ee:
            r6.A()     // Catch:{ JSONException -> 0x023b }
        L_0x01f1:
            com.immomo.momo.android.activity.tieba.x r0 = new com.immomo.momo.android.activity.tieba.x
            r1 = 2131165799(0x7f070267, float:1.7945825E38)
            android.view.View r1 = r6.findViewById(r1)
            r0.<init>(r6, r1, r4)
            r6.j = r0
            com.immomo.momo.android.activity.tieba.x r0 = new com.immomo.momo.android.activity.tieba.x
            r1 = 2131165801(0x7f070269, float:1.794583E38)
            android.view.View r1 = r6.findViewById(r1)
            r0.<init>(r6, r1, r5)
            r6.l = r0
            com.immomo.momo.android.activity.tieba.x r0 = new com.immomo.momo.android.activity.tieba.x
            r1 = 2131165800(0x7f070268, float:1.7945827E38)
            android.view.View r1 = r6.findViewById(r1)
            r2 = 3
            r0.<init>(r6, r1, r2)
            r6.k = r0
            com.immomo.momo.android.activity.tieba.x r0 = new com.immomo.momo.android.activity.tieba.x
            r1 = 2131165797(0x7f070265, float:1.7945821E38)
            android.view.View r1 = r6.findViewById(r1)
            r2 = 5
            r0.<init>(r6, r1, r2)
            r6.m = r0
            com.immomo.momo.android.activity.tieba.x r0 = new com.immomo.momo.android.activity.tieba.x
            r1 = 2131165815(0x7f070277, float:1.7945858E38)
            android.view.View r1 = r6.findViewById(r1)
            r2 = 6
            r0.<init>(r6, r1, r2)
            r6.n = r0
            return
        L_0x023b:
            r0 = move-exception
            com.immomo.momo.util.m r1 = r6.e
            r1.a(r0)
            goto L_0x01f1
        L_0x0242:
            if (r7 == 0) goto L_0x034a
            java.lang.String r0 = "from_saveinstance"
            boolean r0 = r7.getBoolean(r0, r3)
            if (r0 == 0) goto L_0x034a
            java.lang.String r0 = "post_id"
            java.lang.String r0 = r7.getString(r0)
            r6.u = r0
            java.lang.String r0 = "tieba_name"
            java.lang.String r0 = r7.getString(r0)
            r6.v = r0
            java.lang.String r0 = "save_tiecontent"
            java.lang.Object r0 = r7.get(r0)
            if (r0 != 0) goto L_0x0340
            java.lang.String r0 = ""
        L_0x0266:
            boolean r1 = android.support.v4.b.a.a(r0)
            if (r1 != 0) goto L_0x027a
            com.immomo.momo.android.view.MEmoteEditeText r1 = r6.o
            r1.setText(r0)
            com.immomo.momo.android.view.MEmoteEditeText r1 = r6.o
            int r0 = r0.length()
            r1.setSelection(r0)
        L_0x027a:
            java.lang.String r0 = "camera_filename"
            boolean r0 = r7.containsKey(r0)
            if (r0 == 0) goto L_0x028a
            java.lang.String r0 = "camera_filename"
            java.lang.String r0 = r7.getString(r0)
            r6.L = r0
        L_0x028a:
            java.lang.String r0 = "camera_filepath"
            boolean r0 = r7.containsKey(r0)
            if (r0 == 0) goto L_0x029f
            java.io.File r0 = new java.io.File
            java.lang.String r1 = "camera_filepath"
            java.lang.String r1 = r7.getString(r1)
            r0.<init>(r1)
            r6.M = r0
        L_0x029f:
            java.lang.String r0 = "local_filepath"
            boolean r0 = r7.containsKey(r0)
            if (r0 == 0) goto L_0x02b4
            java.io.File r0 = new java.io.File
            java.lang.String r1 = "local_filepath"
            java.lang.String r1 = r7.getString(r1)
            r0.<init>(r1)
            r6.N = r0
        L_0x02b4:
            java.lang.String r0 = "posFilter"
            int r0 = r7.getInt(r0)
            r6.Q = r0
        L_0x02bc:
            java.lang.String r0 = r6.u
            boolean r0 = android.support.v4.b.a.a(r0)
            if (r0 != 0) goto L_0x02d3
            com.immomo.momo.service.ao r0 = new com.immomo.momo.service.ao
            r0.<init>()
            java.lang.String r1 = r6.u
            com.immomo.momo.service.bean.c.b r0 = r0.c(r1)
            r6.y = r0
            if (r0 != 0) goto L_0x02db
        L_0x02d3:
            java.lang.String r0 = "没有指定话题"
            r6.a(r0)
            r6.finish()
        L_0x02db:
            r6.A()
            com.immomo.momo.service.bean.c.b r0 = r6.y
            if (r0 == 0) goto L_0x0332
            com.immomo.momo.android.view.MEmoteEditeText r0 = r6.o
            com.immomo.momo.service.bean.c.b r1 = r6.y
            java.lang.String r1 = r1.g
            r0.setText(r1)
            com.immomo.momo.android.view.MEmoteEditeText r0 = r6.o
            com.immomo.momo.android.view.MEmoteEditeText r1 = r6.o
            android.text.Editable r1 = r1.getText()
            java.lang.String r1 = r1.toString()
            java.lang.String r1 = r1.trim()
            int r1 = r1.length()
            r0.setSelection(r1)
            android.widget.EditText r0 = r6.p
            com.immomo.momo.service.bean.c.b r1 = r6.y
            java.lang.String r1 = r1.d
            r0.setText(r1)
            android.widget.EditText r0 = r6.p
            android.widget.EditText r1 = r6.p
            android.text.Editable r1 = r1.getText()
            java.lang.String r1 = r1.toString()
            java.lang.String r1 = r1.trim()
            int r1 = r1.length()
            r0.setSelection(r1)
            com.immomo.momo.service.bean.c.b r0 = r6.y
            int r0 = r0.a()
            if (r0 != r4) goto L_0x037e
            r6.t = r5
            r6.z()
            r0 = 0
            r6.O = r0
        L_0x0332:
            android.os.Handler r0 = r6.z
            com.immomo.momo.android.activity.tieba.n r1 = new com.immomo.momo.android.activity.tieba.n
            r1.<init>(r6)
            r2 = 200(0xc8, double:9.9E-322)
            r0.postDelayed(r1, r2)
            goto L_0x01f1
        L_0x0340:
            java.lang.String r0 = "save_tiecontent"
            java.lang.Object r0 = r7.get(r0)
            java.lang.String r0 = (java.lang.String) r0
            goto L_0x0266
        L_0x034a:
            android.content.Intent r1 = r6.getIntent()
            if (r1 == 0) goto L_0x02bc
            android.os.Bundle r0 = r1.getExtras()
            if (r0 == 0) goto L_0x02bc
            java.lang.String r0 = "post_id"
            java.lang.String r0 = r1.getStringExtra(r0)
            if (r0 != 0) goto L_0x0370
            java.lang.String r0 = ""
        L_0x0360:
            r6.u = r0
            java.lang.String r0 = "tieba_name"
            java.lang.String r0 = r1.getStringExtra(r0)
            if (r0 != 0) goto L_0x0377
            java.lang.String r0 = ""
        L_0x036c:
            r6.v = r0
            goto L_0x02bc
        L_0x0370:
            java.lang.String r0 = "post_id"
            java.lang.String r0 = r1.getStringExtra(r0)
            goto L_0x0360
        L_0x0377:
            java.lang.String r0 = "tieba_name"
            java.lang.String r0 = r1.getStringExtra(r0)
            goto L_0x036c
        L_0x037e:
            com.immomo.momo.service.bean.c.b r0 = r6.y
            int r0 = r0.a()
            if (r0 != r5) goto L_0x0394
            r6.t = r4
            com.immomo.momo.plugin.b.a r0 = new com.immomo.momo.plugin.b.a
            com.immomo.momo.service.bean.c.b r1 = r6.y
            java.lang.String r1 = r1.s
            r0.<init>(r1)
            r6.O = r0
            goto L_0x0332
        L_0x0394:
            r6.t = r3
            goto L_0x0332
        */
        throw new UnsupportedOperationException("Method not decompiled: com.immomo.momo.android.activity.tieba.EditTieActivity.a(android.os.Bundle):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.h.a(java.lang.String, android.graphics.Bitmap, int, boolean):java.io.File
     arg types: [java.lang.String, android.graphics.Bitmap, int, int]
     candidates:
      com.immomo.momo.util.h.a(java.lang.String, java.lang.String, int, boolean):void
      com.immomo.momo.util.h.a(java.lang.String, android.graphics.Bitmap, int, boolean):java.io.File */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.b.a.a(android.graphics.Bitmap, float, boolean):android.graphics.Bitmap
     arg types: [android.graphics.Bitmap, int, int]
     candidates:
      android.support.v4.b.a.a(android.graphics.Bitmap, int, int):android.graphics.Bitmap
      android.support.v4.b.a.a(java.io.File, int, int):android.graphics.Bitmap
      android.support.v4.b.a.a(java.lang.String, java.lang.String, java.lang.String):java.lang.String
      android.support.v4.b.a.a(java.lang.Object[], java.lang.String, java.lang.String):java.lang.String
      android.support.v4.b.a.a(android.graphics.Bitmap, float, boolean):android.graphics.Bitmap */
    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        int i4 = 0;
        switch (i2) {
            case 11:
                if (i3 == -1) {
                    this.f.an = true;
                    new aq().b(this.f);
                    this.j.g = true;
                    this.j.a(true);
                    Intent intent2 = new Intent(w.f2366a);
                    intent2.putExtra("momoid", this.f.h);
                    sendBroadcast(intent2);
                    return;
                }
                return;
            case 12:
                if (i3 == -1) {
                    this.f.ar = true;
                    this.k.g = true;
                    this.k.a(true);
                    Intent intent3 = new Intent(w.f2366a);
                    intent3.putExtra("momoid", this.f.h);
                    sendBroadcast(intent3);
                    return;
                }
                return;
            case 13:
                if (i3 == -1) {
                    this.f.at = true;
                    new aq().b(this.f);
                    this.l.g = true;
                    this.l.a(true);
                    Intent intent4 = new Intent(w.f2366a);
                    intent4.putExtra("momoid", this.f.h);
                    sendBroadcast(intent4);
                    return;
                }
                return;
            case PurchaseCode.QUERY_OK /*101*/:
                if (i3 == -1 && intent != null) {
                    this.t = 2;
                    if (!android.support.v4.b.a.a((CharSequence) this.L)) {
                        File file = new File(com.immomo.momo.a.i(), this.L);
                        if (file.exists()) {
                            file.delete();
                        }
                        this.L = null;
                    }
                    if (this.M != null) {
                        String absolutePath = this.M.getAbsolutePath();
                        String substring = this.M.getName().substring(0, this.M.getName().lastIndexOf("."));
                        Bitmap l2 = android.support.v4.b.a.l(absolutePath);
                        if (l2 != null) {
                            File a2 = h.a(substring, l2, 16, false);
                            this.e.a((Object) ("scaleAndSavePhoto, uploadFile=" + a2.getPath()));
                            this.P = android.support.v4.b.a.a(l2, 150.0f, true);
                            h.a(substring, this.P, 15, false);
                            av avVar = new av();
                            avVar.b = a2;
                            avVar.f2998a = a2.getAbsolutePath();
                            avVar.c = this.P;
                            this.K.put(avVar.f2998a, avVar);
                            this.B.b(avVar);
                            this.B.setData(this.B.getDatalist());
                            l2.recycle();
                        }
                        try {
                            this.M.delete();
                            this.M = null;
                        } catch (Exception e) {
                        }
                        getWindow().getDecorView().requestFocus();
                        return;
                    }
                    return;
                } else if (i3 == 1003) {
                    ao.b("图片尺寸太小，请重新选择", 1);
                    return;
                } else if (i3 == 1000) {
                    ao.d(R.string.cropimage_error_other);
                    return;
                } else if (i3 == 1002) {
                    ao.d(R.string.cropimage_error_store);
                    return;
                } else if (i3 == 1001) {
                    ao.d(R.string.cropimage_error_filenotfound);
                    return;
                } else {
                    return;
                }
            case PurchaseCode.UNSUB_OK /*103*/:
                if (i3 == -1 && !android.support.v4.b.a.a((CharSequence) this.L)) {
                    this.o.requestFocus();
                    Uri fromFile = Uri.fromFile(new File(com.immomo.momo.a.i(), this.L));
                    if (fromFile != null) {
                        Intent intent5 = new Intent(this, ImageFactoryActivity.class);
                        intent5.setData(fromFile);
                        intent5.putExtra("minsize", 320);
                        intent5.putExtra("process_model", "filter");
                        intent5.putExtra("maxwidth", 720);
                        intent5.putExtra("maxheight", 3000);
                        this.M = new File(com.immomo.momo.a.g(), "temp_" + com.immomo.a.a.f.b.a() + ".jpg_");
                        intent5.putExtra("outputFilePath", this.M.getAbsolutePath());
                        startActivityForResult(intent5, PurchaseCode.QUERY_OK);
                        return;
                    }
                    return;
                }
                return;
            case PurchaseCode.AUTH_OK /*104*/:
                if (i3 == -1 && intent != null) {
                    this.t = 2;
                    ArrayList<String> stringArrayListExtra = intent.getStringArrayListExtra("select_images_path");
                    ArrayList arrayList = new ArrayList();
                    while (true) {
                        int i5 = i4;
                        if (i5 >= stringArrayListExtra.size()) {
                            b(new v(this, this, arrayList, D()));
                            return;
                        }
                        av avVar2 = new av();
                        avVar2.f2998a = stringArrayListExtra.get(i5);
                        arrayList.add(avVar2);
                        i4 = i5 + 1;
                    }
                } else {
                    return;
                }
                break;
            case 105:
                if (i3 != -1 || intent == null) {
                    if (i3 == 1003) {
                        ao.b("图片尺寸太小，请重新选择", 1);
                        return;
                    } else if (i3 == 1000) {
                        ao.d(R.string.cropimage_error_other);
                        return;
                    } else if (i3 == 1002) {
                        ao.d(R.string.cropimage_error_store);
                        return;
                    } else if (i3 == 1001) {
                        ao.d(R.string.cropimage_error_filenotfound);
                        return;
                    } else {
                        return;
                    }
                } else if (this.N != null) {
                    String absolutePath2 = this.N.getAbsolutePath();
                    String substring2 = this.N.getName().substring(0, this.N.getName().lastIndexOf("."));
                    Bitmap l3 = android.support.v4.b.a.l(absolutePath2);
                    if (l3 != null) {
                        File a3 = h.a(substring2, l3, 16, false);
                        this.e.a((Object) ("scaleAndSavePhoto, uploadFile=" + a3.getPath()));
                        Bitmap a4 = android.support.v4.b.a.a(l3, 150.0f, true);
                        h.a(substring2, a4, 15, false);
                        av avVar3 = (av) this.B.getDatalist().get(this.Q);
                        avVar3.b = a3;
                        avVar3.f2998a = a3.getAbsolutePath();
                        avVar3.c = a4;
                        this.K.put(avVar3.f2998a, avVar3);
                        this.B.a(this.Q, avVar3);
                        this.B.setData(this.B.getDatalist());
                        l3.recycle();
                    }
                    try {
                        this.N.delete();
                        this.N = null;
                    } catch (Exception e2) {
                    }
                    getWindow().getDecorView().requestFocus();
                    return;
                } else {
                    return;
                }
            default:
                return;
        }
    }

    public void onBackPressed() {
        if (r()) {
            if (this.x.isShown()) {
                this.x.a();
                this.h = false;
                return;
            }
            if (this.O != null || (this.B.getDatalist() != null && this.B.getDatalist().size() > 0) || android.support.v4.b.a.f(this.o.getText().toString().trim()) || android.support.v4.b.a.f(this.p.getText().toString().trim())) {
                n nVar = new n(this);
                nVar.a();
                nVar.setTitle((int) R.string.feed_publish_dialog_title);
                nVar.a("要放弃编辑话题吗？");
                nVar.a(0, (int) R.string.dialog_btn_confim, new l(this));
                nVar.a(1, (int) R.string.dialog_btn_cancel, new m());
                nVar.show();
                return;
            }
        }
        super.onBackPressed();
    }

    public void onClick(View view) {
        int i2 = 0;
        switch (view.getId()) {
            case R.id.btn_localphoto /*2131165805*/:
                E();
                if (this.B.getDatalist() == null || this.B.getDatalist().size() < 6) {
                    ArrayList arrayList = new ArrayList();
                    if (this.B.getDatalist() != null) {
                        for (int i3 = 0; i3 < this.B.getDatalist().size(); i3++) {
                            if (!((av) this.B.getDatalist().get(i3)).d) {
                                arrayList.add(((av) this.B.getDatalist().get(i3)).f2998a);
                            }
                        }
                    }
                    if (this.B.getDatalist() != null) {
                        i2 = this.B.getDatalist().size() - arrayList.size();
                    }
                    Intent intent = new Intent(this, MulImagePickerActivity.class);
                    intent.putStringArrayListExtra("select_images_path_results", arrayList);
                    intent.putExtra("max_select_images_num", 6 - i2);
                    startActivityForResult(intent, PurchaseCode.AUTH_OK);
                    return;
                }
                a((CharSequence) "最多发送6张图片");
                return;
            case R.id.btn_camera /*2131165806*/:
                E();
                if (this.B.getDatalist() == null) {
                    i2 = 1;
                } else if (this.B.getDatalist() != null && this.B.getDatalist().size() < 6) {
                    i2 = 1;
                }
                if (i2 == 0) {
                    a((CharSequence) "最多选择6张图片");
                    return;
                }
                Intent intent2 = new Intent("android.media.action.IMAGE_CAPTURE");
                StringBuffer stringBuffer = new StringBuffer();
                stringBuffer.append("immomo_");
                stringBuffer.append(android.support.v4.b.a.c(new Date()));
                stringBuffer.append("_" + UUID.randomUUID());
                stringBuffer.append(".jpg");
                this.L = stringBuffer.toString();
                intent2.putExtra("output", Uri.fromFile(new File(com.immomo.momo.a.i(), this.L)));
                startActivityForResult(intent2, PurchaseCode.UNSUB_OK);
                return;
            case R.id.btn_emote /*2131165807*/:
                c(4);
                return;
            case R.id.layout_selected_photo /*2131165808*/:
            case R.id.list_selectphotos /*2131165809*/:
            case R.id.layout_selected_emote /*2131165810*/:
            case R.id.iv_selected_emote /*2131165811*/:
            case R.id.tv_tip /*2131165813*/:
            case R.id.layout_line /*2131165814*/:
            case R.id.signeditor_layout_syncto_weixin /*2131165815*/:
            default:
                return;
            case R.id.iv_delete_emote /*2131165812*/:
                E();
                I();
                this.O = null;
                d(0);
                return;
            case R.id.btn_selectpic /*2131165816*/:
                u();
                this.z.postDelayed(new i(this), 300);
                return;
            case R.id.btn_selectemote /*2131165817*/:
                G();
                this.x.a();
                this.F.setSelected(false);
                this.G.setSelected(true);
                c(1);
                return;
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        y();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.j.b();
        this.k.b();
        this.l.b();
        this.m.b();
        this.n.b();
        if (this.J != null) {
            this.J.l();
        }
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Bundle bundle) {
        super.onRestoreInstanceState(bundle);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.s) {
            this.z.postDelayed(new b(this), 200);
            getWindow().getDecorView().requestFocus();
            this.h = this.x.isShown();
        }
        if (!this.s) {
            this.s = true;
        }
        this.h = false;
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        String editable = this.o.getText().toString();
        if (!android.support.v4.b.a.a((CharSequence) editable)) {
            bundle.putString("save_tiecontent", editable);
        }
        bundle.putString("post_id", this.u);
        bundle.putString("tieba_name", this.v);
        bundle.putBoolean("from_saveinstance", true);
        if (!android.support.v4.b.a.a((CharSequence) this.L)) {
            bundle.putString("camera_filename", this.L);
        }
        if (this.M != null) {
            bundle.putString("camera_filepath", this.M.getPath());
        }
        if (this.N != null) {
            bundle.putString("local_filepath", this.N.getPath());
        }
        bundle.putInt("posFilter", this.Q);
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        switch (view.getId()) {
            case R.id.layout_content /*2131165199*/:
                if ((this.x.isShown() || this.h) && 1 == motionEvent.getAction() && motionEvent.getEventTime() - motionEvent.getDownTime() < 100) {
                    if (this.x.isShown()) {
                        this.x.a();
                    } else {
                        u();
                    }
                    this.h = false;
                    break;
                }
            case R.id.signeditor_tv_text /*2131165795*/:
            case R.id.et_title /*2131165818*/:
                if (motionEvent.getAction() == 1) {
                    this.x.a();
                    G();
                    break;
                }
                break;
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public final void u() {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService("input_method");
        View currentFocus = getCurrentFocus();
        if (currentFocus != null) {
            inputMethodManager.hideSoftInputFromWindow(currentFocus.getWindowToken(), 2);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.h.a(java.lang.String, android.graphics.Bitmap, int, boolean):java.io.File
     arg types: [java.lang.String, android.graphics.Bitmap, int, int]
     candidates:
      com.immomo.momo.util.h.a(java.lang.String, java.lang.String, int, boolean):void
      com.immomo.momo.util.h.a(java.lang.String, android.graphics.Bitmap, int, boolean):java.io.File */
    public final void v() {
        int i2 = 1;
        boolean a2 = this.k.a();
        boolean a3 = this.j.a();
        boolean a4 = this.l.a();
        boolean a5 = this.m.a();
        boolean a6 = this.n.a();
        this.y = new b();
        HashMap hashMap = new HashMap();
        hashMap.put(v.j, new StringBuilder(String.valueOf(a3 ? 1 : 0)).toString());
        hashMap.put(v.k, new StringBuilder(String.valueOf(a2 ? 1 : 0)).toString());
        hashMap.put(v.l, new StringBuilder(String.valueOf(a4 ? 1 : 0)).toString());
        hashMap.put(v.m, new StringBuilder(String.valueOf(a5 ? 1 : 0)).toString());
        String str = v.n;
        if (!a6) {
            i2 = 0;
        }
        hashMap.put(str, new StringBuilder(String.valueOf(i2)).toString());
        hashMap.put(v.o, this.p.getText().toString().trim());
        hashMap.put(v.i, this.o.getText().toString().trim());
        hashMap.put(v.p, this.u);
        switch (this.t) {
            case 0:
                this.U = v.a().a(hashMap, new HashMap(), (a) null, this.y, this.T);
                break;
            case 1:
                this.U = v.a().a(hashMap, new HashMap(), this.O, this.y, this.T);
                break;
            case 2:
                for (Map.Entry entry : this.R.entrySet()) {
                    File file = (File) entry.getValue();
                    if (file == null || !file.exists()) {
                        this.z.post(new j(this));
                        throw new Exception();
                    }
                    String substring = file.getName().substring(0, file.getName().lastIndexOf("."));
                    entry.setValue(h.a(substring, android.support.v4.b.a.a(file, 720, 3000), 16, false));
                    this.S.put((String) entry.getKey(), substring);
                }
                this.U = v.a().a(hashMap, this.R, (a) null, this.y, this.T);
                break;
        }
        new com.immomo.momo.service.ao().a(this.y);
        a(this.y);
        if (this.n.a()) {
            this.z.post(new k(this));
        }
    }

    public final m w() {
        return this.V;
    }

    public final void x() {
        if (this.B.getDatalist() != null) {
            this.H.setText(new StringBuilder().append(this.B.getDatalist().size()).toString());
            if (this.B.getDatalist().size() > 0) {
                this.E.setVisibility(0);
                findViewById(R.id.btn_localphoto).setEnabled(true);
                findViewById(R.id.btn_camera).setEnabled(true);
                findViewById(R.id.btn_emote).setEnabled(false);
                findViewById(R.id.layout_tip).setVisibility(8);
                this.H.setVisibility(0);
                RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) this.D.getLayoutParams();
                layoutParams.height = -2;
                this.D.setLayoutParams(layoutParams);
                return;
            }
            I();
            this.H.setVisibility(8);
            RelativeLayout.LayoutParams layoutParams2 = (RelativeLayout.LayoutParams) this.D.getLayoutParams();
            layoutParams2.height = (int) (266.0f * g.k());
            this.D.setLayoutParams(layoutParams2);
            H();
            return;
        }
        this.H.setVisibility(8);
        H();
    }
}
