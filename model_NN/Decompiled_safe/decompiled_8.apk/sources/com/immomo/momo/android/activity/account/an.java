package com.immomo.momo.android.activity.account;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.b.a;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import com.immomo.momo.R;
import com.immomo.momo.android.view.a.n;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.util.ao;
import com.immomo.momo.util.aq;
import com.immomo.momo.util.k;

public final class an extends ab implements RadioGroup.OnCheckedChangeListener {

    /* renamed from: a  reason: collision with root package name */
    private bf f942a = null;
    private EditText b = null;
    private RadioGroup c = null;
    private Context d = null;
    private boolean e = false;

    public an(bf bfVar, View view, Context context) {
        super(view);
        this.f942a = bfVar;
        this.d = context;
        this.b = (EditText) a((int) R.id.rg_et_name);
        this.c = (RadioGroup) a((int) R.id.rg_radiogroup_gender);
        EditText editText = this.b;
        EditText editText2 = this.b;
        editText.addTextChangedListener(new aq(32));
        if (!a.a((CharSequence) this.f942a.i)) {
            this.b.setText(this.f942a.i);
        }
        if ("M".equalsIgnoreCase(this.f942a.H)) {
            ((RadioButton) a((int) R.id.rg_radiobutton_Male)).setChecked(true);
        } else if ("F".equalsIgnoreCase(this.f942a.H)) {
            ((RadioButton) a((int) R.id.rg_radiobutton_Female)).setChecked(true);
        }
        this.c.setOnCheckedChangeListener(this);
    }

    public final boolean a() {
        if (a(this.b)) {
            ao.e(R.string.reg_username_empty);
            return false;
        } else if (this.c.getCheckedRadioButtonId() < 0) {
            ao.e(R.string.reg_gender_empty);
            return false;
        } else {
            String trim = this.b.getText().toString().trim();
            try {
                if (trim.getBytes("GBK").length > 32) {
                    ao.b("姓名输入过长");
                    return false;
                }
            } catch (Exception e2) {
            }
            this.f942a.H = this.c.getCheckedRadioButtonId() == R.id.rg_radiobutton_Female ? "F" : "M";
            this.f942a.i = trim;
            return true;
        }
    }

    public final void e() {
        new k("PI", "P124").e();
    }

    public final void f() {
        new k("PO", "P124").e();
    }

    public final void onCheckedChanged(RadioGroup radioGroup, int i) {
        if (!this.e) {
            this.e = true;
            n.b(this.d, "注册成功后性别将不可更改", (DialogInterface.OnClickListener) null).show();
        }
        if (i == R.id.rg_radiobutton_Male) {
            new k("C", "C12403").e();
        } else if (i == R.id.rg_radiobutton_Female) {
            new k("C", "C12404").e();
        }
    }
}
