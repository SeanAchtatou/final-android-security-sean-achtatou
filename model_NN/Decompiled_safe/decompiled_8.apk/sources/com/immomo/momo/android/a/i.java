package com.immomo.momo.android.a;

import android.app.Activity;
import android.content.Context;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.service.aa;
import com.immomo.momo.service.bean.b.a;
import java.util.List;

public final class i extends BaseAdapter {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public Context f869a = null;
    /* access modifiers changed from: private */
    public aa b = null;
    private List c = null;
    private int d = 80;

    public i(Context context) {
        this.f869a = context;
        this.b = aa.a(this.f869a);
        this.c = this.b.e();
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) this.f869a).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        this.d = (displayMetrics.widthPixels / 3) - 42;
    }

    public final int getCount() {
        return this.c.size();
    }

    public final /* synthetic */ Object getItem(int i) {
        return (a) this.c.get(i);
    }

    public final long getItemId(int i) {
        return (long) i;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        n nVar;
        if (view == null) {
            view = LayoutInflater.from(this.f869a).inflate((int) R.layout.include_buckets_info, (ViewGroup) null);
            nVar = new n((byte) 0);
            nVar.f909a = (ImageView) view.findViewById(R.id.iv_image);
            nVar.b = (TextView) view.findViewById(R.id.tv_buckets_name);
            nVar.d = (TextView) view.findViewById(R.id.tv_selectnum);
            nVar.c = (TextView) view.findViewById(R.id.tv_buckets_num);
            nVar.e = view.findViewById(R.id.iv_image_cover);
            view.setTag(nVar);
        } else {
            nVar = (n) view.getTag();
        }
        ViewGroup.LayoutParams layoutParams = nVar.f909a.getLayoutParams();
        layoutParams.height = this.d;
        layoutParams.width = this.d;
        nVar.f909a.setLayoutParams(layoutParams);
        nVar.e.setLayoutParams(layoutParams);
        new k(this, new l(this.f869a.getMainLooper(), nVar.f909a), ((a) this.c.get(i)).d, ((a) this.c.get(i)).f == 1).start();
        nVar.b.setText(((a) this.c.get(i)).b);
        nVar.c.setText(" (" + ((a) this.c.get(i)).c + ")");
        if (((a) this.c.get(i)).e > 0) {
            nVar.d.setVisibility(0);
            nVar.d.setText("已选择" + ((a) this.c.get(i)).e + "张");
        } else {
            nVar.d.setVisibility(8);
        }
        nVar.e.setTag(R.id.tag_item_position, Integer.valueOf(i));
        nVar.e.setOnClickListener(new j(this));
        return view;
    }
}
