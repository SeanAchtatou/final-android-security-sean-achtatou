package com.immomo.momo.android.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.Adapter;
import android.widget.FrameLayout;
import com.immomo.momo.android.plugin.cropimage.q;
import com.immomo.momo.util.m;
import com.sina.weibo.sdk.constant.Constants;
import mm.purchasesdk.PurchaseCode;

public class FlingGallery extends FrameLayout {

    /* renamed from: a  reason: collision with root package name */
    public int f2629a = PurchaseCode.UNSUPPORT_ENCODING_ERR;
    protected int b = 0;
    private int c = 0;
    private boolean d = true;
    /* access modifiers changed from: private */
    public int e = 0;
    /* access modifiers changed from: private */
    public boolean f = false;
    /* access modifiers changed from: private */
    public boolean g = false;
    /* access modifiers changed from: private */
    public float h = 0.0f;
    /* access modifiers changed from: private */
    public long i = 0;
    private int j = 0;
    /* access modifiers changed from: private */
    public int k = 0;
    /* access modifiers changed from: private */
    public Context l;
    /* access modifiers changed from: private */
    public Adapter m;
    /* access modifiers changed from: private */
    public ar[] n;
    private aq o;
    /* access modifiers changed from: private */
    public Interpolator p;
    private q q = null;
    private q r;
    private View s;

    public FlingGallery(Context context) {
        super(context);
        new m("FlingGallery");
        this.r = null;
        this.s = null;
        this.l = context;
        d();
    }

    public FlingGallery(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        new m("FlingGallery");
        this.r = null;
        this.s = null;
        this.l = context;
        d();
    }

    public FlingGallery(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        new m("FlingGallery");
        this.r = null;
        this.s = null;
        this.l = context;
        d();
    }

    static /* synthetic */ int a(FlingGallery flingGallery, int i2, int i3) {
        int i4 = flingGallery.e + flingGallery.c;
        if (i2 == e(i3)) {
            return i4;
        }
        if (i2 == f(i3)) {
            return i4 * -1;
        }
        return 0;
    }

    private int c(int i2) {
        int i3 = i2 - 1;
        if (i3 >= getFirstPosition()) {
            return i3;
        }
        return this.d ? getLastPosition() : getFirstPosition() - 1;
    }

    private int d(int i2) {
        int i3 = i2 + 1;
        if (i3 <= getLastPosition()) {
            return i3;
        }
        return this.d ? getFirstPosition() : getLastPosition() + 1;
    }

    private void d() {
        this.m = null;
        this.n = new ar[3];
        this.n[0] = new ar(this, 0, this);
        this.n[1] = new ar(this, 1, this);
        this.n[2] = new ar(this, 2, this);
        this.o = new aq(this);
        new GestureDetector(new as(this, (byte) 0));
        this.p = AnimationUtils.loadInterpolator(this.l, 17432582);
    }

    /* access modifiers changed from: private */
    public static int e(int i2) {
        if (i2 == 0) {
            return 2;
        }
        return i2 - 1;
    }

    /* access modifiers changed from: private */
    public static int f(int i2) {
        if (i2 == 2) {
            return 0;
        }
        return i2 + 1;
    }

    public final void a() {
        this.b = 1;
        c();
    }

    public final void b() {
        this.b = -1;
        c();
    }

    /* access modifiers changed from: package-private */
    public final void c() {
        int i2;
        int i3;
        int i4;
        if (this.m.getCount() > 0) {
            int i5 = this.k;
            this.f = false;
            this.g = false;
            int i6 = this.j;
            if (this.b <= 0 || (this.j <= getFirstPosition() && !this.d)) {
                i2 = 0;
                i3 = i5;
                i4 = 0;
            } else {
                i3 = e(this.k);
                this.j = c(this.j);
                i2 = f(this.k);
                i4 = c(this.j);
            }
            if (this.b < 0 && (this.j < getLastPosition() || this.d)) {
                i3 = f(this.k);
                this.j = d(this.j);
                i2 = e(this.k);
                i4 = d(this.j);
            }
            if (i3 != this.k) {
                int i7 = this.j;
                this.k = i3;
                this.n[i2].a(i4);
                View a2 = this.n[this.k].e;
                if (this.q != null) {
                    q qVar = this.q;
                    int i8 = this.j;
                }
                if (!(i6 == this.j || this.r == null)) {
                    q qVar2 = this.r;
                    View view = this.s;
                }
                this.s = a2;
            }
            this.o.a(this.k);
            startAnimation(this.o);
            this.n[this.k].b();
            this.b = 0;
        }
    }

    public Adapter getAdapter() {
        return this.m;
    }

    public int getCurrentPosition() {
        return this.j;
    }

    public int getFirstPosition() {
        return 0;
    }

    public int getGalleryCount() {
        if (this.m == null) {
            return 0;
        }
        return this.m.getCount();
    }

    /* access modifiers changed from: protected */
    public Interpolator getInterpolator() {
        return new DecelerateInterpolator();
    }

    public int getLastPosition() {
        if (getGalleryCount() == 0) {
            return 0;
        }
        return getGalleryCount() - 1;
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        switch (i2) {
            case 21:
                a();
                return true;
            case Constants.WEIBO_SDK_VERSION /*22*/:
                b();
                return true;
            default:
                return super.onKeyDown(i2, keyEvent);
        }
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        super.onLayout(z, i2, i3, i4, i5);
        this.e = i4 - i2;
        if (z) {
            this.n[0].a(0, this.k);
            this.n[1].a(0, this.k);
            this.n[2].a(0, this.k);
        }
    }

    public void setAdapter(Adapter adapter) {
        this.m = adapter;
        this.j = 0;
        this.k = 0;
        if (adapter.getCount() > 0) {
            this.s = this.n[0].a(this.j);
            this.n[1].a(d(this.j));
            this.n[2].a(c(this.j));
            this.n[0].a(0, this.k);
            this.n[1].a(0, this.k);
            this.n[2].a(0, this.k);
            if (this.q != null) {
                q qVar = this.q;
                View view = this.s;
                int i2 = this.j;
            }
        }
    }

    public void setAnimationDuration(int i2) {
        this.f2629a = i2;
    }

    public void setIsGalleryCircular(boolean z) {
        if (this.d != z) {
            this.d = z;
            if (this.j == getFirstPosition()) {
                this.n[e(this.k)].a(c(this.j));
            }
            if (this.j == getLastPosition()) {
                this.n[f(this.k)].a(d(this.j));
            }
        }
    }

    public void setOnItemHidedListener$1a5d7bbe(q qVar) {
        this.r = qVar;
    }

    public void setOnItemSelectedListener$6516d18d(q qVar) {
        this.q = qVar;
    }

    public void setPaddingWidth(int i2) {
        this.c = i2;
    }

    public void setSnapBorderRatio(float f2) {
    }
}
