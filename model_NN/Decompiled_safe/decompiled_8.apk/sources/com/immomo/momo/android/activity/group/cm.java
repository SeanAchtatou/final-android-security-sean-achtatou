package com.immomo.momo.android.activity.group;

import android.content.Context;
import android.support.v4.b.a;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.n;
import com.immomo.momo.service.bean.as;
import java.util.ArrayList;

final class cm extends d {

    /* renamed from: a  reason: collision with root package name */
    private v f1586a;
    private /* synthetic */ GroupPartyActivity c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public cm(GroupPartyActivity groupPartyActivity, Context context) {
        super(context);
        this.c = groupPartyActivity;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        return n.a().f(this.c.u, "join");
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.f1586a = new v(this.c);
        this.f1586a.a("请求提交中");
        this.f1586a.setCancelable(true);
        this.f1586a.setOnCancelListener(new cn(this));
        this.f1586a.show();
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        String str = (String) obj;
        if (!a.a((CharSequence) str)) {
            a(str);
            this.c.t.o = 1;
            if (this.c.t.p == null) {
                this.c.t.p = new ArrayList();
            }
            this.c.t.p.add(0, new as(this.c.f.h, this.c.f.i, this.c.f.ae[0], this.c.f.b));
            this.c.t.n++;
            this.c.w.a(this.c.t);
            this.c.A();
        }
        GroupPartyActivity.g(this.c);
    }

    /* access modifiers changed from: protected */
    public final void b() {
        if (this.f1586a != null) {
            this.f1586a.dismiss();
        }
    }
}
