package com.immomo.momo.android.activity;

import android.content.Context;
import android.location.Location;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.w;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

final class mi extends d {

    /* renamed from: a  reason: collision with root package name */
    private StringBuilder f2001a = null;
    private Location c = null;
    private /* synthetic */ UserRoamActivity d;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.android.activity.UserRoamActivity.b(com.immomo.momo.android.activity.UserRoamActivity, boolean):void
     arg types: [com.immomo.momo.android.activity.UserRoamActivity, int]
     candidates:
      com.immomo.momo.android.activity.UserRoamActivity.b(com.immomo.momo.android.activity.UserRoamActivity, java.lang.String):void
      com.immomo.momo.android.activity.UserRoamActivity.b(com.immomo.momo.android.activity.UserRoamActivity, boolean):void */
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public mi(UserRoamActivity userRoamActivity, Context context, Location location) {
        super(context);
        this.d = userRoamActivity;
        this.c = location;
        userRoamActivity.C = true;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        ArrayList arrayList = new ArrayList();
        this.f2001a = new StringBuilder();
        w.a().a(arrayList, this.c, this.f2001a, this.d.e.n.a(), this.d.e.o.a());
        this.d.r.a(arrayList);
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.d.a(new v(f(), this));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.android.activity.UserRoamActivity.b(com.immomo.momo.android.activity.UserRoamActivity, boolean):void
     arg types: [com.immomo.momo.android.activity.UserRoamActivity, int]
     candidates:
      com.immomo.momo.android.activity.UserRoamActivity.b(com.immomo.momo.android.activity.UserRoamActivity, java.lang.String):void
      com.immomo.momo.android.activity.UserRoamActivity.b(com.immomo.momo.android.activity.UserRoamActivity, boolean):void */
    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        super.a(exc);
        this.d.C = false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.android.a.a.a(java.util.Collection, boolean):void
     arg types: [java.util.List, int]
     candidates:
      com.immomo.momo.android.a.a.a(int, java.lang.Object):void
      com.immomo.momo.android.a.a.a(int, java.util.Collection):void
      com.immomo.momo.android.a.a.a(java.lang.Object, int):void
      com.immomo.momo.android.a.a.a(java.util.Collection, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.android.activity.UserRoamActivity.b(com.immomo.momo.android.activity.UserRoamActivity, boolean):void
     arg types: [com.immomo.momo.android.activity.UserRoamActivity, int]
     candidates:
      com.immomo.momo.android.activity.UserRoamActivity.b(com.immomo.momo.android.activity.UserRoamActivity, java.lang.String):void
      com.immomo.momo.android.activity.UserRoamActivity.b(com.immomo.momo.android.activity.UserRoamActivity, boolean):void */
    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        List list = (List) obj;
        if (this.d.C) {
            this.d.q.a(false);
            this.d.q.a((Collection) list, false);
            if (this.d.h.isShown()) {
                this.d.h.setVisibility(8);
                this.d.p.setVisibility(0);
            }
            if (!this.d.p.isShown()) {
                this.d.p.setVisibility(0);
            }
            if (!this.d.j.isShown()) {
                this.d.j.setAnimation(this.d.i());
                this.d.j.setVisibility(0);
            }
            if (!this.d.g.isShown()) {
                this.d.g.setAnimation(this.d.h());
                this.d.g.setVisibility(0);
            }
            this.d.q.notifyDataSetChanged();
            this.d.a(this.c);
            this.d.a();
            this.d.v = this.f2001a.toString();
            this.d.b.setText(this.d.v);
            this.d.f.setText(this.d.v);
            this.d.C = false;
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.d.b();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.android.activity.UserRoamActivity.b(com.immomo.momo.android.activity.UserRoamActivity, boolean):void
     arg types: [com.immomo.momo.android.activity.UserRoamActivity, int]
     candidates:
      com.immomo.momo.android.activity.UserRoamActivity.b(com.immomo.momo.android.activity.UserRoamActivity, java.lang.String):void
      com.immomo.momo.android.activity.UserRoamActivity.b(com.immomo.momo.android.activity.UserRoamActivity, boolean):void */
    /* access modifiers changed from: protected */
    public final void onCancelled() {
        this.d.C = false;
    }
}
