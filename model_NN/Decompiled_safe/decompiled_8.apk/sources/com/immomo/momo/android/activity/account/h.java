package com.immomo.momo.android.activity.account;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.b.a;
import com.immomo.momo.R;
import com.immomo.momo.a.i;
import com.immomo.momo.a.j;
import com.immomo.momo.a.l;
import com.immomo.momo.android.activity.maintab.MaintabActivity;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.n;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.g;
import com.immomo.momo.protocol.a.w;
import com.immomo.momo.service.aq;
import com.immomo.momo.service.bean.at;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.util.jni.Codec;
import com.immomo.momo.util.k;
import org.json.JSONObject;

final class h extends d {

    /* renamed from: a  reason: collision with root package name */
    private bf f1002a = null;
    private aq c = null;
    private int d;
    private /* synthetic */ LoginActivity e;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public h(LoginActivity loginActivity, Context context, bf bfVar, int i) {
        super(context);
        this.e = loginActivity;
        this.f1002a = bfVar;
        this.d = i;
        this.c = new aq(this.f1002a.h);
        this.f1002a.aE = at.a(g.c(), this.f1002a.h);
    }

    /* access modifiers changed from: protected */
    public final Object a(Object... objArr) {
        w.a().a(this.f1002a, this.f1002a.W, this.d, a.b(Codec.gpi(this.e.n(), this.d)));
        this.c.b(this.f1002a);
        SharedPreferences.Editor edit = g.d().f668a.a().edit();
        edit.putString("momoid", this.f1002a.h);
        this.b.a((Object) ("--------------------putaccount=" + this.e.z));
        edit.putString("account", this.e.z);
        edit.putString("cookie", Codec.c(this.f1002a.W));
        edit.commit();
        this.e.t().a(this.f1002a, this.f1002a.aE);
        this.f1002a = null;
        return null;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        v vVar = new v(this.e, (int) R.string.login_success_init);
        vVar.setOnCancelListener(new i(this));
        this.e.a(vVar);
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        this.e.p();
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("error", exc.toString());
            new k("U", "U95", jSONObject).e();
        } catch (Exception e2) {
        }
        if ((exc instanceof l) && !this.e.isFinishing()) {
            n b = n.b(this.e.n(), exc.getMessage(), (DialogInterface.OnClickListener) null);
            b.setCancelable(false);
            b.show();
        } else if (exc instanceof i) {
            this.e.g();
        } else if (exc instanceof j) {
            this.e.f();
        } else if (exc instanceof com.immomo.momo.a.k) {
            this.e.f();
        } else {
            super.a(exc);
        }
    }

    /* access modifiers changed from: protected */
    public final void a(Object obj) {
        this.e.startActivity(new Intent(this.e.getApplicationContext(), MaintabActivity.class));
        this.e.finish();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.c.a();
    }
}
