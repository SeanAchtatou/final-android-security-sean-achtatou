package com.immomo.momo.android.activity.group.foundgroup;

import android.text.InputFilter;
import android.text.Spanned;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.util.ao;
import java.io.UnsupportedEncodingException;

final class i implements InputFilter {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ h f1668a;

    i(h hVar) {
        this.f1668a = hVar;
    }

    public final CharSequence filter(CharSequence charSequence, int i, int i2, Spanned spanned, int i3, int i4) {
        try {
            if ((String.valueOf(this.f1668a.b.getText().toString().trim()) + charSequence.toString().trim()).getBytes("GBK").length > 20) {
                ao.e(R.string.foundgroup_step2_string6);
                return PoiTypeDef.All;
            }
        } catch (UnsupportedEncodingException e) {
        }
        return null;
    }
}
