package com.immomo.momo.android.activity.message;

import android.content.Intent;
import com.immomo.momo.android.activity.group.GroupMemberListActivity;
import com.immomo.momo.android.view.LoadingButton;
import com.immomo.momo.android.view.bl;

final class bi implements bl {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ GroupChatActivity f1942a;
    private final /* synthetic */ LoadingButton b;

    bi(GroupChatActivity groupChatActivity, LoadingButton loadingButton) {
        this.f1942a = groupChatActivity;
        this.b = loadingButton;
    }

    public final void u() {
        this.b.e();
        Intent intent = new Intent(this.f1942a.n(), GroupMemberListActivity.class);
        intent.putExtra("gid", this.f1942a.i);
        intent.putExtra("count", this.f1942a.Q.k);
        this.f1942a.startActivity(intent);
    }
}
