package com.immomo.momo.android.a;

import android.view.View;

final class ba implements View.OnLongClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ax f730a;
    private final /* synthetic */ int b;

    ba(ax axVar, int i) {
        this.f730a = axVar;
        this.b = i;
    }

    public final boolean onLongClick(View view) {
        if (this.f730a.f.getOnItemLongClickListenerInWrapper() == null) {
            return false;
        }
        return this.f730a.f.getOnItemLongClickListenerInWrapper().onItemLongClick(this.f730a.f, view, this.b, (long) view.getId());
    }
}
