package com.immomo.momo.android.activity.feed;

import com.immomo.momo.android.view.MGifImageView;
import com.immomo.momo.android.view.au;
import java.io.File;

final class bc implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ bb f1460a;
    private final /* synthetic */ File b;
    private final /* synthetic */ boolean c;
    private final /* synthetic */ MGifImageView d;

    bc(bb bbVar, File file, boolean z, MGifImageView mGifImageView) {
        this.f1460a = bbVar;
        this.b = file;
        this.c = z;
        this.d = mGifImageView;
    }

    public final void run() {
        if (this.b != null && this.b.exists()) {
            this.f1460a.f1459a.H = new au(this.c ? 1 : 2);
            this.f1460a.f1459a.H.a(this.b, this.d);
            this.f1460a.f1459a.H.b(20);
            this.d.setGifDecoder(this.f1460a.f1459a.H);
        }
    }
}
