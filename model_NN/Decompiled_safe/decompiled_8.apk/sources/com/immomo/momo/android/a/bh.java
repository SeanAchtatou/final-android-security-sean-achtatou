package com.immomo.momo.android.a;

import android.content.Context;
import android.support.v4.b.a;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.g;
import com.immomo.momo.plugin.a.c;
import com.immomo.momo.util.e;
import java.util.ArrayList;

public final class bh extends a {
    public bh(Context context) {
        super(context, new ArrayList());
        new bi();
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = g.o().inflate((int) R.layout.listitem_renren, (ViewGroup) null);
            bj bjVar = new bj((byte) 0);
            view.setTag(bjVar);
            bjVar.f737a = (TextView) view.findViewById(R.id.renrenitem_tv_textcontent);
            bjVar.b = (TextView) view.findViewById(R.id.renrenitem_iv_posttime);
        }
        c cVar = (c) getItem(i);
        bj bjVar2 = (bj) view.getTag();
        e.a(bjVar2.f737a, (cVar.f2843a == null ? PoiTypeDef.All : cVar.f2843a).replaceAll("</?[a-zA-Z]+[^><]*>", PoiTypeDef.All), d());
        bjVar2.b.setText(cVar.b == null ? "未知" : a.e(cVar.b));
        return view;
    }
}
