package com.immomo.momo.android.view;

import android.view.View;
import com.immomo.momo.service.bean.d;

final class h implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ g f2807a;

    h(g gVar) {
        this.f2807a = gVar;
    }

    public final void onClick(View view) {
        int intValue = ((Integer) view.getTag()).intValue();
        if (intValue >= 0 && intValue < this.f2807a.g.size()) {
            this.f2807a.a((d) this.f2807a.g.get(intValue));
        }
    }
}
