package com.immomo.momo.android.view;

import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;

final class ae extends Handler {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ EmoteInputView f2714a;

    ae(EmoteInputView emoteInputView) {
        this.f2714a = emoteInputView;
    }

    public final void handleMessage(Message message) {
        switch (message.what) {
            case 338:
                if (this.f2714a.f != null) {
                    if (!this.f2714a.f.isFocused()) {
                        this.f2714a.f.requestFocus();
                        this.f2714a.f.setSelection(this.f2714a.f.getText().length());
                    }
                    this.f2714a.f.dispatchKeyEvent(new KeyEvent(0, 67));
                    return;
                }
                return;
            default:
                return;
        }
    }
}
