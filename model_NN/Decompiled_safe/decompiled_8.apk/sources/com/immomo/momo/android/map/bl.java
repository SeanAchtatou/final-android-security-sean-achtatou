package com.immomo.momo.android.map;

import android.location.Location;

final class bl implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ bk f2491a;
    private final /* synthetic */ Location b;

    bl(bk bkVar, Location location) {
        this.f2491a = bkVar;
        this.b = location;
    }

    public final void run() {
        if (!this.f2491a.f2490a.isFinishing() && this.f2491a.f2490a.x != null) {
            this.f2491a.f2490a.x.dismiss();
            this.f2491a.f2490a.x = null;
        }
        this.f2491a.f2490a.o = this.b;
        this.f2491a.f2490a.b();
    }
}
