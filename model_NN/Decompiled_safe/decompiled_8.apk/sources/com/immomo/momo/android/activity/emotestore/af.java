package com.immomo.momo.android.activity.emotestore;

import android.content.Intent;
import android.support.v4.b.a;
import com.immomo.momo.android.broadcast.d;
import com.immomo.momo.android.broadcast.z;
import com.immomo.momo.service.bean.q;

final class af implements d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ae f1284a;

    af(ae aeVar) {
        this.f1284a = aeVar;
    }

    public final void a(Intent intent) {
        if (z.f2368a.equals(intent.getAction())) {
            String stringExtra = intent.getStringExtra("eid");
            if (!a.a((CharSequence) stringExtra)) {
                for (q qVar : this.f1284a.R.a()) {
                    if (qVar.f3035a.equals(stringExtra)) {
                        qVar.q = true;
                        this.f1284a.R.notifyDataSetChanged();
                        return;
                    }
                }
            }
        }
    }
}
