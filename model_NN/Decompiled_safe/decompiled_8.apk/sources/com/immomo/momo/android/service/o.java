package com.immomo.momo.android.service;

import android.content.Intent;
import android.net.NetworkInfo;
import com.immomo.momo.g;

final class o extends Thread {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ NetWorkTestService f2615a;

    private o(NetWorkTestService netWorkTestService) {
        this.f2615a = netWorkTestService;
    }

    /* synthetic */ o(NetWorkTestService netWorkTestService, byte b) {
        this(netWorkTestService);
    }

    private static Intent a() {
        Intent intent = new Intent(String.valueOf(g.h()) + ".action.net.conn.CONNECTIVITY_CHANGE");
        NetworkInfo newInstance = NetworkInfo.class.getConstructor(Integer.TYPE, Integer.TYPE, String.class, String.class).newInstance(0, 0, "mobile", "mobile");
        NetworkInfo.class.getMethod("setDetailedState", NetworkInfo.DetailedState.class, String.class, String.class).invoke(newInstance, NetworkInfo.DetailedState.DISCONNECTED, "reason", "extraInfo");
        intent.putExtra("networkInfo", newInstance);
        return intent;
    }

    private static Intent b() {
        Intent intent = new Intent(String.valueOf(g.h()) + ".action.net.conn.CONNECTIVITY_CHANGE");
        NetworkInfo newInstance = NetworkInfo.class.getConstructor(Integer.TYPE, Integer.TYPE, String.class, String.class).newInstance(0, 0, "mobile", "mobile");
        NetworkInfo.class.getMethod("setDetailedState", NetworkInfo.DetailedState.class, String.class, String.class).invoke(newInstance, NetworkInfo.DetailedState.CONNECTED, "reason", "extraInfo");
        intent.putExtra("networkInfo", newInstance);
        return intent;
    }

    private static Intent c() {
        Intent intent = new Intent(String.valueOf(g.h()) + ".action.net.conn.CONNECTIVITY_CHANGE");
        NetworkInfo newInstance = NetworkInfo.class.getConstructor(Integer.TYPE, Integer.TYPE, String.class, String.class).newInstance(0, 0, "WIFI", "WIFI");
        NetworkInfo.class.getMethod("setDetailedState", NetworkInfo.DetailedState.class, String.class, String.class).invoke(newInstance, NetworkInfo.DetailedState.CONNECTED, "reason", "extraInfo");
        intent.putExtra("networkInfo", newInstance);
        return intent;
    }

    private static Intent d() {
        Intent intent = new Intent(String.valueOf(g.h()) + ".action.net.conn.CONNECTIVITY_CHANGE");
        NetworkInfo newInstance = NetworkInfo.class.getConstructor(Integer.TYPE, Integer.TYPE, String.class, String.class).newInstance(0, 0, "WIFI", "WIFI");
        NetworkInfo.class.getMethod("setDetailedState", NetworkInfo.DetailedState.class, String.class, String.class).invoke(newInstance, NetworkInfo.DetailedState.DISCONNECTED, "reason", "extraInfo");
        intent.putExtra("networkInfo", newInstance);
        return intent;
    }

    public final void run() {
        while (NetWorkTestService.f2601a) {
            try {
                Thread.sleep(10000);
                int nextInt = this.f2615a.b.nextInt(30);
                if (nextInt % 2 == 0) {
                    if (nextInt % 3 == 0) {
                        this.f2615a.c.a((Object) "model 1");
                        this.f2615a.sendBroadcast(a());
                        this.f2615a.sendBroadcast(b());
                        this.f2615a.sendBroadcast(c());
                        this.f2615a.sendBroadcast(a());
                        Thread.sleep(500);
                        this.f2615a.sendBroadcast(c());
                        Thread.sleep(2000);
                        this.f2615a.sendBroadcast(a());
                        this.f2615a.sendBroadcast(b());
                        this.f2615a.sendBroadcast(c());
                        this.f2615a.sendBroadcast(a());
                        Thread.sleep(500);
                        this.f2615a.sendBroadcast(c());
                        Thread.sleep(2000);
                        this.f2615a.sendBroadcast(a());
                        this.f2615a.sendBroadcast(b());
                        this.f2615a.sendBroadcast(c());
                        this.f2615a.sendBroadcast(a());
                        Thread.sleep(500);
                        this.f2615a.sendBroadcast(c());
                    } else if (nextInt % 4 == 0) {
                        this.f2615a.c.a((Object) "model 2");
                        this.f2615a.sendBroadcast(c());
                        this.f2615a.sendBroadcast(c());
                        Thread.sleep(1000);
                        this.f2615a.sendBroadcast(a());
                        this.f2615a.sendBroadcast(c());
                        Thread.sleep(2000);
                        this.f2615a.sendBroadcast(c());
                        this.f2615a.sendBroadcast(c());
                        Thread.sleep(1000);
                        this.f2615a.sendBroadcast(a());
                        this.f2615a.sendBroadcast(c());
                        Thread.sleep(2000);
                        this.f2615a.sendBroadcast(c());
                        this.f2615a.sendBroadcast(c());
                        Thread.sleep(1000);
                        this.f2615a.sendBroadcast(a());
                        this.f2615a.sendBroadcast(c());
                    } else if (nextInt % 5 == 0) {
                        this.f2615a.c.a((Object) "model 3");
                        this.f2615a.sendBroadcast(c());
                        Thread.sleep(1000);
                        this.f2615a.sendBroadcast(a());
                        Thread.sleep(5000);
                        this.f2615a.sendBroadcast(c());
                        Thread.sleep(2000);
                        this.f2615a.sendBroadcast(c());
                        Thread.sleep(1000);
                        this.f2615a.sendBroadcast(a());
                        Thread.sleep(5000);
                        this.f2615a.sendBroadcast(c());
                        Thread.sleep(2000);
                        this.f2615a.sendBroadcast(c());
                        Thread.sleep(1000);
                        this.f2615a.sendBroadcast(a());
                        Thread.sleep(5000);
                        this.f2615a.sendBroadcast(c());
                    } else if (nextInt % 10 == 0) {
                        this.f2615a.c.a((Object) "model 4");
                        this.f2615a.sendBroadcast(a());
                        this.f2615a.sendBroadcast(c());
                        this.f2615a.sendBroadcast(c());
                        Thread.sleep(2000);
                        this.f2615a.sendBroadcast(a());
                        this.f2615a.sendBroadcast(c());
                        this.f2615a.sendBroadcast(d());
                        Thread.sleep(8000);
                        this.f2615a.sendBroadcast(c());
                        this.f2615a.sendBroadcast(a());
                        this.f2615a.sendBroadcast(c());
                    } else {
                        this.f2615a.c.a((Object) "model 5");
                        this.f2615a.sendBroadcast(c());
                        this.f2615a.sendBroadcast(a());
                        Thread.sleep(2000);
                        this.f2615a.sendBroadcast(b());
                        this.f2615a.sendBroadcast(a());
                        Thread.sleep(2000);
                        this.f2615a.sendBroadcast(a());
                        this.f2615a.sendBroadcast(c());
                        Thread.sleep(2000);
                        this.f2615a.sendBroadcast(c());
                        this.f2615a.sendBroadcast(a());
                        Thread.sleep(2000);
                        this.f2615a.sendBroadcast(b());
                        this.f2615a.sendBroadcast(a());
                        Thread.sleep(2000);
                        this.f2615a.sendBroadcast(a());
                        this.f2615a.sendBroadcast(c());
                        Thread.sleep(2000);
                        this.f2615a.sendBroadcast(c());
                        this.f2615a.sendBroadcast(a());
                        Thread.sleep(2000);
                        this.f2615a.sendBroadcast(b());
                        this.f2615a.sendBroadcast(a());
                        Thread.sleep(2000);
                        this.f2615a.sendBroadcast(d());
                        this.f2615a.sendBroadcast(c());
                    }
                }
            } catch (Exception e) {
            }
        }
    }
}
