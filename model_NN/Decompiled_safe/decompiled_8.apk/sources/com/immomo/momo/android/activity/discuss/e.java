package com.immomo.momo.android.activity.discuss;

import android.view.View;
import com.immomo.momo.android.view.a.n;
import java.util.List;

final class e implements View.OnClickListener {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public /* synthetic */ DiscussMemberListActivity f1231a;

    e(DiscussMemberListActivity discussMemberListActivity) {
        this.f1231a = discussMemberListActivity;
    }

    public final void onClick(View view) {
        List d = this.f1231a.h.d();
        if (!d.isEmpty()) {
            n.a(this.f1231a, "是否移除所选群成员？", new f(this, d)).show();
        }
    }
}
