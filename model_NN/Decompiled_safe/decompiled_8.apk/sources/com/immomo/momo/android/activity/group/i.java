package com.immomo.momo.android.activity.group;

import android.app.TimePickerDialog;
import android.support.v4.b.a;
import android.widget.TimePicker;

final class i implements TimePickerDialog.OnTimeSetListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ EditGroupPartyActivity f1684a;

    i(EditGroupPartyActivity editGroupPartyActivity) {
        this.f1684a = editGroupPartyActivity;
    }

    public final void onTimeSet(TimePicker timePicker, int i, int i2) {
        this.f1684a.k = i;
        this.f1684a.l = i2;
        this.f1684a.m.clear();
        this.f1684a.m.set(this.f1684a.h, this.f1684a.i, this.f1684a.j, this.f1684a.k, this.f1684a.l);
        this.f1684a.x.f = this.f1684a.m.getTime();
        this.f1684a.q.setText(a.g(this.f1684a.x.f));
    }
}
