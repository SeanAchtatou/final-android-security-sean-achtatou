package com.immomo.momo.android.game;

import android.os.Bundle;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.ao;
import com.immomo.momo.android.view.HeaderLayout;
import com.immomo.momo.android.view.MomoRatingBar;
import com.immomo.momo.android.view.bi;

public class MDKFeedBackActivity extends ao implements CompoundButton.OnCheckedChangeListener {
    private MomoRatingBar h;
    /* access modifiers changed from: private */
    public EditText i;
    /* access modifiers changed from: private */
    public TextView j;
    private bi k;
    private HeaderLayout l;
    private RadioButton m = null;
    private RadioButton n = null;
    private RadioButton o = null;
    private RadioButton p = null;
    /* access modifiers changed from: private */
    public int q = 0;
    /* access modifiers changed from: private */
    public int r = 1;
    /* access modifiers changed from: private */
    public String s;
    /* access modifiers changed from: private */
    public String t;

    static /* synthetic */ String c(int i2) {
        return i2 <= 1 ? "不满" : i2 == 2 ? "很差" : i2 == 3 ? "一般" : i2 == 4 ? "不错" : i2 >= 5 ? "很好" : PoiTypeDef.All;
    }

    public void onCheckedChanged(CompoundButton compoundButton, boolean z) {
        if (z) {
            switch (compoundButton.getId()) {
                case R.id.feedback_filter_radiobutton_advice /*2131165621*/:
                    this.r = 1;
                    return;
                case R.id.feedback_filter_radiobutton_suggest /*2131165622*/:
                    this.r = 2;
                    return;
                case R.id.feedback_filter_radiobutton_error /*2131165623*/:
                    this.r = 3;
                    return;
                case R.id.feedback_filter_radiobutton_complaints /*2131165624*/:
                    this.r = 4;
                    return;
                default:
                    return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_open_feedback);
        this.s = getIntent().getStringExtra("appid");
        this.t = getIntent().getStringExtra("token");
        this.h = (MomoRatingBar) findViewById(R.id.feedback_ratingBar);
        this.h.setMinRating(1);
        this.i = (EditText) findViewById(R.id.feedback_edit);
        this.j = (TextView) findViewById(R.id.feedback_txt_score);
        this.l = (HeaderLayout) findViewById(R.id.layout_header);
        this.k = new bi(this);
        this.k.a((int) R.drawable.ic_topbar_confirm_white);
        this.k.setBackgroundResource(R.drawable.bg_header_submit);
        this.k.setMarginRight(8);
        this.m = (RadioButton) findViewById(R.id.feedback_filter_radiobutton_advice);
        this.n = (RadioButton) findViewById(R.id.feedback_filter_radiobutton_suggest);
        this.o = (RadioButton) findViewById(R.id.feedback_filter_radiobutton_error);
        this.p = (RadioButton) findViewById(R.id.feedback_filter_radiobutton_complaints);
        this.l.setTitleText((int) R.string.feedback_title);
        this.h.setOnRatingBarChangeListener(new u(this));
        this.l.a(this.k, new v(this));
        this.m.setOnCheckedChangeListener(this);
        this.n.setOnCheckedChangeListener(this);
        this.o.setOnCheckedChangeListener(this);
        this.p.setOnCheckedChangeListener(this);
    }
}
