package com.immomo.momo.android.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.immomo.a.a.f.b;
import com.immomo.momo.R;
import com.immomo.momo.android.view.PopupActionBar;
import com.immomo.momo.android.view.a;
import com.immomo.momo.android.view.a.n;
import com.immomo.momo.android.view.a.o;
import com.immomo.momo.android.view.bi;
import com.immomo.momo.android.view.draggablegridview.DraggableGridView;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.aj;
import com.immomo.momo.service.bean.al;
import com.immomo.momo.util.ao;
import com.immomo.momo.util.h;
import com.immomo.momo.util.j;
import com.immomo.momo.util.k;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class EditUserPhotoActivity extends ah {
    DraggableGridView h;
    List i = null;
    File j = null;
    PopupActionBar k = null;
    a l = null;
    boolean m = false;
    boolean n = false;
    private File o = null;
    private View p = null;

    private void a(String[] strArr) {
        View inflate = g.o().inflate((int) R.layout.include_editephoto_add, (ViewGroup) null);
        this.h.addView(inflate);
        this.p = inflate;
        inflate.setOnClickListener(new cn(this));
        if (strArr != null) {
            for (String c : strArr) {
                c(c);
            }
        }
    }

    static /* synthetic */ void b(EditUserPhotoActivity editUserPhotoActivity) {
        o oVar = new o(editUserPhotoActivity, (int) R.array.editprofile_add_photo);
        oVar.setTitle((int) R.string.dialog_title_add_photo);
        oVar.a(new ch(editUserPhotoActivity));
        editUserPhotoActivity.a(oVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup, int, boolean, boolean, int):void
     arg types: [com.immomo.momo.service.bean.al, android.widget.ImageView, ?[OBJECT, ARRAY], int, int, int, int]
     candidates:
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, int, android.view.View, android.view.ViewGroup, boolean, int, boolean):com.immomo.momo.android.c.o
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup, int, boolean, boolean, int):void */
    private void c(String str) {
        ImageView imageView = new ImageView(getApplicationContext());
        j.a((aj) new al(str), imageView, (ViewGroup) null, 3, false, true, 5);
        this.h.addView(imageView, this.h.getChildCount() - 1);
        this.i.add(str);
        u();
    }

    private boolean u() {
        if (this.i.size() >= (this.f.b() ? 16 : 8)) {
            this.p.setVisibility(4);
            return true;
        }
        this.p.setVisibility(0);
        return false;
    }

    /* access modifiers changed from: private */
    public boolean v() {
        if (this.f.k() != this.i.size()) {
            return true;
        }
        if (this.f.ae == null) {
            return false;
        }
        for (int i2 = 0; i2 < this.f.k(); i2++) {
            if (!this.f.ae[i2].equals(this.i.get(i2))) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        super.a(bundle);
        setContentView((int) R.layout.activity_edit_userphoto);
        this.h = (DraggableGridView) findViewById(R.id.editphoto_gridview);
        this.k = (PopupActionBar) findViewById(R.id.editphoto_actionbar);
        PopupActionBar popupActionBar = this.k;
        a a2 = new a(getApplicationContext()).a();
        this.l = a2;
        popupActionBar.a(a2, null);
        bi a3 = new bi(getApplicationContext()).a((int) R.drawable.ic_topbar_confirm_white);
        a3.setBackgroundResource(R.drawable.bg_header_submit);
        a3.setMarginRight(8);
        a(a3, new cm(this));
        this.h.setOnRearrangeListener(new cg(this));
        this.h.setOnItemClickListener(new ci(this));
        this.h.setCanDragListener(new cj(this));
        this.h.setOnItemDragListener(new ck(this));
        this.h.setOnTouchListener(new cl(this));
        String[] b = (bundle == null || !bundle.containsKey("editphotos")) ? this.f.ae : android.support.v4.b.a.b(bundle.get("editphotos").toString(), ",");
        this.i = new ArrayList();
        a(b);
    }

    public final void c(int i2) {
        if (this.i.size() <= 1) {
            a((CharSequence) "无法继续删除,至少保存一张图片做为头像");
            return;
        }
        this.i.remove(i2);
        this.h.removeViewAt(i2);
        u();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.h.a(java.lang.String, android.graphics.Bitmap, int, boolean):java.io.File
     arg types: [java.lang.String, android.graphics.Bitmap, int, int]
     candidates:
      com.immomo.momo.util.h.a(java.lang.String, java.lang.String, int, boolean):void
      com.immomo.momo.util.h.a(java.lang.String, android.graphics.Bitmap, int, boolean):java.io.File */
    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        switch (i2) {
            case 11:
                if (i3 == -1 && intent != null) {
                    Uri data = intent.getData();
                    Intent intent2 = new Intent(getApplicationContext(), ImageFactoryActivity.class);
                    intent2.setData(data);
                    intent2.putExtra("aspectY", 1);
                    intent2.putExtra("aspectX", 1);
                    intent2.putExtra("minsize", 320);
                    this.o = new File(com.immomo.momo.a.g(), String.valueOf(b.a()) + ".jpg_");
                    intent2.putExtra("outputFilePath", this.o.getAbsolutePath());
                    startActivityForResult(intent2, 13);
                    return;
                }
                return;
            case 12:
                if (i3 == -1 && this.j != null && this.j.exists()) {
                    Uri fromFile = Uri.fromFile(this.j);
                    this.o = new File(com.immomo.momo.a.g(), this.j.getName());
                    Intent intent3 = new Intent(getApplicationContext(), ImageFactoryActivity.class);
                    intent3.setData(fromFile);
                    intent3.putExtra("aspectY", 1);
                    intent3.putExtra("aspectX", 1);
                    intent3.putExtra("minsize", 320);
                    intent3.putExtra("outputFilePath", this.o.getAbsolutePath());
                    startActivityForResult(intent3, 13);
                    return;
                }
                return;
            case 13:
                if (i3 == -1 && intent != null) {
                    if (this.j != null) {
                        if (this.j.exists()) {
                            this.j.delete();
                        }
                        this.j = null;
                    }
                    if (this.o != null) {
                        String a2 = b.a();
                        Bitmap l2 = android.support.v4.b.a.l(this.o.getPath());
                        if (l2 != null) {
                            this.e.a((Object) ("save file=" + h.a(a2, l2, 2, true)));
                            l2.recycle();
                            c(a2);
                        } else {
                            ao.b("发生未知错误，图片添加失败");
                        }
                        this.o = null;
                        return;
                    }
                    return;
                } else if (i3 == 1003) {
                    ao.d(R.string.cropimage_error_size);
                    return;
                } else if (i3 == 1000) {
                    ao.d(R.string.cropimage_error_other);
                    return;
                } else if (i3 == 1002) {
                    ao.d(R.string.cropimage_error_store);
                    return;
                } else if (i3 == 1001) {
                    ao.d(R.string.cropimage_error_filenotfound);
                    return;
                } else {
                    return;
                }
            default:
                return;
        }
    }

    public void onBackPressed() {
        if (v()) {
            n.a(this, "您的照片信息有修改，是否保存", "保存", "不保存", new co(this), new cp(this)).show();
        } else {
            super.onBackPressed();
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        new k("PO", "P911").e();
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Bundle bundle) {
        super.onRestoreInstanceState(bundle);
        if (bundle.containsKey("camera")) {
            this.j = new File(bundle.getString("camera"));
        }
        if (bundle.containsKey("newavator")) {
            this.o = new File(bundle.getString("newavator"));
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        new k("PI", "P911").e();
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putString("editphotos", android.support.v4.b.a.a(this.i, ","));
        if (this.j != null) {
            bundle.putString("camera", this.j.getPath());
        }
        if (this.o != null) {
            bundle.putString("newavator", this.o.getPath());
        }
    }
}
