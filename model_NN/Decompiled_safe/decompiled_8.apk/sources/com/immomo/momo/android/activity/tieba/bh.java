package com.immomo.momo.android.activity.tieba;

import com.immomo.momo.android.c.i;
import com.immomo.momo.android.c.u;
import com.immomo.momo.android.view.an;
import com.immomo.momo.plugin.b.a;
import com.immomo.momo.service.bean.q;
import java.io.File;

final class bh implements an {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ PublishTieCommentActivity f2184a;

    bh(PublishTieCommentActivity publishTieCommentActivity) {
        this.f2184a = publishTieCommentActivity;
    }

    public final void a(CharSequence charSequence, int i) {
        this.f2184a.e.b((Object) ("+++++++++++++++++++++ onEmoteSelected:" + ((Object) charSequence) + "  type:" + i));
        if (i == 2) {
            PublishTieCommentActivity publishTieCommentActivity = this.f2184a;
            PublishTieCommentActivity publishTieCommentActivity2 = this.f2184a;
            publishTieCommentActivity.C = new a(charSequence.toString());
            this.f2184a.B = null;
            File a2 = q.a(this.f2184a.C.g(), this.f2184a.C.h());
            if (a2 == null || !a2.exists()) {
                u.b().execute(new i(this.f2184a.C.g(), this.f2184a.C.h(), new bi(this), (byte) 0));
                return;
            }
            this.f2184a.m.setImageBitmap(android.support.v4.b.a.l(a2.getAbsolutePath()));
        }
    }
}
