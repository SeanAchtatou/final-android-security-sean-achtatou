package com.immomo.momo.android.activity;

import android.content.Context;
import android.content.Intent;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.w;
import com.immomo.momo.service.ai;
import com.immomo.momo.service.aq;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.service.y;
import java.util.List;

final class j extends d {

    /* renamed from: a  reason: collision with root package name */
    private v f1755a = null;
    private /* synthetic */ AddFriendActivity c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public j(AddFriendActivity addFriendActivity, Context context) {
        super(context);
        this.c = addFriendActivity;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        this.c.h = new bf(((String[]) objArr)[0]);
        w.a().a(this.c.h, (List) null);
        return "yes";
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.f1755a = new v(this.c);
        this.f1755a.setCancelable(true);
        this.f1755a.a("正在查找,请稍候...");
        this.f1755a.setOnCancelListener(new k(this));
        this.c.a(this.f1755a);
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        super.a(exc);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        if (((String) obj).equals("yes")) {
            new y().d(this.c.h.h);
            new aq().b(this.c.h);
            if (this.c.h.ah != null) {
                new ai().a(this.c.h.ah);
            }
            Intent intent = new Intent(com.immomo.momo.android.broadcast.w.f2366a);
            intent.putExtra("momoid", this.c.h.h);
            this.c.sendBroadcast(intent);
            Intent intent2 = new Intent(this.c, OtherProfileActivity.class);
            intent2.putExtra("momoid", this.c.h.h);
            intent2.putExtra("tag", "notreflsh");
            this.c.startActivity(intent2);
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.c.p();
    }
}
