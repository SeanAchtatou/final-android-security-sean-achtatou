package com.immomo.momo.android.activity.account;

import android.content.Context;
import android.graphics.Bitmap;
import com.immomo.momo.android.c.d;
import com.immomo.momo.protocol.a.w;

final class cb extends d {

    /* renamed from: a  reason: collision with root package name */
    private StringBuilder f982a = new StringBuilder();
    private /* synthetic */ bn c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public cb(bn bnVar, Context context) {
        super(context);
        this.c = bnVar;
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ Object a(Object... objArr) {
        w.a();
        return w.a(this.f982a);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        if (this.c.i != null) {
            this.c.i.cancel(true);
            this.c.i = (cb) null;
        }
        this.c.i = this;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        this.c.m.setImageBitmap((Bitmap) obj);
        this.c.j = true;
        this.c.k = this.f982a.toString();
        this.b.a((Object) ("cookie:" + this.c.k));
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.c.i = (cb) null;
    }
}
