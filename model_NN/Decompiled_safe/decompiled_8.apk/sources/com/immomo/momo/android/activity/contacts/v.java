package com.immomo.momo.android.activity.contacts;

import android.content.Context;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.a.a;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.n;
import com.immomo.momo.protocol.a.i;
import com.immomo.momo.protocol.a.w;
import com.immomo.momo.service.bean.e;
import com.immomo.momo.service.bean.f;

final class v extends d {

    /* renamed from: a  reason: collision with root package name */
    private com.immomo.momo.android.view.a.v f1199a = null;
    private int c = 0;
    private int d = 0;
    /* access modifiers changed from: private */
    public /* synthetic */ CommunityPeopleActivity e;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public v(CommunityPeopleActivity communityPeopleActivity, Context context, int i, int i2) {
        super(context);
        this.e = communityPeopleActivity;
        if (communityPeopleActivity.p != null) {
            communityPeopleActivity.p.cancel(true);
        }
        communityPeopleActivity.p = this;
        this.c = i;
        this.d = i2;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        switch (this.e.i) {
            case 1:
                if (this.e.j == 11) {
                    w a2 = w.a();
                    String str = this.e.f.h;
                    return a2.c(((e) ((f) this.e.m.get(this.c)).b.get(this.d)).c, 1, (String) null);
                } else if (this.e.j == 22) {
                    i a3 = i.a();
                    String str2 = this.e.f.h;
                    return a3.j(((e) ((f) this.e.m.get(this.c)).b.get(this.d)).c);
                }
                break;
            case 2:
                if (this.e.j == 11) {
                    w a4 = w.a();
                    String str3 = this.e.f.h;
                    return a4.b(((e) ((f) this.e.m.get(this.c)).b.get(this.d)).c, 1, PoiTypeDef.All);
                } else if (this.e.j == 22) {
                    i a5 = i.a();
                    String str4 = this.e.f.h;
                    return a5.i(((e) ((f) this.e.m.get(this.c)).b.get(this.d)).c);
                }
                break;
            case 3:
                if (this.e.j == 11) {
                    w a6 = w.a();
                    String str5 = this.e.f.h;
                    return a6.a(((e) ((f) this.e.m.get(this.c)).b.get(this.d)).c, 1, PoiTypeDef.All);
                } else if (this.e.j == 22) {
                    i a7 = i.a();
                    String str6 = this.e.f.h;
                    return a7.h(((e) ((f) this.e.m.get(this.c)).b.get(this.d)).c);
                }
                break;
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
        this.f1199a = new com.immomo.momo.android.view.a.v(this.e);
        this.f1199a.a("请求提交中...");
        this.f1199a.setCancelable(true);
        this.f1199a.setOnCancelListener(new w(this));
        this.e.a(this.f1199a);
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        this.e.p();
        if ((exc instanceof a) && ((a) exc).f670a == 407 && this.e.i == 1) {
            n a2 = n.a(this.e, (int) R.string.invite_sinarebind_into, (int) R.string.invite_sinarebind_btn, (int) R.string.dialog_btn_cancel, new x(this), new y(this));
            a2.setTitle((int) R.string.invite_sinarebind_title);
            this.e.a(a2);
            return;
        }
        super.a(exc);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        String str = (String) obj;
        if (!android.support.v4.b.a.a((CharSequence) str)) {
            this.e.b((CharSequence) str);
        }
        this.e.k.a(this.c, this.d);
        this.e.k.notifyDataSetChanged();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        super.b();
        this.e.p();
    }
}
