package com.immomo.momo.android.b;

import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import mm.purchasesdk.PurchaseCode;

final class y implements LocationListener {

    /* renamed from: a  reason: collision with root package name */
    private p f2342a;
    private /* synthetic */ x b;

    public y(x xVar, p pVar) {
        this.b = xVar;
        this.f2342a = pVar;
    }

    public final void onLocationChanged(Location location) {
        if (location != null) {
            this.b.c.a((Object) ("google receive a location: " + location.getLatitude() + "," + location.getLongitude() + "," + location.getAccuracy() + "provider: " + location.getProvider()));
            this.f2342a.a(location, 0, 100, PurchaseCode.LOADCHANNEL_ERR);
        }
    }

    public final void onProviderDisabled(String str) {
    }

    public final void onProviderEnabled(String str) {
    }

    public final void onStatusChanged(String str, int i, Bundle bundle) {
    }
}
