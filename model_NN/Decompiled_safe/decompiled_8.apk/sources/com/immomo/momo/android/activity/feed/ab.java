package com.immomo.momo.android.activity.feed;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.protocol.a.k;
import java.util.Collection;
import java.util.Date;

final class ab extends d {

    /* renamed from: a  reason: collision with root package name */
    private boolean f1434a = true;
    private /* synthetic */ y c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ab(y yVar, Context context, boolean z) {
        super(context);
        this.c = yVar;
        this.f1434a = z;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        if (this.f1434a) {
            com.immomo.momo.protocol.a.a.d b = k.a().b();
            this.c.O = 1;
            this.c.R.b(b.b);
            return b;
        }
        com.immomo.momo.protocol.a.a.d a2 = k.a().a(this.c.O * 20, y.e(this.c));
        y yVar = this.c;
        yVar.O = yVar.O + 1;
        this.c.R.a(a2.b);
        return a2;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        com.immomo.momo.protocol.a.a.d dVar = (com.immomo.momo.protocol.a.a.d) obj;
        if (!this.f1434a) {
            this.c.Q.b((Collection) dVar.b);
        } else {
            this.c.Q.a((Collection) dVar.b);
            ((MainFeedActivity) this.c.c()).y();
            this.c.P = new Date();
            this.c.N.a("prf_time_frients_feed", this.c.P);
            this.c.S.setLastFlushTime(this.c.P);
        }
        this.c.T.setVisibility(dVar.f2883a > 0 ? 0 : 8);
    }

    /* access modifiers changed from: protected */
    public final void b() {
        if (this.f1434a) {
            this.c.S.n();
            this.c.U = null;
            return;
        }
        this.c.T.e();
        this.c.V = null;
    }
}
