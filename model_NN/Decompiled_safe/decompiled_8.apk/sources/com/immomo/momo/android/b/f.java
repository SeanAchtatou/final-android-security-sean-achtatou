package com.immomo.momo.android.b;

import android.location.Location;
import java.util.List;

final class f extends p {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ a f2325a;
    private final /* synthetic */ List c;
    private final /* synthetic */ String d;
    private final /* synthetic */ Object e;

    f(a aVar, List list, String str, Object obj) {
        this.f2325a = aVar;
        this.c = list;
        this.d = str;
        this.e = obj;
    }

    public final void a(Location location, int i, int i2, int i3) {
        this.c.add(location);
        if (this.c.size() > 0) {
            try {
                this.f2325a.b.a(this.d);
            } catch (Exception e2) {
                this.f2325a.f2312a.a((Throwable) e2);
            }
            synchronized (this.e) {
                this.e.notify();
            }
        }
    }
}
