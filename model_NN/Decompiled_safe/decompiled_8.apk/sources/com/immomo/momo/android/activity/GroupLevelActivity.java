package com.immomo.momo.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.android.broadcast.d;
import com.immomo.momo.android.broadcast.u;
import com.immomo.momo.android.pay.MemberCenterActivity;
import com.immomo.momo.android.view.MomoProgressbar;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.a.a;
import com.immomo.momo.service.y;

public class GroupLevelActivity extends ah implements View.OnClickListener {
    private static String h = ("http://m.immomo.com/inc/android/grouplevel_v4.html?v=" + g.z());
    private WebView i = null;
    /* access modifiers changed from: private */
    public View j = null;
    private MomoProgressbar k;
    private TextView l;
    private Button m;
    private Button n;
    /* access modifiers changed from: private */
    public y o;
    private View p;
    private ImageView q;
    /* access modifiers changed from: private */
    public a r;
    private String s;
    private boolean t;
    private boolean u;
    private u v;
    /* access modifiers changed from: private */
    public eu w;
    private d x = new eq(this);

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0089  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00fd  */
    /* JADX WARNING: Removed duplicated region for block: B:34:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0067  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void u() {
        /*
            r8 = this;
            r7 = 2130837877(0x7f020175, float:1.728072E38)
            r6 = 1
            r1 = 0
            com.immomo.momo.service.bean.a.a r0 = r8.r
            boolean r0 = r0.a()
            if (r0 == 0) goto L_0x00e0
            com.immomo.momo.service.bean.a.a r0 = r8.r
            int r0 = r0.A
            switch(r0) {
                case 0: goto L_0x00cc;
                case 1: goto L_0x00d1;
                case 2: goto L_0x00d6;
                case 3: goto L_0x00db;
                default: goto L_0x0014;
            }
        L_0x0014:
            r0 = r1
        L_0x0015:
            android.widget.ImageView r2 = r8.q
            r2.setImageResource(r0)
            com.immomo.momo.android.view.MomoProgressbar r0 = r8.k
            com.immomo.momo.service.bean.a.a r2 = r8.r
            long r2 = r2.v
            int r2 = (int) r2
            long r2 = (long) r2
            r0.setMax(r2)
            com.immomo.momo.android.view.MomoProgressbar r0 = r8.k
            com.immomo.momo.service.bean.a.a r2 = r8.r
            long r2 = r2.w
            int r2 = (int) r2
            long r2 = (long) r2
            r0.setProgress(r2)
            r0 = 2131493294(0x7f0c01ae, float:1.8610064E38)
            java.lang.String r0 = com.immomo.momo.g.a(r0)
            com.immomo.momo.android.view.MomoProgressbar r2 = r8.k
            r3 = 2
            java.lang.Object[] r3 = new java.lang.Object[r3]
            com.immomo.momo.service.bean.a.a r4 = r8.r
            long r4 = r4.w
            java.lang.String r4 = java.lang.String.valueOf(r4)
            r3[r1] = r4
            com.immomo.momo.service.bean.a.a r4 = r8.r
            long r4 = r4.v
            java.lang.String r4 = java.lang.String.valueOf(r4)
            r3[r6] = r4
            java.lang.String r0 = java.lang.String.format(r0, r3)
            r2.setProgressText(r0)
            r0 = 2131493295(0x7f0c01af, float:1.8610066E38)
            com.immomo.momo.g.a(r0)
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            com.immomo.momo.service.bean.a.a r0 = r8.r
            boolean r0 = r0.a()
            if (r0 == 0) goto L_0x00fd
            java.lang.String r0 = "会员群 LV"
        L_0x0069:
            java.lang.String r0 = java.lang.String.valueOf(r0)
            r2.<init>(r0)
            com.immomo.momo.service.bean.a.a r0 = r8.r
            int r0 = r0.A
            java.lang.String r0 = java.lang.String.valueOf(r0)
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            android.widget.TextView r2 = r8.l
            r2.setText(r0)
            boolean r0 = r8.t
            if (r0 == 0) goto L_0x00cb
            android.view.View r0 = r8.p
            r0.setVisibility(r1)
            boolean r0 = r8.u
            if (r0 == 0) goto L_0x00cb
            android.widget.Button r0 = r8.m
            r0.setVisibility(r1)
            com.immomo.momo.service.bean.bf r0 = r8.f
            boolean r0 = r0.b()
            if (r0 != 0) goto L_0x00a9
            r0 = 2131165480(0x7f070128, float:1.7945178E38)
            android.view.View r0 = r8.findViewById(r0)
            r0.setVisibility(r1)
        L_0x00a9:
            com.immomo.momo.service.bean.a.a r0 = r8.r
            boolean r0 = r0.r
            if (r0 == 0) goto L_0x0101
            android.widget.Button r0 = r8.m
            java.lang.String r2 = "已达到最高等级"
            r0.setText(r2)
            android.widget.Button r0 = r8.m
            r0.setBackgroundResource(r1)
            android.widget.Button r0 = r8.m
            android.content.res.Resources r1 = r8.getResources()
            r2 = 2131230766(0x7f08002e, float:1.8077594E38)
            int r1 = r1.getColor(r2)
            r0.setTextColor(r1)
        L_0x00cb:
            return
        L_0x00cc:
            r0 = 2130838083(0x7f020243, float:1.7281138E38)
            goto L_0x0015
        L_0x00d1:
            r0 = 2130838084(0x7f020244, float:1.728114E38)
            goto L_0x0015
        L_0x00d6:
            r0 = 2130838085(0x7f020245, float:1.7281142E38)
            goto L_0x0015
        L_0x00db:
            r0 = 2130838086(0x7f020246, float:1.7281144E38)
            goto L_0x0015
        L_0x00e0:
            com.immomo.momo.service.bean.a.a r0 = r8.r
            int r0 = r0.A
            switch(r0) {
                case 0: goto L_0x00e9;
                case 1: goto L_0x00ee;
                case 2: goto L_0x00f3;
                case 3: goto L_0x00f8;
                default: goto L_0x00e7;
            }
        L_0x00e7:
            goto L_0x0014
        L_0x00e9:
            r0 = 2130838060(0x7f02022c, float:1.7281092E38)
            goto L_0x0015
        L_0x00ee:
            r0 = 2130838061(0x7f02022d, float:1.7281094E38)
            goto L_0x0015
        L_0x00f3:
            r0 = 2130838062(0x7f02022e, float:1.7281096E38)
            goto L_0x0015
        L_0x00f8:
            r0 = 2130838063(0x7f02022f, float:1.7281098E38)
            goto L_0x0015
        L_0x00fd:
            java.lang.String r0 = "普通群 LV"
            goto L_0x0069
        L_0x0101:
            com.immomo.momo.service.bean.a.a r0 = r8.r
            boolean r0 = r0.x
            if (r0 == 0) goto L_0x0135
            r0 = 2131493296(0x7f0c01b0, float:1.8610068E38)
            java.lang.String r0 = com.immomo.momo.g.a(r0)
            android.widget.TextView r2 = r8.l
            java.lang.Object[] r3 = new java.lang.Object[r6]
            com.immomo.momo.service.bean.a.a r4 = r8.r
            int r4 = r4.A
            java.lang.String r4 = java.lang.String.valueOf(r4)
            r3[r1] = r4
            java.lang.String r0 = java.lang.String.format(r0, r3)
            r2.setText(r0)
            android.widget.Button r0 = r8.m
            r0.setEnabled(r6)
            android.widget.Button r0 = r8.m
            r0.setBackgroundResource(r7)
            android.widget.Button r0 = r8.m
            java.lang.String r1 = "立刻升级"
            r0.setText(r1)
            goto L_0x00cb
        L_0x0135:
            android.widget.Button r0 = r8.m
            r0.setEnabled(r1)
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r1 = "暂无法升级（还差"
            r0.<init>(r1)
            com.immomo.momo.service.bean.a.a r1 = r8.r
            long r1 = r1.v
            int r1 = (int) r1
            long r1 = (long) r1
            com.immomo.momo.service.bean.a.a r3 = r8.r
            long r3 = r3.w
            long r1 = r1 - r3
            java.lang.String r1 = java.lang.String.valueOf(r1)
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = "天）"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            android.widget.Button r1 = r8.m
            r1.setText(r0)
            android.widget.Button r0 = r8.m
            r0.setBackgroundResource(r7)
            goto L_0x00cb
        */
        throw new UnsupportedOperationException("Method not decompiled: com.immomo.momo.android.activity.GroupLevelActivity.u():void");
    }

    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        super.a(bundle);
        setContentView((int) R.layout.activity_grouplevel);
        this.i = (WebView) findViewById(R.id.webview);
        this.q = (ImageView) findViewById(R.id.image_level);
        this.i.getSettings().setJavaScriptEnabled(true);
        this.k = (MomoProgressbar) findViewById(R.id.grouplevel_progress);
        this.l = (TextView) findViewById(R.id.grouplevel_levelinfo_text);
        this.m = (Button) findViewById(R.id.grouplevel_updata_btn);
        this.n = (Button) findViewById(R.id.btn_gotovip);
        this.j = findViewById(R.id.loading_indicator);
        this.p = findViewById(R.id.layout_progresss);
        this.v = new u(this);
        this.v.a(this.x);
        this.m.setOnClickListener(this);
        this.n.setOnClickListener(this);
        this.i.setWebChromeClient(new es());
        this.i.setWebViewClient(new et(this));
        d();
        u();
        this.i.loadUrl(h);
        setTitle((int) R.string.group_level_title);
        findViewById(R.id.layout_header_container).setVisibility(0);
        WebSettings settings = this.i.getSettings();
        settings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        settings.setCacheMode(2);
        this.i.postDelayed(new er(this), 10);
    }

    /* access modifiers changed from: protected */
    public final void d() {
        this.s = getIntent().getStringExtra("gid");
        if (!android.support.v4.b.a.a((CharSequence) this.s)) {
            this.o = new y();
            this.r = this.o.e(this.s);
            this.t = this.o.c(this.f.h, this.s);
            if (this.t) {
                this.u = this.f.h.equals(this.r.g);
            }
        }
    }

    public void onClick(View view) {
        if (view.equals(this.m)) {
            b(new eu(this, this));
        } else if (view.equals(this.n)) {
            startActivity(new Intent(this, MemberCenterActivity.class));
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.v != null) {
            unregisterReceiver(this.v);
            this.v = null;
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }
}
