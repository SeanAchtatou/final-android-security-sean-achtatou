package com.immomo.momo.android.activity.tieba;

import android.support.v4.b.a;
import android.text.Editable;
import android.text.TextWatcher;

final class fe implements TextWatcher {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ TiebaSearchActivity f2282a;

    fe(TiebaSearchActivity tiebaSearchActivity) {
        this.f2282a = tiebaSearchActivity;
    }

    public final void afterTextChanged(Editable editable) {
    }

    public final void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public final void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        this.f2282a.m.setVisibility(a.a(charSequence.toString()) ? 8 : 0);
    }
}
