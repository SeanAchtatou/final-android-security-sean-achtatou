package com.immomo.momo.android.game;

import android.content.DialogInterface;

final class b implements DialogInterface.OnCancelListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ a f2420a;

    b(a aVar) {
        this.f2420a = aVar;
    }

    public final void onCancel(DialogInterface dialogInterface) {
        this.f2420a.cancel(true);
        this.f2420a.f2397a.setResult(0);
        this.f2420a.f2397a.finish();
    }
}
