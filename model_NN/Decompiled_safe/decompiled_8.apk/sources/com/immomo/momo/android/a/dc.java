package com.immomo.momo.android.a;

import android.view.View;

final class dc implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ da f769a;
    private final /* synthetic */ int b;

    dc(da daVar, int i) {
        this.f769a = daVar;
        this.b = i;
    }

    public final void onClick(View view) {
        if (this.f769a.e.getOnItemClickListener() != null) {
            this.f769a.e.getOnItemClickListener().onItemClick(this.f769a.e, view, this.b, (long) view.getId());
        }
    }
}
