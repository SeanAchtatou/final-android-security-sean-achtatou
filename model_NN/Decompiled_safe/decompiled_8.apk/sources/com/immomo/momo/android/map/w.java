package com.immomo.momo.android.map;

final class w implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ GeoGoogleMapActivity f2517a;

    w(GeoGoogleMapActivity geoGoogleMapActivity) {
        this.f2517a = geoGoogleMapActivity;
    }

    public final void run() {
        if (!this.f2517a.isFinishing() && this.f2517a.f2450a != null) {
            this.f2517a.f2450a.dismiss();
            this.f2517a.f2450a = null;
        }
    }
}
