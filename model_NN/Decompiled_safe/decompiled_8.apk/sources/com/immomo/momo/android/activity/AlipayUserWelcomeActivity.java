package com.immomo.momo.android.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.b.a;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.account.LoginActivity;
import com.immomo.momo.android.activity.account.RegisterActivityWithP;
import com.immomo.momo.android.activity.maintab.MaintabActivity;
import com.immomo.momo.android.broadcast.b;
import com.immomo.momo.android.broadcast.d;
import com.immomo.momo.android.broadcast.l;
import com.immomo.momo.android.view.EmoteEditeText;
import com.immomo.momo.android.view.HandyListView;
import com.immomo.momo.android.view.a.n;
import com.immomo.momo.android.view.bi;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.aj;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.util.j;

public class AlipayUserWelcomeActivity extends ao implements View.OnClickListener, d {
    /* access modifiers changed from: private */
    public String h;
    /* access modifiers changed from: private */
    public String i;
    /* access modifiers changed from: private */
    public String j;
    /* access modifiers changed from: private */
    public String k;
    /* access modifiers changed from: private */
    public String l;
    private b m = null;
    private View n;
    private View o;
    private View p;
    private TextView q;
    private TextView r;
    private Button s;
    private Button t;
    private Button u;
    private ImageView v;
    /* access modifiers changed from: private */
    public boolean w = false;
    /* access modifiers changed from: private */
    public Handler x = new Handler();
    private DialogInterface.OnCancelListener y = new o(this);

    static /* synthetic */ void a(AlipayUserWelcomeActivity alipayUserWelcomeActivity, com.immomo.momo.service.bean.b bVar) {
        String str;
        alipayUserWelcomeActivity.p.setVisibility(0);
        TextView textView = alipayUserWelcomeActivity.q;
        if (bVar != null) {
            if (!a.a((CharSequence) bVar.c)) {
                str = bVar.c;
            } else if (!a.a((CharSequence) bVar.f3003a)) {
                str = bVar.f3003a;
            }
            textView.setText(str);
        }
        str = alipayUserWelcomeActivity.h;
        textView.setText(str);
    }

    static /* synthetic */ void a(AlipayUserWelcomeActivity alipayUserWelcomeActivity, bf bfVar) {
        bf bfVar2 = new bf();
        bfVar2.W = bfVar.W;
        bfVar2.h = bfVar.h;
        alipayUserWelcomeActivity.b(new ae(alipayUserWelcomeActivity, alipayUserWelcomeActivity, bfVar2));
    }

    static /* synthetic */ void b(AlipayUserWelcomeActivity alipayUserWelcomeActivity, boolean z) {
        alipayUserWelcomeActivity.o.setVisibility(0);
        alipayUserWelcomeActivity.x.postDelayed(new r(alipayUserWelcomeActivity, z), 1500);
    }

    static /* synthetic */ void d(AlipayUserWelcomeActivity alipayUserWelcomeActivity) {
        Intent intent = new Intent(alipayUserWelcomeActivity.getApplicationContext(), MaintabActivity.class);
        intent.setFlags(335544320);
        alipayUserWelcomeActivity.getApplicationContext().startActivity(intent);
        alipayUserWelcomeActivity.finish();
    }

    static /* synthetic */ void e(AlipayUserWelcomeActivity alipayUserWelcomeActivity) {
        n nVar = new n(alipayUserWelcomeActivity);
        nVar.setTitle("提示");
        nVar.a();
        nVar.a(g.a((int) R.string.alipaybind_not_match_des));
        nVar.a(0, "确认", new s(alipayUserWelcomeActivity));
        nVar.a(1, "取消", new v(alipayUserWelcomeActivity));
        nVar.setOnCancelListener(alipayUserWelcomeActivity.y);
        nVar.show();
    }

    /* access modifiers changed from: private */
    public void f() {
        View inflate = g.o().inflate((int) R.layout.dialog_bindalipay_pwd, (ViewGroup) null);
        EmoteEditeText emoteEditeText = (EmoteEditeText) inflate.findViewById(R.id.edittext_pwd);
        emoteEditeText.requestFocus();
        n nVar = new n(this);
        nVar.setTitle("绑定支付宝");
        nVar.setContentView(inflate);
        nVar.a();
        nVar.a(0, getString(R.string.dialog_btn_confim), new w(this, emoteEditeText));
        nVar.a(1, getString(R.string.dialog_btn_cancel), new x(this));
        nVar.setOnCancelListener(this.y);
        nVar.show();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, com.immomo.momo.android.view.HandyListView):void
     arg types: [com.immomo.momo.service.bean.bf, android.widget.ImageView, ?[OBJECT, ARRAY]]
     candidates:
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, int):void
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup):void
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, com.immomo.momo.android.view.HandyListView):void */
    static /* synthetic */ void f(AlipayUserWelcomeActivity alipayUserWelcomeActivity) {
        alipayUserWelcomeActivity.n.setVisibility(0);
        bf bfVar = alipayUserWelcomeActivity.f;
        alipayUserWelcomeActivity.r.setText(String.valueOf(bfVar.i) + " (" + bfVar.h + ")");
        j.a((aj) bfVar, alipayUserWelcomeActivity.v, (HandyListView) null);
    }

    /* access modifiers changed from: private */
    public void g() {
        Intent intent = new Intent(this, LoginActivity.class);
        intent.putExtra("alipay_user_id", this.h);
        intent.putExtra("alipay_user_phonenumber", this.i);
        startActivity(intent);
        overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
    }

    /* access modifiers changed from: private */
    public void i() {
        if (!this.w) {
            a("该支付宝手机号已经注册过陌陌", 1);
            return;
        }
        Intent intent = new Intent(this, RegisterActivityWithP.class);
        intent.putExtra("alipay_user_id", this.h);
        intent.putExtra("alipay_user_phonenumber", this.i);
        intent.putExtra("alipay_user_gender", this.j);
        startActivity(intent);
        overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
    }

    public final void a(Intent intent) {
        if (intent.getAction().equals(l.f2355a)) {
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public final void e() {
        super.e();
        if (this.f != null) {
            b(new ac(this, this, true, this.f.h));
        } else {
            b(new ac(this, this, false, PoiTypeDef.All));
        }
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_login /*2131165287*/:
                g();
                return;
            case R.id.btn_register /*2131165288*/:
                i();
                return;
            case R.id.btn_bind /*2131165394*/:
                f();
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_alipayuserwelcome);
        this.h = getIntent().getStringExtra("alipay_user_id");
        this.k = getIntent().getStringExtra("alipay_user_authcode");
        this.l = getIntent().getStringExtra("alipay_user_appid");
        this.m = new l(this);
        this.m.a(this);
        this.s = (Button) findViewById(R.id.btn_login);
        this.t = (Button) findViewById(R.id.btn_register);
        this.u = (Button) findViewById(R.id.btn_bind);
        m().a(new bi(this).a("帮助"), new q(this));
        m().setTitleText("支付宝钱包登录");
        this.n = findViewById(R.id.layout_bind);
        this.o = findViewById(R.id.layout_register);
        this.p = findViewById(R.id.layout_aliaccount);
        this.v = (ImageView) findViewById(R.id.iv_avatar);
        this.q = (TextView) findViewById(R.id.tv_name);
        this.r = (TextView) findViewById(R.id.tv_momo);
        this.s.setOnClickListener(this);
        this.t.setOnClickListener(this);
        this.u.setOnClickListener(this);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.m != null) {
            unregisterReceiver(this.m);
            this.m = null;
        }
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        n nVar = new n(this);
        nVar.a();
        nVar.setTitle("提示");
        nVar.a("重复打开，请返回重试");
        nVar.a(0, (int) R.string.dialog_btn_confim, new p(this));
        nVar.show();
    }
}
