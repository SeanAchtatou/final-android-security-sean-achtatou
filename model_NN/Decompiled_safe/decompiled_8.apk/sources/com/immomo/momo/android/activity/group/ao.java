package com.immomo.momo.android.activity.group;

import android.content.DialogInterface;
import java.util.List;

final class ao implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ an f1539a;
    private final /* synthetic */ List b;

    ao(an anVar, List list) {
        this.f1539a = anVar;
        this.b = list;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f1539a.f1538a.W.a().removeAll(this.b);
        this.f1539a.f1538a.W.notifyDataSetChanged();
        this.f1539a.f1538a.S.b(this.b);
        this.f1539a.f1538a.aa.e();
    }
}
