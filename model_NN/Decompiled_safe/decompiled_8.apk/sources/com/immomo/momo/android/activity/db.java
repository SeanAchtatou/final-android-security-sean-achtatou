package com.immomo.momo.android.activity;

import android.view.View;

final class db implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ EditUserProfileActivity f1216a;

    db(EditUserProfileActivity editUserProfileActivity) {
        this.f1216a = editUserProfileActivity;
    }

    public final void onClick(View view) {
        this.f1216a.v();
        if (this.f1216a.G) {
            EditUserProfileActivity.o(this.f1216a);
            return;
        }
        this.f1216a.setResult(-1);
        this.f1216a.finish();
    }
}
