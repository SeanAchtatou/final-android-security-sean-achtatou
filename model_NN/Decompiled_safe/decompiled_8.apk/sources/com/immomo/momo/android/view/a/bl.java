package com.immomo.momo.android.view.a;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.immomo.momo.R;

final class bl extends BaseAdapter {

    /* renamed from: a  reason: collision with root package name */
    private LayoutInflater f2691a;
    private /* synthetic */ bj b;

    public bl(bj bjVar, Context context) {
        this.b = bjVar;
        this.f2691a = LayoutInflater.from(context);
    }

    public final int getCount() {
        return (this.b.f - this.b.e) + 1;
    }

    public final Object getItem(int i) {
        return Integer.valueOf(i);
    }

    public final long getItemId(int i) {
        return (long) i;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = this.f2691a.inflate((int) R.layout.listitem_tiebapager, (ViewGroup) null);
        }
        TextView textView = (TextView) view.findViewById(R.id.tiebapager_tv);
        textView.setVisibility(0);
        textView.setText(new StringBuilder(String.valueOf(i + 1)).toString());
        return view;
    }
}
