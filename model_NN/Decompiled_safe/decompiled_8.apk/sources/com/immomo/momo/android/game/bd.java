package com.immomo.momo.android.game;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import com.immomo.momo.android.activity.OtherProfileActivity;
import com.immomo.momo.service.bean.bf;

final class bd implements AdapterView.OnItemClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ NearbyPlayersActivity f2424a;

    bd(NearbyPlayersActivity nearbyPlayersActivity) {
        this.f2424a = nearbyPlayersActivity;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        Intent intent = new Intent(this.f2424a.getApplicationContext(), OtherProfileActivity.class);
        intent.putExtra("momoid", ((bf) this.f2424a.j.getItem(i)).h);
        intent.putExtra("tag", "local");
        this.f2424a.startActivity(intent);
    }
}
