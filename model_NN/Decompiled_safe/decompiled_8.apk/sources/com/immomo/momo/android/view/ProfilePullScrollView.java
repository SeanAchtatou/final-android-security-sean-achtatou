package com.immomo.momo.android.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ScrollView;
import com.handmark.pulltorefresh.library.a;
import com.handmark.pulltorefresh.library.a.b;
import com.handmark.pulltorefresh.library.c;
import com.handmark.pulltorefresh.library.e;
import com.immomo.momo.R;
import com.immomo.momo.android.plugin.cropimage.q;
import com.immomo.momo.util.m;

public class ProfilePullScrollView extends a {
    private static int e = 700;
    private m d = new m(this);
    private cp f = null;
    private int g = 0;

    public ProfilePullScrollView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ View a(Context context, AttributeSet attributeSet) {
        co coVar = new co(context, attributeSet);
        coVar.setId(R.id.scrollview);
        coVar.setVerticalScrollBarEnabled(false);
        return coVar;
    }

    /* access modifiers changed from: protected */
    public final b a(Context context, c cVar, TypedArray typedArray) {
        return super.a(context, cVar, typedArray);
    }

    /* access modifiers changed from: protected */
    public final boolean b() {
        return ((ScrollView) this.b).getScrollY() == 0;
    }

    /* access modifiers changed from: protected */
    public final boolean c() {
        View childAt = ((ScrollView) this.b).getChildAt(0);
        if (childAt != null) {
            return ((ScrollView) this.b).getScrollY() >= childAt.getHeight() - getHeight();
        }
        return false;
    }

    public final void d() {
        ((ScrollView) getRefreshableView()).smoothScrollTo(0, 0);
        a();
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
    }

    /* access modifiers changed from: protected */
    public void onScrollChanged(int i, int i2, int i3, int i4) {
        super.onScrollChanged(i, i2, i3, i4);
        if (i2 <= (-getHeight()) && this.c != null) {
            q qVar = this.c;
            a(e.RESET, new boolean[0]);
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case 1:
            case 3:
                if (this.f642a) {
                    this.f642a = false;
                    if (getState() == e.RELEASE_TO_REFRESH) {
                        this.d.a((Object) ("height=" + (-getHeight())));
                        a(-getHeight(), (long) e);
                        return true;
                    }
                    a(e.RESET, new boolean[0]);
                    return true;
                }
                break;
        }
        return super.onTouchEvent(motionEvent);
    }

    /* access modifiers changed from: protected */
    public void setHeaderScroll(float f2) {
        if (Math.abs(f2) <= ((float) this.g)) {
            super.setHeaderScroll(f2);
            if (this.f != null) {
                this.f.a(Math.round(f2));
            }
        }
    }

    public void setMaxScroll(int i) {
        this.g = i;
    }

    public void setOnPullScrollChangedListener(cp cpVar) {
        this.f = cpVar;
    }
}
