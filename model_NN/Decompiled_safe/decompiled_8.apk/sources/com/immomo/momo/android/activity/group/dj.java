package com.immomo.momo.android.activity.group;

import android.content.Intent;
import android.support.v4.b.a;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.common.InviteSNSActivity;
import com.immomo.momo.android.view.a.al;
import com.immomo.momo.android.view.a.n;
import com.immomo.momo.service.bean.a.i;
import com.immomo.momo.util.k;

final class dj implements al {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ GroupProfileActivity f1606a;
    private final /* synthetic */ String[] b;

    dj(GroupProfileActivity groupProfileActivity, String[] strArr) {
        this.f1606a = groupProfileActivity;
        this.b = strArr;
    }

    public final void a(int i) {
        boolean z = true;
        boolean z2 = false;
        if (!this.b[i].equals(this.f1606a.getString(R.string.gprofile_owner_op0)) || this.f1606a.r.G == 4 || this.f1606a.r.G == 5) {
            if (this.b[i].equals(this.f1606a.getString(R.string.f312gprofile_owner_op11)) || this.b[i].equals(this.f1606a.getString(R.string.f313gprofile_owner_op12))) {
                this.f1606a.b(new dt(this.f1606a, this.f1606a));
            } else if (this.b[i].equals(this.f1606a.getString(R.string.f317gprofile_member_op02)) || this.b[i].equals(this.f1606a.getString(R.string.f316gprofile_member_op01))) {
                if (this.f1606a.Y != null) {
                    i r = this.f1606a.Y;
                    i r2 = this.f1606a.Y;
                    if (!this.f1606a.Y.f2978a) {
                        z2 = true;
                    }
                    r2.f2978a = z2;
                    r.a("group_ispush", Boolean.valueOf(z2));
                }
            } else if (this.b[i].equals(this.f1606a.getString(R.string.gprofile_owner_op3))) {
                GroupProfileActivity.h(this.f1606a);
            } else if (this.b[i].equals(this.f1606a.getString(R.string.gprofile_owner_op4))) {
                GroupProfileActivity.s(this.f1606a);
            } else if (this.b[i].equals(this.f1606a.getString(R.string.gprofile_member_op3))) {
                Intent intent = new Intent(this.f1606a, InviteSNSActivity.class);
                intent.putExtra("invite_id", this.f1606a.l);
                intent.putExtra("invite_type", 2);
                this.f1606a.startActivity(intent);
            } else if (this.b[i].equals(this.f1606a.getString(R.string.gprofile_member_op2))) {
                n.a(this.f1606a, this.f1606a.getString(R.string.group_setting_quit_tip), new cx(this.f1606a)).show();
            }
        } else if (!a.a((CharSequence) this.f1606a.r.g) && this.f1606a.r.g.equals(this.f1606a.f.h) && this.f1606a.r.G != 4 && this.f1606a.r.G != 5) {
            Intent intent2 = new Intent(this.f1606a, EditGroupProfileActivity.class);
            intent2.putExtra("gid", this.f1606a.l);
            if (this.f1606a.r.G != 2) {
                z = false;
            }
            intent2.putExtra("key_group_is_pass", z);
            this.f1606a.startActivity(intent2);
            new k("C", "C6401").e();
        }
    }
}
