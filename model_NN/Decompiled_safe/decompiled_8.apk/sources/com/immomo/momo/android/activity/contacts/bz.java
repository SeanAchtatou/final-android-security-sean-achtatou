package com.immomo.momo.android.activity.contacts;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.protocol.a.h;
import java.util.List;

final class bz extends d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ bk f1180a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public bz(bk bkVar, Context context) {
        super(context);
        this.f1180a = bkVar;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        List b = h.a().b();
        bk.a(this.f1180a, b.size());
        return b;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        List list = (List) obj;
        super.a(list);
        if (list != null) {
            this.f1180a.R = list;
            this.f1180a.P.b(list);
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
    }
}
