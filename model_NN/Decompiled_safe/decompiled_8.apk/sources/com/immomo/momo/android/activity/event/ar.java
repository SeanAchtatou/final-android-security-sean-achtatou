package com.immomo.momo.android.activity.event;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.j;

final class ar extends d {

    /* renamed from: a  reason: collision with root package name */
    private v f1361a = null;
    private /* synthetic */ EventProfileActivity c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ar(EventProfileActivity eventProfileActivity, Context context) {
        super(context);
        this.c = eventProfileActivity;
        this.f1361a = new v(context);
        this.f1361a.setCancelable(true);
        this.f1361a.setOnCancelListener(new as(this));
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        String d = j.a().d(this.c.m);
        this.c.n.t = true;
        this.c.o.a(this.c.n);
        return d;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.c.a(this.f1361a);
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        super.a(exc);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        String str = (String) obj;
        super.a((Object) str);
        this.c.y();
        if (EventProfileActivity.j(this.c)) {
            EventProfileActivity.k(this.c);
        } else {
            a(str);
        }
        this.c.v();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        super.b();
        this.c.p();
    }
}
