package com.immomo.momo.android.a;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.service.bean.a.a;
import com.immomo.momo.util.j;
import com.immomo.momo.util.m;
import java.util.List;

public final class hr extends a {
    private Context d = null;
    private List e = null;
    private AbsListView f = null;

    public hr(Context context, List list, AbsListView absListView) {
        super(context, list);
        new hs();
        this.d = context;
        this.e = list;
        this.f = absListView;
        new m("GroupListAdapter").a();
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            ht htVar = new ht((byte) 0);
            view = LayoutInflater.from(this.d).inflate((int) R.layout.listitem_group_site, (ViewGroup) null);
            htVar.f864a = (ImageView) view.findViewById(R.id.userlist_item_iv_face);
            htVar.b = (TextView) view.findViewById(R.id.userlist_item_tv_name);
            htVar.c = (TextView) view.findViewById(R.id.userlist_item_tv_sign);
            htVar.e = (ImageView) view.findViewById(R.id.userlist_item_pic_iv_party);
            htVar.d = (TextView) view.findViewById(R.id.userlist_item_tv_count);
            view.findViewById(R.id.userlist_item_layout_membercount_backgroud);
            view.findViewById(R.id.userlist_item_iv_person_icon);
            htVar.f = (ImageView) view.findViewById(R.id.iv_level_icon);
            htVar.g = (ImageView) view.findViewById(R.id.userlist_item_tv_new);
            view.setTag(R.id.tag_userlist_item, htVar);
        }
        a aVar = (a) this.e.get(i);
        ht htVar2 = (ht) view.getTag(R.id.tag_userlist_item);
        if (!android.support.v4.b.a.a((CharSequence) aVar.c)) {
            htVar2.b.setText(aVar.c);
        } else {
            htVar2.b.setText(aVar.b);
        }
        if (aVar.h != null) {
            htVar2.c.setText(aVar.h);
        } else {
            htVar2.c.setText(PoiTypeDef.All);
        }
        htVar2.e.setVisibility(aVar.C ? 0 : 8);
        int a2 = a.a(aVar.A, aVar.a());
        if (a2 != -1) {
            htVar2.f.setVisibility(0);
            htVar2.f.setImageResource(a2);
        } else {
            htVar2.f.setVisibility(8);
        }
        if (aVar.c()) {
            htVar2.g.setVisibility(0);
        } else {
            htVar2.g.setVisibility(8);
        }
        htVar2.d.setText(String.valueOf(aVar.k) + "/" + aVar.j);
        j.a(aVar, htVar2.f864a, this.f, 3);
        return view;
    }
}
