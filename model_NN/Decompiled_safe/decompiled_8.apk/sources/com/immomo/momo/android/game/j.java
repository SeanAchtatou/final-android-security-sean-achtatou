package com.immomo.momo.android.game;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.m;

final class j extends d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ g f2435a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public j(g gVar, Context context) {
        super(context);
        this.f2435a = gVar;
    }

    /* access modifiers changed from: protected */
    public final Object a(Object... objArr) {
        m.a().a(this.f2435a.P, this.f2435a.Q, this.f2435a.O, this.f2435a.R);
        this.f2435a.R.i = true;
        return null;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.f2435a.a(new v(this.f2435a.c(), "正在获取支付信息...", this));
    }

    /* access modifiers changed from: protected */
    public final void a(Object obj) {
        this.f2435a.O();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.f2435a.N();
    }
}
