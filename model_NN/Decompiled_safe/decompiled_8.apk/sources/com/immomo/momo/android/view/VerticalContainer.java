package com.immomo.momo.android.view;

import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.immomo.momo.R;
import com.immomo.momo.util.m;
import java.util.HashMap;

class VerticalContainer extends LinearLayout {

    /* renamed from: a  reason: collision with root package name */
    private HashMap f2660a = new HashMap();
    /* access modifiers changed from: private */
    public HashMap b = new HashMap();
    private int c = 0;
    private int d = 0;
    private m e = new m("VerticalContainer");
    private int f = 0;

    public VerticalContainer(Context context) {
        super(context);
    }

    public VerticalContainer(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public VerticalContainer(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public void addView(View view, int i, ViewGroup.LayoutParams layoutParams) {
        super.addView(view, i, layoutParams);
    }

    public int getChildCount() {
        return super.getChildCount();
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        super.onFinishInflate();
        setWillNotDraw(false);
        setOrientation(1);
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int i5;
        int i6 = i3 - i;
        int childCount = super.getChildCount();
        int i7 = 0;
        int i8 = 0;
        while (i7 < childCount) {
            View childAt = getChildAt(i7);
            if (childAt.getVisibility() != 8) {
                float animatedFraction = this.b.containsKey(childAt) ? 1.0f - ((ValueAnimator) this.b.get(childAt)).getAnimatedFraction() : this.f2660a.containsKey(childAt) ? 1.0f - ((ValueAnimator) this.f2660a.get(childAt)).getAnimatedFraction() : 1.0f;
                int intValue = (int) (((float) ((Integer) childAt.getTag(R.id.tag_height)).intValue()) * (animatedFraction > 1.0f ? 1.0f : animatedFraction));
                childAt.layout(0, i8, i6, i8 + intValue);
                i5 = intValue + i8;
            } else {
                i5 = i8;
            }
            i7++;
            i8 = i5;
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int childCount = super.getChildCount();
        this.c = 0;
        for (int i3 = 0; i3 < childCount; i3++) {
            View childAt = getChildAt(i3);
            if (childAt.getVisibility() != 8) {
                childAt.measure(i, i2);
                int measuredHeight = childAt.getMeasuredHeight();
                childAt.setTag(R.id.tag_height, Integer.valueOf(measuredHeight));
                if (!this.b.containsKey(childAt)) {
                    LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) childAt.getLayoutParams();
                    this.c = layoutParams.bottomMargin + measuredHeight + this.c + layoutParams.topMargin;
                }
            }
        }
        if (this.f != this.c) {
            this.f = this.c;
            this.e.a((Object) "oldHeight != height");
            if (isShown()) {
                ObjectAnimator.ofInt(this, "forcedHeight", this.c).setDuration(250L).start();
            } else {
                int i4 = this.c;
                if (i4 != this.d) {
                    this.d = i4;
                    requestLayout();
                }
            }
        }
        setMeasuredDimension(getDefaultSize(getSuggestedMinimumWidth(), i), resolveSize(this.d, i2));
    }

    public void removeView(View view) {
        ValueAnimator ofFloat;
        float alpha = view.getAlpha();
        if (((double) alpha) > 0.1d) {
            ofFloat = ObjectAnimator.ofFloat(view, "alpha", alpha, 0.0f);
        } else {
            if (alpha > 0.0f) {
                view.setAlpha(0.0f);
            }
            ofFloat = ValueAnimator.ofFloat(0.0f, 1.0f);
        }
        ofFloat.setDuration(250L);
        ofFloat.addListener(new dv(this, view));
        ofFloat.start();
        this.b.put(view, ofFloat);
        requestLayout();
    }
}
