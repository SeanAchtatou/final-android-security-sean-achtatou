package com.immomo.momo.android.activity.group;

import android.content.Context;
import android.os.Handler;
import android.widget.ListAdapter;
import com.immomo.momo.android.a.hr;
import com.immomo.momo.android.c.d;
import com.immomo.momo.service.bean.a.a;
import java.util.List;

final class fi extends d {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public /* synthetic */ SiteGroupsActivity f1657a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public fi(SiteGroupsActivity siteGroupsActivity, Context context) {
        super(context);
        this.f1657a = siteGroupsActivity;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        return this.f1657a.j.l(this.f1657a.h);
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        List<a> list = (List) obj;
        if (list != null) {
            this.f1657a.l = new hr(this.f1657a.getApplicationContext(), list, this.f1657a.k);
            this.f1657a.k.setAdapter((ListAdapter) this.f1657a.l);
            for (a aVar : list) {
                this.f1657a.i.put(aVar.b, aVar);
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
        new Handler().post(new fj(this));
    }
}
