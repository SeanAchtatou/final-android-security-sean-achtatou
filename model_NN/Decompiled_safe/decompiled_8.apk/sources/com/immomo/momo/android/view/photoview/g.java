package com.immomo.momo.android.view.photoview;

import android.content.Context;
import android.widget.Scroller;

final class g extends e {

    /* renamed from: a  reason: collision with root package name */
    private Scroller f2822a;

    public g(Context context) {
        this.f2822a = new Scroller(context);
    }

    public final void a(int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
        this.f2822a.fling(i, i2, i3, i4, i5, i6, i7, i8);
    }

    public final boolean a() {
        return this.f2822a.computeScrollOffset();
    }

    public final void b() {
        this.f2822a.forceFinished(true);
    }

    public final int c() {
        return this.f2822a.getCurrX();
    }

    public final int d() {
        return this.f2822a.getCurrY();
    }
}
