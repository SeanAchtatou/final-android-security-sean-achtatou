package com.immomo.momo.android.activity.message;

import com.immomo.momo.plugin.audio.j;

final class f implements j {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ a f1981a;

    f(a aVar) {
        this.f1981a = aVar;
    }

    public final void a() {
        this.f1981a.w.a(this.f1981a.m);
        this.f1981a.s.setVisibility(0);
        this.f1981a.I = System.currentTimeMillis();
        this.f1981a.j.sendEmptyMessage(10021);
    }

    public final void b() {
        a.e(this.f1981a);
    }

    public final void c() {
        if (this.f1981a.J != null && this.f1981a.J.exists()) {
            this.f1981a.J.delete();
        }
    }

    public final void d() {
        this.f1981a.j.post(new g(this));
    }
}
