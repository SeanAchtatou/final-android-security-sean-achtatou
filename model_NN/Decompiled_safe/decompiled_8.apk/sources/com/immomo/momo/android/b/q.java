package com.immomo.momo.android.b;

import android.content.Context;
import android.location.Location;
import android.support.v4.app.x;
import com.immomo.a.a.f.b;
import com.immomo.momo.R;
import com.immomo.momo.a.w;
import com.immomo.momo.g;
import com.immomo.momo.util.m;
import java.util.ArrayList;

public final class q extends x {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public x f2335a;
    /* access modifiers changed from: private */
    public m b = new m("GoogleClient");

    public q(Context context) {
        new r(this);
        this.f2335a = x.a(context);
    }

    /* access modifiers changed from: private */
    public void b(String str, p pVar, boolean z) {
        this.b.a((Object) "google smart locating....");
        if (!g.y()) {
            g.a((int) R.string.errormsg_network_unfind);
            throw new w();
        }
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        String str2 = String.valueOf(str) + "_gps";
        String str3 = String.valueOf(str) + "_network";
        Object obj = new Object();
        s sVar = new s(this, arrayList, str3, str2, obj);
        this.f2335a.a(str2, new t(this, arrayList2, str3, str2, obj));
        this.f2335a.b(str3, sVar);
        new Thread(new u(this, obj, str3, str2, arrayList, arrayList2, pVar, z)).start();
    }

    public final void a() {
        this.b.b((Object) "Google client cancel all listener");
        if (this.f2335a != null) {
            this.f2335a.b();
        }
    }

    public final void a(p pVar) {
        String str = String.valueOf(g.h()) + b.a();
        if (!g.y()) {
            g.a((int) R.string.errormsg_network_unfind);
            throw new w();
        } else {
            b(str, pVar, false);
        }
    }

    public final void a(String str) {
        if (this.f2335a != null) {
            this.f2335a.a(String.valueOf(str) + "_gps");
            this.f2335a.a(String.valueOf(str) + "_network");
        }
    }

    public final void a(String str, p pVar, boolean z) {
        this.b.a((Object) "google smart locating....");
        if (!g.y()) {
            g.a((int) R.string.errormsg_network_unfind);
            throw new w();
        }
        ArrayList arrayList = new ArrayList();
        String str2 = String.valueOf(str) + "_gps";
        Object obj = new Object();
        this.f2335a.a(str2, new v(this, arrayList, str2, obj));
        new Thread(new w(this, obj, str2, arrayList, pVar, z)).start();
    }

    public final Location b() {
        return this.f2335a.c();
    }

    public final boolean c() {
        return this.f2335a.a();
    }
}
