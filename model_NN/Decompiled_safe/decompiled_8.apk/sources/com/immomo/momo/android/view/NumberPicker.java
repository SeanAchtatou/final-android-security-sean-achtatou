package com.immomo.momo.android.view;

import android.content.Context;
import android.os.Handler;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import mm.purchasesdk.PurchaseCode;

public class NumberPicker extends LinearLayout implements TextWatcher, View.OnClickListener, View.OnFocusChangeListener, View.OnLongClickListener {
    /* access modifiers changed from: private */
    public static final char[] m = {'-', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};

    /* renamed from: a  reason: collision with root package name */
    protected int f2641a;
    protected int b;
    /* access modifiers changed from: private */
    public final Handler c;
    private final Runnable d;
    private final EditText e;
    /* access modifiers changed from: private */
    public final InputFilter f;
    private int g;
    private cm h;
    private cj i;
    /* access modifiers changed from: private */
    public long j;
    /* access modifiers changed from: private */
    public boolean k;
    /* access modifiers changed from: private */
    public boolean l;
    private NumberPickerButton n;
    private NumberPickerButton o;

    static {
        new cj();
    }

    public NumberPicker(Context context) {
        this(context, null);
    }

    public NumberPicker(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, com.immomo.momo.android.view.NumberPicker, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public NumberPicker(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet);
        this.d = new ci(this);
        this.j = 300;
        setOrientation(1);
        ((LayoutInflater) context.getSystemService("layout_inflater")).inflate((int) R.layout.common_number_picker, (ViewGroup) this, true);
        this.c = new Handler();
        ck ckVar = new ck(this, (byte) 0);
        this.f = new cl(this, (byte) 0);
        this.n = (NumberPickerButton) findViewById(R.id.increment);
        this.n.setOnClickListener(this);
        this.n.setOnLongClickListener(this);
        this.n.setNumberPicker(this);
        this.o = (NumberPickerButton) findViewById(R.id.decrement);
        this.o.setOnClickListener(this);
        this.o.setOnLongClickListener(this);
        this.o.setNumberPicker(this);
        this.e = (EditText) findViewById(R.id.timepicker_input);
        this.e.setOnFocusChangeListener(this);
        this.e.setFilters(new InputFilter[]{ckVar});
        this.e.setRawInputType(2);
        this.e.addTextChangedListener(this);
        if (!isEnabled()) {
            setEnabled(false);
        }
        this.f2641a = PurchaseCode.LOADCHANNEL_ERR;
    }

    private void a(View view) {
        String valueOf = String.valueOf(((TextView) view).getText());
        if (PoiTypeDef.All.equals(valueOf)) {
            g();
            return;
        }
        int parseInt = Integer.parseInt(valueOf.toString());
        if (parseInt >= 0 && parseInt <= this.f2641a && this.b != parseInt) {
            this.g = this.b;
            this.b = parseInt;
            f();
        }
        g();
    }

    static /* synthetic */ String[] d() {
        return null;
    }

    private void f() {
        if (this.h != null) {
            cm cmVar = this.h;
            int i2 = this.g;
            int i3 = this.b;
            cmVar.a();
        }
    }

    private void g() {
        String valueOf;
        EditText editText = this.e;
        int i2 = this.b;
        if (this.i != null) {
            cj cjVar = this.i;
            cjVar.c[0] = Integer.valueOf(i2);
            cjVar.f2762a.delete(0, cjVar.f2762a.length());
            cjVar.b.format("%02d", cjVar.c);
            valueOf = cjVar.b.toString();
        } else {
            valueOf = String.valueOf(i2);
        }
        editText.setText(valueOf);
        this.e.setSelection(this.e.getText().length());
    }

    public final void a() {
        this.f2641a = 23;
        this.b = 0;
        g();
    }

    /* access modifiers changed from: protected */
    public final void a(int i2) {
        if (i2 > this.f2641a) {
            i2 = 0;
        } else if (i2 < 0) {
            i2 = this.f2641a;
        }
        this.g = this.b;
        this.b = i2;
        f();
        g();
    }

    public void afterTextChanged(Editable editable) {
        int parseInt;
        if (!editable.toString().trim().equals(PoiTypeDef.All) && !editable.toString().trim().equals("-") && this.b != (parseInt = Integer.parseInt(editable.toString()))) {
            a(parseInt);
        }
    }

    public final void b() {
        this.k = false;
    }

    public void beforeTextChanged(CharSequence charSequence, int i2, int i3, int i4) {
    }

    public final void c() {
        this.l = false;
    }

    public int getCurrent() {
        return this.b;
    }

    public void onClick(View view) {
        a(this.e);
        if (!this.e.hasFocus()) {
            this.e.requestFocus();
        }
        if (R.id.increment == view.getId()) {
            a(this.b + 1);
        } else if (R.id.decrement == view.getId()) {
            a(this.b - 1);
        }
    }

    public void onFocusChange(View view, boolean z) {
        if (!z) {
            a(view);
        }
    }

    public boolean onLongClick(View view) {
        this.e.clearFocus();
        if (R.id.increment == view.getId()) {
            this.k = true;
            this.c.post(this.d);
        } else if (R.id.decrement == view.getId()) {
            this.l = true;
            this.c.post(this.d);
        }
        return true;
    }

    public void onTextChanged(CharSequence charSequence, int i2, int i3, int i4) {
    }

    public void setCurrent(int i2) {
        this.b = i2;
        g();
    }

    public void setEnabled(boolean z) {
        super.setEnabled(z);
        this.n.setEnabled(z);
        this.o.setEnabled(z);
        this.e.setEnabled(z);
    }

    public void setFormatter(cj cjVar) {
        this.i = cjVar;
    }

    public void setOnChangeListener(cm cmVar) {
        this.h = cmVar;
    }

    public void setSpeed(long j2) {
        this.j = j2;
    }
}
