package com.immomo.momo.android.a;

import android.content.Intent;
import android.view.View;
import com.immomo.momo.android.activity.OtherProfileActivity;
import com.immomo.momo.service.bean.ag;

final class dd implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ da f770a;
    private final /* synthetic */ ag b;

    dd(da daVar, ag agVar) {
        this.f770a = daVar;
        this.b = agVar;
    }

    public final void onClick(View view) {
        Intent intent = new Intent();
        intent.setClass(this.f770a.d, OtherProfileActivity.class);
        intent.putExtra("tag", "local");
        intent.putExtra("momoid", this.b.f());
        this.f770a.d.startActivity(intent);
    }
}
