package com.immomo.momo.android.activity.tieba;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.protocol.a.v;
import java.util.ArrayList;
import java.util.List;

final class fh extends d {

    /* renamed from: a  reason: collision with root package name */
    private List f2285a = new ArrayList();
    private /* synthetic */ TiebaVerifyActivity c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public fh(TiebaVerifyActivity tiebaVerifyActivity, Context context) {
        super(context);
        this.c = tiebaVerifyActivity;
        if (tiebaVerifyActivity.k != null) {
            tiebaVerifyActivity.k.cancel(true);
        }
        tiebaVerifyActivity.k = this;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        return Boolean.valueOf(v.a().a(this.c.f.h, this.c.i.getCount(), this.f2285a));
    }

    /* access modifiers changed from: protected */
    public final void a() {
        if (this.c.j != null) {
            cancel(true);
            this.c.l.e();
            this.c.k = (fh) null;
            return;
        }
        this.c.l.f();
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        Boolean bool = (Boolean) obj;
        for (com.immomo.momo.service.bean.c.d dVar : this.f2285a) {
            if (!this.c.m.contains(dVar)) {
                this.c.i.a(dVar);
                this.c.m.add(dVar);
            }
        }
        if (bool.booleanValue()) {
            this.c.l.setVisibility(0);
        } else {
            this.c.l.setVisibility(8);
        }
        this.c.i.notifyDataSetChanged();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.c.h.n();
        this.c.l.e();
        this.c.k = (fh) null;
    }
}
