package com.immomo.momo.android.activity.emotestore;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.b.a;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.OtherProfileActivity;
import com.immomo.momo.android.activity.ah;
import com.immomo.momo.android.activity.common.SelectSingleTabsActivity;
import com.immomo.momo.android.activity.d;
import com.immomo.momo.android.broadcast.g;
import com.immomo.momo.android.c.r;
import com.immomo.momo.android.c.u;
import com.immomo.momo.android.c.w;
import com.immomo.momo.android.view.MomoProgressbar;
import com.immomo.momo.android.view.a.n;
import com.immomo.momo.android.view.bi;
import com.immomo.momo.protocol.a.i;
import com.immomo.momo.service.aq;
import com.immomo.momo.service.bean.aj;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.service.bean.q;
import com.immomo.momo.service.o;
import com.immomo.momo.util.j;
import com.immomo.momo.util.k;
import org.json.JSONException;
import org.json.JSONObject;

public class EmotionProfileActivity extends ah implements View.OnClickListener {
    private TextView A;
    private TextView B;
    private TextView C;
    private View D;
    private View E;
    private View F;
    private View G;
    private Button H;
    private Button I;
    private Button J;
    private MomoProgressbar K = null;
    private g L = null;
    q h = null;
    o i = new o();
    String j = PoiTypeDef.All;
    boolean k = false;
    bf l = null;
    ImageView m;
    View n;
    Handler o = new i(this);
    private aq p = new aq();
    private String q = PoiTypeDef.All;
    private boolean r = true;
    private ImageView s;
    private ImageView t;
    private TextView u;
    private TextView v;
    private TextView w;
    private TextView x;
    private TextView y;
    private TextView z;

    static /* synthetic */ void d(EmotionProfileActivity emotionProfileActivity) {
        n a2 = n.a(emotionProfileActivity, "你的" + emotionProfileActivity.getString(R.string.gold_str) + "余额不足，现在去充值吗？", emotionProfileActivity.getString(R.string.dialog_btn_confim), emotionProfileActivity.getString(R.string.dialog_btn_cancel), new t(emotionProfileActivity), new j());
        a2.setTitle("付费提示");
        emotionProfileActivity.a(a2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup, int, boolean, boolean, int):void
     arg types: [com.immomo.momo.service.bean.al, android.widget.ImageView, ?[OBJECT, ARRAY], int, int, int, int]
     candidates:
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, int, android.view.View, android.view.ViewGroup, boolean, int, boolean):com.immomo.momo.android.c.o
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup, int, boolean, boolean, int):void */
    /* access modifiers changed from: private */
    public void u() {
        if (this.h.o != null) {
            this.D.setVisibility(0);
            this.A.setText(this.h.o.l());
            this.B.setText(this.h.o.h());
        } else {
            this.D.setVisibility(8);
        }
        this.w.setText(this.h.l);
        this.x.setText(this.h.m);
        if (this.h.i == 3 || this.h.i == 4) {
            this.x.getPaint().setFlags(16);
        }
        this.u.setText(this.h.b);
        j.b(this.h.a(), this.s, null, 18, true);
        j.b(this.h.c(), this.m, null, 18, true);
        j.a((aj) this.h.d(), this.t, (ViewGroup) null, 18, false, true, 0);
        v();
        if (this.h.i != 0) {
            this.y.setText(this.h.j);
            this.y.setVisibility(0);
        }
        if (this.h.i == 1) {
            this.y.setBackgroundResource(R.drawable.bg_eshop_profile_label1);
        } else if (this.h.i == 2) {
            this.y.setBackgroundResource(R.drawable.bg_eshop_profile_label2);
        } else if (this.h.i == 3) {
            this.y.setBackgroundResource(R.drawable.bg_eshop_profile_label3);
        } else if (this.h.i == 4) {
            this.y.setBackgroundResource(R.drawable.bg_eshop_profile_label4);
        } else {
            this.y.setVisibility(8);
        }
        if (this.h.c != 0) {
            this.v.setText(this.h.d);
            this.v.setVisibility(0);
        }
        if (this.h.c == 1) {
            this.v.setBackgroundResource(R.drawable.round_eshop_lable1);
        } else if (this.h.c == 2) {
            this.v.setBackgroundResource(R.drawable.round_eshop_lable2);
        } else if (this.h.c == 3) {
            this.v.setBackgroundResource(R.drawable.round_eshop_lable3);
        } else if (this.h.c == 4) {
            this.v.setBackgroundResource(R.drawable.round_eshop_lable4);
        } else {
            this.v.setVisibility(8);
        }
        if (!a.a((CharSequence) this.h.z)) {
            this.C.setText(this.h.z);
            if (!this.h.x || !this.h.y) {
                this.C.setCompoundDrawablesWithIntrinsicBounds((int) R.drawable.ic_common_notice, 0, 0, 0);
            }
            this.E.setVisibility(0);
        } else {
            this.E.setVisibility(8);
        }
        if (!a.a((CharSequence) this.h.s)) {
            this.z.setText(this.h.s);
        }
        if (!a.a((CharSequence) this.h.B)) {
            r rVar = new r(a.n(this.h.B), new k(this), 18, null);
            rVar.a(this.h.B);
            rVar.a();
        }
    }

    /* access modifiers changed from: private */
    public void v() {
        boolean z2 = false;
        i.a();
        w a2 = i.a(this.j);
        i.a();
        boolean z3 = !this.k && i.b(this.j);
        this.F.setVisibility(z3 ? 0 : 8);
        this.G.setVisibility(z3 ? 8 : 0);
        if (!z3) {
            if (this.k) {
                this.H.setVisibility(8);
            } else {
                this.H.setVisibility(0);
                this.H.setEnabled(!this.h.q || !this.h.t);
                if (!this.h.q || this.h.t) {
                    if (this.h.q) {
                        this.H.setText("已下载");
                    } else {
                        this.H.setText("购买");
                        this.H.setEnabled(this.h.x);
                    }
                } else if (this.h.w > 0) {
                    this.H.setText("下载(" + (this.h.w >= 102400 ? String.valueOf(a.b((double) ((((float) this.h.w) / 1024.0f) / 1024.0f))) + "MB" : String.valueOf(a.b((double) (((float) this.h.w) / 1024.0f))) + "KB") + ")");
                } else {
                    this.H.setText("下载");
                }
            }
            if (this.h.A != null) {
                Button button = this.I;
                if (!this.h.A.f) {
                    z2 = true;
                }
                button.setEnabled(z2);
                if (this.h.A.f3037a == 4) {
                    this.I.setText(com.immomo.momo.service.bean.a.a(this.h.A.e).f2969a == null ? PoiTypeDef.All : com.immomo.momo.service.bean.a.a(this.h.A.e).f2969a);
                } else {
                    this.I.setText(this.h.A.e);
                }
            } else {
                this.I.setText("赠送");
                this.I.setEnabled(this.h.y);
            }
            this.o.removeMessages(45);
            return;
        }
        this.K.setMax(a2.b);
        this.K.setProgress(a2.f2395a);
        if (!this.o.hasMessages(45)) {
            this.o.sendEmptyMessageDelayed(45, 500);
        }
    }

    private void w() {
        n a2 = n.a(this, "你将向好友" + this.l.h() + "赠送表情" + this.h.b + ", 消耗" + this.h.n, new q(this));
        a2.setTitle("付费提示");
        a(a2);
    }

    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        super.a(bundle);
        setContentView((int) R.layout.activity_emotestore_profile);
        this.j = getIntent().getStringExtra("eid");
        this.q = getIntent().getStringExtra("gremoveid");
        this.r = getIntent().getBooleanExtra("key_showemotionshop", true);
        if (a.a((CharSequence) this.j)) {
            finish();
            return;
        }
        if (!a.a((CharSequence) this.q)) {
            this.l = this.p.b(this.q);
            if (this.l == null) {
                this.l = new bf(this.q);
            }
            this.k = true;
        }
        this.D = findViewById(R.id.emotionprofile_layout_author);
        this.A = (TextView) this.D.findViewById(R.id.emotionprofile_tv_authordesc);
        this.t = (ImageView) this.D.findViewById(R.id.emotionprofile_iv_authoravator);
        this.B = (TextView) this.D.findViewById(R.id.emotionprofile_tv_authorname);
        this.v = (TextView) findViewById(R.id.emotionprofile_tv_nameflag);
        this.u = (TextView) findViewById(R.id.emotionprofile_tv_name);
        this.s = (ImageView) findViewById(R.id.emotionprofile_iv_cover);
        this.m = (ImageView) findViewById(R.id.emotionprofile_iv_thumb);
        this.y = (TextView) findViewById(R.id.emotionprofile_iv_lable);
        this.w = (TextView) findViewById(R.id.emotionprofile_tv_price_first);
        this.x = (TextView) findViewById(R.id.emotionprofile_tv_price_second);
        this.C = (TextView) findViewById(R.id.emotionprofile_tv_special_desc);
        this.z = (TextView) findViewById(R.id.emotionprofile_iv_desc);
        this.E = findViewById(R.id.emotionprofile_layout_special_desc);
        this.n = findViewById(R.id.emotionprofile_layout_emotioninfo);
        this.F = findViewById(R.id.emotionprofile_layout_progress);
        this.G = findViewById(R.id.emotionprofile_layout_btnbar);
        this.K = (MomoProgressbar) findViewById(R.id.emotionprofile_progress_download);
        this.K.setBackgroud(R.drawable.bg_progressbar2);
        this.K.setInnderDrawable(R.drawable.bg_progressbar2_inner);
        this.H = (Button) findViewById(R.id.emotionprofile_btn_buy);
        this.I = (Button) findViewById(R.id.emotionprofile_btn_gift);
        this.J = (Button) findViewById(R.id.emotionprofile_btn_pause);
        this.m.post(new o(this));
        this.t.setOnClickListener(this);
        this.H.setOnClickListener(this);
        this.I.setOnClickListener(this);
        this.J.setOnClickListener(this);
        if (this.r) {
            bi biVar = new bi(this);
            biVar.a("表情商店");
            a(biVar, new n(this));
        }
        d();
        this.L = new g(this, g.c, g.f2350a);
        this.L.a(new m(this));
    }

    /* access modifiers changed from: protected */
    public final void d() {
        this.h = this.i.b(this.j);
        if (this.h == null) {
            this.h = new q();
            this.h.f3035a = this.j;
            this.I.setEnabled(false);
            this.H.setEnabled(false);
            return;
        }
        q a2 = this.i.a(this.j);
        if (a2 != null) {
            this.h.t = a2.t;
            this.h.u = a2.u;
        }
        u();
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        if (i2 == 312 && i3 == -1) {
            String stringExtra = intent.getStringExtra("smomoid");
            if (!a.a((CharSequence) stringExtra)) {
                this.l = this.p.b(stringExtra);
                if (this.l == null) {
                    this.l = new bf(stringExtra);
                }
                w();
            }
        }
        super.onActivityResult(i2, i3, intent);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.emotionprofile_iv_authoravator /*2131165576*/:
                if (this.h.o != null && !a.a((CharSequence) this.h.o.h)) {
                    Intent intent = new Intent(getApplicationContext(), OtherProfileActivity.class);
                    intent.putExtra("momoid", this.h.o.h);
                    startActivity(intent);
                    return;
                }
                return;
            case R.id.emotionprofile_tv_authorname /*2131165577*/:
            case R.id.emotionprofile_tv_authordesc /*2131165578*/:
            case R.id.emotionprofile_layout_btnbar /*2131165579*/:
            case R.id.emotionprofile_layout_progress /*2131165582*/:
            case R.id.emotionprofile_progress_download /*2131165583*/:
            default:
                return;
            case R.id.emotionprofile_btn_buy /*2131165580*/:
                if (!this.h.q || this.h.t) {
                    n a2 = n.a(this, "你将购买表情" + this.h.b + ",消耗" + this.h.n, getString(R.string.dialog_btn_confim), getString(R.string.dialog_btn_cancel), new r(this), new s());
                    a2.setTitle("付费提示");
                    a(a2);
                    return;
                }
                u.b().execute(new p(this));
                this.o.sendEmptyMessageDelayed(45, 100);
                return;
            case R.id.emotionprofile_btn_gift /*2131165581*/:
                if (this.h.A != null) {
                    if (this.h.A.f3037a == 3) {
                        b(new w(this, this));
                        return;
                    } else if (this.h.A.f3037a == 1) {
                        Intent intent2 = new Intent(getApplicationContext(), EmotionInviteTaskActivity.class);
                        intent2.putExtra("eid", this.h.f3035a);
                        startActivity(intent2);
                        return;
                    } else if (this.h.A.f3037a == 2) {
                        Intent intent3 = new Intent(getApplicationContext(), EmotionSetTaskActivity.class);
                        intent3.putExtra("eid", this.h.f3035a);
                        startActivity(intent3);
                        return;
                    } else if (this.h.A.f3037a == 4) {
                        d.a(this.h.A.e, this);
                        return;
                    } else {
                        return;
                    }
                } else if (this.k) {
                    w();
                    return;
                } else {
                    Intent intent4 = new Intent(getApplicationContext(), SelectSingleTabsActivity.class);
                    intent4.putExtra("title", "选择赠送好友");
                    startActivityForResult(intent4, 312);
                    return;
                }
            case R.id.emotionprofile_btn_pause /*2131165584*/:
                i.a();
                i.c(this.j);
                v();
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.o.removeMessages(45);
        if (this.L != null) {
            unregisterReceiver(this.L);
            this.L = null;
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        try {
            new JSONObject().put("eid", this.j);
            new k("PO", "P949").e();
        } catch (JSONException e) {
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        b(new aa(this, this));
        try {
            new JSONObject().put("eid", this.j);
            new k("PI", "P949").e();
        } catch (JSONException e) {
        }
    }
}
