package com.immomo.momo.android.view;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;

public final class a extends LinearLayout {

    /* renamed from: a  reason: collision with root package name */
    private int f2662a = R.layout.common_actionbutton;
    private CharSequence b = PoiTypeDef.All;
    private View c = null;
    private View d = null;

    public a(Context context) {
        super(context);
        b();
    }

    public a(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        b();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, com.immomo.momo.android.view.a, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    private void b() {
        ((LayoutInflater) getContext().getSystemService("layout_inflater")).inflate(this.f2662a, (ViewGroup) this, true);
        this.d = findViewById(R.id.acionbutton_layout_container);
        this.c = findViewById(R.id.acionbutton_iv_icon);
        findViewById(R.id.acionbutton_tv_text);
    }

    public final a a() {
        this.c.setVisibility(0);
        this.c.setBackgroundResource(R.drawable.ic_bottombar_delete);
        return this;
    }

    public final Drawable getBackground() {
        return this.d.getBackground();
    }

    public final CharSequence getText() {
        return this.b;
    }

    public final void setBackgroundDrawable(Drawable drawable) {
        this.d.setBackgroundDrawable(drawable);
    }

    public final void setEnabled(boolean z) {
        this.d.setEnabled(z);
    }

    public final void setOnClickListener(View.OnClickListener onClickListener) {
        if (this.d != null) {
            this.d.setOnClickListener(onClickListener);
        }
    }

    public final void setSelected(boolean z) {
        this.d.setSelected(z);
    }
}
