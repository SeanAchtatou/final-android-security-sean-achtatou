package com.immomo.momo.android.a;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v4.b.a;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.OtherProfileActivity;
import com.immomo.momo.android.c.u;
import com.immomo.momo.android.view.AltImageView;
import com.immomo.momo.android.view.EmoteTextView;
import com.immomo.momo.android.view.HandyListView;
import com.immomo.momo.b;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.aa;
import com.immomo.momo.service.bean.ab;
import com.immomo.momo.service.bean.aj;
import com.immomo.momo.service.bean.q;
import com.immomo.momo.util.j;
import com.immomo.momo.util.m;
import java.io.File;
import java.lang.ref.SoftReference;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class bv extends a implements View.OnClickListener {
    private static Map f = new HashMap(24);
    private HandyListView d;
    private int e = 0;

    public bv(Context context, List list, HandyListView handyListView) {
        super(context, list);
        new m(this);
        this.d = handyListView;
        this.e = g.n();
    }

    private void a(ab abVar, ImageView imageView) {
        SoftReference softReference = (SoftReference) f.get(abVar.j);
        Bitmap bitmap = softReference == null ? null : (Bitmap) softReference.get();
        if (bitmap != null && bitmap.isRecycled()) {
            f.remove(bitmap);
            bitmap = null;
        }
        if (bitmap != null) {
            bitmap = (Bitmap) ((SoftReference) f.get(abVar.j)).get();
        } else if (!this.d.g()) {
            File a2 = q.a(abVar.j, abVar.k);
            if (a2.exists() && (bitmap = a.l(a2.getPath())) != null) {
                f.put(abVar.j, new SoftReference(bitmap));
            }
            if (bitmap == null && !abVar.isImageLoading()) {
                abVar.setImageLoading(true);
                u.b().execute(new bx(this, abVar));
            }
        }
        if (bitmap != null) {
            imageView.setImageBitmap(bitmap);
        } else {
            imageView.setImageDrawable(null);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.b.a(java.lang.String, boolean):android.graphics.Bitmap
     arg types: [java.lang.String, int]
     candidates:
      com.immomo.momo.b.a(java.lang.String, android.graphics.Bitmap):android.graphics.Bitmap
      com.immomo.momo.b.a(java.lang.String, boolean):android.graphics.Bitmap */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, com.immomo.momo.android.view.HandyListView):void
     arg types: [com.immomo.momo.service.bean.bf, android.widget.ImageView, com.immomo.momo.android.view.HandyListView]
     candidates:
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, int):void
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup):void
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, com.immomo.momo.android.view.HandyListView):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.b.a.a(java.util.Date, boolean):java.lang.String
     arg types: [java.util.Date, int]
     candidates:
      android.support.v4.b.a.a(android.graphics.Bitmap, float):android.graphics.Bitmap
      android.support.v4.b.a.a(android.graphics.Bitmap, java.io.File):android.graphics.Bitmap
      android.support.v4.b.a.a(int, int):java.lang.String
      android.support.v4.b.a.a(java.util.Collection, java.lang.String):java.lang.String
      android.support.v4.b.a.a(java.lang.Object[], java.lang.String):java.lang.String
      android.support.v4.b.a.a(java.lang.String, java.lang.String):org.json.JSONObject
      android.support.v4.b.a.a(float, int[]):void
      android.support.v4.b.a.a(android.app.Activity, int):void
      android.support.v4.b.a.a(android.view.View, java.lang.Runnable):void
      android.support.v4.b.a.a(java.io.File, java.io.File):void
      android.support.v4.b.a.a(java.io.File, java.util.zip.ZipInputStream):void
      android.support.v4.b.a.a(java.lang.Object, java.lang.StringBuilder):void
      android.support.v4.b.a.a(android.content.Context, java.lang.String):boolean
      android.support.v4.b.a.a(com.immomo.a.a.d.b, com.immomo.momo.service.bean.Message):boolean
      android.support.v4.b.a.a(byte[], java.lang.String):byte[]
      android.support.v4.b.a.a(byte[], byte[]):byte[]
      android.support.v4.b.a.a(int[], boolean):byte[]
      android.support.v4.b.a.a(byte[], boolean):int[]
      android.support.v4.b.a.a(int, float):void
      android.support.v4.b.a.a(java.lang.Object, com.immomo.a.a.f):void
      android.support.v4.view.bf.a(int, float):void
      com.immomo.a.a.f.a(java.lang.Object, com.immomo.a.a.f):void
      android.support.v4.b.a.a(java.util.Date, boolean):java.lang.String */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup, int, boolean, boolean, int):void
     arg types: [com.immomo.momo.service.bean.aa, com.immomo.momo.android.view.AltImageView, com.immomo.momo.android.view.HandyListView, int, int, int, int]
     candidates:
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, int, android.view.View, android.view.ViewGroup, boolean, int, boolean):com.immomo.momo.android.c.o
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup, int, boolean, boolean, int):void */
    public final View getView(int i, View view, ViewGroup viewGroup) {
        bz bzVar;
        if (view == null) {
            view = g.o().inflate((int) R.layout.listitem_eventfeed, (ViewGroup) null);
            bz bzVar2 = new bz((byte) 0);
            view.setTag(R.id.tag_userlist_item, bzVar2);
            bzVar2.f749a = (TextView) view.findViewById(R.id.tv_feed_time);
            bzVar2.b = (TextView) view.findViewById(R.id.tv_feed_site);
            bzVar2.c = view.findViewById(R.id.layout_feed_site);
            bzVar2.d = (EmoteTextView) view.findViewById(R.id.tv_feed_content);
            bzVar2.e = (AltImageView) view.findViewById(R.id.iv_feed_content);
            bzVar2.f = (ImageView) view.findViewById(R.id.iv_feed_photo);
            bzVar2.g = (TextView) view.findViewById(R.id.tv_feed_name);
            bzVar2.i = (TextView) view.findViewById(R.id.tv_feed_commentcount);
            bzVar2.j = view.findViewById(R.id.layout_feed_commentcount);
            bzVar2.k = view.findViewById(R.id.bt_feed_more);
            bzVar2.h = view.findViewById(R.id.layout_feed_content);
            bzVar2.w = view.findViewById(R.id.userlist_item_layout_badgeContainer);
            bzVar2.m = (ImageView) view.findViewById(R.id.userlist_item_iv_gender);
            bzVar2.u = view.findViewById(R.id.userlist_item_layout_genderbackgroud);
            bzVar2.n = (ImageView) view.findViewById(R.id.userlist_item_pic_iv_weibo);
            bzVar2.o = (ImageView) view.findViewById(R.id.userlist_item_pic_iv_tweibo);
            bzVar2.t = (ImageView) view.findViewById(R.id.userlist_item_pic_iv_group_role);
            bzVar2.p = (ImageView) view.findViewById(R.id.userlist_item_pic_iv_renren);
            bzVar2.q = (ImageView) view.findViewById(R.id.userlist_item_pic_iv_vip);
            bzVar2.r = (ImageView) view.findViewById(R.id.userlist_item_pic_iv_multipic);
            bzVar2.s = (ImageView) view.findViewById(R.id.userlist_item_pic_iv_industry);
            bzVar2.v = (TextView) view.findViewById(R.id.userlist_item_tv_age);
            bzVar2.l = view.findViewById(R.id.feed_layout_app);
            bzVar2.l.findViewById(R.id.feed_iv_appicon);
            bzVar2.l.findViewById(R.id.feed_tv_appdesc);
            bzVar2.l.findViewById(R.id.feed_tv_apptitle);
            view.findViewById(R.id.feed_view_app_border_bottom);
            view.findViewById(R.id.feed_view_app_border_top);
            bzVar2.x = (TextView) view.findViewById(R.id.tv_last_comment_time);
            bzVar = bzVar2;
        } else {
            bzVar = (bz) view.getTag(R.id.tag_userlist_item);
        }
        aa aaVar = (aa) getItem(i);
        bzVar.f.setTag(R.id.tag_item_position, Integer.valueOf(i));
        bzVar.g.setTag(R.id.tag_item_position, Integer.valueOf(i));
        bzVar.w.setTag(R.id.tag_item_position, Integer.valueOf(i));
        bzVar.e.setTag(R.id.tag_item_position, Integer.valueOf(i));
        bzVar.h.setTag(R.id.tag_item_position, Integer.valueOf(i));
        bzVar.j.setTag(R.id.tag_item_position, Integer.valueOf(i));
        bzVar.k.setTag(R.id.tag_item_position, Integer.valueOf(i));
        bzVar.b.setTag(R.id.tag_item_position, Integer.valueOf(i));
        bzVar.c.setTag(R.id.tag_item_position, Integer.valueOf(i));
        bzVar.l.setTag(R.id.tag_item_position, Integer.valueOf(i));
        bzVar.x.setTag(R.id.tag_item_position, Integer.valueOf(i));
        bzVar.f.setOnClickListener(this);
        bzVar.g.setOnClickListener(this);
        bzVar.w.setOnClickListener(this);
        bzVar.e.setOnClickListener(new bw(this));
        if (aaVar.b != null) {
            bzVar.g.setText(aaVar.b.h());
            if (aaVar.b.b()) {
                bzVar.g.setTextColor(g.c((int) R.color.font_vip_name));
            } else {
                bzVar.g.setTextColor(g.c((int) R.color.font_value));
            }
            bzVar.w.setVisibility(0);
            bzVar.v.setText(new StringBuilder(String.valueOf(aaVar.b.I)).toString());
            if ("F".equals(aaVar.b.H)) {
                bzVar.u.setBackgroundResource(R.drawable.bg_gender_famal);
                bzVar.m.setImageResource(R.drawable.ic_user_famale);
            } else {
                bzVar.u.setBackgroundResource(R.drawable.bg_gender_male);
                bzVar.m.setImageResource(R.drawable.ic_user_male);
            }
            if (aaVar.b.j()) {
                bzVar.r.setVisibility(0);
            } else {
                bzVar.r.setVisibility(8);
            }
            if (!com.immomo.a.a.f.a.a(aaVar.b.M)) {
                bzVar.s.setVisibility(0);
                bzVar.s.setImageBitmap(b.a(aaVar.b.M, true));
            } else {
                bzVar.s.setVisibility(8);
            }
            if (aaVar.b.an) {
                bzVar.n.setVisibility(0);
                bzVar.n.setImageResource(aaVar.b.ap ? R.drawable.ic_userinfo_weibov : R.drawable.ic_userinfo_weibo);
            } else {
                bzVar.n.setVisibility(8);
            }
            if (aaVar.b.at) {
                bzVar.o.setVisibility(0);
                bzVar.o.setImageResource(aaVar.b.au ? R.drawable.ic_userinfo_tweibov : R.drawable.ic_userinfo_tweibo);
            } else {
                bzVar.o.setVisibility(8);
            }
            if (aaVar.b.aJ == 1 || aaVar.b.aJ == 3) {
                bzVar.t.setVisibility(0);
                bzVar.t.setImageResource(aaVar.b.aJ == 1 ? R.drawable.ic_userinfo_groupowner : R.drawable.ic_userinfo_group);
            } else {
                bzVar.t.setVisibility(8);
            }
            if (aaVar.b.ar) {
                bzVar.p.setVisibility(0);
            } else {
                bzVar.p.setVisibility(8);
            }
            if (aaVar.b.b()) {
                bzVar.q.setVisibility(0);
                if (aaVar.b.c()) {
                    bzVar.q.setImageResource(R.drawable.ic_userinfo_vip_year);
                } else {
                    bzVar.q.setImageResource(R.drawable.ic_userinfo_vip);
                }
            } else {
                bzVar.q.setVisibility(8);
            }
        } else {
            bzVar.g.setText(aaVar.c);
            bzVar.w.setVisibility(8);
        }
        j.a((aj) aaVar.b, bzVar.f, this.d);
        bzVar.c.setEnabled((aaVar.d == null || aaVar.d.o == 0) ? false : true);
        bzVar.b.setText(aaVar.g());
        if (a.f(aaVar.b())) {
            bzVar.d.setText(aaVar.c());
            bzVar.d.setVisibility(0);
        } else {
            bzVar.d.setVisibility(8);
        }
        bzVar.f749a.setText(aaVar.f);
        if (aaVar.l != null) {
            ViewGroup.LayoutParams layoutParams = bzVar.e.getLayoutParams();
            layoutParams.height = this.e;
            layoutParams.width = (int) ((((float) layoutParams.height) / ((float) aaVar.l.k())) * ((float) aaVar.l.j()));
            bzVar.e.setLayoutParams(layoutParams);
            bzVar.e.setAlt(aaVar.j);
            a(aaVar, bzVar.e);
            bzVar.e.setAlt(aaVar.j);
            bzVar.e.setVisibility(0);
        } else if (a.f(aaVar.j) && a.f(aaVar.k)) {
            bzVar.e.setVisibility(0);
            ViewGroup.LayoutParams layoutParams2 = bzVar.e.getLayoutParams();
            layoutParams2.height = this.e;
            layoutParams2.width = this.e;
            bzVar.e.setLayoutParams(layoutParams2);
            bzVar.e.setAlt(aaVar.j);
            a(aaVar, bzVar.e);
            bzVar.e.setAlt(aaVar.j);
        } else if (a.f(aaVar.getLoadImageId())) {
            ViewGroup.LayoutParams layoutParams3 = bzVar.e.getLayoutParams();
            layoutParams3.height = this.e;
            layoutParams3.width = this.e;
            bzVar.e.setLayoutParams(layoutParams3);
            bzVar.e.setAlt(PoiTypeDef.All);
            bzVar.e.setVisibility(0);
            j.a((aj) aaVar, (ImageView) bzVar.e, (ViewGroup) this.d, 15, false, false, 0);
        } else {
            bzVar.e.setVisibility(8);
        }
        bzVar.i.setText(new StringBuilder().append(aaVar.g).toString());
        if (aaVar.g <= 0 || aaVar.f2981a == null) {
            bzVar.x.setVisibility(4);
        } else {
            bzVar.x.setText("最后回复: " + a.a(aaVar.f2981a, false));
            bzVar.x.setVisibility(0);
        }
        return view;
    }

    public final void onClick(View view) {
        ((Integer) view.getTag(R.id.tag_item_position)).intValue();
        switch (view.getId()) {
            case R.id.userlist_item_layout_badgeContainer /*2131165265*/:
            case R.id.iv_feed_photo /*2131166248*/:
            case R.id.tv_feed_name /*2131166251*/:
                Intent intent = new Intent();
                intent.setClass(d(), OtherProfileActivity.class);
                intent.putExtra("tag", "local");
                intent.putExtra("momoid", ((aa) getItem(((Integer) view.getTag(R.id.tag_item_position)).intValue())).c);
                d().startActivity(intent);
                return;
            default:
                return;
        }
    }
}
