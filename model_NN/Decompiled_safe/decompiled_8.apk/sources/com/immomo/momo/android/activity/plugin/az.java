package com.immomo.momo.android.activity.plugin;

import android.app.Activity;
import android.os.AsyncTask;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.a.e;
import com.immomo.momo.a.w;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.g;
import com.immomo.momo.protocol.a.d;
import com.immomo.momo.service.aq;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.util.ao;

final class az extends AsyncTask {

    /* renamed from: a  reason: collision with root package name */
    private Activity f2053a = null;
    private v b = null;
    private bf c = null;
    private int d = -1;
    private aq e;
    private /* synthetic */ CommunityStatusActivity f;

    public az(CommunityStatusActivity communityStatusActivity, Activity activity, int i) {
        this.f = communityStatusActivity;
        if (communityStatusActivity.w != null) {
            communityStatusActivity.w.cancel(true);
        }
        communityStatusActivity.w = this;
        this.f2053a = activity;
        this.d = i;
        this.e = new aq();
        this.c = g.q();
    }

    private Boolean a() {
        try {
            switch (this.d) {
                case 1:
                    d.d();
                    break;
                case 2:
                    d.g();
                    break;
                case 3:
                    d.e();
                    break;
                case 4:
                case 5:
                default:
                    return false;
                case 6:
                    d.f();
                    break;
            }
            return true;
        } catch (w e2) {
            this.f.e.a((Throwable) e2);
            ao.g(R.string.errormsg_network_unfind);
        } catch (e e3) {
            this.f.e.a((Throwable) e3);
            ao.g(R.string.errormsg_network_normal400);
        } catch (com.immomo.momo.a.g e4) {
            this.f.e.a((Throwable) e4);
            ao.g(R.string.errormsg_network_normal403);
        } catch (Exception e5) {
            this.f.e.a((Throwable) e5);
            ao.g(R.string.errormsg_server);
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object doInBackground(Object... objArr) {
        return a();
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void onPostExecute(Object obj) {
        this.f.p();
        if (((Boolean) obj).booleanValue()) {
            switch (this.d) {
                case 1:
                    this.c.an = false;
                    this.c.am = PoiTypeDef.All;
                    this.c.ao = PoiTypeDef.All;
                    break;
                case 2:
                    bf bfVar = this.c;
                    this.c.av = PoiTypeDef.All;
                    bf bfVar2 = this.c;
                    break;
                case 3:
                    this.c.ar = false;
                    this.c.aq = PoiTypeDef.All;
                    bf bfVar3 = this.c;
                    break;
                case 4:
                case 5:
                default:
                    return;
                case 6:
                    this.c.at = false;
                    this.c.as = PoiTypeDef.All;
                    this.c.au = false;
                    break;
            }
            this.e.b(this.c);
            this.f.finish();
        }
    }

    /* access modifiers changed from: protected */
    public final void onPreExecute() {
        this.b = new v(this.f2053a, "请稍候，正在提交...");
        this.b.setOnCancelListener(new ba(this));
        this.f.a(this.b);
    }
}
