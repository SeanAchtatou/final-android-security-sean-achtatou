package com.immomo.momo.android.activity.maintab;

import android.content.Context;
import com.immomo.a.a.f.b;
import com.immomo.momo.android.b.o;
import com.immomo.momo.android.b.z;
import com.immomo.momo.android.c.d;
import com.immomo.momo.protocol.a.g;
import java.util.Date;

final class c extends d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ a f1873a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public c(a aVar, Context context) {
        super(context);
        this.f1873a = aVar;
    }

    /* access modifiers changed from: protected */
    public final Object a(Object... objArr) {
        this.f1873a.O = b.a();
        this.b.a((Object) "pre getLocation");
        o a2 = z.a(this.f1873a.c(), this.f1873a.O);
        this.b.a((Object) ("---------getlocation=" + a2));
        this.f1873a.O = null;
        if (a2 != null) {
            g.a().a(this.f1873a.P, a2);
            this.f1873a.Q.a(this.f1873a.P);
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public final void a(Object obj) {
        this.f1873a.R.notifyDataSetChanged();
        this.f1873a.N.a("lasttime_discover", new Date());
    }
}
