package com.immomo.momo.android.activity;

import android.content.Context;
import android.content.Intent;
import com.immomo.momo.a.o;
import com.immomo.momo.android.broadcast.w;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.service.aq;

final class jv extends d {

    /* renamed from: a  reason: collision with root package name */
    private v f1776a;
    private /* synthetic */ ResendEmailActivity c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public jv(ResendEmailActivity resendEmailActivity, Context context) {
        super(context);
        this.c = resendEmailActivity;
        this.f1776a = new v(resendEmailActivity);
        this.f1776a.a("请求提交中");
        this.f1776a.setCancelable(true);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        com.immomo.momo.protocol.a.d.e(this.c.k, this.c.l);
        return "yes";
    }

    /* access modifiers changed from: protected */
    public final void a() {
        if (this.f1776a != null) {
            this.f1776a.setOnCancelListener(new jw(this));
            this.f1776a.show();
        }
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        if (exc instanceof o) {
            this.b.a((Throwable) exc);
            this.c.b((CharSequence) exc.getMessage());
            aq aqVar = new aq();
            this.c.f.aw = true;
            aqVar.b(this.c.f);
            Intent intent = new Intent(w.f2366a);
            intent.putExtra("momoid", this.c.f.h);
            this.c.sendBroadcast(intent);
            this.c.finish();
            return;
        }
        super.a(exc);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        String str = (String) obj;
        if (str != null && str.equals("yes")) {
            a("验证邮件已发送到你的注册email账号");
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
        if (this.f1776a != null) {
            this.f1776a.dismiss();
            this.f1776a = null;
        }
    }
}
