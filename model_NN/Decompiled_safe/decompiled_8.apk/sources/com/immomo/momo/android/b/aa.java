package com.immomo.momo.android.b;

import android.content.Context;
import android.location.Location;

final class aa extends p {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ Object f2313a;
    private final /* synthetic */ o c;
    private final /* synthetic */ Context d;

    aa(Object obj, o oVar, Context context) {
        this.f2313a = obj;
        this.c = oVar;
        this.d = context;
    }

    public final void a(Location location, int i, int i2, int i3) {
        synchronized (this.f2313a) {
            if (this.b) {
                if (z.a(location)) {
                    this.c.a(location.getAccuracy());
                    this.c.a(location.getLatitude());
                    this.c.b(location.getLongitude());
                    this.c.a(i);
                } else {
                    z.b(this.d, i2);
                }
            }
            this.f2313a.notify();
        }
    }
}
