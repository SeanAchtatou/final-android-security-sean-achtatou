package com.immomo.momo.android.activity;

import android.content.Context;
import android.os.AsyncTask;
import com.immomo.momo.a.a;
import com.immomo.momo.a.w;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.d;
import java.io.InterruptedIOException;

final class bq extends AsyncTask {

    /* renamed from: a  reason: collision with root package name */
    private v f1049a;
    private Context b;
    /* access modifiers changed from: private */
    public /* synthetic */ CheckStatusActivity c;

    public bq(CheckStatusActivity checkStatusActivity, Context context) {
        this.c = checkStatusActivity;
        this.b = context;
        this.f1049a = new v(context);
        this.f1049a.setCancelable(true);
        this.f1049a.setOnCancelListener(new br(this));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.android.activity.CheckStatusActivity.a(com.immomo.momo.android.activity.CheckStatusActivity, boolean):void
     arg types: [com.immomo.momo.android.activity.CheckStatusActivity, int]
     candidates:
      com.immomo.momo.android.activity.ah.a(android.os.Bundle, java.lang.String):boolean
      com.immomo.momo.android.activity.ao.a(int, java.lang.String[]):com.immomo.momo.protocol.imjson.c.a
      com.immomo.momo.android.activity.ao.a(com.immomo.momo.android.view.bi, android.view.View$OnClickListener):void
      com.immomo.momo.android.activity.ao.a(java.lang.CharSequence, int):void
      com.immomo.momo.android.activity.ao.a(android.os.Bundle, java.lang.String):boolean
      com.immomo.momo.android.activity.CheckStatusActivity.a(com.immomo.momo.android.activity.CheckStatusActivity, boolean):void */
    private String a() {
        try {
            d.a(100);
            this.c.n = true;
            return null;
        } catch (w e) {
            this.c.n = false;
            this.c.e.a((Throwable) e);
            return null;
        } catch (a e2) {
            this.c.n = true;
            this.c.e.a((Throwable) e2);
            return null;
        } catch (InterruptedIOException e3) {
            this.c.n = false;
            this.c.e.a((Throwable) e3);
            return null;
        } catch (Exception e4) {
            this.c.n = true;
            this.c.e.a((Throwable) e4);
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object doInBackground(Object... objArr) {
        return a();
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void onPostExecute(Object obj) {
        super.onPostExecute((String) obj);
        CheckStatusActivity.a(this.c);
        CheckStatusActivity checkStatusActivity = this.c;
        CheckStatusActivity.u();
        CheckStatusActivity checkStatusActivity2 = this.c;
        CheckStatusActivity.v();
        if (this.f1049a != null) {
            this.f1049a.dismiss();
            this.f1049a = null;
        }
    }

    /* access modifiers changed from: protected */
    public final void onPreExecute() {
        super.onPreExecute();
        if (this.f1049a == null) {
            this.f1049a = new v(this.b);
        }
        this.f1049a.a("正在检查您的网络和定位功能");
        this.f1049a.show();
    }
}
