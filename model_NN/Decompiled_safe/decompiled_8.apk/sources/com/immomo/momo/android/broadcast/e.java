package com.immomo.momo.android.broadcast;

import android.content.Context;
import com.immomo.momo.g;

public final class e extends b {

    /* renamed from: a  reason: collision with root package name */
    public static final String f2348a = (String.valueOf(g.h()) + ".action.blocklist.delete.block");
    public static final String b = (String.valueOf(g.h()) + ".action.blocklist.block.add");

    public e(Context context) {
        super(context);
        a(f2348a, b);
    }
}
