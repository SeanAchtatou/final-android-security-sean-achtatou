package com.immomo.momo.android.activity;

import android.content.Context;
import android.location.Location;
import com.immomo.momo.android.c.d;
import com.immomo.momo.protocol.a.w;
import com.immomo.momo.service.bean.bj;

final class ng extends d {

    /* renamed from: a  reason: collision with root package name */
    private Location f2024a;
    private int c;
    private /* synthetic */ WelcomeActivity d;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ng(WelcomeActivity welcomeActivity, Context context, Location location, int i) {
        super(context);
        this.d = welcomeActivity;
        this.f2024a = location;
        this.c = i;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        this.d.v = w.a().a(this.f2024a.getLatitude(), this.f2024a.getLongitude(), this.c);
        return this.d.v;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        bj bjVar = (bj) obj;
        this.d.l.setPointCount(bjVar.b.size());
        WelcomeActivity.b(this.d, bjVar);
    }
}
