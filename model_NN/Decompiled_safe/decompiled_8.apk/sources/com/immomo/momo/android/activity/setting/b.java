package com.immomo.momo.android.activity.setting;

import android.os.Bundle;
import android.widget.Toast;
import com.weibo.sdk.android.Oauth2AccessToken;
import com.weibo.sdk.android.WeiboAuthListener;
import com.weibo.sdk.android.WeiboDialogError;
import com.weibo.sdk.android.WeiboException;
import com.weibo.sdk.android.util.AccessTokenKeeper;
import java.text.SimpleDateFormat;
import java.util.Date;

final class b implements WeiboAuthListener {

    /* renamed from: a  reason: collision with root package name */
    private Oauth2AccessToken f2126a;
    private /* synthetic */ CommunityBindActivity b;

    b(CommunityBindActivity communityBindActivity) {
        this.b = communityBindActivity;
    }

    public final void onCancel() {
        Toast.makeText(this.b.getApplicationContext(), "Auth cancel", 1).show();
    }

    public final void onComplete(Bundle bundle) {
        String string = bundle.getString("access_token");
        String string2 = bundle.getString("expires_in");
        this.f2126a = new Oauth2AccessToken(string, string2);
        if (this.f2126a.isSessionValid()) {
            this.b.k.setText("认证成功: \r\n access_token: " + string + "\r\nexpires_in: " + string2 + "\r\n有效期：" + new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date(this.f2126a.getExpiresTime())));
            AccessTokenKeeper.keepAccessToken(this.b, this.f2126a);
            Toast.makeText(this.b, "认证成功", 0).show();
        }
    }

    public final void onError(WeiboDialogError weiboDialogError) {
        Toast.makeText(this.b.getApplicationContext(), "Auth error : " + weiboDialogError.getMessage(), 1).show();
    }

    public final void onWeiboException(WeiboException weiboException) {
        Toast.makeText(this.b.getApplicationContext(), "Auth exception : " + weiboException.getMessage(), 1).show();
    }
}
