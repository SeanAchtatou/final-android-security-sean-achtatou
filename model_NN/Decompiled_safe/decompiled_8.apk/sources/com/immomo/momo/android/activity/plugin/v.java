package com.immomo.momo.android.activity.plugin;

final class v implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ BindSinaActivity f2100a;
    private final /* synthetic */ String b;

    v(BindSinaActivity bindSinaActivity, String str) {
        this.f2100a = bindSinaActivity;
        this.b = str;
    }

    public final void run() {
        this.f2100a.j.setVisibility(0);
        this.f2100a.j.requestFocusFromTouch();
        this.f2100a.j.loadUrl(this.b);
    }
}
