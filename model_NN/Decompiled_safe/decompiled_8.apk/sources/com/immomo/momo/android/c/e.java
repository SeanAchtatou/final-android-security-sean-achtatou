package com.immomo.momo.android.c;

import android.content.Context;
import android.content.Intent;
import com.immomo.momo.a.a;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.w;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.util.ao;
import java.util.Date;

public final class e extends x {
    private v g;

    public e(Context context, bf bfVar, bf bfVar2, z zVar) {
        super(context, bfVar, bfVar2, zVar);
        this.g = new v(context);
        this.g.setOnCancelListener(new f(this));
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        w.a().e(this.c.h);
        return true;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
        if (this.g != null) {
            this.g.a("请求提交中");
            this.g.show();
        }
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        if (exc instanceof a) {
            this.e.a((Throwable) exc);
            ao.a((CharSequence) exc.getMessage());
            return;
        }
        this.e.a((Throwable) exc);
        ao.a((CharSequence) "拉黑失败");
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        bf b;
        Boolean bool = (Boolean) obj;
        super.a(bool);
        if (bool.booleanValue() && (b = this.d.b(this.c.h)) != null) {
            ao.a((CharSequence) "拉黑成功");
            b.P = "none";
            this.c.P = "none";
            b.Y = new Date();
            this.d.k(b);
            this.d.d(b);
            c();
            d();
            if (this.f != null) {
                this.f.a();
            }
            Intent intent = new Intent(com.immomo.momo.android.broadcast.e.b);
            intent.putExtra("key_momoid", this.c.h);
            this.f2396a.sendBroadcast(intent);
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
        super.b();
        if (this.g != null) {
            this.g.dismiss();
            this.g = null;
        }
    }
}
