package com.immomo.momo.android.activity.common;

import android.os.Bundle;
import android.support.v4.b.a;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.view.bi;
import com.immomo.momo.service.ag;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.util.k;

public class CreateDiscussTabsActivity extends j {
    private String i = PoiTypeDef.All;

    public CreateDiscussTabsActivity() {
        new ad();
    }

    static /* synthetic */ void a(CreateDiscussTabsActivity createDiscussTabsActivity) {
        if (createDiscussTabsActivity.D().size() <= 0) {
            createDiscussTabsActivity.a((int) R.string.discuss_select_createwarn_little_select);
        } else if (createDiscussTabsActivity.D().size() < 2) {
            createDiscussTabsActivity.a((int) R.string.discuss_select_createwarn_little);
        } else if (createDiscussTabsActivity.D().size() > createDiscussTabsActivity.h) {
            createDiscussTabsActivity.a((int) R.string.discuss_select_toastwarn_much);
        } else {
            new af(createDiscussTabsActivity, createDiscussTabsActivity, createDiscussTabsActivity.D()).execute(new Object[0]);
        }
    }

    /* access modifiers changed from: protected */
    public final String A() {
        return getString(R.string.discuss_select_createwarn_much);
    }

    /* access modifiers changed from: protected */
    public final void B() {
        new ag();
    }

    /* access modifiers changed from: protected */
    public final void C() {
        int i2 = 0;
        a(ay.class, a.class);
        findViewById(R.id.contact_tab_both).setOnClickListener(this);
        findViewById(R.id.contact_tab_follows).setOnClickListener(this);
        int intExtra = getIntent().getIntExtra("showindex", 0);
        if (intExtra >= 0) {
            i2 = intExtra > 1 ? 1 : intExtra;
        }
        c(i2);
    }

    /* access modifiers changed from: protected */
    public final void a(int i2, int i3) {
        m().setTitleText(getResources().getString(R.string.discuss_select_createtitle, Integer.valueOf(i2), Integer.valueOf(i3)));
    }

    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        super.a(bundle);
        this.h = 19;
        E().put("10000", new bf("10000"));
        if (getIntent().getStringExtra("invite_user_id") != null) {
            this.i = getIntent().getStringExtra("invite_user_id");
        }
        if (!a.a((CharSequence) this.i)) {
            D().put(this.i, new bf(this.i));
        }
        a(D().size(), 19);
        bi biVar = new bi(this);
        biVar.a("提交");
        biVar.a((int) R.drawable.ic_topbar_confirm);
        a(biVar, new ae(this));
    }

    /* access modifiers changed from: protected */
    public final void a(String str, int i2) {
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        new k("PO", "P651").e();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        new k("PI", "P651").e();
    }

    /* access modifiers changed from: protected */
    public final boolean y() {
        return false;
    }

    /* access modifiers changed from: protected */
    public final int z() {
        return this.h;
    }
}
