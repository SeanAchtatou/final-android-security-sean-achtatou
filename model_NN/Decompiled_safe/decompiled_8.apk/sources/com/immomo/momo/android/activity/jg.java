package com.immomo.momo.android.activity;

import android.content.Context;
import android.support.v4.b.a;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.android.c.d;
import com.immomo.momo.service.c;

final class jg extends d {

    /* renamed from: a  reason: collision with root package name */
    private String f1762a = PoiTypeDef.All;
    private /* synthetic */ OtherProfileV2Activity c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public jg(OtherProfileV2Activity otherProfileV2Activity, Context context, String str) {
        super(context);
        this.c = otherProfileV2Activity;
        this.f1762a = str;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        String a2 = c.c().a(this.f1762a);
        if (!a.a((CharSequence) a2)) {
            this.c.t.o = a2;
            this.c.q.b(a2, this.c.t.h);
        }
        return a2;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        String str = (String) obj;
        this.b.a((Object) ("GetPhoneContactNameTask, result=" + str));
        if (str != null) {
            this.c.O.setVisibility(0);
            this.c.af.setText(str);
            return;
        }
        this.c.O.setVisibility(8);
    }
}
