package com.immomo.momo.android.activity.plugin;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;

final class an extends d {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public /* synthetic */ BindTwitterActivity f2041a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public an(BindTwitterActivity bindTwitterActivity, Context context) {
        super(context);
        this.f2041a = bindTwitterActivity;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        String[] strArr = {"gBxG1ywhkOWGdSJy67zqAg", "DZPi4Nlkl4NtoNkzOIyF81WfCxeYxlcNeaeLbzCTgZw"};
        if (strArr.length == 2) {
            this.f2041a.m = strArr[0];
            this.f2041a.n = strArr[1];
            if (this.f2041a.u()) {
                this.f2041a.r = "https://api.twitter.com/oauth/authorize?oauth_token=" + this.f2041a.o + "&oauth_callback=" + this.f2041a.i;
                this.b.b((Object) ("authUrl : " + this.f2041a.r));
                this.f2041a.j.post(new ao(this));
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.f2041a.l = new v(this.f2041a, "正在请求数据，请稍候...");
        this.f2041a.l.setOnCancelListener(new ap(this));
        this.f2041a.l.show();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        if (this.f2041a.l != null && this.f2041a.l.isShowing()) {
            this.f2041a.l.dismiss();
        }
    }
}
