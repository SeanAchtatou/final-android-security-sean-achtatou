package com.immomo.momo.android.a;

import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;

final class fx extends Handler {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ fw f831a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    fx(fw fwVar, Looper looper) {
        super(looper);
        this.f831a = fwVar;
    }

    public final void handleMessage(Message message) {
        this.f831a.b.setImageBitmap((Bitmap) message.obj);
    }
}
