package com.immomo.momo.android.activity;

import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import com.immomo.momo.R;
import com.immomo.momo.g;
import java.util.ArrayList;

public abstract class js extends ah {
    /* access modifiers changed from: protected */
    public ArrayList h = new ArrayList();
    protected View[] i;
    protected LinearLayout j;
    protected LinearLayout k;

    /* access modifiers changed from: protected */
    public void u() {
        int round = Math.round(((((float) g.J()) - (g.l().getDimension(R.dimen.pic_item_margin) * 8.0f)) - (g.l().getDimension(R.dimen.pic_layout_margin) * 2.0f)) / 4.0f);
        this.j = (LinearLayout) findViewById(R.id.avatar_line0);
        this.k = (LinearLayout) findViewById(R.id.avatar_line1);
        this.i = new RelativeLayout[8];
        for (int i2 = 0; i2 < 8; i2++) {
            this.i[i2] = findViewById(getResources().getIdentifier("avatar_block" + i2, "id", getPackageName()));
            ViewGroup.LayoutParams layoutParams = this.i[i2].getLayoutParams();
            layoutParams.height = round;
            layoutParams.width = round;
            this.i[i2].setLayoutParams(layoutParams);
        }
    }
}
