package com.immomo.momo.android.activity.plugin;

import android.content.Context;
import com.immomo.momo.R;
import com.immomo.momo.a.a;
import com.immomo.momo.a.o;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.n;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.service.aq;
import com.immomo.momo.util.ao;

final class q extends d {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public String f2096a;
    private v c;
    private boolean d;
    /* access modifiers changed from: private */
    public /* synthetic */ BindRenrenActivity e;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public q(BindRenrenActivity bindRenrenActivity, Context context, String str, boolean z) {
        super(context);
        this.e = bindRenrenActivity;
        this.f2096a = str;
        this.d = z;
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ Object a(Object... objArr) {
        return com.immomo.momo.protocol.a.d.a(this.f2096a, this.d);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
        this.c = new v(this.e);
        this.c.a("请求提交中");
        this.c.setCancelable(true);
        this.c.setOnCancelListener(new r(this));
        this.e.a(this.c);
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        this.e.p();
        if (exc instanceof o) {
            this.e.a(n.a(this.e, (int) R.string.bingrenren_dialog_msg, new s(this), new t()));
        } else if (exc instanceof a) {
            ao.a((CharSequence) exc.getMessage());
        } else {
            ao.g(R.string.errormsg_server);
        }
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        String str = (String) obj;
        super.a((Object) str);
        this.e.p();
        this.e.f.ar = true;
        this.e.f.aq = str;
        new aq().b(this.e.f);
        BindRenrenActivity.e(this.e);
        this.e.finish();
    }
}
