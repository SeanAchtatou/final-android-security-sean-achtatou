package com.immomo.momo.android.activity;

import com.immomo.momo.android.view.a.al;
import com.immomo.momo.service.bean.au;
import com.immomo.momo.util.ao;

final class cx implements al {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ cw f1211a;
    private final /* synthetic */ au b;

    cx(cw cwVar, au auVar) {
        this.f1211a = cwVar;
        this.b = auVar;
    }

    public final void a(int i) {
        switch (i) {
            case 0:
                au auVar = this.b;
                this.f1211a.f1210a.h.remove(this.b);
                if (!EditUserProfileActivity.f(this.f1211a.f1210a)) {
                    au auVar2 = new au();
                    auVar2.d = true;
                    auVar2.c = false;
                    this.f1211a.f1210a.h.add(auVar2);
                }
                this.f1211a.f1210a.h.add(0, auVar);
                if (this.f1211a.f1210a.h.size() > 8) {
                    this.f1211a.f1210a.h.remove(this.f1211a.f1210a.h.size() - 1);
                }
                this.f1211a.f1210a.w();
                this.f1211a.f1210a.G = true;
                return;
            case 1:
                if (this.f1211a.f1210a.h.size() <= 2) {
                    ao.b("无法继续删除,至少保存一张图片做为头像");
                    return;
                }
                this.f1211a.f1210a.h.remove(this.b);
                if (!EditUserProfileActivity.f(this.f1211a.f1210a)) {
                    au auVar3 = new au();
                    auVar3.d = true;
                    auVar3.c = false;
                    this.f1211a.f1210a.h.add(auVar3);
                }
                this.f1211a.f1210a.w();
                this.f1211a.f1210a.G = true;
                return;
            default:
                return;
        }
    }
}
