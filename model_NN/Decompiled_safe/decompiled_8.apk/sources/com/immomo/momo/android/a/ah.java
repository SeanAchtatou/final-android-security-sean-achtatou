package com.immomo.momo.android.a;

import android.content.Intent;
import android.support.v4.b.a;
import android.view.View;
import com.immomo.momo.android.activity.ao;
import com.immomo.momo.android.activity.contacts.ContactPeopleActivity;
import com.immomo.momo.android.activity.contacts.OpenContactActivity;
import com.immomo.momo.android.activity.d;
import com.immomo.momo.android.activity.setting.CommunityBindActivity;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.service.bean.i;

final class ah implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ae f715a;
    private final /* synthetic */ i b;

    ah(ae aeVar, i iVar) {
        this.f715a = aeVar;
        this.b = iVar;
    }

    public final void onClick(View view) {
        ao a2 = this.f715a.e;
        bf q = g.q();
        i iVar = this.b;
        switch (iVar.b()) {
            case 1:
                if (!q.g || a2.o() == null || !a2.o().c) {
                    a2.startActivity(new Intent(a2, OpenContactActivity.class));
                    return;
                } else {
                    a2.startActivity(new Intent(a2, ContactPeopleActivity.class));
                    return;
                }
            case 2:
                if (q.q()) {
                    a.a(a2, 1);
                    return;
                }
                Intent intent = new Intent(a2, CommunityBindActivity.class);
                intent.putExtra("type", 1);
                a2.startActivityForResult(intent, 23);
                return;
            case 3:
                if (q.at) {
                    a.a(a2, 2);
                    return;
                }
                Intent intent2 = new Intent(a2, CommunityBindActivity.class);
                intent2.putExtra("type", 2);
                a2.startActivityForResult(intent2, 22);
                return;
            case 4:
                if (q.ar) {
                    a.a(a2, 3);
                    return;
                }
                Intent intent3 = new Intent(a2, CommunityBindActivity.class);
                intent3.putExtra("type", 3);
                a2.startActivityForResult(intent3, 21);
                return;
            case 5:
            default:
                return;
            case 6:
                if (!a.a((CharSequence) iVar.d().getAction())) {
                    d.a(iVar.d().getAction(), a2);
                    return;
                }
                return;
        }
    }
}
