package com.immomo.momo.android.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import com.immomo.momo.MomoApplication;
import com.immomo.momo.R;
import com.immomo.momo.android.view.HeaderLayout;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.at;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.util.ao;
import com.immomo.momo.util.m;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;

public abstract class ar extends Fragment {
    protected final m L = new m(getClass().getSimpleName());
    protected bf M = null;
    /* access modifiers changed from: protected */
    public at N = null;
    private WeakReference O = null;
    private Map P = null;
    private HeaderLayout Q = null;
    private boolean R = false;
    private boolean S = false;
    private boolean T = false;
    private boolean U = false;
    private int V;
    private int W;
    private Intent X;
    private Dialog Y = null;

    public static Context H() {
        return g.c();
    }

    public static MomoApplication L() {
        return g.d();
    }

    private View O() {
        if (this.O != null) {
            return (View) this.O.get();
        }
        return null;
    }

    public static void b(Intent intent) {
        g.c().sendBroadcast(intent);
    }

    public static void c(Intent intent) {
        g.c().sendOrderedBroadcast(intent, null);
    }

    /* access modifiers changed from: protected */
    public abstract int B();

    /* access modifiers changed from: protected */
    public void C() {
    }

    public final ao D() {
        return (ao) c();
    }

    /* access modifiers changed from: protected */
    public void E() {
    }

    public boolean F() {
        return false;
    }

    public boolean G() {
        return false;
    }

    public final boolean I() {
        return this.S;
    }

    public final boolean J() {
        return this.R;
    }

    public HeaderLayout K() {
        return this.Q;
    }

    public final boolean M() {
        return this.U;
    }

    public final synchronized void N() {
        if (this.Y != null && this.Y.isShowing() && !c().isFinishing()) {
            this.Y.dismiss();
            this.Y = null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final View a(LayoutInflater layoutInflater, ViewGroup viewGroup) {
        View inflate;
        this.L.a((Object) ("onCreateView getContentView()=" + O()));
        if (O() != null) {
            this.L.a((Object) ("onCreateView, view parent=" + O().getParent()));
            View O2 = O();
            ViewParent parent = O2.getParent();
            if (parent != null) {
                ((ViewGroup) parent).removeView(O2);
            }
            this.S = true;
            inflate = O2;
        } else {
            this.S = false;
            inflate = layoutInflater.inflate(B(), viewGroup, false);
            this.O = new WeakReference(inflate);
        }
        this.L.a((Object) "onCreateView~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        return inflate;
    }

    public final void a(int i, int i2, Intent intent) {
        if (this.R) {
            b(i, i2, intent);
        } else {
            this.L.c("requestCode=" + i + ", resultCode=" + i2 + ", fragment not created");
            this.T = true;
            this.V = i;
            this.W = i2;
            this.X = intent;
        }
        super.a(i, i2, intent);
    }

    public void a(Activity activity) {
        super.a(activity);
        this.L.a((Object) "-----onAttach");
    }

    public final synchronized void a(Dialog dialog) {
        N();
        this.Y = dialog;
        if (!c().isFinishing()) {
            dialog.show();
        }
    }

    public final void a(BroadcastReceiver broadcastReceiver) {
        c().unregisterReceiver(broadcastReceiver);
    }

    public final void a(Bundle bundle) {
        super.a(bundle);
        this.P = new HashMap();
        this.O = null;
        this.S = false;
        this.R = false;
    }

    public final void a(CharSequence charSequence) {
        if (K() != null) {
            K().setTitleText(charSequence);
        } else {
            c().setTitle(charSequence);
        }
    }

    public final void a(String str) {
        if (this.U) {
            ao.b(str);
        }
    }

    public final void b(int i) {
        c().overridePendingTransition(R.anim.zoom_enter, i);
    }

    /* access modifiers changed from: protected */
    public void b(int i, int i2, Intent intent) {
    }

    public void b(Bundle bundle) {
        super.b(bundle);
        this.M = ((ao) c()).h();
        this.N = ((ao) c()).o();
        if (!F() || (this.M != null && g.d().j())) {
            if (bundle != null) {
                h(bundle);
            }
            View c = c((int) R.id.layout_header);
            if (c != null && (c instanceof HeaderLayout)) {
                this.Q = (HeaderLayout) c;
            }
            if (this.S) {
                E();
            } else {
                C();
            }
            g(bundle);
            i(bundle);
            this.R = true;
            if (this.T) {
                b(this.V, this.W, this.X);
                this.T = false;
            }
        }
    }

    public final void b(String str) {
        if (this.U) {
            ao.a((CharSequence) str);
        }
    }

    public final View c(int i) {
        View view = this.P.get(Integer.valueOf(i)) != null ? (View) ((WeakReference) this.P.get(Integer.valueOf(i))).get() : null;
        if (view == null) {
            view = O() == null ? null : O().findViewById(i);
            if (view != null) {
                this.P.put(Integer.valueOf(i), new WeakReference(view));
            }
        }
        return view;
    }

    public final void d(int i) {
        if (this.U) {
            ao.e(i);
        }
    }

    public final void d(boolean z) {
        this.U = z;
    }

    public final void e(int i) {
        if (this.U) {
            ao.g(i);
        }
    }

    /* access modifiers changed from: protected */
    public abstract void g(Bundle bundle);

    public void h(Bundle bundle) {
    }

    /* access modifiers changed from: protected */
    public void i(Bundle bundle) {
    }

    public void m() {
        super.m();
    }

    public final void n() {
        super.n();
    }

    public void p() {
        super.p();
        this.L.a((Object) "-----onDestroy");
    }

    public void r() {
        super.r();
        this.R = false;
        this.L.a((Object) "-----onDetach");
    }
}
