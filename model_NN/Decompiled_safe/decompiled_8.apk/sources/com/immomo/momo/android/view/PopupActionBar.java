package com.immomo.momo.android.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;

public class PopupActionBar extends LinearLayout {

    /* renamed from: a  reason: collision with root package name */
    private LinearLayout f2645a = null;
    private TextView b = null;
    private TextView c = null;
    private CharSequence d = PoiTypeDef.All;

    public PopupActionBar(Context context) {
        super(context);
        a();
    }

    public PopupActionBar(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, com.immomo.momo.android.view.PopupActionBar, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    private void a() {
        ((LayoutInflater) getContext().getSystemService("layout_inflater")).inflate((int) R.layout.common_actionbar, (ViewGroup) this, true);
        this.f2645a = (LinearLayout) findViewById(R.id.actionbar_layout_buttoncontainer);
        findViewById(R.id.actionbar_layout_leftview);
        this.b = (TextView) findViewById(R.id.actionbar_tv_title);
        this.c = (TextView) findViewById(R.id.actionbar_tv_subtitle);
    }

    public final void a(a aVar, View.OnClickListener onClickListener) {
        if (aVar != null && aVar != null) {
            if (onClickListener != null) {
                aVar.setOnClickListener(onClickListener);
            }
            this.f2645a.setVisibility(0);
            this.f2645a.addView(aVar, 0, new LinearLayout.LayoutParams(-1, -1, 1.0f));
        }
    }

    public CharSequence getTitleText() {
        return this.d;
    }

    public void setSubTitleText(CharSequence charSequence) {
        this.c.setText(charSequence);
        if (charSequence.toString().trim().length() > 0) {
            this.c.setVisibility(0);
        } else {
            this.c.setVisibility(8);
        }
    }

    public void setTitleText(int i) {
        setTitleText(getResources().getString(i));
    }

    public void setTitleText(CharSequence charSequence) {
        this.d = charSequence;
        this.b.setText(this.d);
        if (charSequence.toString().trim().length() > 0) {
            this.b.setVisibility(0);
        } else {
            this.b.setVisibility(4);
        }
    }

    public void setTitleViewOnClickListener(View.OnClickListener onClickListener) {
        this.b.setOnClickListener(onClickListener);
    }
}
