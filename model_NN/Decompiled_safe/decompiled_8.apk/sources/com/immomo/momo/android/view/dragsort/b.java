package com.immomo.momo.android.view.dragsort;

import android.view.GestureDetector;
import android.view.MotionEvent;

final class b extends GestureDetector.SimpleOnGestureListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ a f2790a;

    b(a aVar) {
        this.f2790a = aVar;
    }

    public final boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
        boolean z = false;
        if (this.f2790a.d && this.f2790a.e) {
            int width = this.f2790a.w.getWidth() / 5;
            if (f > this.f2790a.r) {
                if (this.f2790a.x > (-width)) {
                    z = this.f2790a.w.a(f);
                }
            } else if (f < (-this.f2790a.r) && this.f2790a.x < width) {
                z = this.f2790a.w.a(f);
            }
            this.f2790a.e = false;
        }
        return z;
    }
}
