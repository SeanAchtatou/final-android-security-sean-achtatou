package com.immomo.momo.android.activity.tieba;

import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.a.a.f.b;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.ImageFactoryActivity;
import com.immomo.momo.android.activity.ah;
import com.immomo.momo.android.view.EmoteInputView;
import com.immomo.momo.android.view.HeaderLayout;
import com.immomo.momo.android.view.MEmoteEditeText;
import com.immomo.momo.android.view.ResizeListenerLayout;
import com.immomo.momo.android.view.a.n;
import com.immomo.momo.android.view.a.o;
import com.immomo.momo.android.view.bi;
import com.immomo.momo.plugin.b.a;
import com.immomo.momo.service.bean.c.c;
import com.immomo.momo.service.bean.q;
import com.immomo.momo.util.ao;
import com.immomo.momo.util.h;
import com.immomo.momo.util.j;
import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.UUID;
import mm.purchasesdk.PurchaseCode;
import org.json.JSONArray;
import org.json.JSONObject;

public class PublishTieCommentActivity extends ah implements View.OnClickListener, View.OnTouchListener {
    private File A = null;
    /* access modifiers changed from: private */
    public File B = null;
    /* access modifiers changed from: private */
    public a C = null;
    private Bitmap D = null;
    /* access modifiers changed from: private */
    public HashMap E = new HashMap();
    /* access modifiers changed from: private */
    public String F = PoiTypeDef.All;
    protected boolean h = false;
    /* access modifiers changed from: private */
    public MEmoteEditeText i = null;
    private View j;
    private TextView k;
    private TextView l;
    /* access modifiers changed from: private */
    public ImageView m = null;
    /* access modifiers changed from: private */
    public TextView n = null;
    private ResizeListenerLayout o = null;
    private boolean p = false;
    private String q;
    /* access modifiers changed from: private */
    public String r;
    /* access modifiers changed from: private */
    public String s;
    /* access modifiers changed from: private */
    public String t;
    private String u;
    private String v;
    /* access modifiers changed from: private */
    public int w;
    /* access modifiers changed from: private */
    public EmoteInputView x = null;
    private Handler y = new Handler();
    private String z = PoiTypeDef.All;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.h.a(java.lang.String, java.lang.String, int, boolean):void
     arg types: [java.lang.String, java.lang.String, int, int]
     candidates:
      com.immomo.momo.util.h.a(java.lang.String, android.graphics.Bitmap, int, boolean):java.io.File
      com.immomo.momo.util.h.a(java.lang.String, java.lang.String, int, boolean):void */
    static /* synthetic */ void a(PublishTieCommentActivity publishTieCommentActivity, c cVar) {
        try {
            if (!android.support.v4.b.a.a((CharSequence) cVar.getLoadImageId()) && android.support.v4.b.a.a((CharSequence) cVar.s) && publishTieCommentActivity.B != null) {
                h.a(publishTieCommentActivity.B.getName().substring(0, publishTieCommentActivity.B.getName().lastIndexOf(".")), cVar.getLoadImageId(), 16, true);
            }
        } catch (Exception e) {
            publishTieCommentActivity.e.a((Throwable) e);
        }
    }

    static /* synthetic */ void i(PublishTieCommentActivity publishTieCommentActivity) {
        if (!publishTieCommentActivity.g.z) {
            n a2 = n.a(publishTieCommentActivity, PoiTypeDef.All, "接受协议", new bo(publishTieCommentActivity));
            a2.setTitle("陌陌吧用户协议");
            View inflate = publishTieCommentActivity.getLayoutInflater().inflate((int) R.layout.dialog_tieba_protocol, (ViewGroup) null);
            WebView webView = (WebView) inflate.findViewById(R.id.webview);
            View findViewById = inflate.findViewById(R.id.loading_indicator);
            WebSettings settings = webView.getSettings();
            settings.setCacheMode(2);
            settings.setJavaScriptEnabled(true);
            settings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
            webView.setWebChromeClient(new bq());
            webView.setDownloadListener(new br(publishTieCommentActivity));
            webView.setWebViewClient(new bs(publishTieCommentActivity, findViewById));
            webView.loadUrl(PublishTieActivity.h);
            a2.setOnCancelListener(new bp(publishTieCommentActivity));
            a2.setContentView(inflate);
            a2.show();
        }
    }

    static /* synthetic */ boolean l(PublishTieCommentActivity publishTieCommentActivity) {
        String trim = publishTieCommentActivity.i.getText().toString().trim();
        if (trim.length() > 1000) {
            publishTieCommentActivity.a((CharSequence) "内容不能超过1000字");
            return false;
        } else if (publishTieCommentActivity.C == null && publishTieCommentActivity.B == null && android.support.v4.b.a.a((CharSequence) trim)) {
            publishTieCommentActivity.a((CharSequence) "内容不能为空");
            return false;
        } else if (trim.length() >= 4) {
            return true;
        } else {
            publishTieCommentActivity.a((CharSequence) "内容不能少于 4个字");
            return false;
        }
    }

    static /* synthetic */ void m(PublishTieCommentActivity publishTieCommentActivity) {
        if (publishTieCommentActivity.B != null) {
            try {
                JSONArray jSONArray = new JSONArray();
                JSONObject jSONObject = new JSONObject();
                jSONObject.put("upload", "NO");
                jSONObject.put("key", "photo_0");
                publishTieCommentActivity.E.put("photo_0", publishTieCommentActivity.B);
                jSONArray.put(jSONObject);
                publishTieCommentActivity.F = jSONArray.toString();
            } catch (Exception e) {
                publishTieCommentActivity.e.a((Throwable) e);
            }
        }
        publishTieCommentActivity.b(new bt(publishTieCommentActivity, publishTieCommentActivity));
    }

    static /* synthetic */ void r(PublishTieCommentActivity publishTieCommentActivity) {
        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("immomo_");
        stringBuffer.append(android.support.v4.b.a.c(new Date()));
        stringBuffer.append("_" + UUID.randomUUID());
        stringBuffer.append(".jpg");
        publishTieCommentActivity.z = stringBuffer.toString();
        intent.putExtra("output", Uri.fromFile(new File(com.immomo.momo.a.i(), publishTieCommentActivity.z)));
        publishTieCommentActivity.startActivityForResult(intent, PurchaseCode.UNSUB_OK);
    }

    static /* synthetic */ void s(PublishTieCommentActivity publishTieCommentActivity) {
        Intent intent = new Intent("android.intent.action.GET_CONTENT");
        intent.setType("image/*");
        publishTieCommentActivity.startActivityForResult(intent, PurchaseCode.ORDER_OK);
    }

    static /* synthetic */ void t(PublishTieCommentActivity publishTieCommentActivity) {
        if (publishTieCommentActivity.B != null) {
            try {
                publishTieCommentActivity.B.delete();
                publishTieCommentActivity.B = null;
            } catch (Exception e) {
                publishTieCommentActivity.e.a((Throwable) e);
            }
        }
        if (publishTieCommentActivity.C != null) {
            publishTieCommentActivity.C = null;
        }
        publishTieCommentActivity.F = PoiTypeDef.All;
        publishTieCommentActivity.E.clear();
        publishTieCommentActivity.m.setImageResource(R.drawable.ic_publish_camera);
    }

    private void u() {
        Display defaultDisplay = getWindowManager().getDefaultDisplay();
        defaultDisplay.getWidth();
        this.w = defaultDisplay.getHeight();
    }

    private void v() {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService("input_method");
        View currentFocus = getCurrentFocus();
        if (currentFocus != null) {
            inputMethodManager.hideSoftInputFromWindow(currentFocus.getWindowToken(), 2);
        }
    }

    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        super.a(bundle);
        setContentView((int) R.layout.activity_publish_tiecomment);
        u();
        this.o = (ResizeListenerLayout) findViewById(R.id.rootlayout);
        this.i = (MEmoteEditeText) findViewById(R.id.signeditor_tv_text);
        this.m = (ImageView) findViewById(R.id.iv_camera);
        this.n = (TextView) findViewById(R.id.tv_textcount);
        this.j = findViewById(R.id.layout_title);
        this.l = (TextView) findViewById(R.id.tv_comment_content);
        this.k = (TextView) findViewById(R.id.tv_comment_name);
        this.x = (EmoteInputView) findViewById(R.id.emoteview);
        this.x.setEmoteFlag(1);
        this.x.setEditText(this.i);
        this.x.setOnEmoteSelectedListener(new bh(this));
        this.i.setOnTouchListener(this);
        this.i.addTextChangedListener(new be(this));
        bi a2 = new bi(getApplicationContext()).a((int) R.drawable.ic_topbar_confirm_white);
        a2.setMarginRight(8);
        a2.setBackgroundResource(R.drawable.bg_header_submit);
        m().a(a2, new bf(this));
        findViewById(R.id.iv_camera_cover).setOnClickListener(this);
        findViewById(R.id.iv_emote_cover).setOnClickListener(this);
        this.o.setOnResizeListener(new bg(this));
        if (bundle == null || !bundle.getBoolean("from_saveinstance", false)) {
            Intent intent = getIntent();
            if (!(intent == null || intent.getExtras() == null)) {
                this.t = intent.getStringExtra("tiezi_id") == null ? PoiTypeDef.All : intent.getStringExtra("tiezi_id");
                this.q = intent.getStringExtra("tiezi_name") == null ? PoiTypeDef.All : intent.getStringExtra("tiezi_name");
                this.s = intent.getStringExtra("tiecommen_tocommentid") == null ? PoiTypeDef.All : intent.getStringExtra("tiecommen_tocommentid");
                this.r = intent.getStringExtra("tiecomment_tomomoid") == null ? PoiTypeDef.All : intent.getStringExtra("tiecomment_tomomoid");
                this.v = intent.getStringExtra("tiecommen_tousercontent") == null ? PoiTypeDef.All : intent.getStringExtra("tiecommen_tousercontent");
                this.u = intent.getStringExtra("tiecommen_tousername") == null ? PoiTypeDef.All : intent.getStringExtra("tiecommen_tousername");
            }
        } else {
            this.z = bundle.getString("camera_filename");
            String string = bundle.getString("avatorFilePath");
            String string2 = bundle.getString("save_uploadguid");
            String string3 = bundle.getString("save_emote");
            this.t = bundle.getString("tiezi_id") == null ? PoiTypeDef.All : bundle.getString("tiezi_id");
            this.q = bundle.getString("tiezi_name") == null ? PoiTypeDef.All : bundle.getString("tiezi_name");
            this.s = bundle.getString("tiecommen_tocommentid") == null ? PoiTypeDef.All : bundle.getString("tiecommen_tocommentid");
            this.r = bundle.getString("tiecomment_tomomoid") == null ? PoiTypeDef.All : bundle.getString("tiecomment_tomomoid");
            this.v = bundle.getString("tiecommen_tousercontent") == null ? PoiTypeDef.All : bundle.getString("tiecommen_tousercontent");
            this.u = bundle.getString("tiecommen_tousername") == null ? PoiTypeDef.All : bundle.getString("tiecommen_tousername");
            if (!android.support.v4.b.a.a((CharSequence) string)) {
                this.A = new File(string);
            }
            if (!android.support.v4.b.a.a((CharSequence) string2)) {
                this.B = new File(string2);
            }
            if (!android.support.v4.b.a.a((CharSequence) string3)) {
                this.C = new a(string3.toString());
            }
            String str = bundle.get("save_tiecontent") == null ? PoiTypeDef.All : (String) bundle.get("save_tiecontent");
            if (!android.support.v4.b.a.a((CharSequence) str)) {
                this.i.setText(str);
                this.i.setSelection(str.length());
            }
            if (this.B != null) {
                this.m.setImageBitmap(j.a(this.B));
            }
            if (this.C != null) {
                this.m.setImageBitmap(android.support.v4.b.a.l(q.a(this.C.g(), this.C.h()).getAbsolutePath()));
            }
        }
        if (android.support.v4.b.a.a((CharSequence) this.t)) {
            a((CharSequence) "没有指定陌陌吧ID");
            finish();
        }
        m().setTitleText("评论");
        String str2 = android.support.v4.b.a.a(this.q) ? PoiTypeDef.All : this.q;
        HeaderLayout m2 = m();
        if (android.support.v4.b.a.a((CharSequence) str2)) {
            str2 = PoiTypeDef.All;
        }
        m2.setSubTitleText(str2);
        this.j.setVisibility(android.support.v4.b.a.a(this.s) ? 8 : 0);
        this.l.setText(this.v);
        this.k.setText("回复 " + this.u + ": ");
        this.y.postDelayed(new bn(this), 200);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.h.a(java.lang.String, android.graphics.Bitmap, int, boolean):java.io.File
     arg types: [java.lang.String, android.graphics.Bitmap, int, int]
     candidates:
      com.immomo.momo.util.h.a(java.lang.String, java.lang.String, int, boolean):void
      com.immomo.momo.util.h.a(java.lang.String, android.graphics.Bitmap, int, boolean):java.io.File */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.b.a.a(android.graphics.Bitmap, float, boolean):android.graphics.Bitmap
     arg types: [android.graphics.Bitmap, int, int]
     candidates:
      android.support.v4.b.a.a(android.graphics.Bitmap, int, int):android.graphics.Bitmap
      android.support.v4.b.a.a(java.io.File, int, int):android.graphics.Bitmap
      android.support.v4.b.a.a(java.lang.String, java.lang.String, java.lang.String):java.lang.String
      android.support.v4.b.a.a(java.lang.Object[], java.lang.String, java.lang.String):java.lang.String
      android.support.v4.b.a.a(android.graphics.Bitmap, float, boolean):android.graphics.Bitmap */
    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        switch (i2) {
            case PurchaseCode.QUERY_OK /*101*/:
                if (i3 == -1 && intent != null) {
                    if (!android.support.v4.b.a.a((CharSequence) this.z)) {
                        File file = new File(com.immomo.momo.a.i(), this.z);
                        if (file.exists()) {
                            file.delete();
                        }
                        this.z = null;
                    }
                    if (this.A != null) {
                        String absolutePath = this.A.getAbsolutePath();
                        String substring = this.A.getName().substring(0, this.A.getName().lastIndexOf("."));
                        Bitmap l2 = android.support.v4.b.a.l(absolutePath);
                        if (l2 != null) {
                            this.B = h.a(substring, l2, 16, false);
                            this.e.a((Object) ("scaleAndSavePhoto, uploadFile=" + this.B.getPath()));
                            this.D = android.support.v4.b.a.a(l2, 150.0f, true);
                            h.a(substring, this.D, 15, false);
                            this.m.setImageBitmap(this.D);
                            this.C = null;
                        }
                        try {
                            this.A.delete();
                            this.A = null;
                        } catch (Exception e) {
                        }
                        getWindow().getDecorView().requestFocus();
                        return;
                    }
                    return;
                } else if (i3 == 1003) {
                    ao.b("图片尺寸太小，请重新选择", 1);
                    return;
                } else if (i3 == 1000) {
                    ao.d(R.string.cropimage_error_other);
                    return;
                } else if (i3 == 1002) {
                    ao.d(R.string.cropimage_error_store);
                    return;
                } else if (i3 == 1001) {
                    ao.d(R.string.cropimage_error_filenotfound);
                    return;
                } else {
                    return;
                }
            case PurchaseCode.ORDER_OK /*102*/:
                if (i3 == -1 && intent != null) {
                    Uri data = intent.getData();
                    Intent intent2 = new Intent(this, ImageFactoryActivity.class);
                    intent2.setData(data);
                    intent2.putExtra("aspectY", 1);
                    intent2.putExtra("aspectX", 1);
                    intent2.putExtra("maxwidth", 720);
                    intent2.putExtra("maxheight", 3000);
                    intent2.putExtra("minsize", 320);
                    intent2.putExtra("process_model", "filter");
                    this.A = new File(com.immomo.momo.a.g(), "temp_" + b.a() + ".jpg_");
                    intent2.putExtra("outputFilePath", this.A.getAbsolutePath());
                    startActivityForResult(intent2, PurchaseCode.QUERY_OK);
                    return;
                }
                return;
            case PurchaseCode.UNSUB_OK /*103*/:
                if (i3 == -1 && !android.support.v4.b.a.a((CharSequence) this.z)) {
                    this.i.requestFocus();
                    Uri fromFile = Uri.fromFile(new File(com.immomo.momo.a.i(), this.z));
                    if (fromFile != null) {
                        Intent intent3 = new Intent(this, ImageFactoryActivity.class);
                        intent3.setData(fromFile);
                        intent3.putExtra("minsize", 320);
                        intent3.putExtra("process_model", "filter");
                        intent3.putExtra("maxwidth", 720);
                        intent3.putExtra("maxheight", 3000);
                        this.A = new File(com.immomo.momo.a.g(), "temp_" + b.a() + ".jpg_");
                        intent3.putExtra("outputFilePath", this.A.getAbsolutePath());
                        startActivityForResult(intent3, PurchaseCode.QUERY_OK);
                        return;
                    }
                    return;
                }
                return;
            default:
                return;
        }
    }

    public void onBackPressed() {
        if (r()) {
            if (this.x.isShown()) {
                this.x.a();
                this.h = false;
                return;
            } else if (!(this.C == null && this.B == null && !android.support.v4.b.a.f(this.i.getText().toString().trim()))) {
                n nVar = new n(this);
                nVar.a();
                nVar.setTitle((int) R.string.feed_publish_dialog_title);
                nVar.a((int) R.string.publishtiecomment_quit_tip);
                nVar.a(0, (int) R.string.dialog_btn_confim, new bl(this));
                nVar.a(1, (int) R.string.dialog_btn_cancel, new bm());
                nVar.show();
                return;
            }
        }
        super.onBackPressed();
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_camera_cover /*2131165403*/:
                this.x.a();
                v();
                o oVar = new o(this, this.B != null || this.C != null ? R.array.pfeed_add_delete_photo : R.array.publishtiecomment_add_pic);
                oVar.setTitle((int) R.string.dialog_title_add_pic);
                oVar.a(new bk(this));
                oVar.show();
                return;
            case R.id.iv_emote_cover /*2131165824*/:
                v();
                if (this.h) {
                    this.y.postDelayed(new bd(this), 300);
                    return;
                } else {
                    this.x.b();
                    return;
                }
            default:
                return;
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        u();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Bundle bundle) {
        super.onRestoreInstanceState(bundle);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (!this.p) {
            this.p = true;
        }
        this.h = false;
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        String editable = this.i.getText().toString();
        if (!android.support.v4.b.a.a((CharSequence) editable)) {
            bundle.putString("save_tiecontent", editable);
        }
        if (this.B != null) {
            bundle.putString("save_uploadguid", this.B.getAbsolutePath());
        }
        if (this.C != null) {
            bundle.putString("save_emote", this.C.toString());
        }
        bundle.putString("tiezi_id", this.t);
        bundle.putString("tiezi_name", this.q);
        bundle.putString("tiecomment_tomomoid", this.r);
        bundle.putString("tiezi_name", this.s);
        bundle.putString("tiecommen_tousername", this.u);
        bundle.putString("tiecommen_tousercontent", this.v);
        if (!android.support.v4.b.a.a((CharSequence) this.z)) {
            bundle.putString("camera_filename", this.z);
        }
        if (this.A != null) {
            bundle.putString("avatorFilePath", this.A.getAbsolutePath());
        }
        bundle.putBoolean("from_saveinstance", true);
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        switch (view.getId()) {
            case R.id.layout_content /*2131165199*/:
                if ((this.x.isShown() || this.h) && 1 == motionEvent.getAction() && motionEvent.getEventTime() - motionEvent.getDownTime() < 100) {
                    if (this.x.isShown()) {
                        this.x.a();
                    } else {
                        v();
                    }
                    this.h = false;
                    break;
                }
            case R.id.signeditor_tv_text /*2131165795*/:
                if (motionEvent.getAction() == 1) {
                    this.x.a();
                    break;
                }
                break;
        }
        return false;
    }
}
