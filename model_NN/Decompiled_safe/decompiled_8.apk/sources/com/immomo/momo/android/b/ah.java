package com.immomo.momo.android.b;

import android.location.Location;
import com.immomo.momo.util.ak;
import mm.purchasesdk.PurchaseCode;

final class ah extends p {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ Object f2318a;
    private final /* synthetic */ ak c;
    private final /* synthetic */ p d;

    ah(Object obj, ak akVar, p pVar) {
        this.f2318a = obj;
        this.c = akVar;
        this.d = pVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.Integer]
     candidates:
      com.immomo.momo.util.ak.a(android.content.Context, java.lang.String):com.immomo.momo.util.ak
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Integer):int
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Long):long
      com.immomo.momo.util.ak.a(java.lang.String, int):void
      com.immomo.momo.util.ak.a(java.lang.String, long):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.String):void
      com.immomo.momo.util.ak.a(java.lang.String, boolean):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Boolean):boolean
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void */
    public final void a(Location location, int i, int i2, int i3) {
        if (z.a(location)) {
            synchronized (this.f2318a) {
                z.d.a((Object) "google is ok in all....");
                z.c();
                this.c.a("momolocate_locater_type", (Object) Integer.valueOf((int) PurchaseCode.LOADCHANNEL_ERR));
                this.d.a(location, i, i2, i3);
                a();
                this.f2318a.notify();
            }
        }
    }
}
