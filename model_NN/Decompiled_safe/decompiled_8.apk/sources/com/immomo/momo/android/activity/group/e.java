package com.immomo.momo.android.activity.group;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.n;

final class e extends d {

    /* renamed from: a  reason: collision with root package name */
    private v f1623a;
    private /* synthetic */ EditGroupAnnounceActivity c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public e(EditGroupAnnounceActivity editGroupAnnounceActivity, Context context) {
        super(context);
        this.c = editGroupAnnounceActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.service.y.a(com.immomo.momo.service.bean.a.a, boolean):void
     arg types: [com.immomo.momo.service.bean.a.a, int]
     candidates:
      com.immomo.momo.service.y.a(com.immomo.momo.service.bean.a.a, java.lang.String):void
      com.immomo.momo.service.y.a(java.lang.String, int):void
      com.immomo.momo.service.y.a(java.lang.String, java.lang.String):void
      com.immomo.momo.service.y.a(java.lang.String, java.util.List):void
      com.immomo.momo.service.y.a(java.lang.String, boolean):void
      com.immomo.momo.service.y.a(java.lang.String, java.lang.String[]):void
      com.immomo.momo.service.y.a(java.util.List, java.lang.String):void
      com.immomo.momo.service.y.a(com.immomo.momo.service.bean.a.a, boolean):void */
    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        String a2 = n.a().a(this.c.n);
        this.c.o.a(this.c.n, false);
        return a2;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.f1623a = new v(this.c);
        this.f1623a.a("请求提交中...");
        this.f1623a.setCancelable(true);
        this.f1623a.setOnCancelListener(new f(this));
        this.f1623a.show();
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        this.b.a((Throwable) exc);
        a(exc.getMessage());
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        a((String) obj);
        this.c.setResult(-1);
        this.c.finish();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        if (this.f1623a != null) {
            this.f1623a.dismiss();
        }
    }
}
