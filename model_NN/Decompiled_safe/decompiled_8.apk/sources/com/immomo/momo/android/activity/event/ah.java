package com.immomo.momo.android.activity.event;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.protocol.a.j;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.service.r;
import java.util.ArrayList;
import java.util.List;

final class ah extends d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ EventMemberListActivity f1352a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ah(EventMemberListActivity eventMemberListActivity, Context context) {
        super(context);
        this.f1352a = eventMemberListActivity;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        ArrayList arrayList = new ArrayList();
        this.f1352a.s = j.a().b(arrayList, this.f1352a.n, this.f1352a.l.getCount());
        r d = this.f1352a.k;
        String unused = this.f1352a.n;
        d.f(arrayList);
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        List<bf> list = (List) obj;
        if (list != null) {
            if (list.size() > 0) {
                for (bf bfVar : list) {
                    if (!this.f1352a.r.containsKey(bfVar.h)) {
                        this.f1352a.j.add(bfVar);
                        this.f1352a.r.put(bfVar.h, bfVar);
                    }
                }
                this.f1352a.l.notifyDataSetChanged();
            } else {
                this.f1352a.i.k();
            }
            EventMemberListActivity.j(this.f1352a);
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.f1352a.o = null;
        this.f1352a.q.e();
    }
}
