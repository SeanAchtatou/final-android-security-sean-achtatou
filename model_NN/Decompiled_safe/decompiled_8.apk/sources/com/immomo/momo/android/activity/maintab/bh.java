package com.immomo.momo.android.activity.maintab;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.b.a;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.android.a.a.ah;
import com.immomo.momo.android.activity.an;
import com.immomo.momo.android.activity.group.GroupActionListFragment;
import com.immomo.momo.android.activity.message.HiSessionListActivity;
import com.immomo.momo.android.broadcast.d;
import com.immomo.momo.android.broadcast.e;
import com.immomo.momo.android.broadcast.w;
import com.immomo.momo.android.c.u;
import com.immomo.momo.android.view.HeaderLayout;
import com.immomo.momo.android.view.LoadingButton;
import com.immomo.momo.android.view.RefreshOnOverScrollListView;
import com.immomo.momo.android.view.bi;
import com.immomo.momo.android.view.bl;
import com.immomo.momo.android.view.cd;
import com.immomo.momo.android.view.cf;
import com.immomo.momo.android.view.dp;
import com.immomo.momo.android.view.g;
import com.immomo.momo.protocol.imjson.c.b;
import com.immomo.momo.service.af;
import com.immomo.momo.service.ag;
import com.immomo.momo.service.aq;
import com.immomo.momo.service.bean.Message;
import com.immomo.momo.service.bean.ax;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.service.bean.n;
import com.immomo.momo.service.c;
import com.immomo.momo.service.h;
import com.immomo.momo.service.y;
import com.immomo.momo.util.k;
import com.immomo.momo.util.m;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Iterator;
import java.util.concurrent.ThreadPoolExecutor;
import mm.purchasesdk.PurchaseCode;
import org.json.JSONObject;

public class bh extends k implements bl {
    cd O = null;
    /* access modifiers changed from: private */
    public m P = new m("test_momo", "[ --- from SessionListActivity --- ]");
    /* access modifiers changed from: private */
    public ag Q = null;
    private RefreshOnOverScrollListView R = null;
    private HeaderLayout S = null;
    private View T = null;
    private TextView U = null;
    private ThreadPoolExecutor V = null;
    private bi W;
    /* access modifiers changed from: private */
    public ah X = null;
    /* access modifiers changed from: private */
    public aq Y = null;
    /* access modifiers changed from: private */
    public y Z = null;
    /* access modifiers changed from: private */
    public h aa = null;
    /* access modifiers changed from: private */
    public LoadingButton ab = null;
    /* access modifiers changed from: private */
    public int ac = 0;
    private boolean ad = false;
    private e ae = null;
    private w af = null;
    private g ag;
    /* access modifiers changed from: private */
    public Handler ah = new bi(this);
    private d ai;

    public bh() {
        com.immomo.momo.g.i();
        this.ai = new bp(this);
    }

    static /* synthetic */ void a(bh bhVar, String str) {
        if (!a.a((CharSequence) str)) {
            bhVar.X.c(new ax(str));
            bhVar.f(-1);
        }
    }

    private void a(Message message) {
        if (message.remoteUser == null) {
            message.remoteUser = this.Y.b(message.remoteId);
            if (message.remoteUser == null && !a.a((CharSequence) message.remoteId)) {
                this.V.execute(new bn(this, message));
            }
        }
    }

    private void a(ax axVar) {
        if (axVar.o == 1 && axVar.f > 5) {
            an anVar = new an(1003, "陌生人招呼过多，可设置不提醒或隐身");
            anVar.b();
            b(anVar);
        }
    }

    private void a(String str, int i) {
        ax axVar;
        f(i);
        if (!a.a((CharSequence) str)) {
            ax e = this.Q.e(str);
            if (e == null) {
                this.X.c(new ax(str));
                return;
            }
            int f = this.X.f(e);
            if (f >= 0) {
                axVar = (ax) this.X.c(f);
            } else if (this.X.getCount() <= 0 || !this.ab.isShown() || ((ax) this.X.getItem(this.X.getCount() - 1)).j <= e.j) {
                f = 0;
                axVar = null;
            } else {
                return;
            }
            if (e.o == 0) {
                b(e);
            } else if (e.o == 2) {
                c(e);
            } else if (e.o == 6) {
                d(e);
            }
            if (this.X.getCount() > 0 && f > 0 && (axVar == null || axVar.j != e.j)) {
                int i2 = -1;
                while (true) {
                    i2++;
                    if (i2 < f) {
                        if (((ax) this.X.getItem(i2)).j < e.j) {
                            f = i2;
                            break;
                        }
                    } else {
                        break;
                    }
                }
            }
            this.X.a(e, f);
            a(e);
        }
    }

    private void a(String str, String str2, int i) {
        int f = this.X.f(new ax(str));
        if (f >= 0) {
            ax axVar = (ax) this.X.getItem(f);
            this.P.a((Object) ("updateStatus....position=" + f + ", session=" + axVar));
            if (axVar.k != null && axVar.i.equals(str2) && axVar.k.status != 6) {
                axVar.k.status = i;
                this.X.notifyDataSetChanged();
            }
        }
    }

    private void a(String str, String[] strArr) {
        int f;
        if (a.f(str) && (f = this.X.f(new ax(str))) >= 0) {
            ax axVar = (ax) this.X.getItem(f);
            if (axVar.k == null) {
                return;
            }
            if (strArr.length > 0) {
                for (String equals : strArr) {
                    if (equals.equals(axVar.i)) {
                        axVar.k.status = 6;
                        this.X.notifyDataSetChanged();
                        return;
                    }
                }
            } else if (axVar.k.status == 2) {
                axVar.k.status = 6;
                this.X.notifyDataSetChanged();
            }
        }
    }

    private void al() {
        if (this.ac > 0) {
            this.W.a("忽略未读(" + this.ac + ")");
            this.W.setVisibility(0);
            return;
        }
        this.W.setVisibility(8);
    }

    private void am() {
        this.T = com.immomo.momo.g.o().inflate((int) R.layout.common_list_loadmore, (ViewGroup) null);
        this.ab = (LoadingButton) this.T.findViewById(R.id.btn_loadmore);
        this.ab.setNormalIconResId(R.drawable.ic_btn_inner_clock);
        this.ab.setOnProcessListener(this);
    }

    private void b(ax axVar) {
        axVar.b = null;
        if (axVar.m && axVar.b == null) {
            axVar.m = false;
            bf b = this.Y.b(axVar.f3000a);
            if (b != null) {
                axVar.b = b;
                return;
            }
            axVar.b = new bf(axVar.f3000a);
            this.V.execute(new bl(this, axVar));
        }
    }

    private void c(ax axVar) {
        if (axVar.c == null && axVar.m) {
            axVar.m = false;
            com.immomo.momo.service.bean.a.a e = this.Z.e(axVar.f3000a);
            if (e != null) {
                axVar.c = e;
            } else {
                this.V.execute(new bm(this, axVar));
            }
        }
        if (axVar.k != null && axVar.k.receive) {
            a(axVar.k);
        }
    }

    static /* synthetic */ ArrayList d(bh bhVar) {
        boolean z;
        ArrayList arrayList = (ArrayList) bhVar.Q.a(bhVar.X.getCount());
        if (arrayList.size() > 20) {
            arrayList.remove(arrayList.size() - 1);
            z = true;
        } else {
            z = false;
        }
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            ax axVar = (ax) it.next();
            if (axVar.o == 0) {
                bhVar.b(axVar);
            } else if (axVar.o == 2) {
                bhVar.c(axVar);
            } else if (axVar.o == 6) {
                bhVar.d(axVar);
            }
            bhVar.a(axVar);
        }
        bhVar.X.b((Collection) arrayList);
        if (z) {
            bhVar.ab.setVisibility(0);
        } else {
            bhVar.ab.setVisibility(8);
        }
        return arrayList;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.service.h.a(java.lang.String, boolean):com.immomo.momo.service.bean.n
     arg types: [java.lang.String, int]
     candidates:
      com.immomo.momo.service.h.a(com.immomo.momo.service.bean.n, java.lang.String):void
      com.immomo.momo.service.h.a(java.lang.String, int):void
      com.immomo.momo.service.h.a(java.lang.String, java.lang.String):void
      com.immomo.momo.service.h.a(java.lang.String, java.lang.String[]):void
      com.immomo.momo.service.h.a(java.util.List, java.lang.String):void
      com.immomo.momo.service.h.a(java.lang.String, boolean):com.immomo.momo.service.bean.n */
    private void d(ax axVar) {
        if (axVar.d == null && axVar.m) {
            axVar.m = false;
            n a2 = this.aa.a(axVar.f3000a, false);
            if (a2 != null) {
                axVar.d = a2;
            } else {
                this.V.execute(new bo(this, axVar));
            }
        }
        if (axVar.k != null && axVar.k.receive) {
            a(axVar.k);
        }
    }

    /* access modifiers changed from: private */
    public void f(int i) {
        if (i < 0) {
            this.ac = af.c().q();
        } else {
            this.ac = i;
        }
        if (this.ac > 0) {
            this.U.setText(new StringBuilder(String.valueOf(this.ac)).toString());
            this.U.setVisibility(0);
        } else {
            this.U.setVisibility(8);
        }
        al();
        int i2 = Calendar.getInstance().get(11);
        if (this.ac > 0 && i2 >= 0 && i2 <= 3) {
            an anVar = new an(1005, "夜间消息太多，可设置免打扰时段", 2147483646);
            anVar.b();
            b(anVar);
        }
    }

    /* access modifiers changed from: protected */
    public final int B() {
        return R.layout.activity_sessionlist;
    }

    /* access modifiers changed from: protected */
    public final void C() {
        this.S = (HeaderLayout) c((int) R.id.layout_header);
        this.W = new bi(com.immomo.momo.g.c());
        this.R = (RefreshOnOverScrollListView) c((int) R.id.listview);
        this.S = (HeaderLayout) c((int) R.id.layout_header);
        this.S.setTitleText((int) R.string.sessions);
        am();
        this.R.setListPaddingBottom(MaintabActivity.h);
        this.ag = new g(c(), 2);
        this.R.addHeaderView(this.ag.getWappview());
        if (dp.a(3)) {
            this.R.addHeaderView(new dp(c(), 3));
        }
        this.S.a(this.W, new bs(this));
    }

    /* access modifiers changed from: protected */
    public final void E() {
        super.E();
        if (this.ab.d()) {
            this.ab.e();
        }
    }

    public final boolean G() {
        if (!this.O.g()) {
            return false;
        }
        this.O.e();
        return true;
    }

    public final Handler P() {
        return this.ah;
    }

    public final void Q() {
        Iterator it = ((ArrayList) this.X.a()).iterator();
        while (it.hasNext()) {
            ax axVar = (ax) it.next();
            axVar.f = 0;
            axVar.g = 0;
        }
        this.ac = 0;
        this.U.setVisibility(8);
        com.immomo.momo.g.d().t();
        al();
        this.X.notifyDataSetChanged();
        new ca(this).start();
    }

    public final void a(View view) {
        super.a(view);
        if (view != null) {
            this.U = (TextView) view.findViewById(R.id.tab_item_tv_badge);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.service.bean.at.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, boolean]
     candidates:
      com.immomo.momo.service.bean.at.a(android.content.Context, java.lang.String):com.immomo.momo.service.bean.at
      com.immomo.momo.service.bean.at.a(java.lang.Integer, java.lang.Integer):void
      com.immomo.momo.service.bean.at.a(java.lang.String, java.util.Date):void
      com.immomo.momo.service.bean.at.a(java.lang.String, java.lang.Object):void */
    /* access modifiers changed from: protected */
    public final void a(an anVar) {
        if (anVar != null) {
            switch (anVar.a()) {
                case 1003:
                    a(com.immomo.momo.android.view.a.n.a(c(), (int) R.string.tips_hi, (int) R.string.tips_btn_goto, (int) R.string.tips_btn_nevermind, new bj(this), (DialogInterface.OnClickListener) null));
                    this.N.a("tips_" + anVar.a(), (Object) true);
                    c(anVar);
                    return;
                case 1004:
                    a(com.immomo.momo.android.view.a.n.a(c(), (int) R.string.tips_feed, (int) R.string.tips_btn_goto, (int) R.string.tips_btn_nevermind, new cb(this), (DialogInterface.OnClickListener) null));
                    this.N.a("tips_" + anVar.a(), (Object) true);
                    c(anVar);
                    return;
                case 1005:
                    a(com.immomo.momo.android.view.a.n.a(c(), (int) R.string.tips_night, (int) R.string.tips_btn_goto, (int) R.string.tips_btn_nevermind, new bk(this), (DialogInterface.OnClickListener) null));
                    this.N.a("tips_" + anVar.a(), (Object) true);
                    c(anVar);
                    return;
                default:
                    return;
            }
        }
    }

    public final boolean a(Bundle bundle, String str) {
        String string;
        if ("actions.usermessage".equals(str) || "actions.userlocalmsg".equals(str)) {
            a(bundle.getString("remoteuserid"), bundle.getInt("totalunreaded", -1));
        } else if ("actions.gmessage".equals(str) || "actions.glocalmsg".equals(str)) {
            a(bundle.getString("groupid"), bundle.getInt("totalunreaded", -1));
        } else if ("actions.discuss".equals(str) || "actions.dlocalmsg".equals(str)) {
            a(bundle.getString("discussid"), bundle.getInt("totalunreaded", -1));
        } else if ("actions.himessage".equals(str)) {
            a("-2222", bundle.getInt("totalunreaded", -1));
        } else if ("actions.contactnotice".equals(str)) {
            a("-2230", bundle.getInt("totalunreaded", -1));
        } else if ("actions.message.status".equals(str)) {
            String string2 = bundle.getString("stype");
            int i = bundle.getInt("chattype");
            this.P.a((Object) ("chatType=" + i + ", type=" + string2));
            if (i == 2) {
                string = bundle.getString("groupid");
            } else if (i == 1) {
                string = bundle.getString("remoteuserid");
            } else if (i != 3) {
                return false;
            } else {
                string = bundle.getString("discussid");
            }
            if ("msgreaded".equals(string2)) {
                a(string, bundle.getStringArray("msgid"));
            } else if ("msgsending".equals(string2)) {
                a(string, bundle.getString("msgid"), 1);
            } else if ("msgsuccess".equals(string2)) {
                a(string, bundle.getString("msgid"), 2);
            } else if ("msgfailed".equals(string2)) {
                a(string, bundle.getString("msgid"), 3);
            } else if ("msgdistance".equals(string2)) {
                String string3 = bundle.getString("msgid");
                int f = this.X.f(new ax(string));
                if (f >= 0) {
                    ax axVar = (ax) this.X.getItem(f);
                    if (axVar.k != null && axVar.i.equals(string3)) {
                        axVar.k.distance = (float) bundle.getInt("distance", -1);
                        this.X.notifyDataSetChanged();
                    }
                }
            }
        } else if ("action.sessionchanged".equals(str)) {
            a(bundle.getString("sessionid"), -1);
        } else if ("actions.logger".equals(str)) {
            a(bundle.getString("remoteuserid"), bundle.getInt("totalunreaded", -1));
        } else if ("action.syncfinished".equals(str)) {
            this.ad = true;
        } else if ("actions.userlocalmsg".equals(str)) {
            a(bundle.getString("remoteuserid"), bundle.getInt("totalunreaded", -1));
        } else if ("actions.frienddiscover".equals(str)) {
            a("-2240", bundle.getInt("totalunreaded", -1));
        }
        return super.a(bundle, str);
    }

    /* access modifiers changed from: protected */
    public final boolean aa() {
        return true;
    }

    public final void ae() {
        super.ae();
        if (!a.a((CharSequence) O().i)) {
            if ("sayhi".equals(O().i)) {
                a(new Intent(com.immomo.momo.g.c(), HiSessionListActivity.class));
            } else if ("groupnotice".equals(O().i)) {
                a(new Intent(com.immomo.momo.g.c(), GroupActionListFragment.class));
            }
            O().i = null;
            this.R.i();
        }
        if (this.ad) {
            am();
            aj();
            this.ad = false;
        }
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("number", this.ac);
            new k("PI", "P5", jSONObject).e();
            this.ag.g();
        } catch (Exception e) {
        }
    }

    public final void ag() {
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("number", this.ac);
            new k("PO", "P5", jSONObject).e();
            this.ag.f();
        } catch (Exception e) {
        }
    }

    public final void ai() {
        this.R.i();
    }

    public final void aj() {
        boolean z;
        this.P.a((Object) ("isInited=" + ak()));
        ArrayList arrayList = (ArrayList) this.Q.a(0);
        if (arrayList.size() > 20) {
            arrayList.remove(arrayList.size() - 1);
            z = true;
        } else {
            z = false;
        }
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            ax axVar = (ax) it.next();
            if (axVar.o == 0) {
                b(axVar);
            } else if (axVar.o == 2) {
                c(axVar);
            } else if (axVar.o == 6) {
                d(axVar);
            }
            a(axVar);
        }
        this.R.removeFooterView(this.T);
        this.R.addFooterView(this.T);
        this.X = new ah(this, arrayList, this.R);
        this.R.setAdapter((ListAdapter) this.X);
        if (!z) {
            this.ab.setVisibility(8);
        } else {
            this.ab.setVisibility(0);
        }
        f(-1);
        super.aj();
    }

    public final void c(Bundle bundle) {
        bundle.putBoolean("needReloadSessions", this.ad);
        super.c(bundle);
    }

    public final void g(Bundle bundle) {
        this.Q = new ag();
        this.Y = new aq();
        this.Z = new y();
        this.aa = new h();
        this.V = u.a();
        new c();
        this.O = new cd(c(), this.R);
        this.O.a((int) R.id.item_layout);
        this.R.setMultipleSelector(this.O);
        this.O.a((cf) new bv());
        this.O.a(new com.immomo.momo.android.view.a(com.immomo.momo.g.c()).a(), new bw(this));
        this.R.setOnItemClickListener(new bz(this));
        aj();
        a((int) PurchaseCode.BILL_DYMARK_CREATE_ERROR, b.b);
        a((int) PurchaseCode.BILL_DYMARK_CREATE_ERROR, "action.sessionchanged", "action.syncfinished", "actions.logger", "actions.userlocalmsg", "actions.glocalmsg", "actions.dlocalmsg");
        this.ae = new e(c());
        this.ae.a(new br(this));
        this.af = new w(c());
        this.af.a(this.ai);
    }

    public final void h(Bundle bundle) {
        this.ad = bundle.getBoolean("needReloadSessions", false);
        super.h(bundle);
    }

    public final void p() {
        super.p();
        a(this.ae);
        a(this.af);
    }

    public final void u() {
        a(new cc(this, (byte) 0));
    }
}
