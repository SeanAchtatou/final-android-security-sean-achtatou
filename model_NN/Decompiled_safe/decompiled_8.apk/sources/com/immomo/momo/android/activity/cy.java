package com.immomo.momo.android.activity;

import android.support.v4.b.a;
import com.immomo.momo.android.c.r;
import com.immomo.momo.service.bean.au;
import com.immomo.momo.util.j;

final class cy implements Runnable {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public /* synthetic */ EditUserProfileActivity f1212a;

    cy(EditUserProfileActivity editUserProfileActivity) {
        this.f1212a = editUserProfileActivity;
    }

    public final void run() {
        int size = this.f1212a.h.size();
        for (int i = 0; i < size; i++) {
            au auVar = (au) this.f1212a.h.get(i);
            if (!a.a((CharSequence) auVar.b)) {
                cz czVar = new cz(this, auVar);
                if (j.a(auVar.b) != null) {
                    czVar.a(j.a(auVar.b));
                } else {
                    new Thread(new r(auVar.b, czVar, 3, null)).start();
                }
            }
        }
    }
}
