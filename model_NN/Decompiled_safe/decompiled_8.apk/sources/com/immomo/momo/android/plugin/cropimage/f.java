package com.immomo.momo.android.plugin.cropimage;

import java.util.concurrent.CountDownLatch;

final class f implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ CropImageActivity f2585a;

    f(CropImageActivity cropImageActivity) {
        this.f2585a = cropImageActivity;
    }

    public final void run() {
        CountDownLatch countDownLatch = new CountDownLatch(1);
        this.f2585a.g.post(new g(this, this.f2585a.l, countDownLatch));
        try {
            countDownLatch.await();
            this.f2585a.d.run();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
