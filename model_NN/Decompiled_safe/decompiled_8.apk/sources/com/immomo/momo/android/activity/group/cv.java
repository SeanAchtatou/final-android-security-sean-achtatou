package com.immomo.momo.android.activity.group;

import android.content.Intent;
import android.os.Bundle;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.common.InviteSNSActivity;
import com.immomo.momo.android.view.a.al;
import com.immomo.momo.android.view.a.n;
import com.immomo.momo.g;
import com.immomo.momo.service.w;

final class cv implements al {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ GroupProfileActivity f1594a;

    cv(GroupProfileActivity groupProfileActivity) {
        this.f1594a = groupProfileActivity;
    }

    public final void a(int i) {
        switch (i) {
            case 0:
                if (this.f1594a.Y != null) {
                    this.f1594a.Y.f2978a = !this.f1594a.Y.f2978a;
                    this.f1594a.Y.a("group_ispush", Boolean.valueOf(this.f1594a.Y.f2978a));
                    w wVar = new w();
                    if (wVar.d(this.f1594a.l)) {
                        if (this.f1594a.Y.f2978a) {
                            wVar.a(this.f1594a.l, 5, 13);
                        } else {
                            wVar.a(this.f1594a.l, 13, 5);
                        }
                    }
                    Bundle bundle = new Bundle();
                    bundle.putString("sessionid", this.f1594a.l);
                    bundle.putInt("sessiontype", 2);
                    g.d().a(bundle, "action.sessionchanged");
                    return;
                }
                return;
            case 1:
                GroupProfileActivity.u(this.f1594a);
                return;
            case 2:
                n.a(this.f1594a, this.f1594a.getString(R.string.group_setting_quit_tip), new cx(this.f1594a)).show();
                return;
            case 3:
                Intent intent = new Intent(this.f1594a, InviteSNSActivity.class);
                intent.putExtra("invite_id", this.f1594a.l);
                intent.putExtra("invite_type", 2);
                this.f1594a.startActivity(intent);
                return;
            default:
                return;
        }
    }
}
