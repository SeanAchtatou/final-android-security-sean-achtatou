package com.immomo.momo.android.activity.common;

import android.content.Intent;
import android.support.v4.b.a;
import com.immomo.momo.android.broadcast.d;
import com.immomo.momo.android.broadcast.j;
import com.immomo.momo.service.bean.bf;

final class g implements d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ a f1120a;

    g(a aVar) {
        this.f1120a = aVar;
    }

    public final void a(Intent intent) {
        bf b;
        if (intent.getAction().equals(j.d) || intent.getAction().equals(j.f2353a)) {
            String stringExtra = intent.getStringExtra("key_momoid");
            if (!a.a((CharSequence) stringExtra) && "both".equals(intent.getStringExtra("relation")) && (b = this.f1120a.V.b(stringExtra)) != null && !this.f1120a.R.contains(b)) {
                this.f1120a.R.add(0, b);
                this.f1120a.P.notifyDataSetChanged();
            }
        }
    }
}
