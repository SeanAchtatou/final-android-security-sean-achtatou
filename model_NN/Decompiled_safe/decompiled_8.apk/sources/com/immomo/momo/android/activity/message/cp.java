package com.immomo.momo.android.activity.message;

import android.content.Context;
import android.content.Intent;
import com.immomo.momo.android.broadcast.t;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.h;

final class cp extends d {

    /* renamed from: a  reason: collision with root package name */
    private v f1975a = null;
    private String c = null;
    private /* synthetic */ MultiChatActivity d;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public cp(MultiChatActivity multiChatActivity, Context context, String str) {
        super(context);
        this.d = multiChatActivity;
        this.f1975a = new v(context);
        this.c = str;
        this.f1975a.setOnCancelListener(new cq(this));
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        String a2 = h.a().a(this.d.i, this.c);
        this.d.P.b = this.c;
        this.d.O.a(this.d.P);
        return a2;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
        this.d.a(this.f1975a);
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        super.a(exc);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        a((String) obj);
        this.d.ag();
        Intent intent = new Intent();
        intent.setAction(t.d);
        intent.putExtra("disid", this.d.i);
        this.d.sendBroadcast(intent);
    }

    /* access modifiers changed from: protected */
    public final void b() {
        super.b();
        this.d.p();
    }
}
