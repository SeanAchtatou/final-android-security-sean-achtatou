package com.immomo.momo.android.map;

import android.view.View;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.view.a.n;

final class bd implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ SelectSiteGoogleActivity f2483a;
    private final /* synthetic */ bo b;
    private final /* synthetic */ n c;

    bd(SelectSiteGoogleActivity selectSiteGoogleActivity, bo boVar, n nVar) {
        this.f2483a = selectSiteGoogleActivity;
        this.b = boVar;
        this.c = nVar;
    }

    public final void onClick(View view) {
        switch (view.getId()) {
            case R.id.layout_type_all /*2131166087*/:
                this.f2483a.u = 0;
                break;
            case R.id.layout_type_department /*2131166088*/:
                this.f2483a.u = 1;
                break;
            case R.id.layout_type_office /*2131166089*/:
                this.f2483a.u = 2;
                break;
            case R.id.layout_type_school /*2131166090*/:
                this.f2483a.u = 3;
                break;
            case R.id.layout_type_entertainment /*2131166091*/:
                this.f2483a.u = 4;
                break;
        }
        this.f2483a.v.setText(this.f2483a.d());
        this.f2483a.b.setText(PoiTypeDef.All);
        this.b.a(false);
        this.c.dismiss();
    }
}
