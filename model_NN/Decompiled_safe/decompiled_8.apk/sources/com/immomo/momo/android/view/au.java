package com.immomo.momo.android.view;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.b.a;
import com.immomo.momo.android.c.u;
import com.immomo.momo.util.m;
import com.jcraft.jzlib.GZIPHeader;
import com.mapabc.minimap.map.vmap.NativeMapEngine;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.Vector;
import java.util.concurrent.Semaphore;
import java.util.concurrent.ThreadPoolExecutor;

public final class au {
    /* access modifiers changed from: private */
    public static Semaphore D = new Semaphore(Runtime.getRuntime().availableProcessors());
    private static ThreadPoolExecutor E = new u(1, 1);
    private static Set I = new HashSet();
    private Bitmap A;
    private Bitmap B;
    private int C;
    /* access modifiers changed from: private */
    public m F;
    private aw G;
    /* access modifiers changed from: private */
    public int H;
    private byte[] J;
    private int K;
    private int L;
    private int M;
    private boolean N;
    private int O;
    private int P;
    private short[] Q;
    private byte[] R;
    private byte[] S;
    private byte[] T;
    private Vector U;
    private int V;
    private aw W;
    private File X;
    /* access modifiers changed from: private */
    public ay Y;

    /* renamed from: a  reason: collision with root package name */
    protected int f2729a;
    protected int b;
    protected int c;
    boolean d;
    private InputStream e;
    private boolean f;
    private int g;
    private int h;
    private int i;
    private int[] j;
    private int[] k;
    private int[] l;
    private int m;
    private int n;
    private int o;
    private boolean p;
    private boolean q;
    private int r;
    private int s;
    private int t;
    private int u;
    private int v;
    private int w;
    private int x;
    private int y;
    private int z;

    public au() {
        this.f2729a = 4;
        this.h = 0;
        this.i = 0;
        this.C = 0;
        this.F = new m(this);
        this.d = false;
        this.G = null;
        this.H = -1;
        this.J = new byte[1024];
        this.K = 0;
        this.L = 0;
        this.M = 0;
        this.N = false;
        this.O = 0;
        this.W = null;
        this.X = null;
        this.Y = null;
        I.add(this);
    }

    public au(int i2) {
        this();
        this.H = i2;
    }

    private int[] c(int i2) {
        int i3;
        int i4 = 0;
        int i5 = i2 * 3;
        int[] iArr = null;
        byte[] bArr = new byte[i5];
        try {
            i3 = this.e.read(bArr);
        } catch (Exception e2) {
            this.F.a((Throwable) e2);
            i3 = 0;
        }
        if (i3 < i5) {
            this.f2729a = 1;
        } else {
            iArr = new int[256];
            int i6 = 0;
            while (i6 < i2) {
                int i7 = i4 + 1;
                byte b2 = bArr[i4] & GZIPHeader.OS_UNKNOWN;
                int i8 = i7 + 1;
                iArr[i6] = (b2 << 16) | -16777216 | ((bArr[i7] & GZIPHeader.OS_UNKNOWN) << 8) | (bArr[i8] & GZIPHeader.OS_UNKNOWN);
                i6++;
                i4 = i8 + 1;
            }
        }
        return iArr;
    }

    public static void k() {
        while (true) {
            Runnable poll = E.getQueue().poll();
            if (poll != null) {
                ((ax) poll).f2732a.f2729a = 4;
            } else {
                return;
            }
        }
    }

    public static void m() {
        ArrayList arrayList = new ArrayList();
        arrayList.addAll(I);
        I.clear();
        E.execute(new av(arrayList));
    }

    private void o() {
        this.U.size();
    }

    private boolean p() {
        return this.f2729a == 2 || this.f2729a == 1;
    }

    private int q() {
        try {
            return this.e.read();
        } catch (Exception e2) {
            this.f2729a = 1;
            return 0;
        }
    }

    private int r() {
        this.K = q();
        int i2 = 0;
        if (this.K > 0) {
            while (i2 < this.K) {
                try {
                    int read = this.e.read(this.J, i2, this.K - i2);
                    if (read == -1) {
                        break;
                    }
                    i2 += read;
                } catch (Exception e2) {
                    this.F.a((Throwable) e2);
                }
            }
            if (i2 < this.K) {
                this.f2729a = 1;
            }
        }
        return i2;
    }

    /* JADX WARN: Failed to insert an additional move for type inference into block B:180:0x010c */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:186:0x010c */
    /* JADX WARN: Type inference failed for: r7v2 */
    /* JADX WARN: Type inference failed for: r6v2 */
    /* JADX WARN: Type inference failed for: r7v5 */
    /* JADX WARN: Type inference failed for: r7v8, types: [int] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:115:0x0345 A[LOOP:3: B:45:0x0111->B:115:0x0345, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:172:0x0003 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x011e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void s() {
        /*
            r25 = this;
            r1 = 0
            r16 = r1
        L_0x0003:
            if (r16 != 0) goto L_0x000b
            boolean r1 = r25.p()
            if (r1 == 0) goto L_0x000c
        L_0x000b:
            return
        L_0x000c:
            int r1 = r25.q()
            switch(r1) {
                case 0: goto L_0x0003;
                case 33: goto L_0x03e2;
                case 44: goto L_0x0019;
                case 59: goto L_0x0461;
                default: goto L_0x0013;
            }
        L_0x0013:
            r1 = 1
            r0 = r25
            r0.f2729a = r1
            goto L_0x0003
        L_0x0019:
            int r1 = r25.v()
            r0 = r25
            r0.s = r1
            int r1 = r25.v()
            r0 = r25
            r0.t = r1
            int r1 = r25.v()
            r0 = r25
            r0.u = r1
            int r1 = r25.v()
            r0 = r25
            r0.v = r1
            int r2 = r25.q()
            r1 = r2 & 128(0x80, float:1.794E-43)
            if (r1 == 0) goto L_0x0239
            r1 = 1
        L_0x0042:
            r0 = r25
            r0.p = r1
            r1 = r2 & 64
            if (r1 == 0) goto L_0x023c
            r1 = 1
        L_0x004b:
            r0 = r25
            r0.q = r1
            r1 = 2
            r2 = r2 & 7
            int r1 = r1 << r2
            r0 = r25
            r0.r = r1
            r0 = r25
            boolean r1 = r0.p
            if (r1 == 0) goto L_0x023f
            r0 = r25
            int r1 = r0.r
            r0 = r25
            int[] r1 = r0.c(r1)
            r0 = r25
            r0.k = r1
            r0 = r25
            int[] r1 = r0.k
            r0 = r25
            r0.l = r1
        L_0x0073:
            r1 = 0
            r0 = r25
            boolean r2 = r0.N
            if (r2 == 0) goto L_0x008f
            r0 = r25
            int[] r1 = r0.l
            r0 = r25
            int r2 = r0.P
            r1 = r1[r2]
            r0 = r25
            int[] r2 = r0.l
            r0 = r25
            int r3 = r0.P
            r4 = 0
            r2[r3] = r4
        L_0x008f:
            r9 = r1
            r0 = r25
            int[] r1 = r0.l
            if (r1 != 0) goto L_0x009b
            r1 = 1
            r0 = r25
            r0.f2729a = r1
        L_0x009b:
            boolean r1 = r25.p()
            if (r1 != 0) goto L_0x0003
            r0 = r25
            int r1 = r0.u
            r0 = r25
            int r2 = r0.v
            int r17 = r1 * r2
            r0 = r25
            byte[] r1 = r0.T
            if (r1 == 0) goto L_0x00ba
            r0 = r25
            byte[] r1 = r0.T
            int r1 = r1.length
            r0 = r17
            if (r1 >= r0) goto L_0x00c2
        L_0x00ba:
            r0 = r17
            byte[] r1 = new byte[r0]
            r0 = r25
            r0.T = r1
        L_0x00c2:
            r0 = r25
            short[] r1 = r0.Q
            if (r1 != 0) goto L_0x00d0
            r1 = 4096(0x1000, float:5.74E-42)
            short[] r1 = new short[r1]
            r0 = r25
            r0.Q = r1
        L_0x00d0:
            r0 = r25
            byte[] r1 = r0.R
            if (r1 != 0) goto L_0x00de
            r1 = 4096(0x1000, float:5.74E-42)
            byte[] r1 = new byte[r1]
            r0 = r25
            r0.R = r1
        L_0x00de:
            r0 = r25
            byte[] r1 = r0.S
            if (r1 != 0) goto L_0x00ec
            r1 = 4097(0x1001, float:5.741E-42)
            byte[] r1 = new byte[r1]
            r0 = r25
            r0.S = r1
        L_0x00ec:
            int r18 = r25.q()
            r1 = 1
            int r19 = r1 << r18
            int r20 = r19 + 1
            int r11 = r19 + 2
            r10 = -1
            int r3 = r18 + 1
            r1 = 1
            int r1 = r1 << r3
            int r4 = r1 + -1
            r1 = 0
        L_0x00ff:
            r0 = r19
            if (r1 < r0) goto L_0x0258
            r13 = 0
            r1 = 0
            r5 = r13
            r6 = r13
            r15 = r1
            r2 = r13
            r7 = r13
            r12 = r13
            r1 = r13
        L_0x010c:
            r0 = r17
            if (r15 < r0) goto L_0x026a
        L_0x0110:
            r1 = r13
        L_0x0111:
            r0 = r17
            if (r1 < r0) goto L_0x0345
            r25.w()
            boolean r1 = r25.p()
            if (r1 != 0) goto L_0x0003
            r0 = r25
            int r1 = r0.V
            int r1 = r1 + 1
            r0 = r25
            r0.V = r1
            r0 = r25
            int r1 = r0.b
            r0 = r25
            int r2 = r0.c
            android.graphics.Bitmap$Config r3 = android.graphics.Bitmap.Config.RGB_565
            android.graphics.Bitmap r1 = android.graphics.Bitmap.createBitmap(r1, r2, r3)
            r0 = r25
            r0.A = r1
            r0 = r25
            int r1 = r0.b
            r0 = r25
            int r2 = r0.c
            int r1 = r1 * r2
            int[] r2 = new int[r1]
            r0 = r25
            int r1 = r0.M
            if (r1 <= 0) goto L_0x01aa
            r0 = r25
            int r1 = r0.M
            r3 = 3
            if (r1 != r3) goto L_0x0175
            r0 = r25
            int r1 = r0.V
            int r1 = r1 + -2
            if (r1 <= 0) goto L_0x0350
            int r3 = r1 + -1
            r1 = 0
            if (r3 < 0) goto L_0x0171
            r0 = r25
            int r4 = r0.V
            if (r3 >= r4) goto L_0x0171
            r0 = r25
            java.util.Vector r1 = r0.U
            java.lang.Object r1 = r1.elementAt(r3)
            com.immomo.momo.android.view.aw r1 = (com.immomo.momo.android.view.aw) r1
            android.graphics.Bitmap r1 = r1.f2731a
        L_0x0171:
            r0 = r25
            r0.B = r1
        L_0x0175:
            r0 = r25
            android.graphics.Bitmap r1 = r0.B
            if (r1 == 0) goto L_0x01aa
            r0 = r25
            android.graphics.Bitmap r1 = r0.B
            r3 = 0
            r0 = r25
            int r4 = r0.b
            r5 = 0
            r6 = 0
            r0 = r25
            int r7 = r0.b
            r0 = r25
            int r8 = r0.c
            r1.getPixels(r2, r3, r4, r5, r6, r7, r8)
            r0 = r25
            int r1 = r0.M
            r3 = 2
            if (r1 != r3) goto L_0x01aa
            r1 = 0
            r0 = r25
            boolean r3 = r0.N
            if (r3 != 0) goto L_0x01a3
            r0 = r25
            int r1 = r0.o
        L_0x01a3:
            r3 = 0
        L_0x01a4:
            r0 = r25
            int r4 = r0.z
            if (r3 < r4) goto L_0x0357
        L_0x01aa:
            r5 = 1
            r4 = 8
            r3 = 0
            r1 = 0
        L_0x01af:
            r0 = r25
            int r6 = r0.v
            if (r1 < r6) goto L_0x0376
            r0 = r25
            int r1 = r0.b
            r0 = r25
            int r3 = r0.c
            android.graphics.Bitmap$Config r4 = android.graphics.Bitmap.Config.ARGB_4444
            android.graphics.Bitmap r1 = android.graphics.Bitmap.createBitmap(r2, r1, r3, r4)
            r0 = r25
            r0.A = r1
            r0 = r25
            java.util.Vector r1 = r0.U
            com.immomo.momo.android.view.aw r2 = new com.immomo.momo.android.view.aw
            r0 = r25
            android.graphics.Bitmap r3 = r0.A
            r0 = r25
            int r4 = r0.O
            r2.<init>(r3, r4)
            r1.addElement(r2)
            r0 = r25
            boolean r1 = r0.N
            if (r1 == 0) goto L_0x01eb
            r0 = r25
            int[] r1 = r0.l
            r0 = r25
            int r2 = r0.P
            r1[r2] = r9
        L_0x01eb:
            r0 = r25
            int r1 = r0.L
            r0 = r25
            r0.M = r1
            r0 = r25
            int r1 = r0.s
            r0 = r25
            r0.w = r1
            r0 = r25
            int r1 = r0.t
            r0 = r25
            r0.x = r1
            r0 = r25
            int r1 = r0.u
            r0 = r25
            r0.y = r1
            r0 = r25
            int r1 = r0.v
            r0 = r25
            r0.z = r1
            r0 = r25
            android.graphics.Bitmap r1 = r0.A
            r0 = r25
            r0.B = r1
            r0 = r25
            int r1 = r0.n
            r0 = r25
            r0.o = r1
            r1 = 0
            r0 = r25
            r0.L = r1
            r1 = 0
            r0 = r25
            r0.N = r1
            r1 = 0
            r0 = r25
            r0.O = r1
            r1 = 0
            r0 = r25
            r0.k = r1
            goto L_0x0003
        L_0x0239:
            r1 = 0
            goto L_0x0042
        L_0x023c:
            r1 = 0
            goto L_0x004b
        L_0x023f:
            r0 = r25
            int[] r1 = r0.j
            r0 = r25
            r0.l = r1
            r0 = r25
            int r1 = r0.m
            r0 = r25
            int r2 = r0.P
            if (r1 != r2) goto L_0x0073
            r1 = 0
            r0 = r25
            r0.n = r1
            goto L_0x0073
        L_0x0258:
            r0 = r25
            short[] r2 = r0.Q
            r5 = 0
            r2[r1] = r5
            r0 = r25
            byte[] r2 = r0.R
            byte r5 = (byte) r1
            r2[r1] = r5
            int r1 = r1 + 1
            goto L_0x00ff
        L_0x026a:
            if (r5 != 0) goto L_0x046d
            if (r7 >= r3) goto L_0x0289
            if (r2 != 0) goto L_0x0277
            int r2 = r25.r()
            if (r2 <= 0) goto L_0x0110
            r1 = 0
        L_0x0277:
            r0 = r25
            byte[] r8 = r0.J
            byte r8 = r8[r1]
            r8 = r8 & 255(0xff, float:3.57E-43)
            int r8 = r8 << r7
            int r6 = r6 + r8
            int r7 = r7 + 8
            int r1 = r1 + 1
            int r2 = r2 + -1
            goto L_0x010c
        L_0x0289:
            r8 = r6 & r4
            int r6 = r6 >> r3
            int r7 = r7 - r3
            if (r8 > r11) goto L_0x0110
            r0 = r20
            if (r8 == r0) goto L_0x0110
            r0 = r19
            if (r8 != r0) goto L_0x02a2
            int r3 = r18 + 1
            r4 = 1
            int r4 = r4 << r3
            int r4 = r4 + -1
            int r11 = r19 + 2
            r10 = -1
            goto L_0x010c
        L_0x02a2:
            r14 = -1
            if (r10 != r14) goto L_0x02b8
            r0 = r25
            byte[] r12 = r0.S
            int r10 = r5 + 1
            r0 = r25
            byte[] r14 = r0.R
            byte r14 = r14[r8]
            r12[r5] = r14
            r5 = r10
            r12 = r8
            r10 = r8
            goto L_0x010c
        L_0x02b8:
            if (r8 != r11) goto L_0x0469
            r0 = r25
            byte[] r0 = r0.S
            r21 = r0
            int r14 = r5 + 1
            byte r12 = (byte) r12
            r21[r5] = r12
            r12 = r10
        L_0x02c6:
            r0 = r19
            if (r12 > r0) goto L_0x032b
            r0 = r25
            byte[] r5 = r0.R
            byte r5 = r5[r12]
            r12 = r5 & 255(0xff, float:3.57E-43)
            r5 = 4096(0x1000, float:5.74E-42)
            if (r11 >= r5) goto L_0x0110
            r0 = r25
            byte[] r0 = r0.S
            r21 = r0
            int r5 = r14 + 1
            byte r0 = (byte) r12
            r22 = r0
            r21[r14] = r22
            r0 = r25
            short[] r14 = r0.Q
            short r10 = (short) r10
            r14[r11] = r10
            r0 = r25
            byte[] r10 = r0.R
            byte r14 = (byte) r12
            r10[r11] = r14
            int r10 = r11 + 1
            r11 = r10 & r4
            if (r11 != 0) goto L_0x02fe
            r11 = 4096(0x1000, float:5.74E-42)
            if (r10 >= r11) goto L_0x02fe
            int r3 = r3 + 1
            int r4 = r4 + r10
        L_0x02fe:
            r11 = r12
            r23 = r6
            r6 = r8
            r8 = r4
            r4 = r23
            r24 = r3
            r3 = r5
            r5 = r7
            r7 = r24
        L_0x030b:
            int r12 = r3 + -1
            r0 = r25
            byte[] r14 = r0.T
            int r3 = r13 + 1
            r0 = r25
            byte[] r0 = r0.S
            r21 = r0
            byte r21 = r21[r12]
            r14[r13] = r21
            int r13 = r15 + 1
            r15 = r13
            r13 = r3
            r3 = r7
            r7 = r5
            r5 = r12
            r12 = r11
            r11 = r10
            r10 = r6
            r6 = r4
            r4 = r8
            goto L_0x010c
        L_0x032b:
            r0 = r25
            byte[] r0 = r0.S
            r21 = r0
            int r5 = r14 + 1
            r0 = r25
            byte[] r0 = r0.R
            r22 = r0
            byte r22 = r22[r12]
            r21[r14] = r22
            r0 = r25
            short[] r14 = r0.Q
            short r12 = r14[r12]
            r14 = r5
            goto L_0x02c6
        L_0x0345:
            r0 = r25
            byte[] r2 = r0.T
            r3 = 0
            r2[r1] = r3
            int r1 = r1 + 1
            goto L_0x0111
        L_0x0350:
            r1 = 0
            r0 = r25
            r0.B = r1
            goto L_0x0175
        L_0x0357:
            r0 = r25
            int r4 = r0.x
            int r4 = r4 + r3
            r0 = r25
            int r5 = r0.b
            int r4 = r4 * r5
            r0 = r25
            int r5 = r0.w
            int r4 = r4 + r5
            r0 = r25
            int r5 = r0.y
            int r5 = r5 + r4
        L_0x036b:
            if (r4 < r5) goto L_0x0371
            int r3 = r3 + 1
            goto L_0x01a4
        L_0x0371:
            r2[r4] = r1
            int r4 = r4 + 1
            goto L_0x036b
        L_0x0376:
            r0 = r25
            boolean r6 = r0.q
            if (r6 == 0) goto L_0x0466
            r0 = r25
            int r6 = r0.v
            if (r3 < r6) goto L_0x0387
            int r5 = r5 + 1
            switch(r5) {
                case 2: goto L_0x03c1;
                case 3: goto L_0x03c3;
                case 4: goto L_0x03c6;
                default: goto L_0x0387;
            }
        L_0x0387:
            int r6 = r3 + r4
            r23 = r3
            r3 = r6
            r6 = r23
        L_0x038e:
            r0 = r25
            int r7 = r0.t
            int r6 = r6 + r7
            r0 = r25
            int r7 = r0.c
            if (r6 >= r7) goto L_0x03bd
            r0 = r25
            int r7 = r0.b
            int r7 = r7 * r6
            r0 = r25
            int r6 = r0.s
            int r8 = r7 + r6
            r0 = r25
            int r6 = r0.u
            int r6 = r6 + r8
            r0 = r25
            int r10 = r0.b
            int r10 = r10 + r7
            if (r10 >= r6) goto L_0x03b5
            r0 = r25
            int r6 = r0.b
            int r6 = r6 + r7
        L_0x03b5:
            r0 = r25
            int r7 = r0.u
            int r7 = r7 * r1
            r10 = r8
        L_0x03bb:
            if (r10 < r6) goto L_0x03c9
        L_0x03bd:
            int r1 = r1 + 1
            goto L_0x01af
        L_0x03c1:
            r3 = 4
            goto L_0x0387
        L_0x03c3:
            r3 = 2
            r4 = 4
            goto L_0x0387
        L_0x03c6:
            r3 = 1
            r4 = 2
            goto L_0x0387
        L_0x03c9:
            r0 = r25
            byte[] r11 = r0.T
            int r8 = r7 + 1
            byte r7 = r11[r7]
            r7 = r7 & 255(0xff, float:3.57E-43)
            r0 = r25
            int[] r11 = r0.l
            r7 = r11[r7]
            if (r7 == 0) goto L_0x03dd
            r2[r10] = r7
        L_0x03dd:
            int r7 = r10 + 1
            r10 = r7
            r7 = r8
            goto L_0x03bb
        L_0x03e2:
            int r1 = r25.q()
            switch(r1) {
                case 249: goto L_0x03ee;
                case 255: goto L_0x042a;
                default: goto L_0x03e9;
            }
        L_0x03e9:
            r25.w()
            goto L_0x0003
        L_0x03ee:
            r25.q()
            int r1 = r25.q()
            r2 = r1 & 28
            int r2 = r2 >> 2
            r0 = r25
            r0.L = r2
            r0 = r25
            int r2 = r0.L
            if (r2 != 0) goto L_0x0408
            r2 = 1
            r0 = r25
            r0.L = r2
        L_0x0408:
            r1 = r1 & 1
            if (r1 == 0) goto L_0x0428
            r1 = 1
        L_0x040d:
            r0 = r25
            r0.N = r1
            int r1 = r25.v()
            int r1 = r1 * 10
            r0 = r25
            r0.O = r1
            int r1 = r25.q()
            r0 = r25
            r0.P = r1
            r25.q()
            goto L_0x0003
        L_0x0428:
            r1 = 0
            goto L_0x040d
        L_0x042a:
            r25.r()
            java.lang.String r2 = ""
            r1 = 0
        L_0x0430:
            r3 = 11
            if (r1 < r3) goto L_0x0441
            java.lang.String r1 = "NETSCAPE2.0"
            boolean r1 = r2.equals(r1)
            if (r1 == 0) goto L_0x045c
            r25.u()
            goto L_0x0003
        L_0x0441:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r2 = java.lang.String.valueOf(r2)
            r3.<init>(r2)
            r0 = r25
            byte[] r2 = r0.J
            byte r2 = r2[r1]
            char r2 = (char) r2
            java.lang.StringBuilder r2 = r3.append(r2)
            java.lang.String r2 = r2.toString()
            int r1 = r1 + 1
            goto L_0x0430
        L_0x045c:
            r25.w()
            goto L_0x0003
        L_0x0461:
            r1 = 1
            r16 = r1
            goto L_0x0003
        L_0x0466:
            r6 = r1
            goto L_0x038e
        L_0x0469:
            r14 = r5
            r12 = r8
            goto L_0x02c6
        L_0x046d:
            r8 = r4
            r4 = r6
            r6 = r10
            r10 = r11
            r11 = r12
            r23 = r5
            r5 = r7
            r7 = r3
            r3 = r23
            goto L_0x030b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.immomo.momo.android.view.au.s():void");
    }

    private void t() {
        boolean z2 = true;
        char[] cArr = new char[10];
        for (int i2 = 0; i2 < 6; i2++) {
            cArr[i2] = (char) q();
        }
        if (cArr[0] == 'G' && cArr[1] == 'I' && cArr[2] == 'F') {
            this.H = 1;
            this.b = v();
            this.c = v();
            int q2 = q();
            if ((q2 & NativeMapEngine.MAX_ICON_SIZE) == 0) {
                z2 = false;
            }
            this.f = z2;
            this.g = 2 << (q2 & 7);
            this.m = q();
            q();
            if (this.f && !p()) {
                this.j = c(this.g);
                this.n = this.j[this.m];
            }
        } else if (cArr[1] == 'P' && cArr[2] == 'N' && cArr[3] == 'G') {
            this.H = 2;
        } else {
            for (int i3 = 6; i3 < 10; i3++) {
                cArr[i3] = (char) q();
            }
            if (cArr[6] == 'J' && cArr[7] == 'F' && cArr[8] == 'I' && cArr[9] == 'F') {
                this.H = 3;
            } else {
                this.f2729a = 1;
            }
        }
    }

    private void u() {
        do {
            r();
            if (this.J[0] == 1) {
                byte[] bArr = this.J;
                byte[] bArr2 = this.J;
            }
            if (this.K <= 0) {
                return;
            }
        } while (!p());
    }

    private int v() {
        return q() | (q() << 8);
    }

    private void w() {
        do {
            r();
            if (this.K <= 0) {
                return;
            }
        } while (!p());
    }

    public final int a() {
        return this.C;
    }

    public final int a(int i2) {
        this.O = -1;
        if (i2 >= 0 && i2 < this.U.size()) {
            this.O = ((aw) this.U.elementAt(i2)).b;
        }
        return this.O;
    }

    public final int a(InputStream inputStream) {
        this.f2729a = 3;
        this.V = 0;
        this.U = new Vector();
        this.j = null;
        this.k = null;
        if (inputStream != null) {
            this.e = inputStream;
            t();
            if (!p()) {
                switch (this.H) {
                    case 1:
                        s();
                        o();
                        if (this.V >= 0) {
                            this.f2729a = 0;
                            break;
                        } else {
                            this.f2729a = 1;
                            break;
                        }
                    case 2:
                    case 3:
                        this.e.close();
                        this.e = new FileInputStream(this.X);
                        try {
                            this.A = BitmapFactory.decodeStream(this.e);
                            if (this.A != null) {
                                this.U.addElement(new aw(this.A, this.O));
                                this.b = this.A.getWidth();
                                this.c = this.A.getHeight();
                                o();
                                c();
                                this.f2729a = 0;
                            } else {
                                this.f2729a = 1;
                            }
                            break;
                        } finally {
                            a.a((Closeable) this.e);
                        }
                    default:
                        this.f2729a = 2;
                        break;
                }
            }
        } else {
            this.f2729a = 2;
        }
        try {
            inputStream.close();
        } catch (Exception e2) {
            this.F.a((Throwable) e2);
        }
        return this.f2729a;
    }

    public final void a(ay ayVar) {
        this.Y = ayVar;
    }

    public final void a(File file, ay ayVar) {
        this.X = file;
        this.d = false;
        this.Y = ayVar;
        this.f2729a = 3;
        try {
            E.execute(new ax(this, file, this));
        } catch (Exception e2) {
            this.f2729a = 2;
        }
    }

    public final int b() {
        return this.H;
    }

    public final void b(int i2) {
        this.h = i2;
    }

    public final void c() {
        this.d = true;
        Vector vector = this.U;
        if (vector == null) {
            this.B = null;
            return;
        }
        if (vector.size() > 0) {
            this.W = (aw) vector.elementAt(0);
            for (int i2 = 1; i2 < vector.size(); i2++) {
                aw awVar = (aw) vector.elementAt(i2);
                if (!(awVar == null || awVar.f2731a == null)) {
                    awVar.f2731a.recycle();
                    awVar.f2731a = null;
                }
            }
        }
        vector.clear();
        this.B = null;
    }

    public final boolean d() {
        return this.h != 0 && this.h == this.i;
    }

    public final void e() {
        if (d()) {
            this.G = this.W;
        } else if (this.f2729a == 0 && !this.U.isEmpty()) {
            this.C++;
            if (this.C > this.U.size() - 1) {
                this.C = 0;
                this.i++;
            }
            try {
                this.G = (aw) this.U.elementAt(this.C);
            } catch (Exception e2) {
            }
        }
    }

    public final File f() {
        return this.X;
    }

    public final int g() {
        return this.f2729a;
    }

    public final boolean h() {
        return this.f2729a == 0;
    }

    public final void i() {
        this.C = 0;
        this.i = 0;
    }

    public final Bitmap j() {
        if (this.G != null) {
            return this.G.f2731a;
        }
        if (this.W != null) {
            return this.W.f2731a;
        }
        return null;
    }

    public final void l() {
        Bitmap bitmap;
        c();
        if (!(this.G == null || (bitmap = this.G.f2731a) == null || bitmap.isRecycled())) {
            bitmap.recycle();
        }
        this.G = null;
        if (this.B != null && !this.B.isRecycled()) {
            this.B.recycle();
        }
        this.B = null;
        if (this.A != null && !this.A.isRecycled()) {
            this.A.recycle();
        }
        this.A = null;
        if (!(this.W == null || this.W.f2731a == null || this.W.f2731a.isRecycled())) {
            this.W.f2731a.recycle();
        }
        this.W = null;
    }
}
