package com.immomo.momo.android.view;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import com.immomo.momo.R;
import com.immomo.momo.android.a.bm;
import com.immomo.momo.android.activity.emotestore.EmotionProfileActivity;
import com.immomo.momo.android.broadcast.d;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.at;
import com.immomo.momo.service.bean.bd;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.service.bean.q;
import com.immomo.momo.service.bean.t;
import com.immomo.momo.service.o;
import com.immomo.momo.util.j;
import com.immomo.momo.util.m;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EmoteInputView extends FrameLayout implements View.OnClickListener, d {

    /* renamed from: a  reason: collision with root package name */
    private bm f2626a = null;
    private bm b = null;
    private Map c = new HashMap();
    private View d = null;
    /* access modifiers changed from: private */
    public GridView e = null;
    /* access modifiers changed from: private */
    public EmoteEditeText f = null;
    private HorizontalScrollView g = null;
    private ViewGroup h = null;
    /* access modifiers changed from: private */
    public View i;
    /* access modifiers changed from: private */
    public View j;
    /* access modifiers changed from: private */
    public View k = null;
    private int l = 2;
    /* access modifiers changed from: private */
    public boolean m = false;
    private Runnable n = null;
    /* access modifiers changed from: private */
    public an o = null;
    private int p = 5;
    private boolean q = true;
    private List r = null;
    private o s = new o();
    /* access modifiers changed from: private */
    public m t = new m(this);
    private com.immomo.momo.android.broadcast.o u = null;
    /* access modifiers changed from: private */
    public bf v;
    /* access modifiers changed from: private */
    public at w;
    /* access modifiers changed from: private */
    public Handler x = new ae(this);

    public EmoteInputView(Context context) {
        super(context);
        c();
    }

    public EmoteInputView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        c();
    }

    public EmoteInputView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        c();
    }

    private void a(View view) {
        if (view != this.i) {
            if (view == this.j) {
                if (this.b == null) {
                    this.b = new bm(getContext());
                }
                this.e.setColumnWidth(getResources().getDimensionPixelSize(R.dimen.momoemote_item_width));
                this.e.setAdapter((ListAdapter) this.b);
                if (this.i != null) {
                    this.i.setSelected(false);
                }
                view.setSelected(true);
                this.i = view;
                e();
            } else if (view == this.k) {
                if (this.f2626a == null) {
                    t tVar = new t();
                    tVar.v = bd.b().c();
                    this.f2626a = new bm(getContext(), tVar);
                }
                this.e.setColumnWidth(getResources().getDimensionPixelSize(R.dimen.momogifemote_item_width));
                this.e.setAdapter((ListAdapter) this.f2626a);
                if (this.i != null) {
                    this.i.setSelected(false);
                }
                view.setSelected(true);
                this.i = view;
                e();
            } else {
                q qVar = (q) view.getTag();
                if (qVar.t) {
                    if (this.d.isShown()) {
                        this.d.setAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.push_right_out));
                    }
                    this.d.setVisibility(8);
                    bm bmVar = (bm) this.c.get(qVar.f3035a);
                    if (bmVar == null) {
                        bmVar = new bm(getContext(), qVar);
                        this.c.put(qVar.f3035a, bmVar);
                    }
                    this.e.setColumnWidth(getResources().getDimensionPixelSize(R.dimen.momogifemote_item_width));
                    this.e.setAdapter((ListAdapter) bmVar);
                    view.setSelected(true);
                    if (this.i != null) {
                        this.i.setSelected(false);
                    }
                    this.i = view;
                    return;
                }
                Intent intent = new Intent(getContext(), EmotionProfileActivity.class);
                intent.putExtra("eid", qVar.f3035a);
                getContext().startActivity(intent);
            }
        }
    }

    static /* synthetic */ void a(EmoteInputView emoteInputView, MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case 0:
                emoteInputView.l = 1;
                emoteInputView.m = false;
                emoteInputView.n = new al(emoteInputView);
                emoteInputView.d.postDelayed(emoteInputView.n, (long) ViewConfiguration.getTapTimeout());
                emoteInputView.x.sendEmptyMessage(338);
                return;
            case 1:
            case 3:
                emoteInputView.m = false;
                if (emoteInputView.n != null) {
                    emoteInputView.d.removeCallbacks(emoteInputView.n);
                    emoteInputView.n = null;
                    break;
                }
                break;
            case 2:
                int x2 = (int) motionEvent.getX();
                int y = (int) motionEvent.getY();
                if ((x2 < 0 || x2 >= emoteInputView.d.getWidth() + 0 || y < 0 || y >= emoteInputView.d.getHeight() + 0) && emoteInputView.l == 1) {
                    emoteInputView.d.removeCallbacks(emoteInputView.n);
                    emoteInputView.n = null;
                    emoteInputView.m = false;
                    break;
                } else {
                    return;
                }
            default:
                return;
        }
        emoteInputView.l = 2;
    }

    private void c() {
        this.v = g.q();
        this.w = g.r();
        inflate(getContext(), R.layout.common_emotionbar, this);
        this.d = findViewById(R.id.emotionbar_iv_delete);
        this.e = (GridView) findViewById(R.id.emotionbar_gridview);
        this.g = (HorizontalScrollView) findViewById(R.id.emotionbar_scrollview_emotions);
        this.h = (ViewGroup) this.g.findViewById(R.id.emotionbar_layout_emotions);
        this.j = this.g.findViewById(R.id.emotionbar_iv_default);
        this.k = this.g.findViewById(R.id.emotionbar_iv_used);
        this.j.setOnClickListener(this);
        this.k.setOnClickListener(this);
        this.d.setOnTouchListener(new ag(this));
        this.e.setOnItemClickListener(new ah(this));
        findViewById(R.id.emotionbar_layout_storebtn).setOnClickListener(new aj(this));
        if (this.v.ak > this.w.j) {
            findViewById(R.id.emotionbar_iv_newmark).setVisibility(0);
        } else {
            findViewById(R.id.emotionbar_iv_newmark).setVisibility(8);
        }
        this.e.setOnScrollListener(new ak(this));
        d();
        setVisibility(8);
    }

    private void d() {
        boolean z;
        View childAt;
        View view = null;
        List<q> e2 = this.s.e();
        this.r = new ArrayList();
        if ((this.p & 4) != 0) {
            for (q qVar : e2) {
                this.r.add(qVar);
                qVar.v = this.s.c(qVar.f3035a);
                bm bmVar = (bm) this.c.get(qVar.f3035a);
                if (bmVar != null) {
                    bmVar.a(qVar);
                }
            }
            while (this.h.getChildCount() > 2) {
                this.h.removeViewAt(2);
            }
            z = this.i != null && (this.i == this.j || this.i == this.k);
            for (q qVar2 : this.r) {
                g.o().inflate((int) R.layout.include_emotionbar_item, this.h);
                ImageView imageView = (ImageView) this.h.findViewById(R.id.imageview);
                ((View) imageView.getParent()).setTag(qVar2);
                ((View) imageView.getParent()).setOnClickListener(this);
                imageView.setId(qVar2.hashCode());
                View view2 = (this.i == null || z || !((q) this.i.getTag()).equals(qVar2) || !qVar2.t) ? view : (View) imageView.getParent();
                if (!qVar2.t) {
                    if (g.a()) {
                        imageView.setAlpha(0.2f);
                    } else {
                        imageView.setAlpha(70);
                    }
                }
                j.b(qVar2.b(), imageView, null, 18, true);
                view = view2;
            }
            findViewById(R.id.emotionbar_layout_storebtn).setVisibility(0);
            this.k.setVisibility(0);
        } else {
            while (this.h.getChildCount() > 2) {
                this.h.removeViewAt(2);
            }
            this.k.setVisibility(8);
            view = this.j;
            findViewById(R.id.emotionbar_layout_storebtn).setVisibility(8);
            z = true;
        }
        if ((this.p & 1) != 0) {
            this.j.setVisibility(0);
            this.d.setVisibility(0);
            childAt = view;
        } else {
            this.j.setVisibility(8);
            this.d.setVisibility(8);
            childAt = ((view == null || view == this.j) && this.h.getChildCount() > 2) ? this.h.getChildAt(2) : view;
        }
        if (childAt == null) {
            childAt = !z ? this.j : this.i;
        }
        if (childAt != this.i) {
            a(childAt);
        }
    }

    /* access modifiers changed from: private */
    public void e() {
        if (this.f != null) {
            if (this.i != this.j || this.f.length() <= 0) {
                if (this.d.isShown()) {
                    this.d.setAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.push_right_out));
                } else {
                    return;
                }
            } else if (this.d.getVisibility() != 0) {
                this.d.setAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.push_left_in));
                this.d.setVisibility(0);
                return;
            } else {
                return;
            }
        }
        this.d.setVisibility(8);
    }

    static /* synthetic */ void g(EmoteInputView emoteInputView) {
        if (emoteInputView.f2626a != null) {
            emoteInputView.f2626a.notifyDataSetChanged();
        }
    }

    public final void a() {
        setVisibility(8);
    }

    public final void a(Intent intent) {
        if (intent.getAction().equals(com.immomo.momo.android.broadcast.o.f2358a)) {
            d();
            if (!"disable".equals(intent.getStringExtra("event"))) {
                return;
            }
            if (this.i != null && this.i == this.k) {
                t tVar = new t();
                tVar.v = bd.b().c();
                this.f2626a = new bm(getContext(), tVar);
                this.e.setAdapter((ListAdapter) this.f2626a);
            } else if (this.f2626a != null) {
                this.f2626a = null;
            }
        }
    }

    public final void b() {
        if (this.q) {
            setVisibility(0);
            e();
        }
    }

    public EmoteEditeText getEditText() {
        return this.f;
    }

    public int getEmoteFlag() {
        return this.p;
    }

    public an getOnEmoteSelectedListener() {
        return this.o;
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.t.a((Object) "onAttachedToWindow");
        this.u = new com.immomo.momo.android.broadcast.o(getContext());
        this.u.a(this);
    }

    public void onClick(View view) {
        if (view == this.k || view == this.j || (view.getTag() != null && (view.getTag() instanceof q))) {
            a(view);
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.t.a((Object) "onDetachedFromWindow");
        if (this.u != null) {
            getContext().unregisterReceiver(this.u);
            this.u = null;
            bd.b().a(true);
        }
    }

    public void setEditText(EmoteEditeText emoteEditeText) {
        this.f = emoteEditeText;
        if (emoteEditeText != null) {
            emoteEditeText.addTextChangedListener(new af(this));
        }
    }

    public void setEmoteFlag(int i2) {
        if (i2 != this.p) {
            this.p = i2;
            d();
        }
    }

    public void setOnEmoteSelectedListener(an anVar) {
        this.o = anVar;
    }
}
