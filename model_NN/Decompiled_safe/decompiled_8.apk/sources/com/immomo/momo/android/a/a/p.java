package com.immomo.momo.android.a.a;

import android.content.Intent;
import android.view.View;
import com.immomo.momo.android.activity.OtherProfileActivity;
import com.immomo.momo.service.bean.aw;

final class p implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ m f698a;
    private final /* synthetic */ aw b;

    p(m mVar, aw awVar) {
        this.f698a = mVar;
        this.b = awVar;
    }

    public final void onClick(View view) {
        Intent intent = new Intent();
        intent.setClass(this.f698a.d, OtherProfileActivity.class);
        intent.putExtra("tag", "local");
        intent.putExtra("momoid", this.b.d());
        this.f698a.d.startActivity(intent);
    }
}
