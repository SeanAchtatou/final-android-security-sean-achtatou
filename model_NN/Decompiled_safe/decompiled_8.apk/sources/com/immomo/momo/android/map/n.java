package com.immomo.momo.android.map;

import android.view.View;
import com.immomo.momo.R;

final class n implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ CreateSiteActivity f2508a;
    private final /* synthetic */ com.immomo.momo.android.view.a.n b;

    n(CreateSiteActivity createSiteActivity, com.immomo.momo.android.view.a.n nVar) {
        this.f2508a = createSiteActivity;
        this.b = nVar;
    }

    public final void onClick(View view) {
        switch (view.getId()) {
            case R.id.layout_type_department /*2131166088*/:
                this.f2508a.k = 1;
                break;
            case R.id.layout_type_office /*2131166089*/:
                this.f2508a.k = 2;
                break;
            case R.id.layout_type_school /*2131166090*/:
                this.f2508a.k = 3;
                break;
            case R.id.layout_type_entertainment /*2131166091*/:
                this.f2508a.k = 4;
                break;
        }
        this.f2508a.i.setText(CreateSiteActivity.h(this.f2508a));
        this.b.dismiss();
    }
}
