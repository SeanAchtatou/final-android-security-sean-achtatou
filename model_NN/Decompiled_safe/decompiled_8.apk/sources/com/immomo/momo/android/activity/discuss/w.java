package com.immomo.momo.android.activity.discuss;

import android.content.Context;
import android.content.Intent;
import com.immomo.momo.android.broadcast.t;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.h;

final class w extends d {

    /* renamed from: a  reason: collision with root package name */
    private v f1248a = null;
    private /* synthetic */ DiscussProfileActivity c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public w(DiscussProfileActivity discussProfileActivity, Context context) {
        super(context);
        this.c = discussProfileActivity;
        this.f1248a = new v(context);
        this.f1248a.setOnCancelListener(new x(this));
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        String a2 = h.a().a(this.c.q);
        this.c.j.a(this.c.q, 2);
        this.c.j.b(this.c.q);
        this.c.j.c(this.c.f.h, this.c.q);
        return a2;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
        this.c.a(this.f1248a);
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        super.a(exc);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        a((String) obj);
        Intent intent = new Intent();
        intent.putExtra("disid", this.c.q);
        intent.setAction(t.b);
        this.c.sendBroadcast(intent);
        this.c.finish();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        super.b();
        this.c.p();
    }
}
