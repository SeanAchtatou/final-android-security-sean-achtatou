package com.immomo.momo.android.activity.feed;

import android.content.DialogInterface;

final class bk implements DialogInterface.OnDismissListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ SharePublishFeedActivity f1468a;

    bk(SharePublishFeedActivity sharePublishFeedActivity) {
        this.f1468a = sharePublishFeedActivity;
    }

    public final void onDismiss(DialogInterface dialogInterface) {
        this.f1468a.finish();
    }
}
