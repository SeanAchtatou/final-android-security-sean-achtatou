package com.immomo.momo.android.activity.group.foundgroup;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.protocol.a.n;

final class v extends d {

    /* renamed from: a  reason: collision with root package name */
    private com.immomo.momo.android.view.a.v f1680a;
    private /* synthetic */ JoinGroupActivity c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public v(JoinGroupActivity joinGroupActivity, Context context) {
        super(context);
        this.c = joinGroupActivity;
        this.f1680a = new com.immomo.momo.android.view.a.v(joinGroupActivity, "申请提交中...");
        this.f1680a.setOnCancelListener(new w(this));
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        n.a().a((String) objArr[0], this.c.h);
        return true;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.f1680a.show();
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        if (((Boolean) obj) != null) {
            this.c.setResult(-1);
            this.c.finish();
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.f1680a.dismiss();
    }
}
