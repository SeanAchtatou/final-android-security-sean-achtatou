package com.immomo.momo.android.activity;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.view.ViewPropertyAnimator;
import com.immomo.momo.g;

final class hm implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ hl f1718a;
    private final /* synthetic */ Bitmap b;

    hm(hl hlVar, Bitmap bitmap) {
        this.f1718a = hlVar;
        this.b = bitmap;
    }

    @SuppressLint({"NewApi"})
    public final void run() {
        if (g.a()) {
            this.f1718a.f1717a.aM.setLayerType(2, null);
            this.f1718a.f1717a.aM.setAlpha(0.0f);
            ViewPropertyAnimator animate = this.f1718a.f1717a.aM.animate();
            animate.alphaBy(1.0f);
            animate.setDuration(600);
            animate.start();
        }
        this.f1718a.f1717a.aM.setImageBitmap(this.b);
    }
}
