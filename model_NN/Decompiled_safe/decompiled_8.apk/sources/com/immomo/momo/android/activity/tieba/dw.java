package com.immomo.momo.android.activity.tieba;

import android.content.Context;
import android.os.Bundle;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import com.immomo.momo.R;
import com.immomo.momo.android.a.is;
import com.immomo.momo.android.activity.lh;
import com.immomo.momo.android.broadcast.aa;
import com.immomo.momo.android.view.HeaderLayout;
import com.immomo.momo.android.view.LoadingButton;
import com.immomo.momo.android.view.MomoRefreshExpandableListView;
import com.immomo.momo.android.view.a.az;
import com.immomo.momo.android.view.bi;
import com.immomo.momo.android.view.bl;
import com.immomo.momo.android.view.bu;
import com.immomo.momo.android.view.cv;
import com.immomo.momo.android.view.g;
import com.immomo.momo.protocol.a.a.e;
import com.immomo.momo.service.af;
import com.immomo.momo.service.ao;
import com.immomo.momo.service.bean.bb;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class dw extends lh implements bl, bu, cv {
    /* access modifiers changed from: private */
    public MomoRefreshExpandableListView O = null;
    private ao P = null;
    /* access modifiers changed from: private */
    public int Q = 0;
    /* access modifiers changed from: private */
    public int R = 0;
    /* access modifiers changed from: private */
    public int S = 0;
    /* access modifiers changed from: private */
    public boolean T = false;
    private Date U = null;
    /* access modifiers changed from: private */
    public is V = null;
    /* access modifiers changed from: private */
    public List W = null;
    private aa X = null;
    /* access modifiers changed from: private */
    public ee Y = null;
    /* access modifiers changed from: private */
    public g Z = null;
    /* access modifiers changed from: private */
    public LoadingButton aa = null;
    /* access modifiers changed from: private */
    public ed ab;
    private az ac;

    static /* synthetic */ void a(dw dwVar, e eVar) {
        dwVar.W = eVar.f2884a;
        dwVar.Q = eVar.b;
        dwVar.S = eVar.d;
        dwVar.T = eVar.e;
        dwVar.R = eVar.c;
        dwVar.N.c("tieba_index_applycount", Integer.valueOf(dwVar.Q));
        dwVar.N.c("tieba_index_mycount", Integer.valueOf(dwVar.R));
        dwVar.N.c("tieba_index_remian", Integer.valueOf(dwVar.S));
        dwVar.N.c("tieba_index_admin", Boolean.valueOf(dwVar.T));
        for (bb bbVar : dwVar.W) {
            if (bbVar.a() == 0) {
                dwVar.P.d(bbVar.c());
            } else if (bbVar.a() == 1) {
                dwVar.P.e(bbVar.c());
            }
        }
    }

    static /* synthetic */ void j(dw dwVar) {
        dwVar.U = new Date();
        dwVar.O.setLastFlushTime(dwVar.U);
        dwVar.N.a("tieba_lasttime_success", dwVar.U);
        int l = af.c().l();
        if (dwVar.T) {
            dwVar.V.a(af.c().m(), l);
        } else if (l > 0) {
            af.c().d(0);
        }
    }

    /* access modifiers changed from: protected */
    public final int B() {
        return R.layout.fragment_tieba_index;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, com.immomo.momo.android.view.MomoRefreshExpandableListView, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* access modifiers changed from: protected */
    public final void C() {
        this.O = (MomoRefreshExpandableListView) c((int) R.id.listview);
        this.O.setListPaddingBottom(-3);
        this.O.setEnableLoadMoreFoolter(true);
        this.aa = this.O.getFooterViewButton();
        this.O.setMMHeaderView(com.immomo.momo.g.o().inflate((int) R.layout.listitem_groupsite, (ViewGroup) this.O, false));
        this.Z = new g(c(), 6);
        this.O.addHeaderView(this.Z.getWappview());
        MomoRefreshExpandableListView momoRefreshExpandableListView = this.O;
        LinearLayout linearLayout = (LinearLayout) com.immomo.momo.g.o().inflate((int) R.layout.include_multiselect_searchbar, (ViewGroup) null);
        ((EditText) linearLayout.findViewById(R.id.search_edittext)).setText("搜索或创建感兴趣的陌陌吧");
        ((EditText) linearLayout.findViewById(R.id.search_edittext)).setFocusable(false);
        ((EditText) linearLayout.findViewById(R.id.search_edittext)).setOnClickListener(new ec(this));
        momoRefreshExpandableListView.addHeaderView(linearLayout);
        this.O.a(com.immomo.momo.g.o().inflate((int) R.layout.include_tiebaindex_listempty, (ViewGroup) null));
    }

    public final boolean O() {
        return this.Y != null && this.Y.e();
    }

    public final void P() {
        this.ac = new az(c());
        this.ac.a(new ea(this));
        this.ac.show();
    }

    public final void S() {
        super.S();
        if (this.V.isEmpty()) {
            this.O.h();
        } else if (this.U == null || System.currentTimeMillis() - this.U.getTime() > 900000) {
            a(new ee(this, c()));
        }
    }

    public final void a(Context context, HeaderLayout headerLayout) {
        super.a(context, headerLayout);
        headerLayout.setTitleText((int) R.string.tieba_title_index0);
        headerLayout.a(new bi(com.immomo.momo.g.c()).a("分类"), new eb(this));
    }

    /* access modifiers changed from: protected */
    public final boolean a(Bundle bundle, String str) {
        if ("actions.tiebareportchanged".equals(str) || "actions.tiebareport".equals(str)) {
            int i = bundle.getInt("tiebareport", -1);
            if (i < 0) {
                i = af.c().l();
            }
            this.V.a(i);
            if (bundle.containsKey("content")) {
                this.V.a(bundle.getString("content"));
            }
        }
        return super.a(bundle, str);
    }

    /* access modifiers changed from: protected */
    public final void ae() {
        super.ae();
        this.Z.g();
    }

    /* access modifiers changed from: protected */
    public final void ag() {
        super.ag();
        this.Z.f();
    }

    public final void ai() {
        this.O.e();
    }

    public final void aj() {
        this.aa.setOnProcessListener(this);
        this.O.setOnPullToRefreshListener(this);
        this.O.setOnCancelListener(this);
        this.O.setOnChildClickListener(new dy(this));
        this.O.setOnGroupClickListener(new dz());
        this.U = this.N.b("tieba_lasttime_success");
        this.Q = ((Integer) this.N.b("tieba_index_applycount", Integer.valueOf(this.Q))).intValue();
        this.R = ((Integer) this.N.b("tieba_index_mycount", Integer.valueOf(this.R))).intValue();
        this.S = ((Integer) this.N.b("tieba_index_remian", Integer.valueOf(this.S))).intValue();
        this.T = ((Boolean) this.N.b("tieba_index_admin", Boolean.valueOf(this.T))).booleanValue();
        this.O.setLastFlushTime(this.U);
        ArrayList arrayList = new ArrayList();
        bb bbVar = new bb(0);
        bbVar.a(this.P.b());
        arrayList.add(bbVar);
        bb bbVar2 = new bb(1);
        bbVar2.a(this.P.c());
        arrayList.add(bbVar2);
        this.V = new is(this, arrayList, this.O, this.Q, this.R, this.S, this.T);
        this.O.setAdapter(this.V);
        this.V.a();
        int l = af.c().l();
        if (this.T) {
            this.V.a(af.c().m(), l);
        } else if (l > 0) {
            af.c().d(0);
        }
        a(600, "actions.tiebareport", "actions.tiebareportchanged");
    }

    public final void b_() {
        a(new ee(this, c()));
    }

    /* access modifiers changed from: protected */
    public final void g(Bundle bundle) {
        this.P = new ao();
        this.X = new aa(c());
        this.X.a(new dx(this));
        aj();
    }

    public final void m() {
        super.m();
    }

    public final void p() {
        if (this.X != null) {
            a(this.X);
            this.X = null;
        }
        if (this.Z != null) {
            g gVar = this.Z;
            g.k();
        }
        super.p();
    }

    public final void u() {
        a(new ef(this, c()));
    }

    public final void v() {
        this.O.i();
    }
}
