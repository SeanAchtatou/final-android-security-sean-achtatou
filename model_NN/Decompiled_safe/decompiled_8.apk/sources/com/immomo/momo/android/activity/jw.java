package com.immomo.momo.android.activity;

import android.content.DialogInterface;

final class jw implements DialogInterface.OnCancelListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ jv f1777a;

    jw(jv jvVar) {
        this.f1777a = jvVar;
    }

    public final void onCancel(DialogInterface dialogInterface) {
        this.f1777a.cancel(true);
    }
}
