package com.immomo.momo.android.view;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.v4.b.a;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.widget.ImageView;
import com.immomo.momo.R;
import com.immomo.momo.android.c.u;
import com.immomo.momo.g;
import com.immomo.momo.util.m;
import java.util.concurrent.ThreadPoolExecutor;
import mm.purchasesdk.PurchaseCode;

public class MGifImageView extends ImageView implements ay {
    /* access modifiers changed from: private */
    public static ThreadPoolExecutor h = new u(5, 10);
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public au f2635a;
    private int b;
    private int c;
    /* access modifiers changed from: private */
    public bn d;
    /* access modifiers changed from: private */
    public bo e;
    private float f;
    private boolean g;
    private String i;
    private TextPaint j;

    public MGifImageView(Context context) {
        super(context);
        this.f2635a = null;
        this.b = 0;
        this.c = 0;
        this.d = null;
        this.e = null;
        new m(this);
        this.j = null;
    }

    public MGifImageView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public MGifImageView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.f2635a = null;
        this.b = 0;
        this.c = 0;
        this.d = null;
        this.e = null;
        new m(this);
        this.j = null;
    }

    public final void a() {
        au auVar = this.f2635a;
        if (auVar != null && auVar.h()) {
            if (this.d == null || !this.d.N()) {
                g.d().c().post(new bm(this, auVar));
            }
        }
    }

    public int getDensity() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        if (!(getContext() instanceof Activity)) {
            return PurchaseCode.AUTH_NOORDER;
        }
        ((Activity) getContext()).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.densityDpi;
    }

    public float getScale() {
        this.f = ((float) getContext().getResources().getDisplayMetrics().densityDpi) / ((float) getDensity());
        if (this.f < 0.1f) {
            this.f = 0.1f;
        }
        if (this.f > 5.0f) {
            this.f = 5.0f;
        }
        return this.f;
    }

    public void onDraw(Canvas canvas) {
        boolean z = false;
        au auVar = this.f2635a;
        if (auVar != null) {
            Bitmap j2 = auVar.j();
            if (j2 != null) {
                canvas.drawBitmap(j2, new Rect(0, 0, j2.getWidth(), j2.getHeight()), new Rect(0, 0, getMeasuredWidth(), getMeasuredHeight()), new Paint());
                z = true;
            }
            if (auVar.b() == 1 && auVar.a() == 0 && auVar.d()) {
                bo boVar = this.e;
                if (boVar != null) {
                    boVar.f2747a = false;
                }
                this.f2635a.c();
            }
        }
        if (z) {
            return;
        }
        if (!a.a((CharSequence) this.i)) {
            if (this.j == null) {
                this.j = new TextPaint();
                this.j.setTextSize((float) g.b(13.0f));
                this.j.setAntiAlias(true);
                this.j.setColor(getResources().getColor(R.color.font_value));
            }
            String str = "[" + this.i + "]";
            int measureText = (int) this.j.measureText("[" + str + "]");
            if (measureText > getMeasuredWidth()) {
                str = "[表情]";
                measureText = (int) this.j.measureText("[" + str + "]");
            }
            canvas.drawText(str, (float) ((getMeasuredWidth() - measureText) / 2), (((float) getMeasuredHeight()) - this.j.getTextSize()) / 2.0f, this.j);
            return;
        }
        super.onDraw(canvas);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        au auVar = this.f2635a;
        if (this.b > 0 && this.c > 0) {
            setMeasuredDimension(this.b, this.c);
        } else if (auVar == null || !auVar.h()) {
            super.onMeasure(i2, i3);
        } else {
            setMeasuredDimension(auVar.b, auVar.c);
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        return super.onTouchEvent(motionEvent);
    }

    public void setAlt(String str) {
        this.i = str;
    }

    public void setGifDecoder(au auVar) {
        boolean z = true;
        if (this.e != null) {
            this.e.f2747a = false;
            this.e = null;
        }
        if (auVar != null) {
            auVar.a(this);
            this.f2635a = auVar;
            if (auVar.b() != 1) {
                z = false;
            }
            this.g = z;
            if (this.f2635a != null && this.g && !this.f2635a.d && this.f2635a.h()) {
                this.e = new bo(this, (byte) 0);
                h.execute(this.e);
            }
        } else {
            this.g = false;
            this.f2635a = null;
        }
        requestLayout();
    }

    public void setGifStatus(bn bnVar) {
        this.d = bnVar;
    }

    public void setHeight(int i2) {
        this.c = i2;
    }

    public void setWidth(int i2) {
        this.b = i2;
    }
}
