package com.immomo.momo.android.activity.maintab;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ListAdapter;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.a.a;
import com.immomo.momo.android.a.dl;
import com.immomo.momo.android.activity.an;
import com.immomo.momo.android.b.p;
import com.immomo.momo.android.b.z;
import com.immomo.momo.android.broadcast.w;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.LoadingButton;
import com.immomo.momo.android.view.MomoRefreshListView;
import com.immomo.momo.android.view.a.aj;
import com.immomo.momo.android.view.a.x;
import com.immomo.momo.android.view.bi;
import com.immomo.momo.android.view.bl;
import com.immomo.momo.android.view.bu;
import com.immomo.momo.android.view.cv;
import com.immomo.momo.android.view.g;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.util.ao;
import com.immomo.momo.util.k;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class aq extends k implements bl, bu, cv {
    /* access modifiers changed from: private */
    public List O = null;
    /* access modifiers changed from: private */
    public Map P = null;
    /* access modifiers changed from: private */
    public a Q = null;
    private w R = null;
    /* access modifiers changed from: private */
    public com.immomo.momo.service.aq S = null;
    /* access modifiers changed from: private */
    public MomoRefreshListView T = null;
    /* access modifiers changed from: private */
    public LoadingButton U = null;
    /* access modifiers changed from: private */
    public int V = 0;
    /* access modifiers changed from: private */
    public int W = 0;
    /* access modifiers changed from: private */
    public d X = null;
    /* access modifiers changed from: private */
    public d Y = null;
    /* access modifiers changed from: private */
    public Date Z = null;
    /* access modifiers changed from: private */
    public boolean aa = false;
    /* access modifiers changed from: private */
    public bi ab;
    /* access modifiers changed from: private */
    public g ac = null;
    /* access modifiers changed from: private */
    public Handler ad = new Handler();
    private p ae = null;
    private com.immomo.momo.android.broadcast.d af = new ar(this);

    /* access modifiers changed from: private */
    public void P() {
        if (this.N == null || !(this.N.l == 2 || this.N.l == 1)) {
            K().setTitleText((int) R.string.nearby);
        } else {
            K().setTitleText((int) R.string.nearby_hide);
        }
    }

    /* access modifiers changed from: private */
    public void Q() {
        if (this.O.size() <= 0) {
            this.U.setVisibility(8);
        } else {
            this.U.setVisibility(0);
        }
    }

    static /* synthetic */ void a(aq aqVar, double d) {
        if (d >= 1200.0d) {
            an anVar = new an(1006, aqVar.a((int) R.string.tips_nearbypeople));
            anVar.b();
            aqVar.b(anVar);
            return;
        }
        aqVar.ac();
    }

    /* access modifiers changed from: private */
    public void al() {
        this.ad.post(new bd(this));
    }

    /* access modifiers changed from: private */
    public void am() {
        this.Z = new Date();
        this.N.a("lasttime_neayby", this.Z);
        this.U.e();
        this.T.n();
    }

    private void an() {
        z.b(this.ae);
        if (this.ae != null) {
            this.ae.b = false;
        }
    }

    static /* synthetic */ List b(aq aqVar, boolean z) {
        aqVar.L.a((Object) ("downloadUsers ----> gender=" + aqVar.N.n.a() + ",  timeline=" + aqVar.N.o.a()));
        List a2 = com.immomo.momo.protocol.a.w.a().a(aqVar.N.n.a(), aqVar.V, z, aqVar.N.o.a(), aqVar.N.q, aqVar.N.r, aqVar.N.p, aqVar.N.s);
        if (a2 != null && a2.size() > 0) {
            aqVar.V += a2.size();
            aqVar.L.a((Object) ("download success, lastindex:" + aqVar.V));
            aqVar.S.a(a2);
        }
        return a2;
    }

    static /* synthetic */ void r(aq aqVar) {
        if (!aqVar.M.b()) {
            aqVar.N.s = PoiTypeDef.All;
            aqVar.N.p = aj.ALL;
            aqVar.N.q = 0;
            aqVar.N.r = 0;
        }
        x xVar = new x(aqVar.c(), aqVar.N.n, aqVar.N.o, aqVar.N.q, aqVar.N.r, aqVar.N.p, aqVar.N.s);
        xVar.a(new ax(aqVar));
        xVar.a(new be(aqVar, (byte) 0));
        xVar.a(aqVar.K());
    }

    static /* synthetic */ void x(aq aqVar) {
        Intent intent = new Intent();
        intent.setAction("com.android.settings.APPLICATION_DEVELOPMENT_SETTINGS");
        intent.setFlags(268435456);
        try {
            aqVar.a(intent);
        } catch (Throwable th) {
            aqVar.L.a(th);
            intent.setAction("android.settings.SETTINGS");
            try {
                aqVar.a(intent);
            } catch (Exception e) {
                aqVar.L.a((Throwable) e);
            }
        }
    }

    /* access modifiers changed from: protected */
    public final int B() {
        return R.layout.fragment_nearby_people;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x004f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void C() {
        /*
            r5 = this;
            r1 = 0
            r4 = 1
            r0 = 2131165255(0x7f070047, float:1.7944722E38)
            android.view.View r0 = r5.c(r0)
            com.immomo.momo.android.view.MomoRefreshListView r0 = (com.immomo.momo.android.view.MomoRefreshListView) r0
            r5.T = r0
            com.immomo.momo.android.view.MomoRefreshListView r0 = r5.T
            r0.setEnableLoadMoreFoolter(r4)
            com.immomo.momo.android.view.MomoRefreshListView r0 = r5.T
            com.immomo.momo.android.view.LoadingButton r0 = r0.getFooterViewButton()
            r5.U = r0
            com.immomo.momo.service.bean.at r0 = r5.N
            java.lang.String r2 = "lasttime_neayby_success"
            java.lang.String r3 = ""
            java.lang.Object r0 = r0.b(r2, r3)
            java.lang.String r0 = (java.lang.String) r0
            boolean r2 = android.support.v4.b.a.a(r0)     // Catch:{ Exception -> 0x00b2 }
            if (r2 != 0) goto L_0x00b3
            java.util.Date r0 = android.support.v4.b.a.k(r0)     // Catch:{ Exception -> 0x00b2 }
        L_0x0030:
            com.immomo.momo.android.view.MomoRefreshListView r2 = r5.T
            r2.setLastFlushTime(r0)
            com.immomo.momo.android.view.g r0 = new com.immomo.momo.android.view.g
            android.support.v4.app.g r2 = r5.c()
            r0.<init>(r2, r4)
            com.immomo.momo.android.view.MomoRefreshListView r2 = r5.T
            android.view.View r3 = r0.getWappview()
            r2.addHeaderView(r3)
            r5.ac = r0
            boolean r0 = com.immomo.momo.android.view.dp.a(r4)
            if (r0 == 0) goto L_0x005d
            com.immomo.momo.android.view.MomoRefreshListView r0 = r5.T
            com.immomo.momo.android.view.dp r2 = new com.immomo.momo.android.view.dp
            android.support.v4.app.g r3 = r5.c()
            r2.<init>(r3, r4)
            r0.addHeaderView(r2)
        L_0x005d:
            com.immomo.momo.android.view.HeaderLayout r0 = r5.K()
            com.immomo.momo.android.view.HeaderSpinner r0 = r0.getHeaderSpinner()
            r2 = 8
            r0.setVisibility(r2)
            r5.P()
            com.immomo.momo.android.view.HeaderLayout r0 = r5.K()
            com.immomo.momo.android.view.bi r2 = new com.immomo.momo.android.view.bi
            android.support.v4.app.g r3 = r5.c()
            r2.<init>(r3)
            r3 = 2130838256(0x7f0202f0, float:1.728149E38)
            com.immomo.momo.android.view.bi r2 = r2.a(r3)
            r5.ab = r2
            com.immomo.momo.android.activity.maintab.au r3 = new com.immomo.momo.android.activity.maintab.au
            r3.<init>(r5)
            r0.a(r2, r3)
            com.immomo.momo.android.view.MomoRefreshListView r0 = r5.T
            android.view.LayoutInflater r2 = com.immomo.momo.g.o()
            r3 = 2130903380(0x7f030154, float:1.7413576E38)
            android.view.View r1 = r2.inflate(r3, r1)
            r0.a(r1)
            r0 = 2131166316(0x7f07046c, float:1.7946874E38)
            android.view.View r0 = r1.findViewById(r0)
            com.immomo.momo.android.activity.maintab.av r1 = new com.immomo.momo.android.activity.maintab.av
            r1.<init>(r5)
            r0.setOnClickListener(r1)
            com.immomo.momo.android.view.MomoRefreshListView r0 = r5.T
            int r1 = com.immomo.momo.android.activity.maintab.MaintabActivity.h
            r0.setListPaddingBottom(r1)
            return
        L_0x00b2:
            r0 = move-exception
        L_0x00b3:
            r0 = r1
            goto L_0x0030
        */
        throw new UnsupportedOperationException("Method not decompiled: com.immomo.momo.android.activity.maintab.aq.C():void");
    }

    /* access modifiers changed from: protected */
    public final void E() {
        super.E();
        this.T.o();
        if (this.U.d()) {
            this.U.e();
        }
    }

    public final void S() {
        super.S();
        this.L.a((Object) "onFristResume---------------");
        if (this.O.isEmpty() || this.Z == null || System.currentTimeMillis() - this.Z.getTime() > 900000) {
            this.T.m();
        }
    }

    /* access modifiers changed from: protected */
    public final void a(an anVar) {
        if (anVar != null) {
            switch (anVar.a()) {
                case 1006:
                    this.ad.post(new as(this));
                    return;
                default:
                    return;
            }
        }
    }

    public final void ae() {
        super.ae();
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("type", 1);
            jSONObject.put("sex", this.N.n.a());
            jSONObject.put("time", this.N.o.a());
            new k("PI", "P2", jSONObject).e();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        this.ac.g();
    }

    public final void ag() {
        super.ag();
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("type", 1);
            jSONObject.put("sex", this.N.n.a());
            jSONObject.put("time", this.N.o.a());
            new k("PO", "P2", jSONObject).e();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        this.ac.f();
    }

    public final void ai() {
        this.T.i();
    }

    public final void b_() {
        if (this.X != null && !this.X.isCancelled()) {
            this.X.cancel(true);
        }
        this.T.setLoadingViewText(R.string.pull_to_refresh_locate_label);
        this.ae = new ay(this);
        try {
            z.a(this.ae);
        } catch (Exception e) {
            this.L.a((Throwable) e);
            ao.g(R.string.errormsg_location_nearby_failed);
            al();
        }
        new k("S", "S201").e();
    }

    public final void g(Bundle bundle) {
        this.S = new com.immomo.momo.service.aq();
        this.R = new w(c());
        this.R.a(this.af);
        this.T.setOnPullToRefreshListener$42b903f6(this);
        this.T.setOnCancelListener$135502(this);
        this.U.setOnProcessListener(this);
        this.T.setOnItemClickListener(new aw(this));
        this.P = new HashMap();
        this.O = this.S.j();
        this.Q = new dl(com.immomo.momo.g.c(), this.O, this.T, false);
        this.T.setAdapter((ListAdapter) this.Q);
        Q();
        for (bf bfVar : this.O) {
            this.P.put(bfVar.h, bfVar);
        }
        this.V = this.O.size();
        this.Z = this.N.b("lasttime_neayby");
    }

    public final void p() {
        super.p();
        if (this.X != null && !this.X.isCancelled()) {
            this.X.cancel(true);
            this.X = null;
        }
        if (this.Y != null && !this.Y.isCancelled()) {
            this.Y.cancel(true);
            this.Y = null;
        }
        if (this.R != null) {
            a(this.R);
            this.R = null;
        }
        if (this.ac != null) {
            g gVar = this.ac;
            g.k();
        }
        an();
    }

    public final void u() {
        new k("C", "C201").e();
        this.U.f();
        this.Y = new bf(this, c());
        this.Y.execute(new Object[0]);
    }

    public final void v() {
        this.V = this.W;
        am();
        if (this.aa) {
            this.aa = false;
        }
        if (this.X != null && !this.X.isCancelled()) {
            this.X.cancel(true);
        }
        an();
    }
}
