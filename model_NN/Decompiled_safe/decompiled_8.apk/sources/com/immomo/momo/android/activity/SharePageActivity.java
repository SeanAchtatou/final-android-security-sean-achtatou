package com.immomo.momo.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.a;
import com.immomo.momo.android.c.r;
import com.immomo.momo.android.view.HeaderLayout;
import com.immomo.momo.android.view.bi;
import com.immomo.momo.g;
import java.util.Calendar;

public class SharePageActivity extends ah implements View.OnClickListener {
    public static final String h = (String.valueOf(a.c) + "/sharecard").replace("https", "http");
    Handler i = new km(this);
    private HeaderLayout j = null;
    /* access modifiers changed from: private */
    public CheckBox k;
    private View l;
    private View m;
    private TextView n;
    /* access modifiers changed from: private */
    public ImageView o;
    private int p = -1;
    private String q = null;
    private String r = null;
    private String s = null;
    private String t = null;

    /* access modifiers changed from: private */
    public void u() {
        switch (this.p) {
            case 0:
                if (this.f == null) {
                    return;
                }
                if (this.f.an) {
                    new kt(this).execute(new String[0]);
                    return;
                }
                Intent intent = new Intent(this, ShareWebviewActivity.class);
                intent.putExtra("share_type", 0);
                startActivity(intent);
                return;
            case 1:
            default:
                return;
            case 2:
                if (this.f.at) {
                    new kr(this).execute(new String[0]);
                    return;
                }
                Intent intent2 = new Intent(this, ShareWebviewActivity.class);
                intent2.putExtra("share_type", 2);
                startActivity(intent2);
                return;
            case 3:
                if (this.f == null) {
                    return;
                }
                if (this.f.ar) {
                    new kp(this).execute(new String[0]);
                    return;
                }
                Intent intent3 = new Intent(this, ShareWebviewActivity.class);
                intent3.putExtra("share_type", 3);
                startActivity(intent3);
                return;
            case 4:
                Intent intent4 = new Intent(this, ShareWebviewActivity.class);
                intent4.putExtra("share_type", 4);
                startActivity(intent4);
                return;
        }
    }

    /* access modifiers changed from: private */
    public Intent v() {
        Intent intent = new Intent();
        intent.putExtra("share_type", this.p);
        return intent;
    }

    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        super.a(bundle);
        setContentView((int) R.layout.activity_setting_sharepage_user);
        d();
        this.j = (HeaderLayout) findViewById(R.id.layout_header);
        this.k = (CheckBox) findViewById(R.id.checkbox_focus);
        this.l = findViewById(R.id.layout_fellow);
        this.n = (TextView) findViewById(R.id.share_title);
        this.o = (ImageView) findViewById(R.id.share_image);
        this.m = findViewById(R.id.layout_share);
        bi biVar = new bi(this);
        biVar.a("分享");
        biVar.a((int) R.drawable.ic_topbar_confirm);
        a(biVar, new kn(this));
        if (this.f != null) {
            String c = g.c("momoshared_" + this.f.h);
            if (!android.support.v4.b.a.a((CharSequence) this.t)) {
                c = new StringBuilder().append(System.currentTimeMillis()).toString();
            }
            r rVar = new r(c, new ko(this), 17, null);
            int i2 = Calendar.getInstance().get(5);
            if (android.support.v4.b.a.a((CharSequence) this.t)) {
                this.t = String.valueOf(h) + "/" + this.f.h + ".jpg?day=" + i2;
            }
            rVar.a(this.t);
            new Thread(rVar).start();
        }
        if (!android.support.v4.b.a.a((CharSequence) this.q)) {
            this.j.setTitleText(this.q);
        }
        if (!android.support.v4.b.a.a((CharSequence) this.r)) {
            this.n.setText(this.r);
        }
        if (!android.support.v4.b.a.a((CharSequence) this.s)) {
            this.k.setText(this.s);
            this.l.setVisibility(0);
            this.k.setVisibility(0);
        } else {
            this.l.setVisibility(8);
        }
        this.m.setOnClickListener(this);
        this.l.setOnClickListener(this);
    }

    /* access modifiers changed from: protected */
    public final void d() {
        Intent intent = getIntent();
        this.p = intent.getIntExtra("share_type", -1);
        this.q = intent.getStringExtra("title_str");
        this.r = intent.getStringExtra("content_str");
        this.s = intent.getStringExtra("show_checkbox");
        this.t = intent.getStringExtra("img_url");
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.share_button /*2131165926*/:
                u();
                return;
            case R.id.layout_fellow /*2131165930*/:
                this.k.toggle();
                return;
            default:
                return;
        }
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 != 4) {
            return super.onKeyDown(i2, keyEvent);
        }
        setResult(-1, v());
        finish();
        return true;
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
    }
}
