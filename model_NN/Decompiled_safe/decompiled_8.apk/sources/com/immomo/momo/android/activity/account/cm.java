package com.immomo.momo.android.activity.account;

import com.immomo.momo.protocol.a.w;
import java.util.TimerTask;

final class cm extends TimerTask {

    /* renamed from: a  reason: collision with root package name */
    private int f992a = 0;
    /* access modifiers changed from: private */
    public /* synthetic */ ck b;

    cm(ck ckVar) {
        this.b = ckVar;
    }

    public final void run() {
        if (this.b.h.isDestroyed() || this.b.c.f || this.f992a > 240) {
            this.b.b.cancel();
            this.b.b = null;
            cancel();
            return;
        }
        try {
            this.b.f990a.b((Object) "!!! timer run");
            this.f992a++;
            this.b.c.e = w.a().a(this.b.c.c, this.b.c.b, this.b.h.q, this.b.h.s);
            this.b.f990a.b((Object) ("!!! timer get verificationCode:" + this.b.c.e));
            this.b.c.f = true;
            this.b.b.cancel();
            this.b.b = null;
            this.b.h.runOnUiThread(new cn(this));
        } catch (Exception e) {
            this.b.f990a.a((Throwable) e);
        }
    }
}
