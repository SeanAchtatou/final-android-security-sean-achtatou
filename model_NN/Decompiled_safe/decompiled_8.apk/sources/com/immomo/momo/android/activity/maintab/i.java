package com.immomo.momo.android.activity.maintab;

import android.os.AsyncTask;
import com.immomo.momo.protocol.a.w;
import com.immomo.momo.service.aq;
import com.immomo.momo.service.bean.bf;
import java.util.ArrayList;

final class i extends AsyncTask {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ MaintabActivity f1891a;

    private i(MaintabActivity maintabActivity) {
        this.f1891a = maintabActivity;
    }

    /* synthetic */ i(MaintabActivity maintabActivity, byte b) {
        this(maintabActivity);
    }

    private Boolean a() {
        try {
            long j = this.f1891a.g.k;
            ArrayList arrayList = new ArrayList();
            ArrayList<bf> arrayList2 = new ArrayList<>();
            long a2 = w.a().a(j, arrayList);
            aq aqVar = new aq();
            if (arrayList.size() > 0) {
                for (bf bfVar : arrayList2) {
                    if (!aqVar.f(bfVar.h)) {
                        aqVar.i(bfVar);
                        if (!aqVar.c(bfVar)) {
                            arrayList2.add(bfVar);
                        }
                    }
                }
            }
            try {
                if (arrayList2.size() > 0) {
                    w.a().a(arrayList2);
                    aqVar.a(arrayList2);
                }
            } catch (Exception e) {
                this.f1891a.e.a((Throwable) e);
            }
            this.f1891a.g.k = a2;
            this.f1891a.g.a("bothlist_version", Long.valueOf(a2));
        } catch (Exception e2) {
            this.f1891a.e.a((Throwable) e2);
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object doInBackground(Object... objArr) {
        return a();
    }
}
