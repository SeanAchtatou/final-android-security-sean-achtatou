package com.immomo.momo.android.broadcast;

import android.content.Context;
import com.immomo.momo.g;

public final class s extends b {

    /* renamed from: a  reason: collision with root package name */
    public static final String f2362a = (String.valueOf(g.h()) + ".action.memberlist.delete");

    public s(Context context) {
        super(context, f2362a);
    }
}
