package com.immomo.momo.android.activity.message;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.service.aq;

final class ah extends d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ a f1915a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ah(a aVar, Context context) {
        super(context);
        this.f1915a = aVar;
    }

    /* access modifiers changed from: protected */
    public final Object a(Object... objArr) {
        com.immomo.momo.protocol.imjson.d.a(this.f1915a.f.S, this.f1915a.f.T, this.f1915a.f.U, this.f1915a.f.aH, this.f1915a.f.aI);
        return null;
    }

    /* access modifiers changed from: protected */
    public final void a(Object obj) {
        super.a(obj);
        this.f1915a.f.a(System.currentTimeMillis());
        new aq().a(this.f1915a.f);
    }
}
