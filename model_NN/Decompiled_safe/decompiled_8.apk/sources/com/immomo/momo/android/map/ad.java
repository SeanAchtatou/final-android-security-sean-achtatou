package com.immomo.momo.android.map;

import android.content.Intent;
import android.location.Location;
import android.net.Uri;
import com.immomo.momo.R;
import com.immomo.momo.android.b.p;
import com.immomo.momo.android.b.z;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.util.ao;

final class ad extends p {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ GoogleMapActivity f2457a;
    private final /* synthetic */ v c;

    ad(GoogleMapActivity googleMapActivity, v vVar) {
        this.f2457a = googleMapActivity;
        this.c = vVar;
    }

    public final void a(Location location, int i, int i2, int i3) {
        if (this.c.isShowing()) {
            this.c.dismiss();
            if (z.a(location)) {
                this.f2457a.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://maps.google.com/maps?daddr=" + this.f2457a.c.getLatitude() + "," + this.f2457a.c.getLongitude() + "&saddr=" + location.getLatitude() + "," + location.getLongitude() + "&ie=UTF-8&om=0&sort=num")));
            } else if (i2 == 104) {
                ao.g(R.string.errormsg_network_unfind);
            } else {
                ao.g(R.string.errormsg_location_nearby_failed);
            }
        }
    }
}
