package com.immomo.momo.android.activity.account;

import android.content.DialogInterface;
import android.support.v4.b.a;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.android.view.a.n;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.util.ao;
import com.immomo.momo.util.aq;
import com.immomo.momo.util.k;
import com.immomo.momo.util.m;

public final class ba extends ab implements RadioGroup.OnCheckedChangeListener {

    /* renamed from: a  reason: collision with root package name */
    private bf f954a = null;
    private EditText b = null;
    private RadioGroup c = null;
    private RegisterActivityWithP d = null;
    private boolean e = false;

    public ba(bf bfVar, View view, RegisterActivityWithP registerActivityWithP) {
        super(view);
        this.f954a = bfVar;
        this.d = registerActivityWithP;
        this.d.c(RegisterActivityWithP.l);
        new m("RegisterStep1");
        this.b = (EditText) a((int) R.id.rg_et_name);
        this.c = (RadioGroup) a((int) R.id.rg_radiogroup_gender);
        EditText editText = this.b;
        EditText editText2 = this.b;
        editText.addTextChangedListener(new aq(32));
        if (!a.a((CharSequence) this.f954a.i)) {
            this.b.setText(this.f954a.i);
        }
        if ("M".equalsIgnoreCase(this.f954a.H)) {
            ((RadioButton) a((int) R.id.rg_radiobutton_Male)).setChecked(true);
        } else if ("F".equalsIgnoreCase(this.f954a.H)) {
            ((RadioButton) a((int) R.id.rg_radiobutton_Female)).setChecked(true);
        }
        this.c.setOnCheckedChangeListener(this);
    }

    public final boolean a() {
        if (a(this.b)) {
            ao.e(R.string.reg_username_empty);
            this.d.a((TextView) this.b);
            return false;
        } else if (this.c.getCheckedRadioButtonId() < 0) {
            ao.e(R.string.reg_gender_empty);
            this.d.f();
            return false;
        } else {
            String trim = this.b.getText().toString().trim();
            try {
                if (trim.getBytes("GBK").length > 32) {
                    ao.b("姓名输入过长");
                    this.d.a((TextView) this.b);
                    return false;
                }
            } catch (Exception e2) {
            }
            this.f954a.H = this.c.getCheckedRadioButtonId() == R.id.rg_radiobutton_Female ? "F" : "M";
            this.f954a.i = trim;
            return true;
        }
    }

    public final void c() {
        if (a()) {
            this.d.u();
        }
    }

    public final void d() {
        super.d();
        this.d.i();
    }

    public final void e() {
        new k("PI", "P124").e();
    }

    public final void f() {
        new k("PO", "P124").e();
    }

    public final void onCheckedChanged(RadioGroup radioGroup, int i) {
        if (!this.e) {
            this.e = true;
            n.b(this.d, "注册成功后性别将不可更改", (DialogInterface.OnClickListener) null).show();
        }
        if (i == R.id.rg_radiobutton_Male) {
            new k("C", "C12403").e();
        } else if (i == R.id.rg_radiobutton_Female) {
            new k("C", "C12404").e();
        }
    }
}
