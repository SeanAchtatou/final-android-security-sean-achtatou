package com.immomo.momo.android.activity;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import com.immomo.momo.service.bean.bf;

final class lz implements AdapterView.OnItemClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ UserRoamActivity f1825a;

    lz(UserRoamActivity userRoamActivity) {
        this.f1825a = userRoamActivity;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        Intent intent = new Intent(this.f1825a.getApplicationContext(), OtherProfileActivity.class);
        intent.putExtra("momoid", ((bf) this.f1825a.q.getItem(i)).h);
        intent.putExtra("afrom", UserRoamActivity.class.getName());
        this.f1825a.startActivity(intent);
    }
}
