package com.immomo.momo.android.activity;

import android.content.DialogInterface;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import com.immomo.momo.android.view.a.n;

final class mt extends WebChromeClient {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ WebviewActivity f2011a;

    mt(WebviewActivity webviewActivity) {
        this.f2011a = webviewActivity;
    }

    public final boolean onJsAlert(WebView webView, String str, String str2, JsResult jsResult) {
        if (this.f2011a.isFinishing()) {
            return false;
        }
        this.f2011a.a(n.b(this.f2011a, str2, (DialogInterface.OnClickListener) null));
        jsResult.confirm();
        return true;
    }
}
