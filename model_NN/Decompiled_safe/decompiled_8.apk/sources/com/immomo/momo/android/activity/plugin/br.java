package com.immomo.momo.android.activity.plugin;

import android.content.Context;
import com.immomo.momo.R;
import com.immomo.momo.a.a;
import com.immomo.momo.android.c.d;
import com.immomo.momo.plugin.e.e;

final class br extends d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ SinaWeiboActivity f2072a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public br(SinaWeiboActivity sinaWeiboActivity, Context context) {
        super(context);
        this.f2072a = sinaWeiboActivity;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        return com.immomo.momo.protocol.a.d.b(this.f2072a.j, this.f2072a.k);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
        this.f2072a.findViewById(R.id.process_layout_root).setVisibility(0);
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        if (exc instanceof a) {
            super.a(exc);
        } else {
            this.b.a((Throwable) exc);
            a((int) R.string.plus_error_profile);
        }
        this.f2072a.findViewById(R.id.process_layout_root).setVisibility(8);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        e eVar = (e) obj;
        super.a(eVar);
        this.f2072a.l = eVar;
        this.f2072a.v();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        super.b();
    }
}
