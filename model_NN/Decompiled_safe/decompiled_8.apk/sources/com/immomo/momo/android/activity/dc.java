package com.immomo.momo.android.activity;

import android.app.DatePickerDialog;
import android.widget.DatePicker;
import com.immomo.momo.R;
import com.immomo.momo.g;
import com.immomo.momo.util.ao;
import java.util.Calendar;

final class dc implements DatePickerDialog.OnDateSetListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ EditUserProfileActivity f1217a;

    dc(EditUserProfileActivity editUserProfileActivity) {
        this.f1217a = editUserProfileActivity;
    }

    public final void onDateSet(DatePicker datePicker, int i, int i2, int i3) {
        Calendar instance = Calendar.getInstance();
        instance.set(i, i2, i3);
        if (instance.getTime().after(this.f1217a.I) || instance.getTime().before(this.f1217a.H)) {
            ao.b(String.format(g.a((int) R.string.reg_age_range), 12, 100));
            return;
        }
        this.f1217a.t.setText(String.valueOf(i) + "-" + (i2 + 1) + "-" + i3);
    }
}
