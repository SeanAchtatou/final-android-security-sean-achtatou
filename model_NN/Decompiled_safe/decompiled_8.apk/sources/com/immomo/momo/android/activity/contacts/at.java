package com.immomo.momo.android.activity.contacts;

import com.immomo.momo.android.view.cv;
import java.util.Date;

final class at implements cv {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ an f1151a;

    private at(an anVar) {
        this.f1151a = anVar;
    }

    /* synthetic */ at(an anVar, byte b) {
        this(anVar);
    }

    public final void v() {
        this.f1151a.W = new Date();
        this.f1151a.O.n();
        this.f1151a.Q.e();
        this.f1151a.U = this.f1151a.V;
        if (this.f1151a.Y != null && !this.f1151a.Y.isCancelled()) {
            this.f1151a.Y.cancel(true);
        }
    }
}
