package com.immomo.momo.android.c;

import java.util.ArrayList;
import java.util.List;

public abstract class t implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private List f2393a;
    private boolean b;

    public t() {
        this.f2393a = new ArrayList();
        this.b = false;
    }

    public t(g gVar) {
        this();
        a(gVar);
    }

    public abstract void a();

    public final void a(g gVar) {
        this.f2393a.add(gVar);
    }

    public void a(Object obj) {
        this.b = true;
        try {
            for (g a2 : this.f2393a) {
                a2.a(obj);
            }
        } catch (OutOfMemoryError e) {
            for (g a3 : this.f2393a) {
                a3.a(null);
            }
        }
    }

    public final boolean d() {
        return this.b;
    }
}
