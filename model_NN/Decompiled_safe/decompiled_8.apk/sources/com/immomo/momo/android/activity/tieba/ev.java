package com.immomo.momo.android.activity.tieba;

import android.view.View;
import android.widget.AdapterView;

final class ev implements AdapterView.OnItemClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ TiebaProfileActivity f2273a;

    ev(TiebaProfileActivity tiebaProfileActivity) {
        this.f2273a = tiebaProfileActivity;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        switch (i) {
            case 0:
                TiebaProfileActivity.n(this.f2273a);
                return;
            case 1:
                if (!this.f2273a.l.l || !this.f2273a.l.m) {
                    if (this.f2273a.l.m) {
                        this.f2273a.b(new fb(this.f2273a, this.f2273a, 22));
                        return;
                    } else {
                        this.f2273a.b(new fb(this.f2273a, this.f2273a, 1));
                        return;
                    }
                }
            case 2:
                TiebaProfileActivity.p(this.f2273a);
                return;
            case 3:
                break;
            default:
                return;
        }
        TiebaProfileActivity.o(this.f2273a);
    }
}
