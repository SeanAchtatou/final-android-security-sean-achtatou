package com.immomo.momo.android.map;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import com.immomo.momo.service.bean.ay;

final class as implements AdapterView.OnItemClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ SelectSiteAMapActivity f2471a;

    as(SelectSiteAMapActivity selectSiteAMapActivity) {
        this.f2471a = selectSiteAMapActivity;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        ay ayVar = (ay) this.f2471a.q.getItem(i);
        Intent intent = new Intent();
        intent.putExtra("siteid", ayVar.f3001a);
        intent.putExtra("sitename", ayVar.f);
        intent.putExtra("sitetype", ayVar.d);
        this.f2471a.setResult(-1, intent);
        this.f2471a.finish();
    }
}
