package com.immomo.momo.android.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.util.m;

public class MomoProgressbar extends FrameLayout {

    /* renamed from: a  reason: collision with root package name */
    private View f2637a;
    private View b;
    private View c;
    private TextView d;
    private long e = 100;

    public MomoProgressbar(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        new m(this);
        this.f2637a = LayoutInflater.from(getContext()).inflate((int) R.layout.common_progressbar, (ViewGroup) null);
        addView(this.f2637a);
        this.b = this.f2637a.findViewById(R.id.progresssbarbg);
        this.c = this.f2637a.findViewById(R.id.progresssbarbg_inner);
        this.d = (TextView) this.f2637a.findViewById(R.id.progresssbar_txt);
    }

    public void setBackgroud(int i) {
        this.b.setBackgroundResource(i);
    }

    public void setInnderDrawable(int i) {
        this.c.setBackgroundResource(i);
    }

    public void setMax(long j) {
        if (j < 1) {
            this.e = 100;
        } else {
            this.e = j;
        }
    }

    public void setProgress(long j) {
        if (j > this.e) {
            long j2 = this.e;
        }
        int measuredWidth = (int) ((((long) getMeasuredWidth()) * j) / this.e);
        if (measuredWidth < 30 && measuredWidth != 0) {
            measuredWidth = 40;
        }
        if (measuredWidth < 0) {
            measuredWidth = 0;
        }
        ViewGroup.LayoutParams layoutParams = this.c.getLayoutParams();
        layoutParams.width = measuredWidth;
        this.c.setLayoutParams(layoutParams);
    }

    public void setProgressHeight(int i) {
        this.b.getLayoutParams().height = i;
        this.c.getLayoutParams().height = i;
        requestLayout();
    }

    public void setProgressText(String str) {
        this.d.setVisibility(0);
        this.d.setText(str);
    }
}
