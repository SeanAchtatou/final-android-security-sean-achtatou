package com.immomo.momo.android.activity;

import android.content.DialogInterface;
import com.immomo.momo.android.view.EmoteEditeText;

final class ik implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ OtherProfileV2Activity f1741a;
    private final /* synthetic */ EmoteEditeText b;

    ik(OtherProfileV2Activity otherProfileV2Activity, EmoteEditeText emoteEditeText) {
        this.f1741a = otherProfileV2Activity;
        this.b = emoteEditeText;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        OtherProfileV2Activity.a(this.f1741a, this.b);
        new ji(this.f1741a, this.f1741a, this.b.getText().toString().trim()).execute(new Object[0]);
        dialogInterface.dismiss();
    }
}
