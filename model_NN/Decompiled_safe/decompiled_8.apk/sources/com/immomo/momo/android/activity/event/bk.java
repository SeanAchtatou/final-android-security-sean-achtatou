package com.immomo.momo.android.activity.event;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import com.immomo.momo.service.bean.u;

final class bk implements AdapterView.OnItemClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ bd f1380a;

    bk(bd bdVar) {
        this.f1380a = bdVar;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        Intent intent = new Intent(this.f1380a.c(), EventProfileActivity.class);
        intent.putExtra("eventid", ((u) this.f1380a.V.getItem(i)).f3038a);
        this.f1380a.a(intent);
    }
}
