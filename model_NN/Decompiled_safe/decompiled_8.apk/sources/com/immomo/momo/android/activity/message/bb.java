package com.immomo.momo.android.activity.message;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.protocol.a.x;
import com.immomo.momo.service.bean.bh;
import java.util.Collection;
import java.util.List;

final class bb extends d {

    /* renamed from: a  reason: collision with root package name */
    private List f1935a;
    private /* synthetic */ ChatBGSettingActivity c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public bb(ChatBGSettingActivity chatBGSettingActivity, Context context) {
        super(context);
        this.c = chatBGSettingActivity;
    }

    /* access modifiers changed from: protected */
    public final Object a(Object... objArr) {
        this.f1935a = x.a().b();
        this.c.o.a(this.f1935a);
        return null;
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.android.a.a.a(java.util.Collection, boolean):void
     arg types: [java.util.List, int]
     candidates:
      com.immomo.momo.android.a.a.a(int, java.lang.Object):void
      com.immomo.momo.android.a.a.a(int, java.util.Collection):void
      com.immomo.momo.android.a.a.a(java.lang.Object, int):void
      com.immomo.momo.android.a.a.a(java.util.Collection, boolean):void */
    /* access modifiers changed from: protected */
    public final void a(Object obj) {
        bh e = this.c.j.e();
        this.f1935a.add((bh) this.c.j.getItem(this.c.j.getCount() - 1));
        this.c.j.a(false);
        this.c.j.a((Collection) this.f1935a, false);
        this.c.j.a(e);
        super.a(obj);
    }
}
