package com.immomo.momo.android.view;

import android.view.GestureDetector;
import android.view.MotionEvent;

final class as extends GestureDetector.SimpleOnGestureListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ FlingGallery f2727a;

    private as(FlingGallery flingGallery) {
        this.f2727a = flingGallery;
    }

    /* synthetic */ as(FlingGallery flingGallery, byte b) {
        this(flingGallery);
    }

    public final boolean onDown(MotionEvent motionEvent) {
        this.f2727a.f = true;
        this.f2727a.b = 0;
        return true;
    }

    public final boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
        if (Math.abs(motionEvent.getY() - motionEvent2.getY()) > 250.0f) {
            return false;
        }
        if (motionEvent2.getX() - motionEvent.getX() > 120.0f && Math.abs(f) > 400.0f) {
            this.f2727a.a();
        }
        if (motionEvent.getX() - motionEvent2.getX() <= 120.0f || Math.abs(f) <= 400.0f) {
            return false;
        }
        this.f2727a.b();
        return false;
    }

    public final void onLongPress(MotionEvent motionEvent) {
        this.f2727a.b = 0;
        this.f2727a.c();
    }

    public final boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
        if (motionEvent2.getAction() == 2) {
            if (!this.f2727a.g) {
                this.f2727a.f = true;
                this.f2727a.g = true;
                this.f2727a.b = 0;
                this.f2727a.i = System.currentTimeMillis();
                this.f2727a.h = (float) this.f2727a.n[this.f2727a.k].a();
            }
            float j = (((float) this.f2727a.e) / (((float) this.f2727a.f2629a) / 1000.0f)) * (((float) (System.currentTimeMillis() - this.f2727a.i)) / 1000.0f);
            float x = motionEvent.getX() - motionEvent2.getX();
            if (x < j * -1.0f) {
                x = j * -1.0f;
            }
            if (x <= j) {
                j = x;
            }
            int round = Math.round(j + this.f2727a.h);
            if (round >= this.f2727a.e) {
                round = this.f2727a.e;
            }
            if (round <= this.f2727a.e * -1) {
                round = this.f2727a.e * -1;
            }
            this.f2727a.n[0].a(round, this.f2727a.k);
            this.f2727a.n[1].a(round, this.f2727a.k);
            this.f2727a.n[2].a(round, this.f2727a.k);
        }
        return false;
    }

    public final void onShowPress(MotionEvent motionEvent) {
    }

    public final boolean onSingleTapUp(MotionEvent motionEvent) {
        this.f2727a.b = 0;
        return false;
    }
}
