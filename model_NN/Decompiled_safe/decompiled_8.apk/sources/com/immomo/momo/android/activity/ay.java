package com.immomo.momo.android.activity;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;

final class ay implements AdapterView.OnItemClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ BlacklistActivity f1035a;

    ay(BlacklistActivity blacklistActivity) {
        this.f1035a = blacklistActivity;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        Intent intent = new Intent(this.f1035a.getApplicationContext(), OtherProfileActivity.class);
        intent.putExtra("tag", "local");
        intent.putExtra("momoid", this.f1035a.l.getItem(i).h);
        this.f1035a.startActivity(intent);
    }
}
