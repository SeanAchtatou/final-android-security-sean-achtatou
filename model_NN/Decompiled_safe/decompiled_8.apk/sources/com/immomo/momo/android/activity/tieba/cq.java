package com.immomo.momo.android.activity.tieba;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import com.immomo.a.a.f.a;
import com.immomo.momo.service.bean.c.e;

final class cq implements AdapterView.OnItemClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ TiebaAdminActivity f2218a;

    cq(TiebaAdminActivity tiebaAdminActivity) {
        this.f2218a = tiebaAdminActivity;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        if (j == 2131166823) {
            e eVar = (e) this.f2218a.k.getItem(i);
            if ("p".equals(eVar.b)) {
                TiebaAdminActivity.a(this.f2218a, eVar);
            } else if ("c".equals(eVar.b)) {
                TiebaAdminActivity.b(this.f2218a, eVar);
            }
        } else {
            e eVar2 = (e) this.f2218a.k.getItem(i);
            if (!a.a(eVar2.e)) {
                Intent intent = new Intent(this.f2218a.getApplicationContext(), TieDetailActivity.class);
                intent.putExtra("key_tieid", eVar2.e);
                this.f2218a.startActivity(intent);
            }
        }
    }
}
