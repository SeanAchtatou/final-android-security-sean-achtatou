package com.immomo.momo.android.activity;

import android.view.MotionEvent;
import android.view.View;

final class mb implements View.OnTouchListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ UserRoamActivity f1905a;

    mb(UserRoamActivity userRoamActivity) {
        this.f1905a = userRoamActivity;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        if (motionEvent.getAction() == 0) {
            UserRoamActivity.x(this.f1905a);
            view.setPressed(true);
        } else if (motionEvent.getAction() == 1 || motionEvent.getAction() == 3) {
            if (this.f1905a.c()) {
                UserRoamActivity.y(this.f1905a);
            }
            view.setPressed(false);
        } else if (motionEvent.getAction() == 2 && !view.isPressed()) {
            view.setPressed(true);
        }
        return true;
    }
}
