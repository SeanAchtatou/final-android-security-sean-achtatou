package com.immomo.momo.android.activity.group;

import android.support.v4.b.a;
import com.immomo.momo.android.c.r;
import com.immomo.momo.service.bean.au;
import com.immomo.momo.util.j;

final class aa implements Runnable {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public /* synthetic */ EditGroupProfileActivity f1526a;

    aa(EditGroupProfileActivity editGroupProfileActivity) {
        this.f1526a = editGroupProfileActivity;
    }

    public final void run() {
        int size = this.f1526a.h.size();
        for (int i = 0; i < size; i++) {
            au auVar = (au) this.f1526a.h.get(i);
            if (!a.a((CharSequence) auVar.b)) {
                ab abVar = new ab(this, auVar);
                if (j.a(auVar.b) != null) {
                    abVar.a(j.a(auVar.b));
                } else {
                    new Thread(new r(auVar.b, abVar, 3, null)).start();
                }
            }
        }
    }
}
