package com.immomo.momo.android.b;

import android.content.Context;
import android.location.Location;
import android.os.Handler;
import android.support.v4.b.a;
import com.amap.mapapi.location.LocationManagerProxy;
import com.immomo.momo.R;
import com.immomo.momo.a.v;
import com.immomo.momo.a.w;
import com.immomo.momo.android.activity.ao;
import com.immomo.momo.android.view.a.n;
import com.immomo.momo.g;
import com.immomo.momo.util.ak;
import com.immomo.momo.util.m;
import java.util.Map;
import java.util.Vector;
import java.util.concurrent.ConcurrentHashMap;
import mm.purchasesdk.PurchaseCode;

public final class z {

    /* renamed from: a  reason: collision with root package name */
    private static Context f2343a;
    private static q b;
    private static a c;
    /* access modifiers changed from: private */
    public static m d = new m("LocationClient");
    private static Vector e = new Vector();
    private static z f;
    private static Map g = new ConcurrentHashMap();

    private z() {
        b = new q(f2343a);
        c = new a(f2343a);
    }

    public static o a(Context context, String str) {
        Object obj = new Object();
        o oVar = new o();
        aa aaVar = new aa(obj, oVar, context);
        try {
            g.put(str, aaVar);
            new Handler(context.getMainLooper()).post(new ac(aaVar));
            synchronized (obj) {
                obj.wait(61000);
            }
            if (oVar.b() == -1.0d || oVar.c() == -1.0d) {
                g.remove(str);
                return null;
            }
            g.remove(str);
            return oVar;
        } catch (Exception e2) {
            try {
                d.a((Throwable) e2);
                b(context, Integer.MAX_VALUE);
            } finally {
                g.remove(str);
            }
        }
    }

    public static String a(double d2, double d3, double d4, double d5) {
        q qVar = b;
        float[] fArr = new float[1];
        Location.distanceBetween(d2, d3, d4, d5, fArr);
        float f2 = fArr[0];
        return f2 == -1.0f ? g.a((int) R.string.profile_distance_unknown) : f2 == -2.0f ? g.a((int) R.string.profile_distance_hide) : String.valueOf(a.a(f2 / 1000.0f)) + "km";
    }

    public static synchronized void a() {
        synchronized (z.class) {
            c();
            e.clear();
        }
    }

    public static void a(Context context) {
        f2343a = context;
        if (f == null) {
            f = new z();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.ak.a(java.lang.String, java.lang.Integer):int
     arg types: [java.lang.String, int]
     candidates:
      com.immomo.momo.util.ak.a(android.content.Context, java.lang.String):com.immomo.momo.util.ak
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Long):long
      com.immomo.momo.util.ak.a(java.lang.String, int):void
      com.immomo.momo.util.ak.a(java.lang.String, long):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.String):void
      com.immomo.momo.util.ak.a(java.lang.String, boolean):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Boolean):boolean
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Integer):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, int]
     candidates:
      com.immomo.momo.util.ak.a(android.content.Context, java.lang.String):com.immomo.momo.util.ak
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Integer):int
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Long):long
      com.immomo.momo.util.ak.a(java.lang.String, int):void
      com.immomo.momo.util.ak.a(java.lang.String, long):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.String):void
      com.immomo.momo.util.ak.a(java.lang.String, boolean):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Boolean):boolean
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void */
    public static synchronized void a(p pVar) {
        int i;
        synchronized (z.class) {
            if (e.size() <= 0) {
                e.add(pVar);
                af afVar = new af();
                try {
                    if (!g.K()) {
                        if (g.y()) {
                            ak akVar = g.d().f668a;
                            if (akVar != null) {
                                int a2 = akVar.a("momolocate_locater_type", (Integer) 203);
                                d.a((Object) ("current locater_type: " + a2));
                                ag agVar = new ag(afVar, akVar);
                                switch (a2) {
                                    case PurchaseCode.LOADCHANNEL_ERR /*200*/:
                                        b.a(agVar);
                                        break;
                                    case 201:
                                        c.a(agVar);
                                        break;
                                    case 202:
                                    default:
                                        a(afVar, akVar);
                                        break;
                                    case 203:
                                        a(afVar, akVar);
                                        break;
                                }
                            }
                        } else {
                            g.a((int) R.string.errormsg_network_unfind);
                            throw new w();
                        }
                    } else {
                        throw new v(g.a((int) R.string.errormsg_location_monilocationset));
                    }
                } catch (Exception e2) {
                    if (e2 instanceof w) {
                        i = PurchaseCode.AUTH_OK;
                    } else if (e2 instanceof v) {
                        i = 105;
                    } else {
                        ak akVar2 = g.d().f668a;
                        if (akVar2 != null) {
                            akVar2.a("momolocate_locater_type", (Object) 203);
                        }
                        i = PurchaseCode.QUERY_OK;
                    }
                    b(null, 0, i, PurchaseCode.LOADCHANNEL_ERR);
                }
            } else {
                e.add(pVar);
            }
        }
    }

    private static void a(p pVar, ak akVar) {
        d.a((Object) "AllType locating....");
        Object obj = new Object();
        ah ahVar = new ah(obj, akVar, pVar);
        ai aiVar = new ai(obj, pVar, akVar);
        aj ajVar = new aj(obj, pVar, akVar);
        try {
            b.a(ahVar);
        } catch (Exception e2) {
            d.a((Throwable) e2);
        }
        try {
            c.a(aiVar);
        } catch (Exception e3) {
            d.a((Throwable) e3);
        }
        new Thread(new ab(obj, ajVar, aiVar, ahVar, pVar, akVar)).start();
    }

    public static boolean a(double d2, double d3) {
        return !(d2 == 0.0d && d3 == 0.0d) && d2 >= -90.0d && d2 <= 90.0d && d3 >= -180.0d && d3 <= 180.0d;
    }

    public static boolean a(long j) {
        return Math.abs(j - System.currentTimeMillis()) > 600000;
    }

    public static boolean a(Location location) {
        if (location == null) {
            return false;
        }
        double latitude = location.getLatitude();
        double longitude = location.getLongitude();
        return !(latitude == 0.0d && longitude == 0.0d) && latitude >= -90.0d && latitude <= 90.0d && longitude >= -180.0d && longitude <= 180.0d;
    }

    public static boolean a(String str) {
        p pVar = (p) g.get(str);
        if (pVar == null || !pVar.b) {
            return false;
        }
        pVar.b = false;
        b(pVar);
        return true;
    }

    public static Location b() {
        try {
            return b.b();
        } catch (Exception e2) {
            d.a((Throwable) e2);
            return null;
        }
    }

    /* access modifiers changed from: private */
    public static void b(Context context, int i) {
        if (context instanceof ao) {
            ao aoVar = (ao) context;
            if (aoVar.isFinishing()) {
                return;
            }
            if (i == 104) {
                aoVar.b((int) R.string.errormsg_network_unfind);
            } else if (i == 105) {
                n nVar = new n(aoVar);
                nVar.a();
                nVar.a((int) R.string.errormsg_location_monilocationset);
                nVar.a(0, (int) R.string.dialog_btn_confim, new ad(aoVar));
                nVar.a(1, (int) R.string.dialog_btn_cancel, new ae());
                nVar.show();
            } else {
                aoVar.b((int) R.string.errormsg_location_nearby_failed);
            }
        }
    }

    /* access modifiers changed from: private */
    public static synchronized void b(Location location, int i, int i2, int i3) {
        synchronized (z.class) {
            while (e.size() > 0) {
                p pVar = (p) e.remove(0);
                if (pVar != null) {
                    pVar.a(location, i, i2, i3);
                }
            }
        }
    }

    public static synchronized void b(p pVar) {
        synchronized (z.class) {
            e.removeElement(pVar);
            if (e.size() <= 0) {
                c();
            }
        }
    }

    public static void b(String str) {
        b.a(str);
        c.a(str);
    }

    public static void c() {
        b.a();
        c.a();
    }

    public static void c(p pVar) {
        Location location = new Location(LocationManagerProxy.NETWORK_PROVIDER);
        location.setLatitude(39.99403d);
        location.setLongitude(116.339293d);
        pVar.a(location, 0, 100, PurchaseCode.LOADCHANNEL_ERR);
    }

    public static boolean d() {
        return b.c();
    }
}
