package com.immomo.momo.android.activity.message;

import android.content.Intent;
import android.support.v4.b.a;
import com.immomo.momo.android.broadcast.d;
import com.immomo.momo.android.broadcast.e;

final class bx implements d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ HiSessionListActivity f1957a;

    bx(HiSessionListActivity hiSessionListActivity) {
        this.f1957a = hiSessionListActivity;
    }

    public final void a(Intent intent) {
        if (intent.getAction().equals(e.b)) {
            String stringExtra = intent.getStringExtra("key_momoid");
            if (!a.a((CharSequence) stringExtra)) {
                HiSessionListActivity.a(this.f1957a, stringExtra);
            }
        }
    }
}
