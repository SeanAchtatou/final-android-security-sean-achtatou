package com.immomo.momo.android.activity.plugin;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;

final class i extends d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ BindFaceBookActivity f2088a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public i(BindFaceBookActivity bindFaceBookActivity, Context context) {
        super(context);
        this.f2088a = bindFaceBookActivity;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        String[] strArr = {"255035774601283", "6fb09a87d43d74c69197a0088334f7d6"};
        if (strArr.length != 2) {
            return false;
        }
        this.f2088a.m = strArr[0];
        BindFaceBookActivity bindFaceBookActivity = this.f2088a;
        BindFaceBookActivity.u();
        this.f2088a.n = "https://m.facebook.com/dialog/oauth?display=touch&client_id=" + this.f2088a.m + "&scope=publish_stream,read_stream,offline_access,user_about_me,friends_about_me,user_events,user_hometown,user_interests,user_likes,user_location,user_photos,user_relationships,friends_relationships,user_relationship_details,friends_relationship_details,user_status,user_website,read_friendlists,xmpp_login,create_event,publish_checkins,publish_stream,publish_actions,manage_pages&type=user_agent&redirect_uri=" + this.f2088a.i;
        this.b.b((Object) ("authUrl:" + this.f2088a.n));
        this.f2088a.j.loadUrl(this.f2088a.n);
        return true;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.f2088a.l = new v(this.f2088a, "正在请求数据，请稍候...");
        this.f2088a.l.setOnCancelListener(new j(this));
        this.f2088a.l.show();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        if (this.f2088a.l != null && this.f2088a.l.isShowing()) {
            this.f2088a.l.dismiss();
        }
    }
}
