package com.immomo.momo.android.activity.feed;

import android.content.Intent;
import android.support.v4.b.a;
import com.immomo.momo.android.broadcast.d;
import com.immomo.momo.android.broadcast.f;
import com.immomo.momo.android.broadcast.p;
import com.immomo.momo.service.bean.ab;

final class af implements d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ MyFeedListActivity f1437a;

    af(MyFeedListActivity myFeedListActivity) {
        this.f1437a = myFeedListActivity;
    }

    public final void a(Intent intent) {
        ab a2;
        if (f.f2349a.equals(intent.getAction())) {
            String str = intent.getExtras() != null ? (String) intent.getExtras().get("feedid") : null;
            String str2 = intent.getExtras() != null ? (String) intent.getExtras().get("userid") : null;
            if (!a.a((CharSequence) str) && this.f1437a.f.h.equals(str2)) {
                this.f1437a.o.a(str);
            }
        } else if (p.b.equals(intent.getAction()) && (a2 = this.f1437a.l.a(intent.getStringExtra("feedid"))) != null && !this.f1437a.o.a().contains(a2)) {
            this.f1437a.o.b(0, a2);
        }
    }
}
