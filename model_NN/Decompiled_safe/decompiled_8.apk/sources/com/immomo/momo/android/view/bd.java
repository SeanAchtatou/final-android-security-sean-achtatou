package com.immomo.momo.android.view;

import android.view.View;
import android.widget.AdapterView;

final class bd implements AdapterView.OnItemLongClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ az f2738a;

    private bd(az azVar) {
        this.f2738a = azVar;
    }

    /* synthetic */ bd(az azVar, byte b) {
        this(azVar);
    }

    public final boolean onItemLongClick(AdapterView adapterView, View view, int i, long j) {
        if (this.f2738a.i == null) {
            return false;
        }
        return this.f2738a.i.onItemLongClick(adapterView, view, i - this.f2738a.getHeaderViewsCount(), j);
    }
}
