package com.immomo.momo.android.activity.feed;

import android.content.Context;
import android.content.Intent;
import com.immomo.momo.android.broadcast.f;
import com.immomo.momo.android.c.b;
import com.immomo.momo.protocol.a.k;
import com.immomo.momo.service.bean.ab;

final class x extends b {
    private ab c;
    private /* synthetic */ FeedProfileActivity d;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public x(FeedProfileActivity feedProfileActivity, Context context, ab abVar) {
        super(context);
        this.d = feedProfileActivity;
        this.c = abVar;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        String b = k.a().b(this.c.h);
        this.d.m.f(this.c.h);
        Intent intent = new Intent(f.f2349a);
        intent.putExtra("feedid", this.c.h);
        intent.putExtra("siteid", this.c.e);
        intent.putExtra("userid", this.d.f.h);
        this.d.sendBroadcast(intent);
        return b;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        a((String) obj);
        this.c.i = 2;
        this.d.finish();
    }
}
