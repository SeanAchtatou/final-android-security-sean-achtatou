package com.immomo.momo.android.activity;

import android.content.Intent;
import android.support.v4.b.a;
import com.immomo.momo.android.broadcast.d;
import com.immomo.momo.android.broadcast.u;

final class eq implements d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ GroupLevelActivity f1341a;

    eq(GroupLevelActivity groupLevelActivity) {
        this.f1341a = groupLevelActivity;
    }

    public final void a(Intent intent) {
        if (u.g.equals(intent.getAction())) {
            String stringExtra = intent.getStringExtra("gid");
            if (!a.a((CharSequence) stringExtra) && stringExtra.equals(this.f1341a.r.b)) {
                this.f1341a.r = this.f1341a.o.e(stringExtra);
                this.f1341a.u();
            }
        }
    }
}
