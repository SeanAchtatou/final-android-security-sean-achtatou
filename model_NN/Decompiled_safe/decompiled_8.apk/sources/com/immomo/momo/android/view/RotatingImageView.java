package com.immomo.momo.android.view;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;

public class RotatingImageView extends ImageView {

    /* renamed from: a  reason: collision with root package name */
    private int f2653a;
    private dc b = null;

    public RotatingImageView(Context context) {
        super(context);
        a();
    }

    public RotatingImageView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a();
    }

    public RotatingImageView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        a();
    }

    private void a() {
        this.f2653a = 0;
        this.b = new dc(getContext(), new LinearInterpolator(), (byte) 0);
    }

    public final void a(int i, int i2, int i3) {
        if (!this.b.a()) {
            this.b.e();
        }
        this.b.a(i, 0, i2, 0, i3);
        invalidate();
    }

    public void computeScroll() {
        if (this.b.d()) {
            this.f2653a = this.b.b();
            invalidate();
        }
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        int top = getTop();
        int left = getLeft();
        int bottom = getBottom() - top;
        int save = canvas.save();
        canvas.rotate((float) this.f2653a, ((float) (getRight() - left)) / 2.0f, ((float) bottom) / 2.0f);
        super.onDraw(canvas);
        canvas.restoreToCount(save);
    }

    public void setDegress(int i) {
        this.f2653a = i;
        invalidate();
    }
}
