package com.immomo.momo.android.activity.tieba;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.protocol.a.v;
import com.immomo.momo.service.ao;
import com.immomo.momo.service.bean.c.f;
import com.immomo.momo.util.u;
import java.util.Collection;
import java.util.Date;
import java.util.List;

final class df extends d {

    /* renamed from: a  reason: collision with root package name */
    private f f2234a = new f();
    private /* synthetic */ TiebaCategoryDetailActivity c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public df(TiebaCategoryDetailActivity tiebaCategoryDetailActivity, Context context) {
        super(context);
        this.c = tiebaCategoryDetailActivity;
        if (tiebaCategoryDetailActivity.o != null) {
            tiebaCategoryDetailActivity.o.cancel(true);
        }
        if (tiebaCategoryDetailActivity.p != null && tiebaCategoryDetailActivity.p.isCancelled()) {
            tiebaCategoryDetailActivity.p.cancel(true);
        }
        tiebaCategoryDetailActivity.o = this;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        boolean a2 = v.a().a(this.c.q, 0, this.f2234a);
        ao d = this.c.n;
        List list = this.f2234a.f;
        String c2 = this.c.q;
        d.a(list);
        u.a("tiebacatogrylists" + c2, list);
        return Boolean.valueOf(a2);
    }

    /* access modifiers changed from: protected */
    public final void a() {
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        Boolean bool = (Boolean) obj;
        this.c.g.a("categorydetail_latttime_reflush", this.c.l);
        this.c.l = new Date();
        this.c.m.b();
        this.c.m.b((Collection) this.f2234a.f);
        this.c.m.notifyDataSetChanged();
        this.c.v.clear();
        this.c.s = String.valueOf(this.c.r) + "(" + this.f2234a.e + ")";
        this.c.setTitle(this.c.s);
        for (com.immomo.momo.service.bean.c.d add : this.f2234a.f) {
            this.c.v.add(add);
        }
        if (bool.booleanValue()) {
            this.c.u.setVisibility(0);
        } else {
            this.c.u.setVisibility(8);
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.c.k.n();
        this.c.l = new Date();
        this.c.o = (df) null;
        this.c.k.setLastFlushTime(this.c.l);
    }
}
