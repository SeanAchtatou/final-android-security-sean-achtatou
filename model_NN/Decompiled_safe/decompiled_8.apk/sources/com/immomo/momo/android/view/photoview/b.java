package com.immomo.momo.android.view.photoview;

import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

final class b extends GestureDetector.SimpleOnGestureListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ a f2818a;

    b(a aVar) {
        this.f2818a = aVar;
    }

    public final void onLongPress(MotionEvent motionEvent) {
        if (this.f2818a.s != null) {
            this.f2818a.s.onLongClick((View) this.f2818a.g.get());
        }
    }
}
