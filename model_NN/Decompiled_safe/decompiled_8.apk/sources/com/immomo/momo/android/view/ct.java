package com.immomo.momo.android.view;

import android.os.Handler;
import android.os.Message;

final class ct extends Handler {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ PushDownLayout f2768a;

    ct(PushDownLayout pushDownLayout) {
        this.f2768a = pushDownLayout;
    }

    public final void handleMessage(Message message) {
        switch (message.what) {
            case 11:
                if (this.f2768a.c) {
                    this.f2768a.f2648a = 0.0f;
                    ct.super.setVisibility(8);
                    return;
                }
                if (!this.f2768a.d) {
                    this.f2768a.f2648a = 0.0f;
                    this.f2768a.d = true;
                }
                PushDownLayout pushDownLayout = this.f2768a;
                pushDownLayout.f2648a = pushDownLayout.f2648a - 0.2f;
                this.f2768a.c = this.f2768a.f2648a <= 0.0f;
                this.f2768a.requestLayout();
                sendEmptyMessageDelayed(11, 0);
                return;
            case 12:
                if (!this.f2768a.b) {
                    if (!this.f2768a.d) {
                        this.f2768a.f2648a = 1.0f;
                        this.f2768a.d = true;
                    }
                    PushDownLayout pushDownLayout2 = this.f2768a;
                    pushDownLayout2.f2648a = pushDownLayout2.f2648a + 0.1f;
                    if (this.f2768a.f2648a >= 1.0f) {
                        this.f2768a.f2648a = 1.0f;
                        this.f2768a.b = true;
                    }
                    this.f2768a.requestLayout();
                    sendEmptyMessageDelayed(12, 0);
                    return;
                }
                return;
            default:
                return;
        }
    }
}
