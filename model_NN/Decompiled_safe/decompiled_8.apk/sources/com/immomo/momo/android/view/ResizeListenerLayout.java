package com.immomo.momo.android.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

public class ResizeListenerLayout extends RelativeLayout {

    /* renamed from: a  reason: collision with root package name */
    private cx f2651a;

    public ResizeListenerLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        if (this.f2651a != null) {
            this.f2651a.a(i2, i4);
        }
    }

    public void setOnResizeListener(cx cxVar) {
        this.f2651a = cxVar;
    }
}
