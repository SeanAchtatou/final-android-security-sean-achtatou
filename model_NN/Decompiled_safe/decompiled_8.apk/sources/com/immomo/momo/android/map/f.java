package com.immomo.momo.android.map;

import android.location.Location;
import com.amap.mapapi.core.GeoPoint;
import com.immomo.momo.R;
import com.immomo.momo.android.b.p;
import com.immomo.momo.android.b.z;

final class f extends p {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ e f2501a;

    f(e eVar) {
        this.f2501a = eVar;
    }

    public final void a(Location location, int i, int i2, int i3) {
        this.f2501a.f2500a.b();
        if (z.a(location)) {
            AMapActivity.a(this.f2501a.f2500a, new GeoPoint((int) (location.getLatitude() * 1000000.0d), (int) (location.getLongitude() * 1000000.0d)));
        } else {
            this.f2501a.f2500a.a((int) R.string.errormsg_location_failed);
        }
    }
}
