package com.immomo.momo.android.activity.event;

import android.content.Context;
import android.widget.ListAdapter;
import com.immomo.momo.android.c.d;
import com.immomo.momo.protocol.a.j;
import com.immomo.momo.service.r;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

final class ad extends d {

    /* renamed from: a  reason: collision with root package name */
    private List f1349a = new ArrayList();
    private /* synthetic */ EventFeedsActivity c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ad(EventFeedsActivity eventFeedsActivity, Context context) {
        super(context);
        this.c = eventFeedsActivity;
        if (eventFeedsActivity.n != null) {
            eventFeedsActivity.n.cancel(true);
        }
        eventFeedsActivity.n = this;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        this.f1349a.clear();
        boolean a2 = j.a().a(this.f1349a, 0, this.c.h);
        r unused = this.c.q;
        String unused2 = this.c.h;
        List list = this.f1349a;
        return Boolean.valueOf(a2);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        Boolean bool = (Boolean) obj;
        super.a(bool);
        Date date = new Date();
        this.c.i.setLastFlushTime(date);
        this.c.g.a("neventfeeds_lasttime_success", date);
        this.c.g.c("neventfeeds_remain", bool);
        this.c.j.clear();
        this.c.j.addAll(this.f1349a);
        this.c.i.setAdapter((ListAdapter) this.c.k);
        if (!bool.booleanValue()) {
            this.c.i.k();
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
        super.b();
        this.c.i.n();
        this.c.m = new Date();
        this.c.g.a("neventfeeds_latttime_reflush", this.c.m);
        this.c.n = (ad) null;
    }
}
