package com.immomo.momo.android.a;

import android.view.View;

final class af implements View.OnLongClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ae f713a;
    private final /* synthetic */ int b;

    af(ae aeVar, int i) {
        this.f713a = aeVar;
        this.b = i;
    }

    public final boolean onLongClick(View view) {
        return this.f713a.f.getOnItemLongClickListenerInWrapper().onItemLongClick(this.f713a.f, view, this.b, (long) view.getId());
    }
}
