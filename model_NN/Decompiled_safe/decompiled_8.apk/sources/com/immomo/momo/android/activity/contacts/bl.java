package com.immomo.momo.android.activity.contacts;

import android.content.Intent;
import com.immomo.momo.android.a.hf;
import com.immomo.momo.android.broadcast.d;
import com.immomo.momo.android.broadcast.u;
import com.immomo.momo.service.bean.a.a;

final class bl implements d {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ bk f1166a;

    bl(bk bkVar) {
        this.f1166a = bkVar;
    }

    public final void a(Intent intent) {
        a e;
        if (u.f2364a.equals(intent.getAction())) {
            String stringExtra = intent.getStringExtra("gid");
            if (!android.support.v4.b.a.a((CharSequence) stringExtra) && (e = this.f1166a.T.e(stringExtra)) != null && !this.f1166a.Q.contains(e)) {
                this.f1166a.P.a(e);
                this.f1166a.aa.post(new bm(this));
            }
        } else if (u.b.equals(intent.getAction())) {
            if (!android.support.v4.b.a.a((CharSequence) intent.getStringExtra("gid"))) {
                this.f1166a.Q = this.f1166a.T.c();
                if (this.f1166a.P == null) {
                    this.f1166a.O();
                    return;
                }
                this.f1166a.P.a(this.f1166a.Q);
                this.f1166a.aa.post(new bn(this));
            }
        } else if (u.c.equals(intent.getAction())) {
            String stringExtra2 = intent.getStringExtra("gid");
            if (!android.support.v4.b.a.a((CharSequence) stringExtra2)) {
                int a2 = this.f1166a.P.a(stringExtra2, 0);
                if (a2 < 0 || a2 > this.f1166a.P.getCount()) {
                    this.f1166a.Q = this.f1166a.T.c();
                    this.f1166a.aa.post(new bo(this));
                    return;
                }
                ((hf) this.f1166a.P.getItem(a2)).f.G = 4;
                this.f1166a.aa.post(new bp(this));
            }
        } else if (u.d.equals(intent.getAction())) {
            String stringExtra3 = intent.getStringExtra("gid");
            if (!android.support.v4.b.a.a((CharSequence) stringExtra3)) {
                int a3 = this.f1166a.P.a(stringExtra3, 0);
                if (a3 < 0 || a3 > this.f1166a.P.getCount()) {
                    this.f1166a.Q = this.f1166a.T.c();
                    this.f1166a.P.a(this.f1166a.Q);
                    return;
                }
                ((hf) this.f1166a.P.getItem(a3)).f.G = 2;
                this.f1166a.aa.post(new bq(this));
            }
        } else if (u.g.equals(intent.getAction())) {
            String str = intent.getExtras() != null ? (String) intent.getExtras().get("gid") : null;
            if (!android.support.v4.b.a.a((CharSequence) str) && this.f1166a.P != null) {
                new Thread(new br(this, str)).start();
            }
        }
    }
}
