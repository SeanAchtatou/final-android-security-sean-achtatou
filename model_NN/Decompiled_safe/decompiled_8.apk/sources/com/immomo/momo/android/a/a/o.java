package com.immomo.momo.android.a.a;

import android.view.View;

final class o implements View.OnLongClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ m f697a;
    private final /* synthetic */ int b;

    o(m mVar, int i) {
        this.f697a = mVar;
        this.b = i;
    }

    public final boolean onLongClick(View view) {
        return this.f697a.e.getOnItemLongClickListenerInWrapper().onItemLongClick(this.f697a.e, view, this.b, (long) view.getId());
    }
}
