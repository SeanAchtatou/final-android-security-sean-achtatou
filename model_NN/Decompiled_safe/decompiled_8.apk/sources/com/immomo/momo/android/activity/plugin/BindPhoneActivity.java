package com.immomo.momo.android.activity.plugin;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.b.a;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.ao;
import com.immomo.momo.android.view.HeaderLayout;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.util.k;

public class BindPhoneActivity extends ao implements View.OnClickListener {
    private EditText h = null;
    /* access modifiers changed from: private */
    public TextView i = null;
    private EditText j = null;
    private HeaderLayout k = null;
    /* access modifiers changed from: private */
    public String l = "+86";
    /* access modifiers changed from: private */
    public String m = PoiTypeDef.All;
    /* access modifiers changed from: private */
    public String n = PoiTypeDef.All;
    /* access modifiers changed from: private */
    public String[] o = null;
    /* access modifiers changed from: private */
    public String p = PoiTypeDef.All;
    /* access modifiers changed from: private */
    public v q = null;
    /* access modifiers changed from: private */
    public n r;

    private static boolean a(EditText editText) {
        String trim = editText.getText().toString().trim();
        return trim == null || trim.length() <= 0;
    }

    private boolean f() {
        if (a(this.j)) {
            com.immomo.momo.util.ao.b("请输入手机号码");
            this.j.requestFocus();
            return false;
        } else if (this.j.getText().length() < 3) {
            a((CharSequence) "手机号码不得小于三位数");
            this.j.requestFocus();
            return false;
        } else if ("+86".equals(this.l) && this.j.getText().length() != 11) {
            new k("U", "U12").e();
            a((CharSequence) "手机号码格式错误");
            this.j.requestFocus();
            return false;
        } else if (a(this.h)) {
            a((CharSequence) "请输入当前密码");
            this.h.requestFocus();
            return false;
        } else {
            this.n = this.h.getText().toString();
            this.m = this.j.getText().toString().trim();
            return true;
        }
    }

    /* access modifiers changed from: private */
    public void g() {
        Intent intent = new Intent(this, SubmitVerifyCodeActivity.class);
        intent.putExtra("areacode", this.l);
        intent.putExtra("phonenumber", this.m);
        intent.putExtra("password", this.n);
        startActivityForResult(intent, 20);
    }

    /* access modifiers changed from: protected */
    public final void d() {
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        if (i2 == 20 && i3 == -1) {
            setResult(-1, intent);
            finish();
        }
        super.onActivityResult(i2, i3, intent);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_back /*2131165286*/:
                finish();
                return;
            case R.id.btn_next /*2131165417*/:
                if (!f()) {
                    return;
                }
                if (this.p.equals(String.valueOf(this.l) + this.m + this.n)) {
                    g();
                    return;
                } else {
                    b(new n(this, this));
                    return;
                }
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_bindphone);
        this.o = a.d();
        this.h = (EditText) findViewById(R.id.input_password);
        this.i = (TextView) findViewById(R.id.input_areacode);
        this.j = (EditText) findViewById(R.id.input_phonenumber);
        this.k = (HeaderLayout) findViewById(R.id.layout_header);
        this.k.setTitleText("绑定手机");
        findViewById(R.id.btn_next).setOnClickListener(this);
        findViewById(R.id.btn_back).setOnClickListener(this);
        this.i.setOnClickListener(new l(this));
        this.e.a((Object) "onCreate...");
    }
}
