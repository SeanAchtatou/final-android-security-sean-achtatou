package com.immomo.momo.android.game;

import android.content.Context;
import com.immomo.momo.a.a;
import com.immomo.momo.a.s;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.m;

final class ah extends d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ac f2404a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ah(ac acVar, Context context) {
        super(context);
        this.f2404a = acVar;
    }

    /* access modifiers changed from: protected */
    public final Object a(Object... objArr) {
        m a2 = m.a();
        String str = this.f2404a.Q;
        String str2 = this.f2404a.R;
        ai aiVar = this.f2404a.O;
        a2.a(str, str2, this.f2404a.S);
        return null;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.f2404a.a(new v(this.f2404a.c(), "正在支付...", this));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.android.game.ac.a(com.immomo.momo.android.game.ac, boolean):void
     arg types: [com.immomo.momo.android.game.ac, int]
     candidates:
      com.immomo.momo.android.activity.lh.a(int, java.lang.String[]):com.immomo.momo.protocol.imjson.c.a
      com.immomo.momo.android.activity.lh.a(android.content.Context, com.immomo.momo.android.view.HeaderLayout):void
      com.immomo.momo.android.activity.lh.a(int, android.view.KeyEvent):boolean
      com.immomo.momo.android.activity.lh.a(android.os.Bundle, java.lang.String):boolean
      com.immomo.momo.android.activity.ar.a(android.view.LayoutInflater, android.view.ViewGroup):android.view.View
      android.support.v4.app.Fragment.a(android.content.Context, java.lang.String):android.support.v4.app.Fragment
      android.support.v4.app.Fragment.a(android.view.LayoutInflater, android.view.ViewGroup):android.view.View
      android.support.v4.app.Fragment.a(int, android.support.v4.app.Fragment):void
      android.support.v4.app.Fragment.a(android.content.Intent, int):void
      android.support.v4.app.Fragment.a(android.view.Menu, android.view.MenuInflater):boolean
      com.immomo.momo.android.game.ac.a(com.immomo.momo.android.game.ac, boolean):void */
    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        int i;
        if (exc instanceof a) {
            a aVar = (a) exc;
            i = aVar.f670a > 0 ? aVar.f670a : 0;
            this.f2404a.T = exc instanceof s;
        } else {
            i = 0;
        }
        if (i == 30213) {
            a("余额不足");
        } else if (i == 30210) {
            this.f2404a.e(false);
        } else {
            a("支付失败，请重试");
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.android.game.ac.a(com.immomo.momo.android.game.ac, boolean):void
     arg types: [com.immomo.momo.android.game.ac, int]
     candidates:
      com.immomo.momo.android.activity.lh.a(int, java.lang.String[]):com.immomo.momo.protocol.imjson.c.a
      com.immomo.momo.android.activity.lh.a(android.content.Context, com.immomo.momo.android.view.HeaderLayout):void
      com.immomo.momo.android.activity.lh.a(int, android.view.KeyEvent):boolean
      com.immomo.momo.android.activity.lh.a(android.os.Bundle, java.lang.String):boolean
      com.immomo.momo.android.activity.ar.a(android.view.LayoutInflater, android.view.ViewGroup):android.view.View
      android.support.v4.app.Fragment.a(android.content.Context, java.lang.String):android.support.v4.app.Fragment
      android.support.v4.app.Fragment.a(android.view.LayoutInflater, android.view.ViewGroup):android.view.View
      android.support.v4.app.Fragment.a(int, android.support.v4.app.Fragment):void
      android.support.v4.app.Fragment.a(android.content.Intent, int):void
      android.support.v4.app.Fragment.a(android.view.Menu, android.view.MenuInflater):boolean
      com.immomo.momo.android.game.ac.a(com.immomo.momo.android.game.ac, boolean):void */
    /* access modifiers changed from: protected */
    public final void a(Object obj) {
        this.f2404a.e(true);
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.f2404a.N();
        this.f2404a.T = false;
    }
}
