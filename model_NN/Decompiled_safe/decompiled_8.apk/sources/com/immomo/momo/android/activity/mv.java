package com.immomo.momo.android.activity;

import android.content.Intent;

final class mv implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ WebviewActivity f2013a;
    private final /* synthetic */ Intent b;

    mv(WebviewActivity webviewActivity, Intent intent) {
        this.f2013a = webviewActivity;
        this.b = intent;
    }

    public final void run() {
        this.f2013a.startActivity(Intent.createChooser(this.b, this.f2013a.j == null ? "打开应用" : this.f2013a.j));
        this.f2013a.finish();
    }
}
