package com.immomo.momo.android.pay;

import android.content.DialogInterface;
import com.immomo.momo.android.view.a.ao;
import java.io.File;

final class af implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ae f2525a;
    private final /* synthetic */ ao b;
    private final /* synthetic */ String c;
    private final /* synthetic */ File d;

    af(ae aeVar, ao aoVar, String str, File file) {
        this.f2525a = aeVar;
        this.b = aoVar;
        this.c = str;
        this.d = file;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        new aj(this.f2525a.f2524a.f2523a, this.f2525a.f2524a.f2523a, this.b.e(), this.b.f(), this.b.g(), this.b.h(), this.c.trim(), this.d).execute(new String[0]);
        dialogInterface.dismiss();
    }
}
