package com.immomo.momo.android.a.a;

import android.view.View;

final class ak implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ah f680a;
    private final /* synthetic */ int b;

    ak(ah ahVar, int i) {
        this.f680a = ahVar;
        this.b = i;
    }

    public final void onClick(View view) {
        this.f680a.e.getOnItemClickListener().onItemClick(this.f680a.e, view, this.b, (long) view.getId());
    }
}
