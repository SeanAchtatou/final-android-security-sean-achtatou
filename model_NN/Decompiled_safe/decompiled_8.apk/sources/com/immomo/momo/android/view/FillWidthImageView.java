package com.immomo.momo.android.view;

import android.content.Context;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.ImageView;
import com.immomo.momo.util.m;

public class FillWidthImageView extends ImageView {

    /* renamed from: a  reason: collision with root package name */
    private final Matrix f2628a = new Matrix();
    private final Matrix b = new Matrix();
    private final RectF c = new RectF();
    private boolean d = false;
    private int e;
    private int f;

    public FillWidthImageView(Context context) {
        super(context);
        new m(this);
        super.setScaleType(ImageView.ScaleType.MATRIX);
        this.d = true;
    }

    public FillWidthImageView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        new m(this);
        super.setScaleType(ImageView.ScaleType.MATRIX);
        this.d = true;
    }

    public FillWidthImageView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        new m(this);
        super.setScaleType(ImageView.ScaleType.MATRIX);
        this.d = true;
    }

    /* access modifiers changed from: private */
    public void a() {
        RectF rectF;
        Matrix displayMatrix = getDisplayMatrix();
        Drawable drawable = getDrawable();
        if (drawable != null) {
            this.c.set(0.0f, 0.0f, (float) drawable.getIntrinsicWidth(), (float) drawable.getIntrinsicHeight());
            displayMatrix.mapRect(this.c);
            rectF = this.c;
        } else {
            rectF = null;
        }
        if (rectF != null) {
            float height = rectF.height();
            float width = rectF.width();
            float width2 = (float) getWidth();
            displayMatrix.postScale(width2 / width, width2 / width);
            this.f = (int) (height * (width2 / width));
            this.e = (int) width2;
            setImageMatrix(displayMatrix);
            requestLayout();
        }
    }

    /* access modifiers changed from: protected */
    public Matrix getDisplayMatrix() {
        this.f2628a.set(this.b);
        return this.f2628a;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        if (getDrawable() == null || this.e == 0 || this.f == 0) {
            super.onMeasure(i, i2);
        } else {
            setMeasuredDimension(this.e, this.f);
        }
    }

    public void setImageDrawable(Drawable drawable) {
        super.setImageDrawable(drawable);
        if (!this.d) {
            return;
        }
        if (getWidth() <= 0) {
            post(new ap(this));
        } else {
            a();
        }
    }
}
