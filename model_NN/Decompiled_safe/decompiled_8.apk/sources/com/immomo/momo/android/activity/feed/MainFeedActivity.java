package com.immomo.momo.android.activity.feed;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.ka;
import com.immomo.momo.android.activity.lh;
import com.immomo.momo.android.activity.maintab.l;
import com.immomo.momo.android.view.bi;
import com.immomo.momo.g;
import com.immomo.momo.protocol.imjson.d;
import com.immomo.momo.service.af;

public class MainFeedActivity extends ka implements View.OnClickListener {
    private TextView h;
    private TextView i;

    private boolean A() {
        int i2 = af.c().i();
        if (i2 <= 0) {
            this.h.setVisibility(8);
        } else if (i2 < 100) {
            this.h.setVisibility(0);
            this.h.setText(new StringBuilder(String.valueOf(i2)).toString());
        } else {
            this.h.setVisibility(0);
            this.h.setText("99+");
        }
        int j = af.c().j();
        if (j <= 0) {
            this.i.setVisibility(8);
        } else if (j < 100) {
            this.i.setVisibility(0);
            this.i.setText(new StringBuilder(String.valueOf(j)).toString());
        } else {
            this.i.setVisibility(0);
            this.i.setText("99+");
        }
        if (!g()) {
            if (i2 > 0) {
                c(1);
            } else if (j > 0) {
                c(2);
            }
        }
        return i2 > 0 || j > 0;
    }

    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        setContentView((int) R.layout.activity_feedmain);
        a(l.class, y.class, ad.class);
        findViewById(R.id.feedmain_layout_tab1).setOnClickListener(this);
        findViewById(R.id.feedmain_layout_tab2).setOnClickListener(this);
        findViewById(R.id.feedmain_layout_tab3).setOnClickListener(this);
        this.h = (TextView) findViewById(R.id.feedmain_layout_tab2).findViewById(R.id.feedmain_tab_count);
        this.i = (TextView) findViewById(R.id.feedmain_layout_tab3).findViewById(R.id.feedmain_tab_count);
        if (A()) {
            t().p();
        }
        a(800, "actions.feeds");
        bi a2 = new bi(this).a((int) R.drawable.ic_topbar_addfeed_white);
        a2.setMarginRight(8);
        a2.setBackgroundResource(R.drawable.bg_header_submit);
        a(a2, new ac(this));
    }

    /* access modifiers changed from: protected */
    public final void a(Fragment fragment, int i2) {
        switch (i2) {
            case 0:
                findViewById(R.id.feedmain_layout_tab1).setSelected(true);
                findViewById(R.id.feedmain_layout_tab2).setSelected(false);
                findViewById(R.id.feedmain_layout_tab3).setSelected(false);
                break;
            case 1:
                findViewById(R.id.feedmain_layout_tab2).setSelected(true);
                findViewById(R.id.feedmain_layout_tab1).setSelected(false);
                findViewById(R.id.feedmain_layout_tab3).setSelected(false);
                break;
            case 2:
                findViewById(R.id.feedmain_layout_tab2).setSelected(false);
                findViewById(R.id.feedmain_layout_tab1).setSelected(false);
                findViewById(R.id.feedmain_layout_tab3).setSelected(true);
                break;
        }
        ((lh) fragment).a(this, m());
        if (!((lh) fragment).T()) {
            ((lh) fragment).S();
        }
    }

    /* access modifiers changed from: protected */
    public final boolean a(Bundle bundle, String str) {
        if ("actions.feeds".equals(str)) {
            A();
        }
        return super.a(bundle, str);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.feedmain_layout_tab1 /*2131165630*/:
                c(0);
                return;
            case R.id.feedmain_tab_label /*2131165631*/:
            case R.id.feedmain_tab_count /*2131165632*/:
            default:
                return;
            case R.id.feedmain_layout_tab2 /*2131165633*/:
                c(1);
                return;
            case R.id.feedmain_layout_tab3 /*2131165634*/:
                c(2);
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        g.d().a(new Bundle(), "actions.feedchanged");
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
    }

    /* access modifiers changed from: protected */
    public final void s() {
        super.s();
    }

    public final void y() {
        if (this.h.isShown()) {
            t().p();
        }
        this.h.setVisibility(8);
        af.c().a(0);
        d.a();
    }

    public final void z() {
        if (this.i.isShown()) {
            t().p();
        }
        this.i.setVisibility(8);
        af.c().b(0);
        d.f();
    }
}
