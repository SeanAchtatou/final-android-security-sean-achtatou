package com.immomo.momo.android.activity.account;

import android.location.Location;
import android.os.AsyncTask;
import com.immomo.momo.android.activity.WelcomeActivity;
import com.immomo.momo.android.b.z;
import com.immomo.momo.protocol.a.w;
import com.immomo.momo.service.bean.bf;
import java.io.InterruptedIOException;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONException;

final class cj extends AsyncTask {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ cc f989a;

    private cj(cc ccVar) {
        this.f989a = ccVar;
    }

    /* synthetic */ cj(cc ccVar, byte b) {
        this(ccVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.m.a(java.lang.String, java.lang.Throwable):void
     arg types: [java.lang.String, org.json.JSONException]
     candidates:
      com.immomo.momo.util.m.a(java.lang.Appendable, java.lang.Throwable):void
      com.immomo.momo.util.m.a(java.lang.Object, java.lang.Throwable):void
      com.immomo.momo.util.m.a(java.lang.String, java.io.File):void
      com.immomo.momo.util.m.a(java.lang.String, java.lang.Throwable):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.m.a(java.lang.String, java.lang.Throwable):void
     arg types: [java.lang.String, java.lang.Exception]
     candidates:
      com.immomo.momo.util.m.a(java.lang.Appendable, java.lang.Throwable):void
      com.immomo.momo.util.m.a(java.lang.Object, java.lang.Throwable):void
      com.immomo.momo.util.m.a(java.lang.String, java.io.File):void
      com.immomo.momo.util.m.a(java.lang.String, java.lang.Throwable):void */
    private List a() {
        ArrayList arrayList = new ArrayList();
        if (WelcomeActivity.i == null || WelcomeActivity.i.size() <= 0) {
            Location b = z.b();
            if (z.a(b)) {
                try {
                    this.f989a.c = w.a().a(arrayList, b.getLatitude(), b.getLongitude());
                    if (this.f989a.c > 0) {
                        if (arrayList.size() > 8) {
                            ArrayList arrayList2 = new ArrayList();
                            for (int i = 0; i < 8; i++) {
                                arrayList2.add((bf) arrayList.get(i));
                            }
                            arrayList = arrayList2;
                        }
                        bf bfVar = new bf();
                        bfVar.S = b.getLatitude();
                        bfVar.T = b.getLongitude();
                    }
                } catch (JSONException e) {
                    this.f989a.b.a("download nearby users failed", (Throwable) e);
                } catch (InterruptedIOException e2) {
                } catch (Exception e3) {
                    this.f989a.b.a("download nearby users failed", (Throwable) e3);
                }
            }
        } else {
            arrayList.addAll(WelcomeActivity.i);
        }
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object doInBackground(Object... objArr) {
        return a();
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void onPostExecute(Object obj) {
        List list = (List) obj;
        super.onPostExecute(list);
        if (list != null) {
            this.f989a.i = list;
            cc.c(this.f989a);
        }
    }
}
