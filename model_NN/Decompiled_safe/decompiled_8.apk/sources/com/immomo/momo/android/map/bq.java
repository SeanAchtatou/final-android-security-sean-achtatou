package com.immomo.momo.android.map;

import android.content.Intent;
import android.view.View;
import com.amap.mapapi.poisearch.PoiTypeDef;

final class bq implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ bp f2496a;

    bq(bp bpVar) {
        this.f2496a = bpVar;
    }

    public final void onClick(View view) {
        if (this.f2496a.g.n != null) {
            Intent intent = new Intent();
            intent.putExtra("siteid", PoiTypeDef.All);
            intent.putExtra("sitename", this.f2496a.f2495a);
            intent.putExtra("sitetype", this.f2496a.g.u);
            intent.putExtra("lat", this.f2496a.g.n.getLatitude());
            intent.putExtra("lng", this.f2496a.g.n.getLongitude());
            intent.putExtra("loctype", this.f2496a.g.p);
            this.f2496a.g.setResult(-1, intent);
            this.f2496a.g.finish();
        }
    }
}
