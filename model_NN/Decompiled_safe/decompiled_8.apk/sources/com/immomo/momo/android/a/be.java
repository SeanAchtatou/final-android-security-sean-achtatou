package com.immomo.momo.android.a;

import android.content.Context;
import android.content.Intent;
import com.immomo.momo.android.broadcast.r;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.h;
import com.immomo.momo.service.bean.p;
import java.util.ArrayList;

final class be extends d {

    /* renamed from: a  reason: collision with root package name */
    private v f734a = null;
    private p c;
    private String d;
    private /* synthetic */ ax e;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public be(ax axVar, Context context, p pVar, String str) {
        super(context);
        this.e = axVar;
        this.c = pVar;
        this.d = str;
        this.f734a = new v(context);
        this.f734a.setCancelable(true);
        this.f734a.setOnCancelListener(new bf(this));
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add(this.c.f3034a);
        h.a().a(this.d, arrayList);
        com.immomo.momo.service.h hVar = new com.immomo.momo.service.h();
        for (String c2 : arrayList) {
            hVar.c(c2, this.d);
        }
        this.e.d.sendBroadcast(new Intent(r.f2361a));
        return true;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
        if (this.f734a != null) {
            this.f734a.a("请求提交中");
            this.f734a.show();
        }
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        super.a(exc);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        super.a((Boolean) obj);
        this.e.c(this.c);
    }

    /* access modifiers changed from: protected */
    public final void b() {
        super.b();
        if (this.f734a != null) {
            this.f734a.dismiss();
            this.f734a = null;
        }
    }
}
