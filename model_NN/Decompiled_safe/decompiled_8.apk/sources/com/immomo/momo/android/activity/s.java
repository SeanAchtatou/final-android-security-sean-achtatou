package com.immomo.momo.android.activity;

import android.content.DialogInterface;
import android.content.Intent;
import com.immomo.momo.android.broadcast.m;

final class s implements DialogInterface.OnClickListener {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public /* synthetic */ AlipayUserWelcomeActivity f2124a;

    s(AlipayUserWelcomeActivity alipayUserWelcomeActivity) {
        this.f2124a = alipayUserWelcomeActivity;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        new Thread(new t(this)).start();
        this.f2124a.t().l();
        this.f2124a.sendOrderedBroadcast(new Intent(m.f2356a), null);
        this.f2124a.x.postDelayed(new u(this), 500);
    }
}
