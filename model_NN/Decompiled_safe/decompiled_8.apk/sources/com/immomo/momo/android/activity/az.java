package com.immomo.momo.android.activity;

import android.content.Intent;
import android.widget.ListAdapter;
import com.immomo.momo.android.a.g;
import com.immomo.momo.android.broadcast.d;
import com.immomo.momo.android.broadcast.e;

final class az implements d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ BlacklistActivity f1036a;

    az(BlacklistActivity blacklistActivity) {
        this.f1036a = blacklistActivity;
    }

    public final void a(Intent intent) {
        if (intent.getAction().equals(e.f2348a)) {
            this.f1036a.j = this.f1036a.k.i();
            this.f1036a.l = new g(this.f1036a, this.f1036a.j, this.f1036a.i);
            this.f1036a.i.setAdapter((ListAdapter) this.f1036a.l);
            this.f1036a.e.a((Object) ("blacklist receiver: black user count " + this.f1036a.j.size()));
        }
    }
}
