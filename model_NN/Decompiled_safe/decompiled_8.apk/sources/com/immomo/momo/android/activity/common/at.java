package com.immomo.momo.android.activity.common;

import android.content.Context;
import android.support.v4.b.a;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.w;
import com.immomo.momo.service.bean.e;
import com.immomo.momo.util.ao;

final class at extends d {

    /* renamed from: a  reason: collision with root package name */
    private v f1088a = null;
    private int c = 0;
    private /* synthetic */ ar d;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public at(ar arVar, Context context, int i) {
        super(context);
        this.d = arVar;
        this.c = i;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        w a2 = w.a();
        String str = this.d.M.h;
        return a2.b(((e) this.d.T.getItem(this.c)).c, ((InviteSNSActivity) this.d.c()).z(), ((InviteSNSActivity) this.d.c()).y());
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
        this.f1088a = new v(this.d.c());
        this.f1088a.a("请求提交中...");
        this.f1088a.setCancelable(true);
        this.f1088a.setOnCancelListener(new au(this));
        this.d.a(this.f1088a);
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        super.a(exc);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        String str = (String) obj;
        if (!a.a((CharSequence) str)) {
            ao.a((CharSequence) str);
        }
        this.d.T.d(this.c);
        this.d.T.notifyDataSetChanged();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        super.b();
        this.d.N();
    }
}
