package com.immomo.momo.android.a;

import android.content.Context;
import android.support.v4.b.a;
import com.immomo.momo.a.o;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.n;
import com.immomo.momo.service.bean.a.j;
import com.immomo.momo.service.y;

final class fl extends d {

    /* renamed from: a  reason: collision with root package name */
    private v f823a = null;
    private j c;
    private String d;
    private String e;
    private /* synthetic */ es f;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public fl(es esVar, Context context, j jVar, String str, String str2) {
        super(context);
        this.f = esVar;
        this.c = jVar;
        this.d = str;
        this.e = str2;
        this.f823a = new v(context);
        this.f823a.setCancelable(true);
        this.f823a.setOnCancelListener(new fm(this));
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        String a2 = n.a().a(this.d, this.c.f2979a, this.e);
        new y().a(3, this.d, this.f.c.g);
        this.f.c.g = this.c.f2979a;
        this.f.c.n = 3;
        this.f.e = 3;
        return a2;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
        this.f.d.a(this.f823a);
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        if (exc instanceof o) {
            com.immomo.momo.android.view.a.n.b(this.f.d, exc.getMessage(), new fo()).show();
        } else {
            super.a(exc);
        }
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        String str = (String) obj;
        if (!a.a((CharSequence) str)) {
            com.immomo.momo.android.view.a.n.b(this.f.d, str, new fn()).show();
        }
        this.f.b.h();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        super.b();
        this.f.d.p();
    }
}
