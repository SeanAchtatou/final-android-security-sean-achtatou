package com.immomo.momo.android.activity.contacts;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.b.a;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.a.dl;
import com.immomo.momo.android.activity.lh;
import com.immomo.momo.android.activity.maintab.MaintabActivity;
import com.immomo.momo.android.broadcast.d;
import com.immomo.momo.android.broadcast.j;
import com.immomo.momo.android.broadcast.w;
import com.immomo.momo.android.view.HeaderLayout;
import com.immomo.momo.android.view.LoadingButton;
import com.immomo.momo.android.view.MomoRefreshListView;
import com.immomo.momo.android.view.bl;
import com.immomo.momo.android.view.bu;
import com.immomo.momo.g;
import com.immomo.momo.service.aq;
import com.immomo.momo.util.k;
import java.util.Date;
import java.util.List;

public class an extends lh implements bl, bu {
    /* access modifiers changed from: private */
    public MomoRefreshListView O;
    /* access modifiers changed from: private */
    public dl P;
    /* access modifiers changed from: private */
    public LoadingButton Q;
    /* access modifiers changed from: private */
    public List R;
    private j S;
    private w T = null;
    /* access modifiers changed from: private */
    public int U = 0;
    /* access modifiers changed from: private */
    public int V = 0;
    /* access modifiers changed from: private */
    public Date W = null;
    /* access modifiers changed from: private */
    public aq X;
    /* access modifiers changed from: private */
    public au Y;
    /* access modifiers changed from: private */
    public av Z;
    /* access modifiers changed from: private */
    public Handler aa = new Handler();
    private d ab = new ao(this);

    static /* synthetic */ int a(an anVar, List list) {
        int a2 = com.immomo.momo.protocol.a.w.a().a(list, anVar.U);
        if (list.size() > 0) {
            anVar.X.a(list);
        }
        return a2;
    }

    static /* synthetic */ void b(an anVar, List list) {
        anVar.R = list;
        anVar.P = new dl(anVar.c(), anVar.R, anVar.O, false);
        anVar.O.setAdapter((ListAdapter) anVar.P);
    }

    /* access modifiers changed from: protected */
    public final int B() {
        return R.layout.layout_relation_fan;
    }

    /* access modifiers changed from: protected */
    public final void C() {
        this.O = (MomoRefreshListView) c((int) R.id.fans_listview);
        this.O.setEnableLoadMoreFoolter(true);
        View inflate = g.o().inflate((int) R.layout.listitem_relation_empty, (ViewGroup) null);
        ((TextView) inflate.findViewById(R.id.tv_tip)).setText((int) R.string.fanlist_empty_tip);
        this.O.a(inflate);
        this.O.setListPaddingBottom(MaintabActivity.h);
        this.Q = this.O.getFooterViewButton();
    }

    /* access modifiers changed from: protected */
    public final void E() {
        super.E();
        this.O.o();
        if (this.Q.d()) {
            this.Q.e();
        }
    }

    public final void S() {
        super.S();
        if (this.Y != null && this.Y.isCancelled()) {
            return;
        }
        if ((this.W != null && new Date().getTime() - this.W.getTime() > 900000) || this.R.size() <= 0 || (g.q() != null && g.q().v > 0)) {
            this.O.l();
        }
    }

    public final void a(Context context, HeaderLayout headerLayout) {
    }

    public final void ae() {
        super.ae();
        new k("PI", "P63").e();
    }

    public final void ag() {
        super.ag();
        new k("PO", "P63").e();
    }

    public final void ai() {
        this.O.i();
    }

    public final void aj() {
        Date date;
        this.R = this.X.g();
        try {
            String str = (String) this.N.b("lasttime_fans_success", PoiTypeDef.All);
            date = !a.a(str) ? a.k(str) : null;
        } catch (Exception e) {
            date = null;
        }
        this.O.setLastFlushTime(date);
        String str2 = (String) this.N.b("lasttime_fans", PoiTypeDef.All);
        try {
            if (!a.a((CharSequence) str2)) {
                this.W = a.k(str2);
            }
        } catch (Exception e2) {
        }
        this.P = new dl(c(), this.R, this.O, false);
        this.O.setAdapter((ListAdapter) this.P);
        this.U += this.P.getCount();
        if (this.P.getCount() < 50) {
            this.O.k();
        }
        if (this.R.size() <= 0) {
            this.O.l();
        }
    }

    public final void b_() {
        if (this.Y != null && !this.Y.isCancelled()) {
            this.Y.cancel(true);
        }
        this.Y = new au(this, c());
        this.Y.execute(new Object[0]);
    }

    /* access modifiers changed from: protected */
    public final void g(Bundle bundle) {
        this.X = new aq();
        this.T = new w(c());
        this.T.a(this.ab);
        this.O.setOnPullToRefreshListener$42b903f6(this);
        this.O.setOnCancelListener$135502(new at(this, (byte) 0));
        this.O.setOnItemClickListener(new ar(this));
        this.Q.setOnProcessListener(this);
        aj();
        this.S = new j(c());
        this.S.a(new as(this));
    }

    public final void p() {
        super.p();
        if (this.S != null) {
            a(this.S);
            this.S = null;
        }
        if (this.T != null) {
            a(this.T);
            this.T = null;
        }
        if (this.Y != null && !this.Y.isCancelled()) {
            this.Y.cancel(true);
            this.Y = null;
        }
        if (this.Z != null && !this.Z.isCancelled()) {
            this.Z.cancel(true);
            this.Z = null;
        }
    }

    public final void u() {
        this.Q.f();
        this.Z = new av(this, c());
        this.Z.execute(new Object[0]);
    }
}
