package com.immomo.momo.android.activity.tieba;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.protocol.a.v;
import java.util.List;

final class dk extends d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ TiebaCreateActivity f2239a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public dk(TiebaCreateActivity tiebaCreateActivity, Context context) {
        super(context);
        this.f2239a = tiebaCreateActivity;
        if (tiebaCreateActivity.A != null && !tiebaCreateActivity.A.isCancelled()) {
            tiebaCreateActivity.A.cancel(true);
        }
        tiebaCreateActivity.A = this;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        return v.a().g(this.f2239a.s);
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void a(Object obj) {
        TiebaCreateActivity.a(this.f2239a, (List) obj);
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.f2239a.A = (dk) null;
    }
}
