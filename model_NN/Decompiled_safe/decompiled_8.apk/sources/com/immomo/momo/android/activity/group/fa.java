package com.immomo.momo.android.activity.group;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.protocol.a.n;
import com.immomo.momo.service.bean.a.a;
import java.util.ArrayList;
import java.util.List;

final class fa extends d {

    /* renamed from: a  reason: collision with root package name */
    private List f1649a = new ArrayList();
    private /* synthetic */ SearchGroupActivity c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public fa(SearchGroupActivity searchGroupActivity, Context context) {
        super(context);
        this.c = searchGroupActivity;
        if (searchGroupActivity.t != null && !searchGroupActivity.t.isCancelled()) {
            searchGroupActivity.t.cancel(true);
        }
        searchGroupActivity.t = this;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        return Boolean.valueOf(n.a().a(this.f1649a, this.c.f.S, this.c.f.T, this.c.f.aH, this.c.u, this.c.o.getCount()));
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
        this.c.h.b();
        this.c.w();
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        super.a(exc);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        this.c.s.setVisibility(0);
        if (((Boolean) obj).booleanValue()) {
            this.c.n.setVisibility(0);
        } else {
            this.c.n.setVisibility(8);
        }
        if (!this.f1649a.isEmpty()) {
            for (a aVar : this.f1649a) {
                if (this.c.p.get(aVar.b) == null) {
                    this.c.p.put(aVar.b, aVar);
                    this.c.o.a((Object[]) new a[]{aVar});
                } else {
                    this.b.b((Object) "搜索更多有重复");
                }
            }
            this.c.o.notifyDataSetChanged();
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.c.h.c();
        this.c.n.e();
    }
}
