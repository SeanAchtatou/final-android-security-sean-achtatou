package com.immomo.momo.android.activity;

import android.content.DialogInterface;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.view.EmoteEditeText;

final class cs implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ cr f1207a;
    private final /* synthetic */ EmoteEditeText b;

    cs(cr crVar, EmoteEditeText emoteEditeText) {
        this.f1207a = crVar;
        this.b = emoteEditeText;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        if (this.b.getText().toString().trim().equals(PoiTypeDef.All)) {
            this.f1207a.f1206a.a((int) R.string.dialog_editprofile_name_empty);
            dialogInterface.dismiss();
            return;
        }
        this.f1207a.f1206a.s.setText(this.b.getText().toString().trim());
    }
}
