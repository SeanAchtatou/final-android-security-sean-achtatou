package com.immomo.momo.android.view;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;

final class dl extends d {

    /* renamed from: a  reason: collision with root package name */
    private v f2781a;
    private /* synthetic */ TiebaSearchView c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public dl(TiebaSearchView tiebaSearchView, Context context) {
        super(context);
        this.c = tiebaSearchView;
        if (tiebaSearchView.e != null && !tiebaSearchView.e.isCancelled()) {
            tiebaSearchView.e.cancel(true);
        }
        tiebaSearchView.e = this;
        this.f2781a = new v(f());
        this.f2781a.a("请求提交中");
        this.f2781a.setCancelable(true);
        this.f2781a.setOnCancelListener(new dm(this));
        try {
            this.f2781a.show();
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        return com.immomo.momo.protocol.a.v.a().h(this.c.f);
    }

    /* access modifiers changed from: protected */
    public final void a() {
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        this.c.g.w();
        a((String) obj);
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.c.d = (dn) null;
        try {
            this.f2781a.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
