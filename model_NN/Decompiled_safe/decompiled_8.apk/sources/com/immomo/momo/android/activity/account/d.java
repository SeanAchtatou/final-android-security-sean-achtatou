package com.immomo.momo.android.activity.account;

import android.content.DialogInterface;

final class d implements DialogInterface.OnDismissListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ LoginActivity f998a;

    d(LoginActivity loginActivity) {
        this.f998a = loginActivity;
    }

    public final void onDismiss(DialogInterface dialogInterface) {
        if (this.f998a.r != null) {
            this.f998a.r.cancel(true);
            this.f998a.r = (j) null;
        }
    }
}
