package com.immomo.momo.android.activity.account;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.b.a;
import com.immomo.momo.R;
import com.immomo.momo.a.l;
import com.immomo.momo.a.o;
import com.immomo.momo.a.p;
import com.immomo.momo.a.x;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.g;
import com.immomo.momo.protocol.a.w;
import com.immomo.momo.util.jni.Codec;
import org.json.JSONException;

final class n extends d {

    /* renamed from: a  reason: collision with root package name */
    private v f1008a = null;
    private /* synthetic */ RegisterActivity c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public n(RegisterActivity registerActivity, Context context) {
        super(context);
        this.c = registerActivity;
    }

    /* access modifiers changed from: protected */
    public final Object a(Object... objArr) {
        int currentTimeMillis = (int) (((System.currentTimeMillis() - this.c.j) / 1000) + ((long) this.c.i));
        w.a().a(this.c.n.G, this.c.l, this.c.n.f3011a, currentTimeMillis, a.b(Codec.gpi(this.c.n(), currentTimeMillis)));
        return null;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.f1008a = new v(this.c, (int) R.string.reg_email_validating);
        this.f1008a.setOnCancelListener(new o(this));
        this.f1008a.show();
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        if (exc instanceof com.immomo.momo.a.w) {
            a(exc.getMessage());
        } else if ((exc instanceof l) && !this.c.isFinishing()) {
            com.immomo.momo.android.view.a.n b = com.immomo.momo.android.view.a.n.b(this.c, exc.getMessage(), (DialogInterface.OnClickListener) null);
            b.setCancelable(false);
            b.show();
        } else if (exc instanceof o) {
            a(exc.getMessage());
        } else if (exc instanceof p) {
            this.c.p.g();
        } else if (exc instanceof JSONException) {
            a((int) R.string.errormsg_dataerror);
        } else if (exc instanceof com.immomo.momo.a.a) {
            a(exc.getMessage());
        } else if (exc instanceof x) {
            a("邮箱验证失败，请稍后重试");
        } else if (!"mobile".equals(g.Y()) || !g.ac()) {
            a((int) R.string.errormsg_server);
        } else {
            com.immomo.momo.android.view.a.n.b(this.c, (int) R.string.errormsg_net_cmwap, (DialogInterface.OnClickListener) null).show();
        }
    }

    /* access modifiers changed from: protected */
    public final void a(Object obj) {
        this.c.o = String.valueOf(this.c.n.G) + this.c.n.f3011a;
        this.c.u.showNext();
        RegisterActivity registerActivity = this.c;
        registerActivity.m = registerActivity.m + 1;
        this.c.t = this.c.j();
        this.c.z.setText((int) R.string.reg_prestep);
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.f1008a.dismiss();
        this.f1008a = null;
    }
}
