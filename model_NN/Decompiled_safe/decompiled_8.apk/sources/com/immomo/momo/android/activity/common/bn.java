package com.immomo.momo.android.activity.common;

import com.immomo.momo.android.a.hp;

final class bn implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ bk f1107a;
    private final /* synthetic */ int b;
    private final /* synthetic */ String c;

    bn(bk bkVar, int i, String str) {
        this.f1107a = bkVar;
        this.b = i;
        this.c = str;
    }

    public final void run() {
        if (this.b < 0 || this.b > this.f1107a.f1104a.P.getCount()) {
            this.f1107a.f1104a.R = this.f1107a.f1104a.U.c();
            this.f1107a.f1104a.P.b(this.f1107a.f1104a.R);
        } else {
            this.f1107a.f1104a.U.a(((hp) this.f1107a.f1104a.P.getItem(this.b)).g, this.c);
        }
        this.f1107a.f1104a.P.notifyDataSetChanged();
    }
}
