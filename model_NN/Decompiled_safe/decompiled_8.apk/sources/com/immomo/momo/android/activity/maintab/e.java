package com.immomo.momo.android.activity.maintab;

import android.support.v4.app.w;
import android.view.View;
import com.immomo.momo.android.activity.lh;

final class e implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ MaintabActivity f1887a;

    e(MaintabActivity maintabActivity) {
        this.f1887a = maintabActivity;
    }

    public final void onClick(View view) {
        lh lhVar;
        int i = 0;
        while (true) {
            if (i >= this.f1887a.k.length) {
                lhVar = null;
                break;
            } else if (this.f1887a.k[i][4] == view) {
                lhVar = (lh) this.f1887a.k[i][3];
                break;
            } else {
                i++;
            }
        }
        if (lhVar == null) {
            return;
        }
        if (this.f1887a.o == lhVar) {
            this.f1887a.o.ai();
        } else {
            this.f1887a.a(lhVar, (w) null);
        }
    }
}
