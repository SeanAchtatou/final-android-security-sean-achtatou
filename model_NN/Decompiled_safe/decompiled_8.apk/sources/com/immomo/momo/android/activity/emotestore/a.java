package com.immomo.momo.android.activity.emotestore;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.g;
import com.immomo.momo.protocol.a.i;
import com.immomo.momo.service.bean.al;
import com.immomo.momo.util.j;
import java.util.regex.Pattern;
import org.json.JSONArray;
import org.json.JSONObject;

final class a extends d {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public /* synthetic */ EmotionInviteTaskActivity f1280a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public a(EmotionInviteTaskActivity emotionInviteTaskActivity, Context context) {
        super(context);
        this.f1280a = emotionInviteTaskActivity;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        return i.a().g(this.f1280a.h);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.f1280a.a(new v(f(), "请稍候，正在获取数据...", this));
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        JSONObject jSONObject = (JSONObject) obj;
        ((TextView) this.f1280a.findViewById(R.id.emotiontask_tv_desc)).setText(jSONObject.optString("desc"));
        ((TextView) this.f1280a.findViewById(R.id.emotiontask_tv_progress)).setText(jSONObject.optString("process"));
        this.f1280a.setTitle(jSONObject.optString("title"));
        ImageView imageView = (ImageView) this.f1280a.findViewById(R.id.emotiontask_iv_cover);
        String optString = jSONObject.optString("img");
        if (!android.support.v4.b.a.a((CharSequence) optString)) {
            imageView.setVisibility(0);
            j.b(new al(optString, true), imageView, null, 18, true);
        } else {
            imageView.setVisibility(8);
        }
        String optString2 = jSONObject.optString("invite_action");
        if (Pattern.compile("\\[.*?\\|.*?\\|.*?\\]").matcher(optString2).matches()) {
            String[] split = optString2.substring(1, optString2.length() - 1).split("\\|");
            this.f1280a.i.setVisibility(0);
            if (split != null && split.length > 0) {
                this.f1280a.i.setText(split[0]);
            }
        }
        this.f1280a.i.setOnClickListener(new b(this, optString2));
        JSONArray optJSONArray = jSONObject.optJSONArray("list");
        if (optJSONArray != null && optJSONArray.length() > 0) {
            ViewGroup viewGroup = (ViewGroup) this.f1280a.findViewById(R.id.emotiontask_layout_invitelist);
            for (int i = 0; i < optJSONArray.length(); i++) {
                g.o().inflate((int) R.layout.listitem_invitetask_user, viewGroup);
                View childAt = viewGroup.getChildAt(i);
                JSONObject optJSONObject = optJSONArray.optJSONObject(i);
                ((TextView) childAt.findViewById(R.id.textView)).setText(optJSONObject.optString("name"));
                j.a(new al(optJSONObject.optString("avatar")), (ImageView) childAt.findViewById(R.id.imageview), 3);
                childAt.setOnClickListener(new c(this, optJSONObject.optString("momoid")));
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.f1280a.p();
    }

    /* access modifiers changed from: protected */
    public final void onCancelled() {
        this.f1280a.finish();
    }
}
