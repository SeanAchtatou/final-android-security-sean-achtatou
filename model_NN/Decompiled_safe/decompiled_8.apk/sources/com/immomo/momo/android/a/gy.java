package com.immomo.momo.android.a;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.a.a;
import com.immomo.momo.service.bean.a.i;
import com.immomo.momo.service.bean.at;
import com.immomo.momo.util.j;
import com.immomo.momo.util.m;
import java.util.List;

public final class gy extends a {
    private Context d = null;
    private List e = null;
    private AbsListView f = null;

    public gy(Context context, List list, AbsListView absListView) {
        super(context, list);
        new gz();
        this.d = context;
        this.e = list;
        this.f = absListView;
        new m("GroupListAdapter").a();
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            ha haVar = new ha((byte) 0);
            view = LayoutInflater.from(this.d).inflate((int) R.layout.listitem_group_relation, (ViewGroup) null);
            haVar.f854a = (ImageView) view.findViewById(R.id.userlist_item_iv_face);
            haVar.b = (TextView) view.findViewById(R.id.userlist_item_tv_name);
            haVar.c = (TextView) view.findViewById(R.id.userlist_item_tv_distance);
            haVar.d = (TextView) view.findViewById(R.id.userlist_item_tv_sign);
            haVar.e = (TextView) view.findViewById(R.id.userlist_item_tv_sitename);
            view.findViewById(R.id.userlist_item_tv_role);
            haVar.g = (TextView) view.findViewById(R.id.userlist_item_tv_status);
            haVar.h = (ImageView) view.findViewById(R.id.userlist_item_pic_iv_party);
            view.findViewById(R.id.userlist_item_layout_membercount_backgroud);
            view.findViewById(R.id.userlist_item_iv_person_icon);
            haVar.f = view.findViewById(R.id.userlist_item_mute);
            haVar.i = (ImageView) view.findViewById(R.id.userlist_item_tv_new);
            view.setTag(R.id.tag_userlist_item, haVar);
        }
        a aVar = (a) this.e.get(i);
        ha haVar2 = (ha) view.getTag(R.id.tag_userlist_item);
        haVar2.c.setText(aVar.p);
        if (!android.support.v4.b.a.a((CharSequence) aVar.c)) {
            haVar2.b.setText(aVar.c);
        } else {
            haVar2.b.setText(aVar.b);
        }
        if (aVar.a()) {
            haVar2.b.setTextColor(g.c((int) R.color.font_vip_name));
        } else {
            haVar2.b.setTextColor(g.c((int) R.color.text_color));
        }
        if (aVar.h != null) {
            haVar2.d.setText(aVar.h);
        } else {
            haVar2.d.setText(PoiTypeDef.All);
        }
        if (aVar.C) {
            haVar2.h.setVisibility(0);
        } else {
            haVar2.h.setVisibility(8);
        }
        if (aVar.G == 1) {
            haVar2.g.setVisibility(0);
            haVar2.g.setText((int) R.string.grouplist_group_status_waiting);
        } else if (aVar.G == 4) {
            haVar2.g.setVisibility(0);
            haVar2.g.setText((int) R.string.grouplist_group_status_baned);
        } else if (aVar.G == 3) {
            haVar2.g.setVisibility(0);
            haVar2.g.setText((int) R.string.grouplist_group_status_notpass);
        } else {
            haVar2.g.setVisibility(8);
        }
        if (!android.support.v4.b.a.a((CharSequence) aVar.K)) {
            haVar2.e.setVisibility(0);
            haVar2.e.setText(aVar.K);
        } else {
            haVar2.e.setVisibility(8);
        }
        at r = g.r();
        if (r != null) {
            i c = r.c(aVar.b);
            if (c == null || c.f2978a) {
                haVar2.f.setVisibility(8);
            } else {
                haVar2.f.setVisibility(0);
            }
        } else {
            haVar2.f.setVisibility(8);
        }
        if (aVar.c()) {
            haVar2.i.setVisibility(0);
        } else {
            haVar2.i.setVisibility(8);
        }
        j.a(aVar, haVar2.f854a, this.f, 3);
        return view;
    }
}
