package com.immomo.momo.android.activity.feed;

import android.content.Context;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.android.c.b;
import com.immomo.momo.protocol.a.a.m;
import com.immomo.momo.protocol.a.k;
import com.immomo.momo.service.bean.ab;

final class v extends b {
    private String c;
    private String d;
    private String e;
    private String f;
    private String g;
    private int h;
    private int i;
    private /* synthetic */ FeedProfileActivity j;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public v(FeedProfileActivity feedProfileActivity, Context context, String str, String str2, int i2, int i3, String str3, String str4, String str5) {
        super(context);
        this.j = feedProfileActivity;
        this.c = str;
        this.d = str2;
        this.h = i2;
        this.i = i3;
        this.e = str3;
        this.f = str4;
        this.g = str5;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        this.e = this.e.replaceAll("\n{2,}", "\n");
        m a2 = k.a().a(this.c, this.d, this.h, this.i, this.e, this.f, this.g);
        this.j.m.a(a2.f2892a);
        return a2;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
        this.j.w();
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        m mVar = (m) obj;
        mVar.f2892a.f2985a = this.j.f;
        FeedProfileActivity feedProfileActivity = this.j;
        ab h2 = this.j.k;
        int i2 = h2.g + 1;
        h2.g = i2;
        FeedProfileActivity.a(feedProfileActivity, i2);
        this.j.l.b(0, mVar.f2892a);
        this.j.Z.setText(PoiTypeDef.All);
        this.j.ai = true;
        this.j.w();
        this.j.v();
        this.j.aj = null;
        this.j.ab.setText(PoiTypeDef.All);
        this.j.ab.setVisibility(8);
        super.a(mVar);
    }
}
