package com.immomo.momo.android.activity.retrieve;

import android.content.Context;
import com.immomo.momo.R;
import com.immomo.momo.a.a;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.w;
import org.json.JSONException;

final class p extends d {

    /* renamed from: a  reason: collision with root package name */
    private String f2122a = null;
    private v c = null;
    private /* synthetic */ ResetPswByPhoneActivity d;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public p(ResetPswByPhoneActivity resetPswByPhoneActivity, Context context) {
        super(context);
        this.d = resetPswByPhoneActivity;
        this.f2122a = resetPswByPhoneActivity.l.getText().toString().trim();
    }

    /* access modifiers changed from: protected */
    public final Object a(Object... objArr) {
        w.a().a(this.d.p, this.d.o, this.d.n, this.f2122a);
        return null;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.c = new v(this.d, "正在验证，请稍候...");
        this.c.setOnCancelListener(new q(this));
        this.c.show();
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        if (exc instanceof com.immomo.momo.a.w) {
            a(exc.getMessage());
        } else if (exc instanceof JSONException) {
            a((int) R.string.errormsg_dataerror);
        } else if (exc instanceof a) {
            a(exc.getMessage());
        } else {
            a((int) R.string.errormsg_server);
        }
    }

    /* access modifiers changed from: protected */
    public final void a(Object obj) {
        a("密码修改成功");
        this.d.setResult(33);
        this.d.finish();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.c.dismiss();
        this.c = null;
    }
}
