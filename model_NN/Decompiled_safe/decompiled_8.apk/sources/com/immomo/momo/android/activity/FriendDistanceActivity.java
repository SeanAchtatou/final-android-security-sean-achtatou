package com.immomo.momo.android.activity;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.b.a;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.android.a.da;
import com.immomo.momo.android.c.u;
import com.immomo.momo.android.view.LoadingButton;
import com.immomo.momo.android.view.RefreshOnOverScrollListView;
import com.immomo.momo.android.view.cd;
import com.immomo.momo.android.view.cg;
import com.immomo.momo.g;
import com.immomo.momo.service.aq;
import com.immomo.momo.service.bean.ag;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.service.t;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadPoolExecutor;
import mm.purchasesdk.PurchaseCode;

public class FriendDistanceActivity extends ah {
    da h = null;
    Handler i = new ek(this);
    /* access modifiers changed from: private */
    public RefreshOnOverScrollListView j = null;
    /* access modifiers changed from: private */
    public cd k = null;
    /* access modifiers changed from: private */
    public t l = null;
    private LoadingButton m;
    private ThreadPoolExecutor n = null;
    /* access modifiers changed from: private */
    public aq o;
    private boolean p = false;
    private TextView q = null;
    private Map r = new HashMap();

    private void a(ag agVar) {
        String f = agVar.f();
        if (!a.a((CharSequence) f)) {
            bf b = this.o.b(f);
            if (b == null) {
                if (this.r.get(f) != null) {
                    b = (bf) this.r.get(f);
                } else {
                    b = new bf(f);
                    this.r.put(f, b);
                }
                b.setImageMultipleDiaplay(true);
            }
            agVar.a(b);
        }
    }

    /* access modifiers changed from: private */
    public void c(int i2) {
        if (i2 > 0) {
            this.q.setVisibility(0);
            this.q.setText(String.valueOf(i2) + "条新动态");
            return;
        }
        this.q.setVisibility(8);
    }

    /* access modifiers changed from: private */
    public void d(int i2) {
        List<ag> a2 = this.l.a(i2);
        if (!a2.isEmpty()) {
            if (a2.size() > 20) {
                a2.remove(a2.size() - 1);
                this.m.setVisibility(0);
            } else {
                this.m.setVisibility(8);
            }
            for (ag a3 : a2) {
                a(a3);
            }
            u();
            this.h.b((Collection) a2);
        }
    }

    private void u() {
        if (this.r.size() > 0) {
            this.n.execute(new ep(this, this.r));
            this.r.clear();
        }
    }

    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        super.a(bundle);
        setContentView((int) R.layout.activity_frienddistance);
        this.l = new t();
        this.o = new aq();
        this.n = u.a();
        this.j = (RefreshOnOverScrollListView) findViewById(R.id.listview);
        View inflate = g.o().inflate((int) R.layout.common_list_loadmore, (ViewGroup) null);
        this.m = (LoadingButton) inflate.findViewById(R.id.btn_loadmore);
        this.m.setNormalIconResId(R.drawable.ic_btn_inner_clock);
        this.m.setVisibility(8);
        this.j.addFooterView(inflate);
        this.q = (TextView) LayoutInflater.from(this).inflate((int) R.layout.include_righttext_hi, (ViewGroup) null);
        m().a(this.q);
        this.j.addHeaderView(g.o().inflate((int) R.layout.listitem_blank, (ViewGroup) null));
        this.j.a(g.o().inflate((int) R.layout.include_frienddiscover_listempty, (ViewGroup) null));
        this.m.setButtonOnClickListener(new el(this));
        this.k = new cd(this, this.j);
        this.k.a();
        this.j.setMultipleSelector(this.k);
        this.k.a((int) R.id.item_layout);
        this.k.a((cg) new em(this));
        this.k.a(new com.immomo.momo.android.view.a(this).a(), new en(this));
        this.h = new da(this.j, this, new ArrayList());
    }

    /* access modifiers changed from: protected */
    public final boolean a(Bundle bundle, String str) {
        if (!"actions.frienddiscover".equals(str)) {
            return super.a(bundle, str);
        }
        ag a2 = this.l.a(bundle.getString("msgid"));
        if (a2 != null) {
            a(a2);
            u();
            this.h.b(0, a2);
        }
        if (!q()) {
            this.p = true;
        }
        if (bundle.containsKey("frienddiscover")) {
            c(bundle.getInt("frienddiscover"));
        }
        return q();
    }

    /* access modifiers changed from: protected */
    public final void d() {
        int c = this.l.c();
        if (c > 0) {
            t().p();
        }
        d(0);
        c(c);
    }

    /* access modifiers changed from: protected */
    public final void e() {
        super.e();
        this.j.setAdapter((ListAdapter) this.h);
        d();
        a((int) PurchaseCode.QUERY_FROZEN, "actions.frienddiscover");
    }

    public void onBackPressed() {
        if (this.k.g()) {
            this.k.e();
        } else {
            super.onBackPressed();
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.l.d();
        Bundle bundle = new Bundle();
        bundle.putString("sessionid", "-2240");
        bundle.putInt("sessiontype", 8);
        g.d().a(bundle, "action.sessionchanged");
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.p) {
            t().p();
            this.p = false;
        }
    }
}
