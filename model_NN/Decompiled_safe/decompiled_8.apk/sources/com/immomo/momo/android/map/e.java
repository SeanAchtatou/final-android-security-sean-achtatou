package com.immomo.momo.android.map;

import android.location.Location;
import com.amap.mapapi.core.GeoPoint;
import com.immomo.momo.R;
import com.immomo.momo.android.b.l;
import com.immomo.momo.android.b.p;
import com.immomo.momo.android.b.z;
import com.immomo.momo.util.ao;

final class e extends p {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AMapActivity f2500a;

    e(AMapActivity aMapActivity) {
        this.f2500a = aMapActivity;
    }

    public final void a(Location location, int i, int i2, int i3) {
        if (this.f2500a.isFinishing()) {
            return;
        }
        if (z.a(location)) {
            this.f2500a.k.a((Object) "Navigation");
            if (i == 1) {
                this.f2500a.b();
                AMapActivity.a(this.f2500a, new GeoPoint((int) (location.getLatitude() * 1000000.0d), (int) (location.getLongitude() * 1000000.0d)));
                return;
            }
            new l(new f(this), 0).execute(location);
            return;
        }
        this.f2500a.k.a((Object) "No Available Location");
        if (i2 == 104) {
            ao.g(R.string.errormsg_network_unfind);
        } else {
            ao.g(R.string.errormsg_location_nearby_failed);
        }
    }
}
