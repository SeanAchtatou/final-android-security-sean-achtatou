package com.immomo.momo.android.activity.tieba;

import android.content.Intent;
import android.view.View;
import android.widget.ExpandableListView;
import com.immomo.momo.service.bean.c.d;

final class dy implements ExpandableListView.OnChildClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ dw f2251a;

    dy(dw dwVar) {
        this.f2251a = dwVar;
    }

    public final boolean onChildClick(ExpandableListView expandableListView, View view, int i, int i2, long j) {
        if (this.f2251a.V.b(i)) {
            this.f2251a.a(new Intent(this.f2251a.c(), TiebaAdminActivity.class));
        } else if (!this.f2251a.V.d(i2, i) && !this.f2251a.V.a(i2, i)) {
            if (this.f2251a.V.c(i2, i)) {
                this.f2251a.a(new Intent(this.f2251a.c(), TiebaCreatingActivity.class));
            } else if (!this.f2251a.V.a(i2, i)) {
                d e = this.f2251a.V.e(i, i2);
                Intent intent = new Intent(this.f2251a.c(), TiebaProfileActivity.class);
                intent.putExtra("tiebaid", e.f3019a);
                this.f2251a.a(intent);
                if (e.q) {
                    e.q = false;
                    this.f2251a.V.notifyDataSetChanged();
                }
            }
        }
        return true;
    }
}
