package com.immomo.momo.android.pay;

import android.os.Handler;
import android.os.Message;
import com.immomo.momo.R;
import java.util.HashMap;

final class d extends Handler {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ BuyMemberActivity f2556a;

    d(BuyMemberActivity buyMemberActivity) {
        this.f2556a = buyMemberActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.android.pay.BuyMemberActivity.a(com.immomo.momo.android.pay.BuyMemberActivity, boolean):void
     arg types: [com.immomo.momo.android.pay.BuyMemberActivity, int]
     candidates:
      com.immomo.momo.android.pay.BuyMemberActivity.a(com.immomo.momo.android.pay.BuyMemberActivity, com.immomo.momo.android.pay.bk):void
      com.immomo.momo.android.pay.BuyMemberActivity.a(com.immomo.momo.android.pay.BuyMemberActivity, com.immomo.momo.android.pay.m):void
      com.immomo.momo.android.pay.BuyMemberActivity.a(com.immomo.momo.android.pay.BuyMemberActivity, com.immomo.momo.android.view.a.v):void
      com.immomo.momo.android.pay.BuyMemberActivity.a(com.immomo.momo.android.pay.BuyMemberActivity, java.lang.String):void
      com.immomo.momo.android.pay.BuyMemberActivity.a(com.immomo.momo.android.pay.BuyMemberActivity, java.util.List):void
      com.immomo.momo.android.pay.BuyMemberActivity.a(java.lang.String, int):void
      com.immomo.momo.android.activity.ah.a(android.os.Bundle, java.lang.String):boolean
      com.immomo.momo.android.activity.ao.a(int, java.lang.String[]):com.immomo.momo.protocol.imjson.c.a
      com.immomo.momo.android.activity.ao.a(com.immomo.momo.android.view.bi, android.view.View$OnClickListener):void
      com.immomo.momo.android.activity.ao.a(java.lang.CharSequence, int):void
      com.immomo.momo.android.activity.ao.a(android.os.Bundle, java.lang.String):boolean
      com.immomo.momo.android.pay.BuyMemberActivity.a(com.immomo.momo.android.pay.BuyMemberActivity, boolean):void */
    public final void handleMessage(Message message) {
        try {
            switch (message.what) {
                case 1:
                    this.f2556a.I = (String) message.obj;
                    BuyMemberActivity.a(this.f2556a);
                    try {
                        if ("9000".equals(this.f2556a.I.substring("resultStatus={".length() + this.f2556a.I.indexOf("resultStatus="), this.f2556a.I.indexOf("};memo=")))) {
                            try {
                                this.f2556a.y();
                                this.f2556a.H = true;
                                this.f2556a.t.setText((int) R.string.payvip_btn_recheck);
                                this.f2556a.a(this.f2556a.I, 0);
                            } catch (Exception e) {
                                this.f2556a.e.a((Throwable) e);
                            }
                        }
                    } catch (Exception e2) {
                        this.f2556a.e.a((Throwable) e2);
                        this.f2556a.a((CharSequence) "支付失败，请稍后重试。");
                    }
                    super.handleMessage(message);
                    return;
                case 2:
                    BuyMemberActivity.a(this.f2556a);
                    this.f2556a.a((CharSequence) "支付失败，请稍后重试。");
                    super.handleMessage(message);
                    return;
                case 123:
                    this.f2556a.b(new i(this.f2556a, this.f2556a));
                    super.handleMessage(message);
                    return;
                case 124:
                    this.f2556a.b(new j(this.f2556a, this.f2556a));
                    super.handleMessage(message);
                    return;
                case 125:
                    HashMap hashMap = (HashMap) message.obj;
                    int intValue = ((Integer) hashMap.get("count")).intValue();
                    this.f2556a.b((String) hashMap.get("data"), intValue);
                    super.handleMessage(message);
                    return;
                default:
                    super.handleMessage(message);
                    return;
            }
        } catch (Exception e3) {
            this.f2556a.e.a((Throwable) e3);
        }
    }
}
