package com.immomo.momo.android.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.g;

public class BottomTipView extends LinearLayout {

    /* renamed from: a  reason: collision with root package name */
    private Context f2624a;
    private TextView b;
    private Animation c = null;

    public BottomTipView(Context context) {
        super(context);
        this.f2624a = context;
        a();
    }

    public BottomTipView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f2624a = context;
        a();
    }

    private void a() {
        this.c = AnimationUtils.loadAnimation(this.f2624a, R.anim.layout_alpha_in);
        this.b = (TextView) g.o().inflate((int) R.layout.common_bottom_tip, this).findViewById(R.id.tv_content);
    }

    public void setText(CharSequence charSequence) {
        this.b.setText(charSequence);
    }

    public void setVisibility(int i) {
        if (getVisibility() != 0) {
            if (this.c == null) {
                this.c = AnimationUtils.loadAnimation(this.f2624a, R.anim.layout_alpha_in);
            }
            clearAnimation();
            setAnimation(this.c);
        }
        super.setVisibility(i);
    }
}
