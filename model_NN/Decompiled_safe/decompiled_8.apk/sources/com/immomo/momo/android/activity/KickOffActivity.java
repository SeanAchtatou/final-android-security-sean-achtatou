package com.immomo.momo.android.activity;

import android.os.Bundle;
import android.support.v4.b.a;
import android.view.KeyEvent;
import android.widget.TextView;
import com.immomo.momo.R;

public class KickOffActivity extends ao {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_kickoff);
        this.e.a((Object) "onCreate");
        TextView textView = (TextView) findViewById(R.id.dialog_tv_message);
        Bundle extras = getIntent().getExtras();
        String str = null;
        if (extras != null) {
            str = (String) extras.get("message");
        }
        if (a.a((CharSequence) str)) {
            str = "服务器断开了和您的连接，请与我们联系";
        }
        textView.setText(str);
        findViewById(R.id.dilaog_button1).setOnClickListener(new ft(this));
        t().k();
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i == 4) {
            return true;
        }
        return super.onKeyDown(i, keyEvent);
    }
}
