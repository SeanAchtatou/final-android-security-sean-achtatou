package com.immomo.momo.android.activity.discuss;

import android.content.Intent;
import com.immomo.momo.android.broadcast.d;
import com.immomo.momo.android.broadcast.r;
import com.immomo.momo.service.bean.n;

final class a implements d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ DiscussMemberListActivity f1223a;

    a(DiscussMemberListActivity discussMemberListActivity) {
        this.f1223a = discussMemberListActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.service.h.b(java.lang.String, boolean):java.util.List
     arg types: [java.lang.String, int]
     candidates:
      com.immomo.momo.service.h.b(java.lang.String, int):void
      com.immomo.momo.service.h.b(java.lang.String, java.lang.String):void
      com.immomo.momo.service.h.b(java.lang.String, boolean):java.util.List */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.service.h.a(java.lang.String, boolean):com.immomo.momo.service.bean.n
     arg types: [java.lang.String, int]
     candidates:
      com.immomo.momo.service.h.a(com.immomo.momo.service.bean.n, java.lang.String):void
      com.immomo.momo.service.h.a(java.lang.String, int):void
      com.immomo.momo.service.h.a(java.lang.String, java.lang.String):void
      com.immomo.momo.service.h.a(java.lang.String, java.lang.String[]):void
      com.immomo.momo.service.h.a(java.util.List, java.lang.String):void
      com.immomo.momo.service.h.a(java.lang.String, boolean):com.immomo.momo.service.bean.n */
    public final void a(Intent intent) {
        if (r.f2361a.equals(intent.getAction())) {
            this.f1223a.u = true;
            int size = this.f1223a.l.b(this.f1223a.o, false).size();
            n a2 = this.f1223a.l.a(this.f1223a.o, false);
            if (a2 != null) {
                a2.j = size;
                this.f1223a.l.b(this.f1223a.o, a2.j);
            }
            this.f1223a.a(this.f1223a.l.c(this.f1223a.o, this.f1223a.q));
        }
    }
}
