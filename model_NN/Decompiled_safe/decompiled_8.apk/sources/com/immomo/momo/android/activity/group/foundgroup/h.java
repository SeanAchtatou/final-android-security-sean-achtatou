package com.immomo.momo.android.activity.group.foundgroup;

import android.support.v4.b.a;
import android.text.InputFilter;
import android.view.View;
import com.immomo.momo.R;
import com.immomo.momo.android.view.EmoteEditeText;
import com.immomo.momo.util.k;

public final class h extends a implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private FoundGroupActivity f1667a;
    /* access modifiers changed from: private */
    public EmoteEditeText b;
    private s c;
    private InputFilter d = new i(this);

    public h(View view, FoundGroupActivity foundGroupActivity, s sVar) {
        super(view);
        this.f1667a = foundGroupActivity;
        this.c = sVar;
        this.b = (EmoteEditeText) a((int) R.id.et_group_name);
        if (!a.a((CharSequence) this.c.c)) {
            this.b.setText(this.c.c);
        }
        this.b.setFilters(new InputFilter[]{this.d});
    }

    public final void b() {
    }

    /* access modifiers changed from: protected */
    public final boolean c() {
        String trim = this.b.getText().toString().trim();
        if (trim.length() < 2) {
            this.f1667a.a((int) R.string.str_group_name_tip);
            return false;
        }
        this.c.c = trim;
        return true;
    }

    /* access modifiers changed from: protected */
    public final void d() {
        new k("PI", "P643").e();
    }

    /* access modifiers changed from: protected */
    public final void e() {
        new k("PO", "P643").e();
    }

    public final void onClick(View view) {
    }
}
