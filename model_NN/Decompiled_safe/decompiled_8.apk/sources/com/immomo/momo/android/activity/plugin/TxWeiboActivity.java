package com.immomo.momo.android.activity.plugin;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.b.a;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.android.a.hu;
import com.immomo.momo.android.activity.ah;
import com.immomo.momo.android.c.r;
import com.immomo.momo.android.view.HandyListView;
import com.immomo.momo.android.view.HeaderLayout;
import com.immomo.momo.android.view.LoadingButton;
import com.immomo.momo.android.view.bi;
import com.immomo.momo.android.view.bl;
import com.immomo.momo.g;
import com.immomo.momo.plugin.f.b;
import com.immomo.momo.util.m;

public class TxWeiboActivity extends ah implements View.OnClickListener, bl {
    private TextView A = null;
    private View B = null;
    private TextView C = null;
    /* access modifiers changed from: private */
    public View D = null;
    /* access modifiers changed from: private */
    public LoadingButton E = null;
    private bi F = null;
    /* access modifiers changed from: private */
    public AsyncTask G = null;
    /* access modifiers changed from: private */
    public AsyncTask H = null;
    /* access modifiers changed from: private */
    public Handler I;
    /* access modifiers changed from: private */
    public int h = 1;
    /* access modifiers changed from: private */
    public hu i = null;
    /* access modifiers changed from: private */
    public String j = null;
    /* access modifiers changed from: private */
    public String k = null;
    private int l;
    /* access modifiers changed from: private */
    public b m = null;
    private View n = null;
    private HeaderLayout o = null;
    /* access modifiers changed from: private */
    public HandyListView p = null;
    /* access modifiers changed from: private */
    public ImageView q = null;
    private TextView r = null;
    private TextView s = null;
    private TextView t = null;
    private View u = null;
    private TextView v = null;
    private View w = null;
    private ImageView x = null;
    private View y = null;
    private TextView z = null;

    public TxWeiboActivity() {
        new m(TxWeiboActivity.class.getSimpleName());
        this.I = new Handler();
    }

    /* access modifiers changed from: private */
    public void v() {
        if (this.m != null) {
            this.r.setText(this.m.b);
            this.z.setText(this.m.l);
            this.A.setText(this.m.d);
            if (this.m.a()) {
                if (!a.a((CharSequence) this.m.m)) {
                    this.B.setVisibility(0);
                    this.C.setText(this.m.m);
                }
                this.r.setCompoundDrawablesWithIntrinsicBounds(0, 0, (int) R.drawable.ic_user_sinav, 0);
            } else {
                this.B.setVisibility(8);
                this.r.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            }
            if ("f".equalsIgnoreCase(this.m.k)) {
                this.x.setBackgroundResource(R.drawable.bg_gender_famal_weibo);
                this.x.setImageResource(R.drawable.ic_user_famale);
            } else if ("m".equalsIgnoreCase(this.m.k)) {
                this.x.setBackgroundResource(R.drawable.bg_gender_male_weibo);
                this.x.setImageResource(R.drawable.ic_user_male);
            } else {
                this.x.setVisibility(8);
            }
            this.s.setText(new StringBuilder(String.valueOf(this.m.i)).toString());
            if (this.m.j < 100000) {
                this.v.setText(new StringBuilder(String.valueOf(this.m.j)).toString());
                this.w.setVisibility(8);
            } else {
                this.v.setText(new StringBuilder(String.valueOf(this.m.j / 10000)).toString());
                this.w.setVisibility(0);
            }
            if (this.m.h < 100000) {
                this.t.setText(new StringBuilder(String.valueOf(this.m.h)).toString());
                this.u.setVisibility(8);
            } else {
                this.t.setText(new StringBuilder(String.valueOf(this.m.h / 10000)).toString());
                this.u.setVisibility(0);
            }
            this.o.a(this.F, new bv(this));
            if (!a.a((CharSequence) this.m.f)) {
                r rVar = new r(a.n(this.m.f), new bw(this), 5, null);
                rVar.a(this.m.f);
                new Thread(rVar).start();
                this.y.setOnClickListener(new by(this));
            }
            b(new ca(this, this));
        }
    }

    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        super.a(bundle);
        setContentView((int) R.layout.activity_plus_sinaweibo);
        this.o = (HeaderLayout) findViewById(R.id.layout_header);
        this.o.setTitleText((int) R.string.plus_txweibo);
        this.F = new bi(this);
        this.F.a("访问微博");
        this.F.a((int) R.drawable.ic_topbar_link);
        this.p = (HandyListView) findViewById(R.id.weibo_list);
        this.D = g.o().inflate((int) R.layout.common_list_loadmore, (ViewGroup) null);
        this.D.setVisibility(8);
        this.E = (LoadingButton) this.D.findViewById(R.id.btn_loadmore);
        this.E.setNormalIconResId(R.drawable.ic_btn_inner_clock);
        this.n = g.o().inflate((int) R.layout.include_weibo_prifile, (ViewGroup) null);
        this.q = (ImageView) this.n.findViewById(R.id.weibo_iv_avator);
        this.q.setEnabled(false);
        this.r = (TextView) this.n.findViewById(R.id.weibo_tv_name);
        this.s = (TextView) this.n.findViewById(R.id.weibo_tv_followcount);
        this.t = (TextView) this.n.findViewById(R.id.weibo_tv_fanscount);
        this.u = this.n.findViewById(R.id.weibo_tv_fanscount_w);
        this.v = (TextView) this.n.findViewById(R.id.weibo_tv_weibocount);
        this.w = this.n.findViewById(R.id.weibo_tv_weibocount_w);
        this.x = (ImageView) this.n.findViewById(R.id.weibo_iv_gender);
        this.y = this.n.findViewById(R.id.weibo_iv_cover);
        this.z = (TextView) this.n.findViewById(R.id.weibo_tv_location);
        this.A = (TextView) this.n.findViewById(R.id.weibo_tv_sign);
        this.B = this.n.findViewById(R.id.weibo_layout_vip);
        this.C = (TextView) this.n.findViewById(R.id.weibo_tv_vip);
        this.q.setOnClickListener(this);
        this.E.setOnProcessListener(this);
        d();
    }

    /* access modifiers changed from: protected */
    public final void d() {
        this.p.addHeaderView(this.n);
        this.p.addFooterView(this.D);
        this.D.setVisibility(8);
        HandyListView handyListView = this.p;
        this.i = new hu(this);
        this.p.setAdapter((ListAdapter) this.i);
        if (getIntent().getExtras() != null) {
            if (getIntent().getExtras().get("uid") != null) {
                this.l = getIntent().getExtras().getInt("keyType", 1);
                if (this.l == 1) {
                    this.j = (String) getIntent().getExtras().get("uid");
                    this.k = null;
                } else {
                    this.k = (String) getIntent().getExtras().get("uid");
                    this.j = null;
                }
                b(new bz(this, this));
                return;
            } else if (getIntent().getExtras().get("profile") != null) {
                this.m = (b) getIntent().getExtras().get("profile");
                this.j = this.f.h;
                v();
                return;
            }
        }
        finish();
    }

    public void onClick(View view) {
        switch (view.getId()) {
        }
    }

    public final void u() {
        b(new ca(this, this));
    }
}
