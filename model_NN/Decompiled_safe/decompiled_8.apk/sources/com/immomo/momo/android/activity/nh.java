package com.immomo.momo.android.activity;

import android.support.v4.view.u;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.android.view.WelcomeView;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.bj;
import com.immomo.momo.util.e;

final class nh extends u {

    /* renamed from: a  reason: collision with root package name */
    private View f2025a;
    private bj b;
    private /* synthetic */ WelcomeActivity c;

    private nh(WelcomeActivity welcomeActivity) {
        this.c = welcomeActivity;
    }

    /* synthetic */ nh(WelcomeActivity welcomeActivity, byte b2) {
        this(welcomeActivity);
    }

    private View b(ViewGroup viewGroup) {
        if (this.f2025a == null) {
            this.f2025a = g.o().inflate((int) R.layout.include_welcome_item_first, (ViewGroup) null);
        }
        if (this.b != null) {
            try {
                ((WelcomeView) this.f2025a.findViewById(R.id.welcomelogin_welcomeview)).a(this.b.b);
            } catch (Exception e) {
            }
            ((TextView) this.f2025a.findViewById(R.id.welcomelogin_tv_info)).setText(this.b.f3014a);
        }
        viewGroup.addView(this.f2025a);
        return this.f2025a;
    }

    private View b(ViewGroup viewGroup, int i) {
        View inflate = g.o().inflate((int) R.layout.include_welcome_item_nomarl, (ViewGroup) null);
        ImageView imageView = (ImageView) inflate.findViewById(R.id.welcomelogin_img_bg);
        ImageView imageView2 = (ImageView) inflate.findViewById(R.id.welcomelogin_img_photo);
        TextView textView = (TextView) inflate.findViewById(R.id.welcomelogin_tv_no);
        TextView textView2 = (TextView) inflate.findViewById(R.id.welcomelogin_tv_job);
        TextView textView3 = (TextView) inflate.findViewById(R.id.welcomelogin_tv_info);
        inflate.findViewById(R.id.welcomelogin_tv_location);
        if (this.c.n.getValue(i, this.c.h) && this.c.h.resourceId > 0) {
            try {
                imageView.setImageDrawable(this.c.n.getDrawable(i));
            } catch (Exception e) {
            }
        }
        if (this.c.o.getValue(i, this.c.h) && this.c.h.resourceId > 0) {
            try {
                imageView2.setImageDrawable(this.c.o.getDrawable(i));
            } catch (Exception e2) {
            }
        }
        if (this.c.q != null && this.c.q.length > i) {
            textView.setText(this.c.q[i]);
        }
        if (this.c.p != null && this.c.p.length > i) {
            textView2.setText(this.c.p[i]);
        }
        if (this.c.r != null && this.c.r.length > i) {
            textView3.setText(this.c.r[i]);
            int indexOf = this.c.r[i].indexOf(this.c.s[i]);
            e.a(textView3, indexOf, this.c.s[i].length() + indexOf, R.style.Style_Text_blue);
        }
        viewGroup.addView(inflate);
        return inflate;
    }

    public final Object a(ViewGroup viewGroup, int i) {
        return i % 5 == 0 ? b(viewGroup) : b(viewGroup, (i % 5) - 1);
    }

    public final void a(ViewGroup viewGroup, int i, Object obj) {
        viewGroup.removeView((View) obj);
    }

    public final void a(bj bjVar) {
        this.b = bjVar;
        c();
        if (this.f2025a != null) {
            ((WelcomeView) this.f2025a.findViewById(R.id.welcomelogin_welcomeview)).a(this.b.b);
            ((TextView) this.f2025a.findViewById(R.id.welcomelogin_tv_info)).setText(this.b.f3014a);
        }
    }

    public final boolean a(View view, Object obj) {
        return view == obj;
    }

    public final int b() {
        return Integer.MAX_VALUE;
    }
}
