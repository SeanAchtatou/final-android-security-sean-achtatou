package com.immomo.momo.android.pay;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import com.alipay.android.app.IRemoteServiceCallback;

final class ar extends IRemoteServiceCallback.Stub {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ap f2536a;

    ar(ap apVar) {
        this.f2536a = apVar;
    }

    public final boolean isHideLoadingScreen() {
        return false;
    }

    public final void payEnd(boolean z, String str) {
    }

    public final void startActivity(String str, String str2, int i, Bundle bundle) {
        Intent intent = new Intent("android.intent.action.MAIN", (Uri) null);
        if (bundle == null) {
            bundle = new Bundle();
        }
        try {
            bundle.putInt("CallingPid", i);
            intent.putExtras(bundle);
        } catch (Exception e) {
            e.printStackTrace();
        }
        intent.setClassName(str, str2);
        this.f2536a.d.startActivity(intent);
    }
}
