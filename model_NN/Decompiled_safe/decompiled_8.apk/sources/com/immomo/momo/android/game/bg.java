package com.immomo.momo.android.game;

import android.content.Context;
import com.immomo.a.a.f.b;
import com.immomo.momo.R;
import com.immomo.momo.a.a;
import com.immomo.momo.android.b.o;
import com.immomo.momo.android.b.z;
import com.immomo.momo.android.c.d;
import com.immomo.momo.protocol.a.m;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

final class bg extends d {

    /* renamed from: a  reason: collision with root package name */
    private boolean f2427a = true;
    private o c = null;
    private /* synthetic */ NearbyPlayersActivity d;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public bg(NearbyPlayersActivity nearbyPlayersActivity, Context context) {
        super(context);
        this.d = nearbyPlayersActivity;
        nearbyPlayersActivity.k = this;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        ArrayList arrayList = new ArrayList();
        this.d.l = b.a();
        this.c = z.a(this.d, this.d.l);
        this.d.l = (String) null;
        if (this.c != null) {
            this.f2427a = m.a().a(arrayList, 0, this.c, this.d.o);
            this.d.m.a(arrayList);
        } else {
            new a(this.d.getString(R.string.errormsg_location_failed));
        }
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public final void a() {
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        this.d.n = this.c;
        this.d.j.a((Collection) ((List) obj));
        if (!this.f2427a) {
            this.d.i.setVisibility(8);
        } else {
            this.d.i.setVisibility(0);
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.d.h.n();
        this.d.k = (bg) null;
    }
}
