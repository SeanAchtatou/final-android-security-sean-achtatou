package com.immomo.momo.android.activity.group;

import android.view.View;
import com.immomo.momo.util.k;

final class ct implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ GroupProfileActivity f1592a;

    ct(GroupProfileActivity groupProfileActivity) {
        this.f1592a = groupProfileActivity;
    }

    public final void onClick(View view) {
        if (this.f1592a.o.d(this.f1592a.r.b, this.f1592a.f.h) == 1 || this.f1592a.o.d(this.f1592a.r.b, this.f1592a.f.h) == 2) {
            new k("C", "C3105").e();
            GroupProfileActivity.d(this.f1592a);
        } else if (this.f1592a.K) {
            new k("C", "C3105").e();
            GroupProfileActivity.f(this.f1592a);
        }
    }
}
