package com.immomo.momo.android.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.immomo.momo.g;

public class BootCompletedReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        if (g.q() != null) {
            g.d().d();
        }
    }
}
