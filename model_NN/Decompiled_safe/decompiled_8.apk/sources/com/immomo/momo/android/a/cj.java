package com.immomo.momo.android.a;

import android.view.View;

final class cj implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ch f755a;
    private final /* synthetic */ int b;

    cj(ch chVar, int i) {
        this.f755a = chVar;
        this.b = i;
    }

    public final void onClick(View view) {
        this.f755a.e.getOnItemClickListener().onItemClick(this.f755a.e, view, this.b, (long) view.getId());
    }
}
