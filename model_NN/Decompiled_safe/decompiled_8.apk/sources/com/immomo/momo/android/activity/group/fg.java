package com.immomo.momo.android.activity.group;

import android.content.Context;
import android.support.v4.b.a;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.a.j;
import com.immomo.momo.protocol.a.n;

final class fg extends d {

    /* renamed from: a  reason: collision with root package name */
    private v f1655a;
    private j c;
    private /* synthetic */ ShareGroupPartyActivity d;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public fg(ShareGroupPartyActivity shareGroupPartyActivity, Context context) {
        super(context);
        this.d = shareGroupPartyActivity;
    }

    /* access modifiers changed from: protected */
    public final Object a(Object... objArr) {
        this.c = n.a().a(this.d.k, this.d.p, this.d.r, this.d.q, this.d.s, this.d.t);
        return null;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.f1655a = new v(this.d);
        this.f1655a.a("请求提交中");
        this.f1655a.setCancelable(true);
        super.a();
    }

    /* access modifiers changed from: protected */
    public final void a(Object obj) {
        if (this.d.s && a.f(this.c.b)) {
            com.immomo.momo.plugin.g.a a2 = com.immomo.momo.plugin.g.a.a();
            String str = this.c.b;
            ShareGroupPartyActivity shareGroupPartyActivity = this.d;
            a2.a(str, "我在陌陌群发起了活动", ShareGroupPartyActivity.c(this.d.k));
        } else if (a.f(this.c.f2889a)) {
            a(this.c.f2889a);
        }
        this.d.finish();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        if (this.f1655a != null) {
            this.f1655a.dismiss();
        }
        super.b();
    }
}
