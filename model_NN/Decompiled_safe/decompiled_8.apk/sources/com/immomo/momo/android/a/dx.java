package com.immomo.momo.android.a;

import android.content.Intent;
import android.view.View;
import com.immomo.momo.android.activity.OtherProfileActivity;
import com.immomo.momo.service.bean.a.b;

final class dx implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ dn f787a;
    private final /* synthetic */ b b;

    dx(dn dnVar, b bVar) {
        this.f787a = dnVar;
        this.b = bVar;
    }

    public final void onClick(View view) {
        Intent intent = new Intent();
        intent.setClass(this.f787a.d, OtherProfileActivity.class);
        intent.putExtra("tag", "local");
        intent.putExtra("momoid", this.b.g());
        this.f787a.d.startActivity(intent);
    }
}
