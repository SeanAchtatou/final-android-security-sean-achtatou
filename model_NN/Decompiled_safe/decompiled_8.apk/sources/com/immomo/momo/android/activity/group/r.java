package com.immomo.momo.android.activity.group;

import android.content.Intent;
import android.view.View;
import com.immomo.momo.R;
import com.immomo.momo.android.map.SelectSiteAMapActivity;

final class r implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ EditGroupProfileActivity f1692a;

    r(EditGroupProfileActivity editGroupProfileActivity) {
        this.f1692a = editGroupProfileActivity;
    }

    public final void onClick(View view) {
        switch (view.getId()) {
            case R.id.profile_layout_sitename /*2131165522*/:
                Intent intent = new Intent(this.f1692a, SelectSiteAMapActivity.class);
                intent.putExtra("sitetype", this.f1692a.m.L);
                this.f1692a.startActivityForResult(intent, 151);
                return;
            case R.id.tv_choose_location /*2131165523*/:
            case R.id.profile_tv_sitename /*2131165524*/:
            case R.id.layout_name /*2131165525*/:
            default:
                return;
        }
    }
}
