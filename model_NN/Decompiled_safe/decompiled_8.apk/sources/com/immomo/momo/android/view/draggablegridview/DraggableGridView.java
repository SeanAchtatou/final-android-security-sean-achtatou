package com.immomo.momo.android.view.draggablegridview;

import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.os.Handler;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.ImageView;
import com.immomo.momo.util.m;
import java.util.ArrayList;
import java.util.Collections;
import mm.purchasesdk.PurchaseCode;

public class DraggableGridView extends ViewGroup implements View.OnClickListener, View.OnLongClickListener, View.OnTouchListener {
    private static float h = 0.95f;
    private static int o = 150;

    /* renamed from: a  reason: collision with root package name */
    protected int f2787a;
    protected int b = 0;
    protected float c = 0.0f;
    protected Handler d = new Handler();
    protected int e = -1;
    protected int f = -1;
    protected boolean g = false;
    private int i;
    private int j;
    private int k;
    private int l = -1;
    private int m = -1;
    private boolean n = true;
    private ArrayList p = new ArrayList();
    private d q;
    private View.OnClickListener r;
    private View.OnTouchListener s;
    private AdapterView.OnItemClickListener t;
    private a u;
    private c v;
    private Runnable w;

    public DraggableGridView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        new m(this);
        this.w = new b(this);
        super.setOnTouchListener(this);
        super.setOnClickListener(this);
        super.setOnLongClickListener(this);
        this.d.removeCallbacks(this.w);
        this.d.postAtTime(this.w, SystemClock.uptimeMillis() + 500);
        setChildrenDrawingOrderEnabled(true);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        this.k = displayMetrics.densityDpi;
    }

    private int a(int i2) {
        int i3 = i2 - this.f2787a;
        int i4 = 0;
        while (i3 > 0) {
            if (i3 < this.j) {
                return i4;
            }
            i3 -= this.j + this.f2787a;
            i4++;
        }
        return -1;
    }

    private int a(int i2, int i3) {
        int i4;
        int a2 = a(i2);
        int a3 = a(this.b + i3);
        if (a2 == -1 || a3 == -1 || (i4 = a2 + (a3 * this.i)) >= getChildCount()) {
            return -1;
        }
        return i4;
    }

    private Point b(int i2) {
        return new Point(((i2 % this.i) * (this.j + this.f2787a)) + this.f2787a, (((i2 / this.i) * (this.j + this.f2787a)) + this.f2787a) - this.b);
    }

    private void b() {
        if (this.q != null) {
            this.q.a(this.e, this.m);
        }
        ArrayList arrayList = new ArrayList();
        for (int i2 = 0; i2 < getChildCount(); i2++) {
            getChildAt(i2).clearAnimation();
            arrayList.add(getChildAt(i2));
        }
        removeAllViews();
        while (this.e != this.m) {
            if (this.m == arrayList.size()) {
                arrayList.add((View) arrayList.remove(this.e));
                this.e = this.m;
            } else if (this.e < this.m) {
                Collections.swap(arrayList, this.e, this.e + 1);
                this.e++;
            } else if (this.e > this.m) {
                Collections.swap(arrayList, this.e, this.e - 1);
                this.e--;
            }
        }
        for (int i3 = 0; i3 < arrayList.size(); i3++) {
            this.p.set(i3, -1);
            addView((View) arrayList.get(i3));
        }
        onLayout(true, getLeft(), getTop(), getRight(), getBottom());
    }

    /* access modifiers changed from: protected */
    public final void a() {
        int height = getHeight() / 2;
        int max = Math.max(getMaxScroll(), 0);
        if (this.b < (-height)) {
            this.b = -height;
            this.c = 0.0f;
        } else if (this.b > max + height) {
            this.b = height + max;
            this.c = 0.0f;
        } else if (this.b < 0) {
            if (this.b >= -3) {
                this.b = 0;
            } else if (!this.g) {
                this.b -= this.b / 3;
            }
        } else if (this.b <= max) {
        } else {
            if (this.b <= max + 3) {
                this.b = max;
            } else if (!this.g) {
                this.b += (max - this.b) / 3;
            }
        }
    }

    public void addView(View view, int i2, ViewGroup.LayoutParams layoutParams) {
        super.addView(view, i2, layoutParams);
        this.p.add(-1);
    }

    /* access modifiers changed from: protected */
    public int getChildDrawingOrder(int i2, int i3) {
        return this.e == -1 ? i3 : i3 == i2 + -1 ? this.e : i3 >= this.e ? i3 + 1 : i3;
    }

    public int getDraggedPosition() {
        return this.e;
    }

    public View getDraggedView() {
        if (this.e != -1) {
            return getChildAt(this.e);
        }
        return null;
    }

    public int getLastIndex() {
        return a(this.l, this.f);
    }

    /* access modifiers changed from: protected */
    public int getMaxScroll() {
        int ceil = (int) Math.ceil(((double) getChildCount()) / ((double) this.i));
        return (((ceil + 1) * this.f2787a) + (this.j * ceil)) - getHeight();
    }

    public void onClick(View view) {
        if (this.n) {
            if (this.r != null) {
                this.r.onClick(view);
            }
            if (this.t != null && getLastIndex() != -1) {
                this.t.onItemClick(null, getChildAt(getLastIndex()), getLastIndex(), (long) (getLastIndex() / this.i));
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        float f2 = ((float) (i4 - i2)) / (((float) this.k) / 160.0f);
        this.i = 3;
        int i6 = PurchaseCode.AUTH_NOORDER;
        float f3 = f2 - 280.0f;
        while (f3 > 0.0f) {
            this.i++;
            f3 -= (float) i6;
            i6 += 40;
        }
        this.j = (i4 - i2) / this.i;
        this.j = Math.round(((float) this.j) * h);
        this.f2787a = ((i4 - i2) - (this.j * this.i)) / (this.i + 1);
        for (int i7 = 0; i7 < getChildCount(); i7++) {
            if (i7 != this.e) {
                Point b2 = b(i7);
                getChildAt(i7).layout(b2.x, b2.y, b2.x + this.j, b2.y + this.j);
            }
        }
    }

    public boolean onLongClick(View view) {
        int lastIndex;
        if (!this.n || (lastIndex = getLastIndex()) == -1) {
            return false;
        }
        if (this.u != null && !this.u.a(lastIndex)) {
            return false;
        }
        this.e = lastIndex;
        View childAt = getChildAt(this.e);
        int i2 = b(this.e).x + (this.j / 2);
        int i3 = b(this.e).y + (this.j / 2);
        int i4 = i2 - ((this.j * 3) / 4);
        int i5 = i3 - ((this.j * 3) / 4);
        childAt.layout(i4, i5, ((this.j * 3) / 2) + i4, ((this.j * 3) / 2) + i5);
        AnimationSet animationSet = new AnimationSet(true);
        ScaleAnimation scaleAnimation = new ScaleAnimation(0.667f, 1.0f, 0.667f, 1.0f, (float) ((this.j * 3) / 4), (float) ((this.j * 3) / 4));
        scaleAnimation.setDuration((long) o);
        AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.5f);
        alphaAnimation.setDuration((long) o);
        animationSet.addAnimation(scaleAnimation);
        animationSet.addAnimation(alphaAnimation);
        animationSet.setFillEnabled(true);
        animationSet.setFillAfter(true);
        childAt.clearAnimation();
        childAt.startAnimation(animationSet);
        if (this.v != null) {
            this.v.b();
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        super.onMeasure(i2, i3);
        int size = View.MeasureSpec.getSize(i2);
        this.i = 3;
        int i4 = PurchaseCode.AUTH_NOORDER;
        int i5 = size - 280;
        while (i5 > 0) {
            this.i++;
            i5 -= i4;
            i4 += 40;
        }
        this.j = (size + 0) / this.i;
        this.j = Math.round(((float) this.j) * h);
        this.f2787a = ((size + 0) - (this.j * this.i)) / (this.i + 1);
        for (int i6 = 0; i6 < getChildCount(); i6++) {
            if (i6 != this.e) {
                Point b2 = b(i6);
                getChildAt(i6).layout(b2.x, b2.y, b2.x + this.j, b2.y + this.j);
                getChildAt(i6).measure(this.j, this.j);
            }
        }
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        int i2;
        switch (motionEvent.getAction() & PurchaseCode.AUTH_INVALID_APP) {
            case 0:
                this.n = true;
                this.l = (int) motionEvent.getX();
                this.f = (int) motionEvent.getY();
                this.g = true;
                break;
            case 1:
                if (this.e != -1) {
                    View childAt = getChildAt(this.e);
                    if (this.m != -1) {
                        b();
                    } else {
                        Point b2 = b(this.e);
                        childAt.layout(b2.x, b2.y, b2.x + this.j, b2.y + this.j);
                    }
                    childAt.clearAnimation();
                    if (childAt instanceof ImageView) {
                        ((ImageView) childAt).setAlpha(PurchaseCode.AUTH_INVALID_APP);
                    }
                    if (this.v != null) {
                        this.v.c();
                    }
                    this.m = -1;
                    this.e = -1;
                }
                this.g = false;
                break;
            case 2:
                int y = this.f - ((int) motionEvent.getY());
                if (this.e != -1) {
                    int x = (int) motionEvent.getX();
                    int y2 = (int) motionEvent.getY();
                    int i3 = x - ((this.j * 3) / 4);
                    int i4 = y2 - ((this.j * 3) / 4);
                    getChildAt(this.e).layout(i3, i4, ((this.j * 3) / 2) + i3, ((this.j * 3) / 2) + i4);
                    if (a(this.b + y2) == -1) {
                        i2 = -1;
                    } else {
                        int a2 = a(x - (this.j / 4), y2);
                        int a3 = a(x + (this.j / 4), y2);
                        if (a2 == -1 && a3 == -1) {
                            i2 = -1;
                        } else if (a2 == a3) {
                            i2 = -1;
                        } else {
                            if (a3 < 0) {
                                a3 = a2 >= 0 ? a2 + 1 : -1;
                            }
                            i2 = a3;
                        }
                    }
                    if (this.m == i2 || i2 == -1) {
                        if (this.v != null) {
                            c cVar = this.v;
                            motionEvent.getRawX();
                            motionEvent.getRawY();
                            cVar.a();
                        }
                    } else if (this.u == null || this.u.a(i2)) {
                        int i5 = 0;
                        while (true) {
                            int i6 = i5;
                            if (i6 >= getChildCount()) {
                                this.m = i2;
                            } else {
                                View childAt2 = getChildAt(i6);
                                if (i6 != this.e) {
                                    int i7 = (this.e >= i2 || i6 < this.e + 1 || i6 > i2) ? (i2 >= this.e || i6 < i2 || i6 >= this.e) ? i6 : i6 + 1 : i6 - 1;
                                    int intValue = ((Integer) this.p.get(i6)).intValue() != -1 ? ((Integer) this.p.get(i6)).intValue() : i6;
                                    if (intValue != i7) {
                                        Point b3 = b(intValue);
                                        Point b4 = b(i7);
                                        Point point = new Point(b3.x - childAt2.getLeft(), b3.y - childAt2.getTop());
                                        Point point2 = new Point(b4.x - childAt2.getLeft(), b4.y - childAt2.getTop());
                                        TranslateAnimation translateAnimation = new TranslateAnimation(0, (float) point.x, 0, (float) point2.x, 0, (float) point.y, 0, (float) point2.y);
                                        translateAnimation.setDuration((long) o);
                                        translateAnimation.setFillEnabled(true);
                                        translateAnimation.setFillAfter(true);
                                        childAt2.clearAnimation();
                                        childAt2.startAnimation(translateAnimation);
                                        this.p.set(i6, Integer.valueOf(i7));
                                    }
                                }
                                i5 = i6 + 1;
                            }
                        }
                    }
                } else {
                    this.b = this.b + y;
                    a();
                    if (Math.abs(y) > 2) {
                        this.n = false;
                    }
                    onLayout(true, getLeft(), getTop(), getRight(), getBottom());
                }
                this.l = (int) motionEvent.getX();
                this.f = (int) motionEvent.getY();
                this.c = (float) y;
                break;
        }
        if (this.s != null) {
            this.s.onTouch(view, motionEvent);
        }
        return this.e != -1;
    }

    public void removeViewAt(int i2) {
        super.removeViewAt(i2);
        this.p.remove(i2);
    }

    public void setCanDragListener(a aVar) {
        this.u = aVar;
    }

    public void setOnClickListener(View.OnClickListener onClickListener) {
        this.r = onClickListener;
    }

    public void setOnItemClickListener(AdapterView.OnItemClickListener onItemClickListener) {
        this.t = onItemClickListener;
    }

    public void setOnItemDragListener(c cVar) {
        this.v = cVar;
    }

    public void setOnRearrangeListener(d dVar) {
        this.q = dVar;
    }

    public void setOnTouchListener(View.OnTouchListener onTouchListener) {
        this.s = onTouchListener;
    }
}
