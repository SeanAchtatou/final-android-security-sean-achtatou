package com.immomo.momo.android.a.a;

import android.content.Intent;
import android.view.View;
import com.immomo.momo.android.activity.OtherProfileActivity;
import com.immomo.momo.android.activity.maintab.bh;
import com.immomo.momo.service.bean.ax;

final class al implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ah f681a;
    private final /* synthetic */ ax b;

    al(ah ahVar, ax axVar) {
        this.f681a = ahVar;
        this.b = axVar;
    }

    public final void onClick(View view) {
        Intent intent = new Intent();
        bh unused = this.f681a.d;
        intent.setClass(bh.H(), OtherProfileActivity.class);
        intent.putExtra("tag", "local");
        intent.putExtra("momoid", this.b.f3000a);
        this.f681a.d.a(intent);
    }
}
