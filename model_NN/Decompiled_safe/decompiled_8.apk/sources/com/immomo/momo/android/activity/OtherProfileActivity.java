package com.immomo.momo.android.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.b.a;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.a.gv;
import com.immomo.momo.android.broadcast.d;
import com.immomo.momo.android.broadcast.f;
import com.immomo.momo.android.broadcast.j;
import com.immomo.momo.android.broadcast.w;
import com.immomo.momo.android.view.EmoteEditeText;
import com.immomo.momo.android.view.EmoteTextView;
import com.immomo.momo.android.view.HeaderLayout;
import com.immomo.momo.android.view.MomoGridView;
import com.immomo.momo.android.view.ProfilePullScrollView;
import com.immomo.momo.android.view.UserPhotosView;
import com.immomo.momo.android.view.a.n;
import com.immomo.momo.android.view.a.o;
import com.immomo.momo.android.view.bi;
import com.immomo.momo.android.view.du;
import com.immomo.momo.b;
import com.immomo.momo.g;
import com.immomo.momo.service.ai;
import com.immomo.momo.service.aq;
import com.immomo.momo.service.bean.aj;
import com.immomo.momo.service.bean.am;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.service.y;
import com.immomo.momo.util.e;
import java.util.List;

public class OtherProfileActivity extends ah implements du {
    private LinearLayout A;
    private View B;
    private View C;
    private View D;
    private View E;
    private View F;
    private View G;
    private View H;
    private LinearLayout I;
    private LinearLayout J;
    private LinearLayout K;
    private LinearLayout L;
    private LinearLayout M;
    /* access modifiers changed from: private */
    public LinearLayout N;
    private LinearLayout O;
    private LinearLayout P;
    private LinearLayout Q;
    private LinearLayout R;
    private LinearLayout S;
    private LinearLayout T;
    private View U;
    private ImageView V;
    private TextView W;
    private TextView X;
    private TextView Y;
    private TextView Z;
    private ImageView aA;
    private View aB;
    /* access modifiers changed from: private */
    public MomoGridView aC;
    /* access modifiers changed from: private */
    public gv aD;
    /* access modifiers changed from: private */
    public LinearLayout aE;
    private LinearLayout aF;
    private LinearLayout aG;
    private LinearLayout aH;
    private ProfilePullScrollView aI = null;
    private View aJ = null;
    private UserPhotosView aK;
    private UserPhotosView aL;
    /* access modifiers changed from: private */
    public ImageView aM;
    /* access modifiers changed from: private */
    public View aN;
    private ImageView aO;
    private ImageView aP;
    private ImageView aQ;
    private TextView aR;
    private TextView aS;
    private TextView aT;
    private TextView aU;
    private w aV;
    /* access modifiers changed from: private */
    public int aW = 0;
    private boolean aX = false;
    /* access modifiers changed from: private */
    public AnimationDrawable aY = null;
    private d aZ = new gq(this);
    private TextView aa;
    private TextView ab;
    private TextView ac;
    private TextView ad;
    /* access modifiers changed from: private */
    public TextView ae;
    private TextView af;
    private TextView ag;
    private TextView ah;
    private TextView ai;
    private TextView aj;
    private TextView ak;
    private TextView al;
    private TextView am;
    private TextView an;
    private TextView ao;
    private TextView ap;
    private View aq;
    private View ar;
    private TextView as;
    private TextView at;
    private TextView au;
    private TextView av;
    private TextView aw;
    private TextView ax;
    private TextView ay;
    private ImageView az;
    /* access modifiers changed from: private */
    public int ba = 0;
    /* access modifiers changed from: private */
    public int bb = 0;
    private View.OnClickListener bc = new hf(this);
    private View.OnClickListener bd;
    private View.OnClickListener be;
    Handler h = new Handler();
    /* access modifiers changed from: private */
    public hq i;
    /* access modifiers changed from: private */
    public hu j;
    /* access modifiers changed from: private */
    public List k = null;
    /* access modifiers changed from: private */
    public boolean l = false;
    /* access modifiers changed from: private */
    public boolean m = false;
    private String n;
    /* access modifiers changed from: private */
    public String o;
    private HeaderLayout p = null;
    /* access modifiers changed from: private */
    public aq q = null;
    /* access modifiers changed from: private */
    public ai r = null;
    /* access modifiers changed from: private */
    public y s = null;
    /* access modifiers changed from: private */
    public bf t = null;
    private bi u;
    private bi v;
    private LinearLayout w;
    private LinearLayout x;
    private LinearLayout y;
    private LinearLayout z;

    public OtherProfileActivity() {
        new hi(this);
        this.bd = new hj(this);
        this.be = new hk(this);
    }

    private void A() {
        if (this.t != null) {
            if (!a.a((CharSequence) this.t.L)) {
                this.I.setVisibility(0);
                e.a(this.ai, this.t.L, this);
            } else {
                this.I.setVisibility(8);
            }
            if (!a.a((CharSequence) this.t.F)) {
                this.L.setVisibility(0);
                e.a(this.al, this.t.F, this);
            } else {
                this.L.setVisibility(8);
            }
            if (!a.a((CharSequence) this.t.D)) {
                this.M.setVisibility(0);
                e.a(this.am, this.t.D, this);
            } else {
                this.M.setVisibility(8);
            }
            if (!a.a((CharSequence) this.t.L) || !a.a((CharSequence) this.t.F) || !a.a((CharSequence) this.t.D)) {
                this.B.setVisibility(0);
            } else {
                this.B.setVisibility(8);
            }
            if (this.t.ar || this.t.an || this.t.at || (this.t.O != null && this.t.O.size() > 0)) {
                this.aC.postDelayed(new hn(this), 80);
            } else {
                this.aE.setVisibility(8);
            }
            if (this.t.aM > 0) {
                this.G.setVisibility(0);
                List list = this.t.aN;
                if (list != null && list.size() > 0) {
                    this.aH.removeAllViews();
                    View inflate = LayoutInflater.from(this).inflate((int) R.layout.listitem_jointieba, (ViewGroup) null);
                    View inflate2 = LayoutInflater.from(this).inflate((int) R.layout.listitem_jointieba, (ViewGroup) null);
                    this.aH.addView(inflate);
                    if (list.size() > 3) {
                        this.aH.addView(inflate2);
                    }
                    int i2 = 0;
                    while (i2 < list.size()) {
                        View view = i2 >= 3 ? inflate2 : inflate;
                        TextView textView = i2 % 3 == 0 ? (TextView) view.findViewById(R.id.tv_left) : i2 % 3 == 1 ? (TextView) view.findViewById(R.id.tv_middle) : i2 % 3 == 2 ? (TextView) view.findViewById(R.id.tv_right) : null;
                        if (textView != null) {
                            textView.setVisibility(0);
                            textView.setText(((com.immomo.momo.service.bean.c.d) list.get(i2)).b);
                            textView.setTag(R.id.tag_item, list.get(i2));
                            textView.setOnClickListener(this.be);
                        }
                        i2++;
                    }
                }
                this.ax.setText(String.valueOf(this.t.aL) + "(" + this.t.aM + ")");
            } else {
                this.G.setVisibility(8);
            }
            this.H.setVisibility(this.t.aM > 0 ? 0 : 8);
            if (this.t.aP > 0) {
                this.S.setVisibility(0);
                this.T.setVisibility(0);
                this.aw.setText(String.valueOf(this.t.aO) + "(" + this.t.aP + ")");
                this.ay.setText(this.t.aQ);
                return;
            }
            this.S.setVisibility(8);
            this.T.setVisibility(8);
        }
    }

    /* access modifiers changed from: private */
    public void B() {
        if (this.l) {
            this.ad.setText("自己");
        } else if (this.t.P != null) {
            if (this.t.P.equals("none")) {
                this.ad.setText((int) R.string.relation_stanger);
            } else if (this.t.P.equals("follow")) {
                this.ad.setText((int) R.string.relation_follow);
            } else if (this.t.P.equals("fans")) {
                this.ad.setText((int) R.string.relation_fans);
            } else if (this.t.P.equals("both")) {
                this.ad.setText((int) R.string.relation_both);
            }
        }
        if (this.t != null) {
            if (a.a((CharSequence) this.t.P) || this.t.P.equals("none")) {
                this.m = false;
            } else if (this.t.P.equals("fans")) {
                this.m = false;
            } else if (this.t.P.equals("follow")) {
                this.m = true;
            } else if (this.t.P.equals("both")) {
                this.m = true;
            }
        }
        if (this.m) {
            this.y.setVisibility(8);
            this.x.setVisibility(8);
            this.z.setVisibility(8);
            C();
        } else {
            this.y.setVisibility(8);
            this.x.setVisibility(0);
            this.z.setVisibility(0);
        }
        if (this.g == null || !"both".equals(this.t.P) || this.l) {
            this.N.setVisibility(8);
            this.O.setVisibility(8);
            this.P.setVisibility(8);
            this.Q.setVisibility(8);
            return;
        }
        if (this.t.d == null || !this.g.c) {
            this.N.setVisibility(8);
        } else if (!a.a((CharSequence) this.t.o)) {
            this.N.setVisibility(0);
            this.ae.setText(this.t.o);
        } else {
            this.N.setVisibility(8);
            b(new hw(this, this, this.t.d));
        }
        if (!this.f.an || a.a((CharSequence) this.t.p)) {
            this.O.setVisibility(8);
        } else {
            this.af.setText(this.t.p);
            this.O.setVisibility(0);
        }
        if (!this.f.at || a.a((CharSequence) this.t.q)) {
            this.P.setVisibility(8);
        } else {
            this.ag.setText(this.t.q);
            this.P.setVisibility(0);
        }
        if (!this.f.ar || a.a((CharSequence) this.t.r)) {
            this.Q.setVisibility(8);
            return;
        }
        this.ah.setText(this.t.r);
        this.Q.setVisibility(0);
    }

    /* access modifiers changed from: private */
    public void C() {
        if (this.m) {
            this.u.setVisibility(0);
        } else {
            this.u.setVisibility(8);
        }
        if (this.t.b()) {
            this.aT.setText(this.t.h());
            this.p.setTitleText("会员资料");
            return;
        }
        this.p.setTitleText(this.t.h());
    }

    static /* synthetic */ void C(OtherProfileActivity otherProfileActivity) {
        View inflate = g.o().inflate((int) R.layout.dialog_otherprofile_remark, (ViewGroup) null);
        EmoteEditeText emoteEditeText = (EmoteEditeText) inflate.findViewById(R.id.edittext_remark);
        EmoteTextView emoteTextView = (EmoteTextView) inflate.findViewById(R.id.tv_original_name);
        if (otherProfileActivity.t != null) {
            if (!a.a((CharSequence) otherProfileActivity.t.l)) {
                emoteEditeText.setText(otherProfileActivity.t.l);
                emoteEditeText.setSelection(otherProfileActivity.t.l.length());
            }
            emoteTextView.setText("原用户名： " + otherProfileActivity.t.i);
            emoteEditeText.requestFocus();
            InputMethodManager inputMethodManager = (InputMethodManager) otherProfileActivity.getSystemService("input_method");
            if (!(inputMethodManager == null || emoteEditeText == null)) {
                inputMethodManager.showSoftInput(emoteEditeText, 2);
                inputMethodManager.toggleSoftInput(2, 1);
            }
            emoteEditeText.addTextChangedListener(new com.immomo.momo.util.aq(24));
            n nVar = new n(otherProfileActivity);
            nVar.setTitle("修改备注");
            nVar.setContentView(inflate);
            nVar.a();
            nVar.a(0, otherProfileActivity.getString(R.string.dialog_btn_confim), new ha(otherProfileActivity, emoteEditeText));
            nVar.a(1, otherProfileActivity.getString(R.string.dialog_btn_cancel), new hb(otherProfileActivity, emoteEditeText));
            nVar.show();
        }
    }

    private void D() {
        if (!"notreflsh".equals(this.n)) {
            b(new hx(this, this));
        }
    }

    static /* synthetic */ void a(OtherProfileActivity otherProfileActivity, int i2) {
        if (otherProfileActivity.t != null) {
            if (i2 == 0) {
                if ("none".equals(otherProfileActivity.t.P)) {
                    otherProfileActivity.t.P = "follow";
                } else if ("fans".equals(otherProfileActivity.t.P)) {
                    otherProfileActivity.t.P = "both";
                }
                otherProfileActivity.q.h(otherProfileActivity.t);
                otherProfileActivity.f.x++;
                Intent intent = new Intent(j.f2353a);
                intent.putExtra("key_momoid", otherProfileActivity.t.h);
                intent.putExtra("newfollower", otherProfileActivity.f.v);
                intent.putExtra("followercount", otherProfileActivity.f.w);
                intent.putExtra("total_friends", otherProfileActivity.f.x);
                intent.putExtra("relation", otherProfileActivity.t.P);
                otherProfileActivity.sendBroadcast(intent);
            } else if (i2 == 1) {
                if ("both".equals(otherProfileActivity.t.P)) {
                    otherProfileActivity.t.P = "fans";
                } else if ("follow".equals(otherProfileActivity.t.P)) {
                    otherProfileActivity.t.P = "none";
                }
                otherProfileActivity.q.i(otherProfileActivity.t.h);
                if (otherProfileActivity.f.x > 0) {
                    bf bfVar = otherProfileActivity.f;
                    bfVar.x--;
                }
                Intent intent2 = new Intent(j.b);
                intent2.putExtra("key_momoid", otherProfileActivity.t.h);
                intent2.putExtra("newfollower", otherProfileActivity.f.v);
                intent2.putExtra("followercount", otherProfileActivity.f.w);
                intent2.putExtra("total_friends", otherProfileActivity.f.x);
                intent2.putExtra("relation", otherProfileActivity.t.P);
                otherProfileActivity.sendBroadcast(intent2);
            }
            otherProfileActivity.q.d(otherProfileActivity.f.x, otherProfileActivity.f.h);
            otherProfileActivity.q.d(otherProfileActivity.t.h, otherProfileActivity.t.P);
            otherProfileActivity.B();
            otherProfileActivity.C();
        }
    }

    static /* synthetic */ void a(OtherProfileActivity otherProfileActivity, EmoteEditeText emoteEditeText) {
        InputMethodManager inputMethodManager = (InputMethodManager) otherProfileActivity.getSystemService("input_method");
        if (inputMethodManager != null && emoteEditeText != null) {
            inputMethodManager.hideSoftInputFromWindow(emoteEditeText.getWindowToken(), 0);
        }
    }

    private void c(Bundle bundle) {
        if (bundle == null || !bundle.getBoolean("from_saveinstance", false)) {
            Intent intent = getIntent();
            this.n = intent.getStringExtra("tag");
            this.o = intent.getStringExtra("momoid");
        } else {
            this.o = (String) bundle.get("momoid");
            this.n = (String) bundle.get("tag");
            this.n = this.n == null ? "local" : this.n;
        }
        if (!a.a((CharSequence) this.o)) {
            this.l = this.f != null && this.o.equals(this.f.h);
            if (this.l) {
                this.t = this.f;
                this.q.a(this.f, this.o);
            } else {
                this.t = this.q.b(this.o);
            }
            if (this.t == null) {
                this.t = new bf(this.o);
                this.p.setTitleText(this.t.h);
                this.ab.setText(this.t.h);
            } else if (!a.a((CharSequence) this.t.ag)) {
                this.t.ah = this.r.a(this.t.ag);
            }
            if (this.l) {
                this.A.setVisibility(8);
                this.u.setVisibility(8);
            } else {
                this.A.setVisibility(0);
            }
            x();
            z();
            B();
            w();
        }
    }

    static /* synthetic */ void i(OtherProfileActivity otherProfileActivity) {
        n nVar = new n(otherProfileActivity);
        nVar.setTitle("提示");
        nVar.a();
        nVar.a(g.a((int) R.string.dialog_follow_tip));
        nVar.a(0, (int) R.string.dialog_btn_confim, new hc(otherProfileActivity));
        nVar.a(1, (int) R.string.dialog_btn_cancel, new hd());
        nVar.show();
    }

    static /* synthetic */ void j(OtherProfileActivity otherProfileActivity) {
        n nVar = new n(otherProfileActivity);
        nVar.setTitle("提示");
        nVar.a();
        nVar.a(g.a((int) R.string.dialog_unfollow_tip));
        nVar.a(0, (int) R.string.dialog_btn_confim, new he(otherProfileActivity));
        nVar.a(1, (int) R.string.dialog_btn_cancel, new hg());
        nVar.show();
    }

    static /* synthetic */ void k(OtherProfileActivity otherProfileActivity) {
        o oVar = new o(otherProfileActivity, (int) R.array.report_block_items);
        oVar.a(new gu(otherProfileActivity));
        oVar.show();
    }

    static /* synthetic */ void n(OtherProfileActivity otherProfileActivity) {
        if (otherProfileActivity.t != null && !a.a((CharSequence) otherProfileActivity.t.h)) {
            otherProfileActivity.q.p(otherProfileActivity.t.h);
            Intent intent = new Intent(com.immomo.momo.android.broadcast.e.f2348a);
            intent.putExtra("key_momoid", otherProfileActivity.t.h);
            otherProfileActivity.sendBroadcast(intent);
        }
    }

    /* access modifiers changed from: private */
    public void v() {
        if (this.t != null) {
            x();
            z();
            A();
            y();
        }
    }

    /* access modifiers changed from: private */
    public void w() {
        int J2 = g.J();
        int i2 = (J2 * 7) / 8;
        ViewGroup.LayoutParams layoutParams = this.D.getLayoutParams();
        layoutParams.width = J2;
        layoutParams.height = i2;
        this.D.setLayoutParams(layoutParams);
        ViewGroup.LayoutParams layoutParams2 = this.aN.getLayoutParams();
        this.aW = Math.round(((float) i2) * 0.2f);
        layoutParams2.width = J2;
        layoutParams2.height = i2;
        this.aN.setLayoutParams(layoutParams2);
        if (!this.aX) {
            this.aN.scrollTo(0, this.aW);
            this.aX = true;
        }
        int photoHeight = this.aL.getPhotoHeight();
        if (photoHeight < this.aW) {
            photoHeight = this.aW;
        }
        if (this.t.b()) {
            this.aI.setMaxScroll(photoHeight);
        } else {
            this.aI.setMaxScroll(0);
        }
    }

    static /* synthetic */ void w(OtherProfileActivity otherProfileActivity) {
        o oVar = new o(otherProfileActivity, new String[]{"取消关注", "备注", "拉黑/举报"});
        oVar.setTitle((int) R.string.dialog_title_avatar_long_press);
        oVar.a();
        oVar.a(new gz(otherProfileActivity));
        oVar.show();
    }

    private void x() {
        if (this.t != null) {
            if (this.t.b()) {
                this.F.setVisibility(8);
                this.E.setVisibility(8);
                this.e.a((Object) ("-fillVipAvatarBg--------------------------------user.backgroud=" + this.t.af));
                if (!a.a((CharSequence) this.t.af)) {
                    hl hlVar = new hl(this);
                    if (this.aM.getTag(R.id.tag_item_imageid) == null || !this.aM.getTag(R.id.tag_item_imageid).equals(this.t.af)) {
                        com.immomo.momo.android.c.o oVar = new com.immomo.momo.android.c.o(this.t.af, hlVar, 2, null);
                        Bitmap a2 = com.immomo.momo.util.j.a(this.t.af);
                        this.aM.setTag(R.id.tag_item_imageid, this.t.af);
                        if (a2 != null) {
                            this.aM.setImageBitmap(a2);
                        } else {
                            this.t.setImageLoading(true);
                            oVar.a();
                        }
                    }
                }
                this.aN.setVisibility(0);
                this.D.setVisibility(0);
                this.aL.a(this.t.ae, true, true);
                this.C.setVisibility(0);
                if (this.t.ae == null || this.t.ae.length <= 8) {
                    this.aQ.setVisibility(4);
                } else {
                    this.aQ.setVisibility(0);
                    this.aQ.setBackgroundResource(R.anim.avatar_flip_tip);
                    this.aY = (AnimationDrawable) this.aQ.getBackground();
                    this.aQ.setBackgroundDrawable(this.aY);
                    this.h.post(new hh(this));
                }
                this.aT.setText(this.t.h());
                if (this.t.H != null) {
                    if (this.t.H.equals("F")) {
                        this.aO.setImageResource(R.drawable.ic_user_famale2);
                    } else if (this.t.H.equals("M")) {
                        this.aO.setImageResource(R.drawable.ic_user_male2);
                    }
                }
                this.aS.setText(new StringBuilder(String.valueOf(this.t.I)).toString());
                if (this.t.K != null) {
                    this.aR.setText(this.t.K);
                }
                if (this.t.ac) {
                    this.ap.setVisibility(0);
                    this.ap.setText((int) R.string.user_profile_baned_tip);
                } else {
                    this.ap.setVisibility(8);
                }
                if (this.t.d() < 0.0f) {
                    this.aU.setText(this.t.Z);
                } else {
                    this.aU.setText(String.valueOf(this.t.Z) + (this.t.V ? "(误差大)" : PoiTypeDef.All) + (!a.a(this.t.aa) ? "/" + this.t.aa : PoiTypeDef.All));
                }
            } else {
                this.F.setVisibility(0);
                this.E.setVisibility(0);
                this.C.setVisibility(4);
                this.aN.setVisibility(8);
                this.D.setVisibility(8);
                this.aK.a(this.t.ae, false, false);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.service.y.a(java.lang.String, java.lang.String, boolean):com.immomo.momo.service.bean.a.j
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      com.immomo.momo.service.y.a(int, java.lang.String, java.lang.String):void
      com.immomo.momo.service.y.a(java.lang.String, java.lang.String, int):void
      com.immomo.momo.service.y.a(java.lang.String, boolean, java.lang.String):void
      com.immomo.momo.service.y.a(java.lang.String, java.lang.String, boolean):com.immomo.momo.service.bean.a.j */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup, int, boolean, boolean, int):void
     arg types: [com.immomo.momo.service.bean.a.a, android.widget.ImageView, ?[OBJECT, ARRAY], int, int, int, int]
     candidates:
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, int, android.view.View, android.view.ViewGroup, boolean, int, boolean):com.immomo.momo.android.c.o
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup, int, boolean, boolean, int):void */
    /* access modifiers changed from: private */
    public void y() {
        if (this.k == null || this.k.size() <= 0) {
            this.aF.setVisibility(8);
            return;
        }
        this.aF.setVisibility(0);
        this.ao.setText("加入的群组(" + this.k.size() + ")");
        this.aG.removeAllViews();
        for (int i2 = 0; i2 < this.k.size(); i2++) {
            View inflate = LayoutInflater.from(this).inflate((int) R.layout.otherprofile_joingroup_item, (ViewGroup) null);
            this.aG.addView(inflate);
            com.immomo.momo.service.bean.a.a aVar = (com.immomo.momo.service.bean.a.a) this.k.get(i2);
            ((TextView) inflate.findViewById(R.id.tv_groupname)).setText(aVar.c);
            com.immomo.momo.service.bean.a.j a2 = this.s.a(aVar.b, this.o, false);
            if (a2 != null) {
                if (a2.g == 1) {
                    inflate.findViewById(R.id.group_owner).setVisibility(0);
                } else {
                    inflate.findViewById(R.id.group_owner).setVisibility(8);
                }
            }
            inflate.setTag(R.id.tag_item, aVar.b);
            inflate.setOnClickListener(this.bd);
            com.immomo.momo.util.j.a((aj) aVar, (ImageView) inflate.findViewById(R.id.avatar_imageview), (ViewGroup) null, 3, false, true, g.a(8.0f));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup, int, boolean, boolean, int):void
     arg types: [com.immomo.momo.service.bean.ab, android.widget.ImageView, ?[OBJECT, ARRAY], int, int, int, int]
     candidates:
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, int, android.view.View, android.view.ViewGroup, boolean, int, boolean):com.immomo.momo.android.c.o
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup, int, boolean, boolean, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.b.a(java.lang.String, boolean):android.graphics.Bitmap
     arg types: [java.lang.String, int]
     candidates:
      com.immomo.momo.b.a(java.lang.String, android.graphics.Bitmap):android.graphics.Bitmap
      com.immomo.momo.b.a(java.lang.String, boolean):android.graphics.Bitmap */
    /* access modifiers changed from: private */
    public void z() {
        if (this.t != null) {
            this.v.setVisibility(this.f.h.equals(this.t.h) ? 0 : 8);
            if (this.t.b()) {
                this.p.setTitleText("会员资料");
            } else {
                this.p.setTitleText(this.t.h());
            }
            if (this.t.ah != null) {
                this.aJ.setVisibility(0);
                this.au.setText(this.t.ah.b());
                if (this.t.ah.d != null) {
                    this.av.setText(String.valueOf(this.t.ah.d.f) + "(" + this.t.ah.m + ")");
                } else {
                    this.av.setText(this.t.ah.m);
                }
                if (a.a((CharSequence) this.t.ah.getLoadImageId())) {
                    this.aA.setVisibility(8);
                } else {
                    com.immomo.momo.util.j.a((aj) this.t.ah, this.aA, (ViewGroup) null, 15, true, true, g.a(8.0f));
                    this.aA.setVisibility(0);
                }
            } else {
                this.aJ.setVisibility(8);
            }
            if (this.t.H != null) {
                if (this.t.H.equals("F")) {
                    this.U.setBackgroundResource(R.drawable.bg_gender_famal);
                    this.V.setImageResource(R.drawable.ic_user_famale);
                } else if (this.t.H.equals("M")) {
                    this.U.setBackgroundResource(R.drawable.bg_gender_male);
                    this.V.setImageResource(R.drawable.ic_user_male);
                }
            }
            this.X.setText(new StringBuilder(String.valueOf(this.t.I)).toString());
            if (this.t.K != null) {
                this.W.setText(this.t.K);
            }
            if (this.t.ac) {
                this.an.setVisibility(0);
            } else {
                this.an.setVisibility(8);
            }
            if (this.t.d() < 0.0f) {
                this.aB.setVisibility(8);
                this.Z.setText(this.t.Z);
            } else {
                this.aB.setVisibility(0);
                this.Z.setText(String.valueOf(this.t.Z) + (this.t.V ? "(误差大)" : PoiTypeDef.All));
            }
            if (!a.a((CharSequence) this.t.aa)) {
                this.Y.setText(this.t.aa);
            }
            if (!a.a((CharSequence) this.t.l())) {
                findViewById(R.id.layout_editsign).setVisibility(0);
                e.a(this.aa, this.t.l(), this);
            } else {
                findViewById(R.id.layout_editsign).setVisibility(8);
            }
            if (this.t.h != null) {
                this.ab.setText(this.t.h);
            }
            if (this.t.ay != null) {
                this.ac.setText(a.d(this.t.ay));
            }
            if (!a.a((CharSequence) this.t.N)) {
                e.a(this.as, this.t.N, this);
            } else {
                this.as.setText(PoiTypeDef.All);
            }
            if (!a.a((CharSequence) this.t.C)) {
                this.J.setVisibility(0);
                e.a(this.aj, this.t.C, this);
            } else {
                this.J.setVisibility(8);
            }
            if (!a.a((CharSequence) this.t.Q)) {
                this.K.setVisibility(0);
                e.a(this.ak, this.t.Q, this);
            } else {
                this.K.setVisibility(8);
            }
            if (!a.a((CharSequence) this.t.M) || !a.a((CharSequence) this.t.N)) {
                this.aq.setVisibility(0);
            } else {
                this.aq.setVisibility(8);
            }
            if (this.t.b()) {
                this.R.setVisibility(0);
                if (this.t.c()) {
                    this.aP.setImageResource(R.drawable.ic_profile_vip_year);
                } else {
                    this.aP.setImageResource(R.drawable.ic_profile_vip);
                }
            } else {
                this.R.setVisibility(8);
            }
            if (!a.a((CharSequence) this.t.M)) {
                am c = b.c(this.t.M);
                this.e.a((Object) ("displayIndustry, getIndustry=" + c));
                if (c != null) {
                    this.ar.setVisibility(0);
                    this.e.a((Object) ("displayIndustry, item.name=" + c.b));
                    this.at.setText(a.a(c.b) ? PoiTypeDef.All : c.b);
                    this.e.a((Object) ("displayIndustry, item.icon=" + c.d));
                    if (a.a((CharSequence) c.d)) {
                        this.az.setImageBitmap(null);
                    } else {
                        this.az.setImageBitmap(b.a(c.f2991a, false));
                    }
                } else {
                    this.at.setText(PoiTypeDef.All);
                    this.az.setImageBitmap(null);
                }
            } else {
                this.ar.setVisibility(8);
            }
            B();
            C();
        }
    }

    public final void a(Intent intent, int i2, Bundle bundle, String str) {
        if (intent.getComponent() != null && d.e(intent.getComponent().getClassName())) {
            if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("afromname")) {
                intent.putExtra("afromname", getIntent().getStringExtra("afromname"));
            }
            intent.putExtra("from", l());
        }
        super.a(intent, i2, bundle, str);
    }

    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        super.a(bundle);
        setContentView((int) R.layout.activity_otherprofile);
        this.q = new aq();
        this.s = new y();
        this.r = new ai();
        this.aV = new w(this);
        this.aV.a(this.aZ);
        this.aV.a(f.f2349a);
        this.aV.a(this.aZ);
        this.p = (HeaderLayout) findViewById(R.id.layout_header);
        this.v = new bi(this);
        this.v.a((int) R.drawable.ic_topbar_editor);
        this.u = new bi(getApplicationContext());
        this.u.a((int) R.drawable.ic_topbar_arrow_down);
        this.A = (LinearLayout) findViewById(R.id.profile_layout_bottom);
        this.w = (LinearLayout) findViewById(R.id.profile_layout_start_chat);
        this.x = (LinearLayout) findViewById(R.id.profile_layout_follow);
        this.y = (LinearLayout) findViewById(R.id.profile_layout_unfollow);
        this.z = (LinearLayout) findViewById(R.id.profile_layout_report);
        this.N = (LinearLayout) findViewById(R.id.profile_layout_phonesrc);
        this.O = (LinearLayout) findViewById(R.id.profile_layout_weibosrc);
        this.P = (LinearLayout) findViewById(R.id.profile_layout_tweibosrc);
        this.Q = (LinearLayout) findViewById(R.id.profile_layout_renrensrc);
        this.C = findViewById(R.id.layout_vip_userinfo);
        this.F = findViewById(R.id.layout_avatar_normal);
        this.E = findViewById(R.id.profile_layout_sex_sign);
        this.D = findViewById(R.id.layout_avatar_vip);
        this.aK = (UserPhotosView) this.F.findViewById(R.id.normal_photoview);
        this.aL = (UserPhotosView) this.D.findViewById(R.id.vip_photoview);
        this.aB = findViewById(R.id.layout_time_container);
        this.V = (ImageView) findViewById(R.id.profile_iv_gender);
        this.U = findViewById(R.id.profile_sex_background);
        this.X = (TextView) findViewById(R.id.profile_tv_age);
        this.W = (TextView) findViewById(R.id.profile_tv_constellation);
        this.Y = (TextView) findViewById(R.id.profile_tv_time);
        this.Z = (TextView) findViewById(R.id.profile_tv_distance);
        this.aS = (TextView) findViewById(R.id.vip_profile_tv_age);
        this.aR = (TextView) findViewById(R.id.vip_profile_tv_constellation);
        this.aU = (TextView) findViewById(R.id.vip_profile_tv_timedistance);
        this.aT = (TextView) findViewById(R.id.vip_tv_name);
        this.aO = (ImageView) findViewById(R.id.vip_profile_iv_gender);
        this.aQ = (ImageView) findViewById(R.id.vip_iv_flip_tip);
        this.aM = (ImageView) findViewById(R.id.vip_iv_avatar_bg);
        findViewById(R.id.vip_iv_avatar_bg_cover);
        this.aN = findViewById(R.id.vip_iv_avatar_bglayout);
        this.aa = (TextView) findViewById(R.id.profile_tv_sign);
        this.ab = (TextView) findViewById(R.id.profile_tv_momoid);
        this.ac = (TextView) findViewById(R.id.profile_tv_regist_time);
        this.ad = (TextView) findViewById(R.id.profile_tv_relation);
        this.ae = (TextView) findViewById(R.id.profile_tv_phonesource);
        this.af = (TextView) findViewById(R.id.profile_tv_weibosource);
        this.ag = (TextView) findViewById(R.id.profile_tv_tweibosource);
        this.ah = (TextView) findViewById(R.id.profile_tv_renrensource);
        this.aq = findViewById(R.id.layout_industry);
        this.ar = findViewById(R.id.layout_icon_container);
        this.as = (TextView) findViewById(R.id.tv_industry_job);
        this.at = (TextView) findViewById(R.id.tv_industry);
        this.az = (ImageView) findViewById(R.id.icon_industry);
        this.an = (TextView) findViewById(R.id.profile_baned_tip);
        this.ap = (TextView) findViewById(R.id.tv_editavatar_tip);
        this.J = (LinearLayout) findViewById(R.id.layout_company);
        this.R = (LinearLayout) findViewById(R.id.layout_vip);
        this.aP = (ImageView) findViewById(R.id.icon_vip);
        this.aj = (TextView) findViewById(R.id.profile_tv_company);
        this.K = (LinearLayout) findViewById(R.id.layout_school);
        this.ak = (TextView) findViewById(R.id.profile_tv_school);
        this.aI = (ProfilePullScrollView) findViewById(R.id.scrollview_content);
        this.aJ = findViewById(R.id.layout_feed);
        this.au = (TextView) findViewById(R.id.tv_feeddes);
        this.av = (TextView) findViewById(R.id.tv_placedistance);
        this.aA = (ImageView) findViewById(R.id.iv_feedimg);
        c(bundle);
    }

    /* access modifiers changed from: protected */
    public final void d() {
    }

    /* access modifiers changed from: protected */
    public final void e() {
        super.e();
        try {
            ViewStub viewStub = (ViewStub) findViewById(R.id.vs_profile_content);
            viewStub.setLayoutResource(R.layout.include_otherprofile_stub_part);
            viewStub.inflate();
        } catch (Exception e) {
        }
        this.B = findViewById(R.id.layout_self_introduction);
        this.I = (LinearLayout) findViewById(R.id.layout_interest);
        this.ai = (TextView) findViewById(R.id.profile_tv_interest);
        this.H = findViewById(R.id.layout_moretieba_container);
        this.L = (LinearLayout) findViewById(R.id.layout_hangout);
        this.M = (LinearLayout) findViewById(R.id.layout_website);
        this.al = (TextView) findViewById(R.id.profile_tv_hangout);
        this.am = (TextView) findViewById(R.id.profile_tv_web);
        this.aE = (LinearLayout) findViewById(R.id.profile_layout_bind_info);
        this.aF = (LinearLayout) findViewById(R.id.profile_layout_joingroup);
        this.aG = (LinearLayout) findViewById(R.id.group_container);
        this.ao = (TextView) findViewById(R.id.profile_textview_joingroup_title);
        this.S = (LinearLayout) findViewById(R.id.profile_layout_joins);
        this.T = (LinearLayout) findViewById(R.id.layout_join_envent);
        this.G = findViewById(R.id.profile_layout_jointieba);
        this.aw = (TextView) findViewById(R.id.txt_join_event);
        this.ax = (TextView) findViewById(R.id.tv_jointieba_title);
        this.ay = (TextView) findViewById(R.id.txt_join_eventdesc);
        this.aH = (LinearLayout) findViewById(R.id.tieba_container);
        this.k = this.s.d(this.o);
        this.aC = (MomoGridView) findViewById(R.id.layout_app_icon);
        this.aD = new gv(this);
        this.aC.setAdapter((ListAdapter) this.aD);
        if (this.t != null && this.t.h.equals(this.f.h)) {
            this.p.a(this.v, new ho(this));
        }
        this.p.a(this.u, new hp(this));
        this.w.setOnClickListener(this.bc);
        this.x.setOnClickListener(this.bc);
        this.y.setOnClickListener(this.bc);
        this.z.setOnClickListener(this.bc);
        this.R.setOnClickListener(this.bc);
        findViewById(R.id.layout_feed).setOnClickListener(this.bc);
        this.T.setOnClickListener(this.bc);
        this.aL.setPageSelectedListener(this);
        findViewById(R.id.layout_more_tieba).setOnClickListener(this.bc);
        gr grVar = new gr();
        grVar.a(false);
        this.aK.setAvatarClickListener(grVar);
        this.aL.setAvatarClickListener(grVar);
        this.aC.setOnItemClickListener(new gs(this));
        this.aI.setOnPullScrollChangedListener(new gt(this));
        A();
        y();
        D();
    }

    public void onBindIconClicked(View view) {
        b(view.getId());
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (this.i != null && !this.i.isCancelled()) {
            this.i.cancel(true);
        }
        if (this.j != null && !this.j.isCancelled()) {
            this.j.cancel(true);
        }
        if (this.aV != null) {
            unregisterReceiver(this.aV);
            this.aV = null;
        }
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        String str = intent.getExtras() != null ? (String) intent.getExtras().get("momoid") : null;
        if (!a.a((CharSequence) str) && !this.o.equals(str)) {
            Bundle bundle = new Bundle();
            bundle.putBoolean("from_saveinstance", true);
            bundle.putString("momoid", str);
            c(bundle);
            if (r()) {
                v();
                this.aI.d();
                D();
                return;
            }
            e();
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        bundle.putBoolean("from_saveinstance", true);
        bundle.putString("momoid", this.o);
        bundle.putString("tag", this.n);
        super.onSaveInstanceState(bundle);
    }

    public final void u() {
        if (this.aY != null && this.t.b() && this.aY.isVisible() && this.aY.isRunning()) {
            this.aY.stop();
            this.aQ.setVisibility(4);
        }
    }
}
