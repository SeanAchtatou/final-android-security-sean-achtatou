package com.immomo.momo.android.activity;

import android.content.Context;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.a.f;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.w;
import com.immomo.momo.util.ao;

final class bf extends d {

    /* renamed from: a  reason: collision with root package name */
    private String f1043a = PoiTypeDef.All;
    private String c = PoiTypeDef.All;
    private /* synthetic */ ChangePwdActivity d;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public bf(ChangePwdActivity changePwdActivity, Context context) {
        super(context);
        this.d = changePwdActivity;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        w.a().a(this.f1043a, this.c);
        return true;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.f1043a = this.d.i.getText().toString().trim();
        this.c = this.d.j.getText().toString().trim();
        this.d.o = new v(this.d);
        this.d.o.a("请稍候，正在修改密码...");
        this.d.o.setOnCancelListener(new bg(this));
        this.d.o.show();
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        if (exc instanceof f) {
            ao.e(R.string.errormsg_network_password401);
        } else {
            super.a(exc);
        }
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        ao.b("密码修改成功");
        this.d.finish();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.d.o.dismiss();
    }
}
