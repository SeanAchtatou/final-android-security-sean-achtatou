package com.immomo.momo.android.activity;

import android.content.DialogInterface;

final class ds implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ EditVipProfileActivity f1260a;

    ds(EditVipProfileActivity editVipProfileActivity) {
        this.f1260a = editVipProfileActivity;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        if (this.f1260a.i != null && this.f1260a.i.exists()) {
            this.f1260a.i.delete();
        }
        this.f1260a.setResult(0);
        this.f1260a.finish();
    }
}
