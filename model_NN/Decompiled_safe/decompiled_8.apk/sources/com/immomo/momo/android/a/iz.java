package com.immomo.momo.android.a;

import android.content.Context;
import android.support.v4.b.a;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.view.HandyTextView;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.c.b;
import java.util.List;

public final class iz extends a {
    private List d = null;

    public iz(Context context, List list) {
        super(context, list);
        this.d = list;
        new ja();
    }

    /* renamed from: d */
    public final b getItem(int i) {
        return (b) this.d.get(i);
    }

    public final int getCount() {
        return this.d.size();
    }

    public final long getItemId(int i) {
        return (long) i;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.b.a.a(java.util.Date, boolean):java.lang.String
     arg types: [java.util.Date, int]
     candidates:
      android.support.v4.b.a.a(android.graphics.Bitmap, float):android.graphics.Bitmap
      android.support.v4.b.a.a(android.graphics.Bitmap, java.io.File):android.graphics.Bitmap
      android.support.v4.b.a.a(int, int):java.lang.String
      android.support.v4.b.a.a(java.util.Collection, java.lang.String):java.lang.String
      android.support.v4.b.a.a(java.lang.Object[], java.lang.String):java.lang.String
      android.support.v4.b.a.a(java.lang.String, java.lang.String):org.json.JSONObject
      android.support.v4.b.a.a(float, int[]):void
      android.support.v4.b.a.a(android.app.Activity, int):void
      android.support.v4.b.a.a(android.view.View, java.lang.Runnable):void
      android.support.v4.b.a.a(java.io.File, java.io.File):void
      android.support.v4.b.a.a(java.io.File, java.util.zip.ZipInputStream):void
      android.support.v4.b.a.a(java.lang.Object, java.lang.StringBuilder):void
      android.support.v4.b.a.a(android.content.Context, java.lang.String):boolean
      android.support.v4.b.a.a(com.immomo.a.a.d.b, com.immomo.momo.service.bean.Message):boolean
      android.support.v4.b.a.a(byte[], java.lang.String):byte[]
      android.support.v4.b.a.a(byte[], byte[]):byte[]
      android.support.v4.b.a.a(int[], boolean):byte[]
      android.support.v4.b.a.a(byte[], boolean):int[]
      android.support.v4.b.a.a(int, float):void
      android.support.v4.b.a.a(java.lang.Object, com.immomo.a.a.f):void
      android.support.v4.view.bf.a(int, float):void
      com.immomo.a.a.f.a(java.lang.Object, com.immomo.a.a.f):void
      android.support.v4.b.a.a(java.util.Date, boolean):java.lang.String */
    public final View getView(int i, View view, ViewGroup viewGroup) {
        jb jbVar;
        int i2 = 8;
        if (view == null) {
            view = g.o().inflate((int) R.layout.listitem_tiebaprofile, (ViewGroup) null);
            jbVar = new jb((byte) 0);
            jbVar.f893a = (TextView) view.findViewById(R.id.tv_item_tile);
            jbVar.b = (HandyTextView) view.findViewById(R.id.tv_item_username);
            jbVar.c = (HandyTextView) view.findViewById(R.id.tv_item_distance);
            jbVar.d = (HandyTextView) view.findViewById(R.id.tv_item_createtime);
            jbVar.e = (HandyTextView) view.findViewById(R.id.tv_item_commentscount);
            jbVar.f = (ImageView) view.findViewById(R.id.list_tie_jing);
            jbVar.g = (ImageView) view.findViewById(R.id.list_tie_ding);
            jbVar.h = (ImageView) view.findViewById(R.id.list_tie_new);
            jbVar.i = (ImageView) view.findViewById(R.id.list_tie_hot);
            jbVar.j = (ImageView) view.findViewById(R.id.list_tie_tu);
            view.setTag(jbVar);
        } else {
            jbVar = (jb) view.getTag();
        }
        b bVar = (b) this.d.get(i);
        jbVar.f893a.setText(bVar.d);
        jbVar.f.setVisibility(bVar.p ? 0 : 8);
        jbVar.g.setVisibility(bVar.m ? 0 : 8);
        jbVar.h.setVisibility(bVar.l ? 0 : 8);
        jbVar.i.setVisibility(bVar.q ? 0 : 8);
        ImageView imageView = jbVar.j;
        if (a.f(bVar.getLoadImageId())) {
            i2 = 0;
        }
        imageView.setVisibility(i2);
        if (bVar.e != null) {
            jbVar.b.setText(bVar.e.h());
        }
        if (a.f(bVar.h)) {
            jbVar.c.setText(bVar.h);
        } else {
            jbVar.c.setText(PoiTypeDef.All);
        }
        jbVar.d.setText(a.a(bVar.j, false));
        jbVar.e.setText(new StringBuilder(String.valueOf(bVar.n)).toString());
        return view;
    }
}
