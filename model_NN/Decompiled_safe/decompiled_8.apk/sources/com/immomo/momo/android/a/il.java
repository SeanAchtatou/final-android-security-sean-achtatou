package com.immomo.momo.android.a;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.service.bean.aj;
import com.immomo.momo.service.bean.c.f;
import com.immomo.momo.util.j;
import java.util.List;

public final class il extends a {
    private ListView d;

    public il(Context context, List list, ListView listView) {
        super(context, list);
        this.d = listView;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, int, boolean, boolean):void
     arg types: [com.immomo.momo.service.bean.c.f, android.widget.ImageView, int, int, int]
     candidates:
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup, int, boolean):void
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, int, boolean, boolean):void */
    public final View getView(int i, View view, ViewGroup viewGroup) {
        im imVar;
        f fVar = (f) getItem(i);
        if (view == null || view.getTag() == null) {
            im imVar2 = new im((byte) 0);
            view = a((int) R.layout.listitem_tiebacategory);
            imVar2.f880a = (ImageView) view.findViewById(R.id.tiebacategory_item_img_category);
            imVar2.b = (TextView) view.findViewById(R.id.tiebacategory_item_tx_category_name);
            imVar2.d = (TextView) view.findViewById(R.id.tiebacategory_item_tx_category_des);
            imVar2.c = (TextView) view.findViewById(R.id.tiebacategory_item_tx_category_count);
            view.setTag(imVar2);
            imVar = imVar2;
        } else {
            imVar = (im) view.getTag();
        }
        imVar.b.setText(fVar.b);
        imVar.d.setText(fVar.c);
        imVar.c.setText(String.valueOf(fVar.e) + "个");
        ImageView imageView = imVar.f880a;
        ListView listView = this.d;
        j.a((aj) fVar, imageView, 15, false, true);
        return view;
    }
}
