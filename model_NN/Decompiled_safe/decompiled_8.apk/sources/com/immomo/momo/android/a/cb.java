package com.immomo.momo.android.a;

import android.content.Intent;
import com.immomo.momo.android.activity.event.EventFeedProfileActivity;
import com.immomo.momo.android.activity.event.EventProfileActivity;
import com.immomo.momo.android.view.a.al;
import com.immomo.momo.service.bean.ae;

final class cb implements al {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ca f750a;
    private final /* synthetic */ ae b;

    cb(ca caVar, ae aeVar) {
        this.f750a = caVar;
        this.b = aeVar;
    }

    public final void a(int i) {
        switch (i) {
            case 0:
                if (this.b.o == 1) {
                    EventFeedProfileActivity.a(this.f750a.d, this.b);
                    return;
                }
                return;
            case 1:
                if (this.b.o == 1) {
                    Intent intent = new Intent(this.f750a.d, EventProfileActivity.class);
                    intent.putExtra("eventid", this.b.i);
                    this.f750a.d.startActivity(intent);
                    return;
                }
                return;
            default:
                return;
        }
    }
}
