package com.immomo.momo.android.activity.event;

import com.immomo.momo.android.view.cx;

final class bz implements cx {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ PublishEventFeedActivity f1393a;

    bz(PublishEventFeedActivity publishEventFeedActivity) {
        this.f1393a = publishEventFeedActivity;
    }

    public final void a(int i, int i2) {
        if (i < i2) {
            this.f1393a.e.a((Object) ("OnResize--------------h < oldh=" + i + "<" + i2));
            if (((double) i) <= ((double) this.f1393a.B) * 0.8d) {
                this.f1393a.h = true;
            }
        } else if (((double) i2) <= ((double) this.f1393a.B) * 0.8d && this.f1393a.h && !this.f1393a.C.isShown()) {
            this.f1393a.h = false;
        }
    }
}
