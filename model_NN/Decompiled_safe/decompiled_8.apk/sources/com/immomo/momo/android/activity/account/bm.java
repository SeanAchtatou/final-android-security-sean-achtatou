package com.immomo.momo.android.activity.account;

import android.content.Context;
import android.content.DialogInterface;
import com.immomo.momo.R;
import com.immomo.momo.a.a;
import com.immomo.momo.a.g;
import com.immomo.momo.a.l;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.n;
import com.immomo.momo.protocol.a.w;
import org.json.JSONException;

final class bm extends d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ bc f966a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public bm(bc bcVar, Context context) {
        super(context);
        this.f966a = bcVar;
        if (bcVar.m != null) {
            bcVar.m.cancel(true);
        }
        bcVar.m = this;
    }

    /* access modifiers changed from: protected */
    public final Object a(Object... objArr) {
        w.a().a(1, this.f966a.d.c, this.f966a.d.b, (String) null, this.f966a.j.r);
        return null;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.f966a.k.sendEmptyMessage(2245);
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        this.f966a.b = 0;
        if (exc instanceof com.immomo.momo.a.w) {
            this.f966a.j.a((CharSequence) exc.getMessage());
        } else if ((exc instanceof l) && !this.f966a.j.isFinishing()) {
            n b = n.b(this.f966a.j, exc.getMessage(), (DialogInterface.OnClickListener) null);
            b.setCancelable(false);
            b.show();
        } else if (exc instanceof g) {
            this.f966a.j.a((CharSequence) exc.getMessage());
        } else if (exc instanceof JSONException) {
            this.f966a.j.a((int) R.string.errormsg_dataerror);
        } else if (exc instanceof a) {
            this.f966a.j.a((CharSequence) exc.getMessage());
        } else if (!"mobile".equals(com.immomo.momo.g.Y()) || !com.immomo.momo.g.ac()) {
            this.f966a.j.a((int) R.string.errormsg_server);
        } else {
            this.f966a.j.g();
        }
    }
}
