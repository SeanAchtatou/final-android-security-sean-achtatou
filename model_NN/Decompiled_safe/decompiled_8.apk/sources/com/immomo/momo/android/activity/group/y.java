package com.immomo.momo.android.activity.group;

import android.view.View;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.jt;
import com.immomo.momo.android.view.a.o;
import com.immomo.momo.service.bean.au;

final class y implements View.OnLongClickListener {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public /* synthetic */ EditGroupProfileActivity f1698a;

    y(EditGroupProfileActivity editGroupProfileActivity) {
        this.f1698a = editGroupProfileActivity;
    }

    public final boolean onLongClick(View view) {
        au auVar = (au) this.f1698a.h.get(((jt) view.getTag()).b);
        if (auVar.d) {
            return false;
        }
        o oVar = new o(this.f1698a, (int) R.array.profile_edit_avatar);
        oVar.setTitle((int) R.string.dialog_title_avatar_long_press);
        oVar.a(new z(this, auVar));
        oVar.show();
        return true;
    }
}
