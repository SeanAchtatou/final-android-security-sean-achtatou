package com.immomo.momo.android.activity.tieba;

import android.content.DialogInterface;

final class p implements DialogInterface.OnCancelListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ EditTieActivity f2297a;

    p(EditTieActivity editTieActivity) {
        this.f2297a = editTieActivity;
    }

    public final void onCancel(DialogInterface dialogInterface) {
        dialogInterface.dismiss();
        this.f2297a.finish();
    }
}
