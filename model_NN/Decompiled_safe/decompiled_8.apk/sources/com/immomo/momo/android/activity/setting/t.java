package com.immomo.momo.android.activity.setting;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.protocol.a.w;
import java.util.ArrayList;
import java.util.List;

final class t extends d {

    /* renamed from: a  reason: collision with root package name */
    private List f2143a = new ArrayList();
    private int c = 0;
    private /* synthetic */ FeedBackListActivity d;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public t(FeedBackListActivity feedBackListActivity, Context context, boolean z) {
        super(context);
        this.d = feedBackListActivity;
        if (z) {
            this.c = 0;
        } else {
            this.c = feedBackListActivity.k.getCount();
        }
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        return Boolean.valueOf(w.a().e(this.f2143a, this.c));
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        Boolean bool = (Boolean) obj;
        super.a(bool);
        if (this.c == 0) {
            this.d.j.clear();
            this.d.k.b();
        }
        this.d.j.addAll(this.f2143a);
        this.d.k.notifyDataSetChanged();
        if (bool.booleanValue()) {
            this.d.l.setVisibility(0);
        } else {
            this.d.l.setVisibility(8);
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
        super.b();
        if (this.c != 0) {
            this.d.l.e();
        }
        this.d.i.n();
    }
}
