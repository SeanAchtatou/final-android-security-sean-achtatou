package com.immomo.momo.android.activity.tieba;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.b.a;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.a.hy;
import com.immomo.momo.android.activity.ImageBrowserActivity;
import com.immomo.momo.android.activity.OtherProfileActivity;
import com.immomo.momo.android.activity.ah;
import com.immomo.momo.android.activity.common.CommonShareActivity;
import com.immomo.momo.android.activity.emotestore.EmotionProfileActivity;
import com.immomo.momo.android.broadcast.x;
import com.immomo.momo.android.c.i;
import com.immomo.momo.android.view.AltImageView;
import com.immomo.momo.android.view.EmoteTextView;
import com.immomo.momo.android.view.HandyListView;
import com.immomo.momo.android.view.HandyTextView;
import com.immomo.momo.android.view.HeaderLayout;
import com.immomo.momo.android.view.MGifImageView;
import com.immomo.momo.android.view.MultiImageView;
import com.immomo.momo.android.view.a.ay;
import com.immomo.momo.android.view.a.bj;
import com.immomo.momo.android.view.a.o;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.android.view.au;
import com.immomo.momo.android.view.bi;
import com.immomo.momo.android.view.cb;
import com.immomo.momo.service.ao;
import com.immomo.momo.service.bean.aj;
import com.immomo.momo.service.bean.c.b;
import com.immomo.momo.service.bean.c.c;
import com.immomo.momo.service.bean.c.d;
import com.immomo.momo.service.bean.c.g;
import com.immomo.momo.service.bean.q;
import com.immomo.momo.util.j;
import com.immomo.momo.util.m;
import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import mm.purchasesdk.PurchaseCode;

public class TieDetailActivity extends ah implements View.OnClickListener, cb {
    private View A;
    private TextView B;
    private TextView C;
    private View D;
    private ImageView E;
    private View F;
    private EmoteTextView G;
    private View H;
    private HandyListView I;
    private ImageButton J;
    private ImageButton K;
    private Button L;
    /* access modifiers changed from: private */
    public Button M;
    /* access modifiers changed from: private */
    public hy N;
    /* access modifiers changed from: private */
    public String O;
    /* access modifiers changed from: private */
    public Lock P = new ReentrantLock();
    /* access modifiers changed from: private */
    public Condition Q = this.P.newCondition();
    /* access modifiers changed from: private */
    public au R;
    private String S = "是否分享[%s]这个话题给更多朋友？";
    /* access modifiers changed from: private */
    public String T = "陌陌吧话题：%s";
    private String[] U = {"评论", "分享给好友", "分享到社交网络", "编辑话题", "删除话题"};
    private String[] V = {"评论", "分享给好友", "分享到社交网络", "举报"};
    /* access modifiers changed from: private */
    public String[] W = {"垃圾广告", "色情信息", "敏感内容", "不实信息"};
    private m h = new m("话题详情");
    /* access modifiers changed from: private */
    public b i;
    private String j;
    private String k;
    private String l;
    /* access modifiers changed from: private */
    public String m;
    /* access modifiers changed from: private */
    public boolean n;
    /* access modifiers changed from: private */
    public ao o = new ao();
    /* access modifiers changed from: private */
    public cm p;
    /* access modifiers changed from: private */
    public cl q;
    /* access modifiers changed from: private */
    public boolean r;
    /* access modifiers changed from: private */
    public int s;
    /* access modifiers changed from: private */
    public HashMap t = new HashMap();
    /* access modifiers changed from: private */
    public int u;
    /* access modifiers changed from: private */
    public int v = 0;
    /* access modifiers changed from: private */
    public int w = 0;
    private HeaderLayout x;
    private bi y;
    private View z;

    static /* synthetic */ void B(TieDetailActivity tieDetailActivity) {
        bj bjVar = new bj(tieDetailActivity, tieDetailActivity.w, tieDetailActivity.u + 1);
        bjVar.a(tieDetailActivity.x, 80);
        bjVar.a(new cc(tieDetailActivity));
    }

    static /* synthetic */ void a(TieDetailActivity tieDetailActivity, AsyncTask asyncTask) {
        v vVar = new v(tieDetailActivity, asyncTask);
        vVar.a("正在加载...");
        tieDetailActivity.a(vVar);
    }

    static /* synthetic */ void a(TieDetailActivity tieDetailActivity, b bVar, int i2) {
        if (tieDetailActivity.f.h.equals(bVar.f)) {
            tieDetailActivity.b(new cp(tieDetailActivity, tieDetailActivity, i2, bVar, PoiTypeDef.All));
            return;
        }
        ay ayVar = new ay(tieDetailActivity);
        ayVar.a(new ca(tieDetailActivity, i2, bVar, ayVar));
        tieDetailActivity.a(ayVar);
    }

    static /* synthetic */ void a(TieDetailActivity tieDetailActivity, boolean z2) {
        tieDetailActivity.F.setVisibility(z2 ? 0 : 8);
    }

    /* access modifiers changed from: private */
    public void a(b bVar) {
        if (bVar.o == 4) {
            x();
        } else {
            d(bVar);
        }
    }

    /* access modifiers changed from: private */
    public void a(List list) {
        this.N.b();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            c cVar = (c) it.next();
            cVar.i = this.i;
            this.N.a(cVar);
        }
        this.N.notifyDataSetChanged();
    }

    /* access modifiers changed from: private */
    public void b(b bVar) {
        if (bVar.b != null) {
            this.l = bVar.b.b;
            this.x.setTitleText(String.valueOf(this.l) + "吧话题");
            this.B.setText(String.valueOf(this.l) + "吧  ");
            this.A.setVisibility(0);
            this.n = bVar.b.l;
            return;
        }
        d a2 = this.o.a(bVar.c);
        if (a2 == null) {
            this.l = "陌陌";
            this.x.setTitleText("陌陌吧话题");
            this.B.setText("陌陌吧  ");
            this.A.setVisibility(0);
            this.n = false;
            return;
        }
        bVar.b = a2;
        this.l = a2.b;
        this.x.setTitleText(String.valueOf(this.l) + "吧话题");
        this.B.setText(String.valueOf(this.l) + "吧  ");
        this.A.setVisibility(0);
        this.n = a2.l;
    }

    /* access modifiers changed from: private */
    public void c(int i2) {
        boolean z2 = true;
        int i3 = 0;
        this.J.setEnabled(i2 != 0);
        ImageButton imageButton = this.K;
        if (i2 == this.w - 1 || this.w == 0) {
            z2 = false;
        }
        imageButton.setEnabled(z2);
        if (this.u != i2) {
            this.u = i2;
            View view = this.H;
            if (i2 != 0) {
                i3 = 8;
            }
            view.setVisibility(i3);
            this.L.setText(String.valueOf(this.u + 1) + "/" + this.w);
            this.I.i();
        }
    }

    private void c(Bundle bundle) {
        if (bundle == null || !bundle.getBoolean("from_saveinstance", false)) {
            Intent intent = getIntent();
            this.j = intent.getStringExtra("key_tieid");
            this.k = intent.getStringExtra("key_tietitle");
        } else {
            this.j = (String) bundle.get("key_tieid");
            this.k = (String) bundle.get("key_tietitle");
        }
        d();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup, int, boolean, boolean, int):void
     arg types: [com.immomo.momo.service.bean.c.g, android.widget.ImageView, ?[OBJECT, ARRAY], int, int, int, int]
     candidates:
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, int, android.view.View, android.view.ViewGroup, boolean, int, boolean):com.immomo.momo.android.c.o
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup, int, boolean, boolean, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.b.a.a(java.util.Date, boolean):java.lang.String
     arg types: [java.util.Date, int]
     candidates:
      android.support.v4.b.a.a(android.graphics.Bitmap, float):android.graphics.Bitmap
      android.support.v4.b.a.a(android.graphics.Bitmap, java.io.File):android.graphics.Bitmap
      android.support.v4.b.a.a(int, int):java.lang.String
      android.support.v4.b.a.a(java.util.Collection, java.lang.String):java.lang.String
      android.support.v4.b.a.a(java.lang.Object[], java.lang.String):java.lang.String
      android.support.v4.b.a.a(java.lang.String, java.lang.String):org.json.JSONObject
      android.support.v4.b.a.a(float, int[]):void
      android.support.v4.b.a.a(android.app.Activity, int):void
      android.support.v4.b.a.a(android.view.View, java.lang.Runnable):void
      android.support.v4.b.a.a(java.io.File, java.io.File):void
      android.support.v4.b.a.a(java.io.File, java.util.zip.ZipInputStream):void
      android.support.v4.b.a.a(java.lang.Object, java.lang.StringBuilder):void
      android.support.v4.b.a.a(android.content.Context, java.lang.String):boolean
      android.support.v4.b.a.a(com.immomo.a.a.d.b, com.immomo.momo.service.bean.Message):boolean
      android.support.v4.b.a.a(byte[], java.lang.String):byte[]
      android.support.v4.b.a.a(byte[], byte[]):byte[]
      android.support.v4.b.a.a(int[], boolean):byte[]
      android.support.v4.b.a.a(byte[], boolean):int[]
      android.support.v4.b.a.a(int, float):void
      android.support.v4.b.a.a(java.lang.Object, com.immomo.a.a.f):void
      android.support.v4.view.bf.a(int, float):void
      com.immomo.a.a.f.a(java.lang.Object, com.immomo.a.a.f):void
      android.support.v4.b.a.a(java.util.Date, boolean):java.lang.String */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup, int, boolean):void
     arg types: [com.immomo.momo.service.bean.c.b, com.immomo.momo.android.view.AltImageView, ?[OBJECT, ARRAY], int, int]
     candidates:
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, int, boolean, boolean):void
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup, int, boolean):void */
    /* access modifiers changed from: private */
    public void c(b bVar) {
        if (this.u == 0) {
            this.H.setVisibility(0);
        } else {
            this.H.setVisibility(8);
        }
        this.G.setText(bVar.d);
        this.C.setText(new StringBuilder(String.valueOf(bVar.n)).toString());
        ImageView imageView = (ImageView) this.z.findViewById(R.id.iv_tie_icon_elite);
        if (bVar.p) {
            imageView.setVisibility(0);
        } else {
            imageView.setVisibility(8);
        }
        ImageView imageView2 = (ImageView) this.z.findViewById(R.id.iv_tie_icon_top);
        if (bVar.m) {
            imageView2.setVisibility(0);
        } else {
            imageView2.setVisibility(8);
        }
        ImageView imageView3 = (ImageView) this.z.findViewById(R.id.iv_tie_icon_hot);
        if (bVar.q) {
            imageView3.setVisibility(0);
        } else {
            imageView3.setVisibility(8);
        }
        ImageView imageView4 = (ImageView) this.z.findViewById(R.id.iv_tie_icon_new);
        if (bVar.l) {
            imageView4.setVisibility(0);
        } else {
            imageView4.setVisibility(8);
        }
        ImageView imageView5 = (ImageView) this.z.findViewById(R.id.iv_tie_icon_recommend);
        if (bVar.k) {
            imageView5.setVisibility(0);
        } else {
            imageView5.setVisibility(8);
        }
        ImageView imageView6 = (ImageView) this.H.findViewById(R.id.iv_tie_userheader);
        EmoteTextView emoteTextView = (EmoteTextView) this.H.findViewById(R.id.tv_tie_username);
        TextView textView = (TextView) this.H.findViewById(R.id.tv_tie_floor);
        TextView textView2 = (TextView) this.H.findViewById(R.id.tv_tie_time);
        View findViewById = this.H.findViewById(R.id.userlist_item_layout_genderbackgroud);
        ImageView imageView7 = (ImageView) this.H.findViewById(R.id.userlist_item_iv_gender);
        HandyTextView handyTextView = (HandyTextView) this.H.findViewById(R.id.userlist_item_tv_age);
        ImageView imageView8 = (ImageView) this.H.findViewById(R.id.userlist_item_pic_iv_weibo);
        EmoteTextView emoteTextView2 = (EmoteTextView) this.H.findViewById(R.id.tv_tie_content);
        AltImageView altImageView = (AltImageView) this.H.findViewById(R.id.iv_tie_content);
        MultiImageView multiImageView = (MultiImageView) this.H.findViewById(R.id.mv_tie_content);
        MGifImageView mGifImageView = (MGifImageView) this.H.findViewById(R.id.gv_tie_content);
        View findViewById2 = this.H.findViewById(R.id.layout_tie_distance);
        TextView textView3 = (TextView) this.H.findViewById(R.id.tv_tie_distance);
        View findViewById3 = this.H.findViewById(R.id.layout_tie_usersinfo);
        altImageView.setOnClickListener(this);
        mGifImageView.setOnClickListener(this);
        g gVar = bVar.e;
        imageView6.setOnClickListener(new cj(this, bVar));
        findViewById3.setOnClickListener(new ck(this, bVar));
        j.a((aj) this.i.e, imageView6, (ViewGroup) null, 3, false, true, com.immomo.momo.g.a(8.0f));
        emoteTextView.setText(gVar.h());
        if (gVar.b()) {
            emoteTextView.setTextColor(com.immomo.momo.g.c((int) R.color.font_vip_name));
        } else {
            emoteTextView.setTextColor(com.immomo.momo.g.c((int) R.color.text_color));
        }
        textView.setText("楼主");
        textView.setTextColor(com.immomo.momo.g.c((int) R.color.blue));
        textView2.setText(a.a(bVar.i, false));
        if ("F".equals(gVar.H)) {
            findViewById.setBackgroundResource(R.drawable.bg_gender_famal);
            imageView7.setImageResource(R.drawable.ic_user_famale);
        } else {
            findViewById.setBackgroundResource(R.drawable.bg_gender_male);
            imageView7.setImageResource(R.drawable.ic_user_male);
        }
        handyTextView.setText(new StringBuilder(String.valueOf(gVar.I)).toString());
        if (gVar.an) {
            imageView8.setVisibility(0);
            imageView8.setImageResource(gVar.ap ? R.drawable.ic_userinfo_weibov : R.drawable.ic_userinfo_weibo);
        } else if (gVar.at) {
            imageView8.setVisibility(0);
            imageView8.setImageResource(gVar.au ? R.drawable.ic_userinfo_tweibov : R.drawable.ic_userinfo_tweibo);
        } else if (gVar.ar) {
            imageView8.setVisibility(0);
        } else {
            imageView8.setVisibility(8);
        }
        if (a.a((CharSequence) this.i.g)) {
            emoteTextView2.setVisibility(8);
        } else {
            emoteTextView2.setVisibility(0);
            emoteTextView2.setText(this.i.g);
        }
        altImageView.setVisibility(8);
        mGifImageView.setVisibility(8);
        multiImageView.setVisibility(8);
        if (a.f(bVar.u) && a.f(bVar.t)) {
            mGifImageView.setVisibility(0);
            mGifImageView.setAlt(bVar.t);
            boolean endsWith = bVar.t.endsWith(".gif");
            if (this.R == null) {
                File a2 = q.a(bVar.t, bVar.u);
                this.h.b((Object) ("emoteFile:" + (a2 == null ? "null" : a2.getAbsolutePath())));
                if (a2 == null || !a2.exists()) {
                    new i(bVar.t, bVar.u, new bw(this, mGifImageView, endsWith)).a();
                } else {
                    this.R = new au(endsWith ? 1 : 2);
                    this.R.a(a2, mGifImageView);
                    this.R.b(20);
                    mGifImageView.setGifDecoder(this.R);
                }
            } else {
                mGifImageView.setGifDecoder(this.R);
                if ((this.R.g() == 4 || this.R.g() == 2) && this.R.f() != null && this.R.f().exists()) {
                    this.R.a(this.R.f(), mGifImageView);
                }
            }
        } else if (bVar.b() > 1) {
            multiImageView.setVisibility(0);
            multiImageView.setImage(bVar.c());
            multiImageView.setOnclickHandler(this);
        } else if (a.f(bVar.getLoadImageId())) {
            altImageView.setVisibility(0);
            j.a((aj) bVar, (ImageView) altImageView, (ViewGroup) null, 15, false);
        }
        if (a.a((CharSequence) bVar.h)) {
            findViewById2.setVisibility(8);
            return;
        }
        findViewById2.setVisibility(0);
        textView3.setText(bVar.h);
    }

    /* access modifiers changed from: private */
    public void d(int i2) {
        if (this.t.containsKey(Integer.valueOf(i2))) {
            ((List) this.t.get(Integer.valueOf(i2))).clear();
            this.t.remove(Integer.valueOf(i2));
        }
    }

    static /* synthetic */ void d(TieDetailActivity tieDetailActivity, int i2) {
        if (tieDetailActivity.t.containsKey(Integer.valueOf(i2))) {
            tieDetailActivity.c(i2);
            tieDetailActivity.a((List) tieDetailActivity.t.get(Integer.valueOf(i2)));
            return;
        }
        tieDetailActivity.b(new cl(tieDetailActivity, tieDetailActivity, tieDetailActivity.j, i2));
    }

    static /* synthetic */ void d(TieDetailActivity tieDetailActivity, b bVar) {
        boolean z2 = true;
        if (bVar.n > 0) {
            tieDetailActivity.w = bVar.n / 20;
            if (bVar.n % 20 != 0) {
                tieDetailActivity.w++;
            }
            Button button = tieDetailActivity.L;
            if (tieDetailActivity.w == 1) {
                z2 = false;
            }
            button.setEnabled(z2);
            tieDetailActivity.L.setText(String.valueOf(tieDetailActivity.u + 1) + "/" + tieDetailActivity.w);
        } else {
            tieDetailActivity.w = 0;
            tieDetailActivity.L.setEnabled(false);
        }
        if (tieDetailActivity.w < 2) {
            tieDetailActivity.K.setEnabled(false);
            tieDetailActivity.J.setEnabled(false);
        }
    }

    /* access modifiers changed from: private */
    public void d(b bVar) {
        this.M.setEnabled(true);
        int i2 = bVar.n;
        this.M.setText("评论");
    }

    static /* synthetic */ boolean d(TieDetailActivity tieDetailActivity) {
        if (tieDetailActivity.s != 17) {
            return false;
        }
        tieDetailActivity.r = false;
        tieDetailActivity.y();
        return true;
    }

    static /* synthetic */ void f(TieDetailActivity tieDetailActivity, b bVar) {
        Intent intent = new Intent(x.b);
        intent.putExtra("key_tiebaid", bVar.c);
        intent.putExtra("key_pid", bVar.f3017a);
        tieDetailActivity.sendBroadcast(intent);
    }

    static /* synthetic */ void h(TieDetailActivity tieDetailActivity, b bVar) {
        String[] strArr;
        if (bVar != null) {
            if (tieDetailActivity.m.equals(bVar.f)) {
                if (tieDetailActivity.n) {
                    String[] strArr2 = new String[8];
                    strArr2[0] = "评论";
                    strArr2[1] = "分享给好友";
                    strArr2[2] = "分享到社交网络";
                    strArr2[3] = tieDetailActivity.i.m ? "取消置顶" : "置顶话题";
                    strArr2[4] = tieDetailActivity.i.p ? "取消精华" : "设为精华";
                    strArr2[5] = tieDetailActivity.i.o == 4 ? "取消锁定" : "锁定话题";
                    strArr2[6] = "编辑话题";
                    strArr2[7] = "删除话题";
                    strArr = strArr2;
                } else {
                    if (tieDetailActivity.U == null) {
                        tieDetailActivity.U = new String[]{"评论", "分享给好友", "分享到社交网络", "编辑话题", "删除话题"};
                    }
                    strArr = tieDetailActivity.U;
                }
            } else if (tieDetailActivity.n) {
                String[] strArr3 = new String[8];
                strArr3[0] = "评论";
                strArr3[1] = "分享给好友";
                strArr3[2] = "分享到社交网络";
                strArr3[3] = tieDetailActivity.i.m ? "取消置顶" : "置顶话题";
                strArr3[4] = tieDetailActivity.i.p ? "取消精华" : "设为精华";
                strArr3[5] = tieDetailActivity.i.o == 4 ? "取消锁定" : "锁定话题";
                strArr3[6] = "删除话题";
                strArr3[7] = "删除并禁言";
                strArr = strArr3;
            } else {
                if (tieDetailActivity.V == null) {
                    tieDetailActivity.V = new String[]{"评论", "分享给好友", "分享到社交网络", "举报"};
                }
                strArr = tieDetailActivity.V;
            }
            o oVar = new o(tieDetailActivity, strArr);
            oVar.a(new bz(tieDetailActivity, strArr, bVar));
            oVar.b();
            oVar.show();
        }
    }

    static /* synthetic */ void i(TieDetailActivity tieDetailActivity, b bVar) {
        Intent intent = new Intent(tieDetailActivity, TiebaProfileActivity.class);
        intent.putExtra("tiebaid", bVar.c);
        intent.setFlags(603979776);
        tieDetailActivity.startActivity(intent);
    }

    static /* synthetic */ void j(TieDetailActivity tieDetailActivity, b bVar) {
        Intent intent = new Intent(tieDetailActivity, OtherProfileActivity.class);
        intent.putExtra("momoid", bVar.f);
        tieDetailActivity.startActivity(intent);
    }

    static /* synthetic */ void k(TieDetailActivity tieDetailActivity, b bVar) {
        Intent intent = new Intent(tieDetailActivity, CommonShareActivity.class);
        intent.putExtra("from_type", 1);
        intent.putExtra("from_id", bVar.f3017a);
        tieDetailActivity.startActivity(intent);
    }

    static /* synthetic */ void l(TieDetailActivity tieDetailActivity, b bVar) {
        com.immomo.momo.android.view.a.ao aoVar = new com.immomo.momo.android.view.a.ao(tieDetailActivity, tieDetailActivity.f);
        aoVar.setTitle("分享");
        aoVar.a(String.format(tieDetailActivity.S, bVar.d), 0);
        aoVar.a(new by(tieDetailActivity, aoVar, bVar));
        tieDetailActivity.a(aoVar);
    }

    static /* synthetic */ void m(TieDetailActivity tieDetailActivity, b bVar) {
        o oVar = new o(tieDetailActivity, tieDetailActivity.W);
        oVar.a(new cb(tieDetailActivity, bVar));
        oVar.setTitle("举报话题");
        oVar.show();
    }

    static /* synthetic */ void n(TieDetailActivity tieDetailActivity, b bVar) {
        Intent intent = new Intent(tieDetailActivity, EditTieActivity.class);
        intent.putExtra("post_id", tieDetailActivity.i.f3017a);
        if (bVar.b != null) {
            intent.putExtra("tieba_name", bVar.b.b);
        }
        tieDetailActivity.startActivityForResult(intent, PurchaseCode.UNSUB_OK);
    }

    private void w() {
        this.M = (Button) findViewById(R.id.button_publish_tie_comment);
        this.M.setOnClickListener(new cd(this));
        this.L = (Button) findViewById(R.id.button_page);
        this.J = (ImageButton) findViewById(R.id.button_prev);
        this.K = (ImageButton) findViewById(R.id.button_next);
        this.L.setEnabled(false);
        this.J.setEnabled(false);
        this.K.setEnabled(false);
    }

    /* access modifiers changed from: private */
    public void x() {
        this.M.setEnabled(false);
        this.M.setText("已被锁定");
    }

    /* access modifiers changed from: private */
    public void y() {
        this.E.clearAnimation();
        this.D.setVisibility(8);
    }

    static /* synthetic */ void y(TieDetailActivity tieDetailActivity) {
        if (tieDetailActivity.i != null && tieDetailActivity.i.o != 2) {
            Intent intent = new Intent(tieDetailActivity, PublishTieCommentActivity.class);
            intent.putExtra("tiezi_id", tieDetailActivity.i.f3017a);
            intent.putExtra("tiezi_name", tieDetailActivity.i.d);
            intent.putExtra("tiecomment_tomomoid", tieDetailActivity.i.f);
            tieDetailActivity.startActivityForResult(intent, PurchaseCode.QUERY_OK);
        }
    }

    public final void a(Intent intent, int i2, Bundle bundle, String str) {
        if (intent.getComponent() != null && com.immomo.momo.android.activity.d.g(intent.getComponent().getClassName())) {
            intent.putExtra("afromname", (this.i == null || this.i.b == null) ? PoiTypeDef.All : this.i.b.b);
        }
        super.a(intent, i2, bundle, str);
    }

    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        super.a(bundle);
        setContentView((int) R.layout.activity_tie_detail);
        this.j = getIntent().getStringExtra("key_tieid");
        this.k = getIntent().getStringExtra("key_tietitle");
        this.u = 0;
        this.x = (HeaderLayout) findViewById(R.id.layout_header);
        this.y = new bi(this).a((int) R.drawable.ic_topbar_more);
        m().a(this.y, new bv(this));
        this.z = com.immomo.momo.g.o().inflate((int) R.layout.include_tieba_tie_detail_header, (ViewGroup) null);
        this.B = (TextView) this.z.findViewById(R.id.tv_to_tieba);
        this.C = (TextView) this.z.findViewById(R.id.tv_tie_commentcount);
        this.A = this.z.findViewById(R.id.layout_to_tieba);
        this.A.setVisibility(8);
        this.D = this.z.findViewById(R.id.layout_tie_load);
        this.E = (ImageView) this.z.findViewById(R.id.iv_tie_load);
        this.G = (EmoteTextView) this.z.findViewById(R.id.tv_tie_title);
        this.G.setText(this.k);
        this.H = this.z.findViewById(R.id.layout_tie_content);
        this.H.setVisibility(8);
        this.F = this.z.findViewById(R.id.tv_tie_no_comment);
        this.F.setVisibility(8);
        w();
        this.I = (HandyListView) findViewById(R.id.listview);
        this.I.addHeaderView(this.z);
        this.N = new hy(this, this.I);
        this.I.setAdapter((ListAdapter) this.N);
        w();
        this.B.setOnClickListener(new ce(this));
        this.H.setOnClickListener(new cf(this));
        this.J.setOnClickListener(new cg(this));
        this.K.setOnClickListener(new ch(this));
        this.L.setOnClickListener(new ci(this));
        c(bundle);
    }

    public final void b(int i2, String[] strArr) {
        Intent intent = new Intent(this, ImageBrowserActivity.class);
        intent.putExtra("array", strArr);
        intent.putExtra("imagetype", "feed");
        intent.putExtra("index", i2);
        startActivity(intent);
        if (getParent() != null) {
            getParent().overridePendingTransition(R.anim.zoom_enter, R.anim.normal);
        } else {
            overridePendingTransition(R.anim.zoom_enter, R.anim.normal);
        }
    }

    /* access modifiers changed from: protected */
    public final void d() {
        this.r = true;
        this.m = (com.immomo.momo.g.q() == null || a.a(com.immomo.momo.g.q().h)) ? PoiTypeDef.All : com.immomo.momo.g.q().h;
        this.i = this.o.c(this.j);
        if (this.i != null) {
            if (this.i.o == 2) {
                com.immomo.momo.util.ao.b("该话题已经被删除");
                finish();
                return;
            }
            b(this.i);
            a(this.i);
            c(this.i);
            this.N.a(this.m, this.n);
        }
        this.r = true;
        this.s = 0;
        this.D.setVisibility(0);
        if (this.E.getDrawable() != null) {
            this.E.startAnimation(AnimationUtils.loadAnimation(this, R.anim.loading));
        }
        b(new cm(this, this, this.j));
        b(new cl(this, this, this.j, this.u));
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        if (i3 == -1) {
            switch (i2) {
                case PurchaseCode.QUERY_OK /*101*/:
                case PurchaseCode.ORDER_OK /*102*/:
                    this.h.b((Object) "评论发表成功，加载话题最新内容和最后一页");
                    b(new cm(this, this, this.j));
                    if (this.w == 0 || this.u == this.w - 1) {
                        b(new cl(this, this, this.j, this.u));
                        return;
                    } else {
                        d(this.w - 1);
                        return;
                    }
                case PurchaseCode.UNSUB_OK /*103*/:
                    b(new cm(this, this, this.j));
                    return;
                default:
                    throw new RuntimeException("no this request code : " + i2);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void onClick(View view) {
        if (this.i != null) {
            String str = this.i.u;
            if (a.f(str)) {
                Intent intent = new Intent(this, EmotionProfileActivity.class);
                intent.putExtra("eid", str);
                startActivity(intent);
                return;
            }
            Intent intent2 = new Intent(this, ImageBrowserActivity.class);
            intent2.putExtra("array", new String[]{this.i.getLoadImageId()});
            intent2.putExtra("imagetype", "feed");
            intent2.putExtra("autohide_header", true);
            startActivity(intent2);
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.p != null && !this.p.isCancelled()) {
            this.p.cancel(true);
        }
        if (this.q != null && !this.q.isCancelled()) {
            this.q.cancel(true);
        }
        this.t.clear();
        if (this.R != null) {
            this.R.l();
        }
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        String str = intent.getExtras() != null ? (String) intent.getExtras().get("key_tieid") : null;
        if (!a.a((CharSequence) str) && !this.j.equals(str)) {
            Bundle bundle = new Bundle();
            bundle.putBoolean("from_saveinstance", true);
            bundle.putString("key_tieid", str);
            c(bundle);
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        bundle.putBoolean("from_saveinstance", true);
        bundle.putString("key_tieid", this.j);
        bundle.putString("key_tietitle", this.k);
        super.onSaveInstanceState(bundle);
    }

    public final void u() {
        b(new cm(this, this, this.j));
        b(new cl(this, this, this.j, this.u));
    }

    public final void v() {
        b(new cm(this, this, this.j));
        b(new cl(this, this, this.j, this.u));
    }
}
