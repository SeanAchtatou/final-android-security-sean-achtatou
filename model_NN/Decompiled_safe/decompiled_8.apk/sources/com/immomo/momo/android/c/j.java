package com.immomo.momo.android.c;

public final class j extends w {
    private String f;

    public j(String str) {
        this.f = str;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0083, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0084, code lost:
        if (r7 != null) goto L_0x0086;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0086, code lost:
        r7.delete();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0089, code lost:
        throw r0;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0083 A[ExcHandler: all (r0v4 'th' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:3:0x0007] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(long r10, long r12, int r14, java.net.HttpURLConnection r15) {
        /*
            r9 = this;
            r1 = 2
            r8 = 1
            r0 = 4
            if (r14 != r0) goto L_0x008a
            java.io.File r7 = r9.c
            long r0 = r7.length()     // Catch:{ Exception -> 0x0071, all -> 0x0083 }
            com.immomo.momo.util.ar.h(r0)     // Catch:{ Exception -> 0x0071, all -> 0x0083 }
            java.io.File r0 = new java.io.File     // Catch:{ Exception -> 0x0071, all -> 0x0083 }
            java.io.File r1 = com.immomo.momo.a.v()     // Catch:{ Exception -> 0x0071, all -> 0x0083 }
            java.lang.String r2 = r9.f     // Catch:{ Exception -> 0x0071, all -> 0x0083 }
            r0.<init>(r1, r2)     // Catch:{ Exception -> 0x0071, all -> 0x0083 }
            android.support.v4.b.a.a(r7, r0)     // Catch:{ Exception -> 0x0071, all -> 0x0083 }
            if (r7 == 0) goto L_0x0021
            r7.delete()
        L_0x0021:
            com.immomo.momo.service.bean.bf r0 = com.immomo.momo.g.q()
            if (r0 == 0) goto L_0x0058
            com.immomo.momo.service.o r1 = new com.immomo.momo.service.o
            r1.<init>()
            java.lang.String r0 = r9.f
            com.immomo.momo.service.bean.q r0 = r1.a(r0)
            if (r0 != 0) goto L_0x003a
            java.lang.String r0 = r9.f
            com.immomo.momo.service.bean.q r0 = r1.b(r0)
        L_0x003a:
            if (r0 == 0) goto L_0x0058
            r0.t = r8
            r0.u = r8
            r1.a(r0)
            android.content.Intent r0 = new android.content.Intent
            java.lang.String r1 = com.immomo.momo.android.broadcast.o.f2358a
            r0.<init>(r1)
            java.lang.String r1 = "event"
            java.lang.String r2 = "enable"
            r0.putExtra(r1, r2)
            android.content.Context r1 = com.immomo.momo.g.c()
            r1.sendBroadcast(r0)
        L_0x0058:
            android.content.Intent r0 = new android.content.Intent
            java.lang.String r1 = com.immomo.momo.android.broadcast.g.f2350a
            r0.<init>(r1)
            java.lang.String r1 = "eid"
            java.lang.String r2 = r9.f
            r0.putExtra(r1, r2)
            android.content.Context r1 = com.immomo.momo.g.c()
            r1.sendBroadcast(r0)
        L_0x006d:
            super.a(r10, r12, r14, r15)
            return
        L_0x0071:
            r0 = move-exception
            r7.delete()     // Catch:{ Exception -> 0x00ab, all -> 0x0083 }
            r5 = 2
            r0 = r9
            r1 = r10
            r3 = r12
            r6 = r15
            r0.a(r1, r3, r5, r6)     // Catch:{ Exception -> 0x00ab, all -> 0x0083 }
        L_0x007d:
            if (r7 == 0) goto L_0x0021
            r7.delete()
            goto L_0x0021
        L_0x0083:
            r0 = move-exception
            if (r7 == 0) goto L_0x0089
            r7.delete()
        L_0x0089:
            throw r0
        L_0x008a:
            if (r14 != r1) goto L_0x006d
            android.content.Intent r0 = new android.content.Intent
            java.lang.String r1 = com.immomo.momo.android.broadcast.g.c
            r0.<init>(r1)
            java.lang.String r1 = "eid"
            java.lang.String r2 = r9.f
            r0.putExtra(r1, r2)
            android.content.Context r1 = com.immomo.momo.g.c()
            r1.sendBroadcast(r0)
            java.io.File r0 = r9.c
            if (r0 == 0) goto L_0x006d
            java.io.File r0 = r9.c
            r0.delete()
            goto L_0x006d
        L_0x00ab:
            r0 = move-exception
            goto L_0x007d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.immomo.momo.android.c.j.a(long, long, int, java.net.HttpURLConnection):void");
    }
}
