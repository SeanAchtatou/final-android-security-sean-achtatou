package com.immomo.momo.android.broadcast;

import android.content.Context;
import com.immomo.momo.g;

public final class q extends b {

    /* renamed from: a  reason: collision with root package name */
    public static final String f2360a = (String.valueOf(g.h()) + ".action.draft.success");
    public static final String b = (String.valueOf(g.h()) + ".action.draft.failed");
    public static final String c = (String.valueOf(g.h()) + ".action.draft.begin");

    public q(Context context) {
        super(context);
        a(f2360a, b, c);
    }
}
