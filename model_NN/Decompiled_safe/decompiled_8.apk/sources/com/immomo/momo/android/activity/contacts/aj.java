package com.immomo.momo.android.activity.contacts;

import android.content.Context;
import com.immomo.momo.R;
import com.immomo.momo.a.a;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.w;
import com.immomo.momo.service.bean.g;
import com.immomo.momo.service.bean.h;
import com.immomo.momo.service.c;
import com.immomo.momo.util.u;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

final class aj extends d {

    /* renamed from: a  reason: collision with root package name */
    private v f1143a = null;
    /* access modifiers changed from: private */
    public /* synthetic */ ContactPeopleActivity c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public aj(ContactPeopleActivity contactPeopleActivity, Context context) {
        super(context);
        this.c = contactPeopleActivity;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        c unused = this.c.l;
        c.d();
        Map a2 = this.c.l.a(this.c.m);
        if (a2.keySet().size() <= 0) {
            throw new a(this.c.getString(R.string.contact_readfailedtip));
        }
        List<g> a3 = w.a().a(a2.keySet(), 2);
        h hVar = new h();
        h hVar2 = new h();
        h hVar3 = new h();
        for (g gVar : a3) {
            gVar.e = (String) a2.get(gVar.d);
            if (android.support.v4.b.a.a((CharSequence) gVar.e)) {
                this.b.a((Object) (String.valueOf(gVar.d) + "--------------" + c.c().b(gVar.d) + "---------" + c.c().a(gVar.d)));
            } else {
                switch (gVar.b) {
                    case 1:
                        hVar.b.add(gVar);
                        continue;
                    case 2:
                        hVar2.b.add(gVar);
                        continue;
                    case 3:
                        hVar3.b.add(gVar);
                        continue;
                }
            }
        }
        hVar.f3026a = String.valueOf(hVar.b.size()) + this.c.getString(R.string.contact_grouptitle1);
        hVar2.f3026a = String.valueOf(hVar2.b.size()) + this.c.getString(R.string.contact_grouptitle2);
        hVar3.f3026a = String.valueOf(hVar3.b.size()) + this.c.getString(R.string.contact_grouptitle3);
        Collections.sort(hVar.b, this.c.h);
        Collections.sort(hVar2.b, this.c.h);
        Collections.sort(hVar3.b, this.c.h);
        ArrayList arrayList = new ArrayList();
        arrayList.add(hVar);
        arrayList.add(hVar2);
        arrayList.add(hVar3);
        u.a("contactpeopleCache", arrayList);
        this.c.k = arrayList;
        if (a2.size() <= 0) {
            return null;
        }
        this.c.l.a(a2.keySet());
        return null;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
        this.f1143a = new v(this.c);
        this.f1143a.a("请求提交中...");
        this.f1143a.setCancelable(true);
        this.f1143a.setOnCancelListener(new ak(this));
        this.c.a(this.f1143a);
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        super.a(exc);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        this.c.u();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        super.b();
        this.c.p();
    }
}
