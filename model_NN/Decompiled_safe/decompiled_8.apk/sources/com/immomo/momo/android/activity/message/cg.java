package com.immomo.momo.android.activity.message;

import android.os.Bundle;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.Message;

final class cg implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ Message f1967a;

    cg(Message message) {
        this.f1967a = message;
    }

    public final void run() {
        Bundle bundle = new Bundle();
        bundle.putString("stype", "msgfailed");
        bundle.putString("msgid", this.f1967a.msgId);
        if (this.f1967a.chatType == 2) {
            bundle.putInt("chattype", this.f1967a.chatType);
            bundle.putString("groupid", this.f1967a.groupId);
        } else if (this.f1967a.chatType == 1) {
            bundle.putString("remoteuserid", this.f1967a.remoteId);
        } else if (this.f1967a.chatType == 3) {
            bundle.putInt("chattype", this.f1967a.chatType);
            bundle.putString("discussid", this.f1967a.discussId);
        }
        g.d().a(bundle, "actions.message.status");
    }
}
