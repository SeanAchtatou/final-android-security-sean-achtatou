package com.immomo.momo.android.activity.account;

import android.content.Context;
import com.immomo.momo.R;
import com.immomo.momo.a.a;
import com.immomo.momo.a.x;
import com.immomo.momo.android.c.b;
import com.immomo.momo.protocol.a.w;
import org.json.JSONException;

final class ay extends b {
    /* access modifiers changed from: private */
    public /* synthetic */ SecurityCheckActivity c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ay(SecurityCheckActivity securityCheckActivity, Context context) {
        super(context);
        this.c = securityCheckActivity;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        return w.a().i(this.c.m);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
        this.f2379a.setCanceledOnTouchOutside(false);
        this.f2379a.setOnCancelListener(new az(this));
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        this.b.a((Throwable) exc);
        if (exc instanceof a) {
            a(exc.getMessage());
        } else if (exc instanceof JSONException) {
            a((int) R.string.errormsg_dataerror);
        } else if (exc instanceof x) {
            a((int) R.string.errormsg_dataerror);
        } else {
            a((int) R.string.errormsg_server);
        }
        this.c.finish();
    }

    /* access modifiers changed from: protected */
    public final void a(Object obj) {
        this.c.l = ((String[]) obj)[0];
        this.c.k = ((String[]) obj)[1];
        SecurityCheckActivity.b(this.c);
        this.c.f();
    }

    /* access modifiers changed from: protected */
    public final String c() {
        return "正在读取，请稍候";
    }
}
