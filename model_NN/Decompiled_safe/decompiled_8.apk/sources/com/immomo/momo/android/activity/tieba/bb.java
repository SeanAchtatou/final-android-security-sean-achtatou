package com.immomo.momo.android.activity.tieba;

import android.view.View;
import android.widget.ImageView;
import com.immomo.momo.R;

final class bb {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ PublishTieActivity f2178a;
    private View b;
    private ImageView c;
    private int d;
    private int e;
    /* access modifiers changed from: private */
    public boolean f = false;
    /* access modifiers changed from: private */
    public boolean g = false;
    private int h = -1;
    /* access modifiers changed from: private */
    public int i;

    public bb(PublishTieActivity publishTieActivity, View view, int i2) {
        boolean z = true;
        this.f2178a = publishTieActivity;
        this.b = view;
        this.c = (ImageView) this.b.findViewById(R.id.signeditor_iv_icon);
        this.h = i2;
        switch (i2) {
            case 1:
                this.i = 11;
                this.g = publishTieActivity.f.q();
                this.d = R.drawable.ic_publish_weibo_normal;
                this.e = R.drawable.ic_publish_weibo_selected;
                this.f = (!((Boolean) publishTieActivity.g.b("publishtie_sync_weibo", false)).booleanValue() || !this.g) ? false : z;
                break;
            case 2:
                this.i = 13;
                this.g = publishTieActivity.f.at;
                this.d = R.drawable.ic_publish_tweibo_normal;
                this.e = R.drawable.ic_publish_tweibo_selected;
                this.f = (!((Boolean) publishTieActivity.g.b("publishtie_sync_tx", false)).booleanValue() || !this.g) ? false : z;
                break;
            case 3:
                this.i = 12;
                this.g = publishTieActivity.f.ar;
                this.d = R.drawable.ic_publish_renren_normal;
                this.e = R.drawable.ic_publish_renren_selected;
                this.f = ((Boolean) publishTieActivity.g.b("publishtie_sync_renren", false)).booleanValue() && this.g;
                break;
            case 5:
                this.d = R.drawable.ic_publish_feed_normal;
                this.e = R.drawable.ic_publish_feed_selected;
                this.f = ((Boolean) publishTieActivity.g.b("publishtie_sync_feed", false)).booleanValue();
                break;
            case 6:
                this.d = R.drawable.ic_publish_weixin_normal;
                this.e = R.drawable.ic_publish_weixin_selected;
                this.f = ((Boolean) publishTieActivity.g.b("publishtie_sync_weinxin", true)).booleanValue();
                break;
        }
        a(this.f);
        this.b.setOnClickListener(new bc(this, i2));
    }

    public final void a(boolean z) {
        this.f = z;
        if (z) {
            this.c.setImageResource(this.e);
        } else {
            this.c.setImageResource(this.d);
        }
    }

    public final boolean a() {
        return this.f;
    }

    public final void b() {
        if (this.g) {
            switch (this.h) {
                case 1:
                    this.f2178a.g.c("publishtie_sync_weibo", Boolean.valueOf(this.f));
                    break;
                case 2:
                    this.f2178a.g.c("publishtie_sync_tx", Boolean.valueOf(this.f));
                    break;
                case 3:
                    this.f2178a.g.c("publishtie_sync_renren", Boolean.valueOf(this.f));
                    break;
            }
        }
        if (this.h == 5) {
            this.f2178a.g.c("publishtie_sync_feed", Boolean.valueOf(this.f));
        } else if (this.h == 6) {
            this.f2178a.g.c("publishtie_sync_weinxin", Boolean.valueOf(this.f));
        }
    }
}
