package com.immomo.momo.android.activity.account;

import android.content.Context;
import com.immomo.momo.R;
import com.immomo.momo.a.g;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.w;
import com.immomo.momo.util.ao;

final class f extends d {

    /* renamed from: a  reason: collision with root package name */
    private v f1000a = null;
    private String c = null;
    private /* synthetic */ LoginActivity d;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public f(LoginActivity loginActivity, Context context, String str) {
        super(context);
        this.d = loginActivity;
        this.c = str;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        this.b.a((Object) ("校验：" + this.c));
        return w.a().c(this.c, this.d.t);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.f1000a = new v(this.d, "请稍候，正在校验....");
        this.f1000a.setOnCancelListener(new g(this));
        this.f1000a.show();
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        if (exc instanceof g) {
            this.d.b(new j(this.d, this.d));
            this.d.v.setImageBitmap(null);
            ao.d(R.string.reg_scode_timeout);
            return;
        }
        super.a(exc);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        String str = (String) obj;
        if (this.d.x != null && this.d.x.isShowing()) {
            this.d.x.dismiss();
        }
        this.d.h = str;
        this.d.i();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.f1000a.dismiss();
        this.f1000a = null;
    }
}
