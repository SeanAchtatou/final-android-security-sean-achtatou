package com.immomo.momo.android.a;

import android.content.DialogInterface;
import android.support.v4.b.a;
import android.widget.EditText;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.service.bean.a.j;
import com.immomo.momo.util.ao;

final class fa implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ es f812a;
    private final /* synthetic */ EditText b;
    private final /* synthetic */ j c;

    fa(es esVar, EditText editText, j jVar) {
        this.f812a = esVar;
        this.b = editText;
        this.c = jVar;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        String editable = this.b.getText().toString();
        if (editable.length() < 4 || editable.length() > 16) {
            dialogInterface.dismiss();
            this.b.setText(PoiTypeDef.All);
            ao.a((CharSequence) "密码无效");
            return;
        }
        dialogInterface.dismiss();
        this.f812a.d.b(new fl(this.f812a, this.f812a.d, this.c, this.f812a.c.b, a.n(editable)));
    }
}
