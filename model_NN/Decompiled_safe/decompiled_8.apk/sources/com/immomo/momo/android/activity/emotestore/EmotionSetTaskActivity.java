package com.immomo.momo.android.activity.emotestore;

import android.os.Bundle;
import android.widget.ListView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.ah;
import org.json.JSONArray;

public class EmotionSetTaskActivity extends ah {
    String h = PoiTypeDef.All;
    ListView i = null;
    JSONArray j = null;

    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        super.a(bundle);
        setContentView((int) R.layout.activity_emotestore_settask);
        this.i = (ListView) findViewById(R.id.listview);
        setTitle("任务表情");
        this.h = getIntent().getStringExtra("eid");
        b(new ad(this, this));
        this.i.setOnItemClickListener(new ab(this));
    }
}
