package com.immomo.momo.android.activity;

import android.os.AsyncTask;
import com.immomo.momo.R;
import com.immomo.momo.a.a;
import com.immomo.momo.a.w;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.d;

final class ki extends AsyncTask {

    /* renamed from: a  reason: collision with root package name */
    private v f1787a;
    private /* synthetic */ ShareGroupPageActivity b;

    public ki(ShareGroupPageActivity shareGroupPageActivity) {
        this.b = shareGroupPageActivity;
        this.f1787a = new v(shareGroupPageActivity);
        this.f1787a.a("请求提交中");
        this.f1787a.setCancelable(true);
    }

    private String a() {
        try {
            d.a(this.b.f);
            return "yes";
        } catch (w e) {
            this.b.e.a((Throwable) e);
            this.b.b((int) R.string.errormsg_network_unfind);
            return "no";
        } catch (a e2) {
            this.b.e.a((Throwable) e2);
            this.b.b((CharSequence) e2.getMessage());
            return "no";
        } catch (Exception e3) {
            this.b.e.a((Throwable) e3);
            this.b.b((int) R.string.errormsg_server);
            return "no";
        }
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object doInBackground(Object... objArr) {
        return a();
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void onPostExecute(Object obj) {
        String str = (String) obj;
        super.onPostExecute(str);
        if (this.f1787a != null) {
            this.f1787a.dismiss();
        }
        if (str.equals("yes")) {
            this.b.a((CharSequence) "分享成功");
            this.b.finish();
        }
    }

    /* access modifiers changed from: protected */
    public final void onPreExecute() {
        if (this.f1787a != null) {
            this.f1787a.setOnCancelListener(new kj(this));
            this.f1787a.show();
        }
        super.onPreExecute();
    }
}
