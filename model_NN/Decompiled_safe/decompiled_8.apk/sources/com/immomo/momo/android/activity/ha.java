package com.immomo.momo.android.activity;

import android.content.DialogInterface;
import com.immomo.momo.android.view.EmoteEditeText;

final class ha implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ OtherProfileActivity f1708a;
    private final /* synthetic */ EmoteEditeText b;

    ha(OtherProfileActivity otherProfileActivity, EmoteEditeText emoteEditeText) {
        this.f1708a = otherProfileActivity;
        this.b = emoteEditeText;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        OtherProfileActivity.a(this.f1708a, this.b);
        new hy(this.f1708a, this.f1708a, this.b.getText().toString().trim()).execute(new Object[0]);
        dialogInterface.dismiss();
    }
}
