package com.immomo.momo.android.activity;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.g;
import android.support.v4.app.r;
import android.support.v4.view.ViewPager;
import android.support.v4.view.bf;
import android.view.ViewGroup;
import java.util.ArrayList;
import java.util.Iterator;

public final class kc extends r implements bf {

    /* renamed from: a  reason: collision with root package name */
    private final Context f1782a;
    private final ViewPager b;
    private ArrayList c;
    private boolean d;
    private /* synthetic */ ka e;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public kc(ka kaVar, g gVar, ViewPager viewPager, ArrayList arrayList) {
        super(gVar.c());
        this.e = kaVar;
        this.c = null;
        this.d = true;
        this.c = new ArrayList();
        this.f1782a = gVar;
        this.b = viewPager;
        if (arrayList != null) {
            Iterator it = arrayList.iterator();
            while (it.hasNext()) {
                this.c.add((kb) it.next());
            }
        }
        this.b.setOnPageChangeListener(this);
        this.b.setAdapter(this);
    }

    public final Fragment a(int i) {
        lh a2 = lh.a(this.f1782a, ((kb) this.c.get(i)).f1781a, this.e.n);
        a2.a(this.e.n);
        this.e.a(a2, i);
        return a2;
    }

    public final Object a(ViewGroup viewGroup, int i) {
        Object a2 = super.a(viewGroup, i);
        this.e.k.put(Integer.valueOf(i), (lh) a2);
        return a2;
    }

    public final void a(int i, float f) {
    }

    public final void a(ViewGroup viewGroup) {
        super.a(viewGroup);
        if (this.d) {
            this.d = false;
            a_(this.b.getCurrentItem());
        }
    }

    public final void a(ViewGroup viewGroup, int i, Object obj) {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.android.activity.ka.a(android.support.v4.app.Fragment, int):void
     arg types: [com.immomo.momo.android.activity.lh, int]
     candidates:
      com.immomo.momo.android.activity.ka.a(com.immomo.momo.android.activity.ka, int):void
      com.immomo.momo.android.activity.ka.a(com.immomo.momo.android.activity.lh, int):void
      com.immomo.momo.android.activity.ah.a(android.os.Bundle, java.lang.String):boolean
      com.immomo.momo.android.activity.ao.a(int, java.lang.String[]):com.immomo.momo.protocol.imjson.c.a
      com.immomo.momo.android.activity.ao.a(com.immomo.momo.android.view.bi, android.view.View$OnClickListener):void
      com.immomo.momo.android.activity.ao.a(java.lang.CharSequence, int):void
      com.immomo.momo.android.activity.ao.a(android.os.Bundle, java.lang.String):boolean
      com.immomo.momo.android.activity.ka.a(android.support.v4.app.Fragment, int):void */
    public final void a_(int i) {
        if (!(this.e.l < 0 || this.e.l == i || this.e.k.get(Integer.valueOf(this.e.l)) == null)) {
            ((lh) this.e.k.get(Integer.valueOf(this.e.l))).af();
            ((lh) this.e.k.get(Integer.valueOf(this.e.l))).ah();
        }
        lh lhVar = (lh) this.e.k.get(Integer.valueOf(i));
        if (lhVar != null) {
            lhVar.ad();
            this.e.a((Fragment) lhVar, i);
            this.e.l = i;
        }
    }

    public final int b() {
        return this.c.size();
    }

    public final void b(int i) {
    }
}
