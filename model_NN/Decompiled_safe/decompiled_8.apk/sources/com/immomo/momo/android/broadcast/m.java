package com.immomo.momo.android.broadcast;

import android.content.Context;
import android.content.IntentFilter;
import com.baidu.location.LocationClientOption;
import com.immomo.momo.g;

public final class m extends b {

    /* renamed from: a  reason: collision with root package name */
    public static final String f2356a = (String.valueOf(g.h()) + ".action.logout");

    public m(Context context) {
        super(context);
        IntentFilter intentFilter = new IntentFilter(f2356a);
        intentFilter.setPriority(LocationClientOption.MIN_SCAN_SPAN);
        a(intentFilter);
    }
}
