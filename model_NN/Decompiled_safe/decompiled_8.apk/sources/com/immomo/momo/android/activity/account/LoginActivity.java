package com.immomo.momo.android.activity.account;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.b.a;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.ao;
import com.immomo.momo.android.activity.retrieve.RetrievePWDActivity;
import com.immomo.momo.android.broadcast.l;
import com.immomo.momo.android.view.HeaderLayout;
import com.immomo.momo.android.view.a.n;
import com.immomo.momo.android.view.a.o;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.util.e;
import com.immomo.momo.util.k;
import java.util.regex.Pattern;

public class LoginActivity extends ao implements View.OnClickListener {
    /* access modifiers changed from: private */
    public String h = null;
    /* access modifiers changed from: private */
    public String[] i;
    /* access modifiers changed from: private */
    public String j;
    private TextView k = null;
    private TextView l = null;
    /* access modifiers changed from: private */
    public EditText m = null;
    private EditText n = null;
    private Button o = null;
    private Button p = null;
    private HeaderLayout q = null;
    /* access modifiers changed from: private */
    public j r = null;
    /* access modifiers changed from: private */
    public boolean s = false;
    /* access modifiers changed from: private */
    public String t = null;
    private TextView u = null;
    /* access modifiers changed from: private */
    public ImageView v = null;
    /* access modifiers changed from: private */
    public EditText w = null;
    /* access modifiers changed from: private */
    public n x = null;
    /* access modifiers changed from: private */
    public String y = PoiTypeDef.All;
    /* access modifiers changed from: private */
    public String z = PoiTypeDef.All;

    static /* synthetic */ void a(LoginActivity loginActivity, bf bfVar, int i2) {
        bf bfVar2 = new bf();
        bfVar2.W = bfVar.W;
        bfVar2.h = bfVar.h;
        bfVar2.f3011a = loginActivity.y;
        loginActivity.sendBroadcast(new Intent(l.f2355a));
        loginActivity.b(new h(loginActivity, loginActivity, bfVar2, i2));
    }

    private static boolean a(EditText editText) {
        String trim = editText.getText().toString().trim();
        if (trim != null && trim.length() > 0) {
            return false;
        }
        editText.requestFocus();
        return true;
    }

    /* access modifiers changed from: private */
    public void i() {
        boolean z2;
        boolean z3 = false;
        if (a(this.m)) {
            com.immomo.momo.util.ao.e(R.string.login_error_empty_account);
            z2 = false;
        } else {
            String trim = this.m.getText().toString().trim();
            if (Pattern.compile("(\\d{11})|(\\+\\d{3,})").matcher(trim).matches()) {
                if (trim.length() < 3) {
                    com.immomo.momo.util.ao.e(R.string.login_error_account);
                    this.m.requestFocus();
                    z2 = false;
                } else {
                    this.z = trim;
                    z2 = true;
                }
            } else if (Pattern.compile("\\d{3,}").matcher(trim).matches()) {
                this.z = trim;
                z2 = true;
            } else {
                if (Pattern.compile("\\w[\\w.-]*@[\\w.]+\\.\\w+").matcher(trim).matches()) {
                    this.z = trim;
                    z2 = true;
                } else {
                    com.immomo.momo.util.ao.e(R.string.login_error_account);
                    this.m.requestFocus();
                    z2 = false;
                }
            }
        }
        if (z2) {
            String trim2 = this.n.getText().toString().trim();
            if (a(this.n)) {
                com.immomo.momo.util.ao.e(R.string.login_error_empty_pwd);
            } else if (trim2.length() < 4) {
                com.immomo.momo.util.ao.b(String.format(g.a((int) R.string.login_pwd_sizemin), 4));
                this.n.requestFocus();
            } else {
                this.y = trim2;
                z3 = true;
            }
            if (z3) {
                b(new k(this, this));
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void d() {
    }

    public final void f() {
        Intent intent = new Intent(this, SecurityCheckActivity.class);
        intent.putExtra("account", this.z);
        startActivityForResult(intent, 867);
    }

    public final void g() {
        if (this.x != null) {
            this.x.dismiss();
        }
        this.s = false;
        View inflate = g.o().inflate((int) R.layout.include_register_1_scode, (ViewGroup) null);
        this.x = new n(this);
        this.x.a(0, (int) R.string.dialog_btn_confim, new c(this));
        this.x.a(1, (int) R.string.dialog_btn_cancel, (DialogInterface.OnClickListener) null);
        this.x.setTitle("输入验证码");
        this.x.a();
        this.x.setContentView(inflate);
        this.x.show();
        this.x.setOnDismissListener(new d(this));
        this.u = (TextView) inflate.findViewById(R.id.scode_tv_reload);
        this.v = (ImageView) inflate.findViewById(R.id.scode_iv_code);
        this.w = (EditText) inflate.findViewById(R.id.scode_et_inputcode);
        e.a(this.u, 0, this.u.getText().length());
        this.u.setOnClickListener(new e(this));
        b(new j(this, this));
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        if (i2 == 867 && i3 == -1) {
            i();
        }
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_back /*2131165286*/:
                finish();
                return;
            case R.id.btn_ok /*2131165289*/:
                i();
                return;
            case R.id.login_tv_forgotpassword /*2131165356*/:
                startActivity(new Intent(this, RetrievePWDActivity.class));
                return;
            case R.id.login_tv_selectcountrycode /*2131165749*/:
                this.i = a.d();
                o oVar = new o(this, this.i);
                oVar.setTitle("选择国家区号");
                oVar.a(new b(this));
                oVar.show();
                return;
            default:
                return;
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_login);
        this.k = (TextView) findViewById(R.id.login_tv_forgotpassword);
        e.a(this.k, 0, this.k.getText().length());
        this.l = (TextView) findViewById(R.id.login_tv_selectcountrycode);
        e.a(this.l, 0, this.l.getText().length());
        this.m = (EditText) findViewById(R.id.login_et_momoid);
        this.n = (EditText) findViewById(R.id.login_et_pwd);
        this.o = (Button) findViewById(R.id.btn_back);
        this.p = (Button) findViewById(R.id.btn_ok);
        this.q = (HeaderLayout) findViewById(R.id.layout_header);
        this.q.setTitleText((int) R.string.login);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String string = extras.getString("account");
            this.j = extras.getString("alipay_user_id");
            if (string != null) {
                this.m.setText(string);
                String string2 = extras.getString("passwrod");
                if (string2 != null) {
                    this.n.setText(string2);
                }
                this.n.setOnEditorActionListener(new a(this));
                this.o.setOnClickListener(this);
                this.p.setOnClickListener(this);
                this.k.setOnClickListener(this);
                this.l.setOnClickListener(this);
            }
        }
        String b = g.d().f668a.b("account", PoiTypeDef.All);
        if (!a.a((CharSequence) b)) {
            this.m.setText(b);
        }
        if (a.f(getIntent().getStringExtra("alipay_user_phonenumber"))) {
            this.m.setText(getIntent().getStringExtra("alipay_user_phonenumber"));
        }
        this.n.setOnEditorActionListener(new a(this));
        this.o.setOnClickListener(this);
        this.p.setOnClickListener(this);
        this.k.setOnClickListener(this);
        this.l.setOnClickListener(this);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        new k("PO", "P13").e();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        new k("PI", "P13").e();
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
    }
}
