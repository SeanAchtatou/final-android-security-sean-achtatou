package com.immomo.momo.android.activity;

import android.content.Intent;
import android.support.v4.b.a;
import com.immomo.momo.android.broadcast.d;
import com.immomo.momo.android.broadcast.f;
import com.immomo.momo.android.broadcast.w;

final class ia implements d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ OtherProfileV2Activity f1733a;

    ia(OtherProfileV2Activity otherProfileV2Activity) {
        this.f1733a = otherProfileV2Activity;
    }

    public final void a(Intent intent) {
        if (w.f2366a.equals(intent.getAction())) {
            String str = intent.getExtras() != null ? (String) intent.getExtras().get("momoid") : null;
            if (!a.a((CharSequence) str) && this.f1733a.t.h.equals(str) && this.f1733a.l && this.f1733a.r() && !this.f1733a.q()) {
                this.f1733a.v();
            }
        } else if (w.b.equals(intent.getAction())) {
            this.f1733a.k = this.f1733a.s.d(this.f1733a.o);
            this.f1733a.y();
        } else if (f.f2349a.equals(intent.getAction())) {
            if (this.f1733a.t.h.equals(intent.getStringExtra("userid"))) {
                this.f1733a.t.ah = this.f1733a.r.b(this.f1733a.t.h);
                this.f1733a.z();
            }
        }
    }
}
