package com.immomo.momo.android.view.a;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ViewFlipper;
import com.immomo.momo.R;
import com.immomo.momo.android.view.TiebaCategoryGuidView;
import com.immomo.momo.android.view.TiebaGuidView;

public final class az extends n {
    private View b;
    private Animation c = null;
    /* access modifiers changed from: private */
    public ImageView d;
    /* access modifiers changed from: private */
    public ImageView e;
    /* access modifiers changed from: private */
    public ViewFlipper f;
    /* access modifiers changed from: private */
    public TiebaCategoryGuidView g;
    /* access modifiers changed from: private */
    public TiebaGuidView h;
    /* access modifiers changed from: private */
    public Button i;
    /* access modifiers changed from: private */
    public Button j;
    /* access modifiers changed from: private */
    public int k = 0;
    /* access modifiers changed from: private */
    public be l;
    /* access modifiers changed from: private */
    public bg m;
    /* access modifiers changed from: private */
    public bi n;

    public az(Context context) {
        super(context);
        this.b = LayoutInflater.from(context).inflate((int) R.layout.dialog_tiebaguide, (ViewGroup) null);
        setContentView(this.b);
        this.d = (ImageView) this.b.findViewById(R.id.iv_loading1);
        this.e = (ImageView) this.b.findViewById(R.id.iv_loading2);
        this.f = (ViewFlipper) this.b.findViewById(R.id.dialog_tiebaguide_viewflipper);
        this.f.setInAnimation(getContext(), R.anim.push_left_in);
        this.f.setOutAnimation(getContext(), R.anim.push_left_out);
        this.g = (TiebaCategoryGuidView) this.b.findViewById(R.id.dialog_tiebaguide_guidview1);
        this.h = (TiebaGuidView) this.b.findViewById(R.id.dialog_tiebaguide_guidview2);
        this.i = a(0, "跳过", new bc(this));
        this.j = a(1, "下一步", new bd(this));
        d();
        this.h.setOnMomoGridViewItemClickListener(new ba(this));
        setOnDismissListener(new bb(this));
        ((Button) this.f2702a.get(1)).setBackgroundResource(R.drawable.btn_default_popsubmit);
        setCancelable(false);
    }

    public static void b(ImageView imageView) {
        imageView.clearAnimation();
        imageView.setVisibility(8);
    }

    /* access modifiers changed from: private */
    public void d() {
        if (this.k == 0) {
            setTitle((int) R.string.dialog_tiebaguide1);
        } else if (this.k == 1) {
            setTitle((int) R.string.dialog_tiebaguide2);
        }
    }

    public final void a(ImageView imageView) {
        imageView.setVisibility(0);
        if (this.c == null) {
            this.c = AnimationUtils.loadAnimation(getContext(), R.anim.loading);
        }
        imageView.startAnimation(this.c);
    }

    public final void a(bi biVar) {
        this.n = biVar;
    }

    public final void show() {
        super.show();
        new be(this, getContext()).execute(new Object[0]);
    }
}
