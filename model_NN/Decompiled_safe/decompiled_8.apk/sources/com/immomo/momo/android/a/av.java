package com.immomo.momo.android.a;

import android.content.Context;
import android.support.v4.b.a;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.android.view.HandyListView;
import com.immomo.momo.service.bean.GameApp;
import com.immomo.momo.service.bean.aj;
import com.immomo.momo.service.bean.l;
import com.immomo.momo.util.j;
import com.immomo.momo.util.m;

public final class av extends a {
    private m d = new m(this);
    private l e = null;
    private HandyListView f = null;

    public av(Context context, l lVar, HandyListView handyListView) {
        super(context);
        this.f = handyListView;
        this.e = lVar;
    }

    private boolean e(int i) {
        return i < this.e.c();
    }

    private boolean f(int i) {
        return i > this.e.c() && i <= this.e.c() + this.e.d();
    }

    public final boolean areAllItemsEnabled() {
        return false;
    }

    public final com.immomo.momo.service.bean.m d(int i) {
        if (e(i)) {
            return (com.immomo.momo.service.bean.m) this.e.a().get(i);
        }
        if (f(i)) {
            return (com.immomo.momo.service.bean.m) this.e.b().get((i - this.e.c()) - 1);
        }
        this.d.a((Object) ("getDiscoverItem 。。。。。。。position=" + i + ", count=" + getCount()));
        return null;
    }

    public final int getCount() {
        return this.e.d() == 0 ? this.e.c() : this.e.c() + this.e.d() + 1;
    }

    public final int getItemViewType(int i) {
        return (!e(i) && !f(i)) ? 0 : 1;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup, int, boolean):void
     arg types: [com.immomo.momo.service.bean.al, android.widget.ImageView, com.immomo.momo.android.view.HandyListView, int, int]
     candidates:
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, int, boolean, boolean):void
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup, int, boolean):void */
    public final View getView(int i, View view, ViewGroup viewGroup) {
        aw awVar;
        if (getItemViewType(i) == 1) {
            if (view == null) {
                view = a((int) R.layout.listitem_discover);
                awVar = new aw((byte) 0);
                awVar.b = (ImageView) view.findViewById(R.id.discover_iv_cover);
                awVar.c = (TextView) view.findViewById(R.id.discover_tv_name);
                awVar.f = (TextView) view.findViewById(R.id.discover_tv_number);
                awVar.e = (ImageView) view.findViewById(R.id.discover_iv_photo);
                awVar.d = (TextView) view.findViewById(R.id.discover_tv_subname);
                awVar.f728a = (ImageView) view.findViewById(R.id.discover_iv_point);
                view.setTag(awVar);
            } else {
                awVar = (aw) view.getTag();
            }
            com.immomo.momo.service.bean.m d2 = d(i);
            switch (d2.f3031a) {
                case 1:
                case 2:
                case 3:
                case 5:
                case 6:
                case 7:
                    awVar.e.setVisibility(8);
                    awVar.d.setVisibility(8);
                    awVar.c.setText(d2.b);
                    if (d2.b()) {
                        awVar.f728a.setVisibility(0);
                        awVar.f.setVisibility(8);
                    } else if (d2.a() > 0) {
                        awVar.f.setVisibility(0);
                        awVar.f.setText(new StringBuilder(String.valueOf(d2.a())).toString());
                        awVar.f728a.setVisibility(8);
                    } else {
                        awVar.f.setVisibility(8);
                        awVar.f728a.setVisibility(8);
                    }
                    switch (d2.f3031a) {
                        case 1:
                            awVar.b.setImageResource(R.drawable.ic_discover_group);
                            break;
                        case 2:
                            awVar.b.setImageResource(R.drawable.ic_discover_feed);
                            break;
                        case 3:
                            awVar.b.setImageResource(R.drawable.ic_discover_event);
                            break;
                        case 5:
                            if (d2.h && d2.a() <= 0) {
                                awVar.f.setVisibility(0);
                                awVar.f728a.setVisibility(8);
                                awVar.f.setText("新");
                            } else if (d2.a() <= 0) {
                                awVar.f728a.setVisibility(d2.d ? 0 : 8);
                            }
                            awVar.b.setImageResource(R.drawable.ic_discover_tieba);
                            break;
                        case 6:
                            awVar.b.setImageResource(R.drawable.ic_discover_roam);
                            break;
                        case 7:
                            if (d2.d && !a.a((CharSequence) d2.f)) {
                                awVar.f.setText(d2.f);
                                awVar.f.setVisibility(0);
                            }
                            awVar.b.setImageResource(R.drawable.ic_discover_emoteshop);
                            break;
                    }
                case 4:
                    awVar.e.setVisibility(8);
                    awVar.f.setVisibility(8);
                    GameApp gameApp = (GameApp) d2.c;
                    awVar.c.setText(gameApp.appname);
                    if (d2.d) {
                        awVar.f728a.setVisibility(0);
                        awVar.d.setVisibility(0);
                        awVar.d.setText(gameApp.eventNotice);
                    } else {
                        awVar.f728a.setVisibility(8);
                        awVar.d.setVisibility(0);
                        awVar.d.setText(gameApp.mcount);
                    }
                    j.a((aj) gameApp.appIconLoader(), awVar.b, (ViewGroup) this.f, 18, true);
                    break;
            }
        } else {
            if (view == null) {
                view = a((int) R.layout.listitem_discover_title);
            }
            TextView textView = (TextView) view.findViewById(R.id.discover_tv_title);
            textView.setVisibility(0);
            textView.setText("游戏信息");
        }
        return view;
    }

    public final int getViewTypeCount() {
        return 2;
    }

    public final boolean isEnabled(int i) {
        return getItemViewType(i) == 1;
    }
}
