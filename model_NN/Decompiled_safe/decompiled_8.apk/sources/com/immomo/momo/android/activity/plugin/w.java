package com.immomo.momo.android.activity.plugin;

import android.content.Context;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.a.o;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.n;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.plugin.e.a;
import com.immomo.momo.service.aq;
import com.immomo.momo.util.ao;

final class w extends d {

    /* renamed from: a  reason: collision with root package name */
    private int f2101a;
    private v c;
    /* access modifiers changed from: private */
    public a d;
    /* access modifiers changed from: private */
    public /* synthetic */ BindSinaActivity e;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public w(BindSinaActivity bindSinaActivity, Context context, a aVar, int i) {
        super(context);
        this.e = bindSinaActivity;
        this.d = aVar;
        this.f2101a = i;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        if (this.d == null) {
            this.d = com.immomo.momo.protocol.a.d.b(this.e.o, this.e.p, this.e.m, this.e.i);
        }
        return Integer.valueOf(com.immomo.momo.protocol.a.d.a(this.d.c(), this.d.b(), this.d.d(), this.d.a(), this.f2101a));
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
        this.c = new v(this.e);
        this.c.a("请求提交中");
        this.c.setCancelable(true);
        this.c.setOnCancelListener(new x(this));
        this.c.show();
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        this.c.dismiss();
        if (exc instanceof o) {
            n.a(this.e, (int) R.string.bingsina_dialog_msg, new y(this), new z()).show();
        } else if (exc instanceof com.immomo.momo.a.a) {
            ao.a((CharSequence) exc.getMessage());
        } else {
            ao.g(R.string.errormsg_server);
        }
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        Integer num = (Integer) obj;
        super.a(num);
        this.c.dismiss();
        this.e.f.an = true;
        this.e.f.am = this.d.b();
        this.b.a((Object) ("remain day after bind: " + this.e.f.aG));
        if (num.intValue() == 1) {
            this.e.f.ao = "vip";
            this.e.f.ap = true;
        } else {
            this.e.f.ao = PoiTypeDef.All;
            this.e.f.ap = false;
        }
        new aq().b(this.e.f);
        if (this.e.n) {
            ao.a((CharSequence) "绑定成功");
        }
        this.e.setResult(-1, null);
        this.e.finish();
    }
}
