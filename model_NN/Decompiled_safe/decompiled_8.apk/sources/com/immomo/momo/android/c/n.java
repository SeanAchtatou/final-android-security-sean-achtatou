package com.immomo.momo.android.c;

import android.graphics.Bitmap;
import com.immomo.a.a.f.b;
import com.immomo.momo.android.plugin.a.a;
import com.immomo.momo.g;
import com.immomo.momo.util.i;
import com.immomo.momo.util.m;
import mm.purchasesdk.PurchaseCode;

public final class n extends t {
    private static String[] e = {"texture_bluist.jpg", "texture_classical.jpg", "texture_industrial.jpg", "texture_masterpiece.jpg", "texture_mellow.jpg", "texture_old_school.jpg", "texture_shine.png", "texture_sunburn.jpg", "texture_violetburst.png"};

    /* renamed from: a  reason: collision with root package name */
    private Bitmap f2388a;
    private int b;
    private boolean c;
    private m d = new m(getClass().getSimpleName());

    public n(g gVar, Bitmap bitmap, int i, boolean z) {
        super(gVar);
        this.f2388a = bitmap;
        this.b = i;
        this.c = z;
    }

    private Bitmap a(Bitmap bitmap, float f, int i, String str) {
        if (bitmap == null) {
            return null;
        }
        switch (i) {
            case 0:
            case 4:
            case 6:
            case 7:
            case 8:
                return a.a(g.c(), this.f2388a, f, str, e[i]);
            case 1:
                return a.b(g.c(), this.f2388a, f, str, e[i]);
            case 2:
                return a.e(g.c(), this.f2388a, f, str, e[i]);
            case 3:
                return a.d(g.c(), this.f2388a, f, str, e[i]);
            case 5:
                return a.c(g.c(), this.f2388a, f, str, e[i]);
            default:
                return null;
        }
    }

    public final void a() {
    }

    public final Bitmap b() {
        if (this.f2388a == null) {
            return null;
        }
        switch (this.b) {
            case 99:
            default:
                return null;
            case PurchaseCode.INIT_OK /*100*/:
                return a.a(this.f2388a, 30.0f, -10.0f);
            case 1001:
                return a.i(this.f2388a);
            case 1002:
                return a.e(this.f2388a);
            case 1003:
                return a.f(this.f2388a);
            case 1004:
                return a.h(this.f2388a);
            case 1005:
                return a.d(this.f2388a);
            case 1012:
                return a.g(this.f2388a);
            case 1013:
                return a(this.f2388a, 0.95f, 6, "Multiply");
            case 1014:
                return a(this.f2388a, 0.27f, 8, "colorBurn");
            case 1015:
                return a.a(this.f2388a);
            case 1016:
                return a.b(this.f2388a);
            case 1017:
                return a.c(this.f2388a);
        }
    }

    public final void run() {
        Bitmap bitmap;
        try {
            Bitmap b2 = b();
            if (this.c) {
                bitmap = android.support.v4.b.a.a(b2, (float) g.a(3.0f));
                i.a(b.a(), b2);
            } else {
                bitmap = b2;
            }
            a(bitmap);
        } catch (Exception e2) {
            this.d.a((Throwable) e2);
            a((Object) null);
        }
    }
}
