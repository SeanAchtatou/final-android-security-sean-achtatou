package com.immomo.momo.android.a.a;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.AnimationDrawable;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.ImageBrowserActivity;
import com.immomo.momo.android.activity.message.a;
import com.immomo.momo.android.c.r;
import com.immomo.momo.android.c.u;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.Message;
import java.io.File;
import java.util.List;
import java.util.concurrent.ThreadPoolExecutor;
import mm.purchasesdk.PurchaseCode;

public final class s extends ab implements View.OnClickListener, View.OnFocusChangeListener {
    private static ThreadPoolExecutor t = new u(3, 3);

    /* renamed from: a  reason: collision with root package name */
    private ImageView f701a;
    private ImageView o;
    private View p;
    private TextView q;
    private ImageView r;
    /* access modifiers changed from: private */
    public AnimationDrawable s = null;
    /* access modifiers changed from: private */
    public Handler u = new t(this, e().getMainLooper());

    protected s(a aVar) {
        super(aVar);
    }

    static /* synthetic */ void a(s sVar) {
        if (sVar.s != null) {
            sVar.s.stop();
        }
        sVar.p.setVisibility(4);
    }

    /* access modifiers changed from: private */
    public void c() {
        this.p.setVisibility(0);
        this.q.setVisibility(8);
        this.o.setVisibility(4);
        if (this.s != null) {
            this.s.stop();
        }
        this.r.setImageResource(R.drawable.ic_chat_def_pic_failure);
    }

    /* access modifiers changed from: private */
    public void d() {
        this.s = new AnimationDrawable();
        this.s.addFrame(g.b((int) R.drawable.ic_loading_msgplus_01), PurchaseCode.UNSUPPORT_ENCODING_ERR);
        this.s.addFrame(g.b((int) R.drawable.ic_loading_msgplus_02), PurchaseCode.UNSUPPORT_ENCODING_ERR);
        this.s.addFrame(g.b((int) R.drawable.ic_loading_msgplus_03), PurchaseCode.UNSUPPORT_ENCODING_ERR);
        this.s.addFrame(g.b((int) R.drawable.ic_loading_msgplus_04), PurchaseCode.UNSUPPORT_ENCODING_ERR);
        this.s.setOneShot(false);
        this.p.setVisibility(0);
        this.o.setVisibility(0);
        this.o.setImageDrawable(this.s);
        this.r.setImageResource(R.drawable.ic_chat_def_pic);
        this.u.post(new u(this));
    }

    /* access modifiers changed from: private */
    public void k() {
        int round = Math.round(this.h.fileUploadProgrss);
        this.f701a.setVisibility(0);
        if (round < 100) {
            this.q.setVisibility(0);
            this.q.setText(String.valueOf(round) + "%");
        }
    }

    /* access modifiers changed from: protected */
    public final void a() {
        View inflate = this.k.inflate((int) R.layout.message_image, (ViewGroup) null);
        this.i.addView(inflate, 0);
        this.f701a = (ImageView) inflate.findViewById(R.id.message_iv_msgimage);
        this.p = inflate.findViewById(R.id.layer_download);
        this.o = (ImageView) inflate.findViewById(R.id.download_view);
        this.q = (TextView) inflate.findViewById(R.id.progress_text);
        this.r = (ImageView) inflate.findViewById(R.id.download_view_image);
        this.i.setOnLongClickListener(this);
        this.i.setOnClickListener(this);
        this.i.setOnFocusChangeListener(this);
    }

    public final void a(float f) {
        this.h.fileUploadProgrss = f;
        this.u.sendEmptyMessage(PurchaseCode.BILL_DYMARK_CREATE_ERROR);
    }

    /* access modifiers changed from: protected */
    public final void b() {
        int i = 1;
        boolean z = false;
        this.q.setVisibility(8);
        this.f701a.setVisibility(4);
        this.p.setVisibility(4);
        if (j != null && j.contains(this.h.msgId)) {
            d();
        } else if (this.h.status == 7) {
            k();
        }
        Bitmap b = b(this.h.fileName);
        if (b != null) {
            this.f701a.setVisibility(0);
            this.f701a.setImageBitmap(b);
        } else if (this.h.isImageLoadingFailed()) {
            c();
        } else if (this.h.isLoadingResourse || e().y().g()) {
            d();
        } else {
            this.h.isLoadingResourse = true;
            v vVar = new v(this, this.h);
            if (this.h.fileName.indexOf("://") < 0) {
                z = new File(com.immomo.momo.a.k(), String.valueOf(this.h.fileName) + ".jpg_").exists();
            }
            if (!z) {
                j.add(this.h.msgId);
                d();
            }
            ThreadPoolExecutor threadPoolExecutor = t;
            String a2 = android.support.v4.b.a.a(this.h);
            if (this.h.chatType == 2 || this.h.chatType == 3) {
                i = 14;
            }
            threadPoolExecutor.execute(new r(a2, vVar, i, null));
        }
    }

    public final void onClick(View view) {
        int i = 0;
        if (b(this.h.fileName) != null) {
            List I = e().I();
            String[] strArr = new String[I.size()];
            String a2 = android.support.v4.b.a.a(this.h);
            int i2 = -1;
            while (true) {
                int i3 = i;
                if (i3 >= I.size()) {
                    break;
                }
                String a3 = android.support.v4.b.a.a((Message) I.get(i3));
                this.n.b((Object) ("message:" + i3 + "  " + ((Message) I.get(i3)).hashCode() + "  " + I.get(i3)));
                strArr[i3] = a3;
                if (a3.equals(a2)) {
                    i2 = i3;
                }
                i = i3 + 1;
            }
            Intent intent = new Intent(e(), ImageBrowserActivity.class);
            intent.putExtra("array", strArr);
            intent.putExtra("imagetype", (this.h.chatType == 2 || this.h.chatType == 3) ? "gchat" : "chat");
            intent.putExtra("index", i2);
            e().startActivity(intent);
            e().overridePendingTransition(R.anim.zoom_enter, R.anim.normal);
        } else if (this.h.isImageLoadingFailed()) {
            this.h.setImageLoadFailed(false);
            e().Y();
        }
    }

    public final void onFocusChange(View view, boolean z) {
    }
}
