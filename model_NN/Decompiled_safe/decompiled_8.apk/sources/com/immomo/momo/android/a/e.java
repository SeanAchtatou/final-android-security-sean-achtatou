package com.immomo.momo.android.a;

import android.widget.Filter;
import com.immomo.momo.service.bean.bf;
import java.util.ArrayList;
import java.util.List;

final class e extends Filter {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ c f790a;

    private e(c cVar) {
        this.f790a = cVar;
    }

    /* synthetic */ e(c cVar, byte b) {
        this(cVar);
    }

    /* access modifiers changed from: protected */
    public final Filter.FilterResults performFiltering(CharSequence charSequence) {
        ArrayList arrayList;
        ArrayList arrayList2;
        Filter.FilterResults filterResults = new Filter.FilterResults();
        if (this.f790a.e == null) {
            synchronized (this.f790a.i) {
                this.f790a.e = new ArrayList();
            }
        }
        if (charSequence == null || charSequence.length() == 0) {
            synchronized (this.f790a.i) {
                arrayList = new ArrayList(this.f790a.e);
            }
            filterResults.values = arrayList;
            filterResults.count = arrayList.size();
        } else {
            String charSequence2 = charSequence.toString();
            synchronized (this.f790a.i) {
                arrayList2 = (ArrayList) this.f790a.e;
            }
            int size = arrayList2.size();
            ArrayList arrayList3 = new ArrayList();
            for (int i = 0; i < size; i++) {
                bf bfVar = (bf) arrayList2.get(i);
                String trim = bfVar.h().trim();
                String lowerCase = bfVar.j.trim().toLowerCase();
                if (trim.startsWith(charSequence2) || lowerCase.startsWith(charSequence2.toLowerCase())) {
                    arrayList3.add(bfVar);
                } else {
                    String[] split = trim.split(" ");
                    String[] split2 = lowerCase.split(" ");
                    int length = split.length;
                    int length2 = split2.length;
                    boolean z = false;
                    int i2 = 0;
                    while (true) {
                        if (i2 >= length2) {
                            break;
                        } else if (split2[i2].startsWith(charSequence2.toLowerCase())) {
                            arrayList3.add(bfVar);
                            z = true;
                            break;
                        } else {
                            i2++;
                        }
                    }
                    if (!z) {
                        int i3 = 0;
                        while (true) {
                            if (i3 >= length) {
                                break;
                            } else if (split[i3].startsWith(charSequence2)) {
                                arrayList3.add(bfVar);
                                break;
                            } else {
                                i3++;
                            }
                        }
                    }
                }
            }
            filterResults.values = arrayList3;
            filterResults.count = arrayList3.size();
        }
        return filterResults;
    }

    /* access modifiers changed from: protected */
    public final void publishResults(CharSequence charSequence, Filter.FilterResults filterResults) {
        this.f790a.f = (List) filterResults.values;
        if (filterResults.count > 0) {
            this.f790a.notifyDataSetChanged();
        } else {
            this.f790a.notifyDataSetInvalidated();
        }
    }
}
