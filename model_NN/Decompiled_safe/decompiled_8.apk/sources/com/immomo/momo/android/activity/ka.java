package com.immomo.momo.android.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.w;
import android.view.KeyEvent;
import com.immomo.momo.R;
import com.immomo.momo.android.view.ScrollViewPager;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class ka extends ah {
    private final ArrayList h = new ArrayList();
    private ScrollViewPager i;
    private boolean j = true;
    /* access modifiers changed from: private */
    public Map k = new HashMap();
    /* access modifiers changed from: private */
    public int l = -1;
    private int m = 1;
    /* access modifiers changed from: private */
    public boolean n = true;

    /* access modifiers changed from: protected */
    public void a(Fragment fragment, int i2) {
    }

    /* access modifiers changed from: protected */
    public void a(lh lhVar, int i2) {
    }

    public final void a(Class... clsArr) {
        for (Class kbVar : clsArr) {
            this.h.add(new kb(kbVar));
        }
    }

    public final void c(int i2) {
        if (this.i != null) {
            if (i2 == this.l && v() != null && v().J()) {
                v().ai();
            }
            this.i.setCurrentItem(i2);
        }
        this.l = i2;
    }

    public void onBackPressed() {
        lh lhVar = (lh) this.k.get(Integer.valueOf(this.l));
        if (lhVar == null || !lhVar.J() || !lhVar.G()) {
            super.onBackPressed();
        }
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        Fragment fragment = (Fragment) this.k.get(Integer.valueOf(this.l));
        if (fragment == null || !(fragment instanceof lh) || !((lh) fragment).a(i2, keyEvent)) {
            return super.onKeyDown(i2, keyEvent);
        }
        return true;
    }

    public boolean onKeyUp(int i2, KeyEvent keyEvent) {
        Fragment fragment = (Fragment) this.k.get(Integer.valueOf(this.l));
        if (fragment == null || !(fragment instanceof lh) || !((lh) fragment).b(i2, keyEvent)) {
            return super.onKeyUp(i2, keyEvent);
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        lh lhVar = (lh) this.k.get(Integer.valueOf(this.l));
        if (lhVar != null && lhVar.J()) {
            lhVar.af();
        }
    }

    /* access modifiers changed from: protected */
    public void onPostCreate(Bundle bundle) {
        super.onPostCreate(bundle);
        this.i = (ScrollViewPager) findViewById(R.id.tabcontent);
        this.i.setEnableTouchScroll(this.j);
        this.i.setOffscreenPageLimit(this.m);
        if (bundle != null) {
            w a2 = c().a();
            Iterator it = this.h.iterator();
            while (it.hasNext()) {
                Fragment a3 = c().a(((kb) it.next()).f1781a.getName());
                if (a3 != null) {
                    a2.b(a3);
                }
            }
            a2.c();
            this.l = bundle.getInt("tab");
        }
        new kc(this, this, this.i, this.h);
        if (this.l != -1) {
            this.i.setCurrentItem(this.l);
        } else {
            this.i.setCurrentItem(0);
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        lh lhVar = (lh) this.k.get(Integer.valueOf(this.l));
        if (lhVar != null && lhVar.J() && !lhVar.M()) {
            lhVar.ad();
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        bundle.putInt("tab", this.i.getCurrentItem());
        super.onSaveInstanceState(bundle);
    }

    public final void u() {
        this.n = false;
    }

    public final lh v() {
        return (lh) this.k.get(Integer.valueOf(this.i.getCurrentItem()));
    }

    public final int w() {
        return this.i.getCurrentItem();
    }

    /* access modifiers changed from: protected */
    public void x() {
    }
}
