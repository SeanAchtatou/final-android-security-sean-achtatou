package com.immomo.momo.android.map;

import android.location.Location;
import com.immomo.momo.android.b.l;
import com.immomo.momo.android.b.p;

final class z extends p {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ GeoGoogleMapActivity f2520a;

    z(GeoGoogleMapActivity geoGoogleMapActivity) {
        this.f2520a = geoGoogleMapActivity;
    }

    public final void a(Location location, int i, int i2, int i3) {
        if (i != 0) {
            this.f2520a.h.a(location, i, i2, i3);
        } else if (com.immomo.momo.android.b.z.a(location)) {
            new l(this.f2520a.h, 0).execute(location);
        } else {
            this.f2520a.h.a(location, i, i2, i3);
        }
    }
}
