package com.immomo.momo.android.activity.maintab;

import android.content.Context;
import com.immomo.momo.R;
import com.immomo.momo.android.c.d;
import com.immomo.momo.protocol.a.n;
import com.immomo.momo.service.bean.ay;
import com.immomo.momo.util.am;
import java.util.List;

final class an extends d {

    /* renamed from: a  reason: collision with root package name */
    private double f1839a;
    private double c;
    private /* synthetic */ x d;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public an(x xVar, Context context, double d2, double d3) {
        super(context);
        this.d = xVar;
        this.f1839a = d2;
        this.c = d3;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        List a2 = n.a().a(0, this.f1839a, this.c, this.d.M.aH);
        this.d.U.a(a2);
        return a2;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        if (this.d.Y != null && !this.d.Y.isCancelled()) {
            this.d.Y.cancel(true);
        }
        this.d.af.g();
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        super.a(exc);
        this.d.v();
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        List<ay> list = (List) obj;
        if (list == null || list.size() <= 0) {
            a((Exception) null);
            return;
        }
        am.a().a(R.raw.ref_success);
        this.d.Q();
        this.d.ac.setLastFlushTime(this.d.W);
        this.d.N.a("ng_lasttime_success", this.d.W);
        this.d.O.clear();
        for (ay ayVar : list) {
            this.d.O.put(ayVar.f3001a, ayVar);
        }
        this.d.Q.clear();
        this.d.Q.addAll(list);
        this.d.R.notifyDataSetChanged();
        this.d.R.a();
        this.d.al();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.d.X = (d) null;
    }
}
