package com.immomo.momo.android.activity.feed;

import android.support.v4.b.a;
import android.text.Editable;
import android.text.TextWatcher;
import com.amap.mapapi.poisearch.PoiTypeDef;

final class ax implements TextWatcher {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ PublishFeedActivity f1454a;

    ax(PublishFeedActivity publishFeedActivity) {
        this.f1454a = publishFeedActivity;
    }

    public final void afterTextChanged(Editable editable) {
        if (a.a((CharSequence) editable.toString())) {
            this.f1454a.q.setText(PoiTypeDef.All);
        } else {
            this.f1454a.q.setText(String.valueOf(editable.length()) + "/120字");
        }
    }

    public final void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public final void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }
}
