package com.immomo.momo.android.activity.feed;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.protocol.a.a.k;
import java.util.Collection;
import java.util.Date;

final class ae extends d {

    /* renamed from: a  reason: collision with root package name */
    private boolean f1436a = true;
    private /* synthetic */ ad c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ae(ad adVar, Context context, boolean z) {
        super(context);
        this.c = adVar;
        this.f1436a = z;
        if (z && adVar.S != null) {
            adVar.S.cancel(true);
        } else if (!z && adVar.T != null) {
            adVar.T.cancel(true);
        }
        if (z) {
            adVar.S = this;
        } else {
            adVar.T = this;
        }
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        if (this.f1436a) {
            k c2 = com.immomo.momo.protocol.a.k.a().c();
            this.c.P.c(c2.b);
            return c2;
        }
        k a2 = com.immomo.momo.protocol.a.k.a().a(this.c.O.getCount(), ad.e(this.c));
        this.c.P.d(a2.b);
        return a2;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        k kVar = (k) obj;
        if (!this.f1436a) {
            this.c.O.b((Collection) kVar.b);
        } else {
            this.c.O.a((Collection) kVar.b);
            Date date = new Date();
            this.c.N.a("prf_time_my_comment", date);
            this.c.Q.setLastFlushTime(date);
            ((MainFeedActivity) this.c.c()).z();
        }
        if (kVar.f2890a <= 0) {
            this.c.R.setVisibility(8);
        } else {
            this.c.R.setVisibility(0);
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.c.Q.n();
        this.c.R.e();
        if (!this.f1436a) {
            this.c.T.cancel(true);
        } else {
            this.c.S.cancel(true);
        }
    }
}
