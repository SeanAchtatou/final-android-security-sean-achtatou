package com.immomo.momo.android.view;

import android.content.Context;
import android.util.AttributeSet;
import com.immomo.momo.g;

public class MSmallEmoteTextView extends EmoteTextView {

    /* renamed from: a  reason: collision with root package name */
    private static MSmallEmoteTextView f2636a = null;

    public MSmallEmoteTextView(Context context) {
        super(context);
    }

    public MSmallEmoteTextView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public MSmallEmoteTextView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public static EmoteTextView getInstance() {
        if (f2636a == null) {
            f2636a = new MSmallEmoteTextView(g.c());
        }
        return f2636a;
    }

    public final CharSequence a(CharSequence charSequence) {
        return bp.b(super.a(charSequence));
    }
}
