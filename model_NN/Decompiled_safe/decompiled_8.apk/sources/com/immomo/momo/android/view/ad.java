package com.immomo.momo.android.view;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.style.ImageSpan;
import com.immomo.momo.R;
import com.immomo.momo.g;

public final class ad extends ImageSpan {
    public ad(Context context, int i) {
        super(context, i);
    }

    public ad(Context context, int i, byte b) {
        super(context, i, 0);
    }

    public final Drawable getDrawable() {
        try {
            return super.getDrawable();
        } catch (Throwable th) {
            System.gc();
            Drawable b = g.b((int) R.drawable.zemoji_error);
            b.setBounds(0, 0, b.getIntrinsicWidth(), b.getIntrinsicHeight());
            return b;
        }
    }
}
