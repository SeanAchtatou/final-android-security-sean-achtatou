package com.immomo.momo.android.activity.common;

import android.os.Bundle;
import com.immomo.momo.R;
import com.immomo.momo.android.view.bi;
import com.immomo.momo.service.ag;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.service.bean.n;
import com.immomo.momo.service.bean.p;
import com.immomo.momo.service.h;

public class InviteToDiscussTabsActivity extends j {
    private String i = null;
    private n j;
    private h k;

    static /* synthetic */ void a(InviteToDiscussTabsActivity inviteToDiscussTabsActivity) {
        if (inviteToDiscussTabsActivity.D().size() <= 0) {
            inviteToDiscussTabsActivity.a((int) R.string.discuss_select_createwarn_little_select);
        } else if (inviteToDiscussTabsActivity.D().size() <= 0) {
            inviteToDiscussTabsActivity.a((int) R.string.discuss_select_createwarn_little);
        } else if (inviteToDiscussTabsActivity.D().size() > inviteToDiscussTabsActivity.h) {
            inviteToDiscussTabsActivity.a((int) R.string.discuss_select_toastwarn_much);
        } else {
            new aw(inviteToDiscussTabsActivity, inviteToDiscussTabsActivity, inviteToDiscussTabsActivity.D(), inviteToDiscussTabsActivity.i).execute(new Object[0]);
        }
    }

    /* access modifiers changed from: protected */
    public final String A() {
        return String.format(getString(R.string.discuss_select_invitewarn_much), Integer.valueOf(this.h));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.service.h.a(java.lang.String, boolean):com.immomo.momo.service.bean.n
     arg types: [java.lang.String, int]
     candidates:
      com.immomo.momo.service.h.a(com.immomo.momo.service.bean.n, java.lang.String):void
      com.immomo.momo.service.h.a(java.lang.String, int):void
      com.immomo.momo.service.h.a(java.lang.String, java.lang.String):void
      com.immomo.momo.service.h.a(java.lang.String, java.lang.String[]):void
      com.immomo.momo.service.h.a(java.util.List, java.lang.String):void
      com.immomo.momo.service.h.a(java.lang.String, boolean):com.immomo.momo.service.bean.n */
    /* access modifiers changed from: protected */
    public final void B() {
        new ag();
        this.k = new h();
        if (getIntent().getExtras() != null && getIntent().getExtras().getString("did") != null) {
            this.i = getIntent().getExtras().getString("did");
            this.j = this.k.a(this.i, true);
        }
    }

    /* access modifiers changed from: protected */
    public final void C() {
        int i2 = 0;
        a(ay.class, a.class);
        findViewById(R.id.contact_tab_both).setOnClickListener(this);
        findViewById(R.id.contact_tab_follows).setOnClickListener(this);
        int intExtra = getIntent().getIntExtra("showindex", 0);
        if (intExtra >= 0) {
            i2 = intExtra > 1 ? 1 : intExtra;
        }
        c(i2);
    }

    /* access modifiers changed from: protected */
    public final void a(int i2, int i3) {
        m().setTitleText(getResources().getString(R.string.discuss_select_invitetitle, Integer.valueOf(i2), Integer.valueOf(i3)));
    }

    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        super.a(bundle);
        if (this.j != null) {
            this.h = this.j.k - this.j.j;
        } else {
            this.h = 20;
        }
        if (this.j.d != null) {
            int i2 = 0;
            while (true) {
                int i3 = i2;
                if (i3 >= this.j.d.size()) {
                    break;
                }
                p pVar = (p) this.j.d.get(i3);
                if (pVar != null) {
                    E().put(pVar.f3034a, pVar.h);
                }
                i2 = i3 + 1;
            }
        }
        E().put("10000", new bf("10000"));
        a(D().size(), this.h);
        bi biVar = new bi(this);
        biVar.a("提交");
        biVar.a((int) R.drawable.ic_topbar_confirm);
        a(biVar, new av(this));
    }

    /* access modifiers changed from: protected */
    public final void a(String str, int i2) {
    }

    /* access modifiers changed from: protected */
    public final boolean y() {
        return false;
    }

    /* access modifiers changed from: protected */
    public final int z() {
        return this.h;
    }
}
