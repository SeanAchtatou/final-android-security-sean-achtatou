package com.immomo.momo.android.activity.account;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.b.a;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.ViewFlipper;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.a.a.f.b;
import com.immomo.momo.MomoApplication;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.ImageFactoryActivity;
import com.immomo.momo.android.activity.ao;
import com.immomo.momo.android.view.HeaderLayout;
import com.immomo.momo.android.view.a.n;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.util.ak;
import com.immomo.momo.util.h;
import com.immomo.momo.util.m;
import java.io.File;
import java.util.Date;
import java.util.UUID;

public class RegisterActivity extends ao implements View.OnClickListener {
    private static String B = PoiTypeDef.All;
    private String A = null;
    public String h = PoiTypeDef.All;
    public int i = 0;
    public long j = 0;
    public int[] k = null;
    public String l = null;
    /* access modifiers changed from: private */
    public int m = 0;
    /* access modifiers changed from: private */
    public bf n = null;
    /* access modifiers changed from: private */
    public String o = "empty";
    /* access modifiers changed from: private */
    public ac p = null;
    private an q = null;
    private ao r = null;
    private ap s = null;
    /* access modifiers changed from: private */
    public ab t = null;
    /* access modifiers changed from: private */
    public ViewFlipper u = null;
    private HeaderLayout v = null;
    private TextView w = null;
    private m x = new m("RegisterActivity");
    private Button y = null;
    /* access modifiers changed from: private */
    public Button z = null;

    /* access modifiers changed from: private */
    public void i() {
        n.b(this, (int) R.string.errormsg_devices, (DialogInterface.OnClickListener) null).show();
    }

    /* access modifiers changed from: private */
    public ab j() {
        this.w.setText(String.valueOf(this.m) + "/4");
        switch (this.m) {
            case 1:
                if (this.p == null) {
                    this.p = new ac(this.n, this.u.getCurrentView(), this);
                }
                ac acVar = this.p;
                this.v.setTitleText("注册新账号");
                return acVar;
            case 2:
                if (this.q == null) {
                    this.q = new an(this.n, this.u.getCurrentView(), this);
                }
                an anVar = this.q;
                this.v.setTitleText("填写基本资料");
                return anVar;
            case 3:
                if (this.r == null) {
                    this.r = new ao(this.n, this.u.getCurrentView());
                }
                ao aoVar = this.r;
                this.v.setTitleText("您的生日");
                return aoVar;
            case 4:
                if (this.s == null) {
                    this.s = new ap(this.n, this.u.getCurrentView(), this);
                }
                ap apVar = this.s;
                this.v.setTitleText("设置头像");
                return apVar;
            default:
                return null;
        }
    }

    private void k() {
        if (this.m <= 1) {
            n.a(this, "确认要放弃注册么？", new m(this)).show();
            return;
        }
        this.u.setInAnimation(getApplicationContext(), R.anim.push_right_in);
        this.u.setOutAnimation(getApplicationContext(), R.anim.push_right_out);
        this.u.showPrevious();
        this.m--;
        this.t = j();
        switch (this.m) {
            case 1:
                this.z.setText("返回");
                return;
            case 2:
            default:
                return;
            case 3:
                this.y.setText("下一步");
                return;
        }
    }

    public final void b(boolean z2) {
        if (z2) {
            this.u.showNext();
            this.m++;
            this.t = j();
            switch (this.m) {
                case 2:
                    this.z.setText((int) R.string.reg_prestep);
                    return;
                case 3:
                default:
                    return;
                case 4:
                    this.y.setText((int) R.string.reg);
                    return;
            }
        } else if (this.m >= 4 && this.t.a()) {
            b(new r(this, this, this.n, (byte) 0));
        } else if (this.m < 0 || this.t.a()) {
            this.u.setInAnimation(getApplicationContext(), R.anim.push_left_in);
            this.u.setOutAnimation(getApplicationContext(), R.anim.push_left_out);
            switch (this.m) {
                case 1:
                    if (new StringBuilder(String.valueOf(this.o)).toString().equals(String.valueOf(this.n.G) + this.n.f3011a)) {
                        this.z.setText((int) R.string.reg_prestep);
                        break;
                    } else {
                        b(new n(this, this));
                        return;
                    }
                case 3:
                    this.y.setText((int) R.string.reg);
                    break;
            }
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService("input_method");
            View currentFocus = getCurrentFocus();
            if (currentFocus != null) {
                inputMethodManager.hideSoftInputFromWindow(currentFocus.getWindowToken(), 2);
            }
            this.u.showNext();
            this.m++;
            this.t = j();
        }
    }

    public final void f() {
        Intent intent = new Intent("android.intent.action.GET_CONTENT");
        intent.setType("image/*");
        startActivityForResult(Intent.createChooser(intent, "本地图片"), 11);
    }

    public final void g() {
        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("immomo_");
        stringBuffer.append(a.c(new Date()));
        stringBuffer.append("_" + UUID.randomUUID());
        stringBuffer.append(".jpg");
        B = stringBuffer.toString();
        this.x.a((Object) ("camera_filename=" + B));
        intent.putExtra("output", Uri.fromFile(new File(com.immomo.momo.a.i(), B)));
        startActivityForResult(intent, 12);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.b.a.a(android.graphics.Bitmap, float, boolean):android.graphics.Bitmap
     arg types: [android.graphics.Bitmap, int, int]
     candidates:
      android.support.v4.b.a.a(android.graphics.Bitmap, int, int):android.graphics.Bitmap
      android.support.v4.b.a.a(java.io.File, int, int):android.graphics.Bitmap
      android.support.v4.b.a.a(java.lang.String, java.lang.String, java.lang.String):java.lang.String
      android.support.v4.b.a.a(java.lang.Object[], java.lang.String, java.lang.String):java.lang.String
      android.support.v4.b.a.a(android.graphics.Bitmap, float, boolean):android.graphics.Bitmap */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.h.a(java.lang.String, android.graphics.Bitmap, int, boolean):java.io.File
     arg types: [java.lang.String, android.graphics.Bitmap, int, int]
     candidates:
      com.immomo.momo.util.h.a(java.lang.String, java.lang.String, int, boolean):void
      com.immomo.momo.util.h.a(java.lang.String, android.graphics.Bitmap, int, boolean):java.io.File */
    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        Uri fromFile;
        File a2;
        Bitmap a3;
        switch (i2) {
            case 11:
                if (i3 == -1 && intent != null) {
                    Uri data = intent.getData();
                    Intent intent2 = new Intent(getApplicationContext(), ImageFactoryActivity.class);
                    intent2.setData(data);
                    intent2.putExtra("aspectY", 1);
                    intent2.putExtra("aspectX", 1);
                    intent2.putExtra("minsize", 320);
                    this.A = b.a(8);
                    intent2.putExtra("outputFilePath", h.a(this.A, 2).getAbsolutePath());
                    startActivityForResult(intent2, 13);
                    return;
                }
                return;
            case 12:
                this.x.a((Object) ("camera_filename:" + B + ", resultCode:" + i3 + ", data:" + intent));
                if (i3 == -1 && !a.a((CharSequence) B) && (fromFile = Uri.fromFile(new File(com.immomo.momo.a.i(), B))) != null) {
                    Intent intent3 = new Intent(getApplicationContext(), ImageFactoryActivity.class);
                    intent3.setData(fromFile);
                    intent3.putExtra("aspectY", 1);
                    intent3.putExtra("aspectX", 1);
                    intent3.putExtra("minsize", 320);
                    this.A = b.a(8);
                    intent3.putExtra("outputFilePath", h.a(this.A, 2).getAbsolutePath());
                    startActivityForResult(intent3, 13);
                    return;
                }
                return;
            case 13:
                if (i3 == -1) {
                    this.x.a((Object) ("resultCode=" + i3));
                    if (!a.a((CharSequence) B)) {
                        File file = new File(com.immomo.momo.a.i(), B);
                        if (file.exists()) {
                            file.delete();
                        }
                        B = PoiTypeDef.All;
                    }
                    this.x.a((Object) ("avatorGUID=" + this.A));
                    if (this.A != null && (a2 = h.a(this.A, 2)) != null && a2.exists()) {
                        this.x.a((Object) ("avatorFile=" + a2));
                        Bitmap l2 = a.l(a2.getAbsolutePath());
                        if (!(l2 == null || (a3 = a.a(l2, 150.0f, true)) == null)) {
                            h.a(this.A, a3, 3, false);
                            l2.recycle();
                            this.n.ae = new String[]{this.A};
                            if (this.s != null) {
                                this.s.a(a3);
                            }
                        }
                        this.A = null;
                        return;
                    }
                    return;
                } else if (i3 == 1003) {
                    com.immomo.momo.util.ao.d(R.string.cropimage_error_size);
                    return;
                } else if (i3 == 1000) {
                    com.immomo.momo.util.ao.d(R.string.cropimage_error_other);
                    return;
                } else if (i3 == 1001) {
                    com.immomo.momo.util.ao.d(R.string.cropimage_error_store);
                    return;
                } else if (i3 == 1002) {
                    com.immomo.momo.util.ao.d(R.string.cropimage_error_filenotfound);
                    return;
                } else {
                    return;
                }
            default:
                return;
        }
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_back /*2131165286*/:
                k();
                return;
            case R.id.btn_login /*2131165287*/:
            case R.id.btn_register /*2131165288*/:
            default:
                return;
            case R.id.btn_ok /*2131165289*/:
                b(false);
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_register);
        this.n = new bf();
        this.h = getIntent().getStringExtra("alipay_user_id");
        this.y = (Button) findViewById(R.id.btn_ok);
        this.z = (Button) findViewById(R.id.btn_back);
        this.v = (HeaderLayout) findViewById(R.id.layout_header);
        this.w = (TextView) g.o().inflate((int) R.layout.include_headerbar_righttext, (ViewGroup) null);
        this.v.a(this.w);
        this.u = (ViewFlipper) findViewById(R.id.rg_viewflipper);
        b(true);
        String I = g.I();
        if (I == null || PoiTypeDef.All.equals(I)) {
            i();
            this.y.setEnabled(false);
        }
        this.y.setOnClickListener(this);
        this.z.setOnClickListener(this);
        this.k = getIntent().getIntArrayExtra("registInterfacetype");
        this.i = getIntent().getIntExtra("timestamp", 0);
        this.j = getIntent().getLongExtra("starttime", System.currentTimeMillis());
        if (this.k != null && this.k.length == 1) {
            this.p.h();
        }
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 != 4) {
            return super.onKeyDown(i2, keyEvent);
        }
        k();
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.ak.a(java.lang.String, java.lang.Integer):int
     arg types: [java.lang.String, int]
     candidates:
      com.immomo.momo.util.ak.a(android.content.Context, java.lang.String):com.immomo.momo.util.ak
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Long):long
      com.immomo.momo.util.ak.a(java.lang.String, int):void
      com.immomo.momo.util.ak.a(java.lang.String, long):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.String):void
      com.immomo.momo.util.ak.a(java.lang.String, boolean):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Boolean):boolean
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Integer):int */
    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Bundle bundle) {
        super.onRestoreInstanceState(bundle);
        this.x.a((Object) "onRestoreInstanceState~~~~~~~~~~~~");
        ak a2 = ak.a(getApplicationContext(), "tmp_reg");
        if (!a.a((CharSequence) a2.b("email", PoiTypeDef.All))) {
            this.n.G = a2.b("email", PoiTypeDef.All);
        }
        if (!a.a((CharSequence) a2.b("token", PoiTypeDef.All))) {
            this.l = a2.b("token", PoiTypeDef.All);
        }
        if (!a.a((CharSequence) a2.b("password", PoiTypeDef.All))) {
            this.n.f3011a = a2.b("password", PoiTypeDef.All);
        }
        if (!a.a((CharSequence) a2.b("name", PoiTypeDef.All))) {
            this.n.i = a2.b("name", PoiTypeDef.All);
        }
        if (!a.a((CharSequence) a2.b("sex", PoiTypeDef.All))) {
            this.n.H = a2.b("sex", PoiTypeDef.All);
        }
        this.i = a2.a("timestamp", (Integer) 0);
        this.j = a2.a("starttime", Long.valueOf(System.currentTimeMillis()));
        if (a2.a("age", (Integer) 0) > 0) {
            this.n.I = a2.a("age", (Integer) 0);
        }
        if (!a.a((CharSequence) a2.b("birthday", PoiTypeDef.All))) {
            this.n.J = a2.b("birthday", PoiTypeDef.All);
        }
        if (!a.a((CharSequence) a2.b("alipay_user_id", PoiTypeDef.All))) {
            this.h = a2.b("alipay_user_id", PoiTypeDef.All);
        }
        if (!a.a((CharSequence) a2.b("avatorGUID", PoiTypeDef.All))) {
            this.A = a2.b("avatorGUID", PoiTypeDef.All);
        }
        if (!a.a((CharSequence) a2.b("camera_filename", PoiTypeDef.All))) {
            B = a2.b("camera_filename", PoiTypeDef.All);
        }
        if (!a.a((CharSequence) a2.b("inviterid", PoiTypeDef.All))) {
            this.n.s = a2.b("inviterid", PoiTypeDef.All);
        }
        if (!a.a((CharSequence) a2.b("photos", PoiTypeDef.All))) {
            try {
                this.n.ae = a.b(a2.b("photos", PoiTypeDef.All), ",");
            } catch (Exception e) {
            }
        }
        int a3 = a2.a("index", (Integer) 0);
        this.x.a((Object) ("pre step index=" + a3));
        for (int i2 = this.m; i2 < a3; i2++) {
            this.x.a((Object) ("renext...index=" + this.m));
            b(true);
        }
        a2.b();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.immomo.momo.util.ak.a(android.content.Context, java.lang.String):com.immomo.momo.util.ak
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Integer):int
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Long):long
      com.immomo.momo.util.ak.a(java.lang.String, int):void
      com.immomo.momo.util.ak.a(java.lang.String, long):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.String):void
      com.immomo.momo.util.ak.a(java.lang.String, boolean):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Boolean):boolean
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.Integer]
     candidates:
      com.immomo.momo.util.ak.a(android.content.Context, java.lang.String):com.immomo.momo.util.ak
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Integer):int
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Long):long
      com.immomo.momo.util.ak.a(java.lang.String, int):void
      com.immomo.momo.util.ak.a(java.lang.String, long):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.String):void
      com.immomo.momo.util.ak.a(java.lang.String, boolean):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Boolean):boolean
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.Long]
     candidates:
      com.immomo.momo.util.ak.a(android.content.Context, java.lang.String):com.immomo.momo.util.ak
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Integer):int
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Long):long
      com.immomo.momo.util.ak.a(java.lang.String, int):void
      com.immomo.momo.util.ak.a(java.lang.String, long):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.String):void
      com.immomo.momo.util.ak.a(java.lang.String, boolean):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Boolean):boolean
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void */
    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        ak a2 = ak.a(getApplicationContext(), "tmp_reg");
        a2.b();
        if (!a.a((CharSequence) this.l)) {
            a2.a("token", (Object) this.l);
        }
        if (!a.a((CharSequence) this.n.G)) {
            a2.a("email", (Object) this.n.G);
        }
        if (!a.a((CharSequence) this.n.f3011a)) {
            a2.a("password", (Object) this.n.f3011a);
        }
        if (!a.a((CharSequence) this.n.i)) {
            a2.a("name", (Object) this.n.i);
        }
        if (!a.a((CharSequence) this.n.H)) {
            a2.a("sex", (Object) this.n.H);
        }
        if (this.n.I > 0) {
            a2.a("age", (Object) Integer.valueOf(this.n.I));
        }
        if (!a.a((CharSequence) this.n.J)) {
            a2.a("birthday", (Object) this.n.J);
        }
        if (!a.a((CharSequence) this.A)) {
            a2.a("avatorGUID", (Object) this.A);
        }
        if (!a.a((CharSequence) B)) {
            a2.a("camera_filename", (Object) B);
        }
        if (!a.a((CharSequence) this.h)) {
            a2.a("alipay_user_id", (Object) this.h);
        }
        a2.a("timestamp", (Object) Integer.valueOf(this.i));
        a2.a("starttime", (Object) Long.valueOf(this.j));
        if (this.m >= 0) {
            a2.a("index", (Object) Integer.valueOf(this.m));
        }
        if (a.f(this.n.s)) {
            a2.a("inviterid", (Object) this.n.s);
        }
        if (this.n.ae != null && this.n.ae.length > 0) {
            a2.a("photos", (Object) a.a(this.n.ae, ","));
        }
        this.x.a((Object) "onSaveInstanceState~~~~~~~~~~~~~~~~");
    }

    /* access modifiers changed from: protected */
    public final MomoApplication t() {
        return (MomoApplication) getApplication();
    }
}
