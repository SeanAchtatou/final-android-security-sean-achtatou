package com.immomo.momo.android.activity.event;

import android.content.Context;
import com.immomo.momo.android.c.b;
import com.immomo.momo.protocol.a.j;
import com.immomo.momo.service.bean.aa;
import com.immomo.momo.service.bean.ae;
import com.immomo.momo.service.r;
import java.util.List;

final class w extends b {
    private ae c;
    private /* synthetic */ EventFeedProfileActivity d;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public w(EventFeedProfileActivity eventFeedProfileActivity, Context context) {
        super(context);
        this.d = eventFeedProfileActivity;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        this.c = (ae) objArr[0];
        return j.a().f(this.c.k);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        String str = (String) obj;
        a(str);
        this.d.m.c(this.c);
        EventFeedProfileActivity eventFeedProfileActivity = this.d;
        aa i = this.d.l;
        int i2 = i.g - 1;
        i.g = i2;
        EventFeedProfileActivity.a(eventFeedProfileActivity, i2);
        r unused = this.d.n;
        String a2 = this.d.j;
        ae aeVar = this.c;
        List b = r.b(a2);
        if (b != null) {
            b.remove(aeVar);
            r.b(a2, b);
        }
        super.a((Object) str);
    }
}
