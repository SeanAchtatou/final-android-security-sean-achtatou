package com.immomo.momo.android.a;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.b.a;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.ImageBrowserActivity;
import com.immomo.momo.android.activity.OtherProfileActivity;
import com.immomo.momo.android.activity.emotestore.EmotionProfileActivity;
import com.immomo.momo.android.activity.tieba.PublishTieCommentActivity;
import com.immomo.momo.android.activity.tieba.TieDetailActivity;
import com.immomo.momo.android.view.HandyListView;
import com.immomo.momo.android.view.a.o;
import com.immomo.momo.b;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.aj;
import com.immomo.momo.service.bean.c.c;
import com.immomo.momo.util.j;
import com.immomo.momo.util.k;
import com.immomo.momo.util.m;
import java.util.List;

public final class gp extends a implements View.OnClickListener {
    private HandyListView d = null;
    private Activity e;

    public gp(Activity activity, List list, HandyListView handyListView) {
        super(activity, list);
        new m("test_momo", "[ -- FriendFeedAdapter -- ]");
        this.d = handyListView;
        this.e = activity;
    }

    static /* synthetic */ void a(gp gpVar, c cVar) {
        Intent intent = new Intent(gpVar.e, PublishTieCommentActivity.class);
        intent.putExtra("tiezi_id", cVar.j);
        intent.putExtra("tiezi_name", cVar.m);
        intent.putExtra("tiecommen_tousercontent", cVar.d);
        intent.putExtra("tiecommen_tousername", cVar.b.i);
        intent.putExtra("tiecomment_tomomoid", cVar.c);
        intent.putExtra("tiecommen_tocommentid", cVar.f3018a);
        gpVar.e.startActivity(intent);
    }

    static /* synthetic */ void b(gp gpVar, c cVar) {
        Intent intent = new Intent(gpVar.e, TieDetailActivity.class);
        intent.putExtra("key_tieid", cVar.j);
        gpVar.e.startActivity(intent);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup, int, boolean, boolean, int):void
     arg types: [com.immomo.momo.service.bean.c.c, android.widget.ImageView, com.immomo.momo.android.view.HandyListView, int, int, int, int]
     candidates:
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, int, android.view.View, android.view.ViewGroup, boolean, int, boolean):com.immomo.momo.android.c.o
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup, int, boolean, boolean, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, com.immomo.momo.android.view.HandyListView):void
     arg types: [com.immomo.momo.service.bean.c.g, android.widget.ImageView, com.immomo.momo.android.view.HandyListView]
     candidates:
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, int):void
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup):void
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, com.immomo.momo.android.view.HandyListView):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.b.a.a(java.util.Date, boolean):java.lang.String
     arg types: [java.util.Date, int]
     candidates:
      android.support.v4.b.a.a(android.graphics.Bitmap, float):android.graphics.Bitmap
      android.support.v4.b.a.a(android.graphics.Bitmap, java.io.File):android.graphics.Bitmap
      android.support.v4.b.a.a(int, int):java.lang.String
      android.support.v4.b.a.a(java.util.Collection, java.lang.String):java.lang.String
      android.support.v4.b.a.a(java.lang.Object[], java.lang.String):java.lang.String
      android.support.v4.b.a.a(java.lang.String, java.lang.String):org.json.JSONObject
      android.support.v4.b.a.a(float, int[]):void
      android.support.v4.b.a.a(android.app.Activity, int):void
      android.support.v4.b.a.a(android.view.View, java.lang.Runnable):void
      android.support.v4.b.a.a(java.io.File, java.io.File):void
      android.support.v4.b.a.a(java.io.File, java.util.zip.ZipInputStream):void
      android.support.v4.b.a.a(java.lang.Object, java.lang.StringBuilder):void
      android.support.v4.b.a.a(android.content.Context, java.lang.String):boolean
      android.support.v4.b.a.a(com.immomo.a.a.d.b, com.immomo.momo.service.bean.Message):boolean
      android.support.v4.b.a.a(byte[], java.lang.String):byte[]
      android.support.v4.b.a.a(byte[], byte[]):byte[]
      android.support.v4.b.a.a(int[], boolean):byte[]
      android.support.v4.b.a.a(byte[], boolean):int[]
      android.support.v4.b.a.a(int, float):void
      android.support.v4.b.a.a(java.lang.Object, com.immomo.a.a.f):void
      android.support.v4.view.bf.a(int, float):void
      com.immomo.a.a.f.a(java.lang.Object, com.immomo.a.a.f):void
      android.support.v4.b.a.a(java.util.Date, boolean):java.lang.String */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.b.a(java.lang.String, boolean):android.graphics.Bitmap
     arg types: [java.lang.String, int]
     candidates:
      com.immomo.momo.b.a(java.lang.String, android.graphics.Bitmap):android.graphics.Bitmap
      com.immomo.momo.b.a(java.lang.String, boolean):android.graphics.Bitmap */
    public final View getView(int i, View view, ViewGroup viewGroup) {
        gr grVar;
        if (view == null) {
            view = g.o().inflate((int) R.layout.listitem_tieba_mycomment, (ViewGroup) null);
            gr grVar2 = new gr((byte) 0);
            view.setTag(R.id.tag_userlist_item, grVar2);
            grVar2.f846a = (TextView) view.findViewById(R.id.tv_comment_time);
            grVar2.b = (TextView) view.findViewById(R.id.tv_comment_content);
            grVar2.c = (TextView) view.findViewById(R.id.tv_comment_replycontent);
            grVar2.d = (ImageView) view.findViewById(R.id.iv_comment_emotion);
            grVar2.e = (ImageView) view.findViewById(R.id.iv_comment_photo);
            grVar2.f = (TextView) view.findViewById(R.id.tv_comment_name);
            grVar2.g = view.findViewById(R.id.layout_comment_content);
            grVar2.i = view.findViewById(R.id.layout_comment_userinfo);
            grVar2.k = (TextView) grVar2.i.findViewById(R.id.userlist_item_tv_age);
            grVar2.j = (ImageView) grVar2.i.findViewById(R.id.userlist_item_iv_gender);
            grVar2.h = grVar2.i.findViewById(R.id.userlist_item_layout_genderbackgroud);
            grVar2.r = (ImageView) grVar2.i.findViewById(R.id.userlist_item_pic_iv_group_role);
            grVar2.l = (ImageView) grVar2.i.findViewById(R.id.userlist_item_pic_iv_weibo);
            grVar2.m = (ImageView) grVar2.i.findViewById(R.id.userlist_item_pic_iv_txweibo);
            grVar2.n = (ImageView) grVar2.i.findViewById(R.id.userlist_item_pic_iv_renren);
            grVar2.o = (ImageView) grVar2.i.findViewById(R.id.userlist_item_pic_iv_vip);
            grVar2.p = (ImageView) grVar2.i.findViewById(R.id.userlist_item_pic_iv_multipic);
            grVar2.q = (ImageView) grVar2.i.findViewById(R.id.userlist_item_pic_iv_industry);
            view.findViewById(R.id.userlist_tv_timedriver);
            grVar2.e.setOnClickListener(this);
            grVar2.g.setOnClickListener(this);
            grVar2.d.setOnClickListener(this);
            grVar = grVar2;
        } else {
            grVar = (gr) view.getTag(R.id.tag_userlist_item);
        }
        c cVar = (c) getItem(i);
        grVar.e.setTag(R.id.tag_item_position, Integer.valueOf(i));
        grVar.g.setTag(R.id.tag_item_position, Integer.valueOf(i));
        grVar.d.setTag(R.id.tag_item_position, Integer.valueOf(i));
        if (a.f(cVar.getLoadImageId())) {
            grVar.d.setVisibility(0);
            j.a((aj) cVar, grVar.d, (ViewGroup) this.d, 15, false, false, 0);
        } else {
            grVar.d.setVisibility(8);
        }
        j.a((aj) cVar.b, grVar.e, this.d);
        grVar.f846a.setText(a.a(cVar.g, true));
        grVar.b.setText(cVar.d);
        grVar.c.setVisibility(0);
        grVar.c.setText(cVar.n);
        com.immomo.momo.service.bean.c.g gVar = cVar.b;
        grVar.f.setText(gVar.h());
        if (gVar.b()) {
            grVar.f.setTextColor(g.c((int) R.color.font_vip_name));
        } else {
            grVar.f.setTextColor(g.c((int) R.color.font_value));
        }
        grVar.k.setText(new StringBuilder(String.valueOf(gVar.I)).toString());
        grVar.f.setText(gVar.h());
        if (gVar.b()) {
            grVar.f.setTextColor(g.c((int) R.color.font_vip_name));
        } else {
            grVar.f.setTextColor(g.c((int) R.color.text_color));
        }
        if ("F".equalsIgnoreCase(gVar.H)) {
            grVar.h.setBackgroundResource(R.drawable.bg_gender_famal);
            grVar.j.setImageResource(R.drawable.ic_user_famale);
            grVar.i.setVisibility(0);
        } else if ("M".equalsIgnoreCase(gVar.H)) {
            grVar.h.setBackgroundResource(R.drawable.bg_gender_male);
            grVar.j.setImageResource(R.drawable.ic_user_male);
            grVar.i.setVisibility(0);
        } else {
            grVar.i.setVisibility(8);
        }
        if (gVar.j()) {
            grVar.p.setVisibility(0);
        } else {
            grVar.p.setVisibility(8);
        }
        if (gVar.aJ == 1 || gVar.aJ == 3) {
            grVar.r.setVisibility(0);
            grVar.r.setImageResource(gVar.aJ == 1 ? R.drawable.ic_userinfo_groupowner : R.drawable.ic_userinfo_group);
        } else {
            grVar.r.setVisibility(8);
        }
        if (gVar.an) {
            grVar.l.setVisibility(0);
            grVar.l.setImageResource(gVar.ap ? R.drawable.ic_userinfo_weibov : R.drawable.ic_userinfo_weibo);
        } else {
            grVar.l.setVisibility(8);
        }
        if (gVar.at) {
            grVar.m.setVisibility(0);
            grVar.m.setImageResource(gVar.au ? R.drawable.ic_userinfo_tweibov : R.drawable.ic_userinfo_tweibo);
        } else {
            grVar.m.setVisibility(8);
        }
        if (gVar.ar) {
            grVar.n.setVisibility(0);
        } else {
            grVar.n.setVisibility(8);
        }
        if (gVar.b()) {
            grVar.o.setVisibility(0);
            if (gVar.c()) {
                grVar.o.setImageResource(R.drawable.ic_userinfo_vip_year);
            } else {
                grVar.o.setImageResource(R.drawable.ic_userinfo_vip);
            }
        } else {
            grVar.o.setVisibility(8);
        }
        if (!a.a((CharSequence) gVar.M)) {
            grVar.q.setVisibility(0);
            grVar.q.setImageBitmap(b.a(gVar.M, true));
        } else {
            grVar.q.setVisibility(8);
        }
        j.a(gVar, grVar.e, this.d, 3);
        return view;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public final void onClick(View view) {
        int intValue = ((Integer) view.getTag(R.id.tag_item_position)).intValue();
        switch (view.getId()) {
            case R.id.iv_comment_photo /*2131166104*/:
                Intent intent = new Intent(this.e, OtherProfileActivity.class);
                intent.putExtra("momoid", ((c) getItem(intValue)).c);
                this.e.startActivity(intent);
                return;
            case R.id.layout_comment_content /*2131166667*/:
                c cVar = (c) getItem(intValue);
                String[] strArr = cVar.o == 1 ? new String[]{"回复评论", "查看话题"} : new String[]{"查看话题"};
                o oVar = new o(this.e, strArr);
                oVar.a(new gq(this, strArr, cVar));
                oVar.show();
                return;
            case R.id.iv_comment_emotion /*2131166669*/:
                new k("C", "C402").e();
                String str = ((c) getItem(intValue)).s;
                if (a.f(str)) {
                    Intent intent2 = new Intent(this.e, EmotionProfileActivity.class);
                    intent2.putExtra("eid", str);
                    this.e.startActivity(intent2);
                    return;
                }
                Intent intent3 = new Intent(this.e, ImageBrowserActivity.class);
                intent3.putExtra("array", new String[]{((c) getItem(intValue)).getLoadImageId()});
                intent3.putExtra("imagetype", "feed");
                intent3.putExtra("autohide_header", true);
                this.e.startActivity(intent3);
                if (this.e.getParent() != null) {
                    this.e.getParent().overridePendingTransition(R.anim.zoom_enter, R.anim.normal);
                    return;
                } else {
                    this.e.overridePendingTransition(R.anim.zoom_enter, R.anim.normal);
                    return;
                }
            default:
                return;
        }
    }
}
