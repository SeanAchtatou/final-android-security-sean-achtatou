package com.immomo.momo.android.activity.tieba;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import com.immomo.a.a.f.a;
import com.immomo.momo.R;
import com.immomo.momo.android.a.in;
import com.immomo.momo.android.activity.ah;
import com.immomo.momo.android.view.LoadingButton;
import com.immomo.momo.android.view.MomoRefreshListView;
import com.immomo.momo.android.view.bi;
import com.immomo.momo.android.view.bl;
import com.immomo.momo.android.view.bu;
import com.immomo.momo.android.view.cv;
import com.immomo.momo.service.ao;
import com.immomo.momo.service.bean.c.d;
import com.immomo.momo.util.u;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class TiebaCategoryDetailActivity extends ah implements AdapterView.OnItemClickListener, bl, bu, cv {
    public static String h = "tieba_title";
    public static String i = "tieba_id";
    public static String j = "tieba_count";
    /* access modifiers changed from: private */
    public MomoRefreshListView k = null;
    /* access modifiers changed from: private */
    public Date l = null;
    /* access modifiers changed from: private */
    public in m = null;
    /* access modifiers changed from: private */
    public ao n;
    /* access modifiers changed from: private */
    public df o;
    /* access modifiers changed from: private */
    public de p;
    /* access modifiers changed from: private */
    public String q;
    /* access modifiers changed from: private */
    public String r;
    /* access modifiers changed from: private */
    public String s;
    private int t;
    /* access modifiers changed from: private */
    public LoadingButton u;
    /* access modifiers changed from: private */
    public Set v = new HashSet();

    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        super.a(bundle);
        setContentView((int) R.layout.activity_tiebacategorydetail);
        this.k = (MomoRefreshListView) findViewById(R.id.listview);
        this.k.setTimeEnable(true);
        this.k.setCompleteScrollTop(false);
        this.k.setEnableLoadMoreFoolter(true);
        this.u = this.k.getFooterViewButton();
        d();
        this.k.setOnPullToRefreshListener$42b903f6(this);
        this.k.setOnCancelListener$135502(this);
        this.u.setOnProcessListener(this);
        this.k.setOnItemClickListener(this);
        m().a(new bi(this).a((int) R.drawable.ic_topbar_search), new dd(this));
    }

    public final void b_() {
        b(new df(this, this));
    }

    /* access modifiers changed from: protected */
    public final void d() {
        super.d();
        this.n = new ao();
        this.l = this.g.b("categorydetail_latttime_reflush");
        if (this.l != null) {
            this.k.setLastFlushTime(this.l);
        }
        this.q = getIntent().getExtras().getString(i);
        this.r = getIntent().getExtras().getString(h);
        this.t = getIntent().getExtras().getInt(j);
        if (a.a(this.q) || a.a(this.r)) {
            finish();
        }
        ao aoVar = this.n;
        String str = this.q;
        List<d> arrayList = u.c(new StringBuilder("tiebacatogrylists").append(str).toString()) ? (List) u.b("tiebacatogrylists" + str) : new ArrayList<>();
        this.m = new in(this, arrayList, this.k);
        this.k.setAdapter((ListAdapter) this.m);
        this.m.c();
        for (d add : arrayList) {
            this.v.add(add);
        }
        if (this.m.getCount() < 20) {
            this.u.setVisibility(8);
        }
        this.k.l();
        this.s = String.valueOf(this.r) + "(" + this.t + ")";
        setTitle(this.s);
    }

    public void onItemClick(AdapterView adapterView, View view, int i2, long j2) {
        if (i2 <= this.m.getCount()) {
            Intent intent = new Intent(this, TiebaProfileActivity.class);
            intent.putExtra("tiebaid", ((d) this.m.getItem(i2)).f3019a);
            startActivity(intent);
        }
    }

    public final void u() {
        b(new de(this, this));
    }

    public final void v() {
        this.k.n();
        if (this.o != null) {
            this.o.cancel(true);
            this.o = null;
        }
    }
}
