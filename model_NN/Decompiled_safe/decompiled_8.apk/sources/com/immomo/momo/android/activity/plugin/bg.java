package com.immomo.momo.android.activity.plugin;

import android.graphics.Bitmap;
import com.immomo.momo.R;

final class bg implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ bf f2061a;
    private final /* synthetic */ Bitmap b;

    bg(bf bfVar, Bitmap bitmap) {
        this.f2061a = bfVar;
        this.b = bitmap;
    }

    public final void run() {
        if (this.b != null) {
            this.f2061a.f2060a.o.setEnabled(true);
            this.f2061a.f2060a.o.setImageBitmap(this.b);
            return;
        }
        this.f2061a.f2060a.o.setEnabled(false);
        this.f2061a.f2060a.o.setImageResource(R.drawable.ic_common_def_header);
    }
}
