package com.immomo.momo.android.a;

import android.content.Context;
import android.support.v4.b.a;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.service.m;
import java.util.List;

public final class bk extends a {
    public bk(Context context, List list) {
        super(context, list);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.b.a.a(java.util.Date, boolean):java.lang.String
     arg types: [java.util.Date, int]
     candidates:
      android.support.v4.b.a.a(android.graphics.Bitmap, float):android.graphics.Bitmap
      android.support.v4.b.a.a(android.graphics.Bitmap, java.io.File):android.graphics.Bitmap
      android.support.v4.b.a.a(int, int):java.lang.String
      android.support.v4.b.a.a(java.util.Collection, java.lang.String):java.lang.String
      android.support.v4.b.a.a(java.lang.Object[], java.lang.String):java.lang.String
      android.support.v4.b.a.a(java.lang.String, java.lang.String):org.json.JSONObject
      android.support.v4.b.a.a(float, int[]):void
      android.support.v4.b.a.a(android.app.Activity, int):void
      android.support.v4.b.a.a(android.view.View, java.lang.Runnable):void
      android.support.v4.b.a.a(java.io.File, java.io.File):void
      android.support.v4.b.a.a(java.io.File, java.util.zip.ZipInputStream):void
      android.support.v4.b.a.a(java.lang.Object, java.lang.StringBuilder):void
      android.support.v4.b.a.a(android.content.Context, java.lang.String):boolean
      android.support.v4.b.a.a(com.immomo.a.a.d.b, com.immomo.momo.service.bean.Message):boolean
      android.support.v4.b.a.a(byte[], java.lang.String):byte[]
      android.support.v4.b.a.a(byte[], byte[]):byte[]
      android.support.v4.b.a.a(int[], boolean):byte[]
      android.support.v4.b.a.a(byte[], boolean):int[]
      android.support.v4.b.a.a(int, float):void
      android.support.v4.b.a.a(java.lang.Object, com.immomo.a.a.f):void
      android.support.v4.view.bf.a(int, float):void
      com.immomo.a.a.f.a(java.lang.Object, com.immomo.a.a.f):void
      android.support.v4.b.a.a(java.util.Date, boolean):java.lang.String */
    public final View getView(int i, View view, ViewGroup viewGroup) {
        bl blVar;
        if (view == null) {
            view = a((int) R.layout.listitem_publishdraft);
            blVar = new bl();
            view.setTag(blVar);
            blVar.f738a = (TextView) view.findViewById(R.id.draftpublish_tv_content);
            blVar.d = (TextView) view.findViewById(R.id.draftpublish_tv_status);
            blVar.c = (TextView) view.findViewById(R.id.draftpublish_tv_time);
            blVar.b = (TextView) view.findViewById(R.id.draftpublish_tv_type);
        } else {
            blVar = (bl) view.getTag();
        }
        m mVar = (m) getItem(i);
        blVar.f738a.setText(mVar.e);
        blVar.c.setText(a.a(mVar.d, false));
        if (mVar.c == 2) {
            blVar.d.setText(mVar.g);
        } else if (mVar.c == 1) {
            blVar.d.setText("发送中");
        } else if (mVar.c == 3) {
            blVar.d.setText("发送成功");
        } else {
            blVar.d.setText(PoiTypeDef.All);
        }
        if (mVar.f == 2) {
            blVar.b.setText("留言板");
        } else if (mVar.f == 3) {
            blVar.b.setText("群空间");
        } else if (mVar.f == 1) {
            blVar.b.setText("陌陌吧话题");
        } else if (mVar.f == 4) {
            blVar.b.setText("陌陌吧话题");
        } else {
            blVar.b.setText(PoiTypeDef.All);
        }
        return view;
    }
}
