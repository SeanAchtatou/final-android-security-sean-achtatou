package com.immomo.momo.android.activity.maintab;

import android.content.Intent;
import android.view.View;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.ImageBrowserActivity;

final class ce implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ cd f1877a;

    ce(cd cdVar) {
        this.f1877a = cdVar;
    }

    public final void onClick(View view) {
        this.f1877a.L.a((Object) "avatar cover clickded");
        int intValue = ((Integer) view.getTag(R.id.tag_item_position)).intValue();
        if (intValue >= 0 && intValue < this.f1877a.M.ae.length) {
            String[] strArr = this.f1877a.M.ae;
            Intent intent = new Intent(this.f1877a.c(), ImageBrowserActivity.class);
            intent.putExtra("array", strArr);
            intent.putExtra("imagetype", "avator");
            intent.putExtra("index", intValue);
            this.f1877a.a(intent);
            this.f1877a.b((int) R.anim.normal);
        }
    }
}
