package com.immomo.momo.android.activity.plugin;

import android.net.Uri;
import android.os.Bundle;
import android.webkit.WebView;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.ah;
import com.immomo.momo.android.view.HeaderLayout;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.g;
import com.immomo.momo.util.ao;

public class BindFaceBookActivity extends ah {
    private HeaderLayout h = null;
    /* access modifiers changed from: private */
    public String i = "http://test.wdy/seccess";
    /* access modifiers changed from: private */
    public WebView j;
    private TextView k;
    /* access modifiers changed from: private */
    public v l = null;
    /* access modifiers changed from: private */
    public String m = "XXX-XXXX-XXXX-XXXX";
    /* access modifiers changed from: private */
    public String n;
    private i o;

    static /* synthetic */ void g(BindFaceBookActivity bindFaceBookActivity) {
        bindFaceBookActivity.e.a((Object) ("[bindFacebook]facebookUserID" + ((String) null) + " requestToken:" + ((String) null) + " requestTokenSecret:" + ((String) null)));
        ao.a((CharSequence) "FaceBook账号绑定成功");
        g.q();
        bindFaceBookActivity.setResult(-1, bindFaceBookActivity.getIntent());
        bindFaceBookActivity.finish();
    }

    static /* synthetic */ void u() {
    }

    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        super.a(bundle);
        setContentView((int) R.layout.activity_bind_api);
        g.q();
        this.j = (WebView) findViewById(R.id.web);
        this.j.getSettings().setJavaScriptEnabled(true);
        this.j.getSettings().setUserAgentString("Android");
        this.j.getSettings().setAppCacheEnabled(false);
        this.j.getSettings().setDatabaseEnabled(false);
        this.j.getSettings().setSavePassword(false);
        this.j.setWebViewClient(new k(this, (byte) 0));
        this.h = (HeaderLayout) findViewById(R.id.layout_header);
        this.h.setTitleText("Binding FaceBook");
        this.k = (TextView) findViewById(R.id.header_stv_title);
        this.k.setFocusableInTouchMode(false);
        this.e.b();
        this.o = new i(this, this);
        this.o.execute(new Object[0]);
    }

    /* access modifiers changed from: protected */
    public final String[] c(String str) {
        String replace = str.replace("#", "?");
        this.e.b((Object) ("parseAccessToken:" + replace));
        Uri parse = Uri.parse(replace);
        String[] strArr = {parse.getQueryParameter("access_token"), parse.getQueryParameter("expires_in"), parse.getQueryParameter("code")};
        this.e.b((Object) ("access_token:" + strArr[0] + " expires_in:" + strArr[1]));
        return strArr;
    }

    /* access modifiers changed from: protected */
    public final void d() {
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.l = null;
        this.o.cancel(true);
        super.onDestroy();
    }
}
