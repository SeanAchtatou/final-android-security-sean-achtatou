package com.immomo.momo.android.activity.tieba;

import android.content.Intent;
import android.view.View;
import android.widget.ExpandableListView;
import com.immomo.momo.android.activity.OtherProfileActivity;
import com.immomo.momo.service.bean.bc;
import com.immomo.momo.service.bean.c.g;

final class eq implements ExpandableListView.OnChildClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ TiebaMemberListActivity f2268a;

    eq(TiebaMemberListActivity tiebaMemberListActivity) {
        this.f2268a = tiebaMemberListActivity;
    }

    public final boolean onChildClick(ExpandableListView expandableListView, View view, int i, int i2, long j) {
        Intent intent = new Intent(this.f2268a, OtherProfileActivity.class);
        intent.putExtra("momoid", ((g) ((bc) this.f2268a.l.get(i)).f3008a.get(i2)).h);
        this.f2268a.startActivity(intent);
        return false;
    }
}
