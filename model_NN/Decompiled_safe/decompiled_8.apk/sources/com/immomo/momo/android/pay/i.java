package com.immomo.momo.android.pay;

import android.content.Context;
import com.immomo.momo.a.b;
import com.immomo.momo.a.c;
import com.immomo.momo.a.t;
import com.immomo.momo.a.w;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.a;
import com.immomo.momo.service.aq;
import com.immomo.momo.util.ao;

final class i extends d {

    /* renamed from: a  reason: collision with root package name */
    private v f2560a = null;
    private /* synthetic */ BuyMemberActivity c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public i(BuyMemberActivity buyMemberActivity, Context context) {
        super(context);
        this.c = buyMemberActivity;
        this.f2560a = new v(context);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        String a2 = a.a().a(this.c.x, this.c.f, this.c.A ? this.c.z : this.c.y);
        new aq().b(this.c.f);
        return a2;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
        this.f2560a.setCancelable(false);
        this.f2560a.a("请求提交中....");
        this.c.a(this.f2560a);
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        super.a(exc);
        if (!(exc instanceof com.immomo.momo.a.d) && !(exc instanceof c) && !(exc instanceof b) && !(exc instanceof t) && !(exc instanceof w)) {
            BuyMemberActivity.p(this.c);
        }
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        String str = (String) obj;
        if (!com.immomo.a.a.f.a.a(str)) {
            ao.a((CharSequence) str);
        }
        BuyMemberActivity.q(this.c);
        this.c.x();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.c.p();
    }
}
