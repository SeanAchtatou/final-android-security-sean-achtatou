package com.immomo.momo.android.activity;

import android.graphics.Bitmap;
import com.immomo.momo.android.c.g;
import com.immomo.momo.util.m;
import java.io.FileOutputStream;
import java.io.IOException;

final class lf implements g {

    /* renamed from: a  reason: collision with root package name */
    private String f1810a;
    private Bitmap.CompressFormat b;
    private m c = new m(this);

    public lf(String str, Bitmap.CompressFormat compressFormat) {
        this.f1810a = str;
        this.b = compressFormat;
    }

    public final /* synthetic */ void a(Object obj) {
        Bitmap bitmap = (Bitmap) obj;
        if (bitmap == null) {
            this.c.c(String.valueOf(this.f1810a) + " download failed-------------------");
            return;
        }
        FileOutputStream fileOutputStream = null;
        try {
            fileOutputStream = com.immomo.momo.g.c().openFileOutput(this.f1810a, 0);
            bitmap.compress(this.b, 85, fileOutputStream);
            this.c.b((Object) ("save file -> " + this.f1810a));
            if (fileOutputStream != null) {
                try {
                    fileOutputStream.close();
                } catch (IOException e) {
                }
            }
            bitmap.recycle();
        } catch (Exception e2) {
            this.c.a((Throwable) e2);
            if (fileOutputStream != null) {
                try {
                    fileOutputStream.close();
                } catch (IOException e3) {
                }
            }
            bitmap.recycle();
        } catch (Throwable th) {
            if (fileOutputStream != null) {
                try {
                    fileOutputStream.close();
                } catch (IOException e4) {
                }
            }
            bitmap.recycle();
            throw th;
        }
    }
}
