package com.immomo.momo.android.game;

import android.content.Intent;
import android.os.Bundle;
import com.immomo.a.a.f.a;
import com.immomo.momo.android.activity.WelcomeActivity;
import com.immomo.momo.android.activity.ao;

public class AuthorizeActivity extends ao {
    String h;
    String i;
    String j;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (this.f == null) {
            startActivity(new Intent(getApplicationContext(), WelcomeActivity.class));
            Intent intent = new Intent();
            intent.putExtra("emsg", "客户端没有登录");
            setResult(40102, intent);
            finish();
            return;
        }
        this.h = getIntent().getStringExtra("appid");
        this.i = getIntent().getStringExtra("redirecturl");
        this.j = getIntent().getStringExtra("packagename");
        if (a.a(this.h) || a.a(this.i)) {
            Intent intent2 = new Intent();
            intent2.putExtra("emsg", "客户端参数错误");
            setResult(40105, intent2);
            finish();
            return;
        }
        b(new a(this, this));
    }
}
