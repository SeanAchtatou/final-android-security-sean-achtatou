package com.immomo.momo.android.b;

import android.location.Location;
import java.util.List;

final class d extends p {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ a f2323a;
    private final /* synthetic */ List c;
    private final /* synthetic */ String d;
    private final /* synthetic */ String e;
    private final /* synthetic */ Object f;

    d(a aVar, List list, String str, String str2, Object obj) {
        this.f2323a = aVar;
        this.c = list;
        this.d = str;
        this.e = str2;
        this.f = obj;
    }

    public final void a(Location location, int i, int i2, int i3) {
        this.c.add(location);
        if (this.c.size() > 0) {
            try {
                this.f2323a.b.a(this.d);
                this.f2323a.b.a(this.e);
            } catch (Exception e2) {
                this.f2323a.f2312a.a((Throwable) e2);
            }
            synchronized (this.f) {
                this.f.notify();
            }
        }
    }
}
