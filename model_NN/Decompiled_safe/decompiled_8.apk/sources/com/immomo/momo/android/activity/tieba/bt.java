package com.immomo.momo.android.activity.tieba;

import android.content.Context;
import android.support.v4.b.a;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.service.ao;
import com.immomo.momo.service.bean.c.b;
import com.immomo.momo.service.bean.c.c;

final class bt extends d {

    /* renamed from: a  reason: collision with root package name */
    private v f2194a = null;
    private /* synthetic */ PublishTieCommentActivity c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public bt(PublishTieCommentActivity publishTieCommentActivity, Context context) {
        super(context);
        this.c = publishTieCommentActivity;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        c cVar = new c();
        new b().f3017a = this.c.t;
        String a2 = com.immomo.momo.protocol.a.v.a().a(this.c.i.getText().toString().trim(), this.c.E, this.c.F, this.c.C, this.c.s, this.c.r, this.c.t, cVar);
        PublishTieCommentActivity.a(this.c, cVar);
        new ao();
        return a2;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.f2194a = new v(this.c, "请稍候，正在提交...");
        this.f2194a.setOnCancelListener(new bu(this));
        this.c.a(this.f2194a);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        String str = (String) obj;
        this.c.setResult(-1);
        if (a.f(str)) {
            com.immomo.momo.util.ao.b(str);
        }
        this.c.finish();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.c.p();
    }
}
