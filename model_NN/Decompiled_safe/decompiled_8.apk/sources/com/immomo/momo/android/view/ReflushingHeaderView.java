package com.immomo.momo.android.view;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import com.immomo.momo.g;
import com.immomo.momo.util.m;

public class ReflushingHeaderView extends LinearLayout {

    /* renamed from: a  reason: collision with root package name */
    private boolean f2649a;
    private int b;
    private int c;

    public ReflushingHeaderView(Context context) {
        super(context);
        new m(this);
    }

    public ReflushingHeaderView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        new m(this);
    }

    public ReflushingHeaderView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        new m(this);
    }

    public final void a() {
        super.setVisibility(8);
    }

    public int getForcedHeight() {
        return this.b;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        if (this.f2649a) {
            setMeasuredDimension(i, this.b);
            setPadding(0, 0, 0, this.c - this.b);
            return;
        }
        super.onMeasure(i, i2);
    }

    public void setForcedHeight(int i) {
        if (i == 0) {
            this.f2649a = false;
            super.setVisibility(8);
            setPadding(0, 0, 0, 0);
        } else if (this.f2649a && this.b != i) {
            this.b = i;
            requestLayout();
        }
    }

    public void setVisibility(int i) {
        if (i == getVisibility()) {
            super.setVisibility(i);
        } else if (i != 8) {
            super.setVisibility(i);
        } else if (g.a()) {
            this.f2649a = true;
            this.c = getMeasuredHeight();
            ObjectAnimator.ofInt(this, "forcedHeight", this.c, 0).setDuration(500L).start();
        } else {
            super.setVisibility(8);
        }
    }
}
