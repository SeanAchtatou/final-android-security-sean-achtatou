package com.immomo.momo.android.activity.message;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import com.immomo.momo.service.bean.aw;

final class by implements AdapterView.OnItemClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ HiSessionListActivity f1958a;

    by(HiSessionListActivity hiSessionListActivity) {
        this.f1958a = hiSessionListActivity;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        Intent intent = new Intent(this.f1958a, ChatActivity.class);
        intent.putExtra("remoteUserID", ((aw) this.f1958a.q.getItem(i)).d());
        intent.putExtra("from", "from_hiactivity");
        this.f1958a.startActivity(intent);
    }
}
