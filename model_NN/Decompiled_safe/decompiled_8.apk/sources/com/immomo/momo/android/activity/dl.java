package com.immomo.momo.android.activity;

import android.content.DialogInterface;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.view.EmoteEditeText;

final class dl implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ dk f1254a;
    private final /* synthetic */ EmoteEditeText b;

    dl(dk dkVar, EmoteEditeText emoteEditeText) {
        this.f1254a = dkVar;
        this.b = emoteEditeText;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        if (this.b.getText().toString().trim().equals(PoiTypeDef.All)) {
            this.f1254a.f1253a.a((int) R.string.dialog_editprofile_name_empty);
            dialogInterface.dismiss();
            return;
        }
        this.f1254a.f1253a.s.setText(this.b.getText().toString().trim());
    }
}
