package com.immomo.momo.android.a;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.b.a;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.ao;
import com.immomo.momo.android.activity.d;
import com.immomo.momo.android.view.EmoteEditeText;
import com.immomo.momo.android.view.HandyListView;
import com.immomo.momo.android.view.a.n;
import com.immomo.momo.android.view.ce;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.aj;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.service.bean.i;
import com.immomo.momo.service.bean.j;
import com.immomo.momo.service.c;
import com.immomo.momo.util.aq;
import com.immomo.momo.util.k;
import com.immomo.momo.util.m;
import java.util.ArrayList;
import java.util.List;

public final class ae extends ce {
    c d;
    /* access modifiers changed from: private */
    public ao e = null;
    /* access modifiers changed from: private */
    public HandyListView f = null;

    public ae(HandyListView handyListView, ao aoVar) {
        super(aoVar, new ArrayList());
        new m(this);
        this.d = null;
        this.e = aoVar;
        this.f = handyListView;
        this.d = new c();
    }

    static /* synthetic */ void a(ae aeVar, i iVar, j jVar) {
        new k("C", "C5701").e();
        View inflate = g.o().inflate((int) R.layout.dialog_contactpeople_apply, (ViewGroup) null);
        EmoteEditeText emoteEditeText = (EmoteEditeText) inflate.findViewById(R.id.edittext_reason);
        emoteEditeText.addTextChangedListener(new aq(24));
        n a2 = n.a(aeVar.d(), PoiTypeDef.All, new al(aeVar, emoteEditeText, iVar, jVar));
        a2.setTitle("好友验证");
        a2.setContentView(inflate);
        a2.getWindow().setSoftInputMode(4);
        a2.show();
    }

    static /* synthetic */ void a(ae aeVar, j jVar, i iVar) {
        n a2 = n.a(aeVar.e, "屏蔽后对方将被拉入黑名单，确认继续吗？", "确认", "取消", new ak(aeVar, jVar, iVar), (DialogInterface.OnClickListener) null);
        a2.setTitle(aeVar.e.getString(R.string.dialog_title_alert));
        a2.a();
        a2.show();
    }

    static /* synthetic */ void a(ae aeVar, String str, String str2) {
        if (!a.a((CharSequence) str)) {
            Intent intent = new Intent("android.intent.action.SENDTO", Uri.parse("smsto:" + new c().b(str)));
            intent.putExtra("sms_body", str2);
            aeVar.e.startActivity(intent);
        }
    }

    /* access modifiers changed from: private */
    public static boolean c(String str) {
        return str.startsWith("/api/") || str.startsWith("/game/");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup, int, boolean, boolean, int):void
     arg types: [com.immomo.momo.service.bean.bf, android.widget.ImageView, com.immomo.momo.android.view.HandyListView, int, int, int, int]
     candidates:
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, int, android.view.View, android.view.ViewGroup, boolean, int, boolean):com.immomo.momo.android.c.o
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup, int, boolean, boolean, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup, int, boolean, boolean, int):void
     arg types: [com.immomo.momo.service.bean.al, android.widget.ImageView, com.immomo.momo.android.view.HandyListView, int, int, int, int]
     candidates:
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, int, android.view.View, android.view.ViewGroup, boolean, int, boolean):com.immomo.momo.android.c.o
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup, int, boolean, boolean, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup, int, boolean, boolean, int):void
     arg types: [com.immomo.momo.plugin.e.e, android.widget.ImageView, com.immomo.momo.android.view.HandyListView, int, int, int, int]
     candidates:
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, int, android.view.View, android.view.ViewGroup, boolean, int, boolean):com.immomo.momo.android.c.o
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup, int, boolean, boolean, int):void */
    public final View a(int i, View view) {
        boolean z;
        if (view == null) {
            aq aqVar = new aq((byte) 0);
            view = LayoutInflater.from(this.e).inflate((int) R.layout.listitem_contactnotice, (ViewGroup) null);
            aqVar.h = view.findViewById(R.id.layout_command_button);
            aqVar.i[0] = (Button) aqVar.h.findViewById(R.id.button1);
            aqVar.i[1] = (Button) aqVar.h.findViewById(R.id.button2);
            aqVar.i[2] = (Button) aqVar.h.findViewById(R.id.button3);
            aqVar.i[3] = (Button) aqVar.h.findViewById(R.id.button4);
            aqVar.e = (ImageView) view.findViewById(R.id.iv_header_main);
            aqVar.g = (TextView) view.findViewById(R.id.tv_content);
            aqVar.f724a = view.findViewById(R.id.item_layout);
            aqVar.b = view.findViewById(R.id.item_layout_top);
            aqVar.f = (TextView) view.findViewById(R.id.userlist_item_tv_name);
            aqVar.c = (ImageView) view.findViewById(R.id.iv_notice_src);
            aqVar.d = (TextView) view.findViewById(R.id.notice_tv_sourcename);
            view.setTag(R.id.tag_item, aqVar);
        }
        aq aqVar2 = (aq) view.getTag(R.id.tag_item);
        i iVar = (i) getItem(i);
        bf i2 = iVar.i();
        switch (iVar.b()) {
            case 1:
                aqVar2.d.setText("手机联系人");
                aqVar2.c.setImageResource(R.drawable.ic_addfriend_contact);
                break;
            case 2:
                aqVar2.d.setText("新浪微博好友");
                aqVar2.c.setImageResource(R.drawable.ic_setting_weibo);
                break;
            case 3:
                aqVar2.d.setText("腾讯微博好友");
                aqVar2.c.setImageResource(R.drawable.ic_setting_tweibo);
                break;
            case 4:
                aqVar2.d.setText("人人网好友");
                aqVar2.c.setImageResource(R.drawable.ic_setting_renren);
                break;
            case 5:
                aqVar2.d.setText("好友邀请");
                aqVar2.c.setImageBitmap(a.a(g.m(), (float) g.a(8.0f)));
                break;
            case 6:
                aqVar2.d.setText(iVar.d() != null ? "来自" + iVar.d().getAppname() : "游戏");
                com.immomo.momo.util.j.a((aj) iVar.d().appIconLoader(), aqVar2.c, (ViewGroup) this.f, 5, false, true, 3);
                break;
        }
        if (i2 != null) {
            com.immomo.momo.util.j.a((aj) i2, aqVar2.e, (ViewGroup) this.f, 3, false, true, 0);
        } else if (iVar.c() != null) {
            com.immomo.momo.util.j.a((aj) iVar.c(), aqVar2.e, (ViewGroup) this.f, 5, false, true, 3);
        } else {
            aqVar2.e.setImageBitmap(a.a(g.m(), (float) g.a(8.0f)));
        }
        aqVar2.f.setText(iVar.m());
        StringBuilder sb = new StringBuilder();
        if (!a.a((CharSequence) iVar.s())) {
            sb.append("[").append(iVar.s()).append("]");
        }
        aqVar2.g.setText(sb.append(iVar.j()).toString());
        if (iVar.q()) {
            List p = iVar.p();
            int size = p.size();
            aqVar2.h.setVisibility(0);
            boolean z2 = false;
            int dimensionPixelSize = this.e.getResources().getDimensionPixelSize(R.dimen.button_padding);
            int i3 = 0;
            while (i3 < 4) {
                if (i3 < size) {
                    if (!d.a(((j) p.get(i3)).b()) && !c(((j) p.get(i3)).c())) {
                        aqVar2.i[i3].setVisibility(8);
                        z = z2;
                        i3++;
                        z2 = z;
                    } else if (!c(((j) p.get(i3)).c()) || !iVar.g()) {
                        aqVar2.i[i3].setClickable(!((j) p.get(i3)).e() && !this.f.f());
                        aqVar2.i[i3].setEnabled(aqVar2.i[i3].isClickable());
                        aqVar2.i[i3].setText(((j) p.get(i3)).a());
                        aqVar2.i[i3].setVisibility(0);
                        if (((j) p.get(i3)).b().contains("agree")) {
                            aqVar2.i[i3].setBackgroundResource(R.drawable.btn_default_popsubmit);
                        } else {
                            aqVar2.i[i3].setBackgroundResource(R.drawable.btn_default);
                        }
                        aqVar2.i[i3].setPadding(dimensionPixelSize, 0, dimensionPixelSize, 0);
                        if (!this.f.f()) {
                            aqVar2.i[i3].setOnClickListener(new an(this, (j) p.get(i3), iVar));
                            z = z2;
                            i3++;
                            z2 = z;
                        }
                        z = z2;
                        i3++;
                        z2 = z;
                    } else if (!z2) {
                        aqVar2.i[i3].setClickable(false);
                        aqVar2.i[i3].setEnabled(aqVar2.i[i3].isClickable());
                        aqVar2.i[i3].setText("已添加");
                        aqVar2.i[i3].setVisibility(0);
                        aqVar2.i[i3].setBackgroundResource(R.drawable.btn_big_normal_disable);
                        aqVar2.i[i3].setPadding(dimensionPixelSize, 0, dimensionPixelSize, 0);
                        z = true;
                        i3++;
                        z2 = z;
                    }
                }
                aqVar2.i[i3].setVisibility(8);
                z = z2;
                i3++;
                z2 = z;
            }
        } else {
            aqVar2.h.setVisibility(8);
        }
        aqVar2.f724a.setOnLongClickListener(new af(this, i));
        aqVar2.f724a.setOnClickListener(new ag(this, i));
        aqVar2.b.setClickable(!this.f.f());
        aqVar2.b.setOnClickListener(new ah(this, iVar));
        aqVar2.b.setOnLongClickListener(new ai(this, i));
        aqVar2.e.setOnClickListener(new aj(this, iVar));
        aqVar2.e.setClickable(!this.f.f() && (!a.a(iVar.h()) || iVar.c() != null));
        return view;
    }

    public final void a(String str) {
        n a2 = n.a(this.e, "请求已发送成功，该好友可能不能及时收到添加好友消息，是否用短信立即通知他？", "短信通知", "取消", new am(this, str), (DialogInterface.OnClickListener) null);
        a2.setCanceledOnTouchOutside(false);
        a2.show();
    }
}
