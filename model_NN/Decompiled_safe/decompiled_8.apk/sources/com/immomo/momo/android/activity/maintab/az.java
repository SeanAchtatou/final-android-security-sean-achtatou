package com.immomo.momo.android.activity.maintab;

import com.immomo.momo.R;
import com.immomo.momo.g;

final class az implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ay f1849a;

    az(ay ayVar) {
        this.f1849a = ayVar;
    }

    public final void run() {
        this.f1849a.f1848a.T.setLoadingViewText(R.string.pull_to_refresh_refreshing_label);
        if (this.f1849a.f1848a.c() != null) {
            if (g.S()) {
                aq.a(this.f1849a.f1848a, this.f1849a.f1848a.M.U);
            }
            this.f1849a.f1848a.X = new bg(this.f1849a.f1848a, this.f1849a.f1848a.c());
            this.f1849a.f1848a.X.execute(new Object[0]);
        }
    }
}
