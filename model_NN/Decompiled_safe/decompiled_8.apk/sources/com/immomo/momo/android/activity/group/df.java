package com.immomo.momo.android.activity.group;

import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Message;

final class df extends Handler {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ GroupProfileActivity f1603a;

    df(GroupProfileActivity groupProfileActivity) {
        this.f1603a = groupProfileActivity;
    }

    public final void handleMessage(Message message) {
        switch (message.what) {
            case 12:
                String string = message.getData().getString("guid");
                GroupProfileActivity.a(this.f1603a, string, (Bitmap) message.obj);
                break;
        }
        super.handleMessage(message);
    }
}
