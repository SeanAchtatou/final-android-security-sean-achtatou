package com.immomo.momo.android.activity.plugin;

import android.content.Context;
import com.immomo.momo.R;
import com.immomo.momo.a.a;
import com.immomo.momo.android.c.d;
import com.immomo.momo.plugin.d.c;
import com.immomo.momo.service.aq;
import com.immomo.momo.util.ao;

final class aw extends d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ CommunityStatusActivity f2050a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public aw(CommunityStatusActivity communityStatusActivity, Context context) {
        super(context);
        this.f2050a = communityStatusActivity;
        if (communityStatusActivity.u != null) {
            communityStatusActivity.u.cancel(true);
        }
        communityStatusActivity.u = this;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        return com.immomo.momo.protocol.a.d.c(this.f2050a.f.h, null);
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        if (exc instanceof a) {
            super.a(exc);
            return;
        }
        this.b.a((Throwable) exc);
        ao.g(R.string.plus_error_profile);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        c cVar = (c) obj;
        super.a(cVar);
        if (cVar != null) {
            this.f2050a.q = cVar;
            if (!android.support.v4.b.a.a((CharSequence) this.f2050a.q.f2870a)) {
                this.f2050a.k.setText(this.f2050a.q.f2870a);
                this.f2050a.f.r = this.f2050a.q.f2870a;
                new aq().b(this.f2050a.f);
            }
        }
    }
}
