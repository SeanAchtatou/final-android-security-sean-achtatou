package com.immomo.momo.android.activity.feed;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.b.a;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.a.dh;
import com.immomo.momo.android.activity.ImageBrowserActivity;
import com.immomo.momo.android.activity.OtherProfileActivity;
import com.immomo.momo.android.activity.ah;
import com.immomo.momo.android.activity.d;
import com.immomo.momo.android.activity.emotestore.EmotionProfileActivity;
import com.immomo.momo.android.broadcast.h;
import com.immomo.momo.android.c.i;
import com.immomo.momo.android.view.EmoteInputView;
import com.immomo.momo.android.view.EmoteTextView;
import com.immomo.momo.android.view.HandyListView;
import com.immomo.momo.android.view.HeaderLayout;
import com.immomo.momo.android.view.LoadingButton;
import com.immomo.momo.android.view.MGifImageView;
import com.immomo.momo.android.view.MSmallEmoteEditeText;
import com.immomo.momo.android.view.MultiImageView;
import com.immomo.momo.android.view.ResizeListenerLayout;
import com.immomo.momo.android.view.a.at;
import com.immomo.momo.android.view.a.o;
import com.immomo.momo.android.view.au;
import com.immomo.momo.android.view.bi;
import com.immomo.momo.android.view.bl;
import com.immomo.momo.android.view.cb;
import com.immomo.momo.b;
import com.immomo.momo.g;
import com.immomo.momo.service.ai;
import com.immomo.momo.service.aq;
import com.immomo.momo.service.bean.GameApp;
import com.immomo.momo.service.bean.ab;
import com.immomo.momo.service.bean.ac;
import com.immomo.momo.service.bean.ae;
import com.immomo.momo.service.bean.aj;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.service.bean.q;
import com.immomo.momo.service.bean.u;
import com.immomo.momo.util.j;
import com.immomo.momo.util.k;
import java.io.File;
import java.util.Collection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FeedProfileActivity extends ah implements View.OnClickListener, View.OnTouchListener, AdapterView.OnItemClickListener, bl, cb {
    private ImageView A;
    private MultiImageView B;
    /* access modifiers changed from: private */
    public MGifImageView C;
    private ImageView D;
    private TextView E;
    private TextView F;
    /* access modifiers changed from: private */
    public View G;
    private ImageView H;
    private ImageView I;
    private ImageView J;
    private ImageView K;
    private ImageView L;
    private ImageView M;
    private ImageView N;
    private ImageView O;
    private View P;
    private TextView Q;
    private View R;
    private View S;
    private View T;
    private View U;
    private TextView V;
    private TextView W;
    private ImageView X;
    private View Y;
    /* access modifiers changed from: private */
    public MSmallEmoteEditeText Z;
    private ImageView aa;
    /* access modifiers changed from: private */
    public TextView ab;
    private View ac;
    private Button ad;
    private ImageView ae;
    /* access modifiers changed from: private */
    public EmoteInputView af = null;
    private Animation ag = null;
    /* access modifiers changed from: private */
    public au ah;
    /* access modifiers changed from: private */
    public boolean ai = true;
    /* access modifiers changed from: private */
    public ae aj;
    boolean h = false;
    private ResizeListenerLayout i = null;
    /* access modifiers changed from: private */
    public String j = PoiTypeDef.All;
    /* access modifiers changed from: private */
    public ab k = null;
    /* access modifiers changed from: private */
    public dh l = null;
    /* access modifiers changed from: private */
    public ai m;
    /* access modifiers changed from: private */
    public InputMethodManager n = null;
    private Handler o = new Handler();
    private HeaderLayout p = null;
    private bi q = null;
    /* access modifiers changed from: private */
    public HandyListView r = null;
    /* access modifiers changed from: private */
    public LoadingButton s = null;
    /* access modifiers changed from: private */
    public View t = null;
    private View u;
    private TextView v;
    private TextView w;
    private TextView x;
    private View y;
    private EmoteTextView z;

    /* access modifiers changed from: private */
    public void a(int i2, String str) {
        String str2;
        if (this.k != null) {
            if (i2 != 1) {
                str2 = this.Z.getText().toString().trim();
                if (a.a((CharSequence) str2)) {
                    a((CharSequence) "请输入评论内容");
                    return;
                }
            } else {
                str2 = str;
            }
            if (this.ai) {
                String str3 = this.j;
                String str4 = this.j;
                String str5 = this.k.c;
                ab abVar = this.k;
                b(new v(this, this, str3, str4, 1, i2, str2, str5, abVar.b != null ? abVar.b.h() : abVar.c));
            } else if (this.aj != null) {
                String str6 = "回复" + this.aj.f2985a.i + ":" + str2;
                String str7 = this.j;
                String str8 = this.aj.k;
                String str9 = this.aj.b;
                ae aeVar = this.aj;
                b(new v(this, this, str7, str8, 2, i2, str6, str9, aeVar.f2985a != null ? aeVar.f2985a.h() : aeVar.b));
            }
        }
    }

    public static void a(Context context, ae aeVar) {
        Intent intent = new Intent(context, FeedProfileActivity.class);
        intent.putExtra("key_sitefeedid", aeVar.h);
        intent.putExtra("key_commentid", aeVar.k);
        intent.putExtra("key_owner_id", aeVar.b);
        intent.putExtra("key_comment_content", aeVar.f);
        context.startActivity(intent);
    }

    public static void a(Context context, String str, boolean z2) {
        Intent intent = new Intent(context, FeedProfileActivity.class);
        intent.putExtra("key_sitefeedid", str);
        intent.putExtra("key_show_inputmethod", z2);
        context.startActivity(intent);
    }

    static /* synthetic */ void a(FeedProfileActivity feedProfileActivity, int i2) {
        if (feedProfileActivity.k != null) {
            feedProfileActivity.k.g = i2;
            feedProfileActivity.F.setText(new StringBuilder(String.valueOf(i2)).toString());
            if (feedProfileActivity.k.g > 0 || !feedProfileActivity.l.isEmpty()) {
                feedProfileActivity.ac.setVisibility(8);
                return;
            }
            feedProfileActivity.aa.clearAnimation();
            feedProfileActivity.aa.setVisibility(8);
            feedProfileActivity.w.setText("暂无评论");
            feedProfileActivity.w.setVisibility(0);
            feedProfileActivity.ac.setVisibility(0);
        }
    }

    static /* synthetic */ void a(FeedProfileActivity feedProfileActivity, String str, boolean z2) {
        o oVar = new o(feedProfileActivity, (int) R.array.reportfeed_items);
        if (z2) {
            oVar.setTitle((int) R.string.report_dialog_title_comment);
        } else {
            oVar.setTitle((int) R.string.report_dialog_title_feed);
        }
        oVar.a();
        oVar.a(new s(feedProfileActivity, z2, str));
        oVar.show();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.b.a.a(java.util.Date, boolean):java.lang.String
     arg types: [java.util.Date, int]
     candidates:
      android.support.v4.b.a.a(android.graphics.Bitmap, float):android.graphics.Bitmap
      android.support.v4.b.a.a(android.graphics.Bitmap, java.io.File):android.graphics.Bitmap
      android.support.v4.b.a.a(int, int):java.lang.String
      android.support.v4.b.a.a(java.util.Collection, java.lang.String):java.lang.String
      android.support.v4.b.a.a(java.lang.Object[], java.lang.String):java.lang.String
      android.support.v4.b.a.a(java.lang.String, java.lang.String):org.json.JSONObject
      android.support.v4.b.a.a(float, int[]):void
      android.support.v4.b.a.a(android.app.Activity, int):void
      android.support.v4.b.a.a(android.view.View, java.lang.Runnable):void
      android.support.v4.b.a.a(java.io.File, java.io.File):void
      android.support.v4.b.a.a(java.io.File, java.util.zip.ZipInputStream):void
      android.support.v4.b.a.a(java.lang.Object, java.lang.StringBuilder):void
      android.support.v4.b.a.a(android.content.Context, java.lang.String):boolean
      android.support.v4.b.a.a(com.immomo.a.a.d.b, com.immomo.momo.service.bean.Message):boolean
      android.support.v4.b.a.a(byte[], java.lang.String):byte[]
      android.support.v4.b.a.a(byte[], byte[]):byte[]
      android.support.v4.b.a.a(int[], boolean):byte[]
      android.support.v4.b.a.a(byte[], boolean):int[]
      android.support.v4.b.a.a(int, float):void
      android.support.v4.b.a.a(java.lang.Object, com.immomo.a.a.f):void
      android.support.v4.view.bf.a(int, float):void
      com.immomo.a.a.f.a(java.lang.Object, com.immomo.a.a.f):void
      android.support.v4.b.a.a(java.util.Date, boolean):java.lang.String */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.b.a(java.lang.String, boolean):android.graphics.Bitmap
     arg types: [java.lang.String, int]
     candidates:
      com.immomo.momo.b.a(java.lang.String, android.graphics.Bitmap):android.graphics.Bitmap
      com.immomo.momo.b.a(java.lang.String, boolean):android.graphics.Bitmap */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup, int, boolean, boolean, int):void
     arg types: [com.immomo.momo.service.bean.bf, android.widget.ImageView, ?[OBJECT, ARRAY], int, int, int, int]
     candidates:
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, int, android.view.View, android.view.ViewGroup, boolean, int, boolean):com.immomo.momo.android.c.o
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup, int, boolean, boolean, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup, int, boolean, boolean, int):void
     arg types: [com.immomo.momo.service.bean.al, android.widget.ImageView, com.immomo.momo.android.view.HandyListView, int, int, int, int]
     candidates:
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, int, android.view.View, android.view.ViewGroup, boolean, int, boolean):com.immomo.momo.android.c.o
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup, int, boolean, boolean, int):void */
    /* access modifiers changed from: private */
    public void a(ab abVar) {
        if (abVar != null) {
            this.v.setText(a.a(abVar.d(), true));
            this.y.setEnabled((abVar.d == null || abVar.d.o == 0) ? false : true);
            this.x.setText(abVar.g());
            if (a.f(abVar.b())) {
                this.z.setText(abVar.c());
                this.z.setVisibility(0);
            } else {
                this.z.setVisibility(8);
            }
            this.A.setVisibility(8);
            this.C.setVisibility(8);
            this.B.setVisibility(8);
            if (a.f(abVar.k) && a.f(abVar.j)) {
                this.C.setVisibility(0);
                this.C.setAlt(abVar.j);
                boolean endsWith = this.k.j.endsWith(".gif");
                if (this.ah == null) {
                    File a2 = q.a(this.k.j, this.k.k);
                    this.e.b((Object) ("emoteFile:" + (a2 == null ? "null" : a2.getAbsolutePath())));
                    if (a2 == null || !a2.exists()) {
                        new i(this.k.j, this.k.k, new k(this, endsWith)).a();
                    } else {
                        this.ah = new au(endsWith ? 1 : 2);
                        this.ah.a(a2, this.C);
                        this.ah.b(20);
                        this.C.setGifDecoder(this.ah);
                    }
                } else {
                    this.C.setGifDecoder(this.ah);
                    if ((this.ah.g() == 4 || this.ah.g() == 2) && this.ah.f() != null && this.ah.f().exists()) {
                        this.ah.a(this.ah.f(), this.C);
                    }
                }
            } else if (abVar.i() > 1) {
                this.B.setVisibility(0);
                this.B.setImage(abVar.j());
                this.B.setOnclickHandler(this);
            } else if (a.f(abVar.getLoadImageId())) {
                this.A.setVisibility(0);
                j.a(abVar, this.A, (ViewGroup) null, 15);
            }
            if (abVar.b != null) {
                this.E.setText(abVar.b.h());
                this.R.setVisibility(0);
                this.Q.setText(new StringBuilder(String.valueOf(abVar.b.I)).toString());
                if (abVar.b.b()) {
                    this.E.setTextColor(g.c((int) R.color.font_vip_name));
                } else {
                    this.E.setTextColor(g.c((int) R.color.font_value));
                }
                if ("F".equals(abVar.b.H)) {
                    this.P.setBackgroundResource(R.drawable.bg_gender_famal);
                    this.H.setImageResource(R.drawable.ic_user_famale);
                } else {
                    this.P.setBackgroundResource(R.drawable.bg_gender_male);
                    this.H.setImageResource(R.drawable.ic_user_male);
                }
                if (abVar.b.j()) {
                    this.M.setVisibility(0);
                } else {
                    this.M.setVisibility(8);
                }
                if (!a.a((CharSequence) abVar.b.M)) {
                    this.N.setVisibility(0);
                    this.N.setImageBitmap(b.a(abVar.b.M, true));
                } else {
                    this.N.setVisibility(8);
                }
                if (abVar.b.an) {
                    this.I.setVisibility(0);
                    this.I.setImageResource(abVar.b.ap ? R.drawable.ic_userinfo_weibov : R.drawable.ic_userinfo_weibo);
                } else {
                    this.I.setVisibility(8);
                }
                if (abVar.b.at) {
                    this.J.setVisibility(0);
                    this.J.setImageResource(abVar.b.au ? R.drawable.ic_userinfo_tweibov : R.drawable.ic_userinfo_tweibo);
                } else {
                    this.J.setVisibility(8);
                }
                if (abVar.b.aJ == 1 || abVar.b.aJ == 3) {
                    this.O.setVisibility(0);
                    this.O.setImageResource(abVar.b.aJ == 1 ? R.drawable.ic_userinfo_groupowner : R.drawable.ic_userinfo_group);
                } else {
                    this.O.setVisibility(8);
                }
                if (abVar.b.ar) {
                    this.K.setVisibility(0);
                } else {
                    this.K.setVisibility(8);
                }
                if (abVar.b.b()) {
                    this.L.setVisibility(0);
                    if (abVar.b.c()) {
                        this.L.setImageResource(R.drawable.ic_userinfo_vip_year);
                    } else {
                        this.L.setImageResource(R.drawable.ic_userinfo_vip);
                    }
                } else {
                    this.L.setVisibility(8);
                }
            } else {
                this.R.setVisibility(8);
                this.E.setText(abVar.c);
            }
            j.a((aj) abVar.b, this.D, (ViewGroup) null, 3, false, true, g.a(8.0f));
            if (abVar == null || abVar.g <= this.l.getCount()) {
                this.F.setText(new StringBuilder(String.valueOf(this.l.getCount())).toString());
            } else {
                this.F.setText(new StringBuilder(String.valueOf(abVar.g)).toString());
            }
            if (abVar.n != null) {
                GameApp gameApp = abVar.n;
                this.S.setVisibility(0);
                this.T.setVisibility(0);
                this.U.setVisibility(0);
                this.V.setText(gameApp.appdesc);
                this.W.setText(gameApp.appname);
                j.a((aj) gameApp.appIconLoader(), this.X, (ViewGroup) this.r, 18, false, true, 0);
            } else if (abVar.o != null) {
                u uVar = abVar.o;
                this.S.setVisibility(0);
                this.T.setVisibility(0);
                this.U.setVisibility(0);
                this.V.setText(String.valueOf(uVar.d) + "  " + uVar.f() + "人参加");
                this.W.setText(uVar.b);
                this.X.setImageResource(R.drawable.ic_discover_event);
            } else if (abVar.p != null) {
                this.S.setVisibility(0);
                this.T.setVisibility(0);
                this.U.setVisibility(0);
                this.V.setText(abVar.p.b);
                this.W.setText(abVar.p.f2983a);
                j.a(abVar.p, this.X, this.r, 18);
            } else {
                this.S.setVisibility(8);
                this.T.setVisibility(8);
                this.U.setVisibility(8);
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(ae aeVar) {
        this.ai = false;
        this.aj = aeVar;
        this.ab.setVisibility(0);
        this.ab.setText(a.f(aeVar.f2985a.l) ? "回复" + aeVar.f2985a.i + "(" + aeVar.f2985a.h() + ")：" + d(aeVar.f) : "回复" + aeVar.f2985a.i + ":" + d(aeVar.f));
        x();
    }

    static /* synthetic */ String c(String str) {
        Matcher matcher = Pattern.compile("(\\[[.[^\\[\\]]]*?\\|et\\|([.[^\\[\\]]]*?\\|)*[.[^\\[\\]]]*?\\])").matcher(str);
        if (matcher.find()) {
            return matcher.group();
        }
        return null;
    }

    static /* synthetic */ ae d(FeedProfileActivity feedProfileActivity) {
        if (feedProfileActivity.l == null || feedProfileActivity.l.getCount() <= 0) {
            return null;
        }
        return (ae) feedProfileActivity.l.getItem(feedProfileActivity.l.getCount() - 1);
    }

    private static String d(String str) {
        return str.toString().indexOf("回复") == 0 ? str.substring(str.indexOf(":") + 1) : str;
    }

    static /* synthetic */ void p(FeedProfileActivity feedProfileActivity) {
        if (feedProfileActivity.k != null) {
            String[] strArr = a.f(feedProfileActivity.k.b()) ? feedProfileActivity.f.h.equals(feedProfileActivity.k.c) ? new String[]{"复制文本", "删除"} : new String[]{"复制文本", "举报"} : feedProfileActivity.f.h.equals(feedProfileActivity.k.c) ? new String[]{"删除"} : new String[]{"举报"};
            at atVar = new at(feedProfileActivity, feedProfileActivity.q, strArr);
            atVar.a(new o(feedProfileActivity, strArr));
            atVar.d();
        }
    }

    /* access modifiers changed from: private */
    public void w() {
        this.ae.setImageResource(R.drawable.ic_publish_emote);
        this.af.a();
    }

    private void x() {
        this.o.postDelayed(new n(this), 200);
    }

    private void y() {
        this.ai = true;
        this.ab.setVisibility(8);
        x();
    }

    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        super.a(bundle);
        setContentView((int) R.layout.activity_feedprofile);
        this.j = getIntent().getStringExtra("key_sitefeedid");
        if (!a.f(this.j)) {
            this.e.b((Object) "++++++++++++++++++++ empty key when start FriendFeedActivity!");
            finish();
            return;
        }
        this.i = (ResizeListenerLayout) findViewById(R.id.layout_root);
        this.p = (HeaderLayout) findViewById(R.id.layout_header);
        this.p.setTitleText("留言内容");
        this.t = g.o().inflate((int) R.layout.common_list_loadmore, (ViewGroup) null);
        this.s = (LoadingButton) this.t.findViewById(R.id.btn_loadmore);
        this.s.setNormalIconResId(R.drawable.ic_btn_inner_clock);
        this.r = (HandyListView) findViewById(R.id.lv_feed);
        this.r.addFooterView(this.t);
        this.G = findViewById(R.id.layout_cover);
        this.Y = findViewById(R.id.layout_feed_comment);
        this.Z = (MSmallEmoteEditeText) this.Y.findViewById(R.id.tv_feed_editer);
        this.ab = (TextView) this.Y.findViewById(R.id.tv_feed_editertitle);
        this.ad = (Button) this.Y.findViewById(R.id.bt_feed_send);
        this.ae = (ImageView) this.Y.findViewById(R.id.iv_feed_emote);
        this.af = (EmoteInputView) this.Y.findViewById(R.id.emoteview);
        this.af.setEditText(this.Z);
        this.af.setEmoteFlag(5);
        this.af.setOnEmoteSelectedListener(new g(this));
        this.q = new bi(this);
        this.q.a((int) R.drawable.ic_topbar_more);
        this.p.a(this.q, new h(this));
        this.u = g.o().inflate((int) R.layout.include_feedprofile_feed, (ViewGroup) null);
        this.v = (TextView) this.u.findViewById(R.id.tv_feed_time);
        this.x = (TextView) this.u.findViewById(R.id.tv_feed_site);
        this.y = this.u.findViewById(R.id.layout_feed_site);
        this.z = (EmoteTextView) this.u.findViewById(R.id.tv_feed_content);
        this.A = (ImageView) this.u.findViewById(R.id.iv_feed_content);
        this.B = (MultiImageView) this.u.findViewById(R.id.mv_feed_content);
        this.C = (MGifImageView) this.u.findViewById(R.id.gv_feed_content);
        this.D = (ImageView) this.u.findViewById(R.id.iv_feed_photo);
        this.E = (TextView) this.u.findViewById(R.id.tv_feed_name);
        this.F = (TextView) this.u.findViewById(R.id.tv_feed_commentcount);
        this.ac = this.u.findViewById(R.id.layout_feed_titlecomment);
        this.w = (TextView) this.ac.findViewById(R.id.tv_feed_titlecomment);
        this.aa = (ImageView) this.ac.findViewById(R.id.iv_feed_titleanim);
        this.R = this.u.findViewById(R.id.userlist_item_layout_badgeContainer);
        this.H = (ImageView) this.R.findViewById(R.id.userlist_item_iv_gender);
        this.P = this.R.findViewById(R.id.userlist_item_layout_genderbackgroud);
        this.I = (ImageView) this.R.findViewById(R.id.userlist_item_pic_iv_weibo);
        this.J = (ImageView) this.R.findViewById(R.id.userlist_item_pic_iv_tweibo);
        this.O = (ImageView) this.R.findViewById(R.id.userlist_item_pic_iv_group_role);
        this.K = (ImageView) this.R.findViewById(R.id.userlist_item_pic_iv_renren);
        this.L = (ImageView) this.R.findViewById(R.id.userlist_item_pic_iv_vip);
        this.M = (ImageView) this.R.findViewById(R.id.userlist_item_pic_iv_multipic);
        this.N = (ImageView) this.R.findViewById(R.id.userlist_item_pic_iv_industry);
        this.Q = (TextView) this.R.findViewById(R.id.userlist_item_tv_age);
        this.S = this.u.findViewById(R.id.feed_view_app_border_bottom);
        this.T = this.u.findViewById(R.id.feed_view_app_border_top);
        this.U = this.u.findViewById(R.id.feed_layout_app);
        this.V = (TextView) this.u.findViewById(R.id.feed_tv_appdesc);
        this.W = (TextView) this.u.findViewById(R.id.feed_tv_apptitle);
        this.X = (ImageView) this.u.findViewById(R.id.feed_iv_appicon);
        this.r.addHeaderView(this.u);
        this.r.setOnItemClickListener(this);
        this.s.setOnProcessListener(this);
        this.Z.setOnTouchListener(this);
        this.A.setOnClickListener(this);
        this.C.setOnClickListener(this);
        this.D.setOnClickListener(this);
        this.G.setOnClickListener(this);
        this.ae.setOnClickListener(this);
        this.ad.setOnClickListener(this);
        this.U.setOnClickListener(this);
        this.i.setOnResizeListener(new i(this));
        this.m = new ai();
        this.n = (InputMethodManager) getSystemService("input_method");
        this.l = new dh(this, this.r);
        this.r.setAdapter((ListAdapter) this.l);
        d();
    }

    public final void b(int i2, String[] strArr) {
        Intent intent = new Intent(this, ImageBrowserActivity.class);
        intent.putExtra("array", strArr);
        intent.putExtra("imagetype", "feed");
        intent.putExtra("index", i2);
        startActivity(intent);
        if (getParent() != null) {
            getParent().overridePendingTransition(R.anim.zoom_enter, R.anim.normal);
        } else {
            overridePendingTransition(R.anim.zoom_enter, R.anim.normal);
        }
    }

    /* access modifiers changed from: protected */
    public final void d() {
        this.k = this.m.a(this.j);
        if (this.k != null) {
            if (this.k.b == null) {
                this.k.b = new bf(this.k.c);
            }
            this.l.b((Collection) this.m.c(this.j));
            if (this.l.getCount() < 20) {
                this.t.setVisibility(8);
            } else {
                this.t.setVisibility(0);
            }
            a(this.k);
        }
    }

    /* access modifiers changed from: protected */
    public final void e() {
        super.e();
        if (this.k == null || this.k.i != 2) {
            if (getIntent().getBooleanExtra("key_show_inputmethod", false)) {
                y();
            }
            if (this.aa.getDrawable() != null) {
                if (this.ag == null) {
                    this.ag = AnimationUtils.loadAnimation(this, R.anim.loading);
                }
                this.aa.startAnimation(this.ag);
            }
            if (a.f(getIntent().getStringExtra("key_commentid"))) {
                ae aeVar = new ae();
                aeVar.k = getIntent().getStringExtra("key_commentid");
                aeVar.h = this.j;
                aeVar.g = this.k;
                aeVar.b = getIntent().getStringExtra("key_owner_id");
                aeVar.f2985a = new aq().b(aeVar.b);
                if (aeVar.f2985a == null) {
                    aeVar.f2985a = new bf(aeVar.b);
                }
                aeVar.f = getIntent().getStringExtra("key_comment_content");
                a(aeVar);
            }
            b(new u(this, this));
            b(new t(this, this, true));
            return;
        }
        a((CharSequence) "该留言已经被删除");
        finish();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.bt_feed_send /*2131165595*/:
                new k("S", "S4202").e();
                a(0, (String) null);
                return;
            case R.id.iv_feed_emote /*2131165596*/:
                if (this.af.isShown()) {
                    w();
                    x();
                    return;
                }
                v();
                this.ae.setImageResource(R.drawable.ic_publish_keyboard);
                if (this.h) {
                    this.o.postDelayed(new m(this), 300);
                } else {
                    this.af.b();
                }
                this.G.setVisibility(0);
                this.Z.requestFocus();
                return;
            case R.id.layout_cover /*2131165601*/:
                v();
                w();
                this.h = false;
                this.G.setVisibility(8);
                return;
            case R.id.iv_feed_photo /*2131166248*/:
                if (this.k != null) {
                    Intent intent = new Intent(this, OtherProfileActivity.class);
                    intent.putExtra("momoid", this.k.c);
                    startActivity(intent);
                    return;
                }
                return;
            case R.id.iv_feed_content /*2131166257*/:
                Intent intent2 = new Intent(this, ImageBrowserActivity.class);
                intent2.putExtra("array", new String[]{this.k.getLoadImageId()});
                intent2.putExtra("imagetype", "feed");
                intent2.putExtra("autohide_header", true);
                startActivity(intent2);
                overridePendingTransition(R.anim.zoom_enter, R.anim.normal);
                return;
            case R.id.gv_feed_content /*2131166259*/:
                Intent intent3 = new Intent(this, EmotionProfileActivity.class);
                intent3.putExtra("eid", this.k.k);
                startActivity(intent3);
                return;
            case R.id.feed_layout_app /*2131166261*/:
                GameApp gameApp = this.k.n;
                u uVar = this.k.o;
                ac acVar = this.k.p;
                if (gameApp != null) {
                    if (a.f(gameApp.action)) {
                        this.e.a((Object) gameApp.action);
                        d.a(gameApp.action, this);
                        return;
                    }
                    return;
                } else if (uVar != null) {
                    if (a.f(uVar.w)) {
                        this.e.a((Object) uVar.w);
                        d.a(uVar.w, this);
                        return;
                    }
                    return;
                } else if (acVar != null && a.f(acVar.d)) {
                    this.e.a((Object) acVar.d);
                    d.a(acVar.d, this);
                    return;
                } else {
                    return;
                }
            case R.id.layout_feed_commentcount /*2131166270*/:
                y();
                return;
            default:
                return;
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.ah != null) {
            this.ah.l();
        }
    }

    public void onItemClick(AdapterView adapterView, View view, int i2, long j2) {
        if (this.k != null) {
            ae aeVar = (ae) this.l.getItem(i2);
            String[] strArr = aeVar.n == 1 ? (this.f.h.equals(this.k.c) || this.f.h.equals(aeVar.b)) ? new String[]{"回复", "查看表情", "删除"} : new String[]{"回复", "查看表情"} : this.f.h.equals(aeVar.b) ? new String[]{"回复", "复制文本", "删除"} : this.f.h.equals(this.k.c) ? new String[]{"回复", "复制文本", "举报", "删除"} : new String[]{"回复", "复制文本", "举报"};
            o oVar = new o(this, strArr);
            oVar.a(new q(this, strArr, aeVar));
            oVar.show();
        }
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 != 4 || !this.af.isShown()) {
            return super.onKeyDown(i2, keyEvent);
        }
        w();
        this.G.setVisibility(8);
        this.h = false;
        return true;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        new k("PO", "P42").e();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        new k("PI", "P42").e();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        if (r()) {
            Intent intent = new Intent();
            intent.putExtra("feedid", this.j);
            intent.putExtra("comment_count", this.k.g);
            intent.setAction(h.f2351a);
            sendBroadcast(intent);
        }
        super.onStop();
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        switch (view.getId()) {
            case R.id.tv_feed_editer /*2131165597*/:
                w();
                return false;
            default:
                return false;
        }
    }

    public final void u() {
        this.s.f();
        b(new t(this, this, false));
    }

    /* access modifiers changed from: protected */
    public final void v() {
        View currentFocus = getCurrentFocus();
        if (currentFocus != null) {
            this.n.hideSoftInputFromWindow(currentFocus.getWindowToken(), 2);
        }
    }
}
