package com.immomo.momo.android.activity.common;

import android.support.v4.b.a;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.ListAdapter;
import com.immomo.momo.android.a.c;
import java.util.List;

final class b implements TextWatcher {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ a f1094a;

    b(a aVar) {
        this.f1094a = aVar;
    }

    public final void afterTextChanged(Editable editable) {
        String trim = editable.toString().trim();
        if (a.a((CharSequence) trim)) {
            List unused = this.f1094a.Q();
            this.f1094a.P();
            this.f1094a.Z.setVisibility(4);
        } else {
            this.f1094a.R = this.f1094a.V.e(trim);
            a aVar = this.f1094a;
            List unused2 = this.f1094a.R;
            aVar.al();
            this.f1094a.P = new c(this.f1094a.c(), this.f1094a.R, this.f1094a.O, a.h(this.f1094a).y());
            this.f1094a.O.setAdapter((ListAdapter) this.f1094a.P);
            this.f1094a.O.k();
            this.f1094a.Z.setVisibility(0);
        }
        this.f1094a.aa.requestFocus();
        this.f1094a.O();
    }

    public final void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public final void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }
}
