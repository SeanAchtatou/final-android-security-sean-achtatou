package com.immomo.momo.android.activity.group;

import android.content.DialogInterface;
import com.immomo.momo.android.view.a.ao;
import java.io.File;

final class db implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ da f1599a;
    private final /* synthetic */ ao b;
    private final /* synthetic */ String c;
    private final /* synthetic */ File d;

    db(da daVar, ao aoVar, String str, File file) {
        this.f1599a = daVar;
        this.b = aoVar;
        this.c = str;
        this.d = file;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        new eb(this.f1599a.f1598a.f1597a, this.f1599a.f1598a.f1597a, this.b.e(), this.b.f(), this.b.g(), this.b.h(), this.c.trim(), this.d).execute(new String[0]);
        dialogInterface.dismiss();
    }
}
