package com.immomo.momo.android.activity.discuss;

import android.content.Context;
import android.content.Intent;
import com.immomo.momo.android.broadcast.t;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.h;

final class y extends d {

    /* renamed from: a  reason: collision with root package name */
    private v f1250a = null;
    private String c = null;
    private /* synthetic */ DiscussProfileActivity d;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public y(DiscussProfileActivity discussProfileActivity, Context context, String str) {
        super(context);
        this.d = discussProfileActivity;
        this.f1250a = new v(context);
        this.c = str;
        this.f1250a.setOnCancelListener(new z(this));
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        String a2 = h.a().a(this.d.q, this.c);
        this.d.y.b = this.c;
        this.d.j.a(this.d.y);
        return a2;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
        this.d.a(this.f1250a);
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        super.a(exc);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        a((String) obj);
        this.d.r.setTitleText(this.d.y.b);
        Intent intent = new Intent();
        intent.setAction(t.d);
        intent.putExtra("disid", this.d.q);
        this.d.sendBroadcast(intent);
    }

    /* access modifiers changed from: protected */
    public final void b() {
        super.b();
        this.d.p();
    }
}
