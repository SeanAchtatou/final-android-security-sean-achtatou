package com.immomo.momo.android.activity.tieba;

import com.immomo.momo.plugin.g.a;
import com.immomo.momo.service.bean.av;
import com.immomo.momo.service.bean.q;
import com.immomo.momo.util.h;
import java.io.File;

final class k implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ EditTieActivity f2293a;

    k(EditTieActivity editTieActivity) {
        this.f2293a = editTieActivity;
    }

    public final void run() {
        File file = null;
        if (this.f2293a.t == 2) {
            if (this.f2293a.B.getDatalist() != null && this.f2293a.B.getDatalist().size() > 0) {
                av avVar = (av) this.f2293a.B.getDatalist().get(0);
                file = avVar.d ? h.a(avVar.e, 16) : ((av) this.f2293a.B.getDatalist().get(0)).b;
            }
        } else if (this.f2293a.t == 1 && this.f2293a.O != null) {
            file = q.a(this.f2293a.O.g(), this.f2293a.O.h());
        }
        a.a().a(this.f2293a.U, this.f2293a.p.getText().toString().trim(), file);
    }
}
