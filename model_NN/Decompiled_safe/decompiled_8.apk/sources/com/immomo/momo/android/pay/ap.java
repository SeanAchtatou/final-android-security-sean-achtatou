package com.immomo.momo.android.pay;

import android.app.Activity;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageInfo;
import android.os.Handler;
import com.alipay.android.app.IAlixPay;
import com.alipay.android.app.IRemoteServiceCallback;
import com.immomo.momo.util.m;
import java.util.List;

public final class ap {

    /* renamed from: a  reason: collision with root package name */
    Integer f2534a = 0;
    IAlixPay b = null;
    boolean c = false;
    Activity d = null;
    /* access modifiers changed from: private */
    public m e = new m("MobileSecurePayer");
    /* access modifiers changed from: private */
    public ServiceConnection f = new aq(this);
    /* access modifiers changed from: private */
    public IRemoteServiceCallback g = new ar(this);

    public final boolean a(String str, Handler handler, Activity activity) {
        boolean z = false;
        if (this.c) {
            return false;
        }
        this.c = true;
        this.d = activity;
        if (this.b == null) {
            List<PackageInfo> installedPackages = this.d.getPackageManager().getInstalledPackages(0);
            int i = 0;
            while (true) {
                if (i < installedPackages.size()) {
                    PackageInfo packageInfo = installedPackages.get(i);
                    if (packageInfo.packageName.equalsIgnoreCase("com.eg.android.AlipayGphone") && packageInfo.versionCode > 37) {
                        z = true;
                        break;
                    }
                    i++;
                } else {
                    break;
                }
            }
            if (z) {
                this.d.getApplicationContext().bindService(new Intent("com.eg.android.AlipayGphone.IAlixPay"), this.f, 1);
            } else {
                this.d.getApplicationContext().bindService(new Intent(IAlixPay.class.getName()), this.f, 1);
            }
        }
        new Thread(new as(this, str, handler)).start();
        return true;
    }
}
