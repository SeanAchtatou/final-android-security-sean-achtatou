package com.immomo.momo.android.activity.emotestore;

import android.content.Intent;
import android.view.View;

final class n implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ EmotionProfileActivity f1326a;

    n(EmotionProfileActivity emotionProfileActivity) {
        this.f1326a = emotionProfileActivity;
    }

    public final void onClick(View view) {
        this.f1326a.startActivity(new Intent(this.f1326a, MainEmotionActivity.class));
    }
}
