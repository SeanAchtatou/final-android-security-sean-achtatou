package com.immomo.momo.android.view.a;

import android.graphics.Bitmap;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.immomo.momo.R;
import com.immomo.momo.util.ao;

final class s extends WebViewClient {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ q f2703a;

    s(q qVar) {
        this.f2703a = qVar;
    }

    public final void onPageFinished(WebView webView, String str) {
        super.onPageFinished(webView, str);
        if (this.f2703a.c != null) {
            this.f2703a.c.setVisibility(8);
        }
    }

    public final void onPageStarted(WebView webView, String str, Bitmap bitmap) {
        super.onPageStarted(webView, str, bitmap);
        if (this.f2703a.c != null) {
            this.f2703a.c.setVisibility(0);
        }
    }

    public final void onReceivedError(WebView webView, int i, String str, String str2) {
        if (i == -2) {
            ao.e(R.string.errormsg_network_unfind);
        } else {
            ao.e(R.string.errormsg_server);
        }
        if (this.f2703a.c != null) {
            this.f2703a.c.setVisibility(8);
        }
    }
}
