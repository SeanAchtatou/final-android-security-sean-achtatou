package com.immomo.momo.android.map;

import android.content.Intent;
import android.view.View;
import com.amap.mapapi.poisearch.PoiTypeDef;

final class ba implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ az f2480a;

    ba(az azVar) {
        this.f2480a = azVar;
    }

    public final void onClick(View view) {
        if (this.f2480a.f.n != null) {
            Intent intent = new Intent();
            intent.putExtra("siteid", PoiTypeDef.All);
            intent.putExtra("sitename", this.f2480a.f2478a);
            intent.putExtra("sitetype", this.f2480a.f.u);
            intent.putExtra("lat", this.f2480a.f.n.getLatitude());
            intent.putExtra("lng", this.f2480a.f.n.getLongitude());
            intent.putExtra("loctype", this.f2480a.f.p);
            this.f2480a.f.setResult(-1, intent);
            this.f2480a.f.finish();
        }
    }
}
