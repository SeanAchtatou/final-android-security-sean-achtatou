package com.immomo.momo.android.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Handler;
import android.util.AttributeSet;
import android.widget.ImageView;
import com.immomo.momo.R;
import com.immomo.momo.g;
import com.immomo.momo.util.m;

public class AudioView extends ImageView {

    /* renamed from: a  reason: collision with root package name */
    private int f2623a;
    private int b;
    private boolean c;
    private Bitmap d = null;
    private Bitmap e = null;
    private Handler f = new c(this, g.d().getMainLooper());
    private int g;
    private int h;
    private int i;

    public AudioView(Context context) {
        super(context);
        new m("AudioView");
        this.g = 7;
        c();
    }

    public AudioView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        new m("AudioView");
        this.g = 7;
        c();
    }

    public AudioView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        new m("AudioView");
        this.g = 7;
        c();
    }

    private void c() {
        try {
            this.d = BitmapFactory.decodeResource(getResources(), R.drawable.ic_chatbar_audio_record);
            setImageBitmap(this.d);
            setScaleType(ImageView.ScaleType.CENTER);
            this.e = BitmapFactory.decodeResource(getResources(), R.drawable.ic_chatbar_audio_recording);
            this.f2623a = this.e.getHeight();
            this.b = this.e.getWidth();
            this.i = 0;
            setMax(7);
            this.c = true;
        } catch (Throwable th) {
            th.printStackTrace();
            this.c = false;
        }
    }

    public final void a() {
        setOpenRecordingDraw(false);
        setImageResource(R.drawable.ic_chatbar_audio_cancel);
    }

    public final void a(int i2) {
        int i3 = i2 > this.g ? this.g : i2;
        if (i3 < 0) {
            i3 = 0;
        }
        this.i = i3;
        this.f.sendEmptyMessage(333);
    }

    public final void b() {
        setImageBitmap(this.d);
        setOpenRecordingDraw(true);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (this.e != null && this.c && this.i > 0 && this.h > 0) {
            int i2 = this.i * this.h;
            Bitmap createBitmap = Bitmap.createBitmap(this.b, i2, Bitmap.Config.ARGB_8888);
            new Canvas(createBitmap).drawBitmap(this.e, new Rect(0, this.f2623a - i2, this.b, this.f2623a), new Rect(0, 0, this.b, i2), (Paint) null);
            canvas.drawBitmap(createBitmap, 0.0f, (float) (this.f2623a - i2), (Paint) null);
        }
    }

    public void setMax(int i2) {
        this.g = i2;
        this.h = this.f2623a / i2;
    }

    public void setOpenRecordingDraw(boolean z) {
        this.c = z;
    }
}
