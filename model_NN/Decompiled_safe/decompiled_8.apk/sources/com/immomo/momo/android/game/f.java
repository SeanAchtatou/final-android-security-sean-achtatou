package com.immomo.momo.android.game;

import android.content.Context;
import android.content.Intent;
import com.immomo.a.a.f.a;
import com.immomo.momo.android.activity.tieba.TiebaProfileActivity;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.m;
import com.immomo.momo.service.bean.GameApp;
import com.immomo.momo.util.ao;

final class f extends d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ GotoTiebaActivity f2432a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public f(GotoTiebaActivity gotoTiebaActivity, Context context) {
        super(context);
        this.f2432a = gotoTiebaActivity;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        GameApp gameApp = new GameApp();
        gameApp.appid = this.f2432a.h;
        m.a().a(gameApp, 0);
        return gameApp.tiebaId;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.f2432a.a(new v(f(), "正在进入陌陌吧...", this));
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        String str = (String) obj;
        if (!a.a(str)) {
            Intent intent = new Intent(this.f2432a.getApplicationContext(), TiebaProfileActivity.class);
            intent.addFlags(268435456);
            intent.putExtra("tiebaid", str);
            this.f2432a.startActivity(intent);
            this.f2432a.finish();
            return;
        }
        ao.b("没有对应的陌陌吧");
        this.f2432a.finish();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.f2432a.p();
    }
}
