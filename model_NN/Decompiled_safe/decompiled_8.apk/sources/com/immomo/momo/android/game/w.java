package com.immomo.momo.android.game;

import android.content.Context;
import com.immomo.momo.R;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.m;
import com.immomo.momo.util.ao;

final class w extends d {

    /* renamed from: a  reason: collision with root package name */
    private v f2447a;
    private int c;
    private String d;
    private int e;
    private /* synthetic */ MDKFeedBackActivity f;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public w(MDKFeedBackActivity mDKFeedBackActivity, Context context, int i, String str, int i2) {
        super(context);
        this.f = mDKFeedBackActivity;
        this.c = i;
        this.d = str;
        this.e = i2;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        m.a().a(this.d, this.c, this.e, this.f.t, this.f.s);
        return null;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.f2447a = new v(this.f);
        this.f2447a.a("请求提交中");
        this.f2447a.setCancelable(true);
        this.f2447a.setOnCancelListener(new x(this));
        this.f.a(this.f2447a);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        ao.e(R.string.feedback_success);
        this.f.setResult(-1);
        this.f.finish();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.f.p();
    }
}
