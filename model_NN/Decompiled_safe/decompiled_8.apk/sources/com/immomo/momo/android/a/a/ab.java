package com.immomo.momo.android.a.a;

import android.graphics.Bitmap;
import android.graphics.drawable.AnimationDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.d;
import com.immomo.momo.android.activity.message.a;
import com.immomo.momo.android.view.a.o;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.Message;
import com.immomo.momo.service.bean.aj;
import com.immomo.momo.service.bean.aq;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.util.j;
import com.immomo.momo.util.m;
import java.lang.ref.SoftReference;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public abstract class ab implements View.OnClickListener, View.OnLongClickListener {

    /* renamed from: a  reason: collision with root package name */
    private static String[] f673a = {"删除消息", "@ TA"};
    protected static final String[] b = {"重新发送", "删除消息"};
    protected static final String[] c = {"删除消息"};
    protected static final String[] d = {"重新发送", "复制文本", "删除消息"};
    protected static final String[] e = {"复制文本", "删除消息"};
    protected static final String[] f = {"复制文本", "删除消息", "@ TA"};
    public static ArrayList j;
    public static Date m;
    private static Map o = new HashMap();
    private TextView A = null;
    private View B = null;
    private ImageView C = null;
    private TextView D = null;
    private WeakReference E = null;
    /* access modifiers changed from: private */
    public AnimationDrawable F;
    public View g = null;
    public Message h = null;
    public LinearLayout i = null;
    protected LayoutInflater k = null;
    protected bf l = null;
    protected m n = new m("test_momo", "[ --- from MessageItem --- ]").b();
    private int p = 0;
    private int q = 0;
    private TextView r = null;
    private TextView s = null;
    private TextView t = null;
    private TextView u = null;
    private TextView v = null;
    private ImageView w = null;
    private LinearLayout x = null;
    private ImageView y = null;
    private View z = null;

    protected ab(a aVar) {
        this.E = new WeakReference(aVar);
        this.k = g.o();
    }

    public static ab a(int i2, int i3, boolean z2, a aVar) {
        ab aVar2;
        switch (i3) {
            case 1:
                aVar2 = new s(aVar);
                break;
            case 2:
                aVar2 = new w(aVar);
                break;
            case 3:
            default:
                aVar2 = new aq(aVar);
                break;
            case 4:
                aVar2 = new d(aVar);
                break;
            case 5:
                aVar2 = new ag(aVar);
                break;
            case 6:
                aVar2 = new h(aVar);
                break;
            case 7:
                aVar2 = new a(aVar);
                break;
        }
        if (i2 == 2 || i2 == 3) {
            if (z2) {
                aVar2.q = R.layout.message_group_receive_template;
            } else {
                aVar2.q = R.layout.message_group_send_template;
            }
        } else if (i2 == 1) {
            if (z2) {
                aVar2.q = R.layout.message_receive_template;
            } else {
                aVar2.q = R.layout.message_send_template;
            }
        }
        aVar2.g = aVar2.k.inflate(aVar2.q, (ViewGroup) null);
        aVar2.g.setTag(aVar2);
        aVar2.a(aVar2.g);
        return aVar2;
    }

    public static void a(String str, Bitmap bitmap) {
        if (bitmap != null) {
            Bitmap b2 = b(str);
            if (b2 == null || b2 != bitmap) {
                o.put(str, new SoftReference(bitmap));
            }
        }
    }

    public static Bitmap b(String str) {
        SoftReference softReference = (SoftReference) o.get(str);
        Bitmap bitmap = softReference == null ? null : (Bitmap) softReference.get();
        if (bitmap == null || !bitmap.isRecycled()) {
            return bitmap;
        }
        o.remove(bitmap);
        return null;
    }

    /* access modifiers changed from: protected */
    public abstract void a();

    public void a(float f2) {
    }

    /* access modifiers changed from: protected */
    public void a(View view) {
        this.i = (LinearLayout) view.findViewById(R.id.message_layout_messagecontainer);
        this.z = view.findViewById(R.id.message_layout_timecontainer);
        this.r = (TextView) view.findViewById(R.id.message_tv_timestamp);
        this.s = (TextView) view.findViewById(R.id.message_tv_distance);
        this.A = (TextView) view.findViewById(R.id.timebar_tv_distance);
        this.t = (TextView) view.findViewById(R.id.message_tv_username);
        this.u = (TextView) view.findViewById(R.id.message_tv_status);
        this.v = (TextView) view.findViewById(R.id.message_tv_status_audio);
        this.w = (ImageView) view.findViewById(R.id.message_iv_status);
        this.x = (LinearLayout) view.findViewById(R.id.message_layout_status);
        this.y = (ImageView) view.findViewById(R.id.message_iv_userphoto);
        this.B = view.findViewById(R.id.message_layout_tail);
        this.C = (ImageView) this.B.findViewById(R.id.message_iv_tail);
        this.D = (TextView) this.B.findViewById(R.id.message_tv_tail);
        this.y.setTag(Integer.valueOf((int) R.id.tag_userlist_item));
        a();
    }

    public final void a(Message message) {
        bf bfVar;
        ab abVar;
        this.h = message;
        if (message.receive) {
            bfVar = message.remoteUser;
            abVar = this;
        } else {
            a aVar = this.E != null ? (a) this.E.get() : null;
            if (aVar != null) {
                bfVar = aVar.h();
                abVar = this;
            } else {
                bfVar = null;
                abVar = this;
            }
        }
        abVar.l = bfVar;
        if (this.l != null) {
            if (this.l.ae == null || this.l.ae.length <= 0) {
                this.i.setTag(PoiTypeDef.All);
            } else {
                this.i.setTag(this.l.ae[0]);
            }
        }
        f();
    }

    public final void a(Float f2) {
        this.r.setText(android.support.v4.b.a.f(this.h.timestamp));
        this.z.setVisibility(0);
        this.A.setText(String.valueOf(android.support.v4.b.a.a(f2.floatValue() / 1000.0f)) + "km");
        this.A.setVisibility(0);
    }

    public final void a(String str) {
        this.r.setText(android.support.v4.b.a.f(this.h.timestamp));
        this.z.setVisibility(0);
        this.A.setText(str);
        this.A.setVisibility(0);
    }

    /* access modifiers changed from: protected */
    public final void a(String[] strArr) {
        o oVar = new o(e(), strArr);
        oVar.a(new ae(this, strArr));
        oVar.setTitle("操作");
        oVar.show();
    }

    /* access modifiers changed from: protected */
    public abstract void b();

    public final a e() {
        if (this.E != null) {
            return (a) this.E.get();
        }
        return null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup, int, boolean, boolean, int):void
     arg types: [com.immomo.momo.service.bean.bf, android.widget.ImageView, ?[OBJECT, ARRAY], int, int, int, int]
     candidates:
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, int, android.view.View, android.view.ViewGroup, boolean, int, boolean):com.immomo.momo.android.c.o
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup, int, boolean, boolean, int):void */
    /* access modifiers changed from: protected */
    public void f() {
        boolean z2;
        boolean z3 = true;
        if (!this.h.receive || this.h.status == 10) {
            int i2 = this.h.status;
            if ((this.h.chatType != 2 && this.h.chatType != 3) || (i2 != 6 && i2 != 2)) {
                this.u.setVisibility(0);
                switch (i2) {
                    case 1:
                        this.x.setVisibility(0);
                        this.x.setBackgroundResource(R.drawable.bg_msgbox_state_sending);
                        this.u.setVisibility(8);
                        this.w.setVisibility(0);
                        this.w.setBackgroundResource(R.anim.message_sending);
                        ImageView imageView = this.w;
                        this.F = (AnimationDrawable) imageView.getBackground();
                        imageView.setBackgroundDrawable(this.F);
                        e().B().post(new ac(this));
                        break;
                    case 2:
                        this.x.setVisibility(0);
                        this.x.setBackgroundResource(R.drawable.bg_msgbox_state_send);
                        this.u.setVisibility(0);
                        this.w.setVisibility(8);
                        this.u.setText((int) R.string.msg_status_sended);
                        break;
                    case 3:
                        this.x.setVisibility(0);
                        this.x.setBackgroundResource(R.drawable.bg_msgbox_state_failure);
                        this.u.setVisibility(0);
                        this.w.setVisibility(8);
                        this.u.setText((int) R.string.msg_status_failed);
                        break;
                    case 4:
                    case 5:
                    case 9:
                    default:
                        this.x.setVisibility(8);
                        break;
                    case 6:
                        this.x.setVisibility(0);
                        this.x.setBackgroundResource(R.drawable.bg_msgbox_state_read);
                        this.u.setVisibility(0);
                        this.w.setVisibility(8);
                        this.u.setText((int) R.string.msg_status_readed);
                        break;
                    case 7:
                        this.x.setVisibility(0);
                        this.x.setBackgroundResource(R.drawable.bg_msgbox_state_processing);
                        this.u.setVisibility(0);
                        this.w.setVisibility(8);
                        this.u.setText((int) R.string.msg_status_uploading);
                        break;
                    case 8:
                        this.x.setVisibility(0);
                        this.x.setBackgroundResource(R.drawable.bg_msgbox_state_processing);
                        this.u.setVisibility(0);
                        this.w.setVisibility(8);
                        this.u.setText((int) R.string.msg_status_positioning);
                        break;
                    case 10:
                        this.u.setVisibility(0);
                        if (!this.h.receive) {
                            this.w.setVisibility(8);
                            this.x.setBackgroundResource(R.drawable.bg_msgbox_state_cloud);
                        } else {
                            this.v.setVisibility(8);
                            this.u.setBackgroundResource(R.drawable.bg_msgbox_state_cloud);
                        }
                        this.u.setText((int) R.string.msg_status_cloud);
                        break;
                    case 11:
                        this.x.setVisibility(4);
                        break;
                }
            } else {
                this.x.setVisibility(8);
            }
        } else {
            this.u.setVisibility(8);
            if (this.h.contentType != 4 || this.h.isAudioPlayed) {
                this.v.setVisibility(8);
            } else {
                this.v.setVisibility(0);
            }
        }
        j.a((aj) this.l, this.y, (ViewGroup) null, 3, false, true, g.a(8.0f));
        this.y.setOnClickListener(new ad(this));
        if (this.h.contentType == 6) {
            this.i.setBackgroundResource(0);
        } else {
            if (this.h.contentType == 5) {
                this.p = R.drawable.bg_chat_timebar;
            } else if (this.h.receive) {
                if (this.h.bubbleStyle > 0) {
                    this.p = R.drawable.bg_message_vip_receive;
                } else {
                    this.p = R.drawable.bg_message_box_receive;
                }
            } else if (this.h.bubbleStyle > 0) {
                this.p = R.drawable.bg_message_vip_send;
            } else {
                this.p = R.drawable.bg_message_box_send;
            }
            this.i.setBackgroundResource(this.p);
        }
        if (android.support.v4.b.a.f(this.h.tailIcon)) {
            if (this.h.tail == null) {
                this.h.tail = new aq(this.h.tailIcon);
            }
            j.a(this.h.tail, this.C, (ViewGroup) null, 18);
            this.C.setVisibility(0);
            z2 = true;
        } else {
            this.C.setVisibility(8);
            z2 = false;
        }
        if (android.support.v4.b.a.f(this.h.tailTitle)) {
            this.D.setText(this.h.tailTitle);
            this.D.setVisibility(0);
        } else {
            this.D.setVisibility(8);
            z3 = z2;
        }
        if (android.support.v4.b.a.f(this.h.tailAction)) {
            this.B.setOnClickListener(this);
        } else {
            this.B.setOnClickListener(null);
        }
        if (z3) {
            this.B.setVisibility(0);
        } else {
            this.B.setVisibility(8);
        }
        if ((this.h.chatType == 2 || this.h.chatType == 3) && this.h.receive) {
            if (this.h.distance >= 0.0f) {
                this.s.setText(String.valueOf(android.support.v4.b.a.a(this.h.distance / 1000.0f)) + "km");
            } else if (this.h.distance == -1.0f) {
                this.s.setText("未知");
            } else if (this.h.distance == -2.0f) {
                this.s.setText("隐身");
            }
            this.t.setText(this.l.h());
        }
        b();
    }

    public final void g() {
        this.r.setText(android.support.v4.b.a.f(this.h.timestamp));
        this.z.setVisibility(0);
        this.A.setVisibility(8);
    }

    public final void h() {
        this.z.setVisibility(8);
    }

    /* access modifiers changed from: protected */
    public String[] i() {
        return j() ? f673a : this.h.status == 3 ? b : c;
    }

    /* access modifiers changed from: protected */
    public final boolean j() {
        return (this.h.chatType == 2 || this.h.chatType == 3) && this.h.receive;
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.message_layout_tail /*2131166880*/:
                if (!android.support.v4.b.a.a((CharSequence) this.h.tailAction.toString())) {
                    d.a(this.h.tailAction.toString(), e());
                    return;
                }
                return;
            default:
                return;
        }
    }

    public boolean onLongClick(View view) {
        a(i());
        return true;
    }
}
