package com.immomo.momo.android.view.photoview;

import android.content.Context;
import android.graphics.RectF;
import android.os.Build;
import android.support.v4.b.a;
import android.util.Log;
import android.widget.ImageView;

final class d implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final e f2820a;
    private int b;
    private int c;
    private /* synthetic */ a d;

    public d(a aVar, Context context) {
        this.d = aVar;
        this.f2820a = Build.VERSION.SDK_INT < 9 ? new g(context) : new f(context);
    }

    public final void a() {
        if (a.f2817a) {
            Log.d("PhotoViewAttacher", "Cancel Fling");
        }
        this.f2820a.b();
    }

    public final void a(int i, int i2, int i3, int i4) {
        int i5;
        int i6;
        int i7;
        int i8;
        RectF b2 = this.d.b();
        if (b2 != null) {
            int round = Math.round(-b2.left);
            if (((float) i) < b2.width()) {
                i6 = Math.round(b2.width() - ((float) i));
                i5 = 0;
            } else {
                i5 = round;
                i6 = round;
            }
            int round2 = Math.round(-b2.top);
            if (((float) i2) < b2.height()) {
                i8 = Math.round(b2.height() - ((float) i2));
                i7 = 0;
            } else {
                i7 = round2;
                i8 = round2;
            }
            this.b = round;
            this.c = round2;
            if (a.f2817a) {
                Log.d("PhotoViewAttacher", "fling. StartX:" + round + " StartY:" + round2 + " MaxX:" + i6 + " MaxY:" + i8);
            }
            if (round != i6 || round2 != i8) {
                this.f2820a.a(round, round2, i3, i4, i5, i6, i7, i8);
            }
        }
    }

    public final void run() {
        ImageView c2 = this.d.c();
        if (c2 != null && this.f2820a.a()) {
            int c3 = this.f2820a.c();
            int d2 = this.f2820a.d();
            if (a.f2817a) {
                Log.d("PhotoViewAttacher", "fling run(). CurrentX:" + this.b + " CurrentY:" + this.c + " NewX:" + c3 + " NewY:" + d2);
            }
            this.d.m.postTranslate((float) (this.b - c3), (float) (this.c - d2));
            this.d.b(this.d.j());
            this.b = c3;
            this.c = d2;
            a.a(c2, this);
        }
    }
}
