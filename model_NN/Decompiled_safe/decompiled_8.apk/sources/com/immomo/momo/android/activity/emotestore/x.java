package com.immomo.momo.android.activity.emotestore;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.a;

final class x extends d {

    /* renamed from: a  reason: collision with root package name */
    private v f1335a = null;
    private boolean c = false;
    private /* synthetic */ EmotionProfileActivity d;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public x(EmotionProfileActivity emotionProfileActivity, Context context, boolean z) {
        super(context);
        this.d = emotionProfileActivity;
        this.c = z;
        this.f1335a = new v(context);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        return a.a().b();
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
        this.f1335a.setCancelable(true);
        this.f1335a.setOnCancelListener(new y(this));
        this.f1335a.a("生成订单...");
        this.d.a(this.f1335a);
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        super.a(exc);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        String str = (String) obj;
        if (this.c) {
            this.d.b(new z(this.d, this.d, this.d.l.h, str));
        } else {
            new u(this.d, this.d, str).execute(new Object[0]);
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.d.p();
    }
}
