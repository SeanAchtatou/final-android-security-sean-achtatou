package com.immomo.momo.android.view;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

final class c extends Handler {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ AudioView f2757a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    c(AudioView audioView, Looper looper) {
        super(looper);
        this.f2757a = audioView;
    }

    public final void handleMessage(Message message) {
        switch (message.what) {
            case 333:
                this.f2757a.invalidate();
                return;
            default:
                return;
        }
    }
}
