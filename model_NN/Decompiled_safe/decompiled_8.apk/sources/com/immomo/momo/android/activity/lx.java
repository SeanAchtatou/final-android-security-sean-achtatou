package com.immomo.momo.android.activity;

import android.view.View;
import android.widget.ViewSwitcher;
import com.immomo.momo.R;
import com.immomo.momo.android.view.HandyTextView;

final class lx implements ViewSwitcher.ViewFactory {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ UserRoamActivity f1823a;

    lx(UserRoamActivity userRoamActivity) {
        this.f1823a = userRoamActivity;
    }

    public final View makeView() {
        HandyTextView handyTextView = new HandyTextView(this.f1823a.getApplicationContext());
        handyTextView.setTextColor(this.f1823a.getResources().getColor(R.color.font_value));
        handyTextView.setTextSize(2, 15.0f);
        handyTextView.setGravity(17);
        return handyTextView;
    }
}
