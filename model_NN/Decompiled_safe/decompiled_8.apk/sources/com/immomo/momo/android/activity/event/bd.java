package com.immomo.momo.android.activity.event;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.android.a.cf;
import com.immomo.momo.android.activity.lh;
import com.immomo.momo.android.b.p;
import com.immomo.momo.android.b.z;
import com.immomo.momo.android.view.HeaderLayout;
import com.immomo.momo.android.view.LoadingButton;
import com.immomo.momo.android.view.MomoRefreshListView;
import com.immomo.momo.android.view.bi;
import com.immomo.momo.android.view.bu;
import com.immomo.momo.android.view.g;
import com.immomo.momo.service.aq;
import com.immomo.momo.service.bean.v;
import com.immomo.momo.service.bean.w;
import com.immomo.momo.service.bean.x;
import com.immomo.momo.service.r;
import com.immomo.momo.util.ao;
import com.immomo.momo.util.k;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class bd extends lh implements bu {
    /* access modifiers changed from: private */
    public MomoRefreshListView O = null;
    /* access modifiers changed from: private */
    public LoadingButton P;
    /* access modifiers changed from: private */
    public Date Q = null;
    /* access modifiers changed from: private */
    public List R = null;
    /* access modifiers changed from: private */
    public Set S = new HashSet();
    /* access modifiers changed from: private */
    public bt T = null;
    /* access modifiers changed from: private */
    public Handler U = new Handler();
    /* access modifiers changed from: private */
    public cf V = null;
    /* access modifiers changed from: private */
    public r W;
    /* access modifiers changed from: private */
    public aq X;
    /* access modifiers changed from: private */
    public v Y = v.NONE;
    /* access modifiers changed from: private */
    public x Z = x.ALL;
    /* access modifiers changed from: private */
    public w aa = w.Hot;
    private int ab = 0;
    /* access modifiers changed from: private */
    public g ac;
    /* access modifiers changed from: private */
    public TextView ad;
    /* access modifiers changed from: private */
    public TextView ae;
    /* access modifiers changed from: private */
    public TextView af;
    private p ag = null;
    private View.OnClickListener ah = new be(this);

    private void O() {
        this.O.setLoadingViewText(R.string.pull_to_refresh_locate_label);
        this.ag = new bm(this);
        try {
            z.a(this.ag);
        } catch (Exception e) {
            this.L.a((Throwable) e);
            ao.g(R.string.errormsg_location_nearby_failed);
            P();
        }
    }

    /* access modifiers changed from: private */
    public void P() {
        this.U.post(new bo(this));
    }

    static /* synthetic */ void r(bd bdVar) {
        if (bdVar.ag != null) {
            z.b(bdVar.ag);
            bdVar.ag.b = false;
        }
        if (bdVar.T != null) {
            bdVar.T.cancel(true);
            bdVar.T = null;
        }
    }

    static /* synthetic */ void w(bd bdVar) {
        Intent intent = new Intent();
        intent.setAction("com.android.settings.APPLICATION_DEVELOPMENT_SETTINGS");
        intent.setFlags(268435456);
        try {
            bdVar.a(intent);
        } catch (Throwable th) {
            bdVar.L.a(th);
            intent.setAction("android.settings.SETTINGS");
            try {
                bdVar.a(intent);
            } catch (Exception e) {
                bdVar.L.a((Throwable) e);
            }
        }
    }

    /* access modifiers changed from: protected */
    public final int B() {
        return R.layout.activity_nearbyevents;
    }

    public final void C() {
        new bi(com.immomo.momo.g.c()).a((int) R.drawable.ic_topbar_arrow_down);
        this.O = (MomoRefreshListView) c((int) R.id.listview);
        this.O.setFastScrollEnabled(false);
        this.O.setLastFlushTime(this.N.b("nevents_lasttime_success"));
        this.O.setEnableLoadMoreFoolter(true);
        this.P = this.O.getFooterViewButton();
        this.O.addHeaderView(com.immomo.momo.g.o().inflate((int) R.layout.listitem_blank, (ViewGroup) null));
        g gVar = new g(c(), 5);
        gVar.setBannerPadding$3b4dfe4b(com.immomo.momo.g.a(10.0f));
        this.ac = gVar;
        this.O.addHeaderView(gVar.getWappview());
        this.O.a(com.immomo.momo.g.o().inflate((int) R.layout.include_nearbyevent_listempty, (ViewGroup) null));
        this.O.setListPaddingBottom(-3);
        this.ad = (TextView) c((int) R.id.nearbyevent_tv_time);
        this.ae = (TextView) c((int) R.id.nearbyevent_tv_type);
        this.af = (TextView) c((int) R.id.nearbyevent_tv_order);
    }

    public final void S() {
        super.S();
        if (this.V.isEmpty()) {
            this.O.l();
        } else if (this.Q == null || System.currentTimeMillis() - this.Q.getTime() > 900000) {
            O();
        }
    }

    public final void a(Context context, HeaderLayout headerLayout) {
        super.a(context, headerLayout);
        headerLayout.setTitleText("附近活动");
    }

    /* access modifiers changed from: protected */
    public final void ae() {
        super.ae();
        new k("PI", "P71").e();
        this.V.notifyDataSetChanged();
        this.ac.g();
    }

    /* access modifiers changed from: protected */
    public final void ag() {
        super.ag();
        new k("PO", "P71").e();
        this.ac.f();
    }

    public final void ai() {
        this.O.i();
    }

    public final void aj() {
        super.aj();
        this.W = new r();
        this.X = new aq();
        this.R = this.W.c();
        this.S.clear();
        this.S.addAll(this.R);
        MomoRefreshListView momoRefreshListView = this.O;
        cf cfVar = new cf(c(), this.R, this.O);
        this.V = cfVar;
        momoRefreshListView.setAdapter((ListAdapter) cfVar);
        this.Q = this.N.b("nevents_latttime_reflush");
        if (this.R.size() <= 0 || !((Boolean) this.N.b("nefilter_remain", false)).booleanValue()) {
            this.P.setVisibility(8);
        } else {
            this.P.setVisibility(0);
        }
        this.Y = v.a(((Integer) this.N.b("nefilter_type_holdtime", Integer.valueOf(this.Y.a()))).intValue());
        this.Z = x.a(((Integer) this.N.b("nefilter_type_subject", Integer.valueOf(this.Z.a()))).intValue());
        this.aa = w.a(((Integer) this.N.b("nefilter_type_sort", Integer.valueOf(this.aa.a()))).intValue());
        this.ab = ((Integer) this.N.b("nefilter_type_citycode", Integer.valueOf(this.ab))).intValue();
        this.af.setText(this.aa.b());
        this.ad.setText(this.Y.b());
        this.ae.setText(this.Z.b());
    }

    public final void b_() {
        O();
    }

    /* access modifiers changed from: protected */
    public final void g(Bundle bundle) {
        this.P.setOnProcessListener(new bj(this));
        this.O.setOnPullToRefreshListener$42b903f6(this);
        this.O.setOnItemClickListener(new bk(this));
        this.O.setOnCancelListener$135502(new bl(this));
        c((int) R.id.nearbyevent_layout_time).setOnClickListener(this.ah);
        c((int) R.id.nearbyevent_layout_order).setOnClickListener(this.ah);
        c((int) R.id.nearbyevent_layout_type).setOnClickListener(this.ah);
        aj();
    }

    public final void p() {
        super.p();
        if (this.ac != null) {
            g gVar = this.ac;
            g.k();
        }
    }
}
