package com.immomo.momo.android.activity;

import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TabHost;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.android.view.HeaderLayout;
import com.immomo.momo.g;

public class AboutTabsActivity extends TabActivity {

    /* renamed from: a  reason: collision with root package name */
    public static HeaderLayout f921a = null;
    private TabHost b = null;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_abouttabs);
        this.b = getTabHost();
        f921a = (HeaderLayout) findViewById(R.id.layout_header);
        LayoutInflater o = g.o();
        Class<AboutActivity> cls = AboutActivity.class;
        View inflate = o.inflate((int) R.layout.common_tabbar_item_lightblue, (ViewGroup) null);
        ((TextView) inflate.findViewById(R.id.tab_item_tv_label)).setText((int) R.string.abouts_aboutmomo);
        TabHost.TabSpec indicator = this.b.newTabSpec(cls.getName()).setIndicator(inflate);
        indicator.setContent(new Intent(getApplicationContext(), cls));
        this.b.addTab(indicator);
        Class<HelpActivity> cls2 = HelpActivity.class;
        View inflate2 = o.inflate((int) R.layout.common_tabbar_item_lightblue, (ViewGroup) null);
        ((TextView) inflate2.findViewById(R.id.tab_item_tv_label)).setText((int) R.string.abouts_userhelp);
        TabHost.TabSpec indicator2 = this.b.newTabSpec(cls2.getName()).setIndicator(inflate2);
        indicator2.setContent(new Intent(getApplicationContext(), cls2));
        this.b.addTab(indicator2);
        inflate2.findViewById(R.id.tabitem_ligthblue_driver_left).setVisibility(0);
        Class<ProtocolActivity> cls3 = ProtocolActivity.class;
        View inflate3 = o.inflate((int) R.layout.common_tabbar_item_lightblue, (ViewGroup) null);
        inflate3.findViewById(R.id.tabitem_ligthblue_driver_left).setVisibility(0);
        ((TextView) inflate3.findViewById(R.id.tab_item_tv_label)).setText((int) R.string.abouts_protocol);
        TabHost.TabSpec indicator3 = this.b.newTabSpec(cls3.getName()).setIndicator(inflate3);
        indicator3.setContent(new Intent(getApplicationContext(), cls3));
        this.b.addTab(indicator3);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        this.b.setCurrentTab(getIntent().getIntExtra("showindex", 0));
    }
}
