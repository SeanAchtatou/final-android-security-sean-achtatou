package com.immomo.momo.android.activity.retrieve;

import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.ao;
import com.immomo.momo.android.view.a.v;
import java.util.regex.Pattern;

public class InputEmailAddressActivity extends ao implements View.OnClickListener {
    private EditText h = null;
    private TextView i = null;
    private View j = null;
    /* access modifiers changed from: private */
    public v k = null;
    /* access modifiers changed from: private */
    public String l = PoiTypeDef.All;

    /* access modifiers changed from: protected */
    public final void d() {
    }

    public void onClick(View view) {
        boolean z = false;
        switch (view.getId()) {
            case R.id.btn_back /*2131165286*/:
                finish();
                return;
            case R.id.btn_login /*2131165287*/:
            case R.id.btn_register /*2131165288*/:
            default:
                return;
            case R.id.btn_ok /*2131165289*/:
                String trim = this.h.getText().toString().trim();
                if (trim == null || trim.length() <= 0) {
                    com.immomo.momo.util.ao.b("请输入邮箱账号");
                } else {
                    String trim2 = this.h.getText().toString().trim();
                    if (!Pattern.compile("\\w[\\w.-]*@[\\w.]+\\.\\w+").matcher(trim2).matches()) {
                        com.immomo.momo.util.ao.b("邮箱地址格式不正确");
                    } else {
                        this.l = trim2;
                        z = true;
                    }
                }
                if (z) {
                    b(new b(this, this));
                    return;
                }
                return;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x0080  */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0091  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r7) {
        /*
            r6 = this;
            r5 = 2131165416(0x7f0700e8, float:1.7945048E38)
            r1 = 0
            super.onCreate(r7)
            r0 = 2130903121(0x7f030051, float:1.7413051E38)
            r6.setContentView(r0)
            r0 = 2131165733(0x7f070225, float:1.7945691E38)
            android.view.View r0 = r6.findViewById(r0)
            android.widget.EditText r0 = (android.widget.EditText) r0
            r6.h = r0
            r0 = 2131165734(0x7f070226, float:1.7945693E38)
            android.view.View r0 = r6.findViewById(r0)
            android.widget.TextView r0 = (android.widget.TextView) r0
            r6.i = r0
            r0 = 2131165289(0x7f070069, float:1.794479E38)
            android.view.View r0 = r6.findViewById(r0)
            r6.j = r0
            android.view.View r0 = r6.j
            r0.setOnClickListener(r6)
            r0 = 2131165286(0x7f070066, float:1.7944785E38)
            android.view.View r0 = r6.findViewById(r0)
            r0.setOnClickListener(r6)
            android.content.Intent r0 = r6.getIntent()
            java.lang.String r2 = "emaildisable"
            boolean r0 = r0.getBooleanExtra(r2, r1)
            if (r0 == 0) goto L_0x004e
            android.view.View r0 = r6.findViewById(r5)
            r0.setVisibility(r1)
        L_0x004e:
            android.content.Intent r0 = r6.getIntent()
            java.lang.String r2 = "islogin"
            boolean r2 = r0.getBooleanExtra(r2, r1)
            android.content.Intent r0 = r6.getIntent()
            java.lang.String r3 = "isbindemail"
            boolean r0 = r0.getBooleanExtra(r3, r1)
            if (r0 == 0) goto L_0x008f
            android.content.Intent r0 = r6.getIntent()
            java.lang.String r3 = "email"
            java.lang.String r0 = r0.getStringExtra(r3)
            r6.l = r0
            boolean r0 = android.support.v4.b.a.f(r0)
            if (r0 == 0) goto L_0x008f
            r0 = 1
        L_0x0077:
            android.widget.EditText r3 = r6.h
            java.lang.String r4 = r6.l
            r3.setText(r4)
            if (r2 != 0) goto L_0x0091
            android.widget.TextView r0 = r6.i
            java.lang.String r1 = "请输入注册时使用的邮箱地址"
            r0.setText(r1)
        L_0x0087:
            com.immomo.momo.util.m r0 = r6.e
            java.lang.String r1 = "onCreate..."
            r0.a(r1)
            return
        L_0x008f:
            r0 = r1
            goto L_0x0077
        L_0x0091:
            if (r0 == 0) goto L_0x00a5
            android.widget.EditText r0 = r6.h
            r0.setEnabled(r1)
            android.widget.EditText r0 = r6.h
            r0.setFocusable(r1)
            android.widget.TextView r0 = r6.i
            java.lang.String r1 = "将通过当前账号的绑定邮箱找回密码"
            r0.setText(r1)
            goto L_0x0087
        L_0x00a5:
            android.view.View r0 = r6.findViewById(r5)
            r0.setVisibility(r1)
            r0 = 2131165199(0x7f07000f, float:1.7944608E38)
            android.view.View r0 = r6.findViewById(r0)
            r2 = 8
            r0.setVisibility(r2)
            android.view.View r0 = r6.j
            r0.setEnabled(r1)
            goto L_0x0087
        */
        throw new UnsupportedOperationException("Method not decompiled: com.immomo.momo.android.activity.retrieve.InputEmailAddressActivity.onCreate(android.os.Bundle):void");
    }
}
