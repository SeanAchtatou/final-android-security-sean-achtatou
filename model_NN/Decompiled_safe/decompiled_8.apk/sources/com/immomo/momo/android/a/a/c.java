package com.immomo.momo.android.a.a;

import android.graphics.Bitmap;
import android.support.v4.b.a;
import com.immomo.momo.android.c.g;
import com.immomo.momo.service.bean.Message;

final class c implements g {

    /* renamed from: a  reason: collision with root package name */
    private Message f688a;
    private /* synthetic */ a b;

    public c(a aVar, Message message) {
        this.b = aVar;
        this.f688a = message;
    }

    public final /* synthetic */ void a(Object obj) {
        Bitmap bitmap = (Bitmap) obj;
        a.j.remove(this.f688a.msgId);
        this.f688a.isLoadingResourse = false;
        if (bitmap != null) {
            a.a(this.f688a.getLoadImageId(), a.a(bitmap, 6.0f));
            this.f688a.setImageLoadFailed(false);
        } else {
            this.f688a.setImageLoadFailed(true);
        }
        this.b.w.sendEmptyMessage(396);
    }
}
