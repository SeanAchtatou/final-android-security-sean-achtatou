package com.immomo.momo.android.view;

import android.view.MotionEvent;
import android.view.View;

final class ag implements View.OnTouchListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ EmoteInputView f2716a;

    ag(EmoteInputView emoteInputView) {
        this.f2716a = emoteInputView;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        EmoteInputView.a(this.f2716a, motionEvent);
        return false;
    }
}
