package com.immomo.momo.android.b;

public final class o {

    /* renamed from: a  reason: collision with root package name */
    private double f2333a = -1.0d;
    private double b = -1.0d;
    private float c = -1.0f;
    private int d = -1;

    public final int a() {
        return this.d;
    }

    public final void a(double d2) {
        this.f2333a = d2;
    }

    public final void a(float f) {
        this.c = f;
    }

    public final void a(int i) {
        this.d = i;
    }

    public final double b() {
        return this.f2333a;
    }

    public final void b(double d2) {
        this.b = d2;
    }

    public final double c() {
        return this.b;
    }

    public final float d() {
        return this.c;
    }

    public final String toString() {
        return "GeoLocation [latitude=" + this.f2333a + ", longitude=" + this.b + ", corrected=false, accuracy=" + this.c + ", locType=" + this.d + "]";
    }
}
