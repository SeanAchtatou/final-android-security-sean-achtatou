package com.immomo.momo.android.activity.group;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.protocol.a.a.h;
import com.immomo.momo.protocol.a.o;
import java.util.Collection;
import java.util.Date;

final class bw extends d {

    /* renamed from: a  reason: collision with root package name */
    private h f1570a;
    private /* synthetic */ GroupFeedsActivity c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public bw(GroupFeedsActivity groupFeedsActivity, Context context) {
        super(context);
        this.c = groupFeedsActivity;
        if (groupFeedsActivity.z != null) {
            groupFeedsActivity.z.cancel(true);
        }
        groupFeedsActivity.z = this;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        this.f1570a = o.a().a(this.c.u, 0);
        this.c.B.a(this.f1570a.e, this.c.u, true);
        this.c.y.clear();
        this.c.y.addAll(this.f1570a.e);
        return null;
    }

    /* access modifiers changed from: protected */
    public final void a() {
    }

    /* access modifiers changed from: protected */
    public final void a(Object obj) {
        this.c.D = this.f1570a.c;
        if (!this.c.v.D || this.c.v.E.indexOf("_tempimage") < 0) {
            this.c.v.E = this.f1570a.f2887a;
            this.c.v.D = this.f1570a.d;
            this.c.C.a(this.c.u, this.c.v.D, this.c.v.E);
            if (!this.c.v.D) {
                this.c.t = this.c.o().b("gmemberlist_lasttime_success" + this.c.u);
                this.c.b(new bu(this.c, this.c.n()));
            } else {
                this.c.w();
            }
        }
        Date date = new Date();
        this.c.i.setLastFlushTime(date);
        this.c.o().a("gfeeds_lasttime_success" + this.c.u, date);
        this.c.o().c("gffilter_remain" + this.c.u, Boolean.valueOf(this.f1570a.b));
        this.c.x = this.f1570a.e;
        this.c.A.a((Collection) this.c.x);
        this.c.j.setVisibility(this.f1570a.b ? 0 : 8);
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.c.i.n();
        this.c.z = (bw) null;
    }
}
