package com.immomo.momo.android.map;

import android.content.Intent;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.amap.mapapi.core.GeoPoint;
import com.immomo.momo.R;
import com.immomo.momo.android.b.p;
import com.immomo.momo.android.b.z;
import com.immomo.momo.android.view.HeaderLayout;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.g;
import com.immomo.momo.util.m;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class RomaAMapActivity extends i implements View.OnClickListener {
    v b = null;
    /* access modifiers changed from: private */
    public GeoPoint c = null;
    /* access modifiers changed from: private */
    public MomoAMapView f = null;
    private HeaderLayout g = null;
    /* access modifiers changed from: private */
    public m h = new m(getClass().getSimpleName());
    /* access modifiers changed from: private */
    public double i;
    /* access modifiers changed from: private */
    public double j;
    /* access modifiers changed from: private */
    public p k = null;
    private ExecutorService l = Executors.newSingleThreadExecutor();
    private boolean m = false;
    private Button n;
    private Button o;
    private TextView p;
    /* access modifiers changed from: private */
    public Handler q = new Handler();

    /* access modifiers changed from: private */
    public void a() {
        this.q.post(new ai(this));
        z.b(this.k);
        if (this.k != null) {
            this.k.b = false;
        }
    }

    static /* synthetic */ void e(RomaAMapActivity romaAMapActivity) {
        if (!romaAMapActivity.l.isShutdown()) {
            romaAMapActivity.l.execute(new Thread(new ag(romaAMapActivity)));
        }
    }

    /* access modifiers changed from: protected */
    public boolean isRouteDisplayed() {
        return true;
    }

    public void onBackPressed() {
        setResult(0);
        finish();
        super.onBackPressed();
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_back /*2131165286*/:
                setResult(0);
                finish();
                return;
            case R.id.btn_login /*2131165287*/:
            case R.id.btn_register /*2131165288*/:
            default:
                return;
            case R.id.btn_ok /*2131165289*/:
                Intent intent = new Intent();
                intent.putExtra("latitude", this.i);
                intent.putExtra("longitude", this.j);
                intent.putExtra("geo_adress", z.a(this.i, this.j, g.q().S, g.q().T));
                setResult(-1, intent);
                finish();
                return;
        }
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x00d8  */
    /* JADX WARNING: Removed duplicated region for block: B:26:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r8) {
        /*
            r7 = this;
            r5 = 4696837146684686336(0x412e848000000000, double:1000000.0)
            r2 = -4616189618054758400(0xbff0000000000000, double:-1.0)
            super.onCreate(r8)
            r0 = 2130903178(0x7f03008a, float:1.7413167E38)
            r7.setContentView(r0)
            r0 = 2131165197(0x7f07000d, float:1.7944604E38)
            android.view.View r0 = r7.findViewById(r0)
            com.immomo.momo.android.view.HeaderLayout r0 = (com.immomo.momo.android.view.HeaderLayout) r0
            r7.g = r0
            com.immomo.momo.android.view.HeaderLayout r0 = r7.g
            r1 = 2131493288(0x7f0c01a8, float:1.8610052E38)
            r0.setTitleText(r1)
            r0 = 2131165813(0x7f070275, float:1.7945854E38)
            android.view.View r0 = r7.findViewById(r0)
            android.widget.TextView r0 = (android.widget.TextView) r0
            r7.p = r0
            r0 = 2131165286(0x7f070066, float:1.7944785E38)
            android.view.View r0 = r7.findViewById(r0)
            android.widget.Button r0 = (android.widget.Button) r0
            r7.n = r0
            r0 = 2131165289(0x7f070069, float:1.794479E38)
            android.view.View r0 = r7.findViewById(r0)
            android.widget.Button r0 = (android.widget.Button) r0
            r7.o = r0
            r0 = 2131165395(0x7f0700d3, float:1.7945006E38)
            android.view.View r0 = r7.findViewById(r0)
            com.immomo.momo.android.map.MomoAMapView r0 = (com.immomo.momo.android.map.MomoAMapView) r0
            r7.f = r0
            com.immomo.momo.android.map.MomoAMapView r0 = r7.f
            a(r0)
            com.immomo.momo.android.map.MomoAMapView r0 = r7.f
            com.immomo.momo.android.map.ah r1 = new com.immomo.momo.android.map.ah
            r1.<init>(r7)
            r0.setGetAddressListener(r1)
            com.immomo.momo.MomoApplication r0 = com.immomo.momo.g.d()
            com.immomo.momo.util.ak r0 = r0.f668a
            if (r0 == 0) goto L_0x0079
            java.lang.String r1 = "roam_vip_tip"
            java.lang.String r4 = ""
            java.lang.String r0 = r0.b(r1, r4)
            boolean r1 = android.support.v4.b.a.a(r0)
            if (r1 != 0) goto L_0x0079
            android.widget.TextView r1 = r7.p
            r1.setText(r0)
        L_0x0079:
            android.content.Intent r0 = r7.getIntent()
            android.os.Bundle r4 = r0.getExtras()
            if (r4 == 0) goto L_0x011e
            java.lang.String r0 = "latitude"
            java.lang.Object r0 = r4.get(r0)
            if (r0 != 0) goto L_0x00f7
            r0 = r2
        L_0x008c:
            r7.i = r0
            java.lang.String r0 = "longitude"
            java.lang.Object r0 = r4.get(r0)
            if (r0 != 0) goto L_0x010a
        L_0x0096:
            r7.j = r2
            double r0 = r7.i
            double r2 = r7.j
            boolean r0 = com.immomo.momo.android.b.z.a(r0, r2)
            if (r0 == 0) goto L_0x011e
            com.amap.mapapi.core.GeoPoint r0 = new com.amap.mapapi.core.GeoPoint
            double r1 = r7.i
            double r1 = r1 * r5
            int r1 = (int) r1
            double r2 = r7.j
            double r2 = r2 * r5
            int r2 = (int) r2
            r0.<init>(r1, r2)
            r7.c = r0
            com.immomo.momo.android.map.MomoAMapView r0 = r7.f
            com.amap.mapapi.map.MapController r0 = r0.getController()
            com.amap.mapapi.core.GeoPoint r1 = r7.c
            r0.animateTo(r1)
            com.immomo.momo.android.map.MomoAMapView r0 = r7.f
            com.amap.mapapi.map.MapController r0 = r0.getController()
            com.amap.mapapi.core.GeoPoint r1 = r7.c
            r0.setCenter(r1)
            r0 = 0
            r7.m = r0
        L_0x00ca:
            android.widget.Button r0 = r7.n
            r0.setOnClickListener(r7)
            android.widget.Button r0 = r7.o
            r0.setOnClickListener(r7)
            boolean r0 = r7.m
            if (r0 == 0) goto L_0x00f6
            com.immomo.momo.android.view.a.v r0 = new com.immomo.momo.android.view.a.v
            r1 = 2131492910(0x7f0c002e, float:1.8609285E38)
            r0.<init>(r7, r1)
            r7.b = r0
            com.immomo.momo.android.view.a.v r0 = r7.b
            r0.show()
            com.immomo.momo.android.map.aj r0 = new com.immomo.momo.android.map.aj
            r0.<init>(r7)
            r7.k = r0
            com.immomo.momo.android.map.al r0 = new com.immomo.momo.android.map.al     // Catch:{ Exception -> 0x0122 }
            r0.<init>(r7)     // Catch:{ Exception -> 0x0122 }
            com.immomo.momo.android.b.z.a(r0)     // Catch:{ Exception -> 0x0122 }
        L_0x00f6:
            return
        L_0x00f7:
            java.lang.String r0 = "latitude"
            java.lang.Object r0 = r4.get(r0)
            java.lang.String r0 = r0.toString()
            java.lang.Double r0 = java.lang.Double.valueOf(r0)
            double r0 = r0.doubleValue()
            goto L_0x008c
        L_0x010a:
            java.lang.String r0 = "longitude"
            java.lang.Object r0 = r4.get(r0)
            java.lang.String r0 = r0.toString()
            java.lang.Double r0 = java.lang.Double.valueOf(r0)
            double r2 = r0.doubleValue()
            goto L_0x0096
        L_0x011e:
            r0 = 1
            r7.m = r0
            goto L_0x00ca
        L_0x0122:
            r0 = move-exception
            r0 = 2131493069(0x7f0c00cd, float:1.8609608E38)
            com.immomo.momo.util.ao.g(r0)
            r7.a()
            goto L_0x00f6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.immomo.momo.android.map.RomaAMapActivity.onCreate(android.os.Bundle):void");
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        try {
            this.l.shutdown();
        } catch (Exception e) {
            this.h.a((Throwable) e);
        }
        a();
        super.onDestroy();
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        return super.onKeyDown(i2, keyEvent);
    }
}
