package com.immomo.momo.android.activity.account;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import com.immomo.momo.R;
import com.immomo.momo.a;
import com.immomo.momo.a.l;
import com.immomo.momo.a.p;
import com.immomo.momo.a.t;
import com.immomo.momo.a.u;
import com.immomo.momo.a.x;
import com.immomo.momo.android.c.ak;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.n;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.g;
import com.immomo.momo.protocol.a.w;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.util.h;
import com.immomo.momo.util.k;
import java.io.File;
import java.util.HashMap;
import org.json.JSONException;

final class z extends d {

    /* renamed from: a  reason: collision with root package name */
    private v f1020a;
    private bf c;
    private /* synthetic */ RegisterActivityWithP d;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    private z(RegisterActivityWithP registerActivityWithP, Context context, bf bfVar) {
        super(context);
        this.d = registerActivityWithP;
        this.f1020a = null;
        this.c = null;
        this.c = bfVar;
    }

    /* synthetic */ z(RegisterActivityWithP registerActivityWithP, Context context, bf bfVar, byte b) {
        this(registerActivityWithP, context, bfVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.h.a(java.lang.String, java.lang.String, int, boolean):void
     arg types: [java.lang.String, java.lang.String, int, int]
     candidates:
      com.immomo.momo.util.h.a(java.lang.String, android.graphics.Bitmap, int, boolean):java.io.File
      com.immomo.momo.util.h.a(java.lang.String, java.lang.String, int, boolean):void */
    /* access modifiers changed from: protected */
    public final Object a(Object... objArr) {
        HashMap B = a.B();
        String loadImageId = this.c.getLoadImageId();
        File a2 = h.a(loadImageId, 3);
        this.b.a((Object) ("~~~~~~~register avatar file=" + a2.getPath() + ", length=" + a2.length() + " timestamp:" + this.d.A));
        w.a().a(this.c, B, a2, this.d.r, this.d.w);
        if (!loadImageId.equals(this.c.getLoadImageId())) {
            h.a(loadImageId, this.c.getLoadImageId(), 2, true);
        }
        if (android.support.v4.b.a.a((CharSequence) this.c.h) || android.support.v4.b.a.a((CharSequence) this.c.W)) {
            throw new x();
        }
        new k("U", "U81").e();
        return null;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.f1020a = new v(this.d, (int) R.string.press);
        this.f1020a.setOnCancelListener(new aa(this));
        this.f1020a.show();
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        this.b.a((Throwable) exc);
        if (exc instanceof com.immomo.momo.a.w) {
            a(exc.getMessage());
        } else if (exc instanceof JSONException) {
            a((int) R.string.errormsg_dataerror);
        } else if ((exc instanceof l) && !this.d.isFinishing()) {
            n b = n.b(this.d, exc.getMessage(), (DialogInterface.OnClickListener) null);
            b.setCancelable(false);
            b.show();
            new k("U", "U82").e();
        } else if (exc instanceof p) {
            a(exc.getMessage());
            new k("U", "U82").e();
        } else if (exc instanceof com.immomo.momo.a.a) {
            a(exc.getMessage());
        } else if (exc instanceof x) {
            a("邮箱验证失败，请稍后重试");
        } else if (exc instanceof u) {
            n.b(this.d, (int) R.string.errormsg_devices, (DialogInterface.OnClickListener) null).show();
        } else if (exc instanceof t) {
            a(exc.getMessage());
        } else if (!"mobile".equals(g.Y()) || !g.ac()) {
            a((int) R.string.errormsg_server);
        } else {
            this.d.g();
        }
    }

    /* access modifiers changed from: protected */
    public final void a(Object obj) {
        this.d.sendBroadcast(new Intent(com.immomo.momo.android.broadcast.l.f2355a));
        this.d.b(new w(this.d, this.d, this.c, (byte) 0));
        this.d.b(new ak(this.d, this.c));
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.f1020a.dismiss();
        this.f1020a = null;
    }
}
