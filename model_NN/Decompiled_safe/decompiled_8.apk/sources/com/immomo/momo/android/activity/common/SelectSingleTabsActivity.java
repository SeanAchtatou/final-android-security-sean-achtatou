package com.immomo.momo.android.activity.common;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.b.a;
import com.immomo.momo.R;

public class SelectSingleTabsActivity extends j {
    private String i;

    /* access modifiers changed from: protected */
    public final String A() {
        return getString(R.string.discuss_select_createwarn_much);
    }

    /* access modifiers changed from: protected */
    public final void B() {
        this.i = getIntent().getStringExtra("title");
    }

    /* access modifiers changed from: protected */
    public final void C() {
        int i2 = 0;
        a(ay.class, a.class);
        findViewById(R.id.contact_tab_both).setOnClickListener(this);
        findViewById(R.id.contact_tab_follows).setOnClickListener(this);
        int intExtra = getIntent().getIntExtra("showindex", 0);
        if (intExtra >= 0) {
            i2 = intExtra > 1 ? 1 : intExtra;
        }
        c(i2);
    }

    /* access modifiers changed from: protected */
    public final void a(int i2, int i3) {
        if (!a.a((CharSequence) this.i)) {
            setTitle(this.i);
        }
    }

    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        super.a(bundle);
        this.h = 1;
        if (!a.a((CharSequence) this.i)) {
            setTitle(this.i);
        }
    }

    /* access modifiers changed from: protected */
    public final void a(String str, int i2) {
        if (i2 == 0 && !a.a((CharSequence) str)) {
            Intent intent = new Intent();
            intent.putExtra("smomoid", str);
            setResult(-1, intent);
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public final boolean y() {
        return true;
    }

    /* access modifiers changed from: protected */
    public final int z() {
        return this.h;
    }
}
