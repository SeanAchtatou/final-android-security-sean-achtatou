package com.immomo.momo.android.activity.common;

import android.content.DialogInterface;

final class x implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ CommonShareActivity f1135a;
    private final /* synthetic */ String b;
    private final /* synthetic */ int c;

    x(CommonShareActivity commonShareActivity, String str, int i) {
        this.f1135a = commonShareActivity;
        this.b = str;
        this.c = i;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        CommonShareActivity.a(this.f1135a, this.b, this.c);
        dialogInterface.dismiss();
    }
}
