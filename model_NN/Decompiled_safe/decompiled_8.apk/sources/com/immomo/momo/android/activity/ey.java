package com.immomo.momo.android.activity;

import android.view.View;
import android.widget.AdapterView;
import com.immomo.momo.R;
import com.immomo.momo.android.view.a.o;
import com.immomo.momo.service.bean.bf;

final class ey implements AdapterView.OnItemLongClickListener {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public /* synthetic */ HiddenlistActivity f1428a;

    ey(HiddenlistActivity hiddenlistActivity) {
        this.f1428a = hiddenlistActivity;
    }

    public final boolean onItemLongClick(AdapterView adapterView, View view, int i, long j) {
        o oVar = new o(this.f1428a, (int) R.array.hiddenlist_dialog_item);
        oVar.setTitle("请选择操作");
        oVar.a(new ez(this, (bf) this.f1428a.l.getItem(i)));
        oVar.show();
        return true;
    }
}
