package com.immomo.momo.android.activity.maintab;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.b.a;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.handmark.pulltorefresh.library.PullToRefreshScrollView;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.AboutTabsActivity;
import com.immomo.momo.android.activity.BlacklistActivity;
import com.immomo.momo.android.activity.ChangePwdActivity;
import com.immomo.momo.android.activity.HiddenlistActivity;
import com.immomo.momo.android.activity.LanguageSettingsActivity;
import com.immomo.momo.android.activity.LocationServiceSettingActivity;
import com.immomo.momo.android.activity.MsgNoticeSettingActivity;
import com.immomo.momo.android.activity.OnlineSettingActivity;
import com.immomo.momo.android.activity.OtherProfileActivity;
import com.immomo.momo.android.activity.PhoneBoxActivity;
import com.immomo.momo.android.activity.PhoneNumberBindEmailActivity;
import com.immomo.momo.android.activity.SharePageActivity;
import com.immomo.momo.android.activity.VisitorListActivity;
import com.immomo.momo.android.activity.common.CloudMessageTabsActivity;
import com.immomo.momo.android.activity.contacts.OpenContactActivity;
import com.immomo.momo.android.activity.emotestore.MineEmotesActivity;
import com.immomo.momo.android.activity.feed.MyFeedListActivity;
import com.immomo.momo.android.activity.feed.PublishFeedActivity;
import com.immomo.momo.android.activity.message.ChatActivity;
import com.immomo.momo.android.activity.message.ChatBGSettingActivity;
import com.immomo.momo.android.activity.plugin.BindEmailActivity;
import com.immomo.momo.android.activity.plugin.CommunityStatusActivity;
import com.immomo.momo.android.activity.setting.CommunityBindActivity;
import com.immomo.momo.android.activity.setting.FeedBackListActivity;
import com.immomo.momo.android.activity.setting.SettingMutetimeActivity;
import com.immomo.momo.android.activity.setting.SettingShareActivity;
import com.immomo.momo.android.broadcast.d;
import com.immomo.momo.android.broadcast.f;
import com.immomo.momo.android.broadcast.p;
import com.immomo.momo.android.broadcast.w;
import com.immomo.momo.android.broadcast.z;
import com.immomo.momo.android.c.o;
import com.immomo.momo.android.c.r;
import com.immomo.momo.android.c.u;
import com.immomo.momo.android.pay.MemberCenterActivity;
import com.immomo.momo.android.pay.RechargeActivity;
import com.immomo.momo.android.view.a.n;
import com.immomo.momo.android.view.bi;
import com.immomo.momo.b;
import com.immomo.momo.g;
import com.immomo.momo.service.ai;
import com.immomo.momo.service.aq;
import com.immomo.momo.service.bean.ab;
import com.immomo.momo.service.bean.aj;
import com.immomo.momo.service.bean.at;
import com.immomo.momo.util.j;
import com.immomo.momo.util.k;
import com.sina.weibo.sdk.constant.Constants;
import java.io.File;

public class cd extends k implements View.OnClickListener {
    private TextView O;
    private TextView P;
    private TextView Q;
    private TextView R;
    private TextView S;
    private TextView T;
    private TextView U;
    private TextView V;
    private TextView W;
    private TextView X;
    private TextView Y;
    private TextView Z;
    /* access modifiers changed from: private */
    public Handler aA;
    private d aB;
    private TextView aa;
    private ImageView ab;
    private ImageView ac;
    private ImageView ad;
    private ImageView ae;
    private ImageView af;
    private ImageView ag;
    private ImageView ah;
    private ImageView ai;
    private ImageView aj;
    private ImageView ak;
    private ImageView al;
    private ImageView am;
    private ImageView an;
    private ImageView ao;
    private PullToRefreshScrollView ap = null;
    private View aq;
    private TextView ar;
    private TextView as;
    private TextView at;
    private ai au;
    private w av = null;
    private p aw = null;
    private f ax = null;
    private z ay = null;
    private boolean az = false;

    public cd() {
        new ce(this);
        this.aA = new cf(this);
        this.aB = new cg(this);
    }

    private void P() {
        this.O.setText(this.M.h());
        this.P.setText("陌陌号：" + this.M.h);
        if (this.M.H != null) {
            if (this.M.H.equals("F")) {
                this.Q.setBackgroundResource(R.drawable.bg_gender_famal);
                this.Q.setCompoundDrawablesWithIntrinsicBounds((int) R.drawable.ic_user_famale, 0, 0, 0);
            } else if (this.M.H.equals("M")) {
                this.Q.setBackgroundResource(R.drawable.bg_gender_male);
                this.Q.setCompoundDrawablesWithIntrinsicBounds((int) R.drawable.ic_user_male, 0, 0, 0);
            }
        }
        this.Q.setText(new StringBuilder(String.valueOf(this.M.I)).toString());
        al();
        Q();
    }

    /* access modifiers changed from: private */
    public void Q() {
        String string = d().getString(R.string.gold);
        this.R.setText(String.format(string, String.valueOf(this.M.r())));
    }

    static /* synthetic */ void a(cd cdVar, String str, Bitmap bitmap) {
        if (!a.a((CharSequence) str) && bitmap != null) {
            cdVar.ag.setImageBitmap(bitmap);
        }
    }

    private void al() {
        if (this.M.aw) {
            this.Y.setVisibility(4);
            this.af.setImageResource(R.drawable.ic_setting_email);
        } else {
            this.Y.setTextColor(d().getColor(R.color.blue));
            this.Y.setText((int) R.string.user_profile_unbind);
            this.Y.setVisibility(0);
            this.af.setImageResource(R.drawable.ic_setting_email_unbind);
        }
        if (this.M.an) {
            if (this.M.ap) {
                this.ab.setImageResource(R.drawable.ic_setting_weibov);
            } else {
                this.ab.setImageResource(R.drawable.ic_setting_weibo);
            }
            if (this.M.aG == -1 || (this.M.aG >= 0 && this.M.aG <= 3)) {
                this.U.setVisibility(0);
                this.T.setVisibility(8);
            } else {
                this.U.setVisibility(8);
                this.T.setVisibility(4);
            }
            this.T.setText((int) R.string.user_profile_bind);
        } else {
            this.T.setText((int) R.string.user_profile_unbind);
            this.ab.setImageResource(R.drawable.ic_setting_weibo_unbind);
            this.U.setVisibility(8);
            this.T.setVisibility(0);
        }
        if (this.M.ar) {
            this.W.setText((int) R.string.user_profile_bind);
            this.W.setVisibility(4);
            this.ae.setImageResource(R.drawable.ic_setting_renren);
        } else {
            this.W.setText((int) R.string.user_profile_unbind);
            this.W.setVisibility(0);
            this.ae.setImageResource(R.drawable.ic_setting_renren_unbind);
        }
        this.V.setBackgroundResource(0);
        this.V.setTextColor(d().getColor(R.color.blue));
        this.V.setTextSize(2, 16.0f);
        this.V.setLayoutParams(this.V.getLayoutParams());
        if (this.M.at) {
            this.V.setText((int) R.string.user_profile_bind);
            this.V.setVisibility(4);
            if (this.M.au) {
                this.ac.setImageResource(R.drawable.ic_setting_tweibov);
            } else {
                this.ac.setImageResource(R.drawable.ic_setting_tweibo);
            }
        } else {
            this.V.setText((int) R.string.user_profile_unbind);
            this.V.setVisibility(0);
            this.ac.setImageResource(R.drawable.ic_setting_tweibo_unbind);
        }
        if (this.M.g) {
            if (this.N != null) {
                if (this.N.c) {
                    this.X.setText((int) R.string.user_profile_synphone);
                    this.X.setVisibility(4);
                } else {
                    this.X.setText((int) R.string.user_profile_unsynphone);
                    this.X.setVisibility(0);
                }
            }
            this.ad.setImageResource(R.drawable.ic_setting_phone);
            return;
        }
        this.X.setVisibility(0);
        this.X.setText((int) R.string.user_profile_unbind);
        this.ad.setImageResource(R.drawable.ic_setting_phone_unbind);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup, int, boolean, boolean, int):void
     arg types: [com.immomo.momo.service.bean.ab, android.widget.ImageView, ?[OBJECT, ARRAY], int, int, int, int]
     candidates:
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, int, android.view.View, android.view.ViewGroup, boolean, int, boolean):com.immomo.momo.android.c.o
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup, int, boolean, boolean, int):void */
    /* access modifiers changed from: private */
    public void am() {
        ab b = this.au.b(this.M.h);
        if (b != null) {
            this.az = true;
            this.Z.setText(b.b());
            if (b.d != null) {
                this.aa.setText(String.valueOf(b.d.f) + " " + b.m);
            } else {
                this.aa.setText(" " + b.m);
            }
            if (a.a((CharSequence) b.getLoadImageId())) {
                this.ao.setVisibility(8);
                return;
            }
            j.a((aj) b, this.ao, (ViewGroup) null, 15, true, true, g.a(8.0f));
            this.ao.setVisibility(0);
            return;
        }
        this.az = false;
        this.ao.setVisibility(8);
        this.aa.setVisibility(8);
        this.Z.setText((int) R.string.setting_feed_null);
    }

    private boolean an() {
        return this.M.b() && this.M.ad;
    }

    /* access modifiers changed from: private */
    public void ao() {
        boolean z;
        View W2 = W();
        View findViewById = W2 != null ? W2.findViewById(R.id.tabitem_prifile_iv_badge) : null;
        if (an() || ((Boolean) this.N.b("newvip_year", false)).booleanValue()) {
            c((int) R.id.tv_vip_sethidden_newtag).setVisibility(8);
            z = false;
        } else {
            c((int) R.id.tv_vip_sethidden_newtag).setVisibility(0);
            z = true;
        }
        if (findViewById != null) {
            if (z || !this.M.o() || this.M.p() || an()) {
                this.L.b((Object) ("currentUser.isProfileComplete():" + this.M.o() + " currentUser.isSinaWeiboExpired():" + this.M.p() + "currentUser.shopupdateTime > userPreference.shopUpdateTime:" + (this.M.ak > this.N.j) + "isVipExpiring():" + an()));
                findViewById.setVisibility(0);
            } else {
                findViewById.setVisibility(8);
            }
        }
        if (!this.M.o()) {
            a(a((int) R.string.user_profile_incomplete), (int) R.drawable.ic_common_notice, 0);
            View c = c((int) R.id.profile_layout_incomplete);
            c.setClickable(true);
            c.setFocusable(true);
            c.setOnClickListener(new ch(this));
        } else if (ab()) {
            a(0L);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.b.a(java.lang.String, boolean):android.graphics.Bitmap
     arg types: [java.lang.String, int]
     candidates:
      com.immomo.momo.b.a(java.lang.String, android.graphics.Bitmap):android.graphics.Bitmap
      com.immomo.momo.b.a(java.lang.String, boolean):android.graphics.Bitmap */
    private void ap() {
        if (this.M.j()) {
            this.am.setVisibility(0);
        } else {
            this.am.setVisibility(8);
        }
        if (!a.a((CharSequence) this.M.M)) {
            this.al.setVisibility(0);
            this.al.setImageBitmap(b.a(this.M.M, true));
        } else {
            this.al.setVisibility(8);
        }
        if (this.M.an) {
            this.ah.setVisibility(0);
            this.ah.setImageResource(this.M.ap ? R.drawable.ic_userinfo_weibov : R.drawable.ic_userinfo_weibo);
        } else {
            this.ah.setVisibility(8);
        }
        if (this.M.at) {
            this.ai.setVisibility(0);
        } else {
            this.ai.setVisibility(8);
        }
        if (this.M.aJ == 1 || this.M.aJ == 3) {
            this.aj.setVisibility(0);
            this.aj.setImageResource(this.M.aJ == 1 ? R.drawable.ic_userinfo_groupowner : R.drawable.ic_userinfo_group);
        } else {
            this.aj.setVisibility(8);
        }
        if (this.M.ar) {
            this.ak.setVisibility(0);
        } else {
            this.ak.setVisibility(8);
        }
        aq();
    }

    private void aq() {
        if (this.M.b()) {
            if (this.M.c()) {
                this.an.setImageResource(R.drawable.ic_userinfo_vip_year);
            } else {
                this.an.setImageResource(R.drawable.ic_userinfo_vip);
            }
            this.an.setVisibility(0);
            return;
        }
        this.an.setVisibility(8);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.service.bean.at.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, boolean]
     candidates:
      com.immomo.momo.service.bean.at.a(android.content.Context, java.lang.String):com.immomo.momo.service.bean.at
      com.immomo.momo.service.bean.at.a(java.lang.Integer, java.lang.Integer):void
      com.immomo.momo.service.bean.at.a(java.lang.String, java.util.Date):void
      com.immomo.momo.service.bean.at.a(java.lang.String, java.lang.Object):void */
    private void f(int i) {
        Intent intent = new Intent(g.c(), SharePageActivity.class);
        if (i == 1) {
            at atVar = this.N;
            this.N.d = true;
            atVar.a("sinafriend_syn", (Object) true);
            intent.putExtra("share_type", 0);
            intent.putExtra("title_str", "新浪微博绑定成功");
            intent.putExtra("content_str", "分享资料卡到新浪微博");
            intent.putExtra("show_checkbox", "关注@陌陌科技");
            a(intent, 24);
        } else if (i == 3) {
            intent.putExtra("share_type", 3);
            intent.putExtra("title_str", "人人网绑定成功");
            intent.putExtra("content_str", "分享资料卡到人人网");
            a(intent, 24);
        } else if (i == 2) {
            at atVar2 = this.N;
            this.N.d = true;
            atVar2.a("sinafriend_syn", (Object) true);
            intent.putExtra("share_type", 2);
            intent.putExtra("title_str", "腾讯微博绑定成功");
            intent.putExtra("content_str", "分享资料卡到腾讯微博");
            intent.putExtra("show_checkbox", "关注@陌陌科技");
            a(intent, 24);
        }
    }

    static /* synthetic */ void g(cd cdVar) {
        n a2 = n.a(cdVar.c(), (int) R.string.userprofile_notcomplete_tip_content, (int) R.string.userprofile_notcomplete_dialog_confirm, (int) R.string.userprofile_notcomplete_dialog_cancel, new ci(cdVar), new cj());
        a2.setTitle((int) R.string.userprofile_notcomplete_tip_titile);
        a2.a();
        cdVar.a(a2);
    }

    /* access modifiers changed from: protected */
    public final int B() {
        return R.layout.activity_usersetting;
    }

    /* access modifiers changed from: protected */
    public final void C() {
        this.ag = (ImageView) c((int) R.id.userlayout_header_image_img);
        this.O = (TextView) c((int) R.id.userlayout_info_name);
        this.P = (TextView) c((int) R.id.userlayout_info_momoid);
        this.Q = (TextView) c((int) R.id.userlayout_info_age);
        this.R = (TextView) c((int) R.id.tv_accountgold);
        c((int) R.id.tv_shop);
        this.S = (TextView) c((int) R.id.tv_vipexpire);
        this.ap = (PullToRefreshScrollView) c((int) R.id.scrollview_content);
        this.ab = (ImageView) c((int) R.id.button_sina_icon);
        this.ac = (ImageView) c((int) R.id.button_tx_icon);
        this.ad = (ImageView) c((int) R.id.button_mobile_icon);
        this.ae = (ImageView) c((int) R.id.button_renren_icon);
        this.af = (ImageView) c((int) R.id.button_email_icon);
        this.X = (TextView) c((int) R.id.tv_bindinfo_mobile_r);
        this.W = (TextView) c((int) R.id.tv_bindinfo_renren_r);
        this.T = (TextView) c((int) R.id.tv_bindinfo_sina_r);
        this.U = (TextView) c((int) R.id.tv_bindinfo_sina_r_new);
        this.V = (TextView) c((int) R.id.tv_bindinfo_tx_r);
        this.Y = (TextView) c((int) R.id.tv_bindinfo_email_r);
        this.ah = (ImageView) c((int) R.id.userlayout_info_pic_iv_weibo);
        this.ai = (ImageView) c((int) R.id.userlayout_info_pic_iv_txweibo);
        this.aj = (ImageView) c((int) R.id.userlayout_info_pic_iv_group_role);
        this.ak = (ImageView) c((int) R.id.userlayout_info_pic_iv_renren);
        this.an = (ImageView) c((int) R.id.userlayout_info_pic_iv_vip);
        this.am = (ImageView) c((int) R.id.userlayout_info_pic_iv_multipic);
        this.al = (ImageView) c((int) R.id.userlayout_info_pic_iv_industry);
        this.ar = (TextView) c((int) R.id.setting_tv_online_status);
        this.as = (TextView) c((int) R.id.setting_tv_hidden_time);
        this.at = (TextView) c((int) R.id.setting_tv_msgnotice_status);
        this.aq = c((int) R.id.layout_vipfunction);
        this.Z = (TextView) c((int) R.id.tv_feeddes);
        this.aa = (TextView) c((int) R.id.tv_placedistance);
        this.ao = (ImageView) c((int) R.id.iv_feedimg);
    }

    /* access modifiers changed from: protected */
    public final View V() {
        return c((int) R.id.profile_layout_incomplete);
    }

    /* access modifiers changed from: protected */
    public final void X() {
    }

    public final void ae() {
        if (this.M.b()) {
            this.aq.setVisibility(0);
        } else {
            this.aq.setVisibility(8);
        }
        if (this.N.l == 0) {
            this.ar.setText(a((int) R.string.setting_hide_online));
            this.ar.setVisibility(4);
        } else if (this.N.l == 1) {
            this.ar.setText((int) R.string.usersetting_hide_part);
            this.ar.setVisibility(0);
        } else if (this.N.l == 2) {
            this.ar.setText((int) R.string.setting_hide_all);
            this.ar.setVisibility(0);
        }
        if (!this.N.f2996a) {
            this.at.setVisibility(0);
            this.at.setText("关闭");
        } else if (!this.N.b) {
            this.at.setVisibility(0);
            this.at.setText("静音");
        } else {
            this.at.setVisibility(4);
        }
        if (this.N.h) {
            this.as.setText(this.N.b() + ":00-" + this.N.c() + ":00");
            this.as.setVisibility(0);
        } else {
            this.as.setVisibility(4);
        }
        al();
        Q();
        aq();
        this.L.b((Object) ("currentUser.isRemind=" + this.M.ad));
        if (an()) {
            this.S.setVisibility(0);
            this.S.setText("即将到期");
        } else {
            this.S.setVisibility(4);
        }
        ap();
        ao();
        new k("PI", "P9").e();
    }

    public final void ag() {
        new k("PO", "P9").e();
    }

    public final void ai() {
        this.ap.d();
    }

    public final void aj() {
        super.aj();
        P();
        if ((this.M.ae == null ? 0 : this.M.ae.length) > 0) {
            ImageView imageView = this.ag;
            String str = this.M.ae[0];
            imageView.setTag(R.id.tag_item_imageid, str);
            Bitmap a2 = j.a(str);
            if (a2 != null) {
                imageView.setImageBitmap(a2);
            } else {
                ck ckVar = new ck(this, str, imageView);
                if (new File(new File(com.immomo.momo.a.d(), str.substring(0, 1)), String.valueOf(str) + ".jpg_").exists()) {
                    u.f().execute(new r(str, ckVar, 3, null));
                } else {
                    u.g().execute(new o(str, ckVar, 3, null));
                }
            }
        }
        ap();
        am();
    }

    public final void b(int i, int i2, Intent intent) {
        this.L.a((Object) ("requestCode=" + i + ", resultCode=" + i2));
        switch (i) {
            case 16:
                if (i2 == -1) {
                    ao();
                    return;
                }
                return;
            case 17:
                if (i2 == -1 && this.M.aw) {
                    this.af.setClickable(false);
                    return;
                }
                return;
            case 21:
                if (i2 == -1) {
                    f(3);
                    return;
                }
                return;
            case Constants.WEIBO_SDK_VERSION /*22*/:
                if (i2 == -1) {
                    f(2);
                    return;
                }
                return;
            case 23:
                if (i2 == -1) {
                    f(1);
                    return;
                }
                return;
            case 24:
                if (intent != null) {
                    int intExtra = intent.getIntExtra("share_type", 0);
                    Intent intent2 = new Intent(g.c(), CommunityStatusActivity.class);
                    if (intExtra == 2) {
                        intent2.putExtra("type", 2);
                        a(intent2);
                        return;
                    } else if (intExtra == 3) {
                        intent2.putExtra("type", 3);
                        a(intent2);
                        return;
                    } else if (intExtra == 0) {
                        intent2.putExtra("type", 1);
                        a(intent2);
                        return;
                    } else {
                        return;
                    }
                } else {
                    return;
                }
            case 256:
                P();
                return;
            default:
                return;
        }
    }

    public final void g(Bundle bundle) {
        new aq();
        this.au = new ai();
        aj();
        new bi(g.c()).a((int) R.drawable.ic_topbar_setting);
        c((int) R.id.setting_layout_sina_icon).setOnClickListener(this);
        c((int) R.id.setting_layout_tx_icon).setOnClickListener(this);
        c((int) R.id.setting_layout_renren_icon).setOnClickListener(this);
        c((int) R.id.setting_layout_mobile_icon).setOnClickListener(this);
        c((int) R.id.setting_layout_email_icon).setOnClickListener(this);
        c((int) R.id.setting_layout_online_status).setOnClickListener(this);
        c((int) R.id.setting_layout_msg_notice).setOnClickListener(this);
        c((int) R.id.setting_layout_location_service).setOnClickListener(this);
        c((int) R.id.setting_layout_hidden_time).setOnClickListener(this);
        c((int) R.id.setting_layout_chatbackground).setOnClickListener(this);
        c((int) R.id.setting_layout_updatepwd).setOnClickListener(this);
        c((int) R.id.setting_layout_blacklist).setOnClickListener(this);
        c((int) R.id.setting_layout_markfeedback).setOnClickListener(this);
        c((int) R.id.setting_layout_about).setOnClickListener(this);
        c((int) R.id.setting_layout_logout).setOnClickListener(this);
        c((int) R.id.setting_layout_share).setOnClickListener(this);
        c((int) R.id.setting_layout_language).setOnClickListener(this);
        c((int) R.id.setting_layout_shop).setOnClickListener(this);
        c((int) R.id.setting_layout_accountgold).setOnClickListener(this);
        c((int) R.id.setting_layout_vip).setOnClickListener(this);
        c((int) R.id.userlayout_header).setOnClickListener(this);
        c((int) R.id.userlayout_header).setOnClickListener(this);
        c((int) R.id.setting_layout_feed_inner).setOnClickListener(this);
        c((int) R.id.setting_layout_msg_roaming).setOnClickListener(this);
        c((int) R.id.setting_layout_feedback).setOnClickListener(this);
        c((int) R.id.setting_layout_visitors).setOnClickListener(this);
        c((int) R.id.setting_layout_sethidden).setOnClickListener(this);
        this.av = new w(c());
        this.av.a(this.aB);
        this.aw = new p(c());
        this.aw.a(this.aB);
        this.ax = new f(c());
        this.ax.a(this.aB);
        this.ay = new z(c());
        this.ay.a(this.aB);
        ao();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.userlayout_header /*2131165978*/:
                Intent intent = new Intent(g.c(), OtherProfileActivity.class);
                intent.putExtra("momoid", this.M.h);
                a(intent, 256);
                return;
            case R.id.setting_layout_accountgold /*2131166494*/:
                a(new Intent(g.c(), RechargeActivity.class));
                return;
            case R.id.setting_layout_vip /*2131166496*/:
                a(new Intent(g.c(), MemberCenterActivity.class));
                if (!((Boolean) this.N.b("newvip_year", false)).booleanValue()) {
                    this.N.c("newvip_year", true);
                    return;
                }
                return;
            case R.id.setting_layout_shop /*2131166499*/:
                a(new Intent(g.c(), MineEmotesActivity.class));
                if (this.M.ak > this.N.j) {
                    at atVar = this.N;
                    at atVar2 = this.N;
                    long j = this.M.ak;
                    atVar2.j = j;
                    atVar.c("shop_update_time", Long.valueOf(j));
                    return;
                }
                return;
            case R.id.setting_layout_sina_icon /*2131166501*/:
                if (!this.M.an || this.M.p()) {
                    Intent intent2 = new Intent(g.c(), CommunityBindActivity.class);
                    intent2.putExtra("type", 1);
                    a(intent2, 23);
                    return;
                }
                Intent intent3 = new Intent(g.c(), CommunityStatusActivity.class);
                intent3.putExtra("type", 1);
                a(intent3);
                return;
            case R.id.setting_layout_tx_icon /*2131166503*/:
                if (this.M.at) {
                    Intent intent4 = new Intent(g.c(), CommunityStatusActivity.class);
                    intent4.putExtra("type", 2);
                    a(intent4);
                    return;
                }
                Intent intent5 = new Intent(g.c(), CommunityBindActivity.class);
                intent5.putExtra("type", 2);
                a(intent5, 22);
                return;
            case R.id.setting_layout_renren_icon /*2131166507*/:
                if (this.M.ar) {
                    Intent intent6 = new Intent(g.c(), CommunityStatusActivity.class);
                    intent6.putExtra("type", 3);
                    a(intent6);
                    return;
                }
                Intent intent7 = new Intent(g.c(), CommunityBindActivity.class);
                intent7.putExtra("type", 3);
                a(intent7, 21);
                return;
            case R.id.setting_layout_email_icon /*2131166508*/:
                this.L.a((Object) "email cover clicked");
                this.L.a((Object) ("isbind? " + this.M.aw));
                this.L.a((Object) ("regtype? " + this.M.B));
                if (this.M == null) {
                    return;
                }
                if ((this.M.B != 2 || !g.d(this.M.G)) && (this.M.B != 1 || !this.M.aw)) {
                    a(new Intent(g.c(), PhoneNumberBindEmailActivity.class), 17);
                    return;
                }
                Intent intent8 = new Intent(g.c(), BindEmailActivity.class);
                intent8.putExtra("bind_email", this.M.aw);
                a(intent8, 17);
                return;
            case R.id.setting_layout_mobile_icon /*2131166509*/:
                if (this.M.g) {
                    a(new Intent(g.c(), PhoneBoxActivity.class));
                    return;
                } else {
                    a(new Intent(g.c(), OpenContactActivity.class));
                    return;
                }
            case R.id.setting_layout_feed_inner /*2131166510*/:
                if (this.az) {
                    a(new Intent(g.c(), MyFeedListActivity.class));
                    return;
                }
                Intent intent9 = new Intent(g.c(), PublishFeedActivity.class);
                intent9.putExtra("is_from_nearbyfeed", true);
                a(intent9);
                return;
            case R.id.setting_layout_online_status /*2131166511*/:
                a(new Intent(g.c(), OnlineSettingActivity.class));
                return;
            case R.id.setting_layout_msg_notice /*2131166512*/:
                a(new Intent(g.c(), MsgNoticeSettingActivity.class));
                return;
            case R.id.setting_layout_hidden_time /*2131166515*/:
                a(new Intent(g.c(), SettingMutetimeActivity.class));
                return;
            case R.id.setting_layout_chatbackground /*2131166518*/:
                ChatBGSettingActivity.a(c());
                return;
            case R.id.setting_layout_location_service /*2131166521*/:
                a(new Intent(g.c(), LocationServiceSettingActivity.class));
                return;
            case R.id.setting_layout_updatepwd /*2131166522*/:
                a(new Intent(g.c(), ChangePwdActivity.class));
                return;
            case R.id.setting_layout_blacklist /*2131166523*/:
                a(new Intent(g.c(), BlacklistActivity.class));
                return;
            case R.id.setting_layout_language /*2131166524*/:
                a(new Intent(g.c(), LanguageSettingsActivity.class));
                return;
            case R.id.setting_layout_share /*2131166525*/:
                a(new Intent(g.c(), SettingShareActivity.class));
                return;
            case R.id.setting_layout_markfeedback /*2131166526*/:
                a(new Intent(g.c(), FeedBackListActivity.class));
                return;
            case R.id.setting_layout_about /*2131166527*/:
                Intent intent10 = new Intent(g.c(), AboutTabsActivity.class);
                intent10.putExtra("showindex", 0);
                a(intent10);
                return;
            case R.id.setting_layout_logout /*2131166528*/:
                a(n.a(c(), "注销操作将清除当前账号的登录信息并与服务器断开连接。您确定要这样做吗？", new cm(this)));
                return;
            case R.id.setting_layout_visitors /*2131166546*/:
                a(new Intent(g.c(), VisitorListActivity.class));
                return;
            case R.id.setting_layout_sethidden /*2131166548*/:
                a(new Intent(g.c(), HiddenlistActivity.class));
                return;
            case R.id.setting_layout_msg_roaming /*2131166549*/:
                new k("C", "C9301").e();
                a(new Intent(g.c(), CloudMessageTabsActivity.class));
                return;
            case R.id.setting_layout_feedback /*2131166550*/:
                new k("C", "C9304").e();
                Intent intent11 = new Intent(g.c(), ChatActivity.class);
                intent11.putExtra("remoteUserID", "10000");
                a(intent11);
                return;
            default:
                return;
        }
    }

    public final void p() {
        super.p();
        if (this.av != null) {
            a(this.av);
            this.av = null;
        }
        if (this.aw != null) {
            a(this.aw);
            this.aw = null;
        }
        if (this.ax != null) {
            a(this.ax);
            this.ax = null;
        }
        if (this.ay != null) {
            a(this.ay);
            this.ay = null;
        }
    }
}
