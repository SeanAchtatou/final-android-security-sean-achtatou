package com.immomo.momo.android.view;

import android.content.Context;
import android.util.AttributeSet;

public class MSmallEmoteEditeText extends EmoteEditeText {
    public MSmallEmoteEditeText(Context context) {
        this(context, null);
    }

    public MSmallEmoteEditeText(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 16842862);
    }

    public MSmallEmoteEditeText(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public final CharSequence a(CharSequence charSequence) {
        return bp.b(super.a(charSequence));
    }
}
