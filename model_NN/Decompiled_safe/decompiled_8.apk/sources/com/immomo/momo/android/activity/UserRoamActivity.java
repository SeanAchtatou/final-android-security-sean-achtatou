package com.immomo.momo.android.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Point;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.AbsListView;
import android.widget.ListAdapter;
import android.widget.TextSwitcher;
import android.widget.TextView;
import com.amap.mapapi.core.GeoPoint;
import com.amap.mapapi.location.LocationManagerProxy;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.MomoApplication;
import com.immomo.momo.R;
import com.immomo.momo.android.a.dl;
import com.immomo.momo.android.b.z;
import com.immomo.momo.android.map.MomoAMapView;
import com.immomo.momo.android.map.RomaAMapActivity;
import com.immomo.momo.android.map.i;
import com.immomo.momo.android.view.HandyListView;
import com.immomo.momo.android.view.RomaAnimarionView;
import com.immomo.momo.android.view.a.n;
import com.immomo.momo.android.view.bi;
import com.immomo.momo.g;
import com.immomo.momo.service.aq;
import com.immomo.momo.util.am;
import com.mapabc.minimap.map.vmap.NativeMapEngine;
import java.util.ArrayList;
import java.util.Random;
import mm.purchasesdk.PurchaseCode;

public class UserRoamActivity extends i {
    /* access modifiers changed from: private */
    public mh A = null;
    private long B;
    /* access modifiers changed from: private */
    public boolean C;
    private Random D = null;
    /* access modifiers changed from: private */
    public Handler E = new lu(this);
    /* access modifiers changed from: private */
    public TextView b;
    private TextView c;
    /* access modifiers changed from: private */
    public TextView f;
    /* access modifiers changed from: private */
    public View g;
    /* access modifiers changed from: private */
    public View h;
    private View i;
    /* access modifiers changed from: private */
    public View j;
    private View k;
    /* access modifiers changed from: private */
    public View l;
    /* access modifiers changed from: private */
    public HandyListView m = null;
    /* access modifiers changed from: private */
    public TextSwitcher n;
    private RomaAnimarionView o;
    /* access modifiers changed from: private */
    public MomoAMapView p = null;
    /* access modifiers changed from: private */
    public dl q = null;
    /* access modifiers changed from: private */
    public aq r = null;
    /* access modifiers changed from: private */
    public Location s = null;
    private GeoPoint t = null;
    private mj u = null;
    /* access modifiers changed from: private */
    public String v = PoiTypeDef.All;
    /* access modifiers changed from: private */
    public String w = PoiTypeDef.All;
    private String[] x;
    private int y;
    /* access modifiers changed from: private */
    public int z = 0;

    static /* synthetic */ String a(UserRoamActivity userRoamActivity, boolean z2) {
        String str;
        if (!z2) {
            int i2 = userRoamActivity.y;
            while (i2 == userRoamActivity.y) {
                i2 = userRoamActivity.D.nextInt(userRoamActivity.x.length);
            }
            userRoamActivity.y = i2;
            str = userRoamActivity.x[i2];
        } else {
            str = userRoamActivity.w;
        }
        if (z2) {
            am.a().a(R.raw.romafinal);
        } else {
            am.a().a(R.raw.roma0);
        }
        return str;
    }

    static /* synthetic */ void f(UserRoamActivity userRoamActivity) {
        userRoamActivity.o.b();
        userRoamActivity.h.setVisibility(8);
        userRoamActivity.p.setVisibility(0);
        userRoamActivity.g.clearAnimation();
        userRoamActivity.E.postDelayed(new lw(userRoamActivity), 1500);
    }

    /* access modifiers changed from: private */
    public void g() {
        this.E.removeMessages(100);
        this.E.removeMessages(PurchaseCode.QUERY_OK);
        this.c.setText("按住按钮，开始漫游");
        this.n.setText("可以漫游到其他地方，认识那里的人");
        this.o.b();
    }

    /* access modifiers changed from: private */
    public Animation h() {
        TranslateAnimation translateAnimation = new TranslateAnimation(0.0f, 0.0f, (float) (this.h.getMeasuredHeight() - g.a(90.0f)), 0.0f);
        translateAnimation.setDuration(400);
        return translateAnimation;
    }

    /* access modifiers changed from: private */
    public Animation i() {
        TranslateAnimation translateAnimation = new TranslateAnimation(0.0f, 0.0f, (float) this.j.getMeasuredHeight(), 0.0f);
        translateAnimation.setDuration(400);
        return translateAnimation;
    }

    static /* synthetic */ Animation u(UserRoamActivity userRoamActivity) {
        TranslateAnimation translateAnimation = new TranslateAnimation(0.0f, 0.0f, 0.0f, (float) (userRoamActivity.h.getMeasuredHeight() - g.a(90.0f)));
        translateAnimation.setDuration(400);
        return translateAnimation;
    }

    static /* synthetic */ Animation v(UserRoamActivity userRoamActivity) {
        TranslateAnimation translateAnimation = new TranslateAnimation(0.0f, 0.0f, 0.0f, (float) userRoamActivity.j.getMeasuredHeight());
        translateAnimation.setDuration(400);
        return translateAnimation;
    }

    static /* synthetic */ void w(UserRoamActivity userRoamActivity) {
        if (!userRoamActivity.d.b()) {
            userRoamActivity.a(n.a(userRoamActivity, "开通陌陌会员，可在地图上自由选择地点进行漫游", "开通会员", "取消", new lv(userRoamActivity), (DialogInterface.OnClickListener) null));
            return;
        }
        if (userRoamActivity.A != null) {
            userRoamActivity.A.cancel(true);
            userRoamActivity.g();
        }
        Intent intent = new Intent(userRoamActivity.getApplicationContext(), RomaAMapActivity.class);
        double d = userRoamActivity.d.S;
        double d2 = userRoamActivity.d.T;
        if (z.a(d, d2)) {
            intent.putExtra("latitude", d);
            intent.putExtra("longitude", d2);
        }
        userRoamActivity.startActivityForResult(intent, PurchaseCode.NOT_CMCC_ERR);
    }

    static /* synthetic */ void x(UserRoamActivity userRoamActivity) {
        if (userRoamActivity.A != null) {
            userRoamActivity.A.cancel(true);
        }
        userRoamActivity.C = false;
        userRoamActivity.E.removeMessages(100);
        userRoamActivity.E.removeMessages(PurchaseCode.QUERY_OK);
        userRoamActivity.q.a(false);
        if (!userRoamActivity.h.isShown()) {
            if (userRoamActivity.p.isShown()) {
                userRoamActivity.p.setVisibility(4);
            }
            if (userRoamActivity.g.isShown()) {
                userRoamActivity.g.setVisibility(4);
            }
            if (!userRoamActivity.q.isEmpty()) {
                userRoamActivity.E.postDelayed(new mf(userRoamActivity), 500);
            }
            userRoamActivity.h.setVisibility(0);
        }
        if (!userRoamActivity.j.isShown()) {
            userRoamActivity.j.setVisibility(0);
        }
        userRoamActivity.o.a();
        userRoamActivity.B = System.currentTimeMillis();
        userRoamActivity.c.setText("松开按钮，漫游到新地点");
        userRoamActivity.E.sendEmptyMessage(100);
    }

    static /* synthetic */ void y(UserRoamActivity userRoamActivity) {
        userRoamActivity.c.setText("正在进入漫游城市...");
        userRoamActivity.a(new mh(userRoamActivity, userRoamActivity, System.currentTimeMillis() - userRoamActivity.B));
    }

    public final void a() {
        if (this.t != null) {
            this.p.getController().setZoom(7);
            new Point();
            Point pixels = this.p.getProjection().toPixels(this.t, null);
            pixels.y = (this.h.getMeasuredHeight() - (g.a(90.0f) / 2)) - ((this.h.getMeasuredHeight() / 2) - pixels.y);
            this.p.getController().animateTo(this.p.getProjection().fromPixels(pixels.x, pixels.y));
        }
    }

    public final void a(Location location) {
        this.t = new GeoPoint((int) (location.getLatitude() * 1000000.0d), (int) (location.getLongitude() * 1000000.0d));
        this.p.getController().setZoom(7);
        this.p.getController().setCenter(this.t);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        if (i3 == -1 && i2 == 111) {
            double doubleExtra = intent.getDoubleExtra("latitude", 0.0d);
            double doubleExtra2 = intent.getDoubleExtra("longitude", 0.0d);
            Location location = new Location(LocationManagerProxy.NETWORK_PROVIDER);
            location.setLatitude(doubleExtra);
            location.setLongitude(doubleExtra2);
            a(new mi(this, this, location));
        }
        super.onActivityResult(i2, i3, intent);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (this.d == null || !((MomoApplication) getApplication()).j()) {
            finish();
            return;
        }
        setContentView((int) R.layout.activity_userroam);
        this.r = new aq();
        this.D = new Random();
        getWindow().setFlags(NativeMapEngine.MAX_ICON_SIZE, NativeMapEngine.MAX_ICON_SIZE);
        this.m = (HandyListView) findViewById(R.id.listview);
        this.m.setListPaddingBottom(g.a(90.0f));
        this.m.setListPaddingBackground(getResources().getDrawable(R.color.background_normal));
        this.f = (TextView) findViewById(R.id.userroma_tv_address);
        this.l = findViewById(R.id.userroma_layout_topaddress);
        this.k = g.o().inflate((int) R.layout.listitem_blank, (ViewGroup) null);
        this.k.setLayoutParams(new AbsListView.LayoutParams(-1, g.a(90.0f)));
        this.m.addHeaderView(this.k);
        View inflate = g.o().inflate((int) R.layout.include_roma_address, (ViewGroup) null);
        this.m.addHeaderView(inflate);
        this.b = (TextView) inflate.findViewById(R.id.userroma_tv_address);
        this.h = findViewById(R.id.userroma_layout_welcome);
        this.p = (MomoAMapView) findViewById(R.id.mapview);
        this.g = findViewById(R.id.userroma_layout_userlist);
        this.o = (RomaAnimarionView) findViewById(R.id.userroma_surfaceview);
        this.i = findViewById(R.id.userroma_btn_launch);
        this.j = findViewById(R.id.userroma_layout_bottombar);
        this.n = (TextSwitcher) findViewById(R.id.userroma_tvswitcher_loadingcover);
        TextSwitcher textSwitcher = this.n;
        AnimationSet animationSet = new AnimationSet(true);
        animationSet.setInterpolator(new DecelerateInterpolator());
        animationSet.setDuration(100);
        animationSet.addAnimation(new AlphaAnimation(0.0f, 1.0f));
        animationSet.addAnimation(new TranslateAnimation(0.0f, 0.0f, 30.0f, 0.0f));
        textSwitcher.setInAnimation(animationSet);
        TextSwitcher textSwitcher2 = this.n;
        AnimationSet animationSet2 = new AnimationSet(true);
        animationSet2.setInterpolator(new AccelerateInterpolator());
        animationSet2.setDuration(100);
        animationSet2.addAnimation(new AlphaAnimation(1.0f, 0.0f));
        animationSet2.addAnimation(new TranslateAnimation(0.0f, 0.0f, 0.0f, -30.0f));
        textSwitcher2.setOutAnimation(animationSet2);
        a(this.p);
        this.u = new mj(this, this.p);
        this.p.getOverlays().add(this.u);
        this.n.setFactory(new lx(this));
        this.c = (TextView) findViewById(R.id.userroma_tv_label);
        this.c.setText("按住按钮，开始漫游");
        this.n.setText("可以漫游到其他地方，认识那里的人");
        this.k.setOnClickListener(new ly(this));
        this.m.setOnItemClickListener(new lz(this));
        d().a(new bi(getApplicationContext()).a("会员漫游"), new ma(this));
        this.i.setOnTouchListener(new mb(this));
        this.m.setOnScrollListener(new mc(this));
        this.o.setStatusListener(new md(this));
        this.q = new dl(this, new ArrayList(), this.m, false);
        this.m.setAdapter((ListAdapter) this.q);
        this.x = getResources().getStringArray(R.array.roma_city);
        a(new mg(this));
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.o.c();
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        boolean z2 = false;
        if (i2 == 4 && keyEvent.getAction() == keyEvent.getAction()) {
            if (this.p.isShown() && !this.m.isShown() && !this.q.isEmpty()) {
                this.p.invalidate();
                this.g.clearAnimation();
                this.g.setAnimation(h());
                this.g.setVisibility(0);
                if (!this.j.isShown()) {
                    this.j.setAnimation(i());
                    this.j.setVisibility(0);
                }
                a();
                z2 = true;
            }
            if (z2) {
                return true;
            }
        }
        return super.onKeyDown(i2, keyEvent);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (this.A != null) {
            this.A.cancel(true);
        }
        g();
    }
}
