package com.immomo.momo.android.activity;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.view.HeaderLayout;
import com.immomo.momo.android.view.PushDownLayout;
import com.immomo.momo.g;
import com.immomo.momo.protocol.imjson.c.a;
import com.immomo.momo.protocol.imjson.d;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import mm.purchasesdk.PurchaseCode;

public abstract class lh extends ar {
    private static Map X = new HashMap();
    private boolean O = false;
    private boolean P = false;
    private boolean Q = false;
    /* access modifiers changed from: private */
    public View R = null;
    private TextView S = null;
    private am T = new am();
    private ImageView U = null;
    private ImageView V = null;
    private View W;
    private List Y = new ArrayList();
    private View.OnClickListener Z = new li(this);
    private int aa = 1;
    private Handler ab = new lk(this);

    private void O() {
        if (this.R == null) {
            this.L.c("topTipView==null");
            return;
        }
        this.L.a((Object) ("reflushTips, tipsList=" + this.T));
        an a2 = this.T.a();
        if (a2 != null) {
            a(a2.c, 0, a2.d ? R.drawable.ic_common_arrow_right : 0);
            boolean z = a2.d;
            if (this.R != null) {
                this.R.setClickable(z);
            }
            this.R.setTag(R.id.tag_item, a2);
            return;
        }
        a(1000L);
    }

    public static void R() {
        X.clear();
    }

    protected static void Y() {
    }

    public static lh a(Context context, Class cls, boolean z) {
        String name = cls.getName();
        return (!z || !X.containsKey(name)) ? (lh) Fragment.a(context, name, (Bundle) null) : (lh) X.get(name);
    }

    public boolean F() {
        return true;
    }

    public HeaderLayout K() {
        return ((ao) c()).m();
    }

    public void S() {
        this.P = true;
    }

    public final boolean T() {
        return this.P;
    }

    public final void U() {
        this.Q = true;
    }

    /* access modifiers changed from: protected */
    public View V() {
        return g.o().inflate((int) R.layout.common_toptip, (ViewGroup) null);
    }

    public final View W() {
        return this.W;
    }

    /* access modifiers changed from: protected */
    public void X() {
        try {
            ViewGroup viewGroup = (ViewGroup) c((int) R.id.layout_content);
            if (viewGroup != null && this.R != null) {
                if (viewGroup instanceof LinearLayout) {
                    ((LinearLayout) viewGroup).setOrientation(1);
                }
                viewGroup.addView(this.R, 0);
                this.L.a((Object) "onFillTopTip success");
            }
        } catch (Exception e) {
            this.L.a((Throwable) e);
        }
    }

    /* access modifiers changed from: protected */
    public final void Z() {
        this.R.setVisibility(8);
    }

    /* access modifiers changed from: protected */
    public final a a(int i, String... strArr) {
        ll llVar = new ll(this, i, strArr);
        this.Y.add(llVar);
        g.d().i().a(llVar);
        return llVar;
    }

    /* access modifiers changed from: protected */
    public final void a(long j) {
        this.ab.sendEmptyMessageDelayed(123, j);
    }

    public final void a(Activity activity) {
        super.a(activity);
    }

    public void a(Context context, HeaderLayout headerLayout) {
    }

    public final void a(AsyncTask asyncTask) {
        if (c() != null) {
            ((ao) c()).b(asyncTask);
        }
    }

    public void a(View view) {
        this.W = view;
    }

    /* access modifiers changed from: protected */
    public void a(an anVar) {
    }

    /* access modifiers changed from: protected */
    public final void a(String str, int i, int i2) {
        Bitmap decodeResource = i > 0 ? BitmapFactory.decodeResource(d(), i) : null;
        Bitmap decodeResource2 = i2 > 0 ? BitmapFactory.decodeResource(d(), i2) : null;
        if (this.R != null) {
            this.L.a((Object) ("reflushTips, showTopTip, msg=" + str));
            if (this.S != null) {
                TextView textView = this.S;
                if (str == null) {
                    str = PoiTypeDef.All;
                }
                textView.setText(str);
            }
            if (this.U != null) {
                if (decodeResource != null) {
                    this.U.setImageBitmap(decodeResource);
                    this.U.setVisibility(0);
                } else {
                    this.U.setVisibility(8);
                }
                this.U.setOnClickListener(this.Z);
            }
            if (this.V != null) {
                if (decodeResource2 != null) {
                    this.V.setImageBitmap(decodeResource2);
                    this.V.setVisibility(0);
                } else {
                    this.V.setVisibility(8);
                }
                this.V.setOnClickListener(this.Z);
            }
            this.R.setTag(R.id.tag_item, null);
            if (this.R != null) {
                if (!this.R.isShown()) {
                    this.R.setVisibility(0);
                }
                this.ab.removeMessages(123);
            }
        }
    }

    public boolean a(int i, KeyEvent keyEvent) {
        return false;
    }

    /* access modifiers changed from: protected */
    public boolean a(Bundle bundle, String str) {
        if ("actions.removeimjwarning".equals(str)) {
            if (this.R != null && ab()) {
                c(new an(1008));
            }
        } else if ("actions.imjwarning".equals(str) && this.R != null) {
            String string = bundle.getString("imwmsg");
            String string2 = bundle.getString("imwtype");
            if (string != null) {
                if ("XMPP_AUTHFAILED".equals(string2)) {
                    b(new an(1007, string, Integer.MAX_VALUE));
                } else {
                    b(new an(string));
                }
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public boolean aa() {
        return false;
    }

    /* access modifiers changed from: protected */
    public final boolean ab() {
        return this.R != null && this.R.getVisibility() == 0;
    }

    public final void ac() {
        this.T.remove(new an(1006));
        O();
    }

    public final void ad() {
        d(true);
        ae();
    }

    /* access modifiers changed from: protected */
    public void ae() {
    }

    public final void af() {
        d(false);
        ag();
    }

    /* access modifiers changed from: protected */
    public void ag() {
    }

    public void ah() {
    }

    public void ai() {
    }

    public void aj() {
        this.O = true;
    }

    public final boolean ak() {
        return this.O;
    }

    public final void b(Bundle bundle) {
        super.b(bundle);
        this.L.a((Object) ("TabOptionFragment onActivityCreated--" + getClass().getName()));
        if (this.Q) {
            ad();
            S();
        }
    }

    public final void b(an anVar) {
        boolean z;
        this.L.a((Object) ("message=" + anVar));
        if (!((Boolean) this.N.b("tips_" + anVar.b, false)).booleanValue()) {
            int indexOf = this.T.indexOf(anVar);
            if (anVar.f1029a <= 0) {
                int i = this.aa;
                this.aa = i + 1;
                anVar.f1029a = i;
                z = true;
            } else {
                z = false;
            }
            if (indexOf < 0) {
                this.T.add(anVar);
            } else if (z) {
                int i2 = this.aa;
                this.aa = i2 + 1;
                anVar.f1029a = i2;
                this.T.remove(anVar);
                b(anVar);
            }
            O();
            return;
        }
        this.L.a((Object) ("miss, " + anVar));
    }

    public boolean b(int i, KeyEvent keyEvent) {
        return false;
    }

    public final void c(an anVar) {
        this.T.remove(anVar);
        O();
    }

    /* access modifiers changed from: protected */
    public void i(Bundle bundle) {
        if (c((int) R.id.layout_content) != null) {
            if (!I()) {
                this.R = V();
                X();
                if (this.R != null) {
                    this.S = (TextView) this.R.findViewById(R.id.toptip_text);
                    this.U = (ImageView) this.R.findViewById(R.id.toptip_icon_left);
                    this.V = (ImageView) this.R.findViewById(R.id.toptip_icon_right);
                    this.R.setOnClickListener(this.Z);
                    this.S.setClickable(false);
                }
            } else {
                this.T.clear();
                if (this.R != null) {
                    this.R.setVisibility(8);
                }
            }
            if (aa()) {
                StringBuilder sb = new StringBuilder();
                try {
                    boolean a2 = d.a(sb, null);
                    String sb2 = android.support.v4.b.a.a(sb) ? "通讯服务已经断开" : sb.toString();
                    this.L.a((Object) ("reflushImjStatus, result=" + a2 + ", text=" + sb2));
                    if (!a2) {
                        if (!ab() && (this.R instanceof PushDownLayout)) {
                            ((PushDownLayout) this.R).a();
                        }
                        b(new an(1008, sb2, Integer.MAX_VALUE));
                    } else if (ab()) {
                        c(new an(1008));
                    }
                } catch (Exception e) {
                    this.L.a((Throwable) e);
                }
                a((int) PurchaseCode.QUERY_FROZEN, "actions.removeimjwarning", "actions.imjwarning");
            }
            O();
        }
    }

    public void p() {
        super.p();
        if (this.Y.size() > 0) {
            for (a b : this.Y) {
                g.d().i().b(b);
            }
        }
        this.Y.clear();
        this.P = false;
        this.Q = false;
    }

    public final void r() {
        super.r();
    }
}
