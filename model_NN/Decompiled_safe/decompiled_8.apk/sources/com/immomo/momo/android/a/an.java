package com.immomo.momo.android.a;

import android.view.View;
import com.immomo.momo.android.activity.d;
import com.immomo.momo.service.bean.i;
import com.immomo.momo.service.bean.j;

final class an implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private j f721a;
    private i b = null;
    private /* synthetic */ ae c;

    public an(ae aeVar, j jVar, i iVar) {
        this.c = aeVar;
        this.f721a = jVar;
        this.b = iVar;
    }

    public final void onClick(View view) {
        if (d.a(this.f721a.b())) {
            d.a(this.f721a.f(), this.c.d());
        } else if ("add_friends_contacts".equals(this.f721a.b())) {
            ae.a(this.c, this.b, this.f721a);
        } else if ("add_friends_wbcontacts".equals(this.f721a.b())) {
            ae.a(this.c, this.b, this.f721a);
        } else if ("add_friends_qqwbcontacts".equals(this.f721a.b())) {
            ae.a(this.c, this.b, this.f721a);
        } else if ("add_friends_rrcontacts".equals(this.f721a.b())) {
            ae.a(this.c, this.b, this.f721a);
        } else if (this.f721a.b().startsWith("block")) {
            ae.a(this.c, this.f721a, this.b);
        } else {
            ae aeVar = this.c;
            if (ae.c(this.f721a.c())) {
                this.c.e.b(new ap(this.c, this.c.e, this.f721a, this.b));
            }
        }
    }
}
