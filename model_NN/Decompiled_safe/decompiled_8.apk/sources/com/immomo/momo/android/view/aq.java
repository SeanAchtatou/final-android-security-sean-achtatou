package com.immomo.momo.android.view;

import android.view.animation.Animation;
import android.view.animation.Transformation;

final class aq extends Animation {

    /* renamed from: a  reason: collision with root package name */
    private boolean f2725a = false;
    private int b = 0;
    private int c = 0;
    private int d = 0;
    private int e = 0;
    private /* synthetic */ FlingGallery f;

    public aq(FlingGallery flingGallery) {
        this.f = flingGallery;
        setInterpolator(getInterpolator());
    }

    public final void a(int i) {
        char c2 = 65535;
        if (this.b != i) {
            if (this.f2725a) {
                FlingGallery flingGallery = this.f;
                char c3 = i == FlingGallery.e(this.b) ? (char) 1 : 65535;
                if (this.e < 0) {
                    c2 = 1;
                }
                if (c2 == c3) {
                    this.f.n[0].a(this.d, this.b);
                    this.f.n[1].a(this.d, this.b);
                    this.f.n[2].a(this.d, this.b);
                }
            }
            this.b = i;
        }
        this.c = this.f.n[this.b].a();
        this.d = FlingGallery.a(this.f, this.b, this.b);
        this.e = this.d - this.c;
        setDuration((long) this.f.f2629a);
        setInterpolator(this.f.p);
        this.f2725a = true;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0020, code lost:
        if (r0 == com.immomo.momo.android.view.FlingGallery.f(r4.b)) goto L_0x0022;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void applyTransformation(float r5, android.view.animation.Transformation r6) {
        /*
            r4 = this;
            r0 = 1065353216(0x3f800000, float:1.0)
            int r1 = (r5 > r0 ? 1 : (r5 == r0 ? 0 : -1))
            if (r1 <= 0) goto L_0x0007
            r5 = r0
        L_0x0007:
            int r0 = r4.c
            int r1 = r4.e
            float r1 = (float) r1
            float r1 = r1 * r5
            int r1 = (int) r1
            int r1 = r1 + r0
            r0 = 0
        L_0x0010:
            r2 = 3
            if (r0 < r2) goto L_0x0014
            return
        L_0x0014:
            int r2 = r4.e
            if (r2 <= 0) goto L_0x0022
            com.immomo.momo.android.view.FlingGallery r2 = r4.f
            int r2 = r4.b
            int r2 = com.immomo.momo.android.view.FlingGallery.f(r2)
            if (r0 != r2) goto L_0x0030
        L_0x0022:
            int r2 = r4.e
            if (r2 >= 0) goto L_0x003d
            com.immomo.momo.android.view.FlingGallery r2 = r4.f
            int r2 = r4.b
            int r2 = com.immomo.momo.android.view.FlingGallery.e(r2)
            if (r0 == r2) goto L_0x003d
        L_0x0030:
            com.immomo.momo.android.view.FlingGallery r2 = r4.f
            com.immomo.momo.android.view.ar[] r2 = r2.n
            r2 = r2[r0]
            int r3 = r4.b
            r2.a(r1, r3)
        L_0x003d:
            int r0 = r0 + 1
            goto L_0x0010
        */
        throw new UnsupportedOperationException("Method not decompiled: com.immomo.momo.android.view.aq.applyTransformation(float, android.view.animation.Transformation):void");
    }

    public final boolean getTransformation(long j, Transformation transformation) {
        if (super.getTransformation(j, transformation)) {
            return !this.f.f && !this.f.g;
        }
        this.f.n[0].a(this.d, this.b);
        this.f.n[1].a(this.d, this.b);
        this.f.n[2].a(this.d, this.b);
        this.f2725a = false;
        return false;
    }
}
