package com.immomo.momo.android.a;

import android.graphics.Bitmap;
import android.support.v4.b.a;
import com.immomo.momo.android.c.p;
import com.immomo.momo.service.bean.r;
import com.immomo.momo.util.c;
import java.io.File;

final class bn implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ bm f739a;
    private final /* synthetic */ File b;
    private final /* synthetic */ r c;
    private final /* synthetic */ p d;

    bn(bm bmVar, File file, r rVar, p pVar) {
        this.f739a = bmVar;
        this.b = file;
        this.c = rVar;
        this.d = pVar;
    }

    public final void run() {
        Bitmap l = a.l(this.b.getPath());
        if (l != null) {
            c.a().a(this.c.b(), l);
            this.d.a(l);
            return;
        }
        this.f739a.a(this.c);
    }
}
