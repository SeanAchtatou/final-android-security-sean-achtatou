package com.immomo.momo.android.activity.group;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Message;
import com.immomo.momo.android.c.g;
import com.immomo.momo.service.bean.au;

final class dh implements g {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ GroupProfileActivity f1605a;
    private final /* synthetic */ au b;

    dh(GroupProfileActivity groupProfileActivity, au auVar) {
        this.f1605a = groupProfileActivity;
        this.b = auVar;
    }

    public final /* synthetic */ void a(Object obj) {
        Message message = new Message();
        Bundle bundle = new Bundle();
        bundle.putString("guid", this.b.b);
        message.setData(bundle);
        message.obj = (Bitmap) obj;
        message.what = 12;
        this.f1605a.ab.sendMessage(message);
    }
}
