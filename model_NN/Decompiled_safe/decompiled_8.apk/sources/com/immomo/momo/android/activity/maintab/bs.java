package com.immomo.momo.android.activity.maintab;

import android.view.View;
import com.immomo.momo.R;
import com.immomo.momo.android.view.a.n;

final class bs implements View.OnClickListener {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public /* synthetic */ bh f1867a;

    bs(bh bhVar) {
        this.f1867a = bhVar;
    }

    public final void onClick(View view) {
        if (this.f1867a.ac > 0) {
            n.a(this.f1867a.c(), "本操作将清除当前未读消息提示，确认进行此操作吗?", new bt(this), new bu()).show();
        } else {
            this.f1867a.d((int) R.string.msg_notnews);
        }
    }
}
