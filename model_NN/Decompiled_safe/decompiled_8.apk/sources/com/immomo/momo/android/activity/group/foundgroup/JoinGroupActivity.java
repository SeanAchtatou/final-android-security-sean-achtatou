package com.immomo.momo.android.activity.group.foundgroup;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.b.a;
import android.text.Editable;
import android.text.Selection;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.ah;
import com.immomo.momo.util.k;
import org.json.JSONException;
import org.json.JSONObject;

public class JoinGroupActivity extends ah {
    /* access modifiers changed from: private */
    public String h;
    private TextView i;
    /* access modifiers changed from: private */
    public EditText j;
    private Button k;

    static /* synthetic */ boolean b(JoinGroupActivity joinGroupActivity) {
        if (joinGroupActivity.j.getText().toString().trim().length() != 0) {
            return true;
        }
        joinGroupActivity.a((CharSequence) "请提交申请内容");
        Editable text = joinGroupActivity.j.getText();
        Selection.setSelection(text, 0);
        text.clear();
        return false;
    }

    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        super.a(bundle);
        Intent intent = getIntent();
        String stringExtra = intent.getStringExtra("agree_tip");
        this.h = intent.getStringExtra("gid");
        setContentView((int) R.layout.activity_joingroup);
        this.i = (TextView) findViewById(R.id.person_group_information);
        this.j = (EditText) findViewById(R.id.apply_for_content);
        setTitle((int) R.string.str_join_group_title);
        ((Button) findViewById(R.id.btn_back)).setOnClickListener(new t(this));
        this.k = (Button) findViewById(R.id.btn_applyfor);
        this.k.setOnClickListener(new u(this));
        this.i.setText(a.a(stringExtra) ? PoiTypeDef.All : stringExtra);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("group_id", this.h);
            new k("PO", "P312", jSONObject).e();
        } catch (JSONException e) {
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("group_id", this.h);
            new k("PI", "P312", jSONObject).e();
        } catch (JSONException e) {
        }
    }
}
