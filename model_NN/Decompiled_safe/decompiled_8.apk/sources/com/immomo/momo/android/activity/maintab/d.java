package com.immomo.momo.android.activity.maintab;

import android.os.Handler;
import android.os.Message;

final class d extends Handler {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ MaintabActivity f1886a;

    d(MaintabActivity maintabActivity) {
        this.f1886a = maintabActivity;
    }

    public final void handleMessage(Message message) {
        switch (message.what) {
            case 11:
                if (this.f1886a.m != null) {
                    this.f1886a.m.c();
                    this.f1886a.m = null;
                    return;
                }
                return;
            case 12:
                if (this.f1886a.m != null) {
                    this.f1886a.m.c();
                    this.f1886a.m = null;
                    this.f1886a.v();
                    return;
                }
                return;
            default:
                return;
        }
    }
}
