package com.immomo.momo.android.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.immomo.momo.R;

public class PublishButton extends RelativeLayout {

    /* renamed from: a  reason: collision with root package name */
    private ImageView f2646a;
    private ImageView b;

    public PublishButton(Context context) {
        super(context);
        a();
    }

    public PublishButton(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a();
    }

    public PublishButton(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        a();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, com.immomo.momo.android.view.PublishButton, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    private void a() {
        ((LayoutInflater) getContext().getSystemService("layout_inflater")).inflate((int) R.layout.include_publishbtn, (ViewGroup) this, true);
        this.b = (ImageView) findViewById(R.id.iv_icon);
        this.f2646a = (ImageView) findViewById(R.id.iv_triangle);
    }

    public void setIcon(int i) {
        this.b.setImageResource(i);
    }

    public void setSelected(boolean z) {
        super.setSelected(z);
        this.f2646a.setVisibility(z ? 0 : 8);
    }
}
