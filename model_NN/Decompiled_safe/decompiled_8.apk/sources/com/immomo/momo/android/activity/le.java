package com.immomo.momo.android.activity;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.c.u;
import com.immomo.momo.g;
import com.immomo.momo.protocol.a.c;
import com.immomo.momo.service.an;
import com.immomo.momo.service.bean.ba;
import com.immomo.momo.util.m;
import java.util.List;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.atomic.AtomicInteger;

final class le extends d {

    /* renamed from: a  reason: collision with root package name */
    private m f1809a = new m(this);

    public le(Context context) {
        super(context);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.ak.a(java.lang.String, java.lang.Integer):int
     arg types: [java.lang.String, int]
     candidates:
      com.immomo.momo.util.ak.a(android.content.Context, java.lang.String):com.immomo.momo.util.ak
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Long):long
      com.immomo.momo.util.ak.a(java.lang.String, int):void
      com.immomo.momo.util.ak.a(java.lang.String, long):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.String):void
      com.immomo.momo.util.ak.a(java.lang.String, boolean):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Boolean):boolean
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Integer):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.Integer]
     candidates:
      com.immomo.momo.util.ak.a(android.content.Context, java.lang.String):com.immomo.momo.util.ak
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Integer):int
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Long):long
      com.immomo.momo.util.ak.a(java.lang.String, int):void
      com.immomo.momo.util.ak.a(java.lang.String, long):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.String):void
      com.immomo.momo.util.ak.a(java.lang.String, boolean):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Boolean):boolean
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void */
    /* access modifiers changed from: protected */
    public final Object a(Object... objArr) {
        int i = 0;
        AtomicInteger atomicInteger = new AtomicInteger(g.d().f668a.a("spversion", (Integer) 0));
        List a2 = c.a().a(atomicInteger);
        g.d().f668a.a("spversion", (Object) Integer.valueOf(atomicInteger.get()));
        this.f1809a.a((Object) (" CheckSplashImages->" + a2));
        if (a2 == null) {
            return null;
        }
        new an().a(a2);
        ThreadPoolExecutor c = u.c();
        while (true) {
            int i2 = i;
            if (i2 >= a2.size()) {
                return null;
            }
            ba baVar = (ba) a2.get(i2);
            lb.b(c, baVar.i(), baVar.a());
            lb.b(c, baVar.k(), baVar.e());
            lb.b(c, baVar.j(), baVar.d());
            i = i2 + 1;
        }
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        this.f1809a.a((Throwable) exc);
    }
}
