package com.immomo.momo.android.activity.discuss;

import android.content.Context;
import android.content.Intent;
import com.immomo.momo.a.n;
import com.immomo.momo.android.broadcast.t;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.h;

final class aa extends d {

    /* renamed from: a  reason: collision with root package name */
    private v f1224a = null;
    /* access modifiers changed from: private */
    public /* synthetic */ DiscussProfileActivity c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public aa(DiscussProfileActivity discussProfileActivity, Context context) {
        super(context);
        this.c = discussProfileActivity;
        this.f1224a = new v(context);
        this.f1224a.a("请求提交中");
        this.f1224a.setCancelable(true);
        this.f1224a.setOnCancelListener(new ab(this));
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        h.a().a(this.c.q, this.c.y);
        this.c.l = this.c.j.d(this.c.f.h, this.c.q);
        DiscussProfileActivity discussProfileActivity = this.c;
        this.c.f.h.equals(this.c.y.c);
        DiscussProfileActivity.u();
        return 1;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        if (this.c.i && this.f1224a != null) {
            this.c.a(this.f1224a);
        }
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        if (exc instanceof com.immomo.momo.a.h) {
            this.c.j.a(this.c.q, 2);
            a(exc.getMessage());
            this.c.finish();
        } else if (exc instanceof n) {
            a(exc.getMessage());
            this.c.j.a(this.c.q, 3);
            this.c.finish();
        } else {
            super.a(exc);
        }
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        this.c.y();
        DiscussProfileActivity.k(this.c);
        Intent intent = new Intent(t.d);
        intent.putExtra("disid", this.c.q);
        this.c.sendBroadcast(intent);
    }

    /* access modifiers changed from: protected */
    public final void b() {
        if (this.c.i && this.f1224a != null) {
            this.c.p();
        }
    }
}
