package com.immomo.momo.android.view;

import android.text.Editable;

final class ab extends Editable.Factory {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public /* synthetic */ EmoteEditeText f2712a;

    ab(EmoteEditeText emoteEditeText) {
        this.f2712a = emoteEditeText;
    }

    public final Editable newEditable(CharSequence charSequence) {
        return new ac(this, charSequence);
    }
}
