package com.immomo.momo.android.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.immomo.momo.util.m;

public class SettingInfoLayout extends LinearLayout {

    /* renamed from: a  reason: collision with root package name */
    private m f2657a = new m(getClass().getSimpleName());

    public SettingInfoLayout(Context context) {
        super(context);
    }

    public SettingInfoLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        boolean z;
        super.onMeasure(i, i2);
        boolean z2 = true;
        int childCount = getChildCount();
        int i3 = 2;
        while (i3 < childCount) {
            try {
                ViewGroup viewGroup = (ViewGroup) getChildAt(i3);
                if (viewGroup.getChildCount() <= 0 || viewGroup.getVisibility() != 0) {
                    z = z2;
                } else {
                    viewGroup.getChildAt(0).setVisibility(z2 ? 8 : 0);
                    z = false;
                }
                i3++;
                z2 = z;
            } catch (Exception e) {
                this.f2657a.b((Object) ("Exception" + e.toString()));
                return;
            }
        }
    }
}
