package com.immomo.momo.android.activity.message;

import android.support.v4.b.a;
import android.view.animation.Animation;

final class ax implements Animation.AnimationListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ChatActivity f1930a;

    ax(ChatActivity chatActivity) {
        this.f1930a = chatActivity;
    }

    public final void onAnimationEnd(Animation animation) {
        this.f1930a.V.setImageResource(a.b(this.f1930a.Q.e(), this.f1930a.Q.d() < 0.0f));
        this.f1930a.X.setVisibility(8);
    }

    public final void onAnimationRepeat(Animation animation) {
    }

    public final void onAnimationStart(Animation animation) {
    }
}
