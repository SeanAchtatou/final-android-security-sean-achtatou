package com.immomo.momo.android.view.a;

import android.content.Context;
import android.content.DialogInterface;
import com.immomo.momo.R;
import com.immomo.momo.android.c.ae;
import com.immomo.momo.android.c.z;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.bf;

final class t implements al {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ Context f2704a;
    private final /* synthetic */ bf b;
    private final /* synthetic */ bf c;
    private final /* synthetic */ z d;

    t(Context context, bf bfVar, bf bfVar2, z zVar) {
        this.f2704a = context;
        this.b = bfVar;
        this.c = bfVar2;
        this.d = zVar;
    }

    public final void a(int i) {
        String[] stringArray = g.l().getStringArray(R.array.report_items);
        if (i >= stringArray.length) {
            return;
        }
        if (i == 0 || i == 3 || i == 4) {
            n nVar = new n(this.f2704a);
            nVar.a();
            nVar.setTitle("确认举报");
            nVar.a(0, (int) R.string.dialog_btn_confim, new u(this.f2704a, this.b, this.c, this.d, stringArray, i));
            nVar.a(1, (int) R.string.dialog_btn_cancel, (DialogInterface.OnClickListener) null);
            nVar.setContentView((int) R.layout.dialog_report_tip);
            nVar.show();
            return;
        }
        new ae(this.f2704a, this.b, this.c, this.d).execute(new String[]{stringArray[i]});
    }
}
