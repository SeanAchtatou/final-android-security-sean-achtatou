package com.immomo.momo.android.view.dragsort;

import android.util.SparseIntArray;
import java.util.ArrayList;

final class q {

    /* renamed from: a  reason: collision with root package name */
    private SparseIntArray f2799a = new SparseIntArray(3);
    private ArrayList b = new ArrayList(3);
    private int c = 3;

    public final int a(int i) {
        return this.f2799a.get(i, -1);
    }

    public final void a() {
        this.f2799a.clear();
        this.b.clear();
    }

    public final void a(int i, int i2) {
        int i3 = this.f2799a.get(i, -1);
        if (i3 != i2) {
            if (i3 != -1) {
                this.b.remove(Integer.valueOf(i));
            } else if (this.f2799a.size() == this.c) {
                this.f2799a.delete(((Integer) this.b.remove(0)).intValue());
            }
            this.f2799a.put(i, i2);
            this.b.add(Integer.valueOf(i));
        }
    }
}
