package com.immomo.momo.android.activity.discuss;

import android.content.DialogInterface;

final class ab implements DialogInterface.OnCancelListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ aa f1225a;

    ab(aa aaVar) {
        this.f1225a = aaVar;
    }

    public final void onCancel(DialogInterface dialogInterface) {
        this.f1225a.cancel(true);
        this.f1225a.c.finish();
    }
}
