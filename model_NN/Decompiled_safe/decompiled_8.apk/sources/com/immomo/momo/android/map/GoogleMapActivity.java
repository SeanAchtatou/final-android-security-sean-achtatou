package com.immomo.momo.android.map;

import android.location.Location;
import android.os.Handler;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapView;
import com.google.android.maps.MyLocationOverlay;
import com.immomo.momo.util.m;

public class GoogleMapActivity extends MapActivity {

    /* renamed from: a  reason: collision with root package name */
    private MyLocationOverlay f2451a = null;
    /* access modifiers changed from: private */
    public GeoPoint b = null;
    /* access modifiers changed from: private */
    public Location c = null;
    /* access modifiers changed from: private */
    public MapView d = null;
    private boolean e = false;
    private m f = new m(getClass().getSimpleName());
    private String g = null;
    private String h = null;
    /* access modifiers changed from: private */
    public boolean i = false;

    public GoogleMapActivity() {
        new Handler();
    }

    /* JADX WARN: Type inference failed for: r7v0, types: [android.content.Context, com.immomo.momo.android.map.GoogleMapActivity] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static /* synthetic */ android.graphics.Bitmap b(com.immomo.momo.android.map.GoogleMapActivity r7) {
        /*
            r6 = 2131166866(0x7f070692, float:1.794799E38)
            r5 = 2131166865(0x7f070691, float:1.7947987E38)
            r4 = 8
            r3 = 0
            android.view.LayoutInflater r0 = android.view.LayoutInflater.from(r7)
            r1 = 2130903567(0x7f03020f, float:1.7413956E38)
            r2 = 0
            android.view.View r1 = r0.inflate(r1, r2)
            java.lang.String r0 = r7.g
            boolean r0 = android.support.v4.b.a.a(r0)
            if (r0 != 0) goto L_0x0059
            android.view.View r0 = r1.findViewById(r5)
            android.widget.TextView r0 = (android.widget.TextView) r0
            java.lang.String r2 = r7.g
            r0.setText(r2)
        L_0x0028:
            java.lang.String r0 = r7.h
            boolean r0 = android.support.v4.b.a.a(r0)
            if (r0 != 0) goto L_0x0063
            android.view.View r0 = r1.findViewById(r6)
            android.widget.TextView r0 = (android.widget.TextView) r0
            java.lang.String r2 = r7.h
            r0.setText(r2)
        L_0x003b:
            int r0 = android.view.View.MeasureSpec.makeMeasureSpec(r3, r3)
            int r2 = android.view.View.MeasureSpec.makeMeasureSpec(r3, r3)
            r1.measure(r0, r2)
            int r0 = r1.getMeasuredWidth()
            int r2 = r1.getMeasuredHeight()
            r1.layout(r3, r3, r0, r2)
            r1.buildDrawingCache()
            android.graphics.Bitmap r0 = r1.getDrawingCache()
            return r0
        L_0x0059:
            android.view.View r0 = r1.findViewById(r5)
            android.widget.TextView r0 = (android.widget.TextView) r0
            r0.setVisibility(r4)
            goto L_0x0028
        L_0x0063:
            android.view.View r0 = r1.findViewById(r6)
            android.widget.TextView r0 = (android.widget.TextView) r0
            r0.setVisibility(r4)
            goto L_0x003b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.immomo.momo.android.map.GoogleMapActivity.b(com.immomo.momo.android.map.GoogleMapActivity):android.graphics.Bitmap");
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [android.content.Context, com.immomo.momo.android.map.GoogleMapActivity] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static /* synthetic */ void e(com.immomo.momo.android.map.GoogleMapActivity r2) {
        /*
            com.immomo.momo.android.view.a.v r0 = new com.immomo.momo.android.view.a.v
            r0.<init>(r2)
            r1 = 1
            r0.setCancelable(r1)
            java.lang.String r1 = "请稍候,正在获取您的当前位置"
            r0.a(r1)
            r0.show()
            com.immomo.momo.android.map.ad r1 = new com.immomo.momo.android.map.ad
            r1.<init>(r2, r0)
            com.immomo.momo.g.R()     // Catch:{ Exception -> 0x0022 }
            com.immomo.momo.android.map.ae r0 = new com.immomo.momo.android.map.ae     // Catch:{ Exception -> 0x0022 }
            r0.<init>(r1)     // Catch:{ Exception -> 0x0022 }
            com.immomo.momo.android.b.z.a(r0)     // Catch:{ Exception -> 0x0022 }
        L_0x0021:
            return
        L_0x0022:
            r0 = move-exception
            com.immomo.momo.util.m r1 = r2.f
            r1.a(r0)
            r0 = 2131493068(0x7f0c00cc, float:1.8609606E38)
            com.immomo.momo.util.ao.g(r0)
            goto L_0x0021
        */
        throw new UnsupportedOperationException("Method not decompiled: com.immomo.momo.android.map.GoogleMapActivity.e(com.immomo.momo.android.map.GoogleMapActivity):void");
    }

    /* access modifiers changed from: protected */
    public boolean isRouteDisplayed() {
        return true;
    }

    /* JADX WARN: Type inference failed for: r12v0, types: [android.content.Context, com.google.android.maps.MapActivity, com.immomo.momo.android.map.GoogleMapActivity] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    protected void onCreate(android.os.Bundle r13) {
        /*
            r12 = this;
            r11 = 2131493135(0x7f0c010f, float:1.8609742E38)
            r9 = 4696837146684686336(0x412e848000000000, double:1000000.0)
            r2 = -4616189618054758400(0xbff0000000000000, double:-1.0)
            r8 = 0
            r7 = 1
            com.immomo.momo.android.map.GoogleMapActivity.super.onCreate(r13)
            r0 = 2130903104(0x7f030040, float:1.7413017E38)
            r12.setContentView(r0)
            android.content.Intent r0 = r12.getIntent()
            android.os.Bundle r4 = r0.getExtras()
            if (r4 != 0) goto L_0x00ba
            com.immomo.momo.util.ao.e(r11)
        L_0x0022:
            r0 = 2131165395(0x7f0700d3, float:1.7945006E38)
            android.view.View r0 = r12.findViewById(r0)
            com.google.android.maps.MapView r0 = (com.google.android.maps.MapView) r0
            r12.d = r0
            com.google.android.maps.MapView r0 = r12.d
            r0.setBuiltInZoomControls(r7)
            com.google.android.maps.MapView r0 = r12.d
            r0.displayZoomControls(r7)
            com.google.android.maps.MapView r0 = r12.d
            r0.setEnabled(r7)
            com.google.android.maps.MapView r0 = r12.d
            r0.setClickable(r7)
            com.google.android.maps.MapView r0 = r12.d
            com.google.android.maps.MapController r0 = r0.getController()
            r1 = 18
            r0.setZoom(r1)
            com.google.android.maps.MapView r0 = r12.d
            r0.setSatellite(r8)
            r0 = 2131165197(0x7f07000d, float:1.7944604E38)
            android.view.View r0 = r12.findViewById(r0)
            com.immomo.momo.android.view.HeaderLayout r0 = (com.immomo.momo.android.view.HeaderLayout) r0
            r1 = 2131493134(0x7f0c010e, float:1.860974E38)
            r0.setTitleText(r1)
            com.immomo.momo.android.view.bi r1 = new com.immomo.momo.android.view.bi
            r1.<init>(r12)
            r2 = 2130838267(0x7f0202fb, float:1.7281511E38)
            com.immomo.momo.android.view.bi r1 = r1.a(r2)
            com.immomo.momo.android.map.ab r2 = new com.immomo.momo.android.map.ab
            r2.<init>(r12)
            r0.a(r1, r2)
            boolean r1 = r12.e
            if (r1 == 0) goto L_0x008b
            com.immomo.momo.android.view.bi r1 = new com.immomo.momo.android.view.bi
            r1.<init>(r12)
            java.lang.String r2 = "导航"
            com.immomo.momo.android.view.bi r1 = r1.a(r2)
            com.immomo.momo.android.map.ac r2 = new com.immomo.momo.android.map.ac
            r2.<init>(r12)
            r0.a(r1, r2)
        L_0x008b:
            com.google.android.maps.GeoPoint r0 = r12.b
            if (r0 == 0) goto L_0x00a5
            com.google.android.maps.MapView r0 = r12.d
            com.google.android.maps.MapController r0 = r0.getController()
            com.google.android.maps.GeoPoint r1 = r12.b
            r0.animateTo(r1)
            com.google.android.maps.MapView r0 = r12.d
            com.google.android.maps.MapController r0 = r0.getController()
            com.google.android.maps.GeoPoint r1 = r12.b
            r0.setCenter(r1)
        L_0x00a5:
            com.immomo.momo.android.map.af r0 = new com.immomo.momo.android.map.af
            com.google.android.maps.MapView r1 = r12.d
            r0.<init>(r12, r12, r1)
            r12.f2451a = r0
            com.google.android.maps.MapView r0 = r12.d
            java.util.List r0 = r0.getOverlays()
            com.google.android.maps.MyLocationOverlay r1 = r12.f2451a
            r0.add(r1)
            return
        L_0x00ba:
            java.lang.String r0 = "is_receive"
            boolean r0 = r4.getBoolean(r0, r8)
            r12.e = r0
            java.lang.String r0 = "latitude"
            java.lang.Object r0 = r4.get(r0)
            if (r0 != 0) goto L_0x00f6
            r0 = r2
        L_0x00cb:
            java.lang.String r5 = "longitude"
            java.lang.Object r5 = r4.get(r5)
            if (r5 != 0) goto L_0x0109
        L_0x00d3:
            android.location.Location r5 = new android.location.Location
            java.lang.String r6 = "network"
            r5.<init>(r6)
            r12.c = r5
            android.location.Location r5 = r12.c
            r5.setLatitude(r0)
            android.location.Location r5 = r12.c
            r5.setLongitude(r2)
            android.location.Location r5 = r12.c
            boolean r5 = com.immomo.momo.android.b.z.a(r5)
            if (r5 != 0) goto L_0x011c
            com.immomo.momo.util.ao.e(r11)
            r12.finish()
            goto L_0x0022
        L_0x00f6:
            java.lang.String r0 = "latitude"
            java.lang.Object r0 = r4.get(r0)
            java.lang.String r0 = r0.toString()
            java.lang.Double r0 = java.lang.Double.valueOf(r0)
            double r0 = r0.doubleValue()
            goto L_0x00cb
        L_0x0109:
            java.lang.String r2 = "longitude"
            java.lang.Object r2 = r4.get(r2)
            java.lang.String r2 = r2.toString()
            java.lang.Double r2 = java.lang.Double.valueOf(r2)
            double r2 = r2.doubleValue()
            goto L_0x00d3
        L_0x011c:
            java.lang.String r5 = "is_show_add"
            boolean r5 = r4.getBoolean(r5, r8)
            r12.i = r5
            boolean r5 = r12.i
            if (r5 == 0) goto L_0x0138
            java.lang.String r5 = "add_title"
            java.lang.String r5 = r4.getString(r5)
            r12.g = r5
            java.lang.String r5 = "add_info"
            java.lang.String r4 = r4.getString(r5)
            r12.h = r4
        L_0x0138:
            com.google.android.maps.GeoPoint r4 = new com.google.android.maps.GeoPoint
            double r0 = r0 * r9
            int r0 = (int) r0
            double r1 = r2 * r9
            int r1 = (int) r1
            r4.<init>(r0, r1)
            r12.b = r4
            com.google.android.maps.GeoPoint r0 = r12.b
            goto L_0x0022
        */
        throw new UnsupportedOperationException("Method not decompiled: com.immomo.momo.android.map.GoogleMapActivity.onCreate(android.os.Bundle):void");
    }
}
