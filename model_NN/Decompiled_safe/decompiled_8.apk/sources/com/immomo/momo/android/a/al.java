package com.immomo.momo.android.a;

import android.content.DialogInterface;
import android.widget.TextView;
import com.immomo.momo.service.bean.i;
import com.immomo.momo.service.bean.j;

final class al implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ae f719a;
    private final /* synthetic */ TextView b;
    private final /* synthetic */ i c;
    private final /* synthetic */ j d;

    al(ae aeVar, TextView textView, i iVar, j jVar) {
        this.f719a = aeVar;
        this.b = textView;
        this.c = iVar;
        this.d = jVar;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f719a.e.b(new ao(this.f719a, this.f719a.e, this.b.getText().toString().trim(), this.c, this.d));
    }
}
