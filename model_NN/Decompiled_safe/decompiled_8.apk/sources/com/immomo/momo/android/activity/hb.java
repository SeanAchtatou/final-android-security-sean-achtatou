package com.immomo.momo.android.activity;

import android.content.DialogInterface;
import com.immomo.momo.android.view.EmoteEditeText;

final class hb implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ OtherProfileActivity f1709a;
    private final /* synthetic */ EmoteEditeText b;

    hb(OtherProfileActivity otherProfileActivity, EmoteEditeText emoteEditeText) {
        this.f1709a = otherProfileActivity;
        this.b = emoteEditeText;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        OtherProfileActivity.a(this.f1709a, this.b);
        dialogInterface.dismiss();
    }
}
