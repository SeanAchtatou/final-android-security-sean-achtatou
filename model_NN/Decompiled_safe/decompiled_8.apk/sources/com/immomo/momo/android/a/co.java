package com.immomo.momo.android.a;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

final class co extends Handler {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ cn f759a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public co(cn cnVar, Looper looper) {
        super(looper);
        this.f759a = cnVar;
    }

    public final void handleMessage(Message message) {
        if (message.what == 1 && this.f759a.b != null && this.f759a.d != null) {
            this.f759a.d.setImageBitmap(this.f759a.b);
        }
    }
}
