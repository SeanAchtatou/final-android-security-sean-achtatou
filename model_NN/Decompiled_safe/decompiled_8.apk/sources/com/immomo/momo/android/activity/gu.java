package com.immomo.momo.android.activity;

import android.content.DialogInterface;
import com.immomo.momo.R;
import com.immomo.momo.android.view.a.al;
import com.immomo.momo.android.view.a.e;
import com.immomo.momo.android.view.a.n;

final class gu implements al {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ OtherProfileActivity f1702a;

    gu(OtherProfileActivity otherProfileActivity) {
        this.f1702a = otherProfileActivity;
    }

    public final void a(int i) {
        switch (i) {
            case 0:
                n nVar = new n(this.f1702a);
                nVar.setTitle("提醒");
                nVar.a("确定拉黑此用户吗");
                nVar.a();
                nVar.a(0, (int) R.string.dialog_btn_confim, new gv(this));
                nVar.a(1, (int) R.string.dialog_btn_cancel, (DialogInterface.OnClickListener) null);
                nVar.show();
                return;
            case 1:
                e.a(this.f1702a, this.f1702a.f, this.f1702a.t, new gy());
                return;
            default:
                return;
        }
    }
}
