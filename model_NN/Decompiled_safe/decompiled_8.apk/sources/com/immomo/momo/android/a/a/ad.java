package com.immomo.momo.android.a.a;

import android.content.Intent;
import android.view.View;
import com.immomo.momo.android.activity.OtherProfileActivity;
import com.immomo.momo.util.k;

final class ad implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ab f675a;

    ad(ab abVar) {
        this.f675a = abVar;
    }

    public final void onClick(View view) {
        if (this.f675a.h.chatType == 1) {
            new k("C", "C5102").e();
        }
        Intent intent = new Intent();
        intent.setClass(this.f675a.e().getApplicationContext(), OtherProfileActivity.class);
        intent.putExtra("tag", "local");
        intent.putExtra("momoid", this.f675a.l.h);
        this.f675a.e().startActivity(intent);
    }
}
