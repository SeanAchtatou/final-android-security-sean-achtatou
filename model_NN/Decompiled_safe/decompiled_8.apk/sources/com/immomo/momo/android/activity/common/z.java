package com.immomo.momo.android.activity.common;

import android.content.Context;
import android.support.v4.b.a;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.t;
import com.immomo.momo.util.ao;

final class z extends d {

    /* renamed from: a  reason: collision with root package name */
    private int f1136a;
    private String c;
    private v d;
    private /* synthetic */ CommonShareActivity e;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public z(CommonShareActivity commonShareActivity, Context context, int i, String str) {
        super(context);
        this.e = commonShareActivity;
        this.f1136a = i;
        this.c = str;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        switch (this.f1136a) {
            case 0:
                return t.a().a(this.e.l, this.c, this.e.m);
            case 1:
                return t.a().b(this.e.l, this.c, this.e.m);
            case 2:
                return t.a().c(this.e.l, this.c, this.e.m);
            default:
                this.b.c("type=" + this.f1136a);
                return null;
        }
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.d = new v(this.e);
        this.d.a("请求提交中");
        this.d.setCancelable(true);
        this.d.setOnCancelListener(new aa(this));
        this.e.a(this.d);
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        super.a(exc);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        String str = (String) obj;
        if (!a.a((CharSequence) str)) {
            ao.a((CharSequence) str);
        }
        this.e.setResult(-1);
        this.e.finish();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.e.p();
    }
}
