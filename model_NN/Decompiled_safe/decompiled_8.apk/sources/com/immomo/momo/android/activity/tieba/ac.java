package com.immomo.momo.android.activity.tieba;

import android.content.Context;
import android.widget.ListAdapter;
import com.immomo.momo.a;
import com.immomo.momo.android.a.gp;
import com.immomo.momo.android.c.d;
import com.immomo.momo.protocol.a.a.l;
import com.immomo.momo.protocol.a.v;
import com.immomo.momo.service.ao;
import com.immomo.momo.util.h;
import com.immomo.momo.util.u;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

final class ac extends d {

    /* renamed from: a  reason: collision with root package name */
    private l f2154a;
    private /* synthetic */ z c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ac(z zVar, Context context) {
        super(context);
        this.c = zVar;
        if (zVar.V != null && !zVar.V.isCancelled()) {
            zVar.V.cancel(true);
        }
        zVar.V = this;
    }

    private Boolean c() {
        this.f2154a = v.a().b(0);
        ao unused = this.c.T;
        ArrayList a2 = this.f2154a.a();
        u.a("tiebamycomments", a2);
        File u = a.u();
        String a3 = v.a(a2);
        File file = new File(u, "mytiecomms");
        try {
            if (!file.exists()) {
                file.createNewFile();
            }
            h.a(file, a3);
        } catch (IOException e) {
        }
        this.c.R.clear();
        this.c.R.addAll(this.f2154a.a());
        return Boolean.valueOf(this.f2154a.b());
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        return c();
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        Boolean bool = (Boolean) obj;
        this.c.Q = new Date();
        this.c.O.setLastFlushTime(this.c.Q);
        this.c.N.a("tieba_mycom_lasttime_success", this.c.Q);
        this.c.N.c("mtyiecomments_remain", bool);
        ((MainTiebaActivity) this.c.c()).y();
        this.c.S = new gp(this.c.c(), this.f2154a.a(), this.c.O);
        this.c.O.setAdapter((ListAdapter) this.c.S);
        if (!bool.booleanValue()) {
            this.c.O.k();
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.c.O.n();
    }
}
