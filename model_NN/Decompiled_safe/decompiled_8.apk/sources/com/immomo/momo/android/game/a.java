package com.immomo.momo.android.game;

import android.content.Context;
import android.content.Intent;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.m;

final class a extends d {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public /* synthetic */ AuthorizeActivity f2397a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public a(AuthorizeActivity authorizeActivity, Context context) {
        super(context);
        this.f2397a = authorizeActivity;
    }

    private String c() {
        long currentTimeMillis = System.currentTimeMillis();
        try {
            return m.a().a(this.f2397a.h, this.f2397a.i, this.f2397a.f.h, this.f2397a.f.W, this.f2397a.j);
        } finally {
            try {
                if (System.currentTimeMillis() - currentTimeMillis < 500) {
                    Thread.sleep(2000 - (System.currentTimeMillis() - currentTimeMillis));
                }
            } catch (Exception e) {
            }
        }
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        return c();
    }

    /* access modifiers changed from: protected */
    public final void a() {
        v vVar = new v(this.f2397a);
        vVar.a("授权验证中...");
        vVar.setOnCancelListener(new b(this));
        this.f2397a.a(vVar);
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        int i;
        this.b.a((Throwable) exc);
        if (exc instanceof com.immomo.momo.a.a) {
            com.immomo.momo.a.a aVar = (com.immomo.momo.a.a) exc;
            i = aVar.f670a > 0 ? aVar.f670a : 40103;
        } else {
            i = 40103;
        }
        Intent intent = new Intent();
        intent.putExtra("emsg", exc.getMessage());
        this.f2397a.setResult(i, intent);
        this.f2397a.finish();
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        Intent intent = new Intent();
        intent.putExtra("token", (String) obj);
        this.f2397a.setResult(-1, intent);
        this.f2397a.finish();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.f2397a.p();
    }
}
