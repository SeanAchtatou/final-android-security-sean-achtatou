package com.immomo.momo.android.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import com.immomo.momo.R;

public class AddTabsActivity extends ka implements View.OnClickListener {
    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        int i = 0;
        super.a(bundle);
        setContentView((int) R.layout.activity_addtabs);
        x();
        a(OldAddFriendActivity.class, AddGroupActivity.class);
        findViewById(R.id.addtabs_layout_addfriend).setOnClickListener(this);
        findViewById(R.id.addtabs_layout_addgroup).setOnClickListener(this);
        int intExtra = getIntent().getIntExtra("showindex", 0);
        if (intExtra >= 0) {
            i = intExtra > 1 ? 1 : intExtra;
        }
        c(i);
    }

    /* access modifiers changed from: protected */
    public final void a(Fragment fragment, int i) {
        switch (i) {
            case 0:
                findViewById(R.id.addtabs_layout_addfriend).setSelected(true);
                findViewById(R.id.addtabs_layout_addgroup).setSelected(false);
                break;
            case 1:
                findViewById(R.id.addtabs_layout_addgroup).setSelected(true);
                findViewById(R.id.addtabs_layout_addfriend).setSelected(false);
                break;
        }
        ((lh) fragment).a(this, m());
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.addtabs_layout_addfriend /*2131165373*/:
                c(0);
                return;
            case R.id.tabitem_ligthblue_driver_right /*2131165374*/:
            default:
                return;
            case R.id.addtabs_layout_addgroup /*2131165375*/:
                c(1);
                return;
        }
    }
}
