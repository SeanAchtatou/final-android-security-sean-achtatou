package com.immomo.momo.android.map;

import android.location.Location;
import com.immomo.momo.R;
import com.immomo.momo.android.b.p;
import com.immomo.momo.android.b.z;
import com.immomo.momo.util.ao;

final class au extends p {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SelectSiteAMapActivity f2473a;

    au(SelectSiteAMapActivity selectSiteAMapActivity) {
        this.f2473a = selectSiteAMapActivity;
    }

    public final void a(Location location, int i, int i2, int i3) {
        if (this.b) {
            if (z.a(location)) {
                this.f2473a.e.post(new av(this, location));
                return;
            }
            this.f2473a.c();
            if (i2 == 104) {
                ao.g(R.string.errormsg_network_unfind);
            } else {
                ao.g(R.string.errormsg_location_nearby_failed);
            }
        }
    }
}
