package com.immomo.momo.android.activity;

import android.content.Context;
import android.widget.ListAdapter;
import com.immomo.momo.android.a.fq;
import com.immomo.momo.android.c.d;
import com.immomo.momo.protocol.a.w;
import com.immomo.momo.service.aq;
import java.util.Date;
import java.util.List;

final class fd extends d {

    /* renamed from: a  reason: collision with root package name */
    private List f1432a = null;
    private /* synthetic */ HiddenlistActivity c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public fd(HiddenlistActivity hiddenlistActivity, Context context) {
        super(context);
        this.c = hiddenlistActivity;
    }

    /* access modifiers changed from: protected */
    public final Object a(Object... objArr) {
        this.f1432a = w.a().d();
        aq unused = this.c.k;
        aq.f();
        this.c.k.f(this.f1432a);
        return null;
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        this.c.n = new Date();
        this.c.g.c(this.c.n);
    }

    /* access modifiers changed from: protected */
    public final void a(Object obj) {
        this.c.n = new Date();
        this.c.g.c(this.c.n);
        if (this.f1432a.size() > 0) {
            this.c.l = new fq(this.c.getApplicationContext(), this.f1432a, this.c.i);
            this.c.i.setAdapter((ListAdapter) this.c.l);
        } else {
            this.c.l.b();
        }
        this.c.i.setLastFlushTime(this.c.n);
        this.c.g.d(this.c.n);
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.c.i.n();
    }
}
