package com.immomo.momo.android.activity.account;

import android.content.Intent;
import android.net.Uri;
import android.view.View;
import com.immomo.momo.util.ao;
import java.util.Date;

final class cl implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ck f991a;

    cl(ck ckVar) {
        this.f991a = ckVar;
    }

    public final void onClick(View view) {
        Intent intent = new Intent("android.intent.action.SENDTO", Uri.parse("smsto:" + this.f991a.h.p));
        intent.putExtra("sms_body", this.f991a.h.q);
        try {
            this.f991a.h.startActivity(intent);
            ck ckVar = this.f991a;
            new Date();
        } catch (Exception e) {
            ao.b("该设备不支持短信息功能,请使用其他手机发送短信");
        }
    }
}
