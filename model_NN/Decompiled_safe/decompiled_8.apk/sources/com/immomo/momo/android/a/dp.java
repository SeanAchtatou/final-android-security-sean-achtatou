package com.immomo.momo.android.a;

import android.content.Intent;
import android.view.View;
import com.immomo.momo.android.activity.group.GroupProfileActivity;
import com.immomo.momo.service.bean.a.b;

final class dp implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ dn f779a;
    private final /* synthetic */ b b;

    dp(dn dnVar, b bVar) {
        this.f779a = dnVar;
        this.b = bVar;
    }

    public final void onClick(View view) {
        Intent intent = new Intent();
        intent.setClass(this.f779a.d, GroupProfileActivity.class);
        intent.putExtra("tag", "local");
        intent.putExtra("gid", this.b.c());
        this.f779a.d.startActivity(intent);
    }
}
