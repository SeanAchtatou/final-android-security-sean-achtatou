package com.immomo.momo.android.broadcast;

import android.content.Context;
import com.immomo.momo.g;

public final class w extends b {

    /* renamed from: a  reason: collision with root package name */
    public static final String f2366a = (String.valueOf(g.h()) + ".action.user.reflush");
    public static final String b = (String.valueOf(g.h()) + ".action.user.refreshgroup");
    public static final String c = (String.valueOf(g.h()) + ".action.user.usersetting");

    public w(Context context) {
        super(context);
        a(f2366a, c, b);
    }
}
