package com.immomo.momo.android.a;

import com.immomo.momo.service.bean.a.a;
import com.immomo.momo.service.bean.n;

public final class hp {

    /* renamed from: a  reason: collision with root package name */
    public static int f862a = 1;
    public static int b = 2;
    public static int c = 3;
    public static int d = 4;
    public String e;
    public a f;
    public n g;
    public int h;

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        hp hpVar = (hp) obj;
        return this.e == null ? hpVar.e == null : this.e.equals(hpVar.e) && this.h == hpVar.h;
    }

    public final int hashCode() {
        return (this.e == null ? 0 : this.e.hashCode()) + 31;
    }
}
