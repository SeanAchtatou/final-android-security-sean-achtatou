package com.immomo.momo;

import android.os.Build;
import android.os.Environment;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.util.jni.Codec;
import com.immomo.momo.util.m;
import java.io.File;
import java.util.HashMap;

public abstract class a {
    private static String A = (String.valueOf(u) + "/camera");
    private static String B = (String.valueOf(y) + "/thumb");
    private static String C = (String.valueOf(z) + "/thumb");
    private static String D = (String.valueOf(z) + "/large");
    private static String E = (String.valueOf(y) + "/large");
    private static String F = (String.valueOf(u) + "/temp");
    private static String G = (String.valueOf(u) + "/weibo/thumb");
    private static String H = (String.valueOf(u) + "/renren/thumb");
    private static String I = (String.valueOf(u) + "/douban/thumb");
    private static String J = (String.valueOf(u) + "/weibo/middle");
    private static String K = (String.valueOf(u) + "/weibo/origina");
    private static File L = null;
    private static File M = null;
    private static File N = null;
    private static File O = null;
    private static File P = null;
    private static File Q = null;
    private static File R = null;
    private static File S = null;
    private static File T = null;
    private static File U = null;
    private static File V = null;
    private static File W = null;
    private static File X = null;
    private static File Y = null;
    private static File Z = null;

    /* renamed from: a  reason: collision with root package name */
    public static final String f669a = Codec.iiou(0);
    private static File aa = null;
    public static final int b = Codec.hhh();
    public static final String c = Codec.aaa();
    public static final String d = Codec.ggg();
    public static final String e = Codec.a();
    public static final String f = Codec.bbb();
    public static final String g = Codec.ddd();
    public static final String h = Codec.eee();
    public static final String i = Codec.fff();
    public static final String j = Codec.jjj();
    public static final String k = Codec.qqq();
    public static final String l = Codec.lsn();
    public static final String m = Codec.mm();
    public static final String n = Codec.em();
    public static final String o = Codec.sss();
    public static final String p = Codec.coo();
    public static boolean q = true;
    public static final String r = (String.valueOf(f) + "/m/inc/android/version.html?v=" + g.z());
    /* access modifiers changed from: private */
    public static m s = new m("Configs");
    private static String t = Environment.getExternalStorageDirectory().getAbsolutePath();
    private static String u;
    private static String v = (String.valueOf(u) + "/share_img");
    private static String w = (String.valueOf(u) + "/log2");
    private static String x = (String.valueOf(u) + "/users");
    private static String y = (String.valueOf(u) + "/avatar");
    private static String z = (String.valueOf(u) + "/event");

    static {
        Codec.mmm();
        Codec.nnn();
        Codec.ooo();
        Codec.iii();
        Codec.kkk();
        Codec.lll();
        Codec.ppp();
        Codec.rrr();
        Codec.uuu();
        Codec.opq();
        Codec.sssl();
        Codec.saa();
        String str = "immomo";
        if (!t.endsWith("/")) {
            str = "/immomo";
        }
        u = String.valueOf(t) + str;
    }

    public static File A() {
        if (aa == null) {
            aa = new File(K);
        }
        if (!aa.exists()) {
            aa.mkdirs();
        }
        return aa;
    }

    public static HashMap B() {
        int i2 = 1;
        HashMap hashMap = new HashMap();
        String E2 = g.E();
        String C2 = g.C();
        String F2 = g.F();
        String H2 = g.H();
        String T2 = g.T();
        String V2 = g.V();
        int J2 = g.J();
        int L2 = g.L();
        String str = PoiTypeDef.All;
        if (J2 > 0 && L2 > 0) {
            str = String.valueOf(J2) + "x" + L2;
        }
        String dm = Codec.dm();
        if (android.support.v4.b.a.a((CharSequence) E2)) {
            E2 = PoiTypeDef.All;
        }
        hashMap.put(dm, E2);
        hashMap.put(Codec.dr(), android.support.v4.b.a.a(C2) ? PoiTypeDef.All : C2);
        hashMap.put(Codec.dmo(), android.support.v4.b.a.a(F2) ? PoiTypeDef.All : F2);
        hashMap.put(Codec.dbu(), android.support.v4.b.a.a(H2) ? PoiTypeDef.All : H2);
        hashMap.put(Codec.dde(), "android");
        hashMap.put(Codec.du(), g.I());
        hashMap.put(Codec.ds(), str);
        hashMap.put(Codec.dma(), g.g());
        hashMap.put("version", new StringBuilder(String.valueOf(g.z())).toString());
        hashMap.put("phone_type", T2);
        hashMap.put("phone_netWork", V2);
        hashMap.put("gapps", new StringBuilder(String.valueOf(g.v() ? 1 : 0)).toString());
        hashMap.put("osversion_int", new StringBuilder(String.valueOf(Build.VERSION.SDK_INT)).toString());
        String etr968ww = Codec.etr968ww();
        if (!Codec.isEmulator()) {
            i2 = 0;
        }
        hashMap.put(etr968ww, new StringBuilder(String.valueOf(i2)).toString());
        try {
            hashMap.put(Codec.Dse(), g.X());
        } catch (Exception e2) {
        }
        return hashMap;
    }

    public static final boolean C() {
        return false;
    }

    private static String E() {
        bf q2 = g.q();
        if (q2 == null) {
            File file = new File(x);
            if (!file.exists()) {
                file.mkdirs();
            }
            return file.getAbsolutePath();
        }
        return q2 != null ? String.valueOf(x) + "/" + q2.h : x;
    }

    public static final File a() {
        if (L == null) {
            L = new File(u);
        }
        if (!L.exists()) {
            L.mkdirs();
        }
        return L;
    }

    public static final File a(String str) {
        File file = new File(g.c().getFilesDir(), str);
        if (!file.exists()) {
            file.mkdirs();
        }
        return file;
    }

    public static final File b() {
        if (M == null) {
            M = new File(String.valueOf(E()) + "/cache");
        }
        if (!M.exists()) {
            M.mkdirs();
        }
        return M;
    }

    public static final File c() {
        if (N == null) {
            N = new File(a(), "cache");
        }
        if (!N.exists()) {
            N.mkdirs();
        }
        return N;
    }

    public static final File d() {
        if (O == null) {
            O = new File(B);
        }
        if (!O.exists()) {
            O.mkdirs();
        }
        return O;
    }

    public static final File e() {
        if (P == null) {
            P = new File(C);
        }
        if (!P.exists()) {
            P.mkdirs();
        }
        return P;
    }

    public static final File f() {
        if (Q == null) {
            Q = new File(A);
        }
        if (!Q.exists()) {
            Q.mkdirs();
        }
        return Q;
    }

    public static final File g() {
        if (R == null) {
            R = new File(E);
        }
        if (!R.exists()) {
            R.mkdirs();
        }
        return R;
    }

    public static final File h() {
        if (S == null) {
            S = new File(D);
        }
        if (!S.exists()) {
            S.mkdirs();
        }
        return S;
    }

    public static final File i() {
        if (T == null) {
            T = new File(F);
        }
        if (!T.exists()) {
            T.mkdirs();
        }
        return T;
    }

    public static final File j() {
        File file = new File(String.valueOf(E()) + "/large");
        if (!file.exists()) {
            file.mkdirs();
        }
        return file;
    }

    public static final File k() {
        File file = new File(String.valueOf(E()) + "/thumb");
        if (!file.exists()) {
            file.mkdirs();
        }
        return file;
    }

    public static final File l() {
        File file = new File(String.valueOf(E()) + "/map");
        if (!file.exists()) {
            file.mkdirs();
        }
        return file;
    }

    public static final File m() {
        File file = new File(String.valueOf(E()) + "/audio2");
        if (!file.exists()) {
            file.mkdirs();
        }
        return file;
    }

    public static File n() {
        if (U == null) {
            U = new File(H);
        }
        if (!U.exists()) {
            U.mkdirs();
        }
        return U;
    }

    public static File o() {
        if (V == null) {
            V = new File(I);
        }
        if (!V.exists()) {
            V.mkdirs();
        }
        return V;
    }

    public static final File p() {
        File file;
        if (g.q() != null) {
            file = new File(String.valueOf(E()) + "/log2");
            if (!file.exists()) {
                file.mkdirs();
            }
        } else {
            file = new File(w);
            if (!file.exists()) {
                file.mkdirs();
            }
        }
        return file;
    }

    public static final File q() {
        File file = new File(u(), "emotes");
        if (!file.exists()) {
            file.mkdirs();
        }
        return file;
    }

    public static final File r() {
        File file = new File(u(), "event");
        if (!file.exists()) {
            file.mkdirs();
        }
        return file;
    }

    public static final File s() {
        File file = new File(u(), "tieba");
        if (!file.exists()) {
            file.mkdirs();
        }
        return file;
    }

    public static final File t() {
        File file = new File(u(), "discover");
        if (!file.exists()) {
            file.mkdirs();
        }
        return file;
    }

    public static final File u() {
        File file = new File(g.c().getFilesDir(), g.q().h);
        if (!file.exists()) {
            file.mkdirs();
        }
        return file;
    }

    public static final File v() {
        File file = new File(a(), "emotes");
        if (!file.exists()) {
            file.mkdirs();
        }
        return file;
    }

    public static File w() {
        if (W == null) {
            W = new File(G);
        }
        if (!W.exists()) {
            W.mkdirs();
        }
        return W;
    }

    public static File x() {
        if (X == null) {
            X = new File(J);
        }
        if (!X.exists()) {
            X.mkdirs();
        }
        return X;
    }

    public static File y() {
        if (Y == null) {
            Y = new File(v);
        }
        if (!Y.exists()) {
            Y.mkdirs();
        }
        return Y;
    }

    public static File z() {
        if (Z == null) {
            Z = new File(g.c().getFilesDir(), "icon");
        }
        if (!Z.exists()) {
            Z.mkdirs();
        }
        return Z;
    }
}
