package com.immomo.momo;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Process;
import com.immomo.momo.util.m;
import java.io.File;
import java.lang.Thread;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

public final class d implements Thread.UncaughtExceptionHandler {
    private static d b = null;
    private static Map d = null;

    /* renamed from: a  reason: collision with root package name */
    private Thread.UncaughtExceptionHandler f2837a;
    private Context c;

    private d() {
    }

    public static d a() {
        if (b == null) {
            b = new d();
        }
        return b;
    }

    public static String a(Throwable th) {
        File file = new File(a.p(), "e_" + System.currentTimeMillis() + "_" + g.z());
        m.a(new StringBuilder(), th, file);
        return file.getAbsolutePath();
    }

    private void b() {
        try {
            PackageInfo packageInfo = this.c.getPackageManager().getPackageInfo(this.c.getPackageName(), 1);
            if (packageInfo != null) {
                String str = packageInfo.versionName == null ? "null" : packageInfo.versionName;
                String sb = new StringBuilder(String.valueOf(packageInfo.versionCode)).toString();
                d.put("versionName", str);
                d.put("versionCode", sb);
            }
        } catch (PackageManager.NameNotFoundException e) {
        }
        for (Field field : Build.class.getDeclaredFields()) {
            try {
                field.setAccessible(true);
                d.put(field.getName(), field.get(null).toString());
            } catch (Exception e2) {
            }
        }
    }

    public final void a(Context context) {
        this.c = context;
        this.f2837a = Thread.getDefaultUncaughtExceptionHandler();
        Thread.setDefaultUncaughtExceptionHandler(this);
    }

    public final void uncaughtException(Thread thread, Throwable th) {
        if (this.f2837a != null) {
            if (d == null) {
                d = new HashMap();
                b();
            }
            a(th);
            this.f2837a.uncaughtException(thread, th);
            return;
        }
        Process.killProcess(Process.myPid());
        System.exit(1);
    }
}
