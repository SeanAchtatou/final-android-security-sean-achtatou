package com.immomo.momo;

import android.app.Application;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.b.a;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.android.activity.lh;
import com.immomo.momo.android.activity.message.ce;
import com.immomo.momo.android.b.z;
import com.immomo.momo.android.broadcast.k;
import com.immomo.momo.android.service.XService;
import com.immomo.momo.protocol.imjson.c.d;
import com.immomo.momo.protocol.imjson.c.e;
import com.immomo.momo.protocol.imjson.l;
import com.immomo.momo.protocol.imjson.p;
import com.immomo.momo.protocol.imjson.task.AudioMessageTask;
import com.immomo.momo.protocol.imjson.task.ImageMessageTask;
import com.immomo.momo.protocol.imjson.task.MapMessageTask;
import com.immomo.momo.protocol.imjson.task.ReadedTask;
import com.immomo.momo.protocol.imjson.task.TextMessageTask;
import com.immomo.momo.protocol.imjson.util.c;
import com.immomo.momo.service.a.ac;
import com.immomo.momo.service.a.ao;
import com.immomo.momo.service.a.ar;
import com.immomo.momo.service.a.ay;
import com.immomo.momo.service.a.g;
import com.immomo.momo.service.a.q;
import com.immomo.momo.service.af;
import com.immomo.momo.service.bean.Message;
import com.immomo.momo.service.bean.at;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.util.ak;
import com.immomo.momo.util.jni.Codec;
import com.immomo.momo.util.m;
import com.immomo.momo.util.u;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class MomoApplication extends Application {

    /* renamed from: a  reason: collision with root package name */
    public ak f668a = null;
    private bf b = null;
    private at c = null;
    private boolean d = false;
    private SQLiteDatabase e = null;
    private NotificationManager f = null;
    private int g = 55;
    private m h = new m("MomoApplication");
    private Handler i = new e(this);
    /* access modifiers changed from: private */
    public e j = null;
    private SQLiteDatabase k = null;
    private SQLiteDatabase l = null;
    private SQLiteDatabase m = null;
    private long n = 0;

    public static void a(Message message) {
        if (!l.h) {
            message.status = 3;
            return;
        }
        switch (message.contentType) {
            case 1:
                p.a(new ImageMessageTask(message));
                return;
            case 2:
                p.a(new MapMessageTask(message));
                return;
            case 3:
            default:
                TextMessageTask textMessageTask = new TextMessageTask(message);
                if (!c.c() || !"1602".equals(message.remoteId)) {
                    p.a(textMessageTask);
                    return;
                } else {
                    c.a().a(textMessageTask);
                    return;
                }
            case 4:
                a.C();
                p.a(new AudioMessageTask(message));
                return;
        }
    }

    public final bf a() {
        return this.b;
    }

    public final void a(Bitmap bitmap, int i2, String str, String str2, String str3, int i3, int i4, Intent intent) {
        if (this.c.f2996a) {
            if (!this.c.f) {
                str3 = "点击查看消息";
            }
            f fVar = new f(this);
            if ((this.c.b || this.c.e) && (this.n == 0 || Math.abs(System.currentTimeMillis() - this.n) >= 2000)) {
                this.f.cancel(i4);
                this.n = System.currentTimeMillis();
                if (this.c.h) {
                    int intValue = this.c.b().intValue();
                    int intValue2 = this.c.c().intValue();
                    int i5 = Calendar.getInstance().get(11);
                    if (intValue < intValue2 ? i5 >= intValue && i5 < intValue2 : (i5 >= intValue && i5 < 24) || (i5 >= 0 && i5 < intValue2)) {
                        this.h.a((Object) "收到消息，但是是静音时段!!!!!");
                    }
                }
                if (this.c.b && this.c.e) {
                    fVar.a(Uri.parse("android.resource://" + getPackageName() + "/2131034112"));
                    fVar.a(new long[]{50, 100});
                } else if (this.c.b) {
                    fVar.a(Uri.parse("android.resource://" + getPackageName() + "/2131034112"));
                } else if (this.c.e) {
                    fVar.a(new long[]{50, 100});
                }
                fVar.a();
            }
            if (str != null) {
                fVar.c(a.q(str));
            }
            if (str3 != null) {
                str3 = a.q(str3);
            }
            if (i3 > 0) {
                fVar.a(i3);
            }
            fVar.a(str2);
            fVar.b(str3);
            fVar.b(i2);
            fVar.a(bitmap);
            fVar.b();
            intent.addFlags(67108864);
            int i6 = this.g;
            this.g = i6 + 1;
            fVar.a(PendingIntent.getActivity(this, i6, intent, 134217728));
            this.f.notify(i4, fVar.d());
        }
    }

    public final void a(Bundle bundle, String str) {
        android.os.Message message = new android.os.Message();
        message.what = 954;
        message.obj = str;
        message.setData(bundle);
        this.i.sendMessage(message);
    }

    public final void a(bf bfVar, at atVar) {
        this.b = bfVar;
        this.c = atVar;
        this.b.aE = atVar;
        this.d = true;
    }

    public final void a(String str) {
        Configuration configuration = getBaseContext().getResources().getConfiguration();
        configuration.locale = new Locale(str);
        Locale.setDefault(configuration.locale);
        getBaseContext().getResources().updateConfiguration(configuration, getResources().getDisplayMetrics());
        this.h.a((Object) ("language change to --> " + configuration.locale));
        sendBroadcast(new Intent(k.f2354a));
    }

    public final void a(String str, String[] strArr, int i2) {
        if (strArr != null && strArr.length > 0) {
            try {
                ReadedTask readedTask = new ReadedTask(str, strArr, i2);
                HashMap hashMap = new HashMap();
                hashMap.put("msgid", readedTask.c());
                hashMap.put("time", new Date());
                hashMap.put("type", 1);
                hashMap.put("info", readedTask.d());
                ay.g().a((Map) hashMap);
                p.a(readedTask);
            } catch (Exception e2) {
                this.h.a((Throwable) e2);
            }
        }
    }

    public final at b() {
        return this.c;
    }

    public final Handler c() {
        return this.i;
    }

    public final boolean d() {
        if (this.b == null) {
            return false;
        }
        startService(new Intent(this, XService.class));
        return true;
    }

    public final synchronized SQLiteDatabase e() {
        SQLiteDatabase sQLiteDatabase;
        if (this.b == null) {
            sQLiteDatabase = null;
        } else {
            if (this.e == null || !this.e.isOpen()) {
                this.e = new g(this, this.b.h).getWritableDatabase();
                this.e.execSQL("PRAGMA cache_size=8000;");
            }
            sQLiteDatabase = this.e;
        }
        return sQLiteDatabase;
    }

    public final synchronized SQLiteDatabase f() {
        SQLiteDatabase sQLiteDatabase;
        if (this.b == null) {
            sQLiteDatabase = null;
        } else {
            if (this.k == null || !this.k.isOpen()) {
                this.k = new ac(this, this.b.h).getWritableDatabase();
            }
            sQLiteDatabase = this.k;
        }
        return sQLiteDatabase;
    }

    public final synchronized SQLiteDatabase g() {
        if (this.l == null || !this.l.isOpen()) {
            this.l = new ao(this).getWritableDatabase();
        }
        return this.l;
    }

    public final synchronized SQLiteDatabase h() {
        if (this.m == null || !this.m.isOpen()) {
            this.m = new q(this).getWritableDatabase();
            this.m.execSQL("PRAGMA cache_size=8000; ");
        }
        return this.m;
    }

    public final e i() {
        return this.j;
    }

    public final boolean j() {
        return this.d;
    }

    public final void k() {
        this.d = false;
        this.f668a.b("cookie");
        m();
        this.f.cancelAll();
    }

    public final void l() {
        this.b = null;
        this.c = null;
        this.e = null;
        this.k = null;
        this.m = null;
        k();
        ar.b();
        ce.b();
        af.r();
        u.a();
        lh.R();
    }

    public final void m() {
        stopService(new Intent(this, XService.class));
    }

    public final void n() {
        this.f.cancel(1950);
    }

    public final void o() {
        this.f.cancel(1952);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.ak.a(java.lang.String, java.lang.Integer):int
     arg types: [java.lang.String, int]
     candidates:
      com.immomo.momo.util.ak.a(android.content.Context, java.lang.String):com.immomo.momo.util.ak
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Long):long
      com.immomo.momo.util.ak.a(java.lang.String, int):void
      com.immomo.momo.util.ak.a(java.lang.String, long):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.String):void
      com.immomo.momo.util.ak.a(java.lang.String, boolean):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Boolean):boolean
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Integer):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.Integer]
     candidates:
      com.immomo.momo.util.ak.a(android.content.Context, java.lang.String):com.immomo.momo.util.ak
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Integer):int
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Long):long
      com.immomo.momo.util.ak.a(java.lang.String, int):void
      com.immomo.momo.util.ak.a(java.lang.String, long):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.String):void
      com.immomo.momo.util.ak.a(java.lang.String, boolean):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Boolean):boolean
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.ak.a(java.lang.String, java.lang.Long):long
     arg types: [java.lang.String, long]
     candidates:
      com.immomo.momo.util.ak.a(android.content.Context, java.lang.String):com.immomo.momo.util.ak
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Integer):int
      com.immomo.momo.util.ak.a(java.lang.String, int):void
      com.immomo.momo.util.ak.a(java.lang.String, long):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.String):void
      com.immomo.momo.util.ak.a(java.lang.String, boolean):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Boolean):boolean
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Long):long */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.ak.a(java.lang.String, long):void
     arg types: [java.lang.String, int]
     candidates:
      com.immomo.momo.util.ak.a(android.content.Context, java.lang.String):com.immomo.momo.util.ak
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Integer):int
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Long):long
      com.immomo.momo.util.ak.a(java.lang.String, int):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.String):void
      com.immomo.momo.util.ak.a(java.lang.String, boolean):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Boolean):boolean
      com.immomo.momo.util.ak.a(java.lang.String, long):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.immomo.momo.util.ak.a(android.content.Context, java.lang.String):com.immomo.momo.util.ak
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Integer):int
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Long):long
      com.immomo.momo.util.ak.a(java.lang.String, int):void
      com.immomo.momo.util.ak.a(java.lang.String, long):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.String):void
      com.immomo.momo.util.ak.a(java.lang.String, boolean):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Boolean):boolean
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void */
    public void onCreate() {
        super.onCreate();
        d.a().a(this);
        g.a(this);
        com.immomo.momo.util.ao.a(this);
        this.j = new e();
        this.j.a(new d());
        this.f = (NotificationManager) getSystemService("notification");
        this.f668a = ak.a(this, "app_preference");
        String b2 = this.f668a.b("momoid", PoiTypeDef.All);
        String b3 = this.f668a.b("cookie", PoiTypeDef.All);
        ak akVar = this.f668a;
        if (akVar.a("ver", (Integer) 0) != g.z()) {
            akVar.a("ver", (Object) Integer.valueOf(g.z()));
            SharedPreferences.Editor edit = akVar.a().edit();
            edit.remove("last_checkversion_time");
            edit.commit();
            if (!a.a((CharSequence) b2)) {
                ak a2 = ak.a(this, b2);
                long a3 = a2.a("pre_showlog", (Long) 0L);
                this.h.a((Object) ("initVersionInfo, versioncode=" + g.z() + ", preTime = " + a3));
                if (a3 > 0) {
                    a2.a("pre_showlog", 0L);
                }
            }
        }
        if (!a.a((CharSequence) b3) && !a.a((CharSequence) b2)) {
            this.b = new bf(b2);
            String b4 = Codec.b(b3);
            if (b4.startsWith("SESSIONID=")) {
                b4 = b4.replace("SESSIONID=", PoiTypeDef.All);
                this.f668a.a("cookie", (Object) Codec.c(b4));
            }
            if (this.f668a.a("password")) {
                this.f668a.b("password");
            }
            this.b.W = b4;
            this.c = at.a(getApplicationContext(), b2);
            this.b.aE = this.c;
            this.d = true;
        }
        z.a(getApplicationContext());
    }

    public void onLowMemory() {
        super.onLowMemory();
        this.h.a((Object) "onLowMemory!!!!!!!!!!!!!!!!!");
    }

    public void onTerminate() {
        super.onTerminate();
    }

    public final void p() {
        this.f.cancel(1953);
    }

    public final void q() {
        this.f.cancel(1951);
    }

    public final void r() {
        this.f.cancel(1959);
    }

    public final void s() {
        this.f.cancel(1954);
    }

    public void startActivity(Intent intent) {
        super.startActivity(intent);
    }

    public final void t() {
        this.f.cancelAll();
    }
}
