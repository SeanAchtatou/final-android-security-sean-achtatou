package com.immomo.momo.protocol.imjson;

import com.immomo.momo.protocol.imjson.task.SendTask;
import com.immomo.momo.protocol.imjson.task.Task;
import com.immomo.momo.util.m;
import java.util.concurrent.BlockingQueue;

public class s extends Thread {

    /* renamed from: a  reason: collision with root package name */
    private BlockingQueue f2931a = null;
    private boolean b = true;
    private SendTask c = null;
    private Object d;
    private boolean e;
    private /* synthetic */ p f;

    public s(p pVar, BlockingQueue blockingQueue) {
        this.f = pVar;
        new m("Channel");
        this.d = new Object();
        this.e = false;
        if (blockingQueue == null) {
            new RuntimeException(new NullPointerException("queue is null， channel can't to start."));
        }
        this.f2931a = blockingQueue;
        this.e = pVar.k;
    }

    public final void a() {
        this.b = false;
    }

    /* access modifiers changed from: protected */
    public void a(SendTask sendTask) {
        if (sendTask.a(this.f.f2928a)) {
            sendTask.g_();
        } else {
            sendTask.h_();
        }
    }

    public final Task b() {
        return this.c;
    }

    /* access modifiers changed from: protected */
    public void c() {
        if (this.e) {
            synchronized (this.d) {
                try {
                    this.d.wait();
                } catch (InterruptedException e2) {
                }
            }
        }
    }

    public final boolean d() {
        return this.e;
    }

    public final void e() {
        this.e = true;
    }

    public final void f() {
        this.e = false;
        synchronized (this.d) {
            this.d.notifyAll();
        }
    }

    public void run() {
        while (p.b && this.b && this.f.f2928a != null) {
            try {
                this.c = (SendTask) this.f2931a.take();
                c();
                a(this.c);
            } catch (InterruptedException e2) {
            }
            this.c = null;
        }
    }
}
