package com.immomo.momo.protocol.imjson.a;

import android.os.Bundle;
import com.immomo.a.a.d.b;
import com.immomo.a.a.f;
import com.immomo.momo.g;
import com.immomo.momo.service.y;

public final class i implements f {
    public final void a(Object obj, f fVar) {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.service.y.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.immomo.momo.service.y.a(com.immomo.momo.service.bean.a.a, java.lang.String):void
      com.immomo.momo.service.y.a(com.immomo.momo.service.bean.a.a, boolean):void
      com.immomo.momo.service.y.a(java.lang.String, int):void
      com.immomo.momo.service.y.a(java.lang.String, java.lang.String):void
      com.immomo.momo.service.y.a(java.lang.String, java.util.List):void
      com.immomo.momo.service.y.a(java.lang.String, java.lang.String[]):void
      com.immomo.momo.service.y.a(java.util.List, java.lang.String):void
      com.immomo.momo.service.y.a(java.lang.String, boolean):void */
    public final boolean a(b bVar) {
        if (!"gzone".equals(bVar.h())) {
            return false;
        }
        String string = bVar.getString("gid");
        new y().a(string, true);
        Bundle bundle = new Bundle();
        bundle.putString("groupid", string);
        g.d().a(bundle, "actions.groupfeed");
        return true;
    }
}
