package com.immomo.momo.protocol.imjson.a;

import android.content.Intent;
import com.immomo.a.a.d.b;
import com.immomo.a.a.f;
import com.immomo.momo.android.broadcast.u;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.service.y;

public final class e implements f {
    public final void a(Object obj, f fVar) {
    }

    public final boolean a(b bVar) {
        if ("ban".equals(bVar.opt("event"))) {
            String optString = bVar.optString("gid");
            new Intent();
            new y().c(optString, 4);
            Intent intent = new Intent(u.c);
            intent.putExtra("gid", optString);
            g.c().sendBroadcast(intent);
            return true;
        } else if ("createpass".equals(bVar.opt("event"))) {
            new y().c(bVar.optString("gid"), 2);
            new Intent();
            Intent intent2 = new Intent(u.d);
            intent2.putExtra("gid", "gid");
            g.c().sendBroadcast(intent2);
            g.d().b().c(String.valueOf(bVar.optString("gid")) + "_alertshared", true);
            return true;
        } else if (!"rolechange".equals(bVar.opt("event"))) {
            return false;
        } else {
            bf q = g.q();
            y yVar = new y();
            int i = bVar.getInt("role");
            String string = bVar.getString("gid");
            if (i <= 0) {
                yVar.h(string);
                if (!yVar.c(q.h, string)) {
                    return true;
                }
                yVar.b(g.q().h, string);
                Intent intent3 = new Intent(u.b);
                intent3.putExtra("gid", string);
                g.c().sendBroadcast(intent3);
                return true;
            } else if (!yVar.c(q.h, string)) {
                yVar.a(q.h, string, i);
                Intent intent4 = new Intent(u.f2364a);
                intent4.putExtra("gid", string);
                g.c().sendBroadcast(intent4);
                return true;
            } else {
                yVar.a(i, string, q.h);
                return true;
            }
        }
    }
}
