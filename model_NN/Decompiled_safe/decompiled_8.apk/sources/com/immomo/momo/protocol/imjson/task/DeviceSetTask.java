package com.immomo.momo.protocol.imjson.task;

import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.a.a.a;
import com.immomo.a.a.d.b;
import com.immomo.a.a.d.h;
import com.immomo.momo.g;

public class DeviceSetTask extends SendTask {

    /* renamed from: a  reason: collision with root package name */
    private String f2934a = PoiTypeDef.All;

    public DeviceSetTask(String str) {
        super(g.AsyncExpress);
        this.f2934a = str;
    }

    public final boolean a(a aVar) {
        try {
            h hVar = new h(aVar);
            hVar.f("device");
            hVar.put("cflag", this.f2934a);
            b bVar = new b();
            hVar.put("device", bVar);
            bVar.put("uid", g.I());
            bVar.put("rom", g.B());
            bVar.put("cola", String.valueOf(g.N()) + "_" + g.M());
            hVar.j();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public final void g_() {
    }

    public final void h_() {
    }
}
