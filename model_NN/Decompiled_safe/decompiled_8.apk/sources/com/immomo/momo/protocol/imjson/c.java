package com.immomo.momo.protocol.imjson;

import android.os.Bundle;
import com.immomo.a.a.a;
import com.immomo.a.a.d.b;
import com.immomo.a.a.f;
import com.immomo.momo.g;
import com.immomo.momo.service.af;
import com.sina.sdk.api.message.InviteApi;
import org.json.JSONObject;

public final class c implements f {

    /* renamed from: a  reason: collision with root package name */
    private a f2911a = null;

    public c(a aVar) {
        this.f2911a = aVar;
    }

    public final void a(Object obj, f fVar) {
    }

    public final boolean a(b bVar) {
        com.immomo.a.a.d.a aVar = new com.immomo.a.a.d.a();
        aVar.a((Object) bVar.a());
        aVar.b(bVar.d());
        aVar.put("ns", "event-notice");
        this.f2911a.a(aVar);
        Bundle bundle = new Bundle();
        if (bVar.optInt("type") == 1) {
            bundle.putString("stype", "newfeed");
        } else if (bVar.optInt("type") != 2) {
            return false;
        } else {
            bundle.putString("stype", "newcomment");
        }
        if (bVar.has("newfeed")) {
            af.c().e(bVar.optInt("newfeed") + 0);
        }
        if (bVar.has("newcomment")) {
            af.c().f(bVar.optInt("newcomment") + 0);
        }
        af.c().b(bundle);
        if (bVar.has("feed")) {
            StringBuilder sb = new StringBuilder();
            JSONObject jSONObject = bVar.getJSONObject("feed");
            double optDouble = jSONObject.optDouble("distance", -1.0d);
            if (optDouble >= 0.0d) {
                sb.append("[" + android.support.v4.b.a.a(optDouble / 1000.0d) + "km]");
            }
            sb.append(jSONObject.optString(InviteApi.KEY_TEXT));
            bundle.putString("content", sb.toString().replaceAll("&lsb;", "[").replaceAll("&rsb;", "]").replaceAll("&vb;", "|"));
        }
        g.d().a(bundle, "actions.eventdynamics");
        return true;
    }
}
