package com.immomo.momo.protocol.a;

import com.amap.mapapi.location.LocationManagerProxy;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.service.aq;
import com.immomo.momo.service.bean.a.a;
import com.immomo.momo.service.bean.a.e;
import com.immomo.momo.service.bean.a.g;
import com.immomo.momo.service.bean.a.h;
import com.immomo.momo.service.bean.a.j;
import com.immomo.momo.service.bean.as;
import com.immomo.momo.service.bean.ay;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.service.y;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class n extends p {
    private static n f = null;
    private static String g = (String.valueOf(b) + "/group");
    private static String h = (String.valueOf(b) + "/site");
    private static String i = "feedid";
    private static String j = "keywords";

    private static int a(JSONObject jSONObject, a aVar) {
        JSONArray jSONArray;
        aVar.b = jSONObject.optString("gid", aVar.b);
        aVar.c = jSONObject.optString("name", aVar.c);
        aVar.E = jSONObject.optString("cover");
        aVar.D = jSONObject.optInt("use_cover") == 1;
        aVar.F = a(jSONObject.getJSONArray("photos"));
        aVar.n = jSONObject.optInt("role");
        aVar.z = jSONObject.optLong("version", aVar.z);
        aVar.w = jSONObject.optLong("activeday", aVar.w);
        aVar.v = jSONObject.optLong("maxday", aVar.v);
        aVar.h = jSONObject.optString("sign", aVar.h);
        aVar.J = jSONObject.optString("sid", aVar.J);
        aVar.a((float) jSONObject.optLong("distance", -1));
        aVar.K = jSONObject.optString("sname", aVar.K);
        aVar.L = jSONObject.optInt("type", aVar.L);
        aVar.G = jSONObject.optInt(LocationManagerProxy.KEY_STATUS_CHANGED, aVar.G);
        aVar.A = jSONObject.optInt("level", aVar.A);
        aVar.q = jSONObject.optInt("editing", aVar.q);
        aVar.C = jSONObject.optInt("hasparty") == 1;
        aVar.x = jSONObject.optInt("upgrade") == 1;
        aVar.B = jSONObject.optInt("super", aVar.B);
        aVar.y = jSONObject.optInt("new", aVar.y);
        JSONObject optJSONObject = jSONObject.optJSONObject("announce");
        if (optJSONObject != null) {
            aVar.f = optJSONObject.optString("content", aVar.f);
            aVar.e = android.support.v4.b.a.a(optJSONObject.optLong("create_time"));
        }
        try {
            JSONArray jSONArray2 = jSONObject.getJSONArray("parties");
            if (jSONArray2 != null && jSONArray2.length() > 0) {
                aVar.M = new ArrayList();
                for (int i2 = 0; i2 < jSONArray2.length(); i2++) {
                    g gVar = new g();
                    a(gVar, jSONArray2.getJSONObject(i2));
                    gVar.c = aVar.b;
                    aVar.M.add(gVar);
                }
            }
        } catch (Exception e) {
        }
        try {
            JSONArray jSONArray3 = jSONObject.getJSONArray("feeds");
            if (jSONArray3 != null && jSONArray3.length() > 0) {
                aVar.N = new ArrayList();
                for (int i3 = 0; i3 < jSONArray3.length(); i3++) {
                    e eVar = new e();
                    o.a(jSONArray3.getJSONObject(i3), eVar);
                    eVar.f2974a = aVar.b;
                    aVar.N.add(eVar);
                }
            }
        } catch (Exception e2) {
        }
        try {
            JSONObject jSONObject2 = jSONObject.getJSONObject("geoloc");
            aVar.l = jSONObject2.optDouble("lat", -1.0d);
            aVar.m = jSONObject2.optDouble("lng", -1.0d);
        } catch (Exception e3) {
        }
        aVar.i = jSONObject.optString("apply_desc", aVar.i);
        aVar.j = jSONObject.optInt("member_max", aVar.j);
        aVar.k = jSONObject.optInt("member_count", aVar.k);
        aVar.r = jSONObject.optInt("maxlevel", aVar.k) == 1;
        try {
            aVar.d = android.support.v4.b.a.a(jSONObject.getLong("create_time"));
        } catch (Exception e4) {
        }
        aVar.g = jSONObject.optString("owner", aVar.g);
        if (jSONObject.has("members") && (jSONArray = jSONObject.getJSONArray("members")) != null) {
            aVar.I = new String[jSONArray.length()];
            for (int i4 = 0; i4 < jSONArray.length(); i4++) {
                JSONObject jSONObject3 = jSONArray.getJSONObject(i4);
                if (jSONObject3 != null) {
                    aVar.I[i4] = b(jSONObject3);
                }
            }
        }
        try {
            if (jSONObject.has("setting")) {
                aVar.f2970a = jSONObject.getJSONObject("setting").optInt("hiddenmode", -1);
            }
        } catch (Exception e5) {
        }
        return 1;
    }

    public static n a() {
        if (f == null) {
            f = new n();
        }
        return f;
    }

    private static j a(JSONObject jSONObject) {
        j jVar = new j();
        jVar.g = jSONObject.optInt("role", jVar.g);
        try {
            jVar.d = android.support.v4.b.a.a(jSONObject.getLong("activetime"));
        } catch (JSONException e) {
        }
        try {
            jVar.c = android.support.v4.b.a.a(jSONObject.getLong("jointime"));
        } catch (JSONException e2) {
        }
        try {
            jVar.e = android.support.v4.b.a.a(jSONObject.getLong("msgtime"));
        } catch (JSONException e3) {
        }
        jVar.f2979a = jSONObject.getString("momoid");
        jVar.h = new bf();
        w.a(jVar.h, jSONObject);
        jVar.h.aJ = jSONObject.optInt("group_role");
        return jVar;
    }

    public static void a(a aVar, JSONObject jSONObject) {
        boolean z = true;
        aVar.b = jSONObject.optString("gid", aVar.b);
        aVar.c = jSONObject.optString("name", aVar.c);
        aVar.E = jSONObject.optString("cover");
        if (jSONObject.optInt("use_cover") != 1) {
            z = false;
        }
        aVar.D = z;
        aVar.F = a(jSONObject.optJSONArray("photos"));
        aVar.n = jSONObject.optInt("role");
        aVar.K = jSONObject.optString("sname");
        aVar.J = jSONObject.optString("sid");
        aVar.B = jSONObject.optInt("super", aVar.B);
    }

    private static void a(g gVar, JSONObject jSONObject) {
        boolean z = true;
        gVar.b = jSONObject.optString("ptid", gVar.b);
        gVar.d = jSONObject.optString("desc", gVar.d);
        gVar.e = jSONObject.optString("address", gVar.e);
        if (jSONObject.has("geoloc")) {
            JSONObject optJSONObject = jSONObject.optJSONObject("geoloc");
            gVar.j = optJSONObject.optDouble("lat", gVar.j);
            gVar.k = optJSONObject.optDouble("lng", gVar.k);
        }
        gVar.l = jSONObject.optDouble("distance", gVar.l);
        gVar.f = a(jSONObject.optLong("start_time"));
        gVar.g = a(jSONObject.optLong("ctime"));
        gVar.i = jSONObject.optInt(LocationManagerProxy.KEY_STATUS_CHANGED, gVar.i);
        gVar.c = jSONObject.optString("gid", gVar.c);
        gVar.o = jSONObject.optInt("role", gVar.o);
        gVar.h = jSONObject.optInt("need_phone", gVar.h);
        gVar.n = jSONObject.optInt("member_count", gVar.n);
        gVar.f2976a = jSONObject.optString(i, gVar.f2976a);
        if (jSONObject.optInt("top") != 1) {
            z = false;
        }
        gVar.m = z;
        try {
            JSONArray jSONArray = jSONObject.getJSONArray("members");
            if (jSONArray != null && jSONArray.length() > 0) {
                for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                    JSONObject jSONObject2 = jSONArray.getJSONObject(i2);
                    as asVar = new as();
                    asVar.f2995a = jSONObject2.optString("momoid", asVar.f2995a);
                    asVar.b = jSONObject2.optString("name", asVar.b);
                    asVar.d = jSONObject2.optString("avatar", asVar.d);
                    asVar.c = jSONObject2.optString("phone", asVar.c);
                    gVar.p.add(asVar);
                }
            }
        } catch (Exception e) {
        }
    }

    private static String b(JSONObject jSONObject) {
        String str = null;
        try {
            str = jSONObject.getString("momoid");
        } catch (Exception e) {
        }
        try {
            String string = jSONObject.getString("avatar");
            String optString = jSONObject.optString("name");
            if (!android.support.v4.b.a.a((CharSequence) string) && !android.support.v4.b.a.a((CharSequence) str)) {
                new aq().a(str, string, optString);
            }
        } catch (Exception e2) {
        }
        return str;
    }

    private static h c(JSONObject jSONObject) {
        h hVar = new h();
        try {
            hVar.f2977a = new ArrayList();
            JSONArray jSONArray = jSONObject.getJSONArray("agree_list");
            if (jSONArray != null && jSONArray.length() > 0) {
                String[] strArr = new String[jSONArray.length()];
                for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                    strArr[i2] = jSONArray.getString(i2);
                }
                hVar.f2977a = Arrays.asList(strArr);
            }
        } catch (Exception e) {
        }
        try {
            hVar.b = new ArrayList();
            JSONArray jSONArray2 = jSONObject.getJSONArray("disagree_list");
            if (jSONArray2 != null && jSONArray2.length() > 0) {
                String[] strArr2 = new String[jSONArray2.length()];
                for (int i3 = 0; i3 < jSONArray2.length(); i3++) {
                    strArr2[i3] = jSONArray2.getString(i3);
                }
                hVar.b = Arrays.asList(strArr2);
            }
        } catch (Exception e2) {
        }
        hVar.c = jSONObject.optBoolean("allow", hVar.c);
        hVar.d = jSONObject.optString("tip", hVar.d);
        hVar.e = jSONObject.optString("vip_tip", hVar.e);
        hVar.i = jSONObject.optString("normal_info", hVar.i);
        hVar.j = jSONObject.optString("vip_info", hVar.j);
        hVar.k = jSONObject.optString("year_vip_info", hVar.k);
        hVar.m = jSONObject.optInt("create_count", 0);
        hVar.l = jSONObject.optInt("create_max", 0);
        jSONObject.optInt("vip_create_max", 0);
        jSONObject.optBoolean("is_vip", false);
        hVar.f = jSONObject.optString("disagree_tip", hVar.f);
        hVar.h = jSONObject.optInt("join_count", hVar.h);
        hVar.g = jSONObject.optInt("join_max", hVar.g);
        hVar.n = new HashMap();
        JSONObject optJSONObject = jSONObject.optJSONObject("sitephotos");
        if (optJSONObject != null) {
            Iterator<String> keys = optJSONObject.keys();
            while (keys.hasNext()) {
                try {
                    String next = keys.next();
                    hVar.n.put(Integer.valueOf(Integer.parseInt(next)), optJSONObject.optString(new StringBuilder(String.valueOf(Integer.parseInt(next))).toString(), PoiTypeDef.All));
                } catch (Throwable th) {
                }
            }
        }
        return hVar;
    }

    public final int a(a aVar, String str) {
        a(new JSONObject(a(String.valueOf(g) + "/tempprofile/" + str, (Map) null)), aVar);
        return 1;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.service.y.a(com.immomo.momo.service.bean.a.a, boolean):void
     arg types: [com.immomo.momo.service.bean.a.a, int]
     candidates:
      com.immomo.momo.service.y.a(com.immomo.momo.service.bean.a.a, java.lang.String):void
      com.immomo.momo.service.y.a(java.lang.String, int):void
      com.immomo.momo.service.y.a(java.lang.String, java.lang.String):void
      com.immomo.momo.service.y.a(java.lang.String, java.util.List):void
      com.immomo.momo.service.y.a(java.lang.String, boolean):void
      com.immomo.momo.service.y.a(java.lang.String, java.lang.String[]):void
      com.immomo.momo.service.y.a(java.util.List, java.lang.String):void
      com.immomo.momo.service.y.a(com.immomo.momo.service.bean.a.a, boolean):void */
    public final int a(String str, a aVar) {
        if (aVar == null || android.support.v4.b.a.a((CharSequence) str)) {
            throw new Exception("group=null or gid=null");
        }
        JSONObject jSONObject = new JSONObject(a(String.valueOf(g) + "/profile/" + str, (Map) null));
        a(jSONObject, aVar);
        y yVar = new y();
        yVar.a(aVar, false);
        yVar.a(aVar.b, aVar.I);
        ay ayVar = new ay();
        ayVar.f3001a = aVar.J;
        ayVar.f = aVar.K;
        ayVar.d = aVar.L;
        if (jSONObject.optInt("role") != 0) {
            yVar.a(com.immomo.momo.g.q().h, aVar.b, jSONObject.optInt("role", 3));
            return 1;
        }
        yVar.b(com.immomo.momo.g.q().h, aVar.b);
        return 1;
    }

    public final com.immomo.momo.protocol.a.a.j a(String str, boolean z, boolean z2, boolean z3, boolean z4, int i2) {
        HashMap hashMap = new HashMap();
        hashMap.put("ptid", str);
        hashMap.put("sync_sina", z ? "1" : "0");
        hashMap.put("sync_qqwb", z2 ? "1" : "0");
        hashMap.put("sync_renren", z3 ? "1" : "0");
        hashMap.put("sync_weixin", z4 ? "1" : "0");
        hashMap.put("type", new StringBuilder().append(i2).toString());
        JSONObject jSONObject = new JSONObject(a(String.valueOf(g) + "/party/share", hashMap));
        com.immomo.momo.protocol.a.a.j jVar = new com.immomo.momo.protocol.a.a.j();
        jVar.f2889a = jSONObject.optString("msg");
        jVar.b = jSONObject.optString("weixin_url");
        return jVar;
    }

    public final h a(String str) {
        return c(new JSONObject(a(String.valueOf(g) + "/checkapply?gid=" + str, (Map) null)));
    }

    public final String a(a aVar) {
        HashMap hashMap = new HashMap();
        hashMap.put("gid", aVar.b);
        hashMap.put("content", aVar.f);
        JSONObject jSONObject = new JSONObject(a(String.valueOf(g) + "/announce/publish", hashMap));
        JSONObject optJSONObject = jSONObject.optJSONObject("announce");
        if (optJSONObject != null) {
            aVar.f = optJSONObject.optString("content", PoiTypeDef.All);
            aVar.e = android.support.v4.b.a.a(optJSONObject.optLong("create_time"));
        }
        return jSONObject.optString("msg", PoiTypeDef.All);
    }

    public final String a(g gVar) {
        HashMap hashMap = new HashMap();
        hashMap.put("desc", gVar.d);
        hashMap.put("start_time", new StringBuilder(String.valueOf(android.support.v4.b.a.b(gVar.f))).toString());
        hashMap.put("lat", new StringBuilder(String.valueOf(gVar.j)).toString());
        hashMap.put("lng", new StringBuilder(String.valueOf(gVar.k)).toString());
        hashMap.put("need_phone", new StringBuilder(String.valueOf(gVar.h)).toString());
        hashMap.put("loctype", new StringBuilder(String.valueOf(1)).toString());
        hashMap.put("gid", gVar.c);
        JSONObject jSONObject = new JSONObject(a(String.valueOf(g) + "/party/create", hashMap));
        gVar.b = jSONObject.optString("ptid", PoiTypeDef.All);
        gVar.l = jSONObject.optDouble("distance", 0.0d);
        return jSONObject.optString("msg", PoiTypeDef.All);
    }

    public final String a(String str, String str2, ay ayVar, a aVar, double d, double d2, int i2, File file) {
        String str3 = String.valueOf(g) + "/create";
        HashMap hashMap = new HashMap();
        hashMap.put("lat", new StringBuilder(String.valueOf(d)).toString());
        hashMap.put("lng", new StringBuilder(String.valueOf(d2)).toString());
        hashMap.put("loctype", new StringBuilder(String.valueOf(i2)).toString());
        hashMap.put("name", str);
        hashMap.put("sign", str2);
        if (android.support.v4.b.a.a((CharSequence) ayVar.f3001a)) {
            hashMap.put("sname", ayVar.f);
            hashMap.put("type", new StringBuilder(String.valueOf(ayVar.d)).toString());
        } else {
            hashMap.put("sid", new StringBuilder(String.valueOf(ayVar.f3001a)).toString());
        }
        this.e.a((Object) ("content map" + hashMap));
        l[] lVarArr = null;
        if (file != null) {
            lVarArr = new l[]{new l(file.getName(), file, "pic", (byte) 0)};
        }
        JSONObject jSONObject = new JSONObject(a(str3, hashMap, lVarArr));
        a(jSONObject, aVar);
        return jSONObject.optString("msg");
    }

    public final String a(String str, String str2, String str3) {
        HashMap hashMap = new HashMap();
        hashMap.put("gid", str);
        hashMap.put("remoteid", str2);
        hashMap.put("password", str3);
        return new JSONObject(a(String.valueOf(g) + "/transfer", hashMap)).optString("em");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.service.y.a(com.immomo.momo.service.bean.a.a, boolean):void
     arg types: [com.immomo.momo.service.bean.a.a, int]
     candidates:
      com.immomo.momo.service.y.a(com.immomo.momo.service.bean.a.a, java.lang.String):void
      com.immomo.momo.service.y.a(java.lang.String, int):void
      com.immomo.momo.service.y.a(java.lang.String, java.lang.String):void
      com.immomo.momo.service.y.a(java.lang.String, java.util.List):void
      com.immomo.momo.service.y.a(java.lang.String, boolean):void
      com.immomo.momo.service.y.a(java.lang.String, java.lang.String[]):void
      com.immomo.momo.service.y.a(java.util.List, java.lang.String):void
      com.immomo.momo.service.y.a(com.immomo.momo.service.bean.a.a, boolean):void */
    public final String a(String str, Map map, Map map2, boolean z, a aVar) {
        String str2 = String.valueOf(g) + "/edit?gid=" + str;
        this.e.a((Object) ("content: " + map));
        this.e.a((Object) ("photosMap: " + map2));
        l[] lVarArr = new l[map2.size()];
        int i2 = 0;
        for (Map.Entry entry : map2.entrySet()) {
            lVarArr[i2] = new l("avator.jpg", (File) entry.getValue(), (String) entry.getKey(), (byte) 0);
            i2++;
        }
        JSONObject jSONObject = new JSONObject(a(str2, map, lVarArr));
        String optString = jSONObject.optString("msg", null);
        if (z) {
            a(jSONObject, aVar);
            y yVar = new y();
            yVar.a(aVar, false);
            yVar.a(aVar.b, aVar.I);
            ay ayVar = new ay();
            ayVar.f3001a = aVar.J;
            ayVar.f = aVar.K;
            ayVar.d = aVar.L;
            if (jSONObject.optInt("role") != 0) {
                yVar.a(com.immomo.momo.g.q().h, aVar.b, jSONObject.optInt("role", 3));
            } else {
                yVar.b(com.immomo.momo.g.q().h, aVar.b);
            }
        }
        return optString;
    }

    public final String a(String str, boolean z, boolean z2, boolean z3, boolean z4) {
        HashMap hashMap = new HashMap();
        hashMap.put("gid", str);
        hashMap.put("sync_sina", z ? "1" : "0");
        hashMap.put("sync_renren", z2 ? "1" : "0");
        hashMap.put("sync_qqwb", z3 ? "1" : "0");
        hashMap.put("sync_weixin", z4 ? "1" : "0");
        return new JSONObject(a(String.valueOf(b) + "/group/share", hashMap)).optString("weixin_url");
    }

    public final List a(int i2, double d, double d2, int i3) {
        HashMap hashMap = new HashMap();
        hashMap.put("index", new StringBuilder().append(i2).toString());
        hashMap.put("count", "12");
        hashMap.put("lat", new StringBuilder().append(d).toString());
        hashMap.put("lng", new StringBuilder().append(d2).toString());
        hashMap.put("type", "0");
        hashMap.put("loctype", new StringBuilder().append(i3).toString());
        JSONArray optJSONArray = new JSONObject(a(String.valueOf(h) + "/nearby", hashMap)).optJSONArray("list");
        ArrayList arrayList = new ArrayList();
        if (optJSONArray == null || optJSONArray.length() == 0) {
            return arrayList;
        }
        for (int i4 = 0; i4 < optJSONArray.length(); i4++) {
            JSONObject jSONObject = optJSONArray.getJSONObject(i4);
            ay ayVar = new ay();
            arrayList.add(ayVar);
            ayVar.f3001a = jSONObject.optString("sid");
            ayVar.f = jSONObject.optString("name");
            ayVar.c = jSONObject.optInt("group_count");
            ayVar.d = jSONObject.optInt("type");
            ayVar.a((float) jSONObject.optLong("distance", -1));
            JSONArray optJSONArray2 = jSONObject.optJSONArray("groups");
            if (optJSONArray2 != null && optJSONArray2.length() > 0) {
                ArrayList arrayList2 = new ArrayList();
                ayVar.g = arrayList2;
                for (int i5 = 0; i5 < optJSONArray2.length(); i5++) {
                    a aVar = new a();
                    a(optJSONArray2.getJSONObject(i5), aVar);
                    arrayList2.add(aVar);
                }
            }
        }
        return arrayList;
    }

    public final void a(a aVar, List list, List list2, List list3) {
        HashMap hashMap = new HashMap();
        hashMap.put("gid", aVar.b);
        JSONObject optJSONObject = new JSONObject(a(String.valueOf(g) + "/v2/members", hashMap)).optJSONObject("data");
        JSONArray optJSONArray = optJSONObject.optJSONArray("owners");
        for (int i2 = 0; i2 < optJSONArray.length(); i2++) {
            j a2 = a(optJSONArray.getJSONObject(i2));
            list.add(a2);
            aVar.g = a2.f2979a;
        }
        JSONArray optJSONArray2 = optJSONObject.optJSONArray("admins");
        for (int i3 = 0; i3 < optJSONArray2.length(); i3++) {
            list2.add(a(optJSONArray2.getJSONObject(i3)));
        }
        JSONArray optJSONArray3 = optJSONObject.optJSONArray("members");
        for (int i4 = 0; i4 < optJSONArray3.length(); i4++) {
            list3.add(a(optJSONArray3.getJSONObject(i4)));
        }
        aVar.k = optJSONObject.optInt("total");
    }

    public final void a(String str, int i2) {
        HashMap hashMap = new HashMap();
        hashMap.put("hidden", new StringBuilder(String.valueOf(i2)).toString());
        a(String.valueOf(g) + "/setting/hiddenmode?gid=" + str, hashMap);
    }

    public final void a(String str, String str2) {
        HashMap hashMap = new HashMap();
        hashMap.put("reason", str);
        a(String.valueOf(g) + "/apply?gid=" + str2, hashMap);
    }

    public final void a(String str, List list, int i2, String str2) {
        HashMap hashMap = new HashMap();
        hashMap.put("momoid", android.support.v4.b.a.a(list, ","));
        hashMap.put("report", new StringBuilder().append(i2).toString());
        hashMap.put("reason", str2);
        a(String.valueOf(g) + "/remove_member?gid=" + str, hashMap);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.service.y.a(com.immomo.momo.service.bean.a.a, boolean):void
     arg types: [com.immomo.momo.service.bean.a.a, int]
     candidates:
      com.immomo.momo.service.y.a(com.immomo.momo.service.bean.a.a, java.lang.String):void
      com.immomo.momo.service.y.a(java.lang.String, int):void
      com.immomo.momo.service.y.a(java.lang.String, java.lang.String):void
      com.immomo.momo.service.y.a(java.lang.String, java.util.List):void
      com.immomo.momo.service.y.a(java.lang.String, boolean):void
      com.immomo.momo.service.y.a(java.lang.String, java.lang.String[]):void
      com.immomo.momo.service.y.a(java.util.List, java.lang.String):void
      com.immomo.momo.service.y.a(com.immomo.momo.service.bean.a.a, boolean):void */
    public final void a(List list, List list2) {
        JSONObject jSONObject = new JSONObject(a(String.valueOf(b) + "/android/groupdiscuss", (Map) null));
        JSONArray jSONArray = jSONObject.getJSONArray("group_lists");
        y yVar = new y();
        for (int i2 = 0; i2 < jSONArray.length(); i2++) {
            JSONObject jSONObject2 = jSONArray.getJSONObject(i2);
            a aVar = new a();
            a(jSONObject2, aVar);
            yVar.a(aVar, false);
            yVar.a(com.immomo.momo.g.q().h, aVar.b, jSONObject2.optInt("role", 3));
            list.add(aVar);
        }
        yVar.b(list);
        com.immomo.momo.service.h hVar = new com.immomo.momo.service.h();
        JSONArray jSONArray2 = jSONObject.getJSONArray("discuss_lists");
        for (int i3 = 0; i3 < jSONArray2.length(); i3++) {
            JSONObject jSONObject3 = jSONArray2.getJSONObject(i3);
            com.immomo.momo.service.bean.n nVar = new com.immomo.momo.service.bean.n();
            h.a();
            h.a(nVar, jSONObject3);
            list2.add(nVar);
            hVar.a(com.immomo.momo.g.q().h, nVar.f, jSONObject3.optInt("role", 3));
        }
    }

    public final boolean a(List list, double d, double d2, int i2, String str, int i3) {
        boolean z = true;
        HashMap hashMap = new HashMap();
        if (!android.support.v4.b.a.a((CharSequence) str)) {
            hashMap.put("name", str);
        }
        hashMap.put("lat", new StringBuilder(String.valueOf(d)).toString());
        hashMap.put("lng", new StringBuilder(String.valueOf(d2)).toString());
        hashMap.put("loctype", new StringBuilder().append(i2).toString());
        hashMap.put("index", new StringBuilder().append(i3).toString());
        hashMap.put("count", "10");
        JSONObject jSONObject = new JSONObject(a(String.valueOf(g) + "/search/keyword?fr=" + com.immomo.momo.g.q().h, hashMap));
        JSONArray optJSONArray = jSONObject.optJSONArray("list");
        if (jSONObject.optInt("remain") != 1) {
            z = false;
        }
        if (optJSONArray == null || optJSONArray.length() == 0) {
            return z;
        }
        for (int i4 = 0; i4 < optJSONArray.length(); i4++) {
            a aVar = new a();
            a(optJSONArray.getJSONObject(i4), aVar);
            list.add(aVar);
        }
        return z;
    }

    public final boolean a(List list, double d, double d2, String str, int i2, int i3, int i4) {
        HashMap hashMap = new HashMap();
        if (!android.support.v4.b.a.a((CharSequence) str)) {
            hashMap.put("keyword", str);
        }
        hashMap.put("type", new StringBuilder(String.valueOf(i2)).toString());
        hashMap.put("lat", new StringBuilder(String.valueOf(d)).toString());
        hashMap.put("lng", new StringBuilder(String.valueOf(d2)).toString());
        hashMap.put("loctype", new StringBuilder().append(i3).toString());
        hashMap.put("source", new StringBuilder().append(i4).toString());
        JSONObject jSONObject = new JSONObject(a(String.valueOf(h) + "/search", hashMap));
        JSONArray optJSONArray = jSONObject.optJSONArray("list");
        boolean optBoolean = jSONObject.optBoolean("remain");
        if (optJSONArray == null || optJSONArray.length() == 0) {
            return optBoolean;
        }
        int i5 = 0;
        while (true) {
            int i6 = i5;
            if (i6 >= optJSONArray.length()) {
                return optBoolean;
            }
            JSONObject jSONObject2 = optJSONArray.getJSONObject(i6);
            ay ayVar = new ay();
            ayVar.f3001a = jSONObject2.optString("sid");
            ayVar.f = jSONObject2.optString("name");
            ayVar.c = jSONObject2.optInt("group_count");
            ayVar.d = jSONObject2.optInt("type");
            ayVar.q = jSONObject2.optInt("frequent") == 1;
            ayVar.a((float) jSONObject2.optLong("distance"));
            list.add(ayVar);
            i5 = i6 + 1;
        }
    }

    public final h b() {
        return c(new JSONObject(a(String.valueOf(g) + "/getpermissions", (Map) null)));
    }

    public final String b(g gVar) {
        HashMap hashMap = new HashMap();
        hashMap.put("desc", gVar.d);
        hashMap.put("start_time", new StringBuilder(String.valueOf(android.support.v4.b.a.b(gVar.f))).toString());
        hashMap.put("lat", new StringBuilder(String.valueOf(gVar.j)).toString());
        hashMap.put("lng", new StringBuilder(String.valueOf(gVar.k)).toString());
        hashMap.put("loctype", new StringBuilder(String.valueOf(1)).toString());
        hashMap.put("need_phone", new StringBuilder(String.valueOf(gVar.h)).toString());
        hashMap.put("ptid", gVar.b);
        this.e.b((Object) ("editGroupParty submit:" + hashMap));
        JSONObject jSONObject = new JSONObject(a(String.valueOf(g) + "/party/edit", hashMap));
        gVar.l = jSONObject.optDouble("distance", 0.0d);
        return jSONObject.optString("msg");
    }

    public final String b(String str, String str2) {
        HashMap hashMap = new HashMap();
        hashMap.put("gid", str);
        hashMap.put("remoteid", str2);
        return new JSONObject(a(String.valueOf(g) + "/admin/cancel", hashMap)).optString("em");
    }

    public final List b(String str, int i2) {
        HashMap hashMap = new HashMap();
        hashMap.put("sid", str);
        hashMap.put("index", new StringBuilder(String.valueOf(i2)).toString());
        hashMap.put("count", new StringBuilder(String.valueOf(30)).toString());
        this.e.a(hashMap);
        JSONArray jSONArray = new JSONObject(a(String.valueOf(h) + "/group", hashMap)).getJSONArray("list");
        ArrayList arrayList = new ArrayList();
        for (int i3 = 0; i3 < jSONArray.length(); i3++) {
            a aVar = new a();
            a(jSONArray.getJSONObject(i3), aVar);
            arrayList.add(aVar);
        }
        return arrayList;
    }

    public final void b(String str) {
        a(String.valueOf(g) + "/quit?gid=" + str, (Map) null);
    }

    public final String c(String str, String str2) {
        HashMap hashMap = new HashMap();
        hashMap.put("gid", str);
        hashMap.put("remoteid", str2);
        return new JSONObject(a(String.valueOf(g) + "/admin/add", hashMap)).optString("em");
    }

    public final List c() {
        ArrayList arrayList = new ArrayList();
        JSONObject jSONObject = new JSONObject(a(String.valueOf(g) + "/search/keywordlist", (Map) null));
        String[] a2 = a(jSONObject.optJSONArray(j)) == null ? new String[0] : a(jSONObject.optJSONArray(j));
        return (a2 == null || a2.length <= 0) ? arrayList : Arrays.asList(a2);
    }

    public final void c(g gVar) {
        HashMap hashMap = new HashMap();
        hashMap.put("ptid", gVar.b);
        a(gVar, new JSONObject(a(String.valueOf(g) + "/party/profile", hashMap)));
    }

    public final void c(String str) {
        a(String.valueOf(g) + "/cancel?gid=" + str, (Map) null);
    }

    public final String d(String str) {
        HashMap hashMap = new HashMap();
        hashMap.put("ptid", str);
        return new JSONObject(a(String.valueOf(g) + "/party/remove", hashMap)).optString("msg", PoiTypeDef.All);
    }

    public final void d(String str, String str2) {
        HashMap hashMap = new HashMap();
        hashMap.put("content", str2);
        a(String.valueOf(g) + "/report?gid=" + str, hashMap);
    }

    public final String e(String str) {
        HashMap hashMap = new HashMap();
        hashMap.put("gid", str);
        return new JSONObject(a(String.valueOf(g) + "/setting/upgrade", hashMap)).optString("msg", PoiTypeDef.All);
    }

    public final void e(String str, String str2) {
        HashMap hashMap = new HashMap();
        hashMap.put("password", str2);
        hashMap.put("etype", "2");
        a(String.valueOf(g) + "/dismiss?gid=" + str, hashMap);
    }

    public final String f(String str, String str2) {
        HashMap hashMap = new HashMap();
        hashMap.put("ptid", str);
        hashMap.put("action", str2);
        return new JSONObject(a(String.valueOf(g) + "/party/join", hashMap)).optString("msg", PoiTypeDef.All);
    }

    public final String g(String str, String str2) {
        HashMap hashMap = new HashMap();
        hashMap.put("ptid", str);
        hashMap.put("action", str2);
        return new JSONObject(a(String.valueOf(g) + "/party/quit", hashMap)).optString("msg", PoiTypeDef.All);
    }
}
