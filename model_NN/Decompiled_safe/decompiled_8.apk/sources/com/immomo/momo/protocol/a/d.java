package com.immomo.momo.protocol.a;

import android.location.Location;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.a;
import com.immomo.momo.a.s;
import com.immomo.momo.a.t;
import com.immomo.momo.g;
import com.immomo.momo.plugin.e.b;
import com.immomo.momo.plugin.e.e;
import com.immomo.momo.service.bean.am;
import com.immomo.momo.service.bean.ar;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.service.bean.c;
import com.immomo.momo.util.h;
import com.immomo.momo.util.m;
import com.sina.sdk.api.message.InviteApi;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InterruptedIOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.net.ssl.SSLException;
import javax.net.ssl.SSLHandshakeException;
import org.json.JSONArray;
import org.json.JSONObject;

public final class d {
    private static final String A = (String.valueOf(c) + "/sina_weibo_share");
    private static final String B = (String.valueOf(c) + "/tencentweibo_share");
    private static final String C = (String.valueOf(c) + "/upgrade_share");
    private static final String D = (String.valueOf(c) + "/renren_share");
    private static final String E = (String.valueOf(c) + "/shareconfig");
    private static final String F = (String.valueOf(c) + "/convertlocation");
    private static m G = new m("HttpClient");

    /* renamed from: a  reason: collision with root package name */
    private static final String f2898a = a.d;
    private static final String b = a.e;
    private static final String c = (String.valueOf(a.c) + "/api");
    private static final String d = (String.valueOf(c) + "/setting/bindemailbyphone");
    private static final String e = (String.valueOf(c) + "/setting/inviteurl");
    private static final String f = (String.valueOf(c) + "/appconfig");
    private static final String g = (String.valueOf(c) + "/appconfig");
    private static final String h = (String.valueOf(c) + "/encryptdata/tencent");
    private static final String i = (String.valueOf(c) + "/uploadchatimage?action=multi");
    private static final String j = (String.valueOf(c) + "/uploadchataudio");
    private static final String k = (String.valueOf(c) + "/uploadchataudio?action=multi");
    private static final String l = (String.valueOf(c) + "/bind_sina_weibo");
    private static final String m = (String.valueOf(c) + "/unbind_sina_weibo");
    private static final String n = (String.valueOf(c) + "/sina_weibo_profile");
    private static final String o = (String.valueOf(c) + "/sina_weibo_timeline/");
    private static final String p = (String.valueOf(c) + "/bind_tencentweibo");
    private static final String q = (String.valueOf(c) + "/douban_profile/");
    private static final String r = (String.valueOf(c) + "/renren_profile/");
    private static final String s = (String.valueOf(c) + "/tencentweibo_profile/");
    private static final String t = (String.valueOf(c) + "/tencentweibo_timeline/");
    private static final String u = (String.valueOf(c) + "/encryptdata");
    private static final String v = (String.valueOf(c) + "/bind_renren");
    private static final String w = (String.valueOf(c) + "/unbind_renren");
    private static final String x = (String.valueOf(c) + "/bind_douban");
    private static final String y = (String.valueOf(c) + "/unbind_douban");
    private static final String z = (String.valueOf(c) + "/unbind_tencentweibo");

    static {
        String.valueOf(c) + "/setallsessionread";
        String.valueOf(c) + "/publish";
        String.valueOf(c) + "/sina_weibo_oauth";
        String.valueOf(c) + "/sina_weibo_share/creategroup";
        String.valueOf(c) + "/getlatlng?autoconvert=YES";
        String.valueOf(c) + "/getlatlng?autoconvert=NO";
    }

    public static int a(Location location, double d2, double d3, float f2) {
        HashMap hashMap = new HashMap();
        hashMap.put("lat", new StringBuilder().append(d2).toString());
        hashMap.put("lng", new StringBuilder().append(d3).toString());
        hashMap.put("acc", new StringBuilder().append(f2).toString());
        hashMap.put("loctype", "0");
        JSONObject jSONObject = new JSONObject(a(F, hashMap, (l[]) null));
        location.setLatitude(jSONObject.getDouble("lat"));
        location.setLongitude(jSONObject.getDouble("lng"));
        location.setAccuracy((float) jSONObject.optInt("acc"));
        return jSONObject.optInt("loctype", 0);
    }

    public static int a(String str, String str2, String str3, String str4, int i2) {
        HashMap hashMap = new HashMap();
        hashMap.put("oauth_token", str);
        hashMap.put("user_id", str2);
        hashMap.put("expiretime", str3);
        hashMap.put("remindtime", str4);
        hashMap.put("enforce", String.valueOf(i2));
        hashMap.put("version", "2");
        G.b(hashMap);
        JSONObject jSONObject = new JSONObject(a(String.valueOf(l) + "/" + g.q().h, hashMap, (l[]) null));
        if (jSONObject.has("weibo_remain_day")) {
            g.q().aG = jSONObject.optInt("weibo_remain_day");
        }
        return jSONObject.optBoolean("vip") ? 1 : 0;
    }

    /* JADX WARNING: Removed duplicated region for block: B:31:0x009f A[SYNTHETIC, Splitter:B:31:0x009f] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00a4  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.graphics.Bitmap a(double r6, double r8) {
        /*
            r1 = 0
            java.lang.StringBuffer r0 = new java.lang.StringBuffer
            r0.<init>()
            java.lang.String r2 = "http://maps.google.cn/maps/api/staticmap"
            r0.append(r2)
            java.lang.String r2 = "?size=200x100"
            r0.append(r2)
            java.lang.String r2 = "&zoom=13"
            r0.append(r2)
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "&markers=color:red|label:YourPosition|"
            r2.<init>(r3)
            java.lang.StringBuilder r2 = r2.append(r6)
            java.lang.String r3 = ","
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r8)
            java.lang.String r2 = r2.toString()
            r0.append(r2)
            java.lang.String r2 = "&maptype=roadmap"
            r0.append(r2)
            java.lang.String r2 = "&sensor=false"
            r0.append(r2)
            java.lang.String r2 = "&format=jpg"
            r0.append(r2)
            java.net.URL r2 = new java.net.URL     // Catch:{ Exception -> 0x0080, all -> 0x009b }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0080, all -> 0x009b }
            r2.<init>(r0)     // Catch:{ Exception -> 0x0080, all -> 0x009b }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0080, all -> 0x009b }
            java.lang.String r3 = "url--->"
            r0.<init>(r3)     // Catch:{ Exception -> 0x0080, all -> 0x009b }
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Exception -> 0x0080, all -> 0x009b }
            r0.toString()     // Catch:{ Exception -> 0x0080, all -> 0x009b }
            java.net.URLConnection r0 = r2.openConnection()     // Catch:{ Exception -> 0x0080, all -> 0x009b }
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x0080, all -> 0x009b }
            java.lang.String r2 = "GET"
            r0.setRequestMethod(r2)     // Catch:{ Exception -> 0x00bc, all -> 0x00af }
            r0.connect()     // Catch:{ Exception -> 0x00bc, all -> 0x00af }
            java.io.InputStream r3 = r0.getInputStream()     // Catch:{ Exception -> 0x00bc, all -> 0x00af }
            android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeStream(r3)     // Catch:{ Exception -> 0x00c1, all -> 0x00b3 }
            if (r3 == 0) goto L_0x0072
            r3.close()     // Catch:{ IOException -> 0x0079 }
        L_0x0072:
            if (r0 == 0) goto L_0x0077
            r0.disconnect()
        L_0x0077:
            r0 = r1
        L_0x0078:
            return r0
        L_0x0079:
            r2 = move-exception
            com.immomo.momo.util.m r3 = com.immomo.momo.protocol.a.d.G
            r3.a(r2)
            goto L_0x0072
        L_0x0080:
            r0 = move-exception
            r2 = r1
            r3 = r1
        L_0x0083:
            com.immomo.momo.util.m r4 = com.immomo.momo.protocol.a.d.G     // Catch:{ all -> 0x00b9 }
            r4.a(r0)     // Catch:{ all -> 0x00b9 }
            if (r2 == 0) goto L_0x008d
            r2.close()     // Catch:{ IOException -> 0x0094 }
        L_0x008d:
            if (r3 == 0) goto L_0x0092
            r3.disconnect()
        L_0x0092:
            r0 = r1
            goto L_0x0078
        L_0x0094:
            r0 = move-exception
            com.immomo.momo.util.m r2 = com.immomo.momo.protocol.a.d.G
            r2.a(r0)
            goto L_0x008d
        L_0x009b:
            r0 = move-exception
            r3 = r1
        L_0x009d:
            if (r1 == 0) goto L_0x00a2
            r1.close()     // Catch:{ IOException -> 0x00a8 }
        L_0x00a2:
            if (r3 == 0) goto L_0x00a7
            r3.disconnect()
        L_0x00a7:
            throw r0
        L_0x00a8:
            r1 = move-exception
            com.immomo.momo.util.m r2 = com.immomo.momo.protocol.a.d.G
            r2.a(r1)
            goto L_0x00a2
        L_0x00af:
            r2 = move-exception
            r3 = r0
            r0 = r2
            goto L_0x009d
        L_0x00b3:
            r1 = move-exception
            r5 = r1
            r1 = r3
            r3 = r0
            r0 = r5
            goto L_0x009d
        L_0x00b9:
            r0 = move-exception
            r1 = r2
            goto L_0x009d
        L_0x00bc:
            r2 = move-exception
            r3 = r0
            r0 = r2
            r2 = r1
            goto L_0x0083
        L_0x00c1:
            r2 = move-exception
            r5 = r2
            r2 = r3
            r3 = r0
            r0 = r5
            goto L_0x0083
        */
        throw new UnsupportedOperationException("Method not decompiled: com.immomo.momo.protocol.a.d.a(double, double):android.graphics.Bitmap");
    }

    public static ar a(int i2) {
        HashMap hashMap = new HashMap();
        hashMap.put("client", "android");
        hashMap.put("mark", "75");
        hashMap.put("industry_version", new StringBuilder(String.valueOf(i2)).toString());
        hashMap.put("temp_uid", g.I());
        ar arVar = new ar();
        a(new JSONObject(a(f, hashMap, (l[]) null)), arVar);
        return arVar;
    }

    public static File a(String str, int i2) {
        StringBuffer stringBuffer = new StringBuffer();
        String substring = str.substring(0, 2);
        String substring2 = str.substring(2, 4);
        if (i2 == 2 || i2 == 3) {
            stringBuffer.append(f2898a).append("/gchataudio/" + substring + "/" + substring2 + "/" + str + ".amr");
        } else {
            stringBuffer.append(b).append("/chataudio/" + substring + "/" + substring2 + "/" + str + ".amr");
        }
        byte[] a2 = q.a(stringBuffer.toString(), null, null);
        com.immomo.momo.util.ar.c((long) a2.length);
        a.C();
        File b2 = h.b(String.valueOf(str) + ".amr");
        if (!b2.exists()) {
            b2.createNewFile();
        }
        FileOutputStream fileOutputStream = new FileOutputStream(b2);
        fileOutputStream.write(a2);
        fileOutputStream.flush();
        fileOutputStream.close();
        return b2;
    }

    public static String a(String str, File file, String str2, int i2) {
        HashMap hashMap = new HashMap();
        hashMap.put("uuid", str2);
        hashMap.put("remoteid", new StringBuilder(String.valueOf(str)).toString());
        if (i2 == 2 || i2 == 3) {
            hashMap.put("type", "group");
        }
        G.a((Object) (hashMap + "; fileName=" + file.getName()));
        return new JSONObject(a(j, hashMap, new l[]{new l(String.valueOf(file.getName()) + ".amr", file, "fileUpload", (byte) 0)})).getString("filename");
    }

    private static String a(String str, Map map) {
        try {
            if (g.q() != null) {
                str = android.support.v4.b.a.a(str, "fr", g.q().h);
            }
            String str2 = new String(q.a(str, map, null));
            G.a((Object) ("[urlString] Result : " + str2));
            f(str2);
            return str2;
        } catch (InterruptedIOException e2) {
            throw new s();
        } catch (SSLHandshakeException e3) {
            throw new t(e3);
        } catch (SSLException e4) {
            throw new t(e4, g.a((int) R.string.errormsg_ssl));
        }
    }

    private static String a(String str, Map map, l[] lVarArr) {
        try {
            if (g.q() != null) {
                str = android.support.v4.b.a.a(str, "fr", g.q().h);
            }
            String str2 = new String(q.a(str, map, lVarArr, null));
            G.a((Object) ("[urlString] Result : " + str2));
            f(str2);
            return str2;
        } catch (InterruptedIOException e2) {
            throw new s();
        } catch (SSLHandshakeException e3) {
            throw new t(e3);
        } catch (SSLException e4) {
            throw new t(e4, g.a((int) R.string.errormsg_ssl));
        }
    }

    public static String a(String str, boolean z2) {
        String str2 = String.valueOf(v) + "/" + g.q().h;
        HashMap hashMap = new HashMap();
        hashMap.put("authorization_code", str);
        hashMap.put("enforce", z2 ? "1" : "0");
        return new JSONObject(a(str2, hashMap, (l[]) null)).getString("renren_user_id");
    }

    public static String a(boolean z2, boolean z3, boolean z4, boolean z5) {
        String str = String.valueOf(c) + "/vip/share?fr=" + g.q().h;
        HashMap hashMap = new HashMap();
        hashMap.put("sync_sina", z2 ? "1" : "0");
        hashMap.put("sync_renren", z3 ? "1" : "0");
        hashMap.put("sync_qqwb", z4 ? "1" : "0");
        hashMap.put("sync_weixin", z5 ? "1" : "0");
        return new JSONObject(a(str, hashMap, (l[]) null)).optString("weixin_url");
    }

    public static String a(byte[] bArr, int i2, String str, long j2, long j3, int i3) {
        HashMap hashMap = new HashMap();
        hashMap.put("uuid", str);
        hashMap.put("offset", new StringBuilder(String.valueOf(j2)).toString());
        hashMap.put("length", new StringBuilder(String.valueOf(j3)).toString());
        if (i3 == 2 || i3 == 3) {
            hashMap.put("type", "group");
        }
        G.a(hashMap);
        String a2 = a(i, hashMap, new l[]{new l("upload.jpg", bArr, "fileblock", "application/octet-stream")});
        if (((long) i2) + j2 >= j3) {
            return new JSONObject(a2).optString("filename");
        }
        return null;
    }

    public static List a(String str, String str2, int i2, long j2, long j3) {
        StringBuilder sb = new StringBuilder(String.valueOf(t));
        if (str == null) {
            str = "0";
        }
        String sb2 = sb.append(str).toString();
        HashMap hashMap = new HashMap();
        if (str2 != null) {
            hashMap.put("qqwbuid", str2);
        }
        hashMap.put("contenttype", String.valueOf(0));
        hashMap.put("reqnum", String.valueOf(10));
        hashMap.put("pageflag", String.valueOf(i2));
        hashMap.put("pagetime", String.valueOf(j2));
        hashMap.put("lastid", String.valueOf(j3));
        return com.immomo.momo.plugin.f.a.b(a(sb2, hashMap, (l[]) null));
    }

    public static List a(String str, String str2, String str3) {
        StringBuilder sb = new StringBuilder(String.valueOf(o));
        if (android.support.v4.b.a.a((CharSequence) str)) {
            str = "0";
        }
        String sb2 = sb.append(str).toString();
        HashMap hashMap = new HashMap();
        if (str2 != null) {
            hashMap.put("sina_uid", str2);
        }
        hashMap.put("pageflag", String.valueOf(1));
        hashMap.put("reqnum", String.valueOf(20));
        hashMap.put("lastid", str3);
        hashMap.put("contenttype", String.valueOf(0L));
        return com.immomo.momo.plugin.e.d.a(a(sb2, hashMap, (l[]) null));
    }

    public static void a(bf bfVar) {
        a(String.valueOf(D) + "/" + bfVar.h, (Map) null);
    }

    public static void a(bf bfVar, boolean z2) {
        String str = String.valueOf(A) + "/" + bfVar.h;
        HashMap hashMap = new HashMap();
        hashMap.put("isfollow", new StringBuilder().append(z2 ? 1 : 0).toString());
        a(str, hashMap, (l[]) null);
    }

    public static void a(String str, String str2, String str3, String str4) {
        HashMap hashMap = new HashMap();
        hashMap.put("oauth_token", str2);
        hashMap.put("oauth_token_secret", str3);
        hashMap.put("user_id", str);
        hashMap.put("oauth_verifier", str4);
        a(String.valueOf(l) + "/" + g.q().h, hashMap, (l[]) null);
    }

    private static void a(JSONObject jSONObject, ar arVar) {
        if (arVar == null) {
            arVar = new ar();
        }
        try {
            arVar.f2994a = jSONObject.getString("version_name");
        } catch (Exception e2) {
        }
        try {
            arVar.c = jSONObject.getInt("version_counter");
        } catch (Exception e3) {
        }
        try {
            arVar.b = String.valueOf(jSONObject.getString("version_url")) + "?" + System.currentTimeMillis();
        } catch (Exception e4) {
        }
        try {
            jSONObject.getString("chat_notice");
        } catch (Exception e5) {
        }
        try {
            jSONObject.getString("test_url");
        } catch (Exception e6) {
        }
        try {
            arVar.f = jSONObject.optLong("deny_count", 0);
        } catch (Exception e7) {
        }
        try {
            arVar.g = jSONObject.optLong("report_count", 0);
        } catch (Exception e8) {
        }
        try {
            arVar.h = jSONObject.getString("deny_info");
        } catch (Exception e9) {
        }
        try {
            arVar.d = jSONObject.getInt("industry_version");
        } catch (Exception e10) {
        }
        try {
            if (jSONObject.has("industry")) {
                arVar.e = new ArrayList();
                JSONArray jSONArray = jSONObject.getJSONArray("industry");
                if (jSONArray != null && jSONArray.length() > 0) {
                    for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                        am amVar = new am();
                        JSONObject jSONObject2 = jSONArray.getJSONObject(i2);
                        amVar.f2991a = jSONObject2.getString("id");
                        amVar.b = jSONObject2.getString("detail");
                        amVar.c = jSONObject2.getString("char");
                        amVar.d = jSONObject2.getString("icon");
                        arVar.e.add(amVar);
                    }
                }
            }
        } catch (Exception e11) {
            G.a((Throwable) e11);
            arVar.e = null;
        }
    }

    public static void a(boolean z2, boolean z3, boolean z4, bf bfVar) {
        String str = C;
        HashMap hashMap = new HashMap();
        hashMap.put("version", new StringBuilder(String.valueOf(g.z())).toString());
        hashMap.put("client", "android");
        hashMap.put("fr", bfVar.h);
        hashMap.put("sync_sina", z2 ? "1" : "0");
        hashMap.put("sync_renren", z4 ? "1" : "0");
        hashMap.put("sync_qqwb", z3 ? "1" : "0");
        a(str, hashMap);
    }

    public static int[] a() {
        HashMap hashMap = new HashMap();
        hashMap.put("mark", "1024");
        JSONObject jSONObject = new JSONObject(a(g, hashMap, (l[]) null));
        if (!jSONObject.has("weibo_count") && !jSONObject.has("renren_count") && !jSONObject.has("qqwb_count")) {
            return null;
        }
        return new int[]{jSONObject.optInt("weibo_count", 0), jSONObject.optInt("renren_count", 0), jSONObject.optInt("qqwb_count", 0)};
    }

    public static String[] a(String str) {
        String str2 = h;
        HashMap hashMap = new HashMap();
        hashMap.put("momoid", str);
        hashMap.put("temp_uid", g.I());
        JSONObject jSONObject = new JSONObject(a(str2, hashMap, (l[]) null));
        return new String[]{jSONObject.getString("key"), jSONObject.getString("secret")};
    }

    public static String[] a(String str, String str2) {
        HashMap hashMap = new HashMap();
        hashMap.put("code", str);
        hashMap.put("enforce", str2);
        G.b(hashMap);
        JSONObject jSONObject = new JSONObject(a(String.valueOf(p) + "/" + g.q().h, hashMap, (l[]) null));
        String[] strArr = new String[2];
        if (jSONObject.has("tencent_user_id")) {
            strArr[0] = jSONObject.optString("tencent_user_id", PoiTypeDef.All);
        }
        if (jSONObject.has("tencent_vip_desc")) {
            strArr[1] = jSONObject.optString("tencent_vip_desc", PoiTypeDef.All);
        } else {
            strArr[1] = PoiTypeDef.All;
        }
        return strArr;
    }

    public static com.immomo.momo.plugin.e.a b(String str, String str2, String str3, String str4) {
        return b.a(new String(q.a("https://api.weibo.com/oauth2/access_token?client_id=" + str + "&client_secret=" + str2 + "&grant_type=authorization_code&code=" + str3 + "&redirect_uri=" + str4, null, null, null)));
    }

    public static e b(String str, String str2) {
        if (android.support.v4.b.a.a((CharSequence) str) && android.support.v4.b.a.a((CharSequence) str2)) {
            return null;
        }
        HashMap hashMap = new HashMap();
        if (!android.support.v4.b.a.a((CharSequence) str)) {
            hashMap.put("weibouid", str);
        }
        StringBuilder append = new StringBuilder(String.valueOf(n)).append("/");
        if (android.support.v4.b.a.a((CharSequence) str2)) {
            str2 = "0";
        }
        JSONObject jSONObject = new JSONObject(a(append.append(str2).toString(), hashMap, (l[]) null));
        e eVar = new e();
        com.immomo.momo.plugin.e.d.a(eVar, jSONObject.getString("profiles"));
        return eVar;
    }

    public static c b() {
        HashMap hashMap = new HashMap();
        hashMap.put("mark", "4");
        hashMap.put("temp_uid", g.I());
        JSONObject jSONObject = new JSONObject(a(f, hashMap, (l[]) null));
        c cVar = new c();
        cVar.a(jSONObject.getString("akey"));
        cVar.b(jSONObject.getString("skey"));
        return cVar;
    }

    public static String b(byte[] bArr, int i2, String str, long j2, long j3, int i3) {
        HashMap hashMap = new HashMap();
        hashMap.put("uuid", str);
        hashMap.put("offset", new StringBuilder(String.valueOf(j2)).toString());
        hashMap.put("length", new StringBuilder(String.valueOf(j3)).toString());
        hashMap.put("remoteid", new StringBuilder(String.valueOf((Object) null)).toString());
        if (i3 == 2 || i3 == 3) {
            hashMap.put("type", "group");
        }
        String a2 = a(k, hashMap, new l[]{new l("audio.amr", bArr, "fileblock", (String) null)});
        if (((long) i2) + j2 < j3) {
            return null;
        }
        try {
            return new JSONObject(a2).getString("filename");
        } catch (Exception e2) {
            G.a((Throwable) e2);
            return null;
        }
    }

    public static void b(bf bfVar, boolean z2) {
        String str = String.valueOf(B) + "/" + bfVar.h;
        HashMap hashMap = new HashMap();
        hashMap.put("isfollow", new StringBuilder().append(z2 ? 1 : 0).toString());
        a(str, hashMap, (l[]) null);
    }

    public static void b(String str, String str2, String str3) {
        HashMap hashMap = new HashMap();
        hashMap.put("oauth_token", str2);
        hashMap.put("oauth_token_secret", str3);
        hashMap.put("douban_user_id", str);
        a(String.valueOf(x) + "/" + g.q().h, hashMap, (l[]) null);
    }

    public static String[] b(String str) {
        HashMap hashMap = new HashMap();
        hashMap.put("momoid", g.q().h);
        JSONObject jSONObject = new JSONObject(a(String.valueOf(u) + "/" + str, hashMap, (l[]) null));
        String[] strArr = new String[3];
        try {
            if (jSONObject.has("key")) {
                strArr[0] = jSONObject.getString("key");
            }
            if (jSONObject.has("secret")) {
                strArr[1] = jSONObject.getString("secret");
            }
            if (!jSONObject.has("id")) {
                return strArr;
            }
            strArr[2] = jSONObject.getString("id");
            return strArr;
        } catch (Exception e2) {
            return null;
        }
    }

    public static com.immomo.momo.plugin.a.d c(String str) {
        return com.immomo.momo.plugin.a.b.a(new JSONObject(a(String.valueOf(q) + str, (Map) null)).getString("profiles"));
    }

    public static com.immomo.momo.plugin.d.c c(String str, String str2) {
        StringBuilder sb = new StringBuilder(String.valueOf(r));
        if (android.support.v4.b.a.a((CharSequence) str)) {
            str = "0";
        }
        String sb2 = sb.append(str).toString();
        HashMap hashMap = new HashMap();
        if (!android.support.v4.b.a.a((CharSequence) str2)) {
            hashMap.put("renrenuid", str2);
        }
        return com.immomo.momo.plugin.d.a.a(a(sb2, hashMap, (l[]) null));
    }

    public static JSONArray c() {
        return new JSONObject(a(String.valueOf(f) + "?mark=16", new HashMap(), (l[]) null)).optJSONArray("ap");
    }

    public static com.immomo.momo.plugin.f.b d(String str, String str2) {
        StringBuilder sb = new StringBuilder(String.valueOf(s));
        if (str == null) {
            str = "0";
        }
        String sb2 = sb.append(str).toString();
        HashMap hashMap = new HashMap();
        if (str2 != null) {
            hashMap.put("qqwbuid", str2);
        }
        return com.immomo.momo.plugin.f.a.a(a(sb2, hashMap, (l[]) null));
    }

    public static void d() {
        a(String.valueOf(m) + "/" + g.q().h, (Map) null, (l[]) null);
    }

    public static String[] d(String str) {
        HashMap hashMap = new HashMap();
        hashMap.put("mark", "1");
        hashMap.put("gid", str);
        G.b((Object) ("getShareGruopInfo : " + hashMap));
        JSONObject jSONObject = new JSONObject(a(String.valueOf(E) + "?fr=" + g.q().h, hashMap, (l[]) null));
        return new String[]{jSONObject.getString("pic"), jSONObject.getString("desc"), jSONObject.getString("weixin_desc")};
    }

    public static void e() {
        a(String.valueOf(w) + "/" + g.q().h, (Map) null, (l[]) null);
    }

    public static void e(String str, String str2) {
        HashMap hashMap = new HashMap();
        hashMap.put("email", str);
        hashMap.put("password", str2);
        hashMap.put("etype", "2");
        a(d, hashMap, (l[]) null);
    }

    public static String[] e(String str) {
        HashMap hashMap = new HashMap();
        hashMap.put("mark", "8");
        hashMap.put("momoid", str);
        JSONObject jSONObject = new JSONObject(a(String.valueOf(E) + "?fr=" + g.q().h, hashMap, (l[]) null));
        return new String[]{jSONObject.getString("pic"), jSONObject.getString("desc"), jSONObject.getString("weixin_desc")};
    }

    public static void f() {
        a(String.valueOf(z) + "/" + g.q().h, (Map) null, (l[]) null);
    }

    /*  JADX ERROR: JadxOverflowException in pass: RegionMakerVisitor
        jadx.core.utils.exceptions.JadxOverflowException: Regions count limit reached
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
        */
    /* JADX WARNING: Removed duplicated region for block: B:10:0x002e  */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0034  */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x003a  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0040  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0046  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x004c  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0052  */
    /* JADX WARNING: Removed duplicated region for block: B:4:0x001c  */
    /* JADX WARNING: Removed duplicated region for block: B:6:0x0022  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0028  */
    private static void f(java.lang.String r3) {
        /*
            org.json.JSONObject r0 = new org.json.JSONObject
            r0.<init>(r3)
            java.lang.String r1 = "ok"
            boolean r1 = r0.getBoolean(r1)
            if (r1 != 0) goto L_0x0058
            java.lang.String r1 = "errcode"
            int r1 = r0.optInt(r1)
            java.lang.String r2 = "errmsg"
            java.lang.String r0 = r0.optString(r2)
            switch(r1) {
                case 400: goto L_0x0022;
                case 401: goto L_0x0028;
                case 403: goto L_0x002e;
                case 404: goto L_0x0034;
                case 405: goto L_0x003a;
                case 406: goto L_0x0040;
                case 409: goto L_0x0046;
                case 410: goto L_0x004c;
                case 500: goto L_0x0052;
                default: goto L_0x001c;
            }
        L_0x001c:
            com.immomo.momo.a.a r1 = new com.immomo.momo.a.a
            r1.<init>(r0)
            throw r1
        L_0x0022:
            com.immomo.momo.a.e r1 = new com.immomo.momo.a.e
            r1.<init>(r0)
            throw r1
        L_0x0028:
            com.immomo.momo.a.f r1 = new com.immomo.momo.a.f
            r1.<init>(r0)
            throw r1
        L_0x002e:
            com.immomo.momo.a.g r0 = new com.immomo.momo.a.g
            r0.<init>()
            throw r0
        L_0x0034:
            com.immomo.momo.a.h r1 = new com.immomo.momo.a.h
            r1.<init>(r0)
            throw r1
        L_0x003a:
            com.immomo.momo.a.l r1 = new com.immomo.momo.a.l
            r1.<init>(r0)
            throw r1
        L_0x0040:
            com.immomo.momo.a.n r1 = new com.immomo.momo.a.n
            r1.<init>(r0)
            throw r1
        L_0x0046:
            com.immomo.momo.a.o r1 = new com.immomo.momo.a.o
            r1.<init>(r0)
            throw r1
        L_0x004c:
            com.immomo.momo.a.p r1 = new com.immomo.momo.a.p
            r1.<init>(r0)
            throw r1
        L_0x0052:
            com.immomo.momo.a.q r1 = new com.immomo.momo.a.q
            r1.<init>(r0)
            throw r1
        L_0x0058:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.immomo.momo.protocol.a.d.f(java.lang.String):void");
    }

    public static void g() {
        a(String.valueOf(y) + "/" + g.q().h, (Map) null, (l[]) null);
    }

    public static String h() {
        HashMap hashMap = new HashMap();
        hashMap.put("momoid", g.q().h);
        return new JSONObject(a(e, hashMap, (l[]) null)).getString(InviteApi.KEY_URL);
    }
}
