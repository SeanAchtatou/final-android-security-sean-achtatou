package com.immomo.momo.protocol.imjson;

import com.immomo.a.a.e.b;
import com.immomo.a.a.e.c;
import com.immomo.a.a.e.d;
import com.immomo.momo.g;
import java.io.File;

public final class n extends d {
    public final b a() {
        return new m();
    }

    /* access modifiers changed from: protected */
    public final c b(String str) {
        File file = new File(g.c().getFilesDir(), "sync");
        if (!file.exists()) {
            file.mkdirs();
        }
        return super.b(new File(file, str).getAbsolutePath());
    }
}
