package com.immomo.momo.protocol.imjson.task;

import android.content.Intent;
import com.immomo.momo.a.q;
import com.immomo.momo.android.broadcast.i;
import com.immomo.momo.g;
import com.immomo.momo.protocol.imjson.a;
import com.immomo.momo.service.af;
import com.immomo.momo.service.bean.Message;
import com.immomo.momo.util.h;

final class c extends Thread {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ImageMessageTask f2941a;
    private final /* synthetic */ Message b;

    c(ImageMessageTask imageMessageTask, Message message) {
        this.f2941a = imageMessageTask;
        this.b = message;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent} */
    public final void run() {
        try {
            if (this.b.fileUploadedLength == this.f2941a.f2935a.length()) {
                this.b.fileUploadedLength = 0;
            }
            String a2 = a.a(this.f2941a.f2935a, this.b.fileUploadedLength, this.b.msgId, new d(this.b), this.b.chatType);
            if (android.support.v4.b.a.f(a2)) {
                this.b.fileName = a2;
                this.b.fileUploadSuccess = true;
                af.c().b(this.b);
                h.b(this.b.msgId, a2, 0, true);
                this.f2941a.e = true;
            }
        } catch (Exception e) {
            this.f2941a.c.a((Throwable) e);
            if (e instanceof q) {
                this.b.fileUploadedLength = 0;
                af.c().b(this.b);
                Intent intent = new Intent(i.f2352a);
                intent.putExtra("key_message_id", this.b.msgId);
                intent.putExtra("key_upload_progress", 0);
                g.d().sendBroadcast(intent);
            }
        }
        if (!this.f2941a.e) {
            ImageMessageTask imageMessageTask = this.f2941a;
            int i = imageMessageTask.b;
            imageMessageTask.b = i + 1;
            if (i >= 3) {
                this.f2941a.c();
            }
            Intent intent2 = new Intent(i.f2352a);
            intent2.putExtra("key_message_id", this.b.msgId);
            intent2.putExtra("key_upload_progress", -1L);
            g.d().sendBroadcast(intent2);
        }
        if (this.f2941a.g != null) {
            this.f2941a.g.cancel();
            this.f2941a.g = null;
            this.f2941a.h.purge();
        }
        synchronized (this.f2941a.f) {
            this.f2941a.f.notify();
        }
    }
}
