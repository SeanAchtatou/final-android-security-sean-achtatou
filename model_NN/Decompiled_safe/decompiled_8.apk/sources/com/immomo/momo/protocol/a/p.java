package com.immomo.momo.protocol.a;

import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.a;
import com.immomo.momo.a.b;
import com.immomo.momo.a.c;
import com.immomo.momo.a.d;
import com.immomo.momo.a.e;
import com.immomo.momo.a.f;
import com.immomo.momo.a.g;
import com.immomo.momo.a.i;
import com.immomo.momo.a.j;
import com.immomo.momo.a.k;
import com.immomo.momo.a.l;
import com.immomo.momo.a.n;
import com.immomo.momo.a.o;
import com.immomo.momo.a.q;
import com.immomo.momo.a.s;
import com.immomo.momo.a.t;
import com.immomo.momo.android.activity.fs;
import com.immomo.momo.h;
import com.immomo.momo.util.jni.Codec;
import com.immomo.momo.util.m;
import com.immomo.momo.util.v;
import com.mapabc.minimap.map.vmap.VMapProjection;
import java.io.InterruptedIOException;
import java.util.Date;
import java.util.Map;
import javax.net.ssl.SSLException;
import javax.net.ssl.SSLHandshakeException;
import javax.security.cert.CertificateException;
import mm.purchasesdk.PurchaseCode;
import org.json.JSONArray;
import org.json.JSONObject;

public class p {

    /* renamed from: a  reason: collision with root package name */
    public static final String f2900a = a.c;
    public static final String b = (String.valueOf(f2900a) + "/api");
    public static final String c = a.f;
    public static final String d = ("http://" + Codec.kwiwek(0));
    private static String f = a.d;
    private static String g = a.e;
    private static m h = new m("HttpClient");
    protected m e = new m(this);

    public static v a(String str, int i, fs fsVar) {
        if (android.support.v4.b.a.a((CharSequence) str)) {
            return null;
        }
        StringBuffer stringBuffer = new StringBuffer();
        String substring = str.substring(0, 2);
        String substring2 = str.substring(2, 4);
        switch (i) {
            case 0:
                stringBuffer.append("/chatimage/" + substring + "/" + substring2 + "/" + str + "_L.jpg");
                break;
            case 1:
                stringBuffer.append("/chatimage/" + substring + "/" + substring2 + "/" + str + "_S.jpg");
                break;
            case 2:
                stringBuffer.append("/album/" + substring + "/" + substring2 + "/" + str + "_L.jpg");
                break;
            case 3:
                stringBuffer.append("/album/" + substring + "/" + substring2 + "/" + str + "_S.jpg");
                break;
            case 11:
                stringBuffer.append("/m/industry/24x24/" + str);
                break;
            case 12:
                stringBuffer.append("/m/industry/32x32/" + str);
                break;
            case 13:
                stringBuffer.append("/gchatimage/" + substring + "/" + substring2 + "/" + str + "_L.jpg");
                break;
            case h.DragSortListView_drag_handle_id:
                stringBuffer.append("/gchatimage/" + substring + "/" + substring2 + "/" + str + "_S.jpg");
                break;
            case 15:
                stringBuffer.append("/feedimage/" + substring + "/" + substring2 + "/" + str + "_S.jpg");
                break;
            case 16:
                stringBuffer.append("/feedimage/" + substring + "/" + substring2 + "/" + str + "_L.jpg");
                break;
            case VMapProjection.MAXZOOMLEVEL /*20*/:
                stringBuffer.append("/event/" + substring + "/" + substring2 + "/" + str + "_S.jpg");
                break;
            case 21:
                stringBuffer.append("/event/" + substring + "/" + substring2 + "/" + str + "_L.jpg");
                break;
        }
        String stringBuffer2 = stringBuffer.toString();
        StringBuffer stringBuffer3 = new StringBuffer();
        if (i == 0 || i == 1) {
            stringBuffer3.append(String.valueOf(g) + stringBuffer2);
        } else {
            stringBuffer3.append(String.valueOf(f) + stringBuffer2);
        }
        return a(stringBuffer3.toString(), fsVar);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00b6, code lost:
        r1 = r11;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x0115, code lost:
        r1 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:79:0x0121, code lost:
        r1 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x0132, code lost:
        r1 = e;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x00c9  */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x00eb  */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x0115 A[ExcHandler: SSLHandshakeException (e javax.net.ssl.SSLHandshakeException), Splitter:B:1:0x0003] */
    /* JADX WARNING: Removed duplicated region for block: B:79:0x0121 A[Catch:{ InterruptedIOException -> 0x0143, SSLHandshakeException -> 0x0115, SSLException -> 0x0121, Exception -> 0x0132, all -> 0x011f }, ExcHandler: SSLException (e javax.net.ssl.SSLException), Splitter:B:1:0x0003] */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x0132 A[Catch:{ InterruptedIOException -> 0x0143, SSLHandshakeException -> 0x0115, SSLException -> 0x0121, Exception -> 0x0132, all -> 0x011f }, ExcHandler: Exception (e java.lang.Exception), Splitter:B:1:0x0003] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:28:0x0096=Splitter:B:28:0x0096, B:63:0x00f8=Splitter:B:63:0x00f8} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.immomo.momo.util.v a(java.lang.String r17, com.immomo.momo.android.activity.fs r18) {
        /*
            r11 = 0
            r9 = 0
            com.immomo.momo.util.m r1 = com.immomo.momo.protocol.a.p.h     // Catch:{ InterruptedIOException -> 0x0143, SSLHandshakeException -> 0x0115, SSLException -> 0x0121, Exception -> 0x0132 }
            r0 = r17
            r1.a(r0)     // Catch:{ InterruptedIOException -> 0x0143, SSLHandshakeException -> 0x0115, SSLException -> 0x0121, Exception -> 0x0132 }
            if (r18 == 0) goto L_0x0018
            r2 = 4
            r3 = -1
            r5 = -1
            r7 = -1
            r1 = r18
            r1.a(r2, r3, r5, r7)     // Catch:{ InterruptedIOException -> 0x0143, SSLHandshakeException -> 0x0115, SSLException -> 0x0121, Exception -> 0x0132 }
        L_0x0018:
            r1 = 0
            r2 = 0
            r0 = r17
            java.net.HttpURLConnection r11 = com.immomo.momo.protocol.a.q.b(r0, r1, r2)     // Catch:{ InterruptedIOException -> 0x0143, SSLHandshakeException -> 0x0115, SSLException -> 0x0121, Exception -> 0x0132 }
            r1 = 60000(0xea60, float:8.4078E-41)
            r11.setReadTimeout(r1)     // Catch:{ InterruptedIOException -> 0x00b5, SSLHandshakeException -> 0x0115, SSLException -> 0x0121, Exception -> 0x0132 }
            java.io.BufferedInputStream r12 = new java.io.BufferedInputStream     // Catch:{ InterruptedIOException -> 0x00b5, SSLHandshakeException -> 0x0115, SSLException -> 0x0121, Exception -> 0x0132 }
            java.io.InputStream r1 = r11.getInputStream()     // Catch:{ InterruptedIOException -> 0x00b5, SSLHandshakeException -> 0x0115, SSLException -> 0x0121, Exception -> 0x0132 }
            r2 = 2048(0x800, float:2.87E-42)
            r12.<init>(r1, r2)     // Catch:{ InterruptedIOException -> 0x00b5, SSLHandshakeException -> 0x0115, SSLException -> 0x0121, Exception -> 0x0132 }
            int r1 = r11.getContentLength()     // Catch:{ InterruptedIOException -> 0x00b5, SSLHandshakeException -> 0x0115, SSLException -> 0x0121, Exception -> 0x0132 }
            long r13 = (long) r1     // Catch:{ InterruptedIOException -> 0x00b5, SSLHandshakeException -> 0x0115, SSLException -> 0x0121, Exception -> 0x0132 }
            r1 = 1
            int r1 = (r13 > r1 ? 1 : (r13 == r1 ? 0 : -1))
            if (r1 >= 0) goto L_0x00a7
            if (r18 == 0) goto L_0x004a
            r2 = -1
            r3 = -1
            r5 = -1
            r7 = -1
            r1 = r18
            r1.a(r2, r3, r5, r7)     // Catch:{ InterruptedIOException -> 0x00b5, SSLHandshakeException -> 0x0115, SSLException -> 0x0121, Exception -> 0x0132 }
        L_0x004a:
            java.io.ByteArrayOutputStream r15 = new java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x014a, all -> 0x0147 }
            r1 = 8192(0x2000, float:1.14794E-41)
            r15.<init>(r1)     // Catch:{ Exception -> 0x014a, all -> 0x0147 }
            r1 = 2048(0x800, float:2.87E-42)
            byte[] r0 = new byte[r1]     // Catch:{ Exception -> 0x014a, all -> 0x0147 }
            r16 = r0
            r5 = r9
        L_0x0058:
            r0 = r16
            int r1 = r12.read(r0)     // Catch:{ Exception -> 0x00e7 }
            r2 = -1
            if (r1 != r2) goto L_0x00d0
            if (r18 == 0) goto L_0x006c
            r2 = 2
            r7 = -1
            r1 = r18
            r3 = r13
            r1.a(r2, r3, r5, r7)     // Catch:{ Exception -> 0x00e7 }
        L_0x006c:
            byte[] r1 = r15.toByteArray()     // Catch:{ Exception -> 0x00e7 }
            com.immomo.momo.protocol.a.q.b(r11)     // Catch:{ Exception -> 0x00e7 }
            if (r1 == 0) goto L_0x0100
            int r2 = r1.length     // Catch:{ Exception -> 0x00e7 }
            if (r2 <= 0) goto L_0x0100
            com.immomo.momo.util.v r9 = new com.immomo.momo.util.v     // Catch:{ Exception -> 0x00e7 }
            r9.<init>()     // Catch:{ Exception -> 0x00e7 }
            r2 = 0
            int r3 = r1.length     // Catch:{ Exception -> 0x00e7 }
            android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeByteArray(r1, r2, r3)     // Catch:{ Exception -> 0x00e7 }
            r9.b = r1     // Catch:{ Exception -> 0x00e7 }
            java.lang.String r1 = r11.getContentType()     // Catch:{ Exception -> 0x00e7 }
            r9.f3099a = r1     // Catch:{ Exception -> 0x00e7 }
            if (r18 == 0) goto L_0x0096
            r2 = 3
            r7 = -1
            r1 = r18
            r3 = r13
            r1.a(r2, r3, r5, r7)     // Catch:{ Exception -> 0x00e7 }
        L_0x0096:
            android.support.v4.b.a.a(r12)     // Catch:{ InterruptedIOException -> 0x00fc, SSLHandshakeException -> 0x0140, SSLException -> 0x013d, Exception -> 0x013a, all -> 0x0137 }
            r1 = 0
            int r1 = (r5 > r1 ? 1 : (r5 == r1 ? 0 : -1))
            if (r1 <= 0) goto L_0x00a2
            com.immomo.momo.util.ar.b(r5)
        L_0x00a2:
            r11.disconnect()
            r1 = r9
        L_0x00a6:
            return r1
        L_0x00a7:
            if (r18 == 0) goto L_0x004a
            r2 = 0
            r5 = 0
            r7 = -1
            r1 = r18
            r3 = r13
            r1.a(r2, r3, r5, r7)     // Catch:{ InterruptedIOException -> 0x00b5, SSLHandshakeException -> 0x0115, SSLException -> 0x0121, Exception -> 0x0132 }
            goto L_0x004a
        L_0x00b5:
            r1 = move-exception
            r1 = r11
        L_0x00b7:
            com.immomo.momo.protocol.a.q.a(r1)     // Catch:{ all -> 0x00c0 }
            com.immomo.momo.a.s r2 = new com.immomo.momo.a.s     // Catch:{ all -> 0x00c0 }
            r2.<init>()     // Catch:{ all -> 0x00c0 }
            throw r2     // Catch:{ all -> 0x00c0 }
        L_0x00c0:
            r2 = move-exception
            r11 = r1
            r1 = r2
        L_0x00c3:
            r2 = 0
            int r2 = (r9 > r2 ? 1 : (r9 == r2 ? 0 : -1))
            if (r2 <= 0) goto L_0x00cc
            com.immomo.momo.util.ar.b(r9)
        L_0x00cc:
            r11.disconnect()
            throw r1
        L_0x00d0:
            r2 = 0
            r0 = r16
            r15.write(r0, r2, r1)     // Catch:{ Exception -> 0x00e7 }
            long r1 = (long) r1     // Catch:{ Exception -> 0x00e7 }
            long r5 = r5 + r1
            if (r18 == 0) goto L_0x0058
            r2 = 1
            long r7 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x00e7 }
            r1 = r18
            r3 = r13
            r1.a(r2, r3, r5, r7)     // Catch:{ Exception -> 0x00e7 }
            goto L_0x0058
        L_0x00e7:
            r1 = move-exception
            r9 = r1
        L_0x00e9:
            if (r18 == 0) goto L_0x00f6
            r2 = -1
            long r7 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x00f7 }
            r1 = r18
            r3 = r13
            r1.a(r2, r3, r5, r7)     // Catch:{ all -> 0x00f7 }
        L_0x00f6:
            throw r9     // Catch:{ all -> 0x00f7 }
        L_0x00f7:
            r1 = move-exception
        L_0x00f8:
            android.support.v4.b.a.a(r12)     // Catch:{ InterruptedIOException -> 0x00fc, SSLHandshakeException -> 0x0140, SSLException -> 0x013d, Exception -> 0x013a, all -> 0x0137 }
            throw r1     // Catch:{ InterruptedIOException -> 0x00fc, SSLHandshakeException -> 0x0140, SSLException -> 0x013d, Exception -> 0x013a, all -> 0x0137 }
        L_0x00fc:
            r1 = move-exception
            r9 = r5
            r1 = r11
            goto L_0x00b7
        L_0x0100:
            android.support.v4.b.a.a(r12)     // Catch:{ InterruptedIOException -> 0x00fc, SSLHandshakeException -> 0x0140, SSLException -> 0x013d, Exception -> 0x013a, all -> 0x0137 }
            r1 = 0
            int r1 = (r5 > r1 ? 1 : (r5 == r1 ? 0 : -1))
            if (r1 <= 0) goto L_0x010c
            com.immomo.momo.util.ar.b(r5)
        L_0x010c:
            r11.disconnect()
            com.immomo.momo.util.v r1 = new com.immomo.momo.util.v
            r1.<init>()
            goto L_0x00a6
        L_0x0115:
            r1 = move-exception
        L_0x0116:
            com.immomo.momo.protocol.a.q.a(r11)     // Catch:{ all -> 0x011f }
            com.immomo.momo.a.t r2 = new com.immomo.momo.a.t     // Catch:{ all -> 0x011f }
            r2.<init>(r1)     // Catch:{ all -> 0x011f }
            throw r2     // Catch:{ all -> 0x011f }
        L_0x011f:
            r1 = move-exception
            goto L_0x00c3
        L_0x0121:
            r1 = move-exception
        L_0x0122:
            com.immomo.momo.protocol.a.q.a(r11)     // Catch:{ all -> 0x011f }
            com.immomo.momo.a.t r2 = new com.immomo.momo.a.t     // Catch:{ all -> 0x011f }
            r3 = 2131493065(0x7f0c00c9, float:1.86096E38)
            java.lang.String r3 = com.immomo.momo.g.a(r3)     // Catch:{ all -> 0x011f }
            r2.<init>(r1, r3)     // Catch:{ all -> 0x011f }
            throw r2     // Catch:{ all -> 0x011f }
        L_0x0132:
            r1 = move-exception
        L_0x0133:
            com.immomo.momo.protocol.a.q.a(r11)     // Catch:{ all -> 0x011f }
            throw r1     // Catch:{ all -> 0x011f }
        L_0x0137:
            r1 = move-exception
            r9 = r5
            goto L_0x00c3
        L_0x013a:
            r1 = move-exception
            r9 = r5
            goto L_0x0133
        L_0x013d:
            r1 = move-exception
            r9 = r5
            goto L_0x0122
        L_0x0140:
            r1 = move-exception
            r9 = r5
            goto L_0x0116
        L_0x0143:
            r1 = move-exception
            r1 = r11
            goto L_0x00b7
        L_0x0147:
            r1 = move-exception
            r5 = r9
            goto L_0x00f8
        L_0x014a:
            r1 = move-exception
            r5 = r9
            r9 = r1
            goto L_0x00e9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.immomo.momo.protocol.a.p.a(java.lang.String, com.immomo.momo.android.activity.fs):com.immomo.momo.util.v");
    }

    public static Date a(long j) {
        if (j > 0) {
            try {
                return new Date(1000 * j);
            } catch (Exception e2) {
            }
        }
        return null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:111:0x0162, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:112:0x0163, code lost:
        r8 = r1;
        r4 = r11;
        r10 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:115:0x016b, code lost:
        r1 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:120:0x0176, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:121:0x0177, code lost:
        r8 = r1;
        r4 = r11;
        r10 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:129:0x018c, code lost:
        r4 = r11;
        r8 = r7;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:102:0x0149 A[Catch:{ all -> 0x0127 }] */
    /* JADX WARNING: Removed duplicated region for block: B:104:0x014e A[Catch:{ all -> 0x0127 }] */
    /* JADX WARNING: Removed duplicated region for block: B:115:0x016b A[ExcHandler: SSLException (e javax.net.ssl.SSLException), PHI: r11 
      PHI: (r11v2 long) = (r11v0 long), (r11v0 long), (r11v10 long), (r11v10 long), (r11v0 long), (r11v0 long) binds: [B:7:0x0033, B:8:?, B:40:0x00a2, B:41:?, B:22:0x007c, B:12:0x0058] A[DONT_GENERATE, DONT_INLINE], Splitter:B:7:0x0033] */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x00e0  */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x00f0 A[Catch:{ all -> 0x00f9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x0101  */
    /* JADX WARNING: Removed duplicated region for block: B:80:0x010e  */
    /* JADX WARNING: Removed duplicated region for block: B:85:0x011b A[Catch:{ all -> 0x0127 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void a(java.lang.String r17, java.io.File r18, com.immomo.momo.android.c.w r19) {
        /*
            com.immomo.momo.util.m r1 = com.immomo.momo.protocol.a.p.h
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "save->"
            r2.<init>(r3)
            r0 = r17
            java.lang.StringBuilder r2 = r2.append(r0)
            java.lang.String r2 = r2.toString()
            r1.a(r2)
            r10 = 0
            r11 = 0
            r8 = 0
            if (r19 == 0) goto L_0x0028
            r2 = 0
            r4 = 0
            r6 = 1
            r7 = 0
            r1 = r19
            r1.a(r2, r4, r6, r7)     // Catch:{ InterruptedIOException -> 0x017f, SSLHandshakeException -> 0x0108, SSLException -> 0x012a, Exception -> 0x013c, all -> 0x0156 }
        L_0x0028:
            r1 = 0
            r2 = 0
            r0 = r17
            java.net.HttpURLConnection r7 = com.immomo.momo.protocol.a.q.b(r0, r1, r2)     // Catch:{ InterruptedIOException -> 0x017f, SSLHandshakeException -> 0x0108, SSLException -> 0x012a, Exception -> 0x013c, all -> 0x0156 }
            r1 = 60000(0xea60, float:8.4078E-41)
            r7.setReadTimeout(r1)     // Catch:{ InterruptedIOException -> 0x0185, SSLHandshakeException -> 0x0170, SSLException -> 0x016b, Exception -> 0x015c }
            java.io.BufferedInputStream r15 = new java.io.BufferedInputStream     // Catch:{ InterruptedIOException -> 0x0185, SSLHandshakeException -> 0x0170, SSLException -> 0x016b, Exception -> 0x015c }
            java.io.InputStream r1 = r7.getInputStream()     // Catch:{ InterruptedIOException -> 0x0185, SSLHandshakeException -> 0x0170, SSLException -> 0x016b, Exception -> 0x015c }
            r2 = 2048(0x800, float:2.87E-42)
            r15.<init>(r1, r2)     // Catch:{ InterruptedIOException -> 0x0185, SSLHandshakeException -> 0x0170, SSLException -> 0x016b, Exception -> 0x015c }
            java.io.BufferedOutputStream r16 = new java.io.BufferedOutputStream     // Catch:{ InterruptedIOException -> 0x0185, SSLHandshakeException -> 0x0170, SSLException -> 0x016b, Exception -> 0x015c }
            java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ InterruptedIOException -> 0x0185, SSLHandshakeException -> 0x0170, SSLException -> 0x016b, Exception -> 0x015c }
            r0 = r18
            r1.<init>(r0)     // Catch:{ InterruptedIOException -> 0x0185, SSLHandshakeException -> 0x0170, SSLException -> 0x016b, Exception -> 0x015c }
            r0 = r16
            r0.<init>(r1)     // Catch:{ InterruptedIOException -> 0x0185, SSLHandshakeException -> 0x0170, SSLException -> 0x016b, Exception -> 0x015c }
            int r1 = r7.getContentLength()     // Catch:{ InterruptedIOException -> 0x0185, SSLHandshakeException -> 0x0170, SSLException -> 0x016b, Exception -> 0x015c }
            long r2 = (long) r1
            if (r19 == 0) goto L_0x0075
            r0 = r19
            boolean r1 = r0.e     // Catch:{ InterruptedIOException -> 0x018b, SSLHandshakeException -> 0x0176, SSLException -> 0x016b, Exception -> 0x0162 }
            if (r1 == 0) goto L_0x0075
            r4 = 0
            r6 = 5
            r1 = r19
            r1.a(r2, r4, r6, r7)     // Catch:{ InterruptedIOException -> 0x018b, SSLHandshakeException -> 0x0176, SSLException -> 0x016b, Exception -> 0x0162 }
            r1 = 0
            r3 = 0
            int r1 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r1 <= 0) goto L_0x0071
            r1 = 0
            com.immomo.momo.util.ar.b(r1)
        L_0x0071:
            r7.disconnect()
        L_0x0074:
            return
        L_0x0075:
            if (r19 == 0) goto L_0x007f
            r4 = 0
            r6 = 3
            r1 = r19
            r1.a(r2, r4, r6, r7)     // Catch:{ InterruptedIOException -> 0x018b, SSLHandshakeException -> 0x0176, SSLException -> 0x016b, Exception -> 0x0162 }
        L_0x007f:
            com.immomo.momo.protocol.a.q.b(r7)     // Catch:{ InterruptedIOException -> 0x018b, SSLHandshakeException -> 0x0176, SSLException -> 0x016b, Exception -> 0x0162 }
            r1 = 2048(0x800, float:2.87E-42)
            byte[] r8 = new byte[r1]     // Catch:{ all -> 0x00d3 }
            r4 = r11
        L_0x0087:
            int r1 = r15.read(r8)     // Catch:{ all -> 0x0190 }
            r6 = -1
            if (r1 != r6) goto L_0x00b5
            r11 = r4
        L_0x008f:
            r16.flush()     // Catch:{ all -> 0x00d3 }
            if (r19 == 0) goto L_0x00a2
            r0 = r19
            boolean r1 = r0.e     // Catch:{ all -> 0x00d3 }
            if (r1 != 0) goto L_0x00a2
            r13 = 4
            r14 = 0
            r8 = r19
            r9 = r2
            r8.a(r9, r11, r13, r14)     // Catch:{ all -> 0x00d3 }
        L_0x00a2:
            android.support.v4.b.a.a(r15)     // Catch:{ InterruptedIOException -> 0x018b, SSLHandshakeException -> 0x0176, SSLException -> 0x016b, Exception -> 0x0162 }
            android.support.v4.b.a.a(r16)     // Catch:{ InterruptedIOException -> 0x018b, SSLHandshakeException -> 0x0176, SSLException -> 0x016b, Exception -> 0x0162 }
            r1 = 0
            int r1 = (r11 > r1 ? 1 : (r11 == r1 ? 0 : -1))
            if (r1 <= 0) goto L_0x00b1
            com.immomo.momo.util.ar.b(r11)
        L_0x00b1:
            r7.disconnect()
            goto L_0x0074
        L_0x00b5:
            r6 = 0
            r0 = r16
            r0.write(r8, r6, r1)     // Catch:{ all -> 0x0190 }
            long r9 = (long) r1     // Catch:{ all -> 0x0190 }
            long r4 = r4 + r9
            if (r19 == 0) goto L_0x0087
            r6 = 3
            r1 = r19
            r1.a(r2, r4, r6, r7)     // Catch:{ all -> 0x0190 }
            r0 = r19
            boolean r1 = r0.e     // Catch:{ all -> 0x0190 }
            if (r1 == 0) goto L_0x0087
            r6 = 5
            r1 = r19
            r1.a(r2, r4, r6, r7)     // Catch:{ all -> 0x0190 }
            r11 = r4
            goto L_0x008f
        L_0x00d3:
            r1 = move-exception
            r4 = r11
        L_0x00d5:
            android.support.v4.b.a.a(r15)     // Catch:{ InterruptedIOException -> 0x00dc, SSLHandshakeException -> 0x017b, SSLException -> 0x016d, Exception -> 0x0167, all -> 0x015a }
            android.support.v4.b.a.a(r16)     // Catch:{ InterruptedIOException -> 0x00dc, SSLHandshakeException -> 0x017b, SSLException -> 0x016d, Exception -> 0x0167, all -> 0x015a }
            throw r1     // Catch:{ InterruptedIOException -> 0x00dc, SSLHandshakeException -> 0x017b, SSLException -> 0x016d, Exception -> 0x0167, all -> 0x015a }
        L_0x00dc:
            r1 = move-exception
            r8 = r7
        L_0x00de:
            if (r19 == 0) goto L_0x00e7
            r6 = 2
            r7 = 0
            r1 = r19
            r1.a(r2, r4, r6, r7)     // Catch:{ all -> 0x00f9 }
        L_0x00e7:
            com.immomo.momo.protocol.a.q.a(r8)     // Catch:{ all -> 0x00f9 }
            boolean r1 = r18.exists()     // Catch:{ all -> 0x00f9 }
            if (r1 == 0) goto L_0x00f3
            r18.delete()     // Catch:{ all -> 0x00f9 }
        L_0x00f3:
            com.immomo.momo.a.s r1 = new com.immomo.momo.a.s     // Catch:{ all -> 0x00f9 }
            r1.<init>()     // Catch:{ all -> 0x00f9 }
            throw r1     // Catch:{ all -> 0x00f9 }
        L_0x00f9:
            r1 = move-exception
            r7 = r8
        L_0x00fb:
            r2 = 0
            int r2 = (r4 > r2 ? 1 : (r4 == r2 ? 0 : -1))
            if (r2 <= 0) goto L_0x0104
            com.immomo.momo.util.ar.b(r4)
        L_0x0104:
            r7.disconnect()
            throw r1
        L_0x0108:
            r1 = move-exception
            r2 = r8
            r4 = r11
            r8 = r1
        L_0x010c:
            if (r19 == 0) goto L_0x0115
            r6 = 2
            r7 = 0
            r1 = r19
            r1.a(r2, r4, r6, r7)     // Catch:{ all -> 0x0127 }
        L_0x0115:
            boolean r1 = r18.exists()     // Catch:{ all -> 0x0127 }
            if (r1 == 0) goto L_0x011e
            r18.delete()     // Catch:{ all -> 0x0127 }
        L_0x011e:
            com.immomo.momo.protocol.a.q.a(r10)     // Catch:{ all -> 0x0127 }
            com.immomo.momo.a.t r1 = new com.immomo.momo.a.t     // Catch:{ all -> 0x0127 }
            r1.<init>(r8)     // Catch:{ all -> 0x0127 }
            throw r1     // Catch:{ all -> 0x0127 }
        L_0x0127:
            r1 = move-exception
            r7 = r10
            goto L_0x00fb
        L_0x012a:
            r1 = move-exception
            r7 = r10
        L_0x012c:
            com.immomo.momo.a.t r2 = new com.immomo.momo.a.t     // Catch:{ all -> 0x0139 }
            r3 = 2131493065(0x7f0c00c9, float:1.86096E38)
            java.lang.String r3 = com.immomo.momo.g.a(r3)     // Catch:{ all -> 0x0139 }
            r2.<init>(r1, r3)     // Catch:{ all -> 0x0139 }
            throw r2     // Catch:{ all -> 0x0139 }
        L_0x0139:
            r1 = move-exception
            r4 = r11
            goto L_0x00fb
        L_0x013c:
            r1 = move-exception
            r2 = r8
            r4 = r11
            r8 = r1
        L_0x0140:
            com.immomo.momo.protocol.a.q.a(r10)     // Catch:{ all -> 0x0127 }
            boolean r1 = r18.exists()     // Catch:{ all -> 0x0127 }
            if (r1 == 0) goto L_0x014c
            r18.delete()     // Catch:{ all -> 0x0127 }
        L_0x014c:
            if (r19 == 0) goto L_0x0155
            r6 = 2
            r7 = 0
            r1 = r19
            r1.a(r2, r4, r6, r7)     // Catch:{ all -> 0x0127 }
        L_0x0155:
            throw r8     // Catch:{ all -> 0x0127 }
        L_0x0156:
            r1 = move-exception
            r4 = r11
            r7 = r10
            goto L_0x00fb
        L_0x015a:
            r1 = move-exception
            goto L_0x00fb
        L_0x015c:
            r1 = move-exception
            r2 = r8
            r4 = r11
            r10 = r7
            r8 = r1
            goto L_0x0140
        L_0x0162:
            r1 = move-exception
            r8 = r1
            r4 = r11
            r10 = r7
            goto L_0x0140
        L_0x0167:
            r1 = move-exception
            r8 = r1
            r10 = r7
            goto L_0x0140
        L_0x016b:
            r1 = move-exception
            goto L_0x012c
        L_0x016d:
            r1 = move-exception
            r11 = r4
            goto L_0x012c
        L_0x0170:
            r1 = move-exception
            r2 = r8
            r4 = r11
            r10 = r7
            r8 = r1
            goto L_0x010c
        L_0x0176:
            r1 = move-exception
            r8 = r1
            r4 = r11
            r10 = r7
            goto L_0x010c
        L_0x017b:
            r1 = move-exception
            r8 = r1
            r10 = r7
            goto L_0x010c
        L_0x017f:
            r1 = move-exception
            r2 = r8
            r4 = r11
            r8 = r10
            goto L_0x00de
        L_0x0185:
            r1 = move-exception
            r2 = r8
            r4 = r11
            r8 = r7
            goto L_0x00de
        L_0x018b:
            r1 = move-exception
            r4 = r11
            r8 = r7
            goto L_0x00de
        L_0x0190:
            r1 = move-exception
            goto L_0x00d5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.immomo.momo.protocol.a.p.a(java.lang.String, java.io.File, com.immomo.momo.android.c.w):void");
    }

    public static String[] a(JSONArray jSONArray) {
        if (jSONArray == null) {
            return null;
        }
        String[] strArr = new String[jSONArray.length()];
        for (int i = 0; i < jSONArray.length(); i++) {
            strArr[i] = jSONArray.optString(i);
        }
        return strArr;
    }

    protected static void k(String str) {
        JSONObject jSONObject = new JSONObject(str);
        int i = 0;
        String str2 = PoiTypeDef.All;
        if (jSONObject.has("ok")) {
            if (!jSONObject.getBoolean("ok")) {
                i = jSONObject.optInt("errcode");
                str2 = jSONObject.optString("errmsg", PoiTypeDef.All);
            } else {
                return;
            }
        }
        if (jSONObject.has("ec")) {
            i = jSONObject.optInt("ec");
            str2 = jSONObject.optString("em", PoiTypeDef.All);
            if (i == 0) {
                return;
            }
        }
        switch (i) {
            case PurchaseCode.BILL_DYMARK_CREATE_ERROR /*400*/:
                throw new e(str2);
            case PurchaseCode.BILL_CANCEL_FAIL /*401*/:
                throw new f(str2);
            case PurchaseCode.BILL_CHECKCODE_ERROR /*403*/:
                throw new g();
            case PurchaseCode.BILL_PW_FAIL /*404*/:
                throw new com.immomo.momo.a.h(str2);
            case PurchaseCode.BILL_INVALID_SESSION /*405*/:
                throw new l(str2);
            case PurchaseCode.BILL_CSSP_BUSY /*406*/:
                throw new n(str2);
            case PurchaseCode.BILL_INVALID_APP /*409*/:
                throw new o(str2);
            case PurchaseCode.BILL_LICENSE_ERROR /*410*/:
                throw new com.immomo.momo.a.p(str2);
            case PurchaseCode.QUERY_FROZEN /*500*/:
                throw new q(str2);
            case 20403:
                throw new b(str2);
            case 20405:
                throw new c(str2);
            case 20406:
                throw new d(str2);
            case 40403:
                throw new i(str2);
            case 40406:
                throw new j(str2);
            case 40408:
                throw new k(str2);
            case 40500:
                throw new com.immomo.momo.a.m(str2);
            default:
                throw new com.immomo.momo.a.a(str2, i);
        }
    }

    protected static String l(String str) {
        if (str == null || !str.startsWith("[\"") || !str.endsWith("\"]")) {
            return null;
        }
        return str.substring(2, str.length() - 2);
    }

    public final String a(String str, Map map) {
        return a(str, map, null, null);
    }

    /* access modifiers changed from: protected */
    public final String a(String str, Map map, Map map2) {
        if (map != null) {
            this.e.a((Object) ("[urlString] params : " + map));
        }
        if (map2 != null) {
            this.e.a((Object) ("[urlString] params : " + map2));
        }
        try {
            if (com.immomo.momo.g.q() != null) {
                str = android.support.v4.b.a.a(str, "fr", com.immomo.momo.g.q().h);
            }
            String str2 = new String(q.a(str, map, map2));
            this.e.a((Object) ("[urlString] Result : " + str2));
            k(str2);
            return str2;
        } catch (InterruptedIOException e2) {
            throw new s();
        } catch (SSLHandshakeException e3) {
            throw new t(e3);
        } catch (SSLException e4) {
            throw new t(e4, com.immomo.momo.g.a((int) R.string.errormsg_ssl));
        }
    }

    /* access modifiers changed from: protected */
    public final String a(String str, Map map, l[] lVarArr) {
        return a(str, map, lVarArr, null);
    }

    /* access modifiers changed from: protected */
    public final String a(String str, Map map, l[] lVarArr, Map map2) {
        if (map != null) {
            this.e.a((Object) ("[urlString] params : " + map));
        }
        try {
            if (com.immomo.momo.g.q() != null) {
                str = android.support.v4.b.a.a(str, "fr", com.immomo.momo.g.q().h);
            }
            String str2 = new String(q.a(str, map, lVarArr, map2));
            this.e.a((Object) ("[urlString] Result : " + str2));
            k(str2);
            return str2;
        } catch (InterruptedIOException e2) {
            throw new s();
        } catch (SSLHandshakeException e3) {
            throw new t(e3);
        } catch (CertificateException e4) {
            throw new t(e4);
        } catch (SSLException e5) {
            throw new t(e5, com.immomo.momo.g.a((int) R.string.errormsg_ssl));
        } catch (Exception e6) {
            throw new com.immomo.momo.a.a(com.immomo.momo.g.a((int) R.string.errormsg_server), e6);
        }
    }

    public final String b(String str, Map map) {
        return a(str, map, (Map) null);
    }
}
