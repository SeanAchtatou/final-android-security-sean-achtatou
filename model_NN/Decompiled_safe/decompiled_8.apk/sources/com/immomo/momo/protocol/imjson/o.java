package com.immomo.momo.protocol.imjson;

public final class o {

    /* renamed from: a  reason: collision with root package name */
    public String f2927a;
    public int b;

    public o() {
    }

    public o(String str, int i) {
        this.f2927a = str;
        this.b = i;
    }

    public final String toString() {
        return String.valueOf(this.f2927a) + ":" + this.b;
    }
}
