package com.immomo.momo.protocol.imjson;

import com.immomo.momo.android.c.u;
import com.immomo.momo.protocol.imjson.task.SendTask;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;

public final class q extends s {

    /* renamed from: a  reason: collision with root package name */
    private ThreadPoolExecutor f2929a;
    /* access modifiers changed from: private */
    public /* synthetic */ p b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public q(p pVar, BlockingQueue blockingQueue) {
        super(pVar, blockingQueue);
        this.b = pVar;
        this.f2929a = null;
        this.f2929a = new u(5, 15);
    }

    /* access modifiers changed from: protected */
    public final void a(SendTask sendTask) {
        this.f2929a.execute(new r(this, sendTask));
    }
}
