package com.immomo.momo.protocol.imjson.util;

import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.a.a.a.b;
import com.immomo.a.a.a.d;
import com.immomo.momo.util.m;
import com.immomo.momo.util.n;

public final class a extends d {
    private static /* synthetic */ int[] b;

    /* renamed from: a  reason: collision with root package name */
    private m f2949a;

    public a() {
        this(PoiTypeDef.All);
        this.f2949a = new m(PoiTypeDef.All);
    }

    public a(String str) {
        super(str);
        this.f2949a = null;
        this.f2949a = new m(str);
        a(str);
        b(this.f2949a.c());
    }

    private static /* synthetic */ int[] b() {
        int[] iArr = b;
        if (iArr == null) {
            iArr = new int[b.values().length];
            try {
                iArr[b.DEBUG.ordinal()] = 2;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[b.ERROR.ordinal()] = 3;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[b.INFO.ordinal()] = 1;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[b.VERBOSE.ordinal()] = 5;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[b.WARNING.ordinal()] = 4;
            } catch (NoSuchFieldError e5) {
            }
            b = iArr;
        }
        return iArr;
    }

    public final void a(String str) {
        super.a("IMJSON_" + str);
        this.f2949a.a("IMJSON_" + str);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.m.a(java.lang.Appendable, java.lang.Throwable):void
     arg types: [java.lang.StringBuilder, java.lang.Throwable]
     candidates:
      com.immomo.momo.util.m.a(java.lang.Object, java.lang.Throwable):void
      com.immomo.momo.util.m.a(java.lang.String, java.io.File):void
      com.immomo.momo.util.m.a(java.lang.String, java.lang.Throwable):void
      com.immomo.momo.util.m.a(java.lang.Appendable, java.lang.Throwable):void */
    public final void a(String str, Throwable th, b bVar) {
        n nVar = n.LOG_INFO;
        switch (b()[bVar.ordinal()]) {
            case 2:
                nVar = n.LOG_DEBUG;
                break;
            case 5:
                n nVar2 = n.LOG_VERBOSE;
            case 4:
                n nVar3 = n.LOG_WARNING;
            case 3:
                nVar = n.LOG_ERROR;
                break;
        }
        this.f2949a.a(str, th, nVar);
        if (!c.b()) {
            return;
        }
        if (bVar == b.ERROR) {
            StringBuilder sb = new StringBuilder();
            m.a((Appendable) sb, th);
            c.a().b(sb.toString());
            return;
        }
        if (str.startsWith(a())) {
            str = str.substring(a().length() - 1, str.length());
        }
        c.a().b(str);
    }
}
