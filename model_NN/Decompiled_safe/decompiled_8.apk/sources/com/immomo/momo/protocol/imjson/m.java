package com.immomo.momo.protocol.imjson;

import com.immomo.a.a.e.b;
import com.immomo.momo.g;

public final class m extends b {
    public final int a() {
        if (!g.ab()) {
            return g.aa() ? 1 : 0;
        }
        if (g.Z() == 2 || g.Z() == 4) {
            return 3;
        }
        return (g.Z() == 7 || g.Z() == 11 || g.Z() == 1) ? 4 : 2;
    }
}
