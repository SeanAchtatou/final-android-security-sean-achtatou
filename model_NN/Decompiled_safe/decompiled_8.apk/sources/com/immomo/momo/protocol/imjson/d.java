package com.immomo.momo.protocol.imjson;

import com.immomo.a.a.d.b;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.util.m;
import java.util.concurrent.atomic.AtomicBoolean;
import org.json.JSONException;

public final class d {

    /* renamed from: a  reason: collision with root package name */
    private static m f2918a = new m("IMJApi");

    public static String a(String str) {
        b bVar = new b();
        bVar.put("ns", "relation");
        bVar.a("get");
        bVar.put("remoteid", str);
        e.a();
        b a2 = e.a(bVar);
        f2918a.a((Object) ("getRelation reuslt=" + a2));
        return a2.optString("relation");
    }

    public static void a() {
        try {
            b bVar = new b();
            bVar.a("set");
            bVar.put("ns", "friendfeedcount");
            bVar.put("count", 0);
            e.a();
            e.b(bVar);
        } catch (JSONException e) {
        }
    }

    public static void a(double d, double d2, double d3, int i, int i2) {
        b bVar = new b();
        bVar.put("ns", "loc");
        bVar.a("set");
        bVar.put("lat", d);
        bVar.put("lng", d2);
        bVar.put("acc", d3);
        bVar.put("loctype", i);
        bVar.put("locater", i2);
        e.a();
        e.a(bVar);
    }

    public static void a(bf bfVar) {
        boolean z = true;
        b bVar = new b();
        bVar.a("get");
        bVar.put("ns", "distance");
        bVar.put("remoteid", bfVar.h);
        e.a();
        b a2 = e.a(bVar);
        if (a2.optInt("deviation", 0) != 1) {
            z = false;
        }
        bfVar.V = z;
        bfVar.a((float) a2.optInt("distance", -1));
        bfVar.a(a2.optLong("dt"));
    }

    public static boolean a(StringBuilder sb, AtomicBoolean atomicBoolean) {
        if (atomicBoolean != null) {
            atomicBoolean.set(l.c);
        }
        if (sb != null) {
            sb.append(l.b);
        }
        return l.f2926a || !l.h;
    }

    public static void b() {
        try {
            b bVar = new b();
            bVar.a("set");
            bVar.put("ns", "mytiebareportcount");
            bVar.put("count", 0);
            e.a();
            e.b(bVar);
        } catch (JSONException e) {
        }
    }

    public static boolean b(String str) {
        b bVar = new b();
        bVar.a("get");
        bVar.put("ns", "grouprole");
        bVar.put("gid", str);
        e.a();
        return e.a(bVar).optInt("grouprole") != 0;
    }

    public static void c() {
        try {
            b bVar = new b();
            bVar.a("set");
            bVar.put("ns", "eventfeedcount");
            bVar.put("count", 0);
            e.a();
            e.b(bVar);
        } catch (JSONException e) {
        }
    }

    public static boolean c(String str) {
        b bVar = new b();
        bVar.a("get");
        bVar.put("ns", "discussrole");
        bVar.put("did", str);
        e.a();
        return e.a(bVar).optInt("discussrole") != 0;
    }

    public static void d() {
        try {
            b bVar = new b();
            bVar.a("set");
            bVar.put("ns", "myeventcommentcount");
            bVar.put("count", 0);
            e.a();
            e.b(bVar);
        } catch (JSONException e) {
        }
    }

    public static void e() {
        try {
            b bVar = new b();
            bVar.a("set");
            bVar.put("ns", "mytiebacommentcount");
            bVar.put("count", 0);
            e.a();
            e.b(bVar);
        } catch (JSONException e) {
        }
    }

    public static void f() {
        try {
            b bVar = new b();
            bVar.a("set");
            bVar.put("ns", "mycommentcount");
            bVar.put("count", 0);
            e.a();
            e.b(bVar);
        } catch (JSONException e) {
        }
    }
}
