package com.immomo.momo.protocol.a;

import android.support.v4.b.a;
import com.amap.mapapi.location.LocationManagerProxy;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.protocol.a.a.l;
import com.immomo.momo.protocol.a.a.q;
import com.immomo.momo.service.ao;
import com.immomo.momo.service.bean.at;
import com.immomo.momo.service.bean.bb;
import com.immomo.momo.service.bean.bc;
import com.immomo.momo.service.bean.c.b;
import com.immomo.momo.service.bean.c.c;
import com.immomo.momo.service.bean.c.d;
import com.immomo.momo.service.bean.c.e;
import com.immomo.momo.service.bean.c.f;
import com.immomo.momo.service.bean.c.g;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

public final class v extends p {
    private static String A = "tieba";
    private static String B = "count";
    private static String C = "sort";
    private static String D = "new_post_count";
    private static String E = "member_count";
    private static String F = "hot";
    private static String G = "post_count";
    private static String H = "create_time";
    private static String I = "recommend";
    private static String J = "desc";
    private static String K = "photos";
    private static String L = "remain";
    private static String M = "my_tiebas";
    private static String N = "recommend_tiebas";
    private static String O = "apply_count";
    private static String P = "my_tieba_count";
    private static String Q = "emotion_library";
    private static String R = "emotion_name";
    private static String S = "emotion_body";
    private static String T = "pics";
    private static String U = "delete_image";
    private static String V = "floor";
    private static String W = "owner";
    private static String X = "new";
    private static String Y = "top";
    private static String Z = "tocommentid";
    private static String aA = "elite";
    private static String aB = "distance";
    private static String aC = "apply_tiebas";
    private static String aD = "idcard";
    private static String aE = "selected";
    private static String aa = "tomomoid";
    private static String ab = "total";
    private static String ac = "cid";
    private static String ad = "comment_count";
    private static String ae = "posts";
    private static String af = "comments";
    private static String ag = "user";
    private static String ah = "reply_content";
    private static String ai = "admins";
    private static String aj = "members";
    private static String ak = "remoteid";
    private static String al = "is_member";
    private static String am = "is_admin";
    private static String an = "reply";
    private static String ao = "momoid";
    private static String ap = "reason";
    private static String aq = "reply_time";
    private static String ar = "applyid";
    private static String as = "cancreate";
    private static String at = "tags";
    private static String au = "top_tiebas";
    private static String av = "self";
    private static String aw = "other";
    private static String ax = "list";
    private static String ay = "tiebas_found";
    private static String az = "tiebas_wait";
    public static String f = "name";
    public static String g = LocationManagerProxy.KEY_STATUS_CHANGED;
    public static String h = "tid";
    public static String i = "content";
    public static String j = "sync_sina";
    public static String k = "sync_renren";
    public static String l = "sync_qqwb";
    public static String m = "sync_feed";
    public static String n = "sync_weixin";
    public static String o = "title";
    public static String p = "pid";
    private static v q = null;
    private static String s = (String.valueOf(b) + "/tieba");
    private static String t = "avatar";
    private static String u = "category";
    private static String v = "tieba_count";
    private static String w = "icon";
    private static String x = "data";
    private static String y = "index";
    private static String z = "tiebas";
    private ao r = new ao();

    public static v a() {
        if (q == null) {
            q = new v();
        }
        return q;
    }

    private static b a(JSONObject jSONObject, b bVar) {
        boolean z2 = true;
        bVar.o = jSONObject.optInt(g);
        bVar.d = jSONObject.optString(o);
        bVar.i = a.a(jSONObject.optLong(H));
        bVar.s = jSONObject.optString(S);
        bVar.r = jSONObject.optString(V);
        bVar.f3017a = jSONObject.optString(p);
        bVar.a(a(jSONObject.optJSONArray(T)) == null ? bVar.c() : a(jSONObject.optJSONArray(T)));
        bVar.g = jSONObject.optString(i);
        bVar.f = jSONObject.optString(W);
        bVar.q = jSONObject.optInt(F) == 1;
        bVar.t = jSONObject.optString(R);
        bVar.u = jSONObject.optString(Q);
        bVar.c = jSONObject.optString(h);
        bVar.l = jSONObject.optInt(X) == 1;
        bVar.n = jSONObject.optInt(ad);
        bVar.m = jSONObject.optInt(Y) == 1;
        bVar.k = jSONObject.optInt(I) == 1;
        bVar.j = a.a(jSONObject.optLong(aq));
        bVar.h = a((float) jSONObject.optInt(aB, -1));
        if (jSONObject.optInt(aA) != 1) {
            z2 = false;
        }
        bVar.p = z2;
        JSONObject optJSONObject = jSONObject.optJSONObject(ag);
        if (optJSONObject != null) {
            if (bVar.e == null) {
                bVar.e = new g();
            }
            w.a(bVar.e, optJSONObject);
        }
        return bVar;
    }

    private static f a(JSONObject jSONObject) {
        boolean z2 = true;
        f fVar = new f();
        fVar.b = jSONObject.optString(f);
        fVar.e = jSONObject.optInt(v);
        fVar.f3021a = jSONObject.optString(u);
        fVar.d = jSONObject.optString(w);
        fVar.c = jSONObject.optString(au);
        if (jSONObject.optInt(aE) != 1) {
            z2 = false;
        }
        fVar.g = z2;
        return fVar;
    }

    private static String a(float f2) {
        return f2 == -2.0f ? com.immomo.momo.g.a((int) R.string.profile_distance_hide) : f2 >= 0.0f ? String.valueOf(a.a(f2 / 1000.0f)) + "km" : PoiTypeDef.All;
    }

    public static String a(List list) {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        Iterator it = list.iterator();
        while (it.hasNext()) {
            c cVar = (c) it.next();
            StringBuilder sb2 = new StringBuilder();
            sb2.append("{");
            sb2.append((CharSequence) g(Z, cVar.p));
            sb2.append((CharSequence) g(ac, cVar.f3018a));
            sb2.append((CharSequence) g(ah, cVar.n));
            sb2.append((CharSequence) g(S, cVar.q));
            sb2.append((CharSequence) g(V, cVar.h));
            sb2.append((CharSequence) g(p, cVar.j));
            sb2.append((CharSequence) g(i, cVar.d));
            sb2.append((CharSequence) g(R, cVar.r));
            sb2.append((CharSequence) g(Q, cVar.s));
            sb2.append((CharSequence) g(W, cVar.c));
            sb2.append((CharSequence) g(h, cVar.l));
            sb2.append((CharSequence) g(ah, cVar.n));
            sb2.append("\"status\":").append(cVar.o).append(",");
            sb2.append("\"create_time\":").append(cVar.g.getTime() / 1000).append(",");
            sb2.append("\"pics\":").append(com.immomo.momo.util.a.a.a((Object[]) cVar.e)).append(",");
            if (cVar.b != null) {
                sb2.append("\"user\":{").append((CharSequence) g(ao, cVar.b.h));
                sb2.append((CharSequence) g(f, cVar.b.i));
                sb2.append((CharSequence) g(t, cVar.b.getLoadImageId()));
                sb2.deleteCharAt(sb2.length() - 1);
                sb2.append("}");
            }
            sb2.append("}");
            sb.append((CharSequence) sb2);
            sb.append(",");
        }
        if (list.size() > 0) {
            sb.deleteCharAt(sb.length() - 1);
        }
        sb.append("]");
        return sb.toString();
    }

    private static List a(JSONArray jSONArray, List list) {
        for (int i2 = 0; i2 < jSONArray.length(); i2++) {
            d dVar = new d();
            JSONObject jSONObject = jSONArray.getJSONObject(i2);
            dVar.b = jSONObject.optString(f);
            dVar.g = jSONObject.optInt(E);
            dVar.f3019a = jSONObject.optString(ar);
            list.add(dVar);
        }
        return list;
    }

    private void a(JSONObject jSONObject, c cVar) {
        cVar.o = jSONObject.optInt(g);
        cVar.p = jSONObject.optString(Z);
        cVar.f3018a = jSONObject.optString(ac);
        cVar.g = a.a(jSONObject.optLong(H));
        cVar.q = jSONObject.optString(S);
        cVar.h = jSONObject.optString(V);
        cVar.j = jSONObject.optString(p);
        cVar.e = a(jSONObject.optJSONArray(T)) == null ? cVar.e : a(jSONObject.optJSONArray(T));
        cVar.d = jSONObject.optString(i);
        cVar.r = jSONObject.optString(R);
        cVar.s = jSONObject.optString(Q);
        cVar.c = jSONObject.optString(W);
        cVar.f = a((float) jSONObject.optInt(aB, -1));
        cVar.b = new g(cVar.c);
        w.a(cVar.b, jSONObject.optJSONObject(ag));
        cVar.n = jSONObject.optString(ah);
        cVar.l = jSONObject.optString(h);
        d b = jSONObject.has(A) ? b(jSONObject.getJSONObject(A)) : this.r.a(cVar.l);
        if (b != null) {
            cVar.m = b.b;
        }
        if (jSONObject.has(an)) {
            cVar.k = new c();
            JSONObject optJSONObject = jSONObject.optJSONObject(an);
            cVar.k.d = optJSONObject.optString(i);
            cVar.k.h = optJSONObject.optString(V);
            if (optJSONObject.has(ag)) {
                cVar.k.b = new g();
                w.a(cVar.k.b, optJSONObject.optJSONObject(ag));
            }
        }
    }

    private static d b(JSONObject jSONObject) {
        boolean z2 = true;
        d dVar = new d();
        dVar.b = jSONObject.optString(f);
        dVar.i = jSONObject.optInt(D);
        dVar.g = jSONObject.optInt(E);
        dVar.n = jSONObject.optInt(F) == 1;
        dVar.h = jSONObject.optInt(G);
        dVar.f = a.a(jSONObject.optLong(H));
        dVar.p = jSONObject.optInt(g);
        dVar.o = jSONObject.optInt(I) == 1;
        dVar.m = jSONObject.optInt(al) == 1;
        if (jSONObject.optInt(am) != 1) {
            z2 = false;
        }
        dVar.l = z2;
        dVar.f3019a = jSONObject.optString(h);
        dVar.j = jSONObject.optString(J);
        dVar.e = a(jSONObject.optJSONArray(K)) == null ? dVar.e : a(jSONObject.optJSONArray(K));
        return dVar;
    }

    private static StringBuilder g(String str, String str2) {
        StringBuilder sb = new StringBuilder();
        sb.append("\"").append(str).append("\":\"").append(str2).append("\"");
        sb.append(",");
        return sb;
    }

    public final q a(int i2) {
        boolean z2 = true;
        HashMap hashMap = new HashMap();
        hashMap.put(y, new StringBuilder(String.valueOf(i2)).toString());
        hashMap.put(B, new StringBuilder(String.valueOf(20)).toString());
        JSONObject jSONObject = new JSONObject(a(String.valueOf(s) + "/recommend", hashMap)).getJSONObject(x);
        q qVar = new q();
        jSONObject.getInt(B);
        jSONObject.getInt(y);
        if (jSONObject.getInt(L) != 1) {
            z2 = false;
        }
        qVar.a(z2);
        JSONArray jSONArray = jSONObject.getJSONArray(z);
        for (int i3 = 0; i3 < jSONArray.length(); i3++) {
            qVar.a(b(jSONArray.getJSONObject(i3)));
        }
        return qVar;
    }

    public final String a(String str, String str2) {
        if (a.a((CharSequence) str)) {
            return null;
        }
        String str3 = String.valueOf(s) + "/report";
        HashMap hashMap = new HashMap();
        hashMap.put(h, str);
        hashMap.put(i, str2);
        return new JSONObject(a(str3, hashMap)).optString("em", PoiTypeDef.All);
    }

    public final String a(String str, String str2, String str3, String str4) {
        String str5 = String.valueOf(s) + "/post/publishcheck";
        HashMap hashMap = new HashMap();
        hashMap.put(o, str);
        hashMap.put(i, str2);
        hashMap.put(h, str3);
        if (!a.a((CharSequence) str4)) {
            hashMap.put(p, str4);
        }
        return new JSONObject(a(str5, hashMap)).getString("em");
    }

    public final String a(String str, String str2, String str3, String str4, File file) {
        String str5 = String.valueOf(s) + "/admin/applyhost";
        HashMap hashMap = new HashMap();
        hashMap.put(h, str);
        hashMap.put(f, str2);
        hashMap.put(aD, str3);
        hashMap.put(ap, str4);
        return new JSONObject(a(str5, hashMap, new l[]{new l(file.getName(), file, "photo", (byte) 0)})).optString("em", PoiTypeDef.All);
    }

    public final String a(String str, String str2, String str3, String str4, String str5) {
        String str6 = String.valueOf(s) + "/user/limit";
        HashMap hashMap = new HashMap();
        hashMap.put(h, str);
        hashMap.put(ap, str5);
        if (str2 != null) {
            hashMap.put(p, str2);
        }
        if (str3 != null) {
            hashMap.put(ac, str3);
        }
        hashMap.put(ak, str4);
        return new JSONObject(a(str6, hashMap)).optString("em", PoiTypeDef.All);
    }

    public final String a(String str, String str2, HashMap hashMap, String str3, com.immomo.momo.plugin.b.a aVar, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6, b bVar, String str4) {
        JSONObject jSONObject;
        String str5 = String.valueOf(s) + "/post/publish";
        HashMap hashMap2 = new HashMap();
        hashMap2.put(o, str);
        hashMap2.put(i, str2);
        hashMap2.put(j, new StringBuilder(String.valueOf(z2 ? 1 : 0)).toString());
        hashMap2.put(k, new StringBuilder(String.valueOf(z3 ? 1 : 0)).toString());
        hashMap2.put(l, new StringBuilder(String.valueOf(z4 ? 1 : 0)).toString());
        hashMap2.put(m, new StringBuilder(String.valueOf(z5 ? 1 : 0)).toString());
        hashMap2.put(n, new StringBuilder(String.valueOf(z6 ? 1 : 0)).toString());
        hashMap2.put(h, str4);
        if (hashMap.size() > 0) {
            hashMap2.put(T, str3);
            int i2 = 0;
            l[] lVarArr = new l[hashMap.size()];
            Iterator it = hashMap.entrySet().iterator();
            while (true) {
                int i3 = i2;
                if (!it.hasNext()) {
                    break;
                }
                Map.Entry entry = (Map.Entry) it.next();
                i2 = i3 + 1;
                lVarArr[i3] = new l("avator.jpg", (File) entry.getValue(), (String) entry.getKey(), (byte) 0);
            }
            jSONObject = new JSONObject(a(str5, hashMap2, lVarArr));
        } else if (aVar != null) {
            hashMap2.put(R, aVar.g());
            hashMap2.put(Q, aVar.h());
            hashMap2.put(S, aVar.toString());
            jSONObject = new JSONObject(a(str5, hashMap2));
        } else {
            jSONObject = new JSONObject(a(str5, hashMap2));
        }
        a(jSONObject.optJSONObject(x), bVar);
        return jSONObject.optJSONObject(x).optString("weixin_url");
    }

    public final String a(String str, StringBuilder sb) {
        HashMap hashMap = new HashMap();
        hashMap.put(h, str);
        JSONObject jSONObject = new JSONObject(a(String.valueOf(s) + "/admin/check", hashMap));
        sb.append(new StringBuilder(String.valueOf(jSONObject.optInt("ec"))).toString());
        return jSONObject.optString("em", PoiTypeDef.All);
    }

    public final String a(String str, HashMap hashMap, String str2, com.immomo.momo.plugin.b.a aVar, String str3, String str4, String str5, c cVar) {
        JSONObject jSONObject;
        String str6 = String.valueOf(s) + "/comment/publish";
        HashMap hashMap2 = new HashMap();
        hashMap2.put(i, str);
        hashMap2.put(p, str5);
        hashMap2.put(aa, str4);
        hashMap2.put(Z, str3);
        hashMap2.put(T, str2);
        if (hashMap.size() > 0) {
            int i2 = 0;
            l[] lVarArr = new l[hashMap.size()];
            Iterator it = hashMap.entrySet().iterator();
            while (true) {
                int i3 = i2;
                if (!it.hasNext()) {
                    break;
                }
                Map.Entry entry = (Map.Entry) it.next();
                i2 = i3 + 1;
                lVarArr[i3] = new l("avator.jpg", (File) entry.getValue(), (String) entry.getKey(), (byte) 0);
            }
            jSONObject = new JSONObject(a(str6, hashMap2, lVarArr));
        } else if (aVar != null) {
            hashMap2.put(R, aVar.g());
            hashMap2.put(Q, aVar.h());
            hashMap2.put(S, aVar.toString());
            jSONObject = new JSONObject(a(str6, hashMap2));
        } else {
            jSONObject = new JSONObject(a(str6, hashMap2));
        }
        JSONObject optJSONObject = jSONObject.optJSONObject(x);
        cVar.o = optJSONObject.optInt(g);
        cVar.p = optJSONObject.optString(Z);
        cVar.f3018a = optJSONObject.optString(ac);
        cVar.l = optJSONObject.optString(h);
        cVar.j = optJSONObject.optString(p);
        cVar.g = a.a(optJSONObject.optLong(H));
        cVar.q = optJSONObject.optString(S);
        cVar.h = optJSONObject.optString(V);
        cVar.e = a(optJSONObject.optJSONArray(T)) == null ? cVar.e : a(optJSONObject.optJSONArray(T));
        cVar.d = optJSONObject.optString(i);
        cVar.r = optJSONObject.optString(R);
        cVar.s = optJSONObject.optString(Q);
        cVar.c = optJSONObject.optString(W);
        return jSONObject.getString("em");
    }

    public final String a(String str, boolean z2, boolean z3, boolean z4, boolean z5) {
        String str2 = String.valueOf(s) + "/post/share";
        HashMap hashMap = new HashMap();
        hashMap.put(p, str);
        hashMap.put(j, z2 ? "1" : "0");
        hashMap.put(k, z3 ? "1" : "0");
        hashMap.put(l, z4 ? "1" : "0");
        hashMap.put(n, z5 ? "1" : "0");
        return new JSONObject(a(str2, hashMap)).optJSONObject(x).optString("weixin_url", PoiTypeDef.All);
    }

    public final String a(HashMap hashMap, HashMap hashMap2, com.immomo.momo.plugin.b.a aVar, b bVar, String str) {
        JSONObject jSONObject;
        String str2 = String.valueOf(s) + "/post/edit";
        hashMap.put(U, "1");
        if (hashMap2.size() > 0) {
            hashMap.put(T, str);
            l[] lVarArr = new l[hashMap2.size()];
            int i2 = 0;
            for (Map.Entry entry : hashMap2.entrySet()) {
                lVarArr[i2] = new l("avator.jpg", (File) entry.getValue(), (String) entry.getKey(), (byte) 0);
                i2++;
            }
            jSONObject = new JSONObject(a(str2, hashMap, lVarArr));
        } else if (aVar != null) {
            hashMap.put(R, aVar.g());
            hashMap.put(Q, aVar.h());
            hashMap.put(S, aVar.toString());
            jSONObject = new JSONObject(a(str2, hashMap));
        } else {
            jSONObject = new JSONObject(a(str2, hashMap));
        }
        a(jSONObject.optJSONObject(x), bVar);
        return jSONObject.optJSONObject(x).optString("weixin_url", PoiTypeDef.All);
    }

    public final List a(String str) {
        HashMap hashMap = new HashMap();
        hashMap.put(u, str);
        JSONObject jSONObject = new JSONObject(a(String.valueOf(s) + "/welcome/balist", hashMap));
        ArrayList arrayList = new ArrayList();
        if (jSONObject.has(x)) {
            JSONArray jSONArray = jSONObject.getJSONArray(x);
            for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                arrayList.add(b(jSONArray.getJSONObject(i2)));
            }
        }
        return arrayList;
    }

    public final List a(String str, int i2, int i3) {
        HashMap hashMap = new HashMap();
        hashMap.put(p, str);
        hashMap.put(y, String.valueOf(i2));
        hashMap.put(B, String.valueOf(i3));
        JSONObject jSONObject = new JSONObject(a(String.valueOf(s) + "/comment/lists", hashMap));
        ArrayList arrayList = new ArrayList();
        if (jSONObject.has(x)) {
            JSONObject jSONObject2 = jSONObject.getJSONObject(x);
            if (jSONObject2.has(af)) {
                JSONArray optJSONArray = jSONObject2.optJSONArray(af);
                for (int i4 = 0; i4 < optJSONArray.length(); i4++) {
                    JSONObject jSONObject3 = optJSONArray.getJSONObject(i4);
                    c cVar = new c();
                    a(jSONObject3, cVar);
                    arrayList.add(cVar);
                }
            }
        }
        return arrayList;
    }

    public final void a(String str, String str2, String str3) {
        HashMap hashMap = new HashMap();
        if (str != null) {
            hashMap.put("pid", str);
        }
        if (str2 != null) {
            hashMap.put("cid", str2);
        }
        hashMap.put("remoteid", str3);
        a(String.valueOf(s) + "/user/report", hashMap);
    }

    public final void a(String str, boolean z2, boolean z3, boolean z4) {
        String str2 = String.valueOf(s) + "/share";
        HashMap hashMap = new HashMap();
        hashMap.put(f, str);
        hashMap.put(k, z3 ? "1" : "0");
        hashMap.put(j, z2 ? "1" : "0");
        hashMap.put(l, z4 ? "1" : "0");
        a(str2, hashMap);
    }

    public final boolean a(String str, int i2, f fVar) {
        HashMap hashMap = new HashMap();
        hashMap.put(u, str);
        hashMap.put(y, String.valueOf(i2));
        hashMap.put(B, String.valueOf(20));
        JSONObject jSONObject = new JSONObject(a(String.valueOf(s) + "/lists", hashMap));
        if (!jSONObject.has(x)) {
            return false;
        }
        JSONObject jSONObject2 = jSONObject.getJSONObject(x);
        fVar.e = jSONObject2.optInt(ab);
        if (jSONObject2.has(z)) {
            fVar.f = new ArrayList();
            JSONArray jSONArray = jSONObject2.getJSONArray(z);
            for (int i3 = 0; i3 < jSONArray.length(); i3++) {
                fVar.f.add(b(jSONArray.getJSONObject(i3)));
            }
        }
        return jSONObject2.optInt(L) == 1;
    }

    public final boolean a(String str, int i2, List list) {
        HashMap hashMap = new HashMap();
        hashMap.put(ao, str);
        hashMap.put(y, String.valueOf(i2));
        hashMap.put(B, String.valueOf(20));
        JSONObject jSONObject = new JSONObject(a(String.valueOf(s) + "/user/applylist", hashMap));
        if (!jSONObject.has(x)) {
            return false;
        }
        JSONObject jSONObject2 = jSONObject.getJSONObject(x);
        a(jSONObject2.getJSONArray(z), list);
        return jSONObject2.optInt(L) == 1;
    }

    public final boolean a(String str, List list) {
        HashMap hashMap = new HashMap();
        hashMap.put(f, str);
        JSONObject jSONObject = new JSONObject(a(String.valueOf(s) + "/v2/search", hashMap));
        if (!jSONObject.has(x)) {
            return false;
        }
        JSONObject jSONObject2 = jSONObject.getJSONObject(x);
        if (jSONObject2.has(ay)) {
            JSONArray jSONArray = jSONObject2.getJSONArray(ay);
            for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                JSONObject jSONObject3 = jSONArray.getJSONObject(i2);
                com.immomo.momo.service.bean.c.a aVar = new com.immomo.momo.service.bean.c.a();
                aVar.c = b(jSONObject3);
                aVar.f3016a = 1;
                list.add(aVar);
            }
        }
        if (jSONObject2.has(az)) {
            JSONArray jSONArray2 = jSONObject2.getJSONArray(az);
            if (jSONArray2.length() > 0) {
                com.immomo.momo.service.bean.c.a aVar2 = new com.immomo.momo.service.bean.c.a();
                aVar2.f3016a = 3;
                aVar2.b = "待创建";
                list.add(aVar2);
            }
            for (int i3 = 0; i3 < jSONArray2.length(); i3++) {
                JSONObject jSONObject4 = jSONArray2.getJSONObject(i3);
                com.immomo.momo.service.bean.c.a aVar3 = new com.immomo.momo.service.bean.c.a();
                aVar3.c = b(jSONObject4);
                aVar3.f3016a = 2;
                list.add(aVar3);
            }
        }
        return jSONObject2.optInt(as) == 1;
    }

    public final boolean a(List list, int i2) {
        HashMap hashMap = new HashMap();
        hashMap.put(y, new StringBuilder().append(i2).toString());
        hashMap.put(B, "20");
        JSONObject jSONObject = new JSONObject(a(String.valueOf(s) + "/user/adminreportlist", hashMap)).getJSONObject(x);
        JSONArray jSONArray = jSONObject.getJSONArray("list");
        for (int i3 = 0; i3 < jSONArray.length(); i3++) {
            JSONObject jSONObject2 = jSONArray.getJSONObject(i3);
            e eVar = new e();
            eVar.h = jSONObject2.optString("action");
            eVar.i = jSONObject2.optInt("action_count");
            eVar.f = jSONObject2.optString("cid");
            eVar.k = jSONObject2.optString("content");
            eVar.l = jSONObject2.optInt("floor");
            eVar.n = jSONObject2.optInt("is_delete") == 1;
            eVar.m = jSONObject2.optInt("is_lock") == 1;
            eVar.g = jSONObject2.optInt("total");
            eVar.c = jSONObject2.optString("tid");
            eVar.d = jSONObject2.optString("tname");
            eVar.e = jSONObject2.optString("pid");
            eVar.j = jSONObject2.optString("title");
            eVar.b = jSONObject2.optString("type");
            eVar.f3020a = jSONObject2.optString("type_zh");
            eVar.o = jSONObject2.optString("owner");
            eVar.a(jSONObject2.optLong("last_update_time") * 1000);
            list.add(eVar);
        }
        return jSONObject.optInt(L) == 1;
    }

    public final boolean a(List list, int i2, String str, StringBuilder sb) {
        HashMap hashMap = new HashMap();
        hashMap.put(y, new StringBuilder(String.valueOf(i2)).toString());
        hashMap.put(B, new StringBuilder(String.valueOf(20)).toString());
        hashMap.put(h, str);
        hashMap.put(C, new StringBuilder(String.valueOf(1)).toString());
        JSONObject optJSONObject = new JSONObject(a(String.valueOf(s) + "/post/lists", hashMap)).optJSONObject(x);
        if (optJSONObject != null) {
            JSONArray optJSONArray = optJSONObject.optJSONArray(ae);
            for (int i3 = 0; i3 < optJSONArray.length(); i3++) {
                b bVar = new b();
                a(optJSONArray.getJSONObject(i3), bVar);
                list.add(bVar);
            }
            sb.append(new StringBuilder(String.valueOf(optJSONObject.optInt(B))).toString());
        }
        return optJSONObject.optInt(L) == 1;
    }

    public final boolean a(List list, String str, int i2, int i3) {
        HashMap hashMap = new HashMap();
        hashMap.put(h, str);
        hashMap.put(y, String.valueOf(i2));
        hashMap.put(B, String.valueOf(i3));
        JSONObject optJSONObject = new JSONObject(a(String.valueOf(s) + "/members", hashMap)).optJSONObject(x);
        if (optJSONObject == null) {
            return false;
        }
        JSONArray optJSONArray = optJSONObject.optJSONArray(ai);
        JSONArray optJSONArray2 = optJSONObject.optJSONArray(aj);
        for (int i4 = 0; i4 < optJSONArray.length(); i4++) {
            g gVar = new g();
            w.a(gVar, optJSONArray.getJSONObject(i4));
            ((bc) list.get(0)).f3008a.add(gVar);
        }
        for (int i5 = 0; i5 < optJSONArray2.length(); i5++) {
            g gVar2 = new g();
            w.a(gVar2, optJSONArray2.getJSONObject(i5));
            ((bc) list.get(1)).f3008a.add(gVar2);
        }
        return optJSONObject.optInt(L) == 1;
    }

    public final l b(int i2) {
        HashMap hashMap = new HashMap();
        hashMap.put(y, new StringBuilder(String.valueOf(i2)).toString());
        hashMap.put(B, new StringBuilder(String.valueOf(20)).toString());
        JSONObject optJSONObject = new JSONObject(a(String.valueOf(s) + "/comment/my", hashMap)).optJSONObject(x);
        l lVar = new l();
        optJSONObject.getInt(B);
        optJSONObject.getInt(y);
        lVar.a(optJSONObject.getInt(L));
        lVar.a(b(optJSONObject.getJSONArray(af)));
        return lVar;
    }

    public final String b(String str) {
        String str2 = String.valueOf(s) + "/welcome/join";
        HashMap hashMap = new HashMap();
        hashMap.put(h, str);
        return new JSONObject(a(str2, hashMap)).optString("em");
    }

    public final List b() {
        JSONObject jSONObject = new JSONObject(a(String.valueOf(s) + "/category", (Map) null));
        ArrayList arrayList = new ArrayList();
        if (jSONObject.has(x)) {
            JSONArray jSONArray = jSONObject.getJSONArray(x);
            for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                arrayList.add(a(jSONArray.getJSONObject(i2)));
            }
        }
        return arrayList;
    }

    public final List b(JSONArray jSONArray) {
        System.out.println(jSONArray.toString());
        ArrayList arrayList = new ArrayList();
        for (int i2 = 0; i2 < jSONArray.length(); i2++) {
            c cVar = new c();
            a(jSONArray.getJSONObject(i2), cVar);
            arrayList.add(cVar);
        }
        return arrayList;
    }

    public final void b(String str, String str2) {
        HashMap hashMap = new HashMap();
        if (str != null) {
            hashMap.put("type", "p");
            hashMap.put("id", str);
        } else if (str2 != null) {
            hashMap.put("id", str2);
            hashMap.put("type", "c");
        }
        a(String.valueOf(s) + "/admin/ignore", hashMap);
    }

    public final String c(String str) {
        if (a.a((CharSequence) str)) {
            return null;
        }
        String str2 = String.valueOf(s) + "/user/join";
        HashMap hashMap = new HashMap();
        hashMap.put(h, str);
        return new JSONObject(a(str2, hashMap)).optString("em", PoiTypeDef.All);
    }

    public final String c(String str, String str2) {
        String str3 = String.valueOf(s) + "/post/report";
        HashMap hashMap = new HashMap();
        hashMap.put(p, str);
        hashMap.put(ap, new StringBuilder(String.valueOf(str2)).toString());
        return new JSONObject(a(str3, hashMap)).optString("em", PoiTypeDef.All);
    }

    public final List c() {
        JSONObject jSONObject = new JSONObject(a(String.valueOf(s) + "/welcome/catlist", (Map) null));
        ArrayList arrayList = new ArrayList();
        if (jSONObject.has(x)) {
            JSONArray jSONArray = jSONObject.getJSONArray(x);
            for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                arrayList.add(a(jSONArray.getJSONObject(i2)));
            }
        }
        return arrayList;
    }

    public final String d(String str) {
        if (a.a((CharSequence) str)) {
            return null;
        }
        String str2 = String.valueOf(s) + "/user/quit";
        HashMap hashMap = new HashMap();
        hashMap.put(h, str);
        return new JSONObject(a(str2, hashMap)).optString("em", PoiTypeDef.All);
    }

    public final String d(String str, String str2) {
        String str3 = String.valueOf(s) + "/post/delete";
        HashMap hashMap = new HashMap();
        hashMap.put(p, str);
        hashMap.put(ap, str2);
        return new JSONObject(a(str3, hashMap)).optString("em", PoiTypeDef.All);
    }

    public final List d() {
        JSONObject jSONObject = new JSONObject(a(String.valueOf(s) + "/search/tags", (Map) null));
        ArrayList arrayList = new ArrayList();
        if (jSONObject.has(x)) {
            JSONArray optJSONArray = jSONObject.getJSONObject(x).optJSONArray(at);
            for (int i2 = 0; i2 < optJSONArray.length(); i2++) {
                arrayList.add(optJSONArray.getString(i2));
            }
        }
        return arrayList;
    }

    public final com.immomo.momo.protocol.a.a.e e() {
        JSONObject jSONObject = new JSONObject(a(String.valueOf(s) + "/home", (Map) null));
        JSONObject optJSONObject = jSONObject.optJSONObject(x);
        com.immomo.momo.protocol.a.a.e eVar = new com.immomo.momo.protocol.a.a.e();
        eVar.b = optJSONObject.optInt(O);
        eVar.e = optJSONObject.optInt(am) == 1;
        eVar.c = optJSONObject.optInt(P);
        eVar.d = 1;
        ArrayList arrayList = new ArrayList();
        bb bbVar = new bb(0);
        JSONArray jSONArray = optJSONObject.getJSONArray(M);
        if (jSONArray.length() > 0) {
            List b = new ao().b();
            for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                d b2 = b(jSONArray.getJSONObject(i2));
                arrayList.add(b2);
                int indexOf = b.indexOf(b2);
                if (indexOf >= 0) {
                    d dVar = (d) b.get(indexOf);
                    if (!dVar.q) {
                        b2.q = dVar.h != b2.h;
                    } else {
                        b2.q = true;
                    }
                }
            }
        }
        bbVar.a(arrayList);
        eVar.a(bbVar);
        bb bbVar2 = new bb(1);
        ArrayList arrayList2 = new ArrayList();
        JSONArray jSONArray2 = optJSONObject.getJSONArray(N);
        for (int i3 = 0; i3 < jSONArray2.length(); i3++) {
            arrayList2.add(b(jSONArray2.getJSONObject(i3)));
        }
        bbVar2.a(arrayList2);
        eVar.a(bbVar2);
        at r2 = com.immomo.momo.g.r();
        if (r2 != null) {
            int optInt = jSONObject.optInt("timesec");
            this.e.a((Object) ("--------------------time=" + optInt));
            r2.c("tiebahomerefreshtime", Integer.valueOf(optInt));
        }
        return eVar;
    }

    public final String e(String str) {
        String str2 = String.valueOf(s) + "/share";
        HashMap hashMap = new HashMap();
        hashMap.put(f, str);
        return new JSONObject(a(str2, hashMap)).optString("em", PoiTypeDef.All);
    }

    public final String e(String str, String str2) {
        String str3 = String.valueOf(s) + "/comment/report";
        HashMap hashMap = new HashMap();
        hashMap.put(ac, str);
        hashMap.put(ap, new StringBuilder(String.valueOf(str2)).toString());
        return new JSONObject(a(str3, hashMap)).optString("em", PoiTypeDef.All);
    }

    public final d f(String str) {
        String str2 = String.valueOf(s) + "/profile";
        HashMap hashMap = new HashMap();
        hashMap.put(h, str);
        return b(new JSONObject(a(str2, hashMap)).optJSONObject(x));
    }

    public final String f(String str, String str2) {
        String str3 = String.valueOf(s) + "/comment/delete";
        HashMap hashMap = new HashMap();
        hashMap.put(ac, str);
        if (!a.a((CharSequence) str2)) {
            hashMap.put(ap, str2);
        }
        return new JSONObject(a(str3, hashMap)).optString("em", PoiTypeDef.All);
    }

    public final List f() {
        JSONObject jSONObject = new JSONObject(a(String.valueOf(s) + "/creating", (Map) null));
        ArrayList arrayList = new ArrayList();
        if (jSONObject.has(x)) {
            JSONObject jSONObject2 = jSONObject.getJSONObject(x);
            JSONObject optJSONObject = jSONObject2.optJSONObject(av);
            if (optJSONObject.has(ax)) {
                bb bbVar = new bb(3);
                ArrayList arrayList2 = new ArrayList();
                JSONArray jSONArray = optJSONObject.getJSONArray(ax);
                for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                    arrayList2.add(b(jSONArray.getJSONObject(i2)));
                }
                bbVar.a(arrayList2);
                if (arrayList2.size() > 0) {
                    arrayList.add(bbVar);
                }
            }
            JSONObject optJSONObject2 = jSONObject2.optJSONObject(aw);
            if (optJSONObject2.has(ax)) {
                bb bbVar2 = new bb(2);
                ArrayList arrayList3 = new ArrayList();
                JSONArray jSONArray2 = optJSONObject2.getJSONArray(ax);
                for (int i3 = 0; i3 < jSONArray2.length(); i3++) {
                    arrayList3.add(b(jSONArray2.getJSONObject(i3)));
                }
                bbVar2.a(arrayList3);
                arrayList.add(bbVar2);
            }
        }
        return arrayList;
    }

    public final List g(String str) {
        HashMap hashMap = new HashMap();
        hashMap.put(h, str);
        hashMap.put(y, String.valueOf(0));
        hashMap.put(B, String.valueOf(8));
        JSONObject optJSONObject = new JSONObject(a(String.valueOf(s) + "/members", hashMap)).optJSONObject(x);
        ArrayList arrayList = new ArrayList();
        if (optJSONObject != null) {
            JSONArray optJSONArray = optJSONObject.optJSONArray(aj);
            for (int i2 = 0; i2 < optJSONArray.length(); i2++) {
                g gVar = new g();
                w.a(gVar, optJSONArray.getJSONObject(i2));
                arrayList.add(gVar);
            }
        }
        return arrayList;
    }

    public final String h(String str) {
        String str2 = String.valueOf(s) + "/create";
        HashMap hashMap = new HashMap();
        hashMap.put(f, str);
        return new JSONObject(a(str2, hashMap)).optString("em");
    }

    public final b i(String str) {
        HashMap hashMap = new HashMap();
        hashMap.put(p, str);
        JSONObject jSONObject = new JSONObject(a(String.valueOf(s) + "/post/profile", hashMap));
        b bVar = new b();
        if (jSONObject.has(x)) {
            a(jSONObject.optJSONObject(x), bVar);
        }
        return bVar;
    }

    public final String j(String str) {
        String str2 = String.valueOf(s) + "/post/zding";
        HashMap hashMap = new HashMap();
        hashMap.put(p, str);
        return new JSONObject(a(str2, hashMap)).optString("em", PoiTypeDef.All);
    }

    public final String m(String str) {
        String str2 = String.valueOf(s) + "/post/unzding";
        HashMap hashMap = new HashMap();
        hashMap.put(p, str);
        return new JSONObject(a(str2, hashMap)).optString("em", PoiTypeDef.All);
    }

    public final String n(String str) {
        String str2 = String.valueOf(s) + "/post/elite";
        HashMap hashMap = new HashMap();
        hashMap.put(p, str);
        return new JSONObject(a(str2, hashMap)).optString("em", PoiTypeDef.All);
    }

    public final String o(String str) {
        String str2 = String.valueOf(s) + "/post/unelite";
        HashMap hashMap = new HashMap();
        hashMap.put(p, str);
        return new JSONObject(a(str2, hashMap)).optString("em", PoiTypeDef.All);
    }

    public final String p(String str) {
        String str2 = String.valueOf(s) + "/post/lock";
        HashMap hashMap = new HashMap();
        hashMap.put(p, str);
        return new JSONObject(a(str2, hashMap)).optString("em", PoiTypeDef.All);
    }

    public final String q(String str) {
        String str2 = String.valueOf(s) + "/post/unlock";
        HashMap hashMap = new HashMap();
        hashMap.put(p, str);
        return new JSONObject(a(str2, hashMap)).optString("em", PoiTypeDef.All);
    }

    public final List r(String str) {
        HashMap hashMap = new HashMap();
        hashMap.put(ak, str);
        JSONObject jSONObject = new JSONObject(a(String.valueOf(s) + "/user/tiebas", hashMap));
        ArrayList arrayList = new ArrayList();
        if (jSONObject.has(x)) {
            JSONObject jSONObject2 = jSONObject.getJSONObject(x);
            if (jSONObject2.has(z)) {
                bb bbVar = new bb(0);
                ArrayList arrayList2 = new ArrayList();
                JSONArray jSONArray = jSONObject2.getJSONArray(z);
                for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                    arrayList2.add(b(jSONArray.getJSONObject(i2)));
                }
                bbVar.a(arrayList2);
                if (arrayList2.size() != 0) {
                    arrayList.add(bbVar);
                }
            }
            if (jSONObject2.has(aC)) {
                bb bbVar2 = new bb(3);
                ArrayList arrayList3 = new ArrayList();
                JSONArray jSONArray2 = jSONObject2.getJSONArray(aC);
                for (int i3 = 0; i3 < jSONArray2.length(); i3++) {
                    arrayList3.add(b(jSONArray2.getJSONObject(i3)));
                }
                bbVar2.a(arrayList3);
                if (arrayList3.size() != 0) {
                    arrayList.add(bbVar2);
                }
            }
        }
        return arrayList;
    }
}
