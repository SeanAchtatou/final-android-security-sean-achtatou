package com.immomo.momo.protocol.a;

import com.immomo.momo.g;
import java.util.HashMap;
import org.json.JSONObject;

public final class u extends p {
    private static u f = null;
    private static String g = (String.valueOf(b) + "/site");
    private static String h = "sid";
    private static String i = "desc";

    public static u a() {
        if (f == null) {
            f = new u();
        }
        return f;
    }

    public final String a(String str, String str2) {
        String str3 = String.valueOf(g) + "/correction?fr=" + g.q().h;
        HashMap hashMap = new HashMap();
        hashMap.put(h, str);
        hashMap.put(i, str2);
        return new JSONObject(a(str3, hashMap)).optString("msg");
    }
}
