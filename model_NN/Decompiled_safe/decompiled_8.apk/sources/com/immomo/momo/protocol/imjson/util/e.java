package com.immomo.momo.protocol.imjson.util;

import android.support.v4.b.a;
import com.amap.mapapi.poisearch.PoiTypeDef;
import java.io.BufferedReader;
import java.io.InputStreamReader;

final class e extends Thread {

    /* renamed from: a  reason: collision with root package name */
    private boolean f2952a;
    private String b;
    private Process c;

    private e() {
        this.f2952a = true;
        this.b = PoiTypeDef.All;
        this.c = null;
    }

    /* synthetic */ e(byte b2) {
        this();
    }

    /* access modifiers changed from: package-private */
    public final void a() {
        this.f2952a = false;
        if (this.c != null) {
            this.c.destroy();
            this.c = null;
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(String str) {
        this.b = str;
        this.f2952a = true;
        start();
    }

    public final void run() {
        BufferedReader bufferedReader;
        String readLine;
        try {
            this.c = Runtime.getRuntime().exec(this.b);
            bufferedReader = new BufferedReader(new InputStreamReader(this.c.getInputStream()));
            while (this.f2952a && (readLine = bufferedReader.readLine()) != null) {
                c.a().b(readLine);
            }
            a.a(bufferedReader);
            if (this.c != null) {
                this.c.destroy();
                this.c = null;
            }
        } catch (Exception e) {
        } catch (Throwable th) {
            a.a(bufferedReader);
            if (this.c != null) {
                this.c.destroy();
                this.c = null;
            }
            throw th;
        }
    }
}
