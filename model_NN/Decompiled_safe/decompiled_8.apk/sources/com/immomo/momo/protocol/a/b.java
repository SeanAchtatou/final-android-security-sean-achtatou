package com.immomo.momo.protocol.a;

import android.support.v4.b.a;
import com.immomo.momo.util.m;
import java.util.concurrent.atomic.AtomicInteger;

public final class b {

    /* renamed from: a  reason: collision with root package name */
    private String f2897a;
    private String b;
    private String[] c;
    private String d;
    private int e;
    private int f;
    private int g;
    private boolean h;

    public b(String str, String str2) {
        this(str, str2, null);
    }

    public b(String str, String str2, String[] strArr) {
        new m(this);
        this.f2897a = str;
        this.b = str2;
        this.c = strArr;
        this.g = 0;
        this.f = 1;
        this.e = 0;
    }

    public final synchronized void a() {
        this.e++;
        if (this.e % 2 == 0) {
            if (this.f == 1) {
                this.f = 2;
            } else {
                if (this.f == 2) {
                    if (this.c == null || this.c.length <= 0 || a.a((CharSequence) this.d)) {
                        this.f = 1;
                    }
                } else if (this.f == 3) {
                    this.g++;
                    if (this.g >= this.c.length) {
                        this.g = 0;
                        this.f = 1;
                    }
                }
                this.f = 3;
            }
        }
    }

    public final void a(String str) {
        this.d = str;
    }

    public final synchronized void a(AtomicInteger atomicInteger, StringBuilder sb) {
        if (this.f == 3 && this.c != null && this.c.length > 0) {
            if (this.g >= this.c.length) {
                this.g = 0;
            }
            sb.append(this.c[this.g]);
            atomicInteger.set(3);
        } else if (this.f == 1) {
            sb.append(this.f2897a);
            atomicInteger.set(1);
        } else if (this.f == 2) {
            sb.append(this.b);
            atomicInteger.set(2);
        } else {
            sb.append(this.f2897a);
            atomicInteger.set(1);
        }
    }

    public final void b() {
        this.e = 0;
    }

    public final String c() {
        return this.c[this.g];
    }

    public final String d() {
        return this.d;
    }

    public final boolean e() {
        return this.h;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        b bVar = (b) obj;
        return this.f2897a == null ? bVar.f2897a == null : this.f2897a.equals(bVar.f2897a);
    }

    public final void f() {
        this.h = true;
    }

    public final int hashCode() {
        return (this.f2897a == null ? 0 : this.f2897a.hashCode()) + 31;
    }

    public final String toString() {
        return "ApiHost [originalHost=" + this.f2897a + ", tempHost=" + this.b + ", ipHost=" + this.c + ", caFilename=" + this.d + "]";
    }
}
