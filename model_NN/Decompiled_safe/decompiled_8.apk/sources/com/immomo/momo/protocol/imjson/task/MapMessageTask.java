package com.immomo.momo.protocol.imjson.task;

import android.support.v4.b.a;
import com.immomo.a.a.d.i;
import com.immomo.momo.service.bean.Message;

public class MapMessageTask extends MessageTask {
    public MapMessageTask(Message message) {
        super(g.Succession, message);
    }

    /* access modifiers changed from: protected */
    public final void a(Message message, i iVar) {
        iVar.c(a.a(a.a(a.a(String.valueOf(com.immomo.momo.a.f) + "/maps", "lat", new StringBuilder(String.valueOf(message.convertLat)).toString()), "lng", new StringBuilder(String.valueOf(message.convertLng)).toString()), "acc", new StringBuilder(String.valueOf(message.convertAcc)).toString()));
    }
}
