package com.immomo.momo.protocol.a;

import android.support.v4.b.a;
import com.amap.mapapi.location.LocationManagerProxy;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.protocol.a.a.g;
import com.immomo.momo.protocol.a.a.h;
import com.immomo.momo.protocol.a.a.i;
import com.immomo.momo.service.aq;
import com.immomo.momo.service.bean.a.e;
import com.immomo.momo.service.bean.a.f;
import com.immomo.momo.service.bean.bf;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

public final class o extends p {
    private static String A = "toname";
    private static String B = "commentid";
    private static String C = "replycontent";
    private static String D = "emotion_library";
    private static String E = "emotion_name";
    private static String F = "emotion_body";
    private static String G = "gid";
    private static String H = "ptid";
    private static String I = "role";
    private static String J = "top";
    private static String K = "gftype";
    private static String L = "is_admin";
    private static String M = "members";
    private static String N = "use_cover";
    private static String O = "cover";
    private static o f = null;
    private static String g = (String.valueOf(b) + "/group");
    private static String h = (String.valueOf(g) + "/feed");
    private static String i = "feed";
    private static String j = "feeds";
    private static String k = "feedid";
    private static String l = "create_time";
    private static String m = "pics";
    private static String n = "owner";
    private static String o = "comment_count";
    private static String p = "member_count";
    private static String q = "distance";
    private static String r = "loctype";
    private static String s = "remain";
    private static String t = "user";
    private static String u = "content";
    private static String v = "srcid";
    private static String w = "srctype";
    private static String x = "replytype";
    private static String y = "content_type";
    private static String z = "tomomoid";

    public static o a() {
        if (f == null) {
            f = new o();
        }
        return f;
    }

    public static bf a(JSONObject jSONObject) {
        bf bfVar = new bf();
        bfVar.i = jSONObject.optString("name");
        bfVar.h = jSONObject.getString("momoid");
        bfVar.ae = new String[]{jSONObject.optString("avatar")};
        bfVar.a((float) jSONObject.optDouble("distance", -1.0d));
        bfVar.a(a(jSONObject.optLong("loc_timesec")));
        return bfVar;
    }

    public static void a(JSONObject jSONObject, e eVar) {
        eVar.c = jSONObject.optString(n);
        eVar.f = jSONObject.optString(k);
        eVar.h = jSONObject.optInt(LocationManagerProxy.KEY_STATUS_CHANGED);
        eVar.o = jSONObject.optInt(I);
        eVar.n = jSONObject.optInt(J) == 1;
        eVar.p = jSONObject.optInt(L) == 1;
        eVar.m = jSONObject.optInt(K);
        eVar.f2974a = jSONObject.optString(G);
        eVar.g = jSONObject.optString(H);
        eVar.i = jSONObject.optString(E);
        eVar.j = jSONObject.optString(D);
        eVar.b(jSONObject.optString(F));
        eVar.a(a.a(jSONObject.optLong(l)));
        eVar.a(jSONObject.optString(u));
        eVar.a((float) jSONObject.optInt(q, -1));
        if (eVar.m == 1) {
            eVar.e = jSONObject.optInt(p);
        } else {
            eVar.e = jSONObject.optInt(o);
        }
        JSONArray optJSONArray = jSONObject.optJSONArray(m);
        if (optJSONArray != null && optJSONArray.length() > 0) {
            String[] strArr = new String[optJSONArray.length()];
            for (int i2 = 0; i2 < optJSONArray.length(); i2++) {
                strArr[i2] = optJSONArray.getString(i2);
            }
            eVar.a(strArr);
        }
        if (jSONObject.has(t)) {
            JSONObject optJSONObject = jSONObject.optJSONObject(t);
            eVar.b = new aq().b(optJSONObject.optString("momoid", PoiTypeDef.All));
            if (eVar.b == null) {
                eVar.b = new bf();
            }
            eVar.b.X = -1;
            w.a(eVar.b, optJSONObject);
        }
    }

    public static List b(JSONObject jSONObject) {
        ArrayList arrayList = new ArrayList();
        JSONArray jSONArray = jSONObject.getJSONArray("comments");
        if (jSONArray != null && jSONArray.length() > 0) {
            for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                arrayList.add(c(jSONArray.getJSONObject(i2)));
            }
        }
        return arrayList;
    }

    public static f c(JSONObject jSONObject) {
        f fVar = new f();
        try {
            a(jSONObject.getJSONObject(i), new e());
        } catch (Exception e) {
        }
        try {
            jSONObject.optString(C);
        } catch (Exception e2) {
        }
        try {
            fVar.h = jSONObject.optString(k);
        } catch (Exception e3) {
        }
        try {
            fVar.g = jSONObject.optString(u);
        } catch (Exception e4) {
        }
        try {
            fVar.a(a.a(jSONObject.optLong(l)));
        } catch (Exception e5) {
        }
        try {
            fVar.j = jSONObject.optInt(w);
        } catch (Exception e6) {
        }
        try {
            fVar.l = jSONObject.optInt(y);
        } catch (Exception e7) {
        }
        try {
            fVar.b = jSONObject.optString(n);
        } catch (Exception e8) {
        }
        try {
            fVar.i = jSONObject.optString(B);
        } catch (Exception e9) {
        }
        try {
            fVar.k = jSONObject.optInt(x);
        } catch (Exception e10) {
        }
        try {
            if (jSONObject.has(t)) {
                JSONObject optJSONObject = jSONObject.optJSONObject(t);
                fVar.f2975a = new bf();
                fVar.f2975a.X = -1;
                w.a(fVar.f2975a, optJSONObject);
            }
        } catch (Exception e11) {
        }
        try {
            fVar.n = jSONObject.optString(v);
        } catch (Exception e12) {
        }
        try {
            fVar.e = jSONObject.optString(A);
        } catch (Exception e13) {
        }
        try {
            fVar.c = jSONObject.optString(z);
        } catch (Exception e14) {
        }
        try {
            fVar.m = jSONObject.optInt(LocationManagerProxy.KEY_STATUS_CHANGED);
        } catch (Exception e15) {
        }
        return fVar;
    }

    public final g a(String str, int i2, f fVar) {
        String str2 = String.valueOf(g) + "/comment/lists";
        HashMap hashMap = new HashMap();
        hashMap.put(k, new StringBuilder(String.valueOf(str)).toString());
        hashMap.put("index", new StringBuilder(String.valueOf(i2)).toString());
        hashMap.put("count", new StringBuilder(String.valueOf(20)).toString());
        if (fVar != null) {
            hashMap.put(B, fVar.i);
            if (fVar.a() != null) {
                hashMap.put("timestamp", new StringBuilder(String.valueOf(fVar.a().getTime() / 1000)).toString());
            }
        }
        return new g(new JSONObject(a(str2, hashMap)).optJSONObject("data"));
    }

    public final h a(String str, int i2) {
        boolean z2 = true;
        HashMap hashMap = new HashMap();
        hashMap.put("index", new StringBuilder(String.valueOf(i2)).toString());
        hashMap.put("count", new StringBuilder(String.valueOf(20)).toString());
        hashMap.put(G, str);
        JSONObject optJSONObject = new JSONObject(a(String.valueOf(h) + "/lists", hashMap)).optJSONObject("data");
        h hVar = new h();
        optJSONObject.optInt("count");
        optJSONObject.optInt("index");
        hVar.b = optJSONObject.optInt(s) == 1;
        hVar.d = optJSONObject.optInt(N) == 1;
        hVar.f2887a = optJSONObject.optString(O);
        if (optJSONObject.optInt(L) != 1) {
            z2 = false;
        }
        hVar.c = z2;
        hVar.e = new ArrayList();
        JSONArray optJSONArray = optJSONObject.optJSONArray(j);
        if (optJSONArray != null) {
            for (int i3 = 0; i3 < optJSONArray.length(); i3++) {
                e eVar = new e();
                a(optJSONArray.getJSONObject(i3), eVar);
                hVar.e.add(eVar);
            }
        }
        return hVar;
    }

    public final i a(String str) {
        HashMap hashMap = new HashMap();
        hashMap.put("index", new StringBuilder(String.valueOf(0)).toString());
        hashMap.put("count", new StringBuilder(String.valueOf(20)).toString());
        hashMap.put(G, str);
        JSONObject optJSONObject = new JSONObject(a(String.valueOf(g) + "/gzone/members", hashMap)).optJSONObject("data");
        i iVar = new i();
        optJSONObject.optInt("count");
        optJSONObject.optInt("index");
        optJSONObject.optInt(s);
        optJSONObject.optInt(L);
        JSONArray optJSONArray = optJSONObject.optJSONArray(M);
        iVar.f2888a = new ArrayList();
        if (optJSONArray != null) {
            for (int i2 = 0; i2 < optJSONArray.length(); i2++) {
                iVar.f2888a.add(a(optJSONArray.getJSONObject(i2)));
            }
        }
        return iVar;
    }

    public final com.immomo.momo.protocol.a.a.o a(String str, String str2, int i2, int i3, String str3, String str4, String str5) {
        String str6 = String.valueOf(g) + "/comment/publish";
        HashMap hashMap = new HashMap();
        hashMap.put(k, str);
        hashMap.put(v, new StringBuilder(String.valueOf(str2)).toString());
        hashMap.put(w, new StringBuilder(String.valueOf(i2)).toString());
        hashMap.put(y, new StringBuilder(String.valueOf(i3)).toString());
        hashMap.put(u, new StringBuilder(String.valueOf(str3)).toString());
        hashMap.put(z, new StringBuilder(String.valueOf(str4)).toString());
        hashMap.put(A, new StringBuilder(String.valueOf(str5)).toString());
        return new com.immomo.momo.protocol.a.a.o(new JSONObject(a(str6, hashMap)));
    }

    public final String a(e eVar, String str, HashMap hashMap, String str2, com.immomo.momo.plugin.b.a aVar, String str3) {
        JSONObject jSONObject;
        String str4 = String.valueOf(h) + "/publish";
        HashMap hashMap2 = new HashMap();
        hashMap2.put(u, str);
        hashMap2.put(G, str3);
        hashMap2.put("lat", new StringBuilder(String.valueOf(com.immomo.momo.g.q().S)).toString());
        hashMap2.put("lng", new StringBuilder(String.valueOf(com.immomo.momo.g.q().T)).toString());
        hashMap2.put(r, new StringBuilder(String.valueOf(com.immomo.momo.g.q().aH)).toString());
        if (hashMap.size() > 0) {
            hashMap2.put(m, str2);
            int i2 = 0;
            l[] lVarArr = new l[hashMap.size()];
            Iterator it = hashMap.entrySet().iterator();
            while (true) {
                int i3 = i2;
                if (!it.hasNext()) {
                    break;
                }
                Map.Entry entry = (Map.Entry) it.next();
                i2 = i3 + 1;
                lVarArr[i3] = new l("avator.jpg", (File) entry.getValue(), (String) entry.getKey(), (byte) 0);
            }
            jSONObject = new JSONObject(a(str4, hashMap2, lVarArr));
        } else if (aVar != null) {
            hashMap2.put(E, aVar.g());
            hashMap2.put(D, aVar.h());
            hashMap2.put(F, aVar.toString());
            jSONObject = new JSONObject(a(str4, hashMap2));
        } else {
            jSONObject = new JSONObject(a(str4, hashMap2));
        }
        a(jSONObject.optJSONObject("data"), eVar);
        return jSONObject.getString("em");
    }

    public final String a(String str, File file) {
        String str2 = String.valueOf(g) + "/cover/upload";
        HashMap hashMap = new HashMap();
        hashMap.put(G, str);
        return new JSONObject(a(str2, hashMap, new l[]{new l(file.getName(), file, O, (byte) 0)})).getJSONObject("data").getString(O);
    }

    public final String a(String str, String str2) {
        String str3 = String.valueOf(h) + "/publishcheck";
        HashMap hashMap = new HashMap();
        hashMap.put(u, str);
        hashMap.put(G, str2);
        return new JSONObject(a(str3, hashMap)).getString("em");
    }

    public final String a(String str, boolean z2) {
        String str2 = z2 ? String.valueOf(g) + "/party/zding" : String.valueOf(g) + "/party/unzding";
        HashMap hashMap = new HashMap();
        hashMap.put(H, str);
        return new JSONObject(a(str2, hashMap)).optString("em");
    }

    public final String b(String str) {
        String str2 = String.valueOf(h) + "/delete";
        HashMap hashMap = new HashMap();
        hashMap.put(k, str);
        this.e.a(hashMap);
        return new JSONObject(a(str2, hashMap)).optString("em");
    }

    public final String b(String str, boolean z2) {
        String str2 = z2 ? String.valueOf(h) + "/zding" : String.valueOf(h) + "/unzding";
        HashMap hashMap = new HashMap();
        hashMap.put(k, str);
        return new JSONObject(a(str2, hashMap)).optString("em");
    }

    public final e c(String str) {
        HashMap hashMap = new HashMap();
        hashMap.put(k, str);
        e eVar = new e();
        a(new JSONObject(a(String.valueOf(h) + "/profile", hashMap)).optJSONObject("data"), eVar);
        return eVar;
    }

    public final String d(String str) {
        String str2 = String.valueOf(g) + "/comment/delete";
        HashMap hashMap = new HashMap();
        hashMap.put(B, str);
        return new JSONObject(a(str2, hashMap)).optString("em");
    }

    public final String e(String str) {
        String str2 = String.valueOf(g) + "/cover/flag";
        HashMap hashMap = new HashMap();
        hashMap.put(G, str);
        hashMap.put(N, "0");
        return new JSONObject(a(str2, hashMap)).optString("em");
    }

    public final g f(String str) {
        return a(str, 0, (f) null);
    }
}
