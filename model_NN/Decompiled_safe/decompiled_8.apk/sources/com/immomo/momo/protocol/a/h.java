package com.immomo.momo.protocol.a;

import android.support.v4.b.a;
import com.amap.mapapi.location.LocationManagerProxy;
import com.immomo.momo.g;
import com.immomo.momo.service.aq;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.service.bean.n;
import com.immomo.momo.service.bean.p;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

public final class h extends p {
    private static h f = null;
    private static String g = (String.valueOf(b) + "/discuss");

    public static int a(n nVar, JSONObject jSONObject) {
        JSONArray jSONArray;
        bf b;
        nVar.f = jSONObject.optString("did", nVar.f);
        nVar.b = jSONObject.optString("name", nVar.b);
        nVar.f3032a = a(jSONObject.getJSONArray("photos"));
        nVar.c = jSONObject.optString("owner", nVar.c);
        nVar.g = jSONObject.optInt(LocationManagerProxy.KEY_STATUS_CHANGED, nVar.g);
        nVar.h = jSONObject.optInt("level", nVar.h);
        com.immomo.momo.service.h hVar = new com.immomo.momo.service.h();
        if (jSONObject.has("members") && (jSONArray = jSONObject.getJSONArray("members")) != null) {
            nVar.e = new String[jSONArray.length()];
            StringBuffer stringBuffer = new StringBuffer();
            for (int i = 0; i < jSONArray.length(); i++) {
                JSONObject jSONObject2 = jSONArray.getJSONObject(i);
                if (!(jSONObject2 == null || (b = b(jSONObject2)) == null)) {
                    nVar.e[i] = b.h;
                    hVar.a(b.h, nVar.f, 3);
                    if (i == 0) {
                        stringBuffer.append(b.i);
                    } else {
                        stringBuffer.append("," + b.i);
                    }
                }
            }
            nVar.m = stringBuffer.toString();
        }
        hVar.a(nVar.f, nVar.e);
        nVar.j = jSONObject.optInt("member_count", nVar.j);
        nVar.k = jSONObject.optInt("member_max", nVar.k);
        try {
            nVar.i = a.a(jSONObject.getLong("create_time"));
            return 1;
        } catch (Exception e) {
            return 1;
        }
    }

    public static h a() {
        if (f == null) {
            f = new h();
        }
        return f;
    }

    private static p a(JSONObject jSONObject) {
        p pVar = new p();
        try {
            pVar.g = jSONObject.optInt("role", pVar.g);
        } catch (Exception e) {
        }
        try {
            pVar.d = a.a(jSONObject.getLong("activetime"));
        } catch (Exception e2) {
        }
        try {
            pVar.c = a.a(jSONObject.getLong("jointime"));
        } catch (Exception e3) {
        }
        try {
            pVar.e = a.a(jSONObject.getLong("msgtime"));
        } catch (Exception e4) {
        }
        pVar.f3034a = jSONObject.getString("momoid");
        pVar.h = new bf();
        w.a(pVar.h, jSONObject);
        return pVar;
    }

    private static bf b(JSONObject jSONObject) {
        String str;
        try {
            str = jSONObject.getString("momoid");
        } catch (Exception e) {
            str = null;
        }
        try {
            String string = jSONObject.getString("avatar");
            String optString = jSONObject.optString("name");
            if (a.a((CharSequence) string) || a.a((CharSequence) str)) {
                return null;
            }
            aq aqVar = new aq();
            aqVar.a(str, string, optString);
            return aqVar.b(str);
        } catch (Exception e2) {
            return null;
        }
    }

    public final int a(String str, n nVar) {
        if (nVar == null || a.a((CharSequence) str)) {
            throw new Exception("discuss=null or id=null");
        }
        JSONObject jSONObject = new JSONObject(a(String.valueOf(g) + "/profile/" + str + "?fr=" + g.q().h, new HashMap()));
        a(nVar, jSONObject);
        com.immomo.momo.service.h hVar = new com.immomo.momo.service.h();
        hVar.a(nVar);
        if (jSONObject.optInt("role") != 0) {
            hVar.a(g.q().h, nVar.f, jSONObject.optInt("role", 3));
            return 1;
        }
        hVar.c(g.q().h, nVar.f);
        return 1;
    }

    public final String a(String str) {
        if (a.a((CharSequence) str)) {
            throw new Exception("name=null or id=null");
        }
        HashMap hashMap = new HashMap();
        hashMap.put("did", str);
        return new JSONObject(a(String.valueOf(g) + "/dismiss?fr=" + g.q().h, hashMap)).optString("msg");
    }

    public final String a(String str, String str2) {
        if (a.a((CharSequence) str2) || a.a((CharSequence) str)) {
            throw new Exception("name=null or id=null");
        }
        HashMap hashMap = new HashMap();
        hashMap.put("did", str);
        hashMap.put("name", str2);
        return new JSONObject(a(String.valueOf(g) + "/edit?fr=" + g.q().h, hashMap)).optString("msg");
    }

    public final String a(String str, List list) {
        if (a.a((CharSequence) str)) {
            throw new Exception("name=null or id=null");
        }
        HashMap hashMap = new HashMap();
        String a2 = a.a(list, ",");
        hashMap.put("did", str);
        hashMap.put("remoteid", a2);
        return new JSONObject(a(String.valueOf(g) + "/remove_member?fr=" + g.q().h, hashMap)).optString("msg");
    }

    public final List b() {
        ArrayList arrayList = new ArrayList();
        JSONObject jSONObject = new JSONObject(a(String.valueOf(g) + "/mylist?fr=" + g.q().h, (Map) null));
        com.immomo.momo.service.h hVar = new com.immomo.momo.service.h();
        JSONArray jSONArray = jSONObject.getJSONArray("lists");
        for (int i = 0; i < jSONArray.length(); i++) {
            JSONObject jSONObject2 = jSONArray.getJSONObject(i);
            n nVar = new n();
            a(nVar, jSONObject2);
            arrayList.add(nVar);
            hVar.a(g.q().h, nVar.f, jSONObject2.optInt("role", 3));
        }
        return arrayList;
    }

    public final List b(String str) {
        if (a.a((CharSequence) str)) {
            throw new Exception("name=null or id=null");
        }
        ArrayList arrayList = new ArrayList();
        HashMap hashMap = new HashMap();
        hashMap.put("did", str);
        JSONArray jSONArray = new JSONObject(a(String.valueOf(g) + "/members?fr=" + g.q().h, hashMap)).getJSONArray("lists");
        for (int i = 0; i < jSONArray.length(); i++) {
            arrayList.add(a(jSONArray.getJSONObject(i)));
        }
        return arrayList;
    }

    public final void b(String str, String str2) {
        HashMap hashMap = new HashMap();
        hashMap.put("invitee", str);
        hashMap.put("did", str2);
        new JSONObject(a(String.valueOf(g) + "/invite?fr=" + g.q().h, hashMap));
    }

    public final String c(String str) {
        if (a.a((CharSequence) str)) {
            throw new Exception("name=null or id=null");
        }
        HashMap hashMap = new HashMap();
        hashMap.put("did", str);
        return new JSONObject(a(String.valueOf(g) + "/quit?fr=" + g.q().h, hashMap)).optString("msg");
    }

    public final n d(String str) {
        HashMap hashMap = new HashMap();
        hashMap.put("invitee", str);
        String str2 = String.valueOf(g) + "/create?fr=" + g.q().h;
        this.e.b((Object) ("invitee: " + str));
        this.e.b((Object) ("url: " + str2));
        JSONObject jSONObject = new JSONObject(a(str2, hashMap));
        n nVar = new n();
        a(nVar, jSONObject);
        return nVar;
    }
}
