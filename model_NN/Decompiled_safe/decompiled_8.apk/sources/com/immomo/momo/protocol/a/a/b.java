package com.immomo.momo.protocol.a.a;

import com.immomo.momo.protocol.a.k;
import java.util.List;
import org.json.JSONObject;

public final class b {

    /* renamed from: a  reason: collision with root package name */
    public int f2881a = 0;
    public int b = 0;
    public List c;

    public b(JSONObject jSONObject) {
        jSONObject.optInt("index");
        jSONObject.optInt("count");
        this.f2881a = jSONObject.optInt("remain");
        this.b = jSONObject.optInt("total");
        k.a();
        this.c = k.a(jSONObject);
    }
}
