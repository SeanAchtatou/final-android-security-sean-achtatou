package com.immomo.momo.protocol.imjson.c;

import android.os.Bundle;
import java.io.Serializable;

public final class c {

    /* renamed from: a  reason: collision with root package name */
    private Bundle f2914a;
    private String b;

    public c(String str) {
        this.f2914a = null;
        this.b = null;
        this.f2914a = new Bundle();
        this.b = str;
    }

    public final Bundle a() {
        return this.f2914a;
    }

    public final Serializable a(String str) {
        return this.f2914a.getSerializable(str);
    }

    public final void a(String str, int i) {
        this.f2914a.putInt(str, i);
    }

    public final void a(String str, Serializable serializable) {
        this.f2914a.putSerializable(str, serializable);
    }

    public final void a(String str, String str2) {
        this.f2914a.putString(str, str2);
    }

    public final String b() {
        return this.b;
    }
}
