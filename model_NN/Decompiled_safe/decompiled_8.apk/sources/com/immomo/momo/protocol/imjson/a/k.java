package com.immomo.momo.protocol.imjson.a;

import android.database.sqlite.SQLiteException;
import android.os.Bundle;
import com.immomo.a.a.d.b;
import com.immomo.a.a.f;
import com.immomo.momo.g;
import com.immomo.momo.service.af;
import org.json.JSONArray;

public final class k implements f {
    public final void a(Object obj, f fVar) {
    }

    public final boolean a(b bVar) {
        JSONArray optJSONArray = bVar.optJSONArray("msgid");
        if (optJSONArray == null) {
            return false;
        }
        if ("read".equals(bVar.optString("st"))) {
            String[] strArr = new String[optJSONArray.length()];
            for (int i = 0; i < optJSONArray.length(); i++) {
                strArr[i] = optJSONArray.getString(i);
            }
            Bundle bundle = new Bundle();
            bundle.putInt("chattype", 1);
            bundle.putString("stype", "msgreaded");
            bundle.putString("remoteuserid", bVar.g());
            bundle.putStringArray("msgid", strArr);
            g.d().a(bundle, "actions.message.status");
            try {
                if (strArr.length == 0) {
                    af.c().b().d(bVar.g());
                } else {
                    af.c().b().a(strArr);
                }
            } catch (SQLiteException e) {
            }
        }
        return true;
    }
}
