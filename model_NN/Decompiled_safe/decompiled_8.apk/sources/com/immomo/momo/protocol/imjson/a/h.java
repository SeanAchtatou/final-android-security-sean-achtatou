package com.immomo.momo.protocol.imjson.a;

import android.os.Bundle;
import android.support.v4.b.a;
import com.baidu.location.LocationClientOption;
import com.immomo.a.a.d.b;
import com.immomo.a.a.f;
import com.immomo.momo.g;
import com.immomo.momo.service.af;
import com.immomo.momo.service.ag;
import com.immomo.momo.service.bean.a.c;
import com.immomo.momo.service.bean.a.d;
import java.util.ArrayList;
import java.util.Date;
import org.json.JSONArray;

public final class h implements f {
    public final void a(Object obj, f fVar) {
    }

    public final boolean a(b bVar) {
        int i = 0;
        String d = bVar.d();
        ag agVar = new ag();
        if (a.a((CharSequence) d) || agVar.j(d)) {
            return true;
        }
        com.immomo.momo.service.bean.a.b bVar2 = new com.immomo.momo.service.bean.a.b();
        bVar2.a(new Date());
        bVar2.d(d);
        bVar2.a(bVar.optString("gid"));
        bVar2.b(bVar.optString("remoteid"));
        if (bVar.h().equals("gradar-notice")) {
            bVar2.a((int) LocationClientOption.MIN_SCAN_SPAN);
            JSONArray optJSONArray = bVar.optJSONArray("groups");
            if (optJSONArray == null) {
                return false;
            }
            String[] strArr = new String[optJSONArray.length()];
            while (i < optJSONArray.length()) {
                strArr[i] = optJSONArray.getString(i);
                i++;
            }
            bVar2.a(strArr);
        } else {
            bVar2.a(bVar.optInt("type"));
            String e = bVar.e();
            if (!a.a((CharSequence) e)) {
                ArrayList arrayList = new ArrayList();
                StringBuilder sb = new StringBuilder(e);
                StringBuilder sb2 = new StringBuilder();
                d.a(sb, sb2, arrayList);
                bVar2.c(sb2.toString());
                bVar2.c(arrayList);
            }
            String optString = bVar.optString("actions");
            if (!a.a((CharSequence) optString)) {
                JSONArray jSONArray = new JSONArray(optString);
                ArrayList arrayList2 = new ArrayList();
                bVar2.b(arrayList2);
                while (i < jSONArray.length()) {
                    c cVar = new c();
                    cVar.a(jSONArray.getString(i));
                    arrayList2.add(cVar);
                    i++;
                }
            }
        }
        agVar.b(bVar2);
        Bundle bundle = new Bundle();
        bundle.putString("msgid", bVar2.m());
        af.c().b(bundle);
        g.d().a(bundle, "actions.groupaction");
        return true;
    }
}
