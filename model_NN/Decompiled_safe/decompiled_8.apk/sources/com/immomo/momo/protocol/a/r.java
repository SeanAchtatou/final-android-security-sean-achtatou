package com.immomo.momo.protocol.a;

import java.util.Arrays;
import javax.net.ssl.SSLException;
import org.apache.http.conn.ssl.AbstractVerifier;

final class r extends AbstractVerifier {
    private r() {
    }

    /* synthetic */ r(byte b) {
        this();
    }

    public final void verify(String str, String[] strArr, String[] strArr2) {
        try {
            verify(str, strArr, strArr2, true);
        } catch (SSLException e) {
            q.f2901a.a((Object) ("host=" + str + ", cns=" + Arrays.toString(strArr) + ", subjectAlts=" + Arrays.toString(strArr2)));
            throw e;
        }
    }
}
