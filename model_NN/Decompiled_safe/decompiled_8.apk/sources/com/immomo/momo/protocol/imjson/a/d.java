package com.immomo.momo.protocol.imjson.a;

import android.os.Bundle;
import android.support.v4.b.a;
import com.immomo.a.a.d.b;
import com.immomo.a.a.f;
import com.immomo.momo.g;
import com.immomo.momo.service.af;
import com.immomo.momo.service.bean.ag;
import com.immomo.momo.service.bean.ah;
import com.immomo.momo.service.bean.ai;
import com.immomo.momo.service.t;
import java.util.ArrayList;
import java.util.Date;
import org.json.JSONArray;

public final class d implements f {
    public final void a(Object obj, f fVar) {
    }

    public final boolean a(b bVar) {
        String d = bVar.d();
        t tVar = new t();
        if (!a.a((CharSequence) d) && !tVar.b(d)) {
            ag agVar = new ag();
            agVar.a(new Date());
            agVar.d(d);
            agVar.b(bVar.optString("remoteid"));
            agVar.a(bVar.optInt("distance", -1));
            agVar.a(bVar.optString("title"));
            String e = bVar.e();
            if (!a.a((CharSequence) e)) {
                ArrayList arrayList = new ArrayList();
                StringBuilder sb = new StringBuilder(e);
                StringBuilder sb2 = new StringBuilder();
                ai.a(sb, sb2, arrayList);
                agVar.c(sb2.toString());
                agVar.b(arrayList);
            }
            String optString = bVar.optString("actions");
            if (!a.a((CharSequence) optString)) {
                JSONArray jSONArray = new JSONArray(optString);
                ArrayList arrayList2 = new ArrayList();
                agVar.a(arrayList2);
                for (int i = 0; i < jSONArray.length(); i++) {
                    ah ahVar = new ah();
                    ahVar.a(jSONArray.getString(i));
                    arrayList2.add(ahVar);
                }
            }
            tVar.b(agVar);
            Bundle bundle = new Bundle();
            bundle.putString("msgid", agVar.j());
            af.c().a(bundle);
            g.d().a(bundle, "actions.frienddiscover");
        }
        return true;
    }
}
