package com.immomo.momo.protocol.imjson.c;

import android.os.Bundle;
import java.util.ArrayList;
import java.util.List;

public abstract class a implements Comparable {

    /* renamed from: a  reason: collision with root package name */
    private List f2912a = new ArrayList(4);
    private int b;

    public a(int i, String... strArr) {
        for (String add : strArr) {
            this.f2912a.add(add);
        }
        this.b = i;
    }

    public final List a() {
        return this.f2912a;
    }

    public abstract boolean a(Bundle bundle, String str);

    public /* synthetic */ int compareTo(Object obj) {
        a aVar = (a) obj;
        if (aVar == null) {
            throw new NullPointerException("the message receiver shouldn't be null!");
        } else if (equals(aVar)) {
            return 0;
        } else {
            return this.b > aVar.b ? -1 : 1;
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        a aVar = (a) obj;
        if (this.f2912a == null) {
            if (aVar.f2912a != null) {
                return false;
            }
        } else if (!this.f2912a.equals(aVar.f2912a)) {
            return false;
        }
        return this.b == aVar.b;
    }

    public int hashCode() {
        return (((this.f2912a == null ? 0 : this.f2912a.hashCode()) + 31) * 31) + this.b;
    }

    public String toString() {
        return "AbsMessageReceiver [actions=" + this.f2912a + ", priority=" + this.b + "]";
    }
}
