package com.immomo.momo.protocol.imjson.a;

import android.os.Bundle;
import com.immomo.a.a.a;
import com.immomo.a.a.d.b;
import com.immomo.a.a.f;
import com.immomo.momo.g;
import com.immomo.momo.service.af;
import com.sina.sdk.api.message.InviteApi;

public final class s implements f {

    /* renamed from: a  reason: collision with root package name */
    private a f2910a = null;

    public s(a aVar) {
        this.f2910a = aVar;
    }

    public final void a(Object obj, f fVar) {
    }

    public final boolean a(b bVar) {
        Bundle bundle = new Bundle();
        if (bVar.has("newnotice")) {
            int optInt = bVar.optInt("newnotice");
            bundle.putInt("tiebareport", optInt);
            af.c().d(optInt);
        }
        if (bVar.has("feed")) {
            String optString = bVar.getJSONObject("feed").optString(InviteApi.KEY_TEXT);
            bundle.putString("content", optString);
            af.c().b(optString);
        }
        af.c().b(bundle);
        g.d().a(bundle, "actions.tiebareport");
        com.immomo.a.a.d.a aVar = new com.immomo.a.a.d.a();
        aVar.a((Object) bVar.a());
        aVar.b(bVar.d());
        aVar.put("ns", bVar.h());
        this.f2910a.a(aVar);
        return true;
    }
}
