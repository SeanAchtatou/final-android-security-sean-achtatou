package com.immomo.momo.protocol.a.a;

import com.immomo.momo.protocol.a.k;
import com.immomo.momo.service.bean.ab;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class d {

    /* renamed from: a  reason: collision with root package name */
    public int f2883a = 0;
    public ArrayList b = new ArrayList();

    public d(JSONObject jSONObject) {
        jSONObject.optInt("index");
        jSONObject.optInt("count");
        this.f2883a = jSONObject.optInt("remain");
        a(jSONObject);
    }

    private void a(JSONObject jSONObject) {
        JSONArray jSONArray;
        try {
            jSONArray = jSONObject.getJSONArray("feeds");
        } catch (JSONException e) {
            jSONArray = null;
        }
        if (jSONArray != null && jSONArray.length() > 0) {
            for (int i = 0; i < jSONArray.length(); i++) {
                try {
                    ab abVar = new ab();
                    k.a();
                    k.a(jSONArray.getJSONObject(i), abVar);
                    this.b.add(abVar);
                } catch (JSONException e2) {
                }
            }
        }
    }
}
