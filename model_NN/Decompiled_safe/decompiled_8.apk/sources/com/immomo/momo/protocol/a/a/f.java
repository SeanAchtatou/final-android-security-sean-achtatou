package com.immomo.momo.protocol.a.a;

import com.amap.mapapi.poisearch.PoiTypeDef;
import org.json.JSONObject;

public final class f {

    /* renamed from: a  reason: collision with root package name */
    public String f2885a = PoiTypeDef.All;
    public int b = 0;

    public f(JSONObject jSONObject) {
        this.f2885a = jSONObject.optString("msg");
        this.b = jSONObject.optInt("balance", 0);
    }
}
