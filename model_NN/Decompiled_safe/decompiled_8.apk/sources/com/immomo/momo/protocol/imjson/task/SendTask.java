package com.immomo.momo.protocol.imjson.task;

import android.os.Parcel;
import com.immomo.a.a.a;
import com.immomo.momo.util.m;

public abstract class SendTask implements Task {
    /* access modifiers changed from: protected */
    public m c = new m(getClass().getSimpleName());
    public g d;

    public SendTask(g gVar) {
        this.d = gVar;
    }

    public abstract boolean a(a aVar);

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
    }
}
