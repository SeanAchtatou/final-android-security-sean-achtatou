package com.immomo.momo.protocol.a;

import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.android.pay.ai;
import com.immomo.momo.android.pay.at;
import com.immomo.momo.android.pay.au;
import com.immomo.momo.android.pay.bk;
import com.immomo.momo.protocol.a.a.f;
import com.immomo.momo.service.bean.an;
import com.immomo.momo.service.bean.b;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.service.bean.bg;
import com.sina.sdk.api.message.InviteApi;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

public final class a extends p {
    private static a f = null;
    private static String g = (String.valueOf(b) + "/account");
    private static String h = (String.valueOf(b) + "/gold");
    private static String i = (String.valueOf(b) + "/vip");

    public static ai a(String str) {
        JSONObject jSONObject = new JSONObject(str);
        ai aiVar = new ai();
        aiVar.c = new ArrayList();
        JSONObject jSONObject2 = jSONObject.getJSONObject("data");
        aiVar.f2528a = jSONObject2.optInt("version");
        aiVar.b = jSONObject2.optString("banner");
        if (jSONObject2.has("list")) {
            JSONArray jSONArray = jSONObject2.getJSONArray("list");
            for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                JSONObject jSONObject3 = jSONArray.getJSONObject(i2);
                an anVar = new an();
                anVar.f2992a = jSONObject3.optString("title");
                anVar.d = 1;
                aiVar.c.add(anVar);
                JSONArray jSONArray2 = new JSONArray(jSONObject3.optString("item"));
                for (int i3 = 0; i3 < jSONArray2.length(); i3++) {
                    List list = aiVar.c;
                    JSONObject jSONObject4 = jSONArray2.getJSONObject(i3);
                    an anVar2 = new an();
                    anVar2.d = 2;
                    anVar2.f2992a = jSONObject4.optString("name");
                    anVar2.b = jSONObject4.optString("icon");
                    anVar2.c = jSONObject4.optString(InviteApi.KEY_URL);
                    anVar2.e = jSONObject4.optInt("new", 0) == 1;
                    list.add(anVar2);
                }
            }
        }
        return aiVar;
    }

    public static a a() {
        if (f == null) {
            f = new a();
        }
        return f;
    }

    private static void a(JSONArray jSONArray, List list) {
        for (int i2 = 0; i2 < jSONArray.length(); i2++) {
            JSONObject jSONObject = jSONArray.getJSONObject(i2);
            au auVar = new au();
            auVar.c = jSONObject.optString("subject");
            auVar.d = jSONObject.optString("body");
            auVar.b = jSONObject.optDouble("total_fee");
            auVar.f2539a = jSONObject.optString("product_id");
            auVar.e = jSONObject.optInt("default", 0) == 1;
            auVar.f = jSONObject.optString("promotion", PoiTypeDef.All);
            list.add(auVar);
        }
    }

    private static void b(JSONArray jSONArray, List list) {
        for (int i2 = 0; i2 < jSONArray.length(); i2++) {
            JSONObject jSONObject = jSONArray.getJSONObject(i2);
            at atVar = new at();
            atVar.f2538a = jSONObject.optInt("default", 0) == 1;
            atVar.c = jSONObject.optInt("type");
            atVar.b = jSONObject.optString("name");
            atVar.d = jSONObject.optString("pay_address");
            atVar.e = jSONObject.optInt("cashier", -1);
            list.add(atVar);
        }
    }

    public final long a(List list, List list2, List list3, List list4, List list5, long j) {
        JSONObject jSONObject = new JSONObject(a(String.valueOf(h) + "/listsandroid", new HashMap()));
        long optLong = jSONObject.optLong("balance", j);
        JSONObject optJSONObject = jSONObject.optJSONObject("goldlist");
        JSONArray optJSONArray = optJSONObject.optJSONArray("pay_type");
        if (optJSONArray != null) {
            b(optJSONArray, list);
        }
        JSONArray optJSONArray2 = optJSONObject.optJSONArray("pay_tier");
        if (optJSONArray2 != null) {
            a(optJSONArray2, list2);
        }
        JSONArray optJSONArray3 = optJSONObject.optJSONArray("pay_credit");
        if (optJSONArray3 != null) {
            a(optJSONArray3, list3);
        }
        JSONArray optJSONArray4 = optJSONObject.optJSONArray("pay_debit");
        if (optJSONArray4 != null) {
            a(optJSONArray4, list4);
        }
        JSONArray optJSONArray5 = optJSONObject.optJSONArray("pay_union");
        if (optJSONArray5 != null) {
            a(optJSONArray5, list5);
        }
        return optLong;
    }

    public final f a(String str, String str2, String str3) {
        String str4 = String.valueOf(i) + "/androidgift";
        HashMap hashMap = new HashMap();
        hashMap.put("remoteid", str2);
        hashMap.put("product_id", str);
        hashMap.put("in_trade_no", str3);
        return new f(new JSONObject(a(str4, hashMap)));
    }

    public final bg a(List list, List list2, List list3, List list4, List list5, List list6, long j, boolean z) {
        String str = String.valueOf(i) + "/listsandroid";
        HashMap hashMap = new HashMap();
        hashMap.put("is_buy", new StringBuilder(String.valueOf(z ? 1 : 0)).toString());
        JSONObject jSONObject = new JSONObject(a(str, hashMap));
        JSONObject optJSONObject = jSONObject.optJSONObject("viplist");
        JSONArray optJSONArray = optJSONObject.optJSONArray("pay_type");
        if (optJSONArray != null) {
            b(optJSONArray, list);
        }
        JSONArray optJSONArray2 = optJSONObject.optJSONArray("pay_by_gold");
        if (optJSONArray2 != null) {
            a(optJSONArray2, list2);
        }
        JSONArray optJSONArray3 = optJSONObject.optJSONArray("pay_by_money");
        if (optJSONArray3 != null) {
            a(optJSONArray3, list3);
        }
        JSONArray optJSONArray4 = optJSONObject.optJSONArray("pay_by_credit");
        if (optJSONArray4 != null) {
            a(optJSONArray4, list4);
        }
        JSONArray optJSONArray5 = optJSONObject.optJSONArray("pay_by_debit");
        if (optJSONArray5 != null) {
            a(optJSONArray5, list5);
        }
        JSONArray optJSONArray6 = optJSONObject.optJSONArray("pay_by_unipay");
        if (optJSONArray6 != null) {
            a(optJSONArray6, list6);
        }
        bg bgVar = new bg();
        bgVar.b = jSONObject.optLong("balance", j);
        if (jSONObject.has("vip")) {
            JSONObject optJSONObject2 = jSONObject.optJSONObject("vip");
            bgVar.c = optJSONObject2.optLong("expire");
            bgVar.d = optJSONObject2.optInt("level");
            bgVar.e = optJSONObject2.optInt("remind", 0) == 1;
        }
        return bgVar;
    }

    public final String a(ai aiVar) {
        String str = String.valueOf(i) + "/config/vipcenter";
        HashMap hashMap = new HashMap();
        hashMap.put("version", aiVar == null ? "0" : String.valueOf(aiVar.f2528a));
        return a(str, hashMap);
    }

    public final String a(bk bkVar, String str, String str2, String str3, Map map) {
        map.put("mac", str);
        map.put("imei", str2);
        map.put("channelid", str3);
        JSONObject jSONObject = new JSONObject(a(String.valueOf(g) + "/unipay/sign", map));
        JSONObject optJSONObject = jSONObject.optJSONObject("data");
        if (optJSONObject != null) {
            bkVar.f2554a = optJSONObject.getString("cpCode");
            bkVar.b = optJSONObject.getString("orderid");
            bkVar.c = optJSONObject.getString("vacCode");
            bkVar.e = optJSONObject.getString("cpid");
            bkVar.f = optJSONObject.getString("notifyUrl");
            bkVar.g = optJSONObject.getString("money");
            bkVar.h = optJSONObject.getString("company");
            bkVar.i = optJSONObject.getString("sign");
            bkVar.j = optJSONObject.getString("phone");
            bkVar.k = optJSONObject.getString("game");
            bkVar.l = optJSONObject.getString("appid");
            bkVar.d = optJSONObject.getString("vacMode");
        }
        return jSONObject.optString("em");
    }

    public final String a(String str, bf bfVar, String str2) {
        boolean z = true;
        HashMap hashMap = new HashMap();
        hashMap.put("product_id", str);
        hashMap.put("in_trade_no", str2);
        JSONObject jSONObject = new JSONObject(a(String.valueOf(i) + "/androidbuy", hashMap));
        bfVar.b(jSONObject.optLong("balance", bfVar.r()));
        JSONObject optJSONObject = jSONObject.optJSONObject("vip");
        if (optJSONObject != null) {
            bfVar.aj = android.support.v4.b.a.a(optJSONObject.optLong("expire"));
            bfVar.ai = optJSONObject.optInt("level", bfVar.ai);
            if (optJSONObject.optInt("remind", 0) != 1) {
                z = false;
            }
            bfVar.ad = z;
        }
        return jSONObject.optString("msg", PoiTypeDef.All);
    }

    public final String a(String str, bg bgVar) {
        boolean z = true;
        HashMap hashMap = new HashMap();
        hashMap.put("orderid", str);
        JSONObject jSONObject = new JSONObject(a(String.valueOf(g) + "/unipay/check", hashMap));
        JSONObject optJSONObject = jSONObject.optJSONObject("data");
        if (optJSONObject != null) {
            bgVar.f3012a = optJSONObject.optInt("success", 0) == 1;
            bgVar.b = (long) optJSONObject.optInt("balance", 0);
            bgVar.c = optJSONObject.optLong("expire");
            bgVar.d = optJSONObject.optInt("level");
            if (optJSONObject.optInt("remind", 0) != 1) {
                z = false;
            }
            bgVar.e = z;
        }
        return jSONObject.optString("em");
    }

    public final String a(Map map) {
        return new JSONObject(a(String.valueOf(g) + "/alipay/sign", map)).optString("sign_url");
    }

    public final void a(Map map, bg bgVar) {
        boolean z = true;
        JSONObject jSONObject = new JSONObject(a(String.valueOf(g) + "/alipay/charge", map));
        if (jSONObject.optInt("success", 0) != 1) {
            z = false;
        }
        bgVar.f3012a = z;
        jSONObject.optInt("verify", 0);
        bgVar.b = jSONObject.optLong("balance");
        bgVar.f = jSONObject.optString("msg", PoiTypeDef.All);
    }

    public final b b(String str, String str2, String str3) {
        b bVar = new b();
        HashMap hashMap = new HashMap();
        hashMap.put("momoid", str);
        hashMap.put("auth_code", str3);
        hashMap.put("alipay_user_id", str2);
        JSONObject optJSONObject = new JSONObject(a(String.valueOf(g) + "/alipay/isbind", hashMap)).optJSONObject("data");
        bVar.d = optJSONObject.optBoolean("is_bind_momo", false);
        bVar.f = optJSONObject.optBoolean("is_current_momo", false);
        bVar.e = optJSONObject.optBoolean("is_bind_phone", false);
        if (optJSONObject.has("user")) {
            JSONObject optJSONObject2 = optJSONObject.optJSONObject("user");
            bVar.b = optJSONObject2.optString("sex", PoiTypeDef.All);
            bVar.f3003a = optJSONObject2.optString("phone", PoiTypeDef.All);
            bVar.c = optJSONObject2.optString("email", PoiTypeDef.All);
        }
        return bVar;
    }

    public final String b() {
        return new JSONObject(a(String.valueOf(g) + "/gettradeno", new HashMap())).optString("tradeno", PoiTypeDef.All);
    }

    public final String b(Map map) {
        JSONObject optJSONObject = new JSONObject(a(String.valueOf(g) + "/alipay/wapsign", map)).optJSONObject("data");
        if (optJSONObject != null) {
            return optJSONObject.optString("sign_url");
        }
        return null;
    }

    public final void b(Map map, bg bgVar) {
        boolean z = true;
        JSONObject jSONObject = new JSONObject(a(String.valueOf(g) + "/alipay/chargeandbuy", map));
        bgVar.f3012a = jSONObject.optInt("success", 0) == 1;
        jSONObject.optInt("verify", 0);
        bgVar.b = jSONObject.optLong("balance");
        if (jSONObject.has("vip")) {
            JSONObject optJSONObject = jSONObject.optJSONObject("vip");
            bgVar.c = optJSONObject.optLong("expire");
            bgVar.d = optJSONObject.optInt("level");
            if (optJSONObject.optInt("remind", 0) != 1) {
                z = false;
            }
            bgVar.e = z;
        }
        bgVar.f = jSONObject.optString("msg", PoiTypeDef.All);
    }

    public final f c(Map map, bg bgVar) {
        boolean z = true;
        JSONObject jSONObject = new JSONObject(a(String.valueOf(g) + "/alipay/chargeandgift", map));
        if (jSONObject.optInt("success", 0) != 1) {
            z = false;
        }
        bgVar.f3012a = z;
        jSONObject.optInt("verify", 0);
        bgVar.b = jSONObject.optLong("balance");
        if (jSONObject.has("vip")) {
            JSONObject optJSONObject = jSONObject.optJSONObject("vip");
            bgVar.c = optJSONObject.optLong("expire");
            bgVar.d = optJSONObject.optInt("level");
        }
        bgVar.f = jSONObject.optString("msg", PoiTypeDef.All);
        return new f(jSONObject);
    }

    public final void c(String str, String str2, String str3) {
        HashMap hashMap = new HashMap();
        hashMap.put("momoid", str);
        hashMap.put("alipay_user_id", str2);
        hashMap.put("etype", "2");
        hashMap.put("password", str3);
        a(String.valueOf(g) + "/alipay/bind", hashMap);
    }
}
