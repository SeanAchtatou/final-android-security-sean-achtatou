package com.immomo.momo.protocol.imjson;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.a.a.c;
import com.immomo.a.a.d.b;
import com.immomo.a.a.f;
import com.immomo.momo.R;
import com.immomo.momo.a;
import com.immomo.momo.android.activity.KickOffActivity;
import com.immomo.momo.android.service.XService;
import com.immomo.momo.util.jni.Codec;
import com.immomo.momo.util.k;
import com.immomo.momo.util.m;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import org.json.JSONArray;
import org.json.JSONException;

public final class g implements c, f {

    /* renamed from: a  reason: collision with root package name */
    boolean f2921a = false;
    /* access modifiers changed from: private */
    public Handler b = null;
    /* access modifiers changed from: private */
    public XService c = null;
    private Context d = null;
    /* access modifiers changed from: private */
    public m e = new m(this);
    private j f = null;
    private i[] g = {i.LEVEL_1, i.LEVEL_2, i.LEVEL_3, i.LEVEL_5, i.LEVEL_6};
    private int h = 0;
    /* access modifiers changed from: private */
    public int i = 0;
    private long j;
    private long k;
    /* access modifiers changed from: private */
    public boolean l = true;
    /* access modifiers changed from: private */
    public boolean m = false;
    /* access modifiers changed from: private */
    public boolean n = false;
    /* access modifiers changed from: private */
    public boolean o = true;
    /* access modifiers changed from: private */
    public final String p;
    /* access modifiers changed from: private */
    public final String q;
    private final String r;
    private String s = PoiTypeDef.All;
    private String t = PoiTypeDef.All;
    private BlockingQueue u = new LinkedBlockingQueue();

    public g(XService xService) {
        this.c = xService;
        this.d = xService;
        this.p = this.d.getString(R.string.xmpp_conninfo_xmpp_disconnected);
        this.q = this.d.getString(R.string.xmpp_conninfo_network_disconnected);
        this.r = this.d.getString(R.string.xmpp_conninfo_xmpp_authfailed);
        this.o = com.immomo.momo.g.y();
        this.f = new j(this, (byte) 0);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        this.d.registerReceiver(this.f, intentFilter);
        this.c.b.e("set").a("ap", this);
        this.b = new h(this, this.d.getMainLooper());
        l.f2926a = true;
        l.c = false;
    }

    /* access modifiers changed from: private */
    public void a(int i2) {
        Bundle bundle = new Bundle();
        if (i2 == 1) {
            this.n = false;
            bundle.putInt("imwtype", i2);
        } else if (i2 == 2) {
            this.m = false;
            bundle.putInt("imwtype", i2);
            this.l = !this.n;
            if (!this.l) {
                this.s = this.p;
                this.t = "XMPP_TIMEOUT";
                l.f2926a = false;
                l.b = this.p;
                a(this.s, this.t);
                return;
            }
            l.f2926a = true;
            l.b = PoiTypeDef.All;
        } else {
            bundle.putInt("imwtype", 3);
        }
        com.immomo.momo.g.d().a(bundle, "actions.removeimjwarning");
    }

    /* access modifiers changed from: private */
    public void a(String str, String str2) {
        this.s = str;
        this.t = str2;
        Bundle bundle = new Bundle();
        bundle.putString("imwmsg", str);
        bundle.putString("imwtype", str2);
        com.immomo.momo.g.d().a(bundle, "actions.imjwarning");
        this.l = false;
        l.f2926a = false;
        l.b = str;
        this.c.e();
        this.e.a((Object) ("warning!!!!!!, type=" + str2 + ", warn=" + str));
        if (str2.equals("NET_DISCONNECTED")) {
            new k("U", "U21").e();
        } else {
            new k("U", "U4").e();
        }
    }

    private void k() {
        this.l = true;
        this.s = PoiTypeDef.All;
        this.t = PoiTypeDef.All;
        this.n = false;
        this.m = false;
        l();
        this.b.removeMessages(147);
        this.k = 0;
        this.j = 0;
        this.i = 0;
    }

    /* access modifiers changed from: private */
    public void l() {
        this.b.removeMessages(149);
        for (int i2 = 0; i2 <= this.h; i2++) {
            this.g[i2].b();
        }
        this.h = 0;
    }

    public final void a() {
        this.c.a(true);
    }

    public final void a(int i2, b bVar) {
        if (i2 == 410) {
            try {
                if (a(bVar.getJSONArray("ap"))) {
                    this.c.a(false);
                    this.c.c();
                }
            } catch (JSONException e2) {
                this.e.a((Throwable) e2);
            }
        } else if (i2 == 405 || i2 == 409) {
            com.immomo.momo.g.d().l();
            this.c.b();
            Intent intent = new Intent(this.d, KickOffActivity.class);
            intent.putExtra("message", bVar.optString("em"));
            intent.setFlags(268435456);
            this.d.startActivity(intent);
        } else {
            this.c.a(true);
        }
    }

    public final void a(Object obj, f fVar) {
    }

    public final boolean a(b bVar) {
        if (!"ap".equals(bVar.h()) || !a(bVar.getJSONArray("list"))) {
            return true;
        }
        this.c.a(false);
        this.c.c();
        return true;
    }

    public final boolean a(JSONArray jSONArray) {
        this.u.clear();
        for (int i2 = 0; i2 < jSONArray.length(); i2++) {
            try {
                String[] split = jSONArray.getString(i2).split(":");
                this.u.put(new o(split[0], Integer.parseInt(split[1])));
            } catch (Exception e2) {
                this.e.a((Throwable) e2);
            }
        }
        if (this.u.size() <= 0) {
            return false;
        }
        o oVar = (o) this.u.poll();
        this.c.b(oVar.f2927a, oVar.b);
        this.c.a(oVar.f2927a, oVar.b);
        return true;
    }

    public final void b() {
        k();
        if (this.f != null) {
            this.d.unregisterReceiver(this.f);
            this.f = null;
        }
    }

    public final void b(b bVar) {
        if (bVar.has("ap")) {
            try {
                a(bVar.getJSONArray("ap"));
            } catch (Exception e2) {
                this.e.a((Throwable) e2);
            }
        }
    }

    public final void c() {
        k();
        a(1);
        this.f2921a = false;
    }

    public final void d() {
        if (this.o && !this.f2921a) {
            int i2 = this.h;
            while (true) {
                int i3 = this.i;
                this.i = i3 + 1;
                if (i3 == 0) {
                    this.j = System.currentTimeMillis();
                    this.b.sendEmptyMessageDelayed(147, 360000);
                    l.f = 0;
                } else {
                    if (!this.b.hasMessages(147)) {
                        this.b.sendEmptyMessageDelayed(147, 360000);
                    }
                    this.k = System.currentTimeMillis() - this.j;
                    l.f = this.k / 1000;
                }
                l.g = this.i;
                if (this.i > 0 && this.i % 2 == 0) {
                    if (!a.f669a.equals(this.c.f()) || a.b != this.c.g()) {
                        o oVar = (o) this.u.poll();
                        if (oVar != null) {
                            this.c.a(oVar.f2927a, oVar.b);
                        } else {
                            this.c.a(a.f669a, a.b);
                        }
                    } else {
                        this.c.a(Codec.iiou(1), a.b);
                        o a2 = this.c.a();
                        if (a2 != null) {
                            try {
                                this.u.put(a2);
                            } catch (InterruptedException e2) {
                            }
                        }
                    }
                }
                i iVar = this.g[this.h];
                if (iVar.a()) {
                    this.b.sendEmptyMessageDelayed(149, (long) i.a(iVar));
                    return;
                }
                this.h++;
                if (this.h >= this.g.length) {
                    this.h = this.g.length - 1;
                    this.g[this.h].b();
                }
                int i4 = this.h;
            }
        }
    }

    public final void e() {
        k();
        this.f2921a = true;
        a(this.r, "XMPP_AUTHFAILED");
    }

    public final void f() {
        k();
        this.f2921a = false;
    }

    public final void g() {
        this.f2921a = false;
        if (this.h > 0) {
            l();
        }
    }

    public final boolean h() {
        return this.l;
    }

    public final String i() {
        return this.s;
    }

    public final String j() {
        return this.t;
    }
}
