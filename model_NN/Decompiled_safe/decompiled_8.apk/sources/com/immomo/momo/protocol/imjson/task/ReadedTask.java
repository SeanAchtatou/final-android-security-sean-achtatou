package com.immomo.momo.protocol.imjson.task;

import com.immomo.a.a.a;
import com.immomo.a.a.d.c;
import com.immomo.a.a.f.b;
import com.immomo.momo.protocol.imjson.p;
import com.immomo.momo.service.a.ay;
import java.io.Serializable;
import org.json.JSONObject;

public class ReadedTask extends SendTask {

    /* renamed from: a  reason: collision with root package name */
    private String f2937a;
    private String[] b;
    private String e;
    private int f;

    public ReadedTask() {
        super(g.AsyncExpress);
        this.f2937a = null;
        this.b = null;
        this.e = null;
        this.e = b.a();
    }

    public ReadedTask(String str, String[] strArr, int i) {
        this();
        this.f2937a = str;
        this.b = strArr;
        this.f = i;
    }

    public final void a(String str) {
        this.e = str;
    }

    public final boolean a(a aVar) {
        try {
            c cVar = new c(aVar, this.e);
            switch (this.f) {
                case 1:
                    cVar.a("msgst");
                    break;
                case 2:
                    cVar.a("gmsgst");
                    break;
                case 3:
                    cVar.a("dmsgst");
                    break;
            }
            cVar.d(this.f2937a);
            if (this.f == 3 || this.f == 2) {
                cVar.a(new String[0]);
            } else {
                if (this.b == null) {
                    this.b = new String[0];
                }
                cVar.a(this.b);
            }
            cVar.f("read");
            cVar.j();
            return true;
        } catch (Exception e2) {
            return false;
        }
    }

    public final void b(String str) {
        boolean z = false;
        JSONObject jSONObject = new JSONObject(str);
        this.f2937a = jSONObject.getString("remoteid");
        this.b = android.support.v4.b.a.b(jSONObject.getString("msgids"), ",");
        if (jSONObject.has("isgroup")) {
            if (jSONObject.optInt("isgroup", 0) == 1) {
                z = true;
            }
            if (z) {
                this.f = 2;
            } else {
                this.f = 1;
            }
        } else {
            this.f = jSONObject.optInt("chattype", 1);
        }
    }

    public final String c() {
        return this.e;
    }

    public final String d() {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("remoteid", this.f2937a);
        jSONObject.put("msgids", android.support.v4.b.a.a(this.b, ","));
        jSONObject.put("chattype", this.f);
        return jSONObject.toString();
    }

    public final void g_() {
        ay.g().b((Serializable) this.e);
    }

    public final void h_() {
        if (p.a()) {
            p.a(this);
        }
    }
}
