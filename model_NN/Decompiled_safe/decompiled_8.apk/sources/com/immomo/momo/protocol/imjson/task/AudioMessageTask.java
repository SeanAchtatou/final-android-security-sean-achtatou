package com.immomo.momo.protocol.imjson.task;

import android.support.v4.b.a;
import com.immomo.a.a.d.i;
import com.immomo.momo.service.bean.Message;
import java.io.File;
import java.net.URI;
import java.util.Timer;
import java.util.TimerTask;

public class AudioMessageTask extends MessageTask {

    /* renamed from: a  reason: collision with root package name */
    protected File f2933a = null;
    int b = 0;
    /* access modifiers changed from: private */
    public boolean e = false;
    /* access modifiers changed from: private */
    public Object f = new Object();
    /* access modifiers changed from: private */
    public TimerTask g;
    /* access modifiers changed from: private */
    public Timer h = new Timer();

    public AudioMessageTask(Message message) {
        super(g.SuccessionLongtime, message);
    }

    private boolean a(Message message) {
        boolean z = false;
        if (this.f2933a == null && message.fileName.startsWith("file://")) {
            this.f2933a = new File(URI.create(message.fileName));
        }
        boolean z2 = this.f2933a != null;
        if (!z2) {
            z = true;
        }
        this.e = z;
        if (z2) {
            if (this.g != null) {
                this.g.cancel();
                this.g = null;
                this.h.purge();
            }
            this.g = new b(this);
            this.h.schedule(this.g, 120000);
            new a(this, message).start();
            try {
                synchronized (this.f) {
                    this.f.wait();
                }
            } catch (InterruptedException e2) {
                this.c.a((Throwable) e2);
            }
        }
        return this.e;
    }

    /* access modifiers changed from: protected */
    public final void a(Message message, i iVar) {
        if (!a(message)) {
            throw new Exception("audio upload failed");
        }
        iVar.c(a.a(a.a(a.a(a.a(a.a(String.valueOf(com.immomo.momo.a.f) + "/chataudio", "mode", "GUID"), "filesize", new StringBuilder(String.valueOf(message.fileSize)).toString()), "file", message.fileName), "audiotime", new StringBuilder(String.valueOf(message.audiotime)).toString()), "ext", message.expandedName));
    }

    public final boolean a(com.immomo.a.a.a aVar) {
        e();
        return super.a(aVar);
    }
}
