package com.immomo.momo.protocol.imjson;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

final class h extends Handler {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ g f2922a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    h(g gVar, Looper looper) {
        super(looper);
        this.f2922a = gVar;
    }

    public final void handleMessage(Message message) {
        switch (message.what) {
            case 147:
                this.f2922a.n = true;
                this.f2922a.a(this.f2922a.p, "XMPP_TIMEOUT");
                return;
            case 148:
                this.f2922a.m = true;
                this.f2922a.a(this.f2922a.q, "NET_DISCONNECTED");
                return;
            case 149:
                this.f2922a.c.c();
                return;
            default:
                return;
        }
    }
}
