package com.immomo.momo.protocol.a;

import android.support.v4.b.a;
import com.amap.mapapi.location.LocationManagerProxy;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.g;
import com.immomo.momo.protocol.a.a.c;
import com.immomo.momo.protocol.a.a.d;
import com.immomo.momo.protocol.a.a.m;
import com.immomo.momo.service.aq;
import com.immomo.momo.service.bean.GameApp;
import com.immomo.momo.service.bean.ab;
import com.immomo.momo.service.bean.ac;
import com.immomo.momo.service.bean.ae;
import com.immomo.momo.service.bean.ay;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.service.bean.u;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

public final class k extends p {
    private static String A = "loctype";
    private static String B = "remain";
    private static String C = "remoteid";
    private static String D = "user";
    private static String E = "content";
    private static String F = "srcid";
    private static String G = "srctype";
    private static String H = "replytype";
    private static String I = "content_type";
    private static String J = "tomomoid";
    private static String K = "toname";
    private static String L = "commentid";
    private static String M = "replycontent";
    private static String N = "emotion_library";
    private static String O = "emotion_name";
    private static String P = "emotion_body";
    private static String Q = "resource";
    private static String R = "title";
    private static String S = "icon";
    private static String T = "actions";
    private static String U = "desc";
    private static String V = "app";
    private static String W = "appid";
    private static String X = "appname";
    private static String Y = "appicon";
    private static String Z = "appdesc";
    private static String aa = "appstore";
    private static String ab = "actions";
    private static String ac = "appsumm";
    private static String ad = "event";
    private static String ae = "eventid";
    private static String af = "name";
    private static String ag = "address";
    private static String ah = "time";
    private static String ai = "actions";
    private static String aj = "mcount";
    private static String ak = "from";
    private static k f = null;
    private static String g = (String.valueOf(b) + "/feed");
    private static String h = "sync_sina";
    private static String i = "sync_renren";
    private static String j = "sync_qqwb";
    private static String k = "sync_weixin";
    private static String l = "sid";
    private static String m = "site";
    private static String n = "feed";
    private static String o = "feeds";
    private static String p = "name";
    private static String q = "msg";
    private static String r = "feedid";
    private static String s = "create_time";
    private static String t = "pics";
    private static String u = "owner";
    private static String v = "comment_count";
    private static String w = "distance";
    private static String x = "total_visit";
    private static String y = "total_feed";
    private static String z = "recent_visit";

    public static k a() {
        if (f == null) {
            f = new k();
        }
        return f;
    }

    public static List a(JSONObject jSONObject) {
        ArrayList arrayList = new ArrayList();
        JSONArray jSONArray = jSONObject.getJSONArray("comments");
        if (jSONArray != null && jSONArray.length() > 0) {
            for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                arrayList.add(b(jSONArray.getJSONObject(i2)));
            }
        }
        return arrayList;
    }

    public static void a(JSONObject jSONObject, ab abVar) {
        abVar.c = jSONObject.optString(u);
        abVar.h = jSONObject.optString(r);
        abVar.e = jSONObject.optString(l);
        abVar.i = jSONObject.optInt(LocationManagerProxy.KEY_STATUS_CHANGED);
        abVar.j = jSONObject.optString(O);
        abVar.k = jSONObject.optString(N);
        abVar.a(jSONObject.optString(P));
        abVar.a(a.a(jSONObject.optLong(s)));
        abVar.b(jSONObject.optString(E));
        abVar.g = jSONObject.optInt(v);
        abVar.a((float) jSONObject.optInt(w, -1));
        JSONArray optJSONArray = jSONObject.optJSONArray(t);
        if (optJSONArray != null && optJSONArray.length() > 0) {
            String[] strArr = new String[optJSONArray.length()];
            for (int i2 = 0; i2 < optJSONArray.length(); i2++) {
                strArr[i2] = optJSONArray.getString(i2);
            }
            abVar.a(strArr);
        }
        if (jSONObject.has(D)) {
            JSONObject optJSONObject = jSONObject.optJSONObject(D);
            abVar.b = new aq().b(optJSONObject.optString("momoid", PoiTypeDef.All));
            if (abVar.b == null) {
                abVar.b = new bf();
            }
            abVar.b.X = -1;
            w.a(abVar.b, optJSONObject);
        }
        if (jSONObject.has(m)) {
            abVar.d = new ay();
            ay ayVar = abVar.d;
            JSONObject jSONObject2 = jSONObject.getJSONObject(m);
            ayVar.f3001a = jSONObject2.optString(l, ayVar.f3001a);
            ayVar.a((float) jSONObject2.optLong(w, -1));
            ayVar.f = jSONObject2.optString(p, ayVar.f);
            ayVar.d = jSONObject2.optInt("type", ayVar.d);
            ayVar.j = jSONObject2.optInt(x, ayVar.j);
            ayVar.l = jSONObject2.optInt(y, ayVar.l);
            ayVar.k = jSONObject2.optInt(z, ayVar.k);
            ayVar.o = jSONObject2.optInt(LocationManagerProxy.KEY_STATUS_CHANGED, ayVar.o);
        }
        if (jSONObject.has(V)) {
            JSONObject jSONObject3 = jSONObject.getJSONObject(V);
            if (abVar.n == null) {
                abVar.n = new GameApp();
            }
            GameApp gameApp = abVar.n;
            gameApp.appid = jSONObject3.getString(W);
            gameApp.appname = jSONObject3.optString(X);
            gameApp.appicon = jSONObject3.optString(Y);
            gameApp.appdesc = jSONObject3.optString(Z);
            gameApp.appdownload = jSONObject3.optString(aa);
            gameApp.appdesc = jSONObject3.optString(ac);
            gameApp.action = l(jSONObject3.optString(ab));
        } else {
            abVar.n = null;
        }
        if (jSONObject.has(ad)) {
            JSONObject jSONObject4 = jSONObject.getJSONObject(ad);
            if (abVar.o == null) {
                abVar.o = new u();
            }
            abVar.o.f3038a = jSONObject4.getString(ae);
            abVar.o.b = jSONObject4.optString(af);
            abVar.o.e = jSONObject4.optString(ag);
            abVar.o.d = jSONObject4.optString(ah);
            abVar.o.c(jSONObject4.optInt(aj));
            String optString = jSONObject4.optString(ai);
            abVar.o.w = l(optString);
        } else {
            abVar.o = null;
        }
        if (jSONObject.has(Q)) {
            JSONObject jSONObject5 = jSONObject.getJSONObject(Q);
            if (abVar.p == null) {
                abVar.p = new ac();
            }
            abVar.p.f2983a = jSONObject5.getString(R);
            abVar.p.b = jSONObject5.optString(U);
            abVar.p.c = jSONObject5.optString(S);
            String optString2 = jSONObject5.optString(T);
            abVar.p.d = l(optString2);
        }
    }

    public static ae b(JSONObject jSONObject) {
        ae aeVar = new ae();
        try {
            aeVar.i = jSONObject.optString(ae);
        } catch (Exception e) {
        }
        try {
            ab abVar = new ab();
            a(jSONObject.getJSONObject(n), abVar);
            aeVar.g = abVar;
        } catch (Exception e2) {
        }
        try {
            aeVar.o = jSONObject.optInt(ak, aeVar.o);
        } catch (Exception e3) {
        }
        try {
            aeVar.j = jSONObject.optString(M);
        } catch (Exception e4) {
        }
        try {
            aeVar.h = jSONObject.optString(r);
        } catch (Exception e5) {
        }
        try {
            aeVar.f = jSONObject.optString(E);
        } catch (Exception e6) {
        }
        try {
            aeVar.a(a.a(jSONObject.optLong(s)));
        } catch (Exception e7) {
        }
        try {
            aeVar.l = jSONObject.optInt(G);
        } catch (Exception e8) {
        }
        try {
            aeVar.n = jSONObject.optInt(I);
        } catch (Exception e9) {
        }
        try {
            aeVar.b = jSONObject.optString(u);
        } catch (Exception e10) {
        }
        try {
            aeVar.k = jSONObject.optString(L);
        } catch (Exception e11) {
        }
        try {
            aeVar.m = jSONObject.optInt(H);
        } catch (Exception e12) {
        }
        try {
            if (jSONObject.has(D)) {
                JSONObject optJSONObject = jSONObject.optJSONObject(D);
                aeVar.f2985a = new bf();
                aeVar.f2985a.X = -1;
                w.a(aeVar.f2985a, optJSONObject);
            }
        } catch (Exception e13) {
        }
        try {
            j.a(jSONObject.getJSONObject(ad), new u());
        } catch (Exception e14) {
        }
        try {
            aeVar.q = jSONObject.optString(F);
        } catch (Exception e15) {
        }
        try {
            aeVar.d = jSONObject.optString(K);
        } catch (Exception e16) {
        }
        try {
            aeVar.c = jSONObject.optString(J);
        } catch (Exception e17) {
        }
        try {
            aeVar.p = jSONObject.optInt(LocationManagerProxy.KEY_STATUS_CHANGED);
        } catch (Exception e18) {
        }
        return aeVar;
    }

    public final c a(String str, int i2, ae aeVar) {
        String str2 = String.valueOf(g) + "/v2/comment/feedcomments?fr=" + g.q().h;
        HashMap hashMap = new HashMap();
        hashMap.put(r, new StringBuilder(String.valueOf(str)).toString());
        hashMap.put("index", new StringBuilder(String.valueOf(i2)).toString());
        hashMap.put("count", new StringBuilder(String.valueOf(20)).toString());
        if (aeVar != null) {
            hashMap.put(L, aeVar.k);
            if (aeVar.a() != null) {
                hashMap.put("timestamp", new StringBuilder(String.valueOf(aeVar.a().getTime() / 1000)).toString());
            }
        }
        return new c(new JSONObject(a(str2, hashMap)));
    }

    public final d a(int i2, ab abVar) {
        String str = String.valueOf(g) + "/v2/friendsfeed?fr=" + g.q().h;
        HashMap hashMap = new HashMap();
        hashMap.put("index", new StringBuilder(String.valueOf(i2)).toString());
        hashMap.put("count", new StringBuilder(String.valueOf(20)).toString());
        if (abVar != null) {
            hashMap.put(r, abVar.h);
            if (abVar.d() != null) {
                hashMap.put("timestamp", new StringBuilder(String.valueOf(abVar.d().getTime() / 1000)).toString());
            }
        }
        return new d(new JSONObject(a(str, hashMap)));
    }

    public final com.immomo.momo.protocol.a.a.k a(int i2, ae aeVar) {
        String str = String.valueOf(g) + "/v2/comment/mycomments?fr=" + g.q().h;
        HashMap hashMap = new HashMap();
        hashMap.put("index", new StringBuilder(String.valueOf(i2)).toString());
        hashMap.put("count", new StringBuilder(String.valueOf(20)).toString());
        if (aeVar != null) {
            hashMap.put(L, aeVar.k);
            if (aeVar.a() != null) {
                hashMap.put("timestamp", new StringBuilder(String.valueOf(aeVar.a().getTime() / 1000)).toString());
            }
        }
        return new com.immomo.momo.protocol.a.a.k(new JSONObject(a(str, hashMap)));
    }

    public final m a(String str, String str2, int i2, int i3, String str3, String str4, String str5) {
        String str6 = String.valueOf(g) + "/v2/comment/publish?fr=" + g.q().h;
        HashMap hashMap = new HashMap();
        hashMap.put(r, str);
        hashMap.put(F, new StringBuilder(String.valueOf(str2)).toString());
        hashMap.put(G, new StringBuilder(String.valueOf(i2)).toString());
        hashMap.put(I, new StringBuilder(String.valueOf(i3)).toString());
        hashMap.put(E, new StringBuilder(String.valueOf(str3)).toString());
        hashMap.put(J, new StringBuilder(String.valueOf(str4)).toString());
        hashMap.put(K, new StringBuilder(String.valueOf(str5)).toString());
        return new m(new JSONObject(a(str6, hashMap)));
    }

    public final String a(String str) {
        String str2 = String.valueOf(g) + "/v2/publish/check";
        HashMap hashMap = new HashMap();
        hashMap.put(E, str);
        return new JSONObject(a(str2, hashMap)).getString("em");
    }

    public final String a(String str, String str2) {
        String str3 = String.valueOf(g) + "/v2/report?fr=" + g.q().h;
        HashMap hashMap = new HashMap();
        hashMap.put(E, str);
        hashMap.put(r, str2);
        return new JSONObject(a(str3, hashMap)).optString(q);
    }

    public final String a(String str, HashMap hashMap, String str2, com.immomo.momo.plugin.b.a aVar, boolean z2, boolean z3, boolean z4, boolean z5, ab abVar, int i2, double d, double d2) {
        String str3 = String.valueOf(g) + "/v2/publish/send";
        HashMap hashMap2 = new HashMap();
        hashMap2.put(E, str);
        hashMap2.put(h, new StringBuilder(String.valueOf(z2 ? 1 : 0)).toString());
        hashMap2.put(i, new StringBuilder(String.valueOf(z3 ? 1 : 0)).toString());
        hashMap2.put(j, new StringBuilder(String.valueOf(z4 ? 1 : 0)).toString());
        hashMap2.put(k, new StringBuilder(String.valueOf(z5 ? 1 : 0)).toString());
        hashMap2.put(A, new StringBuilder(String.valueOf(i2)).toString());
        hashMap2.put("lat", new StringBuilder(String.valueOf(d)).toString());
        hashMap2.put("lng", new StringBuilder(String.valueOf(d2)).toString());
        JSONObject jSONObject = null;
        if (hashMap.size() > 0) {
            hashMap2.put(t, str2);
            int i3 = 0;
            l[] lVarArr = new l[hashMap.size()];
            Iterator it = hashMap.entrySet().iterator();
            while (true) {
                int i4 = i3;
                if (!it.hasNext()) {
                    break;
                }
                Map.Entry entry = (Map.Entry) it.next();
                i3 = i4 + 1;
                lVarArr[i4] = new l("avator.jpg", (File) entry.getValue(), (String) entry.getKey(), (byte) 0);
            }
            jSONObject = new JSONObject(a(str3, hashMap2, lVarArr));
        } else if (aVar != null) {
            hashMap2.put(O, aVar.g());
            hashMap2.put(N, aVar.h());
            hashMap2.put(P, aVar.toString());
            jSONObject = new JSONObject(a(str3, hashMap2));
        } else {
            new NullPointerException("发布动态，表情和图片不能同时为空");
        }
        a(jSONObject.optJSONObject("data").optJSONObject(n), abVar);
        return jSONObject.optJSONObject("data").optString("weixin_url", PoiTypeDef.All);
    }

    public final boolean a(List list, int i2, double d, double d2, int i3) {
        HashMap hashMap = new HashMap();
        hashMap.put("index", new StringBuilder(String.valueOf(i2)).toString());
        hashMap.put("count", new StringBuilder(String.valueOf(24)).toString());
        hashMap.put("lat", new StringBuilder().append(d).toString());
        hashMap.put("lng", new StringBuilder().append(d2).toString());
        hashMap.put(A, new StringBuilder().append(i3).toString());
        JSONObject jSONObject = new JSONObject(a(String.valueOf(g) + "/v2/nearby?fr=" + g.q().h, hashMap));
        JSONArray optJSONArray = jSONObject.optJSONArray(o);
        if (optJSONArray != null) {
            for (int i4 = 0; i4 < optJSONArray.length(); i4++) {
                ab abVar = new ab();
                a(optJSONArray.getJSONObject(i4), abVar);
                list.add(abVar);
            }
        }
        return jSONObject.optInt(B) == 1;
    }

    public final boolean a(List list, int i2, String str) {
        return a(list, i2, str, (ab) null);
    }

    public final boolean a(List list, int i2, String str, ab abVar) {
        this.e.a((Object) "downloadUserFeedList called");
        String str2 = String.valueOf(g) + "/v2/userfeed?fr=" + g.q().h;
        HashMap hashMap = new HashMap();
        hashMap.put("index", new StringBuilder(String.valueOf(i2)).toString());
        hashMap.put("count", new StringBuilder(String.valueOf(20)).toString());
        hashMap.put(C, str);
        if (abVar != null) {
            hashMap.put(r, abVar.h);
            if (abVar.d() != null) {
                hashMap.put("timestamp", new StringBuilder(String.valueOf(abVar.d().getTime() / 1000)).toString());
            }
        }
        JSONObject jSONObject = new JSONObject(a(str2, hashMap));
        JSONArray optJSONArray = jSONObject.optJSONArray(o);
        if (optJSONArray != null) {
            for (int i3 = 0; i3 < optJSONArray.length(); i3++) {
                ab abVar2 = new ab();
                a(optJSONArray.getJSONObject(i3), abVar2);
                list.add(abVar2);
            }
        }
        return jSONObject.optInt(B) == 1;
    }

    public final d b() {
        return a(0, (ab) null);
    }

    public final String b(String str) {
        String str2 = String.valueOf(g) + "/v2/remove?fr=" + g.q().h;
        HashMap hashMap = new HashMap();
        hashMap.put(r, str);
        this.e.a(hashMap);
        return new JSONObject(a(str2, hashMap)).optString(q);
    }

    public final String b(String str, String str2) {
        String str3 = String.valueOf(g) + "/v2/comment/report?fr=" + g.q().h;
        HashMap hashMap = new HashMap();
        hashMap.put(E, str);
        hashMap.put(L, str2);
        return new JSONObject(a(str3, hashMap)).optString(q);
    }

    public final com.immomo.momo.protocol.a.a.k c() {
        return a(0, (ae) null);
    }

    public final ab c(String str) {
        HashMap hashMap = new HashMap();
        hashMap.put(r, str);
        ab abVar = new ab();
        a(new JSONObject(a(String.valueOf(g) + "/v2/profile?fr=" + g.q().h, hashMap)), abVar);
        return abVar;
    }

    public final String d(String str) {
        String str2 = String.valueOf(g) + "/v2/comment/remove?fr=" + g.q().h;
        HashMap hashMap = new HashMap();
        hashMap.put(L, str);
        return new JSONObject(a(str2, hashMap)).optString(q);
    }

    public final c e(String str) {
        return a(str, 0, (ae) null);
    }
}
