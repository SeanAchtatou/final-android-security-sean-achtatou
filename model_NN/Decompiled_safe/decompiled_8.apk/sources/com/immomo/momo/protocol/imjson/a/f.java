package com.immomo.momo.protocol.imjson.a;

import com.immomo.a.a.d.b;
import java.util.HashMap;
import java.util.Map;

public final class f implements com.immomo.a.a.f {

    /* renamed from: a  reason: collision with root package name */
    private Map f2904a = new HashMap();

    public final void a(Object obj, com.immomo.a.a.f fVar) {
        this.f2904a.put(obj.toString(), fVar);
    }

    public final boolean a(b bVar) {
        com.immomo.a.a.f fVar = (com.immomo.a.a.f) this.f2904a.get(bVar.h());
        if (fVar == null) {
            return false;
        }
        fVar.a(bVar);
        return true;
    }
}
