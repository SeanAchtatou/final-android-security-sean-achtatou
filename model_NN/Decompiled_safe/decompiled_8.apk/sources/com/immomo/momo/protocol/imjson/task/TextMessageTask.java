package com.immomo.momo.protocol.imjson.task;

import com.immomo.a.a.d.i;
import com.immomo.momo.service.bean.Message;

public class TextMessageTask extends MessageTask {
    public TextMessageTask(Message message) {
        super(g.Succession, message);
    }

    /* access modifiers changed from: protected */
    public final void a(Message message, i iVar) {
        iVar.c(message.getContent());
    }
}
