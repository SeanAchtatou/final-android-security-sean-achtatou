package com.immomo.momo.protocol.a;

import com.immomo.momo.android.b.o;
import com.immomo.momo.service.bean.GameApp;
import com.immomo.momo.service.bean.at;
import com.immomo.momo.service.bean.l;
import com.immomo.momo.service.bean.m;
import com.sina.sdk.api.message.InviteApi;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public final class g extends p {
    private static g f = null;

    public static g a() {
        if (f == null) {
            f = new g();
        }
        return f;
    }

    public final void a(l lVar, o oVar) {
        m a2;
        HashMap hashMap = new HashMap();
        hashMap.put("lat", new StringBuilder(String.valueOf(oVar.b())).toString());
        hashMap.put("lng", new StringBuilder(String.valueOf(oVar.c())).toString());
        hashMap.put("acc", new StringBuilder(String.valueOf(oVar.d())).toString());
        hashMap.put("loctype", new StringBuilder(String.valueOf(oVar.a())).toString());
        at r = com.immomo.momo.g.r();
        if (r != null) {
            hashMap.put("tiebalist_last_refresh_time", new StringBuilder().append(r.b("tiebahomerefreshtime", 0)).toString());
        }
        this.e.a(hashMap);
        JSONObject jSONObject = new JSONObject(a(String.valueOf(b) + "/discover", hashMap)).getJSONObject("data");
        if (jSONObject.has("tieba") && (a2 = lVar.a(5)) != null) {
            a2.d = jSONObject.getJSONObject("tieba").optInt("has_new") == 1;
        }
        if (jSONObject.has("emotion")) {
            m a3 = lVar.a(7);
            JSONObject jSONObject2 = jSONObject.getJSONObject("emotion");
            a3.e = jSONObject2.optString("action");
            a3.f = jSONObject2.optString("tips");
            long optLong = jSONObject2.optLong("updatetime");
            if (!(a3.d || optLong == 0 || a3.g == 0)) {
                a3.d = a3.g != optLong;
            }
            a3.g = optLong;
        }
        List b = lVar.b();
        ArrayList arrayList = new ArrayList();
        if (jSONObject.has("game")) {
            JSONArray jSONArray = jSONObject.getJSONArray("game");
            for (int i = 0; i < jSONArray.length(); i++) {
                JSONObject jSONObject3 = jSONArray.getJSONObject(i);
                m mVar = new m(4);
                mVar.b = jSONObject3.optString("app_name");
                GameApp gameApp = new GameApp();
                mVar.c = gameApp;
                gameApp.appname = jSONObject3.optString("app_name");
                gameApp.appicon = jSONObject3.optString("app_icon");
                gameApp.appdownload = jSONObject3.optString("url_download");
                gameApp.appid = jSONObject3.optString("app_id");
                gameApp.appURI = jSONObject3.optString(InviteApi.KEY_URL);
                gameApp.mcount = jSONObject3.optString("mcount");
                gameApp.eventNotice = jSONObject3.optString("tips");
                gameApp.updateTime = a(jSONObject3.optLong("app_updatetime"));
                arrayList.add(mVar);
                Iterator it = b.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        break;
                    }
                    m mVar2 = (m) it.next();
                    if (mVar2.c != null) {
                        GameApp gameApp2 = (GameApp) mVar2.c;
                        if (gameApp2.appid.equals(gameApp.appid)) {
                            if (!mVar.d && gameApp.updateTime != null && (gameApp2.updateTime == null || gameApp2.updateTime.before(gameApp.updateTime))) {
                                mVar.d = true;
                            }
                        }
                    }
                }
            }
            lVar.b().clear();
            lVar.b().addAll(arrayList);
        }
    }
}
