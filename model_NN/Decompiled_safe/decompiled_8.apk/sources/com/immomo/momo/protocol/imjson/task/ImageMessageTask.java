package com.immomo.momo.protocol.imjson.task;

import android.support.v4.b.a;
import com.immomo.a.a.d.i;
import com.immomo.momo.service.bean.Message;
import java.io.File;
import java.net.URI;
import java.util.Timer;
import java.util.TimerTask;

public class ImageMessageTask extends MessageTask {

    /* renamed from: a  reason: collision with root package name */
    protected File f2935a;
    int b;
    /* access modifiers changed from: private */
    public boolean e;
    /* access modifiers changed from: private */
    public Object f;
    /* access modifiers changed from: private */
    public TimerTask g;
    /* access modifiers changed from: private */
    public Timer h;

    public ImageMessageTask(Message message) {
        super(g.SuccessionLongtime, message);
        this.e = false;
        this.f = new Object();
        this.h = null;
        this.b = 0;
        this.f2935a = null;
        this.h = new Timer();
    }

    private boolean a(Message message) {
        boolean z = false;
        if (message.fileName.startsWith("file://")) {
            this.f2935a = new File(URI.create(message.fileName));
        } else {
            this.f2935a = null;
        }
        boolean z2 = this.f2935a != null;
        if (!z2) {
            z = true;
        }
        this.e = z;
        if (z2) {
            if (this.g != null) {
                this.g.cancel();
                this.g = null;
                this.h.purge();
            }
            this.g = new e(this);
            this.h.schedule(this.g, 120000);
            new c(this, message).start();
            try {
                synchronized (this.f) {
                    this.f.wait();
                }
            } catch (InterruptedException e2) {
            }
        }
        return this.e;
    }

    /* access modifiers changed from: protected */
    public final void a(Message message, i iVar) {
        if (!a(message)) {
            throw new Exception("image upload failed");
        }
        e();
        iVar.c(a.a(a.a(a.a(String.valueOf(com.immomo.momo.a.f) + "/chatimage", "mode", "GUID"), "filesize", new StringBuilder(String.valueOf(message.fileSize)).toString()), "file", message.fileName));
    }

    public final boolean a(com.immomo.a.a.a aVar) {
        return super.a(aVar);
    }
}
