package com.immomo.momo.protocol.imjson.c;

import java.util.TreeSet;

public final class f extends TreeSet {

    /* renamed from: a  reason: collision with root package name */
    private String f2917a = null;

    public f(String str) {
        this.f2917a = str;
    }

    /* JADX WARNING: Removed duplicated region for block: B:3:0x000b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(android.os.Bundle r4) {
        /*
            r3 = this;
            java.util.Iterator r1 = r3.iterator()
        L_0x0004:
            boolean r0 = r1.hasNext()
            if (r0 != 0) goto L_0x000b
        L_0x000a:
            return
        L_0x000b:
            java.lang.Object r0 = r1.next()
            com.immomo.momo.protocol.imjson.c.a r0 = (com.immomo.momo.protocol.imjson.c.a) r0
            java.lang.String r2 = r3.f2917a
            boolean r0 = r0.a(r4, r2)
            if (r0 != 0) goto L_0x000a
            goto L_0x0004
        */
        throw new UnsupportedOperationException("Method not decompiled: com.immomo.momo.protocol.imjson.c.f.a(android.os.Bundle):void");
    }
}
