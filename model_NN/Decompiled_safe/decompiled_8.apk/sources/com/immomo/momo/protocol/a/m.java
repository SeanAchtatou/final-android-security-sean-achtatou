package com.immomo.momo.protocol.a;

import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.a.a.f.a;
import com.immomo.a.a.f.b;
import com.immomo.momo.android.b.o;
import com.immomo.momo.android.game.ai;
import com.immomo.momo.android.game.aj;
import com.immomo.momo.android.game.ak;
import com.immomo.momo.android.game.al;
import com.immomo.momo.android.game.am;
import com.immomo.momo.android.game.an;
import com.immomo.momo.android.game.ao;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.GameApp;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.util.f;
import com.immomo.momo.util.jni.Codec;
import com.sina.sdk.api.message.InviteApi;
import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

public final class m extends p {
    private static final String f = (String.valueOf(f2900a) + "/game");
    private static m g = null;

    public static m a() {
        if (g == null) {
            g = new m();
        }
        return g;
    }

    public final String a(String str, String str2, int i, File file) {
        if (file == null) {
            return null;
        }
        String str3 = String.valueOf(f) + "/action/upload";
        HashMap hashMap = new HashMap();
        hashMap.put("token", str);
        hashMap.put("appid", str2);
        hashMap.put("type", new StringBuilder(String.valueOf(i)).toString());
        return new JSONObject(a(str3, hashMap, new l[]{new l(file.getName(), file, "file_upload", (byte) 0)}, (Map) null)).getJSONObject("data").optString(InviteApi.KEY_URL, PoiTypeDef.All);
    }

    public final String a(String str, String str2, String str3, String str4, String str5) {
        HashMap hashMap = new HashMap();
        String a2 = b.a(16);
        StringBuilder sb = new StringBuilder();
        sb.append(str).append("android").append(str3).append(a2).append(str2).append(str4).append(g.z()).append(Codec.wfer68());
        hashMap.put("session_token", a.b(sb.toString()));
        hashMap.put("appid", str);
        hashMap.put("momoid", str3);
        hashMap.put("redirect_uri", str2);
        hashMap.put("client", "android");
        hashMap.put("version", new StringBuilder(String.valueOf(g.z())).toString());
        hashMap.put("random", a2);
        hashMap.put("session_id", str4);
        hashMap.put("packagename", str5);
        this.e.a(hashMap);
        return new JSONObject(a(String.valueOf(f) + "/app/auth", hashMap)).getJSONObject("data").getString("token");
    }

    public final void a(GameApp gameApp, int i) {
        HashMap hashMap = new HashMap();
        hashMap.put("appid", gameApp.appid);
        hashMap.put("referer", new StringBuilder(String.valueOf(i)).toString());
        JSONObject jSONObject = new JSONObject(a(String.valueOf(f) + "/app/info", hashMap)).getJSONObject("data");
        gameApp.appid = jSONObject.getString("appid");
        gameApp.appname = jSONObject.getString("appname");
        gameApp.appdownload = jSONObject.getString("apkurl");
        gameApp.appicon = jSONObject.getString("appicon");
        gameApp.appdesc = jSONObject.optString("appsumm");
        gameApp.appURI = jSONObject.getString("backurl");
        gameApp.picArray = a(jSONObject.optJSONArray("pics"));
        gameApp.mcount = jSONObject.optString("mcount");
        gameApp.eventNotice = jSONObject.optString("eventsumm");
        gameApp.buttonLabel = jSONObject.optString("button");
        gameApp.tiebaId = jSONObject.optString("tiebaid");
        JSONObject optJSONObject = jSONObject.optJSONObject("detail");
        if (optJSONObject != null) {
            gameApp.developers = optJSONObject.optString("developers");
            gameApp.updateTimeString = optJSONObject.optString("uptime");
            gameApp.versionName = optJSONObject.optString("version");
            gameApp.size = optJSONObject.optString("size");
            gameApp.updateNotice = optJSONObject.optString("notice");
            return;
        }
        gameApp.developers = null;
        gameApp.updateTimeString = null;
        gameApp.versionName = null;
        gameApp.size = null;
        gameApp.updateNotice = null;
    }

    public final void a(String str, int i) {
        HashMap hashMap = new HashMap();
        hashMap.put("appid", str);
        hashMap.put("uid", g.I());
        hashMap.put("model", g.F());
        hashMap.put("rom", g.C());
        hashMap.put("referer", new StringBuilder(String.valueOf(i)).toString());
        a(String.valueOf(f) + "/app/log/open", hashMap);
    }

    public final void a(String str, int i, int i2, String str2, String str3) {
        HashMap hashMap = new HashMap();
        hashMap.put("desc", str);
        hashMap.put("star", String.valueOf(i));
        hashMap.put("type", String.valueOf(i2));
        hashMap.put("token", str2);
        hashMap.put("appid", str3);
        a(String.valueOf(f) + "/app/feedback", hashMap);
    }

    public final void a(String str, String str2, ai aiVar) {
        HashMap hashMap = new HashMap();
        hashMap.put("token", str);
        hashMap.put("appid", str2);
        aiVar.f = new JSONObject(a(String.valueOf(f) + "/trade/gold/balance", hashMap)).getJSONObject("data").optDouble("balance", aiVar.f);
    }

    public final void a(String str, String str2, ai aiVar, ak akVar) {
        HashMap hashMap = new HashMap();
        hashMap.put("token", str2);
        hashMap.put("appid", str);
        hashMap.put("productid", aiVar.i);
        JSONArray jSONArray = new JSONObject(a(String.valueOf(f) + "/trade/card/channel", hashMap)).getJSONArray("data");
        akVar.f2407a.clear();
        for (int i = 0; i < jSONArray.length(); i++) {
            JSONObject jSONObject = jSONArray.getJSONObject(i);
            aj ajVar = new aj();
            ajVar.f2406a = jSONObject.getString("pc_id");
            ajVar.b = jSONObject.getString("pm_id");
            jSONObject.getString("province");
            ajVar.c = jSONObject.getString("desc");
            ajVar.g = jSONObject.getString("merchant_key");
            ajVar.h.clear();
            JSONArray jSONArray2 = jSONObject.getJSONArray("amount");
            for (int i2 = 0; i2 < jSONArray2.length(); i2++) {
                ajVar.h.add(Integer.valueOf(jSONArray2.getInt(i2)));
            }
            akVar.f2407a.add(ajVar);
        }
    }

    public final void a(String str, String str2, ai aiVar, al alVar) {
        HashMap hashMap = new HashMap();
        hashMap.put("token", str);
        hashMap.put("appid", str2);
        hashMap.put("productid", aiVar.i);
        if (!a.a(aiVar.j)) {
            hashMap.put("app_trade_no", aiVar.j);
        }
        JSONObject jSONObject = new JSONObject(a(String.valueOf(f) + "/trade/mmpay/sign", hashMap)).getJSONObject("data");
        alVar.g = jSONObject.getString("sign");
        alVar.h = jSONObject.getString("trade_no");
        alVar.f2408a = jSONObject.getString("pay_code");
        alVar.b = jSONObject.getInt("order_count");
        alVar.c = jSONObject.getString("appid");
        alVar.d = jSONObject.getString("appkey");
    }

    public final void a(String str, String str2, ai aiVar, am amVar) {
        HashMap hashMap = new HashMap();
        hashMap.put("token", str);
        hashMap.put("appid", str2);
        hashMap.put("productid", aiVar.i);
        hashMap.put("subject", aiVar.h);
        hashMap.put("body", aiVar.h);
        hashMap.put("total_fee", new StringBuilder(String.valueOf(amVar.e)).toString());
        if (!a.a(aiVar.j)) {
            hashMap.put("app_trade_no", aiVar.j);
        }
        JSONObject jSONObject = new JSONObject(a(String.valueOf(f) + "/trade/alipay/sign", hashMap)).getJSONObject("data");
        amVar.f = jSONObject.getString("sign_url");
        amVar.g = jSONObject.getString("sign");
        amVar.h = jSONObject.getString("trade_no");
    }

    public final void a(String str, String str2, ai aiVar, ao aoVar) {
        HashMap hashMap = new HashMap();
        hashMap.put("token", str);
        hashMap.put("appid", str2);
        hashMap.put("productid", aiVar.i);
        if (!a.a(aiVar.j)) {
            hashMap.put("app_trade_no", aiVar.j);
        }
        String E = g.E();
        if (!a.a(E)) {
            hashMap.put("mac", E);
        }
        hashMap.put("imei", g.D());
        hashMap.put("ip", g.W());
        hashMap.put("channel", aoVar.o);
        hashMap.put("appversion", g.A());
        JSONObject jSONObject = new JSONObject(a(String.valueOf(f) + "/trade/unipay/sign", hashMap)).getJSONObject("data");
        aoVar.g = jSONObject.getString("sign");
        aoVar.h = jSONObject.getString("trade_no");
        aoVar.f2409a = jSONObject.getString("cp_code");
        aoVar.b = jSONObject.getString("vac_code");
        aoVar.c = jSONObject.getString("vac_mode");
        aoVar.d = jSONObject.getString("cpid");
        aoVar.j = jSONObject.optString("company");
        aoVar.k = jSONObject.optString("game");
        aoVar.l = jSONObject.optString("phont");
        aoVar.m = jSONObject.getString("notify_url");
        aoVar.n = jSONObject.getString("appid");
    }

    public final void a(String str, String str2, ak akVar) {
        HashMap hashMap = new HashMap();
        hashMap.put("token", str);
        hashMap.put("appid", str2);
        hashMap.put("sign", akVar.g);
        hashMap.put("trade_no", akVar.h);
        a(String.valueOf(f) + "/trade/card/pay", hashMap);
    }

    public final void a(String str, String str2, am amVar) {
        HashMap hashMap = new HashMap();
        hashMap.put("token", str);
        hashMap.put("appid", str2);
        hashMap.put("sign", amVar.g);
        hashMap.put("trade_no", amVar.h);
        a(String.valueOf(f) + "/trade/gold/pay", hashMap);
    }

    public final void a(String str, String str2, String str3, String str4, String str5, String str6) {
        HashMap hashMap = new HashMap();
        hashMap.put("token", str);
        hashMap.put("appid", str2);
        hashMap.put("content", str4);
        hashMap.put("image", str5);
        hashMap.put("rid", str3);
        hashMap.put("callbackid", str6);
        a(String.valueOf(f) + "/action/share/friend", hashMap);
    }

    public final boolean a(List list, int i, o oVar, String str) {
        HashMap hashMap = new HashMap();
        hashMap.put("loctype", new StringBuilder(String.valueOf(oVar.a())).toString());
        hashMap.put("lat", new StringBuilder(String.valueOf(oVar.b())).toString());
        hashMap.put("lng", new StringBuilder(String.valueOf(oVar.c())).toString());
        hashMap.put("acc", new StringBuilder(String.valueOf(oVar.d())).toString());
        hashMap.put("count", new StringBuilder(String.valueOf(30)).toString());
        hashMap.put("index", new StringBuilder(String.valueOf(i)).toString());
        hashMap.put("appid", str);
        JSONObject jSONObject = new JSONObject(a(String.valueOf(f) + "/nearby/player/formal", hashMap));
        JSONArray jSONArray = jSONObject.getJSONArray("list");
        for (int i2 = 0; i2 < jSONArray.length(); i2++) {
            bf bfVar = new bf();
            list.add(bfVar);
            w.a(bfVar, jSONArray.getJSONObject(i2));
        }
        return jSONObject.optInt("remain") == 1;
    }

    public final void b(String str, int i) {
        HashMap hashMap = new HashMap();
        hashMap.put("appid", str);
        hashMap.put("uid", g.I());
        hashMap.put("model", g.F());
        hashMap.put("rom", g.C());
        hashMap.put("referer", new StringBuilder(String.valueOf(i)).toString());
        a(String.valueOf(f) + "/app/log/download", hashMap);
    }

    public final void b(String str, String str2, ai aiVar) {
        HashMap hashMap = new HashMap();
        hashMap.put("token", str2);
        hashMap.put("appid", str);
        hashMap.put("productid", aiVar.i);
        hashMap.put(Codec.Dse(), g.X());
        hashMap.put(Codec.etr968ww(), new StringBuilder(String.valueOf(Codec.isEmulator() ? 1 : 0)).toString());
        if (!a.a(aiVar.j)) {
            hashMap.put("app_trade_no", aiVar.j);
        }
        JSONObject jSONObject = new JSONObject(a(String.valueOf(f) + "/trade/product/info", hashMap)).getJSONObject("data");
        JSONObject jSONObject2 = jSONObject.getJSONObject("product");
        aiVar.h = jSONObject2.optString("name");
        JSONObject jSONObject3 = jSONObject2.getJSONObject("payment");
        if (jSONObject3.has("alipay")) {
            double optDouble = jSONObject3.optDouble("alipay", -1.0d);
            if (optDouble >= 0.0d) {
                am amVar = new am();
                aiVar.f2405a = amVar;
                amVar.e = optDouble;
            }
        }
        if (jSONObject3.has("gold")) {
            double optDouble2 = jSONObject3.optDouble("gold", -1.0d);
            if (optDouble2 >= 0.0d) {
                am amVar2 = new am();
                aiVar.b = amVar2;
                amVar2.e = optDouble2;
            }
        }
        if (jSONObject3.has("sms")) {
            double optDouble3 = jSONObject3.optDouble("sms", -1.0d);
            if (optDouble3 >= 0.0d) {
                new an().e = optDouble3;
            }
        }
        if (jSONObject3.has("card")) {
            double optDouble4 = jSONObject3.optDouble("card", -1.0d);
            if (optDouble4 >= 0.0d) {
                ak akVar = new ak();
                aiVar.e = akVar;
                akVar.e = optDouble4;
            }
        }
        if (jSONObject3.has("unipay")) {
            double optDouble5 = jSONObject3.optDouble("unipay", -1.0d);
            if (optDouble5 >= 0.0d) {
                ao aoVar = new ao();
                aiVar.c = aoVar;
                aoVar.e = optDouble5;
            }
        }
        if (jSONObject3.has("mmpay")) {
            double optDouble6 = jSONObject3.optDouble("mmpay", -1.0d);
            if (optDouble6 >= 0.0d) {
                al alVar = new al();
                aiVar.d = alVar;
                alVar.e = optDouble6;
            }
        }
        JSONObject optJSONObject = jSONObject.optJSONObject("account");
        if (optJSONObject != null) {
            try {
                bf bfVar = new bf();
                bfVar.i = optJSONObject.optString("name");
                bfVar.h = optJSONObject.optString("mid");
                aiVar.g = bfVar;
                aiVar.f = (double) optJSONObject.optLong("balance");
            } catch (Exception e) {
            }
        } else {
            aiVar.g = null;
        }
    }

    public final void b(String str, String str2, ai aiVar, ak akVar) {
        HashMap hashMap = new HashMap();
        hashMap.put("token", str);
        hashMap.put("appid", str2);
        hashMap.put("productid", aiVar.i);
        hashMap.put("productid", aiVar.i);
        hashMap.put("pm_id", akVar.b.b);
        hashMap.put("pc_id", akVar.b.f2406a);
        hashMap.put("amount", new StringBuilder(String.valueOf(akVar.b.d)).toString());
        hashMap.put("card_num", f.a(akVar.b.e, akVar.b.g));
        hashMap.put("card_pwd", f.a(akVar.b.f, akVar.b.g));
        if (!a.a(aiVar.j)) {
            hashMap.put("app_trade_no", aiVar.j);
        }
        JSONObject jSONObject = new JSONObject(a(String.valueOf(f) + "/trade/card/sign", hashMap)).getJSONObject("data");
        akVar.g = jSONObject.getString("sign");
        akVar.h = jSONObject.getString("trade_no");
    }

    public final void b(String str, String str2, ai aiVar, am amVar) {
        HashMap hashMap = new HashMap();
        hashMap.put("token", str);
        hashMap.put("appid", str2);
        hashMap.put("productid", aiVar.i);
        hashMap.put("total_fee", new StringBuilder(String.valueOf(amVar.e)).toString());
        if (!a.a(aiVar.j)) {
            hashMap.put("app_trade_no", aiVar.j);
        }
        JSONObject jSONObject = new JSONObject(a(String.valueOf(f) + "/trade/gold/sign", hashMap)).getJSONObject("data");
        amVar.g = jSONObject.getString("sign");
        amVar.h = jSONObject.getString("trade_no");
    }

    public final void b(String str, String str2, String str3, String str4, String str5, String str6) {
        HashMap hashMap = new HashMap();
        hashMap.put("token", str);
        hashMap.put("appid", str2);
        hashMap.put("content", str4);
        hashMap.put("image", str5);
        hashMap.put("rid", str3);
        hashMap.put("callbackid", str6);
        a(String.valueOf(f) + "/action/share/group", hashMap);
    }

    public final void c(String str, String str2, String str3, String str4, String str5, String str6) {
        HashMap hashMap = new HashMap();
        hashMap.put("token", str);
        hashMap.put("appid", str2);
        hashMap.put("content", str4);
        hashMap.put("image", str5);
        hashMap.put("rid", str3);
        hashMap.put("callbackid", str6);
        a(String.valueOf(f) + "/action/share/discuss", hashMap);
    }
}
