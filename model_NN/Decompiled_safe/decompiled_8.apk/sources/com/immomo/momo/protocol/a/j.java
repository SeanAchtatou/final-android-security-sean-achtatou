package com.immomo.momo.protocol.a;

import android.support.v4.b.a;
import com.amap.mapapi.location.LocationManagerProxy;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.g;
import com.immomo.momo.protocol.a.a.b;
import com.immomo.momo.protocol.a.a.n;
import com.immomo.momo.service.bean.aa;
import com.immomo.momo.service.bean.ab;
import com.immomo.momo.service.bean.ae;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.service.bean.u;
import com.immomo.momo.service.bean.y;
import com.immomo.momo.service.bean.z;
import com.sina.sdk.api.message.InviteApi;
import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

public final class j extends p {
    private static String A = "eventid";
    private static String B = "srcid";
    private static String C = "srctype";
    private static String D = "content_type";
    private static String E = "tomomoid";
    private static String F = "toname";
    private static String G = "avatar";
    private static String H = "distance";
    private static String I = "address";
    private static String J = "address_detail";
    private static String K = "comment_count";
    private static String L = "join_count";
    private static String M = "fee";
    private static String N = "members";
    private static String O = "momoid";
    private static String P = "content";
    private static String Q = "user";
    private static String R = "host";
    private static String S = "is_member";
    private static String T = "pics";
    private static String U = "emotion_library";
    private static String V = "emotion_name";
    private static String W = "emotion_body";
    private static j f = null;
    private static String g = (String.valueOf(b) + "/event");
    private static String h = "sync_sina";
    private static String i = "sync_renren";
    private static String j = "sync_qqwb";
    private static String k = "sync_feed";
    private static String l = "sync_weixin";
    private static String m = "msgboard";
    private static String n = "commentid";
    private static String o = "loctype";
    private static String p = "holdtime";
    private static String q = "holdtype";
    private static String r = "subject";
    private static String s = "sorttype";
    private static String t = LocationManagerProxy.KEY_STATUS_CHANGED;
    private static String u = "desc";
    private static String v = "desc_url";
    private static String w = "data";
    private static String x = "list";
    private static String y = "remain";
    private static String z = "name";

    public static j a() {
        if (f == null) {
            f = new j();
        }
        return f;
    }

    private static bf a(JSONObject jSONObject) {
        boolean z2 = true;
        bf bfVar = new bf();
        bfVar.h = jSONObject.optString(O, bfVar.h);
        bfVar.i = jSONObject.optString("name", bfVar.i);
        bfVar.ai = jSONObject.optInt("vip_level", bfVar.ai);
        try {
            bfVar.a(jSONObject.optLong("loc_timesec") * 1000);
        } catch (Exception e) {
        }
        bfVar.I = jSONObject.optInt("age", bfVar.I);
        bfVar.aJ = jSONObject.optInt("group_role");
        try {
            bfVar.B = jSONObject.optInt("regtype", bfVar.B);
        } catch (Exception e2) {
        }
        bfVar.H = jSONObject.optString("sex", bfVar.H);
        bfVar.K = jSONObject.optString("constellation", bfVar.K);
        bfVar.b(jSONObject.optString("sign", bfVar.l()));
        bfVar.N = jSONObject.optString("job", bfVar.N);
        bfVar.M = jSONObject.optString("industry", bfVar.M);
        try {
            bfVar.P = jSONObject.optString("relation", PoiTypeDef.All);
        } catch (Exception e3) {
        }
        try {
            bfVar.l = jSONObject.optString("remarkname", PoiTypeDef.All);
        } catch (Exception e4) {
        }
        try {
            if (jSONObject.has("sina_user_id")) {
                bfVar.am = jSONObject.optString("sina_user_id");
                bfVar.an = !a.a(bfVar.am);
            }
        } catch (Exception e5) {
        }
        try {
            if (jSONObject.has("renren_user_id")) {
                bfVar.aq = jSONObject.optString("renren_user_id");
                bfVar.ar = !a.a(bfVar.aq);
            }
        } catch (Exception e6) {
        }
        if (jSONObject.has("tencent_user_id")) {
            bfVar.as = jSONObject.optString("tencent_user_id");
            bfVar.at = !a.a(bfVar.as);
        }
        if (jSONObject.has("tencent_vip_desc")) {
            bfVar.au = !a.a(jSONObject.optString("tencent_vip_desc"));
        }
        try {
            if (jSONObject.has("douban_user_id")) {
                bfVar.av = jSONObject.optString("douban_user_id");
                a.a((CharSequence) bfVar.av);
            }
        } catch (Exception e7) {
        }
        try {
            if (jSONObject.has("sina_vip_desc")) {
                bfVar.ao = jSONObject.optString("sina_vip_desc");
                if (a.a((CharSequence) bfVar.ao)) {
                    z2 = false;
                }
                bfVar.ap = z2;
            }
        } catch (Exception e8) {
        }
        try {
            if (jSONObject.has("client")) {
                bfVar.ab = jSONObject.getString("client");
            } else {
                bfVar.ab = PoiTypeDef.All;
            }
        } catch (Exception e9) {
        }
        try {
            JSONArray jSONArray = jSONObject.getJSONArray("photos");
            if (jSONArray != null && jSONArray.length() > 0) {
                String[] strArr = new String[jSONArray.length()];
                for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                    strArr[i2] = jSONArray.getString(i2);
                }
                bfVar.ae = strArr;
            }
        } catch (Exception e10) {
        }
        try {
            bfVar.a((float) jSONObject.optLong(H, -1));
        } catch (Exception e11) {
        }
        if (jSONObject.has("signex")) {
            try {
                JSONObject jSONObject2 = jSONObject.getJSONObject("signex");
                bfVar.c(jSONObject2.optString("desc"));
                bfVar.R = jSONObject2.optString("pic");
            } catch (Exception e12) {
            }
        }
        return bfVar;
    }

    public static y a(JSONObject jSONObject, List list) {
        y yVar;
        long j2 = 0;
        y yVar2 = new y();
        yVar2.d(jSONObject.optInt(K, 0));
        yVar2.b(jSONObject.optString("avatar"));
        yVar2.d(jSONObject.optString(A));
        yVar2.e(jSONObject.optString(z));
        yVar2.c(jSONObject.optInt(L));
        yVar2.f(jSONObject.optString("time_desc"));
        yVar2.a(a(jSONObject.optLong("update_time")));
        if (jSONObject.has("friend_avatars")) {
            JSONArray jSONArray = jSONObject.getJSONArray("friend_avatars");
            for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                yVar2.g(jSONArray.getString(i2));
            }
        }
        if (1 == jSONObject.optInt("mine")) {
            yVar2.l();
        }
        JSONObject optJSONObject = jSONObject.optJSONObject(u);
        yVar2.b(optJSONObject.optString("img"));
        yVar2.c(optJSONObject.optString(InviteApi.KEY_TEXT));
        yVar2.a(optJSONObject.optString("highlight"));
        if (list != null) {
            Iterator it = list.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                z zVar = (z) it.next();
                String h2 = yVar2.h();
                if (zVar.f3043a != null) {
                    Iterator it2 = zVar.f3043a.iterator();
                    while (true) {
                        if (it2.hasNext()) {
                            yVar = (y) it2.next();
                            if (yVar.h().equalsIgnoreCase(h2)) {
                                continue;
                                break;
                            }
                        } else {
                            break;
                        }
                    }
                }
                yVar = null;
                continue;
                if (yVar != null) {
                    long time = yVar.d() != null ? yVar.d().getTime() : 0;
                    if (yVar2.d() != null) {
                        j2 = yVar2.d().getTime();
                    }
                    if (time != j2) {
                        yVar2.a(false);
                    } else {
                        yVar2.a(true);
                    }
                    yVar2.b(yVar2.k() - yVar.k());
                    yVar2.a(yVar2.j() - yVar.j());
                }
            }
        } else {
            yVar2.a(true);
        }
        return yVar2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.b.a.a(java.util.Date, boolean):java.lang.String
     arg types: [java.util.Date, int]
     candidates:
      android.support.v4.b.a.a(android.graphics.Bitmap, float):android.graphics.Bitmap
      android.support.v4.b.a.a(android.graphics.Bitmap, java.io.File):android.graphics.Bitmap
      android.support.v4.b.a.a(int, int):java.lang.String
      android.support.v4.b.a.a(java.util.Collection, java.lang.String):java.lang.String
      android.support.v4.b.a.a(java.lang.Object[], java.lang.String):java.lang.String
      android.support.v4.b.a.a(java.lang.String, java.lang.String):org.json.JSONObject
      android.support.v4.b.a.a(float, int[]):void
      android.support.v4.b.a.a(android.app.Activity, int):void
      android.support.v4.b.a.a(android.view.View, java.lang.Runnable):void
      android.support.v4.b.a.a(java.io.File, java.io.File):void
      android.support.v4.b.a.a(java.io.File, java.util.zip.ZipInputStream):void
      android.support.v4.b.a.a(java.lang.Object, java.lang.StringBuilder):void
      android.support.v4.b.a.a(android.content.Context, java.lang.String):boolean
      android.support.v4.b.a.a(com.immomo.a.a.d.b, com.immomo.momo.service.bean.Message):boolean
      android.support.v4.b.a.a(byte[], java.lang.String):byte[]
      android.support.v4.b.a.a(byte[], byte[]):byte[]
      android.support.v4.b.a.a(int[], boolean):byte[]
      android.support.v4.b.a.a(byte[], boolean):int[]
      android.support.v4.b.a.a(int, float):void
      android.support.v4.b.a.a(java.lang.Object, com.immomo.a.a.f):void
      android.support.v4.view.bf.a(int, float):void
      com.immomo.a.a.f.a(java.lang.Object, com.immomo.a.a.f):void
      android.support.v4.b.a.a(java.util.Date, boolean):java.lang.String */
    private static void a(JSONObject jSONObject, aa aaVar) {
        aaVar.c = jSONObject.optString("owner");
        aaVar.h = jSONObject.optString("feedid");
        aaVar.e = jSONObject.optString("sid");
        aaVar.i = jSONObject.optInt(t);
        aaVar.j = jSONObject.optString(V);
        aaVar.k = jSONObject.optString(U);
        aaVar.a(jSONObject.optString(W));
        aaVar.a(a.a(jSONObject.optLong("create_time")));
        aaVar.b(jSONObject.optString(P));
        aaVar.g = jSONObject.optInt(K);
        aaVar.a((float) jSONObject.optInt(H, -1));
        Date a2 = a(jSONObject.optLong("comment_time"));
        aaVar.f2981a = a2;
        a.a(a2, false);
        JSONArray optJSONArray = jSONObject.optJSONArray(T);
        if (optJSONArray != null && optJSONArray.length() > 0) {
            String[] strArr = new String[optJSONArray.length()];
            for (int i2 = 0; i2 < optJSONArray.length(); i2++) {
                strArr[i2] = optJSONArray.getString(i2);
            }
            aaVar.a(strArr);
        }
        if (jSONObject.has(Q)) {
            JSONObject optJSONObject = jSONObject.optJSONObject(Q);
            aaVar.b = new bf();
            w.a(aaVar.b, optJSONObject);
        }
    }

    private static void a(JSONObject jSONObject, ab abVar) {
        abVar.c = jSONObject.optString("owner");
        abVar.h = jSONObject.optString("feedid");
        abVar.e = jSONObject.optString("sid");
        abVar.i = jSONObject.optInt(t);
        abVar.a(a.a(jSONObject.optLong("create_time")));
        abVar.b(jSONObject.optString(u));
        abVar.g = jSONObject.optInt(K);
        JSONArray optJSONArray = jSONObject.optJSONArray(T);
        if (optJSONArray != null && optJSONArray.length() > 0) {
            String[] strArr = new String[optJSONArray.length()];
            for (int i2 = 0; i2 < optJSONArray.length(); i2++) {
                strArr[i2] = optJSONArray.getString(i2);
            }
            abVar.a(strArr);
        }
        if (jSONObject.has(Q)) {
            abVar.b = new bf();
            abVar.b.X = -1;
            w.a(abVar.b, jSONObject.optJSONObject(Q));
        }
    }

    public static void a(JSONObject jSONObject, u uVar) {
        boolean z2 = true;
        uVar.b = jSONObject.optString(z, uVar.b);
        uVar.f3038a = jSONObject.optString(A, uVar.f3038a);
        uVar.c = jSONObject.optString(G, uVar.c);
        uVar.d = jSONObject.optString(p, uVar.d);
        uVar.e = jSONObject.optString(I, uVar.e);
        uVar.f = jSONObject.optString(J, uVar.f);
        uVar.g = jSONObject.optString(M, uVar.g);
        uVar.i = jSONObject.optInt(r, uVar.i);
        uVar.j = jSONObject.optInt(t, uVar.j);
        uVar.a(jSONObject.optInt(K, uVar.d()));
        uVar.c(jSONObject.optInt(L, uVar.f()));
        uVar.a((float) jSONObject.optLong(H, -1));
        uVar.k = jSONObject.optString(u, uVar.k);
        uVar.u = jSONObject.optString(v, uVar.u);
        uVar.t = jSONObject.optInt(S, 0) == 1;
        uVar.x = jSONObject.optString("city", uVar.x);
        JSONObject optJSONObject = jSONObject.optJSONObject("geoloc");
        if (optJSONObject != null) {
            uVar.y = optJSONObject.optDouble("lat", 0.0d);
            uVar.z = optJSONObject.optDouble("lng", 0.0d);
        }
        JSONArray optJSONArray = jSONObject.optJSONArray(N);
        if (optJSONArray != null) {
            uVar.l = new ArrayList();
            for (int i2 = 0; i2 < optJSONArray.length(); i2++) {
                bf bfVar = new bf();
                bfVar.h = optJSONArray.optJSONObject(i2).optString(O, PoiTypeDef.All);
                bfVar.i = optJSONArray.optJSONObject(i2).optString(z, PoiTypeDef.All);
                bfVar.ae = new String[]{optJSONArray.optJSONObject(i2).optString(G, PoiTypeDef.All)};
                uVar.l.add(bfVar);
            }
        }
        uVar.b(jSONObject.optInt("friend_count"));
        JSONArray optJSONArray2 = jSONObject.optJSONArray(m);
        if (optJSONArray2 != null && optJSONArray2.length() > 0) {
            uVar.a(new ArrayList());
            for (int i3 = 0; i3 < optJSONArray2.length(); i3++) {
                JSONObject jSONObject2 = optJSONArray2.getJSONObject(i3);
                aa aaVar = new aa();
                k.a();
                k.a(jSONObject2, aaVar);
                uVar.b().add(aaVar);
            }
        }
        JSONObject optJSONObject2 = jSONObject.optJSONObject(R);
        if (optJSONObject2 != null) {
            uVar.q = optJSONObject2.optString(G, PoiTypeDef.All);
            uVar.p = optJSONObject2.optString(z, PoiTypeDef.All);
            uVar.r = optJSONObject2.optString(u, PoiTypeDef.All);
            uVar.s = optJSONObject2.optString(InviteApi.KEY_URL, PoiTypeDef.All);
            optJSONObject2.optString("hostid", PoiTypeDef.All);
        }
        uVar.m = jSONObject.optInt("hot") == 1;
        uVar.o = jSONObject.optInt("free") == 1;
        if (jSONObject.optInt("today") != 1) {
            z2 = false;
        }
        uVar.n = z2;
    }

    private static void b(JSONObject jSONObject, u uVar) {
        boolean z2 = true;
        uVar.b = jSONObject.optString(z);
        uVar.f3038a = jSONObject.optString(A);
        uVar.c = jSONObject.optString(G);
        uVar.d = jSONObject.optString(p);
        uVar.e = jSONObject.optString(I);
        uVar.g = jSONObject.optString(M);
        uVar.i = jSONObject.optInt(r);
        uVar.j = jSONObject.optInt(t, uVar.j);
        uVar.a(jSONObject.optInt(K));
        uVar.c(jSONObject.optInt(L));
        uVar.a((float) jSONObject.optInt(H, -1));
        uVar.x = jSONObject.optString("city", uVar.x);
        JSONObject optJSONObject = jSONObject.optJSONObject("geoloc");
        if (optJSONObject != null) {
            uVar.y = optJSONObject.optDouble("lat", 0.0d);
            uVar.z = optJSONObject.optDouble("lng", 0.0d);
        }
        uVar.m = jSONObject.optInt("hot") == 1;
        uVar.o = jSONObject.optInt("free") == 1;
        if (jSONObject.optInt("today") != 1) {
            z2 = false;
        }
        uVar.n = z2;
    }

    public final b a(String str, int i2, Date date) {
        String str2 = String.valueOf(g) + "/comment/lists";
        HashMap hashMap = new HashMap();
        hashMap.put("feedid", new StringBuilder(String.valueOf(str)).toString());
        hashMap.put("index", new StringBuilder(String.valueOf(i2)).toString());
        hashMap.put("count", new StringBuilder(String.valueOf(20)).toString());
        if (date != null) {
            hashMap.put("timestamp", new StringBuilder(String.valueOf(date.getTime())).toString());
        }
        return new b(new JSONObject(a(str2, hashMap)).optJSONObject("data"));
    }

    public final n a(String str, String str2, int i2, int i3, String str3, String str4, String str5) {
        String str6 = String.valueOf(g) + "/comment/publish";
        HashMap hashMap = new HashMap();
        hashMap.put("feedid", str);
        hashMap.put(B, new StringBuilder(String.valueOf(str2)).toString());
        hashMap.put(C, new StringBuilder(String.valueOf(i2)).toString());
        hashMap.put(P, new StringBuilder(String.valueOf(str3)).toString());
        hashMap.put(E, new StringBuilder(String.valueOf(str4)).toString());
        hashMap.put(F, new StringBuilder(String.valueOf(str5)).toString());
        hashMap.put(D, new StringBuilder(String.valueOf(i3)).toString());
        return new n(new JSONObject(a(str6, hashMap)));
    }

    public final aa a(String str) {
        HashMap hashMap = new HashMap();
        hashMap.put("feedid", str);
        aa aaVar = new aa();
        a(new JSONObject(a(String.valueOf(g) + "/feed/profile", hashMap)).optJSONObject(w), aaVar);
        return aaVar;
    }

    public final String a(aa aaVar, File file, com.immomo.momo.plugin.b.a aVar, boolean z2, boolean z3, boolean z4, boolean z5, int i2, double d, double d2, String str) {
        JSONObject jSONObject;
        String str2 = String.valueOf(g) + "/feed/publish";
        HashMap hashMap = new HashMap();
        hashMap.put(h, new StringBuilder(String.valueOf(z2 ? 1 : 0)).toString());
        hashMap.put(i, new StringBuilder(String.valueOf(z3 ? 1 : 0)).toString());
        hashMap.put(j, new StringBuilder(String.valueOf(z4 ? 1 : 0)).toString());
        hashMap.put(k, new StringBuilder(String.valueOf(z5 ? 1 : 0)).toString());
        hashMap.put(P, new StringBuilder(String.valueOf(aaVar.b())).toString());
        hashMap.put(o, new StringBuilder(String.valueOf(i2)).toString());
        hashMap.put("lat", new StringBuilder(String.valueOf(d)).toString());
        hashMap.put("lng", new StringBuilder(String.valueOf(d2)).toString());
        hashMap.put(A, str);
        if (file != null) {
            jSONObject = new JSONObject(a(str2, hashMap, new l[]{new l(file.getName(), file, T, (byte) 0)}));
        } else if (aVar != null) {
            hashMap.put(V, aVar.g());
            hashMap.put(U, aVar.h());
            hashMap.put(W, aVar.toString());
            jSONObject = new JSONObject(a(str2, hashMap));
        } else {
            jSONObject = new JSONObject(a(str2, hashMap));
        }
        a(jSONObject.optJSONObject(w), aaVar);
        return jSONObject.getString("em");
    }

    public final String a(String str, boolean z2, boolean z3, boolean z4, boolean z5) {
        HashMap hashMap = new HashMap();
        hashMap.put(A, str);
        hashMap.put(h, z2 ? "1" : "0");
        hashMap.put(i, z3 ? "1" : "0");
        hashMap.put(j, z4 ? "1" : "0");
        hashMap.put(l, z5 ? "1" : "0");
        return new JSONObject(a(String.valueOf(g) + "/share", hashMap)).optJSONObject(w).optString("weixin_url", PoiTypeDef.All);
    }

    public final void a(String str, u uVar) {
        if (!a.a((CharSequence) str) && uVar != null) {
            JSONObject jSONObject = new JSONObject(a(String.valueOf(g) + "/profile/" + str, (Map) null));
            if (jSONObject.optJSONObject(w) != null) {
                a(jSONObject.optJSONObject(w), uVar);
            }
        }
    }

    public final boolean a(List list, int i2) {
        return a(list, (String) null, i2);
    }

    public final boolean a(List list, int i2, double d, double d2, int i3, int i4, int i5, int i6) {
        JSONArray optJSONArray;
        HashMap hashMap = new HashMap();
        hashMap.put("index", new StringBuilder(String.valueOf(i2)).toString());
        hashMap.put("count", new StringBuilder(String.valueOf(24)).toString());
        hashMap.put("lat", new StringBuilder().append(d).toString());
        hashMap.put("lng", new StringBuilder().append(d2).toString());
        hashMap.put(o, new StringBuilder().append(i3).toString());
        hashMap.put(q, new StringBuilder().append(i4).toString());
        hashMap.put(r, new StringBuilder().append(i5).toString());
        hashMap.put(s, new StringBuilder().append(i6).toString());
        JSONObject optJSONObject = new JSONObject(a(String.valueOf(g) + "/nearby?fr=" + g.q().h, hashMap)).optJSONObject(w);
        if (!(optJSONObject == null || (optJSONArray = optJSONObject.optJSONArray(x)) == null)) {
            for (int i7 = 0; i7 < optJSONArray.length(); i7++) {
                u uVar = new u();
                b(optJSONArray.getJSONObject(i7), uVar);
                list.add(uVar);
            }
        }
        return optJSONObject.optInt(y) == 1;
    }

    public final boolean a(List list, int i2, String str) {
        HashMap hashMap = new HashMap();
        hashMap.put("index", new StringBuilder(String.valueOf(i2)).toString());
        hashMap.put("count", new StringBuilder(String.valueOf(30)).toString());
        hashMap.put(A, str);
        JSONObject optJSONObject = new JSONObject(a(String.valueOf(g) + "/feed/lists?fr=" + g.q().h, hashMap)).optJSONObject(w);
        if (optJSONObject != null) {
            JSONArray optJSONArray = optJSONObject.optJSONArray("feeds");
            for (int i3 = 0; i3 < optJSONArray.length(); i3++) {
                aa aaVar = new aa();
                a(optJSONArray.getJSONObject(i3), aaVar);
                list.add(aaVar);
            }
        }
        return optJSONObject.optInt(y) == 1;
    }

    public final boolean a(List list, String str, int i2) {
        JSONArray optJSONArray;
        String str2 = String.valueOf(g) + "/history?fr=" + g.q().h;
        HashMap hashMap = new HashMap();
        if (!a.a((CharSequence) str)) {
            hashMap.put("remoteid", str);
        }
        hashMap.put("index", new StringBuilder(String.valueOf(i2)).toString());
        hashMap.put("count", new StringBuilder(String.valueOf(30)).toString());
        JSONObject optJSONObject = new JSONObject(a(str2, hashMap)).optJSONObject(w);
        if (!(optJSONObject == null || (optJSONArray = optJSONObject.optJSONArray("events")) == null)) {
            for (int i3 = 0; i3 < optJSONArray.length(); i3++) {
                u uVar = new u();
                b(optJSONArray.getJSONObject(i3), uVar);
                list.add(uVar);
            }
        }
        return optJSONObject.optInt(y) == 1;
    }

    public final boolean a(List list, List list2) {
        HashMap hashMap = new HashMap();
        hashMap.put("index", new StringBuilder(String.valueOf(0)).toString());
        hashMap.put("count", new StringBuilder(String.valueOf(20)).toString());
        JSONObject optJSONObject = new JSONObject(a(String.valueOf(g) + "/dynamic", hashMap)).optJSONObject(w);
        if (optJSONObject != null) {
            JSONArray optJSONArray = optJSONObject.optJSONArray("dynamics");
            for (int i2 = 0; i2 < optJSONArray.length(); i2++) {
                z zVar = new z();
                zVar.a(optJSONArray.getJSONObject(i2).optString("title"));
                JSONArray optJSONArray2 = optJSONArray.getJSONObject(i2).optJSONArray("events");
                ArrayList arrayList = new ArrayList();
                for (int i3 = 0; i3 < optJSONArray2.length(); i3++) {
                    arrayList.add(a(optJSONArray2.getJSONObject(i3), list2));
                }
                zVar.f3043a = arrayList;
                list.add(zVar);
            }
        }
        return optJSONObject.optInt(y) == 1;
    }

    public final b b(String str) {
        return a(str, 0, (Date) null);
    }

    public final boolean b(List list, int i2) {
        JSONArray optJSONArray;
        HashMap hashMap = new HashMap();
        hashMap.put("index", new StringBuilder(String.valueOf(i2)).toString());
        hashMap.put("count", new StringBuilder(String.valueOf(20)).toString());
        JSONObject optJSONObject = new JSONObject(a(String.valueOf(g) + "/comment/my?fr=" + g.q().h, hashMap)).optJSONObject(w);
        if (!(optJSONObject == null || (optJSONArray = optJSONObject.optJSONArray("comments")) == null)) {
            for (int i3 = 0; i3 < optJSONArray.length(); i3++) {
                ae aeVar = new ae();
                JSONObject jSONObject = optJSONArray.getJSONObject(i3);
                try {
                    aeVar.i = jSONObject.optString(A);
                } catch (Exception e) {
                }
                try {
                    ab abVar = new ab();
                    a(jSONObject.getJSONObject("feed"), abVar);
                    aeVar.g = abVar;
                } catch (Exception e2) {
                }
                try {
                    aeVar.o = jSONObject.optInt("fr", aeVar.o);
                } catch (Exception e3) {
                }
                try {
                    aeVar.j = jSONObject.optString("replycontent");
                } catch (Exception e4) {
                }
                try {
                    aeVar.h = jSONObject.optString("feedid");
                } catch (Exception e5) {
                }
                try {
                    aeVar.f = jSONObject.optString(P);
                } catch (Exception e6) {
                }
                try {
                    aeVar.a(a.a(jSONObject.optLong("create_time")));
                } catch (Exception e7) {
                }
                try {
                    aeVar.l = jSONObject.optInt(C);
                } catch (Exception e8) {
                }
                try {
                    aeVar.n = jSONObject.optInt(D);
                } catch (Exception e9) {
                }
                try {
                    aeVar.b = jSONObject.optString("owner");
                } catch (Exception e10) {
                }
                try {
                    aeVar.k = jSONObject.optString(n);
                } catch (Exception e11) {
                }
                try {
                    aeVar.m = jSONObject.optInt("replytype");
                } catch (Exception e12) {
                }
                try {
                    if (jSONObject.has(Q)) {
                        JSONObject optJSONObject2 = jSONObject.optJSONObject(Q);
                        aeVar.f2985a = new bf();
                        aeVar.f2985a.X = -1;
                        w.a(aeVar.f2985a, optJSONObject2);
                    }
                } catch (Exception e13) {
                }
                try {
                    a(jSONObject.getJSONObject("event"), new u());
                } catch (Exception e14) {
                }
                try {
                    aeVar.q = jSONObject.optString(B);
                } catch (Exception e15) {
                }
                try {
                    aeVar.d = jSONObject.optString(F);
                } catch (Exception e16) {
                }
                try {
                    aeVar.c = jSONObject.optString(E);
                } catch (Exception e17) {
                }
                try {
                    aeVar.p = jSONObject.optInt(t);
                } catch (Exception e18) {
                }
                list.add(aeVar);
            }
        }
        return optJSONObject.optInt(y) == 1;
    }

    public final boolean b(List list, String str, int i2) {
        boolean z2 = true;
        HashMap hashMap = new HashMap();
        hashMap.put(A, str);
        hashMap.put("index", new StringBuilder(String.valueOf(i2)).toString());
        hashMap.put("count", new StringBuilder(String.valueOf(24)).toString());
        JSONObject optJSONObject = new JSONObject(a(String.valueOf(g) + "/members", hashMap)).optJSONObject(w);
        if (optJSONObject.optInt(y, 0) != 1) {
            z2 = false;
        }
        if (optJSONObject != null) {
            JSONArray jSONArray = optJSONObject.getJSONArray("list");
            for (int i3 = 0; i3 < jSONArray.length(); i3++) {
                list.add(a(jSONArray.getJSONObject(i3)));
            }
        }
        return z2;
    }

    public final String c(String str) {
        String str2 = String.valueOf(g) + "/feed/remove";
        HashMap hashMap = new HashMap();
        hashMap.put("feedid", str);
        this.e.a(hashMap);
        return new JSONObject(a(str2, hashMap)).optString("em");
    }

    public final String d(String str) {
        if (a.a((CharSequence) str)) {
            return null;
        }
        String str2 = String.valueOf(g) + "/join";
        HashMap hashMap = new HashMap();
        hashMap.put(A, str);
        return new JSONObject(a(str2, hashMap)).optString("em", PoiTypeDef.All);
    }

    public final String e(String str) {
        if (a.a((CharSequence) str)) {
            return null;
        }
        String str2 = String.valueOf(g) + "/quit";
        HashMap hashMap = new HashMap();
        hashMap.put(A, str);
        return new JSONObject(a(str2, hashMap)).optString("em", PoiTypeDef.All);
    }

    public final String f(String str) {
        String str2 = String.valueOf(g) + "/comment/remove";
        HashMap hashMap = new HashMap();
        hashMap.put(n, str);
        return new JSONObject(a(str2, hashMap)).optString("em");
    }
}
