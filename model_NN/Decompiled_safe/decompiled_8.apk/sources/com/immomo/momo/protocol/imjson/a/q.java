package com.immomo.momo.protocol.imjson.a;

import com.immomo.a.a.d.b;
import com.immomo.a.a.f;
import java.util.HashMap;
import java.util.Map;

public final class q implements f {

    /* renamed from: a  reason: collision with root package name */
    private Map f2908a = new HashMap();

    public final void a(Object obj, f fVar) {
        this.f2908a.put(obj.toString(), fVar);
    }

    public final boolean a(b bVar) {
        f fVar = (f) this.f2908a.get(bVar.h());
        if (fVar == null) {
            return false;
        }
        fVar.a(bVar);
        return true;
    }
}
