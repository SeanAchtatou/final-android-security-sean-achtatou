package com.immomo.momo.protocol.imjson.util;

import android.os.Bundle;
import com.immomo.a.a.f.b;
import com.immomo.momo.a;
import com.immomo.momo.android.c.g;
import com.immomo.momo.service.af;
import com.immomo.momo.service.bean.Message;
import com.immomo.momo.service.bean.aj;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.util.ao;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public final class c implements aj {

    /* renamed from: a  reason: collision with root package name */
    private static boolean f2950a = false;
    private static List b = new ArrayList();
    private static File[] c = null;
    private static int d = -1;
    private static c e = null;
    private bf f = null;
    private boolean g = false;
    private boolean h = false;
    private g i = null;

    public static c a() {
        if (e == null) {
            e = new c();
        }
        return e;
    }

    private void a(String str, int i2) {
        Message message = new Message(true);
        message.setContent(str);
        message.remoteId = "1602";
        message.msgId = b.a();
        message.chatType = 1;
        message.contentType = i2;
        message.status = 4;
        message.remoteUser = f();
        af.c().a(message);
        Bundle bundle = new Bundle();
        bundle.putString("remoteuserid", message.remoteId);
        bundle.putString("msgid", message.msgId);
        bundle.putInt("stype", message.contentType);
        ArrayList arrayList = new ArrayList();
        arrayList.add(message);
        bundle.putSerializable("messagearray", arrayList);
        af.c().a(bundle);
        com.immomo.momo.g.d().a(bundle, "actions.logger");
    }

    public static boolean b() {
        return c() && f2950a;
    }

    public static boolean c() {
        return com.immomo.momo.g.q() != null && (com.immomo.momo.g.q().h.equals("100935") || com.immomo.momo.g.q().h.equals("100882") || com.immomo.momo.g.q().h.equals("3368661") || com.immomo.momo.g.q().h.equals("3185745") || com.immomo.momo.g.q().h.equals("100889") || com.immomo.momo.g.q().h.equals("100993") || com.immomo.momo.g.q().h.equals("1602020") || com.immomo.momo.g.q().h.equals("2308411") || com.immomo.momo.g.q().h.equals("13329529") || com.immomo.momo.g.q().h.equals("1561220") || com.immomo.momo.g.q().h.equals("1602016") || com.immomo.momo.g.q().h.equals("23503905") || com.immomo.momo.g.q().h.equals("100823"));
    }

    public static boolean e() {
        f2950a = false;
        return true;
    }

    public static String g() {
        return "调试小秘书";
    }

    private static File[] h() {
        File[] listFiles = a.p().listFiles();
        if (listFiles.length > 1) {
            for (int i2 = 1; i2 < listFiles.length; i2++) {
                File file = listFiles[i2];
                int i3 = i2;
                while (i3 > 0 && listFiles[i3 - 1].lastModified() < file.lastModified()) {
                    listFiles[i3] = listFiles[i3 - 1];
                    i3--;
                }
                listFiles[i3] = file;
            }
        }
        return listFiles;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:124:0x042e, code lost:
        if (com.immomo.momo.protocol.imjson.util.c.d >= (com.immomo.momo.protocol.imjson.util.c.c.length - 1)) goto L_0x0464;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:126:?, code lost:
        com.immomo.momo.protocol.imjson.util.c.d++;
        r0 = new java.io.FileInputStream(com.immomo.momo.protocol.imjson.util.c.c[com.immomo.momo.protocol.imjson.util.c.d]);
        r1 = android.support.v4.b.a.b(r0);
        android.support.v4.b.a.a((java.io.Closeable) r0);
        a(new java.lang.String(r1), 0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:128:0x0454, code lost:
        r11.d();
        a("日志获取失败", 5);
        com.immomo.momo.protocol.imjson.util.c.d--;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:129:0x0464, code lost:
        a("没有下一条日志", 0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:250:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(com.immomo.momo.protocol.imjson.task.MessageTask r11) {
        /*
            r10 = this;
            r4 = 5
            r8 = 1048576(0x100000, double:5.180654E-318)
            r6 = 1024(0x400, double:5.06E-321)
            r1 = 0
            r5 = 1149239296(0x44800000, float:1024.0)
        L_0x0009:
            com.immomo.momo.service.bean.Message r0 = r11.f()
            java.lang.String r0 = r0.getContent()
            java.lang.String r2 = "logo"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x0025
            r0 = 1
            com.immomo.momo.protocol.imjson.util.c.f2950a = r0
            java.lang.String r0 = "日志已开启, 使用logc关闭"
            r10.a(r0, r4)
        L_0x0021:
            r11.g_()
        L_0x0024:
            return
        L_0x0025:
            java.lang.String r2 = "logc"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x0035
            com.immomo.momo.protocol.imjson.util.c.f2950a = r1
            java.lang.String r0 = "日志已关闭"
            r10.a(r0, r4)
            goto L_0x0021
        L_0x0035:
            java.lang.String r2 = "conn"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x004c
            android.content.Context r0 = com.immomo.momo.g.c()
            android.content.Intent r1 = new android.content.Intent
            java.lang.String r2 = com.immomo.momo.android.service.XService.d
            r1.<init>(r2)
            r0.sendBroadcast(r1)
            goto L_0x0021
        L_0x004c:
            java.lang.String r2 = "pi"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x0084
            boolean r0 = com.immomo.momo.protocol.imjson.util.c.f2950a
            if (r0 != 0) goto L_0x0064
            com.immomo.momo.service.bean.Message r0 = r11.f()
            java.lang.String r1 = "logo"
            r0.setContent(r1)
            r10.a(r11)
        L_0x0064:
            com.immomo.a.a.d.b r0 = new com.immomo.a.a.d.b
            r0.<init>()
            java.lang.String r1 = "pi"
            r0.a(r1)
            com.immomo.momo.protocol.imjson.task.SimpePacketTask r1 = new com.immomo.momo.protocol.imjson.task.SimpePacketTask
            com.immomo.momo.protocol.imjson.task.g r2 = com.immomo.momo.protocol.imjson.task.g.FinesseExpress
            r1.<init>(r2, r0)
            r1.c()
            com.immomo.momo.protocol.imjson.util.d r0 = new com.immomo.momo.protocol.imjson.util.d
            r0.<init>(r11)
            r1.a(r0)
            com.immomo.momo.protocol.imjson.p.a(r1)
            goto L_0x0024
        L_0x0084:
            java.lang.String r2 = "he"
            boolean r2 = r2.equals(r0)
            if (r2 != 0) goto L_0x0094
            java.lang.String r2 = "help"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x010b
        L_0x0094:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r2 = "conn  重连服务器\n"
            r0.append(r2)
            java.lang.String r2 = "logo  开启日志打印\n"
            r0.append(r2)
            java.lang.String r2 = "logc  关闭日志打印\n"
            r0.append(r2)
            java.lang.String r2 = "pi  向服务器发送ping\n"
            r0.append(r2)
            java.lang.String r2 = "pi xxx.xx.x.x  ping某个地址, 使用exec stop结束它\n"
            r0.append(r2)
            java.lang.String r2 = "get  获取get命令列表\n"
            r0.append(r2)
            java.lang.String r2 = "get imjser  获取IMJ当前连接的服务器\n"
            r0.append(r2)
            java.lang.String r2 = "get imjst  获取IMJ当前状态\n"
            r0.append(r2)
            java.lang.String r2 = "set  获取set命令列表\n"
            r0.append(r2)
            java.lang.String r2 = "set pival (val>0) \n\t设置心跳的时间间隔(秒)。需要重连 @see conn。\n"
            r0.append(r2)
            java.lang.String r2 = "set pito (val>0) \n\t设置心跳的响应超时时间(秒, int>0)。。需要重连 @see conn。\n"
            r0.append(r2)
            java.lang.String r2 = "send  发送消息原文(不做修改，直接写给服务器，消息格式必须是JSON)\n"
            r0.append(r2)
            java.lang.String r2 = "shutdown  关闭通讯服务, 重新打开应用依然会触发重启。\n"
            r0.append(r2)
            java.lang.String r2 = "log  查看日志目录下最新一条日志。\n"
            r0.append(r2)
            java.lang.String r2 = "log next  查看下一条日志\n"
            r0.append(r2)
            java.lang.String r2 = "log pre  查看上一条日志\n"
            r0.append(r2)
            java.lang.String r2 = "log list  查看日志列表\n"
            r0.append(r2)
            java.lang.String r2 = "du  查看流量使用情况\n"
            r0.append(r2)
            java.lang.String r2 = "du reset  重置流量统计\n"
            r0.append(r2)
            java.lang.String r2 = "exec  执行linux命令，例如: exec telnet 192.168.1.1 8080, 对部分设备来说，命令前需要加上busybox，如: exec busybox ifconfig\n"
            r0.append(r2)
            java.lang.String r2 = "exec stop  杀死所有linux命令开启的子线程"
            r0.append(r2)
            java.lang.String r0 = r0.toString()
            r10.a(r0, r1)
            goto L_0x0021
        L_0x010b:
            java.lang.String r2 = "get imjser"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x013f
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = com.immomo.momo.protocol.imjson.l.d
            java.lang.String r3 = java.lang.String.valueOf(r3)
            r2.<init>(r3)
            java.lang.String r3 = ":"
            java.lang.StringBuilder r2 = r2.append(r3)
            int r3 = com.immomo.momo.protocol.imjson.l.e
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            r0.append(r2)
            java.lang.String r0 = r0.toString()
            r10.a(r0, r1)
            goto L_0x0021
        L_0x013f:
            java.lang.String r2 = "get imjst"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x01ea
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            boolean r2 = com.immomo.momo.protocol.imjson.l.c
            if (r2 != 0) goto L_0x01b6
            java.lang.String r2 = "连接已断开\n"
            r0.append(r2)
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "已断开："
            r2.<init>(r3)
            long r3 = com.immomo.momo.protocol.imjson.l.f
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = "秒"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            java.lang.StringBuilder r2 = r0.append(r2)
            r3 = 10
            r2.append(r3)
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "已重试："
            r2.<init>(r3)
            int r3 = com.immomo.momo.protocol.imjson.l.g
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = "次"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            r0.append(r2)
            boolean r2 = com.immomo.momo.protocol.imjson.l.f2926a
            if (r2 != 0) goto L_0x01ad
            r2 = 10
            java.lang.StringBuilder r2 = r0.append(r2)
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r4 = "警告："
            r3.<init>(r4)
            java.lang.String r4 = com.immomo.momo.protocol.imjson.l.b
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r3 = r3.toString()
            r2.append(r3)
        L_0x01ad:
            java.lang.String r0 = r0.toString()
            r10.a(r0, r1)
            goto L_0x0021
        L_0x01b6:
            java.lang.String r2 = "连接已建立\n"
            r0.append(r2)
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "host: "
            r2.<init>(r3)
            java.lang.String r3 = com.immomo.momo.protocol.imjson.l.d
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            java.lang.StringBuilder r2 = r0.append(r2)
            r3 = 10
            r2.append(r3)
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "port: "
            r2.<init>(r3)
            int r3 = com.immomo.momo.protocol.imjson.l.e
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            r0.append(r2)
            goto L_0x01ad
        L_0x01ea:
            java.lang.String r2 = "get"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x020a
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r2 = "get imjser  获取IMJ当前连接发服务器\n"
            r0.append(r2)
            java.lang.String r2 = "get imjst  获取IMJ当前状态"
            r0.append(r2)
            java.lang.String r0 = r0.toString()
            r10.a(r0, r1)
            goto L_0x0021
        L_0x020a:
            java.lang.String r2 = "set"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x022a
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r2 = "set pival (val>0) \n\t设置心跳的时间间隔(秒)。需要重连 @see conn。\n"
            r0.append(r2)
            java.lang.String r2 = "set pito (val>0) \n\t设置心跳的响应超时时间(秒, int>0)。。需要重连 @see conn。"
            r0.append(r2)
            java.lang.String r0 = r0.toString()
            r10.a(r0, r1)
            goto L_0x0021
        L_0x022a:
            java.lang.String r2 = "set pival"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x0253
            r2 = 10
            int r3 = r0.length()     // Catch:{ Exception -> 0x0795 }
            java.lang.String r0 = r0.substring(r2, r3)     // Catch:{ Exception -> 0x0795 }
            int r1 = java.lang.Integer.parseInt(r0)     // Catch:{ Exception -> 0x0795 }
        L_0x0240:
            if (r1 > 0) goto L_0x0247
            r11.h_()
            goto L_0x0024
        L_0x0247:
            com.immomo.momo.protocol.imjson.util.Debugger$2 r0 = new com.immomo.momo.protocol.imjson.util.Debugger$2
            com.immomo.momo.protocol.imjson.task.g r2 = com.immomo.momo.protocol.imjson.task.g.FinesseExpress
            r0.<init>(r10, r2, r11, r1)
            com.immomo.momo.protocol.imjson.p.a(r0)
            goto L_0x0021
        L_0x0253:
            java.lang.String r2 = "set pito"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x027c
            r2 = 9
            int r3 = r0.length()     // Catch:{ Exception -> 0x0792 }
            java.lang.String r0 = r0.substring(r2, r3)     // Catch:{ Exception -> 0x0792 }
            int r1 = java.lang.Integer.parseInt(r0)     // Catch:{ Exception -> 0x0792 }
        L_0x0269:
            if (r1 > 0) goto L_0x0270
            r11.h_()
            goto L_0x0024
        L_0x0270:
            com.immomo.momo.protocol.imjson.util.Debugger$3 r0 = new com.immomo.momo.protocol.imjson.util.Debugger$3
            com.immomo.momo.protocol.imjson.task.g r2 = com.immomo.momo.protocol.imjson.task.g.FinesseExpress
            r0.<init>(r10, r2, r11, r1)
            com.immomo.momo.protocol.imjson.p.a(r0)
            goto L_0x0021
        L_0x027c:
            java.lang.String r2 = "exec stop"
            boolean r2 = r0.equals(r2)
            if (r2 == 0) goto L_0x02a1
            java.util.List r0 = com.immomo.momo.protocol.imjson.util.c.b
            java.util.Iterator r1 = r0.iterator()
        L_0x028a:
            boolean r0 = r1.hasNext()
            if (r0 != 0) goto L_0x0297
            java.util.List r0 = com.immomo.momo.protocol.imjson.util.c.b
            r0.clear()
            goto L_0x0021
        L_0x0297:
            java.lang.Object r0 = r1.next()
            com.immomo.momo.protocol.imjson.util.e r0 = (com.immomo.momo.protocol.imjson.util.e) r0
            r0.a()
            goto L_0x028a
        L_0x02a1:
            java.lang.String r2 = "pi"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x02df
            r1 = 3
            int r2 = r0.length()     // Catch:{ Exception -> 0x02bd }
            java.lang.String r0 = r0.substring(r1, r2)     // Catch:{ Exception -> 0x02bd }
            boolean r1 = android.support.v4.b.a.a(r0)     // Catch:{ Exception -> 0x02bd }
            if (r1 == 0) goto L_0x02c0
            r11.d()     // Catch:{ Exception -> 0x02bd }
            goto L_0x0024
        L_0x02bd:
            r0 = move-exception
            goto L_0x0021
        L_0x02c0:
            com.immomo.momo.protocol.imjson.util.e r1 = new com.immomo.momo.protocol.imjson.util.e     // Catch:{ Exception -> 0x02bd }
            r2 = 0
            r1.<init>(r2)     // Catch:{ Exception -> 0x02bd }
            java.util.List r2 = com.immomo.momo.protocol.imjson.util.c.b     // Catch:{ Exception -> 0x02bd }
            r2.add(r1)     // Catch:{ Exception -> 0x02bd }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x02bd }
            java.lang.String r3 = "ping "
            r2.<init>(r3)     // Catch:{ Exception -> 0x02bd }
            java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ Exception -> 0x02bd }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x02bd }
            r1.a(r0)     // Catch:{ Exception -> 0x02bd }
            goto L_0x0021
        L_0x02df:
            java.lang.String r2 = "exec"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x0312
            r1 = 5
            int r2 = r0.length()     // Catch:{ Exception -> 0x02ff }
            java.lang.String r0 = r0.substring(r1, r2)     // Catch:{ Exception -> 0x02ff }
            java.lang.String r1 = r0.trim()     // Catch:{ Exception -> 0x02ff }
            boolean r1 = android.support.v4.b.a.a(r1)     // Catch:{ Exception -> 0x02ff }
            if (r1 == 0) goto L_0x0302
            r11.d()     // Catch:{ Exception -> 0x02ff }
            goto L_0x0024
        L_0x02ff:
            r0 = move-exception
            goto L_0x0021
        L_0x0302:
            com.immomo.momo.protocol.imjson.util.e r1 = new com.immomo.momo.protocol.imjson.util.e     // Catch:{ Exception -> 0x02ff }
            r2 = 0
            r1.<init>(r2)     // Catch:{ Exception -> 0x02ff }
            java.util.List r2 = com.immomo.momo.protocol.imjson.util.c.b     // Catch:{ Exception -> 0x02ff }
            r2.add(r1)     // Catch:{ Exception -> 0x02ff }
            r1.a(r0)     // Catch:{ Exception -> 0x02ff }
            goto L_0x0021
        L_0x0312:
            java.lang.String r2 = "send"
            boolean r2 = r0.equals(r2)
            if (r2 == 0) goto L_0x0324
            r11.d()
            java.lang.String r0 = "命令使用错误 send 后应该包含一个消息实体"
            r10.a(r0, r4)
            goto L_0x0024
        L_0x0324:
            java.lang.String r2 = "send"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x0367
            int r1 = r0.length()
            java.lang.String r0 = r0.substring(r4, r1)
            boolean r1 = android.support.v4.b.a.a(r0)
            if (r1 == 0) goto L_0x033f
            r11.d()
            goto L_0x0024
        L_0x033f:
            com.immomo.a.a.d.b r1 = com.immomo.a.a.d.b.e(r0)     // Catch:{ JSONException -> 0x034f }
            com.immomo.momo.protocol.imjson.util.Debugger$4 r2 = new com.immomo.momo.protocol.imjson.util.Debugger$4     // Catch:{ JSONException -> 0x034f }
            com.immomo.momo.protocol.imjson.task.g r3 = com.immomo.momo.protocol.imjson.task.g.FinesseExpress     // Catch:{ JSONException -> 0x034f }
            r2.<init>(r3, r11, r1)     // Catch:{ JSONException -> 0x034f }
            com.immomo.momo.protocol.imjson.p.a(r2)     // Catch:{ JSONException -> 0x034f }
            goto L_0x0024
        L_0x034f:
            r1 = move-exception
            r11.d()
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r2 = "命令使用错误, 消息实体不是合法的JSON格式:"
            r1.<init>(r2)
            java.lang.StringBuilder r0 = r1.append(r0)
            java.lang.String r0 = r0.toString()
            r10.a(r0, r4)
            goto L_0x0024
        L_0x0367:
            java.lang.String r2 = "shutdown"
            boolean r2 = r0.equals(r2)
            if (r2 == 0) goto L_0x037d
            com.immomo.momo.MomoApplication r0 = com.immomo.momo.g.d()
            r0.m()
            java.lang.String r0 = "通讯服务已关闭"
            r10.a(r0, r4)
            goto L_0x0021
        L_0x037d:
            java.lang.String r2 = "log"
            boolean r2 = r0.equals(r2)
            if (r2 == 0) goto L_0x03c4
            java.io.File[] r0 = h()     // Catch:{ Exception -> 0x039a }
            com.immomo.momo.protocol.imjson.util.c.c = r0     // Catch:{ Exception -> 0x039a }
            if (r0 == 0) goto L_0x0392
            java.io.File[] r0 = com.immomo.momo.protocol.imjson.util.c.c     // Catch:{ Exception -> 0x039a }
            int r0 = r0.length     // Catch:{ Exception -> 0x039a }
            if (r0 != 0) goto L_0x03a5
        L_0x0392:
            java.lang.String r0 = "日志目录是空的"
            r1 = 0
            r10.a(r0, r1)     // Catch:{ Exception -> 0x039a }
            goto L_0x0021
        L_0x039a:
            r0 = move-exception
            r11.d()
            java.lang.String r0 = "日志获取失败"
            r10.a(r0, r4)
            goto L_0x0024
        L_0x03a5:
            java.io.FileInputStream r0 = new java.io.FileInputStream     // Catch:{ Exception -> 0x039a }
            java.io.File[] r1 = com.immomo.momo.protocol.imjson.util.c.c     // Catch:{ Exception -> 0x039a }
            r2 = 0
            r1 = r1[r2]     // Catch:{ Exception -> 0x039a }
            r0.<init>(r1)     // Catch:{ Exception -> 0x039a }
            byte[] r1 = android.support.v4.b.a.b(r0)     // Catch:{ Exception -> 0x039a }
            android.support.v4.b.a.a(r0)     // Catch:{ Exception -> 0x039a }
            java.lang.String r0 = new java.lang.String     // Catch:{ Exception -> 0x039a }
            r0.<init>(r1)     // Catch:{ Exception -> 0x039a }
            r1 = 0
            r10.a(r0, r1)     // Catch:{ Exception -> 0x039a }
            r0 = 0
            com.immomo.momo.protocol.imjson.util.c.d = r0     // Catch:{ Exception -> 0x039a }
            goto L_0x0021
        L_0x03c4:
            java.lang.String r2 = "log list"
            boolean r2 = r0.equals(r2)
            if (r2 == 0) goto L_0x040c
            java.io.File[] r2 = h()
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            int r4 = r2.length
            r0 = r1
        L_0x03d7:
            if (r0 < r4) goto L_0x03fa
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r3 = r3.toString()
            java.lang.String r3 = java.lang.String.valueOf(r3)
            r0.<init>(r3)
            java.lang.String r3 = "\ncount="
            java.lang.StringBuilder r0 = r0.append(r3)
            int r2 = r2.length
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r0 = r0.toString()
            r10.a(r0, r1)
            goto L_0x0021
        L_0x03fa:
            r5 = r2[r0]
            java.lang.String r5 = r5.getName()
            java.lang.StringBuilder r5 = r3.append(r5)
            java.lang.String r6 = "\n"
            r5.append(r6)
            int r0 = r0 + 1
            goto L_0x03d7
        L_0x040c:
            java.lang.String r2 = "log next"
            boolean r2 = r0.equals(r2)
            if (r2 == 0) goto L_0x046b
            int r0 = com.immomo.momo.protocol.imjson.util.c.d
            if (r0 >= 0) goto L_0x0427
            java.io.File[] r0 = com.immomo.momo.protocol.imjson.util.c.c
            if (r0 != 0) goto L_0x0427
            com.immomo.momo.service.bean.Message r0 = r11.f()
            java.lang.String r2 = "log"
            r0.setContent(r2)
            goto L_0x0009
        L_0x0427:
            int r0 = com.immomo.momo.protocol.imjson.util.c.d
            java.io.File[] r2 = com.immomo.momo.protocol.imjson.util.c.c
            int r2 = r2.length
            int r2 = r2 + -1
            if (r0 >= r2) goto L_0x0464
            int r0 = com.immomo.momo.protocol.imjson.util.c.d     // Catch:{ Exception -> 0x0453 }
            int r0 = r0 + 1
            com.immomo.momo.protocol.imjson.util.c.d = r0     // Catch:{ Exception -> 0x0453 }
            java.io.FileInputStream r0 = new java.io.FileInputStream     // Catch:{ Exception -> 0x0453 }
            java.io.File[] r1 = com.immomo.momo.protocol.imjson.util.c.c     // Catch:{ Exception -> 0x0453 }
            int r2 = com.immomo.momo.protocol.imjson.util.c.d     // Catch:{ Exception -> 0x0453 }
            r1 = r1[r2]     // Catch:{ Exception -> 0x0453 }
            r0.<init>(r1)     // Catch:{ Exception -> 0x0453 }
            byte[] r1 = android.support.v4.b.a.b(r0)     // Catch:{ Exception -> 0x0453 }
            android.support.v4.b.a.a(r0)     // Catch:{ Exception -> 0x0453 }
            java.lang.String r0 = new java.lang.String     // Catch:{ Exception -> 0x0453 }
            r0.<init>(r1)     // Catch:{ Exception -> 0x0453 }
            r1 = 0
            r10.a(r0, r1)     // Catch:{ Exception -> 0x0453 }
            goto L_0x0021
        L_0x0453:
            r0 = move-exception
            r11.d()
            java.lang.String r0 = "日志获取失败"
            r10.a(r0, r4)
            int r0 = com.immomo.momo.protocol.imjson.util.c.d
            int r0 = r0 + -1
            com.immomo.momo.protocol.imjson.util.c.d = r0
            goto L_0x0024
        L_0x0464:
            java.lang.String r0 = "没有下一条日志"
            r10.a(r0, r1)
            goto L_0x0021
        L_0x046b:
            java.lang.String r2 = "log pre"
            boolean r2 = r0.equals(r2)
            if (r2 == 0) goto L_0x04d0
            int r0 = com.immomo.momo.protocol.imjson.util.c.d
            if (r0 >= 0) goto L_0x0486
            java.io.File[] r0 = com.immomo.momo.protocol.imjson.util.c.c
            if (r0 != 0) goto L_0x0486
            com.immomo.momo.service.bean.Message r0 = r11.f()
            java.lang.String r2 = "log"
            r0.setContent(r2)
            goto L_0x0009
        L_0x0486:
            int r0 = com.immomo.momo.protocol.imjson.util.c.d
            if (r0 <= 0) goto L_0x04c9
            int r0 = com.immomo.momo.protocol.imjson.util.c.d
            int r0 = r0 + -1
            java.io.File[] r2 = com.immomo.momo.protocol.imjson.util.c.c
            int r2 = r2.length
            int r2 = r2 + -1
            if (r0 >= r2) goto L_0x04c9
            int r0 = com.immomo.momo.protocol.imjson.util.c.d     // Catch:{ Exception -> 0x04b8 }
            int r0 = r0 + -1
            com.immomo.momo.protocol.imjson.util.c.d = r0     // Catch:{ Exception -> 0x04b8 }
            java.io.FileInputStream r0 = new java.io.FileInputStream     // Catch:{ Exception -> 0x04b8 }
            java.io.File[] r1 = com.immomo.momo.protocol.imjson.util.c.c     // Catch:{ Exception -> 0x04b8 }
            int r2 = com.immomo.momo.protocol.imjson.util.c.d     // Catch:{ Exception -> 0x04b8 }
            r1 = r1[r2]     // Catch:{ Exception -> 0x04b8 }
            r0.<init>(r1)     // Catch:{ Exception -> 0x04b8 }
            byte[] r1 = android.support.v4.b.a.b(r0)     // Catch:{ Exception -> 0x04b8 }
            android.support.v4.b.a.a(r0)     // Catch:{ Exception -> 0x04b8 }
            java.lang.String r0 = new java.lang.String     // Catch:{ Exception -> 0x04b8 }
            r0.<init>(r1)     // Catch:{ Exception -> 0x04b8 }
            r1 = 0
            r10.a(r0, r1)     // Catch:{ Exception -> 0x04b8 }
            goto L_0x0021
        L_0x04b8:
            r0 = move-exception
            r11.d()
            java.lang.String r0 = "日志获取失败"
            r10.a(r0, r4)
            int r0 = com.immomo.momo.protocol.imjson.util.c.d
            int r0 = r0 + 1
            com.immomo.momo.protocol.imjson.util.c.d = r0
            goto L_0x0024
        L_0x04c9:
            java.lang.String r0 = "没有上一条日志"
            r10.a(r0, r1)
            goto L_0x0021
        L_0x04d0:
            java.lang.String r2 = "du"
            boolean r2 = r0.equals(r2)
            if (r2 == 0) goto L_0x076a
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r2 = "总使用: "
            r0.append(r2)
            long r2 = com.immomo.momo.util.ar.a()
            int r4 = (r2 > r8 ? 1 : (r2 == r8 ? 0 : -1))
            if (r4 < 0) goto L_0x067c
            long r2 = r2 / r6
            float r2 = (float) r2
            float r2 = r2 / r5
            java.lang.String r2 = android.support.v4.b.a.a(r2)
            java.lang.StringBuilder r2 = r0.append(r2)
            java.lang.String r3 = "MB\n"
            r2.append(r3)
        L_0x04fa:
            java.lang.String r2 = "通讯服务: "
            r0.append(r2)
            long r2 = com.immomo.momo.util.ar.c()
            int r4 = (r2 > r8 ? 1 : (r2 == r8 ? 0 : -1))
            if (r4 < 0) goto L_0x068d
            long r2 = r2 / r6
            float r2 = (float) r2
            float r2 = r2 / r5
            java.lang.String r2 = android.support.v4.b.a.a(r2)
            java.lang.StringBuilder r2 = r0.append(r2)
            java.lang.String r3 = "MB\n"
            r2.append(r3)
        L_0x0517:
            java.lang.String r2 = "心跳: "
            r0.append(r2)
            long r2 = com.immomo.momo.util.ar.i()
            int r4 = (r2 > r8 ? 1 : (r2 == r8 ? 0 : -1))
            if (r4 < 0) goto L_0x069e
            long r2 = r2 / r6
            float r2 = (float) r2
            float r2 = r2 / r5
            java.lang.String r2 = android.support.v4.b.a.a(r2)
            java.lang.StringBuilder r2 = r0.append(r2)
            java.lang.String r3 = "MB\n"
            r2.append(r3)
        L_0x0534:
            java.lang.String r2 = "图片下载: "
            r0.append(r2)
            long r2 = com.immomo.momo.util.ar.b()
            int r4 = (r2 > r8 ? 1 : (r2 == r8 ? 0 : -1))
            if (r4 < 0) goto L_0x06af
            long r2 = r2 / r6
            float r2 = (float) r2
            float r2 = r2 / r5
            java.lang.String r2 = android.support.v4.b.a.a(r2)
            java.lang.StringBuilder r2 = r0.append(r2)
            java.lang.String r3 = "MB\n"
            r2.append(r3)
        L_0x0551:
            java.lang.String r2 = "语音下载: "
            r0.append(r2)
            long r2 = com.immomo.momo.util.ar.d()
            int r4 = (r2 > r8 ? 1 : (r2 == r8 ? 0 : -1))
            if (r4 < 0) goto L_0x06c0
            long r2 = r2 / r6
            float r2 = (float) r2
            float r2 = r2 / r5
            java.lang.String r2 = android.support.v4.b.a.a(r2)
            java.lang.StringBuilder r2 = r0.append(r2)
            java.lang.String r3 = "MB\n"
            r2.append(r3)
        L_0x056e:
            java.lang.String r2 = "图片上传: "
            r0.append(r2)
            long r2 = com.immomo.momo.util.ar.e()
            int r4 = (r2 > r8 ? 1 : (r2 == r8 ? 0 : -1))
            if (r4 < 0) goto L_0x06d1
            long r2 = r2 / r6
            float r2 = (float) r2
            float r2 = r2 / r5
            java.lang.String r2 = android.support.v4.b.a.a(r2)
            java.lang.StringBuilder r2 = r0.append(r2)
            java.lang.String r3 = "MB\n"
            r2.append(r3)
        L_0x058b:
            java.lang.String r2 = "语音上传: "
            r0.append(r2)
            long r2 = com.immomo.momo.util.ar.f()
            int r4 = (r2 > r8 ? 1 : (r2 == r8 ? 0 : -1))
            if (r4 < 0) goto L_0x06e2
            long r2 = r2 / r6
            float r2 = (float) r2
            float r2 = r2 / r5
            java.lang.String r2 = android.support.v4.b.a.a(r2)
            java.lang.StringBuilder r2 = r0.append(r2)
            java.lang.String r3 = "MB\n"
            r2.append(r3)
        L_0x05a8:
            java.lang.String r2 = "最大图片下载: "
            r0.append(r2)
            long r2 = com.immomo.momo.util.ar.m()
            int r4 = (r2 > r8 ? 1 : (r2 == r8 ? 0 : -1))
            if (r4 < 0) goto L_0x06f3
            long r2 = r2 / r6
            float r2 = (float) r2
            float r2 = r2 / r5
            java.lang.String r2 = android.support.v4.b.a.a(r2)
            java.lang.StringBuilder r2 = r0.append(r2)
            java.lang.String r3 = "MB\n"
            r2.append(r3)
        L_0x05c5:
            java.lang.String r2 = "最大语音下载: "
            r0.append(r2)
            long r2 = com.immomo.momo.util.ar.l()
            int r4 = (r2 > r8 ? 1 : (r2 == r8 ? 0 : -1))
            if (r4 < 0) goto L_0x0704
            long r2 = r2 / r6
            float r2 = (float) r2
            float r2 = r2 / r5
            java.lang.String r2 = android.support.v4.b.a.a(r2)
            java.lang.StringBuilder r2 = r0.append(r2)
            java.lang.String r3 = "MB\n"
            r2.append(r3)
        L_0x05e2:
            java.lang.String r2 = "最大图片上传: "
            r0.append(r2)
            long r2 = com.immomo.momo.util.ar.j()
            int r4 = (r2 > r8 ? 1 : (r2 == r8 ? 0 : -1))
            if (r4 < 0) goto L_0x0715
            long r2 = r2 / r6
            float r2 = (float) r2
            float r2 = r2 / r5
            java.lang.String r2 = android.support.v4.b.a.a(r2)
            java.lang.StringBuilder r2 = r0.append(r2)
            java.lang.String r3 = "MB\n"
            r2.append(r3)
        L_0x05ff:
            java.lang.String r2 = "最大语音上传: "
            r0.append(r2)
            long r2 = com.immomo.momo.util.ar.k()
            int r4 = (r2 > r8 ? 1 : (r2 == r8 ? 0 : -1))
            if (r4 < 0) goto L_0x0726
            long r2 = r2 / r6
            float r2 = (float) r2
            float r2 = r2 / r5
            java.lang.String r2 = android.support.v4.b.a.a(r2)
            java.lang.StringBuilder r2 = r0.append(r2)
            java.lang.String r3 = "MB\n"
            r2.append(r3)
        L_0x061c:
            java.lang.String r2 = "其它: "
            r0.append(r2)
            long r2 = com.immomo.momo.util.ar.n()
            int r4 = (r2 > r8 ? 1 : (r2 == r8 ? 0 : -1))
            if (r4 < 0) goto L_0x0737
            long r2 = r2 / r6
            float r2 = (float) r2
            float r2 = r2 / r5
            java.lang.String r2 = android.support.v4.b.a.a(r2)
            java.lang.StringBuilder r2 = r0.append(r2)
            java.lang.String r3 = "MB\n"
            r2.append(r3)
        L_0x0639:
            java.lang.String r2 = "表情图片下载: "
            r0.append(r2)
            long r2 = com.immomo.momo.util.ar.g()
            int r4 = (r2 > r8 ? 1 : (r2 == r8 ? 0 : -1))
            if (r4 < 0) goto L_0x0748
            long r2 = r2 / r6
            float r2 = (float) r2
            float r2 = r2 / r5
            java.lang.String r2 = android.support.v4.b.a.a(r2)
            java.lang.StringBuilder r2 = r0.append(r2)
            java.lang.String r3 = "MB\n"
            r2.append(r3)
        L_0x0656:
            java.lang.String r2 = "表情包下载: "
            r0.append(r2)
            long r2 = com.immomo.momo.util.ar.h()
            int r4 = (r2 > r8 ? 1 : (r2 == r8 ? 0 : -1))
            if (r4 < 0) goto L_0x0759
            long r2 = r2 / r6
            float r2 = (float) r2
            float r2 = r2 / r5
            java.lang.String r2 = android.support.v4.b.a.a(r2)
            java.lang.StringBuilder r2 = r0.append(r2)
            java.lang.String r3 = "MB\n"
            r2.append(r3)
        L_0x0673:
            java.lang.String r0 = r0.toString()
            r10.a(r0, r1)
            goto L_0x0021
        L_0x067c:
            float r2 = (float) r2
            float r2 = r2 / r5
            java.lang.String r2 = android.support.v4.b.a.a(r2)
            java.lang.StringBuilder r2 = r0.append(r2)
            java.lang.String r3 = "KB\n"
            r2.append(r3)
            goto L_0x04fa
        L_0x068d:
            float r2 = (float) r2
            float r2 = r2 / r5
            java.lang.String r2 = android.support.v4.b.a.a(r2)
            java.lang.StringBuilder r2 = r0.append(r2)
            java.lang.String r3 = "KB\n"
            r2.append(r3)
            goto L_0x0517
        L_0x069e:
            float r2 = (float) r2
            float r2 = r2 / r5
            java.lang.String r2 = android.support.v4.b.a.a(r2)
            java.lang.StringBuilder r2 = r0.append(r2)
            java.lang.String r3 = "KB\n"
            r2.append(r3)
            goto L_0x0534
        L_0x06af:
            float r2 = (float) r2
            float r2 = r2 / r5
            java.lang.String r2 = android.support.v4.b.a.a(r2)
            java.lang.StringBuilder r2 = r0.append(r2)
            java.lang.String r3 = "KB\n"
            r2.append(r3)
            goto L_0x0551
        L_0x06c0:
            float r2 = (float) r2
            float r2 = r2 / r5
            java.lang.String r2 = android.support.v4.b.a.a(r2)
            java.lang.StringBuilder r2 = r0.append(r2)
            java.lang.String r3 = "KB\n"
            r2.append(r3)
            goto L_0x056e
        L_0x06d1:
            float r2 = (float) r2
            float r2 = r2 / r5
            java.lang.String r2 = android.support.v4.b.a.a(r2)
            java.lang.StringBuilder r2 = r0.append(r2)
            java.lang.String r3 = "KB\n"
            r2.append(r3)
            goto L_0x058b
        L_0x06e2:
            float r2 = (float) r2
            float r2 = r2 / r5
            java.lang.String r2 = android.support.v4.b.a.a(r2)
            java.lang.StringBuilder r2 = r0.append(r2)
            java.lang.String r3 = "KB\n"
            r2.append(r3)
            goto L_0x05a8
        L_0x06f3:
            float r2 = (float) r2
            float r2 = r2 / r5
            java.lang.String r2 = android.support.v4.b.a.a(r2)
            java.lang.StringBuilder r2 = r0.append(r2)
            java.lang.String r3 = "KB\n"
            r2.append(r3)
            goto L_0x05c5
        L_0x0704:
            float r2 = (float) r2
            float r2 = r2 / r5
            java.lang.String r2 = android.support.v4.b.a.a(r2)
            java.lang.StringBuilder r2 = r0.append(r2)
            java.lang.String r3 = "KB\n"
            r2.append(r3)
            goto L_0x05e2
        L_0x0715:
            float r2 = (float) r2
            float r2 = r2 / r5
            java.lang.String r2 = android.support.v4.b.a.a(r2)
            java.lang.StringBuilder r2 = r0.append(r2)
            java.lang.String r3 = "KB\n"
            r2.append(r3)
            goto L_0x05ff
        L_0x0726:
            float r2 = (float) r2
            float r2 = r2 / r5
            java.lang.String r2 = android.support.v4.b.a.a(r2)
            java.lang.StringBuilder r2 = r0.append(r2)
            java.lang.String r3 = "KB\n"
            r2.append(r3)
            goto L_0x061c
        L_0x0737:
            float r2 = (float) r2
            float r2 = r2 / r5
            java.lang.String r2 = android.support.v4.b.a.a(r2)
            java.lang.StringBuilder r2 = r0.append(r2)
            java.lang.String r3 = "KB\n"
            r2.append(r3)
            goto L_0x0639
        L_0x0748:
            float r2 = (float) r2
            float r2 = r2 / r5
            java.lang.String r2 = android.support.v4.b.a.a(r2)
            java.lang.StringBuilder r2 = r0.append(r2)
            java.lang.String r3 = "KB\n"
            r2.append(r3)
            goto L_0x0656
        L_0x0759:
            float r2 = (float) r2
            float r2 = r2 / r5
            java.lang.String r2 = android.support.v4.b.a.a(r2)
            java.lang.StringBuilder r2 = r0.append(r2)
            java.lang.String r3 = "KB\n"
            r2.append(r3)
            goto L_0x0673
        L_0x076a:
            java.lang.String r1 = "du reset"
            boolean r1 = r0.equals(r1)
            if (r1 == 0) goto L_0x0777
            com.immomo.momo.util.ar.o()
            goto L_0x0021
        L_0x0777:
            r11.d()
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r0 = java.lang.String.valueOf(r0)
            r1.<init>(r0)
            java.lang.String r0 = "是不存在的命令。发送help可以查看命令列表。"
            java.lang.StringBuilder r0 = r1.append(r0)
            java.lang.String r0 = r0.toString()
            r10.a(r0, r4)
            goto L_0x0024
        L_0x0792:
            r0 = move-exception
            goto L_0x0269
        L_0x0795:
            r0 = move-exception
            goto L_0x0240
        */
        throw new UnsupportedOperationException("Method not decompiled: com.immomo.momo.protocol.imjson.util.c.a(com.immomo.momo.protocol.imjson.task.MessageTask):void");
    }

    public final void a(String str) {
        a(str, 5);
    }

    public final void b(String str) {
        a(str, 0);
    }

    public final boolean d() {
        ao.a((CharSequence) "调试模式已激活");
        a("调试模式已激活, 发送help获取命令列表", 5);
        return true;
    }

    public final bf f() {
        if (this.f != null) {
            return this.f;
        }
        bf bfVar = new bf("1602");
        bfVar.ae = new String[]{getLoadImageId()};
        bfVar.i = "调试小秘书";
        this.f = bfVar;
        return bfVar;
    }

    public final g getImageCallback() {
        return this.i;
    }

    public final String getLoadImageId() {
        return "7A14EB7D-5D48-8F21-186B-CCFDC54C0079";
    }

    public final boolean isImageLoading() {
        return this.g;
    }

    public final boolean isImageLoadingFailed() {
        return this.h;
    }

    public final boolean isImageMultipleDiaplay() {
        return false;
    }

    public final boolean isImageUrl() {
        return false;
    }

    public final void setImageCallback(g gVar) {
        this.i = gVar;
    }

    public final void setImageLoadFailed(boolean z) {
        this.h = z;
    }

    public final void setImageLoading(boolean z) {
        this.g = z;
    }
}
