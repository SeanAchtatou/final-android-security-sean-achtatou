package com.immomo.momo.protocol.a;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import com.amap.mapapi.location.LocationManagerProxy;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.a;
import com.immomo.momo.android.c.ak;
import com.immomo.momo.android.view.a.aj;
import com.immomo.momo.g;
import com.immomo.momo.protocol.a.a.p;
import com.immomo.momo.service.bean.GameApp;
import com.immomo.momo.service.bean.ab;
import com.immomo.momo.service.bean.ad;
import com.immomo.momo.service.bean.at;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.service.bean.bi;
import com.immomo.momo.service.bean.bj;
import com.immomo.momo.service.bean.e;
import com.immomo.momo.service.bean.q;
import com.immomo.momo.service.o;
import com.immomo.momo.service.y;
import com.immomo.momo.util.ar;
import com.immomo.momo.util.jni.Codec;
import com.sina.sdk.api.message.InviteApi;
import java.io.File;
import java.io.InputStream;
import java.io.InterruptedIOException;
import java.net.HttpURLConnection;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import javax.net.ssl.SSLException;
import mm.purchasesdk.PurchaseCode;
import org.json.JSONArray;
import org.json.JSONObject;

public final class w extends p {
    private static w f = null;
    private static String g = (String.valueOf(b) + "/personal");
    private static String h = (String.valueOf(b) + "/profile");
    private static String i = (String.valueOf(b) + "/setting");
    private static String j = a.k;
    private static String k = a.j;
    private static String l = Codec.iii();
    private static String m = a.o;

    public static Bitmap a(StringBuilder sb) {
        HttpURLConnection b = q.b(String.valueOf(b.replace("https", "http")) + "/v2" + Codec.rscccc() + "/verifyapp?width=" + g.a(100.0f) + "&height=" + g.a(44.0f), null, null);
        try {
            InputStream inputStream = b.getInputStream();
            byte[] b2 = android.support.v4.b.a.b(inputStream);
            ar.b((long) b2.length);
            inputStream.close();
            String headerField = b.getHeaderField("set-cookie");
            if (!android.support.v4.b.a.a((CharSequence) headerField)) {
                sb.append(headerField.split(";")[0]);
            }
            q.b(b);
            b.disconnect();
            return BitmapFactory.decodeByteArray(b2, 0, b2.length);
        } catch (InterruptedIOException e) {
            q.a(b);
            throw e;
        } catch (UnknownHostException e2) {
            q.a(b);
            throw e2;
        } catch (SSLException e3) {
            q.a(b);
            throw e3;
        } catch (Exception e4) {
            throw e4;
        }
    }

    public static w a() {
        if (f == null) {
            f = new w();
        }
        return f;
    }

    public static void a(bf bfVar, JSONObject jSONObject) {
        boolean z = false;
        bfVar.h = jSONObject.getString("momoid");
        bfVar.i = jSONObject.optString("name", bfVar.i);
        bfVar.l = jSONObject.optString("remarkname", bfVar.l);
        bfVar.I = jSONObject.optInt("age", bfVar.I);
        if (jSONObject.has("vip")) {
            JSONObject optJSONObject = jSONObject.optJSONObject("vip");
            bfVar.ai = optJSONObject.optInt("level", bfVar.ai);
            bfVar.al = optJSONObject.optInt("year", bfVar.al);
            bfVar.a(optJSONObject.optInt("valid", bfVar.b() ? 1 : 0) == 1);
        }
        bfVar.H = jSONObject.optString("sex", bfVar.H);
        bfVar.K = jSONObject.optString("constellation", bfVar.K);
        bfVar.b(jSONObject.optString("sign", bfVar.l()));
        bfVar.az = a(jSONObject.optLong("sign_time"));
        bfVar.a(a(jSONObject.optLong("loc_timesec", bfVar.a() / 1000)));
        bfVar.a((float) jSONObject.optInt("distance", (int) bfVar.d()));
        bfVar.U = jSONObject.optDouble("acc", bfVar.U);
        bfVar.P = jSONObject.optString("relation", bfVar.P);
        bfVar.J = jSONObject.optString("birthday", bfVar.J);
        bfVar.aD = a(jSONObject.optLong("followtime"));
        bfVar.N = jSONObject.optString("job", bfVar.N);
        bfVar.M = jSONObject.optString("industry", bfVar.M);
        bfVar.aJ = jSONObject.optInt("group_role", bfVar.aJ);
        bfVar.ae = a(jSONObject.optJSONArray("photos")) == null ? bfVar.ae : a(jSONObject.optJSONArray("photos"));
        bfVar.ac = jSONObject.has("baned");
        if (jSONObject.has("avatar")) {
            if (bfVar.ae == null || bfVar.ae.length <= 0) {
                bfVar.ae = new String[]{jSONObject.optString("avatar")};
            } else {
                bfVar.ae[0] = jSONObject.optString("avatar");
            }
        }
        if (jSONObject.has("sina_user_id")) {
            bfVar.am = jSONObject.optString("sina_user_id");
            bfVar.an = !android.support.v4.b.a.a(bfVar.am);
        }
        if (jSONObject.has("sina_vip_desc")) {
            bfVar.ao = jSONObject.optString("sina_vip_desc");
            bfVar.ap = !android.support.v4.b.a.a(bfVar.ao);
        }
        if (jSONObject.has("tencent_user_id")) {
            bfVar.as = jSONObject.optString("tencent_user_id");
            bfVar.at = !android.support.v4.b.a.a(bfVar.as);
        }
        if (jSONObject.has("tencent_vip_desc")) {
            bfVar.au = !android.support.v4.b.a.a(jSONObject.optString("tencent_vip_desc"));
        }
        if (jSONObject.has("renren_user_id")) {
            bfVar.aq = jSONObject.optString("renren_user_id");
            if (!android.support.v4.b.a.a((CharSequence) bfVar.aq)) {
                z = true;
            }
            bfVar.ar = z;
        }
        if (jSONObject.has("douban_user_id")) {
            bfVar.av = jSONObject.optString("douban_user_id");
            android.support.v4.b.a.a((CharSequence) bfVar.av);
        }
        if (jSONObject.has("signex")) {
            JSONObject jSONObject2 = jSONObject.getJSONObject("signex");
            bfVar.c(jSONObject2.optString("desc"));
            bfVar.R = jSONObject2.optString("pic");
        }
        if (android.support.v4.b.a.a((CharSequence) bfVar.m())) {
            bfVar.c(bfVar.l());
            bfVar.R = null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.protocol.a.w.a(org.json.JSONObject, com.immomo.momo.service.bean.ad, boolean):void
     arg types: [org.json.JSONObject, com.immomo.momo.service.bean.ad, int]
     candidates:
      com.immomo.momo.protocol.a.w.a(java.util.List, double, double):int
      com.immomo.momo.protocol.a.w.a(java.util.List, int, com.immomo.momo.service.bean.at):int
      com.immomo.momo.protocol.a.w.a(java.lang.String, java.lang.String, java.lang.String):com.immomo.momo.service.bean.bf
      com.immomo.momo.protocol.a.w.a(double, double, int):com.immomo.momo.service.bean.bj
      com.immomo.momo.protocol.a.w.a(java.io.File, java.lang.String, java.lang.String):java.lang.String
      com.immomo.momo.protocol.a.w.a(java.lang.String, int, java.io.File):java.lang.String
      com.immomo.momo.protocol.a.w.a(java.lang.String, int, java.lang.String):java.lang.String
      com.immomo.momo.protocol.a.w.a(com.immomo.momo.service.bean.bf, java.util.Map, java.util.Map):void
      com.immomo.momo.protocol.a.w.a(java.util.List, java.util.concurrent.atomic.AtomicInteger, int):boolean
      com.immomo.momo.protocol.a.w.a(java.lang.String, java.lang.String, boolean):java.lang.String[]
      com.immomo.momo.protocol.a.p.a(java.lang.String, int, com.immomo.momo.android.activity.fs):com.immomo.momo.util.v
      com.immomo.momo.protocol.a.p.a(java.lang.String, java.io.File, com.immomo.momo.android.c.w):void
      com.immomo.momo.protocol.a.p.a(java.lang.String, java.util.Map, java.util.Map):java.lang.String
      com.immomo.momo.protocol.a.p.a(java.lang.String, java.util.Map, com.immomo.momo.protocol.a.l[]):java.lang.String
      com.immomo.momo.protocol.a.w.a(org.json.JSONObject, com.immomo.momo.service.bean.ad, boolean):void */
    private void a(JSONObject jSONObject, ad adVar, boolean z) {
        if (jSONObject != null) {
            JSONObject optJSONObject = z ? jSONObject.optJSONObject("fback") : jSONObject;
            adVar.e = optJSONObject.optString("name");
            adVar.b = optJSONObject.optString("desc");
            optJSONObject.optString("momoid");
            adVar.d = optJSONObject.optString("avatar");
            adVar.c = optJSONObject.optString("pic");
            adVar.f2984a = android.support.v4.b.a.a(optJSONObject.optLong("create_time"));
            JSONArray optJSONArray = jSONObject.optJSONArray("replys");
            if (optJSONArray != null) {
                adVar.f = new ArrayList();
                for (int i2 = 0; i2 < optJSONArray.length(); i2++) {
                    ad adVar2 = new ad();
                    a(optJSONArray.getJSONObject(i2), adVar2, false);
                    adVar.f.add(adVar2);
                }
            }
        }
    }

    private static void a(JSONObject jSONObject, bf bfVar) {
        boolean z = true;
        bfVar.h = jSONObject.optString("momoid", bfVar.h);
        bfVar.i = jSONObject.optString("name", bfVar.i);
        bfVar.H = jSONObject.optString("sex", bfVar.H);
        bfVar.I = jSONObject.optInt("age", bfVar.I);
        bfVar.ac = jSONObject.has("baned");
        bfVar.u = jSONObject.optInt("feed_count", bfVar.u);
        if (jSONObject.has("vip")) {
            JSONObject optJSONObject = jSONObject.optJSONObject("vip");
            bfVar.ai = optJSONObject.optInt("level", bfVar.ai);
            bfVar.al = optJSONObject.optInt("year", bfVar.al);
            bfVar.a(optJSONObject.optInt("valid", bfVar.b() ? 1 : 0) == 1);
        }
        bfVar.G = jSONObject.optString(j, bfVar.G);
        bfVar.ay = android.support.v4.b.a.h(jSONObject.optString("regtime")) == null ? bfVar.ay : android.support.v4.b.a.h(jSONObject.optString("regtime"));
        bfVar.aw = "YES".equals(jSONObject.optString("bind_email", bfVar.aw ? "YES" : "NO"));
        if (jSONObject.has("birthday")) {
            if (jSONObject.get("birthday") instanceof JSONObject) {
                Date a2 = a(jSONObject.getJSONObject("birthday").optLong("sec"));
                if (a2 != null) {
                    Calendar instance = Calendar.getInstance();
                    instance.setTime(a2);
                    bfVar.J = String.valueOf(instance.get(1)) + "-" + (instance.get(2) + 1) + "-" + instance.get(5);
                }
            } else {
                bfVar.J = jSONObject.optString("birthday");
            }
        }
        bfVar.b(jSONObject.optString("sign", bfVar.l()));
        bfVar.az = a(jSONObject.optLong("sign_time"));
        bfVar.ae = a(jSONObject.optJSONArray("photos")) == null ? bfVar.ae : a(jSONObject.optJSONArray("photos"));
        bfVar.S = jSONObject.optDouble("loc_lat", bfVar.S);
        bfVar.T = jSONObject.optDouble("loc_lng", bfVar.T);
        bfVar.U = jSONObject.optDouble("acc", bfVar.U);
        bfVar.V = jSONObject.optInt("deviation", 0) == 1;
        long optLong = jSONObject.optLong("loc_timesec");
        if (optLong > 0) {
            bfVar.a(a(optLong));
        }
        bfVar.a((float) jSONObject.optInt("distance", (int) bfVar.d()));
        bfVar.K = jSONObject.optString("constellation", bfVar.K);
        bfVar.L = jSONObject.optString("interest", bfVar.L);
        bfVar.N = jSONObject.optString("job", bfVar.N);
        bfVar.D = jSONObject.optString("website", bfVar.D);
        bfVar.C = jSONObject.optString("company", bfVar.C);
        bfVar.Q = jSONObject.optString("school", bfVar.Q);
        bfVar.E = jSONObject.optString("aboutme", bfVar.E);
        bfVar.F = jSONObject.optString("hangout", bfVar.F);
        bfVar.c = jSONObject.optString("countrycode", bfVar.c);
        bfVar.b = jSONObject.optString("phonenumber", bfVar.b);
        bfVar.d = jSONObject.optString("phone", bfVar.d);
        bfVar.g = !android.support.v4.b.a.a(bfVar.c) && !android.support.v4.b.a.a(bfVar.b);
        bfVar.B = jSONObject.optInt("regtype", bfVar.B);
        bfVar.P = jSONObject.optString("relation", bfVar.P);
        bfVar.l = jSONObject.optString("remarkname", bfVar.l);
        bfVar.M = jSONObject.optString("industry", bfVar.M);
        bfVar.p = jSONObject.optString("wb_name", bfVar.p);
        bfVar.q = jSONObject.optString("qqwb_name", bfVar.q);
        bfVar.r = jSONObject.optString("rr_name", bfVar.r);
        if (jSONObject.has("tencent_user_id")) {
            bfVar.as = jSONObject.optString("tencent_user_id", bfVar.as);
            if (!android.support.v4.b.a.a((CharSequence) bfVar.as)) {
                bfVar.at = true;
            } else {
                bfVar.at = false;
            }
        }
        if (jSONObject.has("tencent_vip_desc")) {
            if (!android.support.v4.b.a.a((CharSequence) jSONObject.optString("tencent_user_id", PoiTypeDef.All))) {
                bfVar.au = true;
            } else {
                bfVar.au = false;
            }
        }
        if (jSONObject.has("version")) {
            bfVar.X = (long) jSONObject.optInt("version");
        }
        if (jSONObject.has("group_role")) {
            bfVar.aJ = jSONObject.optInt("group_role");
        }
        if (jSONObject.has("sina_user_id")) {
            bfVar.am = jSONObject.optString("sina_user_id");
            bfVar.an = !android.support.v4.b.a.a(bfVar.am);
        }
        if (jSONObject.has("sina_vip_desc")) {
            bfVar.ao = jSONObject.optString("sina_vip_desc");
            bfVar.ap = !android.support.v4.b.a.a(bfVar.ao);
        }
        if (jSONObject.has("renren_user_id")) {
            bfVar.aq = jSONObject.optString("renren_user_id");
            if (android.support.v4.b.a.a((CharSequence) bfVar.aq)) {
                z = false;
            }
            bfVar.ar = z;
        }
        if (jSONObject.has("douban_user_id")) {
            bfVar.av = jSONObject.optString("douban_user_id");
            android.support.v4.b.a.a((CharSequence) bfVar.av);
        }
        bfVar.ag = jSONObject.optString("feedid", PoiTypeDef.All);
        if (jSONObject.has("feed")) {
            try {
                ab abVar = new ab();
                k.a();
                k.a(jSONObject.getJSONObject("feed"), abVar);
                bfVar.ah = abVar;
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            bfVar.ah = null;
        }
        if (jSONObject.has("app_info")) {
            try {
                bfVar.O = new ArrayList();
                JSONArray jSONArray = jSONObject.getJSONArray("app_info");
                for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                    JSONObject jSONObject2 = jSONArray.getJSONObject(i2);
                    GameApp gameApp = new GameApp();
                    gameApp.appname = jSONObject2.optString("app_name");
                    gameApp.appicon = jSONObject2.optString("app_icon");
                    gameApp.appdownload = jSONObject2.optString("url_download");
                    gameApp.appid = jSONObject2.getString("app_id");
                    gameApp.appURI = jSONObject2.optString(InviteApi.KEY_URL);
                    bfVar.O.add(gameApp);
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        bfVar.af = jSONObject.optString("background", bfVar.af);
    }

    private static List b(JSONArray jSONArray) {
        ArrayList arrayList = new ArrayList();
        if (jSONArray != null && jSONArray.length() > 0) {
            for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                bf bfVar = new bf();
                a(bfVar, jSONArray.getJSONObject(i2));
                arrayList.add(bfVar);
            }
        }
        return arrayList;
    }

    private static List h() {
        ArrayList arrayList = new ArrayList();
        arrayList.add("momoid");
        arrayList.add("name");
        arrayList.add("sign");
        arrayList.add("remarkname");
        arrayList.add("photos");
        arrayList.add("loc_timesec");
        arrayList.add("sex");
        arrayList.add("distance");
        arrayList.add("age");
        arrayList.add("relation");
        arrayList.add("sina_user_id");
        arrayList.add("sina_vip_desc");
        arrayList.add("renren_user_id");
        arrayList.add("group_role");
        arrayList.add("baned");
        arrayList.add("industry");
        arrayList.add("level");
        return arrayList;
    }

    public final int a(int i2, List list) {
        HashMap hashMap = new HashMap();
        hashMap.put("index", new StringBuilder(String.valueOf(i2)).toString());
        hashMap.put("count", new StringBuilder(String.valueOf((int) PurchaseCode.BILL_DYMARK_CREATE_ERROR)).toString());
        JSONObject jSONObject = new JSONObject(a(String.valueOf(b) + "/following", hashMap));
        JSONArray jSONArray = jSONObject.getJSONArray("list");
        for (int i3 = 0; i3 < jSONArray.length(); i3++) {
            bf bfVar = new bf();
            list.add(bfVar);
            a(bfVar, jSONArray.getJSONObject(i3));
        }
        return jSONObject.getInt("total");
    }

    public final int a(List list, double d, double d2) {
        HashMap hashMap = new HashMap();
        hashMap.put("lat", new Double(d).toString());
        hashMap.put("lng", new Double(d2).toString());
        hashMap.put("index", "0");
        hashMap.put("count", "20");
        hashMap.put("temp_uid", g.I());
        JSONObject jSONObject = new JSONObject(a(String.valueOf(b) + "/welcome", hashMap));
        int optInt = jSONObject.optInt("count");
        JSONArray jSONArray = jSONObject.getJSONArray("list");
        for (int i2 = 0; i2 < jSONArray.length(); i2++) {
            bf bfVar = new bf();
            list.add(bfVar);
            a(bfVar, jSONArray.getJSONObject(i2));
        }
        return optInt;
    }

    public final int a(List list, int i2) {
        HashMap hashMap = new HashMap();
        hashMap.put("index", new StringBuilder(String.valueOf(i2)).toString());
        hashMap.put("count", new StringBuilder(String.valueOf(50)).toString());
        JSONObject jSONObject = new JSONObject(a(String.valueOf(b) + "/followers", hashMap));
        JSONArray jSONArray = jSONObject.getJSONArray("list");
        for (int i3 = 0; i3 < jSONArray.length(); i3++) {
            bf bfVar = new bf();
            list.add(bfVar);
            a(bfVar, jSONArray.getJSONObject(i3));
        }
        return jSONObject.getInt("total");
    }

    public final int a(List list, int i2, at atVar) {
        HashMap hashMap = new HashMap();
        hashMap.put("index", new StringBuilder(String.valueOf(i2)).toString());
        hashMap.put("count", new StringBuilder(String.valueOf((int) PurchaseCode.QUERY_FROZEN)).toString());
        JSONObject jSONObject = new JSONObject(a(String.valueOf(b) + "/friend", hashMap));
        JSONArray jSONArray = jSONObject.getJSONArray("list");
        for (int i3 = 0; i3 < jSONArray.length(); i3++) {
            bf bfVar = new bf();
            list.add(bfVar);
            a(bfVar, jSONArray.getJSONObject(i3));
        }
        if (atVar != null) {
            atVar.k = jSONObject.optLong("version", atVar.k);
            atVar.a("bothlist_version", Long.valueOf(atVar.k));
        }
        return jSONObject.getInt("total");
    }

    public final long a(long j2, List list) {
        HashMap hashMap = new HashMap();
        hashMap.put("version", new StringBuilder(String.valueOf(j2)).toString());
        JSONObject optJSONObject = new JSONObject(a(String.valueOf(b) + "/friend/lists", hashMap)).optJSONObject("data");
        if (optJSONObject == null) {
            return 0;
        }
        long optLong = optJSONObject.optLong("version", 0);
        JSONArray optJSONArray = optJSONObject.optJSONArray("friends");
        long currentTimeMillis = System.currentTimeMillis() / 1000;
        if (optJSONArray != null) {
            for (int i2 = 0; i2 < optJSONArray.length(); i2++) {
                bf bfVar = new bf(optJSONArray.getString(i2));
                bfVar.aD = a(currentTimeMillis);
                list.add(bfVar);
            }
        }
        return optLong;
    }

    public final Location a(List list, int i2, StringBuilder sb, StringBuilder sb2, String str, int i3) {
        HashMap hashMap = new HashMap();
        hashMap.put("time", new StringBuilder(String.valueOf(i2)).toString());
        hashMap.put("sex", str);
        hashMap.put("activetime", new StringBuilder(String.valueOf(i3)).toString());
        JSONObject jSONObject = new JSONObject(a(String.valueOf(b) + "/roam/common", hashMap)).getJSONObject("data");
        JSONArray jSONArray = jSONObject.getJSONArray("list");
        for (int i4 = 0; i4 < jSONArray.length(); i4++) {
            bf bfVar = new bf();
            a(bfVar, jSONArray.getJSONObject(i4));
            list.add(bfVar);
        }
        Location location = new Location(LocationManagerProxy.NETWORK_PROVIDER);
        location.setLatitude(jSONObject.getDouble("lat"));
        location.setLongitude(jSONObject.getDouble("lng"));
        sb.append(jSONObject.optString("address"));
        sb2.append(jSONObject.optString("city"));
        return location;
    }

    public final bf a(String str, String str2, String str3) {
        HashMap hashMap = new HashMap();
        hashMap.put("appid", str);
        hashMap.put("alipay_user_id", str2);
        hashMap.put("auth_code", str3);
        hashMap.putAll(a.B());
        JSONObject optJSONObject = new JSONObject(a(String.valueOf(b) + "/aoplogin", hashMap)).optJSONObject("data");
        bf bfVar = new bf();
        if (optJSONObject != null) {
            bfVar.i = optJSONObject.optString("name");
            bfVar.h = optJSONObject.getString("momoid");
            bfVar.G = optJSONObject.optString(j);
            bfVar.W = optJSONObject.getString(m);
        }
        return bfVar;
    }

    public final bf a(String str, String str2, String str3, Map map, String str4, AtomicInteger atomicInteger) {
        HashMap hashMap = new HashMap();
        hashMap.put(l, str);
        hashMap.put("etype", "2");
        hashMap.put(k, str2);
        hashMap.putAll(map);
        if (android.support.v4.b.a.f(str3)) {
            hashMap.put("access_token", str3);
        }
        if (android.support.v4.b.a.f(str4)) {
            hashMap.put("alipay_user_id", str4);
        }
        bf bfVar = new bf();
        JSONObject jSONObject = new JSONObject(a(String.valueOf(b) + "/v2" + a.h, hashMap));
        JSONObject jSONObject2 = jSONObject.getJSONObject("data");
        bfVar.i = jSONObject2.optString("name");
        bfVar.h = jSONObject2.getString("momoid");
        bfVar.G = jSONObject2.optString(j);
        bfVar.W = jSONObject2.getString(m);
        atomicInteger.set(jSONObject.getInt("timesec"));
        return bfVar;
    }

    public final bj a(double d, double d2, int i2) {
        HashMap hashMap = new HashMap();
        hashMap.put("lat", new Double(d).toString());
        hashMap.put("lng", new Double(d2).toString());
        hashMap.put("index", "0");
        hashMap.put("count", String.valueOf(i2));
        hashMap.put("temp_uid", g.I());
        JSONObject jSONObject = new JSONObject(a(String.valueOf(b) + "/welcome", hashMap));
        bj bjVar = new bj();
        bjVar.b = new ArrayList();
        bjVar.c = new ArrayList();
        bjVar.f3014a = jSONObject.getString("list_desc");
        JSONArray optJSONArray = jSONObject.optJSONArray("list");
        int i3 = 0;
        while (true) {
            int i4 = i3;
            if (i4 >= optJSONArray.length()) {
                break;
            }
            bf bfVar = new bf();
            bjVar.b.add(bfVar);
            JSONObject jSONObject2 = optJSONArray.getJSONObject(i4);
            bfVar.i = jSONObject2.optString("name", bfVar.i);
            bfVar.H = jSONObject2.optString("sex", bfVar.H);
            bfVar.K = jSONObject2.optString("constellation", bfVar.K);
            bfVar.b(jSONObject2.optString("sign", bfVar.l()));
            bfVar.az = a(jSONObject2.optLong("sign_time"));
            bfVar.a(a(jSONObject2.optLong("loc_timesec", bfVar.a() / 1000)));
            bfVar.a((float) jSONObject2.optInt("distance", (int) bfVar.d()));
            bfVar.U = jSONObject2.optDouble("acc", bfVar.U);
            bfVar.ae = a(jSONObject2.optJSONArray("photos")) == null ? bfVar.ae : a(jSONObject2.optJSONArray("photos"));
            bfVar.ac = jSONObject2.has("baned");
            if (jSONObject2.has("avatar")) {
                if (bfVar.ae == null || bfVar.ae.length <= 0) {
                    bfVar.ae = new String[]{jSONObject2.optString("avatar")};
                } else {
                    bfVar.ae[0] = jSONObject2.optString("avatar");
                }
            }
            i3 = i4 + 1;
        }
        if (jSONObject.has("banners")) {
            JSONArray jSONArray = jSONObject.getJSONArray("banners");
            for (int i5 = 0; i5 < jSONArray.length(); i5++) {
                JSONObject jSONObject3 = jSONArray.getJSONObject(i5);
                bi biVar = new bi();
                jSONObject3.optString("background");
                jSONObject3.optString("avatar");
                jSONObject3.optString("name");
                jSONObject3.optLong("distance");
                jSONObject3.optString("title");
                jSONObject3.optString("desc");
                bjVar.c.add(biVar);
            }
        }
        return bjVar;
    }

    public final Integer a(int i2) {
        HashMap hashMap = new HashMap();
        hashMap.put("hiddenmode", new StringBuilder(String.valueOf(i2)).toString());
        return Integer.valueOf(new JSONObject(a(String.valueOf(i) + "/sethiddenmode", hashMap)).optInt("hiddenmode"));
    }

    public final String a(File file, String str, String str2) {
        HashMap hashMap = new HashMap();
        hashMap.put("cookie", "SESSIONID=" + str2);
        HashMap hashMap2 = new HashMap();
        hashMap2.put("guid", str);
        this.e.a((Object) ("updateAvatar, imageFile=" + file.getPath()));
        l lVar = new l(file.getName(), file, "avatarimg", (byte) 0);
        return new JSONObject(a(String.valueOf(g) + "/avatar", hashMap2, new l[]{lVar}, hashMap)).getJSONObject("data").getString("avatar");
    }

    public final String a(String str, int i2, File file) {
        String str2 = String.valueOf(b) + "/feedback/publish";
        HashMap hashMap = new HashMap();
        hashMap.put("desc", str);
        hashMap.put("type", String.valueOf(i2));
        if (file == null) {
            return new JSONObject(a(str2, hashMap)).optString("msg");
        }
        return new JSONObject(a(str2, hashMap, new l[]{new l(file.getName(), file, "photo", (byte) 0)})).optString("msg");
    }

    public final String a(String str, int i2, String str2) {
        String str3 = String.valueOf(b) + "/renren_invite/" + g.q().h;
        HashMap hashMap = new HashMap();
        hashMap.put("user_id", str);
        hashMap.put("source", new StringBuilder(String.valueOf(i2)).toString());
        hashMap.put("gid", str2);
        return new JSONObject(a(str3, hashMap)).optString("msg");
    }

    public final String a(String str, String str2, String str3, boolean z) {
        HashMap hashMap = new HashMap();
        hashMap.put("phonenumber", str2);
        hashMap.put("countrycode", str);
        hashMap.put("deny", z ? "1" : "0");
        hashMap.put("code", str3);
        hashMap.put("temp_uid", g.I());
        hashMap.put(Codec.Dse(), g.X());
        hashMap.put(Codec.etr968ww(), new StringBuilder(String.valueOf(Codec.isEmulator() ? 1 : 0)).toString());
        return new JSONObject(a(String.valueOf(b) + "/v2/verify/autocheck", hashMap)).optJSONObject("data").getString("verifycode");
    }

    public final List a(String str, int i2, boolean z, int i3, int i4, int i5, aj ajVar, String str2) {
        HashMap hashMap = new HashMap();
        hashMap.put("loctype", new StringBuilder().append(g.q().aH).toString());
        hashMap.put("lat", new StringBuilder(String.valueOf(g.q().S)).toString());
        hashMap.put("lng", new StringBuilder(String.valueOf(g.q().T)).toString());
        hashMap.put("acc", new StringBuilder(String.valueOf(g.q().U)).toString());
        hashMap.put("locater", new StringBuilder(String.valueOf(g.q().aI)).toString());
        hashMap.put("sex", str);
        hashMap.put("count", new StringBuilder(String.valueOf(24)).toString());
        hashMap.put("index", new StringBuilder(String.valueOf(i2)).toString());
        hashMap.put("save", z ? "YES" : "NO");
        hashMap.put("activetime", new StringBuilder(String.valueOf(i3)).toString());
        hashMap.put("agerange", new StringBuilder(String.valueOf(i4)).toString());
        hashMap.put("industry", str2);
        hashMap.put("constellation", new StringBuilder(String.valueOf(i5)).toString());
        hashMap.put("social", new StringBuilder(String.valueOf(ajVar.a())).toString());
        this.e.a(hashMap);
        JSONArray jSONArray = new JSONObject(a(String.valueOf(b) + "/nearby", hashMap)).getJSONArray("list");
        ArrayList arrayList = new ArrayList();
        for (int i6 = 0; i6 < jSONArray.length(); i6++) {
            bf bfVar = new bf();
            arrayList.add(bfVar);
            a(bfVar, jSONArray.getJSONObject(i6));
        }
        return arrayList;
    }

    public final List a(Collection collection, int i2) {
        String a2 = android.support.v4.b.a.a(collection, ",");
        HashMap hashMap = new HashMap();
        hashMap.put("phones", com.immomo.momo.util.a.a().a(a2, Codec.a(g.q().h)));
        hashMap.put("type", new StringBuilder().append(i2).toString());
        hashMap.put(Codec.du(), g.I());
        String a3 = a(String.valueOf(b) + "/contacts/phone/upload", hashMap);
        if (i2 == 1) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        JSONArray jSONArray = new JSONArray(com.immomo.momo.util.a.a().b(new JSONObject(a3).optString("contacts"), Codec.a(g.q().h)));
        if (jSONArray.length() > 0) {
            for (int i3 = 0; i3 < jSONArray.length(); i3++) {
                JSONObject jSONObject = jSONArray.getJSONObject(i3);
                com.immomo.momo.service.bean.g gVar = new com.immomo.momo.service.bean.g();
                gVar.c = jSONObject.optInt("invited");
                gVar.f3025a = jSONObject.optString("momoid");
                gVar.d = jSONObject.optString("phone");
                gVar.b = jSONObject.optInt("relation");
                arrayList.add(gVar);
            }
        }
        this.e.a(arrayList);
        g.d().b().a("contact_uploadtime", new Date());
        return arrayList;
    }

    public final List a(String[] strArr) {
        HashMap hashMap = new HashMap();
        hashMap.put("remoteids", android.support.v4.b.a.a(strArr, ","));
        hashMap.put("fields", android.support.v4.b.a.a(h(), ","));
        this.e.a(hashMap);
        JSONObject jSONObject = new JSONObject(a(String.valueOf(b) + "/profiles/" + g.q().h, hashMap)).getJSONObject("users");
        ArrayList arrayList = new ArrayList(jSONObject.length());
        for (String str : strArr) {
            if (jSONObject.has(str)) {
                bf bfVar = new bf();
                a(bfVar, jSONObject.getJSONObject(str));
                arrayList.add(bfVar);
            }
        }
        return arrayList;
    }

    public final void a(int i2, String str, String str2, String str3, String str4) {
        HashMap hashMap = new HashMap();
        if (android.support.v4.b.a.f(str3)) {
            hashMap.put(k, android.support.v4.b.a.n(str3));
            hashMap.put("etype", "2");
        }
        if (android.support.v4.b.a.f(str4)) {
            hashMap.put("access_token", str4);
        }
        String sb = new StringBuilder().append((int) (Math.random() * 100000.0d)).toString();
        hashMap.put("phonenumber", android.support.v4.b.a.b(Codec.a(str2.getBytes(), sb)));
        hashMap.put("countrycode", str);
        hashMap.put("uid", g.I());
        hashMap.put("ek", sb);
        hashMap.put("et", "4");
        hashMap.put("type", new StringBuilder(String.valueOf(i2)).toString());
        hashMap.put(Codec.Dse(), g.X());
        hashMap.put("temp_uid", g.I());
        hashMap.put(Codec.etr968ww(), new StringBuilder(String.valueOf(Codec.isEmulator() ? 1 : 0)).toString());
        a(String.valueOf(b) + "/v2/verify/getphonecode", hashMap);
    }

    public final void a(bf bfVar, String str) {
        boolean z = true;
        HashMap hashMap = new HashMap();
        hashMap.put("cookie", "SESSIONID=" + str);
        JSONObject jSONObject = new JSONObject(a(String.valueOf(g) + "/profile", (Map) null, (l[]) null, hashMap));
        a(jSONObject.optJSONObject("profile"), bfVar);
        JSONObject optJSONObject = jSONObject.optJSONObject("news");
        if (optJSONObject != null) {
            bfVar.v = optJSONObject.optInt("newfollowercount");
            bfVar.w = optJSONObject.optInt("followercount");
            bfVar.x = optJSONObject.optInt("followingcount");
            bfVar.y = optJSONObject.optInt("friendscount");
            bfVar.z = optJSONObject.optInt("groupcount");
            bfVar.A = optJSONObject.optInt("discusscount", bfVar.A);
        }
        JSONObject optJSONObject2 = jSONObject.optJSONObject("setting");
        if (optJSONObject2 != null) {
            if (bfVar.aE == null) {
                bfVar.aE = at.a(g.c(), bfVar.h);
            }
            if (optJSONObject2.has("hiddenmode")) {
                bfVar.aE.l = optJSONObject2.optInt("hiddenmode", bfVar.aE.l);
            }
            if (optJSONObject2.has("contactsenable")) {
                bfVar.aE.c = optJSONObject2.optInt("contactsenable", bfVar.aE.c ? 1 : 0) == 1;
            }
            if (optJSONObject2.has("wbcontactsenable")) {
                bfVar.aE.d = optJSONObject2.optInt("wbcontactsenable", bfVar.aE.d ? 1 : 0) == 1;
            }
            bfVar.aE.a();
        }
        JSONObject optJSONObject3 = jSONObject.optJSONObject("vip");
        if (optJSONObject3 != null) {
            bfVar.ai = optJSONObject3.optInt("level", 0);
            bfVar.aj = a(optJSONObject3.optLong("expire"));
            bfVar.ad = optJSONObject3.optInt("remind", 0) == 1;
            if (optJSONObject3.optInt("valid", 0) != 1) {
                z = false;
            }
            bfVar.a(z);
            bfVar.al = optJSONObject3.optInt("year", 0);
        }
        JSONObject optJSONObject4 = jSONObject.optJSONObject("money");
        if (optJSONObject4 != null) {
            bfVar.b(optJSONObject4.optLong("balance"));
        }
        JSONObject optJSONObject5 = jSONObject.optJSONObject("emotion");
        if (optJSONObject5 != null) {
            bfVar.ak = optJSONObject5.optLong("updatetime", bfVar.ak);
        }
    }

    public final void a(bf bfVar, String str, int i2, String str2) {
        boolean z = true;
        HashMap hashMap = new HashMap();
        hashMap.put("time", new StringBuilder(String.valueOf(i2)).toString());
        hashMap.put("timestamp", new StringBuilder(String.valueOf(i2)).toString());
        hashMap.put("code", str2);
        HashMap hashMap2 = new HashMap();
        hashMap2.put("cookie", "SESSIONID=" + str);
        JSONObject jSONObject = new JSONObject(a(String.valueOf(b) + "/logininit/android", hashMap, (l[]) null, hashMap2)).getJSONObject("data");
        a(jSONObject.optJSONObject("profile"), bfVar);
        JSONObject optJSONObject = jSONObject.optJSONObject("news");
        if (optJSONObject != null) {
            bfVar.v = optJSONObject.optInt("newfollowercount");
            bfVar.w = optJSONObject.optInt("followercount");
            bfVar.x = optJSONObject.optInt("followingcount");
            bfVar.y = optJSONObject.optInt("friendscount");
            bfVar.z = optJSONObject.optInt("groupcount");
            bfVar.A = optJSONObject.optInt("discusscount", bfVar.A);
        }
        JSONObject optJSONObject2 = jSONObject.optJSONObject("setting");
        if (optJSONObject2 != null) {
            if (bfVar.aE == null) {
                bfVar.aE = at.a(g.c(), bfVar.h);
            }
            if (optJSONObject2.has("hiddenmode")) {
                bfVar.aE.l = optJSONObject2.optInt("hiddenmode", bfVar.aE.l);
            }
            if (optJSONObject2.has("contactsenable")) {
                bfVar.aE.c = optJSONObject2.optInt("contactsenable", bfVar.aE.c ? 1 : 0) == 1;
            }
            if (optJSONObject2.has("wbcontactsenable")) {
                bfVar.aE.d = optJSONObject2.optInt("wbcontactsenable", bfVar.aE.d ? 1 : 0) == 1;
            }
            bfVar.aE.a();
        }
        JSONObject optJSONObject3 = jSONObject.optJSONObject("vip");
        if (optJSONObject3 != null) {
            bfVar.ai = optJSONObject3.optInt("level", 0);
            bfVar.aj = a(optJSONObject3.optLong("expire"));
            bfVar.ad = optJSONObject3.optInt("remind", 0) == 1;
            if (optJSONObject3.optInt("valid", 0) != 1) {
                z = false;
            }
            bfVar.a(z);
            bfVar.al = optJSONObject3.optInt("year", 0);
        }
        JSONObject optJSONObject4 = jSONObject.optJSONObject("money");
        if (optJSONObject4 != null) {
            bfVar.b(optJSONObject4.optLong("balance"));
        }
        JSONObject optJSONObject5 = jSONObject.optJSONObject("emotion");
        if (optJSONObject5 != null) {
            bfVar.ak = optJSONObject5.optLong("updatetime", bfVar.ak);
        }
    }

    public final void a(bf bfVar, String str, Map map, File file, String str2, String str3, String str4) {
        HashMap hashMap = new HashMap();
        hashMap.put(j, bfVar.G);
        hashMap.put(k, android.support.v4.b.a.n(bfVar.f3011a));
        hashMap.put("name", bfVar.i);
        hashMap.put(a.l, bfVar.H);
        hashMap.put("birthday", bfVar.J);
        hashMap.put(Codec.b(), str);
        hashMap.put("timestamp", str3);
        hashMap.put("code", str4);
        hashMap.put("etype", "2");
        hashMap.put("temp_uid", g.I());
        hashMap.putAll(map);
        if (android.support.v4.b.a.f(bfVar.s)) {
            hashMap.put("inviterid", bfVar.s);
        }
        if (android.support.v4.b.a.f(str2)) {
            hashMap.put("alipay_user_id", str2);
        }
        JSONObject jSONObject = new JSONObject(a(String.valueOf(b) + a.g, hashMap, new l[]{new l(file.getName(), file, "avatarimg", (byte) 0)}));
        bfVar.h = jSONObject.getString("momoid");
        bfVar.G = jSONObject.getString(j);
        bfVar.W = jSONObject.getString(m);
        if (jSONObject.has("avatar")) {
            bfVar.ae = new String[]{jSONObject.getString("avatar")};
        }
    }

    public final void a(bf bfVar, List list) {
        HashMap hashMap = new HashMap();
        hashMap.put("pversion", new StringBuilder(String.valueOf(bfVar.X)).toString());
        hashMap.put("signcount", new StringBuilder().append(bfVar.l() == null ? 0 : bfVar.l().length()).toString());
        this.e.a(hashMap);
        if (bfVar == null || android.support.v4.b.a.a((CharSequence) bfVar.h)) {
            throw new IllegalArgumentException("user == null || StringUtils.isEmpty(user.momoid)");
        }
        JSONObject jSONObject = new JSONObject(a(String.valueOf(h) + "/" + bfVar.h, hashMap));
        int optInt = jSONObject.optInt("pversion");
        if (((long) optInt) != bfVar.X) {
            a(jSONObject, bfVar);
        } else {
            if (jSONObject.has("vip")) {
                JSONObject optJSONObject = jSONObject.optJSONObject("vip");
                bfVar.ai = optJSONObject.optInt("level", bfVar.ai);
                bfVar.al = optJSONObject.optInt("year", bfVar.al);
                bfVar.a(optJSONObject.optInt("valid", bfVar.b() ? 1 : 0) == 1);
            }
            bfVar.d = jSONObject.optString("phone", bfVar.d);
            bfVar.af = jSONObject.optString("background", bfVar.af);
            bfVar.a((float) jSONObject.optInt("distance"));
            bfVar.a(a(jSONObject.optLong("loc_timesec")));
            bfVar.V = jSONObject.optInt("deviation", 0) == 1;
            bfVar.P = jSONObject.optString("relation");
            bfVar.p = jSONObject.optString("wb_name", bfVar.p);
            bfVar.q = jSONObject.optString("qqwb_name", bfVar.q);
            bfVar.r = jSONObject.optString("rr_name", bfVar.r);
            bfVar.u = jSONObject.optInt("feed_count", bfVar.u);
            if (jSONObject.has("tencent_user_id")) {
                bfVar.as = jSONObject.optString("tencent_user_id", bfVar.as);
                if (!android.support.v4.b.a.a((CharSequence) bfVar.as)) {
                    bfVar.at = true;
                } else {
                    bfVar.at = false;
                }
            }
            if (jSONObject.has("tencent_vip_desc")) {
                if (!android.support.v4.b.a.a((CharSequence) jSONObject.optString("tencent_user_id", PoiTypeDef.All))) {
                    bfVar.au = true;
                } else {
                    bfVar.au = false;
                }
            }
            bfVar.ac = jSONObject.has("baned");
            if (jSONObject.has("sign")) {
                bfVar.b(jSONObject.optString("sign"));
            }
            bfVar.ag = jSONObject.optString("feedid", PoiTypeDef.All);
            if (jSONObject.has("feed")) {
                ab abVar = new ab();
                k.a();
                k.a(jSONObject.getJSONObject("feed"), abVar);
                bfVar.ah = abVar;
            } else {
                bfVar.ah = null;
            }
        }
        if (jSONObject.has("groups")) {
            y yVar = new y();
            JSONArray jSONArray = jSONObject.getJSONArray("groups");
            if (g.q() == null || !bfVar.h.equals(g.q().h)) {
                yVar.c(bfVar.h);
            }
            for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                com.immomo.momo.service.bean.a.a aVar = new com.immomo.momo.service.bean.a.a();
                n.a(aVar, jSONArray.getJSONObject(i2));
                yVar.a(aVar);
                yVar.a(bfVar.h, aVar.b, aVar.n);
                if (list != null) {
                    list.add(aVar);
                }
            }
        }
        if (jSONObject.has("join")) {
            bfVar.d(jSONObject.getString("join"));
        }
        bfVar.X = (long) optInt;
    }

    public final void a(bf bfVar, Map map, File file, String str, String str2) {
        HashMap hashMap = new HashMap();
        hashMap.put(k, android.support.v4.b.a.n(bfVar.f3011a));
        hashMap.put("name", bfVar.i);
        hashMap.put(a.l, bfVar.H);
        hashMap.put("birthday", bfVar.J);
        hashMap.put("phonenumber", bfVar.b);
        hashMap.put("countrycode", bfVar.c);
        hashMap.put("temp_uid", g.I());
        hashMap.putAll(map);
        if (android.support.v4.b.a.f(str)) {
            hashMap.put("access_token", str);
        }
        if (android.support.v4.b.a.f(bfVar.s)) {
            hashMap.put("inviterid", bfVar.s);
        }
        if (android.support.v4.b.a.f(str2)) {
            hashMap.put("alipay_user_id", str2);
        }
        if (android.support.v4.b.a.f(bfVar.e)) {
            hashMap.put("verifycode", bfVar.e);
        }
        this.e.a(hashMap);
        JSONObject jSONObject = new JSONObject(a(String.valueOf(b) + a.g, hashMap, new l[]{new l(file.getName(), file, "avatarimg", (byte) 0)}));
        bfVar.h = jSONObject.getString(a.m);
        bfVar.G = jSONObject.optString(a.n);
        bfVar.W = jSONObject.getString(a.o);
        if (jSONObject.has("avatar")) {
            bfVar.ae = new String[]{jSONObject.getString("avatar")};
        }
    }

    public final void a(bf bfVar, Map map, Map map2) {
        l[] lVarArr = new l[map2.size()];
        int i2 = 0;
        for (Map.Entry entry : map2.entrySet()) {
            lVarArr[i2] = new l("avator.jpg", (File) entry.getValue(), (String) entry.getKey(), (byte) 0);
            i2++;
        }
        this.e.a(map);
        String a2 = a(String.valueOf(g) + "/edit", map, lVarArr);
        ak.c(bfVar.h);
        a(new JSONObject(a2).getJSONObject("fields"), bfVar);
    }

    public final void a(bf bfVar, Map map, Map map2, File file) {
        int size = map2.size();
        int i2 = file != null ? size + 1 : size;
        l[] lVarArr = new l[i2];
        int i3 = 0;
        for (Map.Entry entry : map2.entrySet()) {
            lVarArr[i3] = new l("avator.jpg", (File) entry.getValue(), (String) entry.getKey(), (byte) 0);
            i3++;
        }
        if (file != null) {
            lVarArr[i2 - 1] = new l(file.getName(), file, "background", (byte) 0);
        }
        String a2 = a(String.valueOf(g) + "/edit", map, lVarArr);
        ak.c(bfVar.h);
        a(new JSONObject(a2).getJSONObject("fields"), bfVar);
    }

    public final void a(String str) {
        HashMap hashMap = new HashMap();
        hashMap.put(j, str);
        a(String.valueOf(i) + "/resetpassword", hashMap);
    }

    public final void a(String str, String str2) {
        HashMap hashMap = new HashMap();
        hashMap.put("oldpwd", str);
        hashMap.put("newpwd", str2);
        hashMap.put("etype", "0");
        a(String.valueOf(b) + a.i, hashMap);
    }

    public final void a(String str, String str2, String str3, int i2, String str4) {
        HashMap hashMap = new HashMap();
        hashMap.put(j, str);
        hashMap.put("access_token", str2);
        hashMap.put(k, str3);
        hashMap.put(Codec.Dse(), g.X());
        hashMap.put(Codec.etr968ww(), new StringBuilder(String.valueOf(Codec.isEmulator())).toString());
        hashMap.put("timestamp", new StringBuilder(String.valueOf(i2)).toString());
        hashMap.put("code", str4);
        a(String.valueOf(b) + a.g + "/checkemail", hashMap);
    }

    public final void a(String str, String str2, String str3, String str4) {
        HashMap hashMap = new HashMap();
        hashMap.put("verifycode", str);
        hashMap.put("countrycode", str3);
        hashMap.put(k, str4);
        hashMap.put("phonenumber", str2);
        hashMap.put("etype", "0");
        a(String.valueOf(i) + "/setpwdbyphone", hashMap);
    }

    public final void a(List list) {
        HashMap hashMap = new HashMap();
        String[] strArr = new String[list.size()];
        for (int i2 = 0; i2 < list.size(); i2++) {
            strArr[i2] = ((bf) list.get(i2)).h;
            hashMap.put(strArr[i2], (bf) list.get(i2));
        }
        HashMap hashMap2 = new HashMap();
        hashMap2.put("remoteids", android.support.v4.b.a.a(strArr, ","));
        hashMap2.put("fields", android.support.v4.b.a.a(h(), ","));
        this.e.a(hashMap2);
        JSONObject jSONObject = new JSONObject(a(String.valueOf(b) + "/profiles/" + g.q().h, hashMap2)).getJSONObject("users");
        for (String str : strArr) {
            if (jSONObject.has(str)) {
                a((bf) hashMap.get(str), jSONObject.getJSONObject(str));
            }
        }
    }

    public final void a(List list, Location location, StringBuilder sb, String str, int i2) {
        HashMap hashMap = new HashMap();
        hashMap.put("lat", new StringBuilder(String.valueOf(location.getLatitude())).toString());
        hashMap.put("lng", new StringBuilder(String.valueOf(location.getLongitude())).toString());
        hashMap.put("loctype", "1");
        hashMap.put("sex", str);
        hashMap.put("activetime", new StringBuilder(String.valueOf(i2)).toString());
        JSONObject jSONObject = new JSONObject(a(String.valueOf(b) + "/roam/vip", hashMap)).getJSONObject("data");
        JSONArray jSONArray = jSONObject.getJSONArray("list");
        for (int i3 = 0; i3 < jSONArray.length(); i3++) {
            bf bfVar = new bf();
            a(bfVar, jSONArray.getJSONObject(i3));
            list.add(bfVar);
        }
        sb.append(jSONObject.optString("address"));
    }

    public final boolean a(bf bfVar) {
        boolean z;
        boolean z2;
        boolean z3;
        int i2 = 0;
        HashMap hashMap = new HashMap();
        hashMap.put("version", new StringBuilder(String.valueOf(bfVar.X)).toString());
        hashMap.put("signcount", new StringBuilder().append(bfVar.l() == null ? 0 : bfVar.l().length()).toString());
        JSONObject jSONObject = new JSONObject(a(String.valueOf(g) + "/checkversion", hashMap));
        if (jSONObject.has("profile")) {
            a(jSONObject.getJSONObject("profile"), bfVar);
            z = true;
        } else {
            z = false;
        }
        JSONObject optJSONObject = jSONObject.optJSONObject("news");
        if (optJSONObject != null) {
            bfVar.v = optJSONObject.optInt("newfollowercount");
            bfVar.w = optJSONObject.optInt("followercount");
            bfVar.x = optJSONObject.optInt("followingcount");
            bfVar.y = optJSONObject.optInt("friendscount");
            bfVar.z = optJSONObject.optInt("groupcount");
            bfVar.A = optJSONObject.optInt("discusscount", bfVar.A);
            z = true;
        }
        int optInt = jSONObject.optInt("weibo_remain_day", 90);
        if (optInt != bfVar.aG) {
            z = true;
        }
        bfVar.aG = optInt;
        JSONObject optJSONObject2 = jSONObject.optJSONObject("setting");
        if (optJSONObject2 != null) {
            if (bfVar.aE == null && g.q() != null) {
                bfVar.aE = at.a(g.c(), g.q().h);
            }
            if (optJSONObject2.has("hiddenmode")) {
                bfVar.aE.l = optJSONObject2.optInt("hiddenmode", bfVar.aE.l);
            }
            if (optJSONObject2.has("contactsenable")) {
                bfVar.aE.c = optJSONObject2.optInt("contactsenable", bfVar.aE.c ? 1 : 0) == 1;
            }
            if (optJSONObject2.has("wbcontactsenable")) {
                bfVar.aE.d = optJSONObject2.optInt("wbcontactsenable", bfVar.aE.d ? 1 : 0) == 1;
            }
            bfVar.aE.a();
        }
        JSONObject optJSONObject3 = jSONObject.optJSONObject("vip");
        if (optJSONObject3 != null) {
            bfVar.ai = optJSONObject3.optInt("level", 0);
            bfVar.aj = android.support.v4.b.a.a(optJSONObject3.optLong("expire"));
            bfVar.ad = optJSONObject3.optInt("remind", 0) == 1;
            bfVar.a(optJSONObject3.optInt("valid", 0) == 1);
            bfVar.al = optJSONObject3.optInt("year", 0);
            z = true;
        }
        JSONObject optJSONObject4 = jSONObject.optJSONObject("money");
        if (optJSONObject4 != null) {
            bfVar.b(optJSONObject4.optLong("balance"));
            z = true;
        }
        JSONObject optJSONObject5 = jSONObject.optJSONObject("emotion");
        if (optJSONObject5 == null) {
            return z;
        }
        long optLong = optJSONObject5.optLong("updatetime", 0);
        if (optLong != 0) {
            bfVar.ak = optLong;
            z2 = true;
        } else {
            z2 = z;
        }
        JSONArray optJSONArray = optJSONObject5.optJSONArray("used");
        o oVar = new o();
        List e = oVar.e();
        ArrayList arrayList = new ArrayList();
        if (optJSONArray != null && optJSONArray.length() > 0) {
            List d = oVar.d();
            boolean z4 = false;
            while (i2 < optJSONArray.length()) {
                q qVar = new q(optJSONArray.getString(i2));
                int indexOf = e.indexOf(qVar);
                if (indexOf >= 0) {
                    arrayList.add((q) e.get(indexOf));
                    if (indexOf != i2) {
                        z4 = true;
                    }
                } else {
                    q b = oVar.b(qVar.f3035a);
                    if (b != null) {
                        b.u = true;
                        b.q = true;
                        arrayList.add(b);
                        z4 = true;
                    } else {
                        qVar.u = true;
                        qVar.q = true;
                        arrayList.add(qVar);
                        z4 = true;
                    }
                }
                if (d.contains(qVar)) {
                    d.remove(qVar);
                    z3 = true;
                } else {
                    z3 = z4;
                }
                i2++;
                z4 = z3;
            }
            if (!z4 && arrayList.size() == e.size()) {
                return z2;
            }
            oVar.i(arrayList);
            oVar.h(d);
            return z2;
        } else if (e.size() == 0) {
            return z2;
        } else {
            oVar.i(arrayList);
            return z2;
        }
    }

    public final boolean a(List list, AtomicInteger atomicInteger, int i2) {
        boolean z = true;
        HashMap hashMap = new HashMap();
        hashMap.put("index", new StringBuilder(String.valueOf(i2)).toString());
        hashMap.put("count", new StringBuilder(String.valueOf(30)).toString());
        JSONObject jSONObject = new JSONObject(a(String.valueOf(b) + "/visitors", hashMap)).getJSONObject("data");
        if (jSONObject.optInt("remain") != 1) {
            z = false;
        }
        JSONArray jSONArray = jSONObject.getJSONArray("list");
        atomicInteger.set(jSONObject.optInt("total"));
        for (int i3 = 0; i3 < jSONArray.length(); i3++) {
            JSONObject jSONObject2 = jSONArray.getJSONObject(i3);
            bf bfVar = new bf();
            a(bfVar, jSONObject2);
            bfVar.aF = a(jSONObject2.optLong("visittime"));
            list.add(bfVar);
        }
        return z;
    }

    public final boolean a(boolean z) {
        String str = String.valueOf(i) + "/contactsctl";
        HashMap hashMap = new HashMap();
        hashMap.put("enable", z ? "1" : "0");
        hashMap.put(Codec.du(), g.I());
        return new JSONObject(a(str, hashMap)).optInt("enable") == 1;
    }

    public final String[] a(String str, String str2, boolean z) {
        HashMap hashMap = new HashMap();
        hashMap.put("phonenumber", str2);
        hashMap.put("countrycode", str);
        hashMap.put("deny", z ? "1" : "0");
        hashMap.put("temp_uid", g.I());
        hashMap.put(Codec.Dse(), g.X());
        hashMap.put(Codec.etr968ww(), new StringBuilder(String.valueOf(Codec.isEmulator() ? 1 : 0)).toString());
        JSONObject optJSONObject = new JSONObject(a(String.valueOf(b) + "/v2/verify/regspinfo", hashMap)).optJSONObject("data");
        return new String[]{optJSONObject.getString("sp_num"), optJSONObject.getString("sp_msg")};
    }

    public final String b(String str, int i2, String str2) {
        String str3 = String.valueOf(b) + "/tencentweibo_invite/" + g.q().h;
        HashMap hashMap = new HashMap();
        hashMap.put("user_id", str);
        hashMap.put("source", new StringBuilder(String.valueOf(i2)).toString());
        hashMap.put("gid", str2);
        return new JSONObject(a(str3, hashMap)).optString("msg");
    }

    public final void b() {
        a(String.valueOf(b) + "/visitors/clear", (Map) null);
    }

    public final void b(bf bfVar, String str) {
        HashMap hashMap = new HashMap();
        hashMap.put("remoteids", str);
        hashMap.put("fields", android.support.v4.b.a.a(h(), ","));
        this.e.a(hashMap);
        a(bfVar, new JSONObject(a(String.valueOf(b) + "/profiles/" + g.q().h, hashMap)).getJSONObject("users").getJSONObject(str));
    }

    public final void b(String str) {
        a(String.valueOf(b) + "/unfollow/" + str, (Map) null);
    }

    public final void b(String str, String str2) {
        HashMap hashMap = new HashMap();
        hashMap.put("content", str2);
        hashMap.put("block", "1");
        a(String.valueOf(b) + "/report/" + str, hashMap);
    }

    public final void b(String str, String str2, String str3) {
        HashMap hashMap = new HashMap();
        hashMap.put("phonenumber", str2);
        hashMap.put("verifycode", str3);
        hashMap.put("countrycode", str);
        hashMap.put(Codec.Dse(), g.X());
        hashMap.put(Codec.etr968ww(), new StringBuilder(String.valueOf(Codec.isEmulator() ? 1 : 0)).toString());
        hashMap.put("temp_uid", g.I());
        if (android.support.v4.b.a.f((String) null)) {
            hashMap.put(k, null);
        }
        a(String.valueOf(b) + "/v2/verify/checkphonecode", hashMap);
    }

    public final void b(String str, String str2, String str3, int i2, String str4) {
        HashMap hashMap = new HashMap();
        if (android.support.v4.b.a.f(str3)) {
            hashMap.put("access_token", str3);
        }
        String sb = new StringBuilder().append((int) (Math.random() * 100000.0d)).toString();
        hashMap.put("phonenumber", android.support.v4.b.a.b(Codec.a(str2.getBytes(), sb)));
        hashMap.put("countrycode", str);
        hashMap.put("uid", g.I());
        hashMap.put("ek", sb);
        hashMap.put("et", "4");
        hashMap.put("type", "1");
        hashMap.put("temp_uid", g.I());
        hashMap.put(Codec.etr968ww(), new StringBuilder(String.valueOf(Codec.isEmulator() ? 1 : 0)).toString());
        hashMap.put("timestamp", new StringBuilder(String.valueOf(i2)).toString());
        hashMap.put("code", str4);
        a(String.valueOf(b) + "/v2/verify/getphonecode", hashMap);
    }

    public final void b(String str, String str2, String str3, String str4) {
        HashMap hashMap = new HashMap();
        hashMap.put("countrycode", str);
        hashMap.put("phonenumber", str2);
        hashMap.put("verifycode", str3);
        hashMap.put("etype", "2");
        hashMap.put(k, android.support.v4.b.a.n(str4));
        a(String.valueOf(i) + "/bindphone", hashMap);
    }

    public final void b(List list, int i2) {
        HashMap hashMap = new HashMap();
        hashMap.put("type", String.valueOf(i2));
        JSONArray jSONArray = new JSONObject(a(String.valueOf(b) + "/contacts/weibo/contacts", hashMap)).getJSONArray("contacts");
        for (int i3 = 0; i3 < jSONArray.length(); i3++) {
            e eVar = new e();
            list.add(eVar);
            JSONObject jSONObject = jSONArray.getJSONObject(i3);
            eVar.f3023a = jSONObject.optString("momoid", eVar.f3023a);
            eVar.d = jSONObject.optString("weibo_name", eVar.d);
            eVar.c = jSONObject.optString("weibo_uid", eVar.c);
            eVar.g = jSONObject.optString("weibo_photo_url", eVar.g);
            eVar.b = jSONObject.optInt("relation", eVar.b);
        }
    }

    public final String c() {
        return new JSONObject(a(String.valueOf(b) + "/roam/notice", (Map) null)).optJSONObject("data").optString("notice");
    }

    public final String c(String str) {
        return new JSONObject(a(String.valueOf(b) + "/follow/" + str, (Map) null)).optString("msg", PoiTypeDef.All);
    }

    public final String c(String str, int i2, String str2) {
        String str3 = String.valueOf(b) + "/weibo_invite/" + g.q().h;
        HashMap hashMap = new HashMap();
        hashMap.put("user_id", str);
        hashMap.put("source", new StringBuilder(String.valueOf(i2)).toString());
        android.support.v4.b.a.a((CharSequence) str2);
        hashMap.put("gid", str2);
        return new JSONObject(a(str3, hashMap)).optString("msg");
    }

    public final String c(String str, String str2) {
        HashMap hashMap = new HashMap();
        hashMap.put(a.p, str2);
        return new JSONObject(a(String.valueOf(b) + "/v2" + Codec.rscccc() + "/checkcode?verifycode=" + str, (Map) null, hashMap)).optJSONObject("data").optString("access_token");
    }

    public final void c(bf bfVar, String str) {
        ArrayList arrayList = new ArrayList();
        arrayList.add("momoid");
        arrayList.add("name");
        arrayList.add("remarkname");
        arrayList.add("photos");
        HashMap hashMap = new HashMap();
        hashMap.put("remoteids", str);
        hashMap.put("fields", android.support.v4.b.a.a(arrayList, ","));
        a(bfVar, new JSONObject(a(String.valueOf(b) + "/profiles/" + g.q().h, hashMap)).getJSONObject("users").getJSONObject(str));
    }

    public final void c(List list, int i2) {
        HashMap hashMap = new HashMap();
        hashMap.put("type", String.valueOf(i2));
        JSONArray jSONArray = new JSONObject(a(String.valueOf(b) + "/contacts/tencent/contacts", hashMap)).getJSONArray("contacts");
        for (int i3 = 0; i3 < jSONArray.length(); i3++) {
            e eVar = new e();
            list.add(eVar);
            JSONObject jSONObject = jSONArray.getJSONObject(i3);
            eVar.f3023a = jSONObject.optString("momoid", eVar.f3023a);
            eVar.d = jSONObject.optString("tencent_name", eVar.d);
            eVar.c = jSONObject.optString("tencent_uid", eVar.c);
            eVar.g = jSONObject.optString("tencent_photo_url", eVar.g);
            eVar.b = jSONObject.optInt("relation", eVar.b);
        }
    }

    public final String[] c(String str, String str2, String str3) {
        HashMap hashMap = new HashMap();
        hashMap.put("phonenumber", str2);
        hashMap.put("countrycode", str);
        hashMap.put(Codec.Dse(), g.X());
        hashMap.put("temp_uid", g.I());
        hashMap.put(Codec.etr968ww(), new StringBuilder(String.valueOf(Codec.isEmulator() ? 1 : 0)).toString());
        if (android.support.v4.b.a.f(str3)) {
            hashMap.put("access_token", str3);
        }
        JSONObject optJSONObject = new JSONObject(a(String.valueOf(b) + "/v2/verify/getspinfo", hashMap)).optJSONObject("data");
        return new String[]{optJSONObject.getString("sp_num"), optJSONObject.getString("sp_msg")};
    }

    public final List d() {
        HashMap hashMap = new HashMap();
        hashMap.put("index", new StringBuilder(String.valueOf(0)).toString());
        hashMap.put("count", new StringBuilder(String.valueOf((int) PurchaseCode.LOADCHANNEL_ERR)).toString());
        return b(new JSONObject(a(String.valueOf(b) + "/hidden/lists", hashMap)).getJSONObject("data").getJSONArray("list"));
    }

    public final void d(String str) {
        a(String.valueOf(b) + "/unblock/" + str, (Map) null);
    }

    public final void d(String str, String str2) {
        HashMap hashMap = new HashMap();
        hashMap.put("name", str2);
        a(String.valueOf(h) + "/remark/" + str, hashMap);
    }

    public final void d(List list, int i2) {
        HashMap hashMap = new HashMap();
        hashMap.put("type", String.valueOf(i2));
        JSONArray jSONArray = new JSONObject(a(String.valueOf(b) + "/contacts/renren/contacts", hashMap)).getJSONArray("contacts");
        for (int i3 = 0; i3 < jSONArray.length(); i3++) {
            e eVar = new e();
            list.add(eVar);
            JSONObject jSONObject = jSONArray.getJSONObject(i3);
            eVar.f3023a = jSONObject.optString("momoid", eVar.f3023a);
            eVar.d = jSONObject.optString("renren_name", eVar.d);
            eVar.c = jSONObject.optString("renren_uid", eVar.c);
            eVar.g = jSONObject.optString("renren_photo_url", eVar.g);
            eVar.b = jSONObject.optInt("relation", eVar.b);
        }
    }

    public final com.immomo.momo.protocol.a.a.a e(String str, String str2) {
        boolean z = true;
        HashMap hashMap = new HashMap();
        hashMap.put("phone", str2);
        hashMap.put("reason", str);
        JSONObject jSONObject = new JSONObject(a(String.valueOf(b) + "/contacts/phone/apply/" + g.q().h, hashMap));
        com.immomo.momo.protocol.a.a.a aVar = new com.immomo.momo.protocol.a.a.a();
        if (jSONObject.optInt("send") != 1) {
            z = false;
        }
        aVar.b = z;
        aVar.f2880a = jSONObject.optString("msg");
        return aVar;
    }

    public final String e() {
        return new JSONObject(a(String.valueOf(b) + "/hidden/config", (Map) null)).optJSONObject("data").optString("desc");
    }

    public final void e(String str) {
        a(String.valueOf(b) + "/block/" + str, (Map) null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.protocol.a.w.a(org.json.JSONObject, com.immomo.momo.service.bean.ad, boolean):void
     arg types: [org.json.JSONObject, com.immomo.momo.service.bean.ad, int]
     candidates:
      com.immomo.momo.protocol.a.w.a(java.util.List, double, double):int
      com.immomo.momo.protocol.a.w.a(java.util.List, int, com.immomo.momo.service.bean.at):int
      com.immomo.momo.protocol.a.w.a(java.lang.String, java.lang.String, java.lang.String):com.immomo.momo.service.bean.bf
      com.immomo.momo.protocol.a.w.a(double, double, int):com.immomo.momo.service.bean.bj
      com.immomo.momo.protocol.a.w.a(java.io.File, java.lang.String, java.lang.String):java.lang.String
      com.immomo.momo.protocol.a.w.a(java.lang.String, int, java.io.File):java.lang.String
      com.immomo.momo.protocol.a.w.a(java.lang.String, int, java.lang.String):java.lang.String
      com.immomo.momo.protocol.a.w.a(com.immomo.momo.service.bean.bf, java.util.Map, java.util.Map):void
      com.immomo.momo.protocol.a.w.a(java.util.List, java.util.concurrent.atomic.AtomicInteger, int):boolean
      com.immomo.momo.protocol.a.w.a(java.lang.String, java.lang.String, boolean):java.lang.String[]
      com.immomo.momo.protocol.a.p.a(java.lang.String, int, com.immomo.momo.android.activity.fs):com.immomo.momo.util.v
      com.immomo.momo.protocol.a.p.a(java.lang.String, java.io.File, com.immomo.momo.android.c.w):void
      com.immomo.momo.protocol.a.p.a(java.lang.String, java.util.Map, java.util.Map):java.lang.String
      com.immomo.momo.protocol.a.p.a(java.lang.String, java.util.Map, com.immomo.momo.protocol.a.l[]):java.lang.String
      com.immomo.momo.protocol.a.w.a(org.json.JSONObject, com.immomo.momo.service.bean.ad, boolean):void */
    public final boolean e(List list, int i2) {
        HashMap hashMap = new HashMap();
        hashMap.put("index", new StringBuilder(String.valueOf(i2)).toString());
        hashMap.put("count", new StringBuilder(String.valueOf(20)).toString());
        JSONObject optJSONObject = new JSONObject(a(String.valueOf(b) + "/feedback/lists", hashMap)).optJSONObject("data");
        if (optJSONObject == null) {
            return false;
        }
        JSONArray optJSONArray = optJSONObject.optJSONArray("feedbacks");
        for (int i3 = 0; i3 < optJSONArray.length(); i3++) {
            ad adVar = new ad();
            a(optJSONArray.getJSONObject(i3), adVar, true);
            list.add(adVar);
        }
        return optJSONObject.optInt("remain") == 1;
    }

    public final p f() {
        int[] iArr;
        HashMap hashMap = new HashMap();
        hashMap.put("client", "android");
        hashMap.put("mark", "32");
        String a2 = a(String.valueOf(b) + "/appconfig", hashMap);
        p pVar = new p();
        JSONObject jSONObject = new JSONObject(a2);
        JSONArray optJSONArray = jSONObject.optJSONArray("regtype");
        if (optJSONArray != null) {
            int[] iArr2 = new int[optJSONArray.length()];
            for (int i2 = 0; i2 < optJSONArray.length(); i2++) {
                iArr2[i2] = optJSONArray.getInt(i2);
            }
            iArr = iArr2;
        } else {
            iArr = new int[0];
        }
        pVar.b = iArr;
        pVar.f2895a = jSONObject.getInt("timesec");
        return pVar;
    }

    public final String f(String str, String str2) {
        String str3 = String.valueOf(b) + "/contacts/weibo/apply/" + g.q().h;
        HashMap hashMap = new HashMap();
        hashMap.put("weibo_uid", str2);
        hashMap.put("reason", str);
        return new JSONObject(a(str3, hashMap)).optString("msg");
    }

    public final void f(String str) {
        HashMap hashMap = new HashMap();
        hashMap.put("remoteid", str);
        a(String.valueOf(b) + "/hidden/unhidden", hashMap);
    }

    public final String g(String str, String str2) {
        String str3 = String.valueOf(b) + "/contacts/tencent/apply/" + g.q().h;
        HashMap hashMap = new HashMap();
        hashMap.put("tencent_uid", str2);
        hashMap.put("reason", str);
        return new JSONObject(a(str3, hashMap)).optString("msg");
    }

    public final List g() {
        JSONArray jSONArray = new JSONObject(a(String.valueOf(b) + "/blacklist", (Map) null)).getJSONArray("list");
        ArrayList arrayList = new ArrayList();
        for (int i2 = 0; i2 < jSONArray.length(); i2++) {
            bf bfVar = new bf();
            arrayList.add(bfVar);
            JSONObject jSONObject = jSONArray.getJSONObject(i2);
            a(bfVar, jSONObject);
            try {
                bfVar.Y = android.support.v4.b.a.j(jSONObject.optString("blocktime"));
            } catch (Exception e) {
            }
        }
        return arrayList;
    }

    public final void g(String str) {
        HashMap hashMap = new HashMap();
        hashMap.put("remoteid", str);
        a(String.valueOf(b) + "/hidden/hidden", hashMap);
    }

    public final String h(String str, String str2) {
        String str3 = String.valueOf(b) + "/contacts/renren/apply/" + g.q().h;
        HashMap hashMap = new HashMap();
        hashMap.put("renren_uid", str2);
        hashMap.put("reason", str);
        return new JSONObject(a(str3, hashMap)).optString("msg");
    }

    public final void h(String str) {
        HashMap hashMap = new HashMap();
        hashMap.put(l, str);
        hashMap.put("temp_uid", g.I());
        hashMap.put(Codec.etr968ww(), new StringBuilder(String.valueOf(Codec.isEmulator() ? 1 : 0)).toString());
        hashMap.put(Codec.Dse(), g.X());
        a(String.valueOf(b) + "/v2/verify/autologincheck", hashMap);
    }

    public final String[] i(String str) {
        HashMap hashMap = new HashMap();
        hashMap.put(l, str);
        hashMap.put(Codec.Dse(), g.X());
        hashMap.put(Codec.etr968ww(), new StringBuilder(String.valueOf(Codec.isEmulator() ? 1 : 0)).toString());
        hashMap.put("temp_uid", g.I());
        JSONObject optJSONObject = new JSONObject(a(String.valueOf(b) + "/v2/verify/loginspinfo", hashMap)).optJSONObject("data");
        return new String[]{optJSONObject.getString("sp_num"), optJSONObject.getString("sp_msg")};
    }

    public final boolean j(String str) {
        HashMap hashMap = new HashMap();
        hashMap.put(Codec.coo(), str);
        return "YES".equals(new JSONObject(a(String.valueOf(i) + "/momologout", (Map) null, (l[]) null, hashMap)).optString("momologout"));
    }

    public final void m(String str) {
        HashMap hashMap = new HashMap();
        hashMap.put("step", str);
        hashMap.put("uid", g.I());
        a(String.valueOf(b) + "/log/register", hashMap);
    }

    public final void n(String str) {
        HashMap hashMap = new HashMap();
        hashMap.put(j, str);
        a(String.valueOf(i) + "/bindemail", hashMap);
    }
}
