package com.immomo.momo.protocol.imjson.a;

import android.os.Handler;
import com.immomo.a.a.d.b;
import com.immomo.a.a.f;
import com.immomo.a.a.f.a;
import com.immomo.momo.g;
import com.immomo.momo.service.aq;
import com.immomo.momo.service.bean.Message;
import com.immomo.momo.service.bean.at;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.util.m;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public final class l implements f {

    /* renamed from: a  reason: collision with root package name */
    List f2905a;
    /* access modifiers changed from: private */
    public Lock b;
    /* access modifiers changed from: private */
    public Handler c;
    /* access modifiers changed from: private */
    public boolean d;
    private bf e;
    /* access modifiers changed from: private */
    public at f;

    public l() {
        new m(this);
        this.b = new ReentrantLock();
        this.f2905a = new ArrayList(5);
        this.c = null;
        this.d = false;
        this.e = null;
        this.f = null;
        this.e = g.q();
        this.f = g.r();
        if (this.e == null || this.f == null) {
            throw new IllegalStateException("user || preference is null");
        }
        new Thread(new m(this)).start();
    }

    public final void a(Object obj, f fVar) {
    }

    /* JADX INFO: finally extract failed */
    public final boolean a(b bVar) {
        boolean z;
        boolean z2;
        Message message = new Message(true);
        message.bubbleStyle = bVar.optInt("bs", 0);
        message.msgId = bVar.d();
        message.remoteId = bVar.g();
        message.timestamp = new Date(bVar.optLong("t", System.currentTimeMillis()));
        message.setDiatance(Integer.valueOf(bVar.optInt("distance", -1)));
        message.setDistanceTime(Long.valueOf(bVar.optLong("dt", -1)));
        message.receive = !this.e.h.equals(message.remoteId);
        if (message.receive) {
            message.status = 5;
        } else {
            message.status = 11;
        }
        if ("gmsg".equals(bVar.a())) {
            message.chatType = 2;
            message.groupId = bVar.f();
            z = true;
        } else {
            z = false;
        }
        if (!z) {
            if ("dmsg".equals(bVar.a())) {
                message.chatType = 3;
                message.discussId = bVar.f();
                z2 = true;
            } else {
                z2 = false;
            }
            if (!z2) {
                message.chatType = 1;
                if (bVar.has("noreply")) {
                    message.username = bVar.optString("username");
                    message.isSayhi = !new aq().k(message.remoteId);
                }
                if (!message.receive) {
                    String f2 = bVar.f();
                    if (!a.a(f2)) {
                        message.remoteId = f2;
                    }
                }
            }
        }
        if (!android.support.v4.b.a.a(bVar, message)) {
            return false;
        }
        if (!this.d) {
            Thread.sleep(2000);
        }
        this.b.lock();
        try {
            if (this.f2905a.isEmpty()) {
                this.c.sendEmptyMessageDelayed(123, 100);
            }
            this.f2905a.add(message);
            this.b.unlock();
            return true;
        } catch (Throwable th) {
            this.b.unlock();
            throw th;
        }
    }
}
