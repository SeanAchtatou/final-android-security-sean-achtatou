package com.immomo.momo.protocol.imjson.a;

import android.content.Intent;
import com.immomo.a.a.d.b;
import com.immomo.a.a.f;
import com.immomo.momo.android.broadcast.w;
import com.immomo.momo.g;
import com.immomo.momo.service.aq;
import com.immomo.momo.service.bean.bf;
import java.util.Date;

public final class o implements f {
    public final void a(Object obj, f fVar) {
    }

    public final boolean a(b bVar) {
        if (!"revive-vip".equals(bVar.getString("event"))) {
            return false;
        }
        long optLong = bVar.optLong("expire") * 1000;
        int optInt = bVar.optInt("level");
        bf q = g.q();
        if (optLong > 0) {
            q.aj = new Date(optLong);
        }
        q.ai = optInt;
        new aq().b(q);
        Intent intent = new Intent(w.f2366a);
        intent.putExtra("momoid", q.h);
        g.c().sendBroadcast(intent);
        return true;
    }
}
