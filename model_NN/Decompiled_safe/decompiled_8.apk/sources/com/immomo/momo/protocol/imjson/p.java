package com.immomo.momo.protocol.imjson;

import com.immomo.a.a.a;
import com.immomo.momo.protocol.imjson.task.ReadedTask;
import com.immomo.momo.protocol.imjson.task.SendTask;
import com.immomo.momo.protocol.imjson.task.Task;
import com.immomo.momo.protocol.imjson.task.g;
import com.immomo.momo.service.a.ay;
import com.immomo.momo.util.m;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public final class p {
    /* access modifiers changed from: private */
    public static boolean b = false;
    private static BlockingQueue c = new LinkedBlockingQueue();
    private static BlockingQueue d = new LinkedBlockingQueue();
    private static BlockingQueue e = new LinkedBlockingQueue();
    private static BlockingQueue f = new LinkedBlockingQueue();
    private static Object l = new Object();
    private static /* synthetic */ int[] m;
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public a f2928a = null;
    private s g;
    private s h;
    private s i;
    private s j;
    /* access modifiers changed from: private */
    public boolean k;

    public p(a aVar) {
        new m(this);
        this.g = null;
        this.h = null;
        this.i = null;
        this.j = null;
        this.k = false;
        this.f2928a = aVar;
        b = false;
        c.clear();
        e.clear();
        d.clear();
        f.clear();
    }

    private static void a(s sVar) {
        if (sVar != null) {
            Task b2 = sVar.b();
            if (b2 != null) {
                b2.h_();
            }
            try {
                sVar.interrupt();
                sVar.a();
                if (sVar.d()) {
                    sVar.f();
                }
            } catch (Exception e2) {
                sVar.a();
                if (sVar.d()) {
                    sVar.f();
                }
            } catch (Throwable th) {
                sVar.a();
                if (sVar.d()) {
                    sVar.f();
                }
                throw th;
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0019, code lost:
        switch(k()[r2.d.ordinal()]) {
            case 1: goto L_0x001d;
            case 2: goto L_0x0031;
            case 3: goto L_0x002b;
            case 4: goto L_0x0037;
            default: goto L_0x001c;
        };
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001d, code lost:
        com.immomo.momo.protocol.imjson.p.d.put(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0024, code lost:
        r2.h_();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:?, code lost:
        com.immomo.momo.protocol.imjson.p.e.put(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0031, code lost:
        com.immomo.momo.protocol.imjson.p.c.put(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0037, code lost:
        com.immomo.momo.protocol.imjson.p.f.put(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void a(com.immomo.momo.protocol.imjson.task.SendTask r2) {
        /*
            java.lang.Object r1 = com.immomo.momo.protocol.imjson.p.l
            monitor-enter(r1)
            boolean r0 = com.immomo.momo.protocol.imjson.p.b     // Catch:{ all -> 0x0028 }
            if (r0 != 0) goto L_0x000c
            r2.h_()     // Catch:{ all -> 0x0028 }
            monitor-exit(r1)     // Catch:{ all -> 0x0028 }
        L_0x000b:
            return
        L_0x000c:
            monitor-exit(r1)
            int[] r0 = k()     // Catch:{ InterruptedException -> 0x0023 }
            com.immomo.momo.protocol.imjson.task.g r1 = r2.d     // Catch:{ InterruptedException -> 0x0023 }
            int r1 = r1.ordinal()     // Catch:{ InterruptedException -> 0x0023 }
            r0 = r0[r1]     // Catch:{ InterruptedException -> 0x0023 }
            switch(r0) {
                case 1: goto L_0x001d;
                case 2: goto L_0x0031;
                case 3: goto L_0x002b;
                case 4: goto L_0x0037;
                default: goto L_0x001c;
            }     // Catch:{ InterruptedException -> 0x0023 }
        L_0x001c:
            goto L_0x000b
        L_0x001d:
            java.util.concurrent.BlockingQueue r0 = com.immomo.momo.protocol.imjson.p.d     // Catch:{ InterruptedException -> 0x0023 }
            r0.put(r2)     // Catch:{ InterruptedException -> 0x0023 }
            goto L_0x000b
        L_0x0023:
            r0 = move-exception
            r2.h_()
            goto L_0x000b
        L_0x0028:
            r0 = move-exception
            monitor-exit(r1)
            throw r0
        L_0x002b:
            java.util.concurrent.BlockingQueue r0 = com.immomo.momo.protocol.imjson.p.e     // Catch:{ InterruptedException -> 0x0023 }
            r0.put(r2)     // Catch:{ InterruptedException -> 0x0023 }
            goto L_0x000b
        L_0x0031:
            java.util.concurrent.BlockingQueue r0 = com.immomo.momo.protocol.imjson.p.c     // Catch:{ InterruptedException -> 0x0023 }
            r0.put(r2)     // Catch:{ InterruptedException -> 0x0023 }
            goto L_0x000b
        L_0x0037:
            java.util.concurrent.BlockingQueue r0 = com.immomo.momo.protocol.imjson.p.f     // Catch:{ InterruptedException -> 0x0023 }
            r0.put(r2)     // Catch:{ InterruptedException -> 0x0023 }
            goto L_0x000b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.immomo.momo.protocol.imjson.p.a(com.immomo.momo.protocol.imjson.task.SendTask):void");
    }

    public static boolean a() {
        return b;
    }

    private void i() {
        j();
        b = true;
        this.g = new s(this, c);
        this.g.start();
        this.h = new s(this, d);
        this.h.start();
        this.i = new q(this, e);
        this.i.start();
        this.j = new t(this, f);
        this.j.start();
        List<Map> b2 = ay.g().b();
        if (b2 != null) {
            for (Map map : b2) {
                try {
                    if (((Integer) map.get("type")).intValue() == 1) {
                        ReadedTask readedTask = new ReadedTask();
                        readedTask.a(map.get("msgid").toString());
                        readedTask.b(map.get("info").toString());
                        e.put(readedTask);
                    }
                } catch (Exception e2) {
                }
            }
        }
    }

    private void j() {
        a(this.g);
        a(this.i);
        a(this.h);
        a(this.j);
        this.g = null;
        this.i = null;
        this.h = null;
        this.j = null;
    }

    private static /* synthetic */ int[] k() {
        int[] iArr = m;
        if (iArr == null) {
            iArr = new int[g.values().length];
            try {
                iArr[g.AsyncExpress.ordinal()] = 3;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[g.FinesseExpress.ordinal()] = 4;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[g.Succession.ordinal()] = 1;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[g.SuccessionLongtime.ordinal()] = 2;
            } catch (NoSuchFieldError e5) {
            }
            m = iArr;
        }
        return iArr;
    }

    public final boolean b() {
        return this.k && b;
    }

    public final void c() {
        synchronized (l) {
            this.k = false;
            i();
        }
    }

    public final void d() {
        synchronized (l) {
            b = false;
            j();
            while (true) {
                SendTask sendTask = (SendTask) c.poll();
                if (sendTask == null) {
                    break;
                }
                sendTask.h_();
            }
            while (true) {
                SendTask sendTask2 = (SendTask) d.poll();
                if (sendTask2 == null) {
                    while (true) {
                        SendTask sendTask3 = (SendTask) e.poll();
                        if (sendTask3 != null) {
                            sendTask3.h_();
                        }
                    }
                } else {
                    sendTask2.h_();
                }
            }
        }
    }

    public final void e() {
        synchronized (l) {
            if (!this.k) {
                if (this.g != null) {
                    this.g.e();
                }
                if (this.i != null) {
                    this.i.e();
                }
                if (this.h != null) {
                    this.h.e();
                }
                if (this.j != null) {
                    this.j.e();
                }
                this.k = true;
            }
        }
    }

    public final void f() {
        synchronized (l) {
            this.k = true;
            i();
        }
    }

    public final void g() {
        synchronized (l) {
            this.k = false;
            if (!b) {
                i();
            } else {
                this.g.f();
                this.i.f();
                this.h.f();
                this.j.f();
            }
        }
    }
}
