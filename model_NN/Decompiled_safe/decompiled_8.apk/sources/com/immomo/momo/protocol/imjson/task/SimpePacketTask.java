package com.immomo.momo.protocol.imjson.task;

import com.immomo.a.a.a;
import com.immomo.a.a.d.b;
import com.immomo.a.a.d.i;
import com.immomo.momo.util.m;
import org.json.JSONObject;

public class SimpePacketTask extends SendTask {

    /* renamed from: a  reason: collision with root package name */
    private b f2938a = null;
    private m b = new m(this);
    private f e = null;
    private b f = null;
    private boolean g = true;

    public SimpePacketTask(g gVar, b bVar) {
        super(gVar);
        this.f2938a = bVar;
    }

    public final void a(f fVar) {
        this.e = fVar;
    }

    public final boolean a(a aVar) {
        try {
            if (this.g) {
                i iVar = new i(aVar);
                iVar.a((JSONObject) this.f2938a);
                this.f = iVar.j();
            } else {
                aVar.a(this.f2938a);
            }
            return true;
        } catch (Exception e2) {
            this.b.a((Throwable) e2);
            return false;
        }
    }

    public final void c() {
        this.g = false;
    }

    public final b d() {
        return this.f;
    }

    public final void g_() {
        if (this.e != null) {
            this.e.a();
        }
    }

    public final void h_() {
        if (this.e != null) {
            this.e.b();
        }
    }

    public String toString() {
        return "SimpePacketTask [packet=" + this.f2938a + "]";
    }
}
