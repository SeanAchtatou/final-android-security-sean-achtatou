package com.immomo.momo.protocol.imjson;

import com.baidu.location.LocationClientOption;

public enum i {
    LEVEL_1(5, 3),
    LEVEL_2(15, 4),
    LEVEL_3(30, 8),
    LEVEL_5(100, 30),
    LEVEL_6(1200, 20);
    
    private final int f;
    private final int g;
    private int h = 0;

    private i(int i2, int i3) {
        this.g = i3;
        this.f = i2 * LocationClientOption.MIN_SCAN_SPAN;
        this.h = 0;
    }

    public final boolean a() {
        int i2 = this.h + 1;
        this.h = i2;
        if (i2 <= this.g) {
            return true;
        }
        this.h = 0;
        return false;
    }

    public final void b() {
        this.h = 0;
    }
}
