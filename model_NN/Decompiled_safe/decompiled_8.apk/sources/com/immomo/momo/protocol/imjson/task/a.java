package com.immomo.momo.protocol.imjson.task;

import com.immomo.momo.service.bean.Message;

final class a extends Thread {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ AudioMessageTask f2939a;
    private final /* synthetic */ Message b;

    a(AudioMessageTask audioMessageTask, Message message) {
        this.f2939a = audioMessageTask;
        this.b = message;
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0097  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x00a8 A[SYNTHETIC, Splitter:B:20:0x00a8] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00b9  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x00d7 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
            r9 = this;
            r1 = 0
            r7 = 3
            r6 = 1
            java.io.File r0 = new java.io.File     // Catch:{ Exception -> 0x00fb }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00fb }
            com.immomo.momo.protocol.imjson.task.AudioMessageTask r3 = r9.f2939a     // Catch:{ Exception -> 0x00fb }
            java.io.File r3 = r3.f2933a     // Catch:{ Exception -> 0x00fb }
            java.lang.String r3 = r3.getPath()     // Catch:{ Exception -> 0x00fb }
            java.lang.String r3 = java.lang.String.valueOf(r3)     // Catch:{ Exception -> 0x00fb }
            r2.<init>(r3)     // Catch:{ Exception -> 0x00fb }
            java.lang.String r3 = ".amr"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x00fb }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x00fb }
            r0.<init>(r2)     // Catch:{ Exception -> 0x00fb }
            r0.createNewFile()     // Catch:{ Exception -> 0x0113 }
            com.immomo.momo.protocol.imjson.task.AudioMessageTask r2 = r9.f2939a     // Catch:{ Exception -> 0x0113 }
            java.io.File r2 = r2.f2933a     // Catch:{ Exception -> 0x0113 }
            java.lang.String r2 = r2.getPath()     // Catch:{ Exception -> 0x0113 }
            java.lang.String r3 = r0.getPath()     // Catch:{ Exception -> 0x0113 }
            r4 = 1
            r5 = 16
            com.immomo.momo.util.jni.LocalAudioHolder.encodeWAV2AMR(r2, r3, r4, r5)     // Catch:{ Exception -> 0x0113 }
            long r2 = r0.length()     // Catch:{ Exception -> 0x0113 }
            r4 = 200(0xc8, double:9.9E-322)
            int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r2 <= 0) goto L_0x008f
            long r2 = r0.length()     // Catch:{ Exception -> 0x0113 }
            r4 = 256000(0x3e800, double:1.26481E-318)
            int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r2 >= 0) goto L_0x008f
            com.immomo.momo.service.bean.Message r2 = r9.b     // Catch:{ Exception -> 0x0113 }
            int r2 = r2.chatType     // Catch:{ Exception -> 0x0113 }
            if (r2 != r6) goto L_0x00e2
            com.immomo.momo.service.bean.Message r1 = r9.b     // Catch:{ Exception -> 0x0113 }
            java.lang.String r1 = r1.remoteId     // Catch:{ Exception -> 0x0113 }
        L_0x0057:
            com.immomo.momo.service.bean.Message r2 = r9.b     // Catch:{ Exception -> 0x0113 }
            int r2 = r2.chatType     // Catch:{ Exception -> 0x0113 }
            java.lang.String r1 = com.immomo.momo.protocol.imjson.a.a(r1, r0, r2)     // Catch:{ Exception -> 0x0113 }
            boolean r2 = android.support.v4.b.a.f(r1)     // Catch:{ Exception -> 0x0113 }
            if (r2 == 0) goto L_0x008f
            com.immomo.momo.service.bean.Message r2 = r9.b     // Catch:{ Exception -> 0x0113 }
            r2.fileName = r1     // Catch:{ Exception -> 0x0113 }
            com.immomo.momo.service.bean.Message r2 = r9.b     // Catch:{ Exception -> 0x0113 }
            long r3 = r0.length()     // Catch:{ Exception -> 0x0113 }
            r2.fileSize = r3     // Catch:{ Exception -> 0x0113 }
            com.immomo.momo.service.af r2 = com.immomo.momo.service.af.c()     // Catch:{ Exception -> 0x0113 }
            com.immomo.momo.service.bean.Message r3 = r9.b     // Catch:{ Exception -> 0x0113 }
            r2.b(r3)     // Catch:{ Exception -> 0x0113 }
            com.immomo.momo.service.bean.Message r2 = r9.b     // Catch:{ Exception -> 0x0113 }
            com.immomo.momo.protocol.imjson.task.AudioMessageTask r3 = r9.f2939a     // Catch:{ Exception -> 0x0113 }
            java.io.File r3 = r3.f2933a     // Catch:{ Exception -> 0x0113 }
            java.lang.String r3 = r3.getName()     // Catch:{ Exception -> 0x0113 }
            java.io.File r1 = com.immomo.momo.util.h.a(r3, r1)     // Catch:{ Exception -> 0x0113 }
            r2.tempFile = r1     // Catch:{ Exception -> 0x0113 }
            com.immomo.momo.protocol.imjson.task.AudioMessageTask r1 = r9.f2939a     // Catch:{ Exception -> 0x0113 }
            r1.e = true     // Catch:{ Exception -> 0x0113 }
        L_0x008f:
            com.immomo.momo.protocol.imjson.task.AudioMessageTask r1 = r9.f2939a
            boolean r1 = r1.e
            if (r1 != 0) goto L_0x00a6
            com.immomo.momo.protocol.imjson.task.AudioMessageTask r1 = r9.f2939a
            int r2 = r1.b
            int r3 = r2 + 1
            r1.b = r3
            if (r2 < r7) goto L_0x00a6
            com.immomo.momo.protocol.imjson.task.AudioMessageTask r1 = r9.f2939a
            r1.c()
        L_0x00a6:
            if (r0 == 0) goto L_0x00b1
            boolean r1 = r0.exists()     // Catch:{ Exception -> 0x0107 }
            if (r1 == 0) goto L_0x00b1
            r0.delete()     // Catch:{ Exception -> 0x0107 }
        L_0x00b1:
            com.immomo.momo.protocol.imjson.task.AudioMessageTask r0 = r9.f2939a
            java.util.TimerTask r0 = r0.g
            if (r0 == 0) goto L_0x00d0
            com.immomo.momo.protocol.imjson.task.AudioMessageTask r0 = r9.f2939a
            java.util.TimerTask r0 = r0.g
            r0.cancel()
            com.immomo.momo.protocol.imjson.task.AudioMessageTask r0 = r9.f2939a
            r0.g = null
            com.immomo.momo.protocol.imjson.task.AudioMessageTask r0 = r9.f2939a
            java.util.Timer r0 = r0.h
            r0.purge()
        L_0x00d0:
            com.immomo.momo.protocol.imjson.task.AudioMessageTask r0 = r9.f2939a
            java.lang.Object r1 = r0.f
            monitor-enter(r1)
            com.immomo.momo.protocol.imjson.task.AudioMessageTask r0 = r9.f2939a     // Catch:{ all -> 0x0110 }
            java.lang.Object r0 = r0.f     // Catch:{ all -> 0x0110 }
            r0.notify()     // Catch:{ all -> 0x0110 }
            monitor-exit(r1)     // Catch:{ all -> 0x0110 }
            return
        L_0x00e2:
            com.immomo.momo.service.bean.Message r2 = r9.b     // Catch:{ Exception -> 0x0113 }
            int r2 = r2.chatType     // Catch:{ Exception -> 0x0113 }
            r3 = 2
            if (r2 != r3) goto L_0x00ef
            com.immomo.momo.service.bean.Message r1 = r9.b     // Catch:{ Exception -> 0x0113 }
            java.lang.String r1 = r1.groupId     // Catch:{ Exception -> 0x0113 }
            goto L_0x0057
        L_0x00ef:
            com.immomo.momo.service.bean.Message r2 = r9.b     // Catch:{ Exception -> 0x0113 }
            int r2 = r2.chatType     // Catch:{ Exception -> 0x0113 }
            if (r2 != r7) goto L_0x0057
            com.immomo.momo.service.bean.Message r1 = r9.b     // Catch:{ Exception -> 0x0113 }
            java.lang.String r1 = r1.discussId     // Catch:{ Exception -> 0x0113 }
            goto L_0x0057
        L_0x00fb:
            r0 = move-exception
            r8 = r0
            r0 = r1
            r1 = r8
        L_0x00ff:
            com.immomo.momo.protocol.imjson.task.AudioMessageTask r2 = r9.f2939a
            com.immomo.momo.util.m r2 = r2.c
            r2.a(r1)
            goto L_0x008f
        L_0x0107:
            r0 = move-exception
            com.immomo.momo.protocol.imjson.task.AudioMessageTask r1 = r9.f2939a
            com.immomo.momo.util.m r1 = r1.c
            r1.a(r0)
            goto L_0x00b1
        L_0x0110:
            r0 = move-exception
            monitor-exit(r1)
            throw r0
        L_0x0113:
            r1 = move-exception
            goto L_0x00ff
        */
        throw new UnsupportedOperationException("Method not decompiled: com.immomo.momo.protocol.imjson.task.a.run():void");
    }
}
