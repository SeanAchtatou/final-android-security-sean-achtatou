package com.immomo.momo.protocol.imjson.a;

import android.os.Looper;

final class m implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ l f2906a;

    m(l lVar) {
        this.f2906a = lVar;
    }

    public final void run() {
        Looper.prepare();
        this.f2906a.c = new n(this.f2906a, Looper.myLooper());
        this.f2906a.d = true;
        Looper.loop();
    }
}
