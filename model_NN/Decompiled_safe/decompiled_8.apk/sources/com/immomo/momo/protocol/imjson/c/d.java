package com.immomo.momo.protocol.imjson.c;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.b.a;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.maintab.MaintabActivity;
import com.immomo.momo.g;
import com.immomo.momo.service.ag;
import com.immomo.momo.service.aq;
import com.immomo.momo.service.bean.Message;
import com.immomo.momo.service.bean.a.b;
import com.immomo.momo.service.bean.a.i;
import com.immomo.momo.service.bean.at;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.service.bean.n;
import com.immomo.momo.service.bean.o;
import com.immomo.momo.service.c;
import com.immomo.momo.service.t;
import com.immomo.momo.service.y;
import com.immomo.momo.util.h;
import com.immomo.momo.util.j;
import java.util.List;

public final class d extends a {

    /* renamed from: a  reason: collision with root package name */
    private bf f2915a = null;
    private at b = null;
    private Context c = null;

    public d() {
        super(50, b.f2913a);
    }

    private static Message a(Bundle bundle) {
        List list = (List) bundle.getSerializable("messagearray");
        if (list == null || list.isEmpty()) {
            return null;
        }
        for (int size = list.size() - 1; size >= 0; size--) {
            Message message = (Message) list.get(size);
            if (message != null && 5 != message.contentType) {
                return message;
            }
        }
        return null;
    }

    private static String a(Message message) {
        return (message.contentType == 0 || message.contentType == 3) ? new StringBuilder(String.valueOf(message.getContent())).toString().replaceAll("(\\[[.[^\\[\\]]]*?\\|et\\|([.[^\\[\\]]]*?\\|)*[.[^\\[\\]]]*?\\])", "[表情]") : message.contentType == 1 ? "发了一张图片" : message.contentType == 2 ? "发了一个位置" : message.contentType == 4 ? "发了一段语音" : message.contentType == 6 ? "发了一个表情" : message.contentType == 7 ? new StringBuilder(String.valueOf(message.getContent())).toString() : PoiTypeDef.All;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.service.h.a(java.lang.String, boolean):com.immomo.momo.service.bean.n
     arg types: [java.lang.String, int]
     candidates:
      com.immomo.momo.service.h.a(com.immomo.momo.service.bean.n, java.lang.String):void
      com.immomo.momo.service.h.a(java.lang.String, int):void
      com.immomo.momo.service.h.a(java.lang.String, java.lang.String):void
      com.immomo.momo.service.h.a(java.lang.String, java.lang.String[]):void
      com.immomo.momo.service.h.a(java.util.List, java.lang.String):void
      com.immomo.momo.service.h.a(java.lang.String, boolean):com.immomo.momo.service.bean.n */
    public final boolean a(Bundle bundle, String str) {
        String h;
        String str2;
        int i;
        int i2;
        String str3;
        String str4;
        String str5;
        String str6;
        o d;
        String str7;
        i c2;
        String str8;
        com.immomo.momo.service.bean.i c3;
        String str9;
        String str10;
        String str11;
        Bitmap bitmap = null;
        this.f2915a = g.q();
        this.b = g.r();
        this.c = g.c();
        if (!(this.f2915a == null || this.b == null || this.c == null || !this.b.f2996a)) {
            if ("actions.groupaction".equals(str)) {
                if (this.b.t) {
                    int i3 = bundle.getInt("gaunreaded", 0);
                    b i4 = new ag().i(bundle.getString("msgid"));
                    if (i4 != null) {
                        String k = i4.a() == 1000 ? "附近群组推荐" : i4.k();
                        String str12 = k.length() > 20 ? "有新的群组消息:" + k.substring(0, 20) + "..." : "有新的群组消息:" + k;
                        Intent intent = new Intent(this.c, MaintabActivity.class);
                        intent.putExtra("tabindex", 1);
                        g.d().a(null, R.drawable.ic_taskbar_system, str12, "群组动态", k, i3, 1953, intent);
                    }
                }
            } else if ("actions.feeds".equals(str)) {
                if (this.b.u) {
                    String string = bundle.getString("stype");
                    if ("newfeed".equals(string)) {
                        str10 = "好友动态";
                        String string2 = bundle.getString("content");
                        if (string2 == null) {
                            string2 = PoiTypeDef.All;
                        }
                        if (string2.length() > 20) {
                            str9 = "新的好友动态:" + string2.substring(0, 20) + "...";
                            str11 = string2;
                        } else {
                            str9 = "新的好友动态:" + string2;
                            str11 = string2;
                        }
                    } else if ("newcomment".equals(string)) {
                        str9 = "你的留言有新评论";
                        str10 = "留言评论";
                        str11 = "你的留言有新评论";
                    }
                    int i5 = bundle.getInt("feedunreaded", 1);
                    Intent intent2 = new Intent(this.c, MaintabActivity.class);
                    intent2.putExtra("tabindex", 1);
                    g.d().a(null, R.drawable.ic_taskbar_system, str9, str10, str11, i5, 1953, intent2);
                }
            } else if ("actions.contactnotice".equals(str)) {
                if (this.b.x) {
                    int i6 = bundle.getInt("contactnoticeunreded", 0);
                    String string3 = bundle.getString("msgid");
                    if (!a.a((CharSequence) string3) && (c3 = new c().c(string3)) != null) {
                        String j = c3.j();
                        Intent intent3 = new Intent(this.c, MaintabActivity.class);
                        intent3.putExtra("tabindex", 2);
                        g.d().a(null, R.drawable.ic_taskbar_system, "新的好友推荐", "好友推荐", j, i6, 1954, intent3);
                    }
                }
            } else if ("actions.usermessage".equals(str) || "actions.himessage".equals(str)) {
                Message a2 = a(bundle);
                if (a2 != null && a2.receive) {
                    Intent intent4 = new Intent(this.c, MaintabActivity.class);
                    intent4.putExtra("tabindex", 2);
                    bf c4 = new aq().c(a2.remoteId);
                    String a3 = a(a2);
                    if (!a2.isSayhi) {
                        h = c4 != null ? c4.h() : a2.remoteId;
                        if (a.a((CharSequence) h)) {
                            h = a2.remoteId;
                        }
                        if (this.b.f) {
                            String str13 = PoiTypeDef.All;
                            if (a2.getDiatance() >= 0.0f) {
                                str13 = "[" + a.a(a2.getDiatance() / 1000.0f) + "km]";
                            }
                            str2 = String.valueOf(str13) + (a3.length() > 20 ? String.valueOf(h) + ": " + a3.substring(0, 20) + "..." : String.valueOf(h) + ": " + a3);
                        } else {
                            str2 = "陌陌的新消息";
                        }
                        i = 1950;
                        i2 = R.drawable.ic_taskbar_icon;
                        if (c4 != null && !a.a((CharSequence) c4.getLoadImageId()) && (bitmap = j.a(c4.getLoadImageId())) == null && (bitmap = a.l(h.a(c4.getLoadImageId(), 3).getPath())) != null) {
                            j.a(c4.getLoadImageId(), bitmap);
                        }
                        str3 = h;
                    } else if (this.b.y) {
                        h = bundle.getString("username");
                        str2 = "陌生人向你打招呼";
                        i = 1951;
                        i2 = R.drawable.ic_taskbar_sayhi;
                        intent4.putExtra("source", "sayhi");
                        str3 = str2;
                    }
                    if (a.a((CharSequence) h)) {
                        h = a2.remoteId;
                    }
                    String str14 = String.valueOf(h) + ": " + a3;
                    if (a2.getDiatance() >= 0.0f) {
                        str14 = "[" + a.a(a2.getDiatance() / 1000.0f) + "km] " + str14;
                    }
                    g.d().a(bitmap, i2, str2, str3, str14, bundle.getInt("userunreaded", 1), i, intent4);
                }
            } else if ("actions.gmessage".equals(str)) {
                Message a4 = a(bundle);
                if (a4 != null && a4.receive && ((c2 = this.b.c(a4.groupId)) == null || c2.f2978a)) {
                    bf c5 = new aq().c(a4.remoteId);
                    String a5 = a(a4);
                    String str15 = c5 != null ? String.valueOf(c5.h()) + ": " + a5 : String.valueOf(a4.remoteId) + ": " + a5;
                    String str16 = a4.getDiatance() >= 0.0f ? "[" + a.a(a4.getDiatance() / 1000.0f) + "km] " + str15 : str15;
                    com.immomo.momo.service.bean.a.a e = new y().e(a4.groupId);
                    if (e != null) {
                        str8 = e.c;
                        String loadImageId = e.getLoadImageId();
                        if (!a.a((CharSequence) loadImageId) && (bitmap = j.a(loadImageId)) == null && (bitmap = a.l(h.a(loadImageId, 3).getPath())) != null) {
                            j.a(loadImageId, bitmap);
                        }
                    } else {
                        str8 = null;
                    }
                    int i7 = bundle.getInt("groupunreaded", 1);
                    String str17 = a.a(str8) ? a4.groupId : str8;
                    String str18 = this.b.f ? String.valueOf(str17) + " 的群消息" : "陌陌的群消息";
                    Intent intent5 = new Intent(this.c, MaintabActivity.class);
                    intent5.putExtra("tabindex", 2);
                    g.d().a(bitmap, R.drawable.ic_taskbar_group, str18, str17, str16, i7, 1952, intent5);
                }
            } else if ("actions.discuss".equals(str)) {
                Message a6 = a(bundle);
                if (a6 != null && a6.receive && ((d = this.b.d(a6.discussId)) == null || d.f3033a)) {
                    bf c6 = new aq().c(a6.remoteId);
                    String a7 = a(a6);
                    String str19 = c6 != null ? String.valueOf(c6.h()) + ": " + a7 : String.valueOf(a6.remoteId) + ": " + a7;
                    String str20 = a6.getDiatance() >= 0.0f ? "[" + a.a(a6.getDiatance() / 1000.0f) + "km] " + str19 : str19;
                    n a8 = new com.immomo.momo.service.h().a(a6.discussId, false);
                    if (a8 != null) {
                        String loadImageId2 = a8.getLoadImageId();
                        str7 = a8.b;
                        if (!a.a((CharSequence) loadImageId2) && (bitmap = j.a(loadImageId2)) == null && (bitmap = a.l(h.a(loadImageId2, 3).getPath())) != null) {
                            j.a(loadImageId2, bitmap);
                        }
                    } else {
                        str7 = null;
                    }
                    int i8 = bundle.getInt("discussunreaded", 1);
                    String str21 = a.a(str7) ? a6.discussId : str7;
                    String str22 = this.b.f ? String.valueOf(str21) + " 的多人对话消息" : "陌陌的多人对话消息";
                    Intent intent6 = new Intent(this.c, MaintabActivity.class);
                    intent6.putExtra("tabindex", 2);
                    g.d().a(bitmap, R.drawable.ic_taskbar_group, str22, str21, str20, i8, 1959, intent6);
                }
            } else if ("actions.eventdynamics".equals(str)) {
                if (this.b.v) {
                    String string4 = bundle.getString("stype");
                    if ("newfeed".equals(string4)) {
                        str4 = "活动动态";
                        str6 = "新的活动动态";
                        str5 = bundle.getString("content");
                        if (str5 == null) {
                            str5 = PoiTypeDef.All;
                        }
                    } else if ("newcomment".equals(string4)) {
                        str4 = "活动评论";
                        str5 = "你的陌陌附近活动有新的评论";
                        str6 = "你的陌陌附近活动有新的评论";
                    }
                    int i9 = bundle.getInt("eventtotalcount", 1);
                    Intent intent7 = new Intent(this.c, MaintabActivity.class);
                    intent7.putExtra("tabindex", 1);
                    g.d().a(null, R.drawable.ic_taskbar_system, str6, str4, str5, i9, 1953, intent7);
                }
            } else if ("actions.tieba".equals(str)) {
                if (this.b.w && "newcomment".equals(bundle.getString("stype"))) {
                    int i10 = bundle.getInt("tiebacomment", 1);
                    Intent intent8 = new Intent(this.c, MaintabActivity.class);
                    intent8.putExtra("tabindex", 1);
                    g.d().a(null, R.drawable.ic_taskbar_system, "你的陌陌吧发言有新的评论", "陌陌吧评论", "你的陌陌吧发言有新的评论", i10, 1953, intent8);
                }
            } else if ("actions.frienddiscover".equals(str)) {
                int i11 = bundle.getInt("totalunreadeddiscover", 0);
                com.immomo.momo.service.bean.ag a9 = new t().a(bundle.getString("msgid"));
                if (a9 != null) {
                    String h2 = a9.h();
                    Intent intent9 = new Intent(this.c, MaintabActivity.class);
                    intent9.putExtra("tabindex", 2);
                    g.d().a(null, R.drawable.ic_taskbar_system, "新的好友出没", "陌陌动态通知", h2, i11, 1953, intent9);
                }
            } else if ("actions.tiebareport".equals(str) && this.b.w) {
                String string5 = bundle.getString("content");
                int i12 = bundle.getInt("tiebareport", 1);
                Intent intent10 = new Intent(this.c, MaintabActivity.class);
                intent10.putExtra("tabindex", 1);
                g.d().a(null, R.drawable.ic_taskbar_system, "陌陌吧有新的举报需要处理", "陌陌吧管理", string5, i12, 1953, intent10);
            }
        }
        return false;
    }
}
