package com.immomo.momo.protocol.imjson.a;

import android.os.Bundle;
import com.immomo.a.a.a;
import com.immomo.a.a.d.b;
import com.immomo.a.a.f;
import com.immomo.momo.g;
import com.immomo.momo.service.af;

public final class r implements f {

    /* renamed from: a  reason: collision with root package name */
    private a f2909a = null;

    public r(a aVar) {
        this.f2909a = aVar;
    }

    public final void a(Object obj, f fVar) {
    }

    public final boolean a(b bVar) {
        Bundle bundle = new Bundle();
        if (bVar.has("newcomment")) {
            int optInt = bVar.optInt("newcomment");
            bundle.putInt("tiebacomment", optInt);
            af.c().c(optInt);
        }
        if (bVar.optInt("type") == 2) {
            bundle.putString("stype", "newcomment");
        }
        af.c().b(bundle);
        g.d().a(bundle, "actions.tieba");
        com.immomo.a.a.d.a aVar = new com.immomo.a.a.d.a();
        aVar.a((Object) bVar.a());
        aVar.b(bVar.d());
        aVar.put("ns", "tieba-notice");
        this.f2909a.a(aVar);
        return true;
    }
}
