package com.immomo.momo.protocol.imjson.a;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.b.a;
import com.immomo.a.a.f;
import com.immomo.momo.android.broadcast.t;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.Message;
import com.immomo.momo.service.bean.n;
import com.immomo.momo.service.h;
import java.util.Date;

public final class b implements f {
    public final void a(Object obj, f fVar) {
    }

    public final boolean a(com.immomo.a.a.d.b bVar) {
        if ("kick".equals(bVar.opt("event")) || "ban".equals(bVar.opt("event"))) {
            Message message = new Message(true);
            message.chatType = 3;
            message.msgId = bVar.d();
            message.discussId = bVar.optString("did");
            message.contentType = 5;
            message.timestamp = new Date();
            message.setContent(bVar.e());
            Bundle bundle = new Bundle();
            bundle.putString("msgid", message.msgId);
            bundle.putString("remoteuserid", message.remoteId);
            bundle.putString("discussid", message.discussId);
            g.d().a(bundle, "actions.discuss");
            new Intent();
            h hVar = new h();
            if ("kick".equals(bVar.opt("event"))) {
                hVar.c(g.q().h, message.discussId);
                Intent intent = new Intent(t.b);
                intent.putExtra("disid", message.discussId);
                g.c().sendBroadcast(intent);
            } else {
                hVar.a(message.discussId, 3);
                Intent intent2 = new Intent(t.c);
                intent2.putExtra("disid", message.discussId);
                g.c().sendBroadcast(intent2);
            }
        } else if ("rename".equals(bVar.opt("event"))) {
            h hVar2 = new h();
            n nVar = new n();
            nVar.f = bVar.optString("did");
            nVar.b = bVar.optString("name");
            hVar2.b(nVar.f, nVar.b);
            Intent intent3 = new Intent(t.d);
            intent3.putExtra("disid", nVar.f);
            g.c().sendBroadcast(intent3);
        } else if ("join".equals(bVar.opt("event"))) {
            h hVar3 = new h();
            String optString = bVar.optString("did");
            if (a.f(optString)) {
                hVar3.a(g.q().h, optString, 0);
                Intent intent4 = new Intent(t.f2363a);
                intent4.putExtra("disid", optString);
                g.c().sendBroadcast(intent4);
            }
        }
        return true;
    }
}
