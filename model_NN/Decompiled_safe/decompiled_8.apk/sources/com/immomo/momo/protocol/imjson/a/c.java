package com.immomo.momo.protocol.imjson.a;

import android.os.Bundle;
import com.immomo.a.a.a;
import com.immomo.a.a.d.b;
import com.immomo.a.a.f;
import com.immomo.momo.g;
import com.immomo.momo.service.af;
import com.immomo.momo.service.bean.a.d;
import com.sina.sdk.api.message.InviteApi;
import java.util.ArrayList;
import org.json.JSONObject;

public final class c implements f {

    /* renamed from: a  reason: collision with root package name */
    private a f2903a = null;

    public c(a aVar) {
        this.f2903a = aVar;
    }

    public final void a(Object obj, f fVar) {
    }

    public final boolean a(b bVar) {
        String str;
        int i = 0;
        if (bVar.has("newfeed")) {
            af.c().a(bVar.optInt("newfeed"));
        }
        if (bVar.has("newcomment")) {
            af.c().b(bVar.optInt("newcomment"));
        }
        Bundle bundle = new Bundle();
        if (bVar.optInt("type") == 1) {
            bundle.putString("stype", "newfeed");
            if (bVar.has("feed")) {
                StringBuilder sb = new StringBuilder();
                JSONObject jSONObject = bVar.getJSONObject("feed");
                double optDouble = jSONObject.optDouble("distance", -1.0d);
                if (optDouble >= 0.0d) {
                    sb.append("[" + android.support.v4.b.a.a(optDouble / 1000.0d) + "km]");
                }
                String optString = jSONObject.optString(InviteApi.KEY_TEXT);
                if (optString != null) {
                    StringBuilder sb2 = new StringBuilder();
                    StringBuilder sb3 = new StringBuilder(optString);
                    ArrayList arrayList = new ArrayList();
                    d.a(sb3, sb2, arrayList);
                    if (!arrayList.isEmpty()) {
                        int i2 = 0;
                        while (i2 < arrayList.size()) {
                            int indexOf = sb2.indexOf("%s", i);
                            sb2.replace(indexOf, indexOf + 2, ((d) arrayList.get(i2)).f2973a);
                            i2++;
                            i = indexOf + 2;
                        }
                        str = sb2.toString();
                    } else {
                        str = optString;
                    }
                    sb.append(str.replaceAll("&lsb;", "[").replaceAll("&rsb;", "]").replaceAll("&vb;", "|"));
                    af.c().c(sb.toString());
                    bundle.putString("content", sb.toString());
                }
            }
        } else if (bVar.optInt("type") == 2) {
            bundle.putString("stype", "newcomment");
        }
        af.c().b(bundle);
        g.d().a(bundle, "actions.feeds");
        com.immomo.a.a.d.a aVar = new com.immomo.a.a.d.a();
        aVar.a((Object) bVar.a());
        aVar.b(bVar.d());
        aVar.put("ns", "feed-notice");
        this.f2903a.a(aVar);
        return true;
    }
}
