package com.immomo.momo.protocol.a;

import android.support.v4.b.a;
import com.amap.mapapi.location.LocationManagerProxy;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.android.c.w;
import com.immomo.momo.protocol.a.a.f;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.service.bean.q;
import com.immomo.momo.service.bean.s;
import com.immomo.momo.util.ar;
import com.sina.sdk.api.message.InviteApi;
import java.io.File;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

public final class i extends p {
    private static i f = null;
    private static String g = (String.valueOf(b) + "/emotion");
    private static Map h = new HashMap();
    private static String i = "sync_sina";
    private static String j = "sync_renren";
    private static String k = "sync_qqwb";
    private static String l = "user_id";
    private static String m = "msg";
    private static String n = "tag";
    private static String o = "name_tag";
    private static String p = "bg";
    private static String q = InviteApi.KEY_TEXT;

    public static w a(String str) {
        return (w) h.get(str);
    }

    public static i a() {
        if (f == null) {
            f = new i();
        }
        return f;
    }

    public static boolean b(String str) {
        return h.get(str) != null && !((w) h.get(str)).d;
    }

    public static boolean c(String str) {
        w wVar = (w) h.get(str);
        if (wVar != null) {
            h.remove(str);
            if (!wVar.e && !wVar.d) {
                wVar.a();
                return true;
            }
        }
        return false;
    }

    public final f a(String str, String str2, String str3) {
        HashMap hashMap = new HashMap();
        hashMap.put("eid", str);
        hashMap.put("remoteid", str2);
        hashMap.put("in_trade_no", str3);
        return new f(new JSONObject(a(String.valueOf(g) + "/gift", hashMap)));
    }

    public final String a(String str, String str2, bf bfVar) {
        HashMap hashMap = new HashMap();
        hashMap.put("eid", str);
        hashMap.put("in_trade_no", str2);
        this.e.a(hashMap);
        JSONObject jSONObject = new JSONObject(a(String.valueOf(g) + "/buy", hashMap));
        bfVar.b((long) jSONObject.optInt("balance", 0));
        return jSONObject.optString("msg");
    }

    public final String a(boolean z, boolean z2, boolean z3) {
        HashMap hashMap = new HashMap();
        hashMap.put(i, z ? "1" : "0");
        hashMap.put(j, z2 ? "1" : "0");
        hashMap.put(k, z3 ? "1" : "0");
        return new JSONObject(a(String.valueOf(g) + "/invite/share", hashMap)).optString("em", PoiTypeDef.All);
    }

    public final void a(q qVar) {
        s sVar;
        boolean z = true;
        if (a.a((CharSequence) qVar.f3035a)) {
            throw new IllegalArgumentException("emotion.id is null");
        }
        HashMap hashMap = new HashMap();
        hashMap.put("eid", qVar.f3035a);
        JSONObject jSONObject = new JSONObject(a(String.valueOf(g) + "/profile", hashMap));
        qVar.f3035a = jSONObject.optString("eid");
        qVar.b = jSONObject.optString("name");
        qVar.e = jSONObject.optString("cover");
        qVar.f = jSONObject.optString("cover_s");
        qVar.s = jSONObject.optString("desc");
        qVar.l = jSONObject.optString("price_first");
        qVar.m = jSONObject.optString("price_second");
        qVar.k = jSONObject.optInt("sold_count");
        if (jSONObject.has(n)) {
            JSONObject jSONObject2 = jSONObject.getJSONObject(n);
            qVar.i = jSONObject2.optInt(p, 0);
            qVar.j = jSONObject2.optString(q);
        }
        if (jSONObject.has(o)) {
            JSONObject jSONObject3 = jSONObject.getJSONObject(o);
            qVar.c = jSONObject3.optInt(p, 0);
            qVar.d = jSONObject3.optString(q);
        }
        qVar.h = jSONObject.optInt(LocationManagerProxy.KEY_STATUS_CHANGED);
        qVar.r = jSONObject.optString("thumbnail");
        qVar.g = jSONObject.optString("expire_desc");
        qVar.q = jSONObject.optInt("own") == 1;
        qVar.w = jSONObject.optLong("zip_size");
        qVar.x = jSONObject.optInt("is_restrict_buy") == 0;
        if (jSONObject.optInt("is_restrict_gift") != 0) {
            z = false;
        }
        qVar.y = z;
        qVar.z = jSONObject.optString("special_desc");
        qVar.n = jSONObject.getString("price_final");
        qVar.B = jSONObject.optString("background");
        JSONObject optJSONObject = jSONObject.optJSONObject("author");
        if (optJSONObject != null) {
            qVar.o = new bf();
            qVar.o.h = optJSONObject.optString("id");
            qVar.o.i = optJSONObject.optString("name");
            qVar.o.b(optJSONObject.optString("desc"));
            qVar.p = optJSONObject.optString("avatar");
        } else {
            qVar.o = null;
            qVar.p = null;
        }
        JSONObject optJSONObject2 = jSONObject.optJSONObject("task");
        if (optJSONObject2 != null) {
            if (qVar.A != null) {
                sVar = qVar.A;
            } else {
                sVar = new s();
                qVar.A = sVar;
            }
            sVar.f3037a = optJSONObject2.getInt("type");
            sVar.d = optJSONObject2.optString("name");
            sVar.b = optJSONObject2.optInt("total_num");
            sVar.c = optJSONObject2.optInt("own_num");
            optJSONObject2.optString("apkurl");
            sVar.e = optJSONObject2.optString("button");
            return;
        }
        qVar.A = null;
    }

    public final void a(String str, String str2, File file, boolean z) {
        String str3 = z ? String.valueOf(d) + "/et/" + str + "/middle/" + str2 : String.valueOf(d) + "/et/" + str + "/" + str2;
        this.e.a((Object) str3);
        if (z) {
            File file2 = new File(String.valueOf(file.getPath()) + "_");
            a(str3, file2, (w) null);
            q.a(file2, file);
            file2.delete();
        } else {
            File file3 = new File(String.valueOf(file.getPath()) + "_");
            a(str3, file3, (w) null);
            if (file.exists()) {
                file.delete();
            }
            file3.renameTo(file);
            ar.g(file.length());
        }
        this.e.a((Object) (String.valueOf(str3) + " ---success"));
    }

    public final void a(Collection collection) {
        HashMap hashMap = new HashMap();
        hashMap.put("index", "0");
        hashMap.put("count", "-1");
        JSONArray optJSONArray = new JSONObject(a(String.valueOf(g) + "/list/my", hashMap)).optJSONArray("list");
        if (optJSONArray != null) {
            for (int i2 = 0; i2 < optJSONArray.length(); i2++) {
                JSONObject jSONObject = optJSONArray.getJSONObject(i2);
                q qVar = new q();
                collection.add(qVar);
                qVar.f3035a = jSONObject.optString("eid");
                qVar.b = jSONObject.optString("name");
                qVar.e = jSONObject.optString("cover");
                qVar.q = true;
                if (jSONObject.has(n)) {
                    JSONObject jSONObject2 = jSONObject.getJSONObject(n);
                    qVar.i = jSONObject2.optInt(p, 0);
                    qVar.j = jSONObject2.optString(q);
                }
                if (jSONObject.has(o)) {
                    JSONObject jSONObject3 = jSONObject.getJSONObject(o);
                    qVar.c = jSONObject3.optInt(p, 0);
                    qVar.d = jSONObject3.optString(q);
                }
                qVar.f = jSONObject.optString("cover_s");
                qVar.u = jSONObject.optInt("used") == 1;
            }
        }
    }

    public final void a(Collection collection, int i2) {
        HashMap hashMap = new HashMap();
        hashMap.put("index", new StringBuilder(String.valueOf(i2)).toString());
        hashMap.put("count", new StringBuilder(String.valueOf(30)).toString());
        JSONArray optJSONArray = new JSONObject(a(String.valueOf(g) + "/list/hotest", hashMap)).optJSONArray("list");
        if (optJSONArray != null) {
            for (int i3 = 0; i3 < optJSONArray.length(); i3++) {
                JSONObject jSONObject = optJSONArray.getJSONObject(i3);
                q qVar = new q();
                collection.add(qVar);
                qVar.f3035a = jSONObject.optString("eid");
                qVar.b = jSONObject.optString("name");
                qVar.e = jSONObject.optString("cover");
                qVar.q = jSONObject.optInt("own") == 1;
                if (jSONObject.has(n)) {
                    JSONObject jSONObject2 = jSONObject.getJSONObject(n);
                    qVar.i = jSONObject2.optInt(p, 0);
                    qVar.j = jSONObject2.optString(q);
                }
                if (jSONObject.has(o)) {
                    JSONObject jSONObject3 = jSONObject.getJSONObject(o);
                    qVar.c = jSONObject3.optInt(p, 0);
                    qVar.d = jSONObject3.optString(q);
                }
                qVar.f = jSONObject.optString("cover_s");
                qVar.l = jSONObject.optString("price_first");
                qVar.m = jSONObject.optString("price_second");
            }
        }
    }

    public final void a(List list) {
        String[] strArr = new String[list.size()];
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= list.size()) {
                HashMap hashMap = new HashMap();
                hashMap.put("eids", a.a(strArr, ","));
                this.e.a(hashMap);
                a(String.valueOf(b) + "/emotion/remove", hashMap);
                return;
            }
            strArr[i3] = ((q) list.get(i3)).f3035a;
            i2 = i3 + 1;
        }
    }

    public final void b(Collection collection, int i2) {
        HashMap hashMap = new HashMap();
        hashMap.put("index", new StringBuilder(String.valueOf(i2)).toString());
        hashMap.put("count", new StringBuilder(String.valueOf(30)).toString());
        JSONArray optJSONArray = new JSONObject(a(String.valueOf(g) + "/list/latest", hashMap)).optJSONArray("list");
        if (optJSONArray != null) {
            for (int i3 = 0; i3 < optJSONArray.length(); i3++) {
                JSONObject jSONObject = optJSONArray.getJSONObject(i3);
                q qVar = new q();
                collection.add(qVar);
                qVar.f3035a = jSONObject.optString("eid");
                qVar.b = jSONObject.optString("name");
                qVar.e = jSONObject.optString("cover");
                qVar.q = jSONObject.optInt("own") == 1;
                if (jSONObject.has(n)) {
                    JSONObject jSONObject2 = jSONObject.getJSONObject(n);
                    qVar.i = jSONObject2.optInt(p, 0);
                    qVar.j = jSONObject2.optString(q);
                }
                if (jSONObject.has(o)) {
                    JSONObject jSONObject3 = jSONObject.getJSONObject(o);
                    qVar.c = jSONObject3.optInt(p, 0);
                    qVar.d = jSONObject3.optString(q);
                }
                qVar.f = jSONObject.optString("cover_s");
                qVar.l = jSONObject.optString("price_first");
                qVar.m = jSONObject.optString("price_second");
            }
        }
    }

    public final void c(Collection collection, int i2) {
        HashMap hashMap = new HashMap();
        hashMap.put("index", new StringBuilder(String.valueOf(i2)).toString());
        hashMap.put("count", new StringBuilder(String.valueOf(30)).toString());
        JSONArray optJSONArray = new JSONObject(a(String.valueOf(g) + "/list/free", hashMap)).optJSONArray("list");
        if (optJSONArray != null) {
            for (int i3 = 0; i3 < optJSONArray.length(); i3++) {
                JSONObject jSONObject = optJSONArray.getJSONObject(i3);
                q qVar = new q();
                collection.add(qVar);
                qVar.f3035a = jSONObject.optString("eid");
                qVar.b = jSONObject.optString("name");
                qVar.e = jSONObject.optString("cover");
                qVar.q = jSONObject.optInt("own") == 1;
                if (jSONObject.has(n)) {
                    JSONObject jSONObject2 = jSONObject.getJSONObject(n);
                    qVar.i = jSONObject2.optInt(p, 0);
                    qVar.j = jSONObject2.optString(q);
                }
                if (jSONObject.has(o)) {
                    JSONObject jSONObject3 = jSONObject.getJSONObject(o);
                    qVar.c = jSONObject3.optInt(p, 0);
                    qVar.d = jSONObject3.optString(q);
                }
                qVar.f = jSONObject.optString("cover_s");
                qVar.l = jSONObject.optString("price_first");
                qVar.m = jSONObject.optString("price_second");
            }
        }
    }

    public final String d(String str) {
        HashMap hashMap = new HashMap();
        hashMap.put("eid", str);
        return new JSONObject(a(String.valueOf(g) + "/task/one", hashMap)).optString("msg");
    }

    public final JSONObject e(String str) {
        HashMap hashMap = new HashMap();
        hashMap.put("eid", str);
        return new JSONObject(a(String.valueOf(g) + "/task/set", hashMap));
    }

    /* JADX WARNING: Removed duplicated region for block: B:23:0x00eb  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void f(java.lang.String r14) {
        /*
            r13 = this;
            r1 = 0
            java.util.Map r0 = com.immomo.momo.protocol.a.i.h     // Catch:{ Exception -> 0x00e8 }
            java.lang.Object r0 = r0.get(r14)     // Catch:{ Exception -> 0x00e8 }
            com.immomo.momo.android.c.w r0 = (com.immomo.momo.android.c.w) r0     // Catch:{ Exception -> 0x00e8 }
            if (r0 == 0) goto L_0x000f
            boolean r0 = r0.d     // Catch:{ Exception -> 0x00e8 }
            if (r0 == 0) goto L_0x010d
        L_0x000f:
            com.immomo.momo.android.c.j r2 = new com.immomo.momo.android.c.j     // Catch:{ Exception -> 0x00e8 }
            r2.<init>(r14)     // Catch:{ Exception -> 0x00e8 }
            java.util.Map r0 = com.immomo.momo.protocol.a.i.h     // Catch:{ Exception -> 0x0107 }
            r0.put(r14, r2)     // Catch:{ Exception -> 0x0107 }
            android.content.Intent r0 = new android.content.Intent     // Catch:{ Exception -> 0x0107 }
            java.lang.String r1 = com.immomo.momo.android.broadcast.g.b     // Catch:{ Exception -> 0x0107 }
            r0.<init>(r1)     // Catch:{ Exception -> 0x0107 }
            java.lang.String r1 = "eid"
            r0.putExtra(r1, r14)     // Catch:{ Exception -> 0x0107 }
            android.content.Context r1 = com.immomo.momo.g.c()     // Catch:{ Exception -> 0x0107 }
            r1.sendBroadcast(r0)     // Catch:{ Exception -> 0x0107 }
            r7 = r2
        L_0x002d:
            java.util.HashMap r0 = new java.util.HashMap     // Catch:{ Exception -> 0x010a }
            r0.<init>()     // Catch:{ Exception -> 0x010a }
            java.lang.String r1 = "eid"
            r0.put(r1, r14)     // Catch:{ Exception -> 0x010a }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x010a }
            java.lang.String r2 = com.immomo.momo.protocol.a.i.g     // Catch:{ Exception -> 0x010a }
            java.lang.String r2 = java.lang.String.valueOf(r2)     // Catch:{ Exception -> 0x010a }
            r1.<init>(r2)     // Catch:{ Exception -> 0x010a }
            java.lang.String r2 = "/download"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x010a }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x010a }
            java.lang.String r0 = r13.a(r1, r0)     // Catch:{ Exception -> 0x010a }
            org.json.JSONObject r9 = new org.json.JSONObject     // Catch:{ Exception -> 0x010a }
            r9.<init>(r0)     // Catch:{ Exception -> 0x010a }
            java.lang.String r0 = "emotions"
            org.json.JSONArray r10 = r9.getJSONArray(r0)     // Catch:{ Exception -> 0x010a }
            java.util.ArrayList r11 = new java.util.ArrayList     // Catch:{ Exception -> 0x010a }
            r11.<init>()     // Catch:{ Exception -> 0x010a }
            r0 = 0
            r8 = r0
        L_0x0062:
            int r0 = r10.length()     // Catch:{ Exception -> 0x010a }
            if (r8 < r0) goto L_0x00b6
            com.immomo.momo.service.o r0 = new com.immomo.momo.service.o     // Catch:{ Exception -> 0x010a }
            r0.<init>()     // Catch:{ Exception -> 0x010a }
            com.immomo.momo.service.o.a(r11, r14)     // Catch:{ Exception -> 0x010a }
            com.immomo.momo.android.c.j r1 = new com.immomo.momo.android.c.j     // Catch:{ Exception -> 0x010a }
            r1.<init>(r14)     // Catch:{ Exception -> 0x010a }
            java.util.Map r0 = com.immomo.momo.protocol.a.i.h     // Catch:{ Exception -> 0x00e8 }
            r0.put(r14, r1)     // Catch:{ Exception -> 0x00e8 }
            java.lang.String r0 = "download_url"
            java.lang.String r0 = r9.getString(r0)     // Catch:{ Exception -> 0x00e8 }
            com.immomo.momo.util.m r2 = r13.e     // Catch:{ Exception -> 0x00e8 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00e8 }
            java.lang.String r4 = "------------download zip file, url="
            r3.<init>(r4)     // Catch:{ Exception -> 0x00e8 }
            java.lang.StringBuilder r3 = r3.append(r0)     // Catch:{ Exception -> 0x00e8 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x00e8 }
            r2.a(r3)     // Catch:{ Exception -> 0x00e8 }
            java.io.File r2 = new java.io.File     // Catch:{ Exception -> 0x00e8 }
            java.io.File r3 = com.immomo.momo.a.c()     // Catch:{ Exception -> 0x00e8 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00e8 }
            java.lang.String r5 = java.lang.String.valueOf(r14)     // Catch:{ Exception -> 0x00e8 }
            r4.<init>(r5)     // Catch:{ Exception -> 0x00e8 }
            java.lang.String r5 = ".zip"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x00e8 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x00e8 }
            r2.<init>(r3, r4)     // Catch:{ Exception -> 0x00e8 }
            r1.c = r2     // Catch:{ Exception -> 0x00e8 }
            a(r0, r2, r1)     // Catch:{ Exception -> 0x00e8 }
            return
        L_0x00b6:
            org.json.JSONObject r3 = r10.getJSONObject(r8)     // Catch:{ Exception -> 0x010a }
            com.immomo.momo.service.bean.r r0 = new com.immomo.momo.service.bean.r     // Catch:{ Exception -> 0x010a }
            java.lang.String r1 = "d"
            java.lang.String r1 = r3.optString(r1)     // Catch:{ Exception -> 0x010a }
            java.lang.String r2 = "n"
            java.lang.String r2 = r3.optString(r2)     // Catch:{ Exception -> 0x010a }
            java.lang.String r4 = "s"
            java.lang.String r4 = r3.getString(r4)     // Catch:{ Exception -> 0x010a }
            java.lang.String r5 = "k"
            java.lang.String r5 = r3.optString(r5)     // Catch:{ Exception -> 0x010a }
            java.lang.String r6 = "e"
            java.lang.String r12 = ""
            java.lang.String r6 = r3.optString(r6, r12)     // Catch:{ Exception -> 0x010a }
            r3 = r14
            r0.<init>(r1, r2, r3, r4, r5, r6)     // Catch:{ Exception -> 0x010a }
            r11.add(r0)     // Catch:{ Exception -> 0x010a }
            int r0 = r8 + 1
            r8 = r0
            goto L_0x0062
        L_0x00e8:
            r0 = move-exception
        L_0x00e9:
            if (r1 == 0) goto L_0x00ee
            r2 = 1
            r1.d = r2
        L_0x00ee:
            android.content.Intent r1 = new android.content.Intent
            java.lang.String r2 = com.immomo.momo.android.broadcast.g.c
            r1.<init>(r2)
            java.lang.String r2 = "eid"
            r1.putExtra(r2, r14)
            android.content.Context r2 = com.immomo.momo.g.c()
            r2.sendBroadcast(r1)
            com.immomo.momo.util.m r1 = r13.e
            r1.a(r0)
            throw r0
        L_0x0107:
            r0 = move-exception
            r1 = r2
            goto L_0x00e9
        L_0x010a:
            r0 = move-exception
            r1 = r7
            goto L_0x00e9
        L_0x010d:
            r7 = r1
            goto L_0x002d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.immomo.momo.protocol.a.i.f(java.lang.String):void");
    }

    public final JSONObject g(String str) {
        HashMap hashMap = new HashMap();
        hashMap.put("eid", str);
        return new JSONObject(a(String.valueOf(g) + "/task/invite", hashMap));
    }

    public final String h(String str) {
        String str2 = String.valueOf(b) + "/emotion/invite/renren";
        HashMap hashMap = new HashMap();
        hashMap.put(l, str);
        return new JSONObject(a(str2, hashMap)).optString(m);
    }

    public final String i(String str) {
        String str2 = String.valueOf(b) + "/emotion/invite/qqwb";
        HashMap hashMap = new HashMap();
        hashMap.put(l, str);
        return new JSONObject(a(str2, hashMap)).optString(m);
    }

    public final String j(String str) {
        String str2 = String.valueOf(b) + "/emotion/invite/sina";
        HashMap hashMap = new HashMap();
        hashMap.put(l, str);
        return new JSONObject(a(str2, hashMap)).optString(m);
    }
}
