package com.immomo.momo.protocol.a;

import com.immomo.momo.service.bean.bh;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

public final class x extends p {
    private static x f = null;

    public static x a() {
        if (f == null) {
            f = new x();
        }
        return f;
    }

    public final List b() {
        String a2 = a(String.valueOf(b) + "/chatbg/config", (Map) null);
        ArrayList arrayList = new ArrayList();
        JSONArray jSONArray = new JSONObject(a2).getJSONArray("data");
        for (int i = 0; i < jSONArray.length(); i++) {
            arrayList.add(new bh("bg_chat_preview_" + jSONArray.getString(i)));
        }
        return arrayList;
    }
}
