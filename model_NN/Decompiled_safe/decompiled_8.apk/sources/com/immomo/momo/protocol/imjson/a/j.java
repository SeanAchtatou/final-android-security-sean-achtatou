package com.immomo.momo.protocol.imjson.a;

import android.os.Bundle;
import android.util.Log;
import com.immomo.a.a.d.b;
import com.immomo.a.a.f;
import com.immomo.momo.g;
import com.immomo.momo.service.af;
import com.immomo.momo.service.bean.Message;
import java.util.Date;

public final class j implements f {
    public final void a(Object obj, f fVar) {
    }

    public final boolean a(b bVar) {
        String optString = bVar.optString("remoteid");
        String optString2 = bVar.optString("msgid");
        int optInt = bVar.optInt("distance", -1);
        long optLong = bVar.optLong("dt");
        long optLong2 = bVar.optLong("t", System.currentTimeMillis());
        int optInt2 = bVar.optInt("deviation", 0);
        Bundle bundle = new Bundle();
        bundle.putString("stype", "msgdistance");
        bundle.putInt("distance", optInt);
        bundle.putString("remoteuserid", optString);
        bundle.putInt("deviation", optInt2);
        bundle.putString("msgid", optString2);
        bundle.putInt("chattype", 1);
        bundle.putLong("dtime", optLong);
        g.d().a(bundle, "actions.message.status");
        Message a2 = af.c().a(optString2);
        if (a2 != null) {
            a2.distance = (float) optInt;
            a2.distanceTime = null;
            if (optLong > 0) {
                try {
                    a2.distanceTime = new Date(optLong);
                } catch (Exception e) {
                }
            }
            if (a2.status == 1) {
                Log.w("MessageDistance", "===========MOMO====message.status" + a2.status);
                a2.status = 2;
            }
            a2.timestamp = new Date(optLong2);
            af.c().b().b(a2);
        }
        return true;
    }
}
