package com.immomo.momo.protocol.imjson.task;

import android.content.Intent;
import com.immomo.momo.android.broadcast.i;
import com.immomo.momo.g;
import com.immomo.momo.protocol.imjson.b;
import com.immomo.momo.service.af;
import com.immomo.momo.service.bean.Message;

final class d implements b {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ Message f2942a;

    d(Message message) {
        this.f2942a = message;
    }

    public final void a(long j) {
        this.f2942a.fileUploadedLength = j;
        this.f2942a.fileUploadProgrss = (((float) j) * 100.0f) / ((float) this.f2942a.fileSize);
        af.c().b(this.f2942a);
        Intent intent = new Intent(i.f2352a);
        intent.putExtra("key_message_id", this.f2942a.msgId);
        intent.putExtra("key_upload_progress", j);
        g.d().sendBroadcast(intent);
    }
}
