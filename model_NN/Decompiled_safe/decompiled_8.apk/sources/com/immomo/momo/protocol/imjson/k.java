package com.immomo.momo.protocol.imjson;

import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.a.a.a.a;
import com.immomo.a.a.d.b;
import com.immomo.a.a.d.d;
import com.immomo.a.a.i;
import com.immomo.momo.util.jni.Codec;
import org.json.JSONArray;

public final class k implements i {

    /* renamed from: a  reason: collision with root package name */
    public static d f2925a = null;
    private a b = com.immomo.a.a.a.a().a("IMJPacketSecurity");
    private int[] c = {4, 2};
    private int d = -1;
    private boolean e = false;
    private String f = PoiTypeDef.All;
    private com.immomo.a.a.a g = null;

    public k(com.immomo.a.a.a aVar) {
        this.g = aVar;
    }

    public final int a() {
        return this.d;
    }

    public final synchronized String a(String str) {
        if (this.d == 4) {
            this.b.a("L1 --> encryptPassword");
            str = android.support.v4.b.a.b(Codec.a(str.getBytes(), this.f));
        } else if (this.d == 2) {
            str = new String(android.support.v4.b.a.a(str.getBytes(), this.f));
        }
        return str;
    }

    public final void a(b bVar) {
        f2925a = bVar;
        if ("conn".equals(bVar.a()) && bVar.has("etype")) {
            JSONArray jSONArray = bVar.getJSONArray("etype");
            int[] iArr = new int[jSONArray.length()];
            for (int i = 0; i < jSONArray.length(); i++) {
                iArr[i] = jSONArray.getInt(i);
            }
            c();
            if (this.c != null) {
                for (int i2 = 1; i2 < iArr.length; i2++) {
                    int i3 = iArr[i2];
                    int i4 = i2;
                    while (i4 > 0 && iArr[i4 - 1] < i3) {
                        iArr[i4] = iArr[i4 - 1];
                        i4--;
                    }
                    iArr[i4] = i3;
                }
                int i5 = 0;
                loop2:
                while (true) {
                    if (i5 >= iArr.length) {
                        break;
                    }
                    for (int i6 = 0; i6 < this.c.length; i6++) {
                        if (iArr[i5] == this.c[i6]) {
                            this.d = this.c[i6];
                            break loop2;
                        }
                    }
                    i5++;
                }
            }
            this.f = String.valueOf(Codec.kkkxxx(Integer.valueOf(this.g.b().c()).intValue(), Integer.valueOf(bVar.getString("ek")).intValue()));
        }
    }

    public final synchronized byte[] a(byte[] bArr) {
        if (this.e) {
            if (this.d == 4) {
                bArr = Codec.a(bArr, this.f);
            } else if (this.d == 2) {
                bArr = android.support.v4.b.a.a(bArr, this.f);
            }
        }
        return bArr;
    }

    public final void b() {
        this.e = true;
    }

    public final synchronized byte[] b(byte[] bArr) {
        if (this.e) {
            if (this.d == 4) {
                bArr = Codec.b(bArr, this.f);
            } else if (this.d == 2) {
                bArr = android.support.v4.b.a.a(android.support.v4.b.a.r(new String(bArr)), this.f.getBytes());
            }
        }
        return bArr;
    }

    public final synchronized void c() {
        this.d = -1;
        this.e = false;
        this.f = PoiTypeDef.All;
    }

    public final String d() {
        return this.f;
    }
}
