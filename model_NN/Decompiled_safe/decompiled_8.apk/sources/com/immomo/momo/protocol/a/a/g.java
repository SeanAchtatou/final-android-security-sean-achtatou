package com.immomo.momo.protocol.a.a;

import com.immomo.momo.protocol.a.o;
import java.util.List;
import org.json.JSONObject;

public final class g {

    /* renamed from: a  reason: collision with root package name */
    public int f2886a = 0;
    public int b = 0;
    public List c;

    public g(JSONObject jSONObject) {
        jSONObject.optInt("index");
        jSONObject.optInt("count");
        this.f2886a = jSONObject.optInt("remain");
        this.b = jSONObject.optInt("total");
        o.a();
        this.c = o.b(jSONObject);
    }
}
