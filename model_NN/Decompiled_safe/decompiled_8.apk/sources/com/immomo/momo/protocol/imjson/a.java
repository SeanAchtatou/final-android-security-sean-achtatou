package com.immomo.momo.protocol.imjson;

import com.immomo.momo.protocol.a.d;
import com.immomo.momo.util.m;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.UUID;

public final class a {

    /* renamed from: a  reason: collision with root package name */
    private static m f2902a = new m("UploadFileHandler");

    private static String a(ByteArrayOutputStream byteArrayOutputStream, String str, long j, long j2, int i) {
        int size = byteArrayOutputStream.size();
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        f2902a.a((Object) ("正在上传,  大小：" + size + ", uid=" + str));
        return android.support.v4.b.a.f(null) ? d.b(byteArray, size, str, j, j2, i) : d.a(byteArray, size, str, j, j2, i);
    }

    public static final String a(File file, long j, String str, b bVar, int i) {
        return b(file, j, str, bVar, i);
    }

    public static String a(String str, File file, int i) {
        return d.a(str, file, android.support.v4.b.a.n(String.valueOf(UUID.randomUUID().toString()) + System.currentTimeMillis()), i);
    }

    /* JADX WARNING: Removed duplicated region for block: B:48:0x0102  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x0107  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.String b(java.io.File r15, long r16, java.lang.String r18, com.immomo.momo.protocol.imjson.b r19, int r20) {
        /*
            r4 = 0
            r3 = 0
            r2 = 0
            long r6 = r15.length()     // Catch:{ Exception -> 0x00fc, all -> 0x010b }
            com.immomo.momo.util.m r5 = com.immomo.momo.protocol.imjson.a.f2902a     // Catch:{ Exception -> 0x00fc, all -> 0x010b }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00fc, all -> 0x010b }
            java.lang.String r9 = "准备上传，文件总大小："
            r8.<init>(r9)     // Catch:{ Exception -> 0x00fc, all -> 0x010b }
            java.lang.StringBuilder r8 = r8.append(r6)     // Catch:{ Exception -> 0x00fc, all -> 0x010b }
            java.lang.String r9 = ", offset:"
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ Exception -> 0x00fc, all -> 0x010b }
            r0 = r16
            java.lang.StringBuilder r8 = r8.append(r0)     // Catch:{ Exception -> 0x00fc, all -> 0x010b }
            java.lang.String r8 = r8.toString()     // Catch:{ Exception -> 0x00fc, all -> 0x010b }
            r5.a(r8)     // Catch:{ Exception -> 0x00fc, all -> 0x010b }
            java.io.FileInputStream r9 = new java.io.FileInputStream     // Catch:{ Exception -> 0x00fc, all -> 0x010b }
            r9.<init>(r15)     // Catch:{ Exception -> 0x00fc, all -> 0x010b }
            r4 = 0
            int r4 = (r16 > r4 ? 1 : (r16 == r4 ? 0 : -1))
            if (r4 <= 0) goto L_0x009a
            int r4 = (r16 > r6 ? 1 : (r16 == r6 ? 0 : -1))
            if (r4 > 0) goto L_0x009a
            r0 = r16
            long r4 = r9.skip(r0)     // Catch:{ Exception -> 0x0118, all -> 0x010e }
            r10 = 0
            int r8 = (r4 > r10 ? 1 : (r4 == r10 ? 0 : -1))
            if (r8 < 0) goto L_0x0046
            int r4 = (r4 > r16 ? 1 : (r4 == r16 ? 0 : -1))
            if (r4 == 0) goto L_0x0125
        L_0x0046:
            r10 = 0
            r4 = 0
            r9.skip(r4)     // Catch:{ Exception -> 0x0118, all -> 0x010e }
        L_0x004d:
            com.immomo.momo.util.m r4 = com.immomo.momo.protocol.imjson.a.f2902a     // Catch:{ Exception -> 0x0118, all -> 0x010e }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0118, all -> 0x010e }
            java.lang.String r8 = "开始位置："
            r5.<init>(r8)     // Catch:{ Exception -> 0x0118, all -> 0x010e }
            java.lang.StringBuilder r5 = r5.append(r10)     // Catch:{ Exception -> 0x0118, all -> 0x010e }
            java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x0118, all -> 0x010e }
            r4.a(r5)     // Catch:{ Exception -> 0x0118, all -> 0x010e }
            if (r19 == 0) goto L_0x0068
            r0 = r19
            r0.a(r10)     // Catch:{ Exception -> 0x0118, all -> 0x010e }
        L_0x0068:
            java.io.ByteArrayOutputStream r4 = new java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x0118, all -> 0x010e }
            r5 = 10240(0x2800, float:1.4349E-41)
            r4.<init>(r5)     // Catch:{ Exception -> 0x0118, all -> 0x010e }
            r3 = 2048(0x800, float:2.87E-42)
            byte[] r12 = new byte[r3]     // Catch:{ Exception -> 0x011b, all -> 0x0110 }
            r3 = r2
            r2 = r4
            r4 = r10
        L_0x0076:
            int r8 = r9.read(r12)     // Catch:{ Exception -> 0x011f, all -> 0x0113 }
            if (r8 > 0) goto L_0x009d
            int r10 = r2.size()     // Catch:{ Exception -> 0x011f, all -> 0x0113 }
            if (r10 <= 0) goto L_0x0093
            r3 = r18
            r8 = r20
            java.lang.String r3 = a(r2, r3, r4, r6, r8)     // Catch:{ Exception -> 0x011f, all -> 0x0113 }
            long r6 = (long) r10     // Catch:{ Exception -> 0x011f, all -> 0x0113 }
            long r4 = r4 + r6
            if (r19 == 0) goto L_0x0093
            r0 = r19
            r0.a(r4)     // Catch:{ Exception -> 0x011f, all -> 0x0113 }
        L_0x0093:
            r2.close()
            r9.close()
            return r3
        L_0x009a:
            r10 = 0
            goto L_0x004d
        L_0x009d:
            r10 = 0
            r2.write(r12, r10, r8)     // Catch:{ Exception -> 0x011f, all -> 0x0113 }
            int r10 = r2.size()     // Catch:{ Exception -> 0x011f, all -> 0x0113 }
            com.immomo.momo.util.m r8 = com.immomo.momo.protocol.imjson.a.f2902a     // Catch:{ Exception -> 0x011f, all -> 0x0113 }
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x011f, all -> 0x0113 }
            java.lang.String r13 = "@@@@@@@@@@@@@ bosBufferSize"
            r11.<init>(r13)     // Catch:{ Exception -> 0x011f, all -> 0x0113 }
            java.lang.StringBuilder r11 = r11.append(r10)     // Catch:{ Exception -> 0x011f, all -> 0x0113 }
            java.lang.String r11 = r11.toString()     // Catch:{ Exception -> 0x011f, all -> 0x0113 }
            r8.b(r11)     // Catch:{ Exception -> 0x011f, all -> 0x0113 }
            r8 = 10240(0x2800, float:1.4349E-41)
            if (r10 < r8) goto L_0x0076
            r3 = r18
            r8 = r20
            java.lang.String r3 = a(r2, r3, r4, r6, r8)     // Catch:{ Exception -> 0x011f, all -> 0x0113 }
            long r10 = (long) r10     // Catch:{ Exception -> 0x011f, all -> 0x0113 }
            long r10 = r10 + r4
            com.immomo.momo.util.m r4 = com.immomo.momo.protocol.imjson.a.f2902a     // Catch:{ Exception -> 0x011f, all -> 0x0113 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x011f, all -> 0x0113 }
            java.lang.String r8 = "上传成功,  已上传大小："
            r5.<init>(r8)     // Catch:{ Exception -> 0x011f, all -> 0x0113 }
            java.lang.StringBuilder r5 = r5.append(r10)     // Catch:{ Exception -> 0x011f, all -> 0x0113 }
            java.lang.String r8 = " "
            java.lang.StringBuilder r5 = r5.append(r8)     // Catch:{ Exception -> 0x011f, all -> 0x0113 }
            r0 = r18
            java.lang.StringBuilder r5 = r5.append(r0)     // Catch:{ Exception -> 0x011f, all -> 0x0113 }
            java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x011f, all -> 0x0113 }
            r4.a(r5)     // Catch:{ Exception -> 0x011f, all -> 0x0113 }
            if (r19 == 0) goto L_0x00ee
            r0 = r19
            r0.a(r10)     // Catch:{ Exception -> 0x011f, all -> 0x0113 }
        L_0x00ee:
            r2.close()     // Catch:{ Exception -> 0x011f, all -> 0x0113 }
            java.io.ByteArrayOutputStream r4 = new java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x011f, all -> 0x0113 }
            r5 = 10240(0x2800, float:1.4349E-41)
            r4.<init>(r5)     // Catch:{ Exception -> 0x011f, all -> 0x0113 }
            r2 = r4
            r4 = r10
            goto L_0x0076
        L_0x00fc:
            r2 = move-exception
        L_0x00fd:
            throw r2     // Catch:{ all -> 0x00fe }
        L_0x00fe:
            r2 = move-exception
            r9 = r4
        L_0x0100:
            if (r3 == 0) goto L_0x0105
            r3.close()
        L_0x0105:
            if (r9 == 0) goto L_0x010a
            r9.close()
        L_0x010a:
            throw r2
        L_0x010b:
            r2 = move-exception
            r9 = r4
            goto L_0x0100
        L_0x010e:
            r2 = move-exception
            goto L_0x0100
        L_0x0110:
            r2 = move-exception
            r3 = r4
            goto L_0x0100
        L_0x0113:
            r3 = move-exception
            r14 = r3
            r3 = r2
            r2 = r14
            goto L_0x0100
        L_0x0118:
            r2 = move-exception
            r4 = r9
            goto L_0x00fd
        L_0x011b:
            r2 = move-exception
            r3 = r4
            r4 = r9
            goto L_0x00fd
        L_0x011f:
            r3 = move-exception
            r4 = r9
            r14 = r2
            r2 = r3
            r3 = r14
            goto L_0x00fd
        L_0x0125:
            r10 = r16
            goto L_0x004d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.immomo.momo.protocol.imjson.a.b(java.io.File, long, java.lang.String, com.immomo.momo.protocol.imjson.b, int):java.lang.String");
    }
}
