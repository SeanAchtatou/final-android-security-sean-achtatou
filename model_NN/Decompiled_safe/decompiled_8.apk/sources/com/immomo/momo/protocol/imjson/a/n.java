package com.immomo.momo.protocol.imjson.a;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.immomo.momo.g;
import com.immomo.momo.protocol.imjson.c.c;
import com.immomo.momo.service.af;
import com.immomo.momo.service.bean.a.i;
import com.immomo.momo.service.bean.o;
import java.util.ArrayList;
import java.util.HashMap;

final class n extends Handler {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ l f2907a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public n(l lVar, Looper looper) {
        super(looper);
        this.f2907a = lVar;
    }

    /* JADX INFO: finally extract failed */
    public final void handleMessage(Message message) {
        ArrayList arrayList;
        c cVar;
        ArrayList arrayList2;
        ArrayList arrayList3;
        ArrayList arrayList4;
        if (message.what == 123 || message.what == 234) {
            this.f2907a.b.lock();
            try {
                if (message.what != 123 || this.f2907a.f2905a.size() < 5) {
                    ArrayList<com.immomo.momo.service.bean.Message> arrayList5 = new ArrayList<>(this.f2907a.f2905a.size());
                    arrayList5.addAll(this.f2907a.f2905a);
                    this.f2907a.f2905a.clear();
                    this.f2907a.b.unlock();
                    HashMap hashMap = new HashMap();
                    HashMap hashMap2 = new HashMap();
                    HashMap hashMap3 = new HashMap();
                    HashMap hashMap4 = new HashMap();
                    af c = af.c();
                    if (c != null) {
                        try {
                            c.a().beginTransaction();
                            for (com.immomo.momo.service.bean.Message message2 : arrayList5) {
                                if (message2 != null && !c.a(message2.msgId, message2.chatType)) {
                                    if (message2.chatType == 2) {
                                        String str = message2.groupId;
                                        cVar = (c) hashMap2.get(str);
                                        if (cVar == null) {
                                            cVar = new c("actions.gmessage");
                                            cVar.a("groupid", str);
                                            hashMap2.put(str, cVar);
                                            arrayList4 = new ArrayList();
                                            cVar.a("messagearray", arrayList4);
                                        } else {
                                            arrayList4 = (ArrayList) cVar.a("messagearray");
                                        }
                                        arrayList4.add(message2);
                                        i c2 = this.f2907a.f.c(message2.groupId);
                                        if (c2 != null && !c2.f2978a && message2.status == 5) {
                                            message2.status = 13;
                                        }
                                    } else if (message2.chatType == 1) {
                                        String str2 = message2.remoteId;
                                        if (message2.isSayhi) {
                                            c cVar2 = (c) hashMap4.get(str2);
                                            if (cVar2 == null) {
                                                cVar2 = new c("actions.himessage");
                                                cVar2.a("username", message2.username);
                                                hashMap4.put(str2, cVar2);
                                                arrayList3 = new ArrayList();
                                                cVar2.a("messagearray", arrayList3);
                                            } else {
                                                arrayList3 = (ArrayList) cVar2.a("messagearray");
                                            }
                                            arrayList3.add(message2);
                                            if (!this.f2907a.f.y && message2.status == 5) {
                                                message2.status = 13;
                                            }
                                        } else {
                                            c cVar3 = (c) hashMap.get(str2);
                                            if (cVar3 == null) {
                                                cVar3 = new c("actions.usermessage");
                                                hashMap.put(str2, cVar3);
                                                arrayList2 = new ArrayList();
                                                cVar3.a("messagearray", arrayList2);
                                            } else {
                                                arrayList2 = (ArrayList) cVar3.a("messagearray");
                                            }
                                            arrayList2.add(message2);
                                        }
                                    } else if (message2.chatType == 3) {
                                        String str3 = message2.discussId;
                                        c cVar4 = (c) hashMap3.get(str3);
                                        if (cVar4 == null) {
                                            cVar4 = new c("actions.discuss");
                                            cVar4.a("discussid", str3);
                                            hashMap3.put(str3, cVar4);
                                            arrayList = new ArrayList();
                                            cVar4.a("messagearray", arrayList);
                                        } else {
                                            arrayList = (ArrayList) cVar4.a("messagearray");
                                        }
                                        arrayList.add(message2);
                                        o d = this.f2907a.f.d(message2.discussId);
                                        if (d != null && !d.f3033a && message2.status == 5) {
                                            message2.status = 13;
                                        }
                                    }
                                    c.a(message2);
                                    cVar.a("remoteuserid", message2.remoteId);
                                }
                            }
                            c.a().setTransactionSuccessful();
                            c.a().endTransaction();
                            int e = c.e();
                            int f = c.f();
                            int g = c.g();
                            int j = c.j() + c.i();
                            int d2 = c.d();
                            int h = c.h();
                            int i = this.f2907a.f.x ? e + f + h + d2 : e + f + d2;
                            for (c cVar5 : hashMap.values()) {
                                cVar5.a("groupunreaded", f);
                                cVar5.a("userunreaded", e);
                                cVar5.a("gaunreaded", g);
                                cVar5.a("feedunreaded", j);
                                cVar5.a("totalunreaded", i);
                                cVar5.a("contactnoticeunreded", h);
                                cVar5.a("discussunreaded", d2);
                                g.d().a(cVar5.a(), cVar5.b());
                            }
                            for (c cVar6 : hashMap4.values()) {
                                cVar6.a("groupunreaded", f);
                                cVar6.a("userunreaded", e);
                                cVar6.a("gaunreaded", g);
                                cVar6.a("feedunreaded", j);
                                cVar6.a("totalunreaded", i);
                                cVar6.a("contactnoticeunreded", h);
                                cVar6.a("discussunreaded", d2);
                                g.d().a(cVar6.a(), cVar6.b());
                            }
                            for (c cVar7 : hashMap2.values()) {
                                cVar7.a("groupunreaded", f);
                                cVar7.a("userunreaded", e);
                                cVar7.a("gaunreaded", g);
                                cVar7.a("feedunreaded", j);
                                cVar7.a("totalunreaded", i);
                                cVar7.a("contactnoticeunreded", h);
                                cVar7.a("discussunreaded", d2);
                                g.d().a(cVar7.a(), cVar7.b());
                            }
                            for (c cVar8 : hashMap3.values()) {
                                cVar8.a("groupunreaded", f);
                                cVar8.a("userunreaded", e);
                                cVar8.a("gaunreaded", g);
                                cVar8.a("feedunreaded", j);
                                cVar8.a("totalunreaded", i);
                                cVar8.a("contactnoticeunreded", h);
                                cVar8.a("discussunreaded", d2);
                                g.d().a(cVar8.a(), cVar8.b());
                            }
                        } catch (Throwable th) {
                            c.a().setTransactionSuccessful();
                            c.a().endTransaction();
                            throw th;
                        }
                    }
                } else {
                    if (this.f2907a.f2905a.size() > 10) {
                        sendEmptyMessageDelayed(234, 2000);
                    } else {
                        sendEmptyMessageDelayed(234, 1000);
                    }
                }
            } finally {
                this.f2907a.b.unlock();
            }
        }
    }
}
