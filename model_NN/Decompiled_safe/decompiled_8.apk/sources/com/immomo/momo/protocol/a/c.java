package com.immomo.momo.protocol.a;

import android.support.v4.b.a;
import com.immomo.a.a.f.b;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.ba;
import com.immomo.momo.service.bean.d;
import com.sina.sdk.api.message.InviteApi;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import org.json.JSONArray;
import org.json.JSONObject;

public final class c extends p {
    private static c f = null;
    private static Set g = new HashSet();

    public static c a() {
        if (f == null) {
            f = new c();
        }
        return f;
    }

    public static void b() {
        g.clear();
    }

    public final List a(int i, AtomicInteger atomicInteger, AtomicBoolean atomicBoolean, Date date) {
        boolean z = true;
        HashMap hashMap = new HashMap();
        hashMap.put("pos", new StringBuilder(String.valueOf(i)).toString());
        hashMap.put("version", new StringBuilder().append(atomicInteger).toString());
        hashMap.put("screen", String.valueOf(g.J()) + "x" + g.L());
        hashMap.put("sn", g.I());
        this.e.a(hashMap);
        JSONObject jSONObject = new JSONObject(a(String.valueOf(b) + "/banners/v2", hashMap));
        int optInt = jSONObject.optInt("pos");
        boolean z2 = optInt == atomicInteger.get();
        atomicInteger.set(jSONObject.optInt("version"));
        if (jSONObject.optInt("allow_close") != 1) {
            z = false;
        }
        atomicBoolean.set(z);
        date.setTime(jSONObject.optLong("sys_time") * 1000);
        if (z2 || !jSONObject.has("banners")) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        JSONArray optJSONArray = jSONObject.optJSONArray("banners");
        if (optJSONArray != null) {
            for (int i2 = 0; i2 < optJSONArray.length(); i2++) {
                JSONObject jSONObject2 = optJSONArray.getJSONObject(i2);
                d dVar = new d();
                dVar.h = optInt;
                dVar.f3022a = jSONObject2.optString("bannerid");
                if (!a.a((CharSequence) dVar.f3022a)) {
                    dVar.j = atomicBoolean.get();
                    dVar.b = jSONObject2.optInt("linktype");
                    dVar.c = jSONObject2.optInt("duration");
                    dVar.e = a(jSONObject2.optLong("start_time"));
                    dVar.f = a(jSONObject2.optLong("end_time"));
                    String[] a2 = a(jSONObject2.optJSONArray("pics"));
                    dVar.d = (a2 == null || a2.length <= 0) ? null : a2[0];
                    dVar.g = jSONObject2.optString(InviteApi.KEY_URL);
                    JSONObject optJSONObject = jSONObject2.optJSONObject("monitor");
                    if (optJSONObject != null) {
                        dVar.n = optJSONObject.optString("curl");
                        dVar.m = optJSONObject.optString("webview");
                    }
                    arrayList.add(dVar);
                }
            }
        }
        return arrayList;
    }

    public final List a(AtomicInteger atomicInteger) {
        JSONObject jSONObject = new JSONObject(a(String.valueOf(b) + "/welcomeconfig/android?version=" + atomicInteger.get(), (Map) null));
        atomicInteger.set(jSONObject.optInt("version", atomicInteger.get()));
        JSONArray optJSONArray = jSONObject.optJSONArray("list");
        if (optJSONArray == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < optJSONArray.length(); i++) {
            JSONObject jSONObject2 = optJSONArray.getJSONObject(i);
            ba baVar = new ba();
            baVar.a(jSONObject2.optString("bgurl"));
            baVar.b(jSONObject2.optString("fturl"));
            baVar.c(jSONObject2.optString("crurl"));
            baVar.b(a(jSONObject2.optLong("end_time")));
            baVar.a(a(jSONObject2.optLong("start_time")));
            baVar.b(jSONObject2.optInt("weight"));
            baVar.c(jSONObject2.optInt("duration"));
            baVar.d(jSONObject2.optString("monitor"));
            arrayList.add(baVar);
        }
        return arrayList;
    }

    public final void a(int i, String str, boolean z) {
        HashMap hashMap = new HashMap();
        hashMap.put("version", new StringBuilder(String.valueOf(i)).toString());
        hashMap.put("momoid", str);
        if (z) {
            hashMap.put("monitor", "1");
        } else {
            hashMap.put("monitor", "0");
        }
        b(String.valueOf(b) + "/banners?action=cover_showed", hashMap);
    }

    public final void a(int i, String[] strArr, int i2, long j, int i3, int i4) {
        HashMap hashMap = new HashMap();
        hashMap.put("pos", new StringBuilder(String.valueOf(i)).toString());
        hashMap.put("version", new StringBuilder(String.valueOf(i2)).toString());
        hashMap.put("duration", new StringBuilder(String.valueOf(j)).toString());
        hashMap.put("showed_count", new StringBuilder(String.valueOf(i3)).toString());
        hashMap.put("all_count", new StringBuilder(String.valueOf(i4)).toString());
        hashMap.put("bannerids", a.a(strArr, ","));
        this.e.a(hashMap);
        a(String.valueOf(b) + "/banners/close", hashMap);
    }

    public final void a(d dVar, int i) {
        HashMap hashMap = new HashMap();
        hashMap.put("pos", new StringBuilder(String.valueOf(dVar.h)).toString());
        hashMap.put("version", new StringBuilder(String.valueOf(i)).toString());
        hashMap.put("bannerid", dVar.f3022a);
        this.e.a(hashMap);
        a(String.valueOf(b) + "/banners/show", hashMap);
    }

    public final void a(File file, String str) {
        boolean z;
        BufferedReader bufferedReader;
        String str2 = null;
        int i = 0;
        HashMap hashMap = new HashMap();
        if (!a.a((CharSequence) str)) {
            hashMap.put("momoid", str);
            z = true;
        } else {
            z = false;
        }
        hashMap.put("version", "81");
        hashMap.put("time", new StringBuilder(String.valueOf(file.lastModified() / 1000)).toString());
        hashMap.put("client", "android");
        try {
            BufferedReader bufferedReader2 = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
            try {
                StringBuilder sb = new StringBuilder();
                while (true) {
                    String readLine = bufferedReader2.readLine();
                    if (readLine == null) {
                        break;
                    }
                    i++;
                    if (i > 2 && (!z || i > 3)) {
                        sb.append(readLine);
                    }
                    if (z && i == 4) {
                        str2 = readLine;
                    } else if (!z && i == 3) {
                        str2 = readLine;
                    }
                }
                String n = a.n(sb.toString());
                if (g.contains(n)) {
                    a.a(bufferedReader2);
                    return;
                }
                hashMap.put("sign", n);
                hashMap.put("title", str2);
                hashMap.put("file", sb.toString());
                a.a(bufferedReader2);
                g.add(n);
                a(String.valueOf(b) + "/log/crash", hashMap);
                a.a(bufferedReader2);
            } catch (Throwable th) {
                th = th;
                bufferedReader = bufferedReader2;
                a.a(bufferedReader);
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            bufferedReader = null;
            a.a(bufferedReader);
            throw th;
        }
    }

    public final void a(File file, String str, int i, long j) {
        BufferedInputStream bufferedInputStream;
        try {
            bufferedInputStream = new BufferedInputStream(new FileInputStream(file));
            try {
                String str2 = new String(a.b(bufferedInputStream));
                String n = a.n(str2);
                HashMap hashMap = new HashMap();
                hashMap.put("client", "android");
                hashMap.put("sign", n);
                hashMap.put("version", new StringBuilder(String.valueOf(i)).toString());
                hashMap.put("time", new StringBuilder(String.valueOf(j / 1000)).toString());
                hashMap.put("title", str2.substring(0, str2.indexOf(10)));
                if (!a.a((CharSequence) str)) {
                    hashMap.put("momoid", str);
                }
                hashMap.put("file", str2);
                if (g.contains(n)) {
                    a.a((Closeable) bufferedInputStream);
                    return;
                }
                a.a((Closeable) bufferedInputStream);
                a(String.valueOf(b) + "/log/crash", hashMap);
                g.add(n);
                a.a((Closeable) bufferedInputStream);
            } catch (Throwable th) {
                th = th;
                a.a((Closeable) bufferedInputStream);
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            bufferedInputStream = null;
            a.a((Closeable) bufferedInputStream);
            throw th;
        }
    }

    public final void a(String str) {
        HashMap hashMap = new HashMap();
        hashMap.put("uid", g.I());
        hashMap.put("log", str);
        HashMap hashMap2 = new HashMap();
        hashMap2.put("useip", "true");
        a(String.valueOf(b) + "/log/connect", hashMap, (l[]) null, hashMap2);
    }

    public final void b(d dVar, int i) {
        HashMap hashMap = new HashMap();
        hashMap.put("pos", new StringBuilder(String.valueOf(dVar.h)).toString());
        hashMap.put("version", new StringBuilder(String.valueOf(i)).toString());
        hashMap.put("id", dVar.f3022a);
        hashMap.put("linktype", new StringBuilder(String.valueOf(dVar.b)).toString());
        hashMap.put("momoid", g.q().h);
        this.e.a(hashMap);
        a(String.valueOf(b) + "/banners?action=access", hashMap);
    }

    public final void b(File file, String str) {
        HashMap hashMap = new HashMap();
        if (a.a((CharSequence) str)) {
            hashMap.put("log", "log1");
        } else {
            hashMap.put("log", "log2");
            hashMap.put("momoid", str);
        }
        hashMap.put("loctime", new StringBuilder(String.valueOf(System.currentTimeMillis() / 1000)).toString());
        this.e.a(hashMap);
        this.e.a((Object) ("file=" + file.getPath() + ",file.length()=" + file.length()));
        a(String.valueOf(b) + "/statis/upload", hashMap, new l[]{new l(file.getName(), file, "logfile", (byte) 0)});
    }

    public final void b(String str) {
        HashMap hashMap = new HashMap();
        hashMap.put("momoid", g.q().h);
        hashMap.put("client", "android");
        hashMap.put("uuid", b.a(8));
        this.e.a((Object) ("map=" + hashMap + ", log=" + str));
        l lVar = new l("file", str.getBytes(), "fileUpload", (String) null);
        a(String.valueOf(b) + "/pipeline?action=postDatabase", hashMap, new l[]{lVar});
    }
}
