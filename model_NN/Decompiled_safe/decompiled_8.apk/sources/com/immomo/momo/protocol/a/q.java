package com.immomo.momo.protocol.a;

import com.immomo.momo.R;
import com.immomo.momo.a.a;
import com.immomo.momo.a.r;
import com.immomo.momo.g;
import com.immomo.momo.util.ar;
import com.immomo.momo.util.jni.Codec;
import com.immomo.momo.util.m;
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.Map;
import javax.net.ssl.HttpsURLConnection;

public final class q {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static m f2901a = new m("HttpRequester");
    private static Map b = new HashMap();
    private static Map c = new HashMap();

    static {
        b bVar = new b(Codec.wfergfe(0), Codec.wfergfe(1), Codec.xxxArray());
        bVar.f();
        bVar.a("MomoRootCA.der");
        b.put(Codec.wfergfe(0), bVar);
        b.put(Codec.iiou(0), new b(Codec.iiou(0), Codec.iiou(1)));
        b.put(Codec.loiwq(0), new b(Codec.loiwq(0), Codec.loiwq(1)));
        b.put(Codec.njaei(0), new b(Codec.njaei(0), Codec.njaei(1)));
        b.put(Codec.owqtwn(0), new b(Codec.owqtwn(0), Codec.owqtwn(1)));
        b.put(Codec.oqhyn(0), new b(Codec.oqhyn(0), Codec.oqhyn(1)));
        b.put(Codec.lwjey(0), new b(Codec.lwjey(0), Codec.lwjey(1)));
        HttpsURLConnection.setDefaultHostnameVerifier(new r((byte) 0));
    }

    /* JADX INFO: finally extract failed */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0048, code lost:
        if (android.support.v4.b.a.a((java.lang.CharSequence) r2) == false) goto L_0x004a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.net.HttpURLConnection a(java.lang.String r9, java.util.Map r10) {
        /*
            r3 = 0
            java.net.URL r4 = new java.net.URL
            r4.<init>(r9)
            java.lang.String r5 = r4.getHost()
            java.util.Map r0 = com.immomo.momo.protocol.a.q.b
            java.lang.Object r0 = r0.get(r5)
            com.immomo.momo.protocol.a.b r0 = (com.immomo.momo.protocol.a.b) r0
            if (r0 == 0) goto L_0x018c
            java.util.concurrent.atomic.AtomicInteger r6 = new java.util.concurrent.atomic.AtomicInteger
            r6.<init>()
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            if (r10 == 0) goto L_0x0188
            java.lang.String r1 = "useip"
            boolean r1 = r10.containsKey(r1)
            if (r1 == 0) goto L_0x0188
            java.lang.String r1 = "true"
            java.lang.String r2 = "useip"
            java.lang.Object r2 = r10.get(r2)
            boolean r1 = r1.equals(r2)
            if (r1 == 0) goto L_0x0184
            java.lang.String r1 = r0.c()
            java.lang.String r2 = r0.d()
            boolean r8 = android.support.v4.b.a.a(r1)
            if (r8 != 0) goto L_0x0184
            boolean r8 = android.support.v4.b.a.a(r2)
            if (r8 != 0) goto L_0x0184
        L_0x004a:
            java.lang.String r8 = "useip"
            r10.remove(r8)
        L_0x004f:
            if (r1 != 0) goto L_0x0063
            r0.a(r6, r7)
            java.lang.String r1 = r7.toString()
            int r6 = r6.get()
            r7 = 3
            if (r6 != r7) goto L_0x0063
            java.lang.String r2 = r0.d()
        L_0x0063:
            boolean r6 = android.support.v4.b.a.a(r1)
            if (r6 != 0) goto L_0x0180
            boolean r6 = r1.equals(r5)
            if (r6 != 0) goto L_0x0180
            java.lang.String r4 = r9.replace(r5, r1)
            com.immomo.momo.util.m r1 = com.immomo.momo.protocol.a.q.f2901a
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            java.lang.String r6 = "replace "
            r5.<init>(r6)
            java.lang.StringBuilder r5 = r5.append(r9)
            java.lang.String r6 = " -> "
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.StringBuilder r5 = r5.append(r4)
            java.lang.String r5 = r5.toString()
            r1.c(r5)
            java.net.URL r1 = new java.net.URL
            r1.<init>(r4)
            r9 = r4
            r4 = r2
        L_0x0098:
            com.immomo.momo.util.m r2 = com.immomo.momo.protocol.a.q.f2901a
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            java.lang.String r6 = "--> "
            r5.<init>(r6)
            java.lang.StringBuilder r5 = r5.append(r9)
            java.lang.String r5 = r5.toString()
            r2.a(r5)
            java.lang.String r2 = r1.getProtocol()
            java.lang.String r5 = "https"
            boolean r2 = r2.equals(r5)
            if (r2 == 0) goto L_0x014e
            java.net.URLConnection r1 = r1.openConnection()
            javax.net.ssl.HttpsURLConnection r1 = (javax.net.ssl.HttpsURLConnection) r1
            r2 = r1
        L_0x00bf:
            java.lang.String r1 = "User-Agent"
            java.lang.String r5 = com.immomo.momo.g.O()
            r2.setRequestProperty(r1, r5)
            if (r0 == 0) goto L_0x00d0
            boolean r1 = r0.e()
            if (r1 != 0) goto L_0x0157
        L_0x00d0:
            boolean r1 = android.support.v4.b.a.a(r4)
            if (r1 != 0) goto L_0x0146
            com.immomo.momo.util.m r1 = com.immomo.momo.protocol.a.q.f2901a
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            java.lang.String r6 = "caFilename="
            r5.<init>(r6)
            java.lang.StringBuilder r5 = r5.append(r4)
            java.lang.String r5 = r5.toString()
            r1.a(r5)
            java.lang.String r1 = "X.509"
            java.security.cert.CertificateFactory r1 = java.security.cert.CertificateFactory.getInstance(r1)
            java.io.BufferedInputStream r5 = new java.io.BufferedInputStream
            android.content.Context r6 = com.immomo.momo.g.c()
            android.content.res.AssetManager r6 = r6.getAssets()
            java.io.InputStream r4 = r6.open(r4)
            r5.<init>(r4)
            java.security.cert.Certificate r1 = r1.generateCertificate(r5)     // Catch:{ all -> 0x017b }
            com.immomo.momo.util.m r4 = com.immomo.momo.protocol.a.q.f2901a     // Catch:{ all -> 0x017b }
            java.lang.String r6 = "ca success"
            r4.c(r6)     // Catch:{ all -> 0x017b }
            r5.close()
            java.lang.String r4 = java.security.KeyStore.getDefaultType()
            java.security.KeyStore r4 = java.security.KeyStore.getInstance(r4)
            r4.load(r3, r3)
            java.lang.String r5 = "ca"
            r4.setCertificateEntry(r5, r1)
            java.lang.String r1 = javax.net.ssl.TrustManagerFactory.getDefaultAlgorithm()
            javax.net.ssl.TrustManagerFactory r1 = javax.net.ssl.TrustManagerFactory.getInstance(r1)
            r1.init(r4)
            java.lang.String r4 = "TLS"
            javax.net.ssl.SSLContext r4 = javax.net.ssl.SSLContext.getInstance(r4)
            javax.net.ssl.TrustManager[] r1 = r1.getTrustManagers()
            java.security.SecureRandom r5 = new java.security.SecureRandom
            r5.<init>()
            r4.init(r3, r1, r5)
            r1 = r2
            javax.net.ssl.HttpsURLConnection r1 = (javax.net.ssl.HttpsURLConnection) r1
            javax.net.ssl.SSLSocketFactory r3 = r4.getSocketFactory()
            r1.setSSLSocketFactory(r3)
        L_0x0146:
            if (r0 == 0) goto L_0x014d
            java.util.Map r1 = com.immomo.momo.protocol.a.q.c
            r1.put(r2, r0)
        L_0x014d:
            return r2
        L_0x014e:
            java.net.URLConnection r1 = r1.openConnection()
            java.net.HttpURLConnection r1 = (java.net.HttpURLConnection) r1
            r2 = r1
            goto L_0x00bf
        L_0x0157:
            java.lang.String r1 = com.immomo.momo.g.s()
            boolean r1 = android.support.v4.b.a.a(r1)
            if (r1 != 0) goto L_0x00d0
            java.lang.String r1 = com.immomo.momo.a.p
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            java.lang.String r6 = "SESSIONID="
            r5.<init>(r6)
            java.lang.String r6 = com.immomo.momo.g.s()
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.String r5 = r5.toString()
            r2.setRequestProperty(r1, r5)
            goto L_0x00d0
        L_0x017b:
            r0 = move-exception
            r5.close()
            throw r0
        L_0x0180:
            r1 = r4
            r4 = r2
            goto L_0x0098
        L_0x0184:
            r1 = r3
            r2 = r3
            goto L_0x004a
        L_0x0188:
            r1 = r3
            r2 = r3
            goto L_0x004f
        L_0x018c:
            r1 = r4
            r4 = r3
            goto L_0x0098
        */
        throw new UnsupportedOperationException("Method not decompiled: com.immomo.momo.protocol.a.q.a(java.lang.String, java.util.Map):java.net.HttpURLConnection");
    }

    public static void a(HttpURLConnection httpURLConnection) {
        b bVar;
        if (httpURLConnection != null && (bVar = (b) c.get(httpURLConnection)) != null) {
            bVar.a();
            c.remove(httpURLConnection);
        }
    }

    private static byte[] a(InputStream inputStream) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byte[] bArr = new byte[1024];
        while (true) {
            int read = inputStream.read(bArr);
            if (read == -1) {
                byteArrayOutputStream.close();
                inputStream.close();
                return byteArrayOutputStream.toByteArray();
            }
            byteArrayOutputStream.write(bArr, 0, read);
        }
    }

    public static byte[] a(String str, Map map, Map map2) {
        int i = 0;
        long currentTimeMillis = System.currentTimeMillis();
        while (true) {
            int i2 = i + 1;
            if (i >= 5) {
                throw new a(g.a((int) R.string.errormsg_server));
            }
            HashMap hashMap = new HashMap();
            if (map2 != null) {
                hashMap.putAll(map2);
            }
            HttpURLConnection httpURLConnection = null;
            try {
                httpURLConnection = b(str, map, hashMap);
                byte[] a2 = a(httpURLConnection.getInputStream());
                ar.a((long) a2.length);
                b(httpURLConnection);
                if (httpURLConnection != null) {
                    httpURLConnection.disconnect();
                }
                return a2;
            } catch (Exception e) {
                a(httpURLConnection);
                if (i2 >= 5 || System.currentTimeMillis() - currentTimeMillis > 20000) {
                    throw e;
                } else if (!g.y()) {
                    throw e;
                } else {
                    Thread.sleep(1000);
                    if (httpURLConnection != null) {
                        httpURLConnection.disconnect();
                        i = i2;
                    } else {
                        i = i2;
                    }
                }
            } catch (Throwable th) {
                if (httpURLConnection != null) {
                    httpURLConnection.disconnect();
                }
                throw th;
            }
        }
        throw e;
    }

    public static byte[] a(String str, Map map, l[] lVarArr, Map map2) {
        int i = 0;
        long currentTimeMillis = System.currentTimeMillis();
        while (true) {
            int i2 = i + 1;
            if (i >= 5) {
                throw new a(g.a((int) R.string.errormsg_server));
            }
            HttpURLConnection httpURLConnection = null;
            try {
                httpURLConnection = b(str, map, lVarArr, map2);
                if (httpURLConnection.getResponseCode() >= 200 && httpURLConnection.getResponseCode() < 300) {
                    byte[] a2 = a(new BufferedInputStream(httpURLConnection.getInputStream()));
                    ar.a((long) a2.length);
                    b(httpURLConnection);
                    if (httpURLConnection != null) {
                        httpURLConnection.disconnect();
                    }
                    return a2;
                } else if (httpURLConnection.getResponseCode() <= 0) {
                    throw new a(g.a((int) R.string.errormsg_server));
                } else {
                    throw new r("response code != 200", httpURLConnection.getResponseCode());
                }
            } catch (Exception e) {
                a(httpURLConnection);
                if (i2 >= 5 || System.currentTimeMillis() - currentTimeMillis > 20000) {
                    throw e;
                } else if (!g.y()) {
                    throw e;
                } else {
                    f2901a.c("retry----" + i2 + " -> " + str);
                    Thread.sleep(1000);
                    if (httpURLConnection != null) {
                        httpURLConnection.disconnect();
                        i = i2;
                    } else {
                        i = i2;
                    }
                }
            } catch (Throwable th) {
                if (httpURLConnection != null) {
                    httpURLConnection.disconnect();
                }
                throw th;
            }
        }
        throw e;
    }

    public static HttpURLConnection b(String str, Map map, Map map2) {
        if (map != null) {
            for (Map.Entry entry : map.entrySet()) {
                str = android.support.v4.b.a.a(str, (String) entry.getKey(), (String) entry.getValue());
            }
        }
        HttpURLConnection a2 = a(str, map2);
        a2.setConnectTimeout(5000);
        a2.setReadTimeout(15000);
        a2.setRequestMethod("GET");
        if (map2 != null) {
            for (Map.Entry entry2 : map2.entrySet()) {
                a2.setRequestProperty((String) entry2.getKey(), (String) entry2.getValue());
            }
        }
        return a2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:105:0x0314, code lost:
        r2 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:106:0x0315, code lost:
        r3 = r8;
        r4 = r9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:108:0x031d, code lost:
        r2 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:115:0x0326, code lost:
        r2 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:116:0x0327, code lost:
        r3 = r8;
        r4 = r6;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:108:0x031d A[ExcHandler: all (th java.lang.Throwable), Splitter:B:31:0x00c9] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.net.HttpURLConnection b(java.lang.String r19, java.util.Map r20, com.immomo.momo.protocol.a.l[] r21, java.util.Map r22) {
        /*
            boolean r2 = com.immomo.momo.g.y()
            if (r2 != 0) goto L_0x000d
            com.immomo.momo.a.w r2 = new com.immomo.momo.a.w
            r3 = 0
            r2.<init>(r3)
            throw r2
        L_0x000d:
            r6 = 0
            r2 = 0
            if (r21 == 0) goto L_0x0019
            r0 = r21
            int r5 = r0.length
            r4 = 0
        L_0x0017:
            if (r4 < r5) goto L_0x0106
        L_0x0019:
            r4 = r2
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            r9.<init>()
            if (r20 == 0) goto L_0x002f
            java.util.Set r2 = r20.entrySet()
            java.util.Iterator r8 = r2.iterator()
        L_0x0029:
            boolean r2 = r8.hasNext()
            if (r2 != 0) goto L_0x0184
        L_0x002f:
            java.lang.String r2 = r9.toString()
            byte[] r2 = r2.getBytes()
            int r2 = r2.length
            long r2 = (long) r2
            long r2 = r2 + r4
            r4 = 0
            int r4 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r4 <= 0) goto L_0x032f
            java.lang.String r4 = "-----------------------------7da2137580612--\r\n"
            byte[] r4 = r4.getBytes()
            int r4 = r4.length
            long r4 = (long) r4
            long r2 = r2 + r4
            r4 = r2
        L_0x004a:
            r0 = r19
            r1 = r22
            java.net.HttpURLConnection r11 = a(r0, r1)
            r2 = 1
            r11.setDoInput(r2)
            r2 = 1
            r11.setDoOutput(r2)
            r2 = 5000(0x1388, float:7.006E-42)
            r11.setConnectTimeout(r2)
            r2 = 15000(0x3a98, float:2.102E-41)
            r11.setReadTimeout(r2)
            java.lang.String r2 = "POST"
            r11.setRequestMethod(r2)
            java.lang.String r2 = "Connection"
            java.lang.String r3 = "Keep-Alive"
            r11.setRequestProperty(r2, r3)
            java.lang.String r2 = "Charset"
            java.lang.String r3 = "UTF-8"
            r11.setRequestProperty(r2, r3)
            java.lang.String r2 = "Content-Type"
            java.lang.String r3 = "multipart/form-data; boundary=---------------------------7da2137580612"
            r11.setRequestProperty(r2, r3)
            java.lang.String r2 = "Content-Length"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r8 = java.lang.String.valueOf(r4)
            r3.<init>(r8)
            java.lang.String r3 = r3.toString()
            r11.setRequestProperty(r2, r3)
            java.lang.String r2 = "Accept-Language"
            java.lang.String r3 = "zh-CN"
            r11.setRequestProperty(r2, r3)
            java.lang.String r2 = "Expect"
            java.lang.String r3 = "100-continue"
            r11.setRequestProperty(r2, r3)
            if (r22 == 0) goto L_0x00ae
            java.util.Set r2 = r22.entrySet()
            java.util.Iterator r8 = r2.iterator()
        L_0x00a8:
            boolean r2 = r8.hasNext()
            if (r2 != 0) goto L_0x01c7
        L_0x00ae:
            if (r20 == 0) goto L_0x00b6
            boolean r2 = r20.isEmpty()
            if (r2 == 0) goto L_0x00bd
        L_0x00b6:
            if (r21 == 0) goto L_0x0102
            r0 = r21
            int r2 = r0.length
            if (r2 <= 0) goto L_0x0102
        L_0x00bd:
            r3 = 0
            java.io.BufferedOutputStream r8 = new java.io.BufferedOutputStream     // Catch:{ Exception -> 0x0323, all -> 0x0320 }
            java.io.OutputStream r2 = r11.getOutputStream()     // Catch:{ Exception -> 0x0323, all -> 0x0320 }
            r8.<init>(r2)     // Catch:{ Exception -> 0x0323, all -> 0x0320 }
            if (r20 == 0) goto L_0x00e3
            boolean r2 = r20.isEmpty()     // Catch:{ Exception -> 0x0326, all -> 0x031d }
            if (r2 != 0) goto L_0x00e3
            java.lang.String r2 = r9.toString()     // Catch:{ Exception -> 0x0326, all -> 0x031d }
            byte[] r2 = r2.getBytes()     // Catch:{ Exception -> 0x0326, all -> 0x031d }
            r8.write(r2)     // Catch:{ Exception -> 0x0326, all -> 0x031d }
            r8.flush()     // Catch:{ Exception -> 0x0326, all -> 0x031d }
            r9 = 0
            int r2 = r2.length     // Catch:{ Exception -> 0x0326, all -> 0x031d }
            long r2 = (long) r2     // Catch:{ Exception -> 0x0326, all -> 0x031d }
            long r6 = r9 + r2
        L_0x00e3:
            if (r21 == 0) goto L_0x00f3
            r0 = r21
            int r2 = r0.length     // Catch:{ Exception -> 0x0326, all -> 0x031d }
            if (r2 <= 0) goto L_0x00f3
            r0 = r21
            int r12 = r0.length     // Catch:{ Exception -> 0x0326, all -> 0x031d }
            r2 = 0
        L_0x00ee:
            if (r2 < r12) goto L_0x01de
            r8.flush()     // Catch:{ Exception -> 0x0326, all -> 0x031d }
        L_0x00f3:
            java.lang.String r2 = "-----------------------------7da2137580612--\r\n"
            byte[] r2 = r2.getBytes()     // Catch:{ Exception -> 0x0326, all -> 0x031d }
            r8.write(r2)     // Catch:{ Exception -> 0x0326, all -> 0x031d }
            r8.flush()     // Catch:{ Exception -> 0x0326, all -> 0x031d }
            android.support.v4.b.a.a(r8)
        L_0x0102:
            com.immomo.momo.util.ar.a(r4)
            return r11
        L_0x0106:
            r8 = r21[r4]
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            r9.<init>()
            java.lang.String r10 = "--"
            r9.append(r10)
            java.lang.String r10 = "---------------------------7da2137580612"
            r9.append(r10)
            java.lang.String r10 = "\r\n"
            r9.append(r10)
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            java.lang.String r11 = "Content-Disposition: form-data;name=\""
            r10.<init>(r11)
            java.lang.String r11 = r8.e()
            java.lang.StringBuilder r10 = r10.append(r11)
            java.lang.String r11 = "\";filename=\""
            java.lang.StringBuilder r10 = r10.append(r11)
            java.lang.String r11 = r8.d()
            java.lang.StringBuilder r10 = r10.append(r11)
            java.lang.String r11 = "\"\r\n"
            java.lang.StringBuilder r10 = r10.append(r11)
            java.lang.String r10 = r10.toString()
            r9.append(r10)
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            java.lang.String r11 = "Content-Type: "
            r10.<init>(r11)
            java.lang.String r11 = r8.f()
            java.lang.StringBuilder r10 = r10.append(r11)
            java.lang.String r11 = "\r\n\r\n"
            java.lang.StringBuilder r10 = r10.append(r11)
            java.lang.String r10 = r10.toString()
            r9.append(r10)
            java.lang.String r10 = "\r\n"
            r9.append(r10)
            int r9 = r9.length()
            long r9 = (long) r9
            long r2 = r2 + r9
            java.io.File r9 = r8.a()
            if (r9 == 0) goto L_0x017c
            long r8 = r9.length()
            long r2 = r2 + r8
        L_0x0178:
            int r4 = r4 + 1
            goto L_0x0017
        L_0x017c:
            byte[] r8 = r8.c()
            int r8 = r8.length
            long r8 = (long) r8
            long r2 = r2 + r8
            goto L_0x0178
        L_0x0184:
            java.lang.Object r2 = r8.next()
            java.util.Map$Entry r2 = (java.util.Map.Entry) r2
            java.lang.String r3 = "--"
            r9.append(r3)
            java.lang.String r3 = "---------------------------7da2137580612"
            r9.append(r3)
            java.lang.String r3 = "\r\n"
            r9.append(r3)
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            java.lang.String r3 = "Content-Disposition: form-data; name=\""
            r10.<init>(r3)
            java.lang.Object r3 = r2.getKey()
            java.lang.String r3 = (java.lang.String) r3
            java.lang.StringBuilder r3 = r10.append(r3)
            java.lang.String r10 = "\"\r\n\r\n"
            java.lang.StringBuilder r3 = r3.append(r10)
            java.lang.String r3 = r3.toString()
            r9.append(r3)
            java.lang.Object r2 = r2.getValue()
            java.lang.String r2 = (java.lang.String) r2
            r9.append(r2)
            java.lang.String r2 = "\r\n"
            r9.append(r2)
            goto L_0x0029
        L_0x01c7:
            java.lang.Object r2 = r8.next()
            java.util.Map$Entry r2 = (java.util.Map.Entry) r2
            java.lang.Object r3 = r2.getKey()
            java.lang.String r3 = (java.lang.String) r3
            java.lang.Object r2 = r2.getValue()
            java.lang.String r2 = (java.lang.String) r2
            r11.setRequestProperty(r3, r2)
            goto L_0x00a8
        L_0x01de:
            r13 = r21[r2]     // Catch:{ Exception -> 0x0326, all -> 0x031d }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0326, all -> 0x031d }
            r3.<init>()     // Catch:{ Exception -> 0x0326, all -> 0x031d }
            java.lang.String r9 = "--"
            r3.append(r9)     // Catch:{ Exception -> 0x0326, all -> 0x031d }
            java.lang.String r9 = "---------------------------7da2137580612"
            r3.append(r9)     // Catch:{ Exception -> 0x0326, all -> 0x031d }
            java.lang.String r9 = "\r\n"
            r3.append(r9)     // Catch:{ Exception -> 0x0326, all -> 0x031d }
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0326, all -> 0x031d }
            java.lang.String r10 = "Content-Disposition: form-data;name=\""
            r9.<init>(r10)     // Catch:{ Exception -> 0x0326, all -> 0x031d }
            java.lang.String r10 = r13.e()     // Catch:{ Exception -> 0x0326, all -> 0x031d }
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ Exception -> 0x0326, all -> 0x031d }
            java.lang.String r10 = "\";filename=\""
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ Exception -> 0x0326, all -> 0x031d }
            java.lang.String r10 = r13.d()     // Catch:{ Exception -> 0x0326, all -> 0x031d }
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ Exception -> 0x0326, all -> 0x031d }
            java.lang.String r10 = "\"\r\n"
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ Exception -> 0x0326, all -> 0x031d }
            java.lang.String r9 = r9.toString()     // Catch:{ Exception -> 0x0326, all -> 0x031d }
            r3.append(r9)     // Catch:{ Exception -> 0x0326, all -> 0x031d }
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0326, all -> 0x031d }
            java.lang.String r10 = "Content-Type: "
            r9.<init>(r10)     // Catch:{ Exception -> 0x0326, all -> 0x031d }
            java.lang.String r10 = r13.f()     // Catch:{ Exception -> 0x0326, all -> 0x031d }
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ Exception -> 0x0326, all -> 0x031d }
            java.lang.String r10 = "\r\n\r\n"
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ Exception -> 0x0326, all -> 0x031d }
            java.lang.String r9 = r9.toString()     // Catch:{ Exception -> 0x0326, all -> 0x031d }
            r3.append(r9)     // Catch:{ Exception -> 0x0326, all -> 0x031d }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x0326, all -> 0x031d }
            byte[] r3 = r3.getBytes()     // Catch:{ Exception -> 0x0326, all -> 0x031d }
            r8.write(r3)     // Catch:{ Exception -> 0x0326, all -> 0x031d }
            int r3 = r3.length     // Catch:{ Exception -> 0x0326, all -> 0x031d }
            long r9 = (long) r3
            long r9 = r9 + r6
            r3 = 0
            r6 = 0
            java.io.InputStream r3 = r13.b()     // Catch:{ all -> 0x032a }
            if (r3 == 0) goto L_0x0298
            r14 = 2048(0x800, float:2.87E-42)
            byte[] r14 = new byte[r14]     // Catch:{ all -> 0x02a6 }
        L_0x0255:
            int r15 = r3.read(r14)     // Catch:{ all -> 0x02a6 }
            r16 = -1
            r0 = r16
            if (r15 != r0) goto L_0x028e
        L_0x025f:
            java.lang.String r14 = "\r\n"
            byte[] r14 = r14.getBytes()     // Catch:{ all -> 0x02a6 }
            r8.write(r14)     // Catch:{ all -> 0x02a6 }
            java.lang.StringBuilder r14 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0314, all -> 0x031d }
            java.lang.String r13 = r13.d()     // Catch:{ Exception -> 0x0314, all -> 0x031d }
            java.lang.String r13 = java.lang.String.valueOf(r13)     // Catch:{ Exception -> 0x0314, all -> 0x031d }
            r14.<init>(r13)     // Catch:{ Exception -> 0x0314, all -> 0x031d }
            java.lang.String r13 = r14.toString()     // Catch:{ Exception -> 0x0314, all -> 0x031d }
            java.lang.String r14 = ".amr"
            boolean r13 = r13.endsWith(r14)     // Catch:{ Exception -> 0x0314, all -> 0x031d }
            if (r13 == 0) goto L_0x0318
            com.immomo.momo.util.ar.f(r6)     // Catch:{ Exception -> 0x0314, all -> 0x031d }
        L_0x0284:
            if (r3 == 0) goto L_0x0289
            android.support.v4.b.a.a(r3)     // Catch:{ Exception -> 0x0314, all -> 0x031d }
        L_0x0289:
            long r6 = r6 + r9
            int r2 = r2 + 1
            goto L_0x00ee
        L_0x028e:
            r16 = 0
            r0 = r16
            r8.write(r14, r0, r15)     // Catch:{ all -> 0x02a6 }
            long r15 = (long) r15     // Catch:{ all -> 0x02a6 }
            long r6 = r6 + r15
            goto L_0x0255
        L_0x0298:
            byte[] r14 = r13.c()     // Catch:{ all -> 0x02a6 }
            if (r14 != 0) goto L_0x02f0
            java.lang.IllegalArgumentException r2 = new java.lang.IllegalArgumentException     // Catch:{ all -> 0x02a6 }
            java.lang.String r4 = "upload file is null"
            r2.<init>(r4)     // Catch:{ all -> 0x02a6 }
            throw r2     // Catch:{ all -> 0x02a6 }
        L_0x02a6:
            r2 = move-exception
            r5 = r3
            r3 = r6
        L_0x02a9:
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0314, all -> 0x031d }
            java.lang.String r7 = r13.d()     // Catch:{ Exception -> 0x0314, all -> 0x031d }
            java.lang.String r7 = java.lang.String.valueOf(r7)     // Catch:{ Exception -> 0x0314, all -> 0x031d }
            r6.<init>(r7)     // Catch:{ Exception -> 0x0314, all -> 0x031d }
            java.lang.String r6 = r6.toString()     // Catch:{ Exception -> 0x0314, all -> 0x031d }
            java.lang.String r7 = ".amr"
            boolean r6 = r6.endsWith(r7)     // Catch:{ Exception -> 0x0314, all -> 0x031d }
            if (r6 == 0) goto L_0x0310
            com.immomo.momo.util.ar.f(r3)     // Catch:{ Exception -> 0x0314, all -> 0x031d }
        L_0x02c5:
            if (r5 == 0) goto L_0x02ca
            android.support.v4.b.a.a(r5)     // Catch:{ Exception -> 0x0314, all -> 0x031d }
        L_0x02ca:
            long r3 = r3 + r9
            throw r2     // Catch:{ Exception -> 0x02cc, all -> 0x031d }
        L_0x02cc:
            r2 = move-exception
            r17 = r3
            r4 = r17
            r3 = r8
        L_0x02d2:
            a(r11)     // Catch:{ all -> 0x02ea }
            com.immomo.momo.util.ar.a(r4)     // Catch:{ all -> 0x02ea }
            java.lang.String r4 = r2.getMessage()     // Catch:{ all -> 0x02ea }
            java.lang.String r5 = "was not verified"
            boolean r4 = r4.contains(r5)     // Catch:{ all -> 0x02ea }
            if (r4 == 0) goto L_0x031f
            javax.net.ssl.SSLException r4 = new javax.net.ssl.SSLException     // Catch:{ all -> 0x02ea }
            r4.<init>(r2)     // Catch:{ all -> 0x02ea }
            throw r4     // Catch:{ all -> 0x02ea }
        L_0x02ea:
            r2 = move-exception
            r8 = r3
        L_0x02ec:
            android.support.v4.b.a.a(r8)
            throw r2
        L_0x02f0:
            byte[] r14 = r13.c()     // Catch:{ all -> 0x02a6 }
            r15 = 0
            byte[] r16 = r13.c()     // Catch:{ all -> 0x02a6 }
            r0 = r16
            int r0 = r0.length     // Catch:{ all -> 0x02a6 }
            r16 = r0
            r0 = r16
            r8.write(r14, r15, r0)     // Catch:{ all -> 0x02a6 }
            r14 = 0
            byte[] r16 = r13.c()     // Catch:{ all -> 0x02a6 }
            r0 = r16
            int r6 = r0.length     // Catch:{ all -> 0x02a6 }
            long r6 = (long) r6
            long r6 = r6 + r14
            goto L_0x025f
        L_0x0310:
            com.immomo.momo.util.ar.e(r3)     // Catch:{ Exception -> 0x0314, all -> 0x031d }
            goto L_0x02c5
        L_0x0314:
            r2 = move-exception
            r3 = r8
            r4 = r9
            goto L_0x02d2
        L_0x0318:
            com.immomo.momo.util.ar.e(r6)     // Catch:{ Exception -> 0x0314, all -> 0x031d }
            goto L_0x0284
        L_0x031d:
            r2 = move-exception
            goto L_0x02ec
        L_0x031f:
            throw r2     // Catch:{ all -> 0x02ea }
        L_0x0320:
            r2 = move-exception
            r8 = r3
            goto L_0x02ec
        L_0x0323:
            r2 = move-exception
            r4 = r6
            goto L_0x02d2
        L_0x0326:
            r2 = move-exception
            r3 = r8
            r4 = r6
            goto L_0x02d2
        L_0x032a:
            r2 = move-exception
            r5 = r3
            r3 = r6
            goto L_0x02a9
        L_0x032f:
            r4 = r2
            goto L_0x004a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.immomo.momo.protocol.a.q.b(java.lang.String, java.util.Map, com.immomo.momo.protocol.a.l[], java.util.Map):java.net.HttpURLConnection");
    }

    public static void b(HttpURLConnection httpURLConnection) {
        b bVar;
        if (httpURLConnection != null && (bVar = (b) c.get(httpURLConnection)) != null) {
            bVar.b();
            c.remove(httpURLConnection);
        }
    }
}
