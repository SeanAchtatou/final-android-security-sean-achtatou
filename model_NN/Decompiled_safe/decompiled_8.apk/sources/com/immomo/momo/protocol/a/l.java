package com.immomo.momo.protocol.a;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

public final class l {

    /* renamed from: a  reason: collision with root package name */
    private byte[] f2899a;
    private File b;
    private String c;
    private String d;
    private String e;

    private l(String str, File file, String str2) {
        String str3;
        this.e = "application/octet-stream";
        this.c = str;
        this.d = str2;
        this.b = file;
        if (file == null || file.getName().indexOf(46) < 0) {
            str3 = null;
        } else {
            String substring = file.getName().substring(file.getName().lastIndexOf(46));
            str3 = ".html".equalsIgnoreCase(substring) ? "text/html" : ".jpg".equalsIgnoreCase(substring) ? "image/jpeg" : ".jpeg".equalsIgnoreCase(substring) ? "image/jpeg" : ".mp3".equalsIgnoreCase(substring) ? "audio/mpeg" : ".mp4".equalsIgnoreCase(substring) ? "video/mp4" : ".gif".equalsIgnoreCase(substring) ? "image/gif" : ".txt".equalsIgnoreCase(substring) ? "text/plain" : ".png".equalsIgnoreCase(substring) ? "image/png" : "application/octet-stream";
        }
        this.e = str3;
    }

    public l(String str, File file, String str2, byte b2) {
        this(str, file, str2);
    }

    public l(String str, byte[] bArr, String str2, String str3) {
        this.e = "application/octet-stream";
        this.f2899a = bArr;
        this.c = str;
        this.d = str2;
        if (str3 != null) {
            this.e = str3;
        }
    }

    public final File a() {
        return this.b;
    }

    public final InputStream b() {
        if (this.b != null) {
            return new BufferedInputStream(new FileInputStream(this.b), 4096);
        }
        return null;
    }

    public final byte[] c() {
        return this.f2899a;
    }

    public final String d() {
        return this.c;
    }

    public final String e() {
        return this.d;
    }

    public final String f() {
        return this.e;
    }

    public final String toString() {
        return "FormFile [data=" + this.f2899a + ", file=" + this.b + ", filname=" + this.c + ", parameterName=" + this.d + ", contentType=" + this.e + "]";
    }
}
