package com.immomo.momo.protocol.imjson.task;

import android.os.Bundle;
import com.immomo.a.a.d.i;
import com.immomo.momo.g;
import com.immomo.momo.protocol.imjson.p;
import com.immomo.momo.service.af;
import com.immomo.momo.service.bean.Message;

public abstract class MessageTask extends SendTask {

    /* renamed from: a  reason: collision with root package name */
    private Message f2936a = null;
    private long b = 0;
    private boolean e = false;
    private int f;

    public MessageTask(g gVar, Message message) {
        super(gVar);
        this.f2936a = message;
    }

    /* access modifiers changed from: protected */
    public abstract void a(Message message, i iVar);

    /* JADX WARNING: Removed duplicated region for block: B:38:0x00c8 A[Catch:{ Exception -> 0x005a }] */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0030 A[Catch:{ Exception -> 0x005a }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean a(com.immomo.a.a.a r9) {
        /*
            r8 = this;
            r7 = 4
            r0 = 0
            r6 = 3
            r5 = 2
            r1 = 1
            com.immomo.momo.protocol.imjson.b.a r2 = new com.immomo.momo.protocol.imjson.b.a     // Catch:{ Exception -> 0x005a }
            com.immomo.momo.service.bean.Message r3 = r8.f2936a     // Catch:{ Exception -> 0x005a }
            java.lang.String r3 = r3.msgId     // Catch:{ Exception -> 0x005a }
            r2.<init>(r9, r3)     // Catch:{ Exception -> 0x005a }
            com.immomo.momo.service.bean.Message r3 = r8.f2936a     // Catch:{ Exception -> 0x005a }
            int r3 = r3.chatType     // Catch:{ Exception -> 0x005a }
            if (r3 != r5) goto L_0x0034
            java.lang.String r3 = "gmsg"
            r2.a(r3)     // Catch:{ Exception -> 0x005a }
            com.immomo.momo.service.bean.Message r3 = r8.f2936a     // Catch:{ Exception -> 0x005a }
            java.lang.String r3 = r3.groupId     // Catch:{ Exception -> 0x005a }
            r2.d(r3)     // Catch:{ Exception -> 0x005a }
        L_0x0020:
            com.immomo.momo.service.bean.Message r3 = r8.f2936a     // Catch:{ Exception -> 0x005a }
            int r3 = r3.contentType     // Catch:{ Exception -> 0x005a }
            if (r3 != r7) goto L_0x0076
            r3 = 3
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ Exception -> 0x005a }
            r2.b(r3)     // Catch:{ Exception -> 0x005a }
        L_0x002e:
            if (r2 != 0) goto L_0x00c8
            r1 = 1
            r8.e = r1     // Catch:{ Exception -> 0x005a }
        L_0x0033:
            return r0
        L_0x0034:
            com.immomo.momo.service.bean.Message r3 = r8.f2936a     // Catch:{ Exception -> 0x005a }
            int r3 = r3.chatType     // Catch:{ Exception -> 0x005a }
            if (r3 != r1) goto L_0x0061
            java.lang.String r3 = "msg"
            r2.a(r3)     // Catch:{ Exception -> 0x005a }
            com.immomo.momo.service.bean.Message r3 = r8.f2936a     // Catch:{ Exception -> 0x005a }
            java.lang.String r3 = r3.remoteId     // Catch:{ Exception -> 0x005a }
            r2.d(r3)     // Catch:{ Exception -> 0x005a }
            com.immomo.momo.service.bean.Message r3 = r8.f2936a     // Catch:{ Exception -> 0x005a }
            java.lang.String r3 = r3.source     // Catch:{ Exception -> 0x005a }
            boolean r3 = com.immomo.a.a.f.a.a(r3)     // Catch:{ Exception -> 0x005a }
            if (r3 != 0) goto L_0x0020
            java.lang.String r3 = "nt"
            com.immomo.momo.service.bean.Message r4 = r8.f2936a     // Catch:{ Exception -> 0x005a }
            java.lang.String r4 = r4.source     // Catch:{ Exception -> 0x005a }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x005a }
            goto L_0x0020
        L_0x005a:
            r1 = move-exception
            com.immomo.momo.util.m r2 = r8.c
            r2.a(r1)
            goto L_0x0033
        L_0x0061:
            com.immomo.momo.service.bean.Message r3 = r8.f2936a     // Catch:{ Exception -> 0x005a }
            int r3 = r3.chatType     // Catch:{ Exception -> 0x005a }
            if (r3 != r6) goto L_0x0074
            java.lang.String r3 = "dmsg"
            r2.a(r3)     // Catch:{ Exception -> 0x005a }
            com.immomo.momo.service.bean.Message r3 = r8.f2936a     // Catch:{ Exception -> 0x005a }
            java.lang.String r3 = r3.discussId     // Catch:{ Exception -> 0x005a }
            r2.d(r3)     // Catch:{ Exception -> 0x005a }
            goto L_0x0020
        L_0x0074:
            r2 = 0
            goto L_0x002e
        L_0x0076:
            com.immomo.momo.service.bean.Message r3 = r8.f2936a     // Catch:{ Exception -> 0x005a }
            int r3 = r3.contentType     // Catch:{ Exception -> 0x005a }
            if (r3 != r5) goto L_0x0085
            r3 = 4
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ Exception -> 0x005a }
            r2.b(r3)     // Catch:{ Exception -> 0x005a }
            goto L_0x002e
        L_0x0085:
            com.immomo.momo.service.bean.Message r3 = r8.f2936a     // Catch:{ Exception -> 0x005a }
            int r3 = r3.contentType     // Catch:{ Exception -> 0x005a }
            if (r3 != r1) goto L_0x0094
            r3 = 2
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ Exception -> 0x005a }
            r2.b(r3)     // Catch:{ Exception -> 0x005a }
            goto L_0x002e
        L_0x0094:
            com.immomo.momo.service.bean.Message r3 = r8.f2936a     // Catch:{ Exception -> 0x005a }
            int r3 = r3.contentType     // Catch:{ Exception -> 0x005a }
            r4 = 5
            if (r3 != r4) goto L_0x00aa
            java.lang.String r3 = "style"
            r4 = 2
            r2.put(r3, r4)     // Catch:{ Exception -> 0x005a }
        L_0x00a1:
            r3 = 1
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ Exception -> 0x005a }
            r2.b(r3)     // Catch:{ Exception -> 0x005a }
            goto L_0x002e
        L_0x00aa:
            com.immomo.momo.service.bean.Message r3 = r8.f2936a     // Catch:{ Exception -> 0x005a }
            int r3 = r3.contentType     // Catch:{ Exception -> 0x005a }
            if (r3 != r6) goto L_0x00b7
            java.lang.String r3 = "style"
            r4 = 1
            r2.put(r3, r4)     // Catch:{ Exception -> 0x005a }
            goto L_0x00a1
        L_0x00b7:
            com.immomo.momo.service.bean.Message r3 = r8.f2936a     // Catch:{ Exception -> 0x005a }
            int r3 = r3.contentType     // Catch:{ Exception -> 0x005a }
            r4 = 6
            if (r3 != r4) goto L_0x00a1
            r3 = 5
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ Exception -> 0x005a }
            r2.b(r3)     // Catch:{ Exception -> 0x005a }
            goto L_0x002e
        L_0x00c8:
            com.immomo.momo.service.bean.Message r3 = r8.f2936a     // Catch:{ Exception -> 0x005a }
            r8.a(r3, r2)     // Catch:{ Exception -> 0x005a }
            boolean r3 = r2.i()     // Catch:{ Exception -> 0x005a }
            if (r3 != 0) goto L_0x00eb
            com.immomo.momo.util.m r3 = r8.c     // Catch:{ Exception -> 0x005a }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x005a }
            java.lang.String r5 = "packet not found 'body' field. --> "
            r4.<init>(r5)     // Catch:{ Exception -> 0x005a }
            java.lang.String r5 = r2.c()     // Catch:{ Exception -> 0x005a }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x005a }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x005a }
            r3.c(r4)     // Catch:{ Exception -> 0x005a }
        L_0x00eb:
            long r3 = r8.b     // Catch:{ Exception -> 0x005a }
            r5 = 0
            int r3 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r3 != 0) goto L_0x00f9
            long r3 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x005a }
            r8.b = r3     // Catch:{ Exception -> 0x005a }
        L_0x00f9:
            r2.j()     // Catch:{ Exception -> 0x005a }
            r0 = r1
            goto L_0x0033
        */
        throw new UnsupportedOperationException("Method not decompiled: com.immomo.momo.protocol.imjson.task.MessageTask.a(com.immomo.a.a.a):boolean");
    }

    public final void c() {
        this.e = true;
    }

    public final void d() {
        this.e = true;
        h_();
    }

    /* access modifiers changed from: protected */
    public final void e() {
        this.f2936a.status = 1;
        Bundle bundle = new Bundle();
        bundle.putString("stype", "msgsending");
        bundle.putInt("chattype", this.f2936a.chatType);
        bundle.putString("remoteuserid", this.f2936a.remoteId);
        bundle.putString("msgid", this.f2936a.msgId);
        if (this.f2936a.chatType == 2) {
            bundle.putString("groupid", this.f2936a.groupId);
        } else if (this.f2936a.chatType == 3) {
            bundle.putString("discussid", this.f2936a.discussId);
        }
        g.d().a(bundle, "actions.message.status");
        af.c().a(this.f2936a.msgId, this.f2936a.status, this.f2936a.chatType);
    }

    public final Message f() {
        return this.f2936a;
    }

    public final void g_() {
        this.f2936a.status = 2;
        Bundle bundle = new Bundle();
        bundle.putString("stype", "msgsuccess");
        bundle.putInt("chattype", this.f2936a.chatType);
        bundle.putString("remoteuserid", this.f2936a.remoteId);
        bundle.putString("msgid", this.f2936a.msgId);
        if (this.f2936a.chatType == 2) {
            bundle.putString("groupid", this.f2936a.groupId);
        } else if (this.f2936a.chatType == 3) {
            bundle.putString("discussid", this.f2936a.discussId);
        }
        g.d().a(bundle, "actions.message.status");
        af.c().a(this.f2936a.msgId, this.f2936a.status, this.f2936a.chatType);
    }

    public final void h_() {
        if (!this.e && p.a()) {
            int i = this.f;
            this.f = i + 1;
            if (i < 20) {
                p.a(this);
                return;
            }
        }
        this.f2936a.status = 3;
        Bundle bundle = new Bundle();
        bundle.putString("stype", "msgfailed");
        bundle.putInt("chattype", this.f2936a.chatType);
        bundle.putString("remoteuserid", this.f2936a.remoteId);
        bundle.putString("msgid", this.f2936a.msgId);
        if (this.f2936a.chatType == 2) {
            bundle.putString("groupid", this.f2936a.groupId);
        } else if (this.f2936a.chatType == 3) {
            bundle.putString("discussid", this.f2936a.discussId);
        }
        g.d().a(bundle, "actions.message.status");
        af.c().a(this.f2936a.msgId, this.f2936a.status, this.f2936a.chatType);
    }
}
