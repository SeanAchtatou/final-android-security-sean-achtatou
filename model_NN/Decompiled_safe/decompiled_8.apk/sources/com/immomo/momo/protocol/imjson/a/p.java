package com.immomo.momo.protocol.imjson.a;

import android.content.Intent;
import com.immomo.a.a.d.b;
import com.immomo.a.a.f;
import com.immomo.momo.android.broadcast.j;
import com.immomo.momo.g;
import com.immomo.momo.service.aq;
import com.immomo.momo.service.bean.bf;

public final class p implements f {
    public final void a(Object obj, f fVar) {
    }

    public final boolean a(b bVar) {
        int i;
        Intent intent = new Intent();
        intent.putExtra("key_momoid", bVar.optString("remoteid"));
        aq aqVar = new aq();
        if ("follow".equals(bVar.opt("ns"))) {
            int optInt = bVar.optInt("newfollower", 0);
            i = bVar.optInt("followercount", 0);
            g.q().w = i;
            g.q().v = optInt;
            aqVar.a(i, optInt, g.q().h);
            if ("both".equals(bVar.optString("relation"))) {
                aqVar.a(bVar.optString("remoteid"), 3);
            } else {
                aqVar.a(bVar.optString("remoteid"), 2);
            }
            intent.setAction(j.d);
            intent.putExtra("newfollower", optInt);
        } else if (!"unfollow".equals(bVar.opt("ns"))) {
            return false;
        } else {
            bf q = g.q();
            if (q.w > 0) {
                q.w--;
            }
            aqVar.h(g.q().h);
            aqVar.a(bVar.optString("remoteid"), 4);
            i = q.w;
            intent.setAction(j.c);
        }
        intent.putExtra("followercount", i);
        g.c().sendBroadcast(intent);
        return true;
    }
}
