package com.immomo.momo.protocol.imjson.a;

import android.os.Bundle;
import com.immomo.a.a.d.b;
import com.immomo.a.a.f;
import com.immomo.momo.g;
import com.immomo.momo.plugin.e.e;
import com.immomo.momo.service.af;
import com.immomo.momo.service.bean.GameApp;
import com.immomo.momo.service.bean.i;
import com.immomo.momo.service.bean.j;
import com.immomo.momo.service.bean.k;
import com.immomo.momo.service.c;
import java.util.ArrayList;
import java.util.Date;
import org.json.JSONArray;
import org.json.JSONObject;

public final class a implements f {
    public final void a(Object obj, f fVar) {
    }

    public final boolean a(b bVar) {
        String d = bVar.d();
        c cVar = new c();
        if (android.support.v4.b.a.a((CharSequence) d) || cVar.d(d)) {
            return false;
        }
        i iVar = new i();
        iVar.a(new Date());
        iVar.e(d);
        iVar.a(bVar.optInt("type"));
        if ("contacts-notice".equals(bVar.h())) {
            iVar.b(1);
        } else if ("wbcontacts-notice".equals(bVar.h())) {
            iVar.b(2);
        } else if ("qqwbcontacts-notice".equals(bVar.h())) {
            iVar.b(3);
        } else if ("rrcontacts-notice".equals(bVar.h())) {
            iVar.b(4);
        } else if ("momocontacts-notice".equals(bVar.h())) {
            iVar.b(5);
        } else if (!"appcontacts-notice".equals(bVar.h())) {
            return false;
        } else {
            iVar.b(6);
        }
        JSONObject optJSONObject = bVar.optJSONObject("contacts");
        if (optJSONObject != null) {
            iVar.a((float) optJSONObject.optInt("distance", -1));
            if (iVar.b() == 1 || iVar.b() == 5) {
                iVar.b(optJSONObject.optString("phone"));
                iVar.a(optJSONObject.optString("remoteid"));
            } else if (iVar.b() == 2) {
                iVar.a(optJSONObject.optString("remoteid"));
                e eVar = new e();
                iVar.a(eVar);
                eVar.f2875a = optJSONObject.optString("weibouid");
                eVar.c = optJSONObject.optString("weiboname");
                eVar.h = optJSONObject.optString("weibophotourl");
            } else if (iVar.b() == 3) {
                iVar.a(optJSONObject.optString("remoteid"));
                e eVar2 = new e();
                iVar.a(eVar2);
                eVar2.f2875a = optJSONObject.optString("qqwbuid");
                eVar2.c = optJSONObject.optString("qqwbname");
                eVar2.h = optJSONObject.optString("qqwbphotourl");
            } else if (iVar.b() == 4) {
                iVar.a(optJSONObject.optString("remoteid"));
                e eVar3 = new e();
                iVar.a(eVar3);
                eVar3.f2875a = optJSONObject.optString("renrenuid");
                eVar3.c = optJSONObject.optString("renrenname");
                eVar3.h = optJSONObject.optString("renrenphotourl");
            } else if (iVar.b() == 6) {
                iVar.a(optJSONObject.optString("remoteid"));
                GameApp gameApp = new GameApp();
                iVar.a(gameApp);
                gameApp.appid = optJSONObject.optString("appid");
                gameApp.appname = optJSONObject.optString("appname");
                gameApp.appicon = optJSONObject.optString("appphotourl");
                gameApp.action = optJSONObject.optString("action");
            }
        }
        String e = bVar.e();
        if (!android.support.v4.b.a.a((CharSequence) e)) {
            ArrayList arrayList = new ArrayList();
            StringBuilder sb = new StringBuilder(e);
            StringBuilder sb2 = new StringBuilder();
            k.a(sb, sb2, arrayList);
            iVar.d(sb2.toString());
            iVar.b(arrayList);
        }
        String optString = bVar.optString("actions");
        if (optString != null) {
            JSONArray jSONArray = new JSONArray(optString);
            ArrayList arrayList2 = new ArrayList();
            iVar.a(arrayList2);
            for (int i = 0; i < jSONArray.length(); i++) {
                j jVar = new j();
                jVar.a(jSONArray.getString(i));
                arrayList2.add(jVar);
            }
        }
        cVar.b(iVar);
        Bundle bundle = new Bundle();
        bundle.putString("msgid", iVar.o());
        af.c().a(bundle);
        g.d().a(bundle, "actions.contactnotice");
        return true;
    }
}
