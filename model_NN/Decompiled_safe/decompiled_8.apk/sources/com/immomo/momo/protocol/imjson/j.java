package com.immomo.momo.protocol.imjson;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.NetworkInfo;
import com.immomo.momo.android.c.ak;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.util.k;

final class j extends BroadcastReceiver {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ g f2924a;

    private j(g gVar) {
        this.f2924a = gVar;
    }

    /* synthetic */ j(g gVar, byte b) {
        this(gVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.protocol.imjson.g.a(com.immomo.momo.protocol.imjson.g, boolean):void
     arg types: [com.immomo.momo.protocol.imjson.g, int]
     candidates:
      com.immomo.momo.protocol.imjson.g.a(java.lang.String, java.lang.String):void
      com.immomo.momo.protocol.imjson.g.a(int, com.immomo.a.a.d.b):void
      com.immomo.momo.protocol.imjson.g.a(java.lang.Object, com.immomo.a.a.f):void
      com.immomo.a.a.c.a(int, com.immomo.a.a.d.b):void
      com.immomo.a.a.f.a(java.lang.Object, com.immomo.a.a.f):void
      com.immomo.momo.protocol.imjson.g.a(com.immomo.momo.protocol.imjson.g, boolean):void */
    public final void onReceive(Context context, Intent intent) {
        if (!this.f2924a.f2921a) {
            NetworkInfo networkInfo = (NetworkInfo) intent.getParcelableExtra("networkInfo");
            boolean z = networkInfo == null || networkInfo.getState() == NetworkInfo.State.DISCONNECTED;
            String typeName = networkInfo != null ? networkInfo.getTypeName() : "unkown";
            this.f2924a.e.a((Object) ("Network State Changed ### [" + typeName + "], state=" + networkInfo.getState()));
            if (!"mobile".equalsIgnoreCase(typeName) && !"WIFI".equalsIgnoreCase(typeName)) {
                return;
            }
            if (z) {
                this.f2924a.o = false;
                this.f2924a.b.removeMessages(148);
                this.f2924a.b.removeMessages(147);
                this.f2924a.b.sendEmptyMessageDelayed(148, 10000);
                this.f2924a.c.a(false);
                this.f2924a.l();
                this.f2924a.i = 0;
            } else if (networkInfo.getState() == NetworkInfo.State.CONNECTED) {
                this.f2924a.o = true;
                this.f2924a.b.removeMessages(148);
                if (this.f2924a.m) {
                    this.f2924a.a(2);
                    if (this.f2924a.l) {
                        this.f2924a.c.d().f();
                    }
                }
                this.f2924a.c.c();
                if ("WIFI".equalsIgnoreCase(typeName)) {
                    new k("U", "U22").e();
                } else {
                    new k("U", "U23").e();
                }
                bf q = g.q();
                if (q != null && !ak.f2376a && ak.b(q.h)) {
                    new ak(g.c(), q).execute(new Object[0]);
                }
            }
        }
    }
}
