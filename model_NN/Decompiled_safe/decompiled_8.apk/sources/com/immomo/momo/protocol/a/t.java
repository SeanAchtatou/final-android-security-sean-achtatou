package com.immomo.momo.protocol.a;

import java.util.HashMap;
import org.json.JSONObject;

public final class t extends p {
    private static final String f = (String.valueOf(b) + "/share");
    private static t g = null;

    public static t a() {
        if (g == null) {
            g = new t();
        }
        return g;
    }

    public final String a(String str, String str2, int i) {
        String str3 = String.valueOf(f) + "/friend";
        HashMap hashMap = new HashMap();
        hashMap.put("fromtype", new StringBuilder().append(i).toString());
        hashMap.put("fromid", str);
        hashMap.put("remoteid", str2);
        return new JSONObject(a(str3, hashMap)).optString("em");
    }

    public final String b(String str, String str2, int i) {
        String str3 = String.valueOf(f) + "/group";
        HashMap hashMap = new HashMap();
        hashMap.put("fromtype", new StringBuilder().append(i).toString());
        hashMap.put("fromid", str);
        hashMap.put("gid", str2);
        return new JSONObject(a(str3, hashMap)).optString("em");
    }

    public final String c(String str, String str2, int i) {
        String str3 = String.valueOf(f) + "/discuss";
        HashMap hashMap = new HashMap();
        hashMap.put("fromtype", new StringBuilder().append(i).toString());
        hashMap.put("fromid", str);
        hashMap.put("did", str2);
        return new JSONObject(a(str3, hashMap)).optString("em");
    }
}
