package com.immomo.momo.protocol.imjson.c;

import android.os.Bundle;
import java.util.HashMap;
import java.util.Map;

public final class e {

    /* renamed from: a  reason: collision with root package name */
    private Map f2916a = new HashMap();

    private f a(String str) {
        return (f) this.f2916a.get(str);
    }

    public final void a(a aVar) {
        if (aVar.a().size() == 0) {
            throw new IllegalArgumentException("actions.length==0");
        }
        for (String str : aVar.a()) {
            f a2 = a(str);
            if (a2 == null) {
                a2 = new f(str);
                this.f2916a.put(str, a2);
            }
            a2.add(aVar);
        }
    }

    public final boolean a(Bundle bundle, String str) {
        f a2 = a(str);
        if (a2 == null || a2.isEmpty()) {
            return false;
        }
        a2.a(bundle);
        return true;
    }

    public final boolean b(a aVar) {
        boolean z = false;
        for (String a2 : aVar.a()) {
            f a3 = a(a2);
            if (a3 != null && a3.remove(aVar)) {
                z = true;
            }
        }
        return z;
    }
}
