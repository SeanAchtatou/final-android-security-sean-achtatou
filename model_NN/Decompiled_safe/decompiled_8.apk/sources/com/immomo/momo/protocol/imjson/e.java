package com.immomo.momo.protocol.imjson;

import com.immomo.a.a.b.d;
import com.immomo.a.a.d.b;
import com.immomo.momo.protocol.imjson.task.SimpePacketTask;
import com.immomo.momo.protocol.imjson.task.g;
import com.immomo.momo.util.m;

public final class e {

    /* renamed from: a  reason: collision with root package name */
    private static e f2919a = null;

    private e() {
        new m(this);
    }

    public static b a(b bVar) {
        Object obj = new Object();
        SimpePacketTask simpePacketTask = new SimpePacketTask(g.AsyncExpress, bVar);
        simpePacketTask.a(new f(obj));
        p.a(simpePacketTask);
        synchronized (obj) {
            obj.wait(10000);
        }
        if (simpePacketTask.d() != null) {
            return simpePacketTask.d();
        }
        throw new d("send failed->" + bVar);
    }

    public static e a() {
        if (f2919a == null) {
            f2919a = new e();
        }
        return f2919a;
    }

    public static void b(b bVar) {
        p.a(new SimpePacketTask(g.AsyncExpress, bVar));
    }
}
