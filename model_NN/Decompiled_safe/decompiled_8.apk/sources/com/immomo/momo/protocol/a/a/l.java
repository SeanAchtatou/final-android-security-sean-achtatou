package com.immomo.momo.protocol.a.a;

import java.util.ArrayList;
import java.util.List;

public final class l {

    /* renamed from: a  reason: collision with root package name */
    private int f2891a = 0;
    private ArrayList b = new ArrayList();

    public final ArrayList a() {
        return this.b;
    }

    public final void a(int i) {
        this.f2891a = i;
    }

    public final void a(List list) {
        this.b.addAll(list);
    }

    public final boolean b() {
        return this.f2891a == 1;
    }
}
