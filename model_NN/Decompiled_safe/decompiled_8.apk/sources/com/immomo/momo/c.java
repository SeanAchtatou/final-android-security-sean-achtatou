package com.immomo.momo;

import android.support.v4.b.a;
import com.immomo.momo.android.c.w;
import com.immomo.momo.protocol.a.p;
import java.io.File;

final class c implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ String f2836a;

    c(String str) {
        this.f2836a = str;
    }

    public final void run() {
        try {
            String n = a.n(this.f2836a);
            File file = new File(a.z(), String.valueOf(n) + ".png_");
            File file2 = new File(a.z(), String.valueOf(n) + ".temp");
            file2.createNewFile();
            a.s.a((Object) ("save " + this.f2836a));
            p.a(this.f2836a, file2, (w) null);
            if (file.exists()) {
                file.delete();
            }
            file2.renameTo(file);
            b.a(n);
        } catch (Throwable th) {
        }
        b.f2835a.remove(this.f2836a);
    }
}
