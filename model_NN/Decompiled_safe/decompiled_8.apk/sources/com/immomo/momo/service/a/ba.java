package com.immomo.momo.service.a;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.b.a;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.GameApp;
import com.immomo.momo.service.bean.Message;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.util.d;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONObject;

public final class ba extends b {
    private static Set d = new HashSet();

    public ba(SQLiteDatabase sQLiteDatabase) {
        super(sQLiteDatabase, "users2", "u_momoid");
    }

    private static void a(bf bfVar, Cursor cursor) {
        boolean z = true;
        bfVar.h = cursor.getString(cursor.getColumnIndex("u_momoid"));
        if (!a.a((CharSequence) bfVar.h)) {
            d.add(bfVar.h);
        }
        try {
            bfVar.i = cursor.getString(cursor.getColumnIndex("u_name"));
        } catch (Exception e) {
        }
        try {
            bfVar.j = cursor.getString(cursor.getColumnIndex(Message.DBFIELD_SAYHI));
        } catch (Exception e2) {
        }
        try {
            bfVar.k = cursor.getString(cursor.getColumnIndex(Message.DBFIELD_LOCATIONJSON));
        } catch (Exception e3) {
        }
        try {
            bfVar.o = cursor.getString(cursor.getColumnIndex("field55"));
        } catch (Exception e4) {
        }
        try {
            bfVar.l = cursor.getString(cursor.getColumnIndex("field6"));
        } catch (Exception e5) {
        }
        try {
            bfVar.m = cursor.getString(cursor.getColumnIndex(Message.DBFIELD_GROUPID));
        } catch (Exception e6) {
        }
        try {
            bfVar.n = cursor.getString(cursor.getColumnIndex("field5"));
        } catch (Exception e7) {
        }
        try {
            bfVar.X = (long) cursor.getInt(cursor.getColumnIndex("u_version"));
        } catch (Exception e8) {
        }
        try {
            long j = cursor.getLong(cursor.getColumnIndex("u_distance"));
            if (j == 2147483647L) {
                j = -1;
            } else if (j == 2147483646) {
                j = -2;
            }
            bfVar.a((float) j);
        } catch (Exception e9) {
            bfVar.a(-1.0f);
        }
        try {
            long j2 = cursor.getLong(cursor.getColumnIndex("u_loctime"));
            if (j2 == 2147483647L) {
                j2 = 0;
            }
            bfVar.a(a(j2));
        } catch (Exception e10) {
            bfVar.a((Date) null);
        }
        try {
            bfVar.M = cursor.getString(cursor.getColumnIndex("field7"));
        } catch (Exception e11) {
        }
        try {
            bfVar.ax = a(cursor.getLong(cursor.getColumnIndex("u_updatetime")));
        } catch (Exception e12) {
        }
        try {
            if (cursor.getColumnIndex("f_followtime") >= 0) {
                bfVar.aD = a(cursor.getLong(cursor.getColumnIndex("f_followtime")));
            }
        } catch (Exception e13) {
        }
        try {
            if (a.a((CharSequence) bfVar.h) && cursor.getColumnIndex("n_momoid") >= 0) {
                bfVar.h = cursor.getString(cursor.getColumnIndex("n_momoid"));
            }
        } catch (Exception e14) {
        }
        try {
            if (a.a((CharSequence) bfVar.h) && cursor.getColumnIndex("f_momoid") >= 0) {
                bfVar.h = cursor.getString(cursor.getColumnIndex("f_momoid"));
            }
        } catch (Exception e15) {
        }
        try {
            if (a.a((CharSequence) bfVar.h) && cursor.getColumnIndex("bf_momoid") >= 0) {
                bfVar.h = cursor.getString(cursor.getColumnIndex("bf_momoid"));
            }
        } catch (Exception e16) {
        }
        try {
            if (cursor.getColumnIndex("b_blacktime") >= 0) {
                bfVar.Y = a(cursor.getLong(cursor.getColumnIndex("b_blacktime")));
            }
        } catch (Exception e17) {
        }
        try {
            if (a.a((CharSequence) bfVar.h) && cursor.getColumnIndex("b_momoid") >= 0) {
                bfVar.h = cursor.getString(cursor.getColumnIndex("b_momoid"));
            }
        } catch (Exception e18) {
        }
        try {
            bfVar.a(cursor.getString(cursor.getColumnIndex(Message.DBFIELD_CONVERLOCATIONJSON)));
        } catch (Exception e19) {
        }
        try {
            bfVar.G = cursor.getString(cursor.getColumnIndex("field8"));
        } catch (Exception e20) {
        }
        try {
            bfVar.aD = a(cursor.getLong(cursor.getColumnIndex("field17")));
        } catch (Exception e21) {
        }
        try {
            bfVar.aA = a(cursor.getLong(cursor.getColumnIndex("field18")));
        } catch (Exception e22) {
        }
        try {
            bfVar.aB = a(cursor.getLong(cursor.getColumnIndex("field19")));
        } catch (Exception e23) {
        }
        try {
            bfVar.aC = a(cursor.getLong(cursor.getColumnIndex("field20")));
        } catch (Exception e24) {
        }
        try {
            bfVar.H = cursor.getString(cursor.getColumnIndex("field9"));
        } catch (Exception e25) {
        }
        try {
            bfVar.I = cursor.getInt(cursor.getColumnIndex("field10"));
        } catch (Exception e26) {
        }
        try {
            bfVar.b(cursor.getLong(cursor.getColumnIndex("field58")));
        } catch (Exception e27) {
        }
        try {
            bfVar.aJ = cursor.getInt(cursor.getColumnIndex("field51"));
        } catch (Exception e28) {
        }
        bfVar.d(c(cursor, "field72"));
        try {
            int i = cursor.getInt(cursor.getColumnIndex("field50"));
            if (i == 0) {
                bfVar.B = 2;
            } else {
                bfVar.B = i;
            }
        } catch (Exception e29) {
        }
        try {
            bfVar.ay = a(cursor.getLong(cursor.getColumnIndex("field11")));
        } catch (Exception e30) {
        }
        try {
            bfVar.J = cursor.getString(cursor.getColumnIndex("field12"));
        } catch (Exception e31) {
        }
        try {
            bfVar.b(cursor.getString(cursor.getColumnIndex("field13")));
        } catch (Exception e32) {
        }
        try {
            bfVar.az = a(cursor.getLong(cursor.getColumnIndex("field14")));
        } catch (Exception e33) {
        }
        try {
            bfVar.ae = a.b(cursor.getString(cursor.getColumnIndex("field16")), ",");
        } catch (Exception e34) {
        }
        try {
            bfVar.P = cursor.getString(cursor.getColumnIndex("field23"));
        } catch (Exception e35) {
        }
        try {
            bfVar.p = cursor.getString(cursor.getColumnIndex("field56"));
        } catch (Exception e36) {
        }
        try {
            bfVar.r = cursor.getString(cursor.getColumnIndex("field67"));
        } catch (Exception e37) {
        }
        try {
            bfVar.q = cursor.getString(cursor.getColumnIndex("field66"));
        } catch (Exception e38) {
        }
        try {
            bfVar.K = cursor.getString(cursor.getColumnIndex("field25"));
        } catch (Exception e39) {
        }
        try {
            bfVar.L = cursor.getString(cursor.getColumnIndex("field26"));
        } catch (Exception e40) {
        }
        try {
            bfVar.N = cursor.getString(cursor.getColumnIndex("field27"));
        } catch (Exception e41) {
        }
        try {
            bfVar.D = cursor.getString(cursor.getColumnIndex("field28"));
        } catch (Exception e42) {
        }
        try {
            bfVar.X = (long) cursor.getInt(cursor.getColumnIndex("u_version"));
        } catch (Exception e43) {
        }
        try {
            bfVar.C = cursor.getString(cursor.getColumnIndex("field29"));
        } catch (Exception e44) {
        }
        try {
            bfVar.Q = cursor.getString(cursor.getColumnIndex("field30"));
        } catch (Exception e45) {
        }
        try {
            bfVar.E = cursor.getString(cursor.getColumnIndex("field31"));
        } catch (Exception e46) {
        }
        try {
            bfVar.F = cursor.getString(cursor.getColumnIndex("field32"));
        } catch (Exception e47) {
        }
        try {
            bfVar.v = cursor.getInt(cursor.getColumnIndex("field33"));
        } catch (Exception e48) {
        }
        try {
            bfVar.w = cursor.getInt(cursor.getColumnIndex("field34"));
        } catch (Exception e49) {
        }
        bfVar.u = a(cursor, "field77");
        try {
            bfVar.x = cursor.getInt(cursor.getColumnIndex("field35"));
        } catch (Exception e50) {
        }
        try {
            bfVar.y = cursor.getInt(cursor.getColumnIndex("field45"));
        } catch (Exception e51) {
        }
        try {
            bfVar.aG = cursor.getInt(cursor.getColumnIndex("field46"));
        } catch (Exception e52) {
        }
        try {
            bfVar.aH = cursor.getInt(cursor.getColumnIndex("field47"));
        } catch (Exception e53) {
        }
        try {
            bfVar.aI = cursor.getInt(cursor.getColumnIndex("field74"));
        } catch (Exception e54) {
        }
        try {
            bfVar.aw = cursor.getInt(cursor.getColumnIndex("field37")) == 1;
        } catch (Exception e55) {
        }
        try {
            bfVar.ai = cursor.getInt(cursor.getColumnIndex("field60"));
        } catch (Exception e56) {
        }
        try {
            bfVar.am = cursor.getString(cursor.getColumnIndex("field40"));
            bfVar.an = !a.a(bfVar.am);
            bfVar.ao = cursor.getString(cursor.getColumnIndex("field41"));
            bfVar.ap = !a.a(bfVar.ao);
        } catch (Exception e57) {
        }
        try {
            bfVar.as = cursor.getString(cursor.getColumnIndex("field64"));
            bfVar.at = !a.a(bfVar.as);
        } catch (Exception e58) {
        }
        try {
            bfVar.aq = cursor.getString(cursor.getColumnIndex("field38"));
            bfVar.ar = !a.a(bfVar.aq);
        } catch (Exception e59) {
        }
        try {
            bfVar.av = cursor.getString(cursor.getColumnIndex("field39"));
            a.a((CharSequence) bfVar.av);
        } catch (Exception e60) {
        }
        try {
            bfVar.ac = cursor.getInt(cursor.getColumnIndex("field43")) == 1;
        } catch (Exception e61) {
        }
        try {
            bfVar.au = cursor.getInt(cursor.getColumnIndex("field65")) == 1;
        } catch (Exception e62) {
        }
        try {
            bfVar.ad = cursor.getInt(cursor.getColumnIndex("field63")) == 1;
        } catch (Exception e63) {
        }
        try {
            bfVar.b = cursor.getString(cursor.getColumnIndex("field48"));
            bfVar.c = cursor.getString(cursor.getColumnIndex("field49"));
            if (a.f(bfVar.c) && !bfVar.c.startsWith("+")) {
                bfVar.c = "+" + bfVar.c;
            }
            if (a.a((CharSequence) bfVar.b) || a.a((CharSequence) bfVar.c)) {
                z = false;
            }
            bfVar.g = z;
        } catch (Exception e64) {
        }
        try {
            bfVar.d = cursor.getString(cursor.getColumnIndex("field54"));
        } catch (Exception e65) {
        }
        try {
            bfVar.ag = cursor.getString(cursor.getColumnIndex("field52"));
        } catch (Exception e66) {
        }
        try {
            bfVar.af = cursor.getString(cursor.getColumnIndex("field62"));
        } catch (Exception e67) {
        }
        try {
            bfVar.z = cursor.getInt(cursor.getColumnIndex("field53"));
        } catch (Exception e68) {
        }
        try {
            bfVar.A = cursor.getInt(cursor.getColumnIndex("field57"));
        } catch (Exception e69) {
        }
        try {
            bfVar.aj = a(cursor.getLong(cursor.getColumnIndex("field59")));
        } catch (Exception e70) {
        }
        try {
            bfVar.ax = a(cursor.getLong(cursor.getColumnIndex("u_updatetime")));
        } catch (Exception e71) {
        }
        try {
            bfVar.ak = cursor.getLong(cursor.getColumnIndex("field61"));
        } catch (Exception e72) {
        }
        bfVar.aF = a(b(cursor, "field71"));
        bfVar.c(c(cursor, "field69"));
        bfVar.R = c(cursor, "field70");
        if (a.a((CharSequence) bfVar.m())) {
            bfVar.c(bfVar.l());
            bfVar.R = null;
        }
        bfVar.a(e(cursor, "field75"));
        bfVar.al = a(cursor, "field76");
        String c = c(cursor, "field68");
        bfVar.aR = c(cursor, "field73");
        try {
            if (!a.a((CharSequence) c)) {
                bfVar.O = new ArrayList();
                JSONArray jSONArray = new JSONArray(c);
                for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                    bfVar.O.add(GameApp.initWithJson(jSONArray.getJSONObject(i2).toString()));
                }
            }
        } catch (Exception e73) {
        }
    }

    private void a(String str, bf bfVar, boolean z) {
        long longValue;
        String str2;
        String str3;
        Object[] objArr;
        int i = 1;
        float d2 = bfVar.d();
        if (d2 == -1.0f) {
            longValue = 2147483647L;
        } else if (d2 == -2.0f) {
            longValue = 2147483646;
        } else {
            new BigDecimal((double) d2).longValue();
            longValue = new BigDecimal((double) d2).longValue();
        }
        if (a.a((CharSequence) bfVar.P)) {
            bfVar.P = "none";
        }
        JSONArray jSONArray = new JSONArray();
        if (bfVar.O != null) {
            for (GameApp json : bfVar.O) {
                try {
                    jSONArray.put(new JSONObject(json.toJson()));
                } catch (Exception e) {
                }
            }
        }
        String b = d.b(bfVar.i);
        String c = d.c(bfVar.i);
        if (!a.a((CharSequence) bfVar.l)) {
            str2 = d.c(bfVar.l);
            str3 = d.b(bfVar.l);
        } else {
            str2 = c;
            str3 = b;
        }
        Object[] objArr2 = new Object[73];
        objArr2[0] = bfVar.i;
        objArr2[1] = b;
        objArr2[2] = c;
        objArr2[3] = Long.valueOf(bfVar.X);
        objArr2[4] = Long.valueOf(bfVar.a());
        objArr2[5] = Long.valueOf(longValue);
        objArr2[6] = bfVar.g();
        objArr2[7] = bfVar.l == null ? PoiTypeDef.All : bfVar.l;
        objArr2[8] = str2;
        objArr2[9] = str3;
        objArr2[10] = bfVar.M;
        objArr2[11] = bfVar.G;
        objArr2[12] = bfVar.H;
        objArr2[13] = Integer.valueOf(bfVar.I);
        objArr2[14] = Long.valueOf(bfVar.r());
        objArr2[15] = Integer.valueOf(bfVar.aJ);
        objArr2[16] = Integer.valueOf(bfVar.B);
        objArr2[17] = Long.valueOf(a(bfVar.ay));
        objArr2[18] = bfVar.J;
        objArr2[19] = bfVar.l();
        objArr2[20] = Long.valueOf(a(bfVar.az));
        objArr2[21] = a.a(bfVar.ae, ",");
        objArr2[22] = Long.valueOf(a(bfVar.aD));
        objArr2[23] = Long.valueOf(a(bfVar.aA));
        objArr2[24] = Long.valueOf(a(bfVar.aB));
        objArr2[25] = Long.valueOf(a(bfVar.aC));
        objArr2[26] = bfVar.P;
        objArr2[27] = bfVar.p;
        objArr2[28] = bfVar.r;
        objArr2[29] = bfVar.q;
        objArr2[30] = bfVar.K;
        objArr2[31] = bfVar.L;
        objArr2[32] = bfVar.N;
        objArr2[33] = bfVar.D;
        objArr2[34] = Long.valueOf(bfVar.X);
        objArr2[35] = bfVar.C;
        objArr2[36] = bfVar.Q;
        objArr2[37] = bfVar.E;
        objArr2[38] = bfVar.F;
        objArr2[39] = Integer.valueOf(bfVar.v);
        objArr2[40] = Integer.valueOf(bfVar.w);
        objArr2[41] = Integer.valueOf(bfVar.x);
        objArr2[42] = Integer.valueOf(bfVar.y);
        objArr2[43] = Integer.valueOf(bfVar.u);
        objArr2[44] = Integer.valueOf(bfVar.aG);
        objArr2[45] = Integer.valueOf(bfVar.aH);
        objArr2[46] = Integer.valueOf(bfVar.aI);
        objArr2[47] = Integer.valueOf(bfVar.aw ? 1 : 0);
        objArr2[48] = Integer.valueOf(bfVar.ai);
        objArr2[49] = bfVar.aq;
        objArr2[50] = bfVar.av;
        objArr2[51] = bfVar.am;
        objArr2[52] = bfVar.as;
        objArr2[53] = bfVar.ao;
        objArr2[54] = bfVar.ab;
        objArr2[55] = Integer.valueOf(bfVar.ac ? 1 : 0);
        objArr2[56] = Integer.valueOf(bfVar.au ? 1 : 0);
        objArr2[57] = Integer.valueOf(bfVar.ad ? 1 : 0);
        objArr2[58] = bfVar.d;
        objArr2[59] = bfVar.b;
        objArr2[60] = bfVar.c;
        objArr2[61] = bfVar.ag;
        objArr2[62] = bfVar.af;
        objArr2[63] = Long.valueOf(a(bfVar.aj));
        objArr2[64] = Long.valueOf(a(bfVar.ax));
        objArr2[65] = Long.valueOf(bfVar.ak);
        objArr2[66] = Integer.valueOf(bfVar.z);
        objArr2[67] = Integer.valueOf(bfVar.A);
        objArr2[68] = jSONArray.toString();
        objArr2[69] = bfVar.aK;
        if (!bfVar.b()) {
            i = 0;
        }
        objArr2[70] = Integer.valueOf(i);
        objArr2[71] = Integer.valueOf(bfVar.al);
        objArr2[72] = bfVar.h;
        if (z) {
            ArrayList arrayList = new ArrayList(Arrays.asList(objArr2));
            arrayList.add(Long.valueOf(a(new Date())));
            objArr = arrayList.toArray();
        } else {
            objArr = objArr2;
        }
        a(str, objArr);
    }

    private static bf b(Cursor cursor) {
        bf bfVar = new bf();
        a(bfVar, cursor);
        return bfVar;
    }

    public static void g() {
        Set set = d;
        String[] strArr = new String[set.size()];
        set.toArray(strArr);
        if (g.d().e() != null) {
            new ba(g.d().e()).a("field44", new Date(), "u_momoid", strArr);
            set.clear();
        }
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Cursor cursor) {
        return b(cursor);
    }

    public final void a(bf bfVar) {
        boolean z;
        long longValue;
        String str;
        String str2;
        Object[] objArr;
        int i = 1;
        StringBuilder sb = new StringBuilder();
        if (a.a((CharSequence) bfVar.P)) {
            bfVar.P = "none";
        }
        if (c(bfVar.h)) {
            sb.append("update " + this.f2954a + " set ").append("u_name=?, field1=?, field2=?, u_loctime=?,u_distance=?, field6=?, field5=?, field4=?, field7=?, field9=?, field10=?, field60=?, field51=?, field13=?, field27=?, field16=?, field23=?, field56=?, field67=?, field66=?, field25=?, field38=?, field39=?, field40=?, field64=?, field41=?, field42=?, field69=?, field70=?, field75=?, field76=?, field71=? ").append(" where u_momoid=?");
            z = false;
        } else {
            sb.append("insert into " + this.f2954a + " (").append("u_name, field1, field2, u_loctime,u_distance, field6, field5, field4, field7, field9, field10, field60, field51, field13, field27, field16, field23, field56, field67, field66, field25, field38, field39, field40, field64, field41, field42,field69,field70,field75,field76,field71,").append("u_momoid,field44) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            z = true;
        }
        float d2 = bfVar.d();
        if (d2 == -1.0f) {
            longValue = 2147483647L;
        } else if (d2 == -2.0f) {
            longValue = 2147483646;
        } else {
            new BigDecimal((double) d2).longValue();
            longValue = new BigDecimal((double) d2).longValue();
        }
        String b = d.b(bfVar.i);
        String c = d.c(bfVar.i);
        if (!a.a((CharSequence) bfVar.l)) {
            str = d.c(bfVar.l);
            str2 = d.b(bfVar.l);
        } else {
            str = c;
            str2 = b;
        }
        Object[] objArr2 = new Object[33];
        objArr2[0] = bfVar.i;
        objArr2[1] = b;
        objArr2[2] = c;
        objArr2[3] = Long.valueOf(bfVar.a());
        objArr2[4] = Long.valueOf(longValue);
        objArr2[5] = bfVar.l == null ? PoiTypeDef.All : bfVar.l;
        objArr2[6] = str;
        objArr2[7] = str2;
        objArr2[8] = bfVar.M;
        objArr2[9] = bfVar.H;
        objArr2[10] = Integer.valueOf(bfVar.I);
        objArr2[11] = Integer.valueOf(bfVar.ai);
        objArr2[12] = Integer.valueOf(bfVar.aJ);
        objArr2[13] = bfVar.l();
        objArr2[14] = bfVar.N;
        objArr2[15] = a.a(bfVar.ae, ",");
        objArr2[16] = bfVar.P;
        objArr2[17] = bfVar.p;
        objArr2[18] = bfVar.r;
        objArr2[19] = bfVar.q;
        objArr2[20] = bfVar.K;
        objArr2[21] = bfVar.aq;
        objArr2[22] = bfVar.av;
        objArr2[23] = bfVar.am;
        objArr2[24] = bfVar.as;
        objArr2[25] = bfVar.ao;
        objArr2[26] = bfVar.ab;
        objArr2[27] = bfVar.m();
        objArr2[28] = bfVar.R;
        if (!bfVar.b()) {
            i = 0;
        }
        objArr2[29] = Integer.valueOf(i);
        objArr2[30] = Integer.valueOf(bfVar.al);
        objArr2[31] = bfVar.aF;
        objArr2[32] = bfVar.h;
        if (z) {
            ArrayList arrayList = new ArrayList(Arrays.asList(objArr2));
            arrayList.add(Long.valueOf(a(new Date())));
            objArr = arrayList.toArray();
        } else {
            objArr = objArr2;
        }
        a(sb.toString(), objArr);
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void a(Object obj, Cursor cursor) {
        a((bf) obj, cursor);
    }

    public final bf b(String str) {
        bf bfVar;
        boolean z = false;
        StringBuilder sb = new StringBuilder();
        sb.append("select u_name,u_loctime,u_distance,field7,field6,field7,field9,field10,field51,field13,field27,field16,field23,field56,field67,field66").append(",field25,field38,field39,field40,field64,field69,field70,field41,field60,field73,field75,field76,field42").append(" from " + this.f2954a).append(" where u_momoid=?");
        Cursor a2 = a(sb.toString(), new String[]{str});
        if (a2.moveToFirst()) {
            if (!a.a((CharSequence) str)) {
                d.add(str);
            }
            bfVar = new bf(str);
            bfVar.i = a2.getString(a2.getColumnIndex("u_name"));
            try {
                long j = a2.getLong(a2.getColumnIndex("u_distance"));
                if (j == 2147483647L) {
                    j = -1;
                } else if (j == 2147483646) {
                    j = -2;
                }
                bfVar.a((float) j);
            } catch (Exception e) {
                bfVar.a(-1.0f);
            }
            try {
                bfVar.a(a(a2.getLong(a2.getColumnIndex("u_loctime"))));
            } catch (Exception e2) {
                bfVar.a((Date) null);
            }
            try {
                bfVar.M = a2.getString(a2.getColumnIndex("field7"));
            } catch (Exception e3) {
            }
            try {
                bfVar.l = a2.getString(a2.getColumnIndex("field6"));
            } catch (Exception e4) {
            }
            bfVar.a(e(a2, "field75"));
            bfVar.al = a(a2, "field76");
            try {
                bfVar.l = a2.getString(a2.getColumnIndex("field6"));
            } catch (Exception e5) {
            }
            try {
                bfVar.M = a2.getString(a2.getColumnIndex("field7"));
            } catch (Exception e6) {
            }
            try {
                bfVar.H = a2.getString(a2.getColumnIndex("field9"));
            } catch (Exception e7) {
            }
            try {
                bfVar.I = a2.getInt(a2.getColumnIndex("field10"));
            } catch (Exception e8) {
            }
            try {
                bfVar.aJ = a2.getInt(a2.getColumnIndex("field51"));
            } catch (Exception e9) {
            }
            try {
                bfVar.b(a2.getString(a2.getColumnIndex("field13")));
            } catch (Exception e10) {
            }
            try {
                bfVar.N = a2.getString(a2.getColumnIndex("field27"));
            } catch (Exception e11) {
            }
            try {
                bfVar.ae = a.b(a2.getString(a2.getColumnIndex("field16")), ",");
            } catch (Exception e12) {
            }
            try {
                bfVar.P = a2.getString(a2.getColumnIndex("field23"));
            } catch (Exception e13) {
            }
            try {
                bfVar.p = a2.getString(a2.getColumnIndex("field56"));
            } catch (Exception e14) {
            }
            try {
                bfVar.r = a2.getString(a2.getColumnIndex("field67"));
            } catch (Exception e15) {
            }
            try {
                bfVar.q = a2.getString(a2.getColumnIndex("field66"));
            } catch (Exception e16) {
            }
            try {
                bfVar.K = a2.getString(a2.getColumnIndex("field25"));
            } catch (Exception e17) {
            }
            try {
                bfVar.aq = a2.getString(a2.getColumnIndex("field38"));
                bfVar.ar = !a.a(bfVar.aq);
            } catch (Exception e18) {
            }
            try {
                bfVar.av = a2.getString(a2.getColumnIndex("field39"));
                a.a((CharSequence) bfVar.av);
            } catch (Exception e19) {
            }
            try {
                bfVar.am = a2.getString(a2.getColumnIndex("field40"));
                bfVar.an = !a.a(bfVar.am);
            } catch (Exception e20) {
            }
            try {
                bfVar.as = a2.getString(a2.getColumnIndex("field64"));
                bfVar.at = !a.a(bfVar.as);
            } catch (Exception e21) {
            }
            try {
                bfVar.ao = a2.getString(a2.getColumnIndex("field41"));
                if (!a.a((CharSequence) bfVar.ao)) {
                    z = true;
                }
                bfVar.ap = z;
            } catch (Exception e22) {
            }
            try {
                bfVar.ab = a2.getString(a2.getColumnIndex("field42"));
            } catch (Exception e23) {
            }
            try {
                bfVar.ai = a2.getInt(a2.getColumnIndex("field60"));
            } catch (Exception e24) {
            }
            bfVar.a(e(a2, "field75"));
            bfVar.al = a(a2, "field76");
            bfVar.c(c(a2, "field69"));
            bfVar.R = c(a2, "field70");
            if (a.a((CharSequence) bfVar.m())) {
                bfVar.c(bfVar.l());
                bfVar.R = null;
            }
            bfVar.aR = c(a2, "field73");
        } else {
            bfVar = null;
        }
        a2.close();
        return bfVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.service.a.ba.a(java.lang.String, com.immomo.momo.service.bean.bf, boolean):void
     arg types: [java.lang.String, com.immomo.momo.service.bean.bf, int]
     candidates:
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object[], java.lang.String):java.util.List
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object, java.io.Serializable):void
      com.immomo.momo.service.a.b.a(java.util.Map, java.lang.String[], java.lang.Object[]):void
      com.immomo.momo.service.a.b.a(java.lang.String[], java.lang.Object[], java.io.Serializable):void
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.String[], java.lang.String[]):java.lang.String[]
      com.immomo.momo.service.a.ba.a(java.lang.String, com.immomo.momo.service.bean.bf, boolean):void */
    /* renamed from: b */
    public final void a(bf bfVar) {
        StringBuilder sb = new StringBuilder();
        sb.append("insert into " + this.f2954a + " (").append("u_name, field1, field2, u_version, u_loctime,u_distance, field3, field6, field5, field4, field7, field8, field9, field10, field58, field51, field50, field11, field12, field13, field14, field16, field17, field18, field19, field20, field23, field56, field67, field66, field25, field26, ").append("field27, field28, u_version, field29, field30, field31, field32, field33, field34, field35, field45, field77, field46, field47, field74, field37, field60, field38, field39, field40, field64, field41, field42, field43, field65, field63, field54, field48, field49, field52, field62, field59, ").append("u_updatetime, field61, field53, field57, field68, field72, field75, field76, u_momoid, field44) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
        a(sb.toString(), bfVar, true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.service.a.ba.a(java.lang.String, com.immomo.momo.service.bean.bf, boolean):void
     arg types: [java.lang.String, com.immomo.momo.service.bean.bf, int]
     candidates:
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object[], java.lang.String):java.util.List
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object, java.io.Serializable):void
      com.immomo.momo.service.a.b.a(java.util.Map, java.lang.String[], java.lang.Object[]):void
      com.immomo.momo.service.a.b.a(java.lang.String[], java.lang.Object[], java.io.Serializable):void
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.String[], java.lang.String[]):java.lang.String[]
      com.immomo.momo.service.a.ba.a(java.lang.String, com.immomo.momo.service.bean.bf, boolean):void */
    public final void c(bf bfVar) {
        StringBuilder sb = new StringBuilder();
        sb.append("update " + this.f2954a + " set ").append("u_name=?, field1=?, field2=?, u_version=?,u_loctime=?,u_distance=?, field3=?, field6=?, field5=?, field4=?, field7=?, field8=?, field9=?, field10=?, field58=?, field51=?, field50=?, field11=?, field12=?, field13=?, field14=?, field16=?, field17=?, field18=?, field19=?, field20=?, field23=?, field56=?, field67=?, field66=?, field25=?, field26=?, ").append("field27=?, field28=?, u_version=?, field29=?, field30=?, field31=?, field32=?, field33=?, field34=?, field35=?, field45=?, field77=?, field46=?, field47=?, field74=?, field37=?, field60=?, field38=?, field39=?, field40=?, field64=?, field41=?, field42=?, field43=?, field65=?, field63=?, field54=?, field48=?, field49=?, field52=?, field62=?, field59=?, ").append("u_updatetime=?, field61=?, field53=?, field57=?, field68=?, field72=?, field75=?, field76=?  where u_momoid=?");
        a(sb.toString(), bfVar, false);
    }
}
