package com.immomo.momo.service.a;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.b.a;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.service.bean.Message;
import com.immomo.momo.service.bean.a.b;
import com.immomo.momo.service.bean.a.c;
import com.immomo.momo.service.bean.a.d;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;

public final class v extends b {
    public v(SQLiteDatabase sQLiteDatabase) {
        super(sQLiteDatabase, "gasessions", "field5");
    }

    /* access modifiers changed from: private */
    public void a(b bVar, Cursor cursor) {
        boolean z = true;
        bVar.d(cursor.getString(cursor.getColumnIndex("field5")));
        bVar.a(a(cursor.getLong(cursor.getColumnIndex(Message.DBFIELD_LOCATIONJSON))));
        bVar.a(cursor.getString(cursor.getColumnIndex(Message.DBFIELD_SAYHI)));
        bVar.a(cursor.getInt(cursor.getColumnIndex("field6")) == 0);
        bVar.a(cursor.getInt(cursor.getColumnIndex(Message.DBFIELD_CONVERLOCATIONJSON)));
        bVar.b(cursor.getString(cursor.getColumnIndex("field7")));
        if (cursor.getInt(cursor.getColumnIndex("field9")) != 1) {
            z = false;
        }
        bVar.b(z);
        bVar.b(cursor.getInt(cursor.getColumnIndex(Message.DBFIELD_ID)));
        bVar.a(a.b(c(cursor, "field10"), ","));
        try {
            String string = cursor.getString(cursor.getColumnIndex("field8"));
            if (string != null && !a.a((CharSequence) string)) {
                JSONArray jSONArray = new JSONArray(string);
                ArrayList arrayList = new ArrayList();
                bVar.b(arrayList);
                for (int i = 0; i < jSONArray.length(); i++) {
                    c cVar = new c();
                    cVar.a(jSONArray.getString(i));
                    arrayList.add(cVar);
                }
            }
        } catch (JSONException e) {
            this.c.a((Throwable) e);
        }
        try {
            String string2 = cursor.getString(cursor.getColumnIndex(Message.DBFIELD_GROUPID));
            if (string2 != null && !a.a((CharSequence) string2)) {
                ArrayList arrayList2 = new ArrayList();
                StringBuilder sb = new StringBuilder(string2);
                StringBuilder sb2 = new StringBuilder();
                d.a(sb, sb2, arrayList2);
                bVar.c(sb2.toString());
                bVar.c(arrayList2);
            }
        } catch (Exception e2) {
            this.c.a((Throwable) e2);
        }
    }

    private void a(StringBuilder sb, b bVar) {
        String str;
        String str2;
        int i = 1;
        if (bVar.o()) {
            JSONArray jSONArray = new JSONArray();
            int i2 = 0;
            for (c cVar : bVar.n()) {
                int i3 = i2 + 1;
                try {
                    jSONArray.put(i2, cVar.toString());
                    i2 = i3;
                } catch (JSONException e) {
                    this.c.a((Throwable) e);
                    i2 = i3;
                }
            }
            str = jSONArray.toString();
        } else {
            str = PoiTypeDef.All;
        }
        this.c.a((Object) ("actions=" + str));
        String l = bVar.l();
        List q = bVar.q();
        if (q == null || q.isEmpty()) {
            str2 = l;
        } else {
            String[] strArr = new String[q.size()];
            for (int i4 = 0; i4 < q.size(); i4++) {
                strArr[i4] = ((d) q.get(i4)).toString();
            }
            str2 = String.format(l, strArr);
        }
        this.c.a((Object) ("save session, body=" + str2));
        String sb2 = sb.toString();
        Object[] objArr = new Object[10];
        objArr[0] = Long.valueOf(a(bVar.f()));
        objArr[1] = bVar.c();
        objArr[2] = str2;
        objArr[3] = Integer.valueOf(bVar.b() ? 0 : 1);
        objArr[4] = Integer.valueOf(bVar.a());
        objArr[5] = bVar.g();
        objArr[6] = str;
        if (!bVar.p()) {
            i = 0;
        }
        objArr[7] = Integer.valueOf(i);
        objArr[8] = a.a(bVar.i(), ",");
        objArr[9] = bVar.m();
        a(sb2, objArr);
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public b a(Cursor cursor) {
        b bVar = new b();
        a(bVar, cursor);
        return bVar;
    }

    public final void a(b bVar) {
        StringBuilder sb = new StringBuilder();
        sb.append("insert into " + this.f2954a + " (").append("field2, field1, field4, field6, field3, field7, field8, field9, field10, field5) values(?,?,?,?,?,?,?,?,?,?)");
        a(sb, bVar);
    }

    public final void b(b bVar) {
        StringBuilder sb = new StringBuilder();
        sb.append("update " + this.f2954a + " set ").append("field2=?, field1=?, field4=?, field6=?, field3=?, field7=?, field8=?, field9=?, field10=? where field5=?");
        a(sb, bVar);
    }
}
