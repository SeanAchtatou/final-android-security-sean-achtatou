package com.immomo.momo.service.a;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.b.a;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.service.bean.Message;
import com.immomo.momo.service.bean.ag;
import com.immomo.momo.service.bean.ah;
import com.immomo.momo.service.bean.ai;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;

public final class s extends b {
    public s(SQLiteDatabase sQLiteDatabase) {
        super(sQLiteDatabase, "frienddistancenotices", Message.DBFIELD_ID);
    }

    /* access modifiers changed from: private */
    public void a(ag agVar, Cursor cursor) {
        boolean z = true;
        agVar.d(cursor.getString(cursor.getColumnIndex(Message.DBFIELD_ID)));
        agVar.a(a(cursor.getLong(cursor.getColumnIndex(Message.DBFIELD_SAYHI))));
        agVar.a(cursor.getInt(cursor.getColumnIndex("field9")));
        agVar.a(cursor.getInt(cursor.getColumnIndex(Message.DBFIELD_CONVERLOCATIONJSON)) == 0);
        agVar.b(cursor.getString(cursor.getColumnIndex(Message.DBFIELD_GROUPID)));
        if (cursor.getInt(cursor.getColumnIndex("field6")) != 1) {
            z = false;
        }
        agVar.b(z);
        agVar.a(c(cursor, "field8"));
        try {
            String string = cursor.getString(cursor.getColumnIndex("field5"));
            if (string != null && !a.a((CharSequence) string)) {
                JSONArray jSONArray = new JSONArray(string);
                ArrayList arrayList = new ArrayList();
                agVar.a(arrayList);
                for (int i = 0; i < jSONArray.length(); i++) {
                    ah ahVar = new ah();
                    ahVar.a(jSONArray.getString(i));
                    arrayList.add(ahVar);
                }
            }
        } catch (JSONException e) {
            this.c.a((Throwable) e);
        }
        try {
            String string2 = cursor.getString(cursor.getColumnIndex(Message.DBFIELD_LOCATIONJSON));
            if (string2 != null && !a.a((CharSequence) string2)) {
                ArrayList arrayList2 = new ArrayList();
                StringBuilder sb = new StringBuilder(string2);
                StringBuilder sb2 = new StringBuilder();
                ai.a(sb, sb2, arrayList2);
                agVar.c(sb2.toString());
                agVar.b(arrayList2);
            }
        } catch (Exception e2) {
            this.c.a((Throwable) e2);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public ag a(Cursor cursor) {
        ag agVar = new ag();
        a(agVar, cursor);
        return agVar;
    }

    public final void a(ag agVar) {
        String str;
        boolean z = false;
        if (agVar.l()) {
            JSONArray jSONArray = new JSONArray();
            int i = 0;
            for (ah ahVar : agVar.k()) {
                int i2 = i + 1;
                try {
                    jSONArray.put(i, ahVar.toString());
                    i = i2;
                } catch (JSONException e) {
                    this.c.a((Throwable) e);
                    i = i2;
                }
            }
            str = jSONArray.toString();
        } else {
            str = PoiTypeDef.All;
        }
        String i3 = agVar.i();
        List m = agVar.m();
        if (m != null && !m.isEmpty()) {
            StringBuilder sb = new StringBuilder(i3);
            int i4 = 0;
            int i5 = 0;
            while (i4 < m.size()) {
                int indexOf = sb.indexOf("%s", i5);
                sb.replace(indexOf, indexOf + 2, ((ai) m.get(i4)).toString());
                i4++;
                i5 = indexOf + 2;
            }
            i3 = sb.toString();
        }
        HashMap hashMap = new HashMap();
        hashMap.put("field5", str);
        hashMap.put(Message.DBFIELD_LOCATIONJSON, i3);
        hashMap.put(Message.DBFIELD_SAYHI, agVar.b());
        hashMap.put(Message.DBFIELD_ID, agVar.j());
        hashMap.put("field9", Integer.valueOf(agVar.e()));
        if (!agVar.a()) {
            z = true;
        }
        hashMap.put(Message.DBFIELD_CONVERLOCATIONJSON, Boolean.valueOf(z));
        hashMap.put(Message.DBFIELD_GROUPID, agVar.f());
        hashMap.put("field6", Boolean.valueOf(agVar.d()));
        hashMap.put("field8", agVar.c());
        a((Map) hashMap);
    }

    public final void b(ag agVar) {
        String str;
        if (agVar.l()) {
            JSONArray jSONArray = new JSONArray();
            int i = 0;
            for (ah ahVar : agVar.k()) {
                int i2 = i + 1;
                try {
                    jSONArray.put(i, ahVar.toString());
                    i = i2;
                } catch (JSONException e) {
                    this.c.a((Throwable) e);
                    i = i2;
                }
            }
            str = jSONArray.toString();
        } else {
            str = PoiTypeDef.All;
        }
        String i3 = agVar.i();
        List m = agVar.m();
        if (m != null && !m.isEmpty()) {
            StringBuilder sb = new StringBuilder(i3);
            int i4 = 0;
            int i5 = 0;
            while (i4 < m.size()) {
                int indexOf = sb.indexOf("%s", i5);
                sb.replace(indexOf, indexOf + 2, ((ai) m.get(i4)).toString());
                i4++;
                i5 = indexOf + 2;
            }
            i3 = sb.toString();
        }
        HashMap hashMap = new HashMap();
        hashMap.put("field5", str);
        hashMap.put(Message.DBFIELD_LOCATIONJSON, i3);
        hashMap.put(Message.DBFIELD_SAYHI, agVar.b());
        hashMap.put("field9", Integer.valueOf(agVar.e()));
        hashMap.put(Message.DBFIELD_CONVERLOCATIONJSON, Boolean.valueOf(!agVar.a()));
        hashMap.put(Message.DBFIELD_GROUPID, agVar.f());
        hashMap.put("field6", Boolean.valueOf(agVar.d()));
        hashMap.put("field8", agVar.c());
        a(hashMap, new String[]{Message.DBFIELD_ID}, new Object[]{agVar.j()});
    }
}
