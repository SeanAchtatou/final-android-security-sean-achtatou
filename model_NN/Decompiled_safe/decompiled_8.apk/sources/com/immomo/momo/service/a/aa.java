package com.immomo.momo.service.a;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.immomo.momo.service.bean.Message;
import com.immomo.momo.service.bean.a.g;

public final class aa extends b {
    public aa(SQLiteDatabase sQLiteDatabase) {
        super(sQLiteDatabase, "group_activity2", "gp_id");
    }

    private static void a(g gVar, Cursor cursor) {
        boolean z = true;
        gVar.b = cursor.getString(cursor.getColumnIndex("gp_id"));
        gVar.f2976a = cursor.getString(cursor.getColumnIndex("field12"));
        if (cursor.getInt(cursor.getColumnIndex("field13")) != 1) {
            z = false;
        }
        gVar.m = z;
        gVar.c = cursor.getString(cursor.getColumnIndex(Message.DBFIELD_SAYHI));
        gVar.d = cursor.getString(cursor.getColumnIndex(Message.DBFIELD_LOCATIONJSON));
        gVar.e = cursor.getString(cursor.getColumnIndex(Message.DBFIELD_CONVERLOCATIONJSON));
        gVar.f = a(cursor.getLong(cursor.getColumnIndex(Message.DBFIELD_GROUPID)));
        gVar.g = a(cursor.getLong(cursor.getColumnIndex("field10")));
        gVar.h = cursor.getInt(cursor.getColumnIndex("field5"));
        gVar.i = cursor.getInt(cursor.getColumnIndex("field6"));
        gVar.n = cursor.getInt(cursor.getColumnIndex("field9"));
        gVar.j = cursor.getDouble(cursor.getColumnIndex("field7"));
        gVar.k = cursor.getDouble(cursor.getColumnIndex("field8"));
        gVar.l = cursor.getDouble(cursor.getColumnIndex("field11"));
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Cursor cursor) {
        g gVar = new g();
        a(gVar, cursor);
        return gVar;
    }

    public final void a(g gVar) {
        int i = 1;
        StringBuilder sb = new StringBuilder();
        sb.append("insert into " + this.f2954a + " (").append("gp_id, field12, field13, field1, field2, field3, field4, field10, field5, field7, field8, field11, field9, field6) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
        String sb2 = sb.toString();
        Object[] objArr = new Object[14];
        objArr[0] = gVar.b;
        objArr[1] = gVar.f2976a;
        if (!gVar.m) {
            i = 0;
        }
        objArr[2] = Integer.valueOf(i);
        objArr[3] = gVar.c;
        objArr[4] = gVar.d;
        objArr[5] = gVar.e;
        objArr[6] = Long.valueOf(a(gVar.f));
        objArr[7] = Long.valueOf(a(gVar.g));
        objArr[8] = Integer.valueOf(gVar.h);
        objArr[9] = Double.valueOf(gVar.j);
        objArr[10] = Double.valueOf(gVar.k);
        objArr[11] = Double.valueOf(gVar.l);
        objArr[12] = Integer.valueOf(gVar.n);
        objArr[13] = Integer.valueOf(gVar.i);
        a(sb2, objArr);
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void a(Object obj, Cursor cursor) {
        a((g) obj, cursor);
    }

    public final void b(g gVar) {
        int i = 1;
        StringBuilder sb = new StringBuilder();
        sb.append("update " + this.f2954a + " set ").append("field1=?, field12=?, field13=?, field2=?, field3=?, field4=?, field10=?, field5=?, field7=?, field8=?, field11=?, field9=?, field6=? where gp_id=?");
        String sb2 = sb.toString();
        Object[] objArr = new Object[14];
        objArr[0] = gVar.c;
        objArr[1] = gVar.f2976a;
        if (!gVar.m) {
            i = 0;
        }
        objArr[2] = Integer.valueOf(i);
        objArr[3] = gVar.d;
        objArr[4] = gVar.e;
        objArr[5] = Long.valueOf(a(gVar.f));
        objArr[6] = Long.valueOf(a(gVar.g));
        objArr[7] = Integer.valueOf(gVar.h);
        objArr[8] = Double.valueOf(gVar.j);
        objArr[9] = Double.valueOf(gVar.k);
        objArr[10] = Double.valueOf(gVar.l);
        objArr[11] = Integer.valueOf(gVar.n);
        objArr[12] = Integer.valueOf(gVar.i);
        objArr[13] = gVar.b;
        a(sb2, objArr);
    }
}
