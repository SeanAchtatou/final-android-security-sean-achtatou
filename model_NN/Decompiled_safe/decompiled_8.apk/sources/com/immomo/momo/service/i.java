package com.immomo.momo.service;

import com.immomo.momo.service.bean.p;
import java.util.Comparator;

final class i implements Comparator {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ h f3047a;
    private final /* synthetic */ int b;

    i(h hVar, int i) {
        this.f3047a = hVar;
        this.b = i;
    }

    public final /* synthetic */ int compare(Object obj, Object obj2) {
        float f = -1.0f;
        long j = 0;
        p pVar = (p) obj;
        p pVar2 = (p) obj2;
        if (this.b == 1) {
            long time = pVar.c != null ? pVar.c.getTime() : 0;
            if (pVar2.c != null) {
                j = pVar2.c.getTime();
            }
            if (time > j) {
                return -1;
            }
            if (time < j) {
                return 1;
            }
        } else if (this.b == 2) {
            float d = pVar.h != null ? pVar.h.d() : -1.0f;
            if (pVar2.h != null) {
                f = pVar2.h.d();
            }
            if (f < 0.0f) {
                f = 2.14748365E9f;
            }
            if (d < 0.0f) {
                d = 2.14748365E9f;
            }
            if (d < f) {
                return -1;
            }
            if (d > f) {
                return 1;
            }
        } else if (this.b == 3) {
            long time2 = (pVar.h == null || pVar.h.e() == null) ? 0 : pVar.h.e().getTime();
            if (!(pVar2.h == null || pVar2.h.e() == null)) {
                j = pVar2.h.e().getTime();
            }
            if (time2 > j) {
                return -1;
            }
            if (time2 < j) {
                return 1;
            }
        } else if (this.b == 4) {
            this.f3047a.b.a((Object) ("orderType: " + this.b));
            this.f3047a.b.a((Object) ("lhs.msgTime: " + pVar.e));
            this.f3047a.b.a((Object) ("rhs.msgTime" + pVar2.e));
            long time3 = pVar.e != null ? pVar.e.getTime() : 0;
            if (pVar2.e != null) {
                j = pVar2.e.getTime();
            }
            if (time3 > j) {
                return -1;
            }
            if (time3 < j) {
                return 1;
            }
        }
        return 0;
    }
}
