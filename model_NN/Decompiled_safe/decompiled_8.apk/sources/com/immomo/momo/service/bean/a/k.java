package com.immomo.momo.service.bean.a;

import com.amap.mapapi.poisearch.PoiTypeDef;
import java.util.List;

public final class k {

    /* renamed from: a  reason: collision with root package name */
    public List f2980a;
    private String b;
    private int c = 3;

    public k(int i) {
        this.c = i;
        switch (i) {
            case 1:
                this.b = "群主";
                return;
            case 2:
                this.b = "管理员";
                return;
            case 3:
                this.b = "成员";
                return;
            default:
                this.b = PoiTypeDef.All;
                return;
        }
    }

    public final String a() {
        return this.b;
    }

    public final int b() {
        return this.c;
    }
}
