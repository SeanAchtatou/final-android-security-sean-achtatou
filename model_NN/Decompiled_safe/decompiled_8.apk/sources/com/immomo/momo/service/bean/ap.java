package com.immomo.momo.service.bean;

import java.text.SimpleDateFormat;
import java.util.Date;

public final class ap {

    /* renamed from: a  reason: collision with root package name */
    public String f2993a = null;
    public String b = null;
    public int c = 0;
    public Date d = null;
    public int e = 0;

    public final String toString() {
        StringBuilder append = new StringBuilder("MessageCache [filePath=").append(this.f2993a).append(", remoteId=").append(this.b).append(", id=").append(this.c).append(", dateTime=");
        Date date = this.d;
        return append.append(date == null ? "UNKNOWN" : new SimpleDateFormat("yyyyMMddHHmmssSSS").format(date)).append(", count=").append(this.e).append("]").toString();
    }
}
