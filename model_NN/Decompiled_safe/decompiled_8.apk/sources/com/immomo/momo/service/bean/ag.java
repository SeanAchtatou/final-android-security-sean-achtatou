package com.immomo.momo.service.bean;

import android.support.v4.b.a;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.g;
import java.util.Date;
import java.util.List;

public final class ag {

    /* renamed from: a  reason: collision with root package name */
    private String f2987a = PoiTypeDef.All;
    private boolean b = true;
    private Date c;
    private String d;
    private String e;
    private bf f;
    private List g = null;
    private List h = null;
    private String i;
    private boolean j = false;
    private String k;
    private int l;
    private String m;

    public final void a(int i2) {
        this.l = i2;
        if (i2 == -2) {
            this.f2987a = g.a((int) R.string.profile_distance_hide);
        } else if (i2 >= 0) {
            this.f2987a = String.valueOf(a.a(((float) i2) / 1000.0f)) + "km";
        } else {
            this.f2987a = g.a((int) R.string.profile_distance_unknown);
        }
    }

    public final void a(bf bfVar) {
        this.f = bfVar;
    }

    public final void a(String str) {
        this.k = str;
    }

    public final void a(Date date) {
        this.c = date;
    }

    public final void a(List list) {
        this.g = list;
    }

    public final void a(boolean z) {
        this.b = z;
    }

    public final boolean a() {
        return this.b;
    }

    public final Date b() {
        return this.c;
    }

    public final void b(String str) {
        this.e = str;
    }

    public final void b(List list) {
        this.h = list;
    }

    public final void b(boolean z) {
        this.j = z;
    }

    public final String c() {
        return this.k;
    }

    public final void c(String str) {
        if (str == null) {
            str = PoiTypeDef.All;
        }
        this.i = str;
    }

    public final void d(String str) {
        this.d = str;
    }

    public final boolean d() {
        return this.j;
    }

    public final int e() {
        return this.l;
    }

    public final String f() {
        return this.e == null ? PoiTypeDef.All : this.e;
    }

    public final bf g() {
        return this.f;
    }

    public final String h() {
        int i2 = 0;
        if (this.m == null) {
            if (this.h == null || this.h.isEmpty()) {
                this.m = this.i;
            } else {
                StringBuilder sb = new StringBuilder(this.i);
                int i3 = 0;
                while (i3 < this.h.size()) {
                    int indexOf = sb.indexOf("%s", i2);
                    sb.replace(indexOf, indexOf + 2, ((ai) this.h.get(i3)).f2989a);
                    i3++;
                    i2 = indexOf + 2;
                }
                this.m = sb.toString();
            }
            if (this.m != null) {
                this.m = this.m.replaceAll("&lsb;", "[").replaceAll("&rsb;", "]").replaceAll("&vb;", "|");
            }
        }
        return this.m;
    }

    public final String i() {
        return this.i;
    }

    public final String j() {
        return this.d;
    }

    public final List k() {
        return this.g;
    }

    public final boolean l() {
        return this.g != null && !this.g.isEmpty();
    }

    public final List m() {
        return this.h;
    }

    public final String n() {
        return this.f2987a;
    }
}
