package com.immomo.momo.service.bean;

import android.graphics.Bitmap;
import com.amap.mapapi.poisearch.PoiTypeDef;

public final class au {

    /* renamed from: a  reason: collision with root package name */
    public Bitmap f2997a;
    public String b = PoiTypeDef.All;
    public boolean c = true;
    public boolean d = false;

    public final boolean equals(Object obj) {
        return this.b.equals(((au) obj).b);
    }
}
