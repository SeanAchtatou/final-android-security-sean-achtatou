package com.immomo.momo.service;

import android.database.sqlite.SQLiteDatabase;
import android.support.v4.b.a;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.android.c.u;
import com.immomo.momo.g;
import com.immomo.momo.protocol.imjson.util.c;
import com.immomo.momo.service.a.ap;
import com.immomo.momo.service.a.aq;
import com.immomo.momo.service.a.ar;
import com.immomo.momo.service.a.ba;
import com.immomo.momo.service.a.h;
import com.immomo.momo.service.a.v;
import com.immomo.momo.service.a.w;
import com.immomo.momo.service.bean.Message;
import com.immomo.momo.service.bean.a.b;
import com.immomo.momo.service.bean.a.i;
import com.immomo.momo.service.bean.aw;
import com.immomo.momo.service.bean.ax;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.service.bean.n;
import com.immomo.momo.service.bean.o;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public final class ag extends b {
    private ba c;
    private ae d;
    private w e;
    private g f;
    private aq g;
    private ap h;
    private v i;
    private w j;
    private h k;
    private c l;
    private t m;

    public ag() {
        this(PoiTypeDef.All);
    }

    public ag(SQLiteDatabase sQLiteDatabase) {
        this.c = null;
        this.d = null;
        this.e = null;
        this.f = null;
        this.g = null;
        this.h = null;
        this.i = null;
        this.j = null;
        this.k = null;
        this.l = null;
        this.m = null;
        this.f2968a = sQLiteDatabase;
        this.m = new t(sQLiteDatabase);
        this.d = new ae(sQLiteDatabase);
        this.e = new w(sQLiteDatabase);
        this.j = new w(sQLiteDatabase);
        this.i = new v(sQLiteDatabase);
        this.c = new ba(sQLiteDatabase);
        this.g = new aq(sQLiteDatabase);
        this.h = new ap(sQLiteDatabase);
        this.k = new h(sQLiteDatabase);
        this.f = new g(sQLiteDatabase);
    }

    private ag(String str) {
        this.c = null;
        this.d = null;
        this.e = null;
        this.f = null;
        this.g = null;
        this.h = null;
        this.i = null;
        this.j = null;
        this.k = null;
        this.l = null;
        this.m = null;
        if (a.a((CharSequence) str)) {
            this.f2968a = g.d().e();
        } else {
            this.f2968a = new com.immomo.momo.service.a.g(g.c(), str).getWritableDatabase();
        }
        this.m = new t(this.f2968a);
        this.d = new ae(this.f2968a);
        this.e = new w(this.f2968a);
        this.f = new g(this.f2968a);
        this.j = new w(this.f2968a);
        this.i = new v(this.f2968a);
        this.c = new ba(this.f2968a);
        this.g = new aq(this.f2968a);
        this.h = new ap(this.f2968a);
        this.l = new c();
        this.k = new h(this.f2968a);
    }

    private void a(ax axVar) {
        int i2 = 0;
        switch (axVar.o) {
            case 0:
                axVar.b = (bf) this.c.a((Serializable) axVar.f3000a);
                axVar.f = this.d.l(axVar.f3000a);
                break;
            case 1:
                if (!g.r().y) {
                    String[] a2 = this.h.a("remoteid", new String[0], new String[0]);
                    if (a2 != null && a2.length > 0) {
                        i2 = this.d.d(a2);
                    }
                    axVar.g = i2;
                    break;
                } else {
                    axVar.f = e();
                    break;
                }
            case 2:
                axVar.c = (com.immomo.momo.service.bean.a.a) this.j.a((Serializable) axVar.f3000a);
                i c2 = g.r().c(axVar.f3000a);
                if (c2 != null && !c2.f2978a) {
                    axVar.g = this.e.f(axVar.f3000a);
                    break;
                } else {
                    axVar.f = this.e.e(axVar.f3000a);
                    break;
                }
                break;
            case 5:
                if (!g.r().x) {
                    axVar.g = this.l.g();
                    break;
                } else {
                    axVar.f = this.l.g();
                    break;
                }
            case 6:
                axVar.d = (n) this.k.a((Serializable) axVar.f3000a);
                o d2 = g.r().d(axVar.f3000a);
                if (d2 != null && !d2.f3033a) {
                    axVar.g = this.f.g(axVar.f3000a);
                    break;
                } else {
                    axVar.f = this.f.f(axVar.f3000a);
                    break;
                }
                break;
            case 7:
                axVar.e = c.a();
                break;
            case 8:
                axVar.f = this.m.c();
                break;
        }
        if (!a.a((CharSequence) axVar.i)) {
            switch (axVar.o) {
                case 0:
                case 1:
                case 7:
                    axVar.k = this.d.h(axVar.i);
                    return;
                case 2:
                    axVar.k = this.e.c(axVar.i);
                    return;
                case 3:
                case 4:
                default:
                    return;
                case 5:
                    com.immomo.momo.service.bean.i c3 = this.l.c(axVar.i);
                    if (c3 != null) {
                        Message message = new Message(true);
                        message.msgId = axVar.i;
                        message.setContent(c3.j());
                        message.timestamp = c3.f();
                        message.remoteId = c3.h();
                        axVar.k = message;
                        return;
                    }
                    return;
                case 6:
                    axVar.k = this.f.c(axVar.i);
                    return;
                case 8:
                    com.immomo.momo.service.bean.ag a3 = this.m.a(axVar.i);
                    if (a3 != null) {
                        Message message2 = new Message(true);
                        message2.msgId = axVar.i;
                        message2.setContent(a3.h());
                        message2.timestamp = a3.b();
                        message2.remoteId = a3.f();
                        axVar.k = message2;
                        return;
                    }
                    return;
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.service.w.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.immomo.momo.service.w.a(java.lang.String, int):void
      com.immomo.momo.service.w.a(java.lang.String, boolean):void */
    private void a(ax axVar, boolean z) {
        boolean c2 = this.g.c(axVar.f3000a);
        if (c2) {
            this.g.b((Serializable) axVar.f3000a);
        }
        if (axVar.o == 1) {
            String[] a2 = this.h.a("remoteid", new String[0], new String[0]);
            this.h.c();
            if (z) {
                this.d.b(a2);
            } else {
                this.d.a(a2, 9, 5);
            }
            if (axVar.f > 0) {
                g.d().q();
            }
        } else if (axVar.o == 0) {
            if (axVar.f > 0) {
                g.d().n();
            }
            if (!c2 || !z) {
                this.d.c(axVar.f3000a);
                return;
            }
            this.d.i(axVar.f3000a);
            u.b().execute(new ah(axVar.f3000a));
        } else if (axVar.o == 2) {
            if (axVar.f > 0) {
                g.d().o();
            }
            if (z) {
                this.e.a(axVar.f3000a, false);
            } else {
                this.e.a(axVar.f3000a);
            }
        } else if (axVar.o == 5) {
            if (z) {
                this.l.i();
            } else {
                this.l.h();
            }
            if (axVar.f > 0) {
                g.d().s();
            }
        } else if (axVar.o == 6) {
            if (axVar.f > 0) {
                g.d().r();
            }
            if (z) {
                this.f.d(axVar.f3000a);
            } else {
                this.f.a(axVar.f3000a);
            }
        } else if (axVar.o == 7) {
            c.a();
            c.e();
            this.d.i(axVar.f3000a);
        } else if (axVar.o == 8) {
            if (z) {
                this.m.c.c();
            } else {
                this.m.d();
            }
            if (axVar.f > 0) {
                g.d().p();
            }
        }
    }

    public final aw a(String str) {
        Map map = (Map) this.h.a((Serializable) str);
        if (map == null || map.isEmpty()) {
            return null;
        }
        Map.Entry entry = (Map.Entry) map.entrySet().iterator().next();
        String str2 = (String) entry.getKey();
        aw awVar = new aw();
        awVar.a(str2);
        awVar.a((bf) this.c.a((Serializable) str2));
        awVar.a(this.d.a(str2, 0, 1));
        this.d.n(str2);
        awVar.a(this.d.m(str2));
        awVar.a((Date) entry.getValue());
        return awVar;
    }

    public final List a(int i2) {
        long currentTimeMillis = System.currentTimeMillis();
        List<ax> a2 = this.g.a(new String[0], new String[0], "orderid", false, i2, 21);
        boolean z = false;
        for (ax axVar : a2) {
            if (axVar.o == 5) {
                z = true;
            }
            a(axVar);
        }
        if (i2 == 0 && !z && !this.g.c("-2230")) {
            a2.add(0, e("-2230"));
        }
        this.b.a((Object) ("findSessions->" + (System.currentTimeMillis() - currentTimeMillis)));
        return a2;
    }

    public final void a(int i2, int i3) {
        String[] a2 = this.h.a("remoteid", new String[0], new String[0]);
        if (a2 != null && a2.length > 0) {
            this.d.a(a2, i3, i2);
        }
    }

    public final void a(b bVar) {
        this.i.b(bVar);
    }

    public final void a(String str, int i2) {
        if (i2 == 1 || i2 == 0) {
            this.g.a(Message.DBFIELD_LOCATIONJSON, Integer.valueOf(i2), str);
        }
    }

    public final void a(String str, String str2) {
        this.g.a("s_draft", str2, str);
    }

    public final void a(List list) {
        this.f2968a.beginTransaction();
        try {
            Iterator it = list.iterator();
            while (it.hasNext()) {
                String d2 = ((aw) it.next()).d();
                this.h.b((Serializable) d2);
                this.d.i(d2);
            }
            if (this.h.c(new String[0], new String[0]) <= 0) {
                this.g.b((Serializable) "-2222");
            } else {
                String a2 = this.h.a("remoteid", "time", new String[0], new String[0]);
                if (!a.a((CharSequence) a2)) {
                    String b = this.d.b(a2);
                    if (a.a((CharSequence) b)) {
                        b = "-1";
                    }
                    this.g.a("s_lastmsgid", b, "-2222");
                }
            }
            this.f2968a.setTransactionSuccessful();
        } catch (Exception e2) {
            this.b.a((Throwable) e2);
        } finally {
            this.f2968a.endTransaction();
        }
    }

    public final void a(List list, boolean z) {
        this.f2968a.beginTransaction();
        try {
            Iterator it = list.iterator();
            while (it.hasNext()) {
                a((ax) it.next(), z);
            }
            this.f2968a.setTransactionSuccessful();
        } catch (Exception e2) {
            this.b.a((Throwable) e2);
        } finally {
            this.f2968a.endTransaction();
        }
    }

    public final List b(int i2) {
        ArrayList arrayList = new ArrayList();
        for (Map entrySet : this.h.a(new String[0], new String[0], "time", false, i2, 21)) {
            Map.Entry entry = (Map.Entry) entrySet.entrySet().iterator().next();
            String str = (String) entry.getKey();
            aw awVar = new aw();
            awVar.a(str);
            awVar.a((bf) this.c.a((Serializable) str));
            awVar.a(this.d.a(str, 0, 1));
            this.d.n(str);
            awVar.a(this.d.m(str));
            awVar.a((Date) entry.getValue());
            arrayList.add(awVar);
        }
        return arrayList;
    }

    public final List b(int i2, int i3) {
        List<b> a2 = this.i.a(new String[0], new String[0], Message.DBFIELD_ID, false, i2, i3);
        for (b bVar : a2) {
            if (!a.a((CharSequence) bVar.c())) {
                bVar.a((com.immomo.momo.service.bean.a.a) this.j.a((Serializable) bVar.c()));
            }
            if (!a.a((CharSequence) bVar.g())) {
                bVar.a((bf) this.c.a((Serializable) bVar.g()));
            }
        }
        return a2;
    }

    public final void b(b bVar) {
        try {
            this.f2968a.beginTransaction();
            this.i.a(bVar);
            this.f2968a.setTransactionSuccessful();
        } catch (Exception e2) {
            this.b.a((Throwable) e2);
        } finally {
            this.f2968a.endTransaction();
        }
    }

    public final void b(List list) {
        this.f2968a.beginTransaction();
        try {
            Iterator it = list.iterator();
            while (it.hasNext()) {
                this.i.b((Serializable) ((b) it.next()).m());
            }
            this.f2968a.setTransactionSuccessful();
        } catch (Exception e2) {
            this.b.a((Throwable) e2);
        } finally {
            this.f2968a.endTransaction();
        }
    }

    public final boolean b(String str) {
        return this.h.a("remoteid", str);
    }

    public final List c() {
        StringBuilder sb = new StringBuilder();
        sb.append("select u.*, s.s_remoteid from sessions as s LEFT OUTER JOIN ");
        sb.append("users2 as u");
        sb.append(" on s.s_remoteid");
        sb.append(" = u.u_momoid where u.field23='both' and s.field1=0 order by s.orderid desc");
        this.b.a(sb);
        return this.c.b(sb.toString(), new String[0]);
    }

    public final boolean c(String str) {
        ax axVar;
        boolean z;
        Message a2;
        ax axVar2;
        if (!b(str)) {
            return false;
        }
        this.f2968a.beginTransaction();
        Message a3 = this.d.a(str);
        if (a3 == null) {
            return false;
        }
        ax axVar3 = (ax) this.g.a((Serializable) str);
        boolean j2 = this.d.j(str);
        if (axVar3 == null) {
            axVar = new ax(str);
            z = false;
        } else {
            axVar = axVar3;
            z = true;
        }
        axVar.i = a3.msgId;
        axVar.h = a3.timestamp;
        axVar.o = 0;
        axVar.j = ar.a(this.f2968a).a();
        if (z) {
            this.g.b(axVar);
        } else {
            this.g.a(axVar);
        }
        this.h.b((Serializable) str);
        if (this.h.c(new String[0], new String[0]) <= 0) {
            this.g.b((Serializable) "-2222");
        } else {
            String a4 = this.h.a("remoteid", "time", new String[0], new String[0]);
            if (!(a.a((CharSequence) a4) || (a2 = this.d.a(a4)) == null || (axVar2 = (ax) this.g.a((Serializable) "-2222")) == null)) {
                axVar2.i = a2.msgId;
                this.g.b(axVar2);
            }
        }
        if (j2) {
            this.d.k(str);
        }
        this.f2968a.setTransactionSuccessful();
        this.f2968a.endTransaction();
        return true;
    }

    public final int d() {
        String[] a2 = this.h.a("remoteid", new String[0], new String[0]);
        if (a2 == null || a2.length <= 0) {
            return 0;
        }
        return this.d.e(a2);
    }

    public final void d(String str) {
        this.h.b((Serializable) str);
        this.d.i(str);
        if (this.h.c(new String[0], new String[0]) <= 0) {
            this.g.b((Serializable) "-2222");
            return;
        }
        String a2 = this.h.a("remoteid", "time", new String[0], new String[0]);
        if (!a.a((CharSequence) a2)) {
            String b = this.d.b(a2);
            if (a.a((CharSequence) b)) {
                b = "-1";
            }
            this.g.a("s_lastmsgid", b, "-2222");
        }
    }

    public final int e() {
        String[] a2 = this.h.a("remoteid", new String[0], new String[0]);
        if (a2 == null || a2.length <= 0) {
            return 0;
        }
        return this.d.c(a2);
    }

    public final ax e(String str) {
        ax axVar = (ax) this.g.a((Serializable) str);
        if (axVar == null) {
            if (!"-2230".equals(str)) {
                return null;
            }
            axVar = new ax();
            axVar.f3000a = "-2230";
            axVar.j = ar.a(b()).a();
            axVar.o = 5;
            if (this.g.c(axVar.f3000a)) {
                this.g.b(axVar);
            } else {
                this.g.a(axVar);
            }
        }
        a(axVar);
        return axVar;
    }

    public final String f(String str) {
        ax axVar = (ax) this.g.a((Serializable) str);
        if (axVar == null) {
            return PoiTypeDef.All;
        }
        String str2 = axVar.l;
        if (a.a((CharSequence) str2)) {
            return PoiTypeDef.All;
        }
        this.g.a("s_draft", PoiTypeDef.All, str);
        return str2;
    }

    public final void f() {
        String[] a2 = this.h.a("remoteid", new String[0], new String[0]);
        if (a2 != null && a2.length > 0) {
            this.d.a(a2, 9, 5);
            this.d.a(a2, 9, 13);
        }
    }

    public final int g() {
        return this.i.c(new String[]{"field6"}, new String[]{"0"});
    }

    public final int g(String str) {
        return "1".equals(this.g.c(Message.DBFIELD_LOCATIONJSON, new String[]{"s_remoteid"}, new String[]{str})) ? 1 : 0;
    }

    public final int h() {
        return this.i.c(new String[0], new String[0]);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.service.ag.a(com.immomo.momo.service.bean.ax, boolean):void
     arg types: [com.immomo.momo.service.bean.ax, int]
     candidates:
      com.immomo.momo.service.ag.a(int, int):void
      com.immomo.momo.service.ag.a(java.lang.String, int):void
      com.immomo.momo.service.ag.a(java.lang.String, java.lang.String):void
      com.immomo.momo.service.ag.a(java.util.List, boolean):void
      com.immomo.momo.service.ag.a(com.immomo.momo.service.bean.ax, boolean):void */
    public final void h(String str) {
        ax axVar = new ax(str);
        axVar.o = 0;
        a(axVar, true);
    }

    public final b i(String str) {
        b bVar = (b) this.i.a((Serializable) str);
        if (bVar != null) {
            if (!a.a((CharSequence) bVar.c())) {
                bVar.a((com.immomo.momo.service.bean.a.a) this.j.a((Serializable) bVar.c()));
            }
            if (!a.a((CharSequence) bVar.g())) {
                bVar.a((bf) this.c.a((Serializable) bVar.g()));
            }
        }
        return bVar;
    }

    public final void i() {
        this.i.a(new String[]{"field6"}, new Object[]{1}, new String[0], new String[0]);
    }

    public final boolean j(String str) {
        return this.i.a(str) != null;
    }
}
