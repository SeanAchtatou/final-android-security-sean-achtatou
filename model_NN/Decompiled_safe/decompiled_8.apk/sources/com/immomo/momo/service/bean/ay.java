package com.immomo.momo.service.bean;

import android.support.v4.b.a;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.g;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public final class ay extends al {

    /* renamed from: a  reason: collision with root package name */
    public String f3001a;
    public String b;
    public int c;
    public int d;
    public String e;
    public String f;
    public List g = new ArrayList();
    public double h;
    public double i;
    public int j;
    public int k;
    public int l;
    public String m;
    public String[] n;
    public int o = 1;
    public boolean p = false;
    public boolean q = false;
    public String r;
    public String s;
    public Date t;
    public boolean u;
    private float v = -1.0f;

    public final float a() {
        return this.v;
    }

    public final void a(float f2) {
        this.v = f2;
        if (f2 < 0.0f) {
            this.e = g.a((int) R.string.profile_distance_unknown);
        } else {
            this.e = String.valueOf(a.a(f2 / 1000.0f)) + "km";
        }
    }

    public final boolean equals(Object obj) {
        return (!(obj instanceof ay) || this.f3001a == null) ? super.equals(obj) : this.f3001a.equals(((ay) obj).f3001a);
    }

    public final String getLoadImageId() {
        return (this.n == null || this.n.length <= 0) ? PoiTypeDef.All : this.n[0];
    }

    public final String toString() {
        return "GroupSite [siteId=" + this.f3001a + ", venueid=" + this.b + ", groupCount=" + this.c + ", type=" + this.d + ", distancString=" + this.e + ", name=" + this.f + ", distance=" + this.v + ", groups=" + this.g + "]";
    }
}
