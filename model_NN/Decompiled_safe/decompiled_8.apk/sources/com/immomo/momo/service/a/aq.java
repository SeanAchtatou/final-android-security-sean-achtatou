package com.immomo.momo.service.a;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.Message;
import com.immomo.momo.service.bean.ax;

public final class aq extends b {
    public aq(SQLiteDatabase sQLiteDatabase) {
        super(sQLiteDatabase, "sessions", "s_remoteid");
    }

    private static void a(ax axVar, Cursor cursor) {
        boolean z = true;
        axVar.f3000a = cursor.getString(cursor.getColumnIndex("s_remoteid"));
        axVar.i = cursor.getString(cursor.getColumnIndex("s_lastmsgid"));
        axVar.h = a(cursor.getLong(cursor.getColumnIndex("s_fetchtime")));
        axVar.l = cursor.getString(cursor.getColumnIndex("s_draft"));
        try {
            axVar.f = cursor.getInt(cursor.getColumnIndex(Message.DBFIELD_CONVERLOCATIONJSON));
        } catch (Exception e) {
        }
        try {
            if (cursor.getInt(cursor.getColumnIndex(Message.DBFIELD_LOCATIONJSON)) != 1) {
                z = false;
            }
            axVar.n = z;
        } catch (Exception e2) {
        }
        try {
            axVar.o = cursor.getInt(cursor.getColumnIndex(Message.DBFIELD_SAYHI));
        } catch (Exception e3) {
            axVar.o = 0;
        }
        try {
            axVar.j = cursor.getInt(cursor.getColumnIndex("orderid"));
        } catch (Exception e4) {
        }
    }

    private void a(String str, ax axVar) {
        Object[] objArr = new Object[8];
        objArr[0] = Long.valueOf(a(axVar.h));
        objArr[1] = axVar.i;
        objArr[2] = axVar.l == null ? PoiTypeDef.All : axVar.l;
        objArr[3] = Integer.valueOf(axVar.n ? 1 : 0);
        objArr[4] = Integer.valueOf(axVar.o);
        objArr[5] = Integer.valueOf(axVar.j);
        objArr[6] = Integer.valueOf(axVar.f);
        objArr[7] = axVar.f3000a;
        a(str, objArr);
    }

    private static ax b(Cursor cursor) {
        ax axVar = new ax();
        a(axVar, cursor);
        return axVar;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Cursor cursor) {
        return b(cursor);
    }

    public final void a(ax axVar) {
        StringBuilder sb = new StringBuilder();
        sb.append("insert into " + this.f2954a + " (").append("s_fetchtime, s_lastmsgid, s_draft, field2, field1, orderid, field3,s_remoteid) values(?,?,?,?,?,?,?,?)");
        a(sb.toString(), axVar);
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void a(Object obj, Cursor cursor) {
        a((ax) obj, cursor);
    }

    public final void a(String str, Object[] objArr) {
        super.a(str, objArr);
        g.q();
    }

    public final void b(ax axVar) {
        StringBuilder sb = new StringBuilder();
        sb.append("update " + this.f2954a + " set ").append("s_fetchtime=?, s_lastmsgid=?, s_draft=?, field2=?, field1=?, orderid=?, field3=? where s_remoteid=?");
        a(sb.toString(), axVar);
    }
}
