package com.immomo.momo.service;

import android.database.sqlite.SQLiteDatabase;
import android.support.v4.b.a;
import com.amap.mapapi.location.LocationManagerProxy;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.g;
import com.immomo.momo.service.a.af;
import com.immomo.momo.service.a.ag;
import com.immomo.momo.service.a.ap;
import com.immomo.momo.service.a.aq;
import com.immomo.momo.service.a.ar;
import com.immomo.momo.service.a.b;
import com.immomo.momo.service.a.r;
import com.immomo.momo.service.bean.Message;
import com.immomo.momo.service.bean.ax;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.util.jni.Codec;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.Serializable;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

public final class ae extends b {
    private ag c;
    private aq d;
    private ap e;
    private af f;

    public ae() {
        this(PoiTypeDef.All);
    }

    public ae(SQLiteDatabase sQLiteDatabase) {
        this.c = null;
        this.d = null;
        this.e = null;
        this.f = new af();
        this.f2968a = sQLiteDatabase;
        this.c = new ag(sQLiteDatabase);
        this.d = new aq(sQLiteDatabase);
        this.e = new ap(sQLiteDatabase);
        new r(sQLiteDatabase);
    }

    private ae(String str) {
        this.c = null;
        this.d = null;
        this.e = null;
        this.f = new af();
        if (a.a((CharSequence) str)) {
            this.f2968a = g.d().e();
        } else {
            this.f2968a = new com.immomo.momo.service.a.g(g.c(), str).getWritableDatabase();
        }
        this.c = new ag(this.f2968a);
        this.d = new aq(this.f2968a);
        this.e = new ap(this.f2968a);
        new r(this.f2968a);
    }

    private void d(Message message) {
        String str;
        int i;
        boolean z = true;
        boolean c2 = this.e.c(message.remoteId);
        if (!message.receive || !message.isSayhi) {
            str = message.remoteId;
            i = "1602".equals(str) ? 7 : 0;
            if (c2) {
                this.e.b((Serializable) str);
                if (this.e.c(new String[0], new String[0]) <= 0) {
                    this.d.b((Serializable) "-2222");
                } else {
                    String a2 = this.e.a("remoteid", "time", new String[0], new String[0]);
                    if (!a.a((CharSequence) a2)) {
                        String a3 = this.c.a(Message.DBFIELD_MSGID, Message.DBFIELD_ID, new String[]{Message.DBFIELD_REMOTEID}, new String[]{a2});
                        if (a.a((CharSequence) a3)) {
                            a3 = "-1";
                        }
                        this.d.a("s_lastmsgid", a3, "-2222");
                    }
                }
            }
        } else {
            HashMap hashMap = new HashMap();
            hashMap.put(message.remoteId, message.timestamp);
            if (c2) {
                this.e.c((Map) hashMap);
                i = 1;
                str = "-2222";
            } else {
                this.e.a((Map) hashMap);
                i = 1;
                str = "-2222";
            }
        }
        ax axVar = (ax) this.d.a((Serializable) str);
        if (axVar == null) {
            axVar = new ax(str);
            z = false;
        }
        axVar.i = message.msgId;
        axVar.j = ar.a(this.f2968a).a();
        axVar.h = message.timestamp;
        axVar.o = i;
        if (z) {
            this.d.b(axVar);
        } else {
            this.d.a(axVar);
        }
    }

    public final Message a(String str) {
        return (Message) this.c.b(Message.DBFIELD_TIME, new String[]{Message.DBFIELD_REMOTEID}, new String[]{str});
    }

    public final List a(String str, int i, int i2) {
        long currentTimeMillis = System.currentTimeMillis();
        List a2 = this.c.a(new String[]{Message.DBFIELD_REMOTEID}, new String[]{str}, Message.DBFIELD_TIME, false, i, i2);
        Collections.reverse(a2);
        this.b.a((Object) ("findMessageByRemoteId->" + (System.currentTimeMillis() - currentTimeMillis)));
        return a2;
    }

    public final void a() {
        if (this.f2968a != null) {
            this.f2968a.close();
        }
    }

    public final void a(Message message) {
        try {
            if (a.a((CharSequence) message.msgId)) {
                throw new NullPointerException("msg.msg.remoteId or msg.msgId is null");
            }
            message.msginfo = com.immomo.momo.util.a.a.a(message);
            this.c.a(message);
            d(message);
        } catch (Exception e2) {
            this.b.a((Throwable) e2);
        }
    }

    public final void a(String str, int i) {
        this.c.a(new String[]{Message.DBFIELD_STATUS}, new Object[]{Integer.valueOf(i)}, new String[]{Message.DBFIELD_MSGID}, new Object[]{str});
    }

    public final void a(List list, String str) {
        this.f2968a.beginTransaction();
        try {
            Iterator it = list.iterator();
            while (it.hasNext()) {
                Message message = (Message) it.next();
                message.msginfo = com.immomo.momo.util.a.a.a(message);
                if (!f(message.msgId)) {
                    this.c.a(message);
                }
            }
            new ax(str).o = 0;
            d(a(str));
            this.f2968a.setTransactionSuccessful();
        } finally {
            this.f2968a.endTransaction();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object, java.lang.String, java.lang.Object[]):void
     arg types: [java.lang.String, int, java.lang.String, java.lang.String[]]
     candidates:
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.String[], java.lang.String[], java.lang.String[]):int
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.String, java.lang.String[], java.lang.String[]):java.lang.String
      com.immomo.momo.service.a.b.a(java.lang.String[], java.lang.String[], java.lang.String, boolean):java.util.List
      com.immomo.momo.service.a.b.a(java.lang.String[], java.lang.Object[], java.lang.String[], java.lang.Object[]):void
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object, java.lang.String, java.lang.Object[]):void */
    public final void a(String[] strArr) {
        this.c.a(Message.DBFIELD_STATUS, (Object) 6, Message.DBFIELD_MSGID, (Object[]) strArr);
    }

    public final void a(String[] strArr, int i, int i2) {
        if (strArr != null && strArr.length > 0) {
            this.c.a(Message.DBFIELD_STATUS, Integer.valueOf(i), Integer.valueOf(i2), Message.DBFIELD_REMOTEID, strArr);
        }
    }

    public final boolean a(int i, bf bfVar, List list) {
        FileInputStream fileInputStream;
        List a2 = this.f.a(new String[]{"remoteid"}, new String[]{bfVar.h}, "id", false, i, 2);
        if (a2 != null && !a2.isEmpty()) {
            try {
                fileInputStream = new FileInputStream(new File(((com.immomo.momo.service.bean.ap) a2.get(0)).f2993a));
                try {
                    byte[] b = Codec.b(a.b(fileInputStream), bfVar.h);
                    if (b != null && b.length > 0) {
                        JSONArray jSONArray = new JSONArray(new String(b));
                        for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                            JSONObject jSONObject = jSONArray.getJSONObject(i2);
                            Message message = new Message();
                            message.msgId = jSONObject.getString("msgid");
                            message.remoteId = jSONObject.getString("remoteid");
                            message.contentType = jSONObject.getInt("type");
                            message.status = jSONObject.getInt(LocationManagerProxy.KEY_STATUS_CHANGED);
                            message.timestamp = b.a(jSONObject.getLong("timestamp"));
                            message.receive = jSONObject.getBoolean("receive");
                            com.immomo.momo.util.a.a.a(jSONObject.getString("msginfo"), message);
                            if (message.contentType == 2) {
                                message.parseDbLocationJson(jSONObject.optString("map_location"));
                                message.parseDbConverLocationJson(jSONObject.optString("map_convertlocation"));
                            }
                            message.remoteUser = bfVar;
                            list.add(message);
                        }
                    }
                    a.a((Closeable) fileInputStream);
                } catch (Throwable th) {
                    th = th;
                    a.a((Closeable) fileInputStream);
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                fileInputStream = null;
                a.a((Closeable) fileInputStream);
                throw th;
            }
        }
        return a2 != null && a2.size() > 1;
    }

    public final String b(String str) {
        return this.c.a(Message.DBFIELD_MSGID, Message.DBFIELD_TIME, new String[]{Message.DBFIELD_REMOTEID}, new String[]{str});
    }

    public final void b(Message message) {
        if (a.a((CharSequence) message.remoteId) || a.a((CharSequence) message.msgId)) {
            throw new NullPointerException("msg.msg.remoteId or msg.msgId is null");
        }
        message.msginfo = com.immomo.momo.util.a.a.a(message);
        this.c.b(message);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.service.a.b.c(java.lang.String, java.lang.Object[]):void
     arg types: [java.lang.String, java.lang.String[]]
     candidates:
      com.immomo.momo.service.a.b.c(android.database.Cursor, java.lang.String):java.lang.String
      com.immomo.momo.service.a.b.c(java.lang.String[], java.lang.String[]):int
      com.immomo.momo.service.a.b.c(java.lang.String, java.lang.String[]):java.util.List
      com.immomo.momo.service.a.b.c(java.lang.String, java.lang.Object[]):void */
    public final void b(String[] strArr) {
        this.c.c(Message.DBFIELD_MSGID, (Object[]) strArr);
        this.f.c("remoteid", (Object[]) strArr);
    }

    public final int c(String[] strArr) {
        if (strArr == null || strArr.length <= 0) {
            return 0;
        }
        return this.c.a(Message.DBFIELD_REMOTEID, strArr, new String[]{Message.DBFIELD_STATUS}, new String[]{"5"});
    }

    public final void c() {
        this.c.a(new String[]{Message.DBFIELD_STATUS}, new Object[]{9}, new String[]{Message.DBFIELD_RECEIVE, Message.DBFIELD_STATUS}, new Object[]{1, 5});
        this.c.a(new String[]{Message.DBFIELD_STATUS}, new Object[]{9}, new String[]{Message.DBFIELD_RECEIVE, Message.DBFIELD_STATUS}, new Object[]{1, 13});
    }

    public final void c(Message message) {
        this.c.c(message);
        if (this.d.c(message.remoteId)) {
            String a2 = this.c.a(Message.DBFIELD_MSGID, Message.DBFIELD_TIME, new String[]{Message.DBFIELD_REMOTEID}, new String[]{message.remoteId});
            if (a.a((CharSequence) a2)) {
                a2 = "-1";
            }
            this.d.a("s_lastmsgid", a2, message.remoteId);
        }
    }

    public final void c(String str) {
        this.c.a(new String[]{Message.DBFIELD_STATUS}, new Object[]{9}, new String[]{Message.DBFIELD_REMOTEID, Message.DBFIELD_RECEIVE, Message.DBFIELD_STATUS}, new Object[]{str, 1, 5});
        this.c.a(new String[]{Message.DBFIELD_STATUS}, new Object[]{9}, new String[]{Message.DBFIELD_REMOTEID, Message.DBFIELD_RECEIVE, Message.DBFIELD_STATUS}, new Object[]{str, 1, 13});
    }

    public final int d(String[] strArr) {
        if (strArr == null || strArr.length <= 0) {
            return 0;
        }
        return this.c.a(Message.DBFIELD_REMOTEID, strArr, new String[]{Message.DBFIELD_STATUS}, new String[]{"13"});
    }

    public final void d() {
        this.f2968a.beginTransaction();
        try {
            this.c.a(new String[]{Message.DBFIELD_STATUS}, new Object[]{3}, new String[]{Message.DBFIELD_STATUS}, new Object[]{1});
            this.c.a(new String[]{Message.DBFIELD_STATUS}, new Object[]{3}, new String[]{Message.DBFIELD_STATUS}, new Object[]{7});
            this.c.a(new String[]{Message.DBFIELD_STATUS}, new Object[]{3}, new String[]{Message.DBFIELD_STATUS}, new Object[]{8});
            this.f2968a.setTransactionSuccessful();
        } catch (Exception e2) {
        } finally {
            this.f2968a.endTransaction();
        }
    }

    public final void d(String str) {
        this.c.a(new String[]{Message.DBFIELD_STATUS}, new Object[]{6}, new String[]{Message.DBFIELD_REMOTEID, Message.DBFIELD_RECEIVE, Message.DBFIELD_STATUS}, new Object[]{str, 0, 2});
    }

    public final int e() {
        long currentTimeMillis = System.currentTimeMillis();
        int c2 = this.c.c(new String[]{Message.DBFIELD_STATUS}, new String[]{"5"});
        this.b.a((Object) ("getUnreadedCount->" + (System.currentTimeMillis() - currentTimeMillis)));
        return c2;
    }

    public final int e(String[] strArr) {
        return this.c.a(Message.DBFIELD_REMOTEID, strArr, new String[]{Message.DBFIELD_RECEIVE}, new String[]{"1"});
    }

    public final Message e(String str) {
        return (Message) this.c.b(Message.DBFIELD_MSGID, str);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object, java.lang.String, java.lang.Object[]):void
     arg types: [java.lang.String, int, java.lang.String, java.lang.String[]]
     candidates:
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.String[], java.lang.String[], java.lang.String[]):int
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.String, java.lang.String[], java.lang.String[]):java.lang.String
      com.immomo.momo.service.a.b.a(java.lang.String[], java.lang.String[], java.lang.String, boolean):java.util.List
      com.immomo.momo.service.a.b.a(java.lang.String[], java.lang.Object[], java.lang.String[], java.lang.Object[]):void
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object, java.lang.String, java.lang.Object[]):void */
    public final void f(String[] strArr) {
        if (strArr != null && strArr.length > 0) {
            this.c.a(Message.DBFIELD_STATUS, (Object) 4, Message.DBFIELD_MSGID, (Object[]) strArr);
        }
    }

    public final boolean f(String str) {
        return this.c.c(new String[]{Message.DBFIELD_MSGID}, new String[]{str}) > 0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.service.a.b.a(java.lang.String[], java.lang.String[], java.lang.String, boolean):java.util.List
     arg types: [java.lang.String[], java.lang.String[], java.lang.String, int]
     candidates:
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.String[], java.lang.String[], java.lang.String[]):int
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.String, java.lang.String[], java.lang.String[]):java.lang.String
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object, java.lang.String, java.lang.Object[]):void
      com.immomo.momo.service.a.b.a(java.lang.String[], java.lang.Object[], java.lang.String[], java.lang.Object[]):void
      com.immomo.momo.service.a.b.a(java.lang.String[], java.lang.String[], java.lang.String, boolean):java.util.List */
    public final List g(String str) {
        long currentTimeMillis = System.currentTimeMillis();
        List a2 = this.c.a(new String[]{Message.DBFIELD_REMOTEID, Message.DBFIELD_TYPE}, new String[]{str, new StringBuilder(String.valueOf(1)).toString()}, Message.DBFIELD_TIME, true);
        this.b.a((Object) ("findMessageByRemoteIdContentType->" + (System.currentTimeMillis() - currentTimeMillis)));
        return a2;
    }

    public final Message h(String str) {
        return (Message) this.c.b(Message.DBFIELD_MSGID, str);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.immomo.momo.service.a.ag.a(com.immomo.momo.service.bean.Message, android.database.Cursor):void
      com.immomo.momo.service.a.ag.a(java.lang.String, com.immomo.momo.service.bean.Message):void
      com.immomo.momo.service.a.ag.a(java.lang.Object, android.database.Cursor):void
      com.immomo.momo.service.a.b.a(android.database.Cursor, java.lang.String):int
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.String[]):android.database.Cursor
      com.immomo.momo.service.a.b.a(java.lang.String[], java.lang.String[]):java.util.List
      com.immomo.momo.service.a.b.a(java.lang.Object, android.database.Cursor):void
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object[]):void
      com.immomo.momo.service.a.b.a(java.lang.String[], java.lang.Object[]):void
      com.immomo.momo.service.a.b.a(java.lang.Object, java.io.Serializable):boolean
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.String):boolean
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.immomo.momo.service.a.af.a(com.immomo.momo.service.bean.ap, android.database.Cursor):void
      com.immomo.momo.service.a.af.a(java.lang.Object, android.database.Cursor):void
      com.immomo.momo.service.a.b.a(android.database.Cursor, java.lang.String):int
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.String[]):android.database.Cursor
      com.immomo.momo.service.a.b.a(java.lang.String[], java.lang.String[]):java.util.List
      com.immomo.momo.service.a.b.a(java.lang.Object, android.database.Cursor):void
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object[]):void
      com.immomo.momo.service.a.b.a(java.lang.String[], java.lang.Object[]):void
      com.immomo.momo.service.a.b.a(java.lang.Object, java.io.Serializable):boolean
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.String):boolean
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object):void */
    public final void i(String str) {
        this.c.a(Message.DBFIELD_REMOTEID, (Object) str);
        this.d.a("s_lastmsgid", PoiTypeDef.All, str);
        this.f.a("remoteid", (Object) str);
    }

    public final boolean j(String str) {
        if (l(str) <= 0) {
            if (this.c.c(new String[]{Message.DBFIELD_REMOTEID, Message.DBFIELD_STATUS}, new String[]{str, "13"}) <= 0) {
                return false;
            }
        }
        return true;
    }

    public final void k(String str) {
        this.c.a(new String[]{Message.DBFIELD_STATUS}, new String[]{new StringBuilder(String.valueOf(5)).toString()}, new String[]{Message.DBFIELD_REMOTEID, Message.DBFIELD_STATUS}, new String[]{str, new StringBuilder(String.valueOf(13)).toString()});
    }

    public final int l(String str) {
        return this.c.c(new String[]{Message.DBFIELD_REMOTEID, Message.DBFIELD_STATUS}, new String[]{str, "5"});
    }

    public final int m(String str) {
        int c2 = this.c.c(new String[]{Message.DBFIELD_REMOTEID, Message.DBFIELD_STATUS}, new String[]{str, "9"});
        int c3 = this.c.c(new String[]{Message.DBFIELD_REMOTEID, Message.DBFIELD_STATUS}, new String[]{str, "5"});
        return c2 + c3 + this.c.c(new String[]{Message.DBFIELD_REMOTEID, Message.DBFIELD_STATUS}, new String[]{str, "13"});
    }

    public final int n(String str) {
        return this.c.c(new String[]{Message.DBFIELD_REMOTEID, Message.DBFIELD_RECEIVE}, new String[]{str, "1"});
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.immomo.momo.service.a.af.a(com.immomo.momo.service.bean.ap, android.database.Cursor):void
      com.immomo.momo.service.a.af.a(java.lang.Object, android.database.Cursor):void
      com.immomo.momo.service.a.b.a(android.database.Cursor, java.lang.String):int
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.String[]):android.database.Cursor
      com.immomo.momo.service.a.b.a(java.lang.String[], java.lang.String[]):java.util.List
      com.immomo.momo.service.a.b.a(java.lang.Object, android.database.Cursor):void
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object[]):void
      com.immomo.momo.service.a.b.a(java.lang.String[], java.lang.Object[]):void
      com.immomo.momo.service.a.b.a(java.lang.Object, java.io.Serializable):boolean
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.String):boolean
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object):void */
    public final void o(String str) {
        this.f.a("remoteid", (Object) str);
    }
}
