package com.immomo.momo.service;

import com.immomo.momo.a;
import com.immomo.momo.service.bean.y;
import com.immomo.momo.service.bean.z;
import com.sina.sdk.api.message.InviteApi;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

final class s implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ r f3056a;
    private final /* synthetic */ List b;

    s(r rVar, List list) {
        this.f3056a = rVar;
        this.b = list;
    }

    public final void run() {
        BufferedWriter bufferedWriter;
        try {
            File file = new File(a.r(), "eventdynamics");
            file.createNewFile();
            bufferedWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file)));
            try {
                JSONArray jSONArray = new JSONArray();
                for (z zVar : this.b) {
                    JSONObject jSONObject = new JSONObject();
                    jSONObject.put("title", zVar.a());
                    JSONArray jSONArray2 = new JSONArray();
                    for (y yVar : zVar.f3043a) {
                        JSONObject jSONObject2 = new JSONObject();
                        jSONObject2.put("eventid", yVar.h());
                        jSONObject2.put("update_time", yVar.d() != null ? yVar.d().getTime() / 1000 : 0);
                        jSONObject2.put("name", yVar.i());
                        jSONObject2.put("start_time", (Object) null);
                        jSONObject2.put("end_time", (Object) null);
                        jSONObject2.put("mine", yVar.m() ? 1 : 0);
                        jSONObject2.put("comment_count", yVar.k());
                        jSONObject2.put("time_desc", yVar.n());
                        jSONObject2.put("join_count", yVar.j());
                        JSONObject jSONObject3 = new JSONObject();
                        jSONObject3.put("img", yVar.f());
                        jSONObject3.put(InviteApi.KEY_TEXT, yVar.g());
                        jSONObject3.put("highlight", yVar.e());
                        jSONObject2.put("desc", jSONObject3);
                        JSONArray jSONArray3 = new JSONArray();
                        for (String put : yVar.o()) {
                            jSONArray3.put(put);
                        }
                        jSONObject2.put("friend_avatars", jSONArray3);
                        jSONArray2.put(jSONObject2);
                    }
                    jSONObject.put("events", jSONArray2);
                    jSONArray.put(jSONObject);
                }
                bufferedWriter.write(jSONArray.toString());
                bufferedWriter.flush();
                android.support.v4.b.a.a(bufferedWriter);
            } catch (Exception e) {
                e = e;
            }
        } catch (Exception e2) {
            e = e2;
            bufferedWriter = null;
            try {
                this.f3056a.c.a((Throwable) e);
                android.support.v4.b.a.a(bufferedWriter);
            } catch (Throwable th) {
                th = th;
                android.support.v4.b.a.a(bufferedWriter);
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            bufferedWriter = null;
            android.support.v4.b.a.a(bufferedWriter);
            throw th;
        }
    }
}
