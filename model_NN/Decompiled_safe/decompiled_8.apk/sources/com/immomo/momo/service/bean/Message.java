package com.immomo.momo.service.bean;

import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.a.a.f.b;
import com.immomo.momo.android.view.ao;
import com.immomo.momo.android.view.au;
import com.immomo.momo.service.bean.a.a;
import com.immomo.momo.util.jni.Codec;
import java.io.File;
import java.io.Serializable;
import java.util.Date;
import org.json.JSONObject;

public class Message extends al implements Serializable {
    public static final int CHATTYPE_DISCUSS = 3;
    public static final int CHATTYPE_GROUP = 2;
    public static final int CHATTYPE_USER = 1;
    public static final int CONTENTTYPE_MESSAGE_ACTION = 7;
    public static final int CONTENTTYPE_MESSAGE_AUDIO = 4;
    public static final int CONTENTTYPE_MESSAGE_EMOTE = 6;
    public static final int CONTENTTYPE_MESSAGE_IMAGE = 1;
    public static final int CONTENTTYPE_MESSAGE_MAP = 2;
    public static final int CONTENTTYPE_MESSAGE_NOTICE = 5;
    public static final int CONTENTTYPE_MESSAGE_SYSTEM = 3;
    public static final int CONTENTTYPE_MESSAGE_TEXT = 0;
    public static final String DBFIELD_CONVERLOCATIONJSON = "field3";
    public static final String DBFIELD_FAILCOUNT = "m_failcount";
    public static final String DBFIELD_GROUPID = "field4";
    public static final String DBFIELD_ID = "_id";
    public static final String DBFIELD_LOCATIONJSON = "field2";
    public static final String DBFIELD_MSGID = "m_msgid";
    public static final String DBFIELD_MSMGINFO = "m_msginfo";
    public static final String DBFIELD_RECEIVE = "m_receive";
    public static final String DBFIELD_REMOTEID = "m_remoteid";
    public static final String DBFIELD_SAYHI = "field1";
    public static final String DBFIELD_STATUS = "m_status";
    public static final String DBFIELD_TIME = "m_time";
    public static final String DBFIELD_TYPE = "m_type";
    public static final String EXPAND_MESSAGE_AUDIO = "amr";
    public static final int STATUS_CLOUD = 10;
    public static final int STATUS_FAILED = 3;
    public static final int STATUS_IGNORE = 9;
    public static final int STATUS_NONE = 11;
    public static final int STATUS_POSITIONING = 8;
    public static final int STATUS_RECEIVER_READED = 4;
    public static final int STATUS_RECE_SILENT = 13;
    public static final int STATUS_RECE_UNREADED = 5;
    public static final int STATUS_SENDED = 2;
    public static final int STATUS_SENDING = 1;
    public static final int STATUS_SEND_READED = 6;
    public static final int STATUS_UPLOADING = 7;
    private static final long serialVersionUID = 1;
    public float acc;
    public a action;
    public int audiotime;
    public int bubbleStyle;
    public int chatType;
    private String content;
    public int contentType;
    public float convertAcc;
    public double convertLat;
    public double convertLng;
    public n discuss;
    public String discussId;
    public float distance;
    public Date distanceTime;
    private ao emoteText;
    public String expandedName;
    public int failcount;
    public String fileName;
    public long fileSize;
    public float fileUploadProgrss;
    public boolean fileUploadSuccess;
    public long fileUploadedLength;
    public au gifDecoder;
    public a group;
    public String groupId;
    public int id;
    public int imageHeight;
    public int imageWidth;
    public boolean isAudioPlayed;
    public boolean isLoadingResourse;
    public boolean isSayhi;
    public double lat;
    public int layout;
    public double lng;
    public String msgId;
    public String msginfo;
    public boolean receive;
    public String remoteId;
    public bf remoteUser;
    public String source;
    public int status;
    public aq tail;
    public String tailAction;
    public String tailIcon;
    public String tailTitle;
    public File tempFile;
    public Date timestamp;
    public String username;

    public Message() {
        this.isSayhi = false;
        this.chatType = 1;
        this.groupId = null;
        this.group = null;
        this.discussId = null;
        this.discuss = null;
        this.bubbleStyle = 0;
        this.contentType = 0;
        this.status = 1;
        this.isAudioPlayed = false;
        this.isLoadingResourse = false;
        this.timestamp = new Date();
        this.fileUploadSuccess = false;
        this.fileUploadedLength = 0;
        this.fileUploadProgrss = 0.0f;
        this.failcount = 0;
        this.layout = 0;
        this.imageWidth = 0;
        this.imageHeight = 0;
        this.receive = false;
        this.tempFile = null;
        this.action = null;
        this.distance = -1.0f;
        this.distanceTime = null;
        this.username = PoiTypeDef.All;
        this.emoteText = new ao();
    }

    public Message(int i, boolean z) {
        this.isSayhi = false;
        this.chatType = 1;
        this.groupId = null;
        this.group = null;
        this.discussId = null;
        this.discuss = null;
        this.bubbleStyle = 0;
        this.contentType = 0;
        this.status = 1;
        this.isAudioPlayed = false;
        this.isLoadingResourse = false;
        this.timestamp = new Date();
        this.fileUploadSuccess = false;
        this.fileUploadedLength = 0;
        this.fileUploadProgrss = 0.0f;
        this.failcount = 0;
        this.layout = 0;
        this.imageWidth = 0;
        this.imageHeight = 0;
        this.receive = false;
        this.tempFile = null;
        this.action = null;
        this.distance = -1.0f;
        this.distanceTime = null;
        this.username = PoiTypeDef.All;
        this.emoteText = new ao();
        this.receive = z;
        this.contentType = i;
        if (z) {
            this.status = 5;
        } else {
            this.status = 1;
        }
    }

    public Message(String str) {
        this.isSayhi = false;
        this.chatType = 1;
        this.groupId = null;
        this.group = null;
        this.discussId = null;
        this.discuss = null;
        this.bubbleStyle = 0;
        this.contentType = 0;
        this.status = 1;
        this.isAudioPlayed = false;
        this.isLoadingResourse = false;
        this.timestamp = new Date();
        this.fileUploadSuccess = false;
        this.fileUploadedLength = 0;
        this.fileUploadProgrss = 0.0f;
        this.failcount = 0;
        this.layout = 0;
        this.imageWidth = 0;
        this.imageHeight = 0;
        this.receive = false;
        this.tempFile = null;
        this.action = null;
        this.distance = -1.0f;
        this.distanceTime = null;
        this.username = PoiTypeDef.All;
        this.emoteText = new ao();
        this.msgId = str;
    }

    public Message(boolean z) {
        this.isSayhi = false;
        this.chatType = 1;
        this.groupId = null;
        this.group = null;
        this.discussId = null;
        this.discuss = null;
        this.bubbleStyle = 0;
        this.contentType = 0;
        this.status = 1;
        this.isAudioPlayed = false;
        this.isLoadingResourse = false;
        this.timestamp = new Date();
        this.fileUploadSuccess = false;
        this.fileUploadedLength = 0;
        this.fileUploadProgrss = 0.0f;
        this.failcount = 0;
        this.layout = 0;
        this.imageWidth = 0;
        this.imageHeight = 0;
        this.receive = false;
        this.tempFile = null;
        this.action = null;
        this.distance = -1.0f;
        this.distanceTime = null;
        this.username = PoiTypeDef.All;
        this.emoteText = new ao();
        this.receive = z;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.service.bean.Message.<init>(int, boolean):void
     arg types: [int, int]
     candidates:
      com.immomo.momo.service.bean.Message.<init>(boolean, int):void
      com.immomo.momo.service.bean.Message.<init>(int, boolean):void */
    public Message(boolean z, int i) {
        this(i, false);
        if (z) {
            createMessageID();
        }
    }

    public void createMessageID() {
        this.msgId = b.a();
    }

    public boolean equals(Object obj) {
        return this.msgId.equals(((Message) obj).msgId);
    }

    public void fillRemoteUser(bf bfVar) {
        this.remoteUser = bfVar;
        this.remoteId = bfVar.h;
    }

    public String getAction() {
        return this.action != null ? this.action.toString() : PoiTypeDef.All;
    }

    public int getAudioPlayed() {
        return this.isAudioPlayed ? 1 : 0;
    }

    public int getAudiotime() {
        return this.audiotime;
    }

    public int getBubbleStyle() {
        return this.bubbleStyle;
    }

    public String getContent() {
        return this.content;
    }

    public String getDbConverLocationJson() {
        if (this.contentType == 2) {
            try {
                JSONObject jSONObject = new JSONObject();
                jSONObject.put("lat", this.convertLat);
                jSONObject.put("lng", this.convertLng);
                jSONObject.put("acc", (double) this.convertAcc);
                return Codec.c(jSONObject.toString());
            } catch (Exception e) {
            }
        }
        return PoiTypeDef.All;
    }

    public String getDbLocationjson() {
        if (this.contentType == 2) {
            try {
                JSONObject jSONObject = new JSONObject();
                jSONObject.put("lat", this.lat);
                jSONObject.put("lng", this.lng);
                jSONObject.put("acc", (double) this.acc);
                return Codec.c(jSONObject.toString());
            } catch (Exception e) {
            }
        }
        return PoiTypeDef.All;
    }

    public float getDiatance() {
        return this.distance;
    }

    public long getDistanceTime() {
        if (this.distanceTime != null) {
            return this.distanceTime.getTime();
        }
        return 0;
    }

    public ao getEmoteContent() {
        return this.emoteText;
    }

    public String getExpandedName() {
        return this.expandedName;
    }

    public int getFailcount() {
        return this.failcount;
    }

    public String getFileName() {
        return this.fileName;
    }

    public long getFileSize() {
        return this.fileSize;
    }

    public Float getFileUploadProgrss() {
        return Float.valueOf(this.fileUploadProgrss);
    }

    public int getFileUploadSuccess() {
        return this.fileUploadSuccess ? 1 : 0;
    }

    public int getImageHeight() {
        return this.imageHeight;
    }

    public long getImageUploadedLength() {
        return this.fileUploadedLength;
    }

    public int getImageWidth() {
        return this.imageWidth;
    }

    public int getLayout() {
        return this.layout;
    }

    public int getSayhi() {
        return this.isSayhi ? 1 : 0;
    }

    public String getSource() {
        return this.source;
    }

    public String getTailAction() {
        return this.tailAction;
    }

    public String getTailIcon() {
        return this.tailIcon;
    }

    public String getTailTitle() {
        return this.tailTitle;
    }

    public boolean parseDbConverLocationJson(String str) {
        if (!android.support.v4.b.a.a((CharSequence) str) && this.contentType == 2) {
            try {
                JSONObject jSONObject = new JSONObject(Codec.b(str));
                this.convertLat = jSONObject.getDouble("lat");
                this.convertLng = jSONObject.getDouble("lng");
                this.convertAcc = Float.parseFloat(jSONObject.get("acc").toString());
                return true;
            } catch (Exception e) {
            }
        }
        return false;
    }

    public boolean parseDbLocationJson(String str) {
        if (!android.support.v4.b.a.a((CharSequence) str) && this.contentType == 2) {
            try {
                JSONObject jSONObject = new JSONObject(Codec.b(str));
                this.lat = jSONObject.getDouble("lat");
                this.lng = jSONObject.getDouble("lng");
                this.acc = Double.valueOf(jSONObject.getDouble("acc")).floatValue();
                return true;
            } catch (Exception e) {
            }
        }
        return false;
    }

    public void setAcc(Object obj) {
        try {
            this.acc = Float.parseFloat(obj.toString());
        } catch (Exception e) {
        }
    }

    public void setAction(Object obj) {
        if (obj != null) {
            this.action = a.a(obj.toString());
        }
    }

    public void setAudioPlayed(Object obj) {
        boolean z = true;
        if (Integer.valueOf(Integer.parseInt(obj.toString())).intValue() != 1) {
            z = false;
        }
        this.isAudioPlayed = z;
    }

    public void setAudiotime(Object obj) {
        this.audiotime = Integer.valueOf(obj.toString()).intValue();
    }

    public void setBubbleStyle(Object obj) {
        this.bubbleStyle = Integer.valueOf(obj.toString()).intValue();
    }

    public void setContent(Object obj) {
        this.content = obj.toString();
        this.emoteText.a(this.content);
    }

    public void setConvertAcc(Object obj) {
        this.convertAcc = Float.parseFloat(obj.toString());
    }

    public void setConvertLat(Object obj) {
        this.convertLat = Double.parseDouble(obj.toString());
    }

    public void setConvertLng(Object obj) {
        this.convertLng = Double.parseDouble(obj.toString());
    }

    public void setDiatance(Object obj) {
        try {
            this.distance = Float.parseFloat(obj.toString());
        } catch (Exception e) {
            this.distance = -1.0f;
        }
    }

    public void setDistanceTime(Object obj) {
        try {
            long parseLong = Long.parseLong(obj.toString());
            if (parseLong > 0) {
                this.distanceTime = new Date(parseLong);
            }
        } catch (Exception e) {
        }
    }

    public void setExpandedName(Object obj) {
        this.expandedName = obj.toString();
    }

    public void setFailcount(Object obj) {
        this.failcount = Integer.valueOf(obj.toString()).intValue();
    }

    public void setFileName(Object obj) {
        this.fileName = obj.toString();
    }

    public void setFileSize(Object obj) {
        this.fileSize = Long.valueOf(obj.toString()).longValue();
    }

    public void setFileUploadProgrss(Object obj) {
        this.fileUploadProgrss = Float.parseFloat(obj.toString());
    }

    public void setFileUploadSuccess(Object obj) {
        boolean z = true;
        if (Integer.valueOf(Integer.parseInt(obj.toString())).intValue() != 1) {
            z = false;
        }
        this.fileUploadSuccess = z;
    }

    public void setImageHeight(Object obj) {
        this.imageHeight = Integer.valueOf(obj.toString()).intValue();
    }

    public void setImageUploadedLength(Object obj) {
        this.fileUploadedLength = Long.valueOf(obj.toString()).longValue();
    }

    public void setImageWidth(Object obj) {
        this.imageWidth = Integer.valueOf(obj.toString()).intValue();
    }

    public void setLat(Object obj) {
        try {
            this.lat = Double.parseDouble(obj.toString());
        } catch (Exception e) {
        }
    }

    public void setLayout(Object obj) {
        this.layout = Integer.valueOf(obj.toString()).intValue();
    }

    public void setLng(Object obj) {
        try {
            this.lng = Double.parseDouble(obj.toString());
        } catch (Exception e) {
        }
    }

    public void setSayhi(Object obj) {
        boolean z = true;
        if (Integer.valueOf(Integer.parseInt(obj.toString())).intValue() != 1) {
            z = false;
        }
        this.isSayhi = z;
    }

    public void setSource(Object obj) {
        this.source = obj.toString();
    }

    public void setTailAction(Object obj) {
        this.tailAction = obj.toString();
    }

    public void setTailIcon(Object obj) {
        this.tailIcon = obj.toString();
    }

    public void setTailTitle(Object obj) {
        this.tailTitle = obj.toString();
    }

    public String toString() {
        return "Message [msgId=" + this.msgId + ", remoteId=" + this.remoteId + ", msginfo=" + this.msginfo + ", content=" + this.content + ", actions=" + getAction() + ", action=" + this.action + ", type=" + this.contentType + ", status=" + this.status + ", diatance=" + this.distance + "]";
    }
}
