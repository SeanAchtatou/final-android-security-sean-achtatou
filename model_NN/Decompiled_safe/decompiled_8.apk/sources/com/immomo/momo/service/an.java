package com.immomo.momo.service;

import android.database.sqlite.SQLiteDatabase;
import com.immomo.momo.g;
import com.immomo.momo.service.a.av;
import com.immomo.momo.service.bean.ba;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public final class an extends b {
    private av c = null;

    public an() {
        this.f2968a = g.d().g();
        SQLiteDatabase sQLiteDatabase = this.f2968a;
        this.c = new av();
    }

    public final ba a(Date date) {
        String sb = new StringBuilder(String.valueOf(date.getTime())).toString();
        List c2 = this.c.c("endtime>? and starttime<=? order by field1 desc", new String[]{sb, sb});
        if (c2 == null || c2.size() <= 0) {
            return null;
        }
        return (ba) c2.get(0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.m.a(java.lang.String, java.lang.Throwable):void
     arg types: [java.lang.String, java.lang.Exception]
     candidates:
      com.immomo.momo.util.m.a(java.lang.Appendable, java.lang.Throwable):void
      com.immomo.momo.util.m.a(java.lang.Object, java.lang.Throwable):void
      com.immomo.momo.util.m.a(java.lang.String, java.io.File):void
      com.immomo.momo.util.m.a(java.lang.String, java.lang.Throwable):void */
    public final void a(List list) {
        try {
            this.f2968a.beginTransaction();
            this.c.c();
            Iterator it = list.iterator();
            while (it.hasNext()) {
                this.c.a((ba) it.next());
            }
            this.f2968a.setTransactionSuccessful();
        } catch (Exception e) {
            this.b.a("splashscreen save failed, " + list, (Throwable) e);
        } finally {
            this.f2968a.endTransaction();
        }
    }

    public final List c() {
        return this.c.b();
    }
}
