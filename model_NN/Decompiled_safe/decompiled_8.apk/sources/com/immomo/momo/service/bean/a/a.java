package com.immomo.momo.service.bean.a;

import com.immomo.momo.R;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.al;
import java.util.Date;
import java.util.List;

public final class a extends al {
    public int A = 0;
    public int B = 0;
    public boolean C = false;
    public boolean D = false;
    public String E;
    public String[] F = null;
    public int G = 2;
    public List H = null;
    public String[] I = null;
    public String J;
    public String K;
    public int L = 1;
    public List M = null;
    public List N = null;
    public String O;
    private float P = -1.0f;

    /* renamed from: a  reason: collision with root package name */
    public int f2970a = -1;
    public String b;
    public String c;
    public Date d;
    public Date e;
    public String f;
    public String g;
    public String h;
    public String i;
    public int j;
    public int k;
    public double l;
    public double m;
    public int n = 0;
    public String o;
    public String p;
    public int q = 1;
    public boolean r;
    public boolean s;
    public boolean t;
    public boolean u;
    public long v;
    public long w;
    public boolean x = false;
    public int y = 0;
    public long z = 0;

    public a() {
    }

    public a(String str) {
        this.b = str;
    }

    public static int a(int i2, boolean z2) {
        if (z2) {
            if (i2 == 1) {
                return R.drawable.ic_group_vip_level1;
            }
            if (i2 == 2) {
                return R.drawable.ic_group_vip_level2;
            }
            if (i2 >= 3) {
                return R.drawable.ic_group_vip_level3;
            }
            return -1;
        } else if (i2 == 1) {
            return R.drawable.ic_group_level1;
        } else {
            if (i2 == 2) {
                return R.drawable.ic_group_level2;
            }
            if (i2 >= 3) {
                return R.drawable.ic_group_level3;
            }
            return -1;
        }
    }

    public final void a(float f2) {
        this.P = f2;
        if (f2 == -1.0f) {
            this.p = g.a((int) R.string.profile_distance_unknown);
        } else if (f2 == -2.0f) {
            this.p = g.a((int) R.string.profile_distance_hide);
        } else {
            this.p = String.valueOf(android.support.v4.b.a.a(f2 / 1000.0f)) + "km";
        }
    }

    public final boolean a() {
        return this.B > 0;
    }

    public final float b() {
        return this.P;
    }

    public final boolean c() {
        return this.y == 1;
    }

    public final String d() {
        return android.support.v4.b.a.f(this.c) ? this.c : this.b;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        a aVar = (a) obj;
        return this.b == null ? aVar.b == null : this.b.equals(aVar.b);
    }

    public final String getLoadImageId() {
        if (this.F == null || this.F.length <= 0) {
            return null;
        }
        return this.F[0];
    }

    public final int hashCode() {
        return (this.b == null ? 0 : this.b.hashCode()) + 31;
    }

    public final String toString() {
        return "Group [gid=" + this.b + ", name=" + this.c + ", activeday=" + this.w + ", maxday=" + this.v + ", isUpgrade=" + this.x + ", rloe=" + this.n + ", siteId=" + this.J + ", siteName=" + this.K + ", status=" + this.G + ", siteType=" + this.L + ", sign=" + this.h + "]";
    }
}
