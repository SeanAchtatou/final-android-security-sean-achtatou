package com.immomo.momo.service.bean.c;

import android.support.v4.b.a;
import java.util.Date;

public final class e {

    /* renamed from: a  reason: collision with root package name */
    public String f3020a;
    public String b;
    public String c;
    public String d;
    public String e;
    public String f;
    public int g;
    public String h;
    public int i;
    public String j;
    public String k;
    public int l;
    public boolean m;
    public boolean n;
    public String o;
    private long p;
    private String q = "未知";

    public final String a() {
        return this.q;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.b.a.a(java.util.Date, boolean):java.lang.String
     arg types: [java.util.Date, int]
     candidates:
      android.support.v4.b.a.a(android.graphics.Bitmap, float):android.graphics.Bitmap
      android.support.v4.b.a.a(android.graphics.Bitmap, java.io.File):android.graphics.Bitmap
      android.support.v4.b.a.a(int, int):java.lang.String
      android.support.v4.b.a.a(java.util.Collection, java.lang.String):java.lang.String
      android.support.v4.b.a.a(java.lang.Object[], java.lang.String):java.lang.String
      android.support.v4.b.a.a(java.lang.String, java.lang.String):org.json.JSONObject
      android.support.v4.b.a.a(float, int[]):void
      android.support.v4.b.a.a(android.app.Activity, int):void
      android.support.v4.b.a.a(android.view.View, java.lang.Runnable):void
      android.support.v4.b.a.a(java.io.File, java.io.File):void
      android.support.v4.b.a.a(java.io.File, java.util.zip.ZipInputStream):void
      android.support.v4.b.a.a(java.lang.Object, java.lang.StringBuilder):void
      android.support.v4.b.a.a(android.content.Context, java.lang.String):boolean
      android.support.v4.b.a.a(com.immomo.a.a.d.b, com.immomo.momo.service.bean.Message):boolean
      android.support.v4.b.a.a(byte[], java.lang.String):byte[]
      android.support.v4.b.a.a(byte[], byte[]):byte[]
      android.support.v4.b.a.a(int[], boolean):byte[]
      android.support.v4.b.a.a(byte[], boolean):int[]
      android.support.v4.b.a.a(int, float):void
      android.support.v4.b.a.a(java.lang.Object, com.immomo.a.a.f):void
      android.support.v4.view.bf.a(int, float):void
      com.immomo.a.a.f.a(java.lang.Object, com.immomo.a.a.f):void
      android.support.v4.b.a.a(java.util.Date, boolean):java.lang.String */
    public final void a(long j2) {
        this.p = j2;
        if (j2 > 0) {
            this.q = a.a(new Date(j2), false);
        } else {
            this.q = "未知";
        }
    }

    public final long b() {
        return this.p;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        e eVar = (e) obj;
        if (this.f == null) {
            if (eVar.f != null) {
                return false;
            }
        } else if (!this.f.equals(eVar.f)) {
            return false;
        }
        if (this.o == null) {
            if (eVar.o != null) {
                return false;
            }
        } else if (!this.o.equals(eVar.o)) {
            return false;
        }
        return this.e == null ? eVar.e == null : this.e.equals(eVar.e);
    }

    public final int hashCode() {
        int i2 = 0;
        int hashCode = ((this.o == null ? 0 : this.o.hashCode()) + (((this.f == null ? 0 : this.f.hashCode()) + 31) * 31)) * 31;
        if (this.e != null) {
            i2 = this.e.hashCode();
        }
        return hashCode + i2;
    }
}
