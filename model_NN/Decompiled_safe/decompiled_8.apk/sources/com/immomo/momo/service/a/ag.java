package com.immomo.momo.service.a;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.immomo.momo.service.bean.Message;
import com.immomo.momo.util.a.a;
import org.json.JSONException;

public final class ag extends b {
    public ag(SQLiteDatabase sQLiteDatabase) {
        super(sQLiteDatabase, "messages");
    }

    private static void a(Message message, Cursor cursor) {
        int i = cursor.getInt(cursor.getColumnIndex(Message.DBFIELD_TYPE));
        boolean z = cursor.getInt(cursor.getColumnIndex(Message.DBFIELD_RECEIVE)) == 1;
        message.contentType = i;
        message.receive = z;
        message.id = cursor.getInt(cursor.getColumnIndex(Message.DBFIELD_ID));
        message.msgId = cursor.getString(cursor.getColumnIndex(Message.DBFIELD_MSGID));
        message.msginfo = cursor.getString(cursor.getColumnIndex(Message.DBFIELD_MSMGINFO));
        message.failcount = cursor.getInt(cursor.getColumnIndex(Message.DBFIELD_FAILCOUNT));
        message.remoteId = cursor.getString(cursor.getColumnIndex(Message.DBFIELD_REMOTEID));
        message.status = cursor.getInt(cursor.getColumnIndex(Message.DBFIELD_STATUS));
        message.timestamp = a(cursor.getLong(cursor.getColumnIndex(Message.DBFIELD_TIME)));
        message.chatType = 1;
        try {
            a.a(message.msginfo, message);
        } catch (JSONException e) {
        }
        if (i == 2) {
            message.parseDbLocationJson(cursor.getString(cursor.getColumnIndex(Message.DBFIELD_LOCATIONJSON)));
            message.parseDbConverLocationJson(cursor.getString(cursor.getColumnIndex(Message.DBFIELD_CONVERLOCATIONJSON)));
        }
    }

    private void a(String str, Message message) {
        int i = 1;
        Object[] objArr = new Object[10];
        objArr[0] = Integer.valueOf(message.failcount);
        objArr[1] = message.msginfo;
        objArr[2] = message.remoteId;
        objArr[3] = Integer.valueOf(message.status);
        objArr[4] = Long.valueOf(a(message.timestamp));
        if (!message.receive) {
            i = 0;
        }
        objArr[5] = Integer.valueOf(i);
        objArr[6] = Integer.valueOf(message.contentType);
        objArr[7] = message.getDbLocationjson();
        objArr[8] = message.getDbConverLocationJson();
        objArr[9] = message.msgId;
        a(str, objArr);
    }

    private static Message b(Cursor cursor) {
        Message message = new Message();
        a(message, cursor);
        return message;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Cursor cursor) {
        return b(cursor);
    }

    public final void a(Message message) {
        StringBuilder sb = new StringBuilder();
        sb.append("insert into " + this.f2954a + " (").append("m_failcount, m_msginfo, m_remoteid, m_status, m_time, m_receive, m_type, field2, field3, m_msgid) values(?,?,?,?,?,?,?,?, ?, ?)");
        a(sb.toString(), message);
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void a(Object obj, Cursor cursor) {
        a((Message) obj, cursor);
    }

    public final void b(Message message) {
        StringBuilder sb = new StringBuilder();
        sb.append("update " + this.f2954a + " set ").append("m_failcount=?, m_msginfo=?, m_remoteid=?, m_status=?, m_time=?, m_receive=?, m_type=?, field2=?, field3=? where m_msgid=?");
        a(sb.toString(), message);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.immomo.momo.service.a.ag.a(com.immomo.momo.service.bean.Message, android.database.Cursor):void
      com.immomo.momo.service.a.ag.a(java.lang.String, com.immomo.momo.service.bean.Message):void
      com.immomo.momo.service.a.ag.a(java.lang.Object, android.database.Cursor):void
      com.immomo.momo.service.a.b.a(android.database.Cursor, java.lang.String):int
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.String[]):android.database.Cursor
      com.immomo.momo.service.a.b.a(java.lang.String[], java.lang.String[]):java.util.List
      com.immomo.momo.service.a.b.a(java.lang.Object, android.database.Cursor):void
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object[]):void
      com.immomo.momo.service.a.b.a(java.lang.String[], java.lang.Object[]):void
      com.immomo.momo.service.a.b.a(java.lang.Object, java.io.Serializable):boolean
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.String):boolean
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object):void */
    public final void c(Message message) {
        a(Message.DBFIELD_MSGID, (Object) message.msgId);
    }
}
