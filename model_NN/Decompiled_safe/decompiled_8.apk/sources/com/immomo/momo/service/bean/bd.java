package com.immomo.momo.service.bean;

import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.service.o;
import com.immomo.momo.util.s;
import com.immomo.momo.util.u;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class bd extends s {

    /* renamed from: a  reason: collision with root package name */
    public static boolean f3009a = false;
    /* access modifiers changed from: private */
    public o b;
    private List c;

    private bd() {
        this.b = null;
        this.b = new o();
        ArrayList arrayList = new ArrayList(40);
        this.b.k(arrayList);
        for (int size = arrayList.size() - 1; size >= 0; size--) {
            a((r) arrayList.get(size), this);
        }
        this.c = arrayList;
        f3009a = true;
    }

    public static bd b() {
        if (u.c(bd.class.getName())) {
            return (bd) u.b(bd.class.getName());
        }
        bd bdVar = new bd();
        u.a(bd.class.getName(), bdVar);
        return bdVar;
    }

    public final void a() {
        if (d() > 0) {
            a(false);
            a(-1);
            this.c.clear();
            this.b.k(this.c);
            for (int size = this.c.size() - 1; size >= 0; size--) {
                a((r) this.c.get(size), this);
            }
        }
    }

    public final void a(r rVar) {
        r g = rVar.g();
        g.a(PoiTypeDef.All);
        if (this.c.size() < 40 && a((Object) g) == null) {
            this.c.add(0, g);
        }
        a(g, this);
    }

    public final synchronized void a(boolean z) {
        this.c.clear();
        for (Map.Entry key : e().entrySet()) {
            this.c.add((r) key.getKey());
        }
        Collections.reverse(this.c);
        if (z) {
            com.immomo.momo.android.c.u.b().execute(new be(this, new ArrayList(this.c)));
        } else {
            this.b.j(this.c);
        }
    }

    public final synchronized List c() {
        return this.c;
    }
}
