package com.immomo.momo.service;

import java.util.Date;

public final class m {

    /* renamed from: a  reason: collision with root package name */
    public String f3050a;
    public int b;
    public int c = 1;
    public Date d;
    public String e;
    public int f;
    public String g;

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        return this.b == ((m) obj).b;
    }

    public final int hashCode() {
        return this.b + 31;
    }
}
