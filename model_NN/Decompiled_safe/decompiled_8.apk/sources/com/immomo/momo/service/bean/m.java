package com.immomo.momo.service.bean;

public final class m {

    /* renamed from: a  reason: collision with root package name */
    public int f3031a;
    public String b;
    public Object c;
    public boolean d;
    public String e;
    public String f;
    public long g;
    public boolean h;
    private int i;
    private int j;

    public m(int i2) {
        this.f3031a = i2;
        switch (i2) {
            case 1:
                this.b = "附近群组";
                return;
            case 2:
                this.b = "留言板";
                return;
            case 3:
                this.b = "附近活动";
                return;
            case 4:
            default:
                return;
            case 5:
                this.b = "陌陌吧";
                return;
            case 6:
                this.b = "地点漫游";
                return;
            case 7:
                this.b = "陌陌表情";
                return;
        }
    }

    public final int a() {
        return this.i;
    }

    public final void a(int i2, boolean z) {
        if (z) {
            this.j = i2;
            this.i = 0;
            return;
        }
        this.i = i2;
        this.j = 0;
    }

    public final boolean b() {
        return this.j > 0;
    }
}
