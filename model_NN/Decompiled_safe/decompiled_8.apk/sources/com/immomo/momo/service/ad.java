package com.immomo.momo.service;

import com.immomo.momo.b;
import com.immomo.momo.g;
import com.immomo.momo.service.a.ae;
import com.immomo.momo.service.bean.Message;
import com.immomo.momo.service.bean.am;
import java.util.Iterator;
import java.util.List;

public final class ad extends b {
    private ae c;

    public ad() {
        this.c = null;
        this.c = new ae(g.d().g());
    }

    public final void a(List list) {
        try {
            this.c.a().beginTransaction();
            this.c.c();
            Iterator it = list.iterator();
            while (it.hasNext()) {
                this.c.a((am) it.next());
            }
            b.a(list);
            this.c.a().setTransactionSuccessful();
        } catch (Exception e) {
            this.b.a((Throwable) e);
        } finally {
            this.c.a().endTransaction();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.service.a.b.a(java.lang.String[], java.lang.String[], java.lang.String, boolean):java.util.List
     arg types: [java.lang.String[], java.lang.String[], java.lang.String, int]
     candidates:
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.String[], java.lang.String[], java.lang.String[]):int
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.String, java.lang.String[], java.lang.String[]):java.lang.String
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object, java.lang.String, java.lang.Object[]):void
      com.immomo.momo.service.a.b.a(java.lang.String[], java.lang.Object[], java.lang.String[], java.lang.Object[]):void
      com.immomo.momo.service.a.b.a(java.lang.String[], java.lang.String[], java.lang.String, boolean):java.util.List */
    public final List c() {
        return this.c.a(new String[0], new String[0], Message.DBFIELD_ID, true);
    }
}
