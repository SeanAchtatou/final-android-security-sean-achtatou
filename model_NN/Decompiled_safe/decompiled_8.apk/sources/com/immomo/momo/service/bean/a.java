package com.immomo.momo.service.bean;

public final class a {

    /* renamed from: a  reason: collision with root package name */
    public String f2969a;
    private String b;
    private String c;

    public static a a(String str) {
        if (!android.support.v4.b.a.f(str)) {
            return null;
        }
        a aVar = new a();
        StringBuilder sb = new StringBuilder(str);
        sb.deleteCharAt(0);
        sb.deleteCharAt(sb.length() - 1);
        String[] split = sb.toString().split("\\|");
        if (split.length <= 0) {
            return aVar;
        }
        aVar.f2969a = split[0];
        if (split.length <= 1) {
            return aVar;
        }
        aVar.b = split[1];
        if (split.length <= 2) {
            return aVar;
        }
        aVar.c = split[2];
        return aVar;
    }

    public final String toString() {
        return "[" + this.f2969a + "|" + this.b + "|" + this.c + "]";
    }
}
