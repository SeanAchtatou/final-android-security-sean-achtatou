package com.immomo.momo.service.a;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.service.bean.Message;
import com.immomo.momo.service.bean.d;
import java.io.File;

public final class a extends b {
    public a(SQLiteDatabase sQLiteDatabase) {
        super(sQLiteDatabase, "banners", Message.DBFIELD_SAYHI);
    }

    private static void a(d dVar, Cursor cursor) {
        boolean z = true;
        dVar.f3022a = cursor.getString(cursor.getColumnIndex(Message.DBFIELD_SAYHI));
        dVar.c = cursor.getInt(cursor.getColumnIndex(Message.DBFIELD_GROUPID));
        dVar.f = a(cursor.getLong(cursor.getColumnIndex("field6")));
        dVar.d = cursor.getString(cursor.getColumnIndex("field7"));
        String string = cursor.getString(cursor.getColumnIndex("field8"));
        if (!android.support.v4.b.a.a((CharSequence) string)) {
            dVar.i = new File(string);
        }
        dVar.b = cursor.getInt(cursor.getColumnIndex(Message.DBFIELD_CONVERLOCATIONJSON));
        dVar.h = cursor.getInt(cursor.getColumnIndex(Message.DBFIELD_LOCATIONJSON));
        dVar.e = a(cursor.getLong(cursor.getColumnIndex("field5")));
        dVar.g = cursor.getString(cursor.getColumnIndex("field9"));
        dVar.j = cursor.getInt(cursor.getColumnIndex("field10")) == 1;
        if (cursor.getInt(cursor.getColumnIndex("field11")) != 1) {
            z = false;
        }
        dVar.k = z;
        dVar.l = a(cursor.getLong(cursor.getColumnIndex("field12")));
        dVar.n = c(cursor, "field14");
        dVar.m = c(cursor, "field13");
        dVar.o = e(cursor, "field15");
        dVar.p = b(cursor, "field16");
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Cursor cursor) {
        d dVar = new d();
        a(dVar, cursor);
        return dVar;
    }

    public final void a(d dVar) {
        String[] strArr = {Message.DBFIELD_SAYHI, Message.DBFIELD_GROUPID, "field6", "field7", "field8", Message.DBFIELD_CONVERLOCATIONJSON, Message.DBFIELD_LOCATIONJSON, "field5", "field9", "field10", "field11", "field14", "field13", "field15", "field16"};
        Object[] objArr = new Object[15];
        objArr[0] = dVar.f3022a;
        objArr[1] = Integer.valueOf(dVar.c);
        objArr[2] = dVar.f;
        objArr[3] = dVar.d;
        objArr[4] = dVar.i != null ? dVar.i.getPath() : PoiTypeDef.All;
        objArr[5] = Integer.valueOf(dVar.b);
        objArr[6] = Integer.valueOf(dVar.h);
        objArr[7] = dVar.e;
        objArr[8] = dVar.g;
        objArr[9] = Boolean.valueOf(dVar.j);
        objArr[10] = Boolean.valueOf(dVar.k);
        objArr[11] = dVar.n;
        objArr[12] = dVar.m;
        objArr[13] = Boolean.valueOf(dVar.o);
        objArr[14] = Long.valueOf(dVar.p);
        b(strArr, objArr);
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void a(Object obj, Cursor cursor) {
        a((d) obj, cursor);
    }
}
