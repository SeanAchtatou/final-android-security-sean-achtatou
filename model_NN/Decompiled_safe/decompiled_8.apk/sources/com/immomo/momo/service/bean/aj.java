package com.immomo.momo.service.bean;

import com.immomo.momo.android.c.g;

public interface aj {
    g getImageCallback();

    String getLoadImageId();

    boolean isImageLoading();

    boolean isImageLoadingFailed();

    boolean isImageMultipleDiaplay();

    boolean isImageUrl();

    void setImageCallback(g gVar);

    void setImageLoadFailed(boolean z);

    void setImageLoading(boolean z);
}
