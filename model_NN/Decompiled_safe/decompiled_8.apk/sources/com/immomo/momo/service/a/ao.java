package com.immomo.momo.service.a;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.immomo.momo.service.bean.am;

public final class ao extends SQLiteOpenHelper {
    public ao(Context context) {
        super(context, "cache", (SQLiteDatabase.CursorFactory) null, 6);
    }

    private static String a(String str) {
        int i = 1;
        while (true) {
            int i2 = i;
            if (i2 >= 10) {
                return String.valueOf(str) + ")";
            }
            String str2 = "field" + i2 + " NUMERIC";
            if (i2 < 9) {
                str2 = String.valueOf(str2) + ",";
            }
            str = String.valueOf(str) + str2;
            i = i2 + 1;
        }
    }

    private static void a(SQLiteDatabase sQLiteDatabase, am amVar) {
        sQLiteDatabase.execSQL("insert into industry (field1, field3, field2, field4) values(" + "?,?,?,?)", new Object[]{amVar.f2991a, amVar.c, amVar.b, amVar.d});
    }

    public final void onCreate(SQLiteDatabase sQLiteDatabase) {
        int i = 0;
        sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS imagecache(i_imageid VARCHAR(100) primary key, i_path VARCHAR(200), i_maxday INTEGER, i_type VARCHAR(10), i_time VARCHAR(50))");
        sQLiteDatabase.execSQL(a("CREATE TABLE IF NOT EXISTS splashscreen2(_id INTEGER primary key autoincrement,ft_url VARCHAR(200),bg_url VARCHAR(200),cr_url VARCHAR(200),starttime VARCHAR(50),endtime VARCHAR(50), "));
        sQLiteDatabase.execSQL(a("CREATE TABLE IF NOT EXISTS industry(_id INTEGER primary key autoincrement,"));
        Cursor rawQuery = sQLiteDatabase.rawQuery("select count(*) from industry", new String[0]);
        if (rawQuery.moveToFirst()) {
            i = rawQuery.getInt(0);
        }
        if (i <= 0) {
            a(sQLiteDatabase, new am("I1", "计算机/互联网/通信", "IT", "I1.png"));
            a(sQLiteDatabase, new am("I2", "生产/工艺/制造", "工", "I2.png"));
            a(sQLiteDatabase, new am("I3", "商业/服务业/个体经营", "商", "I3.png"));
            a(sQLiteDatabase, new am("I4", "金融/银行/投资/保险", "金", "I4.png"));
            a(sQLiteDatabase, new am("I5", "文化/广告/传媒", "文", "I5.png"));
            a(sQLiteDatabase, new am("I6", "娱乐/艺术/表演", "艺", "I6.png"));
            a(sQLiteDatabase, new am("I7", "医疗/护理/制药", "医", "I7.png"));
            a(sQLiteDatabase, new am("I8", "律师/法务", "法", "I8.png"));
            a(sQLiteDatabase, new am("I9", "教育/培训", "教", "I9.png"));
            a(sQLiteDatabase, new am("I10", "公务员/事业单位", "政", "I10.png"));
            a(sQLiteDatabase, new am("I11", "学生", "学", "I11.png"));
            rawQuery.close();
        }
    }

    public final void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        onCreate(sQLiteDatabase);
    }
}
