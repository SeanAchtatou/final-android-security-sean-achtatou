package com.immomo.momo.service;

import android.os.Bundle;
import android.support.v4.b.a;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.g;
import com.immomo.momo.service.a.ak;
import com.immomo.momo.service.a.b;
import com.immomo.momo.service.a.ba;
import com.immomo.momo.service.a.c;
import com.immomo.momo.service.a.d;
import com.immomo.momo.service.a.e;
import com.immomo.momo.service.a.n;
import com.immomo.momo.service.a.r;
import com.immomo.momo.service.bean.Message;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.util.h;
import com.immomo.momo.util.m;
import com.immomo.momo.util.u;
import java.io.File;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;

public final class aq extends b {
    private ba c;
    private e d;
    private b e;
    private b f;
    private b g;
    private b h;
    private b i;
    private ag j;

    public aq() {
        this(null);
    }

    public aq(String str) {
        this.c = null;
        this.d = null;
        this.e = null;
        this.f = null;
        this.g = null;
        this.h = null;
        this.i = null;
        this.j = null;
        this.b = new m(getClass().getName());
        if (str == null) {
            this.f2968a = g.d().e();
        } else {
            this.f2968a = new com.immomo.momo.service.a.g(g.c(), str).getWritableDatabase();
        }
        this.c = new ba(this.f2968a);
        this.e = new r(this.f2968a);
        this.f = new c(this.f2968a);
        this.g = new n(this.f2968a);
        this.i = new ak(this.f2968a);
        this.h = new d(this.f2968a);
        this.d = new e(this.f2968a);
        this.j = new ag(this.f2968a);
    }

    private void a(String str, String str2, Object obj) {
        this.c.a(str2, obj, str);
    }

    private void a(String str, Date date) {
        if (k(str)) {
            this.b.c("addFriend failed. this user is exisit");
            return;
        }
        HashMap hashMap = new HashMap();
        hashMap.put(str, date);
        this.e.a((Object) hashMap);
        if (this.j.c(str)) {
            Bundle bundle = new Bundle();
            bundle.putString("sessionid", str);
            bundle.putInt("sessiontype", 0);
            g.d().a(bundle, "action.sessionchanged");
        }
    }

    private static void a(List list, String str) {
        if (!PoiTypeDef.All.equals(str) && list.size() > 1) {
            Iterator it = list.iterator();
            while (it.hasNext()) {
                bf bfVar = (bf) it.next();
                String p = a.p(bfVar.i);
                String p2 = a.p(bfVar.l);
                String str2 = bfVar.j;
                String str3 = bfVar.k;
                String str4 = bfVar.m;
                String str5 = bfVar.n;
                String str6 = bfVar.h;
                int i2 = bfVar.t;
                if (str.equals(p2)) {
                    i2 += 100;
                } else if (str.equals(p)) {
                    i2 += 99;
                } else if (str.equals(str4)) {
                    i2 += 98;
                } else if (str.equals(str2)) {
                    i2 += 97;
                } else if (str.equals(str5)) {
                    i2 += 96;
                } else if (str.equals(str3)) {
                    i2 += 95;
                } else if (str.equals(str6)) {
                    i2 += 94;
                } else if (a.d(p2, str)) {
                    i2 += 90;
                } else if (a.d(p, str)) {
                    i2 += 89;
                } else if (a.d(str4, str)) {
                    i2 += 88;
                } else if (a.d(str2, str)) {
                    i2 += 87;
                } else if (a.d(str5, str)) {
                    i2 += 86;
                } else if (a.d(str3, str)) {
                    i2 += 85;
                } else if (a.d(str6, str)) {
                    i2 += 84;
                } else {
                    if (a.c(p2, str)) {
                        i2 += 10;
                    }
                    if (a.c(p, str)) {
                        i2 += 9;
                    }
                    if (a.c(str4, str)) {
                        i2 += 8;
                    }
                    if (a.c(str2, str)) {
                        i2 += 7;
                    }
                    if (a.c(str5, str)) {
                        i2 += 6;
                    }
                    if (a.c(str3, str)) {
                        i2 += 5;
                    }
                }
                bfVar.t = i2;
            }
            Collections.sort(list, new ar());
        }
    }

    private void b(String str, Date date) {
        HashMap hashMap = new HashMap();
        hashMap.put(str, date);
        if (!f(str)) {
            this.h.a((Object) hashMap);
            this.j.c(str);
        }
        u.a("userbothlist_0");
        u.a("userbothlist_1");
        u.a("userbothlist_2");
        u.a("userbothlist_3");
    }

    public static void c(int i2) {
        u.a("uservisitorcount", Integer.valueOf(i2));
        g.r().a("uservisitorcount", Integer.valueOf(i2));
    }

    public static void f() {
        if (u.c("userhidelist")) {
            u.a("userhidelist");
        }
        File file = new File(com.immomo.momo.a.u(), "hidelist");
        if (file.exists()) {
            file.delete();
        }
    }

    private void q(String str) {
        if (this.h.c(str)) {
            this.h.b((Serializable) str);
            u.a("userbothlist_0");
            u.a("userbothlist_1");
            u.a("userbothlist_2");
            u.a("userbothlist_3");
        }
    }

    public final String a(String str) {
        try {
            String[] b = a.b(this.c.c("field16", new String[]{"u_momoid"}, new String[]{str}), ",");
            if (b != null && b.length > 0) {
                return b[0];
            }
        } catch (Exception e2) {
        }
        return null;
    }

    public final List a(int i2) {
        if (u.c("userbothlist_" + i2)) {
            return (List) u.b("userbothlist_" + i2);
        }
        boolean z = (i2 == 1 || i2 == 2) ? false : true;
        StringBuilder sb = new StringBuilder();
        sb.append("select u.*, bf.f_followtime, bf.bf_momoid from bothfollow as bf LEFT OUTER JOIN ");
        sb.append("users2 as u");
        sb.append(" on bf.bf_momoid");
        sb.append(" = u.u_momoid ");
        if (i2 >= 0 && i2 <= 3) {
            sb.append("order by ");
            if (i2 == 1) {
                sb.append("u_loctime");
            } else if (i2 == 0) {
                sb.append("u_distance");
            } else if (i2 == 2) {
                sb.append("f_followtime");
            } else if (i2 == 3) {
                sb.append("field4 asc, field1 asc");
            }
            sb.append(" ");
            if (z) {
                sb.append("asc");
            } else {
                sb.append("desc");
            }
        }
        List b = this.c.b(sb.toString(), new String[0]);
        u.a("userbothlist_" + i2, b);
        return b;
    }

    public final void a() {
        if (this.f2968a != null) {
            this.f2968a.close();
        }
    }

    public final void a(int i2, int i3, String str) {
        this.c.a(new String[]{"field34", "field33"}, new Object[]{Integer.valueOf(i2), Integer.valueOf(i3)}, new String[]{"u_momoid"}, new String[]{str});
    }

    public final void a(int i2, String str) {
        this.c.a("field45", Integer.valueOf(i2), str);
    }

    public final void a(long j2, String str) {
        this.c.a("field58", Long.valueOf(j2), str);
    }

    public final void a(bf bfVar) {
        if (bfVar != null) {
            this.c.a(new String[]{Message.DBFIELD_CONVERLOCATIONJSON, "u_loctime", "field47", "field74"}, new Object[]{bfVar.g(), Long.valueOf(bfVar.a()), Integer.valueOf(bfVar.aH), Integer.valueOf(bfVar.aI)}, new String[]{"u_momoid"}, new Object[]{bfVar.h});
        }
    }

    public final void a(String str, int i2) {
        String str2;
        this.f2968a.beginTransaction();
        try {
            String c2 = this.c.c("field23", new String[]{"u_momoid"}, new String[]{str});
            if (i2 == 4) {
                if ("fans".equalsIgnoreCase(c2)) {
                    i2 = 0;
                } else if ("both".equalsIgnoreCase(c2)) {
                    i2 = 1;
                }
            }
            switch (i2) {
                case 0:
                    q(str);
                    i(str);
                    l(str);
                    str2 = "none";
                    break;
                case 1:
                    a(str, new Date());
                    q(str);
                    l(str);
                    str2 = "follow";
                    break;
                case 2:
                    i(str);
                    q(str);
                    str2 = "fans";
                    break;
                case 3:
                    a(str, new Date());
                    b(str, new Date());
                    str2 = "both";
                    break;
                default:
                    return;
            }
            d(str, str2);
            this.f2968a.setTransactionSuccessful();
            this.f2968a.endTransaction();
        } catch (Exception e2) {
            this.b.a((Throwable) e2);
        } finally {
            this.f2968a.endTransaction();
        }
    }

    public final void a(String str, String str2) {
        this.c.a("field52", str, str2);
    }

    public final void a(String str, String str2, String str3) {
        String[] strArr;
        boolean z;
        if (this.c.c(str)) {
            String[] b = a.b(this.c.c("field16", new String[]{"u_momoid"}, new String[]{str}), ",");
            if (b == null || b.length <= 0) {
                b = new String[1];
            }
            strArr = b;
            z = true;
        } else {
            strArr = new String[1];
            z = false;
        }
        strArr[0] = str2;
        if (!z) {
            bf bfVar = new bf(str);
            bfVar.ae = strArr;
            bfVar.i = str3;
            this.c.a(bfVar);
            return;
        }
        this.c.a(new String[]{"field16", "u_name"}, new Object[]{a.a(strArr, ","), str3}, new String[]{"u_momoid"}, new String[]{str});
    }

    public final void a(Date date, String str) {
        this.c.a("field59", Long.valueOf(b.a(date)), str);
    }

    public final void a(List list) {
        this.f2968a.beginTransaction();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            bf bfVar = (bf) it.next();
            if (g.q().h.equals(bfVar.h)) {
                bfVar.a(g.q().e());
            }
            d(bfVar);
        }
        this.f2968a.setTransactionSuccessful();
        this.f2968a.endTransaction();
    }

    public final boolean a(bf bfVar, String str) {
        return this.c.a(bfVar, str);
    }

    public final bf b(String str) {
        return (!"1602".equals(str) || !com.immomo.momo.protocol.imjson.util.c.c()) ? (bf) this.c.a((Serializable) str) : com.immomo.momo.protocol.imjson.util.c.a().f();
    }

    public final List b(int i2) {
        if (u.c("userfriendlist_" + i2)) {
            return (List) u.b("userfriendlist_" + i2);
        }
        boolean z = (i2 == 1 || i2 == 2) ? false : true;
        StringBuilder sb = new StringBuilder();
        sb.append("select u.*, f.f_followtime, f.f_momoid from friends as f LEFT OUTER JOIN ");
        sb.append("users2 as u");
        sb.append(" on f.f_momoid");
        sb.append(" = u.u_momoid ");
        if (i2 >= 0 && i2 <= 3) {
            sb.append("order by ");
            if (i2 == 1) {
                sb.append("u_loctime");
            } else if (i2 == 0) {
                sb.append("u_distance");
            } else if (i2 == 2) {
                sb.append("f_followtime");
            } else if (i2 == 3) {
                sb.append("field4 asc");
            }
            sb.append(" ");
            if (z) {
                sb.append("asc");
            } else {
                sb.append("desc");
            }
        }
        List b = this.c.b(sb.toString(), new String[0]);
        u.a("userfriendlist_" + i2, b);
        return b;
    }

    public final void b(int i2, String str) {
        this.c.a("field53", Integer.valueOf(i2), str);
    }

    public final void b(bf bfVar) {
        if (a.a((CharSequence) bfVar.h)) {
            new IllegalArgumentException("momoid is null");
        }
        if (this.c.c(bfVar.h)) {
            this.c.c(bfVar);
        } else {
            this.c.a(bfVar);
        }
    }

    public final void b(String str, String str2) {
        this.c.a("field55", str, str2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.m.a(java.lang.String, java.lang.Throwable):void
     arg types: [java.lang.String, java.lang.Exception]
     candidates:
      com.immomo.momo.util.m.a(java.lang.Appendable, java.lang.Throwable):void
      com.immomo.momo.util.m.a(java.lang.Object, java.lang.Throwable):void
      com.immomo.momo.util.m.a(java.lang.String, java.io.File):void
      com.immomo.momo.util.m.a(java.lang.String, java.lang.Throwable):void */
    public final void b(List list) {
        try {
            this.f2968a.beginTransaction();
            this.e.c();
            Iterator it = list.iterator();
            while (it.hasNext()) {
                bf bfVar = (bf) it.next();
                h(bfVar);
                this.c.a(bfVar);
            }
            u.a("userfriendlist_0");
            u.a("userfriendlist_1");
            u.a("userfriendlist_2");
            u.a("userfriendlist_3");
            this.f2968a.setTransactionSuccessful();
        } catch (Exception e2) {
            this.b.a("addFriend user failed, " + list, (Throwable) e2);
        } finally {
            this.f2968a.endTransaction();
        }
    }

    public final bf c(String str) {
        return (!"1602".equals(str) || !com.immomo.momo.protocol.imjson.util.c.c()) ? this.c.b(str) : com.immomo.momo.protocol.imjson.util.c.a().f();
    }

    public final List c() {
        return this.c.b("select u.*, bf.f_followtime, bf.bf_momoid from bothfollow as bf LEFT OUTER JOIN " + "users2 as u" + " on bf.bf_momoid" + " = u.u_momoid", new String[0]);
    }

    public final void c(int i2, String str) {
        this.c.a("field57", Integer.valueOf(i2), str);
    }

    public final void c(String str, String str2) {
        if (this.c.c(str2)) {
            this.c.a("field73", str, str2);
            return;
        }
        this.c.b(new String[]{"field73", "u_momoid"}, new Object[]{str, str2});
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.m.a(java.lang.String, java.lang.Throwable):void
     arg types: [java.lang.String, java.lang.Exception]
     candidates:
      com.immomo.momo.util.m.a(java.lang.Appendable, java.lang.Throwable):void
      com.immomo.momo.util.m.a(java.lang.Object, java.lang.Throwable):void
      com.immomo.momo.util.m.a(java.lang.String, java.io.File):void
      com.immomo.momo.util.m.a(java.lang.String, java.lang.Throwable):void */
    public final void c(List list) {
        try {
            this.f2968a.beginTransaction();
            this.h.c();
            Iterator it = list.iterator();
            while (it.hasNext()) {
                bf bfVar = (bf) it.next();
                i(bfVar);
                this.c.a(bfVar);
            }
            this.f2968a.setTransactionSuccessful();
        } catch (Exception e2) {
            this.b.a("addBothFollow user failed, " + list, (Throwable) e2);
        } finally {
            this.f2968a.endTransaction();
        }
    }

    public final boolean c(bf bfVar) {
        return this.c.c(bfVar.h);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object[], java.lang.String):java.util.List
     arg types: [java.lang.String, java.lang.String[], ?[OBJECT, ARRAY]]
     candidates:
      com.immomo.momo.service.a.ba.a(java.lang.String, com.immomo.momo.service.bean.bf, boolean):void
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object, java.io.Serializable):void
      com.immomo.momo.service.a.b.a(java.util.Map, java.lang.String[], java.lang.Object[]):void
      com.immomo.momo.service.a.b.a(java.lang.String[], java.lang.Object[], java.io.Serializable):void
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.String[], java.lang.String[]):java.lang.String[]
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object[], java.lang.String):java.util.List */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.m.a(java.lang.String, java.lang.Throwable):void
     arg types: [java.lang.String, java.lang.Exception]
     candidates:
      com.immomo.momo.util.m.a(java.lang.Appendable, java.lang.Throwable):void
      com.immomo.momo.util.m.a(java.lang.Object, java.lang.Throwable):void
      com.immomo.momo.util.m.a(java.lang.String, java.io.File):void
      com.immomo.momo.util.m.a(java.lang.String, java.lang.Throwable):void */
    public final List d() {
        if (u.c("uservisitorlist")) {
            return (List) u.b("uservisitorlist");
        }
        List arrayList = new ArrayList();
        try {
            File file = new File(com.immomo.momo.a.u(), "visitors");
            if (file.exists()) {
                String a2 = h.a(file);
                if (!a.a((CharSequence) a2)) {
                    JSONArray jSONArray = new JSONArray(a2);
                    ArrayList arrayList2 = new ArrayList();
                    String[] strArr = new String[jSONArray.length()];
                    for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                        strArr[i2] = jSONArray.optString(i2);
                        arrayList2.add(strArr[i2]);
                    }
                    arrayList = this.c.a("u_momoid", (Object[]) strArr, (String) null);
                    Collections.sort(arrayList, new as(arrayList2));
                }
            }
        } catch (Exception e2) {
            this.b.a("add Visitors failed, " + arrayList, (Throwable) e2);
        }
        u.a("uservisitorlist", arrayList);
        return arrayList;
    }

    public final List d(String str) {
        if (str == null) {
            str = PoiTypeDef.All;
        }
        String p = a.p(str);
        boolean d2 = com.immomo.momo.util.d.d(p);
        StringBuilder sb = new StringBuilder();
        sb.append("select u.*, f.f_followtime, f.f_momoid from users2 u, friends f where u.u_momoid = f.f_momoid").append(" and (u.field5 like '%" + p + "%'").append(" or u.field6 like '%" + p + "%'").append(" or u.u_name like '%" + p + "%'");
        if (d2) {
            sb.append(" or u.field4 like '%" + p + "%'");
        }
        if (p.length() >= 3) {
            sb.append(" or u.u_momoid like '" + p + "%'");
        }
        sb.append(")");
        List b = this.c.b(sb.toString(), new String[0]);
        a(b, p);
        return b;
    }

    public final void d(int i2, String str) {
        a(str, "field35", Integer.valueOf(i2));
    }

    public final void d(bf bfVar) {
        if (a.a((CharSequence) bfVar.h)) {
            new IllegalArgumentException("momoid is null");
        }
        this.c.a(bfVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.service.aq.a(java.lang.String, java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.String, java.lang.String]
     candidates:
      com.immomo.momo.service.aq.a(int, int, java.lang.String):void
      com.immomo.momo.service.aq.a(java.lang.String, java.lang.String, java.lang.String):void
      com.immomo.momo.service.aq.a(java.lang.String, java.lang.String, java.lang.Object):void */
    public final void d(String str, String str2) {
        a(str, "field23", (Object) str2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.m.a(java.lang.String, java.lang.Throwable):void
     arg types: [java.lang.String, java.lang.Exception]
     candidates:
      com.immomo.momo.util.m.a(java.lang.Appendable, java.lang.Throwable):void
      com.immomo.momo.util.m.a(java.lang.Object, java.lang.Throwable):void
      com.immomo.momo.util.m.a(java.lang.String, java.io.File):void
      com.immomo.momo.util.m.a(java.lang.String, java.lang.Throwable):void */
    public final void d(List list) {
        try {
            this.f2968a.beginTransaction();
            this.g.c();
            Iterator it = list.iterator();
            while (it.hasNext()) {
                bf bfVar = (bf) it.next();
                try {
                    String str = bfVar.h;
                    Date date = bfVar.aD;
                    if (o(str)) {
                        this.b.c("addFans failed. this user is exisit");
                    } else {
                        HashMap hashMap = new HashMap();
                        hashMap.put(str, date);
                        this.g.a((Object) hashMap);
                    }
                } catch (Exception e2) {
                    this.b.a("add fans failed, " + bfVar, (Throwable) e2);
                }
            }
            u.a("userfanslist", list);
            this.f2968a.setTransactionSuccessful();
        } catch (Exception e3) {
            this.b.a("add fans failed, " + list, (Throwable) e3);
        } finally {
            this.f2968a.endTransaction();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object[], java.lang.String):java.util.List
     arg types: [java.lang.String, java.lang.String[], ?[OBJECT, ARRAY]]
     candidates:
      com.immomo.momo.service.a.ba.a(java.lang.String, com.immomo.momo.service.bean.bf, boolean):void
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object, java.io.Serializable):void
      com.immomo.momo.service.a.b.a(java.util.Map, java.lang.String[], java.lang.Object[]):void
      com.immomo.momo.service.a.b.a(java.lang.String[], java.lang.Object[], java.io.Serializable):void
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.String[], java.lang.String[]):java.lang.String[]
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object[], java.lang.String):java.util.List */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.m.a(java.lang.String, java.lang.Throwable):void
     arg types: [java.lang.String, java.lang.Exception]
     candidates:
      com.immomo.momo.util.m.a(java.lang.Appendable, java.lang.Throwable):void
      com.immomo.momo.util.m.a(java.lang.Object, java.lang.Throwable):void
      com.immomo.momo.util.m.a(java.lang.String, java.io.File):void
      com.immomo.momo.util.m.a(java.lang.String, java.lang.Throwable):void */
    public final List e() {
        if (u.c("userhidelist")) {
            return (List) u.b("userhidelist");
        }
        List arrayList = new ArrayList();
        try {
            File file = new File(com.immomo.momo.a.u(), "hidelist");
            if (file.exists()) {
                String a2 = h.a(file);
                if (!a.a((CharSequence) a2)) {
                    JSONArray jSONArray = new JSONArray(a2);
                    ArrayList arrayList2 = new ArrayList();
                    String[] strArr = new String[jSONArray.length()];
                    for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                        strArr[i2] = jSONArray.optString(i2);
                        arrayList2.add(strArr[i2]);
                    }
                    arrayList = this.c.a("u_momoid", (Object[]) strArr, (String) null);
                    Collections.sort(arrayList, new at(arrayList2));
                }
            }
        } catch (Exception e2) {
            this.b.a("add Hidelist failed, " + arrayList, (Throwable) e2);
        }
        u.a("userhidelist", arrayList);
        return arrayList;
    }

    public final List e(String str) {
        if (str == null) {
            str = PoiTypeDef.All;
        }
        String p = a.p(str);
        boolean d2 = com.immomo.momo.util.d.d(p);
        StringBuilder sb = new StringBuilder();
        sb.append("select u.*, bf.f_followtime, bf.bf_momoid from users2 u, bothfollow bf where u.u_momoid = bf.bf_momoid").append(" and (u.field2 like '%" + p + "%'").append(" or u.field5 like '%" + p + "%'").append(" or u.field6 like '%" + p + "%'").append(" or u.u_name like '%" + p + "%'");
        if (d2) {
            sb.append(" or u.field4 like '%" + p + "%'");
        }
        if (p.length() >= 3) {
            sb.append(" or u.u_momoid like '" + p + "%'");
        }
        sb.append(")");
        List b = this.c.b(sb.toString(), new String[0]);
        a(b, p);
        return b;
    }

    public final void e(bf bfVar) {
        if (a.a((CharSequence) bfVar.h)) {
            new IllegalArgumentException("momoid is null");
        }
        if (this.c.c(bfVar.h)) {
            this.c.a(new String[]{"u_name", "u_distance"}, new Object[]{bfVar.i, Float.valueOf(bfVar.d())}, new String[]{"u_momoid"}, new String[]{bfVar.h});
            return;
        }
        this.c.b(new String[]{"u_name", "u_distance", "u_momoid"}, new Object[]{bfVar.i, Float.valueOf(bfVar.d()), bfVar.h});
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.m.a(java.lang.String, java.lang.Throwable):void
     arg types: [java.lang.String, java.lang.Exception]
     candidates:
      com.immomo.momo.util.m.a(java.lang.Appendable, java.lang.Throwable):void
      com.immomo.momo.util.m.a(java.lang.Object, java.lang.Throwable):void
      com.immomo.momo.util.m.a(java.lang.String, java.io.File):void
      com.immomo.momo.util.m.a(java.lang.String, java.lang.Throwable):void */
    public final void e(List list) {
        try {
            u.a("uservisitorlist", list);
            this.f2968a.beginTransaction();
            JSONArray jSONArray = new JSONArray();
            Iterator it = list.iterator();
            while (it.hasNext()) {
                bf bfVar = (bf) it.next();
                d(bfVar);
                jSONArray.put(bfVar.h);
            }
            h.a(new File(com.immomo.momo.a.u(), "visitors"), jSONArray.toString());
            this.f2968a.setTransactionSuccessful();
        } catch (Exception e2) {
            this.b.a("add Visitors failed, " + list, (Throwable) e2);
        } finally {
            this.f2968a.endTransaction();
        }
    }

    public final void f(bf bfVar) {
        if (a.a((CharSequence) bfVar.h)) {
            new IllegalArgumentException("momoid is null");
        }
        if (this.c.c(bfVar.h)) {
            this.c.a(new String[]{"u_name", "u_distance", "field10"}, new Object[]{bfVar.i, Float.valueOf(bfVar.d()), Integer.valueOf(bfVar.I)}, new String[]{"u_momoid"}, new String[]{bfVar.h});
            return;
        }
        this.c.b(new String[]{"u_name", "u_distance", "field10", "u_momoid"}, new Object[]{bfVar.i, Float.valueOf(bfVar.d()), Integer.valueOf(bfVar.I), bfVar.h});
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.m.a(java.lang.String, java.lang.Throwable):void
     arg types: [java.lang.String, java.lang.Exception]
     candidates:
      com.immomo.momo.util.m.a(java.lang.Appendable, java.lang.Throwable):void
      com.immomo.momo.util.m.a(java.lang.Object, java.lang.Throwable):void
      com.immomo.momo.util.m.a(java.lang.String, java.io.File):void
      com.immomo.momo.util.m.a(java.lang.String, java.lang.Throwable):void */
    public final void f(List list) {
        try {
            u.a("userhidelist", list);
            this.f2968a.beginTransaction();
            JSONArray jSONArray = new JSONArray();
            Iterator it = list.iterator();
            while (it.hasNext()) {
                bf bfVar = (bf) it.next();
                d(bfVar);
                jSONArray.put(bfVar.h);
            }
            h.a(new File(com.immomo.momo.a.u(), "hidelist"), jSONArray.toString());
            this.f2968a.setTransactionSuccessful();
        } catch (Exception e2) {
            this.b.a("add Hidelist failed, " + list, (Throwable) e2);
        } finally {
            this.f2968a.endTransaction();
        }
    }

    public final boolean f(String str) {
        return this.h.c(str);
    }

    public final String g(String str) {
        return this.c.c("field23", new String[]{"u_momoid"}, new String[]{str});
    }

    public final List g() {
        if (u.c("userfanslist")) {
            return (List) u.b("userfanslist");
        }
        List b = this.c.b("select u.*, f.f_momoid, f.f_followtime from fans as f LEFT OUTER JOIN " + "users2 as u" + " on f.f_momoid" + " = u.u_momoid", new String[0]);
        u.a("userfanslist", b);
        return b;
    }

    public final void g(bf bfVar) {
        a(bfVar.h, bfVar.getLoadImageId(), bfVar.i);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.m.a(java.lang.String, java.lang.Throwable):void
     arg types: [java.lang.String, java.lang.Exception]
     candidates:
      com.immomo.momo.util.m.a(java.lang.Appendable, java.lang.Throwable):void
      com.immomo.momo.util.m.a(java.lang.Object, java.lang.Throwable):void
      com.immomo.momo.util.m.a(java.lang.String, java.io.File):void
      com.immomo.momo.util.m.a(java.lang.String, java.lang.Throwable):void */
    public final void g(List list) {
        try {
            this.f2968a.beginTransaction();
            this.f.c();
            Iterator it = list.iterator();
            while (it.hasNext()) {
                k((bf) it.next());
            }
            this.f2968a.setTransactionSuccessful();
        } catch (Exception e2) {
            this.b.a("add blackuser failed, " + list, (Throwable) e2);
        } finally {
            this.f2968a.endTransaction();
        }
    }

    public final void h() {
        this.f.c();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.m.a(java.lang.String, java.lang.Throwable):void
     arg types: [java.lang.String, java.lang.Exception]
     candidates:
      com.immomo.momo.util.m.a(java.lang.Appendable, java.lang.Throwable):void
      com.immomo.momo.util.m.a(java.lang.Object, java.lang.Throwable):void
      com.immomo.momo.util.m.a(java.lang.String, java.io.File):void
      com.immomo.momo.util.m.a(java.lang.String, java.lang.Throwable):void */
    public final void h(bf bfVar) {
        try {
            a(bfVar.h, bfVar.aD);
        } catch (Exception e2) {
            this.b.a("addFriend user failed, " + bfVar, (Throwable) e2);
        }
    }

    public final void h(String str) {
        int i2 = 1;
        String c2 = this.c.c("field34", new String[]{"field34"}, new String[]{str});
        try {
            if (a.a((CharSequence) c2)) {
                i2 = Integer.parseInt(c2);
            }
        } catch (Exception e2) {
        }
        a(str, "field34", Integer.valueOf(i2 - 1));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.m.a(java.lang.String, java.lang.Throwable):void
     arg types: [java.lang.String, java.lang.Exception]
     candidates:
      com.immomo.momo.util.m.a(java.lang.Appendable, java.lang.Throwable):void
      com.immomo.momo.util.m.a(java.lang.Object, java.lang.Throwable):void
      com.immomo.momo.util.m.a(java.lang.String, java.io.File):void
      com.immomo.momo.util.m.a(java.lang.String, java.lang.Throwable):void */
    public final void h(List list) {
        try {
            this.f2968a.beginTransaction();
            this.i.c();
            Iterator it = list.iterator();
            while (it.hasNext()) {
                this.i.a((Object) ((bf) it.next()).h);
            }
            u.a("usernearby", list);
            this.f2968a.setTransactionSuccessful();
        } catch (Exception e2) {
            this.b.a("addNearUsers failed", (Throwable) e2);
        } finally {
            this.f2968a.endTransaction();
        }
    }

    public final List i() {
        return this.c.b("select u.*, b.b_blacktime, b.b_momoid from blacklist as b LEFT OUTER JOIN " + "users2 as u" + " on b.b_momoid" + " = u.u_momoid", new String[0]);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.m.a(java.lang.String, java.lang.Throwable):void
     arg types: [java.lang.String, java.lang.Exception]
     candidates:
      com.immomo.momo.util.m.a(java.lang.Appendable, java.lang.Throwable):void
      com.immomo.momo.util.m.a(java.lang.Object, java.lang.Throwable):void
      com.immomo.momo.util.m.a(java.lang.String, java.io.File):void
      com.immomo.momo.util.m.a(java.lang.String, java.lang.Throwable):void */
    public final void i(bf bfVar) {
        try {
            b(bfVar.h, bfVar.aD);
        } catch (Exception e2) {
            this.b.a("addBothFollow user failed, " + bfVar, (Throwable) e2);
        }
    }

    public final void i(String str) {
        if (this.e.c(str)) {
            this.e.b((Serializable) str);
        }
        q(str);
    }

    public final bf j(String str) {
        if (!k(str)) {
            return null;
        }
        bf bfVar = new bf(str);
        bf b = b(str);
        return b != null ? b : bfVar;
    }

    public final List j() {
        if (u.c("usernearby")) {
            List<bf> list = (List) u.b("usernearby");
            for (bf bfVar : list) {
                bfVar.a(bfVar.a());
            }
            return list;
        }
        List b = this.c.b("select u.*, n.n_momoid from nearybys as n LEFT OUTER JOIN " + "users2 as u" + " on n.n_momoid" + " = u.u_momoid order by n._id asc", new String[0]);
        u.a("usernearby", b);
        return b;
    }

    public final void j(bf bfVar) {
        long longValue;
        float d2 = bfVar.d();
        if (d2 == -1.0f) {
            longValue = 2147483647L;
        } else if (d2 == -2.0f) {
            longValue = 2147483646;
        } else {
            new BigDecimal((double) d2).longValue();
            longValue = new BigDecimal((double) d2).longValue();
        }
        this.c.a(new String[]{"u_distance", "u_loctime"}, new Object[]{Long.valueOf(longValue), Long.valueOf(bfVar.a())}, new String[]{"u_momoid"}, new Object[]{bfVar.h});
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.m.a(java.lang.String, java.lang.Throwable):void
     arg types: [java.lang.String, java.lang.Exception]
     candidates:
      com.immomo.momo.util.m.a(java.lang.Appendable, java.lang.Throwable):void
      com.immomo.momo.util.m.a(java.lang.Object, java.lang.Throwable):void
      com.immomo.momo.util.m.a(java.lang.String, java.io.File):void
      com.immomo.momo.util.m.a(java.lang.String, java.lang.Throwable):void */
    public final void k(bf bfVar) {
        try {
            String str = bfVar.h;
            Date date = bfVar.Y;
            boolean z = false;
            if (this.f.c(str)) {
                z = true;
            }
            if (z) {
                this.b.c("addBlackUser failed. this user is exisit");
            } else {
                if (date == null) {
                    date = new Date();
                }
                HashMap hashMap = new HashMap();
                hashMap.put(str, date);
                this.f.a((Object) hashMap);
                this.j.h(str);
                this.j.d(str);
            }
            b(bfVar);
        } catch (Exception e2) {
            this.b.a("add fans failed, " + bfVar, (Throwable) e2);
        }
    }

    public final boolean k(String str) {
        return this.e.c(str);
    }

    public final void l(String str) {
        this.g.b((Serializable) str);
    }

    public final boolean m(String str) {
        return this.d.c(str);
    }

    public final void n(String str) {
        try {
            this.d.a(str);
        } catch (Exception e2) {
        }
    }

    public final boolean o(String str) {
        return this.g.c(str);
    }

    public final void p(String str) {
        this.f.b((Serializable) str);
    }
}
