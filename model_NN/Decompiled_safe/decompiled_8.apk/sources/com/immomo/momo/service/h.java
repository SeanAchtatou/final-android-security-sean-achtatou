package com.immomo.momo.service;

import android.database.sqlite.SQLiteDatabase;
import android.support.v4.b.a;
import com.immomo.momo.g;
import com.immomo.momo.service.a.ai;
import com.immomo.momo.service.a.j;
import com.immomo.momo.service.bean.Message;
import com.immomo.momo.service.bean.n;
import com.immomo.momo.service.bean.p;
import com.immomo.momo.util.u;
import java.io.Serializable;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public final class h extends b {
    private ai c;
    private com.immomo.momo.service.a.h d;
    private aq e;
    private j f;

    public h() {
        this(g.d().e());
    }

    private h(SQLiteDatabase sQLiteDatabase) {
        this.c = null;
        this.d = null;
        this.e = null;
        this.f = null;
        this.f2968a = sQLiteDatabase;
        this.d = new com.immomo.momo.service.a.h(sQLiteDatabase);
        this.c = new ai(sQLiteDatabase);
        this.f = new j(sQLiteDatabase);
        this.e = new aq();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.service.h.b(java.lang.String, boolean):java.util.List
     arg types: [java.lang.String, int]
     candidates:
      com.immomo.momo.service.h.b(java.lang.String, int):void
      com.immomo.momo.service.h.b(java.lang.String, java.lang.String):void
      com.immomo.momo.service.h.b(java.lang.String, boolean):java.util.List */
    public final n a(String str, boolean z) {
        n nVar = (n) this.d.a((Serializable) str);
        if (nVar != null && z) {
            nVar.d = b(str, true);
        }
        return nVar;
    }

    public final String a(String str) {
        return this.d.c(Message.DBFIELD_SAYHI, new String[]{"did"}, new String[]{str});
    }

    public final void a(n nVar) {
        if (this.d.c(nVar.f)) {
            this.d.a(new String[]{Message.DBFIELD_SAYHI, "field10", "field7", Message.DBFIELD_GROUPID, Message.DBFIELD_LOCATIONJSON, Message.DBFIELD_CONVERLOCATIONJSON, "field5", "field8", "field9", "field6"}, new Object[]{nVar.b, nVar.m, Long.valueOf(com.immomo.momo.service.a.h.a(nVar.i)), a.a(nVar.e, ","), nVar.c, a.a(nVar.f3032a, ","), Integer.valueOf(nVar.g), Integer.valueOf(nVar.j), Integer.valueOf(nVar.k), Integer.valueOf(nVar.h)}, new String[]{"did"}, new String[]{nVar.f});
            return;
        }
        this.d.a(nVar);
    }

    public final void a(n nVar, String str) {
        this.d.a(nVar, str);
    }

    public final void a(String str, int i) {
        this.d.a("field5", Integer.valueOf(i), str);
    }

    public final void a(String str, String str2) {
        this.d.a("field11", str, str2);
    }

    public final void a(String str, String str2, int i) {
        if (str.equals(g.q().h) && !this.c.c(str2)) {
            this.c.a(str2);
        }
        if (!d(str, str2)) {
            p pVar = new p();
            pVar.b = str2;
            pVar.g = i;
            pVar.c = new Date();
            pVar.f3034a = str;
            this.f.a(pVar);
            return;
        }
        p pVar2 = new p();
        pVar2.b = str2;
        pVar2.g = i;
        pVar2.c = new Date();
        pVar2.f3034a = str;
        this.f.b(pVar2);
    }

    public final void a(String str, String[] strArr) {
        this.d.a(Message.DBFIELD_GROUPID, a.a(strArr, ","), str);
    }

    public final void a(List list) {
        this.d.d();
        try {
            this.c.c();
            Iterator it = list.iterator();
            while (it.hasNext()) {
                n nVar = (n) it.next();
                a(nVar);
                this.c.a(nVar.f);
            }
            u.a("discussminelist", list);
            this.d.f();
        } finally {
            this.d.e();
        }
    }

    public final void a(List list, String str) {
        this.f2968a.beginTransaction();
        try {
            b(str);
            Iterator it = list.iterator();
            while (it.hasNext()) {
                p pVar = (p) it.next();
                pVar.b = str;
                this.f.a(pVar);
                if (pVar.h != null) {
                    this.e.d(pVar.h);
                }
            }
            this.f2968a.setTransactionSuccessful();
        } catch (Exception e2) {
            this.b.a((Throwable) e2);
        } finally {
            this.f2968a.endTransaction();
        }
    }

    public final List b(String str, boolean z) {
        List<p> a2 = this.f.a(new String[]{Message.DBFIELD_GROUPID}, new String[]{str});
        if (z && a2 != null) {
            for (p pVar : a2) {
                pVar.h = this.e.c(pVar.f3034a);
            }
        }
        return a2;
    }

    public final void b(n nVar) {
        if (!this.c.c(nVar.f)) {
            this.c.a(nVar.f);
        }
        if (u.c("discussminelist")) {
            List list = (List) u.b("discussminelist");
            if (!list.contains(nVar)) {
                list.add(0, nVar);
                u.a("discussminelist", list);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.immomo.momo.service.a.j.a(com.immomo.momo.service.bean.p, android.database.Cursor):void
      com.immomo.momo.service.a.j.a(java.lang.Object, android.database.Cursor):void
      com.immomo.momo.service.a.b.a(android.database.Cursor, java.lang.String):int
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.String[]):android.database.Cursor
      com.immomo.momo.service.a.b.a(java.lang.String[], java.lang.String[]):java.util.List
      com.immomo.momo.service.a.b.a(java.lang.Object, android.database.Cursor):void
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object[]):void
      com.immomo.momo.service.a.b.a(java.lang.String[], java.lang.Object[]):void
      com.immomo.momo.service.a.b.a(java.lang.Object, java.io.Serializable):boolean
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.String):boolean
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object):void */
    public final void b(String str) {
        this.f.a(Message.DBFIELD_GROUPID, (Object) str);
    }

    public final void b(String str, int i) {
        this.d.a("field8", Integer.valueOf(i), str);
    }

    public final void b(String str, String str2) {
        this.d.a(Message.DBFIELD_SAYHI, str2, str);
    }

    public final int c(String str) {
        List a2 = this.f.a(new String[]{Message.DBFIELD_GROUPID}, new String[]{str});
        if (a2 == null) {
            return 0;
        }
        return a2.size();
    }

    public final List c() {
        if (u.c("discussminelist")) {
            return (List) u.b("discussminelist");
        }
        List a2 = this.d.a("did", this.c.b().toArray(), (String) null);
        u.a("discussminelist", a2);
        return a2;
    }

    public final List c(String str, int i) {
        List<p> a2 = this.f.a(new String[]{Message.DBFIELD_GROUPID}, new String[]{str});
        if (a2 != null) {
            for (p pVar : a2) {
                pVar.h = this.e.c(pVar.f3034a);
            }
        }
        Collections.sort(a2, new i(this, i));
        return a2;
    }

    public final void c(String str, String str2) {
        this.f.a(new String[]{Message.DBFIELD_GROUPID, Message.DBFIELD_SAYHI}, new Object[]{str2, str});
        if (str.equals(g.q().h)) {
            this.c.b((Serializable) str2);
            if (u.c("discussminelist")) {
                List list = (List) u.b("discussminelist");
                list.remove(new n(str2));
                u.a("discussminelist", list);
            }
        }
    }

    public final boolean d(String str, String str2) {
        return this.f.c(new String[]{Message.DBFIELD_GROUPID, Message.DBFIELD_SAYHI}, new String[]{str2, str}) > 0;
    }
}
