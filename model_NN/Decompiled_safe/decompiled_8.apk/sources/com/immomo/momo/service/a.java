package com.immomo.momo.service;

import android.database.sqlite.SQLiteDatabase;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.Message;
import com.immomo.momo.service.bean.d;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public final class a extends b {
    private com.immomo.momo.service.a.a c;

    public a() {
        this(g.d().e());
    }

    private a(SQLiteDatabase sQLiteDatabase) {
        this.c = null;
        this.f2968a = sQLiteDatabase;
        this.c = new com.immomo.momo.service.a.a(sQLiteDatabase);
    }

    public final d a(String str, int i) {
        return (d) this.c.b(new String[]{Message.DBFIELD_SAYHI, Message.DBFIELD_LOCATIONJSON}, new String[]{str, new StringBuilder(String.valueOf(i)).toString()});
    }

    public final List a(Date date, int i) {
        StringBuilder sb = new StringBuilder();
        sb.append("field5<=").append(date.getTime());
        sb.append(" and ");
        sb.append("field6>").append(date.getTime());
        sb.append(" and ");
        sb.append("field2=").append(i);
        sb.append(" order by rowid");
        return this.c.c(sb.toString(), new String[0]);
    }

    public final void a(int i) {
        this.c.a(new String[]{"field11"}, new Object[]{false}, new String[]{Message.DBFIELD_LOCATIONJSON}, new String[]{new StringBuilder(String.valueOf(i)).toString()});
    }

    public final void a(d dVar) {
        this.c.a(new String[]{"field8"}, new String[]{dVar.i.getPath()}, new String[]{Message.DBFIELD_LOCATIONJSON, Message.DBFIELD_SAYHI}, new String[]{new StringBuilder(String.valueOf(dVar.h)).toString(), dVar.f3022a});
    }

    public final void a(String str, int i, Date date) {
        this.c.a(new String[]{"field12"}, new Object[]{date}, new String[]{Message.DBFIELD_SAYHI, Message.DBFIELD_LOCATIONJSON}, new String[]{str, new StringBuilder(String.valueOf(i)).toString()});
    }

    public final void a(List list) {
        this.f2968a.beginTransaction();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            d dVar = (d) it.next();
            com.immomo.momo.service.a.a aVar = this.c;
            String[] strArr = {Message.DBFIELD_GROUPID, "field6", "field7", "field8", Message.DBFIELD_CONVERLOCATIONJSON, "field5", "field9", "field10", "field11", "field14", "field13", "field15", "field16"};
            Object[] objArr = new Object[13];
            objArr[0] = Integer.valueOf(dVar.c);
            objArr[1] = dVar.f;
            objArr[2] = dVar.d;
            objArr[3] = dVar.i != null ? dVar.i.getPath() : PoiTypeDef.All;
            objArr[4] = Integer.valueOf(dVar.b);
            objArr[5] = dVar.e;
            objArr[6] = dVar.g;
            objArr[7] = Boolean.valueOf(dVar.j);
            objArr[8] = Boolean.valueOf(dVar.k);
            objArr[9] = dVar.n;
            objArr[10] = dVar.m;
            objArr[11] = Boolean.valueOf(dVar.o);
            objArr[12] = Long.valueOf(dVar.p);
            aVar.a(strArr, objArr, new String[]{Message.DBFIELD_SAYHI, Message.DBFIELD_LOCATIONJSON}, new Object[]{dVar.f3022a, Integer.valueOf(dVar.h)});
        }
        this.f2968a.setTransactionSuccessful();
        this.f2968a.endTransaction();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.service.a.b.a(java.lang.String[], java.lang.Object[]):void
     arg types: [java.lang.String[], java.lang.String[]]
     candidates:
      com.immomo.momo.service.a.a.a(com.immomo.momo.service.bean.d, android.database.Cursor):void
      com.immomo.momo.service.a.a.a(java.lang.Object, android.database.Cursor):void
      com.immomo.momo.service.a.b.a(android.database.Cursor, java.lang.String):int
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.String[]):android.database.Cursor
      com.immomo.momo.service.a.b.a(java.lang.String[], java.lang.String[]):java.util.List
      com.immomo.momo.service.a.b.a(java.lang.Object, android.database.Cursor):void
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object):void
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object[]):void
      com.immomo.momo.service.a.b.a(java.lang.Object, java.io.Serializable):boolean
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.String):boolean
      com.immomo.momo.service.a.b.a(java.lang.String[], java.lang.Object[]):void */
    public final void a(List list, int i) {
        this.f2968a.beginTransaction();
        try {
            this.c.a(new String[]{Message.DBFIELD_LOCATIONJSON}, (Object[]) new String[]{new StringBuilder(String.valueOf(i)).toString()});
            Iterator it = list.iterator();
            while (it.hasNext()) {
                d dVar = (d) it.next();
                if (!(this.c.c(new String[]{Message.DBFIELD_SAYHI, Message.DBFIELD_LOCATIONJSON}, new String[]{dVar.f3022a, new StringBuilder(String.valueOf(i)).toString()}) > 0)) {
                    this.c.a(dVar);
                }
            }
            this.f2968a.setTransactionSuccessful();
        } catch (Exception e) {
            this.b.a((Throwable) e);
        } finally {
            this.f2968a.endTransaction();
        }
    }

    public final void b(String str, int i) {
        this.c.a(new String[]{"field11"}, new Object[]{true}, new String[]{Message.DBFIELD_SAYHI, Message.DBFIELD_LOCATIONJSON}, new String[]{str, new StringBuilder(String.valueOf(i)).toString()});
    }
}
