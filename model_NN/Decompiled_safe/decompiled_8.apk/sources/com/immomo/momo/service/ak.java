package com.immomo.momo.service;

import com.immomo.momo.service.bean.ae;
import java.util.Comparator;
import java.util.List;

final class ak implements Comparator {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ List f2961a;
    private final /* synthetic */ List b;

    ak(List list, List list2) {
        this.f2961a = list;
        this.b = list2;
    }

    public final /* synthetic */ int compare(Object obj, Object obj2) {
        ae aeVar = (ae) obj;
        ae aeVar2 = (ae) obj2;
        int indexOf = this.f2961a.indexOf(aeVar.k);
        int indexOf2 = this.f2961a.indexOf(aeVar2.k);
        if (indexOf >= 0) {
            aeVar.j = ((String[]) this.b.get(indexOf))[1];
        }
        if (indexOf2 >= 0) {
            aeVar2.j = ((String[]) this.b.get(indexOf2))[1];
        }
        return indexOf > indexOf2 ? 1 : -1;
    }
}
