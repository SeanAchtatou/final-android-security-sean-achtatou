package com.immomo.momo.service;

import android.database.sqlite.SQLiteDatabase;
import android.support.v4.b.a;
import com.immomo.momo.android.c.u;
import com.immomo.momo.g;
import com.immomo.momo.protocol.a.v;
import com.immomo.momo.service.a.aw;
import com.immomo.momo.service.a.ax;
import com.immomo.momo.service.bean.Message;
import com.immomo.momo.service.bean.c.b;
import com.immomo.momo.service.bean.c.d;
import com.immomo.momo.service.bean.c.f;
import com.immomo.momo.util.h;
import com.immomo.momo.util.m;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;

public final class ao {

    /* renamed from: a  reason: collision with root package name */
    private aw f2963a;
    private ax b;
    private aq c;
    private SQLiteDatabase d;
    private m e;

    public ao() {
        this.f2963a = null;
        this.b = null;
        this.c = null;
        this.d = null;
        this.e = new m(this);
        this.d = g.d().e();
        this.b = new ax(this.d);
        this.f2963a = new aw(this.d);
        this.c = new aq();
    }

    public final d a(String str) {
        d dVar = (d) this.b.a((Serializable) str);
        if (dVar != null && !a.a((CharSequence) dVar.c)) {
            com.immomo.momo.service.bean.c.g gVar = new com.immomo.momo.service.bean.c.g();
            if (this.c.a(gVar, dVar.c)) {
                dVar.d = gVar;
            }
        }
        return dVar;
    }

    /* JADX WARNING: Unknown top exception splitter block from list: {B:19:0x00ca=Splitter:B:19:0x00ca, B:15:0x00bf=Splitter:B:15:0x00bf} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.List a() {
        /*
            r9 = this;
            java.util.ArrayList r2 = new java.util.ArrayList
            r2.<init>()
            r1 = 0
            java.io.File r3 = new java.io.File     // Catch:{ IOException -> 0x00be, JSONException -> 0x00c9 }
            java.io.File r0 = com.immomo.momo.a.s()     // Catch:{ IOException -> 0x00be, JSONException -> 0x00c9 }
            java.lang.String r4 = "admin"
            r3.<init>(r0, r4)     // Catch:{ IOException -> 0x00be, JSONException -> 0x00c9 }
            boolean r0 = r3.exists()     // Catch:{ IOException -> 0x00be, JSONException -> 0x00c9 }
            if (r0 == 0) goto L_0x00e8
            java.io.FileInputStream r0 = new java.io.FileInputStream     // Catch:{ IOException -> 0x00be, JSONException -> 0x00c9 }
            r0.<init>(r3)     // Catch:{ IOException -> 0x00be, JSONException -> 0x00c9 }
            byte[] r1 = android.support.v4.b.a.b(r0)     // Catch:{ IOException -> 0x00e3, JSONException -> 0x00de, all -> 0x00d9 }
            org.json.JSONArray r3 = new org.json.JSONArray     // Catch:{ IOException -> 0x00e3, JSONException -> 0x00de, all -> 0x00d9 }
            java.lang.String r4 = new java.lang.String     // Catch:{ IOException -> 0x00e3, JSONException -> 0x00de, all -> 0x00d9 }
            r4.<init>(r1)     // Catch:{ IOException -> 0x00e3, JSONException -> 0x00de, all -> 0x00d9 }
            r3.<init>(r4)     // Catch:{ IOException -> 0x00e3, JSONException -> 0x00de, all -> 0x00d9 }
            r1 = 0
        L_0x002b:
            int r4 = r3.length()     // Catch:{ IOException -> 0x00e3, JSONException -> 0x00de, all -> 0x00d9 }
            if (r1 < r4) goto L_0x0035
        L_0x0031:
            android.support.v4.b.a.a(r0)
        L_0x0034:
            return r2
        L_0x0035:
            org.json.JSONObject r4 = r3.getJSONObject(r1)     // Catch:{ IOException -> 0x00e3, JSONException -> 0x00de, all -> 0x00d9 }
            com.immomo.momo.service.bean.c.e r5 = new com.immomo.momo.service.bean.c.e     // Catch:{ IOException -> 0x00e3, JSONException -> 0x00de, all -> 0x00d9 }
            r5.<init>()     // Catch:{ IOException -> 0x00e3, JSONException -> 0x00de, all -> 0x00d9 }
            java.lang.String r6 = "floor"
            int r6 = r4.optInt(r6)     // Catch:{ IOException -> 0x00e3, JSONException -> 0x00de, all -> 0x00d9 }
            r5.l = r6     // Catch:{ IOException -> 0x00e3, JSONException -> 0x00de, all -> 0x00d9 }
            java.lang.String r6 = "action"
            java.lang.String r6 = r4.optString(r6)     // Catch:{ IOException -> 0x00e3, JSONException -> 0x00de, all -> 0x00d9 }
            r5.h = r6     // Catch:{ IOException -> 0x00e3, JSONException -> 0x00de, all -> 0x00d9 }
            java.lang.String r6 = "cid"
            java.lang.String r6 = r4.optString(r6)     // Catch:{ IOException -> 0x00e3, JSONException -> 0x00de, all -> 0x00d9 }
            r5.f = r6     // Catch:{ IOException -> 0x00e3, JSONException -> 0x00de, all -> 0x00d9 }
            java.lang.String r6 = "content"
            java.lang.String r6 = r4.optString(r6)     // Catch:{ IOException -> 0x00e3, JSONException -> 0x00de, all -> 0x00d9 }
            r5.k = r6     // Catch:{ IOException -> 0x00e3, JSONException -> 0x00de, all -> 0x00d9 }
            java.lang.String r6 = "rcount"
            int r6 = r4.optInt(r6)     // Catch:{ IOException -> 0x00e3, JSONException -> 0x00de, all -> 0x00d9 }
            r5.g = r6     // Catch:{ IOException -> 0x00e3, JSONException -> 0x00de, all -> 0x00d9 }
            java.lang.String r6 = "tid"
            java.lang.String r6 = r4.optString(r6)     // Catch:{ IOException -> 0x00e3, JSONException -> 0x00de, all -> 0x00d9 }
            r5.c = r6     // Catch:{ IOException -> 0x00e3, JSONException -> 0x00de, all -> 0x00d9 }
            java.lang.String r6 = "tname"
            java.lang.String r6 = r4.optString(r6)     // Catch:{ IOException -> 0x00e3, JSONException -> 0x00de, all -> 0x00d9 }
            r5.d = r6     // Catch:{ IOException -> 0x00e3, JSONException -> 0x00de, all -> 0x00d9 }
            java.lang.String r6 = "pid"
            java.lang.String r6 = r4.optString(r6)     // Catch:{ IOException -> 0x00e3, JSONException -> 0x00de, all -> 0x00d9 }
            r5.e = r6     // Catch:{ IOException -> 0x00e3, JSONException -> 0x00de, all -> 0x00d9 }
            java.lang.String r6 = "title"
            java.lang.String r6 = r4.optString(r6)     // Catch:{ IOException -> 0x00e3, JSONException -> 0x00de, all -> 0x00d9 }
            r5.j = r6     // Catch:{ IOException -> 0x00e3, JSONException -> 0x00de, all -> 0x00d9 }
            java.lang.String r6 = "type"
            java.lang.String r6 = r4.optString(r6)     // Catch:{ IOException -> 0x00e3, JSONException -> 0x00de, all -> 0x00d9 }
            r5.b = r6     // Catch:{ IOException -> 0x00e3, JSONException -> 0x00de, all -> 0x00d9 }
            java.lang.String r6 = "tyname"
            java.lang.String r6 = r4.optString(r6)     // Catch:{ IOException -> 0x00e3, JSONException -> 0x00de, all -> 0x00d9 }
            r5.f3020a = r6     // Catch:{ IOException -> 0x00e3, JSONException -> 0x00de, all -> 0x00d9 }
            java.lang.String r6 = "utime"
            long r6 = r4.optLong(r6)     // Catch:{ IOException -> 0x00e3, JSONException -> 0x00de, all -> 0x00d9 }
            r5.a(r6)     // Catch:{ IOException -> 0x00e3, JSONException -> 0x00de, all -> 0x00d9 }
            java.lang.String r6 = "action_count"
            int r6 = r4.optInt(r6)     // Catch:{ IOException -> 0x00e3, JSONException -> 0x00de, all -> 0x00d9 }
            r5.i = r6     // Catch:{ IOException -> 0x00e3, JSONException -> 0x00de, all -> 0x00d9 }
            java.lang.String r6 = "lock"
            boolean r6 = r4.optBoolean(r6)     // Catch:{ IOException -> 0x00e3, JSONException -> 0x00de, all -> 0x00d9 }
            r5.m = r6     // Catch:{ IOException -> 0x00e3, JSONException -> 0x00de, all -> 0x00d9 }
            java.lang.String r6 = "delete"
            boolean r4 = r4.optBoolean(r6)     // Catch:{ IOException -> 0x00e3, JSONException -> 0x00de, all -> 0x00d9 }
            r5.n = r4     // Catch:{ IOException -> 0x00e3, JSONException -> 0x00de, all -> 0x00d9 }
            r2.add(r5)     // Catch:{ IOException -> 0x00e3, JSONException -> 0x00de, all -> 0x00d9 }
            int r1 = r1 + 1
            goto L_0x002b
        L_0x00be:
            r0 = move-exception
        L_0x00bf:
            com.immomo.momo.util.m r3 = r9.e     // Catch:{ all -> 0x00d4 }
            r3.a(r0)     // Catch:{ all -> 0x00d4 }
            android.support.v4.b.a.a(r1)
            goto L_0x0034
        L_0x00c9:
            r0 = move-exception
        L_0x00ca:
            com.immomo.momo.util.m r3 = r9.e     // Catch:{ all -> 0x00d4 }
            r3.a(r0)     // Catch:{ all -> 0x00d4 }
            android.support.v4.b.a.a(r1)
            goto L_0x0034
        L_0x00d4:
            r0 = move-exception
        L_0x00d5:
            android.support.v4.b.a.a(r1)
            throw r0
        L_0x00d9:
            r1 = move-exception
            r8 = r1
            r1 = r0
            r0 = r8
            goto L_0x00d5
        L_0x00de:
            r1 = move-exception
            r8 = r1
            r1 = r0
            r0 = r8
            goto L_0x00ca
        L_0x00e3:
            r1 = move-exception
            r8 = r1
            r1 = r0
            r0 = r8
            goto L_0x00bf
        L_0x00e8:
            r0 = r1
            goto L_0x0031
        */
        throw new UnsupportedOperationException("Method not decompiled: com.immomo.momo.service.ao.a():java.util.List");
    }

    public final void a(b bVar) {
        if (a.a((CharSequence) bVar.f3017a)) {
            throw new RuntimeException("tie.id is null");
        }
        if (this.f2963a.c(bVar.f3017a)) {
            this.f2963a.b(bVar);
        } else {
            this.f2963a.a(bVar);
        }
        if (bVar.e != null) {
            this.c.f(bVar.e);
        }
    }

    public final void a(d dVar) {
        if (a.a((CharSequence) dVar.f3019a)) {
            throw new RuntimeException("tieba.id is null");
        }
        if (this.b.c(dVar.f3019a)) {
            this.b.b(dVar);
        } else {
            this.b.a(dVar);
        }
        com.immomo.momo.service.bean.c.g gVar = dVar.d;
    }

    public final void a(String str, int i) {
        this.f2963a.a("field11", Integer.valueOf(i), str);
    }

    public final void a(List list) {
        try {
            this.d.beginTransaction();
            Iterator it = list.iterator();
            while (it.hasNext()) {
                a((d) it.next());
            }
            this.d.setTransactionSuccessful();
        } finally {
            this.d.endTransaction();
        }
    }

    public final void a(List list, String str) {
        this.d.beginTransaction();
        try {
            String[] strArr = new String[list.size()];
            Iterator it = list.iterator();
            int i = 0;
            while (it.hasNext()) {
                b bVar = (b) it.next();
                if (this.f2963a.c(bVar.f3017a)) {
                    HashMap hashMap = new HashMap();
                    hashMap.put("field10", Integer.valueOf(bVar.n));
                    hashMap.put(Message.DBFIELD_CONVERLOCATIONJSON, a.a(bVar.c(), ","));
                    hashMap.put("field8", Boolean.valueOf(bVar.l));
                    hashMap.put(Message.DBFIELD_LOCATIONJSON, bVar.f);
                    hashMap.put("field7", Boolean.valueOf(bVar.k));
                    hashMap.put("field5", bVar.c);
                    hashMap.put(Message.DBFIELD_GROUPID, bVar.i);
                    hashMap.put(Message.DBFIELD_SAYHI, bVar.d);
                    hashMap.put("field9", Boolean.valueOf(bVar.m));
                    hashMap.put("field11", Integer.valueOf(bVar.o));
                    hashMap.put("field12", Boolean.valueOf(bVar.q));
                    hashMap.put("field17", bVar.j);
                    this.f2963a.a(hashMap, new String[]{Message.DBFIELD_ID}, new String[]{bVar.f3017a});
                } else {
                    this.f2963a.a(bVar);
                }
                if (bVar.e != null) {
                    this.c.e(bVar.e);
                }
                strArr[i] = bVar.f3017a;
                i++;
            }
            if (this.b.c(str)) {
                this.b.a("field14", a.a(strArr, ","), str);
            } else {
                this.b.b(new String[]{"field14", Message.DBFIELD_ID}, new Object[]{a.a(strArr, ","), str});
            }
            this.d.setTransactionSuccessful();
        } finally {
            this.d.endTransaction();
        }
    }

    /* JADX WARNING: Unknown top exception splitter block from list: {B:24:0x0067=Splitter:B:24:0x0067, B:20:0x005d=Splitter:B:20:0x005d} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.List b() {
        /*
            r6 = this;
            java.lang.String r0 = "tiebamytiebas"
            boolean r0 = com.immomo.momo.util.u.c(r0)
            if (r0 == 0) goto L_0x0011
            java.lang.String r0 = "tiebamytiebas"
            java.lang.Object r0 = com.immomo.momo.util.u.b(r0)
            java.util.List r0 = (java.util.List) r0
        L_0x0010:
            return r0
        L_0x0011:
            java.util.ArrayList r2 = new java.util.ArrayList
            r2.<init>()
            java.io.File r3 = new java.io.File
            java.io.File r0 = com.immomo.momo.a.s()
            java.lang.String r1 = "mytieba"
            r3.<init>(r0, r1)
            r1 = 0
            boolean r0 = r3.exists()     // Catch:{ IOException -> 0x005c, JSONException -> 0x0066 }
            if (r0 == 0) goto L_0x0084
            java.io.FileInputStream r0 = new java.io.FileInputStream     // Catch:{ IOException -> 0x005c, JSONException -> 0x0066 }
            r0.<init>(r3)     // Catch:{ IOException -> 0x005c, JSONException -> 0x0066 }
            byte[] r1 = android.support.v4.b.a.b(r0)     // Catch:{ IOException -> 0x007f, JSONException -> 0x007a, all -> 0x0075 }
            org.json.JSONArray r3 = new org.json.JSONArray     // Catch:{ IOException -> 0x007f, JSONException -> 0x007a, all -> 0x0075 }
            java.lang.String r4 = new java.lang.String     // Catch:{ IOException -> 0x007f, JSONException -> 0x007a, all -> 0x0075 }
            r4.<init>(r1)     // Catch:{ IOException -> 0x007f, JSONException -> 0x007a, all -> 0x0075 }
            r3.<init>(r4)     // Catch:{ IOException -> 0x007f, JSONException -> 0x007a, all -> 0x0075 }
            r1 = 0
        L_0x003c:
            int r4 = r3.length()     // Catch:{ IOException -> 0x007f, JSONException -> 0x007a, all -> 0x0075 }
            if (r1 < r4) goto L_0x004c
        L_0x0042:
            android.support.v4.b.a.a(r0)
        L_0x0045:
            java.lang.String r0 = "tiebamytiebas"
            com.immomo.momo.util.u.a(r0, r2)
            r0 = r2
            goto L_0x0010
        L_0x004c:
            java.lang.String r4 = r3.getString(r1)     // Catch:{ IOException -> 0x007f, JSONException -> 0x007a, all -> 0x0075 }
            com.immomo.momo.service.bean.c.d r4 = r6.a(r4)     // Catch:{ IOException -> 0x007f, JSONException -> 0x007a, all -> 0x0075 }
            if (r4 == 0) goto L_0x0059
            r2.add(r4)     // Catch:{ IOException -> 0x007f, JSONException -> 0x007a, all -> 0x0075 }
        L_0x0059:
            int r1 = r1 + 1
            goto L_0x003c
        L_0x005c:
            r0 = move-exception
        L_0x005d:
            com.immomo.momo.util.m r3 = r6.e     // Catch:{ all -> 0x0070 }
            r3.a(r0)     // Catch:{ all -> 0x0070 }
            android.support.v4.b.a.a(r1)
            goto L_0x0045
        L_0x0066:
            r0 = move-exception
        L_0x0067:
            com.immomo.momo.util.m r3 = r6.e     // Catch:{ all -> 0x0070 }
            r3.a(r0)     // Catch:{ all -> 0x0070 }
            android.support.v4.b.a.a(r1)
            goto L_0x0045
        L_0x0070:
            r0 = move-exception
        L_0x0071:
            android.support.v4.b.a.a(r1)
            throw r0
        L_0x0075:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x0071
        L_0x007a:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x0067
        L_0x007f:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x005d
        L_0x0084:
            r0 = r1
            goto L_0x0042
        */
        throw new UnsupportedOperationException("Method not decompiled: com.immomo.momo.service.ao.b():java.util.List");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object, java.io.Serializable):void
     arg types: [java.lang.String, boolean, java.lang.String]
     candidates:
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object[], java.lang.String):java.util.List
      com.immomo.momo.service.a.b.a(java.util.Map, java.lang.String[], java.lang.Object[]):void
      com.immomo.momo.service.a.b.a(java.lang.String[], java.lang.Object[], java.io.Serializable):void
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.String[], java.lang.String[]):java.lang.String[]
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object, java.io.Serializable):void */
    public final void b(String str) {
        this.b.a("field16", (Object) false, (Serializable) str);
    }

    public final void b(List list) {
        ArrayList arrayList = new ArrayList();
        arrayList.addAll(list);
        u.b().execute(new ap(this, arrayList));
    }

    public final b c(String str) {
        b bVar = (b) this.f2963a.a((Serializable) str);
        if (bVar != null && !a.a((CharSequence) bVar.f)) {
            com.immomo.momo.service.bean.c.g gVar = new com.immomo.momo.service.bean.c.g();
            if (this.c.a(gVar, bVar.f)) {
                bVar.e = gVar;
            } else {
                bVar.e = new com.immomo.momo.service.bean.c.g(bVar.f);
            }
        }
        return bVar;
    }

    /* JADX WARNING: Unknown top exception splitter block from list: {B:24:0x0067=Splitter:B:24:0x0067, B:20:0x005d=Splitter:B:20:0x005d} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.List c() {
        /*
            r6 = this;
            java.lang.String r0 = "tiebarecommendlis"
            boolean r0 = com.immomo.momo.util.u.c(r0)
            if (r0 == 0) goto L_0x0011
            java.lang.String r0 = "tiebarecommendlis"
            java.lang.Object r0 = com.immomo.momo.util.u.b(r0)
            java.util.List r0 = (java.util.List) r0
        L_0x0010:
            return r0
        L_0x0011:
            java.util.ArrayList r2 = new java.util.ArrayList
            r2.<init>()
            java.io.File r3 = new java.io.File
            java.io.File r0 = com.immomo.momo.a.s()
            java.lang.String r1 = "recommend"
            r3.<init>(r0, r1)
            r1 = 0
            boolean r0 = r3.exists()     // Catch:{ IOException -> 0x005c, JSONException -> 0x0066 }
            if (r0 == 0) goto L_0x0084
            java.io.FileInputStream r0 = new java.io.FileInputStream     // Catch:{ IOException -> 0x005c, JSONException -> 0x0066 }
            r0.<init>(r3)     // Catch:{ IOException -> 0x005c, JSONException -> 0x0066 }
            byte[] r1 = android.support.v4.b.a.b(r0)     // Catch:{ IOException -> 0x007f, JSONException -> 0x007a, all -> 0x0075 }
            org.json.JSONArray r3 = new org.json.JSONArray     // Catch:{ IOException -> 0x007f, JSONException -> 0x007a, all -> 0x0075 }
            java.lang.String r4 = new java.lang.String     // Catch:{ IOException -> 0x007f, JSONException -> 0x007a, all -> 0x0075 }
            r4.<init>(r1)     // Catch:{ IOException -> 0x007f, JSONException -> 0x007a, all -> 0x0075 }
            r3.<init>(r4)     // Catch:{ IOException -> 0x007f, JSONException -> 0x007a, all -> 0x0075 }
            r1 = 0
        L_0x003c:
            int r4 = r3.length()     // Catch:{ IOException -> 0x007f, JSONException -> 0x007a, all -> 0x0075 }
            if (r1 < r4) goto L_0x004c
        L_0x0042:
            android.support.v4.b.a.a(r0)
        L_0x0045:
            java.lang.String r0 = "tiebarecommendlis"
            com.immomo.momo.util.u.a(r0, r2)
            r0 = r2
            goto L_0x0010
        L_0x004c:
            java.lang.String r4 = r3.getString(r1)     // Catch:{ IOException -> 0x007f, JSONException -> 0x007a, all -> 0x0075 }
            com.immomo.momo.service.bean.c.d r4 = r6.a(r4)     // Catch:{ IOException -> 0x007f, JSONException -> 0x007a, all -> 0x0075 }
            if (r4 == 0) goto L_0x0059
            r2.add(r4)     // Catch:{ IOException -> 0x007f, JSONException -> 0x007a, all -> 0x0075 }
        L_0x0059:
            int r1 = r1 + 1
            goto L_0x003c
        L_0x005c:
            r0 = move-exception
        L_0x005d:
            com.immomo.momo.util.m r3 = r6.e     // Catch:{ all -> 0x0070 }
            r3.a(r0)     // Catch:{ all -> 0x0070 }
            android.support.v4.b.a.a(r1)
            goto L_0x0045
        L_0x0066:
            r0 = move-exception
        L_0x0067:
            com.immomo.momo.util.m r3 = r6.e     // Catch:{ all -> 0x0070 }
            r3.a(r0)     // Catch:{ all -> 0x0070 }
            android.support.v4.b.a.a(r1)
            goto L_0x0045
        L_0x0070:
            r0 = move-exception
        L_0x0071:
            android.support.v4.b.a.a(r1)
            throw r0
        L_0x0075:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x0071
        L_0x007a:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x0067
        L_0x007f:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x005d
        L_0x0084:
            r0 = r1
            goto L_0x0042
        */
        throw new UnsupportedOperationException("Method not decompiled: com.immomo.momo.service.ao.c():java.util.List");
    }

    /* JADX WARNING: Unknown top exception splitter block from list: {B:15:0x00c4=Splitter:B:15:0x00c4, B:19:0x00cf=Splitter:B:19:0x00cf} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void c(java.util.List r9) {
        /*
            r8 = this;
            r1 = 0
            org.json.JSONArray r3 = new org.json.JSONArray     // Catch:{ IOException -> 0x00c3, JSONException -> 0x00ce }
            r3.<init>()     // Catch:{ IOException -> 0x00c3, JSONException -> 0x00ce }
            java.util.Iterator r2 = r9.iterator()     // Catch:{ IOException -> 0x00c3, JSONException -> 0x00ce }
        L_0x000a:
            boolean r0 = r2.hasNext()     // Catch:{ IOException -> 0x00c3, JSONException -> 0x00ce }
            if (r0 != 0) goto L_0x0041
            java.io.File r0 = new java.io.File     // Catch:{ IOException -> 0x00c3, JSONException -> 0x00ce }
            java.io.File r2 = com.immomo.momo.a.s()     // Catch:{ IOException -> 0x00c3, JSONException -> 0x00ce }
            java.lang.String r4 = "admin"
            r0.<init>(r2, r4)     // Catch:{ IOException -> 0x00c3, JSONException -> 0x00ce }
            boolean r2 = r0.exists()     // Catch:{ IOException -> 0x00c3, JSONException -> 0x00ce }
            if (r2 != 0) goto L_0x0024
            r0.createNewFile()     // Catch:{ IOException -> 0x00c3, JSONException -> 0x00ce }
        L_0x0024:
            java.io.BufferedWriter r2 = new java.io.BufferedWriter     // Catch:{ IOException -> 0x00c3, JSONException -> 0x00ce }
            java.io.OutputStreamWriter r4 = new java.io.OutputStreamWriter     // Catch:{ IOException -> 0x00c3, JSONException -> 0x00ce }
            java.io.FileOutputStream r5 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x00c3, JSONException -> 0x00ce }
            r5.<init>(r0)     // Catch:{ IOException -> 0x00c3, JSONException -> 0x00ce }
            r4.<init>(r5)     // Catch:{ IOException -> 0x00c3, JSONException -> 0x00ce }
            r2.<init>(r4)     // Catch:{ IOException -> 0x00c3, JSONException -> 0x00ce }
            java.lang.String r0 = r3.toString()     // Catch:{ IOException -> 0x00e4, JSONException -> 0x00e1, all -> 0x00de }
            r2.write(r0)     // Catch:{ IOException -> 0x00e4, JSONException -> 0x00e1, all -> 0x00de }
            r2.flush()     // Catch:{ IOException -> 0x00e4, JSONException -> 0x00e1, all -> 0x00de }
            android.support.v4.b.a.a(r2)
        L_0x0040:
            return
        L_0x0041:
            java.lang.Object r0 = r2.next()     // Catch:{ IOException -> 0x00c3, JSONException -> 0x00ce }
            com.immomo.momo.service.bean.c.e r0 = (com.immomo.momo.service.bean.c.e) r0     // Catch:{ IOException -> 0x00c3, JSONException -> 0x00ce }
            org.json.JSONObject r4 = new org.json.JSONObject     // Catch:{ IOException -> 0x00c3, JSONException -> 0x00ce }
            r4.<init>()     // Catch:{ IOException -> 0x00c3, JSONException -> 0x00ce }
            java.lang.String r5 = "floor"
            int r6 = r0.l     // Catch:{ IOException -> 0x00c3, JSONException -> 0x00ce }
            r4.put(r5, r6)     // Catch:{ IOException -> 0x00c3, JSONException -> 0x00ce }
            java.lang.String r5 = "action"
            java.lang.String r6 = r0.h     // Catch:{ IOException -> 0x00c3, JSONException -> 0x00ce }
            r4.put(r5, r6)     // Catch:{ IOException -> 0x00c3, JSONException -> 0x00ce }
            java.lang.String r5 = "cid"
            java.lang.String r6 = r0.f     // Catch:{ IOException -> 0x00c3, JSONException -> 0x00ce }
            r4.put(r5, r6)     // Catch:{ IOException -> 0x00c3, JSONException -> 0x00ce }
            java.lang.String r5 = "content"
            java.lang.String r6 = r0.k     // Catch:{ IOException -> 0x00c3, JSONException -> 0x00ce }
            r4.put(r5, r6)     // Catch:{ IOException -> 0x00c3, JSONException -> 0x00ce }
            java.lang.String r5 = "rcount"
            int r6 = r0.g     // Catch:{ IOException -> 0x00c3, JSONException -> 0x00ce }
            r4.put(r5, r6)     // Catch:{ IOException -> 0x00c3, JSONException -> 0x00ce }
            java.lang.String r5 = "tid"
            java.lang.String r6 = r0.c     // Catch:{ IOException -> 0x00c3, JSONException -> 0x00ce }
            r4.put(r5, r6)     // Catch:{ IOException -> 0x00c3, JSONException -> 0x00ce }
            java.lang.String r5 = "tname"
            java.lang.String r6 = r0.d     // Catch:{ IOException -> 0x00c3, JSONException -> 0x00ce }
            r4.put(r5, r6)     // Catch:{ IOException -> 0x00c3, JSONException -> 0x00ce }
            java.lang.String r5 = "pid"
            java.lang.String r6 = r0.e     // Catch:{ IOException -> 0x00c3, JSONException -> 0x00ce }
            r4.put(r5, r6)     // Catch:{ IOException -> 0x00c3, JSONException -> 0x00ce }
            java.lang.String r5 = "title"
            java.lang.String r6 = r0.j     // Catch:{ IOException -> 0x00c3, JSONException -> 0x00ce }
            r4.put(r5, r6)     // Catch:{ IOException -> 0x00c3, JSONException -> 0x00ce }
            java.lang.String r5 = "type"
            java.lang.String r6 = r0.b     // Catch:{ IOException -> 0x00c3, JSONException -> 0x00ce }
            r4.put(r5, r6)     // Catch:{ IOException -> 0x00c3, JSONException -> 0x00ce }
            java.lang.String r5 = "tyname"
            java.lang.String r6 = r0.f3020a     // Catch:{ IOException -> 0x00c3, JSONException -> 0x00ce }
            r4.put(r5, r6)     // Catch:{ IOException -> 0x00c3, JSONException -> 0x00ce }
            java.lang.String r5 = "utime"
            long r6 = r0.b()     // Catch:{ IOException -> 0x00c3, JSONException -> 0x00ce }
            r4.put(r5, r6)     // Catch:{ IOException -> 0x00c3, JSONException -> 0x00ce }
            java.lang.String r5 = "action_count"
            int r6 = r0.i     // Catch:{ IOException -> 0x00c3, JSONException -> 0x00ce }
            r4.put(r5, r6)     // Catch:{ IOException -> 0x00c3, JSONException -> 0x00ce }
            java.lang.String r5 = "lock"
            boolean r6 = r0.m     // Catch:{ IOException -> 0x00c3, JSONException -> 0x00ce }
            r4.put(r5, r6)     // Catch:{ IOException -> 0x00c3, JSONException -> 0x00ce }
            java.lang.String r5 = "delete"
            boolean r6 = r0.n     // Catch:{ IOException -> 0x00c3, JSONException -> 0x00ce }
            r4.put(r5, r6)     // Catch:{ IOException -> 0x00c3, JSONException -> 0x00ce }
            java.lang.String r5 = "owner"
            java.lang.String r0 = r0.o     // Catch:{ IOException -> 0x00c3, JSONException -> 0x00ce }
            r4.put(r5, r0)     // Catch:{ IOException -> 0x00c3, JSONException -> 0x00ce }
            r3.put(r4)     // Catch:{ IOException -> 0x00c3, JSONException -> 0x00ce }
            goto L_0x000a
        L_0x00c3:
            r0 = move-exception
        L_0x00c4:
            com.immomo.momo.util.m r2 = r8.e     // Catch:{ all -> 0x00d9 }
            r2.a(r0)     // Catch:{ all -> 0x00d9 }
            android.support.v4.b.a.a(r1)
            goto L_0x0040
        L_0x00ce:
            r0 = move-exception
        L_0x00cf:
            com.immomo.momo.util.m r2 = r8.e     // Catch:{ all -> 0x00d9 }
            r2.a(r0)     // Catch:{ all -> 0x00d9 }
            android.support.v4.b.a.a(r1)
            goto L_0x0040
        L_0x00d9:
            r0 = move-exception
        L_0x00da:
            android.support.v4.b.a.a(r1)
            throw r0
        L_0x00de:
            r0 = move-exception
            r1 = r2
            goto L_0x00da
        L_0x00e1:
            r0 = move-exception
            r1 = r2
            goto L_0x00cf
        L_0x00e4:
            r0 = move-exception
            r1 = r2
            goto L_0x00c4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.immomo.momo.service.ao.c(java.util.List):void");
    }

    /* JADX WARNING: Unknown top exception splitter block from list: {B:19:0x0052=Splitter:B:19:0x0052, B:15:0x0048=Splitter:B:15:0x0048} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.List d() {
        /*
            r7 = this;
            java.util.ArrayList r2 = new java.util.ArrayList
            r2.<init>()
            java.io.File r3 = new java.io.File
            java.io.File r0 = com.immomo.momo.a.s()
            java.lang.String r1 = "category"
            r3.<init>(r0, r1)
            r1 = 0
            boolean r0 = r3.exists()     // Catch:{ IOException -> 0x0047, JSONException -> 0x0051 }
            if (r0 == 0) goto L_0x006f
            java.io.FileInputStream r0 = new java.io.FileInputStream     // Catch:{ IOException -> 0x0047, JSONException -> 0x0051 }
            r0.<init>(r3)     // Catch:{ IOException -> 0x0047, JSONException -> 0x0051 }
            byte[] r1 = android.support.v4.b.a.b(r0)     // Catch:{ IOException -> 0x006a, JSONException -> 0x0065, all -> 0x0060 }
            org.json.JSONArray r3 = new org.json.JSONArray     // Catch:{ IOException -> 0x006a, JSONException -> 0x0065, all -> 0x0060 }
            java.lang.String r4 = new java.lang.String     // Catch:{ IOException -> 0x006a, JSONException -> 0x0065, all -> 0x0060 }
            r4.<init>(r1)     // Catch:{ IOException -> 0x006a, JSONException -> 0x0065, all -> 0x0060 }
            r3.<init>(r4)     // Catch:{ IOException -> 0x006a, JSONException -> 0x0065, all -> 0x0060 }
            r1 = 0
        L_0x002b:
            int r4 = r3.length()     // Catch:{ IOException -> 0x006a, JSONException -> 0x0065, all -> 0x0060 }
            if (r1 < r4) goto L_0x0035
        L_0x0031:
            android.support.v4.b.a.a(r0)
        L_0x0034:
            return r2
        L_0x0035:
            org.json.JSONObject r4 = r3.getJSONObject(r1)     // Catch:{ IOException -> 0x006a, JSONException -> 0x0065, all -> 0x0060 }
            com.immomo.momo.service.bean.c.f r5 = new com.immomo.momo.service.bean.c.f     // Catch:{ IOException -> 0x006a, JSONException -> 0x0065, all -> 0x0060 }
            r5.<init>()     // Catch:{ IOException -> 0x006a, JSONException -> 0x0065, all -> 0x0060 }
            r5.a(r4)     // Catch:{ IOException -> 0x006a, JSONException -> 0x0065, all -> 0x0060 }
            r2.add(r5)     // Catch:{ IOException -> 0x006a, JSONException -> 0x0065, all -> 0x0060 }
            int r1 = r1 + 1
            goto L_0x002b
        L_0x0047:
            r0 = move-exception
        L_0x0048:
            com.immomo.momo.util.m r3 = r7.e     // Catch:{ all -> 0x005b }
            r3.a(r0)     // Catch:{ all -> 0x005b }
            android.support.v4.b.a.a(r1)
            goto L_0x0034
        L_0x0051:
            r0 = move-exception
        L_0x0052:
            com.immomo.momo.util.m r3 = r7.e     // Catch:{ all -> 0x005b }
            r3.a(r0)     // Catch:{ all -> 0x005b }
            android.support.v4.b.a.a(r1)
            goto L_0x0034
        L_0x005b:
            r0 = move-exception
        L_0x005c:
            android.support.v4.b.a.a(r1)
            throw r0
        L_0x0060:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
            goto L_0x005c
        L_0x0065:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
            goto L_0x0052
        L_0x006a:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
            goto L_0x0048
        L_0x006f:
            r0 = r1
            goto L_0x0031
        */
        throw new UnsupportedOperationException("Method not decompiled: com.immomo.momo.service.ao.d():java.util.List");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object, java.io.Serializable):void
     arg types: [java.lang.String, boolean, java.lang.String]
     candidates:
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object[], java.lang.String):java.util.List
      com.immomo.momo.service.a.b.a(java.util.Map, java.lang.String[], java.lang.Object[]):void
      com.immomo.momo.service.a.b.a(java.lang.String[], java.lang.Object[], java.io.Serializable):void
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.String[], java.lang.String[]):java.lang.String[]
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object, java.io.Serializable):void */
    public final void d(String str) {
        this.f2963a.a("field18", (Object) false, (Serializable) str);
    }

    public final void d(List list) {
        com.immomo.momo.util.u.a("tiebamytiebas", list);
        JSONArray jSONArray = new JSONArray();
        BufferedWriter bufferedWriter = null;
        this.d.beginTransaction();
        try {
            Iterator it = list.iterator();
            while (it.hasNext()) {
                d dVar = (d) it.next();
                if (this.b.c(dVar.f3019a)) {
                    dVar.m = true;
                    this.b.b(dVar);
                } else {
                    this.b.a(dVar);
                }
                jSONArray.put(dVar.f3019a);
            }
            this.d.setTransactionSuccessful();
            File file = new File(com.immomo.momo.a.s(), "mytieba");
            if (!file.exists()) {
                file.createNewFile();
            }
            BufferedWriter bufferedWriter2 = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file)));
            try {
                bufferedWriter2.write(jSONArray.toString());
                bufferedWriter2.flush();
                this.d.endTransaction();
                a.a(bufferedWriter2);
            } catch (IOException e2) {
                e = e2;
                bufferedWriter = bufferedWriter2;
                try {
                    this.e.a((Throwable) e);
                    this.d.endTransaction();
                    a.a(bufferedWriter);
                } catch (Throwable th) {
                    th = th;
                    this.d.endTransaction();
                    a.a(bufferedWriter);
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                bufferedWriter = bufferedWriter2;
                this.d.endTransaction();
                a.a(bufferedWriter);
                throw th;
            }
        } catch (IOException e3) {
            e = e3;
        }
    }

    public final List e() {
        if (com.immomo.momo.util.u.c("tiebamycomments")) {
            return (List) com.immomo.momo.util.u.b("tiebamycomments");
        }
        ArrayList arrayList = new ArrayList();
        File file = new File(com.immomo.momo.a.u(), "mytiecomms");
        if (!file.exists()) {
            return arrayList;
        }
        try {
            return v.a().b(new JSONArray(h.a(file)));
        } catch (Exception e2) {
            this.e.a((Throwable) e2);
            return arrayList;
        }
    }

    public final void e(List list) {
        com.immomo.momo.util.u.a("tiebarecommendlis", list);
        JSONArray jSONArray = new JSONArray();
        BufferedWriter bufferedWriter = null;
        this.d.beginTransaction();
        try {
            Iterator it = list.iterator();
            while (it.hasNext()) {
                d dVar = (d) it.next();
                if (!this.b.c(dVar.f3019a)) {
                    this.b.a(dVar);
                }
                jSONArray.put(dVar.f3019a);
            }
            this.d.setTransactionSuccessful();
            File file = new File(com.immomo.momo.a.s(), "recommend");
            if (!file.exists()) {
                file.createNewFile();
            }
            BufferedWriter bufferedWriter2 = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file)));
            try {
                bufferedWriter2.write(jSONArray.toString());
                bufferedWriter2.flush();
                this.d.endTransaction();
                a.a(bufferedWriter2);
            } catch (IOException e2) {
                e = e2;
                bufferedWriter = bufferedWriter2;
                try {
                    this.e.a((Throwable) e);
                    this.d.endTransaction();
                    a.a(bufferedWriter);
                } catch (Throwable th) {
                    th = th;
                    this.d.endTransaction();
                    a.a(bufferedWriter);
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                bufferedWriter = bufferedWriter2;
                this.d.endTransaction();
                a.a(bufferedWriter);
                throw th;
            }
        } catch (IOException e3) {
            e = e3;
            this.e.a((Throwable) e);
            this.d.endTransaction();
            a.a(bufferedWriter);
        }
    }

    public final boolean e(String str) {
        return "1".equals(this.f2963a.c("field18", new String[]{Message.DBFIELD_ID}, new String[]{str}));
    }

    public final List f(String str) {
        ArrayList arrayList = new ArrayList();
        String[] b2 = a.b(this.b.c("field14", new String[]{Message.DBFIELD_ID}, new String[]{str}), ",");
        if (b2 != null && b2.length > 0) {
            for (String c2 : b2) {
                b c3 = c(c2);
                if (!(c3 == null || 2 == c3.o)) {
                    arrayList.add(c3);
                }
            }
        }
        return arrayList;
    }

    public final void f(List list) {
        JSONArray jSONArray = new JSONArray();
        BufferedWriter bufferedWriter = null;
        try {
            Iterator it = list.iterator();
            while (it.hasNext()) {
                jSONArray.put(((f) it.next()).a());
            }
            File file = new File(com.immomo.momo.a.s(), "category");
            if (!file.exists()) {
                file.createNewFile();
            }
            BufferedWriter bufferedWriter2 = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file)));
            try {
                bufferedWriter2.write(jSONArray.toString());
                bufferedWriter2.flush();
                a.a(bufferedWriter2);
            } catch (IOException e2) {
                e = e2;
                bufferedWriter = bufferedWriter2;
                try {
                    this.e.a((Throwable) e);
                    a.a(bufferedWriter);
                } catch (Throwable th) {
                    th = th;
                    a.a(bufferedWriter);
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                bufferedWriter = bufferedWriter2;
                a.a(bufferedWriter);
                throw th;
            }
        } catch (IOException e3) {
            e = e3;
        }
    }

    public final void g(List list) {
        this.d.beginTransaction();
        try {
            Iterator it = list.iterator();
            while (it.hasNext()) {
                b bVar = (b) it.next();
                if (this.f2963a.c(bVar.f3017a)) {
                    this.f2963a.b(bVar);
                } else {
                    this.f2963a.a(bVar);
                }
                if (bVar.e != null) {
                    this.c.e(bVar.e);
                }
            }
            this.d.setTransactionSuccessful();
        } finally {
            this.d.endTransaction();
        }
    }
}
