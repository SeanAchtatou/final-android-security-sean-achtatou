package com.immomo.momo.service.a;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.immomo.momo.service.bean.GameApp;
import com.immomo.momo.service.bean.Message;

public final class u extends b {
    public u(SQLiteDatabase sQLiteDatabase) {
        super(sQLiteDatabase, "games", Message.DBFIELD_ID);
    }

    private static void a(GameApp gameApp, Cursor cursor) {
        gameApp.readJson(c(cursor, Message.DBFIELD_SAYHI));
        gameApp.appid = c(cursor, Message.DBFIELD_ID);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Cursor cursor) {
        GameApp gameApp = new GameApp();
        a(gameApp, cursor);
        return gameApp;
    }

    public final void a(GameApp gameApp) {
        b(new String[]{Message.DBFIELD_ID, Message.DBFIELD_SAYHI}, new Object[]{gameApp.appid, gameApp.toJson()});
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void a(Object obj, Cursor cursor) {
        a((GameApp) obj, cursor);
    }
}
