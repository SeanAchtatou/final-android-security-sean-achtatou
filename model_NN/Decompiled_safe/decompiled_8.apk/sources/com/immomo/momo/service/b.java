package com.immomo.momo.service;

import android.database.sqlite.SQLiteDatabase;
import com.immomo.momo.util.m;

public abstract class b {

    /* renamed from: a  reason: collision with root package name */
    protected SQLiteDatabase f2968a = null;
    protected m b = new m(getClass().getSimpleName());

    public void a() {
        if (this.f2968a != null) {
            this.f2968a.close();
            this.f2968a = null;
        }
    }

    public final SQLiteDatabase b() {
        return this.f2968a;
    }
}
