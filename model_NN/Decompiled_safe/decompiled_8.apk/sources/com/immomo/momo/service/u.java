package com.immomo.momo.service;

import com.immomo.momo.g;
import com.immomo.momo.service.bean.GameApp;
import com.immomo.momo.service.bean.Message;
import java.io.Serializable;

public final class u {

    /* renamed from: a  reason: collision with root package name */
    private com.immomo.momo.service.a.u f3057a = new com.immomo.momo.service.a.u(g.d().e());

    public final GameApp a(String str) {
        return (GameApp) this.f3057a.a((Serializable) str);
    }

    public final void a(GameApp gameApp) {
        if (this.f3057a.c(gameApp.appid)) {
            this.f3057a.a(new String[]{Message.DBFIELD_SAYHI}, new Object[]{gameApp.toJson()}, new String[]{Message.DBFIELD_ID}, new String[]{gameApp.appid});
            return;
        }
        this.f3057a.a(gameApp);
    }
}
