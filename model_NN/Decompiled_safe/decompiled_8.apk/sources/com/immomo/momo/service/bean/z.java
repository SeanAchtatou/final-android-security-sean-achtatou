package com.immomo.momo.service.bean;

import java.util.List;

public final class z {

    /* renamed from: a  reason: collision with root package name */
    public List f3043a = null;
    private String b = null;

    public final String a() {
        return this.b;
    }

    public final void a(String str) {
        this.b = str;
    }

    public final String toString() {
        return "EventDynamicGroup [mDynamics=" + this.f3043a + ", Title=" + this.b + "]";
    }
}
