package com.immomo.momo.service;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.Message;
import com.immomo.momo.service.bean.b.a;
import com.immomo.momo.service.bean.b.b;
import java.io.File;
import java.lang.ref.SoftReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import mm.purchasesdk.PurchaseCode;

public final class aa {
    private static aa g = null;

    /* renamed from: a  reason: collision with root package name */
    public List f2956a = new ArrayList();
    List b = new ArrayList();
    private int c = 6;
    private Map d = new HashMap();
    private List e = new ArrayList();
    private Context f = null;
    private int h = 12;
    private ac i = null;

    private aa(Context context) {
        this.f = context;
    }

    public static aa a(Context context) {
        if (g == null) {
            g = new aa(context);
        }
        return g;
    }

    public static void a() {
    }

    /* access modifiers changed from: private */
    public void d(b bVar) {
        HashSet hashSet = new HashSet();
        hashSet.addAll(this.f2956a);
        bVar.d = !hashSet.add(bVar.c);
    }

    private void f() {
        if (this.f2956a.size() > 0) {
            ArrayList<String> arrayList = new ArrayList<>();
            for (String str : this.f2956a) {
                Cursor query = this.f.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, new String[]{"bucket_id"}, "_data=?", new String[]{str}, null);
                if (query.moveToFirst()) {
                    arrayList.add(query.getString(0));
                }
                query.close();
            }
            for (a aVar : this.e) {
                aVar.e = 0;
                for (String equalsIgnoreCase : arrayList) {
                    if (aVar.f3004a.equalsIgnoreCase(equalsIgnoreCase)) {
                        aVar.e++;
                    }
                }
            }
            return;
        }
        for (a aVar2 : this.e) {
            aVar2.e = 0;
        }
    }

    private List g() {
        ArrayList arrayList = new ArrayList();
        if (!g.f()) {
            return arrayList;
        }
        Cursor query = this.f.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, new String[]{"bucket_id", "bucket_display_name", "COUNT(DISTINCT _display_name) AS files_count", "MAX(_id)"}, "0==0) GROUP BY (bucket_id", null, null);
        while (query.moveToNext()) {
            a aVar = new a();
            aVar.f3004a = query.getString(0);
            aVar.b = query.getString(1);
            aVar.c = query.getInt(2);
            aVar.d = query.getInt(3);
            aVar.f = 2;
            arrayList.add(aVar);
        }
        return arrayList;
    }

    public final Bitmap a(int i2, boolean z) {
        Bitmap bitmap;
        OutOfMemoryError e2;
        Bitmap bitmap2;
        if (z) {
            return null;
        }
        if (this.d.get(Integer.valueOf(i2)) != null && ((SoftReference) this.d.get(Integer.valueOf(i2))).get() != null) {
            return (Bitmap) ((SoftReference) this.d.get(Integer.valueOf(i2))).get();
        }
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) this.f).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        try {
            Bitmap thumbnail = displayMetrics.widthPixels > 480 ? MediaStore.Images.Thumbnails.getThumbnail(this.f.getContentResolver(), (long) i2, 1, null) : MediaStore.Images.Thumbnails.getThumbnail(this.f.getContentResolver(), (long) i2, 3, null);
            if (thumbnail == null) {
                try {
                    Cursor query = this.f.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, new String[]{Message.DBFIELD_ID, "_data"}, "_id=?", new String[]{new StringBuilder(String.valueOf(i2)).toString()}, null);
                    if (query.moveToFirst()) {
                        File file = new File(query.getString(1));
                        if (file.exists()) {
                            bitmap2 = android.support.v4.b.a.a(file, (int) PurchaseCode.LOADCHANNEL_ERR, (int) PurchaseCode.LOADCHANNEL_ERR);
                            query.close();
                            bitmap = bitmap2;
                        }
                    }
                    bitmap2 = thumbnail;
                    try {
                        query.close();
                        bitmap = bitmap2;
                    } catch (OutOfMemoryError e3) {
                        OutOfMemoryError outOfMemoryError = e3;
                        bitmap = bitmap2;
                        e2 = outOfMemoryError;
                        e2.printStackTrace();
                        return bitmap;
                    }
                } catch (OutOfMemoryError e4) {
                    e2 = e4;
                    bitmap = thumbnail;
                    e2.printStackTrace();
                    return bitmap;
                }
            } else {
                bitmap = thumbnail;
            }
            try {
                this.d.put(Integer.valueOf(i2), new SoftReference(bitmap));
                return bitmap;
            } catch (OutOfMemoryError e5) {
                e2 = e5;
            }
        } catch (OutOfMemoryError e6) {
            OutOfMemoryError outOfMemoryError2 = e6;
            bitmap = null;
            e2 = outOfMemoryError2;
        }
    }

    public final List a(String str, ab abVar) {
        if (this.i != null) {
            this.i.a();
            this.i = null;
        }
        this.b.clear();
        Cursor query = this.f.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, new String[]{Message.DBFIELD_ID, "_data", "bucket_id"}, "bucket_id=?", new String[]{str}, "_id DESC");
        for (int i2 = 0; i2 < this.h && query.moveToNext(); i2++) {
            b bVar = new b();
            bVar.f3005a = str;
            bVar.b = query.getInt(0);
            bVar.c = query.getString(1);
            d(bVar);
            this.b.add(bVar);
        }
        if (query.getCount() < this.h) {
            query.close();
        } else {
            this.i = new ac(this, query, abVar);
            this.i.start();
        }
        return this.b;
    }

    public final void a(int i2) {
        this.c = i2;
    }

    public final void a(b bVar) {
        for (a aVar : this.e) {
            if (aVar.f3004a.equalsIgnoreCase(bVar.f3005a)) {
                if (bVar.d) {
                    aVar.e++;
                } else {
                    aVar.e--;
                }
            }
        }
    }

    public final int b() {
        return this.c;
    }

    public final b b(int i2) {
        return (b) this.b.get(i2);
    }

    public final void b(b bVar) {
        Iterator it = this.f2956a.iterator();
        while (it.hasNext()) {
            if (((String) it.next()).equalsIgnoreCase(bVar.c)) {
                it.remove();
                return;
            }
        }
    }

    public final int c() {
        return this.f2956a.size();
    }

    public final void c(b bVar) {
        for (String equalsIgnoreCase : this.f2956a) {
            if (equalsIgnoreCase.equalsIgnoreCase(bVar.c)) {
                return;
            }
        }
        this.f2956a.add(bVar.c);
    }

    public final boolean d() {
        return this.f2956a.size() < this.c;
    }

    public final List e() {
        if (this.e == null) {
            this.e = new ArrayList();
        }
        this.e.clear();
        this.e.addAll(g());
        f();
        return this.e;
    }
}
