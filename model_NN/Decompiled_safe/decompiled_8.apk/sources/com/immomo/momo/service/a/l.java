package com.immomo.momo.service.a;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.immomo.momo.service.bean.Message;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.service.bean.q;
import com.immomo.momo.service.bean.s;

public final class l extends b {
    public l(SQLiteDatabase sQLiteDatabase) {
        super(sQLiteDatabase, "emotion", "eid");
    }

    private static void a(q qVar, Cursor cursor) {
        boolean z = true;
        qVar.f3035a = cursor.getString(cursor.getColumnIndex("eid"));
        qVar.e = cursor.getString(cursor.getColumnIndex(Message.DBFIELD_LOCATIONJSON));
        qVar.m = cursor.getString(cursor.getColumnIndex("field26"));
        qVar.l = cursor.getString(cursor.getColumnIndex("field25"));
        qVar.b = cursor.getString(cursor.getColumnIndex(Message.DBFIELD_SAYHI));
        qVar.h = cursor.getInt(cursor.getColumnIndex(Message.DBFIELD_GROUPID));
        qVar.g = cursor.getString(cursor.getColumnIndex(Message.DBFIELD_CONVERLOCATIONJSON));
        qVar.r = cursor.getString(cursor.getColumnIndex("field14"));
        qVar.s = cursor.getString(cursor.getColumnIndex("field15"));
        qVar.i = cursor.getInt(cursor.getColumnIndex("field5"));
        qVar.x = cursor.getInt(cursor.getColumnIndex("field20")) == 1;
        qVar.y = cursor.getInt(cursor.getColumnIndex("field21")) == 1;
        qVar.z = cursor.getString(cursor.getColumnIndex("field22"));
        qVar.n = cursor.getString(cursor.getColumnIndex("field27"));
        qVar.k = cursor.getInt(cursor.getColumnIndex("field6"));
        qVar.o = new bf();
        qVar.o.h = c(cursor, "field11");
        qVar.o.i = c(cursor, "field12");
        qVar.o.b(c(cursor, "field13"));
        qVar.q = a(cursor, "field16") == 1;
        qVar.p = c(cursor, "field17");
        qVar.B = c(cursor, "field24");
        qVar.f = c(cursor, "field18");
        qVar.w = b(cursor, "field19");
        qVar.j = c(cursor, "field35");
        qVar.c = a(cursor, "field37");
        qVar.d = c(cursor, "field36");
        int a2 = a(cursor, "field29");
        if (a2 > 0) {
            s sVar = new s();
            qVar.A = sVar;
            sVar.f3037a = a2;
            sVar.e = c(cursor, "field31");
            sVar.d = c(cursor, "field28");
            sVar.c = a(cursor, "field32");
            sVar.b = a(cursor, "field30");
            if (a(cursor, "field33") != 1) {
                z = false;
            }
            sVar.f = z;
        }
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Cursor cursor) {
        q qVar = new q();
        a(qVar, cursor);
        return qVar;
    }

    public final void a(q qVar) {
        int i = -1;
        String str = null;
        String[] strArr = {"eid", Message.DBFIELD_LOCATIONJSON, "field26", "field25", Message.DBFIELD_SAYHI, Message.DBFIELD_GROUPID, Message.DBFIELD_CONVERLOCATIONJSON, "field5", "field20", "field21", "field22", "field27", "field11", "field12", "field13", "field14", "field15", "field16", "field17", "field24", "field18", "field19", "field31", "field28", "field32", "field30", "field29", "field33", "field35", "field37", "field36"};
        Object[] objArr = new Object[31];
        objArr[0] = qVar.f3035a;
        objArr[1] = qVar.e;
        objArr[2] = qVar.m;
        objArr[3] = qVar.l;
        objArr[4] = qVar.b;
        objArr[5] = Integer.valueOf(qVar.h);
        objArr[6] = qVar.g;
        objArr[7] = Integer.valueOf(qVar.i);
        objArr[8] = Boolean.valueOf(qVar.x);
        objArr[9] = Boolean.valueOf(qVar.y);
        objArr[10] = qVar.z;
        objArr[11] = qVar.n;
        objArr[12] = qVar.o != null ? qVar.o.h : null;
        objArr[13] = qVar.o != null ? qVar.o.i : null;
        objArr[14] = qVar.o != null ? qVar.o.l() : null;
        objArr[15] = qVar.r;
        objArr[16] = qVar.s;
        objArr[17] = Boolean.valueOf(qVar.q);
        objArr[18] = qVar.p;
        objArr[19] = qVar.B;
        objArr[20] = qVar.f;
        objArr[21] = Long.valueOf(qVar.w);
        objArr[22] = qVar.A != null ? qVar.A.e : null;
        if (qVar.A != null) {
            str = qVar.A.d;
        }
        objArr[23] = str;
        objArr[24] = Integer.valueOf(qVar.A != null ? qVar.A.c : -1);
        objArr[25] = Integer.valueOf(qVar.A != null ? qVar.A.b : -1);
        if (qVar.A != null) {
            i = qVar.A.f3037a;
        }
        objArr[26] = Integer.valueOf(i);
        objArr[27] = Boolean.valueOf(qVar.A != null ? qVar.A.f : false);
        objArr[28] = qVar.j;
        objArr[29] = Integer.valueOf(qVar.c);
        objArr[30] = qVar.d;
        b(strArr, objArr);
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void a(Object obj, Cursor cursor) {
        a((q) obj, cursor);
    }
}
