package com.immomo.momo.service.bean;

import java.util.Date;

public final class ak {

    /* renamed from: a  reason: collision with root package name */
    public String f2990a = null;
    public String b = null;
    public int c = 7;
    public int d = -1;
    public Date e = null;

    public final boolean equals(Object obj) {
        return (!(obj instanceof ak) || this.b == null) ? super.equals(obj) : this.b.equals(((ak) obj).b);
    }
}
