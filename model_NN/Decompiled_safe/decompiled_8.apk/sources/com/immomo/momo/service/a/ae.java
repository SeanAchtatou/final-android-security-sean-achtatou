package com.immomo.momo.service.a;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.immomo.momo.service.bean.Message;
import com.immomo.momo.service.bean.am;

public final class ae extends b {
    public ae(SQLiteDatabase sQLiteDatabase) {
        super(sQLiteDatabase, "industry", Message.DBFIELD_SAYHI);
    }

    private static void a(am amVar, Cursor cursor) {
        amVar.f2991a = cursor.getString(cursor.getColumnIndex(Message.DBFIELD_SAYHI));
        amVar.b = cursor.getString(cursor.getColumnIndex(Message.DBFIELD_LOCATIONJSON));
        amVar.c = cursor.getString(cursor.getColumnIndex(Message.DBFIELD_CONVERLOCATIONJSON));
        amVar.d = cursor.getString(cursor.getColumnIndex(Message.DBFIELD_GROUPID));
        cursor.getInt(cursor.getColumnIndex(Message.DBFIELD_ID));
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Cursor cursor) {
        am amVar = new am();
        a(amVar, cursor);
        return amVar;
    }

    public final void a(am amVar) {
        StringBuilder sb = new StringBuilder();
        sb.append("insert into " + this.f2954a + " (").append("field1, field3, field2, field4) values(");
        sb.append("?,?,?,?)");
        this.b.execSQL(sb.toString(), new Object[]{amVar.f2991a, amVar.c, amVar.b, amVar.d});
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void a(Object obj, Cursor cursor) {
        a((am) obj, cursor);
    }
}
