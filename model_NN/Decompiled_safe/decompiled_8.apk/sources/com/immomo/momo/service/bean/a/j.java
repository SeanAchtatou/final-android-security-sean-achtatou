package com.immomo.momo.service.bean.a;

import com.immomo.momo.service.bean.bf;
import java.io.Serializable;
import java.util.Date;

public final class j implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    public String f2979a;
    public String b;
    public Date c;
    public Date d;
    public Date e;
    public int f;
    public int g;
    public bf h;

    public final boolean equals(Object obj) {
        return this.f2979a.equals(((j) obj).f2979a);
    }

    public final String toString() {
        return "GroupUser [momoid=" + this.f2979a + ", groupId=" + this.b + ", joinTime=" + this.c + ", level=" + this.f + ", user=" + this.h + "]";
    }
}
