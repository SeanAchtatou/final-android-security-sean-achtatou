package com.immomo.momo.service.a;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.b.a;
import com.immomo.momo.service.bean.Message;
import com.immomo.momo.service.bean.az;

public final class as extends b {
    public as(SQLiteDatabase sQLiteDatabase) {
        super(sQLiteDatabase, "sitetypes", "c_id");
    }

    private static void a(az azVar, Cursor cursor) {
        azVar.f3002a = a.b(cursor.getString(cursor.getColumnIndex(Message.DBFIELD_LOCATIONJSON)), ",");
        azVar.c = cursor.getString(cursor.getColumnIndex("c_id"));
        azVar.b = cursor.getString(cursor.getColumnIndex(Message.DBFIELD_SAYHI));
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Cursor cursor) {
        az azVar = new az();
        a(azVar, cursor);
        return azVar;
    }

    public final /* synthetic */ void a(Object obj) {
        az azVar = (az) obj;
        b(new String[]{Message.DBFIELD_LOCATIONJSON, "c_id", Message.DBFIELD_SAYHI}, new Object[]{a.a(azVar.f3002a, ","), azVar.c, azVar.b});
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void a(Object obj, Cursor cursor) {
        a((az) obj, cursor);
    }
}
