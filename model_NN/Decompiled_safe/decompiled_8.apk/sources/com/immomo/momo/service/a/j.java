package com.immomo.momo.service.a;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.immomo.momo.service.bean.Message;
import com.immomo.momo.service.bean.p;

public final class j extends b {
    public j(SQLiteDatabase sQLiteDatabase) {
        super(sQLiteDatabase, "discuss_member");
    }

    private static void a(p pVar, Cursor cursor) {
        pVar.b = cursor.getString(cursor.getColumnIndex(Message.DBFIELD_GROUPID));
        pVar.f3034a = cursor.getString(cursor.getColumnIndex(Message.DBFIELD_SAYHI));
        pVar.f = cursor.getInt(cursor.getColumnIndex(Message.DBFIELD_CONVERLOCATIONJSON));
        pVar.g = cursor.getInt(cursor.getColumnIndex("field6"));
        pVar.c = a(cursor.getLong(cursor.getColumnIndex(Message.DBFIELD_LOCATIONJSON)));
        pVar.e = a(cursor.getLong(cursor.getColumnIndex("field8")));
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Cursor cursor) {
        p pVar = new p();
        a(pVar, cursor);
        return pVar;
    }

    public final void a(p pVar) {
        StringBuilder sb = new StringBuilder();
        sb.append("insert into " + this.f2954a + " (").append("field4, field2, field5, field8, field3, field6, field1) values(?,?,?,?,?,?,?)");
        a(sb.toString(), new Object[]{pVar.b, Long.valueOf(a(pVar.c)), Long.valueOf(a(pVar.d)), Long.valueOf(a(pVar.e)), Integer.valueOf(pVar.f), Integer.valueOf(pVar.g), pVar.f3034a});
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void a(Object obj, Cursor cursor) {
        a((p) obj, cursor);
    }

    public final void b(p pVar) {
        if (pVar.c == null) {
            StringBuilder sb = new StringBuilder();
            sb.append("update " + this.f2954a + " set ").append("field6=?, where field4=? and field1=?");
            a(sb.toString(), new Object[]{Integer.valueOf(pVar.g), pVar.b, pVar.f3034a});
            return;
        }
        StringBuilder sb2 = new StringBuilder();
        sb2.append("update " + this.f2954a + " set ").append("field2=?, field5=?, field8=?, field3=?, field6=? where field4=? and field1=?");
        a(sb2.toString(), new Object[]{Long.valueOf(a(pVar.c)), Long.valueOf(a(pVar.d)), Long.valueOf(a(pVar.e)), Integer.valueOf(pVar.f), Integer.valueOf(pVar.g), pVar.b, pVar.f3034a});
    }
}
