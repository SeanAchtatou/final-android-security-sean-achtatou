package com.immomo.momo.service;

import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.Message;
import com.immomo.momo.service.bean.at;

public final class af {
    private static af i = null;

    /* renamed from: a  reason: collision with root package name */
    private g f2958a;
    private ae b;
    private w c;
    private ag d;
    private c e;
    private t f;
    private SQLiteDatabase g;
    private at h;

    private af() {
        this.f2958a = null;
        this.b = null;
        this.c = null;
        this.d = null;
        this.e = null;
        this.f = null;
        this.g = null;
        this.b = new ae();
        this.c = new w();
        this.d = new ag();
        this.f2958a = new g();
        this.e = new c();
        this.f = new t();
        this.g = this.b.b();
        this.h = g.r();
        g.q();
    }

    public static synchronized af c() {
        af afVar;
        synchronized (af.class) {
            if (i == null) {
                try {
                    af afVar2 = new af();
                    i = afVar2;
                    if (afVar2.g == null) {
                        i = null;
                        afVar = new af();
                    }
                } catch (Exception e2) {
                }
            }
            afVar = i;
        }
        return afVar;
    }

    public static synchronized void r() {
        synchronized (af.class) {
            i = null;
        }
    }

    public final SQLiteDatabase a() {
        return this.g;
    }

    public final Bundle a(Bundle bundle) {
        int e2 = this.b.e();
        int e3 = this.c.e();
        int g2 = this.e.g();
        int e4 = this.f2958a.e();
        int c2 = this.f.c();
        int i2 = g.r().x ? e4 + e2 + e3 + g2 + c2 : e4 + e2 + e3 + c2;
        bundle.putInt("groupunreaded", e3);
        bundle.putInt("userunreaded", e2);
        bundle.putInt("totalunreaded", i2);
        bundle.putInt("contactnoticeunreded", g2);
        bundle.putInt("frienddiscover", c2);
        return bundle;
    }

    public final Message a(String str) {
        return this.b.e(str);
    }

    public final void a(int i2) {
        if (i2 < 0) {
            i2 = 0;
        }
        this.h.c("feedunread", Integer.valueOf(i2));
    }

    public final void a(Message message) {
        if (message.chatType == 2) {
            this.c.a(message);
        } else if (message.chatType == 1) {
            this.b.a(message);
        } else if (message.chatType == 3) {
            this.f2958a.a(message);
        } else {
            throw new IllegalArgumentException("error message type");
        }
    }

    public final void a(String str, int i2, int i3) {
        if (i3 == 2) {
            this.c.a(str, i2);
        } else if (i3 == 1) {
            this.b.a(str, i2);
        } else if (i3 == 3) {
            this.f2958a.a(str, i2);
        }
    }

    public final boolean a(String str, int i2) {
        if (i2 == 2) {
            return this.c.b(str);
        }
        if (i2 == 1) {
            return this.b.f(str);
        }
        if (i2 == 3) {
            return this.f2958a.b(str);
        }
        return false;
    }

    public final Bundle b(Bundle bundle) {
        int g2 = this.d.g();
        int j = j() + i();
        int n = n();
        int o = o();
        int t = t();
        bundle.putInt("gaunreaded", g2);
        bundle.putInt("feedunreaded", j);
        bundle.putInt("totalunreadeddiscover", n + o + g2 + j + t);
        bundle.putInt("eventdynamicunread", n);
        bundle.putInt("eventfeedcomments", o);
        bundle.putInt("eventtotalcount", o + n);
        bundle.putInt("tiebaunreaded", t);
        return bundle;
    }

    public final ae b() {
        return this.b;
    }

    public final void b(int i2) {
        if (i2 < 0) {
            i2 = 0;
        }
        this.h.c("feedcommentunread", Integer.valueOf(i2));
    }

    public final void b(Message message) {
        if (message.chatType == 2) {
            this.c.b(message);
        } else if (message.chatType == 1) {
            this.b.b(message);
        } else if (message.chatType == 3) {
            this.f2958a.b(message);
        }
    }

    public final void b(String str) {
        this.h.c("tiebareportcontent", str);
    }

    public final void c(int i2) {
        if (i2 < 0) {
            i2 = 0;
        }
        this.h.c("tiebacommentunread", Integer.valueOf(i2));
    }

    public final void c(String str) {
        this.h.c("lastfeedcontent", str);
    }

    public final int d() {
        return this.f2958a.e();
    }

    public final void d(int i2) {
        if (i2 < 0) {
            i2 = 0;
        }
        this.h.c("tiebareportunread", Integer.valueOf(i2));
    }

    public final int e() {
        return this.b.e();
    }

    public final void e(int i2) {
        if (i2 < 0) {
            i2 = 0;
        }
        this.h.c("dynamicunread", Integer.valueOf(i2));
    }

    public final int f() {
        return this.c.e();
    }

    public final void f(int i2) {
        if (i2 < 0) {
            i2 = 0;
        }
        this.h.c("eventfeedcommentunread", Integer.valueOf(i2));
    }

    public final int g() {
        return this.d.g();
    }

    public final int h() {
        return this.e.g();
    }

    public final int i() {
        return ((Integer) this.h.b("feedunread", 0)).intValue();
    }

    public final int j() {
        return ((Integer) this.h.b("feedcommentunread", 0)).intValue();
    }

    public final int k() {
        return ((Integer) this.h.b("tiebacommentunread", 0)).intValue();
    }

    public final int l() {
        return ((Integer) this.h.b("tiebareportunread", 0)).intValue();
    }

    public final String m() {
        return (String) this.h.b("tiebareportcontent", PoiTypeDef.All);
    }

    public final int n() {
        return ((Integer) this.h.b("dynamicunread", 0)).intValue();
    }

    public final int o() {
        return ((Integer) this.h.b("eventfeedcommentunread", 0)).intValue();
    }

    public final void p() {
        c().g.beginTransaction();
        try {
            this.b.c();
            this.c.c();
            this.f2958a.c();
            this.e.h();
            this.f.d();
            c().g.setTransactionSuccessful();
        } finally {
            c().g.endTransaction();
        }
    }

    public final int q() {
        if (this.g == null) {
            return 0;
        }
        return g.r().x ? this.b.e() + this.c.e() + this.e.g() + this.f2958a.e() + this.f.c() : this.b.e() + this.c.e() + this.f2958a.e() + this.f.c();
    }

    public final int s() {
        int j = j() + i();
        if (j >= 0) {
            return j;
        }
        return 0;
    }

    public final int t() {
        return k() + l();
    }
}
