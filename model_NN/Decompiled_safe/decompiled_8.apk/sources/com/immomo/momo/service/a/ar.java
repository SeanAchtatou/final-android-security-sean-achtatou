package com.immomo.momo.service.a;

import android.database.sqlite.SQLiteDatabase;
import android.support.v4.b.a;

public final class ar {
    private static ar b = null;

    /* renamed from: a  reason: collision with root package name */
    private aq f2953a = null;

    private ar(SQLiteDatabase sQLiteDatabase) {
        this.f2953a = new aq(sQLiteDatabase);
    }

    public static synchronized ar a(SQLiteDatabase sQLiteDatabase) {
        ar arVar;
        synchronized (ar.class) {
            if (b == null) {
                b = new ar(sQLiteDatabase);
            }
            arVar = b;
        }
        return arVar;
    }

    public static synchronized void b() {
        synchronized (ar.class) {
            b = null;
        }
    }

    public final synchronized int a() {
        int i;
        int i2 = 0;
        synchronized (this) {
            String a2 = this.f2953a.a("orderid", "orderid", new String[0], new String[0]);
            if (!a.a((CharSequence) a2)) {
                try {
                    i2 = Integer.parseInt(a2) + 0;
                } catch (Exception e) {
                }
            }
            i = i2 + 1;
        }
        return i;
    }
}
