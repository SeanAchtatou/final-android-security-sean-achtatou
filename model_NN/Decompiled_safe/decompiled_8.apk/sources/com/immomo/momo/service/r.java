package com.immomo.momo.service;

import com.amap.mapapi.location.LocationManagerProxy;
import com.immomo.momo.a;
import com.immomo.momo.g;
import com.immomo.momo.protocol.a.j;
import com.immomo.momo.service.a.m;
import com.immomo.momo.service.bean.aa;
import com.immomo.momo.service.bean.ab;
import com.immomo.momo.service.bean.ae;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.service.bean.u;
import com.immomo.momo.service.bean.z;
import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public final class r {

    /* renamed from: a  reason: collision with root package name */
    private m f3055a;
    private aq b;
    /* access modifiers changed from: private */
    public com.immomo.momo.util.m c;

    public r() {
        this.f3055a = null;
        this.b = null;
        this.c = new com.immomo.momo.util.m(this);
        this.f3055a = new m(g.d().h());
        this.b = new aq();
    }

    private static void a(List list, String str) {
        BufferedWriter bufferedWriter;
        try {
            File file = new File(a.r(), str);
            file.createNewFile();
            bufferedWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file)));
            try {
                JSONArray jSONArray = new JSONArray();
                if (list != null) {
                    Iterator it = list.iterator();
                    while (it.hasNext()) {
                        u uVar = (u) it.next();
                        JSONObject jSONObject = new JSONObject();
                        jSONObject.put("evid", uVar.f3038a);
                        jSONObject.put("name", uVar.b);
                        jSONObject.put("adss", uVar.e);
                        jSONObject.put("time", uVar.d);
                        jSONObject.put("distance", (double) uVar.a());
                        jSONObject.put("fee", uVar.g);
                        jSONObject.put("subject", uVar.i);
                        jSONObject.put(LocationManagerProxy.KEY_STATUS_CHANGED, uVar.j);
                        jSONObject.put("jcount", uVar.f());
                        jSONObject.put("ccount", uVar.d());
                        jSONObject.put("avatar", uVar.c);
                        jSONObject.put("hot", uVar.m ? 1 : 0);
                        jSONObject.put("today", uVar.n ? 1 : 0);
                        jSONObject.put("free", uVar.o ? 1 : 0);
                        jSONArray.put(jSONObject);
                    }
                }
                bufferedWriter.write(jSONArray.toString());
                bufferedWriter.flush();
                android.support.v4.b.a.a(bufferedWriter);
            } catch (Throwable th) {
                th = th;
                android.support.v4.b.a.a(bufferedWriter);
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            bufferedWriter = null;
            android.support.v4.b.a.a(bufferedWriter);
            throw th;
        }
    }

    public static List b(String str) {
        List list = (List) com.immomo.momo.util.u.b(String.valueOf(str) + "eventfeedcomment");
        return list != null ? list : new ArrayList();
    }

    public static void b(String str, List list) {
        com.immomo.momo.util.u.a(String.valueOf(str) + "eventfeedcomment", list);
    }

    private List d(String str) {
        BufferedInputStream bufferedInputStream;
        BufferedInputStream bufferedInputStream2 = null;
        ArrayList arrayList = new ArrayList();
        try {
            File file = new File(a.r(), str);
            if (file.exists()) {
                bufferedInputStream = new BufferedInputStream(new FileInputStream(file));
                try {
                    byte[] b2 = android.support.v4.b.a.b(bufferedInputStream);
                    if (b2 != null && b2.length > 0) {
                        JSONArray jSONArray = new JSONArray(new String(b2));
                        for (int i = 0; i < jSONArray.length(); i++) {
                            JSONObject jSONObject = jSONArray.getJSONObject(i);
                            u uVar = new u();
                            uVar.f3038a = jSONObject.optString("evid");
                            uVar.b = jSONObject.optString("name");
                            uVar.e = jSONObject.optString("adss");
                            uVar.d = jSONObject.optString("time");
                            uVar.a((float) jSONObject.optInt("distance"));
                            uVar.g = jSONObject.optString("fee");
                            uVar.c(jSONObject.optInt("jcount"));
                            uVar.i = jSONObject.optInt("subject");
                            uVar.j = jSONObject.optInt(LocationManagerProxy.KEY_STATUS_CHANGED);
                            uVar.a(jSONObject.optInt("ccount"));
                            uVar.c = jSONObject.optString("avatar");
                            if (1 == jSONObject.optInt("hot")) {
                                uVar.m = true;
                            } else {
                                uVar.m = false;
                            }
                            if (1 == jSONObject.optInt("today")) {
                                uVar.n = true;
                            } else {
                                uVar.n = false;
                            }
                            if (1 == jSONObject.optInt("free")) {
                                uVar.o = true;
                            } else {
                                uVar.o = false;
                            }
                            arrayList.add(uVar);
                        }
                    }
                } catch (Exception e) {
                    Exception exc = e;
                    bufferedInputStream2 = bufferedInputStream;
                    e = exc;
                } catch (Throwable th) {
                    Throwable th2 = th;
                    bufferedInputStream2 = bufferedInputStream;
                    th = th2;
                    android.support.v4.b.a.a((Closeable) bufferedInputStream2);
                    throw th;
                }
            } else {
                bufferedInputStream = null;
            }
            android.support.v4.b.a.a((Closeable) bufferedInputStream);
        } catch (Exception e2) {
            e = e2;
            try {
                this.c.a((Throwable) e);
                android.support.v4.b.a.a((Closeable) bufferedInputStream2);
                return arrayList;
            } catch (Throwable th3) {
                th = th3;
                android.support.v4.b.a.a((Closeable) bufferedInputStream2);
                throw th;
            }
        }
        return arrayList;
    }

    private List e() {
        BufferedInputStream bufferedInputStream;
        BufferedInputStream bufferedInputStream2 = null;
        ArrayList arrayList = new ArrayList();
        try {
            File file = new File(a.r(), "eventfeedcomments");
            if (file.exists()) {
                bufferedInputStream = new BufferedInputStream(new FileInputStream(file));
                try {
                    byte[] b2 = android.support.v4.b.a.b(bufferedInputStream);
                    if (b2 != null && b2.length > 0) {
                        JSONArray jSONArray = new JSONArray(new String(b2));
                        this.c.a(jSONArray);
                        for (int i = 0; i < jSONArray.length(); i++) {
                            ae aeVar = new ae();
                            JSONObject jSONObject = jSONArray.getJSONObject(i);
                            aeVar.i = jSONObject.optString("eventid");
                            aeVar.o = jSONObject.optInt("from");
                            aeVar.j = jSONObject.optString("replycontent");
                            aeVar.h = jSONObject.optString("feedid");
                            aeVar.f = jSONObject.optString("content");
                            long optLong = jSONObject.optLong("create_time");
                            if (optLong > 0) {
                                aeVar.a(new Date(optLong));
                            } else {
                                aeVar.a(null);
                            }
                            aeVar.l = jSONObject.optInt("srctype");
                            aeVar.n = jSONObject.optInt("content_type");
                            aeVar.b = jSONObject.optString("owner");
                            aeVar.k = jSONObject.optString("commentid");
                            aeVar.m = jSONObject.optInt("replytype");
                            JSONObject optJSONObject = jSONObject.optJSONObject("user");
                            if (optJSONObject != null) {
                                bf bfVar = new bf();
                                bfVar.h = optJSONObject.optString("momoid");
                                bfVar.i = optJSONObject.optString("name");
                                bfVar.ae = new String[]{optJSONObject.optString("avatar")};
                                aeVar.f2985a = bfVar;
                            }
                            JSONObject optJSONObject2 = jSONObject.optJSONObject("feed");
                            if (optJSONObject2 != null) {
                                ab abVar = new ab();
                                abVar.c = optJSONObject2.optString("owner");
                                abVar.b(optJSONObject2.optString("content"));
                                abVar.i = optJSONObject2.optInt(LocationManagerProxy.KEY_STATUS_CHANGED);
                                abVar.h = optJSONObject2.optString("feedid");
                                JSONArray optJSONArray = optJSONObject2.optJSONArray("pics");
                                if (optJSONArray != null && optJSONArray.length() > 0) {
                                    String[] strArr = new String[optJSONArray.length()];
                                    for (int i2 = 0; i2 < optJSONArray.length(); i2++) {
                                        strArr[i2] = optJSONArray.getString(i2);
                                    }
                                    abVar.a(strArr);
                                }
                                aeVar.g = abVar;
                            }
                            arrayList.add(aeVar);
                        }
                    }
                } catch (Exception e) {
                    Exception exc = e;
                    bufferedInputStream2 = bufferedInputStream;
                    e = exc;
                    try {
                        this.c.a((Throwable) e);
                        android.support.v4.b.a.a((Closeable) bufferedInputStream2);
                        return arrayList;
                    } catch (Throwable th) {
                        th = th;
                        android.support.v4.b.a.a((Closeable) bufferedInputStream2);
                        throw th;
                    }
                } catch (Throwable th2) {
                    Throwable th3 = th2;
                    bufferedInputStream2 = bufferedInputStream;
                    th = th3;
                    android.support.v4.b.a.a((Closeable) bufferedInputStream2);
                    throw th;
                }
            } else {
                this.c.a((Object) "eventfeedcomments not exists");
                bufferedInputStream = null;
            }
            android.support.v4.b.a.a((Closeable) bufferedInputStream);
        } catch (Exception e2) {
            e = e2;
            this.c.a((Throwable) e);
            android.support.v4.b.a.a((Closeable) bufferedInputStream2);
            return arrayList;
        }
        return arrayList;
    }

    public final List a() {
        if (com.immomo.momo.util.u.c("eventsfeedmycommens")) {
            return (List) com.immomo.momo.util.u.b("eventsfeedmycommens");
        }
        List e = e();
        com.immomo.momo.util.u.a("eventsfeedmycommens", e);
        return e;
    }

    public final List a(String str) {
        u c2 = c(str);
        if (c2 == null) {
            return new ArrayList();
        }
        if (c2.b() == null) {
            c2.a(new ArrayList());
        }
        return c2.b();
    }

    public final void a(aa aaVar, String str) {
        List a2 = a(str);
        if (a2 != null) {
            a2.remove(aaVar);
            if (com.immomo.momo.util.u.c(str)) {
                u uVar = (u) com.immomo.momo.util.u.b(str);
                uVar.setImageLoadFailed(false);
                uVar.setImageLoading(false);
                uVar.a(a2);
            }
        }
        u c2 = c(str);
        if (c2 != null) {
            c2.a(c2.d() - 1);
        }
    }

    public final void a(u uVar) {
        if (this.f3055a.c(uVar.f3038a)) {
            this.f3055a.b(uVar);
        } else {
            this.f3055a.a(uVar);
        }
        com.immomo.momo.util.u.a(uVar.f3038a, uVar);
        if (com.immomo.momo.util.u.c("eventnearbylist")) {
            for (u uVar2 : (List) com.immomo.momo.util.u.b("eventnearbylist")) {
                if (uVar.equals(uVar2)) {
                    uVar2.a(uVar.d());
                    uVar2.c(uVar.f());
                    return;
                }
            }
        }
    }

    public final void a(String str, List list) {
        com.immomo.momo.util.u.a(String.valueOf(str) + "member", list);
        this.b.a(list);
    }

    public final void a(List list) {
        this.f3055a.d();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            u uVar = (u) it.next();
            if (!this.f3055a.c(uVar.f3038a)) {
                this.f3055a.a(uVar);
                com.immomo.momo.util.u.a(uVar.f3038a, uVar);
            }
        }
        this.f3055a.f();
        this.f3055a.e();
        com.immomo.momo.util.u.a("eventnearbylist", list);
        try {
            a(list, "nearbyevent");
        } catch (Exception e) {
            this.c.a((Throwable) e);
        }
    }

    public final List b() {
        BufferedInputStream bufferedInputStream;
        BufferedInputStream bufferedInputStream2 = null;
        if (com.immomo.momo.util.u.c("eventdynamics")) {
            return (List) com.immomo.momo.util.u.b("eventdynamics");
        }
        ArrayList arrayList = new ArrayList();
        try {
            File file = new File(a.r(), "eventdynamics");
            if (file.exists()) {
                bufferedInputStream = new BufferedInputStream(new FileInputStream(file));
                try {
                    byte[] b2 = android.support.v4.b.a.b(bufferedInputStream);
                    if (b2 != null && b2.length > 0) {
                        JSONArray jSONArray = new JSONArray(new String(b2));
                        for (int i = 0; i < jSONArray.length(); i++) {
                            z zVar = new z();
                            zVar.a(jSONArray.getJSONObject(i).optString("title"));
                            JSONArray optJSONArray = jSONArray.getJSONObject(i).optJSONArray("events");
                            ArrayList arrayList2 = new ArrayList();
                            for (int i2 = 0; i2 < optJSONArray.length(); i2++) {
                                j.a();
                                arrayList2.add(j.a(optJSONArray.getJSONObject(i2), (List) null));
                            }
                            zVar.f3043a = arrayList2;
                            arrayList.add(zVar);
                        }
                    }
                } catch (Exception e) {
                    Exception exc = e;
                    bufferedInputStream2 = bufferedInputStream;
                    e = exc;
                    try {
                        this.c.a((Throwable) e);
                        android.support.v4.b.a.a((Closeable) bufferedInputStream2);
                        com.immomo.momo.util.u.a("eventdynamics", arrayList);
                        return arrayList;
                    } catch (Throwable th) {
                        th = th;
                        android.support.v4.b.a.a((Closeable) bufferedInputStream2);
                        throw th;
                    }
                } catch (Throwable th2) {
                    Throwable th3 = th2;
                    bufferedInputStream2 = bufferedInputStream;
                    th = th3;
                    android.support.v4.b.a.a((Closeable) bufferedInputStream2);
                    throw th;
                }
            } else {
                bufferedInputStream = null;
            }
            android.support.v4.b.a.a((Closeable) bufferedInputStream);
        } catch (Exception e2) {
            e = e2;
            this.c.a((Throwable) e);
            android.support.v4.b.a.a((Closeable) bufferedInputStream2);
            com.immomo.momo.util.u.a("eventdynamics", arrayList);
            return arrayList;
        }
        com.immomo.momo.util.u.a("eventdynamics", arrayList);
        return arrayList;
    }

    public final void b(List list) {
        this.f3055a.d();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            u uVar = (u) it.next();
            if (!this.f3055a.c(uVar.f3038a)) {
                this.f3055a.a(uVar);
                com.immomo.momo.util.u.a(uVar.f3038a, uVar);
            }
        }
        this.f3055a.f();
        this.f3055a.e();
        com.immomo.momo.util.u.a("historyEventlist", list);
        try {
            a(list, "historyevent");
        } catch (Exception e) {
            this.c.a((Throwable) e);
        }
    }

    public final u c(String str) {
        if (com.immomo.momo.util.u.c(str)) {
            u uVar = (u) com.immomo.momo.util.u.b(str);
            uVar.setImageLoadFailed(false);
            uVar.setImageLoading(false);
            return uVar;
        }
        u uVar2 = (u) this.f3055a.a((Serializable) str);
        if (uVar2 == null) {
            return uVar2;
        }
        com.immomo.momo.util.u.a(uVar2.f3038a, uVar2);
        return uVar2;
    }

    public final List c() {
        List d = d("nearbyevent");
        com.immomo.momo.util.u.a("eventnearbylist", d);
        return d;
    }

    public final void c(List list) {
        BufferedWriter bufferedWriter;
        com.immomo.momo.util.u.a("eventsfeedmycommens", list);
        try {
            File file = new File(a.r(), "eventfeedcomments");
            file.createNewFile();
            bufferedWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file)));
            try {
                JSONArray jSONArray = new JSONArray();
                if (list != null) {
                    Iterator it = list.iterator();
                    while (it.hasNext()) {
                        ae aeVar = (ae) it.next();
                        JSONObject jSONObject = new JSONObject();
                        jSONObject.put("eventid", aeVar.i);
                        jSONObject.put("from", aeVar.o);
                        jSONObject.put("replycontent", aeVar.j);
                        jSONObject.put("feedid", aeVar.h);
                        jSONObject.put("content", aeVar.f);
                        jSONObject.put("create_time", aeVar.a() != null ? aeVar.a().getTime() : 0);
                        jSONObject.put("srctype", aeVar.l);
                        jSONObject.put("content_type", aeVar.n);
                        jSONObject.put("owner", aeVar.b);
                        jSONObject.put("commentid", aeVar.k);
                        jSONObject.put("replytype", aeVar.m);
                        if (aeVar.g != null) {
                            JSONObject jSONObject2 = new JSONObject();
                            jSONObject2.put("owner", aeVar.g.c);
                            jSONObject2.put("content", aeVar.g.b());
                            jSONObject2.put(LocationManagerProxy.KEY_STATUS_CHANGED, aeVar.g.i);
                            jSONObject2.put("feedid", aeVar.g.h);
                            JSONArray jSONArray2 = new JSONArray();
                            if (aeVar.g.h()) {
                                for (String put : aeVar.g.j()) {
                                    jSONArray2.put(put);
                                }
                            }
                            jSONObject2.put("pics", jSONArray2);
                            jSONObject.put("feed", jSONObject2);
                        }
                        if (aeVar.f2985a != null) {
                            JSONObject jSONObject3 = new JSONObject();
                            jSONObject3.put("momoid", aeVar.f2985a.h);
                            jSONObject3.put("name", aeVar.f2985a.h());
                            jSONObject3.put("avatar", aeVar.f2985a.getLoadImageId());
                            jSONObject.put("user", jSONObject3);
                        }
                        jSONArray.put(jSONObject);
                    }
                }
                bufferedWriter.write(jSONArray.toString());
                bufferedWriter.flush();
                android.support.v4.b.a.a(bufferedWriter);
            } catch (Exception e) {
                e = e;
                try {
                    this.c.a((Throwable) e);
                    android.support.v4.b.a.a(bufferedWriter);
                } catch (Throwable th) {
                    th = th;
                    android.support.v4.b.a.a(bufferedWriter);
                    throw th;
                }
            }
        } catch (Exception e2) {
            e = e2;
            bufferedWriter = null;
            this.c.a((Throwable) e);
            android.support.v4.b.a.a(bufferedWriter);
        } catch (Throwable th2) {
            th = th2;
            bufferedWriter = null;
            android.support.v4.b.a.a(bufferedWriter);
            throw th;
        }
    }

    public final List d() {
        List d = d("historyevent");
        com.immomo.momo.util.u.a("historyEventlist", d);
        return d;
    }

    public final void d(List list) {
        com.immomo.momo.util.u.a("eventdynamics", list);
        ArrayList arrayList = new ArrayList();
        arrayList.addAll(list);
        com.immomo.momo.android.c.u.b().execute(new s(this, arrayList));
    }

    public final void e(List list) {
        this.f3055a.d();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            u uVar = (u) it.next();
            if (!this.f3055a.c(uVar.f3038a)) {
                this.f3055a.a(uVar);
                com.immomo.momo.util.u.a(uVar.f3038a, uVar);
            }
        }
        this.f3055a.f();
        this.f3055a.e();
    }

    public final void f(List list) {
        this.b.a(list);
    }
}
