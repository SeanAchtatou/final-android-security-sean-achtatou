package com.immomo.momo.service;

import com.immomo.momo.service.bean.bf;
import java.util.Comparator;
import java.util.List;

final class at implements Comparator {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ List f2966a;

    at(List list) {
        this.f2966a = list;
    }

    public final /* synthetic */ int compare(Object obj, Object obj2) {
        return this.f2966a.indexOf(((bf) obj).h) > this.f2966a.indexOf(((bf) obj2).h) ? 1 : -1;
    }
}
