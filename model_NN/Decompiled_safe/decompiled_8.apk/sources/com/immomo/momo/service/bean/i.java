package com.immomo.momo.service.bean;

import android.support.v4.b.a;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.g;
import com.immomo.momo.plugin.e.e;
import com.immomo.momo.service.c;
import java.util.Date;
import java.util.List;

public final class i {

    /* renamed from: a  reason: collision with root package name */
    private String f3027a;
    private boolean b = true;
    private String c;
    private String d;
    private Date e;
    private String f;
    private int g;
    private String h;
    private bf i;
    private List j = null;
    private List k = null;
    private String l;
    private boolean m = false;
    private boolean n = false;
    private int o = 1;
    private e p;
    private GameApp q;
    private String r;

    public final int a() {
        return this.g;
    }

    public final void a(float f2) {
        if (f2 == -2.0f) {
            this.f3027a = g.a((int) R.string.profile_distance_hide);
        } else if (f2 >= 0.0f) {
            this.f3027a = String.valueOf(a.a(f2 / 1000.0f)) + "km";
        } else {
            this.f3027a = g.a((int) R.string.profile_distance_unknown);
        }
    }

    public final void a(int i2) {
        this.g = i2;
    }

    public final void a(e eVar) {
        this.p = eVar;
    }

    public final void a(GameApp gameApp) {
        this.q = gameApp;
    }

    public final void a(bf bfVar) {
        this.i = bfVar;
    }

    public final void a(String str) {
        this.h = str;
    }

    public final void a(Date date) {
        this.e = date;
    }

    public final void a(List list) {
        this.j = list;
    }

    public final void a(boolean z) {
        this.b = z;
    }

    public final int b() {
        return this.o;
    }

    public final void b(int i2) {
        this.o = i2;
    }

    public final void b(String str) {
        this.c = str;
    }

    public final void b(List list) {
        this.k = list;
    }

    public final void b(boolean z) {
        this.m = z;
    }

    public final e c() {
        return this.p;
    }

    public final void c(String str) {
        this.d = str;
    }

    public final GameApp d() {
        return this.q;
    }

    public final void d(String str) {
        if (str == null) {
            str = PoiTypeDef.All;
        }
        this.l = str;
    }

    public final void e(String str) {
        this.f = str;
    }

    public final boolean e() {
        return this.b;
    }

    public final Date f() {
        return this.e;
    }

    public final void f(String str) {
        this.f3027a = str;
    }

    public final boolean g() {
        return this.m;
    }

    public final String h() {
        return this.h == null ? PoiTypeDef.All : this.h;
    }

    public final bf i() {
        return this.i;
    }

    public final String j() {
        int i2 = 0;
        if (this.r == null) {
            if (this.k == null || this.k.isEmpty()) {
                this.r = this.l;
            } else {
                StringBuilder sb = new StringBuilder(this.l);
                int i3 = 0;
                while (i3 < this.k.size()) {
                    int indexOf = sb.indexOf("%s", i2);
                    sb.replace(indexOf, indexOf + 2, ((k) this.k.get(i3)).f3029a);
                    i3++;
                    i2 = indexOf + 2;
                }
                this.r = sb.toString();
            }
            if (this.r != null) {
                this.r = this.r.replaceAll("&lsb;", "[").replaceAll("&rsb;", "]").replaceAll("&vb;", "|");
            }
        }
        if (this.r == null || !this.r.contains("&F7A4") || this.o != 1) {
            return this.r;
        }
        String l2 = l();
        String str = this.r;
        if (l2 == null) {
            l2 = PoiTypeDef.All;
        }
        return str.replaceAll("&F7A4", l2);
    }

    public final String k() {
        return this.c;
    }

    public final String l() {
        if (this.d == null && !a.a((CharSequence) this.c) && !this.n) {
            this.d = c.c().a(this.c);
            this.n = this.d == null;
        }
        return this.d;
    }

    public final String m() {
        if (this.i != null) {
            return this.i.h();
        }
        if (this.o != 1) {
            return (this.o == 2 || this.o == 3 || this.o == 4) ? this.p != null ? this.p.c : PoiTypeDef.All : (this.o != 6 || this.q == null) ? PoiTypeDef.All : this.q.appname;
        }
        String l2 = l();
        return a.a(l2) ? a.a(h()) ? PoiTypeDef.All : h() : l2;
    }

    public final String n() {
        return this.l;
    }

    public final String o() {
        return this.f;
    }

    public final List p() {
        return this.j;
    }

    public final boolean q() {
        return this.j != null && !this.j.isEmpty();
    }

    public final List r() {
        return this.k;
    }

    public final String s() {
        return this.f3027a;
    }
}
