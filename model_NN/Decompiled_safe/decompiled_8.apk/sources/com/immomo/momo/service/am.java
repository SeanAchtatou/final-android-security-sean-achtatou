package com.immomo.momo.service;

import android.database.sqlite.SQLiteDatabase;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.g;
import com.immomo.momo.service.a.an;
import com.immomo.momo.service.a.as;
import com.immomo.momo.service.a.at;
import com.immomo.momo.service.a.p;
import com.immomo.momo.service.bean.Message;
import com.immomo.momo.service.bean.a.a;
import com.immomo.momo.service.bean.ay;
import com.immomo.momo.service.bean.az;
import com.immomo.momo.util.u;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public final class am extends b {
    public as c;
    private at d;
    private y e;
    private an f;

    public am() {
        this(g.d().e());
        this.d = new at(g.d().h());
        new p(g.d().h());
        this.c = new as(g.d().h());
        new aq();
    }

    private am(SQLiteDatabase sQLiteDatabase) {
        this.d = null;
        this.e = null;
        this.f = null;
        this.c = null;
        this.f2968a = sQLiteDatabase;
        this.d = new at(sQLiteDatabase);
        this.e = new y(sQLiteDatabase);
        this.f = new an(sQLiteDatabase);
        new com.immomo.momo.service.a.am(sQLiteDatabase);
    }

    private boolean c(String str) {
        return this.d.c(str);
    }

    public final az a(String str) {
        List<az> list = u.c("sitetypelist") ? (List) u.b("sitetypelist") : null;
        if (list == null || list.isEmpty()) {
            list = this.c.b();
            az azVar = new az();
            azVar.c = "101";
            azVar.b = g.a((int) R.string.sitetype_hote);
            list.add(0, azVar);
            az azVar2 = new az();
            azVar2.c = "100";
            azVar2.b = g.a((int) R.string.sitetype_fav);
            list.add(1, azVar2);
        }
        u.a("sitetypelist", list);
        for (az azVar3 : list) {
            if (str.equals(azVar3.c)) {
                return azVar3;
            }
        }
        return null;
    }

    public final void a(ay ayVar) {
        if (c(ayVar.f3001a)) {
            this.d.a(new String[]{Message.DBFIELD_CONVERLOCATIONJSON, Message.DBFIELD_SAYHI, Message.DBFIELD_GROUPID, Message.DBFIELD_LOCATIONJSON}, new Object[]{Float.valueOf(ayVar.a()), Integer.valueOf(ayVar.c), ayVar.f, Integer.valueOf(ayVar.d)}, new String[]{"gs_siteid"}, new String[]{ayVar.f3001a});
            return;
        }
        this.d.b(new String[]{Message.DBFIELD_CONVERLOCATIONJSON, Message.DBFIELD_SAYHI, Message.DBFIELD_GROUPID, Message.DBFIELD_LOCATIONJSON, "gs_siteid"}, new Object[]{Float.valueOf(ayVar.a()), Integer.valueOf(ayVar.c), ayVar.f, Integer.valueOf(ayVar.d), ayVar.f3001a});
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.m.a(java.lang.String, java.lang.Throwable):void
     arg types: [java.lang.String, java.lang.Exception]
     candidates:
      com.immomo.momo.util.m.a(java.lang.Appendable, java.lang.Throwable):void
      com.immomo.momo.util.m.a(java.lang.Object, java.lang.Throwable):void
      com.immomo.momo.util.m.a(java.lang.String, java.io.File):void
      com.immomo.momo.util.m.a(java.lang.String, java.lang.Throwable):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.service.y.a(com.immomo.momo.service.bean.a.a, boolean):void
     arg types: [com.immomo.momo.service.bean.a.a, int]
     candidates:
      com.immomo.momo.service.y.a(com.immomo.momo.service.bean.a.a, java.lang.String):void
      com.immomo.momo.service.y.a(java.lang.String, int):void
      com.immomo.momo.service.y.a(java.lang.String, java.lang.String):void
      com.immomo.momo.service.y.a(java.lang.String, java.util.List):void
      com.immomo.momo.service.y.a(java.lang.String, boolean):void
      com.immomo.momo.service.y.a(java.lang.String, java.lang.String[]):void
      com.immomo.momo.service.y.a(java.util.List, java.lang.String):void
      com.immomo.momo.service.y.a(com.immomo.momo.service.bean.a.a, boolean):void */
    public final void a(List list) {
        try {
            this.f2968a.beginTransaction();
            this.f.c();
            HashMap hashMap = new HashMap();
            Iterator it = list.iterator();
            while (it.hasNext()) {
                ay ayVar = (ay) it.next();
                a(ayVar);
                hashMap.put(Message.DBFIELD_SAYHI, ayVar.f3001a);
                if (ayVar.g == null || ayVar.g.isEmpty()) {
                    hashMap.put(Message.DBFIELD_LOCATIONJSON, PoiTypeDef.All);
                } else {
                    String[] strArr = new String[ayVar.g.size()];
                    int i = 0;
                    for (a aVar : ayVar.g) {
                        this.e.a(aVar, false);
                        int i2 = i + 1;
                        strArr[i] = aVar.b;
                        i = i2;
                    }
                    hashMap.put(Message.DBFIELD_LOCATIONJSON, android.support.v4.b.a.a(strArr, ","));
                }
                this.f.a((Map) hashMap);
            }
            u.a("gropnearby", list);
            this.f2968a.setTransactionSuccessful();
        } catch (Exception e2) {
            this.b.a("addNearGroups failed", (Throwable) e2);
        } finally {
            this.f2968a.endTransaction();
        }
    }

    public final ay b(String str) {
        return (ay) this.d.a((Serializable) str);
    }

    public final void b(ay ayVar) {
        if (c(ayVar.f3001a)) {
            this.d.a(new String[]{Message.DBFIELD_CONVERLOCATIONJSON, Message.DBFIELD_GROUPID, "field10", Message.DBFIELD_LOCATIONJSON}, new Object[]{Float.valueOf(ayVar.a()), ayVar.f, Integer.valueOf(ayVar.o), Integer.valueOf(ayVar.d)}, new String[]{"gs_siteid"}, new Object[]{ayVar.f3001a});
            return;
        }
        this.d.a(ayVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object[], java.lang.String):java.util.List
     arg types: [java.lang.String, java.lang.String[], java.lang.String]
     candidates:
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object, java.io.Serializable):void
      com.immomo.momo.service.a.b.a(java.util.Map, java.lang.String[], java.lang.Object[]):void
      com.immomo.momo.service.a.b.a(java.lang.String[], java.lang.Object[], java.io.Serializable):void
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.String[], java.lang.String[]):java.lang.String[]
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object[], java.lang.String):java.util.List */
    public final List c() {
        if (u.c("gropnearby")) {
            return (List) u.b("gropnearby");
        }
        List<Map> b = this.f.b();
        if (b == null || b.isEmpty()) {
            return new ArrayList();
        }
        String[] strArr = new String[b.size()];
        HashMap hashMap = new HashMap();
        int i = 0;
        for (Map map : b) {
            int i2 = i + 1;
            strArr[i] = (String) map.get(Message.DBFIELD_SAYHI);
            hashMap.put(strArr[i2 - 1], (String) map.get(Message.DBFIELD_LOCATIONJSON));
            i = i2;
        }
        List<ay> a2 = this.d.a("gs_siteid", (Object[]) strArr, Message.DBFIELD_CONVERLOCATIONJSON);
        for (ay ayVar : a2) {
            String[] b2 = android.support.v4.b.a.b((String) hashMap.get(ayVar.f3001a), ",");
            if (b2 != null && b2.length > 0) {
                ayVar.g = this.e.a(b2);
            }
        }
        u.a("gropnearby", a2);
        return a2;
    }
}
