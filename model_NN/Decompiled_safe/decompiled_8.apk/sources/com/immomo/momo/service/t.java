package com.immomo.momo.service;

import android.database.sqlite.SQLiteDatabase;
import android.support.v4.b.a;
import com.immomo.momo.g;
import com.immomo.momo.service.a.aq;
import com.immomo.momo.service.a.ar;
import com.immomo.momo.service.a.s;
import com.immomo.momo.service.bean.Message;
import com.immomo.momo.service.bean.ag;
import com.immomo.momo.service.bean.ax;
import java.io.Serializable;
import java.util.Iterator;
import java.util.List;

public final class t extends b {
    s c = null;
    private aq d = null;

    public t() {
        this.f2968a = g.d().e();
        this.c = new s(this.f2968a);
        this.d = new aq(this.f2968a);
    }

    public t(SQLiteDatabase sQLiteDatabase) {
        this.f2968a = sQLiteDatabase;
        this.c = new s(sQLiteDatabase);
        this.d = new aq(sQLiteDatabase);
    }

    public final ag a(String str) {
        return (ag) this.c.a((Serializable) str);
    }

    public final List a(int i) {
        return this.c.a(new String[0], new String[0], "rowid", false, i, 21);
    }

    public final void a(ag agVar) {
        this.c.b(agVar);
    }

    public final void a(List list) {
        this.f2968a.beginTransaction();
        try {
            Iterator it = list.iterator();
            while (it.hasNext()) {
                this.c.b((Serializable) ((ag) it.next()).j());
            }
            if (this.c.c(new String[0], new String[0]) > 0) {
                String a2 = this.c.a(Message.DBFIELD_ID, "rowid", new String[0], new String[0]);
                if (!a.a((CharSequence) a2)) {
                    this.d.a("s_lastmsgid", a2, "-2240");
                }
            }
            this.f2968a.setTransactionSuccessful();
        } catch (Exception e) {
            this.b.a((Throwable) e);
        } finally {
            this.f2968a.endTransaction();
        }
    }

    public final void b(ag agVar) {
        try {
            this.f2968a.beginTransaction();
            boolean z = true;
            ax axVar = (ax) this.d.a((Serializable) "-2240");
            if (axVar == null) {
                axVar = new ax("-2240");
                z = false;
            }
            axVar.i = agVar.j();
            axVar.j = ar.a(this.f2968a).a();
            axVar.h = agVar.b();
            axVar.o = 8;
            if (z) {
                this.d.b(axVar);
            } else {
                this.d.a(axVar);
            }
            this.c.a(agVar);
            this.f2968a.setTransactionSuccessful();
        } catch (Exception e) {
            this.b.a((Throwable) e);
        } finally {
            this.f2968a.endTransaction();
        }
    }

    public final boolean b(String str) {
        return this.c.a(str) != null;
    }

    public final int c() {
        return this.c.c(new String[]{Message.DBFIELD_CONVERLOCATIONJSON}, new String[]{"0"});
    }

    public final void d() {
        this.c.a(new String[]{Message.DBFIELD_CONVERLOCATIONJSON}, new Object[]{true}, new String[0], new String[0]);
    }
}
