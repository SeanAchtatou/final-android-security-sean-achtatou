package com.immomo.momo.service;

final class d {

    /* renamed from: a  reason: collision with root package name */
    int f3044a;
    String b;
    String c;
    String d;
    private /* synthetic */ c e;

    private d(c cVar) {
        this.e = cVar;
    }

    /* synthetic */ d(c cVar, byte b2) {
        this(cVar);
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        d dVar = (d) obj;
        if (!this.e.equals(dVar.e)) {
            return false;
        }
        return this.d == null ? dVar.d == null : this.d.equals(dVar.d);
    }

    public final int hashCode() {
        return (this.d == null ? 0 : this.d.hashCode()) + ((this.e.hashCode() + 31) * 31);
    }

    public final String toString() {
        return "Contact [contactId=" + this.f3044a + ", contactName=" + this.b + ", contactNumber=" + this.c + ", contactMd5NUmber=" + this.d + "]";
    }
}
