package com.immomo.momo.service.a;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public final class c extends b {
    public c(SQLiteDatabase sQLiteDatabase) {
        super(sQLiteDatabase, "blacklist", "b_momoid");
    }

    private static void a(Map map, Cursor cursor) {
        map.put(cursor.getString(cursor.getColumnIndex("b_momoid")), a(cursor.getLong(cursor.getColumnIndex("b_blacktime"))));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.service.a.c.a(java.util.Map, android.database.Cursor):void
     arg types: [java.util.HashMap, android.database.Cursor]
     candidates:
      com.immomo.momo.service.a.c.a(java.lang.Object, android.database.Cursor):void
      com.immomo.momo.service.a.b.a(android.database.Cursor, java.lang.String):int
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.String[]):android.database.Cursor
      com.immomo.momo.service.a.b.a(java.lang.String[], java.lang.String[]):java.util.List
      com.immomo.momo.service.a.b.a(java.lang.Object, android.database.Cursor):void
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object):void
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object[]):void
      com.immomo.momo.service.a.b.a(java.lang.String[], java.lang.Object[]):void
      com.immomo.momo.service.a.b.a(java.lang.Object, java.io.Serializable):boolean
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.String):boolean
      com.immomo.momo.service.a.c.a(java.util.Map, android.database.Cursor):void */
    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Cursor cursor) {
        HashMap hashMap = new HashMap();
        a((Map) hashMap, cursor);
        return hashMap;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object[]):void
     arg types: [java.lang.String, java.lang.String[]]
     candidates:
      com.immomo.momo.service.a.c.a(java.util.Map, android.database.Cursor):void
      com.immomo.momo.service.a.c.a(java.lang.Object, android.database.Cursor):void
      com.immomo.momo.service.a.b.a(android.database.Cursor, java.lang.String):int
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.String[]):android.database.Cursor
      com.immomo.momo.service.a.b.a(java.lang.String[], java.lang.String[]):java.util.List
      com.immomo.momo.service.a.b.a(java.lang.Object, android.database.Cursor):void
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object):void
      com.immomo.momo.service.a.b.a(java.lang.String[], java.lang.Object[]):void
      com.immomo.momo.service.a.b.a(java.lang.Object, java.io.Serializable):boolean
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.String):boolean
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object[]):void */
    public final /* synthetic */ void a(Object obj) {
        StringBuilder sb = new StringBuilder();
        sb.append("insert into " + this.f2954a + " (").append("b_momoid, b_blacktime) values(?,?)");
        Map.Entry entry = (Map.Entry) ((Map) obj).entrySet().iterator().next();
        String sb2 = new StringBuilder().append(a((Date) entry.getValue())).toString();
        a(sb.toString(), (Object[]) new String[]{(String) entry.getKey(), sb2});
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void a(Object obj, Cursor cursor) {
        a((Map) obj, cursor);
    }
}
