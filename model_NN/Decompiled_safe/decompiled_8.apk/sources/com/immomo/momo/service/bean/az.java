package com.immomo.momo.service.bean;

import com.amap.mapapi.poisearch.PoiTypeDef;

public final class az {

    /* renamed from: a  reason: collision with root package name */
    public String[] f3002a = null;
    public String b = null;
    public String c = null;

    public final String a() {
        return (this.f3002a == null || this.f3002a.length <= 0) ? PoiTypeDef.All : this.f3002a[0];
    }

    public final String toString() {
        return "SiteType [name=" + this.b + ", id=" + this.c + "]";
    }
}
