package com.immomo.momo.service.bean;

import android.support.v4.b.a;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class k {

    /* renamed from: a  reason: collision with root package name */
    public String f3029a;
    private String b;
    private String c;

    public static void a(StringBuilder sb, StringBuilder sb2, List list) {
        Matcher matcher = Pattern.compile("(\\[.*?\\|.*?\\|.*?\\])").matcher(sb);
        int i = 0;
        while (matcher.find()) {
            k kVar = new k();
            String group = matcher.group();
            if (group != null && !a.a((CharSequence) group)) {
                StringBuilder sb3 = new StringBuilder(group);
                sb3.deleteCharAt(0);
                sb3.deleteCharAt(sb3.length() - 1);
                String[] split = sb3.toString().split("\\|");
                if (split.length > 0) {
                    kVar.f3029a = split[0];
                    if (split.length > 1) {
                        kVar.b = split[1];
                        if (split.length > 2) {
                            kVar.c = split[2];
                        }
                    }
                }
            }
            list.add(kVar);
            sb2.append(sb.substring(i, matcher.start()));
            sb2.append("%s");
            i = matcher.end();
        }
        sb2.append(sb.substring(i, sb.length()));
    }

    public final String toString() {
        return "[" + this.f3029a + "|" + this.b + "|" + this.c + "]";
    }
}
