package com.immomo.momo.service.bean;

import android.graphics.Bitmap;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.a.a.f.a;
import com.immomo.a.a.f.b;
import com.immomo.momo.g;
import java.io.File;
import java.net.URLEncoder;
import java.util.Date;

public final class d extends al {

    /* renamed from: a  reason: collision with root package name */
    public String f3022a;
    public int b;
    public int c;
    public String d;
    public Date e;
    public Date f;
    public String g;
    public int h;
    public File i;
    public boolean j = false;
    public boolean k = false;
    public Date l = null;
    public String m;
    public String n;
    public boolean o = false;
    public long p;
    private Bitmap q = null;

    public static String a(String str, String str2) {
        String str3;
        String str4;
        String W = g.W();
        String b2 = g.q() != null ? a.b(g.q().h) : b.a();
        String str5 = g.q() != null ? String.valueOf(g.q().S) + "x" + g.q().T : "0x0";
        String C = g.C();
        String F = g.F();
        if (g.aa()) {
            str4 = "1";
            String E = g.E();
            str3 = !a.a(E) ? a.b(E.toLowerCase()) : PoiTypeDef.All;
        } else {
            str3 = PoiTypeDef.All;
            str4 = "0";
        }
        return a(a(a(a(a(a(a(a(a(a(a(a(str, "f", W), "j", b2), "l", str5), "n", str3), "o", b2), "q", C), "r", F), "u", str2), "t", new StringBuilder(String.valueOf(System.currentTimeMillis() / 1000)).toString()), "w", str4), "x", g.h()), "y", "陌陌");
    }

    private static String a(String str, String str2, String str3) {
        if (str3 == null) {
            str3 = PoiTypeDef.All;
        }
        return str.replaceAll("\\{" + str2 + "\\}", URLEncoder.encode(str3));
    }

    public final Bitmap a() {
        return this.q;
    }

    public final void a(Bitmap bitmap) {
        this.q = bitmap;
    }

    public final boolean b() {
        return this.i != null && this.i.exists();
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        d dVar = (d) obj;
        if (this.f3022a == null) {
            if (dVar.f3022a != null) {
                return false;
            }
        } else if (!this.f3022a.equals(dVar.f3022a)) {
            return false;
        }
        return this.h == dVar.h;
    }

    public final String getLoadImageId() {
        return this.d;
    }

    public final int hashCode() {
        return (((this.f3022a == null ? 0 : this.f3022a.hashCode()) + 31) * 31) + this.h;
    }

    public final String toString() {
        return "Banner [bannerid=" + this.f3022a + ", linkType=" + this.b + ", duration=" + this.c + ", image=" + this.d + ", startTime=" + this.e + ", endTime=" + this.f + ", url=" + this.g + ", panelId=" + this.h + ", cacheFilePath=" + this.i + "]";
    }
}
