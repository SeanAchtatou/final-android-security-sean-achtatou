package com.immomo.momo.service.bean;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public final class y extends al {

    /* renamed from: a  reason: collision with root package name */
    private List f3042a = new ArrayList();
    private String b = null;
    private boolean c = false;
    private int d = 0;
    private int e = 0;
    private String f = null;
    private String g = null;
    private String h = null;
    private String i = null;
    private String j = null;
    private Date k = null;
    private boolean l = true;
    private int m = 0;
    private int n = 0;

    public final void a(int i2) {
        if (i2 < 0) {
            i2 = 0;
        }
        this.m = i2;
    }

    public final void a(String str) {
        this.j = str;
    }

    public final void a(Date date) {
        this.k = date;
    }

    public final void a(boolean z) {
        this.l = !z;
    }

    public final boolean a() {
        return !this.l;
    }

    public final int b() {
        return this.m;
    }

    public final void b(int i2) {
        if (i2 < 0) {
            i2 = 0;
        }
        this.n = i2;
    }

    public final void b(String str) {
        this.h = str;
    }

    public final int c() {
        return this.n;
    }

    public final void c(int i2) {
        this.e = i2;
    }

    public final void c(String str) {
        this.i = str;
    }

    public final Date d() {
        return this.k;
    }

    public final void d(int i2) {
        this.d = i2;
    }

    public final void d(String str) {
        this.g = str;
    }

    public final String e() {
        return this.j;
    }

    public final void e(String str) {
        this.f = str;
    }

    public final String f() {
        return this.h;
    }

    public final void f(String str) {
        this.b = str;
    }

    public final String g() {
        return this.i;
    }

    public final void g(String str) {
        this.f3042a.add(str);
    }

    public final String h() {
        return this.g;
    }

    public final String i() {
        return this.f;
    }

    public final int j() {
        return this.e;
    }

    public final int k() {
        return this.d;
    }

    public final void l() {
        this.c = true;
    }

    public final boolean m() {
        return this.c;
    }

    public final String n() {
        return this.b;
    }

    public final List o() {
        if (this.f3042a == null) {
            this.f3042a = new ArrayList();
        }
        return this.f3042a;
    }

    public final String toString() {
        return "EventDynamic [mTimeDesc=" + this.b + ", mIsMine=" + this.c + ", mCommentCount=" + this.d + ", mJoinCount=" + this.e + ", mEventName=" + this.f + ", mEventId=" + this.g + ", mContentAvator=" + this.h + ", mContentText=" + this.i + ", mHilightContent=" + this.j + ", mStartTime=" + ((Object) null) + ", mEndTime=" + ((Object) null) + ", mUpdateTime=" + this.k + ", mIsNew=" + this.l + ", mUpdateJoin=" + this.m + ", mUpdateComment=" + this.n + "]";
    }
}
