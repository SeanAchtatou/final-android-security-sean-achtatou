package com.immomo.momo.service.bean.c;

import android.support.v4.b.a;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.service.bean.al;
import java.util.Date;

public final class b extends al {

    /* renamed from: a  reason: collision with root package name */
    public String f3017a;
    public d b;
    public String c;
    public String d;
    public g e;
    public String f;
    public String g;
    public String h;
    public Date i;
    public Date j;
    public boolean k;
    public boolean l;
    public boolean m;
    public int n;
    public int o;
    public boolean p;
    public boolean q;
    public String r;
    public String s;
    public String t;
    public String u;
    private String[] v;
    private al[] w;

    public final int a() {
        if (this.v == null) {
            return 0;
        }
        if (this.v == null || !a.a((CharSequence) this.u)) {
            return (this.v == null || a.a(this.u)) ? 0 : 2;
        }
        return 1;
    }

    public final void a(String[] strArr) {
        this.v = strArr;
        if (strArr != null && strArr.length > 0) {
            this.w = new al[strArr.length];
            for (int i2 = 0; i2 < strArr.length; i2++) {
                this.w[i2] = new al(strArr[i2]);
            }
        }
    }

    public final int b() {
        if (this.v != null) {
            return this.v.length;
        }
        return 0;
    }

    public final String[] c() {
        return this.v;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        b bVar = (b) obj;
        return this.f3017a == null ? bVar.f3017a == null : this.f3017a.equals(bVar.f3017a);
    }

    public final String getLoadImageId() {
        return (this.v == null || this.v.length <= 0) ? PoiTypeDef.All : this.v[0];
    }

    public final int hashCode() {
        return (this.f3017a == null ? 0 : this.f3017a.hashCode()) + 31;
    }
}
