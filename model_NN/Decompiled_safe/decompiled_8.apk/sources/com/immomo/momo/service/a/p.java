package com.immomo.momo.service.a;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.b.a;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.GameApp;
import com.immomo.momo.service.bean.Message;
import com.immomo.momo.service.bean.ab;
import com.immomo.momo.service.bean.ac;
import com.immomo.momo.service.bean.u;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import org.json.JSONException;
import org.json.JSONObject;

public final class p extends b {
    private static Set d = new HashSet();

    public p(SQLiteDatabase sQLiteDatabase) {
        super(sQLiteDatabase, "sitefeed", "sf_id");
    }

    /* access modifiers changed from: private */
    public void a(ab abVar, Cursor cursor) {
        abVar.a(a(cursor.getLong(cursor.getColumnIndex(Message.DBFIELD_GROUPID))));
        abVar.a(a.b(cursor.getString(cursor.getColumnIndex("field5")), ","));
        abVar.h = cursor.getString(cursor.getColumnIndex("sf_id"));
        abVar.e = cursor.getString(cursor.getColumnIndex(Message.DBFIELD_LOCATIONJSON));
        abVar.b(cursor.getString(cursor.getColumnIndex("field7")));
        abVar.c = cursor.getString(cursor.getColumnIndex(Message.DBFIELD_SAYHI));
        abVar.g = cursor.getInt(cursor.getColumnIndex("field6"));
        abVar.a(a(cursor.getLong(cursor.getColumnIndex(Message.DBFIELD_GROUPID))));
        abVar.i = cursor.getInt(cursor.getColumnIndex("field8"));
        abVar.a(cursor.getFloat(cursor.getColumnIndex("field10")));
        abVar.j = cursor.getString(cursor.getColumnIndex("field11"));
        abVar.a(cursor.getString(cursor.getColumnIndex("field16")));
        abVar.k = cursor.getString(cursor.getColumnIndex("field12"));
        String c = c(cursor, "field13");
        if (!a.a((CharSequence) c)) {
            abVar.n = GameApp.initWithJson(c);
        }
        String c2 = c(cursor, "field14");
        if (!a.a((CharSequence) c2)) {
            abVar.o = u.a(c2);
        }
        if (!a.a((CharSequence) abVar.h)) {
            d.add(abVar.h);
        }
        String c3 = c(cursor, "field15");
        if (!a.a((CharSequence) c3)) {
            ac acVar = new ac();
            try {
                JSONObject jSONObject = new JSONObject(c3);
                acVar.f2983a = jSONObject.getString("title");
                acVar.b = jSONObject.optString("desc");
                acVar.c = jSONObject.optString("icon");
                acVar.d = jSONObject.optString("action");
                abVar.p = acVar;
            } catch (JSONException e) {
                this.c.a((Throwable) e);
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public ab a(Cursor cursor) {
        ab abVar = new ab();
        a(abVar, cursor);
        return abVar;
    }

    public static void g() {
        Set set = d;
        String[] strArr = new String[set.size()];
        set.toArray(strArr);
        set.clear();
        if (g.d().h() != null) {
            new p(g.d().h()).a("field9", new Date(), "sf_id", strArr);
        }
    }

    public final void a(ab abVar) {
        String str = null;
        String[] strArr = {"field5", "field6", "field8", "sf_id", Message.DBFIELD_LOCATIONJSON, Message.DBFIELD_SAYHI, "field7", Message.DBFIELD_GROUPID, "field9", "field10", "field11", "field12", "field16", "field13", "field14", "field15"};
        Object[] objArr = new Object[16];
        objArr[0] = a.a(abVar.j(), ",");
        objArr[1] = Integer.valueOf(abVar.g);
        objArr[2] = Integer.valueOf(abVar.i);
        objArr[3] = abVar.h;
        objArr[4] = abVar.e;
        objArr[5] = abVar.c;
        objArr[6] = abVar.b();
        objArr[7] = abVar.d();
        objArr[8] = new Date();
        objArr[9] = Float.valueOf(abVar.e());
        objArr[10] = abVar.j;
        objArr[11] = abVar.k;
        objArr[12] = abVar.a();
        objArr[13] = abVar.n != null ? abVar.n.toJson() : null;
        objArr[14] = abVar.o != null ? abVar.o.c() : null;
        if (abVar.p != null) {
            str = abVar.p.a();
        }
        objArr[15] = str;
        b(strArr, objArr);
    }

    public final void b(ab abVar) {
        String str = null;
        String[] strArr = {"field5", "field6", "field8", Message.DBFIELD_LOCATIONJSON, Message.DBFIELD_SAYHI, "field7", Message.DBFIELD_GROUPID, "field10", "field11", "field12", "field16", "field13", "field14", "field15"};
        Object[] objArr = new Object[14];
        objArr[0] = a.a(abVar.j(), ",");
        objArr[1] = Integer.valueOf(abVar.g);
        objArr[2] = Integer.valueOf(abVar.i);
        objArr[3] = abVar.e;
        objArr[4] = abVar.c;
        objArr[5] = abVar.b();
        objArr[6] = abVar.d();
        objArr[7] = Float.valueOf(abVar.e());
        objArr[8] = abVar.j;
        objArr[9] = abVar.k;
        objArr[10] = abVar.a();
        objArr[11] = abVar.n != null ? abVar.n.toJson() : null;
        objArr[12] = abVar.o != null ? abVar.o.c() : null;
        if (abVar.p != null) {
            str = abVar.p.a();
        }
        objArr[13] = str;
        a(strArr, objArr, new String[]{"sf_id"}, new Object[]{abVar.h});
    }
}
