package com.immomo.momo.service;

import android.database.Cursor;
import com.immomo.momo.service.bean.b.b;

final class ac extends Thread {

    /* renamed from: a  reason: collision with root package name */
    private Cursor f2957a = null;
    private ab b = null;
    private boolean c = false;
    private /* synthetic */ aa d;

    ac(aa aaVar, Cursor cursor, ab abVar) {
        this.d = aaVar;
        this.f2957a = cursor;
        this.b = abVar;
        this.c = false;
    }

    public final void a() {
        this.c = true;
        if (!this.f2957a.isClosed()) {
            this.f2957a.close();
        }
    }

    public final void run() {
        while (!this.c && this.f2957a.moveToNext()) {
            b bVar = new b();
            bVar.f3005a = this.f2957a.getString(2);
            bVar.b = this.f2957a.getInt(0);
            bVar.c = this.f2957a.getString(1);
            this.d.d(bVar);
            this.d.b.add(bVar);
        }
        this.f2957a.close();
        if (!this.c) {
            this.b.a(this.d.b);
        }
    }
}
