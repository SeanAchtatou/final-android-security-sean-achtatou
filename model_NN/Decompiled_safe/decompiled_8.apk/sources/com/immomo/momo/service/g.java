package com.immomo.momo.service;

import android.database.sqlite.SQLiteDatabase;
import android.support.v4.b.a;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.service.a.aq;
import com.immomo.momo.service.a.ar;
import com.immomo.momo.service.a.ba;
import com.immomo.momo.service.a.i;
import com.immomo.momo.service.bean.Message;
import com.immomo.momo.service.bean.ax;
import com.immomo.momo.service.bean.bf;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class g extends b {
    private i c;
    private aq d;
    private ba e;

    public g() {
        this(PoiTypeDef.All);
    }

    public g(SQLiteDatabase sQLiteDatabase) {
        this.c = null;
        this.d = null;
        this.e = null;
        this.f2968a = sQLiteDatabase;
        this.c = new i(sQLiteDatabase);
        this.d = new aq(sQLiteDatabase);
        this.e = new ba(sQLiteDatabase);
    }

    private g(String str) {
        this.c = null;
        this.d = null;
        this.e = null;
        if (a.a((CharSequence) str)) {
            this.f2968a = com.immomo.momo.g.d().e();
        } else {
            this.f2968a = new com.immomo.momo.service.a.g(com.immomo.momo.g.c(), str).getWritableDatabase();
        }
        this.c = new i(this.f2968a);
        this.d = new aq(this.f2968a);
        this.e = new ba(this.f2968a);
    }

    public final void a(Message message) {
        if (a.a((CharSequence) message.discussId) || a.a((CharSequence) message.msgId)) {
            throw new NullPointerException("msg.discussId or msg.msgId is null");
        }
        try {
            message.msginfo = com.immomo.momo.util.a.a.a(message);
            this.c.a(message);
            boolean z = true;
            String str = message.discussId;
            ax axVar = (ax) this.d.a((Serializable) str);
            if (axVar == null) {
                axVar = new ax(str);
                z = false;
            }
            axVar.i = message.msgId;
            axVar.j = ar.a(this.f2968a).a();
            axVar.h = message.timestamp;
            axVar.o = 6;
            if (z) {
                this.d.b(axVar);
            } else {
                this.d.a(axVar);
            }
        } catch (Exception e2) {
            this.b.a((Throwable) e2);
        }
    }

    public final void a(String str) {
        this.c.a(new String[]{Message.DBFIELD_STATUS}, new Object[]{9}, new String[]{Message.DBFIELD_GROUPID, Message.DBFIELD_RECEIVE, Message.DBFIELD_STATUS}, new Object[]{str, 1, 5});
        this.c.a(new String[]{Message.DBFIELD_STATUS}, new Object[]{9}, new String[]{Message.DBFIELD_GROUPID, Message.DBFIELD_RECEIVE, Message.DBFIELD_STATUS}, new Object[]{str, 1, 13});
    }

    public final void a(String str, int i) {
        this.c.a(new String[]{Message.DBFIELD_STATUS}, new Object[]{Integer.valueOf(i)}, new String[]{Message.DBFIELD_MSGID}, new Object[]{str});
    }

    public final void a(String str, int i, int i2) {
        this.c.a(new String[]{Message.DBFIELD_STATUS}, new String[]{new StringBuilder(String.valueOf(i)).toString()}, new String[]{Message.DBFIELD_GROUPID, Message.DBFIELD_STATUS}, new String[]{str, new StringBuilder(String.valueOf(i2)).toString()});
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object, java.lang.String, java.lang.Object[]):void
     arg types: [java.lang.String, int, java.lang.String, java.lang.String[]]
     candidates:
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.String[], java.lang.String[], java.lang.String[]):int
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.String, java.lang.String[], java.lang.String[]):java.lang.String
      com.immomo.momo.service.a.b.a(java.lang.String[], java.lang.String[], java.lang.String, boolean):java.util.List
      com.immomo.momo.service.a.b.a(java.lang.String[], java.lang.Object[], java.lang.String[], java.lang.Object[]):void
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object, java.lang.String, java.lang.Object[]):void */
    public final void a(String[] strArr) {
        if (strArr != null && strArr.length > 0) {
            this.c.a(Message.DBFIELD_STATUS, (Object) 4, Message.DBFIELD_MSGID, (Object[]) strArr);
        }
    }

    public final List b(String str, int i, int i2) {
        ArrayList arrayList = new ArrayList();
        List a2 = this.c.a(new String[]{Message.DBFIELD_GROUPID}, new String[]{str}, Message.DBFIELD_ID, false, i, i2);
        for (int size = a2.size() - 1; size >= 0; size--) {
            Message message = (Message) a2.get(size);
            message.remoteUser = (bf) this.e.a((Serializable) message.remoteId);
            arrayList.add(message);
        }
        return arrayList;
    }

    public final void b(Message message) {
        if (a.a((CharSequence) message.discussId) || a.a((CharSequence) message.msgId)) {
            throw new NullPointerException("msg.discussId or msg.msgId or msg.remoteId is null");
        }
        message.msginfo = com.immomo.momo.util.a.a.a(message);
        this.c.b(message);
    }

    public final boolean b(String str) {
        return this.c.c(new String[]{Message.DBFIELD_MSGID}, new String[]{str}) > 0;
    }

    public final Message c(String str) {
        return (Message) this.c.b(Message.DBFIELD_MSGID, str);
    }

    public final void c() {
        this.c.a(new String[]{Message.DBFIELD_STATUS}, new Object[]{9}, new String[]{Message.DBFIELD_RECEIVE, Message.DBFIELD_STATUS}, new Object[]{1, 5});
        this.c.a(new String[]{Message.DBFIELD_STATUS}, new Object[]{9}, new String[]{Message.DBFIELD_RECEIVE, Message.DBFIELD_STATUS}, new Object[]{1, 13});
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.immomo.momo.service.a.i.a(com.immomo.momo.service.bean.Message, android.database.Cursor):void
      com.immomo.momo.service.a.i.a(java.lang.String, com.immomo.momo.service.bean.Message):void
      com.immomo.momo.service.a.i.a(java.lang.Object, android.database.Cursor):void
      com.immomo.momo.service.a.b.a(android.database.Cursor, java.lang.String):int
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.String[]):android.database.Cursor
      com.immomo.momo.service.a.b.a(java.lang.String[], java.lang.String[]):java.util.List
      com.immomo.momo.service.a.b.a(java.lang.Object, android.database.Cursor):void
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object[]):void
      com.immomo.momo.service.a.b.a(java.lang.String[], java.lang.Object[]):void
      com.immomo.momo.service.a.b.a(java.lang.Object, java.io.Serializable):boolean
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.String):boolean
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object):void */
    public final void c(Message message) {
        this.c.a(Message.DBFIELD_MSGID, (Object) message.msgId);
        if (this.d.c(message.discussId)) {
            String a2 = this.c.a(Message.DBFIELD_MSGID, Message.DBFIELD_ID, new String[]{Message.DBFIELD_GROUPID}, new String[]{message.discussId});
            if (a.a((CharSequence) a2)) {
                a2 = "-1";
            }
            this.d.a("s_lastmsgid", a2, message.discussId);
        }
    }

    public final void d() {
        this.f2968a.beginTransaction();
        try {
            this.c.a(new String[]{Message.DBFIELD_STATUS}, new Object[]{3}, new String[]{Message.DBFIELD_STATUS}, new Object[]{1});
            this.c.a(new String[]{Message.DBFIELD_STATUS}, new Object[]{3}, new String[]{Message.DBFIELD_STATUS}, new Object[]{7});
            this.c.a(new String[]{Message.DBFIELD_STATUS}, new Object[]{3}, new String[]{Message.DBFIELD_STATUS}, new Object[]{8});
            this.f2968a.setTransactionSuccessful();
        } catch (Exception e2) {
        } finally {
            this.f2968a.endTransaction();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.immomo.momo.service.a.i.a(com.immomo.momo.service.bean.Message, android.database.Cursor):void
      com.immomo.momo.service.a.i.a(java.lang.String, com.immomo.momo.service.bean.Message):void
      com.immomo.momo.service.a.i.a(java.lang.Object, android.database.Cursor):void
      com.immomo.momo.service.a.b.a(android.database.Cursor, java.lang.String):int
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.String[]):android.database.Cursor
      com.immomo.momo.service.a.b.a(java.lang.String[], java.lang.String[]):java.util.List
      com.immomo.momo.service.a.b.a(java.lang.Object, android.database.Cursor):void
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object[]):void
      com.immomo.momo.service.a.b.a(java.lang.String[], java.lang.Object[]):void
      com.immomo.momo.service.a.b.a(java.lang.Object, java.io.Serializable):boolean
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.String):boolean
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object):void */
    public final void d(String str) {
        this.c.a(Message.DBFIELD_GROUPID, (Object) str);
        this.d.a("s_lastmsgid", PoiTypeDef.All, str);
    }

    public final int e() {
        return this.c.c(new String[]{Message.DBFIELD_STATUS}, new String[]{"5"});
    }

    public final boolean e(String str) {
        return f(str) > 0 || g(str) > 0;
    }

    public final int f(String str) {
        return this.c.c(new String[]{Message.DBFIELD_GROUPID, Message.DBFIELD_STATUS}, new String[]{str, "5"});
    }

    public final int g(String str) {
        return this.c.c(new String[]{Message.DBFIELD_GROUPID, Message.DBFIELD_STATUS}, new String[]{str, "13"});
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.service.a.b.a(java.lang.String[], java.lang.String[], java.lang.String, boolean):java.util.List
     arg types: [java.lang.String[], java.lang.String[], java.lang.String, int]
     candidates:
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.String[], java.lang.String[], java.lang.String[]):int
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.String, java.lang.String[], java.lang.String[]):java.lang.String
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object, java.lang.String, java.lang.Object[]):void
      com.immomo.momo.service.a.b.a(java.lang.String[], java.lang.Object[], java.lang.String[], java.lang.Object[]):void
      com.immomo.momo.service.a.b.a(java.lang.String[], java.lang.String[], java.lang.String, boolean):java.util.List */
    public final List h(String str) {
        long currentTimeMillis = System.currentTimeMillis();
        List a2 = this.c.a(new String[]{Message.DBFIELD_GROUPID, Message.DBFIELD_TYPE}, new String[]{str, new StringBuilder(String.valueOf(1)).toString()}, Message.DBFIELD_ID, false);
        Collections.reverse(a2);
        this.b.a((Object) ("findMessageByGroupidContentType->" + (System.currentTimeMillis() - currentTimeMillis)));
        return a2;
    }
}
