package com.immomo.momo.service.a;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.immomo.momo.service.bean.Message;
import com.immomo.momo.service.bean.a.j;

public final class ab extends b {
    public ab(SQLiteDatabase sQLiteDatabase) {
        super(sQLiteDatabase, "group_member", Message.DBFIELD_SAYHI);
    }

    private static void a(j jVar, Cursor cursor) {
        jVar.b = cursor.getString(cursor.getColumnIndex(Message.DBFIELD_GROUPID));
        jVar.f2979a = cursor.getString(cursor.getColumnIndex(Message.DBFIELD_SAYHI));
        jVar.f = cursor.getInt(cursor.getColumnIndex(Message.DBFIELD_CONVERLOCATIONJSON));
        jVar.g = cursor.getInt(cursor.getColumnIndex("field6"));
        jVar.c = a(cursor.getLong(cursor.getColumnIndex(Message.DBFIELD_LOCATIONJSON)));
        jVar.e = a(cursor.getLong(cursor.getColumnIndex("field8")));
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Cursor cursor) {
        j jVar = new j();
        a(jVar, cursor);
        return jVar;
    }

    public final void a(j jVar) {
        StringBuilder sb = new StringBuilder();
        sb.append("insert into " + this.f2954a + " (").append("field4, field2, field5, field8, field3, field6, field1) values(?,?,?,?,?,?,?)");
        a(sb.toString(), new Object[]{jVar.b, Long.valueOf(a(jVar.c)), Long.valueOf(a(jVar.d)), Long.valueOf(a(jVar.e)), Integer.valueOf(jVar.f), Integer.valueOf(jVar.g), jVar.f2979a});
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void a(Object obj, Cursor cursor) {
        a((j) obj, cursor);
    }

    public final void b(j jVar) {
        if (jVar.c == null) {
            StringBuilder sb = new StringBuilder();
            sb.append("update " + this.f2954a + " set ").append("field6=?, where field4=? and field1=?");
            a(sb.toString(), new Object[]{Integer.valueOf(jVar.g), jVar.b, jVar.f2979a});
            return;
        }
        StringBuilder sb2 = new StringBuilder();
        sb2.append("update " + this.f2954a + " set ").append("field2=?, field5=?, field8=?, field3=?, field6=? where field4=? and field1=?");
        a(sb2.toString(), new Object[]{Long.valueOf(a(jVar.c)), Long.valueOf(a(jVar.d)), Long.valueOf(a(jVar.e)), Integer.valueOf(jVar.f), Integer.valueOf(jVar.g), jVar.b, jVar.f2979a});
    }
}
