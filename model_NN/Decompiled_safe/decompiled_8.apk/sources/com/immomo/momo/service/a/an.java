package com.immomo.momo.service.a;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.immomo.momo.service.bean.Message;
import java.util.HashMap;
import java.util.Map;

public final class an extends b {
    public an(SQLiteDatabase sQLiteDatabase) {
        super(sQLiteDatabase, "nearbysites", Message.DBFIELD_SAYHI);
    }

    private static void a(Map map, Cursor cursor) {
        map.put(Message.DBFIELD_SAYHI, cursor.getString(cursor.getColumnIndex(Message.DBFIELD_SAYHI)));
        map.put(Message.DBFIELD_LOCATIONJSON, cursor.getString(cursor.getColumnIndex(Message.DBFIELD_LOCATIONJSON)));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.service.a.an.a(java.util.Map, android.database.Cursor):void
     arg types: [java.util.HashMap, android.database.Cursor]
     candidates:
      com.immomo.momo.service.a.an.a(java.lang.Object, android.database.Cursor):void
      com.immomo.momo.service.a.b.a(android.database.Cursor, java.lang.String):int
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.String[]):android.database.Cursor
      com.immomo.momo.service.a.b.a(java.lang.String[], java.lang.String[]):java.util.List
      com.immomo.momo.service.a.b.a(java.lang.Object, android.database.Cursor):void
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object):void
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object[]):void
      com.immomo.momo.service.a.b.a(java.lang.String[], java.lang.Object[]):void
      com.immomo.momo.service.a.b.a(java.lang.Object, java.io.Serializable):boolean
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.String):boolean
      com.immomo.momo.service.a.an.a(java.util.Map, android.database.Cursor):void */
    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Cursor cursor) {
        HashMap hashMap = new HashMap();
        a((Map) hashMap, cursor);
        return hashMap;
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void a(Object obj, Cursor cursor) {
        a((Map) obj, cursor);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object[]):void
     arg types: [java.lang.String, java.lang.String[]]
     candidates:
      com.immomo.momo.service.a.an.a(java.util.Map, android.database.Cursor):void
      com.immomo.momo.service.a.an.a(java.lang.Object, android.database.Cursor):void
      com.immomo.momo.service.a.b.a(android.database.Cursor, java.lang.String):int
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.String[]):android.database.Cursor
      com.immomo.momo.service.a.b.a(java.lang.String[], java.lang.String[]):java.util.List
      com.immomo.momo.service.a.b.a(java.lang.Object, android.database.Cursor):void
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object):void
      com.immomo.momo.service.a.b.a(java.lang.String[], java.lang.Object[]):void
      com.immomo.momo.service.a.b.a(java.lang.Object, java.io.Serializable):boolean
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.String):boolean
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object[]):void */
    /* renamed from: b */
    public final void a(Map map) {
        StringBuilder sb = new StringBuilder();
        sb.append("insert into " + this.f2954a + " (").append("field1,field2) values(?,?)");
        a(sb.toString(), (Object[]) new String[]{(String) map.get(Message.DBFIELD_SAYHI), (String) map.get(Message.DBFIELD_LOCATIONJSON)});
    }
}
