package com.immomo.momo.service.a;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.b.a;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.Message;
import com.immomo.momo.service.bean.a.e;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public final class y extends b {
    private static Set d = new HashSet();

    public y(SQLiteDatabase sQLiteDatabase) {
        super(sQLiteDatabase, "groupfeed", "sf_id");
    }

    private static void a(e eVar, Cursor cursor) {
        eVar.a(d(cursor, Message.DBFIELD_LOCATIONJSON));
        eVar.a(a.b(c(cursor, Message.DBFIELD_CONVERLOCATIONJSON), ","));
        eVar.f = c(cursor, "sf_id");
        eVar.a(c(cursor, "field5"));
        eVar.c = c(cursor, Message.DBFIELD_SAYHI);
        eVar.e = a(cursor, Message.DBFIELD_GROUPID);
        eVar.h = a(cursor, "field6");
        eVar.o = a(cursor, "field13");
        eVar.p = e(cursor, "field16");
        eVar.n = e(cursor, "field14");
        int columnIndex = cursor.getColumnIndex("field7");
        eVar.a(columnIndex >= 0 ? cursor.getFloat(columnIndex) : -1.0f);
        eVar.i = c(cursor, "field8");
        eVar.j = c(cursor, "field9");
        eVar.b(c(cursor, "field17"));
        eVar.f2974a = c(cursor, "field10");
        eVar.g = c(cursor, "field15");
        eVar.m = a(cursor, "field12");
        if (!a.a((CharSequence) eVar.f)) {
            d.add(eVar.f);
        }
    }

    public static void g() {
        Set set = d;
        String[] strArr = new String[set.size()];
        set.toArray(strArr);
        set.clear();
        if (g.d().h() != null) {
            new y(g.d().h()).a("field11", new Date(), "sf_id", strArr);
        }
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Cursor cursor) {
        e eVar = new e();
        a(eVar, cursor);
        return eVar;
    }

    public final void a(e eVar) {
        HashMap hashMap = new HashMap();
        hashMap.put(Message.DBFIELD_CONVERLOCATIONJSON, a.a(eVar.g(), ","));
        hashMap.put(Message.DBFIELD_GROUPID, Integer.valueOf(eVar.e));
        hashMap.put("field6", Integer.valueOf(eVar.h));
        hashMap.put("field13", Integer.valueOf(eVar.o));
        hashMap.put("field14", Boolean.valueOf(eVar.n));
        hashMap.put("field16", Boolean.valueOf(eVar.p));
        hashMap.put(Message.DBFIELD_SAYHI, eVar.c);
        hashMap.put("field5", eVar.a());
        hashMap.put(Message.DBFIELD_LOCATIONJSON, eVar.d());
        hashMap.put("field11", new Date());
        hashMap.put("field7", Float.valueOf(eVar.e()));
        hashMap.put("field8", eVar.i);
        hashMap.put("field17", eVar.b());
        hashMap.put("field9", eVar.j);
        hashMap.put("field10", eVar.f2974a);
        hashMap.put("field12", Integer.valueOf(eVar.m));
        hashMap.put("field15", eVar.g);
        hashMap.put("sf_id", eVar.f);
        a((Map) hashMap);
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void a(Object obj, Cursor cursor) {
        a((e) obj, cursor);
    }

    public final void b(e eVar) {
        HashMap hashMap = new HashMap();
        hashMap.put(Message.DBFIELD_CONVERLOCATIONJSON, a.a(eVar.g(), ","));
        hashMap.put(Message.DBFIELD_GROUPID, Integer.valueOf(eVar.e));
        hashMap.put("field6", Integer.valueOf(eVar.h));
        hashMap.put("field13", Integer.valueOf(eVar.o));
        hashMap.put("field14", Boolean.valueOf(eVar.n));
        hashMap.put("field16", Boolean.valueOf(eVar.p));
        hashMap.put(Message.DBFIELD_SAYHI, eVar.c);
        hashMap.put("field5", eVar.a());
        hashMap.put(Message.DBFIELD_LOCATIONJSON, eVar.d());
        hashMap.put("field7", Float.valueOf(eVar.e()));
        hashMap.put("field8", eVar.i);
        hashMap.put("field17", eVar.b());
        hashMap.put("field9", eVar.j);
        hashMap.put("field10", eVar.f2974a);
        hashMap.put("field12", Integer.valueOf(eVar.m));
        hashMap.put("field15", eVar.g);
        a(hashMap, new String[]{"sf_id"}, new String[]{eVar.f});
    }
}
