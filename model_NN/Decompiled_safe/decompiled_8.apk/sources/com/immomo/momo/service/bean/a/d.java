package com.immomo.momo.service.bean.a;

import android.support.v4.b.a;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class d {

    /* renamed from: a  reason: collision with root package name */
    public String f2973a;
    private String b;
    private String c;

    public static void a(StringBuilder sb, StringBuilder sb2, List list) {
        Matcher matcher = Pattern.compile("(\\[.*?\\|.*?\\|.*?\\])").matcher(sb);
        int i = 0;
        while (matcher.find()) {
            d dVar = new d();
            String group = matcher.group();
            if (group != null && !a.a((CharSequence) group)) {
                StringBuilder sb3 = new StringBuilder(group);
                sb3.deleteCharAt(0);
                sb3.deleteCharAt(sb3.length() - 1);
                String[] split = sb3.toString().split("\\|");
                if (split.length > 0) {
                    dVar.f2973a = split[0];
                    if (split.length > 1) {
                        dVar.b = split[1];
                        if (split.length > 2) {
                            dVar.c = split[2];
                        }
                    }
                }
            }
            list.add(dVar);
            sb2.append(sb.substring(i, matcher.start()));
            sb2.append("%s");
            i = matcher.end();
        }
        sb2.append(sb.substring(i, sb.length()));
    }

    public final String toString() {
        return "[" + this.f2973a + "|" + this.b + "|" + this.c + "]";
    }
}
