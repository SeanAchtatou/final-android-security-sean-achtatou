package com.immomo.momo.service.bean;

import java.util.Date;
import java.util.List;

public final class aw {

    /* renamed from: a  reason: collision with root package name */
    private List f2999a;
    private String b;
    private bf c;
    private int d;
    private Date e;

    public aw() {
    }

    public aw(String str) {
        this.b = str;
    }

    public final Date a() {
        return this.e;
    }

    public final void a(int i) {
        this.d = i;
    }

    public final void a(bf bfVar) {
        this.c = bfVar;
    }

    public final void a(String str) {
        this.b = str;
    }

    public final void a(Date date) {
        this.e = date;
    }

    public final void a(List list) {
        this.f2999a = list;
    }

    public final int b() {
        return this.d;
    }

    public final List c() {
        return this.f2999a;
    }

    public final String d() {
        return this.b;
    }

    public final bf e() {
        return this.c;
    }

    public final boolean equals(Object obj) {
        return this.b.equals(((aw) obj).b);
    }
}
