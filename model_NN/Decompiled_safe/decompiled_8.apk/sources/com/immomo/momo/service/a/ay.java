package com.immomo.momo.service.a;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.immomo.momo.g;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public final class ay extends b {
    private static ay d = null;

    private ay(SQLiteDatabase sQLiteDatabase) {
        super(sQLiteDatabase, "sendtask", "msgid");
    }

    private static void a(Map map, Cursor cursor) {
        map.put("info", cursor.getString(cursor.getColumnIndex("info")));
        map.put("type", Integer.valueOf(cursor.getInt(cursor.getColumnIndex("type"))));
        map.put("msgid", cursor.getString(cursor.getColumnIndex("msgid")));
        try {
            map.put("time", a(cursor.getLong(cursor.getColumnIndex("time"))));
        } catch (Exception e) {
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.service.a.ay.a(java.util.Map, android.database.Cursor):void
     arg types: [java.util.HashMap, android.database.Cursor]
     candidates:
      com.immomo.momo.service.a.ay.a(java.lang.Object, android.database.Cursor):void
      com.immomo.momo.service.a.b.a(android.database.Cursor, java.lang.String):int
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.String[]):android.database.Cursor
      com.immomo.momo.service.a.b.a(java.lang.String[], java.lang.String[]):java.util.List
      com.immomo.momo.service.a.b.a(java.lang.Object, android.database.Cursor):void
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object):void
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object[]):void
      com.immomo.momo.service.a.b.a(java.lang.String[], java.lang.Object[]):void
      com.immomo.momo.service.a.b.a(java.lang.Object, java.io.Serializable):boolean
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.String):boolean
      com.immomo.momo.service.a.ay.a(java.util.Map, android.database.Cursor):void */
    private static Map b(Cursor cursor) {
        HashMap hashMap = new HashMap();
        a((Map) hashMap, cursor);
        return hashMap;
    }

    public static ay g() {
        if (!(d == null || d.a() == g.d().f())) {
            d.a().close();
            d = null;
        }
        if (d == null) {
            d = new ay(g.d().f());
        }
        return d;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Cursor cursor) {
        return b(cursor);
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void a(Object obj, Cursor cursor) {
        a((Map) obj, cursor);
    }

    /* renamed from: b */
    public final void a(Map map) {
        long j = 0;
        if (map.get("time") != null) {
            j = a((Date) map.get("time"));
        }
        StringBuilder sb = new StringBuilder();
        sb.append("insert into " + this.f2954a + " (").append("info, msgid, time, type) values(?,?,?,?)");
        a(sb.toString(), new Object[]{map.get("info"), map.get("msgid"), Long.valueOf(j), map.get("type")});
    }
}
