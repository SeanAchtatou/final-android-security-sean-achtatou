package com.immomo.momo.service.bean.a;

import java.util.List;
import java.util.Map;

public final class h {

    /* renamed from: a  reason: collision with root package name */
    public List f2977a;
    public List b;
    public boolean c = false;
    public String d;
    public String e;
    public String f;
    public int g;
    public int h;
    public String i;
    public String j;
    public String k;
    public int l;
    public int m;
    public Map n = null;

    public final String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("agreeList=" + this.f2977a + "\r\n");
        stringBuffer.append("disgreeList=" + this.b + "\r\n");
        stringBuffer.append("tip=" + this.d + "\r\n");
        return stringBuffer.toString();
    }
}
