package com.immomo.momo.service.bean;

import org.json.JSONObject;

public final class ac extends al {

    /* renamed from: a  reason: collision with root package name */
    public String f2983a;
    public String b;
    public String c;
    public String d;

    public final String a() {
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("title", this.f2983a);
            jSONObject.put("desc", this.b);
            jSONObject.put("icon", this.c);
            jSONObject.put("action", this.d);
            return jSONObject.toString();
        } catch (Exception e) {
            return null;
        }
    }

    public final String getLoadImageId() {
        return this.c;
    }

    public final boolean isImageUrl() {
        return true;
    }
}
