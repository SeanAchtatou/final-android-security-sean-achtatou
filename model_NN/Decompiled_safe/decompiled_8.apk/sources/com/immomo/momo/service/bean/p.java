package com.immomo.momo.service.bean;

import java.io.Serializable;
import java.util.Date;

public final class p implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    public String f3034a;
    public String b;
    public Date c;
    public Date d;
    public Date e;
    public int f;
    public int g;
    public bf h;

    public final boolean equals(Object obj) {
        return this.f3034a.equals(((p) obj).f3034a);
    }

    public final String toString() {
        return "DiscussUser [momoid=" + this.f3034a + ", did=" + this.b + ", joinTime=" + this.c + ", level=" + this.f + ", user=" + this.h + "]";
    }
}
