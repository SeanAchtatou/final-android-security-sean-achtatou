package com.immomo.momo.service.bean;

import com.immomo.momo.protocol.imjson.util.c;
import com.immomo.momo.service.bean.a.a;
import java.util.Date;

public final class ax {

    /* renamed from: a  reason: collision with root package name */
    public String f3000a;
    public bf b;
    public a c;
    public n d;
    public c e;
    public int f = 0;
    public int g = 0;
    public Date h;
    public String i;
    public int j;
    public Message k;
    public String l;
    public boolean m = true;
    public boolean n = false;
    public int o = 0;

    public ax() {
    }

    public ax(String str) {
        this.f3000a = str;
    }

    public final boolean a() {
        return this.g > 0;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        ax axVar = (ax) obj;
        return this.f3000a == null ? axVar.f3000a == null : this.f3000a.equals(axVar.f3000a);
    }

    public final int hashCode() {
        return (this.f3000a == null ? 0 : this.f3000a.hashCode()) + 31;
    }

    public final String toString() {
        return "Session [momoID=" + this.f3000a + ", fetchtime=" + (this.h != null ? android.support.v4.b.a.c(this.h) : "null") + ", lastmsgId=" + this.i + "]";
    }
}
