package com.immomo.momo.service.a;

import android.database.Cursor;
import android.support.v4.b.a;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.ak;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public final class ad extends b {
    private static ad d = null;

    private ad() {
        super(g.d().g(), "imagecache", "i_imageid");
    }

    private static void a(ak akVar, Cursor cursor) {
        akVar.f2990a = cursor.getString(cursor.getColumnIndex("i_imageid"));
        akVar.b = cursor.getString(cursor.getColumnIndex("i_path"));
        akVar.d = cursor.getInt(cursor.getColumnIndex("i_type"));
        akVar.e = a.a(cursor.getLong(cursor.getColumnIndex("i_time")));
        akVar.c = cursor.getInt(cursor.getColumnIndex("i_maxday"));
    }

    private void a(String str, ak akVar) {
        a(str, new Object[]{Integer.valueOf(akVar.c), akVar.b, Long.valueOf(a.b(akVar.e)), Integer.valueOf(akVar.d), akVar.f2990a});
    }

    /* access modifiers changed from: private */
    /* renamed from: c */
    public void a(ak akVar) {
        a("insert into imagecache (i_maxday, i_path, i_time, i_type, i_imageid) values(?,?,?,?,?)", akVar);
    }

    public static ad g() {
        if (d == null) {
            d = new ad();
        }
        return d;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Cursor cursor) {
        ak akVar = new ak();
        a(akVar, cursor);
        return akVar;
    }

    public final void a(ak akVar) {
        b((Serializable) akVar.f2990a);
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void a(Object obj, Cursor cursor) {
        a((ak) obj, cursor);
    }

    public final void a(String str, Date date) {
        a("i_time", Long.valueOf(a.b(date)), str);
    }

    public final void b(ak akVar) {
        if (c(akVar.f2990a)) {
            a("update imagecache set i_maxday=?, i_path=?, i_time=?, i_type=? where i_imageid=?", akVar);
            return;
        }
        a(akVar);
    }

    public final List h() {
        Calendar instance = Calendar.getInstance();
        instance.add(6, -15);
        return c("i_time<? order by i_time desc", new String[]{new StringBuilder().append(a.b(instance.getTime())).toString()});
    }
}
