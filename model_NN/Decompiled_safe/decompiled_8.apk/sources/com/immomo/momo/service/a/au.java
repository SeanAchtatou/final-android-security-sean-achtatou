package com.immomo.momo.service.a;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.b.a;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.Message;
import com.immomo.momo.service.bean.ae;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public final class au extends b {
    private static Set d = new HashSet();

    public au(SQLiteDatabase sQLiteDatabase) {
        super(sQLiteDatabase, "sitefeedcomments", "c_id");
    }

    private static void a(ae aeVar, Cursor cursor) {
        aeVar.k = cursor.getString(cursor.getColumnIndex("c_id"));
        aeVar.b = cursor.getString(cursor.getColumnIndex(Message.DBFIELD_LOCATIONJSON));
        aeVar.l = cursor.getInt(cursor.getColumnIndex("field7"));
        aeVar.n = cursor.getInt(cursor.getColumnIndex("field12"));
        aeVar.m = cursor.getInt(cursor.getColumnIndex("field13"));
        aeVar.o = cursor.getInt(cursor.getColumnIndex("field14"));
        aeVar.i = cursor.getString(cursor.getColumnIndex("field15"));
        aeVar.p = cursor.getInt(cursor.getColumnIndex("field10"));
        aeVar.h = cursor.getString(cursor.getColumnIndex("field5"));
        aeVar.q = cursor.getString(cursor.getColumnIndex("field8"));
        aeVar.f = cursor.getString(cursor.getColumnIndex(Message.DBFIELD_GROUPID));
        aeVar.c = cursor.getString(cursor.getColumnIndex(Message.DBFIELD_SAYHI));
        aeVar.d = cursor.getString(cursor.getColumnIndex("field9"));
        aeVar.a(a(cursor.getLong(cursor.getColumnIndex(Message.DBFIELD_CONVERLOCATIONJSON))));
        if (!a.a((CharSequence) aeVar.k)) {
            d.add(aeVar.k);
        }
    }

    public static void g() {
        Set set = d;
        String[] strArr = new String[set.size()];
        set.toArray(strArr);
        if (g.d().h() != null) {
            new au(g.d().h()).a("field11", new Date(), "c_id", strArr);
            set.clear();
        }
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Cursor cursor) {
        ae aeVar = new ae();
        a(aeVar, cursor);
        return aeVar;
    }

    public final void a(ae aeVar) {
        b(new String[]{"c_id", Message.DBFIELD_LOCATIONJSON, "field7", "field10", "field5", "field8", Message.DBFIELD_GROUPID, "field9", Message.DBFIELD_CONVERLOCATIONJSON, Message.DBFIELD_SAYHI, "field11", "field12", "field13", "field14", "field15"}, new Object[]{aeVar.k, aeVar.b, Integer.valueOf(aeVar.l), Integer.valueOf(aeVar.p), aeVar.h, aeVar.q, aeVar.f, aeVar.d, aeVar.a(), aeVar.c, new Date(), Integer.valueOf(aeVar.n), Integer.valueOf(aeVar.m), Integer.valueOf(aeVar.o), aeVar.i});
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void a(Object obj, Cursor cursor) {
        a((ae) obj, cursor);
    }

    public final void b(ae aeVar) {
        a(new String[]{Message.DBFIELD_LOCATIONJSON, "field5", "field7", "field10", "field8", Message.DBFIELD_GROUPID, "field9", Message.DBFIELD_CONVERLOCATIONJSON, Message.DBFIELD_SAYHI, "field12", "field13", "field14", "field15"}, new Object[]{aeVar.b, aeVar.h, Integer.valueOf(aeVar.l), Integer.valueOf(aeVar.p), aeVar.q, aeVar.f, aeVar.d, aeVar.a(), aeVar.c, Integer.valueOf(aeVar.n), Integer.valueOf(aeVar.m), Integer.valueOf(aeVar.o), aeVar.i}, new String[]{"c_id"}, new Object[]{aeVar.k});
    }
}
