package com.immomo.momo.service.bean.c;

import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.service.bean.al;
import java.util.Arrays;
import java.util.Date;

public final class c extends al {

    /* renamed from: a  reason: collision with root package name */
    public String f3018a;
    public g b;
    public String c;
    public String d;
    public String[] e;
    public String f;
    public Date g;
    public String h;
    public b i;
    public String j;
    public c k;
    public String l;
    public String m;
    public String n;
    public int o;
    public String p;
    public String q;
    public String r;
    public String s;

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        c cVar = (c) obj;
        return this.f3018a == null ? cVar.f3018a == null : this.f3018a.equals(cVar.f3018a);
    }

    public final String getLoadImageId() {
        return (this.e == null || this.e.length <= 0) ? PoiTypeDef.All : this.e[0];
    }

    public final int hashCode() {
        return (this.f3018a == null ? 0 : this.f3018a.hashCode()) + 31;
    }

    public final String toString() {
        return "TieComment [cid=" + this.f3018a + ", ownerUser=" + this.b + ", ownerUserId=" + this.c + ", textContent=" + this.d + ", images=" + Arrays.toString(this.e) + ", createTime=" + this.g + ", floor=" + this.h + ", citeCount=0, tie=" + this.i + ", tieId=" + this.j + ", citeComment=" + this.k + ", tiebaId=" + this.l + ", tiebaName=" + this.m + ", status=" + this.o + ", tocommentid=" + this.p + ", emotion_body=" + this.q + ", emotion_name=" + this.r + ", emotion_library=" + this.s + "]";
    }
}
