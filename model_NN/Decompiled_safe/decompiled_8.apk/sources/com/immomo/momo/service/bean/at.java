package com.immomo.momo.service.bean;

import android.content.Context;
import android.support.v4.b.a;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.android.view.a.ah;
import com.immomo.momo.android.view.a.aj;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.a.i;
import com.immomo.momo.util.ak;
import java.util.Date;
import java.util.HashMap;

public final class at {
    private boolean A = false;
    private boolean B;
    private int C;
    private int D;
    private boolean E;
    private boolean F;
    private int G;
    private String H;
    private int I;
    private int J;
    private String K;
    private Context L;
    private ak M;
    private HashMap N;
    private HashMap O;

    /* renamed from: a  reason: collision with root package name */
    public boolean f2996a = true;
    public boolean b = true;
    public boolean c = false;
    public boolean d = false;
    public boolean e;
    public boolean f;
    public String g;
    public boolean h;
    public int i;
    public long j;
    public long k;
    public int l;
    public String m;
    public ah n;
    public com.immomo.momo.android.view.a.ak o;
    public aj p;
    public int q;
    public int r;
    public String s;
    public boolean t;
    public boolean u;
    public boolean v;
    public boolean w;
    public boolean x;
    public boolean y;
    public boolean z;

    private at(Context context, String str) {
        this.e = g.i().getVibrateSetting(0) > 0;
        this.f = true;
        this.B = false;
        this.g = PoiTypeDef.All;
        this.h = false;
        this.C = 0;
        this.D = 0;
        this.i = 0;
        this.j = 0;
        this.k = 0;
        this.E = true;
        this.F = true;
        this.l = 0;
        this.G = 0;
        this.H = PoiTypeDef.All;
        this.m = PoiTypeDef.All;
        this.I = 23;
        this.J = 7;
        this.n = ah.ALL;
        this.o = com.immomo.momo.android.view.a.ak.MINUTE_4320;
        this.p = aj.ALL;
        this.q = 0;
        this.r = 0;
        this.s = PoiTypeDef.All;
        this.K = null;
        this.L = null;
        this.M = null;
        this.N = new HashMap();
        this.O = new HashMap();
        this.t = true;
        this.u = true;
        this.v = true;
        this.w = true;
        this.x = true;
        this.y = true;
        this.z = false;
        this.K = str;
        this.L = context;
        this.M = ak.a(context, str);
    }

    public static at a(Context context, String str) {
        at atVar = new at(context, str);
        ak akVar = atVar.M;
        atVar.f2996a = akVar.a("openAlarm", Boolean.valueOf(atVar.f2996a));
        atVar.b = akVar.a("sound", Boolean.valueOf(atVar.b));
        atVar.c = akVar.a("phonebook_syn", Boolean.valueOf(atVar.c));
        atVar.A = akVar.a("msg_roaming_formember", Boolean.valueOf(atVar.A));
        atVar.d = akVar.a("sinafriend_syn", Boolean.valueOf(atVar.d));
        atVar.B = akVar.a("locate_only_use_gps", Boolean.valueOf(atVar.b));
        atVar.e = akVar.a("vibrate", Boolean.valueOf(atVar.e));
        atVar.f = akVar.a("is_show_msg_content", Boolean.valueOf(atVar.f));
        atVar.l = akVar.a("hiddenmode", Integer.valueOf(atVar.l));
        atVar.G = akVar.a("location_mode", Integer.valueOf(atVar.G));
        atVar.g = akVar.b("mutetime", atVar.g);
        int a2 = akVar.a("neayby_filter_gender", Integer.valueOf(atVar.n.ordinal()));
        int a3 = akVar.a("neayby_filter_timeline", Integer.valueOf(atVar.o.ordinal()));
        atVar.n = ah.values()[a2];
        atVar.o = com.immomo.momo.android.view.a.ak.values()[a3];
        atVar.p = aj.values()[akVar.a("neayby_filter_bind", Integer.valueOf(atVar.p.ordinal()))];
        atVar.r = akVar.a("neayby_filter_constellation", Integer.valueOf(atVar.r));
        atVar.q = akVar.a("neayby_filter_age", Integer.valueOf(atVar.q));
        atVar.s = akVar.b("neayby_filter_industry", PoiTypeDef.All);
        if (atVar.g != null && !PoiTypeDef.All.equals(atVar.g)) {
            try {
                String[] b2 = a.b(atVar.g, "-");
                atVar.I = Integer.parseInt(b2[0]);
                atVar.J = Integer.parseInt(b2[1]);
            } catch (Exception e2) {
            }
        }
        atVar.h = akVar.a("open_mutetime", Boolean.valueOf(atVar.h));
        atVar.C = akVar.a("momo_server_locate_count", Integer.valueOf(atVar.C));
        atVar.D = akVar.a("location_google_server_locate_count", Integer.valueOf(atVar.D));
        atVar.E = akVar.a("key_is_google_first", Boolean.valueOf(atVar.E));
        atVar.F = akVar.a("key_is_httplocate_from_google", Boolean.valueOf(atVar.F));
        atVar.H = akVar.b("momo_alert_flags", atVar.H);
        atVar.m = akVar.b("momo_gsearch_history", atVar.m);
        atVar.i = akVar.a("key_audio_type", Integer.valueOf(atVar.i));
        atVar.v = akVar.a("notify_event", Boolean.valueOf(atVar.v));
        atVar.w = akVar.a("notify_tieba", Boolean.valueOf(atVar.w));
        atVar.u = akVar.a("notify_friend", Boolean.valueOf(atVar.u));
        atVar.t = akVar.a("notify_groupnotice", Boolean.valueOf(atVar.t));
        atVar.j = akVar.a("shop_update_time", Long.valueOf(atVar.j));
        atVar.k = akVar.a("bothlist_version", Long.valueOf(atVar.k));
        atVar.y = akVar.a("notify_stranger", Boolean.valueOf(atVar.y));
        atVar.z = akVar.a("key_tieba_agree_protocol", Boolean.valueOf(atVar.z));
        return atVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.Boolean]
     candidates:
      com.immomo.momo.util.ak.a(android.content.Context, java.lang.String):com.immomo.momo.util.ak
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Integer):int
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Long):long
      com.immomo.momo.util.ak.a(java.lang.String, int):void
      com.immomo.momo.util.ak.a(java.lang.String, long):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.String):void
      com.immomo.momo.util.ak.a(java.lang.String, boolean):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Boolean):boolean
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.Integer]
     candidates:
      com.immomo.momo.util.ak.a(android.content.Context, java.lang.String):com.immomo.momo.util.ak
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Integer):int
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Long):long
      com.immomo.momo.util.ak.a(java.lang.String, int):void
      com.immomo.momo.util.ak.a(java.lang.String, long):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.String):void
      com.immomo.momo.util.ak.a(java.lang.String, boolean):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Boolean):boolean
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.immomo.momo.util.ak.a(android.content.Context, java.lang.String):com.immomo.momo.util.ak
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Integer):int
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Long):long
      com.immomo.momo.util.ak.a(java.lang.String, int):void
      com.immomo.momo.util.ak.a(java.lang.String, long):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.String):void
      com.immomo.momo.util.ak.a(java.lang.String, boolean):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Boolean):boolean
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void */
    public final void a() {
        this.M.a("openAlarm", (Object) Boolean.valueOf(this.f2996a));
        this.M.a("sound", (Object) Boolean.valueOf(this.b));
        this.M.a("phonebook_syn", (Object) Boolean.valueOf(this.c));
        this.M.a("msg_roaming_formember", (Object) Boolean.valueOf(this.A));
        this.M.a("sinafriend_syn", (Object) Boolean.valueOf(this.d));
        this.M.a("locate_only_use_gps", (Object) Boolean.valueOf(this.B));
        this.M.a("vibrate", (Object) Boolean.valueOf(this.e));
        this.M.a("is_show_msg_content", (Object) Boolean.valueOf(this.f));
        this.M.a("hiddenmode", (Object) Integer.valueOf(this.l));
        this.M.a("location_mode", (Object) Integer.valueOf(this.G));
        this.g = String.valueOf(this.I) + "-" + this.J;
        this.M.a("mutetime", (Object) this.g);
        this.M.a("open_mutetime", (Object) Boolean.valueOf(this.h));
        this.M.a("momo_server_locate_count", (Object) Integer.valueOf(this.C));
        this.M.a("location_google_server_locate_count", (Object) Integer.valueOf(this.D));
        this.M.a("key_is_google_first", (Object) Boolean.valueOf(this.E));
        this.M.a("key_is_httplocate_from_google", (Object) Boolean.valueOf(this.F));
        this.M.a("notify_friend", (Object) Boolean.valueOf(this.u));
        this.M.a("notify_event", (Object) Boolean.valueOf(this.v));
        this.M.a("notify_tieba", (Object) Boolean.valueOf(this.w));
        this.M.a("notify_groupnotice", (Object) Boolean.valueOf(this.t));
        this.M.a("notify_stranger", (Object) Boolean.valueOf(this.y));
        this.M.a("key_audio_type", this.i);
        this.M.a("shop_update_time", this.j);
        this.M.a("bothlist_version", this.k);
        this.M.a("neayby_filter_gender", (Object) Integer.valueOf(this.n.ordinal()));
        this.M.a("neayby_filter_timeline", (Object) Integer.valueOf(this.o.ordinal()));
        this.M.a("neayby_filter_bind", (Object) Integer.valueOf(this.p.ordinal()));
        this.M.a("neayby_filter_age", (Object) Integer.valueOf(this.q));
        this.M.a("neayby_filter_constellation", (Object) Integer.valueOf(this.r));
        this.M.a("neayby_filter_industry", (Object) this.s);
        this.M.a("momo_alert_flags", (Object) this.H);
        this.M.a("momo_gsearch_history", (Object) this.m);
        this.M.a("key_tieba_agree_protocol", (Object) Boolean.valueOf(this.z));
    }

    public final void a(Integer num, Integer num2) {
        if (num != null && num2 != null) {
            this.I = num.intValue();
            this.J = num2.intValue();
            this.g = num + "-" + num2;
        }
    }

    public final void a(String str) {
        this.M.b(str);
    }

    public final void a(String str, Object obj) {
        this.M.a(str, obj);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.Long]
     candidates:
      com.immomo.momo.util.ak.a(android.content.Context, java.lang.String):com.immomo.momo.util.ak
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Integer):int
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Long):long
      com.immomo.momo.util.ak.a(java.lang.String, int):void
      com.immomo.momo.util.ak.a(java.lang.String, long):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.String):void
      com.immomo.momo.util.ak.a(java.lang.String, boolean):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Boolean):boolean
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void */
    public final void a(String str, Date date) {
        if (g.q() != null) {
            try {
                this.M.a(str, (Object) Long.valueOf(date == null ? 0 : date.getTime()));
            } catch (Exception e2) {
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.immomo.momo.util.ak.a(android.content.Context, java.lang.String):com.immomo.momo.util.ak
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Integer):int
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Long):long
      com.immomo.momo.util.ak.a(java.lang.String, int):void
      com.immomo.momo.util.ak.a(java.lang.String, long):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.String):void
      com.immomo.momo.util.ak.a(java.lang.String, boolean):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Boolean):boolean
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void */
    public final void a(Date date) {
        if (date != null) {
            this.M.a("lasttime_blacklist", (Object) a.c(date));
        }
    }

    public final Integer b() {
        return Integer.valueOf(this.I);
    }

    public final Object b(String str, Object obj) {
        try {
            return this.M.b(str, obj);
        } catch (Exception e2) {
            return obj;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.ak.a(java.lang.String, java.lang.Long):long
     arg types: [java.lang.String, long]
     candidates:
      com.immomo.momo.util.ak.a(android.content.Context, java.lang.String):com.immomo.momo.util.ak
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Integer):int
      com.immomo.momo.util.ak.a(java.lang.String, int):void
      com.immomo.momo.util.ak.a(java.lang.String, long):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.String):void
      com.immomo.momo.util.ak.a(java.lang.String, boolean):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Boolean):boolean
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Long):long */
    public final Date b(String str) {
        try {
            long a2 = this.M.a(str, (Long) 0L);
            if (a2 > 0) {
                return new Date(a2);
            }
        } catch (Exception e2) {
        }
        return null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.immomo.momo.util.ak.a(android.content.Context, java.lang.String):com.immomo.momo.util.ak
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Integer):int
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Long):long
      com.immomo.momo.util.ak.a(java.lang.String, int):void
      com.immomo.momo.util.ak.a(java.lang.String, long):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.String):void
      com.immomo.momo.util.ak.a(java.lang.String, boolean):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Boolean):boolean
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void */
    public final void b(Date date) {
        if (date != null) {
            this.M.a("lasttime_blacklist_success", (Object) a.c(date));
        }
    }

    public final i c(String str) {
        i iVar = (i) this.N.get(str);
        if (iVar != null) {
            return iVar;
        }
        i a2 = i.a(this.L, str);
        this.N.put(str, a2);
        return a2;
    }

    public final Integer c() {
        return Integer.valueOf(this.J);
    }

    public final void c(String str, Object obj) {
        a(str, obj);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.immomo.momo.util.ak.a(android.content.Context, java.lang.String):com.immomo.momo.util.ak
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Integer):int
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Long):long
      com.immomo.momo.util.ak.a(java.lang.String, int):void
      com.immomo.momo.util.ak.a(java.lang.String, long):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.String):void
      com.immomo.momo.util.ak.a(java.lang.String, boolean):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Boolean):boolean
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void */
    public final void c(Date date) {
        if (date != null) {
            this.M.a("lasttime_hidelist", (Object) a.c(date));
        }
    }

    public final o d(String str) {
        o oVar = (o) this.O.get(str);
        if (oVar != null) {
            return oVar;
        }
        o a2 = o.a(this.L, str);
        this.O.put(str, a2);
        return a2;
    }

    public final Date d() {
        String b2 = this.M.b("lasttime_blacklist", PoiTypeDef.All);
        try {
            if (!a.a((CharSequence) b2)) {
                return a.k(b2);
            }
        } catch (Exception e2) {
        }
        return null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.immomo.momo.util.ak.a(android.content.Context, java.lang.String):com.immomo.momo.util.ak
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Integer):int
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Long):long
      com.immomo.momo.util.ak.a(java.lang.String, int):void
      com.immomo.momo.util.ak.a(java.lang.String, long):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.String):void
      com.immomo.momo.util.ak.a(java.lang.String, boolean):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Boolean):boolean
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void */
    public final void d(Date date) {
        if (date != null) {
            this.M.a("lasttime_hidelist_success", (Object) a.c(date));
        }
    }

    public final Date e() {
        String b2 = this.M.b("lasttime_blacklist_success", PoiTypeDef.All);
        try {
            if (!a.a((CharSequence) b2)) {
                return a.k(b2);
            }
        } catch (Exception e2) {
        }
        return null;
    }

    public final Date f() {
        String b2 = this.M.b("lasttime_hidelist", PoiTypeDef.All);
        try {
            if (!a.a((CharSequence) b2)) {
                return a.k(b2);
            }
        } catch (Exception e2) {
        }
        return null;
    }

    public final String toString() {
        return "Preference [hiddenmode=" + this.l + ", openAlarm=" + this.f2996a + "location_mode, " + this.G + ", sound=" + this.b + ", phonebooksyn=" + this.c + ", onlygps=" + this.B + ", vibrate=" + this.e + ", mutetime=" + this.g + ", name=" + this.K + ", context=" + this.L + ", agreeTiebaProtocol=" + this.z + ", audioPlayType=" + this.i + "]";
    }
}
