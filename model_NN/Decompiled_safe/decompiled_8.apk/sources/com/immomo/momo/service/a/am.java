package com.immomo.momo.service.a;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.immomo.momo.service.bean.Message;

public final class am extends b {
    public am(SQLiteDatabase sQLiteDatabase) {
        super(sQLiteDatabase, "nearbysites2", "siteid");
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Cursor cursor) {
        return new String[]{cursor.getString(cursor.getColumnIndex("siteid")), cursor.getString(cursor.getColumnIndex(Message.DBFIELD_SAYHI))};
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object[]):void
     arg types: [java.lang.String, java.lang.String[]]
     candidates:
      com.immomo.momo.service.a.am.a(java.lang.Object, android.database.Cursor):void
      com.immomo.momo.service.a.b.a(android.database.Cursor, java.lang.String):int
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.String[]):android.database.Cursor
      com.immomo.momo.service.a.b.a(java.lang.String[], java.lang.String[]):java.util.List
      com.immomo.momo.service.a.b.a(java.lang.Object, android.database.Cursor):void
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object):void
      com.immomo.momo.service.a.b.a(java.lang.String[], java.lang.Object[]):void
      com.immomo.momo.service.a.b.a(java.lang.Object, java.io.Serializable):boolean
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.String):boolean
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object[]):void */
    public final /* synthetic */ void a(Object obj) {
        String[] strArr = (String[]) obj;
        StringBuilder sb = new StringBuilder();
        sb.append("insert into " + this.f2954a + " (").append("siteid,field1) values(?,?)");
        a(sb.toString(), (Object[]) new String[]{strArr[0], strArr[1]});
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void a(Object obj, Cursor cursor) {
    }
}
