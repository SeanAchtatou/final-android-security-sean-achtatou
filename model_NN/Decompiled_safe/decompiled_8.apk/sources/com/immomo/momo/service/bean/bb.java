package com.immomo.momo.service.bean;

import java.util.ArrayList;
import java.util.List;

public final class bb {

    /* renamed from: a  reason: collision with root package name */
    private int f3007a = 0;
    private List b = new ArrayList();

    public bb() {
    }

    public bb(int i) {
        this.f3007a = i;
    }

    public final int a() {
        return this.f3007a;
    }

    public final void a(List list) {
        this.b = list;
    }

    public final int b() {
        return this.b.size();
    }

    public final void b(List list) {
        this.b.addAll(list);
    }

    public final List c() {
        return this.b;
    }
}
