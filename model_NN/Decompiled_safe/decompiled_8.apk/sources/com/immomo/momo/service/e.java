package com.immomo.momo.service;

import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.a;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.GameApp;
import com.immomo.momo.service.bean.l;
import com.immomo.momo.util.m;
import com.immomo.momo.util.u;
import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import org.json.JSONArray;
import org.json.JSONObject;

public final class e {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public m f3045a = new m(this);

    public e() {
        new u();
    }

    public static l a() {
        l a2;
        boolean z = false;
        if (u.c("discover")) {
            a2 = (l) u.b("discover");
            com.immomo.momo.service.bean.m a3 = a2.a(2);
            com.immomo.momo.service.bean.m a4 = a2.a(1);
            com.immomo.momo.service.bean.m a5 = a2.a(3);
            com.immomo.momo.service.bean.m a6 = a2.a(5);
            a4.a(af.c().g(), !g.r().t);
            a3.a(af.c().s(), !g.r().u);
            a5.a(af.c().o() + af.c().n(), !g.r().v);
            int t = af.c().t();
            if (!g.r().w) {
                z = true;
            }
            a6.a(t, z);
        } else {
            a2 = a("nearby");
            if (a2 != null) {
                u.a("discover", a2);
            }
        }
        return a2;
    }

    private static l a(String str) {
        BufferedInputStream bufferedInputStream;
        boolean z = true;
        l lVar = new l();
        BufferedInputStream bufferedInputStream2 = null;
        try {
            com.immomo.momo.service.bean.m mVar = new com.immomo.momo.service.bean.m(1);
            lVar.a(mVar);
            com.immomo.momo.service.bean.m mVar2 = new com.immomo.momo.service.bean.m(2);
            lVar.a(mVar2);
            com.immomo.momo.service.bean.m mVar3 = new com.immomo.momo.service.bean.m(3);
            lVar.a(mVar3);
            com.immomo.momo.service.bean.m mVar4 = new com.immomo.momo.service.bean.m(5);
            lVar.a(mVar4);
            lVar.a(new com.immomo.momo.service.bean.m(6));
            com.immomo.momo.service.bean.m mVar5 = new com.immomo.momo.service.bean.m(7);
            lVar.a(mVar5);
            mVar.a(af.c().g(), !g.r().t);
            mVar2.a(af.c().s(), !g.r().u);
            mVar3.a(af.c().o() + af.c().n(), !g.r().v);
            int t = af.c().t();
            if (g.r().w) {
                z = false;
            }
            mVar4.a(t, z);
            File file = new File(a.t(), str);
            if (file.exists()) {
                bufferedInputStream = new BufferedInputStream(new FileInputStream(file));
                try {
                    byte[] b = android.support.v4.b.a.b(bufferedInputStream);
                    if (b != null && b.length > 0) {
                        JSONObject jSONObject = new JSONObject(new String(b));
                        if (jSONObject.has("game")) {
                            JSONArray optJSONArray = jSONObject.optJSONArray("game");
                            for (int i = 0; i < optJSONArray.length(); i++) {
                                JSONObject jSONObject2 = optJSONArray.getJSONObject(i);
                                com.immomo.momo.service.bean.m mVar6 = new com.immomo.momo.service.bean.m(4);
                                GameApp initWithJson = GameApp.initWithJson(jSONObject2.toString());
                                mVar6.c = initWithJson;
                                mVar6.b = initWithJson.appname;
                                lVar.b(mVar6);
                            }
                        }
                        if (jSONObject.has("tieba")) {
                            mVar4.d = jSONObject.getJSONObject("tieba").optBoolean("news");
                        }
                        if (jSONObject.has("emtion")) {
                            JSONObject jSONObject3 = jSONObject.getJSONObject("emtion");
                            mVar5.f = jSONObject3.optString("tips");
                            mVar5.e = jSONObject3.optString("action");
                            mVar5.g = jSONObject3.optLong("updatetime");
                            mVar5.d = jSONObject3.optBoolean("news");
                        }
                    }
                } catch (Exception e) {
                    bufferedInputStream2 = bufferedInputStream;
                    android.support.v4.b.a.a((Closeable) bufferedInputStream2);
                    return lVar;
                } catch (Throwable th) {
                    Throwable th2 = th;
                    bufferedInputStream2 = bufferedInputStream;
                    th = th2;
                    android.support.v4.b.a.a((Closeable) bufferedInputStream2);
                    throw th;
                }
            } else {
                bufferedInputStream = null;
            }
            android.support.v4.b.a.a((Closeable) bufferedInputStream);
        } catch (Exception e2) {
            android.support.v4.b.a.a((Closeable) bufferedInputStream2);
            return lVar;
        } catch (Throwable th3) {
            th = th3;
            android.support.v4.b.a.a((Closeable) bufferedInputStream2);
            throw th;
        }
        return lVar;
    }

    /* access modifiers changed from: private */
    public static void b(String str, l lVar) {
        BufferedWriter bufferedWriter;
        try {
            File file = new File(a.t(), str);
            file.createNewFile();
            bufferedWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file)));
            if (lVar != null) {
                try {
                    JSONObject jSONObject = new JSONObject();
                    com.immomo.momo.service.bean.m a2 = lVar.a(5);
                    JSONObject jSONObject2 = new JSONObject();
                    jSONObject2.put("news", a2.d);
                    jSONObject.put("tieba", jSONObject2);
                    com.immomo.momo.service.bean.m a3 = lVar.a(7);
                    JSONObject jSONObject3 = new JSONObject();
                    jSONObject3.put("tips", a3.f);
                    jSONObject3.put("action", a3.e);
                    jSONObject3.put("updatetime", a3.g);
                    jSONObject3.put("news", a3.d);
                    jSONObject.put("emtion", jSONObject3);
                    JSONArray jSONArray = new JSONArray();
                    for (com.immomo.momo.service.bean.m mVar : lVar.b()) {
                        try {
                            if (mVar.c != null) {
                                jSONArray.put(new JSONObject(((GameApp) mVar.c).toJson()));
                            }
                        } catch (Exception e) {
                        }
                    }
                    jSONObject.put("game", jSONArray);
                    bufferedWriter.write(jSONObject.toString());
                } catch (Throwable th) {
                    th = th;
                }
            } else {
                bufferedWriter.write(PoiTypeDef.All);
            }
            bufferedWriter.flush();
            android.support.v4.b.a.a(bufferedWriter);
        } catch (Throwable th2) {
            th = th2;
            bufferedWriter = null;
            android.support.v4.b.a.a(bufferedWriter);
            throw th;
        }
    }

    public final void a(l lVar) {
        try {
            u.a("discover", lVar);
            b("nearby", lVar);
        } catch (Exception e) {
            this.f3045a.a((Throwable) e);
        }
    }

    public final void b(l lVar) {
        u.a("discover", lVar);
        com.immomo.momo.android.c.u.b().execute(new f(this, lVar));
    }
}
