package com.immomo.momo.service.a;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public final class ap extends b {
    public ap(SQLiteDatabase sQLiteDatabase) {
        super(sQLiteDatabase, "sayhi", "remoteid");
    }

    private void a(StringBuilder sb, Map map) {
        Map.Entry entry = (Map.Entry) map.entrySet().iterator().next();
        a(sb.toString(), new Object[]{Long.valueOf(a((Date) entry.getValue())), entry.getKey()});
    }

    private static void a(Map map, Cursor cursor) {
        map.put(cursor.getString(cursor.getColumnIndex("remoteid")), a(cursor.getLong(cursor.getColumnIndex("time"))));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.service.a.ap.a(java.util.Map, android.database.Cursor):void
     arg types: [java.util.HashMap, android.database.Cursor]
     candidates:
      com.immomo.momo.service.a.ap.a(java.lang.StringBuilder, java.util.Map):void
      com.immomo.momo.service.a.ap.a(java.lang.Object, android.database.Cursor):void
      com.immomo.momo.service.a.b.a(android.database.Cursor, java.lang.String):int
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.String[]):android.database.Cursor
      com.immomo.momo.service.a.b.a(java.lang.String[], java.lang.String[]):java.util.List
      com.immomo.momo.service.a.b.a(java.lang.Object, android.database.Cursor):void
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object):void
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object[]):void
      com.immomo.momo.service.a.b.a(java.lang.String[], java.lang.Object[]):void
      com.immomo.momo.service.a.b.a(java.lang.Object, java.io.Serializable):boolean
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.String):boolean
      com.immomo.momo.service.a.ap.a(java.util.Map, android.database.Cursor):void */
    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Cursor cursor) {
        HashMap hashMap = new HashMap();
        a((Map) hashMap, cursor);
        return hashMap;
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void a(Object obj, Cursor cursor) {
        a((Map) obj, cursor);
    }

    /* renamed from: b */
    public final void a(Map map) {
        StringBuilder sb = new StringBuilder();
        sb.append("insert into " + this.f2954a + " (").append("time, remoteid) values(?,?)");
        a(sb, map);
    }

    public final void c(Map map) {
        StringBuilder sb = new StringBuilder();
        sb.append("update " + this.f2954a + " set ").append("time=? where remoteid=?");
        a(sb, map);
    }
}
