package com.immomo.momo.service;

import com.immomo.momo.a;
import com.immomo.momo.service.bean.bh;
import com.immomo.momo.util.m;
import com.immomo.momo.util.u;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;

public final class au {

    /* renamed from: a  reason: collision with root package name */
    private m f2967a = new m("test_momo", "[ -- from WallpaperService -- ]");

    /* JADX WARNING: Unknown top exception splitter block from list: {B:22:0x0066=Splitter:B:22:0x0066, B:18:0x005c=Splitter:B:18:0x005c} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.List a() {
        /*
            r7 = this;
            java.lang.String r0 = "wallpaperlist"
            boolean r0 = com.immomo.momo.util.u.c(r0)
            if (r0 == 0) goto L_0x0011
            java.lang.String r0 = "wallpaperlist"
            java.lang.Object r0 = com.immomo.momo.util.u.b(r0)
            java.util.List r0 = (java.util.List) r0
        L_0x0010:
            return r0
        L_0x0011:
            java.util.ArrayList r2 = new java.util.ArrayList
            r2.<init>()
            java.io.File r3 = new java.io.File
            java.io.File r0 = com.immomo.momo.a.a()
            java.lang.String r1 = "wallpaperlist"
            r3.<init>(r0, r1)
            r1 = 0
            boolean r0 = r3.exists()     // Catch:{ IOException -> 0x005b, JSONException -> 0x0065 }
            if (r0 == 0) goto L_0x0083
            java.io.FileInputStream r0 = new java.io.FileInputStream     // Catch:{ IOException -> 0x005b, JSONException -> 0x0065 }
            r0.<init>(r3)     // Catch:{ IOException -> 0x005b, JSONException -> 0x0065 }
            byte[] r1 = android.support.v4.b.a.b(r0)     // Catch:{ IOException -> 0x007e, JSONException -> 0x0079, all -> 0x0074 }
            org.json.JSONArray r3 = new org.json.JSONArray     // Catch:{ IOException -> 0x007e, JSONException -> 0x0079, all -> 0x0074 }
            java.lang.String r4 = new java.lang.String     // Catch:{ IOException -> 0x007e, JSONException -> 0x0079, all -> 0x0074 }
            r4.<init>(r1)     // Catch:{ IOException -> 0x007e, JSONException -> 0x0079, all -> 0x0074 }
            r3.<init>(r4)     // Catch:{ IOException -> 0x007e, JSONException -> 0x0079, all -> 0x0074 }
            r1 = 0
        L_0x003c:
            int r4 = r3.length()     // Catch:{ IOException -> 0x007e, JSONException -> 0x0079, all -> 0x0074 }
            if (r1 < r4) goto L_0x004c
        L_0x0042:
            android.support.v4.b.a.a(r0)
        L_0x0045:
            java.lang.String r0 = "wallpaperlist"
            com.immomo.momo.util.u.a(r0, r2)
            r0 = r2
            goto L_0x0010
        L_0x004c:
            com.immomo.momo.service.bean.bh r4 = new com.immomo.momo.service.bean.bh     // Catch:{ IOException -> 0x007e, JSONException -> 0x0079, all -> 0x0074 }
            java.lang.String r5 = r3.getString(r1)     // Catch:{ IOException -> 0x007e, JSONException -> 0x0079, all -> 0x0074 }
            r4.<init>(r5)     // Catch:{ IOException -> 0x007e, JSONException -> 0x0079, all -> 0x0074 }
            r2.add(r4)     // Catch:{ IOException -> 0x007e, JSONException -> 0x0079, all -> 0x0074 }
            int r1 = r1 + 1
            goto L_0x003c
        L_0x005b:
            r0 = move-exception
        L_0x005c:
            com.immomo.momo.util.m r3 = r7.f2967a     // Catch:{ all -> 0x006f }
            r3.a(r0)     // Catch:{ all -> 0x006f }
            android.support.v4.b.a.a(r1)
            goto L_0x0045
        L_0x0065:
            r0 = move-exception
        L_0x0066:
            com.immomo.momo.util.m r3 = r7.f2967a     // Catch:{ all -> 0x006f }
            r3.a(r0)     // Catch:{ all -> 0x006f }
            android.support.v4.b.a.a(r1)
            goto L_0x0045
        L_0x006f:
            r0 = move-exception
        L_0x0070:
            android.support.v4.b.a.a(r1)
            throw r0
        L_0x0074:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
            goto L_0x0070
        L_0x0079:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
            goto L_0x0066
        L_0x007e:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
            goto L_0x005c
        L_0x0083:
            r0 = r1
            goto L_0x0042
        */
        throw new UnsupportedOperationException("Method not decompiled: com.immomo.momo.service.au.a():java.util.List");
    }

    public final void a(List list) {
        u.a("wallpaperlist", list);
        JSONArray jSONArray = new JSONArray();
        BufferedWriter bufferedWriter = null;
        try {
            Iterator it = list.iterator();
            while (it.hasNext()) {
                jSONArray.put(((bh) it.next()).f3013a);
            }
            File file = new File(a.a(), "wallpaperlist");
            if (!file.exists()) {
                file.createNewFile();
            }
            BufferedWriter bufferedWriter2 = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file)));
            try {
                bufferedWriter2.write(jSONArray.toString());
                bufferedWriter2.flush();
                android.support.v4.b.a.a(bufferedWriter2);
            } catch (IOException e) {
                e = e;
                bufferedWriter = bufferedWriter2;
                try {
                    this.f2967a.a((Throwable) e);
                    android.support.v4.b.a.a(bufferedWriter);
                } catch (Throwable th) {
                    th = th;
                    android.support.v4.b.a.a(bufferedWriter);
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                bufferedWriter = bufferedWriter2;
                android.support.v4.b.a.a(bufferedWriter);
                throw th;
            }
        } catch (IOException e2) {
            e = e2;
        }
    }
}
