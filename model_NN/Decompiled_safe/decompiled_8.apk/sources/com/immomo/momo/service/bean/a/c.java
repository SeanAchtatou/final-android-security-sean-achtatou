package com.immomo.momo.service.bean.a;

import android.support.v4.b.a;

public final class c {

    /* renamed from: a  reason: collision with root package name */
    private String f2972a;
    private String b;
    private String c;
    private boolean d = false;

    public final String a() {
        return this.f2972a.replaceAll("&lsb;", "[").replaceAll("&rsb;", "]").replaceAll("&vb;", "|");
    }

    public final void a(String str) {
        if (!a.a((CharSequence) str)) {
            StringBuilder sb = new StringBuilder(str);
            sb.deleteCharAt(0);
            sb.deleteCharAt(sb.length() - 1);
            String[] split = sb.toString().split("\\|");
            if (split.length > 0) {
                this.f2972a = split[0];
                if (split.length > 1) {
                    this.b = split[1];
                    if (split.length > 2) {
                        this.c = split[2];
                    }
                    if (split.length > 3) {
                        this.d = split[3].equals("1");
                    } else {
                        this.d = false;
                    }
                }
            }
        }
    }

    public final String b() {
        return this.b;
    }

    public final String c() {
        return this.c;
    }

    public final void d() {
        this.d = true;
    }

    public final boolean e() {
        return this.d;
    }

    public final String f() {
        return "[" + this.f2972a + "|" + this.b + "|" + this.c + "]";
    }

    public final String toString() {
        return "[" + this.f2972a + "|" + this.b + "|" + this.c + "|" + (this.d ? 1 : 0) + "]";
    }
}
