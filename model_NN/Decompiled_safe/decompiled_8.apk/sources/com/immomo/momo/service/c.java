package com.immomo.momo.service;

import android.database.Cursor;
import android.provider.ContactsContract;
import android.support.v4.b.a;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.g;
import com.immomo.momo.service.a.aq;
import com.immomo.momo.service.a.ar;
import com.immomo.momo.service.a.az;
import com.immomo.momo.service.a.f;
import com.immomo.momo.service.bean.Message;
import com.immomo.momo.service.bean.ax;
import com.immomo.momo.service.bean.i;
import com.immomo.momo.util.u;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public final class c extends b {
    private static c f = null;
    private f c = null;
    private aq d = null;
    private az e = null;

    public c() {
        this.f2968a = g.d().e();
        this.c = new f(this.f2968a);
        this.d = new aq(this.f2968a);
        this.e = new az(this.f2968a);
    }

    private c(byte b) {
    }

    public static c c() {
        if (f == null) {
            f = new c((byte) 0);
        }
        return f;
    }

    public static void d() {
        u.a("contactelist");
        u.a("contactelisttime");
    }

    private Map j() {
        Date date = new Date();
        if (u.c("contactelist") && u.c("contactelisttime") && date.getTime() - ((Date) u.b("contactelisttime")).getTime() < 7200000) {
            return (Map) u.b("contactelist");
        }
        HashMap hashMap = new HashMap();
        Cursor query = g.e().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, new String[]{"data1", "display_name", "contact_id"}, null, null, null);
        if (query != null) {
            while (query.moveToNext()) {
                String string = query.getString(0);
                if (!a.a((CharSequence) string) && string.length() >= 11) {
                    String string2 = query.getString(1);
                    d dVar = new d(this, (byte) 0);
                    dVar.f3044a = query.getInt(2);
                    dVar.c = string;
                    dVar.b = string2;
                    String replaceAll = string.replaceAll("[^\\d]", PoiTypeDef.All);
                    if (replaceAll.length() > 11) {
                        replaceAll = replaceAll.substring(replaceAll.length() - 11, replaceAll.length());
                    }
                    String lowerCase = a.n("$_$+86" + replaceAll).toLowerCase();
                    dVar.d = String.valueOf(lowerCase.substring(lowerCase.length() - 10, lowerCase.length())) + lowerCase.substring(0, 9);
                    hashMap.put(dVar.d, dVar);
                }
            }
            query.close();
        }
        u.a("contactelist", hashMap);
        u.a("contactelist", date);
        return hashMap;
    }

    public final String a(String str) {
        d dVar = (d) j().get(str);
        if (dVar != null) {
            return a.a(dVar.b) ? dVar.c : dVar.b;
        }
        return null;
    }

    public final List a(int i) {
        return this.c.a(new String[0], new String[0], Message.DBFIELD_ID, false, i, 21);
    }

    public final Map a(boolean z) {
        HashMap hashMap = new HashMap();
        for (d dVar : j().values()) {
            String str = a.a(dVar.b) ? dVar.c : dVar.b;
            if (z) {
                hashMap.put(dVar.d, str);
            } else {
                hashMap.put(dVar.c, str);
            }
        }
        return hashMap;
    }

    public final void a(i iVar) {
        this.c.b(iVar);
    }

    public final void a(Collection collection) {
        this.f2968a.beginTransaction();
        try {
            this.e.c();
            Iterator it = collection.iterator();
            while (it.hasNext()) {
                this.e.a((String) it.next());
            }
            this.f2968a.setTransactionSuccessful();
        } catch (Exception e2) {
            this.b.a((Throwable) e2);
        } finally {
            this.f2968a.endTransaction();
        }
    }

    public final void a(List list) {
        this.f2968a.beginTransaction();
        try {
            Iterator it = list.iterator();
            while (it.hasNext()) {
                String str = (String) it.next();
                try {
                    this.b.a((Object) ("addUploadedList, number=" + str));
                    this.e.a(str);
                } catch (Exception e2) {
                }
            }
            this.f2968a.setTransactionSuccessful();
        } catch (Exception e3) {
            this.b.a((Throwable) e3);
        } finally {
            this.f2968a.endTransaction();
        }
    }

    public final String b(String str) {
        d dVar = (d) j().get(str);
        if (dVar != null) {
            return dVar.c;
        }
        return null;
    }

    public final void b(i iVar) {
        try {
            this.f2968a.beginTransaction();
            boolean z = true;
            ax axVar = (ax) this.d.a((Serializable) "-2230");
            if (axVar == null) {
                axVar = new ax("-2230");
                z = false;
            }
            axVar.i = iVar.o();
            axVar.j = ar.a(this.f2968a).a();
            axVar.h = iVar.f();
            axVar.o = 5;
            if (z) {
                this.d.b(axVar);
            } else {
                this.d.a(axVar);
            }
            this.c.a(iVar);
            this.f2968a.setTransactionSuccessful();
        } catch (Exception e2) {
            this.b.a((Throwable) e2);
        } finally {
            this.f2968a.endTransaction();
        }
    }

    public final void b(List list) {
        this.f2968a.beginTransaction();
        try {
            Iterator it = list.iterator();
            while (it.hasNext()) {
                this.c.b((Serializable) ((i) it.next()).o());
            }
            if (this.c.c(new String[0], new String[0]) > 0) {
                String a2 = this.c.a("field5", Message.DBFIELD_ID, new String[0], new String[0]);
                if (!a.a((CharSequence) a2)) {
                    this.d.a("s_lastmsgid", a2, "-2230");
                }
            }
            this.f2968a.setTransactionSuccessful();
        } catch (Exception e2) {
            this.b.a((Throwable) e2);
        } finally {
            this.f2968a.endTransaction();
        }
    }

    public final i c(String str) {
        return (i) this.c.a((Serializable) str);
    }

    public final boolean d(String str) {
        return this.c.a(str) != null;
    }

    public final Collection e() {
        Map j = j();
        List b = this.e.b();
        HashSet hashSet = new HashSet();
        if (j.size() == b.size()) {
            Iterator it = j.values().iterator();
            while (true) {
                if (it.hasNext()) {
                    if (!b.contains(((d) it.next()).d)) {
                        hashSet.addAll(j.keySet());
                        break;
                    }
                } else {
                    break;
                }
            }
        } else {
            hashSet.addAll(j.keySet());
        }
        return hashSet;
    }

    public final boolean f() {
        Map j = j();
        List b = this.e.b();
        if (!j.isEmpty()) {
            for (String contains : j.keySet()) {
                if (!b.contains(contains)) {
                    return true;
                }
            }
        }
        return false;
    }

    public final int g() {
        return this.c.c(new String[]{"field6"}, new String[]{"0"});
    }

    public final void h() {
        this.c.a(new String[]{"field6"}, new Object[]{true}, new String[0], new String[0]);
    }

    public final void i() {
        this.c.c();
    }
}
