package com.immomo.momo.service.a;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.b.a;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.Message;
import com.immomo.momo.service.bean.aa;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.service.bean.u;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class m extends b {
    private static Set d = new HashSet();

    public m(SQLiteDatabase sQLiteDatabase) {
        super(sQLiteDatabase, "events", "evid");
    }

    private static void a(u uVar, Cursor cursor) {
        uVar.e = c(cursor, Message.DBFIELD_GROUPID);
        uVar.f = c(cursor, "field24");
        uVar.c = c(cursor, Message.DBFIELD_LOCATIONJSON);
        uVar.a(a(cursor, "field5"));
        uVar.c(a(cursor, "field16"));
        uVar.k = c(cursor, "field9");
        uVar.g = c(cursor, "field6");
        uVar.d = c(cursor, Message.DBFIELD_CONVERLOCATIONJSON);
        uVar.q = c(cursor, "field13");
        uVar.r = c(cursor, "field14");
        uVar.p = c(cursor, "field12");
        uVar.f3038a = c(cursor, "evid");
        uVar.b = c(cursor, Message.DBFIELD_SAYHI);
        uVar.i = a(cursor, "field8");
        uVar.j = a(cursor, "field22");
        uVar.a((float) a(cursor, "field7"));
        uVar.u = c(cursor, "field18");
        uVar.v = c(cursor, "field19");
        uVar.t = a(cursor, "field17") == 1;
        uVar.x = c(cursor, "field21");
        String c = c(cursor, "field23");
        if (!a.a((CharSequence) c)) {
            try {
                JSONObject jSONObject = new JSONObject(c);
                uVar.y = jSONObject.optDouble("lat", 0.0d);
                uVar.z = jSONObject.optDouble("lng", 0.0d);
            } catch (JSONException e) {
            }
        }
        String c2 = c(cursor, "field25");
        if (!a.a((CharSequence) c2)) {
            try {
                ArrayList arrayList = new ArrayList();
                u uVar2 = null;
                uVar2.a(arrayList);
                JSONArray jSONArray = new JSONArray(c2);
                for (int i = 0; i < jSONArray.length(); i++) {
                    JSONObject jSONObject2 = jSONArray.getJSONObject(i);
                    aa aaVar = new aa();
                    aaVar.a(jSONObject2);
                    arrayList.add(aaVar);
                }
            } catch (Exception e2) {
            }
        } else {
            uVar.a((List) null);
        }
        String c3 = c(cursor, "field10");
        if (!a.a((CharSequence) c3)) {
            try {
                JSONArray jSONArray2 = new JSONArray(c3);
                ArrayList arrayList2 = new ArrayList();
                for (int i2 = 0; i2 < jSONArray2.length(); i2++) {
                    JSONObject jSONObject3 = jSONArray2.getJSONObject(i2);
                    bf bfVar = new bf();
                    bfVar.h = jSONObject3.getString("uid");
                    bfVar.i = jSONObject3.getString("name");
                    String string = jSONObject3.getString("avatar");
                    if (!a.a((CharSequence) string)) {
                        bfVar.ae = new String[]{string};
                    }
                    arrayList2.add(bfVar);
                }
                uVar.l = arrayList2;
            } catch (Exception e3) {
            }
        } else {
            uVar.l = null;
        }
        if (!a.a((CharSequence) uVar.f3038a)) {
            d.add(uVar.f3038a);
        }
        if (1 == cursor.getInt(cursor.getColumnIndex("field25"))) {
            uVar.m = true;
        } else {
            uVar.m = false;
        }
        if (1 == cursor.getInt(cursor.getColumnIndex("field26"))) {
            uVar.n = true;
        } else {
            uVar.n = false;
        }
        if (1 == cursor.getInt(cursor.getColumnIndex("field27"))) {
            uVar.o = true;
        } else {
            uVar.o = false;
        }
    }

    private static u b(Cursor cursor) {
        u uVar = new u();
        a(uVar, cursor);
        return uVar;
    }

    public static void g() {
        Set set = d;
        String[] strArr = new String[set.size()];
        set.toArray(strArr);
        set.clear();
        if (g.d().h() != null) {
            new m(g.d().h()).a("field20", new Date(), "evid", strArr);
        }
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Cursor cursor) {
        return b(cursor);
    }

    public final void a(u uVar) {
        int i = 1;
        JSONArray jSONArray = new JSONArray();
        if (uVar.l != null) {
            for (bf bfVar : uVar.l) {
                try {
                    JSONObject jSONObject = new JSONObject();
                    jSONObject.put("uid", bfVar.h);
                    jSONObject.put("name", bfVar.i);
                    jSONObject.put("avatar", bfVar.getLoadImageId());
                } catch (JSONException e) {
                    this.c.a((Throwable) e);
                }
            }
        }
        JSONObject jSONObject2 = new JSONObject();
        try {
            jSONObject2.put("lat", uVar.y);
            jSONObject2.put("lng", uVar.z);
        } catch (JSONException e2) {
        }
        JSONArray jSONArray2 = new JSONArray();
        List b = uVar.b();
        if (b != null) {
            int i2 = 0;
            while (i2 < 3 && i2 < b.size()) {
                jSONArray2.put(((aa) b.get(i2)).f());
                i2++;
            }
        }
        String[] strArr = {Message.DBFIELD_GROUPID, "field24", Message.DBFIELD_LOCATIONJSON, "field5", "field9", "field7", "field23", "evid", "field6", Message.DBFIELD_CONVERLOCATIONJSON, "field13", "field14", "field12", "field25", "field10", "field16", "field8", "field22", Message.DBFIELD_SAYHI, "field18", "field19", "field17", "field20", "field21", "field25", "field26", "field27"};
        Object[] objArr = new Object[27];
        objArr[0] = uVar.e;
        objArr[1] = uVar.f;
        objArr[2] = uVar.c;
        objArr[3] = Integer.valueOf(uVar.d());
        objArr[4] = uVar.k;
        objArr[5] = Float.valueOf(uVar.a());
        objArr[6] = jSONObject2.toString();
        objArr[7] = uVar.f3038a;
        objArr[8] = uVar.g;
        objArr[9] = uVar.d;
        objArr[10] = uVar.q;
        objArr[11] = uVar.r;
        objArr[12] = uVar.p;
        objArr[13] = jSONArray2.toString();
        objArr[14] = jSONArray;
        objArr[15] = Integer.valueOf(uVar.f());
        objArr[16] = Integer.valueOf(uVar.i);
        objArr[17] = Integer.valueOf(uVar.j);
        objArr[18] = uVar.b;
        objArr[19] = uVar.u;
        objArr[20] = uVar.v;
        objArr[21] = Boolean.valueOf(uVar.t);
        objArr[22] = new Date();
        objArr[23] = uVar.x;
        objArr[24] = Integer.valueOf(uVar.m ? 1 : 0);
        objArr[25] = Integer.valueOf(uVar.n ? 1 : 0);
        if (!uVar.o) {
            i = 0;
        }
        objArr[26] = Integer.valueOf(i);
        b(strArr, objArr);
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void a(Object obj, Cursor cursor) {
        a((u) obj, cursor);
    }

    public final void b(u uVar) {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("lat", uVar.y);
            jSONObject.put("lng", uVar.z);
        } catch (JSONException e) {
        }
        JSONArray jSONArray = new JSONArray();
        List b = uVar.b();
        int i = 0;
        while (i < 3 && i < b.size()) {
            jSONArray.put(((aa) b.get(i)).f());
            i++;
        }
        String[] strArr = {Message.DBFIELD_GROUPID, "field24", Message.DBFIELD_LOCATIONJSON, "field5", "field9", "field7", "field23", "field6", Message.DBFIELD_CONVERLOCATIONJSON, "field13", "field14", "field12", "field25", "field16", "field8", "field22", Message.DBFIELD_SAYHI, "field18", "field19", "field17", "field21", "field25", "field26", "field27"};
        Object[] objArr = new Object[24];
        objArr[0] = uVar.e;
        objArr[1] = uVar.f;
        objArr[2] = uVar.c;
        objArr[3] = Integer.valueOf(uVar.d());
        objArr[4] = uVar.k;
        objArr[5] = Float.valueOf(uVar.a());
        objArr[6] = jSONObject.toString();
        objArr[7] = uVar.g;
        objArr[8] = uVar.d;
        objArr[9] = uVar.q;
        objArr[10] = uVar.r;
        objArr[11] = uVar.p;
        objArr[12] = jSONArray.toString();
        objArr[13] = Integer.valueOf(uVar.f());
        objArr[14] = Integer.valueOf(uVar.i);
        objArr[15] = Integer.valueOf(uVar.j);
        objArr[16] = uVar.b;
        objArr[17] = uVar.u;
        objArr[18] = uVar.v;
        objArr[19] = Boolean.valueOf(uVar.t);
        objArr[20] = uVar.x;
        objArr[21] = Integer.valueOf(uVar.m ? 1 : 0);
        objArr[22] = Integer.valueOf(uVar.n ? 1 : 0);
        objArr[23] = Integer.valueOf(uVar.o ? 1 : 0);
        a(strArr, objArr, new String[]{"evid"}, new Object[]{uVar.f3038a});
    }
}
