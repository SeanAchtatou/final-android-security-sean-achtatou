package com.immomo.momo.service.bean;

public final class as {

    /* renamed from: a  reason: collision with root package name */
    public String f2995a;
    public String b;
    public String c;
    public String d;

    public as() {
    }

    public as(String str) {
        this.f2995a = str;
    }

    public as(String str, String str2, String str3, String str4) {
        this.f2995a = str;
        this.b = str2;
        this.d = str3;
        this.c = str4;
    }

    public final boolean equals(Object obj) {
        return (obj == null || this.f2995a == null || !(obj instanceof as)) ? super.equals(obj) : this.f2995a.equals(((as) obj).f2995a);
    }

    public final String toString() {
        return "momoid=" + this.f2995a + ",name=" + this.b + ",avatar=" + this.d + ",phoneNum=" + this.c + ",";
    }
}
