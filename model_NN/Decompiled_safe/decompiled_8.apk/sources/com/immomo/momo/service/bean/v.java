package com.immomo.momo.service.bean;

public enum v {
    NONE(0, "不限时间"),
    TODAY(1, "今天"),
    TOMORROW(2, "明天"),
    WEEKEND(4, "周末");
    
    private final int e;
    private final String f;

    private v(int i, String str) {
        this.e = i;
        this.f = str;
    }

    public static v a(int i) {
        for (v vVar : values()) {
            if (vVar.e == i) {
                return vVar;
            }
        }
        return NONE;
    }

    public static String[] c() {
        v[] values = values();
        String[] strArr = new String[values.length];
        for (int i = 0; i < values.length; i++) {
            strArr[i] = values[i].f;
        }
        return strArr;
    }

    public final int a() {
        return this.e;
    }

    public final String b() {
        return this.f;
    }
}
