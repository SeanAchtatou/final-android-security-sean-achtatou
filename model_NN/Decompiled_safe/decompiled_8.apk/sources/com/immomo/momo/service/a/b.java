package com.immomo.momo.service.a;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.support.v4.b.a;
import com.immomo.momo.service.bean.Message;
import com.immomo.momo.util.m;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public abstract class b {

    /* renamed from: a  reason: collision with root package name */
    protected String f2954a;
    protected SQLiteDatabase b;
    protected m c;
    private String d;

    public b(SQLiteDatabase sQLiteDatabase, String str) {
        this.b = null;
        this.c = null;
        this.d = Message.DBFIELD_ID;
        this.f2954a = str;
        this.b = sQLiteDatabase;
        this.c = new m(getClass().getSimpleName()).a();
        this.c.b("SQL**==");
    }

    public b(SQLiteDatabase sQLiteDatabase, String str, String str2) {
        this(sQLiteDatabase, str);
        this.d = str2;
    }

    public static int a(Cursor cursor, String str) {
        int columnIndex = cursor.getColumnIndex(str);
        if (columnIndex >= 0) {
            return cursor.getInt(columnIndex);
        }
        return -1;
    }

    public static long a(Date date) {
        if (date == null) {
            return 0;
        }
        return date.getTime();
    }

    public static Date a(long j) {
        if (j <= 0) {
            return null;
        }
        return new Date(j);
    }

    public static long b(Cursor cursor, String str) {
        int columnIndex = cursor.getColumnIndex(str);
        if (columnIndex >= 0) {
            return cursor.getLong(columnIndex);
        }
        return -1;
    }

    private void b(String str) {
        if (this.b == null) {
            this.c.a((Object) "db instance is null");
            return;
        }
        this.c.b((Object) ("executeSQL-->" + str));
        g();
        this.b.execSQL(str);
    }

    public static String c(Cursor cursor, String str) {
        int columnIndex = cursor.getColumnIndex(str);
        if (columnIndex >= 0) {
            return cursor.getString(columnIndex);
        }
        return null;
    }

    public static Date d(Cursor cursor, String str) {
        return a(b(cursor, str));
    }

    public static boolean e(Cursor cursor, String str) {
        return a(cursor, str) == 1;
    }

    private void g() {
        if (this.b == null) {
            new RuntimeException(new NullPointerException("db is null"));
        }
        if (!this.b.isOpen()) {
            new RuntimeException(new SQLiteException("db is already closed"));
        }
        if (this.b.isReadOnly()) {
            new RuntimeException(new SQLiteException("db is read only"));
        }
    }

    public final int a(String str, String[] strArr, String[] strArr2, String[] strArr3) {
        StringBuilder sb = new StringBuilder("select count(*) c from " + this.f2954a + " where " + str + " in (" + a.a(strArr, "'", ",") + ") ");
        int length = strArr2.length;
        if (length > 0) {
            sb.append(" and ");
        }
        int i = 0;
        while (i < length) {
            sb.append(strArr2[i]).append("=? ");
            i++;
            if (i < length) {
                sb.append(" and ");
            }
        }
        Cursor a2 = a(sb.toString(), strArr3);
        a2.moveToFirst();
        int i2 = a2.getInt(0);
        a2.close();
        return i2;
    }

    public final Cursor a(String str, String[] strArr) {
        if (this.b == null) {
            this.c.a((Object) "db instance is null");
            return null;
        }
        this.c.b((Object) ("execute query --> " + str + "  |-------| params:" + Arrays.toString(strArr)));
        return this.b.rawQuery(str, strArr);
    }

    public final SQLiteDatabase a() {
        return this.b;
    }

    /* access modifiers changed from: protected */
    public abstract Object a(Cursor cursor);

    public final Object a(Serializable serializable) {
        if (this.b == null) {
            this.c.a((Object) "db instance is null");
            return null;
        }
        Cursor a2 = a("select * from " + this.f2954a + " where " + this.d + "=?", new String[]{new StringBuilder().append(serializable).toString()});
        if (a2.moveToNext()) {
            Object a3 = a(a2);
            a2.close();
            return a3;
        }
        a2.close();
        return null;
    }

    public final String a(String str, String str2, String[] strArr, String[] strArr2) {
        String str3 = null;
        int i = 0;
        if (this.b == null) {
            this.c.a((Object) "db instance is null");
        } else {
            StringBuilder sb = new StringBuilder("select " + str + " from " + this.f2954a + " where " + str2 + "=(select max(" + str2 + ") from " + this.f2954a + " ");
            int length = strArr.length;
            if (length > 0) {
                sb.append("where ");
            }
            while (i < length) {
                sb.append(strArr[i]).append("=? ");
                i++;
                if (i < length) {
                    sb.append("and ");
                }
            }
            sb.append(")");
            Cursor a2 = a(sb.toString(), strArr2);
            try {
                if (a2.moveToFirst()) {
                    str3 = a2.getString(0);
                } else {
                    a2.close();
                }
            } finally {
                a2.close();
            }
        }
        return str3;
    }

    public final List a(String str) {
        if (this.b == null) {
            this.c.a((Object) "db instance is null");
            return null;
        }
        ArrayList arrayList = new ArrayList();
        Cursor a2 = a(String.valueOf("select * from " + this.f2954a + " order by " + str) + " desc", new String[0]);
        while (a2.moveToNext()) {
            arrayList.add(a(a2));
        }
        a2.close();
        return arrayList;
    }

    public final List a(String str, Object[] objArr, String str2) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("select * from ").append(this.f2954a).append(" where ").append(str).append(" in (").append(a.a(objArr, "'", ",")).append(")");
        if (!a.a((CharSequence) str2)) {
            stringBuffer.append(" order by ").append(str2);
            stringBuffer.append(" asc");
        }
        return b(stringBuffer.toString(), new String[0]);
    }

    public final List a(String[] strArr, String[] strArr2) {
        ArrayList arrayList = new ArrayList();
        if (this.b == null) {
            this.c.a((Object) "db instance is null");
        } else {
            StringBuilder sb = new StringBuilder("select * from " + this.f2954a + " ");
            int length = strArr.length;
            if (length > 0) {
                sb.append("where ");
            }
            int i = 0;
            while (i < length) {
                sb.append(strArr[i]).append("=? ");
                i++;
                if (i < length) {
                    sb.append("and ");
                }
            }
            Cursor a2 = a(sb.toString(), strArr2);
            while (a2.moveToNext()) {
                arrayList.add(a(a2));
            }
            a2.close();
        }
        return arrayList;
    }

    public final List a(String[] strArr, String[] strArr2, String str, boolean z) {
        ArrayList arrayList = new ArrayList();
        if (this.b == null) {
            this.c.a((Object) "db instance is null");
        } else {
            StringBuilder sb = new StringBuilder("select * from " + this.f2954a + " ");
            int length = strArr.length;
            if (length > 0) {
                sb.append("where ");
            }
            int i = 0;
            while (i < length) {
                sb.append(strArr[i]).append("=? ");
                i++;
                if (i < length) {
                    sb.append("and ");
                }
            }
            sb.append(" order by ").append(str);
            if (!z) {
                sb.append(" desc");
            } else {
                sb.append(" asc");
            }
            Cursor a2 = a(sb.toString(), strArr2);
            while (a2.moveToNext()) {
                arrayList.add(a(a2));
            }
            a2.close();
        }
        return arrayList;
    }

    public final List a(String[] strArr, String[] strArr2, String str, boolean z, int i, int i2) {
        ArrayList arrayList = new ArrayList();
        if (this.b != null) {
            StringBuilder sb = new StringBuilder("select * from " + this.f2954a + " ");
            int length = strArr.length;
            if (length > 0) {
                sb.append("where ");
            }
            int i3 = 0;
            while (i3 < length) {
                sb.append(strArr[i3]).append("=? ");
                i3++;
                if (i3 < length) {
                    sb.append("and ");
                }
            }
            if (str != null) {
                sb.append(" order by ").append(str);
                if (z) {
                    sb.append(" asc");
                } else {
                    sb.append(" desc");
                }
            }
            sb.append(" limit ").append(i).append(",").append(i2);
            Cursor a2 = a(sb.toString(), strArr2);
            while (a2.moveToNext()) {
                arrayList.add(a(a2));
            }
            a2.close();
        }
        return arrayList;
    }

    public abstract void a(Object obj);

    /* access modifiers changed from: protected */
    public abstract void a(Object obj, Cursor cursor);

    public final void a(String str, Object obj) {
        if (this.b == null) {
            this.c.a((Object) "db instance is null");
            return;
        }
        a("delete from " + this.f2954a + " where " + str + "=?", new Object[]{obj.toString()});
    }

    public final void a(String str, Object obj, Serializable serializable) {
        if (this.b == null) {
            this.c.a((Object) "db instance is null");
            return;
        }
        a(new String[]{str}, new Object[]{obj}, new String[]{this.d}, new Object[]{serializable});
    }

    public final void a(String str, Object obj, Object obj2, String str2, Object[] objArr) {
        StringBuilder sb = new StringBuilder();
        sb.append("update " + this.f2954a + " set ").append(String.valueOf(str) + "=? ").append(" where " + str + "=? ").append(" and " + str2 + " in (" + a.a(objArr, "'", ",") + ")");
        a(sb.toString(), new Object[]{obj, obj2});
    }

    public final void a(String str, Object obj, String str2, Object[] objArr) {
        StringBuilder sb = new StringBuilder();
        sb.append("update " + this.f2954a + " set ").append(String.valueOf(str) + "=? ").append(" where " + str2 + " in (" + a.a(objArr, "'", ",") + ")");
        a(sb.toString(), new Object[]{obj});
    }

    public void a(String str, Object[] objArr) {
        if (this.b == null) {
            this.c.a((Object) "db instance is null");
            return;
        }
        this.c.b((Object) ("executeSQL-->" + str + "   |-------| params:" + Arrays.toString(objArr)));
        g();
        if (objArr != null) {
            for (int i = 0; i < objArr.length; i++) {
                Object obj = objArr[i];
                if (obj != null && (obj instanceof Date)) {
                    objArr[i] = Long.valueOf(a((Date) obj));
                } else if (obj != null && (obj instanceof Boolean)) {
                    objArr[i] = Integer.valueOf(((Boolean) obj).booleanValue() ? 1 : 0);
                }
            }
        }
        this.b.execSQL(str, objArr);
    }

    public final void a(Map map) {
        String[] strArr = new String[map.size()];
        Object[] objArr = new Object[map.size()];
        int i = 0;
        Iterator it = map.entrySet().iterator();
        while (true) {
            int i2 = i;
            if (!it.hasNext()) {
                b(strArr, objArr);
                return;
            }
            Map.Entry entry = (Map.Entry) it.next();
            strArr[i2] = (String) entry.getKey();
            objArr[i2] = entry.getValue();
            i = i2 + 1;
        }
    }

    public final void a(Map map, String[] strArr, Object[] objArr) {
        String[] strArr2 = new String[map.size()];
        Object[] objArr2 = new Object[map.size()];
        int i = 0;
        Iterator it = map.entrySet().iterator();
        while (true) {
            int i2 = i;
            if (!it.hasNext()) {
                a(strArr2, objArr2, strArr, objArr);
                return;
            }
            Map.Entry entry = (Map.Entry) it.next();
            strArr2[i2] = (String) entry.getKey();
            objArr2[i2] = entry.getValue();
            i = i2 + 1;
        }
    }

    public final void a(String[] strArr, Object[] objArr) {
        if (this.b == null) {
            this.c.a((Object) "db instance is null");
            return;
        }
        StringBuilder sb = new StringBuilder("delete from " + this.f2954a + " ");
        int length = strArr.length;
        if (length > 0) {
            sb.append("where ");
        }
        int i = 0;
        while (i < length) {
            sb.append(strArr[i]).append("=? ");
            i++;
            if (i < length) {
                sb.append("and ");
            }
        }
        a(sb.toString(), objArr);
    }

    public final void a(String[] strArr, Object[] objArr, Serializable serializable) {
        if (this.b == null) {
            this.c.a((Object) "db instance is null");
            return;
        }
        a(strArr, objArr, new String[]{this.d}, new Object[]{serializable});
    }

    public final void a(String[] strArr, Object[] objArr, String[] strArr2, Object[] objArr2) {
        int i = 0;
        if (this.b == null) {
            this.c.a((Object) "db instance is null");
        } else if (strArr.length != objArr.length) {
            throw new SQLiteException("fields.length != values.length");
        } else if (strArr2.length != objArr2.length) {
            throw new SQLiteException("whereFields.length != wherevalues.length");
        } else {
            StringBuilder sb = new StringBuilder();
            sb.append("update " + this.f2954a + " set ");
            Object[] objArr3 = new Object[(strArr.length + strArr2.length)];
            int i2 = 0;
            while (i2 < strArr.length) {
                objArr3[i2] = objArr[i2];
                sb.append(strArr[i2]).append("=? ");
                i2++;
                if (i2 < strArr.length) {
                    sb.append(", ");
                }
            }
            int length = strArr2.length;
            if (length > 0) {
                sb.append(" where ");
            }
            while (i < length) {
                objArr3[strArr.length + i] = objArr2[i];
                sb.append(strArr2[i]).append("=? ");
                i++;
                if (i < length) {
                    sb.append("and ");
                }
            }
            a(sb.toString(), objArr3);
        }
    }

    public final boolean a(Object obj, Serializable serializable) {
        boolean z = true;
        if (this.b == null) {
            this.c.a((Object) "db instance is null");
        }
        Cursor a2 = a("select * from " + this.f2954a + " where " + this.d + "=?", new String[]{new StringBuilder().append(serializable).toString()});
        if (a2.moveToNext()) {
            a(obj, a2);
        } else {
            z = false;
        }
        a2.close();
        return z;
    }

    public final boolean a(String str, String str2) {
        if (this.b == null) {
            this.c.a((Object) "db instance is null");
            return false;
        }
        return c(new String[]{str}, new String[]{str2}) > 0;
    }

    public final String[] a(String str, String[] strArr, String[] strArr2) {
        StringBuilder sb = new StringBuilder("select " + str + " from " + this.f2954a + " ");
        int length = strArr.length;
        if (length > 0) {
            sb.append("where ");
        }
        int i = 0;
        while (i < length) {
            sb.append(strArr[i]).append("=? ");
            i++;
            if (i < length) {
                sb.append("and ");
            }
        }
        ArrayList arrayList = new ArrayList();
        Cursor a2 = a(sb.toString(), strArr2);
        while (a2.moveToNext()) {
            arrayList.add(a2.getString(0));
        }
        a2.close();
        return (String[]) arrayList.toArray(new String[arrayList.size()]);
    }

    public final Object b(String str, Object obj) {
        Object obj2 = null;
        if (this.b == null) {
            this.c.a((Object) "db instance is null");
        } else {
            Cursor a2 = a("select * from " + this.f2954a + " where " + str + "=?", new String[]{obj.toString()});
            if (a2.moveToFirst()) {
                obj2 = a(a2);
            }
            a2.close();
        }
        return obj2;
    }

    public final Object b(String str, String[] strArr, String[] strArr2) {
        Object obj = null;
        if (this.b == null) {
            this.c.a((Object) "db instance is null");
        } else {
            StringBuilder sb = new StringBuilder("select * from " + this.f2954a + " where " + str + "=(select max(" + str + ") from " + this.f2954a + " ");
            int length = strArr.length;
            if (length > 0) {
                sb.append("where ");
            }
            int i = 0;
            while (i < length) {
                sb.append(strArr[i]).append("=? ");
                i++;
                if (i < length) {
                    sb.append("and ");
                }
            }
            sb.append(")");
            Cursor a2 = a(sb.toString(), strArr2);
            try {
                if (a2.moveToFirst()) {
                    obj = a(a2);
                    a2.close();
                } else {
                    a2.close();
                }
            } finally {
                a2.close();
            }
        }
        return obj;
    }

    public final Object b(String[] strArr, String[] strArr2) {
        if (this.b == null) {
            this.c.a((Object) "db instance is null");
            return null;
        }
        StringBuilder sb = new StringBuilder("select * from " + this.f2954a + " ");
        int length = strArr.length;
        if (length > 0) {
            sb.append("where ");
        }
        int i = 0;
        while (i < length) {
            sb.append(strArr[i]).append("=? ");
            i++;
            if (i < length) {
                sb.append("and ");
            }
        }
        Cursor a2 = a(sb.toString(), strArr2);
        if (a2.moveToNext()) {
            Object a3 = a(a2);
            a2.close();
            return a3;
        }
        a2.close();
        return null;
    }

    public final List b() {
        if (this.b == null) {
            this.c.a((Object) "db instance is null");
            return null;
        }
        ArrayList arrayList = new ArrayList();
        Cursor a2 = a("select * from " + this.f2954a, new String[0]);
        while (a2.moveToNext()) {
            arrayList.add(a(a2));
        }
        a2.close();
        return arrayList;
    }

    public final List b(String str, String[] strArr) {
        ArrayList arrayList = new ArrayList();
        if (this.b == null) {
            this.c.a((Object) "db instance is null");
        } else {
            Cursor a2 = a(str, strArr);
            while (a2.moveToNext()) {
                arrayList.add(a(a2));
            }
            a2.close();
        }
        return arrayList;
    }

    public final void b(Serializable serializable) {
        if (this.b == null) {
            this.c.a((Object) "db instance is null");
            return;
        }
        a("delete from " + this.f2954a + " where " + this.d + "=?", new Object[]{serializable});
    }

    public final void b(String str, Object[] objArr) {
        if (this.b == null) {
            this.c.a((Object) "db instance is null");
            return;
        }
        StringBuilder sb = new StringBuilder("delete from " + this.f2954a + " ");
        sb.append(" where ").append(str);
        a(sb.toString(), objArr);
    }

    public final void b(String[] strArr, Object[] objArr) {
        if (strArr.length != objArr.length) {
            throw new SQLiteException("fields.length != values.length");
        }
        StringBuilder sb = new StringBuilder();
        StringBuilder sb2 = new StringBuilder();
        sb.append("insert into " + this.f2954a + " (");
        for (int i = 0; i < strArr.length; i++) {
            sb.append(strArr[i]);
            sb2.append("?");
            if (i < strArr.length - 1) {
                sb.append(", ");
                sb2.append(",");
            }
        }
        sb.append(") values (");
        sb.append((CharSequence) sb2);
        sb.append(") ");
        a(sb.toString(), objArr);
    }

    public final int c(String[] strArr, String[] strArr2) {
        if (this.b == null) {
            this.c.a((Object) "db instance is null");
            return 0;
        }
        StringBuilder sb = new StringBuilder("select count(*) c from " + this.f2954a + " ");
        int length = strArr.length;
        if (length > 0) {
            sb.append("where ");
        }
        int i = 0;
        while (i < length) {
            sb.append(strArr[i]).append("=? ");
            i++;
            if (i < length) {
                sb.append("and ");
            }
        }
        Cursor a2 = a(sb.toString(), strArr2);
        a2.moveToFirst();
        int i2 = a2.getInt(0);
        a2.close();
        return i2;
    }

    public final String c(String str, String[] strArr, String[] strArr2) {
        String str2 = null;
        if (this.b == null) {
            this.c.a((Object) "db instance is null");
        } else {
            StringBuilder sb = new StringBuilder("select " + str + " from " + this.f2954a + " ");
            int length = strArr.length;
            if (length > 0) {
                sb.append("where ");
            }
            int i = 0;
            while (i < length) {
                sb.append(strArr[i]).append("=? ");
                i++;
                if (i < length) {
                    sb.append("and ");
                }
            }
            Cursor a2 = a(sb.toString(), strArr2);
            if (a2.moveToNext()) {
                str2 = a2.getString(0);
            }
            a2.close();
        }
        return str2;
    }

    public final List c(String str, String[] strArr) {
        return b("select * from " + this.f2954a + " where " + str, strArr);
    }

    public final void c() {
        if (this.b == null) {
            this.c.a((Object) "db instance is null");
        } else {
            b("delete from " + this.f2954a);
        }
    }

    public final void c(String str, Object[] objArr) {
        if (this.b == null) {
            this.c.a((Object) "db instance is null");
            return;
        }
        StringBuilder sb = new StringBuilder("delete from " + this.f2954a + " ");
        sb.append(" where ").append(str).append(" in (").append(a.a(objArr, "'", ",")).append(")");
        b(sb.toString());
    }

    public final boolean c(Serializable serializable) {
        return a(this.d, serializable.toString());
    }

    public final void d() {
        this.b.beginTransaction();
    }

    public final void e() {
        this.b.endTransaction();
    }

    public final void f() {
        this.b.setTransactionSuccessful();
    }
}
