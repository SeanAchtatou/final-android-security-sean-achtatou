package com.immomo.momo.service;

import android.database.sqlite.SQLiteDatabase;
import android.support.v4.b.a;
import com.immomo.momo.g;
import com.immomo.momo.service.a.ab;
import com.immomo.momo.service.a.aj;
import com.immomo.momo.service.a.w;
import com.immomo.momo.service.bean.Message;
import com.immomo.momo.service.bean.a.j;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.util.u;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class y extends b {
    private w c;
    private ab d;
    private aq e;
    private aj f;

    public y() {
        this(g.d().e());
    }

    public y(SQLiteDatabase sQLiteDatabase) {
        this.c = null;
        this.d = null;
        this.e = null;
        this.f = null;
        this.f2968a = sQLiteDatabase;
        this.c = new w(sQLiteDatabase);
        this.d = new ab(sQLiteDatabase);
        this.e = new aq();
        this.f = new aj(sQLiteDatabase);
    }

    private int m(String str) {
        String c2 = this.c.c("field19", new String[]{Message.DBFIELD_SAYHI}, new String[]{str});
        if (!a.a((CharSequence) c2)) {
            try {
                return Integer.parseInt(c2);
            } catch (Exception e2) {
            }
        }
        return -1;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.immomo.momo.service.a.ab.a(com.immomo.momo.service.bean.a.j, android.database.Cursor):void
      com.immomo.momo.service.a.ab.a(java.lang.Object, android.database.Cursor):void
      com.immomo.momo.service.a.b.a(android.database.Cursor, java.lang.String):int
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.String[]):android.database.Cursor
      com.immomo.momo.service.a.b.a(java.lang.String[], java.lang.String[]):java.util.List
      com.immomo.momo.service.a.b.a(java.lang.Object, android.database.Cursor):void
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object[]):void
      com.immomo.momo.service.a.b.a(java.lang.String[], java.lang.Object[]):void
      com.immomo.momo.service.a.b.a(java.lang.Object, java.io.Serializable):boolean
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.String):boolean
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object):void */
    private void n(String str) {
        this.d.a(Message.DBFIELD_GROUPID, (Object) str);
    }

    private boolean o(String str) {
        return this.c.c(str);
    }

    public final j a(String str, String str2, boolean z) {
        j jVar = (j) this.d.b(new String[]{Message.DBFIELD_GROUPID, Message.DBFIELD_SAYHI}, new String[]{str, str2});
        if (z && jVar != null) {
            jVar.h = this.e.c(jVar.f2979a);
        }
        return jVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object[], java.lang.String):java.util.List
     arg types: [java.lang.String, java.lang.String[], ?[OBJECT, ARRAY]]
     candidates:
      com.immomo.momo.service.a.w.a(java.lang.String, com.immomo.momo.service.bean.a.a, boolean):void
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object, java.io.Serializable):void
      com.immomo.momo.service.a.b.a(java.util.Map, java.lang.String[], java.lang.Object[]):void
      com.immomo.momo.service.a.b.a(java.lang.String[], java.lang.Object[], java.io.Serializable):void
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.String[], java.lang.String[]):java.lang.String[]
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object[], java.lang.String):java.util.List */
    public final List a(String[] strArr) {
        return this.c.a(Message.DBFIELD_SAYHI, (Object[]) strArr, (String) null);
    }

    public final void a(int i, String str, String str2) {
        this.d.a(new String[]{"field6"}, new Object[]{Integer.valueOf(i)}, new String[]{Message.DBFIELD_GROUPID, Message.DBFIELD_SAYHI}, new String[]{str, str2});
        this.c.a("field37", Integer.valueOf(i), str);
    }

    public final void a(com.immomo.momo.service.bean.a.a aVar) {
        if (!o(aVar.b)) {
            this.c.a(aVar);
            return;
        }
        this.c.a(new String[]{Message.DBFIELD_LOCATIONJSON, "field5", "field20", "field21", "field22"}, new Object[]{aVar.c, a.a(aVar.F, ","), aVar.J, aVar.K, Integer.valueOf(aVar.L)}, new String[]{Message.DBFIELD_SAYHI}, new String[]{aVar.b});
    }

    public final void a(com.immomo.momo.service.bean.a.a aVar, String str) {
        this.c.a(aVar, str);
    }

    public final void a(com.immomo.momo.service.bean.a.a aVar, boolean z) {
        if (!o(aVar.b)) {
            this.c.a(aVar);
        } else {
            this.c.b(aVar);
        }
        if (z) {
            try {
                this.f2968a.beginTransaction();
                n(aVar.b);
                if (aVar.H != null) {
                    for (j a2 : aVar.H) {
                        this.d.a(a2);
                    }
                }
                this.f2968a.setTransactionSuccessful();
            } catch (Exception e2) {
                this.b.a((Throwable) e2);
            } finally {
                this.f2968a.endTransaction();
            }
        }
    }

    public final void a(String str, int i) {
        this.c.a("field19", Integer.valueOf(i), str);
    }

    public final void a(String str, String str2) {
        this.c.a("field36", str, str2);
    }

    public final void a(String str, String str2, int i) {
        com.immomo.momo.service.bean.a.a e2;
        if (str.equals(g.q().h) && !this.f.c(str2)) {
            if (!this.f.c(str2)) {
                this.f.a(str2);
            }
            if (u.c("groupminelist") && (e2 = e(str2)) != null) {
                List list = (List) u.b("groupminelist");
                if (!list.contains(e2)) {
                    list.add(0, e2);
                    u.a("groupminelist", list);
                }
            }
        }
        if (!c(str, str2)) {
            j jVar = new j();
            jVar.b = str2;
            jVar.c = new Date();
            jVar.g = i;
            jVar.f2979a = str;
            this.d.a(jVar);
            return;
        }
        j jVar2 = new j();
        jVar2.b = str2;
        jVar2.c = new Date();
        jVar2.g = i;
        jVar2.f2979a = str;
        this.d.b(jVar2);
    }

    public final void a(String str, List list) {
        u.a("grouptempuserlist" + str, list);
        JSONArray jSONArray = new JSONArray();
        BufferedWriter bufferedWriter = null;
        try {
            Iterator it = list.iterator();
            while (it.hasNext()) {
                bf bfVar = (bf) it.next();
                JSONObject jSONObject = new JSONObject();
                try {
                    jSONObject.put("momoid", bfVar.h);
                    jSONObject.put("name", bfVar.i);
                    jSONObject.put("avatar", bfVar.ae[0]);
                    jSONObject.put("distance", (double) bfVar.d());
                    jSONObject.put("loc_timesec", bfVar.a() / 1000);
                    jSONArray.put(jSONObject);
                } catch (JSONException e2) {
                    this.b.a((Throwable) e2);
                }
            }
            File file = new File(com.immomo.momo.a.s(), "grouptempuserlist" + str);
            if (!file.exists()) {
                file.createNewFile();
            }
            BufferedWriter bufferedWriter2 = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file)));
            try {
                bufferedWriter2.write(jSONArray.toString());
                bufferedWriter2.flush();
                a.a(bufferedWriter2);
            } catch (IOException e3) {
                e = e3;
                bufferedWriter = bufferedWriter2;
                try {
                    this.b.a((Throwable) e);
                    a.a(bufferedWriter);
                } catch (Throwable th) {
                    th = th;
                    a.a(bufferedWriter);
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                bufferedWriter = bufferedWriter2;
                a.a(bufferedWriter);
                throw th;
            }
        } catch (IOException e4) {
            e = e4;
        }
    }

    public final void a(String str, boolean z) {
        this.c.a("field38", Boolean.valueOf(z), str);
    }

    public final void a(String str, boolean z, String str2) {
        w wVar = this.c;
        String[] strArr = {"field39", "field40"};
        String[] strArr2 = new String[2];
        strArr2[0] = z ? "1" : "0";
        strArr2[1] = str2;
        wVar.a(strArr, strArr2, str);
    }

    public final void a(String str, String[] strArr) {
        this.c.a("field14", a.a(strArr, ","), str);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.service.y.a(com.immomo.momo.service.bean.a.a, boolean):void
     arg types: [com.immomo.momo.service.bean.a.a, int]
     candidates:
      com.immomo.momo.service.y.a(com.immomo.momo.service.bean.a.a, java.lang.String):void
      com.immomo.momo.service.y.a(java.lang.String, int):void
      com.immomo.momo.service.y.a(java.lang.String, java.lang.String):void
      com.immomo.momo.service.y.a(java.lang.String, java.util.List):void
      com.immomo.momo.service.y.a(java.lang.String, boolean):void
      com.immomo.momo.service.y.a(java.lang.String, java.lang.String[]):void
      com.immomo.momo.service.y.a(java.util.List, java.lang.String):void
      com.immomo.momo.service.y.a(com.immomo.momo.service.bean.a.a, boolean):void */
    public final void a(List list) {
        try {
            this.f2968a.beginTransaction();
            Iterator it = list.iterator();
            while (it.hasNext()) {
                a((com.immomo.momo.service.bean.a.a) it.next(), false);
            }
            this.f2968a.setTransactionSuccessful();
        } catch (Exception e2) {
            this.b.a((Throwable) e2);
        } finally {
            this.f2968a.endTransaction();
        }
    }

    public final void a(List list, String str) {
        this.f2968a.beginTransaction();
        try {
            n(str);
            Iterator it = list.iterator();
            while (it.hasNext()) {
                j jVar = (j) it.next();
                jVar.b = str;
                this.d.a(jVar);
                if (jVar.h != null) {
                    this.e.d(jVar.h);
                }
            }
            this.f2968a.setTransactionSuccessful();
        } catch (Exception e2) {
            this.b.a((Throwable) e2);
        } finally {
            this.f2968a.endTransaction();
        }
    }

    public final boolean a(String str) {
        return m(str) == 1;
    }

    /* JADX WARNING: Unknown top exception splitter block from list: {B:22:0x0099=Splitter:B:22:0x0099, B:18:0x008f=Splitter:B:18:0x008f} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.List b(java.lang.String r7) {
        /*
            r6 = this;
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r1 = "tiebamytiebas"
            r0.<init>(r1)
            java.lang.StringBuilder r0 = r0.append(r7)
            java.lang.String r0 = r0.toString()
            boolean r0 = com.immomo.momo.util.u.c(r0)
            if (r0 == 0) goto L_0x002b
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r1 = "grouptempuserlist"
            r0.<init>(r1)
            java.lang.StringBuilder r0 = r0.append(r7)
            java.lang.String r0 = r0.toString()
            java.lang.Object r0 = com.immomo.momo.util.u.b(r0)
            java.util.List r0 = (java.util.List) r0
        L_0x002a:
            return r0
        L_0x002b:
            java.util.ArrayList r2 = new java.util.ArrayList
            r2.<init>()
            java.io.File r3 = new java.io.File
            java.io.File r0 = com.immomo.momo.a.s()
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r4 = "grouptempuserlist"
            r1.<init>(r4)
            java.lang.StringBuilder r1 = r1.append(r7)
            java.lang.String r1 = r1.toString()
            r3.<init>(r0, r1)
            r1 = 0
            boolean r0 = r3.exists()     // Catch:{ IOException -> 0x008e, JSONException -> 0x0098 }
            if (r0 == 0) goto L_0x00b6
            java.io.FileInputStream r0 = new java.io.FileInputStream     // Catch:{ IOException -> 0x008e, JSONException -> 0x0098 }
            r0.<init>(r3)     // Catch:{ IOException -> 0x008e, JSONException -> 0x0098 }
            byte[] r1 = android.support.v4.b.a.b(r0)     // Catch:{ IOException -> 0x00b1, JSONException -> 0x00ac, all -> 0x00a7 }
            org.json.JSONArray r3 = new org.json.JSONArray     // Catch:{ IOException -> 0x00b1, JSONException -> 0x00ac, all -> 0x00a7 }
            java.lang.String r4 = new java.lang.String     // Catch:{ IOException -> 0x00b1, JSONException -> 0x00ac, all -> 0x00a7 }
            r4.<init>(r1)     // Catch:{ IOException -> 0x00b1, JSONException -> 0x00ac, all -> 0x00a7 }
            r3.<init>(r4)     // Catch:{ IOException -> 0x00b1, JSONException -> 0x00ac, all -> 0x00a7 }
            r1 = 0
        L_0x0063:
            int r4 = r3.length()     // Catch:{ IOException -> 0x00b1, JSONException -> 0x00ac, all -> 0x00a7 }
            if (r1 < r4) goto L_0x0080
        L_0x0069:
            android.support.v4.b.a.a(r0)
        L_0x006c:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r1 = "grouptempuserlist"
            r0.<init>(r1)
            java.lang.StringBuilder r0 = r0.append(r7)
            java.lang.String r0 = r0.toString()
            com.immomo.momo.util.u.a(r0, r2)
            r0 = r2
            goto L_0x002a
        L_0x0080:
            org.json.JSONObject r4 = r3.getJSONObject(r1)     // Catch:{ IOException -> 0x00b1, JSONException -> 0x00ac, all -> 0x00a7 }
            com.immomo.momo.service.bean.bf r4 = com.immomo.momo.protocol.a.o.a(r4)     // Catch:{ IOException -> 0x00b1, JSONException -> 0x00ac, all -> 0x00a7 }
            r2.add(r4)     // Catch:{ IOException -> 0x00b1, JSONException -> 0x00ac, all -> 0x00a7 }
            int r1 = r1 + 1
            goto L_0x0063
        L_0x008e:
            r0 = move-exception
        L_0x008f:
            com.immomo.momo.util.m r3 = r6.b     // Catch:{ all -> 0x00a2 }
            r3.a(r0)     // Catch:{ all -> 0x00a2 }
            android.support.v4.b.a.a(r1)
            goto L_0x006c
        L_0x0098:
            r0 = move-exception
        L_0x0099:
            com.immomo.momo.util.m r3 = r6.b     // Catch:{ all -> 0x00a2 }
            r3.a(r0)     // Catch:{ all -> 0x00a2 }
            android.support.v4.b.a.a(r1)
            goto L_0x006c
        L_0x00a2:
            r0 = move-exception
        L_0x00a3:
            android.support.v4.b.a.a(r1)
            throw r0
        L_0x00a7:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x00a3
        L_0x00ac:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x0099
        L_0x00b1:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x008f
        L_0x00b6:
            r0 = r1
            goto L_0x0069
        */
        throw new UnsupportedOperationException("Method not decompiled: com.immomo.momo.service.y.b(java.lang.String):java.util.List");
    }

    public final List b(String str, int i) {
        List<j> a2 = this.d.a(new String[]{Message.DBFIELD_GROUPID, "field6"}, new String[]{str, "3"});
        if (a2 != null) {
            for (j jVar : a2) {
                jVar.h = this.e.c(jVar.f2979a);
            }
        }
        Collections.sort(a2, new z(this, i));
        return a2;
    }

    public final void b(String str, String str2) {
        this.d.a(new String[]{Message.DBFIELD_GROUPID, Message.DBFIELD_SAYHI}, new Object[]{str2, str});
        if (str.equals(g.q().h)) {
            h(str2);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.m.a(java.lang.String, java.lang.Throwable):void
     arg types: [java.lang.String, java.lang.Exception]
     candidates:
      com.immomo.momo.util.m.a(java.lang.Appendable, java.lang.Throwable):void
      com.immomo.momo.util.m.a(java.lang.Object, java.lang.Throwable):void
      com.immomo.momo.util.m.a(java.lang.String, java.io.File):void
      com.immomo.momo.util.m.a(java.lang.String, java.lang.Throwable):void */
    public final void b(List list) {
        try {
            this.f2968a.beginTransaction();
            this.f.c();
            Iterator it = list.iterator();
            while (it.hasNext()) {
                this.f.a(((com.immomo.momo.service.bean.a.a) it.next()).b);
            }
            u.a("groupminelist", list);
            this.f2968a.setTransactionSuccessful();
        } catch (Exception e2) {
            this.b.a("saveMyGroups failed", (Throwable) e2);
        } finally {
            this.f2968a.endTransaction();
        }
    }

    public final List c() {
        if (u.c("groupminelist")) {
            return (List) u.b("groupminelist");
        }
        List b = this.c.b("select g.*, mg.mg_gid from mygroups as mg JOIN " + "groups as g" + " on mg.mg_gid" + " = g.field1", new String[0]);
        u.a("groupminelist", b);
        return b;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.immomo.momo.service.a.ab.a(com.immomo.momo.service.bean.a.j, android.database.Cursor):void
      com.immomo.momo.service.a.ab.a(java.lang.Object, android.database.Cursor):void
      com.immomo.momo.service.a.b.a(android.database.Cursor, java.lang.String):int
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.String[]):android.database.Cursor
      com.immomo.momo.service.a.b.a(java.lang.String[], java.lang.String[]):java.util.List
      com.immomo.momo.service.a.b.a(java.lang.Object, android.database.Cursor):void
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object[]):void
      com.immomo.momo.service.a.b.a(java.lang.String[], java.lang.Object[]):void
      com.immomo.momo.service.a.b.a(java.lang.Object, java.io.Serializable):boolean
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.String):boolean
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object):void */
    public final void c(String str) {
        this.d.a(Message.DBFIELD_SAYHI, (Object) str);
    }

    public final void c(String str, int i) {
        this.c.a("field13", Integer.valueOf(i), str);
    }

    public final boolean c(String str, String str2) {
        return this.d.c(new String[]{Message.DBFIELD_GROUPID, Message.DBFIELD_SAYHI}, new String[]{str2, str}) > 0;
    }

    public final int d(String str, String str2) {
        try {
            return Integer.parseInt(this.d.c("field6", new String[]{Message.DBFIELD_GROUPID, Message.DBFIELD_SAYHI}, new String[]{str, str2}));
        } catch (Exception e2) {
            return 0;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object[], java.lang.String):java.util.List
     arg types: [java.lang.String, java.lang.String[], ?[OBJECT, ARRAY]]
     candidates:
      com.immomo.momo.service.a.w.a(java.lang.String, com.immomo.momo.service.bean.a.a, boolean):void
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object, java.io.Serializable):void
      com.immomo.momo.service.a.b.a(java.util.Map, java.lang.String[], java.lang.Object[]):void
      com.immomo.momo.service.a.b.a(java.lang.String[], java.lang.Object[], java.io.Serializable):void
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.String[], java.lang.String[]):java.lang.String[]
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object[], java.lang.String):java.util.List */
    public final List d(String str) {
        String[] a2 = this.d.a(Message.DBFIELD_GROUPID, new String[]{Message.DBFIELD_SAYHI}, new String[]{str});
        return (a2 == null || a2.length <= 0) ? new ArrayList() : this.c.a(Message.DBFIELD_SAYHI, (Object[]) a2, (String) null);
    }

    public final com.immomo.momo.service.bean.a.a e(String str) {
        com.immomo.momo.service.bean.a.a aVar = (com.immomo.momo.service.bean.a.a) this.c.a((Serializable) str);
        if (aVar != null) {
            aVar.H = this.d.a(new String[]{Message.DBFIELD_GROUPID}, new String[]{str});
        }
        return (com.immomo.momo.service.bean.a.a) this.c.a((Serializable) str);
    }

    public final String f(String str) {
        return this.c.c(Message.DBFIELD_LOCATIONJSON, new String[]{Message.DBFIELD_SAYHI}, new String[]{str});
    }

    public final List g(String str) {
        List<j> a2 = this.d.a(new String[]{Message.DBFIELD_GROUPID, "field6"}, new String[]{str, "2"});
        if (a2 != null) {
            for (j jVar : a2) {
                jVar.h = this.e.c(jVar.f2979a);
            }
        }
        return a2;
    }

    public final void h(String str) {
        this.f.b((Serializable) str);
        if (u.c("groupminelist")) {
            List list = (List) u.b("groupminelist");
            list.remove(new com.immomo.momo.service.bean.a.a(str));
            u.a("groupminelist", list);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object, java.io.Serializable):void
     arg types: [java.lang.String, boolean, java.lang.String]
     candidates:
      com.immomo.momo.service.a.w.a(java.lang.String, com.immomo.momo.service.bean.a.a, boolean):void
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object[], java.lang.String):java.util.List
      com.immomo.momo.service.a.b.a(java.util.Map, java.lang.String[], java.lang.Object[]):void
      com.immomo.momo.service.a.b.a(java.lang.String[], java.lang.Object[], java.io.Serializable):void
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.String[], java.lang.String[]):java.lang.String[]
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object, java.io.Serializable):void */
    public final void i(String str) {
        this.c.a("field34", (Object) false, (Serializable) str);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object, java.io.Serializable):void
     arg types: [java.lang.String, boolean, java.lang.String]
     candidates:
      com.immomo.momo.service.a.w.a(java.lang.String, com.immomo.momo.service.bean.a.a, boolean):void
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object[], java.lang.String):java.util.List
      com.immomo.momo.service.a.b.a(java.util.Map, java.lang.String[], java.lang.Object[]):void
      com.immomo.momo.service.a.b.a(java.lang.String[], java.lang.Object[], java.io.Serializable):void
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.String[], java.lang.String[]):java.lang.String[]
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object, java.io.Serializable):void */
    public final void j(String str) {
        this.c.a("field35", (Object) false, (Serializable) str);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object, java.io.Serializable):void
     arg types: [java.lang.String, int, java.lang.String]
     candidates:
      com.immomo.momo.service.a.w.a(java.lang.String, com.immomo.momo.service.bean.a.a, boolean):void
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object[], java.lang.String):java.util.List
      com.immomo.momo.service.a.b.a(java.util.Map, java.lang.String[], java.lang.Object[]):void
      com.immomo.momo.service.a.b.a(java.lang.String[], java.lang.Object[], java.io.Serializable):void
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.String[], java.lang.String[]):java.lang.String[]
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object, java.io.Serializable):void */
    public final void k(String str) {
        this.c.a("field17", (Object) 1, (Serializable) str);
    }

    public final List l(String str) {
        return this.c.a(new String[]{"field20"}, new String[]{str}, null, false, 0, 30);
    }
}
