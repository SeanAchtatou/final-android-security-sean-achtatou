package com.immomo.momo.service.bean;

import android.support.v4.b.a;
import com.amap.mapapi.poisearch.PoiTypeDef;
import java.util.Date;
import java.util.List;

public final class n extends al {

    /* renamed from: a  reason: collision with root package name */
    public String[] f3032a;
    public String b;
    public String c;
    public List d = null;
    public String[] e = null;
    public String f;
    public int g = 1;
    public int h;
    public Date i;
    public int j = 0;
    public int k = 0;
    public String l;
    public String m = null;

    public n() {
    }

    public n(String str) {
        this.f = str;
    }

    public final String a() {
        return a.f(this.b) ? this.b : this.f;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        n nVar = (n) obj;
        return this.f == null ? nVar.f == null : this.f.equals(nVar.f);
    }

    public final String getLoadImageId() {
        return (this.f3032a == null || this.f3032a.length <= 0) ? PoiTypeDef.All : this.f3032a[0];
    }

    public final int hashCode() {
        return (this.f == null ? 0 : this.f.hashCode()) + 31;
    }
}
