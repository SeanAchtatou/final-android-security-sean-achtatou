package com.immomo.momo.service;

import com.immomo.momo.service.bean.ab;
import java.util.Comparator;
import java.util.List;

final class aj implements Comparator {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ List f2960a;

    aj(List list) {
        this.f2960a = list;
    }

    public final /* synthetic */ int compare(Object obj, Object obj2) {
        return this.f2960a.indexOf(((ab) obj).h) > this.f2960a.indexOf(((ab) obj2).h) ? 1 : -1;
    }
}
