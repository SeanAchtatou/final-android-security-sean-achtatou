package com.immomo.momo.service.a;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.b.a;
import com.immomo.momo.service.bean.Message;
import com.immomo.momo.service.bean.c.b;
import java.util.HashMap;
import java.util.Map;

public final class aw extends b {
    public aw(SQLiteDatabase sQLiteDatabase) {
        super(sQLiteDatabase, "tie", Message.DBFIELD_ID);
    }

    private static void a(b bVar, Cursor cursor) {
        bVar.n = a(cursor, "field10");
        bVar.i = d(cursor, Message.DBFIELD_GROUPID);
        bVar.f3017a = c(cursor, Message.DBFIELD_ID);
        bVar.a(a.b(c(cursor, Message.DBFIELD_CONVERLOCATIONJSON), ","));
        bVar.l = e(cursor, "field8");
        bVar.f = c(cursor, Message.DBFIELD_LOCATIONJSON);
        bVar.k = e(cursor, "field7");
        bVar.o = a(cursor, "field11");
        bVar.g = c(cursor, "field6");
        bVar.c = c(cursor, "field5");
        bVar.d = c(cursor, Message.DBFIELD_SAYHI);
        bVar.m = e(cursor, "field9");
        bVar.q = e(cursor, "field12");
        bVar.r = c(cursor, "field13");
        bVar.h = c(cursor, "field19");
        bVar.s = c(cursor, "field14");
        bVar.u = c(cursor, "field16");
        bVar.t = c(cursor, "field15");
        bVar.j = d(cursor, "field17");
        e(cursor, "field18");
        bVar.p = e(cursor, "field20");
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Cursor cursor) {
        b bVar = new b();
        a(bVar, cursor);
        return bVar;
    }

    public final void a(b bVar) {
        HashMap hashMap = new HashMap();
        hashMap.put(Message.DBFIELD_ID, bVar.f3017a);
        hashMap.put("field10", Integer.valueOf(bVar.n));
        hashMap.put("field6", bVar.g);
        hashMap.put(Message.DBFIELD_CONVERLOCATIONJSON, a.a(bVar.c(), ","));
        hashMap.put("field8", Boolean.valueOf(bVar.l));
        hashMap.put(Message.DBFIELD_LOCATIONJSON, bVar.f);
        hashMap.put("field7", Boolean.valueOf(bVar.k));
        hashMap.put("field5", bVar.c);
        hashMap.put(Message.DBFIELD_GROUPID, bVar.i);
        hashMap.put(Message.DBFIELD_SAYHI, bVar.d);
        hashMap.put("field9", Boolean.valueOf(bVar.m));
        hashMap.put("field11", Integer.valueOf(bVar.o));
        hashMap.put("field12", Boolean.valueOf(bVar.q));
        hashMap.put("field13", bVar.r);
        hashMap.put("field19", bVar.h);
        hashMap.put("field14", bVar.s);
        hashMap.put("field15", bVar.t);
        hashMap.put("field16", bVar.u);
        hashMap.put("field17", bVar.j);
        hashMap.put("field20", Boolean.valueOf(bVar.p));
        a((Map) hashMap);
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void a(Object obj, Cursor cursor) {
        a((b) obj, cursor);
    }

    public final void b(b bVar) {
        HashMap hashMap = new HashMap();
        hashMap.put("field10", Integer.valueOf(bVar.n));
        hashMap.put("field6", bVar.g);
        hashMap.put(Message.DBFIELD_CONVERLOCATIONJSON, a.a(bVar.c(), ","));
        hashMap.put("field8", Boolean.valueOf(bVar.l));
        hashMap.put(Message.DBFIELD_LOCATIONJSON, bVar.f);
        hashMap.put("field7", Boolean.valueOf(bVar.k));
        hashMap.put("field5", bVar.c);
        hashMap.put(Message.DBFIELD_GROUPID, bVar.i);
        hashMap.put(Message.DBFIELD_SAYHI, bVar.d);
        hashMap.put("field9", Boolean.valueOf(bVar.m));
        hashMap.put("field11", Integer.valueOf(bVar.o));
        hashMap.put("field12", Boolean.valueOf(bVar.q));
        hashMap.put("field13", bVar.r);
        hashMap.put("field19", bVar.h);
        hashMap.put("field14", bVar.s);
        hashMap.put("field15", bVar.t);
        hashMap.put("field16", bVar.u);
        hashMap.put("field17", bVar.j);
        hashMap.put("field20", Boolean.valueOf(bVar.p));
        a(hashMap, new String[]{Message.DBFIELD_ID}, new String[]{bVar.f3017a});
    }
}
