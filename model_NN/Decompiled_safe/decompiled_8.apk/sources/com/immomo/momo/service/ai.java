package com.immomo.momo.service;

import android.support.v4.b.a;
import com.immomo.momo.g;
import com.immomo.momo.service.a.ah;
import com.immomo.momo.service.a.al;
import com.immomo.momo.service.a.au;
import com.immomo.momo.service.a.o;
import com.immomo.momo.service.a.p;
import com.immomo.momo.service.a.t;
import com.immomo.momo.service.bean.Message;
import com.immomo.momo.service.bean.ab;
import com.immomo.momo.service.bean.ae;
import com.immomo.momo.util.u;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public final class ai extends b {
    private p c = null;
    private al d = null;
    private au e = null;
    private am f = null;
    private aq g = null;
    private t h = null;
    private ah i = null;

    public ai() {
        this.f2968a = g.d().h();
        this.c = new p(this.f2968a);
        this.e = new au(this.f2968a);
        this.g = new aq();
        this.f = new am();
        this.d = new al(g.d().e());
        this.h = new t(g.d().e());
        this.i = new ah(g.d().e());
        new o(g.d().h());
    }

    private boolean h(String str) {
        return this.c.c(str);
    }

    public final ab a(String str) {
        ab abVar = (ab) this.c.a((Serializable) str);
        if (abVar != null) {
            abVar.b = this.g.b(abVar.c);
            abVar.d = this.f.b(abVar.e);
        }
        return abVar;
    }

    public final List a(String str, int i2) {
        List<ab> a2 = this.c.a(new String[]{Message.DBFIELD_SAYHI, "field8"}, new String[]{str, "1"}, Message.DBFIELD_GROUPID, false, 0, i2);
        for (ab abVar : a2) {
            abVar.b = this.g.b(str);
            abVar.d = this.f.b(abVar.e);
        }
        return a2;
    }

    public final void a(ab abVar) {
        if (h(abVar.h)) {
            this.c.b(abVar);
        } else {
            this.c.a(abVar);
        }
    }

    public final void a(ae aeVar) {
        if (a.a((CharSequence) aeVar.k)) {
            throw new RuntimeException("comment.id is null");
        } else if (!this.e.c(aeVar.k)) {
            this.e.a(aeVar);
        } else {
            this.e.b(aeVar);
        }
    }

    public final void a(List list) {
        this.f2968a.beginTransaction();
        this.g.b().beginTransaction();
        try {
            Iterator it = list.iterator();
            while (it.hasNext()) {
                b((ab) it.next());
            }
            this.f2968a.setTransactionSuccessful();
            this.g.b().setTransactionSuccessful();
        } catch (Exception e2) {
            this.b.a((Throwable) e2);
        } finally {
            this.f2968a.endTransaction();
            this.g.b().endTransaction();
        }
    }

    public final ab b(String str) {
        List a2 = a(str, 1);
        if (a2 == null || a2.size() <= 0) {
            return null;
        }
        return (ab) a2.get(0);
    }

    public final void b(ab abVar) {
        a(abVar);
        if (abVar.d != null) {
            this.f.b(abVar.d);
        }
        if (abVar.b != null) {
            this.g.d(abVar.b);
        }
    }

    public final void b(List list) {
        this.f2968a.beginTransaction();
        this.g.b().beginTransaction();
        try {
            this.h.c();
            Iterator it = list.iterator();
            while (it.hasNext()) {
                ab abVar = (ab) it.next();
                this.h.a(abVar.h);
                b(abVar);
            }
            this.f2968a.setTransactionSuccessful();
            this.g.b().setTransactionSuccessful();
            u.a("feedmyfriendlist", list);
        } catch (Exception e2) {
            this.b.a((Throwable) e2);
        } finally {
            this.g.b().endTransaction();
            this.f2968a.endTransaction();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.service.a.b.a(java.lang.String[], java.lang.String[], java.lang.String, boolean):java.util.List
     arg types: [java.lang.String[], java.lang.String[], java.lang.String, int]
     candidates:
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.String[], java.lang.String[], java.lang.String[]):int
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.String, java.lang.String[], java.lang.String[]):java.lang.String
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object, java.lang.String, java.lang.Object[]):void
      com.immomo.momo.service.a.b.a(java.lang.String[], java.lang.Object[], java.lang.String[], java.lang.Object[]):void
      com.immomo.momo.service.a.b.a(java.lang.String[], java.lang.String[], java.lang.String, boolean):java.util.List */
    public final List c() {
        if (u.c("feedmyfriendlist")) {
            return (List) u.b("feedmyfriendlist");
        }
        List a2 = this.h.a(new String[0], new String[0], Message.DBFIELD_ID, true);
        List<ab> a3 = this.c.a("sf_id", a2.toArray(), (String) null);
        for (ab abVar : a3) {
            abVar.b = this.g.b(abVar.c);
            abVar.d = this.f.b(abVar.e);
        }
        Collections.sort(a3, new aj(a2));
        u.a("feedmyfriendlist", a3);
        return a3;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.service.a.b.a(java.lang.String[], java.lang.String[], java.lang.String, boolean):java.util.List
     arg types: [java.lang.String[], java.lang.String[], java.lang.String, int]
     candidates:
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.String[], java.lang.String[], java.lang.String[]):int
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.String, java.lang.String[], java.lang.String[]):java.lang.String
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object, java.lang.String, java.lang.Object[]):void
      com.immomo.momo.service.a.b.a(java.lang.String[], java.lang.Object[], java.lang.String[], java.lang.Object[]):void
      com.immomo.momo.service.a.b.a(java.lang.String[], java.lang.String[], java.lang.String, boolean):java.util.List */
    public final List c(String str) {
        List<ae> a2 = this.e.a(new String[]{"field5"}, new String[]{str}, Message.DBFIELD_CONVERLOCATIONJSON, false);
        for (ae aeVar : a2) {
            aeVar.f2985a = this.g.b(aeVar.b);
        }
        return a2;
    }

    public final void c(List list) {
        this.f2968a.beginTransaction();
        this.g.b().beginTransaction();
        try {
            this.i.c();
            String[] strArr = new String[2];
            Iterator it = list.iterator();
            while (it.hasNext()) {
                ae aeVar = (ae) it.next();
                a(aeVar);
                strArr[0] = aeVar.k;
                strArr[1] = aeVar.j;
                this.i.a(strArr);
                if (aeVar.g != null) {
                    if (h(aeVar.g.h)) {
                        this.c.a(new String[]{"field5", "field8", "field7"}, new Object[]{a.a(aeVar.g.j(), ","), Integer.valueOf(aeVar.g.i), aeVar.g.b()}, new String[]{"sf_id"}, new String[]{aeVar.g.h});
                    } else {
                        this.c.a(aeVar.g);
                    }
                    if (aeVar.g.d != null) {
                        this.f.b(aeVar.g.d);
                    }
                }
                if (aeVar.f2985a != null) {
                    this.g.g(aeVar.f2985a);
                }
            }
            this.f2968a.setTransactionSuccessful();
            this.g.b().setTransactionSuccessful();
            u.a("feedmycomments", list);
        } catch (Exception e2) {
            this.b.a((Throwable) e2);
        } finally {
            this.g.b().endTransaction();
            this.f2968a.endTransaction();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.service.a.b.a(java.lang.String[], java.lang.String[], java.lang.String, boolean):java.util.List
     arg types: [java.lang.String[], java.lang.String[], java.lang.String, int]
     candidates:
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.String[], java.lang.String[], java.lang.String[]):int
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.String, java.lang.String[], java.lang.String[]):java.lang.String
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object, java.lang.String, java.lang.Object[]):void
      com.immomo.momo.service.a.b.a(java.lang.String[], java.lang.Object[], java.lang.String[], java.lang.Object[]):void
      com.immomo.momo.service.a.b.a(java.lang.String[], java.lang.String[], java.lang.String, boolean):java.util.List */
    public final List d() {
        if (u.c("feedmycomments")) {
            return (List) u.b("feedmycomments");
        }
        List a2 = this.i.a(new String[0], new String[0], Message.DBFIELD_ID, true);
        ArrayList arrayList = new ArrayList(a2.size());
        for (int i2 = 0; i2 < a2.size(); i2++) {
            arrayList.add(((String[]) a2.get(i2))[0]);
        }
        List<ae> a3 = this.e.a("c_id", arrayList.toArray(), (String) null);
        for (ae aeVar : a3) {
            aeVar.g = (ab) this.c.a((Serializable) aeVar.h);
            aeVar.f2985a = this.g.b(aeVar.b);
        }
        if (a3.size() <= 0 || a3.size() >= 2) {
            Collections.sort(a3, new ak(arrayList, a2));
        } else {
            ((ae) a3.get(0)).j = ((String[]) a2.get(0))[1];
        }
        u.a("feedmycomments", a3);
        return a3;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.immomo.momo.service.a.au.a(com.immomo.momo.service.bean.ae, android.database.Cursor):void
      com.immomo.momo.service.a.au.a(java.lang.Object, android.database.Cursor):void
      com.immomo.momo.service.a.b.a(android.database.Cursor, java.lang.String):int
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.String[]):android.database.Cursor
      com.immomo.momo.service.a.b.a(java.lang.String[], java.lang.String[]):java.util.List
      com.immomo.momo.service.a.b.a(java.lang.Object, android.database.Cursor):void
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object[]):void
      com.immomo.momo.service.a.b.a(java.lang.String[], java.lang.Object[]):void
      com.immomo.momo.service.a.b.a(java.lang.Object, java.io.Serializable):boolean
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.String):boolean
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object):void */
    public final void d(String str) {
        this.e.a("field5", (Object) str);
    }

    public final void d(List list) {
        try {
            this.f2968a.beginTransaction();
            Iterator it = list.iterator();
            while (it.hasNext()) {
                ae aeVar = (ae) it.next();
                if (a.a((CharSequence) aeVar.k)) {
                    throw new RuntimeException("comment.id is null");
                }
                a(aeVar);
                if (aeVar.f2985a != null) {
                    this.g.g(aeVar.f2985a);
                }
            }
            this.f2968a.setTransactionSuccessful();
        } catch (Exception e2) {
            this.b.a((Throwable) e2);
        } finally {
            this.f2968a.endTransaction();
        }
    }

    public final List e() {
        if (u.c("nearbyfeed_cache")) {
            List<ab> list = (List) u.b("nearbyfeed_cache");
            for (ab abVar : list) {
                abVar.a(abVar.d());
            }
            return list;
        }
        List b = this.d.b();
        List<ab> a2 = this.c.a("sf_id", b.toArray(), (String) null);
        for (ab abVar2 : a2) {
            abVar2.b = this.g.b(abVar2.c);
            abVar2.d = this.f.b(abVar2.e);
        }
        Collections.sort(a2, new al(b));
        u.a("nearbyfeed_cache", a2);
        return a2;
    }

    public final void e(String str) {
        this.e.b((Serializable) str);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.m.a(java.lang.String, java.lang.Throwable):void
     arg types: [java.lang.String, java.lang.Exception]
     candidates:
      com.immomo.momo.util.m.a(java.lang.Appendable, java.lang.Throwable):void
      com.immomo.momo.util.m.a(java.lang.Object, java.lang.Throwable):void
      com.immomo.momo.util.m.a(java.lang.String, java.io.File):void
      com.immomo.momo.util.m.a(java.lang.String, java.lang.Throwable):void */
    public final void e(List list) {
        try {
            this.f2968a.beginTransaction();
            g.d().e().beginTransaction();
            this.d.c();
            Iterator it = list.iterator();
            while (it.hasNext()) {
                ab abVar = (ab) it.next();
                this.d.a(abVar.h);
                b(abVar);
            }
            u.a("nearbyfeed_cache", list);
            g.d().e().setTransactionSuccessful();
            this.f2968a.setTransactionSuccessful();
        } catch (Exception e2) {
            this.b.a("addNearUsers failed", (Throwable) e2);
        } finally {
            this.f2968a.endTransaction();
            g.d().e().endTransaction();
        }
    }

    public final void f(String str) {
        this.c.b((Serializable) str);
        if (u.c("feedmyfriendlist")) {
            List list = (List) u.b("feedmyfriendlist");
            Iterator it = list.iterator();
            while (true) {
                if (it.hasNext()) {
                    ab abVar = (ab) it.next();
                    if (str.equals(abVar.h)) {
                        list.remove(abVar);
                        break;
                    }
                } else {
                    break;
                }
            }
            u.a("feedmyfriendlist", list);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.service.a.b.a(java.lang.String[], java.lang.Object[]):void
     arg types: [java.lang.String[], java.lang.String[]]
     candidates:
      com.immomo.momo.service.a.p.a(com.immomo.momo.service.bean.ab, android.database.Cursor):void
      com.immomo.momo.service.a.p.a(java.lang.Object, android.database.Cursor):void
      com.immomo.momo.service.a.b.a(android.database.Cursor, java.lang.String):int
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.String[]):android.database.Cursor
      com.immomo.momo.service.a.b.a(java.lang.String[], java.lang.String[]):java.util.List
      com.immomo.momo.service.a.b.a(java.lang.Object, android.database.Cursor):void
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object):void
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object[]):void
      com.immomo.momo.service.a.b.a(java.lang.Object, java.io.Serializable):boolean
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.String):boolean
      com.immomo.momo.service.a.b.a(java.lang.String[], java.lang.Object[]):void */
    public final void g(String str) {
        this.c.a(new String[]{Message.DBFIELD_SAYHI}, (Object[]) new String[]{str});
    }
}
