package com.immomo.momo.service.bean;

import android.support.v4.b.a;
import com.amap.mapapi.poisearch.PoiTypeDef;
import java.util.Date;

public final class ba {

    /* renamed from: a  reason: collision with root package name */
    private String f3006a;
    private String b;
    private String c;
    private Date d;
    private Date e;
    private int f;
    private int g;
    private int h;
    private String i;

    public final String a() {
        return this.f3006a;
    }

    public final void a(int i2) {
        this.f = i2;
    }

    public final void a(String str) {
        this.f3006a = str;
    }

    public final void a(Date date) {
        this.d = date;
    }

    public final int b() {
        return this.g;
    }

    public final void b(int i2) {
        this.g = i2;
    }

    public final void b(String str) {
        this.b = str;
    }

    public final void b(Date date) {
        this.e = date;
    }

    public final int c() {
        return this.h;
    }

    public final void c(int i2) {
        this.h = i2;
    }

    public final void c(String str) {
        this.c = str;
    }

    public final String d() {
        return this.b;
    }

    public final void d(String str) {
        this.i = str;
    }

    public final String e() {
        return this.c;
    }

    public final String f() {
        return this.i;
    }

    public final Date g() {
        return this.d;
    }

    public final Date h() {
        return this.e;
    }

    public final String i() {
        return !a.a(this.f3006a) ? "cp_" + a.n(this.f3006a) : PoiTypeDef.All;
    }

    public final String j() {
        return !a.a(this.b) ? "cp_" + a.n(this.b) : PoiTypeDef.All;
    }

    public final String k() {
        return !a.a(this.c) ? "cp_" + a.n(this.c) : PoiTypeDef.All;
    }

    public final String toString() {
        return "SplashscreenItem [bgurl=" + this.f3006a + ", fturl=" + this.b + ", crurl=" + this.c + ", starttime=" + this.d + ", endtime=" + this.e + ", id=" + this.f + "]";
    }
}
