package com.immomo.momo.service.bean;

import com.amap.mapapi.poisearch.PoiTypeDef;

public final class am {

    /* renamed from: a  reason: collision with root package name */
    public String f2991a = PoiTypeDef.All;
    public String b;
    public String c;
    public String d;

    public am() {
    }

    public am(String str, String str2, String str3, String str4) {
        this.f2991a = str;
        this.b = str2;
        this.c = str3;
        this.d = str4;
    }

    public final String toString() {
        return this.b;
    }
}
