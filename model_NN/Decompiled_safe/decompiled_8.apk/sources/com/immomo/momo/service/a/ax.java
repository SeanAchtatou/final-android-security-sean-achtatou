package com.immomo.momo.service.a;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.b.a;
import com.immomo.momo.service.bean.Message;
import com.immomo.momo.service.bean.c.d;
import com.immomo.momo.service.bean.c.f;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public final class ax extends b {
    public ax(SQLiteDatabase sQLiteDatabase) {
        super(sQLiteDatabase, "tieba", Message.DBFIELD_ID);
    }

    /* access modifiers changed from: private */
    public void a(d dVar, Cursor cursor) {
        dVar.f3019a = c(cursor, Message.DBFIELD_ID);
        dVar.b = c(cursor, Message.DBFIELD_SAYHI);
        String c = c(cursor, "field9");
        if (!a.a((CharSequence) c)) {
            try {
                f fVar = new f();
                fVar.a(new JSONObject(c));
                dVar.k = fVar;
            } catch (JSONException e) {
                this.c.a((Throwable) e);
            }
        }
        dVar.f = d(cursor, Message.DBFIELD_GROUPID);
        dVar.n = e(cursor, "field11");
        dVar.m = e(cursor, "field10");
        dVar.l = e(cursor, "field15");
        dVar.g = a(cursor, "field5");
        dVar.b = c(cursor, Message.DBFIELD_SAYHI);
        dVar.i = a(cursor, "field7");
        dVar.c = c(cursor, Message.DBFIELD_LOCATIONJSON);
        dVar.e = a.b(c(cursor, Message.DBFIELD_CONVERLOCATIONJSON), ",");
        dVar.o = e(cursor, "field12");
        dVar.j = c(cursor, "field8");
        dVar.h = a(cursor, "field6");
        dVar.p = a(cursor, "field13");
        dVar.q = e(cursor, "field16");
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public d a(Cursor cursor) {
        d dVar = new d();
        a(dVar, cursor);
        return dVar;
    }

    public final void a(d dVar) {
        HashMap hashMap = new HashMap();
        hashMap.put(Message.DBFIELD_ID, dVar.f3019a);
        hashMap.put("field9", dVar.k != null ? dVar.k.a().toString() : null);
        hashMap.put("field11", Boolean.valueOf(dVar.n));
        hashMap.put("field10", Boolean.valueOf(dVar.m));
        hashMap.put("field15", Boolean.valueOf(dVar.l));
        hashMap.put("field5", Integer.valueOf(dVar.g));
        hashMap.put(Message.DBFIELD_SAYHI, dVar.b);
        hashMap.put("field7", Integer.valueOf(dVar.i));
        hashMap.put(Message.DBFIELD_LOCATIONJSON, dVar.c);
        hashMap.put(Message.DBFIELD_CONVERLOCATIONJSON, a.a(dVar.e, ","));
        hashMap.put("field12", Boolean.valueOf(dVar.o));
        hashMap.put("field8", dVar.j);
        hashMap.put("field6", Integer.valueOf(dVar.h));
        hashMap.put(Message.DBFIELD_GROUPID, dVar.f);
        hashMap.put("field13", Integer.valueOf(dVar.p));
        hashMap.put("field16", Boolean.valueOf(dVar.q));
        a((Map) hashMap);
    }

    public final void b(d dVar) {
        HashMap hashMap = new HashMap();
        hashMap.put("field9", dVar.k != null ? dVar.k.a().toString() : null);
        hashMap.put("field11", Boolean.valueOf(dVar.n));
        hashMap.put("field10", Boolean.valueOf(dVar.m));
        hashMap.put("field15", Boolean.valueOf(dVar.l));
        hashMap.put("field5", Integer.valueOf(dVar.g));
        hashMap.put(Message.DBFIELD_SAYHI, dVar.b);
        hashMap.put("field7", Integer.valueOf(dVar.i));
        hashMap.put(Message.DBFIELD_LOCATIONJSON, dVar.c);
        hashMap.put(Message.DBFIELD_CONVERLOCATIONJSON, a.a(dVar.e, ","));
        hashMap.put("field12", Boolean.valueOf(dVar.o));
        hashMap.put("field8", dVar.j);
        hashMap.put("field6", Integer.valueOf(dVar.h));
        hashMap.put(Message.DBFIELD_GROUPID, dVar.f);
        hashMap.put("field13", Integer.valueOf(dVar.p));
        hashMap.put("field16", Boolean.valueOf(dVar.q));
        a(hashMap, new String[]{Message.DBFIELD_ID}, new String[]{dVar.f3019a});
    }
}
