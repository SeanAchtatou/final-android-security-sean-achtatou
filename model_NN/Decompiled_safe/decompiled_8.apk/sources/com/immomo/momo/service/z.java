package com.immomo.momo.service;

import com.immomo.momo.service.bean.a.j;
import java.util.Comparator;

final class z implements Comparator {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ y f3058a;
    private final /* synthetic */ int b;

    z(y yVar, int i) {
        this.f3058a = yVar;
        this.b = i;
    }

    public final /* synthetic */ int compare(Object obj, Object obj2) {
        float f = -1.0f;
        long j = 0;
        j jVar = (j) obj;
        j jVar2 = (j) obj2;
        if (this.b == 1) {
            long time = jVar.c != null ? jVar.c.getTime() : 0;
            if (jVar2.c != null) {
                j = jVar2.c.getTime();
            }
            if (time > j) {
                return -1;
            }
            if (time < j) {
                return 1;
            }
        } else if (this.b == 2) {
            float d = jVar.h != null ? jVar.h.d() : -1.0f;
            if (jVar2.h != null) {
                f = jVar2.h.d();
            }
            if (f < 0.0f) {
                f = 2.14748365E9f;
            }
            if (d < 0.0f) {
                d = 2.14748365E9f;
            }
            if (d < f) {
                return -1;
            }
            if (d > f) {
                return 1;
            }
        } else if (this.b == 3) {
            long time2 = (jVar.h == null || jVar.h.e() == null) ? 0 : jVar.h.e().getTime();
            if (!(jVar2.h == null || jVar2.h.e() == null)) {
                j = jVar2.h.e().getTime();
            }
            if (time2 > j) {
                return -1;
            }
            if (time2 < j) {
                return 1;
            }
        } else if (this.b == 4) {
            this.f3058a.b.a((Object) ("orderType: " + this.b));
            this.f3058a.b.a((Object) ("lhs.msgTime: " + jVar.e));
            this.f3058a.b.a((Object) ("rhs.msgTime" + jVar2.e));
            long time3 = jVar.e != null ? jVar.e.getTime() : 0;
            if (jVar2.e != null) {
                j = jVar2.e.getTime();
            }
            if (time3 > j) {
                return -1;
            }
            if (time3 < j) {
                return 1;
            }
        }
        return 0;
    }
}
