package com.immomo.momo.service.bean.c;

import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.service.bean.al;
import java.util.Arrays;
import java.util.Date;

public final class d extends al {

    /* renamed from: a  reason: collision with root package name */
    public String f3019a;
    public String b;
    public String c;
    public g d;
    public String[] e;
    public Date f;
    public int g;
    public int h;
    public int i;
    public String j;
    public f k;
    public boolean l;
    public boolean m;
    public boolean n;
    public boolean o;
    public int p;
    public boolean q;

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        d dVar = (d) obj;
        return this.f3019a == null ? dVar.f3019a == null : this.f3019a.equals(dVar.f3019a);
    }

    public final String getLoadImageId() {
        return (this.e == null || this.e.length <= 0) ? PoiTypeDef.All : this.e[0];
    }

    public final int hashCode() {
        return (this.f3019a == null ? 0 : this.f3019a.hashCode()) + 31;
    }

    public final String toString() {
        return "Tieba [id=" + this.f3019a + ", name=" + this.b + ", ownerMomoid=" + this.c + ", ownerUser=" + this.d + ", photos=" + Arrays.toString(this.e) + ", createTime=" + this.f + ", memberCount=" + this.g + ", tieCount=" + this.h + ", newCount=" + this.i + ", sign=" + this.j + ", category=" + this.k + ", isManager=" + this.l + ", isMember=" + this.m + ", hot=" + this.n + ", rcmd=" + this.o + ", status=" + this.p + ", hasUnread=" + this.q + "]";
    }
}
