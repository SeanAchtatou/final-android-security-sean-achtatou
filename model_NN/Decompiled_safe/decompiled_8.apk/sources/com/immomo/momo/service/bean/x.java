package com.immomo.momo.service.bean;

public enum x {
    ALL(0, "所有类型"),
    MUSIC(1, "音乐现场"),
    OPERA(2, "戏剧演出"),
    FILM(3, "讲座电影"),
    PARTY(6, "聚会玩乐"),
    EXHIBITION(5, "展览展会"),
    OTHERS(7, "其他类型"),
    SPORT(8, "体育运动");
    
    private String i;
    private int j;

    private x(int i2, String str) {
        this.j = i2;
        this.i = str;
    }

    public static x a(int i2) {
        for (x xVar : values()) {
            if (xVar.j == i2) {
                return xVar;
            }
        }
        return ALL;
    }

    public static String[] c() {
        x[] values = values();
        String[] strArr = new String[values.length];
        for (int i2 = 0; i2 < values.length; i2++) {
            strArr[i2] = values[i2].i;
        }
        return strArr;
    }

    public final int a() {
        return this.j;
    }

    public final String b() {
        return this.i;
    }
}
