package com.immomo.momo.service.bean;

import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.util.a.a;
import java.util.Date;
import org.json.JSONException;

public class GameApp {
    public String action;
    public String appURI;
    public String appdesc;
    public String appdownload;
    public String appicon;
    public String appid;
    public String appname;
    public String buttonLabel;
    public String developers;
    public String eventNotice;
    private al iconLoader;
    public String mcount;
    public String[] picArray;
    public String size;
    public String tiebaId;
    public String updateNotice;
    public Date updateTime;
    public String updateTimeString;
    public String versionName;

    public static GameApp initWithJson(String str) {
        try {
            GameApp gameApp = new GameApp();
            a.a(str, gameApp);
            return gameApp;
        } catch (JSONException e) {
            return null;
        }
    }

    public al appIconLoader() {
        if (this.iconLoader == null || !this.iconLoader.getLoadImageId().equals(this.appicon)) {
            if (this.appicon == null) {
                this.iconLoader = null;
            } else if (this.appicon.startsWith("http://")) {
                this.iconLoader = new al(this.appicon);
                this.iconLoader.setImageUrl(true);
            }
        }
        return this.iconLoader;
    }

    public String getAction() {
        return this.action;
    }

    public String getAppURI() {
        return this.appURI;
    }

    public String getAppdesc() {
        return this.appdesc;
    }

    public String getAppicon() {
        return this.appicon;
    }

    public String getAppid() {
        return this.appid;
    }

    public String getAppname() {
        return this.appname;
    }

    public String getAppstore() {
        return this.appdownload;
    }

    public String getButtonLabel() {
        return this.buttonLabel;
    }

    public String getDesc() {
        return this.updateNotice;
    }

    public String getDevelopers() {
        return this.developers;
    }

    public String getEventNotice() {
        return this.eventNotice;
    }

    public String getMcount() {
        return this.mcount;
    }

    public String getNotice() {
        return this.eventNotice;
    }

    public String getPicArrayString() {
        return this.picArray != null ? android.support.v4.b.a.a(this.picArray, ",") : PoiTypeDef.All;
    }

    public String getSize() {
        return this.size;
    }

    public long getUpdateTime() {
        if (this.updateTime != null) {
            return this.updateTime.getTime();
        }
        return 0;
    }

    public String getUpdateTimeString() {
        return this.updateTimeString;
    }

    public String getVersionName() {
        return this.versionName;
    }

    public void readJson(String str) {
        try {
            a.a(str, this);
        } catch (JSONException e) {
        }
    }

    public void setAction(Object obj) {
        this.action = (String) obj;
    }

    public void setAppURI(Object obj) {
        this.appURI = (String) obj;
    }

    public void setAppdesc(Object obj) {
        this.appdesc = (String) obj;
    }

    public void setAppicon(Object obj) {
        this.appicon = (String) obj;
    }

    public void setAppid(Object obj) {
        this.appid = (String) obj;
    }

    public void setAppname(Object obj) {
        this.appname = (String) obj;
    }

    public void setAppstore(Object obj) {
        this.appdownload = (String) obj;
    }

    public void setButtonLabel(Object obj) {
        if (obj == null) {
            this.buttonLabel = PoiTypeDef.All;
        } else {
            this.buttonLabel = obj.toString();
        }
    }

    public void setDesc(Object obj) {
        this.updateNotice = obj.toString();
    }

    public void setDevelopers(Object obj) {
        this.developers = obj.toString();
    }

    public void setEventNotice(String str) {
        this.eventNotice = str.toString();
    }

    public void setMcount(Object obj) {
        this.mcount = (String) obj;
    }

    public void setNotice(Object obj) {
        this.eventNotice = obj.toString();
    }

    public void setPicArrayString(Object obj) {
        if (obj != null) {
            this.picArray = android.support.v4.b.a.b(obj.toString(), ",");
        } else {
            this.picArray = null;
        }
    }

    public void setSize(Object obj) {
        this.size = obj.toString();
    }

    public void setUpdateTime(Object obj) {
        long j;
        try {
            j = Long.parseLong(obj.toString());
        } catch (Exception e) {
            j = 0;
        }
        if (j != 0) {
            this.updateTime = new Date(j);
        }
    }

    public void setUpdateTimeString(Object obj) {
        this.updateTimeString = obj.toString();
    }

    public void setVersionName(Object obj) {
        this.versionName = obj.toString();
    }

    public String toJson() {
        return a.b(this);
    }
}
