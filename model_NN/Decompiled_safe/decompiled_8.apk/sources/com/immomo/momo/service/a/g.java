package com.immomo.momo.service.a;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.v4.b.a;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.service.bean.Message;
import com.immomo.momo.service.bean.a.i;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.service.bean.n;
import com.immomo.momo.util.ak;
import com.immomo.momo.util.jni.Codec;
import com.immomo.momo.util.m;
import java.util.List;
import org.json.JSONObject;

public final class g extends SQLiteOpenHelper {

    /* renamed from: a  reason: collision with root package name */
    private m f2955a = new m(getClass().getSimpleName());
    private bf b = null;

    public g(Context context, String str) {
        super(context, str, (SQLiteDatabase.CursorFactory) null, 76);
    }

    private static String a(String str, int i) {
        for (int i2 = 1; i2 < i; i2++) {
            String str2 = "field" + i2 + " NUMERIC";
            if (i2 < i - 1) {
                str2 = String.valueOf(str2) + ",";
            }
            str = String.valueOf(str) + str2;
        }
        return String.valueOf(str) + ")";
    }

    public final void onCreate(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL(a("CREATE TABLE IF NOT EXISTS users2(_id INTEGER primary key autoincrement,u_momoid VARCHAR(50), u_name VARCHAR(50), u_updatetime VARCHAR(50),u_loctime NUMERIC, u_distance NUMERIC DEFAULT -1, u_version INTEGER, ", 91));
        sQLiteDatabase.execSQL(a("CREATE TABLE IF NOT EXISTS messages(_id INTEGER primary key autoincrement,m_failcount INTEGER, m_msgid VARCHAR(50) ,m_msginfo VARCHAR(500),m_remoteid VARCHAR(10) ,m_status INTEGER,m_receive VARCHAR(10),m_time VARCHAR(50),m_type INTEGER,", 10));
        sQLiteDatabase.execSQL(a("CREATE TABLE IF NOT EXISTS gmessages(_id INTEGER primary key autoincrement,m_failcount INTEGER, m_msgid VARCHAR(50) ,m_msginfo VARCHAR(500),m_remoteid VARCHAR(10) ,m_status INTEGER,m_receive VARCHAR(10),m_time VARCHAR(50),m_type INTEGER,", 10));
        sQLiteDatabase.execSQL(a("CREATE TABLE IF NOT EXISTS discussmessages(_id INTEGER primary key autoincrement,m_failcount INTEGER, m_msgid VARCHAR(50) ,m_msginfo VARCHAR(500),m_remoteid VARCHAR(10) ,m_status INTEGER,m_receive VARCHAR(10),m_time VARCHAR(50),m_type INTEGER,", 10));
        sQLiteDatabase.execSQL(a("CREATE TABLE IF NOT EXISTS friends(f_momoid VARCHAR(10), f_followtime VARCHAR(50), _id INTEGER primary key autoincrement ,", 10));
        sQLiteDatabase.execSQL(a("CREATE TABLE IF NOT EXISTS bothfollow(bf_momoid VARCHAR(10) primary key, f_followtime VARCHAR(50), ", 10));
        sQLiteDatabase.execSQL(a("CREATE TABLE IF NOT EXISTS mygroups(mg_gid VARCHAR(10) primary key, ", 10));
        sQLiteDatabase.execSQL(a("CREATE TABLE IF NOT EXISTS fans(f_momoid VARCHAR(10), f_followtime VARCHAR(50), _id INTEGER primary key autoincrement ,", 10));
        sQLiteDatabase.execSQL(a("CREATE TABLE IF NOT EXISTS blacklist(b_momoid VARCHAR(10), b_blacktime VARCHAR(50), _id INTEGER primary key autoincrement ,", 10));
        sQLiteDatabase.execSQL(a("CREATE TABLE IF NOT EXISTS nearybys(n_momoid VARCHAR(10), _id INTEGER primary key autoincrement , ", 10));
        sQLiteDatabase.execSQL(a("CREATE TABLE IF NOT EXISTS nearybyfeed(_id INTEGER primary key autoincrement, feedid VARCHAR(50), ", 10));
        sQLiteDatabase.execSQL(a("CREATE TABLE IF NOT EXISTS nearybyfeed(n_momoid VARCHAR(10), _id INTEGER primary key autoincrement , ", 10));
        sQLiteDatabase.execSQL(a("CREATE TABLE IF NOT EXISTS nearbysites(_id INTEGER primary key autoincrement, ", 10));
        sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS appinfo(a_key VARCHAR(50) primary key , a_value VARCHAR(50) )");
        sQLiteDatabase.execSQL(a("CREATE TABLE IF NOT EXISTS sessions(s_remoteid VARCHAR(10) primary key, s_fetchtime VARCHAR(50),s_lastmsgid VARCHAR(50),s_draft VARCHAR(300),orderid INTEGER,s_unread INTEGER, ", 10));
        sQLiteDatabase.execSQL(a("CREATE TABLE IF NOT EXISTS sayhi(remoteid VARCHAR(10), time VARCHAR(50), _id INTEGER primary key autoincrement ,", 10));
        sQLiteDatabase.execSQL(a("CREATE TABLE IF NOT EXISTS groups(_id INTEGER primary key autoincrement,", 70));
        sQLiteDatabase.execSQL(a("CREATE TABLE IF NOT EXISTS group_member(_id INTEGER primary key autoincrement,", 20));
        sQLiteDatabase.execSQL(a("CREATE TABLE IF NOT EXISTS gasessions(_id INTEGER primary key autoincrement,", 20));
        sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS chat_history(momoid VARCHAR(50) primary key)");
        sQLiteDatabase.execSQL(a("CREATE TABLE IF NOT EXISTS group_activity2(gp_id VARCHAR(50) primary key,", 70));
        sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS mcaches(id INTEGER primary key autoincrement,remoteid VARCHAR(50),filepath VARCHAR(50),count INTEGER,datetime LONG)");
        sQLiteDatabase.execSQL(a("CREATE TABLE IF NOT EXISTS nearbysites2(_id INTEGER primary key autoincrement, siteid VARCHAR(50), ", 10));
        sQLiteDatabase.execSQL(a("CREATE TABLE IF NOT EXISTS friendfeeds(_id INTEGER primary key autoincrement, feedid VARCHAR(50), ", 10));
        sQLiteDatabase.execSQL(a("CREATE TABLE IF NOT EXISTS mycommends(_id INTEGER primary key autoincrement, commentid VARCHAR(50), ", 10));
        String a2 = a("CREATE TABLE IF NOT EXISTS banners(", 20);
        sQLiteDatabase.execSQL(String.valueOf(a2.substring(0, a2.length() - 1)) + ", primary key (field1,field2))");
        sQLiteDatabase.execSQL(a("CREATE TABLE IF NOT EXISTS contactnotices(_id INTEGER primary key autoincrement,", 20));
        sQLiteDatabase.execSQL(a("CREATE TABLE IF NOT EXISTS c_uploadedlist(c_id VARCHAR(50) primary key,", 20));
        sQLiteDatabase.execSQL(a("CREATE TABLE IF NOT EXISTS discuss(did VARCHAR(50) primary key,", 20));
        sQLiteDatabase.execSQL(a("CREATE TABLE IF NOT EXISTS mydiscuss(md_id VARCHAR(10) primary key, ", 10));
        sQLiteDatabase.execSQL(a("CREATE TABLE IF NOT EXISTS discuss_member(_id INTEGER primary key autoincrement,", 20));
        sQLiteDatabase.execSQL(a("CREATE TABLE IF NOT EXISTS emotion(eid VARCHAR(10) primary key, ", 50));
        sQLiteDatabase.execSQL(a("CREATE TABLE IF NOT EXISTS tieba(_id VARCHAR(50) primary key, ", 50));
        sQLiteDatabase.execSQL(a("CREATE TABLE IF NOT EXISTS tie(_id VARCHAR(50) primary key, ", 50));
        sQLiteDatabase.execSQL(a("CREATE TABLE IF NOT EXISTS games(_id VARCHAR(50) primary key, ", 20));
        sQLiteDatabase.execSQL(a("CREATE TABLE IF NOT EXISTS frienddistancenotices(_id VARCHAR(50) primary key,", 20));
        sQLiteDatabase.execSQL(a("CREATE TABLE IF NOT EXISTS publishdraft(_id INETGER primary key, ", 20));
        try {
            if (this.b != null) {
                ba baVar = new ba(sQLiteDatabase);
                if (!baVar.c(this.b.h)) {
                    baVar.a(this.b);
                    this.b = null;
                }
            }
        } catch (Exception e) {
        }
        try {
            sQLiteDatabase.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS UserIndex on users2(u_momoid);");
            sQLiteDatabase.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS MessageIdIndex on messages(m_msgid);");
            sQLiteDatabase.execSQL("CREATE INDEX IF NOT EXISTS MessageUserIndex on messages(m_remoteid);");
            sQLiteDatabase.execSQL("CREATE INDEX IF NOT EXISTS MessageStatusIndex on messages(m_status);");
            sQLiteDatabase.execSQL("CREATE INDEX IF NOT EXISTS HiUserIndex on sayhi(remoteid);");
            sQLiteDatabase.execSQL("CREATE INDEX IF NOT EXISTS GMessageStatusIndex on gmessages(m_status);");
            sQLiteDatabase.execSQL("CREATE INDEX IF NOT EXISTS GMessageGroupIdIndex on gmessages(field4);");
            sQLiteDatabase.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS GMessageIdIndex on gmessages(m_msgid);");
            sQLiteDatabase.execSQL("CREATE INDEX IF NOT EXISTS DMessageStatusIndex on discussmessages(m_status);");
            sQLiteDatabase.execSQL("CREATE INDEX IF NOT EXISTS DMessageGroupIdIndex on discussmessages(field4);");
            sQLiteDatabase.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS DMessageIdIndex on discussmessages(m_msgid);");
        } catch (Exception e2) {
        }
    }

    public final void onOpen(SQLiteDatabase sQLiteDatabase) {
        super.onOpen(sQLiteDatabase);
        this.f2955a.a((Object) "db opened!");
    }

    public final void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        i c;
        i c2;
        this.f2955a.a((Object) ("onUpgrade~~~~~~~~~~~~oldVersion=" + i + ", newVersion=" + i2));
        if (i > 0 && i < 32) {
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS groups");
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS group_member");
        }
        if (i > 0 && i < 18) {
            this.f2955a.a((Object) "upgrade users start.... ");
            try {
                String b2 = ak.a(com.immomo.momo.g.c(), "app_preference").b("momoid", PoiTypeDef.All);
                try {
                    if (!a.a((CharSequence) b2)) {
                        Cursor rawQuery = sQLiteDatabase.rawQuery("select * from users where u_momoid=?", new String[]{b2});
                        if (rawQuery.moveToFirst()) {
                            bf bfVar = new bf(b2);
                            try {
                                bfVar.i = rawQuery.getString(rawQuery.getColumnIndex("u_name"));
                            } catch (Exception e) {
                            }
                            try {
                                bfVar.X = (long) rawQuery.getInt(rawQuery.getColumnIndex("u_version"));
                            } catch (Exception e2) {
                            }
                            try {
                                bfVar.M = rawQuery.getString(rawQuery.getColumnIndex("field7"));
                            } catch (Exception e3) {
                            }
                            try {
                                String string = rawQuery.getString(rawQuery.getColumnIndex("u_userinfo"));
                                if (!a.a((CharSequence) string)) {
                                    com.immomo.momo.util.a.a.a(string, bfVar);
                                }
                            } catch (Exception e4) {
                            }
                            try {
                                String string2 = rawQuery.getString(rawQuery.getColumnIndex(Message.DBFIELD_CONVERLOCATIONJSON));
                                if (!a.a((CharSequence) string2)) {
                                    JSONObject jSONObject = new JSONObject(Codec.b(string2));
                                    bfVar.S = jSONObject.getDouble("lat");
                                    bfVar.T = jSONObject.getDouble("lng");
                                    bfVar.U = jSONObject.getDouble("acc");
                                }
                            } catch (Exception e5) {
                            }
                            this.b = bfVar;
                        }
                        rawQuery.close();
                    }
                } catch (Exception e6) {
                    this.f2955a.a((Throwable) e6);
                }
                sQLiteDatabase.execSQL("DROP TABLE IF EXISTS users");
            } catch (Exception e7) {
                this.f2955a.a((Throwable) e7);
            }
            this.f2955a.a((Object) ("upgrade users end.... tmpCurrentUser=" + this.b));
        }
        if (i > 0 && i < 23) {
            try {
                sQLiteDatabase.execSQL("delete from users2 where _id not in (select min(_id) from users2 group by u_momoid)");
                sQLiteDatabase.execSQL("delete from messages where _id not in (select min(_id) from messages group by m_msgid)");
            } catch (Exception e8) {
            }
        }
        if (i < 11) {
            try {
                sQLiteDatabase.execSQL("alter table sessions add column orderid INTEGER default -1");
            } catch (Exception e9) {
            }
            Cursor rawQuery2 = sQLiteDatabase.rawQuery("select s_remoteid, s_lastmsgid from sessions", new String[0]);
            while (rawQuery2.moveToNext()) {
                String string3 = rawQuery2.getString(0);
                int i3 = -1;
                try {
                    Cursor rawQuery3 = sQLiteDatabase.rawQuery("select _id from messages where m_msgid=?", new String[]{rawQuery2.getString(1)});
                    if (rawQuery3.moveToFirst()) {
                        i3 = rawQuery3.getInt(0);
                    }
                    rawQuery3.close();
                } catch (Exception e10) {
                }
                sQLiteDatabase.execSQL("update sessions set orderid=? where s_remoteid=?", new Object[]{Integer.valueOf(i3), string3});
            }
            rawQuery2.close();
        }
        if (i < 4) {
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS fans");
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS friends");
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS blacklist");
        }
        if (i < 69) {
            sQLiteDatabase.beginTransaction();
            for (int i4 = 70; i4 <= 90; i4++) {
                try {
                    StringBuilder sb = new StringBuilder();
                    sb.append("alter table users2 add field").append(i4).append(" NUMERIC;");
                    sQLiteDatabase.execSQL(sb.toString());
                    this.f2955a.a(sb);
                } catch (Exception e11) {
                }
            }
            sQLiteDatabase.setTransactionSuccessful();
            sQLiteDatabase.endTransaction();
        }
        if (i >= 44 && i < 70) {
            try {
                sQLiteDatabase.execSQL("delete from sessions where s_remoteid=-2224");
            } catch (Exception e12) {
            }
        }
        if (i >= 44 && i < 72) {
            try {
                sQLiteDatabase.execSQL("delete from sessions where s_remoteid=-2223");
            } catch (Exception e13) {
            }
        }
        if (i >= 32 && i < 72) {
            try {
                List<com.immomo.momo.service.bean.a.a> b3 = new w(sQLiteDatabase).b("select g.*, mg.mg_gid from mygroups as mg JOIN " + "groups as g" + " on mg.mg_gid" + " = g.field1", new String[0]);
                if (b3.size() > 0) {
                    z zVar = new z(sQLiteDatabase);
                    for (com.immomo.momo.service.bean.a.a aVar : b3) {
                        if (!(com.immomo.momo.g.r() == null || (c2 = com.immomo.momo.g.r().c(aVar.b)) == null || c2.f2978a)) {
                            zVar.a(new String[]{Message.DBFIELD_STATUS}, new String[]{"13"}, new String[]{Message.DBFIELD_GROUPID, Message.DBFIELD_STATUS}, new String[]{aVar.b, "5"});
                        }
                    }
                }
                this.f2955a.a((Object) "群数据库升级完毕");
            } catch (Exception e14) {
                this.f2955a.a((Throwable) e14);
            }
            try {
                List<n> a2 = new h(sQLiteDatabase).a("did", new ai(sQLiteDatabase).b().toArray(), (String) null);
                if (a2.size() > 0) {
                    i iVar = new i(sQLiteDatabase);
                    for (n nVar : a2) {
                        if (!(com.immomo.momo.g.r() == null || (c = com.immomo.momo.g.r().c(nVar.f)) == null || c.f2978a)) {
                            iVar.a(new String[]{Message.DBFIELD_STATUS}, new String[]{"13"}, new String[]{Message.DBFIELD_GROUPID, Message.DBFIELD_STATUS}, new String[]{nVar.f, "5"});
                        }
                    }
                }
                this.f2955a.a((Object) "讨论组数据库升级完毕");
            } catch (Exception e15) {
                this.f2955a.a((Throwable) e15);
            }
        }
        onCreate(sQLiteDatabase);
    }
}
