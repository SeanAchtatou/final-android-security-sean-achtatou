package com.immomo.momo.service.bean;

import java.io.File;

public final class r extends al {

    /* renamed from: a  reason: collision with root package name */
    public File f3036a;
    private final String b;
    private final String c;
    private final String d;
    private final String e;
    private String f;
    private final String g;

    public r(String str, String str2, String str3, String str4, String str5, String str6) {
        this.b = str;
        this.c = str2;
        this.d = str3;
        this.e = str4;
        this.f = str5;
        this.g = str6;
    }

    public final String a() {
        return this.b;
    }

    public final void a(String str) {
        this.f = str;
    }

    public final String b() {
        return this.c;
    }

    public final String c() {
        return this.g;
    }

    public final String d() {
        return this.d;
    }

    public final String e() {
        return this.e;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        r rVar = (r) obj;
        if (this.d == null) {
            if (rVar.d != null) {
                return false;
            }
        } else if (!this.d.equals(rVar.d)) {
            return false;
        }
        return this.c == null ? rVar.c == null : this.c.equals(rVar.c);
    }

    public final String f() {
        return this.f;
    }

    public final r g() {
        return new r(this.b, this.c, this.d, this.e, this.f, this.g);
    }

    public final String getLoadImageId() {
        return this.c;
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = ((this.d == null ? 0 : this.d.hashCode()) + 31) * 31;
        if (this.c != null) {
            i = this.c.hashCode();
        }
        return hashCode + i;
    }

    public final String toString() {
        return "[" + this.b + "|et|l=" + this.d + "|n=" + this.c + "." + this.g + "|s=" + this.e + "]";
    }
}
