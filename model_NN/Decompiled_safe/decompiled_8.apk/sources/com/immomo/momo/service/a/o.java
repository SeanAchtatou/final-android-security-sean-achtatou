package com.immomo.momo.service.a;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.immomo.momo.service.bean.Message;
import com.immomo.momo.service.bean.af;

public final class o extends b {
    public o(SQLiteDatabase sQLiteDatabase) {
        super(sQLiteDatabase, "feedfilter_types", "c_id");
    }

    private static void a(af afVar, Cursor cursor) {
        afVar.b = cursor.getString(cursor.getColumnIndex("c_id"));
        afVar.f2986a = cursor.getString(cursor.getColumnIndex(Message.DBFIELD_SAYHI));
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Cursor cursor) {
        af afVar = new af();
        a(afVar, cursor);
        return afVar;
    }

    public final /* synthetic */ void a(Object obj) {
        af afVar = (af) obj;
        b(new String[]{"c_id", Message.DBFIELD_SAYHI}, new Object[]{afVar.b, afVar.f2986a});
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void a(Object obj, Cursor cursor) {
        a((af) obj, cursor);
    }
}
