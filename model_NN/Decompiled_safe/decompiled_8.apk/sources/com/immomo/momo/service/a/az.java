package com.immomo.momo.service.a;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public final class az extends b {
    public az(SQLiteDatabase sQLiteDatabase) {
        super(sQLiteDatabase, "c_uploadedlist", "c_id");
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Cursor cursor) {
        return cursor.getString(cursor.getColumnIndex("c_id"));
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void a(Object obj, Cursor cursor) {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.service.a.b.b(java.lang.String[], java.lang.Object[]):void
     arg types: [java.lang.String[], java.lang.String[]]
     candidates:
      com.immomo.momo.service.a.b.b(android.database.Cursor, java.lang.String):long
      com.immomo.momo.service.a.b.b(java.lang.String, java.lang.Object):java.lang.Object
      com.immomo.momo.service.a.b.b(java.lang.String[], java.lang.String[]):java.lang.Object
      com.immomo.momo.service.a.b.b(java.lang.String, java.lang.String[]):java.util.List
      com.immomo.momo.service.a.b.b(java.lang.String, java.lang.Object[]):void
      com.immomo.momo.service.a.b.b(java.lang.String[], java.lang.Object[]):void */
    /* renamed from: b */
    public final void a(String str) {
        b(new String[]{"c_id"}, (Object[]) new String[]{str});
    }
}
