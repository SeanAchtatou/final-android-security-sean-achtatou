package com.immomo.momo.service.a;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.b.a;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.Message;
import com.immomo.momo.service.bean.a.f;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public final class x extends b {
    private static Set d = new HashSet();

    public x(SQLiteDatabase sQLiteDatabase) {
        super(sQLiteDatabase, "groupfeedcomments", "c_id");
    }

    private static void a(f fVar, Cursor cursor) {
        fVar.i = c(cursor, "c_id");
        fVar.b = c(cursor, Message.DBFIELD_LOCATIONJSON);
        fVar.j = a(cursor, "field7");
        fVar.l = a(cursor, "field12");
        fVar.k = a(cursor, "field13");
        fVar.m = a(cursor, "field10");
        fVar.h = c(cursor, "field5");
        fVar.n = c(cursor, "field8");
        fVar.g = c(cursor, Message.DBFIELD_GROUPID);
        fVar.c = c(cursor, Message.DBFIELD_SAYHI);
        fVar.e = c(cursor, "field9");
        fVar.a(d(cursor, Message.DBFIELD_CONVERLOCATIONJSON));
        fVar.d = c(cursor, "field14");
        if (!a.a((CharSequence) fVar.i)) {
            d.add(fVar.i);
        }
    }

    public static void g() {
        Set set = d;
        String[] strArr = new String[set.size()];
        set.toArray(strArr);
        if (g.d().h() != null) {
            new x(g.d().h()).a("field11", new Date(), "c_id", strArr);
            set.clear();
        }
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Cursor cursor) {
        f fVar = new f();
        a(fVar, cursor);
        return fVar;
    }

    public final void a(f fVar) {
        HashMap hashMap = new HashMap();
        hashMap.put("c_id", fVar.i);
        hashMap.put(Message.DBFIELD_LOCATIONJSON, fVar.b);
        hashMap.put("field7", Integer.valueOf(fVar.j));
        hashMap.put("field10", Integer.valueOf(fVar.m));
        hashMap.put("field5", fVar.h);
        hashMap.put("field8", fVar.n);
        hashMap.put(Message.DBFIELD_GROUPID, fVar.g);
        hashMap.put("field9", fVar.e);
        hashMap.put(Message.DBFIELD_CONVERLOCATIONJSON, fVar.a());
        hashMap.put(Message.DBFIELD_SAYHI, fVar.c);
        hashMap.put("field11", new Date());
        hashMap.put("field12", Integer.valueOf(fVar.l));
        hashMap.put("field13", Integer.valueOf(fVar.k));
        hashMap.put("field14", fVar.d);
        a((Map) hashMap);
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void a(Object obj, Cursor cursor) {
        a((f) obj, cursor);
    }

    public final void b(f fVar) {
        HashMap hashMap = new HashMap();
        hashMap.put(Message.DBFIELD_LOCATIONJSON, fVar.b);
        hashMap.put("field7", Integer.valueOf(fVar.j));
        hashMap.put("field10", Integer.valueOf(fVar.m));
        hashMap.put("field5", fVar.h);
        hashMap.put("field8", fVar.n);
        hashMap.put(Message.DBFIELD_GROUPID, fVar.g);
        hashMap.put("field9", fVar.e);
        hashMap.put(Message.DBFIELD_CONVERLOCATIONJSON, fVar.a());
        hashMap.put(Message.DBFIELD_SAYHI, fVar.c);
        hashMap.put("field11", new Date());
        hashMap.put("field12", Integer.valueOf(fVar.l));
        hashMap.put("field13", Integer.valueOf(fVar.k));
        hashMap.put("field14", fVar.d);
        a(hashMap, new String[]{"c_id"}, new Object[]{fVar.i});
    }
}
