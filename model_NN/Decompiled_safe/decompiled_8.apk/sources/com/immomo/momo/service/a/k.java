package com.immomo.momo.service.a;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.immomo.momo.service.bean.Message;
import com.immomo.momo.service.m;
import java.util.HashMap;
import java.util.Map;

public final class k extends b {
    public k(SQLiteDatabase sQLiteDatabase) {
        super(sQLiteDatabase, "publishdraft", Message.DBFIELD_ID);
    }

    private static void a(m mVar, Cursor cursor) {
        mVar.b = a(cursor, Message.DBFIELD_ID);
        mVar.f3050a = c(cursor, Message.DBFIELD_SAYHI);
        mVar.c = a(cursor, Message.DBFIELD_LOCATIONJSON);
        mVar.e = c(cursor, Message.DBFIELD_GROUPID);
        mVar.d = d(cursor, "field5");
        mVar.f = a(cursor, Message.DBFIELD_CONVERLOCATIONJSON);
        mVar.g = c(cursor, "field6");
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Cursor cursor) {
        m mVar = new m();
        a(mVar, cursor);
        return mVar;
    }

    public final void a(m mVar) {
        HashMap hashMap = new HashMap();
        hashMap.put(Message.DBFIELD_SAYHI, mVar.f3050a);
        hashMap.put(Message.DBFIELD_ID, Integer.valueOf(mVar.b));
        hashMap.put(Message.DBFIELD_LOCATIONJSON, Integer.valueOf(mVar.c));
        hashMap.put(Message.DBFIELD_GROUPID, mVar.e);
        hashMap.put("field5", mVar.d);
        hashMap.put(Message.DBFIELD_CONVERLOCATIONJSON, Integer.valueOf(mVar.f));
        hashMap.put("field6", mVar.g);
        a((Map) hashMap);
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void a(Object obj, Cursor cursor) {
        a((m) obj, cursor);
    }
}
