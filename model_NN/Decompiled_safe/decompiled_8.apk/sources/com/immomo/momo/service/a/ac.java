package com.immomo.momo.service.a;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public final class ac extends SQLiteOpenHelper {
    public ac(Context context, String str) {
        super(context, "ij_" + str, (SQLiteDatabase.CursorFactory) null, 4);
    }

    public final void onCreate(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS sendtask(msgid VARCHAR(50) primary key,info VARCHAR(200),time VARCHAR(50),type Integer) ");
    }

    public final void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        onCreate(sQLiteDatabase);
    }
}
