package com.immomo.momo.service.a;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public final class e extends b {
    public e(SQLiteDatabase sQLiteDatabase) {
        super(sQLiteDatabase, "chat_history", "momoid");
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Cursor cursor) {
        return cursor.getString(cursor.getColumnIndex("momoid"));
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void a(Object obj, Cursor cursor) {
    }

    /* renamed from: b */
    public final void a(String str) {
        StringBuilder sb = new StringBuilder();
        sb.append("insert into " + this.f2954a + " (").append("momoid) values(?)");
        a(sb.toString(), new Object[]{str});
    }
}
