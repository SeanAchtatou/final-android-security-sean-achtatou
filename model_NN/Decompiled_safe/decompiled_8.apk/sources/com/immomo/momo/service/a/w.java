package com.immomo.momo.service.a;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.immomo.momo.service.bean.Message;
import com.immomo.momo.service.bean.a.a;

public final class w extends b {
    public w(SQLiteDatabase sQLiteDatabase) {
        super(sQLiteDatabase, "groups", Message.DBFIELD_SAYHI);
    }

    private static void a(a aVar, Cursor cursor) {
        boolean z = true;
        aVar.b = cursor.getString(cursor.getColumnIndex(Message.DBFIELD_SAYHI));
        aVar.F = android.support.v4.b.a.b(cursor.getString(cursor.getColumnIndex("field5")), ",");
        aVar.d = a(cursor.getLong(cursor.getColumnIndex(Message.DBFIELD_CONVERLOCATIONJSON)));
        aVar.e = a(cursor.getLong(cursor.getColumnIndex("field32")));
        aVar.g = cursor.getString(cursor.getColumnIndex(Message.DBFIELD_GROUPID));
        aVar.q = cursor.getInt(cursor.getColumnIndex("field17"));
        aVar.o = cursor.getString(cursor.getColumnIndex("field10"));
        aVar.f = cursor.getString(cursor.getColumnIndex("field33"));
        aVar.h = cursor.getString(cursor.getColumnIndex("field6"));
        aVar.i = cursor.getString(cursor.getColumnIndex("field7"));
        aVar.j = cursor.getInt(cursor.getColumnIndex("field8"));
        aVar.k = cursor.getInt(cursor.getColumnIndex("field9"));
        aVar.r = cursor.getInt(cursor.getColumnIndex("field31")) == 1;
        aVar.c = cursor.getString(cursor.getColumnIndex(Message.DBFIELD_LOCATIONJSON));
        aVar.E = cursor.getString(cursor.getColumnIndex("field40"));
        aVar.D = cursor.getInt(cursor.getColumnIndex("field39")) == 1;
        aVar.z = cursor.getLong(cursor.getColumnIndex("field18"));
        aVar.w = cursor.getLong(cursor.getColumnIndex("field27"));
        aVar.v = cursor.getLong(cursor.getColumnIndex("field26"));
        aVar.l = cursor.getDouble(cursor.getColumnIndex("field15"));
        aVar.m = cursor.getDouble(cursor.getColumnIndex("field16"));
        aVar.G = cursor.getInt(cursor.getColumnIndex("field13"));
        aVar.A = cursor.getInt(cursor.getColumnIndex("field24"));
        aVar.B = cursor.getInt(cursor.getColumnIndex("field29"));
        aVar.y = cursor.getInt(cursor.getColumnIndex("field30"));
        aVar.C = cursor.getInt(cursor.getColumnIndex("field25")) == 1;
        aVar.x = cursor.getInt(cursor.getColumnIndex("field28")) == 1;
        aVar.I = android.support.v4.b.a.b(cursor.getString(cursor.getColumnIndex("field14")), ",");
        aVar.J = cursor.getString(cursor.getColumnIndex("field20"));
        aVar.K = cursor.getString(cursor.getColumnIndex("field21"));
        aVar.L = cursor.getInt(cursor.getColumnIndex("field22"));
        aVar.s = cursor.getInt(cursor.getColumnIndex("field35")) == 1;
        aVar.u = e(cursor, "field38");
        if (cursor.getInt(cursor.getColumnIndex("field34")) != 1) {
            z = false;
        }
        aVar.t = z;
        aVar.O = c(cursor, "field36");
        aVar.n = a(cursor, "field37");
        try {
            aVar.a(cursor.getFloat(cursor.getColumnIndex("field23")));
        } catch (Exception e) {
            aVar.a(-1.0f);
        }
        try {
            if (android.support.v4.b.a.a((CharSequence) aVar.b)) {
                aVar.b = cursor.getString(cursor.getColumnIndex("mg_gid"));
            }
        } catch (Exception e2) {
        }
        try {
            aVar.f2970a = cursor.getInt(cursor.getColumnIndex("field19"));
        } catch (Exception e3) {
        }
    }

    private void a(String str, a aVar, boolean z) {
        int i = 1;
        if (z) {
            Object[] objArr = new Object[35];
            objArr[0] = aVar.o;
            objArr[1] = aVar.f;
            objArr[2] = Long.valueOf(aVar.z);
            objArr[3] = Long.valueOf(aVar.w);
            objArr[4] = Long.valueOf(aVar.v);
            objArr[5] = android.support.v4.b.a.a(aVar.F, ",");
            objArr[6] = Long.valueOf(a(aVar.d));
            objArr[7] = Long.valueOf(a(aVar.e));
            objArr[8] = aVar.g;
            objArr[9] = aVar.h;
            objArr[10] = aVar.i;
            objArr[11] = Integer.valueOf(aVar.j);
            objArr[12] = Integer.valueOf(aVar.k);
            objArr[13] = Integer.valueOf(aVar.r ? 1 : 0);
            objArr[14] = aVar.c;
            objArr[15] = aVar.E;
            objArr[16] = Integer.valueOf(aVar.D ? 1 : 0);
            objArr[17] = Integer.valueOf(aVar.n);
            objArr[18] = Integer.valueOf(aVar.q);
            objArr[19] = Double.valueOf(aVar.l);
            objArr[20] = Double.valueOf(aVar.m);
            objArr[21] = Float.valueOf(aVar.b());
            objArr[22] = Integer.valueOf(aVar.G);
            objArr[23] = Integer.valueOf(aVar.A);
            objArr[24] = Integer.valueOf(aVar.B);
            objArr[25] = Integer.valueOf(aVar.y);
            objArr[26] = Integer.valueOf(aVar.C ? 1 : 0);
            if (!aVar.x) {
                i = 0;
            }
            objArr[27] = Integer.valueOf(i);
            objArr[28] = aVar.J;
            objArr[29] = aVar.K;
            objArr[30] = Integer.valueOf(aVar.L);
            objArr[31] = Boolean.valueOf(aVar.s);
            objArr[32] = Boolean.valueOf(aVar.t);
            objArr[33] = Integer.valueOf(aVar.f2970a);
            objArr[34] = aVar.b;
            a(str, objArr);
            return;
        }
        Object[] objArr2 = new Object[34];
        objArr2[0] = aVar.o;
        objArr2[1] = aVar.f;
        objArr2[2] = Long.valueOf(aVar.z);
        objArr2[3] = Long.valueOf(aVar.w);
        objArr2[4] = Long.valueOf(aVar.v);
        objArr2[5] = android.support.v4.b.a.a(aVar.F, ",");
        objArr2[6] = Long.valueOf(a(aVar.d));
        objArr2[7] = Long.valueOf(a(aVar.e));
        objArr2[8] = aVar.g;
        objArr2[9] = aVar.h;
        objArr2[10] = aVar.i;
        objArr2[11] = Integer.valueOf(aVar.j);
        objArr2[12] = Integer.valueOf(aVar.k);
        objArr2[13] = Integer.valueOf(aVar.r ? 1 : 0);
        objArr2[14] = aVar.c;
        objArr2[15] = aVar.E;
        objArr2[16] = Integer.valueOf(aVar.D ? 1 : 0);
        objArr2[17] = Integer.valueOf(aVar.n);
        objArr2[18] = Integer.valueOf(aVar.q);
        objArr2[19] = Double.valueOf(aVar.l);
        objArr2[20] = Double.valueOf(aVar.m);
        objArr2[21] = Float.valueOf(aVar.b());
        objArr2[22] = Integer.valueOf(aVar.G);
        objArr2[23] = Integer.valueOf(aVar.A);
        objArr2[24] = Integer.valueOf(aVar.B);
        objArr2[25] = Integer.valueOf(aVar.y);
        objArr2[26] = Integer.valueOf(aVar.C ? 1 : 0);
        if (!aVar.x) {
            i = 0;
        }
        objArr2[27] = Integer.valueOf(i);
        objArr2[28] = aVar.J;
        objArr2[29] = aVar.K;
        objArr2[30] = Integer.valueOf(aVar.L);
        objArr2[31] = Boolean.valueOf(aVar.s);
        objArr2[32] = Boolean.valueOf(aVar.t);
        objArr2[33] = aVar.b;
        a(str, objArr2);
    }

    private static a b(Cursor cursor) {
        a aVar = new a();
        a(aVar, cursor);
        return aVar;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Cursor cursor) {
        return b(cursor);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.service.a.w.a(java.lang.String, com.immomo.momo.service.bean.a.a, boolean):void
     arg types: [java.lang.String, com.immomo.momo.service.bean.a.a, int]
     candidates:
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object[], java.lang.String):java.util.List
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object, java.io.Serializable):void
      com.immomo.momo.service.a.b.a(java.util.Map, java.lang.String[], java.lang.Object[]):void
      com.immomo.momo.service.a.b.a(java.lang.String[], java.lang.Object[], java.io.Serializable):void
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.String[], java.lang.String[]):java.lang.String[]
      com.immomo.momo.service.a.w.a(java.lang.String, com.immomo.momo.service.bean.a.a, boolean):void */
    public final void a(a aVar) {
        StringBuilder sb = new StringBuilder();
        sb.append("insert into " + this.f2954a + " (").append("field10, field33, field18, field27, field26, field5, field3, field32, field4, field6, field7, field8, field9, field31, field2, field40, field39, field37, field17, field15, field16, field23, field13, field24, field29, field30, field25, field28, field20, field21, field22, field35, ").append("field34, field19, field1) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
        a(sb.toString(), aVar, true);
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void a(Object obj, Cursor cursor) {
        a((a) obj, cursor);
    }

    public final void b(a aVar) {
        StringBuilder sb = new StringBuilder();
        sb.append("update " + this.f2954a + " set ").append("field10=?, field33=?, field18=?, field27=?, field26=?, field5=?, field3=?, field32=?, field4=?, field6=?, field7=?, field8=?, field9=?, field31=?, field2=?, field40=?, field39=?, field37=?, field17=?, field15=?, field16=?, field23=?, field13=?, field24=?, field29=?, field30=?, field25=?, field28=?, field20=?, field21=?, field22=?, field35=?, ").append("field34=? ");
        boolean z = aVar.f2970a != -1;
        if (z) {
            sb.append(",field19=? ");
        }
        sb.append("where field1=? ");
        a(sb.toString(), aVar, z);
    }
}
