package com.immomo.momo.service.bean;

import android.graphics.Bitmap;
import java.io.File;

public final class av {

    /* renamed from: a  reason: collision with root package name */
    public String f2998a;
    public File b;
    public Bitmap c;
    public boolean d = false;
    public String e;

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        av avVar = (av) obj;
        return this.f2998a == null ? avVar.f2998a == null : this.f2998a.equals(avVar.f2998a);
    }

    public final int hashCode() {
        return (this.f2998a == null ? 0 : this.f2998a.hashCode()) + 31;
    }

    public final String toString() {
        return "PublishImageBean [thumbPath=" + ((String) null) + ", largePath=" + this.f2998a + "]";
    }
}
