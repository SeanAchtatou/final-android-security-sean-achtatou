package com.immomo.momo.service;

import com.immomo.momo.service.bean.ab;
import java.util.Comparator;
import java.util.List;

final class al implements Comparator {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ List f2962a;

    al(List list) {
        this.f2962a = list;
    }

    public final /* synthetic */ int compare(Object obj, Object obj2) {
        return this.f2962a.indexOf(((ab) obj).h) > this.f2962a.indexOf(((ab) obj2).h) ? 1 : -1;
    }
}
