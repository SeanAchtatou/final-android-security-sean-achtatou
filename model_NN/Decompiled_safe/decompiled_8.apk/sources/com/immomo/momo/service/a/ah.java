package com.immomo.momo.service.a;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.immomo.momo.service.bean.Message;

public final class ah extends b {
    public ah(SQLiteDatabase sQLiteDatabase) {
        super(sQLiteDatabase, "mycommends", "commentid");
    }

    private static void a(String[] strArr, Cursor cursor) {
        try {
            strArr[0] = cursor.getString(cursor.getColumnIndex("commentid"));
            strArr[1] = cursor.getString(cursor.getColumnIndex(Message.DBFIELD_SAYHI));
        } catch (Exception e) {
        }
    }

    private static String[] b(Cursor cursor) {
        String[] strArr = new String[2];
        a(strArr, cursor);
        return strArr;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Cursor cursor) {
        return b(cursor);
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void a(Object obj, Cursor cursor) {
        a((String[]) obj, cursor);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object[]):void
     arg types: [java.lang.String, java.lang.String[]]
     candidates:
      com.immomo.momo.service.a.ah.a(java.lang.String[], android.database.Cursor):void
      com.immomo.momo.service.a.ah.a(java.lang.Object, android.database.Cursor):void
      com.immomo.momo.service.a.b.a(android.database.Cursor, java.lang.String):int
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.String[]):android.database.Cursor
      com.immomo.momo.service.a.b.a(java.lang.String[], java.lang.String[]):java.util.List
      com.immomo.momo.service.a.b.a(java.lang.Object, android.database.Cursor):void
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object):void
      com.immomo.momo.service.a.b.a(java.lang.String[], java.lang.Object[]):void
      com.immomo.momo.service.a.b.a(java.lang.Object, java.io.Serializable):boolean
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.String):boolean
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object[]):void */
    public final void a(String[] strArr) {
        StringBuilder sb = new StringBuilder();
        sb.append("insert into " + this.f2954a + " (").append("commentid,field1) values(?,?)");
        a(sb.toString(), (Object[]) new String[]{strArr[0], strArr[1]});
    }
}
