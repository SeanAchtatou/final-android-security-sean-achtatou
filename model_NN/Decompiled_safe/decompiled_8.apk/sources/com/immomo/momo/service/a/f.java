package com.immomo.momo.service.a;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.b.a;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.plugin.e.e;
import com.immomo.momo.service.bean.GameApp;
import com.immomo.momo.service.bean.Message;
import com.immomo.momo.service.bean.i;
import com.immomo.momo.service.bean.j;
import com.immomo.momo.service.bean.k;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;

public final class f extends b {
    public f(SQLiteDatabase sQLiteDatabase) {
        super(sQLiteDatabase, "contactnotices", "field5");
        this.c.b();
    }

    /* access modifiers changed from: private */
    public void a(i iVar, Cursor cursor) {
        iVar.e(cursor.getString(cursor.getColumnIndex("field5")));
        iVar.a(a(cursor.getLong(cursor.getColumnIndex(Message.DBFIELD_LOCATIONJSON))));
        iVar.f(cursor.getString(cursor.getColumnIndex("field19")));
        iVar.b(cursor.getString(cursor.getColumnIndex(Message.DBFIELD_SAYHI)));
        iVar.a(cursor.getInt(cursor.getColumnIndex("field6")) == 0);
        iVar.a(cursor.getInt(cursor.getColumnIndex(Message.DBFIELD_CONVERLOCATIONJSON)));
        iVar.a(cursor.getString(cursor.getColumnIndex("field7")));
        iVar.c(cursor.getString(cursor.getColumnIndex("field10")));
        iVar.b(cursor.getInt(cursor.getColumnIndex("field9")) == 1);
        iVar.b(cursor.getInt(cursor.getColumnIndex("field11")));
        if (iVar.b() <= 0) {
            iVar.b(1);
        }
        if (iVar.b() == 2 || iVar.b() == 3 || iVar.b() == 4) {
            e eVar = new e();
            iVar.a(eVar);
            eVar.f2875a = cursor.getString(cursor.getColumnIndex("field12"));
            eVar.c = cursor.getString(cursor.getColumnIndex("field13"));
            eVar.h = cursor.getString(cursor.getColumnIndex("field14"));
        }
        if (iVar.b() == 6) {
            GameApp gameApp = new GameApp();
            iVar.a(gameApp);
            gameApp.appid = cursor.getString(cursor.getColumnIndex("field15"));
            gameApp.appname = cursor.getString(cursor.getColumnIndex("field16"));
            gameApp.setAction(cursor.getString(cursor.getColumnIndex("field18")));
            gameApp.appicon = cursor.getString(cursor.getColumnIndex("field17"));
        }
        try {
            String string = cursor.getString(cursor.getColumnIndex("field8"));
            if (string != null && !a.a((CharSequence) string)) {
                JSONArray jSONArray = new JSONArray(string);
                ArrayList arrayList = new ArrayList();
                iVar.a(arrayList);
                for (int i = 0; i < jSONArray.length(); i++) {
                    j jVar = new j();
                    jVar.a(jSONArray.getString(i));
                    arrayList.add(jVar);
                }
            }
        } catch (JSONException e) {
            this.c.a((Throwable) e);
        }
        try {
            String string2 = cursor.getString(cursor.getColumnIndex(Message.DBFIELD_GROUPID));
            if (string2 != null && !a.a((CharSequence) string2)) {
                ArrayList arrayList2 = new ArrayList();
                StringBuilder sb = new StringBuilder(string2);
                StringBuilder sb2 = new StringBuilder();
                k.a(sb, sb2, arrayList2);
                iVar.d(sb2.toString());
                iVar.b(arrayList2);
            }
        } catch (Exception e2) {
            this.c.a((Throwable) e2);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public i a(Cursor cursor) {
        i iVar = new i();
        a(iVar, cursor);
        return iVar;
    }

    public final void a(i iVar) {
        String str;
        String str2;
        int i = 0;
        String str3 = null;
        if (iVar.q()) {
            JSONArray jSONArray = new JSONArray();
            int i2 = 0;
            for (j jVar : iVar.p()) {
                int i3 = i2 + 1;
                try {
                    jSONArray.put(i2, jVar.toString());
                    i2 = i3;
                } catch (JSONException e) {
                    this.c.a((Throwable) e);
                    i2 = i3;
                }
            }
            str = jSONArray.toString();
        } else {
            str = PoiTypeDef.All;
        }
        this.c.a((Object) ("actions=" + str));
        String n = iVar.n();
        List r = iVar.r();
        if (r == null || r.isEmpty()) {
            str2 = n;
        } else {
            String[] strArr = new String[r.size()];
            for (int i4 = 0; i4 < r.size(); i4++) {
                strArr[i4] = ((k) r.get(i4)).toString();
            }
            str2 = String.format(n, strArr);
        }
        this.c.a((Object) ("message=" + str2));
        String[] strArr2 = {"field8", Message.DBFIELD_GROUPID, "field10", Message.DBFIELD_LOCATIONJSON, "field5", "field19", Message.DBFIELD_SAYHI, "field6", "field7", Message.DBFIELD_CONVERLOCATIONJSON, "field9", "field11", "field12", "field13", "field14", "field15", "field16", "field18", "field17"};
        Object[] objArr = new Object[19];
        objArr[0] = str;
        objArr[1] = str2;
        objArr[2] = iVar.l();
        objArr[3] = iVar.f();
        objArr[4] = iVar.o();
        objArr[5] = iVar.s();
        objArr[6] = iVar.k();
        if (!iVar.e()) {
            i = 1;
        }
        objArr[7] = Integer.valueOf(i);
        objArr[8] = iVar.h();
        objArr[9] = Integer.valueOf(iVar.a());
        objArr[10] = Boolean.valueOf(iVar.g());
        objArr[11] = Integer.valueOf(iVar.b());
        objArr[12] = iVar.c() != null ? iVar.c().f2875a : null;
        objArr[13] = iVar.c() != null ? iVar.c().c : null;
        objArr[14] = iVar.c() != null ? iVar.c().h : null;
        objArr[15] = iVar.d() != null ? iVar.d().appid : null;
        objArr[16] = iVar.d() != null ? iVar.d().appname : null;
        objArr[17] = iVar.d() != null ? iVar.d().getAction() : null;
        if (iVar.d() != null) {
            str3 = iVar.d().appicon;
        }
        objArr[18] = str3;
        b(strArr2, objArr);
    }

    public final void b(i iVar) {
        String str;
        String str2;
        String str3 = null;
        if (iVar.q()) {
            JSONArray jSONArray = new JSONArray();
            int i = 0;
            for (j jVar : iVar.p()) {
                int i2 = i + 1;
                try {
                    jSONArray.put(i, jVar.toString());
                    i = i2;
                } catch (JSONException e) {
                    this.c.a((Throwable) e);
                    i = i2;
                }
            }
            str = jSONArray.toString();
        } else {
            str = PoiTypeDef.All;
        }
        this.c.a((Object) ("actions=" + str));
        String n = iVar.n();
        List r = iVar.r();
        if (r == null || r.isEmpty()) {
            str2 = n;
        } else {
            String[] strArr = new String[r.size()];
            for (int i3 = 0; i3 < r.size(); i3++) {
                strArr[i3] = ((k) r.get(i3)).toString();
            }
            str2 = String.format(n, strArr);
        }
        this.c.a((Object) ("message=" + str2));
        String[] strArr2 = {"field8", Message.DBFIELD_GROUPID, "field10", Message.DBFIELD_LOCATIONJSON, "field19", Message.DBFIELD_SAYHI, "field6", "field7", Message.DBFIELD_CONVERLOCATIONJSON, "field9", "field11", "field12", "field13", "field14", "field15", "field16", "field18", "field17"};
        Object[] objArr = new Object[18];
        objArr[0] = str;
        objArr[1] = str2;
        objArr[2] = iVar.l();
        objArr[3] = iVar.f();
        objArr[4] = iVar.s();
        objArr[5] = iVar.k();
        objArr[6] = Integer.valueOf(iVar.e() ? 0 : 1);
        objArr[7] = iVar.h();
        objArr[8] = Integer.valueOf(iVar.a());
        objArr[9] = Boolean.valueOf(iVar.g());
        objArr[10] = Integer.valueOf(iVar.b());
        objArr[11] = iVar.c() != null ? iVar.c().f2875a : null;
        objArr[12] = iVar.c() != null ? iVar.c().c : null;
        objArr[13] = iVar.c() != null ? iVar.c().h : null;
        objArr[14] = iVar.d() != null ? iVar.d().appid : null;
        objArr[15] = iVar.d() != null ? iVar.d().appname : null;
        objArr[16] = iVar.d() != null ? iVar.d().getAction() : null;
        if (iVar.d() != null) {
            str3 = iVar.d().appicon;
        }
        objArr[17] = str3;
        a(strArr2, objArr, new String[]{"field5"}, new String[]{iVar.o()});
    }
}
