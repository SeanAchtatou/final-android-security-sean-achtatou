package com.immomo.momo.service.bean;

public enum w {
    Hot(0, "按热门排序"),
    Distance(1, "按距离排序"),
    Joincount(2, "按人数排序");
    
    private final int d;
    private final String e;

    private w(int i, String str) {
        this.d = i;
        this.e = str;
    }

    public static w a(int i) {
        for (w wVar : values()) {
            if (wVar.d == i) {
                return wVar;
            }
        }
        return Hot;
    }

    public static String[] c() {
        w[] values = values();
        String[] strArr = new String[values.length];
        for (int i = 0; i < values.length; i++) {
            strArr[i] = values[i].e;
        }
        return strArr;
    }

    public final int a() {
        return this.d;
    }

    public final String b() {
        return this.e;
    }
}
