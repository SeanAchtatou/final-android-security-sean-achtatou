package com.immomo.momo.service.bean;

import android.graphics.Bitmap;

public final class e {

    /* renamed from: a  reason: collision with root package name */
    public String f3023a;
    public int b;
    public String c;
    public String d;
    public String e = null;
    public boolean f = false;
    public String g;
    public Bitmap h;

    public final String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("momoid=" + this.f3023a + ", relation=" + this.b + ", weiboID=" + this.c + ", weiboName=" + this.d);
        return stringBuffer.toString();
    }
}
