package com.immomo.momo.service.bean;

import android.support.v4.b.a;
import com.immomo.momo.R;
import com.immomo.momo.g;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONObject;

public final class u extends al {
    private int A;
    private float B;
    private int C;
    private List D;
    private int E;

    /* renamed from: a  reason: collision with root package name */
    public String f3038a;
    public String b;
    public String c;
    public String d;
    public String e;
    public String f;
    public String g;
    public String h;
    public int i;
    public int j;
    public String k;
    public List l;
    public boolean m;
    public boolean n;
    public boolean o;
    public String p;
    public String q;
    public String r;
    public String s;
    public boolean t;
    public String u;
    public String v;
    public String w;
    public String x;
    public double y;
    public double z;

    public u() {
        this.j = 1;
        this.l = null;
        this.m = false;
        this.n = false;
        this.o = false;
        this.D = new ArrayList();
    }

    public u(String str) {
        this.j = 1;
        this.l = null;
        this.m = false;
        this.n = false;
        this.o = false;
        this.f3038a = str;
        this.D = new ArrayList();
    }

    public static u a(String str) {
        try {
            if (!a.a((CharSequence) str)) {
                JSONObject jSONObject = new JSONObject(str);
                u uVar = new u();
                uVar.f3038a = jSONObject.optString("id");
                uVar.b = jSONObject.optString("name");
                uVar.e = jSONObject.optString("address");
                uVar.d = jSONObject.optString("time");
                uVar.w = jSONObject.optString("action");
                uVar.E = jSONObject.optInt("mcount");
                return uVar;
            }
        } catch (Exception e2) {
        }
        return null;
    }

    public final float a() {
        return this.B;
    }

    public final void a(float f2) {
        this.B = f2;
        if (f2 < 0.0f) {
            this.h = g.a((int) R.string.profile_distance_unknown);
        } else {
            this.h = String.valueOf(a.a(f2 / 1000.0f)) + "km";
        }
    }

    public final void a(int i2) {
        if (i2 >= 0) {
            this.A = i2;
        } else {
            this.A = 0;
        }
    }

    public final void a(List list) {
        if (list == null) {
            list = new ArrayList();
        }
        this.D = list;
    }

    public final List b() {
        return this.D == null ? new ArrayList() : this.D;
    }

    public final void b(int i2) {
        if (i2 >= 0) {
            this.C = i2;
        } else {
            this.C = 0;
        }
    }

    public final String c() {
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("id", this.f3038a);
            jSONObject.put("name", this.b);
            jSONObject.put("address", this.e);
            jSONObject.put("time", this.d);
            jSONObject.put("action", this.w);
            jSONObject.put("mcount", this.E);
            return jSONObject.toString();
        } catch (Exception e2) {
            return null;
        }
    }

    public final void c(int i2) {
        if (i2 >= 0) {
            this.E = i2;
        } else {
            this.E = 0;
        }
    }

    public final int d() {
        return this.A;
    }

    public final int e() {
        return this.C;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        u uVar = (u) obj;
        return this.f3038a == null ? uVar.f3038a == null : this.f3038a.equals(uVar.f3038a);
    }

    public final int f() {
        return this.E;
    }

    public final String getLoadImageId() {
        return this.c;
    }

    public final int hashCode() {
        return (this.f3038a == null ? 0 : this.f3038a.hashCode()) + 31;
    }
}
