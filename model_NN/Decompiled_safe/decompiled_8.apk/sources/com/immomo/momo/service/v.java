package com.immomo.momo.service;

import android.support.v4.b.a;
import com.immomo.momo.g;
import com.immomo.momo.service.a.x;
import com.immomo.momo.service.a.y;
import com.immomo.momo.service.bean.Message;
import com.immomo.momo.service.bean.a.e;
import com.immomo.momo.service.bean.a.f;
import com.immomo.momo.util.m;
import java.io.Serializable;
import java.util.Iterator;
import java.util.List;

public final class v extends b {
    private y c = null;
    private x d = null;
    private aq e = null;
    private m f = new m(this);

    public v() {
        this.f2968a = g.d().h();
        this.c = new y(this.f2968a);
        this.d = new x(this.f2968a);
        this.e = new aq();
    }

    public final e a(String str) {
        e eVar = (e) this.c.a((Serializable) str);
        if (eVar != null) {
            eVar.b = this.e.b(eVar.c);
        }
        return eVar;
    }

    public final List a(String str, int i) {
        List<e> a2 = this.c.a(new String[]{"field10", "field6"}, new String[]{str, "1"}, "rowid", true, 0, i);
        for (e eVar : a2) {
            if (!a.a((CharSequence) eVar.c)) {
                eVar.b = this.e.b(eVar.c);
            }
        }
        return a2;
    }

    public final void a(e eVar) {
        if (this.c.c(eVar.f)) {
            this.c.b(eVar);
        } else {
            this.c.a(eVar);
        }
    }

    public final void a(f fVar) {
        if (a.a((CharSequence) fVar.i)) {
            throw new RuntimeException("comment.id is null");
        } else if (!this.d.c(fVar.i)) {
            this.d.a(fVar);
        } else {
            this.d.b(fVar);
        }
    }

    public final void a(List list, String str) {
        try {
            this.f2968a.beginTransaction();
            Iterator it = list.iterator();
            while (it.hasNext()) {
                f fVar = (f) it.next();
                if (a.a((CharSequence) fVar.i)) {
                    throw new RuntimeException("comment.id is null");
                }
                fVar.h = str;
                a(fVar);
                if (fVar.f2975a != null) {
                    this.e.g(fVar.f2975a);
                }
            }
            this.f2968a.setTransactionSuccessful();
        } catch (Exception e2) {
            this.f.a((Throwable) e2);
        } finally {
            this.f2968a.endTransaction();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.immomo.momo.service.a.y.a(com.immomo.momo.service.bean.a.e, android.database.Cursor):void
      com.immomo.momo.service.a.y.a(java.lang.Object, android.database.Cursor):void
      com.immomo.momo.service.a.b.a(android.database.Cursor, java.lang.String):int
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.String[]):android.database.Cursor
      com.immomo.momo.service.a.b.a(java.lang.String[], java.lang.String[]):java.util.List
      com.immomo.momo.service.a.b.a(java.lang.Object, android.database.Cursor):void
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object[]):void
      com.immomo.momo.service.a.b.a(java.lang.String[], java.lang.Object[]):void
      com.immomo.momo.service.a.b.a(java.lang.Object, java.io.Serializable):boolean
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.String):boolean
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object):void */
    public final void a(List list, String str, boolean z) {
        this.f2968a.beginTransaction();
        this.e.b().beginTransaction();
        if (z) {
            try {
                this.c.a("field10", (Object) str);
            } catch (Exception e2) {
                this.f.a((Throwable) e2);
                this.f2968a.endTransaction();
                this.e.b().endTransaction();
                return;
            } catch (Throwable th) {
                this.f2968a.endTransaction();
                this.e.b().endTransaction();
                throw th;
            }
        }
        Iterator it = list.iterator();
        while (it.hasNext()) {
            b((e) it.next());
        }
        this.f2968a.setTransactionSuccessful();
        this.e.b().setTransactionSuccessful();
        this.f2968a.endTransaction();
        this.e.b().endTransaction();
    }

    public final e b(String str) {
        List a2 = a(str, 1);
        if (a2 == null || a2.size() <= 0) {
            return null;
        }
        e eVar = (e) a2.get(0);
        if (a.a((CharSequence) eVar.c)) {
            return eVar;
        }
        eVar.b = this.e.b(eVar.c);
        return eVar;
    }

    public final void b(e eVar) {
        a(eVar);
        if (eVar.b != null) {
            this.e.d(eVar.b);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.service.a.b.a(java.lang.String[], java.lang.String[], java.lang.String, boolean):java.util.List
     arg types: [java.lang.String[], java.lang.String[], java.lang.String, int]
     candidates:
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.String[], java.lang.String[], java.lang.String[]):int
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.String, java.lang.String[], java.lang.String[]):java.lang.String
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object, java.lang.String, java.lang.Object[]):void
      com.immomo.momo.service.a.b.a(java.lang.String[], java.lang.Object[], java.lang.String[], java.lang.Object[]):void
      com.immomo.momo.service.a.b.a(java.lang.String[], java.lang.String[], java.lang.String, boolean):java.util.List */
    public final List c(String str) {
        List<f> a2 = this.d.a(new String[]{"field5"}, new String[]{str}, Message.DBFIELD_CONVERLOCATIONJSON, false);
        for (f fVar : a2) {
            if (!a.a((CharSequence) fVar.b)) {
                fVar.f2975a = this.e.b(fVar.b);
            }
        }
        return a2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.immomo.momo.service.a.x.a(com.immomo.momo.service.bean.a.f, android.database.Cursor):void
      com.immomo.momo.service.a.x.a(java.lang.Object, android.database.Cursor):void
      com.immomo.momo.service.a.b.a(android.database.Cursor, java.lang.String):int
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.String[]):android.database.Cursor
      com.immomo.momo.service.a.b.a(java.lang.String[], java.lang.String[]):java.util.List
      com.immomo.momo.service.a.b.a(java.lang.Object, android.database.Cursor):void
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object[]):void
      com.immomo.momo.service.a.b.a(java.lang.String[], java.lang.Object[]):void
      com.immomo.momo.service.a.b.a(java.lang.Object, java.io.Serializable):boolean
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.String):boolean
      com.immomo.momo.service.a.b.a(java.lang.String, java.lang.Object):void */
    public final void d(String str) {
        this.d.a("field5", (Object) str);
    }

    public final void e(String str) {
        this.d.b((Serializable) str);
    }

    public final void f(String str) {
        this.c.b((Serializable) str);
    }
}
