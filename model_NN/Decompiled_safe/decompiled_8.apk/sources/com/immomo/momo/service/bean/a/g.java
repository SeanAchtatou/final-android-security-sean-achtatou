package com.immomo.momo.service.bean.a;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public final class g {

    /* renamed from: a  reason: collision with root package name */
    public String f2976a;
    public String b;
    public String c;
    public String d;
    public String e;
    public Date f;
    public Date g;
    public int h = 0;
    public int i;
    public double j;
    public double k;
    public double l;
    public boolean m = false;
    public int n = 1;
    public int o = 2;
    public List p = new ArrayList();

    public g() {
    }

    public g(String str) {
        this.b = str;
    }

    public final boolean equals(Object obj) {
        return (obj == null || this.b == null || !(obj instanceof g)) ? super.equals(obj) : this.b.equals(((g) obj).b);
    }

    public final String toString() {
        return "GroupActivity [ptid=" + this.b + ", groupId=" + this.c + ", lat=" + this.j + ", lng=" + this.k + ", desc=" + this.d + ", joinList=" + this.p + ", member_count=" + this.n + ", address=" + this.e + ", need_phone=" + this.h + ", status=" + this.i + ", ctime=" + this.g + ", start_time=" + this.f + ", distance" + this.l + "]";
    }
}
