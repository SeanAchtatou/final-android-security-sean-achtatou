package com.immomo.momo.service;

import com.immomo.momo.service.bean.bf;
import java.util.Comparator;

final class ar implements Comparator {
    ar() {
    }

    public final /* bridge */ /* synthetic */ int compare(Object obj, Object obj2) {
        bf bfVar = (bf) obj;
        bf bfVar2 = (bf) obj2;
        if (bfVar.t == bfVar2.t) {
            return 0;
        }
        return bfVar.t > bfVar2.t ? -1 : 1;
    }
}
