package com.immomo.momo.service.a;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.ap;

public final class af extends b {
    public af() {
        super(g.d().e(), "mcaches");
    }

    public af(SQLiteDatabase sQLiteDatabase) {
        super(sQLiteDatabase, "mcaches");
    }

    private static void a(ap apVar, Cursor cursor) {
        apVar.f2993a = cursor.getString(cursor.getColumnIndex("filepath"));
        apVar.c = cursor.getInt(cursor.getColumnIndex("id"));
        apVar.b = cursor.getString(cursor.getColumnIndex("remoteid"));
        apVar.d = a(cursor.getLong(cursor.getColumnIndex("datetime")));
        apVar.e = cursor.getInt(cursor.getColumnIndex("count"));
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Cursor cursor) {
        ap apVar = new ap();
        a(apVar, cursor);
        return apVar;
    }

    public final void a(ap apVar) {
        b(new String[]{"filepath", "datetime", "count", "remoteid"}, new Object[]{apVar.f2993a, apVar.d, Integer.valueOf(apVar.e), apVar.b});
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void a(Object obj, Cursor cursor) {
        a((ap) obj, cursor);
    }
}
