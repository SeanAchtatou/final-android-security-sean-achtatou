package com.immomo.momo.service;

import android.database.sqlite.SQLiteDatabase;
import com.immomo.momo.g;
import com.immomo.momo.service.a.aa;
import java.io.Serializable;

public final class x extends b {
    private aa c;

    public x() {
        this(g.d().e());
    }

    private x(SQLiteDatabase sQLiteDatabase) {
        this.c = null;
        this.f2968a = sQLiteDatabase;
        this.c = new aa(sQLiteDatabase);
    }

    public final void a(com.immomo.momo.service.bean.a.g gVar) {
        if (!this.c.c(gVar.b)) {
            this.c.a(gVar);
        } else {
            this.c.b(gVar);
        }
    }

    public final void a(com.immomo.momo.service.bean.a.g gVar, String str) {
        this.c.a(gVar, str);
    }

    public final void a(String str) {
        this.c.b((Serializable) str);
    }

    public final com.immomo.momo.service.bean.a.g b(String str) {
        return (com.immomo.momo.service.bean.a.g) this.c.a((Serializable) str);
    }
}
