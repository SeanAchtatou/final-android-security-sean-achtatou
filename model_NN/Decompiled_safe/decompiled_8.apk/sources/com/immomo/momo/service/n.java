package com.immomo.momo.service;

import android.content.Context;
import com.immomo.momo.android.c.d;

final class n extends d {

    /* renamed from: a  reason: collision with root package name */
    private l f3051a;
    private int c;
    private /* synthetic */ j d;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public n(j jVar, Context context, l lVar, int i) {
        super(context);
        this.d = jVar;
        this.f3051a = lVar;
        this.c = i;
    }

    /* access modifiers changed from: protected */
    public final Object a(Object... objArr) {
        this.f3051a.v();
        return null;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        j jVar = this.d;
        j.a(this.f3051a, this.c);
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        this.b.a((Throwable) exc);
        j.a(this.d, this.f3051a, this.c, exc);
    }

    /* access modifiers changed from: protected */
    public final void a(Object obj) {
        super.a(obj);
        j.a(this.d, this.f3051a, this.c);
    }
}
