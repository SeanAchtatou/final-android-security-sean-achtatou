package com.immomo.momo.service.bean;

import android.content.Context;
import com.immomo.momo.util.ak;

public final class o {

    /* renamed from: a  reason: collision with root package name */
    public boolean f3033a = true;
    private ak b = null;

    private o(Context context, String str) {
        "d_" + str;
        this.b = ak.a(context, str);
    }

    public static o a(Context context, String str) {
        o oVar = new o(context, str);
        oVar.f3033a = oVar.b.a("discuss_ispush", Boolean.valueOf(oVar.f3033a));
        return oVar;
    }

    public final void a(String str, Object obj) {
        this.b.a(str, obj);
    }

    public final String toString() {
        return "isPush=" + this.f3033a;
    }
}
