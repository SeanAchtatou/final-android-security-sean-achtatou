package com.immomo.momo.service.a;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.immomo.momo.service.bean.Message;
import com.immomo.momo.util.a.a;
import org.json.JSONException;

public final class i extends b {
    public i(SQLiteDatabase sQLiteDatabase) {
        super(sQLiteDatabase, "discussmessages");
    }

    private static void a(Message message, Cursor cursor) {
        boolean z = true;
        int i = cursor.getInt(cursor.getColumnIndex(Message.DBFIELD_TYPE));
        if (cursor.getInt(cursor.getColumnIndex(Message.DBFIELD_RECEIVE)) != 1) {
            z = false;
        }
        message.contentType = i;
        message.receive = z;
        message.id = cursor.getInt(cursor.getColumnIndex(Message.DBFIELD_ID));
        message.msgId = cursor.getString(cursor.getColumnIndex(Message.DBFIELD_MSGID));
        message.msginfo = cursor.getString(cursor.getColumnIndex(Message.DBFIELD_MSMGINFO));
        message.failcount = cursor.getInt(cursor.getColumnIndex(Message.DBFIELD_FAILCOUNT));
        message.remoteId = cursor.getString(cursor.getColumnIndex(Message.DBFIELD_REMOTEID));
        message.status = cursor.getInt(cursor.getColumnIndex(Message.DBFIELD_STATUS));
        message.discussId = cursor.getString(cursor.getColumnIndex(Message.DBFIELD_GROUPID));
        message.chatType = 3;
        message.timestamp = a(cursor.getLong(cursor.getColumnIndex(Message.DBFIELD_TIME)));
        try {
            a.a(message.msginfo, message);
        } catch (JSONException e) {
        }
        if (i == 2) {
            message.parseDbLocationJson(cursor.getString(cursor.getColumnIndex(Message.DBFIELD_LOCATIONJSON)));
            message.parseDbConverLocationJson(cursor.getString(cursor.getColumnIndex(Message.DBFIELD_CONVERLOCATIONJSON)));
        }
    }

    private void a(String str, Message message) {
        int i = 1;
        Object[] objArr = new Object[11];
        objArr[0] = Integer.valueOf(message.failcount);
        objArr[1] = message.msginfo;
        objArr[2] = message.remoteId;
        objArr[3] = Integer.valueOf(message.status);
        objArr[4] = Long.valueOf(a(message.timestamp));
        if (!message.receive) {
            i = 0;
        }
        objArr[5] = Integer.valueOf(i);
        objArr[6] = Integer.valueOf(message.contentType);
        objArr[7] = message.getDbLocationjson();
        objArr[8] = message.getDbConverLocationJson();
        objArr[9] = message.discussId;
        objArr[10] = message.msgId;
        a(str, objArr);
    }

    private static Message b(Cursor cursor) {
        Message message = new Message();
        a(message, cursor);
        return message;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Cursor cursor) {
        return b(cursor);
    }

    public final void a(Message message) {
        StringBuilder sb = new StringBuilder();
        sb.append("insert into " + this.f2954a + " (").append("m_failcount, m_msginfo, m_remoteid, m_status, m_time, m_receive, m_type, field2, field3, field4, m_msgid) values(?,?,?,?,?,?,?,?, ?, ?, ?)");
        a(sb.toString(), message);
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void a(Object obj, Cursor cursor) {
        a((Message) obj, cursor);
    }

    public final void b(Message message) {
        StringBuilder sb = new StringBuilder();
        sb.append("update " + this.f2954a + " set ").append("m_failcount=?, m_msginfo=?, m_remoteid=?, m_status=?, m_time=?, m_receive=?, m_type=?, field2=?, field3=?, field4=? where m_msgid=?");
        a(sb.toString(), message);
    }
}
