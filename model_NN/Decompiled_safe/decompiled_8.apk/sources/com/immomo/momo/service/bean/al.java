package com.immomo.momo.service.bean;

import com.immomo.momo.android.c.g;

public class al implements aj {
    g callback = null;
    private String imageId;
    boolean loading = false;
    boolean loadingFailed = false;
    private boolean multipleDiaplay = false;
    private boolean url = false;

    public al() {
    }

    public al(String str) {
        this.imageId = str;
    }

    public al(String str, boolean z) {
        this.imageId = str;
        this.url = true;
    }

    public g getImageCallback() {
        return this.callback;
    }

    public String getLoadImageId() {
        return this.imageId;
    }

    public boolean isImageLoading() {
        return this.loading;
    }

    public boolean isImageLoadingFailed() {
        return this.loadingFailed;
    }

    public boolean isImageMultipleDiaplay() {
        return this.multipleDiaplay;
    }

    public boolean isImageUrl() {
        return this.url;
    }

    public void setImageCallback(g gVar) {
        this.callback = gVar;
    }

    public void setImageId(String str) {
        this.imageId = str;
    }

    public void setImageLoadFailed(boolean z) {
        this.loadingFailed = z;
    }

    public void setImageLoading(boolean z) {
        this.loading = z;
    }

    public void setImageMultipleDiaplay(boolean z) {
        this.multipleDiaplay = z;
    }

    public void setImageUrl(boolean z) {
        this.url = z;
    }
}
