package com.immomo.momo.service.bean;

import android.support.v4.b.a;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.view.ao;
import com.immomo.momo.g;
import com.immomo.momo.protocol.a.v;
import com.immomo.momo.service.bean.c.d;
import com.immomo.momo.util.jni.Codec;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import mm.purchasesdk.PurchaseCode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class bf extends al {
    public int A = 0;
    public int B = 2;
    public String C = PoiTypeDef.All;
    public String D = PoiTypeDef.All;
    public String E = PoiTypeDef.All;
    public String F = PoiTypeDef.All;
    public String G = PoiTypeDef.All;
    public String H = PoiTypeDef.All;
    public int I;
    public String J = PoiTypeDef.All;
    public String K = PoiTypeDef.All;
    public String L = PoiTypeDef.All;
    public String M = PoiTypeDef.All;
    public String N = PoiTypeDef.All;
    public List O = null;
    public String P = PoiTypeDef.All;
    public String Q = PoiTypeDef.All;
    public String R = PoiTypeDef.All;
    public double S;
    public double T;
    public double U;
    public boolean V = false;
    public String W;
    public long X = 0;
    public Date Y;
    public String Z;

    /* renamed from: a  reason: collision with root package name */
    public String f3011a;
    public Date aA = null;
    public Date aB = null;
    public Date aC = null;
    public Date aD = null;
    public at aE = null;
    public Date aF = null;
    public int aG = Integer.MAX_VALUE;
    public int aH = 0;
    public int aI = PurchaseCode.LOADCHANNEL_ERR;
    public int aJ = 0;
    public String aK;
    public String aL;
    public int aM;
    public List aN = null;
    public String aO;
    public int aP;
    public String aQ;
    public String aR;
    private long aS = 0;
    private String aT = PoiTypeDef.All;
    private String aU = PoiTypeDef.All;
    private float aV = -1.0f;
    private boolean aW = false;
    private Date aX;
    private long aY = 0;
    private ao aZ = new ao();
    public String aa;
    public String ab;
    public boolean ac = false;
    public boolean ad = false;
    public String[] ae = null;
    public String af;
    public String ag;
    public ab ah;
    public int ai = 0;
    public Date aj;
    public long ak;
    public int al;
    public String am;
    public boolean an = false;
    public String ao;
    public boolean ap = false;
    public String aq;
    public boolean ar = false;
    public String as;
    public boolean at = false;
    public boolean au = false;
    public String av;
    public boolean aw;
    public Date ax;
    public Date ay;
    public Date az;
    public String b = PoiTypeDef.All;
    private ao ba = new ao();
    public String c = PoiTypeDef.All;
    public String d = PoiTypeDef.All;
    public String e = PoiTypeDef.All;
    public boolean f = false;
    public boolean g = false;
    public String h;
    public String i = PoiTypeDef.All;
    public String j = PoiTypeDef.All;
    public String k = PoiTypeDef.All;
    public String l;
    public String m = PoiTypeDef.All;
    public String n = PoiTypeDef.All;
    public String o = PoiTypeDef.All;
    public String p = PoiTypeDef.All;
    public String q = PoiTypeDef.All;
    public String r = PoiTypeDef.All;
    public String s = PoiTypeDef.All;
    public int t;
    public int u = 0;
    public int v = 0;
    public int w = 0;
    public int x = 0;
    public int y = 0;
    public int z = 0;

    public bf() {
    }

    public bf(String str) {
        this.h = str;
    }

    public final long a() {
        return this.aS;
    }

    public final void a(float f2) {
        this.aV = f2;
        if (f2 == -2.0f) {
            this.Z = g.a((int) R.string.profile_distance_hide);
        } else if (f2 >= 0.0f) {
            this.Z = String.valueOf(a.a(f2 / 1000.0f)) + "km";
        } else {
            this.Z = g.a((int) R.string.profile_distance_unknown);
        }
    }

    public final void a(long j2) {
        if (j2 > 0) {
            a(new Date(j2));
        } else {
            a((Date) null);
        }
    }

    public final void a(Date date) {
        this.aX = date;
        this.aa = a.a(date);
        if (date != null) {
            this.aS = date.getTime();
        } else {
            this.aS = 0;
        }
    }

    public final void a(boolean z2) {
        this.aW = z2;
    }

    public final boolean a(String str) {
        if (!a.a((CharSequence) str) && g.q() != null && this.h.equals(g.q().h)) {
            try {
                JSONObject jSONObject = new JSONObject(Codec.b(str));
                this.S = jSONObject.getDouble("lat");
                this.T = jSONObject.getDouble("lng");
                this.U = jSONObject.getDouble("acc");
                return true;
            } catch (Exception e2) {
            }
        }
        return false;
    }

    public final void b(long j2) {
        this.aY = j2;
    }

    public final void b(String str) {
        this.aT = str;
        this.aZ.a(str);
    }

    public final boolean b() {
        return this.aW;
    }

    public final void c(String str) {
        this.aU = str;
        this.ba.a(str);
    }

    public final boolean c() {
        return this.al > 0;
    }

    public final float d() {
        return this.aV;
    }

    public final void d(String str) {
        if (!a.a((CharSequence) str)) {
            this.aK = str;
            try {
                JSONObject jSONObject = new JSONObject(str);
                if (jSONObject.has("tieba")) {
                    JSONObject jSONObject2 = jSONObject.getJSONObject("tieba");
                    this.aL = jSONObject2.optString("name");
                    this.aM = jSONObject2.optInt("count", 0);
                    jSONObject2.optString("desc");
                    this.aN = new ArrayList();
                    JSONArray optJSONArray = jSONObject2.optJSONArray("list");
                    if (optJSONArray != null) {
                        for (int i2 = 0; i2 < optJSONArray.length(); i2++) {
                            d dVar = new d();
                            dVar.f3019a = optJSONArray.getJSONObject(i2).optString(v.h);
                            dVar.b = optJSONArray.getJSONObject(i2).optString(v.f);
                            dVar.p = optJSONArray.getJSONObject(i2).optInt(v.g);
                            this.aN.add(dVar);
                        }
                    }
                }
                if (jSONObject.has("event")) {
                    JSONObject jSONObject3 = jSONObject.getJSONObject("event");
                    this.aO = jSONObject3.optString("name");
                    this.aP = jSONObject3.optInt("count", 0);
                    this.aQ = jSONObject3.optString("desc");
                }
            } catch (JSONException e2) {
                e2.printStackTrace();
            }
        }
    }

    public final Date e() {
        return this.aX;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        bf bfVar = (bf) obj;
        return this.h == null ? bfVar.h == null : this.h.equals(bfVar.h);
    }

    public final String f() {
        return a.a(this.aX);
    }

    public final String g() {
        if (g.q() != null && this.h.equals(g.q().h)) {
            try {
                JSONObject jSONObject = new JSONObject();
                jSONObject.put("lat", this.S);
                jSONObject.put("lng", this.T);
                jSONObject.put("acc", this.U);
                return Codec.c(jSONObject.toString());
            } catch (Exception e2) {
            }
        }
        return PoiTypeDef.All;
    }

    public String getLoadImageId() {
        if (this.ae == null || this.ae.length <= 0) {
            return null;
        }
        return this.ae[0];
    }

    public final String h() {
        return !a.a(this.l) ? this.l : !a.a(this.i) ? this.i : this.h;
    }

    public int hashCode() {
        return (this.h == null ? 0 : this.h.hashCode()) + 31;
    }

    public final String i() {
        return !a.a(this.i) ? this.i : this.h;
    }

    public final boolean j() {
        return this.ae != null && this.ae.length > 1;
    }

    public final int k() {
        if (this.ae != null) {
            return this.ae.length;
        }
        return 0;
    }

    public final String l() {
        return this.aT;
    }

    public final String m() {
        return this.aU;
    }

    public final ao n() {
        return this.ba;
    }

    public final boolean o() {
        return (this.ae != null && this.ae.length > 1) || !a.a(this.aT) || !a.a(this.N) || !a.a(this.L) || !a.a(this.C) || !a.a(this.Q) || !a.a(this.F) || !a.a(this.D);
    }

    public final boolean p() {
        return (this.aG == -1 || (this.aG >= 0 && this.aG <= 3)) && this.an;
    }

    public final boolean q() {
        return !p() && this.an;
    }

    public final long r() {
        return this.aY;
    }

    public String toString() {
        return "User [momoid=" + this.h + ", name=" + this.i + ", loc_timesec=" + this.aS + ", group_role=" + this.aJ + ", regtype=" + this.B + ", birthday=" + this.J + ", regtime=" + this.ay + ", email=" + this.G + ", bindemail=" + this.aw + ", sex=" + this.H + ", distance=" + this.aV + ", agotime=" + this.aa + ", job=" + this.N + ", weibo_remain_day=" + this.aG + ", geo_fixedTYpe=" + this.aH + ", industry=" + this.M + ", relation=" + this.P + "]";
    }
}
