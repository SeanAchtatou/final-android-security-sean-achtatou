package com.immomo.momo.service.a;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.b.a;
import com.immomo.momo.service.bean.Message;
import com.immomo.momo.service.bean.ay;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public final class at extends b {
    private static Set d = new HashSet();

    public at(SQLiteDatabase sQLiteDatabase) {
        super(sQLiteDatabase, "sites", "gs_siteid");
    }

    private static void a(ay ayVar, Cursor cursor) {
        boolean z = true;
        ayVar.f3001a = cursor.getString(cursor.getColumnIndex("gs_siteid"));
        ayVar.c = cursor.getInt(cursor.getColumnIndex(Message.DBFIELD_SAYHI));
        ayVar.f = cursor.getString(cursor.getColumnIndex(Message.DBFIELD_GROUPID));
        ayVar.b = cursor.getString(cursor.getColumnIndex("field20"));
        ayVar.d = cursor.getInt(cursor.getColumnIndex(Message.DBFIELD_LOCATIONJSON));
        ayVar.p = cursor.getInt(cursor.getColumnIndex("field14")) == 1;
        ayVar.a(cursor.getFloat(cursor.getColumnIndex(Message.DBFIELD_CONVERLOCATIONJSON)));
        ayVar.j = cursor.getInt(cursor.getColumnIndex("field7"));
        ayVar.l = cursor.getInt(cursor.getColumnIndex("field15"));
        ayVar.k = cursor.getInt(cursor.getColumnIndex("field13"));
        ayVar.o = cursor.getInt(cursor.getColumnIndex("field10"));
        ayVar.t = a(cursor.getLong(cursor.getColumnIndex("field11")));
        ayVar.m = cursor.getString(cursor.getColumnIndex("field8"));
        ayVar.n = a.b(cursor.getString(cursor.getColumnIndex("field9")), ",");
        ayVar.r = cursor.getString(cursor.getColumnIndex("field12"));
        ayVar.s = cursor.getString(cursor.getColumnIndex("field18"));
        if (cursor.getInt(cursor.getColumnIndex("field19")) != 1) {
            z = false;
        }
        ayVar.u = z;
        if (!a.a((CharSequence) ayVar.f3001a)) {
            d.add(ayVar.f3001a);
        }
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Cursor cursor) {
        ay ayVar = new ay();
        a(ayVar, cursor);
        return ayVar;
    }

    public final void a(ay ayVar) {
        int i = 1;
        String[] strArr = {"gs_siteid", Message.DBFIELD_SAYHI, Message.DBFIELD_GROUPID, "field20", Message.DBFIELD_LOCATIONJSON, "field14", Message.DBFIELD_CONVERLOCATIONJSON, "field5", "field6", "field7", "field15", "field11", "field8", "field9", "field10", "field12", "field13", "field16", "field18", "field19"};
        Object[] objArr = new Object[20];
        objArr[0] = ayVar.f3001a;
        objArr[1] = Integer.valueOf(ayVar.c);
        objArr[2] = ayVar.f;
        objArr[3] = ayVar.b;
        objArr[4] = Integer.valueOf(ayVar.d);
        if (!ayVar.p) {
            i = 0;
        }
        objArr[5] = Integer.valueOf(i);
        objArr[6] = Float.valueOf(ayVar.a());
        objArr[7] = Double.valueOf(ayVar.h);
        objArr[8] = Double.valueOf(ayVar.i);
        objArr[9] = Integer.valueOf(ayVar.j);
        objArr[10] = Integer.valueOf(ayVar.l);
        objArr[11] = ayVar.t;
        objArr[12] = ayVar.m;
        objArr[13] = a.a(ayVar.n, ",");
        objArr[14] = Integer.valueOf(ayVar.o);
        objArr[15] = ayVar.r;
        objArr[16] = Integer.valueOf(ayVar.k);
        objArr[17] = new Date();
        objArr[18] = ayVar.s;
        objArr[19] = Boolean.valueOf(ayVar.u);
        b(strArr, objArr);
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void a(Object obj, Cursor cursor) {
        a((ay) obj, cursor);
    }
}
