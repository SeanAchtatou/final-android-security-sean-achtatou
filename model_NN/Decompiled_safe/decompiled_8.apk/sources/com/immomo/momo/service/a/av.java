package com.immomo.momo.service.a;

import android.database.Cursor;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.Message;
import com.immomo.momo.service.bean.ba;

public final class av extends b {
    public av() {
        super(g.d().g(), "splashscreen2");
    }

    private static void a(ba baVar, Cursor cursor) {
        baVar.a(cursor.getInt(cursor.getColumnIndex(Message.DBFIELD_ID)));
        baVar.a(cursor.getString(cursor.getColumnIndex("bg_url")));
        baVar.b(cursor.getString(cursor.getColumnIndex("ft_url")));
        baVar.c(cursor.getString(cursor.getColumnIndex("cr_url")));
        baVar.d(c(cursor, Message.DBFIELD_CONVERLOCATIONJSON));
        baVar.b(a(b(cursor, "endtime")));
        baVar.a(a(b(cursor, "starttime")));
        baVar.b(a(cursor, Message.DBFIELD_SAYHI));
        baVar.c(a(cursor, Message.DBFIELD_LOCATIONJSON));
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Cursor cursor) {
        ba baVar = new ba();
        a(baVar, cursor);
        return baVar;
    }

    public final void a(ba baVar) {
        b(new String[]{"starttime", "endtime", "bg_url", "cr_url", "ft_url", Message.DBFIELD_SAYHI, Message.DBFIELD_LOCATIONJSON, Message.DBFIELD_CONVERLOCATIONJSON}, new Object[]{baVar.g(), baVar.h(), baVar.a(), baVar.e(), baVar.d(), Integer.valueOf(baVar.b()), Integer.valueOf(baVar.c()), baVar.f()});
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void a(Object obj, Cursor cursor) {
        a((ba) obj, cursor);
    }
}
