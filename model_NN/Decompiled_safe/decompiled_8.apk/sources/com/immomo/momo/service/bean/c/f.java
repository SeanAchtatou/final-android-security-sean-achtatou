package com.immomo.momo.service.bean.c;

import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.service.bean.al;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

public final class f extends al {

    /* renamed from: a  reason: collision with root package name */
    public String f3021a;
    public String b;
    public String c;
    public String d;
    public int e;
    public List f;
    public boolean g;

    public final JSONObject a() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("icon", this.d);
            jSONObject.put("name", this.b);
            jSONObject.put("id", this.f3021a);
            jSONObject.put("count", this.e);
            jSONObject.put("top_tiebas", this.c);
        } catch (JSONException e2) {
        }
        return jSONObject;
    }

    public final void a(JSONObject jSONObject) {
        this.f3021a = jSONObject.optString("id", PoiTypeDef.All);
        this.b = jSONObject.optString("name", PoiTypeDef.All);
        this.d = jSONObject.optString("icon", PoiTypeDef.All);
        this.e = jSONObject.optInt("count");
        this.c = jSONObject.optString("top_tiebas");
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        f fVar = (f) obj;
        return this.f3021a == null ? fVar.f3021a == null : this.f3021a.equals(fVar.f3021a);
    }

    public final String getLoadImageId() {
        return this.d;
    }

    public final int hashCode() {
        return (this.f3021a == null ? 0 : this.f3021a.hashCode()) + 31;
    }

    public final boolean isImageUrl() {
        return true;
    }
}
