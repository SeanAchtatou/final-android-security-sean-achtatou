package com.immomo.momo.service.bean;

import android.support.v4.b.a;
import com.amap.mapapi.poisearch.PoiTypeDef;

public final class bh extends al {

    /* renamed from: a  reason: collision with root package name */
    public String f3013a;
    public long b = 0;
    public long c = 0;

    public bh() {
        setImageUrl(true);
    }

    public bh(String str) {
        this.f3013a = str;
        setImageUrl(true);
    }

    public final String a() {
        if (a.f(this.f3013a)) {
            return this.f3013a.replace("_preview", PoiTypeDef.All);
        }
        return null;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        bh bhVar = (bh) obj;
        return this.f3013a == null ? bhVar.f3013a == null : this.f3013a.equals(bhVar.f3013a);
    }

    public final String getLoadImageId() {
        return "http://img.immomo.com/m/chat/android/" + this.f3013a + ".png";
    }
}
