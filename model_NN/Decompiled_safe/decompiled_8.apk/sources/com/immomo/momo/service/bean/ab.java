package com.immomo.momo.service.bean;

import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.view.MSmallEmoteTextView;
import com.immomo.momo.android.view.ao;
import com.immomo.momo.g;
import com.immomo.momo.plugin.b.a;
import java.util.Date;
import org.json.JSONException;
import org.json.JSONObject;

public class ab extends al {

    /* renamed from: a  reason: collision with root package name */
    private Date f2982a;
    public bf b;
    public String c;
    public ay d;
    public String e;
    public String f;
    public int g = 0;
    public String h;
    public int i;
    public String j;
    public String k;
    public a l;
    public String m;
    public GameApp n;
    public u o;
    public ac p;
    private String[] q;
    private al[] r;
    private String s;
    private String t;
    private float u;
    private ao v = new ao();

    public ab() {
    }

    public ab(String str) {
        this.h = str;
    }

    public final String a() {
        return this.t;
    }

    public final void a(float f2) {
        this.u = f2;
        if (f2 < 0.0f) {
            this.m = g.a((int) R.string.profile_distance_unknown);
        } else {
            this.m = String.valueOf(android.support.v4.b.a.a(f2 / 1000.0f)) + "km";
        }
    }

    public final void a(String str) {
        this.t = str;
        if (android.support.v4.b.a.f(str)) {
            this.l = new a(str);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.b.a.a(java.util.Date, boolean):java.lang.String
     arg types: [java.util.Date, int]
     candidates:
      android.support.v4.b.a.a(android.graphics.Bitmap, float):android.graphics.Bitmap
      android.support.v4.b.a.a(android.graphics.Bitmap, java.io.File):android.graphics.Bitmap
      android.support.v4.b.a.a(int, int):java.lang.String
      android.support.v4.b.a.a(java.util.Collection, java.lang.String):java.lang.String
      android.support.v4.b.a.a(java.lang.Object[], java.lang.String):java.lang.String
      android.support.v4.b.a.a(java.lang.String, java.lang.String):org.json.JSONObject
      android.support.v4.b.a.a(float, int[]):void
      android.support.v4.b.a.a(android.app.Activity, int):void
      android.support.v4.b.a.a(android.view.View, java.lang.Runnable):void
      android.support.v4.b.a.a(java.io.File, java.io.File):void
      android.support.v4.b.a.a(java.io.File, java.util.zip.ZipInputStream):void
      android.support.v4.b.a.a(java.lang.Object, java.lang.StringBuilder):void
      android.support.v4.b.a.a(android.content.Context, java.lang.String):boolean
      android.support.v4.b.a.a(com.immomo.a.a.d.b, com.immomo.momo.service.bean.Message):boolean
      android.support.v4.b.a.a(byte[], java.lang.String):byte[]
      android.support.v4.b.a.a(byte[], byte[]):byte[]
      android.support.v4.b.a.a(int[], boolean):byte[]
      android.support.v4.b.a.a(byte[], boolean):int[]
      android.support.v4.b.a.a(int, float):void
      android.support.v4.b.a.a(java.lang.Object, com.immomo.a.a.f):void
      android.support.v4.view.bf.a(int, float):void
      com.immomo.a.a.f.a(java.lang.Object, com.immomo.a.a.f):void
      android.support.v4.b.a.a(java.util.Date, boolean):java.lang.String */
    public final void a(Date date) {
        this.f2982a = date;
        this.f = android.support.v4.b.a.a(date, false);
    }

    public final void a(JSONObject jSONObject) {
        bf bfVar = new bf();
        bfVar.h = jSONObject.optString("ouserid");
        bfVar.i = jSONObject.optString("ousername");
        bfVar.ae = new String[]{jSONObject.optString("ouseravator")};
        b(jSONObject.optString("content"));
        a((float) jSONObject.optInt("distance"));
        this.b = bfVar;
        this.c = bfVar.h;
        this.h = jSONObject.optString("id");
        this.j = jSONObject.optString("emotionname");
        this.k = jSONObject.optString("emotionlibrary");
        a(jSONObject.optString("emotionbody"));
        this.q = com.immomo.a.a.f.a.a(jSONObject.optString("images"), ",");
        long optLong = jSONObject.optLong("time");
        if (optLong > 0) {
            a(new Date(optLong));
        }
    }

    public final void a(String[] strArr) {
        this.q = strArr;
        if (strArr != null && strArr.length > 0) {
            this.r = new al[strArr.length];
            for (int i2 = 0; i2 < strArr.length; i2++) {
                this.r[i2] = new al(strArr[i2]);
            }
        }
    }

    public final String b() {
        return this.s;
    }

    public final void b(String str) {
        this.s = str;
        this.v.a(str);
        this.v.b(MSmallEmoteTextView.getInstance().a(str));
    }

    public final ao c() {
        return this.v;
    }

    public final Date d() {
        return this.f2982a;
    }

    public final float e() {
        return this.u;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        ab abVar = (ab) obj;
        return this.h == null ? abVar.h == null : this.h.equals(abVar.h);
    }

    public final String f() {
        try {
            JSONObject jSONObject = new JSONObject();
            if (this.b != null) {
                jSONObject.put("ousername", this.b.i);
                jSONObject.put("ouseravator", this.b.getLoadImageId());
            }
            jSONObject.put("ouserid", this.c);
            jSONObject.put("content", this.s);
            jSONObject.put("time", this.f2982a == null ? 0 : this.f2982a.getTime());
            jSONObject.put("images", com.immomo.a.a.f.a.a(this.q, ","));
            jSONObject.put("distance", (double) this.u);
            jSONObject.put("emotionname", this.j);
            jSONObject.put("emotionbody", this.t);
            jSONObject.put("emotionlibrary", this.k);
            jSONObject.put("id", this.h);
            return jSONObject.toString();
        } catch (JSONException e2) {
            return null;
        }
    }

    public final String g() {
        return (this.d == null || !android.support.v4.b.a.f(this.d.f)) ? this.m : String.valueOf(this.d.f) + " " + this.m;
    }

    public String getLoadImageId() {
        return (this.q == null || this.q.length <= 0) ? PoiTypeDef.All : this.q[0];
    }

    public final boolean h() {
        return this.q != null && this.q.length > 0;
    }

    public int hashCode() {
        return (this.h == null ? 0 : this.h.hashCode()) + 31;
    }

    public final int i() {
        if (this.q != null) {
            return this.q.length;
        }
        return 0;
    }

    public final String[] j() {
        return this.q;
    }

    public String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("feedid=" + this.h + ", cotent=" + this.s + ", status=" + this.i + ", owner=" + this.c + ", commentsCount=" + this.g);
        return stringBuffer.toString();
    }
}
