package com.immomo.momo.service.bean;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class l {

    /* renamed from: a  reason: collision with root package name */
    private List f3030a = new ArrayList();
    private List b = new ArrayList();
    private Map c = new HashMap();

    public final m a(int i) {
        return (m) this.c.get(Integer.valueOf(i));
    }

    public final List a() {
        return this.f3030a;
    }

    public final void a(m mVar) {
        this.f3030a.add(mVar);
        this.c.put(Integer.valueOf(mVar.f3031a), mVar);
    }

    public final List b() {
        return this.b;
    }

    public final void b(m mVar) {
        this.b.add(mVar);
    }

    public final int c() {
        return this.f3030a.size();
    }

    public final int d() {
        return this.b.size();
    }
}
