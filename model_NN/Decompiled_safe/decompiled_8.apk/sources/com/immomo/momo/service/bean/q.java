package com.immomo.momo.service.bean;

import android.graphics.Bitmap;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.a;
import com.immomo.momo.g;
import com.immomo.momo.util.l;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

public class q {
    private static int C = g.a(47.0f);
    public s A;
    public String B;
    private al D;
    private al E;
    private al F;
    private al G;

    /* renamed from: a  reason: collision with root package name */
    public String f3035a;
    public String b;
    public int c;
    public String d;
    public String e;
    public String f;
    public String g;
    public int h;
    public int i;
    public String j;
    public int k;
    public String l;
    public String m;
    public String n = PoiTypeDef.All;
    public bf o;
    public String p;
    public boolean q;
    public String r;
    public String s;
    public boolean t;
    public boolean u;
    public List v = new ArrayList();
    public long w;
    public boolean x = false;
    public boolean y = false;
    public String z;

    public q() {
    }

    public q(String str) {
        this.f3035a = str;
    }

    public static File a(String str) {
        File file = new File(a.v(), String.valueOf(str) + "/middle");
        if (!file.exists()) {
            file.mkdirs();
        }
        return file;
    }

    public static File a(String str, String str2) {
        File file = new File(a.v(), String.valueOf(str2) + "/large");
        if (!file.exists()) {
            file.mkdirs();
        }
        return new File(file, String.valueOf(str) + "_");
    }

    public static void a(File file, File file2) {
        Bitmap l2;
        BufferedOutputStream bufferedOutputStream;
        if (file.exists() && (l2 = android.support.v4.b.a.l(file.getPath())) != null) {
            Bitmap a2 = android.support.v4.b.a.a(l2, C, C);
            l2.recycle();
            if (a2 != null) {
                try {
                    bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(file2));
                    try {
                        a2.compress(Bitmap.CompressFormat.PNG, 85, bufferedOutputStream);
                        a2.recycle();
                        android.support.v4.b.a.a(bufferedOutputStream);
                    } catch (Exception e2) {
                        e = e2;
                        try {
                            new l("Emotion").a((Throwable) e);
                            android.support.v4.b.a.a(bufferedOutputStream);
                        } catch (Throwable th) {
                            th = th;
                            android.support.v4.b.a.a(bufferedOutputStream);
                            throw th;
                        }
                    }
                } catch (Exception e3) {
                    e = e3;
                    bufferedOutputStream = null;
                    new l("Emotion").a((Throwable) e);
                    android.support.v4.b.a.a(bufferedOutputStream);
                } catch (Throwable th2) {
                    th = th2;
                    bufferedOutputStream = null;
                    android.support.v4.b.a.a(bufferedOutputStream);
                    throw th;
                }
            }
        }
    }

    public static File b(String str, String str2) {
        return new File(a(str2), String.valueOf(str) + ".png_");
    }

    public final al a() {
        if (this.D == null || !this.D.getLoadImageId().equals(this.e)) {
            if (!android.support.v4.b.a.a((CharSequence) this.e) && this.e.startsWith("http://")) {
                this.D = new al(this.e);
                this.D.setImageUrl(true);
            } else if (this.D != null) {
                this.D = null;
            }
        }
        return this.D;
    }

    public final al b() {
        if (this.E == null || !this.E.getLoadImageId().equals(this.f)) {
            if (!android.support.v4.b.a.a((CharSequence) this.f)) {
                if (this.f.startsWith("http://")) {
                    this.E = new al(this.f);
                    this.E.setImageUrl(true);
                }
            } else if (this.E == null) {
                this.E = new al("http://et.momocdn.com/et/" + this.f3035a + "/profile/cover_s.png");
                this.E.setImageUrl(true);
            }
        }
        return this.E;
    }

    public final al c() {
        if (this.F == null || !this.F.getLoadImageId().equals(this.r)) {
            if (!android.support.v4.b.a.a((CharSequence) this.r)) {
                this.F = new al(this.r);
                this.F.setImageUrl(true);
            } else if (this.F != null) {
                this.F = null;
            }
        }
        return this.F;
    }

    public final al d() {
        if (this.G == null || !this.G.getLoadImageId().equals(this.p)) {
            if (!android.support.v4.b.a.a((CharSequence) this.p) && this.p.startsWith("http://")) {
                this.G = new al(this.p);
                this.G.setImageUrl(true);
            } else if (this.G != null) {
                this.G = null;
            }
        }
        return this.G;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        q qVar = (q) obj;
        return this.f3035a == null ? qVar.f3035a == null : this.f3035a.equals(qVar.f3035a);
    }

    public int hashCode() {
        return (this.f3035a == null ? 0 : this.f3035a.hashCode()) + 31;
    }

    public String toString() {
        return "Emotion [id=" + this.f3035a + ", displayName=" + this.b + ", downloaded=" + this.t + ", enable=" + this.u + "]";
    }
}
