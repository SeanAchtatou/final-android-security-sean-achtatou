package com.immomo.momo.service.a;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.b.a;
import com.immomo.momo.service.bean.Message;
import com.immomo.momo.service.bean.n;

public final class h extends b {
    public h(SQLiteDatabase sQLiteDatabase) {
        super(sQLiteDatabase, "discuss", "did");
    }

    private static void a(n nVar, Cursor cursor) {
        nVar.f = cursor.getString(cursor.getColumnIndex("did"));
        nVar.b = cursor.getString(cursor.getColumnIndex(Message.DBFIELD_SAYHI));
        nVar.m = cursor.getString(cursor.getColumnIndex("field10"));
        nVar.i = a(cursor.getLong(cursor.getColumnIndex("field7")));
        nVar.c = cursor.getString(cursor.getColumnIndex(Message.DBFIELD_LOCATIONJSON));
        nVar.f3032a = a.b(cursor.getString(cursor.getColumnIndex(Message.DBFIELD_CONVERLOCATIONJSON)), ",");
        nVar.g = cursor.getInt(cursor.getColumnIndex("field5"));
        nVar.j = cursor.getInt(cursor.getColumnIndex("field8"));
        nVar.k = cursor.getInt(cursor.getColumnIndex("field9"));
        nVar.h = cursor.getInt(cursor.getColumnIndex("field6"));
        nVar.e = a.b(cursor.getString(cursor.getColumnIndex(Message.DBFIELD_GROUPID)), ",");
        nVar.l = c(cursor, "field11");
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Cursor cursor) {
        n nVar = new n();
        a(nVar, cursor);
        return nVar;
    }

    public final void a(n nVar) {
        b(new String[]{"did", Message.DBFIELD_SAYHI, "field10", "field7", Message.DBFIELD_GROUPID, Message.DBFIELD_LOCATIONJSON, Message.DBFIELD_CONVERLOCATIONJSON, "field5", "field8", "field9", "field6"}, new Object[]{nVar.f, nVar.b, nVar.m, Long.valueOf(a(nVar.i)), a.a(nVar.e, ","), nVar.c, a.a(nVar.f3032a, ","), Integer.valueOf(nVar.g), Integer.valueOf(nVar.j), Integer.valueOf(nVar.k), Integer.valueOf(nVar.h)});
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void a(Object obj, Cursor cursor) {
        a((n) obj, cursor);
    }
}
