package com.immomo.momo.service;

import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.a;
import com.immomo.momo.g;
import com.immomo.momo.service.a.l;
import com.immomo.momo.service.bean.Message;
import com.immomo.momo.service.bean.bd;
import com.immomo.momo.service.bean.q;
import com.immomo.momo.service.bean.r;
import com.immomo.momo.service.bean.s;
import com.immomo.momo.util.m;
import com.immomo.momo.util.u;
import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public final class o {

    /* renamed from: a  reason: collision with root package name */
    private l f3052a;
    private m b;

    public o() {
        this.f3052a = null;
        this.b = new m(this);
        this.f3052a = new l(g.d().e());
    }

    private static r a(JSONObject jSONObject) {
        return new r(jSONObject.optString("d"), jSONObject.optString("n"), jSONObject.getString("l"), jSONObject.getString("s"), jSONObject.optString("k"), jSONObject.optString("e", PoiTypeDef.All));
    }

    private static JSONObject a(r rVar) {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("d", rVar.a());
        jSONObject.put("n", rVar.b());
        jSONObject.put("s", rVar.e());
        jSONObject.put("l", rVar.d());
        jSONObject.put("k", rVar.f());
        jSONObject.put("e", rVar.c());
        return jSONObject;
    }

    public static void a(List list, String str) {
        BufferedWriter bufferedWriter;
        ArrayList<r> arrayList = new ArrayList<>();
        arrayList.addAll(list);
        try {
            File file = new File(a.q(), String.valueOf(str) + "_list");
            file.createNewFile();
            bufferedWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file)));
            try {
                JSONArray jSONArray = new JSONArray();
                for (r a2 : arrayList) {
                    jSONArray.put(a(a2));
                }
                bufferedWriter.write(jSONArray.toString());
                u.a(str, arrayList);
                android.support.v4.b.a.a(bufferedWriter);
            } catch (Throwable th) {
                th = th;
            }
        } catch (Throwable th2) {
            th = th2;
            bufferedWriter = null;
            android.support.v4.b.a.a(bufferedWriter);
            throw th;
        }
    }

    /* access modifiers changed from: private */
    public static void c(List list, String str) {
        BufferedWriter bufferedWriter;
        ArrayList<q> arrayList = new ArrayList<>();
        arrayList.addAll(list);
        try {
            File file = new File(a.q(), str);
            file.createNewFile();
            bufferedWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file)));
            try {
                JSONArray jSONArray = new JSONArray();
                if (list != null) {
                    for (q qVar : arrayList) {
                        JSONObject jSONObject = new JSONObject();
                        jSONObject.put("eid", qVar.f3035a);
                        jSONObject.put("name", qVar.b);
                        jSONObject.put("cover", qVar.e);
                        jSONObject.put("scover", qVar.f);
                        jSONObject.put("own", qVar.q);
                        jSONObject.put("label", qVar.i);
                        jSONObject.put("enable", qVar.u);
                        jSONObject.put("down", qVar.t);
                        jSONObject.put("price", qVar.l);
                        jSONObject.put("fprice", qVar.n);
                        jSONObject.put("onumber", qVar.A != null ? qVar.A.c : -1);
                        jSONObject.put("tnumber", qVar.A != null ? qVar.A.b : -1);
                        jSONObject.put("labelFlag", qVar.i);
                        jSONObject.put("labelString", qVar.j);
                        jSONObject.put("displaynameFlag", qVar.c);
                        jSONObject.put("displayNameString", qVar.d);
                        jSONArray.put(jSONObject);
                    }
                }
                bufferedWriter.write(jSONArray.toString());
                bufferedWriter.flush();
                android.support.v4.b.a.a(bufferedWriter);
            } catch (Throwable th) {
                th = th;
                android.support.v4.b.a.a(bufferedWriter);
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            bufferedWriter = null;
            android.support.v4.b.a.a(bufferedWriter);
            throw th;
        }
    }

    private List d(String str) {
        BufferedInputStream bufferedInputStream;
        BufferedInputStream bufferedInputStream2 = null;
        ArrayList arrayList = new ArrayList();
        try {
            File file = new File(a.q(), str);
            if (file.exists()) {
                bufferedInputStream = new BufferedInputStream(new FileInputStream(file));
                try {
                    byte[] b2 = android.support.v4.b.a.b(bufferedInputStream);
                    if (b2 != null && b2.length > 0) {
                        JSONArray jSONArray = new JSONArray(new String(b2));
                        for (int i = 0; i < jSONArray.length(); i++) {
                            JSONObject jSONObject = jSONArray.getJSONObject(i);
                            q qVar = new q();
                            qVar.f3035a = jSONObject.optString("eid");
                            qVar.b = jSONObject.optString("name");
                            qVar.e = jSONObject.optString("cover");
                            qVar.q = jSONObject.optBoolean("own");
                            qVar.u = jSONObject.optBoolean("enable");
                            qVar.t = jSONObject.optBoolean("down");
                            qVar.i = jSONObject.optInt("label");
                            qVar.f = jSONObject.optString("scover");
                            qVar.l = jSONObject.optString("price");
                            qVar.n = jSONObject.optString("fprice", PoiTypeDef.All);
                            qVar.i = jSONObject.optInt("labelFlag");
                            qVar.j = jSONObject.optString("labelString");
                            qVar.c = jSONObject.optInt("displaynameFlag");
                            qVar.d = jSONObject.optString("displayNameString");
                            int optInt = jSONObject.optInt("tnumber", -1);
                            int optInt2 = jSONObject.optInt("onumber", -1);
                            if (!(optInt == -1 || optInt2 == -1)) {
                                qVar.A = new s();
                                qVar.A.c = optInt2;
                                qVar.A.b = optInt;
                            }
                            arrayList.add(qVar);
                        }
                    }
                } catch (Exception e) {
                    Exception exc = e;
                    bufferedInputStream2 = bufferedInputStream;
                    e = exc;
                    try {
                        this.b.a((Throwable) e);
                        android.support.v4.b.a.a((Closeable) bufferedInputStream2);
                        return arrayList;
                    } catch (Throwable th) {
                        th = th;
                        android.support.v4.b.a.a((Closeable) bufferedInputStream2);
                        throw th;
                    }
                } catch (Throwable th2) {
                    Throwable th3 = th2;
                    bufferedInputStream2 = bufferedInputStream;
                    th = th3;
                    android.support.v4.b.a.a((Closeable) bufferedInputStream2);
                    throw th;
                }
            } else {
                bufferedInputStream = null;
            }
            android.support.v4.b.a.a((Closeable) bufferedInputStream);
        } catch (Exception e2) {
            e = e2;
            this.b.a((Throwable) e);
            android.support.v4.b.a.a((Closeable) bufferedInputStream2);
            return arrayList;
        }
        return arrayList;
    }

    public final q a(String str) {
        List e = e();
        int indexOf = e.indexOf(new q(str));
        if (indexOf >= 0) {
            return (q) e.get(indexOf);
        }
        List d = d();
        int indexOf2 = d.indexOf(new q(str));
        if (indexOf2 >= 0) {
            return (q) d.get(indexOf2);
        }
        return null;
    }

    public final List a() {
        return d("hot");
    }

    public final void a(q qVar) {
        if (qVar.u) {
            List e = e();
            int indexOf = e.indexOf(qVar);
            if (indexOf >= 0) {
                e.remove(indexOf);
            }
            e.add(0, qVar);
            i(e);
            List d = d();
            int indexOf2 = d.indexOf(qVar);
            if (indexOf2 >= 0) {
                d.remove(indexOf2);
                h(d);
                return;
            }
            return;
        }
        List d2 = d();
        int indexOf3 = d2.indexOf(qVar);
        if (indexOf3 >= 0) {
            d2.remove(indexOf3);
            d2.add(indexOf3, qVar);
        } else {
            d2.add(qVar);
        }
        h(d2);
        List e2 = e();
        int indexOf4 = e2.indexOf(qVar);
        if (indexOf4 >= 0) {
            e2.remove(indexOf4);
            i(e2);
        }
    }

    public final void a(List list) {
        this.f3052a.d();
        try {
            Iterator it = list.iterator();
            while (it.hasNext()) {
                q qVar = (q) it.next();
                if (!this.f3052a.c(qVar.f3035a)) {
                    this.f3052a.a(qVar);
                }
            }
            this.f3052a.f();
            c(list, "hot");
        } catch (Exception e) {
            this.b.a((Throwable) e);
        } finally {
            this.f3052a.e();
        }
    }

    public final q b(String str) {
        return (q) this.f3052a.a((Serializable) str);
    }

    public final List b() {
        return d("free");
    }

    public final void b(q qVar) {
        int i = -1;
        String str = null;
        if (!this.f3052a.c(qVar.f3035a)) {
            this.f3052a.a(qVar);
            return;
        }
        l lVar = this.f3052a;
        String[] strArr = {Message.DBFIELD_LOCATIONJSON, "field26", "field25", Message.DBFIELD_SAYHI, Message.DBFIELD_GROUPID, Message.DBFIELD_CONVERLOCATIONJSON, "field5", "field20", "field21", "field22", "field27", "field11", "field12", "field13", "field14", "field15", "field16", "field17", "field24", "field18", "field19", "field31", "field28", "field32", "field30", "field29", "field33", "field35", "field37", "field36"};
        Object[] objArr = new Object[30];
        objArr[0] = qVar.e;
        objArr[1] = qVar.m;
        objArr[2] = qVar.l;
        objArr[3] = qVar.b;
        objArr[4] = Integer.valueOf(qVar.h);
        objArr[5] = qVar.g;
        objArr[6] = Integer.valueOf(qVar.i);
        objArr[7] = Boolean.valueOf(qVar.x);
        objArr[8] = Boolean.valueOf(qVar.y);
        objArr[9] = qVar.z;
        objArr[10] = qVar.n;
        objArr[11] = qVar.o != null ? qVar.o.h : null;
        objArr[12] = qVar.o != null ? qVar.o.i : null;
        objArr[13] = qVar.o != null ? qVar.o.l() : null;
        objArr[14] = qVar.r;
        objArr[15] = qVar.s;
        objArr[16] = Boolean.valueOf(qVar.q);
        objArr[17] = qVar.p;
        objArr[18] = qVar.B;
        objArr[19] = qVar.f;
        objArr[20] = Long.valueOf(qVar.w);
        objArr[21] = qVar.A != null ? qVar.A.e : null;
        if (qVar.A != null) {
            str = qVar.A.d;
        }
        objArr[22] = str;
        objArr[23] = Integer.valueOf(qVar.A != null ? qVar.A.c : -1);
        objArr[24] = Integer.valueOf(qVar.A != null ? qVar.A.b : -1);
        if (qVar.A != null) {
            i = qVar.A.f3037a;
        }
        objArr[25] = Integer.valueOf(i);
        objArr[26] = Boolean.valueOf(qVar.A != null ? qVar.A.f : false);
        objArr[27] = qVar.j;
        objArr[28] = Integer.valueOf(qVar.c);
        objArr[29] = qVar.d;
        lVar.a(strArr, objArr, new String[]{"eid"}, new Object[]{qVar.f3035a});
    }

    public final void b(List list) {
        this.f3052a.d();
        try {
            Iterator it = list.iterator();
            while (it.hasNext()) {
                q qVar = (q) it.next();
                if (!this.f3052a.c(qVar.f3035a)) {
                    this.f3052a.a(qVar);
                }
            }
            this.f3052a.f();
        } catch (Exception e) {
            this.b.a((Throwable) e);
        } finally {
            this.f3052a.e();
        }
    }

    public final List c() {
        return d("news");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0071, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:?, code lost:
        r7.b.a((java.lang.Throwable) r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x008a, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x008b, code lost:
        r6 = r1;
        r1 = r0;
        r0 = r6;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x008a A[ExcHandler: all (r1v3 'th' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:8:0x003f] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.List c(java.lang.String r8) {
        /*
            r7 = this;
            boolean r0 = com.immomo.momo.util.u.c(r8)
            if (r0 == 0) goto L_0x000d
            java.lang.Object r0 = com.immomo.momo.util.u.b(r8)
            java.util.List r0 = (java.util.List) r0
        L_0x000c:
            return r0
        L_0x000d:
            r1 = 0
            java.util.ArrayList r2 = new java.util.ArrayList
            r2.<init>()
            java.io.File r3 = new java.io.File     // Catch:{ Exception -> 0x008f }
            java.io.File r0 = com.immomo.momo.a.q()     // Catch:{ Exception -> 0x008f }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x008f }
            java.lang.String r5 = java.lang.String.valueOf(r8)     // Catch:{ Exception -> 0x008f }
            r4.<init>(r5)     // Catch:{ Exception -> 0x008f }
            java.lang.String r5 = "_list"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x008f }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x008f }
            r3.<init>(r0, r4)     // Catch:{ Exception -> 0x008f }
            boolean r0 = r3.exists()     // Catch:{ Exception -> 0x008f }
            if (r0 == 0) goto L_0x0091
            java.io.BufferedInputStream r0 = new java.io.BufferedInputStream     // Catch:{ Exception -> 0x008f }
            java.io.FileInputStream r4 = new java.io.FileInputStream     // Catch:{ Exception -> 0x008f }
            r4.<init>(r3)     // Catch:{ Exception -> 0x008f }
            r0.<init>(r4)     // Catch:{ Exception -> 0x008f }
            byte[] r1 = android.support.v4.b.a.b(r0)     // Catch:{ Exception -> 0x0078, all -> 0x008a }
            if (r1 == 0) goto L_0x005a
            int r3 = r1.length     // Catch:{ Exception -> 0x0078, all -> 0x008a }
            if (r3 <= 0) goto L_0x005a
            org.json.JSONArray r4 = new org.json.JSONArray     // Catch:{ Exception -> 0x0078, all -> 0x008a }
            java.lang.String r3 = new java.lang.String     // Catch:{ Exception -> 0x0078, all -> 0x008a }
            r3.<init>(r1)     // Catch:{ Exception -> 0x0078, all -> 0x008a }
            r4.<init>(r3)     // Catch:{ Exception -> 0x0078, all -> 0x008a }
            r1 = 0
            r3 = r1
        L_0x0054:
            int r1 = r4.length()     // Catch:{ Exception -> 0x0078, all -> 0x008a }
            if (r3 < r1) goto L_0x0062
        L_0x005a:
            android.support.v4.b.a.a(r0)
        L_0x005d:
            com.immomo.momo.util.u.a(r8, r2)
            r0 = r2
            goto L_0x000c
        L_0x0062:
            org.json.JSONObject r1 = r4.getJSONObject(r3)     // Catch:{ Exception -> 0x0071, all -> 0x008a }
            com.immomo.momo.service.bean.r r1 = a(r1)     // Catch:{ Exception -> 0x0071, all -> 0x008a }
            r2.add(r1)     // Catch:{ Exception -> 0x0071, all -> 0x008a }
        L_0x006d:
            int r1 = r3 + 1
            r3 = r1
            goto L_0x0054
        L_0x0071:
            r1 = move-exception
            com.immomo.momo.util.m r5 = r7.b     // Catch:{ Exception -> 0x0078, all -> 0x008a }
            r5.a(r1)     // Catch:{ Exception -> 0x0078, all -> 0x008a }
            goto L_0x006d
        L_0x0078:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
        L_0x007c:
            com.immomo.momo.util.m r3 = r7.b     // Catch:{ all -> 0x0085 }
            r3.a(r0)     // Catch:{ all -> 0x0085 }
            android.support.v4.b.a.a(r1)
            goto L_0x005d
        L_0x0085:
            r0 = move-exception
        L_0x0086:
            android.support.v4.b.a.a(r1)
            throw r0
        L_0x008a:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
            goto L_0x0086
        L_0x008f:
            r0 = move-exception
            goto L_0x007c
        L_0x0091:
            r0 = r1
            goto L_0x005a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.immomo.momo.service.o.c(java.lang.String):java.util.List");
    }

    public final void c(List list) {
        this.f3052a.d();
        try {
            Iterator it = list.iterator();
            while (it.hasNext()) {
                q qVar = (q) it.next();
                if (!this.f3052a.c(qVar.f3035a)) {
                    this.f3052a.a(qVar);
                }
            }
            this.f3052a.f();
            c(list, "news");
        } catch (Exception e) {
            this.b.a((Throwable) e);
        } finally {
            this.f3052a.e();
        }
    }

    public final List d() {
        if (u.c("emotionminedisablelist")) {
            return (List) u.b("emotionminedisablelist");
        }
        List d = d("minedis");
        u.a("emotionminedisablelist", d);
        return d;
    }

    public final void d(List list) {
        this.f3052a.d();
        try {
            Iterator it = list.iterator();
            while (it.hasNext()) {
                q qVar = (q) it.next();
                if (!this.f3052a.c(qVar.f3035a)) {
                    this.f3052a.a(qVar);
                }
            }
            this.f3052a.f();
            c(list, "free");
        } catch (Exception e) {
            this.b.a((Throwable) e);
        } finally {
            this.f3052a.e();
        }
    }

    public final List e() {
        if (u.c("emotionmineenablelist")) {
            return (List) u.b("emotionmineenablelist");
        }
        List d = d("mine");
        u.a("emotionmineenablelist", d);
        return d;
    }

    public final void e(List list) {
        this.f3052a.d();
        try {
            Iterator it = list.iterator();
            while (it.hasNext()) {
                q qVar = (q) it.next();
                if (!this.f3052a.c(qVar.f3035a)) {
                    this.f3052a.a(qVar);
                }
            }
            this.f3052a.f();
        } catch (Exception e) {
            this.b.a((Throwable) e);
        } finally {
            this.f3052a.e();
        }
    }

    public final void f(List list) {
        this.f3052a.d();
        try {
            this.f3052a.d();
            try {
                List e = e();
                Iterator it = list.iterator();
                while (it.hasNext()) {
                    q qVar = (q) it.next();
                    int indexOf = e.indexOf(qVar);
                    if (indexOf >= 0) {
                        qVar.t = ((q) e.get(indexOf)).t;
                        qVar.A = ((q) e.get(indexOf)).A;
                    }
                    if (!this.f3052a.c(qVar.f3035a)) {
                        this.f3052a.a(qVar);
                    }
                }
                i(list);
                this.f3052a.f();
                this.f3052a.e();
            } catch (Exception e2) {
                this.b.a((Throwable) e2);
            } catch (Throwable th) {
                this.f3052a.e();
                throw th;
            }
            this.f3052a.f();
            this.f3052a.e();
        } catch (Exception e3) {
            this.b.a((Throwable) e3);
        } finally {
            this.f3052a.e();
        }
    }

    public final void g(List list) {
        this.f3052a.d();
        try {
            Iterator it = list.iterator();
            while (it.hasNext()) {
                q qVar = (q) it.next();
                if (!this.f3052a.c(qVar.f3035a)) {
                    this.f3052a.a(qVar);
                }
            }
            h(list);
            this.f3052a.f();
        } catch (Exception e) {
            this.b.a((Throwable) e);
        } finally {
            this.f3052a.e();
        }
    }

    public final void h(List list) {
        u.a("emotionminedisablelist", list);
        com.immomo.momo.android.c.u.b().execute(new p(this, list));
    }

    public final void i(List list) {
        u.a("emotionmineenablelist", list);
        com.immomo.momo.android.c.u.b().execute(new q(this, list));
        if (bd.f3009a) {
            bd.b().a();
        }
    }

    public final void j(List list) {
        BufferedWriter bufferedWriter;
        JSONArray jSONArray = new JSONArray();
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= list.size()) {
                BufferedWriter bufferedWriter2 = null;
                try {
                    bufferedWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(new File(a.q(), "usedlist"))));
                    try {
                        bufferedWriter.write(jSONArray.toString());
                        bufferedWriter.flush();
                        android.support.v4.b.a.a(bufferedWriter);
                        return;
                    } catch (Exception e) {
                        e = e;
                        try {
                            this.b.a((Throwable) e);
                            android.support.v4.b.a.a(bufferedWriter);
                            return;
                        } catch (Throwable th) {
                            th = th;
                            bufferedWriter2 = bufferedWriter;
                            android.support.v4.b.a.a(bufferedWriter2);
                            throw th;
                        }
                    }
                } catch (Exception e2) {
                    e = e2;
                    bufferedWriter = null;
                    this.b.a((Throwable) e);
                    android.support.v4.b.a.a(bufferedWriter);
                    return;
                } catch (Throwable th2) {
                    th = th2;
                    android.support.v4.b.a.a(bufferedWriter2);
                    throw th;
                }
            } else {
                try {
                    jSONArray.put(a((r) list.get(i2)));
                } catch (Exception e3) {
                    this.b.a((Throwable) e3);
                }
                i = i2 + 1;
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x006b, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:?, code lost:
        r8.b.a((java.lang.Throwable) r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0084, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0085, code lost:
        r7 = r1;
        r1 = r0;
        r0 = r7;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0084 A[ExcHandler: all (r1v3 'th' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:5:0x0020] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void k(java.util.List r9) {
        /*
            r8 = this;
            r1 = 0
            java.util.List r3 = r8.e()     // Catch:{ Exception -> 0x0089 }
            java.io.File r2 = new java.io.File     // Catch:{ Exception -> 0x0089 }
            java.io.File r0 = com.immomo.momo.a.q()     // Catch:{ Exception -> 0x0089 }
            java.lang.String r4 = "usedlist"
            r2.<init>(r0, r4)     // Catch:{ Exception -> 0x0089 }
            boolean r0 = r2.exists()     // Catch:{ Exception -> 0x0089 }
            if (r0 == 0) goto L_0x008b
            java.io.BufferedInputStream r0 = new java.io.BufferedInputStream     // Catch:{ Exception -> 0x0089 }
            java.io.FileInputStream r4 = new java.io.FileInputStream     // Catch:{ Exception -> 0x0089 }
            r4.<init>(r2)     // Catch:{ Exception -> 0x0089 }
            r0.<init>(r4)     // Catch:{ Exception -> 0x0089 }
            byte[] r1 = android.support.v4.b.a.b(r0)     // Catch:{ Exception -> 0x0072, all -> 0x0084 }
            if (r1 == 0) goto L_0x003b
            int r2 = r1.length     // Catch:{ Exception -> 0x0072, all -> 0x0084 }
            if (r2 <= 0) goto L_0x003b
            org.json.JSONArray r4 = new org.json.JSONArray     // Catch:{ Exception -> 0x0072, all -> 0x0084 }
            java.lang.String r2 = new java.lang.String     // Catch:{ Exception -> 0x0072, all -> 0x0084 }
            r2.<init>(r1)     // Catch:{ Exception -> 0x0072, all -> 0x0084 }
            r4.<init>(r2)     // Catch:{ Exception -> 0x0072, all -> 0x0084 }
            r1 = 0
            r2 = r1
        L_0x0035:
            int r1 = r4.length()     // Catch:{ Exception -> 0x0072, all -> 0x0084 }
            if (r2 < r1) goto L_0x003f
        L_0x003b:
            android.support.v4.b.a.a(r0)
        L_0x003e:
            return
        L_0x003f:
            org.json.JSONObject r1 = r4.getJSONObject(r2)     // Catch:{ Exception -> 0x006b, all -> 0x0084 }
            com.immomo.momo.service.bean.r r5 = a(r1)     // Catch:{ Exception -> 0x006b, all -> 0x0084 }
            java.lang.String r6 = r5.d()     // Catch:{ Exception -> 0x006b, all -> 0x0084 }
            boolean r6 = com.immomo.a.a.f.a.a(r6)     // Catch:{ Exception -> 0x006b, all -> 0x0084 }
            if (r6 != 0) goto L_0x0067
            com.immomo.momo.service.bean.q r6 = new com.immomo.momo.service.bean.q     // Catch:{ Exception -> 0x006b, all -> 0x0084 }
            java.lang.String r5 = r5.d()     // Catch:{ Exception -> 0x006b, all -> 0x0084 }
            r6.<init>(r5)     // Catch:{ Exception -> 0x006b, all -> 0x0084 }
            boolean r5 = r3.contains(r6)     // Catch:{ Exception -> 0x006b, all -> 0x0084 }
            if (r5 == 0) goto L_0x0067
            com.immomo.momo.service.bean.r r1 = a(r1)     // Catch:{ Exception -> 0x006b, all -> 0x0084 }
            r9.add(r1)     // Catch:{ Exception -> 0x006b, all -> 0x0084 }
        L_0x0067:
            int r1 = r2 + 1
            r2 = r1
            goto L_0x0035
        L_0x006b:
            r1 = move-exception
            com.immomo.momo.util.m r5 = r8.b     // Catch:{ Exception -> 0x0072, all -> 0x0084 }
            r5.a(r1)     // Catch:{ Exception -> 0x0072, all -> 0x0084 }
            goto L_0x0067
        L_0x0072:
            r1 = move-exception
            r7 = r1
            r1 = r0
            r0 = r7
        L_0x0076:
            com.immomo.momo.util.m r2 = r8.b     // Catch:{ all -> 0x007f }
            r2.a(r0)     // Catch:{ all -> 0x007f }
            android.support.v4.b.a.a(r1)
            goto L_0x003e
        L_0x007f:
            r0 = move-exception
        L_0x0080:
            android.support.v4.b.a.a(r1)
            throw r0
        L_0x0084:
            r1 = move-exception
            r7 = r1
            r1 = r0
            r0 = r7
            goto L_0x0080
        L_0x0089:
            r0 = move-exception
            goto L_0x0076
        L_0x008b:
            r0 = r1
            goto L_0x003b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.immomo.momo.service.o.k(java.util.List):void");
    }
}
