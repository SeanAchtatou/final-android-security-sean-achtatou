package com.immomo.momo.service.bean.a;

import android.content.Context;
import com.immomo.momo.util.ak;

public final class i {

    /* renamed from: a  reason: collision with root package name */
    public boolean f2978a = true;
    private ak b = null;

    private i(Context context, String str) {
        "g_" + str;
        this.b = ak.a(context, str);
    }

    public static i a(Context context, String str) {
        i iVar = new i(context, str);
        iVar.f2978a = iVar.b.a("group_ispush", Boolean.valueOf(iVar.f2978a));
        return iVar;
    }

    public final void a(String str, Object obj) {
        this.b.a(str, obj);
    }

    public final String toString() {
        return "isPush=" + this.f2978a;
    }
}
