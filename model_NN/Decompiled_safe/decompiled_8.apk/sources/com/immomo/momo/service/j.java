package com.immomo.momo.service;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.a.a.b.d;
import com.immomo.momo.R;
import com.immomo.momo.a.a;
import com.immomo.momo.android.activity.DraftPublishActivity;
import com.immomo.momo.android.broadcast.q;
import com.immomo.momo.f;
import com.immomo.momo.g;
import com.immomo.momo.service.a.k;
import com.immomo.momo.service.bean.Message;
import com.immomo.momo.util.u;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import org.json.JSONException;

public class j {

    /* renamed from: a  reason: collision with root package name */
    private k f3048a = new k(g.d().e());

    private j() {
    }

    public static j a() {
        if (u.c(j.class.getName())) {
            return (j) u.b(j.class.getName());
        }
        j jVar = new j();
        u.a(j.class.getName(), jVar);
        return jVar;
    }

    private void a(int i, int i2, String str) {
        if (i2 == 3) {
            this.f3048a.b(Integer.valueOf(i));
            return;
        }
        this.f3048a.a(new String[]{Message.DBFIELD_LOCATIONJSON, "field6"}, new Object[]{Integer.valueOf(i2), str}, new String[]{Message.DBFIELD_ID}, new String[]{new StringBuilder(String.valueOf(i)).toString()});
    }

    static /* synthetic */ void a(j jVar, l lVar, int i) {
        String str;
        String str2;
        Context c = g.c();
        NotificationManager notificationManager = (NotificationManager) c.getSystemService("notification");
        notificationManager.cancel(lVar.w().b);
        f fVar = new f(c);
        Intent intent = new Intent(c, DraftPublishActivity.class);
        intent.addFlags(335544320);
        String str3 = "(发送成功)" + lVar.w().e;
        switch (i) {
            case 1:
            case 4:
                str = "话题发送成功";
                str2 = "陌陌吧话题";
                break;
            case 2:
                str = "留言板发送成功";
                str2 = "留言板";
                break;
            case 3:
                str = "群空间发送成功";
                str2 = "群空间";
                break;
            default:
                return;
        }
        fVar.a(PendingIntent.getActivity(c, lVar.w().b, intent, 134217728));
        fVar.c(str);
        fVar.a(str2);
        fVar.b(str3);
        fVar.b(17301641);
        fVar.a(System.currentTimeMillis());
        notificationManager.notify(lVar.w().b, fVar.d());
        jVar.a(lVar.w().b, 3, PoiTypeDef.All);
        new Handler().postDelayed(new k(lVar.w().b), 3000);
        Intent intent2 = new Intent(q.f2360a);
        intent2.putExtra("draftid", lVar.w().b);
        c.sendBroadcast(intent2);
    }

    static /* synthetic */ void a(j jVar, l lVar, int i, Exception exc) {
        String str;
        String str2;
        Context c = g.c();
        NotificationManager notificationManager = (NotificationManager) c.getSystemService("notification");
        notificationManager.cancel(lVar.w().b);
        f fVar = new f(c);
        Intent intent = new Intent(c, DraftPublishActivity.class);
        intent.addFlags(335544320);
        String str3 = "(发送失败,点击重发)" + lVar.w().e;
        switch (i) {
            case 1:
            case 4:
                str = "话题发送失败";
                str2 = "陌陌吧话题";
                break;
            case 2:
                str = "留言板发送失败";
                str2 = "留言板";
                break;
            case 3:
                str = "群空间发送失败";
                str2 = "群空间";
                break;
            default:
                return;
        }
        fVar.a(PendingIntent.getActivity(c, lVar.w().b, intent, 134217728));
        fVar.c(str);
        fVar.a(str2);
        fVar.b(str3);
        fVar.b(17301624);
        fVar.a(System.currentTimeMillis());
        fVar.b();
        notificationManager.notify(lVar.w().b, fVar.d());
        jVar.a(lVar.w().b, 2, exc instanceof a ? !android.support.v4.b.a.a(exc.getMessage()) ? exc.getMessage() : g.a((int) R.string.errormsg_server) : exc instanceof d ? exc.getMessage() : exc instanceof JSONException ? g.a((int) R.string.errormsg_dataerror) : g.a((int) R.string.errormsg_client));
        Intent intent2 = new Intent(q.b);
        intent2.putExtra("draftid", lVar.w().b);
        c.sendBroadcast(intent2);
    }

    static /* synthetic */ void a(l lVar, int i) {
        String str;
        Context c = g.c();
        f fVar = new f(c);
        Intent intent = new Intent(c, DraftPublishActivity.class);
        intent.addFlags(335544320);
        String str2 = "(发送中)" + lVar.w().e;
        switch (i) {
            case 1:
            case 4:
                str = "陌陌吧话题";
                break;
            case 2:
                str = "留言板";
                break;
            case 3:
                str = "群空间";
                break;
            default:
                return;
        }
        fVar.a(PendingIntent.getActivity(c, lVar.w().b, intent, 134217728));
        fVar.c();
        fVar.c("正在发送...");
        fVar.a(str);
        fVar.b(str2);
        fVar.b(17301640);
        fVar.a(System.currentTimeMillis());
        ((NotificationManager) c.getSystemService("notification")).notify(lVar.w().b, fVar.d());
        Intent intent2 = new Intent(q.c);
        intent2.putExtra("draftid", lVar.w().b);
        c.sendBroadcast(intent2);
    }

    private void b(l lVar, int i) {
        m w = lVar.w();
        w.d = new Date();
        w.f = i;
        w.c = 1;
        if (w.b <= 0) {
            w.b = (int) (System.currentTimeMillis() / 1000);
        }
        if (this.f3048a.c(Integer.valueOf(w.b))) {
            k kVar = this.f3048a;
            HashMap hashMap = new HashMap();
            hashMap.put(Message.DBFIELD_SAYHI, w.f3050a);
            hashMap.put(Message.DBFIELD_LOCATIONJSON, Integer.valueOf(w.c));
            hashMap.put(Message.DBFIELD_GROUPID, w.e);
            hashMap.put("field5", w.d);
            hashMap.put(Message.DBFIELD_CONVERLOCATIONJSON, Integer.valueOf(w.f));
            hashMap.put("field6", w.g);
            kVar.a(hashMap, new String[]{Message.DBFIELD_ID}, new Object[]{Integer.valueOf(w.b)});
        } else {
            this.f3048a.a(w);
        }
        new n(this, g.c(), lVar, i).execute(new Object[0]);
    }

    public final m a(int i) {
        return (m) this.f3048a.a((Serializable) Integer.valueOf(i));
    }

    public final void a(l lVar) {
        b(lVar, 4);
    }

    public final List b() {
        return this.f3048a.a("rowid");
    }

    public final void b(l lVar) {
        b(lVar, 1);
    }

    public final void c(l lVar) {
        b(lVar, 2);
    }

    public final void d(l lVar) {
        b(lVar, 3);
    }
}
