package com.immomo.momo.service.bean.a;

import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.view.MSmallEmoteTextView;
import com.immomo.momo.android.view.ao;
import com.immomo.momo.g;
import com.immomo.momo.plugin.b.a;
import com.immomo.momo.service.bean.al;
import com.immomo.momo.service.bean.bf;
import java.util.Date;

public final class e extends al {

    /* renamed from: a  reason: collision with root package name */
    public String f2974a;
    public bf b;
    public String c;
    public String d;
    public int e = 0;
    public String f;
    public String g;
    public int h;
    public String i;
    public String j;
    public a k;
    public String l;
    public int m;
    public boolean n = false;
    public int o = 0;
    public boolean p = false;
    private Date q;
    private String[] r;
    private al[] s;
    private String t;
    private String u;
    private float v;
    private ao w = new ao();

    public e() {
    }

    public e(String str) {
        this.f = str;
    }

    public final String a() {
        return this.t;
    }

    public final void a(float f2) {
        this.v = f2;
        if (f2 < 0.0f) {
            this.l = g.a((int) R.string.profile_distance_unknown);
        } else {
            this.l = String.valueOf(android.support.v4.b.a.a(f2 / 1000.0f)) + "km";
        }
    }

    public final void a(String str) {
        this.t = str;
        this.w.a(str);
        this.w.b(MSmallEmoteTextView.getInstance().a(str));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.b.a.a(java.util.Date, boolean):java.lang.String
     arg types: [java.util.Date, int]
     candidates:
      android.support.v4.b.a.a(android.graphics.Bitmap, float):android.graphics.Bitmap
      android.support.v4.b.a.a(android.graphics.Bitmap, java.io.File):android.graphics.Bitmap
      android.support.v4.b.a.a(int, int):java.lang.String
      android.support.v4.b.a.a(java.util.Collection, java.lang.String):java.lang.String
      android.support.v4.b.a.a(java.lang.Object[], java.lang.String):java.lang.String
      android.support.v4.b.a.a(java.lang.String, java.lang.String):org.json.JSONObject
      android.support.v4.b.a.a(float, int[]):void
      android.support.v4.b.a.a(android.app.Activity, int):void
      android.support.v4.b.a.a(android.view.View, java.lang.Runnable):void
      android.support.v4.b.a.a(java.io.File, java.io.File):void
      android.support.v4.b.a.a(java.io.File, java.util.zip.ZipInputStream):void
      android.support.v4.b.a.a(java.lang.Object, java.lang.StringBuilder):void
      android.support.v4.b.a.a(android.content.Context, java.lang.String):boolean
      android.support.v4.b.a.a(com.immomo.a.a.d.b, com.immomo.momo.service.bean.Message):boolean
      android.support.v4.b.a.a(byte[], java.lang.String):byte[]
      android.support.v4.b.a.a(byte[], byte[]):byte[]
      android.support.v4.b.a.a(int[], boolean):byte[]
      android.support.v4.b.a.a(byte[], boolean):int[]
      android.support.v4.b.a.a(int, float):void
      android.support.v4.b.a.a(java.lang.Object, com.immomo.a.a.f):void
      android.support.v4.view.bf.a(int, float):void
      com.immomo.a.a.f.a(java.lang.Object, com.immomo.a.a.f):void
      android.support.v4.b.a.a(java.util.Date, boolean):java.lang.String */
    public final void a(Date date) {
        this.q = date;
        this.d = android.support.v4.b.a.a(date, false);
    }

    public final void a(String[] strArr) {
        this.r = strArr;
        if (strArr != null && strArr.length > 0) {
            this.s = new al[strArr.length];
            for (int i2 = 0; i2 < strArr.length; i2++) {
                this.s[i2] = new al(strArr[i2]);
            }
        }
    }

    public final String b() {
        return this.u;
    }

    public final void b(String str) {
        this.u = str;
        if (android.support.v4.b.a.f(str)) {
            this.k = new a(str);
        }
    }

    public final ao c() {
        return this.w;
    }

    public final Date d() {
        return this.q;
    }

    public final float e() {
        return this.v;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        e eVar = (e) obj;
        return this.f == null ? eVar.f == null : this.f.equals(eVar.f);
    }

    public final int f() {
        if (this.r != null) {
            return this.r.length;
        }
        return 0;
    }

    public final String[] g() {
        return this.r;
    }

    public final String getLoadImageId() {
        return (this.r == null || this.r.length <= 0) ? PoiTypeDef.All : this.r[0];
    }

    public final int hashCode() {
        return (this.f == null ? 0 : this.f.hashCode()) + 31;
    }

    public final String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("feedid=" + this.f + ", cotent=" + this.t + ", status=" + this.h + ", owner=" + this.c + ", commentsCount=" + this.e + ", isTop=" + this.n);
        return stringBuffer.toString();
    }
}
