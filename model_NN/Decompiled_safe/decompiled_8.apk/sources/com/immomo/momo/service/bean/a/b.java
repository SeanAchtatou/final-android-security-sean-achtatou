package com.immomo.momo.service.bean.a;

import android.support.v4.b.a;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.service.bean.bf;
import java.util.Date;
import java.util.List;

public final class b {

    /* renamed from: a  reason: collision with root package name */
    private boolean f2971a = true;
    private boolean b;
    private String c;
    private a d;
    private Date e;
    private String f;
    private int g;
    private String h;
    private bf i;
    private List j = null;
    private List k = null;
    private String l;
    private List m;
    private String[] n = null;
    private int o;
    private String p;

    public final int a() {
        return this.g;
    }

    public final void a(int i2) {
        this.g = i2;
    }

    public final void a(a aVar) {
        this.d = aVar;
    }

    public final void a(bf bfVar) {
        this.i = bfVar;
    }

    public final void a(String str) {
        this.c = str;
    }

    public final void a(Date date) {
        this.e = date;
    }

    public final void a(List list) {
        this.m = list;
    }

    public final void a(boolean z) {
        this.f2971a = z;
    }

    public final void a(String[] strArr) {
        this.n = strArr;
    }

    public final void b(int i2) {
        this.o = i2;
    }

    public final void b(String str) {
        this.h = str;
    }

    public final void b(List list) {
        this.j = list;
    }

    public final void b(boolean z) {
        this.b = z;
    }

    public final boolean b() {
        return this.f2971a;
    }

    public final String c() {
        return this.c;
    }

    public final void c(String str) {
        if (str == null) {
            str = PoiTypeDef.All;
        }
        this.l = str;
    }

    public final void c(List list) {
        this.k = list;
    }

    public final a d() {
        return this.d;
    }

    public final void d(String str) {
        this.f = str;
    }

    public final bf e() {
        return this.i;
    }

    public final Date f() {
        return this.e;
    }

    public final String g() {
        return this.h == null ? PoiTypeDef.All : this.h;
    }

    public final List h() {
        return this.m;
    }

    public final String[] i() {
        return this.n;
    }

    public final int j() {
        return this.o;
    }

    public final String k() {
        int i2 = 0;
        if (!a.a((CharSequence) this.p)) {
            return this.p;
        }
        if (this.k == null || this.k.isEmpty()) {
            this.p = this.l;
        } else {
            StringBuilder sb = new StringBuilder(this.l);
            int i3 = 0;
            while (i3 < this.k.size()) {
                int indexOf = sb.indexOf("%s", i2);
                sb.replace(indexOf, indexOf + 2, ((d) this.k.get(i3)).f2973a);
                i3++;
                i2 = indexOf + 2;
            }
            this.p = sb.toString();
        }
        this.p = this.p.replaceAll("&lsb;", "[").replaceAll("&rsb;", "]").replaceAll("&vb;", "|");
        return this.p;
    }

    public final String l() {
        return this.l;
    }

    public final String m() {
        return this.f;
    }

    public final List n() {
        return this.j;
    }

    public final boolean o() {
        return this.j != null && !this.j.isEmpty();
    }

    public final boolean p() {
        return this.b;
    }

    public final List q() {
        return this.k;
    }

    public final String toString() {
        return "GroupActionSession [unreaded=" + this.f2971a + ", processed=" + this.b + ", groupid=" + this.c + ", group=" + this.d + ", fetchTime=" + this.e + ", id=" + this.f + ", type=" + this.g + ", remoteMomoid=" + this.h + ", remoteUser=" + this.i + ", actions=" + this.j + ", bodys=" + this.k + ", message=" + this.l + "]";
    }
}
