package com.immomo.momo.service.a;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public final class q extends SQLiteOpenHelper {
    public q(Context context) {
        super(context, "feeds", (SQLiteDatabase.CursorFactory) null, 12);
    }

    private static String a(String str, int i) {
        for (int i2 = 1; i2 < i; i2++) {
            String str2 = "field" + i2 + " NUMERIC";
            if (i2 < i - 1) {
                str2 = String.valueOf(str2) + ",";
            }
            str = String.valueOf(str) + str2;
        }
        return String.valueOf(str) + ")";
    }

    public final void onCreate(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL(a("CREATE TABLE IF NOT EXISTS sitefeed(sf_id VARCHAR(50) primary key,", 50));
        sQLiteDatabase.execSQL(a("CREATE TABLE IF NOT EXISTS sitefeedcomments(c_id VARCHAR(50) primary key,", 50));
        sQLiteDatabase.execSQL(a("CREATE TABLE IF NOT EXISTS sites(gs_siteid VARCHAR(50) primary key,", 70));
        sQLiteDatabase.execSQL(a("CREATE TABLE IF NOT EXISTS sitetypes(_id INTEGER primary key autoincrement, c_id VARCHAR(50), ", 10));
        sQLiteDatabase.execSQL(a("CREATE TABLE IF NOT EXISTS feedfilter_types(_id INTEGER primary key autoincrement, c_id VARCHAR(50), ", 10));
        sQLiteDatabase.execSQL(a("CREATE TABLE IF NOT EXISTS events(evid VARCHAR(10) primary key,", 50));
        sQLiteDatabase.execSQL(a("CREATE TABLE IF NOT EXISTS groupfeed(sf_id VARCHAR(10) primary key,", 50));
        sQLiteDatabase.execSQL(a("CREATE TABLE IF NOT EXISTS groupfeedcomments(c_id VARCHAR(10) primary key,", 50));
    }

    public final void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        onCreate(sQLiteDatabase);
    }
}
