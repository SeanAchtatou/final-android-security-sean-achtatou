package com.sina.sdk.api.message;

import android.content.Context;
import android.os.Bundle;
import com.weibo.sdk.android.Oauth2AccessToken;
import com.weibo.sdk.android.WeiboParameters;
import com.weibo.sdk.android.api.WeiboAPI;
import com.weibo.sdk.android.net.RequestListener;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class InviteApi extends WeiboAPI {
    public static final String KEY_INVITE_LOGO = "invite_logo";
    public static final String KEY_TEXT = "text";
    public static final String KEY_URL = "url";
    private final String INVITE_URL = "https://m.api.weibo.com/2/messages/invite.json";
    private String mAccessToken;
    private String mContent;
    private Context mContext;
    private Bundle mData;
    private String mUid;

    public InviteApi(Oauth2AccessToken oauth2AccessToken) {
        super(oauth2AccessToken);
    }

    private boolean encodeInvteContent() {
        if (this.mData == null || !this.mData.containsKey(KEY_TEXT)) {
            return false;
        }
        try {
            this.mContent = URLEncoder.encode(parseBundle2Json(), "UTF-8");
            return true;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return false;
        }
    }

    private String parseBundle2Json() {
        StringBuilder sb = new StringBuilder();
        sb.append("{\"text\":\"").append(this.mData.getString(KEY_TEXT)).append("\"");
        if (this.mData.containsKey(KEY_URL)) {
            sb.append(",\"url\":\"").append(this.mData.getString(KEY_URL)).append("\"");
        }
        if (this.mData.containsKey(KEY_INVITE_LOGO)) {
            sb.append(",\"invite_logo\":\"").append(this.mData.getString(KEY_INVITE_LOGO)).append("\"");
        } else {
            sb.append("}");
        }
        return sb.toString();
    }

    public boolean sendInvite(Context context, Bundle bundle, String str, RequestListener requestListener) {
        this.mContext = context;
        this.mData = bundle;
        this.mUid = str;
        if (!encodeInvteContent()) {
            return false;
        }
        WeiboParameters weiboParameters = new WeiboParameters();
        weiboParameters.add("uid", this.mUid);
        weiboParameters.add("data", this.mContent);
        request("https://m.api.weibo.com/2/messages/invite.json", weiboParameters, WeiboAPI.HTTPMETHOD_POST, requestListener);
        return true;
    }
}
