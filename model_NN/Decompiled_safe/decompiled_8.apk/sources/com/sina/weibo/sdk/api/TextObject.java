package com.sina.weibo.sdk.api;

import android.os.Parcel;
import android.os.Parcelable;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.sina.weibo.sdk.log.Log;

public class TextObject extends BaseMediaObject {
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public TextObject createFromParcel(Parcel parcel) {
            return new TextObject(parcel);
        }

        public TextObject[] newArray(int i) {
            return new TextObject[i];
        }
    };
    public String text;

    public TextObject() {
    }

    public TextObject(Parcel parcel) {
        this.text = parcel.readString();
    }

    public boolean checkArgs() {
        if (this.text != null && this.text.length() != 0 && this.text.length() <= 1024) {
            return true;
        }
        Log.e("Weibo.TextObject", "checkArgs fail, text is invalid");
        return false;
    }

    public int describeContents() {
        return 0;
    }

    public int getObjType() {
        return 1;
    }

    /* access modifiers changed from: protected */
    public BaseMediaObject toExtraMediaObject(String str) {
        return this;
    }

    /* access modifiers changed from: protected */
    public String toExtraMediaString() {
        return PoiTypeDef.All;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.text);
    }
}
