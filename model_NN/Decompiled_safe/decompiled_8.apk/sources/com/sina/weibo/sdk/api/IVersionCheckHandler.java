package com.sina.weibo.sdk.api;

import android.content.Context;

interface IVersionCheckHandler {
    boolean check(Context context, WeiboMessage weiboMessage);

    boolean check(Context context, WeiboMultiMessage weiboMultiMessage);
}
