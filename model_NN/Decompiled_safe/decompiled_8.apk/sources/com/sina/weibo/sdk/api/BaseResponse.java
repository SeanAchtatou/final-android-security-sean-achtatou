package com.sina.weibo.sdk.api;

import android.os.Bundle;
import com.sina.weibo.sdk.constant.Constants;

public abstract class BaseResponse extends Base {
    public int errCode;
    public String errMsg;
    public String reqPackageName;

    public void fromBundle(Bundle bundle) {
        this.errCode = bundle.getInt(Constants.Response.ERRCODE);
        this.errMsg = bundle.getString(Constants.Response.ERRMSG);
        this.transaction = bundle.getString(Constants.TRAN);
        this.reqPackageName = bundle.getString(Constants.Base.APP_PKG);
    }

    public void toBundle(Bundle bundle) {
        bundle.putInt(Constants.COMMAND_TYPE_KEY, getType());
        bundle.putInt(Constants.Response.ERRCODE, this.errCode);
        bundle.putString(Constants.Response.ERRMSG, this.errMsg);
        bundle.putString(Constants.TRAN, this.transaction);
    }
}
