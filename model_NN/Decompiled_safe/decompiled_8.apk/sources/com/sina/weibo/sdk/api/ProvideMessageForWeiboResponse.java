package com.sina.weibo.sdk.api;

import android.content.Context;
import android.os.Bundle;
import com.sina.weibo.sdk.log.Log;

public class ProvideMessageForWeiboResponse extends BaseResponse {
    private static final String TAG = "ProvideMessageForWeiboResponse";
    public WeiboMessage message;

    public ProvideMessageForWeiboResponse() {
    }

    public ProvideMessageForWeiboResponse(Bundle bundle) {
        fromBundle(bundle);
    }

    /* access modifiers changed from: package-private */
    public final boolean check(Context context, VersionCheckHandler versionCheckHandler) {
        Log.d(TAG, "check()");
        if (this.message == null) {
            return false;
        }
        if (versionCheckHandler != null) {
            versionCheckHandler.setPackageName(this.reqPackageName);
            if (!versionCheckHandler.check(context, this.message)) {
                return false;
            }
        }
        return this.message.checkArgs();
    }

    public void fromBundle(Bundle bundle) {
        super.fromBundle(bundle);
        this.message = new WeiboMessage(bundle);
    }

    public int getType() {
        return 2;
    }

    public void toBundle(Bundle bundle) {
        super.toBundle(bundle);
        bundle.putAll(this.message.toBundle(bundle));
    }
}
