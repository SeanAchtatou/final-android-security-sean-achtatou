package com.sina.weibo.sdk.api;

import android.os.Bundle;
import com.sina.weibo.sdk.constant.Constants;
import com.sina.weibo.sdk.log.Log;

public final class WeiboMessage {
    public BaseMediaObject mediaObject;

    public WeiboMessage() {
    }

    public WeiboMessage(Bundle bundle) {
        toBundle(bundle);
    }

    public final boolean checkArgs() {
        if (this.mediaObject == null) {
            Log.e("Weibo.WeiboMessage", "checkArgs fail, mediaObject is null");
            return false;
        } else if (this.mediaObject == null || this.mediaObject.checkArgs()) {
            return true;
        } else {
            Log.e("Weibo.WeiboMessage", "checkArgs fail, mediaObject is invalid");
            return false;
        }
    }

    public final Bundle toBundle(Bundle bundle) {
        if (this.mediaObject != null) {
            bundle.putParcelable(Constants.Msg.MEDIA, this.mediaObject);
            bundle.putString(Constants.Msg.MEDIA_EXTRA, this.mediaObject.toExtraMediaString());
        }
        return bundle;
    }

    public final WeiboMessage toObject(Bundle bundle) {
        this.mediaObject = (BaseMediaObject) bundle.getParcelable(Constants.Msg.MEDIA);
        if (this.mediaObject != null) {
            this.mediaObject.toExtraMediaObject(bundle.getString(Constants.Msg.MEDIA_EXTRA));
        }
        return this;
    }
}
