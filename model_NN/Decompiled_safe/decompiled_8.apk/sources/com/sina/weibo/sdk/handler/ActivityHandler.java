package com.sina.weibo.sdk.handler;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Bundle;
import com.sina.weibo.sdk.constant.Constants;
import com.sina.weibo.sdk.log.Log;
import com.sina.weibo.sdk.utils.MD5;
import com.sina.weibo.sdk.utils.Util;

public class ActivityHandler {
    public static boolean send(Activity activity, String str, String str2, String str3, Bundle bundle) {
        if (activity == null || str == null || str.length() == 0 || str2 == null || str2.length() == 0) {
            Log.e("ActivityHandler", "send fail, invalid arguments");
            return false;
        }
        Intent intent = new Intent();
        intent.setPackage(str);
        intent.setAction(str2);
        String packageName = activity.getPackageName();
        intent.putExtra(Constants.Base.SDK_VER, 22);
        intent.putExtra(Constants.Base.APP_PKG, packageName);
        intent.putExtra(Constants.Base.APP_KEY, str3);
        intent.putExtra(Constants.SDK.FLAG, (int) Constants.WEIBO_FLAG_SDK);
        intent.putExtra(Constants.SIGN, MD5.hexdigest(Util.getSign(activity, packageName)));
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        try {
            activity.startActivityForResult(intent, Constants.SDK_ACTIVITY_FOR_RESULT_CODE);
            Log.d("ActivityHandler", "send weibo message, intent=" + intent);
            return true;
        } catch (ActivityNotFoundException e) {
            Log.e("ActivityHandler", "send fail, target ActivityNotFound");
            return false;
        }
    }

    public static boolean sendToWeibo(Activity activity, String str, String str2, Bundle bundle) {
        return send(activity, str, Constants.ACTIVITY_WEIBO, str2, bundle);
    }
}
