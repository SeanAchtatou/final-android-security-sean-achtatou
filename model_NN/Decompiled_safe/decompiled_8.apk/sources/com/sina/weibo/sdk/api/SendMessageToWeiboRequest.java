package com.sina.weibo.sdk.api;

import android.content.Context;
import android.os.Bundle;
import com.sina.weibo.sdk.log.Log;

public class SendMessageToWeiboRequest extends BaseRequest {
    private static final String TAG = "SendMessageToWeiboRequest";
    public WeiboMessage message;

    public SendMessageToWeiboRequest() {
    }

    public SendMessageToWeiboRequest(Bundle bundle) {
        fromBundle(bundle);
    }

    /* access modifiers changed from: package-private */
    public final boolean check(Context context, VersionCheckHandler versionCheckHandler) {
        Log.d(TAG, "check()");
        if (this.message == null) {
            return false;
        }
        if (versionCheckHandler == null || versionCheckHandler.check(context, this.message)) {
            return this.message.checkArgs();
        }
        return false;
    }

    public void fromBundle(Bundle bundle) {
        super.fromBundle(bundle);
        this.message = new WeiboMessage(bundle);
    }

    public int getType() {
        return 1;
    }

    public void toBundle(Bundle bundle) {
        super.toBundle(bundle);
        bundle.putAll(this.message.toBundle(bundle));
    }
}
