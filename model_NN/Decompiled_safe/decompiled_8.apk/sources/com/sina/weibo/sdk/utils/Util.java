package com.sina.weibo.sdk.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.net.Uri;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.sina.weibo.sdk.api.IWeiboDownloadListener;
import com.sina.weibo.sdk.constant.Constants;
import java.util.Locale;
import java.util.UUID;

public class Util {
    private static final String CANCEL_CHINESS = "以后再说";
    private static final String CANCEL_ENGLISH = "Download Later";
    private static final String OK_CHINESS = "现在下载";
    private static final String OK_ENGLISH = "Download Now";
    private static final String PROMPT_CHINESS = "未安装微博客户端，是否现在去下载？";
    private static final String PROMPT_ENGLISH = "Sina Weibo client is not installed, download now?";
    private static final String TITLE_CHINESS = "提示";
    private static final String TITLE_ENGLISH = "Notice";

    public static void createConfirmDialog(final Activity activity, final IWeiboDownloadListener iWeiboDownloadListener) {
        String str = TITLE_CHINESS;
        String str2 = PROMPT_CHINESS;
        String str3 = OK_CHINESS;
        String str4 = CANCEL_CHINESS;
        if (!isChineseLocale(activity.getApplicationContext())) {
            str = TITLE_ENGLISH;
            str2 = PROMPT_ENGLISH;
            str3 = OK_ENGLISH;
            str4 = CANCEL_ENGLISH;
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setMessage(str2);
        builder.setTitle(str);
        builder.setPositiveButton(str3, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                Util.downloadWeibo(activity);
                try {
                    dialogInterface.dismiss();
                } catch (Exception e) {
                }
            }
        });
        builder.setNegativeButton(str4, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                try {
                    dialogInterface.dismiss();
                    if (IWeiboDownloadListener.this != null) {
                        IWeiboDownloadListener.this.onCancel();
                    }
                } catch (Exception e) {
                }
            }
        });
        builder.create().show();
    }

    /* access modifiers changed from: private */
    public static void downloadWeibo(Activity activity) {
        Intent intent = new Intent();
        intent.setAction("android.intent.action.VIEW");
        intent.setFlags(268435456);
        intent.setData(Uri.parse(Constants.WEIBO_DOWNLOAD_URL));
        try {
            activity.startActivity(intent);
        } catch (Exception e) {
        }
    }

    public static String generateId() {
        return UUID.randomUUID().toString().replace("-", PoiTypeDef.All);
    }

    public static byte[] getSign(Context context, String str) {
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(str, 64);
            for (Signature byteArray : packageInfo.signatures) {
                byte[] byteArray2 = byteArray.toByteArray();
                if (byteArray2 != null) {
                    return byteArray2;
                }
            }
            return null;
        } catch (PackageManager.NameNotFoundException e) {
            return null;
        }
    }

    private static boolean isChineseLocale(Context context) {
        try {
            Locale locale = context.getResources().getConfiguration().locale;
            return Locale.CHINA.equals(locale) || Locale.CHINESE.equals(locale) || Locale.SIMPLIFIED_CHINESE.equals(locale) || Locale.TAIWAN.equals(locale);
        } catch (Exception e) {
            return true;
        }
    }
}
