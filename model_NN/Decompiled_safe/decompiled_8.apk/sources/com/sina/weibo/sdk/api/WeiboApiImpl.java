package com.sina.weibo.sdk.api;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import com.sina.weibo.sdk.api.ApiUtils;
import com.sina.weibo.sdk.api.IWeiboHandler;
import com.sina.weibo.sdk.constant.Constants;
import com.sina.weibo.sdk.handler.ActivityHandler;
import com.sina.weibo.sdk.handler.ReceiverHandler;
import com.sina.weibo.sdk.log.Log;
import com.sina.weibo.sdk.utils.Util;

public class WeiboApiImpl implements IWeiboAPI {
    private static final String TAG = "WeiboApiImpl";
    private String mAppKey;
    private Context mContext;
    private boolean mDownWeibo = true;
    private IWeiboDownloadListener mDownloadListener;

    public WeiboApiImpl(Context context, String str, boolean z) {
        this.mContext = context.getApplicationContext();
        this.mAppKey = str;
        this.mDownWeibo = z;
    }

    public int getWeiboAppSupportAPI() {
        ApiUtils.WeiboInfo queryWeiboInfo = ApiUtils.queryWeiboInfo(this.mContext);
        if (queryWeiboInfo != null) {
            return queryWeiboInfo.supportApi;
        }
        Log.e(TAG, "getWeiboAppSupportAPI() faild winfo is null");
        return -1;
    }

    public boolean isWeiboAppInstalled() {
        if (ApiUtils.queryWeiboInfo(this.mContext) != null) {
            return true;
        }
        Log.e(TAG, "isWeiboAppInstalled() faild winfo is null");
        return false;
    }

    public boolean isWeiboAppSupportAPI() {
        return ApiUtils.isWeiboAppSupportAPI(getWeiboAppSupportAPI());
    }

    public boolean registerApp() {
        ApiUtils.WeiboInfo queryWeiboInfo = ApiUtils.queryWeiboInfo(this.mContext);
        if (queryWeiboInfo == null) {
            Log.e(TAG, "registerApp() failed winfo == null");
            return false;
        } else if (!ApiUtils.isWeiboAppSupportAPI(queryWeiboInfo.supportApi)) {
            Log.e(TAG, "registerApp() failed not isWeiboAppSupportAPI");
            return false;
        } else {
            String str = queryWeiboInfo.packageName;
            if (this.mAppKey == null || this.mAppKey.length() == 0) {
                Log.e(TAG, "registerApp() failed appkey is null");
                return false;
            }
            Log.d(TAG, "registerApp() packageName : " + str);
            ReceiverHandler.register(this.mContext, this.mAppKey);
            return true;
        }
    }

    public void registerWeiboDownloadListener(IWeiboDownloadListener iWeiboDownloadListener) {
        this.mDownloadListener = iWeiboDownloadListener;
    }

    public boolean requestListener(Intent intent, IWeiboHandler.Request request) {
        String stringExtra = intent.getStringExtra(Constants.Base.APP_PKG);
        if (stringExtra == null) {
            Log.e(TAG, "requestListener() faild appPackage validateSign faild");
            return false;
        } else if (intent.getStringExtra(Constants.TRAN) == null) {
            Log.e(TAG, "requestListener() faild intent TRAN is null");
            return false;
        } else if (!ApiUtils.validateSign(this.mContext, stringExtra)) {
            Log.e(TAG, "requestListener() faild appPackage validateSign faild");
            return false;
        } else {
            request.onRequest(new ProvideMessageForWeiboRequest(intent.getExtras()));
            return true;
        }
    }

    public boolean responseListener(Intent intent, IWeiboHandler.Response response) {
        String stringExtra = intent.getStringExtra(Constants.Base.APP_PKG);
        if (stringExtra == null) {
            Log.e(TAG, "responseListener() faild appPackage is null");
            return false;
        } else if (!(response instanceof Activity)) {
            Log.e(TAG, "responseListener() faild handler is not Activity");
            return false;
        } else {
            Log.d(TAG, "responseListener() callPkg : " + ((Activity) response).getCallingPackage());
            if (intent.getStringExtra(Constants.TRAN) == null) {
                Log.e(TAG, "responseListener() faild intent TRAN is null");
                return false;
            } else if (!ApiUtils.validateSign(this.mContext, stringExtra)) {
                Log.e(TAG, "responseListener() faild appPackage validateSign faild");
                return false;
            } else {
                response.onResponse(new SendMessageToWeiboResponse(intent.getExtras()));
                return true;
            }
        }
    }

    public boolean sendRequest(Activity activity, BaseRequest baseRequest) {
        if (activity == null || baseRequest == null) {
            Log.e(TAG, "sendRequest faild act == null or request == null");
            return false;
        }
        ApiUtils.WeiboInfo queryWeiboInfo = ApiUtils.queryWeiboInfo(this.mContext);
        if (queryWeiboInfo == null) {
            Log.e(TAG, "sendRequest faild winfo is null");
            if (!this.mDownWeibo) {
                return false;
            }
            Util.createConfirmDialog(activity, this.mDownloadListener);
            return false;
        } else if (!ApiUtils.isWeiboAppSupportAPI(queryWeiboInfo.supportApi)) {
            Log.e(TAG, "sendRequest faild isWeiboAppSupportAPI");
            return false;
        } else {
            if (!baseRequest.check(this.mContext, new VersionCheckHandler(queryWeiboInfo.packageName))) {
                Log.e(TAG, "sendRequest faild request check faild");
                return false;
            }
            Bundle bundle = new Bundle();
            baseRequest.toBundle(bundle);
            return ActivityHandler.sendToWeibo(activity, queryWeiboInfo.packageName, this.mAppKey, bundle);
        }
    }

    public boolean sendResponse(BaseResponse baseResponse) {
        if (baseResponse == null) {
            Log.e(TAG, "sendResponse failed response null");
            return false;
        } else if (!baseResponse.check(this.mContext, new VersionCheckHandler())) {
            Log.e(TAG, "sendResponse checkArgs fail");
            return false;
        } else {
            Bundle bundle = new Bundle();
            baseResponse.toBundle(bundle);
            return ReceiverHandler.sendToWeibo(this.mContext, this.mAppKey, bundle, baseResponse.reqPackageName);
        }
    }

    public boolean startWeibo() {
        ApiUtils.WeiboInfo queryWeiboInfo = ApiUtils.queryWeiboInfo(this.mContext);
        if (queryWeiboInfo == null) {
            Log.e(TAG, "startWeibo() faild winfo is null");
            return false;
        }
        try {
            String str = queryWeiboInfo.packageName;
            Log.d(TAG, "startWeibo() packageName : " + str);
            if (TextUtils.isEmpty(str)) {
                Log.e(TAG, "startWeibo() faild packageName is null");
                return false;
            }
            this.mContext.startActivity(this.mContext.getPackageManager().getLaunchIntentForPackage(str));
            return true;
        } catch (Exception e) {
            Log.e(TAG, e.getMessage(), e);
            return false;
        }
    }
}
