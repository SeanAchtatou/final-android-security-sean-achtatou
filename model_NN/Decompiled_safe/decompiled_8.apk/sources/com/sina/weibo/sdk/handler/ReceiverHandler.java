package com.sina.weibo.sdk.handler;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import com.sina.weibo.sdk.constant.Constants;
import com.sina.weibo.sdk.log.Log;
import com.sina.weibo.sdk.utils.MD5;
import com.sina.weibo.sdk.utils.Util;

public class ReceiverHandler {
    public static void register(Context context, String str) {
        sendBroadcast(context, Constants.ACTION_WEIBO_REGISTER, str, null, null);
    }

    public static void sendBroadcast(Context context, String str, String str2, Bundle bundle, String str3) {
        Intent intent = new Intent(str);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        String packageName = context.getPackageName();
        intent.putExtra(Constants.Base.SDK_VER, 22);
        intent.putExtra(Constants.Base.APP_PKG, packageName);
        intent.putExtra(Constants.Base.APP_KEY, str2);
        intent.putExtra(Constants.SDK.FLAG, (int) Constants.WEIBO_FLAG_SDK);
        intent.putExtra(Constants.SIGN, MD5.hexdigest(Util.getSign(context, packageName)));
        if (!TextUtils.isEmpty(str3)) {
            intent.setPackage(str3);
        }
        context.sendBroadcast(intent, "com.sina.weibo.permission.WEIBO_SDK_PERMISSION");
        Log.d("ReceiverHandler", "send message, intent=" + str + ", appPackage=" + packageName);
    }

    public static boolean sendToWeibo(Context context, String str, Bundle bundle, String str2) {
        sendBroadcast(context, Constants.ACTION_WEIBO_RESPONSE, str, bundle, str2);
        return true;
    }
}
