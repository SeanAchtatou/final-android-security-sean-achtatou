package com.sina.weibo.sdk.api;

import android.app.Activity;
import android.content.Intent;
import com.sina.weibo.sdk.api.IWeiboHandler;

public interface IWeiboAPI {
    int getWeiboAppSupportAPI();

    boolean isWeiboAppInstalled();

    boolean isWeiboAppSupportAPI();

    boolean registerApp();

    void registerWeiboDownloadListener(IWeiboDownloadListener iWeiboDownloadListener);

    boolean requestListener(Intent intent, IWeiboHandler.Request request);

    boolean responseListener(Intent intent, IWeiboHandler.Response response);

    boolean sendRequest(Activity activity, BaseRequest baseRequest);

    boolean sendResponse(BaseResponse baseResponse);

    boolean startWeibo();
}
