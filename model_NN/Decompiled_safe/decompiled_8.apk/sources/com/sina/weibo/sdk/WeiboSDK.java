package com.sina.weibo.sdk;

import android.content.Context;
import com.sina.weibo.sdk.api.IWeiboAPI;
import com.sina.weibo.sdk.api.WeiboApiImpl;

public class WeiboSDK {
    private static IWeiboAPI weibo = null;

    public static IWeiboAPI createWeiboAPI(Context context, String str) {
        return createWeiboAPI(context, str, true);
    }

    public static synchronized IWeiboAPI createWeiboAPI(Context context, String str, boolean z) {
        IWeiboAPI iWeiboAPI;
        synchronized (WeiboSDK.class) {
            if (weibo == null) {
                weibo = new WeiboApiImpl(context.getApplicationContext(), str, z);
            }
            iWeiboAPI = weibo;
        }
        return iWeiboAPI;
    }
}
