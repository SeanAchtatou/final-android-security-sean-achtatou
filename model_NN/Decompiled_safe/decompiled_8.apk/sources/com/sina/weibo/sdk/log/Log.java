package com.sina.weibo.sdk.log;

public class Log {
    private static final String TAG = "weibosdk";
    private static boolean isDebug = true;

    public static void d(String str, String str2) {
        if (isDebug) {
            android.util.Log.d(TAG, String.valueOf(str) + "  " + str2);
        }
    }

    public static void e(String str, String str2) {
        if (isDebug) {
            android.util.Log.e(TAG, String.valueOf(str) + "  " + str2);
        }
    }

    public static void e(String str, String str2, Exception exc) {
        if (isDebug) {
            android.util.Log.e(TAG, String.valueOf(str) + "  " + str2, exc);
        }
    }

    public static void i(String str, String str2) {
        if (isDebug) {
            android.util.Log.i(TAG, String.valueOf(str) + "  " + str2);
        }
    }

    public static void setDebug(boolean z) {
        isDebug = z;
    }

    public static void v(String str, String str2) {
        if (isDebug) {
            android.util.Log.v(TAG, String.valueOf(str) + "  " + str2);
        }
    }

    public static void w(String str, String str2) {
        if (isDebug) {
            android.util.Log.w(TAG, String.valueOf(str) + "  " + str2);
        }
    }
}
