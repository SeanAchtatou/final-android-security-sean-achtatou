package com.sina.weibo.sdk.api;

import android.content.Context;
import com.sina.weibo.sdk.api.ApiUtils;
import com.sina.weibo.sdk.log.Log;

public class VersionCheckHandler implements IVersionCheckHandler {
    private static final String TAG = "VersionCheckHandler";
    private String mPackageName;

    public VersionCheckHandler() {
    }

    public VersionCheckHandler(String str) {
        this.mPackageName = str;
    }

    public boolean check(Context context, WeiboMessage weiboMessage) {
        ApiUtils.WeiboInfo queryWeiboInfoByPackage;
        Log.d(TAG, "check WeiboMessage package : " + this.mPackageName);
        if (this.mPackageName == null || this.mPackageName.length() == 0 || (queryWeiboInfoByPackage = ApiUtils.queryWeiboInfoByPackage(context, this.mPackageName)) == null) {
            return false;
        }
        Log.d(TAG, "check WeiboMessage WeiboInfo supportApi : " + queryWeiboInfoByPackage.supportApi);
        if (queryWeiboInfoByPackage.supportApi < 10351 && weiboMessage.mediaObject != null && (weiboMessage.mediaObject instanceof VoiceObject)) {
            weiboMessage.mediaObject = null;
        }
        if (queryWeiboInfoByPackage.supportApi < 10352 && weiboMessage.mediaObject != null && (weiboMessage.mediaObject instanceof CmdObject)) {
            weiboMessage.mediaObject = null;
        }
        return true;
    }

    public boolean check(Context context, WeiboMultiMessage weiboMultiMessage) {
        ApiUtils.WeiboInfo queryWeiboInfoByPackage;
        Log.d(TAG, "check WeiboMultiMessage package : " + this.mPackageName);
        if (this.mPackageName == null || this.mPackageName.length() == 0 || (queryWeiboInfoByPackage = ApiUtils.queryWeiboInfoByPackage(context, this.mPackageName)) == null) {
            return false;
        }
        Log.d(TAG, "check WeiboMultiMessage WeiboInfo supportApi : " + queryWeiboInfoByPackage.supportApi);
        if (queryWeiboInfoByPackage.supportApi < 10351) {
            return false;
        }
        if (queryWeiboInfoByPackage.supportApi < 10352 && weiboMultiMessage.mediaObject != null && (weiboMultiMessage.mediaObject instanceof CmdObject)) {
            weiboMultiMessage.mediaObject = null;
        }
        return true;
    }

    public void setPackageName(String str) {
        this.mPackageName = str;
    }
}
