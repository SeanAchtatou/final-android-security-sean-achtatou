package com.sina.weibo.sdk.api;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.Signature;
import android.net.Uri;
import android.text.TextUtils;
import com.sina.weibo.sdk.log.Log;
import com.sina.weibo.sdk.utils.MD5;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

public class ApiUtils {
    public static final int BUILD_INT = 10350;
    public static final int BUILD_INT_VER_2_2 = 10351;
    public static final int BUILD_INT_VER_2_3 = 10352;
    private static final String TAG = "ApiUtils";
    private static final String WEIBO_IDENTITY_ACTION = "com.sina.weibo.action.sdkidentity";
    private static final Uri WEIBO_NAME_URI = Uri.parse("content://com.sina.weibo.sdkProvider/query/package");
    private static final String WEIBO_SIGN = "18da2bf10352443a00a5e046d9fca6bd";

    public class WeiboInfo {
        public String packageName;
        public int supportApi;
    }

    public static boolean compareSign(Signature[] signatureArr) {
        for (int i = 0; i < signatureArr.length; i++) {
            if (WEIBO_SIGN.equals(MD5.hexdigest(signatureArr[0].toByteArray()))) {
                Log.d("Weibo", "check pass");
                return true;
            }
        }
        return false;
    }

    /* JADX WARNING: Removed duplicated region for block: B:40:0x007b A[SYNTHETIC, Splitter:B:40:0x007b] */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x0085 A[SYNTHETIC, Splitter:B:46:0x0085] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.sina.weibo.sdk.api.ApiUtils.WeiboInfo getAssetWeiboInfo(android.content.Context r7, java.lang.String r8) {
        /*
            r1 = 0
            r0 = 2
            android.content.Context r0 = r7.createPackageContext(r8, r0)     // Catch:{ NameNotFoundException -> 0x0089, Exception -> 0x0094 }
            r2 = 1024(0x400, float:1.435E-42)
            byte[] r3 = new byte[r2]     // Catch:{ IOException -> 0x00a9, Exception -> 0x006e, all -> 0x0081 }
            android.content.res.AssetManager r0 = r0.getAssets()     // Catch:{ IOException -> 0x00a9, Exception -> 0x006e, all -> 0x0081 }
            java.lang.String r2 = "weibo_for_sdk.json"
            java.io.InputStream r2 = r0.open(r2)     // Catch:{ IOException -> 0x00a9, Exception -> 0x006e, all -> 0x0081 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x003e, Exception -> 0x00a7 }
            r0.<init>()     // Catch:{ IOException -> 0x003e, Exception -> 0x00a7 }
        L_0x0019:
            r4 = 0
            r5 = 1024(0x400, float:1.435E-42)
            int r4 = r2.read(r3, r4, r5)     // Catch:{ IOException -> 0x003e, Exception -> 0x00a7 }
            r5 = -1
            if (r4 != r5) goto L_0x0034
            java.lang.String r3 = r0.toString()     // Catch:{ IOException -> 0x003e, Exception -> 0x00a7 }
            boolean r3 = android.text.TextUtils.isEmpty(r3)     // Catch:{ IOException -> 0x003e, Exception -> 0x00a7 }
            if (r3 == 0) goto L_0x004f
        L_0x002d:
            if (r2 == 0) goto L_0x0032
            r2.close()     // Catch:{ IOException -> 0x009f }
        L_0x0032:
            r0 = r1
        L_0x0033:
            return r0
        L_0x0034:
            java.lang.String r5 = new java.lang.String     // Catch:{ IOException -> 0x003e, Exception -> 0x00a7 }
            r6 = 0
            r5.<init>(r3, r6, r4)     // Catch:{ IOException -> 0x003e, Exception -> 0x00a7 }
            r0.append(r5)     // Catch:{ IOException -> 0x003e, Exception -> 0x00a7 }
            goto L_0x0019
        L_0x003e:
            r0 = move-exception
        L_0x003f:
            java.lang.String r3 = "ApiUtils"
            java.lang.String r4 = r0.getMessage()     // Catch:{ all -> 0x00a5 }
            com.sina.weibo.sdk.log.Log.e(r3, r4, r0)     // Catch:{ all -> 0x00a5 }
            if (r2 == 0) goto L_0x004d
            r2.close()     // Catch:{ IOException -> 0x00a1 }
        L_0x004d:
            r0 = r1
            goto L_0x0033
        L_0x004f:
            boolean r3 = validateSign(r7, r8)     // Catch:{ IOException -> 0x003e, Exception -> 0x00a7 }
            if (r3 == 0) goto L_0x002d
            java.lang.String r0 = r0.toString()     // Catch:{ IOException -> 0x003e, Exception -> 0x00a7 }
            int r3 = parseSupportApi(r0)     // Catch:{ IOException -> 0x003e, Exception -> 0x00a7 }
            com.sina.weibo.sdk.api.ApiUtils$WeiboInfo r0 = new com.sina.weibo.sdk.api.ApiUtils$WeiboInfo     // Catch:{ IOException -> 0x003e, Exception -> 0x00a7 }
            r0.<init>()     // Catch:{ IOException -> 0x003e, Exception -> 0x00a7 }
            r0.packageName = r8     // Catch:{ IOException -> 0x003e, Exception -> 0x00a7 }
            r0.supportApi = r3     // Catch:{ IOException -> 0x003e, Exception -> 0x00a7 }
            if (r2 == 0) goto L_0x0033
            r2.close()     // Catch:{ IOException -> 0x006c }
            goto L_0x0033
        L_0x006c:
            r1 = move-exception
            goto L_0x0033
        L_0x006e:
            r0 = move-exception
            r2 = r1
        L_0x0070:
            java.lang.String r3 = "ApiUtils"
            java.lang.String r4 = r0.getMessage()     // Catch:{ all -> 0x00a5 }
            com.sina.weibo.sdk.log.Log.e(r3, r4, r0)     // Catch:{ all -> 0x00a5 }
            if (r2 == 0) goto L_0x004d
            r2.close()     // Catch:{ IOException -> 0x007f }
            goto L_0x004d
        L_0x007f:
            r0 = move-exception
            goto L_0x004d
        L_0x0081:
            r0 = move-exception
            r2 = r1
        L_0x0083:
            if (r2 == 0) goto L_0x0088
            r2.close()     // Catch:{ IOException -> 0x00a3 }
        L_0x0088:
            throw r0     // Catch:{ NameNotFoundException -> 0x0089, Exception -> 0x0094 }
        L_0x0089:
            r0 = move-exception
            java.lang.String r2 = "ApiUtils"
            java.lang.String r3 = r0.getMessage()
            com.sina.weibo.sdk.log.Log.e(r2, r3, r0)
            goto L_0x004d
        L_0x0094:
            r0 = move-exception
            java.lang.String r2 = "ApiUtils"
            java.lang.String r3 = r0.getMessage()
            com.sina.weibo.sdk.log.Log.e(r2, r3, r0)
            goto L_0x004d
        L_0x009f:
            r0 = move-exception
            goto L_0x0032
        L_0x00a1:
            r0 = move-exception
            goto L_0x004d
        L_0x00a3:
            r2 = move-exception
            goto L_0x0088
        L_0x00a5:
            r0 = move-exception
            goto L_0x0083
        L_0x00a7:
            r0 = move-exception
            goto L_0x0070
        L_0x00a9:
            r0 = move-exception
            r2 = r1
            goto L_0x003f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.sina.weibo.sdk.api.ApiUtils.getAssetWeiboInfo(android.content.Context, java.lang.String):com.sina.weibo.sdk.api.ApiUtils$WeiboInfo");
    }

    public static boolean isWeiboAppSupportAPI(int i) {
        return i >= 10350;
    }

    private static int parseSupportApi(String str) {
        if (TextUtils.isEmpty(str)) {
            return -1;
        }
        try {
            return new JSONObject(str).optInt("support_api", -1);
        } catch (JSONException e) {
            return -1;
        }
    }

    public static WeiboInfo queryWeiboInfo(Context context) {
        WeiboInfo queryWeiboInfoByProvider = queryWeiboInfoByProvider(context);
        return queryWeiboInfoByProvider != null ? queryWeiboInfoByProvider : queryWeiboInfoByFile(context);
    }

    private static WeiboInfo queryWeiboInfoByFile(Context context) {
        WeiboInfo assetWeiboInfo;
        Intent intent = new Intent(WEIBO_IDENTITY_ACTION);
        intent.addCategory("android.intent.category.DEFAULT");
        List<ResolveInfo> queryIntentServices = context.getPackageManager().queryIntentServices(intent, 0);
        if (queryIntentServices == null || queryIntentServices.isEmpty()) {
            return null;
        }
        for (int size = queryIntentServices.size() - 1; size >= 0; size--) {
            ResolveInfo resolveInfo = queryIntentServices.get(size);
            if (resolveInfo.serviceInfo != null && resolveInfo.serviceInfo.applicationInfo != null && resolveInfo.serviceInfo.applicationInfo.packageName != null && resolveInfo.serviceInfo.applicationInfo.packageName.length() != 0 && (assetWeiboInfo = getAssetWeiboInfo(context, resolveInfo.serviceInfo.applicationInfo.packageName)) != null) {
                return assetWeiboInfo;
            }
        }
        return null;
    }

    public static WeiboInfo queryWeiboInfoByPackage(Context context, String str) {
        WeiboInfo assetWeiboInfo = getAssetWeiboInfo(context, str);
        if (assetWeiboInfo != null) {
            return assetWeiboInfo;
        }
        WeiboInfo queryWeiboInfoByProvider = queryWeiboInfoByProvider(context);
        if (queryWeiboInfoByProvider == null || !str.equals(queryWeiboInfoByProvider.packageName)) {
            return null;
        }
        return queryWeiboInfoByProvider;
    }

    /* JADX WARNING: Removed duplicated region for block: B:36:0x006c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static com.sina.weibo.sdk.api.ApiUtils.WeiboInfo queryWeiboInfoByProvider(android.content.Context r7) {
        /*
            r6 = 0
            android.content.ContentResolver r0 = r7.getContentResolver()
            android.net.Uri r1 = com.sina.weibo.sdk.api.ApiUtils.WEIBO_NAME_URI     // Catch:{ Exception -> 0x0056, all -> 0x0068 }
            r2 = 0
            r3 = 0
            r4 = 0
            r5 = 0
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x0056, all -> 0x0068 }
            if (r1 != 0) goto L_0x0018
            if (r1 == 0) goto L_0x0016
            r1.close()
        L_0x0016:
            r0 = r6
        L_0x0017:
            return r0
        L_0x0018:
            java.lang.String r0 = "support_api"
            int r2 = r1.getColumnIndex(r0)     // Catch:{ Exception -> 0x0078 }
            java.lang.String r0 = "package"
            int r3 = r1.getColumnIndex(r0)     // Catch:{ Exception -> 0x0078 }
            boolean r0 = r1.moveToFirst()     // Catch:{ Exception -> 0x0078 }
            if (r0 == 0) goto L_0x0070
            r0 = -1
            java.lang.String r2 = r1.getString(r2)     // Catch:{ Exception -> 0x0078 }
            int r0 = java.lang.Integer.parseInt(r2)     // Catch:{ NumberFormatException -> 0x0053 }
            r2 = r0
        L_0x0034:
            java.lang.String r3 = r1.getString(r3)     // Catch:{ Exception -> 0x0078 }
            boolean r0 = android.text.TextUtils.isEmpty(r3)     // Catch:{ Exception -> 0x0078 }
            if (r0 != 0) goto L_0x0070
            boolean r0 = validateSign(r7, r3)     // Catch:{ Exception -> 0x0078 }
            if (r0 == 0) goto L_0x0070
            com.sina.weibo.sdk.api.ApiUtils$WeiboInfo r0 = new com.sina.weibo.sdk.api.ApiUtils$WeiboInfo     // Catch:{ Exception -> 0x0078 }
            r0.<init>()     // Catch:{ Exception -> 0x0078 }
            r0.packageName = r3     // Catch:{ Exception -> 0x0078 }
            r0.supportApi = r2     // Catch:{ Exception -> 0x0078 }
            if (r1 == 0) goto L_0x0017
            r1.close()
            goto L_0x0017
        L_0x0053:
            r2 = move-exception
            r2 = r0
            goto L_0x0034
        L_0x0056:
            r0 = move-exception
            r1 = r6
        L_0x0058:
            java.lang.String r2 = "ApiUtils"
            java.lang.String r3 = r0.getMessage()     // Catch:{ all -> 0x0076 }
            com.sina.weibo.sdk.log.Log.e(r2, r3, r0)     // Catch:{ all -> 0x0076 }
            if (r1 == 0) goto L_0x0066
            r1.close()
        L_0x0066:
            r0 = r6
            goto L_0x0017
        L_0x0068:
            r0 = move-exception
            r1 = r6
        L_0x006a:
            if (r1 == 0) goto L_0x006f
            r1.close()
        L_0x006f:
            throw r0
        L_0x0070:
            if (r1 == 0) goto L_0x0066
            r1.close()
            goto L_0x0066
        L_0x0076:
            r0 = move-exception
            goto L_0x006a
        L_0x0078:
            r0 = move-exception
            goto L_0x0058
        */
        throw new UnsupportedOperationException("Method not decompiled: com.sina.weibo.sdk.api.ApiUtils.queryWeiboInfoByProvider(android.content.Context):com.sina.weibo.sdk.api.ApiUtils$WeiboInfo");
    }

    public static boolean validateSign(Context context, String str) {
        try {
            return compareSign(context.getPackageManager().getPackageInfo(str, 64).signatures);
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }
}
