package com.sina.weibo.sdk.api;

import android.os.Bundle;
import com.sina.weibo.sdk.constant.Constants;

public abstract class BaseRequest extends Base {
    public String packageName;

    public void fromBundle(Bundle bundle) {
        this.transaction = bundle.getString(Constants.TRAN);
        this.packageName = bundle.getString(Constants.Base.APP_PKG);
    }

    public void toBundle(Bundle bundle) {
        bundle.putInt(Constants.COMMAND_TYPE_KEY, getType());
        bundle.putString(Constants.TRAN, this.transaction);
    }
}
