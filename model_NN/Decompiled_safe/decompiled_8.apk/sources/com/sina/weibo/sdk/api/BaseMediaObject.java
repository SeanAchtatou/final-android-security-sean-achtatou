package com.sina.weibo.sdk.api;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;
import com.sina.weibo.sdk.log.Log;
import java.io.ByteArrayOutputStream;

public abstract class BaseMediaObject implements Parcelable {
    public static final int COMMAND_CMD = 7;
    public static final int COMMAND_IMAGE = 2;
    public static final int COMMAND_MUSIC = 3;
    public static final int COMMAND_TEXT = 1;
    public static final int COMMAND_VIDEO = 4;
    public static final int COMMAND_VOICE = 6;
    public static final int COMMAND_WEBPAGE = 5;
    public String actionUrl;
    public String description;
    public String identify;
    public String schema;
    public byte[] thumbData;
    public String title;

    public BaseMediaObject() {
    }

    public BaseMediaObject(Parcel parcel) {
        this.actionUrl = parcel.readString();
        this.schema = parcel.readString();
        this.identify = parcel.readString();
        this.title = parcel.readString();
        this.description = parcel.readString();
        this.thumbData = parcel.createByteArray();
    }

    /* access modifiers changed from: package-private */
    public boolean checkArgs() {
        if (this.actionUrl == null || this.actionUrl.length() > 512) {
            Log.e("Weibo.BaseMediaObject", "checkArgs fail, actionUrl is invalid");
            return false;
        } else if (this.identify == null || this.identify.length() > 512) {
            Log.e("Weibo.BaseMediaObject", "checkArgs fail, identify is invalid");
            return false;
        } else if (this.thumbData == null || this.thumbData.length > 32768) {
            Log.e("Weibo.BaseMediaObject", "checkArgs fail, thumbData is invalid,size is " + this.thumbData.length + "! more then 32768.");
            return false;
        } else if (this.title == null || this.title.length() > 512) {
            Log.e("Weibo.BaseMediaObject", "checkArgs fail, title is invalid");
            return false;
        } else if (this.description != null && this.description.length() <= 1024) {
            return true;
        } else {
            Log.e("Weibo.BaseMediaObject", "checkArgs fail, description is invalid");
            return false;
        }
    }

    public int describeContents() {
        return 0;
    }

    public abstract int getObjType();

    public final void setThumbImage(Bitmap bitmap) {
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 85, byteArrayOutputStream);
            this.thumbData = byteArrayOutputStream.toByteArray();
            byteArrayOutputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("Weibo.BaseMediaObject", "put thumb failed");
        }
    }

    /* access modifiers changed from: protected */
    public abstract BaseMediaObject toExtraMediaObject(String str);

    /* access modifiers changed from: protected */
    public abstract String toExtraMediaString();

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.actionUrl);
        parcel.writeString(this.schema);
        parcel.writeString(this.identify);
        parcel.writeString(this.title);
        parcel.writeString(this.description);
        parcel.writeByteArray(this.thumbData);
    }
}
