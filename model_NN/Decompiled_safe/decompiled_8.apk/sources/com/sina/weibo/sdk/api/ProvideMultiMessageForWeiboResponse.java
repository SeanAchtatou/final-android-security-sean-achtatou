package com.sina.weibo.sdk.api;

import android.content.Context;
import android.os.Bundle;
import com.sina.weibo.sdk.log.Log;

public class ProvideMultiMessageForWeiboResponse extends BaseResponse {
    private static final String TAG = "ProvideMultiMessageForWeiboResponse";
    public WeiboMultiMessage multiMessage;

    public ProvideMultiMessageForWeiboResponse() {
    }

    public ProvideMultiMessageForWeiboResponse(Bundle bundle) {
        fromBundle(bundle);
    }

    /* access modifiers changed from: package-private */
    public final boolean check(Context context, VersionCheckHandler versionCheckHandler) {
        Log.d(TAG, "check()");
        if (this.multiMessage == null) {
            return false;
        }
        if (versionCheckHandler != null) {
            versionCheckHandler.setPackageName(this.reqPackageName);
            if (!versionCheckHandler.check(context, this.multiMessage)) {
                return false;
            }
        }
        return this.multiMessage.checkArgs();
    }

    public void fromBundle(Bundle bundle) {
        super.fromBundle(bundle);
        this.multiMessage = new WeiboMultiMessage(bundle);
    }

    public int getType() {
        return 2;
    }

    public void toBundle(Bundle bundle) {
        super.toBundle(bundle);
        bundle.putAll(this.multiMessage.toBundle(bundle));
    }
}
