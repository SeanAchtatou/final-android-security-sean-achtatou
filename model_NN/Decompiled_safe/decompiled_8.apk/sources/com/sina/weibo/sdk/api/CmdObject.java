package com.sina.weibo.sdk.api;

import android.os.Parcel;
import android.os.Parcelable;
import com.amap.mapapi.poisearch.PoiTypeDef;

public class CmdObject extends BaseMediaObject {
    public static final String CMD_HOME = "home";
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public CmdObject createFromParcel(Parcel parcel) {
            return new CmdObject(parcel);
        }

        public CmdObject[] newArray(int i) {
            return new CmdObject[i];
        }
    };
    public String cmd;

    public CmdObject() {
    }

    public CmdObject(Parcel parcel) {
        this.cmd = parcel.readString();
    }

    public boolean checkArgs() {
        return (this.cmd == null || this.cmd.length() == 0 || this.cmd.length() > 1024) ? false : true;
    }

    public int describeContents() {
        return 0;
    }

    public int getObjType() {
        return 7;
    }

    /* access modifiers changed from: protected */
    public BaseMediaObject toExtraMediaObject(String str) {
        return this;
    }

    /* access modifiers changed from: protected */
    public String toExtraMediaString() {
        return PoiTypeDef.All;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.cmd);
    }
}
