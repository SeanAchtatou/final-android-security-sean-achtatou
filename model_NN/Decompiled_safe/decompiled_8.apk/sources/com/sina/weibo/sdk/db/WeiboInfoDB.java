package com.sina.weibo.sdk.db;

import android.net.Uri;

public class WeiboInfoDB {
    private static final Uri WEIBO_NAME_URI = Uri.parse("content://com.sina.weibo.sdkProvider/query/package");

    class WeiboInfo {
        public static boolean isDownWeibo;
        public static String packageName;
        public static int supportApi;
        public static int versionCode;
        public static String versionName;

        private WeiboInfo() {
        }
    }

    public static String getPackageName() {
        return WeiboInfo.packageName;
    }

    public static int getSupportApi() {
        return WeiboInfo.supportApi;
    }

    public static int getVersionCode() {
        return WeiboInfo.versionCode;
    }

    public static String getVersionName() {
        return WeiboInfo.versionName;
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0058  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x005f  */
    /* JADX WARNING: Removed duplicated region for block: B:26:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void initWeiboInfo(android.content.Context r7, boolean r8) {
        /*
            r6 = 0
            android.content.ContentResolver r0 = r7.getContentResolver()
            com.sina.weibo.sdk.db.WeiboInfoDB.WeiboInfo.isDownWeibo = r8     // Catch:{ Exception -> 0x004d, all -> 0x005c }
            android.net.Uri r1 = com.sina.weibo.sdk.db.WeiboInfoDB.WEIBO_NAME_URI     // Catch:{ Exception -> 0x004d, all -> 0x005c }
            r2 = 0
            r3 = 0
            r4 = 0
            r5 = 0
            android.database.Cursor r0 = r0.query(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x004d, all -> 0x005c }
            java.lang.String r1 = "package"
            int r1 = r0.getColumnIndex(r1)     // Catch:{ Exception -> 0x0067 }
            java.lang.String r2 = "version_name"
            int r2 = r0.getColumnIndex(r2)     // Catch:{ Exception -> 0x0067 }
            java.lang.String r3 = "version_code"
            int r3 = r0.getColumnIndex(r3)     // Catch:{ Exception -> 0x0067 }
            java.lang.String r4 = "support_api"
            int r4 = r0.getColumnIndex(r4)     // Catch:{ Exception -> 0x0067 }
            boolean r5 = r0.moveToFirst()     // Catch:{ Exception -> 0x0067 }
            if (r5 == 0) goto L_0x0047
            java.lang.String r1 = r0.getString(r1)     // Catch:{ Exception -> 0x0067 }
            com.sina.weibo.sdk.db.WeiboInfoDB.WeiboInfo.packageName = r1     // Catch:{ Exception -> 0x0067 }
            java.lang.String r1 = r0.getString(r2)     // Catch:{ Exception -> 0x0067 }
            com.sina.weibo.sdk.db.WeiboInfoDB.WeiboInfo.versionName = r1     // Catch:{ Exception -> 0x0067 }
            int r1 = r0.getInt(r3)     // Catch:{ Exception -> 0x0067 }
            com.sina.weibo.sdk.db.WeiboInfoDB.WeiboInfo.versionCode = r1     // Catch:{ Exception -> 0x0067 }
            int r1 = r0.getInt(r4)     // Catch:{ Exception -> 0x0067 }
            com.sina.weibo.sdk.db.WeiboInfoDB.WeiboInfo.supportApi = r1     // Catch:{ Exception -> 0x0067 }
        L_0x0047:
            if (r0 == 0) goto L_0x004c
            r0.close()
        L_0x004c:
            return
        L_0x004d:
            r0 = move-exception
            r0 = r6
        L_0x004f:
            java.lang.String r1 = "WeiboInfoDB"
            java.lang.String r2 = "get db error"
            android.util.Log.e(r1, r2)     // Catch:{ all -> 0x0063 }
            if (r0 == 0) goto L_0x004c
            r0.close()
            goto L_0x004c
        L_0x005c:
            r0 = move-exception
        L_0x005d:
            if (r6 == 0) goto L_0x0062
            r6.close()
        L_0x0062:
            throw r0
        L_0x0063:
            r1 = move-exception
            r6 = r0
            r0 = r1
            goto L_0x005d
        L_0x0067:
            r1 = move-exception
            goto L_0x004f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.sina.weibo.sdk.db.WeiboInfoDB.initWeiboInfo(android.content.Context, boolean):void");
    }

    public static boolean isDownWeibo() {
        return WeiboInfo.isDownWeibo;
    }
}
