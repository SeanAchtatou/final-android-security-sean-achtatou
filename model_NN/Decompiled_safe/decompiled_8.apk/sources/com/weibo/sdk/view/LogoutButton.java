package com.weibo.sdk.view;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import com.immomo.momo.R;
import com.weibo.sdk.android.Oauth2AccessToken;
import com.weibo.sdk.android.WeiboParameters;
import com.weibo.sdk.android.api.WeiboAPI;
import com.weibo.sdk.android.net.AsyncWeiboRunner;
import com.weibo.sdk.android.net.RequestListener;
import com.weibo.sdk.android.util.AccessTokenKeeper;

public class LogoutButton extends Button implements View.OnClickListener {
    public static final String TAG = "WEIBO_SDK_LOGIN";
    private final String REVOKE_OAUTH_URL = "https://api.weibo.com/oauth2/revokeoauth2";
    private Context mContext;
    RequestListener mRequestListener = null;

    public LogoutButton(Context context) {
        super(context);
        this.mContext = context;
        setOnClickListener(this);
    }

    public LogoutButton(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.mContext = context;
        setOnClickListener(this);
    }

    public LogoutButton(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.mContext = context;
        setOnClickListener(this);
    }

    public void onClick(View view) {
        Oauth2AccessToken readAccessToken = AccessTokenKeeper.readAccessToken(this.mContext);
        if (TextUtils.isEmpty(readAccessToken.getToken())) {
            Toast.makeText(this.mContext, this.mContext.getResources().getString(R.id.header_iv_avatar), 1).show();
            return;
        }
        WeiboParameters weiboParameters = new WeiboParameters();
        weiboParameters.add("access_token", readAccessToken.getToken());
        Log.d("WEIBO_SDK_LOGIN", "click logout : url==https://api.weibo.com/oauth2/revokeoauth2");
        AsyncWeiboRunner.request("https://api.weibo.com/oauth2/revokeoauth2", weiboParameters, WeiboAPI.HTTPMETHOD_POST, this.mRequestListener);
    }

    public void setRequestListener(RequestListener requestListener) {
        this.mRequestListener = requestListener;
    }
}
