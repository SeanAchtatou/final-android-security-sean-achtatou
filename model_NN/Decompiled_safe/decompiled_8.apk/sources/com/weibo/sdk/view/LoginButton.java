package com.weibo.sdk.view;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import com.weibo.sdk.android.Weibo;
import com.weibo.sdk.android.WeiboAuthListener;
import com.weibo.sdk.android.sso.SsoHandler;

public class LoginButton extends Button {
    private Context mContext;
    /* access modifiers changed from: private */
    public Activity mCurrentActivity;
    /* access modifiers changed from: private */
    public WeiboAuthListener mListener = null;
    /* access modifiers changed from: private */
    public SsoHandler mSsoHandler;
    /* access modifiers changed from: private */
    public Weibo mWeiboAuth;

    public LoginButton(Context context) {
        super(context);
        this.mContext = context;
        initButton();
    }

    public LoginButton(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.mContext = context;
        initButton();
    }

    public LoginButton(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.mContext = context;
        initButton();
    }

    private void initButton() {
        setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                LoginButton.this.mWeiboAuth = Weibo.getInstance("2045436852", "http://www.sina.com", "email,direct_messages_read,direct_messages_write,friendships_groups_read,friendships_groups_write,statuses_to_me_read,follow_app_official_microblog,invitation_write");
                LoginButton.this.mSsoHandler = new SsoHandler(LoginButton.this.mCurrentActivity, LoginButton.this.mWeiboAuth);
                LoginButton.this.mSsoHandler.authorize(LoginButton.this.mListener);
            }
        });
    }

    public Activity getCurrentActivity() {
        return this.mCurrentActivity;
    }

    public SsoHandler getSsoHandler() {
        return this.mSsoHandler;
    }

    public void setAuthListener(WeiboAuthListener weiboAuthListener) {
        this.mListener = weiboAuthListener;
    }

    public void setCurrentActivity(Activity activity) {
        this.mCurrentActivity = activity;
    }
}
