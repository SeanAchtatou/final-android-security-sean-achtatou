package com.weibo.sdk.android;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.webkit.CookieSyncManager;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.sina.weibo.sdk.log.Log;
import com.weibo.sdk.android.api.WeiboAPI;
import com.weibo.sdk.android.net.AsyncWeiboRunner;
import com.weibo.sdk.android.net.RequestListener;
import com.weibo.sdk.android.util.AccessTokenKeeper;
import com.weibo.sdk.android.util.Utility;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class Weibo {
    private static final int AUTH_CODE = 0;
    private static final int AUTH_ONCMPLT = 1000;
    private static final int AUTH_ONRR = 1001;
    private static final int AUTH_TOKEN = 1;
    private static final String KEY_EXPIRES = "expires_in";
    private static final String KEY_REFRESHTOKEN = "refresh_token";
    private static final String KEY_TOKEN = "access_token";
    private static final String SCOPE_DIRECT_MESSAGES_READ = "direct_messages_read";
    private static final String SCOPE_DIRECT_MESSAGES_WRITE = "direct_messages_write";
    private static final String SCOPE_EMAIL = "email";
    private static final String SCOPE_FOLLOW_APP_OFFICIAL_MICROBLOG = "follow_app_official_microblog";
    private static final String SCOPE_FRIENDSHIPS_GROUPS_READ = "friendships_groups_read";
    private static final String SCOPE_FRIENDSHIPS_GROUPS_WRITE = "friendships_groups_write";
    private static final String SCOPE_STATUSES_TO_ME_READ = "statuses_to_me_read";
    public static final String TAG = "WEIBO_SDK_LOGIN";
    private static String URL_OAUTH2_ACCESS_AUTHORIZE = "https://open.weibo.cn/oauth2";
    private static String app_key = PoiTypeDef.All;
    private static String app_secret = PoiTypeDef.All;
    private static boolean isWifi = false;
    private static String mPackagename;
    private static Weibo mWeiboInstance = null;
    private static String mkeyHash;
    private static String redirecturl = PoiTypeDef.All;
    private static String scope = PoiTypeDef.All;
    /* access modifiers changed from: private */
    public Oauth2AccessToken accessToken = null;
    private Context ct;
    private Handler mWeiboHandler = new Handler() {
        public void handleMessage(Message message) {
            switch (message.what) {
                case 1000:
                    if (message.getData() != null) {
                        Weibo.this.mlistener.onComplete(message.getData());
                        return;
                    } else {
                        Weibo.this.mlistener.onWeiboException(new WeiboException("Failed to receive access token."));
                        return;
                    }
                case Weibo.AUTH_ONRR /*1001*/:
                    if (message.obj != null) {
                        Weibo.this.mlistener.onWeiboException((WeiboException) message.obj);
                    }
                    if (message.getData() != null) {
                        String string = message.getData().getString("error");
                        String string2 = message.getData().getString("error_code");
                        Weibo.this.mlistener.onWeiboException(new WeiboException(String.valueOf(string) + "-" + message.getData().getString("error_description"), Integer.parseInt(string2)));
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    };
    /* access modifiers changed from: private */
    public WeiboAuthListener mlistener;

    /* access modifiers changed from: private */
    public void FtchAccessToken(String str) {
        WeiboParameters weiboParameters = new WeiboParameters();
        weiboParameters.add("client_id", app_key);
        weiboParameters.add("client_secret", app_secret);
        weiboParameters.add("grant_type", "authorization_code");
        weiboParameters.add("code", str);
        weiboParameters.add("redirect_uri", redirecturl);
        AsyncWeiboRunner.request(String.valueOf(URL_OAUTH2_ACCESS_AUTHORIZE) + "/access_token", weiboParameters, WeiboAPI.HTTPMETHOD_POST, new RequestListener() {
            public void onComplete(String str) {
                if (Weibo.this.accessToken == null) {
                    Weibo.this.accessToken = new Oauth2AccessToken(str);
                }
                if (Weibo.this.accessToken.isSessionValid()) {
                    Log.d("Weibo-authorize", "Login Success! access_token=" + Weibo.this.accessToken.getToken() + " expires=" + Weibo.this.accessToken.getExpiresTime() + " refresh_token=" + Weibo.this.accessToken.getRefreshToken());
                    Weibo.this.handleListItemEvent(1000, Utility.formBundle(Weibo.this.accessToken), null);
                    return;
                }
                Log.d("Weibo-authorize", "Failed to receive access token");
                Weibo.this.handleListItemEvent(Weibo.AUTH_ONRR, null, null);
            }

            public void onComplete4binary(ByteArrayOutputStream byteArrayOutputStream) {
            }

            public void onError(WeiboException weiboException) {
                Weibo.this.handleListItemEvent(Weibo.AUTH_ONRR, Utility.errorSAX(weiboException.getMessage()), null);
            }

            public void onIOException(IOException iOException) {
                Weibo.this.handleListItemEvent(Weibo.AUTH_ONRR, null, iOException);
            }
        });
    }

    private void KeepAccessToken(Bundle bundle, WeiboAuthListener weiboAuthListener) {
        if (bundle.getString("code") != null) {
            weiboAuthListener.onComplete(bundle);
            return;
        }
        String string = bundle.getString(KEY_TOKEN);
        String string2 = bundle.getString(KEY_EXPIRES);
        if (TextUtils.isEmpty(string) || TextUtils.isEmpty(string2)) {
            weiboAuthListener.onWeiboException(new WeiboException("授权失败！"));
            return;
        }
        Oauth2AccessToken oauth2AccessToken = new Oauth2AccessToken(string, string2);
        if (oauth2AccessToken.isSessionValid()) {
            AccessTokenKeeper.keepAccessToken(this.ct, oauth2AccessToken);
            weiboAuthListener.onComplete(bundle);
        }
    }

    public static String getApp_key() {
        return app_key;
    }

    public static synchronized Weibo getInstance(String str, String str2, String str3) {
        Weibo weibo;
        synchronized (Weibo.class) {
            if (TextUtils.isEmpty(str) || TextUtils.isEmpty(str2)) {
                throw new RuntimeException("Parameter is not complete, please fill complete appkey and redirectUrl.");
            }
            if (mWeiboInstance == null) {
                mWeiboInstance = new Weibo();
            }
            app_key = str;
            redirecturl = str2;
            scope = str3;
            weibo = mWeiboInstance;
        }
        return weibo;
    }

    public static String getRedirecturl() {
        return redirecturl;
    }

    public static String getScope() {
        return scope;
    }

    /* access modifiers changed from: private */
    public void handleListItemEvent(int i, Bundle bundle, Exception exc) {
        Message obtain = Message.obtain();
        obtain.what = i;
        switch (i) {
            case 1000:
                if (bundle != null) {
                    obtain.setData(bundle);
                    break;
                }
                break;
            case AUTH_ONRR /*1001*/:
                if (exc != null) {
                    obtain.obj = exc;
                }
                if (bundle != null) {
                    obtain.setData(bundle);
                    break;
                }
                break;
        }
        this.mWeiboHandler.sendMessage(obtain);
    }

    public static boolean isWifi() {
        return isWifi;
    }

    public static void setWifi(boolean z) {
        isWifi = z;
    }

    private void startDialog(Context context, WeiboParameters weiboParameters, WeiboAuthListener weiboAuthListener) {
        weiboParameters.add("client_id", app_key);
        weiboParameters.add("response_type", "code");
        weiboParameters.add("redirect_uri", redirecturl);
        weiboParameters.add("display", "mobile");
        weiboParameters.add("scope", scope);
        weiboParameters.add("packagename", mPackagename);
        weiboParameters.add("key_hash", Utility.getSign(context, mPackagename));
        String str = String.valueOf(URL_OAUTH2_ACCESS_AUTHORIZE) + "/authorize?" + Utility.encodeUrl(weiboParameters);
        if (context.checkCallingOrSelfPermission("android.permission.INTERNET") != 0) {
            Utility.showAlert(context, "Error", "Application requires permission to access the Internet");
        } else {
            new WeiboDialog(context, str, weiboAuthListener).show();
        }
    }

    public void anthorize(Context context, WeiboAuthListener weiboAuthListener) {
        this.mlistener = weiboAuthListener;
        this.ct = context;
        mPackagename = this.ct.getApplicationContext().getPackageName();
        isWifi = Utility.isWifi(this.ct);
        startAuthDialog(context, weiboAuthListener, 0);
    }

    public void setupConsumerConfig(String str, String str2) {
        app_key = str;
        redirecturl = str2;
    }

    public void startAuthDialog(Context context, final WeiboAuthListener weiboAuthListener, final int i) {
        WeiboParameters weiboParameters = new WeiboParameters();
        CookieSyncManager.createInstance(context);
        startDialog(context, weiboParameters, new WeiboAuthListener() {
            public void onCancel() {
                weiboAuthListener.onCancel();
            }

            public void onComplete(Bundle bundle) {
                CookieSyncManager.getInstance().sync();
                String string = bundle.getString("code");
                if (1 == i) {
                    Weibo.this.FtchAccessToken(string);
                } else if (i == 0) {
                    weiboAuthListener.onComplete(bundle);
                }
            }

            public void onError(WeiboDialogError weiboDialogError) {
                weiboAuthListener.onError(weiboDialogError);
            }

            public void onWeiboException(WeiboException weiboException) {
                weiboAuthListener.onWeiboException(weiboException);
            }
        });
    }
}
