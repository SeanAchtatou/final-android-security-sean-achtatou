package com.weibo.sdk.android.util;

import android.app.AlertDialog;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Toast;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.jcraft.jzlib.GZIPHeader;
import com.sina.weibo.sdk.log.Log;
import com.weibo.sdk.android.Oauth2AccessToken;
import com.weibo.sdk.android.Weibo;
import com.weibo.sdk.android.WeiboParameters;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import org.json.JSONException;
import org.json.JSONObject;

public class Utility {
    private static byte[] decodes = new byte[256];
    private static char[] encodes = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/".toCharArray();

    public final class UploadImageUtils {
        private static void revitionImageSize(String str, int i, int i2) {
            if (i <= 0) {
                throw new IllegalArgumentException("size must be greater than 0!");
            } else if (!Utility.doesExisted(str)) {
                if (str == null) {
                    str = "null";
                }
                throw new FileNotFoundException(str);
            } else if (!BitmapHelper.verifyBitmap(str)) {
                throw new IOException(PoiTypeDef.All);
            } else {
                FileInputStream fileInputStream = new FileInputStream(str);
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeStream(fileInputStream, null, options);
                try {
                    fileInputStream.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                int i3 = 0;
                while (true) {
                    if ((options.outWidth >> i3) <= i && (options.outHeight >> i3) <= i) {
                        break;
                    }
                    i3++;
                }
                options.inSampleSize = (int) Math.pow(2.0d, (double) i3);
                options.inJustDecodeBounds = false;
                Bitmap safeDecodeBimtapFile = safeDecodeBimtapFile(str, options);
                if (safeDecodeBimtapFile == null) {
                    throw new IOException("Bitmap decode error!");
                }
                boolean unused = Utility.deleteDependon(str);
                Utility.makesureFileExist(str);
                FileOutputStream fileOutputStream = new FileOutputStream(str);
                if (options.outMimeType == null || !options.outMimeType.contains("png")) {
                    safeDecodeBimtapFile.compress(Bitmap.CompressFormat.JPEG, i2, fileOutputStream);
                } else {
                    safeDecodeBimtapFile.compress(Bitmap.CompressFormat.PNG, i2, fileOutputStream);
                }
                try {
                    fileOutputStream.close();
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
                safeDecodeBimtapFile.recycle();
            }
        }

        private static void revitionImageSizeHD(String str, int i, int i2) {
            float f;
            Bitmap createBitmap;
            if (i <= 0) {
                throw new IllegalArgumentException("size must be greater than 0!");
            } else if (!Utility.doesExisted(str)) {
                if (str == null) {
                    str = "null";
                }
                throw new FileNotFoundException(str);
            } else if (!BitmapHelper.verifyBitmap(str)) {
                throw new IOException(PoiTypeDef.All);
            } else {
                int i3 = i * 2;
                FileInputStream fileInputStream = new FileInputStream(str);
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeStream(fileInputStream, null, options);
                try {
                    fileInputStream.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                int i4 = 0;
                while (true) {
                    if ((options.outWidth >> i4) <= i3 && (options.outHeight >> i4) <= i3) {
                        break;
                    }
                    i4++;
                }
                options.inSampleSize = (int) Math.pow(2.0d, (double) i4);
                options.inJustDecodeBounds = false;
                Bitmap safeDecodeBimtapFile = safeDecodeBimtapFile(str, options);
                if (safeDecodeBimtapFile == null) {
                    throw new IOException("Bitmap decode error!");
                }
                boolean unused = Utility.deleteDependon(str);
                Utility.makesureFileExist(str);
                float width = ((float) i) / ((float) (safeDecodeBimtapFile.getWidth() > safeDecodeBimtapFile.getHeight() ? safeDecodeBimtapFile.getWidth() : safeDecodeBimtapFile.getHeight()));
                if (width < 1.0f) {
                    while (true) {
                        try {
                            f = width;
                            createBitmap = Bitmap.createBitmap((int) (((float) safeDecodeBimtapFile.getWidth()) * f), (int) (((float) safeDecodeBimtapFile.getHeight()) * f), Bitmap.Config.ARGB_8888);
                            break;
                        } catch (OutOfMemoryError e2) {
                            System.gc();
                            width = (float) (((double) f) * 0.8d);
                        }
                    }
                    if (createBitmap == null) {
                        safeDecodeBimtapFile.recycle();
                    }
                    Canvas canvas = new Canvas(createBitmap);
                    Matrix matrix = new Matrix();
                    matrix.setScale(f, f);
                    canvas.drawBitmap(safeDecodeBimtapFile, matrix, new Paint());
                    safeDecodeBimtapFile.recycle();
                    safeDecodeBimtapFile = createBitmap;
                }
                FileOutputStream fileOutputStream = new FileOutputStream(str);
                if (options.outMimeType == null || !options.outMimeType.contains("png")) {
                    safeDecodeBimtapFile.compress(Bitmap.CompressFormat.JPEG, i2, fileOutputStream);
                } else {
                    safeDecodeBimtapFile.compress(Bitmap.CompressFormat.PNG, i2, fileOutputStream);
                }
                try {
                    fileOutputStream.close();
                } catch (Exception e3) {
                    e3.printStackTrace();
                }
                safeDecodeBimtapFile.recycle();
            }
        }

        public static boolean revitionPostImageSize(String str) {
            try {
                if (Weibo.isWifi()) {
                    revitionImageSizeHD(str, 1600, 75);
                } else {
                    revitionImageSize(str, 1024, 75);
                }
                return true;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        }

        /* JADX WARNING: Code restructure failed: missing block: B:19:0x0028, code lost:
            r2 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:20:0x0029, code lost:
            r6 = r2;
            r2 = r3;
            r3 = r1;
            r1 = r6;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:40:?, code lost:
            return r1;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Removed duplicated region for block: B:27:0x0044 A[ExcHandler: FileNotFoundException (e java.io.FileNotFoundException), Splitter:B:7:0x0013] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private static android.graphics.Bitmap safeDecodeBimtapFile(java.lang.String r7, android.graphics.BitmapFactory.Options r8) {
            /*
                r4 = 0
                if (r8 != 0) goto L_0x004c
                android.graphics.BitmapFactory$Options r0 = new android.graphics.BitmapFactory$Options
                r0.<init>()
                r1 = 1
                r0.inSampleSize = r1
            L_0x000b:
                r1 = 0
                r5 = r1
                r1 = r4
            L_0x000e:
                r2 = 5
                if (r5 < r2) goto L_0x0013
                r0 = r1
            L_0x0012:
                return r0
            L_0x0013:
                java.io.FileInputStream r3 = new java.io.FileInputStream     // Catch:{ OutOfMemoryError -> 0x0047, FileNotFoundException -> 0x0044 }
                r3.<init>(r7)     // Catch:{ OutOfMemoryError -> 0x0047, FileNotFoundException -> 0x0044 }
                r2 = 0
                android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeStream(r3, r2, r8)     // Catch:{ OutOfMemoryError -> 0x0028, FileNotFoundException -> 0x0044 }
                r3.close()     // Catch:{ IOException -> 0x0022 }
                r0 = r1
                goto L_0x0012
            L_0x0022:
                r2 = move-exception
                r2.printStackTrace()     // Catch:{ OutOfMemoryError -> 0x0028, FileNotFoundException -> 0x0044 }
                r0 = r1
                goto L_0x0012
            L_0x0028:
                r2 = move-exception
                r6 = r2
                r2 = r3
                r3 = r1
                r1 = r6
            L_0x002d:
                r1.printStackTrace()
                int r1 = r0.inSampleSize
                int r1 = r1 * 2
                r0.inSampleSize = r1
                r2.close()     // Catch:{ IOException -> 0x003f }
            L_0x0039:
                int r1 = r5 + 1
                r5 = r1
                r4 = r2
                r1 = r3
                goto L_0x000e
            L_0x003f:
                r1 = move-exception
                r1.printStackTrace()
                goto L_0x0039
            L_0x0044:
                r0 = move-exception
                r0 = r1
                goto L_0x0012
            L_0x0047:
                r2 = move-exception
                r3 = r1
                r1 = r2
                r2 = r4
                goto L_0x002d
            L_0x004c:
                r0 = r8
                goto L_0x000b
            */
            throw new UnsupportedOperationException("Method not decompiled: com.weibo.sdk.android.util.Utility.UploadImageUtils.safeDecodeBimtapFile(java.lang.String, android.graphics.BitmapFactory$Options):android.graphics.Bitmap");
        }
    }

    private static boolean __createNewFile(File file) {
        if (file == null) {
            return false;
        }
        makesureParentExist(file);
        if (file.exists()) {
            delete(file);
        }
        try {
            return file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    private static void createNewFile(File file) {
        if (file != null && !__createNewFile(file)) {
            throw new RuntimeException(String.valueOf(file.getAbsolutePath()) + " doesn't be created!");
        }
    }

    public static byte[] decodeBase62(String str) {
        int i;
        char c;
        int i2 = 0;
        if (str == null) {
            return null;
        }
        char[] charArray = str.toCharArray();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(str.toCharArray().length);
        byte b = 0;
        int i3 = 0;
        while (i2 < charArray.length) {
            char c2 = charArray[i2];
            if (c2 == 'i') {
                i = i2 + 1;
                char c3 = charArray[i];
                if (c3 == 'a') {
                    c = 'i';
                } else if (c3 == 'b') {
                    c = '+';
                } else if (c3 == 'c') {
                    c = '/';
                } else {
                    i--;
                    c = charArray[i];
                }
            } else {
                char c4 = c2;
                i = i2;
                c = c4;
            }
            b = (b << 6) | decodes[c];
            i3 += 6;
            while (i3 > 7) {
                i3 -= 8;
                byteArrayOutputStream.write(b >> i3);
                b &= (1 << i3) - 1;
            }
            i2 = i + 1;
        }
        return byteArrayOutputStream.toByteArray();
    }

    public static Bundle decodeUrl(String str) {
        Bundle bundle = new Bundle();
        if (str != null) {
            for (String split : str.split("&")) {
                String[] split2 = split.split("=");
                bundle.putString(URLDecoder.decode(split2[0]), URLDecoder.decode(split2[1]));
            }
        }
        return bundle;
    }

    private static void delete(File file) {
        if (file != null && file.exists() && !file.delete()) {
            throw new RuntimeException(String.valueOf(file.getAbsolutePath()) + " doesn't be deleted!");
        }
    }

    private static boolean deleteDependon(File file, int i) {
        int i2 = 1;
        if (i <= 0) {
            i = 5;
        }
        boolean z = false;
        if (file != null) {
            while (!z && i2 <= i && file.isFile() && file.exists()) {
                z = file.delete();
                if (!z) {
                    i2++;
                }
            }
        }
        return z;
    }

    /* access modifiers changed from: private */
    public static boolean deleteDependon(String str) {
        return deleteDependon(str, 0);
    }

    private static boolean deleteDependon(String str, int i) {
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        return deleteDependon(new File(str), i);
    }

    private static boolean doesExisted(File file) {
        return file != null && file.exists();
    }

    /* access modifiers changed from: private */
    public static boolean doesExisted(String str) {
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        return doesExisted(new File(str));
    }

    public static String encodeBase62(byte[] bArr) {
        StringBuffer stringBuffer = new StringBuffer(bArr.length * 2);
        byte b = 0;
        int i = 0;
        for (byte b2 : bArr) {
            b = (b << 8) | (b2 & GZIPHeader.OS_UNKNOWN);
            i += 8;
            while (i > 5) {
                int i2 = i - 6;
                char c = encodes[b >> i2];
                stringBuffer.append(c == 'i' ? "ia" : c == '+' ? "ib" : c == '/' ? "ic" : Character.valueOf(c));
                b &= (1 << i2) - 1;
                i = i2;
            }
        }
        if (i > 0) {
            char c2 = encodes[b << (6 - i)];
            stringBuffer.append(c2 == 'i' ? "ia" : c2 == '+' ? "ib" : c2 == '/' ? "ic" : Character.valueOf(c2));
        }
        return stringBuffer.toString();
    }

    public static String encodeParameters(WeiboParameters weiboParameters) {
        if (weiboParameters == null || isBundleEmpty(weiboParameters)) {
            return PoiTypeDef.All;
        }
        StringBuilder sb = new StringBuilder();
        int i = 0;
        for (int i2 = 0; i2 < weiboParameters.size(); i2++) {
            String key = weiboParameters.getKey(i2);
            if (i != 0) {
                sb.append("&");
            }
            try {
                sb.append(URLEncoder.encode(key, "UTF-8")).append("=").append(URLEncoder.encode(weiboParameters.getValue(key), "UTF-8"));
            } catch (UnsupportedEncodingException e) {
            }
            i++;
        }
        return sb.toString();
    }

    public static String encodeUrl(WeiboParameters weiboParameters) {
        if (weiboParameters == null) {
            return PoiTypeDef.All;
        }
        StringBuilder sb = new StringBuilder();
        boolean z = true;
        for (int i = 0; i < weiboParameters.size(); i++) {
            if (z) {
                z = false;
            } else {
                sb.append("&");
            }
            String key = weiboParameters.getKey(i);
            if (weiboParameters.getValue(key) == null) {
                Log.i("encodeUrl", "key:" + key + " 's value is null");
            } else {
                sb.append(String.valueOf(URLEncoder.encode(weiboParameters.getKey(i))) + "=" + URLEncoder.encode(weiboParameters.getValue(i)));
            }
            Log.i("encodeUrl", sb.toString());
        }
        return sb.toString();
    }

    public static Bundle errorSAX(String str) {
        Bundle bundle = new Bundle();
        if (str != null && str.indexOf("{") >= 0) {
            try {
                JSONObject jSONObject = new JSONObject(str);
                bundle.putString("error", jSONObject.optString("error"));
                bundle.putString("error_code", jSONObject.optString("error_code"));
                bundle.putString("error_description", jSONObject.optString("error_description"));
            } catch (JSONException e) {
                bundle.putString("error", "JSONExceptionerror");
            }
        }
        return bundle;
    }

    public static Bundle formBundle(Oauth2AccessToken oauth2AccessToken) {
        Bundle bundle = new Bundle();
        bundle.putString("access_token", oauth2AccessToken.getToken());
        bundle.putString("refresh_token", oauth2AccessToken.getRefreshToken());
        bundle.putString("expires_in", new StringBuilder(String.valueOf(oauth2AccessToken.getExpiresTime())).toString());
        return bundle;
    }

    public static Bundle formErrorBundle(Exception exc) {
        Bundle bundle = new Bundle();
        bundle.putString("error", exc.getMessage());
        return bundle;
    }

    public static String getSign(Context context, String str) {
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(str, 64);
            for (Signature byteArray : packageInfo.signatures) {
                byte[] byteArray2 = byteArray.toByteArray();
                if (byteArray2 != null) {
                    return MD5.hexdigest(byteArray2);
                }
            }
            return null;
        } catch (PackageManager.NameNotFoundException e) {
            return null;
        }
    }

    private static boolean isBundleEmpty(WeiboParameters weiboParameters) {
        return weiboParameters == null || weiboParameters.size() == 0;
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        if (connectivityManager == null) {
            return false;
        }
        NetworkInfo[] allNetworkInfo = connectivityManager.getAllNetworkInfo();
        if (allNetworkInfo != null) {
            for (NetworkInfo state : allNetworkInfo) {
                if (NetworkInfo.State.CONNECTED == state.getState()) {
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean isWifi(Context context) {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.getType() == 1;
    }

    private static void makesureFileExist(File file) {
        if (file != null && !file.exists()) {
            makesureParentExist(file);
            createNewFile(file);
        }
    }

    /* access modifiers changed from: private */
    public static void makesureFileExist(String str) {
        if (str != null) {
            makesureFileExist(new File(str));
        }
    }

    private static void makesureParentExist(File file) {
        File parentFile;
        if (file != null && (parentFile = file.getParentFile()) != null && !parentFile.exists()) {
            mkdirs(parentFile);
        }
    }

    private static void mkdirs(File file) {
        if (file != null && !file.exists() && !file.mkdirs()) {
            throw new RuntimeException("fail to make " + file.getAbsolutePath());
        }
    }

    public static Bundle parseUrl(String str) {
        try {
            URL url = new URL(str);
            Bundle decodeUrl = decodeUrl(url.getQuery());
            decodeUrl.putAll(decodeUrl(url.getRef()));
            return decodeUrl;
        } catch (MalformedURLException e) {
            return new Bundle();
        }
    }

    public static void showAlert(Context context, String str, String str2) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(str);
        builder.setMessage(str2);
        builder.create().show();
    }

    public static void showToast(String str, Context context) {
        Toast.makeText(context, str, 1).show();
    }
}
