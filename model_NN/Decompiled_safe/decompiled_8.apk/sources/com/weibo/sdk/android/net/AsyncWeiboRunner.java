package com.weibo.sdk.android.net;

import com.weibo.sdk.android.WeiboException;
import com.weibo.sdk.android.WeiboParameters;
import java.io.ByteArrayOutputStream;

public class AsyncWeiboRunner {
    public static void request(final String str, final WeiboParameters weiboParameters, final String str2, final RequestListener requestListener) {
        new Thread() {
            public void run() {
                try {
                    String openUrl = HttpManager.openUrl(str, str2, weiboParameters, weiboParameters.getValue("pic"));
                    if (requestListener != null) {
                        requestListener.onComplete(openUrl);
                    }
                } catch (WeiboException e) {
                    if (requestListener != null) {
                        requestListener.onError(e);
                    }
                }
            }
        }.start();
    }

    public static void request4Binary(final String str, final WeiboParameters weiboParameters, final String str2, final RequestListener requestListener) {
        new Thread() {
            public void run() {
                try {
                    ByteArrayOutputStream openUrl4Binary = HttpManager.openUrl4Binary(str, str2, weiboParameters, weiboParameters.getValue("pic"));
                    if (requestListener != null) {
                        requestListener.onComplete4binary(openUrl4Binary);
                    }
                } catch (WeiboException e) {
                    if (requestListener != null) {
                        requestListener.onError(e);
                    }
                }
            }
        }.start();
    }
}
