package com.weibo.sdk.android.sso;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.Signature;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.sina.sso.RemoteSSO;
import com.sina.weibo.sdk.api.ApiUtils;
import com.sina.weibo.sdk.constant.Constants;
import com.weibo.sdk.android.Oauth2AccessToken;
import com.weibo.sdk.android.Weibo;
import com.weibo.sdk.android.WeiboAuthListener;
import com.weibo.sdk.android.WeiboDialogError;
import com.weibo.sdk.android.util.AccessTokenKeeper;
import com.weibo.sdk.android.util.Utility;
import java.util.List;

public class SsoHandler {
    private static final int AUTH_CODE = 0;
    private static final int AUTH_TOKEN = 1;
    private static final int DEFAULT_AUTH_ACTIVITY_CODE = 32973;
    private static final String KEY_EXPIRES = "expires_in";
    private static final String KEY_REFRESHTOKEN = "refresh_token";
    private static final String KEY_TOKEN = "access_token";
    private static final String WEIBO_SIGNATURE = "30820295308201fea00302010202044b4ef1bf300d06092a864886f70d010105050030818d310b300906035504061302434e3110300e060355040813074265694a696e673110300e060355040713074265694a696e67312c302a060355040a132353696e612e436f6d20546563686e6f6c6f677920284368696e612920436f2e204c7464312c302a060355040b132353696e612e436f6d20546563686e6f6c6f677920284368696e612920436f2e204c74643020170d3130303131343130323831355a180f32303630303130323130323831355a30818d310b300906035504061302434e3110300e060355040813074265694a696e673110300e060355040713074265694a696e67312c302a060355040a132353696e612e436f6d20546563686e6f6c6f677920284368696e612920436f2e204c7464312c302a060355040b132353696e612e436f6d20546563686e6f6c6f677920284368696e612920436f2e204c746430819f300d06092a864886f70d010101050003818d00308189028181009d367115bc206c86c237bb56c8e9033111889b5691f051b28d1aa8e42b66b7413657635b44786ea7e85d451a12a82a331fced99c48717922170b7fc9bc1040753c0d38b4cf2b22094b1df7c55705b0989441e75913a1a8bd2bc591aa729a1013c277c01c98cbec7da5ad7778b2fad62b85ac29ca28ced588638c98d6b7df5a130203010001300d06092a864886f70d0101050500038181000ad4b4c4dec800bd8fd2991adfd70676fce8ba9692ae50475f60ec468d1b758a665e961a3aedbece9fd4d7ce9295cd83f5f19dc441a065689d9820faedbb7c4a4c4635f5ba1293f6da4b72ed32fb8795f736a20c95cda776402099054fccefb4a1a558664ab8d637288feceba9508aa907fc1fe2b1ae5a0dec954ed831c0bea4";
    /* access modifiers changed from: private */
    public static String ssoActivityName = PoiTypeDef.All;
    /* access modifiers changed from: private */
    public static String ssoPackageName = PoiTypeDef.All;
    private ServiceConnection conn = null;
    private Oauth2AccessToken mAccessToken = null;
    /* access modifiers changed from: private */
    public Activity mAuthActivity;
    /* access modifiers changed from: private */
    public int mAuthActivityCode;
    /* access modifiers changed from: private */
    public WeiboAuthListener mAuthDialogListener;
    /* access modifiers changed from: private */
    public Weibo mWeibo;

    public SsoHandler(Activity activity, Weibo weibo) {
        this.mAuthActivity = activity;
        this.mWeibo = weibo;
        Weibo.setWifi(Utility.isWifi(activity));
        this.conn = new ServiceConnection() {
            public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
                RemoteSSO asInterface = RemoteSSO.Stub.asInterface(iBinder);
                try {
                    SsoHandler.ssoPackageName = asInterface.getPackageName();
                    SsoHandler.ssoActivityName = asInterface.getActivityName();
                    if (!SsoHandler.this.startSingleSignOn(SsoHandler.this.mAuthActivity, Weibo.getApp_key(), Weibo.getScope(), SsoHandler.this.mAuthActivityCode)) {
                        SsoHandler.this.mWeibo.startAuthDialog(SsoHandler.this.mAuthActivity, SsoHandler.this.mAuthDialogListener, 0);
                    }
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }

            public void onServiceDisconnected(ComponentName componentName) {
                SsoHandler.this.mWeibo.startAuthDialog(SsoHandler.this.mAuthActivity, SsoHandler.this.mAuthDialogListener, 0);
            }
        };
    }

    private void KeepAccessToken(Bundle bundle, WeiboAuthListener weiboAuthListener) {
        Oauth2AccessToken oauth2AccessToken = new Oauth2AccessToken(bundle.getString(KEY_TOKEN), bundle.getString(KEY_EXPIRES));
        if (oauth2AccessToken.isSessionValid()) {
            AccessTokenKeeper.keepAccessToken(this.mAuthActivity, oauth2AccessToken);
            weiboAuthListener.onComplete(bundle);
        }
    }

    private boolean bindRemoteSSOService(Activity activity, String str) {
        Context applicationContext = activity.getApplicationContext();
        if (str == null || str.trim().equals(PoiTypeDef.All)) {
            Intent intent = new Intent("com.sina.weibo.remotessoservice");
            intent.setPackage("com.sina.weibo");
            if (!applicationContext.bindService(intent, this.conn, 1)) {
                return applicationContext.bindService(new Intent("com.sina.weibo.remotessoservice"), this.conn, 1);
            }
            return true;
        }
        Intent intent2 = new Intent("com.sina.weibo.remotessoservice");
        intent2.setPackage(str);
        if (applicationContext.bindService(intent2, this.conn, 1)) {
            return true;
        }
        return applicationContext.bindService(new Intent("com.sina.weibo.remotessoservice"), this.conn, 1);
    }

    private boolean checkResponse(Intent intent) {
        ApiUtils.WeiboInfo queryWeiboInfo = ApiUtils.queryWeiboInfo(this.mAuthActivity);
        if ((queryWeiboInfo != null && queryWeiboInfo.supportApi <= 10352) || queryWeiboInfo == null) {
            return true;
        }
        String stringExtra = intent.getStringExtra(Constants.Base.APP_PKG);
        if (stringExtra == null) {
            return false;
        }
        if (intent.getStringExtra(Constants.TRAN) == null) {
            return false;
        }
        return ApiUtils.validateSign(this.mAuthActivity, stringExtra);
    }

    public static ComponentName isServiceExisted(Context context, String str) {
        List<ActivityManager.RunningServiceInfo> runningServices = ((ActivityManager) context.getSystemService("activity")).getRunningServices(Integer.MAX_VALUE);
        if (runningServices.size() <= 0) {
            return null;
        }
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= runningServices.size()) {
                return null;
            }
            ComponentName componentName = runningServices.get(i2).service;
            if (componentName.getPackageName().equals(str) && componentName.getClassName().equals(String.valueOf(str) + ".business.RemoteSSOService")) {
                return componentName;
            }
            i = i2 + 1;
        }
    }

    /* access modifiers changed from: private */
    public boolean startSingleSignOn(Activity activity, String str, String str2, int i) {
        boolean z = true;
        String packageName = activity.getApplicationContext().getPackageName();
        Intent intent = new Intent();
        intent.setClassName(ssoPackageName, ssoActivityName);
        intent.putExtra("appKey", str);
        intent.putExtra("redirectUri", Weibo.getRedirecturl());
        intent.putExtra("packagename", packageName);
        intent.putExtra("key_hash", Utility.getSign(activity.getApplicationContext(), packageName));
        Bundle bundle = new Bundle();
        bundle.putInt(Constants.COMMAND_TYPE_KEY, 3);
        bundle.putString(Constants.TRAN, String.valueOf(System.currentTimeMillis()));
        intent.putExtras(bundle);
        if (str2 != null) {
            intent.putExtra("scope", str2);
        }
        if (!validateAppSignatureForIntent(activity, intent)) {
            return false;
        }
        try {
            activity.startActivityForResult(intent, i);
        } catch (ActivityNotFoundException e) {
            z = false;
        }
        activity.getApplication().unbindService(this.conn);
        return z;
    }

    private boolean validateAppSignatureForIntent(Activity activity, Intent intent) {
        ResolveInfo resolveActivity = activity.getPackageManager().resolveActivity(intent, 0);
        if (resolveActivity == null) {
            return false;
        }
        try {
            for (Signature charsString : activity.getPackageManager().getPackageInfo(resolveActivity.activityInfo.packageName, 64).signatures) {
                if (WEIBO_SIGNATURE.equals(charsString.toCharsString())) {
                    return true;
                }
            }
            return false;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    public void authorize(int i, WeiboAuthListener weiboAuthListener, String str) {
        this.mAuthActivityCode = i;
        this.mAuthDialogListener = weiboAuthListener;
        if (!bindRemoteSSOService(this.mAuthActivity, str) && this.mWeibo != null) {
            this.mWeibo.anthorize(this.mAuthActivity, this.mAuthDialogListener);
        }
    }

    public void authorize(WeiboAuthListener weiboAuthListener) {
        authorize(DEFAULT_AUTH_ACTIVITY_CODE, weiboAuthListener, null);
    }

    public void authorize(WeiboAuthListener weiboAuthListener, String str) {
        authorize(DEFAULT_AUTH_ACTIVITY_CODE, weiboAuthListener, str);
    }

    public void authorizeCallBack(int i, int i2, Intent intent) {
        if (i != this.mAuthActivityCode) {
            return;
        }
        if (i2 == -1) {
            if (checkResponse(intent)) {
                String stringExtra = intent.getStringExtra("error");
                if (stringExtra == null) {
                    stringExtra = intent.getStringExtra("error_type");
                }
                if (stringExtra == null) {
                    if (this.mAccessToken == null) {
                        this.mAccessToken = new Oauth2AccessToken();
                    }
                    this.mAccessToken.setToken(intent.getStringExtra(KEY_TOKEN));
                    this.mAccessToken.setExpiresIn(intent.getStringExtra(KEY_EXPIRES));
                    this.mAccessToken.setRefreshToken(intent.getStringExtra(KEY_REFRESHTOKEN));
                    if (this.mAccessToken.isSessionValid()) {
                        Log.d("Weibo-authorize", "Login Success! access_token=" + this.mAccessToken.getToken() + " expires=" + this.mAccessToken.getExpiresTime() + "refresh_token=" + this.mAccessToken.getRefreshToken());
                        KeepAccessToken(intent.getExtras(), this.mAuthDialogListener);
                        return;
                    }
                    Log.d("Weibo-authorize", "Failed to receive access token by SSO");
                    this.mWeibo.anthorize(this.mAuthActivity, this.mAuthDialogListener);
                } else if (stringExtra.equals("access_denied") || stringExtra.equals("OAuthAccessDeniedException")) {
                    Log.d("Weibo-authorize", "Login canceled by user.");
                    this.mAuthDialogListener.onCancel();
                } else {
                    String stringExtra2 = intent.getStringExtra("error_description");
                    if (stringExtra2 != null) {
                        stringExtra = String.valueOf(stringExtra) + ":" + stringExtra2;
                    }
                    Log.d("Weibo-authorize", "Login failed: " + stringExtra);
                    this.mAuthDialogListener.onError(new WeiboDialogError(stringExtra, i2, stringExtra2));
                }
            }
        } else if (i2 != 0) {
        } else {
            if (intent != null) {
                Log.d("Weibo-authorize", "Login failed: " + intent.getStringExtra("error"));
                this.mAuthDialogListener.onError(new WeiboDialogError(intent.getStringExtra("error"), intent.getIntExtra("error_code", -1), intent.getStringExtra("failing_url")));
                return;
            }
            Log.d("Weibo-authorize", "Login canceled by user.");
            this.mAuthDialogListener.onCancel();
        }
    }
}
