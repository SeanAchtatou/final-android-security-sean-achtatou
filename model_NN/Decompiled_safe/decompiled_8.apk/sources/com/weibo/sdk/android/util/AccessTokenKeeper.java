package com.weibo.sdk.android.util;

import android.content.Context;
import android.content.SharedPreferences;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.weibo.sdk.android.Oauth2AccessToken;

public class AccessTokenKeeper {
    private static final String PREFERENCES_NAME = "com_weibo_sdk_android";

    public static void clear(Context context) {
        SharedPreferences.Editor edit = context.getSharedPreferences(PREFERENCES_NAME, 32768).edit();
        edit.clear();
        edit.commit();
    }

    public static void keepAccessToken(Context context, Oauth2AccessToken oauth2AccessToken) {
        SharedPreferences.Editor edit = context.getSharedPreferences(PREFERENCES_NAME, 32768).edit();
        edit.putString("token", oauth2AccessToken.getToken());
        edit.putLong("expiresTime", oauth2AccessToken.getExpiresTime());
        edit.commit();
    }

    public static Oauth2AccessToken readAccessToken(Context context) {
        Oauth2AccessToken oauth2AccessToken = new Oauth2AccessToken();
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREFERENCES_NAME, 32768);
        oauth2AccessToken.setToken(sharedPreferences.getString("token", PoiTypeDef.All));
        oauth2AccessToken.setExpiresTime(sharedPreferences.getLong("expiresTime", 0));
        return oauth2AccessToken;
    }
}
