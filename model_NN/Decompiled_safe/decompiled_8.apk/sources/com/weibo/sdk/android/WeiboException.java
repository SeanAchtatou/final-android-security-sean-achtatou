package com.weibo.sdk.android;

public class WeiboException extends Exception {
    private static final long serialVersionUID = 475022994858770424L;
    private int statusCode = -1;

    public WeiboException() {
    }

    public WeiboException(int i) {
        this.statusCode = i;
    }

    public WeiboException(Exception exc) {
        super(exc);
    }

    public WeiboException(String str) {
        super(str);
    }

    public WeiboException(String str, int i) {
        super(str);
        this.statusCode = i;
    }

    public WeiboException(String str, Exception exc) {
        super(str, exc);
    }

    public WeiboException(String str, Exception exc, int i) {
        super(str, exc);
        this.statusCode = i;
    }

    public WeiboException(String str, Throwable th) {
        super(str, th);
    }

    public WeiboException(Throwable th) {
        super(th);
    }

    public int getStatusCode() {
        return this.statusCode;
    }

    public void setStatusCode(int i) {
        this.statusCode = i;
    }
}
