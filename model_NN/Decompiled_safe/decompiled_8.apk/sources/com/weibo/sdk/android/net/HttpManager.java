package com.weibo.sdk.android.net;

import android.text.TextUtils;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.weibo.sdk.android.WeiboException;
import com.weibo.sdk.android.WeiboParameters;
import com.weibo.sdk.android.util.Utility;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.security.KeyStore;
import java.security.cert.X509Certificate;
import java.util.zip.GZIPInputStream;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import mm.purchasesdk.PurchaseCode;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpProtocolParams;

public class HttpManager {
    private static final String BOUNDARY = getBoundry();
    private static final String END_MP_BOUNDARY = ("--" + BOUNDARY + "--");
    public static final String HTTPMETHOD_GET = "GET";
    private static final String HTTPMETHOD_POST = "POST";
    private static final String MP_BOUNDARY = ("--" + BOUNDARY);
    private static final String MULTIPART_FORM_DATA = "multipart/form-data";
    private static final int SET_CONNECTION_TIMEOUT = 5000;
    private static final int SET_SOCKET_TIMEOUT = 20000;

    class MySSLSocketFactory extends SSLSocketFactory {
        SSLContext sslContext = SSLContext.getInstance("TLS");

        public MySSLSocketFactory(KeyStore keyStore) {
            super(keyStore);
            AnonymousClass1 r0 = new X509TrustManager() {
                public void checkClientTrusted(X509Certificate[] x509CertificateArr, String str) {
                }

                public void checkServerTrusted(X509Certificate[] x509CertificateArr, String str) {
                }

                public X509Certificate[] getAcceptedIssuers() {
                    return null;
                }
            };
            this.sslContext.init(null, new TrustManager[]{r0}, null);
        }

        public Socket createSocket() {
            return this.sslContext.getSocketFactory().createSocket();
        }

        public Socket createSocket(Socket socket, String str, int i, boolean z) {
            return this.sslContext.getSocketFactory().createSocket(socket, str, i, z);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0053 A[SYNTHETIC, Splitter:B:16:0x0053] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void fileToUpload(java.io.OutputStream r4, java.lang.String r5) {
        /*
            if (r5 != 0) goto L_0x0003
        L_0x0002:
            return
        L_0x0003:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = com.weibo.sdk.android.net.HttpManager.MP_BOUNDARY
            java.lang.StringBuilder r1 = r0.append(r1)
            java.lang.String r2 = "\r\n"
            r1.append(r2)
            java.lang.String r1 = "content-disposition: form-data; name=\"file\"; filename=\""
            java.lang.StringBuilder r1 = r0.append(r1)
            java.lang.StringBuilder r1 = r1.append(r5)
            java.lang.String r2 = "\"\r\n"
            r1.append(r2)
            java.lang.String r1 = "Content-Type: application/octet-stream; charset=utf-8\r\n\r\n"
            r0.append(r1)
            java.lang.String r0 = r0.toString()
            byte[] r0 = r0.getBytes()
            r2 = 0
            r4.write(r0)     // Catch:{ IOException -> 0x008d, all -> 0x008a }
            java.io.FileInputStream r1 = new java.io.FileInputStream     // Catch:{ IOException -> 0x008d, all -> 0x008a }
            r1.<init>(r5)     // Catch:{ IOException -> 0x008d, all -> 0x008a }
            r0 = 51200(0xc800, float:7.1746E-41)
            byte[] r0 = new byte[r0]     // Catch:{ IOException -> 0x0049 }
        L_0x003d:
            int r2 = r1.read(r0)     // Catch:{ IOException -> 0x0049 }
            r3 = -1
            if (r2 == r3) goto L_0x0057
            r3 = 0
            r4.write(r0, r3, r2)     // Catch:{ IOException -> 0x0049 }
            goto L_0x003d
        L_0x0049:
            r0 = move-exception
        L_0x004a:
            com.weibo.sdk.android.WeiboException r2 = new com.weibo.sdk.android.WeiboException     // Catch:{ all -> 0x0050 }
            r2.<init>(r0)     // Catch:{ all -> 0x0050 }
            throw r2     // Catch:{ all -> 0x0050 }
        L_0x0050:
            r0 = move-exception
        L_0x0051:
            if (r1 == 0) goto L_0x0056
            r1.close()     // Catch:{ IOException -> 0x0083 }
        L_0x0056:
            throw r0
        L_0x0057:
            java.lang.String r0 = "\r\n"
            byte[] r0 = r0.getBytes()     // Catch:{ IOException -> 0x0049 }
            r4.write(r0)     // Catch:{ IOException -> 0x0049 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0049 }
            java.lang.String r2 = "\r\n"
            r0.<init>(r2)     // Catch:{ IOException -> 0x0049 }
            java.lang.String r2 = com.weibo.sdk.android.net.HttpManager.END_MP_BOUNDARY     // Catch:{ IOException -> 0x0049 }
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ IOException -> 0x0049 }
            java.lang.String r0 = r0.toString()     // Catch:{ IOException -> 0x0049 }
            byte[] r0 = r0.getBytes()     // Catch:{ IOException -> 0x0049 }
            r4.write(r0)     // Catch:{ IOException -> 0x0049 }
            r1.close()     // Catch:{ IOException -> 0x007c }
            goto L_0x0002
        L_0x007c:
            r0 = move-exception
            com.weibo.sdk.android.WeiboException r1 = new com.weibo.sdk.android.WeiboException
            r1.<init>(r0)
            throw r1
        L_0x0083:
            r0 = move-exception
            com.weibo.sdk.android.WeiboException r1 = new com.weibo.sdk.android.WeiboException
            r1.<init>(r0)
            throw r1
        L_0x008a:
            r0 = move-exception
            r1 = r2
            goto L_0x0051
        L_0x008d:
            r0 = move-exception
            r1 = r2
            goto L_0x004a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.weibo.sdk.android.net.HttpManager.fileToUpload(java.io.OutputStream, java.lang.String):void");
    }

    static String getBoundry() {
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 1; i < 12; i++) {
            long currentTimeMillis = System.currentTimeMillis() + ((long) i);
            if (currentTimeMillis % 3 == 0) {
                stringBuffer.append(((char) ((int) currentTimeMillis)) % 9);
            } else if (currentTimeMillis % 3 == 1) {
                stringBuffer.append((char) ((int) ((currentTimeMillis % 26) + 65)));
            } else {
                stringBuffer.append((char) ((int) ((currentTimeMillis % 26) + 97)));
            }
        }
        return stringBuffer.toString();
    }

    private static HttpClient getNewHttpClient() {
        try {
            KeyStore instance = KeyStore.getInstance(KeyStore.getDefaultType());
            instance.load(null, null);
            MySSLSocketFactory mySSLSocketFactory = new MySSLSocketFactory(instance);
            mySSLSocketFactory.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
            BasicHttpParams basicHttpParams = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(basicHttpParams, 10000);
            HttpConnectionParams.setSoTimeout(basicHttpParams, 10000);
            HttpProtocolParams.setVersion(basicHttpParams, HttpVersion.HTTP_1_1);
            HttpProtocolParams.setContentCharset(basicHttpParams, "UTF-8");
            SchemeRegistry schemeRegistry = new SchemeRegistry();
            schemeRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
            schemeRegistry.register(new Scheme("https", mySSLSocketFactory, 443));
            ThreadSafeClientConnManager threadSafeClientConnManager = new ThreadSafeClientConnManager(basicHttpParams, schemeRegistry);
            HttpConnectionParams.setConnectionTimeout(basicHttpParams, SET_CONNECTION_TIMEOUT);
            HttpConnectionParams.setSoTimeout(basicHttpParams, SET_SOCKET_TIMEOUT);
            return new DefaultHttpClient(threadSafeClientConnManager, basicHttpParams);
        } catch (Exception e) {
            return new DefaultHttpClient();
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0055 A[SYNTHETIC, Splitter:B:16:0x0055] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void imageContentToUpload(java.io.OutputStream r4, java.lang.String r5) {
        /*
            if (r5 != 0) goto L_0x0003
        L_0x0002:
            return
        L_0x0003:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = com.weibo.sdk.android.net.HttpManager.MP_BOUNDARY
            java.lang.StringBuilder r1 = r0.append(r1)
            java.lang.String r2 = "\r\n"
            r1.append(r2)
            java.lang.String r1 = "Content-Disposition: form-data; name=\"pic\"; filename=\"news_image\"\r\n"
            r0.append(r1)
            java.lang.String r1 = "image/png"
            java.lang.String r2 = "Content-Type: "
            java.lang.StringBuilder r2 = r0.append(r2)
            java.lang.StringBuilder r1 = r2.append(r1)
            java.lang.String r2 = "\r\n\r\n"
            r1.append(r2)
            java.lang.String r0 = r0.toString()
            byte[] r0 = r0.getBytes()
            r2 = 0
            r4.write(r0)     // Catch:{ IOException -> 0x008f, all -> 0x008c }
            java.io.FileInputStream r1 = new java.io.FileInputStream     // Catch:{ IOException -> 0x008f, all -> 0x008c }
            r1.<init>(r5)     // Catch:{ IOException -> 0x008f, all -> 0x008c }
            r0 = 51200(0xc800, float:7.1746E-41)
            byte[] r0 = new byte[r0]     // Catch:{ IOException -> 0x004b }
        L_0x003f:
            int r2 = r1.read(r0)     // Catch:{ IOException -> 0x004b }
            r3 = -1
            if (r2 == r3) goto L_0x0059
            r3 = 0
            r4.write(r0, r3, r2)     // Catch:{ IOException -> 0x004b }
            goto L_0x003f
        L_0x004b:
            r0 = move-exception
        L_0x004c:
            com.weibo.sdk.android.WeiboException r2 = new com.weibo.sdk.android.WeiboException     // Catch:{ all -> 0x0052 }
            r2.<init>(r0)     // Catch:{ all -> 0x0052 }
            throw r2     // Catch:{ all -> 0x0052 }
        L_0x0052:
            r0 = move-exception
        L_0x0053:
            if (r1 == 0) goto L_0x0058
            r1.close()     // Catch:{ IOException -> 0x0085 }
        L_0x0058:
            throw r0
        L_0x0059:
            java.lang.String r0 = "\r\n"
            byte[] r0 = r0.getBytes()     // Catch:{ IOException -> 0x004b }
            r4.write(r0)     // Catch:{ IOException -> 0x004b }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x004b }
            java.lang.String r2 = "\r\n"
            r0.<init>(r2)     // Catch:{ IOException -> 0x004b }
            java.lang.String r2 = com.weibo.sdk.android.net.HttpManager.END_MP_BOUNDARY     // Catch:{ IOException -> 0x004b }
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ IOException -> 0x004b }
            java.lang.String r0 = r0.toString()     // Catch:{ IOException -> 0x004b }
            byte[] r0 = r0.getBytes()     // Catch:{ IOException -> 0x004b }
            r4.write(r0)     // Catch:{ IOException -> 0x004b }
            r1.close()     // Catch:{ IOException -> 0x007e }
            goto L_0x0002
        L_0x007e:
            r0 = move-exception
            com.weibo.sdk.android.WeiboException r1 = new com.weibo.sdk.android.WeiboException
            r1.<init>(r0)
            throw r1
        L_0x0085:
            r0 = move-exception
            com.weibo.sdk.android.WeiboException r1 = new com.weibo.sdk.android.WeiboException
            r1.<init>(r0)
            throw r1
        L_0x008c:
            r0 = move-exception
            r1 = r2
            goto L_0x0053
        L_0x008f:
            r0 = move-exception
            r1 = r2
            goto L_0x004c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.weibo.sdk.android.net.HttpManager.imageContentToUpload(java.io.OutputStream, java.lang.String):void");
    }

    public static String openUrl(String str, String str2, WeiboParameters weiboParameters, String str3) {
        try {
            HttpClient newHttpClient = getNewHttpClient();
            HttpGet httpGet = null;
            newHttpClient.getParams().setParameter("http.route.default-proxy", NetStateManager.getAPN());
            if (str2.equals("GET")) {
                httpGet = new HttpGet(String.valueOf(str) + "?" + Utility.encodeUrl(weiboParameters));
            } else if (str2.equals("POST")) {
                httpGet = new HttpPost(str);
                String value = weiboParameters.getValue("content-type");
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                if (!TextUtils.isEmpty(str3)) {
                    paramToUpload(byteArrayOutputStream, weiboParameters);
                    httpGet.setHeader("Content-Type", "multipart/form-data; boundary=" + BOUNDARY);
                    imageContentToUpload(byteArrayOutputStream, str3);
                } else {
                    if (value != null) {
                        weiboParameters.remove("content-type");
                        httpGet.setHeader("Content-Type", value);
                    } else {
                        httpGet.setHeader("Content-Type", "application/x-www-form-urlencoded");
                    }
                    byteArrayOutputStream.write(Utility.encodeParameters(weiboParameters).getBytes("UTF-8"));
                }
                byte[] byteArray = byteArrayOutputStream.toByteArray();
                byteArrayOutputStream.close();
                httpGet.setEntity(new ByteArrayEntity(byteArray));
            } else if (str2.equals("DELETE")) {
                httpGet = new HttpDelete(str);
            }
            HttpResponse execute = newHttpClient.execute(httpGet);
            int statusCode = execute.getStatusLine().getStatusCode();
            if (statusCode == 200) {
                return readHttpResponse(execute);
            }
            throw new WeiboException(readHttpResponse(execute), statusCode);
        } catch (IOException e) {
            throw new WeiboException((Exception) e);
        }
    }

    public static ByteArrayOutputStream openUrl4Binary(String str, String str2, WeiboParameters weiboParameters, String str3) {
        try {
            HttpClient newHttpClient = getNewHttpClient();
            HttpGet httpGet = null;
            newHttpClient.getParams().setParameter("http.route.default-proxy", NetStateManager.getAPN());
            if (str2.equals("GET")) {
                httpGet = new HttpGet(String.valueOf(str) + "?" + Utility.encodeUrl(weiboParameters));
            } else if (str2.equals("POST")) {
                httpGet = new HttpPost(str);
                String value = weiboParameters.getValue("content-type");
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                if (!TextUtils.isEmpty(str3)) {
                    paramToUpload(byteArrayOutputStream, weiboParameters);
                    httpGet.setHeader("Content-Type", "multipart/form-data; boundary=" + BOUNDARY);
                    Utility.UploadImageUtils.revitionPostImageSize(str3);
                    imageContentToUpload(byteArrayOutputStream, str3);
                } else {
                    if (value != null) {
                        weiboParameters.remove("content-type");
                        httpGet.setHeader("Content-Type", value);
                    } else {
                        httpGet.setHeader("Content-Type", "application/x-www-form-urlencoded");
                    }
                    byteArrayOutputStream.write(Utility.encodeParameters(weiboParameters).getBytes("UTF-8"));
                }
                byte[] byteArray = byteArrayOutputStream.toByteArray();
                byteArrayOutputStream.close();
                httpGet.setEntity(new ByteArrayEntity(byteArray));
            } else if (str2.equals("DELETE")) {
                httpGet = new HttpDelete(str);
            }
            HttpResponse execute = newHttpClient.execute(httpGet);
            int statusCode = execute.getStatusLine().getStatusCode();
            if (statusCode == 200) {
                return readBytesFromHttpResponse(execute);
            }
            throw new WeiboException(readHttpResponse(execute), statusCode);
        } catch (IOException e) {
            throw new WeiboException((Exception) e);
        }
    }

    private static void paramToUpload(OutputStream outputStream, WeiboParameters weiboParameters) {
        int i = 0;
        while (i < weiboParameters.size()) {
            String key = weiboParameters.getKey(i);
            StringBuilder sb = new StringBuilder(10);
            sb.setLength(0);
            sb.append(MP_BOUNDARY).append("\r\n");
            sb.append("content-disposition: form-data; name=\"").append(key).append("\"\r\n\r\n");
            sb.append(weiboParameters.getValue(key)).append("\r\n");
            try {
                outputStream.write(sb.toString().getBytes());
                i++;
            } catch (IOException e) {
                throw new WeiboException((Exception) e);
            }
        }
    }

    private static ByteArrayOutputStream readBytesFromHttpResponse(HttpResponse httpResponse) {
        try {
            InputStream content = httpResponse.getEntity().getContent();
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            Header firstHeader = httpResponse.getFirstHeader("Content-Encoding");
            InputStream gZIPInputStream = (firstHeader == null || firstHeader.getValue().toLowerCase().indexOf("gzip") < 0) ? content : new GZIPInputStream(content);
            byte[] bArr = new byte[PurchaseCode.QUERY_NO_APP];
            while (true) {
                int read = gZIPInputStream.read(bArr);
                if (read == -1) {
                    return byteArrayOutputStream;
                }
                byteArrayOutputStream.write(bArr, 0, read);
            }
        } catch (IOException | IllegalStateException e) {
            return null;
        }
    }

    private static String readHttpResponse(HttpResponse httpResponse) {
        try {
            InputStream content = httpResponse.getEntity().getContent();
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            Header firstHeader = httpResponse.getFirstHeader("Content-Encoding");
            InputStream gZIPInputStream = (firstHeader == null || firstHeader.getValue().toLowerCase().indexOf("gzip") < 0) ? content : new GZIPInputStream(content);
            byte[] bArr = new byte[PurchaseCode.QUERY_NO_APP];
            while (true) {
                int read = gZIPInputStream.read(bArr);
                if (read == -1) {
                    return new String(byteArrayOutputStream.toByteArray(), "UTF-8");
                }
                byteArrayOutputStream.write(bArr, 0, read);
            }
        } catch (IOException | IllegalStateException e) {
            return PoiTypeDef.All;
        }
    }

    public static String uploadFile(String str, String str2, WeiboParameters weiboParameters, String str3) {
        try {
            HttpClient newHttpClient = getNewHttpClient();
            HttpGet httpGet = null;
            newHttpClient.getParams().setParameter("http.route.default-proxy", NetStateManager.getAPN());
            if (str2.equals("GET")) {
                httpGet = new HttpGet(String.valueOf(str) + "?" + Utility.encodeUrl(weiboParameters));
            } else if (str2.equals("POST")) {
                httpGet = new HttpPost(str);
                String value = weiboParameters.getValue("content-type");
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                if (!TextUtils.isEmpty(str3)) {
                    paramToUpload(byteArrayOutputStream, weiboParameters);
                    httpGet.setHeader("Content-Type", "multipart/form-data; boundary=" + BOUNDARY);
                    fileToUpload(byteArrayOutputStream, str3);
                } else {
                    if (value != null) {
                        weiboParameters.remove("content-type");
                        httpGet.setHeader("Content-Type", value);
                    } else {
                        httpGet.setHeader("Content-Type", "application/x-www-form-urlencoded");
                    }
                    byteArrayOutputStream.write(Utility.encodeParameters(weiboParameters).getBytes("UTF-8"));
                }
                byte[] byteArray = byteArrayOutputStream.toByteArray();
                byteArrayOutputStream.close();
                httpGet.setEntity(new ByteArrayEntity(byteArray));
            } else if (str2.equals("DELETE")) {
                httpGet = new HttpDelete(str);
            }
            HttpResponse execute = newHttpClient.execute(httpGet);
            int statusCode = execute.getStatusLine().getStatusCode();
            if (statusCode == 200) {
                return readHttpResponse(execute);
            }
            throw new WeiboException(readHttpResponse(execute), statusCode);
        } catch (IOException e) {
            throw new WeiboException((Exception) e);
        }
    }
}
