package com.weibo.sdk.android;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.NinePatchDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.weibo.sdk.android.util.Utility;
import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;

public class WeiboDialog extends Dialog implements View.OnClickListener {
    private static final String TAG = "WEIBO_SDK_LOGIN";
    private static int bottom_margin = 0;
    private static int left_margin = 0;
    private static int right_margin = 0;
    private static int theme = 16973840;
    private static int top_margin = 0;
    private String mAuthUrl;
    private RelativeLayout mContent;
    private Context mContext;
    /* access modifiers changed from: private */
    public WeiboAuthListener mListener;
    /* access modifiers changed from: private */
    public ProgressDialog mSpinner;
    /* access modifiers changed from: private */
    public WebView mWebView;
    private RelativeLayout mWebViewContainer;

    class WeiboWebViewClient extends WebViewClient {
        private boolean isCallBacked;

        private WeiboWebViewClient() {
            this.isCallBacked = false;
        }

        /* synthetic */ WeiboWebViewClient(WeiboDialog weiboDialog, WeiboWebViewClient weiboWebViewClient) {
            this();
        }

        public void onPageFinished(WebView webView, String str) {
            super.onPageFinished(webView, str);
            Log.d("WEIBO_SDK_LOGIN", "onPageFinished URL: " + str);
            if (WeiboDialog.this.mSpinner.isShowing()) {
                WeiboDialog.this.mSpinner.dismiss();
            }
            WeiboDialog.this.mWebView.setVisibility(0);
        }

        public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
            Log.d("WEIBO_SDK_LOGIN", "onPageStarted URL: " + str);
            if (!str.startsWith(Weibo.getRedirecturl()) || this.isCallBacked) {
                super.onPageStarted(webView, str, bitmap);
                WeiboDialog.this.mSpinner.show();
                return;
            }
            this.isCallBacked = true;
            WeiboDialog.this.dismiss();
            WeiboDialog.this.handleRedirectUrl(webView, str);
            webView.stopLoading();
        }

        public void onReceivedError(WebView webView, int i, String str, String str2) {
            super.onReceivedError(webView, i, str, str2);
            WeiboDialog.this.dismiss();
            WeiboDialog.this.mListener.onError(new WeiboDialogError(str, i, str2));
        }

        public boolean shouldOverrideUrlLoading(WebView webView, String str) {
            if (!str.startsWith("sms:")) {
                return super.shouldOverrideUrlLoading(webView, str);
            }
            Intent intent = new Intent("android.intent.action.VIEW");
            intent.putExtra("address", str.replace("sms:", PoiTypeDef.All));
            intent.setType("vnd.android-dir/mms-sms");
            WeiboDialog.this.getContext().startActivity(intent);
            return true;
        }
    }

    public WeiboDialog(Context context, String str, WeiboAuthListener weiboAuthListener) {
        super(context, theme);
        this.mAuthUrl = str;
        this.mListener = weiboAuthListener;
        this.mContext = context;
    }

    /* JADX WARNING: Removed duplicated region for block: B:23:0x004d A[SYNTHETIC, Splitter:B:23:0x004d] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private android.graphics.drawable.Drawable getDrawableFromAssert(java.lang.String r6) {
        /*
            r5 = this;
            r0 = 0
            android.content.Context r1 = r5.getContext()
            android.content.res.AssetManager r1 = r1.getAssets()
            java.io.InputStream r2 = r1.open(r6)     // Catch:{ IOException -> 0x0038, all -> 0x0048 }
            if (r2 == 0) goto L_0x0032
            android.graphics.Bitmap r3 = android.graphics.BitmapFactory.decodeStream(r2)     // Catch:{ IOException -> 0x005d }
            android.content.Context r1 = r5.getContext()     // Catch:{ IOException -> 0x005d }
            android.content.res.Resources r1 = r1.getResources()     // Catch:{ IOException -> 0x005d }
            android.util.DisplayMetrics r1 = r1.getDisplayMetrics()     // Catch:{ IOException -> 0x005d }
            int r1 = r1.densityDpi     // Catch:{ IOException -> 0x005d }
            r3.setDensity(r1)     // Catch:{ IOException -> 0x005d }
            android.graphics.drawable.BitmapDrawable r1 = new android.graphics.drawable.BitmapDrawable     // Catch:{ IOException -> 0x005d }
            android.content.Context r4 = r5.getContext()     // Catch:{ IOException -> 0x005d }
            android.content.res.Resources r4 = r4.getResources()     // Catch:{ IOException -> 0x005d }
            r1.<init>(r4, r3)     // Catch:{ IOException -> 0x005d }
            r0 = r1
        L_0x0032:
            if (r2 == 0) goto L_0x0037
            r2.close()     // Catch:{ IOException -> 0x0056 }
        L_0x0037:
            return r0
        L_0x0038:
            r1 = move-exception
            r2 = r0
        L_0x003a:
            r1.printStackTrace()     // Catch:{ all -> 0x005b }
            if (r2 == 0) goto L_0x0037
            r2.close()     // Catch:{ IOException -> 0x0043 }
            goto L_0x0037
        L_0x0043:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0037
        L_0x0048:
            r1 = move-exception
            r2 = r0
            r0 = r1
        L_0x004b:
            if (r2 == 0) goto L_0x0050
            r2.close()     // Catch:{ IOException -> 0x0051 }
        L_0x0050:
            throw r0
        L_0x0051:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0050
        L_0x0056:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0037
        L_0x005b:
            r0 = move-exception
            goto L_0x004b
        L_0x005d:
            r1 = move-exception
            goto L_0x003a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.weibo.sdk.android.WeiboDialog.getDrawableFromAssert(java.lang.String):android.graphics.drawable.Drawable");
    }

    /* access modifiers changed from: private */
    public void handleRedirectUrl(WebView webView, String str) {
        Bundle parseUrl = Utility.parseUrl(str);
        String string = parseUrl.getString("error");
        String string2 = parseUrl.getString("error_code");
        if (string == null && string2 == null) {
            this.mListener.onComplete(parseUrl);
        } else if (string2 == null) {
            this.mListener.onWeiboException(new WeiboException(string, 0));
        } else {
            this.mListener.onWeiboException(new WeiboException(string, Integer.parseInt(string2)));
        }
    }

    private void initCloseButton() {
        Drawable drawableFromAssert = getDrawableFromAssert(getContext().getResources().getDisplayMetrics().densityDpi >= 240 ? "weibosdk_close_hdpi.png" : "weibosdk_close_mdpi.png");
        ImageView imageView = new ImageView(this.mContext);
        imageView.setImageDrawable(drawableFromAssert);
        imageView.setOnClickListener(this);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        RelativeLayout.LayoutParams layoutParams2 = (RelativeLayout.LayoutParams) this.mWebViewContainer.getLayoutParams();
        layoutParams.leftMargin = (layoutParams2.leftMargin - (drawableFromAssert.getIntrinsicWidth() / 2)) + 5;
        layoutParams.topMargin = (layoutParams2.topMargin - (drawableFromAssert.getIntrinsicHeight() / 2)) + 5;
        this.mContent.addView(imageView, layoutParams);
    }

    private void initLoadingDlg() {
        this.mSpinner = new ProgressDialog(getContext());
        this.mSpinner.requestWindowFeature(1);
        Locale locale = Locale.getDefault();
        String language = locale.getLanguage();
        String str = "loading……";
        if ("zh".equalsIgnoreCase(language) || "zh_CN".equalsIgnoreCase(language)) {
            str = "加载中……";
            if ("TW".equalsIgnoreCase(locale.getCountry())) {
                str = "加載中……";
            }
        }
        this.mSpinner.setMessage(str);
    }

    private void initWebview() {
        this.mWebViewContainer = new RelativeLayout(getContext());
        this.mWebView = new WebView(getContext());
        this.mWebView.getSettings().setJavaScriptEnabled(true);
        this.mWebView.getSettings().setSavePassword(false);
        this.mWebView.setWebViewClient(new WeiboWebViewClient(this, null));
        synCookies(this.mContext, this.mAuthUrl);
        this.mWebView.loadUrl(this.mAuthUrl);
        this.mWebView.requestFocus();
        this.mWebView.setScrollBarStyle(0);
        this.mWebView.setVisibility(4);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -1);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-1, -1);
        AssetManager assets = getContext().getAssets();
        InputStream inputStream = null;
        DisplayMetrics displayMetrics = getContext().getResources().getDisplayMetrics();
        float f = displayMetrics.density;
        try {
            inputStream = assets.open("weibosdk_dialog_bg.9.png");
            layoutParams2.leftMargin = (int) (10.0f * f);
            layoutParams2.topMargin = (int) (10.0f * f);
            layoutParams2.rightMargin = (int) (10.0f * f);
            layoutParams2.bottomMargin = (int) (10.0f * f);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (inputStream == null) {
            try {
                this.mWebViewContainer.setBackgroundResource(R.drawable.bg_chat_dis_normal);
            } catch (Exception e2) {
                e2.printStackTrace();
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (IOException e3) {
                        e3.printStackTrace();
                    }
                }
            } catch (Throwable th) {
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (IOException e4) {
                        e4.printStackTrace();
                    }
                }
                throw th;
            }
        } else {
            Bitmap decodeStream = BitmapFactory.decodeStream(inputStream);
            this.mWebViewContainer.setBackgroundDrawable(new NinePatchDrawable(decodeStream, decodeStream.getNinePatchChunk(), new Rect(0, 0, 0, 0), null));
        }
        if (inputStream != null) {
            try {
                inputStream.close();
            } catch (IOException e5) {
                e5.printStackTrace();
            }
        }
        this.mWebViewContainer.addView(this.mWebView, layoutParams2);
        this.mWebViewContainer.setGravity(17);
        if (parseDimens()) {
            layoutParams.leftMargin = left_margin;
            if (((double) f) == 1.0d) {
                layoutParams.topMargin = 15;
            } else if (((double) f) == 1.5d) {
                layoutParams.topMargin = 25;
            } else {
                layoutParams.topMargin = displayMetrics.heightPixels / 15;
            }
            layoutParams.rightMargin = right_margin;
            layoutParams.bottomMargin = (displayMetrics.heightPixels - layoutParams.topMargin) - ((int) (1.5d * ((double) (displayMetrics.widthPixels - (left_margin * 2)))));
            layoutParams.bottomMargin = layoutParams.bottomMargin <= 0 ? bottom_margin : layoutParams.bottomMargin;
        } else {
            Resources resources = getContext().getResources();
            layoutParams.leftMargin = resources.getDimensionPixelSize(R.array.guide_backgroud);
            layoutParams.rightMargin = resources.getDimensionPixelSize(R.array.welcome_bg);
            layoutParams.topMargin = resources.getDimensionPixelSize(R.array.welcome_photo);
            layoutParams.bottomMargin = resources.getDimensionPixelSize(R.array.order_friend_list);
        }
        this.mContent.setBackgroundColor(0);
        this.mContent.addView(this.mWebViewContainer, layoutParams);
    }

    private void initWindow() {
        requestWindowFeature(1);
        getWindow().setFeatureDrawableAlpha(0, 0);
        getWindow().setSoftInputMode(16);
        this.mContent = new RelativeLayout(getContext());
        addContentView(this.mContent, new ViewGroup.LayoutParams(-1, -1));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0082, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0083, code lost:
        r2 = r3;
        r6 = r0;
        r0 = r1;
        r1 = r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00ab, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:?, code lost:
        r3.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x00c9, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x00ca, code lost:
        r1.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x00d7, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x00d8, code lost:
        r1 = r0;
        r0 = false;
        r2 = null;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0031 A[SYNTHETIC, Splitter:B:11:0x0031] */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x00ab A[ExcHandler: all (th java.lang.Throwable), Splitter:B:1:0x001b] */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x00ae A[SYNTHETIC, Splitter:B:47:0x00ae] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean parseDimens() {
        /*
            r7 = this;
            r3 = 0
            r1 = 1
            r2 = 0
            android.content.Context r0 = r7.getContext()
            android.content.res.AssetManager r0 = r0.getAssets()
            android.content.Context r4 = r7.getContext()
            android.content.res.Resources r4 = r4.getResources()
            android.util.DisplayMetrics r4 = r4.getDisplayMetrics()
            float r4 = r4.density
            java.lang.String r5 = "values/dimens.xml"
            java.io.InputStream r3 = r0.open(r5)     // Catch:{ IOException -> 0x00d7, all -> 0x00ab }
            org.xmlpull.v1.XmlPullParser r5 = android.util.Xml.newPullParser()     // Catch:{ IOException -> 0x00dc, all -> 0x00ab }
            java.lang.String r0 = "utf-8"
            r5.setInput(r3, r0)     // Catch:{ XmlPullParserException -> 0x00e1 }
            int r0 = r5.getEventType()     // Catch:{ XmlPullParserException -> 0x00e1 }
        L_0x002c:
            if (r0 != r1) goto L_0x0035
            r0 = r1
        L_0x002f:
            if (r3 == 0) goto L_0x0034
            r3.close()     // Catch:{ IOException -> 0x00ce }
        L_0x0034:
            return r0
        L_0x0035:
            switch(r0) {
                case 2: goto L_0x003d;
                default: goto L_0x0038;
            }
        L_0x0038:
            int r0 = r5.next()     // Catch:{ XmlPullParserException -> 0x0066 }
            goto L_0x002c
        L_0x003d:
            java.lang.String r0 = r5.getName()     // Catch:{ XmlPullParserException -> 0x0066 }
            java.lang.String r2 = "dimen"
            boolean r0 = r0.equals(r2)     // Catch:{ XmlPullParserException -> 0x0066 }
            if (r0 == 0) goto L_0x0038
            r0 = 0
            java.lang.String r2 = "name"
            java.lang.String r0 = r5.getAttributeValue(r0, r2)     // Catch:{ XmlPullParserException -> 0x0066 }
            java.lang.String r2 = "weibosdk_dialog_left_margin"
            boolean r2 = r2.equals(r0)     // Catch:{ XmlPullParserException -> 0x0066 }
            if (r2 == 0) goto L_0x006c
            java.lang.String r0 = r5.nextText()     // Catch:{ XmlPullParserException -> 0x0066 }
            int r0 = java.lang.Integer.parseInt(r0)     // Catch:{ XmlPullParserException -> 0x0066 }
            float r0 = (float) r0     // Catch:{ XmlPullParserException -> 0x0066 }
            float r0 = r0 * r4
            int r0 = (int) r0     // Catch:{ XmlPullParserException -> 0x0066 }
            com.weibo.sdk.android.WeiboDialog.left_margin = r0     // Catch:{ XmlPullParserException -> 0x0066 }
            goto L_0x0038
        L_0x0066:
            r0 = move-exception
        L_0x0067:
            r0.printStackTrace()     // Catch:{ IOException -> 0x0082, all -> 0x00ab }
            r0 = r1
            goto L_0x002f
        L_0x006c:
            java.lang.String r2 = "weibosdk_dialog_top_margin"
            boolean r2 = r2.equals(r0)     // Catch:{ XmlPullParserException -> 0x0066 }
            if (r2 == 0) goto L_0x0095
            java.lang.String r0 = r5.nextText()     // Catch:{ XmlPullParserException -> 0x0066 }
            int r0 = java.lang.Integer.parseInt(r0)     // Catch:{ XmlPullParserException -> 0x0066 }
            float r0 = (float) r0     // Catch:{ XmlPullParserException -> 0x0066 }
            float r0 = r0 * r4
            int r0 = (int) r0     // Catch:{ XmlPullParserException -> 0x0066 }
            com.weibo.sdk.android.WeiboDialog.top_margin = r0     // Catch:{ XmlPullParserException -> 0x0066 }
            goto L_0x0038
        L_0x0082:
            r0 = move-exception
            r2 = r3
            r6 = r0
            r0 = r1
            r1 = r6
        L_0x0087:
            r1.printStackTrace()     // Catch:{ all -> 0x00d4 }
            if (r2 == 0) goto L_0x0034
            r2.close()     // Catch:{ IOException -> 0x0090 }
            goto L_0x0034
        L_0x0090:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0034
        L_0x0095:
            java.lang.String r2 = "weibosdk_dialog_right_margin"
            boolean r2 = r2.equals(r0)     // Catch:{ XmlPullParserException -> 0x0066 }
            if (r2 == 0) goto L_0x00b2
            java.lang.String r0 = r5.nextText()     // Catch:{ XmlPullParserException -> 0x0066 }
            int r0 = java.lang.Integer.parseInt(r0)     // Catch:{ XmlPullParserException -> 0x0066 }
            float r0 = (float) r0     // Catch:{ XmlPullParserException -> 0x0066 }
            float r0 = r0 * r4
            int r0 = (int) r0     // Catch:{ XmlPullParserException -> 0x0066 }
            com.weibo.sdk.android.WeiboDialog.right_margin = r0     // Catch:{ XmlPullParserException -> 0x0066 }
            goto L_0x0038
        L_0x00ab:
            r0 = move-exception
        L_0x00ac:
            if (r3 == 0) goto L_0x00b1
            r3.close()     // Catch:{ IOException -> 0x00c9 }
        L_0x00b1:
            throw r0
        L_0x00b2:
            java.lang.String r2 = "weibosdk_dialog_bottom_margin"
            boolean r0 = r2.equals(r0)     // Catch:{ XmlPullParserException -> 0x0066 }
            if (r0 == 0) goto L_0x0038
            java.lang.String r0 = r5.nextText()     // Catch:{ XmlPullParserException -> 0x0066 }
            int r0 = java.lang.Integer.parseInt(r0)     // Catch:{ XmlPullParserException -> 0x0066 }
            float r0 = (float) r0     // Catch:{ XmlPullParserException -> 0x0066 }
            float r0 = r0 * r4
            int r0 = (int) r0     // Catch:{ XmlPullParserException -> 0x0066 }
            com.weibo.sdk.android.WeiboDialog.bottom_margin = r0     // Catch:{ XmlPullParserException -> 0x0066 }
            goto L_0x0038
        L_0x00c9:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x00b1
        L_0x00ce:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0034
        L_0x00d4:
            r0 = move-exception
            r3 = r2
            goto L_0x00ac
        L_0x00d7:
            r0 = move-exception
            r1 = r0
            r0 = r2
            r2 = r3
            goto L_0x0087
        L_0x00dc:
            r0 = move-exception
            r1 = r0
            r0 = r2
            r2 = r3
            goto L_0x0087
        L_0x00e1:
            r0 = move-exception
            r1 = r2
            goto L_0x0067
        */
        throw new UnsupportedOperationException("Method not decompiled: com.weibo.sdk.android.WeiboDialog.parseDimens():boolean");
    }

    public static void synCookies(Context context, String str) {
        CookieSyncManager.createInstance(context);
        CookieManager instance = CookieManager.getInstance();
        instance.setAcceptCookie(true);
        instance.removeSessionCookie();
        instance.removeAllCookie();
        CookieSyncManager.getInstance().sync();
    }

    /* access modifiers changed from: protected */
    public void onBack() {
        try {
            this.mSpinner.dismiss();
            if (this.mWebView != null) {
                this.mWebView.stopLoading();
                this.mWebView.destroy();
            }
        } catch (Exception e) {
        }
        dismiss();
    }

    public void onClick(View view) {
        if (view instanceof ImageView) {
            onBack();
            this.mListener.onCancel();
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        initWindow();
        initLoadingDlg();
        initWebview();
        initCloseButton();
        Utility.isNetworkAvailable(this.mContext);
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i == 4) {
            this.mListener.onCancel();
        }
        return super.onKeyDown(i, keyEvent);
    }
}
