package com.weibo.sdk.android;

import android.text.TextUtils;
import com.amap.mapapi.poisearch.PoiTypeDef;
import org.json.JSONException;
import org.json.JSONObject;

public class Oauth2AccessToken {
    private String mAccessToken = PoiTypeDef.All;
    private long mExpiresTime = 0;
    private String mRefreshToken = PoiTypeDef.All;

    public Oauth2AccessToken() {
    }

    public Oauth2AccessToken(String str) {
        if (str != null && str.indexOf("{") >= 0) {
            try {
                JSONObject jSONObject = new JSONObject(str);
                setToken(jSONObject.optString("access_token"));
                setExpiresIn(jSONObject.optString("expires_in"));
                setRefreshToken(jSONObject.optString("refresh_token"));
            } catch (JSONException e) {
            }
        }
    }

    public Oauth2AccessToken(String str, String str2) {
        this.mAccessToken = str;
        this.mExpiresTime = System.currentTimeMillis();
        if (str2 != null) {
            this.mExpiresTime += Long.parseLong(str2) * 1000;
        }
    }

    public long getExpiresTime() {
        return this.mExpiresTime;
    }

    public String getRefreshToken() {
        return this.mRefreshToken;
    }

    public String getToken() {
        return this.mAccessToken;
    }

    public boolean isSessionValid() {
        return !TextUtils.isEmpty(this.mAccessToken) && (this.mExpiresTime == 0 || System.currentTimeMillis() < this.mExpiresTime);
    }

    public void setExpiresIn(String str) {
        if (str != null && !str.equals("0")) {
            setExpiresTime(System.currentTimeMillis() + (Long.parseLong(str) * 1000));
        }
    }

    public void setExpiresTime(long j) {
        this.mExpiresTime = j;
    }

    public void setRefreshToken(String str) {
        this.mRefreshToken = str;
    }

    public void setToken(String str) {
        this.mAccessToken = str;
    }
}
