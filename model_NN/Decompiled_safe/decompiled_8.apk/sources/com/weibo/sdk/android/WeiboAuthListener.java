package com.weibo.sdk.android;

import android.os.Bundle;

public interface WeiboAuthListener {
    void onCancel();

    void onComplete(Bundle bundle);

    void onError(WeiboDialogError weiboDialogError);

    void onWeiboException(WeiboException weiboException);
}
