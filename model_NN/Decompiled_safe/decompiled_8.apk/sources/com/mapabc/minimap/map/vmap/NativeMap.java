package com.mapabc.minimap.map.vmap;

import android.content.Context;
import com.amap.mapapi.map.be;
import java.nio.ByteBuffer;

public class NativeMap {
    public static final String MINIMAP_VERSION = "minimapv320";

    /* renamed from: a  reason: collision with root package name */
    byte[] f3112a;
    private int b;

    static {
        try {
            System.loadLibrary(MINIMAP_VERSION);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public NativeMap() {
        this.b = 0;
        this.f3112a = ByteBuffer.allocate(48000).array();
        this.b = nativeCreate();
    }

    public NativeMap(Context context) {
        this.b = 0;
        this.f3112a = ByteBuffer.allocate(48000).array();
        this.b = nativeCreate();
    }

    private static native int nativeCreate();

    private static native void nativeFinalizer(int i);

    private static native void nativeGetLabelStruct(int i, byte[] bArr);

    private static native int nativeGetMapAngle(int i);

    private static native int nativeGetMapCenterX(int i);

    private static native int nativeGetMapCenterY(int i);

    private static native int nativeGetMapHeight(int i);

    private static native int nativeGetMapLevel(int i);

    private static native int nativeGetMapWidth(int i);

    private static native void nativeGetScreenGridNames(int i, byte[] bArr);

    private static native void nativeInitMap(int i, byte[] bArr, int i2, int i3);

    private static native boolean nativePaint(int i, int i2, byte[] bArr, int i3);

    private static native void nativePx20ToScreen(int i, int i2, int i3, be beVar);

    private static native void nativePxToScreen(int i, int i2, int i3, be beVar);

    private static native void nativeResetLabelManager(int i);

    private static native void nativeScreenToPx(int i, int i2, int i3, be beVar);

    private static native void nativeScreenToPx20(int i, int i2, int i3, be beVar);

    private static native void nativeSetMapLevel(int i, int i2);

    private static native void nativeSetMapParameter(int i, int i2, int i3, int i4, int i5);

    public void ScreenToPx(int i, int i2, be beVar) {
        nativeScreenToPx(this.b, i, i2, beVar);
    }

    public void ScreenToPx20(int i, int i2, be beVar) {
        nativeScreenToPx20(this.b, i, i2, beVar);
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        nativeFinalizer(this.b);
        this.b = 0;
        this.f3112a = null;
    }

    public byte[] getLabelBuffer() {
        return this.f3112a;
    }

    public void getLabelStruct(byte[] bArr) {
        nativeGetLabelStruct(this.b, bArr);
    }

    public int getMapAngle() {
        return nativeGetMapAngle(this.b);
    }

    public int getMapCenterX() {
        return nativeGetMapCenterX(this.b);
    }

    public int getMapCenterY() {
        return nativeGetMapCenterY(this.b);
    }

    public int getMapHeight() {
        return nativeGetMapHeight(this.b);
    }

    public int getMapLevel() {
        return nativeGetMapLevel(this.b);
    }

    public int getMapWidth() {
        return nativeGetMapWidth(this.b);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.<init>(byte[], int, int):void}
     arg types: [byte[], int, byte]
     candidates:
      ClspMth{java.lang.String.<init>(int[], int, int):void}
      ClspMth{java.lang.String.<init>(char[], int, int):void}
      ClspMth{java.lang.String.<init>(byte[], int, int):void} */
    public String[] getScreenGridNames() {
        byte[] bArr = new byte[2048];
        nativeGetScreenGridNames(this.b, bArr);
        int i = 1;
        byte b2 = bArr[0];
        String[] strArr = new String[b2];
        for (int i2 = 0; i2 < b2; i2++) {
            int i3 = i + 1;
            byte b3 = bArr[i];
            strArr[i2] = new String(bArr, i3, (int) b3);
            i = b3 + i3;
        }
        return strArr;
    }

    public void initMap(byte[] bArr, int i, int i2) {
        nativeInitMap(this.b, bArr, i, i2);
    }

    public boolean paint(NativeMapEngine nativeMapEngine, byte[] bArr, int i) {
        return nativePaint(nativeMapEngine.b, this.b, bArr, i);
    }

    /* JADX WARN: Type inference failed for: r4v85, types: [int] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void paintLables(com.mapabc.minimap.map.vmap.NativeMapEngine r29, android.graphics.Canvas r30, int r31) {
        /*
            r28 = this;
            android.graphics.Paint r15 = new android.graphics.Paint
            r15.<init>()
            java.util.ArrayList r16 = new java.util.ArrayList
            r16.<init>()
            byte[] r6 = r28.getLabelBuffer()
            r3 = 0
            short r7 = com.amap.mapapi.core.c.b(r6, r3)
            r4 = 2
            r3 = 0
            r5 = r3
            r3 = r4
        L_0x0017:
            if (r5 >= r7) goto L_0x012b
            com.mapabc.minimap.map.vmap.b r8 = new com.mapabc.minimap.map.vmap.b
            r8.<init>()
            r0 = r16
            r0.add(r8)
            int r4 = r3 + 1
            byte r9 = r6[r3]
            java.lang.StringBuffer r10 = new java.lang.StringBuffer
            r10.<init>()
            r3 = 0
        L_0x002d:
            if (r3 >= r9) goto L_0x003c
            short r11 = com.amap.mapapi.core.c.b(r6, r4)
            char r11 = (char) r11
            r10.append(r11)
            int r4 = r4 + 2
            int r3 = r3 + 1
            goto L_0x002d
        L_0x003c:
            java.lang.String r3 = r10.toString()
            r8.f3115a = r3
            int r3 = r4 + 1
            byte r4 = r6[r4]
            r8.p = r4
            int r4 = com.amap.mapapi.core.c.a(r6, r3)
            r9 = -16777216(0xffffffffff000000, float:-1.7014118E38)
            r4 = r4 | r9
            r8.l = r4
            int r3 = r3 + 4
            int r4 = com.amap.mapapi.core.c.a(r6, r3)
            r9 = -16777216(0xffffffffff000000, float:-1.7014118E38)
            r4 = r4 | r9
            r8.m = r4
            int r3 = r3 + 4
            short r4 = com.amap.mapapi.core.c.b(r6, r3)
            r8.b = r4
            int r3 = r3 + 4
            short r4 = com.amap.mapapi.core.c.b(r6, r3)
            r8.c = r4
            int r4 = r3 + 4
            int r3 = r4 + 1
            byte r4 = r6[r4]
            r8.n = r4
            int r4 = r8.n
            if (r4 != 0) goto L_0x0084
            short r4 = com.amap.mapapi.core.c.b(r6, r3)
            r8.d = r4
            int r3 = r3 + 4
        L_0x0080:
            int r4 = r5 + 1
            r5 = r4
            goto L_0x0017
        L_0x0084:
            int r4 = r8.n
            r9 = 1
            if (r4 != r9) goto L_0x0080
            int r4 = r3 + 1
            byte r3 = r6[r3]
            r8.g = r3
            int r3 = com.amap.mapapi.core.c.a(r6, r4)
            r8.j = r3
            int r3 = r4 + 4
            int r4 = com.amap.mapapi.core.c.a(r6, r3)
            r8.k = r4
            int r3 = r3 + 4
            int r4 = r3 + 1
            byte r3 = r6[r3]
            r8.o = r3
            short r3 = com.amap.mapapi.core.c.b(r6, r4)
            r8.e = r3
            int r3 = r4 + 2
            short r4 = com.amap.mapapi.core.c.b(r6, r3)
            r8.f = r4
            int r3 = r3 + 2
            int r4 = r8.o
            if (r4 <= 0) goto L_0x00d3
            int r4 = r8.g
            r0 = r29
            android.graphics.Bitmap r4 = r0.getIconBitmap(r4)
            if (r4 == 0) goto L_0x00d3
            int r9 = r8.b
            int r9 = r9 + -6
            float r9 = (float) r9
            int r10 = r8.c
            int r10 = r10 + -6
            float r10 = (float) r10
            r11 = 0
            r0 = r30
            r0.drawBitmap(r4, r9, r10, r11)
        L_0x00d3:
            int r4 = r8.o
            if (r4 != 0) goto L_0x00e9
            int r4 = r8.b
            int r9 = r8.e
            int r9 = r9 >> 1
            int r4 = r4 - r9
            r8.h = r4
            int r4 = r8.c
            int r9 = r8.f
            int r9 = r9 >> 1
            int r4 = r4 - r9
            r8.i = r4
        L_0x00e9:
            int r4 = r8.o
            r9 = 1
            if (r4 != r9) goto L_0x00fc
            int r4 = r8.b
            int r9 = r8.e
            int r9 = r9 >> 1
            int r4 = r4 - r9
            r8.h = r4
            int r4 = r8.c
            r8.i = r4
            goto L_0x0080
        L_0x00fc:
            int r4 = r8.o
            r9 = 2
            if (r4 != r9) goto L_0x0112
            int r4 = r8.b
            int r4 = r4 + 6
            r8.h = r4
            int r4 = r8.c
            int r9 = r8.f
            int r9 = r9 >> 1
            int r4 = r4 - r9
            r8.i = r4
            goto L_0x0080
        L_0x0112:
            int r4 = r8.o
            r9 = 3
            if (r4 != r9) goto L_0x0080
            int r4 = r8.b
            int r4 = r4 + -6
            int r9 = r8.e
            int r4 = r4 - r9
            r8.h = r4
            int r4 = r8.c
            int r9 = r8.f
            int r9 = r9 >> 1
            int r4 = r4 - r9
            r8.i = r4
            goto L_0x0080
        L_0x012b:
            r3 = 1
            r15.setAntiAlias(r3)
            android.graphics.Paint$FontMetrics r17 = new android.graphics.Paint$FontMetrics
            r17.<init>()
            android.graphics.Matrix r18 = new android.graphics.Matrix
            r18.<init>()
            android.graphics.Matrix r19 = new android.graphics.Matrix
            r19.<init>()
            r8 = 0
            r9 = 0
            r3 = 2
            float[] r0 = new float[r3]
            r20 = r0
            r3 = 2
            float[] r0 = new float[r3]
            r21 = r0
            r3 = 5
            int[][] r0 = new int[r3][]
            r22 = r0
            r3 = 0
            r4 = 2
            int[] r4 = new int[r4]
            r4 = {-1, -1} // fill-array
            r22[r3] = r4
            r3 = 1
            r4 = 2
            int[] r4 = new int[r4]
            r4 = {1, -1} // fill-array
            r22[r3] = r4
            r3 = 2
            r4 = 2
            int[] r4 = new int[r4]
            r4 = {-1, 1} // fill-array
            r22[r3] = r4
            r3 = 3
            r4 = 2
            int[] r4 = new int[r4]
            r4 = {1, 1} // fill-array
            r22[r3] = r4
            r3 = 4
            r4 = 2
            int[] r4 = new int[r4]
            r4 = {0, 0} // fill-array
            r22[r3] = r4
            int r23 = r16.size()
            r3 = 0
            r10 = r3
        L_0x0182:
            r3 = 5
            if (r10 >= r3) goto L_0x02de
            r3 = 2
            r0 = r31
            if (r0 >= r3) goto L_0x0193
            r3 = 1
            if (r10 == r3) goto L_0x02d9
            r3 = 2
            if (r10 == r3) goto L_0x02d9
            r3 = 3
            if (r10 == r3) goto L_0x02d9
        L_0x0193:
            r3 = 4
            if (r10 >= r3) goto L_0x020e
            r3 = 0
            r15.setAntiAlias(r3)
            r3 = 1
            r15.setFakeBoldText(r3)
        L_0x019e:
            r3 = r22[r10]
            r4 = 0
            r24 = r3[r4]
            r3 = r22[r10]
            r4 = 1
            r25 = r3[r4]
            r3 = 0
            r11 = r3
        L_0x01aa:
            r0 = r23
            if (r11 >= r0) goto L_0x02d9
            r0 = r16
            java.lang.Object r3 = r0.get(r11)
            com.mapabc.minimap.map.vmap.b r3 = (com.mapabc.minimap.map.vmap.b) r3
            int r4 = r3.l
            r5 = 4
            if (r10 >= r5) goto L_0x01bd
            int r4 = r3.m
        L_0x01bd:
            int r5 = r3.p
            float r5 = (float) r5
            r15.setTextSize(r5)
            r15.setColor(r4)
            r0 = r17
            r15.getFontMetrics(r0)
            r0 = r17
            float r4 = r0.bottom
            r0 = r17
            float r5 = r0.top
            float r4 = r4 - r5
            int r5 = (int) r4
            int r4 = r3.n
            r6 = 1
            if (r4 != r6) goto L_0x0217
            android.graphics.Paint$Align r4 = android.graphics.Paint.Align.LEFT
            r15.setTextAlign(r4)
            int r4 = r3.h
            int r8 = r4 + r24
            int r4 = r3.i
            int r4 = r4 + r25
            int r6 = r3.p
            int r9 = r4 + r6
        L_0x01eb:
            r18.reset()
            int r4 = r3.d
            int r6 = r3.n
            if (r6 <= 0) goto L_0x0274
            java.lang.String r4 = r3.f3115a
            int r7 = r4.length()
            r4 = 7
            if (r7 > r4) goto L_0x0229
            java.lang.String r3 = r3.f3115a
            float r4 = (float) r8
            float r5 = (float) r9
            r0 = r30
            r0.drawText(r3, r4, r5, r15)
            r3 = r8
            r4 = r9
        L_0x0208:
            int r5 = r11 + 1
            r11 = r5
            r8 = r3
            r9 = r4
            goto L_0x01aa
        L_0x020e:
            r3 = 1
            r15.setAntiAlias(r3)
            r3 = 0
            r15.setFakeBoldText(r3)
            goto L_0x019e
        L_0x0217:
            int r4 = r3.n
            if (r4 != 0) goto L_0x01eb
            android.graphics.Paint$Align r4 = android.graphics.Paint.Align.CENTER
            r15.setTextAlign(r4)
            int r4 = r3.b
            int r8 = r4 + r24
            int r4 = r3.c
            int r9 = r4 + r25
            goto L_0x01eb
        L_0x0229:
            int r4 = r7 / 7
            int r5 = r7 % 7
            if (r5 <= 0) goto L_0x0231
            int r4 = r4 + 1
        L_0x0231:
            int r5 = r7 % r4
            if (r5 != 0) goto L_0x026c
            int r5 = r7 / r4
        L_0x0237:
            r12 = 0
            r6 = 0
            r13 = r12
            r14 = r9
            r12 = r6
        L_0x023c:
            if (r12 >= r4) goto L_0x0271
            int r6 = r13 + r5
            if (r6 < r7) goto L_0x0243
            r6 = r7
        L_0x0243:
            java.lang.String r0 = r3.f3115a
            r26 = r0
            r0 = r26
            java.lang.String r13 = r0.substring(r13, r6)
            int r0 = r3.h
            r26 = r0
            r0 = r26
            float r0 = (float) r0
            r26 = r0
            float r0 = (float) r14
            r27 = r0
            r0 = r30
            r1 = r26
            r2 = r27
            r0.drawText(r13, r1, r2, r15)
            int r13 = r3.p
            int r13 = r13 + 4
            int r13 = r13 + r14
            int r12 = r12 + 1
            r14 = r13
            r13 = r6
            goto L_0x023c
        L_0x026c:
            int r5 = r7 / r4
            int r5 = r5 + 1
            goto L_0x0237
        L_0x0271:
            r3 = r8
            r4 = r9
            goto L_0x0208
        L_0x0274:
            if (r31 <= 0) goto L_0x02df
            r6 = -45
            if (r4 >= r6) goto L_0x02d2
            int r4 = r4 + 90
        L_0x027c:
            int r6 = -r4
            float r6 = (float) r6
            float r7 = (float) r8
            float r12 = (float) r9
            r0 = r18
            r0.postRotate(r6, r7, r12)
            r0 = r17
            float r6 = r0.top
            int r6 = (int) r6
            int r5 = r5 / 2
            int r5 = r5 + r6
            int r5 = r9 - r5
            r6 = 0
            float r7 = (float) r8
            r21[r6] = r7
            r6 = 1
            float r5 = (float) r5
            r21[r6] = r5
            r0 = r18
            r1 = r20
            r2 = r21
            r0.mapPoints(r1, r2)
            r5 = 0
            r5 = r20[r5]
            int r5 = (int) r5
            r6 = 1
            r6 = r20[r6]
            int r6 = (int) r6
            r30.save()
            r0 = r30
            r1 = r19
            r0.getMatrix(r1)
            int r4 = -r4
            float r4 = (float) r4
            float r7 = (float) r5
            float r8 = (float) r6
            r0 = r19
            r0.preRotate(r4, r7, r8)
            r0 = r30
            r1 = r19
            r0.setMatrix(r1)
            java.lang.String r3 = r3.f3115a
            float r4 = (float) r5
            float r7 = (float) r6
            r0 = r30
            r0.drawText(r3, r4, r7, r15)
            r30.restore()
            r3 = r5
            r4 = r6
            goto L_0x0208
        L_0x02d2:
            r6 = 45
            if (r4 <= r6) goto L_0x027c
            int r4 = r4 + -90
            goto L_0x027c
        L_0x02d9:
            int r3 = r10 + 1
            r10 = r3
            goto L_0x0182
        L_0x02de:
            return
        L_0x02df:
            r3 = r8
            r4 = r9
            goto L_0x0208
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mapabc.minimap.map.vmap.NativeMap.paintLables(com.mapabc.minimap.map.vmap.NativeMapEngine, android.graphics.Canvas, int):void");
    }

    public boolean paintMap(NativeMapEngine nativeMapEngine, int i) {
        return paint(nativeMapEngine, this.f3112a, i);
    }

    public void px20ToScreen(int i, int i2, be beVar) {
        nativePx20ToScreen(this.b, i, i2, beVar);
    }

    public void pxToScreen(int i, int i2, be beVar) {
        nativePxToScreen(this.b, i, i2, beVar);
    }

    public void resetLabelManager() {
        nativeResetLabelManager(this.b);
    }

    public void setDrawMode(int i) {
    }

    public void setMapLevel(int i) {
        nativeSetMapLevel(this.b, i);
    }

    public void setMapParameter(int i, int i2, int i3, int i4) {
        nativeSetMapParameter(this.b, i, i2, i3, i4);
    }
}
