package com.mapabc.minimap.map.vmap;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import com.amap.mapapi.core.n;
import java.io.File;
import mm.purchasesdk.PurchaseCode;

public class NativeMapEngine {
    public static final int ICON_HEIGHT = 12;
    public static final int ICON_WIDTH = 12;
    public static final int MAX_ICON_SIZE = 128;
    public static final int MAX_LABELAINE = 7;

    /* renamed from: a  reason: collision with root package name */
    Bitmap[] f3113a = new Bitmap[MAX_ICON_SIZE];
    int b = 0;

    static {
        try {
            System.loadLibrary(NativeMap.MINIMAP_VERSION);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public NativeMapEngine(Context context) {
        String str;
        if (!Environment.getExternalStorageState().equals("mounted")) {
            str = context.getCacheDir().toString() + "/";
        } else {
            File file = new File(Environment.getExternalStorageDirectory(), "Amap");
            if (!file.exists()) {
                file.mkdir();
            }
            File file2 = new File(file, "mini_mapv2");
            if (!file2.exists()) {
                file2.mkdir();
            }
            str = file2.toString() + "/";
        }
        this.b = nativeCreate(str);
        setBitmapCacheMaxSize(256);
        setVecotormapCacheMaxSize(PurchaseCode.LOADCHANNEL_ERR);
    }

    private static native void nativeClearBackground(int i);

    private static native int nativeCreate(String str);

    private static native void nativeFillBitmapBufferData(int i, String str, byte[] bArr);

    private static native void nativeFinalizer(int i);

    private static native int nativeGetBKColor(int i, int i2);

    private static native int nativeGetBitmapCacheSize(int i);

    private static native byte[] nativeGetGridData(int i, String str);

    private static native boolean nativeHasBitMapData(int i, String str);

    private static native boolean nativeHasGridData(int i, String str);

    private static native void nativePutBitmapData(int i, String str, byte[] bArr, int i2, int i3);

    private static native void nativePutGridData(int i, byte[] bArr, int i2, int i3);

    private static native void nativeRemoveBitmapData(int i, String str, int i2);

    private static native void nativeSetBackgroundImageData(int i, byte[] bArr);

    private static native void nativeSetBitmapCacheMaxSize(int i, int i2);

    private static native void nativeSetIconData(int i, int i2, byte[] bArr);

    private static native void nativeSetStyleData(int i, byte[] bArr);

    private static native void nativeSetVectormapCacheMaxSize(int i, int i2);

    public void clearBackground() {
        nativeClearBackground(this.b);
    }

    public void destory() {
        if (this.b != 0) {
            nativeFinalizer(this.b);
            this.b = 0;
            for (int i = 1; i < 53; i++) {
                if (this.f3113a[i] != null) {
                    this.f3113a[i].recycle();
                    this.f3113a[i] = null;
                }
            }
        }
    }

    public void fillBitmapBufferData(String str, byte[] bArr) {
        nativeFillBitmapBufferData(this.b, str, bArr);
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        destory();
    }

    public int getBKColor(int i) {
        return nativeGetBKColor(this.b, i);
    }

    public int getBitmapCacheSize() {
        return nativeGetBitmapCacheSize(this.b);
    }

    public byte[] getGridData(String str) {
        return nativeGetGridData(this.b, str);
    }

    public Bitmap getIconBitmap(int i) {
        return this.f3113a[i];
    }

    public boolean hasBitMapData(String str) {
        return nativeHasBitMapData(this.b, str);
    }

    public boolean hasGridData(String str) {
        return nativeHasGridData(this.b, str);
    }

    public void initIconData(Context context) {
        setBackgroudImageData(n.a(context, "bk.data"));
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 1;
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        for (int i = 1; i < 53; i++) {
            byte[] a2 = n.a(context, i + ".png");
            if (a2 != null) {
                this.f3113a[i] = BitmapFactory.decodeByteArray(a2, 0, a2.length, options);
            }
        }
    }

    public void initStyleData(Context context) {
        String str = "style_l.data";
        if (context.getResources().getDisplayMetrics().densityDpi == 120 || context.getResources().getDisplayMetrics().densityDpi == 160) {
            str = "style_s.data";
        }
        setStyleData(n.a(context, str));
    }

    public void putBitmapData(String str, byte[] bArr, int i, int i2) {
        nativePutBitmapData(this.b, str, bArr, i, i2);
    }

    public void putGridData(byte[] bArr, int i, int i2) {
        nativePutGridData(this.b, bArr, i, i2);
    }

    public void removeBitmapData(String str, int i) {
        nativeRemoveBitmapData(this.b, str, i);
    }

    public void setBackgroudImageData(byte[] bArr) {
        nativeSetBackgroundImageData(this.b, bArr);
    }

    public void setBitmapCacheMaxSize(int i) {
        nativeSetBitmapCacheMaxSize(this.b, i);
    }

    public void setIconData(int i, byte[] bArr) {
        nativeSetIconData(this.b, i, bArr);
    }

    public void setStyleData(byte[] bArr) {
        nativeSetStyleData(this.b, bArr);
    }

    public void setVecotormapCacheMaxSize(int i) {
        nativeSetVectormapCacheMaxSize(this.b, i);
    }
}
