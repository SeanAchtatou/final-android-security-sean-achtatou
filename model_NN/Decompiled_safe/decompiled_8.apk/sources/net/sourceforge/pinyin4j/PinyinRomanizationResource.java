package net.sourceforge.pinyin4j;

import com.amap.mapapi.poisearch.PoiTypeDef;
import com.e.a.a.b;
import com.e.a.a.k;
import com.e.a.a.m;
import java.io.FileNotFoundException;
import java.io.IOException;

class PinyinRomanizationResource {
    private b pinyinMappingDoc;

    /* renamed from: net.sourceforge.pinyin4j.PinyinRomanizationResource$1  reason: invalid class name */
    class AnonymousClass1 {
    }

    class PinyinRomanizationSystemResourceHolder {
        static final PinyinRomanizationResource theInstance = new PinyinRomanizationResource(null);

        private PinyinRomanizationSystemResourceHolder() {
        }
    }

    private PinyinRomanizationResource() {
        initializeResource();
    }

    PinyinRomanizationResource(AnonymousClass1 r1) {
        this();
    }

    static PinyinRomanizationResource getInstance() {
        return PinyinRomanizationSystemResourceHolder.theInstance;
    }

    private void initializeResource() {
        try {
            this.pinyinMappingDoc = m.a(PoiTypeDef.All, ResourceHelper.getResourceInputStream("/pinyindb/pinyin_mapping.xml"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        } catch (k e3) {
            e3.printStackTrace();
        }
    }

    private void setPinyinMappingDoc(b bVar) {
        this.pinyinMappingDoc = bVar;
    }

    /* access modifiers changed from: package-private */
    public b getPinyinMappingDoc() {
        return this.pinyinMappingDoc;
    }
}
