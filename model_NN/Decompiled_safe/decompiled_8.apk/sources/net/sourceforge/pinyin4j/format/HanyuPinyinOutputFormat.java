package net.sourceforge.pinyin4j.format;

public final class HanyuPinyinOutputFormat {
    private HanyuPinyinCaseType caseType;
    private HanyuPinyinToneType toneType;
    private HanyuPinyinVCharType vCharType;

    public HanyuPinyinOutputFormat() {
        restoreDefault();
    }

    public final HanyuPinyinCaseType getCaseType() {
        return this.caseType;
    }

    public final HanyuPinyinToneType getToneType() {
        return this.toneType;
    }

    public final HanyuPinyinVCharType getVCharType() {
        return this.vCharType;
    }

    public final void restoreDefault() {
        this.vCharType = HanyuPinyinVCharType.WITH_U_AND_COLON;
        this.caseType = HanyuPinyinCaseType.LOWERCASE;
        this.toneType = HanyuPinyinToneType.WITH_TONE_NUMBER;
    }

    public final void setCaseType(HanyuPinyinCaseType hanyuPinyinCaseType) {
        this.caseType = hanyuPinyinCaseType;
    }

    public final void setToneType(HanyuPinyinToneType hanyuPinyinToneType) {
        this.toneType = hanyuPinyinToneType;
    }

    public final void setVCharType(HanyuPinyinVCharType hanyuPinyinVCharType) {
        this.vCharType = hanyuPinyinVCharType;
    }
}
