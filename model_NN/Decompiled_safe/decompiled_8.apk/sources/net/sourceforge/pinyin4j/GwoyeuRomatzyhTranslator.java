package net.sourceforge.pinyin4j;

import com.e.a.a.d;
import com.e.a.a.k;

class GwoyeuRomatzyhTranslator {
    private static String[] tones = {"_I", "_II", "_III", "_IV", "_V"};

    GwoyeuRomatzyhTranslator() {
    }

    static String convertHanyuPinyinToGwoyeuRomatzyh(String str) {
        String extractPinyinString = TextHelper.extractPinyinString(str);
        String extractToneNumber = TextHelper.extractToneNumber(str);
        try {
            d b = GwoyeuRomatzyhResource.getInstance().getPinyinToGwoyeuMappingDoc().b(new StringBuffer("//").append(PinyinRomanizationType.HANYU_PINYIN.getTagName()).append("[text()='").append(extractPinyinString).append("']").toString());
            if (b != null) {
                return b.c(new StringBuffer("../").append(PinyinRomanizationType.GWOYEU_ROMATZYH.getTagName()).append(tones[Integer.parseInt(extractToneNumber) - 1]).append("/text()").toString());
            }
            return null;
        } catch (k e) {
            e.printStackTrace();
            return null;
        }
    }
}
