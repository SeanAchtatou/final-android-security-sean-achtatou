package net.sourceforge.pinyin4j;

import com.e.a.a.d;
import com.e.a.a.k;

class PinyinRomanizationTranslator {
    PinyinRomanizationTranslator() {
    }

    static String convertRomanizationSystem(String str, PinyinRomanizationType pinyinRomanizationType, PinyinRomanizationType pinyinRomanizationType2) {
        String extractPinyinString = TextHelper.extractPinyinString(str);
        String extractToneNumber = TextHelper.extractToneNumber(str);
        try {
            d b = PinyinRomanizationResource.getInstance().getPinyinMappingDoc().b(new StringBuffer("//").append(pinyinRomanizationType.getTagName()).append("[text()='").append(extractPinyinString).append("']").toString());
            if (b == null) {
                return null;
            }
            return new StringBuffer().append(b.c(new StringBuffer("../").append(pinyinRomanizationType2.getTagName()).append("/text()").toString())).append(extractToneNumber).toString();
        } catch (k e) {
            e.printStackTrace();
            return null;
        }
    }
}
