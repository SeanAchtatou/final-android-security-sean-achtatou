package net.sourceforge.pinyin4j;

import com.amap.mapapi.poisearch.PoiTypeDef;
import com.e.a.a.b;
import com.e.a.a.k;
import com.e.a.a.m;
import java.io.FileNotFoundException;
import java.io.IOException;

class GwoyeuRomatzyhResource {
    private b pinyinToGwoyeuMappingDoc;

    /* renamed from: net.sourceforge.pinyin4j.GwoyeuRomatzyhResource$1  reason: invalid class name */
    class AnonymousClass1 {
    }

    class GwoyeuRomatzyhSystemResourceHolder {
        static final GwoyeuRomatzyhResource theInstance = new GwoyeuRomatzyhResource(null);

        private GwoyeuRomatzyhSystemResourceHolder() {
        }
    }

    private GwoyeuRomatzyhResource() {
        initializeResource();
    }

    GwoyeuRomatzyhResource(AnonymousClass1 r1) {
        this();
    }

    static GwoyeuRomatzyhResource getInstance() {
        return GwoyeuRomatzyhSystemResourceHolder.theInstance;
    }

    private void initializeResource() {
        try {
            this.pinyinToGwoyeuMappingDoc = m.a(PoiTypeDef.All, ResourceHelper.getResourceInputStream("/pinyindb/pinyin_gwoyeu_mapping.xml"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        } catch (k e3) {
            e3.printStackTrace();
        }
    }

    private void setPinyinToGwoyeuMappingDoc(b bVar) {
        this.pinyinToGwoyeuMappingDoc = bVar;
    }

    /* access modifiers changed from: package-private */
    public b getPinyinToGwoyeuMappingDoc() {
        return this.pinyinToGwoyeuMappingDoc;
    }
}
