package a.a.c;

import java.io.InputStream;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpEntityEnclosingRequest;
import org.apache.http.client.methods.HttpUriRequest;

public final class b {

    /* renamed from: a  reason: collision with root package name */
    private HttpUriRequest f323a;
    private HttpEntity b;

    public b(HttpUriRequest httpUriRequest) {
        this.f323a = httpUriRequest;
        if (httpUriRequest instanceof HttpEntityEnclosingRequest) {
            this.b = ((HttpEntityEnclosingRequest) httpUriRequest).getEntity();
        }
    }

    public final String a() {
        return this.f323a.getRequestLine().getMethod();
    }

    public final String a(String str) {
        Header firstHeader = this.f323a.getFirstHeader(str);
        if (firstHeader == null) {
            return null;
        }
        return firstHeader.getValue();
    }

    public final void a(String str, String str2) {
        this.f323a.setHeader(str, str2);
    }

    public final String b() {
        return this.f323a.getURI().toString();
    }

    public final InputStream c() {
        if (this.b == null) {
            return null;
        }
        return this.b.getContent();
    }

    public final String d() {
        Header contentType;
        if (this.b == null || (contentType = this.b.getContentType()) == null) {
            return null;
        }
        return contentType.getValue();
    }

    public final Object e() {
        return this.f323a;
    }
}
