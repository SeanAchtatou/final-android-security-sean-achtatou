package a.a.c;

import java.io.InputStream;
import org.apache.http.HttpResponse;

public final class c {

    /* renamed from: a  reason: collision with root package name */
    private HttpResponse f324a;

    public c(HttpResponse httpResponse) {
        this.f324a = httpResponse;
    }

    public final int a() {
        return this.f324a.getStatusLine().getStatusCode();
    }

    public final String b() {
        return this.f324a.getStatusLine().getReasonPhrase();
    }

    public final InputStream c() {
        return this.f324a.getEntity().getContent();
    }

    public final Object d() {
        return this.f324a;
    }
}
