package a.a.c;

import java.io.Serializable;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;

public final class a implements Serializable, Map {

    /* renamed from: a  reason: collision with root package name */
    private TreeMap f322a = new TreeMap();

    public final String a(String str) {
        String b = b(str);
        if (b == null) {
            return null;
        }
        return str + "=\"" + b + "\"";
    }

    public final String a(String str, String str2) {
        return a(str, str2, false);
    }

    public final String a(String str, String str2, boolean z) {
        Object obj = (SortedSet) this.f322a.get(str);
        if (obj == null) {
            obj = new TreeSet();
            TreeMap treeMap = this.f322a;
            if (z) {
                str = a.a.a.a(str);
            }
            treeMap.put(str, obj);
        }
        if (str2 != null) {
            if (z) {
                str2 = a.a.a.a(str2);
            }
            obj.add(str2);
        }
        return str2;
    }

    /* renamed from: a */
    public final SortedSet get(Object obj) {
        return (SortedSet) this.f322a.get(obj);
    }

    public final void a(Map map, boolean z) {
        if (z) {
            for (String str : map.keySet()) {
                remove(str);
                for (String a2 : (SortedSet) map.get(str)) {
                    a(str, a2, true);
                }
                get((Object) str);
            }
            return;
        }
        this.f322a.putAll(map);
    }

    public final void a(String[] strArr) {
        for (int i = 0; i < strArr.length - 1; i += 2) {
            a(strArr[i], strArr[i + 1], true);
        }
    }

    public final String b(Object obj) {
        SortedSet sortedSet = (SortedSet) this.f322a.get(obj);
        if (sortedSet == null || sortedSet.isEmpty()) {
            return null;
        }
        return (String) sortedSet.first();
    }

    public final String c(Object obj) {
        StringBuilder sb = new StringBuilder();
        String a2 = a.a.a.a((String) obj);
        Set set = (Set) this.f322a.get(a2);
        if (set == null) {
            return ((Object) a2) + "=";
        }
        Iterator it = set.iterator();
        while (it.hasNext()) {
            sb.append(((Object) a2) + "=" + ((String) it.next()));
            if (it.hasNext()) {
                sb.append("&");
            }
        }
        return sb.toString();
    }

    public final void clear() {
        this.f322a.clear();
    }

    public final boolean containsKey(Object obj) {
        return this.f322a.containsKey(obj);
    }

    public final boolean containsValue(Object obj) {
        for (SortedSet contains : this.f322a.values()) {
            if (contains.contains(obj)) {
                return true;
            }
        }
        return false;
    }

    /* renamed from: d */
    public final SortedSet remove(Object obj) {
        return (SortedSet) this.f322a.remove(obj);
    }

    public final Set entrySet() {
        return this.f322a.entrySet();
    }

    public final boolean isEmpty() {
        return this.f322a.isEmpty();
    }

    public final Set keySet() {
        return this.f322a.keySet();
    }

    public final /* bridge */ /* synthetic */ Object put(Object obj, Object obj2) {
        return (SortedSet) this.f322a.put((String) obj, (SortedSet) obj2);
    }

    public final void putAll(Map map) {
        this.f322a.putAll(map);
    }

    public final int size() {
        int i = 0;
        Iterator it = this.f322a.keySet().iterator();
        while (true) {
            int i2 = i;
            if (!it.hasNext()) {
                return i2;
            }
            i = ((SortedSet) this.f322a.get((String) it.next())).size() + i2;
        }
    }

    public final Collection values() {
        return this.f322a.values();
    }
}
