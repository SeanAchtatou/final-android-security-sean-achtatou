package a.a;

import a.a.b.c;
import a.a.c.a;
import a.a.d.d;
import com.amap.mapapi.poisearch.PoiTypeDef;
import java.io.IOException;
import java.io.Serializable;
import java.util.Map;
import java.util.Random;

public abstract class b implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    private String f320a;
    private String b;
    private String c;
    private a.a.d.b d;
    private d e = new d();
    private a f;
    private a g;

    public b(String str, String str2) {
        this.f320a = str;
        this.b = str2;
        a.a.d.a aVar = new a.a.d.a();
        this.d = aVar;
        aVar.a(this.b);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.c.a.a(java.util.Map, boolean):void
     arg types: [a.a.c.a, int]
     candidates:
      a.a.c.a.a(java.lang.String, java.lang.String):java.lang.String
      a.a.c.a.a(java.util.Map, boolean):void */
    public final a.a.c.b a(a.a.c.b bVar) {
        if (this.f320a == null) {
            throw new c("consumer key not set");
        } else if (this.b == null) {
            throw new c("consumer secret not set");
        } else {
            this.g = new a();
            try {
                if (this.f != null) {
                    this.g.a((Map) this.f, false);
                }
                this.g.a((Map) a.c(bVar.a("Authorization")), false);
                a aVar = this.g;
                String b2 = bVar.b();
                int indexOf = b2.indexOf(63);
                if (indexOf >= 0) {
                    aVar.a((Map) a.b(b2.substring(indexOf + 1)), true);
                }
                a aVar2 = this.g;
                String d2 = bVar.d();
                if (d2 != null && d2.startsWith("application/x-www-form-urlencoded")) {
                    aVar2.a((Map) a.a(bVar.c()), true);
                }
                a aVar3 = this.g;
                if (!aVar3.containsKey("oauth_consumer_key")) {
                    aVar3.a("oauth_consumer_key", this.f320a, true);
                }
                if (!aVar3.containsKey("oauth_signature_method")) {
                    aVar3.a("oauth_signature_method", this.d.a(), true);
                }
                if (!aVar3.containsKey("oauth_timestamp")) {
                    aVar3.a("oauth_timestamp", Long.toString(System.currentTimeMillis() / 1000), true);
                }
                if (!aVar3.containsKey("oauth_nonce")) {
                    aVar3.a("oauth_nonce", Long.toString(new Random().nextLong()), true);
                }
                if (!aVar3.containsKey("oauth_version")) {
                    aVar3.a("oauth_version", "1.0", true);
                }
                if (!aVar3.containsKey("oauth_token") && this.c != null && !this.c.equals(PoiTypeDef.All)) {
                    aVar3.a("oauth_token", this.c, true);
                }
                this.g.remove("oauth_signature");
                String a2 = this.d.a(bVar, this.g);
                a.b("signature", a2);
                d dVar = this.e;
                a aVar4 = this.g;
                StringBuilder sb = new StringBuilder();
                sb.append("OAuth ");
                if (aVar4.containsKey("realm")) {
                    sb.append(aVar4.a("realm"));
                    sb.append(", ");
                }
                if (aVar4.containsKey("oauth_token")) {
                    sb.append(aVar4.a("oauth_token"));
                    sb.append(", ");
                }
                if (aVar4.containsKey("oauth_callback")) {
                    sb.append(aVar4.a("oauth_callback"));
                    sb.append(", ");
                }
                if (aVar4.containsKey("oauth_verifier")) {
                    sb.append(aVar4.a("oauth_verifier"));
                    sb.append(", ");
                }
                sb.append(aVar4.a("oauth_consumer_key"));
                sb.append(", ");
                sb.append(aVar4.a("oauth_version"));
                sb.append(", ");
                sb.append(aVar4.a("oauth_signature_method"));
                sb.append(", ");
                sb.append(aVar4.a("oauth_timestamp"));
                sb.append(", ");
                sb.append(aVar4.a("oauth_nonce"));
                sb.append(", ");
                sb.append(a.a("oauth_signature", a2));
                bVar.a("Authorization", sb.toString());
                a.b("Auth header", bVar.a("Authorization"));
                a.b("Request URL", bVar.b());
                return bVar;
            } catch (IOException e2) {
                throw new a.a.b.a(e2);
            }
        }
    }

    public final String a() {
        return this.c;
    }

    public final void a(a aVar) {
        this.f = aVar;
    }

    public final void a(String str, String str2) {
        this.c = str;
        this.d.b(str2);
    }

    public final String b() {
        return this.d.c();
    }

    public final String c() {
        return this.f320a;
    }

    public final String d() {
        return this.b;
    }
}
