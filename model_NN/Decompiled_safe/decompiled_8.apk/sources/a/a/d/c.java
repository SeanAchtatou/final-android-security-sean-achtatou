package a.a.d;

import a.a.b.d;
import a.a.c.a;
import a.a.c.b;
import com.amap.mapapi.poisearch.PoiTypeDef;
import java.net.URI;
import java.util.Iterator;

public final class c {

    /* renamed from: a  reason: collision with root package name */
    private b f326a;
    private a b;

    public c(b bVar, a aVar) {
        this.f326a = bVar;
        this.b = aVar;
    }

    private String b() {
        if (this.b == null) {
            return PoiTypeDef.All;
        }
        StringBuilder sb = new StringBuilder();
        Iterator it = this.b.keySet().iterator();
        int i = 0;
        while (true) {
            int i2 = i;
            if (!it.hasNext()) {
                return sb.toString();
            }
            String str = (String) it.next();
            if (!"oauth_signature".equals(str) && !"realm".equals(str)) {
                if (i2 > 0) {
                    sb.append("&");
                }
                sb.append(this.b.c(str));
            }
            i = i2 + 1;
        }
    }

    public final String a() {
        int lastIndexOf;
        boolean z = false;
        try {
            URI uri = new URI(this.f326a.b());
            String lowerCase = uri.getScheme().toLowerCase();
            String lowerCase2 = uri.getAuthority().toLowerCase();
            if ((lowerCase.equals("http") && uri.getPort() == 80) || (lowerCase.equals("https") && uri.getPort() == 443)) {
                z = true;
            }
            String substring = (!z || (lastIndexOf = lowerCase2.lastIndexOf(":")) < 0) ? lowerCase2 : lowerCase2.substring(0, lastIndexOf);
            String rawPath = uri.getRawPath();
            if (rawPath == null || rawPath.length() <= 0) {
                rawPath = "/";
            }
            return this.f326a.a() + '&' + a.a.a.a(lowerCase + "://" + substring + rawPath) + '&' + a.a.a.a(b());
        } catch (Exception e) {
            throw new d(e);
        }
    }
}
