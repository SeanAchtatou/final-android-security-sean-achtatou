package a.a.d;

import a.a.c.a;
import java.io.Serializable;
import org.apache.commons.codec.binary.Base64;

public abstract class b implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    private transient Base64 f325a = new Base64();
    private String b;
    private String c;

    public abstract String a();

    public abstract String a(a.a.c.b bVar, a aVar);

    /* access modifiers changed from: protected */
    public final String a(byte[] bArr) {
        return new String(this.f325a.encode(bArr));
    }

    public final void a(String str) {
        this.b = str;
    }

    public final String b() {
        return this.b;
    }

    public final void b(String str) {
        this.c = str;
    }

    public final String c() {
        return this.c;
    }
}
