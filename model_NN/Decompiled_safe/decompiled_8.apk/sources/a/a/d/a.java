package a.a.d;

import a.a.b.d;
import a.a.c.b;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

public final class a extends b {
    public final String a() {
        return "HMAC-SHA1";
    }

    public final String a(b bVar, a.a.c.a aVar) {
        try {
            SecretKeySpec secretKeySpec = new SecretKeySpec((a.a.a.a(b()) + '&' + a.a.a.a(c())).getBytes("UTF-8"), "HmacSHA1");
            Mac instance = Mac.getInstance("HmacSHA1");
            instance.init(secretKeySpec);
            String a2 = new c(bVar, aVar).a();
            a.a.a.b("SBS", a2);
            return a(instance.doFinal(a2.getBytes("UTF-8"))).trim();
        } catch (GeneralSecurityException e) {
            throw new d(e);
        } catch (UnsupportedEncodingException e2) {
            throw new d(e2);
        }
    }
}
