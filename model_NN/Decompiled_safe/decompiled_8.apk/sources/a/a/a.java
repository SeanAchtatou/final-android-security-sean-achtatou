package a.a;

import com.amap.mapapi.poisearch.PoiTypeDef;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

public final class a {

    /* renamed from: a  reason: collision with root package name */
    private static final com.d.a.a.a.a.a f318a = new com.d.a.a.a.a.a("-._~");

    public static a.a.c.a a(InputStream inputStream) {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder sb = new StringBuilder();
        for (String readLine = bufferedReader.readLine(); readLine != null; readLine = bufferedReader.readLine()) {
            sb.append(readLine);
        }
        return b(sb.toString());
    }

    public static String a(String str) {
        return str == null ? PoiTypeDef.All : f318a.a(str);
    }

    public static String a(String str, String str2) {
        return a(str) + "=\"" + a(str2) + "\"";
    }

    public static String a(String str, String... strArr) {
        StringBuilder sb = new StringBuilder(str + (str.contains("?") ? "&" : "?"));
        for (int i = 0; i < strArr.length; i += 2) {
            if (i > 0) {
                sb.append("&");
            }
            sb.append(a(strArr[i]) + "=" + a(strArr[i + 1]));
        }
        return sb.toString();
    }

    public static a.a.c.a b(String str) {
        String d;
        String d2;
        a.a.c.a aVar = new a.a.c.a();
        if (str == null || str.length() == 0) {
            return aVar;
        }
        for (String str2 : str.split("\\&")) {
            int indexOf = str2.indexOf(61);
            if (indexOf < 0) {
                d = d(str2);
                d2 = null;
            } else {
                d = d(str2.substring(0, indexOf));
                d2 = d(str2.substring(indexOf + 1));
            }
            aVar.a(d, d2);
        }
        return aVar;
    }

    public static void b(String str, String str2) {
        if (System.getProperty("debug") != null) {
            System.out.println("[SIGNPOST] " + str + ": " + str2);
        }
    }

    public static a.a.c.a c(String str) {
        a.a.c.a aVar = new a.a.c.a();
        if (str != null && str.startsWith("OAuth ")) {
            for (String split : str.substring(6).split(",")) {
                String[] split2 = split.split("=");
                aVar.a(split2[0].trim(), split2[1].replace("\"", PoiTypeDef.All).trim());
            }
        }
        return aVar;
    }

    private static String d(String str) {
        if (str == null) {
            return PoiTypeDef.All;
        }
        try {
            return URLDecoder.decode(str, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }
}
