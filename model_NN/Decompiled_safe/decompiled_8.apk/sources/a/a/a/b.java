package a.a.a;

import a.a.c;
import java.io.IOException;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;

public final class b extends c {

    /* renamed from: a  reason: collision with root package name */
    private transient HttpClient f319a = new DefaultHttpClient();

    public b(String str, String str2, String str3) {
        super(str, str2, str3);
    }

    /* access modifiers changed from: protected */
    public final a.a.c.b a(String str) {
        return new a.a.c.b(new HttpPost(str));
    }

    /* access modifiers changed from: protected */
    public final a.a.c.c a(a.a.c.b bVar) {
        return new a.a.c.c(this.f319a.execute((HttpUriRequest) bVar.e()));
    }

    /* access modifiers changed from: protected */
    public final void a(a.a.c.c cVar) {
        HttpEntity entity;
        if (cVar != null && (entity = ((HttpResponse) cVar.d()).getEntity()) != null) {
            try {
                entity.consumeContent();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
