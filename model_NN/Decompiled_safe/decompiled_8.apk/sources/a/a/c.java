package a.a;

import a.a.c.a;
import a.a.c.b;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public abstract class c implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    private String f321a;
    private String b;
    private String c;
    private a d = new a();
    private Map e = new HashMap();
    private boolean f;

    public c(String str, String str2, String str3) {
        this.f321a = str;
        this.b = str2;
        this.c = str3;
    }

    /* JADX WARNING: Unknown top exception splitter block from list: {B:48:0x00ea=Splitter:B:48:0x00ea, B:14:0x003b=Splitter:B:14:0x003b, B:39:0x00bc=Splitter:B:39:0x00bc} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(a.a.b r7, java.lang.String r8, java.lang.String... r9) {
        /*
            r6 = this;
            java.util.Map r3 = r6.e
            java.lang.String r0 = r7.c()
            if (r0 == 0) goto L_0x000e
            java.lang.String r0 = r7.d()
            if (r0 != 0) goto L_0x0016
        L_0x000e:
            a.a.b.c r0 = new a.a.b.c
            java.lang.String r1 = "Consumer key or secret not set"
            r0.<init>(r1)
            throw r0
        L_0x0016:
            r2 = 0
            a.a.c.b r4 = r6.a(r8)     // Catch:{ e -> 0x0039, c -> 0x010c, Exception -> 0x0109, all -> 0x0105 }
            java.util.Set r0 = r3.keySet()     // Catch:{ e -> 0x0039, c -> 0x010c, Exception -> 0x0109, all -> 0x0105 }
            java.util.Iterator r5 = r0.iterator()     // Catch:{ e -> 0x0039, c -> 0x010c, Exception -> 0x0109, all -> 0x0105 }
        L_0x0023:
            boolean r0 = r5.hasNext()     // Catch:{ e -> 0x0039, c -> 0x010c, Exception -> 0x0109, all -> 0x0105 }
            if (r0 == 0) goto L_0x0041
            java.lang.Object r0 = r5.next()     // Catch:{ e -> 0x0039, c -> 0x010c, Exception -> 0x0109, all -> 0x0105 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ e -> 0x0039, c -> 0x010c, Exception -> 0x0109, all -> 0x0105 }
            java.lang.Object r1 = r3.get(r0)     // Catch:{ e -> 0x0039, c -> 0x010c, Exception -> 0x0109, all -> 0x0105 }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ e -> 0x0039, c -> 0x010c, Exception -> 0x0109, all -> 0x0105 }
            r4.a(r0, r1)     // Catch:{ e -> 0x0039, c -> 0x010c, Exception -> 0x0109, all -> 0x0105 }
            goto L_0x0023
        L_0x0039:
            r0 = move-exception
            r1 = r2
        L_0x003b:
            throw r0     // Catch:{ all -> 0x003c }
        L_0x003c:
            r0 = move-exception
        L_0x003d:
            r6.a(r1)     // Catch:{ Exception -> 0x00fe }
            throw r0
        L_0x0041:
            if (r9 == 0) goto L_0x004e
            a.a.c.a r0 = new a.a.c.a     // Catch:{ e -> 0x0039, c -> 0x010c, Exception -> 0x0109, all -> 0x0105 }
            r0.<init>()     // Catch:{ e -> 0x0039, c -> 0x010c, Exception -> 0x0109, all -> 0x0105 }
            r0.a(r9)     // Catch:{ e -> 0x0039, c -> 0x010c, Exception -> 0x0109, all -> 0x0105 }
            r7.a(r0)     // Catch:{ e -> 0x0039, c -> 0x010c, Exception -> 0x0109, all -> 0x0105 }
        L_0x004e:
            r7.a(r4)     // Catch:{ e -> 0x0039, c -> 0x010c, Exception -> 0x0109, all -> 0x0105 }
            a.a.c.c r1 = r6.a(r4)     // Catch:{ e -> 0x0039, c -> 0x010c, Exception -> 0x0109, all -> 0x0105 }
            int r2 = r1.a()     // Catch:{ e -> 0x00af, c -> 0x00bb, Exception -> 0x00e9 }
            r0 = 300(0x12c, float:4.2E-43)
            if (r2 < r0) goto L_0x00bd
            if (r1 == 0) goto L_0x00bd
            java.io.BufferedReader r3 = new java.io.BufferedReader     // Catch:{ e -> 0x00af, c -> 0x00bb, Exception -> 0x00e9 }
            java.io.InputStreamReader r0 = new java.io.InputStreamReader     // Catch:{ e -> 0x00af, c -> 0x00bb, Exception -> 0x00e9 }
            java.io.InputStream r4 = r1.c()     // Catch:{ e -> 0x00af, c -> 0x00bb, Exception -> 0x00e9 }
            r0.<init>(r4)     // Catch:{ e -> 0x00af, c -> 0x00bb, Exception -> 0x00e9 }
            r3.<init>(r0)     // Catch:{ e -> 0x00af, c -> 0x00bb, Exception -> 0x00e9 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ e -> 0x00af, c -> 0x00bb, Exception -> 0x00e9 }
            r4.<init>()     // Catch:{ e -> 0x00af, c -> 0x00bb, Exception -> 0x00e9 }
            java.lang.String r0 = r3.readLine()     // Catch:{ e -> 0x00af, c -> 0x00bb, Exception -> 0x00e9 }
        L_0x0076:
            if (r0 == 0) goto L_0x0080
            r4.append(r0)     // Catch:{ e -> 0x00af, c -> 0x00bb, Exception -> 0x00e9 }
            java.lang.String r0 = r3.readLine()     // Catch:{ e -> 0x00af, c -> 0x00bb, Exception -> 0x00e9 }
            goto L_0x0076
        L_0x0080:
            switch(r2) {
                case 401: goto L_0x00b1;
                default: goto L_0x0083;
            }     // Catch:{ e -> 0x00af, c -> 0x00bb, Exception -> 0x00e9 }
        L_0x0083:
            a.a.b.a r0 = new a.a.b.a     // Catch:{ e -> 0x00af, c -> 0x00bb, Exception -> 0x00e9 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ e -> 0x00af, c -> 0x00bb, Exception -> 0x00e9 }
            java.lang.String r5 = "Service provider responded in error: "
            r3.<init>(r5)     // Catch:{ e -> 0x00af, c -> 0x00bb, Exception -> 0x00e9 }
            java.lang.StringBuilder r2 = r3.append(r2)     // Catch:{ e -> 0x00af, c -> 0x00bb, Exception -> 0x00e9 }
            java.lang.String r3 = " ("
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ e -> 0x00af, c -> 0x00bb, Exception -> 0x00e9 }
            java.lang.String r3 = r1.b()     // Catch:{ e -> 0x00af, c -> 0x00bb, Exception -> 0x00e9 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ e -> 0x00af, c -> 0x00bb, Exception -> 0x00e9 }
            java.lang.String r3 = ")"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ e -> 0x00af, c -> 0x00bb, Exception -> 0x00e9 }
            java.lang.String r2 = r2.toString()     // Catch:{ e -> 0x00af, c -> 0x00bb, Exception -> 0x00e9 }
            r4.toString()     // Catch:{ e -> 0x00af, c -> 0x00bb, Exception -> 0x00e9 }
            r0.<init>(r2)     // Catch:{ e -> 0x00af, c -> 0x00bb, Exception -> 0x00e9 }
            throw r0     // Catch:{ e -> 0x00af, c -> 0x00bb, Exception -> 0x00e9 }
        L_0x00af:
            r0 = move-exception
            goto L_0x003b
        L_0x00b1:
            a.a.b.e r0 = new a.a.b.e     // Catch:{ e -> 0x00af, c -> 0x00bb, Exception -> 0x00e9 }
            r4.toString()     // Catch:{ e -> 0x00af, c -> 0x00bb, Exception -> 0x00e9 }
            r2 = 0
            r0.<init>(r2)     // Catch:{ e -> 0x00af, c -> 0x00bb, Exception -> 0x00e9 }
            throw r0     // Catch:{ e -> 0x00af, c -> 0x00bb, Exception -> 0x00e9 }
        L_0x00bb:
            r0 = move-exception
        L_0x00bc:
            throw r0     // Catch:{ all -> 0x003c }
        L_0x00bd:
            java.io.InputStream r0 = r1.c()     // Catch:{ e -> 0x00af, c -> 0x00bb, Exception -> 0x00e9 }
            a.a.c.a r0 = a.a.a.a(r0)     // Catch:{ e -> 0x00af, c -> 0x00bb, Exception -> 0x00e9 }
            java.lang.String r2 = "oauth_token"
            java.lang.String r2 = r0.b(r2)     // Catch:{ e -> 0x00af, c -> 0x00bb, Exception -> 0x00e9 }
            java.lang.String r3 = "oauth_token_secret"
            java.lang.String r3 = r0.b(r3)     // Catch:{ e -> 0x00af, c -> 0x00bb, Exception -> 0x00e9 }
            java.lang.String r4 = "oauth_token"
            r0.remove(r4)     // Catch:{ e -> 0x00af, c -> 0x00bb, Exception -> 0x00e9 }
            java.lang.String r4 = "oauth_token_secret"
            r0.remove(r4)     // Catch:{ e -> 0x00af, c -> 0x00bb, Exception -> 0x00e9 }
            r6.d = r0     // Catch:{ e -> 0x00af, c -> 0x00bb, Exception -> 0x00e9 }
            if (r2 == 0) goto L_0x00e1
            if (r3 != 0) goto L_0x00f0
        L_0x00e1:
            a.a.b.c r0 = new a.a.b.c     // Catch:{ e -> 0x00af, c -> 0x00bb, Exception -> 0x00e9 }
            java.lang.String r2 = "Request token or token secret not set in server reply. The service provider you use is probably buggy."
            r0.<init>(r2)     // Catch:{ e -> 0x00af, c -> 0x00bb, Exception -> 0x00e9 }
            throw r0     // Catch:{ e -> 0x00af, c -> 0x00bb, Exception -> 0x00e9 }
        L_0x00e9:
            r0 = move-exception
        L_0x00ea:
            a.a.b.a r2 = new a.a.b.a     // Catch:{ all -> 0x003c }
            r2.<init>(r0)     // Catch:{ all -> 0x003c }
            throw r2     // Catch:{ all -> 0x003c }
        L_0x00f0:
            r7.a(r2, r3)     // Catch:{ e -> 0x00af, c -> 0x00bb, Exception -> 0x00e9 }
            r6.a(r1)     // Catch:{ Exception -> 0x00f7 }
            return
        L_0x00f7:
            r0 = move-exception
            a.a.b.a r1 = new a.a.b.a
            r1.<init>(r0)
            throw r1
        L_0x00fe:
            r0 = move-exception
            a.a.b.a r1 = new a.a.b.a
            r1.<init>(r0)
            throw r1
        L_0x0105:
            r0 = move-exception
            r1 = r2
            goto L_0x003d
        L_0x0109:
            r0 = move-exception
            r1 = r2
            goto L_0x00ea
        L_0x010c:
            r0 = move-exception
            r1 = r2
            goto L_0x00bc
        */
        throw new UnsupportedOperationException("Method not decompiled: a.a.c.a(a.a.b, java.lang.String, java.lang.String[]):void");
    }

    public final a a() {
        return this.d;
    }

    /* access modifiers changed from: protected */
    public abstract b a(String str);

    /* access modifiers changed from: protected */
    public abstract a.a.c.c a(b bVar);

    public final String a(b bVar, String str) {
        bVar.a(null, null);
        a(bVar, this.f321a, "oauth_callback", str);
        String b2 = this.d.b("oauth_callback_confirmed");
        this.d.remove("oauth_callback_confirmed");
        this.f = Boolean.TRUE.toString().equals(b2);
        if (this.f) {
            return a.a(this.c, "oauth_token", bVar.a());
        }
        return a.a(this.c, "oauth_token", bVar.a(), "oauth_callback", str);
    }

    /* access modifiers changed from: protected */
    public void a(a.a.c.c cVar) {
    }

    public final void b() {
        this.f = true;
    }

    public final void b(b bVar, String str) {
        if (bVar.a() == null || bVar.b() == null) {
            throw new a.a.b.c("Authorized request token or token secret not set. Did you retrieve an authorized request token before?");
        } else if (!this.f || str == null) {
            a(bVar, this.b, new String[0]);
        } else {
            a(bVar, this.b, "oauth_verifier", str);
        }
    }
}
