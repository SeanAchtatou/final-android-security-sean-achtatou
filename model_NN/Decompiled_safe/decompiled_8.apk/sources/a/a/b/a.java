package a.a.b;

public final class a extends b {
    public a(Exception exc) {
        super("Communication with the service provider failed: " + exc.getLocalizedMessage(), exc);
    }

    public a(String str) {
        super(str);
    }
}
