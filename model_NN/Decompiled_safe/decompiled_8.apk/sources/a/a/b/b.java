package a.a.b;

public abstract class b extends Exception {
    public b(String str) {
        super(str);
    }

    public b(String str, Throwable th) {
        super(str, th);
    }

    public b(Throwable th) {
        super(th);
    }
}
