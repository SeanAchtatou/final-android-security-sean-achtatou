package mm.purchasesdk;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import mm.purchasesdk.l.e;

class h extends Handler {
    final /* synthetic */ g b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    h(g gVar, Looper looper) {
        super(looper);
        this.b = gVar;
    }

    public void handleMessage(Message message) {
        b bVar = (b) message.obj;
        e.c("TaskThread", "ReqHandler Handler id:" + Thread.currentThread().getId());
        e.c("TaskThread", "ReqHandler Handler name:" + Thread.currentThread().getName());
        boolean unused = g.c = false;
        switch (message.what) {
            case 0:
                this.b.m290a(bVar);
                break;
            case 1:
                this.b.b(bVar);
                break;
            case 2:
                int i = message.arg1;
                Bundle data = message.getData();
                if (i != 0) {
                    if (i != 2) {
                        boolean unused2 = g.c = true;
                        this.b.a(bVar, data);
                        break;
                    } else {
                        this.b.b(bVar);
                        break;
                    }
                } else {
                    this.b.a(bVar);
                    break;
                }
            case 3:
                this.b.d(bVar);
                break;
            case 4:
                this.b.e(bVar);
                break;
        }
        super.handleMessage(message);
    }
}
