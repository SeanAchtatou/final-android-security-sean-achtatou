package mm.purchasesdk;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import com.ccit.mmwlan.MMClientSDK_ForPad;
import com.ccit.mmwlan.phone.MMClientSDK_ForPhone;
import java.util.HashMap;
import mm.purchasesdk.d.a;
import mm.purchasesdk.l.c;
import mm.purchasesdk.l.d;
import mm.purchasesdk.l.e;
import mm.purchasesdk.l.g;
import mm.purchasesdk.ui.u;

public class f {

    /* renamed from: a  reason: collision with root package name */
    private static boolean f3143a = false;
    private static boolean b = false;

    public static int a(Context context) {
        if (f3143a) {
            return 0;
        }
        mm.purchasesdk.l.f.X(context.getPackageName());
        d.g(c.d());
        d.d(Boolean.valueOf(g.m300a(context)));
        String a2 = a.a(context);
        if (a2 == null) {
            return PurchaseCode.LOADCHANNEL_ERR;
        }
        d.N(a2);
        mm.purchasesdk.fingerprint.c.init();
        int b2 = b(context);
        if (b2 != 0) {
            return b2;
        }
        f3143a = true;
        return 0;
    }

    public static void a(Handler handler, Handler handler2, b bVar, int i) {
        switch (d.ab) {
            case 0:
                if (d.g().booleanValue() || !d.d()) {
                    bVar.onInitFinish(i);
                    return;
                } else {
                    bVar.onInitFinish(100);
                    return;
                }
            case 1:
                if (i == 100) {
                    Message obtainMessage = handler.obtainMessage();
                    obtainMessage.what = 1;
                    obtainMessage.obj = bVar;
                    obtainMessage.sendToTarget();
                    return;
                } else if (i == 6) {
                    a(bVar, handler, handler2);
                    return;
                } else {
                    bVar.onQueryFinish(i, null);
                    return;
                }
            case 2:
                if (i == 100) {
                    Message obtainMessage2 = handler.obtainMessage();
                    obtainMessage2.what = 2;
                    obtainMessage2.obj = bVar;
                    obtainMessage2.arg1 = 0;
                    obtainMessage2.sendToTarget();
                    return;
                } else if (i == 6) {
                    a(bVar, handler, handler2);
                    return;
                } else {
                    bVar.a(i, null);
                    return;
                }
            case 3:
                if (i == 100) {
                    Message obtainMessage3 = handler.obtainMessage();
                    obtainMessage3.what = 3;
                    obtainMessage3.obj = bVar;
                    obtainMessage3.sendToTarget();
                    return;
                }
                bVar.onUnsubscribeFinish(i);
                return;
            default:
                return;
        }
    }

    public static void a(b bVar, int i) {
        if (i == 101) {
            HashMap hashMap = new HashMap();
            String Y = d.Y();
            hashMap.put(OnPurchaseListener.PAYCODE, d.H());
            if (!(Y == null || Y.trim().length() == 0)) {
                hashMap.put(OnPurchaseListener.ORDERID, Y);
            }
            String b2 = d.b();
            if (!(b2 == null || b2.trim().length() == 0)) {
                hashMap.put(OnPurchaseListener.LEFTDAY, b2);
            }
            String X = d.X();
            if (!(X == null || X.trim().length() == 0)) {
                hashMap.put(OnPurchaseListener.TRADEID, X);
            }
            bVar.onQueryFinish(i, hashMap);
            return;
        }
        bVar.onQueryFinish(i, null);
    }

    private static void a(b bVar, Handler handler, Handler handler2) {
        new mm.purchasesdk.c.a(d.getContext(), bVar, handler, handler2).show();
    }

    private static int b(Context context) {
        if (!b) {
            try {
                int initialMMSDK = d.g().booleanValue() ? MMClientSDK_ForPhone.initialMMSDK(context, g.a()) : MMClientSDK_ForPad.initialMMSDK(context, g.m299a());
                if (initialMMSDK == 1) {
                    return PurchaseCode.APPLYCERT_IMEI_ERR;
                }
                if (initialMMSDK == 2) {
                    return PurchaseCode.APPLYCERT_APP_ERR;
                }
                if (initialMMSDK == 3) {
                    return PurchaseCode.APPLYCERT_CONFIG_ERR;
                }
                if (initialMMSDK == 4) {
                    return PurchaseCode.APPLYCERT_VALUE_ERR;
                }
                if (d.g().booleanValue()) {
                    d.Q(MMClientSDK_ForPhone.getIMSI());
                    d.R(MMClientSDK_ForPhone.getDeviceID());
                } else {
                    d.Q(MMClientSDK_ForPad.getIMSI_PAD());
                    d.R(MMClientSDK_ForPad.getDeviceID_PAD());
                }
                b = true;
            } catch (Exception e) {
                e.a("InitCASDK", "init mmclientsdk failure", e);
                return PurchaseCode.APPLYCERT_OTHER_ERR;
            }
        }
        return 0;
    }

    public static void b() {
        u.a().b(g.a());
    }

    public static void b(Handler handler, Handler handler2, b bVar, int i) {
        if (102 == i || 104 == i || 101 == i) {
            if (102 == i) {
                mm.purchasesdk.f.a.l();
                mm.purchasesdk.f.a.m();
            }
            HashMap hashMap = new HashMap();
            hashMap.put(OnPurchaseListener.PAYCODE, d.H());
            String Y = d.Y();
            if (!(Y == null || Y.trim().length() == 0)) {
                hashMap.put(OnPurchaseListener.ORDERID, Y);
            }
            String b2 = d.b();
            if (!(b2 == null || b2.trim().length() == 0)) {
                hashMap.put(OnPurchaseListener.LEFTDAY, b2);
            }
            String X = d.X();
            if (!(X == null || X.trim().length() == 0)) {
                hashMap.put(OnPurchaseListener.TRADEID, X);
            }
            String n = d.n();
            if (!(n == null || n.trim().length() == 0)) {
                hashMap.put(OnPurchaseListener.ORDERTYPE, n);
            }
            if (i == 102 || 101 == i) {
                bVar.a(i, hashMap);
            } else if (i == 104) {
                bVar.a((int) PurchaseCode.AUTH_OK);
                bVar.a(hashMap);
                bVar.a();
            }
        } else if (i == 240) {
            mm.purchasesdk.f.a.l();
            mm.purchasesdk.f.a.m();
            u.a().a(handler, handler2, bVar, g.a());
        } else if (i == 422) {
            u.a().a(i, d.E(), bVar, handler, handler2, null);
        } else {
            HashMap hashMap2 = g.m291a() ? new HashMap() : null;
            e.e("PurchaseInternal", " order fail =" + i);
            mm.purchasesdk.f.a.l();
            c.onEventRT(d.getContext(), d.F(), null);
            bVar.a(i, hashMap2);
        }
    }

    public static int c(Context context) {
        String F = d.F();
        String G = d.G();
        if (F == null || F.trim().length() == 0) {
            e.e("PurchaseInternal", "appid is null");
            return PurchaseCode.PARAMETER_ERR;
        } else if (G == null || G.trim().length() == 0) {
            e.e("PurchaseInternal", "appkey is null");
            return PurchaseCode.PARAMETER_ERR;
        } else {
            String b2 = mm.purchasesdk.g.d.b(context);
            if (b2 == null) {
                return PurchaseCode.PARAMETER_ERR;
            }
            d.V(b2.trim());
            d.b(F, G);
            if (!g.b(context)) {
                return PurchaseCode.NONE_NETWORK;
            }
            d.d(Boolean.valueOf(g.m300a(context)));
            return 0;
        }
    }
}
