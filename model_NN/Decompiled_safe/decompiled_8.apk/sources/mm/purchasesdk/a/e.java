package mm.purchasesdk.a;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import mm.purchasesdk.PurchaseCode;
import mm.purchasesdk.fingerprint.IdentifyApp;
import mm.purchasesdk.fingerprint.a;
import mm.purchasesdk.fingerprint.c;
import mm.purchasesdk.g.b;
import mm.purchasesdk.l.d;

public class e {
    private static final String TAG = e.class.getSimpleName();

    public static int a(String str, String str2, String str3) {
        String str4;
        boolean z = true;
        b bVar = new b();
        String str5 = d.F() + "&" + d.L();
        mm.purchasesdk.l.e.c(TAG, "KEY--〉" + str5);
        try {
            byte[] decrypt = IdentifyApp.decrypt(c.base64decode(str), new String(str5));
            if (decrypt == null) {
                mm.purchasesdk.l.e.e(TAG, "Base64 decrypt license file failure,auth file is null,code=241");
                str4 = str;
            } else {
                str4 = "<MM_User_Authorization>" + new String(decrypt) + "</MM_User_Authorization>";
                mm.purchasesdk.l.e.c(TAG, "after ndk decode: " + str4);
            }
            try {
                mm.purchasesdk.l.e.c(TAG, str4);
                a a2 = b.a(str4.getBytes(), "utf-8");
                if (a2 == null) {
                    return PurchaseCode.AUTH_VALIDATE_FAIL;
                }
                if (!(d.H().equals(a2.m) && d.F().equals(a2.f) && d.L().equals(a2.d))) {
                    bVar.a(d.F(), d.H(), d.L());
                    mm.purchasesdk.l.e.e(TAG, "Auth validate failed! paycode or appid or imsi error,code=242");
                    return PurchaseCode.AUTH_VALIDATE_FAIL;
                } else if (!a(str4, str2)) {
                    bVar.a(d.F(), d.H(), d.L());
                    mm.purchasesdk.l.e.e(TAG, "Auth validate failed! verify auth failed,code=242");
                    return PurchaseCode.AUTH_VALIDATE_FAIL;
                } else {
                    long j = a2.b;
                    long timeInMillis = Calendar.getInstance().getTimeInMillis();
                    a(j);
                    a(timeInMillis);
                    if (timeInMillis < j) {
                        bVar.a(d.F(), d.H(), d.L());
                    }
                    if (a2.b - a2.f3124a <= 10000) {
                        z = false;
                    }
                    a(a2.b);
                    a(a2.f3124a);
                    if (d.e() && z) {
                        bVar.a(d.F(), d.H(), str, a2.b(), a2.a(), str3, d.L());
                    }
                    return PurchaseCode.AUTH_OK;
                }
            } catch (Exception e) {
                mm.purchasesdk.l.e.a(TAG, "parsed auth xml file failed!", e);
                d.setErrMsg(null);
                bVar.a(d.F(), d.H(), d.L());
                return PurchaseCode.AUTH_PARSE_FAIL;
            }
        } catch (Exception e2) {
            mm.purchasesdk.l.e.e(TAG, "base64 decrypt license file failure");
            return PurchaseCode.AUTH_PARSE_FAIL;
        }
    }

    public static void a(long j) {
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.setTimeInMillis(j);
        mm.purchasesdk.l.e.c(TAG, "timer: " + new SimpleDateFormat().format(gregorianCalendar.getTime()));
    }

    public static boolean a(String str, String str2) {
        mm.purchasesdk.l.e.c(TAG, "verificationAuthSign certificate is: " + str2);
        int indexOf = str.indexOf("<Signature_content>");
        int indexOf2 = str.indexOf("</Signature_content>") + 20;
        if (indexOf < 0) {
            return false;
        }
        String substring = str.substring(indexOf, indexOf2);
        mm.purchasesdk.l.e.c(TAG, "verificationAuthSign signContent is: " + substring);
        int indexOf3 = str.indexOf("<SignatureValue>");
        if (indexOf3 < 0) {
            return false;
        }
        String substring2 = str.substring(indexOf3 + 16, str.indexOf("</SignatureValue>"));
        mm.purchasesdk.l.e.c(TAG, "verificationAuthSign signValue is: " + substring2);
        try {
            int a2 = a.a((int) PurchaseCode.AUTH_NO_ABILITY, str2.getBytes(), substring.getBytes(), substring2.getBytes());
            mm.purchasesdk.l.e.c(TAG, "verificationAuthSign verifyWithCert result is: " + a2);
            return a2 == 0;
        } catch (mm.purchasesdk.fingerprint.b e) {
            mm.purchasesdk.l.e.a(TAG, "verify failed", e);
            return false;
        } catch (Exception e2) {
            mm.purchasesdk.l.e.e(TAG, "base64 decrypt license file failure");
            return false;
        }
    }

    public static String b(String str) {
        String substring = str.substring("<MM_User_Authorization>".length() + str.indexOf("<MM_User_Authorization>"), str.indexOf("</MM_User_Authorization>"));
        mm.purchasesdk.l.e.c(TAG, "before ndk decode: " + substring);
        return substring;
    }
}
