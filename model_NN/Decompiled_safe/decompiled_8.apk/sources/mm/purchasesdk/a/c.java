package mm.purchasesdk.a;

import android.util.Log;
import android.util.Xml;
import com.amap.mapapi.poisearch.PoiTypeDef;
import java.io.StringWriter;
import java.util.Calendar;
import mm.purchasesdk.OnPurchaseListener;
import mm.purchasesdk.PurchaseCode;
import mm.purchasesdk.h.e;
import mm.purchasesdk.l.a;
import mm.purchasesdk.l.d;
import org.xmlpull.v1.XmlSerializer;

public class c extends e {
    private final String TAG = c.class.getSimpleName();
    private String n = "1";
    private String o = "0";
    int statusCode;

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v12, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v16, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v17, resolved type: byte} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.String a(java.lang.String r6) {
        /*
            r5 = this;
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r1 = mm.purchasesdk.l.d.F()
            r0.<init>(r1)
            java.lang.String r1 = "&"
            java.lang.StringBuilder r1 = r0.append(r1)
            java.lang.String r2 = mm.purchasesdk.l.d.N()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = "&"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = mm.purchasesdk.l.d.G()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = "&"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r6)
            java.lang.String r2 = "&"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = mm.purchasesdk.l.d.t()
            r1.append(r2)
            java.lang.String r1 = r5.TAG
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "plain text: "
            r2.<init>(r3)
            java.lang.String r3 = r0.toString()
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            mm.purchasesdk.l.e.c(r1, r2)
            java.lang.String r0 = r0.toString()
            byte[] r2 = mm.purchasesdk.fingerprint.IdentifyApp.a(r0)
            if (r2 != 0) goto L_0x0067
            java.lang.String r0 = r5.TAG
            java.lang.String r1 = "create signature failed."
            mm.purchasesdk.l.e.e(r0, r1)
            r0 = 0
        L_0x0066:
            return r0
        L_0x0067:
            java.lang.StringBuffer r3 = new java.lang.StringBuffer
            java.lang.String r0 = ""
            r3.<init>(r0)
            r0 = 0
        L_0x006f:
            int r1 = r2.length
            if (r0 >= r1) goto L_0x008b
            byte r1 = r2[r0]
            if (r1 >= 0) goto L_0x0078
            int r1 = r1 + 256
        L_0x0078:
            r4 = 16
            if (r1 >= r4) goto L_0x0081
            java.lang.String r4 = "0"
            r3.append(r4)
        L_0x0081:
            java.lang.String r1 = java.lang.Integer.toHexString(r1)
            r3.append(r1)
            int r0 = r0 + 1
            goto L_0x006f
        L_0x008b:
            java.lang.String r0 = r3.toString()
            java.lang.String r0 = r0.toUpperCase()
            byte[] r0 = r0.getBytes()
            java.lang.String r0 = mm.purchasesdk.fingerprint.IdentifyApp.base64encode(r0)
            goto L_0x0066
        */
        throw new UnsupportedOperationException("Method not decompiled: mm.purchasesdk.a.c.a(java.lang.String):java.lang.String");
    }

    public String a() {
        XmlSerializer newSerializer = Xml.newSerializer();
        StringWriter stringWriter = new StringWriter();
        try {
            newSerializer.setOutput(stringWriter);
            newSerializer.startDocument("UTF-8", true);
            newSerializer.startTag(PoiTypeDef.All, "Trusted3AuthReq");
            newSerializer.startTag(PoiTypeDef.All, "MsgType");
            newSerializer.text("Trusted3AuthReq");
            newSerializer.endTag(PoiTypeDef.All, "MsgType");
            newSerializer.startTag(PoiTypeDef.All, "Version");
            newSerializer.text(d.Z());
            newSerializer.endTag(PoiTypeDef.All, "Version");
            newSerializer.startTag(PoiTypeDef.All, "BracketID");
            newSerializer.text(PoiTypeDef.All);
            newSerializer.endTag(PoiTypeDef.All, "BracketID");
            newSerializer.startTag(PoiTypeDef.All, "SessionID");
            newSerializer.text("0");
            newSerializer.endTag(PoiTypeDef.All, "SessionID");
            newSerializer.startTag(PoiTypeDef.All, "osid");
            newSerializer.text("1");
            newSerializer.endTag(PoiTypeDef.All, "osid");
            newSerializer.startTag(PoiTypeDef.All, "OSInfo");
            newSerializer.text(d.ac());
            newSerializer.endTag(PoiTypeDef.All, "OSInfo");
            newSerializer.startTag(PoiTypeDef.All, "PayCode");
            newSerializer.text(d.H());
            newSerializer.endTag(PoiTypeDef.All, "PayCode");
            newSerializer.startTag(PoiTypeDef.All, "AppID");
            newSerializer.text(d.F());
            newSerializer.endTag(PoiTypeDef.All, "AppID");
            newSerializer.startTag(PoiTypeDef.All, "BracketID");
            newSerializer.text(PoiTypeDef.All);
            newSerializer.endTag(PoiTypeDef.All, "BracketID");
            newSerializer.startTag(PoiTypeDef.All, "ProgramId");
            newSerializer.text(d.N());
            newSerializer.endTag(PoiTypeDef.All, "ProgramId");
            newSerializer.startTag(PoiTypeDef.All, "IsNextCycle");
            newSerializer.text(this.o);
            newSerializer.endTag(PoiTypeDef.All, "IsNextCycle");
            newSerializer.startTag(PoiTypeDef.All, "Timestamp");
            String l = Long.valueOf(Calendar.getInstance().getTimeInMillis() / 1000).toString();
            newSerializer.text(l);
            newSerializer.endTag(PoiTypeDef.All, "Timestamp");
            newSerializer.startTag(PoiTypeDef.All, "Signature");
            newSerializer.text(a(l));
            newSerializer.endTag(PoiTypeDef.All, "Signature");
            newSerializer.startTag(PoiTypeDef.All, "SubsNumb");
            newSerializer.text(this.n);
            newSerializer.endTag(PoiTypeDef.All, "SubsNumb");
            newSerializer.startTag(PoiTypeDef.All, "MccMnc");
            newSerializer.text(d.aa());
            newSerializer.endTag(PoiTypeDef.All, "MccMnc");
            newSerializer.startTag(PoiTypeDef.All, "UAInfo");
            newSerializer.text(d.ab());
            newSerializer.endTag(PoiTypeDef.All, "UAInfo");
            newSerializer.startTag(PoiTypeDef.All, OnPurchaseListener.TRADEID);
            newSerializer.text(d.X());
            newSerializer.endTag(PoiTypeDef.All, OnPurchaseListener.TRADEID);
            newSerializer.startTag(PoiTypeDef.All, "StaticMark");
            String d = mm.purchasesdk.fingerprint.c.d(l);
            if (mm.purchasesdk.fingerprint.c.getStatus() != 0) {
                this.statusCode = PurchaseCode.AUTH_STATICMARK_FIALED;
                mm.purchasesdk.l.e.e(this.TAG, "create static mark failed.code=" + this.statusCode);
                return null;
            }
            mm.purchasesdk.l.e.c(this.TAG, "Static Mark->" + d);
            newSerializer.text(d);
            newSerializer.endTag(PoiTypeDef.All, "StaticMark");
            newSerializer.startTag(PoiTypeDef.All, "UserType");
            newSerializer.text(d.I());
            newSerializer.endTag(PoiTypeDef.All, "UserType");
            newSerializer.startTag(PoiTypeDef.All, "NetInfo");
            newSerializer.text(d.Q());
            newSerializer.endTag(PoiTypeDef.All, "NetInfo");
            newSerializer.startTag(PoiTypeDef.All, "programHash");
            newSerializer.text(PoiTypeDef.All);
            newSerializer.endTag(PoiTypeDef.All, "programHash");
            newSerializer.startTag(PoiTypeDef.All, "imsi");
            newSerializer.text(d.L());
            newSerializer.endTag(PoiTypeDef.All, "imsi");
            newSerializer.startTag(PoiTypeDef.All, "imei");
            newSerializer.text(d.M());
            newSerializer.endTag(PoiTypeDef.All, "imei");
            newSerializer.startTag(PoiTypeDef.All, "ChannelID");
            newSerializer.text(d.t());
            newSerializer.endTag(PoiTypeDef.All, "ChannelID");
            newSerializer.startTag(PoiTypeDef.All, "sidSignature");
            newSerializer.text(a().getUserSignature());
            newSerializer.endTag(PoiTypeDef.All, "sidSignature");
            newSerializer.startTag(PoiTypeDef.All, "ApData");
            String f = d.f(a.z() + d.X() + d.G());
            newSerializer.text(f);
            Log.e(this.TAG, a.z() + d.X() + d.G());
            Log.e(this.TAG, f);
            newSerializer.endTag(PoiTypeDef.All, "ApData");
            newSerializer.startTag(PoiTypeDef.All, "EXDATA");
            if (d.C() != null) {
                newSerializer.text(d.C());
            }
            newSerializer.endTag(PoiTypeDef.All, "EXDATA");
            d.J(PoiTypeDef.All);
            newSerializer.endTag(PoiTypeDef.All, "Trusted3AuthReq");
            newSerializer.endDocument();
            return stringWriter.toString();
        } catch (Exception e) {
            mm.purchasesdk.l.e.a(this.TAG, "create AuthRequest xml file failed!!", e);
            this.statusCode = PurchaseCode.XML_EXCPTION_ERROR;
            return null;
        }
    }

    public void a(Boolean bool) {
        if (bool.booleanValue()) {
            this.o = "1";
        } else {
            this.o = "0";
        }
    }

    /* renamed from: a  reason: collision with other method in class */
    public void m266a(String str) {
        this.n = str;
    }
}
