package mm.purchasesdk.a;

import android.graphics.BitmapFactory;
import com.amap.mapapi.poisearch.PoiTypeDef;
import java.io.ByteArrayInputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import mm.purchasesdk.PurchaseCode;
import mm.purchasesdk.b.a;
import mm.purchasesdk.g.c;
import mm.purchasesdk.g.e;
import mm.purchasesdk.h.f;
import mm.purchasesdk.l.d;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

public class b {
    private static final String TAG = b.class.getSimpleName();

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: mm.purchasesdk.g.c.a(mm.purchasesdk.h.e, mm.purchasesdk.h.f):boolean
     arg types: [mm.purchasesdk.a.c, mm.purchasesdk.a.d]
     candidates:
      mm.purchasesdk.g.c.a(mm.purchasesdk.h.e, mm.purchasesdk.h.f):java.lang.String
      mm.purchasesdk.g.c.a(mm.purchasesdk.h.e, mm.purchasesdk.h.f):boolean */
    public static Boolean a(a aVar) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new mm.purchasesdk.g.b());
        arrayList.add(new e());
        c cVar = new c();
        d dVar = new d();
        cVar.m266a(new StringBuilder().append(d.a()).toString());
        cVar.a(d.i());
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            try {
                if (((c) it.next()).m294a((mm.purchasesdk.h.e) cVar, (f) dVar)) {
                    d.U(dVar.k());
                    d.T(dVar.e());
                    d.b(dVar.m268b());
                    return true;
                } else if (dVar.w() == null) {
                    continue;
                } else if (!dVar.b().booleanValue()) {
                    return null;
                } else {
                    a(dVar);
                    Boolean a2 = dVar.a();
                    try {
                        byte[] base64decode = mm.purchasesdk.fingerprint.c.base64decode(new String(dVar.i().getBytes()));
                        aVar.a(BitmapFactory.decodeByteArray(base64decode, 0, base64decode.length));
                        aVar.l(dVar.h());
                        aVar.j(dVar.g());
                        aVar.b(dVar.j().equals("1"));
                        aVar.b(dVar.c());
                        aVar.s(d.H());
                        aVar.t(mm.purchasesdk.e.b.a().f3140a.ab);
                        aVar.c(a2);
                        aVar.a(d.d());
                        mm.purchasesdk.b.e a3 = dVar.m267a();
                        if (a3 == null) {
                            PurchaseCode.setStatusCode(PurchaseCode.AUTH_PRODUCT_ERROR);
                            return null;
                        }
                        aVar.a(a3);
                        aVar.e(dVar.m269c());
                        aVar.f(dVar.d());
                        return false;
                    } catch (Exception e) {
                        mm.purchasesdk.l.e.e(TAG, "base 64 decrypt license file failure" + e.toString());
                        return null;
                    }
                }
            } catch (mm.purchasesdk.e e2) {
                PurchaseCode.setStatusCode(PurchaseCode.NETWORKTIMEOUT_ERR);
                mm.purchasesdk.l.e.e(TAG, "checkAuth failure " + e2.toString());
            }
        }
        return null;
    }

    public static a a(XmlPullParser xmlPullParser) {
        int eventType = xmlPullParser.getEventType();
        a aVar = null;
        while (eventType != 1) {
            switch (eventType) {
                case 2:
                    String name = xmlPullParser.getName();
                    if (!"MM_User_Authorization".equals(name)) {
                        if (!"IMEI".equals(name)) {
                            if (!"IMSI".equals(name)) {
                                if (!"mobile_phone".equals(name)) {
                                    if (!"APPUID".equals(name)) {
                                        if (!"cotnentID".equals(name)) {
                                            if (!"PayCode".equals(name)) {
                                                if (!"order_time".equals(name)) {
                                                    if (!"notBeforetime".equals(name)) {
                                                        if (!"notAftertime".equals(name)) {
                                                            if (!"digestAlg".equals(name)) {
                                                                if (!"digest".equals(name)) {
                                                                    if (!"timestamp".equals(name)) {
                                                                        if (!"SignatureValue".equals(name)) {
                                                                            break;
                                                                        } else {
                                                                            aVar.l = xmlPullParser.nextText();
                                                                            break;
                                                                        }
                                                                    } else {
                                                                        aVar.k = xmlPullParser.nextText();
                                                                        break;
                                                                    }
                                                                } else {
                                                                    aVar.j = xmlPullParser.nextText();
                                                                    break;
                                                                }
                                                            } else {
                                                                aVar.i = xmlPullParser.nextText();
                                                                break;
                                                            }
                                                        } else {
                                                            aVar.b = new SimpleDateFormat("yyyyMMddHHmmss").parse(xmlPullParser.nextText()).getTime();
                                                            mm.purchasesdk.l.e.c("AUTH_XML", "after: " + aVar.b);
                                                            break;
                                                        }
                                                    } else {
                                                        aVar.f3124a = new SimpleDateFormat("yyyyMMddHHmmss").parse(xmlPullParser.nextText()).getTime();
                                                        mm.purchasesdk.l.e.c("AUTH_XML", "before: " + aVar.f3124a);
                                                        break;
                                                    }
                                                } else {
                                                    aVar.h = xmlPullParser.nextText();
                                                    break;
                                                }
                                            } else {
                                                aVar.m = xmlPullParser.nextText();
                                                break;
                                            }
                                        } else {
                                            aVar.g = xmlPullParser.nextText();
                                            break;
                                        }
                                    } else {
                                        aVar.f = xmlPullParser.nextText();
                                        break;
                                    }
                                } else {
                                    aVar.e = xmlPullParser.nextText();
                                    break;
                                }
                            } else {
                                aVar.d = xmlPullParser.nextText();
                                break;
                            }
                        } else {
                            aVar.c = xmlPullParser.nextText();
                            break;
                        }
                    } else {
                        aVar = new a();
                        break;
                    }
            }
            eventType = xmlPullParser.next();
        }
        if (aVar == null || aVar.f != null) {
            return aVar;
        }
        return null;
    }

    public static a a(byte[] bArr, String str) {
        XmlPullParser newPullParser = XmlPullParserFactory.newInstance().newPullParser();
        newPullParser.setInput(new ByteArrayInputStream(bArr), str);
        return a(newPullParser);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: mm.purchasesdk.fingerprint.c.a(java.lang.String, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      mm.purchasesdk.fingerprint.c.a(java.lang.String, java.lang.String):java.lang.String
      mm.purchasesdk.fingerprint.c.a(java.lang.String, java.lang.String):void */
    private static void a(d dVar) {
        String f = dVar.f();
        mm.purchasesdk.l.e.c(TAG, "dyQuestion-->" + f);
        if (f == null || f.equals(PoiTypeDef.All)) {
            mm.purchasesdk.fingerprint.c.e = false;
            return;
        }
        mm.purchasesdk.fingerprint.c.e = true;
        mm.purchasesdk.fingerprint.c.m289a(f, dVar.g());
    }
}
