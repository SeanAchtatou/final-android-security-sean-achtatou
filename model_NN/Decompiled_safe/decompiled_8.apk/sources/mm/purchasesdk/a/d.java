package mm.purchasesdk.a;

import com.amap.mapapi.poisearch.PoiTypeDef;
import java.io.ByteArrayInputStream;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import mm.purchasesdk.OnPurchaseListener;
import mm.purchasesdk.b.e;
import mm.purchasesdk.h.f;
import mm.purchasesdk.ui.ab;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

public class d extends f {
    private String A = PoiTypeDef.All;
    private String B = PoiTypeDef.All;
    private String C = PoiTypeDef.All;
    private String D = PoiTypeDef.All;
    private String E = PoiTypeDef.All;

    /* renamed from: a  reason: collision with root package name */
    private e f3125a = null;
    private String p;
    private String q;
    private String r;
    private String s;
    private String t;
    private String u;
    private String v;
    private String w;
    private String x;
    private String y = "0";
    private String z = PoiTypeDef.All;

    private e a(String str) {
        e eVar = new e();
        Element documentElement = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new ByteArrayInputStream(str.getBytes())).getDocumentElement();
        NodeList elementsByTagName = documentElement.getElementsByTagName("type");
        for (int i = 0; i < elementsByTagName.getLength(); i++) {
            eVar.d(new Integer(((Element) elementsByTagName.item(i)).getAttribute("id")).intValue());
        }
        NodeList elementsByTagName2 = documentElement.getElementsByTagName("mobile");
        for (int i2 = 0; i2 < elementsByTagName2.getLength(); i2++) {
            eVar.A(((Element) elementsByTagName2.item(i2)).getAttribute("id"));
        }
        NodeList elementsByTagName3 = documentElement.getElementsByTagName("Item");
        for (int i3 = 0; i3 < elementsByTagName3.getLength(); i3++) {
            Element element = (Element) elementsByTagName3.item(i3);
            String attribute = element.getAttribute("id");
            String attribute2 = element.getAttribute("name");
            String attribute3 = element.getAttribute("value");
            ab abVar = new ab();
            abVar.av = attribute;
            abVar.au = attribute2 + ":";
            abVar.mValue = attribute3;
            if (abVar.av.equals("totalprice") || abVar.av.equals("itemcount") || abVar.av.equals("itemprice") || abVar.av.equals("renttime")) {
                abVar.X = -39424;
            } else {
                abVar.X = -16777216;
            }
            eVar.B(attribute2);
            eVar.a(attribute2, abVar);
        }
        return eVar;
    }

    private void r(String str) {
        mm.purchasesdk.l.d.O(str);
    }

    public Boolean a() {
        return !w().equals("6");
    }

    /* renamed from: a  reason: collision with other method in class */
    public e m267a() {
        return this.f3125a;
    }

    public Boolean b() {
        return w().equals("2") || w().equals("6");
    }

    /* renamed from: b  reason: collision with other method in class */
    public String m268b() {
        return this.E;
    }

    public void b(String str) {
        this.E = str;
    }

    public Boolean c() {
        return !l().equals("0");
    }

    /* renamed from: c  reason: collision with other method in class */
    public String m269c() {
        return this.C;
    }

    public void c(String str) {
        this.A = str;
    }

    public String d() {
        return this.D;
    }

    public void d(String str) {
        this.z = str;
    }

    public String e() {
        return this.B;
    }

    public void e(String str) {
        this.C = str;
    }

    public String f() {
        mm.purchasesdk.l.e.c("AuthResponse", "Dynamic Question->" + this.w);
        return this.w;
    }

    public void f(String str) {
        this.D = str;
    }

    public String g() {
        return this.p;
    }

    public void g(String str) {
        if (str != null) {
            this.B = str;
        }
    }

    public String h() {
        return this.r;
    }

    public void h(String str) {
        this.w = str;
    }

    public String i() {
        return this.s;
    }

    public void i(String str) {
        this.v = str;
    }

    public String j() {
        return this.t;
    }

    public void j(String str) {
        this.p = str;
    }

    public String k() {
        return this.x;
    }

    public void k(String str) {
        this.q = str;
    }

    public String l() {
        return this.y;
    }

    public void l(String str) {
        this.r = str;
    }

    public void m(String str) {
        this.s = str;
    }

    public void n(String str) {
        this.t = str;
    }

    public void o(String str) {
        this.u = str;
    }

    public void p(String str) {
        this.x = str;
    }

    public boolean parse(String str) {
        XmlPullParser newPullParser = XmlPullParserFactory.newInstance().newPullParser();
        newPullParser.setInput(new ByteArrayInputStream(str.getBytes()), "utf-8");
        for (int eventType = newPullParser.getEventType(); eventType != 1; eventType = newPullParser.next()) {
            switch (eventType) {
                case 2:
                    String name = newPullParser.getName();
                    if (!"ReturnCode".equals(name)) {
                        if (!"SessionID".equals(name)) {
                            if (!"FeeRemind".equals(name)) {
                                if (!"CheckID".equals(name)) {
                                    if (!"CheckCode".equals(name)) {
                                        if (!"PayPassStatus".equals(name)) {
                                            if (!"SessionID".equals(name)) {
                                                if (!"MultiSubs".equals(name)) {
                                                    if (!"OrderID".equals(name)) {
                                                        if (!"randNum".equals(name)) {
                                                            if (!"DynamicQuest".equals(name)) {
                                                                if (!"SmsCodeSwitch".equals(name)) {
                                                                    if (!"OnderValidDate".equals(name)) {
                                                                        if (!OnPurchaseListener.TRADEID.equals(name)) {
                                                                            if (!"PaycodeInfo".equals(name)) {
                                                                                if (!"PromptMsg".equals(name)) {
                                                                                    if (!"PromptUrl".equals(name)) {
                                                                                        if (!"RespMd5".equals(name)) {
                                                                                            if (!"AllPayWay".equals(name)) {
                                                                                                if (!"ErrorMsg".equals(name)) {
                                                                                                    break;
                                                                                                } else {
                                                                                                    String nextText = newPullParser.nextText();
                                                                                                    setErrMsg(nextText);
                                                                                                    mm.purchasesdk.l.d.setErrMsg(nextText);
                                                                                                    break;
                                                                                                }
                                                                                            } else {
                                                                                                r(newPullParser.nextText());
                                                                                                break;
                                                                                            }
                                                                                        } else {
                                                                                            d(newPullParser.nextText());
                                                                                            break;
                                                                                        }
                                                                                    } else {
                                                                                        f(newPullParser.nextText());
                                                                                        break;
                                                                                    }
                                                                                } else {
                                                                                    e(newPullParser.nextText());
                                                                                    break;
                                                                                }
                                                                            } else {
                                                                                String substring = str.substring(str.indexOf("<PaycodeInfo>"), str.indexOf("</PaycodeInfo>") + 14);
                                                                                c(substring);
                                                                                try {
                                                                                    this.f3125a = a(substring);
                                                                                    break;
                                                                                } catch (ParserConfigurationException e) {
                                                                                    e.printStackTrace();
                                                                                    break;
                                                                                } catch (SAXException e2) {
                                                                                    e2.printStackTrace();
                                                                                    break;
                                                                                }
                                                                            }
                                                                        } else {
                                                                            g(newPullParser.nextText());
                                                                            break;
                                                                        }
                                                                    } else {
                                                                        b(newPullParser.nextText());
                                                                        break;
                                                                    }
                                                                } else {
                                                                    q(newPullParser.nextText());
                                                                    mm.purchasesdk.l.d.W(l());
                                                                    break;
                                                                }
                                                            } else {
                                                                h(newPullParser.nextText());
                                                                break;
                                                            }
                                                        } else {
                                                            i(newPullParser.nextText());
                                                            break;
                                                        }
                                                    } else {
                                                        p(newPullParser.nextText());
                                                        break;
                                                    }
                                                } else {
                                                    o(newPullParser.nextText());
                                                    break;
                                                }
                                            } else {
                                                j(newPullParser.nextText());
                                                break;
                                            }
                                        } else {
                                            n(newPullParser.nextText());
                                            break;
                                        }
                                    } else {
                                        m(newPullParser.nextText());
                                        break;
                                    }
                                } else {
                                    l(newPullParser.nextText());
                                    break;
                                }
                            } else {
                                k(newPullParser.nextText());
                                break;
                            }
                        } else {
                            j(newPullParser.nextText());
                            break;
                        }
                    } else {
                        C(newPullParser.nextText());
                        break;
                    }
            }
        }
        return true;
    }

    public void q(String str) {
        this.y = str;
    }
}
