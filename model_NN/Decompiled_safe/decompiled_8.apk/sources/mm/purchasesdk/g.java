package mm.purchasesdk;

import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import mm.purchasesdk.a.b;
import mm.purchasesdk.b.a;
import mm.purchasesdk.c.l;
import mm.purchasesdk.l.d;
import mm.purchasesdk.l.e;

public class g extends HandlerThread {

    /* renamed from: a  reason: collision with root package name */
    private static a f3144a;
    /* access modifiers changed from: private */
    public static boolean c;

    /* renamed from: a  reason: collision with other field name */
    Handler f258a;
    Handler b;

    public g(String str) {
        super(str);
    }

    /* access modifiers changed from: private */
    public int a(b bVar) {
        if (f3144a == null) {
            f3144a = new a();
        }
        mm.purchasesdk.f.a.k();
        Boolean a2 = b.a(f3144a);
        if (a2 == null) {
            e.e("TaskThread", "AuthManager checkAuth ret = null.code=" + PurchaseCode.getStatusCode());
            Message obtainMessage = this.b.obtainMessage();
            obtainMessage.what = 2;
            obtainMessage.arg1 = PurchaseCode.getStatusCode();
            obtainMessage.obj = bVar;
            obtainMessage.sendToTarget();
            return PurchaseCode.AUTH_OK;
        } else if (a2.booleanValue()) {
            e.c("TaskThread", "AuthManager checkAuth ret = " + PurchaseCode.getStatusCode());
            Message obtainMessage2 = this.b.obtainMessage();
            obtainMessage2.what = 2;
            obtainMessage2.arg1 = PurchaseCode.getStatusCode();
            obtainMessage2.obj = bVar;
            obtainMessage2.sendToTarget();
            return PurchaseCode.AUTH_OK;
        } else {
            PurchaseCode.setStatusCode(PurchaseCode.AUTH_NOORDER);
            Message obtainMessage3 = this.b.obtainMessage();
            obtainMessage3.what = 2;
            obtainMessage3.arg1 = PurchaseCode.getStatusCode();
            obtainMessage3.obj = bVar;
            obtainMessage3.sendToTarget();
            return PurchaseCode.AUTH_OK;
        }
    }

    public static a a() {
        return f3144a;
    }

    /* access modifiers changed from: private */
    /* renamed from: a  reason: collision with other method in class */
    public void m290a(b bVar) {
        e.c("TaskThread", "init() called");
        mm.purchasesdk.f.a.k();
        int c2 = f.c(d.getContext());
        if (c2 != 0) {
            b(bVar, c2);
            return;
        }
        int e = mm.purchasesdk.e.b.e();
        if (e != 0) {
            b(bVar, e);
        } else if (d.g().booleanValue()) {
            int b2 = l.b(bVar);
            if (b2 == 219) {
                b(bVar, b2);
                return;
            }
            mm.purchasesdk.f.a.l();
            mm.purchasesdk.f.a.m();
            d.S(mm.purchasesdk.e.b.a().f3140a.Y);
            b(bVar, b2);
        } else {
            int d = l.d();
            mm.purchasesdk.f.a.l();
            mm.purchasesdk.f.a.m();
            d.S(mm.purchasesdk.e.b.a().f3140a.Y);
            b(bVar, d);
        }
    }

    /* access modifiers changed from: private */
    public void a(b bVar, Bundle bundle) {
        int a2 = mm.purchasesdk.b.b.a(f3144a, bundle);
        PurchaseCode.setStatusCode(a2);
        if (a2 == 104) {
            PurchaseCode.setStatusCode(PurchaseCode.ORDER_OK);
            String string = bundle.getString(OnPurchaseListener.ORDERID);
            String string2 = bundle.getString(OnPurchaseListener.LEFTDAY);
            String string3 = bundle.getString(OnPurchaseListener.ORDERTYPE);
            d.U(string);
            d.b(string2);
            d.y(string3);
            Message obtainMessage = this.b.obtainMessage();
            obtainMessage.what = 2;
            obtainMessage.arg1 = PurchaseCode.ORDER_OK;
            obtainMessage.obj = bVar;
            obtainMessage.setData(bundle);
            obtainMessage.sendToTarget();
            return;
        }
        e.e("TaskThread", " order fail =" + a2);
        Message obtainMessage2 = this.b.obtainMessage();
        obtainMessage2.what = 2;
        obtainMessage2.arg1 = PurchaseCode.getStatusCode();
        obtainMessage2.obj = bVar;
        obtainMessage2.sendToTarget();
    }

    /* renamed from: a  reason: collision with other method in class */
    public static boolean m291a() {
        return c;
    }

    /* access modifiers changed from: private */
    public void b(b bVar) {
        int g = mm.purchasesdk.i.a.g();
        e.c("TaskThread", "query code=" + g);
        if (g != 0) {
            c(bVar, PurchaseCode.getStatusCode());
            return;
        }
        PurchaseCode.setStatusCode(PurchaseCode.QUERY_OK);
        c(bVar, (int) PurchaseCode.QUERY_OK);
    }

    private void b(b bVar, int i) {
        Message obtainMessage = this.b.obtainMessage();
        obtainMessage.what = 0;
        obtainMessage.arg1 = i;
        obtainMessage.obj = bVar;
        obtainMessage.sendToTarget();
    }

    private void c(b bVar) {
        a(bVar);
    }

    private void c(b bVar, int i) {
        Message obtainMessage = this.b.obtainMessage();
        obtainMessage.what = d.ab;
        obtainMessage.arg1 = i;
        obtainMessage.obj = bVar;
        obtainMessage.sendToTarget();
    }

    /* access modifiers changed from: private */
    public void d(b bVar) {
        int i = new mm.purchasesdk.k.a().i();
        PurchaseCode.setStatusCode(i);
        Message obtainMessage = this.b.obtainMessage();
        obtainMessage.what = 3;
        obtainMessage.arg1 = i;
        obtainMessage.obj = bVar;
        obtainMessage.sendToTarget();
    }

    /* access modifiers changed from: private */
    public void e(b bVar) {
        if (f3144a == null) {
            f3144a = new a();
        }
        Boolean a2 = b.a(f3144a);
        if (a2 != null && !a2.booleanValue()) {
            PurchaseCode.setStatusCode(PurchaseCode.AUTH_NOORDER);
        }
        Message obtainMessage = this.b.obtainMessage();
        obtainMessage.what = 4;
        obtainMessage.arg1 = PurchaseCode.getStatusCode();
        obtainMessage.sendToTarget();
    }

    /* renamed from: a  reason: collision with other method in class */
    public Handler m292a() {
        if (this.f258a == null) {
            init();
        }
        return this.f258a;
    }

    public void a(Handler handler) {
        this.b = handler;
    }

    public void init() {
        this.f258a = new h(this, getLooper());
    }
}
