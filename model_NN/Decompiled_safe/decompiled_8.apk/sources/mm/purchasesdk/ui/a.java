package mm.purchasesdk.ui;

import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;

public class a {
    public static Drawable a(int i, int i2, int i3) {
        GradientDrawable gradientDrawable = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{i, i});
        gradientDrawable.setStroke(2, -4802890);
        gradientDrawable.setShape(0);
        gradientDrawable.setCornerRadii(new float[]{(float) i2, (float) i2, (float) i2, (float) i2, (float) i3, (float) i3, (float) i3, (float) i3});
        return gradientDrawable;
    }
}
