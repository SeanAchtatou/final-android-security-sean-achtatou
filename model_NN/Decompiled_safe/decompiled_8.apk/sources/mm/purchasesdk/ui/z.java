package mm.purchasesdk.ui;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import mm.purchasesdk.l.d;

class z implements SensorEventListener {
    final /* synthetic */ y b;

    z(y yVar) {
        this.b = yVar;
    }

    public void onAccuracyChanged(Sensor sensor, int i) {
    }

    public void onSensorChanged(SensorEvent sensorEvent) {
        switch (sensorEvent.sensor.getType()) {
            case 1:
                float[] fArr = sensorEvent.values;
                if (!d.d()) {
                    switch (this.b.f311a.getOrientation()) {
                        case 0:
                            this.b.f(true);
                            return;
                        case 1:
                            this.b.f(false);
                            return;
                        case 2:
                            this.b.f(true);
                            return;
                        case 3:
                            this.b.f(false);
                            return;
                        default:
                            return;
                    }
                } else {
                    switch (this.b.f311a.getOrientation()) {
                        case 0:
                            this.b.f(false);
                            return;
                        case 1:
                            this.b.f(true);
                            return;
                        case 2:
                            this.b.f(false);
                            return;
                        case 3:
                            this.b.f(true);
                            return;
                        default:
                            return;
                    }
                }
            default:
                return;
        }
    }
}
