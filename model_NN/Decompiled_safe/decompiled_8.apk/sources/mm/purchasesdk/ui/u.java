package mm.purchasesdk.ui;

import android.app.Activity;
import android.os.Handler;
import java.util.HashMap;
import mm.purchasesdk.b;
import mm.purchasesdk.b.a;
import mm.purchasesdk.l.d;

public class u {

    /* renamed from: a  reason: collision with root package name */
    private static u f3171a = null;

    /* renamed from: a  reason: collision with other field name */
    private ac f300a;

    /* renamed from: a  reason: collision with other field name */
    private j f301a;

    /* renamed from: a  reason: collision with other field name */
    private v f302a;

    /* renamed from: a  reason: collision with other field name */
    private y f303a;
    private l b;

    private u() {
    }

    public static u a() {
        if (f3171a == null) {
            f3171a = new u();
        }
        return f3171a;
    }

    public void a(int i, String str, b bVar, Handler handler, Handler handler2, HashMap hashMap) {
        if (!((Activity) d.getContext()).isFinishing()) {
            if (!(i == 403 || i == 404 || i == 115)) {
                this.f301a.dismiss();
            }
            this.f300a = new ac(d.getContext(), bVar, handler, handler2, str, i, hashMap);
            this.f300a.show();
        }
    }

    public void a(int i, b bVar, Handler handler, Handler handler2, HashMap hashMap) {
        if (!((Activity) d.getContext()).isFinishing()) {
            if (!(i == 403 || i == 404 || i == 115 || this.f301a == null || !this.f301a.isShowing())) {
                this.f301a.dismiss();
            }
            this.f302a = new v(d.getContext(), bVar, handler, handler2, i, hashMap);
            this.f302a.show();
        }
    }

    public void a(Handler handler, Handler handler2, b bVar, a aVar) {
        if (!((Activity) d.getContext()).isFinishing()) {
            if (this.b == null || this.b.getContext() != d.getContext()) {
                this.b = new l(d.getContext(), handler, handler2, bVar, aVar);
            } else {
                this.b.a(aVar);
            }
            this.b.show();
            t();
        }
    }

    public void b(a aVar) {
        this.b.b(aVar);
    }

    public void r() {
        if (!((Activity) d.getContext()).isFinishing()) {
            if (this.f303a == null || this.f303a.getOwnerActivity() != d.getContext()) {
                this.f303a = new y(d.getContext());
            }
            if (!this.f303a.isShowing()) {
                this.f303a.show();
            }
        }
    }

    public void s() {
        if (!((Activity) d.getContext()).isFinishing()) {
            if (this.f301a == null || this.f301a.getOwnerActivity() != d.getContext()) {
                this.f301a = new j(d.getContext());
            }
            if (!this.f301a.isShowing()) {
                this.f301a.show();
            }
        }
    }

    public void t() {
        if (this.f301a != null && this.f301a.isShowing()) {
            this.f301a.dismiss();
        }
    }

    public void u() {
        if (this.b != null && this.b.isShowing()) {
            this.b.dismiss();
        }
    }

    public void v() {
        if (this.f301a != null && this.f301a.isShowing()) {
            this.f301a.dismiss();
        }
        if (this.b != null && this.b.isShowing()) {
            this.b.dismiss();
        }
        if (this.f302a != null && this.f302a.isShowing()) {
            this.f302a.dismiss();
        }
        if (this.f300a != null && this.f300a.isShowing()) {
            this.f300a.dismiss();
        }
        this.f301a = null;
        this.b = null;
        this.f302a = null;
        this.f300a = null;
    }
}
