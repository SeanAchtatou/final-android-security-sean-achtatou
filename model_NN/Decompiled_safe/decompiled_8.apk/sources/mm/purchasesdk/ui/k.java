package mm.purchasesdk.ui;

import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import mm.purchasesdk.l.d;

public class k {
    private View c = m();

    private View m() {
        RelativeLayout relativeLayout = new RelativeLayout(d.getContext());
        relativeLayout.setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
        ProgressBar progressBar = new ProgressBar(d.getContext());
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams.addRule(13);
        progressBar.setLayoutParams(layoutParams);
        relativeLayout.addView(progressBar);
        return relativeLayout;
    }

    public View l() {
        return this.c;
    }
}
