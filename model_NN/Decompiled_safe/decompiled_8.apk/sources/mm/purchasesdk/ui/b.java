package mm.purchasesdk.ui;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.NinePatch;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.NinePatchDrawable;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import mm.purchasesdk.l.d;

public class b extends Dialog {
    protected Bitmap b;
    private Bitmap c;
    protected Bitmap d;
    private Bitmap e;
    private Bitmap f;
    protected Drawable i;
    /* access modifiers changed from: protected */
    public Drawable j;
    /* access modifiers changed from: protected */
    public Drawable k;
    private Drawable l;
    private Context mContext;

    public b(Context context) {
        super(context);
        this.mContext = context;
        d();
    }

    public b(Context context, int i2) {
        super(context, i2);
        this.mContext = context;
        d();
    }

    private void d() {
        Bitmap a2;
        Bitmap a3;
        Bitmap a4;
        if (this.i == null) {
            this.i = new BitmapDrawable(aa.b(d.getContext(), "mmiap/image/vertical/title1_bg.png"));
        }
        if (this.j == null && (a4 = aa.a(this.mContext, "mmiap/image/vertical/button1_Confirm.9.png")) != null) {
            byte[] ninePatchChunk = a4.getNinePatchChunk();
            NinePatch.isNinePatchChunk(ninePatchChunk);
            this.j = new NinePatchDrawable(a4, ninePatchChunk, new Rect(), null);
        }
        if (this.k == null && (a3 = aa.a(this.mContext, "mmiap/image/vertical/button1_Confirm_Press.9.png")) != null) {
            byte[] ninePatchChunk2 = a3.getNinePatchChunk();
            NinePatch.isNinePatchChunk(ninePatchChunk2);
            this.k = new NinePatchDrawable(a3, ninePatchChunk2, new Rect(), null);
        }
        if (this.l == null && (a2 = aa.a(d.getContext(), "mmiap/image/vertical/title2_bg.png")) != null) {
            this.l = new BitmapDrawable(a2);
        }
        if (this.b == null) {
            this.b = aa.a(d.getContext(), "mmiap/image/vertical/top_button_back.png");
        }
        if (this.c == null) {
            this.c = aa.a(d.getContext(), "mmiap/image/vertical/button_back_Press.png");
        }
        if (this.d == null) {
            this.d = aa.a(d.getContext(), "mmiap/image/vertical/button_finishbilling.png");
        }
        if (this.f == null) {
            this.f = aa.b("mmiap/image/vertical/logo2.png");
        }
        if (this.e == null) {
            this.e = aa.a(d.getContext(), "mmiap/image/vertical/top_button_back.png");
        }
    }

    private View i() {
        Context context = d.getContext();
        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setOrientation(0);
        if (this.f != null) {
            ImageView imageView = new ImageView(context);
            imageView.setLayoutParams(new LinearLayout.LayoutParams(-2, -1));
            imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
            imageView.setBackgroundColor(0);
            imageView.setImageBitmap(this.f);
            linearLayout.addView(imageView);
        }
        TextView textView = new TextView(context);
        textView.setLayoutParams(new LinearLayout.LayoutParams(-2, -1));
        textView.setGravity(17);
        if (d.N().equals("100000000000")) {
            textView.setText("手机话费支付(自测试)");
        } else {
            textView.setText("手机话费支付");
        }
        textView.setTextColor(-16777216);
        textView.setSingleLine(true);
        textView.setTextSize(aa.j);
        linearLayout.addView(textView);
        return linearLayout;
    }

    public View a(Context context, ImageView imageView, View.OnClickListener onClickListener) {
        RelativeLayout relativeLayout = new RelativeLayout(context);
        new RelativeLayout.LayoutParams(-1, -2);
        ImageView imageView2 = new ImageView(context);
        imageView2.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        imageView2.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        imageView2.setImageBitmap(aa.b(d.getContext(), "mmiap/image/vertical/logo1.png"));
        imageView2.setBackgroundDrawable(this.i);
        relativeLayout.addView(imageView2);
        ImageView imageView3 = new ImageView(context);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams.addRule(15);
        imageView3.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        imageView3.setBackgroundColor(0);
        imageView3.setImageBitmap(this.e);
        imageView3.setPadding(5, 0, 0, 0);
        imageView3.setOnClickListener(onClickListener);
        relativeLayout.addView(imageView3, layoutParams);
        return relativeLayout;
    }

    public View a(Button button, View.OnClickListener onClickListener, String str) {
        LinearLayout linearLayout = new LinearLayout(d.getContext());
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2);
        layoutParams.leftMargin = 10;
        layoutParams.rightMargin = 10;
        layoutParams.topMargin = aa.T;
        linearLayout.setOrientation(1);
        layoutParams.gravity = 1;
        linearLayout.setLayoutParams(layoutParams);
        button.setGravity(17);
        button.setOnTouchListener(new c(this));
        LinearLayout.LayoutParams layoutParams2 = null;
        if (d.k < 1.0f) {
            layoutParams2 = aa.f265d.booleanValue() ? new LinearLayout.LayoutParams((int) (((double) d.ad) * 0.6d), 40) : new LinearLayout.LayoutParams(-1, 40);
        }
        if (d.k == 1.0f) {
            layoutParams2 = aa.f265d.booleanValue() ? new LinearLayout.LayoutParams((int) (((double) d.ad) * 0.6d), -2) : new LinearLayout.LayoutParams(-1, -2);
        }
        if (d.k > 1.0f) {
            layoutParams2 = aa.f265d.booleanValue() ? new LinearLayout.LayoutParams((int) (((double) d.ad) * 0.6d), 80) : new LinearLayout.LayoutParams(-1, 80);
        }
        layoutParams2.gravity = 1;
        button.setGravity(17);
        button.setPadding(0, 8, 0, 12);
        button.setText(str);
        button.setTextSize(1, (float) aa.N);
        button.setTextColor(-1);
        button.setLayoutParams(layoutParams2);
        button.setBackgroundDrawable(this.j);
        button.setOnClickListener(onClickListener);
        linearLayout.addView(button);
        return linearLayout;
    }

    public View b(Context context) {
        ImageView imageView = new ImageView(context);
        imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        if (d.k < 1.0f) {
            imageView.setLayoutParams(new LinearLayout.LayoutParams(-1, 50));
        } else {
            imageView.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        }
        imageView.setImageBitmap(aa.b(d.getContext(), "mmiap/image/vertical/logo1.png"));
        imageView.setBackgroundDrawable(this.i);
        return imageView;
    }

    public View b(Context context, ImageView imageView, View.OnClickListener onClickListener) {
        RelativeLayout relativeLayout = new RelativeLayout(context);
        RelativeLayout.LayoutParams layoutParams = d.k < 1.0f ? new RelativeLayout.LayoutParams(-1, 30) : new RelativeLayout.LayoutParams(-1, -2);
        relativeLayout.setPadding(0, aa.U, 0, aa.V);
        relativeLayout.setLayoutParams(layoutParams);
        aa.a(context, "mmiap/image/vertical/title2_bg.png");
        relativeLayout.setBackgroundDrawable(this.l);
        ImageView imageView2 = new ImageView(context);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams2.addRule(15);
        imageView2.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        imageView2.setBackgroundColor(0);
        imageView2.setImageBitmap(this.c);
        imageView2.setPadding(5, 0, 0, 0);
        imageView2.setOnClickListener(onClickListener);
        relativeLayout.addView(imageView2, layoutParams2);
        RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams3.addRule(13);
        relativeLayout.addView(i(), layoutParams3);
        TextView textView = new TextView(context);
        textView.setLayoutParams(new LinearLayout.LayoutParams(0, 0, 1.0f));
        textView.setBackgroundColor(0);
        relativeLayout.addView(textView);
        return relativeLayout;
    }

    public View c(Context context) {
        RelativeLayout relativeLayout = new RelativeLayout(context);
        relativeLayout.setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
        relativeLayout.setPadding(0, 0, 0, 10);
        LinearLayout linearLayout = new LinearLayout(context);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2);
        layoutParams.bottomMargin = 5;
        linearLayout.setOrientation(0);
        linearLayout.setLayoutParams(layoutParams);
        linearLayout.setGravity(1);
        ImageView imageView = new ImageView(context);
        Bitmap a2 = aa.a(context, "mmiap/image/vertical/logo3.png");
        if (a2 != null) {
            imageView.setImageBitmap(a2);
        }
        linearLayout.addView(imageView);
        TextView textView = new TextView(context);
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-2, -1);
        layoutParams2.leftMargin = 5;
        layoutParams2.gravity = 17;
        textView.setGravity(17);
        textView.setTextColor(-16777216);
        textView.setLayoutParams(layoutParams2);
        textView.setTextSize(aa.h);
        textView.setText("版权所有 中国移动");
        linearLayout.addView(textView);
        RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams3.addRule(12);
        relativeLayout.addView(linearLayout, layoutParams3);
        return relativeLayout;
    }

    public void dismiss() {
        super.dismiss();
    }
}
