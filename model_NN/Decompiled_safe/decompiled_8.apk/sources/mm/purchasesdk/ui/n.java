package mm.purchasesdk.ui;

import android.view.MotionEvent;
import android.view.View;

class n implements View.OnTouchListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ l f3164a;

    n(l lVar) {
        this.f3164a = lVar;
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (motionEvent.getAction() != 0) {
            return false;
        }
        if (l.a(this.f3164a) == null) {
            d unused = this.f3164a.f290b = new d(l.a(this.f3164a), true);
        }
        try {
            l.a(this.f3164a).b(l.a(this.f3164a));
        } catch (Exception e) {
            e.printStackTrace();
        }
        l.a(this.f3164a).setVisibility(8);
        switch (view.getId()) {
            case 0:
                if (l.a(this.f3164a) != null) {
                    l.a(this.f3164a).clearFocus();
                }
                if (l.b(this.f3164a) == null) {
                    return false;
                }
                l.b(this.f3164a).requestFocus();
                return false;
            case 1:
                if (l.a(this.f3164a) != null) {
                    l.a(this.f3164a).requestFocus();
                }
                if (l.b(this.f3164a) == null) {
                    return false;
                }
                l.b(this.f3164a).clearFocus();
                return false;
            case 2:
                if (this.f3164a.f298e != null) {
                    this.f3164a.f298e.requestFocus();
                }
                if (l.a(this.f3164a) != null) {
                    l.a(this.f3164a).clearFocus();
                }
                if (l.b(this.f3164a) == null) {
                    return false;
                }
                l.b(this.f3164a).clearFocus();
                return false;
            default:
                return false;
        }
    }
}
