package mm.purchasesdk.ui;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.NinePatch;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.NinePatchDrawable;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import java.util.HashMap;
import mm.purchasesdk.PurchaseCode;
import mm.purchasesdk.b;
import mm.purchasesdk.l.d;
import mm.purchasesdk.l.e;

public class v extends b {
    private int E;
    private int F;
    private final String TAG = "ResultDialog";

    /* renamed from: a  reason: collision with root package name */
    private Drawable f3172a;

    /* renamed from: a  reason: collision with other field name */
    private Handler f304a;

    /* renamed from: a  reason: collision with other field name */
    private ScrollView f305a;

    /* renamed from: a  reason: collision with other field name */
    private HashMap f306a;
    private String ar = PoiTypeDef.All;
    private String as = PoiTypeDef.All;
    private String at = PoiTypeDef.All;
    /* access modifiers changed from: private */
    public Handler b;

    /* renamed from: b  reason: collision with other field name */
    private ImageView f307b;

    /* renamed from: b  reason: collision with other field name */
    private b f308b;
    private View.OnClickListener c = new x(this);
    private View.OnClickListener f = new w(this);

    /* renamed from: f  reason: collision with other field name */
    private Button f309f;

    /* renamed from: f  reason: collision with other field name */
    private TextView f310f;
    private Boolean g = true;
    private Bitmap i;
    private Drawable v;

    public v(Context context, b bVar, Handler handler, Handler handler2, int i2, HashMap hashMap) {
        super(context, 16973829);
        setOwnerActivity((Activity) context);
        getWindow().requestFeature(1);
        if (d.getContext() == null || ((Activity) d.getContext()) != getOwnerActivity()) {
            d.setContext(context);
        }
        this.E = i2;
        this.f304a = handler;
        this.b = handler2;
        this.f308b = bVar;
        this.f306a = hashMap;
        this.f309f = new Button(d.getContext());
        init(i2);
        d();
    }

    private View A() {
        LinearLayout linearLayout = new LinearLayout(d.getContext());
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams((int) (((double) d.ad) * 0.95d), aa.M);
        layoutParams.gravity = 1;
        layoutParams.topMargin = aa.R;
        linearLayout.setOrientation(1);
        linearLayout.setLayoutParams(layoutParams);
        linearLayout.setBackgroundDrawable(this.v);
        int i2 = d.k < 1.0f ? (int) (((double) aa.M) * 0.65d) : (int) (((double) aa.M) * 0.75d);
        LinearLayout linearLayout2 = new LinearLayout(d.getContext());
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-1, i2);
        linearLayout2.setOrientation(0);
        linearLayout2.setLayoutParams(layoutParams2);
        linearLayout2.setGravity(17);
        ImageView imageView = new ImageView(d.getContext());
        imageView.setImageBitmap(this.i);
        linearLayout2.addView(imageView);
        TextView textView = new TextView(d.getContext());
        textView.setTextSize(30.0f);
        textView.setTextColor(this.F);
        textView.setGravity(17);
        textView.setText(this.ar);
        LinearLayout.LayoutParams layoutParams3 = new LinearLayout.LayoutParams(-2, -2);
        layoutParams3.leftMargin = 10;
        layoutParams3.gravity = 16;
        textView.setLayoutParams(layoutParams3);
        linearLayout2.addView(textView);
        linearLayout.addView(linearLayout2);
        TextView textView2 = new TextView(d.getContext());
        textView2.setLayoutParams(new LinearLayout.LayoutParams(-1, 2));
        textView2.setBackgroundDrawable(new BitmapDrawable(aa.a(d.getContext(), "mmiap/image/vertical/line.png")));
        linearLayout.addView(textView2);
        this.f310f = new TextView(d.getContext());
        LinearLayout.LayoutParams layoutParams4 = new LinearLayout.LayoutParams(-2, (aa.M - i2) - 2);
        layoutParams4.gravity = 17;
        this.f310f.setGravity(17);
        this.f310f.setLayoutParams(layoutParams4);
        this.f310f.setTextColor(-8289919);
        this.f310f.setText(this.as);
        if (this.as.length() < 25) {
            this.f310f.setTextSize(15.0f);
        } else {
            this.f310f.setTextSize(12.0f);
        }
        linearLayout.addView(this.f310f);
        return linearLayout;
    }

    private View B() {
        LinearLayout linearLayout = new LinearLayout(d.getContext());
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2);
        layoutParams.leftMargin = 10;
        layoutParams.rightMargin = 10;
        layoutParams.topMargin = 10;
        layoutParams.bottomMargin = 10;
        linearLayout.setLayoutParams(layoutParams);
        linearLayout.setBackgroundDrawable(a.a(-1, 10, 10));
        linearLayout.setPadding(6, 6, 6, 6);
        TextView textView = new TextView(d.getContext());
        textView.setTextColor(-8289919);
        String D = d.D();
        if (D == null) {
            D = "欢迎您使用中国移动手机话费支付功能，您可登陆a.10086.cn或拨打10086查询购买记录.";
        }
        textView.setText(D);
        textView.setTextSize(aa.i);
        textView.setLineSpacing(1.0f, 1.3f);
        linearLayout.addView(textView);
        return linearLayout;
    }

    private void d() {
        Bitmap a2;
        if (this.f3172a == null && (a2 = aa.a(d.getContext(), "mmiap/image/vertical/bg.png")) != null) {
            this.f3172a = new BitmapDrawable(a2);
        }
    }

    private void init(int i2) {
        Bitmap a2;
        if (i2 == 102 || i2 == 104) {
            this.ar = "支 付 成 功";
            this.at = "确 定";
            this.i = aa.a(d.getContext(), "mmiap/image/vertical/icon_success.png");
            this.F = -11037110;
            this.as = PurchaseCode.getReason(i2);
            this.g = true;
        } else {
            this.ar = "支 付 失 败";
            this.g = false;
            this.F = -4703429;
            if (this.f306a == null) {
                e.e("ResultDialog", "mReturnObject null order fail =" + i2);
                this.i = aa.a(d.getContext(), "mmiap/image/vertical/icon_info.png");
            } else {
                e.e("ResultDialog", "mReturnObject order fail =" + i2);
                this.i = aa.a(d.getContext(), "mmiap/image/vertical/icon_false.png");
            }
            this.as = PurchaseCode.getReason(i2);
            if (i2 == 403 || i2 == 404 || i2 == 115) {
                this.at = "重 新 购 买";
            } else {
                this.at = "确 定";
            }
        }
        if (this.v == null && (a2 = aa.a(d.getContext(), "mmiap/image/vertical/infobg.9.png")) != null) {
            byte[] ninePatchChunk = a2.getNinePatchChunk();
            NinePatch.isNinePatchChunk(ninePatchChunk);
            this.v = new NinePatchDrawable(a2, ninePatchChunk, new Rect(), null);
        }
    }

    public void dismiss() {
        u.a().u();
        super.dismiss();
    }

    public void show() {
        setContentView(z());
        setCancelable(false);
        super.show();
    }

    public View z() {
        LinearLayout linearLayout = new LinearLayout(d.getContext());
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -1);
        layoutParams.gravity = 1;
        linearLayout.setOrientation(1);
        linearLayout.setLayoutParams(layoutParams);
        linearLayout.addView(b(d.getContext()));
        linearLayout.addView(b(d.getContext(), this.f307b, this.c));
        linearLayout.addView(A());
        linearLayout.addView(a(this.f309f, this.f, this.at));
        if (this.g.booleanValue()) {
            linearLayout.addView(B());
        }
        linearLayout.addView(c(d.getContext()));
        this.f305a = new ScrollView(d.getContext());
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-1, -1);
        this.f305a = new ScrollView(d.getContext());
        this.f305a.setLayoutParams(layoutParams2);
        this.f305a.setFillViewport(true);
        this.f305a.setBackgroundDrawable(this.f3172a);
        this.f305a.addView(linearLayout);
        return this.f305a;
    }
}
