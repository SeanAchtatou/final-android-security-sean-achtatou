package mm.purchasesdk.ui;

import android.view.View;

class m implements View.OnFocusChangeListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ l f3163a;

    m(l lVar) {
        this.f3163a = lVar;
    }

    public void onFocusChange(View view, boolean z) {
        switch (view.getId()) {
            case 0:
                if (l.a(this.f3163a) != null) {
                    l.a(this.f3163a).setBackgroundDrawable(l.a(this.f3163a));
                }
                if (l.b(this.f3163a) != null) {
                    l.b(this.f3163a).setBackgroundDrawable(l.b(this.f3163a));
                }
                if (this.f3163a.f298e != null) {
                    this.f3163a.f298e.setBackgroundDrawable(l.a(this.f3163a));
                    return;
                }
                return;
            case 1:
                if (l.a(this.f3163a) != null) {
                    l.a(this.f3163a).setBackgroundDrawable(l.b(this.f3163a));
                }
                if (l.b(this.f3163a) != null) {
                    l.b(this.f3163a).setBackgroundDrawable(l.a(this.f3163a));
                }
                if (this.f3163a.f298e != null) {
                    this.f3163a.f298e.setBackgroundDrawable(l.a(this.f3163a));
                    return;
                }
                return;
            case 2:
                if (this.f3163a.f298e != null) {
                    this.f3163a.f298e.setBackgroundDrawable(l.b(this.f3163a));
                }
                if (l.b(this.f3163a) != null) {
                    l.b(this.f3163a).setBackgroundDrawable(l.a(this.f3163a));
                }
                if (l.a(this.f3163a) != null) {
                    l.a(this.f3163a).setBackgroundDrawable(l.a(this.f3163a));
                    return;
                }
                return;
            default:
                return;
        }
    }
}
