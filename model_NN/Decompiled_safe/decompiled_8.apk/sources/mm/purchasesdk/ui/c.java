package mm.purchasesdk.ui;

import android.view.MotionEvent;
import android.view.View;

class c implements View.OnTouchListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ b f3154a;

    c(b bVar) {
        this.f3154a = bVar;
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (motionEvent.getAction() == 0) {
            view.setBackgroundDrawable(this.f3154a.k);
            return false;
        }
        view.setBackgroundDrawable(this.f3154a.j);
        return false;
    }
}
