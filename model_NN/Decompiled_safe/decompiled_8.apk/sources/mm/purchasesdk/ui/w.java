package mm.purchasesdk.ui;

import android.app.Activity;
import android.os.Message;
import android.view.View;
import mm.purchasesdk.l.d;
import mm.purchasesdk.l.e;

class w implements View.OnClickListener {
    final /* synthetic */ v b;

    w(v vVar) {
        this.b = vVar;
    }

    public void onClick(View view) {
        if (v.a(this.b).getText().equals("重 新 购 买")) {
            if (((Activity) d.getContext()).isFinishing()) {
                e.e("ResultDialog", "Activity is finished!");
                return;
            }
            this.b.dismiss();
            Message obtainMessage = v.a(this.b).obtainMessage();
            obtainMessage.what = 0;
            obtainMessage.obj = v.a(this.b);
            obtainMessage.arg1 = 0;
            obtainMessage.sendToTarget();
        } else if (((Activity) d.getContext()).isFinishing()) {
            e.e("ResultDialog", "Activity is finished!");
        } else {
            this.b.dismiss();
            v.a(this.b).a(v.a(this.b));
            v.a(this.b).a(v.a(this.b));
            Message obtainMessage2 = this.b.b.obtainMessage();
            obtainMessage2.what = 5;
            obtainMessage2.obj = v.a(this.b);
            obtainMessage2.sendToTarget();
        }
    }
}
