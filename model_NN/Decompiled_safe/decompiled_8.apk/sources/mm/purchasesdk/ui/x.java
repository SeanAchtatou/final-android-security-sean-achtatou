package mm.purchasesdk.ui;

import android.app.Activity;
import android.os.Message;
import android.view.View;
import mm.purchasesdk.l.d;
import mm.purchasesdk.l.e;

class x implements View.OnClickListener {
    final /* synthetic */ v b;

    x(v vVar) {
        this.b = vVar;
    }

    public void onClick(View view) {
        if (((Activity) d.getContext()).isFinishing()) {
            e.e("ResultDialog", "Activity is finished!");
            return;
        }
        this.b.dismiss();
        u.a().t();
        v.a(this.b).a(v.a(this.b));
        v.a(this.b).a(v.a(this.b));
        Message obtainMessage = this.b.b.obtainMessage();
        obtainMessage.what = 5;
        obtainMessage.obj = v.a(this.b);
        obtainMessage.sendToTarget();
    }
}
