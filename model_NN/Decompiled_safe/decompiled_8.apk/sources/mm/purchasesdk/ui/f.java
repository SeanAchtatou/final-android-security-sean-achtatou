package mm.purchasesdk.ui;

import android.view.View;
import android.widget.Button;

class f implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ d f3157a;

    f(d dVar) {
        this.f3157a = dVar;
    }

    public void onClick(View view) {
        Integer num = (Integer) view.getTag();
        if (num.intValue() == 0 || num.intValue() == 1) {
            new Thread(new g(this, this.f3157a.a(num.intValue() == 0 ? String.valueOf(((Button) view).getText()) : "delete"))).start();
        } else if (num.intValue() == 2) {
            d.a(this.f3157a).dismiss();
            if (d.a(this.f3157a) != null) {
                d.a(this.f3157a).setVisibility(0);
            }
            d.b(this.f3157a).scrollTo(0, 0);
        }
    }
}
