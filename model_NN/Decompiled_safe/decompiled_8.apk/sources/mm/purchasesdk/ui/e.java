package mm.purchasesdk.ui;

import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;

class e implements View.OnTouchListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ d f3156a;

    e(d dVar) {
        this.f3156a = dVar;
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        Integer num = (Integer) view.getTag();
        if (motionEvent.getAction() == 0) {
            if (num.intValue() == 0) {
                view.setBackgroundDrawable(d.a(this.f3156a));
                ((Button) view).setTextColor(-1);
            } else if (num.intValue() == 1) {
                view.setBackgroundDrawable(d.b(this.f3156a));
            } else {
                view.setBackgroundDrawable(this.f3156a.s);
            }
        } else if (motionEvent.getAction() == 1) {
            if (num.intValue() == 0) {
                view.setBackgroundDrawable(this.f3156a.n);
                ((Button) view).setTextColor(-16777216);
            } else if (num.intValue() == 1) {
                view.setBackgroundDrawable(this.f3156a.o);
            } else {
                view.setBackgroundDrawable(this.f3156a.p);
            }
        }
        return false;
    }
}
