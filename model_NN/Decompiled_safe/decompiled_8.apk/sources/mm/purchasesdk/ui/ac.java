package mm.purchasesdk.ui;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashMap;
import mm.purchasesdk.PurchaseCode;
import mm.purchasesdk.b;
import mm.purchasesdk.l.d;
import mm.purchasesdk.l.e;

public class ac extends b {
    private int E;
    private final String TAG = "WebViewLayout";
    private final int Y = 1;
    private final int Z = 2;

    /* renamed from: a  reason: collision with root package name */
    private Drawable f3152a;

    /* renamed from: a  reason: collision with other field name */
    private Handler f267a;

    /* renamed from: a  reason: collision with other field name */
    private WebView f268a;

    /* renamed from: a  reason: collision with other field name */
    private RelativeLayout f269a;

    /* renamed from: a  reason: collision with other field name */
    private HashMap f270a;
    private String aw;
    private Handler b;

    /* renamed from: b  reason: collision with other field name */
    private ImageView f271b;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with other field name */
    public ProgressBar f272b;

    /* renamed from: b  reason: collision with other field name */
    private b f273b;
    private ImageView c;
    private View.OnClickListener g = new ad(this);

    public ac(Context context, b bVar, Handler handler, Handler handler2, String str, int i, HashMap hashMap) {
        super(context, 16973829);
        Bitmap a2;
        setOwnerActivity((Activity) context);
        getWindow().requestFeature(1);
        if (d.getContext() == null || ((Activity) d.getContext()) != getOwnerActivity()) {
            d.setContext(context);
        }
        this.f267a = handler;
        this.b = handler2;
        this.f273b = bVar;
        try {
            str = URLDecoder.decode(str, "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        this.aw = str;
        e.c("WebViewLayout", "murl =" + this.aw);
        this.E = i;
        this.f270a = hashMap;
        if (this.f3152a == null && (a2 = aa.a(d.getContext(), "mmiap/image/vertical/bg.png")) != null) {
            this.f3152a = new BitmapDrawable(a2);
        }
    }

    public View a(Context context, ImageView imageView, ImageView imageView2, View.OnClickListener onClickListener) {
        RelativeLayout relativeLayout = new RelativeLayout(context);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -2);
        relativeLayout.setPadding(0, aa.U, 0, aa.V);
        relativeLayout.setLayoutParams(layoutParams);
        relativeLayout.setBackgroundDrawable(this.i);
        ImageView imageView3 = new ImageView(context);
        imageView3.setTag(0);
        imageView3.setId(1);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams2.addRule(15);
        imageView3.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        imageView3.setBackgroundColor(0);
        imageView3.setImageBitmap(this.b);
        imageView3.setPadding(5, 0, 0, 0);
        imageView3.setOnClickListener(onClickListener);
        relativeLayout.addView(imageView3, layoutParams2);
        RelativeLayout.LayoutParams layoutParams3 = d.k < 1.0f ? new RelativeLayout.LayoutParams((int) PurchaseCode.NONE_NETWORK, 40) : new RelativeLayout.LayoutParams(-2, -2);
        layoutParams3.addRule(13);
        ImageView imageView4 = new ImageView(context);
        imageView4.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        imageView4.setImageBitmap(aa.b(d.getContext(), "mmiap/image/vertical/logo1.png"));
        relativeLayout.addView(imageView4, layoutParams3);
        ImageView imageView5 = new ImageView(context);
        imageView5.setTag(1);
        RelativeLayout.LayoutParams layoutParams4 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams4.addRule(15, -1);
        layoutParams4.addRule(11, -1);
        imageView5.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        imageView5.setId(2);
        imageView5.setBackgroundColor(0);
        imageView5.setImageBitmap(this.d);
        imageView5.setPadding(0, 0, 5, 0);
        imageView5.setOnClickListener(onClickListener);
        relativeLayout.addView(imageView5, layoutParams4);
        return relativeLayout;
    }

    public void dismiss() {
        u.a().u();
        super.dismiss();
    }

    public View f(Context context) {
        this.f269a = new RelativeLayout(context);
        this.f269a.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams.addRule(10);
        View a2 = a(context, this.f271b, this.c, this.g);
        a2.setId(10001);
        this.f269a.addView(a2, layoutParams);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-1, -1);
        layoutParams2.addRule(3, 10001);
        this.f268a = new WebView(context);
        this.f268a.setBackgroundColor(-1);
        this.f268a.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        this.f268a.getSettings().setJavaScriptEnabled(true);
        this.f268a.requestFocus();
        this.f269a.addView(this.f268a, layoutParams2);
        this.f272b = new ProgressBar(context);
        RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams3.addRule(13);
        this.f269a.addView(this.f272b, layoutParams3);
        this.f268a.setWebViewClient(new ae(this, layoutParams3));
        this.f268a.setWebChromeClient(new af(this));
        try {
            this.f268a.loadUrl(this.aw);
        } catch (Exception e) {
            e.d("webview", "error" + e);
        }
        return this.f269a;
    }

    public void show() {
        setContentView(f(d.getContext()));
        setCancelable(false);
        super.show();
    }
}
