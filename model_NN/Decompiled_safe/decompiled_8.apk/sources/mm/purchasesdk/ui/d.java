package mm.purchasesdk.ui;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Random;

public class d {

    /* renamed from: a  reason: collision with root package name */
    private View.OnLongClickListener f3155a = new h(this);

    /* renamed from: a  reason: collision with other field name */
    private View f274a;

    /* renamed from: a  reason: collision with other field name */
    private PopupWindow f275a;
    private View.OnClickListener b = new f(this);

    /* renamed from: b  reason: collision with other field name */
    private View.OnTouchListener f276b = new e(this);

    /* renamed from: b  reason: collision with other field name */
    private View f277b;
    private HashMap c = null;
    private Boolean f = true;
    private Drawable m;
    /* access modifiers changed from: private */
    public Drawable n;
    /* access modifiers changed from: private */
    public Drawable o;
    /* access modifiers changed from: private */
    public Drawable p;
    private Drawable q;
    private Drawable r;
    /* access modifiers changed from: private */
    public Drawable s;

    public d(View view, Boolean bool) {
        this.f = bool;
        this.f277b = view;
        try {
            d();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    public int a(String str) {
        if (this.c == null) {
            this.c = new HashMap();
            this.c.put("0", 7);
            this.c.put("1", 8);
            this.c.put("2", 9);
            this.c.put("3", 10);
            this.c.put("4", 11);
            this.c.put("5", 12);
            this.c.put("6", 13);
            this.c.put("7", 14);
            this.c.put("8", 15);
            this.c.put("9", 16);
            this.c.put("back", 4);
            this.c.put("clear", 28);
            this.c.put("delete", 67);
        }
        if (this.c.get(str) != null) {
            return ((Integer) this.c.get(str)).intValue();
        }
        return 0;
    }

    /* renamed from: a  reason: collision with other method in class */
    private Bitmap m302a(String str) {
        Bitmap a2 = aa.a(mm.purchasesdk.l.d.getContext(), str);
        if (a2 != null) {
            return a2;
        }
        throw new Exception("error," + str + " is not exist!");
    }

    private View d(Context context) {
        LinearLayout linearLayout = new LinearLayout(context);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2);
        linearLayout.setOrientation(1);
        linearLayout.setLayoutParams(layoutParams);
        linearLayout.setBackgroundDrawable(this.m);
        linearLayout.setPadding(5, 5, 5, 5);
        LinkedList linkedList = new LinkedList();
        for (int i = 0; i < 10; i++) {
            Button button = new Button(context);
            button.setPadding(1, 1, 1, 1);
            button.setBackgroundDrawable(this.n);
            button.setText(String.valueOf(i));
            button.setTextSize(25.0f);
            button.setOnClickListener(this.b);
            button.setOnTouchListener(this.f276b);
            button.setTag(0);
            linkedList.add(button);
        }
        Random random = new Random();
        LinearLayout linearLayout2 = new LinearLayout(context);
        linearLayout2.setLayoutParams(new LinearLayout.LayoutParams(-1, 0, 1.0f));
        linearLayout2.setBackgroundColor(0);
        for (int i2 = 0; i2 < 3; i2++) {
            Button button2 = (Button) linkedList.remove(random.nextInt(10 - i2));
            LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(0, -1, 1.0f);
            layoutParams2.rightMargin = 5;
            layoutParams2.bottomMargin = 5;
            button2.setLayoutParams(layoutParams2);
            linearLayout2.addView(button2);
        }
        Button button3 = new Button(context);
        LinearLayout.LayoutParams layoutParams3 = new LinearLayout.LayoutParams(0, -1, 1.0f);
        layoutParams3.bottomMargin = 5;
        button3.setLayoutParams(layoutParams3);
        button3.setBackgroundDrawable(this.o);
        button3.setTag(1);
        button3.setOnTouchListener(this.f276b);
        button3.setOnLongClickListener(this.f3155a);
        button3.setOnClickListener(this.b);
        linearLayout2.addView(button3);
        linearLayout.addView(linearLayout2);
        LinearLayout linearLayout3 = new LinearLayout(context);
        linearLayout3.setLayoutParams(new LinearLayout.LayoutParams(-1, 0, 1.0f));
        linearLayout3.setBackgroundColor(0);
        int i3 = 3;
        while (true) {
            int i4 = i3;
            if (i4 >= 7) {
                break;
            }
            Button button4 = (Button) linkedList.remove(random.nextInt(10 - i4));
            LinearLayout.LayoutParams layoutParams4 = new LinearLayout.LayoutParams(0, -1, 1.0f);
            if (i4 != 6) {
                layoutParams4.rightMargin = 5;
            }
            layoutParams4.bottomMargin = 5;
            button4.setLayoutParams(layoutParams4);
            linearLayout3.addView(button4);
            i3 = i4 + 1;
        }
        linearLayout.addView(linearLayout3);
        LinearLayout linearLayout4 = new LinearLayout(context);
        linearLayout4.setLayoutParams(new LinearLayout.LayoutParams(-1, 0, 1.0f));
        linearLayout4.setBackgroundColor(0);
        int i5 = 7;
        while (true) {
            int i6 = i5;
            if (i6 < 10) {
                Button button5 = (Button) linkedList.remove(random.nextInt(10 - i6));
                LinearLayout.LayoutParams layoutParams5 = new LinearLayout.LayoutParams(0, -1, 1.0f);
                layoutParams5.rightMargin = 5;
                layoutParams5.bottomMargin = 5;
                button5.setLayoutParams(layoutParams5);
                linearLayout4.addView(button5);
                i5 = i6 + 1;
            } else {
                Button button6 = new Button(context);
                LinearLayout.LayoutParams layoutParams6 = new LinearLayout.LayoutParams(0, -1, 1.0f);
                layoutParams6.bottomMargin = 5;
                button6.setLayoutParams(layoutParams6);
                button6.setBackgroundDrawable(this.p);
                button6.setTag(2);
                button6.setOnTouchListener(this.f276b);
                button6.setOnClickListener(this.b);
                linearLayout4.addView(button6);
                linearLayout.addView(linearLayout4);
                return linearLayout;
            }
        }
    }

    private void d() {
        if (this.m == null) {
            Bitmap a2 = aa.a(mm.purchasesdk.l.d.getContext(), "mmiap/image/vertical/keyboard_bg.png");
            if (a2 != null) {
                this.m = new BitmapDrawable(a2);
            } else {
                throw new Exception("error!mmiap/image/vertical/keyboard_bg.pngread failed!");
            }
        }
        if (this.n == null) {
            Bitmap a3 = m302a("mmiap/image/vertical/keyboard_button.png");
            if (a3 != null) {
                this.n = new BitmapDrawable(a3);
            } else {
                throw new Exception("error!mmiap/image/vertical/keyboard_button.pngread failed!");
            }
        }
        if (this.o == null) {
            Bitmap a4 = m302a("mmiap/image/vertical/keyboard_button_delete.png");
            if (a4 != null) {
                this.o = new BitmapDrawable(a4);
            } else {
                throw new Exception("error!mmiap/image/vertical/keyboard_button_delete.pngread failed!");
            }
        }
        if (this.p == null) {
            Bitmap a5 = m302a("mmiap/image/vertical/keyboard_button_hide.png");
            if (a5 != null) {
                this.p = new BitmapDrawable(a5);
            } else {
                throw new Exception("error!mmiap/image/vertical/keyboard_button_hide.pngread failed!");
            }
        }
        if (this.q == null) {
            Bitmap a6 = m302a("mmiap/image/vertical/keyboard_button_press.png");
            if (a6 != null) {
                this.q = new BitmapDrawable(a6);
            } else {
                throw new Exception("error!mmiap/image/vertical/keyboard_button_press.pngread failed!");
            }
        }
        if (this.s == null) {
            Bitmap a7 = m302a("mmiap/image/vertical/keyboard_button_hide_press.png");
            if (a7 != null) {
                this.s = new BitmapDrawable(a7);
            } else {
                throw new Exception("error!mmiap/image/vertical/keyboard_button_hide_press.pngread failed!");
            }
        }
        if (this.r == null) {
            Bitmap a8 = m302a("mmiap/image/vertical/keyboard_button_delete_press.png");
            if (a8 != null) {
                this.r = new BitmapDrawable(a8);
                return;
            }
            throw new Exception("error!mmiap/image/vertical/keyboard_button_delete_press.pngread failed!");
        }
    }

    private View e(Context context) {
        LinearLayout linearLayout = new LinearLayout(context);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2);
        linearLayout.setOrientation(1);
        linearLayout.setLayoutParams(layoutParams);
        linearLayout.setBackgroundDrawable(this.m);
        linearLayout.setPadding(5, 5, 5, 5);
        Bitmap a2 = m302a("mmiap/image/vertical/keyboard_button.png");
        LinkedList linkedList = new LinkedList();
        for (int i = 0; i < 10; i++) {
            Button button = new Button(context);
            button.setPadding(1, 1, 1, 1);
            if (a2 != null) {
                button.setBackgroundDrawable(new BitmapDrawable(a2));
            }
            button.setText(String.valueOf(i));
            button.setTextSize(25.0f);
            button.setOnClickListener(this.b);
            button.setOnTouchListener(this.f276b);
            button.setTag(0);
            linkedList.add(button);
        }
        Random random = new Random();
        LinearLayout linearLayout2 = new LinearLayout(context);
        linearLayout2.setLayoutParams(new LinearLayout.LayoutParams(-1, 0, 1.0f));
        linearLayout2.setBackgroundColor(0);
        for (int i2 = 0; i2 < 5; i2++) {
            Button button2 = (Button) linkedList.remove(random.nextInt(10 - i2));
            LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(0, -1, 1.0f);
            layoutParams2.rightMargin = 5;
            layoutParams2.bottomMargin = 5;
            button2.setLayoutParams(layoutParams2);
            linearLayout2.addView(button2);
        }
        Button button3 = new Button(context);
        LinearLayout.LayoutParams layoutParams3 = new LinearLayout.LayoutParams(0, -1, 1.0f);
        layoutParams3.bottomMargin = 5;
        button3.setLayoutParams(layoutParams3);
        button3.setBackgroundDrawable(this.o);
        button3.setTag(1);
        button3.setOnLongClickListener(this.f3155a);
        button3.setOnTouchListener(this.f276b);
        button3.setOnClickListener(this.b);
        linearLayout2.addView(button3);
        linearLayout.addView(linearLayout2);
        LinearLayout linearLayout3 = new LinearLayout(context);
        linearLayout3.setLayoutParams(new LinearLayout.LayoutParams(-1, 0, 1.0f));
        linearLayout3.setBackgroundColor(0);
        for (int i3 = 5; i3 < 10; i3++) {
            Button button4 = (Button) linkedList.remove(random.nextInt(10 - i3));
            LinearLayout.LayoutParams layoutParams4 = new LinearLayout.LayoutParams(0, -1, 1.0f);
            layoutParams4.rightMargin = 5;
            layoutParams4.bottomMargin = 5;
            button4.setLayoutParams(layoutParams4);
            linearLayout3.addView(button4);
        }
        Button button5 = new Button(context);
        LinearLayout.LayoutParams layoutParams5 = new LinearLayout.LayoutParams(0, -1, 1.0f);
        layoutParams5.bottomMargin = 5;
        button5.setLayoutParams(layoutParams5);
        button5.setBackgroundDrawable(this.p);
        button5.setTag(2);
        button5.setOnTouchListener(this.f276b);
        button5.setOnClickListener(this.b);
        linearLayout3.addView(button5);
        linearLayout.addView(linearLayout3);
        return linearLayout;
    }

    public void b(View view) {
        this.f274a = view;
        this.f = Boolean.valueOf(!aa.f265d.booleanValue());
        if (this.f275a == null) {
            this.f275a = new PopupWindow(j(), -1, aa.S);
        }
        if (!this.f275a.isShowing()) {
            this.f275a.showAtLocation(this.f274a, 80, 0, 0);
        }
    }

    public View j() {
        return this.f.booleanValue() ? d(mm.purchasesdk.l.d.getContext()) : e(mm.purchasesdk.l.d.getContext());
    }
}
