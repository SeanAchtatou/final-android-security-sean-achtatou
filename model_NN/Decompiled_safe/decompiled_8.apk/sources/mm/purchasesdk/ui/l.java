package mm.purchasesdk.ui;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.NinePatch;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.NinePatchDrawable;
import android.os.Handler;
import android.text.Html;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import java.util.ArrayList;
import java.util.HashMap;
import mm.purchasesdk.b;
import mm.purchasesdk.b.a;
import mm.purchasesdk.l.d;

public class l extends b {
    private Drawable A;
    private final String TAG = "PurchaseDialog";

    /* renamed from: a  reason: collision with root package name */
    private Drawable f3162a;

    /* renamed from: a  reason: collision with other field name */
    private Handler f278a;

    /* renamed from: a  reason: collision with other field name */
    private View.OnFocusChangeListener f279a = new m(this);

    /* renamed from: a  reason: collision with other field name */
    public View.OnKeyListener f280a = new o(this);

    /* renamed from: a  reason: collision with other field name */
    private View.OnTouchListener f281a = new n(this);

    /* renamed from: a  reason: collision with other field name */
    private ImageView f282a;

    /* renamed from: a  reason: collision with other field name */
    private LinearLayout f283a;

    /* renamed from: a  reason: collision with other field name */
    private ProgressBar f284a;

    /* renamed from: a  reason: collision with other field name */
    private ScrollView f285a;

    /* renamed from: a  reason: collision with other field name */
    private Boolean f286a = true;

    /* renamed from: a  reason: collision with other field name */
    private b f287a;
    private Handler b;

    /* renamed from: b  reason: collision with other field name */
    private ImageView f288b;

    /* renamed from: b  reason: collision with other field name */
    private a f289b;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with other field name */
    public d f290b = null;
    private View.OnClickListener c = new p(this);

    /* renamed from: c  reason: collision with other field name */
    private EditText f291c;

    /* renamed from: c  reason: collision with other field name */
    private TextView f292c;
    private View.OnClickListener d = new q(this);

    /* renamed from: d  reason: collision with other field name */
    private View f293d;

    /* renamed from: d  reason: collision with other field name */
    private EditText f294d;

    /* renamed from: d  reason: collision with other field name */
    private TextView f295d;

    /* renamed from: d  reason: collision with other field name */
    private Boolean f296d = false;
    private View.OnClickListener e = new t(this);

    /* renamed from: e  reason: collision with other field name */
    private Button f297e;
    /* access modifiers changed from: private */

    /* renamed from: e  reason: collision with other field name */
    public EditText f298e;

    /* renamed from: e  reason: collision with other field name */
    private TextView f299e;
    private Bitmap g;
    private Bitmap h = null;
    private boolean j = true;
    private Drawable t = null;
    private Drawable u = null;
    private Drawable v = null;
    private Drawable w = null;
    private Drawable x = null;
    private Drawable y = null;
    private Drawable z = null;

    public l(Context context, Handler handler, Handler handler2, b bVar, a aVar) {
        super(context, 16973829);
        setOwnerActivity((Activity) context);
        getWindow().requestFeature(1);
        this.f287a = bVar;
        this.b = handler2;
        this.f278a = handler;
        if (context == null || ((Activity) context) != getOwnerActivity()) {
            d.setContext(getOwnerActivity());
        }
        a(aVar);
        this.f297e = new Button(context);
    }

    private View a(String str) {
        LinearLayout linearLayout = new LinearLayout(d.getContext());
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2);
        linearLayout.setOrientation(1);
        layoutParams.leftMargin = 10;
        layoutParams.rightMargin = 10;
        layoutParams.topMargin = 10;
        layoutParams.bottomMargin = 10;
        linearLayout.setLayoutParams(layoutParams);
        linearLayout.setBackgroundDrawable(a.a(-1, 10, 10));
        linearLayout.setPadding(6, 6, 6, 6);
        TextView textView = new TextView(d.getContext());
        String D = d.D();
        if (str == null || str.trim().length() == 0) {
            textView.setText(D);
        } else {
            textView.setText(Html.fromHtml("<a href=\"" + this.f289b.d() + "\">" + D + "</a> "));
            textView.setMovementMethod(LinkMovementMethod.getInstance());
        }
        textView.setLineSpacing(1.0f, 1.3f);
        textView.setTextSize(aa.i);
        textView.setTextColor(-8289919);
        linearLayout.addView(textView);
        return linearLayout;
    }

    private View a(ab abVar) {
        LinearLayout linearLayout = new LinearLayout(d.getContext());
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2);
        linearLayout.setOrientation(0);
        linearLayout.setPadding(10, 5, 10, 5);
        linearLayout.setLayoutParams(layoutParams);
        TextView textView = new TextView(d.getContext());
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-2, -2);
        layoutParams2.rightMargin = 3;
        textView.setLayoutParams(layoutParams2);
        textView.setText(abVar.au);
        textView.setTextColor(abVar.W);
        textView.setTextSize(aa.i);
        linearLayout.addView(textView);
        ProductItemView productItemView = new ProductItemView(d.getContext());
        productItemView.setTextSize(aa.i);
        productItemView.setLayoutParams(new LinearLayout.LayoutParams(-2, -2));
        productItemView.setText(abVar.mValue);
        productItemView.setSingleLine();
        productItemView.setEllipsize(TextUtils.TruncateAt.valueOf("MARQUEE"));
        productItemView.setMarqueeRepeatLimit(-1);
        productItemView.setTextColor(abVar.X);
        linearLayout.addView(productItemView);
        return linearLayout;
    }

    private View a(ab abVar, ab abVar2) {
        LinearLayout linearLayout = new LinearLayout(d.getContext());
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2);
        linearLayout.setPadding(10, 5, 10, 5);
        linearLayout.setLayoutParams(layoutParams);
        LinearLayout linearLayout2 = new LinearLayout(d.getContext());
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(0, -2, 1.0f);
        linearLayout2.setOrientation(0);
        linearLayout2.setLayoutParams(layoutParams2);
        TextView textView = new TextView(d.getContext());
        textView.setText(abVar.au);
        LinearLayout.LayoutParams layoutParams3 = new LinearLayout.LayoutParams(-2, -2);
        layoutParams3.rightMargin = 3;
        textView.setLayoutParams(layoutParams3);
        textView.setTextColor(abVar.W);
        textView.setTextSize(aa.i);
        linearLayout2.addView(textView);
        ProductItemView productItemView = new ProductItemView(d.getContext());
        productItemView.setLayoutParams(new LinearLayout.LayoutParams(-2, -2));
        productItemView.setTextSize(aa.i);
        productItemView.setSingleLine();
        productItemView.setEllipsize(TextUtils.TruncateAt.valueOf("MARQUEE"));
        productItemView.setMarqueeRepeatLimit(-1);
        productItemView.setText(abVar.mValue);
        productItemView.setTextColor(abVar.X);
        linearLayout2.addView(productItemView);
        linearLayout.addView(linearLayout2);
        LinearLayout linearLayout3 = new LinearLayout(d.getContext());
        LinearLayout.LayoutParams layoutParams4 = new LinearLayout.LayoutParams(0, -2, 1.0f);
        linearLayout3.setOrientation(0);
        linearLayout3.setLayoutParams(layoutParams4);
        TextView textView2 = new TextView(d.getContext());
        textView2.setLayoutParams(layoutParams3);
        textView2.setText(abVar2.au);
        textView2.setTextColor(abVar2.W);
        textView2.setTextSize(aa.i);
        linearLayout3.addView(textView2);
        ProductItemView productItemView2 = new ProductItemView(d.getContext());
        productItemView2.setLayoutParams(new LinearLayout.LayoutParams(-2, -2));
        productItemView2.setTextSize(aa.i);
        productItemView2.setSingleLine(true);
        productItemView2.setSingleLine();
        productItemView2.setEllipsize(TextUtils.TruncateAt.valueOf("MARQUEE"));
        productItemView2.setMarqueeRepeatLimit(-1);
        productItemView2.setText(abVar2.mValue);
        productItemView2.setTextColor(abVar2.X);
        linearLayout3.addView(productItemView2);
        linearLayout.addView(linearLayout3);
        return linearLayout;
    }

    private View a(ab abVar, ab abVar2, ab abVar3) {
        LinearLayout linearLayout = new LinearLayout(d.getContext());
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2);
        linearLayout.setPadding(10, 5, 10, 5);
        linearLayout.setLayoutParams(layoutParams);
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-2, -2);
        linearLayout.addView(a(abVar, layoutParams2));
        linearLayout.addView(a(abVar2, layoutParams2));
        linearLayout.addView(a(abVar3, layoutParams2));
        return linearLayout;
    }

    private ImageView a(Bitmap bitmap) {
        ImageView imageView = new ImageView(d.getContext());
        imageView.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        if (bitmap != null) {
            if (this.A == null) {
                this.A = new BitmapDrawable(bitmap);
            }
            imageView.setBackgroundDrawable(this.A);
        }
        return imageView;
    }

    private LinearLayout a(ab abVar, LinearLayout.LayoutParams layoutParams) {
        LinearLayout linearLayout = new LinearLayout(d.getContext());
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(0, -2, 1.0f);
        linearLayout.setOrientation(0);
        linearLayout.setLayoutParams(layoutParams2);
        TextView textView = new TextView(d.getContext());
        textView.setText(abVar.au);
        textView.setLayoutParams(layoutParams);
        textView.setTextColor(abVar.W);
        textView.setTextSize(aa.i);
        linearLayout.addView(textView);
        ProductItemView productItemView = new ProductItemView(d.getContext());
        productItemView.setLayoutParams(new LinearLayout.LayoutParams(-2, -2));
        productItemView.setTextSize(aa.i);
        productItemView.setSingleLine(true);
        productItemView.setText(abVar.mValue);
        productItemView.setSingleLine();
        productItemView.setEllipsize(TextUtils.TruncateAt.valueOf("MARQUEE"));
        productItemView.setMarqueeRepeatLimit(-1);
        productItemView.setTextColor(abVar.X);
        linearLayout.addView(productItemView);
        return linearLayout;
    }

    private void d() {
        Bitmap a2;
        Bitmap a3;
        Bitmap a4;
        Bitmap a5;
        Bitmap a6;
        Bitmap a7;
        Bitmap a8;
        Bitmap a9;
        if (this.t == null && (a9 = aa.a(d.getContext(), "mmiap/image/vertical/editbg.9.png")) != null) {
            byte[] ninePatchChunk = a9.getNinePatchChunk();
            NinePatch.isNinePatchChunk(ninePatchChunk);
            this.t = new NinePatchDrawable(a9, ninePatchChunk, new Rect(), null);
        }
        if (this.u == null && (a8 = aa.a(d.getContext(), "mmiap/image/vertical/editbg_a.9.png")) != null) {
            byte[] ninePatchChunk2 = a8.getNinePatchChunk();
            NinePatch.isNinePatchChunk(ninePatchChunk2);
            this.u = new NinePatchDrawable(a8, ninePatchChunk2, new Rect(), null);
        }
        if (this.v == null && (a7 = aa.a(d.getContext(), "mmiap/image/vertical/infobg.9.png")) != null) {
            byte[] ninePatchChunk3 = a7.getNinePatchChunk();
            NinePatch.isNinePatchChunk(ninePatchChunk3);
            this.v = new NinePatchDrawable(a7, ninePatchChunk3, new Rect(), null);
        }
        if (this.x == null && (a6 = aa.a(d.getContext(), "mmiap/image/vertical/infobg.9.png")) != null) {
            byte[] ninePatchChunk4 = a6.getNinePatchChunk();
            NinePatch.isNinePatchChunk(ninePatchChunk4);
            this.x = new NinePatchDrawable(a6, ninePatchChunk4, new Rect(), null);
        }
        if (this.y == null && (a5 = aa.a(d.getContext(), "mmiap/image/vertical/infobg.9.png")) != null) {
            byte[] ninePatchChunk5 = a5.getNinePatchChunk();
            NinePatch.isNinePatchChunk(ninePatchChunk5);
            this.y = new NinePatchDrawable(a5, ninePatchChunk5, new Rect(), null);
        }
        if (this.w == null && (a4 = aa.a(d.getContext(), "mmiap/image/vertical/infobg.9.png")) != null) {
            byte[] ninePatchChunk6 = a4.getNinePatchChunk();
            NinePatch.isNinePatchChunk(ninePatchChunk6);
            this.w = new NinePatchDrawable(a4, ninePatchChunk6, new Rect(), null);
        }
        if (this.f3162a == null && (a3 = aa.a(d.getContext(), "mmiap/image/vertical/bg.png")) != null) {
            this.f3162a = new BitmapDrawable(a3);
        }
        if (this.z == null && (a2 = aa.a(d.getContext(), "mmiap/image/vertical/infobg.9.png")) != null) {
            byte[] ninePatchChunk7 = a2.getNinePatchChunk();
            NinePatch.isNinePatchChunk(ninePatchChunk7);
            this.z = new NinePatchDrawable(a2, ninePatchChunk7, new Rect(), null);
        }
    }

    private int h() {
        String J = d.J();
        if (J != null && J.trim().length() > 0) {
            String substring = J.substring(0, 3);
            if (substring.equals("101")) {
                return 1;
            }
            if (substring.equals("110")) {
                return 0;
            }
            if (substring.equals("111")) {
                return 2;
            }
        }
        return -1;
    }

    private View n() {
        this.f296d = aa.f265d;
        return this.f296d.booleanValue() ? m304p() : o();
    }

    private View o() {
        this.f283a = new LinearLayout(d.getContext());
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -1);
        this.f283a.setOrientation(1);
        this.f283a.setLayoutParams(layoutParams);
        if (1 != h()) {
            this.f283a.addView(b(d.getContext()));
            this.f283a.addView(b(d.getContext(), this.f288b, this.c));
        } else {
            this.f283a.addView(a(d.getContext(), this.f288b, this.c));
        }
        this.f293d = q();
        this.f283a.addView(this.f293d);
        this.f283a.addView(r());
        if (this.j) {
            this.f283a.addView(s());
        }
        if (this.f286a.booleanValue()) {
            this.f283a.addView(f());
        }
        if (1 == h()) {
            this.f283a.addView(w());
        }
        if (true == d.c().booleanValue()) {
            this.f283a.addView(t());
        }
        this.f283a.addView(a(this.f297e, this.d, "确 认 支 付"));
        if (2 == h()) {
            this.f283a.addView(x());
        }
        String c2 = this.f289b.c();
        d.K(c2);
        d.L(this.f289b.d());
        if (!(c2 == null || c2.trim().length() == 0)) {
            this.f283a.addView(a(this.f289b.d()));
        }
        this.f283a.addView(c(d.getContext()));
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-1, -1);
        this.f285a = new ScrollView(d.getContext());
        this.f285a.setLayoutParams(layoutParams2);
        this.f285a.setFillViewport(true);
        this.f285a.setBackgroundDrawable(this.f3162a);
        this.f285a.addView(this.f283a);
        return this.f285a;
    }

    private void p() {
        this.f295d.setOnClickListener(new r(this));
    }

    private View q() {
        View a2;
        int i = 0;
        LinearLayout linearLayout = new LinearLayout(d.getContext());
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2);
        layoutParams.topMargin = aa.R;
        layoutParams.leftMargin = 5;
        layoutParams.rightMargin = 5;
        linearLayout.setLayoutParams(layoutParams);
        linearLayout.setOrientation(1);
        linearLayout.setPadding(5, 0, 5, 0);
        linearLayout.setBackgroundDrawable(this.w);
        Bitmap a3 = aa.a(d.getContext(), "mmiap/image/vertical/infoline.png");
        HashMap a4 = this.f289b.b().m274a();
        ArrayList a5 = this.f289b.b().a();
        int size = a4.size();
        while (i < size) {
            ab abVar = (ab) a4.get((String) a5.get(i));
            if (i == 0) {
                a2 = a(abVar);
            } else if (i == size - 1) {
                a2 = a(abVar);
            } else if (i == size - 2) {
                a2 = a(abVar);
            } else {
                int i2 = i + 1;
                a2 = a(abVar, (ab) a4.get((String) a5.get(i2)));
                i = i2;
            }
            linearLayout.addView(a2);
            if (i != size - 1) {
                linearLayout.addView(a(a3));
            }
            i++;
        }
        return linearLayout;
    }

    /* renamed from: q  reason: collision with other method in class */
    private void m303q() {
        this.f299e.setOnClickListener(new s(this));
    }

    private View r() {
        LinearLayout linearLayout = new LinearLayout(d.getContext());
        linearLayout.setOrientation(0);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2);
        layoutParams.topMargin = aa.R;
        layoutParams.leftMargin = 5;
        layoutParams.rightMargin = 5;
        linearLayout.setLayoutParams(layoutParams);
        linearLayout.setGravity(16);
        linearLayout.setBackgroundDrawable(this.x);
        TextView textView = new TextView(d.getContext());
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-2, -2);
        layoutParams2.rightMargin = 3;
        textView.setPadding(10, 10, 10, 10);
        textView.setLayoutParams(layoutParams2);
        textView.setText("手机号码:");
        textView.setTextColor(-8289919);
        textView.setTextSize(aa.i);
        linearLayout.addView(textView);
        TextView textView2 = new TextView(d.getContext());
        textView2.setTextSize(aa.i);
        textView2.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        textView2.setText(this.f289b.b().o());
        textView2.setTextColor(-16777216);
        linearLayout.addView(textView2);
        return linearLayout;
    }

    private View u() {
        View view;
        int i;
        int i2 = 0;
        LinearLayout linearLayout = new LinearLayout(d.getContext());
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2);
        layoutParams.topMargin = aa.R;
        layoutParams.leftMargin = 5;
        layoutParams.rightMargin = 5;
        linearLayout.setLayoutParams(layoutParams);
        linearLayout.setOrientation(1);
        linearLayout.setPadding(5, 0, 5, 0);
        linearLayout.setBackgroundDrawable(this.w);
        Bitmap a2 = aa.a(d.getContext(), "mmiap/image/vertical/infoline.png");
        HashMap a3 = this.f289b.b().m274a();
        ArrayList a4 = this.f289b.b().a();
        int size = a3.size();
        while (i2 < size) {
            ab abVar = (ab) a3.get((String) a4.get(i2));
            if (i2 == 0) {
                int i3 = i2 + 1;
                view = a(abVar, (ab) a3.get((String) a4.get(i3)));
                i = i3;
            } else if (i2 == size - 1) {
                view = a(abVar);
                i = i2;
            } else if (size - i2 > 0) {
                switch ((size - i2) - 1) {
                    case 1:
                        view = a(abVar);
                        i = i2;
                        break;
                    case 2:
                        int i4 = i2 + 1;
                        view = a(abVar, (ab) a3.get((String) a4.get(i4)));
                        i = i4;
                        break;
                    default:
                        int i5 = i2 + 1;
                        int i6 = i5 + 1;
                        view = a(abVar, (ab) a3.get((String) a4.get(i5)), (ab) a3.get((String) a4.get(i6)));
                        i = i6;
                        break;
                }
            } else {
                view = null;
                i = i2;
            }
            linearLayout.addView(view);
            if (i != size - 1) {
                linearLayout.addView(a(a2));
            }
            i2 = i + 1;
        }
        return linearLayout;
    }

    private View w() {
        LinearLayout linearLayout = new LinearLayout(d.getContext());
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2);
        if (1 == h()) {
            layoutParams.topMargin = aa.R;
            layoutParams.leftMargin = 5;
            layoutParams.rightMargin = 5;
            linearLayout.setPadding(10, 0, 10, 0);
            linearLayout.setBackgroundDrawable(this.z);
        }
        linearLayout.setLayoutParams(layoutParams);
        linearLayout.setOrientation(0);
        if (this.h == null) {
            this.h = aa.a(d.getContext(), "mmiap/image/vertical/icon_chifubao.png");
        }
        if (this.h != null) {
            ImageView imageView = new ImageView(d.getContext());
            LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-2, -2);
            layoutParams2.leftMargin = 10;
            layoutParams2.topMargin = 10;
            layoutParams2.bottomMargin = 10;
            imageView.setLayoutParams(layoutParams2);
            imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
            imageView.setImageBitmap(this.h);
            if (2 == h()) {
                imageView.setOnClickListener(this.e);
            }
            linearLayout.addView(imageView);
        }
        linearLayout.addView(y());
        return linearLayout;
    }

    private View x() {
        LinearLayout linearLayout = new LinearLayout(d.getContext());
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2);
        layoutParams.topMargin = aa.R;
        layoutParams.leftMargin = 5;
        layoutParams.rightMargin = 5;
        linearLayout.setLayoutParams(layoutParams);
        linearLayout.setOrientation(1);
        linearLayout.setPadding(10, 0, 10, 0);
        linearLayout.setBackgroundDrawable(this.z);
        TextView textView = new TextView(d.getContext());
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-1, -2);
        layoutParams2.leftMargin = 10;
        layoutParams2.topMargin = 5;
        layoutParams2.bottomMargin = 5;
        textView.setLayoutParams(layoutParams2);
        textView.setText("您还可以选择：");
        textView.setTextColor(-9671572);
        textView.setTextSize(aa.i);
        linearLayout.addView(textView);
        linearLayout.addView(a(aa.a(d.getContext(), "mmiap/image/vertical/infoline.png")));
        linearLayout.addView(w());
        return linearLayout;
    }

    private View y() {
        LinearLayout linearLayout = new LinearLayout(d.getContext());
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2);
        layoutParams.gravity = 16;
        layoutParams.leftMargin = 5;
        linearLayout.setLayoutParams(layoutParams);
        linearLayout.setOrientation(1);
        linearLayout.setPadding(5, 0, 5, 0);
        TextView textView = new TextView(d.getContext());
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-1, -2);
        layoutParams2.leftMargin = 3;
        layoutParams2.topMargin = 5;
        textView.setLayoutParams(layoutParams2);
        textView.setText("支付宝页面支付");
        textView.setTextColor(-16753749);
        textView.setTextSize(aa.j);
        if (2 == h()) {
            textView.setOnClickListener(this.e);
        }
        linearLayout.addView(textView);
        TextView textView2 = new TextView(d.getContext());
        LinearLayout.LayoutParams layoutParams3 = new LinearLayout.LayoutParams(-1, -2);
        layoutParams3.leftMargin = 3;
        textView2.setLayoutParams(layoutParams3);
        textView2.setText("支持银行卡支付，无需登录");
        textView2.setTextColor(-9671572);
        textView2.setTextSize(aa.i);
        linearLayout.addView(textView2);
        return linearLayout;
    }

    public void a(a aVar) {
        this.f289b = aVar;
        this.j = aVar.m272b();
        this.g = aVar.m270a();
        this.f286a = aVar.m271a();
    }

    public void b(a aVar) {
        this.f289b = aVar;
        this.g = this.f289b.m270a();
        if (this.f289b == null || this.f289b.m270a() == null) {
            this.f284a.setVisibility(8);
            this.f292c.setVisibility(0);
            this.f282a.setVisibility(8);
            return;
        }
        this.f282a.setImageBitmap(this.g);
        this.f284a.setVisibility(8);
        this.f292c.setVisibility(8);
        this.f282a.setVisibility(0);
    }

    public void dismiss() {
        super.dismiss();
    }

    public View f() {
        LinearLayout linearLayout = new LinearLayout(d.getContext());
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2);
        layoutParams.topMargin = aa.R;
        layoutParams.leftMargin = 5;
        layoutParams.rightMargin = 5;
        linearLayout.setLayoutParams(layoutParams);
        linearLayout.setOrientation(1);
        linearLayout.setPadding(10, 5, 10, 5);
        linearLayout.setBackgroundDrawable(this.v);
        LinearLayout linearLayout2 = new LinearLayout(d.getContext());
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-1, -2);
        linearLayout2.setPadding(10, 5, 10, 5);
        linearLayout2.setLayoutParams(layoutParams2);
        TextView textView = new TextView(d.getContext());
        textView.setText("验证码:");
        LinearLayout.LayoutParams layoutParams3 = new LinearLayout.LayoutParams(-2, -2);
        layoutParams3.rightMargin = 6;
        layoutParams3.gravity = 16;
        textView.setLayoutParams(layoutParams3);
        textView.setTextColor(-8289919);
        textView.setTextSize(aa.i);
        linearLayout2.addView(textView);
        linearLayout.addView(linearLayout2);
        this.f282a = new ImageView(d.getContext());
        this.f282a.setLayoutParams(layoutParams3);
        if (this.g != null) {
            this.f282a.setImageBitmap(this.g);
        } else {
            Bitmap a2 = aa.a(d.getContext(), "mmiap/image/vertical/yanzhengma_bg.png");
            if (a2 != null) {
                this.f282a.setImageBitmap(a2);
            }
        }
        linearLayout2.addView(this.f282a);
        this.f284a = new ProgressBar(d.getContext(), null, 16842873);
        RelativeLayout.LayoutParams layoutParams4 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams4.addRule(13);
        this.f284a.setLayoutParams(layoutParams4);
        this.f284a.setIndeterminate(true);
        this.f284a.setVisibility(0);
        linearLayout2.addView(this.f284a);
        this.f292c = new TextView(d.getContext());
        this.f292c.setLayoutParams(new LinearLayout.LayoutParams(-2, -2));
        this.f292c.setGravity(17);
        this.f292c.setVisibility(8);
        this.f292c.setText("刷新失败，请重试");
        this.f292c.setTextColor(-65536);
        linearLayout2.addView(this.f292c);
        this.f295d = new TextView(d.getContext());
        LinearLayout.LayoutParams layoutParams5 = new LinearLayout.LayoutParams(-2, -2);
        layoutParams5.gravity = 16;
        this.f295d.setLayoutParams(layoutParams5);
        this.f295d.setPadding(1, 1, 1, 1);
        this.f295d.setText(Html.fromHtml("<u>看不清</u>"));
        this.f295d.setTextSize(aa.i);
        this.f295d.setTextColor(-16776961);
        p();
        linearLayout2.addView(this.f295d);
        LinearLayout linearLayout3 = new LinearLayout(d.getContext());
        LinearLayout.LayoutParams layoutParams6 = new LinearLayout.LayoutParams(-1, -2);
        linearLayout3.setPadding(10, 5, 10, 5);
        linearLayout3.setLayoutParams(layoutParams6);
        TextView textView2 = new TextView(d.getContext());
        textView2.setText("请输入答案:");
        LinearLayout.LayoutParams layoutParams7 = new LinearLayout.LayoutParams(0, -2, 1.0f);
        layoutParams7.rightMargin = 6;
        layoutParams7.gravity = 16;
        textView2.setLayoutParams(layoutParams3);
        textView2.setTextColor(-8289919);
        textView2.setTextSize(aa.i);
        linearLayout3.addView(textView2);
        this.f291c = new EditText(d.getContext());
        if (d.k < 1.0f) {
            this.f291c.setLayoutParams(new LinearLayout.LayoutParams(0, 30, 1.0f));
        } else {
            this.f291c.setLayoutParams(new LinearLayout.LayoutParams(0, -2, 1.0f));
        }
        this.f291c.setOnKeyListener(this.f280a);
        this.f291c.setOnFocusChangeListener(this.f279a);
        this.f291c.setOnTouchListener(this.f281a);
        this.f291c.setCursorVisible(true);
        this.f291c.setFocusableInTouchMode(true);
        this.f291c.setBackgroundDrawable(this.u);
        this.f291c.setId(0);
        this.f291c.setHint("验证码答案");
        this.f291c.setInputType(0);
        linearLayout3.addView(this.f291c);
        TextView textView3 = new TextView(d.getContext());
        textView3.setLayoutParams(new LinearLayout.LayoutParams(0, -2, 1.0f));
        linearLayout3.addView(textView3);
        linearLayout.addView(linearLayout3);
        return linearLayout;
    }

    public String getPassword() {
        return this.f294d == null ? PoiTypeDef.All : this.f294d.getText().toString();
    }

    public String m() {
        return this.f298e != null ? this.f298e.getText().toString() : PoiTypeDef.All;
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }

    /* renamed from: p  reason: collision with other method in class */
    public View m304p() {
        this.f283a = new LinearLayout(d.getContext());
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -1);
        this.f283a.setOrientation(1);
        this.f283a.setLayoutParams(layoutParams);
        if (1 != h()) {
            this.f283a.addView(b(d.getContext()));
            this.f283a.addView(b(d.getContext(), this.f288b, this.c));
        } else {
            this.f283a.addView(a(d.getContext(), this.f288b, this.c));
        }
        this.f293d = u();
        this.f283a.addView(this.f293d);
        this.f283a.addView(r());
        if (this.j) {
            this.f283a.addView(s());
        }
        if (this.f286a.booleanValue()) {
            this.f283a.addView(v());
        }
        if (1 == h()) {
            this.f283a.addView(w());
        }
        if (true == d.c().booleanValue()) {
            this.f283a.addView(t());
        }
        this.f283a.addView(a(this.f297e, this.d, "确 认 支 付"));
        if (2 == h()) {
            this.f283a.addView(x());
        }
        String c2 = this.f289b.c();
        d.K(c2);
        d.L(this.f289b.d());
        if (!(c2 == null || c2.trim().length() == 0)) {
            this.f283a.addView(a(this.f289b.d()));
        }
        this.f283a.addView(c(d.getContext()));
        this.f285a = new ScrollView(d.getContext());
        this.f285a.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        this.f285a.setFillViewport(true);
        this.f285a.setBackgroundDrawable(this.f3162a);
        this.f285a.addView(this.f283a);
        return this.f285a;
    }

    public View s() {
        LinearLayout linearLayout = new LinearLayout(d.getContext());
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2);
        layoutParams.topMargin = aa.R;
        layoutParams.leftMargin = 5;
        layoutParams.rightMargin = 5;
        linearLayout.setLayoutParams(layoutParams);
        linearLayout.setOrientation(0);
        linearLayout.setBackgroundDrawable(this.y);
        linearLayout.setGravity(16);
        TextView textView = new TextView(d.getContext());
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-2, -2);
        layoutParams2.rightMargin = 3;
        textView.setPadding(10, 5, 10, 5);
        textView.setLayoutParams(layoutParams2);
        textView.setText("支付密码:");
        textView.setTextColor(-8289919);
        textView.setTextSize(aa.i);
        linearLayout.addView(textView);
        this.f294d = new EditText(d.getContext());
        this.f294d.setId(1);
        LinearLayout.LayoutParams layoutParams3 = d.k < 1.0f ? new LinearLayout.LayoutParams(-1, 30) : new LinearLayout.LayoutParams(-1, -2);
        layoutParams3.rightMargin = 4;
        this.f294d.setLayoutParams(layoutParams3);
        this.f294d.setOnFocusChangeListener(this.f279a);
        this.f294d.setOnKeyListener(this.f280a);
        this.f294d.setOnTouchListener(this.f281a);
        this.f294d.setCursorVisible(true);
        this.f294d.setSingleLine();
        this.f294d.setFocusableInTouchMode(true);
        this.f294d.setTransformationMethod(PasswordTransformationMethod.getInstance());
        this.f294d.setBackgroundDrawable(this.u);
        this.f294d.setHint("请输入支付密码");
        this.f294d.setInputType(0);
        linearLayout.addView(this.f294d);
        return linearLayout;
    }

    public void show() {
        d();
        setContentView(n());
        setCancelable(false);
        super.show();
    }

    public View t() {
        LinearLayout linearLayout = new LinearLayout(d.getContext());
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2);
        layoutParams.topMargin = aa.R;
        layoutParams.leftMargin = 5;
        layoutParams.rightMargin = 5;
        linearLayout.setLayoutParams(layoutParams);
        linearLayout.setOrientation(1);
        linearLayout.setPadding(10, 5, 10, 5);
        linearLayout.setBackgroundDrawable(this.v);
        LinearLayout linearLayout2 = new LinearLayout(d.getContext());
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-1, -2);
        linearLayout2.setPadding(10, 5, 10, 5);
        linearLayout2.setLayoutParams(layoutParams2);
        linearLayout2.setGravity(16);
        TextView textView = new TextView(d.getContext());
        textView.setText("手机验证码:");
        LinearLayout.LayoutParams layoutParams3 = new LinearLayout.LayoutParams(-2, -2);
        layoutParams3.rightMargin = 6;
        layoutParams3.gravity = 16;
        textView.setLayoutParams(layoutParams3);
        textView.setTextColor(-8289919);
        textView.setTextSize(aa.i);
        linearLayout2.addView(textView);
        linearLayout.addView(linearLayout2);
        this.f298e = new EditText(d.getContext());
        LinearLayout.LayoutParams layoutParams4 = d.k < 1.0f ? new LinearLayout.LayoutParams(-2, 30) : new LinearLayout.LayoutParams(-2, -2);
        layoutParams4.gravity = 16;
        this.f298e.setLayoutParams(layoutParams4);
        this.f298e.setOnKeyListener(this.f280a);
        this.f298e.setOnFocusChangeListener(this.f279a);
        this.f298e.setOnTouchListener(this.f281a);
        this.f298e.setCursorVisible(true);
        this.f298e.setFocusableInTouchMode(true);
        this.f298e.setBackgroundDrawable(this.u);
        this.f298e.setId(2);
        this.f298e.setHint("验证码答案");
        this.f298e.setInputType(0);
        linearLayout2.addView(this.f298e);
        this.f299e = new TextView(d.getContext());
        LinearLayout.LayoutParams layoutParams5 = new LinearLayout.LayoutParams(-2, -2);
        layoutParams5.gravity = 16;
        this.f299e.setLayoutParams(layoutParams5);
        this.f299e.setPadding(1, 1, 1, 1);
        this.f299e.setText(Html.fromHtml("<u>获取验证码</u>"));
        this.f299e.setTextSize(aa.i);
        this.f299e.setTextColor(-16776961);
        m303q();
        linearLayout2.addView(this.f299e);
        return linearLayout;
    }

    public View v() {
        LinearLayout linearLayout = new LinearLayout(d.getContext());
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2);
        layoutParams.topMargin = aa.R;
        layoutParams.leftMargin = 5;
        layoutParams.rightMargin = 5;
        linearLayout.setLayoutParams(layoutParams);
        linearLayout.setOrientation(0);
        linearLayout.setPadding(10, 5, 10, 5);
        linearLayout.setBackgroundDrawable(this.v);
        TextView textView = new TextView(d.getContext());
        textView.setText("请输入答案:");
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-2, -2);
        layoutParams2.rightMargin = 10;
        layoutParams2.gravity = 16;
        textView.setPadding(10, 5, 0, 5);
        textView.setLayoutParams(layoutParams2);
        textView.setTextColor(-8289919);
        textView.setTextSize(aa.i);
        linearLayout.addView(textView);
        this.f282a = new ImageView(d.getContext());
        this.f282a.setLayoutParams(layoutParams2);
        if (this.g != null) {
            this.f282a.setImageBitmap(this.g);
        } else {
            Bitmap a2 = aa.a(d.getContext(), "mmiap/image/vertical/yanzhengma_bg.png");
            if (a2 != null) {
                this.f282a.setImageBitmap(a2);
            }
        }
        linearLayout.addView(this.f282a);
        this.f284a = new ProgressBar(d.getContext());
        RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams3.addRule(13);
        this.f284a.setLayoutParams(layoutParams3);
        this.f284a.setIndeterminate(true);
        this.f284a.setVisibility(8);
        linearLayout.addView(this.f284a);
        this.f292c = new TextView(d.getContext());
        this.f292c.setLayoutParams(new LinearLayout.LayoutParams(-2, -2));
        this.f292c.setGravity(17);
        this.f292c.setVisibility(8);
        this.f292c.setText("刷新失败，请重试");
        this.f292c.setTextColor(-65536);
        linearLayout.addView(this.f292c);
        this.f295d = new TextView(d.getContext());
        LinearLayout.LayoutParams layoutParams4 = new LinearLayout.LayoutParams(-2, -2);
        layoutParams4.rightMargin = 10;
        layoutParams4.gravity = 16;
        this.f295d.setLayoutParams(layoutParams4);
        this.f295d.setPadding(1, 1, 1, 1);
        this.f295d.setText(Html.fromHtml("<u>看不清</u>"));
        this.f295d.setTextSize(aa.i);
        this.f295d.setTextColor(-16776961);
        p();
        linearLayout.addView(this.f295d);
        TextView textView2 = new TextView(d.getContext());
        textView2.setText("请输入答案:");
        LinearLayout.LayoutParams layoutParams5 = new LinearLayout.LayoutParams(0, -2, 1.0f);
        layoutParams5.leftMargin = 10;
        layoutParams5.rightMargin = 4;
        layoutParams5.gravity = 16;
        textView2.setLayoutParams(layoutParams2);
        textView2.setTextColor(-8289919);
        textView2.setTextSize(aa.i);
        linearLayout.addView(textView2);
        this.f291c = new EditText(d.getContext());
        this.f291c.setLayoutParams(new LinearLayout.LayoutParams(0, -2, 1.0f));
        this.f291c.setOnKeyListener(this.f280a);
        this.f291c.setOnFocusChangeListener(this.f279a);
        this.f291c.setOnTouchListener(this.f281a);
        this.f291c.setFocusableInTouchMode(true);
        this.f291c.setBackgroundDrawable(this.u);
        this.f291c.setId(0);
        this.f291c.setHint("验证码答案");
        this.f291c.setInputType(0);
        linearLayout.addView(this.f291c);
        TextView textView3 = new TextView(d.getContext());
        textView3.setLayoutParams(new LinearLayout.LayoutParams(0, -2, 1.0f));
        linearLayout.addView(textView3);
        LinearLayout linearLayout2 = new LinearLayout(d.getContext());
        LinearLayout.LayoutParams layoutParams6 = new LinearLayout.LayoutParams(-1, -2);
        linearLayout2.setPadding(10, 5, 10, 5);
        linearLayout2.setLayoutParams(layoutParams6);
        return linearLayout;
    }

    /* renamed from: y  reason: collision with other method in class */
    public String m305y() {
        return this.f291c == null ? PoiTypeDef.All : this.f291c.getText().toString();
    }
}
