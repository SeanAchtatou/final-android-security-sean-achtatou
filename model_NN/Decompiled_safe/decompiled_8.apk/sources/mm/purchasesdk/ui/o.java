package mm.purchasesdk.ui;

import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import com.amap.mapapi.poisearch.PoiTypeDef;

class o implements View.OnKeyListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ l f3165a;

    o(l lVar) {
        this.f3165a = lVar;
    }

    public boolean onKey(View view, int i, KeyEvent keyEvent) {
        if (i != 28) {
            return false;
        }
        ((EditText) view).setText(PoiTypeDef.All);
        return false;
    }
}
