package mm.purchasesdk.ui;

import android.app.Activity;
import android.os.Message;
import android.view.View;
import mm.purchasesdk.l.d;
import mm.purchasesdk.l.e;

class ad implements View.OnClickListener {
    final /* synthetic */ ac b;

    ad(ac acVar) {
        this.b = acVar;
    }

    public void onClick(View view) {
        if (((Activity) d.getContext()).isFinishing()) {
            e.e("WebViewLayout", "Activity is finished!");
            return;
        }
        u.a().s();
        int id = view.getId();
        Message obtainMessage = ac.a(this.b).obtainMessage();
        switch (id) {
            case 1:
                e.c("WebViewLayout", "onClick KAlipayBackButtonType");
                ac.a(this.b).b(1);
                break;
            case 2:
                e.c("WebViewLayout", "onClick KAlipayFinishButtonType");
                ac.a(this.b).b(2);
                break;
        }
        ac.a(this.b).a(ac.a(this.b));
        ac.a(this.b).a(ac.a(this.b));
        d.e((Boolean) true);
        obtainMessage.what = 2;
        obtainMessage.arg1 = 2;
        obtainMessage.obj = ac.a(this.b);
        obtainMessage.sendToTarget();
    }
}
