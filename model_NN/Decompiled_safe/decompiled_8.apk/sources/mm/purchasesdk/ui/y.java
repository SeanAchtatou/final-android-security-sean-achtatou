package mm.purchasesdk.ui;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Handler;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;

public class y extends b {
    private Drawable B = null;
    private Drawable C = null;
    int G;
    int H;

    /* renamed from: a  reason: collision with root package name */
    SensorManager f3173a;

    /* renamed from: a  reason: collision with other field name */
    Display f311a;
    boolean k;
    private Context mContext;

    class a implements Runnable {
        a() {
        }

        public void run() {
            y.this.dismiss();
        }
    }

    public y(Context context) {
        super(context, 16973829);
        this.mContext = context;
        setOwnerActivity((Activity) this.mContext);
        this.G = 0;
        this.H = 0;
        getWindow().requestFeature(1);
        this.f311a = ((WindowManager) this.mContext.getSystemService("window")).getDefaultDisplay();
        this.f3173a = (SensorManager) context.getSystemService("sensor");
        Sensor defaultSensor = this.f3173a.getDefaultSensor(1);
        this.f3173a.registerListener(new z(this), defaultSensor, 1);
    }

    private void d() {
        Bitmap a2;
        Bitmap a3;
        if (this.B == null && (a3 = aa.a(this.mContext, "mmiap/image/vertical/splash_v.jpg")) != null) {
            this.B = new BitmapDrawable(a3);
        }
        if (this.C == null && (a2 = aa.a(this.mContext, "mmiap/image/vertical/splash_h.jpg")) != null) {
            this.C = new BitmapDrawable(a2);
        }
    }

    private View n() {
        this.k = aa.f265d.booleanValue();
        return this.k ? p() : o();
    }

    private View o() {
        LinearLayout linearLayout = new LinearLayout(this.mContext);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -1);
        new LinearLayout.LayoutParams(-1, -1);
        linearLayout.setOrientation(1);
        linearLayout.setLayoutParams(layoutParams);
        linearLayout.setBackgroundDrawable(this.B);
        return linearLayout;
    }

    /* access modifiers changed from: package-private */
    public void f(boolean z) {
        aa.f265d = Boolean.valueOf(z);
        setContentView(n());
        this.G = 0;
    }

    public View p() {
        LinearLayout linearLayout = new LinearLayout(this.mContext);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -1);
        linearLayout.setOrientation(1);
        linearLayout.setLayoutParams(layoutParams);
        linearLayout.setBackgroundDrawable(this.C);
        return linearLayout;
    }

    public void show() {
        d();
        setContentView(n());
        setCancelable(false);
        new Handler().postDelayed(new a(), 3000);
        super.show();
    }
}
