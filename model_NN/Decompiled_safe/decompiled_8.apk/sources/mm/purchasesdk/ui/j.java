package mm.purchasesdk.ui;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import mm.purchasesdk.l.d;

public class j extends Dialog {
    private int D = 1003;

    /* renamed from: a  reason: collision with root package name */
    private k f3161a;
    private LinearLayout b;

    public j(Context context) {
        super(context, 16973829);
        setOwnerActivity((Activity) context);
        getWindow().requestFeature(1);
        WindowManager.LayoutParams attributes = getWindow().getAttributes();
        attributes.alpha = 0.8f;
        getWindow().setAttributes(attributes);
    }

    private View k() {
        this.b = new LinearLayout(d.getContext());
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -1);
        this.b.setOrientation(1);
        this.b.setLayoutParams(layoutParams);
        this.f3161a = new k();
        this.b.addView(this.f3161a.l());
        return this.b;
    }

    public void dismiss() {
        if (getOwnerActivity() == d.getContext()) {
            super.dismiss();
        }
    }

    public void show() {
        setContentView(k());
        setCancelable(false);
        super.show();
    }
}
