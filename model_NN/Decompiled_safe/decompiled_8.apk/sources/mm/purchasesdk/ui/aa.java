package mm.purchasesdk.ui;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import mm.purchasesdk.PurchaseCode;
import mm.purchasesdk.l.d;
import mm.purchasesdk.l.e;

public class aa {
    public static int I = 20;
    public static int J = 80;
    public static int K = 55;
    public static int L = 480;
    public static int M = PurchaseCode.AUTH_CERT_LIMIT;
    public static int N = 20;
    public static int O = 5;
    public static int P = 5;
    public static int Q = 15;
    public static int R = 10;
    public static int S;
    public static int T = 20;
    public static int U = 10;
    public static int V = 10;

    /* renamed from: a  reason: collision with root package name */
    public static float f3151a = 40.0f;
    public static float b = 80.0f;

    /* renamed from: b  reason: collision with other field name */
    private static ArrayList f264b = new ArrayList();
    public static float c = 1.0f;
    public static float d = 1.0f;

    /* renamed from: d  reason: collision with other field name */
    public static Boolean f265d = false;

    /* renamed from: d  reason: collision with other field name */
    private static HashMap f266d = new HashMap();
    public static float e = 1.0f;
    public static float f = 1.0f;
    public static float g = 16.0f;
    public static float h = 16.0f;
    public static float i = 18.0f;
    public static float j = 20.0f;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public static Bitmap a(float f2, float f3, Bitmap bitmap) {
        Matrix matrix = new Matrix();
        int height = bitmap.getHeight();
        int width = bitmap.getWidth();
        if (height > 1 && width > 1) {
            matrix.postScale(f2, f3);
            return Bitmap.createBitmap(bitmap, 0, 0, width, height, matrix, true);
        } else if (height <= 1) {
            matrix.postScale(f2, 1.0f);
            return Bitmap.createBitmap(bitmap, 0, 0, width, height, matrix, true);
        } else if (width > 1) {
            return bitmap;
        } else {
            matrix.postScale(1.0f, f3);
            return Bitmap.createBitmap(bitmap, 0, 0, width, height, matrix, true);
        }
    }

    public static Bitmap a(Context context, String str) {
        return b(context, str);
    }

    public static Bitmap a(Bitmap bitmap) {
        float f2;
        if (d.k > 1.0f) {
            f2 = d;
        } else if (d.k >= 1.0f) {
            return bitmap;
        } else {
            f2 = d;
        }
        return a(f2, f2, bitmap);
    }

    public static Bitmap b(Context context, String str) {
        if (f266d == null) {
            f266d = new HashMap();
        }
        Bitmap bitmap = (Bitmap) f266d.get(str);
        if (bitmap != null) {
            return bitmap;
        }
        try {
            Bitmap decodeStream = BitmapFactory.decodeStream(context.getAssets().open(str));
            if (m301b(str)) {
                Bitmap a2 = a(decodeStream);
                if (a2 != decodeStream) {
                    decodeStream.recycle();
                }
                f266d.put(str, a2);
                return a2;
            }
            f266d.put(str, decodeStream);
            return decodeStream;
        } catch (IOException e2) {
            e.e("ReadImageFile", "read image fail.......");
            e2.printStackTrace();
            return null;
        }
    }

    public static Bitmap b(String str) {
        return c(str);
    }

    /* renamed from: b  reason: collision with other method in class */
    private static boolean m301b(String str) {
        return f264b.contains(str);
    }

    public static Bitmap c(String str) {
        Bitmap bitmap = (Bitmap) f266d.get(str);
        if (bitmap != null) {
            return bitmap;
        }
        Bitmap bitmap2 = str.equals("mmiap/image/vertical/logo2.png") ? null : bitmap;
        if (bitmap2 == null) {
            return null;
        }
        if (m301b(str)) {
            Bitmap a2 = a(bitmap2);
            if (a2 != bitmap2) {
                bitmap2.recycle();
            }
            f266d.put(str, a2);
            return a2;
        }
        f266d.put(str, bitmap2);
        return bitmap2;
    }

    public static void w() {
        if (f266d != null) {
            for (Map.Entry entry : f266d.entrySet()) {
                Bitmap bitmap = (Bitmap) entry.getValue();
                if (bitmap != null && !bitmap.isRecycled()) {
                    e.c("UIConfig", "bitmap=" + entry.getKey());
                    bitmap.recycle();
                }
            }
            f266d.clear();
            f266d = null;
        }
    }

    public static void x() {
        if (f264b.size() != 0) {
            f264b.clear();
        }
        f264b.add("mmiap/image/vertical/logo1.png");
        f264b.add("mmiap/image/vertical/title1_bg.png");
        f264b.add("mmiap/image/vertical/title2_bg.png");
        f264b.add("mmiap/image/vertical/button_back.png");
        f264b.add("mmiap/image/vertical/icon_success.png");
        f264b.add("mmiap/image/vertical/icon_false.png");
        f264b.add("mmiap/image/vertical/icon_info.png");
        f264b.add("mmiap/image/vertical/logo3.png");
        f264b.add("mmiap/image/vertical/infoline.png");
        f264b.add("mmiap/image/vertical/logo2.png");
        f264b.add("mmiap/image/vertical/icon_chifubao.png");
        f264b.add("mmiap/image/vertical/button_back.png");
        f264b.add("mmiap/image/vertical/button_back_Press.png");
        f264b.add("mmiap/image/vertical/button_finishbilling.png");
        f264b.add("mmiap/image/vertical/button_finishbilling_press.png");
        f264b.add("mmiap/image/vertical/top_button_back.png");
        f264b.add("mmiap/image/vertical/top_button_back_press.png");
    }
}
