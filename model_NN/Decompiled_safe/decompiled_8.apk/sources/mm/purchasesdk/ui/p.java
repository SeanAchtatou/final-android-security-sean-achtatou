package mm.purchasesdk.ui;

import android.app.Activity;
import android.os.Message;
import android.view.View;
import java.util.HashMap;
import mm.purchasesdk.PurchaseCode;
import mm.purchasesdk.l.d;
import mm.purchasesdk.l.e;

class p implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ l f3166a;

    p(l lVar) {
        this.f3166a = lVar;
    }

    public void onClick(View view) {
        if (((Activity) d.getContext()).isFinishing()) {
            e.e("PurchaseDialog", "Activity is finished!");
            return;
        }
        this.f3166a.dismiss();
        l.a(this.f3166a).a((int) PurchaseCode.BILL_CANCEL_FAIL);
        l.a(this.f3166a).a((HashMap) null);
        Message obtainMessage = l.a(this.f3166a).obtainMessage();
        obtainMessage.what = 5;
        obtainMessage.obj = l.a(this.f3166a);
        obtainMessage.sendToTarget();
    }
}
