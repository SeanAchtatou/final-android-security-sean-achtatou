package mm.purchasesdk.ui;

import android.graphics.Bitmap;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import mm.purchasesdk.l.d;
import mm.purchasesdk.l.e;

class ae extends WebViewClient {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ RelativeLayout.LayoutParams f3153a;
    final /* synthetic */ ac b;

    ae(ac acVar, RelativeLayout.LayoutParams layoutParams) {
        this.b = acVar;
        this.f3153a = layoutParams;
    }

    public void onPageFinished(WebView webView, String str) {
        e.c("WebViewLayout", "Finished loading URL: " + str);
        ac.a(this.b).removeView(ac.a(this.b));
        ProgressBar unused = this.b.f272b = null;
    }

    public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
        if (ac.a(this.b) == null) {
            ProgressBar unused = this.b.f272b = new ProgressBar(d.getContext());
            ac.a(this.b).addView(ac.a(this.b), this.f3153a);
        }
    }

    public boolean shouldOverrideUrlLoading(WebView webView, String str) {
        ac.a(this.b).loadUrl(str);
        return true;
    }
}
