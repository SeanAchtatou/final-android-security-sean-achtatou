package mm.purchasesdk.ui;

import android.app.Activity;
import android.os.Bundle;
import android.os.Message;
import android.view.View;
import android.widget.Toast;
import com.amap.mapapi.poisearch.PoiTypeDef;
import mm.purchasesdk.PurchaseCode;
import mm.purchasesdk.f.a;
import mm.purchasesdk.fingerprint.c;
import mm.purchasesdk.l.d;
import mm.purchasesdk.l.e;

class q implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ l f3167a;

    q(l lVar) {
        this.f3167a = lVar;
    }

    public void onClick(View view) {
        a.k();
        a.z = 3;
        if (l.a(this.f3167a).m271a().booleanValue()) {
            l.b(this.f3167a).setText(l.b(this.f3167a).getText().toString().trim());
            if (l.b(this.f3167a) != null && l.b(this.f3167a).getText().toString().length() == 0) {
                Toast.makeText(d.getContext(), "请输入验证码", 0).show();
                return;
            }
        }
        if (!l.a(this.f3167a).m272b() || l.a(this.f3167a) == null || !l.a(this.f3167a).isShown() || l.a(this.f3167a).getText().toString().length() != 0) {
            String str = PoiTypeDef.All;
            if (c.e.booleanValue()) {
                str = c.v();
                int status = c.getStatus();
                if (status == -6) {
                    Toast.makeText(d.getContext(), "指纹正在获取中,请稍后再试", 0).show();
                    return;
                } else if (status != 0) {
                    this.f3167a.dismiss();
                    e.e("PurchaseDialog", "failed to create fingerprint,error code=" + status);
                    l.a(this.f3167a).a(PurchaseCode.BILL_DYMARK_CREATE_ERROR, null);
                    return;
                }
            }
            String str2 = str;
            if (((Activity) d.getContext()).isFinishing()) {
                e.e("PurchaseDialog", "Activity is finished!");
                return;
            }
            if (l.a(this.f3167a) == 0) {
                e.e("PurchaseDialog", "getPayType = 0");
                d.P("1");
            } else if (1 == l.a(this.f3167a)) {
                e.e("PurchaseDialog", "getPayType = 1");
                d.P("3");
            } else if (2 == l.a(this.f3167a)) {
                e.e("PurchaseDialog", "getPayType = 2");
                d.P("1");
            }
            u.a().s();
            Bundle bundle = new Bundle();
            bundle.putString("dyMark", str2);
            bundle.putString("CheckAnswer", this.f3167a.m305y());
            bundle.putString("CheckId", l.a(this.f3167a).h());
            bundle.putString("Password", this.f3167a.getPassword());
            bundle.putString("SessionId", l.a(this.f3167a).g());
            bundle.putInt("OrderCount", l.a(this.f3167a).a());
            bundle.putBoolean("multiSubs", l.a(this.f3167a).m273c());
            bundle.putBoolean("NeedPasswd", l.a(this.f3167a).m272b());
            bundle.putBoolean("NeedInput", l.a(this.f3167a).m271a().booleanValue());
            bundle.putString("RandomPwd", this.f3167a.m());
            Message obtainMessage = l.b(this.f3167a).obtainMessage();
            obtainMessage.what = 2;
            obtainMessage.obj = l.a(this.f3167a);
            obtainMessage.arg1 = 1;
            obtainMessage.setData(bundle);
            obtainMessage.sendToTarget();
            return;
        }
        Toast.makeText(d.getContext(), "请输入支付密码", 0).show();
    }
}
