package mm.purchasesdk.g;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import mm.purchasesdk.l.e;

public class a extends SQLiteOpenHelper {
    public a(Context context) {
        super(context, "license_data.db", (SQLiteDatabase.CursorFactory) null, 3);
    }

    public void onCreate(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL("create table copyright (_id integer primary key autoincrement, appid text not null, imsi text not null, content text not null, apksignature text not null)");
        sQLiteDatabase.execSQL("create table auth (_id integer primary key autoincrement, appid text not null, paycode text not null, imsi text not null, notBefore INTEGER, notAfter INTEGER not null, orderId text,content text not null)");
    }

    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        e.d("DatabaseHelper", "Upgrading database from version " + i + " to " + i2 + ", which will destroy all old data");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS copyright");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS auth");
        onCreate(sQLiteDatabase);
    }
}
