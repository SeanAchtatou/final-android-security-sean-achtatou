package mm.purchasesdk.g;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import java.util.Calendar;
import mm.purchasesdk.PurchaseCode;
import mm.purchasesdk.fingerprint.IdentifyApp;
import mm.purchasesdk.h.e;
import mm.purchasesdk.h.f;
import mm.purchasesdk.l.d;

public class b implements c {
    private static final String TAG = b.class.getSimpleName();

    /* renamed from: a  reason: collision with root package name */
    protected SQLiteDatabase f3145a;

    /* renamed from: a  reason: collision with other field name */
    protected a f259a;

    public String a(e eVar, f fVar) {
        return null;
    }

    public String a(f fVar) {
        String c = c("content");
        String c2 = c("orderId");
        mm.purchasesdk.l.e.c(TAG, "content =" + c + " orderID=" + c2);
        if (c == null || mm.purchasesdk.a.e.a(c, mm.purchasesdk.e.b.a().f3140a.ab, c2) != 104) {
            return null;
        }
        return c2;
    }

    /* access modifiers changed from: protected */
    public void a(Context context) {
        this.f259a = new a(context);
        this.f3145a = this.f259a.getWritableDatabase();
    }

    public void a(String str, String str2, String str3) {
        open();
        this.f3145a.delete("auth", "appID = ? and paycode = ? and imsi = ?", new String[]{str, str2, str3});
        close();
    }

    public void a(String str, String str2, String str3, long j, long j2, String str4, String str5) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("imsi", str5);
        contentValues.put("appid", str);
        contentValues.put("paycode", str2);
        contentValues.put("notBefore", Long.valueOf(j));
        contentValues.put("notAfter", Long.valueOf(j2));
        contentValues.put("orderId", str4);
        contentValues.put("content", str3);
        open();
        this.f3145a.delete("auth", "appID = ? and paycode = ? and imsi = ?", new String[]{str, str2, str5});
        this.f3145a.insert("auth", null, contentValues);
        close();
    }

    /* renamed from: a  reason: collision with other method in class */
    public boolean m293a(e eVar, f fVar) {
        String c = c("content");
        String c2 = c("orderId");
        if (c != null) {
            int a2 = mm.purchasesdk.a.e.a(c, mm.purchasesdk.e.b.a().f3140a.ab, c2);
            PurchaseCode.setStatusCode(a2);
            if (a2 == 104) {
                fVar.p(c2);
                return true;
            }
        }
        return false;
    }

    public String b(e eVar, f fVar) {
        return null;
    }

    public void b(Context context) {
        a(context);
        this.f3145a.execSQL("delete from auth");
        close();
    }

    public void b(String str, String str2, String str3) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("imsi", str3);
        contentValues.put("appid", str);
        contentValues.put("content", str2);
        contentValues.put("apksignature", IdentifyApp.getSignature());
        open();
        this.f3145a.delete("copyright", "appID = ? and imsi = ?", new String[]{str, str3});
        this.f3145a.insert("copyright", null, contentValues);
        close();
    }

    public String c(String str) {
        open();
        String valueOf = String.valueOf(Calendar.getInstance().getTimeInMillis());
        Cursor query = this.f3145a.query("auth", new String[]{str}, "appid=? and paycode=? and imsi=? and notAfter>?", new String[]{d.F(), d.H(), d.L(), valueOf}, null, null, null);
        query.moveToFirst();
        if (query.getCount() == 0) {
            query.close();
            close();
            return null;
        }
        String string = query.getString(query.getColumnIndex(str));
        query.close();
        close();
        return string;
    }

    /* access modifiers changed from: protected */
    public void close() {
        this.f259a.close();
    }

    public void n() {
        open();
        this.f3145a.execSQL("delete from copyright");
        close();
    }

    /* access modifiers changed from: protected */
    public void open() {
        this.f259a = new a(d.getContext());
        this.f3145a = this.f259a.getWritableDatabase();
    }

    public String s() {
        open();
        Cursor query = this.f3145a.query("copyright", new String[]{"content", "apksignature"}, "appid=? and imsi=?", new String[]{d.F(), d.L()}, null, null, null);
        query.moveToFirst();
        if (query.getCount() == 0) {
            query.close();
            close();
            return null;
        }
        String string = query.getString(query.getColumnIndex("apksignature"));
        mm.purchasesdk.l.e.c(TAG, "database signature=" + string);
        if (IdentifyApp.checkSignature(string) != 0) {
            mm.purchasesdk.l.e.c(TAG, "signature is error!");
            query.close();
            close();
            n();
            return null;
        }
        String string2 = query.getString(query.getColumnIndex("content"));
        query.close();
        close();
        return string2;
    }
}
