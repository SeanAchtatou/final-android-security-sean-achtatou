package mm.purchasesdk.g;

import android.content.Context;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import mm.purchasesdk.h.f;
import mm.purchasesdk.l.e;

public class d implements c {
    private static final String TAG = d.class.getSimpleName();

    public static String a(String str, Context context) {
        InputStream resourceAsStream = context.getClass().getClassLoader().getResourceAsStream(str);
        if (resourceAsStream == null) {
            e.e(TAG, "failed to find resource file(" + str + "}");
            return null;
        }
        StringBuilder sb = new StringBuilder();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(resourceAsStream));
        while (bufferedReader.ready()) {
            try {
                sb.append(bufferedReader.readLine());
            } catch (IOException e) {
                e.e(TAG, "failed to read resource file(" + str + ")");
                return null;
            }
        }
        bufferedReader.close();
        e.c(TAG, "file content->" + sb.toString());
        return sb.toString();
    }

    public static String b(Context context) {
        return a("VERSION", context);
    }

    public static String t() {
        return a("mmiap.xml", mm.purchasesdk.l.d.getContext());
    }

    public String a(mm.purchasesdk.h.e eVar, f fVar) {
        return null;
    }

    public String a(f fVar) {
        return null;
    }

    /* renamed from: a  reason: collision with other method in class */
    public boolean m295a(mm.purchasesdk.h.e eVar, f fVar) {
        return false;
    }

    public String b(mm.purchasesdk.h.e eVar, f fVar) {
        return null;
    }

    public String s() {
        return a("CopyrightDeclaration.xml", mm.purchasesdk.l.d.getContext());
    }
}
