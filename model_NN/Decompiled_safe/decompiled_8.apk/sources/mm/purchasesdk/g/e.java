package mm.purchasesdk.g;

import android.content.Context;
import com.ccit.mmwlan.MMClientSDK_ForPad;
import com.ccit.mmwlan.phone.MMClientSDK_ForPhone;
import com.ccit.mmwlan.vo.SignView;
import com.immomo.momo.h;
import java.io.UnsupportedEncodingException;
import mm.purchasesdk.PurchaseCode;
import mm.purchasesdk.e.b;
import mm.purchasesdk.h.c;
import mm.purchasesdk.h.f;
import mm.purchasesdk.l.a;
import mm.purchasesdk.l.d;
import mm.purchasesdk.l.g;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.util.EntityUtils;

public class e implements c {
    private static final String TAG = e.class.getSimpleName();

    public static String a(mm.purchasesdk.h.e eVar, String str, f fVar) {
        HttpClient a2 = g.a(d.getContext());
        if (a2 == null) {
            PurchaseCode.setStatusCode(PurchaseCode.NONE_NETWORK);
            throw new mm.purchasesdk.e(PurchaseCode.NONE_NETWORK);
        }
        HttpPost httpPost = new HttpPost(str);
        mm.purchasesdk.l.e.c(TAG, "url=" + str);
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < 3) {
                if (d.d()) {
                    try {
                        SignView SIDSign = MMClientSDK_ForPhone.SIDSign(d.F(), null);
                        if (SIDSign.getResult() != 0) {
                            MMClientSDK_ForPhone.DestorySecCert(null);
                            mm.purchasesdk.l.e.e(TAG, "mobile failed to make sidSignature.code=" + SIDSign.getResult());
                            PurchaseCode.setStatusCode(PurchaseCode.INVALID_SIDSIGN_ERR);
                            return null;
                        }
                        eVar.b(SIDSign);
                    } catch (Exception e) {
                        mm.purchasesdk.l.e.e(TAG, "mobile failed to make sidSignature.code=118");
                        PurchaseCode.setStatusCode(PurchaseCode.INVALID_SIDSIGN_ERR);
                        return null;
                    }
                } else {
                    SignView sidSign_PAD = MMClientSDK_ForPad.sidSign_PAD(d.F(), null, null, null);
                    if (sidSign_PAD.getResult() != 0) {
                        try {
                            MMClientSDK_ForPad.DestorySecCert(null);
                        } catch (Exception e2) {
                            e2.printStackTrace();
                        }
                        mm.purchasesdk.l.e.e(TAG, "pad failed to make sidSignature.code=" + sidSign_PAD.getResult());
                        PurchaseCode.setStatusCode(PurchaseCode.INVALID_SIDSIGN_ERR);
                        return null;
                    }
                    eVar.b(sidSign_PAD);
                }
                try {
                    String a3 = eVar.m297a();
                    if (a3 == null) {
                        mm.purchasesdk.l.e.e(TAG, "failed to make query request.code=");
                        return null;
                    }
                    mm.purchasesdk.l.e.c(TAG, "request : code = " + a3);
                    try {
                        StringEntity stringEntity = new StringEntity(a3);
                        mm.purchasesdk.l.e.c(TAG, "request : code = " + a3);
                        httpPost.setEntity(stringEntity);
                        try {
                            long currentTimeMillis = System.currentTimeMillis();
                            HttpResponse execute = a2.execute(httpPost);
                            mm.purchasesdk.l.e.c(TAG, "Req/Resp Time: " + (System.currentTimeMillis() - currentTimeMillis));
                            if (execute.getStatusLine().getStatusCode() == 200) {
                                String entityUtils = EntityUtils.toString(execute.getEntity(), "utf-8");
                                mm.purchasesdk.l.e.c(TAG, entityUtils);
                                if (fVar.parse(entityUtils)) {
                                    return entityUtils;
                                }
                                PurchaseCode.setStatusCode(PurchaseCode.PROTOCOL_ERR);
                                return null;
                            }
                            continue;
                            i = i2 + 1;
                        } catch (Exception e3) {
                            mm.purchasesdk.l.e.a(TAG, "network failed", e3);
                        }
                    } catch (UnsupportedEncodingException e4) {
                        e4.printStackTrace();
                        return null;
                    }
                } catch (Exception e5) {
                    mm.purchasesdk.l.e.a(TAG, "failed to make query request.exception.code=", e5);
                    return null;
                }
            } else {
                mm.purchasesdk.l.e.e(TAG, "http response network timeout");
                throw new mm.purchasesdk.e(PurchaseCode.NETWORKTIMEOUT_ERR);
            }
        }
    }

    public static boolean a(String str, String str2, f fVar) {
        int i = 0;
        HttpClient a2 = g.a(d.getContext());
        if (a2 == null) {
            PurchaseCode.setStatusCode(PurchaseCode.NETWORKTIMEOUT_ERR);
            throw new mm.purchasesdk.e(PurchaseCode.NETWORKTIMEOUT_ERR);
        }
        HttpPost httpPost = new HttpPost(str2);
        try {
            httpPost.setEntity(new StringEntity(str));
            mm.purchasesdk.l.e.c(TAG, str);
            while (i < 3) {
                try {
                    long currentTimeMillis = System.currentTimeMillis();
                    HttpResponse execute = a2.execute(httpPost);
                    mm.purchasesdk.l.e.c(TAG, "Req/Resp Time: " + (System.currentTimeMillis() - currentTimeMillis));
                    if (execute.getStatusLine().getStatusCode() == 200) {
                        String entityUtils = EntityUtils.toString(execute.getEntity(), "utf-8");
                        mm.purchasesdk.l.e.c(TAG, entityUtils);
                        if (fVar.parse(entityUtils)) {
                            PurchaseCode.setStatusCode(0);
                            return true;
                        }
                        PurchaseCode.setStatusCode(PurchaseCode.COPYRIGHT_PROTOCOL_ERR);
                        throw new mm.purchasesdk.e(PurchaseCode.COPYRIGHT_PROTOCOL_ERR);
                    }
                    i++;
                } catch (Exception e) {
                    PurchaseCode.setStatusCode(PurchaseCode.NETWORKTIMEOUT_ERR);
                    throw new mm.purchasesdk.e(PurchaseCode.NETWORKTIMEOUT_ERR);
                }
            }
            PurchaseCode.setStatusCode(PurchaseCode.NETWORKTIMEOUT_ERR);
            mm.purchasesdk.l.e.e(TAG, "http response network timeout");
            throw new mm.purchasesdk.e(PurchaseCode.NETWORKTIMEOUT_ERR);
        } catch (UnsupportedEncodingException e2) {
            PurchaseCode.setStatusCode(PurchaseCode.UNSUPPORT_ENCODING_ERR);
            e2.printStackTrace();
            throw new mm.purchasesdk.e(PurchaseCode.UNSUPPORT_ENCODING_ERR);
        }
    }

    public String a(mm.purchasesdk.h.e eVar, f fVar) {
        return a(eVar, d.d(d.getContext()), fVar);
    }

    public String a(f fVar) {
        int i = PurchaseCode.QUERY_OK;
        mm.purchasesdk.h.d dVar = (mm.purchasesdk.h.d) fVar;
        String a2 = a(new c(), d.c(d.getContext()), dVar);
        if (a2 != null) {
            mm.purchasesdk.l.e.c(TAG, "queryOrderId retcode=" + dVar.w());
            int intValue = Integer.valueOf(dVar.w()).intValue();
            if ((intValue == 0 || intValue == 100) && !a.a(a2).booleanValue()) {
                PurchaseCode.setStatusCode(PurchaseCode.RESPONSE_ERR);
                return null;
            }
            switch (intValue) {
                case 0:
                    String b = mm.purchasesdk.a.e.b(a2);
                    if (b != null && b.length() != 0) {
                        if (!d.h().booleanValue()) {
                            int a3 = mm.purchasesdk.a.e.a(b, b.a().f3140a.ab, dVar.k());
                            PurchaseCode.setStatusCode(a3);
                            if (a3 != 104) {
                                i = a3;
                                break;
                            }
                        }
                    } else {
                        PurchaseCode.setStatusCode(PurchaseCode.AUTH_PARSE_FAIL);
                        mm.purchasesdk.l.e.e(TAG, "auth file is null,code=241");
                        i = 241;
                        break;
                    }
                    break;
                case 1:
                    i = PurchaseCode.QUERY_FROZEN;
                    break;
                case 2:
                    i = PurchaseCode.QUERY_NOT_FOUND;
                    break;
                case 3:
                case 4:
                case 6:
                case 7:
                case 8:
                case 9:
                case 10:
                default:
                    i = intValue;
                    break;
                case 5:
                    i = PurchaseCode.QUERY_PAYCODE_ERROR;
                    break;
                case 11:
                    i = PurchaseCode.QUERY_NO_AUTHORIZATION;
                    break;
                case 12:
                    i = PurchaseCode.QUERY_CSSP_BUSY;
                    break;
                case 13:
                    i = PurchaseCode.QUERY_OTHER_ERROR;
                    break;
                case h.DragSortListView_drag_handle_id:
                    try {
                        if (d.d()) {
                            MMClientSDK_ForPhone.DestorySecCert(null);
                        } else {
                            MMClientSDK_ForPad.DestorySecCert(null);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    i = PurchaseCode.QUERY_INVALID_USER;
                    break;
                case 15:
                    i = PurchaseCode.QUERY_INVALID_APP;
                    break;
                case 16:
                    i = PurchaseCode.QUERY_LICENSE_ERROR;
                    break;
                case 17:
                    i = PurchaseCode.QUERY_INVALID_SIGN;
                    break;
                case 18:
                    i = PurchaseCode.QUERY_NO_ABILITY;
                    break;
                case 19:
                    i = PurchaseCode.QUERY_NO_APP;
                    break;
            }
            PurchaseCode.setStatusCode(i);
            d.U(dVar.k());
            d.b(dVar.b());
            d.T(dVar.e());
            return dVar.k();
        }
        mm.purchasesdk.l.e.e(TAG, "get getOrderId failed: " + dVar.w());
        return null;
    }

    /* renamed from: a  reason: collision with other method in class */
    public boolean m296a(mm.purchasesdk.h.e eVar, f fVar) {
        String a2 = a(eVar, d.d(d.getContext()), fVar);
        if (a2 == null || a2.length() <= 0) {
            return false;
        }
        int intValue = Integer.valueOf(fVar.w()).intValue();
        if ((intValue == 0 || intValue == 2 || intValue == 6 || intValue == 100) && !a.a(a2).booleanValue()) {
            PurchaseCode.setStatusCode(PurchaseCode.RESPONSE_ERR);
            return false;
        }
        mm.purchasesdk.l.e.c(TAG, "checkAuth return code =" + intValue);
        if (intValue == 0) {
            String b = mm.purchasesdk.a.e.b(a2);
            if (b == null || b.length() == 0) {
                PurchaseCode.setStatusCode(PurchaseCode.AUTH_PARSE_FAIL);
                mm.purchasesdk.l.e.e(TAG, "auth file is null,code=241");
                return false;
            }
            int a3 = mm.purchasesdk.a.e.a(b, b.a().f3140a.ab, fVar.k());
            PurchaseCode.setStatusCode(a3);
            return a3 == 104;
        }
        if (intValue == 2) {
            intValue = PurchaseCode.AUTH_NOT_FOUND;
        } else if (intValue == 1) {
            intValue = PurchaseCode.AUTH_FROZEN;
        } else if (intValue == 4) {
            intValue = PurchaseCode.AUTH_NOT_DOWNLOAD;
        } else if (intValue == 3) {
            intValue = PurchaseCode.AUTH_FORBIDDEN;
        } else if (intValue == 5) {
            intValue = PurchaseCode.AUTH_PAYCODE_ERROR;
        } else if (intValue == 6) {
            intValue = PurchaseCode.AUTH_NO_PICODE;
        } else if (intValue == 37) {
            intValue = PurchaseCode.AUTH_INVALID_ORDERCOUNT;
        } else if (intValue == 11) {
            intValue = PurchaseCode.AUTH_NO_AUTHORIZATION;
        } else if (intValue == 12) {
            intValue = PurchaseCode.AUTH_CSSP_BUSY;
        } else if (intValue == 13) {
            intValue = PurchaseCode.AUTH_OTHER_ERROR;
        } else if (intValue == 14) {
            try {
                if (d.d()) {
                    MMClientSDK_ForPhone.DestorySecCert(null);
                } else {
                    MMClientSDK_ForPad.DestorySecCert(null);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            intValue = PurchaseCode.AUTH_INVALID_USER;
        } else if (intValue == 15) {
            intValue = PurchaseCode.AUTH_INVALID_APP;
        } else if (intValue == 16) {
            intValue = 256;
        } else if (intValue == 17) {
            intValue = PurchaseCode.AUTH_INVALID_SIGN;
        } else if (intValue == 18) {
            intValue = PurchaseCode.AUTH_NO_ABILITY;
        } else if (intValue == 19) {
            intValue = PurchaseCode.AUTH_NO_APP;
        } else if (intValue == 100) {
            intValue = PurchaseCode.AUTH_TIME_LIMIT;
        } else if (intValue == 101) {
            intValue = PurchaseCode.AUTH_NO_BUSINESS;
        } else if (intValue == 99) {
            intValue = PurchaseCode.AUTH_FORBID_ORDER;
        } else if (intValue == 39) {
            intValue = PurchaseCode.AUTH_STATICMARK_DECRY_FAILED;
        } else if (intValue == 40) {
            intValue = PurchaseCode.AUTH_STATICMARK_VERIFY_FAILED;
        } else if (intValue == 41) {
            intValue = PurchaseCode.AUTH_NO_DYQUESTION;
        } else if (intValue == 42) {
            intValue = PurchaseCode.AUTH_DYQUESTION_ENCRYPT_ERROR;
        } else if (intValue == 43) {
            intValue = PurchaseCode.AUTH_AP_CER_VERIFY_ERROR;
        } else if (intValue == 201) {
            intValue = PurchaseCode.AUTH_OVER_COMSUMPTION;
        } else if (intValue == 202) {
            intValue = PurchaseCode.AUTH_OVER_LIMIT;
        } else if (intValue == 401) {
            intValue = PurchaseCode.AUTH_TRADEID_ERROR;
        } else if (intValue == 9019) {
            intValue = PurchaseCode.AUTH_PARAM_ERROR;
        } else if (intValue == 182) {
            intValue = PurchaseCode.AUTH_USERINFO_CLOSE;
        } else if (intValue == 2008) {
            intValue = PurchaseCode.AUTH_INSUFFICIENT_FUNDS;
        } else if (intValue == 2009) {
            intValue = PurchaseCode.AUTH_LIMIT;
        } else if (intValue == 20) {
            intValue = PurchaseCode.AUTH_PWD_DISMISS;
        } else if (intValue == 22) {
            intValue = PurchaseCode.AUTH_CHECK_FAILED;
        } else if (intValue == 25) {
            intValue = PurchaseCode.AUTH_CERT_LIMIT;
        } else {
            mm.purchasesdk.l.e.e(TAG, "unknown error.code=" + intValue);
        }
        PurchaseCode.setStatusCode(intValue);
        return false;
    }

    public String b(mm.purchasesdk.h.e eVar, f fVar) {
        return a(eVar, d.c(d.getContext()), fVar);
    }

    public String c(mm.purchasesdk.h.e eVar, f fVar) {
        boolean z;
        Context context = d.getContext();
        HttpClient a2 = g.a(context);
        if (a2 == null) {
            fVar.C("短信验证码获取失败，请检查网络连接是否正常");
            return null;
        }
        HttpPost httpPost = new HttpPost(d.c(context));
        try {
            String gVar = ((mm.purchasesdk.h.g) eVar).toString();
            mm.purchasesdk.l.b.a(0, TAG, "Query request : code = " + gVar);
            try {
                StringEntity stringEntity = new StringEntity(gVar);
                mm.purchasesdk.l.b.a(0, TAG, gVar);
                httpPost.setEntity(stringEntity);
                int i = 0;
                String str = null;
                while (true) {
                    if (i >= 3) {
                        z = false;
                        break;
                    }
                    try {
                        long currentTimeMillis = System.currentTimeMillis();
                        HttpResponse execute = a2.execute(httpPost);
                        mm.purchasesdk.l.b.a(0, TAG, "SMSRequest Req/Resp Time: " + (System.currentTimeMillis() - currentTimeMillis));
                        if (execute.getStatusLine().getStatusCode() == 200) {
                            String entityUtils = EntityUtils.toString(execute.getEntity(), "utf-8");
                            try {
                                mm.purchasesdk.l.b.a(0, TAG, "response:" + entityUtils);
                                String str2 = entityUtils;
                                z = true;
                                str = str2;
                                break;
                            } catch (Exception e) {
                                Exception exc = e;
                                str = entityUtils;
                                e = exc;
                            }
                        } else {
                            continue;
                            i++;
                        }
                    } catch (Exception e2) {
                        e = e2;
                        fVar.C("短信验证码获取失败，请检查网络连接是否正常");
                        mm.purchasesdk.l.b.a(2, TAG, "sms network failed.code=" + fVar.w(), e);
                        i++;
                    }
                }
                if (!z) {
                    fVar.C("短信验证码获取失败，请检查网络连接是否正常");
                    mm.purchasesdk.l.b.a(1, TAG, "SMS http response status code is " + fVar.w());
                    return null;
                } else if (str != null) {
                    return str;
                } else {
                    mm.purchasesdk.l.b.a(1, TAG, "cannot read authorization response");
                    fVar.C("短信验证码获取失败，请检查网络连接是否正常");
                    return null;
                }
            } catch (UnsupportedEncodingException e3) {
                e3.printStackTrace();
                fVar.C("短信验证码获取失败，系统出现错误");
                return null;
            }
        } catch (Exception e4) {
            mm.purchasesdk.l.b.a(2, TAG, "failed to make query request", e4);
            fVar.C("短信验证码获取失败，短信请求错误");
            return null;
        }
    }

    public String d(mm.purchasesdk.h.e eVar, f fVar) {
        String str;
        String c = c(eVar, fVar);
        if (c != null) {
            try {
                fVar = ((mm.purchasesdk.h.h) fVar).a(c.getBytes(), "utf-8");
                mm.purchasesdk.l.b.a(0, TAG, "SMS response: code = " + c);
                str = fVar.w();
            } catch (Exception e) {
                mm.purchasesdk.l.b.a(2, TAG, "parse failed", e);
                fVar.C("短信验证码获取失败，响应解析错误");
                return null;
            }
        } else {
            str = null;
        }
        return str;
    }

    public String s() {
        mm.purchasesdk.h.a aVar = new mm.purchasesdk.h.a();
        mm.purchasesdk.h.b bVar = new mm.purchasesdk.h.b();
        if (a(aVar.a(), d.O(), bVar)) {
            if (Integer.valueOf(bVar.ag).intValue() == 1) {
                return bVar.ai;
            }
            mm.purchasesdk.l.e.e(TAG, "get copyright failed: " + bVar.ah);
        }
        return null;
    }
}
