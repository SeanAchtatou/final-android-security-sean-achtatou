package mm.purchasesdk.k;

import com.ccit.mmwlan.MMClientSDK_ForPad;
import com.ccit.mmwlan.phone.MMClientSDK_ForPhone;
import com.immomo.momo.h;
import java.util.ArrayList;
import java.util.Iterator;
import mm.purchasesdk.PurchaseCode;
import mm.purchasesdk.g.b;
import mm.purchasesdk.g.c;
import mm.purchasesdk.g.e;
import mm.purchasesdk.l.d;

public class a {
    private final String TAG = a.class.getSimpleName();

    public int i() {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new e());
        b bVar = new b();
        c cVar = new c();
        Iterator it = arrayList.iterator();
        String str = null;
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            try {
                String b = ((c) it.next()).b(bVar, cVar);
                if (b != null) {
                    str = b;
                    break;
                }
                str = b;
            } catch (mm.purchasesdk.e e) {
                e.printStackTrace();
            }
        }
        int i = 0;
        if (str != null) {
            i = Integer.valueOf(cVar.w()).intValue();
            mm.purchasesdk.l.e.c(this.TAG, "retCode =" + i);
            switch (i) {
                case 0:
                    i = PurchaseCode.UNSUB_OK;
                    new b().a(d.F(), d.H(), d.L());
                    break;
                case 1:
                    i = PurchaseCode.UNSUB__FROZEN;
                    break;
                case 2:
                    i = PurchaseCode.UNSUB_NOT_FOUND;
                    break;
                case 5:
                    i = PurchaseCode.UNSUB_PAYCODE_ERROR;
                    break;
                case 11:
                    i = PurchaseCode.UNSUB_NO_AUTHORIZATION;
                    break;
                case 12:
                    i = PurchaseCode.UNSUB_CSSP_BUSY;
                    break;
                case 13:
                    i = PurchaseCode.UNSUB_INTERNAL_ERROR;
                    break;
                case h.DragSortListView_drag_handle_id:
                    try {
                        if (d.d()) {
                            MMClientSDK_ForPhone.DestorySecCert(null);
                        } else {
                            MMClientSDK_ForPad.DestorySecCert(null);
                        }
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                    i = PurchaseCode.UNSUB_INVALID_USER;
                    break;
                case 17:
                    i = PurchaseCode.UNSUB_LICENSE_ERROR;
                    break;
                case 18:
                    i = PurchaseCode.UNSUB_NO_ABILITY;
                    break;
                case 19:
                    i = PurchaseCode.UNSUB_NO_APP;
                    break;
            }
            PurchaseCode.setStatusCode(i);
        }
        return i;
    }
}
