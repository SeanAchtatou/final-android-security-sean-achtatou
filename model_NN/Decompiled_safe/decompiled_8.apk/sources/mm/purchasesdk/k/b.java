package mm.purchasesdk.k;

import android.util.Xml;
import com.amap.mapapi.poisearch.PoiTypeDef;
import java.io.StringWriter;
import java.util.Calendar;
import mm.purchasesdk.PurchaseCode;
import mm.purchasesdk.h.e;
import mm.purchasesdk.l.d;
import org.xmlpull.v1.XmlSerializer;

public class b extends e {
    private final String TAG = b.class.getSimpleName();

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v11, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v14, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v15, resolved type: byte} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.String a(java.lang.String r6) {
        /*
            r5 = this;
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r1 = mm.purchasesdk.l.d.F()
            r0.<init>(r1)
            java.lang.String r1 = "&"
            java.lang.StringBuilder r1 = r0.append(r1)
            java.lang.String r2 = mm.purchasesdk.l.d.H()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = "&"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = mm.purchasesdk.l.d.G()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = "&"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r6)
            java.lang.String r2 = "&"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = mm.purchasesdk.l.d.t()
            r1.append(r2)
            java.lang.String r0 = r0.toString()
            byte[] r2 = mm.purchasesdk.fingerprint.IdentifyApp.a(r0)
            if (r2 != 0) goto L_0x0048
            r0 = 0
        L_0x0047:
            return r0
        L_0x0048:
            java.lang.StringBuffer r3 = new java.lang.StringBuffer
            java.lang.String r0 = ""
            r3.<init>(r0)
            r0 = 0
        L_0x0050:
            int r1 = r2.length
            if (r0 >= r1) goto L_0x006c
            byte r1 = r2[r0]
            if (r1 >= 0) goto L_0x0059
            int r1 = r1 + 256
        L_0x0059:
            r4 = 16
            if (r1 >= r4) goto L_0x0062
            java.lang.String r4 = "0"
            r3.append(r4)
        L_0x0062:
            java.lang.String r1 = java.lang.Integer.toHexString(r1)
            r3.append(r1)
            int r0 = r0 + 1
            goto L_0x0050
        L_0x006c:
            java.lang.String r0 = r3.toString()
            java.lang.String r0 = r0.toUpperCase()
            byte[] r0 = r0.getBytes()
            java.lang.String r0 = mm.purchasesdk.fingerprint.IdentifyApp.base64encode(r0)
            goto L_0x0047
        */
        throw new UnsupportedOperationException("Method not decompiled: mm.purchasesdk.k.b.a(java.lang.String):java.lang.String");
    }

    public String a() {
        XmlSerializer newSerializer = Xml.newSerializer();
        StringWriter stringWriter = new StringWriter();
        try {
            newSerializer.setOutput(stringWriter);
            newSerializer.startDocument("UTF-8", true);
            newSerializer.startTag(PoiTypeDef.All, "Trusted2UnSubsReq");
            newSerializer.startTag(PoiTypeDef.All, "MsgType");
            newSerializer.text("Trusted2UnSubsReq");
            newSerializer.endTag(PoiTypeDef.All, "MsgType");
            newSerializer.startTag(PoiTypeDef.All, "Version");
            newSerializer.text(d.Z());
            newSerializer.endTag(PoiTypeDef.All, "Version");
            newSerializer.startTag(PoiTypeDef.All, "PayCode");
            newSerializer.text(d.H());
            newSerializer.endTag(PoiTypeDef.All, "PayCode");
            newSerializer.startTag(PoiTypeDef.All, "AppID");
            newSerializer.text(d.F());
            newSerializer.endTag(PoiTypeDef.All, "AppID");
            newSerializer.startTag(PoiTypeDef.All, "Timestamp");
            String l = Long.valueOf(Calendar.getInstance().getTimeInMillis() / 1000).toString();
            newSerializer.text(l);
            newSerializer.endTag(PoiTypeDef.All, "Timestamp");
            newSerializer.startTag(PoiTypeDef.All, "ChannelID");
            newSerializer.text(d.t());
            newSerializer.endTag(PoiTypeDef.All, "ChannelID");
            newSerializer.startTag(PoiTypeDef.All, "Signature");
            newSerializer.text(a(l));
            newSerializer.endTag(PoiTypeDef.All, "Signature");
            newSerializer.startTag(PoiTypeDef.All, "sidSignature");
            newSerializer.text(a().getUserSignature());
            newSerializer.endTag(PoiTypeDef.All, "sidSignature");
            newSerializer.endTag(PoiTypeDef.All, "Trusted2UnSubsReq");
            newSerializer.endDocument();
        } catch (Exception e) {
            mm.purchasesdk.l.e.a(this.TAG, "create QueryRequest xml file failed!!", e);
            PurchaseCode.setStatusCode(PurchaseCode.XML_EXCPTION_ERROR);
        }
        return stringWriter.toString();
    }
}
