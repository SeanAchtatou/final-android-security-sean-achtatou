package mm.purchasesdk.k;

import java.io.ByteArrayInputStream;
import mm.purchasesdk.h.f;
import mm.purchasesdk.l.d;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

public class c extends f {
    private String N;
    private String P;

    public void d(String str) {
        this.P = str;
    }

    public void i(String str) {
        this.N = str;
    }

    public boolean parse(String str) {
        XmlPullParser newPullParser = XmlPullParserFactory.newInstance().newPullParser();
        newPullParser.setInput(new ByteArrayInputStream(str.getBytes()), "UTF-8");
        for (int eventType = newPullParser.getEventType(); eventType != 1; eventType = newPullParser.next()) {
            switch (eventType) {
                case 2:
                    String name = newPullParser.getName();
                    if (!"ReturnCode".equals(name)) {
                        if (!"randNum".equals(name)) {
                            if (!"RespMd5".equals(name)) {
                                if (!"ErrorMsg".equals(name)) {
                                    break;
                                } else {
                                    String nextText = newPullParser.nextText();
                                    setErrMsg(nextText);
                                    d.setErrMsg(nextText);
                                    break;
                                }
                            } else {
                                d(newPullParser.nextText());
                                break;
                            }
                        } else {
                            i(newPullParser.nextText());
                            break;
                        }
                    } else {
                        C(newPullParser.nextText());
                        break;
                    }
            }
        }
        return true;
    }
}
