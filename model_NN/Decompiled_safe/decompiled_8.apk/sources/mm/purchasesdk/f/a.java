package mm.purchasesdk.f;

import android.os.Build;
import android.util.Log;
import com.amap.mapapi.poisearch.PoiTypeDef;
import java.util.Calendar;
import mm.purchasesdk.l.d;

public class a {
    private static long c = 0;
    private static long d = 0;
    private static long e = 0;
    public static int z = -1;

    public static long c() {
        return d;
    }

    public static long getStartTime() {
        return c;
    }

    public static void k() {
        c = Calendar.getInstance().getTimeInMillis() / 1000;
    }

    public static void l() {
        d = Calendar.getInstance().getTimeInMillis() / 1000;
    }

    public static void m() {
        com.b.a.a(d.getContext(), q(), r());
    }

    private static String q() {
        switch (z) {
            case 1:
                return "_pay_init";
            case 2:
                return "_pay_auth";
            case 3:
                return "_pay_pay";
            default:
                return "unknown";
        }
    }

    public static String r() {
        String str;
        String F = d.F();
        String H = d.H();
        String valueOf = String.valueOf(d.a());
        String X = d.X();
        String valueOf2 = String.valueOf(d.j());
        String packageName = d.getPackageName();
        String P = d.P();
        String t = d.t();
        String L = d.L();
        String M = d.M();
        String str2 = Build.MODEL;
        String str3 = Build.VERSION.RELEASE;
        String str4 = d.m298a().widthPixels + "*" + d.m298a().heightPixels;
        String R = d.R();
        String Q = d.Q();
        String str5 = PoiTypeDef.All;
        if (Q.compareTo("GPRS") != 0) {
            str5 = d.S();
        }
        String Z = d.Z();
        String valueOf3 = String.valueOf(getStartTime());
        String valueOf4 = String.valueOf(c());
        switch (z) {
            case 1:
                str = F + "@@" + packageName + "@@" + P + "@@" + t + "@@" + L + "@@" + M + "@@" + str2 + "@@" + str3 + "@@" + str4 + "@@" + Q + "@@" + R + "@@" + str5 + "@@" + Z + "@@" + valueOf3 + "@@" + valueOf4;
                break;
            case 2:
                str = F + "@@" + H + "@@" + valueOf + "@@" + X + "@@" + valueOf2 + "@@" + packageName + "@@" + P + "@@" + t + "@@" + L + "@@" + M + "@@" + Q + "@@" + R + "@@" + str5 + "@@" + Z + "@@" + valueOf3 + "@@" + valueOf4;
                break;
            case 3:
                str = F + "@@" + H + "@@" + valueOf + "@@" + X + "@@" + valueOf2 + "@@" + packageName + "@@" + P + "@@" + t + "@@" + L + "@@" + M + "@@" + Q + "@@" + R + "@@" + str5 + "@@" + Z + "@@" + valueOf3 + "@@" + valueOf4;
                break;
            default:
                str = "unknown";
                break;
        }
        Log.d("DAHelper", str);
        return str;
    }
}
