package mm.purchasesdk;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import com.amap.mapapi.poisearch.PoiTypeDef;
import mm.purchasesdk.f.a;
import mm.purchasesdk.g.b;
import mm.purchasesdk.l.d;
import mm.purchasesdk.l.e;
import mm.purchasesdk.l.g;
import mm.purchasesdk.ui.u;

public class Purchase {
    public static final String TAG = Purchase.class.getSimpleName();

    /* renamed from: a  reason: collision with root package name */
    private static Purchase f3122a;
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with other field name */
    public Handler f226a;

    /* renamed from: a  reason: collision with other field name */
    private HandlerThread f227a;

    /* renamed from: a  reason: collision with other field name */
    private g f228a = new g("purchase-task");
    /* access modifiers changed from: private */
    public Handler b;

    private Purchase() {
        this.f228a.start();
        this.f228a.init();
        this.f227a = new HandlerThread("Response-thread");
        this.f227a.start();
        this.b = new d(this, this.f227a.getLooper());
        this.f228a.a(this.b);
        this.f226a = this.f228a.m292a();
    }

    public static String getDescription(int i) {
        return PurchaseCode.getDescription(i);
    }

    public static Purchase getInstance() {
        if (f3122a == null) {
            f3122a = new Purchase();
        }
        return f3122a;
    }

    public static String getReason(int i) {
        return PurchaseCode.getReason(i);
    }

    public void clearCache(Context context) {
        if (context == null || !(context instanceof Activity)) {
            throw new Exception("Context Object is null or Context Object is not instance of Activity");
        }
        new b().b(context);
    }

    public void enableCache(boolean z) {
        d.enableCache(z);
    }

    public String getSDKVersion(Context context) {
        if (context == null || !(context instanceof Activity)) {
            throw new Exception("Context Object is null or Context Object is not instance of Activity");
        }
        String b2 = mm.purchasesdk.g.d.b(context);
        return b2 == null ? "unknown" : b2.trim();
    }

    public void init(Context context, OnPurchaseListener onPurchaseListener) {
        if (context == null || !(context instanceof Activity)) {
            throw new Exception("Context Object is null or Context Object is not instance of Activity");
        } else if (onPurchaseListener == null) {
            throw new Exception("OnPurchaseListener Object is null");
        } else if (!d.a(0)) {
            throw new Exception("Another request is processing");
        } else {
            d.setContext(context);
            b bVar = new b(onPurchaseListener, this.f226a, this.b);
            d.z();
            u.a().r();
            int a2 = f.a(context);
            if (a2 != 0) {
                bVar.onInitFinish(a2);
            } else if (!d.d()) {
                bVar.onInitFinish(100);
            } else {
                c.init(context, d.F(), d.t());
                a.z = 1;
                Message obtainMessage = this.f226a.obtainMessage();
                obtainMessage.what = 0;
                obtainMessage.obj = bVar;
                obtainMessage.sendToTarget();
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: mm.purchasesdk.Purchase.order(android.content.Context, java.lang.String, int, boolean, mm.purchasesdk.OnPurchaseListener):java.lang.String
     arg types: [android.content.Context, java.lang.String, int, int, mm.purchasesdk.OnPurchaseListener]
     candidates:
      mm.purchasesdk.Purchase.order(android.content.Context, java.lang.String, int, java.lang.String, mm.purchasesdk.OnPurchaseListener):java.lang.String
      mm.purchasesdk.Purchase.order(android.content.Context, java.lang.String, int, boolean, mm.purchasesdk.OnPurchaseListener):java.lang.String */
    public String order(Context context, String str, int i, String str2, OnPurchaseListener onPurchaseListener) {
        if (str2 != null && str2.trim().length() > 0) {
            d.J(str2);
        }
        return order(context, str, i, false, onPurchaseListener);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: mm.purchasesdk.Purchase.order(android.content.Context, java.lang.String, int, boolean, mm.purchasesdk.OnPurchaseListener):java.lang.String
     arg types: [android.content.Context, java.lang.String, int, int, mm.purchasesdk.OnPurchaseListener]
     candidates:
      mm.purchasesdk.Purchase.order(android.content.Context, java.lang.String, int, java.lang.String, mm.purchasesdk.OnPurchaseListener):java.lang.String
      mm.purchasesdk.Purchase.order(android.content.Context, java.lang.String, int, boolean, mm.purchasesdk.OnPurchaseListener):java.lang.String */
    public String order(Context context, String str, int i, OnPurchaseListener onPurchaseListener) {
        return order(context, str, i, false, onPurchaseListener);
    }

    public String order(Context context, String str, int i, boolean z, OnPurchaseListener onPurchaseListener) {
        if (context == null || !(context instanceof Activity)) {
            throw new Exception("Context Object is null or Context Object is not instance of Activity");
        } else if (onPurchaseListener == null) {
            throw new Exception("OnPurchaseListener Object is null");
        } else if (str == null || str.trim().length() == 0) {
            throw new Exception("Paycode is null");
        } else if (i <= 0) {
            throw new Exception("orderCount must be greater than 0 ");
        } else if (!d.a(2)) {
            throw new Exception("Another request is processing");
        } else {
            d.setContext(context);
            d.e(str);
            d.f(i);
            d.a(Boolean.valueOf(z));
            u.a().s();
            b bVar = new b(onPurchaseListener, this.f226a, this.b);
            d.z();
            int a2 = f.a(context);
            if (a2 != 0) {
                bVar.a(a2, null);
                return null;
            }
            String W = d.W();
            d.T(W);
            c.init(context, d.F(), d.t());
            a.z = 2;
            Message obtainMessage = this.f226a.obtainMessage();
            obtainMessage.what = 0;
            obtainMessage.obj = bVar;
            obtainMessage.sendToTarget();
            return W;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: mm.purchasesdk.Purchase.order(android.content.Context, java.lang.String, int, boolean, mm.purchasesdk.OnPurchaseListener):java.lang.String
     arg types: [android.content.Context, java.lang.String, int, int, mm.purchasesdk.OnPurchaseListener]
     candidates:
      mm.purchasesdk.Purchase.order(android.content.Context, java.lang.String, int, java.lang.String, mm.purchasesdk.OnPurchaseListener):java.lang.String
      mm.purchasesdk.Purchase.order(android.content.Context, java.lang.String, int, boolean, mm.purchasesdk.OnPurchaseListener):java.lang.String */
    public String order(Context context, String str, OnPurchaseListener onPurchaseListener) {
        return order(context, str, 1, false, onPurchaseListener);
    }

    public void query(Context context, String str, String str2, OnPurchaseListener onPurchaseListener) {
        if (context == null || !(context instanceof Activity)) {
            throw new Exception("Context Object is null or Constext Objext is not instanse of Activity");
        } else if (onPurchaseListener == null) {
            throw new Exception("OnPurchaseListener Object is null!");
        } else if (str == null || str.trim().length() == 0) {
            throw new Exception("Error! Paycode is null!");
        } else if (!d.a(1)) {
            throw new Exception("Another request is processing");
        } else {
            d.setContext(context);
            d.e(str);
            if (str2 == null || str2.trim().length() == 0) {
                d.T(PoiTypeDef.All);
                d.e((Boolean) false);
            } else {
                d.T(str2);
                d.e((Boolean) true);
            }
            b bVar = new b(onPurchaseListener, this.f226a, this.b);
            d.z();
            int a2 = f.a(context);
            if (a2 != 0) {
                bVar.onQueryFinish(a2, null);
                return;
            }
            Message obtainMessage = this.f226a.obtainMessage();
            obtainMessage.what = 0;
            obtainMessage.obj = bVar;
            obtainMessage.sendToTarget();
        }
    }

    public void query(Context context, String str, OnPurchaseListener onPurchaseListener) {
        query(context, str, PoiTypeDef.All, onPurchaseListener);
    }

    public void setAppInfo(String str, String str2) {
        if (str == null || str2 == null || str.trim().length() == 0 || str2.trim().length() == 0) {
            throw new Exception("invalid app parameter, pls check it");
        }
        e.A();
        d.b(str.trim(), str2.trim());
    }

    public void setTimeout(int i, int i2) {
        g.setTimeout(i, i2);
    }

    public void unsubscribe(Context context, String str, OnPurchaseListener onPurchaseListener) {
        if (context == null || !(context instanceof Activity)) {
            throw new Exception("Context Object is null");
        } else if (onPurchaseListener == null) {
            throw new Exception("OnPurchaseListener Object is null");
        } else if (str == null || str.trim().length() == 0) {
            throw new Exception("Paycode is null");
        } else if (!d.a(3)) {
            throw new Exception("Another request is processing");
        } else {
            d.e(str);
            d.setContext(context);
            b bVar = new b(onPurchaseListener, this.f226a, this.b);
            d.z();
            int a2 = f.a(context);
            if (a2 != 0) {
                bVar.onUnsubscribeFinish(a2);
                return;
            }
            Message obtainMessage = this.f226a.obtainMessage();
            obtainMessage.what = 0;
            obtainMessage.obj = bVar;
            obtainMessage.sendToTarget();
        }
    }
}
