package mm.purchasesdk;

import android.os.Handler;
import java.util.HashMap;
import mm.purchasesdk.l.d;
import mm.purchasesdk.l.e;
import mm.purchasesdk.ui.aa;
import mm.purchasesdk.ui.u;

public class b {
    private static int b = 0;

    /* renamed from: a  reason: collision with root package name */
    private int f3126a = 0;

    /* renamed from: a  reason: collision with other field name */
    private Handler f229a;

    /* renamed from: a  reason: collision with other field name */
    private HashMap f230a = null;

    /* renamed from: a  reason: collision with other field name */
    private OnPurchaseListener f231a;

    /* renamed from: b  reason: collision with other field name */
    private Handler f232b;

    public b(OnPurchaseListener onPurchaseListener, Handler handler, Handler handler2) {
        this.f231a = onPurchaseListener;
        this.f229a = handler;
        this.f232b = handler2;
    }

    public void a() {
        e.e("onBillingfinish", "code=" + this.f3126a + "." + PurchaseCode.getReason(this.f3126a));
        u.a().v();
        d.unlock();
        aa.w();
        d.e(this.f3126a);
        this.f231a.onBillingFinish(this.f3126a, this.f230a);
    }

    public void a(int i) {
        this.f3126a = i;
    }

    public void a(int i, HashMap hashMap) {
        int i2 = PurchaseCode.ORDER_OK;
        e.e("onDialogShow", "code=" + i + "." + d.K());
        this.f230a = hashMap;
        if (i != 102 || !d.K().equals("3")) {
            if (i != 101) {
                i2 = i;
            }
            u.a().a(i2, this, this.f229a, this.f232b, hashMap);
            i = i2;
        } else {
            u.a().a(i, d.E(), this, this.f229a, this.f232b, hashMap);
        }
        d.e(i);
    }

    public void a(HashMap hashMap) {
        this.f230a = hashMap;
    }

    public void b(int i) {
        b = i;
    }

    public void onAfterApply() {
        this.f231a.onAfterApply();
    }

    public void onBeforeApply() {
        this.f231a.onBeforeApply();
    }

    public void onInitFinish(int i) {
        e.e("onInitFinish", "code=" + i + "." + PurchaseCode.getReason(i));
        d.unlock();
        d.e(this.f3126a);
        this.f231a.onInitFinish(i);
    }

    public void onQueryFinish(int i, HashMap hashMap) {
        e.e("onQueryFinish", "code=" + i + "." + PurchaseCode.getReason(i));
        d.unlock();
        this.f231a.onQueryFinish(i, hashMap);
        b = 0;
        d.e(i);
    }

    public void onUnsubscribeFinish(int i) {
        e.e("onInitFinish", "code=" + i + "." + PurchaseCode.getReason(i));
        d.unlock();
        d.e(this.f3126a);
        this.f231a.onUnsubscribeFinish(i);
    }
}
