package mm.purchasesdk.j;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.widget.Toast;
import mm.purchasesdk.PurchaseCode;
import mm.purchasesdk.h.g;
import mm.purchasesdk.l.b;
import mm.purchasesdk.l.d;

public class c {
    private final String TAG = "SMSReqFactory";
    private String am;
    private Handler handler;

    public c(String str) {
        this.am = str;
        HandlerThread handlerThread = new HandlerThread("mm-requestSMS");
        handlerThread.start();
        this.handler = new d(this, handlerThread.getLooper());
    }

    /* access modifiers changed from: private */
    public void I(String str) {
        Toast.makeText(d.getContext(), str, 0).show();
    }

    public void o() {
        g gVar = new g();
        gVar.D(this.am);
        b.a(0, "SMSReqFactory", "smsrequest-->" + gVar.toString());
        Message obtainMessage = this.handler.obtainMessage(PurchaseCode.UNSUPPORT_ENCODING_ERR);
        obtainMessage.obj = gVar;
        this.handler.post(new f(obtainMessage, gVar));
    }
}
