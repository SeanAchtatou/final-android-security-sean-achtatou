package mm.purchasesdk.j;

import android.os.Message;
import mm.purchasesdk.g.e;
import mm.purchasesdk.h.g;
import mm.purchasesdk.h.h;

public class f implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private Message f3150a;

    /* renamed from: a  reason: collision with other field name */
    private g f262a;

    public f(Message message, g gVar) {
        this.f3150a = message;
        this.f262a = gVar;
    }

    public void run() {
        h hVar = new h();
        try {
            new e().d(this.f262a, hVar);
        } catch (mm.purchasesdk.e e) {
            e.printStackTrace();
        }
        this.f3150a.arg1 = hVar.f();
        this.f3150a.obj = hVar.getErrMsg();
        this.f3150a.sendToTarget();
    }
}
