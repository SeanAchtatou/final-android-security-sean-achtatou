package mm.purchasesdk.j;

import android.app.Activity;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import java.util.List;

public class e extends ContentObserver {

    /* renamed from: a  reason: collision with root package name */
    private Activity f3149a;

    /* renamed from: a  reason: collision with other field name */
    private List f261a;
    private Handler handler;

    public e(Handler handler2, Activity activity) {
        super(handler2);
        this.f3149a = activity;
        this.handler = handler2;
    }

    public void onChange(boolean z) {
        this.f261a = new a(this.f3149a, Uri.parse("content://sms/inbox")).a();
        Message obtainMessage = this.handler.obtainMessage();
        obtainMessage.what = 1;
        obtainMessage.obj = ((b) this.f261a.get(0)).x();
        obtainMessage.sendToTarget();
        super.onChange(z);
    }
}
