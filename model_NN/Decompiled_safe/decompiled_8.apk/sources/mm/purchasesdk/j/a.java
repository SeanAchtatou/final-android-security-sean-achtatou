package mm.purchasesdk.j;

import android.app.Activity;
import android.database.Cursor;
import android.net.Uri;
import com.immomo.momo.service.bean.Message;
import java.util.ArrayList;
import java.util.List;

public class a {

    /* renamed from: a  reason: collision with root package name */
    private Activity f3147a;

    /* renamed from: a  reason: collision with other field name */
    List f260a = new ArrayList();
    private Uri uri;

    public a(Activity activity, Uri uri2) {
        this.f3147a = activity;
        this.uri = uri2;
    }

    public List a() {
        Cursor managedQuery = this.f3147a.managedQuery(this.uri, new String[]{Message.DBFIELD_ID, "address", "person", "body", "date", "type"}, null, null, "date desc");
        int columnIndex = managedQuery.getColumnIndex("person");
        int columnIndex2 = managedQuery.getColumnIndex("address");
        int columnIndex3 = managedQuery.getColumnIndex("body");
        int columnIndex4 = managedQuery.getColumnIndex("date");
        int columnIndex5 = managedQuery.getColumnIndex("type");
        if (managedQuery != null) {
            while (managedQuery.moveToNext()) {
                b bVar = new b();
                bVar.setName(managedQuery.getString(columnIndex));
                bVar.G(managedQuery.getString(columnIndex4));
                bVar.F(managedQuery.getString(columnIndex2));
                bVar.E(managedQuery.getString(columnIndex3));
                bVar.H(managedQuery.getString(columnIndex5));
                this.f260a.add(bVar);
            }
            managedQuery.close();
        }
        return this.f260a;
    }
}
