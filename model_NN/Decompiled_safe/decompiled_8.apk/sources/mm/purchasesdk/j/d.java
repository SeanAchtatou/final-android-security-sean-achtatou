package mm.purchasesdk.j;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import mm.purchasesdk.PurchaseCode;

class d extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ c f3148a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    d(c cVar, Looper looper) {
        super(looper);
        this.f3148a = cVar;
    }

    public void handleMessage(Message message) {
        switch (message.what) {
            case PurchaseCode.UNSUPPORT_ENCODING_ERR:
                this.f3148a.I(mm.purchasesdk.l.d.getErrMsg());
                return;
            default:
                return;
        }
    }
}
