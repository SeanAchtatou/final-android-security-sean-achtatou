package mm.purchasesdk.fingerprint;

import com.ccit.mmwlan.phone.MMClientSDK_ForPhone;
import mm.purchasesdk.l.e;

public class IdentifyApp {
    static {
        try {
            System.loadLibrary("identifyapp");
        } catch (Exception e) {
            e.c("IdentifyApp", "loadLibrary faile" + e.getMessage());
        }
    }

    public static synchronized byte[] a(String str) {
        byte[] bArr;
        synchronized (IdentifyApp.class) {
            try {
                bArr = c.base64decode(MMClientSDK_ForPhone.md5Algorithm(str));
            } catch (Exception e) {
                e.d("IdentifyApp", "base64 decrypt license file failure");
                bArr = null;
            }
        }
        return bArr;
    }

    public static native byte[] base64decode(String str);

    public static native String base64encode(byte[] bArr);

    public static native int checkResponse(byte[] bArr, byte[] bArr2, byte[] bArr3);

    public static native int checkSignature(String str);

    public static native byte[] decrypt(byte[] bArr, String str);

    public static native String encryptPassword(String str, String str2);

    public static native synchronized boolean gatherAppSignature(String str, String str2, String str3);

    public static native String generateTransactionID(String str, String str2, String str3, String str4);

    public static native String getAppSignature();

    public static native String getAppTrustInfo(String str, String str2, String str3, String str4);

    public static native int getLastError();

    public static native String getSignature();

    public static native boolean init(Object obj, String str);
}
