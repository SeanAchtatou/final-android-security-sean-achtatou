package mm.purchasesdk.fingerprint;

import android.content.ContextWrapper;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import mm.purchasesdk.l.d;
import mm.purchasesdk.l.e;

public class c {
    public static Boolean e = true;
    private static int status = 0;

    public c() {
        try {
            IdentifyApp.init(d.getContext(), u());
            status = IdentifyApp.getLastError();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
    }

    private static String a(String str, String str2) {
        try {
            try {
                return (String) Class.forName("android.os.SystemProperties").getMethod("get", String.class, String.class).invoke(null, str, str2);
            } catch (IllegalArgumentException e2) {
                e2.printStackTrace();
                return null;
            } catch (IllegalAccessException e3) {
                e3.printStackTrace();
                return null;
            } catch (InvocationTargetException e4) {
                e4.printStackTrace();
                return null;
            }
        } catch (SecurityException e5) {
            e5.printStackTrace();
            return null;
        } catch (NoSuchMethodException e6) {
            e6.printStackTrace();
            return null;
        } catch (ClassNotFoundException e7) {
            e7.printStackTrace();
            return null;
        }
    }

    /* renamed from: a  reason: collision with other method in class */
    public static void m289a(String str, String str2) {
        IdentifyApp.gatherAppSignature(str, d.F(), str2);
        status = IdentifyApp.getLastError();
    }

    public static byte[] base64decode(String str) {
        byte[] base64decode = IdentifyApp.base64decode(str);
        if (base64decode != null) {
            return base64decode;
        }
        throw new Exception("base64 decrypt data failed!");
    }

    public static String d(String str) {
        IOException e2;
        String str2;
        try {
            String G = d.G();
            e.c("MarkManager", "key--->" + G);
            String F = d.F();
            e.c("MarkManager", "appid--->" + F);
            str2 = new String(IdentifyApp.getAppTrustInfo(F, u(), G, str));
            try {
                status = IdentifyApp.getLastError();
            } catch (IOException e3) {
                e2 = e3;
                e2.printStackTrace();
                return str2;
            }
        } catch (IOException e4) {
            IOException iOException = e4;
            str2 = null;
            e2 = iOException;
            e2.printStackTrace();
            return str2;
        }
        return str2;
    }

    public static int getStatus() {
        return status;
    }

    public static void init() {
        try {
            IdentifyApp.init(d.getContext(), u());
            status = IdentifyApp.getLastError();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: mm.purchasesdk.fingerprint.c.a(java.lang.String, java.lang.String):java.lang.String
     arg types: [java.lang.String, ?[OBJECT, ARRAY]]
     candidates:
      mm.purchasesdk.fingerprint.c.a(java.lang.String, java.lang.String):void
      mm.purchasesdk.fingerprint.c.a(java.lang.String, java.lang.String):java.lang.String */
    private static String u() {
        String a2 = a("ro.product.cpu.abi", (String) null);
        String a3 = a("ro.product.cpu.abi2", (String) null);
        System.currentTimeMillis();
        if (a2 == null) {
            return null;
        }
        ZipFile zipFile = new ZipFile(((ContextWrapper) d.getContext()).getPackageCodePath());
        boolean z = false;
        Enumeration<? extends ZipEntry> entries = zipFile.entries();
        while (true) {
            boolean z2 = z;
            if (entries.hasMoreElements()) {
                ZipEntry zipEntry = (ZipEntry) entries.nextElement();
                if (zipEntry.getName().startsWith("lib/" + a2 + "/")) {
                    System.currentTimeMillis();
                    return a2;
                }
                z = zipEntry.getName().startsWith(new StringBuilder("lib/").append(a3).append("/").toString()) ? true : z2;
            } else {
                zipFile.close();
                if (z2) {
                    return a3;
                }
                return null;
            }
        }
    }

    public static String v() {
        String appSignature = IdentifyApp.getAppSignature();
        status = IdentifyApp.getLastError();
        return appSignature;
    }
}
