package mm.purchasesdk.l;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInfo;
import java.io.ByteArrayInputStream;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.interfaces.RSAPublicKey;
import mm.purchasesdk.fingerprint.IdentifyApp;

public class a {
    public static Boolean a(String str) {
        int indexOf = str.indexOf("<RespMd5>");
        return IdentifyApp.checkResponse(str.substring(str.indexOf("<MsgType>"), indexOf).getBytes(), str.substring((indexOf + 10) + -1, str.indexOf("</RespMd5>")).getBytes(), d.G().getBytes()) == 0;
    }

    private static String a(byte[] bArr) {
        try {
            String bigInteger = ((RSAPublicKey) ((X509Certificate) CertificateFactory.getInstance("X.509").generateCertificate(new ByteArrayInputStream(bArr))).getPublicKey()).getModulus().toString(16);
            if (bigInteger.indexOf("modulus: ") != -1) {
                bigInteger = bigInteger.substring(bigInteger.indexOf("modulus: ") + 9, bigInteger.indexOf("\n", bigInteger.indexOf("modulus:")));
            }
            e.c("CommUtil", "public key:" + bigInteger);
            return bigInteger;
        } catch (CertificateException e) {
            e.printStackTrace();
            return null;
        }
    }

    private static byte[] a() {
        Context context = d.getContext();
        String str = context.getApplicationInfo().packageName;
        e.c("CommUtil", "package name:" + str);
        for (PackageInfo next : ((Activity) context).getPackageManager().getInstalledPackages(64)) {
            if (next.packageName.equals(str)) {
                return next.signatures[0].toByteArray();
            }
        }
        return null;
    }

    public static String z() {
        return a(a());
    }
}
