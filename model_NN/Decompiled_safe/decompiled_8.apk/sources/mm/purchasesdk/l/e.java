package mm.purchasesdk.l;

import android.os.Environment;
import android.util.Log;
import com.amap.mapapi.poisearch.PoiTypeDef;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import mm.purchasesdk.PurchaseCode;

public class e implements InvocationHandler {
    private static final String TAG = e.class.getSimpleName();
    private static int aa = 1;
    private Object b;

    public static void A() {
        File file = new File(Environment.getExternalStorageDirectory() + "/InAppBillingLibrary/IAPConfig");
        String str = PoiTypeDef.All;
        if (file.exists()) {
            try {
                FileInputStream fileInputStream = new FileInputStream(file);
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(fileInputStream));
                while (true) {
                    String readLine = bufferedReader.readLine();
                    if (readLine == null) {
                        break;
                    }
                    str = str + readLine;
                }
                fileInputStream.close();
                switch (new Integer(str).intValue()) {
                    case 0:
                        aa = 0;
                        return;
                    case 1:
                        aa = 1;
                        return;
                    case 2:
                        aa = 2;
                        return;
                    default:
                        aa = 1;
                        return;
                }
            } catch (FileNotFoundException e) {
                aa = 1;
            } catch (IOException e2) {
                aa = 1;
            }
        } else {
            aa = 1;
        }
    }

    public static void a(int i, String str, String str2) {
        if (i >= aa) {
            switch (i) {
                case 0:
                    Log.d(str, str2);
                    f.f(str, str2);
                    return;
                case 1:
                    Log.w(str, str2);
                    f.f(str, str2);
                    return;
                case 2:
                    Log.e(str, str2);
                    f.f(str, str2);
                    return;
                default:
                    Log.i(str, str2);
                    f.f(str, str2);
                    return;
            }
        }
    }

    public static void a(int i, String str, String str2, Exception exc) {
        if (i >= aa) {
            switch (i) {
                case 0:
                    Log.d(str, str2);
                    f.f(str, str2);
                    return;
                case 1:
                    Log.w(str, str2);
                    f.f(str, str2);
                    return;
                case 2:
                    Log.e(str, str2, exc);
                    f.f(str, str2);
                    return;
                default:
                    Log.i(str, str2);
                    f.f(str, str2);
                    return;
            }
        }
    }

    public static void a(String str, String str2, Exception exc) {
        a(2, str, str2, exc);
    }

    public static void c(String str, String str2) {
        a(0, str, str2);
    }

    public static void d(String str, String str2) {
        a(1, str, str2);
    }

    public static void e(String str, String str2) {
        a(2, str, str2);
    }

    public Object invoke(Object obj, Method method, Object[] objArr) {
        String name = method.getName();
        a(2, "PurchaseListener", name);
        if (name.compareTo("onQueryFinish") == 0) {
            e("PurchaseListener", "query statuscode = " + objArr[0] + " (" + PurchaseCode.getReason(((Integer) objArr[0]).intValue()) + ")");
        } else if (name.compareTo("onInitFinish") == 0) {
            e("PurchaseListener", "Init statuscode = " + objArr[0] + " (" + PurchaseCode.getReason(((Integer) objArr[0]).intValue()) + ")");
        } else if (name.compareTo("onUnsubscribeFinish") == 0) {
            e("PurchaseListener", "Unsubscribe statuscode = " + objArr[0] + " (" + PurchaseCode.getReason(((Integer) objArr[0]).intValue()) + ")");
        } else if (name.compareTo("onBillingFinish") == 0) {
            e("PurchaseListener", "Billing statuscode = " + objArr[0] + " (" + PurchaseCode.getReason(((Integer) objArr[0]).intValue()) + ")");
        }
        return method.invoke(this.b, objArr);
    }
}
