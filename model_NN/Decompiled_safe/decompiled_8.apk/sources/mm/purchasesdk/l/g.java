package mm.purchasesdk.l;

import android.content.Context;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.telephony.TelephonyManager;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.ccit.mmwlan.phone.IPDress_ForPhone;
import com.ccit.mmwlan.vo.IPDress_ForPad;
import java.io.File;
import java.io.FileInputStream;
import java.net.URI;
import java.security.KeyStore;
import org.apache.http.HttpHost;
import org.apache.http.client.HttpClient;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;

public class g {
    private static final String TAG = g.class.getSimpleName();
    public static String aQ = null;
    public static String aR = null;
    public static String aS = null;
    private static int ag = 10000;
    private static int ah = 10000;

    public static IPDress_ForPhone a() {
        IPDress_ForPhone iPDress_ForPhone = new IPDress_ForPhone();
        iPDress_ForPhone.setStrApplySecCertIP("ospd.mmarket.com:80");
        iPDress_ForPhone.setStrSMSNumber("10658424");
        return iPDress_ForPhone;
    }

    /* renamed from: a  reason: collision with other method in class */
    public static IPDress_ForPad m299a() {
        IPDress_ForPad iPDress_ForPad = new IPDress_ForPad();
        iPDress_ForPad.setStrApplySecCertIP("ospd.mmarket.com:80");
        iPDress_ForPad.setStrGetDeviceName("ospd.mmarket.com:80");
        iPDress_ForPad.setStrDeviceOuthIp("ospd.mmarket.com:80");
        return iPDress_ForPad;
    }

    public static HttpClient a(Context context) {
        if (((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo() == null) {
            e.a(2, TAG, "network not exists, pls check network");
            return null;
        }
        BasicHttpParams basicHttpParams = new BasicHttpParams();
        basicHttpParams.setParameter("http.connection.timeout", new Integer(ah));
        basicHttpParams.setParameter("http.socket.timeout", new Integer(ag));
        String e = e(context);
        if (e != null) {
            basicHttpParams.setParameter("http.route.default-proxy", new HttpHost(e, 80, "http"));
        }
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient(basicHttpParams);
        String c = d.c(context);
        if (!c.startsWith("https")) {
            return defaultHttpClient;
        }
        try {
            a(defaultHttpClient, context, c);
            return defaultHttpClient;
        } catch (Exception e2) {
            e.a(TAG, PoiTypeDef.All, e2);
            return defaultHttpClient;
        }
    }

    private static void a(HttpClient httpClient, Context context, String str) {
        e.a(0, TAG, "keystore.type=" + KeyStore.getDefaultType());
        KeyStore instance = KeyStore.getInstance(KeyStore.getDefaultType());
        String str2 = null;
        try {
            str2 = context.getPackageManager().getApplicationInfo(context.getPackageName(), 0).dataDir;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        FileInputStream fileInputStream = new FileInputStream(new File(str2 + "/files/keystore.bks"));
        instance.load(fileInputStream, "mm10086".toCharArray());
        fileInputStream.close();
        SSLSocketFactory sSLSocketFactory = new SSLSocketFactory(instance);
        sSLSocketFactory.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
        httpClient.getConnectionManager().getSchemeRegistry().register(new Scheme("https", sSLSocketFactory, new URI(str).getPort()));
    }

    /* renamed from: a  reason: collision with other method in class */
    public static boolean m300a(Context context) {
        if (!c.d()) {
            return false;
        }
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
        String simOperator = telephonyManager.getSimOperator();
        String simSerialNumber = telephonyManager.getSimSerialNumber();
        if (simSerialNumber != null && simSerialNumber.trim().length() > 0) {
            simSerialNumber = simSerialNumber.substring(0, 6);
        }
        return "46000".equals(simOperator) || "46002".equals(simOperator) || "46007".equals(simOperator) || "898600".equals(simSerialNumber);
    }

    public static boolean b(Context context) {
        return true;
    }

    public static String e(Context context) {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetworkInfo == null) {
            e.a(2, TAG, "network not exists, pls check network");
            return null;
        }
        String extraInfo = activeNetworkInfo.getExtraInfo();
        if (extraInfo != null && extraInfo.startsWith("cmwap")) {
            return "10.0.0.172";
        }
        if (extraInfo == null || !extraInfo.startsWith("cmmm")) {
            return null;
        }
        return "192.168.11.5";
    }

    public static void setTimeout(int i, int i2) {
        ag = i;
        ah = i2;
    }
}
