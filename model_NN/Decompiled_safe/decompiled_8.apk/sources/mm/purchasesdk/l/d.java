package mm.purchasesdk.l;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.WindowManager;
import com.amap.mapapi.poisearch.PoiTypeDef;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;
import mm.purchasesdk.c.a;
import mm.purchasesdk.fingerprint.IdentifyApp;
import mm.purchasesdk.ui.aa;

public class d {
    private static String E = PoiTypeDef.All;
    private static String Q = "0";
    private static String aA = PoiTypeDef.All;
    private static String aB = PoiTypeDef.All;
    private static String aC = PoiTypeDef.All;
    private static String aD = PoiTypeDef.All;
    private static String aE = null;
    private static String aF = "1";
    private static String aG = PoiTypeDef.All;
    private static String aH = null;
    private static String aI = null;
    private static String aJ = PoiTypeDef.All;
    private static String aK = PoiTypeDef.All;
    private static String aL = PoiTypeDef.All;
    private static String aM = PoiTypeDef.All;
    private static String aN = PoiTypeDef.All;
    private static String aO = PoiTypeDef.All;
    public static int ab = -1;
    private static int ac = 1;
    public static int ad;
    public static int ae;
    private static int af;
    private static String al = null;
    private static String az = PoiTypeDef.All;
    private static Boolean h = false;
    private static Boolean i = true;
    private static Boolean j = false;
    public static float k;

    /* renamed from: k  reason: collision with other field name */
    private static Boolean f263k = false;
    private static boolean l = false;
    private static boolean m = false;
    private static Context mContext;
    private static boolean n = true;

    public static String C() {
        return aJ;
    }

    public static String D() {
        return aH;
    }

    public static String E() {
        return aG;
    }

    public static String F() {
        return az;
    }

    public static String G() {
        return aA;
    }

    public static String H() {
        return aK;
    }

    public static String I() {
        return n ? j.booleanValue() ? "1" : "2" : "4";
    }

    public static String J() {
        return aE;
    }

    public static void J(String str) {
        aJ = str;
    }

    public static String K() {
        return aF;
    }

    public static void K(String str) {
        aH = str;
    }

    public static String L() {
        String subscriberId = ((TelephonyManager) mContext.getSystemService("phone")).getSubscriberId();
        aM = subscriberId;
        if (subscriberId == null) {
            aM = "10086";
        }
        e.a(0, "MMBillingSDk", "Imsi-->" + aM);
        return aM;
    }

    public static void L(String str) {
        aI = str;
    }

    public static String M() {
        return aN;
    }

    public static void M(String str) {
        aG = str;
    }

    public static String N() {
        return aO;
    }

    public static void N(String str) {
        aL = str;
    }

    public static String O() {
        e.a(0, "MMBillingSDk", "Copyright url-->" + "http://ospd.mmarket.com:80/taac");
        return "http://ospd.mmarket.com:80/taac";
    }

    public static void O(String str) {
        aE = str;
    }

    public static String P() {
        PackageInfo packageInfo = null;
        try {
            packageInfo = getContext().getPackageManager().getPackageInfo(getContext().getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return packageInfo.versionName;
    }

    public static void P(String str) {
        aF = str;
    }

    public static String Q() {
        Context context = getContext();
        getContext();
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetworkInfo == null) {
            e.e("MMBillingSDk", "network not exists, pls check network");
            return PoiTypeDef.All;
        }
        String typeName = activeNetworkInfo.getTypeName();
        return (typeName.compareTo("MOBILE") == 0 || typeName.compareTo("mobile") == 0) ? "GPRS" : typeName;
    }

    public static void Q(String str) {
    }

    public static String R() {
        try {
            Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
            while (networkInterfaces.hasMoreElements()) {
                Enumeration<InetAddress> inetAddresses = networkInterfaces.nextElement().getInetAddresses();
                while (true) {
                    if (inetAddresses.hasMoreElements()) {
                        InetAddress nextElement = inetAddresses.nextElement();
                        if (!nextElement.isLoopbackAddress()) {
                            return nextElement.getHostAddress().toString();
                        }
                    }
                }
            }
        } catch (SocketException e) {
            Log.e("WifiPreference IpAddress", e.toString());
        }
        return PoiTypeDef.All;
    }

    public static void R(String str) {
        aN = str;
    }

    public static String S() {
        Context context = getContext();
        getContext();
        return ((WifiManager) context.getSystemService("wifi")).getConnectionInfo().getSSID();
    }

    public static void S(String str) {
        aO = str;
    }

    public static String T() {
        return "ospd.mmarket.com";
    }

    public static void T(String str) {
        aC = str;
    }

    public static String U() {
        return "80";
    }

    public static void U(String str) {
        aB = str;
    }

    public static String V() {
        return "MIICZzCCAdKgAwIBAgIDNJv2MAsGCSqGSIb3DQEBBTAzMQswCQYDVQQGEwJDTjEkMCIGA1UEAwwbQ01DQSBhcHBsaWNhdGlvbiBzaWduaW5nIENBMB4XDTExMDMyNDAyMjExOFoXDTMxMDMyNDAyMjExOFowXjELMAkGA1UEBhMCQ04xMzAxBgNVBAMMKuS4reWbveenu+WKqOe7iOerr+W6lOeUqOeJiOadg+S/neaKpOWjsOaYjjEaMBgGA1UEBRMRMjAxMTAzMjQxMDI0MjIyMjUwgZ0wCwYJKoZIhvcNAQEBA4GNADCBiQKBgQDb7UlB5k4kdWACNBmHM+Dw9NSD0Q4o7CR3gTaciZQlXeoCCwuYSAWuhoI5ujQsM47eH12OlIn2IwKYObwa6iVY6CLVnEhPkqQfLXPNCoOI+fFdKqLO1YD0+RRj+4oUXi7vAVBEASeyhZesT8P6m2nPpiExlZjDqJYzX/MKYcIkvwIDAQABo2QwYjATBgNVHSUEDDAKBggrBgEFBQcDAzAfBgNVHSMEGDAWgBSXIbIlzOk/0qZTaEGW5ldxZ9uyjTALBgNVHQ8EBAMCB4AwHQYDVR0OBBYEFDTw9zOSP/ZHrahKl9qApKmRNJZiMAsGCSqGSIb3DQEBBQOBgQB6KJgdTQoNXy4xErgbtiRXz7L+J05HM3K6ZFBUE4/cOFcEXiEuu2YekT+pAZcPm2A6iRdYSKo7LCMIDEZUXdMKzTzkxmk39wy05QAyS6QjW8AWp9A9ufvd741IOnjnRGfN4hzuxPjRHEG86T/+nkmYkVgl7gfLJ7mBpyRNKkzIDg==";
    }

    public static void V(String str) {
        aD = str;
    }

    public static String W() {
        return IdentifyApp.generateTransactionID(F(), H(), M(), L());
    }

    public static void W(String str) {
        if (str.trim().equals("0")) {
            f263k = false;
        } else {
            f263k = true;
        }
    }

    public static String X() {
        return aC;
    }

    public static String Y() {
        return aB;
    }

    public static String Z() {
        return aD;
    }

    public static int a() {
        return ac;
    }

    /* renamed from: a  reason: collision with other method in class */
    public static DisplayMetrics m298a() {
        new DisplayMetrics();
        return getContext().getResources().getDisplayMetrics();
    }

    public static void a(Boolean bool) {
        h = bool;
    }

    public static synchronized boolean a(int i2) {
        boolean z = true;
        synchronized (d.class) {
            if (!m) {
                m = true;
                ab = i2;
            } else {
                z = false;
            }
        }
        return z;
    }

    public static String aa() {
        String L = L();
        return (L == null || L.trim().length() <= 6) ? PoiTypeDef.All : L.substring(0, 5);
    }

    public static String ab() {
        String str = Build.MODEL;
        return (str == null || str.trim().length() <= 0) ? PoiTypeDef.All : str;
    }

    public static String ac() {
        String str = Build.VERSION.RELEASE;
        return (str == null || str.trim().length() <= 0) ? PoiTypeDef.All : str;
    }

    public static String b() {
        return E;
    }

    public static void b(String str) {
        E = str;
    }

    public static void b(String str, String str2) {
        az = str;
        aA = str2;
    }

    public static Boolean c() {
        return f263k;
    }

    public static String c(Context context) {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetworkInfo == null) {
            e.e("MMBillingSDk", "network not exists, pls check network");
            return null;
        }
        activeNetworkInfo.getExtraInfo();
        return "http://ospd.mmarket.com:80/trust";
    }

    public static String d(Context context) {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetworkInfo == null) {
            e.e("MMBillingSDk", "network not exists, pls check network");
            return null;
        }
        activeNetworkInfo.getExtraInfo();
        return "http://ospd.mmarket.com:80/trusted3";
    }

    public static void d(Boolean bool) {
        j = bool;
    }

    public static boolean d() {
        return n;
    }

    public static String e(String str) {
        aK = str;
        return str;
    }

    public static void e(int i2) {
        af = i2;
    }

    public static void e(Boolean bool) {
        i = bool;
    }

    public static boolean e() {
        return l;
    }

    public static void enableCache(boolean z) {
        l = z;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v12, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v16, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v17, resolved type: byte} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String f(java.lang.String r7) {
        /*
            java.lang.String r1 = ""
            java.lang.String r0 = "MD5"
            java.security.MessageDigest r0 = java.security.MessageDigest.getInstance(r0)     // Catch:{ NoSuchAlgorithmException -> 0x005e }
            byte[] r2 = r7.getBytes()     // Catch:{ NoSuchAlgorithmException -> 0x005e }
            r0.update(r2)     // Catch:{ NoSuchAlgorithmException -> 0x005e }
            byte[] r3 = r0.digest()     // Catch:{ NoSuchAlgorithmException -> 0x005e }
            java.lang.StringBuffer r4 = new java.lang.StringBuffer     // Catch:{ NoSuchAlgorithmException -> 0x005e }
            java.lang.String r0 = ""
            r4.<init>(r0)     // Catch:{ NoSuchAlgorithmException -> 0x005e }
            r0 = 0
            r2 = r0
        L_0x001c:
            int r0 = r3.length     // Catch:{ NoSuchAlgorithmException -> 0x005e }
            if (r2 >= r0) goto L_0x0039
            byte r0 = r3[r2]     // Catch:{ NoSuchAlgorithmException -> 0x005e }
            if (r0 >= 0) goto L_0x0025
            int r0 = r0 + 256
        L_0x0025:
            r5 = 16
            if (r0 >= r5) goto L_0x002e
            java.lang.String r5 = "0"
            r4.append(r5)     // Catch:{ NoSuchAlgorithmException -> 0x005e }
        L_0x002e:
            java.lang.String r0 = java.lang.Integer.toHexString(r0)     // Catch:{ NoSuchAlgorithmException -> 0x005e }
            r4.append(r0)     // Catch:{ NoSuchAlgorithmException -> 0x005e }
            int r0 = r2 + 1
            r2 = r0
            goto L_0x001c
        L_0x0039:
            java.lang.String r0 = r4.toString()     // Catch:{ NoSuchAlgorithmException -> 0x005e }
            r2 = 8
            r3 = 24
            java.lang.String r1 = r0.substring(r2, r3)     // Catch:{ NoSuchAlgorithmException -> 0x005e }
            java.lang.String r0 = r1.toUpperCase()     // Catch:{ NoSuchAlgorithmException -> 0x005e }
            java.lang.String r1 = "555"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ NoSuchAlgorithmException -> 0x0066 }
            java.lang.String r3 = "result: "
            r2.<init>(r3)     // Catch:{ NoSuchAlgorithmException -> 0x0066 }
            java.lang.StringBuilder r2 = r2.append(r0)     // Catch:{ NoSuchAlgorithmException -> 0x0066 }
            java.lang.String r2 = r2.toString()     // Catch:{ NoSuchAlgorithmException -> 0x0066 }
            android.util.Log.e(r1, r2)     // Catch:{ NoSuchAlgorithmException -> 0x0066 }
        L_0x005d:
            return r0
        L_0x005e:
            r0 = move-exception
            r6 = r0
            r0 = r1
            r1 = r6
        L_0x0062:
            r1.printStackTrace()
            goto L_0x005d
        L_0x0066:
            r1 = move-exception
            goto L_0x0062
        */
        throw new UnsupportedOperationException("Method not decompiled: mm.purchasesdk.l.d.f(java.lang.String):java.lang.String");
    }

    public static void f(int i2) {
        ac = i2;
    }

    public static Boolean g() {
        return j;
    }

    public static void g(boolean z) {
        n = z;
    }

    public static Context getContext() {
        return mContext;
    }

    public static String getErrMsg() {
        return al;
    }

    public static String getPackageName() {
        PackageInfo packageInfo = null;
        try {
            packageInfo = getContext().getPackageManager().getPackageInfo(getContext().getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return packageInfo.packageName;
    }

    public static Boolean h() {
        return i;
    }

    public static Boolean i() {
        return h;
    }

    public static int j() {
        return af;
    }

    public static String n() {
        return Q;
    }

    private static void reset() {
        a.m = 60;
        a.n = 15;
        aa.V = 10;
        aa.U = 10;
        aa.R = 10;
        aa.T = 20;
        aa.f = 1.0f;
        aa.d = 1.0f;
        aa.g = 16.0f;
        aa.h = 15.0f;
        aa.i = 16.0f;
        aa.j = 18.0f;
    }

    public static void setContext(Context context) {
        mContext = context;
    }

    public static void setErrMsg(String str) {
        al = str;
    }

    public static String t() {
        return aL;
    }

    public static synchronized void unlock() {
        synchronized (d.class) {
            m = false;
            ab = -1;
            y();
        }
    }

    public static void y() {
        mContext = null;
        aC = PoiTypeDef.All;
        ac = 1;
        aB = PoiTypeDef.All;
        E = PoiTypeDef.All;
        aK = PoiTypeDef.All;
        Q = "0";
        h = false;
        i = false;
        j = true;
        f263k = false;
        al = null;
        reset();
    }

    public static void y(String str) {
        Q = str;
    }

    public static void z() {
        reset();
        aa.x();
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((WindowManager) getContext().getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
        k = displayMetrics.density;
        ae = displayMetrics.heightPixels;
        int i2 = displayMetrics.widthPixels;
        ad = i2;
        aa.L = i2;
        if (k < 1.0f) {
            aa.R = 5;
        }
        if (mContext.getResources().getConfiguration().orientation == 2) {
            aa.d = ((float) ae) / 480.0f;
            aa.f = 39.0f;
            aa.S = (int) ((39.0f * ((float) ae)) / 100.0f);
            aa.c = 31.0f;
            aa.M = (int) ((31.0f * ((float) ae)) / 100.0f);
            float f = (float) ((ad * 100) / 800);
            aa.e = f;
            aa.V = (int) ((f * ((float) aa.V)) / 100.0f);
            aa.U = (int) ((aa.e * ((float) aa.U)) / 100.0f);
            aa.R = (int) ((aa.e * ((float) aa.R)) / 100.0f);
            aa.T = (int) ((aa.e * ((float) aa.T)) / 100.0f);
            aa.f265d = true;
            if (k == 1.0f) {
                aa.j *= aa.d;
                aa.h = (aa.h * aa.e) / 100.0f;
                aa.i *= aa.d;
            }
            a.n = (int) ((((float) a.n) * aa.d) / 100.0f);
            a.m = (int) (((float) a.m) * aa.d);
            return;
        }
        aa.d = ((float) ae) / 800.0f;
        aa.f = 42.0f;
        aa.S = (int) ((42.0f * ((float) ae)) / 100.0f);
        aa.c = 35.0f;
        aa.M = (int) ((35.0f * ((float) ae)) / 100.0f);
        float f2 = (float) ((ae * 100) / 800);
        aa.e = f2;
        aa.V = (int) ((f2 * ((float) aa.V)) / 100.0f);
        aa.U = (int) ((aa.e * ((float) aa.U)) / 100.0f);
        aa.R = (int) ((aa.e * ((float) aa.R)) / 100.0f);
        aa.T = (int) ((aa.e * ((float) aa.T)) / 100.0f);
        if (k == 1.0f) {
            aa.j *= aa.d;
            aa.h = (aa.h * aa.e) / 100.0f;
            aa.i *= aa.d;
        }
        aa.f265d = false;
        a.n = (int) (((float) a.n) * aa.d);
        a.m = (int) (((float) a.m) * aa.d);
    }
}
