package mm.purchasesdk.l;

import android.os.Environment;
import android.text.format.Time;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class f {
    public static String aP = null;
    public static Boolean l = true;

    public static void X(String str) {
        l = j();
        a();
        aP = str;
    }

    private static File a() {
        if (!l.booleanValue()) {
            return null;
        }
        String str = Environment.getExternalStorageDirectory() + "/InAppBillingLibrary/log";
        File file = new File(str);
        if (!file.exists()) {
            try {
                file.createNewFile();
                return file;
            } catch (IOException e) {
                File file2 = new File("/data/InAppBillingLibrary/log");
                if (file2.exists()) {
                    return file2;
                }
                try {
                    file2.createNewFile();
                    return file2;
                } catch (IOException e2) {
                    e2.printStackTrace();
                    return file2;
                }
            }
        } else if (((float) (file.length() / 1024)) <= 10240.0f) {
            return file;
        } else {
            file.delete();
            File file3 = new File(str);
            try {
                file3.createNewFile();
                return file3;
            } catch (IOException e3) {
                File file4 = new File("/data/InAppBillingLibrary/log");
                if (file4.exists()) {
                    return file4;
                }
                try {
                    file4.createNewFile();
                    return file4;
                } catch (IOException e4) {
                    return null;
                }
            }
        }
    }

    public static void f(String str, String str2) {
        if (l.booleanValue()) {
            Time time = new Time();
            time.setToNow();
            String format = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
            int i = time.hour;
            int i2 = time.minute;
            int i3 = time.second;
            StringBuilder sb = new StringBuilder(aP + "(appid=" + d.F() + ",paycode=" + d.H() + ")-->:");
            sb.append(format).append("|").append(i).append(":").append(i2).append(":").append(i3).append("-->").append(str).append(":").append(str2);
            write(sb.toString());
        }
    }

    public static Boolean j() {
        File file = new File(Environment.getExternalStorageDirectory() + "/InAppBillingLibrary");
        return Boolean.valueOf((!file.exists() || !file.isDirectory()) ? file.mkdirs() : true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileWriter.<init>(java.io.File, boolean):void throws java.io.IOException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.io.FileWriter.<init>(java.lang.String, boolean):void throws java.io.IOException}
      ClspMth{java.io.FileWriter.<init>(java.io.File, boolean):void throws java.io.IOException} */
    private static void write(String str) {
        j();
        try {
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(a(), true));
            bufferedWriter.append((CharSequence) str);
            bufferedWriter.newLine();
            bufferedWriter.flush();
            bufferedWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
