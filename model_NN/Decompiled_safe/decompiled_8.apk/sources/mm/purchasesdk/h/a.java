package mm.purchasesdk.h;

import android.util.Xml;
import com.amap.mapapi.poisearch.PoiTypeDef;
import java.io.IOException;
import java.io.StringWriter;
import mm.purchasesdk.l.d;
import mm.purchasesdk.l.e;
import org.xmlpull.v1.XmlSerializer;

public class a extends e {
    private static final String TAG = a.class.getSimpleName();

    public String a() {
        XmlSerializer newSerializer = Xml.newSerializer();
        StringWriter stringWriter = new StringWriter();
        try {
            newSerializer.setOutput(stringWriter);
            newSerializer.startDocument("UTF-8", true);
            newSerializer.startTag(PoiTypeDef.All, "applyCopyrightDeclaration");
            newSerializer.startTag(PoiTypeDef.All, "version");
            newSerializer.text("1.0.0");
            newSerializer.endTag(PoiTypeDef.All, "version");
            newSerializer.startTag(PoiTypeDef.All, "appuid");
            newSerializer.text(d.F());
            newSerializer.endTag(PoiTypeDef.All, "appuid");
            newSerializer.startTag(PoiTypeDef.All, "programid");
            newSerializer.text(d.F());
            newSerializer.endTag(PoiTypeDef.All, "programid");
            newSerializer.startTag(PoiTypeDef.All, "osid");
            newSerializer.text("9");
            newSerializer.endTag(PoiTypeDef.All, "osid");
            newSerializer.endTag(PoiTypeDef.All, "applyCopyrightDeclaration");
            newSerializer.endDocument();
            return stringWriter.toString();
        } catch (IllegalArgumentException e) {
            e.a(TAG, PoiTypeDef.All, e);
            throw new RuntimeException();
        } catch (IllegalStateException e2) {
            e.a(TAG, PoiTypeDef.All, e2);
            throw new RuntimeException();
        } catch (IOException e3) {
            e.a(TAG, PoiTypeDef.All, e3);
            throw new RuntimeException();
        }
    }
}
