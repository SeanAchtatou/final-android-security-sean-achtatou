package mm.purchasesdk.h;

import android.util.Xml;
import com.amap.mapapi.poisearch.PoiTypeDef;
import java.io.IOException;
import java.io.StringWriter;
import org.xmlpull.v1.XmlSerializer;

public class g extends e {
    private String am = "0";

    public void D(String str) {
        this.am = str;
    }

    public String getSessionID() {
        return this.am;
    }

    public String toString() {
        XmlSerializer newSerializer = Xml.newSerializer();
        StringWriter stringWriter = new StringWriter();
        try {
            newSerializer.setOutput(stringWriter);
            newSerializer.startDocument("UTF-8", true);
            newSerializer.startTag(PoiTypeDef.All, "Trusted2SmsCodeReq");
            newSerializer.startTag(PoiTypeDef.All, "MsgType");
            newSerializer.text("Trusted2SmsCodeReq");
            newSerializer.endTag(PoiTypeDef.All, "MsgType");
            newSerializer.startTag(PoiTypeDef.All, "Version");
            newSerializer.text("1.0.0");
            newSerializer.endTag(PoiTypeDef.All, "Version");
            newSerializer.startTag(PoiTypeDef.All, "SessionID");
            newSerializer.text(getSessionID());
            newSerializer.endTag(PoiTypeDef.All, "SessionID");
            newSerializer.endTag(PoiTypeDef.All, "Trusted2SmsCodeReq");
            newSerializer.endDocument();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalStateException e2) {
            e2.printStackTrace();
        } catch (IOException e3) {
            e3.printStackTrace();
        }
        return stringWriter.toString();
    }
}
