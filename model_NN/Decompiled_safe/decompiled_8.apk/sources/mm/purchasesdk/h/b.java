package mm.purchasesdk.h;

import java.io.ByteArrayInputStream;
import mm.purchasesdk.l.e;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

public class b extends f {
    private static final String TAG = b.class.getSimpleName();
    public String ag;
    public String ah;
    public String ai;

    public boolean parse(String str) {
        int lastIndexOf;
        XmlPullParser newPullParser = XmlPullParserFactory.newInstance().newPullParser();
        newPullParser.setInput(new ByteArrayInputStream(str.getBytes()), "utf-8");
        for (int eventType = newPullParser.getEventType(); eventType != 1; eventType = newPullParser.next()) {
            switch (eventType) {
                case 2:
                    String name = newPullParser.getName();
                    if (name.compareToIgnoreCase("result") != 0) {
                        if (name.compareToIgnoreCase("errormsg") != 0) {
                            break;
                        } else {
                            this.ah = newPullParser.nextText();
                            break;
                        }
                    } else {
                        this.ag = newPullParser.nextText();
                        break;
                    }
            }
        }
        int indexOf = str.indexOf("<MM_Application_Copyright_Declaration>");
        if (indexOf == -1 || (lastIndexOf = str.lastIndexOf("</MM_Application_Copyright_Declaration>")) == -1) {
            e.d(TAG, "no copyright content found in the response");
            return false;
        }
        this.ai = str.substring(indexOf, lastIndexOf + 39);
        return true;
    }
}
