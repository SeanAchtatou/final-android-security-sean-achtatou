package mm.purchasesdk.h;

import java.io.ByteArrayInputStream;
import mm.purchasesdk.l.d;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

public class h extends f {
    private String getMessage() {
        String w = w();
        if ("0".equals(w)) {
            return "短信正在下发,请稍后。。。";
        }
        if ("1".equals(w)) {
            return "请确认设备是否为pad，或者您是否已经启用了密码";
        }
        if ("11".equals(w)) {
            return "本次申请无效，请重新申请";
        }
        if ("12".equals(w)) {
            return "后台服务繁忙请稍后再试";
        }
        "13".equals(w);
        return "未知错误，请稍后重新申请";
    }

    public h a(byte[] bArr, String str) {
        h hVar = null;
        XmlPullParser newPullParser = XmlPullParserFactory.newInstance().newPullParser();
        newPullParser.setInput(new ByteArrayInputStream(bArr), str);
        d.setErrMsg(null);
        for (int eventType = newPullParser.getEventType(); eventType != 1; eventType = newPullParser.next()) {
            switch (eventType) {
                case 0:
                    hVar = new h();
                    break;
                case 2:
                    String name = newPullParser.getName();
                    if (!"ReturnCode".equals(name)) {
                        if (!"ErrorMsg".equals(name)) {
                            break;
                        } else {
                            String nextText = newPullParser.nextText();
                            setErrMsg(nextText);
                            hVar.setErrMsg(nextText);
                            d.setErrMsg(nextText);
                            break;
                        }
                    } else {
                        String nextText2 = newPullParser.nextText();
                        C(nextText2);
                        hVar.C(nextText2);
                        System.out.println("response=" + w());
                        break;
                    }
            }
        }
        return hVar;
    }

    public int f() {
        int intValue = new Integer(w()).intValue();
        switch (intValue) {
            case 0:
                return 600;
            case 1:
                return 601;
            case 11:
                return 603;
            case 12:
                return 604;
            case 13:
                return 605;
            default:
                return intValue;
        }
    }

    public String getErrMsg() {
        String errMsg = d.getErrMsg();
        return (errMsg == null || errMsg.trim().length() <= 0) ? getMessage() : errMsg;
    }
}
