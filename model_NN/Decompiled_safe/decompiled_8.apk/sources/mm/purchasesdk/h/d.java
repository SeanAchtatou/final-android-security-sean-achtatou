package mm.purchasesdk.h;

import java.io.ByteArrayInputStream;
import mm.purchasesdk.OnPurchaseListener;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

public class d extends f {
    private String E;
    private String N;
    private String P;
    private String aj;

    public String b() {
        return this.E;
    }

    public void b(String str) {
        this.E = str;
    }

    public void d(String str) {
        this.P = str;
    }

    public String e() {
        return this.aj;
    }

    public void g(String str) {
        this.aj = str;
    }

    public void i(String str) {
        this.N = str;
    }

    public boolean parse(String str) {
        XmlPullParser newPullParser = XmlPullParserFactory.newInstance().newPullParser();
        newPullParser.setInput(new ByteArrayInputStream(str.getBytes()), "utf-8");
        for (int eventType = newPullParser.getEventType(); eventType != 1; eventType = newPullParser.next()) {
            switch (eventType) {
                case 2:
                    String name = newPullParser.getName();
                    if (!"ReturnCode".equals(name)) {
                        if (!"OrderID".equals(name)) {
                            if (!"randNum".equals(name)) {
                                if (!"RespMd5".equals(name)) {
                                    if (!"OnderValidDate".equals(name)) {
                                        if (!OnPurchaseListener.TRADEID.equals(name)) {
                                            if (!"ErrorMsg".equals(name)) {
                                                if (!"ErrorMsg".equals(name)) {
                                                    break;
                                                } else {
                                                    String nextText = newPullParser.nextText();
                                                    setErrMsg(nextText);
                                                    mm.purchasesdk.l.d.setErrMsg(nextText);
                                                    break;
                                                }
                                            } else {
                                                setErrMsg(newPullParser.nextText());
                                                break;
                                            }
                                        } else {
                                            g(newPullParser.nextText());
                                            break;
                                        }
                                    } else {
                                        b(newPullParser.nextText());
                                        break;
                                    }
                                } else {
                                    d(newPullParser.nextText());
                                    break;
                                }
                            } else {
                                i(newPullParser.nextText());
                                break;
                            }
                        } else {
                            p(newPullParser.nextText());
                            break;
                        }
                    } else {
                        C(newPullParser.nextText());
                        break;
                    }
            }
        }
        return true;
    }
}
