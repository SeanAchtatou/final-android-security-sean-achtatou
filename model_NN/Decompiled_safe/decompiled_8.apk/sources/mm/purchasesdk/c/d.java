package mm.purchasesdk.c;

import android.os.Message;
import android.view.View;
import java.util.HashMap;
import mm.purchasesdk.PurchaseCode;

class d implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ a f3132a;

    d(a aVar) {
        this.f3132a = aVar;
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case 3:
                if (this.f3132a.m283d().booleanValue()) {
                    this.f3132a.m287f();
                    new e(this).start();
                    this.f3132a.a(view);
                    return;
                }
                return;
            case 4:
                if (a.a(this.f3132a).booleanValue()) {
                    a.a(this.f3132a);
                    return;
                }
                return;
            case 5:
                m.y = 0;
                this.f3132a.dismiss();
                a.a(this.f3132a).a((int) PurchaseCode.CERT_REQUEST_CANCEL);
                a.a(this.f3132a).a((HashMap) null);
                Message obtainMessage = a.b(this.f3132a).obtainMessage();
                obtainMessage.what = 5;
                obtainMessage.obj = a.a(this.f3132a);
                obtainMessage.sendToTarget();
                return;
            default:
                return;
        }
    }
}
