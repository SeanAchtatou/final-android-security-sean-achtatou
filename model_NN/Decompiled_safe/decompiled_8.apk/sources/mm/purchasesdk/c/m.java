package mm.purchasesdk.c;

import android.os.Handler;
import android.os.Message;
import android.widget.Button;

public class m extends Handler {
    public static int y = 0;
    private final String U = "获取授权码";

    /* renamed from: a  reason: collision with root package name */
    private Object f3139a;
    private Button d;

    public m(int i, Object obj) {
        y = i;
        this.f3139a = obj;
    }

    /* access modifiers changed from: protected */
    public void a(int i, Button button) {
        button.setText(i + "s");
    }

    public void a(Button button) {
        this.d = button;
    }

    /* access modifiers changed from: protected */
    public void b(Button button) {
        if (this.f3139a instanceof a) {
            ((a) this.f3139a).m285e();
        }
        button.setText("获取授权码");
    }

    public void handleMessage(Message message) {
        super.handleMessage(message);
        int i = message.what;
        if (y < 0) {
            b(this.d);
            return;
        }
        switch (i) {
            case 30001:
                a(y, this.d);
                y--;
                return;
            case 30002:
                b(this.d);
                return;
            default:
                return;
        }
    }
}
