package mm.purchasesdk.c;

import android.view.MotionEvent;
import android.view.View;

class i implements View.OnTouchListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ a f3137a;

    i(a aVar) {
        this.f3137a = aVar;
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (motionEvent.getAction() == 0) {
            view.setBackgroundDrawable(this.f3137a.j);
            return false;
        }
        view.setBackgroundDrawable(this.f3137a.k);
        return false;
    }
}
