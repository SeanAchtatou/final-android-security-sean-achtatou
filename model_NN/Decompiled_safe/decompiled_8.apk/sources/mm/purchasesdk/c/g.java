package mm.purchasesdk.c;

import android.view.MotionEvent;
import android.view.View;

class g implements View.OnTouchListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ a f3135a;

    g(a aVar) {
        this.f3135a = aVar;
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (motionEvent.getAction() == 0) {
            view.setBackgroundDrawable(a.a(this.f3135a));
            return false;
        }
        view.setBackgroundDrawable(a.b(this.f3135a));
        return false;
    }
}
