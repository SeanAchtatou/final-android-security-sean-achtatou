package mm.purchasesdk.c;

import android.view.MotionEvent;
import android.view.View;

class b implements View.OnTouchListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ a f3130a;

    b(a aVar) {
        this.f3130a = aVar;
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (motionEvent.getAction() != 0) {
            return false;
        }
        switch (view.getId()) {
            case 0:
                a.a(this.f3130a).clearFocus();
                a.b(this.f3130a).requestFocus();
                return false;
            case 1:
                a.a(this.f3130a).requestFocus();
                a.b(this.f3130a).clearFocus();
                return false;
            default:
                return false;
        }
    }
}
