package mm.purchasesdk.c;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.NinePatch;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.NinePatchDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.CycleInterpolator;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.ccit.mmwlan.MMClientSDK_ForPad;
import com.ccit.mmwlan.vo.SignView;
import mm.purchasesdk.PurchaseCode;
import mm.purchasesdk.j.e;
import mm.purchasesdk.l.d;
import mm.purchasesdk.ui.aa;
import mm.purchasesdk.ui.b;

public class a extends b {
    public static int m = 60;
    public static int n = 15;
    private final String S = "我的设备";
    private String T;

    /* renamed from: a  reason: collision with root package name */
    private Drawable f3129a;

    /* renamed from: a  reason: collision with other field name */
    private View.OnClickListener f236a = new d(this);

    /* renamed from: a  reason: collision with other field name */
    private View.OnTouchListener f237a = new b(this);

    /* renamed from: a  reason: collision with other field name */
    private Button f238a;

    /* renamed from: a  reason: collision with other field name */
    private EditText f239a;

    /* renamed from: a  reason: collision with other field name */
    private LinearLayout f240a;

    /* renamed from: a  reason: collision with other field name */
    private ScrollView f241a;

    /* renamed from: a  reason: collision with other field name */
    private TextView f242a;

    /* renamed from: a  reason: collision with other field name */
    private mm.purchasesdk.b f243a;

    /* renamed from: a  reason: collision with other field name */
    private m f244a;
    private Drawable b = null;

    /* renamed from: b  reason: collision with other field name */
    private Handler f245b;

    /* renamed from: b  reason: collision with other field name */
    private Button f246b;

    /* renamed from: b  reason: collision with other field name */
    private EditText f247b;

    /* renamed from: b  reason: collision with other field name */
    private TextView f248b;
    private Drawable c = null;

    /* renamed from: c  reason: collision with other field name */
    private Handler f249c;

    /* renamed from: c  reason: collision with other field name */
    private Button f250c;
    private Drawable d = null;

    /* renamed from: d  reason: collision with other field name */
    private Handler f251d;

    /* renamed from: d  reason: collision with other field name */
    private Boolean f252d = false;
    private Drawable e = null;

    /* renamed from: e  reason: collision with other field name */
    private Handler f253e = new c(this);
    private int f = 5;

    /* renamed from: f  reason: collision with other field name */
    private Drawable f254f = null;
    private int g = 20;

    /* renamed from: g  reason: collision with other field name */
    private Drawable f255g = null;
    private int h = 25;

    /* renamed from: h  reason: collision with other field name */
    private Drawable f256h = null;
    private int i = 15;
    private int j = 20;
    private int k = 15;
    private int l = 5;
    private int o = 5;
    private int p = 10;
    private int q = 15;
    private int r = 10;
    private int s = 5;
    private int t = 16;
    private int u = 3;
    private int v = 20;
    private int w = 10;
    private int x = 15;

    public a(Context context, mm.purchasesdk.b bVar, Handler handler, Handler handler2) {
        super(context, 16973829);
        setOwnerActivity((Activity) context);
        getWindow().requestFeature(1);
        if (d.getContext() == null || ((Activity) d.getContext()) != getOwnerActivity()) {
            d.setContext(context);
        }
        this.f249c = handler;
        this.f245b = handler2;
        this.f243a = bVar;
        this.f244a = new m(60, this);
        this.f251d = new f(this);
        context.getContentResolver().registerContentObserver(Uri.parse("content://sms/"), true, new e(this.f251d, (Activity) context));
    }

    private View a() {
        this.f252d = aa.f265d;
        return this.f252d.booleanValue() ? b() : c();
    }

    private View a(int i2, Drawable drawable) {
        EditText editText = new EditText(d.getContext());
        editText.setId(i2);
        editText.setLayoutParams(d.k < 1.0f ? new RelativeLayout.LayoutParams(-1, 30) : new RelativeLayout.LayoutParams(-1, -2));
        editText.setOnTouchListener(this.f237a);
        editText.setInputType(2);
        editText.setCursorVisible(true);
        editText.setSingleLine();
        editText.setFocusableInTouchMode(true);
        editText.setPadding(this.f, this.f, this.f, this.f);
        editText.setTextSize((float) this.g);
        editText.setBackgroundDrawable(drawable);
        return editText;
    }

    private View a(Context context) {
        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setLayoutParams(new RelativeLayout.LayoutParams(-1, -2));
        linearLayout.setOrientation(0);
        linearLayout.setGravity(1);
        linearLayout.setPadding(this.l, this.l, this.l, this.l);
        this.f248b = new TextView(context);
        this.f248b.setSingleLine();
        this.f248b.setTextColor(-11444116);
        this.f248b.setTextSize((float) this.x);
        this.f248b.setVisibility(8);
        linearLayout.addView(this.f248b);
        return linearLayout;
    }

    /* renamed from: a  reason: collision with other method in class */
    private Animation m275a() {
        TranslateAnimation translateAnimation = new TranslateAnimation(0.0f, 10.0f, 0.0f, 0.0f);
        translateAnimation.setDuration(500);
        translateAnimation.setInterpolator(new CycleInterpolator(5.0f));
        return translateAnimation;
    }

    /* renamed from: a  reason: collision with other method in class */
    private TextView m276a() {
        TextView textView = new TextView(d.getContext());
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, 2);
        layoutParams.setMargins(0, this.v, 0, this.w);
        textView.setLayoutParams(layoutParams);
        textView.setBackgroundDrawable(new BitmapDrawable(aa.a(d.getContext(), "mmiap/image/vertical/line.png")));
        return textView;
    }

    private TextView a(String str) {
        TextView textView = new TextView(d.getContext());
        RelativeLayout.LayoutParams layoutParams = d.k < 1.0f ? new RelativeLayout.LayoutParams(-2, -2) : new RelativeLayout.LayoutParams(-2, -2);
        layoutParams.rightMargin = this.u;
        textView.setPadding(this.r, this.s, this.r, this.s);
        textView.setLayoutParams(layoutParams);
        textView.setText(str);
        textView.setTextColor(-8289919);
        textView.setTextSize((float) this.t);
        return textView;
    }

    /* access modifiers changed from: private */
    public void a(View view) {
        this.f244a.a((Button) view);
        m.y = 45;
        new Thread(new j(this)).start();
    }

    /* access modifiers changed from: private */
    public void a(SignView signView) {
        int result = signView.getResult();
        if (result == 1) {
            m285e();
            a(signView.getErrMsg(), -65536);
            m.y = 5;
            m279g();
        } else if (result == 2) {
            a("生成SID失败,请重新申请手机授权码", -65536);
            i();
            m.y = 0;
            MMClientSDK_ForPad.DestorySecCert(null);
        } else if (result == 3) {
            a("准备PKI密钥对失败,请重新申请手机授权码", -65536);
            i();
            m.y = 0;
            MMClientSDK_ForPad.DestorySecCert(null);
        } else if (result == 6) {
            a("申请参数错误，请核对手机号码是否正确", -65536);
            i();
            m.y = 0;
            MMClientSDK_ForPad.DestorySecCert(null);
        } else if (result == 7) {
            a("申请参数错误，请核对手机号码是否正确", -65536);
            i();
            m.y = 0;
            MMClientSDK_ForPad.DestorySecCert(null);
        } else if (result == 0) {
            a("短信已经下发，收到短信后后请您输入验证码。", -16776961);
        }
    }

    private View b() {
        this.f240a = new LinearLayout(d.getContext());
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -1);
        layoutParams.gravity = 1;
        this.f240a.setOrientation(1);
        this.f240a.setLayoutParams(layoutParams);
        this.f240a.addView(b(d.getContext()));
        this.f240a.addView(m277b());
        this.f240a.addView(m282d());
        this.f240a.addView(m284e());
        this.f240a.addView(f());
        this.f240a.addView(m276a());
        this.f240a.addView(a(d.getContext()));
        this.f240a.addView(h());
        this.f240a.addView(c(d.getContext()));
        this.f241a = new ScrollView(d.getContext());
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-1, -1);
        this.f241a = new ScrollView(d.getContext());
        this.f241a.setLayoutParams(layoutParams2);
        this.f241a.setFillViewport(true);
        this.f241a.setBackgroundDrawable(this.f3129a);
        this.f241a.addView(this.f240a);
        return this.f241a;
    }

    /* renamed from: b  reason: collision with other method in class */
    private TextView m277b() {
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -2);
        TextView textView = new TextView(d.getContext());
        textView.setLayoutParams(layoutParams);
        textView.setText("请输入您的手机号,短信验证码,用以取得设备授权");
        textView.setTextSize(aa.g);
        textView.setTextColor(-8289919);
        textView.setPadding(this.i, this.h, this.k, this.j);
        return textView;
    }

    private View c() {
        this.f240a = new LinearLayout(d.getContext());
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -1);
        layoutParams.gravity = 1;
        this.f240a.setOrientation(1);
        this.f240a.setLayoutParams(layoutParams);
        this.f240a.addView(b(d.getContext()));
        this.f240a.addView(m277b());
        this.f240a.addView(m282d());
        this.f240a.addView(m284e());
        this.f240a.addView(f());
        this.f240a.addView(m276a());
        this.f240a.addView(a(d.getContext()));
        this.f240a.addView(h());
        this.f240a.addView(c(d.getContext()));
        this.f241a = new ScrollView(d.getContext());
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-1, -1);
        this.f241a = new ScrollView(d.getContext());
        this.f241a.setLayoutParams(layoutParams2);
        this.f241a.setFillViewport(true);
        this.f241a.setBackgroundDrawable(this.f3129a);
        this.f241a.addView(this.f240a);
        return this.f241a;
    }

    /* renamed from: c  reason: collision with other method in class */
    private void m278c() {
        String str = Build.DEVICE;
        if (str == null) {
            this.T = "我的设备";
        } else {
            this.T = str;
        }
    }

    private void d() {
        Bitmap a2;
        Bitmap a3;
        Bitmap a4;
        Bitmap a5;
        Bitmap a6;
        Bitmap a7;
        Bitmap a8;
        Bitmap a9;
        if (this.f3129a == null && (a9 = aa.a(d.getContext(), "mmiap/image/vertical/bg.png")) != null) {
            this.f3129a = new BitmapDrawable(a9);
        }
        if (this.c == null && (a8 = aa.a(d.getContext(), "mmiap/image/vertical/editbg.9.png")) != null) {
            byte[] ninePatchChunk = a8.getNinePatchChunk();
            NinePatch.isNinePatchChunk(ninePatchChunk);
            this.c = new NinePatchDrawable(a8, ninePatchChunk, new Rect(), null);
        }
        if (this.f256h == null && (a7 = aa.a(d.getContext(), "mmiap/image/vertical/editbg.9.png")) != null) {
            byte[] ninePatchChunk2 = a7.getNinePatchChunk();
            NinePatch.isNinePatchChunk(ninePatchChunk2);
            this.f256h = new NinePatchDrawable(a7, ninePatchChunk2, new Rect(), null);
        }
        if (this.b == null && (a6 = aa.a(d.getContext(), "mmiap/image/vertical/infobg.9.png")) != null) {
            byte[] ninePatchChunk3 = a6.getNinePatchChunk();
            NinePatch.isNinePatchChunk(ninePatchChunk3);
            this.b = new NinePatchDrawable(a6, ninePatchChunk3, new Rect(), null);
        }
        if (this.f255g == null && (a5 = aa.a(d.getContext(), "mmiap/image/vertical/infobg.9.png")) != null) {
            byte[] ninePatchChunk4 = a5.getNinePatchChunk();
            NinePatch.isNinePatchChunk(ninePatchChunk4);
            this.f255g = new NinePatchDrawable(a5, ninePatchChunk4, new Rect(), null);
        }
        if (this.d == null && (a4 = aa.a(d.getContext(), "mmiap/image/vertical/editbg.9.png")) != null) {
            byte[] ninePatchChunk5 = a4.getNinePatchChunk();
            NinePatch.isNinePatchChunk(ninePatchChunk5);
            this.d = new NinePatchDrawable(a4, ninePatchChunk5, new Rect(), null);
        }
        if (this.e == null && (a3 = aa.a(d.getContext(), "mmiap/image/vertical/get_verificationcode.9.png")) != null) {
            byte[] ninePatchChunk6 = a3.getNinePatchChunk();
            NinePatch.isNinePatchChunk(ninePatchChunk6);
            this.e = new NinePatchDrawable(a3, ninePatchChunk6, new Rect(), null);
        }
        if (this.f254f == null && (a2 = aa.a(d.getContext(), "mmiap/image/vertical/get_verificationcode_press.9.png")) != null) {
            byte[] ninePatchChunk7 = a2.getNinePatchChunk();
            NinePatch.isNinePatchChunk(ninePatchChunk7);
            this.f254f = new NinePatchDrawable(a2, ninePatchChunk7, new Rect(), null);
        }
    }

    private Boolean e() {
        return m286f().booleanValue() && m283d().booleanValue();
    }

    private View g() {
        LinearLayout linearLayout = new LinearLayout(d.getContext());
        linearLayout.setLayoutParams(new RelativeLayout.LayoutParams(-1, -2));
        linearLayout.setGravity(16);
        linearLayout.setOrientation(0);
        this.f247b = new EditText(d.getContext());
        this.f247b.setId(1);
        this.f247b.setLayoutParams(d.k < 1.0f ? new LinearLayout.LayoutParams(0, 30, 2.0f) : new LinearLayout.LayoutParams(0, -2, 2.5f));
        this.f247b.setOnTouchListener(this.f237a);
        this.f247b.setCursorVisible(true);
        this.f247b.setSingleLine();
        this.f247b.setFocusableInTouchMode(true);
        this.f247b.setPadding(this.f, this.f, this.f, this.f);
        this.f247b.setTextSize((float) this.g);
        this.f247b.setBackgroundDrawable(this.f255g);
        this.f247b.setInputType(2);
        linearLayout.addView(this.f247b);
        this.f250c = new Button(d.getContext());
        this.f250c.setId(3);
        this.f250c.setOnTouchListener(new g(this));
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, -1, 1.5f);
        layoutParams.setMargins(this.o, 0, 0, 0);
        this.f250c.setPadding(0, this.p, 0, this.p);
        this.f250c.setText("获取授权码");
        this.f250c.setTextSize(1, (float) this.q);
        this.f250c.setTextColor(-1);
        this.f250c.setLayoutParams(layoutParams);
        this.f250c.setBackgroundDrawable(this.e);
        this.f250c.setOnClickListener(this.f236a);
        this.f250c.setPadding(0, 5, 0, 5);
        linearLayout.addView(this.f250c);
        return linearLayout;
    }

    /* renamed from: g  reason: collision with other method in class */
    private void m279g() {
        ((InputMethodManager) d.getContext().getSystemService("input_method")).toggleSoftInput(0, 2);
    }

    private View h() {
        LinearLayout linearLayout = new LinearLayout(d.getContext());
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams.topMargin = aa.R;
        layoutParams.leftMargin = 5;
        layoutParams.rightMargin = 5;
        linearLayout.setPadding(this.l, this.l, this.l, this.l);
        linearLayout.setLayoutParams(layoutParams);
        linearLayout.setOrientation(0);
        this.f238a = new Button(d.getContext());
        this.f238a.setOnTouchListener(new h(this));
        LinearLayout.LayoutParams layoutParams2 = d.k < 1.0f ? new LinearLayout.LayoutParams(0, 50, 1.0f) : new LinearLayout.LayoutParams(0, m, 1.0f);
        layoutParams2.setMargins(this.o, 0, this.o, 0);
        this.f238a.setPadding(0, n, 0, n);
        this.f238a.setText("确 认");
        this.f238a.setTextSize(1, (float) aa.N);
        this.f238a.setTextColor(-1);
        this.f238a.setLayoutParams(layoutParams2);
        this.f238a.setBackgroundDrawable(this.j);
        this.f238a.setId(4);
        this.f238a.setOnClickListener(this.f236a);
        linearLayout.addView(this.f238a);
        this.f246b = new Button(d.getContext());
        this.f246b.setId(5);
        this.f246b.setOnTouchListener(new i(this));
        this.f246b.setPadding(0, n, 0, n);
        this.f246b.setText("取 消");
        this.f246b.setTextSize(1, (float) aa.N);
        this.f246b.setTextColor(-1);
        this.f246b.setLayoutParams(layoutParams2);
        this.f246b.setBackgroundDrawable(this.k);
        this.f246b.setOnClickListener(this.f236a);
        linearLayout.addView(this.f246b);
        return linearLayout;
    }

    /* renamed from: h  reason: collision with other method in class */
    private void m280h() {
        SignView sidSign_PAD = MMClientSDK_ForPad.sidSign_PAD(d.F(), null, o(), p());
        int result = sidSign_PAD.getResult();
        if (result == 0) {
            Message obtainMessage = this.f249c.obtainMessage();
            obtainMessage.what = d.ab;
            obtainMessage.arg1 = 0;
            obtainMessage.obj = this.f243a;
            obtainMessage.sendToTarget();
            m.y = 0;
            dismiss();
        } else if (result == 4) {
            a("申请失败，请重新申请", -65536);
            i();
            m.y = 0;
        } else if (result == 5) {
            a("安全凭证保存失败，请重申安全凭证", -65536);
            i();
            m.y = 0;
        } else if (result == 6) {
            a("申请参数错误，请核对手机号码是否正确", -65536);
            i();
            m.y = 0;
        } else if (result == 7) {
            a("申请参数错误，请核对手机号码是否正确", -65536);
            i();
            m.y = 0;
        } else {
            a(sidSign_PAD.getErrMsg(), -65536);
            i();
            m.y = 0;
        }
    }

    private void j() {
    }

    private String p() {
        return this.f247b.getText().toString().trim();
    }

    public void a(String str, int i2) {
        this.f248b.setTextColor(i2);
        this.f248b.setText(str);
        this.f248b.setVisibility(0);
    }

    /* renamed from: b  reason: collision with other method in class */
    public int m281b() {
        String obj = this.f239a.getText().toString();
        if (obj == null || obj.trim().length() <= 0) {
            this.f239a.startAnimation(m275a());
            this.f248b.setVisibility(0);
            this.f248b.setTextColor(-65536);
            this.f248b.setText("手机号码不能为空，请输入手机号码！");
            this.f248b.append("\n");
            m285e();
            return PurchaseCode.QUERY_NO_AUTHORIZATION;
        } else if (k.a(obj)) {
            this.f248b.setVisibility(8);
            this.f239a.setTextColor(-11444116);
            return PurchaseCode.QUERY_CSSP_BUSY;
        } else {
            this.f248b.setVisibility(0);
            this.f248b.setTextColor(-65536);
            this.f248b.setText("您输入的手机号码错误，请核对后再次输入!");
            this.f248b.append("\n");
            this.f239a.setTextColor(-65536);
            this.f239a.startAnimation(m275a());
            return PurchaseCode.QUERY_PAYCODE_ERROR;
        }
    }

    /* renamed from: d  reason: collision with other method in class */
    public View m282d() {
        LinearLayout linearLayout = new LinearLayout(d.getContext());
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams.topMargin = aa.R;
        layoutParams.leftMargin = 5;
        layoutParams.rightMargin = 5;
        linearLayout.setPadding(this.l, this.l, this.l, this.l);
        linearLayout.setLayoutParams(layoutParams);
        linearLayout.setOrientation(0);
        linearLayout.addView(a("手 机 号 码 :"));
        this.f239a = (EditText) a(0, this.b);
        linearLayout.addView(this.f239a);
        return linearLayout;
    }

    /* access modifiers changed from: protected */
    /* renamed from: d  reason: collision with other method in class */
    public Boolean m283d() {
        if (504 != m281b()) {
            return false;
        }
        this.f247b.requestFocus();
        this.f239a.clearFocus();
        return true;
    }

    /* renamed from: e  reason: collision with other method in class */
    public View m284e() {
        LinearLayout linearLayout = new LinearLayout(d.getContext());
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams.topMargin = aa.R;
        layoutParams.leftMargin = 5;
        layoutParams.rightMargin = 5;
        linearLayout.setPadding(this.l, this.l, this.l, this.l);
        linearLayout.setLayoutParams(layoutParams);
        linearLayout.setOrientation(0);
        linearLayout.setGravity(16);
        linearLayout.addView(a("设 备 名 称 :"));
        this.f242a = new TextView(d.getContext());
        this.f242a.setId(2);
        this.f242a.setLayoutParams(d.k < 1.0f ? new RelativeLayout.LayoutParams(-1, 30) : new RelativeLayout.LayoutParams(-1, -2));
        this.f242a.setText(this.T);
        this.f242a.setCursorVisible(true);
        this.f242a.setSingleLine();
        this.f242a.setFocusableInTouchMode(true);
        this.f242a.setTextSize((float) this.g);
        this.f242a.setGravity(16);
        this.f242a.setBackgroundDrawable(this.d);
        linearLayout.addView(this.f242a);
        return linearLayout;
    }

    /* renamed from: e  reason: collision with other method in class */
    public void m285e() {
        this.f250c.setClickable(true);
        this.f250c.setBackgroundDrawable(this.e);
    }

    public View f() {
        LinearLayout linearLayout = new LinearLayout(d.getContext());
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams.topMargin = aa.R;
        layoutParams.leftMargin = 5;
        layoutParams.rightMargin = 5;
        linearLayout.setPadding(this.l, this.l, this.l, this.l);
        linearLayout.setLayoutParams(layoutParams);
        linearLayout.setOrientation(0);
        linearLayout.addView(a("手机授权码:"));
        linearLayout.setGravity(16);
        linearLayout.addView(g());
        return linearLayout;
    }

    /* access modifiers changed from: protected */
    /* renamed from: f  reason: collision with other method in class */
    public Boolean m286f() {
        String obj = this.f247b.getText().toString();
        obj.trim();
        if (obj.equals(PoiTypeDef.All)) {
            a("授权码不能为空，请输入短信授权码，或者重新申请短信授权码", -65536);
            this.f247b.startAnimation(m275a());
            return false;
        }
        a("正在发送授权请求，请稍后.....", -16711936);
        return true;
    }

    /* renamed from: f  reason: collision with other method in class */
    public void m287f() {
        this.f250c.setClickable(false);
        this.f250c.setBackgroundDrawable(this.f254f);
    }

    public void i() {
        this.f250c.setClickable(true);
        this.f250c.setBackgroundDrawable(this.e);
    }

    public String o() {
        return this.f239a.getText().toString().trim();
    }

    public void show() {
        d();
        m278c();
        setContentView(a());
        setCancelable(false);
        super.show();
    }
}
