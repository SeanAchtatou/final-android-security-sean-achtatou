package mm.purchasesdk.c;

import com.ccit.mmwlan.MMClientSDK_ForPad;
import com.ccit.mmwlan.phone.MMClientSDK_ForPhone;
import mm.purchasesdk.PurchaseCode;
import mm.purchasesdk.b;
import mm.purchasesdk.l.d;
import mm.purchasesdk.l.e;
import mm.purchasesdk.l.g;
import org.apache.http.HttpHost;

public class l {
    private static final String TAG = l.class.getSimpleName();

    public static int b(b bVar) {
        if (MMClientSDK_ForPhone.checkSecCert() == 0) {
            return 100;
        }
        try {
            bVar.onBeforeApply();
            int c = c();
            if (c != 100) {
                return c;
            }
            bVar.onAfterApply();
            return 100;
        } catch (Exception e) {
            e.a(TAG, "checkPhoneCert exception", e);
            return PurchaseCode.CERT_EXCEPTION;
        }
    }

    public static int c() {
        String e = g.e(d.getContext());
        HttpHost httpHost = null;
        if (e != null) {
            httpHost = new HttpHost(e, 80, "http");
        }
        String F = d.F();
        e.c(TAG, "ip=" + d.T() + ",port=" + d.U() + ",appid=" + F);
        int result = MMClientSDK_ForPhone.SIDSign(F, httpHost).getResult();
        e.c(TAG, "SidSignature result--〉" + result);
        if (result == 0) {
            return 100;
        }
        e.d(TAG, "applyforCert failure: " + result);
        if (result == 2) {
            return PurchaseCode.CETRT_SID_ERR;
        }
        if (result == 3) {
            return PurchaseCode.CERT_PKI_ERR;
        }
        if (result == 4) {
            return PurchaseCode.CERT_PUBKEY_ERR;
        }
        if (result == 5) {
            return PurchaseCode.CERT_IMSI_ERR;
        }
        if (result != 6) {
            return result == 7 ? PurchaseCode.CERT_SMS_ERR : PurchaseCode.APPLYCERT_OTHER_ERR;
        }
        e.d(TAG, "applyforCert failure: 219");
        return PurchaseCode.CERT_NETWORK_FAIL;
    }

    public static int d() {
        return MMClientSDK_ForPad.checkSecCert_PAD() == 0 ? 100 : 6;
    }
}
