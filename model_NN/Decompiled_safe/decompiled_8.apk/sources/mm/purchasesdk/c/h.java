package mm.purchasesdk.c;

import android.view.MotionEvent;
import android.view.View;

class h implements View.OnTouchListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ a f3136a;

    h(a aVar) {
        this.f3136a = aVar;
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (motionEvent.getAction() == 0) {
            view.setBackgroundDrawable(this.f3136a.k);
            return false;
        }
        view.setBackgroundDrawable(this.f3136a.j);
        return false;
    }
}
