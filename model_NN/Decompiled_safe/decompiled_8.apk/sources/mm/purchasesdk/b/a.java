package mm.purchasesdk.b;

import android.graphics.Bitmap;
import com.amap.mapapi.poisearch.PoiTypeDef;

public class a {
    private String A = PoiTypeDef.All;
    private String B = PoiTypeDef.All;
    private String C = PoiTypeDef.All;
    private String D = PoiTypeDef.All;
    private String F;

    /* renamed from: a  reason: collision with root package name */
    private Bitmap f3127a;

    /* renamed from: a  reason: collision with other field name */
    private Boolean f233a = true;
    private Boolean b = false;

    /* renamed from: b  reason: collision with other field name */
    private e f234b;
    private Boolean c = false;
    private int d = 1;

    /* renamed from: d  reason: collision with other field name */
    private boolean f235d;
    private boolean e = false;
    public boolean f = false;
    private boolean g = true;
    private String m;
    private String p;
    private String r;
    private String z = PoiTypeDef.All;

    public int a() {
        return this.d;
    }

    /* renamed from: a  reason: collision with other method in class */
    public Bitmap m270a() {
        return this.f3127a;
    }

    /* renamed from: a  reason: collision with other method in class */
    public Boolean m271a() {
        return this.f233a;
    }

    public void a(Bitmap bitmap) {
        this.f3127a = bitmap;
    }

    public void a(e eVar) {
        this.f234b = eVar;
    }

    public void a(boolean z2) {
        this.g = z2;
    }

    public e b() {
        return this.f234b;
    }

    public void b(Boolean bool) {
        this.c = bool;
    }

    public void b(boolean z2) {
        this.f235d = z2;
    }

    /* renamed from: b  reason: collision with other method in class */
    public boolean m272b() {
        return this.f235d;
    }

    public String c() {
        return this.C;
    }

    public void c(Boolean bool) {
        this.f233a = bool;
    }

    /* renamed from: c  reason: collision with other method in class */
    public boolean m273c() {
        return this.e;
    }

    public String d() {
        return this.D;
    }

    public void e(String str) {
        this.C = str;
    }

    public void f(String str) {
        this.D = str;
    }

    public String g() {
        return this.p;
    }

    public String h() {
        return this.r;
    }

    public void j(String str) {
        this.p = str;
    }

    public void l(String str) {
        this.r = str;
    }

    public void s(String str) {
        this.m = str;
    }

    public void t(String str) {
        this.F = str;
    }
}
