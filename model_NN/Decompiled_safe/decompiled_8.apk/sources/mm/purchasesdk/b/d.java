package mm.purchasesdk.b;

import java.io.ByteArrayInputStream;
import mm.purchasesdk.OnPurchaseListener;
import mm.purchasesdk.h.f;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

public class d extends f {
    private String E;
    private String L;
    private String M;
    private String N;
    private String O;
    private String P;
    private String Q;
    private final String TAG = d.class.getSimpleName();

    public String b() {
        return this.E;
    }

    public void b(String str) {
        this.E = str;
    }

    public void d(String str) {
        this.P = str;
    }

    public void i(String str) {
        this.N = str;
    }

    public void l(String str) {
        this.L = str;
    }

    public void m(String str) {
        this.M = str;
    }

    public String n() {
        return this.Q;
    }

    public boolean parse(String str) {
        if (str == null || str.length() <= 0) {
            return false;
        }
        XmlPullParser newPullParser = XmlPullParserFactory.newInstance().newPullParser();
        newPullParser.setInput(new ByteArrayInputStream(str.getBytes()), "utf-8");
        for (int eventType = newPullParser.getEventType(); eventType != 1; eventType = newPullParser.next()) {
            switch (eventType) {
                case 2:
                    String name = newPullParser.getName();
                    if (!"ReturnCode".equals(name)) {
                        if (!"CheckID".equals(name)) {
                            if (!"CheckCode".equals(name)) {
                                if (!"OrderID".equals(name)) {
                                    if (!"randNum".equals(name)) {
                                        if (!OnPurchaseListener.TRADEID.equals(name)) {
                                            if (!"RespMd5".equals(name)) {
                                                if (!"OnderValidDate".equals(name)) {
                                                    if (!"redirect_url".equals(name)) {
                                                        if (!OnPurchaseListener.ORDERTYPE.equals(name)) {
                                                            if (!"ErrorMsg".equals(name)) {
                                                                break;
                                                            } else {
                                                                String nextText = newPullParser.nextText();
                                                                setErrMsg(nextText);
                                                                mm.purchasesdk.l.d.setErrMsg(nextText);
                                                                break;
                                                            }
                                                        } else {
                                                            y(newPullParser.nextText());
                                                            break;
                                                        }
                                                    } else {
                                                        mm.purchasesdk.l.d.M(newPullParser.nextText());
                                                        break;
                                                    }
                                                } else {
                                                    b(newPullParser.nextText());
                                                    break;
                                                }
                                            } else {
                                                d(newPullParser.nextText());
                                                break;
                                            }
                                        } else {
                                            z(newPullParser.nextText());
                                            break;
                                        }
                                    } else {
                                        i(newPullParser.nextText());
                                        break;
                                    }
                                } else {
                                    p(newPullParser.nextText());
                                    break;
                                }
                            } else {
                                m(newPullParser.nextText());
                                break;
                            }
                        } else {
                            l(newPullParser.nextText());
                            break;
                        }
                    } else {
                        C(newPullParser.nextText());
                        break;
                    }
            }
        }
        return true;
    }

    public void y(String str) {
        this.Q = str;
    }

    public void z(String str) {
        this.O = str;
    }
}
