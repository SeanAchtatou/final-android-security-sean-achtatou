package mm.purchasesdk.b;

import android.os.Bundle;
import com.ccit.mmwlan.phone.MMClientSDK_ForPhone;
import com.immomo.momo.h;
import com.mapabc.minimap.map.vmap.VMapProjection;
import com.sina.weibo.sdk.constant.Constants;
import java.util.ArrayList;
import java.util.Iterator;
import mm.purchasesdk.OnPurchaseListener;
import mm.purchasesdk.PurchaseCode;
import mm.purchasesdk.g.c;
import mm.purchasesdk.g.e;
import mm.purchasesdk.h.f;
import mm.purchasesdk.l.a;
import mm.purchasesdk.l.d;

public class b {
    static final String TAG = b.class.getSimpleName();

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: mm.purchasesdk.g.c.a(mm.purchasesdk.h.e, mm.purchasesdk.h.f):java.lang.String
     arg types: [mm.purchasesdk.b.c, mm.purchasesdk.b.d]
     candidates:
      mm.purchasesdk.g.c.a(mm.purchasesdk.h.e, mm.purchasesdk.h.f):boolean
      mm.purchasesdk.g.c.a(mm.purchasesdk.h.e, mm.purchasesdk.h.f):java.lang.String */
    public static int a(a aVar, Bundle bundle) {
        String str;
        String str2 = null;
        ArrayList arrayList = new ArrayList();
        arrayList.add(new e());
        c cVar = new c();
        if (bundle != null) {
            cVar.v(bundle.getString("dyMark"));
            cVar.w(bundle.getString("CheckAnswer"));
            cVar.l(bundle.getString("CheckId"));
            cVar.x(bundle.getString("Password"));
            cVar.u(bundle.getString("RandomPwd"));
            cVar.j(bundle.getString("SessionId"));
            cVar.c(bundle.getInt("OrderCount"));
            cVar.c(bundle.getBoolean("multiSubs"));
            cVar.d(bundle.getBoolean("NeedPasswd"));
            cVar.e(bundle.getBoolean("NeedInput"));
        }
        d dVar = new d();
        Iterator it = arrayList.iterator();
        while (true) {
            if (!it.hasNext()) {
                str = str2;
                break;
            }
            try {
                str2 = ((c) it.next()).a((mm.purchasesdk.h.e) cVar, (f) dVar);
                if (str2 != null) {
                    str = str2;
                    break;
                }
            } catch (mm.purchasesdk.e e) {
                PurchaseCode.setStatusCode(PurchaseCode.NONE_NETWORK);
                return PurchaseCode.NONE_NETWORK;
            }
        }
        if (str == null || dVar.w() == null) {
            return 0;
        }
        int intValue = Integer.valueOf(dVar.w()).intValue();
        mm.purchasesdk.l.e.c(TAG, "billing code:" + intValue);
        if ((intValue == 0 || intValue == 1) && !a.a(str).booleanValue()) {
            PurchaseCode.setStatusCode(PurchaseCode.RESPONSE_ERR);
            return PurchaseCode.RESPONSE_ERR;
        }
        switch (intValue) {
            case 0:
                String b = mm.purchasesdk.a.e.b(str);
                if (b == null || b.length() == 0) {
                    mm.purchasesdk.l.e.e(TAG, "no license file");
                    return d.K().equals("3") ? PurchaseCode.BILL_THIRDTYPE_PAY : PurchaseCode.AUTH_PARSE_FAIL;
                }
                int a2 = mm.purchasesdk.a.e.a(b, mm.purchasesdk.e.b.a().f3140a.ab, dVar.k());
                if (a2 != 104) {
                    return a2;
                }
                bundle.putString(OnPurchaseListener.ORDERID, dVar.k());
                bundle.putString(OnPurchaseListener.LEFTDAY, dVar.b());
                bundle.putString(OnPurchaseListener.ORDERTYPE, dVar.n());
                return a2;
            case 1:
                return PurchaseCode.BILL_CHECKCODE_ERROR;
            case 11:
                return PurchaseCode.BILL_INVALID_SESSION;
            case 12:
                return PurchaseCode.BILL_CSSP_BUSY;
            case 13:
                return PurchaseCode.BILL_INTERNAL_FAIL;
            case h.DragSortListView_drag_handle_id:
                try {
                    MMClientSDK_ForPhone.DestorySecCert(null);
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
                return PurchaseCode.BILL_INVALID_USER;
            case 15:
                return PurchaseCode.BILL_INVALID_APP;
            case 16:
                return PurchaseCode.BILL_LICENSE_ERROR;
            case 17:
                return PurchaseCode.BILL_INVALID_SIGN;
            case 18:
                return PurchaseCode.BILL_NO_ABILITY;
            case 19:
                return PurchaseCode.BILL_NO_APP;
            case VMapProjection.MAXZOOMLEVEL:
                return PurchaseCode.BILL_PWD_DISMISS;
            case Constants.WEIBO_SDK_VERSION:
                return PurchaseCode.BILL_INVALID_CERT;
            case 25:
                return PurchaseCode.BILL_CERT_LIMIT;
            case 36:
                return PurchaseCode.BILL_PW_FAIL;
            case 38:
                return PurchaseCode.BILL_SMSCODE_ERROR;
            case 42:
                return PurchaseCode.BILL_DYMARK_ERROR;
            case 99:
                return PurchaseCode.BILL_FORBID_ORDER;
            case PurchaseCode.QUERY_OK:
                return PurchaseCode.BILL_NO_BUSINESS;
            case 106:
                return PurchaseCode.BILL_NO_ORDER;
            case 107:
                return PurchaseCode.BILL_ORDERED;
            case 182:
                return PurchaseCode.BILL_USERINFO_CLOSE;
            case 201:
                return PurchaseCode.BILL_OVER_COMSUMPTION;
            case 202:
                return PurchaseCode.BILL_OVER_LIMIT;
            case 2008:
                return PurchaseCode.BILL_INSUFFICIENT_FUNDS;
            case 9019:
                return PurchaseCode.BILL_PARAM_ERROR;
            default:
                return intValue;
        }
    }
}
