package mm.purchasesdk.b;

import java.util.ArrayList;
import java.util.HashMap;
import mm.purchasesdk.ui.ab;

public class e {
    private String R;

    /* renamed from: a  reason: collision with root package name */
    private ArrayList f3128a;
    private HashMap b;
    private int e;

    public void A(String str) {
        this.R = str;
    }

    public void B(String str) {
        if (str != null && str.length() != 0) {
            if (this.f3128a == null) {
                this.f3128a = new ArrayList();
            }
            this.f3128a.add(str);
        }
    }

    public ArrayList a() {
        return this.f3128a;
    }

    /* renamed from: a  reason: collision with other method in class */
    public HashMap m274a() {
        return this.b;
    }

    public void a(String str, ab abVar) {
        if (str != null && abVar != null && str.trim().length() != 0) {
            if (this.b == null) {
                this.b = new HashMap();
            }
            this.b.put(str, abVar);
        }
    }

    public void d(int i) {
        this.e = i;
    }

    public String o() {
        return this.R;
    }
}
