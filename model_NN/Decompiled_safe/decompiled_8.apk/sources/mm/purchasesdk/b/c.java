package mm.purchasesdk.b;

import android.util.Xml;
import com.amap.mapapi.poisearch.PoiTypeDef;
import java.io.StringWriter;
import mm.purchasesdk.PurchaseCode;
import mm.purchasesdk.fingerprint.IdentifyApp;
import mm.purchasesdk.h.e;
import mm.purchasesdk.l.d;
import org.xmlpull.v1.XmlSerializer;

public class c extends e {
    private String G;
    private String H;
    private String I;
    private String J = " ";
    private String K = "0";
    private final String TAG = c.class.getSimpleName();
    private int count = 1;
    private boolean e = false;
    private boolean h = false;
    private boolean i = false;
    private String p;
    private String r;
    private int statusCode;

    public String a() {
        XmlSerializer newSerializer = Xml.newSerializer();
        StringWriter stringWriter = new StringWriter();
        try {
            newSerializer.setOutput(stringWriter);
            newSerializer.startDocument("UTF-8", true);
            newSerializer.startTag(PoiTypeDef.All, "Trusted3SubscribeReq");
            newSerializer.startTag(PoiTypeDef.All, "MsgType");
            newSerializer.text("Trusted3SubscribeReq");
            newSerializer.endTag(PoiTypeDef.All, "MsgType");
            newSerializer.startTag(PoiTypeDef.All, "Version");
            newSerializer.text(d.Z());
            newSerializer.endTag(PoiTypeDef.All, "Version");
            newSerializer.startTag(PoiTypeDef.All, "SessionID");
            newSerializer.text(this.p);
            newSerializer.endTag(PoiTypeDef.All, "SessionID");
            newSerializer.startTag(PoiTypeDef.All, "AppID");
            newSerializer.text(d.F());
            newSerializer.endTag(PoiTypeDef.All, "AppID");
            newSerializer.startTag(PoiTypeDef.All, "BracketID");
            newSerializer.text(PoiTypeDef.All);
            newSerializer.endTag(PoiTypeDef.All, "BracketID");
            newSerializer.startTag(PoiTypeDef.All, "PayCode");
            newSerializer.text(d.H());
            newSerializer.endTag(PoiTypeDef.All, "PayCode");
            newSerializer.startTag(PoiTypeDef.All, "CheckID");
            newSerializer.text(this.r);
            newSerializer.endTag(PoiTypeDef.All, "CheckID");
            newSerializer.startTag(PoiTypeDef.All, "CheckAnswer");
            newSerializer.text(this.G);
            newSerializer.endTag(PoiTypeDef.All, "CheckAnswer");
            newSerializer.startTag(PoiTypeDef.All, "PayPwd");
            newSerializer.text(IdentifyApp.encryptPassword(this.H, this.p));
            newSerializer.endTag(PoiTypeDef.All, "PayPwd");
            newSerializer.startTag(PoiTypeDef.All, "SubsNumb");
            newSerializer.text(String.valueOf(this.count));
            newSerializer.endTag(PoiTypeDef.All, "SubsNumb");
            newSerializer.startTag(PoiTypeDef.All, "RandomPwd");
            newSerializer.text(m());
            newSerializer.endTag(PoiTypeDef.All, "RandomPwd");
            newSerializer.startTag(PoiTypeDef.All, "DynamicMark");
            newSerializer.text(this.I);
            newSerializer.endTag(PoiTypeDef.All, "DynamicMark");
            newSerializer.startTag(PoiTypeDef.All, "NetInfo");
            newSerializer.text(d.Q());
            newSerializer.endTag(PoiTypeDef.All, "NetInfo");
            newSerializer.startTag(PoiTypeDef.All, "programHash");
            newSerializer.text(PoiTypeDef.All);
            newSerializer.endTag(PoiTypeDef.All, "programHash");
            newSerializer.startTag(PoiTypeDef.All, "imsi");
            newSerializer.text(d.L());
            newSerializer.endTag(PoiTypeDef.All, "imsi");
            newSerializer.startTag(PoiTypeDef.All, "imei");
            newSerializer.text(d.M());
            newSerializer.endTag(PoiTypeDef.All, "imei");
            newSerializer.startTag(PoiTypeDef.All, "ChannelID");
            newSerializer.text(d.t());
            newSerializer.endTag(PoiTypeDef.All, "ChannelID");
            newSerializer.startTag(PoiTypeDef.All, "PayWay");
            newSerializer.text(d.K());
            newSerializer.endTag(PoiTypeDef.All, "PayWay");
            newSerializer.startTag(PoiTypeDef.All, "UserType");
            newSerializer.text(d.I());
            newSerializer.endTag(PoiTypeDef.All, "UserType");
            newSerializer.startTag(PoiTypeDef.All, "sidSignature");
            newSerializer.text(a().getUserSignature());
            newSerializer.endTag(PoiTypeDef.All, "sidSignature");
            newSerializer.endTag(PoiTypeDef.All, "Trusted3SubscribeReq");
            newSerializer.endDocument();
            return stringWriter.toString();
        } catch (Exception e2) {
            mm.purchasesdk.l.e.a(this.TAG, "create BillingRequest xml file failed!!", e2);
            this.statusCode = PurchaseCode.XML_EXCPTION_ERROR;
            return null;
        }
    }

    public void c(int i2) {
        this.count = i2;
    }

    public void c(boolean z) {
        this.e = z;
    }

    public void d(boolean z) {
        this.h = z;
    }

    public void e(boolean z) {
        this.i = z;
    }

    public void j(String str) {
        this.p = str;
    }

    public void l(String str) {
        this.r = str;
    }

    public String m() {
        return this.J;
    }

    public void u(String str) {
        this.J = str;
    }

    public void v(String str) {
        this.I = str;
    }

    public void w(String str) {
        this.G = str;
    }

    public void x(String str) {
        this.H = str;
    }
}
