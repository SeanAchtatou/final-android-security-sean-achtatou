package mm.purchasesdk.e;

import java.io.ByteArrayInputStream;
import mm.purchasesdk.PurchaseCode;
import mm.purchasesdk.l.d;
import mm.purchasesdk.l.e;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

public class a {
    private static final String TAG = a.class.getSimpleName();
    public String W;

    /* renamed from: a  reason: collision with root package name */
    public C0005a f3140a = null;

    /* renamed from: a  reason: collision with other field name */
    public b f257a = null;

    /* renamed from: mm.purchasesdk.e.a$a  reason: collision with other inner class name */
    public class C0005a {
        public String X;
        public String Y;
        public String Z;
        public String aa;
        public String ab;
        public String ac;

        public C0005a() {
        }
    }

    public class b {
        public String Z;
        public String aa;
        public String ac;
        public String ad;
        public String ae;
        public String af;

        public b() {
        }
    }

    public static a a(String str) {
        a aVar = new a();
        XmlPullParser newPullParser = XmlPullParserFactory.newInstance().newPullParser();
        newPullParser.setInput(new ByteArrayInputStream(str.getBytes()), "utf-8");
        for (int eventType = newPullParser.getEventType(); eventType != 1; eventType = newPullParser.next()) {
            switch (eventType) {
                case 2:
                    String name = newPullParser.getName();
                    if (name.compareToIgnoreCase("APPID") != 0) {
                        if (name.compareToIgnoreCase("contentSignature") != 0) {
                            if (name.compareToIgnoreCase("copyrightSignature") != 0) {
                                break;
                            } else {
                                b(aVar, newPullParser);
                                break;
                            }
                        } else {
                            a(aVar, newPullParser);
                            break;
                        }
                    } else {
                        aVar.W = newPullParser.nextText();
                        break;
                    }
            }
        }
        return aVar;
    }

    private static void a(a aVar, XmlPullParser xmlPullParser) {
        int eventType = xmlPullParser.getEventType();
        while (eventType != 1) {
            switch (eventType) {
                case 2:
                    String name = xmlPullParser.getName();
                    if (aVar.f3140a == null) {
                        aVar.getClass();
                        aVar.f3140a = new C0005a();
                    }
                    if (name.compareToIgnoreCase("time") != 0) {
                        if (name.compareToIgnoreCase("programID") != 0) {
                            if (name.compareToIgnoreCase("digestAlg") != 0) {
                                if (name.compareToIgnoreCase("digest") != 0) {
                                    if (name.compareToIgnoreCase("certificate") != 0) {
                                        if (name.compareToIgnoreCase("signature") != 0) {
                                            break;
                                        } else {
                                            aVar.f3140a.ac = xmlPullParser.nextText();
                                            break;
                                        }
                                    } else {
                                        aVar.f3140a.ab = xmlPullParser.nextText();
                                        break;
                                    }
                                } else {
                                    aVar.f3140a.aa = xmlPullParser.nextText();
                                    break;
                                }
                            } else {
                                aVar.f3140a.Z = xmlPullParser.nextText();
                                break;
                            }
                        } else {
                            aVar.f3140a.Y = xmlPullParser.nextText();
                            break;
                        }
                    } else {
                        aVar.f3140a.X = xmlPullParser.nextText();
                        break;
                    }
                case 3:
                    if (xmlPullParser.getName().compareToIgnoreCase("contentSignature") != 0) {
                        break;
                    } else {
                        return;
                    }
            }
            eventType = xmlPullParser.next();
        }
    }

    public static boolean a(String str, a aVar) {
        int i;
        int i2;
        int indexOf = str.indexOf("<APPID>");
        int indexOf2 = str.indexOf("</APPID>") + "</APPID>".length();
        e.c(TAG, "localCopyright-->" + str);
        if (indexOf >= 0) {
            if (indexOf2 >= "</APPID>".length() + "<APPID>".length() + 6) {
                String substring = str.substring(indexOf, indexOf2);
                e.c(TAG, "CIDC content is: " + substring);
                try {
                    i = mm.purchasesdk.fingerprint.a.a((int) PurchaseCode.AUTH_NO_ABILITY, aVar.f3140a.ab.trim().getBytes(), substring.trim().getBytes(), aVar.f3140a.ac.trim().getBytes());
                } catch (mm.purchasesdk.fingerprint.b e) {
                    e.c(TAG, e.getMessage());
                    i = 1;
                } catch (Exception e2) {
                    e.d(TAG, "base 64 decrypt license file failure");
                    i = 1;
                }
                e.c(TAG, "CIDC verifyWithCert result is: " + i);
                if (i != 0) {
                    return false;
                }
                String substring2 = str.substring(str.indexOf("<APPID>"), "</contentSignature>".length() + str.indexOf("</contentSignature>"));
                e.c(TAG, "TAAC content is: " + substring2);
                try {
                    i2 = mm.purchasesdk.fingerprint.a.a((int) PurchaseCode.AUTH_NO_ABILITY, d.V().getBytes(), substring2.trim().getBytes(), aVar.f257a.ac.trim().getBytes());
                } catch (mm.purchasesdk.fingerprint.b e3) {
                    e.e(TAG, e3.getMessage());
                    i2 = 1;
                } catch (Exception e4) {
                    e.d(TAG, "base 64 decrypt license file failure");
                    i2 = 1;
                }
                e.c(TAG, "TAAC verifyWithCert result is: " + i2);
                return i2 == 0;
            }
        }
        return false;
    }

    private static void b(a aVar, XmlPullParser xmlPullParser) {
        int eventType = xmlPullParser.getEventType();
        while (eventType != 1) {
            switch (eventType) {
                case 2:
                    String name = xmlPullParser.getName();
                    if (aVar.f257a == null) {
                        aVar.getClass();
                        aVar.f257a = new b();
                    }
                    if (name.compareToIgnoreCase("copyrightID") != 0) {
                        if (name.compareToIgnoreCase("copyRightType") != 0) {
                            if (name.compareToIgnoreCase("digestAlg") != 0) {
                                if (name.compareToIgnoreCase("digest") != 0) {
                                    if (name.compareToIgnoreCase("timestamp") != 0) {
                                        if (name.compareToIgnoreCase("signature") != 0) {
                                            break;
                                        } else {
                                            aVar.f257a.ac = xmlPullParser.nextText();
                                            break;
                                        }
                                    } else {
                                        aVar.f257a.af = xmlPullParser.nextText();
                                        break;
                                    }
                                } else {
                                    aVar.f257a.aa = xmlPullParser.nextText();
                                    break;
                                }
                            } else {
                                aVar.f257a.Z = xmlPullParser.nextText();
                                break;
                            }
                        } else {
                            aVar.f257a.ae = xmlPullParser.nextText();
                            break;
                        }
                    } else {
                        aVar.f257a.ad = xmlPullParser.nextText();
                        break;
                    }
                case 3:
                    if (xmlPullParser.getName().compareToIgnoreCase("copyrightSignature") != 0) {
                        break;
                    } else {
                        return;
                    }
            }
            eventType = xmlPullParser.next();
        }
    }
}
