package mm.purchasesdk.e;

import com.amap.mapapi.core.PoiItem;
import java.util.ArrayList;
import java.util.Iterator;
import mm.purchasesdk.PurchaseCode;
import mm.purchasesdk.g.c;
import mm.purchasesdk.l.d;
import mm.purchasesdk.l.e;

public class b {
    private static final String TAG = b.class.getSimpleName();
    private static a b = null;

    private static int a(String str, c cVar) {
        try {
            a a2 = a.a(str);
            if (a2 == null) {
                return PurchaseCode.COPYRIGHT_PARSE_ERR;
            }
            if (!a.a(str, a2)) {
                e.e(TAG, "copyright verify failed, code=233");
                return PurchaseCode.COPYRIGHT_VALIDATE_FAIL;
            }
            e.c(TAG, "Copyright validate success");
            if (a2.W.equals(d.F()) || a2.W.equals("100000000000")) {
                if (!(cVar instanceof mm.purchasesdk.g.b) && d.e()) {
                    new mm.purchasesdk.g.b().b(d.F(), str, d.L());
                }
                b = a2;
                return 0;
            }
            e.e(TAG, "appid not same: " + d.F() + PoiItem.DesSplit + a2.W);
            return PurchaseCode.COPYRIGHT_VALIDATE_FAIL;
        } catch (Exception e) {
            e.a(TAG, "copyright parse failed.", e);
            return PurchaseCode.COPYRIGHT_PARSE_ERR;
        }
    }

    public static a a() {
        return b;
    }

    public static int e() {
        if (b != null) {
            return 0;
        }
        ArrayList arrayList = new ArrayList();
        arrayList.add(new mm.purchasesdk.g.b());
        arrayList.add(new mm.purchasesdk.g.d());
        arrayList.add(new mm.purchasesdk.g.e());
        Iterator it = arrayList.iterator();
        int i = 0;
        while (it.hasNext()) {
            c cVar = (c) it.next();
            try {
                String s = cVar.s();
                if (s == null) {
                    continue;
                } else {
                    int a2 = a(s, cVar);
                    if (a2 == 0) {
                        return a2;
                    }
                    i = a2;
                }
            } catch (mm.purchasesdk.e e) {
                e.e(TAG, "load copyright failure: " + e.c);
                i = e.c;
            }
        }
        return i;
    }
}
