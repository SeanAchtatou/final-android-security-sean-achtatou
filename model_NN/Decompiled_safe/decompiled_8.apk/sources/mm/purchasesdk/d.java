package mm.purchasesdk;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

class d extends Handler {
    final /* synthetic */ Purchase b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    d(Purchase purchase, Looper looper) {
        super(looper);
        this.b = purchase;
    }

    public void handleMessage(Message message) {
        int i = message.arg1;
        b bVar = (b) message.obj;
        switch (message.what) {
            case 0:
                f.a(this.b.f226a, this.b.b, bVar, i);
                break;
            case 1:
                f.a(bVar, i);
                break;
            case 2:
                f.b(this.b.f226a, this.b.b, bVar, i);
                break;
            case 3:
                bVar.onUnsubscribeFinish(i);
                break;
            case 4:
                f.b();
                break;
            case 5:
                bVar.a();
                break;
        }
        super.handleMessage(message);
    }
}
