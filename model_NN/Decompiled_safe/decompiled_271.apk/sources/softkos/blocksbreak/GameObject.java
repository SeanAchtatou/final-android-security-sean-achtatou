package softkos.blocksbreak;

public class GameObject {
    int dest_px;
    int dest_py;
    int height;
    int origin_x;
    int origin_y;
    int owner_id = -1;
    int px;
    int py;
    boolean visible = true;
    int width;

    public void setOriginPos(int x, int y) {
        this.origin_x = x;
        this.origin_y = y;
    }

    public boolean checkSelection(int x, int y) {
        if (!this.visible) {
            return false;
        }
        return x > getPx() && x < getPx() + getWidth() && y > getPy() && y < getPy() + getHeight();
    }

    public int getOriginX() {
        return this.origin_x;
    }

    public int getOriginY() {
        return this.origin_y;
    }

    public boolean isVisible() {
        return this.visible;
    }

    public void hide() {
        this.visible = false;
    }

    public void show() {
        this.visible = true;
    }

    public void setOwnerID(int id) {
        this.owner_id = id;
    }

    public int getOwnerID() {
        return this.owner_id;
    }

    public int getPx() {
        return this.px;
    }

    public int getPy() {
        return this.py;
    }

    public void SetPos(int x, int y) {
        this.px = x;
        this.py = y;
    }

    public int getWidth() {
        return this.width;
    }

    public int getHeight() {
        return this.height;
    }

    public void setSize(int w, int h) {
        this.width = w;
        this.height = h;
    }

    public int getDestPx() {
        return this.dest_px;
    }

    public int getDestPy() {
        return this.dest_py;
    }

    public void setDestPos(int x, int y) {
        this.dest_px = x;
        this.dest_py = y;
    }
}
