package softkos.blocksbreak;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import java.io.IOException;
import java.util.ArrayList;
import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

public class OnlineScores {
    static OnlineScores instance = null;
    Button closeEnterNameButton = null;
    Button closeScoresDlg = null;
    Dialog enterNameDlg = null;
    String gamename = MainActivity.getInstance().getPackageName();
    String[] names = new String[500];
    Button nextPage = null;
    Button okEnterNameButton = null;
    int page = 0;
    Button prevPage = null;
    int score_count;
    int[] scores = new int[500];
    Dialog scoresDlg = null;
    TextView[] scoresView = null;

    public OnlineScores() {
        for (int i = 0; i < this.names.length; i++) {
            this.scores[i] = 0;
            this.names[i] = null;
        }
    }

    public boolean getScores() {
        this.score_count = 0;
        try {
            HttpEntity resEntityGet = new DefaultHttpClient().execute(new HttpGet("http://mobilsoft.pl/online_scores/" + this.gamename)).getEntity();
            if (resEntityGet != null) {
                parseScores(EntityUtils.toString(resEntityGet));
                return true;
            }
        } catch (Exception e) {
        }
        return true;
    }

    public void parseScores(String data) {
        String[] split = data.split(";");
        int j = 0;
        for (int i = 0; i < split.length - 1; i += 2) {
            this.names[j] = split[i];
            this.scores[j] = Integer.parseInt(split[i + 1]);
            j++;
            this.score_count++;
        }
    }

    public void networkError() {
        AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.getInstance()).create();
        alertDialog.setTitle("Error");
        alertDialog.setMessage("Network error. Check if you are connected to WIFI or GPRS/2g/3g network");
        alertDialog.setButton("close", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        alertDialog.setIcon((int) R.drawable.icon);
        alertDialog.show();
    }

    public void sendNewScores(String name) {
        int score = Vars.getInstance().getScore();
        getScores();
        if (this.score_count == 0) {
            networkError();
            return;
        }
        int position = -1;
        int i = 0;
        while (true) {
            if (i >= this.names.length) {
                break;
            } else if (score < this.scores[i]) {
                i++;
            } else {
                position = i;
                for (int j = this.names.length - 1; j > i; j--) {
                    this.names[j] = this.names[j - 1];
                    this.scores[j] = this.scores[j - 1];
                }
                this.names[i] = name;
                this.scores[i] = score;
            }
        }
        if (position != -1) {
            sendScores();
            this.page = position / 10;
        }
        showScoresDialog(this.page);
    }

    public String prepareScoresToSend() {
        String res = "";
        for (int i = 0; i < this.names.length; i++) {
            if (this.names[i] != null) {
                res = String.valueOf(res) + this.names[i] + ";" + this.scores[i] + ";";
            }
        }
        if (res.length() < 1) {
            return null;
        }
        return res;
    }

    public void sendScores() {
        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://mobilsoft.pl/online_scores/online_score.php");
        try {
            ArrayList<BasicNameValuePair> nameValuePairs = new ArrayList<>(2);
            nameValuePairs.add(new BasicNameValuePair("game", this.gamename));
            String score_to_send = prepareScoresToSend();
            if (score_to_send != null) {
                nameValuePairs.add(new BasicNameValuePair("data", score_to_send));
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                httpclient.execute(httppost);
            }
        } catch (IOException | Exception | ClientProtocolException e) {
        }
    }

    public static OnlineScores getInstance() {
        if (instance == null) {
            instance = new OnlineScores();
        }
        return instance;
    }

    public Dialog getScoresDialog() {
        return this.scoresDlg;
    }

    public void fillScoreDialog() {
        int i = this.page * 10;
        for (int j = 0; j < 10; j++) {
            if (this.names[i + j] == null) {
                this.scoresView[j].setText((i + j + 1) + ". Empty: -");
            } else {
                this.scoresView[j].setText((i + j + 1) + ". " + this.names[i + j] + ": " + this.scores[i + j]);
            }
        }
    }

    public void updateScoresDialog(int p) {
        if (p < 0) {
            this.page--;
        }
        if (this.page < 0) {
            this.page = 0;
        }
        if (p > 0) {
            this.page++;
        }
        if (this.page >= this.names.length / 10) {
            this.page = (this.names.length / 10) - 1;
        }
        fillScoreDialog();
    }

    public void showScoresDialog(int p) {
        if (p < 0) {
            getScores();
            if (this.score_count == 0) {
                networkError();
                return;
            }
        }
        if (p >= 0) {
            this.page = p;
        } else {
            this.page = 0;
        }
        if (this.page < 0) {
            this.page = 0;
        }
        if (this.page >= this.names.length / 10) {
            this.page = (this.names.length / 10) - 1;
        }
        this.scoresDlg = new Dialog(MainActivity.getInstance());
        this.scoresDlg.requestWindowFeature(1);
        this.scoresDlg.setContentView((int) R.layout.online_scores);
        this.scoresView = new TextView[10];
        this.scoresView[0] = (TextView) this.scoresDlg.findViewById(R.id.score_pos0);
        this.scoresView[1] = (TextView) this.scoresDlg.findViewById(R.id.score_pos1);
        this.scoresView[2] = (TextView) this.scoresDlg.findViewById(R.id.score_pos2);
        this.scoresView[3] = (TextView) this.scoresDlg.findViewById(R.id.score_pos3);
        this.scoresView[4] = (TextView) this.scoresDlg.findViewById(R.id.score_pos4);
        this.scoresView[5] = (TextView) this.scoresDlg.findViewById(R.id.score_pos5);
        this.scoresView[6] = (TextView) this.scoresDlg.findViewById(R.id.score_pos6);
        this.scoresView[7] = (TextView) this.scoresDlg.findViewById(R.id.score_pos7);
        this.scoresView[8] = (TextView) this.scoresDlg.findViewById(R.id.score_pos8);
        this.scoresView[9] = (TextView) this.scoresDlg.findViewById(R.id.score_pos9);
        fillScoreDialog();
        this.scoresDlg.show();
        this.nextPage = (Button) this.scoresDlg.findViewById(R.id.scores_next_page);
        this.nextPage.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                OnlineScores.getInstance().updateScoresDialog(1);
            }
        });
        this.prevPage = (Button) this.scoresDlg.findViewById(R.id.scores_prev_page);
        this.prevPage.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                OnlineScores.getInstance().updateScoresDialog(-1);
            }
        });
        this.closeScoresDlg = (Button) this.scoresDlg.findViewById(R.id.closescores);
        this.closeScoresDlg.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                OnlineScores.getInstance().getScoresDialog().dismiss();
            }
        });
    }

    public Dialog getEnterNameDialog() {
        return this.enterNameDlg;
    }

    public void enterNameDialog() {
        this.enterNameDlg = new Dialog(MainActivity.getInstance());
        this.enterNameDlg.setContentView((int) R.layout.enter_name);
        this.enterNameDlg.setTitle("Enter your name");
        this.enterNameDlg.show();
        this.okEnterNameButton = (Button) this.enterNameDlg.findViewById(R.id.okentername);
        this.okEnterNameButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                EditText et = (EditText) OnlineScores.this.enterNameDlg.findViewById(R.id.editname);
                if (et.getText().length() > 0) {
                    OnlineScores.getInstance().sendNewScores(et.getText().toString());
                }
                OnlineScores.getInstance().getEnterNameDialog().dismiss();
            }
        });
        this.closeEnterNameButton = (Button) this.enterNameDlg.findViewById(R.id.cancelentername);
        this.closeEnterNameButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                OnlineScores.getInstance().getEnterNameDialog().dismiss();
            }
        });
    }
}
