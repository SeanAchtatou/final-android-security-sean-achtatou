package softkos.blocksbreak;

import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.net.Uri;
import android.view.MotionEvent;
import android.view.View;

public class MenuCanvas extends View {
    static MenuCanvas instance = null;
    gButton howPlayBtn = null;
    gButton mobilsoftBtn = null;
    gButton otherAppBtn = null;
    Paint paint = new Paint();
    PaintManager paintMgr;
    gButton startGame = null;
    gButton twitterBtn = null;
    Vars vars;

    public MenuCanvas(Context c) {
        super(c);
        instance = this;
        this.vars = Vars.getInstance();
        this.paintMgr = PaintManager.getInstance();
        initUI();
    }

    public void showUI(boolean show) {
        for (int i = 0; i < this.vars.getButtonListSize(); i++) {
            this.vars.getButton(i).hide();
        }
        if (show) {
            this.startGame.show();
            this.mobilsoftBtn.show();
            this.howPlayBtn.show();
            this.twitterBtn.show();
            this.otherAppBtn.show();
        }
    }

    public void initUI() {
        int btn_w = 260;
        int btn_h = 50;
        if (this.vars.getScreenWidth() > 400) {
            btn_w = 380;
            btn_h = 70;
        }
        this.startGame = new gButton();
        this.startGame.setSize(btn_w, btn_h);
        this.startGame.setId(Vars.getInstance().START_GAME);
        this.startGame.show();
        this.startGame.setText("Play");
        this.startGame.setBackImages("240x50off", "240x50on");
        Vars.getInstance().addButton(this.startGame);
        this.howPlayBtn = new gButton();
        this.howPlayBtn.setSize(btn_w, btn_h);
        this.howPlayBtn.setId(Vars.getInstance().HOWTOPLAY);
        this.howPlayBtn.show();
        this.howPlayBtn.setText("How to play");
        this.howPlayBtn.setBackImages("240x50off", "240x50on");
        Vars.getInstance().addButton(this.howPlayBtn);
        this.mobilsoftBtn = new gButton();
        this.mobilsoftBtn.setSize(btn_w, btn_h);
        this.mobilsoftBtn.setId(Vars.getInstance().GO_TO_MOBILSOFT);
        this.mobilsoftBtn.show();
        this.mobilsoftBtn.setText("More free apps");
        this.mobilsoftBtn.setBackImages("240x50off", "240x50on");
        Vars.getInstance().addButton(this.mobilsoftBtn);
        this.twitterBtn = new gButton();
        this.twitterBtn.setSize(btn_w, btn_h);
        this.twitterBtn.setId(Vars.getInstance().TWITTER);
        this.twitterBtn.show();
        this.twitterBtn.setText("Online scores");
        this.twitterBtn.setImage("twitter");
        this.twitterBtn.setBackImages("240x50off", "240x50on");
        Vars.getInstance().addButton(this.twitterBtn);
        this.otherAppBtn = new gButton();
        this.otherAppBtn.setSize(btn_w, btn_h);
        this.otherAppBtn.setId(Vars.getInstance().OTHER_APP);
        this.otherAppBtn.show();
        this.otherAppBtn.setText("Test");
        this.otherAppBtn.setBackImages("240x50off", "240x50on");
        Vars.getInstance().addButton(this.otherAppBtn);
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int w, int h, int oldw, int oldh) {
        layoutUI();
    }

    public void layoutUI() {
        this.startGame.allowMove();
        this.howPlayBtn.allowMove();
        this.otherAppBtn.allowMove();
        this.twitterBtn.allowMove();
        this.mobilsoftBtn.allowMove();
        int xs = (getWidth() / 2) - (this.startGame.getWidth() / 2);
        int spacing = (int) (((double) this.startGame.getHeight()) * 1.05d);
        int ys = (getHeight() - (spacing * 5)) - 20;
        this.startGame.setPosition(xs, getHeight() + ((int) (((double) this.startGame.getHeight()) * 1.05d)));
        this.howPlayBtn.setPosition(xs, getHeight() + ((int) (((double) this.startGame.getHeight()) * 2.1d)));
        this.otherAppBtn.setPosition(xs, getHeight() + ((int) (((double) this.startGame.getHeight()) * 3.15d)));
        this.twitterBtn.setPosition(xs, getHeight() + ((int) (((double) this.startGame.getHeight()) * 4.2d)));
        this.mobilsoftBtn.setPosition(xs, getHeight() + ((int) (((double) this.startGame.getHeight()) * 5.25d)));
        this.startGame.setDestPos(xs, ys);
        this.howPlayBtn.setDestPos(xs, ys + spacing);
        this.otherAppBtn.setDestPos(xs, (spacing * 2) + ys);
        this.twitterBtn.setDestPos(xs, (spacing * 3) + ys);
        this.mobilsoftBtn.setDestPos(xs, (spacing * 4) + ys);
    }

    public void mouseDown(int x, int y) {
        int pressed = 0;
        for (int i = 0; i < Vars.getInstance().getButtonList().size(); i++) {
            pressed |= Vars.getInstance().getButton(i).mouseDown(x, y);
            if (pressed != 0) {
                invalidate();
                return;
            }
        }
    }

    public void mouseUp(int x, int y) {
        int pressed = 0;
        for (int i = 0; i < Vars.getInstance().getButtonList().size(); i++) {
            pressed |= Vars.getInstance().getButton(i).mouseUp(x, y);
            if (pressed != 0) {
                handleCommand(Vars.getInstance().getButton(i).getId());
                invalidate();
                return;
            }
        }
    }

    public void mouseDrag(int x, int y) {
        int pressed = 0;
        for (int i = 0; i < Vars.getInstance().getButtonList().size(); i++) {
            pressed |= Vars.getInstance().getButton(i).mouseDrag(x, y);
            if (pressed != 0) {
                invalidate();
                return;
            }
        }
    }

    public void handleCommand(int id) {
        if (id == Vars.getInstance().START_GAME) {
            MainActivity.getInstance().showGame();
        } else if (id == Vars.getInstance().GO_TO_MOBILSOFT) {
            MainActivity.getInstance().startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://www.mobilsoft.pl")));
        } else if (id == Vars.getInstance().HOWTOPLAY) {
            MainActivity.getInstance().showHowPlay();
        } else if (id == Vars.getInstance().OTHER_APP) {
            MainActivity.getInstance().startActivity(new Intent("android.intent.action.VIEW", Uri.parse(OtherApp.getInstance().urls[OtherApp.getInstance().current])));
        } else if (id == Vars.getInstance().TWITTER) {
            OnlineScores.getInstance().showScoresDialog(-1);
        }
    }

    public void updateButtonText() {
        this.otherAppBtn.setText(OtherApp.getInstance().button_text[OtherApp.getInstance().current]);
    }

    public boolean onTouchEvent(MotionEvent event) {
        int x = (int) event.getX();
        int y = (int) event.getY();
        if (event.getAction() == 0) {
            mouseDown(x, y);
        } else if (2 == event.getAction()) {
            mouseDrag(x, y);
        } else if (1 == event.getAction()) {
            mouseUp(x, y);
        }
        invalidate();
        return true;
    }

    public void drawBackBoard(Canvas canvas) {
        int curr_c;
        int curr_c2;
        int c1 = Color.rgb(122, 89, 63);
        int c2 = Color.rgb(163, 121, 91);
        int cs = getWidth() / 5;
        int csx = cs;
        int csy = cs;
        int row = 0;
        for (int y = 0; y < getHeight(); y += cs) {
            if (row % 2 == 0) {
                curr_c = c1;
            } else {
                curr_c = c2;
            }
            for (int x = 0; x < getWidth(); x += cs) {
                this.paintMgr.setColor(curr_c2);
                this.paintMgr.fillRectangle(canvas, x, y, csx, csy);
                if (curr_c2 == c2) {
                    curr_c2 = c1;
                } else {
                    curr_c2 = c2;
                }
            }
            row++;
        }
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        this.paint.setAntiAlias(true);
        drawBackBoard(canvas);
        this.paintMgr.drawImage(canvas, "gamename", 0, 0, getWidth(), getHeight());
        for (int i = 0; i < Vars.getInstance().getButtonList().size(); i++) {
            Vars.getInstance().getButton(i).draw(canvas, this.paint);
        }
        String img = OtherApp.getInstance().images[OtherApp.getInstance().current];
        this.paintMgr.drawImage(canvas, img, this.otherAppBtn.getPx() + 7, this.otherAppBtn.getPy() + 6, this.otherAppBtn.getHeight() - 12, this.otherAppBtn.getHeight() - 12);
        this.paintMgr.setTextSize(18);
        this.paintMgr.setColor(-256);
        this.paintMgr.drawString(canvas, "Press menu for options  ", 0, getHeight() - 20, getWidth(), 20, PaintManager.STR_RIGHT);
    }

    public static MenuCanvas getInstance() {
        return instance;
    }
}
