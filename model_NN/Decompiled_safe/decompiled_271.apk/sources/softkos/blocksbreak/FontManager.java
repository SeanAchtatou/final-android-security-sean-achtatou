package softkos.blocksbreak;

import android.graphics.Typeface;

public class FontManager {
    public static FontManager instance = null;
    Typeface mFace = null;

    public void init() {
        if (this.mFace == null) {
            this.mFace = Typeface.createFromAsset(MainActivity.getInstance().getBaseContext().getAssets(), "fonts/font.ttf");
        }
    }

    public Typeface getButtonFont() {
        return this.mFace;
    }

    public Typeface getFont() {
        return this.mFace;
    }

    public static FontManager getInstance() {
        if (instance == null) {
            instance = new FontManager();
            instance.init();
        }
        return instance;
    }
}
