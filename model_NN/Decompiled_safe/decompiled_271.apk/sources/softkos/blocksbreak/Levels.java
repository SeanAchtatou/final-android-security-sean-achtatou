package softkos.blocksbreak;

public class Levels {
    static Levels instance = null;

    public static Levels getInstance() {
        if (instance == null) {
            instance = new Levels();
        }
        return instance;
    }

    public void fillLevelData(int l) {
        int box_count = 1;
        int screenW = Vars.getInstance().getScreenWidth();
        int screenH = Vars.getInstance().getScreenHeight();
        int[] x = new int[50];
        int[] y = new int[50];
        int[] sizex = new int[50];
        int[] sizey = new int[50];
        int[] xspeed = new int[50];
        int[] yspeed = new int[50];
        int[] box_types = new int[50];
        if (l == 0) {
            box_count = 1;
            x[0] = screenW / 2;
            y[0] = screenH / 2;
            sizex[0] = screenW / 4;
            sizey[0] = screenW / 4;
            xspeed[0] = screenW / 200;
            yspeed[0] = (-screenH) / 200;
            box_types[0] = 0;
        }
        if (l == 1) {
            box_count = 1;
            x[0] = screenW / 2;
            y[0] = screenH / 2;
            sizex[0] = screenW / 8;
            sizey[0] = screenW / 8;
            xspeed[0] = screenW / 160;
            yspeed[0] = screenH / 160;
            box_types[0] = 2;
        }
        if (l == 2) {
            box_count = 2;
            x[0] = screenW / 2;
            x[1] = screenW / 2;
            y[0] = screenH / 2;
            y[1] = screenH / 2;
            sizex[0] = screenW / 6;
            sizex[1] = screenW / 8;
            sizey[0] = screenW / 6;
            sizey[1] = screenW / 8;
            xspeed[0] = screenW / 110;
            xspeed[1] = (-screenW) / 110;
            yspeed[0] = screenH / 130;
            yspeed[1] = screenH / 90;
            box_types[0] = 2;
            box_types[1] = 3;
        }
        if (l == 3) {
            box_count = 1;
            x[0] = screenW / 2;
            y[0] = screenH / 2;
            sizex[0] = screenW / 7;
            sizey[0] = screenW / 7;
            xspeed[0] = screenW / 80;
            yspeed[0] = screenH / 90;
            box_types[0] = 3;
        }
        if (l == 4) {
            box_count = 2;
            x[0] = screenW / 2;
            x[1] = screenW / 2;
            y[0] = screenH / 2;
            y[1] = screenH / 2;
            sizex[0] = screenW / 6;
            sizex[1] = screenW / 6;
            sizey[0] = screenW / 6;
            sizey[1] = screenW / 6;
            xspeed[0] = screenW / 80;
            xspeed[1] = 0;
            yspeed[0] = 0;
            yspeed[1] = screenH / 70;
            box_types[0] = 2;
            box_types[1] = 1;
        }
        for (int i = 0; i < box_count; i++) {
            Box b = new Box();
            b.SetPos(x[i], y[i]);
            b.setSize(sizex[i], sizey[i]);
            b.setSpeeds(xspeed[i], yspeed[i]);
            b.setType(box_types[i]);
            Vars.getInstance().getBoxList().add(b);
        }
    }
}
