package softkos.blocksbreak;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class GameCanvas extends View {
    static GameCanvas instance = null;
    boolean dialog_opened = false;
    Dialog gameOverDlg = null;
    Button gameOverNo = null;
    Button gameOverYes = null;
    int key_size = 32;
    int mouseDownX = 0;
    int mouseDownY = 0;
    int mouseX = -1;
    int mouseY = -1;
    Paint paint;
    PaintManager paintMgr;
    gButton playNextBtn = null;
    Vars vars;
    int vpx;
    int vpy;

    public GameCanvas(Context context) {
        super(context);
        instance = this;
        this.paint = new Paint();
        this.paintMgr = PaintManager.getInstance();
        this.vars = Vars.getInstance();
        initUI();
    }

    public void initUI() {
        int btn_w = 260;
        int btn_h = 50;
        if (this.vars.getScreenWidth() > 400) {
            btn_w = 380;
            btn_h = 70;
        }
        this.playNextBtn = new gButton();
        this.playNextBtn.setSize(btn_w, btn_h);
        this.playNextBtn.setId(Vars.getInstance().PLAY_NEXT_LEVEL);
        this.playNextBtn.hide();
        this.playNextBtn.setText("Next level");
        this.playNextBtn.setBackImages("240x50off", "240x50on");
        Vars.getInstance().addButton(this.playNextBtn);
    }

    public void showUI(boolean show) {
        this.vars.hideAllButtons();
    }

    public void layoutUI() {
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        layoutUI();
        this.key_size = getWidth() / 4;
        invalidate();
    }

    public void drawBackBoard(Canvas canvas) {
        int curr_c;
        int curr_c2;
        int c1 = Color.rgb(122, 89, 63);
        int c2 = Color.rgb(163, 121, 91);
        int cs = getWidth() / 5;
        int csx = cs;
        int csy = cs;
        int row = 0;
        for (int y = 0; y < getHeight(); y += cs) {
            if (row % 2 == 0) {
                curr_c = c1;
            } else {
                curr_c = c2;
            }
            for (int x = 0; x < getWidth(); x += cs) {
                this.paintMgr.setColor(curr_c2);
                this.paintMgr.fillRectangle(canvas, x, y, csx, csy);
                if (curr_c2 == c2) {
                    curr_c2 = c1;
                } else {
                    curr_c2 = c2;
                }
            }
            row++;
        }
    }

    public void drawLevelProgress(Canvas canvas) {
        int pw = getWidth();
        int ph = 50;
        if (pw < 400) {
            ph = 25;
        }
        int py = this.vars.getBoardPosY() - ((int) (1.2d * ((double) ph)));
        int oy = 4;
        if (pw > 400) {
            oy = 8;
        }
        int ox = 8;
        if (pw > 400) {
            ox = 16;
        }
        int level_progress = (int) (this.vars.getLevelProgress() * ((double) (pw - (ox * 2))));
        this.paintMgr.setColor(-16711936);
        Canvas canvas2 = canvas;
        this.paintMgr.fillRectangle(canvas2, ox + 0, py + oy, level_progress, ph - (oy * 2));
        this.paintMgr.drawImage(canvas, "progress", 0, py, pw, ph);
    }

    public void drawGame(Canvas canvas) {
        if (this.vars.isGameOver()) {
            showGameOverDlg();
        }
        this.paintMgr.setFont(FontManager.getInstance().getFont());
        drawBackBoard(canvas);
        drawLevelProgress(canvas);
        if (this.vars.getBox(0) != null) {
            int bw = this.vars.getBox(0).getWidth();
            int bh = this.vars.getBox(0).getHeight();
            int selected = this.vars.getSelectedBox();
            for (int b = this.vars.getBoxList().size() - 1; b >= 0; b--) {
                if (this.vars.getBox(b).isVisible()) {
                    this.paintMgr.drawImage(canvas, "box_" + this.vars.getBox(b).getType(), this.vars.getBox(b).getPx(), this.vars.getBox(b).getPy(), bw, bh);
                }
            }
            if (selected >= 0 && this.vars.getBox(selected) != null) {
                int off = this.vars.getBox(selected).getWidth() / 8;
                this.paintMgr.drawImage(canvas, "box_" + this.vars.getBox(selected).getType(), this.vars.getBox(selected).getPx() - off, this.vars.getBox(selected).getPy() - off, this.vars.getBox(selected).getWidth() + (off * 2), this.vars.getBox(selected).getHeight() + (off * 2));
            }
            this.paintMgr.setTextSize(18);
            this.paintMgr.setColor(-256);
            this.paintMgr.drawString(canvas, "Possible moves: " + this.vars.movePossible(), 0, this.vars.getBoardPosY() + (this.vars.getBoardHeight() * this.vars.getBox(0).getHeight()), getWidth(), 40, PaintManager.STR_CENTER);
        }
        for (int i = 0; i < this.vars.getGameStringList().size(); i++) {
            GameString s = this.vars.getGameString(i);
            this.paintMgr.setColor(s.getColor());
            this.paintMgr.setTextSize(s.getSize());
            this.paintMgr.drawString(canvas, s.getText(), s.getPx(), s.getPy(), s.getWidth(), s.getSize(), PaintManager.STR_CENTER);
        }
        this.paintMgr.setTextSize(24);
        this.paintMgr.setColor(-256);
        this.paintMgr.drawString(canvas, "Level: " + (this.vars.getLevel() + 1), 5, 10, getWidth(), 25, PaintManager.STR_LEFT);
        this.paintMgr.drawString(canvas, "Score: " + this.vars.getScore(), 5, 10, getWidth() - 10, 25, PaintManager.STR_RIGHT);
        for (int i2 = 0; i2 < this.vars.getStarList().size(); i2++) {
            this.paintMgr.drawImage(canvas, "star_0", this.vars.getStar(i2).getPx(), this.vars.getStar(i2).getPy(), this.vars.getStar(i2).getWidth(), this.vars.getStar(i2).getHeight());
        }
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        if (this.paint == null) {
            this.paint = new Paint();
        }
        if (canvas != null) {
            drawGame(canvas);
            for (int i = 0; i < Vars.getInstance().getButtonList().size(); i++) {
                Vars.getInstance().getButton(i).draw(canvas, this.paint);
            }
        }
    }

    public void mouseDown(int x, int y) {
        this.mouseDownX = x;
        this.mouseDownY = y;
        int pressed = 0;
        for (int i = 0; i < Vars.getInstance().getButtonList().size(); i++) {
            pressed |= Vars.getInstance().getButton(i).mouseDown(x, y);
            if (pressed != 0) {
                invalidate();
                return;
            }
        }
        this.vars.selectBoxAt(x, y);
    }

    public void mouseDrag(int x, int y) {
        this.mouseX = x;
        this.mouseY = y;
        int pressed = 0;
        for (int i = 0; i < Vars.getInstance().getButtonList().size(); i++) {
            pressed |= Vars.getInstance().getButton(i).mouseDrag(x, y);
            if (pressed != 0) {
                invalidate();
                return;
            }
        }
    }

    public void mouseUp(int x, int y) {
        this.mouseX = -1;
        this.mouseY = -1;
        int pressed = 0;
        for (int i = 0; i < Vars.getInstance().getButtonList().size(); i++) {
            pressed |= Vars.getInstance().getButton(i).mouseUp(x, y);
            if (pressed != 0) {
                handleCommand(Vars.getInstance().getButton(i).getId());
                invalidate();
                return;
            }
        }
    }

    public boolean onTouchEvent(MotionEvent event) {
        int x = (int) event.getX();
        int y = (int) event.getY();
        if (event.getAction() == 0) {
            mouseDown(x, y);
        } else if (2 == event.getAction()) {
            mouseDrag(x, y);
        } else if (1 == event.getAction()) {
            mouseUp(x, y);
        }
        invalidate();
        return true;
    }

    public void handleCommand(int id) {
        if (id == this.vars.PLAY_NEXT_LEVEL) {
            this.vars.nextLevel();
        }
    }

    public static GameCanvas getInstance() {
        return instance;
    }

    public Dialog getGameOverDialog() {
        return this.gameOverDlg;
    }

    public void closeGameOverDialog() {
        this.dialog_opened = false;
        getGameOverDialog().dismiss();
    }

    public void showGameOverDlg() {
        if (!this.dialog_opened) {
            this.dialog_opened = true;
            this.gameOverDlg = new Dialog(MainActivity.getInstance());
            this.gameOverDlg.requestWindowFeature(1);
            this.gameOverDlg.setContentView((int) R.layout.game_over);
            ((TextView) this.gameOverDlg.findViewById(R.id.gameover_msg)).setText("Your score is:\n\n" + this.vars.getScore() + "\n\nDo you want to submit it online?");
            this.gameOverDlg.show();
            this.gameOverYes = (Button) this.gameOverDlg.findViewById(R.id.gameover_yes);
            this.gameOverYes.setOnClickListener(new View.OnClickListener() {
                public void onClick(View arg0) {
                    GameCanvas.getInstance().closeGameOverDialog();
                    OnlineScores.getInstance().enterNameDialog();
                }
            });
            this.gameOverNo = (Button) this.gameOverDlg.findViewById(R.id.gameover_no);
            this.gameOverNo.setOnClickListener(new View.OnClickListener() {
                public void onClick(View arg0) {
                    GameCanvas.getInstance().closeGameOverDialog();
                }
            });
        }
    }
}
