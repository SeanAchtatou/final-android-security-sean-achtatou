package softkos.blocksbreak;

import android.graphics.Canvas;
import android.graphics.Paint;

public class gButton extends GameObject {
    boolean enabled;
    boolean hasKeyboardFocus;
    int id;
    String image;
    String image_off;
    String image_on;
    ImageLoader imgInst;
    boolean is_pressed;
    boolean mouseDownVal;
    boolean movable;
    PaintManager paintMgr;
    String text;
    boolean visible;

    public void allowMove() {
        this.movable = true;
    }

    public void setEnabled(boolean e) {
        this.enabled = e;
    }

    public void setKeyboardFocus(boolean f) {
        this.hasKeyboardFocus = f;
    }

    public void setBackImages(String off, String on) {
        this.image_off = off;
        this.image_on = on;
    }

    public boolean getKeyboardFocus() {
        return this.hasKeyboardFocus;
    }

    public void setImage(String img) {
        this.image = img;
    }

    public gButton() {
        this.imgInst = null;
        this.image = null;
        this.enabled = true;
        this.movable = false;
        this.image_off = "";
        this.image_on = "";
        this.is_pressed = false;
        this.mouseDownVal = false;
        this.text = "";
        this.id = -1;
        this.image = null;
        this.imgInst = ImageLoader.getInstance();
        this.paintMgr = PaintManager.getInstance();
    }

    public void setText(String t) {
        this.text = t;
    }

    public String getText() {
        return this.text;
    }

    public void setId(int i) {
        this.id = i;
    }

    public int getId() {
        return this.id;
    }

    public void show() {
        this.visible = true;
    }

    public void hide() {
        this.visible = false;
    }

    public void setPosition(int x, int y) {
        this.px = x;
        this.py = y;
    }

    public void setSize(int w, int h) {
        this.width = w;
        this.height = h;
    }

    public void draw(Canvas g, Paint p) {
        if (this.visible) {
            if (!this.is_pressed || !this.mouseDownVal) {
                this.paintMgr.drawImage(g, this.image_off, this.px, this.py, this.width, this.height);
            } else {
                this.paintMgr.drawImage(g, this.image_on, this.px, this.py, this.width, this.height);
            }
            if (this.image != null) {
                int y = this.py;
                int x = this.px;
                if (this.is_pressed && this.mouseDownVal) {
                    y++;
                    x++;
                }
                this.paintMgr.drawImage(g, this.image, x, y, this.width, this.height);
            }
            p.setTypeface(FontManager.getInstance().getButtonFont());
            p.setAntiAlias(true);
            p.setTextSize(24.0f);
            if (!this.is_pressed || !this.mouseDownVal) {
                p.setColor(-16777216);
            } else {
                p.setColor(-1);
            }
            float w = p.measureText(this.text);
            int y2 = this.py;
            int x2 = this.px;
            if (this.is_pressed && this.mouseDownVal) {
                y2++;
                x2++;
            }
            g.drawText(this.text, ((float) ((this.width / 2) + x2)) - (w / 2.0f), ((float) ((this.height / 2) + y2)) + (p.getTextSize() / 2.0f), p);
        }
    }

    public int mouseDrag(int x, int y) {
        if (!this.visible || !this.enabled) {
            return 0;
        }
        if (x <= this.px || y <= this.py || x >= this.px + this.width || y >= this.py + this.height) {
            this.is_pressed = false;
            return 0;
        }
        this.is_pressed = true;
        return 1;
    }

    public int mouseUp(int x, int y) {
        if (!this.visible || !this.enabled) {
            return 0;
        }
        int val = 0;
        if (x > this.px && y > this.py && x < this.px + this.width && y < this.py + this.height && this.is_pressed && this.mouseDownVal) {
            val = 1;
        }
        this.is_pressed = false;
        this.mouseDownVal = false;
        return val;
    }

    public int mouseDown(int x, int y) {
        if (!this.visible || !this.enabled) {
            return 0;
        }
        if (x <= this.px || y <= this.py || x >= this.px + this.width || y >= this.py + this.height) {
            return 0;
        }
        this.is_pressed = true;
        this.mouseDownVal = true;
        return 1;
    }

    public void updatePosition() {
        if (this.movable && getPy() != getDestPy()) {
            int speed = Math.abs(getDestPy() - getPy()) / 5;
            if (speed < 2) {
                speed = 2;
            }
            if (getPy() < getDestPy()) {
                SetPos(getPx(), getPy() + speed);
            }
            if (getPy() > getDestPy()) {
                SetPos(getPx(), getPy() - speed);
            }
        }
    }
}
