package softkos.blocksbreak;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import softkos.blocksbreak.Vars;

public class MainActivity extends Activity {
    static MainActivity instance;
    Button closeDlgButton = null;
    Button closeSettingsDlgButton = null;
    GameCanvas gameCanvas = null;
    RelativeLayout gameLayout = null;
    Button homepageBtn = null;
    Dialog howToPlayDlg = null;
    Vars.GameState lastState;
    MenuCanvas menuCanvas = null;
    Dialog settingsDlg = null;
    Button soundSettingBtn = null;

    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        instance = this;
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        Vars.getInstance().screenSize(dm.widthPixels, dm.heightPixels);
        ImageLoader.getInstance().LoadImages();
        getWindow().setFlags(1024, 1024);
        requestWindowFeature(1);
        SoundManager.getInstance();
        showMenu();
    }

    public void showMenu() {
        if (Vars.getInstance().gameState == Vars.GameState.Game && GameCanvas.getInstance() != null) {
            GameCanvas.getInstance().showGameOverDlg();
        }
        Vars.getInstance().gameState = Vars.GameState.Menu;
        if (this.menuCanvas == null) {
            this.menuCanvas = new MenuCanvas(this);
        }
        if (this.gameCanvas != null) {
            this.gameCanvas.showUI(false);
        }
        OtherApp.getInstance().random();
        this.menuCanvas.updateButtonText();
        this.menuCanvas.showUI(true);
        this.menuCanvas.layoutUI();
        setContentView(this.menuCanvas);
    }

    public void showSettings() {
        showSettingsDialog();
    }

    public Dialog getHowToPlayDlg() {
        return this.howToPlayDlg;
    }

    public void showHowPlay() {
        this.howToPlayDlg = new Dialog(this);
        this.howToPlayDlg.requestWindowFeature(1);
        this.howToPlayDlg.setContentView((int) R.layout.how_to_play);
        this.howToPlayDlg.show();
        this.closeDlgButton = (Button) this.howToPlayDlg.findViewById(R.id.close_dlg);
        this.closeDlgButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                MainActivity.getInstance().getHowToPlayDlg().dismiss();
            }
        });
    }

    public Dialog getSettingsDlg() {
        return this.settingsDlg;
    }

    public void showSettingsDialog() {
        String s;
        this.settingsDlg = new Dialog(this);
        this.settingsDlg.requestWindowFeature(1);
        this.settingsDlg.setContentView((int) R.layout.settings);
        this.settingsDlg.show();
        this.closeSettingsDlgButton = (Button) this.settingsDlg.findViewById(R.id.close_settings_dlg);
        this.closeSettingsDlgButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                MainActivity.getInstance().getSettingsDlg().dismiss();
            }
        });
        this.soundSettingBtn = (Button) this.settingsDlg.findViewById(R.id.sound_setting);
        if (Settings.getInstnace().getIntSettings(Settings.getInstnace().SOUND_DISABLED) != 0) {
            s = String.valueOf("Sound: ") + "OFF";
        } else {
            s = String.valueOf("Sound: ") + "ON";
        }
        this.soundSettingBtn.setText(s);
        this.soundSettingBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                String s;
                Settings.getInstnace().toggleSettings(Settings.getInstnace().SOUND_DISABLED);
                if (Settings.getInstnace().getIntSettings(Settings.getInstnace().SOUND_DISABLED) != 0) {
                    s = String.valueOf("Sound: ") + "OFF";
                } else {
                    s = String.valueOf("Sound: ") + "ON";
                }
                MainActivity.this.soundSettingBtn.setText(s);
            }
        });
        this.homepageBtn = (Button) this.settingsDlg.findViewById(R.id.home_page);
        this.homepageBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                MainActivity.getInstance().startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://www.mobilsoft.pl")));
            }
        });
    }

    public void showGame() {
        if (this.menuCanvas != null) {
            this.menuCanvas.showUI(false);
        }
        if (this.gameCanvas == null) {
            this.gameLayout = new RelativeLayout(this);
            this.gameLayout.setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
            this.gameCanvas = new GameCanvas(this);
            AdView adView = new AdView(this, AdSize.BANNER, "a14dddfae42b3b5");
            RelativeLayout.LayoutParams lparams = new RelativeLayout.LayoutParams(-1, -2);
            adView.setLayoutParams(lparams);
            lparams.addRule(12);
            this.gameLayout.addView(adView);
            this.gameLayout.addView(this.gameCanvas);
            adView.loadAd(new AdRequest());
            adView.bringToFront();
        }
        Vars.getInstance().gameState = Vars.GameState.Game;
        Vars.getInstance().newGame();
        this.gameCanvas.showUI(true);
        setContentView(this.gameLayout);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4 && (Vars.getInstance().gameState == Vars.GameState.Game || Vars.getInstance().gameState == Vars.GameState.Highscores || Vars.getInstance().gameState == Vars.GameState.HowPlay)) {
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyUp(keyCode, event);
        }
        if (Vars.getInstance().gameState == Vars.GameState.Game || Vars.getInstance().gameState == Vars.GameState.Highscores || Vars.getInstance().gameState == Vars.GameState.HowPlay) {
            showMenu();
        }
        return true;
    }

    public static MainActivity getInstance() {
        return instance;
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuItem add = menu.add(0, 2003, 0, "Settings");
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 2003:
                showSettings();
                return true;
            default:
                return false;
        }
    }
}
