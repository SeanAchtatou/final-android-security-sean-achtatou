package softkos.blocksbreak;

import android.graphics.Color;

public class GameString extends GameObject {
    int a;
    int b;
    int color = 16777215;
    int g;
    int r;
    int size = 20;
    String str = "";
    int timer = 50;

    public void setColor(int a1, int a2, int a3, int a4) {
        this.a = a1;
        this.r = a2;
        this.g = a3;
        this.b = a4;
    }

    public void setColor(int c) {
        this.color = c;
    }

    public int getColor() {
        return this.color;
    }

    public void setText(String s) {
        this.str = s;
    }

    public String getText() {
        return this.str;
    }

    public void setSize(int s) {
        this.size = s;
    }

    public int getSize() {
        return this.size;
    }

    public int getTimer() {
        return this.timer;
    }

    public void update() {
        this.timer--;
        SetPos(getPx(), getPy() - 2);
        this.color = Color.argb(this.a, this.r, this.g, this.b);
        this.a -= 6;
        if (this.a < 0) {
            this.a = 0;
            this.timer = -1;
        }
    }
}
