package softkos.blocksbreak;

import android.graphics.drawable.Drawable;

public class ImageLoader {
    static ImageLoader instance = null;
    String[] image_names = {"240x50off", "240x50on", "gamename", "gameback", "progress", "box_0", "box_1", "box_2", "box_3", "box_4", "box_5", "box_6", "box_7", "box_8", "star_0", "star_1", "star_2", "star_3", "star_4", "rotateme_ad", "pirate_ad", "tank_warriors_ad", "bomb_it_ad", "frogs_jump_ad", "untangle_ad", "cubix_ad", "turnmeon_ad", "boxit_ad", "moveme_ad"};
    Drawable[] images;
    int[] img_id = {R.drawable.btn240x50off, R.drawable.btn240x50on, R.drawable.gamename, R.drawable.gameback, R.drawable.progress, R.drawable.box_0, R.drawable.box_1, R.drawable.box_2, R.drawable.box_3, R.drawable.box_4, R.drawable.box_5, R.drawable.box_6, R.drawable.box_7, R.drawable.box_8, R.drawable.star_0, R.drawable.star_1, R.drawable.star_2, R.drawable.star_3, R.drawable.star_4, R.drawable.rotateme_ad, R.drawable.pirate_islands_ad, R.drawable.tank_warriors_ad, R.drawable.bomb_it_ad, R.drawable.frogs_jump_ad, R.drawable.untangle_ad, R.drawable.cubix_ad, R.drawable.turnmeon_ad, R.drawable.boxit_ad, R.drawable.ad_moveme};

    public static ImageLoader getInstance() {
        if (instance == null) {
            instance = new ImageLoader();
        }
        return instance;
    }

    public void LoadImages() {
        int len = this.img_id.length;
        this.images = new Drawable[this.image_names.length];
        for (int i = 0; i < len; i++) {
            this.images[i] = MainActivity.getInstance().getResources().getDrawable(this.img_id[i]);
        }
    }

    public Drawable getImage(String img) {
        for (int i = 0; i < this.image_names.length; i++) {
            if (img.equals(this.image_names[i])) {
                return this.images[i];
            }
        }
        return null;
    }
}
