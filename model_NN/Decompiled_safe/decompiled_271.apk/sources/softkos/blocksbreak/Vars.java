package softkos.blocksbreak;

import android.graphics.Point;
import android.os.Handler;
import android.os.Message;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Random;
import java.util.Timer;

public class Vars {
    public static final int UPDATE = 100;
    static Vars instance = null;
    public int GO_TO_MOBILSOFT;
    public int HOWTOPLAY;
    public int OTHER_APP;
    public int PLAY_NEXT_LEVEL;
    public int RESET_ALL_LEVELS;
    public int SOUND_SETTINGS;
    public int START_GAME;
    public int TWITTER;
    int[][] board;
    int board_h;
    int board_pos_x;
    int board_pos_y;
    int board_w;
    ArrayList<Box> boxList;
    ArrayList<gButton> buttonList;
    public GameState gameState;
    ArrayList<GameString> gameStringList;
    boolean game_over;
    int level;
    int[] level_borders;
    int level_progress;
    int max_level_progress;
    Handler messageHandler;
    public int painting;
    Random random;
    int scores;
    int[][] scores_board;
    int screenHeight;
    int screenWidth;
    int selectedBox;
    ArrayList<Star> starList;
    int target_level_progress;
    Timer updateTimer;

    public enum GameState {
        Menu,
        Game,
        Highscores,
        HowPlay,
        Settings
    }

    public Vars() {
        this.painting = 0;
        this.level = 0;
        this.scores = 0;
        this.board_w = 7;
        this.board_h = 7;
        this.buttonList = null;
        this.gameStringList = null;
        this.boxList = null;
        this.starList = null;
        this.messageHandler = null;
        this.game_over = false;
        this.SOUND_SETTINGS = 1001;
        this.RESET_ALL_LEVELS = 1002;
        this.START_GAME = 100;
        this.GO_TO_MOBILSOFT = 101;
        this.HOWTOPLAY = 102;
        this.TWITTER = 103;
        this.OTHER_APP = 104;
        this.PLAY_NEXT_LEVEL = 200;
        this.buttonList = new ArrayList<>();
        this.buttonList.clear();
        this.gameStringList = new ArrayList<>();
        this.gameStringList.clear();
        this.boxList = new ArrayList<>();
        this.boxList.clear();
        this.starList = new ArrayList<>();
        this.starList.clear();
        this.random = new Random();
        this.updateTimer = new Timer();
        this.updateTimer.schedule(new UpdateTimer(), 10, 50);
        this.board = (int[][]) Array.newInstance(Integer.TYPE, this.board_w, this.board_h);
        this.level = 0;
        this.messageHandler = new Handler() {
            public void handleMessage(Message msg) {
                if (msg.arg1 == 100) {
                    Vars.this.advanceFrame();
                }
            }
        };
    }

    public Handler getMessageHandler() {
        return this.messageHandler;
    }

    public void setBoardAt(int x, int y, int v) {
        if (x >= 0 && x < this.board_w && y >= 0 && y < this.board_h) {
            this.board[x][y] = v;
        }
    }

    public int getBoardWidth() {
        return this.board_w;
    }

    public int getBoardHeight() {
        return this.board_h;
    }

    public void clearDeadObjects() {
        for (int i = 0; i < this.boxList.size(); i++) {
            if (!getBox(i).isVisible() || getBox(i).getPy() > this.screenHeight) {
                this.boxList.remove(i);
            }
        }
        for (int i2 = 0; i2 < this.starList.size(); i2++) {
            if (getStar(i2).getPy() > this.screenHeight) {
                this.starList.remove(i2);
            }
        }
    }

    public void advanceFrame() {
        if (this.gameState == GameState.Menu || this.gameState == GameState.Game) {
            if (this.gameState == GameState.Game) {
                updateBoxes();
                checkEndLevel();
                for (int i = 0; i < this.starList.size(); i++) {
                    this.starList.get(i).update();
                }
                for (int i2 = 0; i2 < this.gameStringList.size(); i2++) {
                    this.gameStringList.get(i2).update();
                }
                checkLevelFailed();
                clearDeadObjects();
                clearGameStrings();
                if (this.level_progress < this.target_level_progress) {
                    this.level_progress++;
                    if (this.level_progress >= this.max_level_progress) {
                        nextLevel();
                    }
                }
                GameCanvas.getInstance().postInvalidate();
            }
            if (this.gameState == GameState.Menu) {
                for (int i3 = 0; i3 < this.buttonList.size(); i3++) {
                    getButton(i3).updatePosition();
                }
                MenuCanvas.getInstance().postInvalidate();
            }
        }
    }

    public void clearGameStrings() {
        for (int i = 0; i < this.gameStringList.size(); i++) {
            if (getGameString(i).getTimer() < 0) {
                this.gameStringList.remove(i);
            }
        }
    }

    public ArrayList<Star> getStarList() {
        return this.starList;
    }

    /* Debug info: failed to restart local var, previous not found, register: 1 */
    public Star getStar(int i) {
        if (i < 0 || i >= this.starList.size()) {
            return null;
        }
        return this.starList.get(i);
    }

    public void updateBoxes() {
        boolean moved = false;
        for (int i = 0; i < this.boxList.size(); i++) {
            moved |= getBox(i).update();
        }
        if (!moved) {
            addNewBoxes();
        }
    }

    public boolean checkLevelFailed() {
        return false;
    }

    public boolean checkEndLevel() {
        return true;
    }

    public void hideAllButtons() {
        for (int i = 0; i < this.buttonList.size(); i++) {
            getButton(i).hide();
        }
    }

    public int getBoardPosY() {
        return this.board_pos_y;
    }

    public int getBoardPosX() {
        return this.board_pos_x;
    }

    public void initLevel() {
        this.board = (int[][]) Array.newInstance(Integer.TYPE, this.board_w, this.board_h);
        this.starList.clear();
        this.max_level_progress = (getLevel() + 1) * 200;
        this.level_progress = 0;
        this.target_level_progress = 0;
        int boxSize = this.screenWidth / this.board_w;
        this.board_pos_y = (this.screenHeight / 2) - ((this.board_h * boxSize) / 2);
        this.board_pos_x = (this.screenWidth - (this.board_w * boxSize)) / 2;
        for (int y = 0; y < this.board_h; y++) {
            for (int x = 0; x < this.board_w; x++) {
                addBox(x, y);
            }
        }
    }

    public double getLevelProgress() {
        return ((double) this.level_progress) / ((double) this.max_level_progress);
    }

    public void addBox(int x, int y) {
        int boxSize = this.screenWidth / this.board_w;
        this.board[x][y] = nextInt(9);
        Box b = new Box();
        b.setSize(boxSize, boxSize);
        b.SetPos(this.board_pos_x + (x * boxSize), this.board_pos_y + (y * boxSize));
        b.setBoardPos(x, y);
        b.setType(this.board[x][y]);
        this.boxList.add(b);
    }

    public void nextLevel() {
        this.level++;
        for (int i = 0; i < this.boxList.size(); i++) {
            getBox(i).fall();
        }
        addGameString("Level UP!", 50, 255, 100, 255, 100, new Point(0, (this.screenHeight / 2) - 20));
        initLevel();
    }

    public int getScore() {
        return this.scores;
    }

    public int getBoardAt(int x, int y) {
        if (x < 0 || x >= this.board_w || y < 0 || y >= this.board_h) {
            return -1;
        }
        return this.board[x][y];
    }

    public double dist(int x1, int y1, int x2, int y2) {
        return Math.sqrt((double) (((x1 - x2) * (x1 - x2)) + ((y1 - y2) * (y1 - y2))));
    }

    public void setLevel(int l) {
        this.level = l;
    }

    public void prevLevel() {
        this.level--;
        if (this.level < 0) {
            this.level = 0;
        }
        initLevel();
    }

    public void resetLevel() {
        initLevel();
    }

    public int getLevel() {
        return this.level;
    }

    public void newGame() {
        this.scores = 0;
        this.game_over = false;
        this.level = 0;
        this.selectedBox = -1;
        initLevel();
    }

    public int getSelectedBox() {
        return this.selectedBox;
    }

    public void selectBoxAt(int x, int y) {
        int s = -1;
        int i = 0;
        while (true) {
            if (i < this.boxList.size()) {
                if (getBox(i).isVisible() && x >= getBox(i).getPx() && x < getBox(i).getPx() + getBox(i).getWidth() && y >= getBox(i).getPy() && y < getBox(i).getPy() + getBox(i).getHeight()) {
                    s = i;
                    break;
                }
                i++;
            } else {
                break;
            }
        }
        if (this.selectedBox < 0) {
            this.selectedBox = s;
        } else if (this.selectedBox == s) {
            this.selectedBox = -1;
        } else {
            hideBoxes(this.selectedBox, s);
        }
    }

    public void addNewBoxes() {
        for (int y = 0; y < this.board_h; y++) {
            for (int x = 0; x < this.board_w; x++) {
                if (this.board[x][y] < 0) {
                    addBox(x, y);
                }
            }
        }
    }

    public int movePossible() {
        int possible_moves = 0;
        for (int y = 0; y < this.board_h; y++) {
            for (int x = 0; x < this.board_w; x++) {
                int t = getBoardAt(x, y);
                if (t >= 0) {
                    int j = x + 3;
                    while (true) {
                        if (j >= this.board_w) {
                            break;
                        } else if (getBoardAt(j, y) == t) {
                            possible_moves++;
                            break;
                        } else {
                            j++;
                        }
                    }
                }
            }
        }
        for (int x2 = 0; x2 < this.board_w; x2++) {
            for (int y2 = 0; y2 < this.board_h; y2++) {
                int t2 = getBoardAt(x2, y2);
                if (t2 >= 0) {
                    int j2 = y2 + 3;
                    while (true) {
                        if (j2 >= this.board_h) {
                            break;
                        } else if (getBoardAt(x2, j2) == t2) {
                            possible_moves++;
                            break;
                        } else {
                            j2++;
                        }
                    }
                }
            }
        }
        if (possible_moves == 0) {
            gameOver();
        }
        return possible_moves;
    }

    public void addScores(int s, Point p) {
        int a = (s * 5) + getLevel();
        this.scores += a;
        this.target_level_progress += a;
        addGameString("+" + a, 34, 255, 255, 255, 255, p);
    }

    public void addStars(int x, int y) {
        for (int z = 0; z < 7; z++) {
            Star s = new Star();
            s.setSize(this.screenWidth / 12, this.screenWidth / 12);
            s.setPos(x, y);
            this.starList.add(s);
        }
    }

    public void hideBoxes(int b1, int b2) {
        if (b1 >= 0 && b2 >= 0) {
            if (getBox(b1).getType() != getBox(b2).getType()) {
                this.selectedBox = b2;
                return;
            }
            if (getBox(b1).getBoardPosY() == getBox(b2).getBoardPosY()) {
                int p1 = getBox(b1).getBoardPosX();
                int p2 = getBox(b2).getBoardPosX();
                int py = getBox(b1).getBoardPosY();
                if (Math.abs(p1 - p2) > 2) {
                    if (p1 > p2) {
                        int tmp = p1;
                        p1 = p2;
                        p2 = tmp;
                    }
                    int ax = 0;
                    int ay = 0;
                    for (int i = 0; i < this.boxList.size(); i++) {
                        if (getBox(i).getBoardPosY() == py && getBox(i).getBoardPosX() >= p1 && getBox(i).getBoardPosX() <= p2) {
                            getBox(i).hide();
                            setBoardAt(getBox(i).getBoardPosX(), 0, -1);
                            addStars(getBox(i).getPx(), getBox(i).getPy());
                            ax += getBox(i).getPx();
                            ay += getBox(i).getPy();
                        }
                    }
                    SoundManager.getInstance().playSound(1);
                    int sx = ax / (Math.abs(p2 - p1) + 1);
                    int sy = ay / (Math.abs(p2 - p1) + 1);
                    addScores(Math.abs(p2 - p1), new Point(sx, sy));
                    this.selectedBox = -1;
                    for (int i2 = 0; i2 < this.boxList.size(); i2++) {
                        if (getBox(i2).getBoardPosX() >= p1 && getBox(i2).getBoardPosX() <= p2 && getBox(i2).getBoardPosY() < py) {
                            getBox(i2).setBoardPos(getBox(i2).getBoardPosX(), getBox(i2).getBoardPosY() + 1);
                            getBox(i2).setDestPos(getBox(i2).getPx(), getBox(i2).getPy() + getBox(i2).getWidth());
                            getBox(i2).setSpeeds(0, 5);
                        }
                    }
                    return;
                }
                this.selectedBox = b2;
                return;
            }
            if (getBox(b1).getBoardPosX() == getBox(b2).getBoardPosX()) {
                int p12 = getBox(b1).getBoardPosY();
                int p22 = getBox(b2).getBoardPosY();
                int px = getBox(b1).getBoardPosX();
                if (Math.abs(p12 - p22) > 2) {
                    if (p12 > p22) {
                        int tmp2 = p12;
                        p12 = p22;
                        p22 = tmp2;
                    }
                    int ax2 = 0;
                    int ay2 = 0;
                    for (int i3 = 0; i3 < this.boxList.size(); i3++) {
                        if (getBox(i3).getBoardPosX() == px && getBox(i3).getBoardPosY() >= p12 && getBox(i3).getBoardPosY() <= p22) {
                            getBox(i3).hide();
                            addStars(getBox(i3).getPx(), getBox(i3).getPy());
                            ax2 += getBox(i3).getPx();
                            ay2 += getBox(i3).getPy();
                        }
                    }
                    SoundManager.getInstance().playSound(1);
                    int sx2 = ax2 / (Math.abs(p22 - p12) + 1);
                    int sy2 = ay2 / (Math.abs(p22 - p12) + 1);
                    addScores(Math.abs(p22 - p12), new Point(sx2, sy2));
                    int d = Math.abs(p22 - p12) + 1;
                    for (int i4 = 0; i4 < d; i4++) {
                        setBoardAt(getBox(b1).getBoardPosX(), i4, -1);
                    }
                    this.selectedBox = -1;
                    for (int i5 = 0; i5 < this.boxList.size(); i5++) {
                        if (getBox(i5).getBoardPosX() == px && getBox(i5).getBoardPosY() < p22) {
                            getBox(i5).setBoardPos(getBox(i5).getBoardPosX(), getBox(i5).getBoardPosY() + d);
                            getBox(i5).setDestPos(getBox(i5).getPx(), getBox(i5).getPy() + (getBox(i5).getWidth() * d));
                            getBox(i5).setSpeeds(0, 5);
                        }
                    }
                    return;
                }
                this.selectedBox = b2;
                return;
            }
            this.selectedBox = b2;
        }
    }

    public int getButtonListSize() {
        return this.buttonList.size();
    }

    public void addButton(gButton b) {
        this.buttonList.add(b);
    }

    public ArrayList<gButton> getButtonList() {
        return this.buttonList;
    }

    /* Debug info: failed to restart local var, previous not found, register: 1 */
    public gButton getButton(int b) {
        if (b < 0 || b >= this.buttonList.size()) {
            return null;
        }
        return this.buttonList.get(b);
    }

    public ArrayList<GameString> getGameStringList() {
        return this.gameStringList;
    }

    /* Debug info: failed to restart local var, previous not found, register: 1 */
    public GameString getGameString(int b) {
        if (b < 0 || b >= this.gameStringList.size()) {
            return null;
        }
        return this.gameStringList.get(b);
    }

    public void addGameString(String s, int size, int a, int r, int g, int b, Point p) {
        GameString gs = new GameString();
        gs.setText(s);
        gs.setSize(size);
        gs.setColor(a, r, g, b);
        gs.SetPos(p.x, p.y);
        gs.setSize(s.length() * ((int) (((double) size) * 0.8d)), size);
        this.gameStringList.add(gs);
    }

    public void addGameString(String s, int size, int color, Point p) {
        GameString gs = new GameString();
        gs.setText(s);
        gs.setSize(size);
        gs.setColor(color);
        gs.SetPos(p.x, p.y);
        gs.setSize(s.length() * ((int) (((double) size) * 0.8d)), size);
        this.gameStringList.add(gs);
    }

    /* Debug info: failed to restart local var, previous not found, register: 1 */
    public Box getBox(int i) {
        if (i < 0 || i >= this.boxList.size()) {
            return null;
        }
        return this.boxList.get(i);
    }

    public ArrayList<Box> getBoxList() {
        return this.boxList;
    }

    public int getScreenWidth() {
        return this.screenWidth;
    }

    public int getScreenHeight() {
        return this.screenHeight;
    }

    public void init() {
        this.random = new Random();
    }

    public int GetNextInt(int max) {
        if (max == 0) {
            max = 1;
        }
        return Math.abs(this.random.nextInt() % max);
    }

    public Random getRandom() {
        return this.random;
    }

    public void screenSize(int w, int h) {
        this.screenWidth = w;
        this.screenHeight = h;
    }

    public static Vars getInstance() {
        if (instance == null) {
            instance = new Vars();
            instance.init();
        }
        return instance;
    }

    public int nextInt(int max) {
        if (max < 1) {
            return 0;
        }
        return this.random.nextInt(max);
    }

    public void gameOver() {
        this.boxList.clear();
        this.game_over = true;
    }

    public boolean isGameOver() {
        return this.game_over;
    }
}
