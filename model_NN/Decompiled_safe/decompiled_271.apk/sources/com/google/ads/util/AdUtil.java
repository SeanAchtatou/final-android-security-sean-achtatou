package com.google.ads.util;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.location.Location;
import android.media.AudioManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.util.DisplayMetrics;
import android.webkit.WebView;
import com.google.ads.AdActivity;
import com.google.ads.AdRequest;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.URLDecoder;
import java.security.GeneralSecurityException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import softkos.blocksbreak.SoundManager;

public final class AdUtil {
    public static final int a;
    private static Boolean b = null;
    private static String c = null;
    private static String d;
    private static AudioManager e;
    private static boolean f = true;
    private static boolean g = false;
    private static String h = null;

    public static class UserActivityReceiver extends BroadcastReceiver {
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("android.intent.action.USER_PRESENT")) {
                AdUtil.a(true);
            } else if (intent.getAction().equals("android.intent.action.SCREEN_OFF")) {
                AdUtil.a(false);
            }
        }
    }

    public enum a {
        INVALID,
        SPEAKER,
        HEADPHONES,
        VIBRATE,
        EMULATOR,
        OTHER
    }

    static {
        int i;
        try {
            i = Integer.parseInt(Build.VERSION.SDK);
        } catch (NumberFormatException e2) {
            a.e("The Android SDK version couldn't be parsed to an int: " + Build.VERSION.SDK);
            a.e("Defaulting to Android SDK version 3.");
            i = 3;
        }
        a = i;
    }

    private AdUtil() {
    }

    public static int a() {
        return a >= 9 ? 6 : 0;
    }

    public static int a(Context context, DisplayMetrics displayMetrics) {
        return a >= 4 ? c.a(context, displayMetrics) : displayMetrics.heightPixels;
    }

    public static DisplayMetrics a(Activity activity) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics;
    }

    public static String a(Context context) {
        if (c == null) {
            String string = Settings.Secure.getString(context.getContentResolver(), "android_id");
            String a2 = (string == null || c()) ? a("emulator") : a(string);
            if (a2 == null) {
                return null;
            }
            c = a2.toUpperCase(Locale.US);
        }
        return c;
    }

    public static String a(Location location) {
        if (location == null) {
            return null;
        }
        return "e1+" + b(String.format("role: 6 producer: 24 historical_role: 1 historical_producer: 12 timestamp: %d latlng < latitude_e7: %d longitude_e7: %d> radius: %d", Long.valueOf(location.getTime() * 1000), Long.valueOf((long) (location.getLatitude() * 1.0E7d)), Long.valueOf((long) (location.getLongitude() * 1.0E7d)), Long.valueOf((long) (location.getAccuracy() * 1000.0f))));
    }

    public static String a(String str) {
        if (str == null || str.length() <= 0) {
            return null;
        }
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.update(str.getBytes(), 0, str.length());
            return String.format("%032X", new BigInteger(1, instance.digest()));
        } catch (NoSuchAlgorithmException e2) {
            return str.substring(0, 32);
        }
    }

    public static String a(Map<String, Object> map) {
        try {
            return b(map).toString();
        } catch (JSONException e2) {
            a.b("JsonException in serialization: ", e2);
            return null;
        }
    }

    private static JSONArray a(Set<Object> set) throws JSONException {
        JSONArray jSONArray = new JSONArray();
        if (set != null && !set.isEmpty()) {
            for (Object next : set) {
                if ((next instanceof String) || (next instanceof Integer) || (next instanceof Double) || (next instanceof Long) || (next instanceof Float)) {
                    jSONArray.put(next);
                } else if (next instanceof Map) {
                    try {
                        jSONArray.put(b((Map) next));
                    } catch (ClassCastException e2) {
                        a.b("Unknown map type in json serialization: ", e2);
                    }
                } else if (next instanceof Set) {
                    try {
                        jSONArray.put(a((Set) next));
                    } catch (ClassCastException e3) {
                        a.b("Unknown map type in json serialization: ", e3);
                    }
                } else {
                    a.e("Unknown value in json serialization: " + next);
                }
            }
        }
        return jSONArray;
    }

    public static void a(WebView webView) {
        webView.getSettings().setUserAgentString(h(webView.getContext().getApplicationContext()));
    }

    public static void a(HttpURLConnection httpURLConnection, Context context) {
        httpURLConnection.setRequestProperty("User-Agent", h(context));
    }

    public static void a(boolean z) {
        f = z;
    }

    public static boolean a(Uri uri) {
        String scheme = uri.getScheme();
        return "http".equalsIgnoreCase(scheme) || "https".equalsIgnoreCase(scheme);
    }

    public static int b() {
        return a >= 9 ? 7 : 1;
    }

    public static int b(Context context, DisplayMetrics displayMetrics) {
        return a >= 4 ? c.b(context, displayMetrics) : displayMetrics.widthPixels;
    }

    private static String b(String str) {
        try {
            Cipher instance = Cipher.getInstance("AES/CBC/PKCS5Padding");
            instance.init(1, new SecretKeySpec(new byte[]{10, 55, -112, -47, -6, 7, 11, 75, -7, -121, 121, 69, 80, -61, 15, 5}, "AES"));
            byte[] iv = instance.getIV();
            byte[] doFinal = instance.doFinal(str.getBytes());
            byte[] bArr = new byte[(iv.length + doFinal.length)];
            System.arraycopy(iv, 0, bArr, 0, iv.length);
            System.arraycopy(doFinal, 0, bArr, iv.length, doFinal.length);
            return b.a(bArr);
        } catch (GeneralSecurityException e2) {
            return null;
        }
    }

    public static HashMap<String, String> b(Uri uri) {
        if (uri == null) {
            return null;
        }
        HashMap<String, String> hashMap = new HashMap<>();
        String encodedQuery = uri.getEncodedQuery();
        if (encodedQuery == null) {
            return hashMap;
        }
        for (String str : encodedQuery.split("&")) {
            int indexOf = str.indexOf(61);
            if (indexOf == -1) {
                return null;
            }
            hashMap.put(URLDecoder.decode(str.substring(0, indexOf)), URLDecoder.decode(str.substring(indexOf + 1)));
        }
        return hashMap;
    }

    private static JSONObject b(Map<String, Object> map) throws JSONException {
        JSONObject jSONObject = new JSONObject();
        if (map == null || map.isEmpty()) {
            return jSONObject;
        }
        for (String next : map.keySet()) {
            Object obj = map.get(next);
            if ((obj instanceof String) || (obj instanceof Integer) || (obj instanceof Double) || (obj instanceof Long) || (obj instanceof Float)) {
                jSONObject.put(next, obj);
            } else if (obj instanceof Map) {
                try {
                    jSONObject.put(next, b((Map) obj));
                } catch (ClassCastException e2) {
                    a.b("Unknown map type in json serialization: ", e2);
                }
            } else if (obj instanceof Set) {
                try {
                    jSONObject.put(next, a((Set) obj));
                } catch (ClassCastException e3) {
                    a.b("Unknown map type in json serialization: ", e3);
                }
            } else {
                a.e("Unknown value in json serialization: " + obj);
            }
        }
        return jSONObject;
    }

    public static boolean b(Context context) {
        PackageManager packageManager = context.getPackageManager();
        String packageName = context.getPackageName();
        if (packageManager.checkPermission("android.permission.INTERNET", packageName) == -1) {
            a.b("INTERNET permissions must be enabled in AndroidManifest.xml.");
            return false;
        } else if (packageManager.checkPermission("android.permission.ACCESS_NETWORK_STATE", packageName) != -1) {
            return true;
        } else {
            a.b("ACCESS_NETWORK_STATE permissions must be enabled in AndroidManifest.xml.");
            return false;
        }
    }

    public static boolean c() {
        return "unknown".equals(Build.BOARD) && "generic".equals(Build.DEVICE) && "generic".equals(Build.BRAND);
    }

    public static boolean c(Context context) {
        boolean z;
        if (b != null) {
            return b.booleanValue();
        }
        ResolveInfo resolveActivity = context.getPackageManager().resolveActivity(new Intent(context, AdActivity.class), 65536);
        boolean z2 = true;
        if (resolveActivity == null || resolveActivity.activityInfo == null) {
            a.b("Could not find com.google.ads.AdActivity, please make sure it is registered in AndroidManifest.xml.");
            z = false;
        } else {
            if ((resolveActivity.activityInfo.configChanges & 16) == 0) {
                a.b("The android:configChanges value of the com.google.ads.AdActivity must include keyboard.");
                z2 = false;
            }
            if ((resolveActivity.activityInfo.configChanges & 32) == 0) {
                a.b("The android:configChanges value of the com.google.ads.AdActivity must include keyboardHidden.");
                z2 = false;
            }
            if ((resolveActivity.activityInfo.configChanges & 128) == 0) {
                a.b("The android:configChanges value of the com.google.ads.AdActivity must include orientation.");
                z = false;
            } else {
                z = z2;
            }
        }
        b = Boolean.valueOf(z);
        return z;
    }

    public static String d(Context context) {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetworkInfo == null) {
            return null;
        }
        switch (activeNetworkInfo.getType()) {
            case 0:
                return "ed";
            case SoundManager.SOUND_BLOCKS_BREAK /*1*/:
                return "wi";
            default:
                return "unknown";
        }
    }

    public static boolean d() {
        return f;
    }

    public static String e(Context context) {
        if (d == null) {
            StringBuilder sb = new StringBuilder();
            PackageManager packageManager = context.getPackageManager();
            List<ResolveInfo> queryIntentActivities = packageManager.queryIntentActivities(new Intent("android.intent.action.VIEW", Uri.parse("geo:0,0?q=donuts")), 65536);
            if (queryIntentActivities == null || queryIntentActivities.size() == 0) {
                sb.append(AdActivity.TYPE_PARAM);
            }
            List<ResolveInfo> queryIntentActivities2 = packageManager.queryIntentActivities(new Intent("android.intent.action.VIEW", Uri.parse("market://search?q=pname:com.google")), 65536);
            if (queryIntentActivities2 == null || queryIntentActivities2.size() == 0) {
                if (sb.length() > 0) {
                    sb.append(",");
                }
                sb.append("a");
            }
            List<ResolveInfo> queryIntentActivities3 = packageManager.queryIntentActivities(new Intent("android.intent.action.VIEW", Uri.parse("tel://6509313940")), 65536);
            if (queryIntentActivities3 == null || queryIntentActivities3.size() == 0) {
                if (sb.length() > 0) {
                    sb.append(",");
                }
                sb.append("t");
            }
            d = sb.toString();
        }
        return d;
    }

    public static a f(Context context) {
        if (e == null) {
            e = (AudioManager) context.getSystemService("audio");
        }
        a aVar = a.OTHER;
        int mode = e.getMode();
        if (c()) {
            return a.EMULATOR;
        }
        if (e.isMusicActive() || e.isSpeakerphoneOn() || mode == 2 || mode == 1) {
            return a.VIBRATE;
        }
        int ringerMode = e.getRingerMode();
        return (ringerMode == 0 || ringerMode == 1) ? a.VIBRATE : a.SPEAKER;
    }

    public static void g(Context context) {
        if (!g) {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("android.intent.action.USER_PRESENT");
            intentFilter.addAction("android.intent.action.SCREEN_OFF");
            context.registerReceiver(new UserActivityReceiver(), intentFilter);
            g = true;
        }
    }

    private static String h(Context context) {
        if (h == null) {
            String userAgentString = new WebView(context).getSettings().getUserAgentString();
            if (userAgentString == null || userAgentString.length() == 0 || userAgentString.equals("Java0")) {
                String property = System.getProperty("os.name", "Linux");
                String str = "Android " + Build.VERSION.RELEASE;
                Locale locale = Locale.getDefault();
                String lowerCase = locale.getLanguage().toLowerCase(Locale.US);
                if (lowerCase.length() == 0) {
                    lowerCase = "en";
                }
                String lowerCase2 = locale.getCountry().toLowerCase(Locale.US);
                userAgentString = "Mozilla/5.0 (" + property + "; U; " + str + "; " + (lowerCase2.length() > 0 ? lowerCase + "-" + lowerCase2 : lowerCase) + "; " + (Build.MODEL + " Build/" + Build.ID) + ") AppleWebKit/0.0 (KHTML, like " + "Gecko) Version/0.0 Mobile Safari/0.0";
            }
            h = userAgentString + " (Mobile; " + "afma-sdk-a-v" + AdRequest.VERSION + ")";
        }
        return h;
    }
}
