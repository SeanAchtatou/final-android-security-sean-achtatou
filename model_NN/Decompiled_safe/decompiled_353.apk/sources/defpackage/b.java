package defpackage;

import android.content.Context;
import android.os.AsyncTask;
import com.google.ads.util.a;
import java.net.HttpURLConnection;
import java.util.StringTokenizer;

/* renamed from: b  reason: default package */
public final class b extends AsyncTask<String, Void, Void> {
    private c a;
    private d b;
    private Context c;

    b(c cVar, d dVar, Context context) {
        this.a = cVar;
        this.b = dVar;
        this.c = context;
    }

    /* access modifiers changed from: private */
    /*  JADX ERROR: Method load error
        jadx.core.utils.exceptions.DecodeException: Load method exception: Method info already added: java.lang.StringBuilder.<init>():void in method: b.a(java.lang.String[]):java.lang.Void, dex: classes.dex
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:154)
        	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:306)
        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
        	at jadx.core.ProcessClass.generateCode(ProcessClass.java:58)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
        Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Method info already added: java.lang.StringBuilder.<init>():void
        	at jadx.core.dex.info.InfoStorage.putMethod(InfoStorage.java:42)
        	at jadx.core.dex.info.MethodInfo.fromDex(MethodInfo.java:50)
        	at jadx.core.dex.instructions.InsnDecoder.invoke(InsnDecoder.java:678)
        	at jadx.core.dex.instructions.InsnDecoder.decode(InsnDecoder.java:534)
        	at jadx.core.dex.instructions.InsnDecoder.process(InsnDecoder.java:78)
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:139)
        	... 5 more
        */
    /* renamed from: a */
    public java.lang.Void doInBackground(java.lang.String... r1) {
        /*
            r6 = this;
            r5 = 400(0x190, float:5.6E-43)
            r0 = 0
            r4 = 0
            r0 = r7[r0]
            r1 = r0
        L_0x0007:
            java.net.URL r0 = new java.net.URL     // Catch:{ MalformedURLException -> 0x0059, IOException -> 0x0093 }
            r0.<init>(r1)     // Catch:{ MalformedURLException -> 0x0059, IOException -> 0x0093 }
            java.net.URLConnection r0 = r0.openConnection()     // Catch:{ MalformedURLException -> 0x0059, IOException -> 0x0093 }
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ MalformedURLException -> 0x0059, IOException -> 0x0093 }
            android.content.Context r2 = r6.c     // Catch:{ MalformedURLException -> 0x0059, IOException -> 0x0093 }
            com.google.ads.util.AdUtil.a(r0, r2)     // Catch:{ MalformedURLException -> 0x0059, IOException -> 0x0093 }
            r2 = 0
            r0.setInstanceFollowRedirects(r2)     // Catch:{ MalformedURLException -> 0x0059, IOException -> 0x0093 }
            r0.connect()     // Catch:{ MalformedURLException -> 0x0059, IOException -> 0x0093 }
            int r2 = r0.getResponseCode()     // Catch:{ MalformedURLException -> 0x0059, IOException -> 0x0093 }
            r3 = 300(0x12c, float:4.2E-43)
            if (r3 > r2) goto L_0x0068
            if (r2 >= r5) goto L_0x0068
            java.lang.String r1 = "Location"
            java.lang.String r1 = r0.getHeaderField(r1)     // Catch:{ MalformedURLException -> 0x0059, IOException -> 0x0093 }
            if (r1 != 0) goto L_0x0055
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x0059, IOException -> 0x0093 }
            r0.<init>()     // Catch:{ MalformedURLException -> 0x0059, IOException -> 0x0093 }
            java.lang.String r1 = "Could not get redirect location from a "
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ MalformedURLException -> 0x0059, IOException -> 0x0093 }
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ MalformedURLException -> 0x0059, IOException -> 0x0093 }
            java.lang.String r1 = " redirect."
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ MalformedURLException -> 0x0059, IOException -> 0x0093 }
            java.lang.String r0 = r0.toString()     // Catch:{ MalformedURLException -> 0x0059, IOException -> 0x0093 }
            com.google.ads.util.a.c(r0)     // Catch:{ MalformedURLException -> 0x0059, IOException -> 0x0093 }
            c r0 = r6.a     // Catch:{ MalformedURLException -> 0x0059, IOException -> 0x0093 }
            com.google.ads.AdRequest$ErrorCode r1 = com.google.ads.AdRequest.ErrorCode.INTERNAL_ERROR     // Catch:{ MalformedURLException -> 0x0059, IOException -> 0x0093 }
            r0.a(r1)     // Catch:{ MalformedURLException -> 0x0059, IOException -> 0x0093 }
            r0 = r4
        L_0x0054:
            return r0
        L_0x0055:
            r6.a(r0)     // Catch:{ MalformedURLException -> 0x0059, IOException -> 0x0093 }
            goto L_0x0007
        L_0x0059:
            r0 = move-exception
            java.lang.String r1 = "Received malformed ad url from javascript."
            com.google.ads.util.a.a(r1, r0)
            c r0 = r6.a
            com.google.ads.AdRequest$ErrorCode r1 = com.google.ads.AdRequest.ErrorCode.INTERNAL_ERROR
            r0.a(r1)
            r0 = r4
            goto L_0x0054
        L_0x0068:
            r3 = 200(0xc8, float:2.8E-43)
            if (r2 != r3) goto L_0x00f0
            r6.a(r0)     // Catch:{ MalformedURLException -> 0x0059, IOException -> 0x0093 }
            java.io.InputStream r0 = r0.getInputStream()     // Catch:{ MalformedURLException -> 0x0059, IOException -> 0x0093 }
            java.io.BufferedReader r2 = new java.io.BufferedReader     // Catch:{ MalformedURLException -> 0x0059, IOException -> 0x0093 }
            java.io.InputStreamReader r3 = new java.io.InputStreamReader     // Catch:{ MalformedURLException -> 0x0059, IOException -> 0x0093 }
            r3.<init>(r0)     // Catch:{ MalformedURLException -> 0x0059, IOException -> 0x0093 }
            r0 = 4096(0x1000, float:5.74E-42)
            r2.<init>(r3, r0)     // Catch:{ MalformedURLException -> 0x0059, IOException -> 0x0093 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x0059, IOException -> 0x0093 }
            r0.<init>()     // Catch:{ MalformedURLException -> 0x0059, IOException -> 0x0093 }
        L_0x0084:
            java.lang.String r3 = r2.readLine()     // Catch:{ MalformedURLException -> 0x0059, IOException -> 0x0093 }
            if (r3 == 0) goto L_0x00a2
            r0.append(r3)     // Catch:{ MalformedURLException -> 0x0059, IOException -> 0x0093 }
            java.lang.String r3 = "\n"
            r0.append(r3)     // Catch:{ MalformedURLException -> 0x0059, IOException -> 0x0093 }
            goto L_0x0084
        L_0x0093:
            r0 = move-exception
            java.lang.String r1 = "IOException connecting to ad url."
            com.google.ads.util.a.c(r1, r0)
            c r0 = r6.a
            com.google.ads.AdRequest$ErrorCode r1 = com.google.ads.AdRequest.ErrorCode.NETWORK_ERROR
            r0.a(r1)
            r0 = r4
            goto L_0x0054
        L_0x00a2:
            java.lang.String r0 = r0.toString()     // Catch:{ MalformedURLException -> 0x0059, IOException -> 0x0093 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x0059, IOException -> 0x0093 }
            r2.<init>()     // Catch:{ MalformedURLException -> 0x0059, IOException -> 0x0093 }
            java.lang.String r3 = "Response content is: "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ MalformedURLException -> 0x0059, IOException -> 0x0093 }
            java.lang.StringBuilder r2 = r2.append(r0)     // Catch:{ MalformedURLException -> 0x0059, IOException -> 0x0093 }
            java.lang.String r2 = r2.toString()     // Catch:{ MalformedURLException -> 0x0059, IOException -> 0x0093 }
            com.google.ads.util.a.a(r2)     // Catch:{ MalformedURLException -> 0x0059, IOException -> 0x0093 }
            if (r0 == 0) goto L_0x00c8
            java.lang.String r2 = r0.trim()     // Catch:{ MalformedURLException -> 0x0059, IOException -> 0x0093 }
            int r2 = r2.length()     // Catch:{ MalformedURLException -> 0x0059, IOException -> 0x0093 }
            if (r2 > 0) goto L_0x00e8
        L_0x00c8:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x0059, IOException -> 0x0093 }
            r1.<init>()     // Catch:{ MalformedURLException -> 0x0059, IOException -> 0x0093 }
            java.lang.String r2 = "Response message is null or zero length: "
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ MalformedURLException -> 0x0059, IOException -> 0x0093 }
            java.lang.StringBuilder r0 = r1.append(r0)     // Catch:{ MalformedURLException -> 0x0059, IOException -> 0x0093 }
            java.lang.String r0 = r0.toString()     // Catch:{ MalformedURLException -> 0x0059, IOException -> 0x0093 }
            com.google.ads.util.a.a(r0)     // Catch:{ MalformedURLException -> 0x0059, IOException -> 0x0093 }
            c r0 = r6.a     // Catch:{ MalformedURLException -> 0x0059, IOException -> 0x0093 }
            com.google.ads.AdRequest$ErrorCode r1 = com.google.ads.AdRequest.ErrorCode.NO_FILL     // Catch:{ MalformedURLException -> 0x0059, IOException -> 0x0093 }
            r0.a(r1)     // Catch:{ MalformedURLException -> 0x0059, IOException -> 0x0093 }
            r0 = r4
            goto L_0x0054
        L_0x00e8:
            c r2 = r6.a     // Catch:{ MalformedURLException -> 0x0059, IOException -> 0x0093 }
            r2.a(r0, r1)     // Catch:{ MalformedURLException -> 0x0059, IOException -> 0x0093 }
            r0 = r4
            goto L_0x0054
        L_0x00f0:
            if (r2 != r5) goto L_0x0101
            java.lang.String r0 = "Bad request"
            com.google.ads.util.a.c(r0)     // Catch:{ MalformedURLException -> 0x0059, IOException -> 0x0093 }
            c r0 = r6.a     // Catch:{ MalformedURLException -> 0x0059, IOException -> 0x0093 }
            com.google.ads.AdRequest$ErrorCode r1 = com.google.ads.AdRequest.ErrorCode.INVALID_REQUEST     // Catch:{ MalformedURLException -> 0x0059, IOException -> 0x0093 }
            r0.a(r1)     // Catch:{ MalformedURLException -> 0x0059, IOException -> 0x0093 }
            r0 = r4
            goto L_0x0054
        L_0x0101:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x0059, IOException -> 0x0093 }
            r0.<init>()     // Catch:{ MalformedURLException -> 0x0059, IOException -> 0x0093 }
            java.lang.String r1 = "Invalid response code: "
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ MalformedURLException -> 0x0059, IOException -> 0x0093 }
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ MalformedURLException -> 0x0059, IOException -> 0x0093 }
            java.lang.String r0 = r0.toString()     // Catch:{ MalformedURLException -> 0x0059, IOException -> 0x0093 }
            com.google.ads.util.a.c(r0)     // Catch:{ MalformedURLException -> 0x0059, IOException -> 0x0093 }
            c r0 = r6.a     // Catch:{ MalformedURLException -> 0x0059, IOException -> 0x0093 }
            com.google.ads.AdRequest$ErrorCode r1 = com.google.ads.AdRequest.ErrorCode.INTERNAL_ERROR     // Catch:{ MalformedURLException -> 0x0059, IOException -> 0x0093 }
            r0.a(r1)     // Catch:{ MalformedURLException -> 0x0059, IOException -> 0x0093 }
            r0 = r4
            goto L_0x0054
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.b.doInBackground(java.lang.String[]):java.lang.Void");
    }

    private void a(HttpURLConnection httpURLConnection) {
        String headerField = httpURLConnection.getHeaderField("X-Afma-Tracking-Urls");
        if (headerField != null) {
            StringTokenizer stringTokenizer = new StringTokenizer(headerField);
            while (stringTokenizer.hasMoreTokens()) {
                this.b.a(stringTokenizer.nextToken());
            }
        }
        b(httpURLConnection);
        String headerField2 = httpURLConnection.getHeaderField("X-Afma-Refresh-Rate");
        if (headerField2 != null) {
            try {
                this.b.a(Float.parseFloat(headerField2));
                if (!this.b.p()) {
                    this.b.d();
                }
            } catch (NumberFormatException e) {
                a.c("Could not get refresh value: " + headerField2, e);
            }
        }
        String headerField3 = httpURLConnection.getHeaderField("X-Afma-Interstitial-Timeout");
        if (headerField3 != null) {
            try {
                this.b.a((long) (Float.parseFloat(headerField3) * 1000.0f));
            } catch (NumberFormatException e2) {
                a.c("Could not get timeout value: " + headerField3, e2);
            }
        }
        String headerField4 = httpURLConnection.getHeaderField("X-Afma-Orientation");
        if (headerField4 == null) {
            return;
        }
        if (headerField4.equals("portrait")) {
            this.b.a(1);
        } else if (headerField4.equals("landscape")) {
            this.b.a(0);
        }
    }

    private void b(HttpURLConnection httpURLConnection) {
        String headerField = httpURLConnection.getHeaderField("X-Afma-Click-Tracking-Urls");
        if (headerField != null) {
            StringTokenizer stringTokenizer = new StringTokenizer(headerField);
            while (stringTokenizer.hasMoreTokens()) {
                this.b.b(stringTokenizer.nextToken());
            }
        }
    }
}
