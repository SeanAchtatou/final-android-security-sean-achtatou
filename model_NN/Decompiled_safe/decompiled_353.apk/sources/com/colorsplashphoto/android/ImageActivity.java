package com.colorsplashphoto.android;

import android.app.Activity;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.Toast;
import com.google.ads.AdRequest;
import com.google.ads.AdView;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.UUID;

public class ImageActivity extends Activity {
    public static String ImagePath;
    static String extStorageDirectory;
    public static int height;
    public static boolean isReload = false;
    public static int width;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ImagePath = PhotoActivity.selectedImagePath;
        Display display = getWindowManager().getDefaultDisplay();
        height = display.getHeight();
        width = display.getWidth();
        setContentView((int) R.layout.photoalbum);
        ((AdView) findViewById(R.id.adView)).loadAd(new AdRequest());
        ((ImageButton) findViewById(R.id.btnphoto)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                ImageActivity.this.startActivityForResult(new Intent(view.getContext(), PhotoActivity.class), 0);
            }
        });
        ((ImageButton) findViewById(R.id.btnemail)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent("android.intent.action.SEND");
                intent.setType("plain/text");
                intent.putExtra("android.intent.extra.EMAIL", new String[]{""});
                ImageActivity.this.startActivity(intent);
            }
        });
        ((ImageButton) findViewById(R.id.btnsave)).setOnClickListener(new View.OnClickListener() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
             arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
             candidates:
              ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
              ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
            public void onClick(View v) {
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(ImageActivity.ImagePath, options);
                int srcWidth = options.outWidth;
                int srcHeight = options.outHeight;
                int desiredWidth = Panel.viewWidth;
                int desiredHeight = Panel.viewHeight;
                int inSampleSize = 1;
                while (srcWidth / 2 > desiredWidth) {
                    srcWidth /= 2;
                    srcHeight /= 2;
                    inSampleSize *= 2;
                }
                options.inJustDecodeBounds = false;
                options.inDither = false;
                options.inSampleSize = inSampleSize;
                options.inScaled = false;
                options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                Bitmap sampledSrcBitmap = BitmapFactory.decodeFile(ImageActivity.ImagePath, options);
                Matrix matrix = new Matrix();
                matrix.postScale(((float) desiredWidth) / ((float) srcWidth), ((float) desiredHeight) / ((float) srcHeight));
                Bitmap original = Bitmap.createBitmap(sampledSrcBitmap, 0, 0, sampledSrcBitmap.getWidth(), sampledSrcBitmap.getHeight(), matrix, true);
                Bitmap bitmap1 = Bitmap.createBitmap(Panel.viewWidth, Panel.viewHeight, Bitmap.Config.ARGB_8888);
                Canvas pcanvas = new Canvas();
                pcanvas.setBitmap(bitmap1);
                pcanvas.drawBitmap(original, 0.0f, 0.0f, (Paint) null);
                pcanvas.drawBitmap(Panel.myTopImage, 0.0f, 0.0f, (Paint) null);
                ImageActivity.this.StoreByteImage(UUID.randomUUID().toString(), 100, bitmap1);
            }
        });
        ((ImageButton) findViewById(R.id.btnbrush)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ImageActivity.this.startActivity(new Intent(v.getContext(), SettingsActivity.class));
            }
        });
        ((ImageButton) findViewById(R.id.btnreload)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                View myCustomView = ImageActivity.this.findViewById(R.id.note);
                ImageActivity.isReload = true;
                myCustomView.invalidate();
            }
        });
        ((ImageButton) findViewById(R.id.btnfb)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent facebookIntent = new Intent(v.getContext(), Example.class);
                facebookIntent.putExtra("facebookMessage", "");
                ImageActivity.this.startActivity(facebookIntent);
            }
        });
    }

    private void setBrightness(int value) {
        if (value < 10) {
            value = 10;
        } else if (value > 100) {
            value = 100;
        }
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.screenBrightness = ((float) value) / 100.0f;
        lp.flags |= 128;
        getWindow().setAttributes(lp);
    }

    public boolean StoreByteImage(String nameFile, int quality, Bitmap myImage) {
        IOException e;
        FileNotFoundException e2;
        extStorageDirectory = Environment.getExternalStorageDirectory().toString();
        try {
            new BitmapFactory.Options().inSampleSize = 5;
            try {
                BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(new File(extStorageDirectory, String.valueOf(nameFile) + ".jpg")));
                myImage.compress(Bitmap.CompressFormat.JPEG, quality, bos);
                bos.flush();
                bos.close();
                IntentFilter filter = new IntentFilter("android.intent.action.MEDIA_MOUNTED");
                filter.addAction("android.intent.action.MEDIA_UNMOUNTED");
                filter.addDataScheme("file");
                sendBroadcast(new Intent("android.intent.action.MEDIA_MOUNTED", Uri.parse("file://" + Environment.getExternalStorageDirectory())));
            } catch (FileNotFoundException e3) {
                e2 = e3;
                e2.printStackTrace();
                Toast.makeText(this, "Save Failed!", 1).show();
                Toast.makeText(this, "Save Success!", 1).show();
                return true;
            } catch (IOException e4) {
                e = e4;
                e.printStackTrace();
                Toast.makeText(this, "Save Failed!", 1).show();
                Toast.makeText(this, "Save Success!", 1).show();
                return true;
            }
        } catch (FileNotFoundException e5) {
            e2 = e5;
            e2.printStackTrace();
            Toast.makeText(this, "Save Failed!", 1).show();
            Toast.makeText(this, "Save Success!", 1).show();
            return true;
        } catch (IOException e6) {
            e = e6;
            e.printStackTrace();
            Toast.makeText(this, "Save Failed!", 1).show();
            Toast.makeText(this, "Save Success!", 1).show();
            return true;
        }
        Toast.makeText(this, "Save Success!", 1).show();
        return true;
    }
}
