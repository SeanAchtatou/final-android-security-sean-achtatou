package com.colorsplashphoto.android;

public final class R {

    public static final class attr {
    }

    public static final class drawable {
        public static final int apple = 2130837504;
        public static final int black = 2130837517;
        public static final int brush = 2130837505;
        public static final int fb = 2130837506;
        public static final int login = 2130837507;
        public static final int login_down = 2130837508;
        public static final int logout = 2130837509;
        public static final int logout_down = 2130837510;
        public static final int mail = 2130837511;
        public static final int mainapple = 2130837512;
        public static final int photo = 2130837513;
        public static final int reload = 2130837514;
        public static final int save = 2130837515;
        public static final int white = 2130837516;
    }

    public static final class id {
        public static final int adView = 2131034120;
        public static final int bitmapview = 2131034128;
        public static final int btnalbum = 2131034130;
        public static final int btnbrush = 2131034125;
        public static final int btncamera = 2131034131;
        public static final int btncancel = 2131034132;
        public static final int btnemail = 2131034124;
        public static final int btnfb = 2131034127;
        public static final int btnphoto = 2131034122;
        public static final int btnreload = 2131034126;
        public static final int btnsave = 2131034123;
        public static final int deletePostButton = 2131034117;
        public static final int linearlayout = 2131034121;
        public static final int login = 2131034112;
        public static final int note = 2131034119;
        public static final int postButton = 2131034116;
        public static final int relative = 2131034118;
        public static final int relative2 = 2131034129;
        public static final int requestButton = 2131034115;
        public static final int seek = 2131034133;
        public static final int txt = 2131034113;
        public static final int uploadButton = 2131034114;
    }

    public static final class layout {
        public static final int attrs = 2130903040;
        public static final int facebook = 2130903041;
        public static final int login_button = 2130903042;
        public static final int logout_button = 2130903043;
        public static final int main = 2130903044;
        public static final int newfile = 2130903045;
        public static final int photoalbum = 2130903046;
        public static final int photoxml = 2130903047;
        public static final int selector = 2130903048;
        public static final int settings = 2130903049;
        public static final int shape_btn = 2130903050;
        public static final int style = 2130903051;
    }

    public static final class string {
        public static final int app_name = 2130968576;
        public static final int delete = 2130968580;
        public static final int media = 2130968582;
        public static final int post = 2130968579;
        public static final int request = 2130968577;
        public static final int settings = 2130968581;
        public static final int upload = 2130968578;
    }
}
