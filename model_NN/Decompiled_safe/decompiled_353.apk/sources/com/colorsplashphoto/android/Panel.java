package com.colorsplashphoto.android;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

class Panel extends View {
    public static Bitmap myTopImage;
    public static int viewHeight = 0;
    public static int viewWidth = 0;
    Bitmap bgr;
    Bitmap bitmap;
    Bitmap grayscale;
    String imagepath;
    private Paint mPaint;
    Bitmap original;
    Canvas pcanvas;
    int r = 0;
    Bitmap reSized;
    int x = 0;
    int y = 0;

    public Panel(Context context, String imagepath2) {
        super(context);
    }

    public Panel(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        if (ImageActivity.isReload) {
            onReload();
            ImageActivity.isReload = false;
        }
        canvas.drawBitmap(this.original, 0.0f, 0.0f, (Paint) null);
        this.pcanvas.drawCircle((float) this.x, (float) this.y, (float) this.r, this.mPaint);
        canvas.drawBitmap(this.bitmap, 0.0f, 0.0f, (Paint) null);
        myTopImage = this.bitmap;
        super.onDraw(canvas);
    }

    public boolean onTouchEvent(MotionEvent event) {
        this.x = (int) event.getX();
        this.y = (int) event.getY();
        this.r = SettingsActivity.brushsize;
        invalidate();
        return true;
    }

    /* access modifiers changed from: protected */
    public void onReload() {
        if (this.original != null) {
            this.original.recycle();
        }
        if (this.bitmap != null) {
            this.bitmap.recycle();
        }
        this.x = 0;
        this.y = 0;
        this.r = 0;
        myInitDraw();
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int xNew, int yNew, int xOld, int yOld) {
        super.onSizeChanged(xNew, yNew, xOld, yOld);
        viewWidth = xNew;
        viewHeight = yNew;
        onReload();
        invalidate();
    }

    public Bitmap toGrayscale(Bitmap bmpOriginal) {
        Bitmap bmpGrayscale = Bitmap.createBitmap(bmpOriginal.getWidth(), bmpOriginal.getHeight(), Bitmap.Config.RGB_565);
        Canvas c = new Canvas(bmpGrayscale);
        Paint paint = new Paint();
        ColorMatrix cm = new ColorMatrix();
        cm.setSaturation(0.0f);
        paint.setColorFilter(new ColorMatrixColorFilter(cm));
        c.drawBitmap(bmpOriginal, 0.0f, 0.0f, paint);
        return bmpGrayscale;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public void myInitDraw() {
        this.imagepath = ImageActivity.ImagePath;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(this.imagepath, options);
        int srcWidth = options.outWidth;
        int srcHeight = options.outHeight;
        int desiredWidth = viewWidth;
        int desiredHeight = viewHeight;
        int inSampleSize = 1;
        while (srcWidth / 2 > desiredWidth) {
            srcWidth /= 2;
            srcHeight /= 2;
            inSampleSize *= 2;
        }
        options.inJustDecodeBounds = false;
        options.inDither = false;
        options.inSampleSize = inSampleSize;
        options.inScaled = false;
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        Bitmap sampledSrcBitmap = BitmapFactory.decodeFile(this.imagepath, options);
        Matrix matrix = new Matrix();
        matrix.postScale(((float) desiredWidth) / ((float) srcWidth), ((float) desiredHeight) / ((float) srcHeight));
        this.original = Bitmap.createBitmap(sampledSrcBitmap, 0, 0, sampledSrcBitmap.getWidth(), sampledSrcBitmap.getHeight(), matrix, true);
        this.grayscale = toGrayscale(this.original);
        setFocusable(true);
        this.mPaint = new Paint();
        this.mPaint.setAlpha(0);
        this.mPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));
        this.mPaint.setAntiAlias(true);
        this.bitmap = Bitmap.createBitmap(viewWidth, viewHeight, Bitmap.Config.ARGB_8888);
        this.pcanvas = new Canvas();
        this.pcanvas.setBitmap(this.bitmap);
        this.pcanvas.drawBitmap(this.grayscale, 0.0f, 0.0f, (Paint) null);
    }
}
