package com.colorsplashphoto.android;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class PhotoActivity extends Activity {
    private static final int CAMERA_PIC_REQUEST = 2;
    protected static final Uri ContentResolver = null;
    protected static final String DATA_CONSTANT = null;
    private static final int SELECT_PICTURE = 1;
    public static String selectedImagePath;
    protected Object contentResolver;
    Camera mCamera;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.photoxml);
        ((Button) findViewById(R.id.btncamera)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                PhotoActivity.this.startActivityForResult(new Intent("android.media.action.IMAGE_CAPTURE"), PhotoActivity.CAMERA_PIC_REQUEST);
            }
        });
        ((Button) findViewById(R.id.btnalbum)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction("android.intent.action.GET_CONTENT");
                PhotoActivity.this.startActivityForResult(Intent.createChooser(intent, "Select Picture"), PhotoActivity.SELECT_PICTURE);
            }
        });
        ((Button) findViewById(R.id.btncancel)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                PhotoActivity.this.finish();
            }
        });
    }

    /* Debug info: failed to restart local var, previous not found, register: 4 */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == -1) {
            if (requestCode == SELECT_PICTURE) {
                selectedImagePath = getPath(data.getData());
                if (selectedImagePath != null) {
                    System.out.println(selectedImagePath);
                } else {
                    System.out.println("selectedImagePath is null");
                }
                startActivity(new Intent(this, ImageActivity.class));
            }
            if (requestCode == CAMERA_PIC_REQUEST) {
                Bitmap bitmap = (Bitmap) data.getExtras().get("data");
            }
        }
    }

    public String getPath(Uri uri) {
        String[] projection = new String[SELECT_PICTURE];
        projection[0] = "_data";
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow("_data");
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }
}
