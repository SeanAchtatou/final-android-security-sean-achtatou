package com.colorsplashphoto.android;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.SeekBar;
import com.google.ads.AdRequest;
import com.google.ads.AdView;

public class SettingsActivity extends Activity {
    public static int brushsize = 15;
    private static int setBrushSizeValue = 10;
    private SeekBar brushControl;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.settings);
        ((AdView) findViewById(R.id.adView)).loadAd(new AdRequest());
        this.brushControl = (SeekBar) findViewById(R.id.seek);
        this.brushControl.setIndeterminate(false);
        this.brushControl.setProgress(setBrushSizeValue);
    }

    public void okButton(View button) throws InterruptedException {
        setBrushSizeValue = this.brushControl.getProgress();
        brushsize = setBrushSizeValue + 5;
        finish();
    }
}
