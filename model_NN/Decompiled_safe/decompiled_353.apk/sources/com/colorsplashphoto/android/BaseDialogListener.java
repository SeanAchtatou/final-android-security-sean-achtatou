package com.colorsplashphoto.android;

import com.colorsplashphoto.android.Facebook;

public abstract class BaseDialogListener implements Facebook.DialogListener {
    public void onFacebookError(FacebookError e) {
        e.printStackTrace();
    }

    public void onError(DialogError e) {
        e.printStackTrace();
    }

    public void onCancel() {
    }
}
