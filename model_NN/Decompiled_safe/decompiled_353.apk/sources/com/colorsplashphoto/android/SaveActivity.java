package com.colorsplashphoto.android;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

public class SaveActivity extends Activity {
    private static final int SELECT_PICTURE = 1;
    private String filemanagerstring;
    private String selectedImagePath;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.main);
        ((ImageButton) findViewById(R.id.btnsave)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction("android.intent.action.GET_CONTENT");
                SaveActivity.this.startActivityForResult(Intent.createChooser(intent, "Select Picture"), SaveActivity.SELECT_PICTURE);
            }
        });
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == -1 && requestCode == SELECT_PICTURE) {
            Uri selectedImageUri = data.getData();
            this.filemanagerstring = selectedImageUri.getPath();
            this.selectedImagePath = getPath(selectedImageUri);
            if (this.selectedImagePath != null) {
                System.out.println(this.selectedImagePath);
            } else {
                System.out.println("selectedImagePath is null");
            }
            if (this.filemanagerstring != null) {
                System.out.println(this.filemanagerstring);
            } else {
                System.out.println("filemanagerstring is null");
            }
            if (this.selectedImagePath != null) {
                System.out.println("selectedImagePath is the right one for you!");
            } else {
                System.out.println("filemanagerstring is the right one for you!");
            }
        }
    }

    public String getPath(Uri uri) {
        String[] projection = new String[SELECT_PICTURE];
        projection[0] = "_data";
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        if (cursor == null) {
            return null;
        }
        int column_index = cursor.getColumnIndexOrThrow("_data");
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }
}
