package com.colorsplashphoto.android;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.widget.ImageView;

public class AndroidReceiveBitmap extends Activity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.newfile);
        ((ImageView) findViewById(R.id.bitmapview)).setImageBitmap((Bitmap) getIntent().getParcelableExtra("Bitmap"));
    }
}
