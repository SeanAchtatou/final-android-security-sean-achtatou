package com.colorsplashphoto.android;

import android.app.Activity;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.Toast;
import com.google.ads.AdRequest;
import com.google.ads.AdView;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.UUID;

public class OpenActivity extends Activity {
    static String extStorageDirectory;
    public static int height;
    public static boolean isReload = false;
    public static int width;
    Bitmap bitmap;
    Bitmap grayscale;
    Drawable[] layers;
    Bitmap original;
    Canvas pcanvas;
    int r = 0;
    int x = 0;
    int y = 0;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Display display = getWindowManager().getDefaultDisplay();
        height = display.getHeight();
        width = display.getWidth();
        setContentView((int) R.layout.main);
        ((AdView) findViewById(R.id.adView)).loadAd(new AdRequest());
        ((ImageButton) findViewById(R.id.btnphoto)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                OpenActivity.this.startActivityForResult(new Intent(view.getContext(), PhotoActivity.class), 0);
            }
        });
        ((ImageButton) findViewById(R.id.btnemail)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent("android.intent.action.SEND");
                intent.setType("plain/text");
                intent.putExtra("android.intent.extra.EMAIL", new String[]{""});
                OpenActivity.this.startActivity(intent);
            }
        });
        ((ImageButton) findViewById(R.id.btnsave)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Bitmap original = MainScreen.getResizedBitmap(BitmapFactory.decodeResource(OpenActivity.this.getResources(), R.drawable.mainapple), MainScreen.viewWidth, MainScreen.viewHeight);
                Bitmap bitmap1 = Bitmap.createBitmap(MainScreen.viewWidth, MainScreen.viewHeight, Bitmap.Config.ARGB_8888);
                Canvas pcanvas = new Canvas();
                pcanvas.setBitmap(bitmap1);
                pcanvas.drawBitmap(original, 0.0f, 0.0f, (Paint) null);
                pcanvas.drawBitmap(MainScreen.myTopImage, 0.0f, 0.0f, (Paint) null);
                OpenActivity.this.StoreByteImage(UUID.randomUUID().toString(), 100, bitmap1);
            }
        });
        ((ImageButton) findViewById(R.id.btnbrush)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                OpenActivity.this.startActivity(new Intent(v.getContext(), SettingsActivity.class));
            }
        });
        ((ImageButton) findViewById(R.id.btnreload)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                View myCustomView = OpenActivity.this.findViewById(R.id.note);
                OpenActivity.isReload = true;
                myCustomView.invalidate();
            }
        });
        ((ImageButton) findViewById(R.id.btnfb)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                OpenActivity.this.startActivity(new Intent(v.getContext(), Example.class));
            }
        });
    }

    public boolean StoreByteImage(String nameFile, int quality, Bitmap myImage) {
        IOException e;
        FileNotFoundException e2;
        extStorageDirectory = Environment.getExternalStorageDirectory().toString();
        try {
            new BitmapFactory.Options().inSampleSize = 5;
            try {
                BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(new File(extStorageDirectory, String.valueOf(nameFile) + ".jpg")));
                myImage.compress(Bitmap.CompressFormat.JPEG, quality, bos);
                bos.flush();
                bos.close();
                IntentFilter filter = new IntentFilter("android.intent.action.MEDIA_MOUNTED");
                filter.addAction("android.intent.action.MEDIA_UNMOUNTED");
                filter.addDataScheme("file");
                sendBroadcast(new Intent("android.intent.action.MEDIA_MOUNTED", Uri.parse("file://" + Environment.getExternalStorageDirectory())));
            } catch (FileNotFoundException e3) {
                e2 = e3;
                e2.printStackTrace();
                Toast.makeText(this, "Save Failed!", 1).show();
                Toast.makeText(this, "Save Success!", 1).show();
                return true;
            } catch (IOException e4) {
                e = e4;
                Toast.makeText(this, "Save Failed!", 1).show();
                e.printStackTrace();
                Toast.makeText(this, "Save Success!", 1).show();
                return true;
            }
        } catch (FileNotFoundException e5) {
            e2 = e5;
            e2.printStackTrace();
            Toast.makeText(this, "Save Failed!", 1).show();
            Toast.makeText(this, "Save Success!", 1).show();
            return true;
        } catch (IOException e6) {
            e = e6;
            Toast.makeText(this, "Save Failed!", 1).show();
            e.printStackTrace();
            Toast.makeText(this, "Save Success!", 1).show();
            return true;
        }
        Toast.makeText(this, "Save Success!", 1).show();
        return true;
    }

    public Bitmap toGrayscale(Bitmap bmpOriginal) {
        Bitmap bmpGrayscale = Bitmap.createBitmap(bmpOriginal.getWidth(), bmpOriginal.getHeight(), Bitmap.Config.RGB_565);
        Canvas c = new Canvas(bmpGrayscale);
        Paint paint = new Paint();
        ColorMatrix cm = new ColorMatrix();
        cm.setSaturation(0.0f);
        paint.setColorFilter(new ColorMatrixColorFilter(cm));
        c.drawBitmap(bmpOriginal, 0.0f, 0.0f, paint);
        return bmpGrayscale;
    }

    public void setBrightness(int value) {
        if (value < 10) {
            value = 10;
        } else if (value > 100) {
            value = 100;
        }
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.screenBrightness = ((float) value) / 100.0f;
        lp.flags |= 128;
        getWindow().setAttributes(lp);
    }
}
