package com.colorsplashphoto.android;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

class MainScreen extends View {
    public static Bitmap bitmap;
    public static int isFirstTime = 1;
    public static Bitmap myTopImage;
    public static int viewHeight = 0;
    public static int viewWidth = 0;
    int _height;
    int _width;
    Bitmap bgr;
    Bitmap grayscale;
    private Paint mPaint;
    Bitmap original;
    Canvas pcanvas;
    int r = 0;
    Bitmap reSized;
    int x = 0;
    int y = 0;

    public MainScreen(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MainScreen(Context context) {
        super(context);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        if (OpenActivity.isReload) {
            onReload();
            OpenActivity.isReload = false;
        }
        canvas.drawBitmap(this.reSized, 0.0f, 0.0f, (Paint) null);
        this.pcanvas.drawCircle((float) this.x, (float) this.y, (float) this.r, this.mPaint);
        canvas.drawBitmap(bitmap, 0.0f, 0.0f, (Paint) null);
        myTopImage = bitmap;
        super.onDraw(canvas);
    }

    /* access modifiers changed from: protected */
    public void initialDraw() {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inScaled = false;
        this.original = BitmapFactory.decodeResource(getResources(), R.drawable.mainapple, options);
        this.reSized = getResizedBitmap(this.original, viewWidth, viewHeight);
        this.grayscale = toGrayscale(this.reSized);
        setFocusable(true);
        this.mPaint = new Paint();
        this.mPaint.setAlpha(0);
        this.mPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));
        this.mPaint.setAntiAlias(true);
        bitmap = Bitmap.createBitmap(viewWidth, viewHeight, Bitmap.Config.ARGB_8888);
        this.pcanvas = new Canvas();
        this.pcanvas.setBitmap(bitmap);
        this.pcanvas.drawBitmap(this.grayscale, 0.0f, 0.0f, (Paint) null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public static Bitmap getResizedBitmap(Bitmap bm, int newWidth, int newHeight) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        Matrix matrix = new Matrix();
        matrix.postScale(((float) newWidth) / ((float) width), ((float) newHeight) / ((float) height));
        return Bitmap.createBitmap(bm, 0, 0, width, height, matrix, true);
    }

    /* access modifiers changed from: protected */
    public void onReload() {
        if (this.original != null) {
            this.original.recycle();
        }
        if (bitmap != null) {
            bitmap.recycle();
        }
        this.x = 0;
        this.y = 0;
        this.r = 0;
        initialDraw();
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        this._height = View.MeasureSpec.getSize(heightMeasureSpec);
        this._width = View.MeasureSpec.getSize(widthMeasureSpec);
        setMeasuredDimension(this._width, this._height);
    }

    public boolean onTouchEvent(MotionEvent event) {
        this.x = (int) event.getX();
        this.y = (int) event.getY();
        this.r = SettingsActivity.brushsize;
        invalidate();
        return true;
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int xNew, int yNew, int xOld, int yOld) {
        super.onSizeChanged(xNew, yNew, xOld, yOld);
        viewWidth = xNew;
        viewHeight = yNew;
        onReload();
        invalidate();
    }

    public Bitmap toGrayscale(Bitmap bmpOriginal) {
        Bitmap bmpGrayscale = Bitmap.createBitmap(bmpOriginal.getWidth(), bmpOriginal.getHeight(), Bitmap.Config.RGB_565);
        Canvas c = new Canvas(bmpGrayscale);
        Paint paint = new Paint();
        ColorMatrix cm = new ColorMatrix();
        cm.setSaturation(0.0f);
        paint.setColorFilter(new ColorMatrixColorFilter(cm));
        c.drawBitmap(bmpOriginal, 0.0f, 0.0f, paint);
        return bmpGrayscale;
    }
}
