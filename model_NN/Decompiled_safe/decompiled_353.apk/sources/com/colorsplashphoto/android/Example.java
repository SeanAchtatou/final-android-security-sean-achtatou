package com.colorsplashphoto.android;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.colorsplashphoto.android.SessionEvents;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import org.json.JSONException;

public class Example extends Activity {
    public static final String APP_ID = "124680270945471";
    private static final int SELECT_PICTURE1 = 1;
    public static String selectedImagePath1;
    /* access modifiers changed from: private */
    public AsyncFacebookRunner mAsyncRunner;
    /* access modifiers changed from: private */
    public Button mDeleteButton;
    /* access modifiers changed from: private */
    public Facebook mFacebook;
    private LoginButton mLoginButton;
    /* access modifiers changed from: private */
    public Button mPostButton;
    /* access modifiers changed from: private */
    public Button mRequestButton;
    /* access modifiers changed from: private */
    public TextView mText;
    /* access modifiers changed from: private */
    public Button mUploadButton;
    private Uri selectedImageUri;

    public void onCreate(Bundle savedInstanceState) {
        int i;
        int i2;
        int i3;
        super.onCreate(savedInstanceState);
        if (APP_ID == 0) {
            Util.showAlert(this, "Warning", "Facebook Applicaton ID must be specified before running this example: see Example.java");
        }
        setContentView((int) R.layout.facebook);
        this.mLoginButton = (LoginButton) findViewById(R.id.login);
        this.mText = (TextView) findViewById(R.id.txt);
        this.mRequestButton = (Button) findViewById(R.id.requestButton);
        this.mPostButton = (Button) findViewById(R.id.postButton);
        this.mDeleteButton = (Button) findViewById(R.id.deletePostButton);
        this.mUploadButton = (Button) findViewById(R.id.uploadButton);
        this.mFacebook = new Facebook(APP_ID);
        this.mAsyncRunner = new AsyncFacebookRunner(this.mFacebook);
        SessionStore.restore(this.mFacebook, this);
        SessionEvents.addAuthListener(new SampleAuthListener());
        SessionEvents.addLogoutListener(new SampleLogoutListener());
        this.mLoginButton.init(this, this.mFacebook);
        this.mRequestButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Example.this.mAsyncRunner.request("me", new SampleRequestListener());
            }
        });
        Button button = this.mRequestButton;
        if (this.mFacebook.isSessionValid()) {
            i = 0;
        } else {
            i = 4;
        }
        button.setVisibility(i);
        this.mUploadButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction("android.intent.action.GET_CONTENT");
                Example.this.startActivityForResult(Intent.createChooser(intent, "Select Picture"), Example.SELECT_PICTURE1);
            }
        });
        Button button2 = this.mUploadButton;
        if (this.mFacebook.isSessionValid()) {
            i2 = 0;
        } else {
            i2 = 4;
        }
        button2.setVisibility(i2);
        this.mPostButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Example.this.mFacebook.dialog(Example.this, "feed", new SampleDialogListener());
            }
        });
        Button button3 = this.mPostButton;
        if (this.mFacebook.isSessionValid()) {
            i3 = 0;
        } else {
            i3 = 4;
        }
        button3.setVisibility(i3);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != -1) {
            return;
        }
        if (requestCode == SELECT_PICTURE1) {
            this.selectedImageUri = data.getData();
            selectedImagePath1 = getPath(this.selectedImageUri);
            if (selectedImagePath1 != null) {
                System.out.println(selectedImagePath1);
            } else {
                System.out.println("selectedImagePath is null");
            }
            upLoadMyPhoto();
            return;
        }
        this.mFacebook.authorizeCallback(requestCode, resultCode, data);
    }

    public void upLoadMyPhoto() {
        byte[] data = null;
        try {
            Bitmap bi = BitmapFactory.decodeStream(getContentResolver().openInputStream(this.selectedImageUri));
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bi.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            data = baos.toByteArray();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        Bundle params = new Bundle();
        params.putString("method", "photos.upload");
        params.putByteArray("picture", data);
        this.mAsyncRunner.request(null, params, "POST", new SampleUploadListener(), null);
    }

    public String getPath(Uri uri) {
        String[] projection = new String[SELECT_PICTURE1];
        projection[0] = "_data";
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow("_data");
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    public class SampleAuthListener implements SessionEvents.AuthListener {
        public SampleAuthListener() {
        }

        public void onAuthSucceed() {
            Example.this.mText.setText("You have logged in! ");
            Example.this.mRequestButton.setVisibility(0);
            Example.this.mUploadButton.setVisibility(0);
            Example.this.mPostButton.setVisibility(0);
        }

        public void onAuthFail(String error) {
            Example.this.mText.setText("Login Failed: " + error);
        }
    }

    public class SampleLogoutListener implements SessionEvents.LogoutListener {
        public SampleLogoutListener() {
        }

        public void onLogoutBegin() {
            Example.this.mText.setText("Logging out...");
        }

        public void onLogoutFinish() {
            Example.this.mText.setText("You have logged out! ");
            Example.this.mRequestButton.setVisibility(4);
            Example.this.mUploadButton.setVisibility(4);
            Example.this.mPostButton.setVisibility(4);
        }
    }

    public class SampleRequestListener extends BaseRequestListener {
        public SampleRequestListener() {
        }

        public void onComplete(String response, Object state) {
            try {
                Log.d("Facebook-Example", "Response: " + response.toString());
                final String name = Util.parseJson(response).getString("name");
                Example.this.runOnUiThread(new Runnable() {
                    public void run() {
                        Example.this.mText.setText("Color splash, " + name + "!");
                    }
                });
            } catch (JSONException e) {
                Log.w("Facebook-Example", "JSON Error in response");
            } catch (FacebookError e2) {
                Log.w("Facebook-Example", "Facebook Error: " + e2.getMessage());
            }
        }
    }

    public class SampleUploadListener extends BaseRequestListener {
        public SampleUploadListener() {
        }

        public void onComplete(String response, Object state) {
            try {
                Log.d("Facebook-Example", "Response: " + response.toString());
                final String src = Util.parseJson(response).getString("src");
                Example.this.runOnUiThread(new Runnable() {
                    public void run() {
                        Example.this.mText.setText("Color splash, photo has been uploaded at \n" + src);
                    }
                });
            } catch (JSONException e) {
                Log.w("Facebook-Example", "JSON Error in response");
            } catch (FacebookError e2) {
                Log.w("Facebook-Example", "Facebook Error: " + e2.getMessage());
            }
        }
    }

    public class WallPostRequestListener extends BaseRequestListener {
        public WallPostRequestListener() {
        }

        public void onComplete(String response, Object state) {
            Log.d("Facebook-Example", "Got response: " + response);
            String message = "Updated!";
            try {
                message = Util.parseJson(response).getString("message");
            } catch (JSONException e) {
                Log.w("Facebook-Example", "JSON Error in response");
            } catch (FacebookError e2) {
                Log.w("Facebook-Example", "Facebook Error: " + e2.getMessage());
            }
            final String text = "Your Wall Post: " + message;
            Example.this.runOnUiThread(new Runnable() {
                public void run() {
                    Example.this.mText.setText(text);
                }
            });
        }
    }

    public class WallPostDeleteListener extends BaseRequestListener {
        public WallPostDeleteListener() {
        }

        public void onComplete(String response, Object state) {
            if (response.equals("true")) {
                Log.d("Facebook-Example", "Successfully deleted wall post");
                Example.this.runOnUiThread(new Runnable() {
                    public void run() {
                        Example.this.mDeleteButton.setVisibility(4);
                        Example.this.mText.setText("Deleted Wall Post");
                    }
                });
                return;
            }
            Log.d("Facebook-Example", "Could not delete wall post");
        }
    }

    public class SampleDialogListener extends BaseDialogListener {
        public SampleDialogListener() {
        }

        public void onComplete(Bundle values) {
            final String postId = values.getString("post_id");
            if (postId != null) {
                Log.d("Facebook-Example", "Dialog Success! post_id=" + postId);
                Example.this.mAsyncRunner.request(postId, new WallPostRequestListener());
                Example.this.mDeleteButton.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        Example.this.mAsyncRunner.request(postId, new Bundle(), "DELETE", new WallPostDeleteListener(), null);
                    }
                });
                Example.this.mDeleteButton.setVisibility(0);
                return;
            }
            Log.d("Facebook-Example", "No wall post made");
        }
    }
}
