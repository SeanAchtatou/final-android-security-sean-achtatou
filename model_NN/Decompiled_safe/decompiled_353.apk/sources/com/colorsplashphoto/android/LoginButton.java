package com.colorsplashphoto.android;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageButton;
import com.colorsplashphoto.android.Facebook;
import com.colorsplashphoto.android.SessionEvents;

public class LoginButton extends ImageButton {
    /* access modifiers changed from: private */
    public Activity mActivity;
    /* access modifiers changed from: private */
    public Facebook mFb;
    /* access modifiers changed from: private */
    public Handler mHandler;
    /* access modifiers changed from: private */
    public String[] mPermissions;
    private SessionListener mSessionListener = new SessionListener(this, null);

    public LoginButton(Context context) {
        super(context);
    }

    public LoginButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public LoginButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void init(Activity activity, Facebook fb) {
        init(activity, fb, new String[0]);
    }

    public void init(Activity activity, Facebook fb, String[] permissions) {
        int i;
        this.mActivity = activity;
        this.mFb = fb;
        this.mPermissions = permissions;
        this.mHandler = new Handler();
        setBackgroundColor(0);
        setAdjustViewBounds(true);
        if (fb.isSessionValid()) {
            i = R.drawable.logout_down;
        } else {
            i = R.drawable.login_down;
        }
        setImageResource(i);
        drawableStateChanged();
        SessionEvents.addAuthListener(this.mSessionListener);
        SessionEvents.addLogoutListener(this.mSessionListener);
        setOnClickListener(new ButtonOnClickListener(this, null));
    }

    private final class ButtonOnClickListener implements View.OnClickListener {
        private ButtonOnClickListener() {
        }

        /* synthetic */ ButtonOnClickListener(LoginButton loginButton, ButtonOnClickListener buttonOnClickListener) {
            this();
        }

        public void onClick(View arg0) {
            if (LoginButton.this.mFb.isSessionValid()) {
                SessionEvents.onLogoutBegin();
                new AsyncFacebookRunner(LoginButton.this.mFb).logout(LoginButton.this.getContext(), new LogoutRequestListener(LoginButton.this, null));
                return;
            }
            LoginButton.this.mFb.authorize(LoginButton.this.mActivity, LoginButton.this.mPermissions, new LoginDialogListener(LoginButton.this, null));
        }
    }

    private final class LoginDialogListener implements Facebook.DialogListener {
        private LoginDialogListener() {
        }

        /* synthetic */ LoginDialogListener(LoginButton loginButton, LoginDialogListener loginDialogListener) {
            this();
        }

        public void onComplete(Bundle values) {
            SessionEvents.onLoginSuccess();
        }

        public void onFacebookError(FacebookError error) {
            SessionEvents.onLoginError(error.getMessage());
        }

        public void onError(DialogError error) {
            SessionEvents.onLoginError(error.getMessage());
        }

        public void onCancel() {
            SessionEvents.onLoginError("Action Canceled");
        }
    }

    private class LogoutRequestListener extends BaseRequestListener {
        private LogoutRequestListener() {
        }

        /* synthetic */ LogoutRequestListener(LoginButton loginButton, LogoutRequestListener logoutRequestListener) {
            this();
        }

        public void onComplete(String response, Object state) {
            LoginButton.this.mHandler.post(new Runnable() {
                public void run() {
                    SessionEvents.onLogoutFinish();
                }
            });
        }
    }

    private class SessionListener implements SessionEvents.AuthListener, SessionEvents.LogoutListener {
        private SessionListener() {
        }

        /* synthetic */ SessionListener(LoginButton loginButton, SessionListener sessionListener) {
            this();
        }

        public void onAuthSucceed() {
            LoginButton.this.setImageResource(R.drawable.logout_down);
            SessionStore.save(LoginButton.this.mFb, LoginButton.this.getContext());
        }

        public void onAuthFail(String error) {
        }

        public void onLogoutBegin() {
        }

        public void onLogoutFinish() {
            SessionStore.clear(LoginButton.this.getContext());
            LoginButton.this.setImageResource(R.drawable.login_down);
        }
    }
}
