package mediba.ad.sdk.android;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import mediba.ad.sdk.android.AdOverlay;

final class q extends WebViewClient {
    private boolean a;
    private /* synthetic */ AdOverlay.Builder b;

    /* synthetic */ q(AdOverlay.Builder builder) {
        this(builder, (byte) 0);
    }

    private q(AdOverlay.Builder builder, byte b2) {
        this.b = builder;
        this.a = false;
    }

    public final void onPageFinished(WebView webView, String str) {
        super.onPageFinished(webView, str);
        String title = this.b.g.getTitle();
        if (title != null && title.length() > 0) {
            this.b.d.setText(title);
        }
        this.b.i.dismiss();
        this.b.b.setVisibility(0);
        AdContainer.setProgressbarVisibility(4);
        AdContainer.setActionIconVisibility(0);
    }

    public final void onPageStarted(WebView webView, String str, Bitmap bitmap) {
        super.onPageStarted(webView, str, bitmap);
        this.b.i.show();
    }

    public final void onReceivedError(WebView webView, int i, String str, String str2) {
        super.onReceivedError(webView, i, str, str2);
    }

    public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
        super.shouldOverrideUrlLoading(webView, str);
        if (!this.a) {
            webView.loadUrl(str);
            this.a = true;
        } else if (this.a) {
            this.b.a.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
        }
        return true;
    }
}
