package mediba.ad.sdk.android;

import android.util.Log;
import java.lang.ref.WeakReference;

final class w implements Runnable {
    boolean a;
    private WeakReference b;

    public w(MasAdView masAdView) {
        this.b = new WeakReference(masAdView);
    }

    public final void run() {
        try {
            MasAdView masAdView = (MasAdView) this.b.get();
            if (!this.a && masAdView != null) {
                Log.d("MasAdView", "Requesting a fresh ad because a request interval passed (" + (MasAdView.b(masAdView) / 1000) + " seconds).");
                MasAdView.c(masAdView);
            }
        } catch (Exception e) {
            if (Log.isLoggable("MasAdView", 6)) {
                Log.e("MasAdView", "exception caught in RefreshHandler.run(), " + e.getMessage());
            }
        }
    }
}
