package mediba.ad.sdk.android;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;

public class MasAdManager {
    private static String a = "MasAdManager";
    public static String address;
    public static String age;
    private static String b;
    private static String c;
    private static String d;
    private static String e;
    private static String f;
    private static int g;
    public static String gender;
    private static String h;

    static {
        Log.i("MasAdManager", "mediba_ad_sdk_android version 1.0.0.0");
    }

    private static void a(Context context) {
        try {
            PackageManager packageManager = context.getPackageManager();
            ApplicationInfo applicationInfo = packageManager.getApplicationInfo(context.getPackageName(), 128);
            if (applicationInfo != null) {
                if (MasAdView.getTestMode()) {
                    e = "z8YiPhJxo0";
                    setADID("A30100520133");
                } else if (applicationInfo.metaData != null) {
                    String string = applicationInfo.metaData.getString("apkey");
                    Log.d(a, "Manifest APKEY:" + string);
                    if (e == null && string != null) {
                        setAPIKEY(string);
                    }
                    String string2 = applicationInfo.metaData.getString("aid");
                    Log.d(a, "Manifest AID:" + string2);
                    if (d == null && string2 != null) {
                        setADID(string2);
                    }
                }
                f = applicationInfo.packageName;
                if (Log.isLoggable(a, 2)) {
                    Log.i(a, "Application's package name is " + f);
                }
            }
            PackageInfo packageInfo = packageManager.getPackageInfo(context.getPackageName(), 0);
            if (packageInfo != null) {
                g = packageInfo.versionCode;
                if (Log.isLoggable(a, 2)) {
                    Log.i(a, "Application's version number is " + g);
                }
            }
        } catch (Exception e2) {
            Log.e(a, "exception caught in MasAdManager parseManifest, " + e2.getMessage());
        }
    }

    private static void a(StringBuilder sb, String str, String str2) {
        if (str2 != null && str2.length() > 0) {
            try {
                sb.append("&").append(str).append("=").append(str2);
            } catch (Exception e2) {
                Log.e(a, "exception caught in AdRequestor extend_request_param, " + e2.getMessage());
            }
        }
    }

    public static void clientError(String str) {
        Log.e(a, str);
    }

    public static String getADID(Context context) {
        if (d == null) {
            a(context);
        }
        if (d == null && Log.isLoggable(a, 6)) {
            Log.e(a, "ADID not defined");
        }
        return d;
    }

    public static String getAPIKEY(Context context) {
        if (e == null) {
            a(context);
        }
        if (e == null && Log.isLoggable(a, 6)) {
            Log.e(a, "APKEY not defined");
        }
        return e;
    }

    public static String getAddress() {
        return address;
    }

    public static String getAge() {
        return age;
    }

    public static String getAndroidId(Context context) {
        if (b == null) {
            b = Settings.Secure.getString(context.getContentResolver(), "android_id");
        }
        return b;
    }

    public static String getDeviceId(Context context, boolean z) {
        if (c == null && z) {
            c = ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
        }
        return c;
    }

    public static String getDisplay(Context context) {
        int i;
        try {
            int i2 = context.getResources().getConfiguration().orientation;
            int screenWidth = i2 == 1 ? getScreenWidth(context) : getScreenHeight(context);
            int screenHeight = i2 == 1 ? getScreenHeight(context) : getScreenWidth(context);
            switch (screenWidth) {
                case 240:
                    if (screenHeight != 320) {
                        if (screenHeight != 400) {
                            i = 0;
                            break;
                        } else {
                            i = 2;
                            break;
                        }
                    } else {
                        i = 1;
                        break;
                    }
                case 320:
                    if (screenHeight != 480) {
                        i = 0;
                        break;
                    } else {
                        i = 3;
                        break;
                    }
                case 360:
                    if (screenHeight != 640) {
                        i = 0;
                        break;
                    } else {
                        i = 4;
                        break;
                    }
                case 480:
                    if (screenHeight != 800) {
                        if (screenHeight != 854) {
                            if (screenHeight != 864) {
                                if (screenHeight != 960) {
                                    if (screenHeight != 1024) {
                                        i = 0;
                                        break;
                                    } else {
                                        i = 9;
                                        break;
                                    }
                                } else {
                                    i = 8;
                                    break;
                                }
                            } else {
                                i = 7;
                                break;
                            }
                        } else {
                            i = 6;
                            break;
                        }
                    } else {
                        i = 5;
                        break;
                    }
                default:
                    i = 0;
                    break;
            }
            return Integer.toString(i);
        } catch (Exception e2) {
            Log.e(a, "exception caught in MasAdManager getDisplay, " + e2.getMessage());
            return null;
        }
    }

    public static String getGender() {
        return gender;
    }

    public static String getIpAddress(Context context) {
        try {
            Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
            while (networkInterfaces.hasMoreElements()) {
                Enumeration<InetAddress> inetAddresses = networkInterfaces.nextElement().getInetAddresses();
                while (true) {
                    if (inetAddresses.hasMoreElements()) {
                        InetAddress nextElement = inetAddresses.nextElement();
                        if (!nextElement.isLoopbackAddress()) {
                            return nextElement.getHostAddress().toString();
                        }
                    }
                }
            }
        } catch (SocketException e2) {
            Log.e(a, "SocketException caught in MasAdManager getIpAddress, " + e2.getMessage());
        } catch (Exception e3) {
            Log.e(a, "exception caught in MasAdManager getIpAddress, " + e3.getMessage());
        }
        return null;
    }

    public static String getParams(Context context, boolean z) {
        boolean z2;
        boolean z3;
        boolean z4;
        String replace = ((TelephonyManager) context.getSystemService("phone")).getNetworkOperatorName().replace(" ", "");
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        if (z) {
            try {
                z4 = connectivityManager.getNetworkInfo(1).isConnectedOrConnecting();
            } catch (Exception e2) {
                Log.w(a, "no supported device.isWifi");
                z4 = false;
            }
            try {
                boolean z5 = z4;
                z3 = connectivityManager.getNetworkInfo(0).isConnectedOrConnecting();
                z2 = z5;
            } catch (Exception e3) {
                Log.w(a, "no supported device.isMobile");
                z2 = z4;
                z3 = false;
            }
        } else {
            z2 = false;
            z3 = false;
        }
        String str = Build.VERSION.RELEASE.split("-")[0];
        String display = getDisplay(context);
        String num = Integer.toString(context.getResources().getConfiguration().orientation);
        String str2 = z3 ? "1" : z2 ? "2" : null;
        String language = context.getResources().getConfiguration().locale.getLanguage();
        String country = context.getResources().getConfiguration().locale.getCountry();
        String gender2 = getGender();
        String age2 = getAge();
        String address2 = getAddress();
        StringBuilder sb = new StringBuilder();
        a(sb, "nv01", str);
        a(sb, "nv02", display);
        a(sb, "nv03", num);
        a(sb, "nv04", replace);
        a(sb, "nv05", str2);
        a(sb, "nv06", language);
        a(sb, "nv07", country);
        a(sb, "K001", gender2);
        a(sb, "K003", age2);
        a(sb, "K006", address2);
        String sb2 = sb.toString();
        h = sb2;
        return sb2;
    }

    public static int getScreenHeight(Context context) {
        Display defaultDisplay = ((WindowManager) context.getSystemService("window")).getDefaultDisplay();
        if (defaultDisplay != null) {
            return defaultDisplay.getHeight();
        }
        return 0;
    }

    public static int getScreenWidth(Context context) {
        Display defaultDisplay = ((WindowManager) context.getSystemService("window")).getDefaultDisplay();
        if (defaultDisplay != null) {
            return defaultDisplay.getWidth();
        }
        return 0;
    }

    public static void setADID(String str) {
        if (!str.startsWith("A")) {
            Log.e(a, "ADIDが不正にセットされています。");
        }
        d = str.substring(1, str.length());
    }

    public static void setAPIKEY(String str) {
        e = str;
    }
}
