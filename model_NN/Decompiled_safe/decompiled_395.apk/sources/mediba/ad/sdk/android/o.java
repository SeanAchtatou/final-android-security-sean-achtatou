package mediba.ad.sdk.android;

import android.graphics.Bitmap;
import android.view.MotionEvent;
import android.view.View;
import mediba.ad.sdk.android.AdOverlay;

final class o implements View.OnTouchListener {
    private /* synthetic */ AdOverlay.Builder a;
    private final /* synthetic */ Bitmap b;
    private final /* synthetic */ Bitmap c;

    o(AdOverlay.Builder builder, Bitmap bitmap, Bitmap bitmap2) {
        this.a = builder;
        this.b = bitmap;
        this.c = bitmap2;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case 0:
                this.a.f.setImageBitmap(this.b);
                return false;
            case 1:
                this.a.f.setImageBitmap(this.c);
                return false;
            default:
                return false;
        }
    }
}
