package mediba.ad.sdk.android;

import android.util.Log;
import java.lang.ref.WeakReference;

final class n implements Runnable {
    private WeakReference a;

    public n(AdContainer adContainer) {
        Log.d("AdContainer", "Loading Construct");
        this.a = new WeakReference(adContainer);
    }

    public final void run() {
        Log.d("AdContainer", "Running Progress");
        try {
            AdContainer adContainer = (AdContainer) this.a.get();
            if (adContainer != null) {
                adContainer.addView(adContainer.d);
            }
        } catch (Exception e) {
            Log.e("AdContainer", "exception caught in AdContainer post run(), " + e.getMessage());
        }
    }
}
