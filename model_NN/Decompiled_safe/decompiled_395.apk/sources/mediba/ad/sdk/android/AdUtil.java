package mediba.ad.sdk.android;

public class AdUtil {
    public static void onFailedToReceiveAd(MasAdView masAdView) {
        MasAdProxyListener masAdProxyListener;
        if (masAdView != null && (masAdProxyListener = (MasAdProxyListener) masAdView.getAdListener()) != null) {
            if (masAdView.isRefreshed) {
                masAdProxyListener.onFailedToReceiveRefreshedAd(masAdView);
            } else {
                masAdProxyListener.onFailedToReceiveAd(masAdView);
            }
        }
    }

    public static void onReceiveAd(MasAdView masAdView) {
        MasAdProxyListener masAdProxyListener;
        if (masAdView != null && (masAdProxyListener = (MasAdProxyListener) masAdView.getAdListener()) != null) {
            if (masAdView.isRefreshed) {
                masAdProxyListener.onReceiveRefreshedAd(masAdView);
            } else {
                masAdProxyListener.onReceiveAd(masAdView);
            }
        }
    }
}
