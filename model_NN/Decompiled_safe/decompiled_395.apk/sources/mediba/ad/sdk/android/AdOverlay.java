package mediba.ad.sdk.android;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.TextUtils;
import android.util.Log;
import android.webkit.WebView;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.JarURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class AdOverlay extends Dialog {

    public class Builder {
        private static float m = -1.0f;
        /* access modifiers changed from: private */
        public Context a;
        /* access modifiers changed from: private */
        public LinearLayout b;
        private LinearLayout c;
        /* access modifiers changed from: private */
        public TextView d;
        private ImageView e;
        /* access modifiers changed from: private */
        public ImageView f;
        /* access modifiers changed from: private */
        public WebView g;
        private RelativeLayout h;
        /* access modifiers changed from: private */
        public ProgressDialog i;
        private int j;
        private int k;
        private String l;

        public Builder(Context context, String str) {
            this.a = context;
            this.l = str;
            Log.d("AdOverlay", context.toString());
        }

        public AdOverlay create() {
            AdOverlay adOverlay = new AdOverlay(this.a);
            adOverlay.requestWindowFeature(1);
            m = this.a.getResources().getDisplayMetrics().density;
            this.j = MasAdManager.getScreenWidth(this.a);
            this.k = MasAdManager.getScreenHeight(this.a);
            int i2 = (int) (6.0f * m);
            this.i = new ProgressDialog(this.a);
            this.i.requestWindowFeature(1);
            this.i.setMessage("Loading");
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(this.j, this.k);
            this.b = new LinearLayout(this.a);
            this.b.setOrientation(1);
            this.b.setVisibility(4);
            this.h = new RelativeLayout(this.a);
            this.e = new ImageView(this.a);
            Bitmap imageBitmap = getImageBitmap(getClass().getClassLoader().getResource("mediba/ad/sdk/android/images/header_bg.png"));
            Bitmap imageBitmap2 = getImageBitmap(getClass().getClassLoader().getResource("mediba/ad/sdk/android/images/header_btn.png"));
            Bitmap imageBitmap3 = getImageBitmap(getClass().getClassLoader().getResource("mediba/ad/sdk/android/images/header_btn_over.png"));
            this.e.setImageBitmap(imageBitmap);
            this.e.setScaleType(ImageView.ScaleType.FIT_XY);
            this.e.setLayoutParams(new Gallery.LayoutParams(-1, (int) (40.0f * m)));
            this.h.addView(this.e);
            this.c = new LinearLayout(this.a);
            this.c.setOrientation(0);
            this.d = new TextView(this.a);
            this.d.setTextColor(-1);
            this.d.setWidth(this.j - ((int) (128.0f * m)));
            this.d.setSingleLine();
            this.d.setFocusable(true);
            this.d.setSelected(true);
            this.d.setEllipsize(TextUtils.TruncateAt.MARQUEE);
            this.d.setMarqueeRepeatLimit(10);
            this.d.setGravity(16);
            this.d.setPadding((int) (m * 10.0f), (int) (m * 10.0f), 0, 0);
            LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams((int) (86.0f * m), (int) (28.0f * m));
            layoutParams2.setMargins(i2, i2, i2, i2);
            this.f = new ImageView(this.a);
            this.f.setImageBitmap(imageBitmap2);
            this.f.setOnTouchListener(new o(this, imageBitmap3, imageBitmap2));
            this.f.setOnClickListener(new p(this, adOverlay));
            this.c.addView(this.d);
            this.c.addView(this.f, layoutParams2);
            this.h.addView(this.c);
            LinearLayout.LayoutParams layoutParams3 = new LinearLayout.LayoutParams(this.j - ((int) (25.0f * m)), this.k - ((int) (90.0f * m)));
            this.g = new WebView(this.a);
            this.g.setHorizontalScrollBarEnabled(true);
            this.g.setVerticalScrollBarEnabled(true);
            this.g.getSettings().setJavaScriptEnabled(true);
            this.g.setWebViewClient(new q(this));
            this.g.loadUrl(this.l);
            this.b.addView(this.h);
            this.b.addView(this.g, layoutParams3);
            adOverlay.addContentView(this.b, layoutParams);
            return adOverlay;
        }

        public Bitmap getImageBitmap(String str) {
            Bitmap bitmap;
            try {
                BufferedInputStream bufferedInputStream = new BufferedInputStream(((HttpURLConnection) new URL(str).openConnection()).getInputStream(), 1048576);
                bitmap = BitmapFactory.decodeStream(bufferedInputStream, null, new BitmapFactory.Options());
                try {
                    bufferedInputStream.close();
                    return bitmap;
                } catch (MalformedURLException e2) {
                    e = e2;
                    e.printStackTrace();
                    return bitmap;
                } catch (IOException e3) {
                    e = e3;
                    e.printStackTrace();
                    return bitmap;
                }
            } catch (MalformedURLException e4) {
                e = e4;
                bitmap = null;
            } catch (IOException e5) {
                e = e5;
                bitmap = null;
                e.printStackTrace();
                return bitmap;
            }
        }

        public Bitmap getImageBitmap(URL url) {
            Bitmap bitmap;
            try {
                BufferedInputStream bufferedInputStream = new BufferedInputStream(((JarURLConnection) url.openConnection()).getInputStream(), 1048576);
                bitmap = BitmapFactory.decodeStream(bufferedInputStream, null, new BitmapFactory.Options());
                try {
                    bufferedInputStream.close();
                    return bitmap;
                } catch (MalformedURLException e2) {
                    e = e2;
                    e.printStackTrace();
                    return bitmap;
                } catch (IOException e3) {
                    e = e3;
                    e.printStackTrace();
                    return bitmap;
                }
            } catch (MalformedURLException e4) {
                e = e4;
                bitmap = null;
            } catch (IOException e5) {
                e = e5;
                bitmap = null;
                e.printStackTrace();
                return bitmap;
            }
        }
    }

    public AdOverlay(Context context) {
        super(context);
    }

    public AdOverlay(Context context, int i) {
        super(context, i);
    }
}
