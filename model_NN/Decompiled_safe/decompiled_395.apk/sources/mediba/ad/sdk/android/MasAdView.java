package mediba.ad.sdk.android;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Handler;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.RelativeLayout;
import java.util.Vector;
import mediba.ad.sdk.android.AdProxy;

public class MasAdView extends RelativeLayout {
    static final Handler a = new Handler();
    private static String m;
    /* access modifiers changed from: private */
    public static int n;
    private static boolean o;
    private int b;
    private int c;
    private int d;
    private AdProxyListener e;
    /* access modifiers changed from: private */
    public AdContainer f;
    private int g;
    private int h;
    private boolean i;
    public boolean isRefreshed;
    private AdProxy.a j;
    private boolean k;
    private long l;
    private boolean p;
    private w q;

    static {
        new Vector();
    }

    public MasAdView(Activity activity) {
        this(activity, null, 0);
    }

    public MasAdView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public MasAdView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        boolean z;
        int i3;
        int i4;
        int i5 = 30;
        int i6 = 1;
        this.isRefreshed = false;
        this.i = true;
        this.p = true;
        setFocusable(true);
        setFocusableInTouchMode(true);
        setDescendantFocusability(262144);
        setClickable(true);
        setSelected(true);
        setGravity(17);
        if (attributeSet != null) {
            String str = "http://schemas.android.com/apk/res/" + context.getPackageName();
            int attributeUnsignedIntValue = attributeSet.getAttributeUnsignedIntValue(str, "backgroundColor", 1);
            int attributeUnsignedIntValue2 = attributeSet.getAttributeUnsignedIntValue(str, "requestInterval", 30);
            int attributeUnsignedIntValue3 = attributeSet.getAttributeUnsignedIntValue(str, "refreshAnimation", 1);
            int attributeUnsignedIntValue4 = attributeSet.getAttributeUnsignedIntValue(str, "visibility", 0);
            z = attributeSet.getAttributeBooleanValue(str, "testMode", false);
            int i7 = attributeUnsignedIntValue4;
            i6 = attributeUnsignedIntValue;
            i3 = i7;
            int i8 = attributeUnsignedIntValue2;
            i4 = attributeUnsignedIntValue3;
            i5 = i8;
        } else {
            z = false;
            i3 = 0;
            i4 = 1;
        }
        setBackgroundColor(i6);
        setRequestInterval(i5);
        setRefreshAnimation(i4);
        setVisibility(i3);
        testMode(z);
        setUserAgent(context);
        Log.i("MasAdView", "create ads by mediba");
    }

    private void a(int i2) {
        this.b = -16777216 | i2;
        this.c = -16777216 | i2;
    }

    static void a(AdContainer adContainer) {
        adContainer.setVisibility(8);
    }

    static void a(MasAdView masAdView) {
        if (masAdView.e != null) {
            Handler handler = a;
            masAdView.getClass();
            handler.post(new u(masAdView));
        }
    }

    static void a(MasAdView masAdView, AdContainer adContainer) {
        masAdView.f = adContainer;
    }

    static /* synthetic */ void a(MasAdView masAdView, AdProxy adProxy) {
        if (masAdView.e != null) {
            try {
                AdUtil.onReceiveAd(masAdView);
            } catch (Exception e2) {
                Log.w("MasAdView", "Unhandled exception raised in your AdListener.onReceiveAd.", e2);
            }
        }
    }

    static /* synthetic */ void a(boolean z) {
    }

    static int b(MasAdView masAdView) {
        return masAdView.g;
    }

    static AdContainer b(MasAdView masAdView, AdContainer adContainer) {
        masAdView.f = adContainer;
        return adContainer;
    }

    private void b() {
        if ((this.i || super.getVisibility() == 0) && !this.k) {
            this.l = SystemClock.uptimeMillis();
            this.k = true;
            new AdRequestThread(this).start();
        }
    }

    private void b(boolean z) {
        boolean z2;
        AdProxy adProxyInstance;
        synchronized (this) {
            if (z) {
                if (this.g > 0 && getVisibility() == 0) {
                    int i2 = this.g;
                    c();
                    if (this.f == null || (adProxyInstance = this.f.getAdProxyInstance()) == null || !adProxyInstance.b() || this.f.g() >= 120) {
                        z2 = true;
                    } else {
                        Log.d("MasAdView", "Cannot refresh CPM ads.  Ignoring request to refresh the ad.");
                        z2 = false;
                    }
                    if (z2 && this.p) {
                        getClass();
                        this.q = new w(this);
                        a.postDelayed(this.q, (long) i2);
                        Log.d("MasAdView", "Ad refresh scheduled for " + i2 + " from now.");
                    }
                }
            }
            if (!z || this.g == 0 || getVisibility() != 0) {
                c();
            }
        }
    }

    private void c() {
        if (this.q != null) {
            this.q.a = true;
            this.q = null;
            Log.v("MasAdView", "Cancelled an ad refresh scheduled for the future.");
        }
    }

    static void c(MasAdView masAdView) {
        masAdView.b();
    }

    static AdProxy.a d(MasAdView masAdView) {
        if (masAdView.j == null) {
            masAdView.j = new AdProxy.a(masAdView);
        }
        return masAdView.j;
    }

    static boolean e(MasAdView masAdView) {
        masAdView.k = false;
        return false;
    }

    static void f(MasAdView masAdView) {
        masAdView.b(true);
    }

    static long g(MasAdView masAdView) {
        return masAdView.l;
    }

    protected static boolean getTestMode() {
        return o;
    }

    protected static String getUserAgent() {
        return m;
    }

    static AdContainer h(MasAdView masAdView) {
        return masAdView.f;
    }

    /* access modifiers changed from: package-private */
    public final void a(AdProxy adProxy, AdContainer adContainer) {
        int visibility = super.getVisibility();
        double a2 = adProxy.a();
        if (a2 >= 0.0d) {
            setRequestInterval((int) a2);
            b(true);
        }
        if (this.i) {
            this.i = false;
        }
        adContainer.a(adProxy);
        adContainer.setVisibility(visibility);
        adContainer.setGravity(17);
        adProxy.setAdContainer(adContainer);
        adContainer.setLayoutParams(new RelativeLayout.LayoutParams(adProxy.a(adProxy.e()), adProxy.a(adProxy.getContainerHeight())));
        a.post(new v(this, this, adContainer, visibility, true));
    }

    public AdProxyListener getAdListener() {
        return this.e;
    }

    public int getBackgroundColor() {
        return this.d;
    }

    /* access modifiers changed from: protected */
    public int getPrimaryTextColor() {
        return this.b;
    }

    public int getRequestInterval() {
        return this.g;
    }

    /* access modifiers changed from: protected */
    public int getSecondaryTextColor() {
        return this.c;
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        b(true);
        super.onAttachedToWindow();
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        b(false);
        super.onDetachedFromWindow();
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        int i4;
        AdProxy adProxyInstance;
        super.onMeasure(i2, i3);
        int size = View.MeasureSpec.getSize(i2);
        int size2 = View.MeasureSpec.getSize(i3);
        int mode = View.MeasureSpec.getMode(i2);
        int mode2 = View.MeasureSpec.getMode(i3);
        int screenWidth = (mode == Integer.MIN_VALUE || mode == 1073741824) ? size : MasAdManager.getScreenWidth(getContext());
        if (mode2 == 1073741824) {
            i4 = size2;
        } else if (this.f == null || (adProxyInstance = this.f.getAdProxyInstance()) == null) {
            i4 = 0;
        } else {
            int a2 = adProxyInstance.a(adProxyInstance.getContainerHeight());
            i4 = (mode2 != Integer.MIN_VALUE || size2 >= a2) ? a2 : size2;
        }
        setMeasuredDimension(screenWidth, i4);
        Log.v("MasAdView", "AdView.onMeasure:  widthSize " + size + " heightSize " + size2);
        Log.v("MasAdView", "AdView.onMeasure:  measuredWidth " + screenWidth + " measuredHeight " + i4);
        int measuredWidth = getMeasuredWidth();
        Log.d("MasAdView", "AdView size is " + measuredWidth + " by " + getMeasuredHeight());
        if (this.f != null && ((float) measuredWidth) / AdContainer.b() <= 310.0f) {
            Log.w("MasAdView", "表示領域の横幅が狭すぎます。");
            c();
        } else if (this.i) {
            b();
        }
    }

    public void onWindowFocusChanged(boolean z) {
        b(z);
    }

    /* access modifiers changed from: protected */
    public void onWindowVisibilityChanged(int i2) {
        b(i2 == 0);
    }

    public void setAdListener(AdProxyListener adProxyListener) {
        synchronized (this) {
            this.e = adProxyListener;
        }
    }

    public void setAddress(int i2) {
        if (i2 <= 0 || i2 > 47) {
            Log.w("MasAdView", "Function AdView.setAddress(int) must be 1 ‾ 47");
        } else {
            MasAdManager.address = Integer.toString(i2);
        }
    }

    public void setAge(String str) {
        String upperCase = str.toUpperCase();
        if ("ABCDEFGHIJKLMNOPQ".matches(".*" + upperCase + ".*")) {
            MasAdManager.age = upperCase;
        } else {
            Log.w("MasAdView", "Function AdView.setAge(String) must be 1 Character ( A ‾ Q )");
        }
    }

    public void setBackgroundColor(int i2) {
        if (i2 == 1 || i2 == 2) {
            this.d = i2;
        } else {
            Log.w("MasAdView", "setBackgroundColor(int) must be 1(Black) or 2(White). Default is 1.");
            this.d = 1;
        }
        if (this.d == 1) {
            a(Color.rgb(255, 255, 255));
        } else if (this.d == 2) {
            a(Color.rgb(0, 0, 0));
        }
        invalidate();
    }

    public void setEnabled(boolean z) {
        super.setEnabled(z);
        this.p = z;
        if (z) {
            setVisibility(0);
        } else {
            setVisibility(8);
        }
    }

    public void setGender(int i2) {
        if (i2 == 1 || i2 == 2) {
            MasAdManager.gender = Integer.toString(i2);
        } else {
            Log.w("MasAdView", "Function AdView.setGender(int) must be 1(Male) or 2(Female)");
        }
    }

    public void setRefreshAnimation(int i2) {
        if (i2 == 0 || i2 == 1 || i2 == 2 || i2 == 3 || i2 == 4) {
            n = i2;
        } else {
            Log.w("MasAdView", "setRefreshAnimation(int) must be 0 or 1 or 2 or 3 or 4. Default is 1.");
            n = 1;
        }
        if (n == 0) {
            c();
            this.h = this.g;
            this.g = 0;
        } else if (n == 1 && this.h > 0) {
            this.g = this.h;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:7:0x0037  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void setRequestInterval(int r6) {
        /*
            r5 = this;
            r4 = 300(0x12c, float:4.2E-43)
            r3 = 30
            int r0 = r6 * 1000
            int r1 = r5.g
            if (r1 == r0) goto L_0x0058
            if (r6 <= 0) goto L_0x0082
            if (r6 >= r3) goto L_0x0059
            java.lang.String r0 = "MasAdView"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "AdView.setRequestInterval("
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r6)
            java.lang.String r2 = ") seconds must be >= "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r3)
            java.lang.String r1 = r1.toString()
            android.util.Log.w(r0, r1)
            r0 = 30000(0x7530, float:4.2039E-41)
            r1 = r3
        L_0x0033:
            r5.g = r0
            if (r1 > 0) goto L_0x003a
            r5.c()
        L_0x003a:
            java.lang.String r0 = "MasAdView"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Requesting fresh ads every "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r1 = r2.append(r1)
            java.lang.String r2 = " seconds."
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            android.util.Log.i(r0, r1)
        L_0x0058:
            return
        L_0x0059:
            if (r6 <= r4) goto L_0x0082
            java.lang.String r0 = "MasAdView"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "AdView.setRequestInterval("
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r6)
            java.lang.String r2 = ") seconds must be <= "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r4)
            java.lang.String r1 = r1.toString()
            android.util.Log.w(r0, r1)
            r0 = 300000(0x493e0, float:4.2039E-40)
            r1 = r4
            goto L_0x0033
        L_0x0082:
            r1 = r6
            goto L_0x0033
        */
        throw new UnsupportedOperationException("Method not decompiled: mediba.ad.sdk.android.MasAdView.setRequestInterval(int):void");
    }

    /* access modifiers changed from: protected */
    public void setUserAgent(Context context) {
        m = new WebView(context).getSettings().getUserAgentString();
    }

    public void setVisibility(int i2) {
        if (super.getVisibility() != i2) {
            synchronized (this) {
                int childCount = getChildCount();
                for (int i3 = 0; i3 < childCount; i3++) {
                    a.post(new t(this, getChildAt(i3), i2));
                }
                super.setVisibility(i2);
                invalidate();
            }
        }
        b(i2 == 0);
    }

    public void startRequest() {
        this.p = true;
        b();
    }

    public void stopRequest() {
        this.p = false;
        b(false);
    }

    public void testMode(boolean z) {
        o = z;
    }
}
