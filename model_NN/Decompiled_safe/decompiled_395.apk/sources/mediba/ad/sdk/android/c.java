package mediba.ad.sdk.android;

import android.content.Intent;
import android.net.Uri;
import mediba.ad.sdk.android.AdOverlay;

final class c implements Runnable {
    private /* synthetic */ b a;
    private final /* synthetic */ String b;
    private final /* synthetic */ String c;

    c(b bVar, String str, String str2) {
        this.a = bVar;
        this.b = str;
        this.c = str2;
    }

    public final void run() {
        if (this.b.equals("nomal")) {
            AdContainer.intent = new Intent("android.intent.action.VIEW", Uri.parse(this.c));
            AdContainer.r.startActivity(AdContainer.intent);
        } else if (this.b.equals("overlay")) {
            new AdOverlay.Builder(this.a.a.getContext(), this.c).create().show();
        }
    }
}
