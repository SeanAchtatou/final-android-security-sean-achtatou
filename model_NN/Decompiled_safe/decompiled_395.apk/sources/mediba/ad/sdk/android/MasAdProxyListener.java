package mediba.ad.sdk.android;

public abstract class MasAdProxyListener implements AdProxyListener {
    private String a;

    public MasAdProxyListener(String str) {
        this.a = str;
    }

    public String getTag() {
        return this.a;
    }

    public abstract void onFailedToReceiveAd(MasAdView masAdView);

    public abstract void onFailedToReceiveRefreshedAd(MasAdView masAdView);

    public abstract void onReceiveAd(MasAdView masAdView);

    public abstract void onReceiveRefreshedAd(MasAdView masAdView);

    public void setTag(String str) {
        this.a = str;
    }
}
