package mediba.ad.sdk.android;

import android.os.Handler;
import android.view.View;

final class b implements View.OnClickListener {
    /* access modifiers changed from: private */
    public /* synthetic */ AdContainer a;
    private final /* synthetic */ String b;
    private final /* synthetic */ String c;

    b(AdContainer adContainer, String str, String str2) {
        this.a = adContainer;
        this.b = str;
        this.c = str2;
    }

    public final void onClick(View view) {
        this.a.q.setClickable(false);
        Handler handler = new Handler();
        handler.post(new c(this, this.b, this.c));
        handler.postDelayed(new d(this), 1000);
    }
}
