package mediba.ad.sdk.android;

import android.content.Intent;
import android.net.Uri;
import mediba.ad.sdk.android.AdOverlay;

final class h implements Runnable {
    private /* synthetic */ f a;
    private final /* synthetic */ String b;
    private final /* synthetic */ String c;

    h(f fVar, String str, String str2) {
        this.a = fVar;
        this.b = str;
        this.c = str2;
    }

    public final void run() {
        if (this.b.equals("nomal")) {
            AdContainer.intent = new Intent("android.intent.action.VIEW", Uri.parse(this.c));
            AdContainer.r.startActivity(AdContainer.intent);
        } else if (this.b.equals("overlay")) {
            new AdOverlay.Builder(this.a.a.getContext(), this.c).create().show();
        }
    }
}
