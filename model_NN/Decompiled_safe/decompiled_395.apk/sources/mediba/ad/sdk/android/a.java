package mediba.ad.sdk.android;

import android.view.MotionEvent;
import android.view.View;

final class a implements View.OnTouchListener {
    private /* synthetic */ AdContainer a;

    a(AdContainer adContainer) {
        this.a = adContainer;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case 0:
                this.a.setPressed(true);
                break;
            case 1:
                if (this.a.q.isPressed()) {
                    this.a.q.performClick();
                }
                this.a.setPressed(false);
                break;
        }
        return false;
    }
}
