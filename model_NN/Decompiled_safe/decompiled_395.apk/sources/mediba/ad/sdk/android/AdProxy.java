package mediba.ad.sdk.android;

import android.content.Intent;
import android.graphics.Rect;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import java.lang.ref.WeakReference;
import java.util.Hashtable;
import java.util.Vector;

public class AdProxy implements View.OnClickListener, AdProxyConnectorListener {
    private static String b;
    private static String c;
    private static String d;
    private static String e;
    private static String f;
    private static String g;
    private static String h;
    public static Intent intent;
    protected View a;
    private int i = -1;
    private int j = 48;
    private a k = null;
    private Hashtable l = new Hashtable();
    private double m;
    protected AdContainer mAdcontainer;
    private double n;

    public class a {
        private WeakReference a;

        public a(MasAdView masAdView) {
            this.a = new WeakReference(masAdView);
        }

        public void a() {
            MasAdView masAdView = (MasAdView) this.a.get();
            if (masAdView != null) {
                MasAdView.a(masAdView);
            }
        }

        /* access modifiers changed from: package-private */
        public final void a(AdProxy adProxy) {
            MasAdView masAdView = (MasAdView) this.a.get();
            if (masAdView != null) {
                synchronized (masAdView) {
                    try {
                        if (MasAdView.h(masAdView) == null || !adProxy.equals(MasAdView.h(masAdView).getAdProxyInstance())) {
                            Log.i("AdProxy", "Ad Response (" + (SystemClock.uptimeMillis() - MasAdView.g(masAdView)) + " ms)");
                            masAdView.getContext();
                            masAdView.a(adProxy, adProxy.getAdContainer());
                        } else {
                            Log.d("AdProxy", "Same AD SKIP");
                        }
                    } catch (Exception e) {
                        Log.e("AdProxy", "Handler Illeguler in AdProxy.a(AdProxy adproxy) " + e.getMessage());
                    }
                }
                return;
            }
            return;
        }
    }

    public interface b {
        void a();
    }

    protected AdProxy() {
        new Vector();
        this.m = -1.0d;
        this.n = -1.0d;
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0077  */
    /* JADX WARNING: Removed duplicated region for block: B:20:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static mediba.ad.sdk.android.AdProxy a(mediba.ad.sdk.android.AdProxy.a r5, java.lang.String r6, java.lang.String[] r7, int r8, int r9, int r10, mediba.ad.sdk.android.AdContainer r11) {
        /*
            r4 = 0
            mediba.ad.sdk.android.AdProxy r0 = new mediba.ad.sdk.android.AdProxy
            r0.<init>()
            r0.k = r5
            r0.mAdcontainer = r11
            org.json.JSONObject r1 = new org.json.JSONObject
            r1.<init>(r6)
            int r2 = r1.length()
            r3 = 6
            if (r2 == r3) goto L_0x0026
            int r2 = r1.length()
            r3 = 3
            if (r2 == r3) goto L_0x0026
            java.lang.String r0 = "AdProxy"
            java.lang.String r1 = "不正なADを所得しました。"
            android.util.Log.w(r0, r1)
            r0 = r4
        L_0x0025:
            return r0
        L_0x0026:
            java.lang.String r2 = "type"
            java.lang.String r2 = r1.getString(r2)
            java.lang.String r2 = r2.toString()
            mediba.ad.sdk.android.AdProxy.b = r2
            float r2 = mediba.ad.sdk.android.AdContainer.b()
            double r2 = (double) r2
            r0.n = r2
            mediba.ad.sdk.android.AdProxy.c = r4
            mediba.ad.sdk.android.AdProxy.d = r4
            mediba.ad.sdk.android.AdProxy.e = r4
            mediba.ad.sdk.android.AdProxy.f = r4
            mediba.ad.sdk.android.AdProxy.g = r4
            mediba.ad.sdk.android.AdProxy.h = r4
            java.lang.String r2 = mediba.ad.sdk.android.AdProxy.b
            java.lang.String r3 = "1"
            boolean r2 = r2.equals(r3)
            if (r2 != 0) goto L_0x0059
            java.lang.String r2 = mediba.ad.sdk.android.AdProxy.b
            java.lang.String r3 = "3"
            boolean r2 = r2.equals(r3)
            if (r2 == 0) goto L_0x0079
        L_0x0059:
            java.lang.String r2 = "imageurl"
            java.lang.String r2 = r1.getString(r2)
            java.lang.String r2 = r2.toString()
            mediba.ad.sdk.android.AdProxy.f = r2
            java.lang.String r2 = "clickurl"
            java.lang.String r1 = r1.getString(r2)
            java.lang.String r1 = r1.toString()
            mediba.ad.sdk.android.AdProxy.c = r1
        L_0x0071:
            r0.c()
            r1 = 1
        L_0x0075:
            if (r1 != 0) goto L_0x0025
            r0 = r4
            goto L_0x0025
        L_0x0079:
            java.lang.String r2 = mediba.ad.sdk.android.AdProxy.b
            java.lang.String r3 = "2"
            boolean r2 = r2.equals(r3)
            if (r2 != 0) goto L_0x008d
            java.lang.String r2 = mediba.ad.sdk.android.AdProxy.b
            java.lang.String r3 = "4"
            boolean r2 = r2.equals(r3)
            if (r2 == 0) goto L_0x00ca
        L_0x008d:
            java.lang.String r2 = "icon1url"
            java.lang.String r2 = r1.getString(r2)
            java.lang.String r2 = r2.toString()
            mediba.ad.sdk.android.AdProxy.g = r2
            java.lang.String r2 = "icon2url"
            java.lang.String r2 = r1.getString(r2)
            java.lang.String r2 = r2.toString()
            mediba.ad.sdk.android.AdProxy.h = r2
            java.lang.String r2 = "text1"
            java.lang.String r2 = r1.getString(r2)
            java.lang.String r2 = r2.toString()
            mediba.ad.sdk.android.AdProxy.d = r2
            java.lang.String r2 = "text2"
            java.lang.String r2 = r1.getString(r2)
            java.lang.String r2 = r2.toString()
            mediba.ad.sdk.android.AdProxy.e = r2
            java.lang.String r2 = "clickurl"
            java.lang.String r1 = r1.getString(r2)
            java.lang.String r1 = r1.toString()
            mediba.ad.sdk.android.AdProxy.c = r1
            goto L_0x0071
        L_0x00ca:
            java.lang.String r1 = "AdProxy"
            java.lang.String r2 = "不正なADを所得しました。"
            android.util.Log.w(r1, r2)
            r1 = 0
            goto L_0x0075
        */
        throw new UnsupportedOperationException("Method not decompiled: mediba.ad.sdk.android.AdProxy.a(mediba.ad.sdk.android.AdProxy$a, java.lang.String, java.lang.String[], int, int, int, mediba.ad.sdk.android.AdContainer):mediba.ad.sdk.android.AdProxy");
    }

    static void a(AdProxy adProxy) {
        if (adProxy.k != null) {
            adProxy.k.a(adProxy);
        }
    }

    private void c() {
        try {
            r rVar = new r(this.mAdcontainer, this);
            d();
            MasAdView.a.post(rVar);
            if (this.l != null) {
                this.l.clear();
                this.l = null;
            }
        } catch (Exception e2) {
            Log.e("AdProxy", "Handler Illeguler in AdProxy.constructView ");
        }
    }

    private boolean d() {
        new Rect(50, 50, 50, 50);
        try {
            this.mAdcontainer.getContext();
            if (b.equals("1")) {
                this.mAdcontainer.setImageView(f);
                this.mAdcontainer.setImageLink(c, "nomal");
                return true;
            } else if (b.equals("2")) {
                this.mAdcontainer.setIconView(d, e, g, h);
                this.mAdcontainer.setIconLink(c, "nomal");
                return true;
            } else if (b.equals("3")) {
                this.mAdcontainer.setImageView(f);
                this.mAdcontainer.setImageLink(c, "overlay");
                return true;
            } else if (!b.equals("4")) {
                return true;
            } else {
                this.mAdcontainer.setIconView(d, e, g, h);
                this.mAdcontainer.setIconLink(c, "overlay");
                return true;
            }
        } catch (Exception e2) {
            Log.e("AdProxy", "Handler Illeguler in AdProxy.createInnerView " + e2.getMessage());
            return true;
        }
    }

    private void f() {
        if (this.l != null) {
            this.l.clear();
            this.l = null;
        }
        if (this.k != null) {
            this.k.a();
        }
    }

    /* access modifiers changed from: package-private */
    public final double a() {
        return this.m;
    }

    /* access modifiers changed from: package-private */
    public final int a(int i2) {
        double d2 = (double) i2;
        if (this.n > 0.0d) {
            d2 *= this.n;
        }
        return (int) d2;
    }

    public void a(AdProxyConnector adProxyConnector) {
        String e2 = adProxyConnector.e();
        byte[] d2 = adProxyConnector.d();
        if (d2 != null) {
            this.l.put(e2, d2);
            c();
            return;
        }
        Log.d("AdProxy", "Failed reading asset(" + e2 + ") for ad");
        f();
    }

    public void a(AdProxyConnector adProxyConnector, Exception exc) {
        f();
    }

    /* access modifiers changed from: package-private */
    public final boolean b() {
        return false;
    }

    public final int e() {
        return this.i;
    }

    public final AdContainer getAdContainer() {
        return this.mAdcontainer;
    }

    public final int getContainerHeight() {
        return this.j;
    }

    public final void onClick(View view) {
        Log.d("AdProxy", "View Clicked");
    }

    public final void setAdContainer(AdContainer adContainer) {
        this.mAdcontainer = adContainer;
    }
}
