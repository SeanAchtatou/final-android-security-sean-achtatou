package org.codehaus.jackson.sym;

import java.util.Arrays;

public final class CharsToNameCanonicalizer {
    protected static final int DEFAULT_TABLE_SIZE = 64;
    static final int MAX_ENTRIES_FOR_REUSE = 12000;
    protected static final int MAX_TABLE_SIZE = 65536;
    static final CharsToNameCanonicalizer sBootstrapSymbolTable = new CharsToNameCanonicalizer();
    protected Bucket[] _buckets;
    protected final boolean _canonicalize;
    protected boolean _dirty;
    protected int _indexMask;
    protected final boolean _intern;
    protected CharsToNameCanonicalizer _parent;
    protected int _size;
    protected int _sizeThreshold;
    protected String[] _symbols;

    public static CharsToNameCanonicalizer createRoot() {
        return sBootstrapSymbolTable.makeOrphan();
    }

    private CharsToNameCanonicalizer() {
        this._canonicalize = true;
        this._intern = true;
        this._dirty = true;
        initTables(DEFAULT_TABLE_SIZE);
    }

    private void initTables(int initialSize) {
        this._symbols = new String[initialSize];
        this._buckets = new Bucket[(initialSize >> 1)];
        this._indexMask = initialSize - 1;
        this._size = 0;
        this._sizeThreshold = initialSize - (initialSize >> 2);
    }

    private CharsToNameCanonicalizer(CharsToNameCanonicalizer parent, boolean canonicalize, boolean intern, String[] symbols, Bucket[] buckets, int size) {
        this._parent = parent;
        this._canonicalize = canonicalize;
        this._intern = intern;
        this._symbols = symbols;
        this._buckets = buckets;
        this._size = size;
        int arrayLen = symbols.length;
        this._sizeThreshold = arrayLen - (arrayLen >> 2);
        this._indexMask = arrayLen - 1;
        this._dirty = false;
    }

    public synchronized CharsToNameCanonicalizer makeChild(boolean canonicalize, boolean intern) {
        return new CharsToNameCanonicalizer(this, canonicalize, intern, this._symbols, this._buckets, this._size);
    }

    private CharsToNameCanonicalizer makeOrphan() {
        return new CharsToNameCanonicalizer(null, true, true, this._symbols, this._buckets, this._size);
    }

    private synchronized void mergeChild(CharsToNameCanonicalizer child) {
        if (child.size() > MAX_ENTRIES_FOR_REUSE) {
            initTables(DEFAULT_TABLE_SIZE);
        } else if (child.size() > size()) {
            this._symbols = child._symbols;
            this._buckets = child._buckets;
            this._size = child._size;
            this._sizeThreshold = child._sizeThreshold;
            this._indexMask = child._indexMask;
        }
        this._dirty = false;
    }

    public void release() {
        if (maybeDirty() && this._parent != null) {
            this._parent.mergeChild(this);
            this._dirty = false;
        }
    }

    public int size() {
        return this._size;
    }

    public boolean maybeDirty() {
        return this._dirty;
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x002e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String findSymbol(char[] r9, int r10, int r11, int r12) {
        /*
            r8 = this;
            r7 = 1
            if (r11 >= r7) goto L_0x0006
            java.lang.String r5 = ""
        L_0x0005:
            return r5
        L_0x0006:
            boolean r5 = r8._canonicalize
            if (r5 != 0) goto L_0x0010
            java.lang.String r5 = new java.lang.String
            r5.<init>(r9, r10, r11)
            goto L_0x0005
        L_0x0010:
            int r5 = r8._indexMask
            r12 = r12 & r5
            java.lang.String[] r5 = r8._symbols
            r4 = r5[r12]
            if (r4 == 0) goto L_0x0043
            int r5 = r4.length()
            if (r5 != r11) goto L_0x0033
            r2 = 0
        L_0x0020:
            char r5 = r4.charAt(r2)
            int r6 = r10 + r2
            char r6 = r9[r6]
            if (r5 == r6) goto L_0x002e
        L_0x002a:
            if (r2 != r11) goto L_0x0033
            r5 = r4
            goto L_0x0005
        L_0x002e:
            int r2 = r2 + 1
            if (r2 < r11) goto L_0x0020
            goto L_0x002a
        L_0x0033:
            org.codehaus.jackson.sym.CharsToNameCanonicalizer$Bucket[] r5 = r8._buckets
            int r6 = r12 >> 1
            r0 = r5[r6]
            if (r0 == 0) goto L_0x0043
            java.lang.String r4 = r0.find(r9, r10, r11)
            if (r4 == 0) goto L_0x0043
            r5 = r4
            goto L_0x0005
        L_0x0043:
            boolean r5 = r8._dirty
            if (r5 != 0) goto L_0x006d
            r8.copyArrays()
            r8._dirty = r7
        L_0x004c:
            int r5 = r8._size
            int r5 = r5 + 1
            r8._size = r5
            java.lang.String r3 = new java.lang.String
            r3.<init>(r9, r10, r11)
            boolean r5 = r8._intern
            if (r5 == 0) goto L_0x0061
            org.codehaus.jackson.util.InternCache r5 = org.codehaus.jackson.util.InternCache.instance
            java.lang.String r3 = r5.intern(r3)
        L_0x0061:
            java.lang.String[] r5 = r8._symbols
            r5 = r5[r12]
            if (r5 != 0) goto L_0x007f
            java.lang.String[] r5 = r8._symbols
            r5[r12] = r3
        L_0x006b:
            r5 = r3
            goto L_0x0005
        L_0x006d:
            int r5 = r8._size
            int r6 = r8._sizeThreshold
            if (r5 < r6) goto L_0x004c
            r8.rehash()
            int r5 = calcHash(r9, r10, r11)
            int r6 = r8._indexMask
            r12 = r5 & r6
            goto L_0x004c
        L_0x007f:
            int r1 = r12 >> 1
            org.codehaus.jackson.sym.CharsToNameCanonicalizer$Bucket[] r5 = r8._buckets
            org.codehaus.jackson.sym.CharsToNameCanonicalizer$Bucket r6 = new org.codehaus.jackson.sym.CharsToNameCanonicalizer$Bucket
            org.codehaus.jackson.sym.CharsToNameCanonicalizer$Bucket[] r7 = r8._buckets
            r7 = r7[r1]
            r6.<init>(r3, r7)
            r5[r1] = r6
            goto L_0x006b
        */
        throw new UnsupportedOperationException("Method not decompiled: org.codehaus.jackson.sym.CharsToNameCanonicalizer.findSymbol(char[], int, int, int):java.lang.String");
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    public static int calcHash(char[] buffer, int start, int len) {
        int hash = buffer[0];
        for (int i = 1; i < len; i++) {
            hash = (hash * 31) + buffer[i];
        }
        return hash;
    }

    public static int calcHash(String key) {
        int hash = key.charAt(0);
        int len = key.length();
        for (int i = 1; i < len; i++) {
            hash = (hash * 31) + key.charAt(i);
        }
        return hash;
    }

    private void copyArrays() {
        String[] oldSyms = this._symbols;
        int size = oldSyms.length;
        this._symbols = new String[size];
        System.arraycopy(oldSyms, 0, this._symbols, 0, size);
        Bucket[] oldBuckets = this._buckets;
        int size2 = oldBuckets.length;
        this._buckets = new Bucket[size2];
        System.arraycopy(oldBuckets, 0, this._buckets, 0, size2);
    }

    private void rehash() {
        int size = this._symbols.length;
        int newSize = size + size;
        if (newSize > MAX_TABLE_SIZE) {
            this._size = 0;
            Arrays.fill(this._symbols, (Object) null);
            Arrays.fill(this._buckets, (Object) null);
            this._dirty = true;
            return;
        }
        String[] oldSyms = this._symbols;
        Bucket[] oldBuckets = this._buckets;
        this._symbols = new String[newSize];
        this._buckets = new Bucket[(newSize >> 1)];
        this._indexMask = newSize - 1;
        this._sizeThreshold += this._sizeThreshold;
        int count = 0;
        for (int i = 0; i < size; i++) {
            String symbol = oldSyms[i];
            if (symbol != null) {
                count++;
                int index = calcHash(symbol) & this._indexMask;
                if (this._symbols[index] == null) {
                    this._symbols[index] = symbol;
                } else {
                    int bix = index >> 1;
                    this._buckets[bix] = new Bucket(symbol, this._buckets[bix]);
                }
            }
        }
        int size2 = size >> 1;
        for (int i2 = 0; i2 < size2; i2++) {
            for (Bucket b = oldBuckets[i2]; b != null; b = b.getNext()) {
                count++;
                String symbol2 = b.getSymbol();
                int index2 = calcHash(symbol2) & this._indexMask;
                if (this._symbols[index2] == null) {
                    this._symbols[index2] = symbol2;
                } else {
                    int bix2 = index2 >> 1;
                    this._buckets[bix2] = new Bucket(symbol2, this._buckets[bix2]);
                }
            }
        }
        if (count != this._size) {
            throw new Error("Internal error on SymbolTable.rehash(): had " + this._size + " entries; now have " + count + ".");
        }
    }

    static final class Bucket {
        private final String _symbol;
        private final Bucket mNext;

        public Bucket(String symbol, Bucket next) {
            this._symbol = symbol;
            this.mNext = next;
        }

        public String getSymbol() {
            return this._symbol;
        }

        public Bucket getNext() {
            return this.mNext;
        }

        /* JADX WARNING: Removed duplicated region for block: B:8:0x0019  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public java.lang.String find(char[] r6, int r7, int r8) {
            /*
                r5 = this;
                java.lang.String r2 = r5._symbol
                org.codehaus.jackson.sym.CharsToNameCanonicalizer$Bucket r0 = r5.mNext
            L_0x0004:
                int r3 = r2.length()
                if (r3 != r8) goto L_0x001e
                r1 = 0
            L_0x000b:
                char r3 = r2.charAt(r1)
                int r4 = r7 + r1
                char r4 = r6[r4]
                if (r3 == r4) goto L_0x0019
            L_0x0015:
                if (r1 != r8) goto L_0x001e
                r3 = r2
            L_0x0018:
                return r3
            L_0x0019:
                int r1 = r1 + 1
                if (r1 < r8) goto L_0x000b
                goto L_0x0015
            L_0x001e:
                if (r0 != 0) goto L_0x0022
                r3 = 0
                goto L_0x0018
            L_0x0022:
                java.lang.String r2 = r0.getSymbol()
                org.codehaus.jackson.sym.CharsToNameCanonicalizer$Bucket r0 = r0.getNext()
                goto L_0x0004
            */
            throw new UnsupportedOperationException("Method not decompiled: org.codehaus.jackson.sym.CharsToNameCanonicalizer.Bucket.find(char[], int, int):java.lang.String");
        }
    }
}
