package org.codehaus.jackson;

public final class Base64Variants {
    public static final Base64Variant MIME = new Base64Variant("MIME", STD_BASE64_ALPHABET, true, '=', 76);
    public static final Base64Variant MIME_NO_LINEFEEDS = new Base64Variant(MIME, "MIME-NO-LINEFEEDS", Integer.MAX_VALUE);
    public static final Base64Variant MODIFIED_FOR_URL;
    public static final Base64Variant PEM = new Base64Variant(MIME, "PEM", true, '=', 64);
    static final String STD_BASE64_ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.codehaus.jackson.Base64Variant.<init>(java.lang.String, java.lang.String, boolean, char, int):void
     arg types: [java.lang.String, java.lang.String, int, int, int]
     candidates:
      org.codehaus.jackson.Base64Variant.<init>(org.codehaus.jackson.Base64Variant, java.lang.String, boolean, char, int):void
      org.codehaus.jackson.Base64Variant.<init>(java.lang.String, java.lang.String, boolean, char, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.codehaus.jackson.Base64Variant.<init>(org.codehaus.jackson.Base64Variant, java.lang.String, boolean, char, int):void
     arg types: [org.codehaus.jackson.Base64Variant, java.lang.String, int, int, int]
     candidates:
      org.codehaus.jackson.Base64Variant.<init>(java.lang.String, java.lang.String, boolean, char, int):void
      org.codehaus.jackson.Base64Variant.<init>(org.codehaus.jackson.Base64Variant, java.lang.String, boolean, char, int):void */
    static {
        StringBuffer sb = new StringBuffer(STD_BASE64_ALPHABET);
        sb.setCharAt(sb.indexOf("+"), '-');
        sb.setCharAt(sb.indexOf("/"), '_');
        MODIFIED_FOR_URL = new Base64Variant("MODIFIED-FOR-URL", sb.toString(), false, 0, Integer.MAX_VALUE);
    }

    public static Base64Variant getDefaultVariant() {
        return MIME_NO_LINEFEEDS;
    }
}
