package jp.Tontoro.Lib;

import android.app.Activity;
import android.os.Build;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.LinearLayout;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import java.util.Calendar;
import java.util.Locale;
import jp.co.nobot.libAdMaker.libAdMaker;
import mediba.ad.sdk.android.MasAdView;

public class nnAdvertising {
    public static final int FLAG_ALL = 7;
    public static final int FLAG_AdMaker = 1;
    public static final int FLAG_AdMob = 2;
    public static final int FLAG_TGAd = 4;
    private libAdMaker AdMaker = null;

    public void Start(Activity mActivity, int LayoutID, int AdMakerViewID, String AdMaker_siteId, String AdMaker_zoneId, String AdMaker_Url, String AdMob_Publisher_ID, int Flag) {
        if (Locale.JAPAN.equals(Locale.getDefault())) {
            int AdCounter = (PreferenceManager.getDefaultSharedPreferences(mActivity.getBaseContext()).getInt("pref_ad_counter", Calendar.getInstance().get(12) & 1) + 1) & 1;
            PreferenceManager.getDefaultSharedPreferences(mActivity.getBaseContext()).edit().putInt("pref_ad_counter", AdCounter).commit();
            new Build.VERSION();
            if (Build.VERSION.SDK_INT < 8 || AdCounter == 0) {
                Log.d("nnAdvertising", "J Mode AdMaker");
                this.AdMaker = (libAdMaker) mActivity.findViewById(AdMakerViewID);
                this.AdMaker.siteId = AdMaker_siteId;
                this.AdMaker.zoneId = AdMaker_zoneId;
                this.AdMaker.setUrl(AdMaker_Url);
                this.AdMaker.setBackgroundColor(-16777216);
                this.AdMaker.start();
                return;
            }
            Log.d("nnAdvertising", "J Mode mediba");
            MasAdView masAdView = new MasAdView(mActivity);
            masAdView.setRefreshAnimation(2);
            masAdView.setBackgroundColor(2);
            ((LinearLayout) mActivity.findViewById(LayoutID)).addView(masAdView);
            return;
        }
        Log.d("nnAdvertising", "not Japanise mode START!");
        AdView adView = new AdView(mActivity, AdSize.BANNER, AdMob_Publisher_ID);
        ((LinearLayout) mActivity.findViewById(LayoutID)).addView(adView);
        adView.loadAd(new AdRequest());
    }

    public void onDestroy() {
        if (this.AdMaker != null) {
            this.AdMaker.destroy();
            this.AdMaker = null;
        }
    }

    public void onPause() {
        if (this.AdMaker != null) {
            this.AdMaker.stop();
        }
    }

    public void onRestart() {
        if (this.AdMaker != null) {
            this.AdMaker.start();
        }
    }
}
