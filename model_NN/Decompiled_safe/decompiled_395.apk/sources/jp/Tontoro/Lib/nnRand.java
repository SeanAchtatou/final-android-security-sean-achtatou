package jp.Tontoro.Lib;

public class nnRand {
    int mRandA = 0;
    int mRandB = 0;
    int mRandC = 0;
    int mRandD = 0;

    public void SetSeed(int SeedA, int SeedB, int SeedC, int SeedD) {
        this.mRandA = SeedA;
        this.mRandB = SeedB;
        this.mRandC = SeedC;
        this.mRandD = SeedD;
    }

    public int Get() {
        this.mRandA = (this.mRandA * 1103515245) + 12345;
        this.mRandB = (this.mRandB * 214013) + 2531011;
        this.mRandC = (this.mRandC * 5019) + 31033;
        this.mRandD = (this.mRandD * 9933) + 638079;
        return (this.mRandA + (this.mRandB >> 16) + (this.mRandC >> 9) + (this.mRandD >> 5)) & 65535;
    }

    public int Get(int Num) {
        return ((Get() & 65535) * Num) >> 16;
    }

    public float Getf() {
        return ((float) Get()) / 65536.0f;
    }
}
