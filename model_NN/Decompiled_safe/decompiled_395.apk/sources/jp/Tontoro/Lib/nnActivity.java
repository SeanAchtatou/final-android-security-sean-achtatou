package jp.Tontoro.Lib;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;

public class nnActivity extends Activity {
    int mActivityResult = 0;
    protected nnAdvertising mAdvertising = new nnAdvertising();
    protected nnFramework mFramework = null;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(1024);
        requestWindowFeature(1);
        setVolumeControlStream(3);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.mFramework != null) {
            this.mFramework.onResume();
        }
        Log.d("nnActivity", "onResume");
    }

    public void onRestart() {
        super.onRestart();
        this.mAdvertising.onRestart();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.mAdvertising.onPause();
        if (this.mFramework != null) {
            this.mFramework.onPause();
        }
        Log.d("nnActivity", "onPause");
    }

    public void onDestroy() {
        super.onDestroy();
        this.mAdvertising.onDestroy();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        return false;
    }

    public boolean onKeyDown(int keyCode, KeyEvent msg) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, msg);
        }
        if (this.mFramework == null) {
            return super.onKeyDown(keyCode, msg);
        }
        if (!this.mFramework.BackKeySkip()) {
            return super.onKeyDown(keyCode, msg);
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        this.mActivityResult = requestCode;
    }

    public int GetActivityResult() {
        if (this.mActivityResult == 0) {
            return 0;
        }
        int Res = this.mActivityResult;
        this.mActivityResult = 0;
        return Res;
    }
}
