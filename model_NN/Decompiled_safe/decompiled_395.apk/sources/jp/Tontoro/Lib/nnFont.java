package jp.Tontoro.Lib;

import android.content.Context;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import javax.microedition.khronos.opengles.GL10;

public class nnFont {
    private float HeadAscent;
    private float HeadAveCharWidth;
    private float HeadDescent;
    /* access modifiers changed from: private */
    public float HeadHeight;
    private float HeadLeading;
    private float HeadMaxCharWidth;
    private float HeadWidth;
    GL10 gl;
    FGI[] mFGIArray = new FGI[256];
    Packet mPacket;
    int mTex;

    private class FGI {
        float PixWidth;
        float UVHeight;
        float UVWidth;
        int code;

        private FGI() {
        }

        /* synthetic */ FGI(nnFont nnfont, FGI fgi) {
            this();
        }
    }

    public class Packet {
        ByteBuffer bbPos;
        ByteBuffer bbTex;
        FloatBuffer fbPos;
        FloatBuffer fbTex;
        nnFont mParent;
        String mStr;
        int mStrLen = 0;

        public Packet(nnFont Parent) {
            this.mParent = Parent;
        }

        public void makePacketBuffer() {
            this.bbPos = ByteBuffer.allocateDirect(this.mStrLen * 4 * 3 * 6);
            this.bbPos.order(ByteOrder.nativeOrder());
            this.fbPos = this.bbPos.asFloatBuffer();
            this.bbTex = ByteBuffer.allocateDirect(this.mStrLen * 4 * 2 * 6);
            this.bbTex.order(ByteOrder.nativeOrder());
            this.fbTex = this.bbTex.asFloatBuffer();
        }

        /* access modifiers changed from: package-private */
        public void MakePacketChr(int chr, float x, float y, float Scale) {
            FGI fgi = this.mParent.mFGIArray[chr];
            float u0 = ((float) (chr & 15)) * 0.0625f;
            float v0 = ((float) ((chr >> 4) & 7)) * 0.125f;
            float u1 = u0 + fgi.UVWidth;
            float v1 = v0 + 0.125f;
            float x0 = x;
            float x1 = x + (fgi.PixWidth * Scale);
            float y0 = y;
            float y1 = y - (nnFont.this.HeadHeight * Scale);
            this.fbPos.put(new float[]{x0, y0, 0.0f, x1, y0, 0.0f, x0, y1, 0.0f, x1, y0, 0.0f, x0, y1, 0.0f, x1, y1, 0.0f});
            this.fbTex.put(new float[]{u0, v0, u1, v0, u0, v1, u1, v0, u0, v1, u1, v1});
        }

        public void MakePacket(String str, float x, float y, float Scale) {
            this.mStr = str;
            this.mStrLen = str.length();
            makePacketBuffer();
            for (int i = 0; i < this.mStrLen; i++) {
                MakePacketChr(str.charAt(i), x, y, Scale);
                x += nnFont.this.mFGIArray[str.charAt(i)].PixWidth * Scale;
            }
            this.fbPos.position(0);
            this.fbTex.position(0);
        }

        public void DrawPacket(GL10 gl) {
            gl.glVertexPointer(3, 5126, 0, this.fbPos);
            gl.glEnableClientState(32884);
            gl.glTexCoordPointer(2, 5126, 0, this.fbTex);
            gl.glEnableClientState(32888);
            gl.glDisableClientState(32886);
            gl.glEnable(3553);
            gl.glBindTexture(3553, this.mParent.mTex);
            gl.glDrawArrays(4, 0, this.mStrLen * 6);
            gl.glDisable(3553);
        }
    }

    public nnFont(Context context, GL10 _gl, int fgiRes, int bmpRes) {
        this.gl = _gl;
        try {
            BufferedInputStream in = new BufferedInputStream(context.getResources().openRawResource(fgiRes));
            int size = in.available();
            byte[] buf = new byte[size];
            in.read(buf, 0, size);
            this.HeadWidth = (float) _ToInt(buf, 4);
            this.HeadHeight = (float) _ToInt(buf, 8);
            this.HeadAscent = (float) _ToInt(buf, 16);
            this.HeadDescent = (float) _ToInt(buf, 20);
            this.HeadLeading = this.HeadAscent + this.HeadDescent;
            this.HeadAveCharWidth = (float) _ToInt(buf, 28);
            this.HeadMaxCharWidth = (float) _ToInt(buf, 32);
            for (int i = 0; i < 256; i++) {
                int Index = (i * 4 * 4) + 48;
                this.mFGIArray[i] = new FGI(this, null);
                this.mFGIArray[i].PixWidth = (float) _ToInt(buf, Index + 4);
                this.mFGIArray[i].UVWidth = this.mFGIArray[i].PixWidth / (this.HeadWidth * 16.0f);
                this.mFGIArray[i].UVHeight = this.HeadLeading / (this.HeadHeight * 8.0f);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.mTex = nnGraph.LoadTexture(context, this.gl, bmpRes);
    }

    public float GetLeading() {
        return this.HeadLeading;
    }

    public float GetStrWidth(String str) {
        float width = 0.0f;
        for (int i = 0; i < str.length(); i++) {
            width += this.mFGIArray[str.charAt(i)].PixWidth;
        }
        return width;
    }

    public void DrawStr(String str, float x, float y) {
        DrawStr(str, x, y, 1.0f);
    }

    public void DrawStr(String str, float x, float y, float Scale) {
        Packet mPacket2 = new Packet(this);
        mPacket2.MakePacket(str, x, y, Scale);
        mPacket2.DrawPacket(this.gl);
    }

    public void DrawChr(int chr, float x, float y, float Scale) {
        FGI fgi = this.mFGIArray[chr];
        float u0 = ((float) (chr & 15)) * 0.0625f;
        float v0 = ((float) ((chr >> 4) & 7)) * 0.125f;
        this.gl.glEnable(3553);
        this.gl.glBindTexture(3553, this.mTex);
        float f = x;
        float f2 = y;
        nnGraph.DrawRectUVPrim(this.gl, f, f2, x + (fgi.PixWidth * Scale), y - (this.HeadHeight * Scale), u0, v0, u0 + fgi.UVWidth, v0 + 0.125f);
        this.gl.glDisable(3553);
    }

    protected static int _ToInt(byte[] buf, int Index) {
        return (buf[Index + 0] & 255) | ((buf[Index + 1] << 8) & 65280) | ((buf[Index + 2] << 16) & 16711680) | ((buf[Index + 3] << 24) & -16777216);
    }

    public Packet GetPacket() {
        return new Packet(this);
    }
}
