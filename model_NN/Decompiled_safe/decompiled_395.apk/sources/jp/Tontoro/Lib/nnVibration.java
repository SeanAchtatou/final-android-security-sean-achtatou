package jp.Tontoro.Lib;

import android.app.Activity;
import android.os.Vibrator;

public class nnVibration {
    private boolean mEnable = true;
    private Vibrator mVibrator;

    public nnVibration(Activity mActivity) {
        this.mVibrator = (Vibrator) mActivity.getSystemService("vibrator");
    }

    public void Vibrate(int Index) {
        if (this.mEnable && Index > 0) {
            this.mVibrator.vibrate((long) Index);
        }
    }

    public void SetEnable(boolean sw) {
        this.mEnable = sw;
    }
}
