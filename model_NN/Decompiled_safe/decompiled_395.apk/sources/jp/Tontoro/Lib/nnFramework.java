package jp.Tontoro.Lib;

import android.content.Context;
import android.content.pm.PackageManager;
import android.opengl.GLSurfaceView;
import android.opengl.GLU;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.MotionEvent;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

public class nnFramework extends GLSurfaceView {
    public static final int KEY_BACK = 16;
    public static final int KEY_DOWN = 8;
    public static final int KEY_LEFT = 1;
    public static final int KEY_MENU = 32;
    public static final int KEY_RIGHT = 2;
    public static final int KEY_UP = 4;
    Display display;
    DisplayMetrics displayMetrics;
    public boolean isDebug = false;
    public boolean isFrameSkip = false;
    private boolean isInit = false;
    public boolean mBackKeySkip = false;
    public float mClientH = 0.0f;
    public float mClientW = 0.0f;
    public Context mContext;
    protected int mDispH;
    protected int mDispW;
    public int mKeyLev = 0;
    private int mKeyLevBackup = 0;
    public int mKeyTrg = 0;
    public nnActivity mParent;
    protected nnRenderer mRenderer;
    protected int mStep0 = 0;
    protected int mStep1 = 0;
    private long mTimer;
    public float mTouchDownX = 0.0f;
    public float mTouchDownY = 0.0f;
    public boolean mTouchLev = false;
    private boolean mTouchLevBackup = false;
    public float mTouchMoveX = 0.0f;
    private float mTouchMoveX_o = 0.0f;
    public float mTouchMoveY = 0.0f;
    private float mTouchMoveY_o = 0.0f;
    public boolean mTouchOffTrg = false;
    public boolean mTouchTrg = false;
    public float mViewH = 0.0f;
    public float mViewW = 0.0f;

    public void Init(float _DisplayW, float _DisplayH) {
        float DisplayW = _DisplayW;
        float DisplayH = _DisplayH;
        this.display = this.mParent.getWindowManager().getDefaultDisplay();
        this.displayMetrics = new DisplayMetrics();
        this.display.getMetrics(this.displayMetrics);
        float CAspect = this.mClientW / this.mClientH;
        if (CAspect > DisplayW / DisplayH) {
            this.mViewW = DisplayW;
            this.mViewH = this.mViewW / CAspect;
            return;
        }
        this.mViewH = DisplayH;
        this.mViewW = this.mViewH * CAspect;
    }

    public void SetFocus() {
        setFocusable(true);
        setFocusableInTouchMode(true);
        requestFocus();
    }

    public nnFramework(Context context, float ClientW, float ClientH, boolean _isDebug) {
        super(context);
        this.mContext = context;
        this.mParent = (nnActivity) context;
        this.isDebug = _isDebug;
        this.mRenderer = new nnRenderer(this);
        setRenderer(this.mRenderer);
        setFocusable(true);
        setFocusableInTouchMode(true);
        requestFocus();
        this.display = this.mParent.getWindowManager().getDefaultDisplay();
        this.displayMetrics = new DisplayMetrics();
        this.display.getMetrics(this.displayMetrics);
        this.mClientW = ClientW;
        this.mClientH = ClientH;
        float CAspect = ClientW / ClientH;
        if (CAspect > ((float) this.displayMetrics.widthPixels) / ((float) this.displayMetrics.heightPixels)) {
            this.mViewW = (float) this.displayMetrics.widthPixels;
            this.mViewH = this.mViewW / CAspect;
        } else {
            this.mViewH = (float) this.displayMetrics.heightPixels;
            this.mViewW = this.mViewH * CAspect;
        }
        if (this.isDebug) {
            Log.d("Display:xdpi", String.valueOf(this.displayMetrics.xdpi));
            Log.d("Display:ydpi", String.valueOf(this.displayMetrics.ydpi));
            Log.d("Display:widthPixels", String.valueOf(this.displayMetrics.widthPixels));
            Log.d("Display:heightPixels", String.valueOf(this.displayMetrics.heightPixels));
            Log.d("Display:density", String.valueOf(this.displayMetrics.density));
            Log.d("Display:scaledDensity", String.valueOf(this.displayMetrics.scaledDensity));
            Log.d("Display:width", String.valueOf(this.display.getWidth()));
            Log.d("Display:height", String.valueOf(this.display.getHeight()));
            Log.d("Display:orientation", String.valueOf(this.display.getOrientation()));
            Log.d("Display:refreshRate", String.valueOf(this.display.getRefreshRate()));
            Log.d("Display:pixelFormat", String.valueOf(this.display.getPixelFormat()));
            Log.d("Display:ClientW", String.valueOf(this.mClientW));
            Log.d("Display:ClientH", String.valueOf(this.mClientH));
            Log.d("Display:ViewW", String.valueOf(this.mViewW));
            Log.d("Display:ViewH", String.valueOf(this.mViewH));
            Log.d("Display:DispW", String.valueOf(this.mDispW));
            Log.d("Display:DispH", String.valueOf(this.mDispH));
        }
        this.mTimer = System.nanoTime();
    }

    public void TimerReset() {
        this.mTimer = System.nanoTime();
    }

    public void onSurfaceChanged(GL10 gl, int width, int height) {
        if (this.isDebug) {
            Log.d("nnFramework", "onSurfaceChanged");
        }
        this.mDispW = width;
        this.mDispH = height;
        Init((float) width, (float) height);
    }

    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        if (this.isDebug) {
            Log.d("nnFramework", "onSurfaceCreated");
        }
        gl.glHint(3152, 4353);
        gl.glEnable(2884);
        gl.glDisable(3024);
        gl.glShadeModel(7425);
        gl.glEnable(2929);
        onRenderInit(gl);
    }

    public void onDrawFrame(GL10 gl) {
        if (!this.isInit) {
            this.mTimer = System.nanoTime();
            this.isInit = true;
        }
        long old = this.mTimer;
        long start = System.nanoTime();
        this.mTimer = start;
        long Step = ((this.mTimer - old) + 8000000) / 16000000;
        if (Step > 5) {
            Step = 5;
        }
        if (!this.isFrameSkip) {
            Step = 1;
        }
        while (Step > 0) {
            this.mTouchMoveX = (this.mTouchMoveX_o * this.mClientW) / this.mViewW;
            this.mTouchMoveY = (this.mTouchMoveY_o * this.mClientH) / this.mViewH;
            this.mTouchTrg = !this.mTouchLevBackup && this.mTouchLev;
            this.mTouchOffTrg = this.mTouchLevBackup && !this.mTouchLev;
            if (this.mTouchTrg) {
                this.mTouchDownX = this.mTouchMoveX;
                this.mTouchDownY = this.mTouchMoveY;
            }
            this.mTouchLevBackup = this.mTouchLev;
            this.mKeyTrg = (this.mKeyLevBackup ^ this.mKeyLev) & this.mKeyLev;
            this.mKeyLevBackup = this.mKeyLev;
            onRenderMove(gl);
            Step--;
        }
        long move = System.nanoTime();
        gl.glViewport(0, 0, this.mDispW, this.mDispH);
        gl.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        gl.glClear(16640);
        gl.glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
        gl.glViewport(((int) (((float) this.mDispW) - this.mViewW)) / 2, ((int) (((float) this.mDispH) - this.mViewH)) / 2, (int) this.mViewW, (int) this.mViewH);
        gl.glMatrixMode(5889);
        gl.glLoadIdentity();
        float ratio = ((float) this.mDispW) / ((float) this.mDispH);
        gl.glFrustumf(-ratio, ratio, -1.0f, 1.0f, 1.0f, 10.0f);
        onRenderDraw(gl);
        if (this.isDebug) {
            gl.glMatrixMode(5889);
            gl.glLoadIdentity();
            GLU.gluOrtho2D(gl, -1.0f, 1.0f, -1.0f, 1.0f);
            gl.glMatrixMode(5888);
            gl.glLoadIdentity();
            gl.glColor4f(1.0f, 0.0f, 0.0f, 0.5f);
            nnGraph.DrawRectPrim(gl, -1.0f, 0.95f, (((float) (System.nanoTime() - start)) / 1.6E7f) - 1.0f, 0.9f);
            gl.glColor4f(1.0f, 1.0f, 0.0f, 0.5f);
            nnGraph.DrawRectPrim(gl, -1.0f, 0.95f, (((float) (move - start)) / 1.6E7f) - 1.0f, 0.9f);
            if (this.mTouchLev) {
                float x = this.mTouchMoveX_o / (this.mViewW * 0.5f);
                float y = this.mTouchMoveY_o / (this.mViewH * 0.5f);
                gl.glColor4f(1.0f, 1.0f, 0.0f, 0.5f);
                nnGraph.DrawLinePrim(gl, x, 1.0f, x, -1.0f);
                nnGraph.DrawLinePrim(gl, -1.0f, y, 1.0f, y);
            }
        }
    }

    public void onRenderInit(GL10 gl) {
    }

    public void onRenderMove(GL10 gl) {
    }

    public void onRenderDraw(GL10 gl) {
    }

    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case 0:
                if (this.isDebug) {
                    Log.d("nnFramework", "doTouch:ACTION_DOWN");
                }
                this.mTouchLev = true;
                this.mTouchMoveX_o = event.getX() - (((float) this.mDispW) * 0.5f);
                this.mTouchMoveY_o = -(event.getY() - (((float) this.mDispH) * 0.5f));
                break;
            case 1:
                if (this.isDebug) {
                    Log.d("nnFramework", "doTouch:ACTION_UP");
                }
                this.mTouchLev = false;
                break;
            case 2:
                if (this.isDebug) {
                    Log.d("nnFramework", "doTouch:ACTION_MOVE");
                }
                this.mTouchMoveX_o = event.getX() - (((float) this.mDispW) * 0.5f);
                this.mTouchMoveY_o = -(event.getY() - (((float) this.mDispH) * 0.5f));
                break;
        }
        return true;
    }

    public boolean onKeyDown(int keyCode, KeyEvent msg) {
        if (this.isDebug) {
            Log.d("nnFramework", "onKeyDown");
        }
        if (keyCode == 21) {
            this.mKeyLev |= 1;
        }
        if (keyCode == 22) {
            this.mKeyLev |= 2;
        }
        if (keyCode == 19) {
            this.mKeyLev |= 4;
        }
        if (keyCode == 20) {
            this.mKeyLev |= 8;
        }
        if (keyCode == 4) {
            this.mKeyLev |= 16;
        }
        if (keyCode != 82) {
            return false;
        }
        this.mKeyLev |= 32;
        return false;
    }

    public boolean onKeyUp(int keyCode, KeyEvent msg) {
        if (this.isDebug) {
            Log.d("nnFramework", "onKeyUp");
        }
        if (keyCode == 21) {
            this.mKeyLev &= -2;
        }
        if (keyCode == 22) {
            this.mKeyLev &= -3;
        }
        if (keyCode == 19) {
            this.mKeyLev &= -5;
        }
        if (keyCode == 20) {
            this.mKeyLev &= -9;
        }
        if (keyCode == 4) {
            this.mKeyLev &= -17;
        }
        if (keyCode != 82) {
            return false;
        }
        this.mKeyLev &= -33;
        return false;
    }

    public boolean BackKeySkip() {
        return this.mBackKeySkip;
    }

    public int getVersionCode() {
        try {
            return this.mContext.getPackageManager().getPackageInfo(this.mContext.getPackageName(), 1).versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            return -1;
        }
    }

    public String getVersionName() {
        try {
            return this.mContext.getPackageManager().getPackageInfo(this.mContext.getPackageName(), 1).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            return "";
        }
    }

    public String getPackageName() {
        return this.mContext.getPackageName();
    }

    public int Step0() {
        return this.mStep0;
    }

    public int Step1() {
        return this.mStep1;
    }

    public void Step0Inc() {
        Step0Set(this.mStep0 + 1);
    }

    public void Step1Inc() {
        Step1Set(this.mStep1 + 1);
    }

    public void Step0Set(int Step) {
        this.mStep0 = Step;
        Step1Set(0);
    }

    public void Step1Set(int Step) {
        this.mStep1 = Step;
    }
}
