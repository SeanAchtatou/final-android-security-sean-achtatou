package jp.Tontoro.Lib;

import android.opengl.GLSurfaceView;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

/* compiled from: nnFramework */
class nnRenderer implements GLSurfaceView.Renderer {
    nnFramework mFramework;

    public nnRenderer(nnFramework Framework) {
        this.mFramework = Framework;
    }

    public void onDrawFrame(GL10 gl) {
        this.mFramework.onDrawFrame(gl);
    }

    public void onSurfaceChanged(GL10 gl, int width, int height) {
        this.mFramework.onSurfaceChanged(gl, width, height);
    }

    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        this.mFramework.onSurfaceCreated(gl, config);
    }
}
