package jp.Tontoro.Lib;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.GLUtils;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import javax.microedition.khronos.opengles.GL10;

public class nnGraph {
    public static FloatBuffer makeFloatBuffer(float[] arr) {
        ByteBuffer bb = ByteBuffer.allocateDirect(arr.length * 4);
        bb.order(ByteOrder.nativeOrder());
        FloatBuffer fb = bb.asFloatBuffer();
        fb.put(arr);
        fb.position(0);
        return fb;
    }

    public static IntBuffer makeIntBuffer(int[] arr) {
        ByteBuffer bb = ByteBuffer.allocateDirect(arr.length * 4);
        bb.order(ByteOrder.nativeOrder());
        IntBuffer ib = bb.asIntBuffer();
        ib.put(arr);
        ib.position(0);
        return ib;
    }

    public static void DrawLinePrim(GL10 gl, float x0, float y0, float x1, float y1) {
        gl.glVertexPointer(3, 5126, 0, makeFloatBuffer(new float[]{x0, y0, 0.0f, x1, y1, 0.0f}));
        gl.glEnableClientState(32884);
        gl.glDisableClientState(32888);
        gl.glDisableClientState(32886);
        gl.glDrawArrays(1, 0, 2);
    }

    public static void DrawRectPrim(GL10 gl, float x0, float y0, float x1, float y1) {
        gl.glVertexPointer(3, 5126, 0, makeFloatBuffer(new float[]{x0, y0, 0.0f, x1, y0, 0.0f, x0, y1, 0.0f, x1, y1, 0.0f}));
        gl.glEnableClientState(32884);
        gl.glDisableClientState(32888);
        gl.glDisableClientState(32886);
        gl.glDrawArrays(5, 0, 4);
    }

    /* JADX INFO: Multiple debug info for r5v6 java.nio.FloatBuffer: [D('tex' float[]), D('TexBuf' java.nio.FloatBuffer)] */
    public static void DrawRectUVPrim(GL10 gl, float x0, float y0, float x1, float y1, float u0, float v0, float u1, float v1) {
        float[] pos = {x0, y0, 0.0f, x1, y0, 0.0f, x0, y1, 0.0f, x1, y1, 0.0f};
        FloatBuffer PosBuf = makeFloatBuffer(pos);
        FloatBuffer TexBuf = makeFloatBuffer(new float[]{u0, v0, u1, v0, u0, v1, u1, v1});
        gl.glVertexPointer(3, 5126, 0, PosBuf);
        gl.glEnableClientState(32884);
        gl.glTexCoordPointer(2, 5126, 0, TexBuf);
        gl.glEnableClientState(32888);
        gl.glDisableClientState(32886);
        gl.glDrawArrays(5, 0, 4);
    }

    public static int LoadTexture(Context context, GL10 gl, int Tex) {
        return LoadTexture(context, gl, context.getResources().openRawResource(Tex));
    }

    public static int LoadTexture(Context context, GL10 gl, InputStream is) {
        int[] tmp_tex = new int[1];
        gl.glGenTextures(1, tmp_tex, 0);
        gl.glBindTexture(3553, tmp_tex[0]);
        gl.glTexParameterf(3553, 10241, 9729.0f);
        gl.glTexParameterf(3553, 10240, 9729.0f);
        gl.glTexParameterf(3553, 10242, 33071.0f);
        gl.glTexParameterf(3553, 10243, 33071.0f);
        gl.glTexEnvf(8960, 8704, 8448.0f);
        try {
            Bitmap bitmap = BitmapFactory.decodeStream(is);
            GLUtils.texImage2D(3553, 0, bitmap, 0);
            bitmap.recycle();
            return tmp_tex[0];
        } finally {
            try {
                is.close();
            } catch (IOException e) {
            }
        }
    }

    protected static ByteBuffer makeByteBuffer(Bitmap bmp) {
        ByteBuffer bb = ByteBuffer.allocateDirect(bmp.getHeight() * bmp.getWidth() * 4);
        bb.order(ByteOrder.LITTLE_ENDIAN);
        IntBuffer ib = bb.asIntBuffer();
        for (int y = 0; y < bmp.getHeight(); y++) {
            for (int x = 0; x < bmp.getWidth(); x++) {
                int pix = bmp.getPixel(x, y);
                ib.put(((pix << 16) & 16711680) | (-16711936 & pix) | ((pix >> 16) & 255));
            }
        }
        ib.position(0);
        bb.position(0);
        return bb;
    }
}
