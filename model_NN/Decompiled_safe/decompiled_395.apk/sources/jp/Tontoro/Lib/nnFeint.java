package jp.Tontoro.Lib;

import android.app.Activity;
import android.content.Context;
import com.openfeint.api.OpenFeint;
import com.openfeint.api.OpenFeintDelegate;
import com.openfeint.api.OpenFeintSettings;
import com.openfeint.internal.OpenFeintInternal;
import java.util.HashMap;
import java.util.Map;

public class nnFeint {
    public static void Init(Activity MainActivity, Context context, String OF_Name, String OF_Key, String OF_Secret, String OF_ID) {
        Map<String, Object> options = new HashMap<>();
        options.put(OpenFeintSettings.SettingCloudStorageCompressionStrategy, OpenFeintSettings.CloudStorageCompressionStrategyDefault);
        OpenFeint.initialize(context, new OpenFeintSettings(OF_Name, OF_Key, OF_Secret, OF_ID, options), new OpenFeintDelegate() {
        });
    }

    public static boolean ChkUserLogin() {
        if (OpenFeintInternal.getInstance() == null) {
            return false;
        }
        if (!OpenFeint.isUserLoggedIn()) {
            return false;
        }
        return true;
    }
}
