package jp.Tontoro.Lib;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.IntBuffer;
import javax.microedition.khronos.opengles.GL10;

public class nnSprite {
    /* access modifiers changed from: private */
    public static boolean isDebug = false;
    Bitmap Bank;
    private int FormatNum = 0;
    private int FormatTbl = 0;
    private int ImgNum = 0;
    private int VertexAdr = 0;
    private int VertexNum = 0;
    private IntBuffer iCoord;
    private IntBuffer iVertex;
    BufferedInputStream in = null;
    private nnSprForm[] mForm;
    private int[] mTexTbl;

    private class nnSprCell {
        int CellAtb;
        int ColAtb;
        int Flg;
        int ImgBank;
        int VertexIndex;

        public nnSprCell(int Index, byte[] buf, int BufTop) {
            this.ImgBank = nnSprite._ToInt(buf, BufTop + 0);
            this.ColAtb = nnSprite._ToInt(buf, BufTop + 4);
            this.CellAtb = nnSprite._ToInt(buf, BufTop + 8);
            this.Flg = nnSprite._ToInt(buf, BufTop + 12);
            this.VertexIndex = nnSprite._ToInt(buf, BufTop + 16);
            if (nnSprite.isDebug) {
                Log.d("nnSprite", " Cell:" + Index + " ImgBank:" + this.ImgBank + " ColAtb:" + this.ColAtb + " CellAtb:" + this.CellAtb + " Flg:" + this.Flg + " VertexIndex:" + this.VertexIndex);
            }
        }
    }

    private class nnSprForm {
        int CellNum;
        /* access modifiers changed from: private */
        public nnSprCell[] mCell = new nnSprCell[this.CellNum];

        public nnSprForm(int Index, byte[] buf, int BufTop) {
            this.CellNum = nnSprite._ToInt(buf, BufTop + 0);
            if (nnSprite.isDebug) {
                Log.d("nnSprite", "Form:" + Index + " CellNum:" + this.CellNum);
            }
            for (int i = 0; i < this.CellNum; i++) {
                this.mCell[i] = new nnSprCell(i, buf, nnSprite._ToInt(buf, BufTop + 4) + (i * 20));
            }
        }
    }

    private class nnSprVert {
        private nnSprVert() {
        }
    }

    public void SetBmp(Bitmap bmp) {
        this.Bank = bmp;
    }

    public nnSprite(InputStream is) {
        IOException e;
        try {
            BufferedInputStream bufferedInputStream = new BufferedInputStream(is);
            try {
                int size = bufferedInputStream.available();
                byte[] buf = new byte[size];
                bufferedInputStream.read(buf, 0, size);
                this.ImgNum = _ToInt(buf, 8);
                if (isDebug) {
                    Log.d("nnSprite", "ImgNum:" + this.ImgNum);
                }
                this.FormatNum = _ToInt(buf, 12);
                if (isDebug) {
                    Log.d("nnSprite", "FormatNum:" + this.FormatNum);
                }
                this.VertexNum = _ToInt(buf, 16);
                if (isDebug) {
                    Log.d("nnSprite", "VertexNum:" + this.VertexNum);
                }
                this.FormatTbl = _ToInt(buf, 20);
                if (isDebug) {
                    Log.d("nnSprite", "FormatTbl:" + this.FormatTbl);
                }
                this.VertexAdr = _ToInt(buf, 28);
                if (isDebug) {
                    Log.d("nnSprite", "VertexAdr:" + this.VertexAdr);
                }
                this.mForm = new nnSprForm[this.FormatNum];
                for (int i = 0; i < this.FormatNum; i++) {
                    this.mForm[i] = new nnSprForm(i, buf, _ToInt(buf, this.FormatTbl + (i * 4)));
                }
                int[] mVertBuf = new int[(this.VertexNum * 12 * 4)];
                int[] mCoordBuf = new int[(this.VertexNum * 8 * 4)];
                for (int i2 = 0; i2 < this.VertexNum; i2++) {
                    int VTop = this.VertexAdr + (i2 * 20 * 4);
                    for (int j = 0; j < 4; j++) {
                        int Top = VTop + (j * 20);
                        int vTop = (i2 * 12) + (j * 3);
                        int cTop = (i2 * 8) + (j * 2);
                        mVertBuf[vTop + 0] = _ToInt(buf, Top + 0);
                        mVertBuf[vTop + 1] = _ToInt(buf, Top + 4);
                        mVertBuf[vTop + 2] = _ToInt(buf, Top + 8);
                        mCoordBuf[cTop + 0] = _ToInt(buf, Top + 12);
                        mCoordBuf[cTop + 1] = 65536 - _ToInt(buf, Top + 16);
                        if (isDebug) {
                            Log.d("nnSprite", String.format("Vert:%2d-%2d 0x%08x 0x%08x 0x%08x 0x%08x 0x%08x", Integer.valueOf(i2), Integer.valueOf(j), Integer.valueOf(_ToInt(buf, Top + 0)), Integer.valueOf(_ToInt(buf, Top + 4)), Integer.valueOf(_ToInt(buf, Top + 8)), Integer.valueOf(_ToInt(buf, Top + 12)), Integer.valueOf(_ToInt(buf, Top + 16))));
                        }
                    }
                }
                this.iVertex = makeIntBuffer(mVertBuf);
                this.iCoord = makeIntBuffer(mCoordBuf);
            } catch (IOException e2) {
                e = e2;
                e.printStackTrace();
            }
        } catch (IOException e3) {
            e = e3;
            e.printStackTrace();
        }
    }

    public void LoadTextures(Context context, GL10 gl, InputStream isTex) {
        this.mTexTbl = new int[1];
        this.mTexTbl[0] = nnGraph.LoadTexture(context, gl, isTex);
    }

    public void LoadTextures(Context context, GL10 gl, int[] tbl) {
        this.mTexTbl = new int[tbl.length];
        for (int i = 0; i < tbl.length; i++) {
            this.mTexTbl[i] = nnGraph.LoadTexture(context, gl, tbl[i]);
        }
    }

    protected static IntBuffer makeIntBuffer(int[] arr) {
        ByteBuffer bb = ByteBuffer.allocateDirect(arr.length * 4);
        bb.order(ByteOrder.nativeOrder());
        IntBuffer ib = bb.asIntBuffer();
        ib.put(arr);
        ib.position(0);
        return ib;
    }

    protected static int _ToInt(byte[] buf, int Index) {
        return (buf[Index + 0] & 255) | ((buf[Index + 1] << 8) & 65280) | ((buf[Index + 2] << 16) & 16711680) | ((buf[Index + 3] << 24) & -16777216);
    }

    public void Draw(GL10 gl, int Index) {
        nnSprForm _Form = this.mForm[Index];
        gl.glEnable(3553);
        gl.glVertexPointer(3, 5132, 0, this.iVertex);
        gl.glEnableClientState(32884);
        gl.glTexCoordPointer(2, 5132, 0, this.iCoord);
        gl.glEnableClientState(32888);
        gl.glDisableClientState(32886);
        for (int i = 0; i < _Form.CellNum; i++) {
            nnSprCell _Cell = _Form.mCell[i];
            gl.glBindTexture(3553, this.mTexTbl[_Cell.ImgBank]);
            if ((_Cell.CellAtb & 255) != 0) {
                gl.glBlendFunc(770, 1);
                gl.glDrawArrays(5, _Cell.VertexIndex * 4, 4);
                gl.glBlendFunc(770, 771);
            } else {
                gl.glDrawArrays(5, _Cell.VertexIndex * 4, 4);
            }
        }
        gl.glDisable(3553);
    }

    public void SetTexFilter(GL10 gl, int Para) {
        for (int glBindTexture : this.mTexTbl) {
            gl.glBindTexture(3553, glBindTexture);
            gl.glTexParameterf(3553, 10241, (float) Para);
            gl.glTexParameterf(3553, 10240, (float) Para);
        }
    }
}
