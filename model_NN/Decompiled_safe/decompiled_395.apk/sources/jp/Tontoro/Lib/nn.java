package jp.Tontoro.Lib;

import java.util.Calendar;

public class nn {
    public static final float PI = 3.1415927f;
    public static final float PI2 = 6.2831855f;

    public static String GetDateStr() {
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(1);
        int month = calendar.get(2);
        int day = calendar.get(5);
        int i = calendar.get(11);
        int i2 = calendar.get(12);
        int i3 = calendar.get(13);
        return String.format("%4d/%2d/%2d", Integer.valueOf(year), Integer.valueOf(month + 1), Integer.valueOf(day));
    }
}
