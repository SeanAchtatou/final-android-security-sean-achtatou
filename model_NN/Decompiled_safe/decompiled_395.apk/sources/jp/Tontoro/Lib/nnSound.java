package jp.Tontoro.Lib;

import android.content.Context;
import android.media.SoundPool;

public class nnSound {
    private boolean mEnable = true;
    private SoundPool mSoundPool;
    private int[] mSoundTbl;

    public void Init(Context context, int[] tbl) {
        this.mSoundPool = new SoundPool(10, 3, 0);
        this.mSoundTbl = new int[tbl.length];
        for (int i = 0; i < tbl.length; i++) {
            if (tbl[i] != 0) {
                this.mSoundTbl[i] = this.mSoundPool.load(context, tbl[i], 1);
            }
        }
    }

    public void Play(int Index) {
        if (this.mEnable) {
            this.mSoundPool.play(this.mSoundTbl[Index], 100.0f, 100.0f, 1, 0, 1.0f);
        }
    }

    public void SetEnable(boolean sw) {
        this.mEnable = sw;
    }
}
