package jp.Tontoro.Lib;

import android.content.Context;
import java.util.HashMap;
import javax.microedition.khronos.opengles.GL10;
import jp.Tontoro.Lib.nnFont;

public class nnButton {
    public static final int MODE_DECIDE = 3;
    public static final int MODE_FOCUS = 1;
    public static final int MODE_NORMAL = 0;
    public static final int MODE_RESERVE_DECIDE = 2;
    public static final int MODE_STOP = 4;
    /* access modifiers changed from: private */
    public float mFlashTimer = 0.0f;
    /* access modifiers changed from: private */
    public nnFont mFont;
    private HashMap<Integer, BtnObj> mMap = new HashMap<>();
    /* access modifiers changed from: private */
    public float mPanelH;
    /* access modifiers changed from: private */
    public float mPanelMax;
    /* access modifiers changed from: private */
    public float mPanelMin;
    private nnRGBA mPanelRGBA = new nnRGBA();
    /* access modifiers changed from: private */
    public float mPanelW;
    private int mSE_ID = 0;
    private nnSound mSound = null;
    /* access modifiers changed from: private */
    public int mTex;
    private int mTexH;
    private int mTexW;
    private nnRGBA mTextRGBA = new nnRGBA();
    /* access modifiers changed from: private */
    public float mTuchH;
    private float mTuchW;
    private int mVib_ID = 0;
    private nnVibration mVibration = null;

    private class BtnObj {
        private float mFade = 0.0f;
        private int mID;
        private int mMode = 0;
        private nnFont.Packet mPacket;
        private float mPamelTuchW;
        private float mPanelMargin;
        /* access modifiers changed from: private */
        public nnRGBA mPanelRGBA = new nnRGBA();
        private float mPosX;
        private float mPosY;
        private String mStr;
        private float mStrH;
        private float mStrW;
        /* access modifiers changed from: private */
        public nnRGBA mTextRGBA = new nnRGBA();

        BtnObj(int ID, float PosX, float PosY, String str) {
            this.mID = ID;
            this.mPosX = PosX;
            this.mPosY = PosY;
            this.mStr = str;
            this.mStrW = nnButton.this.mFont.GetStrWidth(this.mStr) * 0.5f;
            this.mStrH = nnButton.this.mFont.GetLeading() * 0.5f;
            this.mPacket = nnButton.this.mFont.GetPacket();
            this.mPacket.MakePacket(str, -this.mStrW, this.mStrH, 1.0f);
            this.mPanelMargin = this.mStrW - 0.0f;
            if (this.mPanelMargin < 0.0f) {
                this.mPanelMargin = 0.0f;
            }
            this.mPamelTuchW = this.mStrW + nnButton.this.mPanelW;
            if (this.mPamelTuchW < nnButton.this.mPanelMin * 0.5f) {
                this.mPamelTuchW = nnButton.this.mPanelMin * 0.5f;
            }
            if (this.mPamelTuchW > nnButton.this.mPanelMax * 0.5f) {
                this.mPamelTuchW = nnButton.this.mPanelMax * 0.5f;
            }
        }

        public void Move(nnFramework Framework) {
            if (this.mMode != 4 && this.mMode != 3) {
                if (this.mMode == 2) {
                    this.mFade += 0.06666667f;
                    if (this.mFade >= 1.0f) {
                        SetMode(3);
                        return;
                    }
                    return;
                }
                this.mMode = 0;
                if (Framework.mTouchLev && Math.abs(Framework.mTouchMoveX - this.mPosX) < this.mPamelTuchW && Math.abs(Framework.mTouchMoveY - this.mPosY) < nnButton.this.mTuchH) {
                    SetMode(1);
                }
                if (Framework.mTouchOffTrg && Math.abs(Framework.mTouchMoveX - this.mPosX) < this.mPamelTuchW && Math.abs(Framework.mTouchMoveY - this.mPosY) < nnButton.this.mTuchH) {
                    SetMode(2);
                }
            }
        }

        /* access modifiers changed from: private */
        public void SetMode(int Mode) {
            if (Mode == 3) {
                this.mFade = 1.0f;
            }
            if (Mode == 2) {
                nnButton.this.Vibrate();
                nnButton.this.SoundPlay();
                this.mFade = 0.0f;
                nnButton.this.SetAllMode(4);
            }
            this.mMode = Mode;
        }

        /* access modifiers changed from: private */
        public int GetMode() {
            return this.mMode;
        }

        private void _DrawPanel(GL10 gl, float v) {
            float CntW = this.mPamelTuchW - nnButton.this.mPanelW;
            if (CntW > 0.0f) {
                nnGraph.DrawRectUVPrim(gl, -CntW, nnButton.this.mPanelH, CntW, -nnButton.this.mPanelH, 0.5f, v, 0.5f, v + 0.25f);
            }
            nnGraph.DrawRectUVPrim(gl, (-CntW) - nnButton.this.mPanelW, nnButton.this.mPanelH, -CntW, -nnButton.this.mPanelH, 0.0f, v, 0.5f, v + 0.25f);
            nnGraph.DrawRectUVPrim(gl, CntW, nnButton.this.mPanelH, CntW + nnButton.this.mPanelW, -nnButton.this.mPanelH, 0.5f, v, 1.0f, v + 0.25f);
        }

        public void Draw(GL10 gl) {
            gl.glPushMatrix();
            gl.glTranslatef(this.mPosX, this.mPosY, 0.0f);
            gl.glEnable(3553);
            gl.glBindTexture(3553, nnButton.this.mTex);
            gl.glColor4f(this.mPanelRGBA.r, this.mPanelRGBA.g, this.mPanelRGBA.b, this.mPanelRGBA.a);
            if (this.mMode == 0) {
                _DrawPanel(gl, 0.0f);
                gl.glColor4f(1.0f, 1.0f, 1.0f, ((((float) Math.sin((double) (nnButton.this.mFlashTimer * 3.1415927f))) * 0.5f) + 0.5f) * 0.2f);
                _DrawPanel(gl, 0.5f);
            }
            if (this.mMode == 1) {
                _DrawPanel(gl, 0.0f);
                gl.glColor4f(1.0f, 1.0f, 1.0f, ((((float) Math.sin((double) (nnButton.this.mFlashTimer * 6.2831855f))) * 0.5f) + 0.5f) * 1.0f);
                _DrawPanel(gl, 0.25f);
            }
            if (this.mMode == 3 || this.mMode == 2) {
                _DrawPanel(gl, 0.0f);
                gl.glColor4f(1.0f, 1.0f, 1.0f, ((((float) Math.sin((double) (nnButton.this.mFlashTimer * 6.2831855f * 2.0f))) * 0.5f) + 0.5f) * 1.0f);
                _DrawPanel(gl, 0.5f);
            }
            if (this.mMode == 4) {
                _DrawPanel(gl, 0.0f);
            }
            gl.glColor4f(this.mTextRGBA.r, this.mTextRGBA.g, this.mTextRGBA.b, this.mTextRGBA.a);
            this.mPacket.DrawPacket(gl);
            gl.glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
            gl.glDisable(3553);
            gl.glPopMatrix();
        }
    }

    public nnButton(Context context, GL10 gl, int tex, int TexW, int TexH, nnFont Font, nnSound Sound, int SE, nnVibration Vibration, int Vib) {
        this.mTex = nnGraph.LoadTexture(context, gl, tex);
        this.mTexW = TexW;
        this.mTexH = TexH;
        float f = ((float) TexW) * 0.5f;
        this.mTuchW = f;
        this.mPanelW = f;
        float f2 = (((float) TexH) * 0.5f) / 4.0f;
        this.mTuchH = f2;
        this.mPanelH = f2;
        this.mPanelH = (((float) TexH) * 0.5f) / 4.0f;
        this.mFont = Font;
        this.mSound = Sound;
        this.mSE_ID = SE;
        this.mVibration = Vibration;
        this.mVib_ID = Vib;
    }

    public void Move(nnFramework Framework) {
        this.mFlashTimer += 0.033333335f;
        if (this.mFlashTimer > 1.0f) {
            this.mFlashTimer -= 1.0f;
        }
        for (Object Key : this.mMap.keySet()) {
            this.mMap.get(Key).Move(Framework);
        }
    }

    public void Draw(GL10 gl) {
        for (Object Key : this.mMap.keySet()) {
            this.mMap.get(Key).Draw(gl);
        }
    }

    public void Add(int ID, float PosX, float PosY, String str) {
        BtnObj Obj = new BtnObj(ID, PosX, PosY, str);
        Obj.mTextRGBA.SetRGBA(this.mTextRGBA);
        Obj.mPanelRGBA.SetRGBA(this.mPanelRGBA);
        this.mMap.put(Integer.valueOf(ID), Obj);
    }

    public void SetMode(int ID, int Mode) {
        BtnObj Obj = this.mMap.get(Integer.valueOf(ID));
        if (Obj != null) {
            Obj.SetMode(Mode);
        }
    }

    public int GetMode(int ID) {
        BtnObj Obj = this.mMap.get(Integer.valueOf(ID));
        if (Obj == null) {
            return 0;
        }
        return Obj.GetMode();
    }

    public void SetTextRGBA(float r, float g, float b, float a) {
        this.mTextRGBA.SetRGBA(r, g, b, a);
        for (Object Key : this.mMap.keySet()) {
            this.mMap.get(Key).mTextRGBA.SetRGBA(r, g, b, a);
        }
    }

    public void SetTextRGBA(int ID, float r, float g, float b, float a) {
        BtnObj Obj = this.mMap.get(Integer.valueOf(ID));
        if (Obj != null) {
            Obj.mTextRGBA.SetRGBA(r, g, b, a);
        }
    }

    public void SetPanelRGBA(float r, float g, float b, float a) {
        this.mPanelRGBA.SetRGBA(r, g, b, a);
        for (Object Key : this.mMap.keySet()) {
            this.mMap.get(Key).mPanelRGBA.SetRGBA(r, g, b, a);
        }
    }

    public void SetPanelRGBA(int ID, float r, float g, float b, float a) {
        BtnObj Obj = this.mMap.get(Integer.valueOf(ID));
        if (Obj != null) {
            Obj.mPanelRGBA.SetRGBA(r, g, b, a);
        }
    }

    public void SetPanelMinMax(float min, float max) {
        this.mPanelMin = min;
        this.mPanelMax = max;
    }

    public void SetAllMode(int Mode) {
        for (Object Key : this.mMap.keySet()) {
            this.mMap.get(Key).SetMode(Mode);
        }
    }

    public void SetPanelHeight(float Height) {
        this.mPanelH = Height;
    }

    public void SetTuchHeight(float Height) {
        this.mTuchH = Height;
    }

    public void SoundPlay() {
        if (this.mSE_ID != 0 && this.mSound != null) {
            this.mSound.Play(this.mSE_ID);
        }
    }

    public void Vibrate() {
        if (this.mVib_ID != 0 && this.mVibration != null) {
            this.mVibration.Vibrate(100);
        }
    }
}
