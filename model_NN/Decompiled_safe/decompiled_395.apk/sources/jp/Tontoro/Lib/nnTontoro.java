package jp.Tontoro.Lib;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import java.io.IOException;
import java.io.InputStream;
import javax.microedition.khronos.opengles.GL10;

public class nnTontoro {
    private static final String BlogURL = "http://tontoro-games.blogspot.com/";
    private static final String MaketURL = "market://search?q=pub:%22TONTORO%20games%22";
    public BtnFeint mBtnFeint;
    public BtnMaker mBtnMaker;
    public Context mContext;
    public GL10 mGL;
    public nnSprite mSprite;

    public class ButtonBase {
        boolean mChkNetwork = true;
        public boolean mEnable = true;
        float mFlash = 0.0f;
        float mPosX = 0.0f;
        float mPosY = 0.0f;
        float mTimer = 0.0f;
        public boolean mVisible = true;

        public ButtonBase() {
        }

        public void Init() {
        }

        public void Move(nnFramework Framework) {
            if (this.mFlash > 0.0f) {
                this.mTimer = 0.0f;
                this.mFlash -= 0.033333335f;
                if (this.mFlash <= 0.0f) {
                    this.mFlash = 0.0f;
                    return;
                }
                return;
            }
            this.mTimer += 0.0125f;
            if (this.mTimer >= 1.0f) {
                this.mTimer -= 1.0f;
            }
        }

        public void SetPos(float x, float y) {
            this.mPosX = x;
            this.mPosY = y;
        }

        public boolean Chk(nnFramework Framework) {
            if (!this.mEnable) {
                return false;
            }
            if (!this.mVisible) {
                return false;
            }
            if (this.mFlash > 0.0f) {
                return false;
            }
            if (!Framework.mTouchOffTrg) {
                return false;
            }
            float _x = Framework.mTouchMoveX - this.mPosX;
            float _y = Framework.mTouchMoveY - this.mPosY;
            return (_x * _x) + (_y * _y) < 1024.0f;
        }

        public void Execute() {
            this.mFlash = 1.0f;
        }

        public void SetChkNetwork(boolean sw) {
            this.mChkNetwork = sw;
        }

        public void Draw(int SpriteIndex) {
            if (this.mVisible) {
                if (this.mEnable) {
                    float _f = 0.8f + (0.2f * ((float) Math.cos((double) (6.2831855f * this.mTimer))));
                    if (this.mFlash > 0.0f) {
                        _f = 1.0f;
                    }
                    nnTontoro.this.mGL.glColor4f(_f, _f, _f, 1.0f);
                } else {
                    nnTontoro.this.mGL.glColor4f(0.5f, 0.5f, 0.5f, 0.5f);
                }
                nnTontoro.this.mGL.glPushMatrix();
                nnTontoro.this.mGL.glTranslatef(this.mPosX, this.mPosY, 0.0f);
                nnTontoro.this.mSprite.Draw(nnTontoro.this.mGL, SpriteIndex);
                nnTontoro.this.mGL.glPopMatrix();
                if (this.mFlash > 0.0f) {
                    nnTontoro.this.mGL.glPushMatrix();
                    nnTontoro.this.mGL.glColor4f(1.0f, 1.0f, 1.0f, this.mFlash * this.mFlash * this.mFlash);
                    float _s = 1.0f + ((1.0f - (this.mFlash * this.mFlash)) * 0.5f);
                    nnTontoro.this.mGL.glTranslatef(this.mPosX, this.mPosY, 0.0f);
                    nnTontoro.this.mGL.glScalef(_s, _s, 1.0f);
                    nnTontoro.this.mSprite.Draw(nnTontoro.this.mGL, 4);
                    nnTontoro.this.mGL.glPopMatrix();
                }
                nnTontoro.this.mGL.glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
            }
        }
    }

    public class BtnMaker extends ButtonBase {
        public BtnMaker() {
            super();
        }

        public void Move(nnFramework Framework) {
            if (this.mChkNetwork) {
                this.mEnable = nnTontoro.this.ChkNetworkIsConnected();
            }
            super.Move(Framework);
        }

        public void ExecuteMarket() {
            super.Execute();
            nnTontoro.this.mContext.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(nnTontoro.MaketURL)));
        }

        public void ExecuteBlog() {
            super.Execute();
            nnTontoro.this.mContext.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(nnTontoro.BlogURL)));
        }
    }

    public class BtnFeint extends ButtonBase {
        public BtnFeint() {
            super();
        }

        public void Move(nnFramework Framework) {
            if (this.mChkNetwork) {
                this.mEnable = nnTontoro.this.ChkFeint();
            }
            super.Move(Framework);
        }

        public void Execute() {
            super.Execute();
        }
    }

    public nnTontoro(Context context, GL10 gl) {
        this.mContext = context;
        this.mGL = gl;
        try {
            InputStream isSpr = context.getAssets().open("tontoro.spf.bin");
            InputStream isTex = context.getAssets().open("tontoro00.png");
            this.mSprite = new nnSprite(isSpr);
            this.mSprite.LoadTextures(this.mContext, gl, isTex);
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.mBtnMaker = new BtnMaker();
        this.mBtnFeint = new BtnFeint();
    }

    public void Init() {
        this.mBtnMaker.Init();
        this.mBtnFeint.Init();
    }

    public void Move(nnFramework Framework) {
        this.mBtnMaker.Move(Framework);
        this.mBtnFeint.Move(Framework);
    }

    public void DrawCopyright(float x, float y) {
        this.mGL.glPushMatrix();
        this.mGL.glTranslatef(x, y, 0.0f);
        this.mSprite.Draw(this.mGL, 5);
        this.mGL.glPopMatrix();
    }

    public void DrawMaker() {
        this.mBtnMaker.Draw(1);
    }

    public void DrawFeint() {
        this.mBtnFeint.Draw(2);
    }

    public boolean ChkFeint() {
        if (!ChkNetworkIsConnected()) {
            return false;
        }
        return nnFeint.ChkUserLogin();
    }

    public boolean ChkNetworkIsConnected() {
        NetworkInfo ni = ((ConnectivityManager) this.mContext.getSystemService("connectivity")).getActiveNetworkInfo();
        if (ni != null) {
            return ni.isConnected();
        }
        return false;
    }
}
