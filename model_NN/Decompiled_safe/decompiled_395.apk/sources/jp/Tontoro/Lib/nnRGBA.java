package jp.Tontoro.Lib;

public class nnRGBA {
    public float a;
    public float b;
    public float g;
    public float r;

    public nnRGBA() {
        this.r = 1.0f;
        this.g = 1.0f;
        this.b = 1.0f;
        this.a = 1.0f;
    }

    public nnRGBA(float _r, float _g, float _b, float _a) {
        this.r = _r;
        this.g = _g;
        this.b = _b;
        this.a = _a;
    }

    public void SetRGBA(float _r, float _g, float _b, float _a) {
        this.r = _r;
        this.g = _g;
        this.b = _b;
        this.a = _a;
    }

    public void SetRGBA(nnRGBA rgba) {
        this.r = rgba.r;
        this.g = rgba.g;
        this.b = rgba.b;
        this.a = rgba.a;
    }
}
