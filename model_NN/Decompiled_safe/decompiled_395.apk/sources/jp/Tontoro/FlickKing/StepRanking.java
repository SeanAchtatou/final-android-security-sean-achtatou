package jp.Tontoro.FlickKing;

import javax.microedition.khronos.opengles.GL10;

public class StepRanking {
    float mDrawTgtX = 0.0f;
    float mDrawX = 0.0f;
    float mFlash = 0.0f;
    GL10 mGL;
    GameMain mParent;
    private Ranking[] mRanking = new Ranking[3];

    public void Init(GameMain Parent, GL10 gl) {
        this.mParent = Parent;
        this.mGL = gl;
    }

    public void _Init() {
        for (int i = 0; i < 3; i++) {
            this.mRanking[i] = new Ranking();
        }
        this.mRanking[0].Init(this.mParent.mContext, 0);
        this.mRanking[1].Init(this.mParent.mContext, 1);
        this.mRanking[2].Init(this.mParent.mContext, 2);
        this.mParent.OpenGate();
    }

    public boolean ButtonChk(GameMain Parent, float x, float y, float Rad) {
        if (!this.mParent.mTouchTrg) {
            return false;
        }
        float _x = Parent.mTouchMoveX - x;
        float _y = Parent.mTouchMoveY - y;
        if ((_x * _x) + (_y * _y) < Rad * Rad) {
            return true;
        }
        return false;
    }

    public void Move() {
        switch (this.mParent.Step1()) {
            case 0:
                if (ButtonChk(this.mParent, 128.0f, -200.0f, 32.0f)) {
                    this.mParent.mButtonTitle.SoundPlay();
                    this.mParent.mButtonTitle.Vibrate();
                    this.mDrawTgtX = 320.0f;
                    this.mParent.Step1Set(1);
                    break;
                }
                break;
            case 1:
                if (ButtonChk(this.mParent, -128.0f, -200.0f, 32.0f)) {
                    this.mParent.mButtonTitle.SoundPlay();
                    this.mParent.mButtonTitle.Vibrate();
                    this.mDrawTgtX = 0.0f;
                    this.mParent.Step1Set(0);
                }
                if (ButtonChk(this.mParent, 128.0f, -200.0f, 32.0f)) {
                    this.mParent.mButtonTitle.SoundPlay();
                    this.mParent.mButtonTitle.Vibrate();
                    this.mDrawTgtX = 640.0f;
                    this.mParent.Step1Set(2);
                    break;
                }
                break;
            case 2:
                if (ButtonChk(this.mParent, -128.0f, -200.0f, 32.0f)) {
                    this.mParent.mButtonTitle.SoundPlay();
                    this.mParent.mButtonTitle.Vibrate();
                    this.mDrawTgtX = 320.0f;
                    this.mParent.Step1Set(1);
                    break;
                }
                break;
        }
        switch (this.mParent.Step1()) {
            case 0:
            case 1:
            case 2:
                if ((this.mParent.mKeyTrg & 16) != 0) {
                    this.mParent.CloseGate();
                    this.mParent.Step1Set(224);
                    return;
                }
                break;
            case 224:
                if (this.mParent.SyncGate()) {
                    this.mParent.Step0Set(1);
                    return;
                }
                break;
        }
        this.mFlash += 0.033333335f;
        if (this.mFlash > 1.0f) {
            this.mFlash -= 1.0f;
        }
        this.mDrawX += (this.mDrawTgtX - this.mDrawX) * 0.1f;
    }

    public void Draw() {
        this.mGL.glPushMatrix();
        this.mGL.glTranslatef(0.0f - this.mDrawX, 0.0f, 0.0f);
        this.mRanking[0].Draw(this.mGL, this.mParent.mFont, this.mParent.mSprite);
        this.mParent.DrawGate(this.mGL);
        this.mGL.glPushMatrix();
        this.mGL.glTranslatef(320.0f - this.mDrawX, 0.0f, 0.0f);
        this.mRanking[1].Draw(this.mGL, this.mParent.mFont, this.mParent.mSprite);
        this.mParent.DrawGate(this.mGL);
        this.mGL.glPushMatrix();
        this.mGL.glTranslatef(640.0f - this.mDrawX, 0.0f, 0.0f);
        this.mRanking[2].Draw(this.mGL, this.mParent.mFont, this.mParent.mSprite);
        this.mParent.DrawGate(this.mGL);
        float _f = 1.0f - this.mParent.GetGateFade();
        if (this.mParent.Step1() == 1 || this.mParent.Step1() == 2) {
            this.mGL.glPushMatrix();
            this.mGL.glTranslatef(-64.0f * _f * _f, 0.0f, 0.0f);
            this.mParent.mSprite.Draw(this.mGL, 27);
            this.mGL.glTranslatef((this.mFlash * 4.0f) - 2.0f, 0.0f, 0.0f);
            this.mParent.mSprite.Draw(this.mGL, 28);
            this.mGL.glPopMatrix();
        }
        if (this.mParent.Step1() == 1 || this.mParent.Step1() == 0) {
            this.mGL.glPushMatrix();
            this.mGL.glTranslatef(64.0f * _f * _f, 0.0f, 0.0f);
            this.mParent.mSprite.Draw(this.mGL, 25);
            this.mGL.glTranslatef(-((this.mFlash * 4.0f) - 2.0f), 0.0f, 0.0f);
            this.mParent.mSprite.Draw(this.mGL, 26);
            this.mGL.glPopMatrix();
        }
    }
}
