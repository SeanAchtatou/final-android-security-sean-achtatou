package jp.Tontoro.FlickKing;

import java.util.LinkedList;
import javax.microedition.khronos.opengles.GL10;
import jp.Tontoro.Lib.nnRand;
import jp.Tontoro.Lib.nnSprite;

public class EffSmoke {
    LinkedList PtclList = new LinkedList();
    nnRand mRand = new nnRand();

    public class Ptcl {
        float mFade = 1.0f;
        float mPosX;
        float mPosY;
        float mSpdX;
        float mSpdY;

        public Ptcl(float _x, float _y) {
            this.mPosX = _x;
            this.mPosY = _y;
            float _agl = EffSmoke.this.mRand.Getf() * 6.2831855f;
            this.mSpdX = ((float) Math.sin((double) _agl)) * 6.0f;
            this.mSpdY = ((float) Math.cos((double) _agl)) * 6.0f;
        }

        public boolean Move() {
            this.mFade -= 0.025f;
            if (this.mFade <= 0.0f) {
                return true;
            }
            this.mPosX += this.mSpdX * this.mFade * this.mFade;
            this.mPosY += this.mSpdY * this.mFade * this.mFade;
            return false;
        }

        public void Draw(GL10 gl, nnSprite Sprite) {
            if (this.mFade > 0.0f) {
                gl.glPushMatrix();
                gl.glTranslatef(this.mPosX, this.mPosY, 0.0f);
                gl.glScalef(((1.0f - (this.mFade * this.mFade)) * 0.5f) + 1.5f, ((1.0f - (this.mFade * this.mFade)) * 0.5f) + 1.5f, 1.0f);
                gl.glColor4f(1.0f, 1.0f, 1.0f, this.mFade * 0.3f);
                Sprite.Draw(gl, 40);
                gl.glPopMatrix();
            }
        }
    }

    public void Init() {
        this.PtclList.clear();
        this.mRand.SetSeed(0, 0, 0, 0);
    }

    public void Add(int Dir) {
        float _x = ((this.mRand.Getf() * 2.0f) - 1.0f) * 160.0f;
        float _s = (float) Math.sin((double) (((float) Dir) * 0.5f * 3.1415927f));
        float _c = (float) Math.cos((double) (((float) Dir) * 0.5f * 3.1415927f));
        this.PtclList.add(new Ptcl((_x * _c) + (-160.0f * _s), -((_x * _s) + (-160.0f * _c))));
    }

    public void Move() {
        int i = 0;
        while (i < this.PtclList.size()) {
            if (((Ptcl) this.PtclList.get(i)).Move()) {
                this.PtclList.remove(i);
                i--;
            }
            i++;
        }
    }

    public void Draw(GL10 gl, nnSprite Sprite) {
        for (int i = 0; i < this.PtclList.size(); i++) {
            ((Ptcl) this.PtclList.get(i)).Draw(gl, Sprite);
        }
    }
}
