package jp.Tontoro.FlickKing;

import javax.microedition.khronos.opengles.GL10;
import jp.Tontoro.Lib.nnFramework;
import jp.Tontoro.Lib.nnGraph;
import jp.Tontoro.Lib.nnRand;

public class StepGame {
    static final String[] GAMEMODE_PANELLABELTBL = {"FLICK 10", "FLICK 20", "FLAP 30"};
    static final int[] GAMEMODE_PANELNUMTBL = {10, 20, 30};
    public static final int PANEL_NUM = 30;
    public static final int SCORE_MAX = 999999;
    public static final int SECount_STEP = 250;
    int mClearPanel = 0;
    int mCount;
    float mCountFade;
    EffSmoke mEffSmoke;
    float mFinishFade;
    float mFinishFlash;
    GL10 mGL;
    int mGameMode;
    private ObjPanel[] mPanel = new ObjPanel[30];
    GameMain mParent;
    nnRand mRand = new nnRand();
    Ranking mRanking = new Ranking();
    int mScore;
    int mScoreSECount;
    int mStep;
    long mTimerN;
    long mTimerS;
    int mWait;

    public void Init(GameMain Parent, GL10 gl) {
        this.mEffSmoke = new EffSmoke();
        this.mParent = Parent;
        this.mGL = gl;
    }

    public void Init(int GameMode) {
        this.mEffSmoke.Init();
        this.mGameMode = GameMode;
        this.mStep = 0;
        this.mTimerN = 0;
        this.mClearPanel = 0;
        this.mScore = 0;
        this.mScoreSECount = 0;
        for (int i = 0; i < 30; i++) {
            this.mPanel[i] = new ObjPanel(i);
        }
        if (this.mGameMode != 2) {
            this.mRand.SetSeed((int) System.nanoTime(), 123, 456, 789);
            int[] _tbl = new int[30];
            int _s = this.mRand.Get(4);
            int _pnum = GAMEMODE_PANELNUMTBL[this.mGameMode];
            for (int i2 = 0; i2 < 30; i2++) {
                _tbl[i2] = (_s + i2) % 4;
            }
            for (int i3 = 0; i3 < _pnum; i3++) {
                int _r = this.mRand.Get(_pnum);
                int _temp = _tbl[_r];
                _tbl[_r] = _tbl[i3];
                _tbl[i3] = _temp;
            }
            for (int i4 = 0; i4 < 30; i4++) {
                this.mPanel[i4].Init(_tbl[i4]);
            }
            return;
        }
        for (int i5 = 0; i5 < 30; i5++) {
            this.mPanel[i5].Init((i5 & 1) == 0 ? 1 : 3);
        }
    }

    public void MoveAllPanel(boolean flag) {
        for (int i = 0; i < GAMEMODE_PANELNUMTBL[this.mGameMode]; i++) {
            this.mPanel[i].Move(this, flag);
        }
    }

    public void Move() {
        this.mEffSmoke.Move();
        this.mFinishFlash += 0.2f;
        if (this.mFinishFlash >= 1.0f) {
            this.mFinishFlash -= 1.0f;
        }
        switch (this.mStep) {
            case 0:
                this.mParent.OpenGate();
                this.mCountFade = 0.0f;
                this.mCount = 3;
                this.mFinishFade = 1.0f;
                this.mFinishFlash = 0.0f;
                this.mStep++;
                break;
            case 1:
                MoveAllPanel(false);
                float _Back = this.mCountFade;
                this.mCountFade += 0.016666668f;
                if (_Back < 0.5f && this.mCountFade >= 0.5f) {
                    this.mParent.mSound.Play(this.mCount > 1 ? 5 : 6);
                }
                if (this.mCountFade > 1.0f) {
                    this.mCount--;
                    if (this.mCount == 0) {
                        this.mCountFade = 1.0f;
                        this.mTimerS = System.nanoTime();
                        this.mStep++;
                        break;
                    } else {
                        this.mCountFade = 0.0f;
                        break;
                    }
                }
                break;
            case 2:
                this.mTimerN = System.nanoTime() - this.mTimerS;
                this.mScore = (int) (this.mTimerN / 1000000);
                if (this.mScore > this.mScoreSECount) {
                    this.mScoreSECount += SECount_STEP;
                    this.mParent.mSound.Play(7);
                }
                if (this.mScore > 999999) {
                    this.mScore = 999999;
                }
                MoveAllPanel(true);
                if (this.mPanel[GAMEMODE_PANELNUMTBL[this.mGameMode] - 1].ChkClear()) {
                    this.mFinishFade = 1.0f;
                    this.mStep++;
                    break;
                }
                break;
            case 3:
                MoveAllPanel(false);
                this.mFinishFade -= 0.05f;
                if (this.mFinishFade < 0.0f) {
                    this.mWait = 0;
                    this.mStep = 4;
                    break;
                }
                break;
            case 4:
                MoveAllPanel(false);
                this.mWait++;
                if (this.mWait > 60) {
                    this.mParent.mButtonRetry.SetAllMode(0);
                    this.mRanking.Init(this.mParent.mContext, this.mGameMode);
                    this.mRanking.EntryScore(this.mParent.mContext, this.mScore);
                    this.mFinishFade = 0.0f;
                    this.mStep = 16;
                    break;
                }
                break;
            case 14:
                if (this.mParent.SyncGate()) {
                    this.mParent.Step0Set(1);
                    return;
                }
                return;
            case nnFramework.KEY_BACK /*16*/:
                this.mRanking.Move();
                this.mParent.mButtonRetry.Move(this.mParent);
                if (this.mParent.mButtonRetry.GetMode(0) == 3) {
                    this.mStep = 224;
                    return;
                } else if ((this.mParent.mKeyTrg & 16) != 0) {
                    this.mStep = 224;
                    return;
                } else if (this.mParent.mButtonRetry.GetMode(1) == 3) {
                    this.mParent.CloseGate();
                    this.mStep = 17;
                    return;
                } else {
                    return;
                }
            case 17:
                if (this.mParent.SyncGate()) {
                    this.mWait = 0;
                    this.mStep = 18;
                    return;
                }
                return;
            case 18:
                this.mWait++;
                if (this.mWait > 20) {
                    Init(this.mGameMode);
                    this.mParent.OpenGate();
                    return;
                }
                return;
            case 224:
                this.mParent.CloseGate();
                this.mStep = 225;
                return;
            case 225:
                if (this.mParent.SyncGate()) {
                    this.mParent.Step0Set(1);
                    return;
                }
                return;
        }
        if ((this.mParent.mKeyTrg & 16) != 0) {
            this.mParent.CloseGate();
            this.mStep = 14;
        }
    }

    public void Draw() {
        if (this.mFinishFade == 1.0f) {
            this.mGL.glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
            nnGraph.DrawRectPrim(this.mGL, -160.0f, -160.0f, 160.0f, 160.0f);
        } else {
            float _f = 1.0f - this.mFinishFade;
            this.mGL.glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
            nnGraph.DrawRectPrim(this.mGL, -160.0f, -160.0f, 160.0f, 160.0f);
            this.mGL.glPushMatrix();
            this.mGL.glScalef(_f * _f, _f * _f, 1.0f);
            this.mParent.mSprite.Draw(this.mGL, 17);
            this.mGL.glColor4f(1.0f, 1.0f, 1.0f, ((this.mFinishFlash * 0.5f) + 0.5f) * ((this.mFinishFade * 0.75f) + 0.25f));
            this.mParent.mSprite.Draw(this.mGL, 18);
            this.mGL.glPopMatrix();
            this.mGL.glColor4f(1.0f, 1.0f, 1.0f, this.mFinishFade * this.mFinishFade);
        }
        for (int i = GAMEMODE_PANELNUMTBL[this.mGameMode] - 1; i >= 0; i--) {
            this.mPanel[i].Draw(this.mGL, this.mParent.mSprite);
        }
        if (this.mCount > 0) {
            float _f2 = this.mCountFade * 2.0f;
            if (_f2 > 1.0f) {
                _f2 = 1.0f;
            }
            this.mGL.glColor4f(1.0f, 1.0f, 1.0f, _f2 * _f2);
            float _s = 2.0f - (_f2 * _f2);
            this.mGL.glPushMatrix();
            this.mGL.glScalef(_s, _s, 1.0f);
            this.mParent.mSprite.Draw(this.mGL, this.mCount + 10);
            this.mGL.glPopMatrix();
        }
        this.mEffSmoke.Draw(this.mGL, this.mParent.mSprite);
        this.mGL.glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
        if (this.mStep >= 16) {
            this.mGL.glColor4f(0.0f, 0.0f, 0.0f, 0.75f);
            nnGraph.DrawRectPrim(this.mGL, -160.0f, -160.0f, 160.0f, 160.0f);
            this.mGL.glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
            this.mRanking.Draw(this.mGL, this.mParent.mFont, this.mParent.mSprite);
        }
        this.mParent.DrawGate(this.mGL);
        if (this.mStep == 16) {
            this.mParent.mButtonRetry.Draw(this.mGL);
        }
        this.mGL.glColor4f(1.0f, 0.9f, 0.6f, this.mParent.GetGateFade());
        this.mParent.mFont.DrawStr("TIME:" + GetScoreStr(this.mScore), -124.0f, 220.0f, 1.0f);
        this.mGL.glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
    }

    static String GetScoreStr(int Score) {
        return String.format("%3d.%03d", Integer.valueOf(Score / 1000), Integer.valueOf(Score % 1000));
    }
}
