package jp.Tontoro.FlickKing;

import java.util.LinkedList;
import javax.microedition.khronos.opengles.GL10;
import jp.Tontoro.Lib.nnGraph;
import jp.Tontoro.Lib.nnSprite;

public class ObjPanel {
    private static final float[] AGL_TBL = {0.0f, 90.0f, 180.0f, 270.0f};
    float mArowFlash = 0.0f;
    int mDir = 0;
    boolean mDrag = false;
    float mGrowFlash = 0.0f;
    int mIndex = 0;
    private float mPosY = 0.0f;
    private float mPosYBack = 0.0f;
    private LinkedList mPosYList = new LinkedList();
    private float mPosYMax = 0.0f;
    private float mSeTrg = 0.0f;
    private float mSmokeTrg = 0.0f;
    private float mSpdY = 0.0f;
    int mStep = 0;

    class PosBuf {
        float Pos;

        PosBuf() {
        }
    }

    public ObjPanel(int Index) {
        this.mIndex = Index;
    }

    /* access modifiers changed from: package-private */
    public void Init(int Dir) {
        this.mDir = Dir;
        this.mPosY = 0.0f;
        this.mSpdY = 0.0f;
        this.mStep = 0;
        this.mSeTrg = 0.0f;
        this.mSmokeTrg = 0.0f;
    }

    /* access modifiers changed from: package-private */
    public void PlaySE(StepGame Parent) {
        if (this.mPosY > this.mSeTrg) {
            Parent.mParent.mSound.Play(11);
            float _add = 25.0f;
            while (this.mPosY > this.mSeTrg) {
                this.mSeTrg += _add;
                _add += 25.0f;
            }
        }
        while (this.mPosY > this.mSmokeTrg) {
            Parent.mEffSmoke.Add(this.mDir);
            this.mSmokeTrg += 20.0f;
        }
    }

    /* access modifiers changed from: package-private */
    public void IncAlowFlash() {
        this.mArowFlash += 0.05f;
        if (this.mArowFlash >= 1.0f) {
            this.mArowFlash -= 1.0f;
        }
    }

    /* access modifiers changed from: package-private */
    public void Move(StepGame Parent, boolean flag) {
        this.mGrowFlash += 0.15f;
        if (this.mGrowFlash > 1.0f) {
            this.mGrowFlash -= 1.0f;
        }
        switch (this.mStep) {
            case 0:
                if (this.mIndex <= Parent.mClearPanel + 1) {
                    this.mStep++;
                    return;
                }
                return;
            case 1:
                if (this.mIndex <= Parent.mClearPanel && flag) {
                    this.mStep++;
                    return;
                }
                return;
            case 2:
                if (flag) {
                    float _sss = -((float) Math.sin((double) (((float) this.mDir) * 3.1415927f * 0.5f)));
                    float _ccc = (float) Math.cos((double) (((float) this.mDir) * 3.1415927f * 0.5f));
                    float TouchMoveY = (Parent.mParent.mTouchMoveY * _ccc) + (Parent.mParent.mTouchMoveX * _sss);
                    float TouchDownY = (Parent.mParent.mTouchDownY * _ccc) + (Parent.mParent.mTouchDownX * _sss);
                    boolean _Out = false;
                    IncAlowFlash();
                    if (Parent.mParent.mTouchLev) {
                        if (Math.abs(Parent.mParent.mTouchMoveX) > 160.0f) {
                            _Out = true;
                        }
                        if (Math.abs(Parent.mParent.mTouchMoveY) > 160.0f) {
                            _Out = true;
                        }
                    }
                    if (Parent.mParent.mTouchTrg && !_Out) {
                        this.mPosYBack = this.mPosY;
                        this.mPosYMax = TouchDownY;
                        this.mPosYList.clear();
                        this.mDrag = true;
                    }
                    if (Parent.mParent.mTouchLev && this.mDrag && !_Out) {
                        if (this.mPosYMax < TouchMoveY) {
                            this.mPosYMax = TouchMoveY;
                        }
                        this.mPosY = this.mPosYBack + (this.mPosYMax - TouchDownY > 0.0f ? this.mPosYMax - TouchDownY : 0.0f);
                        if (this.mPosY < 0.0f) {
                            this.mPosY = 0.0f;
                        }
                        PosBuf _pos = new PosBuf();
                        _pos.Pos = this.mPosY;
                        this.mPosYList.addFirst(_pos);
                    }
                    if (this.mDrag && (Parent.mParent.mTouchOffTrg || _Out)) {
                        this.mDrag = false;
                        int _Size = this.mPosYList.size();
                        if (_Size > 0) {
                            float _Min = 1000.0f;
                            float _Max = 0.0f;
                            if (_Size > 10) {
                                _Size = 10;
                            }
                            for (int i = 0; i < _Size; i++) {
                                PosBuf _pos2 = (PosBuf) this.mPosYList.get(i);
                                if (_pos2.Pos < _Min) {
                                    _Min = _pos2.Pos;
                                }
                                if (_pos2.Pos > _Max) {
                                    _Max = _pos2.Pos;
                                }
                            }
                            this.mSpdY += (_Max - _Min) / ((float) _Size);
                        }
                    }
                    this.mSpdY *= 0.9f;
                    if (this.mSpdY < 0.1f) {
                        this.mSpdY = 0.0f;
                    }
                    this.mPosY += this.mSpdY;
                    PlaySE(Parent);
                    if (!Parent.mParent.mTouchLev && this.mPosY > 160.0f) {
                        Parent.mParent.mVibration.Vibrate(5);
                        Parent.mClearPanel++;
                        this.mStep++;
                        return;
                    }
                    return;
                }
                return;
            case 3:
                IncAlowFlash();
                this.mSpdY += (24.0f - this.mSpdY) * 0.2f;
                this.mPosY += this.mSpdY;
                PlaySE(Parent);
                if (this.mPosY > 320.0f) {
                    Parent.mParent.mSound.Play(12);
                    this.mStep++;
                    return;
                }
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: package-private */
    public void Draw(GL10 gl, nnSprite Sprite) {
        if (this.mStep != 0 && this.mStep <= 3) {
            float _f = this.mPosY / 320.0f;
            if (_f > 1.0f) {
                _f = 1.0f;
            }
            gl.glPushMatrix();
            gl.glRotatef(AGL_TBL[this.mDir], 0.0f, 0.0f, 1.0f);
            gl.glTranslatef(0.0f, this.mPosY, 0.0f);
            gl.glTranslatef(0.0f, -160.0f, 0.0f);
            Sprite.Draw(gl, 23);
            gl.glPopMatrix();
            if (_f < 0.3f) {
                gl.glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
                nnGraph.DrawRectPrim(gl, -160.0f, -160.0f, 160.0f, 160.0f);
            } else {
                float _ff = (1.0f - _f) * 1.4285715f;
                gl.glColor4f(1.0f, 1.0f, 1.0f, _ff * _ff);
                nnGraph.DrawRectPrim(gl, -160.0f, -160.0f, 160.0f, 160.0f);
            }
            gl.glColor4f(0.8f, 0.7f, 0.6f, 1.0f);
            gl.glPushMatrix();
            gl.glRotatef(AGL_TBL[this.mDir], 0.0f, 0.0f, 1.0f);
            gl.glTranslatef(0.0f, this.mPosY, 0.0f);
            Sprite.Draw(gl, 2);
            if (this.mStep > 1) {
                Sprite.Draw(gl, 30);
                for (int i = 0; i < 3; i++) {
                    float _af = 1.0f - ((this.mArowFlash - (((float) i) / 4.0f)) * 2.0f);
                    if (_af >= 0.0f && _af <= 1.0f) {
                        gl.glColor4f(_af, _af, _af, 1.0f);
                        Sprite.Draw(gl, i + 32);
                    }
                }
            }
            float _fo = _f < 0.3f ? 1.0f : (1.0f - _f) * 1.4285715f;
            float _fi = _f > 0.3f ? 1.0f : 3.0f * _f;
            gl.glColor4f(1.0f, 1.0f, 1.0f, _fo * _fo);
            gl.glTranslatef(0.0f, -160.0f, 0.0f);
            gl.glScalef(1.0f, 4.0f * _fi * (0.9f + (0.1f * this.mGrowFlash)), 1.0f);
            Sprite.Draw(gl, 5);
            gl.glPopMatrix();
            gl.glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
        }
    }

    /* access modifiers changed from: package-private */
    public boolean ChkClear() {
        return this.mStep == 4;
    }
}
