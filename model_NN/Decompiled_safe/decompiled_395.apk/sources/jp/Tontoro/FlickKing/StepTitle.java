package jp.Tontoro.FlickKing;

import android.content.Intent;
import android.util.Log;
import com.openfeint.api.ui.Dashboard;
import javax.microedition.khronos.opengles.GL10;

public class StepTitle {
    GL10 mGL;
    GameMain mParent;

    public void Init(GameMain Parent, GL10 gl) {
        this.mParent = Parent;
        this.mGL = gl;
    }

    public void MoveLogo() {
        this.mParent.Step0Set(1);
    }

    public void MoveTitle() {
        this.mParent.mTontoro.Move(this.mParent);
        this.mParent.mButtonTitle.Move(this.mParent);
        switch (this.mParent.Step1()) {
            case 0:
                this.mParent.mTontoro.Init();
                this.mParent.mButtonTitle.SetAllMode(0);
                this.mParent.isFrameSkip = true;
                this.mParent.mBackKeySkip = true;
                this.mParent.mKeyTrg = 0;
                this.mParent.Step1Inc();
                return;
            case 1:
                if (this.mParent.mButtonTitle.GetMode(1) == 3) {
                    this.mParent.Step1Set(2);
                    return;
                } else if (this.mParent.mButtonTitle.GetMode(2) == 3) {
                    this.mParent.mStepRanking._Init();
                    this.mParent.Step1Set(3);
                    return;
                } else {
                    if (this.mParent.mButtonTitle.GetMode(3) == 3) {
                        this.mParent.mParent.startActivityForResult(new Intent(this.mParent.mContext, OptionActivity.class), 11);
                        Log.d("startActivityForResult", "startActivityForResult");
                        this.mParent.Step1Set(224);
                    }
                    if (this.mParent.mButtonTitle.GetMode(4) == 3) {
                        this.mParent.Step1Set(240);
                    }
                    if ((this.mParent.mKeyTrg & 16) != 0 && this.mParent.mButtonTitle.GetMode(4) == 0) {
                        this.mParent.mButtonTitle.SetMode(4, 2);
                    }
                    if (this.mParent.mTontoro.mBtnMaker.Chk(this.mParent)) {
                        this.mParent.mTontoro.mBtnMaker.ExecuteMarket();
                        this.mParent.mButtonTitle.SoundPlay();
                        this.mParent.mVibration.Vibrate(100);
                        return;
                    } else if (this.mParent.mTontoro.mBtnFeint.Chk(this.mParent)) {
                        this.mParent.mTontoro.mBtnFeint.Execute();
                        this.mParent.mButtonTitle.SoundPlay();
                        this.mParent.mVibration.Vibrate(100);
                        Dashboard.open();
                        return;
                    } else {
                        return;
                    }
                }
            case 2:
                this.mParent.Step0Set(3);
                return;
            case 3:
                this.mParent.Step0Set(2);
                return;
            case 224:
                Log.d("ModealWait", "Wait");
                if (this.mParent.mParent.GetActivityResult() == 11) {
                    this.mParent.ExecuteOption();
                    this.mParent.mButtonTitle.SetAllMode(0);
                    this.mParent.Step1Set(1);
                    return;
                }
                return;
            case 240:
                this.mParent.mParent.finish();
                return;
            default:
                return;
        }
    }

    public void DrawTitle() {
        this.mParent.DrawGate(this.mGL);
        this.mParent.mButtonTitle.Draw(this.mGL);
        this.mGL.glPushMatrix();
        this.mParent.mSprite.Draw(this.mGL, 22);
        this.mGL.glPopMatrix();
        this.mParent.mTontoro.DrawCopyright(0.0f, -240.0f);
        this.mParent.mTontoro.DrawMaker();
        this.mParent.mTontoro.DrawFeint();
    }

    public void MoveSelect() {
        this.mParent.mButtonSelect.Move(this.mParent);
        switch (this.mParent.Step1()) {
            case 0:
                this.mParent.mButtonSelect.SetAllMode(0);
                this.mParent.Step1Inc();
                return;
            case 1:
                for (int i = 0; i <= 2; i++) {
                    if (this.mParent.mButtonSelect.GetMode(i) == 3) {
                        this.mParent.mStepGame.Init(i);
                        this.mParent.Step1Inc();
                        return;
                    }
                }
                if ((this.mParent.mKeyTrg & 16) != 0) {
                    this.mParent.Step0Set(1);
                    return;
                }
                return;
            case 2:
                this.mParent.Step0Set(4);
                return;
            default:
                return;
        }
    }

    public void DrawSelect() {
        this.mParent.DrawGate(this.mGL);
        this.mParent.mButtonSelect.Draw(this.mGL);
    }
}
