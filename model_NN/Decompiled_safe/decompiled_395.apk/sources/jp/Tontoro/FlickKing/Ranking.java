package jp.Tontoro.FlickKing;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;
import com.openfeint.api.resource.Leaderboard;
import com.openfeint.api.resource.Score;
import javax.microedition.khronos.opengles.GL10;
import jp.Tontoro.Lib.nn;
import jp.Tontoro.Lib.nnFont;
import jp.Tontoro.Lib.nnSprite;

public class Ranking {
    private static final String[] FeintID = {"833976", "833986", "833996"};
    static final String[] GAMERANKING_STR = {"TOP:", "2ND:", "3RD:", "4TH:", "5TH:"};
    public static final int RANKING_NUM = 5;
    public static final int SCORE_MAX = 999999;
    float mFlashTimer = 0.0f;
    int mGameMode;
    int mNewRecordIndex = 0;
    int mNum;
    String[] mStr = new String[5];
    int[] mVal = new int[5];

    /* access modifiers changed from: package-private */
    public void Init(Context Con, int GameMode) {
        this.mGameMode = GameMode;
        this.mNewRecordIndex = 0;
        for (int i = 0; i < 5; i++) {
            this.mVal[i] = 999999;
            this.mStr[i] = "";
        }
        String _Key = "pref_rank_mode" + this.mGameMode + "_";
        this.mNum = PreferenceManager.getDefaultSharedPreferences(Con).getInt(String.valueOf(_Key) + "num", 0);
        Log.d("PreferenceManager", new StringBuilder().append(this.mNum).toString());
        if (this.mNum > 5) {
            this.mNum = 5;
        }
        for (int i2 = 0; i2 < this.mNum; i2++) {
            Log.d("PreferenceManager", "11111");
            this.mVal[i2] = PreferenceManager.getDefaultSharedPreferences(Con).getInt(String.valueOf(_Key) + "Val" + i2, 0);
            this.mStr[i2] = PreferenceManager.getDefaultSharedPreferences(Con).getString(String.valueOf(_Key) + "Str" + i2, "");
        }
    }

    /* access modifiers changed from: package-private */
    public void Move() {
        this.mFlashTimer += 0.01f;
        if (this.mFlashTimer > 1.0f) {
            this.mFlashTimer -= 1.0f;
        }
    }

    /* access modifiers changed from: package-private */
    public void EntryScore(Context Con, int NewScore) {
        new Score((long) (999999 - NewScore), String.format("%s (%s)", StepGame.GetScoreStr(NewScore), nn.GetDateStr())).submitTo(new Leaderboard(FeintID[this.mGameMode]), null);
        if (this.mNum < 5) {
            this.mNum++;
        }
        int i = 0;
        while (true) {
            if (i >= this.mNum) {
                break;
            } else if (NewScore < this.mVal[i]) {
                this.mNewRecordIndex = i + 1;
                if (this.mNum > 1) {
                    for (int j = this.mNum - 1; j > i; j--) {
                        this.mVal[j] = this.mVal[j - 1];
                        this.mStr[j] = this.mStr[j - 1];
                    }
                }
                this.mVal[i] = NewScore;
                this.mStr[i] = nn.GetDateStr();
            } else {
                i++;
            }
        }
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(Con);
        String _Key = "pref_rank_mode" + this.mGameMode + "_";
        for (int i2 = 0; i2 < this.mNum; i2++) {
            sp.edit().putInt(String.valueOf(_Key) + "Val" + i2, this.mVal[i2]).commit();
            sp.edit().putString(String.valueOf(_Key) + "Str" + i2, this.mStr[i2]).commit();
        }
        sp.edit().putInt(String.valueOf(_Key) + "num", this.mNum).commit();
    }

    /* access modifiers changed from: package-private */
    public void Draw(GL10 gl, nnFont mFont, nnSprite Sprite) {
        gl.glColor4f(1.0f, 0.6f, 0.1f, 1.0f);
        String _str = StepGame.GAMEMODE_PANELLABELTBL[this.mGameMode];
        mFont.DrawStr(_str, (-mFont.GetStrWidth(_str)) * 0.75f, 140.0f, 1.5f);
        gl.glPushMatrix();
        gl.glTranslatef(0.0f, 120.0f, 0.0f);
        for (int i = 0; i < this.mNum; i++) {
            gl.glTranslatef(0.0f, -28.0f, 0.0f);
            if (this.mNewRecordIndex - 1 == i) {
                float _f = (((float) Math.sin((double) (this.mFlashTimer * 6.2831855f))) * 0.5f) + 0.5f;
                gl.glColor4f(0.4f * _f, 0.1f * _f, 0.0f, 1.0f);
                gl.glPushMatrix();
                gl.glTranslatef(0.0f, -19.0f, 0.0f);
                gl.glScalef(16.0f, 0.65f, 1.0f);
                Sprite.Draw(gl, 19);
                gl.glPopMatrix();
            }
            gl.glColor4f(1.0f, 1.0f, 0.9f, 1.0f);
            String _str2 = GAMERANKING_STR[i];
            mFont.DrawStr(_str2, -64.0f - mFont.GetStrWidth(_str2), 0.0f, 1.0f);
            String _str3 = StepGame.GetScoreStr(this.mVal[i]);
            mFont.DrawStr(_str3, 140.0f - mFont.GetStrWidth(_str3), 0.0f, 1.0f);
            gl.glColor4f(0.7f, 0.7f, 0.65f, 1.0f);
            gl.glTranslatef(0.0f, -20.0f, 0.0f);
            String _str4 = this.mStr[i];
            mFont.DrawStr(_str4, 140.0f - (mFont.GetStrWidth(_str4) * 0.75f), 0.0f, 0.75f);
        }
        gl.glPopMatrix();
        gl.glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
    }
}
