package jp.Tontoro.FlickKing;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;

public class OptionActivity extends PreferenceActivity {
    public void onCreate(Bundle savedInstanceState) {
        String ver;
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preference);
        Preference preference = findPreference("pref_key_Version");
        Context mContext = getBaseContext();
        try {
            ver = mContext.getPackageManager().getPackageInfo(mContext.getPackageName(), 1).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            ver = "";
        }
        preference.setTitle(String.valueOf(getString(R.string.PrefVersionTitle)) + " " + ver);
    }
}
