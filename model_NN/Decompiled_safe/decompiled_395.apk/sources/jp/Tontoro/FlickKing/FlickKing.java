package jp.Tontoro.FlickKing;

import android.os.Bundle;
import android.widget.LinearLayout;
import jp.Tontoro.Lib.nnActivity;
import jp.Tontoro.Lib.nnFeint;

public class FlickKing extends nnActivity {
    private static final String AdMaker_Url = "http://images.ad-maker.info/apps/n29bpi6yejdv.html";
    private static final String AdMaker_siteId = "1201";
    private static final String AdMaker_zoneId = "3977";
    private static final String AdMob_Publisher_ID = "a14df8d0e447fa9";
    private static final String OF_ID = "300093";
    private static final String OF_Key = "5r9FdpuFKLaZ8Me9KW8IDQ";
    private static final String OF_Name = "Flick King";
    private static final String OF_Secret = "Wglm05Ppjlv1sMZHwSMFwgM4fBsAj8jymNUo2ooM";
    public static boolean isDebug = false;
    public static boolean isLite = false;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.main);
        nnFeint.Init(this, getBaseContext(), OF_Name, OF_Key, OF_Secret, OF_ID);
        this.mAdvertising.Start(this, R.id.linearLayout1, R.id.admakerview, AdMaker_siteId, AdMaker_zoneId, AdMaker_Url, AdMob_Publisher_ID, 7);
        this.mFramework = new GameMain(this, 320.0f, 480.0f, isDebug, isLite);
        ((LinearLayout) findViewById(R.id.linearLayout2)).addView(this.mFramework, new LinearLayout.LayoutParams(-1, -1));
    }
}
