package jp.Tontoro.FlickKing;

public final class R {

    public static final class attr {
    }

    public static final class color {
        public static final int of_transparent = 2131099648;
    }

    public static final class drawable {
        public static final int frame_layout_shape = 2130837504;
        public static final int gunship19 = 2130837505;
        public static final int icon = 2130837506;
        public static final int of_achievement_icon_frame = 2130837507;
        public static final int of_achievement_icon_locked = 2130837508;
        public static final int of_achievement_icon_unlocked = 2130837509;
        public static final int of_achievement_notification_bkg = 2130837510;
        public static final int of_achievement_notification_locked = 2130837511;
        public static final int of_feint_points_white = 2130837512;
        public static final int of_icon_dashboard_exit = 2130837513;
        public static final int of_icon_dashboard_home = 2130837514;
        public static final int of_icon_dashboard_settings = 2130837515;
        public static final int of_icon_highscore_notification = 2130837516;
        public static final int of_ll_logo = 2130837517;
        public static final int of_native_loader = 2130837518;
        public static final int of_native_loader_frame = 2130837519;
        public static final int of_native_loader_leaf = 2130837520;
        public static final int of_native_loader_progress = 2130837521;
        public static final int of_native_loader_progress_01 = 2130837522;
        public static final int of_native_loader_progress_02 = 2130837523;
        public static final int of_native_loader_progress_03 = 2130837524;
        public static final int of_native_loader_progress_04 = 2130837525;
        public static final int of_native_loader_progress_05 = 2130837526;
        public static final int of_native_loader_progress_06 = 2130837527;
        public static final int of_native_loader_progress_07 = 2130837528;
        public static final int of_native_loader_progress_08 = 2130837529;
        public static final int of_native_loader_progress_09 = 2130837530;
        public static final int of_native_loader_progress_10 = 2130837531;
        public static final int of_native_loader_progress_11 = 2130837532;
        public static final int of_native_loader_progress_12 = 2130837533;
        public static final int of_notification_bkg = 2130837534;
    }

    public static final class id {
        public static final int admakerview = 2131361793;
        public static final int exit_feint = 2131361813;
        public static final int frameLayout = 2131361804;
        public static final int home = 2131361811;
        public static final int linearLayout1 = 2131361792;
        public static final int linearLayout2 = 2131361794;
        public static final int nested_window_root = 2131361803;
        public static final int of_achievement_icon = 2131361796;
        public static final int of_achievement_icon_frame = 2131361797;
        public static final int of_achievement_notification = 2131361795;
        public static final int of_achievement_progress_icon = 2131361801;
        public static final int of_achievement_score = 2131361799;
        public static final int of_achievement_score_icon = 2131361800;
        public static final int of_achievement_text = 2131361798;
        public static final int of_icon = 2131361807;
        public static final int of_ll_logo_image = 2131361806;
        public static final int of_text = 2131361808;
        public static final int of_text1 = 2131361809;
        public static final int of_text2 = 2131361810;
        public static final int progress = 2131361802;
        public static final int settings = 2131361812;
        public static final int web_view = 2131361805;
    }

    public static final class layout {
        public static final int main = 2130903040;
        public static final int of_achievement_notification = 2130903041;
        public static final int of_native_loader = 2130903042;
        public static final int of_nested_window = 2130903043;
        public static final int of_simple_notification = 2130903044;
        public static final int of_two_line_notification = 2130903045;
    }

    public static final class menu {
        public static final int of_dashboard = 2131296256;
    }

    public static final class raw {
        public static final int button = 2131034112;
        public static final int flick = 2131034113;
        public static final int flick00 = 2131034114;
        public static final int flick01 = 2131034115;
        public static final int flick02 = 2131034116;
        public static final int flick03 = 2131034117;
        public static final int flick04 = 2131034118;
        public static final int gunship19 = 2131034119;
        public static final int se01 = 2131034120;
        public static final int se05 = 2131034121;
        public static final int se06 = 2131034122;
        public static final int se07 = 2131034123;
        public static final int se08 = 2131034124;
        public static final int se09 = 2131034125;
        public static final int se11 = 2131034126;
        public static final int se12 = 2131034127;
    }

    public static final class string {
        public static final int PrefEffectSummary = 2131165234;
        public static final int PrefEffectTitle = 2131165233;
        public static final int PrefSoundSummary = 2131165230;
        public static final int PrefSoundTitle = 2131165229;
        public static final int PrefVersionSummary = 2131165236;
        public static final int PrefVersionTitle = 2131165235;
        public static final int PrefVibrationSummary = 2131165232;
        public static final int PrefVibrationTitle = 2131165231;
        public static final int app_name = 2131165228;
        public static final int hello = 2131165227;
        public static final int of_achievement_load_null = 2131165192;
        public static final int of_achievement_unlock_null = 2131165191;
        public static final int of_achievement_unlocked = 2131165207;
        public static final int of_banned_dialog = 2131165220;
        public static final int of_bitmap_decode_error = 2131165209;
        public static final int of_cancel = 2131165203;
        public static final int of_cant_compress_blob = 2131165205;
        public static final int of_crash_report_query = 2131165217;
        public static final int of_device = 2131165198;
        public static final int of_error_parsing_error_message = 2131165211;
        public static final int of_exit_feint = 2131165222;
        public static final int of_file_not_found = 2131165210;
        public static final int of_home = 2131165219;
        public static final int of_id_cannot_be_null = 2131165186;
        public static final int of_io_exception_on_download = 2131165204;
        public static final int of_ioexception_reading_body = 2131165213;
        public static final int of_key_cannot_be_null = 2131165184;
        public static final int of_loading_feint = 2131165199;
        public static final int of_low_memory_profile_pic = 2131165194;
        public static final int of_malformed_request_error = 2131165226;
        public static final int of_name_cannot_be_null = 2131165187;
        public static final int of_no = 2131165200;
        public static final int of_no_blob = 2131165206;
        public static final int of_no_video = 2131165218;
        public static final int of_nodisk = 2131165196;
        public static final int of_now_logged_in_as_format = 2131165215;
        public static final int of_null_icon_url = 2131165190;
        public static final int of_offline_notification = 2131165223;
        public static final int of_offline_notification_line2 = 2131165224;
        public static final int of_ok = 2131165202;
        public static final int of_profile_pic_changed = 2131165216;
        public static final int of_profile_picture_download_failed = 2131165195;
        public static final int of_profile_url_null = 2131165193;
        public static final int of_score_submitted_notification = 2131165225;
        public static final int of_sdcard = 2131165197;
        public static final int of_secret_cannot_be_null = 2131165185;
        public static final int of_server_error_code_format = 2131165212;
        public static final int of_settings = 2131165221;
        public static final int of_switched_accounts = 2131165214;
        public static final int of_timeout = 2131165208;
        public static final int of_unexpected_response_format = 2131165188;
        public static final int of_unknown_server_error = 2131165189;
        public static final int of_yes = 2131165201;
    }

    public static final class style {
        public static final int OFLoading = 2131230720;
        public static final int OFNestedWindow = 2131230721;
    }

    public static final class xml {
        public static final int preference = 2130968576;
    }
}
