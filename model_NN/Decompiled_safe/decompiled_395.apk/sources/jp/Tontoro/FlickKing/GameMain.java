package jp.Tontoro.FlickKing;

import android.content.Context;
import android.opengl.GLU;
import android.preference.PreferenceManager;
import javax.microedition.khronos.opengles.GL10;
import jp.Tontoro.Lib.nnButton;
import jp.Tontoro.Lib.nnFont;
import jp.Tontoro.Lib.nnFramework;
import jp.Tontoro.Lib.nnSound;
import jp.Tontoro.Lib.nnSprite;
import jp.Tontoro.Lib.nnTontoro;
import jp.Tontoro.Lib.nnVibration;

public class GameMain extends nnFramework {
    private static final int[] SE_RES_TBL;
    private static final int[] SPRITE_TEX_TBL = {R.raw.flick00, R.raw.flick01, R.raw.flick02, R.raw.flick03, R.raw.flick04};
    public static final int STEP_GAME = 4;
    public static final int STEP_LOGO = 0;
    public static final int STEP_RANKING = 2;
    public static final int STEP_SELECT = 3;
    public static final int STEP_TITLE = 1;
    public nnFont mBtnFont;
    public nnButton mButtonRetry;
    public nnButton mButtonSelect;
    public nnButton mButtonTitle;
    public nnFont mFont;
    public float mGateFade;
    public float mGatePosY;
    public int mGateStep;
    public nnSound mSound;
    public nnSprite mSprite;
    public StepGame mStepGame = new StepGame();
    public StepRanking mStepRanking = new StepRanking();
    public StepTitle mStepTitle = new StepTitle();
    public nnTontoro mTontoro;
    public nnVibration mVibration;

    static {
        int[] iArr = new int[13];
        iArr[1] = R.raw.se01;
        iArr[5] = R.raw.se05;
        iArr[6] = R.raw.se06;
        iArr[7] = R.raw.se07;
        iArr[8] = R.raw.se08;
        iArr[9] = R.raw.se09;
        iArr[11] = R.raw.se11;
        iArr[12] = R.raw.se12;
        SE_RES_TBL = iArr;
    }

    public GameMain(Context context, float ClientW, float ClientH, boolean _isDebug, boolean _isLite) {
        super(context, ClientW, ClientH, _isDebug);
    }

    public void ExecuteOption() {
        this.mSound.SetEnable(PreferenceManager.getDefaultSharedPreferences(this.mContext).getBoolean("pref_key_checkbox_Sound", true));
        this.mVibration.SetEnable(PreferenceManager.getDefaultSharedPreferences(this.mContext).getBoolean("pref_key_checkbox_Vibration", true));
    }

    public void onRenderInit(GL10 gl) {
        this.mSprite = new nnSprite(this.mContext.getResources().openRawResource(R.raw.flick));
        this.mSprite.LoadTextures(this.mContext, gl, SPRITE_TEX_TBL);
        this.mSound = new nnSound();
        this.mSound.Init(this.mContext, SE_RES_TBL);
        this.mVibration = new nnVibration(this.mParent);
        this.mFont = new nnFont(this.mContext, gl, R.raw.gunship19, R.drawable.gunship19);
        this.mBtnFont = new nnFont(this.mContext, gl, R.raw.gunship19, R.drawable.gunship19);
        this.mTontoro = new nnTontoro(this.mContext, gl);
        this.mTontoro.mBtnMaker.SetPos(-130.0f, -208.0f);
        this.mTontoro.mBtnFeint.SetPos(130.0f, -208.0f);
        this.mButtonTitle = new nnButton(this.mContext, gl, R.raw.button, 128, 256, this.mBtnFont, this.mSound, 1, this.mVibration, 1);
        this.mButtonTitle.SetTextRGBA(0.0f, 0.0f, 0.0f, 0.75f);
        this.mButtonTitle.SetPanelMinMax(64.0f, 256.0f);
        this.mButtonTitle.SetPanelHeight(28.0f);
        this.mButtonTitle.SetTuchHeight(25.0f);
        this.mButtonTitle.Add(1, 0.0f, -20.0f, "START");
        this.mButtonTitle.Add(2, 0.0f, -70.0f, "RANKING");
        this.mButtonTitle.Add(3, 0.0f, -120.0f, "OPTION");
        this.mButtonTitle.Add(4, 0.0f, -180.0f, "QUIT");
        this.mButtonSelect = new nnButton(this.mContext, gl, R.raw.button, 128, 256, this.mBtnFont, this.mSound, 1, this.mVibration, 1);
        this.mButtonSelect.SetTextRGBA(0.0f, 0.0f, 0.0f, 0.75f);
        this.mButtonSelect.SetPanelMinMax(64.0f, 256.0f);
        this.mButtonSelect.SetPanelHeight(28.0f);
        this.mButtonSelect.SetTuchHeight(25.0f);
        this.mButtonSelect.Add(0, 0.0f, 80.0f, StepGame.GAMEMODE_PANELLABELTBL[0]);
        this.mButtonSelect.Add(1, 0.0f, 0.0f, StepGame.GAMEMODE_PANELLABELTBL[1]);
        this.mButtonSelect.Add(2, 0.0f, -80.0f, StepGame.GAMEMODE_PANELLABELTBL[2]);
        this.mButtonRetry = new nnButton(this.mContext, gl, R.raw.button, 128, 256, this.mBtnFont, this.mSound, 1, this.mVibration, 1);
        this.mButtonRetry.SetTextRGBA(0.0f, 0.0f, 0.0f, 0.75f);
        this.mButtonRetry.SetPanelMinMax(64.0f, 158.0f);
        this.mButtonRetry.SetPanelHeight(28.0f);
        this.mButtonRetry.SetTuchHeight(25.0f);
        this.mButtonRetry.Add(0, -80.0f, -200.0f, "EXIT");
        this.mButtonRetry.Add(1, 80.0f, -200.0f, "RETRY");
        this.mStepTitle.Init(this, gl);
        this.mStepGame.Init(this, gl);
        this.mStepRanking.Init(this, gl);
        ExecuteOption();
    }

    public void onRenderMove(GL10 gl) {
        while (true) {
            int _Step0 = Step0();
            int _Step1 = Step1();
            switch (Step0()) {
                case 0:
                    this.mStepTitle.MoveLogo();
                    break;
                case 1:
                    this.mStepTitle.MoveTitle();
                    break;
                case 2:
                    this.mStepRanking.Move();
                    break;
                case 3:
                    this.mStepTitle.MoveSelect();
                    break;
                case 4:
                    this.mStepGame.Move();
                    break;
            }
            this.mTouchTrg = false;
            if (_Step0 == Step0() && _Step1 == Step1()) {
                switch (this.mGateStep) {
                    case 0:
                    default:
                        return;
                    case 1:
                        this.mGateFade += 0.033333335f;
                        if (this.mGateFade >= 1.0f) {
                            this.mGateFade = 1.0f;
                            this.mGateStep = 0;
                        }
                        this.mGatePosY = this.mGateFade * 160.0f * this.mGateFade;
                        return;
                    case 2:
                        this.mGateFade -= 0.033333335f;
                        if (this.mGateFade <= 0.0f) {
                            this.mGateFade = 0.0f;
                            this.mGateStep = 0;
                        }
                        this.mGatePosY = this.mGateFade * 160.0f * this.mGateFade;
                        return;
                }
            }
        }
    }

    public float GetGateFade() {
        return this.mGateFade;
    }

    public boolean SyncGate() {
        return this.mGateStep == 0;
    }

    public void OpenGate() {
        this.mGateStep = 1;
    }

    public void CloseGate() {
        this.mGateStep = 2;
    }

    public void onRenderDraw(GL10 gl) {
        gl.glMatrixMode(5889);
        gl.glLoadIdentity();
        GLU.gluOrtho2D(gl, (-this.mClientW) * 0.5f, this.mClientW * 0.5f, (-this.mClientH) * 0.5f, this.mClientH * 0.5f);
        gl.glMatrixMode(5888);
        gl.glLoadIdentity();
        gl.glDisable(2929);
        gl.glDisable(2884);
        gl.glEnable(3042);
        gl.glBlendFunc(770, 771);
        gl.glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
        switch (Step0()) {
            case 0:
            default:
                return;
            case 1:
                this.mStepTitle.DrawTitle();
                return;
            case 2:
                this.mStepRanking.Draw();
                return;
            case 3:
                this.mStepTitle.DrawSelect();
                return;
            case 4:
                this.mStepGame.Draw();
                return;
        }
    }

    public void DrawGate(GL10 gl) {
        gl.glPushMatrix();
        gl.glTranslatef(0.0f, this.mGatePosY, 0.0f);
        this.mSprite.Draw(gl, 23);
        gl.glPopMatrix();
        gl.glPushMatrix();
        gl.glTranslatef(0.0f, -this.mGatePosY, 0.0f);
        this.mSprite.Draw(gl, 24);
        gl.glPopMatrix();
        gl.glColor4f((this.mGateFade * 0.6f) + 0.4f, (this.mGateFade * 0.6f) + 0.4f, (this.mGateFade * 0.6f) + 0.4f, 1.0f);
        gl.glPushMatrix();
        gl.glTranslatef(0.0f, this.mGatePosY, 0.0f);
        this.mSprite.Draw(gl, 20);
        gl.glPopMatrix();
        gl.glPushMatrix();
        gl.glTranslatef(0.0f, -this.mGatePosY, 0.0f);
        this.mSprite.Draw(gl, 21);
        gl.glPopMatrix();
    }
}
