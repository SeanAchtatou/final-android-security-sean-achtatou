package jp.ne.linkshare.android.tgad;

import android.os.AsyncTask;
import org.json.JSONObject;

public class DownloadJSONTask extends AsyncTask<String, Void, JSONObject> {
    private TGAdView adView;

    public DownloadJSONTask(TGAdView view) {
        this.adView = view;
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
    }

    /* access modifiers changed from: protected */
    public JSONObject doInBackground(String... urls) {
        return HttpClient.getJSON(urls[0]);
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(JSONObject result) {
        this.adView.settleSchedule(result);
    }
}
