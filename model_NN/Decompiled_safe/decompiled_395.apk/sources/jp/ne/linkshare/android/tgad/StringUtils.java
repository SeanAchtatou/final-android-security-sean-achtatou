package jp.ne.linkshare.android.tgad;

class StringUtils {
    StringUtils() {
    }

    public static String join(String[] array, String separator) {
        StringBuffer buf = new StringBuffer();
        for (String s : array) {
            if (buf.length() > 0) {
                buf.append(separator);
            }
            buf.append(s);
        }
        return buf.toString();
    }

    public static String uriEscape(String html) {
        StringBuilder buf = new StringBuilder(html.length());
        for (char chr : html.toCharArray()) {
            switch (chr) {
                case '#':
                    buf.append("%23");
                    break;
                case '%':
                    buf.append("%25");
                    break;
                case '\'':
                    buf.append("%27");
                    break;
                case '?':
                    buf.append("%3f");
                    break;
                default:
                    buf.append(chr);
                    break;
            }
        }
        return buf.toString();
    }
}
