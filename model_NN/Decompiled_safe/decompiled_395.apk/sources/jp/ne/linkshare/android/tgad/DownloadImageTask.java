package jp.ne.linkshare.android.tgad;

import android.graphics.Bitmap;
import android.os.AsyncTask;

class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
    private TGAdView adView;
    private String mText;

    public DownloadImageTask(TGAdView view, String text) {
        this.adView = view;
        this.mText = text;
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
    }

    /* access modifiers changed from: protected */
    public Bitmap doInBackground(String... urls) {
        Bitmap image;
        Bitmap image2 = HttpClient.getImage(urls[0]);
        TGAdManager manager = TGAdManager.getInstance();
        if (this.mText.equals("banner")) {
            image = manager.resizeBitmap(image2, ((float) manager.getDisplayWidth()) / ((float) image2.getWidth()));
        } else {
            int size = (int) (40.0f * manager.getScaledDensity());
            image = manager.resizeBitmap(image2, size, size);
        }
        ImageCache.setImage(urls[0], image);
        return image;
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(Bitmap result) {
        if (this.mText.equals("banner")) {
            this.adView.setImageContent(result);
        } else {
            this.adView.setTextContent(result, this.mText);
        }
    }
}
