package jp.ne.linkshare.android.tgad;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class TGAdManager {
    private static TGAdManager instance = new TGAdManager();
    private HashMap<String, String> envParams = new HashMap<>(9);
    private int mDisplayWidth;
    private float mScaledDensity;

    private TGAdManager() {
    }

    public static TGAdManager getInstance() {
        return instance;
    }

    public void init(Context context) {
        SharedPreferences pref = context.getSharedPreferences("TGAdPref", 0);
        if (!pref.getBoolean("installed", false)) {
            pref.edit().putBoolean("installed", true);
        }
    }

    public void setEnvParams(Context context) {
        try {
            this.envParams.put("developer_id", context.getPackageManager().getApplicationInfo(context.getPackageName(), 128).metaData.getString("TGAD_DEVELOPER_ID"));
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        Display display = ((WindowManager) context.getSystemService("window")).getDefaultDisplay();
        this.mDisplayWidth = Math.min(display.getWidth(), display.getHeight());
        DisplayMetrics metrics = new DisplayMetrics();
        display.getMetrics(metrics);
        this.mScaledDensity = metrics.scaledDensity;
        String androidId = Settings.Secure.getString(context.getContentResolver(), "android_id");
        if (androidId != null) {
            this.envParams.put("android_id", androidId);
        }
        this.envParams.put("brand", Build.BRAND);
        this.envParams.put("host", Build.HOST);
        this.envParams.put("id", Build.ID);
        this.envParams.put("model", Build.MODEL);
        this.envParams.put("product", Build.PRODUCT);
        this.envParams.put("user", Build.USER);
        this.envParams.put("lang", Locale.getDefault().toString());
    }

    public int getDisplayWidth() {
        return this.mDisplayWidth;
    }

    public float getScaledDensity() {
        return this.mScaledDensity;
    }

    /* Debug info: failed to restart local var, previous not found, register: 7 */
    public String buildUri(boolean isTest) {
        String scheme;
        String authority;
        String path;
        if (isTest) {
            scheme = "http";
            authority = "client.xtone.co.jp";
            path = "/linkshare/test.json";
        } else {
            scheme = "http";
            authority = "203.104.105.105";
            path = "/cgi-bin/adv_adr";
        }
        Uri.Builder uriBuilder = new Uri.Builder();
        uriBuilder.scheme(scheme);
        uriBuilder.authority(authority);
        uriBuilder.path(path);
        uriBuilder.appendQueryParameter("developer_id", this.envParams.get("developer_id"));
        uriBuilder.appendQueryParameter("lang", this.envParams.get("lang"));
        uriBuilder.appendQueryParameter("brand", this.envParams.get("brand"));
        uriBuilder.appendQueryParameter("host", this.envParams.get("host"));
        uriBuilder.appendQueryParameter("id", this.envParams.get("id"));
        uriBuilder.appendQueryParameter("model", this.envParams.get("model"));
        uriBuilder.appendQueryParameter("product", this.envParams.get("product"));
        uriBuilder.appendQueryParameter("user", this.envParams.get("user"));
        return uriBuilder.toString();
    }

    public String buildUri() {
        return buildUri(false);
    }

    public String buildUri(String baseUri) {
        String[] query = new String[this.envParams.size()];
        int i = 0;
        for (Map.Entry<String, String> entry : this.envParams.entrySet()) {
            int i2 = i + 1;
            try {
                query[i] = String.valueOf((String) entry.getKey()) + "=" + URLEncoder.encode((String) entry.getValue(), "utf-8");
                i = i2;
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        return String.valueOf(baseUri) + (baseUri.indexOf("?") == -1 ? "?" : "&") + StringUtils.join(query, "&");
    }

    public Bitmap resizeBitmap(Bitmap bitmap, int width, int height) {
        Bitmap result = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(result);
        BitmapDrawable drawable = new BitmapDrawable(bitmap);
        drawable.setBounds(0, 0, width, height);
        drawable.draw(canvas);
        return result;
    }

    public Bitmap resizeBitmap(Bitmap bitmap, float ratio) {
        return resizeBitmap(bitmap, (int) (((float) bitmap.getWidth()) * ratio), (int) (((float) bitmap.getHeight()) * ratio));
    }
}
