package jp.ne.linkshare.android.tgad;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.net.Uri;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.animation.AlphaAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.util.Timer;
import java.util.TimerTask;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class TGAdView extends LinearLayout {
    private JSONArray ads;
    private String background;
    private Context context;
    private Integer currentIndex = 0;
    Handler handler = new Handler();
    private Uri href;
    private ImageView imageView;
    private boolean isRotate = false;
    private LinearLayout linearLayout;
    private int switchSec;
    private TextView textView;
    private Timer timer;

    public TGAdView(Context aContext) {
        super(aContext);
        this.context = aContext;
        setGravity(1);
    }

    public TGAdView(Context aContext, String aBackgroundColor) {
        super(aContext);
        this.context = aContext;
        this.background = aBackgroundColor;
        setGravity(1);
    }

    public TGAdView(Context aContext, AttributeSet attrs) {
        super(aContext, attrs);
        this.background = attrs.getAttributeValue("http://schemas.android.com/apk/res/" + aContext.getPackageName(), "background");
        this.context = aContext;
        setGravity(1);
    }

    public void requestAd() {
        int backgroundColor;
        if (!this.isRotate) {
            TGAdManager manager = TGAdManager.getInstance();
            manager.setEnvParams(this.context);
            if (this.background == null) {
                backgroundColor = -16777216;
            } else if (this.background.equals("white")) {
                backgroundColor = -1;
            } else if (this.background.equals("blue")) {
                backgroundColor = Color.rgb(28, 68, 155);
            } else {
                backgroundColor = -16777216;
            }
            setBackgroundColor(backgroundColor);
            String uri = manager.buildUri();
            new DownloadJSONTask(this).execute(uri);
        }
    }

    public void settleSchedule(JSONObject jsonObject) {
        if (jsonObject != null) {
            try {
                this.ads = jsonObject.getJSONArray("ads");
                this.switchSec = jsonObject.getInt("switch");
                int adSize = this.ads.length();
                if (1 < adSize) {
                    this.timer = new Timer(true);
                    this.timer.schedule(new TimerTask() {
                        public void run() {
                            TGAdView.this.handler.post(new Runnable() {
                                public void run() {
                                    TGAdView.this.rotateBanner();
                                }
                            });
                        }
                    }, 0, (long) (this.switchSec * 1000));
                } else if (adSize == 1) {
                    rotateBanner();
                }
                this.isRotate = true;
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public void abortAd() {
        if (this.isRotate) {
            if (this.timer != null) {
                this.timer.cancel();
            }
            this.isRotate = false;
        }
    }

    /* access modifiers changed from: private */
    public void rotateBanner() {
        AlphaAnimation fadeOut = new AlphaAnimation(1.0f, 0.0f);
        fadeOut.setDuration(500);
        fadeOut.setAnimationListener(new TGAdAnimationListener());
        startAnimation(fadeOut);
        removeAllViews();
        setContent();
    }

    private void setContent() {
        TGAdManager manager = TGAdManager.getInstance();
        try {
            JSONObject jsonObj = this.ads.getJSONObject(this.currentIndex.intValue());
            this.href = Uri.parse(manager.buildUri(jsonObj.getString("href")));
            if (jsonObj.has("img")) {
                String imageUrl = manager.buildUri(jsonObj.getString("img"));
                Bitmap imageSource = ImageCache.getImage(imageUrl);
                if (imageSource == null) {
                    new DownloadImageTask(this, "banner").execute(imageUrl);
                } else {
                    setImageContent(imageSource);
                }
            } else {
                String iconUrl = manager.buildUri(jsonObj.getString("icon"));
                String text = jsonObj.getString("text");
                Bitmap imageSource2 = ImageCache.getImage(iconUrl);
                if (imageSource2 == null) {
                    new DownloadImageTask(this, text).execute(iconUrl);
                } else {
                    setTextContent(imageSource2, text);
                }
            }
            if (this.currentIndex.intValue() < this.ads.length() - 1) {
                this.currentIndex = Integer.valueOf(this.currentIndex.intValue() + 1);
            } else {
                this.currentIndex = 0;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e2) {
            Exception e3 = e2;
            e3.printStackTrace();
            Log.d("error", "stack", e3);
        }
        AlphaAnimation fadeIn = new AlphaAnimation(0.0f, 1.0f);
        fadeIn.setDuration(500);
        startAnimation(fadeIn);
    }

    public void setImageContent(Bitmap image) {
        if (this.imageView == null) {
            this.imageView = new ImageView(this.context);
        }
        this.imageView.setImageBitmap(image);
        addView(this.imageView, new LinearLayout.LayoutParams(-1, -2));
    }

    public void setTextContent(Bitmap icon, String text) {
        int textColor;
        if (this.linearLayout == null) {
            this.linearLayout = new LinearLayout(this.context);
            this.linearLayout.setGravity(16);
        }
        if (this.imageView == null) {
            this.imageView = new ImageView(this.context);
        }
        this.imageView.setImageBitmap(icon);
        float scaledDensity = TGAdManager.getInstance().getScaledDensity();
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams((int) (40.0f * scaledDensity), (int) (40.0f * scaledDensity));
        layoutParams.setMargins((int) (5.0f * scaledDensity), 0, (int) (8.0f * scaledDensity), 0);
        this.linearLayout.addView(this.imageView, layoutParams);
        if (this.textView == null) {
            this.textView = new TextView(this.context);
            if (this.background == null) {
                textColor = -1;
            } else if (this.background.equals("white")) {
                textColor = -16777216;
            } else if (this.background.equals("blue")) {
                textColor = -1;
            } else {
                textColor = -1;
            }
            this.textView.setTextColor(textColor);
            this.textView.setGravity(16);
        }
        this.textView.setText(text);
        int height = (int) (50.0f * scaledDensity);
        this.linearLayout.addView(this.textView, new LinearLayout.LayoutParams((int) (262.0f * scaledDensity), height));
        addView(this.linearLayout, new LinearLayout.LayoutParams((int) (320.0f * scaledDensity), height));
    }

    public void removeAllViews() {
        if (this.linearLayout != null && this.linearLayout.getChildCount() > 0) {
            this.linearLayout.removeAllViews();
        }
        super.removeAllViews();
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        abortAd();
    }

    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() != 0) {
            return false;
        }
        this.context.startActivity(new Intent("android.intent.action.VIEW", this.href));
        return true;
    }
}
