package jp.ne.linkshare.android.tgad;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import java.util.HashMap;
import org.json.JSONException;
import org.json.JSONObject;

class HttpClient {
    HttpClient() {
    }

    /* JADX WARN: Type inference failed for: r3v1, types: [java.net.URLConnection] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00d3 A[SYNTHETIC, Splitter:B:39:0x00d3] */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x00d8 A[Catch:{ IOException -> 0x00e5, Exception -> 0x00f0 }] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x00dd A[Catch:{ IOException -> 0x00e5, Exception -> 0x00f0 }] */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x0104 A[SYNTHETIC, Splitter:B:55:0x0104] */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x0109 A[Catch:{ IOException -> 0x0117, Exception -> 0x0123 }] */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x010e A[Catch:{ IOException -> 0x0117, Exception -> 0x0123 }] */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x0138 A[SYNTHETIC, Splitter:B:71:0x0138] */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x013d A[Catch:{ IOException -> 0x014b, Exception -> 0x0157 }] */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x0142 A[Catch:{ IOException -> 0x014b, Exception -> 0x0157 }] */
    /* JADX WARNING: Removed duplicated region for block: B:85:0x0169 A[SYNTHETIC, Splitter:B:85:0x0169] */
    /* JADX WARNING: Removed duplicated region for block: B:88:0x016e A[Catch:{ IOException -> 0x0177, Exception -> 0x0183 }] */
    /* JADX WARNING: Removed duplicated region for block: B:90:0x0173 A[Catch:{ IOException -> 0x0177, Exception -> 0x0183 }] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:68:0x0133=Splitter:B:68:0x0133, B:36:0x00ce=Splitter:B:36:0x00ce, B:52:0x00ff=Splitter:B:52:0x00ff} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static byte[] getByteArrayFromURL(java.lang.String r13, java.lang.String r14, java.util.HashMap<java.lang.String, java.lang.String> r15) {
        /*
            r1 = 1024(0x400, float:1.435E-42)
            byte[] r1 = new byte[r1]
            r8 = 0
            byte[] r8 = (byte[]) r8
            r2 = 0
            r4 = 0
            r6 = 0
            java.net.URL r3 = new java.net.URL     // Catch:{ ProtocolException -> 0x01c6, IOException -> 0x00fb, Exception -> 0x012f, all -> 0x0163 }
            r3.<init>(r13)     // Catch:{ ProtocolException -> 0x01c6, IOException -> 0x00fb, Exception -> 0x012f, all -> 0x0163 }
            java.net.URLConnection r3 = r3.openConnection()     // Catch:{ ProtocolException -> 0x01c6, IOException -> 0x00fb, Exception -> 0x012f, all -> 0x0163 }
            r0 = r3
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ ProtocolException -> 0x01c6, IOException -> 0x00fb, Exception -> 0x012f, all -> 0x0163 }
            r2 = r0
            r13 = 5000(0x1388, float:7.006E-42)
            r2.setConnectTimeout(r13)     // Catch:{ ProtocolException -> 0x01c6, IOException -> 0x00fb, Exception -> 0x012f, all -> 0x0163 }
            java.lang.String r13 = "POST"
            boolean r13 = r14.equals(r13)     // Catch:{ ProtocolException -> 0x01c6, IOException -> 0x00fb, Exception -> 0x012f, all -> 0x0163 }
            if (r13 == 0) goto L_0x002b
            r2.setRequestMethod(r14)     // Catch:{ ProtocolException -> 0x01c6, IOException -> 0x00fb, Exception -> 0x012f, all -> 0x0163 }
            r13 = 1
            r2.setDoOutput(r13)     // Catch:{ ProtocolException -> 0x01c6, IOException -> 0x00fb, Exception -> 0x012f, all -> 0x0163 }
        L_0x002b:
            r2.connect()     // Catch:{ ProtocolException -> 0x01c6, IOException -> 0x00fb, Exception -> 0x012f, all -> 0x0163 }
            java.lang.String r13 = "POST"
            boolean r13 = r14.equals(r13)     // Catch:{ ProtocolException -> 0x01c6, IOException -> 0x00fb, Exception -> 0x012f, all -> 0x0163 }
            if (r13 == 0) goto L_0x0069
            java.io.OutputStreamWriter r5 = new java.io.OutputStreamWriter     // Catch:{ ProtocolException -> 0x01c6, IOException -> 0x00fb, Exception -> 0x012f, all -> 0x0163 }
            java.io.OutputStream r13 = r2.getOutputStream()     // Catch:{ ProtocolException -> 0x01c6, IOException -> 0x00fb, Exception -> 0x012f, all -> 0x0163 }
            r5.<init>(r13)     // Catch:{ ProtocolException -> 0x01c6, IOException -> 0x00fb, Exception -> 0x012f, all -> 0x0163 }
            java.io.BufferedWriter r13 = new java.io.BufferedWriter     // Catch:{ ProtocolException -> 0x01c6, IOException -> 0x00fb, Exception -> 0x012f, all -> 0x0163 }
            r13.<init>(r5)     // Catch:{ ProtocolException -> 0x01c6, IOException -> 0x00fb, Exception -> 0x012f, all -> 0x0163 }
            int r14 = r15.size()     // Catch:{ ProtocolException -> 0x01c6, IOException -> 0x00fb, Exception -> 0x012f, all -> 0x0163 }
            java.lang.String[] r7 = new java.lang.String[r14]     // Catch:{ ProtocolException -> 0x01c6, IOException -> 0x00fb, Exception -> 0x012f, all -> 0x0163 }
            r14 = 0
            java.util.Set r15 = r15.entrySet()     // Catch:{ ProtocolException -> 0x01c6, IOException -> 0x00fb, Exception -> 0x012f, all -> 0x0163 }
            java.util.Iterator r10 = r15.iterator()     // Catch:{ ProtocolException -> 0x01c6, IOException -> 0x00fb, Exception -> 0x012f, all -> 0x0163 }
            r3 = r14
        L_0x0054:
            boolean r14 = r10.hasNext()     // Catch:{ ProtocolException -> 0x01c6, IOException -> 0x00fb, Exception -> 0x012f, all -> 0x0163 }
            if (r14 != 0) goto L_0x0090
            java.lang.String r14 = "\n"
            java.lang.String r14 = jp.ne.linkshare.android.tgad.StringUtils.join(r7, r14)     // Catch:{ ProtocolException -> 0x01c6, IOException -> 0x00fb, Exception -> 0x012f, all -> 0x0163 }
            r13.write(r14)     // Catch:{ ProtocolException -> 0x01c6, IOException -> 0x00fb, Exception -> 0x012f, all -> 0x0163 }
            r13.close()     // Catch:{ ProtocolException -> 0x01c6, IOException -> 0x00fb, Exception -> 0x012f, all -> 0x0163 }
            r5.close()     // Catch:{ ProtocolException -> 0x01c6, IOException -> 0x00fb, Exception -> 0x012f, all -> 0x0163 }
        L_0x0069:
            java.io.InputStream r14 = r2.getInputStream()     // Catch:{ ProtocolException -> 0x01c6, IOException -> 0x00fb, Exception -> 0x012f, all -> 0x0163 }
            java.io.ByteArrayOutputStream r15 = new java.io.ByteArrayOutputStream     // Catch:{ ProtocolException -> 0x01cc, IOException -> 0x01ba, Exception -> 0x01af, all -> 0x01a3 }
            r15.<init>()     // Catch:{ ProtocolException -> 0x01cc, IOException -> 0x01ba, Exception -> 0x01af, all -> 0x01a3 }
            r13 = 0
        L_0x0073:
            int r13 = r14.read(r1)     // Catch:{ ProtocolException -> 0x00ca, IOException -> 0x01c0, Exception -> 0x01b4, all -> 0x01a8 }
            r3 = -1
            if (r13 != r3) goto L_0x00c5
            byte[] r1 = r15.toByteArray()     // Catch:{ ProtocolException -> 0x00ca, IOException -> 0x01c0, Exception -> 0x01b4, all -> 0x01a8 }
            if (r2 == 0) goto L_0x0083
            r2.disconnect()     // Catch:{ IOException -> 0x018f, Exception -> 0x0199 }
        L_0x0083:
            if (r14 == 0) goto L_0x0088
            r14.close()     // Catch:{ IOException -> 0x018f, Exception -> 0x0199 }
        L_0x0088:
            if (r15 == 0) goto L_0x01d2
            r15.close()     // Catch:{ IOException -> 0x018f, Exception -> 0x0199 }
            r13 = r2
        L_0x008e:
            r2 = r1
        L_0x008f:
            return r2
        L_0x0090:
            java.lang.Object r14 = r10.next()     // Catch:{ ProtocolException -> 0x01c6, IOException -> 0x00fb, Exception -> 0x012f, all -> 0x0163 }
            java.util.Map$Entry r14 = (java.util.Map.Entry) r14     // Catch:{ ProtocolException -> 0x01c6, IOException -> 0x00fb, Exception -> 0x012f, all -> 0x0163 }
            int r15 = r3 + 1
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ ProtocolException -> 0x01c6, IOException -> 0x00fb, Exception -> 0x012f, all -> 0x0163 }
            java.lang.Object r9 = r14.getKey()     // Catch:{ ProtocolException -> 0x01c6, IOException -> 0x00fb, Exception -> 0x012f, all -> 0x0163 }
            java.lang.String r9 = (java.lang.String) r9     // Catch:{ ProtocolException -> 0x01c6, IOException -> 0x00fb, Exception -> 0x012f, all -> 0x0163 }
            java.lang.String r9 = java.lang.String.valueOf(r9)     // Catch:{ ProtocolException -> 0x01c6, IOException -> 0x00fb, Exception -> 0x012f, all -> 0x0163 }
            r11.<init>(r9)     // Catch:{ ProtocolException -> 0x01c6, IOException -> 0x00fb, Exception -> 0x012f, all -> 0x0163 }
            java.lang.String r9 = "="
            java.lang.StringBuilder r9 = r11.append(r9)     // Catch:{ ProtocolException -> 0x01c6, IOException -> 0x00fb, Exception -> 0x012f, all -> 0x0163 }
            java.lang.Object r14 = r14.getValue()     // Catch:{ ProtocolException -> 0x01c6, IOException -> 0x00fb, Exception -> 0x012f, all -> 0x0163 }
            java.lang.String r14 = (java.lang.String) r14     // Catch:{ ProtocolException -> 0x01c6, IOException -> 0x00fb, Exception -> 0x012f, all -> 0x0163 }
            java.lang.String r11 = "utf-8"
            java.lang.String r14 = java.net.URLEncoder.encode(r14, r11)     // Catch:{ ProtocolException -> 0x01c6, IOException -> 0x00fb, Exception -> 0x012f, all -> 0x0163 }
            java.lang.StringBuilder r14 = r9.append(r14)     // Catch:{ ProtocolException -> 0x01c6, IOException -> 0x00fb, Exception -> 0x012f, all -> 0x0163 }
            java.lang.String r14 = r14.toString()     // Catch:{ ProtocolException -> 0x01c6, IOException -> 0x00fb, Exception -> 0x012f, all -> 0x0163 }
            r7[r3] = r14     // Catch:{ ProtocolException -> 0x01c6, IOException -> 0x00fb, Exception -> 0x012f, all -> 0x0163 }
            r3 = r15
            goto L_0x0054
        L_0x00c5:
            r3 = 0
            r15.write(r1, r3, r13)     // Catch:{ ProtocolException -> 0x00ca, IOException -> 0x01c0, Exception -> 0x01b4, all -> 0x01a8 }
            goto L_0x0073
        L_0x00ca:
            r13 = move-exception
            r1 = r15
            r15 = r14
            r14 = r2
        L_0x00ce:
            r13.printStackTrace()     // Catch:{ all -> 0x01ad }
            if (r14 == 0) goto L_0x00d6
            r14.disconnect()     // Catch:{ IOException -> 0x00e5, Exception -> 0x00f0 }
        L_0x00d6:
            if (r15 == 0) goto L_0x00db
            r15.close()     // Catch:{ IOException -> 0x00e5, Exception -> 0x00f0 }
        L_0x00db:
            if (r1 == 0) goto L_0x01d5
            r1.close()     // Catch:{ IOException -> 0x00e5, Exception -> 0x00f0 }
            r13 = r14
            r14 = r15
            r15 = r1
            r1 = r8
            goto L_0x008e
        L_0x00e5:
            r13 = move-exception
            r13.printStackTrace()
            r13 = 0
            r2 = r13
            r13 = r14
            r14 = r15
            r15 = r1
            r1 = r8
            goto L_0x008f
        L_0x00f0:
            r13 = move-exception
            r13.printStackTrace()
            r13 = 0
            r2 = r13
            r13 = r14
            r14 = r15
            r15 = r1
            r1 = r8
            goto L_0x008f
        L_0x00fb:
            r13 = move-exception
            r1 = r6
            r15 = r4
            r14 = r2
        L_0x00ff:
            r13.printStackTrace()     // Catch:{ all -> 0x01ad }
            if (r14 == 0) goto L_0x0107
            r14.disconnect()     // Catch:{ IOException -> 0x0117, Exception -> 0x0123 }
        L_0x0107:
            if (r15 == 0) goto L_0x010c
            r15.close()     // Catch:{ IOException -> 0x0117, Exception -> 0x0123 }
        L_0x010c:
            if (r1 == 0) goto L_0x01d5
            r1.close()     // Catch:{ IOException -> 0x0117, Exception -> 0x0123 }
            r13 = r14
            r14 = r15
            r15 = r1
            r1 = r8
            goto L_0x008e
        L_0x0117:
            r13 = move-exception
            r13.printStackTrace()
            r13 = 0
            r2 = r13
            r13 = r14
            r14 = r15
            r15 = r1
            r1 = r8
            goto L_0x008f
        L_0x0123:
            r13 = move-exception
            r13.printStackTrace()
            r13 = 0
            r2 = r13
            r13 = r14
            r14 = r15
            r15 = r1
            r1 = r8
            goto L_0x008f
        L_0x012f:
            r13 = move-exception
            r1 = r6
            r15 = r4
            r14 = r2
        L_0x0133:
            r13.printStackTrace()     // Catch:{ all -> 0x01ad }
            if (r14 == 0) goto L_0x013b
            r14.disconnect()     // Catch:{ IOException -> 0x014b, Exception -> 0x0157 }
        L_0x013b:
            if (r15 == 0) goto L_0x0140
            r15.close()     // Catch:{ IOException -> 0x014b, Exception -> 0x0157 }
        L_0x0140:
            if (r1 == 0) goto L_0x01d5
            r1.close()     // Catch:{ IOException -> 0x014b, Exception -> 0x0157 }
            r13 = r14
            r14 = r15
            r15 = r1
            r1 = r8
            goto L_0x008e
        L_0x014b:
            r13 = move-exception
            r13.printStackTrace()
            r13 = 0
            r2 = r13
            r13 = r14
            r14 = r15
            r15 = r1
            r1 = r8
            goto L_0x008f
        L_0x0157:
            r13 = move-exception
            r13.printStackTrace()
            r13 = 0
            r2 = r13
            r13 = r14
            r14 = r15
            r15 = r1
            r1 = r8
            goto L_0x008f
        L_0x0163:
            r13 = move-exception
            r1 = r6
            r15 = r4
            r14 = r2
        L_0x0167:
            if (r14 == 0) goto L_0x016c
            r14.disconnect()     // Catch:{ IOException -> 0x0177, Exception -> 0x0183 }
        L_0x016c:
            if (r15 == 0) goto L_0x0171
            r15.close()     // Catch:{ IOException -> 0x0177, Exception -> 0x0183 }
        L_0x0171:
            if (r1 == 0) goto L_0x0176
            r1.close()     // Catch:{ IOException -> 0x0177, Exception -> 0x0183 }
        L_0x0176:
            throw r13
        L_0x0177:
            r13 = move-exception
            r13.printStackTrace()
            r13 = 0
            r2 = r13
            r13 = r14
            r14 = r15
            r15 = r1
            r1 = r8
            goto L_0x008f
        L_0x0183:
            r13 = move-exception
            r13.printStackTrace()
            r13 = 0
            r2 = r13
            r13 = r14
            r14 = r15
            r15 = r1
            r1 = r8
            goto L_0x008f
        L_0x018f:
            r13 = move-exception
            r13.printStackTrace()
            r13 = 0
            r12 = r2
            r2 = r13
            r13 = r12
            goto L_0x008f
        L_0x0199:
            r13 = move-exception
            r13.printStackTrace()
            r13 = 0
            r12 = r2
            r2 = r13
            r13 = r12
            goto L_0x008f
        L_0x01a3:
            r13 = move-exception
            r1 = r6
            r15 = r14
            r14 = r2
            goto L_0x0167
        L_0x01a8:
            r13 = move-exception
            r1 = r15
            r15 = r14
            r14 = r2
            goto L_0x0167
        L_0x01ad:
            r13 = move-exception
            goto L_0x0167
        L_0x01af:
            r13 = move-exception
            r1 = r6
            r15 = r14
            r14 = r2
            goto L_0x0133
        L_0x01b4:
            r13 = move-exception
            r1 = r15
            r15 = r14
            r14 = r2
            goto L_0x0133
        L_0x01ba:
            r13 = move-exception
            r1 = r6
            r15 = r14
            r14 = r2
            goto L_0x00ff
        L_0x01c0:
            r13 = move-exception
            r1 = r15
            r15 = r14
            r14 = r2
            goto L_0x00ff
        L_0x01c6:
            r13 = move-exception
            r1 = r6
            r15 = r4
            r14 = r2
            goto L_0x00ce
        L_0x01cc:
            r13 = move-exception
            r1 = r6
            r15 = r14
            r14 = r2
            goto L_0x00ce
        L_0x01d2:
            r13 = r2
            goto L_0x008e
        L_0x01d5:
            r13 = r14
            r14 = r15
            r15 = r1
            r1 = r8
            goto L_0x008e
        */
        throw new UnsupportedOperationException("Method not decompiled: jp.ne.linkshare.android.tgad.HttpClient.getByteArrayFromURL(java.lang.String, java.lang.String, java.util.HashMap):byte[]");
    }

    public static byte[] getByteArrayFromURL(String urlString) {
        return getByteArrayFromURL(urlString, "GET", null);
    }

    public static JSONObject getJSON(String urlString, String method, HashMap<String, String> postParams) {
        byte[] byteArray = getByteArrayFromURL(urlString, method, postParams);
        if (byteArray == null) {
            return null;
        }
        try {
            return new JSONObject(new String(byteArray));
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static JSONObject getJSON(String urlString) {
        return getJSON(urlString, "GET", null);
    }

    public static Bitmap getImage(String urlString, String method, HashMap<String, String> postParams) {
        byte[] byteArray = getByteArrayFromURL(urlString, method, postParams);
        if (byteArray == null) {
            return null;
        }
        return BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
    }

    public static Bitmap getImage(String urlString) {
        return getImage(urlString, "GET", null);
    }
}
