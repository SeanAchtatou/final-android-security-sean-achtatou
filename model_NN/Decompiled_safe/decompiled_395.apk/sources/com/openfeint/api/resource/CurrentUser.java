package com.openfeint.api.resource;

import com.openfeint.internal.APICallback;
import com.openfeint.internal.request.JSONRequest;
import com.openfeint.internal.request.OrderedArgList;
import com.openfeint.internal.resource.Resource;
import com.openfeint.internal.resource.ResourceClass;

public class CurrentUser extends User {

    public static abstract class BefriendCB extends APICallback {
        public abstract void onSuccess();
    }

    public void befriend(User userToBefriend, final BefriendCB cb) {
        OrderedArgList args = new OrderedArgList();
        args.put("friend_id", userToBefriend.resourceID());
        new JSONRequest(args) {
            public boolean wantsLogin() {
                return true;
            }

            public String method() {
                return "POST";
            }

            public String path() {
                return "/xp/friend_requests";
            }

            public void onSuccess(Object responseBody) {
                if (cb != null) {
                    cb.onSuccess();
                }
            }

            public void onFailure(String exceptionMessage) {
                super.onFailure(exceptionMessage);
                if (cb != null) {
                    cb.onFailure(exceptionMessage);
                }
            }
        }.launch();
    }

    public static ResourceClass getResourceClass() {
        return new ResourceClass(CurrentUser.class, "current_user") {
            public Resource factory() {
                return new CurrentUser();
            }
        };
    }
}
