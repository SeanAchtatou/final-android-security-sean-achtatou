package com.openfeint.api.resource;

import com.openfeint.internal.APICallback;
import com.openfeint.internal.request.JSONRequest;
import com.openfeint.internal.resource.DateResourceProperty;
import com.openfeint.internal.resource.LongResourceProperty;
import com.openfeint.internal.resource.Resource;
import com.openfeint.internal.resource.ResourceClass;
import java.util.Date;

public class ServerTimestamp extends Resource {
    public long secondsSinceEpoch;
    public Date timestamp;

    public static abstract class GetCB extends APICallback {
        public abstract void onSuccess(ServerTimestamp serverTimestamp);
    }

    public static void get(final GetCB cb) {
        new JSONRequest() {
            public String method() {
                return "GET";
            }

            public String path() {
                return "/xp/server_timestamp";
            }

            public void onSuccess(Object responseBody) {
                if (GetCB.this != null) {
                    GetCB.this.onSuccess((ServerTimestamp) responseBody);
                }
            }

            public void onFailure(String exceptionMessage) {
                super.onFailure(exceptionMessage);
                if (GetCB.this != null) {
                    GetCB.this.onFailure(exceptionMessage);
                }
            }
        }.launch();
    }

    public static ResourceClass getResourceClass() {
        ResourceClass klass = new ResourceClass(ServerTimestamp.class, "server_timestamp") {
            public Resource factory() {
                return new ServerTimestamp();
            }
        };
        klass.mProperties.put("timestamp", new DateResourceProperty() {
            public Date get(Resource obj) {
                return ((ServerTimestamp) obj).timestamp;
            }

            public void set(Resource obj, Date val) {
                ((ServerTimestamp) obj).timestamp = val;
            }
        });
        klass.mProperties.put("seconds_since_epoch", new LongResourceProperty() {
            public long get(Resource obj) {
                return ((ServerTimestamp) obj).secondsSinceEpoch;
            }

            public void set(Resource obj, long val) {
                ((ServerTimestamp) obj).secondsSinceEpoch = val;
            }
        });
        return klass;
    }
}
