package com.openfeint.internal.notifications;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.openfeint.api.Notification;
import com.openfeint.internal.OpenFeintInternal;
import com.openfeint.internal.request.BitmapRequest;
import java.util.HashMap;
import java.util.Map;
import jp.Tontoro.FlickKing.R;

public class TwoLineNotification extends NotificationBase {
    protected TwoLineNotification(String text, String extra, String imageName, Notification.Category cat, Notification.Type type, Map<String, Object> userData) {
        super(text, imageName, cat, type, userData);
    }

    /* access modifiers changed from: protected */
    public boolean createView() {
        this.displayView = ((LayoutInflater) OpenFeintInternal.getInstance().getContext().getSystemService("layout_inflater")).inflate((int) R.layout.of_two_line_notification, (ViewGroup) null);
        ((TextView) this.displayView.findViewById(R.id.of_text1)).setText(getText());
        ((TextView) this.displayView.findViewById(R.id.of_text2)).setText((String) getUserData().get("extra"));
        final ImageView icon = (ImageView) this.displayView.findViewById(R.id.of_icon);
        if (this.imageName != null) {
            Drawable image = getResourceDrawable(this.imageName);
            if (image == null) {
                new BitmapRequest() {
                    public String path() {
                        return TwoLineNotification.this.imageName;
                    }

                    public void onSuccess(Bitmap responseBody) {
                        icon.setImageDrawable(new BitmapDrawable(responseBody));
                        TwoLineNotification.this.showToast();
                    }

                    public void onFailure(String exceptionMessage) {
                        OpenFeintInternal.log("NotificationImage", "Failed to load image " + TwoLineNotification.this.imageName + ":" + exceptionMessage);
                        icon.setVisibility(4);
                        TwoLineNotification.this.showToast();
                    }
                }.launch();
                return false;
            }
            icon.setImageDrawable(image);
        } else {
            icon.setVisibility(4);
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void drawView(Canvas canvas) {
        this.displayView.draw(canvas);
    }

    public static void show(String text, String extra, Notification.Category c, Notification.Type t) {
        show(text, extra, null, c, t);
    }

    public static void show(String text, String extra, String imageName, Notification.Category c, Notification.Type t) {
        Map<String, Object> userData = new HashMap<>();
        userData.put("extra", extra);
        new TwoLineNotification(text, extra, imageName, c, t, userData).checkDelegateAndView();
    }
}
