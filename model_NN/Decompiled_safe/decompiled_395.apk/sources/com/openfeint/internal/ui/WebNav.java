package com.openfeint.internal.ui;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.KeyEvent;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.Toast;
import com.openfeint.api.OpenFeintDelegate;
import com.openfeint.api.resource.Score;
import com.openfeint.api.resource.User;
import com.openfeint.api.ui.Dashboard;
import com.openfeint.internal.ImagePicker;
import com.openfeint.internal.JsonResourceParser;
import com.openfeint.internal.OpenFeintInternal;
import com.openfeint.internal.Util;
import com.openfeint.internal.request.IRawRequestDelegate;
import com.openfeint.internal.resource.ScoreBlobDelegate;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import jp.Tontoro.FlickKing.R;
import org.codehaus.jackson.JsonFactory;
import org.json.JSONArray;
import org.json.JSONObject;

public class WebNav extends NestedWindow {
    protected static final int REQUEST_CODE_NATIVE_BROWSER = 25565;
    protected static final String TAG = "WebUI";
    ActionHandler mActionHandler;
    boolean mIsFrameworkLoaded = false;
    boolean mIsPageLoaded = false;
    Dialog mLaunchLoadingView;
    /* access modifiers changed from: private */
    public Map<String, String> mNativeBrowserParameters = null;
    protected ArrayList<String> mPreloadConsoleOutput = new ArrayList<>();
    private boolean mShouldRefreshOnResume = true;
    private WebNavClient mWebViewClient;
    protected int pageStackCount;

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle outState) {
        OpenFeintInternal.saveInstanceState(outState);
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Bundle inState) {
        OpenFeintInternal.restoreInstanceState(inState);
    }

    public ActionHandler getActionHandler() {
        return this.mActionHandler;
    }

    public Dialog getLaunchLoadingView() {
        return this.mLaunchLoadingView;
    }

    /* access modifiers changed from: protected */
    public void setFrameworkLoaded(boolean value) {
        this.mIsFrameworkLoaded = value;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        OpenFeintInternal.log(TAG, "--- WebUI Bootup ---");
        this.pageStackCount = 0;
        this.mWebView.getSettings().setJavaScriptEnabled(true);
        this.mWebView.getSettings().setPluginsEnabled(false);
        this.mWebView.setScrollBarStyle(33554432);
        this.mWebView.getSettings().setCacheMode(2);
        this.mLaunchLoadingView = new Dialog(this, R.style.OFLoading);
        this.mLaunchLoadingView.setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface dialog) {
                WebNav.this.finish();
            }
        });
        this.mLaunchLoadingView.setCancelable(true);
        this.mLaunchLoadingView.setContentView((int) R.layout.of_native_loader);
        ProgressBar progress = (ProgressBar) this.mLaunchLoadingView.findViewById(R.id.progress);
        progress.setIndeterminate(true);
        progress.setIndeterminateDrawable(OpenFeintInternal.getInstance().getContext().getResources().getDrawable(R.drawable.of_native_loader_progress));
        this.mActionHandler = createActionHandler(this);
        this.mWebViewClient = createWebNavClient(this.mActionHandler);
        this.mWebView.setWebViewClient(this.mWebViewClient);
        this.mWebView.setWebChromeClient(new WebNavChromeClient(this, null));
        this.mWebView.addJavascriptInterface(new Object() {
            public void action(final String actionUri) {
                WebNav.this.runOnUiThread(new Runnable() {
                    public void run() {
                        WebNav.this.getActionHandler().dispatch(Uri.parse(actionUri));
                    }
                });
            }

            public void frameworkLoaded() {
                WebNav.this.setFrameworkLoaded(true);
            }
        }, "NativeInterface");
        String path = initialContentPath();
        if (path.contains("?")) {
            path = path.split("\\?")[0];
        }
        if (!path.endsWith(".json")) {
            path = String.valueOf(path) + ".json";
        }
        WebViewCache.prioritize(path);
        load(false);
        this.mLaunchLoadingView.show();
    }

    /* access modifiers changed from: protected */
    public String rootPage() {
        return "index.html";
    }

    /* access modifiers changed from: protected */
    public void load(final boolean reload) {
        this.mIsPageLoaded = false;
        WebViewCache.trackPath(rootPage(), new WebViewCacheCallback() {
            public void pathLoaded(String itemPath) {
                if (WebNav.this.mWebView != null) {
                    String url = WebViewCache.getItemUri(itemPath);
                    OpenFeintInternal.log(WebNav.TAG, "Loading URL: " + url);
                    if (reload) {
                        WebNav.this.mWebView.reload();
                    } else {
                        WebNav.this.mWebView.loadUrl(url);
                    }
                }
            }

            public void failLoaded() {
                WebNav.this.closeForDiskError();
            }
        });
    }

    /* access modifiers changed from: private */
    public void closeForDiskError() {
        runOnUiThread(new Runnable() {
            public void run() {
                String place;
                WebNav.this.dismissDialog();
                if (Util.sdcardReady(WebNav.this)) {
                    place = OpenFeintInternal.getRString(R.string.of_sdcard);
                } else {
                    place = OpenFeintInternal.getRString(R.string.of_device);
                }
                new AlertDialog.Builder(WebNav.this).setMessage(String.format(OpenFeintInternal.getRString(R.string.of_nodisk), place)).setPositiveButton(OpenFeintInternal.getRString(R.string.of_no), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        WebNav.this.finish();
                    }
                }).show();
            }
        });
    }

    private static final String jsQuotedStringLiteral(String unquotedString) {
        if (unquotedString == null) {
            return "''";
        }
        return "'" + unquotedString.replace("\\", "\\\\").replace("'", "\\'") + "'";
    }

    public void onResume() {
        super.onResume();
        User localUser = OpenFeintInternal.getInstance().getCurrentUser();
        if (localUser != null && this.mIsFrameworkLoaded) {
            executeJavascript(String.format("if (OF.user) { OF.user.name = %s; OF.user.id = '%s'; }", jsQuotedStringLiteral(localUser.name), localUser.resourceID()));
            if (this.mShouldRefreshOnResume) {
                executeJavascript("if (OF.page) OF.refresh();");
            }
        }
        this.mShouldRefreshOnResume = true;
    }

    public void onStop() {
        super.onStop();
        dismissDialog();
    }

    /* access modifiers changed from: private */
    public void dismissDialog() {
        if (this.mLaunchLoadingView.isShowing()) {
            this.mLaunchLoadingView.dismiss();
        }
    }

    /* access modifiers changed from: private */
    public void showDialog() {
        if (!this.mLaunchLoadingView.isShowing()) {
            this.mLaunchLoadingView.show();
        }
    }

    public void onConfigurationChanged(Configuration newConfig) {
        String orientationString;
        super.onConfigurationChanged(newConfig);
        if (newConfig.orientation == 2) {
            orientationString = "landscape";
        } else {
            orientationString = "portrait";
        }
        executeJavascript(String.format("OF.setOrientation('%s');", orientationString));
    }

    public void loadInitialContent() {
        String path = initialContentPath();
        if (path.contains("?")) {
            path = path.split("\\?")[0];
        }
        if (!path.endsWith(".json")) {
            path = String.valueOf(path) + ".json";
        }
        WebViewCache.trackPath(path, new WebViewCacheCallback() {
            public void pathLoaded(String itemPath) {
                WebNav.this.executeJavascript("OF.navigateToUrl('" + WebNav.this.initialContentPath() + "')");
            }

            public void failLoaded() {
                WebNav.this.closeForDiskError();
            }
        });
    }

    /* access modifiers changed from: protected */
    public ActionHandler createActionHandler(WebNav webNav) {
        return new ActionHandler(webNav);
    }

    /* access modifiers changed from: protected */
    public WebNavClient createWebNavClient(ActionHandler actionHandler) {
        return new WebNavClient(actionHandler);
    }

    /* access modifiers changed from: protected */
    public String initialContentPath() {
        String contentPath = getIntent().getStringExtra("content_path");
        if (contentPath != null) {
            return contentPath;
        }
        throw new RuntimeException("WebNav intent requires extra value 'content_path'");
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 84) {
            executeJavascript(String.format("OF.menu('%s')", "search"));
            return true;
        } else if (keyCode != 4 || this.pageStackCount <= 1) {
            return super.onKeyDown(keyCode, event);
        } else {
            executeJavascript("OF.goBack()");
            return true;
        }
    }

    public void executeJavascript(String js) {
        if (this.mWebView != null) {
            this.mWebView.loadUrl("javascript:" + js);
        }
    }

    protected class WebNavClient extends WebViewClient {
        ActionHandler mActionHandler;

        public WebNavClient(ActionHandler anActionHandler) {
            this.mActionHandler = anActionHandler;
        }

        public boolean shouldOverrideUrlLoading(WebView view, String stringUrl) {
            Uri uri = Uri.parse(stringUrl);
            if (uri.getScheme().equals("http") || uri.getScheme().equals("https")) {
                view.loadUrl(stringUrl);
                return true;
            } else if (uri.getScheme().equals("openfeint")) {
                this.mActionHandler.dispatch(uri);
                return true;
            } else {
                OpenFeintInternal.log(WebNav.TAG, "UNHANDLED PROTOCOL: " + uri.getScheme());
                return true;
            }
        }

        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            this.mActionHandler.hideLoader(null);
        }

        public void onPageFinished(WebView view, String url) {
            if (!WebNav.this.mIsPageLoaded) {
                WebNav.this.mIsPageLoaded = true;
                if (WebNav.this.mIsFrameworkLoaded) {
                    loadInitialContent();
                } else {
                    attemptRecovery(view, url);
                }
            }
        }

        /* access modifiers changed from: protected */
        public void attemptRecovery(WebView view, String url) {
            if (WebViewCache.recover()) {
                WebNav.this.load(true);
                new AlertDialog.Builder(view.getContext()).setMessage(OpenFeintInternal.getRString(R.string.of_crash_report_query)).setNegativeButton(OpenFeintInternal.getRString(R.string.of_no), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        WebNav.this.finish();
                    }
                }).setPositiveButton(OpenFeintInternal.getRString(R.string.of_yes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        WebNavClient.this.submitCrashReport();
                    }
                }).show();
            } else if (!WebViewCache.isDiskError()) {
                WebNav.this.finish();
            }
        }

        /* access modifiers changed from: protected */
        public void submitCrashReport() {
            Map<String, Object> crashReport = new HashMap<>();
            crashReport.put("console", new JSONArray((Collection) WebNav.this.mPreloadConsoleOutput));
            JSONObject json = new JSONObject(crashReport);
            Map<String, Object> params = new HashMap<>();
            params.put("crash_report", json.toString());
            OpenFeintInternal.genericRequest("/webui/crash_report", "POST", params, null, null);
        }

        public void loadInitialContent() {
            OpenFeintInternal of = OpenFeintInternal.getInstance();
            User localUser = of.getCurrentUser();
            int orientation = WebNav.this.getResources().getConfiguration().orientation;
            HashMap<String, Object> user = new HashMap<>();
            if (localUser != null) {
                user.put("id", localUser.resourceID());
                user.put("name", localUser.name);
            }
            HashMap<String, Object> game = new HashMap<>();
            game.put("id", of.getAppID());
            game.put("name", of.getAppName());
            game.put("version", Integer.toString(of.getAppVersion()));
            Map<String, Object> device = OpenFeintInternal.getInstance().getDeviceParams();
            HashMap<String, Object> config = new HashMap<>();
            config.put("platform", "android");
            config.put("clientVersion", of.getOFVersion());
            config.put("hasNativeInterface", true);
            config.put("dpi", Util.getDpiName(WebNav.this));
            config.put("locale", WebNav.this.getResources().getConfiguration().locale.toString());
            config.put("user", new JSONObject(user));
            config.put("game", new JSONObject(game));
            config.put("device", new JSONObject(device));
            config.put("actions", new JSONArray((Collection) WebNav.this.getActionHandler().getActionList()));
            config.put("orientation", orientation == 2 ? "landscape" : "portrait");
            config.put("serverUrl", of.getServerUrl());
            WebNav.this.executeJavascript(String.format("OF.init.clientBoot(%s)", new JSONObject(config).toString()));
            this.mActionHandler.mWebNav.loadInitialContent();
        }
    }

    private class WebNavChromeClient extends WebChromeClient {
        private WebNavChromeClient() {
        }

        /* synthetic */ WebNavChromeClient(WebNav webNav, WebNavChromeClient webNavChromeClient) {
            this();
        }

        public boolean onJsAlert(WebView view, String url, String message, final JsResult result) {
            new AlertDialog.Builder(view.getContext()).setMessage(message).setNegativeButton(OpenFeintInternal.getRString(R.string.of_ok), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    result.cancel();
                }
            }).setOnCancelListener(new DialogInterface.OnCancelListener() {
                public void onCancel(DialogInterface dialog) {
                    result.cancel();
                }
            }).show();
            return true;
        }

        public boolean onJsConfirm(WebView view, String url, String message, final JsResult result) {
            new AlertDialog.Builder(view.getContext()).setMessage(message).setPositiveButton(OpenFeintInternal.getRString(R.string.of_ok), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    result.confirm();
                }
            }).setNegativeButton(OpenFeintInternal.getRString(R.string.of_cancel), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    result.cancel();
                }
            }).setOnCancelListener(new DialogInterface.OnCancelListener() {
                public void onCancel(DialogInterface dialog) {
                    result.cancel();
                }
            }).show();
            return true;
        }

        public void onConsoleMessage(String message, int lineNumber, String sourceID) {
            if (!WebNav.this.mIsFrameworkLoaded) {
                WebNav.this.mPreloadConsoleOutput.add(String.format("%s at %s:%d)", message, sourceID, Integer.valueOf(lineNumber)));
            }
        }
    }

    protected class ActionHandler {
        private static final String WEBUI_PREFS = "OFWebUI";
        private static final String WEBUI_SETTING_PREFIX = "OFWebUISetting_";
        List<String> mActionList = new ArrayList();
        WebNav mWebNav;

        /* access modifiers changed from: protected */
        public List<String> getActionList() {
            return this.mActionList;
        }

        public ActionHandler(WebNav webNav) {
            this.mWebNav = webNav;
            populateActionList(this.mActionList);
        }

        /* access modifiers changed from: protected */
        public void populateActionList(List<String> actionList) {
            actionList.add("log");
            actionList.add("apiRequest");
            actionList.add("contentLoaded");
            actionList.add("startLoading");
            actionList.add("back");
            actionList.add("showLoader");
            actionList.add("hideLoader");
            actionList.add("alert");
            actionList.add("dismiss");
            actionList.add("openMarket");
            actionList.add("isApplicationInstalled");
            actionList.add("openYoutubePlayer");
            actionList.add("profilePicture");
            actionList.add("openBrowser");
            actionList.add("downloadBlob");
            actionList.add("dashboard");
            actionList.add("readSetting");
            actionList.add("writeSetting");
        }

        public void dispatch(Uri uri) {
            if (uri.getHost().equals("action")) {
                Map<String, Object> options = parseQueryString(uri);
                String actionName = uri.getPath().replaceFirst("/", "");
                if (!actionName.equals("log")) {
                    Map<String, Object> escapedOptions = new HashMap<>(options);
                    String params = (String) options.get("params");
                    if (params != null && params.contains("password")) {
                        escapedOptions.put("params", "---FILTERED---");
                    }
                    OpenFeintInternal.log(WebNav.TAG, "ACTION: " + actionName + " " + escapedOptions.toString());
                }
                if (this.mActionList.contains(actionName)) {
                    try {
                        getClass().getMethod(actionName, Map.class).invoke(this, options);
                    } catch (NoSuchMethodException e) {
                        OpenFeintInternal.log(WebNav.TAG, "mActionList contains this method, but it is not implemented: " + actionName);
                    } catch (Exception e2) {
                        Exception e3 = e2;
                        OpenFeintInternal.log(WebNav.TAG, "Unhandled Exception: " + e3.toString() + "   " + e3.getCause());
                    }
                } else {
                    OpenFeintInternal.log(WebNav.TAG, "UNHANDLED ACTION: " + actionName);
                }
            } else {
                OpenFeintInternal.log(WebNav.TAG, "UNHANDLED MESSAGE TYPE: " + uri.getHost());
            }
        }

        private Map<String, Object> parseQueryString(Uri uri) {
            return parseQueryString(uri.getEncodedQuery());
        }

        private Map<String, Object> parseQueryString(String queryString) {
            Map<String, Object> options = new HashMap<>();
            if (queryString != null) {
                for (String stringPair : queryString.split("&")) {
                    String[] pair = stringPair.split("=");
                    if (pair.length == 2) {
                        options.put(pair[0], Uri.decode(pair[1]));
                    } else {
                        options.put(pair[0], null);
                    }
                }
            }
            return options;
        }

        public void apiRequest(Map<String, String> options) {
            final String requestID = options.get("request_id");
            OpenFeintInternal.genericRequest(options.get("path"), options.get("method"), parseQueryString(options.get("params")), parseQueryString(options.get("httpParams")), new IRawRequestDelegate() {
                public void onResponse(int statusCode, String responseBody) {
                    String response = responseBody.trim();
                    if (response.length() == 0) {
                        response = "{}";
                    }
                    ActionHandler.this.mWebNav.executeJavascript(String.format("OF.api.completeRequest(\"%s\", \"%d\", %s)", requestID, Integer.valueOf(statusCode), response));
                }
            });
        }

        public void contentLoaded(Map<String, String> options) {
            if (options.get("keepLoader") == null || !options.get("keepLoader").equals("true")) {
                hideLoader(null);
                WebNav.this.setTitle(options.get("title"));
            }
            this.mWebNav.fade(true);
            WebNav.this.dismissDialog();
        }

        public void startLoading(Map<String, String> options) {
            this.mWebNav.fade(false);
            showLoader(null);
            WebViewCache.trackPath(options.get("path"), new WebViewCacheCallback() {
                public void pathLoaded(String itemPath) {
                    WebNav.this.executeJavascript("OF.navigateToUrlCallback()");
                }

                public void failLoaded() {
                    WebNav.this.closeForDiskError();
                }

                public void onTrackingNeeded() {
                    WebNav.this.showDialog();
                }
            });
            this.mWebNav.pageStackCount++;
        }

        public void back(Map<String, String> options) {
            this.mWebNav.fade(false);
            String root = options.get("root");
            if (root != null && !root.equals("false")) {
                this.mWebNav.pageStackCount = 1;
            }
            if (this.mWebNav.pageStackCount > 1) {
                this.mWebNav.pageStackCount--;
            }
        }

        public void showLoader(Map<String, String> map) {
        }

        public void hideLoader(Map<String, String> map) {
        }

        public void log(Map<String, String> options) {
            if (options.get("message") != null) {
                OpenFeintInternal.log(WebNav.TAG, "WEBLOG: " + options.get("message"));
            }
        }

        public void alert(Map<String, String> options) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this.mWebNav);
            builder.setTitle(options.get("title"));
            builder.setMessage(options.get("message"));
            builder.setNegativeButton(OpenFeintInternal.getRString(R.string.of_ok), (DialogInterface.OnClickListener) null);
            builder.show();
        }

        public void dismiss(Map<String, String> map) {
            WebNav.this.finish();
        }

        public void openMarket(Map<String, String> options) {
            this.mWebNav.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=" + options.get("package_name"))));
        }

        public void isApplicationInstalled(Map<String, String> options) {
            boolean installed = false;
            List<ApplicationInfo> installedApps = this.mWebNav.getPackageManager().getInstalledApplications(0);
            String searchString = options.get("package_name");
            for (ApplicationInfo info : installedApps) {
                if (info.packageName.equals(searchString)) {
                    installed = true;
                }
            }
            WebNav webNav = WebNav.this;
            Object[] objArr = new Object[2];
            objArr[0] = options.get("callback");
            objArr[1] = installed ? "true" : "false";
            webNav.executeJavascript(String.format("%s(%s)", objArr));
        }

        public void openYoutubePlayer(Map<String, String> options) {
            Intent intent = new Intent("android.intent.action.VIEW", Uri.parse("vnd.youtube:" + options.get("video_id")));
            if (WebNav.this.getPackageManager().queryIntentActivities(intent, 65536).size() == 0) {
                Toast.makeText(this.mWebNav, OpenFeintInternal.getRString(R.string.of_no_video), 0).show();
            } else {
                this.mWebNav.startActivity(intent);
            }
        }

        public final void profilePicture(Map<String, String> map) {
            ImagePicker.show(WebNav.this);
        }

        public void openBrowser(Map<String, String> options) {
            Intent browserIntent = new Intent(this.mWebNav, NativeBrowser.class);
            WebNav.this.mNativeBrowserParameters = new HashMap();
            for (String arg : new String[]{"src", "callback", "on_cancel", "on_failure", "timeout"}) {
                String val = options.get(arg);
                if (val != null) {
                    WebNav.this.mNativeBrowserParameters.put(arg, val);
                    browserIntent.putExtra(NativeBrowser.INTENT_ARG_PREFIX + arg, val);
                }
            }
            WebNav.this.startActivityForResult(browserIntent, WebNav.REQUEST_CODE_NATIVE_BROWSER);
        }

        public void downloadBlob(Map<String, String> options) {
            String scoreJSON = options.get("score");
            String onError = options.get("onError");
            String onSuccess = options.get("onSuccess");
            try {
                Object scoreObject = new JsonResourceParser(new JsonFactory().createJsonParser(new StringReader(scoreJSON))).parse();
                if (scoreObject != null && (scoreObject instanceof Score)) {
                    Score score = (Score) scoreObject;
                    final String str = onSuccess;
                    final Score score2 = score;
                    final String str2 = onError;
                    score.downloadBlob(new Score.DownloadBlobCB() {
                        public void onSuccess() {
                            if (str != null) {
                                WebNav.this.executeJavascript(String.format("%s()", str));
                            }
                            ScoreBlobDelegate.notifyBlobDownloaded(score2);
                        }

                        public void onFailure(String exceptionMessage) {
                            if (str2 != null) {
                                WebNav.this.executeJavascript(String.format("%s(%s)", str2, exceptionMessage));
                            }
                        }
                    });
                }
            } catch (Exception e) {
                Exception e2 = e;
                if (onError != null) {
                    WebNav.this.executeJavascript(String.format("%s(%s)", onError, e2.getLocalizedMessage()));
                }
            }
        }

        public void dashboard(Map<String, String> map) {
            Dashboard.openFromSpotlight();
        }

        public void readSetting(Map<String, String> options) {
            String k = options.get("key");
            String cb = options.get("callback");
            if (cb != null) {
                String val = OpenFeintInternal.getInstance().getContext().getSharedPreferences(WEBUI_PREFS, 0).getString(k != null ? WEBUI_SETTING_PREFIX + k : null, null);
                OpenFeintInternal.log(WebNav.TAG, String.format("readSetting(%s) => %s", k, val));
                WebNav webNav = WebNav.this;
                Object[] objArr = new Object[2];
                objArr[0] = cb;
                objArr[1] = val != null ? val : "null";
                webNav.executeJavascript(String.format("%s(%s)", objArr));
            }
        }

        public void writeSetting(Map<String, String> options) {
            String k = options.get("key");
            String v = options.get("value");
            if (k != null && v != null) {
                SharedPreferences.Editor editor = OpenFeintInternal.getInstance().getContext().getSharedPreferences(WEBUI_PREFS, 0).edit();
                editor.putString(WEBUI_SETTING_PREFIX + k, v);
                editor.commit();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Bitmap image;
        super.onActivityResult(requestCode, resultCode, data);
        if (this.mNativeBrowserParameters != null && requestCode == REQUEST_CODE_NATIVE_BROWSER) {
            if (resultCode != 0) {
                this.mShouldRefreshOnResume = false;
                if (data.getBooleanExtra("com.openfeint.internal.ui.NativeBrowser.argument.failed", false)) {
                    String cb = this.mNativeBrowserParameters.get("on_failure");
                    if (cb != null) {
                        executeJavascript(String.format("%s(%d, %s)", cb, Integer.valueOf(data.getIntExtra("com.openfeint.internal.ui.NativeBrowser.argument.failure_code", 0)), jsQuotedStringLiteral(data.getStringExtra("com.openfeint.internal.ui.NativeBrowser.argument.failure_desc"))));
                    }
                } else {
                    String cb2 = this.mNativeBrowserParameters.get("callback");
                    if (cb2 != null) {
                        String rv = data.getStringExtra("com.openfeint.internal.ui.NativeBrowser.argument.result");
                        Object[] objArr = new Object[2];
                        objArr[0] = cb2;
                        objArr[1] = rv != null ? rv : "";
                        executeJavascript(String.format("%s(%s)", objArr));
                    }
                }
            } else {
                String cb3 = this.mNativeBrowserParameters.get("on_cancel");
                if (cb3 != null) {
                    executeJavascript(String.format("%s()", cb3));
                }
            }
            this.mNativeBrowserParameters = null;
        } else if (ImagePicker.isImagePickerActivityResult(requestCode) && (image = ImagePicker.onImagePickerActivityResult(this, resultCode, 152, data)) != null) {
            ImagePicker.compressAndUpload(image, "/xp/users/" + OpenFeintInternal.getInstance().getCurrentUser().resourceID() + "/profile_picture", new OpenFeintInternal.IUploadDelegate() {
                public void fileUploadedTo(String url, boolean success) {
                    if (success) {
                        WebNav.this.executeJavascript("try { OF.page.onProfilePictureChanged('" + url + "'); } catch (e) {}");
                    }
                }
            });
        }
    }

    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        OpenFeintDelegate d = OpenFeintInternal.getInstance().getDelegate();
        if (d == null) {
            return;
        }
        if (hasFocus) {
            d.onDashboardAppear();
        } else {
            d.onDashboardDisappear();
        }
    }

    public void onDestroy() {
        this.mWebView.destroy();
        this.mWebView = null;
        super.onDestroy();
    }
}
