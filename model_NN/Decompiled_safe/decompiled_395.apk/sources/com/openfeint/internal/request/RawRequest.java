package com.openfeint.internal.request;

public abstract class RawRequest extends JSONRequest {
    private IRawRequestDelegate mDelegate;

    public RawRequest() {
    }

    public RawRequest(OrderedArgList args) {
        super(args);
    }

    public void onResponse(int responseCode, byte[] body) {
        String respText;
        try {
            super.onResponse(responseCode, body);
            if (this.mDelegate != null) {
                if (!isResponseJSON()) {
                    respText = notJSONError(responseCode).generate();
                } else {
                    respText = new String(body);
                }
                this.mDelegate.onResponse(responseCode, respText);
            }
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
    }

    public void setDelegate(IRawRequestDelegate mDelegate2) {
        this.mDelegate = mDelegate2;
    }
}
