package com.openfeint.internal.request;

import com.openfeint.internal.OpenFeintInternal;
import com.openfeint.internal.resource.ServerException;
import jp.Tontoro.FlickKing.R;
import org.apache.http.client.methods.HttpUriRequest;

public abstract class JSONContentRequest extends BaseRequest {
    private static final String CONTENT_TYPE = "application/json";
    public static final String DESIRED_RESPONSE_PREFIX = "application/json;";

    protected static ServerException notJSONError(int responseCode) {
        return new ServerException("ServerError", String.format(OpenFeintInternal.getRString(R.string.of_server_error_code_format), Integer.valueOf(responseCode)));
    }

    public JSONContentRequest() {
    }

    /* access modifiers changed from: protected */
    public HttpUriRequest generateRequest() {
        HttpUriRequest req = super.generateRequest();
        req.addHeader("Accept", CONTENT_TYPE);
        return req;
    }

    /* access modifiers changed from: protected */
    public boolean isResponseJSON() {
        String responseType = getResponseType();
        return responseType != null && responseType.startsWith(DESIRED_RESPONSE_PREFIX);
    }

    public JSONContentRequest(OrderedArgList args) {
        super(args);
    }
}
