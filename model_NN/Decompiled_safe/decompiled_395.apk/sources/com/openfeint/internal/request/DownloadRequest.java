package com.openfeint.internal.request;

import com.openfeint.internal.JsonResourceParser;
import com.openfeint.internal.OpenFeintInternal;
import com.openfeint.internal.resource.ServerException;
import java.io.IOException;
import jp.Tontoro.FlickKing.R;
import org.codehaus.jackson.JsonFactory;

public abstract class DownloadRequest extends BaseRequest {
    /* access modifiers changed from: protected */
    public abstract void onSuccess(byte[] bArr);

    public DownloadRequest() {
    }

    public DownloadRequest(OrderedArgList args) {
        super(args);
    }

    public String method() {
        return "GET";
    }

    public void onFailure(String exceptionMessage) {
        OpenFeintInternal.log("ServerException", exceptionMessage);
    }

    public void onResponse(int responseCode, byte[] body) {
        String exceptionMessage = OpenFeintInternal.getRString(R.string.of_unknown_server_error);
        if (200 > responseCode || responseCode >= 300 || body == null) {
            if (404 == responseCode) {
                exceptionMessage = OpenFeintInternal.getRString(R.string.of_file_not_found);
            } else {
                try {
                    Object responseBody = new JsonResourceParser(new JsonFactory().createJsonParser(body)).parse();
                    if (responseBody != null && (responseBody instanceof ServerException)) {
                        ServerException e = (ServerException) responseBody;
                        exceptionMessage = String.valueOf(e.exceptionClass) + ": " + e.message;
                    }
                } catch (IOException e2) {
                    exceptionMessage = OpenFeintInternal.getRString(R.string.of_error_parsing_error_message);
                }
            }
            onFailure(exceptionMessage);
            return;
        }
        onSuccess(body);
    }
}
