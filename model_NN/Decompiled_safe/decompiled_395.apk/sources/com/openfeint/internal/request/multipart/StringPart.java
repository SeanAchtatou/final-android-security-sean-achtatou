package com.openfeint.internal.request.multipart;

import java.io.IOException;
import java.io.OutputStream;

public class StringPart extends PartBase {
    public static final String DEFAULT_CHARSET = "UTF-8";
    public static final String DEFAULT_CONTENT_TYPE = "text/html";
    public static final String DEFAULT_TRANSFER_ENCODING = "8bit";
    private byte[] content;
    private String value;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public StringPart(java.lang.String r4, java.lang.String r5, java.lang.String r6) {
        /*
            r3 = this;
            java.lang.String r0 = "text/html"
            if (r6 != 0) goto L_0x0015
            java.lang.String r1 = "UTF-8"
        L_0x0006:
            java.lang.String r2 = "8bit"
            r3.<init>(r4, r0, r1, r2)
            if (r5 != 0) goto L_0x0017
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "Value may not be null"
            r0.<init>(r1)
            throw r0
        L_0x0015:
            r1 = r6
            goto L_0x0006
        L_0x0017:
            r0 = 0
            int r0 = r5.indexOf(r0)
            r1 = -1
            if (r0 == r1) goto L_0x0027
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "NULs may not be present in string parts"
            r0.<init>(r1)
            throw r0
        L_0x0027:
            r3.value = r5
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.openfeint.internal.request.multipart.StringPart.<init>(java.lang.String, java.lang.String, java.lang.String):void");
    }

    public StringPart(String name, String value2) {
        this(name, value2, null);
    }

    private byte[] getContent() {
        if (this.content == null) {
            this.content = EncodingUtil.getBytes(this.value, getCharSet());
        }
        return this.content;
    }

    /* access modifiers changed from: protected */
    public void sendData(OutputStream out) throws IOException {
        out.write(getContent());
    }

    /* access modifiers changed from: protected */
    public long lengthOfData() throws IOException {
        return (long) getContent().length;
    }

    public void setCharSet(String charSet) {
        super.setCharSet(charSet);
        this.content = null;
    }
}
