package com.openfeint.internal.resource;

import java.io.IOException;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonToken;

public abstract class BooleanResourceProperty extends PrimitiveResourceProperty {
    public abstract boolean get(Resource resource);

    public abstract void set(Resource resource, boolean z);

    public void copy(Resource lhs, Resource rhs) {
        set(lhs, get(rhs));
    }

    public void parse(Resource obj, JsonParser jp2) throws JsonParseException, IOException {
        if (jp2.getCurrentToken() == JsonToken.VALUE_TRUE || jp2.getText().equalsIgnoreCase("true") || jp2.getText().equalsIgnoreCase("1") || jp2.getText().equalsIgnoreCase("YES")) {
            set(obj, true);
        } else {
            set(obj, false);
        }
    }

    public void generate(Resource obj, JsonGenerator generator, String key) throws JsonGenerationException, IOException {
        generator.writeFieldName(key);
        generator.writeBoolean(get(obj));
    }
}
