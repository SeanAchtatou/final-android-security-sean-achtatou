package com.openfeint.internal.resource;

import java.io.IOException;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;

public abstract class LongResourceProperty extends PrimitiveResourceProperty {
    public abstract long get(Resource resource);

    public abstract void set(Resource resource, long j);

    public void copy(Resource lhs, Resource rhs) {
        set(lhs, get(rhs));
    }

    public void parse(Resource obj, JsonParser jp2) throws JsonParseException, IOException {
        set(obj, jp2.getLongValue());
    }

    public void set(Resource obj, String val) {
        set(obj, Long.valueOf(val).longValue());
    }

    public void generate(Resource obj, JsonGenerator generator, String key) throws JsonGenerationException, IOException {
        generator.writeFieldName(key);
        generator.writeNumber(get(obj));
    }
}
