package com.openfeint.internal.resource;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;

public abstract class DateResourceProperty extends PrimitiveResourceProperty {
    public static DateFormat sDateParser = makeDateParser();

    public abstract Date get(Resource resource);

    public abstract void set(Resource resource, Date date);

    public void copy(Resource lhs, Resource rhs) {
        set(lhs, get(rhs));
    }

    static DateFormat makeDateParser() {
        DateFormat p = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        p.setTimeZone(TimeZone.getTimeZone("UTC"));
        return p;
    }

    public void parse(Resource obj, JsonParser jp2) throws JsonParseException, IOException {
        String text = jp2.getText();
        if (text.equals("null")) {
            set(obj, null);
            return;
        }
        try {
            set(obj, sDateParser.parse(text));
        } catch (ParseException e) {
            set(obj, null);
        }
    }

    public void generate(Resource obj, JsonGenerator generator, String key) throws JsonGenerationException, IOException {
        Date o = get(obj);
        if (o != null) {
            generator.writeFieldName(key);
            generator.writeString(sDateParser.format(o));
        }
    }
}
