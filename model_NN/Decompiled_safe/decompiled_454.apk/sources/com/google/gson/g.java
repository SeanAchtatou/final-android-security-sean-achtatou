package com.google.gson;

import java.util.Collection;

final class g implements ExclusionStrategy {
    private final Collection<ExclusionStrategy> a;

    public g(Collection<ExclusionStrategy> collection) {
        at.a(collection);
        this.a = collection;
    }

    public final boolean shouldSkipClass(Class<?> clazz) {
        for (ExclusionStrategy shouldSkipClass : this.a) {
            if (shouldSkipClass.shouldSkipClass(clazz)) {
                return true;
            }
        }
        return false;
    }

    public final boolean shouldSkipField(FieldAttributes f) {
        for (ExclusionStrategy shouldSkipField : this.a) {
            if (shouldSkipField.shouldSkipField(f)) {
                return true;
            }
        }
        return false;
    }
}
