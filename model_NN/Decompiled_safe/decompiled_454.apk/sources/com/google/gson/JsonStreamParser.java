package com.google.gson;

import java.io.EOFException;
import java.io.Reader;
import java.io.StringReader;
import java.util.Iterator;
import java.util.NoSuchElementException;

public final class JsonStreamParser implements Iterator<JsonElement> {
    private final y a;
    private final Object b;
    private JsonElement c;

    public JsonStreamParser(Reader reader) {
        this.a = new y(reader);
        this.b = new Object();
        this.c = null;
    }

    public JsonStreamParser(String json) {
        this(new StringReader(json));
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean hasNext() {
        /*
            r2 = this;
            java.lang.Object r0 = r2.b
            monitor-enter(r0)
            com.google.gson.JsonElement r1 = r2.next()     // Catch:{ NoSuchElementException -> 0x000d }
            r2.c = r1     // Catch:{ NoSuchElementException -> 0x000d }
            r1 = 1
            monitor-exit(r0)     // Catch:{ all -> 0x0015 }
            r0 = r1
        L_0x000c:
            return r0
        L_0x000d:
            r1 = move-exception
            r1 = 0
            r2.c = r1     // Catch:{ all -> 0x0015 }
            r1 = 0
            monitor-exit(r0)     // Catch:{ all -> 0x0015 }
            r0 = r1
            goto L_0x000c
        L_0x0015:
            r1 = move-exception
            monitor-exit(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.gson.JsonStreamParser.hasNext():boolean");
    }

    public final JsonElement next() throws JsonParseException {
        synchronized (this.b) {
            if (this.c != null) {
                JsonElement jsonElement = this.c;
                this.c = null;
                return jsonElement;
            }
            try {
                return this.a.a();
            } catch (ba e) {
                throw new JsonParseException("Failed parsing JSON source to Json", e);
            } catch (as e2) {
                throw new JsonParseException("Failed parsing JSON source to Json", e2);
            } catch (StackOverflowError e3) {
                throw new JsonParseException("Failed parsing JSON source to Json", e3);
            } catch (OutOfMemoryError e4) {
                throw new JsonParseException("Failed parsing JSON source to Json", e4);
            } catch (JsonParseException e5) {
                if (e5.getCause() instanceof EOFException) {
                    throw new NoSuchElementException();
                }
                throw e5;
            }
        }
    }

    public final void remove() {
        throw new UnsupportedOperationException();
    }
}
