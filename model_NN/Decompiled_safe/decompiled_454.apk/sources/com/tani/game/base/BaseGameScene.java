package com.tani.game.base;

import com.tani.game.animalpuzzle.activity.MainActivity;
import org.anddev.andengine.entity.scene.Scene;

public abstract class BaseGameScene extends Scene {
    /* access modifiers changed from: protected */
    public MainActivity parent;

    public abstract void onBackPressed();

    /* access modifiers changed from: protected */
    public abstract void onLoadComplete();

    /* access modifiers changed from: protected */
    public abstract void onLoadResources();

    /* access modifiers changed from: protected */
    public abstract void onLoadScene();

    /* access modifiers changed from: protected */
    public abstract void unloadScene();

    public BaseGameScene(int pLayerCount, MainActivity parent2) {
        super(pLayerCount);
        this.parent = parent2;
    }

    public void loadResources(boolean loadSceneAutomatically) {
        onLoadResources();
        if (loadSceneAutomatically) {
            onLoadScene();
        }
    }

    public void loadScene() {
        onLoadScene();
    }
}
