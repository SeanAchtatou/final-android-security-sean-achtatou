package com.tani.game.animalpuzzle.activity;

import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import com.tani.animalpuzzle.res.CommonResource;
import com.tani.animalpuzzle.res.LevelResource;
import com.tani.animalpuzzle.res.MenuResource;
import com.tani.animalpuzzle.res.PlayingResource;
import com.tani.animalpuzzle.sound.SoundManager;
import com.tani.game.animalpuzzle.scene.SplashScene;
import com.tani.game.base.BaseGameScene;
import org.anddev.andengine.engine.Engine;
import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.engine.options.EngineOptions;
import org.anddev.andengine.engine.options.resolutionpolicy.RatioResolutionPolicy;
import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.opengl.buffer.BufferObjectManager;
import org.anddev.andengine.opengl.texture.Texture;
import org.anddev.andengine.opengl.texture.region.TextureRegion;
import org.anddev.andengine.opengl.view.RenderSurfaceView;
import org.anddev.andengine.ui.activity.BaseGameActivity;

public class MainActivity extends BaseGameActivity {
    public static int HEIGHT = 320;
    public static int WIDTH = 480;
    /* access modifiers changed from: private */
    public AdView adView;
    private PlayingResource animalResource;
    public Camera camera;
    private CommonResource commonResource;
    private boolean commonResourceLoaded = false;
    private int level;
    private LevelResource levelResource;
    private boolean levelResourceLoaded = false;
    private boolean loadAnimalRes = false;
    private SoundManager mainSoundManager;
    private MenuResource menuResource;
    private boolean menuResoureLoaded = false;
    private boolean soundOn = true;

    public Engine onLoadEngine() {
        this.camera = new Camera(0.0f, 0.0f, (float) WIDTH, (float) HEIGHT);
        return new Engine(new EngineOptions(true, EngineOptions.ScreenOrientation.LANDSCAPE, new RatioResolutionPolicy((float) WIDTH, (float) HEIGHT), this.camera).setNeedsSound(true).setNeedsMusic(true));
    }

    public void onLoadResources() {
        this.animalResource = new PlayingResource();
        this.mainSoundManager = new SoundManager(this);
        this.menuResource = new MenuResource();
        this.levelResource = new LevelResource();
        this.commonResource = new CommonResource();
        this.mainSoundManager.initSound();
    }

    public Scene onLoadScene() {
        SplashScene splashScene = new SplashScene(1, this);
        splashScene.loadResources(true);
        return splashScene;
    }

    public void onLoadComplete() {
    }

    public void setScene(Scene scene) {
        this.mEngine.setScene(scene);
    }

    public void vibrate() {
        this.mEngine.enableVibrator(this);
        this.mEngine.vibrate(50);
    }

    public int getLevel() {
        return this.level;
    }

    public void setLevel(int level2) {
        this.level = level2;
    }

    public PlayingResource getAnimalResource() {
        return this.animalResource;
    }

    public void setAnimalResource(PlayingResource animalResource2) {
        this.animalResource = animalResource2;
    }

    public boolean isLoadAnimalRes() {
        return this.loadAnimalRes;
    }

    public void setLoadAnimalRes(boolean loadAnimalRes2) {
        this.loadAnimalRes = loadAnimalRes2;
    }

    public void onBackPressed() {
        ((BaseGameScene) this.mEngine.getScene()).onBackPressed();
    }

    public boolean isSoundOn() {
        return this.soundOn;
    }

    public void setSoundOn(boolean soundOn2) {
        this.soundOn = soundOn2;
    }

    public SoundManager getMainSoundManager() {
        return this.mainSoundManager;
    }

    public void setMainSoundManager(SoundManager mainSoundManager2) {
        this.mainSoundManager = mainSoundManager2;
    }

    public MenuResource getMenuResource() {
        return this.menuResource;
    }

    public void setMenuResource(MenuResource menuResource2) {
        this.menuResource = menuResource2;
    }

    public boolean isMenuResoureLoaded() {
        return this.menuResoureLoaded;
    }

    public void setMenuResoureLoaded(boolean menuResoureLoaded2) {
        this.menuResoureLoaded = menuResoureLoaded2;
    }

    public LevelResource getLevelResource() {
        return this.levelResource;
    }

    public void setLevelResource(LevelResource levelResource2) {
        this.levelResource = levelResource2;
    }

    public boolean isLevelResourceLoaded() {
        return this.levelResourceLoaded;
    }

    public void setLevelResourceLoaded(boolean levelResourceLoaded2) {
        this.levelResourceLoaded = levelResourceLoaded2;
    }

    public CommonResource getCommonResource() {
        return this.commonResource;
    }

    public void setCommonResource(CommonResource commonResource2) {
        this.commonResource = commonResource2;
    }

    public boolean isCommonResourceLoaded() {
        return this.commonResourceLoaded;
    }

    public void setCommonResourceLoaded(boolean commonResourceLoaded2) {
        this.commonResourceLoaded = commonResourceLoaded2;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        if (this.commonResourceLoaded) {
            releaseTextureRetion(this.commonResource.dlgBkgRegion);
            releaseTexture(this.commonResource.dlgBkgTexture);
            releaseTextureRetion(this.commonResource.okBtnRegion);
            releaseTextureRetion(this.commonResource.cancelBtnRegion);
            releaseTextureRetion(this.commonResource.nextBtnRegion);
            releaseTextureRetion(this.commonResource.menuBtnRegion);
            releaseTextureRetion(this.commonResource.replayBtnRegion);
            releaseTexture(this.commonResource.dlgBtnTexture);
        }
        if (this.menuResoureLoaded) {
            releaseTextureRetion(this.menuResource.playingBgTextureRegion);
            releaseTexture(this.menuResource.bkgTexture);
            releaseTextureRetion(this.menuResource.playBtnRegion);
            releaseTextureRetion(this.menuResource.guideBtnRegion);
            releaseTexture(this.menuResource.btnTexture);
            releaseTextureRetion(this.menuResource.soundOffRegion);
            releaseTextureRetion(this.menuResource.soundOnRegion);
            releaseTexture(this.menuResource.soundTexture);
        }
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void releaseTextureRetion(TextureRegion textureRegion) {
        if (textureRegion != null) {
            BufferObjectManager.getActiveInstance().unloadBufferObject(textureRegion.getTextureBuffer());
        }
    }

    /* access modifiers changed from: protected */
    public void releaseTexture(Texture texture) {
        this.mEngine.getTextureManager().unloadTexture(texture);
    }

    /* access modifiers changed from: protected */
    public void onSetContentView() {
        RelativeLayout relativeLayout = new RelativeLayout(this);
        FrameLayout.LayoutParams relativeLayoutLayoutParams = new FrameLayout.LayoutParams(-1, -1);
        this.adView = new AdView(this, AdSize.BANNER, "a14da9c1f653cbd");
        this.adView.loadAd(new AdRequest());
        this.mRenderSurfaceView = new RenderSurfaceView(this);
        this.mRenderSurfaceView.setRenderer(this.mEngine);
        RelativeLayout.LayoutParams surfaceViewLayoutParams = new RelativeLayout.LayoutParams((ViewGroup.MarginLayoutParams) super.createSurfaceViewLayoutParams());
        surfaceViewLayoutParams.addRule(13);
        relativeLayout.addView(this.mRenderSurfaceView, surfaceViewLayoutParams);
        relativeLayout.addView(this.adView, createAdViewLayoutParams());
        setContentView(relativeLayout, relativeLayoutLayoutParams);
    }

    private RelativeLayout.LayoutParams createAdViewLayoutParams() {
        RelativeLayout.LayoutParams adViewLayoutParams = new RelativeLayout.LayoutParams(-2, -2);
        adViewLayoutParams.addRule(12);
        adViewLayoutParams.addRule(9);
        return adViewLayoutParams;
    }

    public void showAd() {
        runOnUiThread(new Runnable() {
            public void run() {
                MainActivity.this.adView.loadAd(new AdRequest());
                MainActivity.this.adView.setVisibility(0);
            }
        });
    }

    public void hideAd() {
        runOnUiThread(new Runnable() {
            public void run() {
                MainActivity.this.adView.setVisibility(8);
            }
        });
    }
}
