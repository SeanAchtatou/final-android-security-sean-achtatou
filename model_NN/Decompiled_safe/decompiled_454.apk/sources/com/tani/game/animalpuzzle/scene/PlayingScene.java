package com.tani.game.animalpuzzle.scene;

import android.graphics.Point;
import android.util.Log;
import com.tani.animalpuzzle.res.PlayingResource;
import com.tani.animalpuzzle.util.AlgorithmUtil;
import com.tani.game.animalpuzzle.activity.MainActivity;
import com.tani.game.animalpuzzle.scene.GameFinish;
import com.tani.game.animalpuzzle.scene.GameOverScene;
import com.tani.game.animalpuzzle.scene.LevelUpScene;
import com.tani.game.animalpuzzle.scene.PauseScene;
import com.tani.game.animalpuzzle.sprite.AnimalSprite;
import com.tani.game.base.AsyncTaskLoader;
import com.tani.game.base.BaseGameScene;
import com.tani.game.base.IAsyncCallback;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.anddev.andengine.engine.handler.timer.ITimerCallback;
import org.anddev.andengine.engine.handler.timer.TimerHandler;
import org.anddev.andengine.entity.primitive.Line;
import org.anddev.andengine.entity.scene.background.EntityBackground;
import org.anddev.andengine.entity.sprite.Sprite;
import org.anddev.andengine.entity.text.ChangeableText;
import org.anddev.andengine.input.touch.TouchEvent;
import org.anddev.andengine.opengl.font.Font;
import org.anddev.andengine.opengl.font.FontFactory;
import org.anddev.andengine.opengl.texture.Texture;
import org.anddev.andengine.opengl.texture.TextureOptions;
import org.anddev.andengine.opengl.texture.region.TextureRegionFactory;

public class PlayingScene extends BaseGameScene {
    public static final int CELL = 44;
    public static final int COLUMNS = 10;
    public static final int LEFT = 30;
    public static final int ROWS = 6;
    public static final int TOP = 32;
    private TimerHandler adsTimeHandler;
    PlayingResource animalResource = null;
    /* access modifiers changed from: private */
    public AnimalSprite[][] animalSprites = ((AnimalSprite[][]) Array.newInstance(AnimalSprite.class, 10, 6));
    /* access modifiers changed from: private */
    public int[][] animalValues = ((int[][]) Array.newInstance(Integer.TYPE, 12, 8));
    /* access modifiers changed from: private */
    public IAsyncCallback callback;
    /* access modifiers changed from: private */
    public boolean childSceneOn = false;
    /* access modifiers changed from: private */
    public Texture fontTexture = null;
    private ChangeableText levelText;
    /* access modifiers changed from: private */
    public Font mFont;
    /* access modifiers changed from: private */
    public boolean running = false;
    /* access modifiers changed from: private */
    public int score = 0;
    /* access modifiers changed from: private */
    public AnimalSprite selectedAnimalSprite;
    /* access modifiers changed from: private */
    public Sprite selectedShapeSprite;
    /* access modifiers changed from: private */
    public int time = 0;
    /* access modifiers changed from: private */
    public ChangeableText timeText;
    /* access modifiers changed from: private */
    public TimerHandler timerHandler;

    public PlayingScene(int pLayerCount, MainActivity parent) {
        super(pLayerCount, parent);
        this.animalResource = parent.getAnimalResource();
    }

    /* access modifiers changed from: protected */
    public void onLoadResources() {
        TextureRegionFactory.setAssetBasePath("images/game/");
        this.callback = new IAsyncCallback() {
            public void workToDo() {
                if (!PlayingScene.this.parent.isLoadAnimalRes()) {
                    PlayingScene.this.animalResource.bkgTexture = new Texture(512, 512, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
                    PlayingScene.this.animalResource.playingBgTextureRegion = TextureRegionFactory.createFromAsset(PlayingScene.this.animalResource.bkgTexture, PlayingScene.this.parent, "play_bg.png", 0, 0);
                    PlayingScene.this.parent.getEngine().getTextureManager().loadTexture(PlayingScene.this.animalResource.bkgTexture);
                    PlayingScene.this.animalResource.animalTexture = new Texture(512, 128, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
                    TextureRegionFactory.createFromAsset(PlayingScene.this.animalResource.animalTexture, PlayingScene.this.parent, "images.png", 0, 0);
                    for (int i = 0; i < 22; i++) {
                        PlayingScene.this.animalResource.animalTextureRegions[i] = TextureRegionFactory.extractFromTexture(PlayingScene.this.animalResource.animalTexture, (i % 12) * 42, (i / 12) * 44, 42, 44);
                    }
                    PlayingScene.this.animalResource.selectAnimalTextureRegion = TextureRegionFactory.extractFromTexture(PlayingScene.this.animalResource.animalTexture, 420, 44, 42, 44);
                    PlayingScene.this.parent.getEngine().getTextureManager().loadTexture(PlayingScene.this.animalResource.animalTexture);
                    PlayingScene.this.parent.setLoadAnimalRes(true);
                }
                FontFactory.setAssetBasePath("font/");
                PlayingScene.this.fontTexture = new Texture(512, 512, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
                PlayingScene.this.mFont = FontFactory.createFromAsset(PlayingScene.this.fontTexture, PlayingScene.this.parent, "Plok.ttf", 18.0f, true, -65536);
                PlayingScene.this.parent.getEngine().getTextureManager().loadTexture(PlayingScene.this.fontTexture);
                PlayingScene.this.parent.getEngine().getFontManager().loadFont(PlayingScene.this.mFont);
            }

            public void onComplete() {
                PlayingScene.this.parent.getEngine().getTextureManager().reloadTextures();
                PlayingScene.this.onLoadScene();
            }
        };
        this.parent.runOnUiThread(new Runnable() {
            public void run() {
                new AsyncTaskLoader().execute(PlayingScene.this.callback);
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onLoadScene() {
        initAnimals();
        setBackground(new EntityBackground(new Sprite(0.0f, 0.0f, this.animalResource.playingBgTextureRegion)));
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 6; j++) {
                AnimalSprite animalSprite = this.animalSprites[i][j];
                getChild(1).attachChild(animalSprite);
                registerTouchArea(animalSprite);
            }
        }
        this.selectedShapeSprite = new Sprite(0.0f, 0.0f, this.animalResource.selectAnimalTextureRegion);
        getChild(2).attachChild(this.selectedShapeSprite);
        this.selectedShapeSprite.setVisible(false);
        this.levelText = new ChangeableText(30.0f, 5.0f, this.mFont, "Level:" + this.parent.getLevel(), 8);
        getChild(3).attachChild(this.levelText);
        this.time = getTimeForLevel();
        this.timeText = new ChangeableText(370.0f, 5.0f, this.mFont, getTimeString(this.time), 11);
        getChild(3).attachChild(this.timeText);
        this.timerHandler = new TimerHandler(1.0f, true, new ITimerCallback() {
            public void onTimePassed(TimerHandler pTimerHandler) {
                if (PlayingScene.this.running) {
                    PlayingScene playingScene = PlayingScene.this;
                    playingScene.time = playingScene.time - 1;
                    PlayingScene.this.timeText.setText(PlayingScene.this.getTimeString(PlayingScene.this.time));
                    if (PlayingScene.this.time == 0) {
                        PlayingScene.this.unregisterUpdateHandler(PlayingScene.this.timerHandler);
                        PlayingScene.this.gameOver();
                    }
                }
            }
        });
        this.adsTimeHandler = new TimerHandler(5.0f, true, new ITimerCallback() {
            public void onTimePassed(TimerHandler pTimerHandler) {
                PlayingScene.this.parent.hideAd();
            }
        });
        registerUpdateHandler(this.timerHandler);
        registerUpdateHandler(this.adsTimeHandler);
        this.parent.setScene(this);
        this.running = true;
        this.parent.showAd();
    }

    /* access modifiers changed from: private */
    public String getTimeString(int value) {
        int minute = value / 60;
        int second = value % 60;
        String ret = "";
        if (minute < 10) {
            ret = String.valueOf(ret) + "0";
        }
        String ret2 = String.valueOf(ret) + minute + ":";
        if (second < 10) {
            ret2 = String.valueOf(ret2) + "0";
        }
        return String.valueOf(ret2) + second;
    }

    private int getTimeForLevel() {
        if (this.parent.getLevel() == 1) {
            return 180;
        }
        if (this.parent.getLevel() == 2) {
            return 180;
        }
        if (this.parent.getLevel() == 3) {
            return 180;
        }
        if (this.parent.getLevel() == 4) {
            return 180;
        }
        if (this.parent.getLevel() == 5) {
            return 150;
        }
        if (this.parent.getLevel() == 6) {
            return 150;
        }
        if (this.parent.getLevel() == 7) {
            return 150;
        }
        if (this.parent.getLevel() == 8) {
            return 150;
        }
        return this.parent.getLevel() == 9 ? 120 : 150;
    }

    private void initAnimals() {
        List<Integer> images = new ArrayList<>();
        for (int i = 1; i <= 22; i++) {
            images.add(new Integer(i));
            images.add(new Integer(i));
            if (i < 9) {
                images.add(new Integer(i));
                images.add(new Integer(i));
            }
        }
        Collections.shuffle(images);
        for (int i2 = 0; i2 < 12; i2++) {
            for (int j = 0; j < 8; j++) {
                this.animalValues[i2][j] = 0;
            }
        }
        int k = 0;
        for (int i3 = 0; i3 < 10; i3++) {
            for (int j2 = 0; j2 < 6; j2++) {
                AnimalSprite animalSprite = new AnimalSprite((float) ((i3 * 42) + 30), (float) ((j2 * 44) + 32), this.parent.getAnimalResource().animalTextureRegions[((Integer) images.get(k)).intValue() - 1]) {
                    public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                        if (pSceneTouchEvent.getAction() == 0) {
                            return true;
                        }
                        if (1 == pSceneTouchEvent.getAction()) {
                            return PlayingScene.this.touchAnimalSprite(this);
                        }
                        return super.onAreaTouched(pSceneTouchEvent, pTouchAreaLocalX, pTouchAreaLocalY);
                    }
                };
                animalSprite.setXPos(i3 + 1);
                animalSprite.setYPos(j2 + 1);
                this.animalSprites[i3][j2] = animalSprite;
                this.animalValues[i3 + 1][j2 + 1] = ((Integer) images.get(k)).intValue();
                k++;
            }
        }
    }

    /* access modifiers changed from: private */
    public boolean touchAnimalSprite(AnimalSprite animalSprite) {
        if (!this.running) {
            return false;
        }
        if (this.selectedAnimalSprite == null) {
            if (this.parent.isSoundOn()) {
                this.parent.getMainSoundManager().playSelectSound();
            }
            this.selectedAnimalSprite = animalSprite;
            this.selectedShapeSprite.setPosition(animalSprite.getX(), animalSprite.getY());
            this.selectedShapeSprite.setVisible(true);
        } else if (animalSprite != this.selectedAnimalSprite) {
            List<Point> points = AlgorithmUtil.checkPath(this.animalValues, animalSprite.getXPos(), animalSprite.getYPos(), this.selectedAnimalSprite.getXPos(), this.selectedAnimalSprite.getYPos());
            if (points == null) {
                if (this.parent.isSoundOn()) {
                    this.parent.getMainSoundManager().playWrongSelectSound();
                }
                this.selectedShapeSprite.setVisible(false);
                this.selectedAnimalSprite = null;
            } else {
                if (this.parent.isSoundOn()) {
                    this.parent.getMainSoundManager().playMatchingSound();
                }
                final List<Line> lines = AlgorithmUtil.drawPaths(points);
                for (Line line : lines) {
                    getChild(2).attachChild(line);
                }
                this.score += 10;
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                }
                final AnimalSprite tmpAnimalSprite = animalSprite;
                this.parent.runOnUpdateThread(new Runnable() {
                    public void run() {
                        PlayingScene.this.selectedShapeSprite.setVisible(false);
                        for (Line line : lines) {
                            PlayingScene.this.getChild(2).detachChild(line);
                        }
                        PlayingScene.this.getChild(1).detachChild(PlayingScene.this.selectedAnimalSprite);
                        PlayingScene.this.getChild(1).detachChild(tmpAnimalSprite);
                        PlayingScene.this.unregisterTouchArea(PlayingScene.this.selectedAnimalSprite);
                        PlayingScene.this.unregisterTouchArea(tmpAnimalSprite);
                        PlayingScene.this.animalValues[tmpAnimalSprite.getXPos()][tmpAnimalSprite.getYPos()] = 0;
                        PlayingScene.this.animalValues[PlayingScene.this.selectedAnimalSprite.getXPos()][PlayingScene.this.selectedAnimalSprite.getYPos()] = 0;
                        PlayingScene.this.animalSprites[tmpAnimalSprite.getXPos() - 1][tmpAnimalSprite.getYPos() - 1] = null;
                        PlayingScene.this.animalSprites[PlayingScene.this.selectedAnimalSprite.getXPos() - 1][PlayingScene.this.selectedAnimalSprite.getYPos() - 1] = null;
                        PlayingScene.this.selectedAnimalSprite = null;
                        if (!AlgorithmUtil.levelComplete(PlayingScene.this.animalValues)) {
                            if (PlayingScene.this.parent.getLevel() > 1) {
                                AlgorithmUtil.fixMatrix(PlayingScene.this.animalValues, PlayingScene.this.animalSprites, PlayingScene.this.parent.getLevel());
                            }
                            if (AlgorithmUtil.isNeedRepair(PlayingScene.this.animalValues)) {
                                AlgorithmUtil.repareMatrix(PlayingScene.this.animalValues, PlayingScene.this.animalSprites);
                            }
                        } else if (PlayingScene.this.parent.getLevel() + 1 > 9) {
                            PlayingScene.this.gameFinish();
                        } else {
                            PlayingScene.this.levelUp();
                        }
                    }
                });
            }
        } else {
            if (this.parent.isSoundOn()) {
                this.parent.getMainSoundManager().playWrongSelectSound();
            }
            this.selectedShapeSprite.setVisible(false);
            this.selectedAnimalSprite = null;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void gameFinish() {
        if (this.parent.isSoundOn()) {
            this.parent.getMainSoundManager().playVictorySound();
        }
        this.parent.runOnUiThread(new Runnable() {
            public void run() {
                PlayingScene.this.running = false;
                PlayingScene playingScene = PlayingScene.this;
                playingScene.score = playingScene.score + (PlayingScene.this.time * 2);
                boolean newHighScore = PlayingScene.this.isNewHighScore(PlayingScene.this.parent.getLevel(), PlayingScene.this.score);
                if (newHighScore) {
                    PlayingScene.this.parent.getSharedPreferences("ANIMAL_PUZZLE", 2).edit().putInt("HIGH_SCORE_LEVEL_" + PlayingScene.this.parent.getLevel(), PlayingScene.this.score).commit();
                }
                int totalScore = 0;
                for (int i = 0; i < 8; i++) {
                    totalScore += PlayingScene.this.parent.getSharedPreferences("ANIMAL_PUZZLE", 1).getInt("HIGH_SCORE_LEVEL_" + (i + 1), 0);
                }
                GameFinish confirmDlg = new GameFinish(PlayingScene.this.parent.getEngine(), PlayingScene.this.parent, totalScore + PlayingScene.this.score, newHighScore);
                confirmDlg.setOkButtonClick(new GameFinish.ButtonClickListerner() {
                    public void onClick() {
                        new LevelScene(2, PlayingScene.this.parent).loadResources(false);
                    }
                });
                PlayingScene.this.childSceneOn = true;
                PlayingScene.this.setChildScene(confirmDlg, false, true, true);
                PlayingScene.this.parent.showAd();
            }
        });
    }

    /* access modifiers changed from: private */
    public void levelUp() {
        if (this.parent.isSoundOn()) {
            this.parent.getMainSoundManager().playVictorySound();
        }
        this.parent.runOnUiThread(new Runnable() {
            public void run() {
                PlayingScene.this.running = false;
                PlayingScene playingScene = PlayingScene.this;
                playingScene.score = playingScene.score + (PlayingScene.this.time * 2);
                boolean newHighScore = PlayingScene.this.isNewHighScore(PlayingScene.this.parent.getLevel(), PlayingScene.this.score);
                if (newHighScore) {
                    PlayingScene.this.parent.getSharedPreferences("ANIMAL_PUZZLE", 2).edit().putInt("HIGH_SCORE_LEVEL_" + PlayingScene.this.parent.getLevel(), PlayingScene.this.score).commit();
                }
                LevelUpScene confirmDlg = new LevelUpScene(PlayingScene.this.parent.getEngine(), PlayingScene.this.parent, PlayingScene.this.score, newHighScore);
                confirmDlg.setOkButtonClick(new LevelUpScene.ButtonClickListerner() {
                    public void onClick() {
                        PlayingScene.this.clearChildScene();
                        PlayingScene.this.parent.runOnUpdateThread(new Runnable() {
                            public void run() {
                                PlayingScene.this.parent.setLevel(PlayingScene.this.parent.getLevel() + 1);
                                PlayingScene.this.clearScene();
                                PlayingScene.this.onLoadScene();
                            }
                        });
                    }
                });
                confirmDlg.setCancelButtonClick(new LevelUpScene.ButtonClickListerner() {
                    public void onClick() {
                        PlayingScene.this.clearChildScene();
                        PlayingScene.this.parent.setLevel(PlayingScene.this.parent.getLevel() + 1);
                        new LevelScene(2, PlayingScene.this.parent).loadResources(false);
                    }
                });
                PlayingScene.this.childSceneOn = true;
                PlayingScene.this.parent.getSharedPreferences("ANIMAL_PUZZLE", 2).edit().putInt("CURRENT_LEVEL", PlayingScene.this.parent.getLevel() + 1).commit();
                PlayingScene.this.setChildScene(confirmDlg, false, true, true);
                PlayingScene.this.parent.showAd();
            }
        });
    }

    /* access modifiers changed from: private */
    public boolean isNewHighScore(int level, int score2) {
        if (score2 > this.parent.getSharedPreferences("ANIMAL_PUZZLE", 1).getInt("HIGH_SCORE_LEVEL_" + level, 0)) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public void unloadScene() {
        Log.d("AnimalPuzzel", "Unload Scene: PlayingScene");
    }

    /* access modifiers changed from: protected */
    public void onLoadComplete() {
    }

    public void onBackPressed() {
        if (!this.childSceneOn) {
            final PauseScene confirmDlg = new PauseScene(this.parent.getEngine(), this.parent);
            confirmDlg.setOkButtonClick(new PauseScene.ButtonClickListerner() {
                public void onClick() {
                    new LevelScene(2, PlayingScene.this.parent).loadResources(false);
                }
            });
            confirmDlg.setCancelButtonClick(new PauseScene.ButtonClickListerner() {
                public void onClick() {
                    PlayingScene.this.clearChildScene();
                    PlayingScene.this.running = true;
                }
            });
            this.parent.runOnUiThread(new Runnable() {
                public void run() {
                    PlayingScene.this.running = false;
                    PlayingScene.this.setChildScene(confirmDlg, false, true, true);
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public void gameOver() {
        if (this.parent.isSoundOn()) {
            this.parent.getMainSoundManager().playFailSound();
        }
        final GameOverScene confirmDlg = new GameOverScene(this.parent.getEngine(), this.parent, "Game over!");
        confirmDlg.setOkButtonClick(new GameOverScene.ButtonClickListerner() {
            public void onClick() {
                PlayingScene.this.reset();
                PlayingScene.this.clearChildScene();
                PlayingScene.this.parent.runOnUpdateThread(new Runnable() {
                    public void run() {
                        PlayingScene.this.clearScene();
                        PlayingScene.this.onLoadScene();
                    }
                });
            }
        });
        confirmDlg.setCancelButtonClick(new GameOverScene.ButtonClickListerner() {
            public void onClick() {
                new LevelScene(2, PlayingScene.this.parent).loadResources(false);
            }
        });
        this.parent.runOnUiThread(new Runnable() {
            public void run() {
                PlayingScene.this.running = false;
                PlayingScene.this.childSceneOn = true;
                PlayingScene.this.setChildScene(confirmDlg, false, true, true);
                PlayingScene.this.parent.showAd();
            }
        });
    }

    /* access modifiers changed from: private */
    public void clearScene() {
        clearEntityModifiers();
        clearTouchAreas();
        clearUpdateHandlers();
        getChild(1).detachChildren();
        getChild(2).detachChildren();
        getChild(3).detachChildren();
        this.running = false;
        this.childSceneOn = false;
    }
}
