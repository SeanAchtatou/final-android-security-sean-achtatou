package com.tani.game.animalpuzzle.scene;

import com.tani.game.animalpuzzle.activity.MainActivity;
import com.tani.game.base.AsyncTaskLoader;
import com.tani.game.base.BaseGameScene;
import com.tani.game.base.IAsyncCallback;
import java.util.ArrayList;
import java.util.List;
import org.anddev.andengine.entity.scene.background.EntityBackground;
import org.anddev.andengine.entity.sprite.Sprite;
import org.anddev.andengine.input.touch.TouchEvent;
import org.anddev.andengine.opengl.texture.Texture;
import org.anddev.andengine.opengl.texture.TextureOptions;
import org.anddev.andengine.opengl.texture.region.TextureRegion;
import org.anddev.andengine.opengl.texture.region.TextureRegionFactory;

public class GuideScene extends BaseGameScene {
    IAsyncCallback callback;
    /* access modifiers changed from: private */
    public TextureRegion guide1TextureRegion;
    /* access modifiers changed from: private */
    public TextureRegion guide2TextureRegion;
    /* access modifiers changed from: private */
    public TextureRegion guide3TextureRegion;
    /* access modifiers changed from: private */
    public TextureRegion guideBgTextureRegion;
    /* access modifiers changed from: private */
    public int guideIdx = 0;
    /* access modifiers changed from: private */
    public Sprite[] guideSprites = new Sprite[3];
    /* access modifiers changed from: private */
    public TextureRegion guideTextTextureRegion;
    List<Texture> loadedTextures = new ArrayList();
    /* access modifiers changed from: private */
    public TextureRegion nextBtnTextureRegion;
    /* access modifiers changed from: private */
    public Sprite nextSprite;
    /* access modifiers changed from: private */
    public TextureRegion okBtnTextureRegion;
    /* access modifiers changed from: private */
    public Sprite okSprite;

    public GuideScene(int pLayerCount, MainActivity parent) {
        super(pLayerCount, parent);
    }

    /* access modifiers changed from: protected */
    public void onLoadResources() {
        TextureRegionFactory.setAssetBasePath("images/guide/");
        this.callback = new IAsyncCallback() {
            public void workToDo() {
                Texture menuTexture = new Texture(512, 512, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
                GuideScene.this.guideBgTextureRegion = TextureRegionFactory.createFromAsset(menuTexture, GuideScene.this.parent, "guide_bg.png", 0, 0);
                GuideScene.this.parent.getEngine().getTextureManager().loadTexture(menuTexture);
                GuideScene.this.loadedTextures.add(menuTexture);
                Texture guide1Texture = new Texture(256, 256, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
                GuideScene.this.guide1TextureRegion = TextureRegionFactory.createFromAsset(guide1Texture, GuideScene.this.parent, "guide1.png", 0, 0);
                GuideScene.this.parent.getEngine().getTextureManager().loadTexture(guide1Texture);
                GuideScene.this.loadedTextures.add(guide1Texture);
                Texture guide2Texture = new Texture(256, 256, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
                GuideScene.this.guide2TextureRegion = TextureRegionFactory.createFromAsset(guide2Texture, GuideScene.this.parent, "guide2.png", 0, 0);
                GuideScene.this.parent.getEngine().getTextureManager().loadTexture(guide2Texture);
                GuideScene.this.loadedTextures.add(guide2Texture);
                Texture guide3Texture = new Texture(256, 256, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
                GuideScene.this.guide3TextureRegion = TextureRegionFactory.createFromAsset(guide3Texture, GuideScene.this.parent, "guide3.png", 0, 0);
                GuideScene.this.parent.getEngine().getTextureManager().loadTexture(guide3Texture);
                GuideScene.this.loadedTextures.add(guide3Texture);
                Texture okBtnTexture = new Texture(128, 128, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
                GuideScene.this.okBtnTextureRegion = TextureRegionFactory.createFromAsset(okBtnTexture, GuideScene.this.parent, "ok.png", 0, 0);
                GuideScene.this.parent.getEngine().getTextureManager().loadTexture(okBtnTexture);
                GuideScene.this.loadedTextures.add(okBtnTexture);
                Texture nextBtnTexture = new Texture(128, 128, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
                GuideScene.this.nextBtnTextureRegion = TextureRegionFactory.createFromAsset(nextBtnTexture, GuideScene.this.parent, "next.png", 0, 0);
                GuideScene.this.parent.getEngine().getTextureManager().loadTexture(nextBtnTexture);
                GuideScene.this.loadedTextures.add(nextBtnTexture);
                Texture guideTextTexture = new Texture(512, 512, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
                GuideScene.this.guideTextTextureRegion = TextureRegionFactory.createFromAsset(guideTextTexture, GuideScene.this.parent, "guideText.png", 0, 0);
                GuideScene.this.parent.getEngine().getTextureManager().loadTexture(guideTextTexture);
                GuideScene.this.loadedTextures.add(guideTextTexture);
            }

            public void onComplete() {
                while (GuideScene.this.loadedTextures.size() > 0) {
                    if (GuideScene.this.loadedTextures.get(0).isLoadedToHardware()) {
                        GuideScene.this.loadedTextures.remove(0);
                    } else {
                        try {
                            Thread.sleep(10);
                        } catch (InterruptedException e) {
                        }
                    }
                }
                GuideScene.this.onLoadScene();
            }
        };
        this.parent.runOnUiThread(new Runnable() {
            public void run() {
                new AsyncTaskLoader().execute(GuideScene.this.callback);
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onLoadScene() {
        setBackground(new EntityBackground(new Sprite(0.0f, 0.0f, this.guideBgTextureRegion)));
        getLastChild().attachChild(new Sprite(80.0f, 70.0f, this.guideTextTextureRegion));
        Sprite sprite1 = new Sprite(130.0f, 140.0f, this.guide1TextureRegion);
        Sprite sprite2 = new Sprite(130.0f, 140.0f, this.guide2TextureRegion);
        Sprite sprite3 = new Sprite(130.0f, 140.0f, this.guide3TextureRegion);
        this.guideSprites[0] = sprite1;
        this.guideSprites[1] = sprite2;
        this.guideSprites[2] = sprite3;
        getLastChild().attachChild(this.guideSprites[0]);
        this.okSprite = new Sprite(320.0f, 180.0f, this.okBtnTextureRegion) {
            public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                GuideScene.this.parent.vibrate();
                new MenuScene(2, GuideScene.this.parent).loadResources(false);
                return false;
            }
        };
        this.nextSprite = new Sprite(320.0f, 180.0f, this.nextBtnTextureRegion) {
            public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                GuideScene.this.parent.vibrate();
                GuideScene.this.parent.runOnUiThread(new Runnable() {
                    public void run() {
                        GuideScene access$0 = GuideScene.this;
                        access$0.guideIdx = access$0.guideIdx + 1;
                        if (GuideScene.this.guideIdx == 2) {
                            GuideScene.this.getLastChild().detachChild(GuideScene.this.nextSprite);
                            GuideScene.this.unregisterTouchArea(GuideScene.this.nextSprite);
                            GuideScene.this.getLastChild().attachChild(GuideScene.this.okSprite);
                            GuideScene.this.registerTouchArea(GuideScene.this.okSprite);
                        }
                        GuideScene.this.getLastChild().detachChild(GuideScene.this.guideSprites[GuideScene.this.guideIdx - 1]);
                        GuideScene.this.getLastChild().attachChild(GuideScene.this.guideSprites[GuideScene.this.guideIdx]);
                    }
                });
                return false;
            }
        };
        getLastChild().attachChild(this.nextSprite);
        registerTouchArea(this.nextSprite);
        this.parent.setScene(this);
        this.parent.showAd();
    }

    /* access modifiers changed from: protected */
    public void unloadScene() {
    }

    /* access modifiers changed from: protected */
    public void onLoadComplete() {
    }

    public void onBackPressed() {
        new MenuScene(2, this.parent).loadResources(false);
    }
}
