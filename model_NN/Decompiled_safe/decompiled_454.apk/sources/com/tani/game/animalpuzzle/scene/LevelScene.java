package com.tani.game.animalpuzzle.scene;

import com.tani.animalpuzzle.res.LevelResource;
import com.tani.game.animalpuzzle.activity.MainActivity;
import com.tani.game.base.AsyncTaskLoader;
import com.tani.game.base.BaseGameScene;
import com.tani.game.base.IAsyncCallback;
import org.anddev.andengine.entity.scene.background.EntityBackground;
import org.anddev.andengine.entity.sprite.Sprite;
import org.anddev.andengine.entity.text.Text;
import org.anddev.andengine.input.touch.TouchEvent;
import org.anddev.andengine.opengl.font.Font;
import org.anddev.andengine.opengl.font.FontFactory;
import org.anddev.andengine.opengl.texture.Texture;
import org.anddev.andengine.opengl.texture.TextureOptions;
import org.anddev.andengine.opengl.texture.region.TextureRegion;
import org.anddev.andengine.opengl.texture.region.TextureRegionFactory;
import org.anddev.andengine.util.HorizontalAlign;

public class LevelScene extends BaseGameScene {
    IAsyncCallback callback;
    /* access modifiers changed from: private */
    public Texture fontTexture = null;
    /* access modifiers changed from: private */
    public LevelResource levelResource = null;
    /* access modifiers changed from: private */
    public Font mFont;

    public LevelScene(int pLayerCount, MainActivity parent) {
        super(pLayerCount, parent);
        this.levelResource = parent.getLevelResource();
    }

    /* access modifiers changed from: protected */
    public void onLoadResources() {
        this.callback = new IAsyncCallback() {
            public void workToDo() {
                if (!LevelScene.this.parent.isLevelResourceLoaded()) {
                    TextureRegionFactory.setAssetBasePath("images/level/");
                    LevelScene.this.levelResource.bkgTexture = new Texture(512, 512, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
                    LevelScene.this.levelResource.playingBgTextureRegion = TextureRegionFactory.createFromAsset(LevelScene.this.levelResource.bkgTexture, LevelScene.this.parent, "level_bg.png", 0, 0);
                    LevelScene.this.parent.getEngine().getTextureManager().loadTexture(LevelScene.this.levelResource.bkgTexture);
                    LevelScene.this.levelResource.lvlLockTexture = new Texture(128, 128, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
                    LevelScene.this.levelResource.lvlLockRegion = TextureRegionFactory.createFromAsset(LevelScene.this.levelResource.lvlLockTexture, LevelScene.this.parent, "lvl_lock.png", 0, 0);
                    LevelScene.this.parent.getEngine().getTextureManager().loadTexture(LevelScene.this.levelResource.lvlLockTexture);
                    LevelScene.this.levelResource.lvlUnLockTexture = new Texture(256, 256, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
                    LevelScene.this.levelResource.lvlUnLockRegions = new TextureRegion[9];
                    TextureRegionFactory.createFromAsset(LevelScene.this.levelResource.lvlUnLockTexture, LevelScene.this.parent, "unlock_lvls.png", 0, 0);
                    for (int i = 0; i < 9; i++) {
                        LevelScene.this.levelResource.lvlUnLockRegions[i] = TextureRegionFactory.extractFromTexture(LevelScene.this.levelResource.lvlUnLockTexture, (i % 3) * 65, (i / 3) * 65, 65, 65);
                    }
                    LevelScene.this.parent.getEngine().getTextureManager().loadTexture(LevelScene.this.levelResource.lvlUnLockTexture);
                    LevelScene.this.parent.setLevelResourceLoaded(true);
                }
                FontFactory.setAssetBasePath("font/");
                LevelScene.this.fontTexture = new Texture(512, 512, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
                LevelScene.this.mFont = FontFactory.createFromAsset(LevelScene.this.fontTexture, LevelScene.this.parent, "Plok.ttf", 18.0f, true, -65536);
                LevelScene.this.parent.getEngine().getTextureManager().loadTexture(LevelScene.this.fontTexture);
                LevelScene.this.parent.getEngine().getFontManager().loadFont(LevelScene.this.mFont);
            }

            public void onComplete() {
                LevelScene.this.parent.getEngine().getTextureManager().reloadTextures();
                LevelScene.this.onLoadScene();
            }
        };
        this.parent.runOnUiThread(new Runnable() {
            public void run() {
                new AsyncTaskLoader().execute(LevelScene.this.callback);
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onLoadScene() {
        setBackground(new EntityBackground(new Sprite(0.0f, 0.0f, this.levelResource.playingBgTextureRegion)));
        int level = this.parent.getSharedPreferences("ANIMAL_PUZZLE", 1).getInt("CURRENT_LEVEL", 1);
        int x = 125;
        int y = 50;
        for (int i = 0; i < level; i++) {
            final int curLevel = i;
            Sprite lvlUnlockSprite = new Sprite((float) x, (float) y, this.levelResource.lvlUnLockRegions[i]) {
                public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                    if (pSceneTouchEvent.isActionDown()) {
                        LevelScene.this.parent.vibrate();
                        return true;
                    } else if (pSceneTouchEvent.getAction() != 1) {
                        return super.onAreaTouched(pSceneTouchEvent, pTouchAreaLocalX, pTouchAreaLocalY);
                    } else {
                        LevelScene.this.parent.setLevel(curLevel + 1);
                        LevelScene.this.getLastChild().attachChild(new Text(300.0f, 10.0f, LevelScene.this.mFont, "Loading...", HorizontalAlign.CENTER));
                        new PlayingScene(4, LevelScene.this.parent).loadResources(false);
                        return false;
                    }
                }
            };
            getLastChild().attachChild(lvlUnlockSprite);
            registerTouchArea(lvlUnlockSprite);
            x += 75;
            if (i > 0 && i % 3 == 2) {
                y += 70;
                x = 125;
            }
        }
        for (int i2 = level; i2 < 9; i2++) {
            getLastChild().attachChild(new Sprite((float) x, (float) y, this.levelResource.lvlLockRegion));
            int x2 = x + 75;
            if (i2 > 0 && i2 % 3 == 2) {
                y += 70;
                x2 = 125;
            }
        }
        this.parent.setScene(this);
        this.parent.showAd();
    }

    /* access modifiers changed from: protected */
    public void unloadScene() {
    }

    /* access modifiers changed from: protected */
    public void onLoadComplete() {
    }

    public void onBackPressed() {
        new MenuScene(2, this.parent).loadResources(false);
    }
}
