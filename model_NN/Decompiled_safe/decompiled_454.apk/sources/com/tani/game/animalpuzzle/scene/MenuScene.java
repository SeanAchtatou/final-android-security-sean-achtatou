package com.tani.game.animalpuzzle.scene;

import com.tani.animalpuzzle.res.CommonResource;
import com.tani.animalpuzzle.res.MenuResource;
import com.tani.game.animalpuzzle.activity.MainActivity;
import com.tani.game.base.AsyncTaskLoader;
import com.tani.game.base.BaseGameScene;
import com.tani.game.base.IAsyncCallback;
import org.anddev.andengine.entity.scene.background.EntityBackground;
import org.anddev.andengine.entity.sprite.Sprite;
import org.anddev.andengine.input.touch.TouchEvent;
import org.anddev.andengine.opengl.font.FontFactory;
import org.anddev.andengine.opengl.texture.Texture;
import org.anddev.andengine.opengl.texture.TextureOptions;
import org.anddev.andengine.opengl.texture.region.TextureRegionFactory;

public class MenuScene extends BaseGameScene {
    IAsyncCallback callback;
    /* access modifiers changed from: private */
    public MenuResource menuResource = null;
    /* access modifiers changed from: private */
    public Sprite soundOffSprite;
    /* access modifiers changed from: private */
    public Sprite soundOnSprite;

    public MenuScene(int pLayerCount, MainActivity parent) {
        super(pLayerCount, parent);
        this.menuResource = parent.getMenuResource();
    }

    /* access modifiers changed from: protected */
    public void onLoadResources() {
        TextureRegionFactory.setAssetBasePath("images/menu/");
        this.callback = new IAsyncCallback() {
            public void workToDo() {
                if (!MenuScene.this.parent.isMenuResoureLoaded()) {
                    MenuScene.this.menuResource.bkgTexture = new Texture(512, 512, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
                    MenuScene.this.menuResource.playingBgTextureRegion = TextureRegionFactory.createFromAsset(MenuScene.this.menuResource.bkgTexture, MenuScene.this.parent, "menu_bg.png", 0, 0);
                    MenuScene.this.parent.getEngine().getTextureManager().loadTexture(MenuScene.this.menuResource.bkgTexture);
                    MenuScene.this.menuResource.btnTexture = new Texture(128, 128, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
                    TextureRegionFactory.createFromAsset(MenuScene.this.menuResource.btnTexture, MenuScene.this.parent, "play_guide.png", 0, 0);
                    MenuScene.this.menuResource.playBtnRegion = TextureRegionFactory.extractFromTexture(MenuScene.this.menuResource.btnTexture, 0, 0, 70, 40);
                    MenuScene.this.menuResource.guideBtnRegion = TextureRegionFactory.extractFromTexture(MenuScene.this.menuResource.btnTexture, 0, 40, 70, 40);
                    MenuScene.this.parent.getEngine().getTextureManager().loadTexture(MenuScene.this.menuResource.btnTexture);
                    Texture scoreTexture = new Texture(128, 128, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
                    MenuScene.this.menuResource.scoreBtnRegion = TextureRegionFactory.createFromAsset(scoreTexture, MenuScene.this.parent, "score_btn.png", 0, 0);
                    MenuScene.this.parent.getEngine().getTextureManager().loadTexture(scoreTexture);
                    MenuScene.this.menuResource.soundTexture = new Texture(64, 128, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
                    TextureRegionFactory.createFromAsset(MenuScene.this.menuResource.soundTexture, MenuScene.this.parent, "sound.png", 0, 0);
                    MenuScene.this.menuResource.soundOnRegion = TextureRegionFactory.extractFromTexture(MenuScene.this.menuResource.soundTexture, 0, 0, 50, 50);
                    MenuScene.this.menuResource.soundOffRegion = TextureRegionFactory.extractFromTexture(MenuScene.this.menuResource.soundTexture, 0, 50, 50, 50);
                    MenuScene.this.parent.getEngine().getTextureManager().loadTexture(MenuScene.this.menuResource.soundTexture);
                    MenuScene.this.parent.setMenuResoureLoaded(true);
                }
                if (!MenuScene.this.parent.isCommonResourceLoaded()) {
                    CommonResource commonResource = MenuScene.this.parent.getCommonResource();
                    TextureRegionFactory.setAssetBasePath("images/menu/");
                    commonResource.dlgBkgTexture = new Texture(512, 256, TextureOptions.DEFAULT);
                    commonResource.dlgBkgRegion = TextureRegionFactory.createFromAsset(commonResource.dlgBkgTexture, MenuScene.this.parent, "dlg_bkg.png", 0, 0);
                    MenuScene.this.parent.getEngine().getTextureManager().loadTexture(commonResource.dlgBkgTexture);
                    commonResource.newHighScoreTexture = new Texture(256, 256, TextureOptions.DEFAULT);
                    commonResource.newHighScoreRegion = TextureRegionFactory.createFromAsset(commonResource.newHighScoreTexture, MenuScene.this.parent, "newHighScore.png", 0, 0);
                    MenuScene.this.parent.getEngine().getTextureManager().loadTexture(commonResource.newHighScoreTexture);
                    FontFactory.setAssetBasePath("font/");
                    commonResource.scoreFontTexture = new Texture(256, 256, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
                    commonResource.scoreFont = FontFactory.createFromAsset(commonResource.scoreFontTexture, MenuScene.this.parent, "Droid.ttf", 24.0f, true, -16776961);
                    MenuScene.this.parent.getEngine().getTextureManager().loadTexture(commonResource.scoreFontTexture);
                    MenuScene.this.parent.getEngine().getFontManager().loadFont(commonResource.scoreFont);
                    commonResource.dlgBtnTexture = new Texture(256, 128, TextureOptions.DEFAULT);
                    TextureRegionFactory.createFromAsset(commonResource.dlgBtnTexture, MenuScene.this.parent, "dlg_btn.png", 0, 0);
                    commonResource.okBtnRegion = TextureRegionFactory.extractFromTexture(commonResource.dlgBtnTexture, 0, 0, 70, 40);
                    commonResource.cancelBtnRegion = TextureRegionFactory.extractFromTexture(commonResource.dlgBtnTexture, 70, 0, 70, 40);
                    commonResource.menuBtnRegion = TextureRegionFactory.extractFromTexture(commonResource.dlgBtnTexture, 0, 40, 70, 40);
                    commonResource.nextBtnRegion = TextureRegionFactory.extractFromTexture(commonResource.dlgBtnTexture, 70, 40, 70, 40);
                    commonResource.replayBtnRegion = TextureRegionFactory.extractFromTexture(commonResource.dlgBtnTexture, 0, 80, 70, 40);
                    MenuScene.this.parent.getEngine().getTextureManager().loadTexture(commonResource.dlgBtnTexture);
                    commonResource.messageTexture = new Texture(256, 128, TextureOptions.DEFAULT);
                    TextureRegionFactory.createFromAsset(commonResource.messageTexture, MenuScene.this.parent, "message.png", 0, 0);
                    commonResource.gameOverMsgRegion = TextureRegionFactory.extractFromTexture(commonResource.messageTexture, 0, 0, 100, 30);
                    commonResource.levelCompletedMsgRegion = TextureRegionFactory.extractFromTexture(commonResource.messageTexture, 100, 0, 156, 30);
                    commonResource.gameFinishMsgRegion = TextureRegionFactory.extractFromTexture(commonResource.messageTexture, 0, 30, 256, 30);
                    commonResource.exitConfirmMsgRegion = TextureRegionFactory.extractFromTexture(commonResource.messageTexture, 0, 60, 256, 30);
                    MenuScene.this.parent.getEngine().getTextureManager().loadTexture(commonResource.messageTexture);
                    MenuScene.this.parent.setCommonResourceLoaded(true);
                }
            }

            public void onComplete() {
                MenuScene.this.parent.getEngine().getTextureManager().reloadTextures();
                MenuScene.this.onLoadScene();
            }
        };
        this.parent.runOnUiThread(new Runnable() {
            public void run() {
                new AsyncTaskLoader().execute(MenuScene.this.callback);
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onLoadScene() {
        setBackground(new EntityBackground(new Sprite(0.0f, 0.0f, this.menuResource.playingBgTextureRegion)));
        Sprite playSprite = new Sprite(30.0f, 150.0f, this.menuResource.playBtnRegion) {
            public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                MenuScene.this.parent.vibrate();
                new LevelScene(2, MenuScene.this.parent).loadResources(false);
                return false;
            }
        };
        getLastChild().attachChild(playSprite);
        registerTouchArea(playSprite);
        Sprite guideSprite = new Sprite(30.0f, 200.0f, this.menuResource.guideBtnRegion) {
            public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                MenuScene.this.parent.vibrate();
                new GuideScene(2, MenuScene.this.parent).loadResources(false);
                return false;
            }
        };
        getLastChild().attachChild(guideSprite);
        registerTouchArea(guideSprite);
        Sprite scoreSprite = new Sprite(120.0f, 200.0f, this.menuResource.scoreBtnRegion) {
            public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                MenuScene.this.parent.vibrate();
                new ScoreScene(3, MenuScene.this.parent).loadResources(false);
                return false;
            }
        };
        getLastChild().attachChild(scoreSprite);
        registerTouchArea(scoreSprite);
        this.soundOnSprite = new Sprite(425.0f, 10.0f, this.menuResource.soundOnRegion) {
            public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                MenuScene.this.postRunnable(new Runnable() {
                    public void run() {
                        MenuScene.this.parent.setSoundOn(false);
                        MenuScene.this.parent.getMainSoundManager().stopBgSound();
                        MenuScene.this.getLastChild().detachChild(MenuScene.this.soundOnSprite);
                        MenuScene.this.unregisterTouchArea(MenuScene.this.soundOnSprite);
                        MenuScene.this.getLastChild().attachChild(MenuScene.this.soundOffSprite);
                        MenuScene.this.registerTouchArea(MenuScene.this.soundOffSprite);
                    }
                });
                return false;
            }
        };
        this.soundOffSprite = new Sprite(425.0f, 10.0f, this.menuResource.soundOffRegion) {
            public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                MenuScene.this.postRunnable(new Runnable() {
                    public void run() {
                        MenuScene.this.parent.setSoundOn(true);
                        MenuScene.this.parent.getMainSoundManager().playBgSound();
                        MenuScene.this.getLastChild().detachChild(MenuScene.this.soundOffSprite);
                        MenuScene.this.unregisterTouchArea(MenuScene.this.soundOffSprite);
                        MenuScene.this.getLastChild().attachChild(MenuScene.this.soundOnSprite);
                        MenuScene.this.registerTouchArea(MenuScene.this.soundOnSprite);
                    }
                });
                return false;
            }
        };
        if (this.parent.isSoundOn()) {
            this.parent.getMainSoundManager().playBgSound();
            getLastChild().attachChild(this.soundOnSprite);
            registerTouchArea(this.soundOnSprite);
        }
        this.parent.setScene(this);
        this.parent.showAd();
    }

    /* access modifiers changed from: protected */
    public void unloadScene() {
    }

    /* access modifiers changed from: protected */
    public void onLoadComplete() {
    }

    public void onBackPressed() {
        this.parent.finish();
    }
}
