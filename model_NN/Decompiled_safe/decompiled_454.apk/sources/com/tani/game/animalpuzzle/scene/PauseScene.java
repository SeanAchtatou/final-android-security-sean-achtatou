package com.tani.game.animalpuzzle.scene;

import android.content.Context;
import com.tani.animalpuzzle.res.CommonResource;
import com.tani.game.animalpuzzle.activity.MainActivity;
import org.anddev.andengine.engine.Engine;
import org.anddev.andengine.entity.modifier.ScaleModifier;
import org.anddev.andengine.entity.sprite.Sprite;
import org.anddev.andengine.input.touch.TouchEvent;

public class PauseScene extends BaseDialogScene {
    /* access modifiers changed from: private */
    public ButtonClickListerner cancelButtonClick = null;
    private CommonResource commonResource = null;
    /* access modifiers changed from: private */
    public ButtonClickListerner okButtonClick = null;

    public interface ButtonClickListerner {
        void onClick();
    }

    public PauseScene(Engine engine, Context context) {
        super(engine, context);
        this.commonResource = ((MainActivity) context).getCommonResource();
        loadResource();
        getChild(0).attachChild(new Sprite(90.0f, 60.0f, this.commonResource.dlgBkgRegion));
        initScene();
    }

    /* access modifiers changed from: protected */
    public void loadResource() {
    }

    /* access modifiers changed from: protected */
    public void initScene() {
        Sprite btnOk = new Sprite(125.0f, 180.0f, this.commonResource.okBtnRegion) {
            public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                if (pSceneTouchEvent.isActionDown()) {
                    ((MainActivity) PauseScene.this.context).vibrate();
                    registerEntityModifier(new ScaleModifier(0.1f, 1.0f, 1.5f));
                    return true;
                }
                if (pSceneTouchEvent.getAction() == 1) {
                    clearEntityModifiers();
                    reset();
                    if (PauseScene.this.okButtonClick != null) {
                        PauseScene.this.okButtonClick.onClick();
                        return false;
                    }
                }
                return super.onAreaTouched(pSceneTouchEvent, pTouchAreaLocalX, pTouchAreaLocalY);
            }
        };
        getLastChild().attachChild(btnOk);
        registerTouchArea(btnOk);
        Sprite btnCancel = new Sprite(280.0f, 180.0f, this.commonResource.cancelBtnRegion) {
            public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                if (pSceneTouchEvent.isActionDown()) {
                    ((MainActivity) PauseScene.this.context).vibrate();
                    registerEntityModifier(new ScaleModifier(0.1f, 1.0f, 1.5f));
                    return true;
                }
                if (pSceneTouchEvent.getAction() == 1) {
                    clearEntityModifiers();
                    reset();
                    if (PauseScene.this.cancelButtonClick != null) {
                        PauseScene.this.cancelButtonClick.onClick();
                        return false;
                    }
                }
                return super.onAreaTouched(pSceneTouchEvent, pTouchAreaLocalX, pTouchAreaLocalY);
            }
        };
        getLastChild().attachChild(btnCancel);
        registerTouchArea(btnCancel);
        getLastChild().attachChild(new Sprite(110.0f, 100.0f, this.commonResource.exitConfirmMsgRegion));
    }

    public void destroyRes() {
    }

    public ButtonClickListerner getOkButtonClick() {
        return this.okButtonClick;
    }

    public void setOkButtonClick(ButtonClickListerner okButtonClick2) {
        this.okButtonClick = okButtonClick2;
    }

    public ButtonClickListerner getCancelButtonClick() {
        return this.cancelButtonClick;
    }

    public void setCancelButtonClick(ButtonClickListerner cancelButtonClick2) {
        this.cancelButtonClick = cancelButtonClick2;
    }
}
