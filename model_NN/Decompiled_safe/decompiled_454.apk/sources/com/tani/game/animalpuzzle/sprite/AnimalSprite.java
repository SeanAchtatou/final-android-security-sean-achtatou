package com.tani.game.animalpuzzle.sprite;

import org.anddev.andengine.entity.sprite.Sprite;
import org.anddev.andengine.opengl.texture.region.TextureRegion;

public class AnimalSprite extends Sprite {
    private int xPos;
    private int yPos;

    public AnimalSprite(float pX, float pY, TextureRegion pTextureRegion) {
        super(pX, pY, pTextureRegion);
    }

    public int getXPos() {
        return this.xPos;
    }

    public void setXPos(int xPos2) {
        this.xPos = xPos2;
    }

    public int getYPos() {
        return this.yPos;
    }

    public void setYPos(int yPos2) {
        this.yPos = yPos2;
    }
}
