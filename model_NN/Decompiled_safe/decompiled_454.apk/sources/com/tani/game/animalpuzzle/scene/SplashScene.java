package com.tani.game.animalpuzzle.scene;

import com.tani.game.animalpuzzle.activity.MainActivity;
import com.tani.game.base.BaseGameScene;
import org.anddev.andengine.engine.handler.timer.ITimerCallback;
import org.anddev.andengine.engine.handler.timer.TimerHandler;
import org.anddev.andengine.entity.scene.background.ColorBackground;
import org.anddev.andengine.entity.sprite.Sprite;
import org.anddev.andengine.opengl.texture.Texture;
import org.anddev.andengine.opengl.texture.TextureOptions;
import org.anddev.andengine.opengl.texture.region.TextureRegion;
import org.anddev.andengine.opengl.texture.region.TextureRegionFactory;

public class SplashScene extends BaseGameScene {
    /* access modifiers changed from: private */
    public boolean loaded = false;
    private Texture loadingTexture;
    private TextureRegion splashBgTextureRegion;

    public SplashScene(int pLayerCount, MainActivity parent) {
        super(pLayerCount, parent);
    }

    /* access modifiers changed from: protected */
    public void onLoadResources() {
        TextureRegionFactory.setAssetBasePath("images/base/");
        this.loadingTexture = new Texture(512, 512, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.splashBgTextureRegion = TextureRegionFactory.createFromAsset(this.loadingTexture, this.parent, "tani.png", 0, 0);
        this.parent.getEngine().getTextureManager().loadTexture(this.loadingTexture);
        this.loaded = true;
    }

    /* access modifiers changed from: protected */
    public void onLoadScene() {
        setBackground(new ColorBackground(1.0f, 1.0f, 1.0f));
        getLastChild().attachChild(new Sprite(0.0f, 0.0f, this.splashBgTextureRegion));
        registerUpdateHandler(new TimerHandler(2.0f, new ITimerCallback() {
            public void onTimePassed(TimerHandler pTimerHandler) {
                if (SplashScene.this.loaded) {
                    SplashScene.this.parent.getEngine().getTextureManager().reloadTextures();
                    new MenuScene(2, SplashScene.this.parent).loadResources(false);
                }
            }
        }));
        this.parent.setScene(this);
    }

    /* access modifiers changed from: protected */
    public void unloadScene() {
    }

    /* access modifiers changed from: protected */
    public void onLoadComplete() {
    }

    public void onBackPressed() {
        this.parent.finish();
    }
}
