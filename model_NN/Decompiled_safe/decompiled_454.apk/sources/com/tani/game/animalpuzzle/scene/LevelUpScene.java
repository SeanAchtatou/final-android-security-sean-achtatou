package com.tani.game.animalpuzzle.scene;

import android.content.Context;
import com.tani.animalpuzzle.res.CommonResource;
import com.tani.game.animalpuzzle.activity.MainActivity;
import org.anddev.andengine.engine.Engine;
import org.anddev.andengine.entity.IEntity;
import org.anddev.andengine.entity.modifier.IEntityModifier;
import org.anddev.andengine.entity.modifier.MoveModifier;
import org.anddev.andengine.entity.modifier.RotationModifier;
import org.anddev.andengine.entity.modifier.ScaleModifier;
import org.anddev.andengine.entity.modifier.SequenceEntityModifier;
import org.anddev.andengine.entity.sprite.Sprite;
import org.anddev.andengine.entity.text.Text;
import org.anddev.andengine.input.touch.TouchEvent;
import org.anddev.andengine.util.modifier.IModifier;
import org.anddev.andengine.util.modifier.ease.EaseBounceIn;

public class LevelUpScene extends BaseDialogScene {
    /* access modifiers changed from: private */
    public ButtonClickListerner cancelButtonClick = null;
    private CommonResource commonResource = null;
    /* access modifiers changed from: private */
    public boolean isNewHighScore;
    /* access modifiers changed from: private */
    public ButtonClickListerner okButtonClick = null;
    private int score;

    public interface ButtonClickListerner {
        void onClick();
    }

    public LevelUpScene(Engine engine, Context context, int score2, boolean isNewHighScore2) {
        super(engine, context);
        this.commonResource = ((MainActivity) context).getCommonResource();
        this.score = score2;
        this.isNewHighScore = isNewHighScore2;
        loadResource();
        getChild(0).attachChild(new Sprite(90.0f, 60.0f, this.commonResource.dlgBkgRegion));
        initScene();
    }

    /* access modifiers changed from: protected */
    public void loadResource() {
    }

    /* access modifiers changed from: protected */
    public void initScene() {
        final Sprite btnOk = new Sprite(125.0f, 180.0f, this.commonResource.nextBtnRegion) {
            public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                if (pSceneTouchEvent.isActionDown()) {
                    ((MainActivity) LevelUpScene.this.context).vibrate();
                    registerEntityModifier(new ScaleModifier(0.1f, 1.0f, 1.5f));
                    return true;
                }
                if (pSceneTouchEvent.getAction() == 1) {
                    clearEntityModifiers();
                    reset();
                    if (LevelUpScene.this.okButtonClick != null) {
                        LevelUpScene.this.okButtonClick.onClick();
                        return false;
                    }
                }
                return super.onAreaTouched(pSceneTouchEvent, pTouchAreaLocalX, pTouchAreaLocalY);
            }
        };
        final Sprite btnCancel = new Sprite(280.0f, 180.0f, this.commonResource.menuBtnRegion) {
            public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                if (pSceneTouchEvent.isActionDown()) {
                    ((MainActivity) LevelUpScene.this.context).vibrate();
                    registerEntityModifier(new ScaleModifier(0.1f, 1.0f, 1.5f));
                    return true;
                }
                if (pSceneTouchEvent.getAction() == 1) {
                    clearEntityModifiers();
                    reset();
                    if (LevelUpScene.this.cancelButtonClick != null) {
                        LevelUpScene.this.cancelButtonClick.onClick();
                        return false;
                    }
                }
                return super.onAreaTouched(pSceneTouchEvent, pTouchAreaLocalX, pTouchAreaLocalY);
            }
        };
        final Sprite newHighScore = new Sprite(230.0f, 40.0f, this.commonResource.newHighScoreRegion);
        final Text scoreText = new Text(140.0f, 140.0f, this.commonResource.scoreFont, "Score:" + this.score);
        Sprite msg = new Sprite(110.0f, 230.0f, this.commonResource.levelCompletedMsgRegion);
        getLastChild().attachChild(msg);
        msg.registerEntityModifier(new SequenceEntityModifier(new IEntityModifier.IEntityModifierListener() {
            public /* bridge */ /* synthetic */ void onModifierFinished(IModifier iModifier, Object obj) {
                onModifierFinished((IModifier<IEntity>) iModifier, (IEntity) obj);
            }

            public void onModifierFinished(IModifier<IEntity> iModifier, IEntity pItem) {
                LevelUpScene.this.getLastChild().attachChild(scoreText);
                if (LevelUpScene.this.isNewHighScore) {
                    LevelUpScene.this.getLastChild().attachChild(newHighScore);
                }
                LevelUpScene.this.getLastChild().attachChild(btnOk);
                LevelUpScene.this.registerTouchArea(btnOk);
                LevelUpScene.this.getLastChild().attachChild(btnCancel);
                LevelUpScene.this.registerTouchArea(btnCancel);
            }
        }, new MoveModifier(2.0f, 110.0f, 110.0f, 230.0f, 100.0f, EaseBounceIn.getInstance()), new RotationModifier(2.0f, 0.0f, 360.0f)));
    }

    public void destroyRes() {
    }

    public ButtonClickListerner getOkButtonClick() {
        return this.okButtonClick;
    }

    public void setOkButtonClick(ButtonClickListerner okButtonClick2) {
        this.okButtonClick = okButtonClick2;
    }

    public ButtonClickListerner getCancelButtonClick() {
        return this.cancelButtonClick;
    }

    public void setCancelButtonClick(ButtonClickListerner cancelButtonClick2) {
        this.cancelButtonClick = cancelButtonClick2;
    }
}
