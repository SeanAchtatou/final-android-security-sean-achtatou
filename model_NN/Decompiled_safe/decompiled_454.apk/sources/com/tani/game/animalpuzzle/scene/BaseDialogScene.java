package com.tani.game.animalpuzzle.scene;

import android.content.Context;
import org.anddev.andengine.engine.Engine;
import org.anddev.andengine.entity.primitive.Rectangle;
import org.anddev.andengine.entity.scene.Scene;

public class BaseDialogScene extends Scene {
    protected Context context = null;
    protected Engine engine = null;

    public BaseDialogScene(Engine engine2, Context context2) {
        super(2);
        this.engine = engine2;
        this.context = context2;
        initBackground();
    }

    /* access modifiers changed from: protected */
    public void initBackground() {
        setBackgroundEnabled(false);
        Rectangle rect = new Rectangle(0.0f, 0.0f, 480.0f, 320.0f);
        rect.setColor(255.0f, 153.0f, 255.0f);
        rect.setAlpha(0.3f);
        getChild(0).attachChild(rect);
    }
}
