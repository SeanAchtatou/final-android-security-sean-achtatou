package com.tani.game.animalpuzzle.scene;

import android.content.Context;
import com.tani.animalpuzzle.res.CommonResource;
import com.tani.game.animalpuzzle.activity.MainActivity;
import org.anddev.andengine.engine.Engine;
import org.anddev.andengine.entity.modifier.ScaleModifier;
import org.anddev.andengine.entity.sprite.Sprite;
import org.anddev.andengine.entity.text.Text;
import org.anddev.andengine.input.touch.TouchEvent;

public class GameFinish extends BaseDialogScene {
    private ButtonClickListerner cancelButtonClick = null;
    private CommonResource commonResource = null;
    private boolean isNewHighScore;
    /* access modifiers changed from: private */
    public ButtonClickListerner okButtonClick = null;
    private int totalScore;

    public interface ButtonClickListerner {
        void onClick();
    }

    public GameFinish(Engine engine, Context context, int totalScore2, boolean isNewHighScore2) {
        super(engine, context);
        this.commonResource = ((MainActivity) context).getCommonResource();
        this.totalScore = totalScore2;
        this.isNewHighScore = isNewHighScore2;
        loadResource();
        initScene();
    }

    /* access modifiers changed from: protected */
    public void loadResource() {
    }

    /* access modifiers changed from: protected */
    public void initScene() {
        Sprite newHighScore = new Sprite(250.0f, 40.0f, this.commonResource.newHighScoreRegion);
        getLastChild().attachChild(new Text(90.0f, 130.0f, this.commonResource.scoreFont, "Total:" + this.totalScore));
        if (this.isNewHighScore) {
            getLastChild().attachChild(newHighScore);
        }
        Sprite btnOk = new Sprite(220.0f, 150.0f, this.commonResource.okBtnRegion) {
            public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                if (pSceneTouchEvent.isActionDown()) {
                    ((MainActivity) GameFinish.this.context).vibrate();
                    registerEntityModifier(new ScaleModifier(0.1f, 1.0f, 1.5f));
                    return true;
                }
                if (pSceneTouchEvent.getAction() == 1) {
                    clearEntityModifiers();
                    reset();
                    if (GameFinish.this.okButtonClick != null) {
                        GameFinish.this.okButtonClick.onClick();
                        return false;
                    }
                }
                return super.onAreaTouched(pSceneTouchEvent, pTouchAreaLocalX, pTouchAreaLocalY);
            }
        };
        getLastChild().attachChild(btnOk);
        registerTouchArea(btnOk);
        getLastChild().attachChild(new Sprite(50.0f, 100.0f, this.commonResource.gameFinishMsgRegion));
    }

    public void destroyRes() {
    }

    public ButtonClickListerner getOkButtonClick() {
        return this.okButtonClick;
    }

    public void setOkButtonClick(ButtonClickListerner okButtonClick2) {
        this.okButtonClick = okButtonClick2;
    }

    public ButtonClickListerner getCancelButtonClick() {
        return this.cancelButtonClick;
    }

    public void setCancelButtonClick(ButtonClickListerner cancelButtonClick2) {
        this.cancelButtonClick = cancelButtonClick2;
    }
}
