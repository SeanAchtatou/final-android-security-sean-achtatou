package com.tani.game.animalpuzzle.scene;

import com.tani.game.animalpuzzle.activity.MainActivity;
import com.tani.game.base.AsyncTaskLoader;
import com.tani.game.base.BaseGameScene;
import com.tani.game.base.IAsyncCallback;
import java.util.ArrayList;
import java.util.List;
import org.anddev.andengine.entity.scene.background.EntityBackground;
import org.anddev.andengine.entity.sprite.Sprite;
import org.anddev.andengine.entity.text.Text;
import org.anddev.andengine.opengl.font.Font;
import org.anddev.andengine.opengl.font.FontFactory;
import org.anddev.andengine.opengl.texture.Texture;
import org.anddev.andengine.opengl.texture.TextureOptions;
import org.anddev.andengine.opengl.texture.region.TextureRegion;
import org.anddev.andengine.opengl.texture.region.TextureRegionFactory;

public class ScoreScene extends BaseGameScene {
    IAsyncCallback callback;
    /* access modifiers changed from: private */
    public Font font;
    List<Texture> loadedTextures = new ArrayList();
    /* access modifiers changed from: private */
    public TextureRegion scoreBgTextureRegion;
    /* access modifiers changed from: private */
    public TextureRegion scoreTextureRegion;

    public ScoreScene(int pLayerCount, MainActivity parent) {
        super(pLayerCount, parent);
    }

    /* access modifiers changed from: protected */
    public void onLoadResources() {
        TextureRegionFactory.setAssetBasePath("images/score/");
        this.callback = new IAsyncCallback() {
            public void workToDo() {
                Texture menuTexture = new Texture(512, 512, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
                ScoreScene.this.scoreBgTextureRegion = TextureRegionFactory.createFromAsset(menuTexture, ScoreScene.this.parent, "score_bg.png", 0, 0);
                ScoreScene.this.parent.getEngine().getTextureManager().loadTexture(menuTexture);
                ScoreScene.this.loadedTextures.add(menuTexture);
                Texture scoreTexture = new Texture(512, 512, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
                ScoreScene.this.scoreTextureRegion = TextureRegionFactory.createFromAsset(scoreTexture, ScoreScene.this.parent, "score.png", 0, 0);
                ScoreScene.this.parent.getEngine().getTextureManager().loadTexture(scoreTexture);
                ScoreScene.this.loadedTextures.add(scoreTexture);
                FontFactory.setAssetBasePath("font/");
                Texture fontTexture = new Texture(256, 256, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
                ScoreScene.this.font = FontFactory.createFromAsset(fontTexture, ScoreScene.this.parent, "Droid.ttf", 24.0f, true, -256);
                ScoreScene.this.parent.getEngine().getTextureManager().loadTexture(fontTexture);
                ScoreScene.this.parent.getEngine().getFontManager().loadFont(ScoreScene.this.font);
            }

            public void onComplete() {
                ScoreScene.this.parent.getEngine().getTextureManager().reloadTextures();
                ScoreScene.this.onLoadScene();
            }
        };
        this.parent.runOnUiThread(new Runnable() {
            public void run() {
                new AsyncTaskLoader().execute(ScoreScene.this.callback);
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onLoadScene() {
        setBackground(new EntityBackground(new Sprite(0.0f, 0.0f, this.scoreBgTextureRegion)));
        getChild(1).attachChild(new Sprite(40.0f, 50.0f, this.scoreTextureRegion));
        getLastChild().attachChild(new Text(140.0f, 52.0f, this.font, new StringBuilder().append(getScore(1)).toString()));
        getLastChild().attachChild(new Text(140.0f, 88.0f, this.font, new StringBuilder().append(getScore(2)).toString()));
        getLastChild().attachChild(new Text(140.0f, 125.0f, this.font, new StringBuilder().append(getScore(3)).toString()));
        getLastChild().attachChild(new Text(140.0f, 160.0f, this.font, new StringBuilder().append(getScore(4)).toString()));
        getLastChild().attachChild(new Text(140.0f, 193.0f, this.font, new StringBuilder().append(getScore(5)).toString()));
        getLastChild().attachChild(new Text(340.0f, 52.0f, this.font, new StringBuilder().append(getScore(6)).toString()));
        getLastChild().attachChild(new Text(340.0f, 88.0f, this.font, new StringBuilder().append(getScore(7)).toString()));
        getLastChild().attachChild(new Text(340.0f, 124.0f, this.font, new StringBuilder().append(getScore(8)).toString()));
        getLastChild().attachChild(new Text(340.0f, 160.0f, this.font, new StringBuilder().append(getScore(9)).toString()));
        getLastChild().attachChild(new Text(270.0f, 226.0f, this.font, new StringBuilder().append(getTotalScore()).toString()));
        this.parent.setScene(this);
        this.parent.showAd();
    }

    private int getTotalScore() {
        int total = 0;
        for (int i = 1; i < 10; i++) {
            total += getScore(i);
        }
        return total;
    }

    private int getScore(int level) {
        return this.parent.getSharedPreferences("ANIMAL_PUZZLE", 1).getInt("HIGH_SCORE_LEVEL_" + level, 0);
    }

    /* access modifiers changed from: protected */
    public void unloadScene() {
    }

    /* access modifiers changed from: protected */
    public void onLoadComplete() {
    }

    public void onBackPressed() {
        new MenuScene(2, this.parent).loadResources(false);
    }
}
