package com.tani.game.animalpuzzle.scene;

import android.content.Context;
import com.tani.animalpuzzle.res.CommonResource;
import com.tani.game.animalpuzzle.activity.MainActivity;
import org.anddev.andengine.engine.Engine;
import org.anddev.andengine.entity.IEntity;
import org.anddev.andengine.entity.modifier.IEntityModifier;
import org.anddev.andengine.entity.modifier.MoveModifier;
import org.anddev.andengine.entity.modifier.ScaleModifier;
import org.anddev.andengine.entity.modifier.SequenceEntityModifier;
import org.anddev.andengine.entity.sprite.Sprite;
import org.anddev.andengine.input.touch.TouchEvent;
import org.anddev.andengine.util.modifier.IModifier;
import org.anddev.andengine.util.modifier.ease.EaseBounceIn;

public class GameOverScene extends BaseDialogScene {
    /* access modifiers changed from: private */
    public ButtonClickListerner cancelButtonClick = null;
    private CommonResource commonResource = null;
    /* access modifiers changed from: private */
    public ButtonClickListerner okButtonClick = null;

    public interface ButtonClickListerner {
        void onClick();
    }

    public GameOverScene(Engine engine, Context context, String message) {
        super(engine, context);
        this.commonResource = ((MainActivity) context).getCommonResource();
        loadResource();
        getChild(0).attachChild(new Sprite(90.0f, 60.0f, this.commonResource.dlgBkgRegion));
        initScene();
    }

    /* access modifiers changed from: protected */
    public void loadResource() {
    }

    /* access modifiers changed from: protected */
    public void initBackground() {
        setBackgroundEnabled(false);
    }

    /* access modifiers changed from: protected */
    public void initScene() {
        final Sprite btnOk = new Sprite(125.0f, 180.0f, this.commonResource.replayBtnRegion) {
            public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                if (pSceneTouchEvent.isActionDown()) {
                    ((MainActivity) GameOverScene.this.context).vibrate();
                    registerEntityModifier(new ScaleModifier(0.1f, 1.0f, 1.5f));
                    return true;
                }
                if (pSceneTouchEvent.getAction() == 1) {
                    clearEntityModifiers();
                    reset();
                    if (GameOverScene.this.okButtonClick != null) {
                        GameOverScene.this.okButtonClick.onClick();
                        return false;
                    }
                }
                return super.onAreaTouched(pSceneTouchEvent, pTouchAreaLocalX, pTouchAreaLocalY);
            }
        };
        final Sprite btnCancel = new Sprite(280.0f, 180.0f, this.commonResource.menuBtnRegion) {
            public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                if (pSceneTouchEvent.isActionDown()) {
                    ((MainActivity) GameOverScene.this.context).vibrate();
                    registerEntityModifier(new ScaleModifier(0.1f, 1.0f, 1.5f));
                    return true;
                }
                if (pSceneTouchEvent.getAction() == 1) {
                    clearEntityModifiers();
                    reset();
                    if (GameOverScene.this.cancelButtonClick != null) {
                        GameOverScene.this.cancelButtonClick.onClick();
                        return false;
                    }
                }
                return super.onAreaTouched(pSceneTouchEvent, pTouchAreaLocalX, pTouchAreaLocalY);
            }
        };
        Sprite msg = new Sprite(159.0f, 0.0f, this.commonResource.gameOverMsgRegion);
        getLastChild().attachChild(msg);
        msg.registerEntityModifier(new SequenceEntityModifier(new IEntityModifier.IEntityModifierListener() {
            public /* bridge */ /* synthetic */ void onModifierFinished(IModifier iModifier, Object obj) {
                onModifierFinished((IModifier<IEntity>) iModifier, (IEntity) obj);
            }

            public void onModifierFinished(IModifier<IEntity> iModifier, IEntity pItem) {
                GameOverScene.this.getLastChild().attachChild(btnOk);
                GameOverScene.this.registerTouchArea(btnOk);
                GameOverScene.this.getLastChild().attachChild(btnCancel);
                GameOverScene.this.registerTouchArea(btnCancel);
            }
        }, new MoveModifier(2.0f, 159.0f, 150.0f, 0.0f, 100.0f, EaseBounceIn.getInstance())));
    }

    public void destroyRes() {
    }

    public ButtonClickListerner getOkButtonClick() {
        return this.okButtonClick;
    }

    public void setOkButtonClick(ButtonClickListerner okButtonClick2) {
        this.okButtonClick = okButtonClick2;
    }

    public ButtonClickListerner getCancelButtonClick() {
        return this.cancelButtonClick;
    }

    public void setCancelButtonClick(ButtonClickListerner cancelButtonClick2) {
        this.cancelButtonClick = cancelButtonClick2;
    }
}
