package com.tani.animalpuzzle.sound;

import com.tani.game.animalpuzzle.activity.MainActivity;
import java.io.IOException;
import org.anddev.andengine.audio.music.Music;
import org.anddev.andengine.audio.music.MusicFactory;
import org.anddev.andengine.audio.sound.Sound;
import org.anddev.andengine.audio.sound.SoundFactory;
import org.anddev.andengine.util.Debug;

public class SoundManager {
    private MainActivity activity;
    public Music bgSound;
    public Sound failSound;
    public Sound matchingSound;
    public Sound selectSound;
    public Sound victorySound;
    public Sound wrongSelectSound;

    public SoundManager() {
    }

    public SoundManager(MainActivity activity2) {
        this.activity = activity2;
    }

    public void initSound() {
        MusicFactory.setAssetBasePath("sounds/");
        try {
            this.bgSound = MusicFactory.createMusicFromAsset(this.activity.getEngine().getMusicManager(), this.activity, "game_bg_sound.mp3");
            this.bgSound.setLooping(true);
        } catch (IOException e) {
            Debug.e("Error", e);
        }
        SoundFactory.setAssetBasePath("sounds/");
        try {
            this.matchingSound = SoundFactory.createSoundFromAsset(this.activity.getEngine().getSoundManager(), this.activity, "matching.mp3");
            this.selectSound = SoundFactory.createSoundFromAsset(this.activity.getEngine().getSoundManager(), this.activity, "select.mp3");
            this.victorySound = SoundFactory.createSoundFromAsset(this.activity.getEngine().getSoundManager(), this.activity, "success.mp3");
            this.failSound = SoundFactory.createSoundFromAsset(this.activity.getEngine().getSoundManager(), this.activity, "failed.mp3");
            this.wrongSelectSound = SoundFactory.createSoundFromAsset(this.activity.getEngine().getSoundManager(), this.activity, "wrong_select.mp3");
        } catch (IOException e2) {
            Debug.e("Error", e2);
        }
    }

    public void playBgSound() {
        this.bgSound.play();
    }

    public void stopBgSound() {
        if (this.bgSound.isPlaying()) {
            this.bgSound.pause();
        }
    }

    public void playSelectSound() {
        this.selectSound.play();
    }

    public void playMatchingSound() {
        this.matchingSound.play();
    }

    public void playFailSound() {
        this.failSound.play();
    }

    public void playVictorySound() {
        this.victorySound.play();
    }

    public void playWrongSelectSound() {
        this.wrongSelectSound.play();
    }
}
