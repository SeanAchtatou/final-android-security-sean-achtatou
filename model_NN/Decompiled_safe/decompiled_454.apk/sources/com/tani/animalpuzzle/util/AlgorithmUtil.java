package com.tani.animalpuzzle.util;

import android.graphics.Point;
import android.util.Log;
import com.tani.game.animalpuzzle.sprite.AnimalSprite;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import org.anddev.andengine.entity.primitive.Line;
import org.anddev.andengine.entity.primitive.Rectangle;

public class AlgorithmUtil {
    private static int[] UU;
    private static int[] VV;

    static {
        int[] iArr = new int[4];
        iArr[2] = 1;
        iArr[3] = -1;
        UU = iArr;
        int[] iArr2 = new int[4];
        iArr2[0] = 1;
        iArr2[1] = -1;
        VV = iArr2;
    }

    public static List<Point> checkPath(int[][] pikachuValues, int x1, int y1, int x2, int y2) {
        if ((x1 == x2 && y1 == y2) || pikachuValues[x1][y1] == 0 || pikachuValues[x2][y2] == 0 || pikachuValues[x1][y1] != pikachuValues[x2][y2]) {
            return null;
        }
        Point[] queue = new Point[96];
        Point[][] dad = (Point[][]) Array.newInstance(Point.class, 12, 8);
        int[][] count = (int[][]) Array.newInstance(Integer.TYPE, 12, 8);
        for (int i = 0; i < 96; i++) {
            queue[i] = new Point(0, 0);
        }
        queue[0].x = x1;
        queue[0].y = y1;
        for (int i2 = 0; i2 < 12; i2++) {
            for (int j = 0; j < 8; j++) {
                dad[i2][j] = new Point(-1, -1);
            }
        }
        dad[x1][y1].x = -2;
        count[x1][y1] = 0;
        boolean[] canGo = new boolean[4];
        int[] p = new int[4];
        int[] q = new int[4];
        int last = 0;
        int first = 0;
        while (first <= last) {
            int i3 = queue[first].x;
            int j2 = queue[first].y;
            first++;
            for (int t = 0; t < 4; t++) {
                canGo[t] = true;
                p[t] = i3;
                q[t] = j2;
            }
            while (true) {
                for (int t2 = 0; t2 < 4; t2++) {
                    if (canGo[t2]) {
                        p[t2] = p[t2] + UU[t2];
                        q[t2] = q[t2] + VV[t2];
                        if (!isInside(p[t2], q[t2])) {
                            canGo[t2] = false;
                        } else if (p[t2] == x2 && q[t2] == y2) {
                            dad[p[t2]][q[t2]].x = i3;
                            dad[p[t2]][q[t2]].y = j2;
                            return createPath(dad, x2, y2);
                        } else if (pikachuValues[p[t2]][q[t2]] > 0) {
                            canGo[t2] = false;
                        } else if (dad[p[t2]][q[t2]].x == -1 && count[i3][j2] != 2) {
                            last++;
                            queue[last].x = p[t2];
                            queue[last].y = q[t2];
                            dad[p[t2]][q[t2]].x = i3;
                            dad[p[t2]][q[t2]].y = j2;
                            count[p[t2]][q[t2]] = count[i3][j2] + 1;
                        }
                    }
                }
                if (canGo[0] || canGo[1] || canGo[2] || canGo[3]) {
                }
            }
        }
        return null;
    }

    private static List<Point> createPath(Point[][] dad, int i, int j) {
        List<Point> points = new ArrayList<>();
        do {
            points.add(new Point(i, j));
            i = dad[i][j].x;
            j = dad[i][j].y;
        } while (i != -2);
        return points;
    }

    private static boolean isInside(int i, int j) {
        return i >= 0 && i < 12 && j >= 0 && j < 8;
    }

    public static void fixMatrix(int[][] pikachuValues, AnimalSprite[][] pikachuSprites, int level2) {
        if (level2 != 1) {
            if (level2 == 2) {
                fixZone(pikachuValues, pikachuSprites, 1, 1, 10, 6, 0);
            } else if (level2 == 3) {
                fixZone(pikachuValues, pikachuSprites, 1, 1, 10, 6, 1);
            } else if (level2 == 4) {
                fixZone(pikachuValues, pikachuSprites, 1, 1, 10, 6, 3);
            } else if (level2 == 5) {
                fixZone(pikachuValues, pikachuSprites, 1, 1, 10, 6, 2);
            } else if (level2 == 6) {
                fixZone(pikachuValues, pikachuSprites, 1, 5, 10, 6, 0);
                fixZone(pikachuValues, pikachuSprites, 1, 1, 10, 3, 1);
            } else if (level2 == 7) {
                fixZone(pikachuValues, pikachuSprites, 1, 5, 10, 6, 1);
                fixZone(pikachuValues, pikachuSprites, 1, 1, 10, 3, 0);
            } else if (level2 == 8) {
                fixZone(pikachuValues, pikachuSprites, 1, 1, 5, 6, 3);
                fixZone(pikachuValues, pikachuSprites, 7, 1, 10, 6, 2);
            } else {
                fixZone(pikachuValues, pikachuSprites, 1, 1, 5, 6, 2);
                fixZone(pikachuValues, pikachuSprites, 7, 1, 5, 6, 3);
            }
        }
    }

    private static void fixZone(int[][] pikachuValues, AnimalSprite[][] pikachuSprites, int i1, int j1, int i2, int j2, int vector) {
        boolean stop;
        do {
            stop = true;
            for (int i = i1; i <= i2; i++) {
                for (int j = j1; j <= j2; j++) {
                    if (pikachuValues[i][j] > 0) {
                        int p = i + UU[vector];
                        int q = j + VV[vector];
                        if (p >= 1 && p <= 10 && q >= 1 && q <= 6 && pikachuValues[p][q] == 0) {
                            swap(pikachuValues, pikachuSprites, i, j, p, q);
                            stop = false;
                        }
                    }
                }
            }
        } while (!stop);
    }

    /* JADX INFO: Multiple debug info for r3v13 int: [D('x' int), D('sprite1' com.tani.game.animalpuzzle.sprite.AnimalSprite)] */
    private static void swap(int[][] pikachuValues, AnimalSprite[][] pikachuSprites, int i, int j, int p, int q) {
        int tmp = pikachuValues[i][j];
        pikachuValues[i][j] = pikachuValues[p][q];
        pikachuValues[p][q] = tmp;
        AnimalSprite sprite1 = pikachuSprites[i - 1][j - 1];
        AnimalSprite sprite2 = pikachuSprites[p - 1][q - 1];
        if (sprite1 != null) {
            sprite1.setXPos(p);
            sprite1.setYPos(q);
            sprite1.setPosition((float) (((p - 1) * 42) + 30), (float) (((q - 1) * 44) + 32));
        }
        if (sprite2 != null) {
            sprite2.setXPos(i);
            sprite2.setYPos(j);
            sprite2.setPosition((float) (((i - 1) * 42) + 30), (float) (((j - 1) * 44) + 32));
        }
        AnimalSprite pTmp = pikachuSprites[i - 1][j - 1];
        pikachuSprites[i - 1][j - 1] = pikachuSprites[p - 1][q - 1];
        pikachuSprites[p - 1][q - 1] = pTmp;
    }

    public static boolean isNeedRepair(int[][] pikachuValues) {
        for (int i = 1; i <= 10; i++) {
            for (int j = 1; j <= 6; j++) {
                if (pikachuValues[i][j] > 0 && findTwin(pikachuValues, i, j)) {
                    return false;
                }
            }
        }
        return true;
    }

    /* JADX INFO: Multiple debug info for r6v3 int[]: [D('i' int), D('b' int[])] */
    /* JADX INFO: Multiple debug info for r8v3 int[][]: [D('tmp2' int[][]), D('k' int)] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void repareMatrix(int[][] r15, com.tani.game.animalpuzzle.sprite.AnimalSprite[][] r16) {
        /*
            r6 = 70
            int[] r9 = new int[r6]
            r7 = 0
            r6 = 1
            r8 = r7
        L_0x0007:
            r7 = 10
            if (r6 <= r7) goto L_0x004a
            int[] r6 = new int[r8]
            r7 = 0
        L_0x000e:
            if (r7 < r8) goto L_0x0062
            mixArray(r6, r8)
            r6 = 14
            r7 = 10
            int[] r6 = new int[]{r6, r7}
            java.lang.Class r7 = java.lang.Integer.TYPE
            java.lang.Object r8 = java.lang.reflect.Array.newInstance(r7, r6)
            int[][] r8 = (int[][]) r8
            r6 = 0
        L_0x0024:
            r7 = 12
            if (r6 < r7) goto L_0x0069
            org.anddev.andengine.entity.primitive.Rectangle r11 = findCheat(r8)
            r7 = 0
            r8 = 0
            r9 = 0
            r6 = 1
            r10 = r7
        L_0x0031:
            r7 = 10
            if (r6 <= r7) goto L_0x0081
        L_0x0035:
            float r6 = r11.getWidth()
            int r6 = (int) r6
            float r7 = r11.getHeight()
            int r7 = (int) r7
            r0 = r15
            r1 = r16
            r2 = r6
            r3 = r7
            r4 = r8
            r5 = r9
            swap(r0, r1, r2, r3, r4, r5)
            return
        L_0x004a:
            r7 = 1
        L_0x004b:
            r10 = 6
            if (r7 <= r10) goto L_0x0051
            int r6 = r6 + 1
            goto L_0x0007
        L_0x0051:
            r10 = r15[r6]
            r10 = r10[r7]
            if (r10 <= 0) goto L_0x005f
            r10 = r15[r6]
            r10 = r10[r7]
            r9[r8] = r10
            int r8 = r8 + 1
        L_0x005f:
            int r7 = r7 + 1
            goto L_0x004b
        L_0x0062:
            r10 = r9[r7]
            r6[r7] = r10
            int r7 = r7 + 1
            goto L_0x000e
        L_0x0069:
            r7 = 0
        L_0x006a:
            r9 = 8
            if (r7 < r9) goto L_0x0071
            int r6 = r6 + 1
            goto L_0x0024
        L_0x0071:
            r9 = r8[r6]
            r10 = r15[r6]
            r10 = r10[r7]
            if (r10 <= 0) goto L_0x007f
            r10 = 1
        L_0x007a:
            r9[r7] = r10
            int r7 = r7 + 1
            goto L_0x006a
        L_0x007f:
            r10 = 0
            goto L_0x007a
        L_0x0081:
            if (r10 > 0) goto L_0x0035
            r7 = 1
        L_0x0084:
            r12 = 6
            if (r7 <= r12) goto L_0x008c
            r7 = r10
        L_0x0088:
            int r6 = r6 + 1
            r10 = r7
            goto L_0x0031
        L_0x008c:
            float r12 = r11.getX()
            int r12 = (int) r12
            float r13 = r11.getY()
            int r13 = (int) r13
            if (r6 != r12) goto L_0x009a
            if (r7 == r13) goto L_0x00a8
        L_0x009a:
            r14 = r15[r6]
            r14 = r14[r7]
            r12 = r15[r12]
            r12 = r12[r13]
            if (r14 != r12) goto L_0x00a8
            r8 = r6
            r9 = r7
            r7 = 1
            goto L_0x0088
        L_0x00a8:
            int r7 = r7 + 1
            goto L_0x0084
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tani.animalpuzzle.util.AlgorithmUtil.repareMatrix(int[][], com.tani.game.animalpuzzle.sprite.AnimalSprite[][]):void");
    }

    private static boolean findTwin(int[][] pikachuValues, int i1, int j1) {
        int i;
        Point[] queue = new Point[96];
        boolean[][] cx = (boolean[][]) Array.newInstance(Boolean.TYPE, 12, 8);
        int[][] count = (int[][]) Array.newInstance(Integer.TYPE, 12, 8);
        for (int i2 = 0; i2 < 96; i2++) {
            queue[i2] = new Point(0, 0);
        }
        int first = 0;
        queue[0].x = i1;
        queue[0].y = j1;
        int j = 0;
        while (true) {
            i = j;
            if (i >= 12) {
                break;
            }
            for (int j2 = 0; j2 < 8; j2++) {
                cx[i][j2] = true;
            }
            j = i + 1;
        }
        cx[i1][j1] = false;
        count[i1][j1] = 0;
        boolean[] canGo = new boolean[4];
        int[] p = new int[4];
        int[] q = new int[4];
        int last = 0;
        while (first <= last) {
            i = queue[first].x;
            int j3 = queue[first].y;
            first++;
            for (int t = 0; t < 4; t++) {
                canGo[t] = true;
                p[t] = i;
                q[t] = j3;
            }
            while (true) {
                for (int t2 = 0; t2 < 4; t2++) {
                    if (canGo[t2]) {
                        p[t2] = p[t2] + UU[t2];
                        q[t2] = q[t2] + VV[t2];
                        if (!isInside(p[t2], q[t2])) {
                            canGo[t2] = false;
                        } else if (pikachuValues[p[t2]][q[t2]] == pikachuValues[i1][j1] && cx[p[t2]][q[t2]]) {
                            Log.w("Match position", "(" + p[t2] + "," + q[t2] + ") , (" + i1 + "," + j1 + ")");
                            return true;
                        } else if (pikachuValues[p[t2]][q[t2]] > 0) {
                            canGo[t2] = false;
                        } else if (cx[p[t2]][q[t2]] && count[i][j3] != 2) {
                            last++;
                            queue[last].x = p[t2];
                            queue[last].y = q[t2];
                            cx[p[t2]][q[t2]] = false;
                            count[p[t2]][q[t2]] = count[i][j3] + 1;
                        }
                    }
                }
                if (canGo[0] || canGo[1] || canGo[2] || canGo[3]) {
                }
            }
        }
        return false;
    }

    public static boolean levelComplete(int[][] pikachuValues) {
        for (int i = 0; i < 12; i++) {
            for (int j = 0; j < 8; j++) {
                if (pikachuValues[i][j] > 0) {
                    return false;
                }
            }
        }
        return true;
    }

    private static Rectangle findCheat(int[][] a) {
        int[] b = new int[60];
        int[] c = new int[60];
        int k = 0;
        for (int i = 1; i <= 10; i++) {
            for (int j = 1; j <= 6; j++) {
                if (a[i][j] > 0) {
                    b[k] = i;
                    c[k] = j;
                    k++;
                }
            }
        }
        mixArray2(b, c, k);
        for (int i2 = 0; i2 < k - 1; i2++) {
            for (int j2 = k - 1; j2 > i2; j2--) {
                if (checkPath(a, b[i2], c[i2], b[j2], c[j2]) != null) {
                    return new Rectangle((float) b[i2], (float) c[i2], (float) b[j2], (float) c[j2]);
                }
            }
        }
        return null;
    }

    private static void mixArray2(int[] a, int[] aa, int n) {
        int[] b = generateHV(n);
        int[] c = new int[n];
        for (int i = 0; i < n; i++) {
            c[i] = a[b[i]];
        }
        for (int i2 = 0; i2 < n; i2++) {
            a[i2] = c[i2];
        }
        for (int i3 = 0; i3 < n; i3++) {
            c[i3] = aa[b[i3]];
        }
        for (int i4 = 0; i4 < n; i4++) {
            aa[i4] = c[i4];
        }
    }

    private static void mixArray(int[] b, int k) {
        int[] tmp = generateHV(k);
        int[] c = new int[k];
        for (int i = 0; i < k; i++) {
            c[i] = b[tmp[i]];
        }
        for (int i2 = 0; i2 < k; i2++) {
            b[i2] = c[i2];
        }
    }

    private static int[] generateHV(int n) {
        int[] a = new int[n];
        for (int i = 0; i < n; i++) {
            a[i] = n;
        }
        int i2 = 0;
        int j = n;
        while (i2 < n) {
            int j2 = j - 1;
            int k = new Random().nextInt(j) + 1;
            int t = 0;
            while (k > 0) {
                int t2 = t + 1;
                if (a[t] == n) {
                    k--;
                    t = t2;
                } else {
                    t = t2;
                }
            }
            a[t - 1] = i2;
            i2++;
            j = j2;
        }
        return a;
    }

    public static List<Line> drawPaths(List<Point> points) {
        List<Line> lines = new ArrayList<>();
        for (int i = 0; i < points.size() - 1; i++) {
            Point p1 = points.get(i);
            Point p2 = points.get(i + 1);
            Point central1 = new Point((p1.x * 42) + 8, (p1.y * 44) + 6);
            Point central2 = new Point((p2.x * 42) + 8, (p2.y * 44) + 6);
            Line l = new Line((float) central1.x, (float) central1.y, (float) central2.x, (float) central2.y);
            l.setLineWidth(3.0f);
            l.setColor(0.0f, 255.0f, 123.0f);
            lines.add(l);
        }
        return lines;
    }
}
