package com.tani.animalpuzzle.res;

import org.anddev.andengine.opengl.texture.Texture;
import org.anddev.andengine.opengl.texture.region.TextureRegion;

public class MenuResource {
    public Texture bkgTexture = null;
    public Texture btnTexture = null;
    public TextureRegion guideBtnRegion = null;
    public TextureRegion playBtnRegion = null;
    public TextureRegion playingBgTextureRegion = null;
    public TextureRegion scoreBtnRegion = null;
    public TextureRegion soundOffRegion = null;
    public TextureRegion soundOnRegion = null;
    public Texture soundTexture = null;
}
