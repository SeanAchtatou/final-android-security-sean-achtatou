package com.tani.animalpuzzle.res;

import org.anddev.andengine.opengl.texture.Texture;
import org.anddev.andengine.opengl.texture.region.TextureRegion;

public class PlayingResource {
    public Texture animalTexture = null;
    public TextureRegion[] animalTextureRegions = new TextureRegion[22];
    public Texture bkgTexture = null;
    public TextureRegion playingBgTextureRegion = null;
    public TextureRegion selectAnimalTextureRegion = null;
}
