package com.tani.animalpuzzle.res;

import org.anddev.andengine.opengl.texture.Texture;
import org.anddev.andengine.opengl.texture.region.TextureRegion;

public class LevelResource {
    public Texture bkgTexture = null;
    public TextureRegion lvlLockRegion = null;
    public Texture lvlLockTexture = null;
    public TextureRegion[] lvlUnLockRegions = null;
    public Texture lvlUnLockTexture = null;
    public TextureRegion playingBgTextureRegion = null;
}
