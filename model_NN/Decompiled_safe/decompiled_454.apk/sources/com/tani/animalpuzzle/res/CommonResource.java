package com.tani.animalpuzzle.res;

import org.anddev.andengine.opengl.font.Font;
import org.anddev.andengine.opengl.texture.Texture;
import org.anddev.andengine.opengl.texture.region.TextureRegion;

public class CommonResource {
    public TextureRegion cancelBtnRegion;
    public TextureRegion dlgBkgRegion;
    public Texture dlgBkgTexture;
    public Texture dlgBtnTexture;
    public TextureRegion exitConfirmMsgRegion;
    public TextureRegion gameFinishMsgRegion;
    public TextureRegion gameOverMsgRegion;
    public TextureRegion levelCompletedMsgRegion;
    public TextureRegion menuBtnRegion;
    public Texture messageTexture;
    public TextureRegion newHighScoreRegion;
    public Texture newHighScoreTexture;
    public TextureRegion nextBtnRegion;
    public TextureRegion okBtnRegion;
    public TextureRegion replayBtnRegion;
    public Font scoreFont;
    public Texture scoreFontTexture;
}
