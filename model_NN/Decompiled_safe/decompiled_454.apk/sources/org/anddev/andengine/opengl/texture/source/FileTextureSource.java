package org.anddev.andengine.opengl.texture.source;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import org.anddev.andengine.util.Debug;
import org.anddev.andengine.util.StreamUtils;

public class FileTextureSource implements ITextureSource {
    private final File mFile;
    private final int mHeight;
    private final int mWidth;

    public FileTextureSource(File pFile) {
        IOException e;
        this.mFile = pFile;
        BitmapFactory.Options decodeOptions = new BitmapFactory.Options();
        decodeOptions.inJustDecodeBounds = true;
        InputStream in = null;
        try {
            InputStream in2 = new FileInputStream(pFile);
            try {
                BitmapFactory.decodeStream(in2, null, decodeOptions);
                StreamUtils.close(in2);
            } catch (IOException e2) {
                e = e2;
                in = in2;
                try {
                    Debug.e("Failed loading Bitmap in FileTextureSource. File: " + pFile, e);
                    StreamUtils.close(in);
                    this.mWidth = decodeOptions.outWidth;
                    this.mHeight = decodeOptions.outHeight;
                } catch (Throwable th) {
                    th = th;
                    StreamUtils.close(in);
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                in = in2;
                StreamUtils.close(in);
                throw th;
            }
        } catch (IOException e3) {
            e = e3;
            Debug.e("Failed loading Bitmap in FileTextureSource. File: " + pFile, e);
            StreamUtils.close(in);
            this.mWidth = decodeOptions.outWidth;
            this.mHeight = decodeOptions.outHeight;
        }
        this.mWidth = decodeOptions.outWidth;
        this.mHeight = decodeOptions.outHeight;
    }

    FileTextureSource(File pFile, int pWidth, int pHeight) {
        this.mFile = pFile;
        this.mWidth = pWidth;
        this.mHeight = pHeight;
    }

    public FileTextureSource clone() {
        return new FileTextureSource(this.mFile, this.mWidth, this.mHeight);
    }

    public int getHeight() {
        return this.mHeight;
    }

    public int getWidth() {
        return this.mWidth;
    }

    public Bitmap onLoadBitmap() {
        IOException e;
        BitmapFactory.Options decodeOptions = new BitmapFactory.Options();
        decodeOptions.inPreferredConfig = Bitmap.Config.ARGB_8888;
        InputStream in = null;
        try {
            InputStream in2 = new FileInputStream(this.mFile);
            try {
                Bitmap decodeStream = BitmapFactory.decodeStream(in2, null, decodeOptions);
                StreamUtils.close(in2);
                return decodeStream;
            } catch (IOException e2) {
                e = e2;
                in = in2;
                try {
                    Debug.e("Failed loading Bitmap in FileTextureSource. File: " + this.mFile, e);
                    StreamUtils.close(in);
                    return null;
                } catch (Throwable th) {
                    th = th;
                    StreamUtils.close(in);
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                in = in2;
                StreamUtils.close(in);
                throw th;
            }
        } catch (IOException e3) {
            e = e3;
            Debug.e("Failed loading Bitmap in FileTextureSource. File: " + this.mFile, e);
            StreamUtils.close(in);
            return null;
        }
    }

    public String toString() {
        return String.valueOf(getClass().getSimpleName()) + "(" + this.mFile + ")";
    }
}
