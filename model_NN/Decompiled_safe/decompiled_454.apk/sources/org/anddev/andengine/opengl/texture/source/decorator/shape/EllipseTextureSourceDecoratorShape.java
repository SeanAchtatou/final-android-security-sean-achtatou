package org.anddev.andengine.opengl.texture.source.decorator.shape;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import org.anddev.andengine.opengl.texture.source.decorator.BaseTextureSourceDecorator;

public class EllipseTextureSourceDecoratorShape implements ITextureSourceDecoratorShape {
    private static EllipseTextureSourceDecoratorShape sDefaultInstance;
    private final RectF mRectF = new RectF();

    public static EllipseTextureSourceDecoratorShape getDefaultInstance() {
        if (sDefaultInstance == null) {
            sDefaultInstance = new EllipseTextureSourceDecoratorShape();
        }
        return sDefaultInstance;
    }

    public void onDecorateBitmap(Canvas pCanvas, Paint pPaint, BaseTextureSourceDecorator.TextureSourceDecoratorOptions pDecoratorOptions) {
        this.mRectF.set(pDecoratorOptions.getInsetLeft(), pDecoratorOptions.getInsetTop(), ((float) (pCanvas.getWidth() - 1)) - pDecoratorOptions.getInsetRight(), ((float) (pCanvas.getHeight() - 1)) - pDecoratorOptions.getInsetBottom());
        pCanvas.drawOval(this.mRectF, pPaint);
    }
}
