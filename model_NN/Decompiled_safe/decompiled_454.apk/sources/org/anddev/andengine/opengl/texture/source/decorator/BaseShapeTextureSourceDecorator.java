package org.anddev.andengine.opengl.texture.source.decorator;

import android.graphics.Canvas;
import org.anddev.andengine.opengl.texture.source.ITextureSource;
import org.anddev.andengine.opengl.texture.source.decorator.BaseTextureSourceDecorator;
import org.anddev.andengine.opengl.texture.source.decorator.shape.ITextureSourceDecoratorShape;

public abstract class BaseShapeTextureSourceDecorator extends BaseTextureSourceDecorator {
    protected final ITextureSourceDecoratorShape mTextureSourceDecoratorShape;

    public abstract BaseShapeTextureSourceDecorator clone();

    public BaseShapeTextureSourceDecorator(ITextureSource pTextureSource, ITextureSourceDecoratorShape pTextureSourceDecoratorShape, BaseTextureSourceDecorator.TextureSourceDecoratorOptions pTextureSourceDecoratorOptions) {
        super(pTextureSource, pTextureSourceDecoratorOptions);
        this.mTextureSourceDecoratorShape = pTextureSourceDecoratorShape;
    }

    /* access modifiers changed from: protected */
    public void onDecorateBitmap(Canvas pCanvas) {
        this.mTextureSourceDecoratorShape.onDecorateBitmap(pCanvas, this.mPaint, this.mTextureSourceDecoratorOptions == null ? BaseTextureSourceDecorator.TextureSourceDecoratorOptions.DEFAULT : this.mTextureSourceDecoratorOptions);
    }
}
