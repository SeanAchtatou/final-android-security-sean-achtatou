package org.anddev.andengine.opengl.texture.region.buffer;

import org.anddev.andengine.opengl.texture.region.TextureRegion;

public class TextureRegionBuffer extends BaseTextureRegionBuffer {
    public TextureRegionBuffer(TextureRegion pTextureRegion, int pDrawType) {
        super(pTextureRegion, pDrawType);
    }

    public TextureRegion getTextureRegion() {
        return (TextureRegion) super.getTextureRegion();
    }

    /* access modifiers changed from: protected */
    public float getX1() {
        TextureRegion textureRegion = getTextureRegion();
        return ((float) textureRegion.getTexturePositionX()) / ((float) textureRegion.getTexture().getWidth());
    }

    /* access modifiers changed from: protected */
    public float getX2() {
        TextureRegion textureRegion = getTextureRegion();
        return ((float) (textureRegion.getTexturePositionX() + textureRegion.getWidth())) / ((float) textureRegion.getTexture().getWidth());
    }

    /* access modifiers changed from: protected */
    public float getY1() {
        TextureRegion textureRegion = getTextureRegion();
        return ((float) textureRegion.getTexturePositionY()) / ((float) textureRegion.getTexture().getHeight());
    }

    /* access modifiers changed from: protected */
    public float getY2() {
        TextureRegion textureRegion = getTextureRegion();
        return ((float) (textureRegion.getTexturePositionY() + textureRegion.getHeight())) / ((float) textureRegion.getTexture().getHeight());
    }
}
