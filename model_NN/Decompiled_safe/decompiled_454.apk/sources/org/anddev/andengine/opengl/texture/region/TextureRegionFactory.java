package org.anddev.andengine.opengl.texture.region;

import android.content.Context;
import org.anddev.andengine.opengl.texture.BuildableTexture;
import org.anddev.andengine.opengl.texture.Texture;
import org.anddev.andengine.opengl.texture.source.AssetTextureSource;
import org.anddev.andengine.opengl.texture.source.ITextureSource;
import org.anddev.andengine.opengl.texture.source.ResourceTextureSource;
import org.anddev.andengine.util.Callback;

public class TextureRegionFactory {
    private static String sAssetBasePath = "";

    public static void setAssetBasePath(String pAssetBasePath) {
        if (pAssetBasePath.endsWith("/") || pAssetBasePath.length() == 0) {
            sAssetBasePath = pAssetBasePath;
            return;
        }
        throw new IllegalStateException("pAssetBasePath must end with '/' or be lenght zero.");
    }

    public static TextureRegion extractFromTexture(Texture pTexture, int pTexturePositionX, int pTexturePositionY, int pWidth, int pHeight) {
        return new TextureRegion(pTexture, pTexturePositionX, pTexturePositionY, pWidth, pHeight);
    }

    public static TextureRegion createFromAsset(Texture pTexture, Context pContext, String pAssetPath, int pTexturePositionX, int pTexturePositionY) {
        return createFromSource(pTexture, new AssetTextureSource(pContext, String.valueOf(sAssetBasePath) + pAssetPath), pTexturePositionX, pTexturePositionY);
    }

    public static TiledTextureRegion createTiledFromAsset(Texture pTexture, Context pContext, String pAssetPath, int pTexturePositionX, int pTexturePositionY, int pTileColumns, int pTileRows) {
        return createTiledFromSource(pTexture, new AssetTextureSource(pContext, String.valueOf(sAssetBasePath) + pAssetPath), pTexturePositionX, pTexturePositionY, pTileColumns, pTileRows);
    }

    public static TextureRegion createFromResource(Texture pTexture, Context pContext, int pDrawableResourceID, int pTexturePositionX, int pTexturePositionY) {
        return createFromSource(pTexture, new ResourceTextureSource(pContext, pDrawableResourceID), pTexturePositionX, pTexturePositionY);
    }

    public static TiledTextureRegion createTiledFromResource(Texture pTexture, Context pContext, int pDrawableResourceID, int pTexturePositionX, int pTexturePositionY, int pTileColumns, int pTileRows) {
        return createTiledFromSource(pTexture, new ResourceTextureSource(pContext, pDrawableResourceID), pTexturePositionX, pTexturePositionY, pTileColumns, pTileRows);
    }

    public static TextureRegion createFromSource(Texture pTexture, ITextureSource pTextureSource, int pTexturePositionX, int pTexturePositionY) {
        TextureRegion textureRegion = new TextureRegion(pTexture, pTexturePositionX, pTexturePositionY, pTextureSource.getWidth(), pTextureSource.getHeight());
        pTexture.addTextureSource(pTextureSource, textureRegion.getTexturePositionX(), textureRegion.getTexturePositionY());
        return textureRegion;
    }

    public static TiledTextureRegion createTiledFromSource(Texture pTexture, ITextureSource pTextureSource, int pTexturePositionX, int pTexturePositionY, int pTileColumns, int pTileRows) {
        TiledTextureRegion tiledTextureRegion = new TiledTextureRegion(pTexture, pTexturePositionX, pTexturePositionY, pTextureSource.getWidth(), pTextureSource.getHeight(), pTileColumns, pTileRows);
        pTexture.addTextureSource(pTextureSource, tiledTextureRegion.getTexturePositionX(), tiledTextureRegion.getTexturePositionY());
        return tiledTextureRegion;
    }

    public static TextureRegion createFromAsset(BuildableTexture pBuildableTexture, Context pContext, String pAssetPath) {
        return createFromSource(pBuildableTexture, new AssetTextureSource(pContext, String.valueOf(sAssetBasePath) + pAssetPath));
    }

    public static TiledTextureRegion createTiledFromAsset(BuildableTexture pBuildableTexture, Context pContext, String pAssetPath, int pTileColumns, int pTileRows) {
        return createTiledFromSource(pBuildableTexture, new AssetTextureSource(pContext, String.valueOf(sAssetBasePath) + pAssetPath), pTileColumns, pTileRows);
    }

    public static TextureRegion createFromResource(BuildableTexture pBuildableTexture, Context pContext, int pDrawableResourceID) {
        return createFromSource(pBuildableTexture, new ResourceTextureSource(pContext, pDrawableResourceID));
    }

    public static TiledTextureRegion createTiledFromResource(BuildableTexture pBuildableTexture, Context pContext, int pDrawableResourceID, int pTileColumns, int pTileRows) {
        return createTiledFromSource(pBuildableTexture, new ResourceTextureSource(pContext, pDrawableResourceID), pTileColumns, pTileRows);
    }

    public static TextureRegion createFromSource(BuildableTexture pBuildableTexture, ITextureSource pTextureSource) {
        final TextureRegion textureRegion = new TextureRegion(pBuildableTexture, 0, 0, pTextureSource.getWidth(), pTextureSource.getHeight());
        pBuildableTexture.addTextureSource(pTextureSource, new Callback<Texture.TextureSourceWithLocation>() {
            public void onCallback(Texture.TextureSourceWithLocation pCallbackValue) {
                TextureRegion.this.setTexturePosition(pCallbackValue.getTexturePositionX(), pCallbackValue.getTexturePositionY());
            }
        });
        return textureRegion;
    }

    public static TiledTextureRegion createTiledFromSource(BuildableTexture pBuildableTexture, ITextureSource pTextureSource, int pTileColumns, int pTileRows) {
        final TiledTextureRegion tiledTextureRegion = new TiledTextureRegion(pBuildableTexture, 0, 0, pTextureSource.getWidth(), pTextureSource.getHeight(), pTileColumns, pTileRows);
        pBuildableTexture.addTextureSource(pTextureSource, new Callback<Texture.TextureSourceWithLocation>() {
            public void onCallback(Texture.TextureSourceWithLocation pCallbackValue) {
                TiledTextureRegion.this.setTexturePosition(pCallbackValue.getTexturePositionX(), pCallbackValue.getTexturePositionY());
            }
        });
        return tiledTextureRegion;
    }
}
