package org.anddev.andengine.util.modifier.ease;

import android.util.FloatMath;
import org.anddev.andengine.util.constants.MathConstants;

public class EaseElasticIn implements IEaseFunction, MathConstants {
    private static EaseElasticIn INSTANCE;

    private EaseElasticIn() {
    }

    public static EaseElasticIn getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new EaseElasticIn();
        }
        return INSTANCE;
    }

    public float getPercentageDone(float pSecondsElapsed, float pDuration, float pMinValue, float pMaxValue) {
        float s;
        float p = 0.0f;
        float a = 0.0f;
        if (pSecondsElapsed == 0.0f) {
            return pMinValue;
        }
        float pSecondsElapsed2 = pSecondsElapsed / pDuration;
        if (pSecondsElapsed2 == 1.0f) {
            return pMinValue + pMaxValue;
        }
        if (0.0f == 0.0f) {
            p = pDuration * 0.3f;
        }
        if (0.0f == 0.0f || ((pMaxValue > 0.0f && 0.0f < pMaxValue) || (pMaxValue < 0.0f && 0.0f < (-pMaxValue)))) {
            a = pMaxValue;
            s = p / 4.0f;
        } else {
            s = (float) (((double) (p / 6.2831855f)) * Math.asin((double) (pMaxValue / 0.0f)));
        }
        float pSecondsElapsed3 = pSecondsElapsed2 - 1.0f;
        return (float) ((-(((double) a) * Math.pow(2.0d, (double) (10.0f * pSecondsElapsed3)) * ((double) FloatMath.sin((((pSecondsElapsed3 * pDuration) - s) * 6.2831855f) / p)))) + ((double) pMinValue));
    }
}
