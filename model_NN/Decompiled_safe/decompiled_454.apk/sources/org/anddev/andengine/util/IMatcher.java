package org.anddev.andengine.util;

public interface IMatcher<T> {
    boolean matches(T t);
}
