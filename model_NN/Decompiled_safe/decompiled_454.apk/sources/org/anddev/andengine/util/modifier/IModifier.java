package org.anddev.andengine.util.modifier;

public interface IModifier<T> extends Cloneable {

    public interface IModifierListener<T> {
        void onModifierFinished(IModifier iModifier, Object obj);
    }

    IModifier<T> clone();

    float getDuration();

    IModifierListener<T> getModifierListener();

    boolean isFinished();

    boolean isRemoveWhenFinished();

    void onUpdate(float f, T t);

    void reset();

    void setModifierListener(IModifierListener<T> iModifierListener);

    void setRemoveWhenFinished(boolean z);
}
