package com.art.yh;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.art.util.BookMarkUtil;
import com.art.util.Constant;
import com.art.util.FileUtil;
import com.art.util.MultiDownloadNew;
import com.art.util.Protocals;
import com.art.util.XMEnDecrypt;
import com.google.ads.AdRequest;
import com.google.ads.AdView;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

public class DownloadedActivity extends BaseImgActivity {
    public static final int NumPerPage = 5;
    /* access modifiers changed from: private */
    public boolean bIndownload;
    private boolean bLocal = true;
    /* access modifiers changed from: private */
    public ProgressDialog dialog;
    /* access modifiers changed from: private */
    public int iDeletePosition = -1;
    private ImageButton mBtnGoto;
    private ImageButton mBtnNext;
    private ImageButton mBtnPrev;
    private List<Protocals.Album> mDataAlbum = new ArrayList();
    /* access modifiers changed from: private */
    public List<String> mDataAlbumName = new ArrayList();
    private List<Protocals.Album> mDataAlbumProc = new ArrayList();
    private List mDataImg = new ArrayList();
    private List<String> mDataURLList = new ArrayList();
    private ListView mListView;
    private TextView mPageInfo;
    /* access modifiers changed from: private */
    public String mPath;
    private ProgressBar mProgressBar;
    private boolean mbResume;
    private int miPageIndex;
    private int miPageTotal;
    private MyAdapter myAdapter;
    /* access modifiers changed from: private */
    public MyHandler myHandler;
    private String strRelativePath = "";
    private String strWebImgRootURL = "";

    static /* synthetic */ int access$008(DownloadedActivity x0) {
        int i = x0.miPageIndex;
        x0.miPageIndex = i + 1;
        return i;
    }

    static /* synthetic */ int access$010(DownloadedActivity x0) {
        int i = x0.miPageIndex;
        x0.miPageIndex = i - 1;
        return i;
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.category);
        ((AdView) findViewById(R.id.ad)).loadAd(new AdRequest());
        initData();
        initView();
        showAlbums(this.mPath + Constant.WPCOLLECTION + "/" + Constant.WPBOOKMARK);
    }

    private void initData() {
        this.mbResume = false;
        this.myHandler = new MyHandler();
        this.miPageIndex = 0;
        this.dialog = new ProgressDialog(this);
        this.bIndownload = false;
        if (Environment.getExternalStorageState().equalsIgnoreCase("mounted")) {
            this.mPath = "/sdcard/wpcollection";
        } else {
            this.mPath = "/data/wpcollection";
        }
        this.strWebImgRootURL = Protocals.strHomeRootURL;
    }

    private void initView() {
        this.mListView = (ListView) findViewById(R.id.category_list);
        this.mPageInfo = (TextView) findViewById(R.id.page_info);
        this.mBtnPrev = (ImageButton) findViewById(R.id.prev);
        this.mBtnNext = (ImageButton) findViewById(R.id.next);
        this.mBtnGoto = (ImageButton) findViewById(R.id.btn_goto);
        this.mProgressBar = (ProgressBar) findViewById(R.id.cat_progressbar);
        this.myAdapter = new MyAdapter();
        this.mListView.setAdapter((ListAdapter) this.myAdapter);
        this.mBtnPrev.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                DownloadedActivity.access$010(DownloadedActivity.this);
                DownloadedActivity.this.downAlbum();
                DownloadedActivity.this.updateView();
            }
        });
        this.mBtnNext.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                DownloadedActivity.access$008(DownloadedActivity.this);
                DownloadedActivity.this.downAlbum();
                DownloadedActivity.this.updateView();
            }
        });
        this.mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView arg0, View arg1, int arg2, long arg3) {
                Object[] arrObjects = DownloadedActivity.this.getObjectList(DownloadedActivity.this.getAlbum((String) DownloadedActivity.this.mDataAlbumName.get(arg2)));
                Intent intent = new Intent();
                Bundle bundle = new Bundle();
                String strURLTmp = (String) arrObjects[0];
                bundle.putString("url", strURLTmp.substring(0, strURLTmp.lastIndexOf("/") + 1));
                intent.putExtras(bundle);
                intent.setClass(DownloadedActivity.this, MyCollectionActivity.class);
                DownloadedActivity.this.startActivityForResult(intent, 0);
            }
        });
        this.mListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            public boolean onItemLongClick(AdapterView arg0, View arg1, int arg2, long arg3) {
                Log.v("zxl", "setOnItemLongClickListener");
                int unused = DownloadedActivity.this.iDeletePosition = arg2;
                DownloadedActivity.this.showInfoDelBookMark(DownloadedActivity.this.getResources().getString(R.string.txt_bookmark_title), DownloadedActivity.this.getResources().getString(R.string.txt_delbookmark_body));
                return true;
            }
        });
    }

    public void showInfoDelBookMark(String msgTitle, String msgString) {
        new AlertDialog.Builder(this).setTitle(msgTitle).setMessage(msgString).setPositiveButton(getResources().getString(R.string.alert_ok_button), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                DownloadedActivity.this.delBookMark();
            }
        }).setNegativeButton(getResources().getString(R.string.btn_cancel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        }).show();
    }

    public void delBookMark() {
        new BookMarkUtil(this.mPath + Constant.WPCOLLECTION + "/" + Constant.WPBOOKMARK, "bookmark").delete((this.miPageIndex * 5) + this.iDeletePosition);
        showAlbums(this.mPath + Constant.WPCOLLECTION + "/" + Constant.WPBOOKMARK);
    }

    /* Debug info: failed to restart local var, previous not found, register: 3 */
    public Protocals.Album getAlbum(String strAlbumName) {
        for (int i = 0; i < this.mDataAlbum.size(); i++) {
            if (this.mDataAlbum.get(i).name.equals(strAlbumName)) {
                return this.mDataAlbum.get(i);
            }
        }
        return null;
    }

    private void showAlbums(String strURL) {
        System.out.println("DownloadedActivity: showalbums.strurl=" + strURL);
        try {
            this.mDataURLList.add(strURL);
            this.mDataAlbum.clear();
            if (this.bLocal) {
                System.out.println("bLocal=true");
                Log.v("zxl", "bLocal=true");
                this.mDataAlbum = getAlbumBookMark(strURL);
            } else {
                System.out.println("bLocal=false");
                this.mDataAlbum = Protocals.getInstance().getCategories(strURL, false);
            }
            Log.v("zxl", "mDataAlbum.size() = " + this.mDataAlbum.size());
            if (this.mDataAlbum.size() == 0) {
                System.out.println("mDataAlbum.size=0");
                this.miPageTotal = 0;
                updateView();
                this.mDataAlbumName.clear();
                this.mDataImg.clear();
                this.myAdapter.notifyDataSetChanged();
                MainActivity.tabHost.setCurrentTab(2);
                return;
            }
            this.miPageTotal = caclTotalPages(this.mDataAlbum.size());
            Log.v("zxl", "miPageTotal = " + this.miPageTotal);
            downAlbum();
            updateView();
        } catch (Exception e) {
        }
    }

    private int caclTotalPages(int paramInt) {
        int iPages = paramInt / 5;
        if (paramInt % 5 != 0) {
            return iPages + 1;
        }
        return iPages;
    }

    private void dismissProgress() {
        this.mProgressBar.setVisibility(8);
    }

    /* access modifiers changed from: private */
    public void updateView() {
        if (this.miPageIndex >= this.miPageTotal) {
            this.miPageIndex--;
        }
        if (this.miPageIndex <= 0) {
            this.mBtnPrev.setVisibility(8);
        } else {
            this.mBtnPrev.setVisibility(0);
        }
        if (this.miPageIndex < this.miPageTotal - 1) {
            this.mBtnNext.setVisibility(0);
        } else {
            this.mBtnNext.setVisibility(8);
        }
        if (this.miPageTotal == 0) {
            this.mPageInfo.setVisibility(4);
            this.mPageInfo.setText(getResources().getString(R.string.page_info_head) + "0/0");
            return;
        }
        this.mPageInfo.setVisibility(0);
        StringBuilder sb = new StringBuilder(getResources().getString(R.string.page_info_head));
        sb.append(this.miPageIndex + 1).append("/");
        sb.append(this.miPageTotal);
        this.mPageInfo.setText(sb.toString());
    }

    public void onResume() {
        System.out.println("DownloadedActiovity.onResume() invoked!");
        if (this.mbResume) {
            showAlbums(this.mPath + Constant.WPCOLLECTION + "/" + Constant.WPBOOKMARK);
            System.out.println(this.mPath + Constant.WPCOLLECTION + "/" + Constant.WPBOOKMARK);
        } else {
            this.mbResume = true;
            System.out.println("resume=true");
        }
        super.onResume();
    }

    private List<Protocals.Album> getAlbumBookMark(String strBookMarkLoc) {
        List<Protocals.Album> list = new ArrayList<>();
        List<String> listBookMark = new BookMarkUtil(this.mPath + Constant.WPCOLLECTION + "/" + Constant.WPBOOKMARK, "bookmark").getElementList();
        for (int i = 0; i < listBookMark.size(); i++) {
            String strTmp = listBookMark.get(i);
            System.out.println("strTmp=" + strTmp);
            String[] strTmpArray = strTmp.split("\\|\\|\\|");
            if (strTmpArray.length < 3) {
                System.out.println("getAlbumBookMark: strTmpArray.size=" + strTmpArray.length);
            } else {
                Protocals p = new Protocals();
                p.getClass();
                Protocals.Album album = new Protocals.Album();
                album.name = strTmpArray[0];
                album.indexurl = strTmpArray[1];
                album.image = strTmpArray[2];
                album.category = "S";
                System.out.println("name=" + strTmpArray[0] + ", indexurl=" + strTmpArray[1] + ", image=" + strTmpArray[2]);
                list.add(album);
            }
        }
        return list;
    }

    /* access modifiers changed from: private */
    public void downAlbum() {
        System.out.println("DownloadedActivity.downAlbum enter");
        Log.v("zxl", "downAlbum called");
        clearImg();
        this.bIndownload = false;
        this.mDataAlbumProc.clear();
        this.myAdapter.notifyDataSetChanged();
        this.myHandler.showMyDialog();
        this.strRelativePath = getRelativePath(this.mDataURLList);
        System.out.println("DownloadedActivity.downAlbum relativepath=" + this.strRelativePath);
        Log.v("zxl", "DownloadedActivity.downAlbum relativepath=" + this.strRelativePath);
        int iTask = 0;
        if (this.miPageIndex * 5 >= this.mDataAlbum.size()) {
            this.miPageIndex--;
        }
        if (this.miPageIndex < 0) {
            this.miPageIndex = 0;
        }
        int iStarts = this.miPageIndex * 5;
        int iEnds = (iStarts + 5) - 1;
        Log.v("zxl", "iStarts =" + iStarts);
        Log.v("zxl", "mDataAlbum.size() =" + this.mDataAlbum.size());
        if (iEnds >= this.mDataAlbum.size()) {
            iEnds = this.mDataAlbum.size() - 1;
        }
        System.out.println("DownloadedActivity.downAlbum miPageindex=" + this.miPageIndex + ", istart=" + iStarts + ", iEnds=" + iEnds);
        Log.v("zxl", "iNFO =DownloadedActivity.downAlbum miPageindex=" + this.miPageIndex + ", istart=" + iStarts + ", iEnds=" + iEnds);
        for (int iIndex = iStarts; iIndex <= iEnds; iIndex++) {
            Protocals.Album album = this.mDataAlbum.get(iIndex);
            if (album != null) {
                System.out.println("DownloadedActivity.downAlbum: album");
                Object[] arrObjects = getObjectList(album);
                String strLocalImgPath = (String) arrObjects[1];
                Log.v("zxl", "strLocalImgPath =" + strLocalImgPath);
                if (FileUtil.fileExist(strLocalImgPath)) {
                    addAdapterData(album.name);
                } else if (!this.bLocal) {
                    if (iTask <= 2) {
                        try {
                            Thread.currentThread();
                            Thread.sleep(300);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        this.bIndownload = true;
                        new QueryImgTask().execute(arrObjects);
                        iTask++;
                    } else {
                        this.mDataAlbumProc.add(album);
                    }
                }
            }
        }
        this.myHandler.dismissMyDialog();
    }

    /* access modifiers changed from: private */
    public Object[] getObjectList(Protocals.Album album) {
        Object[] arrObjects = new Object[4];
        if (this.bLocal) {
            arrObjects[0] = album.indexurl;
            arrObjects[1] = album.image;
            arrObjects[2] = "";
            arrObjects[3] = "";
            System.out.println("url=" + arrObjects[0] + ", image=" + arrObjects[1] + ",,");
        } else {
            arrObjects[0] = this.strWebImgRootURL + this.strRelativePath + album.image;
            arrObjects[1] = this.mPath + this.strRelativePath + album.image;
            arrObjects[2] = String.valueOf(3);
            arrObjects[3] = album.name;
            System.out.println("url=" + arrObjects[0] + ", image=" + arrObjects[1] + ",3, name=" + album.name);
        }
        return arrObjects;
    }

    private void clearImg() {
        this.mDataAlbumName.clear();
        synchronized (this.mDataImg) {
            for (int i = 0; i < this.mDataImg.size(); i++) {
                Bitmap bitmap = (Bitmap) this.mDataImg.get(i);
            }
            this.mDataImg.clear();
        }
    }

    private String getRelativePath(List<String> mURLList) {
        if (mURLList.size() <= 1) {
            return "";
        }
        return mURLList.get(mURLList.size() - 1).substring(mURLList.get(0).length());
    }

    private String GetImagePathByName(String strImgName) {
        for (int i = 0; i < this.mDataAlbum.size(); i++) {
            Protocals.Album album = this.mDataAlbum.get(i);
            if (album.name.equals(strImgName)) {
                return album.image;
            }
        }
        return "";
    }

    private void addAdapterData(String strImgName) {
        System.out.println("Downloaded.addAdapterData:imgname=" + strImgName);
        this.mDataAlbumName.add(strImgName);
        String strImgPath = GetImagePathByName(strImgName);
        if (this.bLocal) {
            System.out.println("bLocal=true, strImgPath=" + strImgPath);
        } else {
            strImgPath = this.mPath + this.strRelativePath + strImgPath;
            System.out.println("bLocal=false, strImgPath=" + strImgPath);
        }
        this.mDataImg.add(BitmapFactory.decodeFile(strImgPath));
        this.myAdapter.notifyDataSetChanged();
    }

    /* access modifiers changed from: private */
    public int getSize() {
        if (this.mDataImg != null) {
            return this.mDataImg.size();
        }
        return 0;
    }

    /* Debug info: failed to restart local var, previous not found, register: 3 */
    /* access modifiers changed from: private */
    public Bitmap getElement(int iIndex) {
        System.out.println("getElement: mDataImg.size=" + this.mDataImg.size() + ", iIndex=" + iIndex + ", miPageIndex=" + this.miPageIndex);
        if (this.mDataImg == null || iIndex < 0 || iIndex >= 5 || iIndex >= this.mDataImg.size()) {
            return null;
        }
        return (Bitmap) this.mDataImg.get(iIndex);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        quitSystem();
        return true;
    }

    public void quitSystem() {
        new AlertDialog.Builder(this).setTitle(getResources().getString(R.string.txt_quit_title)).setMessage(getResources().getString(R.string.txt_quit_body)).setIcon((int) R.drawable.icon).setPositiveButton(getResources().getString(R.string.alert_ok_button), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                DownloadedActivity.this.finish();
            }
        }).setNegativeButton(getResources().getString(R.string.btn_cancel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        }).show();
    }

    public boolean onCreateOptionsMenu(Menu paramMenu) {
        getMenuInflater().inflate(R.menu.local_menu, paramMenu);
        return super.onCreateOptionsMenu(paramMenu);
    }

    public boolean onOptionsItemSelected(MenuItem paramMenuItem) {
        super.onOptionsItemSelected(paramMenuItem);
        switch (paramMenuItem.getItemId()) {
            case R.id.deletealbum /*2131099679*/:
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.txt_deletealbum_msg), 1).show();
                break;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        switch (requestCode) {
            case 0:
                switch (resultCode) {
                    case -1:
                        if (MainActivity.tabHost != null) {
                            MainActivity.tabHost.setCurrentTab(2);
                            return;
                        }
                        return;
                    default:
                        return;
                }
            default:
                return;
        }
    }

    class MyHandler extends Handler {
        private static final int DISMISS_DIALOG = 7;
        private static final int DISMISS_PROGRESSBAR = 3;
        private static final String INFO = "info";
        private static final int NOTIFY_DATA = 1;
        private static final String REMOTE_VERSION = "remote_version";
        private static final int SET_PAGEINFO = 5;
        private static final int SHOW_DIALOG = 6;
        private static final int SHOW_DOWNLOAD_PERCENT = 4;
        private static final int SHOW_MSG = 8;
        private static final int SHOW_PROGRESSBAR = 2;

        MyHandler() {
        }

        /* access modifiers changed from: private */
        public void dismissMyDialog() {
            sendEmptyMessage(DISMISS_DIALOG);
        }

        private void notifyDataSetChanged() {
            Message localMessage = new Message();
            localMessage.what = 1;
            sendMessage(localMessage);
        }

        /* access modifiers changed from: private */
        public void showMyDialog() {
            sendEmptyMessage(SHOW_DIALOG);
        }

        public void handleMessage(Message paramMessage) {
            switch (paramMessage.what) {
                case 1:
                    if (DownloadedActivity.this.dialog != null) {
                        DownloadedActivity.this.dialog.dismiss();
                        break;
                    }
                    break;
                case SHOW_DIALOG /*6*/:
                    if (DownloadedActivity.this.dialog != null) {
                        DownloadedActivity.this.dialog.setMessage("Loading...");
                        DownloadedActivity.this.dialog.show();
                        break;
                    }
                    break;
                case DISMISS_DIALOG /*7*/:
                    if (DownloadedActivity.this.dialog != null) {
                        DownloadedActivity.this.dialog.dismiss();
                        break;
                    }
                    break;
                case SHOW_MSG /*8*/:
                    Bundle bundle = paramMessage.getData();
                    if (bundle != null) {
                        new AlertDialog.Builder(DownloadedActivity.this).setTitle("Message:").setIcon((int) R.drawable.toast_warnning).setMessage(bundle.getString(INFO)).setPositiveButton("Never show", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        }).show();
                        break;
                    }
                    break;
            }
            super.handleMessage(paramMessage);
        }
    }

    public final class ViewHolder {
        public ImageView iv_img_cat;
        public TextView tv_txt_cat1;

        public ViewHolder() {
        }
    }

    class MyAdapter extends BaseAdapter {
        public MyAdapter() {
        }

        public int getCount() {
            return DownloadedActivity.this.getSize();
        }

        public Object getItem(int iIndex) {
            return DownloadedActivity.this.getElement(iIndex);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            byte[] bContent;
            UnsupportedEncodingException e1;
            String strContent;
            if (convertView == null) {
                holder = new ViewHolder();
                convertView = LayoutInflater.from(DownloadedActivity.this).inflate((int) R.layout.album_list, (ViewGroup) null);
                holder.iv_img_cat = (ImageView) convertView.findViewById(R.id.album);
                holder.tv_txt_cat1 = (TextView) convertView.findViewById(R.id.tv_txt_cat1);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            if (holder == null) {
                return null;
            }
            holder.iv_img_cat.setImageBitmap(DownloadedActivity.this.getElement(position));
            Protocals.Album album = DownloadedActivity.this.getAlbum((String) DownloadedActivity.this.mDataAlbumName.get(position));
            if (album.indexurl.indexOf(Protocals.strHomeRootURL) > -1) {
                String strParent = album.indexurl.substring(Protocals.strHomeRootURL.length());
                String strParent2 = strParent.substring(0, strParent.lastIndexOf(47));
                int iPosIndex = strParent2.lastIndexOf(47);
                if (iPosIndex > -1) {
                    strParent2 = strParent2.substring(iPosIndex + 1);
                }
                int iImgs = 0;
                String strLocal = DownloadedActivity.this.mPath + Constant.WPCOLLECTION + "/" + album.indexurl.substring(Protocals.strHomeRootURL.length());
                if (!strLocal.endsWith("/")) {
                    strLocal = strLocal + "/";
                }
                if (FileUtil.fileExist(strLocal + "index.xml")) {
                    strContent = new String(XMEnDecrypt.XMDecryptString(FileUtil.readFile(strLocal + "index.xml")));
                } else {
                    try {
                        FileUtil.createFolder(strLocal + "/");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    strContent = Protocals.getInstance().downWebContent(album.indexurl + "index1.xml", strLocal + "index.xml");
                }
                if (strContent != null) {
                    try {
                        while (Pattern.compile("<Name>(.*?)</Name><Type>(.*?)</Type><CoverImg>(.*?)</CoverImg><Category>(.*?)</Category>", 10).matcher(strContent).find()) {
                            iImgs++;
                        }
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                    holder.tv_txt_cat1.setText(strParent2 + "(" + String.valueOf(iImgs) + " pics)");
                } else {
                    holder.tv_txt_cat1.setText(strParent2 + "(0 pics)");
                    return convertView;
                }
            } else {
                String strParent3 = album.indexurl.substring((DownloadedActivity.this.mPath.substring(0, DownloadedActivity.this.mPath.length() - 1) + Constant.WPCOLLECTION + "/").length());
                String strParent4 = strParent3.substring(0, strParent3.lastIndexOf(47));
                int iPosIndex2 = strParent4.lastIndexOf(47);
                if (iPosIndex2 > -1) {
                    strParent4 = strParent4.substring(iPosIndex2 + 1);
                }
                int iImgs2 = 0;
                if (FileUtil.fileExist(album.indexurl) && (bContent = FileUtil.readFile(album.indexurl)) != null) {
                    byte[] btBuffers = XMEnDecrypt.XMDecryptString(bContent);
                    try {
                        try {
                            while (Pattern.compile("<Name>(.*?)</Name><Type>(.*?)</Type><CoverImg>(.*?)</CoverImg><Category>(.*?)</Category>", 10).matcher(new String(btBuffers, 0, btBuffers.length, "utf-8")).find()) {
                                iImgs2++;
                            }
                        } catch (UnsupportedEncodingException e3) {
                            e1 = e3;
                            e1.printStackTrace();
                            holder.tv_txt_cat1.setText(strParent4 + "(" + String.valueOf(iImgs2) + " pics)");
                            return convertView;
                        }
                    } catch (UnsupportedEncodingException e4) {
                        e1 = e4;
                        e1.printStackTrace();
                        holder.tv_txt_cat1.setText(strParent4 + "(" + String.valueOf(iImgs2) + " pics)");
                        return convertView;
                    }
                }
                holder.tv_txt_cat1.setText(strParent4 + "(" + String.valueOf(iImgs2) + " pics)");
            }
            return convertView;
        }
    }

    class QueryImgTask extends AsyncTask {
        QueryImgTask() {
        }

        /* access modifiers changed from: protected */
        public void onCancelled() {
            super.onCancelled();
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String strtxt) {
            DownloadedActivity.this.myHandler.sendEmptyMessage(1);
        }

        /* access modifiers changed from: protected */
        public void onProgressUpdate(int iPercent) {
            Message m = new Message();
            m.what = 6;
            m.obj = iPercent + "%";
            DownloadedActivity.this.myHandler.sendMessageDelayed(m, 0);
        }

        /* access modifiers changed from: protected */
        public Object doInBackground(Object[] arrParams) {
            String bookFolderTMP = DownloadedActivity.this.mPath + Constant.WPTMP + "/";
            int startThread = Integer.parseInt((String) arrParams[2]);
            System.out.println((String) arrParams[0]);
            System.out.println((String) arrParams[1]);
            System.out.println(bookFolderTMP);
            final String strAlbumImage = (String) arrParams[3];
            final MultiDownloadNew multiDownload = new MultiDownloadNew(startThread, (String) arrParams[0], (String) arrParams[1], bookFolderTMP);
            multiDownload.start();
            new Thread(new Runnable() {
                public void run() {
                    long time = new Date().getTime();
                    long lastdata = 0;
                    while (multiDownload.getPercntInt() < 100 && DownloadedActivity.this.bIndownload) {
                        try {
                            Thread.sleep(200);
                            long currTime = new Date().getTime();
                            long currData = multiDownload.getFileDownloadTotal();
                            if (currData != lastdata) {
                                long lasttime = currTime;
                                lastdata = currData;
                            }
                            Message m = new Message();
                            m.obj = multiDownload.getPercntInt() + "";
                            m.what = 2;
                            DownloadedActivity.this.myHandler.sendMessage(m);
                        } catch (InterruptedException e) {
                            Message m1 = new Message();
                            m1.what = -1;
                            DownloadedActivity.this.myHandler.sendMessage(m1);
                        }
                    }
                    Message m2 = new Message();
                    if (multiDownload.getPercntInt() != 100 || !DownloadedActivity.this.bIndownload) {
                        m2.what = 6;
                    } else {
                        m2.obj = strAlbumImage;
                        m2.what = 1;
                    }
                    DownloadedActivity.this.myHandler.sendMessage(m2);
                }
            }).start();
            return true;
        }
    }
}
