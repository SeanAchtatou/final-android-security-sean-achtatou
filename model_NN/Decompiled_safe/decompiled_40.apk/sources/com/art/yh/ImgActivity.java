package com.art.yh;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.art.util.BookMarkUtil;
import com.art.util.Constant;
import com.art.util.FileUtil;
import com.art.util.MultiDownloadNew;
import com.art.util.Protocals;
import com.art.util.WifiUtil;
import com.google.ads.AdRequest;
import com.google.ads.AdView;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ImgActivity extends BaseImgActivity {
    private static final int BUFFERSIZE = 1024;
    private static final int DISMISS_PROGRESS = 2;
    public static final int LARGE_SCREEN_HEIGHT = 800;
    public static final int LARGE_SCREEN_WIDTH = 480;
    public static final int NORMAL_SCREEN_HEIGHT = 480;
    public static final int NORMAL_SCREEN_WIDTH = 320;
    private static final int NOTIFY_CHANGED = 0;
    private static final int REQUEST_SAVE = 1;
    private static final int SHOW_DOWNLOADPERCENT = 3;
    private static final int SHOW_PROGRESS = 1;
    private static final String TAG = "ImgActivity";
    private static String mPath;
    /* access modifiers changed from: private */
    public static boolean mbAdView = true;
    private boolean bDownloadSuccess = true;
    /* access modifiers changed from: private */
    public boolean bDownloading = true;
    private Boolean bLocal = false;
    private boolean bclickable = true;
    private ImageButton btnNext;
    private ImageButton btnPrev;
    private int iAlbumnum;
    private int iDownloadSize;
    private int iFileSize;
    private int iImgPos;
    /* access modifiers changed from: private */
    public ImageView imgView;
    /* access modifiers changed from: private */
    public AdView mAdView;
    /* access modifiers changed from: private */
    public Bitmap mBitmap = null;
    /* access modifiers changed from: private */
    public Bitmap mBitmapTmp;
    private List<Protocals.Album> mDataAlbum = new ArrayList();
    private List<String> mDataURLList = new ArrayList();
    View.OnClickListener mOnClickListener;
    private HorizontalScrollView mhsv;
    /* access modifiers changed from: private */
    public String mstrRoot;
    /* access modifiers changed from: private */
    public MyHandler myHandler = new MyHandler();
    private ProgressBar progressBar;
    /* access modifiers changed from: private */
    public ProgressDialog progressDialog = null;
    private String strAlbumName;
    private String strBookMark = "";
    private String strFilePath;
    private String strFilename;
    private String strImgUrl;
    private String strLocalRoot = "";
    private String strPath;
    private String strRelativePath = "";
    /* access modifiers changed from: private */
    public TextView txtProgress;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle paramBundle) {
        super.onCreate(paramBundle);
        fullScreen();
        setContentView((int) R.layout.image);
        initView();
        initData();
    }

    private String getRelativePath(List<String> mURLList) {
        if (mURLList.size() <= 1) {
            return "";
        }
        return mURLList.get(mURLList.size() - 1).substring(mURLList.get(0).length());
    }

    /* access modifiers changed from: private */
    public void dismissProgress() {
        this.progressBar.setVisibility(8);
        this.txtProgress.setVisibility(8);
    }

    /* access modifiers changed from: private */
    public void showProgress() {
        this.progressBar.setVisibility(0);
        this.txtProgress.setVisibility(0);
    }

    private void initData() {
        if (Environment.getExternalStorageState().equals("mounted")) {
            mPath = "/sdcard/wpcollection/favorite/";
            this.mstrRoot = "/sdcard";
        } else {
            mPath = "/data/wpcollection/favorite/";
            this.mstrRoot = "/data";
        }
        Intent intent = getIntent();
        String stralbumname = intent.getStringExtra("album_name");
        this.strAlbumName = stralbumname;
        this.iImgPos = intent.getIntExtra("image_pos", 0) - 1;
        this.strImgUrl = intent.getStringExtra("base_url");
        this.strBookMark = intent.getStringExtra("bookmark");
        if (this.strImgUrl.indexOf(this.mstrRoot + Constant.WPPARENT + Constant.WPPACKAGE + "/") > -1) {
            this.bLocal = true;
            this.strLocalRoot = this.mstrRoot + Constant.WPPARENT + Constant.WPPACKAGE + "/";
            this.mDataURLList.add(this.mstrRoot + Constant.WPPARENT + Constant.WPPACKAGE + "/");
        } else {
            this.strLocalRoot = this.mstrRoot + Constant.WPPARENT + Constant.WPCOLLECTION + "/";
            this.mDataURLList.add(Protocals.strHomeRootURL);
        }
        this.mDataURLList.add(this.strImgUrl);
        this.strRelativePath = getRelativePath(this.mDataURLList);
        this.strFilename = stralbumname;
        this.mDataAlbum = Protocals.getInstance().getCategories(this.strImgUrl, true);
        this.iAlbumnum = this.mDataAlbum.size();
        next();
    }

    private void initView() {
        this.imgView = (ImageView) findViewById(R.id.image);
        if (this.imgView == null) {
            System.out.println("imgView is null");
        }
        this.btnPrev = (ImageButton) findViewById(R.id.prev1);
        this.btnNext = (ImageButton) findViewById(R.id.next1);
        this.progressBar = (ProgressBar) findViewById(R.id.img_progressbar);
        this.txtProgress = (TextView) findViewById(R.id.txt_progress);
        this.txtProgress.setText("0%");
        this.btnPrev.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                ImgActivity.this.prev();
            }
        });
        this.btnNext.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                ImgActivity.this.next();
            }
        });
        this.mAdView = (AdView) findViewById(R.id.ad);
        this.mAdView.loadAd(new AdRequest());
        if (!mbAdView) {
            this.mAdView.setVisibility(8);
        }
        this.mAdView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                boolean unused = ImgActivity.mbAdView = false;
                ImgActivity.this.mAdView.setVisibility(8);
            }
        });
        this.mhsv = (HorizontalScrollView) findViewById(R.id.hsv);
        Toast.makeText(getApplicationContext(), (int) R.string.txt_viewimgtips, 1).show();
    }

    private void reajustbtn() {
        if (this.iImgPos <= 0) {
            this.btnPrev.setVisibility(8);
        } else {
            this.btnPrev.setVisibility(0);
        }
    }

    /* access modifiers changed from: private */
    public void next() {
        if (this.iImgPos >= this.iAlbumnum - 1) {
            goMoreImg();
            return;
        }
        this.iImgPos++;
        viewImg(this.iImgPos);
    }

    public void viewImg(int iPos) {
        this.myHandler.sendEmptyMessage(3);
        Boolean bConn = Boolean.valueOf(new WifiUtil(this).wifiConnect());
        Object[] arrObjects = getObjectList(this.mDataAlbum.get(this.iImgPos));
        String strLocalFile = (String) arrObjects[1];
        if (FileUtil.fileExist(strLocalFile)) {
            this.mBitmap = BitmapFactory.decodeFile(strLocalFile);
            this.imgView.setImageBitmap(this.mBitmap);
            this.imgView.startAnimation(AnimationUtils.loadAnimation(this, R.anim.anim));
        } else if (bConn.booleanValue()) {
            this.myHandler.sendEmptyMessage(2);
            this.bDownloading = true;
            new QueryImgTask().execute(arrObjects);
        } else {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.txt_no_wifi_data), 1).show();
        }
        reajustbtn();
        redjustImgPos();
    }

    /* access modifiers changed from: private */
    public void redjustImgPos() {
        if (isLargeScreen() || isNormalScreen()) {
            this.mhsv.smoothScrollTo(getOffsetX(this.mBitmap, 960, this.iScreenHeight), 0);
        } else {
            this.mhsv.smoothScrollTo(this.iScreenWidth, 0);
        }
    }

    /* access modifiers changed from: private */
    public void prev() {
        this.iImgPos--;
        viewImg(this.iImgPos);
    }

    private Object[] getObjectList(Protocals.Album album) {
        Object[] arrObjects = new Object[4];
        String strBigName = album.name.replace("sm_", "");
        if (this.bLocal.booleanValue()) {
            arrObjects[0] = "";
            arrObjects[1] = this.strLocalRoot + this.strRelativePath + strBigName;
            arrObjects[2] = "";
            arrObjects[3] = "";
        } else {
            arrObjects[0] = this.strImgUrl + strBigName;
            arrObjects[1] = this.strLocalRoot + this.strRelativePath + strBigName;
            arrObjects[2] = String.valueOf(1);
            arrObjects[3] = album.name;
        }
        return arrObjects;
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent) {
        super.onActivityResult(paramInt1, paramInt2, paramIntent);
        String strDir = "";
        if (paramInt1 == 1 && paramInt2 == -1) {
            strDir = paramIntent.getStringExtra("M_DIR");
        }
        try {
            if (this.bDownloadSuccess && this.strFilename != null && this.strFilePath != null) {
                File file = new File(strDir + this.strFilename);
                FileOutputStream fos = new FileOutputStream(file);
                if (this.mBitmap != null) {
                    this.mBitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
                    Intent intent = new Intent("");
                    intent.setData(Uri.fromFile(file));
                    sendBroadcast(intent);
                }
                fos.close();
            }
        } catch (Exception e) {
        }
    }

    class QueryImgTask extends AsyncTask {
        QueryImgTask() {
        }

        /* access modifiers changed from: protected */
        public void onCancelled() {
            super.onCancelled();
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String strtxt) {
            ImgActivity.this.myHandler.sendEmptyMessage(1);
        }

        /* access modifiers changed from: protected */
        public void onProgressUpdate(int iPercent) {
            Message m = new Message();
            m.what = 6;
            m.obj = iPercent + "%";
            ImgActivity.this.myHandler.sendMessageDelayed(m, 0);
        }

        /* access modifiers changed from: protected */
        public Object doInBackground(Object[] arrParams) {
            if (new File((String) arrParams[1]).exists()) {
                Message m = new Message();
                m.obj = (String) arrParams[1];
                m.what = 1;
                ImgActivity.this.myHandler.sendMessage(m);
                ImgActivity.this.myHandler.sendEmptyMessage(3);
                return true;
            }
            int startThread = Integer.parseInt((String) arrParams[2]);
            final String strAlbumImage = (String) arrParams[1];
            final MultiDownloadNew multiDownload = new MultiDownloadNew(startThread, (String) arrParams[0], (String) arrParams[1], ImgActivity.this.mstrRoot + Constant.WPPARENT + Constant.WPTMP);
            multiDownload.start();
            new Thread(new Runnable() {
                public void run() {
                    long time = new Date().getTime();
                    long lastdata = 0;
                    ImgActivity.this.myHandler.sendEmptyMessage(2);
                    while (multiDownload.getPercntInt() < 100 && ImgActivity.this.bDownloading) {
                        try {
                            Thread.sleep(200);
                            long currTime = new Date().getTime();
                            long currData = multiDownload.getFileDownloadTotal();
                            if (currData != lastdata) {
                                long lasttime = currTime;
                                lastdata = currData;
                            }
                            Message m = new Message();
                            m.obj = multiDownload.getPercntInt() + "%";
                            m.what = 4;
                            ImgActivity.this.myHandler.sendMessage(m);
                        } catch (InterruptedException e) {
                            Message m1 = new Message();
                            m1.what = -1;
                            ImgActivity.this.myHandler.sendMessage(m1);
                        }
                    }
                    Message m2 = new Message();
                    if (multiDownload.getPercntInt() != 100 || !ImgActivity.this.bDownloading) {
                        m2.what = 3;
                    } else {
                        m2.obj = strAlbumImage;
                        m2.what = 1;
                    }
                    ImgActivity.this.myHandler.sendMessage(m2);
                }
            }).start();
            return true;
        }
    }

    class MyHandler extends Handler {
        private static final int DISMISS_DIALOG = 7;
        private static final int DISMISS_PROGRESSBAR = 3;
        private static final String INFO = "info";
        private static final int NOTIFY_DATA = 1;
        private static final String REMOTE_VERSION = "remote_version";
        private static final int SET_PAGEINFO = 5;
        private static final int SHOW_DIALOG = 6;
        private static final int SHOW_DOWNLOAD_PERCENT = 4;
        private static final int SHOW_MSG = 8;
        private static final int SHOW_PROGRESSBAR = 2;

        MyHandler() {
        }

        private void dismissMyDialog() {
            sendEmptyMessage(DISMISS_DIALOG);
        }

        private void notifyDataSetChanged() {
            Message localMessage = new Message();
            localMessage.what = 1;
            sendMessage(localMessage);
        }

        private void showMyDialog() {
            sendEmptyMessage(SHOW_DIALOG);
        }

        public void handleMessage(Message paramMessage) {
            switch (paramMessage.what) {
                case 1:
                    Bitmap unused = ImgActivity.this.mBitmap = BitmapFactory.decodeFile((String) paramMessage.obj);
                    ImgActivity.this.imgView.setImageBitmap(ImgActivity.this.mBitmap);
                    ImgActivity.this.imgView.startAnimation(AnimationUtils.loadAnimation(ImgActivity.this, R.anim.anim));
                    ImgActivity.this.dismissProgress();
                    ImgActivity.this.redjustImgPos();
                    break;
                case 2:
                    ImgActivity.this.showProgress();
                    ImgActivity.this.txtProgress.setText("0%");
                    break;
                case 3:
                    ImgActivity.this.dismissProgress();
                    break;
                case 4:
                    if (ImgActivity.this.bDownloading) {
                        ImgActivity.this.txtProgress.setText((String) paramMessage.obj);
                        break;
                    }
                    break;
                case SHOW_MSG /*8*/:
                    Bundle bundle = paramMessage.getData();
                    if (bundle != null) {
                        new AlertDialog.Builder(ImgActivity.this).setTitle("Message:").setIcon((int) R.drawable.toast_warnning).setMessage(bundle.getString(INFO)).setPositiveButton("Never show", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        }).show();
                        break;
                    }
                    break;
            }
            super.handleMessage(paramMessage);
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4) {
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }

    public boolean onCreateOptionsMenu(Menu paramMenu) {
        getMenuInflater().inflate(R.menu.image_menu, paramMenu);
        return super.onCreateOptionsMenu(paramMenu);
    }

    public boolean onOptionsItemSelected(MenuItem paramMenuItem) {
        super.onOptionsItemSelected(paramMenuItem);
        switch (paramMenuItem.getItemId()) {
            case R.id.save /*2131099677*/:
                setBookMark();
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.txt_album_save), 1).show();
                break;
            case R.id.set_wallpaper /*2131099678*/:
                setWallpaperFun();
                break;
        }
        return true;
    }

    public void setBookMark() {
        new BookMarkUtil(this.mstrRoot + Constant.WPPARENT + Constant.WPCOLLECTION + Constant.WPBOOKMARK, "bookmark").add(this.strBookMark);
    }

    public void setWallpaperFun() {
        this.progressDialog = ProgressDialog.show(this, getResources().getString(R.string.txt_setWallpaper_title), getResources().getString(R.string.txt_setWallpaper_body), true);
        new Thread(new Runnable() {
            public void run() {
                try {
                    Bitmap unused = ImgActivity.this.mBitmapTmp = ImgActivity.this.combinHImageS2B(ImgActivity.this.mBitmap);
                    ImgActivity.this.setWallpaper(ImgActivity.this.mBitmapTmp);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                ImgActivity.this.progressDialog.dismiss();
            }
        }).start();
    }

    private Bitmap getImageBmp(Bitmap bitmap) {
        int iHeightImg = bitmap.getHeight();
        int iWidthImg = bitmap.getWidth();
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int[] iSize = getResizeScale(iWidthImg, iHeightImg, dm.widthPixels, dm.heightPixels);
        if (iSize[0] != iWidthImg || iSize[1] != iHeightImg) {
            Bitmap resizeBmp = Bitmap.createScaledBitmap(bitmap, iSize[0], iSize[1], false);
        }
        return null;
    }

    private int[] getResizeScale(int iWImg, int iHImg, int iWPhone, int iHPhone) {
        int[] iSize = {iWImg, iHImg};
        if ((iWImg > iWPhone || iHImg > iHPhone) && (iWImg > iWPhone || iHImg > iHPhone)) {
            if (((double) iHImg) / ((double) iHPhone) > ((double) iWImg) / ((double) iWPhone)) {
                iSize[0] = (int) (((double) iWImg) * (((double) iHPhone) / ((double) iHImg)));
                iSize[1] = iHPhone;
            } else {
                iSize[0] = iWPhone;
                iSize[1] = (int) (((double) iHImg) * (((double) iWPhone) / ((double) iWImg)));
            }
        }
        return iSize;
    }

    public void setWallpaper(InputStream data) throws IOException {
        super.setWallpaper(data);
    }

    public void goMoreImg() {
        new AlertDialog.Builder(this).setTitle(getResources().getString(R.string.txt_lastimg_title)).setMessage(getResources().getString(R.string.txt_lastimg_body)).setIcon((int) R.drawable.icon).setPositiveButton(getResources().getString(R.string.alert_ok_button), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                Log.v("startSocketMonitor", "changed to tab1 (it was tab 2 before)");
                MainActivity.tabHost.setCurrentTab(1);
                ImgActivity.this.finish();
            }
        }).setNegativeButton(getResources().getString(R.string.btn_cancel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        }).show();
    }

    public boolean isLargeScreen() {
        Display localDisplay = getWindowManager().getDefaultDisplay();
        DisplayMetrics metrics = new DisplayMetrics();
        localDisplay.getMetrics(metrics);
        int iScreenWidth = metrics.widthPixels;
        int iScreenHeight = metrics.heightPixels;
        float f = metrics.density;
        if ((iScreenWidth >= 480 || iScreenHeight >= 800) && (iScreenWidth >= 800 || iScreenHeight >= 480)) {
            return true;
        }
        return false;
    }

    public boolean isNormalScreen() {
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int iScreenWidth = metrics.widthPixels;
        int iScreenHeight = metrics.heightPixels;
        if ((iScreenWidth >= 320 || iScreenHeight >= 480) && (iScreenWidth >= 480 || iScreenHeight >= 320)) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public void saveBitmap(Bitmap bm, String strFile) {
        File file = new File(strFile);
        if (!file.getParentFile().exists()) {
            file.getParentFile().mkdirs();
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(file);
        } catch (Exception e2) {
        }
        bm.compress(Bitmap.CompressFormat.PNG, 100, fos);
        try {
            fos.flush();
            fos.close();
        } catch (IOException e3) {
            e3.printStackTrace();
        }
    }

    public Bitmap combinHImage(Bitmap bmBackground) {
        Display display = getWindowManager().getDefaultDisplay();
        int iScreenWidth = display.getWidth();
        int iScreenHeight = display.getHeight();
        int width = bmBackground.getWidth();
        if (bmBackground.getHeight() >= 800) {
            return bmBackground;
        }
        int ivWidth = iScreenWidth * 2;
        Bitmap bm = Bitmap.createBitmap(ivWidth, iScreenHeight, Bitmap.Config.RGB_565);
        Canvas canvas = new Canvas(bm);
        canvas.drawColor(-1);
        if (isLargeScreen()) {
            float fXWidth = (float) iScreenWidth;
            float fYHeight = (float) iScreenHeight;
            float fLeft = (((float) ivWidth) - fXWidth) / 2.0f;
            Bitmap bmpScale = ScaleBitmap(bmBackground, iScreenWidth, iScreenHeight);
            Rect rect = new Rect(bmpScale.getWidth() - Math.round(fLeft), 0, iScreenWidth, iScreenHeight);
            RectF rectF = new RectF(0.0f, (((float) iScreenHeight) - fYHeight) / 2.0f, (float) Math.round(fLeft), ((((float) iScreenHeight) - fYHeight) / 2.0f) + fYHeight);
            canvas.drawBitmap(bmpScale, rect, rectF, (Paint) null);
            rect.left = 0;
            rectF.left = fLeft;
            rectF.right = fLeft + fXWidth;
            canvas.drawBitmap(bmpScale, rect, rectF, (Paint) null);
            rect.right = Math.round(fLeft);
            rectF.left = ((float) ivWidth) - fLeft;
            rectF.right = (float) ivWidth;
            canvas.drawBitmap(bmpScale, rect, rectF, (Paint) null);
        } else {
            float fXWidth2 = (float) iScreenWidth;
            float fYHeight2 = (float) iScreenHeight;
            if (!isNormalScreen()) {
                fXWidth2 = 240.0f;
                fYHeight2 = 320.0f;
            }
            float fLeft2 = (((float) ivWidth) - fXWidth2) / 2.0f;
            Rect rect2 = new Rect(bmBackground.getWidth() - Math.round(fLeft2), 0, bmBackground.getWidth(), bmBackground.getHeight());
            RectF rectF2 = new RectF(0.0f, (((float) iScreenHeight) - fYHeight2) / 2.0f, (float) Math.round(fLeft2), ((((float) iScreenHeight) - fYHeight2) / 2.0f) + fYHeight2);
            canvas.drawBitmap(bmBackground, rect2, rectF2, (Paint) null);
            rect2.left = 0;
            rectF2.left = fLeft2;
            rectF2.right = fLeft2 + fXWidth2;
            canvas.drawBitmap(bmBackground, rect2, rectF2, (Paint) null);
            rect2.right = Math.round(fLeft2);
            rectF2.left = ((float) ivWidth) - fLeft2;
            rectF2.right = (float) ivWidth;
            canvas.drawBitmap(bmBackground, rect2, rectF2, (Paint) null);
        }
        canvas.save(31);
        canvas.restore();
        return bm;
    }

    public Bitmap ScaleBitmap(Bitmap bmSrc, int iWidth, int iHeight) {
        Bitmap bmScale = Bitmap.createBitmap(iWidth, iHeight, Bitmap.Config.RGB_565);
        Canvas canvas = new Canvas(bmScale);
        canvas.drawBitmap(bmSrc, new Rect(0, 0, bmSrc.getWidth(), bmSrc.getHeight()), new Rect(0, 0, iWidth, iHeight), (Paint) null);
        canvas.save(31);
        canvas.restore();
        return bmScale;
    }

    private int getOffsetX(Bitmap bInimg, int iStardWidth, int iStardHeight) {
        int iScaleW;
        try {
            int iImgWidth = bInimg.getWidth();
            int iImgHeight = bInimg.getHeight();
            int i = iStardWidth / 2;
            if (((float) iImgHeight) / ((float) iImgWidth) < ((float) iStardHeight) / ((float) iStardWidth)) {
                int i2 = iStardHeight;
                iScaleW = (iImgWidth * iStardHeight) / iImgHeight;
            } else {
                iScaleW = iStardWidth;
                int i3 = (iImgHeight * iStardWidth) / iImgWidth;
            }
            if (iScaleW > iStardWidth) {
                return (iScaleW - iStardWidth) / 2;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return iStardWidth / 4;
    }

    public Bitmap combinHImageS2B(Bitmap bmBackground) {
        int iScaleW;
        int iScaleH;
        Display display = getWindowManager().getDefaultDisplay();
        int iScreenWidth = display.getWidth();
        int iScreenHeight = display.getHeight();
        int iImgWidth = bmBackground.getWidth();
        int iImgHeight = bmBackground.getHeight();
        if (isLargeScreen()) {
            return bmBackground;
        }
        float fXWidth = (float) iScreenWidth;
        float fYHeight = (float) iScreenHeight;
        if (!isNormalScreen()) {
            fXWidth = 240.0f;
            fYHeight = 320.0f;
        }
        int ivWidth = ((int) fXWidth) * 2;
        if (((float) iImgHeight) / ((float) iImgWidth) < fYHeight / ((float) ivWidth)) {
            iScaleH = (int) fYHeight;
            iScaleW = (((int) fYHeight) * iImgWidth) / iImgHeight;
        } else {
            iScaleW = ivWidth;
            iScaleH = (iImgHeight * ivWidth) / iImgWidth;
        }
        Log.v("zxl", "ivWidth = " + ivWidth);
        Log.v("zxl", "iScreenHeight = " + iScreenHeight);
        Bitmap bm = Bitmap.createBitmap(ivWidth, iScreenHeight, Bitmap.Config.RGB_565);
        Canvas canvas = new Canvas(bm);
        canvas.drawColor(-16777216);
        Bitmap bmScale = ScaleBitmap(bmBackground, ivWidth, iScreenHeight);
        if (iScaleW - ivWidth > 0) {
            int i = (iScaleW - ivWidth) / 2;
        }
        if (iScaleH - iScreenHeight > 0) {
            int i2 = (iScaleH - iScreenHeight) / 2;
        }
        canvas.drawBitmap(bmScale, 0.0f, 0.0f, (Paint) null);
        canvas.save(31);
        canvas.restore();
        return bm;
    }
}
