package com.art.yh;

import android.app.AlertDialog;
import android.app.TabActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.TabHost;
import android.widget.TabWidget;
import android.widget.TextView;
import com.art.util.AssetUtil;
import com.art.util.Constant;
import com.art.util.FileMatch;
import com.art.util.FileUtil;
import com.art.util.XMEnDecrypt;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.List;

public class MainActivity extends TabActivity {
    public static TabHost tabHost;
    private TabWidget widget;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.main);
        String strStatus = Environment.getExternalStorageState();
        if (strStatus.equals("mounted_to")) {
            showFinalAlert(getResources().getText(R.string.sdcard_readonly));
        } else if (strStatus.equals("shared")) {
            showFinalAlert(getResources().getText(R.string.sdcard_shared));
        } else {
            if (!strStatus.equals("mounted")) {
                showFinalAlert(getResources().getText(R.string.no_sdcard));
            }
            copyAssetWallpaper();
            tabHost = (TabHost) findViewById(16908306);
            tabHost.setup();
            this.widget = (TabWidget) findViewById(16908307);
            Intent intent1 = new Intent(this, MyHomeActivity.class);
            String strTitle1 = getString(R.string.tab_mycollection);
            tabHost.addTab(tabHost.newTabSpec(strTitle1).setIndicator(strTitle1, getResources().getDrawable(R.drawable.home)).setContent(intent1));
            Intent intent3 = new Intent(this, ImageLibrary.class);
            String strTitle3 = getString(R.string.tab_moreimg);
            tabHost.addTab(tabHost.newTabSpec(strTitle3).setIndicator(strTitle3, getResources().getDrawable(R.drawable.web)).setContent(intent3));
            Intent intent2 = new Intent(this, DownloadedActivity.class);
            String strTitle2 = getString(R.string.tab_downloadedalbum);
            tabHost.addTab(tabHost.newTabSpec(strTitle2).setIndicator(strTitle2, getResources().getDrawable(R.drawable.download)).setContent(intent2));
            setTabBackground();
            tabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
                public void onTabChanged(String tabID) {
                    MainActivity.this.setTabBackground();
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public void setTabBackground() {
        int itabnum = getTabHost().getTabWidget().getChildCount();
        for (int icurIndex = 0; icurIndex < itabnum; icurIndex++) {
            View view = getTabHost().getTabWidget().getChildAt(icurIndex);
            if (tabHost.getCurrentTab() == icurIndex) {
                view.setBackgroundColor(-39424);
            } else {
                view.setBackgroundColor(-1);
            }
            setWidgetColor(view, 17170444);
        }
    }

    private void setWidgetColor(View view, int color) {
        TextView tv = (TextView) view.findViewById(16908310);
        if (tv != null) {
            tv.setTextColor(getResources().getColorStateList(color));
        }
    }

    private void showFinalAlert(CharSequence paramCharSequence) {
        new AlertDialog.Builder(this).setTitle(getResources().getText(R.string.alert_title_failure)).setMessage(paramCharSequence).setPositiveButton((int) R.string.alert_ok_button, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface paramInterface, int paramInt) {
                MainActivity.this.finish();
            }
        }).setCancelable(false).show();
    }

    private void copyAssetWallpaper() {
        String strAppName = getResources().getString(R.string.app_name).replace(" ", "");
        String strRoot = "/sdcard";
        if (!Environment.getExternalStorageState().equals("mounted")) {
            strRoot = "/data";
        }
        String strDestFolder = strRoot + Constant.WPPARENT + Constant.WPPACKAGE + "/";
        List<String> listFile = null;
        try {
            listFile = getSplitsFiles("index.xml");
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("copyAssetWallpaper...");
        if (listFile != null) {
            if (!FileUtil.fileExist(strDestFolder)) {
                try {
                    FileUtil.createFolders(strDestFolder);
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
            AssetUtil aUtil = new AssetUtil(this);
            aUtil.releaseAssetRes("index.xml", strDestFolder + "index.xml", true);
            System.out.println("copyAssetWallpaper: listFile.size=" + listFile.size());
            for (int i = 0; i < listFile.size(); i++) {
                String strOrigFile = listFile.get(i);
                String strDestFile = strDestFolder + strOrigFile;
                System.out.println("src=" + strOrigFile + ", dest=" + strDestFile);
                new File(strDestFile);
                aUtil.releaseAssetRes(strOrigFile, strDestFile, true);
                String strOrigFile2 = strOrigFile.replace("sm_", "");
                String strDestFile2 = strDestFolder + strOrigFile2;
                new File(strDestFile2);
                aUtil.releaseAssetResRet(strOrigFile2, strDestFile2, true);
            }
        }
    }

    public List<String> getSplitsFiles(String strFileName) throws IOException {
        UnsupportedEncodingException e;
        List<String> lstFiles = null;
        InputStream finstream = getAssets().open(strFileName);
        if (finstream != null) {
            int iLen = finstream.available();
            byte[] btBuffer = new byte[iLen];
            if (finstream.read(btBuffer) == iLen) {
                try {
                    try {
                        lstFiles = FileMatch.matchRootListNoencode(new String(XMEnDecrypt.XMDecryptString(btBuffer), "utf-8"), "Name");
                    } catch (UnsupportedEncodingException e2) {
                        e = e2;
                    }
                } catch (UnsupportedEncodingException e3) {
                    e = e3;
                    e.printStackTrace();
                    finstream.close();
                    return lstFiles;
                }
            }
        }
        try {
            finstream.close();
        } catch (IOException e4) {
            e4.printStackTrace();
        }
        return lstFiles;
    }
}
