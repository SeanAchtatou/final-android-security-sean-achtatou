package com.art.yh;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.art.util.Constant;
import com.art.util.FileUtil;
import com.art.util.MultiDownloadNew;
import com.art.util.Protocals;
import com.art.util.WifiUtil;
import com.google.ads.AdRequest;
import com.google.ads.AdView;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MyHomeActivity extends BaseImgActivity {
    private static final int DIALOG_ADDFAVORITE = 0;
    private static final int NumPerPage = 100;
    private static final String TAG = "MyHomeActivity";
    private static boolean bPageInterrupt;
    /* access modifiers changed from: private */
    public static String mPath;
    /* access modifiers changed from: private */
    public boolean bIndownload = false;
    private Boolean bLocal = true;
    private Boolean bResume = false;
    /* access modifiers changed from: private */
    public ProgressDialog dialog;
    private MyAdapter mAdapter;
    private List<Protocals.Album> mDataAlbum = new ArrayList();
    /* access modifiers changed from: private */
    public List<String> mDataAlbumName = new ArrayList();
    /* access modifiers changed from: private */
    public List<Protocals.Album> mDataAlbumProc = new ArrayList();
    private List mDataImg = new ArrayList();
    private List<String> mDataURLList = new ArrayList();
    private TextView mDownloadPercent;
    private GridView mGridView;
    private ProgressBar mProgress;
    private RelativeLayout mProgressLayout;
    private String mRootDir;
    /* access modifiers changed from: private */
    public MyHandler myHandler = new MyHandler();
    /* access modifiers changed from: private */
    public String strBookMark = "";
    private String strRelativePath = "";
    /* access modifiers changed from: private */
    public String strWebImgRootURL = "";

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        fullScreen();
        setContentView((int) R.layout.my_collection);
        ((AdView) findViewById(R.id.ad)).loadAd(new AdRequest());
        initView();
        initData();
        showAlbums(this.strWebImgRootURL);
    }

    private void showAlbums(String strURL) {
        try {
            this.mDataAlbum.clear();
            this.mDataAlbumName.clear();
            this.mDataAlbumProc.clear();
            clearImg();
            this.bIndownload = false;
            this.mDataURLList.clear();
            this.bLocal = true;
            this.mDataURLList.add(this.mRootDir + Constant.WPPACKAGE + "/");
            this.mDataURLList.add(this.strWebImgRootURL);
            this.mAdapter.notifyDataSetChanged();
            this.mDataAlbum = Protocals.getInstance().getHomeCategories(strURL);
            System.out.println("------------> mDataAlbum.size() = " + this.mDataAlbum.size());
            System.out.println("------------> mDataImg.size()begin = " + this.mDataImg.size());
            if (this.mDataAlbum.size() != 0) {
                Boolean valueOf = Boolean.valueOf(new WifiUtil(this).wifiConnect());
                this.myHandler.showMyDialog();
                System.out.println("getRelativePath = " + getRelativePath(this.mDataURLList));
                this.strRelativePath = getRelativePath(this.mDataURLList);
                int iDataCount = 0;
                for (int iIndex = 0; iIndex < this.mDataAlbum.size(); iIndex++) {
                    Protocals.Album album = this.mDataAlbum.get(iIndex);
                    if (album != null) {
                        String strLocalImgPath = (String) getObjectList(album)[1];
                        System.out.println("strLocalImgPath = " + strLocalImgPath);
                        if (FileUtil.fileExist(strLocalImgPath)) {
                            iDataCount++;
                            addAdapterDataLocal(strLocalImgPath);
                        }
                    }
                }
                this.myHandler.dismissMyDialog();
                System.out.println("------------> mDataImg.size() = " + this.mDataImg.size());
                if (iDataCount == 0) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.txt_no_wifi_data), 1).show();
                }
            }
        } catch (Exception e) {
        }
    }

    /* access modifiers changed from: private */
    public Object[] getObjectList(Protocals.Album album) {
        return new Object[]{"", this.mRootDir + ((String) Constant.WPPACKAGE) + "/" + this.strRelativePath + album.name, "", ""};
    }

    private void clearImg() {
        for (int i = 0; i < this.mDataImg.size(); i++) {
            Bitmap bitmap = (Bitmap) this.mDataImg.get(i);
        }
        this.mDataImg.clear();
    }

    private String getRelativePath(List<String> mURLList) {
        if (mURLList.size() <= 1) {
            return "";
        }
        return mURLList.get(mURLList.size() - 1).substring(mURLList.get(0).length());
    }

    private void initData() {
        this.dialog = new ProgressDialog(this);
        if (Environment.getExternalStorageState().equals("mounted")) {
            mPath = "/sdcard/wpcollection/home/";
            this.mRootDir = "/sdcard/wpcollection";
        } else {
            mPath = "/data/wpcollection/home/";
            this.mRootDir = "/data/wpcollection";
        }
        this.strWebImgRootURL = mPath;
    }

    private void initView() {
        this.mProgress = (ProgressBar) findViewById(R.id.img_progressbar);
        this.mDownloadPercent = (TextView) findViewById(R.id.txt_progress);
        this.mGridView = (GridView) findViewById(R.id.grid);
        this.mAdapter = new MyAdapter();
        this.mGridView.setAdapter((ListAdapter) this.mAdapter);
        this.mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView arg0, View arg1, int arg2, long arg3) {
                Intent intent = new Intent();
                Bundle bundle = new Bundle();
                String strName = (String) MyHomeActivity.this.mDataAlbumName.get(arg2);
                int iStart = strName.indexOf("sm_");
                if (iStart >= 0) {
                    boolean unused = MyHomeActivity.this.bIndownload = false;
                    bundle.putString("album_name", strName.substring(iStart + "sm_".length()));
                    bundle.putInt("image_pos", arg2);
                    bundle.putString("base_url", MyHomeActivity.this.strWebImgRootURL);
                    bundle.putString("bookmark", MyHomeActivity.this.strBookMark);
                    intent.putExtras(bundle);
                    intent.setClass(MyHomeActivity.this, ImgHomeActivity.class);
                    MyHomeActivity.this.startActivityForResult(intent, 0);
                }
            }
        });
        this.mGridView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            public boolean onItemLongClick(AdapterView arg0, View arg1, int arg2, long arg3) {
                return true;
            }
        });
    }

    /* Debug info: failed to restart local var, previous not found, register: 3 */
    public Protocals.Album getAlbum(String strAlbumName) {
        for (int i = 0; i < this.mDataAlbum.size(); i++) {
            if (this.mDataAlbum.get(i).name.equals(strAlbumName)) {
                return this.mDataAlbum.get(i);
            }
        }
        return null;
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        quitSystem();
        return true;
    }

    public void quitSystem() {
        new AlertDialog.Builder(this).setTitle(getResources().getString(R.string.txt_quit_title)).setMessage(getResources().getString(R.string.txt_quit_body)).setIcon((int) R.drawable.icon).setPositiveButton(getResources().getString(R.string.alert_ok_button), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                MyHomeActivity.this.finish();
            }
        }).setNegativeButton(getResources().getString(R.string.btn_cancel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        }).show();
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        switch (requestCode) {
            case 0:
                switch (resultCode) {
                    case -1:
                        setResult(-1, new Intent());
                        finish();
                        return;
                    default:
                        return;
                }
            default:
                return;
        }
    }

    /* access modifiers changed from: private */
    public int getSize() {
        if (this.mDataImg != null) {
            return this.mDataImg.size();
        }
        return 0;
    }

    /* Debug info: failed to restart local var, previous not found, register: 3 */
    /* access modifiers changed from: private */
    public Bitmap getElement(int iIndex) {
        System.out.println("-----getElement:" + iIndex + ", mDataImg.size=" + this.mDataImg.size());
        if (this.mDataImg == null || iIndex < 0 || iIndex >= this.mDataImg.size()) {
            return null;
        }
        return (Bitmap) this.mDataImg.get(iIndex);
    }

    class AlbumInfo {
        Protocals.Album mAlbum;
        Bitmap mThumbnail;
        int tag = -1;

        public AlbumInfo() {
        }
    }

    public final class ViewHolder {
        public ImageView iv_img_cat;

        public ViewHolder() {
        }
    }

    class MyAdapter extends BaseAdapter {
        public MyAdapter() {
        }

        public int getCount() {
            return MyHomeActivity.this.getSize();
        }

        public Object getItem(int iIndex) {
            return MyHomeActivity.this.getElement(iIndex);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if (convertView == null) {
                holder = new ViewHolder();
                convertView = LayoutInflater.from(MyHomeActivity.this).inflate((int) R.layout.my_collection_item, (ViewGroup) null);
                holder.iv_img_cat = (ImageView) convertView.findViewById(R.id.grid_img);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            System.out.println("getView:pos=" + position);
            holder.iv_img_cat.setImageBitmap(MyHomeActivity.this.getElement(position));
            return convertView;
        }
    }

    class MyHandler extends Handler {
        private static final int DISMISS_DIALOG = 7;
        private static final int DISMISS_PROGRESSBAR = 3;
        private static final String INFO = "info";
        private static final int NOTIFY_DATA = 1;
        private static final String REMOTE_VERSION = "remote_version";
        private static final int SET_PAGEINFO = 5;
        private static final int SHOW_DIALOG = 6;
        private static final int SHOW_DOWNLOAD_PERCENT = 4;
        private static final int SHOW_MSG = 8;
        private static final int SHOW_PROGRESSBAR = 2;

        MyHandler() {
        }

        /* access modifiers changed from: private */
        public void dismissMyDialog() {
            sendEmptyMessage(DISMISS_DIALOG);
        }

        private void notifyDataSetChanged() {
            Message localMessage = new Message();
            localMessage.what = 1;
            sendMessage(localMessage);
        }

        /* access modifiers changed from: private */
        public void showMyDialog() {
            sendEmptyMessage(SHOW_DIALOG);
        }

        public void handleMessage(Message paramMessage) {
            switch (paramMessage.what) {
                case 1:
                    if (MyHomeActivity.this.dialog != null) {
                        MyHomeActivity.this.dialog.dismiss();
                    }
                    MyHomeActivity.this.addAdapterData((String) paramMessage.obj);
                    if (MyHomeActivity.this.mDataAlbumProc.size() > 0) {
                        new QueryImgTask().execute(MyHomeActivity.this.getObjectList((Protocals.Album) MyHomeActivity.this.mDataAlbumProc.get(0)));
                        MyHomeActivity.this.mDataAlbumProc.remove(0);
                        break;
                    }
                    break;
                case SHOW_DIALOG /*6*/:
                    if (MyHomeActivity.this.dialog != null) {
                        MyHomeActivity.this.dialog.setMessage("Loading...");
                        MyHomeActivity.this.dialog.show();
                        break;
                    }
                    break;
                case DISMISS_DIALOG /*7*/:
                    if (MyHomeActivity.this.dialog != null) {
                        MyHomeActivity.this.dialog.dismiss();
                        break;
                    }
                    break;
                case SHOW_MSG /*8*/:
                    Bundle bundle = paramMessage.getData();
                    if (bundle != null) {
                        new AlertDialog.Builder(MyHomeActivity.this).setTitle("Message:").setIcon((int) R.drawable.toast_warnning).setMessage(bundle.getString(INFO)).setPositiveButton("Never show", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        }).show();
                        break;
                    }
                    break;
            }
            super.handleMessage(paramMessage);
        }
    }

    private String GetImagePathByName(String strImgName) {
        for (int i = 0; i < this.mDataAlbum.size(); i++) {
            Protocals.Album album = this.mDataAlbum.get(i);
            if (album.name.equals(strImgName)) {
                return album.image;
            }
        }
        return "";
    }

    /* access modifiers changed from: private */
    public void addAdapterData(String strImgName) {
        this.mDataAlbumName.add(strImgName);
        String strImgPath = mPath + this.strRelativePath + strImgName;
        System.out.println("addAdapterData: strImgPath = " + strImgPath);
        this.mDataImg.add(BitmapFactory.decodeFile(strImgPath));
        this.mAdapter.notifyDataSetChanged();
    }

    private void addAdapterDataLocal(String strImgPath) {
        this.mDataAlbumName.add(strImgPath);
        System.out.println("addAdapterDataLocal: strImgPath = " + strImgPath);
        this.mDataImg.add(BitmapFactory.decodeFile(strImgPath));
        this.mAdapter.notifyDataSetChanged();
    }

    class QueryImgTask extends AsyncTask {
        QueryImgTask() {
        }

        /* access modifiers changed from: protected */
        public void onCancelled() {
            super.onCancelled();
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String strtxt) {
            MyHomeActivity.this.myHandler.sendEmptyMessage(1);
        }

        /* access modifiers changed from: protected */
        public void onProgressUpdate(int iPercent) {
            Message m = new Message();
            m.what = 6;
            m.obj = iPercent + "%";
            MyHomeActivity.this.myHandler.sendMessageDelayed(m, 0);
        }

        /* access modifiers changed from: protected */
        public Object doInBackground(Object[] arrParams) {
            String bookFolderTMP = MyHomeActivity.mPath + Constant.WPTMP + "/";
            if (!Environment.getExternalStorageState().equals("mounted")) {
                bookFolderTMP = "/data/wpcollection/Tmp/";
            }
            int startThread = Integer.parseInt((String) arrParams[2]);
            System.out.println((String) arrParams[0]);
            System.out.println((String) arrParams[1]);
            System.out.println(bookFolderTMP);
            final String strAlbumImage = (String) arrParams[3];
            final MultiDownloadNew multiDownload = new MultiDownloadNew(startThread, (String) arrParams[0], (String) arrParams[1], bookFolderTMP);
            multiDownload.start();
            new Thread(new Runnable() {
                public void run() {
                    long time = new Date().getTime();
                    long lastdata = 0;
                    while (multiDownload.getPercntInt() < MyHomeActivity.NumPerPage && MyHomeActivity.this.bIndownload) {
                        try {
                            Thread.sleep(200);
                            long currTime = new Date().getTime();
                            long currData = multiDownload.getFileDownloadTotal();
                            if (currData != lastdata) {
                                long lasttime = currTime;
                                lastdata = currData;
                            }
                            Message m = new Message();
                            m.obj = multiDownload.getPercntInt() + "";
                            m.what = 2;
                            MyHomeActivity.this.myHandler.sendMessage(m);
                        } catch (InterruptedException e) {
                            Message m1 = new Message();
                            m1.what = -1;
                            MyHomeActivity.this.myHandler.sendMessage(m1);
                        }
                    }
                    Message m2 = new Message();
                    if (multiDownload.getPercntInt() != MyHomeActivity.NumPerPage || !MyHomeActivity.this.bIndownload) {
                        m2.what = 7;
                    } else {
                        m2.obj = strAlbumImage;
                        m2.what = 1;
                    }
                    MyHomeActivity.this.myHandler.sendMessage(m2);
                }
            }).start();
            return true;
        }
    }

    public void showInfoNoWifi(String msgTitle, String msgString) {
        new AlertDialog.Builder(this).setTitle(msgTitle).setMessage(msgString).setPositiveButton(getResources().getString(R.string.alert_ok_button), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                MyHomeActivity.this.finish();
            }
        }).show();
    }

    public void onResume() {
        if (this.bResume.booleanValue()) {
            showAlbums(this.strWebImgRootURL);
        } else {
            this.bResume = true;
        }
        super.onResume();
    }
}
