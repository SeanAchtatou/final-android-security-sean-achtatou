package com.art.yh;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.art.util.FileUtil;
import com.art.util.MultiDownloadNew;
import com.art.util.Protocals;
import com.wpview.yeexm.R;
import java.io.File;
import java.net.URL;
import java.util.Date;

public class ImageLibrary extends Activity {
    private static final int DIALOG1 = 1;
    private static final int DIALOG2 = 2;
    private static final int DIALOG3 = 3;
    private static final int DIALOG5 = 5;
    private static final int DIALOG6 = 6;
    private static final String PATH = "/sdcard/download/";
    private static boolean contDownload = false;
    /* access modifiers changed from: private */
    public static boolean stopDownload = false;
    private File apk;
    private String bookFileName = "";
    /* access modifiers changed from: private */
    public String bookFolder = "";
    final Activity context = this;
    public URL gUrl;
    Handler handler = new ApkWebViewtemp();
    ImageView mGoBack;
    private WebView mWebView;
    private MultiDownloadNew multiDownload;
    public ProgressDialog pd;
    private AlertDialog.Builder progressBuilder;
    private String strBookMark = "";
    private String strCurrDestFile = "";
    private String strURLFold = "";
    private String strUrl = "";
    private downTask task = null;
    private Thread tdDownload = null;
    LinearLayout webparent;

    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        getWindow().setFlags(1024, 1024);
        setRequestedOrientation(1);
        requestWindowFeature(2);
        setContentView((int) R.layout.more_wps);
        this.mWebView = (WebView) findViewById(R.id.web);
        WebSettings localWebSettings = this.mWebView.getSettings();
        localWebSettings.setJavaScriptEnabled(true);
        localWebSettings.setSavePassword(false);
        localWebSettings.setSaveFormData(false);
        localWebSettings.setSupportZoom(false);
        this.mWebView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                ImageLibrary.this.context.setProgress(progress * 100);
            }
        });
        this.mWebView.setWebChromeClient(new MyWebChromeClient());
        this.mWebView.setWebViewClient(new MyWebClient());
        this.mWebView.addJavascriptInterface(new DemoJavaScriptInterface(), "zhuazhua");
        String strDownURL = getResources().getString(R.string.webimgrooturlHtml);
        this.mWebView.loadUrl(strDownURL);
        Log.v("startSocketMonitor", strDownURL);
    }

    public void setInfo(String msgTitle, String msgString) {
        AlertDialog.Builder myDialogBuilder = new AlertDialog.Builder(this);
        myDialogBuilder.setTitle(msgTitle);
        if (!msgString.equalsIgnoreCase("")) {
            myDialogBuilder.setMessage(msgString);
        }
        myDialogBuilder.create();
        myDialogBuilder.setPositiveButton(getResources().getString(R.string.alert_ok_button), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        myDialogBuilder.setNegativeButton(getResources().getString(R.string.btn_cancel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
    }

    public void showInfo(String msgTitle, String msgString) {
        this.progressBuilder = new AlertDialog.Builder(this);
        this.progressBuilder.setTitle(msgTitle);
        this.progressBuilder.setMessage(msgString);
        this.progressBuilder.setCancelable(true);
        this.progressBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                boolean unused = ImageLibrary.stopDownload = true;
            }
        });
        this.progressBuilder.create();
        this.progressBuilder.show();
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int paramInt) {
        switch (paramInt) {
            case 1:
                return buildDialog1(this);
            default:
                return null;
        }
    }

    private Dialog buildDialog1(Context paramContext) {
        AlertDialog.Builder localBuilder = new AlertDialog.Builder(paramContext);
        localBuilder.setTitle(getResources().getString(R.string.txt_download_msg_error)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
            }
        });
        return localBuilder.create();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        stopProgress();
        stopDownload = true;
        if (this.mWebView.canGoBack()) {
            this.mWebView.goBack();
            return true;
        }
        quitSystem();
        return true;
    }

    public void quitSystem() {
        new AlertDialog.Builder(this).setTitle(getResources().getString(R.string.txt_quit_title)).setMessage(getResources().getString(R.string.txt_quit_body)).setIcon((int) R.drawable.icon).setPositiveButton(getResources().getString(R.string.btn_yes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                ImageLibrary.this.finish();
            }
        }).setNegativeButton(getResources().getString(R.string.btn_cancel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        }).show();
    }

    final class DemoJavaScriptInterface {
        DemoJavaScriptInterface() {
        }

        public void clickOnAndroid(String paramString) {
            Log.v("startSocketMonitor", " clickOnAndroid called " + paramString);
            if (Environment.getExternalStorageState().equals("mounted")) {
                ImageLibrary.this.downLoadFile(paramString);
            } else {
                ImageLibrary.this.showDialog(3);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void downLoadFile(String httpUrl) {
        setProgress("");
        System.out.println("ImageLibrary.downLoadFile: url=" + httpUrl);
        String[] strTmpArray = httpUrl.split("\\|\\|\\|");
        if (strTmpArray.length == 2) {
            String[] arrParam = new String[3];
            String strURLBase = strTmpArray[0].substring(0, strTmpArray[0].length() - 1);
            String strCatNameString = strURLBase.substring(strURLBase.lastIndexOf("/") + 1);
            String strURLBase2 = strURLBase.substring(0, strURLBase.lastIndexOf("/"));
            String strRelativePath = strURLBase2.substring(Protocals.strHomeRootURL.length());
            String strLocalImg = "/sdcard/wpcollection/favorite/" + strRelativePath + "/" + strTmpArray[1];
            if (!Environment.getExternalStorageState().equals("mounted")) {
                strLocalImg = "/data/wpcollection/favorite/" + strRelativePath + "/" + strTmpArray[1];
            }
            this.strBookMark = strCatNameString + "|||" + strTmpArray[0] + "|||" + strLocalImg;
            this.strUrl = strTmpArray[0];
            arrParam[0] = strURLBase2 + "/" + strTmpArray[1];
            arrParam[1] = strLocalImg;
            arrParam[2] = "1";
            if (FileUtil.fileExist(strLocalImg)) {
                startGridActivity();
                stopProgress();
                return;
            }
            this.task = new downTask();
            this.task.execute(arrParam);
        }
    }

    final class MyWebChromeClient extends WebChromeClient {
        MyWebChromeClient() {
        }

        public boolean onJsAlert(WebView paramWebView, String paramString1, String paramString2, JsResult paramJsResult) {
            Log.v("startSocketMonitor", " onJsAlert called paramString1" + paramString1);
            Log.v("startSocketMonitor", " onJsAlert called paramString2" + paramString2);
            paramJsResult.confirm();
            return true;
        }

        public boolean shouldOverrideUrlLoading(WebView paramWebView, String paramString) {
            paramWebView.loadUrl(paramString);
            return true;
        }
    }

    final class MyWebClient extends WebViewClient {
        MyWebClient() {
        }

        public boolean shouldOverrideUrlLoading(WebView paramWebView, String paramString) {
            if (!paramString.startsWith(ImageLibrary.this.getString(R.string.domain_url))) {
                ImageLibrary.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(paramString)));
                return true;
            }
            paramWebView.loadUrl(paramString);
            return true;
        }
    }

    class downTask extends AsyncTask {
        downTask() {
        }

        /* access modifiers changed from: protected */
        public void onCancelled() {
            super.onCancelled();
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String strtxt) {
        }

        /* access modifiers changed from: protected */
        public void onProgressUpdate(int iPercent) {
        }

        /* access modifiers changed from: protected */
        public Object doInBackground(Object[] arrParams) {
            String bookFolderTMP = "/sdcard/wpcollection/Tmp/";
            if (!Environment.getExternalStorageState().equalsIgnoreCase("mounted")) {
                bookFolderTMP = "/data/wpcollection/Tmp/";
            }
            final MultiDownloadNew multiDownload = new MultiDownloadNew(Integer.parseInt((String) arrParams[2]), (String) arrParams[0], (String) arrParams[1], bookFolderTMP);
            multiDownload.start();
            new Thread(new Runnable() {
                public void run() {
                    long time = new Date().getTime();
                    long lastdata = 0;
                    while (multiDownload.getPercntInt() < 100) {
                        if (ImageLibrary.stopDownload) {
                        }
                        try {
                            Thread.sleep(200);
                            long currTime = new Date().getTime();
                            long currData = multiDownload.getFileDownloadTotal();
                            if (currData != lastdata) {
                                long lasttime = currTime;
                                lastdata = currData;
                            }
                            Message m = new Message();
                            m.obj = multiDownload.getPercntInt() + "";
                            m.what = 2;
                            ImageLibrary.this.handler.sendMessage(m);
                        } catch (InterruptedException e) {
                            Message m1 = new Message();
                            m1.what = -1;
                            ImageLibrary.this.handler.sendMessage(m1);
                        }
                    }
                    Message m2 = new Message();
                    System.out.println("multiDownload.getPercntInt() = " + multiDownload.getPercntInt());
                    System.out.println("stopDownload = " + ImageLibrary.stopDownload);
                    if (multiDownload.getPercntInt() == 100) {
                        m2.what = 1;
                    } else {
                        m2.what = ImageLibrary.DIALOG6;
                    }
                    ImageLibrary.this.handler.sendMessage(m2);
                }
            }).start();
            return true;
        }
    }

    public boolean onCreateOptionsMenu(Menu paramMenu) {
        getMenuInflater().inflate(R.menu.apk_menu, paramMenu);
        return super.onCreateOptionsMenu(paramMenu);
    }

    public boolean onOptionsItemSelected(MenuItem paramMenuItem) {
        super.onOptionsItemSelected(paramMenuItem);
        switch (paramMenuItem.getItemId()) {
            case R.id.refresh /*2131099675*/:
                this.mWebView.reload();
                return true;
            default:
                return true;
        }
    }

    /* access modifiers changed from: private */
    public void startGridActivity() {
        Intent intent = new Intent();
        Bundle bundle = new Bundle();
        bundle.putString("url", this.strUrl);
        bundle.putString("bookmark", this.strBookMark);
        intent.putExtras(bundle);
        intent.setClass(this, MyCollectionActivity.class);
        startActivity(intent);
    }

    class ApkWebViewtemp extends Handler {
        ApkWebViewtemp() {
        }

        public void handleMessage(Message paramMessage) {
            switch (paramMessage.what) {
                case -1:
                    ImageLibrary.this.stopProgress();
                    ImageLibrary.this.showDialog(1);
                    return;
                case 0:
                case 3:
                case R.styleable.com_admob_android_ads_AdView_refreshInterval /*4*/:
                default:
                    ImageLibrary.this.setProgress("0");
                    return;
                case 1:
                    ImageLibrary.this.startGridActivity();
                    ImageLibrary.this.stopProgress();
                    return;
                case 2:
                    ImageLibrary.this.setProgress((String) paramMessage.obj);
                    return;
                case 5:
                    ImageLibrary.this.stopProgress();
                    try {
                        FileUtil.delFolder(ImageLibrary.this.bookFolder);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    ImageLibrary.this.showDialog(5);
                    return;
                case ImageLibrary.DIALOG6 /*6*/:
                    ImageLibrary.this.stopProgress();
                    try {
                        FileUtil.delFolder(ImageLibrary.this.bookFolder);
                        return;
                    } catch (Exception e2) {
                        e2.printStackTrace();
                        return;
                    }
            }
        }
    }

    public void stopProgress() {
        if (this.pd != null) {
            this.pd.dismiss();
            this.pd = null;
        }
    }

    public void setProgress(String paramMessage) {
        System.out.println("----------> paramMessage = " + paramMessage);
        String msg = getResources().getString(R.string.txt_download_msg);
        int iPct = 0;
        if (!paramMessage.equals("")) {
            iPct = Integer.parseInt(paramMessage);
        }
        if (this.pd == null) {
            ProgressDialog localProgressDialog = new ProgressDialog(this);
            localProgressDialog.setProgressStyle(0);
            localProgressDialog.setMessage(msg);
            this.pd = localProgressDialog;
            this.pd.show();
        }
        this.pd.setProgress(iPct);
    }
}
