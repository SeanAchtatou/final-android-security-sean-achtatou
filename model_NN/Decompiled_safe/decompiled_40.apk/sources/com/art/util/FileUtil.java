package com.art.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.StringTokenizer;

public class FileUtil {
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x004f, code lost:
        return r8.toString();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0056, code lost:
        r9 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0057, code lost:
        r8.append(r9.toString());
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String readTxt(java.lang.String r11, java.lang.String r12, int r13) throws java.io.IOException {
        /*
            java.lang.String r10 = ""
            java.lang.String r12 = r12.trim()
            java.lang.StringBuffer r8 = new java.lang.StringBuffer
            java.lang.String r9 = ""
            r8.<init>(r10)
            java.lang.String r7 = ""
            r5 = 0
            java.io.FileInputStream r4 = new java.io.FileInputStream     // Catch:{ IOException -> 0x0060 }
            r4.<init>(r11)     // Catch:{ IOException -> 0x0060 }
            java.lang.String r9 = ""
            boolean r9 = r12.equals(r9)     // Catch:{ IOException -> 0x0060 }
            if (r9 == 0) goto L_0x0050
            java.io.InputStreamReader r6 = new java.io.InputStreamReader     // Catch:{ IOException -> 0x0060 }
            r6.<init>(r4)     // Catch:{ IOException -> 0x0060 }
        L_0x0022:
            java.io.BufferedReader r0 = new java.io.BufferedReader     // Catch:{ IOException -> 0x0060 }
            r0.<init>(r6)     // Catch:{ IOException -> 0x0060 }
            java.lang.String r1 = ""
        L_0x0029:
            java.lang.String r1 = r0.readLine()     // Catch:{ IOException -> 0x0056 }
            if (r1 == 0) goto L_0x004b
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0056 }
            r9.<init>()     // Catch:{ IOException -> 0x0056 }
            java.lang.StringBuilder r9 = r9.append(r1)     // Catch:{ IOException -> 0x0056 }
            java.lang.String r10 = "\n"
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ IOException -> 0x0056 }
            java.lang.String r9 = r9.toString()     // Catch:{ IOException -> 0x0056 }
            r8.append(r9)     // Catch:{ IOException -> 0x0056 }
            if (r13 <= 0) goto L_0x0029
            int r5 = r5 + 1
            if (r5 <= r13) goto L_0x0029
        L_0x004b:
            java.lang.String r7 = r8.toString()     // Catch:{ IOException -> 0x0060 }
            return r7
        L_0x0050:
            java.io.InputStreamReader r6 = new java.io.InputStreamReader     // Catch:{ IOException -> 0x0060 }
            r6.<init>(r4, r12)     // Catch:{ IOException -> 0x0060 }
            goto L_0x0022
        L_0x0056:
            r9 = move-exception
            r2 = r9
            java.lang.String r9 = r2.toString()     // Catch:{ IOException -> 0x0060 }
            r8.append(r9)     // Catch:{ IOException -> 0x0060 }
            goto L_0x004b
        L_0x0060:
            r9 = move-exception
            r3 = r9
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.art.util.FileUtil.readTxt(java.lang.String, java.lang.String, int):java.lang.String");
    }

    public static String readTxt(String filePathAndName, String encoding) throws IOException {
        return readTxt(filePathAndName, encoding, 0);
    }

    public static String readFile(File file, int nLine) {
        int iLineCounter = 0;
        if (!file.exists()) {
            System.err.println("Does not exist!");
            return "";
        } else if (file.isFile()) {
            try {
                BufferedReader input = new BufferedReader(new FileReader(file));
                StringBuffer buffer = new StringBuffer();
                while (true) {
                    String text = input.readLine();
                    if (text == null) {
                        break;
                    }
                    buffer.append(text + "\n");
                    if (nLine > 0 && (iLineCounter = iLineCounter + 1) > nLine) {
                        break;
                    }
                }
                return buffer.toString();
            } catch (IOException e) {
                System.err.println("File Error!");
                return "";
            }
        } else if (!file.isDirectory()) {
            return "";
        } else {
            String[] dir = file.list();
            String output = "" + "Directory contents:\n";
            for (int i = 0; i < dir.length; i++) {
                output = output + dir[i] + "\n";
            }
            return output;
        }
    }

    public static void createFile(String filePathAndName, String fileContent) throws Exception {
        try {
            File myFilePath = new File(filePathAndName.toString());
            if (!myFilePath.exists()) {
                myFilePath.createNewFile();
            }
            FileWriter resultFile = new FileWriter(myFilePath);
            PrintWriter myFile = new PrintWriter(resultFile);
            myFile.println(fileContent);
            myFile.close();
            resultFile.close();
        } catch (Exception e) {
            throw new Exception("��������������������������������������������", e);
        }
    }

    public static void createFile(String filePathAndName, String fileContent, String encoding) throws Exception {
        try {
            File myFilePath = new File(filePathAndName.toString());
            if (!myFilePath.exists()) {
                myFilePath.createNewFile();
            }
            PrintWriter myFile = new PrintWriter(myFilePath, encoding);
            myFile.println(fileContent);
            myFile.close();
        } catch (Exception e) {
            throw new Exception("��������������������������������������������", e);
        }
    }

    public static String createFolder(String filePath) throws Exception {
        File file = new File(filePath);
        try {
            if (file.exists() || file.mkdirs()) {
                return filePath;
            }
            throw new IOException("����������������������������" + filePath + "������������");
        } catch (Exception e) {
            throw e;
        }
    }

    public static String createFolders(String filePath) throws Exception {
        File file = new File(filePath);
        try {
            if (file.exists() || file.mkdirs()) {
                return filePath;
            }
            throw new IOException("����������������������������" + filePath + "������������");
        } catch (Exception e) {
            throw e;
        }
    }

    public static String createFolders(String folderPath, String paths) throws Exception {
        String txts = folderPath;
        try {
            StringTokenizer st = new StringTokenizer(paths, "|");
            int i = 0;
            while (st.hasMoreTokens()) {
                txts = createFolder(txts + st.nextToken().trim() + "/");
                i++;
            }
            return txts;
        } catch (Exception e) {
            throw new Exception("�������������������������������������?", e);
        }
    }

    public static void copyFile(String oldPathFile, String newPathFile) throws Exception {
        int bytesum = 0;
        try {
            if (new File(oldPathFile).exists()) {
                InputStream inStream = new FileInputStream(oldPathFile);
                FileOutputStream fs = new FileOutputStream(newPathFile);
                byte[] buffer = new byte[1444];
                while (true) {
                    int byteread = inStream.read(buffer);
                    if (byteread != -1) {
                        bytesum += byteread;
                        fs.write(buffer, 0, byteread);
                    } else {
                        inStream.close();
                        return;
                    }
                }
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public static void getAllFilePaths(String sourceFolder, ArrayList<String> fileArray) throws Exception {
        try {
            File file = new File(sourceFolder);
            if (file.isDirectory()) {
                File[] filelist = file.listFiles();
                for (int i = 0; i < filelist.length; i++) {
                    if (filelist[i].isDirectory()) {
                        getAllFilePaths(filelist[i].getAbsolutePath(), fileArray);
                    } else {
                        fileArray.add(filelist[i].getAbsolutePath());
                    }
                }
            }
        } catch (Exception e) {
            throw new Exception("���������������������������������������������������������������", e);
        }
    }

    public static void copyFolder(String sourceFolder, String destinationFolder) throws Exception {
        File temp;
        try {
            new File(destinationFolder).mkdirs();
            String[] file = new File(sourceFolder).list();
            for (int i = 0; i < file.length; i++) {
                if (sourceFolder.endsWith(File.separator)) {
                    temp = new File(sourceFolder + file[i]);
                } else {
                    temp = new File(sourceFolder + File.separator + file[i]);
                }
                if (temp.isFile()) {
                    FileInputStream input = new FileInputStream(temp);
                    FileOutputStream output = new FileOutputStream(destinationFolder + "/" + temp.getName().toString());
                    byte[] b = new byte[5120];
                    while (true) {
                        int len = input.read(b);
                        if (len == -1) {
                            break;
                        }
                        output.write(b, 0, len);
                    }
                    output.flush();
                    output.close();
                    input.close();
                }
                if (temp.isDirectory()) {
                    copyFolder(sourceFolder + "/" + file[i], destinationFolder + "/" + file[i]);
                }
            }
        } catch (Exception e) {
            throw new Exception("�������������������������������������������������������������������", e);
        }
    }

    public static void convFolder(String sourceFolderIn, String sourceDecode, String destinationFolderOut, String destEncode, String filterIn) throws Exception {
        try {
            String sourceFolder = addFolderSeparator(sourceFolderIn);
            String destinationFolder = addFolderSeparator(destinationFolderOut);
            new File(destinationFolder).mkdirs();
            String[] file = new File(sourceFolder).list();
            for (int i = 0; i < file.length; i++) {
                File temp = new File(sourceFolder + file[i]);
                String source = sourceFolder + file[i];
                String dest = destinationFolder + file[i];
                if (temp.isFile() && temp.getName().toLowerCase().indexOf(filterIn) > -1) {
                    convFile(source, sourceDecode, dest, destEncode);
                }
                if (temp.isDirectory()) {
                    convFolder(source, sourceDecode, dest, destEncode, filterIn);
                }
            }
        } catch (Exception e) {
            throw new Exception("���������������������������������������������������������������", e);
        }
    }

    public static String addFolderSeparator(String strFoldName) {
        String strTmp = strFoldName;
        if (!strTmp.endsWith(File.separator)) {
            return strTmp + "\\";
        }
        return strTmp;
    }

    /* JADX INFO: Multiple debug info for r0v4 java.io.OutputStreamWriter: [D('osr' java.io.OutputStreamWriter), D('encodingD' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r1v7 java.io.InputStreamReader: [D('isr' java.io.InputStreamReader), D('encodingS' java.lang.String)] */
    public static boolean convFile(String sourceFile, String sourceDecode, String destinationFile, String destEncode) {
        InputStreamReader isr;
        OutputStreamWriter osr;
        String encodingS = sourceDecode.trim();
        String encodingD = destEncode.trim();
        try {
            FileInputStream fs = new FileInputStream(sourceFile);
            FileOutputStream os = new FileOutputStream(destinationFile);
            if (encodingS.equals("")) {
                isr = new InputStreamReader(fs);
            } else {
                isr = new InputStreamReader(fs, encodingS);
            }
            if (encodingD.equals("")) {
                osr = new OutputStreamWriter(os);
            } else {
                osr = new OutputStreamWriter(os, encodingD);
            }
            BufferedWriter bw = new BufferedWriter(osr);
            BufferedReader br = new BufferedReader(isr);
            while (true) {
                try {
                    String data = br.readLine();
                    if (data != null) {
                        bw.write(data);
                    } else {
                        bw.flush();
                        bw.close();
                        osr.close();
                        os.close();
                        System.out.println(" inputfile size =  " + getFileLength(sourceFile, sourceDecode));
                        System.out.println(" outputfile size =  " + getFileLength(destinationFile, destEncode));
                        return true;
                    }
                } catch (IOException e) {
                    return false;
                }
            }
        } catch (IOException e2) {
            return false;
        }
    }

    public static long getFileLength(String fPath, String encoding) {
        try {
            try {
                try {
                    return new InputStreamReader(new FileInputStream(fPath), encoding).skip(10000000000L);
                } catch (IOException e) {
                    e.printStackTrace();
                    return -3;
                }
            } catch (UnsupportedEncodingException e2) {
                e2.printStackTrace();
                return -2;
            }
        } catch (FileNotFoundException e3) {
            e3.printStackTrace();
            return -1;
        }
    }

    public static void delFile(String filePathAndName) {
        File myDelFile = new File(filePathAndName);
        if (myDelFile.exists()) {
            myDelFile.delete();
        }
    }

    public static Boolean delFile_Return(String filePathAndName) {
        File myDelFile = new File(filePathAndName);
        if (myDelFile.exists()) {
            return Boolean.valueOf(myDelFile.delete());
        }
        return false;
    }

    public static void delFolder(String folderPath) throws Exception {
        try {
            delAllFile(folderPath);
            if (!new File(folderPath.toString()).delete()) {
                throw new IOException("��������������:" + folderPath + ",����������������");
            }
        } catch (IOException e) {
            throw new Exception("������������������������������", e);
        }
    }

    public static boolean delAllFile(String filePath) throws IOException {
        File file = new File(filePath);
        if (!file.exists()) {
            throw new IOException("������������������������������:" + file.getName());
        }
        boolean rslt = file.delete();
        if (!rslt) {
            File[] subs = file.listFiles();
            for (int i = 0; i <= subs.length - 1; i++) {
                if (subs[i].isDirectory()) {
                    delAllFile(subs[i].toString());
                }
                rslt = subs[i].delete();
            }
        }
        if (rslt) {
            return true;
        }
        throw new IOException("����������������:" + file.getName());
    }

    public static void moveFile(String oldPath, String newPath) throws Exception {
        try {
            copyFile(oldPath, newPath);
            delFile(oldPath);
        } catch (Exception e) {
            throw e;
        }
    }

    public static void moveFolder(String oldPath, String newPath) {
        try {
            copyFolder(oldPath, newPath);
            delFolder(oldPath);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String getClassPath() {
        String strClassFileName;
        Class<FileUtil> cls = FileUtil.class;
        Class<FileUtil> cls2 = FileUtil.class;
        String strClassName = cls.getName();
        String strPackageName = "";
        Class<FileUtil> cls3 = FileUtil.class;
        if (cls.getPackage() != null) {
            Class<FileUtil> cls4 = FileUtil.class;
            strPackageName = cls.getPackage().getName();
        }
        if (!"".equals(strPackageName)) {
            strClassFileName = strClassName.substring(strPackageName.length() + 1, strClassName.length());
        } else {
            strClassFileName = strClassName;
        }
        Class<FileUtil> cls5 = FileUtil.class;
        String strURL = cls.getResource(strClassFileName + ".class").toString();
        return strURL.substring(strURL.indexOf(47) + 1, strURL.lastIndexOf(47)) + "/";
    }

    public static String getProject(String projectName) {
        String classPath = getClassPath();
        return classPath.substring(0, classPath.indexOf(projectName) + projectName.length() + 1);
    }

    public static String getStr(String path) {
        String result = "";
        char[] pathChar = path.toCharArray();
        for (int i = 0; i < pathChar.length; i++) {
            if (pathChar[i] == '\\') {
                pathChar[i] = '/';
            }
            result = result + pathChar[i];
        }
        return result;
    }

    public static boolean fileExist(String filePath) {
        return new File(filePath).exists();
    }

    public static boolean folderExistOrCreate(String folderPath) {
        File file = new File(folderPath);
        if (file.exists()) {
            return true;
        }
        file.mkdirs();
        return true;
    }

    public static String getFileName(String filepath) {
        File file = new File(filepath);
        if (file.exists()) {
            return file.getName();
        }
        return null;
    }

    public static File[] sortFiles(File[] files) {
        File[] returnFiles = null;
        if (files != null) {
            returnFiles = new File[files.length];
            String[] filesTime = new String[files.length];
            for (int i = 0; i < files.length; i++) {
                filesTime[i] = files[i].lastModified() + files[i].getName();
            }
            Collections.sort(Arrays.asList(filesTime));
            Collections.reverse(Arrays.asList(filesTime));
            for (int i2 = 0; i2 < filesTime.length; i2++) {
                for (int j = 0; j < files.length; j++) {
                    if ((files[j].lastModified() + files[j].getName()).equals(filesTime[i2])) {
                        returnFiles[i2] = files[j];
                    }
                }
            }
        }
        return returnFiles;
    }

    public static long getFileSize(String filePath) {
        if (fileExist(filePath)) {
            return new File(filePath).length();
        }
        return -1;
    }

    public static boolean fileRename(String fileSrc, String fileDest) {
        return new File(fileSrc).renameTo(new File(fileDest));
    }

    /* JADX INFO: Multiple debug info for r0v1 java.io.File: [D('outFile' java.io.File), D('fileDir' java.io.File)] */
    /* JADX INFO: Multiple debug info for r9v18 int: [D('b' byte[]), D('i' int)] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:20:0x0058=Splitter:B:20:0x0058, B:29:0x0068=Splitter:B:29:0x0068} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean Filecombine(java.lang.String r8, java.lang.String r9) {
        /*
            java.io.File r0 = new java.io.File
            r0.<init>(r8)
            boolean r1 = r0.exists()
            if (r1 == 0) goto L_0x0098
            java.io.File[] r1 = r0.listFiles()
            java.io.File r0 = new java.io.File
            r0.<init>(r9)
            r9 = 0
            java.io.FileOutputStream r4 = new java.io.FileOutputStream     // Catch:{ FileNotFoundException -> 0x0060 }
            r4.<init>(r0)     // Catch:{ FileNotFoundException -> 0x0060 }
            r0 = 0
            r9 = 0
            r2 = 0
            r5 = r2
            r2 = r0
        L_0x001f:
            int r0 = r1.length
            if (r5 >= r0) goto L_0x008a
            java.io.File r0 = new java.io.File     // Catch:{ FileNotFoundException -> 0x00c3, IOException -> 0x0066, all -> 0x0076 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x00c3, IOException -> 0x0066, all -> 0x0076 }
            r3.<init>()     // Catch:{ FileNotFoundException -> 0x00c3, IOException -> 0x0066, all -> 0x0076 }
            java.lang.StringBuilder r3 = r3.append(r8)     // Catch:{ FileNotFoundException -> 0x00c3, IOException -> 0x0066, all -> 0x0076 }
            int r6 = r5 + 1
            java.lang.StringBuilder r3 = r3.append(r6)     // Catch:{ FileNotFoundException -> 0x00c3, IOException -> 0x0066, all -> 0x0076 }
            java.lang.String r6 = ".x"
            java.lang.StringBuilder r3 = r3.append(r6)     // Catch:{ FileNotFoundException -> 0x00c3, IOException -> 0x0066, all -> 0x0076 }
            java.lang.String r3 = r3.toString()     // Catch:{ FileNotFoundException -> 0x00c3, IOException -> 0x0066, all -> 0x0076 }
            r0.<init>(r3)     // Catch:{ FileNotFoundException -> 0x00c3, IOException -> 0x0066, all -> 0x0076 }
            java.io.FileInputStream r3 = new java.io.FileInputStream     // Catch:{ FileNotFoundException -> 0x00c6, IOException -> 0x00bb, all -> 0x00a9 }
            r3.<init>(r0)     // Catch:{ FileNotFoundException -> 0x00c6, IOException -> 0x00bb, all -> 0x00a9 }
            r9 = 1024(0x400, float:1.435E-42)
            byte[] r9 = new byte[r9]     // Catch:{ FileNotFoundException -> 0x0055, IOException -> 0x00bf, all -> 0x00af }
        L_0x0049:
            int r2 = r3.read(r9)     // Catch:{ FileNotFoundException -> 0x0055, IOException -> 0x00bf, all -> 0x00af }
            r6 = -1
            if (r2 == r6) goto L_0x007e
            r6 = 0
            r4.write(r9, r6, r2)     // Catch:{ FileNotFoundException -> 0x0055, IOException -> 0x00bf, all -> 0x00af }
            goto L_0x0049
        L_0x0055:
            r8 = move-exception
            r9 = r0
            r0 = r3
        L_0x0058:
            r8.printStackTrace()     // Catch:{ all -> 0x00b5 }
            r8 = 0
            r0.close()     // Catch:{ IOException -> 0x009f }
        L_0x005f:
            return r8
        L_0x0060:
            r8 = move-exception
            r8.printStackTrace()
            r8 = 0
            goto L_0x005f
        L_0x0066:
            r8 = move-exception
            r0 = r2
        L_0x0068:
            r8.printStackTrace()     // Catch:{ all -> 0x00b5 }
            r8 = 0
            r0.close()     // Catch:{ IOException -> 0x0070 }
            goto L_0x005f
        L_0x0070:
            r8 = move-exception
            r8.printStackTrace()
        L_0x0074:
            r8 = 0
            goto L_0x005f
        L_0x0076:
            r8 = move-exception
            r0 = r8
            r8 = r9
            r9 = r2
        L_0x007a:
            r9.close()     // Catch:{ IOException -> 0x009a }
            throw r0
        L_0x007e:
            r3.close()     // Catch:{ IOException -> 0x00a4 }
            r0.delete()
            int r9 = r5 + 1
            r5 = r9
            r2 = r3
            r9 = r0
            goto L_0x001f
        L_0x008a:
            r4.flush()     // Catch:{ IOException -> 0x0092 }
            r4.close()     // Catch:{ IOException -> 0x0092 }
            r8 = 1
            goto L_0x005f
        L_0x0092:
            r8 = move-exception
            r8.printStackTrace()
            r8 = 0
            goto L_0x005f
        L_0x0098:
            r8 = 0
            goto L_0x005f
        L_0x009a:
            r8 = move-exception
            r8.printStackTrace()
            goto L_0x0074
        L_0x009f:
            r8 = move-exception
            r8.printStackTrace()
            goto L_0x0074
        L_0x00a4:
            r8 = move-exception
            r8.printStackTrace()
            goto L_0x0074
        L_0x00a9:
            r8 = move-exception
            r9 = r2
            r7 = r0
            r0 = r8
            r8 = r7
            goto L_0x007a
        L_0x00af:
            r8 = move-exception
            r9 = r3
            r7 = r0
            r0 = r8
            r8 = r7
            goto L_0x007a
        L_0x00b5:
            r8 = move-exception
            r7 = r8
            r8 = r9
            r9 = r0
            r0 = r7
            goto L_0x007a
        L_0x00bb:
            r8 = move-exception
            r9 = r0
            r0 = r2
            goto L_0x0068
        L_0x00bf:
            r8 = move-exception
            r9 = r0
            r0 = r3
            goto L_0x0068
        L_0x00c3:
            r8 = move-exception
            r0 = r2
            goto L_0x0058
        L_0x00c6:
            r8 = move-exception
            r9 = r0
            r0 = r2
            goto L_0x0058
        */
        throw new UnsupportedOperationException("Method not decompiled: com.art.util.FileUtil.Filecombine(java.lang.String, java.lang.String):boolean");
    }

    /* JADX INFO: Multiple debug info for r7v1 java.io.File: [D('fList' java.io.File[]), D('outFile' java.io.File)] */
    /* JADX INFO: Multiple debug info for r7v38 int: [D('b' byte[]), D('i' int)] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:31:0x0075=Splitter:B:31:0x0075, B:21:0x005d=Splitter:B:21:0x005d} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean Filecombine(java.io.File[] r7, java.lang.String r8) {
        /*
            java.io.PrintStream r0 = java.lang.System.out
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "FileUtil.Filecombine: files="
            java.lang.StringBuilder r1 = r1.append(r2)
            int r2 = r7.length
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = ", savePath="
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r8)
            java.lang.String r1 = r1.toString()
            r0.println(r1)
            r0 = r7
            java.io.File r7 = new java.io.File
            r7.<init>(r8)
            r8 = 0
            boolean r1 = r7.exists()     // Catch:{ FileNotFoundException -> 0x0067 }
            if (r1 != 0) goto L_0x0037
            java.io.File r1 = r7.getParentFile()     // Catch:{ FileNotFoundException -> 0x0067 }
            r1.mkdirs()     // Catch:{ FileNotFoundException -> 0x0067 }
        L_0x0037:
            java.io.FileOutputStream r3 = new java.io.FileOutputStream     // Catch:{ FileNotFoundException -> 0x0067 }
            r3.<init>(r7)     // Catch:{ FileNotFoundException -> 0x0067 }
            r8 = 0
            r7 = 0
            r1 = 0
            r4 = r1
            r1 = r8
        L_0x0041:
            int r8 = r0.length
            if (r4 >= r8) goto L_0x0092
            r8 = r0[r4]     // Catch:{ FileNotFoundException -> 0x00d1, IOException -> 0x0070, all -> 0x007f }
            java.io.FileInputStream r2 = new java.io.FileInputStream     // Catch:{ FileNotFoundException -> 0x00d7, IOException -> 0x00cb, all -> 0x00bb }
            r2.<init>(r8)     // Catch:{ FileNotFoundException -> 0x00d7, IOException -> 0x00cb, all -> 0x00bb }
            r7 = 1024(0x400, float:1.435E-42)
            byte[] r7 = new byte[r7]     // Catch:{ FileNotFoundException -> 0x005b, IOException -> 0x00ce, all -> 0x00c0 }
        L_0x004f:
            int r1 = r2.read(r7)     // Catch:{ FileNotFoundException -> 0x005b, IOException -> 0x00ce, all -> 0x00c0 }
            r5 = -1
            if (r1 == r5) goto L_0x0086
            r5 = 0
            r3.write(r7, r5, r1)     // Catch:{ FileNotFoundException -> 0x005b, IOException -> 0x00ce, all -> 0x00c0 }
            goto L_0x004f
        L_0x005b:
            r7 = move-exception
            r0 = r2
        L_0x005d:
            r7.printStackTrace()     // Catch:{ all -> 0x00c5 }
            r7 = 0
            r0.close()     // Catch:{ IOException -> 0x00ac }
            r8 = r7
            r7 = r3
        L_0x0066:
            return r8
        L_0x0067:
            r7 = move-exception
            r7.printStackTrace()
            r7 = 0
            r6 = r8
            r8 = r7
            r7 = r6
            goto L_0x0066
        L_0x0070:
            r8 = move-exception
            r0 = r1
            r6 = r7
            r7 = r8
            r8 = r6
        L_0x0075:
            r7.printStackTrace()     // Catch:{ all -> 0x00c5 }
            r7 = 0
            r0.close()     // Catch:{ IOException -> 0x00b1 }
            r8 = r7
            r7 = r3
            goto L_0x0066
        L_0x007f:
            r8 = move-exception
            r0 = r8
            r8 = r1
        L_0x0082:
            r8.close()     // Catch:{ IOException -> 0x00a4 }
            throw r0
        L_0x0086:
            r2.close()     // Catch:{ IOException -> 0x00b6 }
            r8.delete()
            int r7 = r4 + 1
            r4 = r7
            r1 = r2
            r7 = r8
            goto L_0x0041
        L_0x0092:
            r3.flush()     // Catch:{ IOException -> 0x009c }
            r3.close()     // Catch:{ IOException -> 0x009c }
            r7 = 1
            r8 = r7
            r7 = r3
            goto L_0x0066
        L_0x009c:
            r7 = move-exception
            r7.printStackTrace()
            r7 = 0
            r8 = r7
            r7 = r3
            goto L_0x0066
        L_0x00a4:
            r7 = move-exception
            r7.printStackTrace()
        L_0x00a8:
            r7 = 0
            r8 = r7
            r7 = r3
            goto L_0x0066
        L_0x00ac:
            r7 = move-exception
            r7.printStackTrace()
            goto L_0x00a8
        L_0x00b1:
            r7 = move-exception
            r7.printStackTrace()
            goto L_0x00a8
        L_0x00b6:
            r7 = move-exception
            r7.printStackTrace()
            goto L_0x00a8
        L_0x00bb:
            r7 = move-exception
            r0 = r7
            r7 = r8
            r8 = r1
            goto L_0x0082
        L_0x00c0:
            r7 = move-exception
            r0 = r7
            r7 = r8
            r8 = r2
            goto L_0x0082
        L_0x00c5:
            r7 = move-exception
            r6 = r7
            r7 = r8
            r8 = r0
            r0 = r6
            goto L_0x0082
        L_0x00cb:
            r7 = move-exception
            r0 = r1
            goto L_0x0075
        L_0x00ce:
            r7 = move-exception
            r0 = r2
            goto L_0x0075
        L_0x00d1:
            r8 = move-exception
            r0 = r1
            r6 = r7
            r7 = r8
            r8 = r6
            goto L_0x005d
        L_0x00d7:
            r7 = move-exception
            r0 = r1
            goto L_0x005d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.art.util.FileUtil.Filecombine(java.io.File[], java.lang.String):boolean");
    }

    public static String FileNameFilter(String strFileName) {
        return strFileName.replace("?", "").replace("+", "").replace("\"", "").replace("$", "").replace("(", "").replace(")", "").replace("%", "");
    }

    public static String[] convFileSize(long size) {
        String str = "";
        if (size >= 1024) {
            str = "KB";
            size /= 1024;
            if (size >= 1024) {
                str = "MB";
                size /= 1024;
            }
        }
        DecimalFormat formatter = new DecimalFormat();
        formatter.setGroupingSize(3);
        return new String[]{formatter.format(size), str};
    }

    public static String getFolder(String fileFullPath) {
        return fileFullPath.substring(0, fileFullPath.lastIndexOf("\\") + 1);
    }

    public static boolean recreateFolder(String foldPath) {
        try {
            delAllFile(foldPath);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            delFolder(foldPath);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        try {
            createFolders(foldPath);
            return true;
        } catch (Exception e3) {
            e3.printStackTrace();
            return false;
        }
    }

    public static String readTxtcoding(String strFile) {
        String strEnc = "GBK";
        File file = new File(strFile);
        if (file == null || !file.exists()) {
            return "";
        }
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        byte[] btBom = new byte[4];
        try {
            fis.read(btBom);
        } catch (IOException e2) {
            e2.printStackTrace();
        }
        if (btBom[0] == -1 && btBom[1] == -2) {
            strEnc = "unicode";
        } else if (btBom[0] == -2 && btBom[1] == -1) {
            strEnc = "UTF-16BE";
        } else if (btBom[0] == -17 && btBom[1] == -69 && btBom[2] == -65) {
            strEnc = "UTF-8";
        } else if (btBom[0] == -1 && btBom[1] == -2 && btBom[2] == 0 && btBom[3] == 0) {
            strEnc = "UTF-32LE";
        } else if (btBom[0] == 0 && btBom[1] == 0 && btBom[2] == -2 && btBom[3] == -1) {
            strEnc = "UTF-32BE";
        }
        if (fis != null) {
            try {
                fis.close();
            } catch (IOException e3) {
                e3.printStackTrace();
            }
        }
        return strEnc;
    }

    public static byte[] readFile(String strPathFile) {
        File file = new File(strPathFile);
        if (!file.exists()) {
            return null;
        }
        byte[] fileBuffer = new byte[((int) file.length())];
        try {
            InputStream inStream = new FileInputStream(strPathFile);
            byte[] buffer = new byte[1024];
            int bytesum = 0;
            do {
                try {
                    int byteread = inStream.read(buffer);
                    if (byteread == -1) {
                        break;
                    }
                    for (int i = 0; i < byteread; i++) {
                        fileBuffer[bytesum + i] = buffer[i];
                    }
                    bytesum += byteread;
                } catch (IOException e) {
                    e.printStackTrace();
                    return null;
                }
            } while (((long) bytesum) != file.length());
            inStream.close();
            return fileBuffer;
        } catch (FileNotFoundException e2) {
            e2.printStackTrace();
            return null;
        }
    }
}
