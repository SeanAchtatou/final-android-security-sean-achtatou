package com.art.util;

import android.os.Environment;
import android.util.Log;
import com.wpview.yeexm.R;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.http.HttpResponse;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;

public class Protocals {
    public static final String TAG = "Protocals";
    private static Protocals mProtocals = null;
    public static final String strHomeRootURL = "http://www.android2020.com/wp_hdResource/";

    public static Protocals getInstance() {
        Protocals protocals;
        synchronized (Protocals.class) {
            if (mProtocals == null) {
                mProtocals = new Protocals();
            }
            protocals = mProtocals;
        }
        return protocals;
    }

    private HttpResponse executePost(String strURL) {
        try {
            if (strURL.length() <= 0) {
                return null;
            }
            BasicHttpParams bhParams = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(bhParams, 3000);
            HttpConnectionParams.setSoTimeout(bhParams, 6000);
            return new DefaultHttpClient(bhParams).execute(new HttpPost(strURL));
        } catch (Exception e) {
            return null;
        }
    }

    private HttpResponse executePost(String strURL, List list) {
        try {
            if (strURL.length() <= 0) {
                return null;
            }
            BasicHttpParams bhParams = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(bhParams, 3000);
            HttpConnectionParams.setSoTimeout(bhParams, 6000);
            DefaultHttpClient defaultClient = new DefaultHttpClient(bhParams);
            HttpPost httpPost = new HttpPost(strURL);
            httpPost.setEntity(new UrlEncodedFormEntity(list, "utf-8"));
            return defaultClient.execute(httpPost);
        } catch (Exception e) {
            return null;
        }
    }

    /* JADX WARN: Type inference failed for: r7v2, types: [java.net.URLConnection] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String getWebContent(java.lang.String r11) {
        /*
            r10 = this;
            r2 = 0
            java.lang.StringBuffer r4 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x0037 }
            r4.<init>()     // Catch:{ Exception -> 0x0037 }
            java.net.URL r6 = new java.net.URL     // Catch:{ Exception -> 0x0037 }
            r6.<init>(r11)     // Catch:{ Exception -> 0x0037 }
            java.net.URLConnection r7 = r6.openConnection()     // Catch:{ Exception -> 0x0037 }
            r0 = r7
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x0037 }
            r2 = r0
            r7 = 1
            r2.setDoOutput(r7)     // Catch:{ Exception -> 0x0037 }
            java.io.BufferedReader r1 = new java.io.BufferedReader     // Catch:{ Exception -> 0x0037 }
            java.io.InputStreamReader r7 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x0037 }
            java.io.InputStream r8 = r2.getInputStream()     // Catch:{ Exception -> 0x0037 }
            java.lang.String r9 = "gb2312"
            r7.<init>(r8, r9)     // Catch:{ Exception -> 0x0037 }
            r1.<init>(r7)     // Catch:{ Exception -> 0x0037 }
        L_0x0027:
            java.lang.String r5 = r1.readLine()     // Catch:{ Exception -> 0x0037 }
            if (r5 == 0) goto L_0x0040
            java.lang.StringBuffer r7 = r4.append(r5)     // Catch:{ Exception -> 0x0037 }
            java.lang.String r8 = "\r\n"
            r7.append(r8)     // Catch:{ Exception -> 0x0037 }
            goto L_0x0027
        L_0x0037:
            r7 = move-exception
            r3 = r7
            if (r2 == 0) goto L_0x003e
            r2.disconnect()
        L_0x003e:
            r7 = 0
        L_0x003f:
            return r7
        L_0x0040:
            r1.close()     // Catch:{ Exception -> 0x0037 }
            r2.disconnect()     // Catch:{ Exception -> 0x0037 }
            java.lang.String r7 = r4.toString()     // Catch:{ Exception -> 0x0037 }
            goto L_0x003f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.art.util.Protocals.getWebContent(java.lang.String):java.lang.String");
    }

    /* JADX WARN: Type inference failed for: r8v0, types: [java.net.URLConnection] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String downWebContent(java.lang.String r25, java.lang.String r26) {
        /*
            r24 = this;
            r13 = 0
            r12 = 0
            r19 = 0
            r22 = 0
            java.lang.String r23 = "/"
            r0 = r26
            r1 = r23
            int r23 = r0.lastIndexOf(r1)
            r0 = r26
            r1 = r22
            r2 = r23
            java.lang.String r18 = r0.substring(r1, r2)
            com.art.util.FileUtil.createFolders(r18)     // Catch:{ Exception -> 0x0076 }
        L_0x001d:
            java.io.FileOutputStream r14 = new java.io.FileOutputStream     // Catch:{ FileNotFoundException -> 0x007b }
            r0 = r14
            r1 = r26
            r0.<init>(r1)     // Catch:{ FileNotFoundException -> 0x007b }
            r13 = r14
        L_0x0026:
            r7 = 0
            java.lang.StringBuffer r15 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x00bd }
            r15.<init>()     // Catch:{ Exception -> 0x00bd }
            java.net.URL r21 = new java.net.URL     // Catch:{ Exception -> 0x00bd }
            r0 = r21
            r1 = r25
            r0.<init>(r1)     // Catch:{ Exception -> 0x00bd }
            java.net.URLConnection r8 = r21.openConnection()     // Catch:{ Exception -> 0x00bd }
            r0 = r8
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x00bd }
            r7 = r0
            r22 = 1
            r0 = r7
            r1 = r22
            r0.setDoOutput(r1)     // Catch:{ Exception -> 0x00bd }
            int r8 = r7.getContentLength()     // Catch:{ Exception -> 0x00bd }
            byte[] r12 = new byte[r8]     // Catch:{ Exception -> 0x00bd }
            java.io.InputStream r17 = r7.getInputStream()     // Catch:{ Exception -> 0x00bd }
            r22 = 1024(0x400, float:1.435E-42)
            r0 = r22
            byte[] r0 = new byte[r0]     // Catch:{ Exception -> 0x00bd }
            r4 = r0
            r5 = -1
            r6 = 0
        L_0x0058:
            r0 = r17
            r1 = r4
            int r5 = r0.read(r1)     // Catch:{ Exception -> 0x00bd }
            r22 = -1
            r0 = r5
            r1 = r22
            if (r0 == r1) goto L_0x0096
            r16 = 0
        L_0x0068:
            r0 = r16
            r1 = r5
            if (r0 >= r1) goto L_0x0082
            int r22 = r6 + r16
            byte r23 = r4[r16]     // Catch:{ Exception -> 0x00bd }
            r12[r22] = r23     // Catch:{ Exception -> 0x00bd }
            int r16 = r16 + 1
            goto L_0x0068
        L_0x0076:
            r11 = move-exception
            r11.printStackTrace()
            goto L_0x001d
        L_0x007b:
            r22 = move-exception
            r9 = r22
            r9.printStackTrace()
            goto L_0x0026
        L_0x0082:
            if (r13 == 0) goto L_0x00b5
            byte[] r22 = com.art.util.XMEnDecrypt.XMEncryptString(r4)     // Catch:{ Exception -> 0x00bd }
            r23 = 0
            r0 = r13
            r1 = r22
            r2 = r23
            r3 = r5
            r0.write(r1, r2, r3)     // Catch:{ Exception -> 0x00bd }
        L_0x0093:
            int r6 = r6 + r5
            if (r6 != r8) goto L_0x0058
        L_0x0096:
            r13.flush()     // Catch:{ Exception -> 0x00bd }
            r17.close()     // Catch:{ Exception -> 0x00bd }
            r7.disconnect()     // Catch:{ Exception -> 0x00bd }
            r21 = 0
            r0 = r12
            int r0 = r0.length     // Catch:{ Exception -> 0x00bd }
            r22 = r0
            if (r22 <= 0) goto L_0x00b1
            java.lang.String r20 = new java.lang.String     // Catch:{ Exception -> 0x00bd }
            r0 = r20
            r1 = r12
            r0.<init>(r1)     // Catch:{ Exception -> 0x00bd }
            r19 = r20
        L_0x00b1:
            r12 = 0
            r22 = r19
        L_0x00b4:
            return r22
        L_0x00b5:
            java.io.PrintStream r22 = java.lang.System.out     // Catch:{ Exception -> 0x00bd }
            java.lang.String r23 = "fsOutput is null"
            r22.println(r23)     // Catch:{ Exception -> 0x00bd }
            goto L_0x0093
        L_0x00bd:
            r22 = move-exception
            r9 = r22
            if (r7 == 0) goto L_0x00c5
            r7.disconnect()
        L_0x00c5:
            r13.close()     // Catch:{ IOException -> 0x00ce }
            com.art.util.FileUtil.delFile(r26)     // Catch:{ IOException -> 0x00ce }
        L_0x00cb:
            r22 = 0
            goto L_0x00b4
        L_0x00ce:
            r22 = move-exception
            r10 = r22
            r10.printStackTrace()
            goto L_0x00cb
        */
        throw new UnsupportedOperationException("Method not decompiled: com.art.util.Protocals.downWebContent(java.lang.String, java.lang.String):java.lang.String");
    }

    /* Debug info: failed to restart local var, previous not found, register: 24 */
    /* JADX WARN: Type inference failed for: r8v2, types: [java.net.URLConnection] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public byte[] getWebContent(java.lang.String r25, java.lang.String r26) {
        /*
            r24 = this;
            java.io.PrintStream r21 = java.lang.System.out
            java.lang.StringBuilder r22 = new java.lang.StringBuilder
            r22.<init>()
            java.lang.String r23 = "getWebContent: url="
            java.lang.StringBuilder r22 = r22.append(r23)
            r0 = r22
            r1 = r25
            java.lang.StringBuilder r22 = r0.append(r1)
            java.lang.String r23 = ", local="
            java.lang.StringBuilder r22 = r22.append(r23)
            r0 = r22
            r1 = r26
            java.lang.StringBuilder r22 = r0.append(r1)
            java.lang.String r22 = r22.toString()
            r21.println(r22)
            r12 = 0
            r11 = 0
            r21 = 0
            java.lang.String r22 = "/"
            r0 = r26
            r1 = r22
            int r22 = r0.lastIndexOf(r1)
            r0 = r26
            r1 = r21
            r2 = r22
            java.lang.String r19 = r0.substring(r1, r2)
            com.art.util.FileUtil.createFolders(r19)     // Catch:{ Exception -> 0x00a8 }
        L_0x0045:
            java.io.FileOutputStream r13 = new java.io.FileOutputStream     // Catch:{ FileNotFoundException -> 0x00ad }
            r0 = r13
            r1 = r26
            r0.<init>(r1)     // Catch:{ FileNotFoundException -> 0x00ad }
            r12 = r13
        L_0x004e:
            r7 = 0
            java.lang.StringBuffer r14 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x00ca }
            r14.<init>()     // Catch:{ Exception -> 0x00ca }
            java.net.URL r20 = new java.net.URL     // Catch:{ Exception -> 0x00ca }
            r0 = r20
            r1 = r25
            r0.<init>(r1)     // Catch:{ Exception -> 0x00ca }
            java.net.URLConnection r8 = r20.openConnection()     // Catch:{ Exception -> 0x00ca }
            r0 = r8
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x00ca }
            r7 = r0
            r21 = 1
            r0 = r7
            r1 = r21
            r0.setDoOutput(r1)     // Catch:{ Exception -> 0x00ca }
            r21 = 10000(0x2710, float:1.4013E-41)
            r0 = r7
            r1 = r21
            r0.setConnectTimeout(r1)     // Catch:{ Exception -> 0x00ca }
            java.util.ArrayList r18 = new java.util.ArrayList     // Catch:{ Exception -> 0x00ca }
            r18.<init>()     // Catch:{ Exception -> 0x00ca }
            java.io.InputStream r16 = r7.getInputStream()     // Catch:{ Exception -> 0x00ca }
            r21 = 1024(0x400, float:1.435E-42)
            r0 = r21
            byte[] r0 = new byte[r0]     // Catch:{ Exception -> 0x00ca }
            r4 = r0
            r5 = -1
            r6 = 0
        L_0x0087:
            r0 = r16
            r1 = r4
            int r5 = r0.read(r1)     // Catch:{ Exception -> 0x00ca }
            r21 = -1
            r0 = r5
            r1 = r21
            if (r0 == r1) goto L_0x00de
            r15 = 0
        L_0x0096:
            if (r15 >= r5) goto L_0x00b4
            byte r21 = r4[r15]     // Catch:{ Exception -> 0x00ca }
            java.lang.Byte r21 = java.lang.Byte.valueOf(r21)     // Catch:{ Exception -> 0x00ca }
            r0 = r18
            r1 = r21
            r0.add(r1)     // Catch:{ Exception -> 0x00ca }
            int r15 = r15 + 1
            goto L_0x0096
        L_0x00a8:
            r10 = move-exception
            r10.printStackTrace()
            goto L_0x0045
        L_0x00ad:
            r21 = move-exception
            r8 = r21
            r8.printStackTrace()
            goto L_0x004e
        L_0x00b4:
            if (r12 == 0) goto L_0x00c2
            r21 = 0
            r0 = r12
            r1 = r4
            r2 = r21
            r3 = r5
            r0.write(r1, r2, r3)     // Catch:{ Exception -> 0x00ca }
        L_0x00c0:
            int r6 = r6 + r5
            goto L_0x0087
        L_0x00c2:
            java.io.PrintStream r21 = java.lang.System.out     // Catch:{ Exception -> 0x00ca }
            java.lang.String r22 = "fsOutput is null"
            r21.println(r22)     // Catch:{ Exception -> 0x00ca }
            goto L_0x00c0
        L_0x00ca:
            r21 = move-exception
            r8 = r21
            r8.printStackTrace()
            if (r7 == 0) goto L_0x00d5
            r7.disconnect()
        L_0x00d5:
            r12.close()     // Catch:{ IOException -> 0x0116 }
            com.art.util.FileUtil.delFile(r26)     // Catch:{ IOException -> 0x0116 }
        L_0x00db:
            r21 = 0
        L_0x00dd:
            return r21
        L_0x00de:
            int r21 = r18.size()     // Catch:{ Exception -> 0x00ca }
            r0 = r21
            byte[] r0 = new byte[r0]     // Catch:{ Exception -> 0x00ca }
            r11 = r0
            r17 = 0
        L_0x00e9:
            int r21 = r18.size()     // Catch:{ Exception -> 0x00ca }
            r0 = r17
            r1 = r21
            if (r0 >= r1) goto L_0x0106
            r0 = r18
            r1 = r17
            java.lang.Object r24 = r0.get(r1)     // Catch:{ Exception -> 0x00ca }
            java.lang.Byte r24 = (java.lang.Byte) r24     // Catch:{ Exception -> 0x00ca }
            byte r21 = r24.byteValue()     // Catch:{ Exception -> 0x00ca }
            r11[r17] = r21     // Catch:{ Exception -> 0x00ca }
            int r17 = r17 + 1
            goto L_0x00e9
        L_0x0106:
            r12.flush()     // Catch:{ Exception -> 0x00ca }
            r16.close()     // Catch:{ Exception -> 0x00ca }
            r7.disconnect()     // Catch:{ Exception -> 0x00ca }
            r20 = 0
            r18 = 0
            r21 = r11
            goto L_0x00dd
        L_0x0116:
            r21 = move-exception
            r9 = r21
            r9.printStackTrace()
            goto L_0x00db
        */
        throw new UnsupportedOperationException("Method not decompiled: com.art.util.Protocals.getWebContent(java.lang.String, java.lang.String):byte[]");
    }

    public Matcher RegMatchSplit(String strContent, String strRegex) {
        return Pattern.compile(strRegex, 10).matcher(strContent);
    }

    public List getCategories(String strWebImgRootURL, boolean bHome) {
        String strRoot = "/sdcard";
        if (!Environment.getExternalStorageState().equals("mounted")) {
            strRoot = "/data";
        }
        String strRootFold = strRoot + Constant.WPPARENT + Constant.WPPACKAGE + "/";
        if (!bHome) {
            strRootFold = strRoot + Constant.WPPARENT + Constant.WPCOLLECTION + "/";
        }
        return getCategories(strWebImgRootURL, strHomeRootURL, strRootFold);
    }

    public List getCategories(String strWebImgRootURL, String strRootURL, String strRootFold) {
        String strLocal;
        UnsupportedEncodingException e1;
        System.out.println("getCategories:url=" + strWebImgRootURL + ", rooturl=" + strRootURL + ", strRootFold=" + strRootFold);
        String strRoot = "/sdcard";
        if (!Environment.getExternalStorageState().equals("mounted")) {
            strRoot = "/data";
        }
        if (strWebImgRootURL.indexOf(strRoot + Constant.WPPARENT) > -1) {
            strLocal = strWebImgRootURL;
        } else {
            strLocal = strRootFold + strWebImgRootURL.substring(strRootURL.length());
        }
        String strContent = "";
        byte[] bContent = null;
        if (FileUtil.fileExist(strLocal + "index.xml")) {
            bContent = FileUtil.readFile(strLocal + "index.xml");
        }
        if (bContent == null) {
            try {
                if (!strLocal.endsWith("/")) {
                    strLocal = strLocal + "/";
                }
                FileUtil.createFolder(strLocal);
            } catch (Exception e) {
                e.printStackTrace();
            }
            bContent = getWebContent(strWebImgRootURL + "index.xml", strLocal + "index.xml");
        }
        if (bContent != null) {
            System.out.println("xml size=" + bContent.length);
            byte[] btBuffers = XMEnDecrypt.XMDecryptString(bContent);
            try {
                String str = new String(btBuffers, 0, btBuffers.length, "utf-8");
                try {
                    System.out.println("strContent=" + str);
                    strContent = str;
                } catch (UnsupportedEncodingException e2) {
                    e1 = e2;
                    strContent = str;
                    e1.printStackTrace();
                    AlbumReturn albumReturn = new AlbumReturn();
                    albumReturn.parse(strContent, "<Name>(.*?)</Name><Type>(.*?)</Type><CoverImg>(.*?)</CoverImg><Category>(.*?)</Category>");
                    return albumReturn.mlstAlbums;
                }
            } catch (UnsupportedEncodingException e3) {
                e1 = e3;
                e1.printStackTrace();
                AlbumReturn albumReturn2 = new AlbumReturn();
                albumReturn2.parse(strContent, "<Name>(.*?)</Name><Type>(.*?)</Type><CoverImg>(.*?)</CoverImg><Category>(.*?)</Category>");
                return albumReturn2.mlstAlbums;
            }
        } else {
            strContent = "";
            System.out.println("xml size=0");
        }
        AlbumReturn albumReturn22 = new AlbumReturn();
        albumReturn22.parse(strContent, "<Name>(.*?)</Name><Type>(.*?)</Type><CoverImg>(.*?)</CoverImg><Category>(.*?)</Category>");
        return albumReturn22.mlstAlbums;
    }

    private String getRelativePath(String strURL, String strRootURL) {
        return strURL.substring(strRootURL.length());
    }

    public class AlbumReturn implements Serializable {
        public int miTotal = 0;
        public List mlstAlbums = new ArrayList();

        public AlbumReturn() {
        }

        public void parse(String strContent, String strRegex) {
            if (strContent != null && !strContent.equals("")) {
                this.mlstAlbums.clear();
                this.miTotal = 0;
                Matcher matcher = Protocals.this.RegMatchSplit(strContent, strRegex);
                while (matcher.find()) {
                    Album album = new Album();
                    int iGroupNum = matcher.groupCount();
                    for (int iIndex = 1; iIndex <= iGroupNum; iIndex++) {
                        switch (iIndex) {
                            case 1:
                                album.name = matcher.group(iIndex);
                                break;
                            case R.styleable.com_admob_android_ads_AdView_secondaryTextColor /*2*/:
                                album.bIcon = matcher.group(iIndex).toString().equalsIgnoreCase("Fold");
                                break;
                            case R.styleable.com_admob_android_ads_AdView_keywords /*3*/:
                                album.image = matcher.group(iIndex);
                                break;
                            case R.styleable.com_admob_android_ads_AdView_refreshInterval /*4*/:
                                album.category = matcher.group(iIndex);
                                break;
                        }
                    }
                    this.mlstAlbums.add(album);
                    this.miTotal++;
                }
            }
        }
    }

    public class Album {
        public long addtime;
        public boolean bIcon;
        public String category;
        public String image;
        public String indexurl;
        public String mini;
        public String name;

        public Album() {
        }
    }

    public List getHomeCategories(String strLocal) {
        String strContent = "";
        byte[] bContent = null;
        if (FileUtil.fileExist(strLocal + "index.xml")) {
            bContent = FileUtil.readFile(strLocal + "index.xml");
        }
        if (bContent != null) {
            byte[] btBuffers = XMEnDecrypt.XMDecryptString(bContent);
            try {
                strContent = new String(btBuffers, 0, btBuffers.length, "utf-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        } else {
            strContent = "";
        }
        Log.v(TAG, "Content=" + strContent);
        AlbumReturn ar = new AlbumReturn();
        ar.parse(strContent, "<Name>(.*?)</Name><Type>(.*?)</Type><CoverImg>(.*?)</CoverImg><Category>(.*?)</Category>");
        return ar.mlstAlbums;
    }
}
