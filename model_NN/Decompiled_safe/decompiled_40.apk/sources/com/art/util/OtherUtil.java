package com.art.util;

import java.text.DecimalFormat;

public class OtherUtil {
    public static String FormatSpeed(long speedInBytePerSec) {
        long iSpeed = speedInBytePerSec;
        if (iSpeed < 1024) {
            return iSpeed + "B/s";
        }
        if (iSpeed < 1048576) {
            return (iSpeed / 1024) + "KB/s";
        }
        return new DecimalFormat("0.00").format(((double) iSpeed) / 1048576.0d) + "MB/s";
    }

    public static String FormatFileSize(long fileSizeInByte) {
        long iSize = fileSizeInByte;
        DecimalFormat formatter = new DecimalFormat("0.00");
        if (iSize < 1024) {
            return iSize + "B";
        }
        if (iSize < 1048576) {
            return (iSize / 1024) + "KB";
        } else if (iSize < 1073741824) {
            return formatter.format(((double) iSize) / 1048576.0d) + "MB";
        } else {
            return formatter.format(((double) iSize) / 1.073741824E9d) + "GB";
        }
    }

    public static String FormatDuration(int iDuration) {
        int iHour = iDuration / 3600;
        int iMinute = (iDuration % 3600) / 60;
        int iSecond = iDuration % 60;
        if (iHour > 0) {
            return iHour + ":" + iMinute + ":" + iSecond;
        }
        return iMinute + ":" + iSecond;
    }
}
