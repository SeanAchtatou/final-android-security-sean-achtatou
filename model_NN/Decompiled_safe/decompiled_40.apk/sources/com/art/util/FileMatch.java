package com.art.util;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FileMatch {
    public static String body = "<body>(.*?)</body>";
    public static String signiture = "<signiture>(.*?)</signiture>";
    public static String size = "<size>(.*?)</size>";
    public static String subject = "<subject>(.*?)</subject>";
    public static String time = "<time>(.*?)</time>";
    public static String title = "<title>(.*?)</title>";
    public static String type = "<type>(.*?)</type>";
    public static String url = "<url>(.*?)</url>";
    public static String ziped = "<ziped>(.*?)</ziped>";

    public static List matchResultList(String src, String patt) {
        List list = new ArrayList();
        Matcher m = Pattern.compile(patt).matcher(src);
        while (m.find()) {
            if (m.group(1) != null) {
                list.add(m.group(1).trim());
            }
        }
        return list;
    }

    public static List<String> matchRootList(String content, String rootName) {
        List<String> list = new ArrayList<>();
        Matcher m = Pattern.compile("<" + rootName + ">(.*?)</" + rootName + ">").matcher(content);
        while (m.find()) {
            list.add(m.group(1).trim().replaceAll(" ", "%20"));
        }
        return list;
    }

    public static List<String> matchRootListNoencode(String content, String rootName) {
        List<String> list = new ArrayList<>();
        Matcher m = Pattern.compile("<" + rootName + ">(.*?)</" + rootName + ">").matcher(content);
        while (m.find()) {
            list.add(m.group(1).trim());
        }
        return list;
    }

    public static String matchRootContent(String content, String rootName, int index) {
        List<String> list = new ArrayList<>();
        Matcher m = Pattern.compile("<" + rootName + ">(.*?)</" + rootName + ">").matcher(content);
        while (m.find()) {
            list.add(m.group(1).trim().replaceAll(" ", "%20"));
        }
        if (index < list.size()) {
            return list.get(index);
        }
        return null;
    }

    public static String matchRootContentNoencode(String content, String rootName, int index) {
        List<String> list = new ArrayList<>();
        Matcher m = Pattern.compile("<" + rootName + ">(.*?)</" + rootName + ">").matcher(content);
        while (m.find()) {
            list.add(m.group(1).trim());
        }
        if (index < list.size()) {
            return list.get(index);
        }
        return null;
    }
}
