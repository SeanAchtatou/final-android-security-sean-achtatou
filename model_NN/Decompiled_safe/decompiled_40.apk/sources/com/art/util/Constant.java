package com.art.util;

public class Constant {
    public static final String WPBOOKMARK = "bookmark.xml";
    public static final String WPCOLLECTION = "/favorite";
    public static final String WPPACKAGE = "/home";
    public static final String WPPARENT = "/wpcollection";
    public static final String WPTMP = "/Tmp";
}
