package com.art.util;

import android.app.Activity;
import android.net.wifi.WifiManager;

public class WifiUtil {
    private Activity activity;
    private WifiManager mWiFiManager01 = ((WifiManager) this.activity.getSystemService("wifi"));

    public WifiUtil(Activity activity2) {
        this.activity = activity2;
    }

    public boolean wifiConnect() {
        boolean bConn = true;
        if (!this.mWiFiManager01.isWifiEnabled()) {
            bConn = false;
        }
        if (!this.mWiFiManager01.isWifiEnabled() || this.mWiFiManager01.getConnectionInfo().getIpAddress() > 0) {
            return bConn;
        }
        return false;
    }
}
