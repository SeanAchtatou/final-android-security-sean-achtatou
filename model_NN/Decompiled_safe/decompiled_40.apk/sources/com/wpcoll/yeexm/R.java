package com.wpcoll.yeexm;

public final class R {

    public static final class attr {
        public static final int adSize = 2130771968;
        public static final int adUnitId = 2130771969;
    }

    public static final class drawable {
        public static final int bottom = 2130837504;
        public static final int current = 2130837505;
        public static final int download = 2130837506;
        public static final int file_package = 2130837507;
        public static final int go = 2130837508;
        public static final int go_s = 2130837509;
        public static final int home = 2130837510;
        public static final int ic_menu_refresh = 2130837511;
        public static final int ic_menu_set_wallpaper = 2130837512;
        public static final int icon = 2130837513;
        public static final int next = 2130837514;
        public static final int next1 = 2130837515;
        public static final int next_press1 = 2130837516;
        public static final int next_s = 2130837517;
        public static final int prev = 2130837518;
        public static final int prev1 = 2130837519;
        public static final int prev_press1 = 2130837520;
        public static final int prev_s = 2130837521;
        public static final int toast_warnning = 2130837522;
        public static final int web = 2130837523;
        public static final int xml_btn_goto = 2130837524;
        public static final int xml_btn_next = 2130837525;
        public static final int xml_btn_next1 = 2130837526;
        public static final int xml_btn_prev = 2130837527;
        public static final int xml_btn_prev1 = 2130837528;
    }

    public static final class id {
        public static final int BANNER = 2130968576;
        public static final int IAB_BANNER = 2130968578;
        public static final int IAB_LEADERBOARD = 2130968579;
        public static final int IAB_MRECT = 2130968577;
        public static final int ad = 2130968589;
        public static final int album = 2130968580;
        public static final int btn_goto = 2130968586;
        public static final int cat_progressbar = 2130968591;
        public static final int category_list = 2130968590;
        public static final int deletealbum = 2130968607;
        public static final int grid = 2130968601;
        public static final int grid_img = 2130968602;
        public static final int hsv = 2130968592;
        public static final int image = 2130968593;
        public static final int img_progressbar = 2130968596;
        public static final int mylib_layer = 2130968598;
        public static final int next = 2130968588;
        public static final int next1 = 2130968595;
        public static final int page_info = 2130968587;
        public static final int prev = 2130968585;
        public static final int prev1 = 2130968594;
        public static final int progress_layout = 2130968584;
        public static final int refresh = 2130968603;
        public static final int save = 2130968605;
        public static final int savegrid = 2130968604;
        public static final int set_wallpaper = 2130968606;
        public static final int top_layout = 2130968583;
        public static final int tv_txt_cat1 = 2130968581;
        public static final int tv_txt_cat2 = 2130968582;
        public static final int txt_progress = 2130968597;
        public static final int web = 2130968600;
        public static final int webparent = 2130968599;
    }

    public static final class layout {
        public static final int album_list = 2130903040;
        public static final int category = 2130903041;
        public static final int image = 2130903042;
        public static final int main = 2130903043;
        public static final int more_wps = 2130903044;
        public static final int my_collection = 2130903045;
        public static final int my_collection_item = 2130903046;
    }

    public static final class menu {
        public static final int apk_menu = 2131099648;
        public static final int grid_menu = 2131099649;
        public static final int image_menu = 2131099650;
        public static final int local_menu = 2131099651;
    }

    public static final class string {
        public static final int alert_ok_button = 2131034118;
        public static final int alert_title_failure = 2131034117;
        public static final int app_name = 2131034113;
        public static final int btn_cancel = 2131034119;
        public static final int btn_yes = 2131034148;
        public static final int hello = 2131034112;
        public static final int home_root_url = 2131034124;
        public static final int no_sdcard = 2131034121;
        public static final int page_info_head = 2131034139;
        public static final int sdcard_readonly = 2131034120;
        public static final int sdcard_shared = 2131034122;
        public static final int tab_downloadedalbum = 2131034115;
        public static final int tab_moreimg = 2131034116;
        public static final int tab_mycollection = 2131034114;
        public static final int txt_album_save = 2131034131;
        public static final int txt_album_save_exist = 2131034132;
        public static final int txt_bookmark_title = 2131034137;
        public static final int txt_delbookmark_body = 2131034138;
        public static final int txt_deletealbum = 2131034140;
        public static final int txt_deletealbum_msg = 2131034141;
        public static final int txt_download_msg = 2131034145;
        public static final int txt_download_msg_cancel = 2131034147;
        public static final int txt_download_msg_error = 2131034146;
        public static final int txt_lastimg_body = 2131034136;
        public static final int txt_lastimg_title = 2131034135;
        public static final int txt_no_wifi_connection = 2131034128;
        public static final int txt_no_wifi_data = 2131034129;
        public static final int txt_progress_loading = 2131034123;
        public static final int txt_quit_body = 2131034143;
        public static final int txt_quit_title = 2131034142;
        public static final int txt_refresh = 2131034149;
        public static final int txt_savecollection = 2131034125;
        public static final int txt_setWallpaper_body = 2131034134;
        public static final int txt_setWallpaper_title = 2131034133;
        public static final int txt_setwallpaper = 2131034126;
        public static final int txt_viewimgtips = 2131034130;
        public static final int txt_wifi_none_title = 2131034127;
        public static final int webimgrooturlHtml = 2131034144;
    }

    public static final class styleable {
        public static final int[] com_google_ads_AdView = {com.art.yh.R.attr.adSize, com.art.yh.R.attr.adUnitId};
        public static final int com_google_ads_AdView_adSize = 0;
        public static final int com_google_ads_AdView_adUnitId = 1;
    }
}
