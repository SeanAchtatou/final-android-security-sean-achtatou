package defpackage;

import com.qq.taf.jce.JceInputStream;
import com.qq.taf.jce.JceOutputStream;
import com.qq.taf.jce.JceStruct;
import com.qq.taf.jce.JceUtil;
import java.util.ArrayList;
import java.util.Collection;

/* renamed from: e  reason: default package */
public final class e extends JceStruct implements Cloneable {
    static a e;
    static s f;
    static t g;
    static ArrayList<c> h;
    static final /* synthetic */ boolean i = (!e.class.desiredAssertionStatus());

    /* renamed from: a  reason: collision with root package name */
    public a f4146a = null;
    public s b = null;
    public t c = null;
    public ArrayList<c> d = null;

    public e() {
        a(this.f4146a);
        a(this.b);
        a(this.c);
        a(this.d);
    }

    public s a() {
        return this.b;
    }

    public void a(a aVar) {
        this.f4146a = aVar;
    }

    public void a(ArrayList<c> arrayList) {
        this.d = arrayList;
    }

    public void a(s sVar) {
        this.b = sVar;
    }

    public void a(t tVar) {
        this.c = tVar;
    }

    public t b() {
        return this.c;
    }

    public ArrayList<c> c() {
        return this.d;
    }

    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e2) {
            if (i) {
                return null;
            }
            throw new AssertionError();
        }
    }

    public void display(StringBuilder sb, int i2) {
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        e eVar = (e) obj;
        return JceUtil.equals(this.f4146a, eVar.f4146a) && JceUtil.equals(this.b, eVar.b) && JceUtil.equals(this.c, eVar.c) && JceUtil.equals(this.d, eVar.d);
    }

    public int hashCode() {
        try {
            throw new Exception("Need define key first!");
        } catch (Exception e2) {
            e2.printStackTrace();
            return 0;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct, int, boolean):com.qq.taf.jce.JceStruct
     arg types: [a, int, int]
     candidates:
      com.qq.taf.jce.JceInputStream.read(byte, int, boolean):byte
      com.qq.taf.jce.JceInputStream.read(double, int, boolean):double
      com.qq.taf.jce.JceInputStream.read(float, int, boolean):float
      com.qq.taf.jce.JceInputStream.read(int, int, boolean):int
      com.qq.taf.jce.JceInputStream.read(long, int, boolean):long
      com.qq.taf.jce.JceInputStream.read(java.lang.Object, int, boolean):java.lang.Object
      com.qq.taf.jce.JceInputStream.read(java.lang.String, int, boolean):java.lang.String
      com.qq.taf.jce.JceInputStream.read(short, int, boolean):short
      com.qq.taf.jce.JceInputStream.read(boolean, int, boolean):boolean
      com.qq.taf.jce.JceInputStream.read(byte[], int, boolean):byte[]
      com.qq.taf.jce.JceInputStream.read(double[], int, boolean):double[]
      com.qq.taf.jce.JceInputStream.read(float[], int, boolean):float[]
      com.qq.taf.jce.JceInputStream.read(int[], int, boolean):int[]
      com.qq.taf.jce.JceInputStream.read(long[], int, boolean):long[]
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct[], int, boolean):com.qq.taf.jce.JceStruct[]
      com.qq.taf.jce.JceInputStream.read(java.lang.String[], int, boolean):java.lang.String[]
      com.qq.taf.jce.JceInputStream.read(short[], int, boolean):short[]
      com.qq.taf.jce.JceInputStream.read(boolean[], int, boolean):boolean[]
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct, int, boolean):com.qq.taf.jce.JceStruct */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct, int, boolean):com.qq.taf.jce.JceStruct
     arg types: [s, int, int]
     candidates:
      com.qq.taf.jce.JceInputStream.read(byte, int, boolean):byte
      com.qq.taf.jce.JceInputStream.read(double, int, boolean):double
      com.qq.taf.jce.JceInputStream.read(float, int, boolean):float
      com.qq.taf.jce.JceInputStream.read(int, int, boolean):int
      com.qq.taf.jce.JceInputStream.read(long, int, boolean):long
      com.qq.taf.jce.JceInputStream.read(java.lang.Object, int, boolean):java.lang.Object
      com.qq.taf.jce.JceInputStream.read(java.lang.String, int, boolean):java.lang.String
      com.qq.taf.jce.JceInputStream.read(short, int, boolean):short
      com.qq.taf.jce.JceInputStream.read(boolean, int, boolean):boolean
      com.qq.taf.jce.JceInputStream.read(byte[], int, boolean):byte[]
      com.qq.taf.jce.JceInputStream.read(double[], int, boolean):double[]
      com.qq.taf.jce.JceInputStream.read(float[], int, boolean):float[]
      com.qq.taf.jce.JceInputStream.read(int[], int, boolean):int[]
      com.qq.taf.jce.JceInputStream.read(long[], int, boolean):long[]
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct[], int, boolean):com.qq.taf.jce.JceStruct[]
      com.qq.taf.jce.JceInputStream.read(java.lang.String[], int, boolean):java.lang.String[]
      com.qq.taf.jce.JceInputStream.read(short[], int, boolean):short[]
      com.qq.taf.jce.JceInputStream.read(boolean[], int, boolean):boolean[]
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct, int, boolean):com.qq.taf.jce.JceStruct */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct, int, boolean):com.qq.taf.jce.JceStruct
     arg types: [t, int, int]
     candidates:
      com.qq.taf.jce.JceInputStream.read(byte, int, boolean):byte
      com.qq.taf.jce.JceInputStream.read(double, int, boolean):double
      com.qq.taf.jce.JceInputStream.read(float, int, boolean):float
      com.qq.taf.jce.JceInputStream.read(int, int, boolean):int
      com.qq.taf.jce.JceInputStream.read(long, int, boolean):long
      com.qq.taf.jce.JceInputStream.read(java.lang.Object, int, boolean):java.lang.Object
      com.qq.taf.jce.JceInputStream.read(java.lang.String, int, boolean):java.lang.String
      com.qq.taf.jce.JceInputStream.read(short, int, boolean):short
      com.qq.taf.jce.JceInputStream.read(boolean, int, boolean):boolean
      com.qq.taf.jce.JceInputStream.read(byte[], int, boolean):byte[]
      com.qq.taf.jce.JceInputStream.read(double[], int, boolean):double[]
      com.qq.taf.jce.JceInputStream.read(float[], int, boolean):float[]
      com.qq.taf.jce.JceInputStream.read(int[], int, boolean):int[]
      com.qq.taf.jce.JceInputStream.read(long[], int, boolean):long[]
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct[], int, boolean):com.qq.taf.jce.JceStruct[]
      com.qq.taf.jce.JceInputStream.read(java.lang.String[], int, boolean):java.lang.String[]
      com.qq.taf.jce.JceInputStream.read(short[], int, boolean):short[]
      com.qq.taf.jce.JceInputStream.read(boolean[], int, boolean):boolean[]
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct, int, boolean):com.qq.taf.jce.JceStruct */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceInputStream.read(java.lang.Object, int, boolean):java.lang.Object
     arg types: [java.util.ArrayList<c>, int, int]
     candidates:
      com.qq.taf.jce.JceInputStream.read(byte, int, boolean):byte
      com.qq.taf.jce.JceInputStream.read(double, int, boolean):double
      com.qq.taf.jce.JceInputStream.read(float, int, boolean):float
      com.qq.taf.jce.JceInputStream.read(int, int, boolean):int
      com.qq.taf.jce.JceInputStream.read(long, int, boolean):long
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct, int, boolean):com.qq.taf.jce.JceStruct
      com.qq.taf.jce.JceInputStream.read(java.lang.String, int, boolean):java.lang.String
      com.qq.taf.jce.JceInputStream.read(short, int, boolean):short
      com.qq.taf.jce.JceInputStream.read(boolean, int, boolean):boolean
      com.qq.taf.jce.JceInputStream.read(byte[], int, boolean):byte[]
      com.qq.taf.jce.JceInputStream.read(double[], int, boolean):double[]
      com.qq.taf.jce.JceInputStream.read(float[], int, boolean):float[]
      com.qq.taf.jce.JceInputStream.read(int[], int, boolean):int[]
      com.qq.taf.jce.JceInputStream.read(long[], int, boolean):long[]
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct[], int, boolean):com.qq.taf.jce.JceStruct[]
      com.qq.taf.jce.JceInputStream.read(java.lang.String[], int, boolean):java.lang.String[]
      com.qq.taf.jce.JceInputStream.read(short[], int, boolean):short[]
      com.qq.taf.jce.JceInputStream.read(boolean[], int, boolean):boolean[]
      com.qq.taf.jce.JceInputStream.read(java.lang.Object, int, boolean):java.lang.Object */
    public void readFrom(JceInputStream jceInputStream) {
        if (e == null) {
            e = new a();
        }
        a((a) jceInputStream.read((JceStruct) e, 0, true));
        if (f == null) {
            f = new s();
        }
        a((s) jceInputStream.read((JceStruct) f, 1, true));
        if (g == null) {
            g = new t();
        }
        a((t) jceInputStream.read((JceStruct) g, 2, false));
        if (h == null) {
            h = new ArrayList<>();
            h.add(new c());
        }
        a((ArrayList) jceInputStream.read((Object) h, 3, false));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceOutputStream.write(com.qq.taf.jce.JceStruct, int):void
     arg types: [a, int]
     candidates:
      com.qq.taf.jce.JceOutputStream.write(byte, int):void
      com.qq.taf.jce.JceOutputStream.write(double, int):void
      com.qq.taf.jce.JceOutputStream.write(float, int):void
      com.qq.taf.jce.JceOutputStream.write(int, int):void
      com.qq.taf.jce.JceOutputStream.write(long, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Boolean, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Byte, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Double, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Float, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Integer, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Long, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Object, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Short, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.String, int):void
      com.qq.taf.jce.JceOutputStream.write(java.util.Collection, int):void
      com.qq.taf.jce.JceOutputStream.write(java.util.Map, int):void
      com.qq.taf.jce.JceOutputStream.write(short, int):void
      com.qq.taf.jce.JceOutputStream.write(boolean, int):void
      com.qq.taf.jce.JceOutputStream.write(byte[], int):void
      com.qq.taf.jce.JceOutputStream.write(double[], int):void
      com.qq.taf.jce.JceOutputStream.write(float[], int):void
      com.qq.taf.jce.JceOutputStream.write(int[], int):void
      com.qq.taf.jce.JceOutputStream.write(long[], int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Object[], int):void
      com.qq.taf.jce.JceOutputStream.write(short[], int):void
      com.qq.taf.jce.JceOutputStream.write(boolean[], int):void
      com.qq.taf.jce.JceOutputStream.write(com.qq.taf.jce.JceStruct, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceOutputStream.write(com.qq.taf.jce.JceStruct, int):void
     arg types: [s, int]
     candidates:
      com.qq.taf.jce.JceOutputStream.write(byte, int):void
      com.qq.taf.jce.JceOutputStream.write(double, int):void
      com.qq.taf.jce.JceOutputStream.write(float, int):void
      com.qq.taf.jce.JceOutputStream.write(int, int):void
      com.qq.taf.jce.JceOutputStream.write(long, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Boolean, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Byte, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Double, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Float, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Integer, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Long, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Object, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Short, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.String, int):void
      com.qq.taf.jce.JceOutputStream.write(java.util.Collection, int):void
      com.qq.taf.jce.JceOutputStream.write(java.util.Map, int):void
      com.qq.taf.jce.JceOutputStream.write(short, int):void
      com.qq.taf.jce.JceOutputStream.write(boolean, int):void
      com.qq.taf.jce.JceOutputStream.write(byte[], int):void
      com.qq.taf.jce.JceOutputStream.write(double[], int):void
      com.qq.taf.jce.JceOutputStream.write(float[], int):void
      com.qq.taf.jce.JceOutputStream.write(int[], int):void
      com.qq.taf.jce.JceOutputStream.write(long[], int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Object[], int):void
      com.qq.taf.jce.JceOutputStream.write(short[], int):void
      com.qq.taf.jce.JceOutputStream.write(boolean[], int):void
      com.qq.taf.jce.JceOutputStream.write(com.qq.taf.jce.JceStruct, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceOutputStream.write(com.qq.taf.jce.JceStruct, int):void
     arg types: [t, int]
     candidates:
      com.qq.taf.jce.JceOutputStream.write(byte, int):void
      com.qq.taf.jce.JceOutputStream.write(double, int):void
      com.qq.taf.jce.JceOutputStream.write(float, int):void
      com.qq.taf.jce.JceOutputStream.write(int, int):void
      com.qq.taf.jce.JceOutputStream.write(long, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Boolean, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Byte, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Double, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Float, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Integer, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Long, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Object, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Short, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.String, int):void
      com.qq.taf.jce.JceOutputStream.write(java.util.Collection, int):void
      com.qq.taf.jce.JceOutputStream.write(java.util.Map, int):void
      com.qq.taf.jce.JceOutputStream.write(short, int):void
      com.qq.taf.jce.JceOutputStream.write(boolean, int):void
      com.qq.taf.jce.JceOutputStream.write(byte[], int):void
      com.qq.taf.jce.JceOutputStream.write(double[], int):void
      com.qq.taf.jce.JceOutputStream.write(float[], int):void
      com.qq.taf.jce.JceOutputStream.write(int[], int):void
      com.qq.taf.jce.JceOutputStream.write(long[], int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Object[], int):void
      com.qq.taf.jce.JceOutputStream.write(short[], int):void
      com.qq.taf.jce.JceOutputStream.write(boolean[], int):void
      com.qq.taf.jce.JceOutputStream.write(com.qq.taf.jce.JceStruct, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceOutputStream.write(java.util.Collection, int):void
     arg types: [java.util.ArrayList<c>, int]
     candidates:
      com.qq.taf.jce.JceOutputStream.write(byte, int):void
      com.qq.taf.jce.JceOutputStream.write(double, int):void
      com.qq.taf.jce.JceOutputStream.write(float, int):void
      com.qq.taf.jce.JceOutputStream.write(int, int):void
      com.qq.taf.jce.JceOutputStream.write(long, int):void
      com.qq.taf.jce.JceOutputStream.write(com.qq.taf.jce.JceStruct, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Boolean, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Byte, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Double, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Float, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Integer, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Long, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Object, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Short, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.String, int):void
      com.qq.taf.jce.JceOutputStream.write(java.util.Map, int):void
      com.qq.taf.jce.JceOutputStream.write(short, int):void
      com.qq.taf.jce.JceOutputStream.write(boolean, int):void
      com.qq.taf.jce.JceOutputStream.write(byte[], int):void
      com.qq.taf.jce.JceOutputStream.write(double[], int):void
      com.qq.taf.jce.JceOutputStream.write(float[], int):void
      com.qq.taf.jce.JceOutputStream.write(int[], int):void
      com.qq.taf.jce.JceOutputStream.write(long[], int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Object[], int):void
      com.qq.taf.jce.JceOutputStream.write(short[], int):void
      com.qq.taf.jce.JceOutputStream.write(boolean[], int):void
      com.qq.taf.jce.JceOutputStream.write(java.util.Collection, int):void */
    public void writeTo(JceOutputStream jceOutputStream) {
        jceOutputStream.write((JceStruct) this.f4146a, 0);
        jceOutputStream.write((JceStruct) this.b, 1);
        if (this.c != null) {
            jceOutputStream.write((JceStruct) this.c, 2);
        }
        if (this.d != null) {
            jceOutputStream.write((Collection) this.d, 3);
        }
    }
}
