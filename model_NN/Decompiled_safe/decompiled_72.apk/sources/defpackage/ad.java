package defpackage;

import android.content.Context;
import android.net.Proxy;
import android.os.Environment;
import defpackage.ae;
import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import org.apache.http.client.methods.HttpGet;

/* renamed from: ad  reason: default package */
public class ad extends ac {
    private Context c;
    private HttpGet d = null;
    private String e = null;
    private String f = null;
    private String g = null;
    private String h = null;
    private int i = 0;
    private boolean j = false;
    private boolean k = false;
    private long l = 0;

    public ad(Context context) {
        this.c = context;
        this.j = "mounted".equals(Environment.getExternalStorageState());
        if (this.j) {
            this.e = "/sdcard/qqsecure";
            this.f = "/sdcard/qqsecure";
        } else {
            this.e = context.getCacheDir().getAbsolutePath();
            this.f = context.getFilesDir().getAbsolutePath();
        }
        this.d = new HttpGet();
        if (ae.c(context) == ae.a.CONN_CMWAP) {
            a(Proxy.getDefaultHost(), Proxy.getDefaultPort());
            a(true);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
      ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException} */
    /* JADX WARNING: Removed duplicated region for block: B:101:0x015a A[SYNTHETIC, Splitter:B:101:0x015a] */
    /* JADX WARNING: Removed duplicated region for block: B:105:0x0160 A[SYNTHETIC, Splitter:B:105:0x0160] */
    /* JADX WARNING: Removed duplicated region for block: B:116:0x017c A[SYNTHETIC, Splitter:B:116:0x017c] */
    /* JADX WARNING: Removed duplicated region for block: B:120:0x0182 A[SYNTHETIC, Splitter:B:120:0x0182] */
    /* JADX WARNING: Removed duplicated region for block: B:128:0x0199 A[SYNTHETIC, Splitter:B:128:0x0199] */
    /* JADX WARNING: Removed duplicated region for block: B:131:0x019e A[SYNTHETIC, Splitter:B:131:0x019e] */
    /* JADX WARNING: Removed duplicated region for block: B:162:0x01e8  */
    /* JADX WARNING: Removed duplicated region for block: B:163:0x01ea  */
    /* JADX WARNING: Removed duplicated region for block: B:164:0x01ed  */
    /* JADX WARNING: Removed duplicated region for block: B:165:0x01f0  */
    /* JADX WARNING: Removed duplicated region for block: B:166:0x01f3  */
    /* JADX WARNING: Removed duplicated region for block: B:173:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:175:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:177:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:179:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:181:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:183:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x00d9 A[SYNTHETIC, Splitter:B:43:0x00d9] */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x00f6 A[SYNTHETIC, Splitter:B:56:0x00f6] */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x00fc A[SYNTHETIC, Splitter:B:60:0x00fc] */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x0116 A[SYNTHETIC, Splitter:B:71:0x0116] */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x011c A[SYNTHETIC, Splitter:B:75:0x011c] */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x0138 A[SYNTHETIC, Splitter:B:86:0x0138] */
    /* JADX WARNING: Removed duplicated region for block: B:90:0x013e A[SYNTHETIC, Splitter:B:90:0x013e] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private int a(org.apache.http.HttpEntity r16, android.os.Bundle r17, boolean r18) {
        /*
            r15 = this;
            r3 = 0
            r2 = 0
            r1 = 8192(0x2000, float:1.14794E-41)
            byte[] r6 = new byte[r1]
            long r4 = r16.getContentLength()     // Catch:{ FileNotFoundException -> 0x00ee, SocketException -> 0x010e, SocketTimeoutException -> 0x0130, IOException -> 0x0152, Exception -> 0x0174 }
            long r7 = r15.l     // Catch:{ FileNotFoundException -> 0x00ee, SocketException -> 0x010e, SocketTimeoutException -> 0x0130, IOException -> 0x0152, Exception -> 0x0174 }
            long r7 = r7 + r4
            long r4 = r15.l     // Catch:{ FileNotFoundException -> 0x00ee, SocketException -> 0x010e, SocketTimeoutException -> 0x0130, IOException -> 0x0152, Exception -> 0x0174 }
            r9 = 100
            long r4 = r4 * r9
            long r4 = r4 / r7
            int r5 = (int) r4     // Catch:{ FileNotFoundException -> 0x00ee, SocketException -> 0x010e, SocketTimeoutException -> 0x0130, IOException -> 0x0152, Exception -> 0x0174 }
            java.lang.String r1 = "key_total"
            r0 = r17
            r0.putLong(r1, r7)     // Catch:{ FileNotFoundException -> 0x00ee, SocketException -> 0x010e, SocketTimeoutException -> 0x0130, IOException -> 0x0152, Exception -> 0x0174 }
            java.lang.String r1 = "key_completed"
            long r9 = r15.l     // Catch:{ FileNotFoundException -> 0x00ee, SocketException -> 0x010e, SocketTimeoutException -> 0x0130, IOException -> 0x0152, Exception -> 0x0174 }
            r0 = r17
            r0.putLong(r1, r9)     // Catch:{ FileNotFoundException -> 0x00ee, SocketException -> 0x010e, SocketTimeoutException -> 0x0130, IOException -> 0x0152, Exception -> 0x0174 }
            java.lang.String r1 = "key_progress"
            r0 = r17
            r0.putInt(r1, r5)     // Catch:{ FileNotFoundException -> 0x00ee, SocketException -> 0x010e, SocketTimeoutException -> 0x0130, IOException -> 0x0152, Exception -> 0x0174 }
            java.io.File r1 = new java.io.File     // Catch:{ FileNotFoundException -> 0x00ee, SocketException -> 0x010e, SocketTimeoutException -> 0x0130, IOException -> 0x0152, Exception -> 0x0174 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x00ee, SocketException -> 0x010e, SocketTimeoutException -> 0x0130, IOException -> 0x0152, Exception -> 0x0174 }
            r4.<init>()     // Catch:{ FileNotFoundException -> 0x00ee, SocketException -> 0x010e, SocketTimeoutException -> 0x0130, IOException -> 0x0152, Exception -> 0x0174 }
            java.lang.String r9 = r15.e     // Catch:{ FileNotFoundException -> 0x00ee, SocketException -> 0x010e, SocketTimeoutException -> 0x0130, IOException -> 0x0152, Exception -> 0x0174 }
            java.lang.StringBuilder r4 = r4.append(r9)     // Catch:{ FileNotFoundException -> 0x00ee, SocketException -> 0x010e, SocketTimeoutException -> 0x0130, IOException -> 0x0152, Exception -> 0x0174 }
            java.lang.String r9 = java.io.File.separator     // Catch:{ FileNotFoundException -> 0x00ee, SocketException -> 0x010e, SocketTimeoutException -> 0x0130, IOException -> 0x0152, Exception -> 0x0174 }
            java.lang.StringBuilder r4 = r4.append(r9)     // Catch:{ FileNotFoundException -> 0x00ee, SocketException -> 0x010e, SocketTimeoutException -> 0x0130, IOException -> 0x0152, Exception -> 0x0174 }
            java.lang.String r9 = r15.g     // Catch:{ FileNotFoundException -> 0x00ee, SocketException -> 0x010e, SocketTimeoutException -> 0x0130, IOException -> 0x0152, Exception -> 0x0174 }
            java.lang.StringBuilder r4 = r4.append(r9)     // Catch:{ FileNotFoundException -> 0x00ee, SocketException -> 0x010e, SocketTimeoutException -> 0x0130, IOException -> 0x0152, Exception -> 0x0174 }
            java.lang.String r4 = r4.toString()     // Catch:{ FileNotFoundException -> 0x00ee, SocketException -> 0x010e, SocketTimeoutException -> 0x0130, IOException -> 0x0152, Exception -> 0x0174 }
            r1.<init>(r4)     // Catch:{ FileNotFoundException -> 0x00ee, SocketException -> 0x010e, SocketTimeoutException -> 0x0130, IOException -> 0x0152, Exception -> 0x0174 }
            boolean r4 = r1.exists()     // Catch:{ FileNotFoundException -> 0x00ee, SocketException -> 0x010e, SocketTimeoutException -> 0x0130, IOException -> 0x0152, Exception -> 0x0174 }
            if (r4 != 0) goto L_0x005b
            java.io.File r4 = r1.getParentFile()     // Catch:{ FileNotFoundException -> 0x00ee, SocketException -> 0x010e, SocketTimeoutException -> 0x0130, IOException -> 0x0152, Exception -> 0x0174 }
            r4.mkdirs()     // Catch:{ FileNotFoundException -> 0x00ee, SocketException -> 0x010e, SocketTimeoutException -> 0x0130, IOException -> 0x0152, Exception -> 0x0174 }
            r1.createNewFile()     // Catch:{ FileNotFoundException -> 0x00ee, SocketException -> 0x010e, SocketTimeoutException -> 0x0130, IOException -> 0x0152, Exception -> 0x0174 }
        L_0x005b:
            java.io.FileOutputStream r4 = new java.io.FileOutputStream     // Catch:{ FileNotFoundException -> 0x00ee, SocketException -> 0x010e, SocketTimeoutException -> 0x0130, IOException -> 0x0152, Exception -> 0x0174 }
            r9 = 1
            r4.<init>(r1, r9)     // Catch:{ FileNotFoundException -> 0x00ee, SocketException -> 0x010e, SocketTimeoutException -> 0x0130, IOException -> 0x0152, Exception -> 0x0174 }
            if (r18 == 0) goto L_0x0087
            java.util.zip.InflaterInputStream r1 = new java.util.zip.InflaterInputStream     // Catch:{ FileNotFoundException -> 0x01dd, SocketException -> 0x01d2, SocketTimeoutException -> 0x01c7, IOException -> 0x01be, Exception -> 0x01b5, all -> 0x01ac }
            java.io.InputStream r3 = r16.getContent()     // Catch:{ FileNotFoundException -> 0x01dd, SocketException -> 0x01d2, SocketTimeoutException -> 0x01c7, IOException -> 0x01be, Exception -> 0x01b5, all -> 0x01ac }
            r1.<init>(r3)     // Catch:{ FileNotFoundException -> 0x01dd, SocketException -> 0x01d2, SocketTimeoutException -> 0x01c7, IOException -> 0x01be, Exception -> 0x01b5, all -> 0x01ac }
        L_0x006c:
            r2 = 0
            r3 = r5
        L_0x006e:
            int r9 = r1.read(r6)     // Catch:{ FileNotFoundException -> 0x01e1, SocketException -> 0x01d6, SocketTimeoutException -> 0x01cb, IOException -> 0x01c1, Exception -> 0x01b8, all -> 0x01af }
            r5 = -1
            if (r9 == r5) goto L_0x00c4
            boolean r5 = r15.k     // Catch:{ FileNotFoundException -> 0x01e1, SocketException -> 0x01d6, SocketTimeoutException -> 0x01cb, IOException -> 0x01c1, Exception -> 0x01b8, all -> 0x01af }
            if (r5 == 0) goto L_0x0096
            r2 = -5003(0xffffffffffffec75, float:NaN)
            if (r1 == 0) goto L_0x0080
            r1.close()     // Catch:{ IOException -> 0x008c }
        L_0x0080:
            if (r4 == 0) goto L_0x0085
            r4.close()     // Catch:{ IOException -> 0x0091 }
        L_0x0085:
            r1 = r2
        L_0x0086:
            return r1
        L_0x0087:
            java.io.InputStream r1 = r16.getContent()     // Catch:{ FileNotFoundException -> 0x01dd, SocketException -> 0x01d2, SocketTimeoutException -> 0x01c7, IOException -> 0x01be, Exception -> 0x01b5, all -> 0x01ac }
            goto L_0x006c
        L_0x008c:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0080
        L_0x0091:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0085
        L_0x0096:
            long r10 = r15.l     // Catch:{ FileNotFoundException -> 0x01e1, SocketException -> 0x01d6, SocketTimeoutException -> 0x01cb, IOException -> 0x01c1, Exception -> 0x01b8, all -> 0x01af }
            long r12 = (long) r9     // Catch:{ FileNotFoundException -> 0x01e1, SocketException -> 0x01d6, SocketTimeoutException -> 0x01cb, IOException -> 0x01c1, Exception -> 0x01b8, all -> 0x01af }
            long r10 = r10 + r12
            r15.l = r10     // Catch:{ FileNotFoundException -> 0x01e1, SocketException -> 0x01d6, SocketTimeoutException -> 0x01cb, IOException -> 0x01c1, Exception -> 0x01b8, all -> 0x01af }
            int r5 = r2 + r9
            java.lang.String r2 = "key_completed"
            long r10 = r15.l     // Catch:{ FileNotFoundException -> 0x01e1, SocketException -> 0x01d6, SocketTimeoutException -> 0x01cb, IOException -> 0x01c1, Exception -> 0x01b8, all -> 0x01af }
            r0 = r17
            r0.putLong(r2, r10)     // Catch:{ FileNotFoundException -> 0x01e1, SocketException -> 0x01d6, SocketTimeoutException -> 0x01cb, IOException -> 0x01c1, Exception -> 0x01b8, all -> 0x01af }
            long r10 = r15.l     // Catch:{ FileNotFoundException -> 0x01e1, SocketException -> 0x01d6, SocketTimeoutException -> 0x01cb, IOException -> 0x01c1, Exception -> 0x01b8, all -> 0x01af }
            r12 = 100
            long r10 = r10 * r12
            long r10 = r10 / r7
            int r2 = (int) r10     // Catch:{ FileNotFoundException -> 0x01e1, SocketException -> 0x01d6, SocketTimeoutException -> 0x01cb, IOException -> 0x01c1, Exception -> 0x01b8, all -> 0x01af }
            if (r2 == r3) goto L_0x01f6
            java.lang.String r3 = "key_progress"
            r0 = r17
            r0.putInt(r3, r2)     // Catch:{ FileNotFoundException -> 0x01e1, SocketException -> 0x01d6, SocketTimeoutException -> 0x01cb, IOException -> 0x01c1, Exception -> 0x01b8, all -> 0x01af }
            r3 = 2
            r0 = r17
            r15.a(r3, r0)     // Catch:{ FileNotFoundException -> 0x01e1, SocketException -> 0x01d6, SocketTimeoutException -> 0x01cb, IOException -> 0x01c1, Exception -> 0x01b8, all -> 0x01af }
        L_0x00bd:
            r3 = 0
            r4.write(r6, r3, r9)     // Catch:{ FileNotFoundException -> 0x01e1, SocketException -> 0x01d6, SocketTimeoutException -> 0x01cb, IOException -> 0x01c1, Exception -> 0x01b8, all -> 0x01af }
            r3 = r2
            r2 = r5
            goto L_0x006e
        L_0x00c4:
            r4.flush()     // Catch:{ FileNotFoundException -> 0x01e1, SocketException -> 0x01d6, SocketTimeoutException -> 0x01cb, IOException -> 0x01c1, Exception -> 0x01b8, all -> 0x01af }
            long r2 = (long) r2     // Catch:{ FileNotFoundException -> 0x01e1, SocketException -> 0x01d6, SocketTimeoutException -> 0x01cb, IOException -> 0x01c1, Exception -> 0x01b8, all -> 0x01af }
            long r5 = r16.getContentLength()     // Catch:{ FileNotFoundException -> 0x01e1, SocketException -> 0x01d6, SocketTimeoutException -> 0x01cb, IOException -> 0x01c1, Exception -> 0x01b8, all -> 0x01af }
            int r2 = (r2 > r5 ? 1 : (r2 == r5 ? 0 : -1))
            if (r2 != 0) goto L_0x00e4
            r2 = 0
        L_0x00d1:
            if (r1 == 0) goto L_0x00ec
            r1.close()     // Catch:{ IOException -> 0x00e6 }
            r1 = r2
        L_0x00d7:
            if (r4 == 0) goto L_0x0086
            r4.close()     // Catch:{ IOException -> 0x00dd }
            goto L_0x0086
        L_0x00dd:
            r2 = move-exception
            r1 = -7000(0xffffffffffffe4a8, float:NaN)
            r2.printStackTrace()
            goto L_0x0086
        L_0x00e4:
            r2 = -7
            goto L_0x00d1
        L_0x00e6:
            r1 = move-exception
            r2 = -7000(0xffffffffffffe4a8, float:NaN)
            r1.printStackTrace()
        L_0x00ec:
            r1 = r2
            goto L_0x00d7
        L_0x00ee:
            r1 = move-exception
        L_0x00ef:
            r4 = -7001(0xffffffffffffe4a7, float:NaN)
            r1.printStackTrace()     // Catch:{ all -> 0x0196 }
            if (r2 == 0) goto L_0x01f3
            r2.close()     // Catch:{ IOException -> 0x0107 }
            r1 = r4
        L_0x00fa:
            if (r3 == 0) goto L_0x0086
            r3.close()     // Catch:{ IOException -> 0x0100 }
            goto L_0x0086
        L_0x0100:
            r2 = move-exception
            r1 = -7000(0xffffffffffffe4a8, float:NaN)
            r2.printStackTrace()
            goto L_0x0086
        L_0x0107:
            r2 = move-exception
            r1 = -7000(0xffffffffffffe4a8, float:NaN)
            r2.printStackTrace()
            goto L_0x00fa
        L_0x010e:
            r1 = move-exception
        L_0x010f:
            r4 = -5054(0xffffffffffffec42, float:NaN)
            r1.printStackTrace()     // Catch:{ all -> 0x0196 }
            if (r2 == 0) goto L_0x01f0
            r2.close()     // Catch:{ IOException -> 0x0129 }
            r1 = r4
        L_0x011a:
            if (r3 == 0) goto L_0x0086
            r3.close()     // Catch:{ IOException -> 0x0121 }
            goto L_0x0086
        L_0x0121:
            r2 = move-exception
            r1 = -7000(0xffffffffffffe4a8, float:NaN)
            r2.printStackTrace()
            goto L_0x0086
        L_0x0129:
            r2 = move-exception
            r1 = -7000(0xffffffffffffe4a8, float:NaN)
            r2.printStackTrace()
            goto L_0x011a
        L_0x0130:
            r1 = move-exception
        L_0x0131:
            r4 = -5055(0xffffffffffffec41, float:NaN)
            r1.printStackTrace()     // Catch:{ all -> 0x0196 }
            if (r2 == 0) goto L_0x01ed
            r2.close()     // Catch:{ IOException -> 0x014b }
            r1 = r4
        L_0x013c:
            if (r3 == 0) goto L_0x0086
            r3.close()     // Catch:{ IOException -> 0x0143 }
            goto L_0x0086
        L_0x0143:
            r2 = move-exception
            r1 = -7000(0xffffffffffffe4a8, float:NaN)
            r2.printStackTrace()
            goto L_0x0086
        L_0x014b:
            r2 = move-exception
            r1 = -7000(0xffffffffffffe4a8, float:NaN)
            r2.printStackTrace()
            goto L_0x013c
        L_0x0152:
            r1 = move-exception
        L_0x0153:
            r4 = -5056(0xffffffffffffec40, float:NaN)
            r1.printStackTrace()     // Catch:{ all -> 0x0196 }
            if (r2 == 0) goto L_0x01ea
            r2.close()     // Catch:{ IOException -> 0x016d }
            r1 = r4
        L_0x015e:
            if (r3 == 0) goto L_0x0086
            r3.close()     // Catch:{ IOException -> 0x0165 }
            goto L_0x0086
        L_0x0165:
            r2 = move-exception
            r1 = -7000(0xffffffffffffe4a8, float:NaN)
            r2.printStackTrace()
            goto L_0x0086
        L_0x016d:
            r2 = move-exception
            r1 = -7000(0xffffffffffffe4a8, float:NaN)
            r2.printStackTrace()
            goto L_0x015e
        L_0x0174:
            r1 = move-exception
        L_0x0175:
            r4 = -5000(0xffffffffffffec78, float:NaN)
            r1.printStackTrace()     // Catch:{ all -> 0x0196 }
            if (r2 == 0) goto L_0x01e8
            r2.close()     // Catch:{ IOException -> 0x018f }
            r1 = r4
        L_0x0180:
            if (r3 == 0) goto L_0x0086
            r3.close()     // Catch:{ IOException -> 0x0187 }
            goto L_0x0086
        L_0x0187:
            r2 = move-exception
            r1 = -7000(0xffffffffffffe4a8, float:NaN)
            r2.printStackTrace()
            goto L_0x0086
        L_0x018f:
            r2 = move-exception
            r1 = -7000(0xffffffffffffe4a8, float:NaN)
            r2.printStackTrace()
            goto L_0x0180
        L_0x0196:
            r1 = move-exception
        L_0x0197:
            if (r2 == 0) goto L_0x019c
            r2.close()     // Catch:{ IOException -> 0x01a2 }
        L_0x019c:
            if (r3 == 0) goto L_0x01a1
            r3.close()     // Catch:{ IOException -> 0x01a7 }
        L_0x01a1:
            throw r1
        L_0x01a2:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x019c
        L_0x01a7:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x01a1
        L_0x01ac:
            r1 = move-exception
            r3 = r4
            goto L_0x0197
        L_0x01af:
            r2 = move-exception
            r3 = r4
            r14 = r1
            r1 = r2
            r2 = r14
            goto L_0x0197
        L_0x01b5:
            r1 = move-exception
            r3 = r4
            goto L_0x0175
        L_0x01b8:
            r2 = move-exception
            r3 = r4
            r14 = r1
            r1 = r2
            r2 = r14
            goto L_0x0175
        L_0x01be:
            r1 = move-exception
            r3 = r4
            goto L_0x0153
        L_0x01c1:
            r2 = move-exception
            r3 = r4
            r14 = r1
            r1 = r2
            r2 = r14
            goto L_0x0153
        L_0x01c7:
            r1 = move-exception
            r3 = r4
            goto L_0x0131
        L_0x01cb:
            r2 = move-exception
            r3 = r4
            r14 = r1
            r1 = r2
            r2 = r14
            goto L_0x0131
        L_0x01d2:
            r1 = move-exception
            r3 = r4
            goto L_0x010f
        L_0x01d6:
            r2 = move-exception
            r3 = r4
            r14 = r1
            r1 = r2
            r2 = r14
            goto L_0x010f
        L_0x01dd:
            r1 = move-exception
            r3 = r4
            goto L_0x00ef
        L_0x01e1:
            r2 = move-exception
            r3 = r4
            r14 = r1
            r1 = r2
            r2 = r14
            goto L_0x00ef
        L_0x01e8:
            r1 = r4
            goto L_0x0180
        L_0x01ea:
            r1 = r4
            goto L_0x015e
        L_0x01ed:
            r1 = r4
            goto L_0x013c
        L_0x01f0:
            r1 = r4
            goto L_0x011a
        L_0x01f3:
            r1 = r4
            goto L_0x00fa
        L_0x01f6:
            r2 = r3
            goto L_0x00bd
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.ad.a(org.apache.http.HttpEntity, android.os.Bundle, boolean):int");
    }

    /* JADX WARNING: Removed duplicated region for block: B:105:0x013b A[SYNTHETIC, Splitter:B:105:0x013b] */
    /* JADX WARNING: Removed duplicated region for block: B:108:0x0140 A[SYNTHETIC, Splitter:B:108:0x0140] */
    /* JADX WARNING: Removed duplicated region for block: B:140:0x0181  */
    /* JADX WARNING: Removed duplicated region for block: B:141:0x0184  */
    /* JADX WARNING: Removed duplicated region for block: B:142:0x0187  */
    /* JADX WARNING: Removed duplicated region for block: B:143:0x018a  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x005b A[SYNTHETIC, Splitter:B:24:0x005b] */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0061 A[SYNTHETIC, Splitter:B:28:0x0061] */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x00bc A[SYNTHETIC, Splitter:B:49:0x00bc] */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x00c2 A[SYNTHETIC, Splitter:B:53:0x00c2] */
    /* JADX WARNING: Removed duplicated region for block: B:89:0x0117 A[SYNTHETIC, Splitter:B:89:0x0117] */
    /* JADX WARNING: Removed duplicated region for block: B:92:0x011c A[SYNTHETIC, Splitter:B:92:0x011c] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private int b(boolean r10) {
        /*
            r9 = this;
            r7 = 1
            r1 = 0
            r6 = -7001(0xffffffffffffe4a7, float:NaN)
            r3 = 0
            r0 = -7000(0xffffffffffffe4a8, float:NaN)
            java.io.File r2 = new java.io.File     // Catch:{ FileNotFoundException -> 0x0178, IOException -> 0x016c, Exception -> 0x010f, all -> 0x0136 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x0178, IOException -> 0x016c, Exception -> 0x010f, all -> 0x0136 }
            r4.<init>()     // Catch:{ FileNotFoundException -> 0x0178, IOException -> 0x016c, Exception -> 0x010f, all -> 0x0136 }
            java.lang.String r5 = r9.e     // Catch:{ FileNotFoundException -> 0x0178, IOException -> 0x016c, Exception -> 0x010f, all -> 0x0136 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ FileNotFoundException -> 0x0178, IOException -> 0x016c, Exception -> 0x010f, all -> 0x0136 }
            java.lang.String r5 = java.io.File.separator     // Catch:{ FileNotFoundException -> 0x0178, IOException -> 0x016c, Exception -> 0x010f, all -> 0x0136 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ FileNotFoundException -> 0x0178, IOException -> 0x016c, Exception -> 0x010f, all -> 0x0136 }
            java.lang.String r5 = r9.g     // Catch:{ FileNotFoundException -> 0x0178, IOException -> 0x016c, Exception -> 0x010f, all -> 0x0136 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ FileNotFoundException -> 0x0178, IOException -> 0x016c, Exception -> 0x010f, all -> 0x0136 }
            java.lang.String r4 = r4.toString()     // Catch:{ FileNotFoundException -> 0x0178, IOException -> 0x016c, Exception -> 0x010f, all -> 0x0136 }
            r2.<init>(r4)     // Catch:{ FileNotFoundException -> 0x0178, IOException -> 0x016c, Exception -> 0x010f, all -> 0x0136 }
            boolean r4 = r2.exists()     // Catch:{ FileNotFoundException -> 0x00a0, IOException -> 0x00b3, Exception -> 0x0164, all -> 0x0159 }
            if (r4 == 0) goto L_0x00d2
            boolean r4 = r9.j     // Catch:{ FileNotFoundException -> 0x00a0, IOException -> 0x00b3, Exception -> 0x0164, all -> 0x0159 }
            if (r4 != 0) goto L_0x0071
            int r4 = r9.i     // Catch:{ FileNotFoundException -> 0x00a0, IOException -> 0x00b3, Exception -> 0x0164, all -> 0x0159 }
            if (r4 != r7) goto L_0x0071
            android.content.Context r4 = r9.c     // Catch:{ FileNotFoundException -> 0x00a0, IOException -> 0x00b3, Exception -> 0x0164, all -> 0x0159 }
            java.lang.String r5 = r9.h     // Catch:{ FileNotFoundException -> 0x00a0, IOException -> 0x00b3, Exception -> 0x0164, all -> 0x0159 }
            r7 = 1
            java.io.FileOutputStream r5 = r4.openFileOutput(r5, r7)     // Catch:{ FileNotFoundException -> 0x00a0, IOException -> 0x00b3, Exception -> 0x0164, all -> 0x0159 }
        L_0x003e:
            java.io.FileInputStream r4 = new java.io.FileInputStream     // Catch:{ FileNotFoundException -> 0x017d, IOException -> 0x0171, Exception -> 0x0167 }
            r4.<init>(r2)     // Catch:{ FileNotFoundException -> 0x017d, IOException -> 0x0171, Exception -> 0x0167 }
            r3 = 1024(0x400, float:1.435E-42)
            byte[] r3 = new byte[r3]     // Catch:{ FileNotFoundException -> 0x0053, IOException -> 0x0174, Exception -> 0x0169, all -> 0x015e }
        L_0x0047:
            int r7 = r4.read(r3)     // Catch:{ FileNotFoundException -> 0x0053, IOException -> 0x0174, Exception -> 0x0169, all -> 0x015e }
            r8 = -1
            if (r7 == r8) goto L_0x00d5
            r8 = 0
            r5.write(r3, r8, r7)     // Catch:{ FileNotFoundException -> 0x0053, IOException -> 0x0174, Exception -> 0x0169, all -> 0x015e }
            goto L_0x0047
        L_0x0053:
            r1 = move-exception
            r3 = r4
            r4 = r5
        L_0x0056:
            r1.printStackTrace()     // Catch:{ all -> 0x0161 }
            if (r3 == 0) goto L_0x018a
            r3.close()     // Catch:{ IOException -> 0x00f7 }
            r1 = r6
        L_0x005f:
            if (r4 == 0) goto L_0x0187
            r4.close()     // Catch:{ IOException -> 0x00fe }
            r0 = r1
        L_0x0065:
            if (r10 == 0) goto L_0x0070
            boolean r1 = r2.exists()
            if (r1 == 0) goto L_0x0070
            r2.delete()
        L_0x0070:
            return r0
        L_0x0071:
            java.io.File r4 = new java.io.File     // Catch:{ FileNotFoundException -> 0x00a0, IOException -> 0x00b3, Exception -> 0x0164, all -> 0x0159 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x00a0, IOException -> 0x00b3, Exception -> 0x0164, all -> 0x0159 }
            r5.<init>()     // Catch:{ FileNotFoundException -> 0x00a0, IOException -> 0x00b3, Exception -> 0x0164, all -> 0x0159 }
            java.lang.String r7 = r9.f     // Catch:{ FileNotFoundException -> 0x00a0, IOException -> 0x00b3, Exception -> 0x0164, all -> 0x0159 }
            java.lang.StringBuilder r5 = r5.append(r7)     // Catch:{ FileNotFoundException -> 0x00a0, IOException -> 0x00b3, Exception -> 0x0164, all -> 0x0159 }
            java.lang.String r7 = java.io.File.separator     // Catch:{ FileNotFoundException -> 0x00a0, IOException -> 0x00b3, Exception -> 0x0164, all -> 0x0159 }
            java.lang.StringBuilder r5 = r5.append(r7)     // Catch:{ FileNotFoundException -> 0x00a0, IOException -> 0x00b3, Exception -> 0x0164, all -> 0x0159 }
            java.lang.String r7 = r9.h     // Catch:{ FileNotFoundException -> 0x00a0, IOException -> 0x00b3, Exception -> 0x0164, all -> 0x0159 }
            java.lang.StringBuilder r5 = r5.append(r7)     // Catch:{ FileNotFoundException -> 0x00a0, IOException -> 0x00b3, Exception -> 0x0164, all -> 0x0159 }
            java.lang.String r5 = r5.toString()     // Catch:{ FileNotFoundException -> 0x00a0, IOException -> 0x00b3, Exception -> 0x0164, all -> 0x0159 }
            r4.<init>(r5)     // Catch:{ FileNotFoundException -> 0x00a0, IOException -> 0x00b3, Exception -> 0x0164, all -> 0x0159 }
            boolean r5 = r4.exists()     // Catch:{ FileNotFoundException -> 0x00a0, IOException -> 0x00b3, Exception -> 0x0164, all -> 0x0159 }
            if (r5 == 0) goto L_0x00a3
            r4.delete()     // Catch:{ FileNotFoundException -> 0x00a0, IOException -> 0x00b3, Exception -> 0x0164, all -> 0x0159 }
            java.io.FileOutputStream r5 = new java.io.FileOutputStream     // Catch:{ FileNotFoundException -> 0x00a0, IOException -> 0x00b3, Exception -> 0x0164, all -> 0x0159 }
            r5.<init>(r4)     // Catch:{ FileNotFoundException -> 0x00a0, IOException -> 0x00b3, Exception -> 0x0164, all -> 0x0159 }
            goto L_0x003e
        L_0x00a0:
            r1 = move-exception
            r4 = r3
            goto L_0x0056
        L_0x00a3:
            java.io.File r5 = r4.getParentFile()     // Catch:{ FileNotFoundException -> 0x00a0, IOException -> 0x00b3, Exception -> 0x0164, all -> 0x0159 }
            r5.mkdirs()     // Catch:{ FileNotFoundException -> 0x00a0, IOException -> 0x00b3, Exception -> 0x0164, all -> 0x0159 }
            r4.createNewFile()     // Catch:{ FileNotFoundException -> 0x00a0, IOException -> 0x00b3, Exception -> 0x0164, all -> 0x0159 }
            java.io.FileOutputStream r5 = new java.io.FileOutputStream     // Catch:{ FileNotFoundException -> 0x00a0, IOException -> 0x00b3, Exception -> 0x0164, all -> 0x0159 }
            r5.<init>(r4)     // Catch:{ FileNotFoundException -> 0x00a0, IOException -> 0x00b3, Exception -> 0x0164, all -> 0x0159 }
            goto L_0x003e
        L_0x00b3:
            r1 = move-exception
            r5 = r3
        L_0x00b5:
            r4 = -7056(0xffffffffffffe470, float:NaN)
            r1.printStackTrace()     // Catch:{ all -> 0x015c }
            if (r3 == 0) goto L_0x0184
            r3.close()     // Catch:{ IOException -> 0x0104 }
            r1 = r4
        L_0x00c0:
            if (r5 == 0) goto L_0x0181
            r5.close()     // Catch:{ IOException -> 0x010a }
            r0 = r1
        L_0x00c6:
            if (r10 == 0) goto L_0x0070
            boolean r1 = r2.exists()
            if (r1 == 0) goto L_0x0070
            r2.delete()
            goto L_0x0070
        L_0x00d2:
            r4 = r3
            r5 = r3
            r1 = r6
        L_0x00d5:
            if (r4 == 0) goto L_0x00da
            r4.close()     // Catch:{ IOException -> 0x00ec }
        L_0x00da:
            if (r5 == 0) goto L_0x018d
            r5.close()     // Catch:{ IOException -> 0x00f2 }
            r0 = r1
        L_0x00e0:
            if (r10 == 0) goto L_0x0070
            boolean r1 = r2.exists()
            if (r1 == 0) goto L_0x0070
            r2.delete()
            goto L_0x0070
        L_0x00ec:
            r1 = move-exception
            r1.printStackTrace()
            r1 = r0
            goto L_0x00da
        L_0x00f2:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x00e0
        L_0x00f7:
            r1 = move-exception
            r1.printStackTrace()
            r1 = r0
            goto L_0x005f
        L_0x00fe:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0065
        L_0x0104:
            r1 = move-exception
            r1.printStackTrace()
            r1 = r0
            goto L_0x00c0
        L_0x010a:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x00c6
        L_0x010f:
            r1 = move-exception
            r2 = r3
            r5 = r3
        L_0x0112:
            r1.printStackTrace()     // Catch:{ all -> 0x015c }
            if (r3 == 0) goto L_0x011a
            r3.close()     // Catch:{ IOException -> 0x012c }
        L_0x011a:
            if (r5 == 0) goto L_0x011f
            r5.close()     // Catch:{ IOException -> 0x0131 }
        L_0x011f:
            if (r10 == 0) goto L_0x0070
            boolean r1 = r2.exists()
            if (r1 == 0) goto L_0x0070
            r2.delete()
            goto L_0x0070
        L_0x012c:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x011a
        L_0x0131:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x011f
        L_0x0136:
            r0 = move-exception
            r2 = r3
            r5 = r3
        L_0x0139:
            if (r3 == 0) goto L_0x013e
            r3.close()     // Catch:{ IOException -> 0x014f }
        L_0x013e:
            if (r5 == 0) goto L_0x0143
            r5.close()     // Catch:{ IOException -> 0x0154 }
        L_0x0143:
            if (r10 == 0) goto L_0x014e
            boolean r1 = r2.exists()
            if (r1 == 0) goto L_0x014e
            r2.delete()
        L_0x014e:
            throw r0
        L_0x014f:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x013e
        L_0x0154:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0143
        L_0x0159:
            r0 = move-exception
            r5 = r3
            goto L_0x0139
        L_0x015c:
            r0 = move-exception
            goto L_0x0139
        L_0x015e:
            r0 = move-exception
            r3 = r4
            goto L_0x0139
        L_0x0161:
            r0 = move-exception
            r5 = r4
            goto L_0x0139
        L_0x0164:
            r1 = move-exception
            r5 = r3
            goto L_0x0112
        L_0x0167:
            r1 = move-exception
            goto L_0x0112
        L_0x0169:
            r1 = move-exception
            r3 = r4
            goto L_0x0112
        L_0x016c:
            r1 = move-exception
            r2 = r3
            r5 = r3
            goto L_0x00b5
        L_0x0171:
            r1 = move-exception
            goto L_0x00b5
        L_0x0174:
            r1 = move-exception
            r3 = r4
            goto L_0x00b5
        L_0x0178:
            r1 = move-exception
            r2 = r3
            r4 = r3
            goto L_0x0056
        L_0x017d:
            r1 = move-exception
            r4 = r5
            goto L_0x0056
        L_0x0181:
            r0 = r1
            goto L_0x00c6
        L_0x0184:
            r1 = r4
            goto L_0x00c0
        L_0x0187:
            r0 = r1
            goto L_0x0065
        L_0x018a:
            r1 = r6
            goto L_0x005f
        L_0x018d:
            r0 = r1
            goto L_0x00e0
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.ad.b(boolean):int");
    }

    private int c(String str) {
        int i2;
        URI uri = null;
        try {
            uri = new URI(str);
            i2 = -1000;
        } catch (URISyntaxException e2) {
            e2.printStackTrace();
            i2 = -1053;
        }
        if (uri == null) {
            return i2;
        }
        this.d.setURI(uri);
        return 0;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:106:0x01b5, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:107:0x01b6, code lost:
        r3 = r2;
        r2 = null;
        r1 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:108:0x01ba, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:109:0x01bb, code lost:
        r3 = r2;
        r2 = r1;
        r1 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:110:0x01bf, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:111:0x01c0, code lost:
        r3 = r2;
        r2 = null;
        r1 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:112:0x01c4, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:113:0x01c5, code lost:
        r3 = r2;
        r2 = r1;
        r1 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:114:0x01c9, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:115:0x01ca, code lost:
        r3 = r2;
        r2 = null;
        r1 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:116:0x01cf, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:117:0x01d0, code lost:
        r3 = r2;
        r2 = r1;
        r1 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:118:0x01d5, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:119:0x01d6, code lost:
        r3 = r2;
        r2 = null;
        r1 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:120:0x01db, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:121:0x01dc, code lost:
        r3 = r2;
        r2 = r1;
        r1 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:122:0x01e1, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:123:0x01e2, code lost:
        r3 = r2;
        r2 = null;
        r1 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:124:0x01e7, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:125:0x01e8, code lost:
        r3 = r2;
        r2 = r1;
        r1 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x0122, code lost:
        r3.getConnectionManager().shutdown();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x0141, code lost:
        r3.getConnectionManager().shutdown();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:0x0160, code lost:
        r3.getConnectionManager().shutdown();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:92:0x017f, code lost:
        r3.getConnectionManager().shutdown();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:95:0x0193, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:97:0x0196, code lost:
        r2.getConnectionManager().shutdown();
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x0122  */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x0141  */
    /* JADX WARNING: Removed duplicated region for block: B:83:0x0160  */
    /* JADX WARNING: Removed duplicated region for block: B:92:0x017f  */
    /* JADX WARNING: Removed duplicated region for block: B:95:0x0193 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:1:0x0009] */
    /* JADX WARNING: Removed duplicated region for block: B:97:0x0196  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int a(java.lang.String r11, boolean r12) {
        /*
            r10 = this;
            r3 = -3000(0xfffffffffffff448, float:NaN)
            r2 = 0
            r1 = 0
            android.os.Bundle r4 = new android.os.Bundle
            r4.<init>()
            org.apache.http.client.HttpClient r2 = r10.a()     // Catch:{ ClientProtocolException -> 0x00f8, SocketException -> 0x0117, SocketTimeoutException -> 0x0136, IOException -> 0x0155, Exception -> 0x0174, all -> 0x0193 }
            int r3 = r10.c(r11)     // Catch:{ ClientProtocolException -> 0x01e1, SocketException -> 0x01d5, SocketTimeoutException -> 0x01c9, IOException -> 0x01bf, Exception -> 0x01b5, all -> 0x0193 }
            if (r3 == 0) goto L_0x002e
            r0 = r3
        L_0x0014:
            if (r2 == 0) goto L_0x001d
            org.apache.http.conn.ClientConnectionManager r2 = r2.getConnectionManager()
            r2.shutdown()
        L_0x001d:
            if (r1 == 0) goto L_0x001f
        L_0x001f:
            if (r0 == 0) goto L_0x002d
            r1 = -7
            if (r0 == r1) goto L_0x002d
            java.lang.String r1 = "key_errcode"
            r4.putInt(r1, r0)
            r1 = 1
            r10.a(r1, r4)
        L_0x002d:
            return r0
        L_0x002e:
            boolean r0 = r10.k     // Catch:{ ClientProtocolException -> 0x01e1, SocketException -> 0x01d5, SocketTimeoutException -> 0x01c9, IOException -> 0x01bf, Exception -> 0x01b5, all -> 0x0193 }
            if (r0 == 0) goto L_0x0036
            r3 = -3003(0xfffffffffffff445, float:NaN)
            r0 = r3
            goto L_0x0014
        L_0x0036:
            org.apache.http.client.methods.HttpGet r0 = r10.d     // Catch:{ ClientProtocolException -> 0x01e1, SocketException -> 0x01d5, SocketTimeoutException -> 0x01c9, IOException -> 0x01bf, Exception -> 0x01b5, all -> 0x0193 }
            java.net.URI r0 = r0.getURI()     // Catch:{ ClientProtocolException -> 0x01e1, SocketException -> 0x01d5, SocketTimeoutException -> 0x01c9, IOException -> 0x01bf, Exception -> 0x01b5, all -> 0x0193 }
            if (r0 != 0) goto L_0x0042
            r3 = -3053(0xfffffffffffff413, float:NaN)
            r0 = r3
            goto L_0x0014
        L_0x0042:
            java.lang.String r0 = defpackage.ay.c(r11)     // Catch:{ ClientProtocolException -> 0x01e1, SocketException -> 0x01d5, SocketTimeoutException -> 0x01c9, IOException -> 0x01bf, Exception -> 0x01b5, all -> 0x0193 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ ClientProtocolException -> 0x01e1, SocketException -> 0x01d5, SocketTimeoutException -> 0x01c9, IOException -> 0x01bf, Exception -> 0x01b5, all -> 0x0193 }
            r5.<init>()     // Catch:{ ClientProtocolException -> 0x01e1, SocketException -> 0x01d5, SocketTimeoutException -> 0x01c9, IOException -> 0x01bf, Exception -> 0x01b5, all -> 0x0193 }
            java.lang.StringBuilder r5 = r5.append(r0)     // Catch:{ ClientProtocolException -> 0x01e1, SocketException -> 0x01d5, SocketTimeoutException -> 0x01c9, IOException -> 0x01bf, Exception -> 0x01b5, all -> 0x0193 }
            java.lang.String r6 = ".tmp"
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ ClientProtocolException -> 0x01e1, SocketException -> 0x01d5, SocketTimeoutException -> 0x01c9, IOException -> 0x01bf, Exception -> 0x01b5, all -> 0x0193 }
            java.lang.String r5 = r5.toString()     // Catch:{ ClientProtocolException -> 0x01e1, SocketException -> 0x01d5, SocketTimeoutException -> 0x01c9, IOException -> 0x01bf, Exception -> 0x01b5, all -> 0x0193 }
            r10.g = r5     // Catch:{ ClientProtocolException -> 0x01e1, SocketException -> 0x01d5, SocketTimeoutException -> 0x01c9, IOException -> 0x01bf, Exception -> 0x01b5, all -> 0x0193 }
            java.lang.String r5 = r10.h     // Catch:{ ClientProtocolException -> 0x01e1, SocketException -> 0x01d5, SocketTimeoutException -> 0x01c9, IOException -> 0x01bf, Exception -> 0x01b5, all -> 0x0193 }
            if (r5 != 0) goto L_0x0061
            r10.h = r0     // Catch:{ ClientProtocolException -> 0x01e1, SocketException -> 0x01d5, SocketTimeoutException -> 0x01c9, IOException -> 0x01bf, Exception -> 0x01b5, all -> 0x0193 }
        L_0x0061:
            java.io.File r0 = new java.io.File     // Catch:{ ClientProtocolException -> 0x01e1, SocketException -> 0x01d5, SocketTimeoutException -> 0x01c9, IOException -> 0x01bf, Exception -> 0x01b5, all -> 0x0193 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ ClientProtocolException -> 0x01e1, SocketException -> 0x01d5, SocketTimeoutException -> 0x01c9, IOException -> 0x01bf, Exception -> 0x01b5, all -> 0x0193 }
            r5.<init>()     // Catch:{ ClientProtocolException -> 0x01e1, SocketException -> 0x01d5, SocketTimeoutException -> 0x01c9, IOException -> 0x01bf, Exception -> 0x01b5, all -> 0x0193 }
            java.lang.String r6 = r10.e     // Catch:{ ClientProtocolException -> 0x01e1, SocketException -> 0x01d5, SocketTimeoutException -> 0x01c9, IOException -> 0x01bf, Exception -> 0x01b5, all -> 0x0193 }
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ ClientProtocolException -> 0x01e1, SocketException -> 0x01d5, SocketTimeoutException -> 0x01c9, IOException -> 0x01bf, Exception -> 0x01b5, all -> 0x0193 }
            java.lang.String r6 = java.io.File.separator     // Catch:{ ClientProtocolException -> 0x01e1, SocketException -> 0x01d5, SocketTimeoutException -> 0x01c9, IOException -> 0x01bf, Exception -> 0x01b5, all -> 0x0193 }
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ ClientProtocolException -> 0x01e1, SocketException -> 0x01d5, SocketTimeoutException -> 0x01c9, IOException -> 0x01bf, Exception -> 0x01b5, all -> 0x0193 }
            java.lang.String r6 = r10.g     // Catch:{ ClientProtocolException -> 0x01e1, SocketException -> 0x01d5, SocketTimeoutException -> 0x01c9, IOException -> 0x01bf, Exception -> 0x01b5, all -> 0x0193 }
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ ClientProtocolException -> 0x01e1, SocketException -> 0x01d5, SocketTimeoutException -> 0x01c9, IOException -> 0x01bf, Exception -> 0x01b5, all -> 0x0193 }
            java.lang.String r5 = r5.toString()     // Catch:{ ClientProtocolException -> 0x01e1, SocketException -> 0x01d5, SocketTimeoutException -> 0x01c9, IOException -> 0x01bf, Exception -> 0x01b5, all -> 0x0193 }
            r0.<init>(r5)     // Catch:{ ClientProtocolException -> 0x01e1, SocketException -> 0x01d5, SocketTimeoutException -> 0x01c9, IOException -> 0x01bf, Exception -> 0x01b5, all -> 0x0193 }
            boolean r5 = r0.exists()     // Catch:{ ClientProtocolException -> 0x01e1, SocketException -> 0x01d5, SocketTimeoutException -> 0x01c9, IOException -> 0x01bf, Exception -> 0x01b5, all -> 0x0193 }
            if (r5 == 0) goto L_0x00af
            long r5 = r0.length()     // Catch:{ ClientProtocolException -> 0x01e1, SocketException -> 0x01d5, SocketTimeoutException -> 0x01c9, IOException -> 0x01bf, Exception -> 0x01b5, all -> 0x0193 }
            r10.l = r5     // Catch:{ ClientProtocolException -> 0x01e1, SocketException -> 0x01d5, SocketTimeoutException -> 0x01c9, IOException -> 0x01bf, Exception -> 0x01b5, all -> 0x0193 }
            org.apache.http.client.methods.HttpGet r0 = r10.d     // Catch:{ ClientProtocolException -> 0x01e1, SocketException -> 0x01d5, SocketTimeoutException -> 0x01c9, IOException -> 0x01bf, Exception -> 0x01b5, all -> 0x0193 }
            java.lang.String r5 = "RANGE"
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ ClientProtocolException -> 0x01e1, SocketException -> 0x01d5, SocketTimeoutException -> 0x01c9, IOException -> 0x01bf, Exception -> 0x01b5, all -> 0x0193 }
            r6.<init>()     // Catch:{ ClientProtocolException -> 0x01e1, SocketException -> 0x01d5, SocketTimeoutException -> 0x01c9, IOException -> 0x01bf, Exception -> 0x01b5, all -> 0x0193 }
            java.lang.String r7 = "bytes="
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ ClientProtocolException -> 0x01e1, SocketException -> 0x01d5, SocketTimeoutException -> 0x01c9, IOException -> 0x01bf, Exception -> 0x01b5, all -> 0x0193 }
            long r7 = r10.l     // Catch:{ ClientProtocolException -> 0x01e1, SocketException -> 0x01d5, SocketTimeoutException -> 0x01c9, IOException -> 0x01bf, Exception -> 0x01b5, all -> 0x0193 }
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ ClientProtocolException -> 0x01e1, SocketException -> 0x01d5, SocketTimeoutException -> 0x01c9, IOException -> 0x01bf, Exception -> 0x01b5, all -> 0x0193 }
            java.lang.String r7 = "-"
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ ClientProtocolException -> 0x01e1, SocketException -> 0x01d5, SocketTimeoutException -> 0x01c9, IOException -> 0x01bf, Exception -> 0x01b5, all -> 0x0193 }
            java.lang.String r6 = r6.toString()     // Catch:{ ClientProtocolException -> 0x01e1, SocketException -> 0x01d5, SocketTimeoutException -> 0x01c9, IOException -> 0x01bf, Exception -> 0x01b5, all -> 0x0193 }
            r0.setHeader(r5, r6)     // Catch:{ ClientProtocolException -> 0x01e1, SocketException -> 0x01d5, SocketTimeoutException -> 0x01c9, IOException -> 0x01bf, Exception -> 0x01b5, all -> 0x0193 }
        L_0x00af:
            org.apache.http.client.methods.HttpGet r0 = r10.d     // Catch:{ ClientProtocolException -> 0x01e1, SocketException -> 0x01d5, SocketTimeoutException -> 0x01c9, IOException -> 0x01bf, Exception -> 0x01b5, all -> 0x0193 }
            org.apache.http.HttpResponse r1 = r2.execute(r0)     // Catch:{ ClientProtocolException -> 0x01e1, SocketException -> 0x01d5, SocketTimeoutException -> 0x01c9, IOException -> 0x01bf, Exception -> 0x01b5, all -> 0x0193 }
            org.apache.http.StatusLine r0 = r1.getStatusLine()     // Catch:{ ClientProtocolException -> 0x01e7, SocketException -> 0x01db, SocketTimeoutException -> 0x01cf, IOException -> 0x01c4, Exception -> 0x01ba, all -> 0x0193 }
            int r0 = r0.getStatusCode()     // Catch:{ ClientProtocolException -> 0x01e7, SocketException -> 0x01db, SocketTimeoutException -> 0x01cf, IOException -> 0x01c4, Exception -> 0x01ba, all -> 0x0193 }
            r5 = 200(0xc8, float:2.8E-43)
            if (r0 == r5) goto L_0x00ca
            r5 = 206(0xce, float:2.89E-43)
            if (r0 == r5) goto L_0x00ca
            int r3 = -3000 - r0
            r0 = r3
            goto L_0x0014
        L_0x00ca:
            boolean r0 = r10.k     // Catch:{ ClientProtocolException -> 0x01e7, SocketException -> 0x01db, SocketTimeoutException -> 0x01cf, IOException -> 0x01c4, Exception -> 0x01ba, all -> 0x0193 }
            if (r0 == 0) goto L_0x00d3
            r3 = -3003(0xfffffffffffff445, float:NaN)
            r0 = r3
            goto L_0x0014
        L_0x00d3:
            org.apache.http.HttpEntity r0 = r1.getEntity()     // Catch:{ ClientProtocolException -> 0x01e7, SocketException -> 0x01db, SocketTimeoutException -> 0x01cf, IOException -> 0x01c4, Exception -> 0x01ba, all -> 0x0193 }
            if (r0 != 0) goto L_0x00de
            r3 = -4000(0xfffffffffffff060, float:NaN)
            r0 = r3
            goto L_0x0014
        L_0x00de:
            int r3 = r10.a(r0, r4, r12)     // Catch:{ ClientProtocolException -> 0x01e7, SocketException -> 0x01db, SocketTimeoutException -> 0x01cf, IOException -> 0x01c4, Exception -> 0x01ba, all -> 0x0193 }
            if (r3 == 0) goto L_0x00ea
            r0 = -7
            if (r3 != r0) goto L_0x01ed
            r0 = r3
            goto L_0x0014
        L_0x00ea:
            r0 = 1
            int r3 = r10.b(r0)     // Catch:{ ClientProtocolException -> 0x01e7, SocketException -> 0x01db, SocketTimeoutException -> 0x01cf, IOException -> 0x01c4, Exception -> 0x01ba, all -> 0x0193 }
            if (r3 == 0) goto L_0x00f4
            r0 = r3
            goto L_0x0014
        L_0x00f4:
            r3 = 0
            r0 = r3
            goto L_0x0014
        L_0x00f8:
            r0 = move-exception
            r3 = r2
            r2 = r1
            r1 = r0
        L_0x00fc:
            r0 = -3051(0xfffffffffffff415, float:NaN)
            r1.printStackTrace()     // Catch:{ all -> 0x01ae }
            if (r3 == 0) goto L_0x010a
            org.apache.http.conn.ClientConnectionManager r1 = r3.getConnectionManager()
            r1.shutdown()
        L_0x010a:
            if (r2 == 0) goto L_0x010c
        L_0x010c:
            java.lang.String r1 = "key_errcode"
            r4.putInt(r1, r0)
            r1 = 1
            r10.a(r1, r4)
            goto L_0x002d
        L_0x0117:
            r0 = move-exception
            r3 = r2
            r2 = r1
            r1 = r0
        L_0x011b:
            r0 = -3054(0xfffffffffffff412, float:NaN)
            r1.printStackTrace()     // Catch:{ all -> 0x01ae }
            if (r3 == 0) goto L_0x0129
            org.apache.http.conn.ClientConnectionManager r1 = r3.getConnectionManager()
            r1.shutdown()
        L_0x0129:
            if (r2 == 0) goto L_0x012b
        L_0x012b:
            java.lang.String r1 = "key_errcode"
            r4.putInt(r1, r0)
            r1 = 1
            r10.a(r1, r4)
            goto L_0x002d
        L_0x0136:
            r0 = move-exception
            r3 = r2
            r2 = r1
            r1 = r0
        L_0x013a:
            r0 = -3055(0xfffffffffffff411, float:NaN)
            r1.printStackTrace()     // Catch:{ all -> 0x01ae }
            if (r3 == 0) goto L_0x0148
            org.apache.http.conn.ClientConnectionManager r1 = r3.getConnectionManager()
            r1.shutdown()
        L_0x0148:
            if (r2 == 0) goto L_0x014a
        L_0x014a:
            java.lang.String r1 = "key_errcode"
            r4.putInt(r1, r0)
            r1 = 1
            r10.a(r1, r4)
            goto L_0x002d
        L_0x0155:
            r0 = move-exception
            r3 = r2
            r2 = r1
            r1 = r0
        L_0x0159:
            r0 = -3056(0xfffffffffffff410, float:NaN)
            r1.printStackTrace()     // Catch:{ all -> 0x01ae }
            if (r3 == 0) goto L_0x0167
            org.apache.http.conn.ClientConnectionManager r1 = r3.getConnectionManager()
            r1.shutdown()
        L_0x0167:
            if (r2 == 0) goto L_0x0169
        L_0x0169:
            java.lang.String r1 = "key_errcode"
            r4.putInt(r1, r0)
            r1 = 1
            r10.a(r1, r4)
            goto L_0x002d
        L_0x0174:
            r0 = move-exception
            r3 = r2
            r2 = r1
            r1 = r0
        L_0x0178:
            r0 = -3000(0xfffffffffffff448, float:NaN)
            r1.printStackTrace()     // Catch:{ all -> 0x01ae }
            if (r3 == 0) goto L_0x0186
            org.apache.http.conn.ClientConnectionManager r1 = r3.getConnectionManager()
            r1.shutdown()
        L_0x0186:
            if (r2 == 0) goto L_0x0188
        L_0x0188:
            java.lang.String r1 = "key_errcode"
            r4.putInt(r1, r0)
            r1 = 1
            r10.a(r1, r4)
            goto L_0x002d
        L_0x0193:
            r0 = move-exception
        L_0x0194:
            if (r2 == 0) goto L_0x019d
            org.apache.http.conn.ClientConnectionManager r2 = r2.getConnectionManager()
            r2.shutdown()
        L_0x019d:
            if (r1 == 0) goto L_0x019f
        L_0x019f:
            if (r3 == 0) goto L_0x01ad
            r1 = -7
            if (r3 == r1) goto L_0x01ad
            java.lang.String r1 = "key_errcode"
            r4.putInt(r1, r3)
            r1 = 1
            r10.a(r1, r4)
        L_0x01ad:
            throw r0
        L_0x01ae:
            r1 = move-exception
            r9 = r1
            r1 = r2
            r2 = r3
            r3 = r0
            r0 = r9
            goto L_0x0194
        L_0x01b5:
            r0 = move-exception
            r3 = r2
            r2 = r1
            r1 = r0
            goto L_0x0178
        L_0x01ba:
            r0 = move-exception
            r3 = r2
            r2 = r1
            r1 = r0
            goto L_0x0178
        L_0x01bf:
            r0 = move-exception
            r3 = r2
            r2 = r1
            r1 = r0
            goto L_0x0159
        L_0x01c4:
            r0 = move-exception
            r3 = r2
            r2 = r1
            r1 = r0
            goto L_0x0159
        L_0x01c9:
            r0 = move-exception
            r3 = r2
            r2 = r1
            r1 = r0
            goto L_0x013a
        L_0x01cf:
            r0 = move-exception
            r3 = r2
            r2 = r1
            r1 = r0
            goto L_0x013a
        L_0x01d5:
            r0 = move-exception
            r3 = r2
            r2 = r1
            r1 = r0
            goto L_0x011b
        L_0x01db:
            r0 = move-exception
            r3 = r2
            r2 = r1
            r1 = r0
            goto L_0x011b
        L_0x01e1:
            r0 = move-exception
            r3 = r2
            r2 = r1
            r1 = r0
            goto L_0x00fc
        L_0x01e7:
            r0 = move-exception
            r3 = r2
            r2 = r1
            r1 = r0
            goto L_0x00fc
        L_0x01ed:
            r0 = r3
            goto L_0x0014
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.ad.a(java.lang.String, boolean):int");
    }

    public void a(int i2) {
        this.i = i2;
    }

    public void a(String str) {
        this.f = str;
    }

    public String b() {
        return this.f + File.separator + this.h;
    }

    public void b(String str) {
        this.h = str;
    }
}
