package defpackage;

import android.content.Context;
import android.os.Build;

/* renamed from: au  reason: default package */
public class au {

    /* renamed from: a  reason: collision with root package name */
    private static String f107a = "EnvUtil";
    private static long b = -1;
    private static Integer c = null;

    public static String a() {
        String str = Build.MANUFACTURER;
        return str == null ? "UNKNOWN" : str;
    }

    public static String a(Context context) {
        return (new String() + "MANUFACTURER " + a() + ";") + "ram " + b() + ";";
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x0034 A[SYNTHETIC, Splitter:B:15:0x0034] */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x003f  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x006c A[SYNTHETIC, Splitter:B:34:0x006c] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x007c A[SYNTHETIC, Splitter:B:43:0x007c] */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x0089 A[SYNTHETIC, Splitter:B:50:0x0089] */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x0092 A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:31:0x0067=Splitter:B:31:0x0067, B:40:0x0077=Splitter:B:40:0x0077, B:12:0x002f=Splitter:B:12:0x002f} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static long b() {
        /*
            long r0 = defpackage.au.b
            r2 = -1
            int r0 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r0 != 0) goto L_0x0037
            java.io.File r0 = new java.io.File
            java.lang.String r1 = "/proc/meminfo"
            r0.<init>(r1)
            r2 = 0
            boolean r1 = r0.exists()
            if (r1 == 0) goto L_0x0037
            java.io.DataInputStream r1 = new java.io.DataInputStream     // Catch:{ FileNotFoundException -> 0x009b, IOException -> 0x0065, NumberFormatException -> 0x0075, all -> 0x0085 }
            java.io.FileInputStream r3 = new java.io.FileInputStream     // Catch:{ FileNotFoundException -> 0x009b, IOException -> 0x0065, NumberFormatException -> 0x0075, all -> 0x0085 }
            r3.<init>(r0)     // Catch:{ FileNotFoundException -> 0x009b, IOException -> 0x0065, NumberFormatException -> 0x0075, all -> 0x0085 }
            r1.<init>(r3)     // Catch:{ FileNotFoundException -> 0x009b, IOException -> 0x0065, NumberFormatException -> 0x0075, all -> 0x0085 }
            java.lang.String r0 = r1.readLine()     // Catch:{ FileNotFoundException -> 0x002e, IOException -> 0x0099, NumberFormatException -> 0x0097 }
            if (r0 != 0) goto L_0x0042
            java.io.IOException r0 = new java.io.IOException     // Catch:{ FileNotFoundException -> 0x002e, IOException -> 0x0099, NumberFormatException -> 0x0097 }
            java.lang.String r2 = "/proc/meminfo is empty!"
            r0.<init>(r2)     // Catch:{ FileNotFoundException -> 0x002e, IOException -> 0x0099, NumberFormatException -> 0x0097 }
            throw r0     // Catch:{ FileNotFoundException -> 0x002e, IOException -> 0x0099, NumberFormatException -> 0x0097 }
        L_0x002e:
            r0 = move-exception
        L_0x002f:
            r0.printStackTrace()     // Catch:{ all -> 0x0095 }
            if (r1 == 0) goto L_0x0037
            r1.close()     // Catch:{ IOException -> 0x0060 }
        L_0x0037:
            long r0 = defpackage.au.b
            r2 = 0
            int r0 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r0 <= 0) goto L_0x0092
            long r0 = defpackage.au.b
        L_0x0041:
            return r0
        L_0x0042:
            java.lang.String r0 = r0.trim()     // Catch:{ FileNotFoundException -> 0x002e, IOException -> 0x0099, NumberFormatException -> 0x0097 }
            java.lang.String r2 = "[\\s]+"
            java.lang.String[] r0 = r0.split(r2)     // Catch:{ FileNotFoundException -> 0x002e, IOException -> 0x0099, NumberFormatException -> 0x0097 }
            r2 = 1
            r0 = r0[r2]     // Catch:{ FileNotFoundException -> 0x002e, IOException -> 0x0099, NumberFormatException -> 0x0097 }
            long r2 = java.lang.Long.parseLong(r0)     // Catch:{ FileNotFoundException -> 0x002e, IOException -> 0x0099, NumberFormatException -> 0x0097 }
            defpackage.au.b = r2     // Catch:{ FileNotFoundException -> 0x002e, IOException -> 0x0099, NumberFormatException -> 0x0097 }
            if (r1 == 0) goto L_0x0037
            r1.close()     // Catch:{ IOException -> 0x005b }
            goto L_0x0037
        L_0x005b:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0037
        L_0x0060:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0037
        L_0x0065:
            r0 = move-exception
            r1 = r2
        L_0x0067:
            r0.printStackTrace()     // Catch:{ all -> 0x0095 }
            if (r1 == 0) goto L_0x0037
            r1.close()     // Catch:{ IOException -> 0x0070 }
            goto L_0x0037
        L_0x0070:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0037
        L_0x0075:
            r0 = move-exception
            r1 = r2
        L_0x0077:
            r0.printStackTrace()     // Catch:{ all -> 0x0095 }
            if (r1 == 0) goto L_0x0037
            r1.close()     // Catch:{ IOException -> 0x0080 }
            goto L_0x0037
        L_0x0080:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0037
        L_0x0085:
            r0 = move-exception
            r1 = r2
        L_0x0087:
            if (r1 == 0) goto L_0x008c
            r1.close()     // Catch:{ IOException -> 0x008d }
        L_0x008c:
            throw r0
        L_0x008d:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x008c
        L_0x0092:
            r0 = 1
            goto L_0x0041
        L_0x0095:
            r0 = move-exception
            goto L_0x0087
        L_0x0097:
            r0 = move-exception
            goto L_0x0077
        L_0x0099:
            r0 = move-exception
            goto L_0x0067
        L_0x009b:
            r0 = move-exception
            r1 = r2
            goto L_0x002f
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.au.b():long");
    }
}
