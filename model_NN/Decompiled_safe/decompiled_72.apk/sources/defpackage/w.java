package defpackage;

import android.content.Context;
import android.content.Intent;
import com.qq.jce.wup.UniAttribute;
import com.tencent.connect.common.Constants;
import com.tencent.securemodule.jni.SecureEngine;
import com.tencent.securemodule.ui.TransparentActivity;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

/* renamed from: w  reason: default package */
public class w {
    private static w e = null;

    /* renamed from: a  reason: collision with root package name */
    private Context f4164a;
    private SecureEngine b;
    private List<e> c;
    private boolean d = false;

    private w(Context context) {
        this.b = new SecureEngine(context);
        this.f4164a = context;
    }

    public static w a(Context context) {
        if (e == null) {
            e = new w(context);
        }
        return e;
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x0015  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized void a(defpackage.e r3, boolean r4) {
        /*
            r2 = this;
            monitor-enter(r2)
            java.util.ArrayList r0 = r3.c()     // Catch:{ all -> 0x0022 }
            if (r0 != 0) goto L_0x0009
        L_0x0007:
            monitor-exit(r2)
            return
        L_0x0009:
            if (r4 == 0) goto L_0x0007
            java.util.Iterator r1 = r0.iterator()     // Catch:{ all -> 0x0022 }
        L_0x000f:
            boolean r0 = r1.hasNext()     // Catch:{ all -> 0x0022 }
            if (r0 == 0) goto L_0x0007
            java.lang.Object r0 = r1.next()     // Catch:{ all -> 0x0022 }
            c r0 = (defpackage.c) r0     // Catch:{ all -> 0x0022 }
            int r0 = r2.a(r3, r0)     // Catch:{ all -> 0x0022 }
            if (r0 == 0) goto L_0x000f
            goto L_0x0007
        L_0x0022:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.w.a(e, boolean):void");
    }

    private static List<b> b(Context context) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new b(4, 21, SecureEngine.getEngineVersion(context) + "&productid=" + as.a(context, 30003, 0), Constants.STR_EMPTY));
        arrayList.add(new b(8, 21, "buildno=" + as.a(context, 30004, 0) + "&version=" + as.a(context, 30001, Constants.STR_EMPTY) + "&productid=" + as.a(context, 30003, 0), Constants.STR_EMPTY));
        return arrayList;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: w.a(e, boolean):void
     arg types: [e, int]
     candidates:
      w.a(e, c):int
      w.a(e, boolean):void */
    private void b(e eVar) {
        t b2;
        if (a(eVar) && (b2 = eVar.b()) != null) {
            switch (b2.c()) {
                case 0:
                    a(eVar, true);
                    return;
                case 6:
                    this.d = true;
                    Intent intent = new Intent();
                    intent.setClass(this.f4164a, TransparentActivity.class);
                    intent.addFlags(268435456);
                    intent.setAction("1000040");
                    intent.putExtra("data", eVar);
                    this.f4164a.startActivity(intent);
                    return;
                case 8:
                default:
                    return;
            }
        }
    }

    public int a(e eVar, c cVar) {
        int a2;
        boolean z = false;
        boolean z2 = true;
        UniAttribute uniAttribute = new UniAttribute();
        uniAttribute.setEncodeName("UTF-8");
        uniAttribute.decode(cVar.b());
        switch (cVar.a()) {
            case 200:
                UniAttribute uniAttribute2 = new UniAttribute();
                uniAttribute2.decode(cVar.b());
                r rVar = (r) uniAttribute2.getByClass("cloudcmd", new r());
                if (rVar != null) {
                    if (eVar.a().a() == 1) {
                        z = true;
                    }
                    a2 = az.a(this.f4164a, rVar.a(), z);
                    break;
                }
            default:
                z2 = false;
                a2 = -5;
                break;
        }
        return !z2 ? this.b.scanThreatens(cVar.a(), cVar.b()) : a2;
    }

    public synchronized void a(int i) {
        ArrayList<e> arrayList = new ArrayList<>();
        for (e next : this.c) {
            if (next != null && next.a().a() == i) {
                arrayList.add(next);
            }
        }
        if (arrayList.size() > 0) {
            for (e eVar : arrayList) {
                b(eVar);
                this.c.remove(eVar);
            }
        }
    }

    public boolean a() {
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public boolean a(e eVar) {
        return eVar.a().b() == 0 || System.currentTimeMillis() / 1000 < ((long) eVar.a().b());
    }

    public int b() {
        List<b> b2 = b(this.f4164a);
        AtomicReference atomicReference = new AtomicReference();
        int a2 = ag.a(this.f4164a).a(b2, atomicReference);
        if (a2 != 0) {
            return a2;
        }
        q qVar = (q) atomicReference.get();
        if (qVar != null) {
            this.c = qVar.a();
            a(1);
        }
        return a2;
    }
}
