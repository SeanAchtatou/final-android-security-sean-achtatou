package android.support.v4.a;

import android.os.Parcel;
import android.os.Parcelable;

/* compiled from: ProGuard */
class b<T> implements Parcelable.Creator<T> {

    /* renamed from: a  reason: collision with root package name */
    final c<T> f36a;

    public b(c<T> cVar) {
        this.f36a = cVar;
    }

    public T createFromParcel(Parcel parcel) {
        return this.f36a.a(parcel, null);
    }

    public T[] newArray(int i) {
        return this.f36a.a(i);
    }
}
