package android.support.v4.view;

import android.database.DataSetObserver;
import android.support.v4.view.ViewPager;

/* compiled from: ProGuard */
class ao extends DataSetObserver implements ViewPager.OnPageChangeListener, bw {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ PagerTitleStrip f83a;
    private int b;

    private ao(PagerTitleStrip pagerTitleStrip) {
        this.f83a = pagerTitleStrip;
    }

    public void onPageScrolled(int i, float f, int i2) {
        if (f > 0.5f) {
            i++;
        }
        this.f83a.updateTextPositions(i, f, false);
    }

    public void onPageSelected(int i) {
        float f = 0.0f;
        if (this.b == 0) {
            this.f83a.updateText(this.f83a.mPager.getCurrentItem(), this.f83a.mPager.getAdapter());
            if (this.f83a.mLastKnownPositionOffset >= 0.0f) {
                f = this.f83a.mLastKnownPositionOffset;
            }
            this.f83a.updateTextPositions(this.f83a.mPager.getCurrentItem(), f, true);
        }
    }

    public void onPageScrollStateChanged(int i) {
        this.b = i;
    }

    public void a(PagerAdapter pagerAdapter, PagerAdapter pagerAdapter2) {
        this.f83a.updateAdapter(pagerAdapter, pagerAdapter2);
    }

    public void onChanged() {
        float f = 0.0f;
        this.f83a.updateText(this.f83a.mPager.getCurrentItem(), this.f83a.mPager.getAdapter());
        if (this.f83a.mLastKnownPositionOffset >= 0.0f) {
            f = this.f83a.mLastKnownPositionOffset;
        }
        this.f83a.updateTextPositions(this.f83a.mPager.getCurrentItem(), f, true);
    }
}
