package android.support.v4.view;

import android.os.Bundle;
import android.support.v4.view.a.g;
import android.view.View;

/* compiled from: ProGuard */
class d extends a {
    d() {
    }

    public Object a(AccessibilityDelegateCompat accessibilityDelegateCompat) {
        return AccessibilityDelegateCompatJellyBean.a(new e(this, accessibilityDelegateCompat));
    }

    public g a(Object obj, View view) {
        Object a2 = AccessibilityDelegateCompatJellyBean.a(obj, view);
        if (a2 != null) {
            return new g(a2);
        }
        return null;
    }

    public boolean a(Object obj, View view, int i, Bundle bundle) {
        return AccessibilityDelegateCompatJellyBean.a(obj, view, i, bundle);
    }
}
