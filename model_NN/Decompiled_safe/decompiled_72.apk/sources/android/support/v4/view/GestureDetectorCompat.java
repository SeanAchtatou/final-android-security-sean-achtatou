package android.support.v4.view;

import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.view.GestureDetector;
import android.view.MotionEvent;

/* compiled from: ProGuard */
public class GestureDetectorCompat {
    private final s mImpl;

    public GestureDetectorCompat(Context context, GestureDetector.OnGestureListener onGestureListener) {
        this(context, onGestureListener, null);
    }

    public GestureDetectorCompat(Context context, GestureDetector.OnGestureListener onGestureListener, Handler handler) {
        if (Build.VERSION.SDK_INT >= 17) {
            this.mImpl = new v(context, onGestureListener, handler);
        } else {
            this.mImpl = new t(context, onGestureListener, handler);
        }
    }

    public boolean isLongpressEnabled() {
        return this.mImpl.a();
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        return this.mImpl.a(motionEvent);
    }

    public void setIsLongpressEnabled(boolean z) {
        this.mImpl.a(z);
    }

    public void setOnDoubleTapListener(GestureDetector.OnDoubleTapListener onDoubleTapListener) {
        this.mImpl.a(onDoubleTapListener);
    }
}
