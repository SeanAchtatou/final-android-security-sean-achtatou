package android.support.v4.view;

import android.support.v4.view.DirectionalViewPager;
import android.view.View;
import java.util.Comparator;

/* compiled from: ProGuard */
class r implements Comparator<View> {
    r() {
    }

    /* renamed from: a */
    public int compare(View view, View view2) {
        DirectionalViewPager.LayoutParams layoutParams = (DirectionalViewPager.LayoutParams) view.getLayoutParams();
        DirectionalViewPager.LayoutParams layoutParams2 = (DirectionalViewPager.LayoutParams) view2.getLayoutParams();
        if (layoutParams.isDecor != layoutParams2.isDecor) {
            return layoutParams.isDecor ? 1 : -1;
        }
        return layoutParams.position - layoutParams2.position;
    }
}
