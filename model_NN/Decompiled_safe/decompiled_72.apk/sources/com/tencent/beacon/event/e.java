package com.tencent.beacon.event;

import android.content.Context;
import com.tencent.beacon.a.h;
import com.tencent.beacon.c.a.a;
import com.tencent.beacon.c.d.c;
import com.tencent.beacon.c.d.d;
import com.tencent.beacon.f.b;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* compiled from: ProGuard */
public final class e extends b {
    private Context e = null;
    private Long[] f = null;
    private List<Long> g = null;
    private byte[] h = null;
    private boolean i = false;

    public e(Context context) {
        super(context, 1, 4);
        this.e = context;
    }

    private synchronized boolean f() {
        return this.i;
    }

    public final synchronized void a(boolean z) {
        this.i = z;
    }

    public final synchronized com.tencent.beacon.c.a.b a() {
        byte[] bArr;
        List<i> list;
        a aVar;
        byte[] bArr2;
        com.tencent.beacon.c.a.b bVar;
        List a2;
        com.tencent.beacon.c.a.b bVar2 = null;
        synchronized (this) {
            t d = t.d();
            if (d == null || !d.e()) {
                com.tencent.beacon.d.a.c(" imposiable! ua not ready!", new Object[0]);
            } else {
                g g2 = d.g();
                if (g2 == null) {
                    com.tencent.beacon.d.a.c(" imposiable! ua S not ready!", new Object[0]);
                } else {
                    try {
                        if (!f() || (a2 = com.tencent.beacon.a.a.a.a(this.e)) == null || a2.size() <= 0) {
                            bArr = null;
                        } else {
                            this.d = String.valueOf(a2.get(1));
                            bArr = (byte[]) a2.get(3);
                        }
                        if (bArr != null) {
                            bVar2 = a(this.c, this.f2149a, bArr);
                        } else {
                            int e2 = g2.e();
                            if (com.tencent.beacon.d.b.a(this.e)) {
                                e2 /= 2;
                            }
                            if (e2 >= 0) {
                                list = h.a(this.e, (String[]) null, e2);
                            } else {
                                list = null;
                            }
                            if (list == null || list.size() <= 0) {
                                com.tencent.beacon.d.a.h(" no up datas", new Object[0]);
                            } else {
                                int size = list.size();
                                com.tencent.beacon.d.a.h(" size:%d", Integer.valueOf(size));
                                try {
                                    aVar = a(list);
                                } catch (Exception e3) {
                                    b();
                                    aVar = null;
                                }
                                this.f = new Long[size];
                                for (int i2 = 0; i2 < this.f.length; i2++) {
                                    this.f[i2] = Long.valueOf(((i) list.get(i2)).a());
                                }
                                list.clear();
                                if (aVar != null) {
                                    bArr2 = aVar.a();
                                } else {
                                    bArr2 = bArr;
                                }
                                this.h = new byte[bArr2.length];
                                System.arraycopy(bArr2, 0, this.h, 0, bArr2.length);
                                this.d = com.tencent.beacon.b.a.a(this.c, 4);
                                com.tencent.beacon.d.a.a("comm rid:%s", this.d);
                                try {
                                    bVar = a(this.c, this.f2149a, bArr2);
                                } catch (Exception e4) {
                                    b();
                                    bVar = null;
                                }
                                bVar2 = bVar;
                            }
                        }
                    } catch (Throwable th) {
                        th.printStackTrace();
                        com.tencent.beacon.d.a.d(" get req datas error: %s", th.toString());
                    }
                }
            }
        }
        return bVar2;
    }

    private a a(List<i> list) {
        c cVar;
        com.tencent.beacon.c.b.b bVar;
        boolean z = true;
        if (list == null || list.size() <= 0) {
            return null;
        }
        ArrayList<com.tencent.beacon.c.d.b> arrayList = new ArrayList<>(5);
        ArrayList<com.tencent.beacon.c.d.a> arrayList2 = new ArrayList<>(5);
        ArrayList<d> arrayList3 = new ArrayList<>(5);
        ArrayList<com.tencent.beacon.c.b.a> arrayList4 = new ArrayList<>();
        int size = list.size();
        this.g = new ArrayList();
        int i2 = 0;
        while (i2 < size) {
            try {
                i iVar = list.get(i2);
                Map<String, String> e2 = iVar.e();
                com.tencent.beacon.d.a.a(" bean.getTP: " + iVar.b(), new Object[0]);
                if (e2 != null) {
                    if ("IP".equals(iVar.b())) {
                        com.tencent.beacon.c.d.b b = b.b(iVar);
                        if (b != null) {
                            arrayList.add(b);
                        } else {
                            this.g.add(Long.valueOf(iVar.a()));
                        }
                    } else if ("DN".equals(iVar.b())) {
                        com.tencent.beacon.c.d.a c = b.c(iVar);
                        if (c != null) {
                            arrayList2.add(c);
                        } else {
                            this.g.add(Long.valueOf(iVar.a()));
                        }
                    } else if ("HO".equals(iVar.b())) {
                        d d = b.d(iVar);
                        if (d != null) {
                            arrayList3.add(d);
                        } else {
                            this.g.add(Long.valueOf(iVar.a()));
                        }
                    } else if ("UA".equals(iVar.b())) {
                        com.tencent.beacon.d.a.f(" Pack2Upload eventName:}%s ", iVar.d());
                        com.tencent.beacon.c.b.a e3 = b.e(iVar);
                        if (e3 != null) {
                            arrayList4.add(e3);
                        } else {
                            this.g.add(Long.valueOf(iVar.a()));
                        }
                    }
                }
                i2++;
            } catch (Throwable th) {
                th.printStackTrace();
                com.tencent.beacon.d.a.d(" CommonRecordUploadDatas.encode2MixPackage() error1", new Object[0]);
            }
        }
        if (this.g.size() > 0) {
            h.a(this.e, (Long[]) this.g.toArray(new Long[this.g.size()]));
        }
        com.tencent.beacon.d.a.b(" up hmList:" + arrayList3.size(), new Object[0]);
        com.tencent.beacon.d.a.b(" up dmList:" + arrayList2.size(), new Object[0]);
        com.tencent.beacon.d.a.b(" up ipList:" + arrayList.size(), new Object[0]);
        com.tencent.beacon.d.a.b(" up erList:" + arrayList4.size(), new Object[0]);
        try {
            c cVar2 = new c();
            if (arrayList3.size() > 0) {
                cVar2.c = arrayList3;
                z = false;
            }
            if (arrayList2.size() > 0) {
                cVar2.b = arrayList2;
                z = false;
            }
            if (arrayList.size() > 0) {
                cVar2.f2118a = arrayList;
                z = false;
            }
            if (z) {
                cVar = null;
            } else {
                cVar = cVar2;
            }
            if (arrayList4.size() > 0) {
                bVar = new com.tencent.beacon.c.b.b();
                bVar.f2114a = arrayList4;
            } else {
                bVar = null;
            }
            if (cVar == null && bVar == null) {
                return null;
            }
            HashMap hashMap = new HashMap();
            if (cVar != null) {
                hashMap.put(3, cVar.a());
            }
            if (bVar != null) {
                hashMap.put(1, bVar.a());
            }
            a aVar = new a();
            aVar.f2110a = hashMap;
            return aVar;
        } catch (Exception e4) {
            e4.printStackTrace();
            com.tencent.beacon.d.a.d(" CommonRecordUploadDatas.encode2MixPackage() error2", new Object[0]);
            b();
            return null;
        }
    }

    public final synchronized void b() {
        com.tencent.beacon.d.a.c(" encode failed, clear db data", new Object[0]);
        if (this.f != null && this.f.length > 0) {
            com.tencent.beacon.d.a.b(" remove num :" + h.a(this.e, this.f), new Object[0]);
            this.f = null;
        }
    }

    public final synchronized void b(boolean z) {
        if (this.f != null && this.f.length > 0) {
            com.tencent.beacon.d.a.b(" t_event remove num :" + h.a(this.e, this.f), new Object[0]);
        }
        this.f = null;
        if (z && f()) {
            com.tencent.beacon.a.a.a.a(this.e, this.d);
        } else if (!z) {
            if (this.h != null) {
                com.tencent.beacon.d.a.a("comm rid2:%s", this.d);
                com.tencent.beacon.a.a.a.a(this.e, this.h, this.d);
            }
        }
        this.h = null;
    }
}
