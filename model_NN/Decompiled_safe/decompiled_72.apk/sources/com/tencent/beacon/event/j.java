package com.tencent.beacon.event;

import android.content.Context;
import com.tencent.beacon.a.a;
import com.tencent.beacon.a.d;
import com.tencent.beacon.d.b;
import com.tencent.connect.common.Constants;
import java.util.ArrayList;
import java.util.HashMap;

/* compiled from: ProGuard */
public final class j {

    /* renamed from: a  reason: collision with root package name */
    private i f2139a;
    private Context b;
    private int c = 20000;
    private int d = 0;
    private Runnable e = new k(this);

    public j(Context context) {
        this.b = context;
        HashMap hashMap = new HashMap(2);
        if (a.g(this.b)) {
            hashMap.put("A66", "F");
        } else {
            hashMap.put("A66", "B");
        }
        hashMap.put("A68", new StringBuilder().append(a.h(this.b)).toString());
        this.f2139a = b.a(this.b, "rqd_heartbeat", true, 0, 0, hashMap);
    }

    public j(Context context, boolean z) {
        this.b = context;
        HashMap hashMap = new HashMap(2);
        if (z) {
            hashMap.put("A66", "F");
        } else {
            hashMap.put("A66", "B");
        }
        hashMap.put("A68", new StringBuilder().append(a.h(this.b)).toString());
        this.f2139a = b.a(this.b, "rqd_heartbeat", true, 0, 0, hashMap);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        if (b.b(this.b)) {
            com.tencent.beacon.f.j f = t.d().f();
            if (f != null) {
                ArrayList arrayList = new ArrayList(2);
                arrayList.add(this.f2139a);
                f.a(new o(this.b, arrayList));
            }
            a(b() + 1);
            if (b() % 10 == 0) {
                d.a().a(108, this.e, 600000, (long) this.c);
                a(0);
            }
        }
    }

    public final void a(boolean z) {
        int i;
        if (com.tencent.beacon.b.a.g().equals(a.b(this.b, "HEART_DENGTA", Constants.STR_EMPTY))) {
            com.tencent.beacon.d.a.a("heartbeat has been uploaded today!", new Object[0]);
            return;
        }
        if (z) {
            i = ((int) (Math.random() * 60.0d)) * 1000;
        } else {
            i = 0;
        }
        d.a().a(108, this.e, (long) i, (long) this.c);
    }

    private synchronized int b() {
        return this.d;
    }

    private synchronized void a(int i) {
        this.d = i;
    }
}
