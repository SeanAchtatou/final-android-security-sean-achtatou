package com.tencent.beacon.a.a;

import android.annotation.TargetApi;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.tencent.beacon.a.h;
import com.tencent.beacon.d.a;
import java.io.File;

/* compiled from: ProGuard */
public final class c extends SQLiteOpenHelper {
    private static c b = null;

    /* renamed from: a  reason: collision with root package name */
    private Context f2085a;

    private c(Context context) {
        super(context, "beacon_db", (SQLiteDatabase.CursorFactory) null, 15);
        this.f2085a = context;
    }

    public static synchronized c a(Context context) {
        c cVar;
        synchronized (c.class) {
            cVar = new c(context);
        }
        return cVar;
    }

    public final synchronized void onCreate(SQLiteDatabase sQLiteDatabase) {
        synchronized (this) {
            if (sQLiteDatabase != null) {
                if (b.f2084a != null) {
                    for (String[] strArr : b.f2084a) {
                        a.g("table:%s", strArr[0]);
                        sQLiteDatabase.execSQL(strArr[1]);
                    }
                }
            }
        }
    }

    public final synchronized void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        a.g("upgrade a db  [%s] from v %d to v %d , deleted all tables!", "beacon_db", Integer.valueOf(i), Integer.valueOf(i2));
        if (a(sQLiteDatabase)) {
            a.g("drop all success recreate!", new Object[0]);
            onCreate(sQLiteDatabase);
        } else {
            a.d("drop all fail try deleted file,may next time will success!", new Object[0]);
            File databasePath = this.f2085a.getDatabasePath("beacon_db");
            if (databasePath != null && databasePath.canWrite()) {
                databasePath.delete();
            }
        }
    }

    @TargetApi(11)
    public final synchronized void onDowngrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        h.a(this.f2085a);
        if (Integer.parseInt(h.c()) >= 11) {
            a.g("downgrade a db  [%s] from v %d to  v%d , deleted all tables!", "beacon_db", Integer.valueOf(i), Integer.valueOf(i2));
            if (a(sQLiteDatabase)) {
                a.g("drop all success recreate!", new Object[0]);
                onCreate(sQLiteDatabase);
            } else {
                a.d("drop all fail try deleted file,may next time will success!", new Object[0]);
                File databasePath = this.f2085a.getDatabasePath("beacon_db");
                if (databasePath != null && databasePath.canWrite()) {
                    databasePath.delete();
                }
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:30:0x0077 A[SYNTHETIC, Splitter:B:30:0x0077] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized boolean a(android.database.sqlite.SQLiteDatabase r13) {
        /*
            r12 = this;
            r8 = 1
            r10 = 0
            r9 = 0
            monitor-enter(r12)
            java.util.ArrayList r11 = new java.util.ArrayList     // Catch:{ Throwable -> 0x0094, all -> 0x0091 }
            r11.<init>()     // Catch:{ Throwable -> 0x0094, all -> 0x0091 }
            java.lang.String r1 = "sqlite_master"
            r0 = 1
            java.lang.String[] r2 = new java.lang.String[r0]     // Catch:{ Throwable -> 0x0094, all -> 0x0091 }
            r0 = 0
            java.lang.String r3 = "name"
            r2[r0] = r3     // Catch:{ Throwable -> 0x0094, all -> 0x0091 }
            java.lang.String r3 = "type = 'table'"
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            r0 = r13
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ Throwable -> 0x0094, all -> 0x0091 }
            if (r1 == 0) goto L_0x0041
        L_0x0020:
            boolean r0 = r1.moveToNext()     // Catch:{ Throwable -> 0x002f }
            if (r0 == 0) goto L_0x0041
            r0 = 0
            java.lang.String r0 = r1.getString(r0)     // Catch:{ Throwable -> 0x002f }
            r11.add(r0)     // Catch:{ Throwable -> 0x002f }
            goto L_0x0020
        L_0x002f:
            r0 = move-exception
        L_0x0030:
            r0.printStackTrace()     // Catch:{ all -> 0x0074 }
            if (r1 == 0) goto L_0x003e
            boolean r0 = r1.isClosed()     // Catch:{ all -> 0x0081 }
            if (r0 != 0) goto L_0x003e
            r1.close()     // Catch:{ all -> 0x0081 }
        L_0x003e:
            r0 = r9
        L_0x003f:
            monitor-exit(r12)
            return r0
        L_0x0041:
            java.lang.String r2 = "drop table if exists %s"
            int r0 = r11.size()     // Catch:{ Throwable -> 0x002f }
            if (r0 <= 0) goto L_0x0084
            java.util.Iterator r3 = r11.iterator()     // Catch:{ Throwable -> 0x002f }
        L_0x004d:
            boolean r0 = r3.hasNext()     // Catch:{ Throwable -> 0x002f }
            if (r0 == 0) goto L_0x0084
            java.lang.Object r0 = r3.next()     // Catch:{ Throwable -> 0x002f }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Throwable -> 0x002f }
            java.util.Locale r4 = java.util.Locale.US     // Catch:{ Throwable -> 0x002f }
            r5 = 1
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ Throwable -> 0x002f }
            r6 = 0
            r5[r6] = r0     // Catch:{ Throwable -> 0x002f }
            java.lang.String r4 = java.lang.String.format(r4, r2, r5)     // Catch:{ Throwable -> 0x002f }
            r13.execSQL(r4)     // Catch:{ Throwable -> 0x002f }
            java.lang.String r4 = "drop %s"
            r5 = 1
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ Throwable -> 0x002f }
            r6 = 0
            r5[r6] = r0     // Catch:{ Throwable -> 0x002f }
            com.tencent.beacon.d.a.g(r4, r5)     // Catch:{ Throwable -> 0x002f }
            goto L_0x004d
        L_0x0074:
            r0 = move-exception
        L_0x0075:
            if (r1 == 0) goto L_0x0080
            boolean r2 = r1.isClosed()     // Catch:{ all -> 0x0081 }
            if (r2 != 0) goto L_0x0080
            r1.close()     // Catch:{ all -> 0x0081 }
        L_0x0080:
            throw r0     // Catch:{ all -> 0x0081 }
        L_0x0081:
            r0 = move-exception
            monitor-exit(r12)
            throw r0
        L_0x0084:
            if (r1 == 0) goto L_0x008f
            boolean r0 = r1.isClosed()     // Catch:{ all -> 0x0081 }
            if (r0 != 0) goto L_0x008f
            r1.close()     // Catch:{ all -> 0x0081 }
        L_0x008f:
            r0 = r8
            goto L_0x003f
        L_0x0091:
            r0 = move-exception
            r1 = r10
            goto L_0x0075
        L_0x0094:
            r0 = move-exception
            r1 = r10
            goto L_0x0030
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.beacon.a.a.c.a(android.database.sqlite.SQLiteDatabase):boolean");
    }

    public final synchronized SQLiteDatabase getReadableDatabase() {
        SQLiteDatabase sQLiteDatabase;
        int i = 0;
        synchronized (this) {
            sQLiteDatabase = null;
            while (sQLiteDatabase == null && i < 5) {
                i++;
                try {
                    sQLiteDatabase = super.getReadableDatabase();
                } catch (Throwable th) {
                    a.c("getReadableDatabase error count  %d", Integer.valueOf(i));
                    if (i == 5) {
                        a.d("error get DB failed", new Object[0]);
                    }
                    try {
                        Thread.sleep(200);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return sQLiteDatabase;
    }

    public final synchronized SQLiteDatabase getWritableDatabase() {
        SQLiteDatabase sQLiteDatabase;
        int i = 0;
        synchronized (this) {
            sQLiteDatabase = null;
            while (sQLiteDatabase == null && i < 5) {
                i++;
                try {
                    sQLiteDatabase = super.getWritableDatabase();
                } catch (Exception e) {
                    a.c("getWritableDatabase error count %d", Integer.valueOf(i));
                    if (i == 5) {
                        a.d("error get DB failed", new Object[0]);
                    }
                    try {
                        Thread.sleep(200);
                    } catch (InterruptedException e2) {
                        e2.printStackTrace();
                    }
                }
            }
        }
        return sQLiteDatabase;
    }
}
