package com.tencent.beacon.a.a;

import java.util.Locale;

/* compiled from: ProGuard */
public final class b {

    /* renamed from: a  reason: collision with root package name */
    public static final String[][] f2084a = new String[5][];

    static {
        f2084a[0] = new String[]{"t_event", String.format(Locale.US, "CREATE TABLE %s ( %s INTEGER PRIMARY KEY AUTOINCREMENT, %s int , %s int , %s int , %s int , %s blob)", "t_event", "_id", "_time", "_type", "_prority", "_length", "_datas")};
        f2084a[1] = new String[]{"t_count_event", String.format(Locale.US, "CREATE TABLE %s ( %s INTEGER PRIMARY KEY AUTOINCREMENT, %s varchar(255) unique  , %s int , %s int , %s int , %s int , %s int , %s text)", "t_count_event", "_id", "_countid", "_prority", "_local", "_stime", "_utime", "_ctime", "_cparams")};
        f2084a[2] = new String[]{"t_strategy", String.format(Locale.US, "CREATE TABLE %s ( %s INTEGER PRIMARY KEY AUTOINCREMENT, %s int unique , %s int , %s blob)", "t_strategy", "_id", "_key", "_ut", "_datas")};
        f2084a[3] = new String[]{"t_file", String.format(Locale.US, "CREATE TABLE %s ( %s INTEGER PRIMARY KEY AUTOINCREMENT, %s text , %s int , %s int , %s text , %s text , %s text , %s text)", "t_file", "_id", "_n", "_ut", "_sz", "_ac", "_sa", "_t", "_p")};
        f2084a[4] = new String[]{"t_req_data", String.format(Locale.US, "CREATE TABLE %s ( %s INTEGER PRIMARY KEY AUTOINCREMENT, %s text unique , %s int , %s blob)", "t_req_data", "_id", "_rid", "_time", "_datas")};
    }
}
