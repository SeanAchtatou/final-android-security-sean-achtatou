package com.tencent.beacon.c.d;

import com.tencent.beacon.e.c;
import com.tencent.beacon.e.d;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
public final class a extends c implements Cloneable {
    private static byte[] p;

    /* renamed from: a  reason: collision with root package name */
    public String f2116a = Constants.STR_EMPTY;
    public String b = Constants.STR_EMPTY;
    public String c = Constants.STR_EMPTY;
    public long d = 0;
    public long e = 0;
    public long f = 0;
    public long g = 0;
    public long h = 0;
    public String i = Constants.STR_EMPTY;
    public String j = Constants.STR_EMPTY;
    public long k = 0;
    public String l = Constants.STR_EMPTY;
    private int m = 0;
    private int n = 0;
    private byte[] o = null;

    public final void a(d dVar) {
        dVar.a(this.f2116a, 0);
        dVar.a(this.b, 1);
        dVar.a(this.c, 2);
        dVar.a(this.d, 3);
        dVar.a(this.e, 4);
        dVar.a(this.f, 5);
        dVar.a(this.g, 6);
        dVar.a(this.h, 7);
        if (this.i != null) {
            dVar.a(this.i, 8);
        }
        if (this.j != null) {
            dVar.a(this.j, 9);
        }
        dVar.a(this.k, 10);
        dVar.a(this.m, 11);
        dVar.a(this.n, 12);
        if (this.o != null) {
            dVar.a(this.o, 13);
        }
        if (this.l != null) {
            dVar.a(this.l, 14);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.beacon.e.a.a(long, int, boolean):long
     arg types: [long, int, int]
     candidates:
      com.tencent.beacon.e.a.a(double, int, boolean):double
      com.tencent.beacon.e.a.a(float, int, boolean):float
      com.tencent.beacon.e.a.a(byte, int, boolean):byte
      com.tencent.beacon.e.a.a(int, int, boolean):int
      com.tencent.beacon.e.a.a(com.tencent.beacon.e.c, int, boolean):com.tencent.beacon.e.c
      com.tencent.beacon.e.a.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.beacon.e.a.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.beacon.e.a.a(short, int, boolean):short
      com.tencent.beacon.e.a.a(long, int, boolean):long */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.beacon.e.a.a(int, int, boolean):int
     arg types: [int, int, int]
     candidates:
      com.tencent.beacon.e.a.a(double, int, boolean):double
      com.tencent.beacon.e.a.a(float, int, boolean):float
      com.tencent.beacon.e.a.a(byte, int, boolean):byte
      com.tencent.beacon.e.a.a(long, int, boolean):long
      com.tencent.beacon.e.a.a(com.tencent.beacon.e.c, int, boolean):com.tencent.beacon.e.c
      com.tencent.beacon.e.a.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.beacon.e.a.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.beacon.e.a.a(short, int, boolean):short
      com.tencent.beacon.e.a.a(int, int, boolean):int */
    public final void a(com.tencent.beacon.e.a aVar) {
        this.f2116a = aVar.b(0, true);
        this.b = aVar.b(1, true);
        this.c = aVar.b(2, true);
        this.d = aVar.a(this.d, 3, true);
        this.e = aVar.a(this.e, 4, true);
        this.f = aVar.a(this.f, 5, true);
        this.g = aVar.a(this.g, 6, true);
        this.h = aVar.a(this.h, 7, true);
        this.i = aVar.b(8, false);
        this.j = aVar.b(9, false);
        this.k = aVar.a(this.k, 10, true);
        this.m = aVar.a(this.m, 11, false);
        this.n = aVar.a(this.n, 12, false);
        if (p == null) {
            byte[] bArr = new byte[1];
            p = bArr;
            bArr[0] = 0;
        }
        byte[] bArr2 = p;
        this.o = aVar.c(13, false);
        this.l = aVar.b(14, false);
    }
}
