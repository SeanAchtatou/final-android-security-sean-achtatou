package com.tencent.beacon.e;

import com.tencent.connect.common.Constants;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/* compiled from: ProGuard */
public final class f {
    private static HashMap<String, byte[]> e = null;

    /* renamed from: a  reason: collision with root package name */
    private HashMap<String, byte[]> f2131a = new HashMap<>();
    private a b = new a();
    private String c = "GBK";
    private e d = new e();

    public final void a(int i) {
        this.d.b = 1;
    }

    public final void a(String str) {
        this.d.d = str;
    }

    public final void b(String str) {
        this.d.c = str;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.beacon.e.d.a(java.lang.Object, int):void
     arg types: [T, int]
     candidates:
      com.tencent.beacon.e.d.a(byte, int):void
      com.tencent.beacon.e.d.a(int, int):void
      com.tencent.beacon.e.d.a(long, int):void
      com.tencent.beacon.e.d.a(com.tencent.beacon.e.c, int):void
      com.tencent.beacon.e.d.a(java.lang.String, int):void
      com.tencent.beacon.e.d.a(java.util.Collection, int):void
      com.tencent.beacon.e.d.a(java.util.Map, int):void
      com.tencent.beacon.e.d.a(short, int):void
      com.tencent.beacon.e.d.a(boolean, int):void
      com.tencent.beacon.e.d.a(byte[], int):void
      com.tencent.beacon.e.d.a(java.lang.Object, int):void */
    public final <T> void a(String str, T t) {
        if (str == null) {
            throw new IllegalArgumentException("put key can not is null");
        } else if (t == null) {
            throw new IllegalArgumentException("put value can not is null");
        } else if (t instanceof Set) {
            throw new IllegalArgumentException("can not support Set");
        } else {
            d dVar = new d();
            dVar.a(this.c);
            dVar.a((Object) t, 0);
            ByteBuffer a2 = dVar.a();
            byte[] bArr = new byte[a2.position()];
            System.arraycopy(a2.array(), 0, bArr, 0, bArr.length);
            this.f2131a.put(str, bArr);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.beacon.e.d.a(java.util.Map, int):void
     arg types: [java.util.HashMap<java.lang.String, byte[]>, int]
     candidates:
      com.tencent.beacon.e.d.a(byte, int):void
      com.tencent.beacon.e.d.a(int, int):void
      com.tencent.beacon.e.d.a(long, int):void
      com.tencent.beacon.e.d.a(com.tencent.beacon.e.c, int):void
      com.tencent.beacon.e.d.a(java.lang.Object, int):void
      com.tencent.beacon.e.d.a(java.lang.String, int):void
      com.tencent.beacon.e.d.a(java.util.Collection, int):void
      com.tencent.beacon.e.d.a(short, int):void
      com.tencent.beacon.e.d.a(boolean, int):void
      com.tencent.beacon.e.d.a(byte[], int):void
      com.tencent.beacon.e.d.a(java.util.Map, int):void */
    public final byte[] a() {
        d dVar = new d(0);
        dVar.a(this.c);
        dVar.a((Map) this.f2131a, 0);
        this.d.f2130a = 3;
        e eVar = this.d;
        ByteBuffer a2 = dVar.a();
        byte[] bArr = new byte[a2.position()];
        System.arraycopy(a2.array(), 0, bArr, 0, bArr.length);
        eVar.e = bArr;
        d dVar2 = new d(0);
        dVar2.a(this.c);
        this.d.a(dVar2);
        ByteBuffer a3 = dVar2.a();
        byte[] bArr2 = new byte[a3.position()];
        System.arraycopy(a3.array(), 0, bArr2, 0, bArr2.length);
        int length = bArr2.length;
        ByteBuffer allocate = ByteBuffer.allocate(length + 4);
        allocate.putInt(length + 4).put(bArr2).flip();
        return allocate.array();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.beacon.e.a.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
     arg types: [java.util.HashMap<java.lang.String, byte[]>, int, int]
     candidates:
      com.tencent.beacon.e.a.a(double, int, boolean):double
      com.tencent.beacon.e.a.a(float, int, boolean):float
      com.tencent.beacon.e.a.a(byte, int, boolean):byte
      com.tencent.beacon.e.a.a(int, int, boolean):int
      com.tencent.beacon.e.a.a(long, int, boolean):long
      com.tencent.beacon.e.a.a(com.tencent.beacon.e.c, int, boolean):com.tencent.beacon.e.c
      com.tencent.beacon.e.a.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.beacon.e.a.a(short, int, boolean):short
      com.tencent.beacon.e.a.a(java.util.Map, int, boolean):java.util.HashMap<K, V> */
    public final void a(byte[] bArr) {
        if (bArr.length < 4) {
            throw new IllegalArgumentException("decode package must include size head");
        }
        try {
            a aVar = new a(bArr, 4);
            aVar.a(this.c);
            this.d.a(aVar);
            a aVar2 = new a(this.d.e);
            aVar2.a(this.c);
            if (e == null) {
                HashMap<String, byte[]> hashMap = new HashMap<>();
                e = hashMap;
                hashMap.put(Constants.STR_EMPTY, new byte[0]);
            }
            this.f2131a = aVar2.a((Map) e, 0, false);
        } catch (Exception e2) {
            throw new RuntimeException(e2);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.beacon.e.a.a(java.lang.Object, int, boolean):java.lang.Object
     arg types: [T, int, int]
     candidates:
      com.tencent.beacon.e.a.a(double, int, boolean):double
      com.tencent.beacon.e.a.a(float, int, boolean):float
      com.tencent.beacon.e.a.a(byte, int, boolean):byte
      com.tencent.beacon.e.a.a(int, int, boolean):int
      com.tencent.beacon.e.a.a(long, int, boolean):long
      com.tencent.beacon.e.a.a(com.tencent.beacon.e.c, int, boolean):com.tencent.beacon.e.c
      com.tencent.beacon.e.a.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.beacon.e.a.a(short, int, boolean):short
      com.tencent.beacon.e.a.a(java.lang.Object, int, boolean):java.lang.Object */
    public final <T> T b(String str, T t) {
        if (!this.f2131a.containsKey(str)) {
            return null;
        }
        try {
            this.b.a(this.f2131a.get(str));
            this.b.a(this.c);
            return this.b.a((Object) t, 0, true);
        } catch (Exception e2) {
            throw new Exception(e2);
        }
    }
}
