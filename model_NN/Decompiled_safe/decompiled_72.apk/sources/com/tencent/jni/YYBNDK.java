package com.tencent.jni;

/* compiled from: ProGuard */
public class YYBNDK {
    public native boolean createDir(String str, int i);

    public native boolean createFile(String str, int i);

    public native int findInMount(String str);

    public native long getDirSize(String str);

    public native int getFilePermission(String str);

    public native long getFileSize(String str);

    public native int getLastAccessTime(String str);

    public native String getLinkedFile(String str);

    public native String getRealLinkFile(String str);

    public native int getUid(String str);

    public native boolean isLink(String str);

    public native boolean setFilePermission(String str, int i);

    static {
        boolean z;
        try {
            System.loadLibrary("qqndkfile");
            z = true;
        } catch (Throwable th) {
            th.printStackTrace();
            z = false;
        }
        if (!z) {
            try {
                System.load("/data/data/com.tencent.android.qqdownloader/lib/libqqndkfile.so");
            } catch (Throwable th2) {
            }
        }
    }
}
