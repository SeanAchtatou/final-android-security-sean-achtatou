package com.tencent.pangu.b.a;

import android.text.TextUtils;
import com.qq.AppService.AstApp;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.connect.common.Constants;
import com.tencent.downloadsdk.DownloadManager;
import com.tencent.pangu.download.DownloadInfo;
import java.io.File;

/* compiled from: ProGuard */
public class a {
    public static int a(DownloadInfo downloadInfo) {
        if (downloadInfo == null || downloadInfo.ignoreState()) {
            return -1;
        }
        LocalApkInfo localApkInfo = ApkResourceManager.getInstance().getLocalApkInfo(downloadInfo.packageName, downloadInfo.versionCode, downloadInfo.grayVersionCode);
        if (localApkInfo != null && c(localApkInfo.mLocalFilePath)) {
            return 4;
        }
        switch (b.f3469a[downloadInfo.downloadState.ordinal()]) {
            case 1:
                if (!DownloadManager.a().c(downloadInfo.getDownloadSubType(), downloadInfo.downloadTicket)) {
                    return 2;
                }
                return 1;
            case 2:
                return 6;
            case 3:
                return 3;
            case 4:
            case 5:
                return 2;
            case 6:
                return 9;
            case 7:
                if (!new File(downloadInfo.getFilePath()).exists()) {
                    return 9;
                }
                return 4;
            case 8:
                return 8;
            default:
                return -1;
        }
    }

    private static boolean c(String str) {
        if (new File(str).exists()) {
            try {
                AstApp.i().getPackageManager().getPackageArchiveInfo(str, 0);
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            } catch (OutOfMemoryError e2) {
                return false;
            }
        }
        return true;
    }

    public static String a(String str) {
        if (TextUtils.isEmpty(str)) {
            return Constants.STR_EMPTY;
        }
        int indexOf = str.indexOf("&");
        if (indexOf != -1) {
            return str.substring(0, indexOf);
        }
        return str;
    }

    public static String b(String str) {
        int indexOf;
        String substring;
        int indexOf2;
        if (TextUtils.isEmpty(str) || (indexOf = str.indexOf("&")) == -1 || (indexOf2 = (substring = str.substring(indexOf + 1, str.length())).indexOf("=")) == -1) {
            return Constants.STR_EMPTY;
        }
        return substring.substring(indexOf2 + 1, substring.length());
    }
}
