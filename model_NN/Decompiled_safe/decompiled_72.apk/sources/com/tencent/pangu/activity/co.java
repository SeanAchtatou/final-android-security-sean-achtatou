package com.tencent.pangu.activity;

import com.tencent.assistant.utils.TemporaryThreadManager;

/* compiled from: ProGuard */
class co implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Runnable f3366a;
    final /* synthetic */ StartPopWindowActivity b;

    co(StartPopWindowActivity startPopWindowActivity, Runnable runnable) {
        this.b = startPopWindowActivity;
        this.f3366a = runnable;
    }

    public void run() {
        TemporaryThreadManager.get().start(this.f3366a);
    }
}
