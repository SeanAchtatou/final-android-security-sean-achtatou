package com.tencent.pangu.activity;

import com.tencent.assistant.AppConst;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;

/* compiled from: ProGuard */
class ac extends AppConst.TwoBtnDialogInfo {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppTreasureBoxActivity f3304a;

    ac(AppTreasureBoxActivity appTreasureBoxActivity) {
        this.f3304a = appTreasureBoxActivity;
    }

    public void onLeftBtnClick() {
        if (this.f3304a.y != null) {
            this.f3304a.y.setVisibility(0);
        }
        if (!this.f3304a.isFinishing()) {
            this.f3304a.A.show();
        }
        STInfoV2 b = this.f3304a.b(STConst.ST_PAGE_APP_TREASURE_BOX_EXIT_DIALOG, 200);
        b.slotId = a.a("03", "002");
        l.a(b);
    }

    public void onRightBtnClick() {
        this.f3304a.w();
        STInfoV2 b = this.f3304a.b(STConst.ST_PAGE_APP_TREASURE_BOX_EXIT_DIALOG, 200);
        b.slotId = a.a("03", "001");
        l.a(b);
    }

    public void onCancell() {
        if (this.f3304a.y != null) {
            this.f3304a.y.setVisibility(0);
        }
        if (!this.f3304a.isFinishing()) {
            this.f3304a.A.show();
        }
        STInfoV2 b = this.f3304a.b(STConst.ST_PAGE_APP_TREASURE_BOX_EXIT_DIALOG, 200);
        b.slotId = a.a("03", "002");
        l.a(b);
    }
}
