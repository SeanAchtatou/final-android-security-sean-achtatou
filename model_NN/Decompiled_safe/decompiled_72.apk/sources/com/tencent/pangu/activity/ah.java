package com.tencent.pangu.activity;

import com.tencent.assistant.AppConst;
import com.tencent.assistant.module.k;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import com.tencent.pangu.download.DownloadInfo;

/* compiled from: ProGuard */
class ah implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ STInfoV2 f3309a;
    final /* synthetic */ boolean b;
    final /* synthetic */ DownloadInfo c;
    final /* synthetic */ DownloadActivity d;

    ah(DownloadActivity downloadActivity, STInfoV2 sTInfoV2, boolean z, DownloadInfo downloadInfo) {
        this.d = downloadActivity;
        this.f3309a = sTInfoV2;
        this.b = z;
        this.c = downloadInfo;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistantv2.st.page.a.a(com.tencent.assistant.AppConst$AppState, boolean):java.lang.String
     arg types: [com.tencent.assistant.AppConst$AppState, int]
     candidates:
      com.tencent.assistantv2.st.page.a.a(int, java.lang.String):com.tencent.assistantv2.st.page.STPageInfo
      com.tencent.assistantv2.st.page.a.a(android.content.Context, java.lang.String):com.tencent.assistantv2.st.page.STPageInfo
      com.tencent.assistantv2.st.page.a.a(com.tencent.assistant.AppConst$AppState, com.tencent.assistant.model.SimpleAppModel):java.lang.String
      com.tencent.assistantv2.st.page.a.a(com.tencent.assistantv2.st.model.STCommonInfo$ContentIdType, java.lang.String):java.lang.String
      com.tencent.assistantv2.st.page.a.a(java.lang.String, int):java.lang.String
      com.tencent.assistantv2.st.page.a.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.assistantv2.st.page.a.a(com.tencent.assistant.AppConst$AppState, boolean):java.lang.String */
    public void run() {
        if (this.f3309a != null) {
            STInfoV2 sTInfoV2 = this.f3309a;
            if (this.b) {
                sTInfoV2.actionId = 900;
                sTInfoV2.status = STConst.ST_STATUS_DEFAULT;
            } else {
                AppConst.AppState b2 = k.b(this.c);
                sTInfoV2.actionId = a.a(b2);
                sTInfoV2.status = a.a(b2, false);
            }
            sTInfoV2.isImmediately = true;
            l.a(sTInfoV2);
        }
        if (this.c != null) {
            com.tencent.pangu.download.a.a().a(this.c);
        }
    }
}
