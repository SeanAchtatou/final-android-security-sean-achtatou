package com.tencent.pangu.activity;

import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;

/* compiled from: ProGuard */
class cp extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ StartPopWindowActivity f3367a;

    cp(StartPopWindowActivity startPopWindowActivity) {
        this.f3367a = startPopWindowActivity;
    }

    public void onTMAClick(View view) {
        this.f3367a.E();
    }

    public STInfoV2 getStInfo() {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f3367a.K, 200);
        buildSTInfo.slotId = a.a("06", "000");
        buildSTInfo.status = "01";
        return buildSTInfo;
    }
}
