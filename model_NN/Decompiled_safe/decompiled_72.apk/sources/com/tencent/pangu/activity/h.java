package com.tencent.pangu.activity;

import android.graphics.drawable.Drawable;
import com.tencent.pangu.component.appdetail.process.AppdetailActionUIListener;

/* compiled from: ProGuard */
class h implements AppdetailActionUIListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppDetailActivityV5 f3382a;

    h(AppDetailActivityV5 appDetailActivityV5) {
        this.f3382a = appDetailActivityV5;
    }

    public void a(String str, int i) {
        if (this.f3382a.aT != null) {
            this.f3382a.aT.a(str, i);
        }
    }

    public void a(String str, String str2, String str3) {
        if (this.f3382a.aT != null) {
            this.f3382a.aT.a(str, str2, str3);
        }
    }

    public void a(int i) {
        if (this.f3382a.aT != null) {
            this.f3382a.aT.a(i);
        }
    }

    public void b(int i) {
        if (this.f3382a.aT != null) {
            this.f3382a.aT.b(i);
        }
    }

    public void c(int i) {
        if (this.f3382a.aT != null) {
            this.f3382a.aT.c(i);
        }
    }

    public void a(Drawable drawable) {
        if (this.f3382a.aT != null) {
            this.f3382a.aT.a(drawable);
        }
    }

    public void a(int i, int i2) {
        if (this.f3382a.aT != null) {
            this.f3382a.aT.a(i, i2);
        }
    }

    public void a(boolean z) {
        if (this.f3382a.aT != null) {
            this.f3382a.aT.a(z);
        }
    }

    public void b(boolean z) {
        if (this.f3382a.aT != null) {
            this.f3382a.aT.b(z);
        }
    }

    public void c(boolean z) {
        if (this.f3382a.aT != null) {
            this.f3382a.aT.c(z);
        }
    }

    public void a(String str) {
        this.f3382a.aU.setText(str);
        this.f3382a.aU.setVisibility(0);
    }

    public void a(boolean z, AppdetailActionUIListener.AuthType authType) {
        if (this.f3382a.aT != null) {
            this.f3382a.aT.a(z, authType);
        }
    }

    public void a(String str, String str2) {
        this.f3382a.aT.a(str, str2);
    }
}
