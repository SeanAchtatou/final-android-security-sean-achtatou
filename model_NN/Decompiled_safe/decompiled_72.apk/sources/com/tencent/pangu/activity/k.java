package com.tencent.pangu.activity;

import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.model.c;
import com.tencent.assistant.module.update.j;
import com.tencent.assistant.protocol.jce.AppUpdateInfo;
import com.tencent.pangu.module.d;

/* compiled from: ProGuard */
class k implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppDetailActivityV5 f3385a;

    k(AppDetailActivityV5 appDetailActivityV5) {
        this.f3385a = appDetailActivityV5;
    }

    public void run() {
        d a2;
        AppUpdateInfo a3 = j.b().a(this.f3385a.ac.c);
        if (a3 == null || a3.d != this.f3385a.ac.g) {
            LocalApkInfo unused = this.f3385a.aQ = ApkResourceManager.getInstance().getInstalledApkInfo(this.f3385a.ac.c, true);
            a2 = this.f3385a.Z.a(this.f3385a.ac, this.f3385a.aQ, this.f3385a.aL, this.f3385a.aN, this.f3385a.aO, this.f3385a.aP);
        } else {
            a2 = this.f3385a.Z.a(this.f3385a.ac, this.f3385a.aL, this.f3385a.aN, this.f3385a.aO, this.f3385a.aP);
        }
        if (a2 != null) {
            c unused2 = this.f3385a.ae = a2.f3933a;
            this.f3385a.w[3] = a2.b;
        }
    }
}
