package com.tencent.pangu.activity;

import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import com.tencent.pangu.link.b;

/* compiled from: ProGuard */
class cu extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ StartPopWindowActivity f3372a;

    cu(StartPopWindowActivity startPopWindowActivity) {
        this.f3372a = startPopWindowActivity;
    }

    public void onTMAClick(View view) {
        b.b(this.f3372a.K, "tmast://necessity?selflink=1");
    }

    public STInfoV2 getStInfo() {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f3372a.K, 200);
        buildSTInfo.slotId = a.a("05_", "001");
        return buildSTInfo;
    }
}
