package com.tencent.pangu.activity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.component.NormalErrorRecommendPage;
import com.tencent.assistant.component.dialog.DialogUtils;
import com.tencent.assistant.component.invalidater.ListViewScrollListener;
import com.tencent.assistant.component.txscrollview.TXExpandableListView;
import com.tencent.assistant.component.txscrollview.TXScrollViewBase;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.m;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.ad;
import com.tencent.assistant.module.callback.h;
import com.tencent.assistant.module.k;
import com.tencent.assistant.net.c;
import com.tencent.assistant.protocol.jce.AppSimpleDetail;
import com.tencent.assistant.protocol.jce.InstalledAppItem;
import com.tencent.assistant.protocol.jce.UserTaskCfg;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.st.f;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.ah;
import com.tencent.assistant.utils.bm;
import com.tencent.assistantv2.component.SecondNavigationTitleViewV5;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.model.StatInfo;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import com.tencent.connect.common.Constants;
import com.tencent.pangu.adapter.DownloadInfoMultiAdapter;
import com.tencent.pangu.component.DownloadListFooterView;
import com.tencent.pangu.component.br;
import com.tencent.pangu.download.DownloadInfo;
import com.tencent.pangu.download.SimpleDownloadInfo;
import com.tencent.pangu.manager.DownloadProxy;
import com.tencent.pangu.mediadownload.e;
import com.tencent.pangu.model.d;
import com.tencent.pangu.module.a.p;
import com.tencent.pangu.module.bh;

@SuppressLint({"NewApi"})
/* compiled from: ProGuard */
public class DownloadActivity extends BaseActivity implements UIEventListener, h, p {
    private static int G = 2;
    /* access modifiers changed from: private */
    public View A;
    private TextView B;
    private TextView C;
    private Button D;
    private int E;
    /* access modifiers changed from: private */
    public int F = -1;
    /* access modifiers changed from: private */
    public SimpleAppModel H;
    private String I;
    private boolean J = false;
    private boolean K = false;
    private boolean L = false;
    private int M = 0;
    private String N = Constants.STR_EMPTY;
    private final int O = 1;
    /* access modifiers changed from: private */
    public com.tencent.pangu.mediadownload.p P;
    private Dialog Q = null;
    private String R;
    private final int S = 2;
    private NormalErrorRecommendPage T;
    /* access modifiers changed from: private */
    public long U = 0;
    private boolean V = false;
    private boolean W = false;
    private volatile boolean X = false;
    /* access modifiers changed from: private */
    public volatile boolean Y = false;
    /* access modifiers changed from: private */
    public Boolean Z = true;
    /* access modifiers changed from: private */
    public UserTaskCfg aa;
    private Boolean ab = false;
    private String ac = "11_001";
    private boolean ad = false;
    private boolean ae = false;
    private ListViewScrollListener af = new aj(this);
    /* access modifiers changed from: private */
    public Handler ag = new ak(this);
    /* access modifiers changed from: private */
    public Context n;
    private RelativeLayout u;
    private SecondNavigationTitleViewV5 v;
    /* access modifiers changed from: private */
    public TXExpandableListView w;
    /* access modifiers changed from: private */
    public DownloadInfoMultiAdapter x;
    private ad y = new ad();
    private bh z = new bh();

    public void a(Boolean bool) {
        this.ab = bool;
    }

    public int f() {
        if (this.ab.booleanValue()) {
            return STConst.ST_PAGE_DOWNLOAD_USERTASK;
        }
        return STConst.ST_PAGE_DOWNLOAD;
    }

    public int m() {
        if (this.P == null) {
            return super.m();
        }
        if (this.P.d() == 2) {
            return STConst.ST_PAGE_DOWNLOAD_FROM_OUTTER_TMAST;
        }
        return STConst.ST_PAGE_DOWNLOAD_FROM_OUTTER;
    }

    public boolean g() {
        return false;
    }

    private STInfoV2 a(SimpleAppModel simpleAppModel) {
        AppConst.AppState d = k.d(simpleAppModel);
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this, simpleAppModel, STConst.ST_DEFAULT_SLOT, a.a(d), a.a(d, simpleAppModel));
        if (buildSTInfo != null) {
            buildSTInfo.updateWithExternalPara(this.q);
        }
        return buildSTInfo;
    }

    private void t() {
        STInfoV2 a2 = a(this.H);
        if (a2 != null) {
            a2.actionId = 100;
        }
        l.a(a2);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        try {
            setContentView((int) R.layout.activity_download);
            this.n = this;
            this.y.register(this);
            this.z.register(this);
            u();
            z();
            this.x.h();
            this.x.f3402a = this.ad;
            v();
            E();
            AstApp.i().j().sendEmptyMessage(EventDispatcherEnum.UI_EVENT_ENTRANCE_DOWNLOAD_PAGE);
            t();
        } catch (Throwable th) {
            this.ae = true;
            finish();
        }
    }

    private void u() {
        long j;
        Intent intent = getIntent();
        this.P = com.tencent.pangu.mediadownload.p.a(intent);
        if (this.P != null) {
            this.J = true;
            if (this.P.d() == 1) {
                f.b("ANDROID.YYB.DOWNURL");
                if (this.q != null) {
                    this.q.f2059a = "ANDROID.YYB.DOWNURL";
                    return;
                }
                return;
            }
            return;
        }
        Bundle extras = intent.getExtras();
        if (extras != null) {
            String string = extras.getString(com.tencent.assistant.a.a.c);
            if (TextUtils.isEmpty(string)) {
                j = bm.c(extras.getString(com.tencent.assistant.a.a.f382a));
            } else {
                j = 0;
            }
            long c = bm.c(extras.getString(com.tencent.assistant.a.a.b));
            this.R = extras.getString(com.tencent.assistant.a.a.e);
            int d = bm.d(extras.getString(com.tencent.assistant.a.a.h));
            String string2 = extras.getString(com.tencent.assistant.a.a.d);
            String string3 = extras.getString(com.tencent.assistant.a.a.C);
            this.J = extras.getBoolean(com.tencent.assistant.a.a.t);
            this.K = !TextUtils.isEmpty(extras.getString(com.tencent.assistant.a.a.w));
            this.M = bm.d(extras.getString(com.tencent.assistant.a.a.H));
            if (bm.d(extras.getString(com.tencent.assistant.a.a.D)) == 1) {
                this.L = true;
            }
            this.N = extras.getString(com.tencent.assistant.a.a.I);
            if (!TextUtils.isEmpty(string) || c > 0) {
                this.H = new SimpleAppModel();
                this.H.f938a = j;
                this.H.c = string;
                this.H.d = string2;
                this.H.b = c;
                this.H.ac = string3;
                this.H.g = d;
                this.I = extras.getString(com.tencent.assistant.a.a.s);
                this.H.Q = (byte) bm.a(this.I, 0);
                if (this.H.Q == 1) {
                    this.H.ad = bm.d(extras.getString(com.tencent.assistant.a.a.J));
                }
            }
            this.ad = extras.getBoolean("has_new_app_to_install", false);
            m.a().u(true);
        }
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        if (!this.ae) {
            AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE, this);
            AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_UPDATE_DELETE, this);
            AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_ADD, this);
            AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_INSTALL, this);
            AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_DOWNLOADLIST_READY, this);
            AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DOWNLOADING, this);
            AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_DELETE, this);
            AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOADING, this);
            AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_QREADER_DELETE, this);
            AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_ADD, this);
            AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_DELETE, this);
            AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOADING, this);
            AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_ADD, this);
            this.x.f();
            this.x.notifyDataSetChanged();
            w();
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        if (!this.ae) {
            AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE, this);
            AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_UPDATE_DELETE, this);
            AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_ADD, this);
            AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_INSTALL, this);
            AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_DOWNLOADLIST_READY, this);
            AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DOWNLOADING, this);
            AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_DELETE, this);
            AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOADING, this);
            AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_QREADER_DELETE, this);
            AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_ADD, this);
            AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_DELETE, this);
            AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOADING, this);
            AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_ADD, this);
            this.x.g();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (!this.ae) {
            if (this.x != null) {
                this.x.i();
            }
            if (this.y != null) {
                this.y.unregister(this);
            }
            if (this.z != null) {
                this.z.unregister(this);
            }
        }
    }

    private void v() {
        if (DownloadProxy.a().j()) {
            x();
            this.V = true;
            this.x.a();
            InstalledAppItem installedAppItem = null;
            if (this.H != null) {
                installedAppItem = new InstalledAppItem();
                installedAppItem.b = this.H.f938a;
                installedAppItem.f1395a = this.H.c;
                installedAppItem.c = this.H.g;
            }
            this.x.a(this.N, installedAppItem);
            for (int i = 0; i < this.x.getGroupCount(); i++) {
                this.w.expandGroup(i);
            }
            w();
        }
    }

    private void w() {
        if (this.x.c() == 0 && this.x.j() == DownloadInfoMultiAdapter.CreatingTaskStatusEnum.NONE) {
            this.u.setVisibility(8);
            if (this.T.getVisibility() != 0) {
                this.T.setErrorType(60);
                this.T.setErrorText(getString(R.string.down_page_empty_text));
                this.T.setErrorImage(R.drawable.emptypage_pic_07);
                this.T.setVisibility(0);
                return;
            }
            return;
        }
        this.T.setVisibility(8);
        this.u.setVisibility(0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.activity.DownloadActivity.a(com.tencent.pangu.download.DownloadInfo, boolean, com.tencent.assistantv2.st.page.STInfoV2):void
     arg types: [com.tencent.pangu.download.DownloadInfo, int, com.tencent.assistantv2.st.page.STInfoV2]
     candidates:
      com.tencent.assistant.activity.BaseActivity.a(com.tencent.assistantv2.st.page.STPageInfo, java.lang.String, java.lang.String):void
      android.support.v4.app.FragmentActivity.a(java.lang.String, java.io.PrintWriter, android.view.View):void
      android.support.v4.app.FragmentActivity.a(java.lang.String, boolean, boolean):android.support.v4.app.z
      android.support.v4.app.FragmentActivity.a(android.support.v4.app.Fragment, android.content.Intent, int):void
      com.tencent.pangu.activity.DownloadActivity.a(com.tencent.pangu.download.DownloadInfo, boolean, com.tencent.assistantv2.st.page.STInfoV2):void */
    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0076  */
    /* JADX WARNING: Removed duplicated region for block: B:24:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void x() {
        /*
            r6 = this;
            r5 = 0
            r1 = 0
            com.tencent.assistant.model.SimpleAppModel r0 = r6.H
            if (r0 == 0) goto L_0x00ab
            com.tencent.pangu.manager.DownloadProxy r0 = com.tencent.pangu.manager.DownloadProxy.a()
            com.tencent.assistant.model.SimpleAppModel r2 = r6.H
            java.lang.String r2 = r2.q()
            com.tencent.pangu.download.DownloadInfo r0 = r0.d(r2)
            if (r0 != 0) goto L_0x00ad
            com.tencent.pangu.manager.DownloadProxy r0 = com.tencent.pangu.manager.DownloadProxy.a()
            com.tencent.assistant.model.SimpleAppModel r2 = r6.H
            java.lang.String r2 = r2.c
            com.tencent.assistant.model.SimpleAppModel r3 = r6.H
            int r3 = r3.g
            com.tencent.assistant.model.SimpleAppModel r4 = r6.H
            int r4 = r4.ad
            com.tencent.pangu.download.DownloadInfo r0 = r0.a(r2, r3, r4)
            r2 = r0
        L_0x002b:
            if (r2 == 0) goto L_0x00ab
            r6.H = r5
            r0 = 1
            com.tencent.assistant.model.SimpleAppModel r3 = r6.H
            com.tencent.assistantv2.st.page.STInfoV2 r3 = r6.a(r3)
            r6.a(r2)
            r6.a(r2, r1, r3)
        L_0x003c:
            com.tencent.assistant.model.SimpleAppModel r2 = r6.H
            if (r2 == 0) goto L_0x006b
            if (r0 != 0) goto L_0x006b
            com.tencent.pangu.adapter.DownloadInfoMultiAdapter r0 = r6.x
            com.tencent.pangu.adapter.DownloadInfoMultiAdapter$CreatingTaskStatusEnum r1 = com.tencent.pangu.adapter.DownloadInfoMultiAdapter.CreatingTaskStatusEnum.CREATING
            r0.a(r1)
            com.tencent.assistant.localres.ApkResourceManager r0 = com.tencent.assistant.localres.ApkResourceManager.getInstance()
            boolean r0 = r0.isLocalApkDataReady()
            if (r0 == 0) goto L_0x005e
            java.lang.String r0 = "icerao"
            java.lang.String r1 = "local apk is ready."
            android.util.Log.i(r0, r1)
            r6.a(r5)
        L_0x005d:
            return
        L_0x005e:
            com.tencent.assistant.utils.TemporaryThreadManager r0 = com.tencent.assistant.utils.TemporaryThreadManager.get()
            com.tencent.pangu.activity.ae r1 = new com.tencent.pangu.activity.ae
            r1.<init>(r6)
            r0.start(r1)
            goto L_0x005d
        L_0x006b:
            com.tencent.pangu.adapter.DownloadInfoMultiAdapter r0 = r6.x
            com.tencent.pangu.adapter.DownloadInfoMultiAdapter$CreatingTaskStatusEnum r2 = com.tencent.pangu.adapter.DownloadInfoMultiAdapter.CreatingTaskStatusEnum.NONE
            r0.a(r2)
            com.tencent.pangu.mediadownload.p r0 = r6.P
            if (r0 == 0) goto L_0x005d
            com.tencent.pangu.mediadownload.e r0 = com.tencent.pangu.mediadownload.e.c()
            com.tencent.pangu.mediadownload.p r2 = r6.P
            com.tencent.pangu.model.d r0 = r0.a(r2)
            if (r0 == 0) goto L_0x0096
            r1 = 900(0x384, float:1.261E-42)
            com.tencent.assistantv2.st.page.STInfoV2 r1 = com.tencent.assistantv2.st.page.STInfoBuilder.buildSTInfo(r6, r1)
            com.tencent.assistantv2.st.model.StatInfo r1 = com.tencent.assistantv2.st.page.a.a(r1)
            r0.v = r1
            com.tencent.pangu.mediadownload.e r1 = com.tencent.pangu.mediadownload.e.c()
            r1.a(r0)
            goto L_0x005d
        L_0x0096:
            r6.y()
            com.tencent.pangu.module.bh r0 = r6.z
            com.tencent.pangu.mediadownload.p r2 = r6.P
            java.lang.String r2 = r2.a()
            com.tencent.pangu.mediadownload.p r3 = r6.P
            java.lang.String r3 = r3.b()
            r0.a(r2, r3, r1)
            goto L_0x005d
        L_0x00ab:
            r0 = r1
            goto L_0x003c
        L_0x00ad:
            r2 = r0
            goto L_0x002b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.pangu.activity.DownloadActivity.x():void");
    }

    private void y() {
        AppConst.LoadingDialogInfo loadingDialogInfo = new AppConst.LoadingDialogInfo();
        loadingDialogInfo.loadingText = "正在进行安全检查...";
        loadingDialogInfo.blockCaller = true;
        this.Q = DialogUtils.showLoadingDialog(loadingDialogInfo);
        if (this.Q != null) {
            this.Q.setCancelable(true);
            this.Q.setCanceledOnTouchOutside(false);
        }
    }

    /* access modifiers changed from: private */
    public synchronized void a(LocalApkInfo localApkInfo) {
        if (!this.Y) {
            this.Y = true;
            if (localApkInfo == null) {
                if (!TextUtils.isEmpty(this.H.c)) {
                    localApkInfo = ApkResourceManager.getInstance().getLocalApkInfo(this.H.c);
                } else if (this.H.f938a > 0) {
                    localApkInfo = ApkResourceManager.getInstance().getLocalApkInfo(this.H.f938a);
                }
            }
            if (localApkInfo != null && !TextUtils.isEmpty(localApkInfo.manifestMd5)) {
                this.H.ak = localApkInfo.manifestMd5;
                this.H.D = localApkInfo.mVersionCode;
            }
            this.U = (long) this.y.a(this.H);
            Log.i("icerao", "apk pkgname:" + this.H.c + ",manifestMD5:" + this.H.ak + ",requestid:" + this.U);
        }
    }

    private void a(DownloadInfo downloadInfo) {
        Bundle extras = getIntent().getExtras();
        if (extras != null && downloadInfo != null) {
            downloadInfo.initCallParamsFromActionBundle(extras);
        }
    }

    private void z() {
        this.v = (SecondNavigationTitleViewV5) findViewById(R.id.title_view);
        if (D()) {
            this.v.g();
        }
        if (!this.J || this.L) {
            this.v.b(getResources().getString(R.string.down_page_title));
        } else {
            this.v.a(getResources().getString(R.string.down_page_title));
        }
        this.v.b(getString(R.string.down_page_title));
        this.v.a(this);
        this.v.d();
        this.u = (RelativeLayout) findViewById(R.id.container_layout);
        this.w = (TXExpandableListView) findViewById(R.id.list_view);
        this.A = findViewById(R.id.pop_bar);
        this.B = (TextView) findViewById(R.id.group_title);
        this.C = (TextView) findViewById(R.id.group_title_num);
        this.D = (Button) findViewById(R.id.group_action);
        this.x = new DownloadInfoMultiAdapter(this.n, this.w, new al(this));
        this.x.a(D());
        this.w.setDivider(null);
        TemporaryThreadManager.get().start(new am(this));
        try {
            DownloadListFooterView downloadListFooterView = new DownloadListFooterView(this.n, TXScrollViewBase.ScrollDirection.SCROLL_DIRECTION_VERTICAL, TXScrollViewBase.ScrollMode.NONE);
            downloadListFooterView.a().setVisibility(8);
            downloadListFooterView.a(8);
            this.w.addFooterView(downloadListFooterView);
            this.x.a(downloadListFooterView);
        } catch (Throwable th) {
        }
        this.w.setAdapter(this.x);
        this.w.setGroupIndicator(null);
        if (D()) {
            this.w.setOnScrollListener(null);
            this.A.setVisibility(8);
        } else {
            this.w.setOnScrollListener(this.af);
            this.A.setVisibility(0);
        }
        this.x.a(this.af);
        try {
            this.w.setSelector(R.drawable.transparent_selector);
            this.w.setOnGroupClickListener(new an(this));
            this.T = (NormalErrorRecommendPage) findViewById(R.id.error_page);
            this.T.setActivityPageId(STConst.ST_PAGE_DOWNLOAD_ERROR_PAGE);
            this.T.setVisibility(8);
        } catch (Exception e) {
        }
    }

    /* access modifiers changed from: private */
    public void A() {
        View a2 = br.a(this, this.aa.b);
        if (a2 != null) {
            RelativeLayout relativeLayout = (RelativeLayout) a2.findViewById(R.id.header_layout);
            relativeLayout.setTag(R.id.tma_st_slot_tag, this.ac);
            relativeLayout.setOnClickListener(new ao(this));
            ((RelativeLayout) a2.findViewById(R.id.close_zone)).setOnClickListener(new ap(this));
            TemporaryThreadManager.get().start(new aq(this));
            this.w.addHeaderView(a2);
            a((Boolean) true);
        }
    }

    public void a(int i, int i2, byte b, byte b2, AppSimpleDetail appSimpleDetail) {
        if (this.Q != null) {
            this.Q.dismiss();
        }
        if (appSimpleDetail != null) {
            this.R = "1";
            this.U = (long) i;
            onGetAppInfoSuccess(i, i2, appSimpleDetail);
        } else if (this.P != null) {
            b(i, i2, b, b2, this.P.a());
        }
    }

    private void b(int i, int i2, byte b, byte b2, String str) {
        if (b2 == 1) {
            ar arVar = new ar(this, str);
            arVar.titleRes = getString(R.string.file_down_unsafe_tips);
            arVar.contentRes = getString(R.string.file_down_unsafe_tips_content);
            arVar.lBtnTxtRes = getString(R.string.file_down_cancel);
            arVar.rBtnTxtRes = getString(R.string.file_down_confirm);
            DialogUtils.show2BtnDialog(this, arVar);
            l.a(new STInfoV2(STConst.ST_PAGE_DOWNLOAD_URL_UNSAFE, a.a(STConst.ST_DEFAULT_SLOT, 0), f(), STConst.ST_DEFAULT_SLOT, 100));
            return;
        }
        if (b == 1) {
            Toast.makeText(this, (int) R.string.file_down_outter_apk_tips, 0).show();
        } else if (b2 == 0) {
            Toast.makeText(this, (int) R.string.file_down_outter_unkown_file_tips, 0).show();
        }
        d a2 = d.a(this.P.c(), this.P.b(), str);
        a2.v = a.a(STInfoBuilder.buildSTInfo(this, 900));
        e.c().a(a2);
    }

    public void a(int i, int i2, byte b, byte b2, String str) {
        if (!TextUtils.isEmpty(str)) {
            b(i, i2, b, b2, str);
        }
    }

    public void a(int i, int i2) {
        a(i, i2, (byte) 2, (byte) 0, (AppSimpleDetail) null);
    }

    public void onGetAppInfoSuccess(int i, int i2, AppSimpleDetail appSimpleDetail) {
        boolean z2 = false;
        boolean z3 = true;
        Log.e("icerao", "get appinfo succ.seq:" + i + ",errorCode:" + i2 + ",is appmodel null?" + (appSimpleDetail == null));
        if (appSimpleDetail == null) {
            b(11);
            if (((long) i) == this.U) {
                ah.a().post(new as(this));
                return;
            }
            return;
        }
        SimpleAppModel a2 = k.a(appSimpleDetail);
        DownloadInfo a3 = DownloadProxy.a().a(a2);
        if (a3 != null && a3.needReCreateInfo(a2)) {
            DownloadProxy.a().b(a3.downloadTicket);
            a3 = null;
        }
        ah.a().post(new af(this, i));
        STInfoV2 a4 = a(a2);
        if (a3 == null) {
            StatInfo a5 = a.a(a4);
            if (this.J) {
                a2.af = this.N;
            }
            a3 = DownloadInfo.createDownloadInfo(a2, a5);
            a3.downloadState = SimpleDownloadInfo.DownloadState.PAUSED;
            if (!b("2")) {
                z2 = true;
            }
            a3.autoInstall = z2;
            a(a3);
            DownloadProxy.a().d(a3);
            AstApp.i().j().sendMessage(AstApp.i().j().obtainMessage(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_ADD, a3));
        } else {
            if (a3.downloadState == SimpleDownloadInfo.DownloadState.SUCC && !a3.isDownloadFileExist()) {
                z2 = true;
            }
            a(a3);
            ah.a().post(new ag(this));
            z3 = z2;
        }
        a(a3, z3, a4);
        if (((long) i) == this.U) {
            this.H = null;
        }
    }

    private void a(DownloadInfo downloadInfo, boolean z2, STInfoV2 sTInfoV2) {
        boolean z3 = true;
        if (!this.W) {
            this.W = true;
            if (b("2")) {
                z3 = false;
            }
            downloadInfo.autoInstall = z3;
            if ((b("3") && c.e()) || b("1")) {
                TemporaryThreadManager.get().start(new ah(this, sTInfoV2, z2, downloadInfo));
            }
        }
    }

    private boolean b(String str) {
        if (this.R == null) {
            this.R = Constants.STR_EMPTY;
        }
        if ("0".equals(str)) {
            return this.R.length() == 0 || this.R.contains("0");
        }
        return this.R.contains(str);
    }

    public void onGetAppInfoFail(int i, int i2) {
        if (((long) i) == this.U) {
            ah.a().post(new ai(this));
        }
        b(12);
        Log.e("icerao", "get appinfo fail.seq:" + i + ",errorCode:" + i2);
    }

    /* access modifiers changed from: private */
    public void B() {
        long j;
        int pointToPosition = this.w.pointToPosition(0, C() - 10);
        int pointToPosition2 = this.w.pointToPosition(0, 0);
        if (pointToPosition2 != -1) {
            j = this.w.getExpandableListPosition(pointToPosition2);
            int packedPositionChild = ExpandableListView.getPackedPositionChild(j);
            int packedPositionGroup = ExpandableListView.getPackedPositionGroup(j);
            if (packedPositionChild == -1) {
                View expandChildAt = this.w.getExpandChildAt(pointToPosition2 - this.w.getFirstVisiblePosition());
                if (expandChildAt == null) {
                    this.E = 100;
                } else {
                    this.E = expandChildAt.getHeight();
                }
            }
            if (G > 0) {
                this.F = packedPositionGroup;
                String[] b = this.x.b(packedPositionGroup);
                if (b != null) {
                    this.B.setText(b[0]);
                    this.C.setText(" " + b[1]);
                }
                if (packedPositionGroup == 1) {
                    this.D.setText(this.n.getResources().getString(R.string.down_page_group_clear_text));
                    this.D.setVisibility(0);
                    this.D.setOnClickListener(this.x.b);
                } else {
                    this.D.setVisibility(8);
                }
                if (this.F != packedPositionGroup || !this.w.isGroupExpanded(packedPositionGroup)) {
                    this.A.setVisibility(8);
                } else {
                    this.A.setVisibility(0);
                }
            }
            if (G == 0) {
                this.A.setVisibility(8);
            }
        } else {
            j = 0;
        }
        if (this.F != -1) {
            if (j == 0 && pointToPosition == 0) {
                this.A.setVisibility(8);
                return;
            }
            int C2 = C();
            ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) this.A.getLayoutParams();
            marginLayoutParams.topMargin = (-(this.E - C2)) - 5;
            this.A.setLayoutParams(marginLayoutParams);
        }
    }

    private int C() {
        int i = this.E;
        int pointToPosition = this.w.pointToPosition(0, this.E);
        if (pointToPosition == -1 || ExpandableListView.getPackedPositionGroup(this.w.getExpandableListPosition(pointToPosition)) == this.F) {
            return i;
        }
        View expandChildAt = this.w.getExpandChildAt(pointToPosition - this.w.getFirstVisiblePosition());
        if (expandChildAt != null) {
            return expandChildAt.getTop();
        }
        return 100;
    }

    public void handleUIEvent(Message message) {
        switch (message.what) {
            case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DOWNLOADING:
            case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE:
            case EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOADING:
            case EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_ADD:
            case EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_DELETE:
            case EventDispatcherEnum.UI_EVENT_FILE_DOWNLOADING:
            case EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_ADD:
            case EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_DELETE:
            case EventDispatcherEnum.UI_EVENT_QREADER_DELETE:
                w();
                return;
            case EventDispatcherEnum.UI_EVENT_DOWNLOADLIST_READY:
                if (!this.V) {
                    v();
                    return;
                }
                return;
            default:
                return;
        }
    }

    private boolean D() {
        if (!this.J || this.K || this.L) {
            return false;
        }
        return true;
    }

    private void E() {
        if (this.J && this.M != 0 && this.w != null && this.x != null && this.L && this.M < this.x.getGroupCount()) {
            int i = 0;
            for (int i2 = 0; i2 < this.M; i2++) {
                i += this.x.getChildrenCount(i2);
            }
            this.w.setSelection(i - 1);
            this.ag.sendEmptyMessageDelayed(1, 100);
        }
    }

    private void b(int i) {
        if (!TextUtils.isEmpty(this.q.f2059a)) {
            String str = Constants.STR_EMPTY;
            if (this.H != null) {
                str = com.tencent.assistant.st.h.a(this.q.c, this.q.d, this.H.ac, this.q.f2059a, this.q.b, String.valueOf(this.H.f938a), this.q.f, this.H.c, String.valueOf(this.H.g));
            }
            l.a("StatIpcToDownload", this.q.e, str + "_" + i);
        }
    }
}
