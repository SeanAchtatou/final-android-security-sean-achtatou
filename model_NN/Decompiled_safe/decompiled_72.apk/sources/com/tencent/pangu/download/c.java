package com.tencent.pangu.download;

import com.tencent.pangu.download.SimpleDownloadInfo;
import com.tencent.pangu.manager.DownloadProxy;
import com.tencent.pangu.manager.ak;

/* compiled from: ProGuard */
class c implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DownloadInfo f3756a;
    final /* synthetic */ SimpleDownloadInfo.UIType b;
    final /* synthetic */ a c;

    c(a aVar, DownloadInfo downloadInfo, SimpleDownloadInfo.UIType uIType) {
        this.c = aVar;
        this.f3756a = downloadInfo;
        this.b = uIType;
    }

    public void run() {
        if (this.f3756a.uiType != this.b) {
            this.f3756a.uiType = this.b;
            DownloadProxy.a().d(this.f3756a);
        }
        if (!ak.a().b(this.f3756a)) {
            DownloadProxy.a().c(this.f3756a);
        } else if (this.f3756a.downloadState == SimpleDownloadInfo.DownloadState.FAIL) {
            DownloadProxy.a().c(this.f3756a);
        } else {
            ak.a().d(this.f3756a);
        }
    }
}
