package com.tencent.pangu.download;

import com.tencent.assistant.AppConst;
import com.tencent.assistant.module.k;
import com.tencent.connect.common.Constants;
import com.tencent.pangu.mediadownload.c;
import com.tencent.pangu.mediadownload.q;
import com.tencent.pangu.model.d;

/* compiled from: ProGuard */
public class DownloadInfoWrapper {

    /* renamed from: a  reason: collision with root package name */
    public InfoType f3748a;
    public DownloadInfo b;
    public q c;
    public c d;
    public d e;

    /* compiled from: ProGuard */
    public enum InfoType {
        App,
        Video,
        Book,
        CommonFile
    }

    public DownloadInfoWrapper(DownloadInfo downloadInfo) {
        this.f3748a = InfoType.App;
        this.b = downloadInfo;
    }

    public DownloadInfoWrapper(q qVar) {
        this.f3748a = InfoType.Video;
        this.c = qVar;
    }

    public DownloadInfoWrapper(c cVar) {
        this.f3748a = InfoType.Book;
        this.d = cVar;
    }

    public DownloadInfoWrapper(d dVar) {
        this.f3748a = InfoType.CommonFile;
        this.e = dVar;
    }

    public long a() {
        if (this.f3748a == InfoType.App) {
            return this.b.createTime;
        }
        if (this.f3748a == InfoType.Video) {
            return this.c.o;
        }
        if (this.f3748a == InfoType.Book) {
            return this.d.i;
        }
        if (this.f3748a == InfoType.CommonFile) {
            return this.e.o;
        }
        return 0;
    }

    public long b() {
        if (this.f3748a == InfoType.App) {
            return this.b.downloadEndTime;
        }
        if (this.f3748a == InfoType.Video) {
            return this.c.p;
        }
        if (this.f3748a == InfoType.Book) {
            return this.d.j;
        }
        if (this.f3748a == InfoType.CommonFile) {
            return this.e.p;
        }
        return 0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.module.k.a(com.tencent.pangu.download.DownloadInfo, boolean, boolean):com.tencent.assistant.AppConst$AppState
     arg types: [com.tencent.pangu.download.DownloadInfo, int, int]
     candidates:
      com.tencent.assistant.module.k.a(com.tencent.pangu.download.DownloadInfo, com.tencent.assistant.plugin.PluginDownloadInfo, com.tencent.assistant.plugin.PluginInfo):com.tencent.assistant.AppConst$AppState
      com.tencent.assistant.module.k.a(java.util.ArrayList<com.tencent.assistant.protocol.jce.CardItem>, com.tencent.assistant.module.m, int):java.util.ArrayList<com.tencent.assistant.model.SimpleAppModel>
      com.tencent.assistant.module.k.a(com.tencent.pangu.download.DownloadInfo, boolean, boolean):com.tencent.assistant.AppConst$AppState */
    public boolean c() {
        boolean z = true;
        if (this.f3748a != InfoType.App) {
            return false;
        }
        if (k.a(this.b, true, true) != AppConst.AppState.DOWNLOADED) {
            z = false;
        }
        return z;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.module.k.a(com.tencent.pangu.download.DownloadInfo, boolean, boolean):com.tencent.assistant.AppConst$AppState
     arg types: [com.tencent.pangu.download.DownloadInfo, int, int]
     candidates:
      com.tencent.assistant.module.k.a(com.tencent.pangu.download.DownloadInfo, com.tencent.assistant.plugin.PluginDownloadInfo, com.tencent.assistant.plugin.PluginInfo):com.tencent.assistant.AppConst$AppState
      com.tencent.assistant.module.k.a(java.util.ArrayList<com.tencent.assistant.protocol.jce.CardItem>, com.tencent.assistant.module.m, int):java.util.ArrayList<com.tencent.assistant.model.SimpleAppModel>
      com.tencent.assistant.module.k.a(com.tencent.pangu.download.DownloadInfo, boolean, boolean):com.tencent.assistant.AppConst$AppState */
    public boolean d() {
        boolean z = true;
        if (this.f3748a != InfoType.App) {
            return false;
        }
        if (k.a(this.b, true, true) != AppConst.AppState.INSTALLED) {
            z = false;
        }
        return z;
    }

    public String e() {
        if (this.f3748a == InfoType.App) {
            return this.b.name;
        }
        if (this.f3748a == InfoType.Video) {
            return this.c.f3868a;
        }
        if (this.f3748a == InfoType.Book) {
            return this.d.f3855a;
        }
        if (this.f3748a == InfoType.CommonFile) {
            return this.e.g();
        }
        return Constants.STR_EMPTY;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass() || !(obj instanceof DownloadInfoWrapper)) {
            return false;
        }
        DownloadInfoWrapper downloadInfoWrapper = (DownloadInfoWrapper) obj;
        if (this.f3748a == InfoType.App && downloadInfoWrapper.f3748a == InfoType.App && this.b != null && downloadInfoWrapper.b != null) {
            return this.b.downloadTicket.equals(downloadInfoWrapper.b.downloadTicket);
        }
        if (this.f3748a == InfoType.Video && downloadInfoWrapper.f3748a == InfoType.Video && this.c != null && downloadInfoWrapper.c != null) {
            return this.c.m.equals(downloadInfoWrapper.c.m);
        }
        if (this.f3748a == InfoType.Book && downloadInfoWrapper.f3748a == InfoType.Book && this.d != null && downloadInfoWrapper.d != null) {
            return this.d.c.equals(downloadInfoWrapper.d.c);
        }
        if (this.f3748a != InfoType.CommonFile || downloadInfoWrapper.f3748a != InfoType.CommonFile || this.e == null || downloadInfoWrapper.e == null) {
            return false;
        }
        return this.e.m.equals(downloadInfoWrapper.e.m);
    }

    public int hashCode() {
        int hashCode = this.f3748a.hashCode();
        if (this.f3748a == InfoType.App) {
            return (hashCode * 31) + this.b.downloadTicket.hashCode();
        }
        if (this.f3748a == InfoType.Video) {
            return (hashCode * 31) + this.c.m.hashCode();
        }
        if (this.f3748a == InfoType.Book) {
            return (hashCode * 31) + this.d.c.hashCode();
        }
        if (this.f3748a == InfoType.CommonFile) {
            return (hashCode * 31) + this.e.m.hashCode();
        }
        return hashCode;
    }
}
