package com.tencent.pangu.download;

import com.tencent.assistantv2.st.model.StatInfo;
import java.util.List;

/* compiled from: ProGuard */
class i implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ List f3762a;
    final /* synthetic */ StatInfo b;
    final /* synthetic */ a c;

    i(a aVar, List list, StatInfo statInfo) {
        this.c = aVar;
        this.f3762a = list;
        this.b = statInfo;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.download.a.a(com.tencent.pangu.download.a, boolean):boolean
     arg types: [com.tencent.pangu.download.a, int]
     candidates:
      com.tencent.pangu.download.a.a(com.tencent.pangu.download.a, java.util.List):java.util.List
      com.tencent.pangu.download.a.a(java.util.ArrayList<com.tencent.pangu.download.DownloadInfo>, boolean):void
      com.tencent.pangu.download.a.a(long, long):boolean
      com.tencent.pangu.download.a.a(java.lang.String, int):boolean
      com.tencent.pangu.download.a.a(java.util.List<com.tencent.assistant.model.SimpleAppModel>, com.tencent.assistantv2.st.model.StatInfo):int
      com.tencent.pangu.download.a.a(com.tencent.assistant.model.SimpleAppModel, com.tencent.assistantv2.st.model.StatInfo):void
      com.tencent.pangu.download.a.a(com.tencent.pangu.download.DownloadInfo, com.tencent.pangu.download.SimpleDownloadInfo$UIType):void
      com.tencent.pangu.download.a.a(java.lang.String, com.tencent.cloud.a.f):void
      com.tencent.pangu.download.a.a(java.lang.String, java.lang.String):void
      com.tencent.pangu.download.a.a(com.tencent.pangu.download.DownloadInfo, boolean):boolean
      com.tencent.pangu.download.a.a(java.lang.String, boolean):boolean
      com.tencent.pangu.download.a.a(com.tencent.pangu.download.a, boolean):boolean */
    public void run() {
        if (this.c.c() == 0) {
            List a2 = this.c.a(this.f3762a);
            if (a2 != null && !a2.isEmpty()) {
                this.c.b(a2, this.b);
            }
            boolean unused = this.c.g = false;
        }
    }
}
