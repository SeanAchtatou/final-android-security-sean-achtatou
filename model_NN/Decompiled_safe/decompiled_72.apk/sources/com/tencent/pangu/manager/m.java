package com.tencent.pangu.manager;

import com.tencent.downloadsdk.DownloadManager;
import com.tencent.pangu.download.DownloadInfo;
import com.tencent.pangu.download.SimpleDownloadInfo;
import java.util.Iterator;

/* compiled from: ProGuard */
class m implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DownloadProxy f3836a;

    m(DownloadProxy downloadProxy) {
        this.f3836a = downloadProxy;
    }

    public void run() {
        Iterator<DownloadInfo> it = this.f3836a.c().iterator();
        while (it.hasNext()) {
            DownloadInfo next = it.next();
            if (next.downloadState == SimpleDownloadInfo.DownloadState.DOWNLOADING || next.downloadState == SimpleDownloadInfo.DownloadState.QUEUING) {
                next.downloadState = SimpleDownloadInfo.DownloadState.FAIL;
                DownloadManager.a().a(next.getDownloadSubType(), next.downloadTicket);
                this.f3836a.a(next, SimpleDownloadInfo.DownloadState.FAIL);
                this.f3836a.e.a(next);
            }
        }
    }
}
