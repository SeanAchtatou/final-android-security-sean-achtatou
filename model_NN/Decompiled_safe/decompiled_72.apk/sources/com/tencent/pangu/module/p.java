package com.tencent.pangu.module;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.module.BaseEngine;
import com.tencent.assistant.module.k;
import com.tencent.assistant.protocol.jce.SuggestRequest;
import com.tencent.assistant.protocol.jce.SuggestResponse;
import com.tencent.pangu.module.a.j;

/* compiled from: ProGuard */
public class p extends BaseEngine<j> {

    /* renamed from: a  reason: collision with root package name */
    private int f3944a = -1;

    public void a() {
        cancel(this.f3944a);
        this.f3944a = -1;
    }

    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i, JceStruct jceStruct, JceStruct jceStruct2) {
        if (jceStruct2 != null && i == this.f3944a) {
            SuggestResponse suggestResponse = (SuggestResponse) jceStruct2;
            notifyDataChangedInMainThread(new q(this, k.b(suggestResponse.c), suggestResponse));
        }
    }

    /* access modifiers changed from: protected */
    public void onRequestFailed(int i, int i2, JceStruct jceStruct, JceStruct jceStruct2) {
        notifyDataChangedInMainThread(new r(this, i2));
    }

    public int a(String str) {
        SuggestRequest suggestRequest = new SuggestRequest();
        suggestRequest.f1573a = str;
        this.f3944a = send(suggestRequest);
        return this.f3944a;
    }

    public void a(int i) {
        super.cancel(i);
        this.f3944a = -1;
    }
}
