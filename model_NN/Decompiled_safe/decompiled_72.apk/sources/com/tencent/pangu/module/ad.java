package com.tencent.pangu.module;

import com.qq.AppService.AstApp;
import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.b.c;
import com.tencent.assistant.b.i;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.m;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.model.b;
import com.tencent.assistant.module.BaseEngine;
import com.tencent.assistant.module.k;
import com.tencent.assistant.protocol.jce.Banner;
import com.tencent.assistant.protocol.jce.EntranceTemplate;
import com.tencent.assistant.protocol.jce.GetDiscoverRequest;
import com.tencent.assistant.protocol.jce.GetDiscoverResponse;
import com.tencent.assistant.protocol.jce.GetSmartCardsRequest;
import com.tencent.assistant.protocol.jce.GetSmartCardsResponse;
import com.tencent.assistant.protocol.jce.LbsData;
import com.tencent.assistant.protocol.jce.SmartCardWrapper;
import com.tencent.assistant.protocol.jce.TopBanner;
import com.tencent.assistant.protocol.scu.RequestResponePair;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistantv2.st.business.LaunchSpeedSTManager;
import com.tencent.nucleus.socialcontact.login.j;
import com.tencent.pangu.module.a.f;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* compiled from: ProGuard */
public class ad extends BaseEngine<f> {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public c f3895a;
    /* access modifiers changed from: private */
    public LbsData b;
    /* access modifiers changed from: private */
    public ArrayList<Banner> c;
    private ArrayList<SimpleAppModel> d;
    /* access modifiers changed from: private */
    public TopBanner e;
    /* access modifiers changed from: private */
    public long f;
    /* access modifiers changed from: private */
    public com.tencent.assistant.smartcard.b.c g;
    private int h;
    private int i;
    /* access modifiers changed from: private */
    public ar j;
    private b k;
    private long l;
    private ao m;
    private long n;
    private EntranceTemplate o;
    private long p;
    private Map<Integer, Integer> q;
    private i r;

    public ad() {
        this.f3895a = null;
        this.c = new ArrayList<>();
        this.d = new ArrayList<>();
        this.e = null;
        this.g = new com.tencent.assistant.smartcard.b.b();
        this.h = -1;
        this.j = new ar();
        this.k = new b();
        this.l = -1;
        this.m = new ao(this, null);
        this.o = null;
        this.q = Collections.synchronizedMap(new HashMap());
        this.r = new an(this);
        this.f3895a = new c(AstApp.i().getApplicationContext(), this.r);
        TemporaryThreadManager.get().startDelayed(new ae(this), 4000);
    }

    /* access modifiers changed from: private */
    public int b(ar arVar) {
        int i2 = 10;
        if (arVar == null || arVar.f3909a == null || arVar.f3909a.length == 0) {
            arVar = new ar();
            this.j = arVar;
        }
        if (arVar.f3909a == null || arVar.f3909a.length == 0) {
            TemporaryThreadManager.get().startDelayed(new af(this), 4000);
        }
        GetSmartCardsRequest getSmartCardsRequest = new GetSmartCardsRequest();
        getSmartCardsRequest.f1334a = 0;
        getSmartCardsRequest.b = this.b;
        getSmartCardsRequest.e = a();
        getSmartCardsRequest.d = arVar.b;
        GetDiscoverRequest getDiscoverRequest = new GetDiscoverRequest();
        int a2 = m.a().a("key_homepage_per_page_num_4_normal", 10);
        if (a2 > 0) {
            i2 = a2;
        }
        getDiscoverRequest.f1278a = i2;
        XLog.e("panguSetting", "GetHomeEngine sendRequest normal.pageSize: " + getDiscoverRequest.f1278a + ", perPageNum4NormalCard: " + a2);
        getDiscoverRequest.b = arVar.f3909a;
        getDiscoverRequest.d = j.a().n();
        ArrayList arrayList = new ArrayList(2);
        arrayList.add(getSmartCardsRequest);
        arrayList.add(getDiscoverRequest);
        this.h = send(arrayList);
        this.q.put(Integer.valueOf(this.h), Integer.valueOf(getSmartCardsRequest.e));
        XLog.d("GetHomeEngine", "getHomeEngine sendRequest:" + arVar + ",requestid:" + this.h + ", smart req page size: " + getSmartCardsRequest.e);
        return this.h;
    }

    private int a(int i2) {
        if (this.q.containsKey(Integer.valueOf(i2))) {
            return this.q.remove(Integer.valueOf(i2)).intValue();
        }
        return a();
    }

    public int a() {
        int a2 = m.a().a("key_homepage_per_page_num__4_smart", 10);
        if (a2 > 0) {
            return a2;
        }
        return 10;
    }

    public int b() {
        if (this.h > 0 && this.h != this.m.c) {
            return this.h;
        }
        this.i = 0;
        this.h = b((ar) null);
        return this.h;
    }

    public int a(ar arVar) {
        if (arVar == null || arVar.f3909a == null) {
            return -1;
        }
        int b2 = this.m.b();
        if (this.m.d() == null || arVar.f3909a != this.m.d().f3909a || this.m.f() == null || this.m.f().size() == 0) {
            XLog.d("GetHomeEngine", "get next page from network.pagercontext:" + arVar);
            return b(arVar);
        } else if (this.l != this.m.c()) {
            XLog.d("GetHomeEngine", "version not same:" + this.l + ",refresh.");
            return b();
        } else {
            boolean b3 = this.m.e;
            ArrayList<SimpleAppModel> f2 = this.m.f();
            this.j = this.m.e();
            XLog.d("GetHomeEngine", "get data from nextPagerLoader.has next:" + b3 + ",query pagerContext:" + arVar + ",next pager context:" + this.j);
            this.d.addAll(f2);
            this.g.b(this.m.g());
            this.l = this.m.c();
            this.k.b(this.l);
            notifyDataChangedInMainThread(new ag(this, b3, b2, f2));
            if (!b3) {
                return b2;
            }
            XLog.d("GetHomeEngine", "nextPageLoader.exec,pagerContext:" + this.j);
            this.m.a(this.j);
            return b2;
        }
    }

    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i2, List<RequestResponePair> list) {
        int i3;
        GetSmartCardsResponse getSmartCardsResponse;
        GetDiscoverRequest getDiscoverRequest;
        GetDiscoverResponse getDiscoverResponse;
        this.h = -1;
        if (LaunchSpeedSTManager.d().b(i2)) {
            LaunchSpeedSTManager.d().a(LaunchSpeedSTManager.TYPE_TIME_POINT.HomeEngine_onRequestSuccessed_Begin);
        }
        GetDiscoverRequest getDiscoverRequest2 = null;
        GetDiscoverResponse getDiscoverResponse2 = null;
        GetSmartCardsResponse getSmartCardsResponse2 = null;
        RequestResponePair requestResponePair = null;
        for (RequestResponePair next : list) {
            if (next.request instanceof GetDiscoverRequest) {
                getDiscoverResponse = (GetDiscoverResponse) next.response;
                getDiscoverRequest = (GetDiscoverRequest) next.request;
                getSmartCardsResponse = getSmartCardsResponse2;
            } else if (next.request instanceof GetSmartCardsRequest) {
                GetSmartCardsRequest getSmartCardsRequest = (GetSmartCardsRequest) next.request;
                getSmartCardsResponse = (GetSmartCardsResponse) next.response;
                getDiscoverRequest = getDiscoverRequest2;
                next = requestResponePair;
                getDiscoverResponse = getDiscoverResponse2;
            } else {
                next = requestResponePair;
                getSmartCardsResponse = getSmartCardsResponse2;
                getDiscoverRequest = getDiscoverRequest2;
                getDiscoverResponse = getDiscoverResponse2;
            }
            getSmartCardsResponse2 = getSmartCardsResponse;
            getDiscoverResponse2 = getDiscoverResponse;
            getDiscoverRequest2 = getDiscoverRequest;
            requestResponePair = next;
        }
        if (getDiscoverRequest2 == null || getDiscoverResponse2 == null || requestResponePair == null || getDiscoverResponse2.b == null || getDiscoverResponse2.b.size() == 0) {
            if (requestResponePair != null) {
                i3 = requestResponePair.errorCode;
            } else {
                i3 = -841;
            }
            onRequestFailed(i2, i3, list);
            return;
        }
        System.currentTimeMillis();
        ArrayList<SimpleAppModel> b2 = k.b(getDiscoverResponse2.a());
        if (LaunchSpeedSTManager.d().b(i2)) {
            LaunchSpeedSTManager.d().a(LaunchSpeedSTManager.TYPE_TIME_POINT.HomeEngine_transferCardList_End);
        }
        ArrayList<SmartCardWrapper> arrayList = getSmartCardsResponse2 != null ? getSmartCardsResponse2.b : null;
        ar arVar = new ar();
        arVar.f3909a = getDiscoverResponse2.d;
        arVar.c = getDiscoverResponse2.f == 1;
        arVar.b = this.j.b;
        arVar.a(a(i2));
        if (i2 != this.m.b()) {
            boolean z = getDiscoverRequest2.b.length == 0;
            this.l = getDiscoverResponse2.c();
            this.k.b(this.l);
            if (z) {
                this.c.clear();
                this.c.addAll(getDiscoverResponse2.b());
                this.n = getDiscoverResponse2.d();
                this.d.clear();
                this.g.b();
                this.e = getDiscoverResponse2.e();
                this.f = getDiscoverResponse2.f();
                this.o = getDiscoverResponse2.j;
                this.p = getDiscoverResponse2.k;
                XLog.v("home_entry", "getDiscoverResponse --onSuccess--ret= " + getDiscoverResponse2.f1279a + "--mEntranceRevision = " + this.p + "--mHomeEntryData= " + this.o);
            }
            if (!(b2 == null || b2.size() == 0)) {
                this.d.addAll(b2);
            }
            System.currentTimeMillis();
            this.g.b(arrayList);
            this.j = arVar;
            if (LaunchSpeedSTManager.d().b(i2)) {
                LaunchSpeedSTManager.d().a(LaunchSpeedSTManager.TYPE_TIME_POINT.HomeEngine_SmartCardCache_End);
            }
            this.mCallbacks.broadcast(new ah(this, arVar, i2, z, b2));
            if (arVar.c) {
                LaunchSpeedSTManager.d().f();
                if (z) {
                    this.m.a(arVar, 3000);
                } else {
                    this.m.a(arVar);
                }
            }
            TemporaryThreadManager.get().startDelayed(new ai(this, getDiscoverRequest2, getDiscoverResponse2, z, getSmartCardsResponse2), 3000);
            if (LaunchSpeedSTManager.d().b(i2)) {
                AstApp.i().j().sendMessageDelayed(AstApp.i().j().obtainMessage(EventDispatcherEnum.UI_EVENT_FIRST_PAGE_DATA_LOAD), 800);
            }
        } else if (this.m.d() == null || this.j.b == this.m.d().b) {
            XLog.d("GetHomeEngine", "getHomeEngine.onRequestSuccessed save nextpagerloader data.pager context" + arVar);
            com.tencent.assistant.manager.i.y().a(this.m.d() != null ? this.m.d().f3909a : null, getDiscoverResponse2);
            this.m.a(getDiscoverResponse2.c(), b2, arrayList, arVar.c, arVar);
        }
    }

    /* access modifiers changed from: protected */
    public void onRequestFailed(int i2, int i3, List<RequestResponePair> list) {
        GetDiscoverRequest getDiscoverRequest;
        this.h = -1;
        XLog.d("GetHomeEngine", "on request fail,seq:" + i2);
        if (i2 == this.m.b()) {
            this.m.h();
        } else {
            this.i = i3;
            GetDiscoverRequest getDiscoverRequest2 = null;
            for (RequestResponePair next : list) {
                if (next.request instanceof GetDiscoverRequest) {
                    getDiscoverRequest = (GetDiscoverRequest) next.request;
                } else {
                    getDiscoverRequest = getDiscoverRequest2;
                }
                getDiscoverRequest2 = getDiscoverRequest;
            }
            if (getDiscoverRequest2 != null) {
                if (getDiscoverRequest2.b.length == 0) {
                    TemporaryThreadManager.get().start(new aj(this, i3, i2));
                } else {
                    notifyDataChangedInMainThread(new al(this, i2, i3));
                }
            } else {
                return;
            }
        }
        if (LaunchSpeedSTManager.d().b(i2)) {
            AstApp.i().j().sendMessageDelayed(AstApp.i().j().obtainMessage(EventDispatcherEnum.UI_EVENT_FIRST_PAGE_DATA_LOAD), 800);
        }
    }

    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i2, JceStruct jceStruct, JceStruct jceStruct2) {
    }

    /* access modifiers changed from: protected */
    public void onRequestFailed(int i2, int i3, JceStruct jceStruct, JceStruct jceStruct2) {
    }

    /* access modifiers changed from: private */
    public boolean b(int i2) {
        boolean z = false;
        GetDiscoverResponse a2 = com.tencent.assistant.manager.i.y().a((byte[]) null);
        GetSmartCardsResponse a3 = com.tencent.assistant.manager.i.y().a();
        if (!(a2 == null || a3 == null)) {
            ArrayList<Banner> b2 = a2.b();
            ArrayList<SimpleAppModel> b3 = k.b(a2.b);
            ArrayList<SmartCardWrapper> arrayList = a3.b;
            if (!(b3 == null || b3.size() == 0)) {
                ar arVar = this.j;
                if (1 == a2.f) {
                    z = true;
                }
                arVar.c = z;
                this.j.f3909a = a2.d;
                this.j.a(a(-1));
                if (this.c == null) {
                    this.c = new ArrayList<>();
                }
                this.c.clear();
                this.c.addAll(b2);
                if (this.d == null) {
                    this.d = new ArrayList<>();
                }
                this.d.clear();
                this.d.addAll(b3);
                this.g.a(arrayList);
                this.l = a2.c();
                this.k.b(this.l);
                this.e = a2.e();
                this.f = a2.i;
                this.o = a2.g();
                this.p = a2.h();
                XLog.v("home_entry", "getDiscoverResponse --load cache--mEntranceRevision= " + this.p + "--mHomeEntryData= " + this.o);
                notifyDataChangedInMainThread(new am(this, i2, b3));
                if (!this.j.c) {
                    return true;
                }
                this.m.a(this.j);
                return true;
            }
        }
        return false;
    }

    public int c() {
        return this.i;
    }

    public boolean d() {
        return this.d != null && this.d.size() > 0;
    }

    public ar e() {
        return this.j;
    }

    public com.tencent.assistant.smartcard.b.c f() {
        return this.g;
    }

    public ArrayList<SimpleAppModel> g() {
        return this.d;
    }

    public ArrayList<Banner> h() {
        return this.c;
    }

    public b i() {
        return this.k;
    }

    public long j() {
        return this.n;
    }

    public EntranceTemplate k() {
        return this.o;
    }

    public long l() {
        return this.p;
    }
}
