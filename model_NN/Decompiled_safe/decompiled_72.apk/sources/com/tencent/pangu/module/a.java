package com.tencent.pangu.module;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.model.c;
import com.tencent.assistant.module.BaseEngine;
import com.tencent.assistant.module.update.j;
import com.tencent.assistant.protocol.jce.AppDetailParam;
import com.tencent.assistant.protocol.jce.AppDetailWithComment;
import com.tencent.assistant.protocol.jce.GetAppDetailRequest;
import com.tencent.assistant.protocol.jce.GetAppDetailResponse;
import com.tencent.connect.common.Constants;
import java.util.ArrayList;
import java.util.Iterator;

/* compiled from: ProGuard */
public class a extends BaseEngine<com.tencent.pangu.module.a.a> {
    public synchronized d a(SimpleAppModel simpleAppModel, byte b) {
        return a(simpleAppModel, b, Constants.STR_EMPTY, (byte) 0, (byte) 0);
    }

    public synchronized d a(SimpleAppModel simpleAppModel, byte b, String str, byte b2, byte b3) {
        return a(simpleAppModel, null, b, str, b2, b3);
    }

    public synchronized d a(SimpleAppModel simpleAppModel, LocalApkInfo localApkInfo, byte b, String str, byte b2, byte b3) {
        d dVar;
        if (simpleAppModel == null) {
            dVar = null;
        } else {
            dVar = new d();
            AppDetailParam appDetailParam = new AppDetailParam();
            appDetailParam.f1148a = simpleAppModel.f938a;
            appDetailParam.b = simpleAppModel.c;
            appDetailParam.c = simpleAppModel.m;
            appDetailParam.h = simpleAppModel.g;
            appDetailParam.g = simpleAppModel.b;
            appDetailParam.i = simpleAppModel.ac;
            appDetailParam.k = simpleAppModel.ad;
            if (appDetailParam.i == null) {
                appDetailParam.i = Constants.STR_EMPTY;
            }
            appDetailParam.j = b;
            appDetailParam.o = "wx3909f6add1206543";
            appDetailParam.m = b2;
            appDetailParam.n = b3;
            appDetailParam.l = str;
            if (localApkInfo != null) {
                appDetailParam.e = localApkInfo.manifestMd5;
                appDetailParam.d = localApkInfo.mVersionCode;
            }
            ArrayList arrayList = new ArrayList();
            arrayList.add(appDetailParam);
            dVar.b = send(new GetAppDetailRequest(arrayList));
        }
        return dVar;
    }

    /* access modifiers changed from: protected */
    public synchronized c a(GetAppDetailRequest getAppDetailRequest, ArrayList<AppDetailWithComment> arrayList) {
        LocalApkInfo localApkInfo;
        c cVar = null;
        synchronized (this) {
            if (!(arrayList == null || getAppDetailRequest == null)) {
                if (getAppDetailRequest.a().size() != 0) {
                    Iterator<AppDetailWithComment> it = arrayList.iterator();
                    while (it.hasNext()) {
                        c cVar2 = new c(it.next());
                        if (!(cVar2.b() == null || (localApkInfo = ApkResourceManager.getInstance().getLocalApkInfo(cVar2.b())) == null)) {
                            cVar2.a(localApkInfo);
                        }
                        cVar = cVar2;
                    }
                }
            }
        }
        return cVar;
    }

    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i, JceStruct jceStruct, JceStruct jceStruct2) {
        GetAppDetailResponse getAppDetailResponse = (GetAppDetailResponse) jceStruct2;
        c a2 = a((GetAppDetailRequest) jceStruct, getAppDetailResponse.b);
        if (a2 != null) {
            if (!(a2 == null || a2.f941a == null || a2.f941a.d == null || a2.f941a.d == null)) {
                ArrayList arrayList = new ArrayList();
                arrayList.add(a2.f941a.d);
                j.b().a(arrayList);
            }
            notifyDataChangedInMainThread(new b(this, i, a2, getAppDetailResponse.c));
        }
    }

    /* access modifiers changed from: protected */
    public void onRequestFailed(int i, int i2, JceStruct jceStruct, JceStruct jceStruct2) {
        notifyDataChangedInMainThread(new c(this, i, i2));
    }
}
