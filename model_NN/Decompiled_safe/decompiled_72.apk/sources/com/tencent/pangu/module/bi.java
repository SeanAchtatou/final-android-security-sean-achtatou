package com.tencent.pangu.module;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.module.callback.CallbackHelper;
import com.tencent.assistant.protocol.jce.SourceCheckRequest;
import com.tencent.assistant.protocol.jce.SourceCheckResponse;
import com.tencent.pangu.module.a.p;

/* compiled from: ProGuard */
class bi implements CallbackHelper.Caller<p> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ JceStruct f3926a;
    final /* synthetic */ int b;
    final /* synthetic */ SourceCheckResponse c;
    final /* synthetic */ bh d;

    bi(bh bhVar, JceStruct jceStruct, int i, SourceCheckResponse sourceCheckResponse) {
        this.d = bhVar;
        this.f3926a = jceStruct;
        this.b = i;
        this.c = sourceCheckResponse;
    }

    /* renamed from: a */
    public void call(p pVar) {
        if (((SourceCheckRequest) this.f3926a).b == 1) {
            pVar.a(this.b, 0, this.c.b, this.c.a(), this.c.e);
            return;
        }
        pVar.a(this.b, 0, this.c.b, this.c.a(), this.d.a(this.c));
    }
}
