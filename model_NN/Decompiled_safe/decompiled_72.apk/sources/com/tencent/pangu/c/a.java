package com.tencent.pangu.c;

import android.content.Context;
import com.qq.AppService.AstApp;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.manager.q;
import com.tencent.assistant.manager.t;
import com.tencent.pangu.activity.ShareBaseActivity;
import com.tencent.pangu.component.ShareAppBar;
import com.tencent.pangu.component.bm;
import com.tencent.pangu.model.ShareAppModel;
import com.tencent.pangu.model.ShareBaseModel;

/* compiled from: ProGuard */
public class a implements bm {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public ShareAppBar f3471a;
    private ShareBaseActivity b;
    private q c;
    private ShareAppModel d;
    private ShareBaseModel e;
    private boolean f = true;
    private String[] g = null;
    private int h = 0;
    private UIEventListener i = new b(this);

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.c.e.a(android.content.Context, com.tencent.pangu.model.ShareAppModel, boolean):void
     arg types: [com.tencent.pangu.activity.ShareBaseActivity, com.tencent.pangu.model.ShareAppModel, int]
     candidates:
      com.tencent.pangu.c.e.a(android.app.Activity, com.tencent.pangu.model.ShareAppModel, boolean):void
      com.tencent.pangu.c.e.a(android.app.Activity, com.tencent.pangu.model.ShareBaseModel, boolean):void
      com.tencent.pangu.c.e.a(android.content.Context, com.tencent.pangu.model.ShareBaseModel, boolean):void
      com.tencent.pangu.c.e.a(android.content.Context, com.tencent.pangu.model.ShareAppModel, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.c.e.a(android.content.Context, com.tencent.pangu.model.ShareBaseModel, boolean):void
     arg types: [com.tencent.pangu.activity.ShareBaseActivity, com.tencent.pangu.model.ShareBaseModel, int]
     candidates:
      com.tencent.pangu.c.e.a(android.app.Activity, com.tencent.pangu.model.ShareAppModel, boolean):void
      com.tencent.pangu.c.e.a(android.app.Activity, com.tencent.pangu.model.ShareBaseModel, boolean):void
      com.tencent.pangu.c.e.a(android.content.Context, com.tencent.pangu.model.ShareAppModel, boolean):void
      com.tencent.pangu.c.e.a(android.content.Context, com.tencent.pangu.model.ShareBaseModel, boolean):void */
    public void c() {
        e i2 = i();
        if (i2 != null) {
            if (this.g != null) {
                i2.a(this.g);
            }
            if (this.f) {
                i2.a((Context) this.b, this.d, false);
            } else {
                i2.a((Context) this.b, this.e, false);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.c.e.a(android.content.Context, com.tencent.pangu.model.ShareAppModel, boolean):void
     arg types: [com.tencent.pangu.activity.ShareBaseActivity, com.tencent.pangu.model.ShareAppModel, int]
     candidates:
      com.tencent.pangu.c.e.a(android.app.Activity, com.tencent.pangu.model.ShareAppModel, boolean):void
      com.tencent.pangu.c.e.a(android.app.Activity, com.tencent.pangu.model.ShareBaseModel, boolean):void
      com.tencent.pangu.c.e.a(android.content.Context, com.tencent.pangu.model.ShareBaseModel, boolean):void
      com.tencent.pangu.c.e.a(android.content.Context, com.tencent.pangu.model.ShareAppModel, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.c.e.a(android.content.Context, com.tencent.pangu.model.ShareBaseModel, boolean):void
     arg types: [com.tencent.pangu.activity.ShareBaseActivity, com.tencent.pangu.model.ShareBaseModel, int]
     candidates:
      com.tencent.pangu.c.e.a(android.app.Activity, com.tencent.pangu.model.ShareAppModel, boolean):void
      com.tencent.pangu.c.e.a(android.app.Activity, com.tencent.pangu.model.ShareBaseModel, boolean):void
      com.tencent.pangu.c.e.a(android.content.Context, com.tencent.pangu.model.ShareAppModel, boolean):void
      com.tencent.pangu.c.e.a(android.content.Context, com.tencent.pangu.model.ShareBaseModel, boolean):void */
    public void d() {
        e i2 = i();
        if (i2 != null) {
            if (this.g != null) {
                i2.a(this.g);
            }
            if (this.f) {
                i2.a((Context) this.b, this.d, true);
            } else {
                i2.a((Context) this.b, this.e, true);
            }
        }
    }

    public void b() {
        e i2 = i();
        if (i2 != null) {
            if (this.g != null) {
                i2.a(this.g);
            }
            if (this.f) {
                i2.a(this.b, this.d);
            } else {
                i2.a(this.b, this.e);
            }
        }
    }

    public void a() {
        e i2 = i();
        if (i2 != null) {
            if (this.g != null) {
                i2.a(this.g);
            }
            if (this.f) {
                i2.b(this.b, this.d);
            } else {
                i2.b(this.b, this.e);
            }
        }
    }

    public a(ShareBaseActivity shareBaseActivity, ShareAppBar shareAppBar) {
        this.b = shareBaseActivity;
        this.f3471a = shareAppBar;
        this.f3471a.a(this);
    }

    private void g() {
        this.c = new c(this);
        t.a().a(this.c);
    }

    private void h() {
        t.a().b(this.c);
    }

    public void a(ShareAppModel shareAppModel) {
        this.d = shareAppModel;
        this.f = true;
    }

    public void a(ShareBaseModel shareBaseModel) {
        this.e = shareBaseModel;
        this.f = false;
    }

    public void a(int i2) {
        this.h = i2;
    }

    public void e() {
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_INSTALL, this.i);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_UNINSTALL, this.i);
        i().b();
        if (this.f3471a.getVisibility() == 0) {
            this.f3471a.a();
        }
        g();
    }

    public void f() {
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_INSTALL, this.i);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_UNINSTALL, this.i);
        i().a();
        h();
    }

    private e i() {
        if (this.h == 0) {
            return this.b.E();
        }
        return this.b.b(this.h);
    }
}
