package com.tencent.pangu.model;

import com.tencent.assistant.utils.FileUtil;
import com.tencent.assistantv2.st.model.StatInfo;
import java.io.File;

/* compiled from: ProGuard */
public abstract class AbstractDownloadInfo {
    public String k;
    public String l;
    public String m;
    public long n;
    public long o;
    public long p;
    public String q;
    public String r;
    public DownState s = DownState.INIT;
    public int t = 0;
    public a u = new a();
    public StatInfo v = new StatInfo();

    /* compiled from: ProGuard */
    public enum DownState {
        INIT,
        DOWNLOADING,
        QUEUING,
        SUCC,
        FAIL,
        PAUSED,
        DELETE,
        WAITTING_FOR_WIFI
    }

    public abstract String b();

    public int c() {
        int i;
        if (this.u.f3881a > 0 && (i = (int) ((((double) this.u.b) * 100.0d) / ((double) this.u.f3881a))) != 0) {
            return i;
        }
        return 1;
    }

    public boolean d() {
        String str = b() + File.separator + this.k;
        if (!FileUtil.isFileExists(str)) {
            return false;
        }
        this.r = str;
        this.p = System.currentTimeMillis();
        this.s = DownState.SUCC;
        this.t = 0;
        return true;
    }

    public void e() {
        if (this.s == DownState.DOWNLOADING || this.s == DownState.QUEUING) {
            this.s = DownState.PAUSED;
        }
    }

    public boolean f() {
        if (this.s == DownState.SUCC) {
            if (new File(this.r).exists()) {
                return true;
            }
            if (this.u != null) {
                this.u.b = 0;
            }
        }
        return false;
    }
}
