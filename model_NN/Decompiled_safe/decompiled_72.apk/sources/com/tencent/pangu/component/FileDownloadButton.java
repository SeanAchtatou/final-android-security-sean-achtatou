package com.tencent.pangu.component;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.util.AttributeSet;
import android.widget.Button;
import com.qq.AppService.AstApp;
import com.qq.ndk.NativeFileObject;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.dialog.DialogUtils;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import com.tencent.pangu.mediadownload.FileOpenSelector;
import com.tencent.pangu.mediadownload.e;
import com.tencent.pangu.mediadownload.o;
import com.tencent.pangu.model.AbstractDownloadInfo;
import com.tencent.pangu.model.d;
import java.util.List;

/* compiled from: ProGuard */
public class FileDownloadButton extends Button {

    /* renamed from: a  reason: collision with root package name */
    private Context f3492a;
    /* access modifiers changed from: private */
    public d b;

    public FileDownloadButton(Context context) {
        this(context, null);
    }

    public FileDownloadButton(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f3492a = context;
        b();
    }

    private void b() {
        setHeight(this.f3492a.getResources().getDimensionPixelSize(R.dimen.download_button_height));
        setMinWidth(this.f3492a.getResources().getDimensionPixelSize(R.dimen.download_button_minwidth_2));
        setGravity(17);
        setTextSize(0, (float) this.f3492a.getResources().getDimensionPixelSize(R.dimen.download_button_font_size));
        setSingleLine(true);
    }

    public void a(d dVar, STInfoV2 sTInfoV2) {
        this.b = dVar;
        if (this.b != null) {
            a();
            a(sTInfoV2);
        }
    }

    public void a(STInfoV2 sTInfoV2) {
        if (this.b != null) {
            setOnClickListener(new o(this, sTInfoV2));
        }
    }

    public void a() {
        if (this.b != null) {
            a(this.b.s);
        }
    }

    private void a(AbstractDownloadInfo.DownState downState) {
        r b2 = b(downState);
        setTextColor(this.f3492a.getResources().getColor(b2.b));
        a(this.f3492a.getResources().getString(b2.f3688a));
        try {
            setBackgroundDrawable(this.f3492a.getResources().getDrawable(b2.c));
        } catch (Throwable th) {
        }
    }

    private r b(AbstractDownloadInfo.DownState downState) {
        r rVar = new r(null);
        rVar.c = R.drawable.state_bg_common_selector;
        rVar.b = R.color.state_normal;
        rVar.f3688a = R.string.appbutton_unknown;
        switch (q.f3687a[downState.ordinal()]) {
            case 1:
                rVar.f3688a = R.string.appbutton_download;
                break;
            case 2:
                rVar.f3688a = R.string.downloading_display_pause;
                break;
            case 3:
                rVar.f3688a = R.string.queuing;
                break;
            case 4:
            case 5:
            case 6:
                rVar.f3688a = R.string.appbutton_continuing;
                rVar.b = R.color.state_install;
                rVar.c = R.drawable.state_bg_install_selector;
                break;
            case 7:
                rVar.f3688a = R.string.filebutton_open;
                break;
            default:
                rVar.f3688a = R.string.appbutton_unknown;
                break;
        }
        return rVar;
    }

    private void a(String str) {
        if (str.length() == 4) {
            setMinWidth(this.f3492a.getResources().getDimensionPixelSize(R.dimen.download_button_minwidth_4));
        } else {
            setMinWidth(this.f3492a.getResources().getDimensionPixelSize(R.dimen.download_button_minwidth_2));
        }
        setText(str);
    }

    /* access modifiers changed from: private */
    public void b(d dVar, STInfoV2 sTInfoV2) {
        if (dVar != null) {
            switch (q.f3687a[dVar.s.ordinal()]) {
                case 1:
                case 4:
                case 6:
                    e.c().a(dVar);
                    return;
                case 2:
                case 3:
                    e.c().a(dVar.m);
                    return;
                case 5:
                default:
                    return;
                case 7:
                    o b2 = FileOpenSelector.b(dVar.r);
                    if (b2 != null) {
                        Intent intent = b2.f3866a;
                        List<ResolveInfo> queryIntentActivities = getContext().getPackageManager().queryIntentActivities(intent, NativeFileObject.S_IFIFO);
                        if ((!b2.c || queryIntentActivities == null || queryIntentActivities.size() <= 0) && b2.c) {
                            p pVar = new p(this, b2);
                            pVar.titleRes = getContext().getString(R.string.file_down_tips);
                            pVar.contentRes = getContext().getString(R.string.file_down_tips_content);
                            pVar.lBtnTxtRes = getContext().getString(R.string.file_down_cancel);
                            pVar.rBtnTxtRes = getContext().getString(R.string.file_down_search);
                            DialogUtils.show2BtnDialog(pVar);
                            l.a(new STInfoV2(STConst.ST_PAGE_DOWNLOAD_FILE_CANT_OPEN, a.a(STConst.ST_DEFAULT_SLOT, 0), AstApp.m() != null ? AstApp.m().f() : 2000, STConst.ST_DEFAULT_SLOT, 100));
                            return;
                        }
                        getContext().startActivity(intent);
                        return;
                    }
                    return;
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(int i, int i2, String str, int i3) {
        l.a(new STInfoV2(i, str, i2, STConst.ST_DEFAULT_SLOT, i3));
    }
}
