package com.tencent.pangu.component.appdetail;

import android.content.Context;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ViewConfiguration;
import android.widget.ImageView;
import android.widget.ScrollView;
import com.tencent.android.qqdownloader.R;

/* compiled from: ProGuard */
public class AppdetailScrollView extends ScrollView {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public int f3536a = 300;
    private int b = 10;
    private float c;
    private float d;
    private float e;
    private float f;
    private float g;
    private float h;
    /* access modifiers changed from: private */
    public bd i;
    private float j;
    private float k;
    private float l;
    private float m;
    private Context n;
    private GestureDetector o;
    /* access modifiers changed from: private */
    public volatile boolean p = false;

    public AppdetailScrollView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        a(context);
    }

    public AppdetailScrollView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context);
    }

    public AppdetailScrollView(Context context) {
        super(context);
        a(context);
    }

    private void a(Context context) {
        this.n = context;
        this.o = new GestureDetector(new x(this));
        this.b = ViewConfiguration.get(context).getScaledTouchSlop();
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case 0:
                this.d = 0.0f;
                this.c = 0.0f;
                this.h = 0.0f;
                this.g = 0.0f;
                this.e = motionEvent.getX();
                this.f = motionEvent.getY();
                this.j = motionEvent.getX();
                this.k = motionEvent.getY();
                this.m = 0.0f;
                this.l = 0.0f;
                break;
            case 2:
                float x = motionEvent.getX();
                float y = motionEvent.getY();
                this.g += Math.abs(x - this.e);
                this.h += Math.abs(y - this.f);
                this.c += x - this.e;
                this.d += y - this.f;
                this.e = x;
                this.f = y;
                if (this.g > ((float) this.b) || this.h > ((float) this.b)) {
                    if (this.g >= this.h) {
                        return false;
                    }
                    if (this.g < this.h) {
                        return true;
                    }
                }
                break;
        }
        return super.onInterceptTouchEvent(motionEvent);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        this.o.onTouchEvent(motionEvent);
        switch (motionEvent.getAction()) {
            case 0:
                this.j = motionEvent.getX();
                this.k = motionEvent.getY();
                this.m = 0.0f;
                this.l = 0.0f;
                break;
            case 2:
                float x = motionEvent.getX();
                float y = motionEvent.getY();
                this.l += x - this.j;
                this.m += y - this.k;
                int i2 = (int) (y - this.k);
                this.j = x;
                this.k = y;
                if (this.p) {
                    return false;
                }
                if (i2 < 0) {
                    if (getScrollY() < this.f3536a || this.i == null) {
                        a(true);
                        return false;
                    }
                    this.i.a(i2);
                    return false;
                } else if (i2 >= 0) {
                    if (this.i == null || !this.i.a()) {
                        a(false);
                        return false;
                    }
                    this.i.a(i2);
                    return false;
                }
                break;
        }
        try {
            return super.onTouchEvent(motionEvent);
        } catch (Exception e2) {
            return false;
        }
    }

    public void a(bd bdVar) {
        this.i = bdVar;
    }

    /* access modifiers changed from: private */
    public void b(int i2) {
        scrollTo(getScrollX(), getScrollY() - i2);
    }

    public void a(int i2) {
        this.f3536a = i2;
    }

    public void a(boolean z) {
        if (!this.p) {
            post(new y(this, z));
            if (z) {
                findViewById(R.id.appdetail_tag_explain_view).setVisibility(8);
                ((ImageView) findViewById(R.id.appdetail_tag_expand_more_img)).setImageResource(R.drawable.icon_open);
            }
            this.p = true;
        }
    }
}
