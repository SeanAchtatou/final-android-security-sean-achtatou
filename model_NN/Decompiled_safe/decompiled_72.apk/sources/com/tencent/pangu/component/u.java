package com.tencent.pangu.component;

import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;

/* compiled from: ProGuard */
class u extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ PopUpContentView f3742a;

    u(PopUpContentView popUpContentView) {
        this.f3742a = popUpContentView;
    }

    public void onTMAClick(View view) {
        this.f3742a.e.v();
    }

    public STInfoV2 getStInfo() {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f3742a.j, 200);
        buildSTInfo.slotId = a.a("06", "001");
        return buildSTInfo;
    }
}
