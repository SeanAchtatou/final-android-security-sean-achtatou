package com.tencent.pangu.component.appdetail;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Shader;
import android.os.Build;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.ImageView;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.utils.XLog;
import com.tencent.connect.common.Constants;
import com.tencent.pangu.utils.a;
import com.tencent.smtt.sdk.WebView;
import java.text.DecimalFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@SuppressLint({"NewApi"})
/* compiled from: ProGuard */
public class ExchangeColorTextView extends ImageView {

    /* renamed from: a  reason: collision with root package name */
    private LinearGradient f3547a;
    private Paint b;
    private float c;
    private float d;
    private String e;
    private String f;
    private a g;
    private boolean h;

    private void a() {
        this.g = new a(this, "progress");
    }

    public ExchangeColorTextView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.c = 0.0f;
        this.e = Constants.STR_EMPTY;
        this.h = false;
        this.b = new Paint(1);
        a();
    }

    public ExchangeColorTextView(Context context) {
        this(context, null);
        a();
    }

    public void setText(String str) {
        this.f = str;
        if (b()) {
            this.g.a(a(this.f), 100.0f);
            return;
        }
        this.g.a();
        this.e = str;
        invalidate();
    }

    public void setTextWhiteLenth(float f2) {
        this.d = f2;
        if (((double) this.d) <= 0.05d) {
            this.d = 0.05f;
        }
        invalidate();
    }

    public void setOrangeMode(boolean z) {
        this.h = z;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int[], float[], android.graphics.Shader$TileMode):void}
     arg types: [int, int, float, int, int[], float[], android.graphics.Shader$TileMode]
     candidates:
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long, long, android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long[], float[], android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int[], float[], android.graphics.Shader$TileMode):void} */
    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (!this.h) {
            this.f3547a = new LinearGradient(0.0f, 0.0f, ((float) getWidth()) * this.d, 0.0f, new int[]{-1, getResources().getColor(R.color.appdetail_tag_text_color_blue_n)}, new float[]{0.99f, 1.0f}, Shader.TileMode.CLAMP);
        } else {
            this.f3547a = new LinearGradient(0.0f, 0.0f, ((float) getWidth()) * this.d, 0.0f, new int[]{-1, getResources().getColor(R.color.appdetail_btn_text_color_to_install)}, new float[]{0.99f, 1.0f}, Shader.TileMode.CLAMP);
        }
        if (Build.VERSION.SDK_INT > 7) {
            this.b.setShader(this.f3547a);
        } else {
            this.b.setColor((int) WebView.NIGHT_MODE_COLOR);
        }
        this.b.setTextSize((float) getResources().getDimensionPixelSize(R.dimen.app_detail_text_size_large));
        float measureText = this.b.measureText(this.e);
        canvas.drawText(this.e, (float) ((getWidth() - ((int) measureText)) >> 1), (float) getResources().getDimensionPixelSize(R.dimen.appdetail_procesd_exchange_padding_top), this.b);
        canvas.restore();
    }

    private boolean b() {
        return !TextUtils.isEmpty(this.f) && this.f.contains("下载中");
    }

    private float a(String str) {
        if (TextUtils.isEmpty(str)) {
            return 0.0f;
        }
        Matcher matcher = Pattern.compile("\\d+(\\.\\d+)?").matcher(str);
        if (!matcher.find()) {
            return 0.0f;
        }
        try {
            return Float.parseFloat(matcher.group(0));
        } catch (Exception e2) {
            XLog.e("ljh", "格式化错误");
            e2.printStackTrace();
            return 0.0f;
        }
    }

    public void setProgress(float f2) {
        String format = new DecimalFormat("0.00").format((double) f2);
        this.e = String.format(AstApp.i().getResources().getString(R.string.downloading_percent), format);
        invalidate();
    }
}
