package com.tencent.pangu.component.appdetail.process;

import android.graphics.drawable.Drawable;

/* compiled from: ProGuard */
public interface AppdetailActionUIListener {

    /* compiled from: ProGuard */
    public enum AuthType {
        f3604a,
        WX
    }

    void a(int i);

    void a(int i, int i2);

    void a(Drawable drawable);

    void a(String str);

    void a(String str, int i);

    void a(String str, String str2);

    void a(String str, String str2, String str3);

    void a(boolean z);

    void a(boolean z, AuthType authType);

    void b(int i);

    void b(boolean z);

    void c(int i);

    void c(boolean z);
}
