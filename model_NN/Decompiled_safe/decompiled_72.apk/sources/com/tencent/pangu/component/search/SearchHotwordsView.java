package com.tencent.pangu.component.search;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.protocol.jce.AdvancedHotWord;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.by;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import com.tencent.pangu.model.a.f;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class SearchHotwordsView extends LinearLayout {

    /* renamed from: a  reason: collision with root package name */
    private final String f3695a;
    private View b;
    private TextView c;
    private ImageView d;
    private LinearLayout e;
    private TextView f;
    private SearchExplicitHotWordView g;
    private g h;
    /* access modifiers changed from: private */
    public View.OnClickListener i;
    private f j;
    private boolean k;
    private boolean l;
    private boolean m;
    private View.OnClickListener n;
    private View.OnClickListener o;

    public SearchHotwordsView(Context context) {
        this(context, null);
    }

    public SearchHotwordsView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f3695a = "SearchHotwordsView";
        this.h = null;
        this.i = null;
        this.j = new f();
        this.k = false;
        this.l = false;
        this.m = false;
        this.n = new h(this);
        this.o = new i(this);
        a(context);
    }

    private void a(Context context) {
        inflate(context, R.layout.search_hot_words_view, this);
        setOrientation(1);
        int a2 = by.a(getContext(), 8.0f);
        setPadding(a2, 0, a2, 0);
        this.b = findViewById(R.id.container);
        this.b.setVisibility(8);
        this.c = (TextView) findViewById(R.id.title);
        this.d = (ImageView) findViewById(R.id.change);
        this.e = (LinearLayout) findViewById(R.id.content);
        this.f = (TextView) findViewById(R.id.more);
        this.g = (SearchExplicitHotWordView) findViewById(R.id.search_explicit_hot_view);
        this.k = false;
        this.l = false;
        this.m = false;
    }

    public void a(f fVar) {
        if (fVar != null) {
            this.j = (f) fVar.clone();
            this.j.a();
        } else {
            this.j = null;
        }
        b();
    }

    /* access modifiers changed from: private */
    public void b() {
        int i2;
        if (this.j == null || this.j.f() < 1) {
            this.b.setVisibility(8);
            return;
        }
        this.c.setText(this.j.b);
        if (!this.k) {
            this.k = true;
            a("101", 100);
        }
        this.f.setText(this.j.c);
        XLog.d("SearchHotwordsView", "hotwordsModel.moreUrl:" + this.j.d);
        this.f.setTag(R.id.search_hot_word_more_url, this.j.d);
        this.f.setOnClickListener(this.o);
        if (!this.m) {
            this.m = true;
            c("103", 100);
        }
        if (this.j.f() <= 1) {
            this.d.setVisibility(8);
        } else {
            this.d.setVisibility(0);
            if (!this.l) {
                this.l = true;
                b("102", 100);
            }
        }
        List<AdvancedHotWord> b2 = b(a(this.j.c()));
        if (b2 != null && b2.size() > 0) {
            this.e.removeAllViews();
            if (b2.size() / 3 >= 3) {
                i2 = 3;
            } else {
                i2 = 0;
            }
            if (i2 > 0) {
                this.b.setVisibility(0);
            }
            int d2 = this.j.d();
            int e2 = this.j.e();
            for (int i3 = 0; i3 < i2; i3++) {
                LinearLayout linearLayout = new LinearLayout(getContext());
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2);
                if (i3 > 0) {
                    layoutParams.topMargin = by.a(getContext(), 7.0f);
                }
                linearLayout.setLayoutParams(layoutParams);
                linearLayout.setOrientation(0);
                linearLayout.setWeightSum(3.0f);
                for (int i4 = 0; i4 < 3; i4++) {
                    LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(0, by.a(getContext(), 48.0f));
                    layoutParams2.weight = 1.0f;
                    SearchHotwordsItem searchHotwordsItem = new SearchHotwordsItem(getContext());
                    int i5 = (i3 * 3) + i4;
                    searchHotwordsItem.a((d2 * e2) + i5, b2.get(i5));
                    searchHotwordsItem.a(this.h);
                    if (i4 > 0) {
                        layoutParams2.leftMargin = by.a(getContext(), 7.5f);
                    }
                    searchHotwordsItem.setLayoutParams(layoutParams2);
                    linearLayout.addView(searchHotwordsItem);
                }
                this.e.addView(linearLayout);
            }
        }
        this.d.setOnClickListener(this.n);
        c();
    }

    private List<AdvancedHotWord> a(List<AdvancedHotWord> list) {
        AdvancedHotWord advancedHotWord;
        if (list == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        for (AdvancedHotWord next : list) {
            AdvancedHotWord advancedHotWord2 = new AdvancedHotWord();
            if (next != null) {
                advancedHotWord = (AdvancedHotWord) next.clone();
            } else {
                advancedHotWord = advancedHotWord2;
            }
            arrayList.add(advancedHotWord);
        }
        return arrayList;
    }

    private List<AdvancedHotWord> b(List<AdvancedHotWord> list) {
        AdvancedHotWord advancedHotWord;
        if (list != null) {
            ArrayList arrayList = new ArrayList();
            for (int i2 = 0; i2 < list.size(); i2++) {
                AdvancedHotWord advancedHotWord2 = list.get(i2);
                if (advancedHotWord2 != null && advancedHotWord2.i >= 1 && advancedHotWord2.i <= 5) {
                    arrayList.add(Integer.valueOf(i2));
                }
            }
            if (arrayList != null && arrayList.size() >= 3) {
                int intValue = ((Integer) arrayList.get(0)).intValue();
                int i3 = 1;
                int i4 = 1;
                while (i3 < arrayList.size() && i4 < 3) {
                    int intValue2 = ((Integer) arrayList.get(i3)).intValue();
                    if ((intValue2 - intValue) % 2 != 1 || intValue2 < 0 || intValue2 >= list.size()) {
                        i4++;
                    } else {
                        AdvancedHotWord advancedHotWord3 = list.get(intValue2);
                        if (advancedHotWord3 != null) {
                            advancedHotWord3.i = 0;
                        }
                    }
                    i3++;
                }
                while (i3 < arrayList.size()) {
                    int intValue3 = ((Integer) arrayList.get(i3)).intValue();
                    if (intValue3 >= 0 && intValue3 < list.size() && (advancedHotWord = list.get(intValue3)) != null) {
                        advancedHotWord.i = 0;
                    }
                    i3++;
                }
            }
        }
        return list;
    }

    private void c() {
    }

    public void a(View.OnClickListener onClickListener) {
        this.i = onClickListener;
    }

    public void b(View.OnClickListener onClickListener) {
        this.g.a(onClickListener);
    }

    public void a() {
        this.g.a();
    }

    public void a(g gVar) {
        this.h = gVar;
    }

    private void a(String str, int i2) {
        if (getContext() instanceof BaseActivity) {
            STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(getContext(), i2);
            buildSTInfo.slotId = a.a("04", str);
            l.a(buildSTInfo);
        }
    }

    private void b(String str, int i2) {
        if (getContext() instanceof BaseActivity) {
            STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(getContext(), i2);
            buildSTInfo.slotId = a.a("04", str);
            l.a(buildSTInfo);
        }
    }

    private void c(String str, int i2) {
        if (getContext() instanceof BaseActivity) {
            STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(getContext(), i2);
            buildSTInfo.slotId = a.a("04", str);
            l.a(buildSTInfo);
        }
    }
}
