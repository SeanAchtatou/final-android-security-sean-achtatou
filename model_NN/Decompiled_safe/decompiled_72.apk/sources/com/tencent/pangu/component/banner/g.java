package com.tencent.pangu.component.banner;

import android.graphics.Bitmap;
import com.tencent.assistant.manager.notification.a.a.e;
import com.tencent.assistant.utils.XLog;

/* compiled from: ProGuard */
class g implements e {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ f f3642a;

    g(f fVar) {
        this.f3642a = fVar;
    }

    public void a(Bitmap bitmap) {
        if (bitmap != null && !bitmap.isRecycled()) {
            XLog.d("banner", "**** onLoadImageFinish bgImgUrl=" + this.f3642a.f3641a.g);
            this.f3642a.c = bitmap;
        }
    }
}
