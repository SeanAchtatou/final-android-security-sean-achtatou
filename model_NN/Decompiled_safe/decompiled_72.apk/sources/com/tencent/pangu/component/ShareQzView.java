package com.tencent.pangu.component;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.RatingView;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.utils.at;
import com.tencent.assistant.utils.bm;
import com.tencent.pangu.model.ShareAppModel;
import com.tencent.pangu.model.ShareBaseModel;
import com.tencent.pangu.model.ShareModel;

/* compiled from: ProGuard */
public class ShareQzView extends RelativeLayout {
    /* access modifiers changed from: private */
    public static String m = "04";

    /* renamed from: a  reason: collision with root package name */
    private LayoutInflater f3506a;
    private TXImageView b;
    private TextView c;
    private RatingView d;
    private TextView e;
    /* access modifiers changed from: private */
    public EditText f;
    private Button g;
    private Button h;
    /* access modifiers changed from: private */
    public ShareAppModel i;
    /* access modifiers changed from: private */
    public ShareBaseModel j;
    /* access modifiers changed from: private */
    public bq k;
    /* access modifiers changed from: private */
    public Context l;
    private View.OnClickListener n = new bp(this);

    public ShareQzView(Context context) {
        super(context);
        this.l = context;
        this.f3506a = LayoutInflater.from(context);
        b();
    }

    public void a(bq bqVar) {
        this.k = bqVar;
    }

    public void a(ShareModel shareModel) {
        if (shareModel != null) {
            if (shareModel instanceof ShareAppModel) {
                this.i = (ShareAppModel) shareModel;
            }
            if (shareModel instanceof ShareBaseModel) {
                this.j = (ShareBaseModel) shareModel;
            }
            c();
        }
    }

    private void b() {
        this.f3506a.inflate((int) R.layout.dialog_share_to_qz, this);
        this.b = (TXImageView) findViewById(R.id.iv_icon);
        this.c = (TextView) findViewById(R.id.tv_app_name);
        this.d = (RatingView) findViewById(R.id.app_ratingview);
        this.e = (TextView) findViewById(R.id.tv_other_info);
        this.f = (EditText) findViewById(R.id.et_reason);
        this.g = (Button) findViewById(R.id.btn_cancel);
        this.h = (Button) findViewById(R.id.btn_submit);
        this.g.setOnClickListener(this.n);
        this.h.setOnClickListener(this.n);
    }

    private void c() {
        if (this.i != null) {
            this.b.updateImageView(this.i.f, R.drawable.pic_default_app, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
            this.c.setText(this.i.g);
            this.d.setRating(this.i.h);
            TextView textView = this.e;
            Context context = this.l;
            Object[] objArr = new Object[2];
            objArr[0] = TextUtils.isEmpty(this.i.f3879a) ? at.a(this.i.j) : this.i.f3879a;
            objArr[1] = bm.a(this.i.i, 0);
            textView.setText(context.getString(R.string.share_otherInfo, objArr));
        }
        if (this.j != null) {
            this.b.updateImageView(this.j.c, R.drawable.pic_default_app, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
            this.c.setText(this.j.f3880a);
            this.d.setVisibility(8);
            this.e.setText(this.j.b);
        }
    }
}
