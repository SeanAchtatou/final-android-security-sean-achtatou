package com.tencent.pangu.component.appdetail;

import android.graphics.drawable.Drawable;
import com.tencent.android.qqdownloader.R;
import com.tencent.pangu.component.appdetail.process.AppdetailActionUIListener;

/* compiled from: ProGuard */
class p implements AppdetailActionUIListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppdetailDownloadBar f3603a;

    p(AppdetailDownloadBar appdetailDownloadBar) {
        this.f3603a = appdetailDownloadBar;
    }

    public void a(String str, int i) {
        this.f3603a.a(str, i);
    }

    public void a(String str, String str2, String str3) {
        this.f3603a.a(str, str2, str3);
    }

    public void a(int i, int i2) {
        this.f3603a.a(i, i2);
    }

    public void a(int i) {
        if (this.f3603a.p != null) {
            this.f3603a.p.setTextColor(i);
        }
    }

    public void b(int i) {
        if (this.f3603a.l != null) {
            this.f3603a.l.setVisibility(i);
        }
    }

    public void c(int i) {
        if (this.f3603a.q != null) {
            this.f3603a.q.setVisibility(i);
        }
    }

    public void a(Drawable drawable) {
        if (this.f3603a.q != null && drawable != null) {
            this.f3603a.q.setBackgroundDrawable(drawable);
        }
    }

    public void a(boolean z) {
        if (this.f3603a.E == null) {
            return;
        }
        if (z) {
            this.f3603a.E.setVisibility(0);
        } else {
            this.f3603a.E.setVisibility(4);
        }
    }

    public void b(boolean z) {
        if (this.f3603a.F != null) {
            this.f3603a.F.setEnabled(z);
            if (z) {
                if (!(this.f3603a.H == null || this.f3603a.I == null)) {
                    this.f3603a.H.setImageResource(R.drawable.icon_wx);
                    this.f3603a.I.setText((int) R.string.wx_vie_number);
                    this.f3603a.I.setTextColor(this.f3603a.getResources().getColorStateList(17170443));
                }
                this.f3603a.F.setBackgroundResource(R.drawable.appdetail_bar_btn_selector_v5);
                return;
            }
            if (!(this.f3603a.H == null || this.f3603a.I == null)) {
                this.f3603a.H.setImageResource(R.drawable.icon_wx_disable);
                this.f3603a.I.setText((int) R.string.vie_number_end);
                this.f3603a.I.setTextColor(this.f3603a.getResources().getColorStateList(R.color.state_disable));
            }
            this.f3603a.F.setBackgroundResource(R.drawable.state_bg_disable);
        }
    }

    public void c(boolean z) {
        if (this.f3603a.G != null) {
            this.f3603a.G.setEnabled(z);
            if (z) {
                if (!(this.f3603a.J == null || this.f3603a.K == null)) {
                    this.f3603a.J.setImageResource(R.drawable.icon_qq);
                    this.f3603a.K.setText((int) R.string.qq_vie_number);
                    this.f3603a.K.setTextColor(this.f3603a.getResources().getColorStateList(17170443));
                }
                this.f3603a.G.setBackgroundResource(R.drawable.appdetail_bar_btn_selector_v5);
                return;
            }
            if (!(this.f3603a.J == null || this.f3603a.K == null)) {
                this.f3603a.J.setImageResource(R.drawable.icon_qq_disable);
                this.f3603a.K.setText((int) R.string.vie_number_end);
                this.f3603a.K.setTextColor(this.f3603a.getResources().getColorStateList(R.color.state_disable));
            }
            this.f3603a.G.setBackgroundResource(R.drawable.state_bg_disable);
        }
    }

    public void a(String str) {
    }

    public void a(boolean z, AppdetailActionUIListener.AuthType authType) {
        if (this.f3603a.q != null) {
            this.f3603a.q.setEnabled(z);
            this.f3603a.o.setVisibility(0);
            if (z) {
                if (!(this.f3603a.o == null || this.f3603a.p == null)) {
                    if (authType == AppdetailActionUIListener.AuthType.f3604a) {
                        this.f3603a.o.setImageResource(R.drawable.icon_qq);
                        this.f3603a.p.setText((int) R.string.qq_vie_number);
                    } else {
                        this.f3603a.o.setImageResource(R.drawable.icon_wx);
                        this.f3603a.p.setText((int) R.string.wx_vie_number);
                    }
                    this.f3603a.p.setTextColor(this.f3603a.getResources().getColorStateList(17170443));
                }
                this.f3603a.q.setBackgroundResource(R.drawable.appdetail_bar_btn_selector_v5);
                return;
            }
            if (!(this.f3603a.o == null || this.f3603a.p == null)) {
                this.f3603a.p.setText((int) R.string.vie_number_end);
                if (authType == AppdetailActionUIListener.AuthType.f3604a) {
                    this.f3603a.o.setImageResource(R.drawable.icon_qq_disable);
                } else {
                    this.f3603a.o.setImageResource(R.drawable.icon_wx_disable);
                }
                this.f3603a.p.setTextColor(this.f3603a.getResources().getColorStateList(R.color.state_disable));
            }
            this.f3603a.q.setBackgroundResource(R.drawable.state_bg_disable);
        }
    }

    public void a(String str, String str2) {
        this.f3603a.a(str, str2);
    }
}
