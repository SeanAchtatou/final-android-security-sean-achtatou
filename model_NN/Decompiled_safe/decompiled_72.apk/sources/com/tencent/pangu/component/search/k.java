package com.tencent.pangu.component.search;

import android.content.Context;
import com.tencent.pangu.component.search.ISearchResultPage;

/* compiled from: ProGuard */
public class k {

    /* renamed from: a  reason: collision with root package name */
    private ISearchResultPage f3707a = null;

    public ISearchResultPage a() {
        return this.f3707a;
    }

    public void a(Context context, j jVar) {
        if (jVar != null) {
            switch (l.f3708a[jVar.a().ordinal()]) {
                case 1:
                    if (this.f3707a == null || this.f3707a.h() != ISearchResultPage.PageType.NATIVE) {
                        this.f3707a = new NativeSearchResultPage(context);
                        this.f3707a.b(jVar.b());
                        this.f3707a.c(jVar.c());
                        return;
                    }
                    return;
                case 2:
                    if (this.f3707a == null || this.f3707a.h() != ISearchResultPage.PageType.WEB) {
                        this.f3707a = new WebSearchResultPage(context);
                        this.f3707a.b(jVar.b());
                        this.f3707a.c(jVar.c());
                        ((WebSearchResultPage) this.f3707a).c(jVar.d());
                        return;
                    }
                    return;
                default:
                    if (this.f3707a == null) {
                        this.f3707a = new NativeSearchResultPage(context);
                        this.f3707a.b(jVar.b());
                        this.f3707a.c(jVar.c());
                        return;
                    }
                    return;
            }
        }
    }
}
