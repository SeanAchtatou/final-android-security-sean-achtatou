package com.tencent.pangu.component.appdetail;

import android.text.TextUtils;
import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import com.tencent.pangu.activity.AppDetailActivityV5;
import com.tencent.pangu.link.b;

/* compiled from: ProGuard */
class an extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ap f3572a;
    final /* synthetic */ int b;
    final /* synthetic */ int c;
    final /* synthetic */ DetailGameNewsView d;

    an(DetailGameNewsView detailGameNewsView, ap apVar, int i, int i2) {
        this.d = detailGameNewsView;
        this.f3572a = apVar;
        this.b = i;
        this.c = i2;
    }

    public void onTMAClick(View view) {
        if (!this.f3572a.d && !TextUtils.isEmpty(this.f3572a.f)) {
            b.a(this.d.b, this.f3572a.f);
        } else if (this.f3572a.d && !TextUtils.isEmpty(this.f3572a.e)) {
            b.a(this.d.b, this.f3572a.e);
        }
    }

    public STInfoV2 getStInfo() {
        if (!(this.d.b instanceof AppDetailActivityV5)) {
            return null;
        }
        STInfoV2 t = ((AppDetailActivityV5) this.d.b).t();
        t.slotId = a.a(this.b == 1 ? "17" : "09", this.c + 1);
        t.actionId = 200;
        return t;
    }
}
