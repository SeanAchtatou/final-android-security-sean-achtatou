package com.tencent.pangu.component.appdetail;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;

/* compiled from: ProGuard */
public class CustomLinearLayout extends LinearLayout {
    public CustomLinearLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public CustomLinearLayout(Context context) {
        super(context);
    }
}
