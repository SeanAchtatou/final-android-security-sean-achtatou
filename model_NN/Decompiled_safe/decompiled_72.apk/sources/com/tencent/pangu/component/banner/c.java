package com.tencent.pangu.component.banner;

import com.tencent.assistant.manager.i;
import com.tencent.assistant.protocol.jce.GftGetGameGiftFlagResponse;
import com.tencent.assistant.protocol.jce.GftReportGameGiftCheckedRequest;
import com.tencent.game.d.aa;

/* compiled from: ProGuard */
class c implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ b f3639a;

    c(b bVar) {
        this.f3639a = bVar;
    }

    public void run() {
        aa.a().a(new GftReportGameGiftCheckedRequest());
        GftGetGameGiftFlagResponse r = i.y().r();
        if (r != null && r.f1364a == 0) {
            r.b = 0;
            i.y().a(r);
        }
    }
}
