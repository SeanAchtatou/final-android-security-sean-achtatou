package com.tencent.pangu.component.homeEntry;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.protocol.jce.EntranceTemplate;
import com.tencent.assistant.thumbnailCache.k;
import com.tencent.assistant.thumbnailCache.o;
import com.tencent.assistant.thumbnailCache.p;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.ah;

/* compiled from: ProGuard */
public class HomeEntryTemplateView extends LinearLayout implements p {

    /* renamed from: a  reason: collision with root package name */
    public EntranceTemplate f3672a;
    public long b;
    LinearLayout c = null;
    /* access modifiers changed from: private */
    public Context d;

    public HomeEntryTemplateView(Context context, EntranceTemplate entranceTemplate, long j) {
        super(context);
        this.d = context;
        this.b = j;
        this.f3672a = entranceTemplate;
        a(context);
    }

    public void a(Context context) {
        try {
            setBackgroundResource(R.drawable.bg_card_selector_padding);
            this.c = (LinearLayout) LayoutInflater.from(this.d).inflate((int) R.layout.home_entry_template_view, (ViewGroup) null);
            addView(this.c);
            setGravity(17);
            a();
            this.c.setOnClickListener(new d(this));
            ah.a().postDelayed(new e(this), 2000);
        } catch (Throwable th) {
            System.gc();
            throw new Exception("home entry view cteate exception!");
        }
    }

    public void a(View view) {
        this.c.addView(view);
    }

    public void a() {
        if (this.f3672a == null || TextUtils.isEmpty(this.f3672a.b)) {
            XLog.v("home_entry", "template---requestImg--template name:" + getClass().getSimpleName() + "--no image data");
            return;
        }
        Bitmap a2 = k.b().a(this.f3672a.b, 1, this);
        if (a2 != null && !a2.isRecycled()) {
            XLog.v("home_entry", "template---requestImg--template name:" + getClass().getSimpleName() + "--bitmap get");
            ah.a().postDelayed(new g(this, a2), 15);
        }
    }

    public void thumbnailRequestCompleted(o oVar) {
        if (oVar != null) {
            String c2 = oVar.c();
            if (!TextUtils.isEmpty(c2) && c2.endsWith(this.f3672a.b) && oVar.f != null && !oVar.f.isRecycled()) {
                setBackgroundResource(0);
                this.c.setBackgroundDrawable(new BitmapDrawable(oVar.f));
            }
        }
    }

    public void thumbnailRequestFailed(o oVar) {
    }
}
