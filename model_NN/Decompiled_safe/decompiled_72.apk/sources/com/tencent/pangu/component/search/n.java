package com.tencent.pangu.component.search;

import com.tencent.pangu.component.search.SearchResultTabPagesBase;

/* compiled from: ProGuard */
/* synthetic */ class n {

    /* renamed from: a  reason: collision with root package name */
    static final /* synthetic */ int[] f3709a = new int[SearchResultTabPagesBase.TabType.values().length];

    static {
        try {
            f3709a[SearchResultTabPagesBase.TabType.ALL.ordinal()] = 1;
        } catch (NoSuchFieldError e) {
        }
        try {
            f3709a[SearchResultTabPagesBase.TabType.APP.ordinal()] = 2;
        } catch (NoSuchFieldError e2) {
        }
        try {
            f3709a[SearchResultTabPagesBase.TabType.VIDEO.ordinal()] = 3;
        } catch (NoSuchFieldError e3) {
        }
        try {
            f3709a[SearchResultTabPagesBase.TabType.EBOOK.ordinal()] = 4;
        } catch (NoSuchFieldError e4) {
        }
        try {
            f3709a[SearchResultTabPagesBase.TabType.STARTPAGE.ordinal()] = 5;
        } catch (NoSuchFieldError e5) {
        }
    }
}
