package com.tencent.pangu.component.appdetail;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import com.tencent.connect.common.Constants;
import com.tencent.pangu.activity.AppDetailActivityV5;

/* compiled from: ProGuard */
class av extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Context f3580a;
    final /* synthetic */ HorizonImageListView b;

    av(HorizonImageListView horizonImageListView, Context context) {
        this.b = horizonImageListView;
        this.f3580a = context;
    }

    public void onTMAClick(View view) {
        String str = (String) ((ImageView) view).getTag();
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < this.b.c.size()) {
                String str2 = (String) this.b.c.get(i2);
                if (!str2.equals(str) || this.b.d == null) {
                    i = i2 + 1;
                } else {
                    this.b.d.a(i2, str2, view);
                    return;
                }
            } else {
                return;
            }
        }
    }

    public STInfoV2 getStInfo() {
        if (!(this.f3580a instanceof AppDetailActivityV5)) {
            return null;
        }
        STInfoV2 t = ((AppDetailActivityV5) this.f3580a).t();
        if (this.clickViewId >= 0) {
            t.slotId = a.a("07", this.clickViewId - 1);
        } else if (this.clickViewId == -1) {
            t.slotId = a.a(Constants.VIA_REPORT_TYPE_START_WAP, 0);
        }
        t.actionId = 200;
        return t;
    }
}
