package com.tencent.pangu.component;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.pangu.mediadownload.c;

/* compiled from: ProGuard */
public class BookReadButton extends Button {

    /* renamed from: a  reason: collision with root package name */
    private Context f3488a;
    /* access modifiers changed from: private */
    public c b;
    /* access modifiers changed from: private */
    public STInfoV2 c;

    public BookReadButton(Context context) {
        this(context, null);
    }

    public BookReadButton(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f3488a = context;
        b();
    }

    private void b() {
        setHeight(this.f3488a.getResources().getDimensionPixelSize(R.dimen.download_button_height));
        setMinWidth(this.f3488a.getResources().getDimensionPixelSize(R.dimen.download_button_minwidth_2));
        setGravity(17);
        setTextSize(0, (float) this.f3488a.getResources().getDimensionPixelSize(R.dimen.download_button_font_size));
        setSingleLine(true);
        c();
        a();
    }

    public void a(c cVar, STInfoV2 sTInfoV2) {
        this.b = cVar;
        this.c = sTInfoV2;
    }

    public void a() {
        setOnClickListener(new a(this));
    }

    private void c() {
        b d = d();
        setTextColor(this.f3488a.getResources().getColor(d.b));
        a(this.f3488a.getResources().getString(d.f3635a));
        setBackgroundDrawable(this.f3488a.getResources().getDrawable(d.c));
    }

    private b d() {
        b bVar = new b(null);
        bVar.c = R.drawable.state_bg_common_selector;
        bVar.b = R.color.state_normal;
        bVar.f3635a = R.string.bookbutton_play;
        return bVar;
    }

    private void a(String str) {
        if (str.length() == 4) {
            setMinWidth(this.f3488a.getResources().getDimensionPixelSize(R.dimen.download_button_minwidth_4));
        } else {
            setMinWidth(this.f3488a.getResources().getDimensionPixelSize(R.dimen.download_button_minwidth_2));
        }
        setText(str);
    }
}
