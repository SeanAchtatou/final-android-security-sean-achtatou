package com.tencent.pangu.a;

import android.os.Message;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.utils.TemporaryThreadManager;

/* compiled from: ProGuard */
class k implements UIEventListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ h f3297a;

    k(h hVar) {
        this.f3297a = hVar;
    }

    public void handleUIEvent(Message message) {
        switch (message.what) {
            case EventDispatcherEnum.UI_EVENT_SHOW_TREASURE_BOX_ENTRY:
                boolean unused = this.f3297a.g = true;
                this.f3297a.e.setVisibility(0);
                this.f3297a.f.setVisibility(0);
                return;
            case EventDispatcherEnum.UI_EVENT_DISMISS_TREASURE_BOX_ENTRY:
                boolean unused2 = this.f3297a.g = false;
                this.f3297a.e.setVisibility(8);
                this.f3297a.f.setVisibility(8);
                TemporaryThreadManager.get().start(new l(this));
                return;
            default:
                return;
        }
    }
}
