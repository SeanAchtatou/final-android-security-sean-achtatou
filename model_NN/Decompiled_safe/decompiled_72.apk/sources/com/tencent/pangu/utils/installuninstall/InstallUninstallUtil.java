package com.tencent.pangu.utils.installuninstall;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Pair;
import com.qq.AppService.AstApp;
import com.tencent.assistant.Global;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.m;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.r;
import com.tencent.assistant.utils.t;
import com.tencent.assistantv2.st.business.o;
import com.tencent.connect.common.Constants;
import com.tencent.nucleus.manager.root.e;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.concurrent.PriorityBlockingQueue;

/* compiled from: ProGuard */
public class InstallUninstallUtil {

    /* renamed from: a  reason: collision with root package name */
    private static Thread f3986a = new Thread(b, "chmod-thread");
    private static RequestQueue b = new RequestQueue();

    /* JADX WARNING: Code restructure failed: missing block: B:35:0x00c5, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00c8, code lost:
        r1.destroy();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00d1, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00d2, code lost:
        r9 = r0;
        r0 = false;
        r2 = null;
        r1 = r9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x00d7, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00d8, code lost:
        r2 = r1;
        r1 = r0;
        r0 = true;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00c5 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:1:0x0003] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00c8  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean a() {
        /*
            r1 = 0
            r3 = 1
            r2 = 0
            java.lang.ProcessBuilder r0 = new java.lang.ProcessBuilder     // Catch:{ Exception -> 0x00d1, all -> 0x00c5 }
            r4 = 1
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ Exception -> 0x00d1, all -> 0x00c5 }
            r5 = 0
            java.lang.String r6 = "su"
            r4[r5] = r6     // Catch:{ Exception -> 0x00d1, all -> 0x00c5 }
            r0.<init>(r4)     // Catch:{ Exception -> 0x00d1, all -> 0x00c5 }
            r4 = 0
            r0.redirectErrorStream(r4)     // Catch:{ Exception -> 0x00d1, all -> 0x00c5 }
            java.lang.Process r1 = r0.start()     // Catch:{ Exception -> 0x00d1, all -> 0x00c5 }
            java.io.DataOutputStream r0 = new java.io.DataOutputStream     // Catch:{ Exception -> 0x0063, all -> 0x00c5 }
            java.io.OutputStream r4 = r1.getOutputStream()     // Catch:{ Exception -> 0x0063, all -> 0x00c5 }
            r0.<init>(r4)     // Catch:{ Exception -> 0x0063, all -> 0x00c5 }
            java.io.BufferedReader r4 = new java.io.BufferedReader     // Catch:{ Exception -> 0x0063, all -> 0x00c5 }
            java.io.InputStreamReader r5 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x0063, all -> 0x00c5 }
            java.io.InputStream r6 = r1.getInputStream()     // Catch:{ Exception -> 0x0063, all -> 0x00c5 }
            java.lang.String r7 = "UTF-8"
            r5.<init>(r6, r7)     // Catch:{ Exception -> 0x0063, all -> 0x00c5 }
            r4.<init>(r5)     // Catch:{ Exception -> 0x0063, all -> 0x00c5 }
            if (r4 == 0) goto L_0x00dc
            if (r0 == 0) goto L_0x00dc
            r0.flush()     // Catch:{ Exception -> 0x0063, all -> 0x00c5 }
            java.lang.String r5 = "id\n"
            r0.writeBytes(r5)     // Catch:{ Exception -> 0x0063, all -> 0x00c5 }
            r0.flush()     // Catch:{ Exception -> 0x0063, all -> 0x00c5 }
            java.lang.String r5 = "exit\n"
            r0.writeBytes(r5)     // Catch:{ Exception -> 0x0063, all -> 0x00c5 }
            r0.flush()     // Catch:{ Exception -> 0x0063, all -> 0x00c5 }
            r5 = 45000(0xafc8, double:2.2233E-319)
            com.tencent.pangu.utils.installuninstall.aj r7 = new com.tencent.pangu.utils.installuninstall.aj     // Catch:{ Exception -> 0x0063, all -> 0x00c5 }
            r8 = 0
            r7.<init>(r1)     // Catch:{ Exception -> 0x0063, all -> 0x00c5 }
            r7.start()     // Catch:{ Exception -> 0x0063, all -> 0x00c5 }
            r7.join(r5)     // Catch:{ Exception -> 0x0063, all -> 0x00c5 }
            java.lang.Integer r5 = r7.b     // Catch:{ Exception -> 0x0063, all -> 0x00c5 }
            if (r5 != 0) goto L_0x0078
            java.util.concurrent.TimeoutException r0 = new java.util.concurrent.TimeoutException     // Catch:{ Exception -> 0x0063, all -> 0x00c5 }
            r0.<init>()     // Catch:{ Exception -> 0x0063, all -> 0x00c5 }
            throw r0     // Catch:{ Exception -> 0x0063, all -> 0x00c5 }
        L_0x0063:
            r0 = move-exception
            r9 = r0
            r0 = r2
            r2 = r1
            r1 = r9
        L_0x0068:
            r1.printStackTrace()     // Catch:{ all -> 0x00ce }
            if (r2 == 0) goto L_0x0070
            r2.destroy()
        L_0x0070:
            com.tencent.assistant.m r1 = com.tencent.assistant.m.a()
            r1.a(r0)
            return r0
        L_0x0078:
            r0.close()     // Catch:{ Exception -> 0x0063, all -> 0x00c5 }
            r0 = 1024(0x400, float:1.435E-42)
            char[] r0 = new char[r0]     // Catch:{ Exception -> 0x0063, all -> 0x00c5 }
            int r4 = r4.read(r0)     // Catch:{ Exception -> 0x0063, all -> 0x00c5 }
            java.lang.String r5 = ""
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0063, all -> 0x00c5 }
            r5.<init>()     // Catch:{ Exception -> 0x0063, all -> 0x00c5 }
            r6 = -1
            if (r4 == r6) goto L_0x00b2
            r6 = 0
            java.lang.StringBuilder r0 = r5.append(r0, r6, r4)     // Catch:{ Exception -> 0x0063, all -> 0x00c5 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0063, all -> 0x00c5 }
        L_0x0096:
            boolean r4 = android.text.TextUtils.isEmpty(r0)     // Catch:{ Exception -> 0x0063, all -> 0x00c5 }
            if (r4 != 0) goto L_0x00cc
            java.lang.String r4 = "uid=0"
            boolean r0 = r0.contains(r4)     // Catch:{ Exception -> 0x0063, all -> 0x00c5 }
            if (r0 == 0) goto L_0x00cc
            com.tencent.nucleus.manager.root.e r0 = com.tencent.nucleus.manager.root.e.a()     // Catch:{ Exception -> 0x00d7, all -> 0x00c5 }
            r0.b()     // Catch:{ Exception -> 0x00d7, all -> 0x00c5 }
            r0 = r3
        L_0x00ac:
            if (r1 == 0) goto L_0x0070
            r1.destroy()
            goto L_0x0070
        L_0x00b2:
            r0 = 1024(0x400, float:1.435E-42)
            byte[] r4 = new byte[r0]     // Catch:{ Exception -> 0x0063, all -> 0x00c5 }
            java.io.InputStream r0 = r1.getErrorStream()     // Catch:{ Exception -> 0x0063, all -> 0x00c5 }
            int r5 = r0.read(r4)     // Catch:{ Exception -> 0x0063, all -> 0x00c5 }
            java.lang.String r0 = new java.lang.String     // Catch:{ Exception -> 0x0063, all -> 0x00c5 }
            r6 = 0
            r0.<init>(r4, r6, r5)     // Catch:{ Exception -> 0x0063, all -> 0x00c5 }
            goto L_0x0096
        L_0x00c5:
            r0 = move-exception
        L_0x00c6:
            if (r1 == 0) goto L_0x00cb
            r1.destroy()
        L_0x00cb:
            throw r0
        L_0x00cc:
            r0 = r2
            goto L_0x00ac
        L_0x00ce:
            r0 = move-exception
            r1 = r2
            goto L_0x00c6
        L_0x00d1:
            r0 = move-exception
            r9 = r0
            r0 = r2
            r2 = r1
            r1 = r9
            goto L_0x0068
        L_0x00d7:
            r0 = move-exception
            r2 = r1
            r1 = r0
            r0 = r3
            goto L_0x0068
        L_0x00dc:
            r0 = r2
            goto L_0x00ac
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.pangu.utils.installuninstall.InstallUninstallUtil.a():boolean");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:52:0x02aa, code lost:
        if (r6 != null) goto L_0x02ac;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x02ac, code lost:
        r6.destroy();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x02af, code lost:
        r2.f3997a = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x02f5, code lost:
        if (r6 == null) goto L_0x02af;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.tencent.pangu.utils.installuninstall.ak a(java.lang.String r12, int r13, int r14, java.lang.String r15) {
        /*
            com.tencent.pangu.utils.installuninstall.ak r2 = new com.tencent.pangu.utils.installuninstall.ak
            r2.<init>()
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "LD_LIBRARY_PATH=/vendor/lib:/system/lib pm install -r '"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.StringBuilder r0 = r0.append(r15)
            java.lang.String r1 = "'\n"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r5 = r0.toString()
            java.lang.String r0 = "million"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r3 = "root install["
            java.lang.StringBuilder r1 = r1.append(r3)
            java.lang.StringBuilder r1 = r1.append(r14)
            java.lang.String r3 = "] "
            java.lang.StringBuilder r1 = r1.append(r3)
            java.lang.StringBuilder r1 = r1.append(r15)
            java.lang.String r1 = r1.toString()
            com.tencent.assistant.utils.XLog.d(r0, r1)
            r4 = 0
            r0 = 2
            if (r14 != r0) goto L_0x0181
            r1 = 0
            boolean r0 = f(r15)     // Catch:{ Exception -> 0x02ff }
            if (r0 != 0) goto L_0x00f3
            java.io.File r0 = new java.io.File     // Catch:{ Exception -> 0x02ff }
            r0.<init>(r15)     // Catch:{ Exception -> 0x02ff }
            java.lang.String r3 = r0.getParent()     // Catch:{ Exception -> 0x02ff }
            java.io.File r6 = new java.io.File     // Catch:{ Exception -> 0x02ff }
            r6.<init>(r3)     // Catch:{ Exception -> 0x02ff }
            java.lang.String r6 = r6.getParent()     // Catch:{ Exception -> 0x02ff }
            java.io.File r7 = new java.io.File     // Catch:{ Exception -> 0x02ff }
            r7.<init>(r6)     // Catch:{ Exception -> 0x02ff }
            java.lang.String r7 = r7.getParent()     // Catch:{ Exception -> 0x02ff }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x02ff }
            r8.<init>()     // Catch:{ Exception -> 0x02ff }
            java.lang.String r9 = "chmod 777 "
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ Exception -> 0x02ff }
            java.lang.String r0 = r0.getAbsolutePath()     // Catch:{ Exception -> 0x02ff }
            java.lang.StringBuilder r0 = r8.append(r0)     // Catch:{ Exception -> 0x02ff }
            java.lang.String r8 = "\n"
            java.lang.StringBuilder r0 = r0.append(r8)     // Catch:{ Exception -> 0x02ff }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x02ff }
            r1 = 1
            com.tencent.nucleus.manager.root.e r8 = com.tencent.nucleus.manager.root.e.a()     // Catch:{ Exception -> 0x02ff }
            r8.b(r0)     // Catch:{ Exception -> 0x02ff }
            r1 = 2
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x02ff }
            r0.<init>()     // Catch:{ Exception -> 0x02ff }
            java.lang.String r8 = "chmod 777 "
            java.lang.StringBuilder r0 = r0.append(r8)     // Catch:{ Exception -> 0x02ff }
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ Exception -> 0x02ff }
            java.lang.String r3 = "\n"
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ Exception -> 0x02ff }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x02ff }
            r1 = 3
            com.tencent.nucleus.manager.root.e r3 = com.tencent.nucleus.manager.root.e.a()     // Catch:{ Exception -> 0x02ff }
            r3.b(r0)     // Catch:{ Exception -> 0x02ff }
            r1 = 4
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x02ff }
            r0.<init>()     // Catch:{ Exception -> 0x02ff }
            java.lang.String r3 = "chmod 777 "
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ Exception -> 0x02ff }
            java.lang.StringBuilder r0 = r0.append(r6)     // Catch:{ Exception -> 0x02ff }
            java.lang.String r3 = "\n"
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ Exception -> 0x02ff }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x02ff }
            r1 = 5
            com.tencent.nucleus.manager.root.e r3 = com.tencent.nucleus.manager.root.e.a()     // Catch:{ Exception -> 0x02ff }
            r3.b(r0)     // Catch:{ Exception -> 0x02ff }
            r1 = 6
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x02ff }
            r0.<init>()     // Catch:{ Exception -> 0x02ff }
            java.lang.String r3 = "chmod 777 "
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ Exception -> 0x02ff }
            java.lang.StringBuilder r0 = r0.append(r7)     // Catch:{ Exception -> 0x02ff }
            java.lang.String r3 = "\n"
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ Exception -> 0x02ff }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x02ff }
            r1 = 7
            com.tencent.nucleus.manager.root.e r3 = com.tencent.nucleus.manager.root.e.a()     // Catch:{ Exception -> 0x02ff }
            r3.b(r0)     // Catch:{ Exception -> 0x02ff }
            r1 = 8
        L_0x00f3:
            com.tencent.nucleus.manager.root.e r0 = com.tencent.nucleus.manager.root.e.a()     // Catch:{ Exception -> 0x02ff }
            android.util.Pair r6 = r0.a(r5)     // Catch:{ Exception -> 0x02ff }
            java.lang.Object r0 = r6.second     // Catch:{ Exception -> 0x02ff }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Exception -> 0x02ff }
            int r3 = r1 + 1
            java.lang.Object r1 = r6.first     // Catch:{ Exception -> 0x0157 }
            java.lang.Integer r1 = (java.lang.Integer) r1     // Catch:{ Exception -> 0x0157 }
            int r1 = r1.intValue()     // Catch:{ Exception -> 0x0157 }
            if (r1 != 0) goto L_0x0149
            boolean r1 = android.text.TextUtils.isEmpty(r0)     // Catch:{ Exception -> 0x0157 }
            if (r1 != 0) goto L_0x0147
            java.lang.String r1 = r0.toLowerCase()     // Catch:{ Exception -> 0x0157 }
            java.lang.String r4 = "success"
            boolean r1 = r1.contains(r4)     // Catch:{ Exception -> 0x0157 }
            if (r1 == 0) goto L_0x0147
            r1 = 1
        L_0x011e:
            r4 = r1
        L_0x011f:
            r2.f3997a = r4     // Catch:{ Exception -> 0x0157 }
            boolean r1 = android.text.TextUtils.isEmpty(r0)     // Catch:{ Exception -> 0x0157 }
            if (r1 != 0) goto L_0x0129
            r2.b = r0     // Catch:{ Exception -> 0x0157 }
        L_0x0129:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.StringBuilder r0 = r0.append(r5)
            java.lang.StringBuilder r0 = r0.append(r5)
            java.lang.String r1 = " result : "
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = r2.b
            java.lang.StringBuilder r0 = r0.append(r1)
            r0.toString()
        L_0x0145:
            r0 = r2
        L_0x0146:
            return r0
        L_0x0147:
            r1 = 0
            goto L_0x011e
        L_0x0149:
            java.lang.Object r1 = r6.first     // Catch:{ Exception -> 0x0157 }
            java.lang.Integer r1 = (java.lang.Integer) r1     // Catch:{ Exception -> 0x0157 }
            int r1 = r1.intValue()     // Catch:{ Exception -> 0x0157 }
            if (r1 <= 0) goto L_0x011f
            r1 = 1
            r2.c = r1     // Catch:{ Exception -> 0x0157 }
            goto L_0x011f
        L_0x0157:
            r0 = move-exception
            r1 = r3
        L_0x0159:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "temp root install fail:"
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r1 = java.lang.String.valueOf(r1)
            java.lang.StringBuilder r1 = r3.append(r1)
            java.lang.String r3 = ":"
            java.lang.StringBuilder r1 = r1.append(r3)
            java.lang.String r0 = r0.getMessage()
            java.lang.StringBuilder r0 = r1.append(r0)
            java.lang.String r0 = r0.toString()
            r2.b = r0
            goto L_0x0129
        L_0x0181:
            r1 = 0
            java.lang.Process r6 = b()
            if (r6 != 0) goto L_0x018e
            java.lang.String r0 = "0"
            r2.b = r0
            r0 = r2
            goto L_0x0146
        L_0x018e:
            java.io.DataOutputStream r0 = new java.io.DataOutputStream     // Catch:{ Exception -> 0x02d0 }
            java.io.OutputStream r3 = r6.getOutputStream()     // Catch:{ Exception -> 0x02d0 }
            r0.<init>(r3)     // Catch:{ Exception -> 0x02d0 }
            java.io.BufferedReader r3 = new java.io.BufferedReader     // Catch:{ Exception -> 0x02d0 }
            java.io.InputStreamReader r7 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x02d0 }
            java.io.InputStream r8 = r6.getInputStream()     // Catch:{ Exception -> 0x02d0 }
            java.lang.String r9 = "UTF-8"
            r7.<init>(r8, r9)     // Catch:{ Exception -> 0x02d0 }
            r3.<init>(r7)     // Catch:{ Exception -> 0x02d0 }
            if (r3 == 0) goto L_0x0302
            if (r0 == 0) goto L_0x0302
            boolean r4 = f(r15)     // Catch:{ Exception -> 0x02d0 }
            if (r4 != 0) goto L_0x0259
            java.io.File r4 = new java.io.File     // Catch:{ Exception -> 0x02d0 }
            r4.<init>(r15)     // Catch:{ Exception -> 0x02d0 }
            java.lang.String r7 = r4.getParent()     // Catch:{ Exception -> 0x02d0 }
            java.io.File r8 = new java.io.File     // Catch:{ Exception -> 0x02d0 }
            r8.<init>(r7)     // Catch:{ Exception -> 0x02d0 }
            java.lang.String r8 = r8.getParent()     // Catch:{ Exception -> 0x02d0 }
            java.io.File r9 = new java.io.File     // Catch:{ Exception -> 0x02d0 }
            r9.<init>(r8)     // Catch:{ Exception -> 0x02d0 }
            java.lang.String r9 = r9.getParent()     // Catch:{ Exception -> 0x02d0 }
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x02d0 }
            r10.<init>()     // Catch:{ Exception -> 0x02d0 }
            java.lang.String r11 = "chmod 777 "
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ Exception -> 0x02d0 }
            java.lang.String r4 = r4.getAbsolutePath()     // Catch:{ Exception -> 0x02d0 }
            java.lang.StringBuilder r4 = r10.append(r4)     // Catch:{ Exception -> 0x02d0 }
            java.lang.String r10 = "\n"
            java.lang.StringBuilder r4 = r4.append(r10)     // Catch:{ Exception -> 0x02d0 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x02d0 }
            r1 = 1
            byte[] r4 = r4.getBytes()     // Catch:{ Exception -> 0x02d0 }
            r0.write(r4)     // Catch:{ Exception -> 0x02d0 }
            r1 = 2
            r0.flush()     // Catch:{ Exception -> 0x02d0 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x02d0 }
            r4.<init>()     // Catch:{ Exception -> 0x02d0 }
            java.lang.String r10 = "chmod 777 "
            java.lang.StringBuilder r4 = r4.append(r10)     // Catch:{ Exception -> 0x02d0 }
            java.lang.StringBuilder r4 = r4.append(r7)     // Catch:{ Exception -> 0x02d0 }
            java.lang.String r7 = "\n"
            java.lang.StringBuilder r4 = r4.append(r7)     // Catch:{ Exception -> 0x02d0 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x02d0 }
            r1 = 3
            r0.writeBytes(r4)     // Catch:{ Exception -> 0x02d0 }
            r1 = 4
            r0.flush()     // Catch:{ Exception -> 0x02d0 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x02d0 }
            r4.<init>()     // Catch:{ Exception -> 0x02d0 }
            java.lang.String r7 = "chmod 777 "
            java.lang.StringBuilder r4 = r4.append(r7)     // Catch:{ Exception -> 0x02d0 }
            java.lang.StringBuilder r4 = r4.append(r8)     // Catch:{ Exception -> 0x02d0 }
            java.lang.String r7 = "\n"
            java.lang.StringBuilder r4 = r4.append(r7)     // Catch:{ Exception -> 0x02d0 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x02d0 }
            r1 = 5
            r0.writeBytes(r4)     // Catch:{ Exception -> 0x02d0 }
            r1 = 6
            r0.flush()     // Catch:{ Exception -> 0x02d0 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x02d0 }
            r4.<init>()     // Catch:{ Exception -> 0x02d0 }
            java.lang.String r7 = "chmod 777 "
            java.lang.StringBuilder r4 = r4.append(r7)     // Catch:{ Exception -> 0x02d0 }
            java.lang.StringBuilder r4 = r4.append(r9)     // Catch:{ Exception -> 0x02d0 }
            java.lang.String r7 = "\n"
            java.lang.StringBuilder r4 = r4.append(r7)     // Catch:{ Exception -> 0x02d0 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x02d0 }
            r1 = 7
            r0.writeBytes(r4)     // Catch:{ Exception -> 0x02d0 }
            r1 = 8
            r0.flush()     // Catch:{ Exception -> 0x02d0 }
        L_0x0259:
            int r1 = r1 + 1
            byte[] r4 = r5.getBytes()     // Catch:{ Exception -> 0x02d0 }
            r0.write(r4)     // Catch:{ Exception -> 0x02d0 }
            int r1 = r1 + 1
            r0.flush()     // Catch:{ Exception -> 0x02d0 }
            int r1 = r1 + 1
            r0.close()     // Catch:{ Exception -> 0x02d0 }
            r0 = 1024(0x400, float:1.435E-42)
            char[] r0 = new char[r0]     // Catch:{ Exception -> 0x02d0 }
            int r1 = r1 + 1
            int r4 = r3.read(r0)     // Catch:{ Exception -> 0x02d0 }
            r3.close()     // Catch:{ Exception -> 0x02d0 }
            java.lang.String r3 = ""
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x02d0 }
            r3.<init>()     // Catch:{ Exception -> 0x02d0 }
            r5 = -1
            if (r4 == r5) goto L_0x02b3
            int r1 = r1 + 1
            r5 = 0
            java.lang.StringBuilder r0 = r3.append(r0, r5, r4)     // Catch:{ Exception -> 0x02d0 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x02d0 }
            r3 = r0
        L_0x028f:
            boolean r0 = android.text.TextUtils.isEmpty(r3)     // Catch:{ Exception -> 0x02d0 }
            if (r0 != 0) goto L_0x02ce
            java.lang.String r0 = r3.toLowerCase()     // Catch:{ Exception -> 0x02d0 }
            java.lang.String r4 = "success"
            boolean r0 = r0.contains(r4)     // Catch:{ Exception -> 0x02d0 }
            if (r0 == 0) goto L_0x02ce
            r0 = 1
        L_0x02a2:
            boolean r4 = android.text.TextUtils.isEmpty(r3)     // Catch:{ Exception -> 0x02d0 }
            if (r4 != 0) goto L_0x02aa
            r2.b = r3     // Catch:{ Exception -> 0x02d0 }
        L_0x02aa:
            if (r6 == 0) goto L_0x02af
        L_0x02ac:
            r6.destroy()
        L_0x02af:
            r2.f3997a = r0
            goto L_0x0145
        L_0x02b3:
            r0 = 1024(0x400, float:1.435E-42)
            byte[] r3 = new byte[r0]     // Catch:{ Exception -> 0x02d0 }
            java.io.InputStream r0 = r6.getErrorStream()     // Catch:{ Exception -> 0x02d0 }
            int r1 = r1 + 1
            int r4 = r0.read(r3)     // Catch:{ Exception -> 0x02d0 }
            r0.close()     // Catch:{ Exception -> 0x02d0 }
            int r1 = r1 + 1
            java.lang.String r0 = new java.lang.String     // Catch:{ Exception -> 0x02d0 }
            r5 = 0
            r0.<init>(r3, r5, r4)     // Catch:{ Exception -> 0x02d0 }
            r3 = r0
            goto L_0x028f
        L_0x02ce:
            r0 = 0
            goto L_0x02a2
        L_0x02d0:
            r0 = move-exception
            r3 = r1
            r1 = r0
            r0 = 0
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x02f8 }
            r4.<init>()     // Catch:{ all -> 0x02f8 }
            java.lang.String r3 = java.lang.String.valueOf(r3)     // Catch:{ all -> 0x02f8 }
            java.lang.StringBuilder r3 = r4.append(r3)     // Catch:{ all -> 0x02f8 }
            java.lang.String r4 = ":"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x02f8 }
            java.lang.String r1 = r1.getMessage()     // Catch:{ all -> 0x02f8 }
            java.lang.StringBuilder r1 = r3.append(r1)     // Catch:{ all -> 0x02f8 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x02f8 }
            r2.b = r1     // Catch:{ all -> 0x02f8 }
            if (r6 == 0) goto L_0x02af
            goto L_0x02ac
        L_0x02f8:
            r0 = move-exception
            if (r6 == 0) goto L_0x02fe
            r6.destroy()
        L_0x02fe:
            throw r0
        L_0x02ff:
            r0 = move-exception
            goto L_0x0159
        L_0x0302:
            r0 = r4
            goto L_0x02aa
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.pangu.utils.installuninstall.InstallUninstallUtil.a(java.lang.String, int, int, java.lang.String):com.tencent.pangu.utils.installuninstall.ak");
    }

    public static boolean a(int i, String str) {
        String str2;
        boolean z;
        boolean z2 = false;
        String str3 = "LD_LIBRARY_PATH=/vendor/lib:/system/lib pm uninstall " + str + "\n";
        XLog.d("million", "root uninstall[" + i + "] " + str);
        if (i == 2) {
            try {
                Pair<Integer, String> a2 = e.a().a(str3);
                String str4 = (String) a2.second;
                if (((Integer) a2.first).intValue() == 0) {
                    z = !TextUtils.isEmpty(str4) && str4.toLowerCase().contains("success");
                } else if (((Integer) a2.first).intValue() <= 0 || (!m.a().i() && m.a().h())) {
                    z = false;
                } else {
                    z = a(1, str);
                }
                str3 + " result : " + str4;
                return z;
            } catch (Exception e) {
                e.printStackTrace();
                str3 + " result : temp root uninstall fail:" + e.getMessage();
                return false;
            }
        } else {
            Process b2 = b();
            if (b2 == null) {
                return false;
            }
            try {
                DataOutputStream dataOutputStream = new DataOutputStream(b2.getOutputStream());
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(b2.getInputStream(), "UTF-8"));
                if (!(bufferedReader == null || dataOutputStream == null)) {
                    dataOutputStream.writeBytes(str3);
                    dataOutputStream.flush();
                    dataOutputStream.close();
                    char[] cArr = new char[1024];
                    int read = bufferedReader.read(cArr);
                    bufferedReader.close();
                    StringBuilder sb = new StringBuilder();
                    if (read != -1) {
                        str2 = sb.append(cArr, 0, read).toString();
                    } else {
                        byte[] bArr = new byte[1024];
                        InputStream errorStream = b2.getErrorStream();
                        int read2 = errorStream.read(bArr);
                        errorStream.close();
                        str2 = new String(bArr, 0, read2);
                    }
                    if (!TextUtils.isEmpty(str2) && str2.toLowerCase().contains("success")) {
                        z2 = true;
                    }
                }
                if (b2 == null) {
                    return z2;
                }
            } catch (Exception e2) {
                e2.printStackTrace();
                if (b2 == null) {
                    return false;
                }
            } catch (Throwable th) {
                if (b2 != null) {
                    b2.destroy();
                }
                throw th;
            }
            b2.destroy();
            return z2;
        }
    }

    public static boolean a(String str, String str2) {
        if (str2 == null || Constants.STR_EMPTY.equals(str2)) {
            return false;
        }
        if (c(str, str2)) {
            return true;
        }
        return d(str, str2);
    }

    private static boolean c(String str, String str2) {
        if (!e.a().c()) {
            return false;
        }
        e.a().b("am force-stop " + str);
        if (!str2.toLowerCase().contains("system")) {
            e.a().b("pm uninstall " + str);
            XLog.d("miles", "runTempRootUninstall pm uninstall " + str + " sourceDir:" + str2 + " 卸载成功");
            return true;
        }
        if (d(e.a().b("mount -o remount,rw -t yaffs2 /dev/mtdblock3 /system"))) {
            XLog.d("runTempRootUninstall cmd = " + "mount -o remount,rw -t yaffs2 /dev/mtdblock3 /system" + " success.");
        } else {
            XLog.d("runTempRootUninstall cmd = " + "mount -o remount,rw -t yaffs2 /dev/mtdblock3 /system" + " failed.");
        }
        e.a().b("mount -o remount,rw /system");
        e.a().b("chmod 777 " + str2);
        if (Build.VERSION.SDK_INT >= 21) {
            e.a().b("pm disable " + str + "\n");
        }
        e.a().b("rm " + str2);
        String replace = str2.replace(".apk", ".odex");
        if (new File(replace).exists()) {
            if (d(e.a().b("rm " + replace))) {
                XLog.d("runTempRootUninstall delete " + replace + " success.");
            } else {
                XLog.d("runTempRootUninstall delete " + replace + " failed.");
            }
        }
        if (d(e.a().b("rm -r /data/data/" + str))) {
            XLog.d("runTempRootUninstall delete /data/data/" + str + " success.");
        } else {
            XLog.d("runTempRootUninstall delete /data/data/" + str + " failed.");
        }
        String str3 = "rm -r /data/dalvik-cache/*" + str2.substring(str2.lastIndexOf("/")) + "*";
        if (d(e.a().b(str3))) {
            XLog.d("runTempRootUninstall " + str3 + " success.");
        } else {
            XLog.d("runTempRootUninstall " + str3 + " failed.");
        }
        if (d(e.a().b("mount -o remount,ro -t yaffs2 /dev/mtdblock3 /system"))) {
            XLog.d("runTempRootUninstall cmd = " + "mount -o remount,ro -t yaffs2 /dev/mtdblock3 /system" + " success.");
        } else {
            XLog.d("runTempRootUninstall cmd = " + "mount -o remount,ro -t yaffs2 /dev/mtdblock3 /system" + " failed.");
        }
        return e(str);
    }

    private static boolean d(String str) {
        String[] strArr = {"failure", "failed", "unallowed", "not permitted"};
        if (str != null) {
            for (String contains : strArr) {
                if (str.contains(contains)) {
                    return false;
                }
            }
        }
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:127:0x032e, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:128:0x032f, code lost:
        r1 = r2;
        r2 = r3;
        r3 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:129:0x0334, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:130:0x0335, code lost:
        r1 = r2;
        r3 = r4;
        r2 = 3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x020f, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x0210, code lost:
        r3 = r4;
        r11 = r2;
        r2 = r1;
        r1 = r11;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:0x02b9, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:?, code lost:
        r4.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:86:0x02c6, code lost:
        r6.destroy();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:88:0x02ca, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:89:0x02cb, code lost:
        r1.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:90:0x02cf, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:91:0x02d0, code lost:
        r1.printStackTrace();
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:133:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x0247 A[SYNTHETIC, Splitter:B:53:0x0247] */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x024c A[SYNTHETIC, Splitter:B:56:0x024c] */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x02b9 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:9:0x0037] */
    /* JADX WARNING: Removed duplicated region for block: B:80:0x02bc A[SYNTHETIC, Splitter:B:80:0x02bc] */
    /* JADX WARNING: Removed duplicated region for block: B:83:0x02c1 A[SYNTHETIC, Splitter:B:83:0x02c1] */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x02c6  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static boolean d(java.lang.String r12, java.lang.String r13) {
        /*
            r5 = 3
            r10 = -1
            r3 = 0
            r1 = 1
            r0 = 0
            java.lang.String r2 = "miles"
            java.lang.String r4 = "runPerRootUninstall start.."
            com.tencent.assistant.utils.XLog.d(r2, r4)
            java.lang.Process r6 = a(r1)
            if (r6 != 0) goto L_0x001a
            java.lang.String r1 = "miles"
            java.lang.String r2 = "runPerRootUninstall >> getProcess return null."
            com.tencent.assistant.utils.XLog.e(r1, r2)
        L_0x0019:
            return r0
        L_0x001a:
            java.io.DataOutputStream r4 = new java.io.DataOutputStream     // Catch:{ Exception -> 0x0319, all -> 0x030e }
            java.io.OutputStream r2 = r6.getOutputStream()     // Catch:{ Exception -> 0x0319, all -> 0x030e }
            r4.<init>(r2)     // Catch:{ Exception -> 0x0319, all -> 0x030e }
            java.io.BufferedReader r2 = new java.io.BufferedReader     // Catch:{ Exception -> 0x031f, all -> 0x0312 }
            java.io.InputStreamReader r7 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x031f, all -> 0x0312 }
            java.io.InputStream r8 = r6.getInputStream()     // Catch:{ Exception -> 0x031f, all -> 0x0312 }
            java.lang.String r9 = "UTF-8"
            r7.<init>(r8, r9)     // Catch:{ Exception -> 0x031f, all -> 0x0312 }
            r2.<init>(r7)     // Catch:{ Exception -> 0x031f, all -> 0x0312 }
            if (r2 == 0) goto L_0x02f6
            if (r4 == 0) goto L_0x02f6
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0326, all -> 0x02b9 }
            r3.<init>()     // Catch:{ Exception -> 0x0326, all -> 0x02b9 }
            java.lang.String r7 = "am force-stop "
            java.lang.StringBuilder r3 = r3.append(r7)     // Catch:{ Exception -> 0x0326, all -> 0x02b9 }
            java.lang.StringBuilder r3 = r3.append(r12)     // Catch:{ Exception -> 0x0326, all -> 0x02b9 }
            java.lang.String r0 = r3.toString()     // Catch:{ Exception -> 0x0326, all -> 0x02b9 }
            byte[] r0 = r0.getBytes()     // Catch:{ Exception -> 0x020f, all -> 0x02b9 }
            r4.write(r0)     // Catch:{ Exception -> 0x020f, all -> 0x02b9 }
            r3 = 2
            r4.flush()     // Catch:{ Exception -> 0x032e, all -> 0x02b9 }
            java.lang.String r0 = r13.toLowerCase()     // Catch:{ Exception -> 0x032e, all -> 0x02b9 }
            java.lang.String r7 = "system"
            boolean r0 = r0.contains(r7)     // Catch:{ Exception -> 0x032e, all -> 0x02b9 }
            if (r0 != 0) goto L_0x00b5
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x032e, all -> 0x02b9 }
            r0.<init>()     // Catch:{ Exception -> 0x032e, all -> 0x02b9 }
            java.lang.String r7 = "pm uninstall "
            java.lang.StringBuilder r0 = r0.append(r7)     // Catch:{ Exception -> 0x032e, all -> 0x02b9 }
            java.lang.StringBuilder r0 = r0.append(r12)     // Catch:{ Exception -> 0x032e, all -> 0x02b9 }
            java.lang.String r7 = "\n"
            java.lang.StringBuilder r0 = r0.append(r7)     // Catch:{ Exception -> 0x032e, all -> 0x02b9 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x032e, all -> 0x02b9 }
            byte[] r0 = r0.getBytes()     // Catch:{ Exception -> 0x0334, all -> 0x02b9 }
            r4.write(r0)     // Catch:{ Exception -> 0x0334, all -> 0x02b9 }
            r3 = 4
            r4.flush()     // Catch:{ Exception -> 0x032e, all -> 0x02b9 }
            java.lang.String r0 = "miles"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x032e, all -> 0x02b9 }
            r5.<init>()     // Catch:{ Exception -> 0x032e, all -> 0x02b9 }
            java.lang.String r7 = "pm uninstall "
            java.lang.StringBuilder r5 = r5.append(r7)     // Catch:{ Exception -> 0x032e, all -> 0x02b9 }
            java.lang.StringBuilder r5 = r5.append(r12)     // Catch:{ Exception -> 0x032e, all -> 0x02b9 }
            java.lang.String r7 = " 卸载成功"
            java.lang.StringBuilder r5 = r5.append(r7)     // Catch:{ Exception -> 0x032e, all -> 0x02b9 }
            java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x032e, all -> 0x02b9 }
            com.tencent.assistant.utils.XLog.d(r0, r5)     // Catch:{ Exception -> 0x032e, all -> 0x02b9 }
            if (r4 == 0) goto L_0x00a8
            r4.close()     // Catch:{ IOException -> 0x02e0 }
        L_0x00a8:
            if (r2 == 0) goto L_0x00ad
            r2.close()     // Catch:{ IOException -> 0x02e6 }
        L_0x00ad:
            if (r6 == 0) goto L_0x00b2
            r6.destroy()
        L_0x00b2:
            r0 = r1
            goto L_0x0019
        L_0x00b5:
            java.lang.String r0 = "mount -o remount,rw -t yaffs2 /dev/mtdblock3 /system\n"
            byte[] r0 = r0.getBytes()     // Catch:{ Exception -> 0x0334, all -> 0x02b9 }
            r4.write(r0)     // Catch:{ Exception -> 0x0334, all -> 0x02b9 }
            r1 = 4
            r4.flush()     // Catch:{ Exception -> 0x020f, all -> 0x02b9 }
            java.lang.String r0 = "mount -o remount,rw /system\n"
            r1 = 5
            byte[] r0 = r0.getBytes()     // Catch:{ Exception -> 0x020f, all -> 0x02b9 }
            r4.write(r0)     // Catch:{ Exception -> 0x020f, all -> 0x02b9 }
            r1 = 6
            r4.flush()     // Catch:{ Exception -> 0x020f, all -> 0x02b9 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x020f, all -> 0x02b9 }
            r0.<init>()     // Catch:{ Exception -> 0x020f, all -> 0x02b9 }
            java.lang.String r3 = "chmod 777 "
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ Exception -> 0x020f, all -> 0x02b9 }
            java.lang.StringBuilder r0 = r0.append(r13)     // Catch:{ Exception -> 0x020f, all -> 0x02b9 }
            java.lang.String r3 = "\n"
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ Exception -> 0x020f, all -> 0x02b9 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x020f, all -> 0x02b9 }
            r1 = 7
            byte[] r0 = r0.getBytes()     // Catch:{ Exception -> 0x020f, all -> 0x02b9 }
            r4.write(r0)     // Catch:{ Exception -> 0x020f, all -> 0x02b9 }
            r1 = 8
            r4.flush()     // Catch:{ Exception -> 0x020f, all -> 0x02b9 }
            int r0 = android.os.Build.VERSION.SDK_INT     // Catch:{ Exception -> 0x020f, all -> 0x02b9 }
            r3 = 21
            if (r0 < r3) goto L_0x0123
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x020f, all -> 0x02b9 }
            r0.<init>()     // Catch:{ Exception -> 0x020f, all -> 0x02b9 }
            java.lang.String r3 = "pm disable "
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ Exception -> 0x020f, all -> 0x02b9 }
            java.lang.StringBuilder r0 = r0.append(r12)     // Catch:{ Exception -> 0x020f, all -> 0x02b9 }
            java.lang.String r3 = "\n"
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ Exception -> 0x020f, all -> 0x02b9 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x020f, all -> 0x02b9 }
            r1 = 9
            byte[] r0 = r0.getBytes()     // Catch:{ Exception -> 0x020f, all -> 0x02b9 }
            r4.write(r0)     // Catch:{ Exception -> 0x020f, all -> 0x02b9 }
            r1 = 10
            r4.flush()     // Catch:{ Exception -> 0x020f, all -> 0x02b9 }
        L_0x0123:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x020f, all -> 0x02b9 }
            r0.<init>()     // Catch:{ Exception -> 0x020f, all -> 0x02b9 }
            java.lang.String r3 = "rm "
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ Exception -> 0x020f, all -> 0x02b9 }
            java.lang.StringBuilder r0 = r0.append(r13)     // Catch:{ Exception -> 0x020f, all -> 0x02b9 }
            java.lang.String r3 = "\n"
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ Exception -> 0x020f, all -> 0x02b9 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x020f, all -> 0x02b9 }
            int r1 = r1 + 1
            byte[] r0 = r0.getBytes()     // Catch:{ Exception -> 0x020f, all -> 0x02b9 }
            r4.write(r0)     // Catch:{ Exception -> 0x020f, all -> 0x02b9 }
            int r1 = r1 + 1
            r4.flush()     // Catch:{ Exception -> 0x020f, all -> 0x02b9 }
            java.lang.String r0 = ".apk"
            java.lang.String r3 = ".odex"
            java.lang.String r0 = r13.replace(r0, r3)     // Catch:{ Exception -> 0x020f, all -> 0x02b9 }
            java.io.File r3 = new java.io.File     // Catch:{ Exception -> 0x020f, all -> 0x02b9 }
            r3.<init>(r0)     // Catch:{ Exception -> 0x020f, all -> 0x02b9 }
            boolean r3 = r3.exists()     // Catch:{ Exception -> 0x020f, all -> 0x02b9 }
            if (r3 == 0) goto L_0x0180
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x020f, all -> 0x02b9 }
            r3.<init>()     // Catch:{ Exception -> 0x020f, all -> 0x02b9 }
            java.lang.String r5 = "rm "
            java.lang.StringBuilder r3 = r3.append(r5)     // Catch:{ Exception -> 0x020f, all -> 0x02b9 }
            java.lang.StringBuilder r0 = r3.append(r0)     // Catch:{ Exception -> 0x020f, all -> 0x02b9 }
            java.lang.String r3 = "\n"
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ Exception -> 0x020f, all -> 0x02b9 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x020f, all -> 0x02b9 }
            int r1 = r1 + 1
            r4.writeBytes(r0)     // Catch:{ Exception -> 0x020f, all -> 0x02b9 }
            int r1 = r1 + 1
            r4.flush()     // Catch:{ Exception -> 0x020f, all -> 0x02b9 }
        L_0x0180:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x020f, all -> 0x02b9 }
            r0.<init>()     // Catch:{ Exception -> 0x020f, all -> 0x02b9 }
            java.lang.String r3 = "rm -r /data/data/"
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ Exception -> 0x020f, all -> 0x02b9 }
            java.lang.StringBuilder r0 = r0.append(r12)     // Catch:{ Exception -> 0x020f, all -> 0x02b9 }
            java.lang.String r3 = "\n"
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ Exception -> 0x020f, all -> 0x02b9 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x020f, all -> 0x02b9 }
            int r1 = r1 + 1
            r4.writeBytes(r0)     // Catch:{ Exception -> 0x020f, all -> 0x02b9 }
            int r1 = r1 + 1
            r4.flush()     // Catch:{ Exception -> 0x020f, all -> 0x02b9 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x020f, all -> 0x02b9 }
            r0.<init>()     // Catch:{ Exception -> 0x020f, all -> 0x02b9 }
            java.lang.String r3 = "rm -r /data/dalvik-cache/*"
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ Exception -> 0x020f, all -> 0x02b9 }
            java.lang.String r3 = "/"
            int r3 = r13.lastIndexOf(r3)     // Catch:{ Exception -> 0x020f, all -> 0x02b9 }
            int r3 = r3 + 1
            java.lang.String r3 = r13.substring(r3)     // Catch:{ Exception -> 0x020f, all -> 0x02b9 }
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ Exception -> 0x020f, all -> 0x02b9 }
            java.lang.String r3 = "*"
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ Exception -> 0x020f, all -> 0x02b9 }
            java.lang.String r3 = "\n"
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ Exception -> 0x020f, all -> 0x02b9 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x020f, all -> 0x02b9 }
            int r1 = r1 + 1
            r4.writeBytes(r0)     // Catch:{ Exception -> 0x020f, all -> 0x02b9 }
            int r1 = r1 + 1
            r4.flush()     // Catch:{ Exception -> 0x020f, all -> 0x02b9 }
            java.lang.String r0 = "mount -o remount,ro -t yaffs2 /dev/mtdblock3 /system\n"
            int r1 = r1 + 1
            r4.writeBytes(r0)     // Catch:{ Exception -> 0x020f, all -> 0x02b9 }
            int r1 = r1 + 1
            r4.flush()     // Catch:{ Exception -> 0x020f, all -> 0x02b9 }
            int r1 = r1 + 1
            java.lang.String r0 = "exit\n"
            int r1 = r1 + 1
            r4.writeBytes(r0)     // Catch:{ Exception -> 0x020f, all -> 0x02b9 }
            int r1 = r1 + 1
            r4.flush()     // Catch:{ Exception -> 0x020f, all -> 0x02b9 }
            int r1 = r1 + 1
            r7 = 45000(0xafc8, double:2.2233E-319)
            com.tencent.pangu.utils.installuninstall.aj r0 = new com.tencent.pangu.utils.installuninstall.aj     // Catch:{ Exception -> 0x020f, all -> 0x02b9 }
            r3 = 0
            r0.<init>(r6)     // Catch:{ Exception -> 0x020f, all -> 0x02b9 }
            r0.start()     // Catch:{ Exception -> 0x020f, all -> 0x02b9 }
            r0.join(r7)     // Catch:{ Exception -> 0x020f, all -> 0x02b9 }
            java.lang.Integer r0 = r0.b     // Catch:{ Exception -> 0x020f, all -> 0x02b9 }
            if (r0 != 0) goto L_0x0256
            java.util.concurrent.TimeoutException r0 = new java.util.concurrent.TimeoutException     // Catch:{ Exception -> 0x020f, all -> 0x02b9 }
            r0.<init>()     // Catch:{ Exception -> 0x020f, all -> 0x02b9 }
            throw r0     // Catch:{ Exception -> 0x020f, all -> 0x02b9 }
        L_0x020f:
            r0 = move-exception
            r3 = r4
            r11 = r2
            r2 = r1
            r1 = r11
        L_0x0214:
            java.lang.String r4 = "miles"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x0315 }
            r5.<init>()     // Catch:{ all -> 0x0315 }
            java.lang.String r7 = "runPerRootUninstall exception. exceptionEndPos = "
            java.lang.StringBuilder r5 = r5.append(r7)     // Catch:{ all -> 0x0315 }
            java.lang.String r2 = java.lang.String.valueOf(r2)     // Catch:{ all -> 0x0315 }
            java.lang.StringBuilder r2 = r5.append(r2)     // Catch:{ all -> 0x0315 }
            java.lang.String r5 = " message = "
            java.lang.StringBuilder r2 = r2.append(r5)     // Catch:{ all -> 0x0315 }
            java.lang.String r5 = r0.getMessage()     // Catch:{ all -> 0x0315 }
            java.lang.StringBuilder r2 = r2.append(r5)     // Catch:{ all -> 0x0315 }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x0315 }
            com.tencent.assistant.utils.XLog.e(r4, r2)     // Catch:{ all -> 0x0315 }
            r0.printStackTrace()     // Catch:{ all -> 0x0315 }
            boolean r0 = e(r12)     // Catch:{ all -> 0x0315 }
            if (r3 == 0) goto L_0x024a
            r3.close()     // Catch:{ IOException -> 0x02d4 }
        L_0x024a:
            if (r1 == 0) goto L_0x024f
            r1.close()     // Catch:{ IOException -> 0x02da }
        L_0x024f:
            if (r6 == 0) goto L_0x0019
        L_0x0251:
            r6.destroy()
            goto L_0x0019
        L_0x0256:
            r0 = 1024(0x400, float:1.435E-42)
            char[] r3 = new char[r0]     // Catch:{ Exception -> 0x020f, all -> 0x02b9 }
            int r1 = r1 + 1
            int r5 = r2.read(r3)     // Catch:{ Exception -> 0x020f, all -> 0x02b9 }
            java.lang.String r0 = ""
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x020f, all -> 0x02b9 }
            r7.<init>()     // Catch:{ Exception -> 0x020f, all -> 0x02b9 }
            if (r5 == r10) goto L_0x029d
            int r1 = r1 + 1
            r0 = 0
            java.lang.StringBuilder r0 = r7.append(r3, r0, r5)     // Catch:{ Exception -> 0x020f, all -> 0x02b9 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x020f, all -> 0x02b9 }
        L_0x0274:
            java.lang.String r3 = "miles"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x020f, all -> 0x02b9 }
            r5.<init>()     // Catch:{ Exception -> 0x020f, all -> 0x02b9 }
            java.lang.String r7 = "runPerRootUninstall result string : "
            java.lang.StringBuilder r5 = r5.append(r7)     // Catch:{ Exception -> 0x020f, all -> 0x02b9 }
            java.lang.StringBuilder r0 = r5.append(r0)     // Catch:{ Exception -> 0x020f, all -> 0x02b9 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x020f, all -> 0x02b9 }
            com.tencent.assistant.utils.XLog.d(r3, r0)     // Catch:{ Exception -> 0x020f, all -> 0x02b9 }
            boolean r0 = e(r12)     // Catch:{ Exception -> 0x020f, all -> 0x02b9 }
            if (r4 == 0) goto L_0x0295
            r4.close()     // Catch:{ IOException -> 0x02ec }
        L_0x0295:
            if (r2 == 0) goto L_0x029a
            r2.close()     // Catch:{ IOException -> 0x02f1 }
        L_0x029a:
            if (r6 == 0) goto L_0x0019
            goto L_0x0251
        L_0x029d:
            r3 = 1024(0x400, float:1.435E-42)
            byte[] r3 = new byte[r3]     // Catch:{ Exception -> 0x020f, all -> 0x02b9 }
            java.io.InputStream r5 = r6.getErrorStream()     // Catch:{ Exception -> 0x020f, all -> 0x02b9 }
            int r1 = r1 + 1
            int r7 = r5.read(r3)     // Catch:{ Exception -> 0x020f, all -> 0x02b9 }
            r5.close()     // Catch:{ Exception -> 0x020f, all -> 0x02b9 }
            int r1 = r1 + 1
            if (r7 == r10) goto L_0x0274
            java.lang.String r0 = new java.lang.String     // Catch:{ Exception -> 0x020f, all -> 0x02b9 }
            r5 = 0
            r0.<init>(r3, r5, r7)     // Catch:{ Exception -> 0x020f, all -> 0x02b9 }
            goto L_0x0274
        L_0x02b9:
            r0 = move-exception
        L_0x02ba:
            if (r4 == 0) goto L_0x02bf
            r4.close()     // Catch:{ IOException -> 0x02ca }
        L_0x02bf:
            if (r2 == 0) goto L_0x02c4
            r2.close()     // Catch:{ IOException -> 0x02cf }
        L_0x02c4:
            if (r6 == 0) goto L_0x02c9
            r6.destroy()
        L_0x02c9:
            throw r0
        L_0x02ca:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x02bf
        L_0x02cf:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x02c4
        L_0x02d4:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x024a
        L_0x02da:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x024f
        L_0x02e0:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x00a8
        L_0x02e6:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x00ad
        L_0x02ec:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0295
        L_0x02f1:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x029a
        L_0x02f6:
            if (r4 == 0) goto L_0x02fb
            r4.close()     // Catch:{ IOException -> 0x0304 }
        L_0x02fb:
            if (r2 == 0) goto L_0x0300
            r2.close()     // Catch:{ IOException -> 0x0309 }
        L_0x0300:
            if (r6 == 0) goto L_0x0019
            goto L_0x0251
        L_0x0304:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x02fb
        L_0x0309:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0300
        L_0x030e:
            r0 = move-exception
            r2 = r3
            r4 = r3
            goto L_0x02ba
        L_0x0312:
            r0 = move-exception
            r2 = r3
            goto L_0x02ba
        L_0x0315:
            r0 = move-exception
            r2 = r1
            r4 = r3
            goto L_0x02ba
        L_0x0319:
            r1 = move-exception
            r2 = r0
            r0 = r1
            r1 = r3
            goto L_0x0214
        L_0x031f:
            r1 = move-exception
            r2 = r0
            r0 = r1
            r1 = r3
            r3 = r4
            goto L_0x0214
        L_0x0326:
            r1 = move-exception
            r3 = r4
            r11 = r2
            r2 = r0
            r0 = r1
            r1 = r11
            goto L_0x0214
        L_0x032e:
            r0 = move-exception
            r1 = r2
            r2 = r3
            r3 = r4
            goto L_0x0214
        L_0x0334:
            r0 = move-exception
            r1 = r2
            r3 = r4
            r2 = r5
            goto L_0x0214
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.pangu.utils.installuninstall.InstallUninstallUtil.d(java.lang.String, java.lang.String):boolean");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    private static boolean e(String str) {
        LocalApkInfo b2 = com.tencent.assistant.utils.e.b(str);
        if (b2 == null || !b2.mIsEnabled) {
            XLog.d("miles", str + " 卸载成功");
            if (b2 != null) {
                Intent intent = new Intent();
                intent.setPackage(AstApp.i().getPackageName());
                intent.setAction("com.tencent.android.qqdownloader.PACKAGE_STATE_CHANGE");
                intent.setData(Uri.parse("package:" + str));
                intent.putExtra("enabled", false);
                AstApp.i().sendBroadcast(intent);
            }
            return true;
        }
        XLog.e("miles", str + " 卸载失败");
        return false;
    }

    public static void a(String str, int i, String str2) {
        if (b != null) {
            b.add(str2);
            try {
                Thread.sleep(250);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setDataAndType(Uri.parse("file://" + str2), "application/vnd.android.package-archive");
        intent.setFlags(268435456);
        try {
            AstApp.i().startActivity(intent);
            o.a().a(str, i, (byte) 0);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    static {
        f3986a.setDaemon(true);
        f3986a.start();
    }

    /* compiled from: ProGuard */
    class RequestQueue extends PriorityBlockingQueue<String> implements Runnable {
        public RequestQueue() {
            super(10);
        }

        public void run() {
            while (true) {
                try {
                    String str = (String) take();
                    if (!TextUtils.isEmpty(str) && !InstallUninstallUtil.f(str)) {
                        File file = new File(str);
                        String parent = file.getParent();
                        String parent2 = new File(parent).getParent();
                        String parent3 = new File(parent2).getParent();
                        try {
                            Runtime.getRuntime().exec("chmod 777 " + file.getAbsolutePath());
                            Runtime.getRuntime().exec("chmod 777 " + parent);
                            Runtime.getRuntime().exec("chmod 777 " + parent2);
                            Runtime.getRuntime().exec("chmod 777" + parent3);
                            if (Global.isDev()) {
                                XLog.d("InstallUninstall", "sytemInstall chmod finish.file path:" + str);
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                } catch (InterruptedException e2) {
                    e2.printStackTrace();
                }
            }
        }

        /* renamed from: a */
        public boolean add(String str) {
            if (TextUtils.isEmpty(str)) {
                return true;
            }
            super.put(str);
            return true;
        }
    }

    public static void a(String str) {
        Intent intent = new Intent("android.intent.action.DELETE", Uri.parse("package:" + str));
        intent.setFlags(268435456);
        try {
            AstApp.i().startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Process b() {
        return a(false);
    }

    public static Process a(boolean z) {
        ProcessBuilder processBuilder = new ProcessBuilder("su");
        processBuilder.redirectErrorStream(z);
        Process process = null;
        try {
            process = processBuilder.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (process != null) {
            e.a().b();
        }
        return process;
    }

    public static Pair<Boolean, String> b(String str, String str2) {
        boolean z = true;
        String str3 = Constants.STR_EMPTY;
        LocalApkInfo localApkInfo = ApkResourceManager.getInstance().getLocalApkInfo(str);
        if (!TextUtils.isEmpty(str2) && localApkInfo != null && !TextUtils.isEmpty(localApkInfo.signature) && !str2.equalsIgnoreCase(localApkInfo.signature)) {
            z = false;
            str3 = "local signature:" + localApkInfo.signature + ", server signature:" + str2;
        }
        return Pair.create(Boolean.valueOf(z), str3);
    }

    public static Pair<Boolean, String> b(String str) {
        boolean z = true;
        String str2 = Constants.STR_EMPTY;
        int d = r.d();
        int b2 = r.b(com.tencent.assistant.utils.e.e(str));
        if (b2 > 0) {
            if (b2 > d) {
                z = false;
            }
            if (!z) {
                str2 = "rom sdk version:" + d + ", apk require:" + b2;
            }
        }
        return Pair.create(Boolean.valueOf(z), str2);
    }

    public static Pair<Boolean, String> a(long j) {
        boolean z;
        boolean z2 = true;
        long c = t.c();
        boolean z3 = ((float) c) > ((float) j) * 3.0f;
        if (z3) {
            if (!z3 || c()) {
                z2 = false;
            }
            z = z2;
        } else {
            z = z3;
        }
        return Pair.create(Boolean.valueOf(z), !z ? "phone memory total:" + t.d() + ", phone memory left:" + c + ", apk size:" + j : Constants.STR_EMPTY);
    }

    public static boolean c() {
        return t.c() <= Math.min(524288000, (t.d() * 10) / 100);
    }

    /* access modifiers changed from: private */
    public static boolean f(String str) {
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        return str.startsWith(Environment.getExternalStorageDirectory().getPath());
    }
}
