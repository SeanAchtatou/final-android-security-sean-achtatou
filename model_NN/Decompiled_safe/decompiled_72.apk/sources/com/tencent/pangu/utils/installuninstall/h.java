package com.tencent.pangu.utils.installuninstall;

import com.qq.AppService.AstApp;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.st.STConstAction;
import com.tencent.assistant.utils.FunctionUtils;
import com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager;

/* compiled from: ProGuard */
class h extends AppConst.TwoBtnDialogInfo {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ FunctionUtils.InstallSpaceNotEnoughForwardPage f4003a;
    final /* synthetic */ InstallUninstallDialogManager b;

    h(InstallUninstallDialogManager installUninstallDialogManager, FunctionUtils.InstallSpaceNotEnoughForwardPage installSpaceNotEnoughForwardPage) {
        this.b = installUninstallDialogManager;
        this.f4003a = installSpaceNotEnoughForwardPage;
    }

    public void onCancell() {
        InstallUninstallDialogManager.DIALOG_DEALWITH_RESULT unused = this.b.d = InstallUninstallDialogManager.DIALOG_DEALWITH_RESULT.RESULT_CANCEL;
        this.b.c();
        this.b.a(this.pageId, AstApp.m() != null ? AstApp.m().f() : 2000, "00_003", 200);
    }

    public void onLeftBtnClick() {
        InstallUninstallDialogManager.DIALOG_DEALWITH_RESULT unused = this.b.d = InstallUninstallDialogManager.DIALOG_DEALWITH_RESULT.RESULT_LEFT_BUTTON;
        this.b.c();
        this.b.a(this.pageId, AstApp.m() != null ? AstApp.m().f() : 2000, "00_002", STConstAction.ACTION_HIT_SEARCH_CANCEL);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.utils.FunctionUtils.a(int, boolean, com.tencent.assistant.utils.FunctionUtils$InstallSpaceNotEnoughForwardPage):void
     arg types: [int, int, com.tencent.assistant.utils.FunctionUtils$InstallSpaceNotEnoughForwardPage]
     candidates:
      com.tencent.assistant.utils.FunctionUtils.a(android.app.Activity, android.app.Dialog, int):android.view.View
      com.tencent.assistant.utils.FunctionUtils.a(int, boolean, com.tencent.assistant.utils.FunctionUtils$InstallSpaceNotEnoughForwardPage):void */
    public void onRightBtnClick() {
        InstallUninstallDialogManager.DIALOG_DEALWITH_RESULT unused = this.b.d = InstallUninstallDialogManager.DIALOG_DEALWITH_RESULT.RESULT_RIGHT_BUTTON;
        this.b.c();
        FunctionUtils.a(this.pageId, true, this.f4003a);
        this.b.a(this.pageId, AstApp.m() != null ? AstApp.m().f() : 2000, "00_001", 200);
    }
}
