package com.tencent.pangu.utils;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import java.io.Serializable;

/* compiled from: ProGuard */
public abstract class d {
    public static String a(Intent intent, String str) {
        if (intent == null) {
            return null;
        }
        try {
            return intent.getStringExtra(str);
        } catch (Throwable th) {
            return null;
        }
    }

    public static Bundle a(Intent intent) {
        if (intent == null) {
            return null;
        }
        try {
            Bundle extras = intent.getExtras();
            if (extras != null) {
                extras.get("ICERAO");
            }
            return extras;
        } catch (Throwable th) {
            if (!(th instanceof RuntimeException) || TextUtils.isEmpty(th.toString()) || th.toString().indexOf("ClassNotFound") < 0) {
                return null;
            }
            Log.w("IntentCommonUtils", "get intent extras fail." + th);
            return null;
        }
    }

    public static Serializable b(Intent intent, String str) {
        if (intent == null) {
            return null;
        }
        try {
            return intent.getSerializableExtra(str);
        } catch (Throwable th) {
            return null;
        }
    }

    public static boolean a(Intent intent, String str, boolean z) {
        if (intent == null) {
            return z;
        }
        try {
            return intent.getBooleanExtra(str, z);
        } catch (Throwable th) {
            return z;
        }
    }

    public static int a(Intent intent, String str, int i) {
        if (intent == null) {
            return i;
        }
        try {
            return intent.getIntExtra(str, i);
        } catch (Throwable th) {
            return i;
        }
    }
}
