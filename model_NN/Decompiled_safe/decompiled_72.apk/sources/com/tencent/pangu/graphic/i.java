package com.tencent.pangu.graphic;

import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.text.TextUtils;
import com.qq.AppService.AstApp;
import com.tencent.assistant.utils.FileUtil;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.aq;
import com.tencent.assistant.utils.r;
import java.io.File;
import java.io.FileInputStream;
import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

/* compiled from: ProGuard */
public class i {
    private static i d;

    /* renamed from: a  reason: collision with root package name */
    private ReferenceQueue<h> f3774a = new ReferenceQueue<>();
    private ConcurrentLinkedQueue<WeakReference<h>> b = new ConcurrentLinkedQueue<>();
    private Map<String, g> c = new ConcurrentHashMap();

    private i() {
    }

    public static synchronized i a() {
        i iVar;
        synchronized (i.class) {
            if (d == null) {
                d = new i();
            }
            iVar = d;
        }
        return iVar;
    }

    public void a(h hVar) {
        if (hVar != null) {
            while (true) {
                Reference<? extends h> poll = this.f3774a.poll();
                if (poll == null) {
                    break;
                }
                this.b.remove(poll);
            }
            Iterator<WeakReference<h>> it = this.b.iterator();
            while (it.hasNext()) {
                if (((h) it.next().get()) == hVar) {
                    return;
                }
            }
            this.b.add(new WeakReference(hVar, this.f3774a));
        }
    }

    /* access modifiers changed from: private */
    public void a(g gVar, e eVar) {
        if (gVar != null && !TextUtils.isEmpty(gVar.f3773a)) {
            this.c.remove(gVar.f3773a);
            Iterator<WeakReference<h>> it = this.b.iterator();
            while (it.hasNext()) {
                h hVar = (h) it.next().get();
                if (hVar != null) {
                    hVar.a(gVar, eVar);
                }
            }
        }
    }

    public boolean a(g gVar) {
        if (gVar == null || TextUtils.isEmpty(gVar.f3773a)) {
            return false;
        }
        if (this.c.get(gVar.f3773a) != null) {
            return true;
        }
        this.c.put(gVar.f3773a, gVar);
        b(gVar);
        return true;
    }

    private void b(g gVar) {
        TemporaryThreadManager.get().start(new j(this, gVar));
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0064, code lost:
        if (r2.isHeld() != false) goto L_0x0066;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0066, code lost:
        r2.release();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x007f, code lost:
        if (r2.isHeld() != false) goto L_0x0066;
     */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0060  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0072 A[SYNTHETIC, Splitter:B:31:0x0072] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x007b  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0086 A[SYNTHETIC, Splitter:B:39:0x0086] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:16:0x0052=Splitter:B:16:0x0052, B:28:0x006d=Splitter:B:28:0x006d} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean a(java.lang.String r8, java.lang.String r9) {
        /*
            r7 = this;
            r2 = 0
            r1 = 0
            boolean r0 = android.text.TextUtils.isEmpty(r8)
            if (r0 == 0) goto L_0x000a
            r0 = r1
        L_0x0009:
            return r0
        L_0x000a:
            org.apache.http.impl.client.DefaultHttpClient r3 = com.tencent.assistant.protocol.c.a()     // Catch:{ ClientProtocolException -> 0x0050, IOException -> 0x006b, all -> 0x0082 }
            org.apache.http.client.methods.HttpGet r4 = new org.apache.http.client.methods.HttpGet     // Catch:{ ClientProtocolException -> 0x00a5, IOException -> 0x00a3 }
            r4.<init>(r8)     // Catch:{ ClientProtocolException -> 0x00a5, IOException -> 0x00a3 }
            com.qq.AppService.AstApp r0 = com.qq.AppService.AstApp.i()     // Catch:{ ClientProtocolException -> 0x00a5, IOException -> 0x00a3 }
            java.lang.String r5 = "power"
            java.lang.Object r0 = r0.getSystemService(r5)     // Catch:{ ClientProtocolException -> 0x00a5, IOException -> 0x00a3 }
            android.os.PowerManager r0 = (android.os.PowerManager) r0     // Catch:{ ClientProtocolException -> 0x00a5, IOException -> 0x00a3 }
            r5 = 1
            java.lang.Class r6 = r7.getClass()     // Catch:{ ClientProtocolException -> 0x00a5, IOException -> 0x00a3 }
            java.lang.String r6 = r6.getSimpleName()     // Catch:{ ClientProtocolException -> 0x00a5, IOException -> 0x00a3 }
            android.os.PowerManager$WakeLock r2 = r0.newWakeLock(r5, r6)     // Catch:{ ClientProtocolException -> 0x00a5, IOException -> 0x00a3 }
            r0 = 0
            r2.setReferenceCounted(r0)     // Catch:{ ClientProtocolException -> 0x00a5, IOException -> 0x00a3 }
            r2.acquire()     // Catch:{ ClientProtocolException -> 0x00a5, IOException -> 0x00a3 }
            org.apache.http.HttpResponse r0 = r3.execute(r4)     // Catch:{ ClientProtocolException -> 0x00a5, IOException -> 0x00a3 }
            boolean r0 = r7.a(r0, r9)     // Catch:{ ClientProtocolException -> 0x00a5, IOException -> 0x00a3 }
            if (r3 == 0) goto L_0x0044
            org.apache.http.conn.ClientConnectionManager r1 = r3.getConnectionManager()     // Catch:{ Exception -> 0x0099 }
            r1.shutdown()     // Catch:{ Exception -> 0x0099 }
        L_0x0044:
            if (r2 == 0) goto L_0x0009
            boolean r1 = r2.isHeld()
            if (r1 == 0) goto L_0x0009
            r2.release()
            goto L_0x0009
        L_0x0050:
            r0 = move-exception
            r3 = r2
        L_0x0052:
            r0.printStackTrace()     // Catch:{ all -> 0x00a1 }
            if (r3 == 0) goto L_0x005e
            org.apache.http.conn.ClientConnectionManager r0 = r3.getConnectionManager()     // Catch:{ Exception -> 0x009d }
            r0.shutdown()     // Catch:{ Exception -> 0x009d }
        L_0x005e:
            if (r2 == 0) goto L_0x0069
            boolean r0 = r2.isHeld()
            if (r0 == 0) goto L_0x0069
        L_0x0066:
            r2.release()
        L_0x0069:
            r0 = r1
            goto L_0x0009
        L_0x006b:
            r0 = move-exception
            r3 = r2
        L_0x006d:
            r0.printStackTrace()     // Catch:{ all -> 0x00a1 }
            if (r3 == 0) goto L_0x0079
            org.apache.http.conn.ClientConnectionManager r0 = r3.getConnectionManager()     // Catch:{ Exception -> 0x009b }
            r0.shutdown()     // Catch:{ Exception -> 0x009b }
        L_0x0079:
            if (r2 == 0) goto L_0x0069
            boolean r0 = r2.isHeld()
            if (r0 == 0) goto L_0x0069
            goto L_0x0066
        L_0x0082:
            r0 = move-exception
            r3 = r2
        L_0x0084:
            if (r3 == 0) goto L_0x008d
            org.apache.http.conn.ClientConnectionManager r1 = r3.getConnectionManager()     // Catch:{ Exception -> 0x009f }
            r1.shutdown()     // Catch:{ Exception -> 0x009f }
        L_0x008d:
            if (r2 == 0) goto L_0x0098
            boolean r1 = r2.isHeld()
            if (r1 == 0) goto L_0x0098
            r2.release()
        L_0x0098:
            throw r0
        L_0x0099:
            r1 = move-exception
            goto L_0x0044
        L_0x009b:
            r0 = move-exception
            goto L_0x0079
        L_0x009d:
            r0 = move-exception
            goto L_0x005e
        L_0x009f:
            r1 = move-exception
            goto L_0x008d
        L_0x00a1:
            r0 = move-exception
            goto L_0x0084
        L_0x00a3:
            r0 = move-exception
            goto L_0x006d
        L_0x00a5:
            r0 = move-exception
            goto L_0x0052
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.pangu.graphic.i.a(java.lang.String, java.lang.String):boolean");
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r3v0 */
    /* JADX WARN: Type inference failed for: r3v1, types: [java.io.FileOutputStream] */
    /* JADX WARN: Type inference failed for: r3v2, types: [java.io.FileOutputStream] */
    /* JADX WARN: Type inference failed for: r3v3, types: [java.io.InputStream] */
    /* JADX WARN: Type inference failed for: r3v4 */
    /* JADX WARN: Type inference failed for: r3v5 */
    /* JADX WARN: Type inference failed for: r3v6 */
    /* JADX WARN: Type inference failed for: r3v7 */
    /* JADX WARN: Type inference failed for: r3v8 */
    /* JADX WARN: Type inference failed for: r3v10 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0047 A[SYNTHETIC, Splitter:B:27:0x0047] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x004c A[SYNTHETIC, Splitter:B:30:0x004c] */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0051 A[SYNTHETIC, Splitter:B:33:0x0051] */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x0075 A[SYNTHETIC, Splitter:B:53:0x0075] */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x007a A[SYNTHETIC, Splitter:B:56:0x007a] */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x007f A[SYNTHETIC, Splitter:B:59:0x007f] */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x008a A[SYNTHETIC, Splitter:B:65:0x008a] */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x008f A[SYNTHETIC, Splitter:B:68:0x008f] */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x0094 A[SYNTHETIC, Splitter:B:71:0x0094] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean a(org.apache.http.HttpResponse r8, java.lang.String r9) {
        /*
            r7 = this;
            r3 = 0
            r0 = 0
            if (r8 == 0) goto L_0x000a
            boolean r1 = android.text.TextUtils.isEmpty(r9)
            if (r1 == 0) goto L_0x000b
        L_0x000a:
            return r0
        L_0x000b:
            if (r8 == 0) goto L_0x000a
            org.apache.http.StatusLine r1 = r8.getStatusLine()
            int r1 = r1.getStatusCode()
            r2 = 200(0xc8, float:2.8E-43)
            if (r1 != r2) goto L_0x000a
            r1 = 0
            r2 = 0
            org.apache.http.HttpEntity r5 = r8.getEntity()     // Catch:{ IllegalStateException -> 0x00d9, IOException -> 0x006d, all -> 0x0085 }
            if (r5 == 0) goto L_0x00ad
            java.io.InputStream r4 = r5.getContent()     // Catch:{ IllegalStateException -> 0x00de, IOException -> 0x00d1, all -> 0x00c4 }
            java.io.File r1 = new java.io.File     // Catch:{ IllegalStateException -> 0x00e3, IOException -> 0x00d4 }
            r1.<init>(r9)     // Catch:{ IllegalStateException -> 0x00e3, IOException -> 0x00d4 }
            java.io.FileOutputStream r2 = new java.io.FileOutputStream     // Catch:{ IllegalStateException -> 0x00e3, IOException -> 0x00d4 }
            r2.<init>(r1)     // Catch:{ IllegalStateException -> 0x00e3, IOException -> 0x00d4 }
            r1 = 2048(0x800, float:2.87E-42)
            byte[] r1 = new byte[r1]     // Catch:{ IllegalStateException -> 0x003f, IOException -> 0x00d6, all -> 0x00c9 }
        L_0x0033:
            int r3 = r4.read(r1)     // Catch:{ IllegalStateException -> 0x003f, IOException -> 0x00d6, all -> 0x00c9 }
            r6 = -1
            if (r3 == r6) goto L_0x005a
            r6 = 0
            r2.write(r1, r6, r3)     // Catch:{ IllegalStateException -> 0x003f, IOException -> 0x00d6, all -> 0x00c9 }
            goto L_0x0033
        L_0x003f:
            r1 = move-exception
            r3 = r4
            r4 = r5
        L_0x0042:
            r1.printStackTrace()     // Catch:{ all -> 0x00cc }
            if (r3 == 0) goto L_0x004a
            r3.close()     // Catch:{ Exception -> 0x00a1 }
        L_0x004a:
            if (r2 == 0) goto L_0x004f
            r2.close()     // Catch:{ Exception -> 0x00a3 }
        L_0x004f:
            if (r4 == 0) goto L_0x000a
            r4.consumeContent()     // Catch:{ IOException -> 0x0055 }
            goto L_0x000a
        L_0x0055:
            r1 = move-exception
        L_0x0056:
            r1.printStackTrace()
            goto L_0x000a
        L_0x005a:
            r0 = 1
            if (r4 == 0) goto L_0x0060
            r4.close()     // Catch:{ Exception -> 0x00a9 }
        L_0x0060:
            if (r2 == 0) goto L_0x0065
            r2.close()     // Catch:{ Exception -> 0x00ab }
        L_0x0065:
            if (r5 == 0) goto L_0x000a
            r5.consumeContent()     // Catch:{ IOException -> 0x006b }
            goto L_0x000a
        L_0x006b:
            r1 = move-exception
            goto L_0x0056
        L_0x006d:
            r1 = move-exception
            r4 = r3
            r5 = r3
        L_0x0070:
            r1.printStackTrace()     // Catch:{ all -> 0x00c7 }
            if (r4 == 0) goto L_0x0078
            r4.close()     // Catch:{ Exception -> 0x00a5 }
        L_0x0078:
            if (r3 == 0) goto L_0x007d
            r3.close()     // Catch:{ Exception -> 0x00a7 }
        L_0x007d:
            if (r5 == 0) goto L_0x000a
            r5.consumeContent()     // Catch:{ IOException -> 0x0083 }
            goto L_0x000a
        L_0x0083:
            r1 = move-exception
            goto L_0x0056
        L_0x0085:
            r0 = move-exception
            r4 = r3
            r5 = r3
        L_0x0088:
            if (r4 == 0) goto L_0x008d
            r4.close()     // Catch:{ Exception -> 0x0098 }
        L_0x008d:
            if (r3 == 0) goto L_0x0092
            r3.close()     // Catch:{ Exception -> 0x009a }
        L_0x0092:
            if (r5 == 0) goto L_0x0097
            r5.consumeContent()     // Catch:{ IOException -> 0x009c }
        L_0x0097:
            throw r0
        L_0x0098:
            r1 = move-exception
            goto L_0x008d
        L_0x009a:
            r1 = move-exception
            goto L_0x0092
        L_0x009c:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0097
        L_0x00a1:
            r1 = move-exception
            goto L_0x004a
        L_0x00a3:
            r1 = move-exception
            goto L_0x004f
        L_0x00a5:
            r1 = move-exception
            goto L_0x0078
        L_0x00a7:
            r1 = move-exception
            goto L_0x007d
        L_0x00a9:
            r1 = move-exception
            goto L_0x0060
        L_0x00ab:
            r1 = move-exception
            goto L_0x0065
        L_0x00ad:
            if (r3 == 0) goto L_0x00b2
            r1.close()     // Catch:{ Exception -> 0x00c0 }
        L_0x00b2:
            if (r3 == 0) goto L_0x00b7
            r2.close()     // Catch:{ Exception -> 0x00c2 }
        L_0x00b7:
            if (r5 == 0) goto L_0x000a
            r5.consumeContent()     // Catch:{ IOException -> 0x00be }
            goto L_0x000a
        L_0x00be:
            r1 = move-exception
            goto L_0x0056
        L_0x00c0:
            r1 = move-exception
            goto L_0x00b2
        L_0x00c2:
            r1 = move-exception
            goto L_0x00b7
        L_0x00c4:
            r0 = move-exception
            r4 = r3
            goto L_0x0088
        L_0x00c7:
            r0 = move-exception
            goto L_0x0088
        L_0x00c9:
            r0 = move-exception
            r3 = r2
            goto L_0x0088
        L_0x00cc:
            r0 = move-exception
            r5 = r4
            r4 = r3
            r3 = r2
            goto L_0x0088
        L_0x00d1:
            r1 = move-exception
            r4 = r3
            goto L_0x0070
        L_0x00d4:
            r1 = move-exception
            goto L_0x0070
        L_0x00d6:
            r1 = move-exception
            r3 = r2
            goto L_0x0070
        L_0x00d9:
            r1 = move-exception
            r2 = r3
            r4 = r3
            goto L_0x0042
        L_0x00de:
            r1 = move-exception
            r2 = r3
            r4 = r5
            goto L_0x0042
        L_0x00e3:
            r1 = move-exception
            r2 = r3
            r3 = r4
            r4 = r5
            goto L_0x0042
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.pangu.graphic.i.a(org.apache.http.HttpResponse, java.lang.String):boolean");
    }

    /* access modifiers changed from: private */
    public String a(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        if (str.startsWith("http")) {
            return FileUtil.getGifDir() + File.separator + b(str);
        }
        return str;
    }

    /* access modifiers changed from: private */
    public e a(String str, int i, int i2) {
        int i3;
        int i4;
        int i5;
        float f = r.f();
        if (((double) f) < 0.6d) {
            i3 = 5;
        } else {
            i3 = (int) (f * ((float) i));
        }
        e eVar = new e();
        eVar.f3771a = false;
        eVar.d = str;
        try {
            c cVar = new c(new FileInputStream(str), null, true, 0);
            cVar.a(i3);
            if (cVar.a() == 1) {
                a aVar = new a();
                aVar.a(1);
                aVar.b(i2);
                BitmapDrawable bitmapDrawable = new BitmapDrawable(BitmapFactory.decodeFile(str));
                bitmapDrawable.setTargetDensity(AstApp.i().getResources().getDisplayMetrics());
                aVar.addFrame(bitmapDrawable, 1000);
                eVar.c = aVar;
                eVar.f3771a = true;
            } else {
                if (cVar.b() == -1) {
                    i4 = cVar.c();
                } else {
                    i4 = 0;
                }
                if (i4 > i3) {
                    i5 = i4 / i3;
                } else {
                    i5 = 0;
                }
                if (i5 > 4) {
                    i5 = 4;
                }
                c cVar2 = new c(new FileInputStream(str), null, false, i5);
                cVar2.a();
                if (cVar2.b() == -1) {
                    int c2 = cVar2.c();
                    if (c2 == 0) {
                        eVar.f3771a = false;
                    } else {
                        a aVar2 = new a();
                        aVar2.a(c2);
                        aVar2.b(i2);
                        f c3 = cVar2.c(0);
                        for (int i6 = 0; i6 < c2 && c3 != null; i6++) {
                            BitmapDrawable bitmapDrawable2 = new BitmapDrawable(c3.f3772a);
                            bitmapDrawable2.setTargetDensity(AstApp.i().getResources().getDisplayMetrics());
                            aVar2.addFrame(bitmapDrawable2, c3.b <= 0 ? 100 : c3.b);
                            c3 = c3.c;
                        }
                        aVar2.setOneShot(false);
                        eVar.c = aVar2;
                        eVar.f3771a = true;
                    }
                } else {
                    eVar.f3771a = false;
                }
            }
        } catch (Throwable th) {
            eVar.b = true;
            th.printStackTrace();
        }
        return eVar;
    }

    private String b(String str) {
        return aq.b(str);
    }
}
