package com.tencent.pangu.adapter;

import android.widget.ImageView;
import com.tencent.assistantv2.component.k;
import com.tencent.pangu.download.DownloadInfo;
import com.tencent.pangu.download.a;

/* compiled from: ProGuard */
class ax extends k {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ RankNormalListAdapter f3426a;

    ax(RankNormalListAdapter rankNormalListAdapter) {
        this.f3426a = rankNormalListAdapter;
    }

    public void a(DownloadInfo downloadInfo) {
        if (downloadInfo != null) {
            a.a().a(downloadInfo);
            com.tencent.assistant.utils.a.a((ImageView) this.f3426a.i.findViewWithTag(downloadInfo.downloadTicket));
        }
    }
}
