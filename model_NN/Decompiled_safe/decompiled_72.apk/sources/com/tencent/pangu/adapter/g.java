package com.tencent.pangu.adapter;

import android.widget.Toast;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.pangu.download.DownloadInfo;
import com.tencent.pangu.download.a;
import com.tencent.pangu.manager.DownloadProxy;

/* compiled from: ProGuard */
class g extends AppConst.TwoBtnDialogInfo {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DownloadInfo f3448a;
    final /* synthetic */ DownloadInfoMultiAdapter b;

    g(DownloadInfoMultiAdapter downloadInfoMultiAdapter, DownloadInfo downloadInfo) {
        this.b = downloadInfoMultiAdapter;
        this.f3448a = downloadInfo;
    }

    public void onLeftBtnClick() {
    }

    public void onRightBtnClick() {
        this.f3448a.response = null;
        DownloadProxy.a().b(this.f3448a.downloadTicket);
        a.a().a(this.f3448a);
        Toast.makeText(this.b.m, this.b.m.getResources().getString(R.string.down_add_tips), 0).show();
    }

    public void onCancell() {
    }
}
