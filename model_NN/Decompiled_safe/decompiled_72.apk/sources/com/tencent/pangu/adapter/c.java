package com.tencent.pangu.adapter;

import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class c extends PagerAdapter {

    /* renamed from: a  reason: collision with root package name */
    private List<View> f3444a = new ArrayList();
    private int b = 0;

    public c(List<View> list) {
        if (list != null) {
            this.f3444a.addAll(list);
        }
    }

    public void a(List<View> list) {
        this.f3444a.clear();
        this.f3444a.addAll(list);
    }

    public void destroyItem(ViewGroup viewGroup, int i, Object obj) {
    }

    public Object instantiateItem(ViewGroup viewGroup, int i) {
        if (((this.b >> i) & 1) != 1) {
            viewGroup.addView(this.f3444a.get(i));
            this.b += 1 << i;
        }
        return this.f3444a.get(i);
    }

    public int getCount() {
        return this.f3444a.size();
    }

    public boolean isViewFromObject(View view, Object obj) {
        return view == obj;
    }

    public View a(int i) {
        return this.f3444a.get(i);
    }
}
