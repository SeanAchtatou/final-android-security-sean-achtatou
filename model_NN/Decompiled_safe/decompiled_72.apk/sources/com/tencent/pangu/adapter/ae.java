package com.tencent.pangu.adapter;

import com.tencent.pangu.download.DownloadInfoWrapper;
import java.util.Comparator;

/* compiled from: ProGuard */
public class ae implements Comparator<DownloadInfoWrapper> {
    /* renamed from: a */
    public int compare(DownloadInfoWrapper downloadInfoWrapper, DownloadInfoWrapper downloadInfoWrapper2) {
        return (int) (downloadInfoWrapper2.a() - downloadInfoWrapper.a());
    }
}
