package com.tencent.assistantv2.adapter.smartlist;

import android.view.View;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistantv2.component.k;
import com.tencent.pangu.download.DownloadInfo;
import com.tencent.pangu.download.a;

/* compiled from: ProGuard */
class u extends k {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppConst.AppState f1919a;
    final /* synthetic */ v b;
    final /* synthetic */ s c;

    u(s sVar, AppConst.AppState appState, v vVar) {
        this.c = sVar;
        this.f1919a = appState;
        this.b = vVar;
    }

    public void a(DownloadInfo downloadInfo) {
        if (!(this.f1919a != AppConst.AppState.DOWNLOAD || this.b == null || this.b.h == null)) {
            this.b.h.setVisibility(8);
        }
        a.a().a(downloadInfo);
    }

    public void a(SimpleAppModel simpleAppModel, View view) {
        this.c.a(simpleAppModel, view);
    }
}
