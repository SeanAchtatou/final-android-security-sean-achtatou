package com.tencent.assistantv2.adapter.smartlist;

import android.content.Context;
import android.text.TextUtils;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.component.invalidater.IViewInvalidater;
import com.tencent.assistant.component.txscrollview.TXAppIconView;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.manager.t;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.model.d;
import com.tencent.assistantv2.component.DownloadButton;
import com.tencent.assistantv2.component.ListItemInfoView;
import com.tencent.assistantv2.component.ListRecommendReasonView;
import com.tencent.assistantv2.component.fps.FPSNormalItemSubView;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import com.tencent.connect.common.Constants;
import com.tencent.pangu.component.appdetail.process.s;
import com.tencent.pangu.model.b;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class n extends a {
    aa c;
    private IViewInvalidater d;
    private LayoutInflater e;

    public n(Context context, aa aaVar, IViewInvalidater iViewInvalidater) {
        super(context, aaVar);
        this.c = aaVar;
        this.d = iViewInvalidater;
        this.e = LayoutInflater.from(context);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, ?[OBJECT, ARRAY], int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public Pair<View, Object> a() {
        View inflate = LayoutInflater.from(this.f1900a).inflate((int) R.layout.game_cardlist, (ViewGroup) null, false);
        q qVar = new q();
        qVar.f1916a = inflate.findViewById(R.id.title_icon);
        qVar.b = (LinearLayout) inflate.findViewById(R.id.card_list);
        qVar.c = new ArrayList(3);
        return Pair.create(inflate, qVar);
    }

    public void a(View view, Object obj, int i, b bVar) {
        List<SimpleAppModel> list;
        q qVar = (q) obj;
        if (bVar != null) {
            list = bVar.b();
        } else {
            list = null;
        }
        if (list != null) {
            a(qVar, list, i, bVar.a());
        }
    }

    private void a(q qVar, List<SimpleAppModel> list, int i, int i2) {
        String str;
        if (list != null && qVar != null && list.size() > 0) {
            String str2 = "08";
            if (i2 == 1) {
                try {
                    qVar.f1916a.setBackgroundResource(R.drawable.common_gamecard_title_01);
                    str2 = "08";
                } catch (Throwable th) {
                    t.a().b();
                    str = str2;
                }
            } else if (i2 == 2) {
                qVar.f1916a.setBackgroundResource(R.drawable.common_gamecard_title_02);
                str2 = "09";
            } else if (i2 == 3) {
                qVar.f1916a.setBackgroundResource(R.drawable.common_gamecard_title_03);
                str2 = Constants.VIA_REPORT_TYPE_SHARE_TO_QQ;
            } else if (i2 == 4) {
                qVar.f1916a.setBackgroundResource(R.drawable.common_gamecard_title_04);
                str2 = Constants.VIA_REPORT_TYPE_SHARE_TO_QZONE;
            }
            str = str2;
            a(qVar, list, i, i2, str);
        } else if (qVar != null) {
            qVar.f1916a.setVisibility(8);
        }
    }

    private void a(q qVar, List<SimpleAppModel> list, int i, int i2, String str) {
        int size = list.size() - qVar.c.size();
        if (size <= 0) {
            int size2 = qVar.c.size() + size;
            while (true) {
                int i3 = size2;
                if (i3 >= qVar.c.size()) {
                    break;
                }
                qVar.c.get(i3).j.setVisibility(8);
                size2 = i3 + 1;
            }
        } else {
            for (int i4 = 0; i4 < size; i4++) {
                p b = b();
                qVar.b.addView(b.j, new LinearLayout.LayoutParams(-1, -2));
                qVar.c.add(b);
            }
        }
        for (int i5 = 0; i5 < list.size(); i5++) {
            a(qVar.c.get(i5), list.get(i5), list.size(), i5, i2, str);
        }
    }

    private void a(p pVar, SimpleAppModel simpleAppModel, int i, int i2, int i3, String str) {
        AppConst.AppState appState;
        if (simpleAppModel != null && pVar != null) {
            d a2 = this.b.a();
            if (a2 != null) {
                appState = a2.c;
            } else {
                appState = null;
            }
            STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f1900a, simpleAppModel, a.a(str, i2), 100, null, a2);
            if (!(this.c == null || this.c.c() == null || buildSTInfo == null || this.c.h() != buildSTInfo.scene)) {
                this.c.c().exposure(buildSTInfo);
            }
            pVar.j.setVisibility(0);
            pVar.j.setOnClickListener(new r(this.f1900a, i2, simpleAppModel, this.c, buildSTInfo));
            a(pVar, simpleAppModel);
            pVar.e.a(ListItemInfoView.InfoType.CATEGORY_SIZE);
            pVar.e.a(simpleAppModel, a2);
            pVar.c.setText(simpleAppModel.d);
            pVar.b.updateImageView(simpleAppModel.e, R.drawable.pic_defaule, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
            pVar.d.a(simpleAppModel, a2);
            if (pVar.f != null) {
                if (!this.c.k()) {
                    pVar.f.setVisibility(4);
                } else if (TextUtils.isEmpty(simpleAppModel.X)) {
                    pVar.f.setVisibility(4);
                } else {
                    pVar.f.setText(simpleAppModel.X);
                    pVar.f.setVisibility(0);
                }
            }
            if (pVar.g != null) {
                pVar.g.a(simpleAppModel, this.d);
            }
            if (s.a(simpleAppModel, appState)) {
                pVar.d.setClickable(false);
            } else {
                pVar.d.setClickable(true);
                pVar.d.a(buildSTInfo, null, a2);
            }
            pVar.h.setVisibility(0);
            if (i2 == i - 1) {
                pVar.i.setVisibility(8);
            } else {
                pVar.i.setVisibility(0);
            }
            com.tencent.assistant.adapter.a.a(this.f1900a, simpleAppModel, pVar.c, false);
        }
    }

    private void a(p pVar, SimpleAppModel simpleAppModel) {
        if (TextUtils.isEmpty(simpleAppModel.ae) && !s.c(simpleAppModel)) {
            pVar.f1915a.setVisibility(8);
        } else if (!TextUtils.isEmpty(simpleAppModel.ae)) {
            pVar.f1915a.setVisibility(0);
            pVar.f1915a.setText(simpleAppModel.ae);
        } else if (s.c(simpleAppModel)) {
            pVar.f1915a.setVisibility(8);
        }
    }

    private p b() {
        p pVar = new p(this);
        FPSNormalItemSubView fPSNormalItemSubView = new FPSNormalItemSubView(this.f1900a);
        pVar.j = fPSNormalItemSubView;
        pVar.f1915a = (TextView) fPSNormalItemSubView.findViewById(R.id.download_rate_desc);
        pVar.b = (TXAppIconView) fPSNormalItemSubView.findViewById(R.id.app_icon_img);
        pVar.b.setInvalidater(this.d);
        pVar.c = (TextView) fPSNormalItemSubView.findViewById(R.id.title);
        pVar.d = (DownloadButton) fPSNormalItemSubView.findViewById(R.id.state_app_btn);
        pVar.e = (ListItemInfoView) fPSNormalItemSubView.findViewById(R.id.download_info);
        pVar.h = fPSNormalItemSubView.findViewById(R.id.empty_padding_bottom);
        pVar.f = (TextView) fPSNormalItemSubView.findViewById(R.id.desc);
        pVar.g = (ListRecommendReasonView) fPSNormalItemSubView.findViewById(R.id.reasonView);
        pVar.i = (ImageView) fPSNormalItemSubView.findViewById(R.id.app_one_more_item_line);
        return pVar;
    }
}
