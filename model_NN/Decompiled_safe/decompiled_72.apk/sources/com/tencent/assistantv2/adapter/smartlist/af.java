package com.tencent.assistantv2.adapter.smartlist;

import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.smartcard.c.p;
import com.tencent.assistant.smartcard.component.as;
import com.tencent.assistant.smartcard.d.n;
import com.tencent.pangu.model.b;
import java.util.Iterator;

/* compiled from: ProGuard */
class af implements as {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SmartListAdapter f1904a;

    af(SmartListAdapter smartListAdapter) {
        this.f1904a = smartListAdapter;
    }

    public void b(int i, int i2) {
        b bVar;
        n f;
        Iterator it = this.f1904a.h.iterator();
        while (true) {
            if (!it.hasNext()) {
                bVar = null;
                break;
            }
            bVar = (b) it.next();
            if (bVar.b == 2 && (f = bVar.f()) != null && f.j == i && f.k == i2) {
                break;
            }
        }
        if (bVar != null) {
            this.f1904a.h.remove(bVar);
            this.f1904a.notifyDataSetChanged();
        }
        p.a().b(i, i2);
    }

    public void a(int i, int i2) {
        p.a().a(i, i2);
    }

    public void a(int i, int i2, SimpleAppModel simpleAppModel) {
        p.a().a(i, i2, simpleAppModel);
    }
}
