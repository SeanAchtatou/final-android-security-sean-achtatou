package com.tencent.assistantv2.activity;

import android.os.Bundle;
import com.tencent.assistant.AppConst;
import com.tencent.nucleus.socialcontact.login.j;

/* compiled from: ProGuard */
class q implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ p f1883a;

    q(p pVar) {
        this.f1883a = pVar;
    }

    public void run() {
        if (this.f1883a.f1882a.getBooleanExtra("action_key_from_guide", false)) {
            Bundle bundle = new Bundle();
            bundle.putInt(AppConst.KEY_LOGIN_TYPE, 5);
            bundle.putInt(AppConst.KEY_FROM_TYPE, 18);
            j.a().a(AppConst.IdentityType.MOBILEQ, bundle);
            return;
        }
        this.f1883a.b.b(this.f1883a.f1882a);
    }
}
