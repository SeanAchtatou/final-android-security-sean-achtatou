package com.tencent.assistantv2.activity;

import android.support.v4.view.ViewPager;
import android.view.View;
import com.tencent.android.qqdownloader.R;

/* compiled from: ProGuard */
public class ac implements ViewPager.OnPageChangeListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ MainActivity f1866a;

    public ac(MainActivity mainActivity) {
        this.f1866a = mainActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistantv2.activity.MainActivity.b(com.tencent.assistantv2.activity.MainActivity, boolean):boolean
     arg types: [com.tencent.assistantv2.activity.MainActivity, int]
     candidates:
      com.tencent.assistantv2.activity.MainActivity.b(com.tencent.assistantv2.activity.MainActivity, int):void
      com.tencent.assistantv2.activity.MainActivity.b(com.tencent.assistantv2.activity.MainActivity, java.util.ArrayList):void
      com.tencent.assistantv2.activity.MainActivity.b(com.tencent.assistantv2.activity.MainActivity, boolean):boolean */
    public void onPageSelected(int i) {
        boolean z;
        for (int i2 = 0; i2 < this.f1866a.I.getCount(); i2++) {
            View b = this.f1866a.J.b(i2);
            if (b != null) {
                if (i2 == i) {
                    z = true;
                } else {
                    z = false;
                }
                b.setSelected(z);
            }
        }
        if (!(this.f1866a.L == i || this.f1866a.L >= this.f1866a.I.getCount() || this.f1866a.I.a(this.f1866a.L) == null)) {
            ((a) this.f1866a.I.a(this.f1866a.L)).D();
        }
        String b2 = this.f1866a.b(i);
        if (i == 5) {
            this.f1866a.a(this.f1866a.L, i + 2, b2);
        } else {
            this.f1866a.a(this.f1866a.L, i, b2);
        }
        ((a) this.f1866a.I.a(i)).C();
        int unused = this.f1866a.L = i;
        if (this.f1866a.O) {
            this.f1866a.K.a(this.f1866a.L, this.f1866a.S);
            boolean unused2 = this.f1866a.O = false;
        }
        this.f1866a.h(i);
        this.f1866a.c(i);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistantv2.activity.MainActivity.b(com.tencent.assistantv2.activity.MainActivity, boolean):boolean
     arg types: [com.tencent.assistantv2.activity.MainActivity, int]
     candidates:
      com.tencent.assistantv2.activity.MainActivity.b(com.tencent.assistantv2.activity.MainActivity, int):void
      com.tencent.assistantv2.activity.MainActivity.b(com.tencent.assistantv2.activity.MainActivity, java.util.ArrayList):void
      com.tencent.assistantv2.activity.MainActivity.b(com.tencent.assistantv2.activity.MainActivity, boolean):boolean */
    public void onPageScrolled(int i, float f, int i2) {
        boolean unused = this.f1866a.O = false;
        this.f1866a.K.a((int) (((float) this.f1866a.getResources().getDimensionPixelSize(R.dimen.navigation_curcor_padding_left)) + ((((float) i) + f) * this.f1866a.S)));
    }

    public void onPageScrollStateChanged(int i) {
    }
}
