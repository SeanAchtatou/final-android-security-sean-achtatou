package com.tencent.assistantv2.activity;

import com.tencent.assistant.m;
import com.tencent.assistant.protocol.jce.GetPhoneUserAppListResponse;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.an;
import com.tencent.pangu.module.at;
import com.tencent.pangu.module.timer.RecoverAppListReceiver;
import com.tencent.pangu.utils.e;

/* compiled from: ProGuard */
class t implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ MainActivity f1886a;

    t(MainActivity mainActivity) {
        this.f1886a = mainActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistantv2.activity.MainActivity.a(com.tencent.assistantv2.activity.MainActivity, boolean):void
     arg types: [com.tencent.assistantv2.activity.MainActivity, int]
     candidates:
      com.tencent.assistantv2.activity.MainActivity.a(com.tencent.assistantv2.activity.MainActivity, int):int
      com.tencent.assistantv2.activity.MainActivity.a(com.tencent.assistantv2.activity.MainActivity, com.tencent.assistant.db.table.ab):com.tencent.assistant.db.table.ab
      com.tencent.assistantv2.activity.MainActivity.a(com.tencent.assistantv2.activity.MainActivity, java.util.ArrayList):java.util.ArrayList
      com.tencent.assistantv2.activity.MainActivity.a(com.tencent.assistant.protocol.jce.GetPhoneUserAppListResponse, boolean):void
      com.tencent.assistantv2.activity.MainActivity.a(com.tencent.assistantv2.activity.MainActivity, android.content.Intent):void
      com.tencent.assistantv2.activity.MainActivity.a(com.tencent.assistantv2.activity.MainActivity, com.tencent.assistant.protocol.jce.DesktopShortCut):void
      com.tencent.assistantv2.activity.MainActivity.a(com.tencent.assistant.protocol.jce.DesktopShortCut, com.tencent.assistant.protocol.jce.DesktopShortCut):boolean
      com.tencent.assistantv2.activity.MainActivity.a(int, boolean):void
      com.tencent.assistant.activity.BaseActivity.a(java.lang.String, java.lang.String):void
      com.tencent.assistantv2.activity.MainActivity.a(com.tencent.assistantv2.activity.MainActivity, boolean):void */
    public void run() {
        MainActivity.z = m.a().a("key_re_app_list_state", 0);
        XLog.d("KEY_RECOVER_APP_LIST_STATE", "KEY_RECOVER_APP_LIST_STATE:" + MainActivity.z);
        XLog.i("PopUpNecessaryAcitivity", "mainActivity mReCoverAppListState:" + MainActivity.z);
        if (MainActivity.z == RecoverAppListReceiver.RecoverAppListState.FIRST_SHORT.ordinal()) {
            byte[] e = m.a().e("key_re_app_list_first_response");
            this.f1886a.B = (GetPhoneUserAppListResponse) an.b(e, GetPhoneUserAppListResponse.class);
            if (this.f1886a.B != null) {
                XLog.i("PopUpNecessaryAcitivity", "mainActivity operationType:" + this.f1886a.B.b);
                switch (this.f1886a.B.b) {
                    case 0:
                    default:
                        return;
                    case 1:
                    case 2:
                        e.a(this.f1886a, false, this.f1886a.f(), this.f1886a.B, false);
                        return;
                    case 3:
                        XLog.i("GetPopUpNecessaryEngines", "main activity sendRequest!");
                        at.a().b();
                        this.f1886a.d(this.f1886a.w);
                        return;
                    case 4:
                        at.a().b();
                        e.a(this.f1886a, false, this.f1886a.f(), null, true);
                        return;
                }
            }
        } else if (MainActivity.z == RecoverAppListReceiver.RecoverAppListState.SECONDLY_SHORT.ordinal()) {
            GetPhoneUserAppListResponse getPhoneUserAppListResponse = (GetPhoneUserAppListResponse) an.b(m.a().e("key_re_app_list_second_response"), GetPhoneUserAppListResponse.class);
            if (getPhoneUserAppListResponse != null) {
                e.a(this.f1886a, false, this.f1886a.f(), getPhoneUserAppListResponse, false);
            } else {
                this.f1886a.c(false);
            }
        }
    }
}
