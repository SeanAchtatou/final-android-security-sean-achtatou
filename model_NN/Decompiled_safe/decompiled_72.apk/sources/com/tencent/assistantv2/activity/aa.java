package com.tencent.assistantv2.activity;

import com.qq.AppService.AstApp;
import com.tencent.assistant.m;
import com.tencent.assistant.protocol.jce.GetPhoneUserAppListResponse;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.an;
import com.tencent.connect.common.Constants;
import com.tencent.pangu.module.a.g;
import com.tencent.pangu.module.timer.RecoverAppListReceiver;

/* compiled from: ProGuard */
class aa implements g {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ MainActivity f1864a;

    private aa(MainActivity mainActivity) {
        this.f1864a = mainActivity;
    }

    /* synthetic */ aa(MainActivity mainActivity, i iVar) {
        this(mainActivity);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistantv2.activity.MainActivity.a(com.tencent.assistantv2.activity.MainActivity, com.tencent.assistant.protocol.jce.GetPhoneUserAppListResponse, boolean):void
     arg types: [com.tencent.assistantv2.activity.MainActivity, com.tencent.assistant.protocol.jce.GetPhoneUserAppListResponse, int]
     candidates:
      com.tencent.assistantv2.activity.MainActivity.a(int, int, java.lang.String):void
      com.tencent.assistantv2.activity.MainActivity.a(android.graphics.Bitmap, java.lang.String, android.content.Intent):void
      com.tencent.assistantv2.activity.MainActivity.a(com.tencent.assistantv2.a.i, int, int):void
      com.tencent.assistantv2.activity.MainActivity.a(com.tencent.assistantv2.activity.MainActivity, com.tencent.assistant.protocol.jce.DesktopShortCut, com.tencent.assistant.protocol.jce.DesktopShortCut):boolean
      com.tencent.assistant.activity.BaseActivity.a(com.tencent.assistantv2.st.page.STPageInfo, java.lang.String, java.lang.String):void
      android.support.v4.app.FragmentActivity.a(java.lang.String, java.io.PrintWriter, android.view.View):void
      android.support.v4.app.FragmentActivity.a(java.lang.String, boolean, boolean):android.support.v4.app.z
      android.support.v4.app.FragmentActivity.a(android.support.v4.app.Fragment, android.content.Intent, int):void
      com.tencent.assistantv2.activity.MainActivity.a(com.tencent.assistantv2.activity.MainActivity, com.tencent.assistant.protocol.jce.GetPhoneUserAppListResponse, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistantv2.activity.MainActivity.a(com.tencent.assistantv2.activity.MainActivity, boolean):void
     arg types: [com.tencent.assistantv2.activity.MainActivity, int]
     candidates:
      com.tencent.assistantv2.activity.MainActivity.a(com.tencent.assistantv2.activity.MainActivity, int):int
      com.tencent.assistantv2.activity.MainActivity.a(com.tencent.assistantv2.activity.MainActivity, com.tencent.assistant.db.table.ab):com.tencent.assistant.db.table.ab
      com.tencent.assistantv2.activity.MainActivity.a(com.tencent.assistantv2.activity.MainActivity, java.util.ArrayList):java.util.ArrayList
      com.tencent.assistantv2.activity.MainActivity.a(com.tencent.assistant.protocol.jce.GetPhoneUserAppListResponse, boolean):void
      com.tencent.assistantv2.activity.MainActivity.a(com.tencent.assistantv2.activity.MainActivity, android.content.Intent):void
      com.tencent.assistantv2.activity.MainActivity.a(com.tencent.assistantv2.activity.MainActivity, com.tencent.assistant.protocol.jce.DesktopShortCut):void
      com.tencent.assistantv2.activity.MainActivity.a(com.tencent.assistant.protocol.jce.DesktopShortCut, com.tencent.assistant.protocol.jce.DesktopShortCut):boolean
      com.tencent.assistantv2.activity.MainActivity.a(int, boolean):void
      com.tencent.assistant.activity.BaseActivity.a(java.lang.String, java.lang.String):void
      com.tencent.assistantv2.activity.MainActivity.a(com.tencent.assistantv2.activity.MainActivity, boolean):void */
    public void a(int i, int i2, GetPhoneUserAppListResponse getPhoneUserAppListResponse) {
        XLog.i("PopUpNecessaryAcitivity", "mainActivity DataCallback!  resp:" + getPhoneUserAppListResponse);
        if (i2 != 0 || getPhoneUserAppListResponse == null || getPhoneUserAppListResponse.c == null || getPhoneUserAppListResponse.c.size() <= 0) {
            this.f1864a.c(true);
            return;
        }
        m.a().b(Constants.STR_EMPTY, "key_re_app_list_state", Integer.valueOf(RecoverAppListReceiver.RecoverAppListState.SECONDLY_SHORT.ordinal()));
        m.a().a("key_re_app_list_second_response", an.a(getPhoneUserAppListResponse));
        MainActivity.z = RecoverAppListReceiver.RecoverAppListState.SECONDLY_SHORT.ordinal();
        XLog.e("zhangyuanchao", "-------onRecoverAppListGet------:" + AstApp.i().l() + ",----");
        switch (getPhoneUserAppListResponse.b) {
            case 0:
            case 3:
            case 4:
                this.f1864a.a(getPhoneUserAppListResponse, true);
                return;
            case 1:
            case 2:
                this.f1864a.a(getPhoneUserAppListResponse, false);
                return;
            default:
                return;
        }
    }
}
