package com.tencent.assistantv2.st.business;

import android.content.Context;
import android.os.Build;
import com.qq.AppService.AstApp;
import com.tencent.assistant.Global;
import com.tencent.assistant.plugin.PluginInfo;
import com.tencent.assistant.plugin.mgr.i;
import com.tencent.assistant.utils.FileUtil;
import com.tencent.connect.common.Constants;
import com.tencent.feedback.eup.b;
import com.tencent.feedback.eup.c;
import com.tencent.feedback.eup.d;
import java.io.File;
import java.util.List;

/* compiled from: ProGuard */
public class k extends BaseSTManagerV2 {
    public byte getSTType() {
        return 1;
    }

    public void flush() {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.eup.c.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.tencent.feedback.eup.c.a(android.content.Context, java.lang.String):void
      com.tencent.feedback.eup.c.a(boolean, boolean):void */
    public static void a(boolean z) {
        Context applicationContext = AstApp.i().getApplicationContext();
        c.a(false, false);
        c.a(true);
        c.b(applicationContext, Global.getPhoneGuidAndGen());
        c.c(applicationContext, Global.getAppVersionName4RQD() + "_" + Global.getBuildNo());
        c.a(applicationContext, c(), e(), z, a());
        File file = new File(FileUtil.getLogDir() + "/exception");
        if (!file.exists()) {
            file.mkdirs();
        }
        if (Build.VERSION.SDK_INT >= 8) {
            c.a(applicationContext, file.getAbsolutePath(), true);
        }
        c.b(true);
    }

    private static b c() {
        return new l();
    }

    private static com.tencent.feedback.b.b e() {
        return new m();
    }

    public static d a() {
        d dVar = new d();
        dVar.a(false);
        return dVar;
    }

    /* access modifiers changed from: private */
    public static String f() {
        try {
            StringBuilder sb = new StringBuilder();
            List<PluginInfo> a2 = i.b().a(-1);
            if (a2 != null) {
                for (PluginInfo next : a2) {
                    sb.append(next.getPackageName()).append(",").append(next.getVersion()).append(";");
                }
            }
            return sb.toString();
        } catch (Exception e) {
            return Constants.STR_EMPTY;
        }
    }
}
