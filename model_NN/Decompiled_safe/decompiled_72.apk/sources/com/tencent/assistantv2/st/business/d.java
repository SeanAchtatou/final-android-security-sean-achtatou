package com.tencent.assistantv2.st.business;

import com.tencent.assistant.db.table.z;
import com.tencent.assistant.protocol.jce.StatTraffic;
import com.tencent.assistant.utils.an;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class d extends BaseSTManagerV2 {

    /* renamed from: a  reason: collision with root package name */
    private static d f2033a = null;
    private ArrayList<StatTraffic> c;
    private z d;

    public d() {
        this.c = null;
        this.d = null;
        this.c = new ArrayList<>();
        this.d = z.a();
    }

    public byte getSTType() {
        return 3;
    }

    public static void a(List<StatTraffic> list) {
        if (list != null && list.size() > 0) {
            ArrayList arrayList = new ArrayList();
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 < list.size()) {
                    byte[] a2 = an.a(list.get(i2));
                    if (a2 != null && a2.length > 0) {
                        arrayList.add(a2);
                    }
                    i = i2 + 1;
                } else {
                    z.a().a((byte) 3, arrayList);
                    return;
                }
            }
        }
    }

    public void flush() {
    }
}
