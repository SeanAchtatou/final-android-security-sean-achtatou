package com.tencent.assistantv2.st.business;

import com.tencent.assistant.db.table.z;
import com.tencent.assistant.utils.TemporaryThreadManager;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public abstract class e extends BaseSTManagerV2 {

    /* renamed from: a  reason: collision with root package name */
    protected List<byte[]> f2034a;
    /* access modifiers changed from: private */
    public z c;

    protected e() {
        this.c = null;
        this.f2034a = null;
        this.f2034a = new ArrayList(5);
        this.c = z.a();
    }

    public void flush() {
        if (this.f2034a != null && this.f2034a.size() > 0) {
            this.c.a(getSTType(), this.f2034a);
            this.f2034a.clear();
        }
    }

    public void a(byte[] bArr) {
        TemporaryThreadManager.get().start(new f(this, bArr));
    }
}
