package com.tencent.assistantv2.component;

import com.tencent.android.qqdownloader.R;

/* compiled from: ProGuard */
class ab {

    /* renamed from: a  reason: collision with root package name */
    public String f1949a;
    public String b;
    public int c;
    public int d;
    public int e;
    public int f;
    public int g;
    public int h;

    public ab(int i, int i2, int i3) {
        this.c = R.drawable.card_avatar;
        this.f = -2;
        this.g = -2;
        this.h = -2;
        this.c = i;
        this.d = i2;
        this.e = i3;
    }

    public ab(String str, int i, int i2, int i3, int i4, int i5) {
        this.c = R.drawable.card_avatar;
        this.f = -2;
        this.g = -2;
        this.h = -2;
        this.b = str;
        this.d = i;
        this.e = i2;
        this.c = i3;
        this.f = i4;
        this.g = i5;
    }

    public ab(String str, int i, int i2, int i3, int i4) {
        this.c = R.drawable.card_avatar;
        this.f = -2;
        this.g = -2;
        this.h = -2;
        this.b = str;
        this.d = i;
        this.e = i2;
        this.f = i3;
        this.g = i4;
    }

    private ab(String str, String str2, int i, int i2, int i3, int i4) {
        this.c = R.drawable.card_avatar;
        this.f = -2;
        this.g = -2;
        this.h = -2;
        this.f1949a = str;
        this.b = str2;
        this.d = i;
        this.e = i2;
        this.f = i3;
        this.g = i4;
    }
}
