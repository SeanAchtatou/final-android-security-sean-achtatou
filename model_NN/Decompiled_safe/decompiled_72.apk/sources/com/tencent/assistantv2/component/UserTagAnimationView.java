package com.tencent.assistantv2.component;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.android.qqdownloader.b;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.component.txscrollview.TXTabBarLayout;
import com.tencent.assistant.protocol.jce.AppTagInfo;
import com.tencent.assistantv2.activity.GuideActivity;
import com.tencent.connect.common.Constants;
import com.tencent.nucleus.socialcontact.login.j;
import com.tencent.nucleus.socialcontact.login.l;
import com.tencent.nucleus.socialcontact.login.m;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class UserTagAnimationView extends RelativeLayout {

    /* renamed from: a  reason: collision with root package name */
    private static final String f1946a = UserTagAnimationView.class.getSimpleName();
    private static int d = 3;
    private static float e = 152.0f;
    private static float f = 110.0f;
    /* access modifiers changed from: private */
    public Context b = null;
    private LayoutInflater c = null;
    private Paint g = new Paint();
    private Paint h = new Paint();
    /* access modifiers changed from: private */
    public float i = 0.0f;
    /* access modifiers changed from: private */
    public float j = 0.0f;
    private boolean k = true;
    private List<View> l = new ArrayList();
    private List<bm> m = new ArrayList();
    private List<Bitmap> n = new ArrayList();
    private TXImageView o;
    private TextView p;
    private ArrayList<AppTagInfo> q = new ArrayList<>();
    private boolean r = false;
    private Handler s = new bl(this);

    public UserTagAnimationView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.b = context;
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, b.r);
        if (obtainStyledAttributes != null) {
            d = obtainStyledAttributes.getInt(0, 3);
            e = obtainStyledAttributes.getDimension(1, 70.0f);
            f = obtainStyledAttributes.getDimension(2, 50.0f);
            obtainStyledAttributes.recycle();
        }
        this.c = LayoutInflater.from(context);
        c();
        d();
        e();
    }

    private void c() {
        this.g.setStyle(Paint.Style.STROKE);
        this.g.setStrokeWidth(1.0f);
        this.g.setARGB(26, 255, 255, 255);
        this.g.setAntiAlias(true);
        this.h.setARGB(26, 255, 255, 255);
        this.h.setAntiAlias(true);
    }

    private void d() {
        this.c.inflate((int) R.layout.user_tags_circles_view, this);
        this.o = (TXImageView) findViewById(R.id.profile_icon);
        this.p = (TextView) findViewById(R.id.nick_name);
        if (j.a().j()) {
            m f2 = l.f();
            this.o.updateImageView(f2.f3165a, R.drawable.tag_user_prefile_defalut, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
            this.p.setText(f2.b);
            this.p.setVisibility(0);
            return;
        }
        this.o.updateImageView(Constants.STR_EMPTY, R.drawable.tag_user_prefile_defalut, TXImageView.TXImageViewType.LOCAL_IMAGE);
        this.p.setVisibility(8);
    }

    private void e() {
        this.l.add(findViewById(R.id.user_tag1));
        this.l.add(findViewById(R.id.user_tag2));
        this.l.add(findViewById(R.id.user_tag3));
        this.l.add(findViewById(R.id.user_tag4));
        this.l.add(findViewById(R.id.user_tag5));
        this.m.add(new bm(this, 0, 30.0f, 0.85f, 3.0f, 2));
        this.m.add(new bm(this, 0, 290.0f, 0.1f, 2.0f, 2));
        this.m.add(new bm(this, 1, 110.0f, 2.0f, 4.0f, 2));
        this.m.add(new bm(this, 2, 110.0f, 0.1f, 3.0f, 2));
        this.m.add(new bm(this, 2, 180.0f, 0.95f, 3.0f, 2));
        this.m.add(new bm(this, 3, 5.0f, 0.2f, 2.0f, 2));
    }

    public void a(ArrayList<AppTagInfo> arrayList) {
        if (arrayList != null) {
            this.q.clear();
            this.q.addAll(arrayList);
            int i2 = 0;
            while (true) {
                int i3 = i2;
                if (i3 < this.q.size()) {
                    if (i3 < this.l.size() && (this.l.get(i3) instanceof UserTagCircleUnitView)) {
                        ((UserTagCircleUnitView) this.l.get(i3)).a(this.q.get(i3).b);
                        ((UserTagCircleUnitView) this.l.get(i3)).a(this.q.get(i3));
                    }
                    i2 = i3 + 1;
                } else {
                    return;
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        if (this.k) {
            this.r = true;
            b();
            this.i = ((float) getLeft()) + (((float) getWidth()) / 2.0f);
            this.j = ((float) getTop()) + (((float) getHeight()) / 2.0f);
        }
        for (int i2 = 0; i2 < d; i2++) {
            canvas.drawCircle(this.i, this.j, e + (((float) i2) * f), this.g);
        }
        int i3 = 0;
        while (true) {
            int i4 = i3;
            if (i4 >= this.m.size()) {
                break;
            }
            bm bmVar = this.m.get(i4);
            canvas.drawCircle(bmVar.b(), bmVar.c(), bmVar.d(), this.h);
            bmVar.a();
            i3 = i4 + 1;
        }
        int i5 = 0;
        for (View next : this.l) {
            if ((next instanceof UserTagCircleUnitView) && i5 < this.n.size()) {
                float d2 = ((UserTagCircleUnitView) next).d();
                float b2 = this.i + ((UserTagCircleUnitView) next).b();
                float c2 = this.j - ((UserTagCircleUnitView) next).c();
                float f2 = ((UserTagCircleUnitView) next).f();
                Bitmap bitmap = this.n.get(i5);
                if (bitmap != null) {
                    if (f2 > 1.0f) {
                        Matrix matrix = new Matrix();
                        matrix.postScale(f2, f2);
                        Bitmap createBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
                        canvas.drawBitmap(createBitmap, b2 - d2, c2 - d2, (Paint) null);
                        createBitmap.recycle();
                    } else {
                        canvas.drawBitmap(bitmap, b2 - d2, c2 - d2, (Paint) null);
                    }
                    ((UserTagCircleUnitView) next).a();
                }
            }
            i5++;
        }
        this.s.removeMessages(TXTabBarLayout.TABITEM_TIPS_TEXT_ID);
        this.s.sendEmptyMessageDelayed(TXTabBarLayout.TABITEM_TIPS_TEXT_ID, 1);
        this.k = false;
    }

    public static float a(int i2) {
        return e + (((float) i2) * f);
    }

    public boolean a() {
        return this.r;
    }

    public void b() {
        for (View next : this.l) {
            next.buildDrawingCache();
            this.n.add(next.getDrawingCache());
            next.setVisibility(4);
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case 0:
                a(motionEvent.getX(), motionEvent.getY());
                return true;
            case 1:
                f();
                return true;
            case 2:
            default:
                return true;
        }
    }

    private void a(float f2, float f3) {
        for (View next : this.l) {
            if (next instanceof UserTagCircleUnitView) {
                float d2 = ((UserTagCircleUnitView) next).d();
                float b2 = this.i + ((UserTagCircleUnitView) next).b();
                float c2 = this.j - ((UserTagCircleUnitView) next).c();
                if (d2 > ((float) Math.sqrt((double) (((f3 - c2) * (f3 - c2)) + ((f2 - b2) * (f2 - b2)))))) {
                    ((UserTagCircleUnitView) next).a(true);
                } else {
                    ((UserTagCircleUnitView) next).a(false);
                }
            }
        }
    }

    private void f() {
        for (View next : this.l) {
            if ((next instanceof UserTagCircleUnitView) && ((UserTagCircleUnitView) next).e()) {
                ((UserTagCircleUnitView) next).a(false);
                AppTagInfo g2 = ((UserTagCircleUnitView) next).g();
                if (g2 != null) {
                    StringBuffer stringBuffer = new StringBuffer();
                    String str = g2.f1161a;
                    String str2 = g2.b;
                    stringBuffer.append("tmast://tagpage?tagID=").append(str).append("&").append("tagName=").append(str2).append("&").append("tagSubTitle=").append(g2.e);
                    Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(stringBuffer.toString()));
                    intent.setFlags(268435456);
                    if (this.b instanceof GuideActivity) {
                        intent.putExtra("from_activity", ((GuideActivity) this.b).t());
                    }
                    this.b.startActivity(intent);
                    ((Activity) this.b).finish();
                }
            }
        }
    }
}
