package com.tencent.assistantv2.component;

import android.content.Context;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.invalidater.IViewInvalidater;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.protocol.jce.ListRecommend;
import com.tencent.assistant.protocol.jce.ListRecommendIIT;
import com.tencent.assistant.protocol.jce.ListRecommendTop;
import com.tencent.assistant.protocol.jce.RecommendIT;
import com.tencent.assistant.utils.by;
import com.tencent.connect.common.Constants;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class ListRecommendReasonView extends LinearLayout {

    /* renamed from: a  reason: collision with root package name */
    List<ac> f1935a;
    private SimpleAppModel b;
    private IViewInvalidater c;

    public ListRecommendReasonView(Context context) {
        this(context, null);
    }

    public ListRecommendReasonView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f1935a = null;
        a();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, com.tencent.assistantv2.component.ListRecommendReasonView, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    private void a() {
        View inflate = LayoutInflater.from(getContext()).inflate((int) R.layout.list_recommend_view, (ViewGroup) this, true);
        this.f1935a = new ArrayList(3);
        this.f1935a.add(new ac((RelativeLayout) inflate.findViewById(R.id.node_1), (TextView) inflate.findViewById(R.id.node_order1), (TXImageView) inflate.findViewById(R.id.node_img1), (TextView) inflate.findViewById(R.id.node_desc1)));
        this.f1935a.add(new ac((RelativeLayout) inflate.findViewById(R.id.node_2), (TextView) inflate.findViewById(R.id.node_order2), (TXImageView) inflate.findViewById(R.id.node_img2), (TextView) inflate.findViewById(R.id.node_desc2)));
        this.f1935a.add(new ac((RelativeLayout) inflate.findViewById(R.id.node_3), (TextView) inflate.findViewById(R.id.node_order3), (TXImageView) inflate.findViewById(R.id.node_img3), (TextView) inflate.findViewById(R.id.node_desc3)));
    }

    public void a(SimpleAppModel simpleAppModel, IViewInvalidater iViewInvalidater) {
        ListRecommend listRecommend;
        boolean z;
        int i;
        this.c = iViewInvalidater;
        this.b = simpleAppModel;
        if (this.b.av != null) {
            listRecommend = this.b.av.b();
        } else {
            listRecommend = null;
        }
        if (listRecommend == null || listRecommend.a() == 0) {
            setVisibility(8);
        } else if (listRecommend.f1403a == 1) {
            ListRecommendIIT b2 = listRecommend.b();
            if (b2 == null || ((b2.a() == null || b2.a().size() == 0) && TextUtils.isEmpty(b2.b()))) {
                setVisibility(8);
                return;
            }
            setVisibility(0);
            ArrayList arrayList = new ArrayList(3);
            if (b2.f1404a != null) {
                int i2 = 0;
                while (true) {
                    int i3 = i2;
                    if (i3 >= b2.f1404a.size()) {
                        break;
                    }
                    arrayList.add(new ab(b2.f1404a.get(i3), 0, 4, 24, 24));
                    i2 = i3 + 1;
                }
            }
            a(listRecommend.b, arrayList);
            a(new ad(b2.b));
        } else if (listRecommend.f1403a == 2) {
            ArrayList<RecommendIT> c2 = listRecommend.c();
            if (c2 == null || c2.size() == 0) {
                setVisibility(8);
                return;
            }
            setVisibility(0);
            ArrayList arrayList2 = new ArrayList(c2.size());
            ArrayList arrayList3 = new ArrayList(c2.size());
            int i4 = 8;
            if (c2.size() > 1) {
                z = true;
            } else {
                z = false;
            }
            int i5 = 0;
            while (i5 < c2.size()) {
                int i6 = c2.get(i5).c;
                if (i5 >= c2.size() - 1) {
                    i = 0;
                } else {
                    i = i4;
                }
                ab abVar = new ab(i6 < 0 ? null : i6 + Constants.STR_EMPTY, c2.get(i5).f1456a, 0, 4, 24, 24);
                if (z) {
                    abVar.h = 85;
                }
                arrayList2.add(abVar);
                arrayList3.add(new ad(c2.get(i5).b, 0, i));
                i5++;
                i4 = i;
            }
            a(listRecommend.b, arrayList2);
            a(arrayList3);
        } else if (listRecommend.f1403a == 5) {
            ListRecommendIIT b3 = listRecommend.b();
            if (b3 == null || ((b3.a() == null || b3.a().size() == 0) && TextUtils.isEmpty(b3.b()))) {
                setVisibility(8);
                return;
            }
            setVisibility(0);
            ArrayList arrayList4 = new ArrayList(1);
            arrayList4.add(new ab(Constants.STR_EMPTY, 0, 4, (int) R.drawable.game_gift_icon_new, 18, 18));
            a(listRecommend.b, arrayList4);
            a(new ad(b3.b));
        } else if (listRecommend.f1403a == 3 || listRecommend.f1403a == 4) {
            ListRecommendTop d = listRecommend.d();
            if (d == null) {
                setVisibility(8);
                return;
            }
            setVisibility(0);
            ArrayList arrayList5 = new ArrayList(2);
            arrayList5.add(new ab(d.f1406a, 0, 4, (int) R.drawable.common_cup, 18, 18));
            if (listRecommend.f1403a == 3) {
                arrayList5.add(new ab(R.drawable.common_ranking_big, 4, 2));
            }
            a(listRecommend.b, arrayList5);
            ArrayList arrayList6 = new ArrayList(2);
            arrayList6.add(new ad(d.b, 0, 0));
            int i7 = 0;
            if (listRecommend.f1403a == 4) {
                i7 = 4;
            }
            arrayList6.add(new ad(d.d, i7, 0));
            a(arrayList6);
        }
    }

    private void a(int i, List<ab> list) {
        int i2;
        int i3;
        int a2;
        int i4;
        int i5 = 0;
        if (list != null && list.size() > 0) {
            int size = list.size();
            if (size > this.f1935a.size()) {
                i2 = this.f1935a.size();
            } else {
                i2 = size;
            }
            for (int i6 = 0; i6 < i2; i6++) {
                ac acVar = this.f1935a.get(i6);
                if (list.get(i6) == null) {
                    a(acVar);
                } else {
                    TXImageView tXImageView = acVar.c;
                    ab abVar = list.get(i6);
                    tXImageView.setInvalidater(this.c);
                    TXImageView.TXImageViewType tXImageViewType = TXImageView.TXImageViewType.NETWORK_IMAGE_ICON;
                    if (i == 1) {
                        tXImageViewType = TXImageView.TXImageViewType.ROUND_IMAGE;
                    }
                    tXImageView.updateImageView(abVar.b, abVar.c, tXImageViewType);
                    ViewGroup.LayoutParams layoutParams = tXImageView.getLayoutParams();
                    if (layoutParams instanceof RelativeLayout.LayoutParams) {
                        if (TextUtils.isEmpty(abVar.f1949a)) {
                            a2 = 0;
                        } else {
                            a2 = by.a(getContext(), 6.0f);
                        }
                        if (abVar.e > 0) {
                            i4 = by.a(getContext(), (float) abVar.e);
                        } else {
                            i4 = 0;
                        }
                        ((RelativeLayout.LayoutParams) layoutParams).setMargins(a2, 0, i4, 0);
                    }
                    ViewGroup.LayoutParams layoutParams2 = acVar.f1950a.getLayoutParams();
                    if (layoutParams2 instanceof LinearLayout.LayoutParams) {
                        if (abVar.d > 0) {
                            i3 = by.a(getContext(), (float) abVar.d);
                        } else {
                            i3 = 0;
                        }
                        ((LinearLayout.LayoutParams) layoutParams2).setMargins(i3, 0, 0, 0);
                        int i7 = abVar.h;
                        if (abVar.h > 0) {
                            i7 = by.a(getContext(), (float) abVar.h);
                        }
                        layoutParams2.width = i7;
                    }
                    if (abVar.f > 0) {
                        layoutParams.height = by.a(getContext(), (float) abVar.f);
                    } else {
                        layoutParams.height = abVar.f;
                    }
                    if (abVar.g > 0) {
                        layoutParams.width = by.a(getContext(), (float) abVar.g);
                    } else {
                        layoutParams.width = abVar.g;
                    }
                    tXImageView.setVisibility(0);
                    if (TextUtils.isEmpty(abVar.f1949a)) {
                        acVar.b.setVisibility(8);
                    } else {
                        acVar.b.setVisibility(0);
                        acVar.b.setText(abVar.f1949a);
                    }
                    acVar.f1950a.setVisibility(0);
                }
            }
            i5 = i2;
        }
        if (i5 < this.f1935a.size()) {
            while (i5 < this.f1935a.size()) {
                a(this.f1935a.get(i5));
                i5++;
            }
        }
    }

    private void a(ac acVar) {
        ViewGroup.LayoutParams layoutParams = acVar.f1950a.getLayoutParams();
        layoutParams.width = -2;
        if (layoutParams instanceof LinearLayout.LayoutParams) {
            ((LinearLayout.LayoutParams) layoutParams).setMargins(0, 0, 0, 0);
        }
        acVar.c.setVisibility(8);
        acVar.b.setVisibility(8);
    }

    private void a(ad adVar) {
        if (adVar == null || TextUtils.isEmpty(adVar.f1951a)) {
            a((List<ad>) null);
            return;
        }
        ArrayList arrayList = new ArrayList(3);
        arrayList.add(null);
        arrayList.add(null);
        arrayList.add(adVar);
        a(arrayList);
    }

    private void a(List<ad> list) {
        int i;
        if (list == null || list.size() <= 0) {
            i = 0;
        } else {
            int size = list.size();
            if (size > this.f1935a.size()) {
                i = this.f1935a.size();
            } else {
                i = size;
            }
            for (int i2 = 0; i2 < i; i2++) {
                ac acVar = this.f1935a.get(i2);
                ad adVar = list.get(i2);
                if (adVar == null || TextUtils.isEmpty(adVar.f1951a)) {
                    acVar.d.setText(Constants.STR_EMPTY);
                    acVar.d.setPadding(0, 0, 0, 0);
                } else {
                    acVar.d.setText(b(adVar));
                    acVar.d.setPadding(by.a(getContext(), (float) adVar.b), 0, by.a(getContext(), (float) adVar.c), 0);
                    acVar.d.setVisibility(0);
                }
            }
        }
        if (i < this.f1935a.size()) {
            while (i < this.f1935a.size()) {
                ac acVar2 = this.f1935a.get(i);
                acVar2.d.setText(Constants.STR_EMPTY);
                acVar2.d.setPadding(0, 0, 0, 0);
                i++;
            }
        }
    }

    private Spanned b(ad adVar) {
        if (adVar == null) {
            return null;
        }
        if (this.b == null || this.b.av == null) {
            return new SpannableString(adVar.f1951a);
        }
        return this.b.av.a(adVar.f1951a);
    }
}
