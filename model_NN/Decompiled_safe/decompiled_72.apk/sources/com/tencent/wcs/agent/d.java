package com.tencent.wcs.agent;

import java.util.ArrayList;

/* compiled from: ProGuard */
class d implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ a f4049a;

    d(a aVar) {
        this.f4049a = aVar;
    }

    public void run() {
        ArrayList arrayList = new ArrayList();
        synchronized (this.f4049a.f) {
            int size = this.f4049a.f.size();
            for (int i = 0; i < size; i++) {
                h hVar = (h) this.f4049a.f.valueAt(i);
                if (hVar != null) {
                    arrayList.add(hVar);
                }
            }
            this.f4049a.f.clear();
        }
        int size2 = arrayList.size();
        for (int i2 = 0; i2 < size2; i2++) {
            ((h) arrayList.get(i2)).b();
            ((h) arrayList.get(i2)).a((p) null);
        }
        arrayList.clear();
    }
}
