package com.tencent.connector.component;

import android.view.View;

/* compiled from: ProGuard */
class j implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ContentQQConnectionRequest f2406a;

    j(ContentQQConnectionRequest contentQQConnectionRequest) {
        this.f2406a = contentQQConnectionRequest;
    }

    public void onClick(View view) {
        this.f2406a.denyRequest();
        if (this.f2406a.b != null) {
            this.f2406a.b.finish();
        }
    }
}
