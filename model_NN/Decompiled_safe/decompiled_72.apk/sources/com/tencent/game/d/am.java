package com.tencent.game.d;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.manager.NetworkMonitor;
import com.tencent.assistant.model.QuickEntranceNotify;
import com.tencent.assistant.module.BaseEngine;
import com.tencent.assistant.module.ag;
import com.tencent.assistant.module.ak;
import com.tencent.assistant.net.APN;
import com.tencent.assistant.protocol.jce.CardsInfo;
import com.tencent.assistant.protocol.jce.GftGetHomeAppListRequest;
import com.tencent.assistant.protocol.jce.GftGetHomeAppListResponse;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.XLog;
import com.tencent.game.d.a.d;
import com.tencent.game.smartcard.b.b;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class am extends BaseEngine<d> implements ak, NetworkMonitor.ConnectivityChangeListener {

    /* renamed from: a  reason: collision with root package name */
    private final String f2689a = "GameRankAggregationEngine";
    /* access modifiers changed from: private */
    public List<b> b = new ArrayList();
    /* access modifiers changed from: private */
    public int c;
    private int d = -1;

    public am() {
        ag.b().a(this);
    }

    public List<b> a() {
        return this.b;
    }

    public void b() {
        XLog.d("GameRankAggregationEngine", "loadData:start");
        if (this.b.size() <= 0 || this.b.size() <= 0) {
            XLog.d("GameRankAggregationEngine", "loadData:load from server");
            TemporaryThreadManager.get().start(new ao(this));
            return;
        }
        XLog.d("GameRankAggregationEngine", "loadData:load from local");
        notifyDataChangedInMainThread(new an(this));
    }

    public int c() {
        if (this.d > 0) {
            cancel(this.d);
        }
        this.d = send(new GftGetHomeAppListRequest());
        return this.d;
    }

    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i, JceStruct jceStruct, JceStruct jceStruct2) {
        GftGetHomeAppListResponse gftGetHomeAppListResponse;
        ArrayList<CardsInfo> b2;
        if (jceStruct2 != null && (b2 = (gftGetHomeAppListResponse = (GftGetHomeAppListResponse) jceStruct2).b()) != null && b2.size() > 0) {
            this.b.clear();
            this.c = gftGetHomeAppListResponse.a();
            for (CardsInfo a2 : b2) {
                b bVar = new b();
                bVar.a((byte) 0, a2);
                this.b.add(bVar);
            }
            notifyDataChangedInMainThread(new ap(this, i));
        }
    }

    /* access modifiers changed from: protected */
    public void onRequestFailed(int i, int i2, JceStruct jceStruct, JceStruct jceStruct2) {
        notifyDataChangedInMainThread(new aq(this, i, i2));
    }

    public void onLocalDataHasUpdate() {
    }

    public void onPromptHasNewNotify(ArrayList<QuickEntranceNotify> arrayList) {
    }

    public void onConnected(APN apn) {
    }

    public void onDisconnected(APN apn) {
    }

    public void onConnectivityChanged(APN apn, APN apn2) {
    }
}
