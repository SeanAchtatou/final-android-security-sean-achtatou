package com.tencent.game.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import com.tencent.assistant.component.invalidater.IViewInvalidater;
import com.tencent.assistant.smartcard.component.as;
import com.tencent.game.smartcard.b.b;
import com.tencent.game.smartcard.view.NormalSmartCardRankAggregationSixItem;
import com.tencent.game.smartcard.view.NormalSmartCardRankAggregationThreeItem;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class GameRankAggregationListAdapter extends BaseAdapter {

    /* renamed from: a  reason: collision with root package name */
    private final String f2633a = "GameRankAggregationListAdapter";
    private List<b> b = new ArrayList();
    private int c;
    private Context d;
    private IViewInvalidater e;
    private as f = new b(this);

    public GameRankAggregationListAdapter(Context context, int i, List<b> list) {
        this.d = context;
        this.c = i;
        if (list != null) {
            this.b.addAll(list);
        }
    }

    public void a(List<b> list, int i) {
        this.b.clear();
        if (list != null) {
            this.b.addAll(list);
        }
        this.c = i;
        notifyDataSetChanged();
    }

    public int getCount() {
        return this.b.size();
    }

    /* renamed from: a */
    public b getItem(int i) {
        if (this.b.size() > i) {
            return this.b.get(i);
        }
        return null;
    }

    public long getItemId(int i) {
        return (long) i;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        b bVar = this.b.get(i);
        if (this.c == 2) {
            if (view == null || !(view instanceof NormalSmartCardRankAggregationSixItem)) {
                return new NormalSmartCardRankAggregationSixItem(this.d, bVar, this.f, this.e);
            }
            ((NormalSmartCardRankAggregationSixItem) view).a(bVar);
            return view;
        } else if (view == null || !(view instanceof NormalSmartCardRankAggregationThreeItem)) {
            return new NormalSmartCardRankAggregationThreeItem(this.d, bVar, this.f, this.e);
        } else {
            ((NormalSmartCardRankAggregationThreeItem) view).a(bVar);
            return view;
        }
    }
}
