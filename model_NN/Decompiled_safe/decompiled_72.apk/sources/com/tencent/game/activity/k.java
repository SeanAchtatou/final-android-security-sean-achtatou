package com.tencent.game.activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.l;
import android.support.v4.app.r;
import android.util.SparseArray;
import com.tencent.game.d.z;
import java.lang.ref.WeakReference;
import java.util.List;

/* compiled from: ProGuard */
public class k extends r {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ GameRankActivity f2624a;
    private Activity b;
    private List<z> c;
    private SparseArray<WeakReference<Fragment>> d = new SparseArray<>();

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public k(GameRankActivity gameRankActivity, l lVar, Activity activity, List<z> list) {
        super(lVar);
        this.f2624a = gameRankActivity;
        this.b = activity;
        this.c = list;
    }

    public Fragment a(int i) {
        Fragment fragment;
        WeakReference weakReference;
        z zVar = this.c.get(i);
        if (zVar == null || (weakReference = this.d.get(zVar.f)) == null || weakReference.get() == null) {
            fragment = null;
        } else {
            fragment = (Fragment) weakReference.get();
        }
        if (!(fragment != null || zVar == null || (fragment = a(zVar)) == null)) {
            this.d.put(zVar.f, new WeakReference(fragment));
        }
        return fragment;
    }

    public int getCount() {
        return this.c.size();
    }

    private Fragment a(z zVar) {
        Bundle bundle = new Bundle();
        bundle.putLong("subId", zVar.e);
        bundle.putLong("subAppListType", (long) zVar.f);
        bundle.putLong("subPageSize", (long) zVar.g);
        bundle.putLong("flag", (long) zVar.h);
        switch (zVar.f) {
            case 8:
                v vVar = new v(this.b);
                vVar.b(bundle);
                return vVar;
            case 9:
                s sVar = new s(this.b);
                sVar.b(bundle);
                return sVar;
            case 10:
                u uVar = new u(this.b);
                uVar.b(bundle);
                return uVar;
            case 11:
                t tVar = new t(this.b);
                tVar.b(bundle);
                return tVar;
            default:
                t tVar2 = new t(this.b);
                tVar2.b(bundle);
                return tVar2;
        }
    }
}
