package com.tencent.game.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.adapter.AppCategoryListAdapter;
import com.tencent.assistant.component.categorydetail.FloatTagHeader;
import com.tencent.assistant.component.invalidater.TXRefreshGetMoreListViewScrollListener;
import com.tencent.assistant.component.invalidater.ViewPageScrollListener;
import com.tencent.assistant.component.txscrollview.TXScrollViewBase;
import com.tencent.assistant.manager.t;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.adapter.smartlist.SmartListAdapter;
import com.tencent.assistantv2.component.SecondNavigationTitleViewV5;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.model.STCommonInfo;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.game.c.a;
import com.tencent.game.component.GameCategoryDetailListPage;

/* compiled from: ProGuard */
public class GameCategoryDetailActivity extends BaseActivity {
    /* access modifiers changed from: private */
    public long A = 0;
    private String B = null;
    /* access modifiers changed from: private */
    public SecondNavigationTitleViewV5 C;
    private View.OnClickListener D = new a(this);
    private View.OnClickListener E = new b(this);
    private View.OnClickListener F = new c(this);
    protected ViewPageScrollListener n = new ViewPageScrollListener();
    private a u;
    /* access modifiers changed from: private */
    public GameCategoryDetailListPage v;
    /* access modifiers changed from: private */
    public SmartListAdapter w;
    private int x = 0;
    /* access modifiers changed from: private */
    public long y = -2;
    private AppCategoryListAdapter.CategoryType z = AppCategoryListAdapter.CategoryType.CATEGORYTYPESOFTWARE;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        try {
            setContentView((int) R.layout.act_tab_cat_detail);
            v();
            t();
            q();
        } catch (Throwable th) {
            t.a().b();
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (this.v != null) {
            this.v.h();
        }
        if (this.C != null) {
            this.C.m();
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.v != null) {
            this.v.i();
        }
        if (this.C != null) {
            this.C.l();
        }
    }

    /* access modifiers changed from: protected */
    public void t() {
        w();
        u();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.y = -1;
        super.onDestroy();
        if (this.v != null) {
            this.v.g();
        }
    }

    private void v() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            this.x = extras.getInt(com.tencent.assistant.a.a.X);
            this.y = extras.getLong("com.tencent.assistant.CATATORY_ID");
            this.B = extras.getString("activityTitleName");
            this.A = extras.getLong("com.tencent.assistant.TAG_ID", 0);
            if (extras.getInt("com.tencent.assistant.CATATORY_TYPE", AppCategoryListAdapter.CategoryType.CATEGORYTYPESOFTWARE.ordinal()) == AppCategoryListAdapter.CategoryType.CATEGORYTYPESOFTWARE.ordinal()) {
                this.z = AppCategoryListAdapter.CategoryType.CATEGORYTYPESOFTWARE;
            } else {
                this.z = AppCategoryListAdapter.CategoryType.CATEGORYTYPEGAME;
            }
        }
    }

    private void w() {
        this.C = (SecondNavigationTitleViewV5) findViewById(R.id.title_view);
        this.C.a(this);
        this.C.d(false);
        this.C.a(this.B, 4);
        this.C.i();
        this.C.d(this.F);
        this.C.d(this.x);
    }

    public void u() {
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.main_ll);
        this.u = new a(this.y, 2);
        FloatTagHeader floatTagHeader = new FloatTagHeader(getApplicationContext());
        this.v = new GameCategoryDetailListPage(this, TXScrollViewBase.ScrollMode.PULL_FROM_END, this.u, floatTagHeader);
        this.w = new SmartListAdapter(this, this.v.a(), this.u.d()) {
            /* access modifiers changed from: protected */
            public SmartListAdapter.SmartListType o() {
                return SmartListAdapter.SmartListType.CategoryDetailPage;
            }

            /* access modifiers changed from: protected */
            public String b(int i) {
                return "06";
            }

            public boolean l() {
                return false;
            }
        };
        this.w.a(f(), this.y, this.A);
        TXRefreshGetMoreListViewScrollListener tXRefreshGetMoreListViewScrollListener = new TXRefreshGetMoreListViewScrollListener();
        this.v.a(tXRefreshGetMoreListViewScrollListener);
        this.w.a(tXRefreshGetMoreListViewScrollListener);
        linearLayout.addView(floatTagHeader);
        linearLayout.addView(this.v);
        this.v.b(this.D);
        this.v.a(this.E);
        this.v.a(this.w);
        this.v.a(this.A);
    }

    public int f() {
        if (this.z == AppCategoryListAdapter.CategoryType.CATEGORYTYPESOFTWARE) {
            return STConst.ST_PAGE_SOFTWARE_CATEGORY_DETAIL;
        }
        return 20060301;
    }

    public boolean g() {
        return false;
    }

    public void q() {
        STInfoV2 p = p();
        if (p != null) {
            p.updateContentId(STCommonInfo.ContentIdType.CATEGORY, this.y + "_" + this.A);
        }
        l.a(p);
    }
}
