package com.tencent.game.smartcard.component;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.utils.at;
import com.tencent.assistantv2.component.DownloadButton;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.pangu.component.appdetail.TXDwonloadProcessBar;

/* compiled from: ProGuard */
public class NormalSmartCardAppTinyNodeWithRank extends LinearLayout {

    /* renamed from: a  reason: collision with root package name */
    TXImageView f2720a;
    TextView b;
    TextView c;
    DownloadButton d;
    TXDwonloadProcessBar e;
    ImageView f;

    public NormalSmartCardAppTinyNodeWithRank(Context context) {
        this(context, null);
    }

    public NormalSmartCardAppTinyNodeWithRank(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a();
    }

    private void a() {
        LayoutInflater.from(getContext()).inflate((int) R.layout.smartcard_app_item_for_rank_aggregation_merge, this);
        this.f2720a = (TXImageView) findViewById(R.id.icon);
        this.b = (TextView) findViewById(R.id.name);
        this.c = (TextView) findViewById(R.id.size);
        this.e = (TXDwonloadProcessBar) findViewById(R.id.progress);
        this.d = (DownloadButton) findViewById(R.id.btn);
        this.f = (ImageView) findViewById(R.id.rank_icon);
        setOrientation(1);
        setGravity(1);
    }

    public void a(SimpleAppModel simpleAppModel, STInfoV2 sTInfoV2, int i) {
        int identifier;
        this.f2720a.updateImageView(simpleAppModel.e, R.drawable.pic_defaule, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
        this.b.setText(simpleAppModel.d);
        this.c.setText(at.a(simpleAppModel.k));
        this.e.a(simpleAppModel, new View[]{this.c});
        this.d.a(simpleAppModel);
        this.d.a(sTInfoV2);
        this.f2720a.setTag(simpleAppModel.q());
        if (simpleAppModel.al <= 6 && (identifier = getResources().getIdentifier("common_ranking_num_" + simpleAppModel.al, "drawable", "com.tencent.android.qqdownloader")) > 0) {
            this.f.setImageResource(identifier);
            this.f.setVisibility(0);
        }
        setOnClickListener(new c(this, i, simpleAppModel, sTInfoV2));
    }
}
