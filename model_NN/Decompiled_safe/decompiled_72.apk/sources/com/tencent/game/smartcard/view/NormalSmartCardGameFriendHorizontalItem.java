package com.tencent.game.smartcard.view;

import android.content.Context;
import android.text.TextUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.invalidater.IViewInvalidater;
import com.tencent.assistant.smartcard.component.NormalSmartcardBaseItem;
import com.tencent.assistant.smartcard.component.as;
import com.tencent.assistant.smartcard.d.n;
import com.tencent.assistant.smartcard.d.y;
import com.tencent.assistant.smartcard.d.z;
import com.tencent.assistant.utils.by;
import com.tencent.assistantv2.st.b.e;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import com.tencent.cloud.smartcard.b.d;
import com.tencent.game.smartcard.component.NormalSmartCardAppHorizontalNodeWithRank;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class NormalSmartCardGameFriendHorizontalItem extends NormalSmartcardBaseItem {
    private LinearLayout i;
    private ImageView l;
    private TextView m;
    private List<NormalSmartCardAppHorizontalNodeWithRank> n;

    public NormalSmartCardGameFriendHorizontalItem(Context context, n nVar, as asVar, IViewInvalidater iViewInvalidater) {
        super(context, nVar, asVar, iViewInvalidater);
        setBackgroundResource(R.drawable.common_card_normal);
    }

    /* access modifiers changed from: protected */
    public void a() {
        this.c = inflate(this.f1693a, R.layout.smartcard_game_friend_layout, this);
        this.l = (ImageView) this.c.findViewById(R.id.image_title);
        this.m = (TextView) this.c.findViewById(R.id.sub_mid_title);
        this.i = (LinearLayout) this.c.findViewById(R.id.card_app_list_layout);
        f();
    }

    private void f() {
        z zVar = (z) this.d;
        this.l.setImageResource(R.drawable.game_title_1);
        this.l.setVisibility(0);
        if (!TextUtils.isEmpty(zVar.m)) {
            this.m.setText(zVar.m);
            this.m.setVisibility(0);
        } else {
            this.m.setVisibility(8);
        }
        a(zVar);
    }

    private void a(z zVar) {
        int i2;
        List<y> list = zVar.f1769a;
        if (list != null) {
            this.i.setVisibility(0);
            if (list.size() >= 3) {
                int size = list.size();
                if (list.size() > 3) {
                    i2 = 3;
                } else {
                    i2 = size;
                }
                if (this.n == null) {
                    g();
                    this.n = new ArrayList(3);
                    for (int i3 = 0; i3 < i2; i3++) {
                        NormalSmartCardAppHorizontalNodeWithRank normalSmartCardAppHorizontalNodeWithRank = new NormalSmartCardAppHorizontalNodeWithRank(this.f1693a);
                        this.n.add(normalSmartCardAppHorizontalNodeWithRank);
                        normalSmartCardAppHorizontalNodeWithRank.setPadding(0, by.a(getContext(), 10.0f), 0, by.a(getContext(), 2.0f));
                        this.i.addView(normalSmartCardAppHorizontalNodeWithRank, new LinearLayout.LayoutParams(0, -2, 1.0f));
                        if (!TextUtils.isEmpty(list.get(i3).b) && list.get(i3).b.startsWith("等")) {
                            list.get(i3).b = list.get(i3).b.substring(1);
                        }
                        normalSmartCardAppHorizontalNodeWithRank.a(list.get(i3).f1768a, list.get(i3).a(), a(list.get(i3), i3), c(a(this.f1693a)), i3, false, 0);
                    }
                    return;
                }
                for (int i4 = 0; i4 < i2; i4++) {
                    if (!TextUtils.isEmpty(list.get(i4).b) && list.get(i4).b.startsWith("等")) {
                        list.get(i4).b = list.get(i4).b.substring(1);
                    }
                    this.n.get(i4).a(list.get(i4).f1768a, list.get(i4).a(), a(list.get(i4), i4), c(a(this.f1693a)), i4, false, 0);
                }
            }
        }
    }

    private void g() {
        this.n = null;
        this.i.removeAllViews();
    }

    private STInfoV2 a(y yVar, int i2) {
        STInfoV2 a2 = a(a.a("05", i2), 100);
        if (!(a2 == null || yVar == null)) {
            a2.updateWithSimpleAppModel(yVar.f1768a);
        }
        if (this.h == null) {
            this.h = new e();
        }
        this.h.a(a2);
        return a2;
    }

    /* access modifiers changed from: protected */
    public void b() {
        f();
    }

    /* access modifiers changed from: protected */
    public String d(int i2) {
        if (this.d instanceof d) {
            return ((d) this.d).e();
        }
        return super.d(i2);
    }
}
