package com.tencent.f.a.a;

import android.support.v4.view.MotionEventCompat;

/* compiled from: ProGuard */
public final class d implements Cloneable {

    /* renamed from: a  reason: collision with root package name */
    private int f2533a;

    public d(byte[] bArr) {
        this(bArr, 0);
    }

    public d(byte[] bArr, int i) {
        this.f2533a = (bArr[i + 1] << 8) & MotionEventCompat.ACTION_POINTER_INDEX_MASK;
        this.f2533a += bArr[i] & 255;
    }

    public d(int i) {
        this.f2533a = i;
    }

    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof d) || this.f2533a != ((d) obj).b()) {
            return false;
        }
        return true;
    }

    public byte[] a() {
        return new byte[]{(byte) (this.f2533a & 255), (byte) ((this.f2533a & MotionEventCompat.ACTION_POINTER_INDEX_MASK) >> 8)};
    }

    public int b() {
        return this.f2533a;
    }

    public int hashCode() {
        return this.f2533a;
    }
}
