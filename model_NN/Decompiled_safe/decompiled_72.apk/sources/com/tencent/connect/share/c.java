package com.tencent.connect.share;

import android.app.Activity;
import android.os.Bundle;
import com.tencent.open.utils.AsynLoadImgBack;
import com.tencent.tauth.IUiListener;
import java.util.ArrayList;

/* compiled from: ProGuard */
class c implements AsynLoadImgBack {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Bundle f2369a;
    final /* synthetic */ Activity b;
    final /* synthetic */ IUiListener c;
    final /* synthetic */ QzoneShare d;

    c(QzoneShare qzoneShare, Bundle bundle, Activity activity, IUiListener iUiListener) {
        this.d = qzoneShare;
        this.f2369a = bundle;
        this.b = activity;
        this.c = iUiListener;
    }

    public void saved(int i, String str) {
    }

    public void batchSaved(int i, ArrayList<String> arrayList) {
        if (i == 0) {
            this.f2369a.putStringArrayList("imageUrl", arrayList);
        }
        this.d.a(this.b, this.f2369a, this.c);
    }
}
