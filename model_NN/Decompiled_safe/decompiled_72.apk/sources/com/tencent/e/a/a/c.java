package com.tencent.e.a.a;

import com.tencent.connect.common.Constants;
import com.tencent.d.a.a;
import com.tencent.d.a.e;
import java.io.UnsupportedEncodingException;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: ProGuard */
public class c {

    /* renamed from: a  reason: collision with root package name */
    public int f2529a = -1;
    public String b = Constants.STR_EMPTY;
    public int c = 0;
    public String d = Constants.STR_EMPTY;
    public int e = 0;
    public String f = Constants.STR_EMPTY;
    public long g = 0;
    public long h = 0;
    public int i = 0;
    public byte[] j = null;

    public c(String str, int i2, String str2, int i3, String str3, long j2, long j3, int i4, byte[] bArr) {
        this.b = str;
        if (this.b == null) {
            this.b = Constants.STR_EMPTY;
        }
        this.c = i2;
        this.d = str2;
        if (this.d == null) {
            this.d = Constants.STR_EMPTY;
        }
        this.e = i3;
        this.f = str3;
        if (this.f == null) {
            this.f = Constants.STR_EMPTY;
        }
        this.g = j2;
        this.h = j3;
        this.i = i4;
        this.j = bArr;
    }

    public static c a(byte[] bArr) {
        JSONObject jSONObject;
        byte[] bArr2;
        String string;
        if (bArr == null || bArr.length <= 0) {
            return null;
        }
        byte[] a2 = new e().a(bArr, "&-*)Wb5_U,[^!9'+".getBytes());
        if (a2 != null) {
            try {
                String str = new String(a2, "UTF-8");
                if (!(str == null || (jSONObject = new JSONObject(str)) == null)) {
                    String string2 = jSONObject.getString("mHostPackageName");
                    int i2 = jSONObject.getInt("mHostVersion");
                    String string3 = jSONObject.getString("mHostUserIdentity");
                    int i3 = jSONObject.getInt("mDataItemType");
                    String string4 = jSONObject.getString("mDataItemAction");
                    long j2 = jSONObject.getLong("mDataItemStartTime");
                    long j3 = jSONObject.getLong("mDataItemEndTime");
                    int i4 = jSONObject.getInt("mDataItemVersion");
                    if (i4 < 1 || (string = jSONObject.getString("mIPCData")) == null) {
                        bArr2 = null;
                    } else {
                        bArr2 = a.a(string, 0);
                    }
                    return new c(string2, i2, string3, i3, string4, j2, j3, i4, bArr2);
                }
            } catch (UnsupportedEncodingException e2) {
                e2.printStackTrace();
            } catch (JSONException e3) {
                e3.printStackTrace();
            }
        }
        return null;
    }
}
