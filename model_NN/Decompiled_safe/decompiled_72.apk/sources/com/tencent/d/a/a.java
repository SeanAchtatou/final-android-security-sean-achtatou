package com.tencent.d.a;

import java.io.UnsupportedEncodingException;

/* compiled from: ProGuard */
public class a {

    /* renamed from: a  reason: collision with root package name */
    static final /* synthetic */ boolean f2439a;

    static {
        boolean z;
        if (!a.class.desiredAssertionStatus()) {
            z = true;
        } else {
            z = false;
        }
        f2439a = z;
    }

    public static byte[] a(String str, int i) {
        return a(str.getBytes(), i);
    }

    public static byte[] a(byte[] bArr, int i) {
        return a(bArr, 0, bArr.length, i);
    }

    public static byte[] a(byte[] bArr, int i, int i2, int i3) {
        c cVar = new c(i3, new byte[((i2 * 3) / 4)]);
        if (!cVar.a(bArr, i, i2, true)) {
            throw new IllegalArgumentException("bad base-64");
        } else if (cVar.b == cVar.f2440a.length) {
            return cVar.f2440a;
        } else {
            byte[] bArr2 = new byte[cVar.b];
            System.arraycopy(cVar.f2440a, 0, bArr2, 0, cVar.b);
            return bArr2;
        }
    }

    public static String b(byte[] bArr, int i) {
        try {
            return new String(c(bArr, i), "US-ASCII");
        } catch (UnsupportedEncodingException e) {
            throw new AssertionError(e);
        }
    }

    public static byte[] c(byte[] bArr, int i) {
        return b(bArr, 0, bArr.length, i);
    }

    public static byte[] b(byte[] bArr, int i, int i2, int i3) {
        int i4;
        d dVar = new d(i3, null);
        int i5 = (i2 / 3) * 4;
        if (!dVar.d) {
            switch (i2 % 3) {
                case 1:
                    i5 += 2;
                    break;
                case 2:
                    i5 += 3;
                    break;
            }
        } else if (i2 % 3 > 0) {
            i5 += 4;
        }
        if (dVar.e && i2 > 0) {
            int i6 = ((i2 - 1) / 57) + 1;
            if (dVar.f) {
                i4 = 2;
            } else {
                i4 = 1;
            }
            i5 += i4 * i6;
        }
        dVar.f2440a = new byte[i5];
        dVar.a(bArr, i, i2, true);
        if (f2439a || dVar.b == i5) {
            return dVar.f2440a;
        }
        throw new AssertionError();
    }

    private a() {
    }
}
