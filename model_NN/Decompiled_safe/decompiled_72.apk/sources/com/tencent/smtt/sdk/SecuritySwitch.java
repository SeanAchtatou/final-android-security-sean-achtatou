package com.tencent.smtt.sdk;

import android.content.Context;
import android.os.Build;
import android.util.Log;
import android.view.View;
import com.tencent.smtt.secure.SecureWebView;
import java.io.PrintWriter;
import java.io.StringWriter;

public class SecuritySwitch {
    private static final String LOGTAG = "SecuritySwitch";
    public static final int STATUS_DEFAULT = 0;
    public static final int STATUS_DISABLE = 2;
    public static final int STATUS_SKIP_LOAD_SO = 1;
    private static Context mContext = null;
    private static Object securityLock = new Object();
    private static boolean shouldApplySecurity = false;

    private static boolean shouldApplySecurity(final Context cx, View view) {
        view.post(new Runnable() {
            public void run() {
                try {
                    Log.e(SecuritySwitch.LOGTAG, "TesDownloader --> pull switch if necessary!");
                    TesDownloader.needDownload(cx);
                } catch (Throwable e) {
                    StringWriter errors = new StringWriter();
                    e.printStackTrace(new PrintWriter(errors));
                    QbSdkLog.e(SecuritySwitch.LOGTAG, "exceptions occurred2:" + errors.toString());
                }
            }
        });
        TesDownloadConfig config = TesDownloadConfig.getInstance(cx);
        if (config != null) {
            return config.mLastSecuritySwitch;
        }
        return false;
    }

    public static void setContext(Context cx) {
        if (cx != null) {
            mContext = cx.getApplicationContext();
        }
    }

    public static void setSecurityStatusIfNecessary(Context cx, View view) {
        if (Build.VERSION.SDK_INT < 14) {
            QbSdkLog.w(LOGTAG, "System is below Android 4.0. We can not apply webview security!");
            return;
        }
        try {
            mContext = cx.getApplicationContext();
            synchronized (securityLock) {
                shouldApplySecurity = shouldApplySecurity(mContext, view);
            }
            QbSdkLog.e(LOGTAG, "shouldApplySecurity:" + shouldApplySecurity);
            SecureWebView.setContext(cx);
            SecureWebView.setSafeUxssStatus(shouldApplySecurity);
        } catch (Throwable e) {
            StringWriter errors = new StringWriter();
            e.printStackTrace(new PrintWriter(errors));
            QbSdkLog.e(LOGTAG, "exceptions occurred1:" + errors.toString());
        }
    }

    public static void setSecureLibraryStatus(int status) {
        SecureWebView.setSecureLibraryStatus(status);
    }

    public static boolean isSecurityApplyed() {
        boolean z;
        synchronized (securityLock) {
            z = shouldApplySecurity;
        }
        return z;
    }

    public static int getSecuritySdkVersion() {
        try {
            return SecureWebView.getSDKVersionCode(mContext);
        } catch (Throwable e) {
            StringWriter errors = new StringWriter();
            e.printStackTrace(new PrintWriter(errors));
            QbSdkLog.e(LOGTAG, "exceptions occurred3:" + errors.toString());
            return 0;
        }
    }

    public static int getSecurityJsVersion() {
        try {
            return SecureWebView.getJSVersionCode(mContext);
        } catch (Throwable e) {
            StringWriter errors = new StringWriter();
            e.printStackTrace(new PrintWriter(errors));
            QbSdkLog.e(LOGTAG, "exceptions occurred4:" + errors.toString());
            return 0;
        }
    }
}
