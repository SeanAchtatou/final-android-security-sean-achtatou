package com.tencent.smtt.export.external;

import android.content.Context;
import android.os.Build;
import java.io.File;
import java.util.ArrayList;

public class LibraryLoader {
    private static String[] sLibrarySearchPaths = null;

    public static String[] getLibrarySearchPaths(Context context) {
        if (sLibrarySearchPaths != null) {
            return sLibrarySearchPaths;
        }
        if (context == null) {
            return new String[]{"/system/lib"};
        }
        ArrayList<String> paths = new ArrayList<>();
        paths.add(getNativeLibraryDir(context));
        paths.add("/system/lib");
        String[] pathArray = new String[paths.size()];
        paths.toArray(pathArray);
        sLibrarySearchPaths = pathArray;
        return sLibrarySearchPaths;
    }

    public static int getSdkVersion() {
        return Integer.parseInt(Build.VERSION.SDK);
    }

    public static String getNativeLibraryDir(Context context) {
        int sdkVer = getSdkVersion();
        if (sdkVer >= 9) {
            return X5Adapter_23.getNativeLibraryDirGB(context);
        }
        if (sdkVer >= 4) {
            return X5Adapter_16.getNativeLibraryDirDonut(context);
        }
        return "/data/data/" + context.getPackageName() + "/lib";
    }

    public static void loadLibrary(Context context, String moduleName) throws UnsatisfiedLinkError {
        String[] searchPath = getLibrarySearchPaths(context);
        String libraryName = System.mapLibraryName(moduleName);
        String[] arr$ = searchPath;
        int len$ = arr$.length;
        int i$ = 0;
        while (i$ < len$) {
            String path = arr$[i$] + "/" + libraryName;
            if (!new File(path).exists()) {
                i$++;
            } else {
                System.load(path);
                return;
            }
        }
        System.loadLibrary(moduleName);
    }
}
