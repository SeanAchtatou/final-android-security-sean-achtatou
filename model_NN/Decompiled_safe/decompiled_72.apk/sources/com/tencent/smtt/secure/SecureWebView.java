package com.tencent.smtt.secure;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import com.tencent.connect.common.Constants;
import com.tencent.smtt.sdk.QbSdkLog;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Locale;

public class SecureWebView {
    private static final String DEFAULT_JS_FILE_NAME = "safe_uxss.js";
    private static final String ERROR_REPORTED_SHARED_PREFERENCE = "SHARED_PREFERENCE_ERROR_REPORTED";
    private static final String FILE_MD5_SHARED_PREFERENCE = "SHARED_PREFERENCE_FILE_MD5";
    public static final String LOG_TAG = "NativeSafeWebViewClient";
    public static final int NOTIFICATION_TYPE_FUNCTION_FOUND = 2;
    public static final int NOTIFICATION_TYPE_SEARCH_FUNCTION_FAIL = 5;
    public static final int NOTIFICATION_TYPE_SEARCH_VIRTUAL_FUNCTION_FAIL = 4;
    public static final int NOTIFICATION_TYPE_UNEXPECTED_VT_INDEX = 3;
    public static final int NOTIFICATION_TYPE_UNINSTALL_DONE = 1;
    public static final int NOTIFICATION_TYPE_UPDATE_DONE = 10;
    public static final int NOTIFICATION_TYPE_VIRTUAL_FUNCTION = 0;
    private static final boolean PERFORMANCE_TEST_MODE = false;
    private static final String SHARED_PREFERENCE_NAME = "SHARED_PREFERENCE_SECURE_WEBVIEW";
    public static final int STATUS_DEFAULT = 0;
    public static final int STATUS_DISABLE = 2;
    public static final int STATUS_SKIP_LOAD_SO = 1;
    private static final int UINT32_MAX = -1;
    private static final String VT_SHARED_PREFERENCE_PREFIX = "SHARED_PREFERENCE_VT_INDEX_";
    private static final int VT_SHARED_PREFERNCE_COUNT = 9;
    private static Context mContext = null;
    private static boolean mExistX5WebViewExtension = false;
    private static boolean mLibraryInitialed = false;
    private static boolean mLibraryLoaded = false;
    private static boolean mLoadLibraryFail = false;
    private static ReportThread mReportThread = null;
    private static UpdateManager mUpdateManager = null;
    private static String mUserDefineJSFileName = Constants.STR_EMPTY;

    private static native boolean GetSafeStatus();

    private static native boolean SetJSFilePath(String str);

    private static native boolean SetSafeStatus(boolean z, boolean z2);

    private static native void SetVirtualFunction(int i, int i2, int i3);

    /* JADX WARNING: Removed duplicated region for block: B:24:0x005e A[SYNTHETIC, Splitter:B:24:0x005e] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.String getMd5ByFile(java.lang.String r15) {
        /*
            java.lang.String r13 = ""
            r10 = 0
            java.io.File r9 = new java.io.File
            r9.<init>(r15)
            boolean r0 = r9.exists()
            if (r0 != 0) goto L_0x0010
            r14 = r13
        L_0x000f:
            return r14
        L_0x0010:
            java.io.FileInputStream r11 = new java.io.FileInputStream     // Catch:{ Exception -> 0x004c }
            r11.<init>(r9)     // Catch:{ Exception -> 0x004c }
            java.nio.channels.FileChannel r0 = r11.getChannel()     // Catch:{ Exception -> 0x006a, all -> 0x0067 }
            java.nio.channels.FileChannel$MapMode r1 = java.nio.channels.FileChannel.MapMode.READ_ONLY     // Catch:{ Exception -> 0x006a, all -> 0x0067 }
            r2 = 0
            long r4 = r9.length()     // Catch:{ Exception -> 0x006a, all -> 0x0067 }
            java.nio.MappedByteBuffer r7 = r0.map(r1, r2, r4)     // Catch:{ Exception -> 0x006a, all -> 0x0067 }
            java.lang.String r0 = "MD5"
            java.security.MessageDigest r12 = java.security.MessageDigest.getInstance(r0)     // Catch:{ Exception -> 0x006a, all -> 0x0067 }
            r12.update(r7)     // Catch:{ Exception -> 0x006a, all -> 0x0067 }
            java.math.BigInteger r6 = new java.math.BigInteger     // Catch:{ Exception -> 0x006a, all -> 0x0067 }
            r0 = 1
            byte[] r1 = r12.digest()     // Catch:{ Exception -> 0x006a, all -> 0x0067 }
            r6.<init>(r0, r1)     // Catch:{ Exception -> 0x006a, all -> 0x0067 }
            r0 = 16
            java.lang.String r13 = r6.toString(r0)     // Catch:{ Exception -> 0x006a, all -> 0x0067 }
            if (r11 == 0) goto L_0x006d
            r11.close()     // Catch:{ IOException -> 0x0046 }
            r10 = r11
        L_0x0044:
            r14 = r13
            goto L_0x000f
        L_0x0046:
            r8 = move-exception
            r8.printStackTrace()
            r10 = r11
            goto L_0x0044
        L_0x004c:
            r8 = move-exception
        L_0x004d:
            r8.printStackTrace()     // Catch:{ all -> 0x005b }
            if (r10 == 0) goto L_0x0044
            r10.close()     // Catch:{ IOException -> 0x0056 }
            goto L_0x0044
        L_0x0056:
            r8 = move-exception
            r8.printStackTrace()
            goto L_0x0044
        L_0x005b:
            r0 = move-exception
        L_0x005c:
            if (r10 == 0) goto L_0x0061
            r10.close()     // Catch:{ IOException -> 0x0062 }
        L_0x0061:
            throw r0
        L_0x0062:
            r8 = move-exception
            r8.printStackTrace()
            goto L_0x0061
        L_0x0067:
            r0 = move-exception
            r10 = r11
            goto L_0x005c
        L_0x006a:
            r8 = move-exception
            r10 = r11
            goto L_0x004d
        L_0x006d:
            r10 = r11
            goto L_0x0044
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.smtt.secure.SecureWebView.getMd5ByFile(java.lang.String):java.lang.String");
    }

    private SecureWebView() {
    }

    public static void setContext(Context context) {
        if (mContext == null) {
            mContext = context.getApplicationContext();
        }
    }

    public static int getSDKVersionCode(Context context) {
        return UpdateManager.getSDKVersionCode();
    }

    public static int getJSVersionCode(Context context) {
        return UpdateManager.getJSVersion(context);
    }

    private static void copyAssetToFiles(Context context, String assetName) {
        if (!context.getFileStreamPath(assetName).exists()) {
            try {
                OutputStream out = context.openFileOutput(assetName, 0);
                out.write(SafeUxssJS.safeuxss, 0, SafeUxssJS.safeuxss.length);
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private static boolean loadSecureLibrary() {
        if (!System.getProperty("os.arch", Constants.STR_EMPTY).toLowerCase(Locale.US).startsWith("arm")) {
            return false;
        }
        try {
            System.loadLibrary("webviewext");
            return true;
        } catch (Exception | UnsatisfiedLinkError e) {
            return false;
        }
    }

    private static boolean initLibrary() {
        SharedPreferences pref = mContext.getSharedPreferences(SHARED_PREFERENCE_NAME, 0);
        if (pref.getBoolean(ERROR_REPORTED_SHARED_PREFERENCE, false)) {
            return false;
        }
        copyAssetToFiles(mContext, DEFAULT_JS_FILE_NAME);
        SetJSFilePath(mContext.getFilesDir() + "/" + DEFAULT_JS_FILE_NAME);
        String preMd5 = pref.getString(FILE_MD5_SHARED_PREFERENCE, Constants.STR_EMPTY);
        String curMd5 = getMd5ByFile("/system/lib/libchromium_net.so");
        if (!curMd5.equals(preMd5) || curMd5.equals(Constants.STR_EMPTY)) {
            SharedPreferences.Editor editor = pref.edit();
            for (int i = 0; i < 9; i++) {
                editor.putInt(VT_SHARED_PREFERENCE_PREFIX + i, -1);
            }
            editor.putString(FILE_MD5_SHARED_PREFERENCE, curMd5);
            editor.commit();
        } else {
            for (int i2 = 0; i2 < 9; i2++) {
                int value = pref.getInt(VT_SHARED_PREFERENCE_PREFIX + i2, -1);
                if (value != -1) {
                    SetVirtualFunction(i2, value, value);
                }
            }
        }
        return true;
    }

    public static void setSecureLibraryStatus(int status) {
        if (status == 1) {
            mLibraryLoaded = true;
        } else if (status == 2) {
            mLoadLibraryFail = true;
        }
    }

    public static synchronized void setSafeUxssStatus(boolean uxss) {
        synchronized (SecureWebView.class) {
            if (!mExistX5WebViewExtension && mContext != null && !mLoadLibraryFail && Build.VERSION.SDK_INT >= 14 && Build.VERSION.SDK_INT <= 18) {
                if (!mLibraryLoaded) {
                    if (!loadSecureLibrary()) {
                        mLoadLibraryFail = true;
                    } else {
                        mLibraryLoaded = true;
                    }
                }
                if (!mLibraryInitialed) {
                    if (!initLibrary()) {
                        mLoadLibraryFail = true;
                    } else {
                        mLibraryInitialed = true;
                    }
                }
                if (uxss) {
                    if (mReportThread == null) {
                        mReportThread = new ReportThread();
                        mReportThread.start();
                    }
                    if (mUpdateManager == null) {
                        mUpdateManager = new UpdateManager(mContext, DEFAULT_JS_FILE_NAME);
                    }
                } else {
                    if (mReportThread != null) {
                        mReportThread.stopThread();
                        mReportThread = null;
                    }
                    if (mUpdateManager != null) {
                        mUpdateManager.stopUpdate();
                        mUpdateManager = null;
                    }
                }
                SetSafeStatus(uxss, false);
            }
        }
    }

    public static void NotificationFromNative(int type, int param1, int param2) {
        int index = param1;
        int value = param2;
        if (mContext != null) {
            switch (type) {
                case 0:
                    SharedPreferences.Editor editor = mContext.getSharedPreferences(SHARED_PREFERENCE_NAME, 0).edit();
                    editor.putInt(VT_SHARED_PREFERENCE_PREFIX + index, value);
                    editor.commit();
                    break;
            }
            if (type == 3 || type == 4 || type == 5) {
                SharedPreferences pref = mContext.getSharedPreferences(SHARED_PREFERENCE_NAME, 0);
                if (!pref.getBoolean(ERROR_REPORTED_SHARED_PREFERENCE, false)) {
                    SharedPreferences.Editor editor2 = pref.edit();
                    editor2.putBoolean(ERROR_REPORTED_SHARED_PREFERENCE, true);
                    editor2.commit();
                    if (mReportThread != null) {
                        mReportThread.report(type, index, value);
                    }
                }
            }
        }
    }

    public static boolean getSafeUxssStatus() {
        if (!mLibraryLoaded) {
            return false;
        }
        return GetSafeStatus();
    }

    public static void LogFromNative(String tag, String message) {
        QbSdkLog.e(tag, message);
    }
}
