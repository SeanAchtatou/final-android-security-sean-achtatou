package com.tencent.feedback.common;

import android.content.Context;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Environment;
import android.os.StatFs;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import java.util.Locale;

/* compiled from: ProGuard */
public final class f {

    /* renamed from: a  reason: collision with root package name */
    private static f f2544a;

    public static synchronized f a(Context context) {
        f fVar;
        synchronized (f.class) {
            if (f2544a == null && context != null) {
                if (context != null) {
                    Context applicationContext = context.getApplicationContext();
                    if (applicationContext != null) {
                        context = applicationContext;
                    }
                }
                f2544a = new f(context);
            }
            fVar = f2544a;
        }
        return fVar;
    }

    private f(Context context) {
        if (context != null && context.getApplicationContext() == null) {
        }
    }

    public static String a() {
        try {
            return Build.MODEL;
        } catch (Throwable th) {
            g.d("rqdp{  getDeviceName error}", new Object[0]);
            if (!g.a(th)) {
                th.printStackTrace();
            }
            return "fail";
        }
    }

    public static String b() {
        try {
            return Build.VERSION.RELEASE;
        } catch (Throwable th) {
            g.d("rqdp{  getVersion error}", new Object[0]);
            if (!g.a(th)) {
                th.printStackTrace();
            }
            return "fail";
        }
    }

    public static String c() {
        try {
            return Build.VERSION.SDK;
        } catch (Throwable th) {
            g.d("rqdp{  getApiLevel error}", new Object[0]);
            if (!g.a(th)) {
                th.printStackTrace();
            }
            return "fail";
        }
    }

    public static String b(Context context) {
        String str;
        Throwable th;
        String str2 = "fail";
        if (context == null) {
            g.d("rqdp{  getImei but context == null!}", new Object[0]);
            return str2;
        }
        try {
            str2 = ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
            if (str2 == null) {
                str = "null";
            } else {
                str = str2.toLowerCase();
            }
            try {
                g.a("rqdp{  IMEI:}" + str, new Object[0]);
                return str;
            } catch (Throwable th2) {
                th = th2;
            }
        } catch (Throwable th3) {
            Throwable th4 = th3;
            str = str2;
            th = th4;
        }
        g.d("rqdp{  getImei error!}", new Object[0]);
        if (g.a(th)) {
            return str;
        }
        th.printStackTrace();
        return str;
    }

    public static String c(Context context) {
        if (context == null) {
            g.d("rqdp{  getImsi but context == null!}", new Object[0]);
            return "fail";
        }
        try {
            String subscriberId = ((TelephonyManager) context.getSystemService("phone")).getSubscriberId();
            if (subscriberId == null) {
                return "null";
            }
            return subscriberId.toLowerCase();
        } catch (Throwable th) {
            Throwable th2 = th;
            String str = "fail";
            Throwable th3 = th2;
            g.d("rqdp{  getImsi error!}", new Object[0]);
            if (g.a(th3)) {
                return str;
            }
            th3.printStackTrace();
            return str;
        }
    }

    public static String d(Context context) {
        String str = "fail";
        if (context == null) {
            g.d("rqdp{  getAndroidId but context == null!}", new Object[0]);
            return str;
        }
        try {
            String string = Settings.Secure.getString(context.getContentResolver(), "android_id");
            if (string == null) {
                return "null";
            }
            try {
                return string.toLowerCase();
            } catch (Throwable th) {
                Throwable th2 = th;
                str = string;
                th = th2;
            }
        } catch (Throwable th3) {
            th = th3;
        }
        g.d("rqdp{  getAndroidId error!}", new Object[0]);
        if (g.a(th)) {
            return str;
        }
        th.printStackTrace();
        return str;
    }

    public static String e(Context context) {
        if (context == null) {
            g.d("rqdp{  getMacAddress but context == null!}", new Object[0]);
            return "fail";
        }
        try {
            String macAddress = ((WifiManager) context.getSystemService("wifi")).getConnectionInfo().getMacAddress();
            if (macAddress == null) {
                return "null";
            }
            return macAddress.toLowerCase();
        } catch (Throwable th) {
            Throwable th2 = th;
            String str = "fail";
            Throwable th3 = th2;
            if (!g.a(th3)) {
                th3.printStackTrace();
            }
            g.d("rqdp{  getMacAddress error!}", new Object[0]);
            return str;
        }
    }

    public static String d() {
        String str = null;
        try {
            str = Build.CPU_ABI;
        } catch (Throwable th) {
            g.c("rqdp{  ge cuabi fa!}", new Object[0]);
            if (!g.a(th)) {
                th.printStackTrace();
            }
        }
        if (str == null || str.trim().length() == 0) {
            str = System.getProperty("os.arch");
        }
        return str == null ? "fail" : str;
    }

    public static long e() {
        try {
            StatFs statFs = new StatFs(Environment.getDataDirectory().getPath());
            return ((long) statFs.getBlockCount()) * ((long) statFs.getBlockSize());
        } catch (Throwable th) {
            g.d("rqdp{  getDisplayMetrics error!}", new Object[0]);
            if (g.a(th)) {
                return -1;
            }
            th.printStackTrace();
            return -1;
        }
    }

    public static long f() {
        try {
            StatFs statFs = new StatFs(Environment.getDataDirectory().getPath());
            return ((long) statFs.getAvailableBlocks()) * ((long) statFs.getBlockSize());
        } catch (Throwable th) {
            g.d("rqdp{  getDisplayMetrics error!}", new Object[0]);
            if (g.a(th)) {
                return -1;
            }
            th.printStackTrace();
            return -1;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:26:0x0061 A[Catch:{ all -> 0x00b0 }] */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0066 A[SYNTHETIC, Splitter:B:28:0x0066] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x006b A[SYNTHETIC, Splitter:B:31:0x006b] */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x008c A[SYNTHETIC, Splitter:B:45:0x008c] */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x0091 A[SYNTHETIC, Splitter:B:48:0x0091] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static long g() {
        /*
            r1 = 0
            java.lang.String r0 = "/proc/meminfo"
            java.io.FileReader r3 = new java.io.FileReader     // Catch:{ Throwable -> 0x0051, all -> 0x0087 }
            r3.<init>(r0)     // Catch:{ Throwable -> 0x0051, all -> 0x0087 }
            java.io.BufferedReader r2 = new java.io.BufferedReader     // Catch:{ Throwable -> 0x00b4, all -> 0x00ab }
            r0 = 2048(0x800, float:2.87E-42)
            r2.<init>(r3, r0)     // Catch:{ Throwable -> 0x00b4, all -> 0x00ab }
            java.lang.String r0 = r2.readLine()     // Catch:{ Throwable -> 0x00b7, all -> 0x00ae }
            java.lang.String r1 = ":\\s+"
            r4 = 2
            java.lang.String[] r0 = r0.split(r1, r4)     // Catch:{ Throwable -> 0x00b7, all -> 0x00ae }
            r1 = 1
            r0 = r0[r1]     // Catch:{ Throwable -> 0x00b7, all -> 0x00ae }
            java.lang.String r0 = r0.toLowerCase()     // Catch:{ Throwable -> 0x00b7, all -> 0x00ae }
            java.lang.String r1 = "kb"
            java.lang.String r4 = ""
            java.lang.String r0 = r0.replace(r1, r4)     // Catch:{ Throwable -> 0x00b7, all -> 0x00ae }
            java.lang.String r0 = r0.trim()     // Catch:{ Throwable -> 0x00b7, all -> 0x00ae }
            long r0 = java.lang.Long.parseLong(r0)     // Catch:{ Throwable -> 0x00b7, all -> 0x00ae }
            r4 = 1000(0x3e8, double:4.94E-321)
            long r0 = r0 * r4
            r2.close()     // Catch:{ IOException -> 0x003b }
        L_0x0037:
            r3.close()     // Catch:{ IOException -> 0x0046 }
        L_0x003a:
            return r0
        L_0x003b:
            r2 = move-exception
            boolean r4 = com.tencent.feedback.common.g.a(r2)
            if (r4 != 0) goto L_0x0037
            r2.printStackTrace()
            goto L_0x0037
        L_0x0046:
            r2 = move-exception
            boolean r3 = com.tencent.feedback.common.g.a(r2)
            if (r3 != 0) goto L_0x003a
            r2.printStackTrace()
            goto L_0x003a
        L_0x0051:
            r0 = move-exception
            r2 = r1
        L_0x0053:
            java.lang.String r3 = "rqdp{  getFreeMem error!}"
            r4 = 0
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ all -> 0x00b0 }
            com.tencent.feedback.common.g.d(r3, r4)     // Catch:{ all -> 0x00b0 }
            boolean r3 = com.tencent.feedback.common.g.a(r0)     // Catch:{ all -> 0x00b0 }
            if (r3 != 0) goto L_0x0064
            r0.printStackTrace()     // Catch:{ all -> 0x00b0 }
        L_0x0064:
            if (r1 == 0) goto L_0x0069
            r1.close()     // Catch:{ IOException -> 0x0071 }
        L_0x0069:
            if (r2 == 0) goto L_0x006e
            r2.close()     // Catch:{ IOException -> 0x007c }
        L_0x006e:
            r0 = -2
            goto L_0x003a
        L_0x0071:
            r0 = move-exception
            boolean r1 = com.tencent.feedback.common.g.a(r0)
            if (r1 != 0) goto L_0x0069
            r0.printStackTrace()
            goto L_0x0069
        L_0x007c:
            r0 = move-exception
            boolean r1 = com.tencent.feedback.common.g.a(r0)
            if (r1 != 0) goto L_0x006e
            r0.printStackTrace()
            goto L_0x006e
        L_0x0087:
            r0 = move-exception
            r2 = r1
            r3 = r1
        L_0x008a:
            if (r2 == 0) goto L_0x008f
            r2.close()     // Catch:{ IOException -> 0x0095 }
        L_0x008f:
            if (r3 == 0) goto L_0x0094
            r3.close()     // Catch:{ IOException -> 0x00a0 }
        L_0x0094:
            throw r0
        L_0x0095:
            r1 = move-exception
            boolean r2 = com.tencent.feedback.common.g.a(r1)
            if (r2 != 0) goto L_0x008f
            r1.printStackTrace()
            goto L_0x008f
        L_0x00a0:
            r1 = move-exception
            boolean r2 = com.tencent.feedback.common.g.a(r1)
            if (r2 != 0) goto L_0x0094
            r1.printStackTrace()
            goto L_0x0094
        L_0x00ab:
            r0 = move-exception
            r2 = r1
            goto L_0x008a
        L_0x00ae:
            r0 = move-exception
            goto L_0x008a
        L_0x00b0:
            r0 = move-exception
            r3 = r2
            r2 = r1
            goto L_0x008a
        L_0x00b4:
            r0 = move-exception
            r2 = r3
            goto L_0x0053
        L_0x00b7:
            r0 = move-exception
            r1 = r2
            r2 = r3
            goto L_0x0053
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.feedback.common.f.g():long");
    }

    /* JADX WARNING: Removed duplicated region for block: B:26:0x0064 A[Catch:{ all -> 0x00b3 }] */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0069 A[SYNTHETIC, Splitter:B:28:0x0069] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x006e A[SYNTHETIC, Splitter:B:31:0x006e] */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x008f A[SYNTHETIC, Splitter:B:45:0x008f] */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x0094 A[SYNTHETIC, Splitter:B:48:0x0094] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static long h() {
        /*
            r1 = 0
            java.lang.String r0 = "/proc/meminfo"
            java.io.FileReader r3 = new java.io.FileReader     // Catch:{ Throwable -> 0x0054, all -> 0x008a }
            r3.<init>(r0)     // Catch:{ Throwable -> 0x0054, all -> 0x008a }
            java.io.BufferedReader r2 = new java.io.BufferedReader     // Catch:{ Throwable -> 0x00b7, all -> 0x00ae }
            r0 = 2048(0x800, float:2.87E-42)
            r2.<init>(r3, r0)     // Catch:{ Throwable -> 0x00b7, all -> 0x00ae }
            r2.readLine()     // Catch:{ Throwable -> 0x00ba, all -> 0x00b1 }
            java.lang.String r0 = r2.readLine()     // Catch:{ Throwable -> 0x00ba, all -> 0x00b1 }
            java.lang.String r1 = ":\\s+"
            r4 = 2
            java.lang.String[] r0 = r0.split(r1, r4)     // Catch:{ Throwable -> 0x00ba, all -> 0x00b1 }
            r1 = 1
            r0 = r0[r1]     // Catch:{ Throwable -> 0x00ba, all -> 0x00b1 }
            java.lang.String r0 = r0.toLowerCase()     // Catch:{ Throwable -> 0x00ba, all -> 0x00b1 }
            java.lang.String r1 = "kb"
            java.lang.String r4 = ""
            java.lang.String r0 = r0.replace(r1, r4)     // Catch:{ Throwable -> 0x00ba, all -> 0x00b1 }
            java.lang.String r0 = r0.trim()     // Catch:{ Throwable -> 0x00ba, all -> 0x00b1 }
            long r0 = java.lang.Long.parseLong(r0)     // Catch:{ Throwable -> 0x00ba, all -> 0x00b1 }
            r4 = 1000(0x3e8, double:4.94E-321)
            long r0 = r0 * r4
            r2.close()     // Catch:{ IOException -> 0x003e }
        L_0x003a:
            r3.close()     // Catch:{ IOException -> 0x0049 }
        L_0x003d:
            return r0
        L_0x003e:
            r2 = move-exception
            boolean r4 = com.tencent.feedback.common.g.a(r2)
            if (r4 != 0) goto L_0x003a
            r2.printStackTrace()
            goto L_0x003a
        L_0x0049:
            r2 = move-exception
            boolean r3 = com.tencent.feedback.common.g.a(r2)
            if (r3 != 0) goto L_0x003d
            r2.printStackTrace()
            goto L_0x003d
        L_0x0054:
            r0 = move-exception
            r2 = r1
        L_0x0056:
            java.lang.String r3 = "rqdp{  getFreeMem error!}"
            r4 = 0
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ all -> 0x00b3 }
            com.tencent.feedback.common.g.d(r3, r4)     // Catch:{ all -> 0x00b3 }
            boolean r3 = com.tencent.feedback.common.g.a(r0)     // Catch:{ all -> 0x00b3 }
            if (r3 != 0) goto L_0x0067
            r0.printStackTrace()     // Catch:{ all -> 0x00b3 }
        L_0x0067:
            if (r1 == 0) goto L_0x006c
            r1.close()     // Catch:{ IOException -> 0x0074 }
        L_0x006c:
            if (r2 == 0) goto L_0x0071
            r2.close()     // Catch:{ IOException -> 0x007f }
        L_0x0071:
            r0 = -2
            goto L_0x003d
        L_0x0074:
            r0 = move-exception
            boolean r1 = com.tencent.feedback.common.g.a(r0)
            if (r1 != 0) goto L_0x006c
            r0.printStackTrace()
            goto L_0x006c
        L_0x007f:
            r0 = move-exception
            boolean r1 = com.tencent.feedback.common.g.a(r0)
            if (r1 != 0) goto L_0x0071
            r0.printStackTrace()
            goto L_0x0071
        L_0x008a:
            r0 = move-exception
            r2 = r1
            r3 = r1
        L_0x008d:
            if (r2 == 0) goto L_0x0092
            r2.close()     // Catch:{ IOException -> 0x0098 }
        L_0x0092:
            if (r3 == 0) goto L_0x0097
            r3.close()     // Catch:{ IOException -> 0x00a3 }
        L_0x0097:
            throw r0
        L_0x0098:
            r1 = move-exception
            boolean r2 = com.tencent.feedback.common.g.a(r1)
            if (r2 != 0) goto L_0x0092
            r1.printStackTrace()
            goto L_0x0092
        L_0x00a3:
            r1 = move-exception
            boolean r2 = com.tencent.feedback.common.g.a(r1)
            if (r2 != 0) goto L_0x0097
            r1.printStackTrace()
            goto L_0x0097
        L_0x00ae:
            r0 = move-exception
            r2 = r1
            goto L_0x008d
        L_0x00b1:
            r0 = move-exception
            goto L_0x008d
        L_0x00b3:
            r0 = move-exception
            r3 = r2
            r2 = r1
            goto L_0x008d
        L_0x00b7:
            r0 = move-exception
            r2 = r3
            goto L_0x0056
        L_0x00ba:
            r0 = move-exception
            r1 = r2
            r2 = r3
            goto L_0x0056
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.feedback.common.f.h():long");
    }

    public final long i() {
        if (!(Environment.getExternalStorageState().equals("mounted"))) {
            return 0;
        }
        try {
            StatFs statFs = new StatFs(Environment.getExternalStorageDirectory().getPath());
            return ((long) statFs.getBlockCount()) * ((long) statFs.getBlockSize());
        } catch (Throwable th) {
            g.d("rqdp{  get total sd error %s}", th.toString());
            if (!g.a(th)) {
                th.printStackTrace();
            }
            return -2;
        }
    }

    public final long j() {
        if (!(Environment.getExternalStorageState().equals("mounted"))) {
            return 0;
        }
        try {
            StatFs statFs = new StatFs(Environment.getExternalStorageDirectory().getPath());
            return ((long) statFs.getAvailableBlocks()) * ((long) statFs.getBlockSize());
        } catch (Throwable th) {
            g.d("rqdp{  get free sd error %s}", th.toString());
            if (!g.a(th)) {
                th.printStackTrace();
            }
            return -2;
        }
    }

    public static String k() {
        try {
            return Locale.getDefault().getCountry();
        } catch (Throwable th) {
            g.d("rqdp{  getCountry error!}", new Object[0]);
            if (g.a(th)) {
                return "fail";
            }
            th.printStackTrace();
            return "fail";
        }
    }

    public static String l() {
        try {
            return Build.BRAND;
        } catch (Throwable th) {
            g.d("rqdp{  getBrand error!}", new Object[0]);
            if (g.a(th)) {
                return "fail";
            }
            th.printStackTrace();
            return "fail";
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:56:0x0108 A[Catch:{ all -> 0x0156 }] */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x010d A[SYNTHETIC, Splitter:B:58:0x010d] */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x0112 A[SYNTHETIC, Splitter:B:61:0x0112] */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x0132 A[SYNTHETIC, Splitter:B:75:0x0132] */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x0137 A[SYNTHETIC, Splitter:B:78:0x0137] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static long m() {
        /*
            r1 = 0
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r2 = "/proc/"
            r0.<init>(r2)
            int r2 = android.os.Process.myPid()
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r2 = "/maps"
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r2 = r0.toString()
            java.lang.Thread r0 = java.lang.Thread.currentThread()     // Catch:{ Throwable -> 0x00f8, all -> 0x012e }
            java.lang.String r3 = "main"
            java.lang.String r0 = r0.getName()     // Catch:{ Throwable -> 0x00f8, all -> 0x012e }
            boolean r0 = r3.equals(r0)     // Catch:{ Throwable -> 0x00f8, all -> 0x012e }
            if (r0 == 0) goto L_0x00a6
            java.lang.String r0 = "[stack]"
        L_0x002c:
            java.lang.String r3 = "stack:%s"
            r4 = 1
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ Throwable -> 0x00f8, all -> 0x012e }
            r5 = 0
            r4[r5] = r0     // Catch:{ Throwable -> 0x00f8, all -> 0x012e }
            com.tencent.feedback.common.g.b(r3, r4)     // Catch:{ Throwable -> 0x00f8, all -> 0x012e }
            java.io.FileReader r3 = new java.io.FileReader     // Catch:{ Throwable -> 0x00f8, all -> 0x012e }
            r3.<init>(r2)     // Catch:{ Throwable -> 0x00f8, all -> 0x012e }
            java.io.BufferedReader r2 = new java.io.BufferedReader     // Catch:{ Throwable -> 0x0159, all -> 0x0151 }
            r4 = 2048(0x800, float:2.87E-42)
            r2.<init>(r3, r4)     // Catch:{ Throwable -> 0x0159, all -> 0x0151 }
        L_0x0043:
            java.lang.String r1 = r2.readLine()     // Catch:{ Throwable -> 0x015c, all -> 0x0153 }
            if (r1 == 0) goto L_0x00d9
            boolean r4 = r1.contains(r0)     // Catch:{ Throwable -> 0x015c, all -> 0x0153 }
            if (r4 == 0) goto L_0x0043
            java.lang.String r4 = "\\s+"
            java.lang.String[] r1 = r1.split(r4)     // Catch:{ Throwable -> 0x015c, all -> 0x0153 }
            int r4 = r1.length     // Catch:{ Throwable -> 0x015c, all -> 0x0153 }
            if (r4 <= 0) goto L_0x0043
            r4 = 0
            r4 = r1[r4]     // Catch:{ Throwable -> 0x015c, all -> 0x0153 }
            java.lang.String r5 = "-"
            int r4 = r4.indexOf(r5)     // Catch:{ Throwable -> 0x015c, all -> 0x0153 }
            if (r4 <= 0) goto L_0x0043
            r5 = 0
            r5 = r1[r5]     // Catch:{ Throwable -> 0x015c, all -> 0x0153 }
            int r5 = r5.length()     // Catch:{ Throwable -> 0x015c, all -> 0x0153 }
            if (r5 <= r4) goto L_0x0043
            r0 = 0
            r0 = r1[r0]     // Catch:{ Throwable -> 0x015c, all -> 0x0153 }
            r5 = 0
            java.lang.String r0 = r0.substring(r5, r4)     // Catch:{ Throwable -> 0x015c, all -> 0x0153 }
            r5 = 16
            long r5 = java.lang.Long.parseLong(r0, r5)     // Catch:{ Throwable -> 0x015c, all -> 0x0153 }
            r0 = 0
            r0 = r1[r0]     // Catch:{ Throwable -> 0x015c, all -> 0x0153 }
            int r1 = r4 + 1
            java.lang.String r0 = r0.substring(r1)     // Catch:{ Throwable -> 0x015c, all -> 0x0153 }
            r1 = 16
            long r0 = java.lang.Long.parseLong(r0, r1)     // Catch:{ Throwable -> 0x015c, all -> 0x0153 }
            long r0 = r0 - r5
            java.lang.String r4 = "st:%d"
            r5 = 1
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ Throwable -> 0x015c, all -> 0x0153 }
            r6 = 0
            java.lang.Long r7 = java.lang.Long.valueOf(r0)     // Catch:{ Throwable -> 0x015c, all -> 0x0153 }
            r5[r6] = r7     // Catch:{ Throwable -> 0x015c, all -> 0x0153 }
            com.tencent.feedback.common.g.b(r4, r5)     // Catch:{ Throwable -> 0x015c, all -> 0x0153 }
            r4 = 0
            int r4 = (r0 > r4 ? 1 : (r0 == r4 ? 0 : -1))
            if (r4 <= 0) goto L_0x00c1
        L_0x009f:
            r2.close()     // Catch:{ IOException -> 0x00c3 }
        L_0x00a2:
            r3.close()     // Catch:{ IOException -> 0x00ce }
        L_0x00a5:
            return r0
        L_0x00a6:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x00f8, all -> 0x012e }
            java.lang.String r3 = "[stack:"
            r0.<init>(r3)     // Catch:{ Throwable -> 0x00f8, all -> 0x012e }
            int r3 = android.os.Process.myTid()     // Catch:{ Throwable -> 0x00f8, all -> 0x012e }
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ Throwable -> 0x00f8, all -> 0x012e }
            java.lang.String r3 = "]"
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ Throwable -> 0x00f8, all -> 0x012e }
            java.lang.String r0 = r0.toString()     // Catch:{ Throwable -> 0x00f8, all -> 0x012e }
            goto L_0x002c
        L_0x00c1:
            long r0 = -r0
            goto L_0x009f
        L_0x00c3:
            r2 = move-exception
            boolean r4 = com.tencent.feedback.common.g.a(r2)
            if (r4 != 0) goto L_0x00a2
            r2.printStackTrace()
            goto L_0x00a2
        L_0x00ce:
            r2 = move-exception
            boolean r3 = com.tencent.feedback.common.g.a(r2)
            if (r3 != 0) goto L_0x00a5
            r2.printStackTrace()
            goto L_0x00a5
        L_0x00d9:
            r2.close()     // Catch:{ IOException -> 0x00e2 }
        L_0x00dc:
            r3.close()     // Catch:{ IOException -> 0x00ed }
        L_0x00df:
            r0 = -1
            goto L_0x00a5
        L_0x00e2:
            r0 = move-exception
            boolean r1 = com.tencent.feedback.common.g.a(r0)
            if (r1 != 0) goto L_0x00dc
            r0.printStackTrace()
            goto L_0x00dc
        L_0x00ed:
            r0 = move-exception
            boolean r1 = com.tencent.feedback.common.g.a(r0)
            if (r1 != 0) goto L_0x00df
            r0.printStackTrace()
            goto L_0x00df
        L_0x00f8:
            r0 = move-exception
            r2 = r1
        L_0x00fa:
            java.lang.String r3 = "rqdp{  getFreeMem error!}"
            r4 = 0
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ all -> 0x0156 }
            com.tencent.feedback.common.g.d(r3, r4)     // Catch:{ all -> 0x0156 }
            boolean r3 = com.tencent.feedback.common.g.a(r0)     // Catch:{ all -> 0x0156 }
            if (r3 != 0) goto L_0x010b
            r0.printStackTrace()     // Catch:{ all -> 0x0156 }
        L_0x010b:
            if (r1 == 0) goto L_0x0110
            r1.close()     // Catch:{ IOException -> 0x0118 }
        L_0x0110:
            if (r2 == 0) goto L_0x0115
            r2.close()     // Catch:{ IOException -> 0x0123 }
        L_0x0115:
            r0 = -2
            goto L_0x00a5
        L_0x0118:
            r0 = move-exception
            boolean r1 = com.tencent.feedback.common.g.a(r0)
            if (r1 != 0) goto L_0x0110
            r0.printStackTrace()
            goto L_0x0110
        L_0x0123:
            r0 = move-exception
            boolean r1 = com.tencent.feedback.common.g.a(r0)
            if (r1 != 0) goto L_0x0115
            r0.printStackTrace()
            goto L_0x0115
        L_0x012e:
            r0 = move-exception
            r3 = r1
        L_0x0130:
            if (r1 == 0) goto L_0x0135
            r1.close()     // Catch:{ IOException -> 0x013b }
        L_0x0135:
            if (r3 == 0) goto L_0x013a
            r3.close()     // Catch:{ IOException -> 0x0146 }
        L_0x013a:
            throw r0
        L_0x013b:
            r1 = move-exception
            boolean r2 = com.tencent.feedback.common.g.a(r1)
            if (r2 != 0) goto L_0x0135
            r1.printStackTrace()
            goto L_0x0135
        L_0x0146:
            r1 = move-exception
            boolean r2 = com.tencent.feedback.common.g.a(r1)
            if (r2 != 0) goto L_0x013a
            r1.printStackTrace()
            goto L_0x013a
        L_0x0151:
            r0 = move-exception
            goto L_0x0130
        L_0x0153:
            r0 = move-exception
            r1 = r2
            goto L_0x0130
        L_0x0156:
            r0 = move-exception
            r3 = r2
            goto L_0x0130
        L_0x0159:
            r0 = move-exception
            r2 = r3
            goto L_0x00fa
        L_0x015c:
            r0 = move-exception
            r1 = r2
            r2 = r3
            goto L_0x00fa
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.feedback.common.f.m():long");
    }

    /* JADX WARNING: Removed duplicated region for block: B:41:0x00b7 A[Catch:{ all -> 0x0106 }] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x00bc A[SYNTHETIC, Splitter:B:43:0x00bc] */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x00c1 A[SYNTHETIC, Splitter:B:46:0x00c1] */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x00e2 A[SYNTHETIC, Splitter:B:60:0x00e2] */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x00e7 A[SYNTHETIC, Splitter:B:63:0x00e7] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static long n() {
        /*
            r6 = 0
            r1 = 0
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r2 = "/proc/"
            r0.<init>(r2)
            int r2 = android.os.Process.myPid()
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r2 = "/maps"
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r0 = r0.toString()
            java.lang.String r8 = "[heap]"
            java.io.FileReader r5 = new java.io.FileReader     // Catch:{ Throwable -> 0x00a7, all -> 0x00dd }
            r5.<init>(r0)     // Catch:{ Throwable -> 0x00a7, all -> 0x00dd }
            java.io.BufferedReader r4 = new java.io.BufferedReader     // Catch:{ Throwable -> 0x010a, all -> 0x0101 }
            r0 = 2048(0x800, float:2.87E-42)
            r4.<init>(r5, r0)     // Catch:{ Throwable -> 0x010a, all -> 0x0101 }
            r2 = r6
        L_0x002b:
            java.lang.String r0 = r4.readLine()     // Catch:{ Throwable -> 0x010d, all -> 0x0104 }
            if (r0 == 0) goto L_0x008a
            boolean r1 = r0.contains(r8)     // Catch:{ Throwable -> 0x010d, all -> 0x0104 }
            if (r1 == 0) goto L_0x002b
            java.lang.String r1 = "\\s+"
            java.lang.String[] r0 = r0.split(r1)     // Catch:{ Throwable -> 0x010d, all -> 0x0104 }
            int r1 = r0.length     // Catch:{ Throwable -> 0x010d, all -> 0x0104 }
            if (r1 <= 0) goto L_0x0111
            r1 = 0
            r1 = r0[r1]     // Catch:{ Throwable -> 0x010d, all -> 0x0104 }
            java.lang.String r9 = "-"
            int r1 = r1.indexOf(r9)     // Catch:{ Throwable -> 0x010d, all -> 0x0104 }
            if (r1 <= 0) goto L_0x0111
            r9 = 0
            r9 = r0[r9]     // Catch:{ Throwable -> 0x010d, all -> 0x0104 }
            int r9 = r9.length()     // Catch:{ Throwable -> 0x010d, all -> 0x0104 }
            if (r9 <= r1) goto L_0x0111
            r9 = 0
            r9 = r0[r9]     // Catch:{ Throwable -> 0x010d, all -> 0x0104 }
            r10 = 0
            java.lang.String r9 = r9.substring(r10, r1)     // Catch:{ Throwable -> 0x010d, all -> 0x0104 }
            r10 = 16
            long r9 = java.lang.Long.parseLong(r9, r10)     // Catch:{ Throwable -> 0x010d, all -> 0x0104 }
            r11 = 0
            r0 = r0[r11]     // Catch:{ Throwable -> 0x010d, all -> 0x0104 }
            int r1 = r1 + 1
            java.lang.String r0 = r0.substring(r1)     // Catch:{ Throwable -> 0x010d, all -> 0x0104 }
            r1 = 16
            long r0 = java.lang.Long.parseLong(r0, r1)     // Catch:{ Throwable -> 0x010d, all -> 0x0104 }
            long r0 = r0 - r9
            java.lang.String r9 = "hp:%d"
            r10 = 1
            java.lang.Object[] r10 = new java.lang.Object[r10]     // Catch:{ Throwable -> 0x010d, all -> 0x0104 }
            r11 = 0
            java.lang.Long r12 = java.lang.Long.valueOf(r0)     // Catch:{ Throwable -> 0x010d, all -> 0x0104 }
            r10[r11] = r12     // Catch:{ Throwable -> 0x010d, all -> 0x0104 }
            com.tencent.feedback.common.g.b(r9, r10)     // Catch:{ Throwable -> 0x010d, all -> 0x0104 }
            int r9 = (r0 > r6 ? 1 : (r0 == r6 ? 0 : -1))
            if (r9 <= 0) goto L_0x0088
        L_0x0085:
            long r0 = r0 + r2
        L_0x0086:
            r2 = r0
            goto L_0x002b
        L_0x0088:
            long r0 = -r0
            goto L_0x0085
        L_0x008a:
            r4.close()     // Catch:{ IOException -> 0x0091 }
        L_0x008d:
            r5.close()     // Catch:{ IOException -> 0x009c }
        L_0x0090:
            return r2
        L_0x0091:
            r0 = move-exception
            boolean r1 = com.tencent.feedback.common.g.a(r0)
            if (r1 != 0) goto L_0x008d
            r0.printStackTrace()
            goto L_0x008d
        L_0x009c:
            r0 = move-exception
            boolean r1 = com.tencent.feedback.common.g.a(r0)
            if (r1 != 0) goto L_0x0090
            r0.printStackTrace()
            goto L_0x0090
        L_0x00a7:
            r0 = move-exception
            r2 = r1
        L_0x00a9:
            java.lang.String r3 = "rqdp{  getFreeMem error!}"
            r4 = 0
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ all -> 0x0106 }
            com.tencent.feedback.common.g.d(r3, r4)     // Catch:{ all -> 0x0106 }
            boolean r3 = com.tencent.feedback.common.g.a(r0)     // Catch:{ all -> 0x0106 }
            if (r3 != 0) goto L_0x00ba
            r0.printStackTrace()     // Catch:{ all -> 0x0106 }
        L_0x00ba:
            if (r1 == 0) goto L_0x00bf
            r1.close()     // Catch:{ IOException -> 0x00c7 }
        L_0x00bf:
            if (r2 == 0) goto L_0x00c4
            r2.close()     // Catch:{ IOException -> 0x00d2 }
        L_0x00c4:
            r2 = -2
            goto L_0x0090
        L_0x00c7:
            r0 = move-exception
            boolean r1 = com.tencent.feedback.common.g.a(r0)
            if (r1 != 0) goto L_0x00bf
            r0.printStackTrace()
            goto L_0x00bf
        L_0x00d2:
            r0 = move-exception
            boolean r1 = com.tencent.feedback.common.g.a(r0)
            if (r1 != 0) goto L_0x00c4
            r0.printStackTrace()
            goto L_0x00c4
        L_0x00dd:
            r0 = move-exception
            r4 = r1
            r5 = r1
        L_0x00e0:
            if (r4 == 0) goto L_0x00e5
            r4.close()     // Catch:{ IOException -> 0x00eb }
        L_0x00e5:
            if (r5 == 0) goto L_0x00ea
            r5.close()     // Catch:{ IOException -> 0x00f6 }
        L_0x00ea:
            throw r0
        L_0x00eb:
            r1 = move-exception
            boolean r2 = com.tencent.feedback.common.g.a(r1)
            if (r2 != 0) goto L_0x00e5
            r1.printStackTrace()
            goto L_0x00e5
        L_0x00f6:
            r1 = move-exception
            boolean r2 = com.tencent.feedback.common.g.a(r1)
            if (r2 != 0) goto L_0x00ea
            r1.printStackTrace()
            goto L_0x00ea
        L_0x0101:
            r0 = move-exception
            r4 = r1
            goto L_0x00e0
        L_0x0104:
            r0 = move-exception
            goto L_0x00e0
        L_0x0106:
            r0 = move-exception
            r4 = r1
            r5 = r2
            goto L_0x00e0
        L_0x010a:
            r0 = move-exception
            r2 = r5
            goto L_0x00a9
        L_0x010d:
            r0 = move-exception
            r1 = r4
            r2 = r5
            goto L_0x00a9
        L_0x0111:
            r0 = r2
            goto L_0x0086
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.feedback.common.f.n():long");
    }
}
