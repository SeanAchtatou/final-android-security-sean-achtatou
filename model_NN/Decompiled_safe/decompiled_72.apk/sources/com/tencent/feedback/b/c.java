package com.tencent.feedback.b;

import android.content.Context;
import com.tencent.connect.common.Constants;
import com.tencent.feedback.common.e;
import com.tencent.feedback.common.g;
import com.tencent.feedback.proguard.C;
import com.tencent.feedback.proguard.ac;

/* compiled from: ProGuard */
public final class c extends a {
    public c(Context context, int i, int i2) {
        super(context, i, i2);
    }

    public final void a(boolean z) {
    }

    public final C a() {
        try {
            C a2 = ac.a(b(), e.a(this.c), Constants.STR_EMPTY.getBytes(), -1, -1);
            if (a2 != null) {
                return a2;
            }
            return null;
        } catch (Throwable th) {
            if (!g.a(th)) {
                th.printStackTrace();
            }
            g.d("rqdp{  encode2RequestPackage failed}", new Object[0]);
            return null;
        }
    }
}
