package com.tencent.feedback.proguard;

import java.util.Locale;

/* compiled from: ProGuard */
public final class ak {

    /* renamed from: a  reason: collision with root package name */
    public static final String[][] f2588a = new String[5][];

    static {
        f2588a[0] = new String[]{"ao", String.format(Locale.US, "CREATE TABLE %s ( %s INTEGER PRIMARY KEY , %s int , %s int , %s int , %s int , %s blob , %s text , %s int ,%s int , %s int)", "ao", "_id", "_time", "_type", "_prority", "_length", "_datas", "_key", "_upCounts", "_count", "_state")};
        f2588a[1] = new String[]{"count", String.format(Locale.US, "CREATE TABLE %s ( %s INTEGER PRIMARY KEY , %s text unique  , %s int , %s int , %s int , %s int , %s int , %s text)", "count", "_id", "_countid", "_prority", "_local", "_stime", "_utime", "_ctime", "_cparams")};
        f2588a[2] = new String[]{"gray", String.format(Locale.US, "CREATE TABLE %s ( %s INTEGER PRIMARY KEY , %s int , %s text unique )", "gray", "_id", "_time", "_uid")};
        f2588a[3] = new String[]{"file", String.format(Locale.US, "CREATE TABLE %s ( %s INTEGER PRIMARY KEY , %s text , %s int , %s int , %s text , %s int , %s text )", "file", "_id", "_n", "_ut", "_sz", "_sa", "_t", "_ac")};
        f2588a[4] = new String[]{"strategy", String.format(Locale.US, "CREATE TABLE %s ( %s INTEGER PRIMARY KEY , %s int unique , %s int , %s blob)", "strategy", "_id", "_key", "_ut", "_datas")};
    }
}
