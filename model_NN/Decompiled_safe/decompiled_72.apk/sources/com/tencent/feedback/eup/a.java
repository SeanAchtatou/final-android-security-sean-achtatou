package com.tencent.feedback.eup;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;

/* compiled from: ProGuard */
public final class a<E> extends LinkedList<E> {

    /* renamed from: a  reason: collision with root package name */
    private int f2553a = -1;

    public a() {
    }

    public a(int i) {
        this.f2553a = i;
    }

    public final boolean add(E e) {
        if (size() == this.f2553a) {
            removeFirst();
        }
        return super.add(e);
    }

    public final void add(int i, E e) {
        if (size() == this.f2553a) {
            removeFirst();
        }
        super.add(i, e);
    }

    public final boolean addAll(Collection<? extends E> collection) {
        int size = (size() + collection.size()) - this.f2553a;
        if (size > 0) {
            removeRange(0, size);
        }
        return super.addAll(collection);
    }

    public final boolean addAll(int i, Collection<? extends E> collection) {
        throw new UnsupportedOperationException();
    }

    public final void addFirst(E e) {
        throw new UnsupportedOperationException();
    }

    public final void addLast(E e) {
        add(e);
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder();
        Iterator it = iterator();
        while (it.hasNext()) {
            sb.append(it.next().toString());
        }
        return sb.toString();
    }
}
