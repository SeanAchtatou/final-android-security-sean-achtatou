package com.tencent.open.web.security;

import android.view.KeyEvent;
import android.view.inputmethod.InputConnection;
import android.view.inputmethod.InputConnectionWrapper;
import com.tencent.open.a.n;

/* compiled from: ProGuard */
public class a extends InputConnectionWrapper {

    /* renamed from: a  reason: collision with root package name */
    public static String f3288a;
    public static boolean b = false;
    public static boolean c = false;

    public a(InputConnection inputConnection, boolean z) {
        super(inputConnection, z);
    }

    public boolean setComposingText(CharSequence charSequence, int i) {
        c = true;
        f3288a = charSequence.toString();
        n.b("CaptureInputConnection", "-->setComposingText: " + charSequence.toString());
        return super.setComposingText(charSequence, i);
    }

    public boolean commitText(CharSequence charSequence, int i) {
        c = true;
        f3288a = charSequence.toString();
        n.b("CaptureInputConnection", "-->commitText: " + charSequence.toString());
        return super.commitText(charSequence, i);
    }

    public boolean sendKeyEvent(KeyEvent keyEvent) {
        if (keyEvent.getAction() == 0) {
            n.c("CaptureInputConnection", "sendKeyEvent");
            f3288a = String.valueOf((char) keyEvent.getUnicodeChar());
            c = true;
            n.c("CaptureInputConnection", "s: " + f3288a);
        }
        n.b("CaptureInputConnection", "-->sendKeyEvent: " + f3288a);
        return super.sendKeyEvent(keyEvent);
    }
}
