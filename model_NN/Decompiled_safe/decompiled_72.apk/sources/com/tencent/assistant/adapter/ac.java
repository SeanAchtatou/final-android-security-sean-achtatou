package com.tencent.assistant.adapter;

import android.os.Handler;
import android.os.Message;
import android.widget.Toast;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.k;
import com.tencent.assistant.m;
import com.tencent.assistant.utils.XLog;
import com.tencent.nucleus.manager.component.SwitchButton;

/* compiled from: ProGuard */
class ac extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ y f582a;

    ac(y yVar) {
        this.f582a = yVar;
    }

    public void handleMessage(Message message) {
        int i;
        boolean z;
        switch (message.what) {
            case 1:
                SwitchButton switchButton = (SwitchButton) message.obj;
                if (message.arg1 == 1) {
                    i = R.string.toast_root_request_succ;
                    if (!m.a().k()) {
                        k.a(4, true);
                        this.f582a.notifyDataSetChanged();
                    }
                    XLog.i("SettingAdapter", "ROOT_PERMISSION_SUC");
                    z = true;
                } else {
                    switchButton.b(false);
                    XLog.i("SettingAdapter", "ROOT_PERMISSION_FAIL");
                    i = R.string.toast_root_request_fail;
                    z = false;
                }
                k.a(3, z);
                Toast.makeText(this.f582a.d, this.f582a.d.getResources().getString(i), 1).show();
                return;
            default:
                return;
        }
    }
}
