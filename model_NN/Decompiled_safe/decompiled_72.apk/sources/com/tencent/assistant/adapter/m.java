package com.tencent.assistant.adapter;

import android.content.Intent;
import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.pangu.activity.AppDetailActivityV5;

/* compiled from: ProGuard */
class m extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SimpleAppModel f597a;
    final /* synthetic */ STInfoV2 b;
    final /* synthetic */ OneMoreAdapter c;

    m(OneMoreAdapter oneMoreAdapter, SimpleAppModel simpleAppModel, STInfoV2 sTInfoV2) {
        this.c = oneMoreAdapter;
        this.f597a = simpleAppModel;
        this.b = sTInfoV2;
    }

    public void onTMAClick(View view) {
        Intent intent = new Intent(this.c.f574a, AppDetailActivityV5.class);
        intent.putExtra("simpleModeInfo", this.f597a);
        intent.putExtra("st_common_data", this.b);
        this.c.f574a.startActivity(intent);
    }

    public STInfoV2 getStInfo() {
        if (this.b != null) {
            this.b.actionId = 200;
            this.b.status = "01";
        }
        return this.b;
    }
}
