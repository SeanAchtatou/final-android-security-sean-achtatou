package com.tencent.assistant.adapter;

import android.view.View;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.adapter.AppCategoryListAdapter;
import com.tencent.assistant.utils.XLog;
import com.tencent.pangu.link.b;

/* compiled from: ProGuard */
class d implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppCategoryListAdapter f588a;

    d(AppCategoryListAdapter appCategoryListAdapter) {
        this.f588a = appCategoryListAdapter;
    }

    public void onClick(View view) {
        String str;
        f fVar = (f) view.getTag(R.id.category_data);
        if (fVar != null) {
            if (fVar.d == 0 || fVar.d == 1) {
                str = ((("tpmast://" + (this.f588a.f == AppCategoryListAdapter.CategoryType.CATEGORYTYPESOFTWARE ? "appcategorydetail" : "gamecategorydetail") + "?") + "&" + "category_detail_category_id" + "=" + fVar.f590a) + "&" + "category_detail_tag_id" + "=" + fVar.b) + "&" + "category_detail_category_name" + "=" + fVar.c;
                XLog.d("AppCategoryListAdapter", "tmast url:" + str);
            } else {
                str = fVar.e;
            }
            b.b(this.f588a.g, str);
        }
    }
}
