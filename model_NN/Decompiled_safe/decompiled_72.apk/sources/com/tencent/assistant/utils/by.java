package com.tencent.assistant.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.view.View;
import com.qq.AppService.AstApp;

/* compiled from: ProGuard */
public class by {

    /* renamed from: a  reason: collision with root package name */
    private static float f1842a = 0.0f;

    public static int a(Context context, float f) {
        return (int) ((context.getResources().getDisplayMetrics().density * f) + 0.5f);
    }

    public static int b(Context context, float f) {
        return (int) ((f / context.getResources().getDisplayMetrics().density) + 0.5f);
    }

    public static float a(float f) {
        if (f1842a == 0.0f) {
            f1842a = (float) AstApp.i().getResources().getDisplayMetrics().densityDpi;
        }
        return (f1842a * f) / 160.0f;
    }

    public static int b(float f) {
        return (int) a(f);
    }

    public static void a(View view, View view2, int[] iArr) {
        int i;
        int i2;
        if (iArr != null && iArr.length >= 2) {
            int i3 = 0;
            int i4 = 0;
            while (true) {
                if (view.getParent() == null) {
                    i = i4;
                    i2 = i3;
                    break;
                }
                i = view.getLeft() + i4;
                i2 = view.getTop() + i3;
                if (view.getParent() == view2) {
                    iArr[0] = i;
                    iArr[1] = i2;
                    if (iArr.length >= 4) {
                        iArr[2] = view.getMeasuredWidth();
                        iArr[3] = view.getMeasuredHeight();
                    }
                } else {
                    try {
                        View view3 = (View) view.getParent();
                        if (iArr.length >= 4) {
                            iArr[2] = view3.getMeasuredWidth();
                            iArr[3] = view3.getMeasuredHeight();
                        }
                        view = view3;
                        i3 = i2;
                        i4 = i;
                    } catch (ClassCastException e) {
                    }
                }
            }
            if (view2 == null) {
                iArr[0] = i;
                iArr[1] = i2;
            }
        }
    }

    public static int a() {
        try {
            Class<?> cls = Class.forName("com.android.internal.R$dimen");
            return AstApp.i().getResources().getDimensionPixelSize(Integer.parseInt(cls.getField("status_bar_height").get(cls.newInstance()).toString()));
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public static int b() {
        return AstApp.i().getResources().getDisplayMetrics().widthPixels;
    }

    public static int c() {
        return AstApp.i().getResources().getDisplayMetrics().heightPixels;
    }

    public static boolean d() {
        int i = AstApp.i().getResources().getDisplayMetrics().widthPixels;
        int i2 = AstApp.i().getResources().getDisplayMetrics().heightPixels;
        if (i == 640 && i2 == 960) {
            return true;
        }
        return false;
    }

    public static Bitmap a(View view) {
        if (view == null) {
            return null;
        }
        Bitmap createBitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(createBitmap);
        canvas.translate((float) (-view.getScrollX()), (float) (-view.getScrollY()));
        view.draw(canvas);
        return createBitmap;
    }
}
