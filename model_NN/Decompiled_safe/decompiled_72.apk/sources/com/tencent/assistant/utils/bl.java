package com.tencent.assistant.utils;

import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;

/* compiled from: ProGuard */
public class bl extends LayerDrawable {
    public bl(Drawable[] drawableArr, int i) {
        super(drawableArr);
        if (drawableArr.length == 2) {
            setLayerInset(0, 0, 0, drawableArr[1].getMinimumWidth() + i, 0);
            setLayerInset(1, drawableArr[0].getMinimumWidth() + i, 0, 0, 0);
            setBounds(0, 0, drawableArr[0].getMinimumWidth() + i + drawableArr[1].getMinimumWidth(), drawableArr[0].getMinimumHeight());
        } else if (drawableArr.length == 3) {
            setLayerInset(0, 0, 0, drawableArr[1].getMinimumWidth() + i + drawableArr[2].getMinimumWidth() + i, 0);
            setLayerInset(1, drawableArr[0].getMinimumWidth() + i, 0, drawableArr[2].getMinimumWidth() + i, 0);
            setLayerInset(2, drawableArr[0].getMinimumWidth() + i + drawableArr[1].getMinimumWidth() + i, 0, 0, 0);
            setBounds(0, 0, drawableArr[0].getMinimumWidth() + i + drawableArr[1].getMinimumWidth() + i + drawableArr[2].getMinimumWidth(), drawableArr[0].getMinimumHeight());
        }
    }
}
