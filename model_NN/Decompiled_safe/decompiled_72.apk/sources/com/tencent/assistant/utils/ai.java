package com.tencent.assistant.utils;

import android.os.Debug;

/* compiled from: ProGuard */
public class ai {
    public static boolean a() {
        Throwable th;
        boolean z;
        try {
            String str = FileUtil.getLogDir() + "/" + "heap_dump_.hprof";
            XLog.d("HeapDump", "dump begin............" + str);
            Debug.dumpHprofData(str);
            z = true;
            try {
                XLog.d("HeapDump", "dump sucess............" + str);
            } catch (Throwable th2) {
                th = th2;
                th.printStackTrace();
                XLog.d("HeapDump", "dump fail............" + th);
                return z;
            }
        } catch (Throwable th3) {
            Throwable th4 = th3;
            z = false;
            th = th4;
            th.printStackTrace();
            XLog.d("HeapDump", "dump fail............" + th);
            return z;
        }
        return z;
    }
}
