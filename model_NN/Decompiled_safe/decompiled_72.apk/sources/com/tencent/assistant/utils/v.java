package com.tencent.assistant.utils;

import android.text.TextUtils;
import com.tencent.assistant.Global;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
public class v {
    public static String a() {
        String h5ServerAddress = Global.getH5ServerAddress();
        if (TextUtils.isEmpty(h5ServerAddress)) {
            return "http://qzs.qq.com/open/mobile/reading/index.html";
        }
        String str = Constants.STR_EMPTY;
        if (!h5ServerAddress.startsWith("http://")) {
            str = str + "http://";
        }
        String str2 = str + h5ServerAddress;
        int lastIndexOf = str2.lastIndexOf("/");
        if (lastIndexOf >= 0 && lastIndexOf == str2.length() - 1) {
            str2 = str2.substring(0, str2.length() - 1);
        }
        return str2 + "/open/mobile/reading/index.html";
    }
}
