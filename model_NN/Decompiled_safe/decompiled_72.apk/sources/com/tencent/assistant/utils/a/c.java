package com.tencent.assistant.utils.a;

import android.text.TextUtils;
import java.io.File;

/* compiled from: ProGuard */
public class c {
    public static long a(String str) {
        if (TextUtils.isEmpty(str)) {
            return 0;
        }
        File file = new File(str);
        if (file.exists()) {
            return file.length();
        }
        return 0;
    }
}
