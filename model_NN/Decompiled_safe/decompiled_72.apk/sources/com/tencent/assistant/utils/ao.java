package com.tencent.assistant.utils;

import com.tencent.assistant.Global;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* compiled from: ProGuard */
public class ao {

    /* renamed from: a  reason: collision with root package name */
    static Map<String, StringBuilder> f1821a = new HashMap();
    static Map<String, ArrayList<String>> b = new HashMap();

    public static boolean a() {
        if (!Global.isOfficial()) {
        }
        return false;
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x0048 A[SYNTHETIC, Splitter:B:10:0x0048] */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0050 A[SYNTHETIC, Splitter:B:16:0x0050] */
    /* JADX WARNING: Removed duplicated region for block: B:28:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void a(java.lang.String r6, java.lang.String r7, boolean r8) {
        /*
            r1 = 0
            java.io.BufferedWriter r0 = new java.io.BufferedWriter     // Catch:{ Exception -> 0x004c, all -> 0x0045 }
            java.io.FileWriter r2 = new java.io.FileWriter     // Catch:{ Exception -> 0x004c, all -> 0x0045 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x004c, all -> 0x0045 }
            r3.<init>()     // Catch:{ Exception -> 0x004c, all -> 0x0045 }
            java.lang.String r4 = com.tencent.assistant.utils.FileUtil.getLogDir()     // Catch:{ Exception -> 0x004c, all -> 0x0045 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x004c, all -> 0x0045 }
            java.lang.String r4 = "/"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x004c, all -> 0x0045 }
            java.lang.StringBuilder r3 = r3.append(r7)     // Catch:{ Exception -> 0x004c, all -> 0x0045 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x004c, all -> 0x0045 }
            r2.<init>(r3, r8)     // Catch:{ Exception -> 0x004c, all -> 0x0045 }
            r0.<init>(r2)     // Catch:{ Exception -> 0x004c, all -> 0x0045 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0065, all -> 0x0060 }
            r1.<init>()     // Catch:{ Exception -> 0x0065, all -> 0x0060 }
            java.lang.StringBuilder r1 = r1.append(r6)     // Catch:{ Exception -> 0x0065, all -> 0x0060 }
            java.lang.String r2 = "\r\n"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x0065, all -> 0x0060 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0065, all -> 0x0060 }
            r0.write(r1)     // Catch:{ Exception -> 0x0065, all -> 0x0060 }
            r0.flush()     // Catch:{ Exception -> 0x0065, all -> 0x0060 }
            if (r0 == 0) goto L_0x0044
            r0.close()     // Catch:{ Exception -> 0x005e }
        L_0x0044:
            return
        L_0x0045:
            r0 = move-exception
        L_0x0046:
            if (r1 == 0) goto L_0x004b
            r1.close()     // Catch:{ Exception -> 0x0059 }
        L_0x004b:
            throw r0
        L_0x004c:
            r0 = move-exception
            r0 = r1
        L_0x004e:
            if (r0 == 0) goto L_0x0044
            r0.close()     // Catch:{ Exception -> 0x0054 }
            goto L_0x0044
        L_0x0054:
            r0 = move-exception
        L_0x0055:
            r0.printStackTrace()
            goto L_0x0044
        L_0x0059:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x004b
        L_0x005e:
            r0 = move-exception
            goto L_0x0055
        L_0x0060:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x0046
        L_0x0065:
            r1 = move-exception
            goto L_0x004e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.utils.ao.a(java.lang.String, java.lang.String, boolean):void");
    }

    public static synchronized void b(String str, String str2, boolean z) {
        synchronized (ao.class) {
            if (a()) {
                ArrayList arrayList = b.get(str2);
                if (arrayList == null || !z) {
                    arrayList = new ArrayList();
                    b.put(str2, arrayList);
                }
                arrayList.add(System.currentTimeMillis() + " " + str);
            }
        }
    }

    public static synchronized void c(String str, String str2, boolean z) {
        synchronized (ao.class) {
            if (a()) {
                ArrayList arrayList = b.get(str2);
                if (arrayList == null || !z) {
                    arrayList = new ArrayList();
                    b.put(str2, arrayList);
                }
                arrayList.add(str);
            }
        }
    }

    public static synchronized void b() {
        synchronized (ao.class) {
            if (a()) {
                for (String next : b.keySet()) {
                    Iterator it = b.get(next).iterator();
                    while (it.hasNext()) {
                        a((String) it.next(), next, true);
                    }
                }
                b.clear();
            }
        }
    }
}
