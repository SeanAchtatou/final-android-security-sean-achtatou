package com.tencent.assistant;

import java.util.Properties;

/* compiled from: ProGuard */
public class a {

    /* renamed from: a  reason: collision with root package name */
    private final Properties f381a = new Properties();

    /* JADX WARNING: Removed duplicated region for block: B:10:0x0029 A[SYNTHETIC, Splitter:B:10:0x0029] */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0031 A[SYNTHETIC, Splitter:B:16:0x0031] */
    /* JADX WARNING: Removed duplicated region for block: B:25:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private a() {
        /*
            r5 = this;
            r5.<init>()
            java.util.Properties r0 = new java.util.Properties
            r0.<init>()
            r5.f381a = r0
            r1 = 0
            java.io.File r2 = new java.io.File     // Catch:{ Exception -> 0x002d, all -> 0x0026 }
            java.io.File r0 = android.os.Environment.getRootDirectory()     // Catch:{ Exception -> 0x002d, all -> 0x0026 }
            java.lang.String r3 = "build.prop"
            r2.<init>(r0, r3)     // Catch:{ Exception -> 0x002d, all -> 0x0026 }
            java.io.FileInputStream r0 = new java.io.FileInputStream     // Catch:{ Exception -> 0x002d, all -> 0x0026 }
            r0.<init>(r2)     // Catch:{ Exception -> 0x002d, all -> 0x0026 }
            java.util.Properties r1 = r5.f381a     // Catch:{ Exception -> 0x003e, all -> 0x0039 }
            r1.load(r0)     // Catch:{ Exception -> 0x003e, all -> 0x0039 }
            if (r0 == 0) goto L_0x0025
            r0.close()     // Catch:{ Exception -> 0x0035 }
        L_0x0025:
            return
        L_0x0026:
            r0 = move-exception
        L_0x0027:
            if (r1 == 0) goto L_0x002c
            r1.close()     // Catch:{ Exception -> 0x0037 }
        L_0x002c:
            throw r0
        L_0x002d:
            r0 = move-exception
            r0 = r1
        L_0x002f:
            if (r0 == 0) goto L_0x0025
            r0.close()     // Catch:{ Exception -> 0x0035 }
            goto L_0x0025
        L_0x0035:
            r0 = move-exception
            goto L_0x0025
        L_0x0037:
            r1 = move-exception
            goto L_0x002c
        L_0x0039:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
            goto L_0x0027
        L_0x003e:
            r1 = move-exception
            goto L_0x002f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.a.<init>():void");
    }

    public String a(String str, String str2) {
        return this.f381a.getProperty(str, str2);
    }

    public static a a() {
        return new a();
    }
}
