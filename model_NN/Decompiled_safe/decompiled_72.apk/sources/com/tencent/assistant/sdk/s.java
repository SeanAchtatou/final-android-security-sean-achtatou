package com.tencent.assistant.sdk;

import android.content.Context;
import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.sdk.param.a;
import com.tencent.assistant.sdk.param.jce.BatchDownloadActionRequest;
import com.tencent.assistant.sdk.param.jce.IPCRequest;

/* compiled from: ProGuard */
public class s {
    public static r a(Context context, IPCRequest iPCRequest, String str) {
        if (iPCRequest == null) {
            return null;
        }
        JceStruct a2 = a.a(iPCRequest);
        if (a2 instanceof BatchDownloadActionRequest) {
            switch (((BatchDownloadActionRequest) a2).f1658a) {
                case 1:
                case 2:
                case 4:
                case 5:
                case 6:
                case 7:
                case 8:
                    return new f(context, iPCRequest, str);
                case 3:
                    return new g(context, iPCRequest);
            }
        }
        return null;
    }
}
