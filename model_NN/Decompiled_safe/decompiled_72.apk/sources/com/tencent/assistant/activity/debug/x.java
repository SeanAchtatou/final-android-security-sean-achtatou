package com.tencent.assistant.activity.debug;

import android.view.View;
import com.tencent.assistant.utils.TemporaryThreadManager;

/* compiled from: ProGuard */
class x implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DActivity f523a;

    x(DActivity dActivity) {
        this.f523a = dActivity;
    }

    public void onClick(View view) {
        TemporaryThreadManager.get().start(new y(this));
    }
}
