package com.tencent.assistant.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.SpannableString;
import android.view.KeyEvent;
import android.view.ViewStub;
import android.widget.ExpandableListView;
import android.widget.RelativeLayout;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.Global;
import com.tencent.assistant.a.a;
import com.tencent.assistant.component.FooterView;
import com.tencent.assistant.component.NormalErrorPage;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.ab;
import com.tencent.assistant.localres.callback.b;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.m;
import com.tencent.assistant.manager.t;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistant.plugin.PluginStartEntry;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.at;
import com.tencent.assistantv2.component.SecondNavigationTitleViewV5;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.nucleus.manager.apkMgr.ApkResultListView;
import com.tencent.nucleus.manager.component.ScaningProgressView;
import com.tencent.nucleus.manager.component.TxManagerCommContainView;
import com.tencent.nucleus.manager.component.f;
import com.tencent.nucleus.manager.component.k;
import com.tencent.nucleus.manager.main.AssistantTabActivity;
import com.tencent.pangu.utils.installuninstall.InstallUninstallTaskBean;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* compiled from: ProGuard */
public class ApkMgrActivity extends BaseActivity implements UIEventListener {
    /* access modifiers changed from: private */
    public ScaningProgressView A;
    /* access modifiers changed from: private */
    public ab B = null;
    private boolean C = false;
    /* access modifiers changed from: private */
    public boolean D = false;
    private ViewStub E = null;
    /* access modifiers changed from: private */
    public volatile boolean F = false;
    /* access modifiers changed from: private */
    public boolean G = false;
    /* access modifiers changed from: private */
    public boolean H = false;
    /* access modifiers changed from: private */
    public long I = 0;
    /* access modifiers changed from: private */
    public long J = 0;
    private PluginStartEntry K = null;
    /* access modifiers changed from: private */
    public boolean L = false;
    /* access modifiers changed from: private */
    public boolean M = false;
    /* access modifiers changed from: private */
    public boolean N = false;
    /* access modifiers changed from: private */
    public boolean O = false;
    /* access modifiers changed from: private */
    public boolean P = false;
    /* access modifiers changed from: private */
    public long Q = 0;
    /* access modifiers changed from: private */
    public f R;
    /* access modifiers changed from: private */
    public Handler S = new a(this);
    private boolean T = false;
    private k U = new n(this);
    private b V = new d(this);
    /* access modifiers changed from: private */
    public com.tencent.tmsecurelite.optimize.f W = new i(this);
    ExpandableListView.OnChildClickListener n = new h(this);
    /* access modifiers changed from: private */
    public Context u;
    private SecondNavigationTitleViewV5 v;
    private NormalErrorPage w;
    /* access modifiers changed from: private */
    public ApkResultListView x = null;
    /* access modifiers changed from: private */
    public FooterView y = null;
    /* access modifiers changed from: private */
    public TxManagerCommContainView z;

    public int f() {
        return STConst.ST_PAGE_APK_MANAGER;
    }

    /* access modifiers changed from: protected */
    public boolean g() {
        return false;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        try {
            setContentView((int) R.layout.activity_apk_manager_new);
            this.u = this;
            w();
            t();
            A();
            u();
        } catch (Throwable th) {
            this.T = true;
            t.a().b();
            finish();
        }
    }

    private void t() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            this.C = extras.getBoolean(a.O, false);
        }
    }

    private void u() {
        HashMap hashMap = new HashMap();
        hashMap.put("B1", Global.getPhoneGuidAndGen());
        hashMap.put("B2", Global.getQUAForBeacon());
        hashMap.put("B3", com.tencent.assistant.utils.t.g());
        XLog.d("beacon", "beacon report >> expose_apkmgr. " + hashMap.toString());
        com.tencent.beacon.event.a.a("expose_apkmgr", true, -1, -1, hashMap, true);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        if (this.T) {
            super.onResume();
            return;
        }
        if (this.v != null) {
            this.v.l();
        }
        super.onResume();
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_ROOT_SILENT_INSTALL_START, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_ROOT_SILENT_INSTALL_SUCC, this);
        AstApp.i().k().addUIEventListener(1027, this);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (!this.T) {
            if (this.v != null) {
                this.v.m();
            }
            AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_ROOT_SILENT_INSTALL_START, this);
            AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_ROOT_SILENT_INSTALL_SUCC, this);
            AstApp.i().k().removeUIEventListener(1027, this);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.b(java.lang.String, java.lang.Object):boolean
     arg types: [java.lang.String, boolean]
     candidates:
      com.tencent.assistant.m.b(byte, int):void
      com.tencent.assistant.m.b(byte, java.lang.String):void
      com.tencent.assistant.m.b(int, byte[]):void
      com.tencent.assistant.m.b(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.b(java.lang.Long, int):boolean
      com.tencent.assistant.m.b(java.lang.String, java.lang.Object):boolean */
    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (this.T) {
            super.onDestroy();
            return;
        }
        this.B.b(this.V);
        com.tencent.assistant.utils.b.a();
        if (this.C && this.D) {
            XLog.d("ApkMgrActivity", "set has run clean to true");
            m.a().b("key_space_clean_has_run_clean", (Object) true);
        }
        super.onDestroy();
    }

    public void onBackPressed() {
        super.onBackPressed();
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (4 != i) {
            return super.onKeyDown(i, keyEvent);
        }
        v();
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    /* access modifiers changed from: private */
    public void v() {
        if (!this.s) {
            finish();
            return;
        }
        Intent intent = new Intent(this, AssistantTabActivity.class);
        intent.setFlags(67108864);
        intent.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, f());
        intent.putExtra(a.G, true);
        startActivity(intent);
        this.s = false;
        finish();
        XLog.i("ApkMgrActivity", "ApkMgrActivity >> key back finish");
    }

    private void w() {
        this.v = (SecondNavigationTitleViewV5) findViewById(R.id.title_view);
        this.v.a(this);
        this.v.b(getString(R.string.apkmgr_title));
        this.v.d();
        this.v.c(new j(this));
        B();
        y();
        this.E = (ViewStub) findViewById(R.id.error_stub);
    }

    /* access modifiers changed from: private */
    public void x() {
        this.R.b(new k(this));
    }

    private void y() {
        this.y = new FooterView(this.u);
        this.z.a(this.y, new RelativeLayout.LayoutParams(-1, -2));
        this.y.updateContent(getString(R.string.apkmgr_delete));
        this.y.setFooterViewEnable(false);
        this.y.setTag(R.id.tma_st_slot_tag, "05_001");
        this.y.setOnFooterViewClickListener(new l(this));
    }

    /* access modifiers changed from: private */
    public void z() {
        TemporaryThreadManager.get().start(new m(this));
    }

    private void A() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            this.K = (PluginStartEntry) extras.getSerializable("dock_plugin");
        }
        this.B = ApkResourceManager.getInstance().getLocalApkLoader();
        this.B.a(this.V);
        this.B.c();
        this.B.a();
        this.I = System.currentTimeMillis();
    }

    private void B() {
        this.z = (TxManagerCommContainView) findViewById(R.id.contentView);
        this.A = new ScaningProgressView(this.u);
        this.x = new ApkResultListView(this.u);
        this.x.a(this.n);
        this.x.a(this.S);
        this.z.a(this.A);
        this.z.b(this.x);
        this.R = new f(this.x.b(), this.U, this.x.a());
    }

    /* access modifiers changed from: private */
    public void a(long j, boolean z2) {
        if (z2) {
            String c = at.c(j);
            String.format(getString(R.string.space_clean_apk_finish_size), c);
            b(c);
            return;
        }
        C();
    }

    private void C() {
        if (this.w == null) {
            this.E.inflate();
            this.w = (NormalErrorPage) findViewById(R.id.error);
        }
        this.w.setErrorType(1);
        this.w.setErrorHint(getResources().getString(R.string.no_apk));
        this.w.setErrorImage(R.drawable.emptypage_pic_01);
        this.w.setErrorHintTextColor(getResources().getColor(R.color.common_listiteminfo));
        this.w.setErrorHintTextSize(getResources().getDimension(R.dimen.appadmin_empty_page_text_size));
        this.w.setErrorTextVisibility(8);
        this.w.setErrorHintVisibility(0);
        this.w.setFreshButtonVisibility(8);
        this.z.setVisibility(8);
        this.y.setVisibility(8);
        this.w.setVisibility(0);
    }

    /* access modifiers changed from: private */
    public void a(int i, long j, boolean z2) {
        String c = at.c(j);
        String string = getString(R.string.apkmgr_delete);
        if (i > 0) {
            this.y.setFooterViewEnable(true);
            if (z2) {
                string = getString(R.string.apkmgr_delete_auto);
            }
            this.y.updateContent(string, " " + String.format(getString(R.string.apkmgr_delete_format), Integer.valueOf(i), c));
            return;
        }
        this.y.setFooterViewEnable(false);
        this.y.updateContent(string);
    }

    /* access modifiers changed from: private */
    public void a(long j, int i, boolean z2, int i2) {
        if (z2) {
            XLog.d("ApkMgrActivity", "ApkMgrActivity--updateHeaderView---scanning ing");
            this.A.a(j);
            return;
        }
        XLog.d("ApkMgrActivity", "ApkMgrActivity--updateHeaderView---scanning end");
        if (i2 == 1 && !this.G) {
            this.A.b(j);
            this.A.c();
            this.S.post(new o(this, i2));
        } else if (i2 == 2) {
            this.A.a(j);
            this.S.postDelayed(new r(this), 1000);
        } else {
            this.A.b(j);
            this.S.post(new s(this));
        }
    }

    private void b(String str) {
        XLog.i("ApkMgrActivity", "showClearAllPage");
        this.H = true;
        String string = getString(R.string.apkmgr_clear_success);
        int length = 5 + str.length();
        try {
            this.z.a(string, new SpannableString(String.format(getString(R.string.apkmgr_clear_size_tip), str)));
            STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this, 100);
            buildSTInfo.scene = STConst.ST_PAGE_APK_MANAGER_EMPTY;
            l.a(buildSTInfo);
            if (this.L) {
                this.z.a(R.drawable.icon_apk_manager_xiaobao, getString(R.string.apkmgr_skip_to_space_clean_tip), 30, 30, 3);
                this.z.a(this, SpaceCleanActivity.class, this.K);
                buildSTInfo.slotId = "03_001";
                l.a(buildSTInfo);
            } else if (this.M) {
                buildSTInfo.slotId = "04_001";
                l.a(buildSTInfo);
                this.z.a(R.drawable.icon_apk_manager_xiaobao, getString(R.string.apkmgr_skip_to_installapp_manager_tip), 11, 11, 3);
                this.z.a(this, InstalledAppManagerActivity.class, (PluginStartEntry) null);
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    public void D() {
        TemporaryThreadManager.get().start(new c(this));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.activity.ApkMgrActivity.a(long, boolean):void
     arg types: [int, int]
     candidates:
      com.tencent.assistant.activity.ApkMgrActivity.a(com.tencent.assistant.activity.ApkMgrActivity, long):long
      com.tencent.assistant.activity.ApkMgrActivity.a(long, int):void
      com.tencent.assistant.activity.ApkMgrActivity.a(java.util.List<com.tencent.assistant.localres.model.LocalApkInfo>, boolean):void
      com.tencent.assistant.activity.ApkMgrActivity.a(com.tencent.assistant.activity.ApkMgrActivity, boolean):boolean
      com.tencent.assistant.activity.BaseActivity.a(java.lang.String, java.lang.String):void
      com.tencent.assistant.activity.ApkMgrActivity.a(long, boolean):void */
    private void a(List<LocalApkInfo> list, boolean z2) {
        Map<Integer, ArrayList<LocalApkInfo>> d = this.B.d();
        if (d == null || d.isEmpty()) {
            a(0L, false);
            return;
        }
        this.x.a(d, true, z2);
        if (this.w != null) {
            this.w.setVisibility(8);
        }
    }

    /* access modifiers changed from: private */
    public void a(List<LocalApkInfo> list, boolean z2, boolean z3, int i, boolean z4) {
        if (list != null && !list.isEmpty()) {
            this.z.setVisibility(0);
            this.y.setVisibility(0);
            if (this.w != null) {
                this.w.setVisibility(4);
            }
            long j = 0;
            long j2 = 0;
            int i2 = 0;
            int i3 = 0;
            while (i3 < list.size()) {
                LocalApkInfo localApkInfo = list.get(i3);
                if (localApkInfo != null) {
                    if (localApkInfo.mIsSelect) {
                        i2++;
                        j2 += localApkInfo.occupySize;
                    }
                    j += localApkInfo.occupySize;
                }
                i3++;
                i2 = i2;
                j2 = j2;
            }
            XLog.d("ApkMgrActivity", "totalSize = " + j);
            a(j, list.size(), z2, i);
            a(i2, j2, z3);
            if (z4 && !z2) {
                a(list, i == 1 || i == 3);
            }
        }
    }

    public void handleUIEvent(Message message) {
        InstallUninstallTaskBean installUninstallTaskBean;
        if ((message.obj instanceof InstallUninstallTaskBean) && (installUninstallTaskBean = (InstallUninstallTaskBean) message.obj) != null) {
            Map<Integer, ArrayList<LocalApkInfo>> d = this.B.d();
            ArrayList arrayList = d.get(2);
            ArrayList arrayList2 = d.get(1);
            ArrayList arrayList3 = new ArrayList();
            if (arrayList != null) {
                arrayList3.addAll(arrayList);
            }
            if (arrayList2 != null) {
                arrayList3.addAll(arrayList2);
            }
            if (arrayList3.size() != 0) {
                Iterator it = arrayList3.iterator();
                while (it.hasNext()) {
                    LocalApkInfo localApkInfo = (LocalApkInfo) it.next();
                    if (localApkInfo.mPackageName.equals(installUninstallTaskBean.packageName)) {
                        switch (message.what) {
                            case EventDispatcherEnum.UI_EVENT_ROOT_SILENT_INSTALL_START /*1025*/:
                                localApkInfo.mApkState = 3;
                                continue;
                            case EventDispatcherEnum.UI_EVENT_ROOT_SILENT_INSTALL_SUCC /*1026*/:
                            case EventDispatcherEnum.UI_EVENT_ROOT_SILENT_UNINSTALL_FAIL /*1030*/:
                                localApkInfo.mApkState = 1;
                                continue;
                            case 1027:
                            case EventDispatcherEnum.UI_EVENT_ROOT_SILENT_UNINSTALL_START /*1028*/:
                            case EventDispatcherEnum.UI_EVENT_ROOT_SILENT_UNINSTALL_SUCC /*1029*/:
                            default:
                                localApkInfo.mApkState = 1;
                                continue;
                        }
                    }
                }
                this.x.a(d, true, true);
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(long j, int i) {
        HashMap hashMap = new HashMap();
        hashMap.put("B1", String.valueOf(i));
        hashMap.put("B2", Global.getPhoneGuidAndGen());
        hashMap.put("B3", Global.getQUAForBeacon());
        hashMap.put("B4", com.tencent.assistant.utils.t.g());
        com.tencent.beacon.event.a.a("ApkScan", true, j, -1, hashMap, true);
        XLog.d("beacon", "beacon report >> event: ApkScan, totalTime : " + j + ", params : " + hashMap.toString());
    }
}
