package com.tencent.assistant.activity;

import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.module.k;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;

/* compiled from: ProGuard */
class cb extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ bz f440a;

    cb(bz bzVar) {
        this.f440a = bzVar;
    }

    public void onTMAClick(View view) {
        this.f440a.f437a.v();
    }

    public STInfoV2 getStInfo() {
        return STInfoBuilder.buildSTInfo(this.f440a.f437a, this.f440a.f437a.A, "03_001", a.a(k.d(this.f440a.f437a.A)), a.a(k.d(this.f440a.f437a.A), this.f440a.f437a.A));
    }
}
