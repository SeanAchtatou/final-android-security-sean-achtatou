package com.tencent.assistant.activity;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.TypedArray;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.a.a;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.module.timer.job.SelfUpdateTimerJob;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistant.st.f;
import com.tencent.assistant.utils.FunctionUtils;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.ah;
import com.tencent.assistant.utils.ca;
import com.tencent.assistantv2.activity.MainActivity;
import com.tencent.assistantv2.st.b;
import com.tencent.assistantv2.st.page.STExternalInfo;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.STPageInfo;
import com.tencent.connect.common.Constants;
import com.tencent.nucleus.manager.about.HelperFeedbackActivity;
import com.tencent.nucleus.manager.setting.SettingActivity;
import com.tencent.pangu.activity.SelfUpdateActivity;
import com.tencent.pangu.manager.SelfUpdateManager;
import com.tencent.pangu.manager.ak;
import com.tencent.pangu.module.a.l;
import com.tencent.pangu.module.ax;
import com.tencent.pangu.module.timer.RecoverAppListReceiver;
import com.tencent.pangu.utils.d;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class BaseActivity extends FragmentActivity {
    private static boolean A = true;
    /* access modifiers changed from: private */
    public static boolean B = false;
    private static Handler C = ah.a("BaseActivityHandler");
    /* access modifiers changed from: protected */
    public static boolean o = false;
    protected static Handler t = new ar();
    /* access modifiers changed from: private */
    public l D = new as(this);
    private List<at> n = new ArrayList();
    protected STPageInfo p = new STPageInfo();
    public STExternalInfo q = new STExternalInfo();
    protected String r = Constants.STR_EMPTY;
    protected boolean s = false;
    private LinearLayout u = null;
    private LayoutInflater v = null;
    /* access modifiers changed from: private */
    public PopupWindow w = null;
    private au x = null;
    private Dialog y;
    private BroadcastReceiver z = new am(this);

    /* access modifiers changed from: protected */
    public void a(Intent intent) {
        if (!isFinishing()) {
            finish();
        }
    }

    public void startActivity(Intent intent) {
        try {
            b(intent);
            super.startActivity(intent);
        } catch (Throwable th) {
        }
    }

    public void startActivityForResult(Intent intent, int i) {
        try {
            b(intent);
            super.startActivityForResult(intent, i);
        } catch (Throwable th) {
        }
    }

    private void b(Intent intent) {
        if (intent != null) {
            Bundle a2 = d.a(intent);
            if (a2 == null || a2.get(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME) == null) {
                intent.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, this.p.f2060a);
                intent.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_SLOT_TAG_NAME, this.p.e);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        AstApp.i().a();
        overridePendingTransition(R.anim.activity_right_in, R.anim.activity_left_out);
        IntentFilter intentFilter = new IntentFilter("com.tencent.android.qqdownloader.action.EXIT_APP");
        intentFilter.addCategory("android.intent.category.DEFAULT");
        try {
            registerReceiver(this.z, intentFilter);
        } catch (Exception e) {
        }
        this.v = (LayoutInflater) getSystemService("layout_inflater");
        t();
        int a2 = d.a(getIntent(), PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, 2000);
        if (a2 == -1000) {
            f.a(a2);
        } else {
            f.a(f());
        }
        o();
        if (g()) {
            q();
        }
        ca.a();
        AstApp.a(this);
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
    }

    private void t() {
        Bundle a2 = d.a(getIntent());
        if (a2 != null) {
            this.s = a2.getBoolean(a.G);
            this.r = a2.getString(a.n);
            if (!B) {
                B = a2.getBoolean(a.t);
            }
            a(a2);
        }
    }

    private void a(Bundle bundle) {
        if (bundle != null) {
            this.q.f2059a = bundle.getString(a.j);
            this.q.b = bundle.getString(a.y);
            this.q.c = bundle.getString(a.n);
            this.q.d = bundle.getString(a.o);
            this.q.e = bundle.getString(a.ac);
            this.q.f = bundle.getString(a.ad);
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        t.sendEmptyMessageDelayed(10086, 100);
        if (this.y != null) {
            this.y.dismiss();
        }
        if (!(this instanceof SelfUpdateActivity)) {
            ax.a().unregister(this.D);
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        int i = 1000;
        super.onResume();
        t.removeMessages(10086);
        j();
        AstApp.a(this);
        if (!(this instanceof SelfUpdateActivity)) {
            TemporaryThreadManager.get().start(new an(this));
        }
        if (A) {
            A = false;
            if (!(this instanceof MainActivity) && h()) {
                o = true;
                XLog.d("voken", this + "mFirstOnResume : 检查更新");
                SelfUpdateTimerJob.e().d();
            }
            int i2 = this instanceof MainActivity ? 1000 : 0;
            if (!(this instanceof MainActivity) && C != null) {
                C.postDelayed(new ao(this), (long) i2);
            }
        }
        if (!(this instanceof MainActivity)) {
            i = 0;
        }
        if (i() && MainActivity.z == RecoverAppListReceiver.RecoverAppListState.NOMAL.ordinal() && C != null) {
            C.postDelayed(new ap(this), (long) i);
        }
    }

    /* access modifiers changed from: protected */
    public boolean h() {
        return !B;
    }

    /* access modifiers changed from: protected */
    public boolean i() {
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.AppService.AstApp.a(boolean, int):void
     arg types: [int, int]
     candidates:
      com.qq.AppService.AstApp.a(android.content.pm.PackageManager, java.lang.String):void
      com.qq.AppService.AstApp.a(boolean, int):void */
    /* access modifiers changed from: protected */
    public void j() {
        AstApp.i().a(true, 0);
        AstApp.i().j().sendEmptyMessage(EventDispatcherEnum.UI_EVENT_APP_ATFRONT);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        try {
            unregisterReceiver(this.z);
        } catch (Exception e) {
        }
        if (AstApp.m() == this) {
            AstApp.a((Activity) null);
        }
        super.onDestroy();
    }

    public void onBackPressed() {
        try {
            super.onBackPressed();
        } catch (Exception e) {
        }
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i == 4) {
            if (l() == 0) {
                return false;
            }
            if (!TextUtils.isEmpty(this.r) || this.s) {
                finish();
                return true;
            }
        }
        return super.onKeyDown(i, keyEvent);
    }

    /* JADX WARNING: Removed duplicated region for block: B:30:0x009a  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00a1  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void finish() {
        /*
            r7 = this;
            r2 = 0
            r6 = 10
            java.lang.String r0 = r7.r
            boolean r0 = android.text.TextUtils.isEmpty(r0)
            if (r0 != 0) goto L_0x00a9
            com.qq.AppService.AstApp r0 = com.qq.AppService.AstApp.i()
            java.lang.String r1 = "android.permission.GET_TASKS"
            int r0 = r0.checkCallingOrSelfPermission(r1)
            if (r0 != 0) goto L_0x00a9
            java.lang.String r0 = "activity"
            java.lang.Object r0 = r7.getSystemService(r0)
            android.app.ActivityManager r0 = (android.app.ActivityManager) r0
            r1 = 1
            java.util.List r1 = r0.getRecentTasks(r6, r1)
            if (r1 == 0) goto L_0x00a9
            java.util.Iterator r3 = r1.iterator()
        L_0x002a:
            boolean r1 = r3.hasNext()
            if (r1 == 0) goto L_0x00a9
            java.lang.Object r1 = r3.next()
            android.app.ActivityManager$RecentTaskInfo r1 = (android.app.ActivityManager.RecentTaskInfo) r1
            android.content.Intent r1 = r1.baseIntent
            if (r1 == 0) goto L_0x002a
            android.content.ComponentName r4 = r1.getComponent()
            java.lang.String r4 = r4.getPackageName()
            java.lang.String r5 = r7.r
            boolean r4 = r4.equals(r5)
            if (r4 == 0) goto L_0x002a
            java.lang.String r3 = r7.r
            java.lang.String r4 = "com.tencent.mobileqq"
            boolean r3 = r3.equals(r4)
            if (r3 == 0) goto L_0x00e5
            android.content.ComponentName r3 = r1.getComponent()
            if (r3 == 0) goto L_0x00e5
            java.lang.String r3 = "com.tencent.mobileqq.activity.LoginActivity"
            android.content.ComponentName r4 = r1.getComponent()
            java.lang.String r4 = r4.getClassName()
            boolean r3 = r3.equals(r4)
            if (r3 == 0) goto L_0x00e5
            java.util.List r0 = r0.getRunningTasks(r6)
            if (r0 == 0) goto L_0x00e5
            java.util.Iterator r3 = r0.iterator()
        L_0x0074:
            boolean r0 = r3.hasNext()
            if (r0 == 0) goto L_0x00e5
            java.lang.Object r0 = r3.next()
            android.app.ActivityManager$RunningTaskInfo r0 = (android.app.ActivityManager.RunningTaskInfo) r0
            android.content.ComponentName r4 = r0.baseActivity
            if (r4 == 0) goto L_0x0074
            java.lang.String r0 = "com.tencent.mobileqq"
            java.lang.String r5 = r4.getPackageName()
            boolean r0 = r0.equals(r5)
            if (r0 == 0) goto L_0x0074
            android.content.Intent r0 = new android.content.Intent
            r0.<init>(r1)
            r0.setComponent(r4)
        L_0x0098:
            if (r0 != 0) goto L_0x009f
            android.content.Intent r0 = new android.content.Intent
            r0.<init>(r1)
        L_0x009f:
            if (r0 == 0) goto L_0x00a9
            r1 = 1048576(0x100000, float:1.469368E-39)
            r0.addFlags(r1)
            r7.startActivity(r0)     // Catch:{ ActivityNotFoundException -> 0x00e1, Exception -> 0x00e3 }
        L_0x00a9:
            boolean r0 = r7.s
            if (r0 == 0) goto L_0x00ce
            com.tencent.assistantv2.activity.MainActivity r0 = com.tencent.assistantv2.activity.MainActivity.t()
            if (r0 == 0) goto L_0x00c3
            com.tencent.assistantv2.activity.MainActivity r0 = com.tencent.assistantv2.activity.MainActivity.t()
            if (r0 == 0) goto L_0x00ce
            com.qq.AppService.AstApp r0 = com.qq.AppService.AstApp.i()
            boolean r0 = r0.l()
            if (r0 != 0) goto L_0x00ce
        L_0x00c3:
            com.qq.AppService.AstApp r0 = com.qq.AppService.AstApp.i()
            java.lang.String r0 = r0.getPackageName()
            com.tencent.assistant.utils.r.a(r0, r2)
        L_0x00ce:
            super.finish()
            boolean r0 = r7.k()
            if (r0 == 0) goto L_0x00e0
            r0 = 2130968578(0x7f040002, float:1.7545814E38)
            r1 = 2130968581(0x7f040005, float:1.754582E38)
            r7.overridePendingTransition(r0, r1)
        L_0x00e0:
            return
        L_0x00e1:
            r0 = move-exception
            goto L_0x00a9
        L_0x00e3:
            r0 = move-exception
            goto L_0x00a9
        L_0x00e5:
            r0 = r2
            goto L_0x0098
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.activity.BaseActivity.finish():void");
    }

    public boolean k() {
        return true;
    }

    public int l() {
        return 1;
    }

    public int f() {
        return 2000;
    }

    public int m() {
        return this.p.c;
    }

    public void a(int i) {
        this.p.c = i;
        ak.a().b(i);
    }

    /* access modifiers changed from: protected */
    public boolean g() {
        return true;
    }

    public STPageInfo n() {
        this.p.f2060a = f();
        this.p.c = m();
        return this.p;
    }

    public void a(String str, String str2) {
        this.p.e = com.tencent.assistantv2.st.page.a.b(str, str2);
    }

    /* access modifiers changed from: protected */
    public void a(STPageInfo sTPageInfo, String str, String str2) {
        if (sTPageInfo != null) {
            this.p.c = sTPageInfo.f2060a;
            this.p.d = com.tencent.assistantv2.st.page.a.b(str, str2);
        }
    }

    /* access modifiers changed from: protected */
    public void o() {
        this.p.c = d.a(getIntent(), PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, 2000);
        this.p.d = d.a(getIntent(), PluginActivity.PARAMS_PRE_ACTIVITY_SLOT_TAG_NAME);
    }

    /* access modifiers changed from: protected */
    public STInfoV2 p() {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this, 100);
        if (buildSTInfo != null) {
            buildSTInfo.updateWithExternalPara(this.q);
        }
        return buildSTInfo;
    }

    /* access modifiers changed from: protected */
    public void q() {
        com.tencent.assistantv2.st.l.a(p());
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add("menu");
        return true;
    }

    public boolean onMenuOpened(int i, Menu menu) {
        this.n.clear();
        this.n.add(new at(this, R.string.menu_feedBack, R.drawable.icon_feedback));
        this.n.add(new at(this, R.string.menu_setting, R.drawable.icon_setting));
        this.n.add(new at(this, R.string.menu_exit, R.drawable.icon_shutdown));
        this.u = (LinearLayout) this.v.inflate((int) R.layout.menu_layout, (ViewGroup) null);
        if (this.u != null) {
            this.w = new PopupWindow(this.u, -1, -2);
            this.w.setOutsideTouchable(true);
            this.w.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.transparent)));
            this.x = new au(this);
            a(this.u, 0, this.n.size() - 1, 1);
        }
        if (!(this.w == null || this.u == null)) {
            if (this.w.isShowing()) {
                this.w.dismiss();
            } else {
                this.u.setFocusable(true);
                this.u.setOnKeyListener(new aq(this));
                this.u.setFocusableInTouchMode(true);
                if (!isFinishing() && !this.w.isShowing()) {
                    try {
                        this.w.showAtLocation(this.u, 80, 0, 0);
                        this.w.setFocusable(true);
                        this.w.update();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return false;
    }

    private void a(LinearLayout linearLayout, int i, int i2, int i3) {
        TypedArray obtainTypedArray = getResources().obtainTypedArray(R.array.menu_item_bgs);
        while (i <= i2) {
            at atVar = this.n.get(i);
            int a2 = atVar.a();
            LinearLayout linearLayout2 = (LinearLayout) this.v.inflate((int) R.layout.menu_item, (ViewGroup) null);
            ImageView imageView = (ImageView) linearLayout2.findViewById(R.id.menu_icon);
            TextView textView = (TextView) linearLayout2.findViewById(R.id.menu_text);
            try {
                imageView.setBackgroundResource(obtainTypedArray.getResourceId(i, 0));
            } catch (Throwable th) {
                th.printStackTrace();
            }
            textView.setText(a2);
            linearLayout2.setLayoutParams(new LinearLayout.LayoutParams(-1, -2, 1.0f));
            linearLayout2.setId(atVar.a());
            if (i == i2) {
                linearLayout2.findViewById(R.id.menu_line).setVisibility(8);
            } else {
                linearLayout2.findViewById(R.id.menu_line).setVisibility(0);
            }
            linearLayout.addView(linearLayout2);
            this.u.findViewById(atVar.a()).setOnClickListener(this.x);
            i++;
        }
        linearLayout.setWeightSum((float) ((i2 - i) + 1));
        obtainTypedArray.recycle();
    }

    /* access modifiers changed from: protected */
    public void a(View view) {
        if (this.w != null && this.w.isShowing()) {
            this.w.dismiss();
        }
        switch (view.getId()) {
            case R.string.menu_setting /*2131362237*/:
                startActivity(new Intent(this, SettingActivity.class));
                return;
            case R.string.menu_feedBack /*2131362238*/:
                startActivity(new Intent(this, HelperFeedbackActivity.class));
                return;
            case R.string.menu_exit /*2131362239*/:
                b.a().e();
                FunctionUtils.a(this);
                return;
            default:
                return;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.AppService.AstApp.a(boolean, int):void
     arg types: [int, int]
     candidates:
      com.qq.AppService.AstApp.a(android.content.pm.PackageManager, java.lang.String):void
      com.qq.AppService.AstApp.a(boolean, int):void */
    protected static void r() {
        List<ActivityManager.RunningTaskInfo> runningTasks;
        try {
            if (AstApp.i().checkCallingOrSelfPermission("android.permission.GET_TASKS") != 0 || (runningTasks = ((ActivityManager) AstApp.i().getApplicationContext().getSystemService("activity")).getRunningTasks(1)) == null || runningTasks.isEmpty()) {
                return;
            }
            if (!runningTasks.get(0).topActivity.getPackageName().equals(AstApp.i().getPackageName())) {
                AstApp.i().a(false, 0);
            } else {
                AstApp.i().a(true, 0);
            }
        } catch (Throwable th) {
        }
    }

    public void a(Dialog dialog) {
        if (dialog != null) {
            this.y = dialog;
        }
    }

    /* access modifiers changed from: private */
    public void u() {
        SelfUpdateManager.SelfUpdateInfo d = SelfUpdateManager.a().d();
        if (d != null) {
            if (SelfUpdateManager.a().h()) {
                if (d != null && SelfUpdateManager.a().k()) {
                    SelfUpdateManager.a().b(false);
                }
            } else if (d != null && !SelfUpdateManager.a().k()) {
                if (o) {
                    o = false;
                    if (SelfUpdateManager.a().p()) {
                        return;
                    }
                } else {
                    return;
                }
            }
            SelfUpdateManager.a().d(true);
            Intent intent = new Intent(this, SelfUpdateActivity.class);
            intent.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, f());
            intent.addFlags(335544320);
            startActivity(intent);
        }
    }
}
