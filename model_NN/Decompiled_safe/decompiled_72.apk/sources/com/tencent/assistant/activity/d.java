package com.tencent.assistant.activity;

import android.os.Message;
import com.tencent.assistant.localres.callback.c;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.utils.XLog;
import java.util.List;

/* compiled from: ProGuard */
class d extends c {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ApkMgrActivity f465a;

    d(ApkMgrActivity apkMgrActivity) {
        this.f465a = apkMgrActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.activity.ApkMgrActivity.a(com.tencent.assistant.activity.ApkMgrActivity, long, boolean):void
     arg types: [com.tencent.assistant.activity.ApkMgrActivity, int, int]
     candidates:
      com.tencent.assistant.activity.ApkMgrActivity.a(int, long, boolean):void
      com.tencent.assistant.activity.ApkMgrActivity.a(com.tencent.assistant.activity.ApkMgrActivity, long, int):void
      com.tencent.assistant.activity.BaseActivity.a(com.tencent.assistantv2.st.page.STPageInfo, java.lang.String, java.lang.String):void
      android.support.v4.app.FragmentActivity.a(java.lang.String, java.io.PrintWriter, android.view.View):void
      android.support.v4.app.FragmentActivity.a(java.lang.String, boolean, boolean):android.support.v4.app.z
      android.support.v4.app.FragmentActivity.a(android.support.v4.app.Fragment, android.content.Intent, int):void
      com.tencent.assistant.activity.ApkMgrActivity.a(com.tencent.assistant.activity.ApkMgrActivity, long, boolean):void */
    public void a(List<LocalApkInfo> list, boolean z, boolean z2, int i) {
        long j;
        XLog.d("ApkMgrActivity", "onApkLoadFinish--apkInfos = " + list);
        if (!this.f465a.F && !this.f465a.H) {
            long unused = this.f465a.J = System.currentTimeMillis();
            if (this.f465a.I != 0) {
                j = this.f465a.J - this.f465a.I;
            } else {
                j = 0;
            }
            this.f465a.a(j, list == null ? 0 : list.size());
            if (i == 0) {
                XLog.d("ApkMgrActivity", "onApkLoadFinish--");
                if (list == null || list.isEmpty()) {
                    boolean unused2 = this.f465a.D = true;
                }
                if (z2) {
                    this.f465a.q();
                }
                if (this.f465a.J - this.f465a.I < 1200) {
                    this.f465a.S.postDelayed(new e(this, list, z), 1200 - (this.f465a.J - this.f465a.I));
                } else {
                    this.f465a.a(list, false, z, 1, true);
                }
            } else {
                boolean unused3 = this.f465a.D = true;
                if (this.f465a.J - this.f465a.I < 1200) {
                    this.f465a.S.postDelayed(new f(this), 1200 - (this.f465a.J - this.f465a.I));
                } else {
                    this.f465a.a(0L, false);
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.activity.ApkMgrActivity.a(com.tencent.assistant.activity.ApkMgrActivity, long, boolean):void
     arg types: [com.tencent.assistant.activity.ApkMgrActivity, int, int]
     candidates:
      com.tencent.assistant.activity.ApkMgrActivity.a(int, long, boolean):void
      com.tencent.assistant.activity.ApkMgrActivity.a(com.tencent.assistant.activity.ApkMgrActivity, long, int):void
      com.tencent.assistant.activity.BaseActivity.a(com.tencent.assistantv2.st.page.STPageInfo, java.lang.String, java.lang.String):void
      android.support.v4.app.FragmentActivity.a(java.lang.String, java.io.PrintWriter, android.view.View):void
      android.support.v4.app.FragmentActivity.a(java.lang.String, boolean, boolean):android.support.v4.app.z
      android.support.v4.app.FragmentActivity.a(android.support.v4.app.Fragment, android.content.Intent, int):void
      com.tencent.assistant.activity.ApkMgrActivity.a(com.tencent.assistant.activity.ApkMgrActivity, long, boolean):void */
    public void a(List<LocalApkInfo> list) {
        XLog.d("ApkMgrActivity", "onApkLoadPortionComplete--apkInfos = " + list);
        if (!this.f465a.F && !this.f465a.H) {
            if (list == null || list.isEmpty()) {
                this.f465a.a(0L, false);
            } else {
                this.f465a.a(list, true, false, 1, true);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.activity.ApkMgrActivity.a(com.tencent.assistant.activity.ApkMgrActivity, long, int, boolean, int):void
     arg types: [com.tencent.assistant.activity.ApkMgrActivity, int, int, int, int]
     candidates:
      com.tencent.assistant.activity.ApkMgrActivity.a(java.util.List<com.tencent.assistant.localres.model.LocalApkInfo>, boolean, boolean, int, boolean):void
      com.tencent.assistant.activity.ApkMgrActivity.a(com.tencent.assistant.activity.ApkMgrActivity, long, int, boolean, int):void */
    public void a(List<LocalApkInfo> list, LocalApkInfo localApkInfo, boolean z, boolean z2) {
        XLog.d("ApkMgrActivity", "onApkDeleteFinish--apkInfos = " + list);
        if (!this.f465a.H) {
            if (!z2) {
                this.f465a.a(list, false, false, 2, false);
            } else if (!z) {
            } else {
                if (list.size() == 0) {
                    this.f465a.a(0L, 0, false, 2);
                    Message obtainMessage = this.f465a.S.obtainMessage(110009);
                    obtainMessage.obj = Long.valueOf(localApkInfo.occupySize);
                    this.f465a.S.sendMessageDelayed(obtainMessage, 1000);
                    return;
                }
                this.f465a.a(list, false, false, 2, true);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.activity.ApkMgrActivity.a(com.tencent.assistant.activity.ApkMgrActivity, long, int, boolean, int):void
     arg types: [com.tencent.assistant.activity.ApkMgrActivity, int, int, int, int]
     candidates:
      com.tencent.assistant.activity.ApkMgrActivity.a(java.util.List<com.tencent.assistant.localres.model.LocalApkInfo>, boolean, boolean, int, boolean):void
      com.tencent.assistant.activity.ApkMgrActivity.a(com.tencent.assistant.activity.ApkMgrActivity, long, int, boolean, int):void */
    public void a(List<LocalApkInfo> list, int i, long j) {
        XLog.d("ApkMgrActivity", "onApkBatchDeleteSucceed--apkInfos = " + list);
        if (!this.f465a.H) {
            if (i > 0) {
                if (list.size() == 0) {
                    this.f465a.a(0L, 0, false, 2);
                    this.f465a.R.a(new g(this, j));
                } else {
                    this.f465a.a(list, false, false, 2, false);
                    this.f465a.x();
                }
            }
            boolean unused = this.f465a.F = false;
        }
    }

    public void b(List<LocalApkInfo> list, int i, long j) {
        XLog.d("ApkMgrActivity", "onApkDidChange--apkInfos = " + list);
        if (!this.f465a.F && !this.f465a.H) {
            if (list == null || list.size() == 0) {
                Message obtainMessage = this.f465a.S.obtainMessage(110009);
                obtainMessage.obj = Long.valueOf(j);
                this.f465a.S.sendMessageDelayed(obtainMessage, 0);
                return;
            }
            this.f465a.a(list, false, false, 3, true);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.activity.ApkMgrActivity.a(com.tencent.assistant.activity.ApkMgrActivity, int, long, boolean):void
     arg types: [com.tencent.assistant.activity.ApkMgrActivity, int, long, int]
     candidates:
      com.tencent.assistant.activity.ApkMgrActivity.a(long, int, boolean, int):void
      com.tencent.assistant.activity.BaseActivity.a(android.widget.LinearLayout, int, int, int):void
      com.tencent.assistant.activity.ApkMgrActivity.a(com.tencent.assistant.activity.ApkMgrActivity, int, long, boolean):void */
    public void a(int i, int i2, long j) {
        this.f465a.a(i2, j, false);
    }
}
