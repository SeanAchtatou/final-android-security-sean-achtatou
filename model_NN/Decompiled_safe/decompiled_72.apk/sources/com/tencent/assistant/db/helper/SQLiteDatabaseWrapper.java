package com.tencent.assistant.db.helper;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

/* compiled from: ProGuard */
public class SQLiteDatabaseWrapper {

    /* renamed from: a  reason: collision with root package name */
    private SQLiteDatabase f746a;

    public SQLiteDatabaseWrapper(SQLiteDatabase sQLiteDatabase) {
        this.f746a = sQLiteDatabase;
    }

    public long insert(String str, String str2, ContentValues contentValues) {
        try {
            return this.f746a.insert(str, str2, contentValues);
        } catch (Throwable th) {
            th.printStackTrace();
            return -1;
        }
    }

    public Cursor rawQuery(String str, String[] strArr) {
        try {
            return this.f746a.rawQuery(str, strArr);
        } catch (Throwable th) {
            th.printStackTrace();
            return null;
        }
    }

    public int delete(String str, String str2, String[] strArr) {
        try {
            return this.f746a.delete(str, str2, strArr);
        } catch (Throwable th) {
            th.printStackTrace();
            return 0;
        }
    }

    public Cursor query(String str, String[] strArr, String str2, String[] strArr2, String str3, String str4, String str5) {
        try {
            return this.f746a.query(str, strArr, str2, strArr2, str3, str4, str5);
        } catch (Throwable th) {
            th.printStackTrace();
            return null;
        }
    }

    public int update(String str, ContentValues contentValues, String str2, String[] strArr) {
        try {
            return this.f746a.update(str, contentValues, str2, strArr);
        } catch (Throwable th) {
            th.printStackTrace();
            return 0;
        }
    }

    public void beginTransaction() {
        try {
            this.f746a.beginTransaction();
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public SQLiteStatement compileStatement(String str) {
        try {
            return this.f746a.compileStatement(str);
        } catch (Throwable th) {
            th.printStackTrace();
            return null;
        }
    }

    public void setTransactionSuccessful() {
        try {
            this.f746a.setTransactionSuccessful();
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public void endTransaction() {
        try {
            this.f746a.endTransaction();
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public void close() {
        try {
            this.f746a.close();
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public Cursor query(String str, String[] strArr, String str2, String[] strArr2, String str3, String str4, String str5, String str6) {
        try {
            return this.f746a.query(str, strArr, str2, strArr2, str3, str4, str5, str6);
        } catch (Throwable th) {
            th.printStackTrace();
            return null;
        }
    }
}
