package com.tencent.assistant.db.table;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import com.qq.AppService.AstApp;
import com.tencent.assistant.db.helper.AstDbHelper;
import com.tencent.assistant.db.helper.SQLiteDatabaseWrapper;
import com.tencent.assistant.db.helper.SqliteHelper;
import com.tencent.assistant.localres.model.a;
import java.util.List;

/* compiled from: ProGuard */
public class b implements IBaseTable {

    /* renamed from: a  reason: collision with root package name */
    public static final String f749a = b.class.getSimpleName();

    public synchronized int a(List<a> list) {
        int i;
        int i2;
        if (list != null) {
            if (list.size() != 0) {
                List<a> b = b();
                if (b != null) {
                    for (int i3 = 0; i3 < b.size(); i3 = i2 + 1) {
                        a aVar = b.get(i3);
                        int i4 = 0;
                        while (true) {
                            if (i4 >= list.size()) {
                                i2 = i3;
                                break;
                            }
                            a aVar2 = list.get(i4);
                            if (!aVar.f845a.equals(aVar2.f845a) || aVar.b != aVar2.b) {
                                i4++;
                            } else if (aVar.d != aVar2.d) {
                                aVar.i = 1;
                                i2 = i3;
                            } else if (aVar.g - aVar.e > 259200000) {
                                aVar2.e = aVar.g;
                                aVar2.f = aVar.h;
                                aVar.i = 1;
                                i2 = i3;
                            } else {
                                b.remove(i3);
                                i2 = i3 - 1;
                            }
                        }
                        if (i4 == list.size()) {
                            aVar.i = 1;
                        }
                    }
                }
                list.addAll(b);
                c();
                SQLiteDatabaseWrapper writableDatabaseWrapper = getHelper().getWritableDatabaseWrapper();
                int i5 = 0;
                for (a next : list) {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put("packageName", next.f845a);
                    contentValues.put("versoncode", Integer.valueOf(next.b));
                    contentValues.put("traffic", Long.valueOf(next.h));
                    contentValues.put("start", Long.valueOf(next.e));
                    contentValues.put("end", Long.valueOf(next.g));
                    contentValues.put("flag", Integer.valueOf(next.i));
                    contentValues.put("startTraffic", Long.valueOf(next.f));
                    contentValues.put("bootTime", Long.valueOf(next.d));
                    i5 = (int) (((long) i5) + writableDatabaseWrapper.insert("apptraffic_table", null, contentValues));
                }
                i = i5;
            }
        }
        i = 0;
        return i;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:23:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0090, code lost:
        if (r0 != null) goto L_0x0092;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:?, code lost:
        r0.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0096, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0097, code lost:
        r8 = r1;
        r1 = r0;
        r0 = r8;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0088 A[SYNTHETIC, Splitter:B:22:0x0088] */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x008f A[ExcHandler: Exception (e java.lang.Exception), Splitter:B:5:0x0012] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized java.util.List<com.tencent.assistant.protocol.jce.StatTraffic> a() {
        /*
            r9 = this;
            r0 = 0
            monitor-enter(r9)
            java.util.ArrayList r1 = new java.util.ArrayList     // Catch:{ all -> 0x008c }
            r1.<init>()     // Catch:{ all -> 0x008c }
            com.tencent.assistant.db.helper.SqliteHelper r2 = r9.getHelper()     // Catch:{ all -> 0x008c }
            com.tencent.assistant.db.helper.SQLiteDatabaseWrapper r2 = r2.getWritableDatabaseWrapper()     // Catch:{ all -> 0x008c }
            java.lang.String r3 = "select * from apptraffic_table where flag > 0 "
            r4 = 0
            android.database.Cursor r0 = r2.rawQuery(r3, r4)     // Catch:{ Exception -> 0x008f, all -> 0x0082 }
            if (r0 == 0) goto L_0x007b
            boolean r3 = r0.moveToFirst()     // Catch:{ Exception -> 0x008f, all -> 0x0096 }
            if (r3 == 0) goto L_0x007b
        L_0x001e:
            com.tencent.assistant.protocol.jce.StatTraffic r3 = new com.tencent.assistant.protocol.jce.StatTraffic     // Catch:{ Exception -> 0x008f, all -> 0x0096 }
            r3.<init>()     // Catch:{ Exception -> 0x008f, all -> 0x0096 }
            java.lang.String r4 = "packageName"
            int r4 = r0.getColumnIndex(r4)     // Catch:{ Exception -> 0x008f, all -> 0x0096 }
            java.lang.String r4 = r0.getString(r4)     // Catch:{ Exception -> 0x008f, all -> 0x0096 }
            r3.f1566a = r4     // Catch:{ Exception -> 0x008f, all -> 0x0096 }
            java.lang.String r4 = "versoncode"
            int r4 = r0.getColumnIndex(r4)     // Catch:{ Exception -> 0x008f, all -> 0x0096 }
            int r4 = r0.getInt(r4)     // Catch:{ Exception -> 0x008f, all -> 0x0096 }
            r3.b = r4     // Catch:{ Exception -> 0x008f, all -> 0x0096 }
            java.lang.String r4 = "traffic"
            int r4 = r0.getColumnIndex(r4)     // Catch:{ Exception -> 0x008f, all -> 0x0096 }
            long r4 = r0.getLong(r4)     // Catch:{ Exception -> 0x008f, all -> 0x0096 }
            java.lang.String r6 = "startTraffic"
            int r6 = r0.getColumnIndex(r6)     // Catch:{ Exception -> 0x008f, all -> 0x0096 }
            long r6 = r0.getLong(r6)     // Catch:{ Exception -> 0x008f, all -> 0x0096 }
            long r4 = r4 - r6
            r3.c = r4     // Catch:{ Exception -> 0x008f, all -> 0x0096 }
            java.lang.String r4 = "start"
            int r4 = r0.getColumnIndex(r4)     // Catch:{ Exception -> 0x008f, all -> 0x0096 }
            long r4 = r0.getLong(r4)     // Catch:{ Exception -> 0x008f, all -> 0x0096 }
            r3.d = r4     // Catch:{ Exception -> 0x008f, all -> 0x0096 }
            java.lang.String r4 = "end"
            int r4 = r0.getColumnIndex(r4)     // Catch:{ Exception -> 0x008f, all -> 0x0096 }
            long r4 = r0.getLong(r4)     // Catch:{ Exception -> 0x008f, all -> 0x0096 }
            r3.e = r4     // Catch:{ Exception -> 0x008f, all -> 0x0096 }
            r1.add(r3)     // Catch:{ Exception -> 0x008f, all -> 0x0096 }
            boolean r3 = r0.moveToNext()     // Catch:{ Exception -> 0x008f, all -> 0x0096 }
            if (r3 != 0) goto L_0x001e
            java.lang.String r3 = "apptraffic_table"
            java.lang.String r4 = " flag > 0 "
            r5 = 0
            r2.delete(r3, r4, r5)     // Catch:{ Exception -> 0x008f, all -> 0x0096 }
        L_0x007b:
            if (r0 == 0) goto L_0x0080
            r0.close()     // Catch:{ all -> 0x008c }
        L_0x0080:
            monitor-exit(r9)
            return r1
        L_0x0082:
            r1 = move-exception
            r8 = r1
            r1 = r0
            r0 = r8
        L_0x0086:
            if (r1 == 0) goto L_0x008b
            r1.close()     // Catch:{ all -> 0x008c }
        L_0x008b:
            throw r0     // Catch:{ all -> 0x008c }
        L_0x008c:
            r0 = move-exception
            monitor-exit(r9)
            throw r0
        L_0x008f:
            r2 = move-exception
            if (r0 == 0) goto L_0x0080
            r0.close()     // Catch:{ all -> 0x008c }
            goto L_0x0080
        L_0x0096:
            r1 = move-exception
            r8 = r1
            r1 = r0
            r0 = r8
            goto L_0x0086
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.db.table.b.a():java.util.List");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0092, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0095, code lost:
        return r9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x009b, code lost:
        if (r1 == null) goto L_0x0095;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0090, code lost:
        if (r1 != null) goto L_0x0092;
     */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x00a2  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.List<com.tencent.assistant.localres.model.a> b() {
        /*
            r10 = this;
            r8 = 0
            java.util.ArrayList r9 = new java.util.ArrayList
            r9.<init>()
            com.tencent.assistant.db.helper.SqliteHelper r0 = r10.getHelper()
            com.tencent.assistant.db.helper.SQLiteDatabaseWrapper r0 = r0.getReadableDatabaseWrapper()
            java.lang.String r1 = "apptraffic_table"
            r2 = 0
            r3 = 0
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ Exception -> 0x0096, all -> 0x009e }
            if (r1 == 0) goto L_0x0090
            boolean r0 = r1.moveToFirst()     // Catch:{ Exception -> 0x00a8 }
            if (r0 == 0) goto L_0x0090
        L_0x0022:
            com.tencent.assistant.localres.model.a r0 = new com.tencent.assistant.localres.model.a     // Catch:{ Exception -> 0x00a8 }
            r0.<init>()     // Catch:{ Exception -> 0x00a8 }
            java.lang.String r2 = "packageName"
            int r2 = r1.getColumnIndex(r2)     // Catch:{ Exception -> 0x00a8 }
            java.lang.String r2 = r1.getString(r2)     // Catch:{ Exception -> 0x00a8 }
            r0.f845a = r2     // Catch:{ Exception -> 0x00a8 }
            java.lang.String r2 = "versoncode"
            int r2 = r1.getColumnIndex(r2)     // Catch:{ Exception -> 0x00a8 }
            int r2 = r1.getInt(r2)     // Catch:{ Exception -> 0x00a8 }
            r0.b = r2     // Catch:{ Exception -> 0x00a8 }
            java.lang.String r2 = "traffic"
            int r2 = r1.getColumnIndex(r2)     // Catch:{ Exception -> 0x00a8 }
            long r2 = r1.getLong(r2)     // Catch:{ Exception -> 0x00a8 }
            r0.h = r2     // Catch:{ Exception -> 0x00a8 }
            java.lang.String r2 = "start"
            int r2 = r1.getColumnIndex(r2)     // Catch:{ Exception -> 0x00a8 }
            long r2 = r1.getLong(r2)     // Catch:{ Exception -> 0x00a8 }
            r0.e = r2     // Catch:{ Exception -> 0x00a8 }
            java.lang.String r2 = "end"
            int r2 = r1.getColumnIndex(r2)     // Catch:{ Exception -> 0x00a8 }
            long r2 = r1.getLong(r2)     // Catch:{ Exception -> 0x00a8 }
            r0.g = r2     // Catch:{ Exception -> 0x00a8 }
            java.lang.String r2 = "flag"
            int r2 = r1.getColumnIndex(r2)     // Catch:{ Exception -> 0x00a8 }
            int r2 = r1.getInt(r2)     // Catch:{ Exception -> 0x00a8 }
            r0.i = r2     // Catch:{ Exception -> 0x00a8 }
            java.lang.String r2 = "startTraffic"
            int r2 = r1.getColumnIndex(r2)     // Catch:{ Exception -> 0x00a8 }
            long r2 = r1.getLong(r2)     // Catch:{ Exception -> 0x00a8 }
            r0.f = r2     // Catch:{ Exception -> 0x00a8 }
            java.lang.String r2 = "bootTime"
            int r2 = r1.getColumnIndex(r2)     // Catch:{ Exception -> 0x00a8 }
            long r2 = r1.getLong(r2)     // Catch:{ Exception -> 0x00a8 }
            r0.d = r2     // Catch:{ Exception -> 0x00a8 }
            r9.add(r0)     // Catch:{ Exception -> 0x00a8 }
            boolean r0 = r1.moveToNext()     // Catch:{ Exception -> 0x00a8 }
            if (r0 != 0) goto L_0x0022
        L_0x0090:
            if (r1 == 0) goto L_0x0095
        L_0x0092:
            r1.close()
        L_0x0095:
            return r9
        L_0x0096:
            r0 = move-exception
            r1 = r8
        L_0x0098:
            r0.printStackTrace()     // Catch:{ all -> 0x00a6 }
            if (r1 == 0) goto L_0x0095
            goto L_0x0092
        L_0x009e:
            r0 = move-exception
            r1 = r8
        L_0x00a0:
            if (r1 == 0) goto L_0x00a5
            r1.close()
        L_0x00a5:
            throw r0
        L_0x00a6:
            r0 = move-exception
            goto L_0x00a0
        L_0x00a8:
            r0 = move-exception
            goto L_0x0098
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.db.table.b.b():java.util.List");
    }

    public int c() {
        return getHelper().getWritableDatabaseWrapper().delete("apptraffic_table", null, null);
    }

    public int tableVersion() {
        return 1;
    }

    public String tableName() {
        return "apptraffic_table";
    }

    public String createTableSQL() {
        return "CREATE TABLE if not exists apptraffic_table( _id INTEGER PRIMARY KEY AUTOINCREMENT, packageName TEXT , versoncode INTEGER, start INTEGER, end INTEGER, traffic INTEGER, flag INTEGER, startTraffic INTEGER,bootTime INTEGER);";
    }

    public String[] getAlterSQL(int i, int i2) {
        if (i != 1 || i2 != 2) {
            return null;
        }
        return new String[]{"alter table apptraffic_table add column startTraffic INTEGER;", "alter table apptraffic_table add column bootTime INTEGER;"};
    }

    public void beforeTableAlter(int i, int i2, SQLiteDatabase sQLiteDatabase) {
    }

    public SqliteHelper getHelper() {
        return AstDbHelper.get(AstApp.i());
    }

    public void afterTableAlter(int i, int i2, SQLiteDatabase sQLiteDatabase) {
    }
}
