package com.tencent.assistant.db.table;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.qq.AppService.AstApp;
import com.tencent.assistant.db.a.b;
import com.tencent.assistant.db.helper.SqliteHelper;
import com.tencent.assistant.db.helper.StatDbHelper;
import com.tencent.open.SocialConstants;
import java.util.List;

/* compiled from: ProGuard */
public class y implements IBaseTable {

    /* renamed from: a  reason: collision with root package name */
    private static volatile y f755a;

    public y() {
    }

    public y(Context context) {
    }

    public static synchronized y a() {
        y yVar;
        synchronized (y.class) {
            if (f755a == null) {
                f755a = new y(AstApp.i());
            }
            yVar = f755a;
        }
        return yVar;
    }

    public boolean a(byte b, String str, int i, byte[] bArr) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(SocialConstants.PARAM_TYPE, Byte.valueOf(b));
        contentValues.put("package_name", str);
        contentValues.put("version_code", Integer.valueOf(i));
        contentValues.put("install_time", Long.valueOf(System.currentTimeMillis()));
        contentValues.put("data", bArr);
        return getHelper().getWritableDatabaseWrapper().insert("st_install_data", null, contentValues) > 0;
    }

    private b a(Cursor cursor) {
        b bVar = new b();
        bVar.f740a = cursor.getLong(cursor.getColumnIndex("_id"));
        bVar.b = cursor.getInt(cursor.getColumnIndex(SocialConstants.PARAM_TYPE));
        bVar.c = cursor.getString(cursor.getColumnIndex("package_name"));
        bVar.d = cursor.getInt(cursor.getColumnIndex("version_code"));
        bVar.e = cursor.getLong(cursor.getColumnIndex("install_time"));
        bVar.f = cursor.getBlob(cursor.getColumnIndex("data"));
        return bVar;
    }

    /* JADX WARNING: Removed duplicated region for block: B:23:0x0064  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.List<com.tencent.assistant.db.a.b> a(long r12, int r14) {
        /*
            r11 = this;
            r9 = 0
            java.util.ArrayList r10 = new java.util.ArrayList
            r10.<init>()
            if (r14 <= 0) goto L_0x006d
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "0,"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.StringBuilder r0 = r0.append(r14)
            java.lang.String r8 = r0.toString()
        L_0x001b:
            com.tencent.assistant.db.helper.SqliteHelper r0 = r11.getHelper()
            com.tencent.assistant.db.helper.SQLiteDatabaseWrapper r0 = r0.getReadableDatabaseWrapper()
            java.lang.String r1 = "st_install_data"
            r2 = 0
            java.lang.String r3 = "install_time <= ?"
            r4 = 1
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ Exception -> 0x0056, all -> 0x0061 }
            r5 = 0
            java.lang.String r6 = java.lang.Long.toString(r12)     // Catch:{ Exception -> 0x0056, all -> 0x0061 }
            r4[r5] = r6     // Catch:{ Exception -> 0x0056, all -> 0x0061 }
            r5 = 0
            r6 = 0
            java.lang.String r7 = "_id asc"
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5, r6, r7, r8)     // Catch:{ Exception -> 0x0056, all -> 0x0061 }
            if (r1 == 0) goto L_0x004f
            boolean r0 = r1.moveToFirst()     // Catch:{ Exception -> 0x006b }
            if (r0 == 0) goto L_0x004f
        L_0x0042:
            com.tencent.assistant.db.a.b r0 = r11.a(r1)     // Catch:{ Exception -> 0x006b }
            r10.add(r0)     // Catch:{ Exception -> 0x006b }
            boolean r0 = r1.moveToNext()     // Catch:{ Exception -> 0x006b }
            if (r0 != 0) goto L_0x0042
        L_0x004f:
            if (r1 == 0) goto L_0x0054
            r1.close()
        L_0x0054:
            r9 = r10
        L_0x0055:
            return r9
        L_0x0056:
            r0 = move-exception
            r1 = r9
        L_0x0058:
            r0.printStackTrace()     // Catch:{ all -> 0x0068 }
            if (r1 == 0) goto L_0x0055
            r1.close()
            goto L_0x0055
        L_0x0061:
            r0 = move-exception
        L_0x0062:
            if (r9 == 0) goto L_0x0067
            r9.close()
        L_0x0067:
            throw r0
        L_0x0068:
            r0 = move-exception
            r9 = r1
            goto L_0x0062
        L_0x006b:
            r0 = move-exception
            goto L_0x0058
        L_0x006d:
            r8 = r9
            goto L_0x001b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.db.table.y.a(long, int):java.util.List");
    }

    public boolean a(List<Long> list) {
        if (list == null || list.size() == 0) {
            return false;
        }
        StringBuffer stringBuffer = new StringBuffer("(");
        for (Long append : list) {
            stringBuffer.append(append);
            stringBuffer.append(",");
        }
        if (stringBuffer.length() > 1) {
            stringBuffer.deleteCharAt(stringBuffer.length() - 1);
        }
        stringBuffer.append(")");
        getHelper().getWritableDatabaseWrapper().delete("st_install_data", "_id in " + stringBuffer.toString(), null);
        return true;
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0043  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x004c  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x006e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean a(java.lang.String r13, int r14) {
        /*
            r12 = this;
            boolean r0 = android.text.TextUtils.isEmpty(r13)
            if (r0 == 0) goto L_0x0008
            r0 = 0
        L_0x0007:
            return r0
        L_0x0008:
            com.tencent.assistant.db.helper.SqliteHelper r0 = r12.getHelper()
            com.tencent.assistant.db.helper.SQLiteDatabaseWrapper r0 = r0.getReadableDatabaseWrapper()
            r9 = 0
            r10 = -1
            java.lang.String r1 = "st_install_data"
            r2 = 0
            java.lang.String r3 = "package_name =? and version_code =? "
            r4 = 2
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ Exception -> 0x005f, all -> 0x006b }
            r5 = 0
            r4[r5] = r13     // Catch:{ Exception -> 0x005f, all -> 0x006b }
            r5 = 1
            java.lang.String r6 = java.lang.String.valueOf(r14)     // Catch:{ Exception -> 0x005f, all -> 0x006b }
            r4[r5] = r6     // Catch:{ Exception -> 0x005f, all -> 0x006b }
            r5 = 0
            r6 = 0
            java.lang.String r7 = "_id desc"
            java.lang.String r8 = "1"
            android.database.Cursor r2 = r0.query(r1, r2, r3, r4, r5, r6, r7, r8)     // Catch:{ Exception -> 0x005f, all -> 0x006b }
            if (r2 == 0) goto L_0x0077
            boolean r1 = r2.moveToFirst()     // Catch:{ Exception -> 0x0075 }
            if (r1 == 0) goto L_0x0077
            java.lang.String r1 = "_id"
            int r1 = r2.getColumnIndex(r1)     // Catch:{ Exception -> 0x0075 }
            long r3 = r2.getLong(r1)     // Catch:{ Exception -> 0x0075 }
        L_0x0041:
            if (r2 == 0) goto L_0x0046
            r2.close()
        L_0x0046:
            r1 = 0
            int r1 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r1 <= 0) goto L_0x005d
            java.lang.String r1 = "st_install_data"
            java.lang.String r2 = "_id =? "
            r5 = 1
            java.lang.String[] r5 = new java.lang.String[r5]
            r6 = 0
            java.lang.String r3 = java.lang.String.valueOf(r3)
            r5[r6] = r3
            r0.delete(r1, r2, r5)
        L_0x005d:
            r0 = 1
            goto L_0x0007
        L_0x005f:
            r1 = move-exception
            r2 = r9
        L_0x0061:
            r1.printStackTrace()     // Catch:{ all -> 0x0072 }
            if (r2 == 0) goto L_0x0069
            r2.close()
        L_0x0069:
            r3 = r10
            goto L_0x0046
        L_0x006b:
            r0 = move-exception
        L_0x006c:
            if (r9 == 0) goto L_0x0071
            r9.close()
        L_0x0071:
            throw r0
        L_0x0072:
            r0 = move-exception
            r9 = r2
            goto L_0x006c
        L_0x0075:
            r1 = move-exception
            goto L_0x0061
        L_0x0077:
            r3 = r10
            goto L_0x0041
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.db.table.y.a(java.lang.String, int):boolean");
    }

    public int tableVersion() {
        return 1;
    }

    public String tableName() {
        return "st_install_data";
    }

    public String createTableSQL() {
        return "CREATE TABLE if not exists st_install_data (_id INTEGER PRIMARY KEY AUTOINCREMENT,type INTEGER,package_name TEXT,version_code INTEGER,install_time INTEGER,data BLOB);";
    }

    public String[] getAlterSQL(int i, int i2) {
        if (i2 != 5) {
            return null;
        }
        return new String[]{"CREATE TABLE if not exists st_install_data (_id INTEGER PRIMARY KEY AUTOINCREMENT,type INTEGER,package_name TEXT,version_code INTEGER,install_time INTEGER,data BLOB);"};
    }

    public void beforeTableAlter(int i, int i2, SQLiteDatabase sQLiteDatabase) {
    }

    public void afterTableAlter(int i, int i2, SQLiteDatabase sQLiteDatabase) {
    }

    public SqliteHelper getHelper() {
        return StatDbHelper.get(AstApp.i());
    }
}
