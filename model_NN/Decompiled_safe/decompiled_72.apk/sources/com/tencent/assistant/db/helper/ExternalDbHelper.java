package com.tencent.assistant.db.helper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import com.tencent.assistant.db.table.v;
import com.tencent.assistant.db.table.w;

/* compiled from: ProGuard */
public class ExternalDbHelper extends SqliteHelper {
    private static final String DB_NAME = "external.db";
    private static final int DB_VERSION = 1;
    private static final Class<?>[] TABLESS = {w.class, v.class};
    private static volatile SqliteHelper instance;

    public static synchronized SqliteHelper get(Context context) {
        SqliteHelper sqliteHelper;
        synchronized (ExternalDbHelper.class) {
            if (instance == null) {
                instance = new ExternalDbHelper(context, DB_NAME, null, 1);
            }
            sqliteHelper = instance;
        }
        return sqliteHelper;
    }

    public ExternalDbHelper(Context context, String str, SQLiteDatabase.CursorFactory cursorFactory, int i) {
        super(context, DB_NAME, null, i);
    }

    public Class<?>[] getTables() {
        return TABLESS;
    }

    public int getDBVersion() {
        return 1;
    }
}
