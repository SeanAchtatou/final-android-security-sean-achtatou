package com.tencent.assistant.smartcard.f;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.protocol.jce.SmartCard;
import com.tencent.assistant.protocol.jce.SmartCardAppList;
import com.tencent.assistant.protocol.jce.SmartCardCpaAdvertise;
import com.tencent.assistant.protocol.jce.SmartCardGrabNumber;
import com.tencent.assistant.protocol.jce.SmartCardHotWords;
import com.tencent.assistant.protocol.jce.SmartCardLibao;
import com.tencent.assistant.protocol.jce.SmartCardOp;
import com.tencent.assistant.protocol.jce.SmartCardPlayerShow;
import com.tencent.assistant.protocol.jce.SmartCardRecommandation;
import com.tencent.assistant.protocol.jce.SmartCardReservation;
import com.tencent.assistant.protocol.jce.SmartCardTemplate;
import com.tencent.assistant.smartcard.d.aa;
import com.tencent.assistant.smartcard.d.b;
import com.tencent.assistant.smartcard.d.c;
import com.tencent.assistant.smartcard.d.d;
import com.tencent.assistant.smartcard.d.h;
import com.tencent.assistant.smartcard.d.l;
import com.tencent.assistant.smartcard.d.n;
import com.tencent.assistant.smartcard.d.p;
import com.tencent.assistant.smartcard.d.r;
import com.tencent.assistant.smartcard.d.s;
import com.tencent.assistant.smartcard.d.t;
import com.tencent.assistant.smartcard.d.z;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.an;
import com.tencent.tmsecurelite.optimize.ISystemOptimize;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class a {
    public static List<n> a(List<SmartCard> list, int i) {
        n nVar;
        if (list == null || list.size() <= 0) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= list.size()) {
                return arrayList;
            }
            try {
                nVar = a(list.get(i3));
            } catch (Exception e) {
                XLog.e("SmartCard", "getSmartCardModel exception", e);
                nVar = null;
            }
            if (nVar != null) {
                nVar.t = i;
                arrayList.add(nVar);
            }
            i2 = i3 + 1;
        }
    }

    public static n a(SmartCard smartCard) {
        JceStruct b;
        if (smartCard == null || smartCard.b == null) {
            return null;
        }
        switch (smartCard.f1498a) {
            case 0:
                aa aaVar = new aa();
                aaVar.j = 0;
                aaVar.a((SmartCardOp) an.b(smartCard.b, SmartCardOp.class));
                return aaVar;
            case 1:
                h hVar = new h();
                hVar.a((SmartCardLibao) an.b(smartCard.b, SmartCardLibao.class), smartCard.f1498a);
                return hVar;
            case 2:
            case 3:
            case 4:
                b bVar = new b();
                bVar.a((SmartCardAppList) an.b(smartCard.b, SmartCardAppList.class), smartCard.f1498a);
                return bVar;
            case 5:
            case 7:
            case 8:
                p pVar = new p();
                if (pVar.a(smartCard.f1498a, (SmartCardRecommandation) an.b(smartCard.b, SmartCardRecommandation.class))) {
                    return pVar;
                }
                break;
            case 6:
                t tVar = new t();
                tVar.a((SmartCardAppList) an.b(smartCard.b, SmartCardAppList.class), smartCard.f1498a);
                return tVar;
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
            case ISystemOptimize.T_getMemoryUsageAsync /*27*/:
                SmartCardTemplate smartCardTemplate = (SmartCardTemplate) an.b(smartCard.b, SmartCardTemplate.class);
                z zVar = new z();
                if (zVar.a(smartCard.f1498a, smartCardTemplate.g, smartCardTemplate)) {
                    return zVar;
                }
                break;
            case 16:
                c cVar = new c();
                cVar.a((SmartCardReservation) an.b(smartCard.b, SmartCardReservation.class), smartCard.f1498a);
                return cVar;
            case 17:
                s sVar = new s();
                sVar.a((SmartCardGrabNumber) an.b(smartCard.b, SmartCardGrabNumber.class), smartCard.f1498a);
                return sVar;
            case 18:
            case 19:
            case 20:
            case 21:
            case ISystemOptimize.T_hasRootAsync /*25*/:
            case ISystemOptimize.T_askForRootAsync /*26*/:
            default:
                com.tencent.assistant.smartcard.c.c a2 = com.tencent.assistant.smartcard.c.b.a(smartCard.f1498a);
                if (!(a2 == null || (b = an.b(smartCard.b, a2.a())) == null)) {
                    com.tencent.assistant.smartcard.d.a b2 = a2.b();
                    if (b2.a(smartCard.f1498a, b)) {
                        return b2;
                    }
                }
                break;
            case ISystemOptimize.T_killTaskAsync /*22*/:
                l lVar = new l();
                lVar.a((SmartCardHotWords) an.b(smartCard.b, SmartCardHotWords.class), smartCard.f1498a);
                return lVar;
            case 23:
                r rVar = new r();
                rVar.a((SmartCardPlayerShow) an.b(smartCard.b, SmartCardPlayerShow.class), smartCard.f1498a);
                return rVar;
            case ISystemOptimize.T_checkVersionAsync /*24*/:
                d dVar = new d();
                dVar.a((SmartCardCpaAdvertise) an.b(smartCard.b, SmartCardCpaAdvertise.class), smartCard.f1498a);
                return dVar;
        }
        return null;
    }
}
