package com.tencent.assistant.smartcard.d;

import com.tencent.assistant.smartcard.c.b;
import java.util.List;

/* compiled from: ProGuard */
public class n {
    public int j;
    public int k;
    public String l;
    public String m;
    public String n;
    public String o;
    public String p;
    public boolean q;
    public String r;
    public byte[] s = null;
    public int t = 2000;
    public int u = -1;

    public int j() {
        return this.j;
    }

    public String k() {
        return this.l;
    }

    public void a(int i) {
        this.u = i;
    }

    public int l() {
        return a(this.j, this.k);
    }

    public static int a(int i, int i2) {
        if (i == 16 || i == 0 || i == 17) {
            if (i == 0) {
                return i2 + 10000;
            }
            return (i * 1000) + i2;
        } else if (b.a(i) != null) {
            return (100000 * i) + i2;
        } else {
            return i;
        }
    }

    public List<Long> d() {
        return null;
    }

    public void a(List<Long> list) {
    }

    public String d_() {
        return String.valueOf(j());
    }

    public String toString() {
        return "id:" + this.k + ",type:" + this.j + ",title:" + this.l + "," + super.toString();
    }
}
