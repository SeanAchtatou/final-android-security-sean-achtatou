package com.tencent.assistant.smartcard.c;

import android.util.Pair;
import com.tencent.assistant.smartcard.d.n;
import com.tencent.assistant.smartcard.d.u;
import com.tencent.assistant.smartcard.d.v;
import com.tencent.assistant.smartcard.d.w;
import com.tencent.assistant.smartcard.d.x;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.bo;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoV2;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* compiled from: ProGuard */
public abstract class z {
    private static long d = 0;
    private static long e = TesDownloadConfig.TES_CONFIG_CHECK_PERIOD;

    /* renamed from: a  reason: collision with root package name */
    protected Map<Integer, w> f1692a = Collections.synchronizedMap(new HashMap());
    protected Map<Integer, x> b = Collections.synchronizedMap(new HashMap());
    protected Map<Integer, List<u>> c = Collections.synchronizedMap(new HashMap());

    public abstract boolean a(n nVar, List<Long> list);

    public void a(v vVar) {
        w wVar;
        int i;
        if (vVar != null) {
            w wVar2 = this.f1692a.get(Integer.valueOf(vVar.a()));
            if (wVar2 == null) {
                w wVar3 = new w();
                wVar3.e = vVar.f1765a;
                wVar3.f = vVar.b;
                wVar = wVar3;
            } else {
                wVar = wVar2;
            }
            if (vVar.d) {
                wVar.d = true;
            }
            if (vVar.c) {
                if (a(vVar.e * 1000)) {
                    wVar.f1766a++;
                }
                x xVar = this.b.get(Integer.valueOf(vVar.a()));
                if (xVar == null || xVar.i <= 0) {
                    i = 7;
                } else {
                    i = xVar.i;
                }
                if (a(vVar.e * 1000, (long) i)) {
                    wVar.b++;
                }
                wVar.c++;
            }
            this.f1692a.put(Integer.valueOf(wVar.a()), wVar);
        }
    }

    public void a(u uVar) {
        if (uVar != null) {
            Object obj = this.c.get(Integer.valueOf(uVar.f1764a));
            if (obj == null) {
                obj = new ArrayList();
            }
            obj.add(uVar);
            this.c.put(Integer.valueOf(uVar.f1764a), obj);
        }
    }

    public void a(x xVar) {
        this.b.put(Integer.valueOf(xVar.a()), xVar);
    }

    public void a(n nVar) {
    }

    public void a(int i, String str, int i2) {
        STInfoV2 sTInfoV2 = new STInfoV2(202900 + i2, STConst.ST_DEFAULT_SLOT, i, STConst.ST_DEFAULT_SLOT, 100);
        sTInfoV2.extraData = str;
        l.a(sTInfoV2);
    }

    public Pair<Boolean, w> b(n nVar) {
        w wVar = this.f1692a.get(Integer.valueOf(nVar.l()));
        x xVar = this.b.get(Integer.valueOf(nVar.l()));
        if (wVar == null) {
            wVar = new w();
            wVar.f = nVar.k;
            wVar.e = nVar.j;
            this.f1692a.put(Integer.valueOf(wVar.a()), wVar);
        }
        if (xVar == null) {
            return Pair.create(false, wVar);
        }
        if (wVar.b >= xVar.b) {
            a(nVar.t, nVar.k + "||" + nVar.j + "|" + 1, nVar.j);
            return Pair.create(false, wVar);
        } else if (wVar.f1766a < xVar.f1767a) {
            return Pair.create(true, wVar);
        } else {
            a(nVar.t, nVar.k + "||" + nVar.j + "|" + 2, nVar.j);
            return Pair.create(false, wVar);
        }
    }

    private static void a() {
        if (System.currentTimeMillis() - d > e) {
            d = bo.d();
        }
    }

    protected static boolean a(long j) {
        a();
        long j2 = j - d;
        if (j2 <= 0 || j2 >= e) {
            return false;
        }
        return true;
    }

    protected static boolean a(long j, long j2) {
        a();
        if (j - (d - ((j2 - 1) * e)) > 0) {
            return true;
        }
        return false;
    }
}
