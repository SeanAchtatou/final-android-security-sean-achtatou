package com.tencent.assistant.smartcard.component;

import android.content.Context;
import android.util.Log;
import com.tencent.assistant.component.invalidater.IViewInvalidater;
import com.tencent.assistant.smartcard.c.b;
import com.tencent.assistant.smartcard.c.c;
import com.tencent.assistant.smartcard.d.n;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.adapter.smartlist.z;
import com.tencent.game.smartcard.view.NormalSmartCardGameFriendHorizontalItem;
import com.tencent.tmsecurelite.optimize.ISystemOptimize;
import java.util.HashMap;

/* compiled from: ProGuard */
public class aq {
    private static aq b;

    /* renamed from: a  reason: collision with root package name */
    private HashMap<ar, Integer> f1717a = new HashMap<>();

    public static synchronized aq a() {
        aq aqVar;
        synchronized (aq.class) {
            if (b == null) {
                b = new aq();
            }
            aqVar = b;
        }
        return aqVar;
    }

    public ISmartcard a(Context context, n nVar, as asVar, int i, z zVar, IViewInvalidater iViewInvalidater) {
        ISmartcard normalSmartCardGameFriendHorizontalItem;
        ISmartcard iSmartcard = null;
        if (!(nVar == null || zVar == null)) {
            switch (nVar.j) {
                case 0:
                    iSmartcard = new NormalSmartcardTopicItem(context, nVar, asVar, iViewInvalidater);
                    break;
                case 1:
                    iSmartcard = new NormalSmartcardLibaoItem(context, nVar, asVar, iViewInvalidater);
                    break;
                case 2:
                case 3:
                case 4:
                    iSmartcard = new NormalSmartcardAppListItem(context, nVar, asVar, iViewInvalidater);
                    break;
                case 5:
                    iSmartcard = new NormalSmartcardPersonalizedItem(context, nVar, asVar, iViewInvalidater);
                    break;
                case 6:
                    iSmartcard = new NormalSmartCardSelfUpdateItem(context, nVar, asVar, iViewInvalidater);
                    break;
                case 7:
                    iSmartcard = new NormalSmartcardPersonalizedItem(context, nVar, asVar, iViewInvalidater);
                    break;
                case 8:
                    iSmartcard = new NormalSmartcardPersonalizedItem(context, nVar, asVar, iViewInvalidater);
                    break;
                case 9:
                case 10:
                case 11:
                case 12:
                case 13:
                case 14:
                case 15:
                case ISystemOptimize.T_getMemoryUsageAsync /*27*/:
                    if (nVar instanceof com.tencent.assistant.smartcard.d.z) {
                        switch (((com.tencent.assistant.smartcard.d.z) nVar).c()) {
                            case 1:
                                normalSmartCardGameFriendHorizontalItem = new NormalSmartCardAppVerticalItem(context, nVar, asVar, iViewInvalidater);
                                break;
                            case 2:
                                normalSmartCardGameFriendHorizontalItem = new SmartSquareCard(context, nVar, asVar, iViewInvalidater);
                                break;
                            case 3:
                                normalSmartCardGameFriendHorizontalItem = new SmartHorizontalCard(context, nVar, asVar, iViewInvalidater);
                                break;
                            case 4:
                            default:
                                normalSmartCardGameFriendHorizontalItem = null;
                                break;
                            case 5:
                                normalSmartCardGameFriendHorizontalItem = new SmartSquareCardWithUser(context, nVar, asVar, iViewInvalidater);
                                break;
                            case 6:
                                normalSmartCardGameFriendHorizontalItem = new NormalSmartCardGameFriendHorizontalItem(context, nVar, asVar, iViewInvalidater);
                                break;
                        }
                        iSmartcard = normalSmartCardGameFriendHorizontalItem;
                        break;
                    }
                    break;
                case 16:
                    iSmartcard = new NormalSmartcardBookingItem(context, nVar, asVar, iViewInvalidater);
                    break;
                case 17:
                    iSmartcard = new NormalSmartCardQianghaoItem(context, nVar, asVar, iViewInvalidater);
                    break;
                case ISystemOptimize.T_killTaskAsync /*22*/:
                    iSmartcard = new NormalSmartCardHotwordsItem(context, nVar, asVar, iViewInvalidater);
                    break;
                case 23:
                    iSmartcard = new NormalSmartCardPlayerSHowItem(context, nVar, asVar, iViewInvalidater);
                    break;
                case ISystemOptimize.T_checkVersionAsync /*24*/:
                    iSmartcard = new NormalSmartcardCpaAdvertiseItem(context, nVar, asVar, iViewInvalidater);
                    break;
                case STConst.ST_PAGE_COMPETITIVE /*2001*/:
                    iSmartcard = new SearchSmartCardTopicItem(context, nVar, asVar, zVar);
                    ((SearchSmartCardTopicItem) iSmartcard).a(zVar.c());
                    break;
                case STConst.ST_PAGE_NECESSARY /*2002*/:
                    iSmartcard = new SearchSmartCardAppListItem(context, nVar, asVar, zVar);
                    break;
                case STConst.ST_PAGE_HOT /*2003*/:
                    iSmartcard = new SearchSmartCardVideoItem(context, nVar, asVar, zVar);
                    break;
                case STConst.ST_PAGE_SPECIAL /*2004*/:
                    iSmartcard = new SearchSmartCardFengyunItem(context, nVar, asVar, zVar);
                    break;
                case 2005:
                    iSmartcard = new SearchSmartCardVideoRelateItem(context, nVar, asVar, zVar);
                    break;
                case 2006:
                    iSmartcard = new SearchSmartCardEbookRelateItem(context, nVar, asVar, zVar);
                    break;
                case STConst.ST_PAGE_SEARCH /*2007*/:
                    iSmartcard = new SearchSmartCardYuyiTagItem(context, nVar, asVar, zVar);
                    break;
                default:
                    c a2 = b.a(nVar.j);
                    if (a2 != null) {
                        iSmartcard = a2.a(context, nVar, asVar, iViewInvalidater);
                        break;
                    }
                    break;
            }
            a(nVar, iSmartcard, i, zVar);
            if (iSmartcard == null) {
                Log.e("SmartCard", "smart view is NULL!!!.smart card model:" + nVar);
            }
        }
        return iSmartcard;
    }

    public ISmartcard a(ISmartcard iSmartcard, Context context, n nVar, as asVar, int i, z zVar, IViewInvalidater iViewInvalidater) {
        if (iSmartcard == null || nVar == null || zVar == null) {
            return null;
        }
        if (iSmartcard.d.j == nVar.j) {
            iSmartcard.a(nVar);
        } else {
            iSmartcard = a(context, nVar, asVar, i, zVar, iViewInvalidater);
        }
        a(nVar, iSmartcard, i, zVar);
        return iSmartcard;
    }

    public void a(n nVar, ISmartcard iSmartcard, int i, z zVar) {
        if (nVar != null && iSmartcard != null && zVar != null) {
            ar arVar = new ar(zVar.b(), nVar.j, nVar.k);
            if (!this.f1717a.containsKey(arVar) || this.f1717a.get(arVar).intValue() != i) {
                iSmartcard.f = false;
                this.f1717a.put(arVar, Integer.valueOf(i));
            }
        }
    }
}
