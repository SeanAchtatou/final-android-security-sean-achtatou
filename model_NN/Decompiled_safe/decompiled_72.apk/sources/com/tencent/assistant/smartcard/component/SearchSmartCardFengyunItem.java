package com.tencent.assistant.smartcard.component;

import android.content.Context;
import android.graphics.Canvas;
import android.text.Html;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.model.d;
import com.tencent.assistant.smartcard.d.f;
import com.tencent.assistant.smartcard.d.n;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.bm;
import com.tencent.assistant.utils.by;
import com.tencent.assistantv2.adapter.smartlist.z;
import com.tencent.assistantv2.component.DownloadButton;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import com.tencent.connect.common.Constants;
import com.tencent.pangu.component.appdetail.TXDwonloadProcessBar;
import com.tencent.pangu.component.appdetail.process.s;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class SearchSmartCardFengyunItem extends SearchSmartCardBaseItem {
    private View l;
    private TextView m;
    private TextView n;
    private ImageView o;
    private LinearLayout p;
    private z q;

    public SearchSmartCardFengyunItem(Context context) {
        super(context);
    }

    public SearchSmartCardFengyunItem(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public SearchSmartCardFengyunItem(Context context, n nVar, as asVar, z zVar) {
        super(context, nVar, asVar);
        this.q = zVar;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (!this.f) {
            this.f = true;
            h();
        }
    }

    /* access modifiers changed from: protected */
    public void a() {
        this.c = this.b.inflate((int) R.layout.smartcard_fengyun, this);
        this.l = findViewById(R.id.title_ly);
        this.m = (TextView) findViewById(R.id.title);
        this.n = (TextView) findViewById(R.id.desc);
        this.o = (ImageView) findViewById(R.id.divider);
        this.p = (LinearLayout) findViewById(R.id.app_list);
        i();
    }

    /* access modifiers changed from: protected */
    public void b() {
        i();
    }

    private void i() {
        this.p.removeAllViews();
        f fVar = (f) this.d;
        if (fVar == null || fVar.f1753a <= 0 || fVar.b == null || fVar.b.size() == 0 || fVar.b.size() < fVar.f1753a) {
            a(8);
            setBackgroundResource(17170445);
            setPadding(0, 0, 0, 0);
            return;
        }
        setBackgroundResource(R.drawable.bg_card_selector_padding);
        a(0);
        ArrayList arrayList = new ArrayList(fVar.b);
        this.m.setText(Html.fromHtml(fVar.l));
        int size = arrayList.size() > fVar.f1753a ? fVar.f1753a : arrayList.size();
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) this.o.getLayoutParams();
        if (TextUtils.isEmpty(fVar.n)) {
            layoutParams.setMargins(0, by.b(14.0f), 0, 0);
            this.n.setVisibility(8);
        }
        this.p.addView(a(arrayList.subList(0, size)));
        if (fVar.b.size() > fVar.f1753a && !TextUtils.isEmpty(fVar.p) && !TextUtils.isEmpty(fVar.o)) {
            View a2 = a(fVar.p);
            this.p.addView(a2, new LinearLayout.LayoutParams(-1, by.b(40.0f)));
            a2.setOnClickListener(this.k);
        }
    }

    public void a(int i) {
        this.c.setVisibility(i);
        this.l.setVisibility(i);
        this.p.setVisibility(i);
    }

    private View a(List<SimpleAppModel> list) {
        LinearLayout linearLayout = new LinearLayout(this.f1693a);
        linearLayout.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        linearLayout.setOrientation(0);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -1);
        layoutParams.weight = 1.0f;
        for (int i = 0; i < list.size(); i++) {
            View inflate = this.b.inflate((int) R.layout.smartcard_app_item, (ViewGroup) null);
            SimpleAppModel simpleAppModel = list.get(i);
            ((TXImageView) inflate.findViewById(R.id.icon)).updateImageView(simpleAppModel.e, R.drawable.pic_defaule, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
            TextView textView = (TextView) inflate.findViewById(R.id.name);
            textView.setText(simpleAppModel.d);
            TXDwonloadProcessBar tXDwonloadProcessBar = (TXDwonloadProcessBar) inflate.findViewById(R.id.progress);
            tXDwonloadProcessBar.a(simpleAppModel, textView);
            DownloadButton downloadButton = (DownloadButton) inflate.findViewById(R.id.btn);
            downloadButton.a(simpleAppModel);
            downloadButton.setTag(R.id.tma_st_smartcard_tag, STConst.ST_PAGE_TYPE_SMARTCARD);
            if (s.a(simpleAppModel)) {
                downloadButton.setClickable(false);
            } else {
                downloadButton.setClickable(true);
                STInfoV2 a2 = a(d(i), 200);
                if (a2 != null) {
                    a2.scene = b(0);
                    a2.searchId = this.i;
                    a2.extraData = c(0);
                    a2.updateWithSimpleAppModel(simpleAppModel);
                }
                downloadButton.a(a2, new ab(this), (d) null, downloadButton, tXDwonloadProcessBar);
            }
            inflate.setTag(R.id.tma_st_smartcard_tag, STConst.ST_PAGE_TYPE_SMARTCARD);
            inflate.setOnClickListener(new ac(this, simpleAppModel, i));
            linearLayout.addView(inflate, layoutParams);
        }
        return linearLayout;
    }

    private View a(String str) {
        View inflate = this.b.inflate((int) R.layout.smartcard_list_footer, (ViewGroup) null);
        ((TextView) inflate.findViewById(R.id.text)).setText(str);
        ImageView imageView = (ImageView) inflate.findViewById(R.id.icon);
        imageView.setImageResource(R.drawable.go);
        imageView.setVisibility(8);
        return inflate;
    }

    /* access modifiers changed from: private */
    public String d(int i) {
        return c() + bm.a(i + 1);
    }

    /* access modifiers changed from: protected */
    public String c() {
        return a.a(Constants.VIA_REPORT_TYPE_SHARE_TO_QQ, this.q == null ? 0 : this.q.a());
    }

    /* access modifiers changed from: protected */
    public int d() {
        return com.tencent.assistantv2.st.page.d.f2062a;
    }

    /* access modifiers changed from: protected */
    public String e() {
        if (this.q == null) {
            return null;
        }
        return this.q.d();
    }

    /* access modifiers changed from: protected */
    public long f() {
        if (this.q == null) {
            return 0;
        }
        return this.q.c();
    }

    /* access modifiers changed from: protected */
    public int g() {
        if (this.q == null) {
            return 2000;
        }
        return this.q.b();
    }
}
