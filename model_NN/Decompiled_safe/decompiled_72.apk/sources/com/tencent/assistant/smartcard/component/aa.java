package com.tencent.assistant.smartcard.component;

import android.content.Context;
import android.view.View;
import com.tencent.assistant.a.a;
import com.tencent.assistant.component.listener.OnTMAParamExClickListener;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistant.smartcard.d.e;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;
import com.tencent.pangu.link.b;

/* compiled from: ProGuard */
class aa extends OnTMAParamExClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SearchSmartCardEbookRelateItem f1702a;
    private Context b;
    private e c;

    public aa(SearchSmartCardEbookRelateItem searchSmartCardEbookRelateItem, Context context, e eVar) {
        this.f1702a = searchSmartCardEbookRelateItem;
        this.b = context;
        this.c = eVar;
    }

    public void onTMAClick(View view) {
        String str;
        if (this.c != null) {
            if (this.f1702a.p != null) {
                str = "tpmast://search?" + PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME + "=" + this.f1702a.p.b();
            } else {
                str = "tpmast://search?" + PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME + "=" + 2000;
            }
            b.a(this.f1702a.getContext(), str + "&" + a.ab + "=" + 3);
        }
    }

    public STInfoV2 getStInfo(View view) {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f1702a.getContext(), 200);
        if (this.f1702a.p != null) {
            buildSTInfo.slotId = com.tencent.assistantv2.st.page.a.a(Constants.VIA_REPORT_TYPE_SHARE_TO_QZONE, this.f1702a.p.a());
        } else {
            buildSTInfo.slotId = com.tencent.assistantv2.st.page.a.a(Constants.VIA_REPORT_TYPE_SHARE_TO_QZONE, 0);
        }
        if (this.f1702a.p != null) {
            buildSTInfo.extraData = this.f1702a.p.d();
        }
        buildSTInfo.status = "01";
        return buildSTInfo;
    }
}
