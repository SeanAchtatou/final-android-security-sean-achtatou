package com.tencent.assistant.smartcard.component;

import android.os.Bundle;
import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamExClickListener;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistant.protocol.jce.SmartCardLibaoItem;
import com.tencent.assistant.st.STConstAction;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.pangu.link.b;

/* compiled from: ProGuard */
class r extends OnTMAParamExClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SmartCardLibaoItem f1735a;
    final /* synthetic */ STInfoV2 b;
    final /* synthetic */ NormalSmartcardLibaoItem c;

    r(NormalSmartcardLibaoItem normalSmartcardLibaoItem, SmartCardLibaoItem smartCardLibaoItem, STInfoV2 sTInfoV2) {
        this.c = normalSmartcardLibaoItem;
        this.f1735a = smartCardLibaoItem;
        this.b = sTInfoV2;
    }

    public void onTMAClick(View view) {
        Bundle bundle = new Bundle();
        bundle.putInt(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, this.c.c(this.c.a(this.c.f1693a)));
        b.b(this.c.f1693a, this.f1735a.d, bundle);
    }

    public STInfoV2 getStInfo(View view) {
        if (this.b != null) {
            if (view.getId() == 11112222) {
                this.b.actionId = STConstAction.ACTION_HIT_GIF_GET;
            } else {
                this.b.actionId = 200;
            }
        }
        return this.b;
    }
}
