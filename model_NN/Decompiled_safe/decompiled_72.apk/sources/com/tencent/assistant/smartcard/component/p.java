package com.tencent.assistant.smartcard.component;

import android.os.Bundle;
import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.pangu.link.b;

/* compiled from: ProGuard */
class p extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f1733a;
    final /* synthetic */ STInfoV2 b;
    final /* synthetic */ NormalSmartcardBookingItem c;

    p(NormalSmartcardBookingItem normalSmartcardBookingItem, String str, STInfoV2 sTInfoV2) {
        this.c = normalSmartcardBookingItem;
        this.f1733a = str;
        this.b = sTInfoV2;
    }

    public void onTMAClick(View view) {
        Bundle bundle = new Bundle();
        bundle.putInt(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, this.c.c(this.c.a(this.c.f1693a)));
        b.b(this.c.getContext(), this.f1733a, bundle);
    }

    public STInfoV2 getStInfo() {
        return this.b;
    }
}
