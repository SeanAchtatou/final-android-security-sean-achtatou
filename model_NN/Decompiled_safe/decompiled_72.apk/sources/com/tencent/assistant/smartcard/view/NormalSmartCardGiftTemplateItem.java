package com.tencent.assistant.smartcard.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.invalidater.IViewInvalidater;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.smartcard.component.NormalSmartcardBaseItem;
import com.tencent.assistant.smartcard.component.as;
import com.tencent.assistant.smartcard.d.i;
import com.tencent.assistant.smartcard.d.j;
import com.tencent.assistant.smartcard.d.n;
import com.tencent.assistantv2.st.b.e;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
public class NormalSmartCardGiftTemplateItem extends NormalSmartcardBaseItem {
    private NormalSmartCardHeader i;
    private LinearLayout l;
    private j m;

    public NormalSmartCardGiftTemplateItem(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public NormalSmartCardGiftTemplateItem(Context context, n nVar, as asVar, IViewInvalidater iViewInvalidater) {
        super(context, nVar, asVar, iViewInvalidater);
    }

    public NormalSmartCardGiftTemplateItem(Context context) {
        super(context);
    }

    /* access modifiers changed from: protected */
    public void a() {
        LayoutInflater.from(this.f1693a).inflate((int) R.layout.smartcard_gift_template_layout, this);
        this.i = (NormalSmartCardHeader) findViewById(R.id.smartcard_header);
        this.l = (LinearLayout) findViewById(R.id.node_area);
        this.m = (j) this.d;
        f();
    }

    /* access modifiers changed from: protected */
    public void b() {
        this.m = (j) this.d;
        f();
    }

    private void f() {
        boolean z;
        this.i.a(this.m.e, this.k);
        int childCount = this.l.getChildCount() - this.m.f.size();
        if (childCount > 0) {
            int i2 = 0;
            while (i2 < childCount) {
                try {
                    this.l.removeViewAt(i2);
                    i2++;
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }
        } else if (childCount < 0) {
            for (int i3 = 0; i3 < (-childCount); i3++) {
                this.l.addView(new NormalSmartCardGiftNode(this.f1693a));
            }
        }
        STInfoV2 sTInfoV2 = null;
        int i4 = 0;
        while (i4 < this.l.getChildCount() && this.m.f != null && i4 < this.m.f.size()) {
            NormalSmartCardGiftNode normalSmartCardGiftNode = (NormalSmartCardGiftNode) this.l.getChildAt(i4);
            if (this.m.f.get(i4) != null) {
                sTInfoV2 = a(i4, this.m.f.get(i4).f1755a);
            }
            i iVar = this.m.f.get(i4);
            if (i4 == this.l.getChildCount() - 1) {
                z = true;
            } else {
                z = false;
            }
            normalSmartCardGiftNode.a(iVar, sTInfoV2, z);
            i4++;
        }
    }

    private STInfoV2 a(int i2, SimpleAppModel simpleAppModel) {
        STInfoV2 a2 = a(a(i2), 100);
        if (!(a2 == null || simpleAppModel == null)) {
            a2.updateWithSimpleAppModel(simpleAppModel);
        }
        if (this.h == null) {
            this.h = new e();
        }
        this.h.a(a2);
        return a2;
    }

    private String a(int i2) {
        return "04_" + (i2 + 1);
    }

    /* access modifiers changed from: protected */
    public String d(int i2) {
        if (this.m != null) {
            return this.m.i;
        }
        return super.d(i2);
    }
}
