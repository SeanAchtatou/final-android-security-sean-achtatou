package com.tencent.assistant.smartcard.component;

import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.pangu.link.b;

/* compiled from: ProGuard */
class an extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f1715a;
    final /* synthetic */ SmartSquareCard b;

    an(SmartSquareCard smartSquareCard, String str) {
        this.b = smartSquareCard;
        this.f1715a = str;
    }

    public void onTMAClick(View view) {
        b.b(this.b.getContext(), this.f1715a);
    }

    public STInfoV2 getStInfo() {
        return this.b.a("03_001", 200);
    }
}
