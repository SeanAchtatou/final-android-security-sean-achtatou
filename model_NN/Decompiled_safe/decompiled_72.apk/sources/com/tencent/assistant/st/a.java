package com.tencent.assistant.st;

import com.qq.AppService.AppService;
import com.tencent.assistant.db.table.z;
import com.tencent.assistant.protocol.jce.StatConnectPC;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.an;
import com.tencent.assistantv2.st.business.BaseSTManagerV2;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class a extends BaseSTManagerV2 {

    /* renamed from: a  reason: collision with root package name */
    private static String f1780a = a.class.getSimpleName();
    private static a c = null;
    private ArrayList<StatConnectPC> d;
    private byte e;
    private byte f;
    private short g;
    private short h;
    private long i;
    private long j;
    private long k;
    private long l;
    private long m;
    private long n;
    private byte o;
    private z p;

    public static synchronized a a() {
        a aVar;
        synchronized (a.class) {
            if (c == null) {
                c = new a();
            }
            aVar = c;
        }
        return aVar;
    }

    private a() {
        this.d = null;
        this.p = null;
        this.d = new ArrayList<>();
        this.p = z.a();
    }

    public byte getSTType() {
        return 7;
    }

    public void flush() {
        e();
    }

    private void e() {
        ArrayList arrayList = new ArrayList();
        int size = this.d.size();
        for (int i2 = 0; i2 < size; i2++) {
            arrayList.add(an.a(this.d.get(i2)));
        }
        this.p.a(getSTType(), arrayList);
        this.d.clear();
    }

    private void f() {
        this.i = System.currentTimeMillis();
    }

    private void g() {
        this.j = System.currentTimeMillis();
    }

    private void h() {
        this.m = System.currentTimeMillis();
    }

    private void i() {
        this.n = System.currentTimeMillis();
    }

    public void a(short s) {
        this.g = s;
        this.h = 0;
    }

    public void b() {
        this.k = System.currentTimeMillis();
    }

    public void c() {
        this.l = System.currentTimeMillis();
    }

    public void a(byte b) {
        this.e = b;
        f();
    }

    public void b(byte b) {
        this.f = b;
        if (b == 0) {
            g();
            h();
            boolean s = AppService.s();
            boolean t = AppService.t();
            if (s) {
                this.e = 1;
            }
            if (t && (this.e == 1 || this.e == 0)) {
                this.e = 4;
            }
        }
        if (b <= 1) {
            return;
        }
        if (b == 8) {
            this.e = 4;
            j();
            e();
        } else if (this.e == 0) {
            k();
        } else {
            j();
            e();
        }
    }

    public void c(byte b) {
        if (this.m != 0) {
            i();
            this.o = b;
            j();
            e();
        }
    }

    private void j() {
        StatConnectPC statConnectPC = new StatConnectPC();
        statConnectPC.f1546a = STConst.ST_PAGE_CONNECT_PC;
        statConnectPC.b = this.e;
        statConnectPC.c = this.f;
        statConnectPC.d = this.g;
        statConnectPC.e = this.h;
        statConnectPC.f = System.currentTimeMillis();
        if (!(this.i == 0 || this.j == 0)) {
            statConnectPC.g = this.j - this.i;
        }
        if (!(this.k == 0 || this.l == 0)) {
            statConnectPC.j = this.l - this.k;
        }
        if (!(this.m == 0 || this.n == 0)) {
            statConnectPC.h = this.n - this.m;
        }
        statConnectPC.i = this.o;
        this.d.add(statConnectPC);
        XLog.d(f1780a, statConnectPC.toString());
        k();
    }

    private void k() {
        this.f = 0;
        this.e = 0;
        this.g = 0;
        this.h = 0;
        this.i = 0;
        this.j = 0;
        this.k = 0;
        this.l = 0;
        this.m = 0;
        this.n = 0;
        this.o = 0;
    }
}
