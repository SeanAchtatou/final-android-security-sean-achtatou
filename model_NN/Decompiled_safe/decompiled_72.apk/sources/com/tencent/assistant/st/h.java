package com.tencent.assistant.st;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.m;
import com.tencent.assistant.net.c;
import com.tencent.assistant.protocol.l;
import com.tencent.assistant.utils.r;
import java.nio.ByteBuffer;
import java.util.List;
import org.apache.http.util.ByteArrayBuffer;

/* compiled from: ProGuard */
public class h {
    public static final byte[] a(List<byte[]> list) {
        try {
            int i = 0;
            for (byte[] length : list) {
                i = length.length + i;
            }
            ByteBuffer allocate = ByteBuffer.allocate((list.size() * 4) + i);
            for (byte[] next : list) {
                allocate.putInt(next.length);
                allocate.put(next);
            }
            return allocate.array();
        } catch (Throwable th) {
            th.printStackTrace();
            return null;
        }
    }

    public static byte[] a(JceStruct jceStruct) {
        byte[] b = l.b(jceStruct);
        byte[] a2 = r.a(b.length);
        ByteArrayBuffer byteArrayBuffer = new ByteArrayBuffer(b.length + 4);
        byteArrayBuffer.append(a2, 0, a2.length);
        byteArrayBuffer.append(b, 0, b.length);
        return byteArrayBuffer.buffer();
    }

    public static final long a() {
        return System.currentTimeMillis() + m.a().A();
    }

    public static final boolean a(byte b) {
        if (b == 1) {
            return true;
        }
        return false;
    }

    public static boolean a(int i) {
        if (c.f()) {
            if ((i & 1) == 1) {
                return true;
            }
            return false;
        } else if (c.g()) {
            if ((i & 2) != 2) {
                return false;
            }
            return true;
        } else if (!c.e() || (i & 4) == 4) {
            return true;
        } else {
            return false;
        }
    }

    public static String a(String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, String str9) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(str).append("_").append(str2).append("_").append(str3).append("_").append(str4).append("_").append(str5).append("_").append(str6).append("_").append(str7).append("_").append(str8).append("_").append(str9);
        return stringBuffer.toString();
    }
}
