package com.tencent.assistant.kapalaiadapter.a;

import android.content.Context;
import android.telephony.TelephonyManager;
import com.tencent.assistant.kapalaiadapter.g;

/* compiled from: ProGuard */
public class d implements j {

    /* renamed from: a  reason: collision with root package name */
    private Object f783a = null;

    public Object a(int i, Context context) {
        if (this.f783a == null) {
            try {
                this.f783a = g.a("android.telephony.MSimTelephonyManager", "getDefault", (Object[]) null, (Class<?>[]) null);
            } catch (Exception e) {
            }
        }
        return this.f783a;
    }

    public String b(int i, Context context) {
        try {
            return (String) g.a(a(i, context), "getSubscriberId", new Object[]{Integer.valueOf(i)});
        } catch (Exception e) {
            return null;
        }
    }

    public String c(int i, Context context) {
        return ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
    }
}
