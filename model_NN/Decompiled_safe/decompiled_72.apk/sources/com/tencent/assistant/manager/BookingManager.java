package com.tencent.assistant.manager;

import com.qq.AppService.AstApp;
import com.tencent.assistant.event.EventDispatcher;
import com.tencent.assistant.event.EventDispatcherEnum;
import java.util.HashMap;

/* compiled from: ProGuard */
public class BookingManager {

    /* renamed from: a  reason: collision with root package name */
    private static BookingManager f849a;
    private EventDispatcher b = AstApp.i().j();
    private HashMap<Long, Integer> c = new HashMap<>();
    private HashMap<Long, Long> d = new HashMap<>();

    /* compiled from: ProGuard */
    public enum BOOKINGSTATUS {
        UNBOOKED,
        BOOKED,
        UNKOWN
    }

    public static synchronized BookingManager a() {
        BookingManager bookingManager;
        synchronized (BookingManager.class) {
            if (f849a == null) {
                f849a = new BookingManager();
            }
            bookingManager = f849a;
        }
        return bookingManager;
    }

    public long a(long j) {
        if (this.d.containsKey(Long.valueOf(j))) {
            return this.d.get(Long.valueOf(j)).longValue();
        }
        return -1;
    }

    public BOOKINGSTATUS b(long j) {
        if (this.d.containsKey(Long.valueOf(j))) {
            long longValue = this.d.get(Long.valueOf(j)).longValue();
            if (this.c.containsKey(Long.valueOf(longValue))) {
                return BOOKINGSTATUS.values()[this.c.get(Long.valueOf(longValue)).intValue()];
            }
        }
        return BOOKINGSTATUS.UNBOOKED;
    }

    public void a(long j, long j2) {
        this.d.put(Long.valueOf(j), Long.valueOf(j2));
    }

    public void a(long j, int i) {
        this.c.put(Long.valueOf(j), Integer.valueOf(i));
        this.b.sendMessage(this.b.obtainMessage(EventDispatcherEnum.UI_EVENT_BOOKING_STATUS_CHANGED, Long.valueOf(j)));
    }
}
