package com.tencent.assistant.manager.webview.js;

import android.net.Uri;
import android.text.TextUtils;
import com.tencent.assistant.Global;
import com.tencent.assistant.m;
import com.tencent.assistant.manager.g;
import com.tencent.assistant.manager.h;
import com.tencent.open.SocialConstants;
import com.tencent.open.utils.ServerSetting;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

/* compiled from: ProGuard */
public class a implements h {

    /* renamed from: a  reason: collision with root package name */
    private static final b[] f922a = {new b(1, "gift.app.qq.com", Long.MAX_VALUE), new b(1, "maweb.3g.qq.com", Long.MAX_VALUE), new b(1, ServerSetting.KEY_HOST_QZS_QQ, Long.MAX_VALUE), new b(1, "mq.wsq.qq.com", Long.MAX_VALUE), new b(1, "m.wsq.qq.com", Long.MAX_VALUE), new b(1, "appicsh.qq.com", Long.MAX_VALUE), new b(1, "*.kf0309.3g.qq.com", Long.MAX_VALUE), new b(1, "dev-qzs.yybv.qq.com", Long.MAX_VALUE), new b(1, "pre-qzs.yybv.qq.com", Long.MAX_VALUE), new b(1, "test-qzs.yybv.qq.com", Long.MAX_VALUE), new b(1, "m.tv.sohu.com", Long.MAX_VALUE), new b(1, "test.qzs.qq.com", Long.MAX_VALUE), new b(1, "appicsh.qq.com", Long.MAX_VALUE), new b(1, "qtheme.cs0309.html5.qq.com", Long.MAX_VALUE), new b(1, "yyb.html5.qq.com", Long.MAX_VALUE), new b(1, "fusionbase.qq.com", Long.MAX_VALUE)};
    private static final b[] b = {new b(1, "*.kf0309.3g.qq.com", Long.MAX_VALUE), new b(1, "dev-qzs.yybv.qq.com", Long.MAX_VALUE), new b(1, "pre-qzs.yybv.qq.com", Long.MAX_VALUE), new b(1, "test-qzs.yybv.qq.com", Long.MAX_VALUE), new b(1, "test.qzs.qq.com", Long.MAX_VALUE), new b(1, "qtheme.cs0309.html5.qq.com", Long.MAX_VALUE), new b(1, "m.tv.sohu.com", 68719476736L)};
    private static a e;
    private static final Map<String, Integer> f = new HashMap();
    private static final Map<String, Integer> g = new HashMap();
    private List<b> c = new ArrayList();
    private final List<b> d = Collections.synchronizedList(new ArrayList());

    static {
        f.put("startDownload", 0);
        f.put("pauseDownload", 1);
        f.put("createDownload", 2);
        f.put("queryDownload", 3);
        f.put("getAppInfo", 4);
        f.put("startOpenApp", 5);
        f.put("getNetInfo", 6);
        f.put("getMobileInfo", 7);
        f.put("getPrivateMobileInfo", 8);
        f.put("setWebView", 9);
        f.put("closeWebView", 10);
        f.put("pageControl", 11);
        f.put("store", 12);
        f.put("getStoreByKey", 13);
        f.put("getAllStore", 14);
        f.put("gray", 15);
        f.put("loadByAnotherWebBrowser", 16);
        f.put("getVersion", 17);
        f.put("checkSelfUpdate", 18);
        f.put("getUserLoginToken", 19);
        f.put("openLoginActivity", 20);
        f.put("getStatTime", 21);
        f.put("refreshTicket", 22);
        f.put("toast", 23);
        f.put("showPics", 24);
        f.put("share", 25);
        f.put("openNewWindow", 26);
        f.put("getMid", 27);
        f.put("showGiftGameItem", 28);
        f.put("hideGiftGameItem", 29);
        f.put("showErrorPage", 30);
        f.put("report", 31);
        f.put("queryAppState", 32);
        f.put("sendHttpRequest", 33);
        f.put("clearPrompt", 34);
        f.put("clearNumEx", 35);
        f.put("saveData", 36);
        f.put("getData", 37);
        f.put("playVideoByApp", 38);
        f.put("playVideoByWebView", 39);
        f.put("playLocalVideo", 40);
        f.put("bookOp", 41);
        f.put("downloadVideo", 42);
        f.put("pauseDownloadVideo", 43);
        f.put("deleteVideo", 44);
        f.put("queryVideoDownloadState", 45);
        f.put("openQQReader", 46);
        f.put("getVideoDownloadInfo", 47);
        f.put(JsBridge.APP_INSTALL_UNINSTALL, 48);
        f.put("getBrowserSignature", 49);
        f.put("notifyBookingStateChange", 50);
        f.put("openQubeTheme", 51);
        f.put("deleteLocalTheme", 52);
        f.put("getAllLocalThemes", 53);
        f.put("obtainMissionFreeTime", 54);
        f.put("showFreewifiDialog", 55);
        f.put("setClipboard", 56);
        f.put("getClipboard", 57);
        f.put("webViewCompatibilityReport", 58);
        f.put("addAppLinkActionTask", 59);
        f.put("deleteDownload", 60);
        f.put("getLBSInfo", 61);
        f.put("getExternalCallTicketStatus", 62);
        g.put("toast", 0);
        g.put("clearNum", 0);
        g.put("scrollToTop", 0);
        g.put("queryAppState", 0);
        g.put("sendHttpRequest", 0);
        g.put(JsBridge.LOGIN_CALLBACK_FUNCTION_NAME, 0);
        g.put(JsBridge.STATE_CALLBACK_FUNCTION_NAME, 0);
        g.put("clearNum", 0);
        g.put(JsBridge.VIDEO_DOWNLOAD_STATE_CALLBACK, 0);
        g.put("deteleAppLinkActionTask", 0);
        g.put("getLBSData", 0);
        g.put(JsBridge.APP_INSTALLED_AND_ACTION_COMPLETE_FUNCTION, 0);
        g.put("hasGameDesktopShortCut", 0);
        g.put("addGameDesktopShortCut", 0);
    }

    public static synchronized a a() {
        a aVar;
        synchronized (a.class) {
            if (e == null) {
                e = new a();
                g.a().a(e);
            }
            aVar = e;
        }
        return aVar;
    }

    private a() {
        b(m.a().z());
    }

    private void b(String str) {
        ArrayList arrayList = new ArrayList();
        if (!TextUtils.isEmpty(str)) {
            try {
                JSONArray jSONArray = new JSONArray(str);
                int length = jSONArray.length();
                for (int i = 0; i < length; i++) {
                    JSONObject jSONObject = jSONArray.getJSONObject(i);
                    arrayList.add(new b(jSONObject.getInt(SocialConstants.PARAM_TYPE), jSONObject.getString(SocialConstants.PARAM_URL), jSONObject.getLong("mask")));
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        this.c = arrayList;
    }

    public void a(b bVar) {
        this.d.add(bVar);
    }

    public boolean a(String str, String str2) {
        if (str2.equals(JsBridge.IS_INTERFACE_READY_NAME) || g.containsKey(str2)) {
            return true;
        }
        long c2 = c(str);
        Integer num = f.get(str2);
        if (num == null) {
            return false;
        }
        return ((c2 >>> num.intValue()) & 1) == 1;
    }

    public boolean a(String str) {
        return c(str) != 0;
    }

    private long c(String str) {
        if (TextUtils.isEmpty(str)) {
            return 0;
        }
        String host = Uri.parse(str).getHost();
        if (TextUtils.isEmpty(host)) {
            return 0;
        }
        String lowerCase = host.toLowerCase();
        for (b next : this.d) {
            if (next.a(str, lowerCase)) {
                return next.b;
            }
        }
        for (b next2 : this.c) {
            if (next2.a(str, lowerCase)) {
                return next2.b;
            }
        }
        for (b bVar : f922a) {
            if (bVar.a(str, lowerCase)) {
                return bVar.b;
            }
        }
        if (Global.isDev()) {
            for (b bVar2 : b) {
                if (bVar2.a(str, lowerCase)) {
                    return bVar2.b;
                }
            }
        }
        return 0;
    }

    public void a(HashMap<String, Object> hashMap) {
        String str = (String) hashMap.get("key_webview_config_json");
        if (!TextUtils.isEmpty(str)) {
            b(str);
        }
    }

    public void b() {
    }
}
