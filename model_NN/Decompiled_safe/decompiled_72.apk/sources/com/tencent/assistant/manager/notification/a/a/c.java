package com.tencent.assistant.manager.notification.a.a;

import android.graphics.Bitmap;
import android.text.TextUtils;
import com.tencent.assistant.thumbnailCache.k;
import com.tencent.assistant.thumbnailCache.p;

/* compiled from: ProGuard */
public class c extends a {
    private int c = -1;
    private String d = null;
    private e e = null;
    private p f = new d(this);

    public c() {
    }

    public c(String str, int i) {
        this.c = i;
        this.d = str;
    }

    public void b(int i) {
        this.c = i;
    }

    public void a(String str) {
        this.d = str;
    }

    public synchronized void a(e eVar) {
        this.e = eVar;
    }

    public void b() {
        if (this.c == -1 || TextUtils.isEmpty(this.d)) {
            a(-3, null);
            return;
        }
        Bitmap a2 = k.b().a(this.d, this.c, this.f);
        if (a2 != null && !a2.isRecycled()) {
            a(0, a2);
        }
    }

    /* access modifiers changed from: protected */
    public synchronized void a(int i, Bitmap bitmap) {
        if (this.e != null) {
            this.e.a(bitmap);
        }
        super.a(i);
    }
}
