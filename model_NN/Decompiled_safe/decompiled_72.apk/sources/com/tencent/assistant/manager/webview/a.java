package com.tencent.assistant.manager.webview;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.ValueCallback;
import android.widget.FrameLayout;
import com.tencent.assistant.manager.webview.component.d;
import com.tencent.assistant.manager.webview.component.e;
import com.tencent.assistant.manager.webview.js.JsBridge;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.bm;
import com.tencent.assistant.utils.r;
import com.tencent.pangu.link.b;
import com.tencent.smtt.export.external.interfaces.ConsoleMessage;
import com.tencent.smtt.export.external.interfaces.GeolocationPermissionsCallback;
import com.tencent.smtt.export.external.interfaces.IX5WebChromeClient;
import com.tencent.smtt.sdk.WebChromeClient;
import com.tencent.smtt.sdk.WebView;
import java.lang.reflect.Method;

/* compiled from: ProGuard */
public class a extends WebChromeClient {
    private static final FrameLayout.LayoutParams i = new FrameLayout.LayoutParams(-1, -1);

    /* renamed from: a  reason: collision with root package name */
    protected d f906a = null;
    protected Activity b = null;
    private View c;
    private IX5WebChromeClient.CustomViewCallback d;
    private int e;
    private FrameLayout f;
    private FrameLayout g;
    private String h;
    private Context j = null;
    private JsBridge k = null;
    private e l = null;
    private ValueCallback<Uri> m;

    public a(Context context, JsBridge jsBridge, e eVar, d dVar) {
        this.j = context;
        this.k = jsBridge;
        this.l = eVar;
        this.f906a = dVar;
        this.b = dVar.a();
    }

    public void onProgressChanged(WebView webView, int i2) {
        if (this.f906a != null) {
            this.f906a.a(webView, i2);
        }
    }

    public void onGeolocationPermissionsShowPrompt(String str, GeolocationPermissionsCallback geolocationPermissionsCallback) {
        geolocationPermissionsCallback.invoke(str, true, false);
    }

    public void onReceivedTitle(WebView webView, String str) {
        if (webView != null) {
            this.h = webView.getUrl();
        }
        if (this.f906a != null) {
            this.f906a.a(webView, str);
        }
        if (webView != null) {
            try {
                Method method = webView.getClass().getMethod("removeJavascriptInterface", String.class);
                if (method != null) {
                    method.invoke(webView, "searchBoxJavaBridge_");
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    public void onShowCustomView(View view, IX5WebChromeClient.CustomViewCallback customViewCallback) {
        if (Build.VERSION.SDK_INT < 14) {
            return;
        }
        if (this.c != null) {
            customViewCallback.onCustomViewHidden();
            return;
        }
        this.e = this.b.getRequestedOrientation();
        this.g = new b(this.b);
        this.g.addView(view, i);
        ((FrameLayout) this.b.getWindow().getDecorView()).addView(this.g, i);
        this.c = view;
        a(true);
        this.d = customViewCallback;
    }

    public void onShowCustomView(View view, int i2, IX5WebChromeClient.CustomViewCallback customViewCallback) {
        if (Build.VERSION.SDK_INT < 14) {
            return;
        }
        if (this.c != null) {
            customViewCallback.onCustomViewHidden();
            return;
        }
        this.e = this.b.getRequestedOrientation();
        this.g = new b(this.b);
        this.g.addView(view, i);
        ((FrameLayout) this.b.getWindow().getDecorView()).addView(this.g, i);
        this.c = view;
        a(true);
        this.d = customViewCallback;
        this.b.setRequestedOrientation(i2);
    }

    public void onHideCustomView() {
        if (this.c != null) {
            a(false);
            ((FrameLayout) this.b.getWindow().getDecorView()).removeView(this.g);
            this.g = null;
            this.c = null;
            this.d.onCustomViewHidden();
            this.b.setRequestedOrientation(this.e);
        }
    }

    private void a(boolean z) {
        Window window = this.b.getWindow();
        WindowManager.LayoutParams attributes = window.getAttributes();
        if (z) {
            attributes.flags |= 1024;
        } else {
            attributes.flags &= -1025;
            if (this.c != null) {
                a(this.c);
            } else {
                a(this.f);
            }
        }
        window.setAttributes(attributes);
    }

    public void openFileChooser(ValueCallback<Uri> valueCallback, String str, String str2) {
        if (com.tencent.assistant.manager.webview.js.a.a().a(this.h)) {
            try {
                this.m = valueCallback;
                Intent intent = new Intent("android.intent.action.GET_CONTENT");
                intent.addCategory("android.intent.category.OPENABLE");
                intent.setType("*/*");
                this.b.startActivityForResult(Intent.createChooser(intent, "完成操作需要使用"), 100);
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    public ValueCallback<Uri> a() {
        return this.m;
    }

    public void b() {
        this.m = null;
    }

    private void a(View view) {
        if (r.d() >= 14) {
            try {
                view.getClass().getMethod("setSystemUiVisibility", Integer.TYPE).invoke(view, 0);
            } catch (Exception e2) {
            }
        }
    }

    public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
        XLog.i("Jie", "[onConsoleMessage] ---> 111");
        if (consoleMessage == null) {
            return false;
        }
        return a(consoleMessage.message());
    }

    private boolean a(String str) {
        boolean z = true;
        XLog.i("Jie", "handleConsoleMessage --- url = " + str);
        if (TextUtils.isEmpty(str)) {
            XLog.i("Jie", "Interface url is empty");
            return false;
        } else if (str.startsWith("http") || str.startsWith("https")) {
            return false;
        } else {
            if (str.startsWith(JsBridge.JS_BRIDGE_SCHEME)) {
                XLog.i("Jie", "Interface request:" + str);
                if (this.k != null) {
                    this.k.invoke(str);
                }
                return true;
            } else if (str.equals("about:blank;") || str.equals("about:blank")) {
                if (Build.VERSION.SDK_INT >= 11) {
                    z = false;
                }
                return z;
            } else {
                Uri parse = Uri.parse(str);
                Intent intent = new Intent("android.intent.action.VIEW", parse);
                if (!b.a(this.j, intent)) {
                    return false;
                }
                String scheme = intent.getScheme();
                if (scheme == null || !scheme.equals("tmast")) {
                    intent.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, this.l.a());
                    this.j.startActivity(intent);
                } else {
                    Bundle bundle = new Bundle();
                    int a2 = bm.a(parse.getQueryParameter("scene"), 0);
                    if (a2 != 0) {
                        bundle.putInt(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, a2);
                    } else {
                        bundle.putInt(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, this.l.a());
                    }
                    b.b(this.j, str, bundle);
                }
                return true;
            }
        }
    }
}
