package com.tencent.assistant.manager.notification.a;

import android.text.TextUtils;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.protocol.jce.PushInfo;

/* compiled from: ProGuard */
public class g extends e {
    public g(int i, PushInfo pushInfo, byte[] bArr) {
        super(i, pushInfo, bArr);
    }

    /* access modifiers changed from: protected */
    public boolean c() {
        if (super.c() && !TextUtils.isEmpty(this.c.c) && this.e != null && !TextUtils.isEmpty(this.e.f1125a)) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public boolean f() {
        b(R.layout.notification_card_1);
        if (this.i == null) {
            return false;
        }
        i();
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean g() {
        this.j = c(R.layout.notification_card1_right1);
        this.j.setOnClickPendingIntent(R.id.rightIconArea, k());
        this.i.removeAllViews(R.id.rightContainer);
        this.i.addView(R.id.rightContainer, this.j);
        this.i.setViewVisibility(R.id.rightContainer, 0);
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean h() {
        return true;
    }
}
