package com.tencent.assistant.b;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.telephony.CellLocation;
import android.telephony.NeighboringCellInfo;
import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;
import com.tencent.assistant.protocol.jce.LbsCell;
import com.tencent.assistant.protocol.jce.LbsData;
import com.tencent.assistant.protocol.jce.LbsWifiMac;
import com.tencent.assistant.utils.XLog;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* compiled from: ProGuard */
public class c {
    private static ArrayList<String> p = new ArrayList<>();

    /* renamed from: a  reason: collision with root package name */
    private Context f613a;
    private i b;
    private Handler c = null;
    private TelephonyManager d = null;
    private WifiManager e = null;
    private BroadcastReceiver f = null;
    /* access modifiers changed from: private */
    public boolean g = false;
    private int h;
    private int i;
    private int j;
    private int k;
    private ArrayList<NeighboringCellInfo> l = null;
    private ArrayList<LbsWifiMac> m = null;
    private Object n = new Object();
    private Runnable o = new d(this);

    static {
        p.add("GT-I9100");
    }

    public c(Context context, i iVar) {
        this.f613a = context;
        this.b = iVar;
        this.c = new Handler(Looper.getMainLooper());
        this.d = (TelephonyManager) context.getSystemService("phone");
        this.e = (WifiManager) context.getSystemService("wifi");
        j();
    }

    public synchronized boolean a() {
        j();
        b();
        if (l()) {
            f();
        }
        if (!m() || !g()) {
            k();
        }
        return true;
    }

    public synchronized void b() {
        if (this.g) {
            e();
        }
    }

    public LbsData c() {
        a aVar = new a();
        ArrayList arrayList = new ArrayList();
        arrayList.add(new LbsCell(this.h, this.i, this.j, this.k, 0));
        ArrayList arrayList2 = new ArrayList();
        if (this.l != null) {
            arrayList2.addAll(this.l);
        }
        Iterator it = arrayList2.iterator();
        while (it.hasNext()) {
            NeighboringCellInfo neighboringCellInfo = (NeighboringCellInfo) it.next();
            arrayList.add(new LbsCell(this.h, this.i, this.j, neighboringCellInfo.getCid(), neighboringCellInfo.getRssi()));
        }
        aVar.a(arrayList);
        ArrayList arrayList3 = new ArrayList();
        if (this.m != null) {
            arrayList3.addAll(this.m);
        }
        aVar.b(arrayList3);
        return aVar.a();
    }

    public void d() {
        b();
    }

    /* access modifiers changed from: private */
    public boolean e() {
        synchronized (this.n) {
            if (this.g) {
                if (this.f != null) {
                    try {
                        XLog.d("LBS", "unregister mWifiReceiver ");
                        this.f613a.unregisterReceiver(this.f);
                        this.c.removeCallbacks(this.o);
                    } catch (Exception e2) {
                    }
                }
            }
        }
        return true;
    }

    private void f() {
        XLog.d("LBS", "request Cell location ");
        switch (this.d.getPhoneType()) {
            case 1:
                try {
                    GsmCellLocation gsmCellLocation = (GsmCellLocation) this.d.getCellLocation();
                    if (gsmCellLocation != null) {
                        this.j = gsmCellLocation.getLac();
                        this.k = gsmCellLocation.getCid();
                        if (this.j > 0 || this.k > 0) {
                            String networkOperator = this.d.getNetworkOperator();
                            if (networkOperator != null) {
                                try {
                                    this.h = Integer.parseInt(networkOperator.substring(0, 3));
                                    this.i = Integer.parseInt(networkOperator.substring(3));
                                } catch (Exception e2) {
                                    this.h = 0;
                                    this.i = 0;
                                    this.j = 0;
                                    this.k = 0;
                                    return;
                                }
                            }
                            List neighboringCellInfo = this.d.getNeighboringCellInfo();
                            if (neighboringCellInfo != null) {
                                this.l.addAll(neighboringCellInfo);
                                return;
                            }
                            return;
                        }
                        this.j = 0;
                        this.k = 0;
                        return;
                    }
                    return;
                } catch (Exception e3) {
                    return;
                }
            case 2:
                try {
                    Class<?> cls = Class.forName("android.telephony.cdma.CdmaCellLocation");
                    if (cls != null) {
                        cls.getConstructor(null);
                        CellLocation cellLocation = this.d.getCellLocation();
                        Method method = cls.getMethod("getSystemId", new Class[0]);
                        if (method != null) {
                            this.i = ((Integer) method.invoke(cellLocation, null)).intValue();
                        }
                        Method method2 = cls.getMethod("getNetworkId", new Class[0]);
                        if (method2 != null) {
                            this.j = ((Integer) method2.invoke(cellLocation, null)).intValue();
                        }
                        Method method3 = cls.getMethod("getBaseStationId", new Class[0]);
                        if (method3 != null) {
                            this.k = ((Integer) method3.invoke(cellLocation, null)).intValue();
                        }
                        String networkOperator2 = this.d.getNetworkOperator();
                        if (networkOperator2 != null) {
                            try {
                                this.h = Integer.parseInt(networkOperator2.substring(0, 3));
                                return;
                            } catch (Exception e4) {
                                return;
                            }
                        } else {
                            return;
                        }
                    } else {
                        return;
                    }
                } catch (Exception e5) {
                    e5.printStackTrace();
                    return;
                }
            default:
                return;
        }
    }

    private boolean g() {
        XLog.d("LBS", "request wifi location ");
        synchronized (this.n) {
            if (this.g) {
                return true;
            }
            try {
                h();
                if (this.m.size() == 0) {
                    if (this.f == null) {
                        this.f = new e(this);
                    }
                    IntentFilter intentFilter = new IntentFilter();
                    intentFilter.addAction("android.net.wifi.SCAN_RESULTS");
                    this.f613a.registerReceiver(this.f, intentFilter);
                    this.g = this.e.startScan();
                    this.c.postDelayed(this.o, 3000);
                } else {
                    this.g = false;
                    k();
                }
                return true;
            } catch (Exception e2) {
                return false;
            }
        }
    }

    private void h() {
        List<ScanResult> scanResults = this.e.getScanResults();
        if (scanResults != null) {
            for (ScanResult next : scanResults) {
                this.m.add(new LbsWifiMac(next.BSSID, next.level));
            }
        }
    }

    /* access modifiers changed from: private */
    public void i() {
        if (this.g) {
            try {
                h();
                this.g = false;
            } catch (Exception e2) {
                this.g = false;
            } catch (Throwable th) {
                this.g = false;
                k();
                throw th;
            }
            k();
        }
    }

    private void j() {
        this.h = 0;
        this.i = 0;
        this.j = 0;
        this.k = 0;
        if (this.l == null) {
            this.l = new ArrayList<>();
        } else {
            this.l.clear();
        }
        if (this.m == null) {
            this.m = new ArrayList<>();
        } else {
            this.m.clear();
        }
    }

    /* access modifiers changed from: private */
    public void k() {
        int i2 = 0;
        if (this.k != 0) {
            i2 = 1;
        }
        if (this.m.size() > 0) {
            i2 |= 2;
        }
        XLog.d("LBS", "result " + i2);
        this.b.a(i2);
        synchronized (this.n) {
            this.c.removeCallbacks(this.o);
        }
    }

    private boolean l() {
        int i2;
        boolean z = false;
        synchronized (this.n) {
            if (this.d != null) {
                try {
                    i2 = Settings.System.getInt(this.f613a.getContentResolver(), "airplane_mode_on", 0);
                } catch (Exception e2) {
                    i2 = 0;
                }
                if (i2 == 0) {
                    z = true;
                }
            }
        }
        return z;
    }

    private boolean m() {
        if (this.e == null) {
            return false;
        }
        try {
            return this.e.isWifiEnabled();
        } catch (Throwable th) {
            th.printStackTrace();
            return false;
        }
    }
}
