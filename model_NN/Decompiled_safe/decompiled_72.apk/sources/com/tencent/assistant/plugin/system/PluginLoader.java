package com.tencent.assistant.plugin.system;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.text.TextUtils;
import com.qq.AppService.AstApp;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.plugin.mgr.i;
import com.tencent.nucleus.socialcontact.login.PluginLoadingDialog;
import com.tencent.pangu.utils.d;
import java.io.File;

/* compiled from: ProGuard */
public class PluginLoader extends Activity implements UIEventListener {

    /* renamed from: a  reason: collision with root package name */
    private final String f1110a = "plugin_path";
    private PluginLoadingDialog b;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Intent intent = getIntent();
        boolean z = false;
        a();
        if (intent != null) {
            String a2 = d.a(intent, "plugin_path");
            if (!TextUtils.isEmpty(a2)) {
                File file = new File(a2);
                if (file.exists()) {
                    String absolutePath = file.getAbsolutePath();
                    if (absolutePath.substring(absolutePath.lastIndexOf(File.separator) + 1).endsWith(".plg")) {
                        this.b = new PluginLoadingDialog(this);
                        this.b.setOwnerActivity(this);
                        if (!isFinishing()) {
                            this.b.show();
                        }
                        z = true;
                        i.b().a(AstApp.i(), absolutePath, (String) null);
                    } else {
                        return;
                    }
                }
            }
            if (!z) {
                finish();
            }
        }
    }

    public void finish() {
        if (this.b != null && this.b.isShowing()) {
            this.b.dismiss();
        }
        super.finish();
    }

    private void a() {
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_INSTALL_FAIL, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_INSTALL_SUCC, this);
    }

    private void b() {
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_INSTALL_FAIL, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_INSTALL_SUCC, this);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        b();
    }

    public void handleUIEvent(Message message) {
        int i = message.what;
        String str = (String) message.obj;
        if (!TextUtils.isEmpty(str) && str.equals("com.assistant.accelerate")) {
            switch (i) {
                case EventDispatcherEnum.UI_EVENT_PLUGIN_INSTALL_SUCC:
                    finish();
                    return;
                case EventDispatcherEnum.UI_EVENT_PLUGIN_INSTALL_FAIL:
                    finish();
                    return;
                default:
                    return;
            }
        }
    }
}
