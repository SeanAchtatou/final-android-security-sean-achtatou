package com.tencent.assistant.plugin;

import android.app.Notification;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;

/* compiled from: ProGuard */
public class PluginService extends Service implements IPluginService {

    /* renamed from: a  reason: collision with root package name */
    private Service f1071a;

    public void attachBaseService(Service service) {
        this.f1071a = service;
        attachBaseContext(this.f1071a.getBaseContext());
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void stopPluginSelf() {
        this.f1071a.stopSelf();
    }

    public void startPluginForeground(int i, Notification notification) {
        this.f1071a.startForeground(i, notification);
    }

    public void stopPluginForeground(boolean z) {
        this.f1071a.stopForeground(z);
    }

    public Context getApplicationContext() {
        return this.f1071a.getApplicationContext();
    }
}
