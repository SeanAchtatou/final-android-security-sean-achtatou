package com.tencent.assistant.thumbnailCache;

/* compiled from: ProGuard */
class e implements Comparable<e> {

    /* renamed from: a  reason: collision with root package name */
    public String f1792a;
    public Long b;
    final /* synthetic */ d c;

    public e(d dVar, String str, Long l) {
        this.c = dVar;
        this.f1792a = str;
        this.b = l;
    }

    /* renamed from: a */
    public int compareTo(e eVar) {
        return -this.b.compareTo(eVar.b);
    }
}
