package com.tencent.assistant.model;

import com.tencent.assistant.protocol.jce.FlashInfo;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
public class k {

    /* renamed from: a  reason: collision with root package name */
    private int f947a;
    private String b;
    private String c;
    private long d;
    private long e;
    private int f;
    private int g;
    private int h;
    private String i;
    private String j;
    private String k;
    private int l;
    private float m = 1.0f;
    private String n;
    private String o;
    private int p;
    private int q;
    private int r;
    private int s;
    private int t;
    private int u;
    private String v;

    public k() {
    }

    public k(FlashInfo flashInfo) {
        this.f947a = (int) flashInfo.a();
        this.b = flashInfo.b();
        this.c = flashInfo.c();
        this.d = flashInfo.d();
        this.e = flashInfo.e();
        this.f = flashInfo.f();
        this.g = flashInfo.g();
        this.l = flashInfo.i();
        this.i = flashInfo.h();
        this.k = Constants.STR_EMPTY;
        this.h = 0;
        this.n = flashInfo.j;
        this.p = flashInfo.k;
        this.q = flashInfo.l;
        this.r = flashInfo.m;
        this.s = flashInfo.n;
        this.t = flashInfo.o;
        this.u = flashInfo.p;
        this.v = flashInfo.q;
    }

    public int a() {
        return this.f947a;
    }

    public void a(int i2) {
        this.f947a = i2;
    }

    public String b() {
        return this.b;
    }

    public void a(String str) {
        this.b = str;
    }

    public String c() {
        return this.c;
    }

    public void b(String str) {
        this.c = str;
    }

    public long d() {
        return this.d;
    }

    public void a(long j2) {
        this.d = j2;
    }

    public long e() {
        return this.e;
    }

    public void b(long j2) {
        this.e = j2;
    }

    public int f() {
        return this.f;
    }

    public void b(int i2) {
        this.f = i2;
    }

    public int g() {
        return this.g;
    }

    public void c(int i2) {
        this.g = i2;
    }

    public int h() {
        return this.h;
    }

    public void d(int i2) {
        this.h = i2;
    }

    public String i() {
        return this.i;
    }

    public void c(String str) {
        this.i = str;
    }

    public String j() {
        return this.j;
    }

    public void d(String str) {
        this.j = str;
    }

    public String k() {
        return this.k;
    }

    public void e(String str) {
        this.k = str;
    }

    public int l() {
        return this.l;
    }

    public void e(int i2) {
        this.l = i2;
    }

    public int hashCode() {
        return this.f947a + 31;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        if (this.f947a != ((k) obj).f947a) {
            return false;
        }
        return true;
    }

    public String m() {
        return this.n;
    }

    public void f(String str) {
        this.n = str;
    }

    public int n() {
        return this.p;
    }

    public void f(int i2) {
        this.p = i2;
    }

    public int o() {
        return this.q;
    }

    public void g(int i2) {
        this.q = i2;
    }

    public int p() {
        return this.r;
    }

    public void h(int i2) {
        this.r = i2;
    }

    public int q() {
        return this.s;
    }

    public void i(int i2) {
        this.s = i2;
    }

    public int r() {
        return this.t;
    }

    public void j(int i2) {
        this.t = i2;
    }

    public int s() {
        return this.u;
    }

    public void k(int i2) {
        this.u = i2;
    }

    public String t() {
        return this.v;
    }

    public void g(String str) {
        this.v = str;
    }

    public String toString() {
        return "SplashInfo [id=" + this.f947a + ", title=" + this.b + ", desc=" + this.c + ", beginTime=" + this.d + ", endTime=" + this.e + ", runTime=" + this.f + ", runTimes=" + this.g + ", hasRunTimes=" + this.h + ", imageUrl=" + this.i + ", imageDataDesc=" + this.k + ", status=" + this.l + ", splashBitmapDensity=" + this.m + ", btnImage=" + this.n + ", btnMarginLeft=" + this.p + ", btnMarginBottom=" + this.q + ", btnHeight=" + this.r + ", btnWidth=" + this.s + ", splashType=" + this.t + ", targetType=" + this.u + ", target=" + this.v + "]";
    }

    public float u() {
        return this.m;
    }

    public void a(float f2) {
        this.m = f2;
    }

    public String v() {
        return this.o;
    }

    public void h(String str) {
        this.o = str;
    }
}
