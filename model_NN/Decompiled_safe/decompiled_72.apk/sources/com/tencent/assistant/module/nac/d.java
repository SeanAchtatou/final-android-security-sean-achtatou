package com.tencent.assistant.module.nac;

import com.tencent.assistant.manager.i;
import com.tencent.assistant.module.nac.NACEngine;
import com.tencent.assistant.protocol.jce.IPData;
import com.tencent.assistant.utils.TemporaryThreadManager;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

/* compiled from: ProGuard */
public class d {
    protected static d e = null;

    /* renamed from: a  reason: collision with root package name */
    protected ArrayList<NACEngine> f1005a = new ArrayList<>();
    protected b b;
    protected ReferenceQueue<INACListener> c = new ReferenceQueue<>();
    protected ArrayList<WeakReference<INACListener>> d = new ArrayList<>();

    public static synchronized d a() {
        d dVar;
        synchronized (d.class) {
            if (e == null) {
                e = new d();
            }
            dVar = e;
        }
        return dVar;
    }

    private d() {
        d();
        b(c());
        Iterator<NACEngine> it = this.f1005a.iterator();
        while (it.hasNext()) {
            it.next().a(NACEngine.NACEMode.NACMODE_DOMAIN);
        }
    }

    public void b() {
        this.f1005a.remove(b(1));
        d();
    }

    private void d() {
        this.b = new b();
        for (Map.Entry<String, c> value : this.b.f1003a.entrySet()) {
            this.f1005a.add(new NACEngine((c) value.getValue()));
        }
    }

    public f a(int i) {
        NACEngine b2 = b(i);
        if (b2 != null) {
            return b2.a();
        }
        return null;
    }

    public f a(String str) {
        f a2 = a(this.b.a(str));
        if (a2 == null) {
            return new f(str);
        }
        return a2;
    }

    public void a(int i, boolean z, long j) {
        NACEngine b2 = b(i);
        if (b2 != null) {
            b2.a(z, j);
        }
    }

    public synchronized void a(ArrayList<IPData> arrayList) {
        b(arrayList);
        c(arrayList);
    }

    /* access modifiers changed from: protected */
    public void b(ArrayList<IPData> arrayList) {
        if (arrayList != null && arrayList.size() != 0) {
            Iterator<IPData> it = arrayList.iterator();
            while (it.hasNext()) {
                IPData next = it.next();
                if (next.f1387a == 3) {
                    NACEngine b2 = b(1);
                    if (b2 != null) {
                        b2.b(next);
                    }
                } else {
                    NACEngine b3 = b(next.f1387a);
                    if (b3 != null) {
                        b3.a(next);
                    }
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public NACEngine b(int i) {
        Iterator<NACEngine> it = this.f1005a.iterator();
        while (it.hasNext()) {
            NACEngine next = it.next();
            if (next.b() == i) {
                return next;
            }
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public void c(ArrayList<IPData> arrayList) {
        if (arrayList != null && !arrayList.isEmpty()) {
            TemporaryThreadManager.get().start(new e(this, arrayList));
        }
    }

    /* access modifiers changed from: protected */
    public ArrayList<IPData> c() {
        return (ArrayList) i.y().h();
    }
}
