package com.tencent.assistant.protocol.jce;

import java.io.Serializable;

/* compiled from: ProGuard */
public final class ResponseHeadErrorCode implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    public static final ResponseHeadErrorCode f1471a = new ResponseHeadErrorCode(0, -1, "EC_AUTH_ACCOUNT_ABNORMAL");
    public static final ResponseHeadErrorCode b = new ResponseHeadErrorCode(1, -2, "EC_AUTH_PWD_CHANGE");
    public static final ResponseHeadErrorCode c = new ResponseHeadErrorCode(2, -3, "EC_AUTH_EXPIRED");
    public static final ResponseHeadErrorCode d = new ResponseHeadErrorCode(3, -4, "EC_ACCESS_CONTROL_FAILED");
    public static final ResponseHeadErrorCode e = new ResponseHeadErrorCode(4, -10, "EC_SERVER_INTERNAL_ERROR");
    public static final ResponseHeadErrorCode f = new ResponseHeadErrorCode(5, -11, "EC_BUSINESS_ERROR");
    public static final ResponseHeadErrorCode g = new ResponseHeadErrorCode(6, -5, "EC_CS_TICKET_EXPIRED");
    public static final ResponseHeadErrorCode h = new ResponseHeadErrorCode(7, -6, "EC_CS_TICKET_INVALID");
    public static final ResponseHeadErrorCode i = new ResponseHeadErrorCode(8, -7, "EC_COMMON_ERROR");
    public static final ResponseHeadErrorCode j = new ResponseHeadErrorCode(9, -8, "EC_CS_TICKET_ERROR");
    static final /* synthetic */ boolean k = (!ResponseHeadErrorCode.class.desiredAssertionStatus());
    private static ResponseHeadErrorCode[] l = new ResponseHeadErrorCode[10];
    private int m;
    private String n = new String();

    public String toString() {
        return this.n;
    }

    private ResponseHeadErrorCode(int i2, int i3, String str) {
        this.n = str;
        this.m = i3;
        l[i2] = this;
    }
}
