package com.tencent.assistant.localres;

import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.utils.bo;
import java.util.Comparator;

/* compiled from: ProGuard */
final class s implements Comparator<LocalApkInfo> {
    s() {
    }

    /* renamed from: a */
    public int compare(LocalApkInfo localApkInfo, LocalApkInfo localApkInfo2) {
        if (localApkInfo == null || localApkInfo2 == null) {
            return 0;
        }
        if (localApkInfo.mInstallDate > 0 && localApkInfo2.mInstallDate > 0) {
            int f = bo.f(localApkInfo.mInstallDate);
            int f2 = bo.f(localApkInfo2.mInstallDate);
            if (f != f2) {
                return f - f2;
            }
            long j = localApkInfo2.occupySize - localApkInfo.occupySize;
            if (j > 0) {
                return 1;
            }
            return j == 0 ? 0 : -1;
        } else if (localApkInfo.mInstallDate > 0 && localApkInfo2.mInstallDate <= 0) {
            return -1;
        } else {
            if (localApkInfo.mInstallDate <= 0 && localApkInfo2.mInstallDate > 0) {
                return 1;
            }
            long j2 = localApkInfo2.occupySize - localApkInfo.occupySize;
            if (j2 <= 0) {
                return j2 == 0 ? 0 : -1;
            }
            return 1;
        }
    }
}
