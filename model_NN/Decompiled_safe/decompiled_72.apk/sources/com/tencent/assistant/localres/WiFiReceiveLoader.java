package com.tencent.assistant.localres;

import android.content.Context;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.utils.FileUtil;
import java.io.File;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class WiFiReceiveLoader extends LocalMediaLoader<WiFiReceiveItem> {
    private ArrayList<WiFiReceiveItem> f = new ArrayList<>();

    public WiFiReceiveLoader(Context context) {
        super(context);
        this.c = 7;
    }

    public ArrayList<WiFiReceiveItem> getLongerList() {
        return this.f;
    }

    public void addReceiveItems(ArrayList<WiFiReceiveItem> arrayList) {
        this.b.addAll(0, arrayList);
    }

    /* access modifiers changed from: protected */
    public void a() {
        this.b.clear();
        this.f.clear();
        a(FileUtil.getWifiRootDir(), 0);
        a(FileUtil.getWifiImageDir(), 0);
        a(this.b);
        a(this.f);
        a(this, this.b, true);
    }

    private void a(String str, int i) {
        File file;
        if (i <= 1 && (file = new File(str)) != null && file.exists()) {
            File[] listFiles = file.listFiles();
            long currentTimeMillis = System.currentTimeMillis();
            if (listFiles != null) {
                for (File file2 : listFiles) {
                    WiFiReceiveItem wiFiReceiveItem = new WiFiReceiveItem();
                    if (file2.isDirectory()) {
                        a(file2.getAbsolutePath(), i + 1);
                    } else if (file2.isFile()) {
                        wiFiReceiveItem.mFileName = file2.getName();
                        wiFiReceiveItem.mFilePath = file2.getAbsolutePath();
                        wiFiReceiveItem.mFileSize = file2.length();
                        wiFiReceiveItem.mFileType = wiFiReceiveItem.mFilePath.substring(wiFiReceiveItem.mFilePath.lastIndexOf(".") + 1);
                        wiFiReceiveItem.mImageType = getImageType(wiFiReceiveItem.mFileType);
                        wiFiReceiveItem.mReceiveState = 1;
                        wiFiReceiveItem.lastModifiedTime = file2.lastModified();
                        if (currentTimeMillis - file2.lastModified() < 604800000) {
                            this.b.add(wiFiReceiveItem);
                        } else {
                            this.f.add(wiFiReceiveItem);
                        }
                    }
                }
            }
        }
    }

    public static TXImageView.TXImageViewType getImageType(String str) {
        if (str == null) {
            return TXImageView.TXImageViewType.UNKNOWN_IMAGE_TYPE;
        }
        String lowerCase = str.toLowerCase();
        if (lowerCase.equals("m4a") || lowerCase.equals("mp3") || lowerCase.equals("mid") || lowerCase.equals("xmf") || lowerCase.equals("ogg") || lowerCase.equals("wav")) {
            return TXImageView.TXImageViewType.LOCAL_AUDIO_COVER;
        }
        if (lowerCase.equals("3gp") || lowerCase.equals("mp4")) {
            return TXImageView.TXImageViewType.LOCAL_VIDEO_THUMBNAIL;
        }
        if (lowerCase.equals("jpg") || lowerCase.equals("gif") || lowerCase.equals("png") || lowerCase.equals("jpeg") || lowerCase.equals("bmp")) {
            return TXImageView.TXImageViewType.LOCAL_LARGER_IMAGE_THUMBNAIL;
        }
        if (lowerCase.equals("apk")) {
            return TXImageView.TXImageViewType.UNINSTALL_APK_ICON;
        }
        return TXImageView.TXImageViewType.UNKNOWN_IMAGE_TYPE;
    }

    private void a(ArrayList<WiFiReceiveItem> arrayList) {
        boolean z;
        int size = arrayList.size();
        int i = 1;
        while (i < size) {
            int i2 = 0;
            boolean z2 = false;
            while (i2 < size - i) {
                if (arrayList.get(i2).lastModifiedTime < arrayList.get(i2 + 1).lastModifiedTime) {
                    arrayList.set(i2, arrayList.get(i2 + 1));
                    arrayList.set(i2 + 1, arrayList.get(i2));
                    z = true;
                } else {
                    z = z2;
                }
                i2++;
                z2 = z;
            }
            if (z2) {
                i++;
            } else {
                return;
            }
        }
    }
}
