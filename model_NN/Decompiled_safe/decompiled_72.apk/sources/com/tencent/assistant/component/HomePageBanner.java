package com.tencent.assistant.component;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.manager.t;
import com.tencent.assistant.protocol.jce.ColorCardItem;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.by;
import com.tencent.assistantv2.st.b.a;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class HomePageBanner extends RelativeLayout {
    public static final byte APP_RECOMMEND_CARD_TYPE_BANNER = 4;
    public static final byte APP_TAB_TYPE_ENTRANCE = 3;
    public static final byte CARD_TYPE_BANNER = 2;
    public static final byte GAME_ENTRANCE_CARD_TYPE = 10;
    public static final byte GAME_RECOMMEND_CARD_TYPE_BANNER = 9;
    private static final int MSG_PLAY = 888;
    private static final int PLAY_TIME = 5000;
    /* access modifiers changed from: private */
    public String bannerSlotTag = STConst.ST_DEFAULT_SLOT;
    /* access modifiers changed from: private */
    public List<ColorCardItem> cards = new ArrayList();
    /* access modifiers changed from: private */
    public Context context = null;
    /* access modifiers changed from: private */
    public Handler handler = new o(this);
    private LayoutInflater inflater = null;
    /* access modifiers changed from: private */
    public HorizonScrollLayout mHorizonScrollLayout = null;
    /* access modifiers changed from: private */
    public boolean playing = false;
    private LinearLayout pointsLy = null;
    private a strategy = null;

    public HomePageBanner(Context context2) {
        super(context2);
        init(context2);
    }

    public HomePageBanner(Context context2, AttributeSet attributeSet) {
        super(context2, attributeSet);
        init(context2);
    }

    public HomePageBanner(Context context2, AttributeSet attributeSet, int i) {
        super(context2, attributeSet, i);
        init(context2);
    }

    private void init(Context context2) {
        this.context = context2;
        this.inflater = LayoutInflater.from(context2);
        initBanner();
    }

    private void initBanner() {
        removeAllViews();
        this.inflater.inflate((int) R.layout.home_page_list_header, this);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, by.a(this.context, 110.0f));
        this.mHorizonScrollLayout = (HorizonScrollLayout) findViewById(R.id.images);
        this.mHorizonScrollLayout.setLayoutParams(layoutParams);
        this.mHorizonScrollLayout.setEnableOverScroll(false);
        this.mHorizonScrollLayout.setScrollSlop(1.75f);
        this.mHorizonScrollLayout.setOnTouchScrollListener(new m(this));
        this.pointsLy = (LinearLayout) findViewById(R.id.points);
    }

    public void setLockAllWhenTouch(boolean z) {
        this.mHorizonScrollLayout.setLockAllWhenTouch(z);
    }

    public void setBannerSlotTag(String str) {
        this.bannerSlotTag = str;
    }

    public void refreshBanner(List<ColorCardItem> list, int i) {
        stopPlay();
        if (list == null || list.size() <= 0) {
            setBackgroundColor(this.context.getResources().getColor(17170445));
            removeAllViews();
            return;
        }
        this.cards.clear();
        if (i <= 0) {
            i = 2;
        }
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= list.size()) {
                break;
            }
            ColorCardItem colorCardItem = list.get(i3);
            if (colorCardItem.c() == i) {
                this.cards.add(colorCardItem);
            }
            i2 = i3 + 1;
        }
        if (this.cards.size() > 0) {
            if (getChildCount() == 0) {
                initBanner();
            }
            refreshBanner();
            return;
        }
        setBackgroundColor(this.context.getResources().getColor(17170445));
        removeAllViews();
    }

    private void refreshBanner() {
        this.mHorizonScrollLayout.removeAllViews();
        for (int i = 0; i < this.cards.size(); i++) {
            ColorCardItem colorCardItem = this.cards.get(i);
            View inflate = this.inflater.inflate((int) R.layout.banner_big_image, (ViewGroup) null);
            ((TextView) inflate.findViewById(R.id.default_pic_txt)).setText(colorCardItem.c);
            TXImageView tXImageView = (TXImageView) inflate.findViewById(R.id.pic);
            tXImageView.updateImageView(colorCardItem.f1199a, -1, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
            String str = colorCardItem.b.f1125a;
            Bundle bundle = new Bundle();
            bundle.putSerializable("com.tencent.assistant.ACTION_URL", colorCardItem.b);
            tXImageView.setOnClickListener(new n(this, str, bundle, i, colorCardItem.f1199a));
            this.mHorizonScrollLayout.addView(inflate);
        }
        this.mHorizonScrollLayout.snapToFirstScreen();
        updateDots(0);
        if (this.cards.size() > 1) {
            this.mHorizonScrollLayout.setCircle(true);
            startPlay();
            return;
        }
        this.mHorizonScrollLayout.setCircle(false);
        stopPlay();
    }

    /* access modifiers changed from: private */
    public void updateDots(int i) {
        bannerExposureReport(i);
        this.pointsLy.removeAllViews();
        if (this.cards.size() >= 2) {
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-2, -2);
            layoutParams.leftMargin = by.b(2.0f);
            layoutParams.rightMargin = by.b(2.0f);
            layoutParams.height = by.a(this.context, 5.0f);
            layoutParams.width = by.a(this.context, 5.0f);
            for (int i2 = 0; i2 < this.cards.size(); i2++) {
                ImageView imageView = new ImageView(this.context);
                imageView.setLayoutParams(layoutParams);
                if (i == i2) {
                    try {
                        imageView.setImageResource(R.drawable.banner_indicator_01);
                    } catch (OutOfMemoryError e) {
                        t.a().b();
                    }
                } else {
                    imageView.setImageResource(R.drawable.banner_indicator_02);
                }
                this.pointsLy.addView(imageView);
            }
        }
    }

    public void startPlay() {
        if (this.cards.size() > 1) {
            this.playing = true;
            if (this.handler != null) {
                this.handler.removeMessages(MSG_PLAY);
                this.handler.sendEmptyMessageDelayed(MSG_PLAY, 5000);
            }
        }
    }

    public void stopPlay() {
        this.playing = false;
        if (this.handler != null) {
            this.handler.removeMessages(MSG_PLAY);
        }
    }

    public void recycle() {
        if (this.handler != null) {
            this.handler.removeMessages(MSG_PLAY);
            this.handler = null;
        }
    }

    private void bannerExposureReport(int i) {
        ColorCardItem colorCardItem;
        if (this.mHorizonScrollLayout != null) {
            STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.context, 100);
            buildSTInfo.slotId = com.tencent.assistantv2.st.page.a.a(this.bannerSlotTag, i);
            if (this.cards.size() > i && (colorCardItem = this.cards.get(i)) != null) {
                buildSTInfo.extraData = colorCardItem.f1199a;
            }
            if (this.strategy == null) {
                this.strategy = new a();
            }
            this.strategy.a(this.context, buildSTInfo);
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        this.mHorizonScrollLayout.onTouchEvent(motionEvent);
        return true;
    }
}
