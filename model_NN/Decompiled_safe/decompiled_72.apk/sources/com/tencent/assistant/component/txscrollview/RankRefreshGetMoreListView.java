package com.tencent.assistant.component.txscrollview;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AbsListView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.txscrollview.TXRefreshScrollViewBase;
import com.tencent.assistant.component.txscrollview.TXScrollViewBase;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.bd;
import com.tencent.assistant.utils.by;
import com.tencent.assistantv2.component.RankHeaderLayout;
import com.tencent.assistantv2.manager.a;

/* compiled from: ProGuard */
public class RankRefreshGetMoreListView extends TXRefreshScrollViewBase<ListView> implements AbsListView.OnScrollListener {
    private static int w = 1;

    /* renamed from: a  reason: collision with root package name */
    protected RankHeaderLayout f688a;
    protected TXLoadingLayoutBase b;
    protected TXRefreshScrollViewBase.RefreshState c = TXRefreshScrollViewBase.RefreshState.RESET;
    protected int d = 0;
    private ListAdapter e;
    private AbsListView.OnScrollListener f;
    public boolean isHideInstalledAppAreaAdded = false;
    private ImageView u;
    private int v = 0;
    /* access modifiers changed from: private */
    public boolean x = true;
    private boolean y = true;
    private boolean z = false;

    public RankRefreshGetMoreListView(Context context) {
        super(context, TXScrollViewBase.ScrollDirection.SCROLL_DIRECTION_VERTICAL, TXScrollViewBase.ScrollMode.PULL_FROM_START);
    }

    public RankRefreshGetMoreListView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    /* access modifiers changed from: protected */
    public void a() {
        LinearLayout.LayoutParams k = k();
        if (this.g != null) {
            if (this.g.getParent() == this) {
                removeView(this.g);
            }
            a(this.g, 0, k);
        }
        this.h = null;
        l();
    }

    public void onRefreshComplete(boolean z2) {
        onRefreshComplete(z2, true);
    }

    public void onRefreshComplete(boolean z2, boolean z3) {
        super.onRefreshComplete(z2);
        if (this.c == TXRefreshScrollViewBase.RefreshState.REFRESHING) {
            if (z2) {
                this.c = TXRefreshScrollViewBase.RefreshState.RESET;
            } else {
                this.c = TXRefreshScrollViewBase.RefreshState.REFRESH_LOAD_FINISH;
            }
        } else if (this.c == TXRefreshScrollViewBase.RefreshState.REFRESH_LOAD_FINISH) {
            this.c = TXRefreshScrollViewBase.RefreshState.RESET;
        } else if (this.c == TXRefreshScrollViewBase.RefreshState.RESET && !z2) {
            this.c = TXRefreshScrollViewBase.RefreshState.REFRESH_LOAD_FINISH;
        }
        a(z3);
        if (z3) {
            w = 1;
        } else {
            w--;
        }
    }

    public void onTopRefreshComplete() {
        if (this.x) {
            this.x = false;
            b();
        }
    }

    public void onTopRefreshCompleteNoAnimation() {
        if (this.x) {
            this.x = false;
            this.y = false;
            b();
        }
    }

    /* access modifiers changed from: protected */
    public void b() {
        int i;
        RankHeaderLayout rankHeaderLayout;
        TXLoadingLayoutBase tXLoadingLayoutBase;
        boolean z2;
        if (this.n != TXScrollViewBase.ScrollState.ScrollState_FromStart || this.g == null || this.f688a == null) {
            i = 0;
            rankHeaderLayout = null;
            tXLoadingLayoutBase = null;
            z2 = false;
        } else {
            tXLoadingLayoutBase = this.g;
            rankHeaderLayout = this.f688a;
            i = 0 - this.g.getContentSize();
            z2 = Math.abs(((ListView) this.s).getFirstVisiblePosition() - 0) <= 1;
        }
        if (rankHeaderLayout != null && rankHeaderLayout.getVisibility() == 0) {
            rankHeaderLayout.setVisibility(8);
            tXLoadingLayoutBase.showAllSubViews();
            if (z2) {
                ((ListView) this.s).setSelection(0);
                a(i);
            }
        }
        this.m = false;
        this.i = true;
        if (this.y) {
            smoothScrollTo(0);
        } else {
            scrollTo(0, 0);
            this.y = true;
        }
        postDelayed(new a(this), 200);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public ListView b(Context context) {
        ListView listView = new ListView(context);
        if (this.o != TXScrollViewBase.ScrollMode.NONE) {
            this.f688a = a(context, listView, TXScrollViewBase.ScrollMode.PULL_FROM_START);
            this.b = new RefreshListLoading(context, TXScrollViewBase.ScrollDirection.SCROLL_DIRECTION_VERTICAL, TXScrollViewBase.ScrollMode.PULL_FROM_END);
            this.b.setVisibility(0);
            listView.addFooterView(this.b);
            listView.setOnScrollListener(this);
        }
        return listView;
    }

    /* access modifiers changed from: protected */
    public RankHeaderLayout a(Context context, ListView listView, TXScrollViewBase.ScrollMode scrollMode) {
        FrameLayout frameLayout = new FrameLayout(getContext());
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-1, -2, 1);
        RankHeaderLayout rankHeaderLayout = (RankHeaderLayout) a(context, scrollMode);
        rankHeaderLayout.setVisibility(8);
        frameLayout.addView(rankHeaderLayout, layoutParams);
        FrameLayout.LayoutParams layoutParams2 = new FrameLayout.LayoutParams(-1, by.a(context, (float) this.v));
        this.u = new ImageView(context);
        layoutParams2.topMargin = rankHeaderLayout.getHeight();
        frameLayout.addView(this.u, layoutParams2);
        listView.addHeaderView(frameLayout, null, false);
        return rankHeaderLayout;
    }

    public void setPaddingTopSize(int i) {
        this.v = i;
    }

    public void addFooterView(View view) {
        if (view != null) {
            if (this.b != null) {
                ((ListView) this.s).removeFooterView(this.b);
            }
            this.b = (TXLoadingLayoutBase) view;
            ((ListView) this.s).addFooterView(this.b);
            this.b.setVisibility(0);
            a_();
        }
    }

    public void addClickLoadMore() {
        this.b.setOnClickListener(new b(this));
    }

    public void onScrollStateChanged(AbsListView absListView, int i) {
        this.d = i;
        if (this.f != null) {
            this.f.onScrollStateChanged(absListView, i);
        }
        if (i == 0 && this.z) {
            q();
            w = 1;
        }
    }

    private void q() {
        if (this.c == TXRefreshScrollViewBase.RefreshState.RESET) {
            if (this.k != null) {
                this.k.onTXRefreshListViewRefresh(TXScrollViewBase.ScrollState.ScrollState_FromEnd);
            }
            this.c = TXRefreshScrollViewBase.RefreshState.REFRESHING;
            a_();
        }
    }

    /* access modifiers changed from: protected */
    public void a_() {
        a(true);
    }

    /* access modifiers changed from: protected */
    public void b_() {
        int i = 0;
        if (getRawAdapter() != null) {
            i = getRawAdapter().getCount();
        }
        if (this.b != null && i > 0) {
            this.b.loadFinish(bd.b(getContext(), i));
        }
    }

    /* access modifiers changed from: protected */
    public void a(boolean z2) {
        if (this.b != null) {
            switch (c.f705a[this.c.ordinal()]) {
                case 1:
                    if (z2) {
                        this.b.loadSuc();
                        return;
                    } else {
                        this.b.loadFail();
                        return;
                    }
                case 2:
                    b_();
                    return;
                case 3:
                    this.b.refreshing();
                    return;
                default:
                    return;
            }
        }
    }

    public void onScroll(AbsListView absListView, int i, int i2, int i3) {
        if (this.s != null) {
            if (this.isHideInstalledAppAreaAdded) {
                this.g.findViewById(R.id.tips).setVisibility(0);
            } else {
                ((RankHeaderLayout) this.g).a(a.a().b().c());
                this.g.findViewById(R.id.tips).setVisibility(8);
            }
            if (this.f != null) {
                this.f.onScroll(absListView, i, i2, i3);
            }
            this.z = f();
            if (this.z && w > 0 && !g()) {
                q();
            }
        }
    }

    /* access modifiers changed from: protected */
    public TXLoadingLayoutBase a(Context context, TXScrollViewBase.ScrollMode scrollMode) {
        return new RankHeaderLayout(context, TXScrollViewBase.ScrollDirection.SCROLL_DIRECTION_VERTICAL, scrollMode);
    }

    /* access modifiers changed from: protected */
    public boolean e() {
        View childAt;
        ListAdapter adapter = ((ListView) this.s).getAdapter();
        if (adapter == null || adapter.isEmpty()) {
            return true;
        }
        if (((ListView) this.s).getFirstVisiblePosition() > 1 || (childAt = ((ListView) this.s).getChildAt(0)) == null) {
            return false;
        }
        return childAt.getTop() >= ((ListView) this.s).getTop();
    }

    public void setRankHeaderPaddingBottomAdded(int i) {
        if (this.g != null && (this.g instanceof RankHeaderLayout)) {
            ((RankHeaderLayout) this.g).a(i);
        }
    }

    /* access modifiers changed from: protected */
    public boolean f() {
        View childAt;
        ListAdapter adapter = ((ListView) this.s).getAdapter();
        if (adapter == null || adapter.isEmpty()) {
            return true;
        }
        int count = ((ListView) this.s).getCount() - 1;
        int lastVisiblePosition = ((ListView) this.s).getLastVisiblePosition();
        if (lastVisiblePosition < count - 1 || (childAt = ((ListView) this.s).getChildAt(lastVisiblePosition - ((ListView) this.s).getFirstVisiblePosition())) == null) {
            return false;
        }
        return childAt.getBottom() <= ((ListView) this.s).getBottom();
    }

    /* access modifiers changed from: protected */
    public boolean g() {
        if (((ListView) this.s).getFirstVisiblePosition() > 0) {
            return true;
        }
        if (((ListView) this.s).getLastVisiblePosition() < ((ListView) this.s).getCount() - 1) {
            return true;
        }
        return false;
    }

    public void setDivider(Drawable drawable) {
        ((ListView) this.s).setDivider(drawable);
    }

    public void setSelection(int i) {
        ((ListView) this.s).setSelection(i);
    }

    public void setAdapter(ListAdapter listAdapter) {
        if (listAdapter != null) {
            this.e = listAdapter;
            ((ListView) this.s).setAdapter(listAdapter);
        }
    }

    public void setSelector(Drawable drawable) {
        if (this.s != null && drawable != null) {
            ((ListView) this.s).setSelector(drawable);
        }
    }

    public int getFirstVisiblePosition() {
        if (this.s != null) {
            return ((ListView) this.s).getFirstVisiblePosition();
        }
        return -1;
    }

    public ListView getListView() {
        return (ListView) this.s;
    }

    public ListAdapter getRawAdapter() {
        return this.e;
    }

    public void setOnScrollerListener(AbsListView.OnScrollListener onScrollListener) {
        this.f = onScrollListener;
    }

    /* access modifiers changed from: protected */
    public int h() {
        return 200;
    }

    /* access modifiers changed from: protected */
    public int i() {
        XLog.d("RefreshListLoading", " scrollMoveEvent");
        int i = super.i();
        int i2 = 0;
        if (i != 0) {
            if (this.n == TXScrollViewBase.ScrollState.ScrollState_FromStart && this.g != null) {
                i2 = this.g.getContentSize() >> 1;
                this.g.onPull(i);
            } else if (this.n == TXScrollViewBase.ScrollState.ScrollState_FromEnd && this.h != null) {
                i2 = this.h.getContentSize();
                this.h.onPull(i);
            }
            if (this.n == TXScrollViewBase.ScrollState.ScrollState_FromEnd && this.j == TXRefreshScrollViewBase.RefreshState.REFRESH_LOAD_FINISH) {
                return i;
            }
            if (this.j != TXRefreshScrollViewBase.RefreshState.PULL_TO_REFRESH && i2 >= Math.abs(i)) {
                a(TXRefreshScrollViewBase.RefreshState.PULL_TO_REFRESH);
            } else if (this.j == TXRefreshScrollViewBase.RefreshState.PULL_TO_REFRESH && i2 < Math.abs(i)) {
                a(TXRefreshScrollViewBase.RefreshState.RELEASE_TO_REFRESH);
            }
        }
        return i;
    }

    public void setShowHeaderFilterInfo(boolean z2) {
        if (this.g != null) {
            ((RankHeaderLayout) this.g).b(z2);
        }
    }

    public void setTopPaddingSize(int i) {
        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) this.u.getLayoutParams();
        layoutParams.height = by.a(getContext(), (float) i);
        this.u.setLayoutParams(layoutParams);
        requestLayout();
    }
}
