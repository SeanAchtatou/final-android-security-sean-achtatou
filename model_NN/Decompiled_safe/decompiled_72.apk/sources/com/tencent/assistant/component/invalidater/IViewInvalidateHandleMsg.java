package com.tencent.assistant.component.invalidater;

/* compiled from: ProGuard */
public interface IViewInvalidateHandleMsg {
    void handleMessage(ViewInvalidateMessage viewInvalidateMessage);
}
