package com.tencent.assistant.component.dialog;

import android.content.DialogInterface;
import com.tencent.assistant.AppConst;

/* compiled from: ProGuard */
final class e implements DialogInterface.OnCancelListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppConst.OneBtnDialogInfo f651a;

    e(AppConst.OneBtnDialogInfo oneBtnDialogInfo) {
        this.f651a = oneBtnDialogInfo;
    }

    public void onCancel(DialogInterface dialogInterface) {
        this.f651a.onCancell();
    }
}
