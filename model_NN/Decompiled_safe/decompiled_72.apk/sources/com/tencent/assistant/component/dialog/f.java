package com.tencent.assistant.component.dialog;

import android.app.Dialog;
import android.view.View;
import com.tencent.assistant.AppConst;

/* compiled from: ProGuard */
final class f implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppConst.OneBtnDialogInfo f652a;
    final /* synthetic */ Dialog b;

    f(AppConst.OneBtnDialogInfo oneBtnDialogInfo, Dialog dialog) {
        this.f652a = oneBtnDialogInfo;
        this.b = dialog;
    }

    public void onClick(View view) {
        if (this.f652a != null) {
            this.f652a.onBtnClick();
            try {
                this.b.dismiss();
            } catch (Exception e) {
            }
        }
    }
}
