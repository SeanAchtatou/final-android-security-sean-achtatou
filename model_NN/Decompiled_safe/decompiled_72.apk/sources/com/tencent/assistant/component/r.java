package com.tencent.assistant.component;

import com.tencent.assistant.component.invalidater.ViewInvalidateMessage;
import com.tencent.assistant.component.invalidater.ViewInvalidateMessageHandler;
import java.util.HashMap;

/* compiled from: ProGuard */
class r extends ViewInvalidateMessageHandler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ LocalPkgSizeTextView f685a;

    r(LocalPkgSizeTextView localPkgSizeTextView) {
        this.f685a = localPkgSizeTextView;
    }

    public void handleMessage(ViewInvalidateMessage viewInvalidateMessage) {
        HashMap hashMap;
        String str;
        if (viewInvalidateMessage != null && this.f685a.mPkgName != null) {
            if (viewInvalidateMessage.params != null) {
                hashMap = (HashMap) viewInvalidateMessage.params;
            } else {
                hashMap = null;
            }
            if (hashMap != null && (str = (String) hashMap.get("NAME")) != null && str.equals(this.f685a.mPkgName)) {
                this.f685a.updateText(str, ((Long) hashMap.get("SIZE")).longValue());
            }
        }
    }
}
