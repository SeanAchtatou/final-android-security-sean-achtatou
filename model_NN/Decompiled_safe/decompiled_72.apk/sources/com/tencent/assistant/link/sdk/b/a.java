package com.tencent.assistant.link.sdk.b;

import com.tencent.connect.common.Constants;
import java.io.UnsupportedEncodingException;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: ProGuard */
public class a {

    /* renamed from: a  reason: collision with root package name */
    public String f798a = Constants.STR_EMPTY;
    public String b = Constants.STR_EMPTY;
    public String c = Constants.STR_EMPTY;
    public int d = 0;

    public a(String str, String str2, String str3) {
        this.f798a = str;
        this.b = str2;
        this.c = str3;
    }

    public byte[] a() {
        JSONObject jSONObject = new JSONObject();
        try {
            String a2 = com.tencent.assistant.link.sdk.a.a.a(this.c);
            jSONObject.put("taskPackageName", this.f798a);
            jSONObject.put("taskVersion", this.b);
            jSONObject.put("taskAction", a2);
            String jSONObject2 = jSONObject.toString();
            if (jSONObject2 != null) {
                return jSONObject2.getBytes("UTF-8");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e2) {
            e2.printStackTrace();
        }
        return null;
    }
}
