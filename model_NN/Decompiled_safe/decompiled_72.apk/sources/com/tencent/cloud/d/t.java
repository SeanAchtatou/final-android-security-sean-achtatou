package com.tencent.cloud.d;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.protocol.jce.AppUpdateInfo;

/* compiled from: ProGuard */
class t implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ s f2316a;

    t(s sVar) {
        this.f2316a = sVar;
    }

    public void run() {
        AppUpdateInfo appUpdateInfo;
        LocalApkInfo localApkInfo;
        JceStruct a2;
        if (this.f2316a.c.get() < this.f2316a.b.size() && (appUpdateInfo = (AppUpdateInfo) this.f2316a.b.get(this.f2316a.c.getAndIncrement())) != null && (localApkInfo = ApkResourceManager.getInstance().getLocalApkInfo(appUpdateInfo.f1162a)) != null && (a2 = this.f2316a.a(localApkInfo, appUpdateInfo)) != null) {
            int unused = this.f2316a.send(a2);
        }
    }
}
