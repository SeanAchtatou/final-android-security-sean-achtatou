package com.tencent.cloud.component;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.manager.webview.component.TxWebViewContainer;
import com.tencent.assistant.manager.webview.component.b;
import com.tencent.assistant.manager.webview.component.i;
import com.tencent.assistant.manager.webview.js.JsBridge;
import com.tencent.assistant.net.c;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.bx;
import com.tencent.smtt.sdk.WebView;

/* compiled from: ProGuard */
public class VideoDetailView extends LinearLayout implements i {
    private static final String c = VideoDetailView.class.getSimpleName();

    /* renamed from: a  reason: collision with root package name */
    private TxWebViewContainer f2271a;
    private Context b = null;

    public VideoDetailView(Context context) {
        super(context);
        this.b = context;
        e();
    }

    public VideoDetailView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.b = context;
        e();
    }

    private void e() {
        LayoutInflater.from(this.b).inflate((int) R.layout.video_detail_layout, this);
        this.f2271a = (TxWebViewContainer) findViewById(R.id.txwebviewcontainer);
        this.f2271a.a(this);
        b bVar = new b();
        if (!c.a()) {
            bVar.d = 1;
        } else {
            bVar.d = -1;
        }
        this.f2271a.a(bVar);
        String a2 = bx.a();
        XLog.i(c, "video url: " + a2);
        a(a2);
    }

    public void c() {
        if (this.f2271a != null) {
            this.f2271a.a();
        }
    }

    public void d() {
        if (this.f2271a != null) {
            this.f2271a.b();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.cloud.component.VideoDetailView.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.cloud.component.VideoDetailView.a(com.tencent.smtt.sdk.WebView, int):void
      com.tencent.assistant.manager.webview.component.i.a(com.tencent.smtt.sdk.WebView, int):void
      com.tencent.cloud.component.VideoDetailView.a(java.lang.String, boolean):void */
    public void a(String str) {
        a(str, true);
    }

    public void a(String str, boolean z) {
        if (this.f2271a != null && !TextUtils.isEmpty(str)) {
            this.f2271a.a(str, z);
            this.f2271a.a(0);
        }
    }

    public void a(WebView webView, int i) {
    }

    public void a() {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.cloud.component.VideoDetailView.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.cloud.component.VideoDetailView.a(com.tencent.smtt.sdk.WebView, int):void
      com.tencent.assistant.manager.webview.component.i.a(com.tencent.smtt.sdk.WebView, int):void
      com.tencent.cloud.component.VideoDetailView.a(java.lang.String, boolean):void */
    public void b() {
        if (!JsBridge.getPreferences().getBoolean(JsBridge.WB_REPORT, false)) {
            a("javascript:(function () { var video = document.createElement('video'); window.location.href = 'jsb://webViewCompatibilityReport/0/console.log?canPlayType1=' + video.canPlayType('video/mp4; codecs=\"avc1.42E01E\"') + '&canPlayType2=' + video.canPlayType('video/mp4; codecs=\"mp4v.20.8\"') + '&canPlayType3=' + video.canPlayType('video/mp4'); })();", false);
            XLog.i(c, "[onPageFinished] ---> load js for report");
        }
    }
}
