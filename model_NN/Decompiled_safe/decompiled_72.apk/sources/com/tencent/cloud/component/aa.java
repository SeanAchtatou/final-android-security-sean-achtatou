package com.tencent.cloud.component;

import android.widget.AbsListView;
import com.tencent.assistant.component.invalidater.ListViewScrollListener;

/* compiled from: ProGuard */
class aa extends ListViewScrollListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ UpdateListView f2273a;

    aa(UpdateListView updateListView) {
        this.f2273a = updateListView;
    }

    public void onScrollStateChanged(AbsListView absListView, int i) {
        super.onScrollStateChanged(absListView, i);
    }

    public void onScroll(AbsListView absListView, int i, int i2, int i3) {
        if (this.f2273a.l != null && this.f2273a.f != null) {
            if (i == 0) {
                this.f2273a.l.setVisibility(8);
            }
            this.f2273a.i();
        }
    }
}
