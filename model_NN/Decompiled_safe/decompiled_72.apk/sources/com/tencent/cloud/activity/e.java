package com.tencent.cloud.activity;

import android.support.v4.view.ViewPager;
import com.tencent.assistantv2.activity.a;

/* compiled from: ProGuard */
public class e implements ViewPager.OnPageChangeListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppRankActivity f2190a;

    public e(AppRankActivity appRankActivity) {
        this.f2190a = appRankActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.cloud.component.AppRankTabBarView.a(int, boolean):void
     arg types: [int, int]
     candidates:
      com.tencent.cloud.component.AppRankTabBarView.a(android.content.Context, android.util.AttributeSet):void
      com.tencent.cloud.component.AppRankTabBarView.a(int, float):void
      com.tencent.cloud.component.AppRankTabBarView.a(int, boolean):void */
    public void onPageSelected(int i) {
        this.f2190a.v.a(i, true);
        if (!(this.f2190a.y == i || this.f2190a.x.a(this.f2190a.y) == null)) {
            ((a) this.f2190a.x.a(this.f2190a.y)).D();
        }
        this.f2190a.d(i);
    }

    public void onPageScrolled(int i, float f, int i2) {
        this.f2190a.v.a(i, f);
    }

    public void onPageScrollStateChanged(int i) {
    }
}
