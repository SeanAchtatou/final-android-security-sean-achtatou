package com.tencent.cloud.updaterec;

import android.content.Context;
import android.widget.ListView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.module.callback.ActionCallback;
import com.tencent.assistant.module.k;
import com.tencent.assistant.protocol.jce.CFTGetOneMoreAppResponse;
import com.tencent.assistant.protocol.jce.CardItem;
import com.tencent.assistant.utils.bz;
import java.util.ArrayList;
import java.util.Iterator;

/* compiled from: ProGuard */
public class a implements ActionCallback {
    public a(Context context, ListView listView) {
        a(context, listView);
    }

    public void a(Context context, ListView listView) {
        bz.a("OMAEDC_CTX", context);
        bz.a("OMAEDC_LV", listView);
    }

    public void a(int i, int i2, CFTGetOneMoreAppResponse cFTGetOneMoreAppResponse) {
        ArrayList<CardItem> arrayList;
        CardItem cardItem;
        Integer num;
        CardItem cardItem2;
        if (cFTGetOneMoreAppResponse != null && cFTGetOneMoreAppResponse.b != null && i2 == 0 && (arrayList = cFTGetOneMoreAppResponse.b) != null && arrayList.size() != 0) {
            if (arrayList.get(0).f1183a == 4 || arrayList.get(0).f1183a == 5 || arrayList.get(0).f1183a == 6) {
                cardItem = arrayList.get(0);
            } else if (arrayList.get(0).f1183a == 0) {
                ArrayList<CardItem> a2 = k.a(arrayList);
                if (a2.size() > 0) {
                    Iterator<CardItem> it = a2.iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            cardItem2 = null;
                            break;
                        }
                        cardItem2 = it.next();
                        if (!com.tencent.cloud.e.a.a(cardItem2.a().f1495a)) {
                            break;
                        }
                    }
                    cardItem = cardItem2;
                } else {
                    return;
                }
            } else {
                cardItem = null;
            }
            ListView listView = (ListView) bz.a("OMAEDC_LV");
            Context context = (Context) bz.a("OMAEDC_CTX");
            if (listView != null && context != null && cardItem != null) {
                int childCount = listView.getChildCount();
                int i3 = 0;
                while (i3 < childCount) {
                    f a3 = com.tencent.cloud.e.a.a(listView.getChildAt(i3).getTag());
                    if (a3 == null || a3.f2337a == null || a3.f2337a.getTag(R.id.one_more_list) == null || (num = (Integer) a3.f2337a.getTag(R.id.one_more_list)) == null || num.intValue() != i) {
                        i3++;
                    } else if (cardItem != null) {
                        com.tencent.cloud.e.a.a(a3.f, cardItem);
                        com.tencent.cloud.e.a.a(context, a3, cardItem);
                        return;
                    } else {
                        return;
                    }
                }
            }
        }
    }
}
