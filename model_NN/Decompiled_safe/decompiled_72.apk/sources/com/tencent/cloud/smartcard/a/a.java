package com.tencent.cloud.smartcard.a;

import android.content.Context;
import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.component.invalidater.IViewInvalidater;
import com.tencent.assistant.protocol.jce.SmartCardContentAggregationComplex;
import com.tencent.assistant.smartcard.c.c;
import com.tencent.assistant.smartcard.c.d;
import com.tencent.assistant.smartcard.c.z;
import com.tencent.assistant.smartcard.component.NormalSmartcardBaseItem;
import com.tencent.assistant.smartcard.component.as;
import com.tencent.assistant.smartcard.d.n;
import com.tencent.cloud.smartcard.b.b;
import com.tencent.cloud.smartcard.view.NormalSmartCardComplexCollectionView;

/* compiled from: ProGuard */
public class a extends c {
    public Class<? extends JceStruct> a() {
        return SmartCardContentAggregationComplex.class;
    }

    public com.tencent.assistant.smartcard.d.a b() {
        return new b();
    }

    /* access modifiers changed from: protected */
    public z c() {
        return new d();
    }

    public NormalSmartcardBaseItem a(Context context, n nVar, as asVar, IViewInvalidater iViewInvalidater) {
        return new NormalSmartCardComplexCollectionView(context, nVar, asVar, iViewInvalidater);
    }
}
