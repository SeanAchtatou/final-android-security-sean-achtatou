package com.tencent.cloud.smartcard.b;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.protocol.jce.ContentAggregationComplexItem;
import com.tencent.assistant.protocol.jce.SmartCardContentAggregationComplex;
import com.tencent.assistant.smartcard.d.a;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* compiled from: ProGuard */
public class b extends a {
    private ArrayList<a> e;

    public boolean a(byte b, JceStruct jceStruct) {
        if (jceStruct == null || !(jceStruct instanceof SmartCardContentAggregationComplex)) {
            return false;
        }
        SmartCardContentAggregationComplex smartCardContentAggregationComplex = (SmartCardContentAggregationComplex) jceStruct;
        if (smartCardContentAggregationComplex.c == null || smartCardContentAggregationComplex.c.size() < 3) {
            return false;
        }
        a(smartCardContentAggregationComplex.b);
        a(b, smartCardContentAggregationComplex.f1504a);
        this.e = new ArrayList<>(smartCardContentAggregationComplex.c.size());
        Iterator<ContentAggregationComplexItem> it = smartCardContentAggregationComplex.c.iterator();
        while (it.hasNext()) {
            this.e.add(new a(it.next()));
        }
        return true;
    }

    public ArrayList<a> h() {
        return this.e;
    }

    public List<SimpleAppModel> a() {
        if (this.e == null || this.e.size() <= 0) {
            return null;
        }
        ArrayList arrayList = new ArrayList(this.e.size());
        Iterator<a> it = this.e.iterator();
        while (it.hasNext()) {
            a next = it.next();
            if (next.b != null) {
                arrayList.add(next.b);
            }
        }
        return arrayList;
    }

    public void a(List<Long> list) {
        long j;
        if (this.e != null && this.e.size() > 0) {
            Iterator<a> it = this.e.iterator();
            while (it.hasNext()) {
                a next = it.next();
                if (next == null || next.b == null) {
                    j = -1;
                } else {
                    j = next.b.f938a;
                }
                if (j != -1 && list.contains(Long.valueOf(j))) {
                    it.remove();
                }
            }
        }
    }
}
