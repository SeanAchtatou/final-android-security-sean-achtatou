package com.tencent.cloud.adapter;

import com.tencent.assistantv2.component.e;

/* compiled from: ProGuard */
class f implements e {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ b f2215a;
    final /* synthetic */ long b;
    final /* synthetic */ AppUpdateIgnoreListAdapter c;

    f(AppUpdateIgnoreListAdapter appUpdateIgnoreListAdapter, b bVar, long j) {
        this.c = appUpdateIgnoreListAdapter;
        this.f2215a = bVar;
        this.b = j;
    }

    public void a(boolean z) {
        Long l = (Long) this.f2215a.j.getTag();
        if (l != null && this.b == l.longValue()) {
            if (!z) {
                this.f2215a.m.setVisibility(8);
                this.f2215a.i.setOnClickListener(null);
                return;
            }
            this.f2215a.m.setVisibility(0);
        }
    }
}
