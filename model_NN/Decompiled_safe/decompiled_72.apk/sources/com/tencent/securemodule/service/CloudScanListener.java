package com.tencent.securemodule.service;

import com.tencent.securemodule.impl.AppInfo;
import java.util.List;

public interface CloudScanListener {
    void onFinish(int i);

    void onRiskFoud(List<AppInfo> list);

    void onRiskFound();
}
