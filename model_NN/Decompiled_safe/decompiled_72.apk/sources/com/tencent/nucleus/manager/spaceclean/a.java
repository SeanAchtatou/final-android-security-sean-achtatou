package com.tencent.nucleus.manager.spaceclean;

import java.util.Comparator;

/* compiled from: ProGuard */
class a implements Comparator<n> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ BigFileAdapter f3013a;

    a(BigFileAdapter bigFileAdapter) {
        this.f3013a = bigFileAdapter;
    }

    /* renamed from: a */
    public int compare(n nVar, n nVar2) {
        if (nVar == null || nVar2 == null) {
            return 0;
        }
        return nVar.h - nVar2.h;
    }
}
