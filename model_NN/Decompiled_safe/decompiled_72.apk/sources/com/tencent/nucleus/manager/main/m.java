package com.tencent.nucleus.manager.main;

import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.ApkMgrActivity;
import com.tencent.assistant.activity.SpaceCleanActivity;
import com.tencent.assistant.activity.StartScanActivity;
import com.tencent.assistant.utils.ar;
import com.tencent.assistant.utils.at;
import com.tencent.assistantv2.model.a;
import com.tencent.connect.common.Constants;
import java.util.Random;

/* compiled from: ProGuard */
class m implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AssistantTabActivity f2955a;

    m(AssistantTabActivity assistantTabActivity) {
        this.f2955a = assistantTabActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.nucleus.manager.main.AssistantTabActivity.b(com.tencent.nucleus.manager.main.AssistantTabActivity, boolean):boolean
     arg types: [com.tencent.nucleus.manager.main.AssistantTabActivity, int]
     candidates:
      com.tencent.nucleus.manager.main.AssistantTabActivity.b(com.tencent.nucleus.manager.main.AssistantTabActivity, int):java.lang.String
      com.tencent.nucleus.manager.main.AssistantTabActivity.b(com.tencent.nucleus.manager.main.AssistantTabActivity, java.lang.String):void
      com.tencent.nucleus.manager.main.AssistantTabActivity.b(java.lang.String, java.lang.String):void
      com.tencent.nucleus.manager.main.AssistantTabActivity.b(com.tencent.nucleus.manager.main.AssistantTabActivity, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.nucleus.manager.main.AssistantTabActivity.c(com.tencent.nucleus.manager.main.AssistantTabActivity, boolean):void
     arg types: [com.tencent.nucleus.manager.main.AssistantTabActivity, int]
     candidates:
      com.tencent.nucleus.manager.main.AssistantTabActivity.c(com.tencent.nucleus.manager.main.AssistantTabActivity, int):int
      com.tencent.nucleus.manager.main.AssistantTabActivity.c(com.tencent.nucleus.manager.main.AssistantTabActivity, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.nucleus.manager.main.AssistantTabActivity.d(com.tencent.nucleus.manager.main.AssistantTabActivity, boolean):boolean
     arg types: [com.tencent.nucleus.manager.main.AssistantTabActivity, int]
     candidates:
      com.tencent.nucleus.manager.main.AssistantTabActivity.d(com.tencent.nucleus.manager.main.AssistantTabActivity, int):int
      com.tencent.nucleus.manager.main.AssistantTabActivity.d(com.tencent.nucleus.manager.main.AssistantTabActivity, boolean):boolean */
    public void run() {
        String string;
        this.f2955a.bi.removeMessages(7);
        boolean unused = this.f2955a.bg = false;
        this.f2955a.c(true);
        synchronized (this.f2955a.ai) {
            if (this.f2955a.am > 0) {
                if (this.f2955a.am > 0) {
                    string = this.f2955a.getString(R.string.manage_toast_apkDel, new Object[]{Integer.valueOf(this.f2955a.am), 10});
                } else {
                    string = this.f2955a.getString(R.string.manage_toast_apkDel_1, new Object[]{10});
                }
                a aVar = new a();
                aVar.b = string;
                aVar.f2019a = ApkMgrActivity.class;
                aVar.c = 10;
                this.f2955a.ah.add(aVar);
            }
            if (!ar.c() || (!this.f2955a.y().c() && !this.f2955a.aJ)) {
                String string2 = this.f2955a.getString(R.string.manage_toast_volClean, new Object[]{10});
                a aVar2 = new a();
                aVar2.b = string2;
                aVar2.f2019a = SpaceCleanActivity.class;
                aVar2.c = 10;
                this.f2955a.ah.add(aVar2);
            }
            if (this.f2955a.an > 0) {
                String string3 = this.f2955a.getString(R.string.manage_toast_safeScan, new Object[]{Integer.valueOf(this.f2955a.an), 10});
                a aVar3 = new a();
                aVar3.b = string3;
                aVar3.f2019a = StartScanActivity.class;
                aVar3.c = 10;
                this.f2955a.ah.add(aVar3);
            }
            if (this.f2955a.ah.size() <= 0) {
                int unused2 = this.f2955a.aE = 100;
                boolean unused3 = this.f2955a.M();
            } else if (!this.f2955a.aI || this.f2955a.F.b() != 0) {
                int unused4 = this.f2955a.aj = new Random().nextInt(this.f2955a.ah.size());
                int i = ((a) this.f2955a.ah.get(this.f2955a.aj)).c;
                String str = "05_001";
                Class unused5 = this.f2955a.ag = ((a) this.f2955a.ah.get(this.f2955a.aj)).f2019a;
                ar.a(i, this.f2955a.ag);
                int unused6 = this.f2955a.aE = 100 - i;
                if (this.f2955a.ag == ApkMgrActivity.class) {
                    this.f2955a.F.setTag(R.id.tma_st_slot_tag, "05_002");
                    str = "05_002";
                } else if (this.f2955a.ag == StartScanActivity.class) {
                    this.f2955a.F.setTag(R.id.tma_st_slot_tag, "05_003");
                    str = "05_003";
                } else if (this.f2955a.ag == SpaceCleanActivity.class) {
                    this.f2955a.F.setTag(R.id.tma_st_slot_tag, "05_001");
                    str = "05_001";
                }
                this.f2955a.c(((a) this.f2955a.ah.get(this.f2955a.aj)).b);
                this.f2955a.b(str, Constants.STR_EMPTY);
            }
        }
        if (this.f2955a.aI) {
            String string4 = this.f2955a.getString(R.string.manage_need_more);
            if (this.f2955a.aE == 100) {
                string4 = this.f2955a.getString(R.string.manage_great);
            }
            this.f2955a.b(string4);
        } else {
            boolean unused7 = this.f2955a.aI = true;
            String string5 = this.f2955a.getString(R.string.manage_opt_100);
            String str2 = "04_001";
            if (this.f2955a.ak > 0) {
                string5 = this.f2955a.getString(R.string.manage_tips_2, new Object[]{at.c(this.f2955a.ak)});
                str2 = "04_001";
            }
            if (this.f2955a.al > 0) {
                string5 = this.f2955a.getString(R.string.manage_tips_3, new Object[]{at.c(this.f2955a.al)});
            }
            if (this.f2955a.ak > 0 && this.f2955a.al > 0) {
                str2 = "04_002";
                string5 = this.f2955a.getString(R.string.manage_tips_1, new Object[]{at.c(this.f2955a.ak), at.c(this.f2955a.al)});
            }
            this.f2955a.b(string5);
            this.f2955a.b(str2, Constants.STR_EMPTY);
        }
        this.f2955a.D();
    }
}
