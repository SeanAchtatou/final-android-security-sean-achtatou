package com.tencent.nucleus.manager.floatingwindow;

import android.content.Context;
import android.os.Message;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.manager.t;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.ar;
import com.tencent.assistant.utils.at;
import com.tencent.assistant.utils.by;
import com.tencent.nucleus.manager.spaceclean.SpaceScanManager;

/* compiled from: ProGuard */
public class FloatWindowSmallView extends RelativeLayout implements UIEventListener {

    /* renamed from: a  reason: collision with root package name */
    public static int f2886a;
    public static int b;
    private long A = 0;
    private long B = 0;
    private boolean C = true;
    private Animation.AnimationListener D = new j(this);
    /* access modifiers changed from: private */
    public WindowManager c;
    /* access modifiers changed from: private */
    public RelativeLayout d;
    /* access modifiers changed from: private */
    public ImageView e;
    private TextView f;
    /* access modifiers changed from: private */
    public WindowManager.LayoutParams g;
    private RelativeLayout.LayoutParams h;
    private float i;
    private float j;
    private float k;
    private float l;
    private float m;
    private float n;
    private int o;
    private boolean p;
    private Animation q;
    private boolean r = false;
    /* access modifiers changed from: private */
    public boolean s = true;
    private boolean t = true;
    /* access modifiers changed from: private */
    public boolean u = true;
    /* access modifiers changed from: private */
    public boolean v = true;
    private boolean w = false;
    private long x;
    private int y = 1;
    private boolean z = false;

    public FloatWindowSmallView(Context context) {
        super(context);
        this.c = (WindowManager) context.getSystemService("window");
        try {
            LayoutInflater.from(context).inflate((int) R.layout.float_window_small, this);
            this.d = (RelativeLayout) findViewById(R.id.small_window_layout);
            f2886a = (int) context.getResources().getDimension(R.dimen.floating_small_window_width);
            b = (int) context.getResources().getDimension(R.dimen.floating_small_window_height);
            this.h = new RelativeLayout.LayoutParams(-2, -2);
            this.e = (ImageView) findViewById(R.id.logo_img);
            this.o = (int) context.getResources().getDimension(R.dimen.floating_window_logo_width);
            this.f = (TextView) findViewById(R.id.percent);
            this.f.setText(n.a().k());
            c();
            d();
        } catch (Throwable th) {
            t.a().b();
            this.C = false;
            setVisibility(8);
        }
    }

    private void c() {
        if (this.q == null) {
            this.q = new RotateAnimation(359.0f, 0.0f, 1, 0.5f, 1, 0.5f);
            this.q.setInterpolator(new LinearInterpolator());
            this.q.setDuration(800);
            this.q.setRepeatCount(-1);
        }
    }

    private void d() {
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_MGR_MEMORY_CLEAN_SUCCESS, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_MGR_MEMORY_CLEAN_FAIL, this);
    }

    public void a() {
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_MGR_MEMORY_CLEAN_SUCCESS, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_MGR_MEMORY_CLEAN_FAIL, this);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (this.C && this.s && this.u && this.v) {
            switch (motionEvent.getAction()) {
                case 0:
                    this.p = true;
                    this.m = motionEvent.getX();
                    this.n = motionEvent.getY();
                    this.k = motionEvent.getRawX();
                    this.l = motionEvent.getRawY() - ((float) by.a());
                    this.i = motionEvent.getRawX();
                    this.j = motionEvent.getRawY() - ((float) by.a());
                    this.A = System.currentTimeMillis();
                    this.y = ((int) Math.sqrt(Math.pow((double) (this.k - ((float) (by.b() / 2))), 2.0d) + Math.pow((double) (((float) by.c()) - this.l), 2.0d))) / 2;
                    break;
                case 1:
                    this.d.setVisibility(8);
                    this.p = false;
                    if (!n.a().j()) {
                        f();
                        if (Math.abs(this.k - this.i) < 12.0f && Math.abs(this.l - this.j) < 12.0f) {
                            h();
                            break;
                        } else {
                            g();
                            break;
                        }
                    } else {
                        e();
                        break;
                    }
                    break;
                case 2:
                    this.B = System.currentTimeMillis();
                    this.i = motionEvent.getRawX();
                    this.j = motionEvent.getRawY() - ((float) by.a());
                    if (Math.abs(this.k - this.i) >= 12.0f || Math.abs(this.l - this.j) >= 12.0f || this.B - this.A >= 400) {
                        g();
                        f();
                        break;
                    }
            }
        }
        return true;
    }

    public int b() {
        return this.y;
    }

    public void a(WindowManager.LayoutParams layoutParams) {
        this.g = layoutParams;
        if (layoutParams != null && this.d != null) {
            try {
                if (ar.d() < 0.85f) {
                    if (this.g.x >= by.b() / 2) {
                        this.d.setBackgroundResource(R.drawable.admin_launch_button_right);
                    } else {
                        this.d.setBackgroundResource(R.drawable.admin_launch_button_left);
                    }
                } else if (this.g.x >= by.b() / 2) {
                    this.d.setBackgroundResource(R.drawable.admin_launch_button_right_orange);
                } else {
                    this.d.setBackgroundResource(R.drawable.admin_launch_button_left_orange);
                }
            } catch (Throwable th) {
                XLog.e("floatingwindow", "FloatWindowSmallView >> <setParams> throws exception.");
            }
        }
    }

    private void e() {
        if (this.C) {
            XLog.d("floatingwindow", "launchRocket");
            this.s = false;
            this.u = false;
            this.v = false;
            this.t = false;
            this.w = true;
            this.x = 0;
            n.a().a(STConst.ST_PAGE_DRAG_TO_ACCELERATE_PAGEID, STConst.ST_DEFAULT_SLOT, 100);
            n.a().a("FloatWindowAccelerate", "SmallWindow");
            SpaceScanManager.a().p();
            this.g.width = f2886a;
            this.g.height = b;
            this.g.x = (int) (this.k - this.m);
            this.g.y = (int) (this.l - this.n);
            n.a().a(this.D);
        }
    }

    private void f() {
        if (this.C && n.a().h()) {
            int i2 = ((int) (this.j - this.n)) - this.o;
            if (i2 > (by.c() - by.a()) - this.h.height) {
                i2 = (by.c() - by.a()) - this.h.height;
            } else if (i2 < 0) {
                i2 = 0;
            }
            if (!this.p) {
                int b2 = by.b();
                if (this.i >= ((float) (b2 / 2))) {
                    try {
                        this.d.setBackgroundResource(R.drawable.admin_launch_button_right);
                    } catch (Throwable th) {
                        XLog.e("floatingwindow", "FloatWindowSmallView >> <updateViewPosition> exception");
                    }
                } else {
                    try {
                        this.d.setBackgroundResource(R.drawable.admin_launch_button_left);
                    } catch (Throwable th2) {
                        XLog.e("floatingwindow", "FloatWindowSmallView >> <updateViewPosition> exception");
                    }
                    b2 = 0;
                }
                this.g.width = f2886a;
                this.g.height = b;
                this.g.y = (int) (this.j - this.n);
                this.g.x = (int) (((float) b2) - this.m);
                try {
                    this.c.updateViewLayout(this, this.g);
                } catch (Throwable th3) {
                    th3.printStackTrace();
                    XLog.d("floatingwindow", "FloatWindowSmallView >> <updateViewPosition> updateViewLayout exception");
                    return;
                }
            } else {
                this.g.width = this.o;
                this.g.height = this.o;
                int i3 = ((int) (((float) ((int) this.i)) - this.m)) - (this.o / 2);
                if (i3 > by.b() - this.h.width) {
                    i3 = by.b() - this.h.width;
                } else if (i3 < 0) {
                    i3 = 0;
                }
                this.g.y = i2;
                this.g.x = i3;
                try {
                    this.c.updateViewLayout(this, this.g);
                } catch (Throwable th4) {
                    th4.printStackTrace();
                    XLog.d("floatingwindow", "FloatWindowSmallView >> <updateViewPosition> updateViewLayout exception");
                    return;
                }
            }
            if (n.a().j()) {
                setVisibility(4);
                if (!this.z) {
                    n.a().a(((int) (this.i - this.m)) - (this.o / 2), i2);
                    this.z = true;
                }
            } else {
                setVisibility(0);
                if (this.z) {
                    n.a().m();
                    this.z = false;
                }
            }
            n.a().a(this.z, (int) Math.sqrt(Math.pow((double) ((int) Math.abs(this.j - this.l)), 2.0d) + Math.pow((double) ((int) Math.abs(this.i - this.k)), 2.0d)));
        }
    }

    /* access modifiers changed from: private */
    public void g() {
        if (this.C) {
            if (this.p && this.e.getVisibility() != 0) {
                this.d.setVisibility(8);
                this.e.setVisibility(0);
                if (!this.r && this.q != null) {
                    this.e.startAnimation(this.q);
                    this.r = true;
                }
                n.a().e();
            } else if (!this.p) {
                this.d.setVisibility(0);
                if (this.r) {
                    this.e.clearAnimation();
                    this.r = false;
                }
                this.e.setVisibility(8);
                n.a().f();
            }
        }
    }

    private void h() {
        n.a().c(getContext());
        n.a().b(getContext());
        n.a().f();
        n.a().a(STConst.ST_PAGE_FLOAT_BIG_WINDOW_PAGEID, STConst.ST_DEFAULT_SLOT, 100);
    }

    public void handleUIEvent(Message message) {
        if (this.w) {
            switch (message.what) {
                case EventDispatcherEnum.UI_EVENT_MGR_MEMORY_CLEAN_SUCCESS:
                    this.s = true;
                    this.t = true;
                    this.x = ((Long) message.obj).longValue();
                    if (this.u) {
                        i();
                    } else {
                        this.d.setVisibility(8);
                    }
                    XLog.d("floatingwindow", "加速成功，释放内存：" + at.c(this.x));
                    return;
                case EventDispatcherEnum.UI_EVENT_MGR_MEMORY_CLEAN_FAIL:
                    this.s = true;
                    this.t = false;
                    if (this.u) {
                        i();
                        return;
                    } else {
                        this.d.setVisibility(8);
                        return;
                    }
                default:
                    return;
            }
        }
    }

    /* access modifiers changed from: private */
    public void i() {
        String string;
        SpannableString spannableString = null;
        if (this.C) {
            if (!this.t) {
                string = AstApp.i().getResources().getString(R.string.floating_window_do_well_tips);
            } else if (n.a().a(this.x)) {
                string = AstApp.i().getResources().getString(R.string.floating_window_best_state_tips);
            } else {
                string = AstApp.i().getResources().getString(R.string.floating_window_result_speed_up_tips);
                String string2 = AstApp.i().getResources().getString(R.string.floating_window_result_release_memory_tips, at.c(this.x));
                spannableString = new SpannableString(string2);
                spannableString.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.floating_winddow_result_text_color_highlight)), 0, string2.indexOf("内"), 33);
            }
            n.a().a(string, spannableString);
            postDelayed(new k(this), 3000);
        }
    }

    /* access modifiers changed from: private */
    public void j() {
        TranslateAnimation translateAnimation;
        if (this.C && this.d != null) {
            this.f.setText(n.a().k());
            this.d.setVisibility(4);
            if (this.g.x >= by.b() / 2) {
                try {
                    this.d.setBackgroundResource(R.drawable.admin_launch_button_right);
                } catch (Throwable th) {
                    XLog.e("floatingwindow", "FloatWindowSmallView >> <startSmallWindowShowAnim> throws exception.");
                }
                translateAnimation = new TranslateAnimation(1, 1.0f, 1, 0.0f, 1, 0.0f, 1, 0.0f);
            } else {
                try {
                    this.d.setBackgroundResource(R.drawable.admin_launch_button_left);
                } catch (Throwable th2) {
                    XLog.e("floatingwindow", "FloatWindowSmallView >> <startSmallWindowShowAnim> throws exception.");
                }
                translateAnimation = new TranslateAnimation(1, -1.0f, 1, 0.0f, 1, 0.0f, 1, 0.0f);
            }
            translateAnimation.setDuration(300);
            translateAnimation.setAnimationListener(new m(this));
            this.d.startAnimation(translateAnimation);
        }
    }
}
