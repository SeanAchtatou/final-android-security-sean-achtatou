package com.tencent.nucleus.manager.usagestats;

import android.text.TextUtils;
import com.tencent.assistant.utils.XLog;
import com.tencent.nucleus.manager.usagestats.UsagestatsSTManager;

/* compiled from: ProGuard */
class p implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ o f3073a;

    p(o oVar) {
        this.f3073a = oVar;
    }

    public void run() {
        if (!TextUtils.isEmpty(this.f3073a.m)) {
            XLog.e("usagestats", "getStatOtherAppList execute timeout !!!, " + this.f3073a.m);
            UsagestatsSTManager.a().a("app_usage_r_report_timely", this.f3073a.m + "_block", null, UsagestatsSTManager.ReportType.timely);
        }
    }
}
