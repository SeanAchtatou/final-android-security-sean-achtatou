package com.tencent.nucleus.manager.appbackup;

import android.os.Bundle;
import android.os.Message;
import com.qq.AppService.AstApp;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.protocol.jce.BackupApp;
import com.tencent.assistant.utils.ah;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class p implements UIEventListener {

    /* renamed from: a  reason: collision with root package name */
    private static p f2812a;
    private static int[] f = {EventDispatcherEnum.UI_EVENT_LOGIN_SUCCESS, EventDispatcherEnum.UI_EVENT_GET_BACKUP_DEVICELIST_SUCCESS, EventDispatcherEnum.UI_EVENT_GET_BACKUP_DEVICELIST_FAIL, EventDispatcherEnum.UI_EVENT_GET_BACKUP_APPS_SUCCESS, EventDispatcherEnum.UI_EVENT_GET_BACKUP_APPS_FAIL};
    private t b;
    private s c;
    private int d = -1;
    private int e = -1;

    private p() {
    }

    public static synchronized p a() {
        p pVar;
        synchronized (p.class) {
            if (f2812a == null) {
                f2812a = new p();
            }
            pVar = f2812a;
        }
        return pVar;
    }

    public void b() {
        c();
        ah.a("BackupPushManagerHandler").postDelayed(new q(this), 5000);
    }

    private void c() {
        for (int addUIEventListener : f) {
            try {
                AstApp.i().k().addUIEventListener(addUIEventListener, this);
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    public void handleUIEvent(Message message) {
        switch (message.what) {
            case EventDispatcherEnum.UI_EVENT_LOGIN_SUCCESS:
                Bundle bundle = (Bundle) message.obj;
                if (bundle != null && bundle.getInt(AppConst.KEY_FROM_TYPE) != 10) {
                    if (message.arg1 != AppConst.LoginEgnineType.ENGINE_MOBILE_QQ.ordinal() || message.arg2 != 1) {
                        if (o.c() == 0 || o.b() == 0) {
                            d();
                            return;
                        }
                        return;
                    }
                    return;
                }
                return;
            case EventDispatcherEnum.UI_EVENT_GET_BACKUP_DEVICELIST_SUCCESS:
                if (message.arg1 == this.d) {
                    a(true);
                    return;
                }
                return;
            case EventDispatcherEnum.UI_EVENT_GET_BACKUP_DEVICELIST_FAIL:
                if (message.arg1 == this.d) {
                    a(false);
                    return;
                }
                return;
            case EventDispatcherEnum.UI_EVENT_GET_BACKUP_APPS_SUCCESS:
                if (message.arg1 == this.e) {
                    b(true);
                    return;
                }
                return;
            case EventDispatcherEnum.UI_EVENT_GET_BACKUP_APPS_FAIL:
                if (message.arg1 == this.e) {
                    b(false);
                    return;
                }
                return;
            default:
                return;
        }
    }

    private void a(boolean z) {
        if (this.b != null && this.b.c() && this.b.d() && !o.g() && !o.h()) {
            if (this.c == null) {
                this.c = new s();
            }
            this.e = this.c.a(this.b.e());
        }
        this.b = null;
    }

    private void b(boolean z) {
        ArrayList<BackupApp> a2 = this.c.a();
        if (a2 == null || a2.size() >= 10) {
        }
        this.c = null;
    }

    private void d() {
        if (this.b == null) {
            this.b = new t();
        }
        this.d = this.b.a();
    }
}
