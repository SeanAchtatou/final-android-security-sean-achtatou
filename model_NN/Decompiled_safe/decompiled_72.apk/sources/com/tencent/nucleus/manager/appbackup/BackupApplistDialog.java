package com.tencent.nucleus.manager.appbackup;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;

/* compiled from: ProGuard */
public class BackupApplistDialog extends Dialog {

    /* renamed from: a  reason: collision with root package name */
    TextView f2797a;
    TextView b;
    private ListView c;
    /* access modifiers changed from: private */
    public BackupAppListAdapter d;
    private ViewGroup e;
    /* access modifiers changed from: private */
    public TextView f;
    private ImageView g;
    private int h;
    /* access modifiers changed from: private */
    public k i;
    /* access modifiers changed from: private */
    public boolean j = false;

    public BackupApplistDialog(Context context) {
        super(context, R.style.dialog);
        setCancelable(true);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.backup_applist_layout);
        Window window = getWindow();
        WindowManager.LayoutParams attributes = window.getAttributes();
        attributes.gravity = 17;
        Display defaultDisplay = getWindow().getWindowManager().getDefaultDisplay();
        attributes.width = (int) (((double) defaultDisplay.getWidth()) * 0.919d);
        attributes.height = (int) (((double) defaultDisplay.getHeight()) * 0.8d);
        window.setAttributes(attributes);
        c();
    }

    /* access modifiers changed from: private */
    public void b() {
        f a2 = this.d.a();
        if (a2 != null) {
            b(a2.f2805a, a2.b, a2.c);
        } else {
            b(0, false, "0M");
        }
    }

    private void c() {
        this.c = (ListView) findViewById(R.id.listview);
        this.c.setDivider(null);
        RelativeLayout relativeLayout = (RelativeLayout) LayoutInflater.from(getContext()).inflate((int) R.layout.backup_app_item_ex, (ViewGroup) null);
        ((TextView) relativeLayout.findViewById(R.id.has_installed_num)).setText(String.format(getContext().getString(R.string.backup_installed_apps), Integer.valueOf(this.h)));
        this.c.addFooterView(relativeLayout);
        if (this.d != null) {
            this.c.setAdapter((ListAdapter) this.d);
        }
        this.g = (ImageView) findViewById(R.id.close);
        this.g.setOnClickListener(new h(this));
        this.e = (ViewGroup) findViewById(R.id.footer);
        this.f2797a = (TextView) this.e.findViewById(R.id.text);
        this.b = (TextView) this.e.findViewById(R.id.extra_text);
        this.e.setOnClickListener(new i(this));
        this.f = (TextView) findViewById(R.id.select_all_checkbox);
        this.f.setOnClickListener(new j(this));
        b();
    }

    public void a(int i2) {
        this.h = i2;
    }

    public void a(BackupAppListAdapter backupAppListAdapter) {
        this.d = backupAppListAdapter;
        this.d.a(this);
        if (this.c != null) {
            this.c.setAdapter((ListAdapter) this.d);
        }
    }

    public void a(k kVar) {
        this.i = kVar;
    }

    public BackupAppListAdapter a() {
        return this.d;
    }

    public void a(int i2, boolean z, String str) {
        this.j = true;
        b(i2, z, str);
    }

    private void b(int i2, boolean z, String str) {
        int i3;
        if (z) {
            this.f.setSelected(true);
        } else {
            this.f.setSelected(false);
        }
        if (this.j) {
            i3 = R.string.restore_manual;
        } else {
            i3 = R.string.restore_smart;
        }
        if (i2 <= 0) {
            this.e.setEnabled(false);
            this.f2797a.setEnabled(false);
            this.b.setEnabled(false);
            this.b.setVisibility(8);
            this.f2797a.setText(i3);
            this.b.setText(" " + String.format(getContext().getString(R.string.restore_extra_msg), 0, "0M"));
            return;
        }
        this.e.setEnabled(true);
        this.f2797a.setEnabled(true);
        this.b.setEnabled(true);
        this.f2797a.setText(i3);
        this.b.setText(" " + String.format(getContext().getString(R.string.restore_extra_msg), Integer.valueOf(i2), str));
        this.b.setVisibility(0);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        if (this.d != null) {
            this.d.d();
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        if (this.d != null) {
            this.d.c();
        }
    }
}
