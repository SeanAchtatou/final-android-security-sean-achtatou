package com.tencent.nucleus.manager.appbackup;

import android.content.Context;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.AppIconView;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.model.d;
import com.tencent.assistant.module.k;
import com.tencent.assistant.protocol.jce.BackupApp;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.ah;
import com.tencent.assistant.utils.at;
import com.tencent.assistant.utils.bm;
import com.tencent.assistantv2.component.DownloadButton;
import com.tencent.assistantv2.component.ListItemInfoView;
import com.tencent.assistantv2.component.j;
import com.tencent.assistantv2.st.model.StatInfo;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.pangu.download.DownloadInfo;
import java.util.ArrayList;
import java.util.Iterator;

/* compiled from: ProGuard */
public class BackupAppListAdapter extends BaseAdapter implements UIEventListener {

    /* renamed from: a  reason: collision with root package name */
    private Context f2796a;
    private ArrayList<BackupApp> b;
    /* access modifiers changed from: private */
    public BackupApplistDialog c;
    private int d = STConst.ST_PAGE_APP_BACKUP_APPLIST;
    /* access modifiers changed from: private */
    public boolean[] e;

    public BackupAppListAdapter(Context context) {
        this.f2796a = context;
    }

    public int getCount() {
        if (this.b == null) {
            return 0;
        }
        return this.b.size();
    }

    /* renamed from: a */
    public BackupApp getItem(int i) {
        if (this.b == null || this.b.size() == 0) {
            return null;
        }
        return this.b.get(i);
    }

    public long getItemId(int i) {
        return 0;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        g gVar;
        if (view == null || view.getTag() == null || !(view.getTag() instanceof g)) {
            view = View.inflate(this.f2796a, R.layout.backup_app_item, null);
            g gVar2 = new g(null);
            gVar2.f2806a = view;
            gVar2.g = (DownloadButton) view.findViewById(R.id.down_btn);
            gVar2.f = (TextView) view.findViewById(R.id.description);
            gVar2.c = (AppIconView) view.findViewById(R.id.app_icon_img);
            gVar2.d = (TextView) view.findViewById(R.id.app_name);
            gVar2.i = (AppBackupAppItemInfo) view.findViewById(R.id.sub_layout);
            gVar2.e = (TextView) view.findViewById(R.id.apk_size);
            gVar2.h = (ListItemInfoView) view.findViewById(R.id.download_item_info);
            gVar2.b = (TextView) view.findViewById(R.id.select_image);
            view.setTag(gVar2);
            gVar = gVar2;
        } else {
            gVar = (g) view.getTag();
        }
        a(gVar, i);
        return view;
    }

    private void a(g gVar, int i) {
        BackupApp a2 = getItem(i);
        if (a2 != null) {
            SimpleAppModel a3 = k.a(a2);
            gVar.b.setSelected(this.e[i]);
            c cVar = new c(this, gVar.b, i);
            gVar.b.setOnClickListener(cVar);
            gVar.f2806a.setOnClickListener(cVar);
            if (!TextUtils.isEmpty(a2.h)) {
                gVar.f.setVisibility(0);
                gVar.f.setText(a2.h);
            } else {
                gVar.f.setVisibility(8);
            }
            gVar.d.setText(a2.c);
            gVar.g.a(a3);
            gVar.h.a(a3);
            gVar.h.a(ListItemInfoView.InfoType.DOWNLOAD_PROGRESS_ONLY);
            gVar.i.a(a3);
            a(a3, gVar.h, gVar.i);
            gVar.g.setClickable(true);
            STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f2796a, a3, b(i), 200, null);
            gVar.g.a(buildSTInfo, (j) null, (d) null, gVar.g, gVar.h);
            gVar.c.setSimpleAppModel(a3, new StatInfo(a3.b, this.d, 0, null, 0), -100);
            gVar.c.getIconImageView().setClickable(false);
            gVar.c.setClickable(false);
            gVar.e.setText(at.a(a3.k));
        }
    }

    private void a(SimpleAppModel simpleAppModel, ListItemInfoView listItemInfoView, LinearLayout linearLayout) {
        switch (e.f2804a[k.d(simpleAppModel).ordinal()]) {
            case 1:
            case 2:
            case 3:
            case 4:
                linearLayout.setVisibility(8);
                listItemInfoView.setVisibility(0);
                return;
            default:
                linearLayout.setVisibility(0);
                listItemInfoView.setVisibility(8);
                return;
        }
    }

    private String b(int i) {
        return "03_" + bm.a(i + 1);
    }

    public void a(ArrayList<BackupApp> arrayList) {
        this.b = arrayList;
        if (arrayList != null) {
            this.e = new boolean[arrayList.size()];
            Iterator<BackupApp> it = arrayList.iterator();
            int i = 0;
            while (it.hasNext()) {
                int i2 = i + 1;
                this.e[i] = it.next().g == 1;
                i = i2;
            }
        }
    }

    public void a(boolean z) {
        if (this.e != null) {
            int length = this.e.length;
            for (int i = 0; i < length; i++) {
                this.e[i] = z;
            }
        }
        notifyDataSetChanged();
    }

    public f a() {
        int i;
        boolean z;
        if (this.e == null) {
            return null;
        }
        long j = 0;
        boolean z2 = true;
        int length = this.e.length;
        int i2 = 0;
        int i3 = 0;
        while (i2 < length) {
            if (this.e[i2]) {
                int i4 = i3 + 1;
                j += this.b.get(i2).m;
                z = z2;
                i = i4;
            } else {
                i = i3;
                z = false;
            }
            i2++;
            boolean z3 = z;
            i3 = i;
            z2 = z3;
        }
        return new f(i3, z2, at.a(j), null);
    }

    public ArrayList<BackupApp> b() {
        ArrayList<BackupApp> arrayList = new ArrayList<>();
        if (!(this.b == null || this.e == null)) {
            int size = this.b.size();
            for (int i = 0; i < size; i++) {
                if (this.e[i]) {
                    arrayList.add(this.b.get(i));
                }
            }
        }
        return arrayList;
    }

    public void a(BackupApplistDialog backupApplistDialog) {
        this.c = backupApplistDialog;
    }

    public void c() {
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE, this);
        AstApp.i().k().removeUIEventListener(1016, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_DOWNLOADLIST_READY, this);
    }

    public void d() {
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE, this);
        AstApp.i().k().addUIEventListener(1016, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_DOWNLOADLIST_READY, this);
    }

    public void handleUIEvent(Message message) {
        DownloadInfo downloadInfo;
        switch (message.what) {
            case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE:
                if (message.obj instanceof DownloadInfo) {
                    DownloadInfo downloadInfo2 = (DownloadInfo) message.obj;
                    if (downloadInfo2 != null && !TextUtils.isEmpty(downloadInfo2.downloadTicket)) {
                        downloadInfo = downloadInfo2;
                    } else {
                        return;
                    }
                } else {
                    downloadInfo = null;
                }
                Iterator<BackupApp> it = this.b.iterator();
                while (it.hasNext()) {
                    SimpleAppModel a2 = k.a(it.next());
                    if (downloadInfo != null && a2 != null && a2.q().equals(downloadInfo.downloadTicket)) {
                        e();
                        return;
                    }
                }
                return;
            case 1016:
                e();
                return;
            case EventDispatcherEnum.UI_EVENT_DOWNLOADLIST_READY:
                e();
                return;
            default:
                return;
        }
    }

    private void e() {
        ah.a().post(new d(this));
    }
}
