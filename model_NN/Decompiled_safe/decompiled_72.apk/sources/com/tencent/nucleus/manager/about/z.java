package com.tencent.nucleus.manager.about;

import android.view.inputmethod.InputMethodManager;
import java.util.TimerTask;

/* compiled from: ProGuard */
class z extends TimerTask {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ HelperFeedbackActivity f2762a;

    z(HelperFeedbackActivity helperFeedbackActivity) {
        this.f2762a = helperFeedbackActivity;
    }

    public void run() {
        try {
            ((InputMethodManager) this.f2762a.getSystemService("input_method")).toggleSoftInput(0, 2);
        } catch (Throwable th) {
        }
    }
}
