package com.tencent.nucleus.manager.floatingwindow;

import android.app.Service;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.os.Handler;
import android.os.IBinder;
import com.qq.AppService.AstApp;
import com.qq.ndk.NativeFileObject;
import com.tencent.assistant.Global;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.m;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.t;
import com.tencent.beacon.event.a;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/* compiled from: ProGuard */
public class FloatingWindowService extends Service {

    /* renamed from: a  reason: collision with root package name */
    private Handler f2887a = new Handler();
    private ScheduledExecutorService b;
    private List<String> c = new ArrayList();
    /* access modifiers changed from: private */
    public List<String> d = new ArrayList();
    private long e = 0;
    private Runnable f = new t(this);

    public IBinder onBind(Intent intent) {
        return null;
    }

    public int onStartCommand(Intent intent, int i, int i2) {
        XLog.d("floatingwindow", "FloatingWindowService >> onStartCommand..");
        if (this.b == null) {
            try {
                this.b = Executors.newSingleThreadScheduledExecutor();
                this.b.scheduleAtFixedRate(this.f, 0, 1500, TimeUnit.MILLISECONDS);
            } catch (Throwable th) {
            }
        }
        if (intent == null) {
            return 2;
        }
        return super.onStartCommand(intent, i, i2);
    }

    public void onDestroy() {
        super.onDestroy();
        if (this.b != null) {
            try {
                this.b.shutdown();
                this.b = null;
            } catch (Throwable th) {
            }
        }
    }

    /* access modifiers changed from: private */
    public void a() {
        if (m.a().o()) {
            boolean c2 = c();
            if (c2 && !n.a().h()) {
                this.f2887a.post(new v(this));
            } else if (!c2 && n.a().h()) {
                this.f2887a.post(new w(this));
            } else if (c2 && n.a().h()) {
                long currentTimeMillis = System.currentTimeMillis();
                if (this.e == 0 || currentTimeMillis - this.e >= 8999) {
                    this.e = currentTimeMillis;
                    this.f2887a.post(new x(this));
                }
            }
        } else if (n.a().h()) {
            this.f2887a.post(new u(this));
        }
    }

    /* access modifiers changed from: private */
    public void b() {
        if (!AstApp.i().l()) {
            if (this.c.isEmpty() || !this.c.equals(this.d)) {
                this.c.clear();
                this.c.addAll(this.d);
                ApkResourceManager.getInstance().updateAppLauncherTime(this.d);
            }
        }
    }

    private boolean c() {
        List<String> d2 = d();
        for (int i = 0; i < d2.size(); i++) {
            if (this.d.contains(d2.get(i))) {
                return true;
            }
        }
        return false;
    }

    private List<String> d() {
        int i;
        ArrayList arrayList = new ArrayList();
        try {
            Intent intent = new Intent("android.intent.action.MAIN");
            intent.addCategory("android.intent.category.HOME");
            for (ResolveInfo resolveInfo : AstApp.i().getPackageManager().queryIntentActivities(intent, NativeFileObject.S_IFIFO)) {
                arrayList.add(resolveInfo.activityInfo.packageName);
            }
        } catch (Throwable th) {
            XLog.e("floatingwindow", "FloatingWindowService >> <getHomes> getInstalledPackages throws a exception.");
            i = 0;
        }
        return arrayList;
        a(i);
        return arrayList;
    }

    private void a(int i) {
        HashMap hashMap = new HashMap();
        hashMap.put("B1", String.valueOf(i));
        hashMap.put("B2", Global.getPhoneGuidAndGen());
        hashMap.put("B3", Global.getQUAForBeacon());
        hashMap.put("B4", t.g());
        XLog.d("beacon", "beacon report >> FloatWindowQueryIntentActivityFail. " + hashMap.toString());
        a.a("FloatWindowQueryIntentActivityFail", i != 0, -1, -1, hashMap, true);
    }
}
