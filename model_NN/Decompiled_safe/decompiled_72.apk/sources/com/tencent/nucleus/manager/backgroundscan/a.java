package com.tencent.nucleus.manager.backgroundscan;

import android.content.Intent;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.open.SocialConstants;

/* compiled from: ProGuard */
public class a {

    /* renamed from: a  reason: collision with root package name */
    private static a f2822a;

    public static synchronized a a() {
        a aVar;
        synchronized (a.class) {
            if (f2822a == null) {
                f2822a = new a();
            }
            aVar = f2822a;
        }
        return aVar;
    }

    public void a(int i, String str, String str2) {
        Intent intent = new Intent();
        intent.putExtra(SocialConstants.PARAM_TYPE, (byte) i);
        intent.putExtra("contentTitle", str);
        intent.putExtra("contentText", str2);
        a(intent);
    }

    public void b(int i, String str, String str2) {
        Intent intent = new Intent();
        intent.putExtra(SocialConstants.PARAM_TYPE, (byte) i);
        intent.putExtra("contentTitle", str);
        intent.putExtra("contentText", str2);
        b(intent);
    }

    private void a(Intent intent) {
        TemporaryThreadManager.get().start(new b(this, intent));
    }

    private void b(Intent intent) {
        TemporaryThreadManager.get().start(new c(this, intent));
    }
}
