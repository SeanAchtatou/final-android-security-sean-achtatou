package com.tencent.nucleus.manager.component;

import android.animation.ValueAnimator;
import android.widget.RelativeLayout;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.by;

/* compiled from: ProGuard */
class ac implements ValueAnimator.AnimatorUpdateListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ TxManagerCommContainView f2849a;

    ac(TxManagerCommContainView txManagerCommContainView) {
        this.f2849a = txManagerCommContainView;
    }

    public void onAnimationUpdate(ValueAnimator valueAnimator) {
        float floatValue = ((Float) valueAnimator.getAnimatedValue()).floatValue();
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) this.f2849a.h.getLayoutParams();
        layoutParams.height = (int) ((1.0f - floatValue) * ((float) (this.f2849a.getMeasuredHeight() - this.f2849a.j.getMeasuredHeight())));
        this.f2849a.a(floatValue / TxManagerCommContainView.k);
        RelativeLayout.LayoutParams layoutParams2 = (RelativeLayout.LayoutParams) this.f2849a.i.getLayoutParams();
        layoutParams2.height = (this.f2849a.getMeasuredHeight() - this.f2849a.j.getMeasuredHeight()) - layoutParams.height;
        float measuredHeight = (float) this.f2849a.h.getMeasuredHeight();
        XLog.d("Donaldxuuu", "topHeight = " + measuredHeight + " marginTop = ");
        if ((measuredHeight - ((float) by.a(this.f2849a.getContext(), 144.0f))) / 2.0f < ((float) by.a(this.f2849a.getContext(), 140.0f))) {
            if (this.f2849a.y) {
                RelativeLayout.LayoutParams layoutParams3 = (RelativeLayout.LayoutParams) this.f2849a.x.getLayoutParams();
                layoutParams3.addRule(13);
                layoutParams3.width = by.a(this.f2849a.getContext(), 144.0f);
                layoutParams3.height = by.a(this.f2849a.getContext(), 144.0f);
                layoutParams3.topMargin = 0;
                layoutParams3.bottomMargin = 0;
                this.f2849a.x.setLayoutParams(layoutParams3);
                boolean unused = this.f2849a.y = false;
            }
            float c = 1.0f - ((floatValue / TxManagerCommContainView.k) * (1.0f - TxManagerCommContainView.l));
            this.f2849a.x.setScaleX(c);
            this.f2849a.x.setScaleY(c);
        }
        this.f2849a.h.setLayoutParams(layoutParams);
        this.f2849a.i.setLayoutParams(layoutParams2);
    }
}
