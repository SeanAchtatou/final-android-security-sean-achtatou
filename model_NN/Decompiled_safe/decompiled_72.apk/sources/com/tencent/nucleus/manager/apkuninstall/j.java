package com.tencent.nucleus.manager.apkuninstall;

import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class j extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ l f2792a;
    final /* synthetic */ LocalApkInfo b;
    final /* synthetic */ STInfoV2 c;
    final /* synthetic */ UserInstalledAppListAdapter d;

    j(UserInstalledAppListAdapter userInstalledAppListAdapter, l lVar, LocalApkInfo localApkInfo, STInfoV2 sTInfoV2) {
        this.d = userInstalledAppListAdapter;
        this.f2792a = lVar;
        this.b = localApkInfo;
        this.c = sTInfoV2;
    }

    public void onTMAClick(View view) {
        this.f2792a.e.setSelected(!this.f2792a.e.isSelected());
        this.b.mIsSelect = this.f2792a.e.isSelected();
        if (this.d.g != null) {
            this.d.g.sendMessage(this.d.g.obtainMessage(10705, this.b));
        }
    }

    public STInfoV2 getStInfo() {
        this.c.actionId = 200;
        this.c.status = "02";
        this.c.extraData = this.b.mAppName;
        return this.c;
    }
}
