package com.tencent.nucleus.manager.floatingwindow;

import android.content.Context;
import android.graphics.Point;
import android.media.AudioManager;
import android.media.SoundPool;
import android.text.SpannableString;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.widget.TextView;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.Global;
import com.tencent.assistant.m;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.ah;
import com.tencent.assistant.utils.ar;
import com.tencent.assistant.utils.by;
import com.tencent.assistantv2.st.b;
import com.tencent.assistantv2.st.business.BaseSTManagerV2;
import com.tencent.assistantv2.st.business.v;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.beacon.event.a;
import java.util.HashMap;

/* compiled from: ProGuard */
public class n {
    private static n n = null;

    /* renamed from: a  reason: collision with root package name */
    private FloatWindowSmallView f2910a;
    private FloatWindowBigView b;
    private RocketLauncher c;
    private FloatWindowResultView d;
    private WindowManager.LayoutParams e;
    private WindowManager.LayoutParams f;
    private WindowManager.LayoutParams g;
    private WindowManager.LayoutParams h;
    private WindowManager i;
    private long j = 0;
    private SoundPool k;
    private int l;
    private int m = 0;

    private n() {
        q();
    }

    public static synchronized n a() {
        n nVar;
        synchronized (n.class) {
            if (n == null) {
                n = new n();
            }
            nVar = n;
        }
        return nVar;
    }

    public void b() {
        TemporaryThreadManager.get().start(new o(this));
    }

    public void c() {
        TemporaryThreadManager.get().start(new p(this));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.m.a(java.lang.String, byte):byte
      com.tencent.assistant.m.a(java.lang.String, int):int
      com.tencent.assistant.m.a(java.lang.String, long):long
      com.tencent.assistant.m.a(java.lang.String, java.lang.Object):java.lang.String
      com.tencent.assistant.m.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.assistant.m.a(byte, byte):void
      com.tencent.assistant.m.a(byte, int):void
      com.tencent.assistant.m.a(byte, long):void
      com.tencent.assistant.m.a(byte, java.lang.String):void
      com.tencent.assistant.m.a(int, byte[]):void
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, int):void
      com.tencent.assistant.m.a(java.lang.String, byte[]):void
      com.tencent.assistant.m.a(long, int):boolean
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.a(java.lang.Long, int):boolean
      com.tencent.assistant.m.a(java.lang.String, boolean):boolean */
    /* access modifiers changed from: private */
    public void o() {
        if (!m.a().a("key_has_show_float_window_create_tips", false)) {
            ah.a().post(new q(this));
        }
    }

    public void d() {
        TemporaryThreadManager.get().start(new s(this));
    }

    public void a(Context context) {
        int i2;
        int i3;
        WindowManager p = p();
        try {
            i2 = p.getDefaultDisplay().getWidth();
            i3 = p.getDefaultDisplay().getHeight();
        } catch (Exception e2) {
            Point point = new Point();
            p().getDefaultDisplay().getSize(point);
            i2 = point.x;
            i3 = point.y;
        }
        if (this.f2910a == null) {
            this.f2910a = new FloatWindowSmallView(context);
            if (this.e == null) {
                this.e = new WindowManager.LayoutParams();
                this.e.type = STConst.ST_PAGE_HOT;
                this.e.format = 1;
                this.e.flags = 40;
                this.e.gravity = 51;
                this.e.width = FloatWindowSmallView.f2886a;
                this.e.height = FloatWindowSmallView.b;
                this.e.x = i2;
                this.e.y = i3 / 2;
            }
            this.f2910a.a(this.e);
            try {
                p.addView(this.f2910a, this.e);
            } catch (Throwable th) {
                XLog.e("floatingwindow", "FloatingWindowManager >> <createSmallWindow> windowManager.addView throws a exception.");
                this.f2910a = null;
            }
        }
    }

    public void b(Context context) {
        if (this.f2910a != null) {
            try {
                p().removeView(this.f2910a);
                this.f2910a.a();
            } catch (Throwable th) {
            }
            this.f2910a = null;
        }
    }

    public void c(Context context) {
        WindowManager p = p();
        int width = p.getDefaultDisplay().getWidth();
        int height = p.getDefaultDisplay().getHeight();
        if (this.b == null) {
            this.b = new FloatWindowBigView(context);
            if (this.f == null) {
                this.f = new WindowManager.LayoutParams();
                this.f.x = (width / 2) - (FloatWindowBigView.f2884a / 2);
                this.f.y = (height / 2) - (FloatWindowBigView.b / 2);
                this.f.type = STConst.ST_PAGE_HOT;
                this.f.flags = 131328;
                this.f.format = 1;
                this.f.gravity = 51;
                this.f.width = -1;
                this.f.height = -1;
            }
            try {
                p.addView(this.b, this.f);
            } catch (Throwable th) {
                XLog.e("floatingwindow", "FloatingWindowManager >> <createBigWindow> windowManager.addView exception");
                this.b = null;
            }
        }
    }

    public void d(Context context) {
        if (this.b != null) {
            try {
                p().removeView(this.b);
            } catch (Throwable th) {
            }
            this.b = null;
        }
    }

    public void e() {
        WindowManager p = p();
        int width = p.getDefaultDisplay().getWidth();
        int height = p.getDefaultDisplay().getHeight();
        if (this.c == null) {
            this.c = new RocketLauncher(AstApp.i());
            if (this.g == null) {
                this.g = new WindowManager.LayoutParams();
                this.g.x = (width / 2) - (RocketLauncher.f2888a / 2);
                this.g.y = height - RocketLauncher.c;
                this.g.type = STConst.ST_PAGE_NECESSARY;
                this.g.format = 1;
                this.g.gravity = 51;
                this.g.width = -1;
                this.g.height = -1;
            }
            try {
                p.addView(this.c, this.g);
            } catch (Throwable th) {
                XLog.e("floatingwindow", "FloatingWindowManager >> <createLauncher> windowManager.addView exception");
                this.c = null;
            }
        }
    }

    public void f() {
        if (this.c != null) {
            try {
                p().removeView(this.c);
            } catch (Throwable th) {
            }
            this.c = null;
        }
    }

    public void a(boolean z, int i2) {
        if (this.f2910a == null || this.c == null) {
            XLog.e("floatingwindow", "smallWindow=" + this.f2910a + ", rocketLauncher=" + this.c);
            return;
        }
        if (!z) {
            this.c.a(i2);
        }
        this.c.a(z, j());
    }

    public void a(Animation.AnimationListener animationListener) {
        if (this.c != null) {
            this.c.a(animationListener);
        }
    }

    public void g() {
        if (this.f2910a != null && this.e != null) {
            TextView textView = (TextView) this.f2910a.findViewById(R.id.percent);
            View findViewById = this.f2910a.findViewById(R.id.small_window_layout);
            if (textView != null && findViewById != null) {
                textView.setText(k());
                try {
                    if (ar.d() < 0.85f) {
                        if (this.e.x >= by.b() / 2) {
                            findViewById.setBackgroundResource(R.drawable.admin_launch_button_right);
                        } else {
                            findViewById.setBackgroundResource(R.drawable.admin_launch_button_left);
                        }
                    } else if (this.e.x >= by.b() / 2) {
                        findViewById.setBackgroundResource(R.drawable.admin_launch_button_right_orange);
                    } else {
                        findViewById.setBackgroundResource(R.drawable.admin_launch_button_left_orange);
                    }
                } catch (Throwable th) {
                    XLog.e("floatingwindow", "FloatingWindowManager >> <updateUsedPercent> throws exception.");
                }
            }
        }
    }

    public boolean h() {
        return (this.f2910a == null && this.b == null && this.c == null) ? false : true;
    }

    public FloatWindowSmallView i() {
        return this.f2910a;
    }

    public boolean j() {
        if (this.f2910a == null) {
            return false;
        }
        int b2 = by.b();
        int c2 = by.c();
        if (this.e.x <= (b2 / 2) - (RocketLauncher.f2888a / 2) || this.e.x + this.e.width >= (b2 / 2) + (RocketLauncher.f2888a / 2) || this.e.y + this.e.height <= c2 - RocketLauncher.b) {
            return false;
        }
        return true;
    }

    private WindowManager p() {
        if (this.i == null) {
            this.i = (WindowManager) AstApp.i().getSystemService("window");
        }
        return this.i;
    }

    public SpannableString k() {
        return new SpannableString(String.valueOf((int) (ar.d() * 100.0f)));
    }

    public void a(String str, CharSequence charSequence) {
        if (this.d == null) {
            WindowManager p = p();
            int width = p.getDefaultDisplay().getWidth();
            this.d = new FloatWindowResultView(AstApp.i());
            this.d.a(str);
            this.d.a(charSequence);
            if (this.h == null) {
                this.h = new WindowManager.LayoutParams();
                this.h.x = (width / 2) - (FloatWindowResultView.f2885a / 2);
                this.h.y = by.a(AstApp.i(), 100.0f);
                this.h.type = STConst.ST_PAGE_NECESSARY;
                this.h.format = 1;
                this.h.gravity = 51;
                this.h.width = FloatWindowResultView.f2885a;
                this.h.height = FloatWindowResultView.b;
            }
            try {
                p.addView(this.d, this.h);
            } catch (Throwable th) {
                XLog.e("floatingwindow", "FloatingWindowManager >> <createResultView> windowManager.addView throws a exception.");
                this.d = null;
            }
        }
    }

    public void l() {
        if (this.d != null) {
            try {
                p().removeView(this.d);
            } catch (Throwable th) {
            }
            this.d = null;
        }
    }

    public void b(Animation.AnimationListener animationListener) {
        if (this.d != null) {
            this.d.a(animationListener);
        }
    }

    public void a(int i2, String str, int i3) {
        l.a(new STInfoV2(i2, str, 2000, STConst.ST_DEFAULT_SLOT, i3));
        this.m++;
        if (this.m >= 3) {
            this.m = 0;
            try {
                BaseSTManagerV2 a2 = b.a().a(6);
                if (a2 == null) {
                    a2 = new v();
                    b.a().a(6, a2);
                }
                a2.flush();
            } catch (Throwable th) {
            }
        }
    }

    public void a(String str, String str2) {
        HashMap hashMap = new HashMap();
        hashMap.put("B1", str2);
        hashMap.put("B2", Global.getPhoneGuidAndGen());
        hashMap.put("B3", Global.getQUAForBeacon());
        XLog.d("beacon", "beacon report >> " + str + ". " + hashMap.toString());
        a.a(str, true, -1, -1, hashMap, true);
    }

    public void a(int i2, int i3) {
        if (this.c != null) {
            this.c.a(i2, i3);
        }
    }

    public void m() {
        if (this.c != null) {
            this.c.a();
        }
    }

    public boolean a(long j2) {
        long currentTimeMillis = System.currentTimeMillis();
        if (currentTimeMillis - this.j < 30000 || j2 < 31457280) {
            return true;
        }
        this.j = currentTimeMillis;
        return false;
    }

    public void n() {
        if (this.k != null) {
            float streamVolume = (float) ((AudioManager) AstApp.i().getSystemService("audio")).getStreamVolume(3);
            this.k.play(this.l, streamVolume, streamVolume, 0, 0, 1.0f);
        }
    }

    private void q() {
        if (this.k == null) {
            this.k = new SoundPool(1, 3, 0);
            try {
                this.l = this.k.load(AstApp.i(), R.raw.rocket_launch, 1);
            } catch (Throwable th) {
                XLog.e("floatingwindow", "FloatingWindowManager >> <initSound> load exception.");
                this.k.release();
                this.k = null;
            }
        }
    }
}
