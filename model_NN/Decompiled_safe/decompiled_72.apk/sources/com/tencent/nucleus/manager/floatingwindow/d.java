package com.tencent.nucleus.manager.floatingwindow;

import android.view.animation.Animation;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.manager.t;

/* compiled from: ProGuard */
class d implements Animation.AnimationListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ c f2900a;

    d(c cVar) {
        this.f2900a = cVar;
    }

    public void onAnimationStart(Animation animation) {
    }

    public void onAnimationRepeat(Animation animation) {
    }

    public void onAnimationEnd(Animation animation) {
        this.f2900a.f2899a.B.clearAnimation();
        try {
            this.f2900a.f2899a.B.setBackgroundResource(R.drawable.admin_launch_circle);
        } catch (Throwable th) {
            t.a().b();
        }
    }
}
