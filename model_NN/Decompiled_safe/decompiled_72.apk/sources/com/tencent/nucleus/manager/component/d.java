package com.tencent.nucleus.manager.component;

import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import com.tencent.assistant.utils.XLog;

/* compiled from: ProGuard */
public class d extends Animation {

    /* renamed from: a  reason: collision with root package name */
    ViewGroup.LayoutParams f2864a = this.d.getLayoutParams();
    int b = this.d.getHeight();
    final /* synthetic */ AnimationExpandableListView c;
    private View d;

    public d(AnimationExpandableListView animationExpandableListView, View view) {
        this.c = animationExpandableListView;
        this.d = view;
    }

    /* access modifiers changed from: protected */
    public void applyTransformation(float f, Transformation transformation) {
        super.applyTransformation(f, transformation);
        XLog.d("AnimationExpand", "interpolatedTime --" + f);
        this.f2864a.height = (int) (((float) this.b) * (1.0f - f));
        this.d.setLayoutParams(this.f2864a);
    }
}
