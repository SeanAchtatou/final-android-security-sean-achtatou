package com.tencent.nucleus.manager.about;

import android.content.Intent;
import android.view.View;

/* compiled from: ProGuard */
class u implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ HelperFAQActivity f2757a;

    u(HelperFAQActivity helperFAQActivity) {
        this.f2757a = helperFAQActivity;
    }

    public void onClick(View view) {
        this.f2757a.startActivity(new Intent(this.f2757a.C, HelperFeedbackActivity.class));
    }
}
