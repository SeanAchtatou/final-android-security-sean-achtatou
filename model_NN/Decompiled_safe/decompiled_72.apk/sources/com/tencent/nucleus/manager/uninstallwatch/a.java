package com.tencent.nucleus.manager.uninstallwatch;

import android.content.Context;
import android.os.Build;
import android.os.Process;
import android.text.TextUtils;
import com.tencent.assistant.m;
import com.tencent.assistant.utils.XLog;
import com.tencent.connect.common.Constants;
import java.util.List;

/* compiled from: ProGuard */
public class a {

    /* renamed from: a  reason: collision with root package name */
    public static String f3050a = "http://a.myapp.com/o/dom/uninstall/survey.jsp";
    private static String b = "uninstall_display_browser";

    public static Process a(Context context) {
        try {
            String f = f(context);
            if (f != null) {
                XLog.d("uninstall", "<java> startWatch path = " + f);
                return Runtime.getRuntime().exec(f);
            }
            XLog.e("uninstall", "<java> startWatch watch file may not exists !!!");
            return null;
        } catch (Throwable th) {
            XLog.e("uninstall", "<java> startWatch", th);
        }
    }

    public static boolean b(Context context) {
        int a2 = d.c(context);
        return a2 == 0 || a2 != d.d(context);
    }

    public static void c(Context context) {
        d.b(context, d.d(context));
    }

    public static b d(Context context) {
        List a2 = b.b(context);
        if (a2 == null || a2.size() <= 0) {
            return null;
        }
        return (b) a2.get(0);
    }

    public static void a(String str) {
        m.a().b(b, str);
    }

    public static String a() {
        return m.a().a(b, Constants.STR_EMPTY);
    }

    public static boolean b(String str) {
        String a2 = m.a().a(b, Constants.STR_EMPTY);
        return TextUtils.isEmpty(a2) || !str.equals(a2);
    }

    public static String e(Context context) {
        String str;
        Object systemService;
        if (Build.VERSION.SDK_INT >= 17 && (systemService = context.getSystemService("user")) != null) {
            try {
                Object invoke = Process.class.getMethod("myUserHandle", null).invoke(Process.class, null);
                str = String.valueOf(systemService.getClass().getMethod("getSerialNumberForUser", invoke.getClass()).invoke(systemService, invoke));
            } catch (Exception e) {
                XLog.e("uninstall", "<java> getUserSerial", e);
            }
            XLog.d("uninstall", "<java> getUserSerial -> sdk_version = " + Build.VERSION.SDK_INT + ", userSerial = " + str);
            return str;
        }
        str = null;
        XLog.d("uninstall", "<java> getUserSerial -> sdk_version = " + Build.VERSION.SDK_INT + ", userSerial = " + str);
        return str;
    }

    public static String b() {
        return f3050a + "?value=" + c.c();
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x005a  */
    /* JADX WARNING: Removed duplicated region for block: B:16:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x001d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String f(android.content.Context r4) {
        /*
            r1 = 0
            int r0 = android.os.Build.VERSION.SDK_INT
            r2 = 9
            if (r0 < r2) goto L_0x0060
            android.content.pm.ApplicationInfo r0 = r4.getApplicationInfo()
            java.lang.Class<android.content.pm.ApplicationInfo> r2 = android.content.pm.ApplicationInfo.class
            java.lang.String r3 = "nativeLibraryDir"
            java.lang.reflect.Field r2 = r2.getField(r3)     // Catch:{ Exception -> 0x005c }
            if (r2 == 0) goto L_0x0060
            java.lang.Object r0 = r2.get(r0)     // Catch:{ Exception -> 0x005c }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Exception -> 0x005c }
        L_0x001b:
            if (r0 != 0) goto L_0x0036
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            android.content.pm.ApplicationInfo r2 = r4.getApplicationInfo()
            java.lang.String r2 = r2.dataDir
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r2 = "/lib"
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r0 = r0.toString()
        L_0x0036:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r2 = java.io.File.separator
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r2 = "libwatch.so"
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r0 = r0.toString()
            java.io.File r2 = new java.io.File
            r2.<init>(r0)
            boolean r2 = r2.exists()
            if (r2 == 0) goto L_0x005b
            r1 = r0
        L_0x005b:
            return r1
        L_0x005c:
            r0 = move-exception
            r0.printStackTrace()
        L_0x0060:
            r0 = r1
            goto L_0x001b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.nucleus.manager.uninstallwatch.a.f(android.content.Context):java.lang.String");
    }
}
