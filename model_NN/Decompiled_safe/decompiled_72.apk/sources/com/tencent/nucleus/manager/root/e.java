package com.tencent.nucleus.manager.root;

import android.app.ActivityManager;
import android.content.Context;
import android.os.Message;
import android.util.Pair;
import com.qq.AppService.AstApp;
import com.tencent.assistant.Global;
import com.tencent.assistant.db.table.s;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.m;
import com.tencent.assistant.plugin.PluginContext;
import com.tencent.assistant.plugin.PluginDownloadInfo;
import com.tencent.assistant.plugin.PluginInfo;
import com.tencent.assistant.plugin.PluginLoaderInfo;
import com.tencent.assistant.plugin.PluginProxyManager;
import com.tencent.assistant.plugin.mgr.h;
import com.tencent.assistant.plugin.mgr.i;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.XLog;
import com.tencent.beacon.event.a;
import com.tencent.connect.common.Constants;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/* compiled from: ProGuard */
public class e implements UIEventListener {

    /* renamed from: a  reason: collision with root package name */
    private static e f2973a = new e();
    private static String b = "com.tencent.assistant.root";
    private static String c = "com.tencent.plugin.root.RootManager";
    /* access modifiers changed from: private */
    public AstApp d = AstApp.i();
    private Object e = null;
    private Method f = null;
    private Method g = null;
    private Method h = null;
    private Method i = null;
    private Method j = null;
    private Method k = null;
    private Context l = null;
    private Object m = new Object();
    private boolean n = false;
    private Object o = new Object();
    /* access modifiers changed from: private */
    public Object p = new Object();
    private boolean q = false;
    /* access modifiers changed from: private */
    public volatile boolean r = false;
    private boolean s = false;
    private boolean t = false;
    private Object u = new Object();
    private long v = 10485760;

    private e() {
        e();
    }

    public static e a() {
        return f2973a;
    }

    private void e() {
        this.d.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_INSTALL_FAIL, this);
        this.d.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_INSTALL_SUCC, this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.nucleus.manager.root.e.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.nucleus.manager.root.e.a(com.tencent.nucleus.manager.root.e, boolean):void
      com.tencent.nucleus.manager.root.e.a(boolean, boolean):void
      com.tencent.nucleus.manager.root.e.a(java.lang.String, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.nucleus.manager.root.e.a(boolean, boolean):void
     arg types: [boolean, int]
     candidates:
      com.tencent.nucleus.manager.root.e.a(com.tencent.nucleus.manager.root.e, boolean):void
      com.tencent.nucleus.manager.root.e.a(java.lang.String, boolean):void
      com.tencent.nucleus.manager.root.e.a(boolean, boolean):void */
    public void handleUIEvent(Message message) {
        XLog.d("RootManager", "handleUIEvent msg.what = " + message.what + ", msg.obj = " + message.obj);
        String str = Constants.STR_EMPTY;
        if (message.obj instanceof String) {
            str = (String) message.obj;
        }
        if (str.equals(b)) {
            switch (message.what) {
                case EventDispatcherEnum.UI_EVENT_PLUGIN_INSTALL_SUCC:
                    XLog.d("RootManager", "root plugin install success !!!");
                    PluginInfo a2 = i.b().a(b);
                    if (a2 == null) {
                        XLog.e("RootManager", "root plugin get pulgin info = null");
                        return;
                    }
                    int version = a2.getVersion();
                    int f2 = f();
                    a("install_plugin", true);
                    XLog.d("RootManager", "root plugin preVersion = " + f2 + ", curVersion = " + version);
                    if (version < f2) {
                        XLog.e("RootManager", "root plugin version error, so will not continue !!!");
                        return;
                    }
                    a(version);
                    a(version > f2, false);
                    return;
                case EventDispatcherEnum.UI_EVENT_PLUGIN_INSTALL_FAIL:
                    XLog.e("RootManager", "root plugin install failed !!!");
                    a("install_plugin", false);
                    return;
                default:
                    return;
            }
        }
    }

    private void a(int i2) {
        m.a().b("key_root_plugin_version", Integer.valueOf(i2));
    }

    private int f() {
        return m.a().a("key_root_plugin_version", 0);
    }

    private boolean g() {
        boolean z;
        int f2 = f();
        if (!this.s && f2 > 0) {
            List<PluginDownloadInfo> a2 = new s().a();
            if (a2 != null && a2.size() > 0) {
                Iterator<PluginDownloadInfo> it = a2.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        break;
                    }
                    PluginDownloadInfo next = it.next();
                    if (next.pluginPackageName.equals(b) && next.version > f2) {
                        synchronized (this.u) {
                            this.t = true;
                            break;
                        }
                    }
                }
            }
            this.s = true;
        }
        synchronized (this.u) {
            z = this.t;
        }
        return z;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.nucleus.manager.root.e.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.nucleus.manager.root.e.a(com.tencent.nucleus.manager.root.e, boolean):void
      com.tencent.nucleus.manager.root.e.a(boolean, boolean):void
      com.tencent.nucleus.manager.root.e.a(java.lang.String, boolean):void */
    public void a(boolean z, boolean z2) {
        XLog.i("RootManager", "start root : " + (z ? "<force>" : "<normal>") + " >>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
        if (p()) {
            XLog.e("RootManager", "memory retained is not enough ... \ntemp root stopped !!!");
            a("tr_mem_not_enough", true);
            return;
        }
        c(z);
        if (!a(z)) {
            XLog.e("RootManager", "temp root init failed, stoped !!!");
        } else {
            Executors.newSingleThreadScheduledExecutor().schedule(new f(this, z), z2 ? 3 : 0, TimeUnit.SECONDS);
        }
    }

    private boolean a(boolean z) {
        boolean z2;
        XLog.i("RootManager", "init");
        this.q = AstApp.d();
        Context d2 = d(z);
        if (d2 != null) {
            Object a2 = a("init", new Object[]{this.d, d2}, Context.class, Context.class);
            boolean booleanValue = a2 == null ? false : ((Boolean) a2).booleanValue();
            synchronized (this.o) {
                this.n = booleanValue;
                z2 = this.n;
            }
        } else {
            synchronized (this.o) {
                this.n = false;
                z2 = this.n;
            }
        }
        return z2;
    }

    private boolean h() {
        boolean z;
        synchronized (this.o) {
            z = this.n;
        }
        return z;
    }

    /* access modifiers changed from: private */
    public void b(boolean z) {
        XLog.i("RootManager", "begin to root ...");
        Context d2 = d(z);
        if (d2 != null) {
            a("tryToGetTempRoot", new Object[]{this.d, d2, Boolean.valueOf(z)}, Context.class, Context.class, Boolean.TYPE);
            return;
        }
        XLog.e("RootManager", "start pluginContext = null");
    }

    public Pair<Integer, String> a(String str) {
        Object obj = null;
        Context d2 = d(false);
        if (d2 != null && h()) {
            obj = a("executeCommandWithResult", new Object[]{this.d, d2, str}, Context.class, Context.class, String.class);
        }
        if (obj != null) {
            return (Pair) obj;
        }
        return new Pair<>(-1, "execute2 failed");
    }

    public String b(String str) {
        Pair<Integer, String> a2 = a(str);
        if (a2 != null) {
            return (String) a2.second;
        }
        return null;
    }

    public void b() {
        if (c()) {
            XLog.d("RootManager", "already have root, no need to save !");
        } else {
            TemporaryThreadManager.get().start(new g(this));
        }
    }

    public boolean c() {
        boolean z;
        if (h()) {
            Object a2 = a("hasRoot", null, new Class[0]);
            z = a2 == null ? false : ((Boolean) a2).booleanValue();
        } else {
            z = false;
        }
        XLog.d("RootManager", "hasRoot = " + z);
        return z;
    }

    public void d() {
        XLog.i("RootManager", "doUserExit");
        a("doUserExit", null, new Class[0]);
    }

    /* access modifiers changed from: private */
    public Object a(String str, Object[] objArr, Class<?>... clsArr) {
        Method method;
        XLog.i("RootManager", "callPluginMethod methodName = " + str);
        if (str.equals("init")) {
            method = j();
        } else if (str.equals("tryToGetTempRoot")) {
            method = k();
        } else if (str.equals("executeCommandWithResult")) {
            method = l();
        } else if (str.equals("hasRoot")) {
            method = n();
        } else if (str.equals("savePermRoot")) {
            method = m();
        } else if (str.equals("doUserExit")) {
            method = o();
        } else {
            XLog.e("RootManager", "callPluginMethod error !!!");
            method = null;
        }
        if (method == null) {
            return null;
        }
        try {
            return method.invoke(i(), objArr);
        } catch (Throwable th) {
            XLog.e("RootManager", "callPluginMethod", th);
            return null;
        }
    }

    private Object i() {
        if (this.e == null) {
            c(false);
        }
        return this.e;
    }

    private Method j() {
        if (this.f == null) {
            c(false);
        }
        return this.f;
    }

    private Method k() {
        if (this.g == null) {
            c(false);
        }
        return this.g;
    }

    private Method l() {
        if (this.h == null) {
            c(false);
        }
        return this.h;
    }

    private Method m() {
        if (this.k == null) {
            c(false);
        }
        return this.k;
    }

    private Method n() {
        if (this.i == null) {
            c(false);
        }
        return this.i;
    }

    private Method o() {
        if (this.j == null) {
            c(false);
        }
        return this.j;
    }

    private void c(boolean z) {
        XLog.i("RootManager", "initReflect");
        synchronized (this.m) {
            PluginLoaderInfo e2 = e(z);
            if (e2 != null) {
                Class<?> cls = null;
                try {
                    cls = e2.loadClass(c);
                } catch (Exception e3) {
                    XLog.e("RootManager", "initReflect", e3);
                }
                if (cls != null) {
                    try {
                        this.e = cls.newInstance();
                    } catch (Exception e4) {
                        XLog.e("RootManager", "initReflect", e4);
                    }
                    try {
                        this.f = cls.getMethod("init", Context.class, Context.class);
                    } catch (Exception e5) {
                        XLog.e("RootManager", "initReflect", e5);
                    }
                    try {
                        this.g = cls.getMethod("tryToGetTempRoot", Context.class, Context.class, Boolean.TYPE);
                    } catch (Exception e6) {
                        XLog.e("RootManager", "initReflect", e6);
                    }
                    try {
                        this.i = cls.getMethod("hasRoot", new Class[0]);
                    } catch (Exception e7) {
                        XLog.e("RootManager", "initReflect", e7);
                    }
                    try {
                        this.k = cls.getMethod("savePermRoot", Context.class, Context.class);
                    } catch (Exception e8) {
                        XLog.e("RootManager", "initReflect", e8);
                    }
                    try {
                        this.h = cls.getMethod("executeCommandWithResult", Context.class, Context.class, String.class);
                    } catch (Exception e9) {
                        XLog.e("RootManager", "initReflect", e9);
                    }
                    try {
                        this.j = cls.getMethod("doUserExit", new Class[0]);
                    } catch (Exception e10) {
                        XLog.e("RootManager", "initReflect", e10);
                    }
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public Context d(boolean z) {
        PluginContext pluginContext = null;
        if (z || this.l == null) {
            PluginLoaderInfo e2 = e(z);
            if (e2 != null) {
                pluginContext = e2.getContext();
            }
            this.l = pluginContext;
            this.q = false;
        } else if (this.q) {
            this.l = null;
        } else if (g()) {
            this.l = null;
        }
        return this.l;
    }

    private PluginLoaderInfo e(boolean z) {
        if (z || (!AstApp.d() && !g())) {
            try {
                return h.a(this.d, i.b().a(b));
            } catch (Exception e2) {
                XLog.e("RootManager", "getPluginLoaderInfo", e2);
            }
        }
        return null;
    }

    private boolean p() {
        try {
            ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
            ((ActivityManager) this.d.getSystemService("activity")).getMemoryInfo(memoryInfo);
            return memoryInfo.lowMemory && memoryInfo.availMem <= this.v;
        } catch (Exception e2) {
            XLog.e("RootManager", "isMemNotEnough", e2);
            return false;
        }
    }

    public final void a(String str, boolean z) {
        HashMap hashMap = new HashMap();
        hashMap.put("B1", Global.getPhoneGuidAndGen());
        hashMap.put("B2", PluginProxyManager.getQUA());
        hashMap.put("B3", Constants.STR_EMPTY + f());
        a.a(str, z, -1, -1, hashMap, true);
    }
}
