package com.tencent.nucleus.manager.root;

import com.tencent.assistant.AppConst;

/* compiled from: ProGuard */
/* synthetic */ class d {

    /* renamed from: a  reason: collision with root package name */
    static final /* synthetic */ int[] f2972a = new int[AppConst.AppState.values().length];

    static {
        try {
            f2972a[AppConst.AppState.DOWNLOAD.ordinal()] = 1;
        } catch (NoSuchFieldError e) {
        }
        try {
            f2972a[AppConst.AppState.UPDATE.ordinal()] = 2;
        } catch (NoSuchFieldError e2) {
        }
        try {
            f2972a[AppConst.AppState.DOWNLOADING.ordinal()] = 3;
        } catch (NoSuchFieldError e3) {
        }
        try {
            f2972a[AppConst.AppState.QUEUING.ordinal()] = 4;
        } catch (NoSuchFieldError e4) {
        }
        try {
            f2972a[AppConst.AppState.PAUSED.ordinal()] = 5;
        } catch (NoSuchFieldError e5) {
        }
        try {
            f2972a[AppConst.AppState.DOWNLOADED.ordinal()] = 6;
        } catch (NoSuchFieldError e6) {
        }
        try {
            f2972a[AppConst.AppState.INSTALLED.ordinal()] = 7;
        } catch (NoSuchFieldError e7) {
        }
        try {
            f2972a[AppConst.AppState.ILLEGAL.ordinal()] = 8;
        } catch (NoSuchFieldError e8) {
        }
        try {
            f2972a[AppConst.AppState.FAIL.ordinal()] = 9;
        } catch (NoSuchFieldError e9) {
        }
        try {
            f2972a[AppConst.AppState.SDKUNSUPPORT.ordinal()] = 10;
        } catch (NoSuchFieldError e10) {
        }
        try {
            f2972a[AppConst.AppState.INSTALLING.ordinal()] = 11;
        } catch (NoSuchFieldError e11) {
        }
        try {
            f2972a[AppConst.AppState.UNINSTALLING.ordinal()] = 12;
        } catch (NoSuchFieldError e12) {
        }
    }
}
