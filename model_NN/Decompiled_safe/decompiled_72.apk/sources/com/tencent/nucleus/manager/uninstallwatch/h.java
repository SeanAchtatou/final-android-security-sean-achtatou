package com.tencent.nucleus.manager.uninstallwatch;

import android.net.LocalSocket;
import android.net.LocalSocketAddress;
import android.os.Process;
import android.text.TextUtils;
import android.util.Log;
import com.qq.AppService.AstApp;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.XLog;
import com.tencent.connect.common.Constants;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;

/* compiled from: ProGuard */
public final class h {

    /* renamed from: a  reason: collision with root package name */
    InputStream f3055a;
    OutputStream b;
    LocalSocket c;
    byte[] d = new byte[1024];
    int e = 0;

    private boolean d() {
        if (this.c != null) {
            XLog.d("uninstall", "<java> already connected !");
            return true;
        }
        XLog.i("uninstall", "<java> connecting...");
        try {
            this.c = new LocalSocket();
            this.c.connect(new LocalSocketAddress("yyb_watchd", LocalSocketAddress.Namespace.ABSTRACT));
            this.f3055a = this.c.getInputStream();
            this.b = this.c.getOutputStream();
            return true;
        } catch (IOException e2) {
            e();
            return false;
        }
    }

    private void e() {
        XLog.i("uninstall", "<java> disconnecting...");
        try {
            if (this.c != null) {
                this.c.close();
            }
        } catch (IOException e2) {
        }
        try {
            if (this.f3055a != null) {
                this.f3055a.close();
            }
        } catch (IOException e3) {
        }
        try {
            if (this.b != null) {
                this.b.close();
            }
        } catch (IOException e4) {
        }
        this.c = null;
        this.f3055a = null;
        this.b = null;
    }

    private boolean a(byte[] bArr, int i) {
        if (i < 0) {
            return false;
        }
        int i2 = 0;
        while (true) {
            if (i2 == i) {
                break;
            }
            try {
                int read = this.f3055a.read(bArr, i2, i - i2);
                if (read <= 0) {
                    XLog.e("uninstall", "<java> read error " + read);
                    break;
                }
                i2 += read;
            } catch (IOException e2) {
                XLog.e("uninstall", "<java> read exception");
            }
        }
        if (i2 == i) {
            return true;
        }
        e();
        return false;
    }

    private boolean f() {
        this.e = 0;
        if (!a(this.d, 2)) {
            return false;
        }
        byte b2 = (this.d[0] & 255) | ((this.d[1] & 255) << 8);
        if (b2 < 1 || b2 > 1024) {
            XLog.e("uninstall", "<java> invalid reply length (" + ((int) b2) + ")");
            e();
            return false;
        } else if (!a(this.d, b2)) {
            return false;
        } else {
            this.e = b2;
            return true;
        }
    }

    private boolean a(String str) {
        byte[] bytes = str.getBytes();
        int length = bytes.length;
        if (length < 1 || length > 1024) {
            return false;
        }
        this.d[0] = (byte) (length & 255);
        this.d[1] = (byte) ((length >> 8) & 255);
        try {
            this.b.write(this.d, 0, 2);
            this.b.write(bytes, 0, length);
            return true;
        } catch (IOException e2) {
            XLog.e("uninstall", "<java> write error");
            e();
            return false;
        }
    }

    private synchronized String b(String str) {
        String str2;
        if (!d()) {
            XLog.e("uninstall", "<java> connection failed");
            str2 = STConst.ST_DEFAULT_SLOT;
        } else {
            if (!a(str)) {
                XLog.e("uninstall", "<java> write command failed? reconnect!");
                if (!d() || !a(str)) {
                    str2 = STConst.ST_DEFAULT_SLOT;
                }
            }
            if (f()) {
                str2 = new String(this.d, 0, this.e);
            } else {
                str2 = STConst.ST_DEFAULT_SLOT;
            }
        }
        return str2;
    }

    private int c(String str) {
        try {
            return Integer.parseInt(b(str));
        } catch (NumberFormatException e2) {
            return -1;
        }
    }

    public boolean a(String str, String str2, String str3) {
        StringBuilder sb = new StringBuilder("watch");
        sb.append(' ');
        sb.append(str);
        sb.append(' ');
        sb.append(str2);
        sb.append(' ');
        sb.append(str3);
        return c(sb.toString()) == 0;
    }

    public int a() {
        StringBuilder sb = new StringBuilder("wake");
        sb.append(' ');
        sb.append(Process.myPid());
        sb.append(' ');
        String g = g();
        if (TextUtils.isEmpty(g)) {
            g = STConst.ST_DEFAULT_SLOT;
        }
        sb.append(g);
        return c(sb.toString());
    }

    public boolean b() {
        if (c("ping") < 0) {
            return false;
        }
        return true;
    }

    public boolean b(String str, String str2, String str3) {
        StringBuilder sb = new StringBuilder("update");
        sb.append(' ');
        sb.append(str);
        sb.append(' ');
        sb.append(str2);
        sb.append(' ');
        sb.append(str3);
        return c(sb.toString()) == 0;
    }

    public boolean c() {
        if (c("check") <= 0) {
            return false;
        }
        return true;
    }

    public int a(b bVar) {
        return c("set_browsers" + ' ' + (bVar.f3051a + "/" + bVar.b).trim());
    }

    private String g() {
        Object systemService = AstApp.i().getSystemService("user");
        if (systemService == null) {
            Log.e("uninstall", "userManager not exsit !!!");
            return null;
        }
        try {
            Object invoke = Process.class.getMethod("myUserHandle", null).invoke(Process.class, null);
            return String.valueOf(((Long) systemService.getClass().getMethod("getSerialNumberForUser", invoke.getClass()).invoke(systemService, invoke)).longValue());
        } catch (NoSuchMethodException e2) {
            Log.e("uninstall", Constants.STR_EMPTY, e2);
        } catch (IllegalArgumentException e3) {
            Log.e("uninstall", Constants.STR_EMPTY, e3);
        } catch (IllegalAccessException e4) {
            Log.e("uninstall", Constants.STR_EMPTY, e4);
        } catch (InvocationTargetException e5) {
            Log.e("uninstall", Constants.STR_EMPTY, e5);
        }
        return null;
    }
}
