package com.tencent.nucleus.manager.apkuninstall;

import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.localres.model.LocalApkInfo;

/* compiled from: ProGuard */
class d extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ PreInstallAppListView f2786a;

    d(PreInstallAppListView preInstallAppListView) {
        this.f2786a = preInstallAppListView;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.nucleus.manager.apkuninstall.PreInstallAppListView.a(com.tencent.nucleus.manager.apkuninstall.PreInstallAppListView, boolean):boolean
     arg types: [com.tencent.nucleus.manager.apkuninstall.PreInstallAppListView, int]
     candidates:
      com.tencent.nucleus.manager.apkuninstall.PreInstallAppListView.a(com.tencent.nucleus.manager.apkuninstall.PreInstallAppListView, int):void
      com.tencent.nucleus.manager.apkuninstall.PreInstallAppListView.a(com.tencent.nucleus.manager.apkuninstall.PreInstallAppListView, com.tencent.assistant.localres.model.LocalApkInfo):void
      com.tencent.nucleus.manager.apkuninstall.PreInstallAppListView.a(java.util.List<com.tencent.assistant.localres.model.LocalApkInfo>, int):void
      com.tencent.nucleus.manager.apkuninstall.PreInstallAppListView.a(com.tencent.nucleus.manager.apkuninstall.PreInstallAppListView, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.nucleus.manager.apkuninstall.PreInstalledAppListAdapter.a(boolean, boolean):void
     arg types: [boolean, int]
     candidates:
      com.tencent.nucleus.manager.apkuninstall.PreInstalledAppListAdapter.a(int, int):com.tencent.assistantv2.st.page.STInfoV2
      com.tencent.nucleus.manager.apkuninstall.PreInstalledAppListAdapter.a(android.widget.TextView, long):void
      com.tencent.nucleus.manager.apkuninstall.PreInstalledAppListAdapter.a(com.tencent.nucleus.manager.apkuninstall.i, com.tencent.assistant.localres.model.LocalApkInfo):void
      com.tencent.nucleus.manager.apkuninstall.PreInstalledAppListAdapter.a(com.tencent.nucleus.manager.apkuninstall.PreInstalledAppListAdapter, boolean):boolean
      com.tencent.nucleus.manager.apkuninstall.PreInstalledAppListAdapter.a(java.util.List<com.tencent.assistant.localres.model.LocalApkInfo>, int):void
      com.tencent.nucleus.manager.apkuninstall.PreInstalledAppListAdapter.a(boolean, boolean):void */
    public void onTMAClick(View view) {
        if (this.f2786a.e.getFooterViewEnable() && this.f2786a.p.size() > 0) {
            this.f2786a.q.clear();
            this.f2786a.b((LocalApkInfo) this.f2786a.p.get(0));
            boolean unused = this.f2786a.n = true;
            boolean unused2 = this.f2786a.o = true;
            this.f2786a.l.a(this.f2786a.n, true);
            this.f2786a.j();
            this.f2786a.b(this.f2786a.p.size());
        }
    }
}
