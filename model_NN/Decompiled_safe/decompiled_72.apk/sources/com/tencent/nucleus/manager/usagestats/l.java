package com.tencent.nucleus.manager.usagestats;

import android.os.Parcel;
import android.os.Parcelable;

/* compiled from: ProGuard */
final class l implements Parcelable.Creator<PkgUsageStats> {
    l() {
    }

    /* renamed from: a */
    public PkgUsageStats createFromParcel(Parcel parcel) {
        return new PkgUsageStats(parcel);
    }

    /* renamed from: a */
    public PkgUsageStats[] newArray(int i) {
        return new PkgUsageStats[i];
    }
}
