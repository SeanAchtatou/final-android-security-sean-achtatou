package com.tencent.nucleus.manager.usagestats;

import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistantv2.st.business.BaseSTManagerV2;
import com.tencent.nucleus.manager.usagestats.UsagestatsSTManager;

/* compiled from: ProGuard */
public class a extends BaseSTManagerV2 {

    /* renamed from: a  reason: collision with root package name */
    private static a f3060a = null;

    public static synchronized a a() {
        a aVar;
        synchronized (a.class) {
            if (f3060a == null) {
                f3060a = new a();
            }
            aVar = f3060a;
        }
        return aVar;
    }

    public byte getSTType() {
        return 17;
    }

    public void flush() {
    }

    public void a(UsagestatsSTManager.ReportScene reportScene) {
        TemporaryThreadManager.get().start(new b(this, reportScene));
    }
}
