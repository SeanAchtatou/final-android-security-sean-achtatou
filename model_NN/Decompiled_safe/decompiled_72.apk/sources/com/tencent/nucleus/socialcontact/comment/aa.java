package com.tencent.nucleus.socialcontact.comment;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.protocol.jce.CommentDetail;
import com.tencent.assistant.protocol.jce.CommentTagInfo;
import com.tencent.assistant.utils.bm;
import com.tencent.connect.common.Constants;
import java.util.List;

/* compiled from: ProGuard */
class aa implements j {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ CommentReplyListActivity f3083a;

    aa(CommentReplyListActivity commentReplyListActivity) {
        this.f3083a = commentReplyListActivity;
    }

    public void a(int i, int i2, boolean z, CommentTagInfo commentTagInfo, List<CommentDetail> list, List<CommentDetail> list2, List<CommentTagInfo> list3, boolean z2, byte[] bArr, CommentDetail commentDetail) {
    }

    public void a(int i, int i2, CommentDetail commentDetail, long j) {
    }

    public void a(int i, int i2, long j, String str, String str2, long j2) {
    }

    public void a(int i, int i2, long j, String str, int i3, long j2) {
    }

    public void a(int i, int i2, long j, int i3, long j2) {
        long j3;
        if (i2 == -800 && this.f3083a.K != null && this.f3083a.K.h == j && this.f3083a.R != null) {
            Drawable drawable = this.f3083a.getResources().getDrawable(R.drawable.pinglun_icon_zan);
            drawable.setBounds(0, 0, drawable.getMinimumWidth(), drawable.getMinimumHeight());
            Drawable drawable2 = this.f3083a.getResources().getDrawable(R.drawable.pinglun_icon_yizan);
            drawable2.setBounds(0, 0, drawable2.getMinimumWidth(), drawable2.getMinimumHeight());
            long parseInt = (long) Integer.parseInt(this.f3083a.R.getTag(R.id.comment_praise_count) + Constants.STR_EMPTY);
            if (((Boolean) this.f3083a.R.getTag()).booleanValue()) {
                this.f3083a.R.setTag(false);
                this.f3083a.R.setCompoundDrawables(drawable, null, null, null);
                j3 = parseInt - 1;
                this.f3083a.R.setText(bm.a(j3) + Constants.STR_EMPTY);
                this.f3083a.R.setTextColor(Color.parseColor("#6e6e6e"));
            } else {
                this.f3083a.R.setTag(true);
                this.f3083a.R.setCompoundDrawables(drawable2, null, null, null);
                j3 = 1 + parseInt;
                this.f3083a.R.setText(bm.a(j3) + Constants.STR_EMPTY);
                this.f3083a.R.setTextColor(Color.parseColor("#b68a46"));
            }
            this.f3083a.R.setTag(R.id.comment_praise_count, Long.valueOf(j3));
        }
    }
}
