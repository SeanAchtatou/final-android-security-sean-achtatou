package com.tencent.nucleus.socialcontact.tagpage;

import java.io.File;
import java.io.FilenameFilter;

/* compiled from: ProGuard */
class n implements FilenameFilter {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ m f3208a;

    n(m mVar) {
        this.f3208a = mVar;
    }

    public boolean accept(File file, String str) {
        return !str.contains(".");
    }
}
