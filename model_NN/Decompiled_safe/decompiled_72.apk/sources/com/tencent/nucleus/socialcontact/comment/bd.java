package com.tencent.nucleus.socialcontact.comment;

import com.tencent.assistant.module.callback.CallbackHelper;
import com.tencent.assistant.protocol.jce.ModifyAppCommentRequest;

/* compiled from: ProGuard */
class bd implements CallbackHelper.Caller<j> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f3110a;
    final /* synthetic */ ModifyAppCommentRequest b;
    final /* synthetic */ bc c;

    bd(bc bcVar, int i, ModifyAppCommentRequest modifyAppCommentRequest) {
        this.c = bcVar;
        this.f3110a = i;
        this.b = modifyAppCommentRequest;
    }

    /* renamed from: a */
    public void call(j jVar) {
        jVar.a(this.f3110a, 0, this.b.f1410a, this.b.b, this.b.d, System.currentTimeMillis());
    }
}
