package com.tencent.nucleus.socialcontact.comment;

import android.text.Editable;
import android.text.SpannableStringBuilder;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.utils.by;

/* compiled from: ProGuard */
class ay implements TextWatcher {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ CommentReplyListFooterView f3106a;

    ay(CommentReplyListFooterView commentReplyListFooterView) {
        this.f3106a = commentReplyListFooterView;
    }

    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public void afterTextChanged(Editable editable) {
        String obj = editable.toString();
        this.f3106a.e.setText(String.format(this.f3106a.f3080a.getString(R.string.comment_txt_tips), Integer.valueOf(obj.length())));
        if (obj.length() > 0) {
            this.f3106a.e.setVisibility(0);
            this.f3106a.e.setTextColor(this.f3106a.f3080a.getResources().getColor(R.color.comment_over_txt_tips));
            this.f3106a.d.setPadding(by.a(this.f3106a.f3080a, 8.0f), by.a(this.f3106a.f3080a, 5.0f), by.a(this.f3106a.f3080a, 8.0f), by.a(this.f3106a.f3080a, 20.0f));
            if (obj.length() > 100) {
                SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(this.f3106a.e.getText().toString());
                ForegroundColorSpan foregroundColorSpan = new ForegroundColorSpan(-65536);
                ForegroundColorSpan foregroundColorSpan2 = new ForegroundColorSpan(this.f3106a.f3080a.getResources().getColor(R.color.comment_over_txt_tips));
                spannableStringBuilder.setSpan(foregroundColorSpan, 0, this.f3106a.e.getText().toString().indexOf("/"), 33);
                spannableStringBuilder.setSpan(foregroundColorSpan2, this.f3106a.e.getText().toString().indexOf("/") + 1, this.f3106a.e.getText().toString().length(), 18);
                this.f3106a.e.setText(spannableStringBuilder);
            }
        } else if (obj.length() == 0) {
            this.f3106a.a();
        } else {
            this.f3106a.e.setVisibility(0);
        }
    }
}
