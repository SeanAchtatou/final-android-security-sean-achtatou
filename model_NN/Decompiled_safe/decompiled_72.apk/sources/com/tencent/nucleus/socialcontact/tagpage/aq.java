package com.tencent.nucleus.socialcontact.tagpage;

import com.tencent.assistant.net.c;
import com.tencent.nucleus.manager.setting.SettingActivity;
import com.tencent.nucleus.socialcontact.tagpage.MiniVideoSetDialog;

/* compiled from: ProGuard */
public class aq {

    /* renamed from: a  reason: collision with root package name */
    private static MiniVideoSetDialog.ItemIndex f3188a = MiniVideoSetDialog.ItemIndex.ONLY_WIFI;

    public static MiniVideoSetDialog.ItemIndex a() {
        f3188a = MiniVideoSetDialog.ItemIndex.ONLY_WIFI;
        int i = SettingActivity.t().getInt("item_index", -1);
        if (i >= 0 && i <= 2) {
            f3188a = MiniVideoSetDialog.ItemIndex.values()[i];
        }
        return f3188a;
    }

    public static boolean b() {
        switch (ar.f3189a[f3188a.ordinal()]) {
            case 1:
                if (c.a()) {
                    return true;
                }
                break;
            case 2:
                if (c.e()) {
                    return true;
                }
                break;
            case 3:
                return false;
        }
        return false;
    }
}
