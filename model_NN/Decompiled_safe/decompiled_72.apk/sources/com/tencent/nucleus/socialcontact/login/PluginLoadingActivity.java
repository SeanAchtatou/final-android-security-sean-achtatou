package com.tencent.nucleus.socialcontact.login;

import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;

/* compiled from: ProGuard */
public class PluginLoadingActivity extends BaseActivity implements UIEventListener {
    private PluginLoadingDialog n;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        overridePendingTransition(R.anim.activity_fade_in, R.anim.activity_fade_out);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_INSTALL_SUCC, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_INSTALL_FAIL, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_DOWNLIST_FAIL, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_DOWNLOAD_FAIL, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_INSTALL_FINAL_FAIL, this);
        this.n = new PluginLoadingDialog(this);
        this.n.setOwnerActivity(this);
        if (!isFinishing()) {
            this.n.show();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_INSTALL_SUCC, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_INSTALL_FAIL, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_DOWNLIST_FAIL, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_DOWNLOAD_FAIL, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_INSTALL_FINAL_FAIL, this);
        super.onDestroy();
        if (this.n != null && this.n.isShowing()) {
            this.n.dismiss();
        }
    }

    public void overridePendingTransition(int i, int i2) {
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Log.d("Donaldxu", "PluginLoadingActivity --onNewIntent");
    }

    public void handleUIEvent(Message message) {
        String str;
        int i = message.what;
        try {
            str = (String) message.obj;
        } catch (Exception e) {
            e.printStackTrace();
            str = null;
        }
        switch (i) {
            case EventDispatcherEnum.UI_EVENT_PLUGIN_DOWNLOAD_FAIL:
            case EventDispatcherEnum.UI_EVENT_PLUGIN_INSTALL_FAIL:
            case EventDispatcherEnum.UI_EVENT_PLUGIN_INSTALL_FINAL_FAIL:
                if (this.n != null && !TextUtils.isEmpty(str) && str.equals(h.b)) {
                    this.n.a();
                    return;
                }
                return;
            case EventDispatcherEnum.UI_EVENT_PLUGIN_INSTALL_SUCC:
                if (!TextUtils.isEmpty(str) && str.equals(h.b)) {
                    if (this.n != null && this.n.isShowing()) {
                        this.n.dismiss();
                    }
                    h.h().i();
                    return;
                }
                return;
            case EventDispatcherEnum.UI_EVENT_PLUGIN_DOWNLIST_FAIL:
                this.n.a();
                return;
            default:
                return;
        }
    }
}
