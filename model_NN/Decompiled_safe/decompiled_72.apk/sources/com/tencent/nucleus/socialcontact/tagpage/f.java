package com.tencent.nucleus.socialcontact.tagpage;

/* compiled from: ProGuard */
class f implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ LoadingView f3202a;

    f(LoadingView loadingView) {
        this.f3202a = loadingView;
    }

    public void run() {
        if (this.f3202a.c <= RoundProgressBar.a(75, 100)) {
            LoadingView.a(this.f3202a, 2);
            this.f3202a.b.a(this.f3202a.c);
            if (this.f3202a.e != null) {
                this.f3202a.e.postDelayed(this.f3202a.f, 50);
            }
        } else if (this.f3202a.e != null) {
            this.f3202a.e.removeCallbacks(this.f3202a.f);
        }
    }
}
