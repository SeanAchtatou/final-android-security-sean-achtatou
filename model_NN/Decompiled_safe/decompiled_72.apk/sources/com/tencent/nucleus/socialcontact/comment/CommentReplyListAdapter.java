package com.tencent.nucleus.socialcontact.comment;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.txscrollview.TXAppIconView;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.protocol.jce.ReplyDetail;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.bm;
import com.tencent.assistant.utils.bo;
import com.tencent.assistant.utils.by;
import com.tencent.assistantv2.st.b.b;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class CommentReplyListAdapter extends BaseAdapter {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public Context f3079a;
    private LayoutInflater b;
    private View c;
    private List<ReplyDetail> d = new ArrayList();
    /* access modifiers changed from: private */
    public int e = 0;
    /* access modifiers changed from: private */
    public long f;
    /* access modifiers changed from: private */
    public String[] g = {"+1", "好", "顶", "赞", "朕觉OK", "不明觉厉", "吊炸天"};
    /* access modifiers changed from: private */
    public aq h;
    /* access modifiers changed from: private */
    public String i = "01_";
    /* access modifiers changed from: private */
    public String j = "02_";
    /* access modifiers changed from: private */
    public String k = "03_";
    /* access modifiers changed from: private */
    public Runnable l = new al(this);
    private b m = null;

    static /* synthetic */ int d(CommentReplyListAdapter commentReplyListAdapter) {
        int i2 = commentReplyListAdapter.e;
        commentReplyListAdapter.e = i2 + 1;
        return i2;
    }

    public CommentReplyListAdapter(Context context, View view, long j2, List<ReplyDetail> list) {
        this.f3079a = context;
        this.b = LayoutInflater.from(context);
        this.c = view;
        this.f = j2;
        if (list != null) {
            this.d.clear();
            this.d.addAll(list);
            notifyDataSetChanged();
        }
    }

    public void a(boolean z, List<ReplyDetail> list) {
        if (list != null) {
            if (z) {
                this.d.clear();
            }
            this.d.addAll(list);
            notifyDataSetChanged();
        }
    }

    public int getCount() {
        if (this.d == null) {
            return 0;
        }
        return this.d.size();
    }

    /* renamed from: a */
    public ReplyDetail getItem(int i2) {
        if (this.d == null) {
            return null;
        }
        return this.d.get(i2);
    }

    public long getItemId(int i2) {
        return (long) i2;
    }

    public View getView(int i2, View view, ViewGroup viewGroup) {
        ap apVar;
        if (view == null || view.getTag() == null) {
            ap apVar2 = new ap(this, null);
            view = this.b.inflate((int) R.layout.comment_reply_item, (ViewGroup) null);
            apVar2.f3098a = (TXAppIconView) view.findViewById(R.id.picture);
            apVar2.b = (TextView) view.findViewById(R.id.nick_name);
            apVar2.f = (TextView) view.findViewById(R.id.flowernumber);
            apVar2.c = (TextView) view.findViewById(R.id.myorfriends);
            apVar2.d = (TextView) view.findViewById(R.id.content);
            apVar2.e = (TextView) view.findViewById(R.id.time);
            apVar2.g = (TextView) view.findViewById(R.id.likecount);
            apVar2.h = (TextView) view.findViewById(R.id.likeanimation);
            apVar2.i = (ImageView) view.findViewById(R.id.replycount);
            view.setTag(apVar2);
            apVar = apVar2;
        } else {
            apVar = (ap) view.getTag();
        }
        a(apVar, i2);
        return view;
    }

    private void a(ap apVar, int i2) {
        ReplyDetail replyDetail;
        if (this.d == null || i2 > this.d.size() - 1) {
            replyDetail = null;
        } else {
            replyDetail = this.d.get(i2);
        }
        if (apVar != null && replyDetail != null) {
            apVar.f3098a.updateImageView(replyDetail.f, R.drawable.common_icon_useravarta_default, TXImageView.TXImageViewType.ROUND_IMAGE);
            if (replyDetail.e) {
                apVar.b.setText((int) R.string.comment_nick_my_reply);
                apVar.b.setTextColor(this.f3079a.getResources().getColor(R.color.comment_nickname_highlight_text));
                apVar.c.setVisibility(8);
            } else if (replyDetail.n) {
                apVar.b.setText((int) R.string.comment_nick_owner_reply);
                apVar.b.setTextColor(this.f3079a.getResources().getColor(R.color.comment_nickname_highlight_text));
                apVar.c.setVisibility(8);
            } else if (replyDetail.g == 3) {
                apVar.b.setText((int) R.string.comment_nick_kaifazhe_reply);
                apVar.b.setTextColor(this.f3079a.getResources().getColor(R.color.comment_nickname_kaifazhe_text));
                apVar.c.setVisibility(8);
            } else if (TextUtils.isEmpty(replyDetail.d)) {
                apVar.b.setText((int) R.string.comment_default_nick_name);
                apVar.b.setTextColor(this.f3079a.getResources().getColor(R.color.comment_content));
                apVar.c.setVisibility(8);
            } else if (!replyDetail.d.startsWith("(好友)")) {
                apVar.b.setText(replyDetail.d);
                apVar.b.setTextColor(this.f3079a.getResources().getColor(R.color.comment_content));
                apVar.c.setVisibility(8);
            } else {
                String substring = replyDetail.d.substring(4);
                if (substring.length() > 8) {
                    substring = substring.substring(0, 8) + "...";
                }
                if (TextUtils.isEmpty(substring)) {
                    apVar.b.setText((int) R.string.comment_default_nick_name);
                    apVar.b.setTextColor(this.f3079a.getResources().getColor(R.color.comment_content));
                    apVar.c.setVisibility(8);
                } else {
                    apVar.b.setText(substring);
                    apVar.b.setTextColor(Color.parseColor("#ff7a0c"));
                    apVar.c.setVisibility(0);
                }
            }
            if (replyDetail.l > 0) {
                apVar.f.setVisibility(0);
                apVar.f.setText(replyDetail.l + "楼");
            } else {
                apVar.f.setVisibility(8);
            }
            if (!TextUtils.isEmpty(replyDetail.c)) {
                String str = Constants.STR_EMPTY;
                if (replyDetail.m > 0) {
                    str = "回复" + replyDetail.m + "楼";
                }
                if (!TextUtils.isEmpty(str)) {
                    SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(str + ", " + replyDetail.c);
                    ForegroundColorSpan foregroundColorSpan = new ForegroundColorSpan(this.f3079a.getResources().getColor(R.color.comment_content));
                    ForegroundColorSpan foregroundColorSpan2 = new ForegroundColorSpan(Color.parseColor("#ff7a0c"));
                    spannableStringBuilder.setSpan(foregroundColorSpan, 0, "回复".length(), 33);
                    spannableStringBuilder.setSpan(foregroundColorSpan2, "回复".length(), str.length(), 18);
                    apVar.d.setText(spannableStringBuilder);
                } else {
                    apVar.d.setText(replyDetail.c);
                }
            }
            apVar.e.setText(bo.g(replyDetail.b * 1000));
            apVar.g.setText(bm.a(replyDetail.j) + Constants.STR_EMPTY);
            Drawable drawable = this.f3079a.getResources().getDrawable(R.drawable.pinglun_icon_zan);
            drawable.setBounds(0, 0, drawable.getMinimumWidth(), drawable.getMinimumHeight());
            Drawable drawable2 = this.f3079a.getResources().getDrawable(R.drawable.pinglun_icon_yizan);
            drawable2.setBounds(0, 0, drawable2.getMinimumWidth(), drawable2.getMinimumHeight());
            apVar.g.setCompoundDrawablePadding(by.a(this.f3079a, 6.0f));
            if (replyDetail.k == 1) {
                apVar.g.setCompoundDrawables(drawable2, null, null, null);
                apVar.g.setTextColor(Color.parseColor("#b68a46"));
            } else {
                apVar.g.setCompoundDrawables(drawable, null, null, null);
                apVar.g.setTextColor(Color.parseColor("#a4a4a4"));
            }
            apVar.g.setTag(R.id.tma_st_slot_tag, bm.a(i2 + 1));
            apVar.g.setOnClickListener(new am(this, replyDetail, i2, apVar));
            apVar.i.setTag(R.id.tma_st_slot_tag, bm.a(i2 + 2));
            apVar.i.setOnClickListener(new an(this, replyDetail));
            l.a(new STInfoV2(STConst.ST_PAGE_COMMENT_REPLY, this.k + bm.a(i2 + 2), 0, STConst.ST_DEFAULT_SLOT, 100));
        }
    }

    public List<ReplyDetail> a() {
        return this.d;
    }

    public void a(aq aqVar) {
        this.h = aqVar;
    }
}
