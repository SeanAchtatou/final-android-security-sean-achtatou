package com.tencent.nucleus.socialcontact.guessfavor;

import android.widget.ImageView;
import com.tencent.assistant.utils.a;
import com.tencent.assistantv2.component.k;
import com.tencent.pangu.download.DownloadInfo;

/* compiled from: ProGuard */
class f extends k {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ GuessFavorAdapter f3148a;

    f(GuessFavorAdapter guessFavorAdapter) {
        this.f3148a = guessFavorAdapter;
    }

    public void a(DownloadInfo downloadInfo) {
        ImageView imageView = (ImageView) this.f3148a.e.findViewWithTag(downloadInfo.downloadTicket);
        if (imageView != null) {
            a.a(imageView);
        }
        com.tencent.pangu.download.a.a().a(downloadInfo);
    }
}
