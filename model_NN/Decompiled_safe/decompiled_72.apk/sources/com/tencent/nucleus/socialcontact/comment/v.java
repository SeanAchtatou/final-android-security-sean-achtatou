package com.tencent.nucleus.socialcontact.comment;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.module.BaseEngine;
import com.tencent.assistant.protocol.jce.AnswerAppCommentRequest;
import com.tencent.assistant.protocol.jce.AnswerAppCommentResponse;
import com.tencent.assistant.utils.XLog;
import java.util.HashMap;

/* compiled from: ProGuard */
public class v extends BaseEngine<y> {

    /* renamed from: a  reason: collision with root package name */
    private HashMap<Long, Integer> f3138a = new HashMap<>();

    public void a(int i, long j, String str, String str2, String str3, long j2, String str4, int i2) {
        if (this.f3138a.containsKey(Long.valueOf(j))) {
            cancel(this.f3138a.get(Long.valueOf(j)).intValue());
            this.f3138a.remove(Long.valueOf(j));
        }
        AnswerAppCommentRequest answerAppCommentRequest = new AnswerAppCommentRequest();
        answerAppCommentRequest.b = i;
        answerAppCommentRequest.f1129a = j;
        answerAppCommentRequest.e = str;
        answerAppCommentRequest.d = str2;
        answerAppCommentRequest.f = str3;
        answerAppCommentRequest.g = j2;
        answerAppCommentRequest.h = str4;
        answerAppCommentRequest.i = i2;
        XLog.d("comment", "CommentReplyAnswerEngine.sendRequest, AnswerAppCommentRequest=" + answerAppCommentRequest.toString());
        this.f3138a.put(Long.valueOf(j), Integer.valueOf(send(answerAppCommentRequest)));
    }

    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i, JceStruct jceStruct, JceStruct jceStruct2) {
        AnswerAppCommentRequest answerAppCommentRequest = (AnswerAppCommentRequest) jceStruct;
        this.f3138a.remove(Long.valueOf(answerAppCommentRequest.f1129a));
        if (jceStruct2 != null) {
            AnswerAppCommentResponse answerAppCommentResponse = (AnswerAppCommentResponse) jceStruct2;
            XLog.d("comment", "CommentReplyAnswerEngine.onRequestSuccessed, AnswerAppCommentRequest=" + answerAppCommentRequest.toString() + ";AnswerAppCommentResponse=" + answerAppCommentResponse.toString());
            notifyDataChangedInMainThread(new w(this, i, answerAppCommentResponse, answerAppCommentRequest));
        }
    }

    /* access modifiers changed from: protected */
    public void onRequestFailed(int i, int i2, JceStruct jceStruct, JceStruct jceStruct2) {
        this.f3138a.remove(Long.valueOf(((AnswerAppCommentRequest) jceStruct).f1129a));
        XLog.d("comment", "CommentReplyAnswerEngine.onRequestFailed, errorCode=" + i2);
        notifyDataChangedInMainThread(new x(this, i, i2));
    }
}
