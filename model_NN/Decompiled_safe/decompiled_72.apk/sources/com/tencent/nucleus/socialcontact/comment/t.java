package com.tencent.nucleus.socialcontact.comment;

import android.text.TextUtils;
import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;

/* compiled from: ProGuard */
class t extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ CommentReplyActivity f3136a;

    t(CommentReplyActivity commentReplyActivity) {
        this.f3136a = commentReplyActivity;
    }

    public void onTMAClick(View view) {
        if (this.f3136a.w != null && this.f3136a.x != null && !TextUtils.isEmpty(this.f3136a.u.getText())) {
            this.f3136a.v.a(0, this.f3136a.w.h, this.f3136a.u.getText().toString(), this.f3136a.w.c, this.f3136a.w.i, this.f3136a.x.f938a, this.f3136a.x.c);
            this.f3136a.finish();
        }
    }

    public STInfoV2 getStInfo() {
        STInfoV2 t = this.f3136a.t();
        if (t != null) {
            t.slotId = a.a("05", "003");
            t.actionId = 200;
        }
        return t;
    }
}
