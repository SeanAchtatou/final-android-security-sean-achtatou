package com.tencent.nucleus.socialcontact.comment;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.module.BaseEngine;
import com.tencent.assistant.protocol.jce.CommentTagInfo;
import com.tencent.assistant.protocol.jce.GetCommentListRequest;
import com.tencent.assistant.protocol.jce.GetCommentListResponse;
import com.tencent.assistant.utils.XLog;
import com.tencent.nucleus.socialcontact.login.j;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class k extends BaseEngine<j> {

    /* renamed from: a  reason: collision with root package name */
    private long f3127a = 0;
    private long b = 0;
    private String c;
    private int d = 0;
    /* access modifiers changed from: private */
    public boolean e = true;
    /* access modifiers changed from: private */
    public byte[] f;
    /* access modifiers changed from: private */
    public p g = new p(this);
    /* access modifiers changed from: private */
    public CommentTagInfo h;
    private boolean i = true;
    private int j = -1;
    private int k = -1;
    private ArrayList<CommentTagInfo> l = null;
    private boolean m = false;

    public boolean a() {
        return this.e;
    }

    public int a(long j2, long j3, String str, int i2, CommentTagInfo commentTagInfo, ArrayList<CommentTagInfo> arrayList) {
        if (j2 == 0) {
            return -1;
        }
        this.f3127a = j2;
        this.b = j3;
        this.c = str;
        this.d = i2;
        this.h = commentTagInfo;
        this.l = arrayList;
        if (this.j > 0) {
            cancel(this.j);
        }
        this.j = a((byte[]) null);
        return this.j;
    }

    public int b() {
        if (this.f3127a == 0 || this.b == 0 || this.f == null || this.f.length == 0) {
            return -1;
        }
        if (this.g.c()) {
            int a2 = this.g.a();
            this.g.b();
            return a2;
        } else if (this.f != this.g.d() || this.g.e() == null) {
            if (this.k > 0) {
                cancel(this.k);
            }
            this.k = a(this.f);
            return this.k;
        } else {
            int a3 = this.g.a();
            runOnUiThread(new l(this, this.g.e(), a3));
            return a3;
        }
    }

    public boolean c() {
        return this.m;
    }

    /* access modifiers changed from: private */
    public int a(byte[] bArr) {
        GetCommentListRequest getCommentListRequest = new GetCommentListRequest();
        getCommentListRequest.f1272a = this.f3127a;
        getCommentListRequest.b = this.b;
        getCommentListRequest.c = 10;
        getCommentListRequest.f = d();
        getCommentListRequest.g = this.d;
        if (bArr == null) {
            if (j.a().j()) {
                this.m = true;
            } else {
                this.m = false;
            }
        }
        if (bArr == null) {
            bArr = new byte[0];
        }
        getCommentListRequest.d = bArr;
        getCommentListRequest.h = this.h;
        if (this.l != null && this.l.size() > 0) {
            this.i = false;
        }
        getCommentListRequest.i = this.i;
        XLog.d("comment", "CommentDetailEngine.sendRequest, GetCommentListRequest=" + getCommentListRequest.toString());
        this.j = send(getCommentListRequest);
        return this.j;
    }

    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i2, JceStruct jceStruct, JceStruct jceStruct2) {
        boolean z;
        if (jceStruct2 != null) {
            GetCommentListResponse getCommentListResponse = (GetCommentListResponse) jceStruct2;
            XLog.d("comment", "CommentDetailEngine.onRequestSucessed, GetCommentListResponse=");
            if (i2 == this.g.a()) {
                this.g.a(getCommentListResponse);
                return;
            }
            GetCommentListRequest getCommentListRequest = (GetCommentListRequest) jceStruct;
            if (this.f != getCommentListResponse.d) {
                this.e = getCommentListResponse.c == 1;
                this.f = getCommentListResponse.d;
                if (getCommentListRequest.d == null || getCommentListRequest.d.length == 0) {
                    z = true;
                } else {
                    z = false;
                }
                ArrayList arrayList = new ArrayList();
                if (this.i) {
                    if (getCommentListResponse.j != null) {
                        arrayList.addAll(getCommentListResponse.j);
                    }
                } else if (this.l != null) {
                    arrayList.addAll(this.l);
                }
                notifyDataChangedInMainThread(new n(this, i2, z, getCommentListResponse, arrayList));
                if (this.e) {
                    this.g.a(this.f);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onRequestFailed(int i2, int i3, JceStruct jceStruct, JceStruct jceStruct2) {
        boolean z = false;
        XLog.d("comment", "CommentDetailEngine.onRequestFailed, errorCode=" + i3);
        if (this.m) {
            this.m = false;
        }
        if (i2 != this.g.a()) {
            GetCommentListRequest getCommentListRequest = (GetCommentListRequest) jceStruct;
            if (getCommentListRequest.d == null || getCommentListRequest.d.length == 0) {
                z = true;
            }
            notifyDataChangedInMainThread(new o(this, i2, i3, z, getCommentListRequest));
            return;
        }
        this.g.f();
    }

    private int d() {
        LocalApkInfo localApkInfo = ApkResourceManager.getInstance().getLocalApkInfo(this.c);
        if (localApkInfo != null) {
            return localApkInfo.mVersionCode;
        }
        return -1;
    }
}
