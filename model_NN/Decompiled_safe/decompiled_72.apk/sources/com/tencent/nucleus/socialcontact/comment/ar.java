package com.tencent.nucleus.socialcontact.comment;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.module.BaseEngine;
import com.tencent.assistant.protocol.jce.GetReplyListRequest;
import com.tencent.assistant.protocol.jce.GetReplyListResponse;
import com.tencent.assistant.protocol.jce.ReplyDetail;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class ar extends BaseEngine<y> {

    /* renamed from: a  reason: collision with root package name */
    private int f3099a = -1;
    private int b = -1;
    private byte[] c;
    /* access modifiers changed from: private */
    public boolean d = true;
    private av e = new av(this);
    private long f;
    private long g;
    /* access modifiers changed from: private */
    public long h;
    private int i = 10;

    private int a() {
        if (this.f3099a > 0) {
            cancel(this.f3099a);
        }
        this.f3099a = a(-1, (byte[]) null);
        return this.f3099a;
    }

    /* access modifiers changed from: private */
    public int a(byte[] bArr) {
        if (this.b > 0) {
            cancel(this.b);
        }
        this.b = a(-1, bArr);
        return this.b;
    }

    private int a(int i2, byte[] bArr) {
        GetReplyListRequest getReplyListRequest = new GetReplyListRequest();
        getReplyListRequest.f1326a = this.f;
        getReplyListRequest.b = this.g;
        getReplyListRequest.c = this.h;
        getReplyListRequest.d = this.i;
        if (bArr == null) {
            bArr = new byte[0];
        }
        getReplyListRequest.e = bArr;
        return send(i2, getReplyListRequest);
    }

    public void a(long j, long j2, long j3) {
        this.f = j;
        this.g = j2;
        this.h = j3;
        a();
    }

    public int a(int i2) {
        this.i = i2;
        if (this.c == null || this.c.length == 0) {
            return -1;
        }
        if (this.e.c()) {
            int a2 = this.e.a();
            this.e.b();
            return a2;
        } else if (this.c != this.e.d() || this.e.e() == null) {
            return a(this.c);
        } else {
            av i3 = this.e.clone();
            int a3 = i3.a();
            ArrayList arrayList = new ArrayList(i3.e());
            this.d = i3.f();
            this.c = i3.g();
            notifyDataChangedInMainThread(new as(this, a3, arrayList, i3));
            if (this.e.d) {
                this.e.a(this.c);
            }
            return this.e.a();
        }
    }

    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i2, JceStruct jceStruct, JceStruct jceStruct2) {
        boolean z = false;
        boolean z2 = true;
        if (jceStruct2 != null) {
            GetReplyListResponse getReplyListResponse = (GetReplyListResponse) jceStruct2;
            GetReplyListRequest getReplyListRequest = (GetReplyListRequest) jceStruct;
            if (i2 == this.e.a()) {
                av avVar = this.e;
                ArrayList<ReplyDetail> arrayList = getReplyListResponse.e;
                if (getReplyListResponse.c != 1) {
                    z2 = false;
                }
                avVar.a(arrayList, z2, getReplyListResponse.d, getReplyListResponse.b);
            } else if (this.c != getReplyListResponse.d) {
                this.d = getReplyListResponse.c == 1;
                this.c = getReplyListResponse.d;
                if (getReplyListRequest.e == null || getReplyListRequest.e.length == 0) {
                    z = true;
                }
                notifyDataChangedInMainThread(new at(this, i2, z, getReplyListResponse));
                if (this.d) {
                    this.e.a(this.c);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onRequestFailed(int i2, int i3, JceStruct jceStruct, JceStruct jceStruct2) {
        if (i2 != this.e.a()) {
            GetReplyListRequest getReplyListRequest = (GetReplyListRequest) jceStruct;
            notifyDataChangedInMainThread(new au(this, i2, i3, getReplyListRequest.e == null || getReplyListRequest.e.length == 0));
            return;
        }
        this.e.h();
    }
}
