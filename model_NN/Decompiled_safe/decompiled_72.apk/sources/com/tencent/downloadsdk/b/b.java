package com.tencent.downloadsdk.b;

import android.os.Handler;
import android.text.TextUtils;
import com.tencent.beacon.event.a;
import com.tencent.connect.common.Constants;
import com.tencent.downloadsdk.DownloadManager;
import com.tencent.downloadsdk.utils.f;
import com.tencent.downloadsdk.utils.g;
import com.tencent.downloadsdk.utils.j;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class b {

    /* renamed from: a  reason: collision with root package name */
    public static int f2467a = 20;
    public static Handler b;
    public static List<d> c = new ArrayList();

    private static HashMap<String, String> a(f fVar) {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("B1", fVar.f2469a);
        hashMap.put("B2", com.tencent.downloadsdk.b.b);
        hashMap.put("B3", com.tencent.downloadsdk.b.c);
        hashMap.put("B4", com.tencent.downloadsdk.b.d + Constants.STR_EMPTY);
        if (!TextUtils.isEmpty(com.tencent.downloadsdk.b.e)) {
            hashMap.put("B5", URLEncoder.encode(com.tencent.downloadsdk.b.e));
        }
        hashMap.put("B6", com.tencent.downloadsdk.b.h + Constants.STR_EMPTY);
        hashMap.put("B7", fVar.b + Constants.STR_EMPTY);
        hashMap.put("B8", fVar.c + Constants.STR_EMPTY);
        if (!TextUtils.isEmpty(com.tencent.downloadsdk.b.g)) {
            hashMap.put("B9", URLEncoder.encode(com.tencent.downloadsdk.b.g));
        }
        hashMap.put("B10", com.tencent.downloadsdk.b.b(DownloadManager.a().b()));
        hashMap.put("B11", String.valueOf(g.a()));
        hashMap.put("B12", String.valueOf(g.c()));
        if (!TextUtils.isEmpty(fVar.d)) {
            hashMap.put("B13", fVar.d);
        }
        hashMap.put("B14", fVar.e + Constants.STR_EMPTY);
        hashMap.put("B15", fVar.f + Constants.STR_EMPTY);
        hashMap.put("B16", fVar.g + Constants.STR_EMPTY);
        hashMap.put("B17", fVar.h + Constants.STR_EMPTY);
        hashMap.put("B18", fVar.i + Constants.STR_EMPTY);
        hashMap.put("B19", fVar.j + Constants.STR_EMPTY);
        hashMap.put("B20", fVar.k);
        hashMap.put("B21", fVar.l + Constants.STR_EMPTY);
        hashMap.put("B22", fVar.m + Constants.STR_EMPTY);
        if (fVar.o.size() > 0) {
            StringBuilder sb = new StringBuilder();
            for (a next : fVar.o.values()) {
                sb.append(next.f2466a).append(",");
                sb.append(URLEncoder.encode(next.b)).append(",");
                if (next.c != null) {
                    Iterator<String> it = next.c.iterator();
                    while (it.hasNext()) {
                        String next2 = it.next();
                        if (!TextUtils.isEmpty(next2)) {
                            sb.append(URLEncoder.encode(next2)).append("+");
                        }
                    }
                }
                sb.append(",");
                sb.append(URLEncoder.encode(next.d)).append(",");
                sb.append(next.e).append(",");
                sb.append(next.f).append(",");
                sb.append(next.h).append(",");
                sb.append(next.g).append(",");
                sb.append(next.i).append(",");
                sb.append(next.j).append(",");
                sb.append(next.k).append(",");
                sb.append(next.l).append(",");
                sb.append(next.m).append(",");
                sb.append(next.n).append(",");
                sb.append(next.o).append(",");
                if (next.p != null) {
                    Iterator<String> it2 = next.p.iterator();
                    while (it2.hasNext()) {
                        String next3 = it2.next();
                        if (!TextUtils.isEmpty(next3)) {
                            sb.append(next3).append("+");
                        }
                    }
                }
                sb.append(",");
                if (next.q != null) {
                    Iterator<String> it3 = next.q.iterator();
                    while (it3.hasNext()) {
                        String next4 = it3.next();
                        if (!TextUtils.isEmpty(next4)) {
                            sb.append(next4).append("+");
                        }
                    }
                }
                sb.append(",");
                if (!TextUtils.isEmpty(next.r)) {
                    sb.append(URLEncoder.encode(next.r));
                } else {
                    sb.append(next.r);
                }
                sb.append(";");
            }
            hashMap.put("B23", sb.toString());
        }
        hashMap.put("B25", fVar.n + Constants.STR_EMPTY);
        if (fVar.p != null && fVar.p.size() > 0) {
            StringBuilder sb2 = new StringBuilder();
            Iterator<String> it4 = fVar.p.iterator();
            while (it4.hasNext()) {
                sb2.append(it4.next()).append(",");
            }
            hashMap.put("B26", sb2.toString());
        }
        if (fVar.q != null && fVar.q.size() > 0) {
            hashMap.putAll(fVar.q);
        }
        hashMap.put("B30", (fVar.r ? 1 : 0) + Constants.STR_EMPTY);
        if (!TextUtils.isEmpty(fVar.s)) {
            hashMap.put("B111", fVar.s);
        }
        return hashMap;
    }

    public static void a(String str, f fVar, int i, String str2, long j, long j2, long j3, long j4, long j5, byte[] bArr, int i2, int i3) {
        HashMap<String, String> hashMap;
        if (fVar != null) {
            fVar.d = str2;
            fVar.e = j;
            fVar.j = j2;
            fVar.f = j3;
            fVar.g = j4;
            fVar.h = j5;
            fVar.n = i;
            if (bArr != null) {
                try {
                    fVar.k = new String(bArr);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            fVar.l = i2;
            fVar.m = i3;
            HashMap<String, String> a2 = a(fVar);
            boolean z = i2 == 0;
            a2.put("B110", str);
            f.a("TMDownloadSDK", a2.toString());
            if (a2 != null) {
                if (a2.toString().length() > 10240) {
                    a2 = new HashMap<>();
                    a2.put("B110", str);
                }
                a2.put("B123", "params size :" + a2.toString().length());
                hashMap = a2;
            } else {
                hashMap = a2;
            }
            if (!a.a("TMDownloadSDK", z, 0, 0, hashMap, true)) {
                d dVar = new d();
                dVar.f2468a = z;
                dVar.b = hashMap;
                c.add(dVar);
            }
            if (b == null) {
                b = j.a("reportToBeacon");
            }
            if (b != null) {
                b.post(new e());
                return;
            }
            return;
        }
        HashMap hashMap2 = new HashMap();
        boolean z2 = i2 == 0;
        hashMap2.put("B110", str);
        hashMap2.put("B123", "taskStatsInfo is null");
        a.a("TMDownloadSDK", z2, 0, 0, hashMap2, true);
    }
}
