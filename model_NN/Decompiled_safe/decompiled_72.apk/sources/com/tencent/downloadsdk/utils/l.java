package com.tencent.downloadsdk.utils;

import android.text.TextUtils;

public final class l {

    /* renamed from: a  reason: collision with root package name */
    public String f2519a;
    public String b;
    public String c;
    public String d;
    public String e;
    public String f;
    public int g;
    public int h;
    public int i;

    private String b() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("0");
        stringBuffer.append(this.b.subSequence(0, 1));
        stringBuffer.append(this.c);
        return stringBuffer.toString();
    }

    public String a() {
        String b2 = b();
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("TMASDK_");
        stringBuffer.append(this.b);
        if (!TextUtils.isEmpty(this.d)) {
            stringBuffer.append("_");
            stringBuffer.append(this.d);
        }
        stringBuffer.append("/");
        stringBuffer.append(b2);
        stringBuffer.append("&NA/");
        stringBuffer.append(b2);
        stringBuffer.append("&");
        stringBuffer.append(this.f);
        stringBuffer.append("_");
        stringBuffer.append(this.i);
        stringBuffer.append("&");
        stringBuffer.append(this.g / 16);
        stringBuffer.append("_");
        stringBuffer.append(this.h / 16);
        stringBuffer.append("_");
        stringBuffer.append("14&");
        stringBuffer.append(this.f2519a);
        stringBuffer.append("&");
        stringBuffer.append(this.e);
        stringBuffer.append("&");
        stringBuffer.append("NA");
        stringBuffer.append("&");
        stringBuffer.append("V3");
        return stringBuffer.toString();
    }
}
