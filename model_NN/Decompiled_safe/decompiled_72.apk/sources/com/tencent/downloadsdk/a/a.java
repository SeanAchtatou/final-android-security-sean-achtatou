package com.tencent.downloadsdk.a;

import com.tencent.downloadsdk.utils.f;

public class a {

    /* renamed from: a  reason: collision with root package name */
    public long f2450a;
    public long b;
    public long c;
    public long d;
    public long e;
    private long f;

    public void a() {
        this.f2450a = System.currentTimeMillis();
    }

    public void b() {
        this.f = System.currentTimeMillis();
    }

    public void c() {
        this.d = (this.d + System.currentTimeMillis()) - this.f;
    }

    public void d() {
        this.b = System.currentTimeMillis();
        this.c = this.b - this.f2450a;
        this.e = this.c - this.d;
        f.a("FileWriterSpeedProbe", "FileWriter cost time:" + this.c);
        f.a("FileWriterSpeedProbe", "FileWriter idle time:" + this.d);
        f.a("FileWriterSpeedProbe", "FileWriter write time:" + this.e);
    }
}
