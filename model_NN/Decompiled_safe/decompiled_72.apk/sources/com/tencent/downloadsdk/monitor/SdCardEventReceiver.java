package com.tencent.downloadsdk.monitor;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class SdCardEventReceiver extends BroadcastReceiver {
    private boolean a(Intent intent) {
        return intent.getAction().equals("android.intent.action.MEDIA_MOUNTED");
    }

    public void onReceive(Context context, Intent intent) {
        f.a().a(a(intent));
    }
}
