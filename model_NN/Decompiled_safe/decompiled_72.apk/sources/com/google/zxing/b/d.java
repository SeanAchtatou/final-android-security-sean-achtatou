package com.google.zxing.b;

import com.google.zxing.ChecksumException;
import com.google.zxing.FormatException;
import com.google.zxing.NotFoundException;
import com.google.zxing.common.a;
import com.google.zxing.h;
import com.google.zxing.j;
import java.util.Hashtable;

public final class d extends k {

    /* renamed from: a  reason: collision with root package name */
    private static final char[] f140a = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ-. $/+%abcd*".toCharArray();
    private static final int[] b = {276, 328, 324, 322, 296, 292, 290, 336, 274, 266, 424, 420, 418, 404, 402, 394, 360, 356, 354, 308, 282, 344, 332, 326, 300, 278, 436, 434, 428, 422, 406, 410, 364, 358, 310, 314, 302, 468, 466, 458, 366, 374, 430, 294, 474, 470, 306, 350};
    private static final int c = b[47];

    private static char a(int i) {
        for (int i2 = 0; i2 < b.length; i2++) {
            if (b[i2] == i) {
                return f140a[i2];
            }
        }
        throw NotFoundException.a();
    }

    private static int a(int[] iArr) {
        int length = iArr.length;
        int i = 0;
        int i2 = 0;
        while (i < length) {
            i++;
            i2 = iArr[i] + i2;
        }
        int i3 = 0;
        for (int i4 = 0; i4 < length; i4++) {
            int i5 = ((iArr[i4] << 8) * 9) / i2;
            int i6 = i5 >> 8;
            int i7 = (i5 & 255) > 127 ? i6 + 1 : i6;
            if (i7 < 1 || i7 > 4) {
                return -1;
            }
            if ((i4 & 1) == 0) {
                int i8 = 0;
                while (i8 < i7) {
                    i8++;
                    i3 = (i3 << 1) | 1;
                }
            } else {
                i3 <<= i7;
            }
        }
        return i3;
    }

    private static String a(StringBuffer stringBuffer) {
        int i;
        char c2;
        int length = stringBuffer.length();
        StringBuffer stringBuffer2 = new StringBuffer(length);
        int i2 = 0;
        while (i2 < length) {
            char charAt = stringBuffer.charAt(i2);
            if (charAt < 'a' || charAt > 'd') {
                stringBuffer2.append(charAt);
                i = i2;
            } else {
                char charAt2 = stringBuffer.charAt(i2 + 1);
                switch (charAt) {
                    case 'a':
                        if (charAt2 >= 'A' && charAt2 <= 'Z') {
                            c2 = (char) (charAt2 - '@');
                            break;
                        } else {
                            throw FormatException.a();
                        }
                    case 'b':
                        if (charAt2 < 'A' || charAt2 > 'E') {
                            if (charAt2 >= 'F' && charAt2 <= 'W') {
                                c2 = (char) (charAt2 - 11);
                                break;
                            } else {
                                throw FormatException.a();
                            }
                        } else {
                            c2 = (char) (charAt2 - '&');
                            break;
                        }
                        break;
                    case 'c':
                        if (charAt2 >= 'A' && charAt2 <= 'O') {
                            c2 = (char) (charAt2 - ' ');
                            break;
                        } else if (charAt2 == 'Z') {
                            c2 = ':';
                            break;
                        } else {
                            throw FormatException.a();
                        }
                        break;
                    case 'd':
                        if (charAt2 >= 'A' && charAt2 <= 'Z') {
                            c2 = (char) (charAt2 + ' ');
                            break;
                        } else {
                            throw FormatException.a();
                        }
                    default:
                        c2 = 0;
                        break;
                }
                stringBuffer2.append(c2);
                i = i2 + 1;
            }
            i2 = i + 1;
        }
        return stringBuffer2.toString();
    }

    private static void a(StringBuffer stringBuffer, int i, int i2) {
        int i3 = 1;
        int i4 = i - 1;
        int i5 = 0;
        while (i4 >= 0) {
            int indexOf = ("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ-. $/+%abcd*".indexOf(stringBuffer.charAt(i4)) * i3) + i5;
            int i6 = i3 + 1;
            if (i6 > i2) {
                i6 = 1;
            }
            i4--;
            i3 = i6;
            i5 = indexOf;
        }
        if (stringBuffer.charAt(i) != f140a[i5 % 47]) {
            throw ChecksumException.a();
        }
    }

    private static int[] a(a aVar) {
        int a2 = aVar.a();
        int i = 0;
        while (i < a2 && !aVar.a(i)) {
            i++;
        }
        int[] iArr = new int[6];
        int length = iArr.length;
        boolean z = false;
        int i2 = 0;
        for (int i3 = i; i3 < a2; i3++) {
            if (aVar.a(i3) ^ z) {
                iArr[i2] = iArr[i2] + 1;
            } else {
                if (i2 != length - 1) {
                    i2++;
                } else if (a(iArr) == c) {
                    return new int[]{i, i3};
                } else {
                    i += iArr[0] + iArr[1];
                    for (int i4 = 2; i4 < length; i4++) {
                        iArr[i4 - 2] = iArr[i4];
                    }
                    iArr[length - 2] = 0;
                    iArr[length - 1] = 0;
                    i2--;
                }
                iArr[i2] = 1;
                z = !z;
            }
        }
        throw NotFoundException.a();
    }

    private static void b(StringBuffer stringBuffer) {
        int length = stringBuffer.length();
        a(stringBuffer, length - 2, 20);
        a(stringBuffer, length - 1, 15);
    }

    public h a(int i, a aVar, Hashtable hashtable) {
        int[] a2 = a(aVar);
        int i2 = a2[1];
        int a3 = aVar.a();
        while (i2 < a3 && !aVar.a(i2)) {
            i2++;
        }
        StringBuffer stringBuffer = new StringBuffer(20);
        int[] iArr = new int[6];
        while (true) {
            a(aVar, i2, iArr);
            int a4 = a(iArr);
            if (a4 < 0) {
                throw NotFoundException.a();
            }
            char a5 = a(a4);
            stringBuffer.append(a5);
            int i3 = i2;
            for (int i4 : iArr) {
                i3 += i4;
            }
            int i5 = i3;
            while (i5 < a3 && !aVar.a(i5)) {
                i5++;
            }
            if (a5 == '*') {
                stringBuffer.deleteCharAt(stringBuffer.length() - 1);
                if (i5 == a3 || !aVar.a(i5)) {
                    throw NotFoundException.a();
                } else if (stringBuffer.length() < 2) {
                    throw NotFoundException.a();
                } else {
                    b(stringBuffer);
                    stringBuffer.setLength(stringBuffer.length() - 2);
                    return new h(a(stringBuffer), null, new j[]{new j(((float) (a2[0] + a2[1])) / 2.0f, (float) i), new j(((float) (i2 + i5)) / 2.0f, (float) i)}, com.google.zxing.a.j);
                }
            } else {
                i2 = i5;
            }
        }
    }
}
