package com.google.zxing.common.a;

import com.google.zxing.NotFoundException;
import com.google.zxing.common.b;
import com.google.zxing.j;

public final class a {

    /* renamed from: a  reason: collision with root package name */
    private final b f157a;
    private final int b;
    private final int c;

    public a(b bVar) {
        this.f157a = bVar;
        this.b = bVar.c();
        this.c = bVar.b();
    }

    private static int a(float f) {
        return (int) (0.5f + f);
    }

    private j a(float f, float f2, float f3, float f4) {
        int b2 = b(f, f2, f3, f4);
        float f5 = (f3 - f) / ((float) b2);
        float f6 = (f4 - f2) / ((float) b2);
        for (int i = 0; i < b2; i++) {
            int a2 = a((((float) i) * f5) + f);
            int a3 = a((((float) i) * f6) + f2);
            if (this.f157a.a(a2, a3)) {
                return new j((float) a2, (float) a3);
            }
        }
        return null;
    }

    private boolean a(int i, int i2, int i3, boolean z) {
        if (z) {
            while (i <= i2) {
                if (this.f157a.a(i, i3)) {
                    return true;
                }
                i++;
            }
        } else {
            while (i <= i2) {
                if (this.f157a.a(i3, i)) {
                    return true;
                }
                i++;
            }
        }
        return false;
    }

    private j[] a(j jVar, j jVar2, j jVar3, j jVar4) {
        float a2 = jVar.a();
        float b2 = jVar.b();
        float a3 = jVar2.a();
        float b3 = jVar2.b();
        float a4 = jVar3.a();
        float b4 = jVar3.b();
        float a5 = jVar4.a();
        float b5 = jVar4.b();
        if (a2 < ((float) (this.c / 2))) {
            return new j[]{new j(a5 - 1.0f, b5 + 1.0f), new j(a3 + 1.0f, b3 + 1.0f), new j(a4 - 1.0f, b4 - 1.0f), new j(a2 + 1.0f, b2 - 1.0f)};
        }
        return new j[]{new j(a5 + 1.0f, b5 + 1.0f), new j(a3 + 1.0f, b3 - 1.0f), new j(a4 - 1.0f, b4 + 1.0f), new j(a2 - 1.0f, b2 - 1.0f)};
    }

    private static int b(float f, float f2, float f3, float f4) {
        float f5 = f - f3;
        float f6 = f2 - f4;
        return a((float) Math.sqrt((double) ((f5 * f5) + (f6 * f6))));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.zxing.common.a.a.a(int, int, int, boolean):boolean
     arg types: [int, int, int, int]
     candidates:
      com.google.zxing.common.a.a.a(float, float, float, float):com.google.zxing.j
      com.google.zxing.common.a.a.a(com.google.zxing.j, com.google.zxing.j, com.google.zxing.j, com.google.zxing.j):com.google.zxing.j[]
      com.google.zxing.common.a.a.a(int, int, int, boolean):boolean */
    public j[] a() {
        int i;
        int i2;
        int i3;
        int i4;
        j jVar;
        j jVar2;
        j jVar3;
        boolean z = false;
        int i5 = (this.c - 40) >> 1;
        int i6 = (this.c + 40) >> 1;
        int i7 = (this.b - 40) >> 1;
        int i8 = (this.b + 40) >> 1;
        boolean z2 = false;
        boolean z3 = true;
        while (true) {
            if (!z3) {
                i = i6;
                i2 = i8;
                i3 = i5;
                i4 = i7;
                break;
            }
            boolean z4 = true;
            boolean z5 = false;
            while (z4 && i6 < this.c) {
                z4 = a(i7, i8, i6, false);
                if (z4) {
                    i6++;
                    z5 = true;
                }
            }
            if (i6 >= this.c) {
                z = true;
                i = i6;
                i2 = i8;
                i3 = i5;
                i4 = i7;
                break;
            }
            boolean z6 = true;
            while (z6 && i8 < this.b) {
                z6 = a(i5, i6, i8, true);
                if (z6) {
                    i8++;
                    z5 = true;
                }
            }
            if (i8 >= this.b) {
                z = true;
                i = i6;
                i2 = i8;
                i3 = i5;
                i4 = i7;
                break;
            }
            boolean z7 = true;
            while (z7 && i5 >= 0) {
                z7 = a(i7, i8, i5, false);
                if (z7) {
                    i5--;
                    z5 = true;
                }
            }
            if (i5 < 0) {
                z = true;
                i = i6;
                i2 = i8;
                i3 = i5;
                i4 = i7;
                break;
            }
            z3 = z5;
            boolean z8 = true;
            while (z8 && i7 >= 0) {
                z8 = a(i5, i6, i7, true);
                if (z8) {
                    i7--;
                    z3 = true;
                }
            }
            if (i7 < 0) {
                z = true;
                i = i6;
                i2 = i8;
                i3 = i5;
                i4 = i7;
                break;
            } else if (z3) {
                z2 = true;
            }
        }
        if (z || !z2) {
            throw NotFoundException.a();
        }
        int i9 = i - i3;
        int i10 = 1;
        j jVar4 = null;
        while (true) {
            if (i10 >= i9) {
                jVar = jVar4;
                break;
            }
            jVar4 = a((float) i3, (float) (i2 - i10), (float) (i3 + i10), (float) i2);
            if (jVar4 != null) {
                jVar = jVar4;
                break;
            }
            i10++;
        }
        if (jVar == null) {
            throw NotFoundException.a();
        }
        int i11 = 1;
        j jVar5 = null;
        while (true) {
            if (i11 >= i9) {
                jVar2 = jVar5;
                break;
            }
            jVar5 = a((float) i3, (float) (i4 + i11), (float) (i3 + i11), (float) i4);
            if (jVar5 != null) {
                jVar2 = jVar5;
                break;
            }
            i11++;
        }
        if (jVar2 == null) {
            throw NotFoundException.a();
        }
        int i12 = 1;
        j jVar6 = null;
        while (true) {
            if (i12 >= i9) {
                jVar3 = jVar6;
                break;
            }
            jVar6 = a((float) i, (float) (i4 + i12), (float) (i - i12), (float) i4);
            if (jVar6 != null) {
                jVar3 = jVar6;
                break;
            }
            i12++;
        }
        if (jVar3 == null) {
            throw NotFoundException.a();
        }
        j jVar7 = null;
        for (int i13 = 1; i13 < i9; i13++) {
            jVar7 = a((float) i, (float) (i2 - i13), (float) (i - i13), (float) i2);
            if (jVar7 != null) {
                break;
            }
        }
        if (jVar7 != null) {
            return a(jVar7, jVar, jVar3, jVar2);
        }
        throw NotFoundException.a();
    }
}
