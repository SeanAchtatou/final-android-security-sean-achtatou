package com.qq.AppService;

import android.content.Context;
import android.net.wifi.WifiManager;
import android.os.PowerManager;
import com.qq.provider.h;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
public final class ax {

    /* renamed from: a  reason: collision with root package name */
    public static volatile boolean f230a = false;
    public static String b = null;
    static PowerManager.WakeLock d = null;
    static volatile WifiManager.WifiLock e = null;
    private static volatile ax h = null;
    Context c = null;
    private ay f = null;
    private av g = null;

    public static ax a() {
        if (h == null) {
            h = new ax();
        }
        return h;
    }

    public static synchronized void a(Context context) {
        synchronized (ax.class) {
            Context b2 = AppService.b();
            if (b2 != null) {
                context = b2;
            }
            if (context != null) {
                try {
                    if (e == null) {
                        e = ((WifiManager) context.getSystemService("wifi")).createWifiLock(1, Constants.STR_EMPTY + System.currentTimeMillis());
                    }
                    if (e != null && !e.isHeld()) {
                        e.acquire();
                    }
                } catch (Throwable th) {
                    th.printStackTrace();
                }
            }
        }
        return;
    }

    public static synchronized void b() {
        synchronized (ax.class) {
            if (d != null && d.isHeld()) {
                d.release();
                d = null;
            }
            if (e != null && e.isHeld()) {
                e.release();
                e = null;
            }
        }
    }

    private ax() {
    }

    public synchronized void a(Context context, int i) {
        this.c = context.getApplicationContext();
        f230a = true;
        System.setProperty("java.net.preferIPv6Addresses", "false");
        h.a(true);
        if (this.g == null) {
            this.g = new av(context.getApplicationContext());
        }
        if (this.g != null && !this.g.b) {
            this.g.a();
        }
    }

    public synchronized void c() {
        if (d != null && d.isHeld()) {
            d.release();
        }
        f230a = false;
        h.a(false);
        synchronized (ax.class) {
            if (e != null && e.isHeld()) {
                e.release();
            }
        }
        if (this.g != null) {
            this.g.b();
        }
        this.g = null;
        h = null;
    }
}
