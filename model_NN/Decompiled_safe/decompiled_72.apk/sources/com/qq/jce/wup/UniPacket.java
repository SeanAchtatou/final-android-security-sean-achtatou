package com.qq.jce.wup;

import com.qq.taf.RequestPacket;
import com.qq.taf.jce.JceInputStream;
import com.qq.taf.jce.JceOutputStream;
import com.qq.taf.jce.JceUtil;
import com.tencent.connect.common.Constants;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;

public class UniPacket extends UniAttribute {
    public static final int UniPacketHeadSize = 4;
    static HashMap<String, HashMap<String, byte[]>> cache__tempdata = null;
    static HashMap<String, byte[]> newCache__tempdata = null;
    protected RequestPacket _package = new RequestPacket();
    private int oldRespIret = 0;
    private int packetSize = 0;

    public UniPacket() {
        this._package.iVersion = 2;
    }

    public UniPacket(boolean useVersion3) {
        if (useVersion3) {
            useVersion3();
        } else {
            this._package.iVersion = 2;
        }
    }

    public int getPackageVersion() {
        return this._package.iVersion;
    }

    public <T> void put(String name, T t) {
        if (name.startsWith(".")) {
            throw new IllegalArgumentException("put name can not startwith . , now is " + name);
        }
        super.put(name, t);
    }

    public void useVersion3() {
        super.useVersion3();
        this._package.iVersion = 3;
    }

    public int getPacketSize() {
        return this.packetSize;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceOutputStream.write(java.util.Map, int):void
     arg types: [java.util.HashMap, int]
     candidates:
      com.qq.taf.jce.JceOutputStream.write(byte, int):void
      com.qq.taf.jce.JceOutputStream.write(double, int):void
      com.qq.taf.jce.JceOutputStream.write(float, int):void
      com.qq.taf.jce.JceOutputStream.write(int, int):void
      com.qq.taf.jce.JceOutputStream.write(long, int):void
      com.qq.taf.jce.JceOutputStream.write(com.qq.taf.jce.JceStruct, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Boolean, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Byte, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Double, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Float, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Integer, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Long, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Object, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Short, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.String, int):void
      com.qq.taf.jce.JceOutputStream.write(java.util.Collection, int):void
      com.qq.taf.jce.JceOutputStream.write(short, int):void
      com.qq.taf.jce.JceOutputStream.write(boolean, int):void
      com.qq.taf.jce.JceOutputStream.write(byte[], int):void
      com.qq.taf.jce.JceOutputStream.write(double[], int):void
      com.qq.taf.jce.JceOutputStream.write(float[], int):void
      com.qq.taf.jce.JceOutputStream.write(int[], int):void
      com.qq.taf.jce.JceOutputStream.write(long[], int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Object[], int):void
      com.qq.taf.jce.JceOutputStream.write(short[], int):void
      com.qq.taf.jce.JceOutputStream.write(boolean[], int):void
      com.qq.taf.jce.JceOutputStream.write(java.util.Map, int):void */
    public byte[] encode() {
        if (this._package.iVersion != 2) {
            if (this._package.sServantName == null) {
                this._package.sServantName = Constants.STR_EMPTY;
            }
            if (this._package.sFuncName == null) {
                this._package.sFuncName = Constants.STR_EMPTY;
            }
        } else if (this._package.sServantName == null || this._package.sServantName.equals(Constants.STR_EMPTY)) {
            throw new IllegalArgumentException("servantName can not is null");
        } else if (this._package.sFuncName == null || this._package.sFuncName.equals(Constants.STR_EMPTY)) {
            throw new IllegalArgumentException("funcName can not is null");
        }
        JceOutputStream _os = new JceOutputStream(0);
        _os.setServerEncoding(this.encodeName);
        if (this._package.iVersion == 2 || this._package.iVersion == 1) {
            _os.write((Map) this._data, 0);
        } else {
            _os.write((Map) this._newData, 0);
        }
        this._package.sBuffer = JceUtil.getJceBufArray(_os.getByteBuffer());
        JceOutputStream _os2 = new JceOutputStream(0);
        _os2.setServerEncoding(this.encodeName);
        this._package.writeTo(_os2);
        byte[] bodys = JceUtil.getJceBufArray(_os2.getByteBuffer());
        int size = bodys.length;
        ByteBuffer buf = ByteBuffer.allocate(size + 4);
        buf.putInt(size + 4).put(bodys).flip();
        return buf.array();
    }

    public void decodeVersion3(byte[] buffer) {
        if (buffer.length < 4) {
            throw new IllegalArgumentException("decode package must include size head");
        }
        try {
            JceInputStream _is = new JceInputStream(buffer, 4);
            _is.setServerEncoding(this.encodeName);
            this._package.readFrom(_is);
            parseBufferV3();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private void parseBufferV3() {
        JceInputStream _is = new JceInputStream(this._package.sBuffer);
        _is.setServerEncoding(this.encodeName);
        if (newCache__tempdata == null) {
            newCache__tempdata = new HashMap<>();
            newCache__tempdata.put(Constants.STR_EMPTY, new byte[0]);
        }
        this._newData = _is.readMap(newCache__tempdata, 0, false);
    }

    public void decodeVersion2(byte[] buffer) {
        if (buffer.length < 4) {
            throw new IllegalArgumentException("decode package must include size head");
        }
        ByteBuffer temp = ByteBuffer.allocate(4);
        byte[] headerBytes = new byte[4];
        System.arraycopy(buffer, 0, headerBytes, 0, headerBytes.length);
        temp.put(headerBytes).flip();
        this.packetSize = temp.getInt();
        try {
            JceInputStream _is = new JceInputStream(buffer, 4);
            _is.setServerEncoding(this.encodeName);
            this._package.readFrom(_is);
            parseBufferV2();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private void parseBufferV2() {
        JceInputStream _is = new JceInputStream(this._package.sBuffer);
        _is.setServerEncoding(this.encodeName);
        if (cache__tempdata == null) {
            cache__tempdata = new HashMap<>();
            HashMap<String, byte[]> h = new HashMap<>();
            h.put(Constants.STR_EMPTY, new byte[0]);
            cache__tempdata.put(Constants.STR_EMPTY, h);
        }
        this._data = _is.readMap(cache__tempdata, 0, false);
        this.cachedClassName = new HashMap();
    }

    public void decode(byte[] buffer) {
        if (buffer.length < 4) {
            throw new IllegalArgumentException("decode package must include size head");
        }
        ByteBuffer temp = ByteBuffer.allocate(4);
        byte[] headerBytes = new byte[4];
        System.arraycopy(buffer, 0, headerBytes, 0, headerBytes.length);
        temp.put(headerBytes).flip();
        this.packetSize = temp.getInt();
        try {
            JceInputStream _is = new JceInputStream(buffer, 4);
            _is.setServerEncoding(this.encodeName);
            this._package.readFrom(_is);
            if (this._package.iVersion == 3) {
                parseBufferV3();
                return;
            }
            this._newData = null;
            parseBufferV2();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public String getServantName() {
        return this._package.sServantName;
    }

    public void setServantName(String servantName) {
        this._package.sServantName = servantName;
    }

    public String getFuncName() {
        return this._package.sFuncName;
    }

    public void setFuncName(String sFuncName) {
        this._package.sFuncName = sFuncName;
    }

    public int getRequestId() {
        return this._package.iRequestId;
    }

    public void setRequestId(int iRequestId) {
        this._package.iRequestId = iRequestId;
    }

    public void writeTo(JceOutputStream _os) {
        this._package.writeTo(_os);
    }

    public void readFrom(JceInputStream _is) {
        this._package.readFrom(_is);
    }

    public void display(StringBuilder _os, int _level) {
        this._package.display(_os, _level);
    }

    public UniPacket createResponse() {
        UniPacket packet = new UniPacket();
        packet.setRequestId(getRequestId());
        packet.setServantName(getServantName());
        packet.setFuncName(getFuncName());
        packet.setEncodeName(this.encodeName);
        packet._package.iVersion = this._package.iVersion;
        return packet;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceOutputStream.write(java.util.Map, int):void
     arg types: [java.util.HashMap, int]
     candidates:
      com.qq.taf.jce.JceOutputStream.write(byte, int):void
      com.qq.taf.jce.JceOutputStream.write(double, int):void
      com.qq.taf.jce.JceOutputStream.write(float, int):void
      com.qq.taf.jce.JceOutputStream.write(int, int):void
      com.qq.taf.jce.JceOutputStream.write(long, int):void
      com.qq.taf.jce.JceOutputStream.write(com.qq.taf.jce.JceStruct, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Boolean, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Byte, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Double, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Float, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Integer, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Long, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Object, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Short, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.String, int):void
      com.qq.taf.jce.JceOutputStream.write(java.util.Collection, int):void
      com.qq.taf.jce.JceOutputStream.write(short, int):void
      com.qq.taf.jce.JceOutputStream.write(boolean, int):void
      com.qq.taf.jce.JceOutputStream.write(byte[], int):void
      com.qq.taf.jce.JceOutputStream.write(double[], int):void
      com.qq.taf.jce.JceOutputStream.write(float[], int):void
      com.qq.taf.jce.JceOutputStream.write(int[], int):void
      com.qq.taf.jce.JceOutputStream.write(long[], int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Object[], int):void
      com.qq.taf.jce.JceOutputStream.write(short[], int):void
      com.qq.taf.jce.JceOutputStream.write(boolean[], int):void
      com.qq.taf.jce.JceOutputStream.write(java.util.Map, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceOutputStream.write(java.util.Map, int):void
     arg types: [java.util.Map<java.lang.String, java.lang.String>, int]
     candidates:
      com.qq.taf.jce.JceOutputStream.write(byte, int):void
      com.qq.taf.jce.JceOutputStream.write(double, int):void
      com.qq.taf.jce.JceOutputStream.write(float, int):void
      com.qq.taf.jce.JceOutputStream.write(int, int):void
      com.qq.taf.jce.JceOutputStream.write(long, int):void
      com.qq.taf.jce.JceOutputStream.write(com.qq.taf.jce.JceStruct, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Boolean, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Byte, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Double, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Float, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Integer, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Long, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Object, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Short, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.String, int):void
      com.qq.taf.jce.JceOutputStream.write(java.util.Collection, int):void
      com.qq.taf.jce.JceOutputStream.write(short, int):void
      com.qq.taf.jce.JceOutputStream.write(boolean, int):void
      com.qq.taf.jce.JceOutputStream.write(byte[], int):void
      com.qq.taf.jce.JceOutputStream.write(double[], int):void
      com.qq.taf.jce.JceOutputStream.write(float[], int):void
      com.qq.taf.jce.JceOutputStream.write(int[], int):void
      com.qq.taf.jce.JceOutputStream.write(long[], int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Object[], int):void
      com.qq.taf.jce.JceOutputStream.write(short[], int):void
      com.qq.taf.jce.JceOutputStream.write(boolean[], int):void
      com.qq.taf.jce.JceOutputStream.write(java.util.Map, int):void */
    public byte[] createOldRespEncode() {
        JceOutputStream _os = new JceOutputStream(0);
        _os.setServerEncoding(this.encodeName);
        _os.write((Map) this._data, 0);
        byte[] oldSBuffer = JceUtil.getJceBufArray(_os.getByteBuffer());
        JceOutputStream _os2 = new JceOutputStream(0);
        _os2.setServerEncoding(this.encodeName);
        _os2.write(this._package.iVersion, 1);
        _os2.write(this._package.cPacketType, 2);
        _os2.write(this._package.iRequestId, 3);
        _os2.write(this._package.iMessageType, 4);
        _os2.write(this.oldRespIret, 5);
        _os2.write(oldSBuffer, 6);
        _os2.write((Map) this._package.status, 7);
        return JceUtil.getJceBufArray(_os2.getByteBuffer());
    }

    public int getOldRespIret() {
        return this.oldRespIret;
    }

    public void setOldRespIret(int oldRespIret2) {
        this.oldRespIret = oldRespIret2;
    }
}
