package com.qq.d.g;

import android.os.SystemProperties;
import android.text.TextUtils;
import java.util.Map;

/* compiled from: ProGuard */
public class v extends r {
    public boolean a(Map<String, String> map, boolean z, s sVar) {
        if (!TextUtils.isEmpty(SystemProperties.get("romzj.rom.id"))) {
            a(map, "shuame", "true");
        } else {
            a(map, "shuame", "false");
        }
        if (this.b == null) {
            return false;
        }
        return this.b.a(map, z, sVar);
    }
}
