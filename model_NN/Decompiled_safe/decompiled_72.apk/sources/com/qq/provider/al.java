package com.qq.provider;

import android.util.Log;
import com.qq.d.b;
import com.tencent.tmsecurelite.commom.DataEntity;
import com.tencent.tmsecurelite.virusscan.ScanListenerStub;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
class al extends ScanListenerStub {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ak f329a;

    al(ak akVar) {
        this.f329a = akVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.provider.ak.a(com.qq.provider.ak, boolean):boolean
     arg types: [com.qq.provider.ak, int]
     candidates:
      com.qq.provider.ak.a(com.qq.provider.ak, com.qq.provider.an):com.qq.provider.an
      com.qq.provider.ak.a(com.qq.provider.ak, com.tencent.tmsecurelite.virusscan.IVirusScan):com.tencent.tmsecurelite.virusscan.IVirusScan
      com.qq.provider.ak.a(int, int):void
      com.qq.provider.ak.a(int, com.qq.d.b):void
      com.qq.provider.ak.a(int, java.util.ArrayList<com.qq.d.b>):void
      com.qq.provider.ak.a(com.qq.provider.ak, byte[]):void
      com.qq.provider.ak.a(boolean, com.tencent.tmsecurelite.virusscan.IVirusScan):void
      com.qq.provider.ak.a(com.qq.provider.ak, int):byte[]
      com.qq.provider.ak.a(android.content.Context, com.qq.g.c):void
      com.qq.provider.ak.a(com.qq.provider.ak, boolean):boolean */
    public void onScanStarted() {
        Log.d("com.qq.connect", "onScanStarted");
        boolean unused = this.f329a.d = true;
        if (this.f329a.i == null) {
            h.a(2);
        } else {
            this.f329a.a(this.f329a.b(2));
        }
    }

    public void onPackageScanProgress(int i, DataEntity dataEntity) {
        b bVar = new b(i, dataEntity);
        Log.d("com.qq.connect", "onPackageScanProgress " + bVar.b + bVar.c);
        if (this.f329a.i == null) {
            h.a(3, bVar);
        } else {
            this.f329a.a(3, bVar);
        }
    }

    public void onSdcardScanProgress(int i, DataEntity dataEntity) {
        b bVar = new b(i, dataEntity);
        Log.d("com.qq.connect", "onPackageScanProgress " + bVar.b + bVar.c);
        if (this.f329a.i == null) {
            h.a(4, bVar);
        } else {
            this.f329a.a(4, bVar);
        }
    }

    public void onCloudScan() {
        Log.d("com.qq.connect", "onCloudScan");
        if (this.f329a.i == null) {
            h.a(5);
        } else {
            this.f329a.a(5);
        }
    }

    public void onCloudScanError(int i) {
        Log.d("com.qq.connect", "onCloudScanError" + i);
        if (this.f329a.i == null) {
            h.a(6, i);
        } else {
            this.f329a.a(6, i);
        }
    }

    public void onScanPaused() {
        Log.d("com.qq.connect", "onScanPaused");
        if (this.f329a.i == null) {
            h.a(7);
        } else {
            this.f329a.a(7);
        }
    }

    public void onScanContinue() {
        Log.d("com.qq.connect", "onScanContinue");
        if (this.f329a.i == null) {
            h.a(8);
        } else {
            this.f329a.a(8);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.provider.ak.a(com.qq.provider.ak, boolean):boolean
     arg types: [com.qq.provider.ak, int]
     candidates:
      com.qq.provider.ak.a(com.qq.provider.ak, com.qq.provider.an):com.qq.provider.an
      com.qq.provider.ak.a(com.qq.provider.ak, com.tencent.tmsecurelite.virusscan.IVirusScan):com.tencent.tmsecurelite.virusscan.IVirusScan
      com.qq.provider.ak.a(int, int):void
      com.qq.provider.ak.a(int, com.qq.d.b):void
      com.qq.provider.ak.a(int, java.util.ArrayList<com.qq.d.b>):void
      com.qq.provider.ak.a(com.qq.provider.ak, byte[]):void
      com.qq.provider.ak.a(boolean, com.tencent.tmsecurelite.virusscan.IVirusScan):void
      com.qq.provider.ak.a(com.qq.provider.ak, int):byte[]
      com.qq.provider.ak.a(android.content.Context, com.qq.g.c):void
      com.qq.provider.ak.a(com.qq.provider.ak, boolean):boolean */
    public void onScanCanceled() {
        Log.d("com.qq.connect", "onScanCanceled");
        boolean unused = this.f329a.d = false;
        if (this.f329a.i == null) {
            h.a(9);
        } else {
            this.f329a.a(9);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.provider.ak.a(com.qq.provider.ak, boolean):boolean
     arg types: [com.qq.provider.ak, int]
     candidates:
      com.qq.provider.ak.a(com.qq.provider.ak, com.qq.provider.an):com.qq.provider.an
      com.qq.provider.ak.a(com.qq.provider.ak, com.tencent.tmsecurelite.virusscan.IVirusScan):com.tencent.tmsecurelite.virusscan.IVirusScan
      com.qq.provider.ak.a(int, int):void
      com.qq.provider.ak.a(int, com.qq.d.b):void
      com.qq.provider.ak.a(int, java.util.ArrayList<com.qq.d.b>):void
      com.qq.provider.ak.a(com.qq.provider.ak, byte[]):void
      com.qq.provider.ak.a(boolean, com.tencent.tmsecurelite.virusscan.IVirusScan):void
      com.qq.provider.ak.a(com.qq.provider.ak, int):byte[]
      com.qq.provider.ak.a(android.content.Context, com.qq.g.c):void
      com.qq.provider.ak.a(com.qq.provider.ak, boolean):boolean */
    public void onScanFinished(List<DataEntity> list) {
        Log.d("com.qq.connect", "onScanFinished");
        boolean unused = this.f329a.d = false;
        ArrayList<b> a2 = b.a(list);
        if (this.f329a.i != null) {
            h.a(10, a2);
        } else {
            this.f329a.a(10, a2);
        }
    }
}
