package com.qq.provider;

import android.content.Context;
import android.database.Cursor;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.Log;
import com.qq.AppService.AppService;
import com.qq.AppService.ac;
import com.qq.AppService.ag;
import com.qq.AppService.ap;
import com.qq.AppService.aq;
import com.qq.AppService.ar;
import com.qq.AppService.at;
import com.qq.AppService.r;
import com.qq.a.a.d;
import com.qq.d.b;
import com.qq.d.c;
import com.qq.e.a;
import com.qq.m.j;
import com.tencent.connect.common.Constants;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class h {

    /* renamed from: a  reason: collision with root package name */
    public static ar f341a = null;
    public static volatile boolean b = false;
    public static at c = null;
    public static ag d = null;
    public static volatile aq e = null;
    public static a f = null;
    private static ap g = null;

    public static void a(Context context, String str) {
        if (f != null && !AppService.e) {
            f.a(context, str);
        }
    }

    public static void a() {
        if (f != null) {
            f.a();
        }
    }

    public static boolean b() {
        if (e == null) {
            return false;
        }
        return e.b;
    }

    private h() {
    }

    public static h c() {
        return new h();
    }

    public static byte[] a(ArrayList<byte[]> arrayList) {
        int i = 0;
        for (int i2 = 0; i2 < arrayList.size(); i2++) {
            i += arrayList.get(i2).length + 4;
        }
        byte[] bArr = new byte[(i + 4)];
        System.arraycopy(r.a(i), 0, bArr, 0, 4);
        int i3 = 4;
        for (int i4 = 0; i4 < arrayList.size(); i4++) {
            int length = arrayList.get(i4).length;
            System.arraycopy(r.a(length), 0, bArr, i3, 4);
            int i5 = i3 + 4;
            System.arraycopy(arrayList.get(i4), 0, bArr, i5, length);
            i3 = i5 + length;
        }
        return bArr;
    }

    private static ArrayList<byte[]> a(com.qq.d.f.a aVar, ArrayList<byte[]> arrayList) {
        if (arrayList == null) {
            arrayList = new ArrayList<>();
        } else {
            arrayList.clear();
        }
        arrayList.add(r.a(1));
        arrayList.add(r.a(1));
        for (int i = 0; i < 1; i++) {
            arrayList.add(r.a(aVar.f280a));
            arrayList.add(r.a(aVar.b));
            arrayList.add(r.a(aVar.c));
            arrayList.add(r.a(aVar.d));
            arrayList.add(r.a(aVar.e));
            arrayList.add(r.a(aVar.f));
            arrayList.add(r.a(aVar.g));
            arrayList.add(r.a(aVar.h));
            arrayList.add(r.a(aVar.i));
            arrayList.add(r.a(aVar.j));
        }
        return arrayList;
    }

    public static void a(com.qq.d.f.a aVar) {
        ArrayList arrayList = new ArrayList();
        a(aVar, arrayList);
        byte[] a2 = a(arrayList);
        arrayList.clear();
        if (e != null) {
            e.a(a2);
            Log.d("com.qq.connect", "got sms and send SMS to PC" + aVar.f280a);
            return;
        }
        Log.d("com.qq.connect", "got sms but server is null");
    }

    public static void a(int i) {
        if (b && e != null) {
            byte[] bArr = new byte[20];
            System.arraycopy(r.a(16), 0, bArr, 0, 4);
            System.arraycopy(r.a(4), 0, bArr, 4, 4);
            System.arraycopy(r.a(16), 0, bArr, 8, 4);
            System.arraycopy(r.a(4), 0, bArr, 12, 4);
            System.arraycopy(r.a(i), 0, bArr, 16, 4);
            if (b) {
                e.a(bArr);
            }
        }
    }

    public static void b(int i) {
        if (b && e != null) {
            byte[] bArr = new byte[20];
            System.arraycopy(r.a(16), 0, bArr, 0, 4);
            System.arraycopy(r.a(4), 0, bArr, 4, 4);
            System.arraycopy(r.a(20), 0, bArr, 8, 4);
            System.arraycopy(r.a(4), 0, bArr, 12, 4);
            System.arraycopy(r.a(i), 0, bArr, 16, 4);
            if (b) {
                e.a(bArr);
            }
        }
    }

    public static void a(int i, long j) {
        if (b && e != null) {
            byte[] bArr = new byte[32];
            System.arraycopy(r.a(bArr.length - 4), 0, bArr, 0, 4);
            System.arraycopy(r.a(4), 0, bArr, 4, 4);
            System.arraycopy(r.a(21), 0, bArr, 8, 4);
            System.arraycopy(r.a(4), 0, bArr, 12, 4);
            System.arraycopy(r.a(i), 0, bArr, 16, 4);
            System.arraycopy(r.a(8), 0, bArr, 20, 4);
            System.arraycopy(r.a(j), 0, bArr, 24, 8);
            if (b) {
                e.a(bArr);
            }
        }
    }

    public static void a(j jVar) {
        if (b && e != null) {
            ArrayList arrayList = new ArrayList();
            jVar.a(arrayList);
            int size = arrayList.size();
            int i = 0;
            int i2 = 12;
            while (i < size) {
                int length = i2 + 4 + ((byte[]) arrayList.get(i)).length;
                i++;
                i2 = length;
            }
            byte[] bArr = new byte[i2];
            System.arraycopy(r.a(i2 - 4), 0, bArr, 0, 4);
            System.arraycopy(r.a(4), 0, bArr, 4, 4);
            System.arraycopy(r.a(22), 0, bArr, 8, 4);
            int i3 = 12;
            for (int i4 = 0; i4 < size; i4++) {
                byte[] bArr2 = (byte[]) arrayList.get(i4);
                System.arraycopy(r.a(bArr2.length), 0, bArr, i3, 4);
                int i5 = i3 + 4;
                System.arraycopy(bArr2, 0, bArr, i5, bArr2.length);
                i3 = i5 + bArr2.length;
            }
            if (b) {
                e.a(bArr);
            }
        }
    }

    public static void a(int i, int i2) {
        if (b && e != null) {
            byte[] bArr = new byte[28];
            System.arraycopy(r.a(24), 0, bArr, 0, 4);
            System.arraycopy(r.a(4), 0, bArr, 4, 4);
            System.arraycopy(r.a(16), 0, bArr, 8, 4);
            System.arraycopy(r.a(4), 0, bArr, 12, 4);
            System.arraycopy(r.a(i), 0, bArr, 16, 4);
            System.arraycopy(r.a(4), 0, bArr, 20, 4);
            System.arraycopy(r.a(i2), 0, bArr, 24, 4);
            if (b) {
                e.a(bArr);
            }
        }
    }

    public static void a(int i, b bVar) {
        if (b && e != null && bVar != null) {
            ArrayList arrayList = new ArrayList();
            arrayList.add(r.a(16));
            arrayList.add(r.a(i));
            arrayList.add(r.a(bVar.f270a));
            arrayList.add(r.a(bVar.b));
            arrayList.add(r.a(bVar.c));
            arrayList.add(r.a(bVar.d));
            arrayList.add(r.a(bVar.e));
            arrayList.add(r.a(bVar.f));
            arrayList.add(r.a(bVar.g));
            arrayList.add(r.a(bVar.h));
            arrayList.add(r.a(bVar.i));
            arrayList.add(r.a(bVar.j));
            byte[] a2 = a(arrayList);
            if (b) {
                e.a(a2);
            }
        }
    }

    public static void a(int i, ArrayList<b> arrayList) {
        int i2;
        int i3;
        int i4 = 0;
        if (b && e != null) {
            ArrayList arrayList2 = new ArrayList();
            arrayList2.add(r.a(16));
            arrayList2.add(r.a(i));
            if (arrayList != null) {
                i2 = arrayList.size();
            } else {
                i2 = 0;
            }
            arrayList2.add(r.a(i2));
            if (i2 > 0) {
                int i5 = 0;
                while (i5 < i2) {
                    b bVar = arrayList.get(i5);
                    if (bVar == null) {
                        i3 = i4;
                    } else {
                        arrayList2.add(r.a(bVar.b));
                        arrayList2.add(r.a(bVar.c));
                        arrayList2.add(r.a(bVar.d));
                        arrayList2.add(r.a(bVar.e));
                        arrayList2.add(r.a(bVar.f));
                        arrayList2.add(r.a(bVar.g));
                        arrayList2.add(r.a(bVar.h));
                        arrayList2.add(r.a(bVar.i));
                        arrayList2.add(r.a(bVar.j));
                        i3 = i4 + 1;
                    }
                    i5++;
                    i4 = i3;
                }
                if (i2 != i4) {
                    arrayList2.set(2, r.a(i4));
                }
            }
            byte[] a2 = a(arrayList2);
            if (b) {
                e.a(a2);
            }
        }
    }

    public static void d() {
        if (b && e != null) {
            byte[] bArr = new byte[20];
            System.arraycopy(r.a(16), 0, bArr, 0, 4);
            System.arraycopy(r.a(4), 0, bArr, 4, 4);
            System.arraycopy(r.a(15), 0, bArr, 8, 4);
            System.arraycopy(r.a(4), 0, bArr, 12, 4);
            System.arraycopy(r.a(0), 0, bArr, 16, 4);
            if (b) {
                e.a(bArr);
            }
        }
    }

    public static void a(int i, int i2, int i3) {
        if (b && e != null) {
            byte[] bArr = new byte[36];
            System.arraycopy(r.a(32), 0, bArr, 0, 4);
            byte[] a2 = r.a(4);
            System.arraycopy(r.a(4), 0, bArr, 4, 4);
            System.arraycopy(r.a(9), 0, bArr, 8, 4);
            System.arraycopy(a2, 0, bArr, 12, 4);
            System.arraycopy(r.a(i), 0, bArr, 16, 4);
            System.arraycopy(a2, 0, bArr, 20, 4);
            System.arraycopy(r.a(i2), 0, bArr, 24, 4);
            System.arraycopy(a2, 0, bArr, 28, 4);
            System.arraycopy(r.a(i3), 0, bArr, 32, 4);
            if (b) {
                e.a(bArr);
            }
        }
    }

    public static void c(int i) {
        if (b && e != null) {
            byte[] bArr = new byte[20];
            System.arraycopy(r.a(16), 0, bArr, 0, 4);
            System.arraycopy(r.a(4), 0, bArr, 4, 4);
            System.arraycopy(r.a(14), 0, bArr, 8, 4);
            System.arraycopy(r.a(4), 0, bArr, 12, 4);
            System.arraycopy(r.a(i), 0, bArr, 16, 4);
            if (b) {
                e.a(bArr);
            }
        }
    }

    public static void a(boolean z) {
        int i;
        if (b && e != null) {
            byte[] bArr = new byte[20];
            System.arraycopy(r.a(16), 0, bArr, 0, 4);
            System.arraycopy(r.a(4), 0, bArr, 4, 4);
            System.arraycopy(r.a(2), 0, bArr, 8, 4);
            System.arraycopy(r.a(4), 0, bArr, 12, 4);
            if (!z) {
                i = 1;
            } else {
                i = 0;
            }
            System.arraycopy(r.a(i), 0, bArr, 16, 4);
            if (b) {
                e.a(bArr);
            }
        }
    }

    public static void a(int i, String str) {
        byte[] bArr;
        if (b && e != null) {
            byte[] bArr2 = null;
            if (i != 1) {
                bArr = new byte[20];
            } else {
                bArr2 = r.a(str);
                bArr = new byte[(bArr2.length + 24)];
            }
            System.arraycopy(r.a(bArr.length - 4), 0, bArr, 0, 4);
            System.arraycopy(r.a(4), 0, bArr, 4, 4);
            System.arraycopy(r.a(17), 0, bArr, 8, 4);
            System.arraycopy(r.a(4), 0, bArr, 12, 4);
            System.arraycopy(r.a(i), 0, bArr, 16, 4);
            if (i == 1) {
                System.arraycopy(r.a(bArr2.length), 0, bArr, 20, 4);
                System.arraycopy(bArr2, 0, bArr, 24, bArr2.length);
            }
            if (b) {
                e.a(bArr);
            }
        }
    }

    public static void d(int i) {
        Log.d("com.qq.connect", "sdcard change " + i);
        if (b && e != null) {
            byte[] bArr = new byte[20];
            System.arraycopy(r.a(16), 0, bArr, 0, 4);
            System.arraycopy(r.a(4), 0, bArr, 4, 4);
            System.arraycopy(r.a(3), 0, bArr, 8, 4);
            System.arraycopy(r.a(4), 0, bArr, 12, 4);
            System.arraycopy(r.a(i), 0, bArr, 16, 4);
            if (b) {
                e.a(bArr);
            }
        }
    }

    public static void e(int i) {
        if (b && e != null) {
            byte[] bArr = new byte[20];
            System.arraycopy(r.a(16), 0, bArr, 0, 4);
            System.arraycopy(r.a(4), 0, bArr, 4, 4);
            System.arraycopy(r.a(10), 0, bArr, 8, 4);
            System.arraycopy(r.a(4), 0, bArr, 12, 4);
            System.arraycopy(r.a(i), 0, bArr, 16, 4);
            if (b) {
                e.a(bArr);
            }
            try {
                Thread.sleep(200);
            } catch (InterruptedException e2) {
                e2.printStackTrace();
            }
        }
    }

    public static void b(int i, int i2) {
        if (e != null) {
            byte[] bArr = new byte[28];
            System.arraycopy(r.a(24), 0, bArr, 0, 4);
            System.arraycopy(r.a(4), 0, bArr, 4, 4);
            System.arraycopy(r.a(7), 0, bArr, 8, 4);
            System.arraycopy(r.a(4), 0, bArr, 12, 4);
            System.arraycopy(r.a(i), 0, bArr, 16, 4);
            System.arraycopy(r.a(4), 0, bArr, 20, 4);
            System.arraycopy(r.a(i2), 0, bArr, 24, 4);
            if (e != null) {
                e.a(bArr);
            }
        }
    }

    public static byte[] a(long j) {
        byte[] a2 = r.a(Constants.STR_EMPTY + j);
        int length = a2.length + 16;
        byte[] bArr = new byte[length];
        System.arraycopy(r.a(length - 4), 0, bArr, 0, 4);
        System.arraycopy(r.a(4), 0, bArr, 4, 4);
        System.arraycopy(r.a(13), 0, bArr, 8, 4);
        System.arraycopy(r.a(a2.length), 0, bArr, 12, 4);
        System.arraycopy(a2, 0, bArr, 16, a2.length);
        return bArr;
    }

    public static void a(int i, String str, boolean z, boolean z2) {
        int i2;
        if (e != null) {
            byte[] a2 = r.a(str);
            int length = a2.length + 28;
            byte[] bArr = new byte[(length + 4)];
            System.arraycopy(r.a(length), 0, bArr, 0, 4);
            System.arraycopy(r.a(4), 0, bArr, 4, 4);
            System.arraycopy(r.a(6), 0, bArr, 8, 4);
            System.arraycopy(r.a(4), 0, bArr, 12, 4);
            System.arraycopy(r.a(i), 0, bArr, 16, 4);
            if (z) {
                i2 = 1;
            } else {
                i2 = 0;
            }
            if (z2) {
                i2 += 4;
            }
            System.arraycopy(r.a(4), 0, bArr, 20, 4);
            System.arraycopy(r.a(i2), 0, bArr, 24, 4);
            System.arraycopy(r.a(a2.length), 0, bArr, 28, 4);
            System.arraycopy(a2, 0, bArr, 32, a2.length);
            if (e != null) {
                e.a(bArr);
            }
        }
    }

    public static long a(Context context, int[] iArr) {
        if (e == null || iArr == null || iArr.length == 0) {
            return -1;
        }
        long currentTimeMillis = System.currentTimeMillis();
        ArrayList arrayList = new ArrayList();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < iArr.length; i++) {
            if (i != 0) {
                sb.append(", ");
            }
            sb.append(iArr[i]);
        }
        Cursor query = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, null, "_id in (" + sb.toString() + ")", null, null);
        if (query == null) {
            return -1;
        }
        boolean moveToFirst = query.moveToFirst();
        int i2 = 0;
        while (moveToFirst) {
            o.a(new com.qq.d.d.a(query, null), arrayList);
            moveToFirst = query.moveToNext();
            i2++;
        }
        query.close();
        int size = arrayList.size();
        int i3 = 0;
        int i4 = 36;
        while (i3 < size) {
            int length = i4 + 4 + ((byte[]) arrayList.get(i3)).length;
            i3++;
            i4 = length;
        }
        byte[] bArr = new byte[(i4 + 4)];
        System.arraycopy(r.a(i4), 0, bArr, 0, 4);
        System.arraycopy(r.a(4), 0, bArr, 4, 4);
        System.arraycopy(r.a(18), 0, bArr, 8, 4);
        System.arraycopy(r.a(8), 0, bArr, 12, 4);
        System.arraycopy(r.a(currentTimeMillis), 0, bArr, 16, 8);
        System.arraycopy(r.a(4), 0, bArr, 24, 4);
        System.arraycopy(r.a(0), 0, bArr, 28, 4);
        System.arraycopy(r.a(4), 0, bArr, 32, 4);
        System.arraycopy(r.a(i2), 0, bArr, 36, 4);
        int i5 = 40;
        for (int i6 = 0; i6 < size; i6++) {
            byte[] bArr2 = (byte[]) arrayList.get(i6);
            System.arraycopy(r.a(bArr2.length), 0, bArr, i5, 4);
            int i7 = i5 + 4;
            System.arraycopy(bArr2, 0, bArr, i7, bArr2.length);
            i5 = i7 + bArr2.length;
        }
        if (e != null) {
            e.a(bArr);
        }
        return currentTimeMillis;
    }

    public static void c(int i, int i2) {
        if (e != null) {
            byte[] bArr = new byte[28];
            System.arraycopy(r.a(24), 0, bArr, 0, 4);
            System.arraycopy(r.a(4), 0, bArr, 4, 4);
            System.arraycopy(r.a(23), 0, bArr, 8, 4);
            System.arraycopy(r.a(4), 0, bArr, 12, 4);
            System.arraycopy(r.a(i), 0, bArr, 16, 4);
            System.arraycopy(r.a(4), 0, bArr, 20, 4);
            System.arraycopy(r.a(i2), 0, bArr, 24, 4);
            if (e != null) {
                e.a(bArr);
            }
        }
    }

    public static void a(c cVar) {
        if (e != null) {
            ArrayList arrayList = new ArrayList();
            cVar.a(arrayList);
            int size = arrayList.size();
            int i = 16;
            for (int i2 = 0; i2 < size; i2++) {
                i += ((byte[]) arrayList.get(i2)).length;
            }
            byte[] bArr = new byte[(i + 4)];
            System.arraycopy(r.a(i), 0, bArr, 0, 4);
            System.arraycopy(r.a(4), 0, bArr, 4, 4);
            System.arraycopy(r.a(23), 0, bArr, 8, 4);
            System.arraycopy(r.a(4), 0, bArr, 12, 4);
            System.arraycopy(r.a(3), 0, bArr, 16, 4);
            int i3 = 20;
            for (int i4 = 0; i4 < size; i4++) {
                byte[] bArr2 = (byte[]) arrayList.get(i4);
                System.arraycopy(bArr2, 0, bArr, i3, bArr2.length);
                i3 += bArr2.length;
            }
            if (e != null) {
                e.a(bArr);
            }
        }
    }

    public synchronized void a(Context context, com.qq.g.c cVar) {
        if (f == null) {
            f = new a();
            f.start();
        }
        if (e == null) {
            e = new aq();
            e.a(true);
            e.a(14089);
            e.a(f341a);
            if (e.f224a) {
                try {
                    e.start();
                } catch (Exception e2) {
                    e2.printStackTrace();
                    e = null;
                }
            } else {
                e = null;
            }
        }
        if (e != null) {
        }
        if (e == null || d != null) {
            d.c();
            d = new ag(context);
            d.b();
        } else {
            d = new ag(context);
            d.b();
        }
        ap.f223a = System.currentTimeMillis();
        if (e != null) {
            b = true;
            cVar.a(0);
            try {
                ac.a(context, ac.c(context));
            } catch (Exception e3) {
                e3.printStackTrace();
            }
        } else {
            b = false;
            cVar.a(8);
        }
        return;
    }

    public synchronized void b(Context context, com.qq.g.c cVar) {
        if (f != null) {
            f.b();
            f = null;
        }
        if (e != null) {
            e.a();
        }
        if (d != null) {
            d.c();
            d = null;
        }
        b = false;
        if (context != null) {
            ak.a(context);
            q.a(context);
        }
        cVar.a(0);
    }

    public static synchronized void a(Context context, Handler handler) {
        synchronized (h.class) {
            if (e == null) {
                e = new aq();
                e.a(true);
                e.a(14089);
                e.a(f341a);
                try {
                    e.start();
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
            if (d == null) {
                d = new ag(context);
            }
            if (g != null) {
                context.getContentResolver().unregisterContentObserver(g);
                g = null;
            }
            if (!(handler == null || context == null)) {
                g = new ap(context, handler);
                try {
                    context.getContentResolver().registerContentObserver(d.f258a, true, g);
                } catch (NullPointerException e3) {
                    e3.printStackTrace();
                }
            }
        }
        return;
    }

    public static synchronized void a(Context context) {
        synchronized (h.class) {
            b = false;
            if (e != null) {
                e.b();
            }
            e = null;
            if (d != null) {
                d.c();
                d = null;
            }
            if (g != null) {
                context.getContentResolver().unregisterContentObserver(g);
                g = null;
            }
        }
    }
}
