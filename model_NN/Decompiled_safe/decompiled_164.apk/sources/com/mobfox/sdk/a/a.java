package com.mobfox.sdk.a;

public enum a {
    INAPP,
    BROWSER;

    public static a a(String str) {
        for (a aVar : values()) {
            if (aVar.name().equalsIgnoreCase(str)) {
                return aVar;
            }
        }
        return null;
    }
}
