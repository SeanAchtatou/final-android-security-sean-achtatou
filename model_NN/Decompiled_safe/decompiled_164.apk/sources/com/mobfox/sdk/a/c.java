package com.mobfox.sdk.a;

import com.mobfox.sdk.m;

public final class c {

    /* renamed from: a  reason: collision with root package name */
    private String f144a;
    private String b;
    private m c;
    private String d;
    private String e;
    private double f = 0.0d;
    private double g = 0.0d;

    public final String a() {
        return this.f144a == null ? "" : this.f144a;
    }

    public final void a(double d2) {
        this.f = d2;
    }

    public final void a(m mVar) {
        this.c = mVar;
    }

    public final void a(String str) {
        this.f144a = str;
    }

    public final String b() {
        return this.b == null ? "" : this.b;
    }

    public final void b(double d2) {
        this.g = d2;
    }

    public final void b(String str) {
        this.b = str;
    }

    public final m c() {
        if (this.c == null) {
            this.c = m.LIVE;
        }
        return this.c;
    }

    public final void c(String str) {
        this.e = str;
    }

    public final String d() {
        return this.e == null ? "" : this.e;
    }

    public final void d(String str) {
        this.d = str;
    }

    public final double e() {
        return this.f;
    }

    public final double f() {
        return this.g;
    }

    public final String g() {
        return this.d == null ? "" : this.d;
    }
}
