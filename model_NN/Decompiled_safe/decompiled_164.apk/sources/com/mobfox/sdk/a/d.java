package com.mobfox.sdk.a;

import com.mobfox.sdk.l;

public final class d {

    /* renamed from: a  reason: collision with root package name */
    private l f145a;
    private int b;
    private int c;
    private String d;
    private String e;
    private a f;
    private String g;
    private b h;
    private int i;
    private boolean j;
    private boolean k;

    public final l a() {
        return this.f145a;
    }

    public final void a(int i2) {
        this.b = i2;
    }

    public final void a(a aVar) {
        this.f = aVar;
    }

    public final void a(l lVar) {
        this.f145a = lVar;
    }

    public final void a(String str) {
        this.e = str;
    }

    public final void a(boolean z) {
        this.j = z;
    }

    public final int b() {
        return this.b;
    }

    public final void b(int i2) {
        this.c = i2;
    }

    public final void b(String str) {
        this.d = str;
    }

    public final void b(boolean z) {
        this.k = z;
    }

    public final int c() {
        return this.c;
    }

    public final void c(int i2) {
        this.i = i2;
    }

    public final void c(String str) {
        this.g = str;
    }

    public final String d() {
        return this.e;
    }

    public final String e() {
        return this.d;
    }

    public final a f() {
        return this.f;
    }

    public final String g() {
        return this.g;
    }

    public final int h() {
        return this.i;
    }

    public final boolean i() {
        return this.k;
    }

    public final String toString() {
        return "Response [refresh=" + this.i + ", type=" + this.f145a + ", bannerWidth=" + this.b + ", bannerHeight=" + this.c + ", text=" + this.d + ", imageUrl=" + this.e + ", clickType=" + this.f + ", clickUrl=" + this.g + ", urlType=" + this.h + ", scale=" + this.j + ", skipPreflight=" + this.k + "]";
    }
}
