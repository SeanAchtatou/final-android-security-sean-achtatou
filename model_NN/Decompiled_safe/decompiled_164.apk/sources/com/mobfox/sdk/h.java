package com.mobfox.sdk;

public final class h extends Exception {
    public h() {
    }

    public h(String str) {
        super(str);
    }

    public h(String str, Throwable th) {
        super(str, th);
    }

    public h(Throwable th) {
        super(th);
    }
}
