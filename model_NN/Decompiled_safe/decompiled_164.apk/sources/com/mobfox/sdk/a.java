package com.mobfox.sdk;

import android.webkit.WebView;
import android.webkit.WebViewClient;

final class a extends WebViewClient {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ InAppWebView f141a;

    a(InAppWebView inAppWebView) {
        this.f141a = inAppWebView;
    }

    public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
        webView.loadUrl(str);
        return false;
    }
}
