package com.mobfox.sdk;

import android.util.Log;
import com.mobfox.sdk.a.d;

final class c implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ MobFoxView f147a;

    c(MobFoxView mobFoxView) {
        this.f147a = mobFoxView;
    }

    public final void run() {
        if (Log.isLoggable("MOBFOX", 3)) {
            Log.d("MOBFOX", "starting request thread");
        }
        new k();
        try {
            d unused = this.f147a.i = k.a(MobFoxView.d(this.f147a));
            if (this.f147a.i != null) {
                if (Log.isLoggable("MOBFOX", 3)) {
                    Log.d("MOBFOX", "response received");
                }
                if (Log.isLoggable("MOBFOX", 3)) {
                    Log.d("MOBFOX", "getVisibility: " + this.f147a.getVisibility());
                }
                this.f147a.f140a.post(this.f147a.b);
            }
        } catch (Throwable th) {
            if (Log.isLoggable("MOBFOX", 6)) {
                Log.e("MOBFOX", "Uncaught exception in request thread", th);
            }
            if (this.f147a.u != null) {
                Log.d("MOBFOX", "notify bannerListener: " + this.f147a.u.getClass().getName());
                if (!(th instanceof h)) {
                    new h(th);
                }
            }
            Log.e("MOBFOX", th.getMessage(), th);
        }
        Thread unused2 = this.f147a.w = null;
        if (Log.isLoggable("MOBFOX", 3)) {
            Log.d("MOBFOX", "finishing request thread");
        }
    }
}
