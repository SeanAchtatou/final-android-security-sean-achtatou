package com.mobfox.sdk;

import android.util.Log;
import com.mobfox.sdk.a.a;
import com.mobfox.sdk.a.c;
import com.mobfox.sdk.a.d;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public final class k {
    private static int a(String str) {
        if (str == null) {
            return 0;
        }
        try {
            return Integer.parseInt(str);
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    public static d a(c cVar) {
        return b(cVar);
    }

    private static d a(InputStream inputStream) {
        DocumentBuilderFactory newInstance = DocumentBuilderFactory.newInstance();
        d dVar = new d();
        try {
            Document parse = newInstance.newDocumentBuilder().parse(new InputSource(inputStream));
            Element documentElement = parse.getDocumentElement();
            if (documentElement == null) {
                throw new h("Cannot parse Response, document is not an xml");
            }
            String b = b(parse, "error");
            if (b != null) {
                throw new h("Error Response received: " + b);
            }
            String attribute = documentElement.getAttribute("type");
            documentElement.normalize();
            if ("imageAd".equalsIgnoreCase(attribute)) {
                dVar.a(l.IMAGE);
                dVar.a(c(parse, "bannerwidth"));
                dVar.b(c(parse, "bannerheight"));
                dVar.a(a.a(b(parse, "clicktype")));
                dVar.c(b(parse, "clickurl"));
                dVar.a(b(parse, "imageurl"));
                dVar.c(c(parse, "refresh"));
                dVar.a(a(parse, "scale"));
                dVar.b(a(parse, "skippreflight"));
            } else if ("textAd".equalsIgnoreCase(attribute)) {
                dVar.a(l.TEXT);
                dVar.b(b(parse, "htmlString"));
                dVar.a(a.a(b(parse, "clicktype")));
                dVar.c(b(parse, "clickurl"));
                dVar.c(c(parse, "refresh"));
                dVar.a(a(parse, "scale"));
                dVar.b(a(parse, "skippreflight"));
            } else if ("noAd".equalsIgnoreCase(attribute)) {
                dVar.a(l.NO_AD);
            } else {
                throw new h("Unknown response type " + attribute);
            }
            return dVar;
        } catch (ParserConfigurationException e) {
            throw new h("Cannot parse Response", e);
        } catch (SAXException e2) {
            throw new h("Cannot parse Response", e2);
        } catch (IOException e3) {
            throw new h("Cannot read Response", e3);
        } catch (Throwable th) {
            throw new h("Cannot read Response", th);
        }
    }

    private static boolean a(Document document, String str) {
        return "yes".equalsIgnoreCase(b(document, str));
    }

    private static d b(c cVar) {
        if (Log.isLoggable("MOBFOX", 3)) {
            Log.d("MOBFOX", "send Request");
        }
        StringBuilder sb = new StringBuilder("http://my.mobfox.com/request.php");
        sb.append("?rt=android_app");
        try {
            sb.append("&o=");
            sb.append(URLEncoder.encode(cVar.b(), "UTF-8"));
            sb.append("&m=");
            sb.append(URLEncoder.encode(cVar.c().toString().toLowerCase(), "UTF-8"));
            sb.append("&s=");
            sb.append(URLEncoder.encode(cVar.d(), "UTF-8"));
            sb.append("&u=");
            sb.append(URLEncoder.encode(cVar.a(), "UTF-8"));
            sb.append("&v=");
            sb.append(URLEncoder.encode(cVar.g(), "UTF-8"));
            if (!(cVar.f() == 0.0d || cVar.e() == 0.0d)) {
                sb.append("&latitude=");
                sb.append(cVar.f());
                sb.append("&longitude=");
                sb.append(cVar.e());
            }
            if (Log.isLoggable("MOBFOX", 3)) {
                Log.d("MOBFOX", "Perform HTTP Get Url: " + ((Object) sb));
            }
            DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
            HttpConnectionParams.setSoTimeout(defaultHttpClient.getParams(), 15000);
            HttpConnectionParams.setConnectionTimeout(defaultHttpClient.getParams(), 15000);
            try {
                return a(defaultHttpClient.execute(new HttpGet(sb.toString())).getEntity().getContent());
            } catch (ClientProtocolException e) {
                throw new h("Error in HTTP request", e);
            } catch (IOException e2) {
                throw new h("Error in HTTP request", e2);
            } catch (Throwable th) {
                throw new h("Error in HTTP request", th);
            }
        } catch (UnsupportedEncodingException e3) {
            throw new h("Cannot create request URL", e3);
        }
    }

    private static String b(Document document, String str) {
        Element element = (Element) document.getElementsByTagName(str).item(0);
        if (element != null) {
            NodeList childNodes = element.getChildNodes();
            if (childNodes.getLength() > 0) {
                return childNodes.item(0).getNodeValue();
            }
        }
        return null;
    }

    private static int c(Document document, String str) {
        return a(b(document, str));
    }
}
