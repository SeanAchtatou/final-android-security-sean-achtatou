package com.mobfox.sdk;

import android.content.Context;
import android.widget.ViewFlipper;

final class d extends ViewFlipper {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ MobFoxView f148a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    d(MobFoxView mobFoxView, Context context) {
        super(context);
        this.f148a = mobFoxView;
    }

    /* access modifiers changed from: protected */
    public final void onDetachedFromWindow() {
        try {
            super.onDetachedFromWindow();
        } catch (IllegalArgumentException e) {
            stopFlipping();
        }
    }
}
