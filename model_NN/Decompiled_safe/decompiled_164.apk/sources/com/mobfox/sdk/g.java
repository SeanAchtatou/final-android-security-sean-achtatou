package com.mobfox.sdk;

import android.util.Log;
import java.lang.Thread;

final class g implements Thread.UncaughtExceptionHandler {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ MobFoxView f151a;

    g(MobFoxView mobFoxView) {
        this.f151a = mobFoxView;
    }

    public final void uncaughtException(Thread thread, Throwable th) {
        if (Log.isLoggable("MOBFOX", 6)) {
            Log.e("MOBFOX", "Uncaught exception in request thread", th);
        }
        Thread unused = this.f151a.w = null;
    }
}
