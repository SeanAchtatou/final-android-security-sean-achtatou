package com.mobfox.sdk;

import android.content.Context;
import android.webkit.WebView;
import android.webkit.WebViewClient;

final class i extends WebViewClient {

    /* renamed from: a  reason: collision with root package name */
    private final MobFoxView f152a;
    private Context b;

    public i(MobFoxView mobFoxView, Context context) {
        this.f152a = mobFoxView;
        this.b = context;
    }

    public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
        return true;
    }
}
