package com.openfeint.internal;

import com.openfeint.internal.a.g;
import com.openfeint.internal.a.p;

final class f extends p {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ w f217a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    f(w wVar, g gVar) {
        super(gVar);
        this.f217a = wVar;
    }

    public final String a() {
        return "POST";
    }

    public final void a(int i, Object obj) {
        this.f217a.g = false;
        if (200 > i || i >= 300) {
            this.f217a.r = null;
            this.f217a.a(i, obj);
        } else {
            this.f217a.h = true;
            if (this.f217a.p != null) {
                this.f217a.b.post(this.f217a.p);
            }
        }
        if (this.f217a.q != null) {
            for (Runnable post : this.f217a.q) {
                this.f217a.b.post(post);
            }
        }
        this.f217a.p = null;
        this.f217a.q = null;
    }

    public final String b() {
        return "/xp/devices";
    }

    public final boolean l() {
        return false;
    }
}
