package com.openfeint.internal;

import a.a.a.c;
import a.a.a.h;
import com.openfeint.internal.b.ab;
import com.openfeint.internal.b.m;
import com.openfeint.internal.b.n;
import com.openfeint.internal.b.x;
import com.openfeint.internal.b.y;
import com.openfeint.internal.b.z;
import java.util.ArrayList;
import java.util.HashMap;

public final class s {

    /* renamed from: a  reason: collision with root package name */
    private h f228a;

    public s(h hVar) {
        this.f228a = hVar;
    }

    private y a(m mVar) {
        a.a.a.m i = this.f228a.i();
        if (i == a.a.a.m.VALUE_NULL) {
            return null;
        }
        if (i == a.a.a.m.START_OBJECT) {
            return b(mVar);
        }
        throw new c("Expected START_OBJECT of " + mVar.c, this.f228a.l());
    }

    private y b(m mVar) {
        ArrayList c;
        y a2 = mVar.a();
        while (this.f228a.i() == a.a.a.m.FIELD_NAME) {
            z zVar = (z) mVar.b.get(this.f228a.m());
            if (zVar == null) {
                this.f228a.i();
                this.f228a.j();
            } else if (zVar instanceof x) {
                this.f228a.i();
                ((x) zVar).a(a2, this.f228a);
            } else if (zVar instanceof ab) {
                ab abVar = (ab) zVar;
                m a3 = y.a(abVar.a());
                if (a3 != null) {
                    abVar.a(a2, a(a3));
                } else {
                    "unknown " + abVar.a();
                }
            } else if (zVar instanceof com.openfeint.internal.b.c) {
                com.openfeint.internal.b.c cVar = (com.openfeint.internal.b.c) zVar;
                a.a.a.m i = this.f228a.i();
                if (i == a.a.a.m.VALUE_NULL) {
                    c = null;
                } else if (i != a.a.a.m.START_ARRAY) {
                    throw new c("Wanted START_ARRAY", this.f228a.l());
                } else {
                    c = c();
                }
                cVar.a(a2, c);
            } else if (zVar instanceof n) {
                a.a.a.m i2 = this.f228a.i();
                if (i2 == a.a.a.m.VALUE_NULL) {
                    continue;
                } else if (i2 != a.a.a.m.START_OBJECT) {
                    throw new c("Expected START_OBJECT", this.f228a.l());
                } else {
                    b();
                }
            } else {
                this.f228a.j();
            }
        }
        if (this.f228a.q() == a.a.a.m.END_OBJECT) {
            return a2;
        }
        throw new c("Expected END_OBJECT of " + mVar.c, this.f228a.l());
    }

    private HashMap b() {
        HashMap hashMap = new HashMap();
        while (this.f228a.i() == a.a.a.m.FIELD_NAME) {
            String m = this.f228a.m();
            this.f228a.i();
            hashMap.put(m, Integer.valueOf(this.f228a.d()));
        }
        return hashMap;
    }

    private ArrayList c() {
        ArrayList arrayList = new ArrayList();
        while (this.f228a.i() != a.a.a.m.END_ARRAY) {
            if (this.f228a.i() != a.a.a.m.FIELD_NAME) {
                throw new c("Couldn't find wrapper object.", this.f228a.k());
            }
            String m = this.f228a.m();
            m b = y.b(m);
            if (b == null) {
                throw new c("Don't know class '" + m + "'.", this.f228a.k());
            }
            y a2 = a(b);
            if (this.f228a.i() != a.a.a.m.END_OBJECT) {
                throw new c("Expected only one k/v in wrapper object.", this.f228a.k());
            }
            arrayList.add(a2);
        }
        return arrayList;
    }

    public final Object a() {
        Object b;
        a.a.a.m i = this.f228a.i();
        if (i == null) {
            return null;
        }
        if (i != a.a.a.m.START_OBJECT) {
            throw new c("Couldn't find toplevel wrapper object.", this.f228a.k());
        } else if (this.f228a.i() != a.a.a.m.FIELD_NAME) {
            throw new c("Couldn't find toplevel wrapper object.", this.f228a.k());
        } else {
            String m = this.f228a.m();
            a.a.a.m i2 = this.f228a.i();
            if (i2 == a.a.a.m.START_ARRAY) {
                b = c();
            } else if (i2 == a.a.a.m.START_OBJECT) {
                m b2 = y.b(m);
                if (b2 == null) {
                    throw new c("Unknown toplevel class '" + m + "'.", this.f228a.k());
                }
                b = b(b2);
            } else {
                throw new c("Expected object or array at top level.", this.f228a.k());
            }
            if (this.f228a.i() == a.a.a.m.END_OBJECT) {
                return b;
            }
            throw new c("Expected only one k/v in toplevel wrapper object.", this.f228a.k());
        }
    }
}
