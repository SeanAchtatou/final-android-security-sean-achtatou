package com.openfeint.internal.ui;

final class ab extends h {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ WebNav f235a;
    private final /* synthetic */ boolean b;

    ab(WebNav webNav, boolean z) {
        this.f235a = webNav;
        this.b = z;
    }

    public final void a() {
        this.f235a.runOnUiThread(new aa(this.f235a));
    }

    public final void a(String str) {
        if (this.f235a.f != null) {
            String b2 = s.b(str);
            "Loading URL: " + b2;
            if (this.b) {
                this.f235a.f.reload();
            } else {
                this.f235a.f.loadUrl(b2);
            }
        }
    }
}
