package com.openfeint.internal.ui;

import android.os.Handler;
import android.os.Message;
import java.util.Map;
import java.util.Set;

final class x extends Handler {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ s f266a;

    x(s sVar) {
        this.f266a = sVar;
    }

    public final void dispatchMessage(Message message) {
        switch (message.what) {
            case 0:
                this.f266a.b = (r) message.obj;
                s.d(this.f266a);
                return;
            case 1:
                s.a(this.f266a, (String) message.obj, message.arg1 > 0);
                return;
            case 2:
                this.f266a.a((Set) message.obj, message.arg1 > 0, false);
                return;
            case 3:
                this.f266a.c = (Map) message.obj;
                s.d(this.f266a);
                return;
            default:
                return;
        }
    }
}
