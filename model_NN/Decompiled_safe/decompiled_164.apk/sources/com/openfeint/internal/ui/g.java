package com.openfeint.internal.ui;

import android.content.DialogInterface;
import android.webkit.JsResult;

final class g implements DialogInterface.OnCancelListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ am f252a;
    private final /* synthetic */ JsResult b;

    g(am amVar, JsResult jsResult) {
        this.f252a = amVar;
        this.b = jsResult;
    }

    public final void onCancel(DialogInterface dialogInterface) {
        this.b.cancel();
    }
}
