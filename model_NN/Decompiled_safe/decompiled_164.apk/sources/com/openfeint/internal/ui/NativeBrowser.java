package com.openfeint.internal.ui;

import android.os.Bundle;
import android.os.Handler;
import android.webkit.WebChromeClient;
import java.util.concurrent.atomic.AtomicBoolean;

public class NativeBrowser extends NestedWindow {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public Handler f230a;
    /* access modifiers changed from: private */
    public Runnable b;
    /* access modifiers changed from: private */
    public AtomicBoolean c = new AtomicBoolean(false);

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Bundle extras = getIntent().getExtras();
        String string = extras.getString("com.openfeint.internal.ui.NativeBrowser.argument.src");
        String string2 = extras.getString("com.openfeint.internal.ui.NativeBrowser.argument.timeout");
        this.f.getSettings().setJavaScriptEnabled(true);
        this.f.addJavascriptInterface(new j(this), "NativeBrowser");
        this.f.setWebViewClient(new aj(this));
        this.f.setWebChromeClient(new WebChromeClient());
        this.f.loadUrl(string);
        if (string2 != null) {
            this.f230a = new Handler();
            this.b = new ak(this);
            this.f230a.postDelayed(this.b, (long) Integer.parseInt(string2));
        }
        c();
    }
}
