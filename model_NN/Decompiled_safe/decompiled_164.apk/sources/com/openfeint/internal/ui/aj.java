package com.openfeint.internal.ui;

import android.content.Intent;
import android.webkit.WebView;
import android.webkit.WebViewClient;

final class aj extends WebViewClient {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ NativeBrowser f243a;

    aj(NativeBrowser nativeBrowser) {
        this.f243a = nativeBrowser;
    }

    private void a() {
        if (this.f243a.f230a != null && this.f243a.b != null) {
            this.f243a.f230a.removeCallbacks(this.f243a.b);
            this.f243a.f230a = null;
            this.f243a.b = null;
        }
    }

    public final void onPageFinished(WebView webView, String str) {
        a();
        super.onPageFinished(webView, str);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public final void onReceivedError(WebView webView, int i, String str, String str2) {
        a();
        super.onReceivedError(webView, i, str, str2);
        Intent intent = new Intent();
        intent.putExtra("com.openfeint.internal.ui.NativeBrowser.argument.failed", true);
        intent.putExtra("com.openfeint.internal.ui.NativeBrowser.argument.failure_code", i);
        intent.putExtra("com.openfeint.internal.ui.NativeBrowser.argument.failure_desc", str);
        this.f243a.setResult(-1, intent);
        this.f243a.finish();
    }
}
