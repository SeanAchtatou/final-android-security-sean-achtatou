package com.openfeint.internal.ui;

import android.app.AlertDialog;
import android.content.Intent;
import android.widget.Toast;
import com.ElekaSoftware.OfficeRage.C0000R;
import com.openfeint.internal.w;

public class Settings extends WebNav {
    private String g;

    /* access modifiers changed from: protected */
    public final ah a(WebNav webNav) {
        return new o(this, webNav);
    }

    /* access modifiers changed from: protected */
    public final String a() {
        return "settings/index";
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i == 10009 && intent != null) {
            Toast.makeText(this, w.a((int) C0000R.string.of_profile_pic_changed), 0).show();
        }
    }

    public void onResume() {
        if (this.g == null) {
            this.g = w.a().d().c();
        } else if (!this.g.equals(w.a().d().c())) {
            new AlertDialog.Builder(this).setTitle(w.a((int) C0000R.string.of_switched_accounts)).setMessage(String.format(w.a((int) C0000R.string.of_now_logged_in_as_format), w.a().d().f159a)).setNegativeButton(w.a((int) C0000R.string.of_ok), new ag(this)).show();
        }
        super.onResume();
    }
}
