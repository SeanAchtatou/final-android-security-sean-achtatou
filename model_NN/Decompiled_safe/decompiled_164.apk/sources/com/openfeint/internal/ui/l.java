package com.openfeint.internal.ui;

import java.util.List;

final class l extends ah {
    private /* synthetic */ IntroFlow c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public l(IntroFlow introFlow, WebNav webNav) {
        super(introFlow, webNav);
        this.c = introFlow;
    }

    /* access modifiers changed from: protected */
    public final void a(List list) {
        super.a(list);
        list.add("createUser");
        list.add("loginUser");
        list.add("cacheImage");
        list.add("uploadImage");
        list.add("clearImage");
        list.add("decline");
        list.add("getEmail");
    }
}
