package com.openfeint.internal.ui;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDiskIOException;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

final class r {

    /* renamed from: a  reason: collision with root package name */
    Set f260a = new HashSet();
    Map b = new HashMap();

    r(SQLiteDatabase sQLiteDatabase) {
        Cursor cursor;
        Cursor cursor2;
        k kVar;
        try {
            cursor = sQLiteDatabase.rawQuery("SELECT path, hash, is_global FROM server_manifest", null);
            try {
                if (cursor.getCount() > 0) {
                    cursor.moveToFirst();
                    do {
                        String string = cursor.getString(0);
                        String string2 = cursor.getString(1);
                        boolean z = cursor.getInt(2) != 0;
                        this.b.put(string, new k(string, string2));
                        if (z) {
                            this.f260a.add(string);
                        }
                    } while (cursor.moveToNext());
                }
                cursor.close();
                Cursor cursor3 = cursor;
                for (String str : this.b.keySet()) {
                    try {
                        cursor3 = sQLiteDatabase.rawQuery("SELECT has_dependency FROM dependencies WHERE path = ?", new String[]{str});
                        if (cursor3.getCount() <= 0 || (kVar = (k) this.b.get(str)) == null) {
                            cursor3.close();
                        } else {
                            Set set = kVar.c;
                            cursor3.moveToFirst();
                            do {
                                set.add(cursor3.getString(0));
                            } while (cursor3.moveToNext());
                            cursor3.close();
                        }
                    } catch (SQLiteDiskIOException e) {
                        cursor = cursor3;
                    } catch (Exception e2) {
                        e = e2;
                        cursor2 = cursor3;
                        try {
                            "SQLite exception. " + e.toString();
                            try {
                                cursor2.close();
                            } catch (Exception e3) {
                                return;
                            }
                        } catch (Throwable th) {
                            th = th;
                            try {
                                cursor2.close();
                            } catch (Exception e4) {
                            }
                            throw th;
                        }
                    } catch (Throwable th2) {
                        th = th2;
                        cursor2 = cursor3;
                        cursor2.close();
                        throw th;
                    }
                }
                try {
                    cursor3.close();
                } catch (Exception e5) {
                }
            } catch (SQLiteDiskIOException e6) {
                try {
                    s.c();
                    try {
                        cursor.close();
                    } catch (Exception e7) {
                    }
                } catch (Throwable th3) {
                    Throwable th4 = th3;
                    cursor2 = cursor;
                    th = th4;
                    cursor2.close();
                    throw th;
                }
            } catch (Exception e8) {
                Exception exc = e8;
                cursor2 = cursor;
                e = exc;
                "SQLite exception. " + e.toString();
                cursor2.close();
            }
        } catch (SQLiteDiskIOException e9) {
            cursor = null;
        } catch (Exception e10) {
            e = e10;
            cursor2 = null;
            "SQLite exception. " + e.toString();
            cursor2.close();
        } catch (Throwable th5) {
            th = th5;
            cursor2 = null;
            cursor2.close();
            throw th;
        }
    }

    r(byte[] bArr) {
        String str;
        k kVar = null;
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(bArr)), 8192);
            while (true) {
                String readLine = bufferedReader.readLine();
                if (readLine != null) {
                    String trim = readLine.trim();
                    if (trim.length() != 0) {
                        switch (trim.charAt(0)) {
                            case '#':
                                continue;
                            case '-':
                                if (kVar != null) {
                                    kVar.c.add(trim.substring(1).trim());
                                    continue;
                                } else {
                                    throw new Exception("Manifest Syntax Error: Dependency without an item");
                                }
                            default:
                                String[] split = trim.split(" ");
                                if (split.length >= 2) {
                                    if (split[0].charAt(0) == '@') {
                                        str = split[0].substring(1);
                                        this.f260a.add(str);
                                    } else {
                                        str = split[0];
                                    }
                                    k kVar2 = new k(str, split[1]);
                                    this.b.put(str, kVar2);
                                    kVar = kVar2;
                                    continue;
                                } else {
                                    throw new Exception("Manifest Syntax Error: Extra items in line");
                                }
                        }
                    }
                } else {
                    return;
                }
            }
        } catch (Exception e) {
            throw new Exception(e);
        }
    }
}
