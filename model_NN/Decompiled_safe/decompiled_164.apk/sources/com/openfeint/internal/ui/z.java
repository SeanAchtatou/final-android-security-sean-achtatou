package com.openfeint.internal.ui;

import com.openfeint.internal.a.g;
import com.openfeint.internal.a.k;
import java.util.Set;

final class z extends k {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ s f268a;
    private final /* synthetic */ int b;
    private final /* synthetic */ Set c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    z(s sVar, g gVar, int i, Set set) {
        super(gVar);
        this.f268a = sVar;
        this.b = i;
        this.c = set;
    }

    public final String a() {
        return "POST";
    }

    public final void a(int i, byte[] bArr) {
    }

    public final String b() {
        return "/webui/assets";
    }

    /* access modifiers changed from: protected */
    public final void b(int i, byte[] bArr) {
        s.a(this.f268a, i, bArr, m(), h(), this.b, this.c);
    }

    public final boolean k() {
        return false;
    }
}
