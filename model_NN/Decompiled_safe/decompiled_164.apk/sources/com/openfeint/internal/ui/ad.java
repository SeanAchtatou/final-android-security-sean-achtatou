package com.openfeint.internal.ui;

import android.content.DialogInterface;

final class ad implements DialogInterface.OnCancelListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ WebNav f237a;

    ad(WebNav webNav) {
        this.f237a = webNav;
    }

    public final void onCancel(DialogInterface dialogInterface) {
        this.f237a.finish();
    }
}
