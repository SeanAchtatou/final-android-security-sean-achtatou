package com.openfeint.internal.ui;

import com.openfeint.internal.v;
import java.io.File;
import java.io.IOException;

final class u implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ s f263a;
    private final /* synthetic */ File b;
    private final /* synthetic */ File c;
    private final /* synthetic */ File d;

    u(s sVar, File file, File file2, File file3) {
        this.f263a = sVar;
        this.b = file;
        this.c = file2;
        this.d = file3;
    }

    public final void run() {
        try {
            if (this.b.isDirectory()) {
                v.a(this.b, this.c);
                this.f263a.h();
                this.f263a.i();
                return;
            }
            s.a(this.f263a, this.d);
        } catch (IOException e) {
            e.getMessage();
            this.f263a.a();
        }
    }
}
