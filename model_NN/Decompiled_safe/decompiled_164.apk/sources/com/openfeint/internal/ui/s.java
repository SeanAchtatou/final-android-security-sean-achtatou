package com.openfeint.internal.ui;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDiskIOException;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import com.openfeint.internal.a.g;
import com.openfeint.internal.e.c;
import com.openfeint.internal.v;
import com.openfeint.internal.w;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

public final class s {
    private static URI e;
    private static String f;
    private static s g;
    private static String h;
    /* access modifiers changed from: private */
    public static String i;
    private static boolean s = false;

    /* renamed from: a  reason: collision with root package name */
    Handler f261a;
    r b;
    Map c;
    Context d;
    private Handler j;
    private Set k;
    private Map l;
    private boolean m = false;
    private boolean n = false;
    private Set o;
    private Set p;
    private boolean q = false;
    private URI r = l();

    private s(Context context) {
        this.d = context;
        this.k = new HashSet();
        this.l = new HashMap();
        this.o = new HashSet();
        this.p = new HashSet();
        this.j = new Handler();
        this.f261a = new x(this);
    }

    public static s a(Context context) {
        if (g != null) {
            g.m();
        }
        s sVar = new s(context);
        g = sVar;
        return sVar;
    }

    private Set a(Set set) {
        String str = v.c(this.d).equals("mdpi") ? ".hdpi." : ".mdpi.";
        HashSet hashSet = new HashSet();
        Iterator it = set.iterator();
        while (it.hasNext()) {
            String str2 = (String) it.next();
            if (!str2.contains(str)) {
                hashSet.add(str2);
            }
        }
        return hashSet;
    }

    static /* synthetic */ void a(s sVar, int i2, byte[] bArr, String str, String str2, int i3, Set set) {
        if (200 <= i2 && i2 < 300) {
            HashSet hashSet = new HashSet();
            ZipInputStream zipInputStream = new ZipInputStream(new ByteArrayInputStream(bArr));
            while (true) {
                try {
                    ZipEntry nextEntry = zipInputStream.getNextEntry();
                    if (nextEntry == null) {
                        break;
                    } else if (!nextEntry.isDirectory()) {
                        String name = nextEntry.getName();
                        v.a(zipInputStream, String.valueOf(i) + name);
                        hashSet.add(name);
                    }
                } catch (Exception e2) {
                    e2.getMessage();
                }
            }
            if (!hashSet.isEmpty()) {
                Message.obtain(sVar.f261a, 2, 1, 0, hashSet).sendToTarget();
            } else {
                Message.obtain(sVar.f261a, 2, 0, 0, set).sendToTarget();
            }
        } else if (302 == i2 || 303 == i2) {
            sVar.a(str, str2, i3, set);
        } else if (i2 != 0 && (400 > i2 || i2 >= 500)) {
            Message.obtain(sVar.f261a, 2, 0, 0, set).sendToTarget();
        } else if (i3 > 0) {
            sVar.j.postDelayed(new y(sVar, str, str2, set, i3), 5000);
        } else {
            Message.obtain(sVar.f261a, 2, 0, 0, set).sendToTarget();
        }
    }

    static /* synthetic */ void a(s sVar, File file) {
        HashSet hashSet = new HashSet();
        sVar.a("webui", hashSet);
        Set<String> a2 = sVar.a(hashSet);
        sVar.a(file, "webui/manifest.plist", a2);
        sVar.b(file, "webui/javascripts/", a2);
        sVar.b(file, "webui/stylesheets/", a2);
        sVar.b(file, "webui/intro/", a2);
        if (v.c(sVar.d).equals("mdpi")) {
            sVar.a(file, "webui/images/space.grid.mdpi.png", a2);
            sVar.a(file, "webui/images/button.gray.mdpi.png", a2);
            sVar.a(file, "webui/images/button.gray.hit.mdpi.png", a2);
            sVar.a(file, "webui/images/button.green.mdpi.png", a2);
            sVar.a(file, "webui/images/button.green.hit.mdpi.png", a2);
            sVar.a(file, "webui/images/logo.small.mdpi.png", a2);
            sVar.a(file, "webui/images/header_bg.mdpi.png", a2);
            sVar.a(file, "webui/images/loading.spinner.mdpi.png", a2);
            sVar.a(file, "webui/images/input.text.mdpi.png", a2);
            sVar.a(file, "webui/images/frame.small.mdpi.png", a2);
            sVar.a(file, "webui/images/icon.leaf.gray.mdpi.png", a2);
            sVar.a(file, "webui/images/tab.divider.mdpi.png", a2);
            sVar.a(file, "webui/images/tab.active_indicator.mdpi.png", a2);
            sVar.a(file, "webui/images/logo.mdpi.png", a2);
            sVar.a(file, "webui/images/header_bg.mdpi.png", a2);
            sVar.a(file, "webui/images/loading.spinner.mdpi.png", a2);
            sVar.a(file, "webui/images/icon.user.male.mdpi.png", a2);
            sVar.a(file, "webui/images/intro.leaderboards.mdpi.png", a2);
            sVar.a(file, "webui/images/intro.friends.mdpi.png", a2);
            sVar.a(file, "webui/images/intro.achievements.mdpi.png", a2);
            sVar.a(file, "webui/images/intro.games.mdpi.png", a2);
        } else {
            sVar.a(file, "webui/images/space.grid.hdpi.png", a2);
            sVar.a(file, "webui/images/button.gray.hdpi.png", a2);
            sVar.a(file, "webui/images/button.gray.hit.hdpi.png", a2);
            sVar.a(file, "webui/images/button.green.hdpi.png", a2);
            sVar.a(file, "webui/images/button.green.hit.hdpi.png", a2);
            sVar.a(file, "webui/images/logo.small.hdpi.png", a2);
            sVar.a(file, "webui/images/header_bg.hdpi.png", a2);
            sVar.a(file, "webui/images/loading.spinner.hdpi.png", a2);
            sVar.a(file, "webui/images/input.text.hdpi.png", a2);
            sVar.a(file, "webui/images/frame.small.hdpi.png", a2);
            sVar.a(file, "webui/images/icon.leaf.gray.hdpi.png", a2);
            sVar.a(file, "webui/images/tab.divider.hdpi.png", a2);
            sVar.a(file, "webui/images/tab.active_indicator.hdpi.png", a2);
            sVar.a(file, "webui/images/logo.hdpi.png", a2);
            sVar.a(file, "webui/images/header_bg.hdpi.png", a2);
            sVar.a(file, "webui/images/loading.spinner.hdpi.png", a2);
            sVar.a(file, "webui/images/icon.user.male.hdpi.png", a2);
            sVar.a(file, "webui/images/intro.leaderboards.hdpi.png", a2);
            sVar.a(file, "webui/images/intro.friends.hdpi.png", a2);
            sVar.a(file, "webui/images/intro.achievements.hdpi.png", a2);
            sVar.a(file, "webui/images/intro.games.hdpi.png", a2);
        }
        sVar.i();
        for (String a3 : a2) {
            sVar.a(file, a3);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.openfeint.internal.ui.s.a(java.util.Set, boolean, boolean):void
     arg types: [java.util.HashSet, boolean, int]
     candidates:
      com.openfeint.internal.ui.s.a(com.openfeint.internal.ui.s, java.lang.String, boolean):void
      com.openfeint.internal.ui.s.a(com.openfeint.internal.ui.s, java.util.Set, int):void
      com.openfeint.internal.ui.s.a(com.openfeint.internal.ui.s, java.util.Set, boolean):void
      com.openfeint.internal.ui.s.a(java.io.File, java.lang.String, java.util.Set):void
      com.openfeint.internal.ui.s.a(java.util.Set, boolean, boolean):void */
    static /* synthetic */ void a(s sVar, String str, boolean z) {
        HashSet hashSet = new HashSet(1);
        hashSet.add(str);
        sVar.a((Set) hashSet, z, true);
    }

    private void a(File file, String str) {
        try {
            File file2 = new File(file, str);
            DataInputStream dataInputStream = new DataInputStream(this.d.getAssets().open(str));
            file2.getParentFile().mkdirs();
            v.a(dataInputStream, new DataOutputStream(new FileOutputStream(file2)));
        } catch (Exception e2) {
            e2.toString();
        }
    }

    private void a(File file, String str, Set set) {
        if (set.contains(str)) {
            a(file, str);
            set.remove(str);
        }
    }

    public static void a(String str) {
        k kVar;
        s sVar = g;
        if (!sVar.m) {
            sVar.p.add(str);
            if (sVar.b != null && (kVar = (k) sVar.b.b.get(str)) != null) {
                HashSet hashSet = new HashSet(kVar.c);
                hashSet.retainAll(sVar.o);
                sVar.p.addAll(hashSet);
                "Prioritizing " + str + " deps:" + hashSet.toString();
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(String str, String str2, int i2, Set set) {
        new w(this, str, str2, i2, set).p();
    }

    private void a(String str, Set set) {
        try {
            String[] list = this.d.getAssets().list(str);
            int length = list.length;
            for (int i2 = 0; i2 < length; i2++) {
                String str2 = String.valueOf(str) + "/" + list[i2];
                try {
                    InputStream open = this.d.getAssets().open(str2);
                    set.add(str2);
                    open.close();
                } catch (IOException e2) {
                    a(str2, set);
                }
            }
        } catch (IOException e3) {
            e3.toString();
        }
    }

    /* access modifiers changed from: private */
    public void a(Set set, int i2) {
        String.format("Syncing %d items", Integer.valueOf(set.size()));
        g gVar = new g();
        Iterator it = set.iterator();
        while (it.hasNext()) {
            k kVar = (k) this.b.b.get((String) it.next());
            gVar.a("files[][path]", kVar.f255a);
            gVar.a("files[][hash]", kVar.b);
        }
        new z(this, gVar, i2, set).p();
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v13, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v0, resolved type: java.lang.String} */
    /* access modifiers changed from: private */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(java.util.Set r9, boolean r10, boolean r11) {
        /*
            r8 = this;
            com.openfeint.internal.ui.r r1 = r8.b
            if (r1 != 0) goto L_0x0005
        L_0x0004:
            return
        L_0x0005:
            if (r10 != 0) goto L_0x0010
            if (r11 != 0) goto L_0x0010
            r1 = 1
            r8.q = r1
        L_0x000c:
            r8.o()
            goto L_0x0004
        L_0x0010:
            java.util.Map r1 = r8.l
            java.util.Collection r1 = r1.values()
            java.util.Iterator r2 = r1.iterator()
        L_0x001a:
            boolean r1 = r2.hasNext()
            if (r1 != 0) goto L_0x006d
            java.util.Set r1 = r8.o
            r1.removeAll(r9)
            com.openfeint.internal.ui.r r1 = r8.b
            java.util.Set r1 = r1.f260a
            r1.removeAll(r9)
            java.util.Set r1 = r8.p
            r1.removeAll(r9)
            boolean r1 = r8.n
            if (r1 == 0) goto L_0x0054
            java.util.HashSet r2 = new java.util.HashSet
            r2.<init>()
            java.util.Map r1 = r8.l
            java.util.Collection r1 = r1.values()
            java.util.Iterator r3 = r1.iterator()
        L_0x0044:
            boolean r1 = r3.hasNext()
            if (r1 != 0) goto L_0x007b
            java.util.Iterator r2 = r2.iterator()
        L_0x004e:
            boolean r1 = r2.hasNext()
            if (r1 != 0) goto L_0x00a8
        L_0x0054:
            int r1 = r9.size()
            java.lang.String[] r3 = new java.lang.String[r1]
            int r1 = r3.length
            java.lang.String[] r4 = new java.lang.String[r1]
            r1 = 0
            java.util.Iterator r5 = r9.iterator()
            r6 = r1
        L_0x0063:
            boolean r1 = r5.hasNext()
            if (r1 != 0) goto L_0x00b4
            com.openfeint.internal.e.c.a(r3, r4)
            goto L_0x000c
        L_0x006d:
            java.lang.Object r1 = r2.next()
            com.openfeint.internal.ui.ai r1 = (com.openfeint.internal.ui.ai) r1
            com.openfeint.internal.ui.k r1 = r1.f242a
            java.util.Set r1 = r1.c
            r1.removeAll(r9)
            goto L_0x001a
        L_0x007b:
            java.lang.Object r1 = r3.next()
            com.openfeint.internal.ui.ai r1 = (com.openfeint.internal.ui.ai) r1
            java.util.Set r4 = r8.o
            com.openfeint.internal.ui.k r5 = r1.f242a
            java.lang.String r5 = r5.f255a
            boolean r4 = r4.contains(r5)
            if (r4 != 0) goto L_0x0044
            com.openfeint.internal.ui.k r4 = r1.f242a
            java.util.Set r4 = r4.c
            int r4 = r4.size()
            if (r4 != 0) goto L_0x0044
            com.openfeint.internal.ui.k r4 = r1.f242a
            java.lang.String r4 = r4.f255a
            r2.add(r4)
            com.openfeint.internal.ui.h r4 = r1.b
            com.openfeint.internal.ui.k r1 = r1.f242a
            java.lang.String r1 = r1.f255a
            r4.a(r1)
            goto L_0x0044
        L_0x00a8:
            java.lang.Object r1 = r2.next()
            java.lang.String r1 = (java.lang.String) r1
            java.util.Map r3 = r8.l
            r3.remove(r1)
            goto L_0x004e
        L_0x00b4:
            java.lang.Object r1 = r5.next()
            r0 = r1
            java.lang.String r0 = (java.lang.String) r0
            r2 = r0
            if (r10 == 0) goto L_0x00d6
            com.openfeint.internal.ui.r r1 = r8.b
            java.util.Map r1 = r1.b
            java.lang.Object r1 = r1.get(r2)
            com.openfeint.internal.ui.k r1 = (com.openfeint.internal.ui.k) r1
            java.lang.String r1 = r1.b
        L_0x00ca:
            r3[r6] = r2
            r4[r6] = r1
            int r6 = r6 + 1
            java.util.Map r7 = r8.c
            r7.put(r2, r1)
            goto L_0x0063
        L_0x00d6:
            java.lang.String r1 = "INVALID"
            goto L_0x00ca
        */
        throw new UnsupportedOperationException("Method not decompiled: com.openfeint.internal.ui.s.a(java.util.Set, boolean, boolean):void");
    }

    public static boolean a(String str, h hVar) {
        s sVar = g;
        if (!sVar.m) {
            if (sVar.b == null) {
                sVar.k.add(new m(str, hVar));
                return true;
            }
            k kVar = (k) sVar.b.b.get(str);
            if (kVar != null) {
                k kVar2 = new k(kVar);
                kVar2.c.retainAll(sVar.o);
                sVar.l.put(str, new ai(kVar2, hVar));
                return true;
            }
        }
        hVar.a(str);
        return false;
    }

    static /* synthetic */ String b(Context context) {
        return String.format("/webui/manifest/%s.%s.%s", "android", f != null ? f : "embed", v.c(context));
    }

    public static final String b(String str) {
        return String.valueOf(h) + str;
    }

    public static void b() {
        s sVar = g;
        if (v.b() || !"mounted".equals(Environment.getExternalStorageState())) {
            sVar.a();
        } else {
            File file = new File(Environment.getExternalStorageDirectory(), "openfeint");
            File file2 = new File(file, "webui");
            boolean z = !file2.exists();
            if (z) {
                try {
                    new File(file, ".nomedia").createNewFile();
                } catch (IOException e2) {
                }
                if (!file2.mkdirs()) {
                    sVar.a();
                }
            }
            i = String.valueOf(file2.getAbsolutePath()) + "/";
            h = "file://" + i;
            if (z) {
                File filesDir = sVar.d.getFilesDir();
                File file3 = new File(filesDir, "webui");
                if (file3.isDirectory()) {
                    try {
                        v.b(sVar.d.getDatabasePath("manifest.db"), new File(file2, "manifest.db"));
                    } catch (IOException e3) {
                    }
                }
                new Thread(new u(sVar, file3, file2, filesDir)).start();
            } else {
                sVar.i();
                sVar.h();
            }
        }
        g.g();
    }

    private void b(File file, String str, Set set) {
        HashSet<String> hashSet = new HashSet<>();
        Iterator it = set.iterator();
        while (it.hasNext()) {
            String str2 = (String) it.next();
            if (str2.startsWith(str)) {
                hashSet.add(str2);
            }
        }
        for (String a2 : hashSet) {
            a(file, a2, set);
        }
    }

    private void b(Set set) {
        a(set, 3);
    }

    public static void c() {
        s = true;
        for (m mVar : g.k) {
            mVar.b.a();
        }
        g.k.clear();
        g.m();
    }

    private final void c(String str) {
        "Syncing item: " + str;
        new v(this, str).p();
    }

    static /* synthetic */ void d(s sVar) {
        if (sVar.b != null && sVar.c != null) {
            for (k kVar : sVar.b.b.values()) {
                if (!kVar.b.equals(sVar.c.get(kVar.f255a))) {
                    sVar.o.add(kVar.f255a);
                }
            }
            sVar.o();
        }
    }

    public static boolean d() {
        return s;
    }

    public static boolean e() {
        if (s) {
            return false;
        }
        s sVar = g;
        boolean b2 = c.b(sVar.d);
        sVar.b = null;
        if (b2) {
            sVar.c = sVar.k();
            b2 = !sVar.c.isEmpty();
        }
        sVar.m = false;
        sVar.n = false;
        sVar.g();
        return b2;
    }

    private void g() {
        new al(this, "manifest").p();
    }

    /* access modifiers changed from: private */
    public void h() {
        v.a(new File(this.d.getFilesDir(), "webui"));
        this.d.getDatabasePath("manifest.db").delete();
    }

    /* access modifiers changed from: private */
    public void i() {
        Map j2 = j();
        Message obtain = Message.obtain(this.f261a, 3);
        obtain.obj = j2;
        obtain.sendToTarget();
    }

    private Map j() {
        Cursor cursor;
        Cursor cursor2;
        try {
            cursor2 = c.f216a.b().rawQuery("SELECT * FROM manifest", null);
            try {
                if (cursor2.getCount() > 0) {
                    HashMap hashMap = new HashMap();
                    cursor2.moveToFirst();
                    do {
                        hashMap.put(cursor2.getString(0), cursor2.getString(1));
                    } while (cursor2.moveToNext());
                    cursor2.close();
                    try {
                        cursor2.close();
                    } catch (Exception e2) {
                    }
                    return hashMap;
                }
                try {
                    cursor2.close();
                } catch (Exception e3) {
                }
                return k();
            } catch (SQLiteDiskIOException e4) {
                try {
                    c();
                    try {
                        cursor2.close();
                    } catch (Exception e5) {
                    }
                    return k();
                } catch (Throwable th) {
                    Throwable th2 = th;
                    cursor = cursor2;
                    th = th2;
                    try {
                        cursor.close();
                    } catch (Exception e6) {
                    }
                    throw th;
                }
            } catch (Exception e7) {
                Exception exc = e7;
                cursor = cursor2;
                e = exc;
                try {
                    "SQLite exception. " + e.toString();
                    try {
                        cursor.close();
                    } catch (Exception e8) {
                    }
                    return k();
                } catch (Throwable th3) {
                    th = th3;
                    cursor.close();
                    throw th;
                }
            }
        } catch (SQLiteDiskIOException e9) {
            cursor2 = null;
        } catch (Exception e10) {
            e = e10;
            cursor = null;
            "SQLite exception. " + e.toString();
            cursor.close();
            return k();
        } catch (Throwable th4) {
            th = th4;
            cursor = null;
            cursor.close();
            throw th;
        }
    }

    private Map k() {
        File file = new File(i, "manifest.plist");
        if (file.isFile()) {
            try {
                XMLReader xMLReader = SAXParserFactory.newInstance().newSAXParser().getXMLReader();
                n nVar = new n(this);
                xMLReader.setContentHandler(nVar);
                xMLReader.parse(new InputSource(new FileInputStream(file.getPath())));
                return nVar.f257a;
            } catch (Exception e2) {
                e2.toString();
            }
        }
        return new HashMap();
    }

    private static final URI l() {
        try {
            return e != null ? e : new URI(w.a().f());
        } catch (Exception e2) {
            return null;
        }
    }

    /* access modifiers changed from: private */
    public void m() {
        for (m mVar : this.k) {
            mVar.b.a(mVar.f256a);
        }
        this.k.clear();
        this.p.clear();
        this.b.f260a.clear();
        this.o.clear();
        n();
    }

    private void n() {
        c.f216a.c();
        this.m = true;
    }

    private void o() {
        k kVar;
        this.b.f260a.retainAll(this.o);
        if (!this.n && this.b.f260a.isEmpty()) {
            for (m mVar : this.k) {
                if (!this.o.contains(mVar.f256a)) {
                    mVar.b.a(mVar.f256a);
                } else {
                    k kVar2 = new k((k) this.b.b.get(mVar.f256a));
                    kVar2.c.retainAll(this.o);
                    this.l.put(mVar.f256a, new ai(kVar2, mVar.b));
                }
            }
            this.k.clear();
            HashSet hashSet = new HashSet();
            for (String str : this.p) {
                if (this.o.contains(str) && (kVar = (k) this.b.b.get(str)) != null) {
                    hashSet.addAll(kVar.c);
                }
            }
            hashSet.retainAll(this.o);
            this.p.addAll(hashSet);
            this.n = true;
        }
        this.p.retainAll(this.o);
        int size = this.b.f260a.size() + this.p.size();
        if (!this.q && size > 1) {
            HashSet hashSet2 = new HashSet();
            hashSet2.addAll(this.b.f260a);
            hashSet2.addAll(this.p);
            b(hashSet2);
        } else if (this.b.f260a.size() > 0) {
            c((String) this.b.f260a.iterator().next());
        } else if (this.p.size() > 0) {
            c((String) this.p.iterator().next());
        } else if (!this.q && this.o.size() > 1) {
            b(this.o);
        } else if (this.o.size() > 0) {
            c((String) this.o.iterator().next());
        } else {
            n();
        }
    }

    public final void a() {
        File filesDir = this.d.getFilesDir();
        i = String.valueOf(new File(filesDir, "webui").getAbsolutePath()) + "/";
        h = "file://" + i;
        if (!new File(filesDir, "webui").isDirectory()) {
            new Thread(new t(this, filesDir)).start();
        } else {
            i();
        }
    }
}
