package com.openfeint.internal.ui;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDiskIOException;
import android.database.sqlite.SQLiteStatement;
import android.os.Message;
import com.openfeint.internal.a.t;
import com.openfeint.internal.e.c;

final class al extends t {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ s f245a;
    /* access modifiers changed from: private */
    public r b = null;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public al(s sVar, String str) {
        super(str);
        this.f245a = sVar;
    }

    /* access modifiers changed from: private */
    public void d() {
        if (this.b != null) {
            try {
                r rVar = this.b;
                SQLiteDatabase a2 = c.f216a.a();
                try {
                    a2.beginTransaction();
                    a2.execSQL("DELETE FROM server_manifest;");
                    a2.execSQL("DELETE FROM dependencies;");
                    SQLiteStatement compileStatement = a2.compileStatement("INSERT INTO server_manifest(path, hash, is_global) VALUES(?, ?, ?)");
                    SQLiteStatement compileStatement2 = a2.compileStatement("INSERT INTO dependencies(path, has_dependency) VALUES(?, ?)");
                    for (String str : rVar.b.keySet()) {
                        k kVar = (k) rVar.b.get(str);
                        compileStatement.bindString(1, str);
                        compileStatement.bindString(2, kVar.b);
                        compileStatement.bindLong(3, (long) (rVar.f260a.contains(str) ? 1 : 0));
                        compileStatement.execute();
                        compileStatement2.bindString(1, str);
                        for (String bindString : kVar.c) {
                            compileStatement2.bindString(2, bindString);
                            compileStatement2.execute();
                        }
                    }
                    a2.setTransactionSuccessful();
                    try {
                        a2.endTransaction();
                    } catch (Exception e) {
                    }
                } catch (SQLiteDiskIOException e2) {
                    s.c();
                    try {
                        a2.endTransaction();
                    } catch (Exception e3) {
                    }
                } catch (Exception e4) {
                    "SQLite exception. " + e4.toString();
                    try {
                        a2.endTransaction();
                    } catch (Exception e5) {
                    }
                } catch (Throwable th) {
                    try {
                        a2.endTransaction();
                    } catch (Exception e6) {
                    }
                    throw th;
                }
            } catch (Exception e7) {
                e7.toString();
            }
            Message.obtain(this.f245a.f261a, 0, this.b).sendToTarget();
            return;
        }
        this.f245a.m();
    }

    public final void a(int i, byte[] bArr) {
    }

    public final String b() {
        return s.b(this.f245a.d);
    }

    public final void b(int i, byte[] bArr) {
        if (i == 200) {
            try {
                this.b = new r(bArr);
            } catch (Exception e) {
                e.toString();
            }
        } else {
            try {
                this.b = new r(c.f216a.b());
            } catch (Exception e2) {
                e2.toString();
            }
        }
        if (this.b == null || this.b.b.isEmpty()) {
            this.b = null;
            new d(this).p();
            return;
        }
        d();
        a(n());
    }

    public final boolean k() {
        return false;
    }
}
