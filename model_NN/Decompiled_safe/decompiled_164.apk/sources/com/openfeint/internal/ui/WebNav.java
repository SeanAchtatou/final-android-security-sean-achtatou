package com.openfeint.internal.ui;

import android.app.Dialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.KeyEvent;
import android.widget.ProgressBar;
import com.ElekaSoftware.OfficeRage.C0000R;
import com.openfeint.api.a.k;
import com.openfeint.internal.a.a.h;
import com.openfeint.internal.m;
import com.openfeint.internal.w;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Map;

public class WebNav extends NestedWindow {

    /* renamed from: a  reason: collision with root package name */
    ah f232a;
    boolean b = false;
    boolean c = false;
    protected int d;
    protected ArrayList e = new ArrayList();
    private i g;
    private Dialog h;
    private boolean i = true;
    private Map j = null;

    private static final String b(String str) {
        return str == null ? "''" : "'" + str.replace("\\", "\\\\").replace("'", "\\'") + "'";
    }

    /* access modifiers changed from: private */
    public void d() {
        if (this.h.isShowing()) {
            this.h.dismiss();
        }
    }

    /* access modifiers changed from: protected */
    public ah a(WebNav webNav) {
        return new ah(this, webNav);
    }

    /* access modifiers changed from: protected */
    public String a() {
        String stringExtra = getIntent().getStringExtra("content_path");
        if (stringExtra != null) {
            return stringExtra;
        }
        throw new RuntimeException("WebNav intent requires extra value 'content_path'");
    }

    public final void a(String str) {
        if (this.f != null) {
            this.f.loadUrl("javascript:" + str);
        }
    }

    /* access modifiers changed from: protected */
    public final void a(boolean z) {
        this.b = false;
        s.a("index.html", new ab(this, z));
    }

    public final void b() {
        String a2 = a();
        if (a2.contains("?")) {
            a2 = a2.split("\\?")[0];
        }
        if (!a2.endsWith(".json")) {
            a2 = String.valueOf(a2) + ".json";
        }
        s.a(a2, new ae(this));
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        Bitmap a2;
        super.onActivityResult(i2, i3, intent);
        if (this.j != null && i2 == 25565) {
            if (i3 != 0) {
                this.i = false;
                if (intent.getBooleanExtra("com.openfeint.internal.ui.NativeBrowser.argument.failed", false)) {
                    String str = (String) this.j.get("on_failure");
                    if (str != null) {
                        a(String.format("%s(%d, %s)", str, Integer.valueOf(intent.getIntExtra("com.openfeint.internal.ui.NativeBrowser.argument.failure_code", 0)), b(intent.getStringExtra("com.openfeint.internal.ui.NativeBrowser.argument.failure_desc"))));
                    }
                } else {
                    String str2 = (String) this.j.get("callback");
                    if (str2 != null) {
                        String stringExtra = intent.getStringExtra("com.openfeint.internal.ui.NativeBrowser.argument.result");
                        Object[] objArr = new Object[2];
                        objArr[0] = str2;
                        objArr[1] = stringExtra != null ? stringExtra : "";
                        a(String.format("%s(%s)", objArr));
                    }
                }
            } else {
                String str3 = (String) this.j.get("on_cancel");
                if (str3 != null) {
                    a(String.format("%s()", str3));
                }
            }
            this.j = null;
        } else if (m.a(i2) && (a2 = m.a(this, i3, intent)) != null) {
            af afVar = new af(this);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            a2.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
            w.a().a("/xp/users/" + w.a().d().c() + "/profile_picture", new h("profile.png", byteArrayOutputStream.toByteArray()), "image/png", afVar);
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        a(String.format("OF.setOrientation('%s');", configuration.orientation == 2 ? "landscape" : "portrait"));
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.d = 0;
        this.f.getSettings().setJavaScriptEnabled(true);
        this.f.getSettings().setPluginsEnabled(false);
        this.f.setScrollBarStyle(33554432);
        this.f.getSettings().setCacheMode(2);
        this.h = new Dialog(this, C0000R.style.OFLoading);
        this.h.setOnCancelListener(new ad(this));
        this.h.setCancelable(true);
        this.h.setContentView((int) C0000R.layout.of_native_loader);
        ProgressBar progressBar = (ProgressBar) this.h.findViewById(C0000R.id.progress);
        progressBar.setIndeterminate(true);
        progressBar.setIndeterminateDrawable(w.a().l().getResources().getDrawable(C0000R.drawable.of_native_loader_progress));
        this.f232a = a(this);
        this.g = new i(this, this.f232a);
        this.f.setWebViewClient(this.g);
        this.f.setWebChromeClient(new am(this));
        this.f.addJavascriptInterface(new ac(this), "NativeInterface");
        String a2 = a();
        if (a2.contains("?")) {
            a2 = a2.split("\\?")[0];
        }
        if (!a2.endsWith(".json")) {
            a2 = String.valueOf(a2) + ".json";
        }
        s.a(a2);
        a(false);
        this.h.show();
    }

    public void onDestroy() {
        this.f.destroy();
        this.f = null;
        super.onDestroy();
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 == 84) {
            a(String.format("OF.menu('%s')", "search"));
            return true;
        } else if (i2 != 4 || this.d <= 1) {
            return super.onKeyDown(i2, keyEvent);
        } else {
            a("OF.goBack()");
            return true;
        }
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Bundle bundle) {
        w.b(bundle);
    }

    public void onResume() {
        super.onResume();
        k d2 = w.a().d();
        if (d2 != null && this.c) {
            a(String.format("if (OF.user) { OF.user.name = %s; OF.user.id = '%s'; }", b(d2.f159a), d2.c()));
            if (this.i) {
                a("if (OF.page) OF.refresh();");
            }
        }
        this.i = true;
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        w.a(bundle);
    }

    public void onStop() {
        super.onStop();
        d();
    }

    public void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
    }
}
