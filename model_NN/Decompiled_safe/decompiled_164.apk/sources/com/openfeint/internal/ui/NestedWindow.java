package com.openfeint.internal.ui;

import android.app.Activity;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.webkit.WebView;
import com.ElekaSoftware.OfficeRage.C0000R;
import com.openfeint.internal.v;

public class NestedWindow extends Activity {

    /* renamed from: a  reason: collision with root package name */
    private View f231a;
    private boolean b = false;
    private final String c = "NestedWindow";
    protected WebView f;

    public final void c() {
        if (this.f != null && !this.b) {
            this.b = true;
            AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
            alphaAnimation.setDuration(200);
            alphaAnimation.setFillAfter(true);
            this.f.startAnimation(alphaAnimation);
            if (this.f.getVisibility() == 4) {
                this.f.setVisibility(0);
                findViewById(C0000R.id.frameLayout).setVisibility(0);
            }
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        v.a((Activity) this);
        Display defaultDisplay = getWindowManager().getDefaultDisplay();
        int width = defaultDisplay.getWidth();
        int height = defaultDisplay.getHeight();
        if ((height <= width || width < 800 || height < 1000) ? width >= 1000 && height >= 800 : true) {
            getWindow().requestFeature(1);
        }
        setContentView((int) C0000R.layout.of_nested_window);
        this.f = (WebView) findViewById(C0000R.id.web_view);
        this.f231a = findViewById(C0000R.id.of_ll_logo_image);
    }
}
