package com.openfeint.internal.ui;

import android.os.Message;
import com.openfeint.internal.a.k;

final class v extends k {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ s f264a;
    private final /* synthetic */ String b;

    v(s sVar, String str) {
        this.f264a = sVar;
        this.b = str;
    }

    public final String a() {
        return "GET";
    }

    public final void a(int i, byte[] bArr) {
        if (i != 200) {
            Message.obtain(this.f264a.f261a, 1, 0, 0, this.b).sendToTarget();
            return;
        }
        try {
            com.openfeint.internal.v.a(bArr, String.valueOf(s.i) + this.b);
            Message.obtain(this.f264a.f261a, 1, 1, 0, this.b).sendToTarget();
        } catch (Exception e) {
            Message.obtain(this.f264a.f261a, 1, 0, 0, this.b).sendToTarget();
        }
    }

    public final String b() {
        return "/webui/" + this.b;
    }

    public final boolean k() {
        return false;
    }
}
