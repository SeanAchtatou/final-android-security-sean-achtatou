package com.openfeint.internal.ui;

import android.app.AlertDialog;
import android.net.Uri;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.ElekaSoftware.OfficeRage.C0000R;
import com.openfeint.api.a.k;
import com.openfeint.internal.a.r;
import com.openfeint.internal.v;
import com.openfeint.internal.w;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

public final class i extends WebViewClient {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ WebNav f253a;
    private ah b;

    public i(WebNav webNav, ah ahVar) {
        this.f253a = webNav;
        this.b = ahVar;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        HashMap hashMap = new HashMap();
        hashMap.put("console", new JSONArray((Collection) this.f253a.e));
        JSONObject jSONObject = new JSONObject(hashMap);
        HashMap hashMap2 = new HashMap();
        hashMap2.put("crash_report", jSONObject.toString());
        w.a(new r("/webui/crash_report", "POST", hashMap2));
    }

    public final void onPageFinished(WebView webView, String str) {
        if (!this.f253a.b) {
            this.f253a.b = true;
            if (this.f253a.c) {
                w a2 = w.a();
                k d = a2.d();
                int i = this.f253a.getResources().getConfiguration().orientation;
                HashMap hashMap = new HashMap();
                if (d != null) {
                    hashMap.put("id", d.c());
                    hashMap.put("name", d.f159a);
                }
                HashMap hashMap2 = new HashMap();
                hashMap2.put("id", a2.i());
                hashMap2.put("name", a2.h());
                hashMap2.put("version", Integer.toString(a2.k()));
                Map c = w.a().c();
                HashMap hashMap3 = new HashMap();
                hashMap3.put("platform", "android");
                hashMap3.put("clientVersion", a2.g());
                hashMap3.put("hasNativeInterface", true);
                hashMap3.put("dpi", v.c(this.f253a));
                hashMap3.put("locale", this.f253a.getResources().getConfiguration().locale.toString());
                hashMap3.put("user", new JSONObject(hashMap));
                hashMap3.put("game", new JSONObject(hashMap2));
                hashMap3.put("device", new JSONObject(c));
                hashMap3.put("actions", new JSONArray((Collection) this.f253a.f232a.b));
                hashMap3.put("orientation", i == 2 ? "landscape" : "portrait");
                hashMap3.put("serverUrl", a2.f());
                this.f253a.a(String.format("OF.init.clientBoot(%s)", new JSONObject(hashMap3).toString()));
                this.b.f241a.b();
            } else if (s.e()) {
                this.f253a.a(true);
                new AlertDialog.Builder(webView.getContext()).setMessage(w.a((int) C0000R.string.of_crash_report_query)).setNegativeButton(w.a((int) C0000R.string.of_no), new p(this)).setPositiveButton(w.a((int) C0000R.string.of_yes), new q(this)).show();
            } else if (!s.d()) {
                this.f253a.finish();
            }
        }
    }

    public final void onReceivedError(WebView webView, int i, String str, String str2) {
    }

    public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
        Uri parse = Uri.parse(str);
        if (parse.getScheme().equals("http") || parse.getScheme().equals("https")) {
            webView.loadUrl(str);
        } else if (parse.getScheme().equals("openfeint")) {
            ah ahVar = this.b;
            if (parse.getHost().equals("action")) {
                String encodedQuery = parse.getEncodedQuery();
                HashMap hashMap = new HashMap();
                if (encodedQuery != null) {
                    for (String split : encodedQuery.split("&")) {
                        String[] split2 = split.split("=");
                        if (split2.length == 2) {
                            hashMap.put(split2[0], Uri.decode(split2[1]));
                        } else {
                            hashMap.put(split2[0], null);
                        }
                    }
                }
                String replaceFirst = parse.getPath().replaceFirst("/", "");
                if (!replaceFirst.equals("log")) {
                    HashMap hashMap2 = new HashMap(hashMap);
                    String str2 = (String) hashMap.get("params");
                    if (str2 != null && str2.contains("password")) {
                        hashMap2.put("params", "---FILTERED---");
                    }
                    "ACTION: " + replaceFirst + " " + hashMap2.toString();
                }
                if (ahVar.b.contains(replaceFirst)) {
                    try {
                        ahVar.getClass().getMethod(replaceFirst, Map.class).invoke(ahVar, hashMap);
                    } catch (NoSuchMethodException e) {
                        "mActionList contains this method, but it is not implemented: " + replaceFirst;
                    } catch (Exception e2) {
                        "Unhandled Exception: " + e2.toString() + "   " + e2.getCause();
                    }
                } else {
                    "UNHANDLED ACTION: " + replaceFirst;
                }
            } else {
                "UNHANDLED MESSAGE TYPE: " + parse.getHost();
            }
        } else {
            "UNHANDLED PROTOCOL: " + parse.getScheme();
        }
        return true;
    }
}
