package com.openfeint.internal.ui;

import android.database.sqlite.SQLiteDiskIOException;
import com.openfeint.internal.e.c;
import java.util.HashMap;
import java.util.Map;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

final class n extends DefaultHandler {

    /* renamed from: a  reason: collision with root package name */
    Map f257a;
    private String b;
    private String c;
    private /* synthetic */ s d;

    /* synthetic */ n(s sVar) {
        this(sVar, (byte) 0);
    }

    private n(s sVar, byte b2) {
        this.d = sVar;
        this.f257a = new HashMap();
    }

    public final void characters(char[] cArr, int i, int i2) {
        this.b = new String(cArr).substring(i, i + i2);
    }

    public final void endElement(String str, String str2, String str3) {
        String trim = str2.trim();
        if (trim.equals("key")) {
            this.c = this.b;
        } else if (trim.equals("string")) {
            this.f257a.put(this.c, this.b);
            String str4 = this.c;
            String str5 = this.b;
            try {
                c.f216a.a().execSQL("INSERT OR REPLACE INTO manifest(path, hash) VALUES(?,?)", new String[]{str4, str5});
            } catch (SQLiteDiskIOException e) {
                s.c();
            } catch (Exception e2) {
                e2.toString();
            }
        }
    }

    public final void startElement(String str, String str2, String str3, Attributes attributes) {
        this.b = "";
    }
}
