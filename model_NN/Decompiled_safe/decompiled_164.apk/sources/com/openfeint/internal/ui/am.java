package com.openfeint.internal.ui;

import android.app.AlertDialog;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import com.ElekaSoftware.OfficeRage.C0000R;
import com.openfeint.internal.w;

final class am extends WebChromeClient {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ WebNav f246a;

    /* synthetic */ am(WebNav webNav) {
        this(webNav, (byte) 0);
    }

    private am(WebNav webNav, byte b) {
        this.f246a = webNav;
    }

    public final void onConsoleMessage(String str, int i, String str2) {
        if (!this.f246a.c) {
            this.f246a.e.add(String.format("%s at %s:%d)", str, str2, Integer.valueOf(i)));
        }
    }

    public final boolean onJsAlert(WebView webView, String str, String str2, JsResult jsResult) {
        new AlertDialog.Builder(webView.getContext()).setMessage(str2).setNegativeButton(w.a((int) C0000R.string.of_ok), new b(this, jsResult)).setOnCancelListener(new c(this, jsResult)).show();
        return true;
    }

    public final boolean onJsConfirm(WebView webView, String str, String str2, JsResult jsResult) {
        new AlertDialog.Builder(webView.getContext()).setMessage(str2).setPositiveButton(w.a((int) C0000R.string.of_ok), new e(this, jsResult)).setNegativeButton(w.a((int) C0000R.string.of_cancel), new f(this, jsResult)).setOnCancelListener(new g(this, jsResult)).show();
        return true;
    }
}
