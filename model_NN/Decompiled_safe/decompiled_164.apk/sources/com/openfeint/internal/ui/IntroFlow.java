package com.openfeint.internal.ui;

import android.content.Intent;
import android.graphics.Bitmap;
import com.openfeint.internal.m;

public class IntroFlow extends WebNav {
    private Bitmap g;

    /* access modifiers changed from: protected */
    public final ah a(WebNav webNav) {
        return new l(this, webNav);
    }

    /* access modifiers changed from: protected */
    public final String a() {
        String stringExtra = getIntent().getStringExtra("content_name");
        return stringExtra != null ? "intro/" + stringExtra : "intro/index";
    }

    public void onActivityResult(int i, int i2, Intent intent) {
        if (m.a(i)) {
            this.g = m.a(this, i2, intent);
        }
    }
}
