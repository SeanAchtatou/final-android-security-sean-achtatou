package com.openfeint.internal.ui;

import java.util.Set;

final class y implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ s f267a;
    private final /* synthetic */ String b;
    private final /* synthetic */ String c;
    private final /* synthetic */ Set d;
    private final /* synthetic */ int e;

    y(s sVar, String str, String str2, Set set, int i) {
        this.f267a = sVar;
        this.b = str;
        this.c = str2;
        this.d = set;
        this.e = i;
    }

    public final void run() {
        if (this.b.equals(this.c)) {
            this.f267a.a(this.d, this.e - 1);
        } else {
            this.f267a.a(this.b, this.c, this.e - 1, this.d);
        }
    }
}
