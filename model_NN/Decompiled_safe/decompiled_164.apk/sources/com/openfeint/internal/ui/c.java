package com.openfeint.internal.ui;

import android.content.DialogInterface;
import android.webkit.JsResult;

final class c implements DialogInterface.OnCancelListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ am f248a;
    private final /* synthetic */ JsResult b;

    c(am amVar, JsResult jsResult) {
        this.f248a = amVar;
        this.b = jsResult;
    }

    public final void onCancel(DialogInterface dialogInterface) {
        this.b.cancel();
    }
}
