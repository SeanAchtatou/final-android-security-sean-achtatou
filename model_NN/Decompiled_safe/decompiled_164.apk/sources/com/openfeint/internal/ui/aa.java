package com.openfeint.internal.ui;

import android.app.AlertDialog;
import com.ElekaSoftware.OfficeRage.C0000R;
import com.openfeint.internal.v;
import com.openfeint.internal.w;

final class aa implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ WebNav f234a;

    aa(WebNav webNav) {
        this.f234a = webNav;
    }

    public final void run() {
        this.f234a.d();
        new AlertDialog.Builder(this.f234a).setMessage(String.format(w.a((int) C0000R.string.of_nodisk), v.a(this.f234a) ? w.a((int) C0000R.string.of_sdcard) : w.a((int) C0000R.string.of_device))).setPositiveButton(w.a((int) C0000R.string.of_no), new a(this)).show();
    }
}
