package com.openfeint.internal;

import android.os.Bundle;
import java.util.Date;
import java.util.HashSet;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.http.impl.cookie.DateParseException;
import org.apache.http.impl.cookie.DateUtils;

public final class l extends BasicCookieStore {

    /* renamed from: a  reason: collision with root package name */
    private static int f223a = "_of_cookie_".length();
    private static int b = "value".length();
    private u c;

    public l(u uVar) {
        this.c = uVar;
        n b2 = this.c.b();
        try {
            for (String str : b2.f224a.f229a.keySet()) {
                if (str.startsWith("_of_cookie_") && str.endsWith("value")) {
                    l.super.addCookie(a(b2, str.substring(f223a, str.length() - b)));
                }
            }
        } finally {
            b2.a();
        }
    }

    private static final Date a(String str) {
        try {
            return DateUtils.parseDate(str);
        } catch (DateParseException e) {
            "Couldn't parse date: '" + str + "'";
            return null;
        }
    }

    private static BasicClientCookie a(n nVar, String str) {
        String str2 = "_of_cookie_" + str;
        String a2 = nVar.a(String.valueOf(str2) + "value", null);
        if (a2 == null) {
            return null;
        }
        String a3 = nVar.a(String.valueOf(str2) + "path", null);
        String a4 = nVar.a(String.valueOf(str2) + "domain", null);
        String a5 = nVar.a(String.valueOf(str2) + "expiry", null);
        BasicClientCookie basicClientCookie = new BasicClientCookie(str, a2);
        basicClientCookie.setPath(a3);
        basicClientCookie.setDomain(a4);
        basicClientCookie.setExpiryDate(a(a5));
        return basicClientCookie;
    }

    public final synchronized void a(Bundle bundle) {
        for (Cookie cookie : l.super.getCookies()) {
            String name = cookie.getName();
            bundle.putString("_of_cookie_" + name + "value", cookie.getValue());
            bundle.putString("_of_cookie_" + name + "path", cookie.getPath());
            bundle.putString("_of_cookie_" + name + "domain", cookie.getDomain());
            Date expiryDate = cookie.getExpiryDate();
            if (expiryDate != null) {
                bundle.putString("_of_cookie_" + name + "expiry", DateUtils.formatDate(expiryDate));
            }
        }
    }

    public final synchronized void addCookie(Cookie cookie) {
        a a2;
        l.super.addCookie(cookie);
        String name = cookie.getName();
        n b2 = this.c.b();
        try {
            BasicClientCookie a3 = a(b2, name);
            b2.a();
            if (a3 == null || !a3.getValue().equals(cookie.getValue()) || !a3.getPath().equals(cookie.getPath()) || !a3.getDomain().equals(cookie.getDomain()) || !a3.getExpiryDate().equals(cookie.getExpiryDate())) {
                a2 = this.c.a();
                String str = "_of_cookie_" + name;
                for (String str2 : new HashSet(a2.f169a.f229a.keySet())) {
                    if (str2.startsWith(str)) {
                        a2.a(str2);
                    }
                }
                if (cookie.getExpiryDate() != null) {
                    String name2 = cookie.getName();
                    a2.a("_of_cookie_" + name2 + "value", cookie.getValue());
                    a2.a("_of_cookie_" + name2 + "path", cookie.getPath());
                    a2.a("_of_cookie_" + name2 + "domain", cookie.getDomain());
                    a2.a("_of_cookie_" + name2 + "expiry", DateUtils.formatDate(cookie.getExpiryDate()));
                }
                a2.a();
            }
        } catch (Throwable th) {
            b2.a();
            throw th;
        }
    }

    public final synchronized void b(Bundle bundle) {
        Cookie cookie;
        for (String next : bundle.keySet()) {
            if (next.startsWith("_of_cookie_") && next.endsWith("value")) {
                String substring = next.substring(f223a, next.length() - b);
                String str = "_of_cookie_" + substring;
                String string = bundle.getString(String.valueOf(str) + "value");
                if (string == null) {
                    cookie = null;
                } else {
                    String string2 = bundle.getString(String.valueOf(str) + "path");
                    String string3 = bundle.getString(String.valueOf(str) + "domain");
                    String string4 = bundle.getString(String.valueOf(str) + "expiry");
                    Cookie basicClientCookie = new BasicClientCookie(substring, string);
                    basicClientCookie.setPath(string2);
                    basicClientCookie.setDomain(string3);
                    if (string4 != null) {
                        basicClientCookie.setExpiryDate(a(string4));
                    }
                    cookie = basicClientCookie;
                }
                l.super.addCookie(cookie);
            }
        }
    }
}
