package com.openfeint.internal;

import com.openfeint.api.a.bc;

final class r implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ w f227a;
    private final /* synthetic */ boolean b = false;

    r(w wVar) {
        this.f227a = wVar;
    }

    public final void run() {
        if (!this.f227a.k && !this.f227a.f && !this.f227a.e()) {
            this.f227a.l = true;
            bc o = this.f227a.v();
            if (o != null) {
                "Logging in last known user: " + o.f159a;
                this.f227a.a((String) null, (String) null, (String) null, new b(this, this.b, o));
                return;
            }
            w.q(this.f227a);
            this.f227a.a(this.b);
        }
    }
}
