package com.openfeint.internal;

import a.a.a.n;
import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public final class v {

    /* renamed from: a  reason: collision with root package name */
    private static int f269a = Integer.valueOf(Build.VERSION.SDK).intValue();

    public static DisplayMetrics a() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((WindowManager) w.a().l().getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics;
    }

    public static Object a(byte[] bArr) {
        try {
            return new s(new n((byte) 0).a(bArr)).a();
        } catch (Exception e) {
            e.getMessage();
            return null;
        }
    }

    public static void a(Activity activity) {
        Integer num = (Integer) w.a().c.e.get("RequestedOrientation");
        if (num != null) {
            activity.setRequestedOrientation(num.intValue());
        }
    }

    public static void a(File file) {
        if (file.isDirectory()) {
            for (String file2 : file.list()) {
                a(new File(file, file2));
            }
        }
        file.delete();
    }

    public static void a(File file, File file2) {
        if (file.isDirectory()) {
            if (!file2.exists()) {
                file2.mkdir();
            }
            String[] list = file.list();
            for (int i = 0; i < list.length; i++) {
                a(new File(file, list[i]), new File(file2, list[i]));
            }
            return;
        }
        b(file, file2);
    }

    public static void a(InputStream inputStream, OutputStream outputStream) {
        b(inputStream, outputStream);
        inputStream.close();
    }

    public static void a(InputStream inputStream, String str) {
        File file = new File(str);
        file.getParentFile().mkdirs();
        b(inputStream, new FileOutputStream(file));
    }

    public static void a(byte[] bArr, String str) {
        File file = new File(str);
        file.getParentFile().mkdirs();
        FileOutputStream fileOutputStream = new FileOutputStream(file);
        fileOutputStream.write(bArr);
        fileOutputStream.close();
    }

    public static boolean a(Context context) {
        if (!a("android.permission.WRITE_EXTERNAL_STORAGE", context)) {
            return false;
        }
        return "mounted".equals(Environment.getExternalStorageState());
    }

    public static boolean a(String str, Context context) {
        return -1 == context.getPackageManager().checkPermission(str, context.getPackageName());
    }

    public static final byte[] a(InputStream inputStream) {
        byte[] bArr = new byte[4096];
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        while (true) {
            int read = inputStream.read(bArr);
            if (read <= 0) {
                byteArrayOutputStream.close();
                return byteArrayOutputStream.toByteArray();
            }
            byteArrayOutputStream.write(bArr, 0, read);
        }
    }

    public static byte[] a(String str) {
        File file = new File(str);
        FileInputStream fileInputStream = new FileInputStream(file);
        byte[] bArr = new byte[((int) file.length())];
        fileInputStream.read(bArr);
        fileInputStream.close();
        return bArr;
    }

    public static void b(Context context) {
        if (a("android.permission.WRITE_EXTERNAL_STORAGE", context)) {
            File file = new File(context.getCacheDir(), "webviewCache");
            if (!b(file) && "mounted".equals(Environment.getExternalStorageState())) {
                File file2 = new File(Environment.getExternalStorageDirectory(), "openfeint/cache");
                if (!file2.exists()) {
                    file2.mkdirs();
                }
                a(file);
                String absolutePath = file2.getAbsolutePath();
                try {
                    Runtime.getRuntime().exec("ln -s " + absolutePath + " " + file.getAbsolutePath());
                } catch (Exception e) {
                    e.getMessage();
                }
            }
        }
    }

    public static void b(File file, File file2) {
        a(new FileInputStream(file), new FileOutputStream(file2));
    }

    private static void b(InputStream inputStream, OutputStream outputStream) {
        byte[] bArr = new byte[16384];
        while (true) {
            int read = inputStream.read(bArr);
            if (read <= 0) {
                outputStream.close();
                return;
            }
            outputStream.write(bArr, 0, read);
        }
    }

    public static boolean b() {
        return a("android.permission.WRITE_EXTERNAL_STORAGE", w.a().l());
    }

    private static boolean b(File file) {
        try {
            return !file.getCanonicalPath().equals(file.getAbsolutePath());
        } catch (IOException e) {
            return false;
        }
    }

    public static String c(Context context) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((WindowManager) context.getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.density >= 2.0f ? "udpi" : ((double) displayMetrics.density) >= 1.5d ? "hdpi" : "mdpi";
    }
}
