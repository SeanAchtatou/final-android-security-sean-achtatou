package com.openfeint.internal.a;

import android.os.Bundle;
import android.os.Handler;
import com.openfeint.internal.l;
import java.util.concurrent.ExecutorService;
import org.apache.http.impl.client.DefaultHttpClient;

public final class x extends DefaultHttpClient {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public o f190a;
    /* access modifiers changed from: private */
    public Handler b;
    private l c;
    /* access modifiers changed from: private */
    public boolean d;
    private ExecutorService e = new n(this);

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public x(java.lang.String r6, java.lang.String r7, com.openfeint.internal.u r8) {
        /*
            r5 = this;
            org.apache.http.conn.scheme.SchemeRegistry r0 = new org.apache.http.conn.scheme.SchemeRegistry
            r0.<init>()
            org.apache.http.conn.scheme.Scheme r1 = new org.apache.http.conn.scheme.Scheme
            java.lang.String r2 = "http"
            org.apache.http.conn.scheme.PlainSocketFactory r3 = org.apache.http.conn.scheme.PlainSocketFactory.getSocketFactory()
            r4 = 80
            r1.<init>(r2, r3, r4)
            r0.register(r1)
            org.apache.http.conn.scheme.Scheme r1 = new org.apache.http.conn.scheme.Scheme
            java.lang.String r2 = "https"
            org.apache.http.conn.ssl.SSLSocketFactory r3 = org.apache.http.conn.ssl.SSLSocketFactory.getSocketFactory()
            r4 = 443(0x1bb, float:6.21E-43)
            r1.<init>(r2, r3, r4)
            r0.register(r1)
            org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager r1 = new org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager
            org.apache.http.params.BasicHttpParams r2 = new org.apache.http.params.BasicHttpParams
            r2.<init>()
            r1.<init>(r2, r0)
            org.apache.http.params.BasicHttpParams r0 = new org.apache.http.params.BasicHttpParams
            r0.<init>()
            r5.<init>(r1, r0)
            com.openfeint.internal.a.n r0 = new com.openfeint.internal.a.n
            r0.<init>(r5)
            r5.e = r0
            com.openfeint.internal.a.o r0 = new com.openfeint.internal.a.o
            r0.<init>(r6, r7)
            r5.f190a = r0
            android.os.Handler r0 = new android.os.Handler
            r0.<init>()
            r5.b = r0
            com.openfeint.internal.l r0 = new com.openfeint.internal.l
            r0.<init>(r8)
            r5.c = r0
            com.openfeint.internal.l r0 = r5.c
            r5.setCookieStore(r0)
            com.openfeint.internal.a.b r0 = new com.openfeint.internal.a.b
            r0.<init>(r5)
            r5.addRequestInterceptor(r0)
            com.openfeint.internal.a.f r0 = new com.openfeint.internal.a.f
            r0.<init>(r5)
            r5.addResponseInterceptor(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.openfeint.internal.a.x.<init>(java.lang.String, java.lang.String, com.openfeint.internal.u):void");
    }

    public final void a(Bundle bundle) {
        this.c.a(bundle);
        bundle.putBoolean("mForceOffline", this.d);
    }

    public final void a(k kVar) {
        long g = kVar.g();
        e eVar = new e(this, kVar);
        kVar.a(this.e.submit(new c(this, new d(this, kVar, eVar), g, kVar, eVar)));
    }

    public final void b(Bundle bundle) {
        this.c.b(bundle);
        this.d = bundle.getBoolean("mForceOffline");
    }
}
