package com.openfeint.internal.a;

import com.ElekaSoftware.OfficeRage.C0000R;
import com.openfeint.internal.b.d;
import com.openfeint.internal.w;
import java.io.UnsupportedEncodingException;
import java.util.concurrent.Future;
import org.apache.http.HttpResponse;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.params.HttpParams;

public abstract class k {

    /* renamed from: a  reason: collision with root package name */
    private static int f181a = 2;
    private static long b = 20000;
    private static String c = "Request";
    private static String k = null;
    private g d;
    /* access modifiers changed from: private */
    public HttpUriRequest e;
    /* access modifiers changed from: private */
    public byte[] f;
    private boolean g = false;
    /* access modifiers changed from: private */
    public String h = null;
    /* access modifiers changed from: private */
    public String i = null;
    /* access modifiers changed from: private */
    public int j;
    private long l;
    private String m = null;
    private String n = null;
    private int o = 0;
    /* access modifiers changed from: private */
    public String p = null;
    private Future q = null;
    private HttpParams r = null;
    /* access modifiers changed from: private */
    public HttpResponse s;

    public k() {
    }

    public k(g gVar) {
        this.d = gVar;
    }

    private void a(d dVar) {
        this.j = 0;
        this.f = dVar.d().getBytes();
        this.i = "application/json;";
    }

    public abstract String a();

    public abstract void a(int i2, byte[] bArr);

    public final void a(g gVar) {
        this.d = gVar;
    }

    public final void a(o oVar) {
        if (this.d == null) {
            this.d = new g();
        }
        if (k()) {
            this.l = System.currentTimeMillis() / 1000;
            this.m = oVar.a(b(), a(), this.d);
            this.n = oVar.a();
        }
    }

    public final void a(Future future) {
        this.q = future;
    }

    /* access modifiers changed from: protected */
    public final void a(HttpUriRequest httpUriRequest) {
        if (this.r != null) {
            httpUriRequest.setParams(this.r);
        }
    }

    /* JADX WARNING: CFG modification limit reached, blocks count: 124 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(boolean r8) {
        /*
            r7 = this;
            r6 = 1
            r5 = 0
            r4 = 0
            org.apache.http.client.methods.HttpUriRequest r0 = r7.c()
            r7.e = r0
            int r0 = r7.f()
            r7.o = r0
            r7.f = r4
            r7.j = r5
            r7.s = r4
            org.apache.http.client.methods.HttpUriRequest r0 = r7.e
            java.net.URI r0 = r0.getURI()
            java.lang.String r0 = r0.getPath()
            java.lang.String r1 = "//"
            boolean r0 = r0.contains(r1)
            if (r0 == 0) goto L_0x00be
            com.openfeint.internal.b.d r0 = new com.openfeint.internal.b.d
            r0.<init>()
            java.lang.String r1 = "RequestError"
            r0.f196a = r1
            r1 = 2131165226(0x7f07002a, float:1.7944663E38)
            java.lang.String r1 = com.openfeint.internal.w.a(r1)
            r0.b = r1
            r0.c = r6
            r7.a(r0)
        L_0x003e:
            int r0 = r7.j
            byte[] r1 = r7.f
            r7.b(r0, r1)
            return
        L_0x0046:
            if (r8 == 0) goto L_0x00a4
            r0 = 0
            r7.o = r0     // Catch:{ Exception -> 0x0053 }
            java.lang.Exception r0 = new java.lang.Exception     // Catch:{ Exception -> 0x0053 }
            java.lang.String r1 = "Forced failure"
            r0.<init>(r1)     // Catch:{ Exception -> 0x0053 }
            throw r0     // Catch:{ Exception -> 0x0053 }
        L_0x0053:
            r0 = move-exception
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r2 = "Error executing request '"
            r1.<init>(r2)
            java.lang.String r2 = r7.b()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = "'."
            java.lang.StringBuilder r1 = r1.append(r2)
            r1.toString()
            java.io.PrintStream r1 = java.lang.System.err
            r0.printStackTrace(r1)
            r7.f = r4
            r7.j = r5
            r7.s = r4
            int r1 = r7.o
            int r1 = r1 - r6
            r7.o = r1
            if (r1 >= 0) goto L_0x00be
            com.openfeint.internal.b.d r1 = new com.openfeint.internal.b.d
            r1.<init>()
            java.lang.Class r2 = r0.getClass()
            java.lang.String r2 = r2.getName()
            r1.f196a = r2
            java.lang.String r0 = r0.getMessage()
            r1.b = r0
            java.lang.String r0 = r1.b
            if (r0 != 0) goto L_0x00a0
            r0 = 2131165189(0x7f070005, float:1.7944588E38)
            java.lang.String r0 = com.openfeint.internal.w.a(r0)
            r1.b = r0
        L_0x00a0:
            r7.a(r1)
            goto L_0x003e
        L_0x00a4:
            com.openfeint.internal.w r0 = com.openfeint.internal.w.a()     // Catch:{ Exception -> 0x0053 }
            org.apache.http.impl.client.AbstractHttpClient r0 = r0.b()     // Catch:{ Exception -> 0x0053 }
            org.apache.http.protocol.BasicHttpContext r1 = new org.apache.http.protocol.BasicHttpContext     // Catch:{ Exception -> 0x0053 }
            r1.<init>()     // Catch:{ Exception -> 0x0053 }
            com.openfeint.internal.a.z r2 = new com.openfeint.internal.a.z     // Catch:{ Exception -> 0x0053 }
            r2.<init>(r7, r1)     // Catch:{ Exception -> 0x0053 }
            org.apache.http.client.methods.HttpUriRequest r3 = r7.e     // Catch:{ Exception -> 0x0053 }
            r0.execute(r3, r2, r1)     // Catch:{ Exception -> 0x0053 }
            r0 = 0
            r7.e = r0     // Catch:{ Exception -> 0x0053 }
        L_0x00be:
            byte[] r0 = r7.f
            if (r0 == 0) goto L_0x0046
            goto L_0x003e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.openfeint.internal.a.k.a(boolean):void");
    }

    public abstract String b();

    /* access modifiers changed from: protected */
    public void b(int i2, byte[] bArr) {
    }

    /* access modifiers changed from: protected */
    public HttpUriRequest c() {
        HttpPost httpPost = null;
        String a2 = a();
        if (a2.equals("GET") || a2.equals("DELETE")) {
            String m2 = m();
            String a3 = this.d.a();
            if (a3 != null) {
                m2 = String.valueOf(m2) + "?" + a3;
            }
            if (a2.equals("GET")) {
                httpPost = new HttpGet(m2);
            } else if (a2.equals("DELETE")) {
                httpPost = new HttpDelete(m2);
            }
        } else {
            if (a2.equals("POST")) {
                httpPost = new HttpPost(m());
            } else if (a2.equals("PUT")) {
                httpPost = new HttpPut(m());
            } else {
                throw new RuntimeException("Unsupported HTTP method: " + a2);
            }
            try {
                UrlEncodedFormEntity urlEncodedFormEntity = new UrlEncodedFormEntity(this.d.f180a, "UTF-8");
                urlEncodedFormEntity.setContentType("application/x-www-form-urlencoded; charset=" + "UTF-8");
                httpPost.setEntity(urlEncodedFormEntity);
            } catch (UnsupportedEncodingException e2) {
                e2.printStackTrace(System.err);
            }
        }
        if (!(!k() || this.m == null || this.n == null)) {
            httpPost.addHeader("X-OF-Signature", this.m);
            httpPost.addHeader("X-OF-Key", this.n);
        }
        a((HttpUriRequest) httpPost);
        return httpPost;
    }

    /* access modifiers changed from: protected */
    public final String e() {
        return this.i;
    }

    public int f() {
        return f181a;
    }

    public long g() {
        return b;
    }

    /* access modifiers changed from: protected */
    public final String h() {
        return this.p != null ? this.p : m();
    }

    public final Future i() {
        return this.q;
    }

    public boolean j() {
        return false;
    }

    public boolean k() {
        return true;
    }

    public boolean l() {
        return k();
    }

    public String m() {
        if (k == null) {
            k = w.a().f();
        }
        return String.valueOf(k) + b();
    }

    public final HttpResponse n() {
        return this.s;
    }

    public final void o() {
        if (!this.g) {
            this.g = true;
            if (this.f == null) {
                this.j = 0;
                d dVar = new d();
                dVar.f196a = "Unknown";
                dVar.b = w.a((int) C0000R.string.of_unknown_server_error);
                a(dVar);
            }
            a(this.j, this.f);
            this.s = null;
        }
    }

    public final void p() {
        w.a(this);
    }

    public final void q() {
        HttpUriRequest httpUriRequest = this.e;
        this.e = null;
        if (httpUriRequest != null) {
            new Thread(new y(this, httpUriRequest)).start();
        }
        d dVar = new d();
        dVar.f196a = "Timeout";
        dVar.b = w.a((int) C0000R.string.of_timeout);
        a(dVar);
    }
}
