package com.openfeint.internal.a;

import org.apache.http.Header;
import org.apache.http.HeaderElement;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpResponseInterceptor;
import org.apache.http.protocol.HttpContext;

final class f implements HttpResponseInterceptor {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ x f179a;

    f(x xVar) {
        this.f179a = xVar;
    }

    public final void process(HttpResponse httpResponse, HttpContext httpContext) {
        Header contentEncoding;
        HeaderElement[] elements;
        HttpEntity entity = httpResponse.getEntity();
        if (entity != null && (contentEncoding = entity.getContentEncoding()) != null && (elements = contentEncoding.getElements()) != null) {
            for (HeaderElement name : elements) {
                if (name.getName().equalsIgnoreCase("gzip")) {
                    httpResponse.setEntity(new l(httpResponse.getEntity()));
                    return;
                }
            }
        }
    }
}
