package com.openfeint.internal.a;

import com.a.a.a.a.c;
import java.util.ArrayList;
import java.util.Map;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

public final class g {

    /* renamed from: a  reason: collision with root package name */
    ArrayList f180a = new ArrayList();

    public g() {
    }

    public g(Map map) {
        a(map, (String) null);
    }

    private void a(Map map, String str) {
        for (Map.Entry entry : map.entrySet()) {
            String str2 = (String) entry.getKey();
            Object value = entry.getValue();
            String str3 = str == null ? str2 : String.valueOf(str) + "[" + str2 + "]";
            if (value instanceof Map) {
                a((Map) value, str3);
            } else {
                a(str3, value.toString());
            }
        }
    }

    public final String a() {
        ArrayList<NameValuePair> arrayList = this.f180a;
        c cVar = new c("-_.*");
        StringBuilder sb = null;
        for (NameValuePair nameValuePair : arrayList) {
            if (sb == null) {
                sb = new StringBuilder();
            } else {
                sb.append('&');
            }
            sb.append(cVar.a(nameValuePair.getName()));
            sb.append('=');
            if (nameValuePair.getValue() != null) {
                sb.append(cVar.a(nameValuePair.getValue()));
            }
        }
        if (sb == null) {
            return null;
        }
        return sb.toString();
    }

    public final void a(String str, String str2) {
        this.f180a.add(new BasicNameValuePair(str, str2));
    }
}
