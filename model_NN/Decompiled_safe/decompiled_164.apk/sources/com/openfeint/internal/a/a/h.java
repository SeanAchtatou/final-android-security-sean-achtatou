package com.openfeint.internal.a.a;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

public final class h implements c {

    /* renamed from: a  reason: collision with root package name */
    private String f173a;
    private byte[] b;

    public h(String str, byte[] bArr) {
        this.f173a = str;
        this.b = bArr;
    }

    public final long a() {
        return (long) this.b.length;
    }

    public final String b() {
        return this.f173a;
    }

    public final InputStream c() {
        return new ByteArrayInputStream(this.b);
    }
}
