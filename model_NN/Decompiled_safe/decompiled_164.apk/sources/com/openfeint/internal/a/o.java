package com.openfeint.internal.a;

import a.b.a.a.a.b;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

public final class o {

    /* renamed from: a  reason: collision with root package name */
    private String f183a;
    private String b;
    private String c = (String.valueOf(this.b) + "&");
    private String d;

    public o(String str, String str2) {
        this.f183a = str;
        this.b = str2;
    }

    public final String a() {
        return this.f183a;
    }

    public final String a(String str, String str2, g gVar) {
        if (this.d != null) {
            gVar.a("token", this.d);
        }
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append('+');
        sb.append(this.b);
        sb.append('+');
        sb.append(str2);
        sb.append('+');
        String a2 = gVar.a();
        if (a2 == null) {
            a2 = "";
        }
        sb.append(a2);
        try {
            SecretKeySpec secretKeySpec = new SecretKeySpec(this.c.getBytes("UTF-8"), "HmacSHA1");
            Mac instance = Mac.getInstance("HmacSHA1");
            instance.init(secretKeySpec);
            return new String(b.a(instance.doFinal(sb.toString().getBytes("UTF-8")))).replace("\r\n", "");
        } catch (UnsupportedEncodingException | InvalidKeyException | NoSuchAlgorithmException e) {
            return null;
        }
    }
}
