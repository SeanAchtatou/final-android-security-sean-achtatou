package com.openfeint.internal.a;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDiskIOException;
import com.openfeint.internal.e.c;
import com.openfeint.internal.ui.s;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpUriRequest;

public abstract class t extends k {

    /* renamed from: a  reason: collision with root package name */
    private String f187a;

    public t(String str) {
        this.f187a = str;
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x0029  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.String d() {
        /*
            r6 = this;
            r4 = 0
            com.openfeint.internal.e.b r0 = com.openfeint.internal.e.c.f216a     // Catch:{ SQLiteDiskIOException -> 0x002d, Exception -> 0x0034 }
            android.database.sqlite.SQLiteDatabase r0 = r0.b()     // Catch:{ SQLiteDiskIOException -> 0x002d, Exception -> 0x0034 }
            r1 = 1
            java.lang.String[] r1 = new java.lang.String[r1]     // Catch:{ SQLiteDiskIOException -> 0x002d, Exception -> 0x0034 }
            r2 = 0
            java.lang.String r3 = r6.f187a     // Catch:{ SQLiteDiskIOException -> 0x002d, Exception -> 0x0034 }
            r1[r2] = r3     // Catch:{ SQLiteDiskIOException -> 0x002d, Exception -> 0x0034 }
            java.lang.String r2 = "SELECT VALUE FROM store where id=?"
            android.database.Cursor r1 = r0.rawQuery(r2, r1)     // Catch:{ SQLiteDiskIOException -> 0x002d, Exception -> 0x0034 }
            int r2 = r1.getCount()     // Catch:{ SQLiteDiskIOException -> 0x0046, Exception -> 0x003d }
            if (r2 <= 0) goto L_0x004c
            r1.moveToFirst()     // Catch:{ SQLiteDiskIOException -> 0x0046, Exception -> 0x003d }
            r2 = 0
            java.lang.String r2 = r1.getString(r2)     // Catch:{ SQLiteDiskIOException -> 0x0046, Exception -> 0x003d }
        L_0x0023:
            r0.close()     // Catch:{ SQLiteDiskIOException -> 0x0049, Exception -> 0x0041 }
            r0 = r2
        L_0x0027:
            if (r1 == 0) goto L_0x002c
            r1.close()
        L_0x002c:
            return r0
        L_0x002d:
            r0 = move-exception
            r0 = r4
            r1 = r4
        L_0x0030:
            com.openfeint.internal.ui.s.c()
            goto L_0x0027
        L_0x0034:
            r0 = move-exception
            r1 = r4
            r2 = r4
        L_0x0037:
            r0.getMessage()
            r0 = r1
            r1 = r2
            goto L_0x0027
        L_0x003d:
            r0 = move-exception
            r2 = r1
            r1 = r4
            goto L_0x0037
        L_0x0041:
            r0 = move-exception
            r5 = r2
            r2 = r1
            r1 = r5
            goto L_0x0037
        L_0x0046:
            r0 = move-exception
            r0 = r4
            goto L_0x0030
        L_0x0049:
            r0 = move-exception
            r0 = r2
            goto L_0x0030
        L_0x004c:
            r2 = r4
            goto L_0x0023
        */
        throw new UnsupportedOperationException("Method not decompiled: com.openfeint.internal.a.t.d():java.lang.String");
    }

    public final String a() {
        return "GET";
    }

    /* access modifiers changed from: protected */
    public final void a(HttpResponse httpResponse) {
        String value;
        Header firstHeader = httpResponse != null ? httpResponse.getFirstHeader("Last-Modified") : null;
        if (firstHeader != null && (value = firstHeader.getValue()) != null) {
            String[] strArr = {this.f187a, value};
            try {
                SQLiteDatabase a2 = c.f216a.a();
                a2.execSQL("INSERT OR REPLACE INTO store VALUES(?, ?)", strArr);
                a2.close();
            } catch (SQLiteDiskIOException e) {
                s.c();
            } catch (Exception e2) {
                e2.getMessage();
            }
        }
    }

    /* access modifiers changed from: protected */
    public final HttpUriRequest c() {
        HttpUriRequest c = super.c();
        String d = d();
        if (d != null) {
            c.addHeader("If-Modified-Since", d);
        }
        return c;
    }
}
