package com.openfeint.internal.a;

import org.apache.http.HttpRequest;
import org.apache.http.HttpRequestInterceptor;
import org.apache.http.protocol.HttpContext;

final class b implements HttpRequestInterceptor {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ x f175a;

    b(x xVar) {
        this.f175a = xVar;
    }

    public final void process(HttpRequest httpRequest, HttpContext httpContext) {
        if (!httpRequest.containsHeader("Accept-Encoding")) {
            httpRequest.addHeader("Accept-Encoding", "gzip");
        }
    }
}
