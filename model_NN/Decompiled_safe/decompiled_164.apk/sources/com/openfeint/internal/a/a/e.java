package com.openfeint.internal.a.a;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.Random;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.message.BasicHeader;

public final class e implements HttpEntity {

    /* renamed from: a  reason: collision with root package name */
    private static byte[] f171a = d.a("-_1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ");
    private f[] b;
    private byte[] c;

    public e(f[] fVarArr) {
        if (fVarArr == null) {
            throw new IllegalArgumentException("parts cannot be null");
        }
        this.b = fVarArr;
    }

    private byte[] a() {
        if (this.c == null) {
            Random random = new Random();
            byte[] bArr = new byte[(random.nextInt(11) + 30)];
            for (int i = 0; i < bArr.length; i++) {
                bArr[i] = f171a[random.nextInt(f171a.length)];
            }
            this.c = bArr;
        }
        return this.c;
    }

    public final void consumeContent() {
        throw new UnsupportedOperationException();
    }

    public final InputStream getContent() {
        throw new UnsupportedOperationException();
    }

    public final Header getContentEncoding() {
        return new BasicHeader("Content-Encoding", "text/html; charset=UTF-8");
    }

    public final long getContentLength() {
        try {
            return f.a(this.b, a());
        } catch (Exception e) {
            return 0;
        }
    }

    public final Header getContentType() {
        StringBuffer stringBuffer = new StringBuffer("multipart/form-data");
        stringBuffer.append("; boundary=");
        byte[] a2 = a();
        stringBuffer.append(d.a(a2, a2.length));
        return new BasicHeader("Content-Type", stringBuffer.toString());
    }

    public final boolean isChunked() {
        return getContentLength() < 0;
    }

    public final boolean isRepeatable() {
        return true;
    }

    public final boolean isStreaming() {
        return false;
    }

    public final void writeTo(OutputStream outputStream) {
        f.a(outputStream, this.b, a());
    }
}
