package com.openfeint.internal.a;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import com.ElekaSoftware.OfficeRage.C0000R;
import com.openfeint.internal.w;

public abstract class u extends m {
    public void a(Bitmap bitmap) {
    }

    /* access modifiers changed from: protected */
    public final void a(byte[] bArr) {
        Bitmap decodeByteArray = BitmapFactory.decodeByteArray(bArr, 0, bArr.length);
        if (decodeByteArray != null) {
            a(decodeByteArray);
        } else {
            a(w.a((int) C0000R.string.of_bitmap_decode_error));
        }
    }
}
