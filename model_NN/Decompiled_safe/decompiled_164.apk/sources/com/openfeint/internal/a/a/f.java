package com.openfeint.internal.a.a;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;

public abstract class f {

    /* renamed from: a  reason: collision with root package name */
    protected static final byte[] f172a = d.a("\"");
    private static byte[] b;
    private static final byte[] c;
    private static byte[] d = d.a("\r\n");
    private static byte[] e = d.a("--");
    private static byte[] f = d.a("Content-Disposition: form-data; name=");
    private static byte[] g = d.a("Content-Type: ");
    private static byte[] h = d.a("; charset=");
    private static byte[] i = d.a("Content-Transfer-Encoding: ");
    private byte[] j;

    static {
        byte[] a2 = d.a("----------------ULTRASONIC_CUPCAKES___-__-");
        b = a2;
        c = a2;
    }

    public static long a(f[] fVarArr, byte[] bArr) {
        long size;
        if (fVarArr == null) {
            throw new IllegalArgumentException("Parts may not be null");
        }
        long j2 = 0;
        for (int i2 = 0; i2 < fVarArr.length; i2++) {
            fVarArr[i2].j = bArr;
            f fVar = fVarArr[i2];
            if (fVar.a() < 0) {
                size = -1;
            } else {
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                fVar.c(byteArrayOutputStream);
                fVar.a(byteArrayOutputStream);
                fVar.d(byteArrayOutputStream);
                fVar.e(byteArrayOutputStream);
                f(byteArrayOutputStream);
                byteArrayOutputStream.write(d);
                size = ((long) byteArrayOutputStream.size()) + fVar.a();
            }
            if (size < 0) {
                return -1;
            }
            j2 += size;
        }
        return j2 + ((long) e.length) + ((long) bArr.length) + ((long) e.length) + ((long) d.length);
    }

    public static void a(OutputStream outputStream, f[] fVarArr, byte[] bArr) {
        if (fVarArr == null) {
            throw new IllegalArgumentException("Parts may not be null");
        } else if (bArr == null || bArr.length == 0) {
            throw new IllegalArgumentException("partBoundary may not be empty");
        } else {
            for (int i2 = 0; i2 < fVarArr.length; i2++) {
                fVarArr[i2].j = bArr;
                f fVar = fVarArr[i2];
                fVar.c(outputStream);
                fVar.a(outputStream);
                fVar.d(outputStream);
                fVar.e(outputStream);
                f(outputStream);
                fVar.b(outputStream);
                outputStream.write(d);
            }
            outputStream.write(e);
            outputStream.write(bArr);
            outputStream.write(e);
            outputStream.write(d);
        }
    }

    private void c(OutputStream outputStream) {
        outputStream.write(e);
        outputStream.write(this.j == null ? c : this.j);
        outputStream.write(d);
    }

    private void d(OutputStream outputStream) {
        String c2 = c();
        if (c2 != null) {
            outputStream.write(d);
            outputStream.write(g);
            outputStream.write(d.a(c2));
            String d2 = d();
            if (d2 != null) {
                outputStream.write(h);
                outputStream.write(d.a(d2));
            }
        }
    }

    private void e(OutputStream outputStream) {
        String e2 = e();
        if (e2 != null) {
            outputStream.write(d);
            outputStream.write(i);
            outputStream.write(d.a(e2));
        }
    }

    private static void f(OutputStream outputStream) {
        outputStream.write(d);
        outputStream.write(d);
    }

    /* access modifiers changed from: protected */
    public abstract long a();

    /* access modifiers changed from: protected */
    public void a(OutputStream outputStream) {
        outputStream.write(f);
        outputStream.write(f172a);
        outputStream.write(d.a(b()));
        outputStream.write(f172a);
    }

    public abstract String b();

    /* access modifiers changed from: protected */
    public abstract void b(OutputStream outputStream);

    public abstract String c();

    public abstract String d();

    public abstract String e();

    public String toString() {
        return b();
    }
}
