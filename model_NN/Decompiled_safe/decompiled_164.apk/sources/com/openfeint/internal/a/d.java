package com.openfeint.internal.a;

final class d implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ x f177a;
    private final /* synthetic */ k b;
    private final /* synthetic */ Runnable c;

    d(x xVar, k kVar, Runnable runnable) {
        this.f177a = xVar;
        this.b = kVar;
        this.c = runnable;
    }

    public final void run() {
        if (this.b.n() == null && this.b.i().cancel(true)) {
            this.b.q();
            this.f177a.b.post(this.c);
        }
    }
}
