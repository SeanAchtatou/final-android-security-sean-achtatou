package com.openfeint.internal.a.a;

import java.io.OutputStream;

public final class g extends b {
    private byte[] b;
    private String c;

    public g(String str, String str2) {
        this(str, str2, (byte) 0);
    }

    private g(String str, String str2, byte b2) {
        super(str, "text/html", "UTF-8", "8bit");
        if (str2 == null) {
            throw new IllegalArgumentException("Value may not be null");
        } else if (str2.indexOf(0) != -1) {
            throw new IllegalArgumentException("NULs may not be present in string parts");
        } else {
            this.c = str2;
        }
    }

    private byte[] f() {
        if (this.b == null) {
            this.b = d.a(this.c, d());
        }
        return this.b;
    }

    /* access modifiers changed from: protected */
    public final long a() {
        return (long) f().length;
    }

    /* access modifiers changed from: protected */
    public final void b(OutputStream outputStream) {
        outputStream.write(f());
    }
}
