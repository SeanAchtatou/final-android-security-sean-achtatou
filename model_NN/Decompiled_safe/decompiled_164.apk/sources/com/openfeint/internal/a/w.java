package com.openfeint.internal.a;

import com.openfeint.internal.a.a.a;
import com.openfeint.internal.a.a.c;
import com.openfeint.internal.a.a.f;
import com.openfeint.internal.a.a.g;
import com.openfeint.internal.b.e;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;

public class w extends k {

    /* renamed from: a  reason: collision with root package name */
    private e f189a;
    private c b;
    private String c;
    private i d;

    public w(e eVar, c cVar, String str) {
        super(null);
        this.f189a = eVar;
        this.b = cVar;
        this.c = str;
    }

    public final String a() {
        return "POST";
    }

    public final void a(int i, byte[] bArr) {
        if (this.d != null) {
            i iVar = this.d;
            new String(bArr);
            iVar.a(i);
        }
    }

    public final void a(i iVar) {
        this.d = iVar;
    }

    public final String b() {
        return "";
    }

    /* access modifiers changed from: protected */
    public final HttpUriRequest c() {
        if (this.b == null) {
            return null;
        }
        HttpPost httpPost = new HttpPost(this.f189a.f197a);
        httpPost.setEntity(new com.openfeint.internal.a.a.e(new f[]{new g("AWSAccessKeyId", this.f189a.c), new g("acl", this.f189a.d), new g("key", this.f189a.b), new g("policy", this.f189a.e), new g("signature", this.f189a.f), new a("file", this.b, this.c)}));
        a((HttpUriRequest) httpPost);
        return httpPost;
    }

    public final boolean k() {
        return false;
    }

    public final String m() {
        return this.f189a.f197a;
    }
}
