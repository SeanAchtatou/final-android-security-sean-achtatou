package com.openfeint.internal.a;

import com.ElekaSoftware.OfficeRage.C0000R;
import com.openfeint.internal.b.d;
import com.openfeint.internal.w;
import org.apache.http.client.methods.HttpUriRequest;

public abstract class j extends k {
    public j() {
    }

    public j(g gVar) {
        super(gVar);
    }

    protected static d a(int i) {
        return new d("ServerError", String.format(w.a((int) C0000R.string.of_server_error_code_format), Integer.valueOf(i)));
    }

    /* access modifiers changed from: protected */
    public final HttpUriRequest c() {
        HttpUriRequest c = super.c();
        c.addHeader("Accept", "application/json");
        return c;
    }

    /* access modifiers changed from: protected */
    public final boolean d() {
        String e = e();
        return e != null && e.startsWith("application/json;");
    }
}
