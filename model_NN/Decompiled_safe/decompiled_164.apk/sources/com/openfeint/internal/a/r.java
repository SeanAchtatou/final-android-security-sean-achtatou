package com.openfeint.internal.a;

import java.util.Map;

public final class r extends j {

    /* renamed from: a  reason: collision with root package name */
    private String f186a;
    private String b;
    private i c;
    private long d = super.g();
    private int e = super.f();

    public r(String str, String str2, Map map) {
        g gVar = new g(map);
        gVar.a("format", "json");
        a(gVar);
        this.f186a = str2;
        this.b = str;
        this.c = null;
    }

    public final String a() {
        return this.f186a;
    }

    public final void a(int i, byte[] bArr) {
        try {
            if (!d()) {
                a(i).d();
            } else if (bArr != null) {
                new String(bArr);
            }
            if (this.c != null) {
                this.c.a(i);
            }
        } catch (IllegalStateException e2) {
            e2.printStackTrace();
        }
    }

    public final String b() {
        return this.b;
    }

    public final int f() {
        return this.e;
    }

    public final long g() {
        return this.d;
    }
}
