package com.openfeint.internal.a;

import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadPoolExecutor;

final class a implements RejectedExecutionHandler {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ x f170a;

    a(x xVar) {
        this.f170a = xVar;
    }

    public final void rejectedExecution(Runnable runnable, ThreadPoolExecutor threadPoolExecutor) {
        "Can't submit runnable " + runnable.toString();
    }
}
