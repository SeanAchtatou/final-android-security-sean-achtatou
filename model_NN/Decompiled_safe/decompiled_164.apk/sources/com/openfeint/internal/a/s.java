package com.openfeint.internal.a;

import com.ElekaSoftware.OfficeRage.C0000R;
import com.openfeint.internal.b.d;
import com.openfeint.internal.v;
import com.openfeint.internal.w;

public abstract class s extends j {
    public s() {
    }

    public s(g gVar) {
        super(gVar);
    }

    /* access modifiers changed from: protected */
    public void a(int i, Object obj) {
        if (200 > i || i >= 300 || (obj != null && (obj instanceof d))) {
            b(obj);
        } else {
            a(obj);
        }
    }

    public void a(int i, byte[] bArr) {
        if (bArr.length == 0 || ((bArr.length == 1 && bArr[0] == 32) || d())) {
            a(i, v.a(bArr));
        } else {
            a(i, a(i));
        }
    }

    public void a(Object obj) {
    }

    public void a(String str) {
    }

    /* access modifiers changed from: protected */
    public final void b(Object obj) {
        String a2 = w.a((int) C0000R.string.of_unknown_server_error);
        if (obj != null && (obj instanceof d)) {
            d dVar = (d) obj;
            a2 = dVar.b;
            if (dVar.c) {
                w.a((CharSequence) a2);
            }
        }
        a(a2);
    }
}
