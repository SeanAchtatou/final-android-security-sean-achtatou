package com.openfeint.internal.a;

import java.io.ByteArrayOutputStream;
import java.util.zip.DeflaterOutputStream;

public final class q {

    /* renamed from: a  reason: collision with root package name */
    private static String f185a = "Compression";
    private static final byte[] b = "OFZLHDR0".getBytes();
    private static /* synthetic */ int[] c;

    /* JADX WARNING: Removed duplicated region for block: B:14:0x003b A[Catch:{ IOException -> 0x00d5 }] */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x00aa A[Catch:{ IOException -> 0x00d5 }] */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x002b A[Catch:{ IOException -> 0x00d5 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static byte[] a(byte[] r9) {
        /*
            r8 = 1120403456(0x42c80000, float:100.0)
            int[] r1 = a()     // Catch:{ IOException -> 0x00d5 }
            com.openfeint.internal.w r0 = com.openfeint.internal.w.a()     // Catch:{ IOException -> 0x00d5 }
            java.util.Map r0 = r0.j()     // Catch:{ IOException -> 0x00d5 }
            java.lang.String r2 = "SettingCloudStorageCompressionStrategy"
            java.lang.Object r0 = r0.get(r2)     // Catch:{ IOException -> 0x00d5 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ IOException -> 0x00d5 }
            if (r0 == 0) goto L_0x0038
            java.lang.String r2 = "CloudStorageCompressionStrategyLegacyHeaderlessCompression"
            boolean r2 = r0.equals(r2)     // Catch:{ IOException -> 0x00d5 }
            if (r2 == 0) goto L_0x002d
            com.openfeint.internal.a.aa r0 = com.openfeint.internal.a.aa.LegacyHeaderless     // Catch:{ IOException -> 0x00d5 }
        L_0x0022:
            int r0 = r0.ordinal()     // Catch:{ IOException -> 0x00d5 }
            r0 = r1[r0]     // Catch:{ IOException -> 0x00d5 }
            switch(r0) {
                case 1: goto L_0x003b;
                case 2: goto L_0x002b;
                case 3: goto L_0x00aa;
                default: goto L_0x002b;
            }     // Catch:{ IOException -> 0x00d5 }
        L_0x002b:
            r0 = r9
        L_0x002c:
            return r0
        L_0x002d:
            java.lang.String r2 = "CloudStorageCompressionStrategyNoCompression"
            boolean r0 = r0.equals(r2)     // Catch:{ IOException -> 0x00d5 }
            if (r0 == 0) goto L_0x0038
            com.openfeint.internal.a.aa r0 = com.openfeint.internal.a.aa.Uncompressed     // Catch:{ IOException -> 0x00d5 }
            goto L_0x0022
        L_0x0038:
            com.openfeint.internal.a.aa r0 = com.openfeint.internal.a.aa.Default     // Catch:{ IOException -> 0x00d5 }
            goto L_0x0022
        L_0x003b:
            byte[] r0 = b(r9)     // Catch:{ IOException -> 0x00d5 }
            int r1 = r9.length     // Catch:{ IOException -> 0x00d5 }
            r2 = 4
            byte[] r2 = new byte[r2]     // Catch:{ IOException -> 0x00d5 }
            r3 = 0
            int r4 = r1 >> 0
            byte r4 = (byte) r4     // Catch:{ IOException -> 0x00d5 }
            r2[r3] = r4     // Catch:{ IOException -> 0x00d5 }
            r3 = 1
            int r4 = r1 >> 8
            byte r4 = (byte) r4     // Catch:{ IOException -> 0x00d5 }
            r2[r3] = r4     // Catch:{ IOException -> 0x00d5 }
            r3 = 2
            int r4 = r1 >> 16
            byte r4 = (byte) r4     // Catch:{ IOException -> 0x00d5 }
            r2[r3] = r4     // Catch:{ IOException -> 0x00d5 }
            r3 = 3
            int r1 = r1 >> 24
            byte r1 = (byte) r1     // Catch:{ IOException -> 0x00d5 }
            r2[r3] = r1     // Catch:{ IOException -> 0x00d5 }
            int r1 = r0.length     // Catch:{ IOException -> 0x00d5 }
            byte[] r3 = com.openfeint.internal.a.q.b     // Catch:{ IOException -> 0x00d5 }
            int r3 = r3.length     // Catch:{ IOException -> 0x00d5 }
            int r1 = r1 + r3
            int r3 = r2.length     // Catch:{ IOException -> 0x00d5 }
            int r1 = r1 + r3
            int r3 = r9.length     // Catch:{ IOException -> 0x00d5 }
            if (r1 >= r3) goto L_0x00a8
            byte[] r3 = new byte[r1]     // Catch:{ IOException -> 0x00d5 }
            byte[] r4 = com.openfeint.internal.a.q.b     // Catch:{ IOException -> 0x00d5 }
            r5 = 0
            r6 = 0
            byte[] r7 = com.openfeint.internal.a.q.b     // Catch:{ IOException -> 0x00d5 }
            int r7 = r7.length     // Catch:{ IOException -> 0x00d5 }
            java.lang.System.arraycopy(r4, r5, r3, r6, r7)     // Catch:{ IOException -> 0x00d5 }
            r4 = 0
            byte[] r5 = com.openfeint.internal.a.q.b     // Catch:{ IOException -> 0x00d5 }
            int r5 = r5.length     // Catch:{ IOException -> 0x00d5 }
            int r6 = r2.length     // Catch:{ IOException -> 0x00d5 }
            java.lang.System.arraycopy(r2, r4, r3, r5, r6)     // Catch:{ IOException -> 0x00d5 }
            r2 = 0
            byte[] r4 = com.openfeint.internal.a.q.b     // Catch:{ IOException -> 0x00d5 }
            int r4 = r4.length     // Catch:{ IOException -> 0x00d5 }
            int r4 = r4 + 4
            int r5 = r0.length     // Catch:{ IOException -> 0x00d5 }
            java.lang.System.arraycopy(r0, r2, r3, r4, r5)     // Catch:{ IOException -> 0x00d5 }
            java.lang.String r0 = "Using Default strategy: orig %d bytes, compressed %d bytes (%.2f%% of original size)"
            r2 = 3
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ IOException -> 0x00d5 }
            r4 = 0
            int r5 = r9.length     // Catch:{ IOException -> 0x00d5 }
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)     // Catch:{ IOException -> 0x00d5 }
            r2[r4] = r5     // Catch:{ IOException -> 0x00d5 }
            r4 = 1
            java.lang.Integer r5 = java.lang.Integer.valueOf(r1)     // Catch:{ IOException -> 0x00d5 }
            r2[r4] = r5     // Catch:{ IOException -> 0x00d5 }
            r4 = 2
            float r1 = (float) r1     // Catch:{ IOException -> 0x00d5 }
            int r5 = r9.length     // Catch:{ IOException -> 0x00d5 }
            float r5 = (float) r5     // Catch:{ IOException -> 0x00d5 }
            float r1 = r1 / r5
            float r1 = r1 * r8
            java.lang.Float r1 = java.lang.Float.valueOf(r1)     // Catch:{ IOException -> 0x00d5 }
            r2[r4] = r1     // Catch:{ IOException -> 0x00d5 }
            java.lang.String.format(r0, r2)     // Catch:{ IOException -> 0x00d5 }
            r0 = r3
            goto L_0x002c
        L_0x00a8:
            r0 = r9
            goto L_0x002c
        L_0x00aa:
            byte[] r0 = b(r9)     // Catch:{ IOException -> 0x00d5 }
            java.lang.String r1 = "Using Default strategy: orig %d bytes, compressed %d bytes (%.2f%% of original size)"
            r2 = 3
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ IOException -> 0x00d5 }
            r3 = 0
            int r4 = r9.length     // Catch:{ IOException -> 0x00d5 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ IOException -> 0x00d5 }
            r2[r3] = r4     // Catch:{ IOException -> 0x00d5 }
            r3 = 1
            int r4 = r0.length     // Catch:{ IOException -> 0x00d5 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ IOException -> 0x00d5 }
            r2[r3] = r4     // Catch:{ IOException -> 0x00d5 }
            r3 = 2
            int r4 = r0.length     // Catch:{ IOException -> 0x00d5 }
            float r4 = (float) r4     // Catch:{ IOException -> 0x00d5 }
            int r5 = r9.length     // Catch:{ IOException -> 0x00d5 }
            float r5 = (float) r5     // Catch:{ IOException -> 0x00d5 }
            float r4 = r4 / r5
            float r4 = r4 * r8
            java.lang.Float r4 = java.lang.Float.valueOf(r4)     // Catch:{ IOException -> 0x00d5 }
            r2[r3] = r4     // Catch:{ IOException -> 0x00d5 }
            java.lang.String.format(r1, r2)     // Catch:{ IOException -> 0x00d5 }
            goto L_0x002c
        L_0x00d5:
            r0 = move-exception
            r0 = 0
            goto L_0x002c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.openfeint.internal.a.q.a(byte[]):byte[]");
    }

    private static /* synthetic */ int[] a() {
        int[] iArr = c;
        if (iArr == null) {
            iArr = new int[aa.values().length];
            try {
                iArr[aa.Default.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[aa.LegacyHeaderless.ordinal()] = 3;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[aa.Uncompressed.ordinal()] = 2;
            } catch (NoSuchFieldError e3) {
            }
            c = iArr;
        }
        return iArr;
    }

    private static byte[] b(byte[] bArr) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        DeflaterOutputStream deflaterOutputStream = new DeflaterOutputStream(byteArrayOutputStream);
        deflaterOutputStream.write(bArr);
        deflaterOutputStream.close();
        return byteArrayOutputStream.toByteArray();
    }
}
