package com.openfeint.internal.a;

import a.a.a.n;
import com.ElekaSoftware.OfficeRage.C0000R;
import com.openfeint.internal.b.d;
import com.openfeint.internal.s;
import com.openfeint.internal.w;
import java.io.IOException;

public abstract class m extends k {
    public final String a() {
        return "GET";
    }

    public final void a(int i, byte[] bArr) {
        String a2;
        String a3 = w.a((int) C0000R.string.of_unknown_server_error);
        if (200 > i || i >= 300 || bArr == null) {
            if (404 == i) {
                a2 = w.a((int) C0000R.string.of_file_not_found);
            } else {
                try {
                    Object a4 = new s(new n((byte) 0).a(bArr)).a();
                    if (a4 == null || !(a4 instanceof d)) {
                        a2 = a3;
                    } else {
                        d dVar = (d) a4;
                        a2 = String.valueOf(dVar.f196a) + ": " + dVar.b;
                    }
                } catch (IOException e) {
                    a2 = w.a((int) C0000R.string.of_error_parsing_error_message);
                }
            }
            a(a2);
            return;
        }
        a(bArr);
    }

    public void a(String str) {
    }

    /* access modifiers changed from: protected */
    public abstract void a(byte[] bArr);
}
