package com.openfeint.internal.a;

final class c implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ x f176a;
    private final /* synthetic */ Runnable b;
    private final /* synthetic */ long c;
    private final /* synthetic */ k d;
    private final /* synthetic */ Runnable e;

    c(x xVar, Runnable runnable, long j, k kVar, Runnable runnable2) {
        this.f176a = xVar;
        this.b = runnable;
        this.c = j;
        this.d = kVar;
        this.e = runnable2;
    }

    public final void run() {
        this.f176a.b.postDelayed(this.b, this.c);
        this.d.a(this.f176a.f190a);
        this.d.a(this.f176a.d);
        this.f176a.b.removeCallbacks(this.b);
        this.f176a.b.post(this.e);
    }
}
