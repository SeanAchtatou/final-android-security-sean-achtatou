package com.openfeint.internal.a.a;

import java.io.InputStream;
import java.io.OutputStream;

public final class a extends b {
    private static final byte[] b = d.a("; filename=");
    private c c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public a(String str, c cVar, String str2) {
        super(str, str2 == null ? "application/octet-stream" : str2, "ISO-8859-1", "binary");
        if (cVar == null) {
            throw new IllegalArgumentException("Source may not be null");
        }
        this.c = cVar;
    }

    /* access modifiers changed from: protected */
    public final long a() {
        return this.c.a();
    }

    /* access modifiers changed from: protected */
    public final void a(OutputStream outputStream) {
        super.a(outputStream);
        String b2 = this.c.b();
        if (b2 != null) {
            outputStream.write(b);
            outputStream.write(f172a);
            outputStream.write(d.a(b2));
            outputStream.write(f172a);
        }
    }

    /* access modifiers changed from: protected */
    public final void b(OutputStream outputStream) {
        if (a() != 0) {
            byte[] bArr = new byte[4096];
            InputStream c2 = this.c.c();
            while (true) {
                try {
                    int read = c2.read(bArr);
                    if (read >= 0) {
                        outputStream.write(bArr, 0, read);
                    } else {
                        return;
                    }
                } finally {
                    c2.close();
                }
            }
        }
    }
}
