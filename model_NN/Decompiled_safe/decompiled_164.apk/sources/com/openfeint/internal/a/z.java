package com.openfeint.internal.a;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;

final class z implements ResponseHandler {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ k f192a;
    private final /* synthetic */ HttpContext b;

    z(k kVar, HttpContext httpContext) {
        this.f192a = kVar;
        this.b = httpContext;
    }

    public final Object handleResponse(HttpResponse httpResponse) {
        this.f192a.p = String.valueOf(((HttpHost) this.b.getAttribute("http.target_host")).toURI()) + ((HttpUriRequest) this.b.getAttribute("http.request")).getURI();
        HttpEntity entity = httpResponse.getEntity();
        this.f192a.f = new byte[0];
        this.f192a.j = httpResponse.getStatusLine().getStatusCode();
        if (entity != null) {
            Header contentEncoding = entity.getContentEncoding();
            if (contentEncoding != null) {
                this.f192a.h = contentEncoding.getValue();
            }
            Header contentType = entity.getContentType();
            if (contentType != null) {
                this.f192a.i = contentType.getValue();
            }
            this.f192a.f = EntityUtils.toByteArray(entity);
            if (entity.getContentLength() >= 0 && entity.getContentLength() != ((long) this.f192a.f.length)) {
                "Content-Length mismatch with content - " + this.f192a.e.getURI().toASCIIString();
                this.f192a.j = 0;
            }
        }
        this.f192a.s = httpResponse;
        return null;
    }
}
