package com.openfeint.internal;

import com.openfeint.api.a.bc;
import com.openfeint.internal.a.g;
import com.openfeint.internal.a.p;

final class k extends p {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ w f222a;
    private final /* synthetic */ boolean b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    k(w wVar, g gVar, boolean z) {
        super(gVar);
        this.f222a = wVar;
        this.b = z;
    }

    public final String a() {
        return "POST";
    }

    public final void a(int i, Object obj) {
        this.f222a.f = false;
        if (200 <= i && i < 300) {
            w.a(this.f222a, (bc) obj);
            if (this.f222a.r != null) {
                this.f222a.b.post(this.f222a.r);
            }
        } else if (this.b) {
            this.f222a.a(i, obj);
        }
        if (this.f222a.s != null) {
            for (Runnable post : this.f222a.s) {
                this.f222a.b.post(post);
            }
        }
        this.f222a.r = null;
        this.f222a.s = null;
    }

    public final String b() {
        return "/xp/sessions.json";
    }
}
