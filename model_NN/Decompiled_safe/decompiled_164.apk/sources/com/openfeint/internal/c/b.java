package com.openfeint.internal.c;

import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.ElekaSoftware.OfficeRage.C0000R;
import com.openfeint.api.c;
import com.openfeint.internal.w;

public final class b extends a {
    private b(String str, String str2, com.openfeint.api.b bVar, c cVar) {
        super(str, str2, bVar, cVar, null);
    }

    public static void a(String str, com.openfeint.api.b bVar, c cVar) {
        a(str, null, bVar, cVar);
    }

    public static void a(String str, String str2, com.openfeint.api.b bVar, c cVar) {
        new b(str, str2, bVar, cVar).e();
    }

    /* access modifiers changed from: protected */
    public final boolean c() {
        this.b = ((LayoutInflater) w.a().l().getSystemService("layout_inflater")).inflate((int) C0000R.layout.of_simple_notification, (ViewGroup) null);
        ((TextView) this.b.findViewById(C0000R.id.of_text)).setText(a());
        ImageView imageView = (ImageView) this.b.findViewById(C0000R.id.of_icon);
        if (this.f202a != null) {
            Drawable a2 = a(this.f202a);
            if (a2 == null) {
                new c(this, imageView).p();
                return false;
            }
            imageView.setImageDrawable(a2);
        } else {
            imageView.setVisibility(4);
        }
        return true;
    }
}
