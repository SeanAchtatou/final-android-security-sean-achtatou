package com.openfeint.internal.c;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.widget.ImageView;
import com.ElekaSoftware.OfficeRage.C0000R;
import com.openfeint.api.a.y;
import com.openfeint.internal.a.v;

final class d extends v {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ f f204a;
    private final /* synthetic */ y b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    d(f fVar, String str, y yVar) {
        super(str);
        this.f204a = fVar;
        this.b = yVar;
    }

    public final void a(Bitmap bitmap) {
        ((ImageView) this.f204a.b.findViewById(C0000R.id.of_achievement_icon)).setImageDrawable(new BitmapDrawable(bitmap));
        this.f204a.d();
    }

    public final void a(String str) {
        "Failed to load image " + this.b.d + ":" + str;
        this.f204a.d();
    }
}
