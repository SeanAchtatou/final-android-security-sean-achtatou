package com.openfeint.internal.c;

import android.content.Context;
import android.widget.Toast;
import com.openfeint.internal.w;

final class g implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ a f205a;

    g(a aVar) {
        this.f205a = aVar;
    }

    public final void run() {
        Context l = w.a().l();
        this.f205a.c = new Toast(l);
        this.f205a.c.setGravity(80, 0, 0);
        this.f205a.c.setDuration(1);
        this.f205a.c.setView(this.f205a.b);
        this.f205a.c.show();
    }
}
