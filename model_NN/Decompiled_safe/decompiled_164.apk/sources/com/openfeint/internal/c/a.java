package com.openfeint.internal.c;

import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.Toast;
import com.openfeint.api.b;
import com.openfeint.api.c;
import com.openfeint.api.d;
import com.openfeint.internal.w;
import java.util.HashMap;
import java.util.Map;

public abstract class a extends d {
    private static Map h = new HashMap();

    /* renamed from: a  reason: collision with root package name */
    protected String f202a;
    View b;
    Toast c;
    private String d;
    private b e;
    private c f;
    private Map g;

    protected a(String str, String str2, b bVar, c cVar, Map map) {
        this.d = str;
        this.f202a = str2;
        this.e = bVar;
        this.f = cVar;
        this.g = map;
    }

    static Drawable a(String str) {
        if (!h.containsKey(str)) {
            w a2 = w.a();
            int a3 = a2.a(str);
            if (a3 == 0) {
                h.put(str, null);
            } else {
                h.put(str, a2.l().getResources().getDrawable(a3));
            }
        }
        return (Drawable) h.get(str);
    }

    public final String a() {
        return this.d;
    }

    public final Map b() {
        return this.g;
    }

    /* access modifiers changed from: protected */
    public abstract boolean c();

    /* access modifiers changed from: protected */
    public final void d() {
        w.a().a(new g(this));
    }

    /* access modifiers changed from: protected */
    public final void e() {
        if (c()) {
            d();
        }
    }
}
