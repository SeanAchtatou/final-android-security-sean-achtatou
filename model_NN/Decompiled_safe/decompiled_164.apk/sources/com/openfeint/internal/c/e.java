package com.openfeint.internal.c;

import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.ElekaSoftware.OfficeRage.C0000R;
import com.openfeint.api.b;
import com.openfeint.api.c;
import com.openfeint.internal.w;
import java.util.HashMap;
import java.util.Map;

public final class e extends a {
    private e(String str, b bVar, c cVar, Map map) {
        super(str, null, bVar, cVar, map);
    }

    public static void a(String str, String str2, b bVar, c cVar) {
        HashMap hashMap = new HashMap();
        hashMap.put("extra", str2);
        new e(str, bVar, cVar, hashMap).e();
    }

    /* access modifiers changed from: protected */
    public final boolean c() {
        this.b = ((LayoutInflater) w.a().l().getSystemService("layout_inflater")).inflate((int) C0000R.layout.of_two_line_notification, (ViewGroup) null);
        ((TextView) this.b.findViewById(C0000R.id.of_text1)).setText(a());
        ((TextView) this.b.findViewById(C0000R.id.of_text2)).setText((String) b().get("extra"));
        ImageView imageView = (ImageView) this.b.findViewById(C0000R.id.of_icon);
        if (this.f202a != null) {
            Drawable a2 = a(this.f202a);
            if (a2 == null) {
                new h(this, imageView).p();
                return false;
            }
            imageView.setImageDrawable(a2);
        } else {
            imageView.setVisibility(4);
        }
        return true;
    }
}
