package com.openfeint.internal.c;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.widget.ImageView;
import com.openfeint.internal.a.u;

final class h extends u {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ e f206a;
    private final /* synthetic */ ImageView b;

    h(e eVar, ImageView imageView) {
        this.f206a = eVar;
        this.b = imageView;
    }

    public final void a(Bitmap bitmap) {
        this.b.setImageDrawable(new BitmapDrawable(bitmap));
        this.f206a.d();
    }

    public final void a(String str) {
        "Failed to load image " + this.f206a.f202a + ":" + str;
        this.b.setVisibility(4);
        this.f206a.d();
    }

    public final String b() {
        return this.f206a.f202a;
    }
}
