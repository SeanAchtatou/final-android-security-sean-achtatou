package com.openfeint.internal.c;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.widget.ImageView;
import com.openfeint.internal.a.u;

final class c extends u {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ b f203a;
    private final /* synthetic */ ImageView b;

    c(b bVar, ImageView imageView) {
        this.f203a = bVar;
        this.b = imageView;
    }

    public final void a(Bitmap bitmap) {
        this.b.setImageDrawable(new BitmapDrawable(bitmap));
        this.f203a.d();
    }

    public final void a(String str) {
        "Failed to load image " + this.f203a.f202a + ":" + str;
        this.b.setVisibility(4);
        this.f203a.d();
    }

    public final String b() {
        return this.f203a.f202a;
    }
}
