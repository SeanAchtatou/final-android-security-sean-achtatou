package com.openfeint.internal.c;

import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.ElekaSoftware.OfficeRage.C0000R;
import com.openfeint.api.a.y;
import com.openfeint.api.b;
import com.openfeint.api.c;
import com.openfeint.internal.w;
import java.util.HashMap;
import java.util.Map;

public final class f extends a {
    private f(Map map) {
        super(w.a((int) C0000R.string.of_achievement_unlocked), null, b.Achievement, c.Success, map);
    }

    public static void a(y yVar) {
        HashMap hashMap = new HashMap();
        hashMap.put("achievement", yVar);
        new f(hashMap).e();
    }

    /* access modifiers changed from: protected */
    public final boolean c() {
        y yVar = (y) b().get("achievement");
        this.b = ((LayoutInflater) w.a().l().getSystemService("layout_inflater")).inflate((int) C0000R.layout.of_achievement_notification, (ViewGroup) null);
        if (yVar.f) {
            this.b.findViewById(C0000R.id.of_achievement_progress_icon).setVisibility(4);
            if (yVar.c == 0) {
                this.b.findViewById(C0000R.id.of_achievement_score_icon).setVisibility(4);
                this.b.findViewById(C0000R.id.of_achievement_score).setVisibility(4);
            }
        } else {
            this.b.findViewById(C0000R.id.of_achievement_score_icon).setVisibility(4);
        }
        ((TextView) this.b.findViewById(C0000R.id.of_achievement_text)).setText((yVar.f165a == null || yVar.f165a.length() <= 0) ? w.a((int) C0000R.string.of_achievement_unlocked) : yVar.f165a);
        ((TextView) this.b.findViewById(C0000R.id.of_achievement_score)).setText(yVar.f ? Integer.toString(yVar.c) : String.format("%d%%", Integer.valueOf((int) yVar.g)));
        if (yVar.d != null) {
            Drawable a2 = a(yVar.d);
            if (a2 == null) {
                new d(this, yVar.d, yVar).p();
                return false;
            }
            ((ImageView) this.b.findViewById(C0000R.id.of_achievement_icon)).setImageDrawable(a2);
        }
        return true;
    }
}
