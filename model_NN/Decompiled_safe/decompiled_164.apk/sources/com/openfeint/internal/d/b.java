package com.openfeint.internal.d;

import com.openfeint.internal.a.g;
import com.openfeint.internal.a.k;
import java.util.ArrayList;
import java.util.Iterator;

final class b extends k {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ String f209a;
    private final /* synthetic */ f b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    b(g gVar, String str, f fVar) {
        super(gVar);
        this.f209a = str;
        this.b = fVar;
    }

    public final String a() {
        return "POST";
    }

    public final void a(int i, byte[] bArr) {
    }

    public final String b() {
        return this.f209a;
    }

    public final void b(int i, byte[] bArr) {
        boolean z;
        boolean z2;
        if (200 > i || i >= 300) {
            if (i != 0 && 500 > i) {
                e.f212a.a();
                f e = e.f212a;
                e.f213a.clear();
                e.b.clear();
                e.l();
            }
            e.c.set(false);
            return;
        }
        f e2 = e.f212a;
        f fVar = this.b;
        Iterator it = fVar.f213a.iterator();
        while (it.hasNext()) {
            c cVar = (c) it.next();
            c b2 = e2.b(cVar.f210a);
            if (b2 == null) {
                b2 = cVar.a();
                e2.f213a.add(b2);
            }
            b2.c = Math.max(b2.c, cVar.b);
        }
        ArrayList arrayList = e2.b;
        e2.b = new ArrayList();
        Iterator it2 = arrayList.iterator();
        while (it2.hasNext()) {
            d dVar = (d) it2.next();
            if (dVar.e != null) {
                e2.b.add(dVar);
            } else {
                Iterator it3 = fVar.b.iterator();
                while (true) {
                    if (it3.hasNext()) {
                        d dVar2 = (d) it3.next();
                        if (!e.a(dVar.f211a, dVar2.f211a) || dVar.b != dVar2.b || !e.a(dVar.c, dVar2.c) || !e.a(dVar.d, dVar2.d) || !e.a(dVar.e, dVar2.e) || !e.a(dVar.f, dVar2.f)) {
                            z2 = false;
                            continue;
                        } else {
                            z2 = true;
                            continue;
                        }
                        if (z2) {
                            z = true;
                            break;
                        }
                    } else {
                        z = false;
                        break;
                    }
                }
                if (!z) {
                    e2.b.add(dVar);
                }
            }
        }
        e.l();
        e.j();
    }
}
