package com.openfeint.internal.d;

import a.b.a.a.a.c;
import android.content.Context;
import com.ElekaSoftware.OfficeRage.C0000R;
import com.openfeint.api.a.bb;
import com.openfeint.api.a.j;
import com.openfeint.internal.b.g;
import com.openfeint.internal.c.b;
import com.openfeint.internal.v;
import com.openfeint.internal.w;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.concurrent.atomic.AtomicBoolean;

public final class e {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static f f212a = null;
    private static String b = null;
    /* access modifiers changed from: private */
    public static AtomicBoolean c = new AtomicBoolean(false);

    public static void a() {
        a((String) null);
    }

    public static void a(bb bbVar, j jVar) {
        d dVar;
        if (b != null) {
            d dVar2 = new d();
            dVar2.f211a = jVar.c();
            dVar2.b = bbVar.b;
            dVar2.c = bbVar.e;
            dVar2.d = bbVar.f;
            dVar2.f = h();
            if (bbVar.i != null) {
                Iterator it = f212a.b.iterator();
                while (true) {
                    if (it.hasNext()) {
                        dVar = (d) it.next();
                        if (dVar2.f211a.equals(dVar.f211a)) {
                            break;
                        }
                    } else {
                        dVar = null;
                        break;
                    }
                }
                if (dVar != null) {
                    if (jVar.d || ((jVar.c && dVar.b < bbVar.b) || (!jVar.c && dVar.b > bbVar.b))) {
                        d(dVar.e);
                        f212a.b.remove(dVar);
                    } else {
                        return;
                    }
                }
                String str = "unknown.blob";
                try {
                    MessageDigest instance = MessageDigest.getInstance("SHA1");
                    instance.update(bbVar.i);
                    str = String.format("%s.blob", new String(c.a(instance.digest())));
                } catch (NoSuchAlgorithmException e) {
                }
                try {
                    v.a(bbVar.i, e(str));
                    dVar2.e = str;
                } catch (IOException e2) {
                    return;
                }
            }
            f212a.b.add(dVar2);
            l();
            if (!i()) {
                b.a(w.a((int) C0000R.string.of_score_submitted_notification), "@drawable/of_icon_highscore_notification", com.openfeint.api.b.HighScore, com.openfeint.api.c.Success);
            }
        }
    }

    static final void a(d dVar) {
        f212a.b.remove(dVar);
        l();
        j();
    }

    public static void a(String str) {
        if (str == null || !str.equals(b)) {
            f a2 = f.a(f(str));
            if (i()) {
                f fVar = f212a;
                Iterator it = a2.f213a.iterator();
                while (it.hasNext()) {
                    c cVar = (c) it.next();
                    c b2 = fVar.b(cVar.f210a);
                    if (b2 == null) {
                        fVar.f213a.add(cVar.a());
                    } else {
                        if (b2.b < cVar.b) {
                            b2.b = cVar.b;
                            b2.d = cVar.d;
                        }
                        b2.c = Math.max(b2.c, cVar.c);
                    }
                }
                fVar.b.addAll(a2.b);
                d(f("0"));
            } else {
                f212a = a2;
            }
            b = str;
            c();
        }
    }

    public static void a(String str, float f) {
        if (b != null) {
            c b2 = f212a.b(str);
            if (b2 == null) {
                c cVar = new c();
                cVar.f210a = str;
                cVar.c = 0.0f;
                cVar.b = f;
                cVar.d = h();
                f212a.f213a.add(cVar);
                l();
            } else if (b2.b < f) {
                b2.b = f;
                b2.d = h();
                l();
            }
        }
    }

    static /* synthetic */ boolean a(String str, String str2) {
        return str == null ? str2 == null : str.equals(str2);
    }

    public static float b(String str) {
        c b2 = f212a.b(str);
        if (b2 == null) {
            return 0.0f;
        }
        return b2.b;
    }

    public static void b() {
        a("0");
    }

    public static void b(String str, float f) {
        if (b != null) {
            c b2 = f212a.b(str);
            if (b2 == null) {
                b2 = new c();
                b2.f210a = str;
                b2.b = f;
                f212a.f213a.add(b2);
            }
            b2.c = f;
            b2.d = h();
            l();
        }
    }

    public static void c() {
        if (b != null && !b.equals("0") && w.a().e()) {
            k();
        }
    }

    /* access modifiers changed from: private */
    public static void d(String str) {
        try {
            Context l = w.a().l();
            l.deleteFile(l.getFileStreamPath(str).getPath());
        } catch (Exception e) {
        }
    }

    private static String e(String str) {
        return w.a().l().getFileStreamPath(str).getPath();
    }

    private static String f(String str) {
        if (str == null) {
            return null;
        }
        String i = w.a().i();
        if (i == null) {
            return null;
        }
        return "of.offline." + str + "." + i;
    }

    private static String h() {
        return g.f198a.format(new Date());
    }

    private static boolean i() {
        return "0".equals(b);
    }

    /* access modifiers changed from: private */
    public static void j() {
        Iterator it = f212a.b.iterator();
        while (it.hasNext()) {
            d dVar = (d) it.next();
            if (dVar.e != null) {
                bb bbVar = new bb(dVar.b, (byte) 0);
                bbVar.f = dVar.d;
                bbVar.e = dVar.c;
                try {
                    bbVar.i = v.a(e(dVar.e));
                } catch (IOException e) {
                }
                if (bbVar.i == null) {
                    a(dVar);
                    return;
                } else {
                    bbVar.a(new j(dVar.f211a), dVar.f, new a(dVar));
                    return;
                }
            }
        }
        c.set(false);
    }

    private static synchronized void k() {
        synchronized (e.class) {
            if (c.compareAndSet(false, true)) {
                f fVar = f212a;
                f fVar2 = new f();
                Iterator it = fVar.b.iterator();
                while (it.hasNext()) {
                    d dVar = (d) it.next();
                    ArrayList arrayList = fVar2.b;
                    d dVar2 = new d();
                    dVar2.f211a = dVar.f211a;
                    dVar2.b = dVar.b;
                    dVar2.c = dVar.c;
                    dVar2.d = dVar.d;
                    dVar2.e = dVar.e;
                    dVar2.f = dVar.f;
                    arrayList.add(dVar2);
                }
                Iterator it2 = fVar.f213a.iterator();
                while (it2.hasNext()) {
                    fVar2.f213a.add(((c) it2.next()).a());
                }
                com.openfeint.internal.a.g gVar = new com.openfeint.internal.a.g();
                Iterator it3 = fVar2.f213a.iterator();
                int i = 0;
                while (it3.hasNext()) {
                    c cVar = (c) it3.next();
                    if (cVar.b != cVar.c) {
                        String.format("Updating achievement %s from known %f to %f completion", cVar.f210a, Float.valueOf(cVar.c), Float.valueOf(cVar.b));
                        gVar.a(String.format("achievements[%d][id]", Integer.valueOf(i)), cVar.f210a);
                        gVar.a(String.format("achievements[%d][percent_complete]", Integer.valueOf(i)), Float.toString(cVar.b));
                        gVar.a(String.format("achievements[%d][timestamp]", Integer.valueOf(i)), cVar.d);
                        i++;
                    }
                }
                Iterator it4 = fVar2.b.iterator();
                int i2 = 0;
                while (it4.hasNext()) {
                    d dVar3 = (d) it4.next();
                    if (dVar3.e == null) {
                        String.format("Posting score %d to leaderboard %s", Long.valueOf(dVar3.b), dVar3.f211a);
                        gVar.a(String.format("high_scores[%d][leaderboard_id]", Integer.valueOf(i2)), dVar3.f211a);
                        gVar.a(String.format("high_scores[%d][score]", Integer.valueOf(i2)), Long.toString(dVar3.b));
                        if (dVar3.c != null) {
                            gVar.a(String.format("high_scores[%d][display_text]", Integer.valueOf(i2)), dVar3.c);
                        }
                        if (dVar3.d != null) {
                            gVar.a(String.format("high_scores[%d][custom_data]", Integer.valueOf(i2)), dVar3.d);
                        }
                        gVar.a(String.format("high_scores[%d][timestamp]", Integer.valueOf(i2)), dVar3.f);
                        i2++;
                    }
                }
                if (i == 0 && i2 == 0) {
                    j();
                } else {
                    new b(gVar, "/xp/games/" + w.a().i() + "/offline_syncs", fVar2).p();
                }
            }
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00b0 A[SYNTHETIC, Splitter:B:37:0x00b0] */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x00b5 A[SYNTHETIC, Splitter:B:40:0x00b5] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void l() {
        /*
            r4 = 0
            java.lang.String r0 = com.openfeint.internal.d.e.b
            java.lang.String r0 = f(r0)
            if (r0 == 0) goto L_0x005a
            com.openfeint.internal.d.f r1 = com.openfeint.internal.d.e.f212a
            com.openfeint.internal.w r2 = com.openfeint.internal.w.a()     // Catch:{ Exception -> 0x00cb, all -> 0x00c3 }
            android.content.Context r2 = r2.l()     // Catch:{ Exception -> 0x00cb, all -> 0x00c3 }
            r3 = 0
            java.io.FileOutputStream r0 = r2.openFileOutput(r0, r3)     // Catch:{ Exception -> 0x00cb, all -> 0x00c3 }
            javax.crypto.CipherOutputStream r2 = com.openfeint.internal.o.a(r0)     // Catch:{ Exception -> 0x00cb, all -> 0x00c3 }
            java.io.ObjectOutputStream r3 = new java.io.ObjectOutputStream     // Catch:{ Exception -> 0x00cf, all -> 0x00c7 }
            r3.<init>(r2)     // Catch:{ Exception -> 0x00cf, all -> 0x00c7 }
            r0 = 0
            r3.writeInt(r0)     // Catch:{ Exception -> 0x0076, all -> 0x00ab }
            java.util.ArrayList r0 = r1.f213a     // Catch:{ Exception -> 0x0076, all -> 0x00ab }
            int r0 = r0.size()     // Catch:{ Exception -> 0x0076, all -> 0x00ab }
            r3.writeInt(r0)     // Catch:{ Exception -> 0x0076, all -> 0x00ab }
            java.util.ArrayList r0 = r1.f213a     // Catch:{ Exception -> 0x0076, all -> 0x00ab }
            java.util.Iterator r4 = r0.iterator()     // Catch:{ Exception -> 0x0076, all -> 0x00ab }
        L_0x0034:
            boolean r0 = r4.hasNext()     // Catch:{ Exception -> 0x0076, all -> 0x00ab }
            if (r0 != 0) goto L_0x005b
            java.util.ArrayList r0 = r1.b     // Catch:{ Exception -> 0x0076, all -> 0x00ab }
            int r0 = r0.size()     // Catch:{ Exception -> 0x0076, all -> 0x00ab }
            r3.writeInt(r0)     // Catch:{ Exception -> 0x0076, all -> 0x00ab }
            java.util.ArrayList r0 = r1.b     // Catch:{ Exception -> 0x0076, all -> 0x00ab }
            java.util.Iterator r1 = r0.iterator()     // Catch:{ Exception -> 0x0076, all -> 0x00ab }
        L_0x0049:
            boolean r0 = r1.hasNext()     // Catch:{ Exception -> 0x0076, all -> 0x00ab }
            if (r0 != 0) goto L_0x0086
            r3.close()     // Catch:{ Exception -> 0x0076, all -> 0x00ab }
            r3.close()     // Catch:{ IOException -> 0x00bf }
        L_0x0055:
            if (r2 == 0) goto L_0x005a
            r2.close()     // Catch:{ IOException -> 0x00c1 }
        L_0x005a:
            return
        L_0x005b:
            java.lang.Object r0 = r4.next()     // Catch:{ Exception -> 0x0076, all -> 0x00ab }
            com.openfeint.internal.d.c r0 = (com.openfeint.internal.d.c) r0     // Catch:{ Exception -> 0x0076, all -> 0x00ab }
            java.lang.String r5 = r0.f210a     // Catch:{ Exception -> 0x0076, all -> 0x00ab }
            r3.writeObject(r5)     // Catch:{ Exception -> 0x0076, all -> 0x00ab }
            float r5 = r0.b     // Catch:{ Exception -> 0x0076, all -> 0x00ab }
            r3.writeFloat(r5)     // Catch:{ Exception -> 0x0076, all -> 0x00ab }
            float r5 = r0.c     // Catch:{ Exception -> 0x0076, all -> 0x00ab }
            r3.writeFloat(r5)     // Catch:{ Exception -> 0x0076, all -> 0x00ab }
            java.lang.String r0 = r0.d     // Catch:{ Exception -> 0x0076, all -> 0x00ab }
            r3.writeObject(r0)     // Catch:{ Exception -> 0x0076, all -> 0x00ab }
            goto L_0x0034
        L_0x0076:
            r0 = move-exception
            r0 = r2
            r1 = r3
        L_0x0079:
            if (r1 == 0) goto L_0x007e
            r1.close()     // Catch:{ IOException -> 0x00b9 }
        L_0x007e:
            if (r0 == 0) goto L_0x005a
            r0.close()     // Catch:{ IOException -> 0x0084 }
            goto L_0x005a
        L_0x0084:
            r0 = move-exception
            goto L_0x005a
        L_0x0086:
            java.lang.Object r0 = r1.next()     // Catch:{ Exception -> 0x0076, all -> 0x00ab }
            com.openfeint.internal.d.d r0 = (com.openfeint.internal.d.d) r0     // Catch:{ Exception -> 0x0076, all -> 0x00ab }
            java.lang.String r4 = r0.f211a     // Catch:{ Exception -> 0x0076, all -> 0x00ab }
            r3.writeObject(r4)     // Catch:{ Exception -> 0x0076, all -> 0x00ab }
            long r4 = r0.b     // Catch:{ Exception -> 0x0076, all -> 0x00ab }
            r3.writeLong(r4)     // Catch:{ Exception -> 0x0076, all -> 0x00ab }
            java.lang.String r4 = r0.c     // Catch:{ Exception -> 0x0076, all -> 0x00ab }
            r3.writeObject(r4)     // Catch:{ Exception -> 0x0076, all -> 0x00ab }
            java.lang.String r4 = r0.d     // Catch:{ Exception -> 0x0076, all -> 0x00ab }
            r3.writeObject(r4)     // Catch:{ Exception -> 0x0076, all -> 0x00ab }
            java.lang.String r4 = r0.e     // Catch:{ Exception -> 0x0076, all -> 0x00ab }
            r3.writeObject(r4)     // Catch:{ Exception -> 0x0076, all -> 0x00ab }
            java.lang.String r0 = r0.f     // Catch:{ Exception -> 0x0076, all -> 0x00ab }
            r3.writeObject(r0)     // Catch:{ Exception -> 0x0076, all -> 0x00ab }
            goto L_0x0049
        L_0x00ab:
            r0 = move-exception
            r1 = r2
            r2 = r3
        L_0x00ae:
            if (r2 == 0) goto L_0x00b3
            r2.close()     // Catch:{ IOException -> 0x00bb }
        L_0x00b3:
            if (r1 == 0) goto L_0x00b8
            r1.close()     // Catch:{ IOException -> 0x00bd }
        L_0x00b8:
            throw r0
        L_0x00b9:
            r1 = move-exception
            goto L_0x007e
        L_0x00bb:
            r2 = move-exception
            goto L_0x00b3
        L_0x00bd:
            r1 = move-exception
            goto L_0x00b8
        L_0x00bf:
            r0 = move-exception
            goto L_0x0055
        L_0x00c1:
            r0 = move-exception
            goto L_0x005a
        L_0x00c3:
            r0 = move-exception
            r1 = r4
            r2 = r4
            goto L_0x00ae
        L_0x00c7:
            r0 = move-exception
            r1 = r2
            r2 = r4
            goto L_0x00ae
        L_0x00cb:
            r0 = move-exception
            r0 = r4
            r1 = r4
            goto L_0x0079
        L_0x00cf:
            r0 = move-exception
            r0 = r2
            r1 = r4
            goto L_0x0079
        */
        throw new UnsupportedOperationException("Method not decompiled: com.openfeint.internal.d.e.l():void");
    }
}
