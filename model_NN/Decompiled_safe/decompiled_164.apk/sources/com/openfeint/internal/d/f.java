package com.openfeint.internal.d;

import java.util.ArrayList;
import java.util.Iterator;

public final class f {

    /* renamed from: a  reason: collision with root package name */
    public ArrayList f213a = new ArrayList();
    public ArrayList b = new ArrayList();

    /* JADX WARNING: Removed duplicated region for block: B:14:0x005a A[SYNTHETIC, Splitter:B:14:0x005a] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00d7 A[SYNTHETIC, Splitter:B:31:0x00d7] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.openfeint.internal.d.f a(java.lang.String r8) {
        /*
            com.openfeint.internal.d.f r0 = new com.openfeint.internal.d.f
            r0.<init>()
            r1 = 0
            if (r8 == 0) goto L_0x005d
            com.openfeint.internal.w r2 = com.openfeint.internal.w.a()     // Catch:{ Exception -> 0x00e5, all -> 0x00d4 }
            android.content.Context r2 = r2.l()     // Catch:{ Exception -> 0x00e5, all -> 0x00d4 }
            java.io.FileInputStream r2 = r2.openFileInput(r8)     // Catch:{ Exception -> 0x00e5, all -> 0x00d4 }
            javax.crypto.CipherInputStream r2 = com.openfeint.internal.o.a(r2)     // Catch:{ Exception -> 0x00e5, all -> 0x00d4 }
            java.io.ObjectInputStream r3 = new java.io.ObjectInputStream     // Catch:{ Exception -> 0x00e5, all -> 0x00d4 }
            r3.<init>(r2)     // Catch:{ Exception -> 0x00e5, all -> 0x00d4 }
            int r1 = r3.readInt()     // Catch:{ Exception -> 0x003a, all -> 0x00df }
            switch(r1) {
                case 0: goto L_0x005e;
                default: goto L_0x0024;
            }     // Catch:{ Exception -> 0x003a, all -> 0x00df }
        L_0x0024:
            java.lang.Exception r2 = new java.lang.Exception     // Catch:{ Exception -> 0x003a, all -> 0x00df }
            java.lang.String r4 = "Unrecognized stream version %d"
            r5 = 1
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ Exception -> 0x003a, all -> 0x00df }
            r6 = 0
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)     // Catch:{ Exception -> 0x003a, all -> 0x00df }
            r5[r6] = r1     // Catch:{ Exception -> 0x003a, all -> 0x00df }
            java.lang.String r1 = java.lang.String.format(r4, r5)     // Catch:{ Exception -> 0x003a, all -> 0x00df }
            r2.<init>(r1)     // Catch:{ Exception -> 0x003a, all -> 0x00df }
            throw r2     // Catch:{ Exception -> 0x003a, all -> 0x00df }
        L_0x003a:
            r1 = move-exception
            r2 = r3
        L_0x003c:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x00e2 }
            java.lang.String r4 = "Couldn't load offline achievements - "
            r3.<init>(r4)     // Catch:{ all -> 0x00e2 }
            java.lang.String r1 = r1.getMessage()     // Catch:{ all -> 0x00e2 }
            java.lang.StringBuilder r1 = r3.append(r1)     // Catch:{ all -> 0x00e2 }
            r1.toString()     // Catch:{ all -> 0x00e2 }
            java.util.ArrayList r1 = r0.f213a     // Catch:{ all -> 0x00e2 }
            r1.clear()     // Catch:{ all -> 0x00e2 }
            java.util.ArrayList r1 = r0.b     // Catch:{ all -> 0x00e2 }
            r1.clear()     // Catch:{ all -> 0x00e2 }
            if (r2 == 0) goto L_0x005d
            r2.close()     // Catch:{ IOException -> 0x00db }
        L_0x005d:
            return r0
        L_0x005e:
            int r1 = r3.readInt()     // Catch:{ Exception -> 0x003a, all -> 0x00df }
        L_0x0062:
            if (r1 > 0) goto L_0x0070
            int r1 = r3.readInt()     // Catch:{ Exception -> 0x003a, all -> 0x00df }
        L_0x0068:
            if (r1 > 0) goto L_0x0099
            r3.close()     // Catch:{ IOException -> 0x006e }
            goto L_0x005d
        L_0x006e:
            r1 = move-exception
            goto L_0x005d
        L_0x0070:
            com.openfeint.internal.d.c r2 = new com.openfeint.internal.d.c     // Catch:{ Exception -> 0x003a, all -> 0x00df }
            r2.<init>()     // Catch:{ Exception -> 0x003a, all -> 0x00df }
            java.lang.Object r8 = r3.readObject()     // Catch:{ Exception -> 0x003a, all -> 0x00df }
            java.lang.String r8 = (java.lang.String) r8     // Catch:{ Exception -> 0x003a, all -> 0x00df }
            r2.f210a = r8     // Catch:{ Exception -> 0x003a, all -> 0x00df }
            float r4 = r3.readFloat()     // Catch:{ Exception -> 0x003a, all -> 0x00df }
            r2.b = r4     // Catch:{ Exception -> 0x003a, all -> 0x00df }
            float r4 = r3.readFloat()     // Catch:{ Exception -> 0x003a, all -> 0x00df }
            r2.c = r4     // Catch:{ Exception -> 0x003a, all -> 0x00df }
            java.lang.Object r8 = r3.readObject()     // Catch:{ Exception -> 0x003a, all -> 0x00df }
            java.lang.String r8 = (java.lang.String) r8     // Catch:{ Exception -> 0x003a, all -> 0x00df }
            r2.d = r8     // Catch:{ Exception -> 0x003a, all -> 0x00df }
            java.util.ArrayList r4 = r0.f213a     // Catch:{ Exception -> 0x003a, all -> 0x00df }
            r4.add(r2)     // Catch:{ Exception -> 0x003a, all -> 0x00df }
            int r1 = r1 + -1
            goto L_0x0062
        L_0x0099:
            com.openfeint.internal.d.d r2 = new com.openfeint.internal.d.d     // Catch:{ Exception -> 0x003a, all -> 0x00df }
            r2.<init>()     // Catch:{ Exception -> 0x003a, all -> 0x00df }
            java.lang.Object r8 = r3.readObject()     // Catch:{ Exception -> 0x003a, all -> 0x00df }
            java.lang.String r8 = (java.lang.String) r8     // Catch:{ Exception -> 0x003a, all -> 0x00df }
            r2.f211a = r8     // Catch:{ Exception -> 0x003a, all -> 0x00df }
            long r4 = r3.readLong()     // Catch:{ Exception -> 0x003a, all -> 0x00df }
            r2.b = r4     // Catch:{ Exception -> 0x003a, all -> 0x00df }
            java.lang.Object r8 = r3.readObject()     // Catch:{ Exception -> 0x003a, all -> 0x00df }
            java.lang.String r8 = (java.lang.String) r8     // Catch:{ Exception -> 0x003a, all -> 0x00df }
            r2.c = r8     // Catch:{ Exception -> 0x003a, all -> 0x00df }
            java.lang.Object r8 = r3.readObject()     // Catch:{ Exception -> 0x003a, all -> 0x00df }
            java.lang.String r8 = (java.lang.String) r8     // Catch:{ Exception -> 0x003a, all -> 0x00df }
            r2.d = r8     // Catch:{ Exception -> 0x003a, all -> 0x00df }
            java.lang.Object r8 = r3.readObject()     // Catch:{ Exception -> 0x003a, all -> 0x00df }
            java.lang.String r8 = (java.lang.String) r8     // Catch:{ Exception -> 0x003a, all -> 0x00df }
            r2.e = r8     // Catch:{ Exception -> 0x003a, all -> 0x00df }
            java.lang.Object r8 = r3.readObject()     // Catch:{ Exception -> 0x003a, all -> 0x00df }
            java.lang.String r8 = (java.lang.String) r8     // Catch:{ Exception -> 0x003a, all -> 0x00df }
            r2.f = r8     // Catch:{ Exception -> 0x003a, all -> 0x00df }
            java.util.ArrayList r4 = r0.b     // Catch:{ Exception -> 0x003a, all -> 0x00df }
            r4.add(r2)     // Catch:{ Exception -> 0x003a, all -> 0x00df }
            int r1 = r1 + -1
            goto L_0x0068
        L_0x00d4:
            r0 = move-exception
        L_0x00d5:
            if (r1 == 0) goto L_0x00da
            r1.close()     // Catch:{ IOException -> 0x00dd }
        L_0x00da:
            throw r0
        L_0x00db:
            r1 = move-exception
            goto L_0x005d
        L_0x00dd:
            r1 = move-exception
            goto L_0x00da
        L_0x00df:
            r0 = move-exception
            r1 = r3
            goto L_0x00d5
        L_0x00e2:
            r0 = move-exception
            r1 = r2
            goto L_0x00d5
        L_0x00e5:
            r2 = move-exception
            r7 = r2
            r2 = r1
            r1 = r7
            goto L_0x003c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.openfeint.internal.d.f.a(java.lang.String):com.openfeint.internal.d.f");
    }

    public final void a() {
        Iterator it = this.b.iterator();
        while (it.hasNext()) {
            e.d(((d) it.next()).e);
        }
    }

    public final c b(String str) {
        Iterator it = this.f213a.iterator();
        while (it.hasNext()) {
            c cVar = (c) it.next();
            if (cVar.f210a.equals(str)) {
                return cVar;
            }
        }
        return null;
    }
}
