package com.openfeint.internal;

import android.content.Intent;
import com.openfeint.internal.ui.IntroFlow;

final class q implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ w f226a;
    private final /* synthetic */ boolean b;

    q(w wVar, boolean z) {
        this.f226a = wVar;
        this.b = z;
    }

    public final void run() {
        Intent intent = new Intent(this.f226a.l(), IntroFlow.class);
        if (this.f226a.j && this.b) {
            intent.putExtra("content_name", "index?preapproved=true&spotlight=true");
        } else if (this.b) {
            intent.putExtra("content_name", "index?spotlight=true");
        } else if (this.f226a.j) {
            intent.putExtra("content_name", "index?preapproved=true");
        }
        intent.addFlags(268435456);
        this.f226a.l().startActivity(intent);
    }
}
