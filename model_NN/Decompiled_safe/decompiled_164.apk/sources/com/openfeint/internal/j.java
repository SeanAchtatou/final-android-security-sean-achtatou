package com.openfeint.internal;

import android.content.Intent;

final class j implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ w f221a;
    private final /* synthetic */ Intent b;

    j(w wVar, Intent intent) {
        this.f221a = wVar;
        this.b = intent;
    }

    public final void run() {
        this.b.addFlags(268435456);
        this.f221a.l().startActivity(this.b);
    }
}
