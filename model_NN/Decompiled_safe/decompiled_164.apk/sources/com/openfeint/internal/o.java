package com.openfeint.internal;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.SecureRandom;
import java.util.Arrays;
import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;

public final class o {

    /* renamed from: a  reason: collision with root package name */
    private static SecretKey f225a;

    public static CipherInputStream a(InputStream inputStream) {
        try {
            byte[] bArr = new byte[10];
            if (inputStream.read(bArr) != 10) {
                throw new Exception("Couldn't read entire salt");
            }
            PBEParameterSpec pBEParameterSpec = new PBEParameterSpec(bArr, 10);
            Cipher instance = Cipher.getInstance("PBEWithSHA256And256BitAES-CBC-BC");
            instance.init(2, f225a, pBEParameterSpec);
            return new CipherInputStream(inputStream, instance);
        } catch (Exception e) {
            e.getMessage();
            return null;
        }
    }

    public static CipherOutputStream a(OutputStream outputStream) {
        try {
            byte[] bArr = new byte[10];
            new SecureRandom().nextBytes(bArr);
            outputStream.write(bArr);
            PBEParameterSpec pBEParameterSpec = new PBEParameterSpec(bArr, 10);
            Cipher instance = Cipher.getInstance("PBEWithSHA256And256BitAES-CBC-BC");
            instance.init(1, f225a, pBEParameterSpec);
            return new CipherOutputStream(outputStream, instance);
        } catch (Exception e) {
            e.getMessage();
            return null;
        }
    }

    public static boolean a() {
        return f225a != null;
    }

    public static boolean a(String str) {
        try {
            f225a = SecretKeyFactory.getInstance("PBEWithSHA256And256BitAES-CBC-BC").generateSecret(new PBEKeySpec(str.toCharArray()));
            byte[] bytes = "PBEWithSHA256And256BitAES-CBC-BC".getBytes();
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            CipherOutputStream a2 = a(byteArrayOutputStream);
            a2.write(bytes);
            a2.close();
            byte[] byteArray = byteArrayOutputStream.toByteArray();
            if (byteArray.length == 0) {
                throw new Exception();
            } else if (Arrays.equals(v.a(a(new ByteArrayInputStream(byteArray))), bytes)) {
                return true;
            } else {
                throw new Exception();
            }
        } catch (Exception e) {
            f225a = null;
            return false;
        }
    }
}
