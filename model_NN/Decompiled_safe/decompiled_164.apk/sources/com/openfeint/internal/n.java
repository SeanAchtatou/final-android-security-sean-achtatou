package com.openfeint.internal;

public final class n {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ u f224a;

    public n(u uVar) {
        this.f224a = uVar;
    }

    public final String a(String str, String str2) {
        String str3 = (String) this.f224a.f229a.get(str);
        return str3 != null ? str3 : str2;
    }

    public final void a() {
        this.f224a.c.readLock().unlock();
    }
}
