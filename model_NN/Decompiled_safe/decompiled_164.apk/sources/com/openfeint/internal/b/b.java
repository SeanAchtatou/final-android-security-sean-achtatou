package com.openfeint.internal.b;

import a.a.a.f;
import a.a.a.h;
import a.a.a.m;

public abstract class b extends x {
    public abstract String a(y yVar);

    public final void a(y yVar, f fVar, String str) {
        String a2 = a(yVar);
        if (a2 != null) {
            fVar.a(str);
            fVar.b(a2);
        }
    }

    public final void a(y yVar, h hVar) {
        if (hVar.q() == m.VALUE_NULL) {
            a(yVar, (String) null);
        } else {
            a(yVar, hVar.m());
        }
    }

    public final void a(y yVar, y yVar2) {
        a(yVar, a(yVar2));
    }

    public abstract void a(y yVar, String str);
}
