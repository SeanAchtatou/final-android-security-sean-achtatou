package com.openfeint.internal.b;

import java.util.HashMap;

public abstract class m {

    /* renamed from: a  reason: collision with root package name */
    public Class f199a;
    public HashMap b = new HashMap();
    public String c;

    public m(Class cls, String str) {
        this.f199a = cls;
        this.c = str;
        a(cls);
    }

    private void a(Class cls) {
        if (cls != y.class) {
            Class superclass = cls.getSuperclass();
            a(superclass);
            m a2 = y.a(superclass);
            for (String str : a2.b.keySet()) {
                this.b.put(str, (z) a2.b.get(str));
            }
        }
    }

    public abstract y a();
}
