package com.openfeint.internal.b;

import a.a.a.f;
import a.a.a.n;
import com.openfeint.api.a.bb;
import com.openfeint.api.a.bc;
import com.openfeint.api.a.i;
import com.openfeint.api.a.j;
import com.openfeint.api.a.k;
import java.io.IOException;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class y {

    /* renamed from: a  reason: collision with root package name */
    private static HashMap f200a = new HashMap();
    private static HashMap b = new HashMap();
    String l;

    static {
        a(a());
        a(d.a());
        a(i.a());
        a(e.a());
        a(com.openfeint.api.a.y.a());
        a(j.a());
        a(bb.a());
        a(bc.a());
        a(k.a());
    }

    public static m a() {
        t tVar = new t(y.class);
        tVar.b.put("id", new u());
        return tVar;
    }

    public static m a(Class cls) {
        return (m) f200a.get(cls);
    }

    private void a(f fVar) {
        HashMap a2;
        m a3 = a(getClass());
        fVar.c();
        fVar.a(a3.c);
        fVar.c();
        for (Map.Entry entry : a3.b.entrySet()) {
            z zVar = (z) entry.getValue();
            if (zVar instanceof x) {
                ((x) zVar).a(this, fVar, (String) entry.getKey());
            } else if (zVar instanceof c) {
                c cVar = (c) zVar;
                List<y> a4 = cVar.a(this);
                if (a4 != null) {
                    fVar.a((String) entry.getKey());
                    m a5 = a(cVar.a());
                    fVar.c();
                    fVar.a(String.valueOf(a5.c) + "s");
                    fVar.a();
                    for (y a6 : a4) {
                        a6.a(fVar);
                    }
                    fVar.b();
                    fVar.d();
                }
            } else if (zVar instanceof ab) {
                y a7 = ((ab) zVar).a(this);
                if (a7 != null) {
                    fVar.a((String) entry.getKey());
                    a7.a(fVar);
                }
            } else if ((zVar instanceof n) && (a2 = ((n) zVar).a()) != null && a2.size() > 0) {
                fVar.a((String) entry.getKey());
                fVar.c();
                for (Map.Entry entry2 : a2.entrySet()) {
                    fVar.a((String) entry2.getKey());
                    fVar.a(((Integer) entry2.getValue()).intValue());
                }
                fVar.d();
            }
        }
        fVar.d();
        fVar.d();
    }

    private static void a(m mVar) {
        f200a.put(mVar.f199a, mVar);
        if (mVar.c != null) {
            b.put(mVar.c, mVar);
        }
    }

    public static m b(String str) {
        return (m) b.get(str);
    }

    private final void c(y yVar) {
        for (Map.Entry value : a(yVar.getClass()).b.entrySet()) {
            z zVar = (z) value.getValue();
            if (zVar instanceof x) {
                ((x) zVar).a(this, yVar);
            } else if (zVar instanceof c) {
                ((c) zVar).a(this, ((c) zVar).a(yVar));
            } else if (zVar instanceof ab) {
                ((ab) zVar).a(this, ((ab) zVar).a(yVar));
            }
        }
    }

    public final void a(y yVar) {
        if (yVar.getClass() != getClass()) {
            throw new UnsupportedOperationException("You can only shallowCopy the same type of resource");
        }
        c(yVar);
    }

    public final void a(String str) {
        this.l = str;
    }

    public final void b(y yVar) {
        Class<?> cls = getClass();
        Class<?> cls2 = yVar.getClass();
        if (cls2 != y.class) {
            while (cls != cls2 && cls != y.class) {
                cls = cls.getSuperclass();
            }
            if (cls == y.class) {
                throw new UnsupportedOperationException(String.valueOf(cls2.getName()) + " is not a superclass of " + getClass().getName());
            }
        }
        c(yVar);
    }

    public final String c() {
        return this.l;
    }

    public final String d() {
        StringWriter stringWriter = new StringWriter();
        try {
            f a2 = new n((byte) 0).a(stringWriter);
            a(a2);
            a2.close();
            return stringWriter.toString();
        } catch (IOException e) {
            e.getMessage();
            return null;
        }
    }
}
