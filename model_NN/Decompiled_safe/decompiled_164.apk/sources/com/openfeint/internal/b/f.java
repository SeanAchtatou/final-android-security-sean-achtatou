package com.openfeint.internal.b;

import a.a.a.h;
import a.a.a.m;

public abstract class f extends x {
    public final void a(y yVar, a.a.a.f fVar, String str) {
        fVar.a(str);
        fVar.a(a(yVar));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.openfeint.internal.b.f.a(com.openfeint.internal.b.y, boolean):void
     arg types: [com.openfeint.internal.b.y, int]
     candidates:
      com.openfeint.internal.b.f.a(com.openfeint.internal.b.y, a.a.a.h):void
      com.openfeint.internal.b.f.a(com.openfeint.internal.b.y, com.openfeint.internal.b.y):void
      com.openfeint.internal.b.x.a(com.openfeint.internal.b.y, a.a.a.h):void
      com.openfeint.internal.b.x.a(com.openfeint.internal.b.y, com.openfeint.internal.b.y):void
      com.openfeint.internal.b.f.a(com.openfeint.internal.b.y, boolean):void */
    public final void a(y yVar, h hVar) {
        if (hVar.q() == m.VALUE_TRUE || hVar.m().equalsIgnoreCase("true") || hVar.m().equalsIgnoreCase("1") || hVar.m().equalsIgnoreCase("YES")) {
            a(yVar, true);
        } else {
            a(yVar, false);
        }
    }

    public final void a(y yVar, y yVar2) {
        a(yVar, a(yVar2));
    }

    public abstract void a(y yVar, boolean z);

    public abstract boolean a(y yVar);
}
