package com.openfeint.internal.b;

import a.a.a.f;
import a.a.a.h;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public abstract class g extends x {

    /* renamed from: a  reason: collision with root package name */
    public static DateFormat f198a;

    static {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        f198a = simpleDateFormat;
    }

    public abstract Date a(y yVar);

    public final void a(y yVar, f fVar, String str) {
        Date a2 = a(yVar);
        if (a2 != null) {
            fVar.a(str);
            fVar.b(f198a.format(a2));
        }
    }

    public final void a(y yVar, h hVar) {
        String m = hVar.m();
        if (m.equals("null")) {
            a(yVar, (Date) null);
            return;
        }
        try {
            a(yVar, f198a.parse(m));
        } catch (ParseException e) {
            a(yVar, (Date) null);
        }
    }

    public final void a(y yVar, y yVar2) {
        a(yVar, a(yVar2));
    }

    public abstract void a(y yVar, Date date);
}
