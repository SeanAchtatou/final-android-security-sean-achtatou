package com.openfeint.internal.b;

import a.a.a.f;

public abstract class h extends x {
    public abstract int a(y yVar);

    public abstract void a(y yVar, int i);

    public final void a(y yVar, f fVar, String str) {
        fVar.a(str);
        fVar.a(a(yVar));
    }

    public final void a(y yVar, a.a.a.h hVar) {
        int i;
        try {
            i = hVar.d();
        } catch (Exception e) {
            i = 0;
        }
        a(yVar, i);
    }

    public final void a(y yVar, y yVar2) {
        a(yVar, a(yVar2));
    }
}
