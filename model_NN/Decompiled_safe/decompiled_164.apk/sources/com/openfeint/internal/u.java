package com.openfeint.internal;

import android.content.Context;
import java.util.HashMap;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public final class u {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public HashMap f229a = new HashMap();
    private Context b;
    /* access modifiers changed from: private */
    public ReentrantReadWriteLock c = new ReentrantReadWriteLock();

    public u(Context context) {
        this.b = context;
        d();
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x0021 A[SYNTHETIC, Splitter:B:17:0x0021] */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0026 A[Catch:{ IOException -> 0x002c }] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0033 A[SYNTHETIC, Splitter:B:26:0x0033] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0039 A[Catch:{ IOException -> 0x0037 }] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0044 A[SYNTHETIC, Splitter:B:35:0x0044] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x004a A[Catch:{ IOException -> 0x0048 }] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x0055 A[SYNTHETIC, Splitter:B:44:0x0055] */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x005b A[Catch:{ IOException -> 0x0059 }] */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x0066 A[SYNTHETIC, Splitter:B:53:0x0066] */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x006a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.util.HashMap a(java.io.File r5) {
        /*
            r3 = 0
            java.io.FileInputStream r0 = new java.io.FileInputStream     // Catch:{ FileNotFoundException -> 0x001c, StreamCorruptedException -> 0x002e, IOException -> 0x003f, ClassNotFoundException -> 0x0050, all -> 0x0061 }
            r0.<init>(r5)     // Catch:{ FileNotFoundException -> 0x001c, StreamCorruptedException -> 0x002e, IOException -> 0x003f, ClassNotFoundException -> 0x0050, all -> 0x0061 }
            java.io.ObjectInputStream r1 = new java.io.ObjectInputStream     // Catch:{ FileNotFoundException -> 0x009f, StreamCorruptedException -> 0x0096, IOException -> 0x008d, ClassNotFoundException -> 0x0084, all -> 0x007a }
            r1.<init>(r0)     // Catch:{ FileNotFoundException -> 0x009f, StreamCorruptedException -> 0x0096, IOException -> 0x008d, ClassNotFoundException -> 0x0084, all -> 0x007a }
            java.lang.Object r5 = r1.readObject()     // Catch:{ FileNotFoundException -> 0x00a4, StreamCorruptedException -> 0x009a, IOException -> 0x0091, ClassNotFoundException -> 0x0088, all -> 0x007f }
            if (r5 == 0) goto L_0x0072
            boolean r2 = r5 instanceof java.util.HashMap     // Catch:{ FileNotFoundException -> 0x00a4, StreamCorruptedException -> 0x009a, IOException -> 0x0091, ClassNotFoundException -> 0x0088, all -> 0x007f }
            if (r2 == 0) goto L_0x0072
            java.util.HashMap r5 = (java.util.HashMap) r5     // Catch:{ FileNotFoundException -> 0x00a4, StreamCorruptedException -> 0x009a, IOException -> 0x0091, ClassNotFoundException -> 0x0088, all -> 0x007f }
            r1.close()     // Catch:{ IOException -> 0x0078 }
        L_0x001a:
            r0 = r5
        L_0x001b:
            return r0
        L_0x001c:
            r0 = move-exception
            r0 = r3
            r1 = r3
        L_0x001f:
            if (r0 == 0) goto L_0x0026
            r0.close()     // Catch:{ IOException -> 0x002c }
        L_0x0024:
            r0 = r3
            goto L_0x001b
        L_0x0026:
            if (r1 == 0) goto L_0x0024
            r1.close()     // Catch:{ IOException -> 0x002c }
            goto L_0x0024
        L_0x002c:
            r0 = move-exception
            goto L_0x0024
        L_0x002e:
            r0 = move-exception
            r0 = r3
            r1 = r3
        L_0x0031:
            if (r0 == 0) goto L_0x0039
            r0.close()     // Catch:{ IOException -> 0x0037 }
            goto L_0x0024
        L_0x0037:
            r0 = move-exception
            goto L_0x0024
        L_0x0039:
            if (r1 == 0) goto L_0x0024
            r1.close()     // Catch:{ IOException -> 0x0037 }
            goto L_0x0024
        L_0x003f:
            r0 = move-exception
            r0 = r3
            r1 = r3
        L_0x0042:
            if (r0 == 0) goto L_0x004a
            r0.close()     // Catch:{ IOException -> 0x0048 }
            goto L_0x0024
        L_0x0048:
            r0 = move-exception
            goto L_0x0024
        L_0x004a:
            if (r1 == 0) goto L_0x0024
            r1.close()     // Catch:{ IOException -> 0x0048 }
            goto L_0x0024
        L_0x0050:
            r0 = move-exception
            r0 = r3
            r1 = r3
        L_0x0053:
            if (r0 == 0) goto L_0x005b
            r0.close()     // Catch:{ IOException -> 0x0059 }
            goto L_0x0024
        L_0x0059:
            r0 = move-exception
            goto L_0x0024
        L_0x005b:
            if (r1 == 0) goto L_0x0024
            r1.close()     // Catch:{ IOException -> 0x0059 }
            goto L_0x0024
        L_0x0061:
            r0 = move-exception
            r1 = r3
            r2 = r3
        L_0x0064:
            if (r1 == 0) goto L_0x006a
            r1.close()     // Catch:{ IOException -> 0x0070 }
        L_0x0069:
            throw r0
        L_0x006a:
            if (r2 == 0) goto L_0x0069
            r2.close()     // Catch:{ IOException -> 0x0070 }
            goto L_0x0069
        L_0x0070:
            r1 = move-exception
            goto L_0x0069
        L_0x0072:
            r1.close()     // Catch:{ IOException -> 0x0076 }
            goto L_0x0024
        L_0x0076:
            r0 = move-exception
            goto L_0x0024
        L_0x0078:
            r0 = move-exception
            goto L_0x001a
        L_0x007a:
            r1 = move-exception
            r2 = r0
            r0 = r1
            r1 = r3
            goto L_0x0064
        L_0x007f:
            r2 = move-exception
            r4 = r2
            r2 = r0
            r0 = r4
            goto L_0x0064
        L_0x0084:
            r1 = move-exception
            r1 = r0
            r0 = r3
            goto L_0x0053
        L_0x0088:
            r2 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
            goto L_0x0053
        L_0x008d:
            r1 = move-exception
            r1 = r0
            r0 = r3
            goto L_0x0042
        L_0x0091:
            r2 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
            goto L_0x0042
        L_0x0096:
            r1 = move-exception
            r1 = r0
            r0 = r3
            goto L_0x0031
        L_0x009a:
            r2 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
            goto L_0x0031
        L_0x009f:
            r1 = move-exception
            r1 = r0
            r0 = r3
            goto L_0x001f
        L_0x00a4:
            r2 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
            goto L_0x001f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.openfeint.internal.u.a(java.io.File):java.util.HashMap");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00d4, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00d5, code lost:
        r12.c.writeLock().unlock();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00de, code lost:
        throw r0;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x006f  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00d4 A[ExcHandler: all (r0v3 'th' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:12:0x004c] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void d() {
        /*
            r12 = this;
            r9 = 0
            r8 = 0
            r12.f229a = r9
            long r1 = java.lang.System.currentTimeMillis()
            android.content.Context r0 = r12.b
            java.lang.String r3 = "of_prefs"
            java.io.File r3 = r0.getFileStreamPath(r3)
            java.util.concurrent.locks.ReentrantReadWriteLock r0 = r12.c
            java.util.concurrent.locks.ReentrantReadWriteLock$WriteLock r0 = r0.writeLock()
            r0.lock()
            android.content.Context r0 = r12.b     // Catch:{ IOException -> 0x00c8, all -> 0x00d4 }
            android.content.pm.PackageManager r0 = r0.getPackageManager()     // Catch:{ IOException -> 0x00c8, all -> 0x00d4 }
            r4 = 0
            java.util.List r4 = r0.getInstalledApplications(r4)     // Catch:{ IOException -> 0x00c8, all -> 0x00d4 }
            java.util.Iterator r5 = r4.iterator()     // Catch:{ IOException -> 0x00c8, all -> 0x00d4 }
        L_0x0028:
            boolean r0 = r5.hasNext()     // Catch:{ IOException -> 0x00c8, all -> 0x00d4 }
            if (r0 != 0) goto L_0x0096
            r0 = r9
        L_0x002f:
            java.lang.String r5 = r3.getCanonicalPath()     // Catch:{ IOException -> 0x00c8, all -> 0x00d4 }
            if (r0 == 0) goto L_0x00e4
            java.lang.String r6 = r0.dataDir     // Catch:{ IOException -> 0x00c8, all -> 0x00d4 }
            boolean r6 = r5.startsWith(r6)     // Catch:{ IOException -> 0x00c8, all -> 0x00d4 }
            if (r6 == 0) goto L_0x00e4
            java.lang.String r0 = r0.dataDir     // Catch:{ IOException -> 0x00c8, all -> 0x00d4 }
            int r0 = r0.length()     // Catch:{ IOException -> 0x00c8, all -> 0x00d4 }
            java.lang.String r5 = r5.substring(r0)     // Catch:{ IOException -> 0x00c8, all -> 0x00d4 }
            java.util.Iterator r4 = r4.iterator()     // Catch:{ IOException -> 0x00c8, all -> 0x00d4 }
            r6 = r8
        L_0x004c:
            boolean r0 = r4.hasNext()     // Catch:{ IOException -> 0x00df, all -> 0x00d4 }
            if (r0 != 0) goto L_0x00ab
            java.util.HashMap r0 = a(r3)     // Catch:{ IOException -> 0x00df, all -> 0x00d4 }
            r12.f229a = r0     // Catch:{ IOException -> 0x00df, all -> 0x00d4 }
            r0 = r6
        L_0x0059:
            java.util.HashMap r3 = r12.f229a     // Catch:{ IOException -> 0x00e2, all -> 0x00d4 }
            if (r3 != 0) goto L_0x0064
            java.util.HashMap r3 = new java.util.HashMap     // Catch:{ IOException -> 0x00e2, all -> 0x00d4 }
            r3.<init>()     // Catch:{ IOException -> 0x00e2, all -> 0x00d4 }
            r12.f229a = r3     // Catch:{ IOException -> 0x00e2, all -> 0x00d4 }
        L_0x0064:
            java.util.concurrent.locks.ReentrantReadWriteLock r3 = r12.c
            java.util.concurrent.locks.ReentrantReadWriteLock$WriteLock r3 = r3.writeLock()
            r3.unlock()
        L_0x006d:
            if (r0 == 0) goto L_0x0072
            r12.c()
        L_0x0072:
            long r3 = java.lang.System.currentTimeMillis()
            long r0 = r3 - r1
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "Loading prefs took "
            r2.<init>(r3)
            java.lang.Long r3 = new java.lang.Long
            r3.<init>(r0)
            java.lang.String r0 = r3.toString()
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r1 = " millis"
            java.lang.StringBuilder r0 = r0.append(r1)
            r0.toString()
            return
        L_0x0096:
            java.lang.Object r0 = r5.next()     // Catch:{ IOException -> 0x00c8, all -> 0x00d4 }
            android.content.pm.ApplicationInfo r0 = (android.content.pm.ApplicationInfo) r0     // Catch:{ IOException -> 0x00c8, all -> 0x00d4 }
            java.lang.String r6 = r0.packageName     // Catch:{ IOException -> 0x00c8, all -> 0x00d4 }
            android.content.Context r7 = r12.b     // Catch:{ IOException -> 0x00c8, all -> 0x00d4 }
            java.lang.String r7 = r7.getPackageName()     // Catch:{ IOException -> 0x00c8, all -> 0x00d4 }
            boolean r6 = r6.equals(r7)     // Catch:{ IOException -> 0x00c8, all -> 0x00d4 }
            if (r6 == 0) goto L_0x0028
            goto L_0x002f
        L_0x00ab:
            java.lang.Object r0 = r4.next()     // Catch:{ IOException -> 0x00df, all -> 0x00d4 }
            android.content.pm.ApplicationInfo r0 = (android.content.pm.ApplicationInfo) r0     // Catch:{ IOException -> 0x00df, all -> 0x00d4 }
            java.io.File r7 = new java.io.File     // Catch:{ IOException -> 0x00df, all -> 0x00d4 }
            java.lang.String r0 = r0.dataDir     // Catch:{ IOException -> 0x00df, all -> 0x00d4 }
            r7.<init>(r0, r5)     // Catch:{ IOException -> 0x00df, all -> 0x00d4 }
            long r8 = r3.lastModified()     // Catch:{ IOException -> 0x00df, all -> 0x00d4 }
            long r10 = r7.lastModified()     // Catch:{ IOException -> 0x00df, all -> 0x00d4 }
            int r0 = (r8 > r10 ? 1 : (r8 == r10 ? 0 : -1))
            if (r0 >= 0) goto L_0x004c
            r0 = 1
            r3 = r7
            r6 = r0
            goto L_0x004c
        L_0x00c8:
            r0 = move-exception
            r0 = r8
        L_0x00ca:
            java.util.concurrent.locks.ReentrantReadWriteLock r3 = r12.c
            java.util.concurrent.locks.ReentrantReadWriteLock$WriteLock r3 = r3.writeLock()
            r3.unlock()
            goto L_0x006d
        L_0x00d4:
            r0 = move-exception
            java.util.concurrent.locks.ReentrantReadWriteLock r1 = r12.c
            java.util.concurrent.locks.ReentrantReadWriteLock$WriteLock r1 = r1.writeLock()
            r1.unlock()
            throw r0
        L_0x00df:
            r0 = move-exception
            r0 = r6
            goto L_0x00ca
        L_0x00e2:
            r3 = move-exception
            goto L_0x00ca
        L_0x00e4:
            r0 = r8
            goto L_0x0059
        */
        throw new UnsupportedOperationException("Method not decompiled: com.openfeint.internal.u.d():void");
    }

    /* access modifiers changed from: package-private */
    public final a a() {
        this.c.writeLock().lock();
        return new a(this);
    }

    /* access modifiers changed from: package-private */
    public final n b() {
        this.c.readLock().lock();
        return new n(this);
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x002f A[SYNTHETIC, Splitter:B:13:0x002f] */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x003c  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x005d A[SYNTHETIC, Splitter:B:27:0x005d] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x006a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void c() {
        /*
            r5 = this;
            r3 = 0
            java.util.concurrent.locks.ReentrantReadWriteLock r0 = r5.c
            java.util.concurrent.locks.ReentrantReadWriteLock$ReadLock r0 = r0.readLock()
            r0.lock()
            android.content.Context r0 = r5.b     // Catch:{ IOException -> 0x002a, all -> 0x0058 }
            java.lang.String r1 = "of_prefs"
            r2 = 1
            java.io.FileOutputStream r0 = r0.openFileOutput(r1, r2)     // Catch:{ IOException -> 0x002a, all -> 0x0058 }
            java.io.ObjectOutputStream r1 = new java.io.ObjectOutputStream     // Catch:{ IOException -> 0x00a6, all -> 0x009c }
            r1.<init>(r0)     // Catch:{ IOException -> 0x00a6, all -> 0x009c }
            java.util.HashMap r2 = r5.f229a     // Catch:{ IOException -> 0x00aa, all -> 0x00a1 }
            r1.writeObject(r2)     // Catch:{ IOException -> 0x00aa, all -> 0x00a1 }
            r1.close()     // Catch:{ IOException -> 0x0086, all -> 0x0091 }
            java.util.concurrent.locks.ReentrantReadWriteLock r0 = r5.c
            java.util.concurrent.locks.ReentrantReadWriteLock$ReadLock r0 = r0.readLock()
            r0.unlock()
        L_0x0029:
            return
        L_0x002a:
            r0 = move-exception
            r0 = r3
            r1 = r3
        L_0x002d:
            if (r0 == 0) goto L_0x003c
            r0.close()     // Catch:{ IOException -> 0x0042, all -> 0x004d }
        L_0x0032:
            java.util.concurrent.locks.ReentrantReadWriteLock r0 = r5.c
            java.util.concurrent.locks.ReentrantReadWriteLock$ReadLock r0 = r0.readLock()
            r0.unlock()
            goto L_0x0029
        L_0x003c:
            if (r1 == 0) goto L_0x0032
            r1.close()     // Catch:{ IOException -> 0x0042, all -> 0x004d }
            goto L_0x0032
        L_0x0042:
            r0 = move-exception
            java.util.concurrent.locks.ReentrantReadWriteLock r0 = r5.c
            java.util.concurrent.locks.ReentrantReadWriteLock$ReadLock r0 = r0.readLock()
            r0.unlock()
            goto L_0x0029
        L_0x004d:
            r0 = move-exception
            java.util.concurrent.locks.ReentrantReadWriteLock r1 = r5.c
            java.util.concurrent.locks.ReentrantReadWriteLock$ReadLock r1 = r1.readLock()
            r1.unlock()
            throw r0
        L_0x0058:
            r0 = move-exception
            r1 = r3
            r2 = r3
        L_0x005b:
            if (r1 == 0) goto L_0x006a
            r1.close()     // Catch:{ IOException -> 0x0070, all -> 0x007b }
        L_0x0060:
            java.util.concurrent.locks.ReentrantReadWriteLock r1 = r5.c
            java.util.concurrent.locks.ReentrantReadWriteLock$ReadLock r1 = r1.readLock()
            r1.unlock()
        L_0x0069:
            throw r0
        L_0x006a:
            if (r2 == 0) goto L_0x0060
            r2.close()     // Catch:{ IOException -> 0x0070, all -> 0x007b }
            goto L_0x0060
        L_0x0070:
            r1 = move-exception
            java.util.concurrent.locks.ReentrantReadWriteLock r1 = r5.c
            java.util.concurrent.locks.ReentrantReadWriteLock$ReadLock r1 = r1.readLock()
            r1.unlock()
            goto L_0x0069
        L_0x007b:
            r0 = move-exception
            java.util.concurrent.locks.ReentrantReadWriteLock r1 = r5.c
            java.util.concurrent.locks.ReentrantReadWriteLock$ReadLock r1 = r1.readLock()
            r1.unlock()
            throw r0
        L_0x0086:
            r0 = move-exception
            java.util.concurrent.locks.ReentrantReadWriteLock r0 = r5.c
            java.util.concurrent.locks.ReentrantReadWriteLock$ReadLock r0 = r0.readLock()
            r0.unlock()
            goto L_0x0029
        L_0x0091:
            r0 = move-exception
            java.util.concurrent.locks.ReentrantReadWriteLock r1 = r5.c
            java.util.concurrent.locks.ReentrantReadWriteLock$ReadLock r1 = r1.readLock()
            r1.unlock()
            throw r0
        L_0x009c:
            r1 = move-exception
            r2 = r0
            r0 = r1
            r1 = r3
            goto L_0x005b
        L_0x00a1:
            r2 = move-exception
            r4 = r2
            r2 = r0
            r0 = r4
            goto L_0x005b
        L_0x00a6:
            r1 = move-exception
            r1 = r0
            r0 = r3
            goto L_0x002d
        L_0x00aa:
            r2 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
            goto L_0x002d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.openfeint.internal.u.c():void");
    }
}
