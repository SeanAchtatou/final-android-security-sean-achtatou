package com.openfeint.internal;

import a.a.a.n;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.content.res.XmlResourceParser;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.util.DisplayMetrics;
import android.util.Log;
import android.webkit.CookieManager;
import com.ElekaSoftware.OfficeRage.C0000R;
import com.openfeint.api.a;
import com.openfeint.api.a.bc;
import com.openfeint.api.a.k;
import com.openfeint.api.b;
import com.openfeint.api.e;
import com.openfeint.internal.a.g;
import com.openfeint.internal.a.i;
import com.openfeint.internal.a.x;
import com.openfeint.internal.b.d;
import com.openfeint.internal.e.c;
import com.openfeint.internal.ui.s;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import org.apache.http.impl.client.AbstractHttpClient;

public final class w {
    private static w d;

    /* renamed from: a  reason: collision with root package name */
    x f270a;
    Handler b;
    a c;
    private k e;
    /* access modifiers changed from: private */
    public boolean f;
    /* access modifiers changed from: private */
    public boolean g;
    /* access modifiers changed from: private */
    public boolean h;
    /* access modifiers changed from: private */
    public boolean i;
    /* access modifiers changed from: private */
    public boolean j;
    /* access modifiers changed from: private */
    public boolean k;
    /* access modifiers changed from: private */
    public boolean l;
    private e m;
    private d n;
    private u o;
    /* access modifiers changed from: private */
    public Runnable p;
    /* access modifiers changed from: private */
    public List q;
    /* access modifiers changed from: private */
    public Runnable r;
    /* access modifiers changed from: private */
    public List s;
    private String t;
    private int u = -1;
    private Properties v;
    private String w;
    private Context x;

    /* JADX INFO: finally extract failed */
    private w(a aVar, Context context) {
        d = this;
        this.x = context;
        this.c = aVar;
        n b2 = q().b();
        try {
            this.k = b2.a(new StringBuilder(String.valueOf(this.x.getPackageName())).append(".of_declined").toString(), null) != null;
            b2.a();
            this.b = new Handler();
            this.v = new Properties();
            this.v.put("server-url", "https://api.openfeint.com");
            this.v.put("of-version", "1.9.2");
            a(this.v, a("@xml/openfeint_internal_settings"));
            Log.i("OpenFeint", "Using OpenFeint version " + this.v.get("of-version") + " (" + this.v.get("server-url") + ")");
            Properties properties = new Properties();
            a(properties, a("@xml/openfeint_app_settings"));
            a aVar2 = this.c;
            String property = properties.getProperty("app-id");
            if (property != null) {
                aVar2.d = property;
            }
            String property2 = properties.getProperty("game-name");
            if (property2 != null) {
                aVar2.f155a = property2;
            }
            String property3 = properties.getProperty("app-key");
            if (property3 != null) {
                aVar2.b = property3;
            }
            String property4 = properties.getProperty("app-secret");
            if (property4 != null) {
                aVar2.c = property4;
            }
            a aVar3 = this.c;
            String a2 = aVar3.b == null ? a((int) C0000R.string.of_key_cannot_be_null) : aVar3.c == null ? a((int) C0000R.string.of_secret_cannot_be_null) : aVar3.d == null ? a((int) C0000R.string.of_id_cannot_be_null) : aVar3.f155a == null ? a((int) C0000R.string.of_name_cannot_be_null) : null;
            if (a2 != null) {
                a((CharSequence) a2);
            }
            if (!o.a()) {
                o.a(this.c.c);
            }
            this.f270a = new x(this.c.b, this.c.c, q());
            v.b(context);
            s.a(context);
            c.a(context);
            s.b();
            this.n = new d();
        } catch (Throwable th) {
            b2.a();
            throw th;
        }
    }

    public static w a() {
        return d;
    }

    public static String a(int i2) {
        return d.x.getResources().getString(i2);
    }

    /* access modifiers changed from: private */
    public void a(int i2, Object obj) {
        Resources resources = this.x.getResources();
        String string = resources.getString(C0000R.string.of_offline_notification_line2);
        if (i2 != 0) {
            if (403 == i2) {
                this.i = true;
            }
            if (obj instanceof d) {
                string = ((d) obj).b;
            }
        }
        com.openfeint.internal.c.e.a(resources.getString(C0000R.string.of_offline_notification), string, b.Foreground, com.openfeint.api.c.NetworkOffline);
        "Unable to launch IntroFlow because: " + string;
    }

    public static void a(Context context, a aVar, e eVar) {
        a(context);
        if (d == null) {
            d = new w(aVar, context);
        }
        d.m = eVar;
        if (!d.k) {
            String n2 = d.n();
            if (n2 == null) {
                com.openfeint.internal.d.e.b();
            } else {
                com.openfeint.internal.d.e.a(n2);
            }
            d.t();
            return;
        }
        com.openfeint.internal.d.e.a();
    }

    public static void a(Bundle bundle) {
        w wVar = d;
        if (wVar.e != null) {
            bundle.putString("mCurrentUser", wVar.e.d());
        }
        if (wVar.f270a != null) {
            wVar.f270a.a(bundle);
        }
        bundle.putBoolean("mCurrentlyLoggingIn", wVar.f);
        bundle.putBoolean("mCreatingDeviceSession", wVar.g);
        bundle.putBoolean("mDeviceSessionCreated", wVar.h);
        bundle.putBoolean("mBanned", wVar.i);
        bundle.putBoolean("mApproved", wVar.j);
        bundle.putBoolean("mDeclined", wVar.k);
    }

    public static void a(com.openfeint.internal.a.k kVar) {
        w wVar = d;
        if (wVar == null) {
            d dVar = new d();
            dVar.f196a = "NoFeint";
            dVar.b = "OpenFeint has not been initialized.";
            kVar.a(0, dVar.d().getBytes());
            return;
        }
        wVar.b(kVar);
    }

    /* JADX INFO: finally extract failed */
    static /* synthetic */ void a(w wVar, bc bcVar) {
        wVar.e = new k();
        wVar.e.b(bcVar);
        bc v2 = wVar.v();
        if (v2 == null || !v2.c().equals(bcVar.c())) {
            CookieManager.getInstance().removeAllCookie();
        }
        a a2 = wVar.q().a();
        try {
            a2.a("last_logged_in_server", wVar.f());
            a2.a(String.valueOf(wVar.x.getPackageName()) + ".of_declined");
            a2.a("last_logged_in_user", bcVar.d());
            a2.a();
            if (wVar.r != null) {
                wVar.b.post(wVar.r);
                wVar.r = null;
            }
            wVar.n.c();
            com.openfeint.internal.d.e.a(bcVar.c());
        } catch (Throwable th) {
            a2.a();
            throw th;
        }
    }

    public static void a(CharSequence charSequence) {
        com.openfeint.internal.c.b.a(charSequence.toString(), b.Foreground, com.openfeint.api.c.Error);
    }

    private void a(Properties properties, int i2) {
        XmlResourceParser xmlResourceParser;
        String str;
        String str2 = null;
        try {
            xmlResourceParser = this.x.getResources().getXml(i2);
        } catch (Exception e2) {
            xmlResourceParser = null;
        }
        if (xmlResourceParser != null) {
            try {
                int eventType = xmlResourceParser.getEventType();
                while (xmlResourceParser.getEventType() != 1) {
                    if (eventType == 2) {
                        str = xmlResourceParser.getName();
                    } else {
                        if (xmlResourceParser.getEventType() == 4) {
                            properties.setProperty(str2, xmlResourceParser.getText());
                        }
                        str = str2;
                    }
                    xmlResourceParser.next();
                    str2 = str;
                    eventType = xmlResourceParser.getEventType();
                }
                xmlResourceParser.close();
            } catch (Exception e3) {
                throw new RuntimeException(e3);
            }
        }
    }

    private static boolean a(Context context) {
        boolean z;
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 1);
            for (String str : new String[]{"com.openfeint.api.ui.Dashboard", "com.openfeint.internal.ui.IntroFlow", "com.openfeint.internal.ui.Settings", "com.openfeint.internal.ui.NativeBrowser"}) {
                ActivityInfo[] activityInfoArr = packageInfo.activities;
                int length = activityInfoArr.length;
                int i2 = 0;
                while (true) {
                    if (i2 >= length) {
                        z = false;
                        break;
                    }
                    ActivityInfo activityInfo = activityInfoArr[i2];
                    if (!activityInfo.name.equals(str)) {
                        i2++;
                    } else if ((activityInfo.configChanges & 128) == 0) {
                        Log.v("OpenFeint", String.format("ActivityInfo for %s has the wrong configChanges.\nPlease consult README.txt for the correct configuration.", str));
                        return false;
                    } else {
                        z = true;
                    }
                }
                if (!z) {
                    Log.v("OpenFeint", String.format("Couldn't find ActivityInfo for %s.\nPlease consult README.txt for the correct configuration.", str));
                    return false;
                }
            }
            for (String a2 : new String[]{"android.permission.INTERNET"}) {
                if (v.a(a2, context)) {
                    return false;
                }
            }
            return true;
        } catch (PackageManager.NameNotFoundException e2) {
            Log.v("OpenFeint", String.format("Couldn't find PackageInfo for %s.\nPlease initialize OF with an Activity that lives in your root package.", context.getPackageName()));
            return false;
        }
    }

    private static bc b(String str) {
        if (str == null) {
            return null;
        }
        try {
            Object a2 = new s(new n((byte) 0).a(new ByteArrayInputStream(str.getBytes()))).a();
            if (a2 != null && (a2 instanceof bc)) {
                return (bc) a2;
            }
        } catch (IOException e2) {
        }
        return null;
    }

    public static void b(Bundle bundle) {
        w wVar = d;
        if (!wVar.l && bundle != null) {
            wVar.e = (k) b(bundle.getString("mCurrentUser"));
            if (wVar.f270a != null) {
                wVar.f270a.b(bundle);
            }
            wVar.f = bundle.getBoolean("mCurrentlyLoggingIn");
            wVar.g = bundle.getBoolean("mCreatingDeviceSession");
            wVar.h = bundle.getBoolean("mDeviceSessionCreated");
            wVar.i = bundle.getBoolean("mBanned");
            wVar.j = bundle.getBoolean("mApproved");
            wVar.k = bundle.getBoolean("mDeclined");
            wVar.l = true;
        }
    }

    /* access modifiers changed from: private */
    public final void b(com.openfeint.internal.a.k kVar) {
        if (!e() && kVar.j() && v() != null && p()) {
            o();
            if (this.s == null) {
                this.s = new ArrayList();
            }
            this.s.add(new i(this, kVar));
        } else if (this.h || !kVar.l()) {
            this.f270a.a(kVar);
        } else {
            t();
            if (this.q == null) {
                this.q = new ArrayList();
            }
            this.q.add(new h(this, kVar));
        }
    }

    private static List c(String str) {
        ArrayList arrayList = new ArrayList();
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(str)), 8192);
            while (true) {
                String readLine = bufferedReader.readLine();
                if (readLine == null) {
                    break;
                }
                arrayList.add(readLine);
            }
            bufferedReader.close();
        } catch (Exception e2) {
        }
        return arrayList;
    }

    private u q() {
        if (this.o == null) {
            this.o = new u(this.x);
        }
        return this.o;
    }

    static /* synthetic */ void q(w wVar) {
        a a2 = wVar.q().a();
        try {
            a2.a("last_logged_in_server");
            a2.a("last_logged_in_user_name");
            a2.a("last_logged_in_user");
        } finally {
            a2.a();
        }
    }

    /* JADX INFO: finally extract failed */
    private bc r() {
        n b2 = q().b();
        try {
            String a2 = b2.a("last_logged_in_user", null);
            b2.a();
            return b(a2);
        } catch (Throwable th) {
            b2.a();
            throw th;
        }
    }

    private static String s() {
        String str;
        try {
            Iterator it = c("/proc/cpuinfo").iterator();
            while (true) {
                if (it.hasNext()) {
                    String str2 = (String) it.next();
                    if (str2.startsWith("Processor\t")) {
                        str = str2.split(":")[1].trim();
                        break;
                    }
                } else {
                    str = "unknown";
                    break;
                }
            }
            return String.format("family(%s) min(%s) max(%s)", str, c("/sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_min_freq").get(0), c("/sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_max_freq").get(0));
        } catch (Exception e2) {
            return "family(unknown) min(unknown) max(unknown)";
        }
    }

    private void t() {
        if (!this.g && !this.h) {
            HashMap hashMap = new HashMap();
            hashMap.put("platform", "android");
            hashMap.put("device", c());
            hashMap.put("of-version", g());
            hashMap.put("game_version", Integer.toString(k()));
            hashMap.put("protocol_version", "1.0");
            g gVar = new g(hashMap);
            this.g = true;
            b(new f(this, gVar));
        }
    }

    private String u() {
        String string = Settings.Secure.getString(this.x.getContentResolver(), "android_id");
        if (string != null && !string.equals("9774d56d682e549c")) {
            return "android-id-" + string;
        }
        n b2 = q().b();
        try {
            String a2 = b2.a("udid", null);
            if (a2 != null) {
                return a2;
            }
            byte[] bArr = new byte[16];
            new Random().nextBytes(bArr);
            String str = "android-emu-" + new String(a.b.a.a.a.c.a(bArr)).replace("\r\n", "");
            a a3 = q().a();
            try {
                a3.a("udid", str);
                return str;
            } finally {
                a3.a();
            }
        } finally {
            b2.a();
        }
    }

    /* access modifiers changed from: private */
    public final bc v() {
        bc r2 = r();
        n b2 = q().b();
        try {
            URL url = new URL(f());
            URL url2 = new URL(b2.a("last_logged_in_server", ""));
            if (r2 != null && url.equals(url2)) {
                b2.a();
                return r2;
            }
        } catch (MalformedURLException e2) {
        } catch (Throwable th) {
            b2.a();
            throw th;
        }
        b2.a();
        return null;
    }

    private boolean w() {
        if (!this.i) {
            return false;
        }
        a(this.x.getText(C0000R.string.of_banned_dialog));
        return true;
    }

    public final int a(String str) {
        return this.x.getResources().getIdentifier(str, null, this.x.getPackageName());
    }

    public final void a(Intent intent) {
        this.k = false;
        j jVar = new j(this, intent);
        if (!e()) {
            "Not logged in yet - queueing intent " + intent.toString() + " for now.";
            this.r = jVar;
            if (!(this.f || this.g)) {
                o();
                return;
            }
            return;
        }
        this.b.post(jVar);
    }

    public final void a(Runnable runnable) {
        this.b.post(runnable);
    }

    public final void a(String str, com.openfeint.internal.a.a.c cVar, String str2, t tVar) {
        b(new g(this, str, cVar, str2, tVar));
    }

    public final void a(String str, String str2, String str3, i iVar) {
        boolean z;
        if (!w()) {
            if (this.g || !this.h) {
                if (!this.g) {
                    t();
                }
                this.p = new e(this, str, str2, str3, iVar);
                return;
            }
            g gVar = new g();
            if (str == null || str2 == null) {
                z = true;
            } else {
                gVar.a("login", str);
                gVar.a("password", str2);
                z = false;
            }
            if (!(str3 == null || str2 == null)) {
                gVar.a("user_id", str3);
                gVar.a("password", str2);
                z = false;
            }
            this.f = true;
            k kVar = new k(this, gVar, z);
            kVar.a(iVar);
            b(kVar);
        }
    }

    public final void a(boolean z) {
        if (!w()) {
            if (p()) {
                q qVar = new q(this, z);
                if (this.g || !this.h) {
                    if (!this.g) {
                        t();
                    }
                    this.p = qVar;
                    return;
                }
                qVar.run();
                return;
            }
            a(0, "");
        }
    }

    public final AbstractHttpClient b() {
        return this.f270a;
    }

    public final Map c() {
        HashMap hashMap = new HashMap();
        if (this.t == null) {
            this.t = u();
        }
        hashMap.put("identifier", this.t);
        hashMap.put("hardware", "p(" + Build.PRODUCT + ")/m(" + Build.MODEL + ")");
        hashMap.put("os", "v" + Build.VERSION.RELEASE + " (" + Build.VERSION.INCREMENTAL + ")");
        DisplayMetrics a2 = v.a();
        hashMap.put("screen_resolution", String.format("%dx%d (%f dpi)", Integer.valueOf(a2.widthPixels), Integer.valueOf(a2.heightPixels), Float.valueOf(a2.density)));
        hashMap.put("processor", s());
        return hashMap;
    }

    public final k d() {
        return this.e;
    }

    public final boolean e() {
        return this.e != null;
    }

    public final String f() {
        if (this.w == null) {
            String trim = this.v.getProperty("server-url").toLowerCase().trim();
            if (trim.endsWith("/")) {
                this.w = trim.substring(0, trim.length() - 1);
            } else {
                this.w = trim;
            }
        }
        return this.w;
    }

    public final String g() {
        return this.v.getProperty("of-version");
    }

    public final String h() {
        return this.c.f155a;
    }

    public final String i() {
        return this.c.d;
    }

    public final Map j() {
        return this.c.e;
    }

    public final int k() {
        if (this.u == -1) {
            Context context = this.x;
            try {
                this.u = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;
            } catch (Exception e2) {
                this.u = 0;
            }
        }
        return this.u;
    }

    public final Context l() {
        return this.x;
    }

    public final d m() {
        return this.n;
    }

    public final String n() {
        k kVar = this.e;
        if (kVar != null) {
            return kVar.c();
        }
        bc v2 = v();
        if (v2 != null) {
            return v2.c();
        }
        return null;
    }

    public final void o() {
        this.b.post(new r(this));
    }

    public final boolean p() {
        if (v.a("android.permission.ACCESS_NETWORK_STATE", this.x)) {
            return true;
        }
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) this.x.getSystemService("connectivity")).getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
