package com.openfeint.internal;

import com.openfeint.internal.a.a.c;
import com.openfeint.internal.a.s;
import com.openfeint.internal.a.w;
import com.openfeint.internal.b.e;

final class g extends s {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ w f218a;
    private final /* synthetic */ String b;
    private final /* synthetic */ c c;
    private final /* synthetic */ String d;
    private final /* synthetic */ t e;

    g(w wVar, String str, c cVar, String str2, t tVar) {
        this.f218a = wVar;
        this.b = str;
        this.c = cVar;
        this.d = str2;
        this.e = tVar;
    }

    public final String a() {
        return "POST";
    }

    public final void a(Object obj) {
        e eVar = (e) obj;
        w wVar = new w(eVar, this.c, this.d);
        if (this.e != null) {
            wVar.a(new c(this, this.e, eVar));
        }
        this.f218a.b(wVar);
    }

    public final void a(String str) {
        if (this.e != null) {
            this.e.a("", false);
        }
    }

    public final String b() {
        return this.b;
    }

    public final boolean j() {
        return true;
    }
}
