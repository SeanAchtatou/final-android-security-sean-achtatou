package com.openfeint.internal;

import android.content.SharedPreferences;
import java.util.Date;

public final class d {

    /* renamed from: a  reason: collision with root package name */
    private int f207a;
    private long b;
    private int c;
    private long d;
    private int e;
    private Date f;
    private Date g;

    public d() {
        SharedPreferences sharedPreferences = w.a().l().getSharedPreferences("FeintAnalytics", 0);
        sharedPreferences.getInt("dashboardLaunches", this.c);
        sharedPreferences.getInt("sessionLaunches", this.f207a);
        sharedPreferences.getInt("onlineSessions", this.e);
        sharedPreferences.getLong("sessionMilliseconds", this.b);
        sharedPreferences.getLong("dashboardMilliseconds", this.d);
    }

    private void d() {
        SharedPreferences.Editor edit = w.a().l().getSharedPreferences("FeintAnalytics", 0).edit();
        edit.putInt("dashboardLaunches", this.c);
        edit.putInt("sessionLaunches", this.f207a);
        edit.putInt("onlineSessions", this.e);
        edit.putLong("sessionMilliseconds", this.b);
        edit.putLong("dashboardMilliseconds", this.d);
        edit.commit();
    }

    public final void a() {
        this.c++;
        this.f = new Date();
        d();
    }

    public final void b() {
        if (this.f != null) {
            this.d += new Date().getTime() - this.f.getTime();
            this.f = null;
            d();
        }
    }

    public final void c() {
        this.f207a++;
        this.e++;
        this.g = new Date();
        d();
    }
}
