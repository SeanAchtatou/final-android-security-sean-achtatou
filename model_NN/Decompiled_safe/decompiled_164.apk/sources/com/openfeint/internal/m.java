package com.openfeint.internal;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.widget.Toast;
import com.ElekaSoftware.OfficeRage.C0000R;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

public final class m {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public static Bitmap a(Activity activity, int i, Intent intent) {
        boolean z = true;
        if (i == -1) {
            String[] strArr = {"_data", "orientation"};
            Cursor query = activity.getContentResolver().query(intent.getData(), strArr, null, null, null);
            if (query != null) {
                query.moveToFirst();
                String string = query.getString(query.getColumnIndex(strArr[0]));
                int i2 = query.getInt(query.getColumnIndex(strArr[1]));
                query.close();
                Bitmap a2 = a(string);
                int width = a2.getWidth();
                int height = a2.getHeight();
                if (height <= width) {
                    z = false;
                }
                int i3 = z ? 0 : (width - height) / 2;
                int i4 = z ? (height - width) / 2 : 0;
                if (!z) {
                    width = height;
                }
                float f = 152.0f / ((float) width);
                Matrix matrix = new Matrix();
                matrix.postScale(f, f);
                matrix.postRotate((float) i2);
                Bitmap createBitmap = Bitmap.createBitmap(a2, i3, i4, width, width, matrix, false);
                "image! " + createBitmap.getWidth() + "x" + createBitmap.getHeight();
                return createBitmap;
            }
            Toast.makeText(w.a().l(), w.a((int) C0000R.string.of_profile_picture_download_failed), 1).show();
        }
        return null;
    }

    private static Bitmap a(String str) {
        File file = new File(str);
        try {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(file), null, options);
            int min = Math.min(options.outWidth, options.outHeight);
            int i = 1;
            while (min / 2 > 152) {
                min /= 2;
                i++;
            }
            BitmapFactory.Options options2 = new BitmapFactory.Options();
            options2.inSampleSize = i;
            return BitmapFactory.decodeStream(new FileInputStream(file), null, options2);
        } catch (FileNotFoundException e) {
            e.toString();
            return null;
        }
    }

    public static boolean a(int i) {
        return i == 10009;
    }
}
