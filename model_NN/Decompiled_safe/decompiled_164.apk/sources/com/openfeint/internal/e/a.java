package com.openfeint.internal.e;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public abstract class a {

    /* renamed from: a  reason: collision with root package name */
    private static final String f215a = a.class.getSimpleName();
    private String b;
    private int c;
    private SQLiteOpenHelper d;
    private SQLiteDatabase e = null;

    public a(SQLiteOpenHelper sQLiteOpenHelper) {
        this.d = sQLiteOpenHelper;
    }

    public a(String str) {
        this.b = str;
        this.c = 3;
    }

    /* JADX WARNING: Unknown top exception splitter block from list: {B:34:0x0051=Splitter:B:34:0x0051, B:29:0x0048=Splitter:B:29:0x0048, B:51:0x006f=Splitter:B:51:0x006f} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized android.database.sqlite.SQLiteDatabase a() {
        /*
            r5 = this;
            r3 = 0
            monitor-enter(r5)
            android.database.sqlite.SQLiteOpenHelper r0 = r5.d     // Catch:{ all -> 0x0072 }
            if (r0 == 0) goto L_0x000e
            android.database.sqlite.SQLiteOpenHelper r0 = r5.d     // Catch:{ all -> 0x0072 }
            android.database.sqlite.SQLiteDatabase r0 = r0.getWritableDatabase()     // Catch:{ all -> 0x0072 }
        L_0x000c:
            monitor-exit(r5)
            return r0
        L_0x000e:
            android.database.sqlite.SQLiteDatabase r0 = r5.e     // Catch:{ all -> 0x0072 }
            if (r0 == 0) goto L_0x0025
            android.database.sqlite.SQLiteDatabase r0 = r5.e     // Catch:{ all -> 0x0072 }
            boolean r0 = r0.isOpen()     // Catch:{ all -> 0x0072 }
            if (r0 == 0) goto L_0x0025
            android.database.sqlite.SQLiteDatabase r0 = r5.e     // Catch:{ all -> 0x0072 }
            boolean r0 = r0.isReadOnly()     // Catch:{ all -> 0x0072 }
            if (r0 != 0) goto L_0x0025
            android.database.sqlite.SQLiteDatabase r0 = r5.e     // Catch:{ all -> 0x0072 }
            goto L_0x000c
        L_0x0025:
            r0 = 0
            java.lang.String r1 = r5.b     // Catch:{ all -> 0x007d }
            r2 = 0
            android.database.sqlite.SQLiteDatabase r1 = android.database.sqlite.SQLiteDatabase.openOrCreateDatabase(r1, r2)     // Catch:{ all -> 0x007d }
            int r2 = r1.getVersion()     // Catch:{ all -> 0x0060 }
            int r3 = r5.c     // Catch:{ all -> 0x0060 }
            if (r2 == r3) goto L_0x0048
            r1.beginTransaction()     // Catch:{ all -> 0x0060 }
            if (r2 != 0) goto L_0x0055
            r5.a(r1)     // Catch:{ all -> 0x005b }
        L_0x003d:
            int r2 = r5.c     // Catch:{ all -> 0x005b }
            r1.setVersion(r2)     // Catch:{ all -> 0x005b }
            r1.setTransactionSuccessful()     // Catch:{ all -> 0x005b }
            r1.endTransaction()     // Catch:{ all -> 0x0060 }
        L_0x0048:
            android.database.sqlite.SQLiteDatabase r0 = r5.e     // Catch:{ all -> 0x0072 }
            if (r0 == 0) goto L_0x0051
            android.database.sqlite.SQLiteDatabase r0 = r5.e     // Catch:{ Exception -> 0x0080 }
            r0.close()     // Catch:{ Exception -> 0x0080 }
        L_0x0051:
            r5.e = r1     // Catch:{ all -> 0x0072 }
            r0 = r1
            goto L_0x000c
        L_0x0055:
            int r3 = r5.c     // Catch:{ all -> 0x005b }
            r5.a(r1, r2, r3)     // Catch:{ all -> 0x005b }
            goto L_0x003d
        L_0x005b:
            r2 = move-exception
            r1.endTransaction()     // Catch:{ all -> 0x0060 }
            throw r2     // Catch:{ all -> 0x0060 }
        L_0x0060:
            r2 = move-exception
            r4 = r2
            r2 = r1
            r1 = r4
        L_0x0064:
            if (r0 == 0) goto L_0x0075
            android.database.sqlite.SQLiteDatabase r0 = r5.e     // Catch:{ all -> 0x0072 }
            if (r0 == 0) goto L_0x006f
            android.database.sqlite.SQLiteDatabase r0 = r5.e     // Catch:{ Exception -> 0x007b }
            r0.close()     // Catch:{ Exception -> 0x007b }
        L_0x006f:
            r5.e = r2     // Catch:{ all -> 0x0072 }
        L_0x0071:
            throw r1     // Catch:{ all -> 0x0072 }
        L_0x0072:
            r0 = move-exception
            monitor-exit(r5)
            throw r0
        L_0x0075:
            if (r2 == 0) goto L_0x0071
            r2.close()     // Catch:{ all -> 0x0072 }
            goto L_0x0071
        L_0x007b:
            r0 = move-exception
            goto L_0x006f
        L_0x007d:
            r1 = move-exception
            r2 = r3
            goto L_0x0064
        L_0x0080:
            r0 = move-exception
            goto L_0x0051
        */
        throw new UnsupportedOperationException("Method not decompiled: com.openfeint.internal.e.a.a():android.database.sqlite.SQLiteDatabase");
    }

    public abstract void a(SQLiteDatabase sQLiteDatabase);

    public abstract void a(SQLiteDatabase sQLiteDatabase, int i, int i2);

    public final synchronized SQLiteDatabase b() {
        SQLiteDatabase sQLiteDatabase;
        SQLiteDatabase openDatabase;
        SQLiteDatabase sQLiteDatabase2;
        if (this.d != null) {
            sQLiteDatabase2 = this.d.getReadableDatabase();
        } else if (this.e == null || !this.e.isOpen()) {
            try {
                sQLiteDatabase2 = a();
            } catch (SQLiteException e2) {
                if (this.b == null) {
                    throw e2;
                }
                Log.e(f215a, "Couldn't open " + this.b + " for writing (will try read-only):", e2);
                openDatabase = SQLiteDatabase.openDatabase(this.b, null, 1);
                if (openDatabase.getVersion() != this.c) {
                    throw new SQLiteException("Can't upgrade read-only database from version " + openDatabase.getVersion() + " to " + this.c + ": " + this.b);
                }
                Log.w(f215a, "Opened " + this.b + " in read-only mode");
                this.e = openDatabase;
                SQLiteDatabase sQLiteDatabase3 = this.e;
                if (openDatabase != null) {
                    if (openDatabase != this.e) {
                        openDatabase.close();
                    }
                }
                sQLiteDatabase2 = sQLiteDatabase3;
            } catch (Throwable th) {
                Throwable th2 = th;
                sQLiteDatabase = openDatabase;
                th = th2;
            }
        } else {
            sQLiteDatabase2 = this.e;
        }
        return sQLiteDatabase2;
        if (sQLiteDatabase != null) {
            if (sQLiteDatabase != this.e) {
                sQLiteDatabase.close();
            }
        }
        throw th;
    }

    public final synchronized void c() {
        if (this.e != null && this.e.isOpen()) {
            this.e.close();
            this.e = null;
        }
        if (this.d != null) {
            this.d.close();
        }
    }
}
