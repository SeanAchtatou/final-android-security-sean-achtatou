package com.openfeint.internal.e;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public final class d extends SQLiteOpenHelper {
    d(Context context) {
        super(context, "manifest.db", (SQLiteDatabase.CursorFactory) null, 3);
    }

    public final void onCreate(SQLiteDatabase sQLiteDatabase) {
        c.b(sQLiteDatabase, 0, 3);
    }

    public final void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        c.b(sQLiteDatabase, i, i2);
    }
}
