package com.openfeint.internal.e;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;
import com.openfeint.internal.v;
import java.io.File;

public final class c {

    /* renamed from: a  reason: collision with root package name */
    public static b f216a;

    public static void a(Context context) {
        if (v.b()) {
            f216a = new b(context);
        } else if ("mounted".equals(Environment.getExternalStorageState())) {
            f216a = new b(String.valueOf(Environment.getExternalStorageDirectory().getAbsolutePath()) + "/openfeint/webui/manifest.db");
        } else {
            f216a = new b(context);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:?, code lost:
        com.openfeint.internal.ui.s.c();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
        r0.endTransaction();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0057, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0058, code lost:
        r5 = r1;
        r1 = r0;
        r0 = r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x005e, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x005f, code lost:
        r5 = r1;
        r1 = r0;
        r0 = r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:?, code lost:
        return;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0034 A[ExcHandler: SQLiteDiskIOException (e android.database.sqlite.SQLiteDiskIOException), Splitter:B:2:0x0005] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void a(java.lang.String[] r6, java.lang.String[] r7) {
        /*
            r0 = 0
            int r1 = r6.length
            int r2 = r7.length
            if (r1 != r2) goto L_0x0021
            com.openfeint.internal.e.b r1 = com.openfeint.internal.e.c.f216a     // Catch:{ SQLiteDiskIOException -> 0x0034, Exception -> 0x003e, all -> 0x004b }
            android.database.sqlite.SQLiteDatabase r0 = r1.a()     // Catch:{ SQLiteDiskIOException -> 0x0034, Exception -> 0x003e, all -> 0x004b }
            java.lang.String r1 = "INSERT OR REPLACE INTO manifest(path, hash) VALUES(?,?)"
            android.database.sqlite.SQLiteStatement r1 = r0.compileStatement(r1)     // Catch:{ SQLiteDiskIOException -> 0x0034, Exception -> 0x005e }
            r0.beginTransaction()     // Catch:{ SQLiteDiskIOException -> 0x0034, Exception -> 0x005e }
            r2 = 0
        L_0x0015:
            int r3 = r6.length     // Catch:{ SQLiteDiskIOException -> 0x0034, Exception -> 0x005e }
            if (r2 < r3) goto L_0x0022
            r1.close()     // Catch:{ SQLiteDiskIOException -> 0x0034, Exception -> 0x005e }
            r0.setTransactionSuccessful()     // Catch:{ SQLiteDiskIOException -> 0x0034, Exception -> 0x005e }
            r0.endTransaction()     // Catch:{ Exception -> 0x0055 }
        L_0x0021:
            return
        L_0x0022:
            r3 = 1
            r4 = r6[r2]     // Catch:{ SQLiteDiskIOException -> 0x0034, Exception -> 0x005e }
            r1.bindString(r3, r4)     // Catch:{ SQLiteDiskIOException -> 0x0034, Exception -> 0x005e }
            r3 = 2
            r4 = r7[r2]     // Catch:{ SQLiteDiskIOException -> 0x0034, Exception -> 0x005e }
            r1.bindString(r3, r4)     // Catch:{ SQLiteDiskIOException -> 0x0034, Exception -> 0x005e }
            r1.execute()     // Catch:{ SQLiteDiskIOException -> 0x0034, Exception -> 0x005e }
            int r2 = r2 + 1
            goto L_0x0015
        L_0x0034:
            r1 = move-exception
            com.openfeint.internal.ui.s.c()     // Catch:{ all -> 0x0057 }
            r0.endTransaction()     // Catch:{ Exception -> 0x003c }
            goto L_0x0021
        L_0x003c:
            r0 = move-exception
            goto L_0x0021
        L_0x003e:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
        L_0x0042:
            r0.toString()     // Catch:{ all -> 0x005c }
            r1.endTransaction()     // Catch:{ Exception -> 0x0049 }
            goto L_0x0021
        L_0x0049:
            r0 = move-exception
            goto L_0x0021
        L_0x004b:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
        L_0x004f:
            r1.endTransaction()     // Catch:{ Exception -> 0x0053 }
        L_0x0052:
            throw r0
        L_0x0053:
            r1 = move-exception
            goto L_0x0052
        L_0x0055:
            r0 = move-exception
            goto L_0x0021
        L_0x0057:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x004f
        L_0x005c:
            r0 = move-exception
            goto L_0x004f
        L_0x005e:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x0042
        */
        throw new UnsupportedOperationException("Method not decompiled: com.openfeint.internal.e.c.a(java.lang.String[], java.lang.String[]):void");
    }

    /* access modifiers changed from: private */
    public static void b(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        int i3;
        if (i == 0) {
            sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS manifest (path TEXT PRIMARY KEY, hash TEXT);");
            i3 = i + 1;
        } else {
            i3 = i;
        }
        if (i3 == 1) {
            sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS store (ID TEXT PRIMARY KEY, VALUE TEXT);");
            i3++;
        }
        if (i3 == 2) {
            sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS server_manifest (path TEXT PRIMARY KEY NOT NULL, hash TEXT DEFAULT NULL, is_global INTEGER DEFAULT 0);");
            sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS dependencies (path TEXT NOT NULL, has_dependency TEXT NOT NULL);");
            i3++;
        }
        if (i3 != i2) {
            String.format("Unable to upgrade DB from %d to %d.", Integer.valueOf(i3), Integer.valueOf(i2));
        }
    }

    public static boolean b(Context context) {
        if (f216a != null) {
            f216a.c();
        }
        boolean delete = (v.b() || !"mounted".equals(Environment.getExternalStorageState())) ? context.getDatabasePath("manifest.db").delete() : new File(Environment.getExternalStorageDirectory(), "/openfeint/webui/manifest.db").delete();
        if (!delete) {
            return delete;
        }
        a(context);
        return f216a != null;
    }
}
