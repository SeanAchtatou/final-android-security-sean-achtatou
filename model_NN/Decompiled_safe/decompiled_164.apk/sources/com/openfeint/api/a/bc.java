package com.openfeint.api.a;

import com.openfeint.internal.b.m;
import com.openfeint.internal.b.y;

public class bc extends y {

    /* renamed from: a  reason: collision with root package name */
    public String f159a;
    public String b;
    public String c;
    public boolean d;
    public String e;
    public String f;
    public boolean g;
    public boolean h;
    public int i;
    public boolean j;
    public double k;
    public double m;

    public static m a() {
        t tVar = new t(bc.class, "user");
        tVar.b.put("name", new s());
        tVar.b.put("profile_picture_url", new v());
        tVar.b.put("profile_picture_source", new u());
        tVar.b.put("uses_facebook_profile_picture", new ao());
        tVar.b.put("last_played_game_id", new an());
        tVar.b.put("last_played_game_name", new am());
        tVar.b.put("gamer_score", new al());
        tVar.b.put("following_local_user", new ak());
        tVar.b.put("followed_by_local_user", new at());
        tVar.b.put("online", new as());
        tVar.b.put("lat", new ar());
        tVar.b.put("lng", new aq());
        return tVar;
    }
}
