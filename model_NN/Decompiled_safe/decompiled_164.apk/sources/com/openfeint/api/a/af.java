package com.openfeint.api.a;

import com.ElekaSoftware.OfficeRage.C0000R;
import com.openfeint.api.c;
import com.openfeint.internal.a.g;
import com.openfeint.internal.a.h;
import com.openfeint.internal.a.s;
import com.openfeint.internal.c.b;
import com.openfeint.internal.d.e;
import com.openfeint.internal.w;
import java.util.List;

final class af extends s {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ bb f156a;
    private final /* synthetic */ String b;
    private final /* synthetic */ boolean c;
    private final /* synthetic */ z d;
    private final /* synthetic */ boolean e;
    private final /* synthetic */ j f;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    af(bb bbVar, g gVar, String str, boolean z, z zVar, boolean z2, j jVar) {
        super(gVar);
        this.f156a = bbVar;
        this.b = str;
        this.c = z;
        this.d = zVar;
        this.e = z2;
        this.f = jVar;
    }

    public final String a() {
        return "POST";
    }

    /* access modifiers changed from: protected */
    public final void a(int i, Object obj) {
        if (201 == i) {
            if (!this.c) {
                b.a(w.a().l().getResources().getString(C0000R.string.of_score_submitted_notification), "@drawable/of_icon_highscore_notification", com.openfeint.api.b.HighScore, c.Success);
            }
            if (this.d != null) {
                this.d.a(true);
            }
            if (this.e && (obj instanceof List)) {
                bb bbVar = (bb) ((List) obj).get(0);
                h hVar = new h(bbVar.k, String.format("blob.%s.bin", bbVar.c()), this.f156a.i);
                if (this.d != null) {
                    hVar.a(new w(this, this.d));
                }
                hVar.p();
            }
        } else if (200 > i || i >= 300) {
            if ((i == 0 || 500 <= i) && !this.c) {
                e.a(this.f156a, this.f);
                if (this.d != null) {
                    this.d.a(false);
                    return;
                }
                return;
            }
            b(obj);
        } else if (this.d != null) {
            this.d.a(false);
        }
    }

    public final void a(String str) {
        super.a(str);
        if (this.d != null) {
            this.d.a(str);
        }
    }

    public final String b() {
        return this.b;
    }
}
