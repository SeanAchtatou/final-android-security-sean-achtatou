package com.openfeint.api.a;

import com.openfeint.internal.a.g;
import com.openfeint.internal.a.s;
import com.openfeint.internal.c.f;
import com.openfeint.internal.d.e;

final class bi extends s {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ y f161a;
    private final /* synthetic */ String b;
    private final /* synthetic */ String c;
    private final /* synthetic */ ap d = null;
    private final /* synthetic */ float e;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    bi(y yVar, g gVar, String str, String str2, float f) {
        super(gVar);
        this.f161a = yVar;
        this.b = str;
        this.c = str2;
        this.e = f;
    }

    public final String a() {
        return "PUT";
    }

    /* access modifiers changed from: protected */
    public final void a(int i, Object obj) {
        if (i >= 200 && i < 300) {
            y yVar = (y) obj;
            float f = this.f161a.g;
            this.f161a.a(yVar);
            float f2 = this.f161a.g;
            e.b(this.c, f2);
            if (201 == i || f2 > f) {
                f.a(yVar);
            }
            if (this.d != null) {
            }
        } else if (400 <= i && i < 500) {
            b(obj);
        } else if (100.0f == this.e) {
            this.f161a.g = this.e;
            this.f161a.f = true;
            f.a(this.f161a);
        }
    }

    public final void a(String str) {
        super.a(str);
        if (this.d != null) {
            this.d.a(str);
        }
    }

    public final String b() {
        return this.b;
    }
}
