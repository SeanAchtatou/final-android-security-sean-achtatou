package com.openfeint.api.a;

import com.ElekaSoftware.OfficeRage.C0000R;
import com.openfeint.internal.a.g;
import com.openfeint.internal.a.s;
import com.openfeint.internal.b.d;
import com.openfeint.internal.w;

final class au extends s {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ j f157a;
    private final /* synthetic */ String b;
    private final /* synthetic */ x c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    au(j jVar, g gVar, String str, x xVar) {
        super(gVar);
        this.f157a = jVar;
        this.b = str;
        this.c = xVar;
    }

    public final String a() {
        return "GET";
    }

    public final void a(int i, Object obj) {
        if (this.c == null) {
            return;
        }
        if (200 <= i && i < 300) {
            this.c.a((bb) obj);
        } else if (404 == i) {
            this.c.a((bb) null);
        } else if (obj instanceof d) {
            a(((d) obj).b);
        } else {
            a(w.a((int) C0000R.string.of_unknown_server_error));
        }
    }

    public final void a(String str) {
        super.a(str);
        if (this.c != null) {
            this.c.a(str);
        }
    }

    public final String b() {
        return this.b;
    }
}
