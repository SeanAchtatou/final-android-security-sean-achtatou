package com.openfeint.api.a;

import com.openfeint.internal.a.g;
import com.openfeint.internal.b.m;
import com.openfeint.internal.b.y;
import com.openfeint.internal.w;
import java.util.List;

public class j extends y {

    /* renamed from: a  reason: collision with root package name */
    public String f163a;
    public bb b;
    public boolean c = true;
    public boolean d;
    public List e;

    public j() {
    }

    public j(String str) {
        a(str);
    }

    public static m a() {
        av avVar = new av(j.class, "leaderboard");
        avVar.b.put("name", new aw());
        avVar.b.put("current_user_high_score", new ax(bb.class));
        avVar.b.put("descending_sort_order", new ay());
        avVar.b.put("allow_posting_lower_scores", new az());
        avVar.b.put("high_scores", new ba(bb.class));
        return avVar;
    }

    public final void a(bc bcVar, x xVar) {
        new au(this, new g(), "/xp/users/" + bcVar.c() + "/games/" + w.a().i() + "/leaderboards/" + c() + "/current_score", xVar).p();
    }
}
