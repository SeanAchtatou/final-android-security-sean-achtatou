package com.openfeint.api.a;

import com.openfeint.internal.a.g;
import com.openfeint.internal.b.m;
import com.openfeint.internal.d.e;
import com.openfeint.internal.w;
import java.util.Date;

public class y extends com.openfeint.internal.b.y {

    /* renamed from: a  reason: collision with root package name */
    public String f165a;
    public String b;
    public int c;
    public String d;
    public boolean e;
    public boolean f;
    public float g;
    public Date h;
    public String i;
    public String j;
    public int k;

    public y() {
    }

    public y(String str) {
        a(str);
    }

    public static m a() {
        bh bhVar = new bh(y.class, "achievement");
        bhVar.b.put("title", new bg());
        bhVar.b.put("description", new bf());
        bhVar.b.put("gamerscore", new be());
        bhVar.b.put("icon_url", new g());
        bhVar.b.put("is_secret", new f());
        bhVar.b.put("is_unlocked", new e());
        bhVar.b.put("percent_complete", new d());
        bhVar.b.put("unlocked_at", new c());
        bhVar.b.put("position", new b());
        bhVar.b.put("end_version", new a());
        bhVar.b.put("start_version", new h());
        return bhVar;
    }

    public static void a(aj ajVar) {
        new bd("/xp/games/" + w.a().i() + "/achievements", ajVar).p();
    }

    public final void a(float f2) {
        float f3 = f2 < 0.0f ? 0.0f : f2 > 100.0f ? 100.0f : f2;
        String c2 = c();
        if (c2 != null && f3 > e.b(c2)) {
            e.a(c2, f3);
            if (w.a().n() == null) {
                this.g = e.b(c2);
                this.f = this.g == 100.0f;
                return;
            }
            g gVar = new g();
            gVar.a("percent_complete", new Float(f3).toString());
            new bi(this, gVar, "/xp/games/" + w.a().i() + "/achievements/" + c2 + "/unlock", c2, f3).p();
        }
    }

    public final void b() {
        a(100.0f);
    }
}
