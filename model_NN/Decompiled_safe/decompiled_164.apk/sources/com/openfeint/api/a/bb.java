package com.openfeint.api.a;

import com.openfeint.internal.a.g;
import com.openfeint.internal.b.e;
import com.openfeint.internal.b.m;
import com.openfeint.internal.b.y;
import com.openfeint.internal.w;

public class bb extends y {

    /* renamed from: a  reason: collision with root package name */
    public bc f158a;
    public long b;
    public int c;
    public int d;
    public String e;
    public String f;
    public double g;
    public double h;
    public byte[] i;
    /* access modifiers changed from: private */
    public String j;
    /* access modifiers changed from: private */
    public e k;

    public bb() {
    }

    public bb(long j2) {
        this.b = j2;
        this.e = null;
    }

    public bb(long j2, byte b2) {
        this.b = j2;
    }

    public static m a() {
        ac acVar = new ac(bb.class, "high_score");
        acVar.b.put("score", new ab());
        acVar.b.put("rank", new ae());
        acVar.b.put("leaderboard_id", new ad());
        acVar.b.put("display_text", new ag());
        acVar.b.put("custom_data", new ah());
        acVar.b.put("lat", new ai());
        acVar.b.put("lng", new p());
        acVar.b.put("user", new o(bc.class));
        acVar.b.put("blob_url", new r());
        acVar.b.put("blob_upload_parameters", new q(e.class));
        return acVar;
    }

    private void a(j jVar, String str, z zVar, boolean z) {
        boolean z2 = false;
        if (jVar == null || jVar.c() == null || jVar.c().length() == 0) {
            if (zVar != null) {
                zVar.a("No leaderboard ID provided.  Please provide a leaderboard ID from the Dev Dashboard.");
            }
        } else if (!w.a().e()) {
            com.openfeint.internal.d.e.a(this, jVar);
            if (zVar != null) {
                zVar.a(false);
            }
        } else {
            String str2 = "/xp/games/" + w.a().i() + "/leaderboards/" + jVar.c() + "/high_scores";
            g gVar = new g();
            gVar.a("high_score[score]", new Long(this.b).toString());
            if (this.e != null) {
                gVar.a("high_score[display_text]", this.e);
            }
            if (this.i != null) {
                z2 = true;
            }
            gVar.a("high_score[has_blob]", z2 ? "1" : "0");
            if (str != null) {
                gVar.a("high_score[timestamp]", str);
            }
            new af(this, gVar, str2, z, zVar, z2, jVar).p();
        }
    }

    public final void a(j jVar, z zVar) {
        a(jVar, null, zVar, false);
    }

    public final void a(j jVar, String str, z zVar) {
        a(jVar, str, zVar, true);
    }
}
