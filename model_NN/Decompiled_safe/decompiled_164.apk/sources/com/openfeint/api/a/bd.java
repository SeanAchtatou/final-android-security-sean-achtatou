package com.openfeint.api.a;

import com.ElekaSoftware.OfficeRage.C0000R;
import com.openfeint.internal.a.s;
import com.openfeint.internal.w;
import java.util.List;

final class bd extends s {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ String f160a;
    private final /* synthetic */ aj b;

    bd(String str, aj ajVar) {
        this.f160a = str;
        this.b = ajVar;
    }

    public final String a() {
        return "GET";
    }

    public final void a(Object obj) {
        if (this.b != null) {
            try {
                this.b.a((List) obj);
            } catch (Exception e) {
                a(w.a((int) C0000R.string.of_unexpected_response_format));
            }
        }
    }

    public final void a(String str) {
        super.a(str);
        if (this.b != null) {
            this.b.a(str);
        }
    }

    public final String b() {
        return this.f160a;
    }
}
