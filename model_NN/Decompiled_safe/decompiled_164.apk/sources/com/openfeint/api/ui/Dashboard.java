package com.openfeint.api.ui;

import android.content.Intent;
import android.content.res.Resources;
import android.view.Menu;
import android.view.MenuItem;
import com.ElekaSoftware.OfficeRage.C0000R;
import com.openfeint.api.b;
import com.openfeint.api.c;
import com.openfeint.internal.d.e;
import com.openfeint.internal.ui.WebNav;
import com.openfeint.internal.ui.ah;
import com.openfeint.internal.w;
import java.util.ArrayList;
import java.util.List;

public class Dashboard extends WebNav {
    private static List h = new ArrayList();
    private boolean g = true;

    private static void b(String str) {
        e.c();
        w a2 = w.a();
        if (!a2.p()) {
            Resources resources = w.a().l().getResources();
            com.openfeint.internal.c.e.a(resources.getString(C0000R.string.of_offline_notification), resources.getString(C0000R.string.of_offline_notification_line2), b.Foreground, c.NetworkOffline);
            return;
        }
        a2.m().a();
        Intent intent = new Intent(a2.l(), Dashboard.class);
        if (str != null) {
            intent.putExtra("screenName", str);
        }
        a2.a(intent);
    }

    public static void d() {
        b((String) null);
    }

    public static void e() {
        b("leaderboards");
    }

    /* access modifiers changed from: protected */
    public final ah a(WebNav webNav) {
        return new a(this, webNav);
    }

    /* access modifiers changed from: protected */
    public final String a() {
        String stringExtra = getIntent().getStringExtra("screenName");
        if (stringExtra == null) {
            return "dashboard/user";
        }
        this.g = false;
        return "dashboard/" + stringExtra;
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(C0000R.menu.of_dashboard, menu);
        return true;
    }

    public void onDestroy() {
        super.onDestroy();
        h.remove(this);
        w.a().m().b();
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        String str = null;
        if (menuItem.getItemId() == C0000R.id.home) {
            str = "home";
            this.g = true;
        } else if (menuItem.getItemId() == C0000R.id.settings) {
            str = "settings";
        } else if (menuItem.getItemId() == C0000R.id.exit_feint) {
            str = "exit";
        }
        if (str == null) {
            return super.onOptionsItemSelected(menuItem);
        }
        a(String.format("OF.menu('%s')", str));
        return true;
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.findItem(C0000R.id.home).setVisible(!this.g || this.d > 1);
        return true;
    }

    public void onResume() {
        super.onResume();
        if (!h.contains(this)) {
            h.add(this);
        }
        if (w.a().d() == null) {
            finish();
        }
    }
}
