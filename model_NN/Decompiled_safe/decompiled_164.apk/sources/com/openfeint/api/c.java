package com.openfeint.api;

public enum c {
    None,
    Submitting,
    Downloading,
    Error,
    Success,
    NewResources,
    UserPresenceOnLine,
    UserPresenceOffline,
    NewMessage,
    Multiplayer,
    NetworkOffline
}
