package com.a.a.a.a;

public abstract class d extends e {
    private static char[] a(char[] cArr, int i, int i2) {
        char[] cArr2 = new char[i2];
        if (i > 0) {
            System.arraycopy(cArr, 0, cArr2, 0, i);
        }
        return cArr2;
    }

    /* access modifiers changed from: protected */
    public abstract int a(CharSequence charSequence, int i, int i2);

    /* access modifiers changed from: protected */
    public final String a(String str, int i) {
        int i2;
        char[] cArr;
        int i3;
        int i4;
        int length = str.length();
        int i5 = 0;
        char[] a2 = a.a();
        int i6 = i;
        int i7 = 0;
        while (i6 < length) {
            if (i6 < length) {
                int i8 = i6 + 1;
                char charAt = str.charAt(i6);
                if (charAt < 55296 || charAt > 57343) {
                    i3 = charAt;
                } else if (charAt > 56319) {
                    throw new IllegalArgumentException("Unexpected low surrogate character '" + charAt + "' with value " + ((int) charAt) + " at index " + (i8 - 1));
                } else if (i8 == length) {
                    i3 = -charAt;
                } else {
                    char charAt2 = str.charAt(i8);
                    if (Character.isLowSurrogate(charAt2)) {
                        i3 = Character.toCodePoint(charAt, charAt2);
                    } else {
                        throw new IllegalArgumentException("Expected low surrogate but got char '" + charAt2 + "' with value " + ((int) charAt2) + " at index " + i8);
                    }
                }
                if (i3 < 0) {
                    throw new IllegalArgumentException("Trailing high surrogate at end of input");
                }
                char[] a3 = a(i3);
                int i9 = (Character.isSupplementaryCodePoint(i3) ? 2 : 1) + i6;
                if (a3 != null) {
                    int i10 = i6 - i7;
                    int length2 = i5 + i10 + a3.length;
                    if (a2.length < length2) {
                        a2 = a(a2, i5, length2 + (length - i6) + 32);
                    }
                    if (i10 > 0) {
                        str.getChars(i7, i6, a2, i5);
                        i4 = i5 + i10;
                    } else {
                        i4 = i5;
                    }
                    if (a3.length > 0) {
                        System.arraycopy(a3, 0, a2, i4, a3.length);
                        i4 += a3.length;
                    }
                    i5 = i4;
                    i7 = i9;
                }
                i6 = a(str, i9, length);
            } else {
                throw new IndexOutOfBoundsException("Index exceeds specified range");
            }
        }
        int i11 = length - i7;
        if (i11 > 0) {
            int i12 = i11 + i5;
            if (a2.length < i12) {
                a2 = a(a2, i5, i12);
            }
            str.getChars(i7, length, a2, i5);
            i2 = i12;
            cArr = a2;
        } else {
            i2 = i5;
            cArr = a2;
        }
        return new String(cArr, 0, i2);
    }

    /* access modifiers changed from: protected */
    public abstract char[] a(int i);
}
