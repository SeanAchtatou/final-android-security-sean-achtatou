package com.ElekaSoftware.OfficeRage;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import com.mobfox.sdk.MobFoxView;
import com.mobfox.sdk.j;

public final class a extends Dialog implements View.OnClickListener, j {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public ImageButton f52a = null;
    /* access modifiers changed from: private */
    public ScoreActivity b;
    private MobFoxView c;

    public a(Context context, ScoreActivity scoreActivity) {
        super(context);
        requestWindowFeature(1);
        setContentView((int) C0000R.layout.score_ad_dialog);
        this.b = scoreActivity;
        this.f52a = (ImageButton) findViewById(C0000R.id.score_ad_dialog_close);
        this.f52a.setOnClickListener(this);
        this.f52a.setEnabled(false);
        this.c = (MobFoxView) findViewById(C0000R.id.mobFoxView);
        this.c.a(this);
        this.c.b();
    }

    /* access modifiers changed from: private */
    public boolean a(int i) {
        switch (i) {
            case 0:
                this.f52a.setBackgroundResource(C0000R.drawable.button_ad_close);
                break;
            case 1:
                this.f52a.setBackgroundResource(C0000R.drawable.button_ad_1);
                break;
            case 2:
                this.f52a.setBackgroundResource(C0000R.drawable.button_ad_2);
                break;
            case 3:
                this.f52a.setBackgroundResource(C0000R.drawable.button_ad_3);
                break;
            case 4:
                this.f52a.setBackgroundResource(C0000R.drawable.button_ad_4);
                break;
            case 5:
                this.f52a.setBackgroundResource(C0000R.drawable.button_ad_5);
                break;
        }
        try {
            this.f52a.invalidate();
            return true;
        } catch (NullPointerException e) {
            return false;
        }
    }

    public final void onBackPressed() {
        if (this.f52a.isEnabled()) {
            this.b.runOnUiThread(new aj(this));
            dismiss();
        }
    }

    public final void onClick(View view) {
        switch (view.getId()) {
            case C0000R.id.score_ad_dialog_close /*2131361827*/:
                this.b.runOnUiThread(new ai(this));
                dismiss();
                return;
            default:
                return;
        }
    }

    public final void onCreate(Bundle bundle) {
    }

    public final void onStart() {
        new j(this).execute(new Void[0]);
    }

    public final void onStop() {
        Log.d("ScoreActivity", "AdDialog onStop");
        this.b = null;
        this.f52a.setOnClickListener(null);
        this.f52a = null;
        this.c.a((j) null);
        this.c = null;
    }
}
