package com.ElekaSoftware.OfficeRage;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;

public final class r extends Drawable {

    /* renamed from: a  reason: collision with root package name */
    private double f129a;
    private double b;
    private boolean c;
    private int d;
    private int e;
    private int f;
    private Bitmap g;
    private Bitmap h;
    private Rect i;
    private int j;
    /* access modifiers changed from: private */
    public h k;
    private int l;
    private int m;
    private int n;
    private int o;
    private int p;
    private int q;
    private boolean r = false;
    private double s;
    private double t;
    private int u = 1;
    private boolean v;

    public r(Context context, h hVar, int i2, int i3, int i4) {
        this.k = hVar;
        this.l = i3;
        this.o = i4;
        this.m = (i3 - i4) / i2;
        this.n = 0;
        this.f129a = (double) (i2 * 10);
        this.b = (double) (i2 * 10);
        this.c = false;
        this.d = 10;
        this.e = 10;
        this.v = false;
        this.g = BitmapFactory.decodeResource(context.getResources(), C0000R.drawable.timecounter_left);
        this.h = BitmapFactory.decodeResource(context.getResources(), C0000R.drawable.timecounter_right);
        this.i = new Rect(this.d, this.e, this.h.getWidth() + 10, this.h.getHeight() + 10);
        this.j = this.h.getWidth() - this.g.getWidth();
        this.f = (this.d - this.j) + ((int) ((this.f129a / this.b) * ((double) this.j)));
        this.s = (2.0d * this.b) / 3.0d;
        this.t = this.b / 4.0d;
    }

    public final synchronized void a() {
        if (this.c) {
            this.f129a -= 1.0d;
            if (this.f129a <= 0.0d) {
                if (!this.v) {
                    this.v = true;
                    this.k.post(new x(this));
                }
                if (this.f129a < -40.0d) {
                    this.c = false;
                    this.k.post(new u(this));
                }
            } else if (this.n >= this.l) {
                this.k.post(new w(this));
                this.n -= this.l;
                if (!this.r) {
                    if (this.l > this.o) {
                        this.l -= this.m;
                    } else {
                        this.l = this.o;
                    }
                }
            } else {
                this.n += 100;
            }
            if (this.f129a < this.s && this.u == 1) {
                this.u = 2;
                this.k.post(new v(this));
            }
            if (this.f129a < this.t && this.u == 2) {
                this.u = 3;
                this.k.post(new y(this));
            }
            this.f = (this.d - this.j) + ((int) ((this.f129a / this.b) * ((double) this.j)));
        }
    }

    public final void a(int i2) {
        this.f129a += (double) (i2 * 10);
        if (this.f129a > this.b) {
            this.f129a = this.b;
        }
    }

    public final void b() {
        this.c = true;
    }

    public final void c() {
        this.p = this.l;
        this.q = this.m;
        this.r = true;
        this.l = this.o / 2;
    }

    public final void d() {
        this.l = this.p;
        this.m = this.q;
        this.r = false;
    }

    public final void draw(Canvas canvas) {
        if (this.c) {
            canvas.save();
            canvas.clipRect(this.i);
            canvas.drawBitmap(this.h, (float) this.f, (float) this.e, (Paint) null);
            if (this.f129a >= 0.0d) {
                canvas.drawBitmap(this.g, (float) this.d, (float) this.e, (Paint) null);
            }
            canvas.restore();
        }
    }

    public final int getOpacity() {
        return 0;
    }

    public final void setAlpha(int i2) {
    }

    public final void setColorFilter(ColorFilter colorFilter) {
    }
}
