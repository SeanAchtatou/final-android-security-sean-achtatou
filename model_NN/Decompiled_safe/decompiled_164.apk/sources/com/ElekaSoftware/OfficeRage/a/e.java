package com.ElekaSoftware.OfficeRage.a;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.text.format.Time;
import com.ElekaSoftware.OfficeRage.C0000R;
import com.ElekaSoftware.OfficeRage.h;

public final class e extends l {
    private static Bitmap y;
    private static Bitmap z;
    private float A = ((float) (y.getWidth() / 2));
    private float B = ((float) y.getHeight());
    private float C = ((float) (z.getWidth() / 2));
    private float D = ((float) z.getHeight());
    private float w;
    private float x;

    public e(Context context, h hVar) {
        super(context, hVar);
        this.d = a((int) C0000R.drawable.gameitem_clock);
        this.e = a((int) C0000R.drawable.gameitem_clock);
        y = a((int) C0000R.drawable.gameitem_clock_hour);
        z = a((int) C0000R.drawable.gameitem_clock_minute);
        this.f = 1;
        this.g = this.d.getWidth() / 2;
        this.h = this.d.getHeight() / 2;
        this.i = "clock";
        this.j = 200;
        this.q = true;
        this.c = 16;
        this.s = 0;
        this.t = 0;
        this.u = C0000R.drawable.score_clock;
        b();
    }

    private void b() {
        Time time = new Time();
        time.set(System.currentTimeMillis());
        this.w = (((float) time.hour) / 12.0f) * 360.0f;
        this.x = (((float) time.minute) / 60.0f) * 360.0f;
    }

    public final void a() {
        this.q = true;
        b();
    }

    public final void a(float f) {
        if (isVisible()) {
            if (!this.q) {
                this.n += this.r * f;
            }
            this.f78a += this.m * f;
            this.b += this.n * f;
            if (this.f78a < -400.0f) {
                setVisible(false, false);
                a();
            }
            if (this.f78a > 1200.0f) {
                setVisible(false, false);
                a();
            }
            if (this.b < -400.0f) {
                setVisible(false, false);
                a();
            }
            if (this.b > 880.0f) {
                setVisible(false, false);
                a();
            }
            if (this.q) {
                this.w = (float) (((double) this.w) + (((double) f) * 0.05d));
                this.x = (float) (((double) this.x) + (((double) f) * 0.6d));
            }
            this.p += this.o * f;
        }
    }

    public final void a(boolean z2) {
        this.q = false;
        this.m /= 5.0f;
        if (z2) {
            this.v.post(new an(this));
        } else {
            this.v.post(new am(this));
        }
    }

    public final void draw(Canvas canvas) {
        if (isVisible()) {
            canvas.save();
            canvas.rotate(this.p, this.f78a, this.b);
            if (!this.q) {
                canvas.drawBitmap(this.e, this.f78a - ((float) this.g), this.b - ((float) this.h), (Paint) null);
            } else {
                canvas.drawBitmap(this.d, this.f78a - ((float) this.g), this.b - ((float) this.h), (Paint) null);
            }
            canvas.save();
            canvas.rotate(this.w, this.f78a, this.b);
            canvas.drawBitmap(y, this.f78a - this.A, this.b - this.B, (Paint) null);
            canvas.restore();
            canvas.save();
            canvas.rotate(this.x, this.f78a, this.b);
            canvas.drawBitmap(z, this.f78a - this.C, this.b - this.D, (Paint) null);
            canvas.restore();
            canvas.restore();
        }
    }
}
