package com.ElekaSoftware.OfficeRage.a;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import com.ElekaSoftware.OfficeRage.h;

public abstract class l extends Drawable {
    private float A;
    private float B;
    private float C;

    /* renamed from: a  reason: collision with root package name */
    public float f78a = -150.0f;
    public float b = -150.0f;
    public int c;
    public Bitmap d;
    public Bitmap e;
    public int f;
    protected int g = 0;
    protected int h = 0;
    public String i;
    public int j = 0;
    public int k = 0;
    public int l = 0;
    public float m = 0.0f;
    public float n = 0.0f;
    public float o = 0.09f;
    public float p = 0.0f;
    public boolean q;
    protected float r = 0.002f;
    public int s = 0;
    public int t = 0;
    public int u = 0;
    protected h v;
    private int w;
    private Context x;
    private Paint y;
    private float z;

    public l(Context context, h hVar) {
        setVisible(false, false);
        this.v = hVar;
        this.w = hVar.g;
        this.x = context;
        this.y = new Paint();
        this.y.setFilterBitmap(true);
        this.z = this.v.m;
        this.A = this.v.l;
        this.B = this.v.l;
        this.C = ((float) this.v.j) - this.v.l;
    }

    /* access modifiers changed from: protected */
    public final Bitmap a(int i2) {
        switch (this.w) {
            case 120:
                Bitmap decodeResource = BitmapFactory.decodeResource(this.x.getResources(), i2);
                return Bitmap.createScaledBitmap(decodeResource, decodeResource.getWidth() / 2, decodeResource.getHeight() / 2, true);
            case 160:
                Bitmap decodeResource2 = BitmapFactory.decodeResource(this.x.getResources(), i2);
                return Bitmap.createScaledBitmap(decodeResource2, (decodeResource2.getWidth() * 2) / 3, (decodeResource2.getHeight() * 2) / 3, true);
            case 240:
                return BitmapFactory.decodeResource(this.x.getResources(), i2);
            default:
                return BitmapFactory.decodeResource(this.x.getResources(), i2);
        }
    }

    public abstract void a();

    public void a(float f2) {
        if (isVisible()) {
            if (!this.q) {
                this.n += this.r * f2;
            }
            this.f78a += this.m * f2;
            this.b += this.n * f2;
            if (this.f78a < this.A) {
                setVisible(false, false);
                a();
            }
            if (this.f78a > this.z) {
                setVisible(false, false);
                a();
            }
            if (this.b < this.B) {
                setVisible(false, false);
                a();
            }
            if (this.b > this.C) {
                setVisible(false, false);
                a();
            }
            this.p += this.o * f2;
        }
    }

    public abstract void a(boolean z2);

    public final void b(float f2) {
        this.o = f2 / 1000.0f;
        this.p = 0.0f;
    }

    public void draw(Canvas canvas) {
        if (isVisible()) {
            canvas.save();
            canvas.rotate(this.p, this.f78a, this.b);
            if (!this.q) {
                canvas.drawBitmap(this.e, this.f78a - ((float) this.g), this.b - ((float) this.h), (Paint) null);
            } else {
                canvas.drawBitmap(this.d, this.f78a - ((float) this.g), this.b - ((float) this.h), (Paint) null);
            }
            canvas.restore();
        }
    }

    public int getOpacity() {
        return 0;
    }

    public void setAlpha(int i2) {
    }

    public void setColorFilter(ColorFilter colorFilter) {
    }
}
