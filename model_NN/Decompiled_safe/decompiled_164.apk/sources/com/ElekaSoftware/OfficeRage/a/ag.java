package com.ElekaSoftware.OfficeRage.a;

import android.content.Context;
import com.ElekaSoftware.OfficeRage.C0000R;
import com.ElekaSoftware.OfficeRage.h;

public final class ag extends l {
    public ag(Context context, h hVar) {
        super(context, hVar);
        this.d = a((int) C0000R.drawable.gameitem_fan);
        this.e = a((int) C0000R.drawable.gameitem_fan);
        this.f = 1;
        this.g = this.d.getWidth() / 2;
        this.h = this.d.getHeight() / 2;
        this.i = "fan";
        this.j = 300;
        this.q = true;
        this.c = 20;
        this.s = 0;
        this.t = 3;
        this.u = C0000R.drawable.score_fan;
    }

    public final void a() {
        this.q = true;
    }

    public final void a(boolean z) {
        this.q = false;
        this.m /= 5.0f;
        if (z) {
            this.v.post(new ax(this));
        } else {
            this.v.post(new aw(this));
        }
    }
}
