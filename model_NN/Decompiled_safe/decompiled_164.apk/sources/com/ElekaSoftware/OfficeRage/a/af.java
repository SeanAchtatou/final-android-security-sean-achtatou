package com.ElekaSoftware.OfficeRage.a;

import android.content.Context;
import com.ElekaSoftware.OfficeRage.C0000R;
import com.ElekaSoftware.OfficeRage.h;

public final class af extends l {
    public af(Context context, h hVar) {
        super(context, hVar);
        this.d = a((int) C0000R.drawable.gameitem_printer);
        this.e = a((int) C0000R.drawable.gameitem_printer_broken);
        this.f = 1;
        this.g = this.d.getWidth() / 2;
        this.h = this.d.getHeight() / 2;
        this.i = "printer";
        this.j = 550;
        this.q = true;
        this.c = 14;
        this.s = 1;
        this.t = 3;
        this.u = C0000R.drawable.score_printer;
    }

    public final void a() {
        this.q = true;
    }

    public final void a(boolean z) {
        this.q = false;
        this.m /= 5.0f;
        if (z) {
            this.v.post(new ab(this));
        } else {
            this.v.post(new aa(this));
        }
    }
}
