package com.ElekaSoftware.OfficeRage.a;

import android.content.Context;
import com.ElekaSoftware.OfficeRage.C0000R;

public final class h extends l {
    public h(Context context, com.ElekaSoftware.OfficeRage.h hVar) {
        super(context, hVar);
        this.d = a((int) C0000R.drawable.gameitem_smartphone);
        this.e = a((int) C0000R.drawable.gameitem_smartphone_broken);
        this.f = 1;
        this.g = this.d.getWidth() / 2;
        this.h = this.d.getHeight() / 2;
        this.j = 350;
        this.i = "smartphone";
        this.q = true;
        this.c = 7;
        this.u = C0000R.drawable.score_smartphone;
    }

    public final void a() {
    }

    public final void a(boolean z) {
        this.q = false;
        this.m /= 5.0f;
        if (z) {
            this.v.post(new bb(this));
        } else {
            this.v.post(new ba(this));
        }
    }
}
