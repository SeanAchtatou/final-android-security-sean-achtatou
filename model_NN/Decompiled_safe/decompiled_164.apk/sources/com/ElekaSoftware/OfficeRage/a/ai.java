package com.ElekaSoftware.OfficeRage.a;

import android.content.Context;
import com.ElekaSoftware.OfficeRage.C0000R;
import com.ElekaSoftware.OfficeRage.h;

public final class ai extends l {
    public ai(Context context, h hVar) {
        super(context, hVar);
        this.d = a((int) C0000R.drawable.gameitem_calculator);
        this.e = a((int) C0000R.drawable.gameitem_calculator);
        this.f = 1;
        this.g = this.d.getWidth() / 2;
        this.h = this.d.getHeight() / 2;
        this.i = "calculator";
        this.j = 350;
        this.q = true;
        this.c = 15;
        this.s = 0;
        this.t = 3;
        this.u = C0000R.drawable.score_calculator;
    }

    public final void a() {
        this.q = true;
    }

    public final void a(boolean z) {
        this.q = false;
        this.m /= 5.0f;
        if (z) {
            this.v.post(new k(this));
        } else {
            this.v.post(new j(this));
        }
    }
}
