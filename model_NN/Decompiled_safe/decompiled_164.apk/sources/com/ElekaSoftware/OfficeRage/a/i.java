package com.ElekaSoftware.OfficeRage.a;

import android.content.Context;
import com.ElekaSoftware.OfficeRage.C0000R;
import com.ElekaSoftware.OfficeRage.h;

public final class i extends l {
    private int w = -100;

    public i(Context context, h hVar) {
        super(context, hVar);
        this.d = a((int) C0000R.drawable.gameitem_cactus);
        this.e = a((int) C0000R.drawable.gameitem_cactus);
        this.f = 3;
        this.g = this.d.getWidth() / 2;
        this.h = this.d.getHeight() / 2;
        this.i = "cactus";
        this.j = -150;
        this.q = true;
        this.c = 1;
        this.s = 1;
        this.u = C0000R.drawable.score_cactus;
    }

    public final void a() {
        this.q = true;
    }

    public final void a(boolean z) {
        this.m /= 5.0f;
        this.q = false;
        this.v.post(new aj(this));
    }
}
