package com.ElekaSoftware.OfficeRage.a;

import android.content.Context;
import com.ElekaSoftware.OfficeRage.C0000R;
import com.ElekaSoftware.OfficeRage.h;

public final class f extends l {
    public f(Context context, h hVar) {
        super(context, hVar);
        this.d = a((int) C0000R.drawable.gameitem_crt);
        this.e = a((int) C0000R.drawable.gameitem_crt_broken);
        this.f = 1;
        this.g = this.d.getWidth() / 2;
        this.h = this.d.getHeight() / 2;
        this.j = 350;
        this.i = "computer";
        this.q = true;
        this.c = 0;
        this.s = 2;
        this.t = 1;
        this.u = C0000R.drawable.score_crt;
    }

    public final void a() {
        this.q = true;
    }

    public final void a(boolean z) {
        this.m /= 5.0f;
        this.q = false;
        if (z) {
            this.v.post(new ad(this));
        } else {
            this.v.post(new ac(this));
        }
    }
}
