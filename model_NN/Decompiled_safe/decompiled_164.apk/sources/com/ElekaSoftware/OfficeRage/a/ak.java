package com.ElekaSoftware.OfficeRage.a;

import android.content.Context;
import com.ElekaSoftware.OfficeRage.C0000R;
import com.ElekaSoftware.OfficeRage.h;

public final class ak extends l {
    public ak(Context context, h hVar) {
        super(context, hVar);
        this.d = a((int) C0000R.drawable.gameitem_mushroom);
        this.e = a((int) C0000R.drawable.gameitem_mushroom);
        this.f = 4;
        this.g = this.d.getWidth() / 2;
        this.h = this.d.getHeight() / 2;
        this.i = "mushroom";
        this.q = true;
        this.c = 3;
        this.l = 5;
        this.u = C0000R.drawable.score_mushroom;
    }

    public final void a() {
        this.q = true;
    }

    public final void a(boolean z) {
        this.q = false;
        this.v.post(new at(this));
        setVisible(false, false);
    }
}
