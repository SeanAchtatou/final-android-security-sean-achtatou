package com.ElekaSoftware.OfficeRage.a;

import android.content.Context;
import com.ElekaSoftware.OfficeRage.C0000R;
import com.ElekaSoftware.OfficeRage.h;

public final class ah extends l {
    public ah(Context context, h hVar) {
        super(context, hVar);
        this.d = a((int) C0000R.drawable.gameitem_briefcase);
        this.e = a((int) C0000R.drawable.gameitem_briefcase);
        this.f = 1;
        this.g = this.d.getWidth() / 2;
        this.h = this.d.getHeight() / 2;
        this.i = "briefcase";
        this.j = 750;
        this.q = true;
        this.c = 17;
        this.s = 4;
        this.t = 8;
        this.u = C0000R.drawable.score_briefcase;
    }

    public final void a() {
        this.q = true;
    }

    public final void a(boolean z) {
        this.q = false;
        this.m /= 5.0f;
        if (z) {
            this.v.post(new au(this));
        } else {
            this.v.post(new av(this));
        }
    }
}
