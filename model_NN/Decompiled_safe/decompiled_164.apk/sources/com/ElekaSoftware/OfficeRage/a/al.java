package com.ElekaSoftware.OfficeRage.a;

import android.content.Context;
import com.ElekaSoftware.OfficeRage.C0000R;
import com.ElekaSoftware.OfficeRage.h;

public final class al extends l {
    public al(Context context, h hVar) {
        super(context, hVar);
        this.d = a((int) C0000R.drawable.gameitem_anvil);
        this.e = a((int) C0000R.drawable.gameitem_anvil);
        this.f = 3;
        this.g = this.d.getWidth() / 2;
        this.h = this.d.getHeight() / 2;
        this.i = "anvil";
        this.q = true;
        this.c = 5;
        this.j = -400;
        this.s = 0;
        this.t = 5;
        this.u = C0000R.drawable.score_anvil;
    }

    public final void a() {
        this.q = true;
    }

    public final void a(boolean z) {
        this.q = false;
        this.v.post(new o(this));
    }
}
