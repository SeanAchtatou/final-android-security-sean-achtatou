package com.ElekaSoftware.OfficeRage.a;

import android.content.Context;
import com.ElekaSoftware.OfficeRage.C0000R;
import com.ElekaSoftware.OfficeRage.h;

public final class ao extends l {
    public ao(Context context, h hVar) {
        super(context, hVar);
        this.d = a((int) C0000R.drawable.gameitem_lcd);
        this.e = a((int) C0000R.drawable.gameitem_lcd_broken);
        this.f = 1;
        this.g = this.d.getWidth() / 2;
        this.h = this.d.getHeight() / 2;
        this.i = "lcd";
        this.j = 500;
        this.q = true;
        this.c = 13;
        this.s = 2;
        this.t = 2;
        this.u = C0000R.drawable.score_lcd;
    }

    public final void a() {
        this.q = true;
    }

    public final void a(boolean z) {
        this.q = false;
        this.m /= 5.0f;
        if (z) {
            this.v.post(new z(this));
        } else {
            this.v.post(new y(this));
        }
    }
}
