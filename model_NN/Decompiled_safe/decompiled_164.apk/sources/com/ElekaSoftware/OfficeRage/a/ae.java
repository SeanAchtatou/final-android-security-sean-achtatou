package com.ElekaSoftware.OfficeRage.a;

import android.content.Context;
import com.ElekaSoftware.OfficeRage.C0000R;
import com.ElekaSoftware.OfficeRage.h;

public final class ae extends l {
    public ae(Context context, h hVar) {
        super(context, hVar);
        this.d = a((int) C0000R.drawable.gameitem_keyboard);
        this.e = a((int) C0000R.drawable.gameitem_keyboard);
        this.f = 1;
        this.j = 150;
        this.g = this.d.getWidth() / 2;
        this.h = this.d.getHeight() / 2;
        this.i = "keyboard";
        this.q = true;
        this.c = 10;
        this.s = 0;
        this.t = 3;
        this.u = C0000R.drawable.score_keyboard;
    }

    public final void a() {
    }

    public final void a(boolean z) {
        this.q = false;
        this.m /= 5.0f;
        if (z) {
            this.v.post(new t(this));
        } else {
            this.v.post(new u(this));
        }
    }
}
