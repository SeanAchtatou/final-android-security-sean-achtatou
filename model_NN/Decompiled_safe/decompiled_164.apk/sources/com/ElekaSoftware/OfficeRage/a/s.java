package com.ElekaSoftware.OfficeRage.a;

import android.content.Context;
import com.ElekaSoftware.OfficeRage.C0000R;
import com.ElekaSoftware.OfficeRage.h;

public final class s extends l {
    public s(Context context, h hVar) {
        super(context, hVar);
        this.d = a((int) C0000R.drawable.gameitem_coffeemug);
        this.e = a((int) C0000R.drawable.gameitem_coffeemug_broken);
        this.f = 1;
        this.g = this.d.getWidth() / 2;
        this.h = this.d.getHeight() / 2;
        this.j = 30;
        this.i = "coffeemug";
        this.q = true;
        this.c = 6;
        this.s = 2;
        this.t = 2;
        this.u = C0000R.drawable.score_coffeemug;
    }

    public final void a() {
    }

    public final void a(boolean z) {
        this.q = false;
        this.m /= 3.0f;
        if (z) {
            this.v.post(new b(this));
        } else {
            this.v.post(new a(this));
        }
    }
}
