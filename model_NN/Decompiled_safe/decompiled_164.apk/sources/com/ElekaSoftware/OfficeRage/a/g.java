package com.ElekaSoftware.OfficeRage.a;

import android.content.Context;
import com.ElekaSoftware.OfficeRage.C0000R;
import com.ElekaSoftware.OfficeRage.h;

public final class g extends l {
    public g(Context context, h hVar) {
        super(context, hVar);
        this.d = a((int) C0000R.drawable.gameitem_box);
        this.e = a((int) C0000R.drawable.gameitem_box);
        this.f = 1;
        this.g = this.d.getWidth() / 2;
        this.h = this.d.getHeight() / 2;
        this.i = "box";
        this.j = 50;
        this.q = true;
        this.c = 11;
        this.s = 0;
        this.t = 3;
        this.u = C0000R.drawable.score_box;
    }

    public final void a() {
        this.q = true;
    }

    public final void a(boolean z) {
        this.q = false;
        this.m /= 5.0f;
        if (z) {
            this.v.post(new ap(this));
        } else {
            this.v.post(new aq(this));
        }
    }
}
