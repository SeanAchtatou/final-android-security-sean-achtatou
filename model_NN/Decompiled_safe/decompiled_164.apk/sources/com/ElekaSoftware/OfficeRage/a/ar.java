package com.ElekaSoftware.OfficeRage.a;

import android.content.Context;
import com.ElekaSoftware.OfficeRage.C0000R;
import com.ElekaSoftware.OfficeRage.h;

public final class ar extends l {
    public ar(Context context, h hVar) {
        super(context, hVar);
        this.d = a((int) C0000R.drawable.gameitem_coffeemaker);
        this.e = a((int) C0000R.drawable.gameitem_coffeemaker);
        this.f = 1;
        this.g = this.d.getWidth() / 2;
        this.h = this.d.getHeight() / 2;
        this.i = "coffeemaker";
        this.j = 300;
        this.q = true;
        this.c = 18;
        this.s = 2;
        this.t = 0;
        this.u = C0000R.drawable.score_coffeemaker;
    }

    public final void a() {
        this.q = true;
    }

    public final void a(boolean z) {
        this.q = false;
        this.m /= 5.0f;
        if (z) {
            this.v.post(new v(this));
        } else {
            this.v.post(new w(this));
        }
    }
}
