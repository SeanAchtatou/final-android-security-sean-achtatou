package com.ElekaSoftware.OfficeRage.a;

import android.content.Context;
import com.ElekaSoftware.OfficeRage.C0000R;
import com.ElekaSoftware.OfficeRage.h;

public final class n extends l {
    public n(Context context, h hVar) {
        super(context, hVar);
        this.d = a((int) C0000R.drawable.gameitem_cigarbox);
        this.e = a((int) C0000R.drawable.gameitem_cigarbox_broken);
        this.f = 1;
        this.g = this.d.getWidth() / 2;
        this.h = this.d.getHeight() / 2;
        this.i = "cigarbox";
        this.j = 600;
        this.q = true;
        this.c = 19;
        this.s = 3;
        this.t = 3;
        this.u = C0000R.drawable.score_cigarbox;
    }

    public final void a() {
        this.q = true;
    }

    public final void a(boolean z) {
        this.q = false;
        this.m /= 5.0f;
        if (z) {
            this.v.post(new r(this));
        } else {
            this.v.post(new q(this));
        }
    }
}
