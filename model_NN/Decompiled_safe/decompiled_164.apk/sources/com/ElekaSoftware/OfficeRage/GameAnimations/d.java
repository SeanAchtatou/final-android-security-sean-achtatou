package com.ElekaSoftware.OfficeRage.GameAnimations;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.Pair;
import android.view.SurfaceHolder;

public final class d extends Thread {

    /* renamed from: a  reason: collision with root package name */
    private e f41a;
    private boolean b;
    private SurfaceHolder c;
    private int d;
    private int e;
    private long f = 0;
    private int g = 0;
    private Bitmap h;
    private String i;
    private Paint j;
    private int k;
    private int l;
    private float m;
    private float n;
    private Paint o;
    private Rect p;
    private boolean q;
    private int r;
    private String s = null;
    private Paint t;
    private int u;
    private int v;
    private o w;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Paint.setShadowLayer(float, float, float, int):void}
     arg types: [int, int, int, int]
     candidates:
      ClspMth{android.graphics.Paint.setShadowLayer(float, float, float, long):void}
      ClspMth{android.graphics.Paint.setShadowLayer(float, float, float, int):void} */
    public d(e eVar, SurfaceHolder surfaceHolder, int i2, int i3, int i4) {
        this.c = surfaceHolder;
        this.f41a = eVar;
        this.d = i2;
        this.e = i3;
        this.j = new Paint();
        this.j.setColor(-1);
        this.j.setAntiAlias(true);
        this.j.setTextSize((float) (i3 / 15));
        this.k = 255;
        this.l = -5;
        switch (i4) {
            case 1:
                this.w = new k(eVar.getContext(), i2, i3);
                break;
            case 2:
                this.w = new i(eVar.getContext(), i2, i3);
                break;
            case 4:
                this.w = new j(eVar.getContext(), i2, i3);
                break;
            case 6:
                this.w = new g(eVar.getContext(), i2, i3);
                break;
            case 8:
                this.w = new h(eVar.getContext(), i2, i3);
                break;
        }
        if (this.w.n) {
            this.h = a(((Integer) ((Pair) this.w.i.get(0)).second).intValue());
            this.w.i.remove(0);
        }
        this.o = new Paint();
        this.o.setColor(-16777216);
        this.o.setAlpha(0);
        this.p = new Rect(0, 0, i2, i3);
        this.q = false;
        if (i4 == 8) {
            this.i = "";
        } else {
            this.i = "Touch to skip...";
        }
        this.m = ((float) i2) - (this.j.measureText(this.i) * 1.1f);
        this.n = (float) ((i3 / 12) * 11);
        this.t = new Paint();
        this.t.setTextSize((float) (i2 / 16));
        this.t.setTextAlign(Paint.Align.CENTER);
        this.t.setColor(-1);
        this.t.setDither(true);
        this.t.setAntiAlias(true);
        this.t.setShadowLayer(2.0f, 1.0f, 1.0f, -16777216);
        this.u = i2 / 2;
        this.v = (i3 / 20) * 19;
    }

    private Bitmap a(int i2) {
        Bitmap createBitmap = Bitmap.createBitmap(this.d, this.e, Bitmap.Config.RGB_565);
        Bitmap decodeResource = BitmapFactory.decodeResource(this.f41a.getResources(), i2);
        Canvas canvas = new Canvas(createBitmap);
        float f2 = ((float) this.d) / ((float) this.e);
        float width = ((float) decodeResource.getWidth()) / ((float) decodeResource.getHeight());
        Rect rect = new Rect();
        if (f2 >= width) {
            rect.set(0, 0, decodeResource.getWidth(), (int) ((((float) decodeResource.getWidth()) / ((float) this.d)) * ((float) this.e)));
        } else {
            float height = (((float) decodeResource.getHeight()) / ((float) this.e)) * ((float) this.d);
            int width2 = (decodeResource.getWidth() - ((int) height)) / 2;
            rect.set(width2, 0, ((int) height) + width2, decodeResource.getHeight());
        }
        Paint paint = new Paint();
        paint.setFilterBitmap(true);
        paint.setAntiAlias(true);
        paint.setDither(false);
        canvas.drawBitmap(decodeResource, rect, new Rect(0, 0, this.d, this.e), paint);
        return createBitmap;
    }

    public final void a(boolean z) {
        this.b = z;
    }

    public final void run() {
        Canvas canvas;
        Throwable th;
        this.f = System.currentTimeMillis();
        while (this.b) {
            try {
                canvas = this.c.lockCanvas();
                try {
                    synchronized (this.c) {
                        this.g = (int) (System.currentTimeMillis() - this.f);
                        if (this.g < this.w.f45a) {
                            this.k += this.l;
                            if (this.k < 0) {
                                this.k = 0;
                                this.l *= -1;
                            }
                            if (this.k > 255) {
                                this.k = 255;
                                this.l *= -1;
                            }
                            this.j.setAlpha(this.k);
                        } else {
                            this.i = "Touch to continue...";
                            this.m = ((float) this.d) - (this.j.measureText(this.i) * 1.1f);
                            this.j.setAlpha(255);
                        }
                        for (int i2 = 0; i2 < this.w.b.size(); i2++) {
                            if (((c) this.w.b.get(i2)).b + ((c) this.w.b.get(i2)).f40a < this.g) {
                                this.w.b.remove(i2);
                            }
                        }
                        for (int i3 = 0; i3 < this.w.b.size(); i3++) {
                            if (((c) this.w.b.get(i3)).c) {
                                ((c) this.w.b.get(i3)).setAlpha(5);
                            } else if (((c) this.w.b.get(i3)).f40a < this.g) {
                                ((c) this.w.b.get(i3)).c = true;
                            }
                        }
                        for (int i4 = 0; i4 < this.w.h.size(); i4++) {
                            if (((Integer) ((Pair) this.w.h.get(i4)).first).intValue() < this.g) {
                                o oVar = this.w;
                                switch (((Integer) ((Pair) this.w.h.get(i4)).second).intValue()) {
                                    case 1:
                                        oVar.e.m = true;
                                        break;
                                    case 2:
                                        oVar.e.m = false;
                                        break;
                                    case 3:
                                        oVar.e.a(true);
                                        break;
                                    case 4:
                                        oVar.e.a(false);
                                        break;
                                    case 5:
                                        oVar.f.m = true;
                                        break;
                                    case 6:
                                        oVar.f.m = false;
                                        break;
                                    case 7:
                                        oVar.f.a(true);
                                        break;
                                    case 8:
                                        oVar.f.a(false);
                                        break;
                                    case 9:
                                        oVar.f.b(false);
                                        break;
                                    case 10:
                                        oVar.f.b(true);
                                        break;
                                    case 11:
                                        oVar.g.m = true;
                                        break;
                                    case 12:
                                        oVar.g.m = false;
                                        break;
                                    case 13:
                                        oVar.g.a(true);
                                        break;
                                    case 14:
                                        oVar.g.a(false);
                                        break;
                                    case 15:
                                        oVar.d.a();
                                        break;
                                    case 16:
                                        oVar.d.a(true);
                                        break;
                                    case 17:
                                        oVar.d.a(false);
                                        break;
                                    case 18:
                                        oVar.d.n = true;
                                        break;
                                    case 19:
                                        oVar.d.n = false;
                                        break;
                                }
                                this.w.h.remove(i4);
                            }
                        }
                        for (int i5 = 0; i5 < this.w.c.size(); i5++) {
                            if (((m) this.w.c.get(i5)).m) {
                                if ((this.g / 100) % 2 == 0) {
                                    ((m) this.w.c.get(i5)).n = true;
                                } else {
                                    ((m) this.w.c.get(i5)).n = false;
                                }
                            }
                        }
                        for (int i6 = 0; i6 < this.w.i.size(); i6++) {
                            int intValue = ((Integer) ((Pair) this.w.i.get(i6)).first).intValue();
                            if (intValue - 500 < this.g) {
                                if (!this.q) {
                                    this.q = true;
                                    this.r = this.g;
                                }
                                if (intValue < this.g) {
                                    this.h = a(((Integer) ((Pair) this.w.i.get(i6)).second).intValue());
                                    this.w.i.remove(i6);
                                }
                            }
                        }
                        if (this.q) {
                            if (this.r + 500 > this.g) {
                                this.o.setAlpha(((this.g - this.r) * 255) / 500);
                            } else if (this.r + 1000 > this.g) {
                                this.o.setAlpha(255 - ((((this.g - this.r) - 500) * 255) / 500));
                            } else {
                                this.q = false;
                            }
                        }
                        for (int i7 = 0; i7 < this.w.j.size(); i7++) {
                            if (((f) this.w.j.get(i7)).f43a < this.g && ((f) this.w.j.get(i7)).c) {
                                ((f) this.w.j.get(i7)).setAlpha(10);
                            }
                        }
                        int i8 = 0;
                        while (true) {
                            if (i8 < this.w.k.size()) {
                                if (((Integer) ((Pair) this.w.k.get(i8)).first).intValue() < this.g) {
                                    this.s = (String) ((Pair) this.w.k.get(i8)).second;
                                    this.w.k.remove(i8);
                                } else {
                                    i8++;
                                }
                            }
                        }
                        if (this.w.n) {
                            canvas.drawBitmap(this.h, 0.0f, 0.0f, (Paint) null);
                        } else {
                            canvas.drawColor(-16777216);
                        }
                        for (int i9 = 0; i9 < this.w.j.size(); i9++) {
                            if (((f) this.w.j.get(i9)).b > 100) {
                                ((f) this.w.j.get(i9)).draw(canvas);
                            }
                        }
                        if (this.s != null) {
                            canvas.drawText(this.s, (float) this.u, (float) this.v, this.t);
                        }
                        for (int i10 = 0; i10 < this.w.c.size(); i10++) {
                            ((m) this.w.c.get(i10)).draw(canvas);
                        }
                        for (int i11 = 0; i11 < this.w.b.size(); i11++) {
                            if (((c) this.w.b.get(i11)).c) {
                                ((c) this.w.b.get(i11)).draw(canvas);
                            }
                        }
                        if (this.q) {
                            canvas.drawRect(this.p, this.o);
                        }
                        canvas.drawText(this.i, this.m, this.n, this.j);
                    }
                    if (canvas != null) {
                        this.c.unlockCanvasAndPost(canvas);
                    }
                } catch (Throwable th2) {
                    th = th2;
                }
            } catch (Throwable th3) {
                Throwable th4 = th3;
                canvas = null;
                th = th4;
            }
        }
        return;
        if (canvas != null) {
            this.c.unlockCanvasAndPost(canvas);
        }
        throw th;
    }
}
