package com.ElekaSoftware.OfficeRage.GameAnimations;

import android.content.Context;
import android.graphics.BitmapFactory;
import com.ElekaSoftware.OfficeRage.C0000R;

public final class l extends m {
    public l(Context context, int i, int i2) {
        this.f44a = (float) i;
        this.b = (float) i2;
        this.e = BitmapFactory.decodeResource(context.getResources(), C0000R.drawable.anim2_police);
        this.f = BitmapFactory.decodeResource(context.getResources(), C0000R.drawable.anim2_police_mouth_open);
        this.g = BitmapFactory.decodeResource(context.getResources(), C0000R.drawable.anim2_police_mouth_closed);
        this.h = BitmapFactory.decodeResource(context.getResources(), C0000R.drawable.anim2_police_eyes_on);
        this.i = BitmapFactory.decodeResource(context.getResources(), C0000R.drawable.anim2_police_eyes_off);
        this.c = this.e.getWidth() / 2;
        this.d = (this.e.getHeight() / 3) * 2;
        this.l = true;
        this.o = true;
    }

    public final void a(boolean z) {
        this.p = z;
    }
}
