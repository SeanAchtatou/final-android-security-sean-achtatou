package com.ElekaSoftware.OfficeRage.GameAnimations;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.NinePatchDrawable;
import com.ElekaSoftware.OfficeRage.C0000R;
import java.util.Random;

public final class c extends Drawable {

    /* renamed from: a  reason: collision with root package name */
    public int f40a;
    public int b;
    public boolean c;
    private String d;
    private int e;
    private int f;
    private Bitmap g;
    private Context h;
    private o i;
    private int j;
    private Paint k;
    private Typeface l;
    private float m = 0.0f;
    private float n = 0.0f;

    public c(o oVar, Context context, int i2, int i3, String str, int i4, int i5) {
        this.i = oVar;
        this.h = context;
        this.f40a = i2;
        this.d = str;
        this.e = i4;
        this.f = i5;
        this.b = i3;
        this.g = a(str);
        this.c = false;
        this.j = 150;
        this.k = new Paint();
        this.k.setAlpha(this.j);
        float f2 = (float) (this.i.l / 40);
        Random random = new Random();
        this.n = (random.nextFloat() * f2) + f2;
        if (this.e == 0) {
            this.m = f2 + (random.nextFloat() * f2);
        } else if (this.e == 1) {
            this.m = (float) ((this.i.l / 2) - (this.g.getWidth() / 2));
        } else {
            this.m = (((float) (this.i.l - this.g.getWidth())) - f2) - (f2 * random.nextFloat());
        }
    }

    private Bitmap a(String str) {
        String[] split = str.split("_");
        Rect[] rectArr = new Rect[split.length];
        int i2 = 0;
        int i3 = this.i.m / 12;
        float f2 = (float) (i3 / 2);
        Paint paint = new Paint();
        paint.setTextAlign(Paint.Align.CENTER);
        paint.setColor(-16777216);
        paint.setTextSize((float) i3);
        paint.setAntiAlias(true);
        this.l = Typeface.createFromAsset(this.h.getResources().getAssets(), "coolvetica.ttf");
        paint.setTypeface(this.l);
        float f3 = 999.0f;
        for (int i4 = 0; i4 < split.length; i4++) {
            Rect rect = new Rect();
            paint.getTextBounds(split[i4], 0, split[i4].length(), rect);
            rectArr[i4] = rect;
            if (((float) rectArr[i4].height()) < f3) {
                f3 = (float) rectArr[i4].height();
            }
        }
        int i5 = 0;
        for (int i6 = 0; i6 < rectArr.length; i6++) {
            if (rectArr[i6].width() > i5) {
                i5 = rectArr[i6].width();
            }
            i2 = (int) (((float) i2) + f3);
        }
        int i7 = (i3 * 2) + i5;
        int length = (int) ((((float) (rectArr.length - 1)) * f2) + ((float) (i2 + (i3 * 2))));
        Bitmap decodeResource = this.f == 1 ? BitmapFactory.decodeResource(this.h.getResources(), C0000R.drawable.bubble_path_left) : BitmapFactory.decodeResource(this.h.getResources(), C0000R.drawable.bubble_path_right);
        Bitmap decodeResource2 = this.f == 1 ? BitmapFactory.decodeResource(this.h.getResources(), C0000R.drawable.bubble_fill_left) : BitmapFactory.decodeResource(this.h.getResources(), C0000R.drawable.bubble_fill_right);
        Bitmap createBitmap = Bitmap.createBitmap(i7, ((decodeResource.getHeight() / 4) * 3) + length, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(createBitmap);
        canvas.drawBitmap(decodeResource, (float) ((i7 / 2) - (decodeResource.getWidth() / 2)), (float) (length - (decodeResource.getHeight() / 4)), (Paint) null);
        NinePatchDrawable ninePatchDrawable = (NinePatchDrawable) this.h.getResources().getDrawable(C0000R.drawable.bubble);
        ninePatchDrawable.setBounds(0, 0, i7, length);
        ninePatchDrawable.draw(canvas);
        canvas.drawBitmap(decodeResource2, (float) ((i7 / 2) - (decodeResource.getWidth() / 2)), (float) (length - (decodeResource.getHeight() / 4)), (Paint) null);
        int i8 = i3;
        for (String drawText : split) {
            int i9 = (int) (((float) i8) + f3);
            canvas.drawText(drawText, (float) (i7 / 2), (float) i9, paint);
            i8 = (int) (((float) i9) + f2);
        }
        return createBitmap;
    }

    public final void draw(Canvas canvas) {
        canvas.drawBitmap(this.g, this.m, this.n, this.k);
    }

    public final int getOpacity() {
        return 0;
    }

    public final void setAlpha(int i2) {
        this.j += i2;
        if (this.j > 255) {
            this.j = 255;
        }
        this.k.setAlpha(this.j);
    }

    public final void setColorFilter(ColorFilter colorFilter) {
    }
}
