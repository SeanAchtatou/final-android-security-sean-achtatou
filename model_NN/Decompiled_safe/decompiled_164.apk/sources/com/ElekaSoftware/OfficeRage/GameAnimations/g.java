package com.ElekaSoftware.OfficeRage.GameAnimations;

import android.content.Context;
import android.util.Pair;
import com.ElekaSoftware.OfficeRage.C0000R;

public final class g extends o {
    public g(Context context, int i, int i2) {
        super(context);
        this.l = i;
        this.m = i2;
        this.g = new n(context, this.l / 3, this.m);
        this.c.add(this.g);
        this.f = new b(context, (this.l / 3) * 2, this.m);
        this.c.add(this.f);
        this.i.add(Pair.create(0, Integer.valueOf((int) C0000R.drawable.interview_background)));
        a(2000, 2500, "reporter", "speak");
        a(2000, 3000, "The incident is continuing_here in downtown.", 2, 0);
        a(5000, 2500, "reporter", "speak");
        a(5000, 3000, "I'm here with_the CEO of the company.", 2, 0);
        a(6000, "boss", "lookAway");
        a(8000, "reporter", "lookAway");
        a(8000, 2500, "reporter", "speak");
        a(8000, 3000, "Will this affect_the company's business?", 2, 0);
        a(10500, 3000, "reporter", "armout");
        a(11000, 2500, "boss", "speak");
        a(11000, 3000, "Of course not, we are_a very wealthy company.", 0, 1);
        a(14000, 2500, "reporter", "speak");
        a(14000, 3000, "So the company's insurance_is up-to-date?", 2, 0);
        a(16500, "reporter", "armout");
        a(17000, 2000, "...", 1, 1);
        a(20000, 500, "boss", "speak");
        a(20000, "boss", "lookCamera");
        a(20000, 2000, "Uh-oh...", 1, 1);
        a(23000, "reporter", "armin");
        a(24000, "reporter", "lookcamera");
        a(25000, 1000, "reporter", "lookaway");
        this.f45a = 29000;
    }
}
