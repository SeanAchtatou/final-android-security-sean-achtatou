package com.ElekaSoftware.OfficeRage.GameAnimations;

import android.content.Context;
import android.util.Pair;
import java.util.Vector;

public abstract class o {

    /* renamed from: a  reason: collision with root package name */
    public int f45a = 0;
    public Vector b = new Vector();
    public Vector c = new Vector();
    public a d = null;
    l e = null;
    b f = null;
    n g = null;
    public Vector h = new Vector();
    public Vector i = new Vector();
    public Vector j = new Vector();
    public Vector k = new Vector();
    protected int l;
    protected int m;
    public boolean n = true;
    private Context o;

    public o(Context context) {
        this.o = context;
    }

    /* access modifiers changed from: protected */
    public final void a(int i2, int i3, String str, int i4, int i5) {
        this.b.add(new c(this, this.o, i2, i3, str, i4, i5));
    }

    /* access modifiers changed from: protected */
    public final void a(int i2, int i3, String str, String str2) {
        if (str.equalsIgnoreCase("police")) {
            if (str2.equalsIgnoreCase("speak")) {
                this.h.add(Pair.create(Integer.valueOf(i2), 1));
                if (i3 > 0) {
                    this.h.add(Pair.create(Integer.valueOf(i2 + i3), 2));
                }
            } else if (str2.equalsIgnoreCase("shutUp")) {
                this.h.add(Pair.create(Integer.valueOf(i2), 2));
            } else if (str2.equalsIgnoreCase("lookCamera")) {
                this.h.add(Pair.create(Integer.valueOf(i2), 3));
                if (i3 > 0) {
                    this.h.add(Pair.create(Integer.valueOf(i2 + i3), 4));
                }
            } else if (str2.equalsIgnoreCase("lookAway")) {
                this.h.add(Pair.create(Integer.valueOf(i2), 4));
                if (i3 > 0) {
                    this.h.add(Pair.create(Integer.valueOf(i2 + i3), 3));
                }
            }
        }
        if (str.equalsIgnoreCase("reporter")) {
            if (str2.equalsIgnoreCase("speak")) {
                this.h.add(Pair.create(Integer.valueOf(i2), 5));
                if (i3 > 0) {
                    this.h.add(Pair.create(Integer.valueOf(i2 + i3), 6));
                }
            } else if (str2.equalsIgnoreCase("shutUp")) {
                this.h.add(Pair.create(Integer.valueOf(i2), 6));
            } else if (str2.equalsIgnoreCase("lookCamera")) {
                this.h.add(Pair.create(Integer.valueOf(i2), 7));
                if (i3 > 0) {
                    this.h.add(Pair.create(Integer.valueOf(i2 + i3), 8));
                }
            } else if (str2.equalsIgnoreCase("lookAway")) {
                this.h.add(Pair.create(Integer.valueOf(i2), 8));
                if (i3 > 0) {
                    this.h.add(Pair.create(Integer.valueOf(i2 + i3), 7));
                }
            } else if (str2.equalsIgnoreCase("armIN")) {
                this.h.add(Pair.create(Integer.valueOf(i2), 9));
                if (i3 > 0) {
                    this.h.add(Pair.create(Integer.valueOf(i2 + i3), 10));
                }
            } else if (str2.equalsIgnoreCase("armOUT")) {
                this.h.add(Pair.create(Integer.valueOf(i2), 10));
                if (i3 > 0) {
                    this.h.add(Pair.create(Integer.valueOf(i2 + i3), 9));
                }
            }
        }
        if (str.equalsIgnoreCase("boss")) {
            if (str2.equalsIgnoreCase("speak")) {
                this.h.add(Pair.create(Integer.valueOf(i2), 11));
                if (i3 > 0) {
                    this.h.add(Pair.create(Integer.valueOf(i2 + i3), 12));
                }
            } else if (str2.equalsIgnoreCase("shutUp")) {
                this.h.add(Pair.create(Integer.valueOf(i2), 12));
            } else if (str2.equalsIgnoreCase("lookCamera")) {
                this.h.add(Pair.create(Integer.valueOf(i2), 13));
                if (i3 > 0) {
                    this.h.add(Pair.create(Integer.valueOf(i2 + i3), 14));
                }
            } else if (str2.equalsIgnoreCase("lookAway")) {
                this.h.add(Pair.create(Integer.valueOf(i2), 14));
                if (i3 > 0) {
                    this.h.add(Pair.create(Integer.valueOf(i2 + i3), 13));
                }
            }
        }
        if (!str.equalsIgnoreCase("heppu")) {
            return;
        }
        if (str2.equalsIgnoreCase("changeimage")) {
            this.h.add(Pair.create(Integer.valueOf(i2), 15));
        } else if (str2.equalsIgnoreCase("lookCamera")) {
            this.h.add(Pair.create(Integer.valueOf(i2), 16));
        } else if (str2.equalsIgnoreCase("lookAway")) {
            this.h.add(Pair.create(Integer.valueOf(i2), 17));
        } else if (str2.equalsIgnoreCase("speak")) {
            this.h.add(Pair.create(Integer.valueOf(i2), 18));
        } else if (str2.equalsIgnoreCase("shutUp")) {
            this.h.add(Pair.create(Integer.valueOf(i2), 19));
        }
    }

    /* access modifiers changed from: protected */
    public final void a(int i2, String str, String str2) {
        a(i2, 0, str, str2);
    }
}
