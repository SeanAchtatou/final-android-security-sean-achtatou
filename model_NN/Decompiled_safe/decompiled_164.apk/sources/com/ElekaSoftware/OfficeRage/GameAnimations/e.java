package com.ElekaSoftware.OfficeRage.GameAnimations;

import android.content.Context;
import android.content.Intent;
import android.view.Display;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import com.ElekaSoftware.OfficeRage.GameActivity;

public final class e extends SurfaceView implements SurfaceHolder.Callback, View.OnTouchListener {

    /* renamed from: a  reason: collision with root package name */
    private GameAnimationActivity f42a;
    private int b;
    private int c;
    private int d;
    private d e;

    public e(Context context, GameAnimationActivity gameAnimationActivity, int i) {
        super(context);
        this.f42a = gameAnimationActivity;
        SurfaceHolder holder = getHolder();
        holder.addCallback(this);
        Display defaultDisplay = gameAnimationActivity.getWindowManager().getDefaultDisplay();
        this.b = defaultDisplay.getWidth();
        this.c = defaultDisplay.getHeight();
        this.d = i;
        this.e = new d(this, holder, this.b, this.c, this.d);
        setOnTouchListener(this);
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        GameAnimationActivity gameAnimationActivity = this.f42a;
        if (gameAnimationActivity.f39a == 8) {
            gameAnimationActivity.finish();
            return false;
        }
        Intent intent = new Intent(gameAnimationActivity, GameActivity.class);
        intent.putExtra("level", gameAnimationActivity.f39a);
        gameAnimationActivity.finish();
        gameAnimationActivity.startActivity(intent);
        return false;
    }

    public final void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i2, int i3) {
    }

    public final void surfaceCreated(SurfaceHolder surfaceHolder) {
        this.e.a(true);
        this.e.start();
    }

    public final void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        boolean z = true;
        this.e.a(false);
        while (z) {
            try {
                this.e.join();
                z = false;
            } catch (InterruptedException e2) {
            }
        }
    }
}
