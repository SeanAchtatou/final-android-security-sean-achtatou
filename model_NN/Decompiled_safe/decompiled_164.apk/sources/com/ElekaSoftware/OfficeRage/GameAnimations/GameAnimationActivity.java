package com.ElekaSoftware.OfficeRage.GameAnimations;

import android.app.Activity;
import android.os.Bundle;
import android.os.PowerManager;

public class GameAnimationActivity extends Activity {

    /* renamed from: a  reason: collision with root package name */
    int f39a;
    private PowerManager.WakeLock b;

    public void onBackPressed() {
        finish();
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        this.f39a = getIntent().getExtras().getInt("animation");
        getApplicationContext();
        this.b = ((PowerManager) getSystemService("power")).newWakeLock(26, "GameAnimationActivity");
        e eVar = new e(this, this, this.f39a);
        setContentView(eVar);
        eVar.requestFocus();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.b.release();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.b.acquire();
    }
}
