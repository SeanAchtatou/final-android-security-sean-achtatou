package com.ElekaSoftware.OfficeRage.GameAnimations;

import android.content.Context;
import android.util.Pair;
import com.ElekaSoftware.OfficeRage.C0000R;

public final class k extends o {
    public k(Context context, int i, int i2) {
        super(context);
        this.l = i;
        this.m = i2;
        this.d = new a(context, this.l / 2, this.m);
        this.c.add(this.d);
        this.i.add(Pair.create(0, Integer.valueOf((int) C0000R.drawable.level3_background)));
        a(1000, 2000, "Work harder!", 0, 1);
        a(3000, 2000, "You are worthless!", 2, 0);
        this.i.add(Pair.create(5000, Integer.valueOf((int) C0000R.drawable.level5_background)));
        a(6000, 2000, "You must come_to work on saturday", 0, 1);
        a(8000, 2000, "...and on sunday", 0, 1);
        this.i.add(Pair.create(10000, Integer.valueOf((int) C0000R.drawable.level4_background)));
        a(11000, 2000, "Work, work, work!", 0, 1);
        a(13000, 2000, "... and more work!", 2, 0);
        this.i.add(Pair.create(15000, Integer.valueOf((int) C0000R.drawable.level1_background)));
        a(15000, "heppu", "changeimage");
        a(17000, "heppu", "lookcamera");
        a(19000, "heppu", "speak");
        this.f45a = 25000;
    }
}
