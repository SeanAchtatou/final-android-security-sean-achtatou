package com.ElekaSoftware.OfficeRage.GameAnimations;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;

public final class f extends Drawable {

    /* renamed from: a  reason: collision with root package name */
    public int f43a = 0;
    public int b = 100;
    public boolean c = true;
    private Bitmap d;
    private int e;
    private int f;
    private int g;
    private int h;
    private Paint i;

    public f(Context context, int i2, int i3, int i4, int i5) {
        this.d = BitmapFactory.decodeResource(context.getResources(), i5);
        this.e = i2 / 2;
        this.f = i3 / 2;
        this.g = this.d.getWidth() / 2;
        this.h = this.d.getHeight() / 2;
        this.f43a = i4;
        this.i = new Paint();
        this.i.setAlpha(this.b);
    }

    public final void draw(Canvas canvas) {
        canvas.drawBitmap(this.d, (float) (this.e - this.g), (float) (this.f - this.h), this.i);
    }

    public final int getOpacity() {
        return 0;
    }

    public final void setAlpha(int i2) {
        this.b += i2;
        if (this.b > 255) {
            this.b = 255;
            this.c = false;
        }
        this.i.setAlpha(this.b);
    }

    public final void setColorFilter(ColorFilter colorFilter) {
    }
}
