package com.ElekaSoftware.OfficeRage.GameAnimations;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;

public abstract class m extends Drawable {

    /* renamed from: a  reason: collision with root package name */
    public float f44a = -300.0f;
    public float b = -300.0f;
    protected int c = 0;
    protected int d = 0;
    public Bitmap e;
    public Bitmap f;
    public Bitmap g;
    public Bitmap h;
    public Bitmap i;
    public Bitmap j;
    public Bitmap k;
    public boolean l = false;
    public boolean m = false;
    public boolean n = false;
    protected boolean o = false;
    protected boolean p = true;
    protected boolean q = false;
    protected boolean r = false;

    public void draw(Canvas canvas) {
        canvas.drawBitmap(this.e, this.f44a - ((float) this.c), this.b - ((float) this.d), (Paint) null);
        if (this.l) {
            if (!this.m) {
                canvas.drawBitmap(this.g, this.f44a - ((float) this.c), this.b - ((float) this.d), (Paint) null);
            } else if (this.n) {
                canvas.drawBitmap(this.f, this.f44a - ((float) this.c), this.b - ((float) this.d), (Paint) null);
            } else {
                canvas.drawBitmap(this.g, this.f44a - ((float) this.c), this.b - ((float) this.d), (Paint) null);
            }
        }
        if (this.o) {
            if (this.p) {
                canvas.drawBitmap(this.h, this.f44a - ((float) this.c), this.b - ((float) this.d), (Paint) null);
            } else {
                canvas.drawBitmap(this.i, this.f44a - ((float) this.c), this.b - ((float) this.d), (Paint) null);
            }
        }
        if (!this.q) {
            return;
        }
        if (this.r) {
            canvas.drawBitmap(this.j, this.f44a - ((float) this.c), this.b - ((float) this.d), (Paint) null);
        } else {
            canvas.drawBitmap(this.k, this.f44a - ((float) this.c), this.b - ((float) this.d), (Paint) null);
        }
    }

    public int getOpacity() {
        return 0;
    }

    public void setAlpha(int i2) {
    }

    public void setColorFilter(ColorFilter colorFilter) {
    }
}
