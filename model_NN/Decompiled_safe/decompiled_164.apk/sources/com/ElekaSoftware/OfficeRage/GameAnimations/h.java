package com.ElekaSoftware.OfficeRage.GameAnimations;

import android.content.Context;
import android.util.Pair;
import com.ElekaSoftware.OfficeRage.C0000R;

public final class h extends o {
    public h(Context context, int i, int i2) {
        super(context);
        this.l = i;
        this.m = i2;
        this.n = false;
        this.j.add(new f(context, i, i2, 5000, C0000R.drawable.anim_last_1));
        this.j.add(new f(context, i, i2, 10000, C0000R.drawable.anim_last_2));
        this.j.add(new f(context, i, i2, 13500, C0000R.drawable.anim_last_3));
        this.j.add(new f(context, i, i2, 22000, C0000R.drawable.anim_last_4));
        this.k.add(Pair.create(1000, "So, what happened in the end?"));
        this.k.add(Pair.create(3500, ""));
        this.k.add(Pair.create(4500, "Two nice gentlemen escorted me"));
        this.k.add(Pair.create(6500, "out of the building."));
        this.k.add(Pair.create(8500, ""));
        this.k.add(Pair.create(9500, "I took a long vacation."));
        this.k.add(Pair.create(12000, ""));
        this.k.add(Pair.create(13000, "The company went over..."));
        this.k.add(Pair.create(15500, ""));
        this.k.add(Pair.create(16500, "and the boss found"));
        this.k.add(Pair.create(19000, "new challenges"));
        this.k.add(Pair.create(21500, "in restaurant business."));
        this.k.add(Pair.create(24000, ""));
        this.k.add(Pair.create(26000, "Thanks for playing"));
        this.k.add(Pair.create(30000, ""));
        this.f45a = 30000;
    }
}
