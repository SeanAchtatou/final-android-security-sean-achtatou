package com.ElekaSoftware.OfficeRage.GameAnimations;

import android.content.Context;
import android.util.Pair;
import com.ElekaSoftware.OfficeRage.C0000R;

public final class i extends o {
    public i(Context context, int i, int i2) {
        super(context);
        this.l = i;
        this.m = i2;
        this.f = new b(context, this.l / 3, this.m);
        this.c.add(this.f);
        this.i.add(Pair.create(0, Integer.valueOf((int) C0000R.drawable.interview_background)));
        a(2000, 2500, "reporter", "speak");
        a(2000, 3000, "I'm here at the downtown_office block where the police_has surrounded a building.", 2, 1);
        a(5000, 2500, "reporter", "speak");
        a(5000, 3000, "Apparently the basement floor_is completely destroyed.", 2, 1);
        a(8000, 1500, "reporter", "speak");
        a(8000, 2000, "Gas explosion?", 0, 0);
        a(10000, 1500, "reporter", "speak");
        a(10000, 2000, "Act of terrorism?", 0, 0);
        a(12000, 1500, "reporter", "speak");
        a(12000, 2000, "Or something else?", 0, 0);
        a(14000, 1500, "reporter", "speak");
        a(14000, 2000, "Stay tuned for updates.", 0, 0);
        this.f45a = 19000;
    }
}
