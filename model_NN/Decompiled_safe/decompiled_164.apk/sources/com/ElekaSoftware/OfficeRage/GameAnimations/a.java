package com.ElekaSoftware.OfficeRage.GameAnimations;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import com.ElekaSoftware.OfficeRage.C0000R;

public final class a extends m {
    private Bitmap s;
    private Bitmap t;
    private Bitmap u;
    private boolean v = false;
    private Paint w = new Paint();
    private boolean x;
    private float y;

    public a(Context context, int i, int i2) {
        this.f44a = (float) i;
        this.b = (float) i2;
        this.e = BitmapFactory.decodeResource(context.getResources(), C0000R.drawable.anim2_heppu);
        this.s = BitmapFactory.decodeResource(context.getResources(), C0000R.drawable.anim2_heppu_zoom_outline);
        this.t = BitmapFactory.decodeResource(context.getResources(), C0000R.drawable.anim2_heppu_zoom_head);
        this.u = BitmapFactory.decodeResource(context.getResources(), C0000R.drawable.anim2_heppu_zoom_head_redfilter);
        this.g = BitmapFactory.decodeResource(context.getResources(), C0000R.drawable.anim2_heppu_zoom_mouth_closed);
        this.f = BitmapFactory.decodeResource(context.getResources(), C0000R.drawable.anim2_heppu_zoom_mouth_open);
        this.i = BitmapFactory.decodeResource(context.getResources(), C0000R.drawable.anim2_heppu_zoom_eyesoff);
        this.h = BitmapFactory.decodeResource(context.getResources(), C0000R.drawable.anim2_heppu_zoom_eyeson);
        this.c = this.e.getWidth() / 2;
        this.d = (this.e.getHeight() / 5) * 4;
        this.n = false;
        this.p = false;
        this.w.setAlpha(0);
        this.x = true;
        this.y = 0.0f;
    }

    public final void a() {
        this.c = this.s.getWidth() / 2;
        this.d = (this.s.getHeight() / 11) * 10;
        this.v = true;
    }

    public final void a(boolean z) {
        this.p = z;
    }

    public final void draw(Canvas canvas) {
        if (this.v) {
            canvas.drawBitmap(this.s, this.f44a - ((float) this.c), this.b - ((float) this.d), (Paint) null);
            canvas.drawBitmap(this.t, this.f44a - ((float) this.c), this.b - ((float) this.d), (Paint) null);
            if (this.x) {
                this.y += 0.7f;
                if (this.y < 200.0f) {
                    this.w.setAlpha((int) this.y);
                } else {
                    this.x = false;
                }
            } else {
                this.y -= 0.2f;
                if (this.y > 70.0f) {
                    this.w.setAlpha((int) this.y);
                }
            }
            canvas.drawBitmap(this.u, this.f44a - ((float) this.c), this.b - ((float) this.d), this.w);
            if (this.p) {
                canvas.drawBitmap(this.h, this.f44a - ((float) this.c), this.b - ((float) this.d), (Paint) null);
            } else {
                canvas.drawBitmap(this.i, this.f44a - ((float) this.c), this.b - ((float) this.d), (Paint) null);
            }
            if (this.n) {
                canvas.drawBitmap(this.f, this.f44a - ((float) this.c), this.b - ((float) this.d), (Paint) null);
            } else {
                canvas.drawBitmap(this.g, this.f44a - ((float) this.c), this.b - ((float) this.d), (Paint) null);
            }
        } else {
            canvas.drawBitmap(this.e, this.f44a - ((float) this.c), this.b - ((float) this.d), (Paint) null);
        }
    }
}
