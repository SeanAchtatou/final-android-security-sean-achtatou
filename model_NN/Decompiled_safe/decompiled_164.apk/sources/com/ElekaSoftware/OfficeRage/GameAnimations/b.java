package com.ElekaSoftware.OfficeRage.GameAnimations;

import android.content.Context;
import android.graphics.BitmapFactory;
import com.ElekaSoftware.OfficeRage.C0000R;

public final class b extends m {
    public b(Context context, int i, int i2) {
        this.f44a = (float) i;
        this.b = (float) i2;
        this.e = BitmapFactory.decodeResource(context.getResources(), C0000R.drawable.anim2_reporter);
        this.f = BitmapFactory.decodeResource(context.getResources(), C0000R.drawable.anim2_reporter_mouth_open);
        this.g = BitmapFactory.decodeResource(context.getResources(), C0000R.drawable.anim2_reporter_mouth_closed);
        this.h = BitmapFactory.decodeResource(context.getResources(), C0000R.drawable.anim2_reporter_eyes_on);
        this.i = BitmapFactory.decodeResource(context.getResources(), C0000R.drawable.anim2_reporter_eyes_off);
        this.j = BitmapFactory.decodeResource(context.getResources(), C0000R.drawable.anim2_reporter_interview_on);
        this.k = BitmapFactory.decodeResource(context.getResources(), C0000R.drawable.anim2_reporter_interview_off);
        this.c = this.e.getWidth() / 2;
        this.d = (this.e.getHeight() / 3) * 2;
        this.l = true;
        this.o = true;
        this.q = true;
    }

    public final void a(boolean z) {
        this.p = z;
    }

    public final void b(boolean z) {
        this.r = z;
    }
}
