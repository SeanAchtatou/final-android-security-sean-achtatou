package com.ElekaSoftware.OfficeRage.GameAnimations;

import android.content.Context;
import android.util.Pair;
import com.ElekaSoftware.OfficeRage.C0000R;

public final class j extends o {
    public j(Context context, int i, int i2) {
        super(context);
        this.l = i;
        this.m = i2;
        this.e = new l(context, this.l / 3, this.m);
        this.c.add(this.e);
        this.f = new b(context, (this.l / 3) * 2, this.m);
        this.c.add(this.f);
        this.i.add(Pair.create(0, Integer.valueOf((int) C0000R.drawable.interview_background)));
        a(1500, "police", "lookaway");
        a(2000, 1500, "reporter", "speak");
        a(2000, 2000, "Here with me is_the police commissioner.", 2, 0);
        a(4000, 1500, "reporter", "speak");
        a(4000, 2000, "Tell me what is happening?", 2, 0);
        a(4000, "reporter", "lookAway");
        a(5500, "reporter", "armout");
        a(6000, 2500, "police", "speak");
        a(6000, 3000, "There seems to be_a worker who is_having a hard time.", 1, 1);
        a(9000, 2500, "police", "speak");
        a(9000, 3000, "He is trying to bring_the company down by_destroying property.", 1, 1);
        a(11500, 2000, "reporter", "armin");
        a(12000, 1500, "reporter", "speak");
        a(12000, 2000, "What's next?", 2, 1);
        a(14000, 1500, "police", "speak");
        a(14000, 2000, "We are evacuating_the building right now.", 1, 1);
        a(16000, 1500, "reporter", "speak");
        a(15500, "reporter", "armin");
        a(16000, 2000, "Thank you, police commissioner.", 2, 0);
        a(18500, "police", "lookcamera");
        a(18000, "reporter", "lookcamera");
        this.f45a = 21000;
    }
}
