package com.ElekaSoftware.OfficeRage;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public final class f extends SQLiteOpenHelper {

    /* renamed from: a  reason: collision with root package name */
    private static String f117a = "/data/data/com.ElekaSoftware.OfficeRage/databases/";
    private static String b = "officerage.db.sqlite";
    private SQLiteDatabase c;
    private final Context d;

    public f(Context context) {
        super(context, b, (SQLiteDatabase.CursorFactory) null, 1);
        this.d = context;
    }

    private static boolean d() {
        SQLiteDatabase sQLiteDatabase;
        try {
            sQLiteDatabase = SQLiteDatabase.openDatabase(String.valueOf(f117a) + b, null, 0);
        } catch (SQLiteException e) {
            sQLiteDatabase = null;
        }
        if (sQLiteDatabase == null) {
            return false;
        }
        sQLiteDatabase.close();
        return true;
    }

    public final int a(int i) {
        Cursor rawQuery = this.c.rawQuery("SELECT max(score) FROM scores WHERE level = " + Integer.toString(i) + ";", null);
        rawQuery.moveToFirst();
        return rawQuery.getInt(0);
    }

    public final void a() {
        if (!d()) {
            getReadableDatabase();
            try {
                InputStream open = this.d.getAssets().open(b);
                FileOutputStream fileOutputStream = new FileOutputStream(String.valueOf(f117a) + b);
                byte[] bArr = new byte[1024];
                while (true) {
                    int read = open.read(bArr);
                    if (read <= 0) {
                        fileOutputStream.flush();
                        fileOutputStream.close();
                        open.close();
                        return;
                    }
                    fileOutputStream.write(bArr, 0, read);
                }
            } catch (IOException e) {
                throw new Error("Error copying database");
            }
        }
    }

    public final void a(String str, int i, int i2) {
        this.c.execSQL("INSERT INTO scores VALUES ('" + str + "','" + Integer.toString(i) + "','" + Integer.toString(i2) + "');");
    }

    public final void b() {
        this.c = SQLiteDatabase.openDatabase(String.valueOf(f117a) + b, null, 0);
    }

    public final void b(int i) {
        Cursor rawQuery = this.c.rawQuery("SELECT max(score) FROM scores WHERE level = " + Integer.toString(i) + ";", null);
        rawQuery.moveToFirst();
        this.c.execSQL("DELETE FROM scores WHERE level = " + Integer.toString(i) + " AND NOT score = " + Integer.toString(rawQuery.getInt(0)) + ";");
    }

    public final void c() {
        this.c.execSQL("DELETE FROM scores;");
    }

    public final synchronized void close() {
        if (this.c != null) {
            this.c.close();
        }
        super.close();
    }

    public final void onCreate(SQLiteDatabase sQLiteDatabase) {
    }

    public final void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
    }
}
