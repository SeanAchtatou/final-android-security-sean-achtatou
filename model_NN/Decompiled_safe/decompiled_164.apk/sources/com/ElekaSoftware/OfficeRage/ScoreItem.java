package com.ElekaSoftware.OfficeRage;

import android.os.Parcel;
import android.os.Parcelable;

public class ScoreItem implements Parcelable {
    public static final Parcelable.Creator CREATOR = new ae();

    /* renamed from: a  reason: collision with root package name */
    int f49a;
    int b;
    int c;
    int d;
    int e;
    int f;
    String g;
    boolean h;

    public ScoreItem(int i, int i2, int i3, int i4, int i5, boolean z) {
        this.f49a = i;
        this.b = i2;
        this.c = i3;
        this.d = i4;
        this.e = i5;
        this.f = i2 * i3;
        this.g = "";
        this.h = z;
    }

    public ScoreItem(int i, String str, int i2) {
        this.f49a = i;
        this.e = 3;
        this.g = str;
        this.c = i2;
    }

    /* synthetic */ ScoreItem(Parcel parcel) {
        this(parcel, (byte) 0);
    }

    private ScoreItem(Parcel parcel, byte b2) {
        this.f49a = parcel.readInt();
        this.b = parcel.readInt();
        this.c = parcel.readInt();
        this.d = parcel.readInt();
        this.e = parcel.readInt();
        this.g = parcel.readString();
        this.f = parcel.readInt();
        if (parcel.readInt() == 0) {
            this.h = false;
        } else {
            this.h = true;
        }
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.f49a);
        parcel.writeInt(this.b);
        parcel.writeInt(this.c);
        parcel.writeInt(this.d);
        parcel.writeInt(this.e);
        parcel.writeString(this.g);
        parcel.writeInt(this.f);
        if (!this.h) {
            parcel.writeInt(0);
        } else {
            parcel.writeInt(1);
        }
    }
}
