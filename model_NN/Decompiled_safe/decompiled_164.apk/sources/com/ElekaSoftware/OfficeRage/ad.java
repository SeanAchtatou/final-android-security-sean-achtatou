package com.ElekaSoftware.OfficeRage;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.util.Log;
import android.view.SurfaceHolder;
import com.ElekaSoftware.OfficeRage.a.l;
import com.ElekaSoftware.OfficeRage.c.a;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Iterator;

public final class ad extends Thread {

    /* renamed from: a  reason: collision with root package name */
    protected boolean f91a;
    private boolean b;
    private SurfaceHolder c;
    private long d = 0;
    private int e = 0;
    private int f = 0;
    private int g = 0;
    private Paint h;
    private h i;
    private int j;
    private Bitmap k;
    private Enumeration l;
    private Enumeration m;
    private Collection n;
    private Collection o;
    private Iterator p;
    private Iterator q;
    private int r;
    private int s;
    private Paint t;
    private Typeface u;
    private long v;
    private int w;
    private boolean x;
    private int y;
    private int z;

    public ad(h hVar, SurfaceHolder surfaceHolder, int i2, int i3, int i4) {
        this.c = surfaceHolder;
        this.r = i2;
        this.s = i3;
        this.h = new Paint();
        this.h.setARGB(255, 255, 150, 0);
        this.h.setTextSize((float) (i3 / 10));
        this.u = Typeface.createFromAsset(hVar.getContext().getAssets(), "coolvetica.ttf");
        this.h.setTypeface(this.u);
        this.h.setTextAlign(Paint.Align.CENTER);
        this.i = hVar;
        this.k = a(i4);
        this.t = new Paint();
        this.t.setARGB(120, 120, 120, 120);
        this.y = i2 / 2;
        this.z = (i3 * 9) / 10;
        this.x = false;
        this.f91a = false;
    }

    private Bitmap a(int i2) {
        int i3;
        Bitmap createBitmap = Bitmap.createBitmap(this.r, this.s, Bitmap.Config.RGB_565);
        switch (i2) {
            case 1:
                i3 = C0000R.drawable.level1_background;
                break;
            case 2:
                i3 = C0000R.drawable.level2_background;
                break;
            case 3:
                i3 = C0000R.drawable.level3_background;
                break;
            case 4:
                i3 = C0000R.drawable.level4_background;
                break;
            case 5:
                i3 = C0000R.drawable.level5_background;
                break;
            case 6:
                i3 = C0000R.drawable.level6_background;
                break;
            case 7:
                i3 = C0000R.drawable.level7_background;
                break;
            case 8:
                i3 = C0000R.drawable.level8_background;
                break;
            default:
                Log.d("TAG", "PERKELEEN PERKELE MENI PIELEEN!!!!!!");
                i3 = C0000R.drawable.level1_background;
                break;
        }
        Bitmap decodeResource = BitmapFactory.decodeResource(this.i.getResources(), i3);
        Canvas canvas = new Canvas(createBitmap);
        float f2 = ((float) this.r) / ((float) this.s);
        float width = ((float) decodeResource.getWidth()) / ((float) decodeResource.getHeight());
        Rect rect = new Rect();
        if (f2 >= width) {
            rect.set(0, 0, decodeResource.getWidth(), (int) ((((float) decodeResource.getWidth()) / ((float) this.r)) * ((float) this.s)));
        } else {
            float height = (((float) decodeResource.getHeight()) / ((float) this.s)) * ((float) this.r);
            int width2 = (decodeResource.getWidth() - ((int) height)) / 2;
            rect.set(width2, 0, ((int) height) + width2, decodeResource.getHeight());
        }
        Paint paint = new Paint();
        paint.setFilterBitmap(true);
        paint.setDither(false);
        paint.setAntiAlias(true);
        canvas.drawBitmap(decodeResource, rect, new Rect(0, 0, this.r, this.s), paint);
        Bitmap decodeResource2 = BitmapFactory.decodeResource(this.i.getResources(), C0000R.drawable.hitpoint);
        int width3 = decodeResource2.getWidth() / 2;
        int height2 = decodeResource2.getHeight() / 2;
        for (int i4 = 0; i4 < 6; i4++) {
            canvas.drawBitmap(decodeResource2, (float) (this.i.f119a[i4].x - width3), (float) (this.i.f119a[i4].y - height2), (Paint) null);
        }
        return createBitmap;
    }

    public final void a(boolean z2) {
        this.b = z2;
    }

    public final void b(boolean z2) {
        this.x = z2;
    }

    public final void run() {
        Canvas canvas;
        this.d = System.currentTimeMillis();
        while (this.b) {
            try {
                canvas = this.c.lockCanvas();
                try {
                    synchronized (this.c) {
                        this.n = this.i.c.values();
                        this.o = this.i.d.values();
                        this.j = (int) (System.currentTimeMillis() - this.d);
                        this.i.b.a();
                        this.i.e.b(this.j);
                        this.i.h.a(this.j);
                        this.p = this.n.iterator();
                        this.q = this.o.iterator();
                        while (this.p.hasNext()) {
                            ((l) this.p.next()).a((float) this.j);
                        }
                        while (this.q.hasNext()) {
                            ((a) this.q.next()).a((float) this.j);
                        }
                        this.v = System.currentTimeMillis();
                        if (this.x && this.d != 0) {
                            this.w = (int) (this.v - this.d);
                            this.f += this.w;
                            this.e++;
                            if (this.e == 10) {
                                this.g = 10000 / this.f;
                                this.f = 0;
                                this.e = 0;
                            }
                        }
                        this.d = this.v;
                        canvas.drawBitmap(this.k, 0.0f, 0.0f, (Paint) null);
                        this.i.b.draw(canvas);
                        this.i.f.draw(canvas);
                        this.i.e.draw(canvas);
                        this.p = this.n.iterator();
                        this.q = this.o.iterator();
                        while (this.p.hasNext()) {
                            ((l) this.p.next()).draw(canvas);
                        }
                        while (this.q.hasNext()) {
                            ((a) this.q.next()).draw(canvas);
                        }
                        if (!this.f91a) {
                            this.i.h.draw(canvas);
                        }
                        if (this.x) {
                            canvas.drawText(String.valueOf(this.g) + " fps", (float) this.y, (float) this.z, this.h);
                        }
                    }
                    if (canvas != null) {
                        this.c.unlockCanvasAndPost(canvas);
                    }
                } catch (Throwable th) {
                    th = th;
                }
            } catch (Throwable th2) {
                th = th2;
                canvas = null;
            }
        }
        this.l = null;
        this.m = null;
        this.n.clear();
        this.o.clear();
        this.p = null;
        this.q = null;
        return;
        if (canvas != null) {
            this.c.unlockCanvasAndPost(canvas);
        }
        throw th;
    }
}
