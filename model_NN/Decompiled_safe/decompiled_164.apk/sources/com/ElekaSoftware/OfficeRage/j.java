package com.ElekaSoftware.OfficeRage;

import android.os.AsyncTask;

final class j extends AsyncTask {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ a f121a;

    /* synthetic */ j(a aVar) {
        this(aVar, (byte) 0);
    }

    private j(a aVar, byte b) {
        this.f121a = aVar;
    }

    private Void a() {
        int i = 5;
        while (i >= 0) {
            try {
                publishProgress(Integer.valueOf(i));
                Thread.sleep(1000);
                i--;
            } catch (InterruptedException e) {
                e.printStackTrace();
                return null;
            }
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ Object doInBackground(Object... objArr) {
        return a();
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
        this.f121a.f52a.setEnabled(true);
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void onProgressUpdate(Object... objArr) {
        if (!this.f121a.a(((Integer[]) objArr)[0].intValue())) {
            cancel(true);
        }
    }
}
