package com.ElekaSoftware.OfficeRage;

public final class s extends Thread {

    /* renamed from: a  reason: collision with root package name */
    private ScoreActivity f130a;
    private final int b;
    private boolean c = true;
    private int d = 0;
    private int e = 0;
    private boolean f = false;
    private int g = 0;

    public s(ScoreActivity scoreActivity, int i) {
        this.f130a = scoreActivity;
        this.b = i;
    }

    public final void a() {
        this.c = false;
    }

    public final void run() {
        while (this.c) {
            if (this.d >= this.b) {
                if (!this.f) {
                    this.f = true;
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e2) {
                        e2.printStackTrace();
                    }
                    this.g = this.f130a.f48a.size();
                    if (this.g == 0 && this.f130a.d) {
                        int i = 0;
                        while (true) {
                            if (i >= 3) {
                                break;
                            }
                            try {
                                Thread.sleep(1000);
                            } catch (InterruptedException e3) {
                                e3.printStackTrace();
                            }
                            if (this.f130a.f48a.size() > 0) {
                                this.g = this.f130a.f48a.size();
                                break;
                            }
                            i++;
                        }
                    }
                }
                if (this.e >= this.g) {
                    this.c = false;
                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e4) {
                        e4.printStackTrace();
                    }
                    this.f130a.b();
                } else {
                    this.f130a.a(this.e, true);
                    this.e++;
                }
            } else {
                this.f130a.a(this.d, false);
                this.d++;
            }
            try {
                Thread.sleep(500);
            } catch (InterruptedException e5) {
                this.c = false;
                e5.printStackTrace();
            }
        }
        this.f130a = null;
    }
}
