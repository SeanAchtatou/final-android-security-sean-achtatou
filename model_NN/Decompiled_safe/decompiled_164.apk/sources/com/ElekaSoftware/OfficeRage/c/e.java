package com.ElekaSoftware.OfficeRage.c;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.ColorFilter;
import android.util.FloatMath;
import com.ElekaSoftware.OfficeRage.C0000R;
import com.ElekaSoftware.OfficeRage.a.l;
import com.ElekaSoftware.OfficeRage.h;

public final class e extends a {
    private float r;
    private float s = (((float) this.c.getWidth()) / 100.0f);
    private float t = 1.0f;
    private float u;
    private float v;

    public e(Context context, h hVar, int i) {
        super(hVar, i);
        this.c = BitmapFactory.decodeResource(context.getResources(), C0000R.drawable.effect_paper);
        this.d = "paper";
        this.h = 1;
        this.l = ((float) this.k.i) - this.k.l;
        this.m = this.k.l;
        this.n = this.k.l * 4.0f;
    }

    public final void a(float f) {
        if (isVisible()) {
            this.f += 5.0E-4f * f;
            if (this.f < 0.0f) {
                this.u += this.e * f;
                this.v += this.f * f;
            } else {
                this.r = (float) (((double) this.r) + (((double) f) * 0.004d));
                this.g = -(FloatMath.sin(this.r) * 40.0f);
                this.e *= 0.99f;
                this.u += this.e * f;
                this.v += this.s * f * Math.abs(1.0f - (Math.abs(this.g) / 40.0f));
            }
            if (this.u < this.m) {
                setVisible(false, false);
            }
            if (this.u > this.l) {
                setVisible(false, false);
            }
            if (this.v < this.n) {
                setVisible(false, false);
            }
            if (this.v > this.o) {
                setVisible(false, false);
            }
            this.f102a = this.u + (this.t * FloatMath.cos((this.g + 90.0f) * 0.01745278f));
            this.b = this.v + (this.t * FloatMath.sin((this.g + 90.0f) * 0.01745278f));
        }
    }

    public final void a(l lVar) {
        this.u = lVar.f78a;
        this.v = lVar.b - (((float) this.c.getWidth()) * 1.5f);
        this.t = ((float) this.c.getWidth()) * 1.5f;
        this.f102a = this.u + (this.t * FloatMath.cos((this.g + 90.0f) * 0.01745278f));
        this.b = this.v + (this.t * FloatMath.sin((this.g + 90.0f) * 0.01745278f));
        this.e = ((((float) (this.i.nextInt(2) - 1)) * 0.1f) + this.i.nextFloat()) - 0.5f;
        this.f = (-((float) (this.i.nextInt(4) + 2))) * 0.1f;
        this.s = (this.i.nextFloat() + 0.5f) * 0.4f;
        if (this.j == 160) {
            this.e *= 0.66f;
            this.f *= 0.66f;
            this.s *= 0.66f;
        } else if (this.j == 120) {
            this.e *= 0.5f;
            this.f *= 0.5f;
            this.s *= 0.566f;
        }
        if (this.e > 0.0f) {
            this.g = -40.0f;
            this.r = 1.5707f;
        } else {
            this.g = 40.0f;
            this.r = 4.71238f;
        }
        setVisible(true, true);
    }

    public final int getOpacity() {
        return 0;
    }

    public final void setAlpha(int i) {
    }

    public final void setColorFilter(ColorFilter colorFilter) {
    }
}
