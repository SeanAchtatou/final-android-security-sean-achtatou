package com.ElekaSoftware.OfficeRage.c;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import com.ElekaSoftware.OfficeRage.a.l;
import com.ElekaSoftware.OfficeRage.h;
import java.util.Random;

public abstract class a extends Drawable {

    /* renamed from: a  reason: collision with root package name */
    public float f102a = -300.0f;
    public float b = -300.0f;
    public Bitmap c;
    public String d;
    public float e = 0.0f;
    public float f = 0.0f;
    public float g = 0.0f;
    public int h;
    protected Random i;
    protected int j;
    protected h k;
    protected float l;
    protected float m;
    protected float n;
    protected float o;
    float[] p;
    int q;
    private int r = 0;
    private int s = 0;
    private float t = 0.09f;
    private int u = 0;

    public a(h hVar, int i2) {
        this.k = hVar;
        setVisible(false, false);
        this.j = i2;
        this.i = new Random();
        this.l = ((float) this.k.i) - (this.k.l * 0.5f);
        this.m = this.k.l * 0.5f;
        this.n = this.k.l;
        this.o = ((float) this.k.j) - (this.k.l * 0.5f);
    }

    public final float a() {
        this.u++;
        if (this.u >= this.q) {
            this.u = 0;
        }
        return this.p[this.u];
    }

    public void a(float f2) {
        if (isVisible()) {
            this.f += 0.001f * f2;
            this.f102a += this.e * f2;
            this.b += this.f * f2;
            if (this.f102a < this.m) {
                setVisible(false, false);
            }
            if (this.f102a > this.l) {
                setVisible(false, false);
            }
            if (this.b < this.n) {
                setVisible(false, false);
            }
            if (this.b > this.o) {
                setVisible(false, false);
            }
            this.g += this.t * f2;
        }
    }

    public abstract void a(l lVar);

    public void draw(Canvas canvas) {
        if (isVisible()) {
            canvas.save();
            canvas.rotate(this.g, this.f102a, this.b);
            canvas.drawBitmap(this.c, this.f102a - ((float) this.r), this.b - ((float) this.s), (Paint) null);
            canvas.restore();
        }
    }
}
