package com.ElekaSoftware.OfficeRage.c;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.ColorFilter;
import com.ElekaSoftware.OfficeRage.C0000R;
import com.ElekaSoftware.OfficeRage.a.l;
import com.ElekaSoftware.OfficeRage.h;

public final class c extends a {
    public c(Context context, h hVar, int i) {
        super(hVar, i);
        switch (this.i.nextInt(3)) {
            case 0:
                this.c = BitmapFactory.decodeResource(context.getResources(), C0000R.drawable.effect_fragment1);
                break;
            case 1:
                this.c = BitmapFactory.decodeResource(context.getResources(), C0000R.drawable.effect_fragment2);
                break;
            case 2:
                this.c = BitmapFactory.decodeResource(context.getResources(), C0000R.drawable.effect_fragment3);
                break;
            default:
                this.c = BitmapFactory.decodeResource(context.getResources(), C0000R.drawable.effect_fragment1);
                break;
        }
        this.p = new float[14];
        this.q = this.p.length;
        for (int i2 = 0; i2 < this.q; i2++) {
            this.p[i2] = (((float) (this.i.nextInt(6) - 3)) * 0.15f) + 0.1f;
        }
        this.d = "fragment";
        this.h = 2;
    }

    public final void a(l lVar) {
        this.f102a = lVar.f78a;
        this.b = lVar.b;
        this.e = a();
        this.f = -0.4f;
        if (this.j == 160) {
            this.e *= 0.66f;
            this.f *= 0.66f;
        } else if (this.j == 120) {
            this.e *= 0.5f;
            this.f *= 0.5f;
        }
        setVisible(true, true);
    }

    public final int getOpacity() {
        return 0;
    }

    public final void setAlpha(int i) {
    }

    public final void setColorFilter(ColorFilter colorFilter) {
    }
}
