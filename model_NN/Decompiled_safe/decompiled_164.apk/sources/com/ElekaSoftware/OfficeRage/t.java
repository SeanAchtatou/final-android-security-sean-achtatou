package com.ElekaSoftware.OfficeRage;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.Vector;

public final class t extends BaseAdapter {

    /* renamed from: a  reason: collision with root package name */
    private Vector f131a;
    private Context b;
    private LayoutInflater c = ((LayoutInflater) this.b.getSystemService("layout_inflater"));
    private ScoreItem d;
    private final Typeface e = Typeface.createFromAsset(this.b.getAssets(), "coolvetica.ttf");
    private z f;
    private View g;

    public t(Context context, Vector vector) {
        this.f131a = vector;
        this.b = context.getApplicationContext();
    }

    public final void a() {
        this.f131a.clear();
        this.f131a = null;
        this.b = null;
        this.c = null;
        this.g = null;
        z zVar = this.f;
        zVar.f137a = null;
        zVar.b = null;
        zVar.c = null;
        zVar.d = null;
        zVar.e = null;
        zVar.f = null;
        this.f = null;
    }

    public final int getCount() {
        return this.f131a.size();
    }

    public final Object getItem(int i) {
        return null;
    }

    public final long getItemId(int i) {
        return (long) i;
    }

    public final int getItemViewType(int i) {
        return ((ScoreItem) this.f131a.get(i)).e;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        this.g = view;
        this.f = null;
        this.d = (ScoreItem) this.f131a.get(i);
        if (view != null) {
            this.f = (z) view.getTag();
        } else if (this.d.e == 0) {
            this.g = this.c.inflate((int) C0000R.layout.scorelist_item, (ViewGroup) null);
            this.f = new z();
            this.f.g = 0;
            this.f.f137a = (ImageView) this.g.findViewById(C0000R.id.scoreview_gameitem_image);
            this.f.b = (TextView) this.g.findViewById(C0000R.id.scoreview_gameitem_amount);
            this.f.c = (TextView) this.g.findViewById(C0000R.id.scoreview_gameitem_price);
            this.f.d = (TextView) this.g.findViewById(C0000R.id.scoreview_gameitem_total);
            this.g.setTag(this.f);
        } else if (this.d.e == 1) {
            this.g = this.c.inflate((int) C0000R.layout.scorelist_specialitem, (ViewGroup) null);
            this.f = new z();
            this.f.g = 1;
            this.f.f137a = (ImageView) this.g.findViewById(C0000R.id.scoreview_specialitem_image);
            this.f.b = (TextView) this.g.findViewById(C0000R.id.scoreview_specialitem_amount);
            this.g.setTag(this.f);
        } else if (this.d.e == 2) {
            this.g = this.c.inflate((int) C0000R.layout.scorelist_achievement, (ViewGroup) null);
            this.f = new z();
            this.f.g = 2;
            this.f.f137a = (ImageView) this.g.findViewById(C0000R.id.scoreview_achievement_image);
            this.f.e = (TextView) this.g.findViewById(C0000R.id.scoreview_achievement_text);
            this.g.setTag(this.f);
        } else if (this.d.e == 3) {
            this.g = this.c.inflate((int) C0000R.layout.scorelist_textfield, (ViewGroup) null);
            this.f = new z();
            this.f.g = 3;
            this.f.e = (TextView) this.g.findViewById(C0000R.id.scoreview_textfield_text);
            this.f.f = (TextView) this.g.findViewById(C0000R.id.scoreview_textfield_score);
            this.g.setTag(this.f);
        }
        if (this.f.g == 0) {
            this.f.f137a.setImageBitmap(BitmapFactory.decodeResource(this.b.getResources(), ((ScoreItem) this.f131a.get(i)).d));
            this.f.b.setText(String.valueOf(Integer.toString(this.d.b)) + "x");
            this.f.b.setTypeface(this.e);
            this.f.b.setTextColor(Color.argb(255, 255, 161, 24));
            this.f.c.setText(Integer.toString(this.d.c));
            this.f.c.setTypeface(this.e);
            this.f.c.setTextColor(Color.argb(255, 255, 161, 24));
            this.f.d.setText(Integer.toString(this.d.f));
            this.f.d.setTypeface(this.e);
            this.f.d.setTextColor(Color.argb(255, 255, 161, 24));
        } else if (this.f.g == 1) {
            this.f.f137a.setImageBitmap(BitmapFactory.decodeResource(this.b.getResources(), ((ScoreItem) this.f131a.get(i)).d));
            this.f.b.setText(String.valueOf(Integer.toString(this.d.b)) + "x");
            this.f.b.setTypeface(this.e);
            this.f.b.setTextColor(Color.argb(255, 255, 161, 24));
        } else if (this.f.g == 2) {
            this.f.f137a.setImageBitmap(BitmapFactory.decodeResource(this.b.getResources(), ((ScoreItem) this.f131a.get(i)).d));
            this.f.e.setText(this.d.g);
            this.f.e.setTypeface(this.e);
            this.f.e.setTextColor(Color.argb(255, 255, 161, 24));
        } else if (this.f.g == 3) {
            this.f.e.setText(this.d.g);
            this.f.e.setTypeface(this.e);
            this.f.e.setTextColor(Color.argb(255, 255, 161, 24));
            if (this.d.c == -1) {
                this.f.f.setText("");
            } else {
                this.f.f.setText(Integer.toString(this.d.c));
            }
            this.f.f.setTypeface(this.e);
            this.f.f.setTextColor(Color.argb(255, 255, 161, 24));
        }
        return this.g;
    }

    public final int getViewTypeCount() {
        return 4;
    }
}
