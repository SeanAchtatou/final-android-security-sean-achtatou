package com.ElekaSoftware.OfficeRage;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;

public final class af extends Drawable {

    /* renamed from: a  reason: collision with root package name */
    protected int f92a;
    public int b = 0;
    private final int c;
    private final int d;
    private final int e;
    private q[] f;
    private final int g;
    private int h;
    private final Bitmap i;
    private int j = 0;
    private int k = 0;
    private int[] l;
    private final Paint m;
    private final int n;
    private final int o;
    private final Bitmap p;
    private final Bitmap q;
    private final Bitmap r;
    private final Bitmap s;
    private final Bitmap t;
    private final Bitmap u;
    private Bitmap v;
    private h w;

    public af(h hVar, Context context, int i2) {
        this.w = hVar;
        this.g = 6;
        this.f92a = 0;
        this.i = BitmapFactory.decodeResource(context.getResources(), C0000R.drawable.scoreroller_background);
        this.c = BitmapFactory.decodeResource(context.getResources(), C0000R.drawable.scoreroller_numbersheet).getWidth() / 10;
        this.d = (i2 - (this.c * 6)) - 10;
        this.e = 10;
        this.f = new q[this.g];
        for (int i3 = 0; i3 < this.g; i3++) {
            this.f[i3] = new q(context, this.d + (this.c * i3), this.e);
        }
        this.m = new Paint();
        this.m.setARGB(255, 0, 0, 0);
        this.m.setTextSize(40.0f);
        this.p = BitmapFactory.decodeResource(context.getResources(), C0000R.drawable.combo_1x);
        this.q = BitmapFactory.decodeResource(context.getResources(), C0000R.drawable.combo_2x);
        this.r = BitmapFactory.decodeResource(context.getResources(), C0000R.drawable.combo_3x);
        this.s = BitmapFactory.decodeResource(context.getResources(), C0000R.drawable.combo_4x);
        this.t = BitmapFactory.decodeResource(context.getResources(), C0000R.drawable.combo_5x);
        this.u = BitmapFactory.decodeResource(context.getResources(), C0000R.drawable.combo_6x);
        this.v = this.p;
        this.n = this.d - this.p.getWidth();
        this.o = this.e - (this.p.getHeight() / 5);
        this.l = new int[6];
    }

    public final int a() {
        return this.k;
    }

    public final void a(int i2) {
        this.f92a += i2;
        if (this.f92a < 0) {
            this.f92a = 0;
        }
        this.f[0].a((this.f92a % 1000000) / 100000);
        this.f[1].a((this.f92a % 100000) / 10000);
        this.f[2].a((this.f92a % 10000) / 1000);
        this.f[3].a((this.f92a % 1000) / 100);
        this.f[4].a((this.f92a % 100) / 10);
        this.f[5].a(this.f92a % 10);
    }

    public final void b(int i2) {
        this.h = 0;
        while (this.h < this.g) {
            this.f[this.h].a((float) i2);
            this.h++;
        }
    }

    public final int[] b() {
        return this.l;
    }

    public final void c() {
        this.b = 0;
        this.j = 0;
    }

    public final void d() {
        this.b++;
        this.j++;
        if (this.b > 6) {
            this.b = 6;
        }
        this.l[this.b - 1] = this.l[this.b - 1] + 1;
        if (this.j > this.k) {
            this.k = this.j;
        }
        switch (this.b) {
            case 1:
                this.v = this.p;
                a(20);
                return;
            case 2:
                this.v = this.q;
                a(40);
                return;
            case 3:
                this.v = this.r;
                a(60);
                return;
            case 4:
                this.v = this.s;
                a(100);
                return;
            case 5:
                this.v = this.t;
                a(150);
                return;
            case 6:
                this.v = this.u;
                a(200);
                return;
            default:
                return;
        }
    }

    public final void draw(Canvas canvas) {
        canvas.drawBitmap(this.i, (float) this.d, (float) this.e, (Paint) null);
        for (q draw : this.f) {
            draw.draw(canvas);
        }
        if (this.b != 0) {
            canvas.drawBitmap(this.v, (float) this.n, (float) this.o, (Paint) null);
        }
    }

    public final int getOpacity() {
        return 0;
    }

    public final void setAlpha(int i2) {
    }

    public final void setColorFilter(ColorFilter colorFilter) {
    }
}
