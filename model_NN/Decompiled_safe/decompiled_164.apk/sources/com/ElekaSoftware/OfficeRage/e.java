package com.ElekaSoftware.OfficeRage;

public final class e extends Thread {

    /* renamed from: a  reason: collision with root package name */
    private boolean f116a;
    private long b;
    private long c;
    /* access modifiers changed from: private */
    public final h d;
    private long e;
    private long f;
    /* access modifiers changed from: private */
    public boolean g = false;
    /* access modifiers changed from: private */
    public int h = 0;

    public e(h hVar) {
        this.d = hVar;
        this.f116a = false;
    }

    public final void a() {
        this.g = true;
        this.h += 20;
        this.d.post(new ag(this));
    }

    public final void a(boolean z) {
        this.f116a = z;
    }

    public final void run() {
        while (this.f116a) {
            this.b = System.currentTimeMillis();
            this.d.post(new ah(this));
            this.c = System.currentTimeMillis();
            this.e = this.c - this.b;
            this.f = 100 - this.e;
            try {
                Thread.sleep(this.f);
            } catch (Exception e2) {
            }
        }
    }
}
