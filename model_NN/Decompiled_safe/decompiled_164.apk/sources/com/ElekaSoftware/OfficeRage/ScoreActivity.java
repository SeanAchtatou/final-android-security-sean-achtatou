package com.ElekaSoftware.OfficeRage;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.ElekaSoftware.OfficeRage.GameAnimations.GameAnimationActivity;
import com.openfeint.api.a.bb;
import com.openfeint.api.a.j;
import com.openfeint.api.a.y;
import com.openfeint.internal.w;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

public class ScoreActivity extends Activity implements View.OnClickListener {
    /* access modifiers changed from: private */
    public static final String[] e = {"810086", "795926", "795986", "795996", "796006", "812366", "812376", "812396", "812416"};
    /* access modifiers changed from: private */
    public static List f = null;

    /* renamed from: a  reason: collision with root package name */
    protected ArrayList f48a;
    int b;
    int c;
    public boolean d;
    private SharedPreferences g;
    /* access modifiers changed from: private */
    public t h;
    private s i;
    private ListView j;
    /* access modifiers changed from: private */
    public ArrayList k;
    /* access modifiers changed from: private */
    public Vector l;
    private int m;
    private int n;
    private float o;
    /* access modifiers changed from: private */
    public a p;
    private View q;
    private View r;
    private View s;
    private boolean t;
    private boolean u = false;
    private LinearLayout v;
    private BitmapDrawable w;

    /* access modifiers changed from: protected */
    public final void a() {
        this.q.setEnabled(true);
        this.q.setBackgroundResource(C0000R.drawable.button_back);
        this.s.setEnabled(true);
        this.s.setBackgroundResource(C0000R.drawable.button_restart);
        this.g = getSharedPreferences("OfficeRagePreferences", 0);
        if (this.c < this.m && this.b > this.g.getInt("levelplayed", 0)) {
            return;
        }
        if (this.b == 8 && !this.u) {
            return;
        }
        if (this.u) {
            this.r.setEnabled(true);
            this.r.setBackgroundResource(C0000R.drawable.button_animation);
            return;
        }
        this.r.setEnabled(true);
        this.r.setBackgroundResource(C0000R.drawable.button_next);
    }

    public final void a(int i2, boolean z) {
        if (!z) {
            runOnUiThread(new l(this, i2));
        } else {
            runOnUiThread(new k(this, i2));
        }
    }

    public final void b() {
        runOnUiThread(new o(this));
    }

    public void onBackPressed() {
        if (this.r.isEnabled()) {
            finish();
        }
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case C0000R.id.score_button_back /*2131361839*/:
                if (this.u) {
                    Intent intent = new Intent(this, GameAnimationActivity.class);
                    intent.putExtra("animation", 8);
                    finish();
                    startActivity(intent);
                    return;
                }
                finish();
                return;
            case C0000R.id.score_button_next /*2131361840*/:
                int i2 = this.b + 1;
                if (this.u) {
                    Intent intent2 = new Intent(this, GameAnimationActivity.class);
                    intent2.putExtra("animation", 8);
                    finish();
                    startActivity(intent2);
                    return;
                } else if (!this.t) {
                    Intent intent3 = new Intent(this, GameActivity.class);
                    intent3.putExtra("level", i2);
                    finish();
                    startActivity(intent3);
                    return;
                } else if (i2 == 1 || i2 == 2 || i2 == 4 || i2 == 6) {
                    Intent intent4 = new Intent(this, GameAnimationActivity.class);
                    intent4.putExtra("animation", i2);
                    finish();
                    startActivity(intent4);
                    return;
                } else {
                    Intent intent5 = new Intent(this, GameActivity.class);
                    intent5.putExtra("level", i2);
                    finish();
                    startActivity(intent5);
                    return;
                }
            case C0000R.id.score_button_restart /*2131361841*/:
                if (this.u) {
                    Intent intent6 = new Intent(this, GameAnimationActivity.class);
                    intent6.putExtra("animation", 8);
                    finish();
                    startActivity(intent6);
                    return;
                }
                Intent intent7 = new Intent(this, GameActivity.class);
                intent7.putExtra("level", this.b);
                finish();
                startActivity(intent7);
                return;
            default:
                return;
        }
    }

    public void onCreate(Bundle bundle) {
        int i2;
        super.onCreate(bundle);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        Bundle extras = getIntent().getExtras();
        setContentView((int) C0000R.layout.scoreview_layout);
        overridePendingTransition(C0000R.anim.view_fade_in, C0000R.anim.view_fade_out);
        ArrayList parcelableArrayListExtra = getIntent().getParcelableArrayListExtra("scoreitems");
        this.k = new ArrayList(parcelableArrayListExtra);
        parcelableArrayListExtra.clear();
        this.f48a = new ArrayList();
        this.o = extras.getFloat("totalgametime");
        this.b = extras.getInt("level");
        this.c = extras.getInt("totalscore");
        this.m = extras.getInt("levelupscore");
        this.n = extras.getInt("longestCombo");
        this.v = (LinearLayout) findViewById(C0000R.id.score_main);
        Resources resources = getResources();
        int i3 = this.b;
        Display defaultDisplay = getWindowManager().getDefaultDisplay();
        int width = defaultDisplay.getWidth();
        int height = defaultDisplay.getHeight();
        Bitmap createBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
        switch (i3) {
            case 1:
                i2 = C0000R.drawable.level1_background;
                break;
            case 2:
                i2 = C0000R.drawable.level2_background;
                break;
            case 3:
                i2 = C0000R.drawable.level3_background;
                break;
            case 4:
                i2 = C0000R.drawable.level4_background;
                break;
            case 5:
                i2 = C0000R.drawable.level5_background;
                break;
            case 6:
                i2 = C0000R.drawable.level6_background;
                break;
            case 7:
                i2 = C0000R.drawable.level7_background;
                break;
            case 8:
                i2 = C0000R.drawable.level8_background;
                break;
            default:
                i2 = C0000R.drawable.level1_background;
                Log.d("TAG", "PERKELEEN PERKELE MENI PIELEEN!!!!!!");
                break;
        }
        Bitmap decodeResource = BitmapFactory.decodeResource(getResources(), i2);
        Canvas canvas = new Canvas(createBitmap);
        float f2 = ((float) width) / ((float) height);
        float width2 = ((float) decodeResource.getWidth()) / ((float) decodeResource.getHeight());
        Rect rect = new Rect();
        if (f2 >= width2) {
            rect.set(0, 0, decodeResource.getWidth(), (int) ((((float) decodeResource.getWidth()) / ((float) width)) * ((float) height)));
        } else {
            float height2 = (((float) decodeResource.getHeight()) / ((float) height)) * ((float) width);
            int width3 = (decodeResource.getWidth() - ((int) height2)) / 2;
            rect.set(width3, 0, ((int) height2) + width3, decodeResource.getHeight());
        }
        Paint paint = new Paint();
        ColorMatrix colorMatrix = new ColorMatrix();
        colorMatrix.setSaturation(0.0f);
        paint.setColorFilter(new ColorMatrixColorFilter(colorMatrix));
        canvas.drawBitmap(decodeResource, rect, new Rect(0, 0, width, height), paint);
        this.w = new BitmapDrawable(resources, createBitmap);
        this.v.setBackgroundDrawable(this.w);
        this.d = false;
        int i4 = extras.getInt("comboreadout");
        int i5 = i4 * 100;
        this.k.add(new ScoreItem(98, "Remaining combo: " + i4, i5));
        this.c += i5;
        this.k.add(new ScoreItem(99, "Total score: ", this.c));
        if (this.b == 8 && this.c >= this.m) {
            this.u = true;
        }
        if (this.c >= this.m) {
            this.k.add(new ScoreItem(101, "Level Complete", -1));
        }
        if (this.u) {
            this.k.add(new ScoreItem(150, "GAME COMPLETE", -1));
        }
        this.l = new Vector();
        this.h = new t(this, this.l);
        this.j = (ListView) findViewById(C0000R.id.score_list);
        this.j.setAdapter((ListAdapter) this.h);
        this.j.setClickable(false);
        this.j.setTranscriptMode(2);
        this.g = getSharedPreferences("OfficeRagePreferences", 0);
        if (this.c >= this.m && this.b > this.g.getInt("levelplayed", 0)) {
            SharedPreferences.Editor edit = this.g.edit();
            edit.putInt("levelplayed", this.b);
            edit.commit();
        }
        this.t = this.g.getBoolean("Cutscene", true);
        f fVar = new f(this);
        try {
            fVar.b();
            if (this.c > fVar.a(this.b)) {
                fVar.a("Unknown", this.b, this.c);
                fVar.b(this.b);
            }
            fVar.close();
        } catch (Exception e2) {
        }
        try {
            if (w.a().e()) {
                new bb((long) this.c).a(new j(e[this.b]), new n(this));
                new bb((long) this.n).a(new j("857246"), new m(this));
                y.a(new p(this));
            }
        } catch (Exception e3) {
        }
        this.p = new a(this, this);
        this.s = findViewById(C0000R.id.score_button_restart);
        this.s.setOnClickListener(this);
        this.s.setEnabled(false);
        this.q = findViewById(C0000R.id.score_button_back);
        this.q.setOnClickListener(this);
        this.q.setEnabled(false);
        this.r = findViewById(C0000R.id.score_button_next);
        this.r.setOnClickListener(this);
        this.r.setEnabled(false);
    }

    public void onDestroy() {
        super.onDestroy();
        Log.d("ScoreActivity", "onDestroy");
        this.i.a();
        this.l.clear();
        this.l = null;
        this.k.clear();
        this.k = null;
        this.f48a.clear();
        this.f48a = null;
        this.i = null;
        this.h.a();
        this.s.setOnClickListener(null);
        this.s = null;
        this.q.setOnClickListener(null);
        this.q = null;
        this.r.setOnClickListener(null);
        this.r = null;
        this.p = null;
        this.h = null;
        this.j.setAdapter((ListAdapter) null);
        this.j = null;
    }

    public void onPause() {
        super.onPause();
        Log.d("ScoreActivity", "onPause");
        this.i.a();
        this.p.dismiss();
    }

    public void onResume() {
        super.onResume();
        Log.d("ScoreActivity", "onResume");
        this.p = new a(this, this);
        this.l.clear();
        this.h.notifyDataSetChanged();
        this.i = new s(this, this.k.size());
        this.i.start();
    }
}
