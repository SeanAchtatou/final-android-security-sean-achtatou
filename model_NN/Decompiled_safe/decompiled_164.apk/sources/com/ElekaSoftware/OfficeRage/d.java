package com.ElekaSoftware.OfficeRage;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;

public final class d extends Drawable {

    /* renamed from: a  reason: collision with root package name */
    private Bitmap f103a;
    private int b = 0;
    private int c = 0;
    private boolean d = true;
    private final h e;
    private final int f;
    private final int g;
    private final String h;
    private final Paint i;
    private final Typeface j;
    private final int k;
    private final int l;
    private float m;
    private final int n;
    private final int o;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Paint.setShadowLayer(float, float, float, int):void}
     arg types: [int, int, int, int]
     candidates:
      ClspMth{android.graphics.Paint.setShadowLayer(float, float, float, long):void}
      ClspMth{android.graphics.Paint.setShadowLayer(float, float, float, int):void} */
    public d(h hVar, String str) {
        this.e = hVar;
        this.h = str;
        this.f103a = BitmapFactory.decodeResource(this.e.getResources(), C0000R.drawable.game_start_ready);
        this.f = (this.e.i / 2) - (this.f103a.getWidth() / 2);
        this.g = (this.e.j / 2) - (this.f103a.getHeight() / 2);
        this.n = this.e.i / 2;
        this.o = this.e.j / 2;
        this.i = new Paint();
        this.i.setColor(Color.argb(255, 255, 161, 24));
        this.i.setTextSize((float) (this.e.i / 14));
        this.j = Typeface.createFromAsset(this.e.getResources().getAssets(), "coolvetica.ttf");
        this.i.setTypeface(this.j);
        this.i.setTextAlign(Paint.Align.CENTER);
        this.i.setAntiAlias(true);
        this.i.setShadowLayer(4.0f, 2.0f, 2.0f, -16777216);
        this.k = this.e.i / 2;
        this.l = this.e.j / 4;
        this.m = 1.0f;
    }

    public final void a(int i2) {
        if (this.d) {
            this.b += i2;
            this.m -= 0.015f;
            if (this.b > 1000 && this.c == 0) {
                this.m = 1.0f;
                this.c = 1;
                this.f103a = BitmapFactory.decodeResource(this.e.getResources(), C0000R.drawable.game_start_set);
            } else if (this.b > 2000 && this.c == 1) {
                this.m = 1.0f;
                this.c = 2;
                this.f103a = BitmapFactory.decodeResource(this.e.getResources(), C0000R.drawable.game_start_go);
            } else if (this.b > 3000) {
                this.d = false;
            }
        }
    }

    public final void draw(Canvas canvas) {
        if (this.d) {
            canvas.drawText(this.h, (float) this.k, (float) this.l, this.i);
            canvas.save();
            canvas.scale(this.m, this.m, (float) this.n, (float) this.o);
            canvas.drawBitmap(this.f103a, (float) this.f, (float) this.g, (Paint) null);
            canvas.restore();
        }
    }

    public final int getOpacity() {
        return 0;
    }

    public final void setAlpha(int i2) {
    }

    public final void setColorFilter(ColorFilter colorFilter) {
    }
}
