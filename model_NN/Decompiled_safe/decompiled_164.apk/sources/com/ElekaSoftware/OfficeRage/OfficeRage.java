package com.ElekaSoftware.OfficeRage;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import com.ElekaSoftware.OfficeRage.LevelSelector.SelectorActivity;
import com.openfeint.api.a;
import com.openfeint.api.ui.Dashboard;
import com.openfeint.internal.w;
import java.io.IOException;
import java.util.HashMap;

public class OfficeRage extends Activity implements View.OnClickListener {
    public void onBackPressed() {
        finish();
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case C0000R.id.ButtonSelectLevel /*2131361802*/:
                startActivity(new Intent(this, SelectorActivity.class));
                return;
            case C0000R.id.ButtonHighscores /*2131361803*/:
                if (w.a().e()) {
                    Dashboard.e();
                    return;
                } else {
                    Dashboard.d();
                    return;
                }
            case C0000R.id.ButtonSettings /*2131361804*/:
                startActivity(new Intent(this, Settings.class));
                return;
            case C0000R.id.ButtonAbout /*2131361805*/:
                startActivity(new Intent(this, About.class));
                return;
            case C0000R.id.ButtonQuit /*2131361806*/:
                finish();
                return;
            case C0000R.id.ButtonFeint /*2131361807*/:
                Dashboard.d();
                return;
            default:
                return;
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        getWindow().setFormat(1);
        overridePendingTransition(C0000R.anim.view_fade_in, C0000R.anim.view_fade_out);
        setContentView((int) C0000R.layout.main);
        f fVar = new f(getBaseContext());
        try {
            fVar.a();
            fVar.b();
            fVar.close();
        } catch (IOException e) {
        }
        RelativeLayout relativeLayout = (RelativeLayout) findViewById(C0000R.id.main_layout);
        Resources resources = getResources();
        Display defaultDisplay = getWindowManager().getDefaultDisplay();
        int width = defaultDisplay.getWidth();
        int height = defaultDisplay.getHeight();
        Bitmap createBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Bitmap decodeResource = BitmapFactory.decodeResource(getResources(), C0000R.drawable.main_background);
        Canvas canvas = new Canvas(createBitmap);
        float f = ((float) width) / ((float) height);
        float width2 = ((float) decodeResource.getWidth()) / ((float) decodeResource.getHeight());
        Rect rect = new Rect();
        if (f >= width2) {
            rect.set(0, 0, decodeResource.getWidth(), (int) ((((float) decodeResource.getWidth()) / ((float) width)) * ((float) height)));
        } else {
            float height2 = (((float) decodeResource.getHeight()) / ((float) height)) * ((float) width);
            int width3 = (decodeResource.getWidth() - ((int) height2)) / 2;
            rect.set(width3, 0, ((int) height2) + width3, decodeResource.getHeight());
        }
        Paint paint = new Paint();
        paint.setFilterBitmap(true);
        paint.setDither(false);
        paint.setAntiAlias(true);
        canvas.drawBitmap(decodeResource, rect, new Rect(0, 0, width, height), paint);
        relativeLayout.setBackgroundDrawable(new BitmapDrawable(resources, createBitmap));
        ((Button) findViewById(C0000R.id.ButtonSelectLevel)).setOnClickListener(this);
        ((Button) findViewById(C0000R.id.ButtonHighscores)).setOnClickListener(this);
        ((Button) findViewById(C0000R.id.ButtonSettings)).setOnClickListener(this);
        ((Button) findViewById(C0000R.id.ButtonAbout)).setOnClickListener(this);
        ((Button) findViewById(C0000R.id.ButtonQuit)).setOnClickListener(this);
        ((Button) findViewById(C0000R.id.ButtonFeint)).setOnClickListener(this);
        HashMap hashMap = new HashMap();
        hashMap.put("SettingCloudStorageCompressionStrategy", "CloudStorageCompressionStrategyDefault");
        hashMap.put("RequestedOrientation", 0);
        w.a(this, new a("Office Rage", "uURlEcfi4Ojv6jQz9iXQ", "TLQzNLB1vOZsyxqvr4uMExF35bdTIs2OtWXMn56rv0I", "297082", hashMap), new aa(this));
        w a2 = w.a();
        if (a2 != null) {
            a2.o();
        }
    }

    public void onResume() {
        super.onResume();
        overridePendingTransition(C0000R.anim.view_fade_in, C0000R.anim.view_fade_out);
    }
}
