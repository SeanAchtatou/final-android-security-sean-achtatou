package com.ElekaSoftware.OfficeRage;

import android.app.Activity;
import android.app.Dialog;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

public class Settings extends Activity implements View.OnClickListener {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public SharedPreferences f50a;
    private CheckBox b;
    private CheckBox c;
    private CheckBox d;
    private CheckBox e;
    private CheckBox f;
    private Button g;
    private ImageButton h;
    /* access modifiers changed from: private */
    public Dialog i;
    private View.OnClickListener j = new b(this);
    private View.OnClickListener k = new c(this);

    public void onClick(View view) {
        switch (view.getId()) {
            case C0000R.id.settings_back /*2131361850*/:
                finish();
                overridePendingTransition(C0000R.anim.view_fade_in, C0000R.anim.view_fade_out);
                return;
            case C0000R.id.reset_levels /*2131361851*/:
                this.i.setContentView((int) C0000R.layout.level_reset_dialog);
                ((TextView) this.i.findViewById(C0000R.id.level_reset_dialog_title)).setText("Reset levels?");
                ((Button) this.i.findViewById(C0000R.id.reset_levels_reset_button)).setOnClickListener(this.k);
                ((Button) this.i.findViewById(C0000R.id.reset_levels_cancel_button)).setOnClickListener(this.j);
                this.i.show();
                return;
            default:
                return;
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        overridePendingTransition(C0000R.anim.view_fade_in, C0000R.anim.view_fade_out);
        setContentView((int) C0000R.layout.settings);
        LinearLayout linearLayout = (LinearLayout) findViewById(C0000R.id.settings_mainlayout);
        Display defaultDisplay = getWindowManager().getDefaultDisplay();
        int width = defaultDisplay.getWidth();
        int height = defaultDisplay.getHeight();
        Bitmap createBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
        Bitmap decodeResource = BitmapFactory.decodeResource(getResources(), C0000R.drawable.main_background);
        Canvas canvas = new Canvas(createBitmap);
        float f2 = ((float) width) / ((float) height);
        float width2 = ((float) decodeResource.getWidth()) / ((float) decodeResource.getHeight());
        Rect rect = new Rect();
        if (f2 >= width2) {
            rect.set(0, 0, decodeResource.getWidth(), (int) ((((float) decodeResource.getWidth()) / ((float) width)) * ((float) height)));
        } else {
            float height2 = (((float) decodeResource.getHeight()) / ((float) height)) * ((float) width);
            int width3 = (decodeResource.getWidth() - ((int) height2)) / 2;
            rect.set(width3, 0, ((int) height2) + width3, decodeResource.getHeight());
        }
        Paint paint = new Paint();
        paint.setFilterBitmap(true);
        canvas.drawBitmap(decodeResource, rect, new Rect(0, 0, width, height), paint);
        linearLayout.setBackgroundDrawable(new BitmapDrawable(createBitmap));
        Typeface createFromAsset = Typeface.createFromAsset(getResources().getAssets(), "coolvetica.ttf");
        this.d = (CheckBox) findViewById(C0000R.id.sfx_button);
        this.d.setOnClickListener(this);
        this.d.setTextColor(Color.argb(255, 255, 161, 24));
        this.d.setTextSize(30.0f);
        this.d.setTypeface(createFromAsset);
        this.d.setShadowLayer(4.0f, 2.0f, 2.0f, -16777216);
        this.b = (CheckBox) findViewById(C0000R.id.vibration_button);
        this.b.setOnClickListener(this);
        this.b.setTextColor(Color.argb(255, 255, 161, 24));
        this.b.setTextSize(30.0f);
        this.b.setTypeface(createFromAsset);
        this.b.setShadowLayer(4.0f, 2.0f, 2.0f, -16777216);
        this.c = (CheckBox) findViewById(C0000R.id.music_button);
        this.c.setOnClickListener(this);
        this.c.setTextColor(Color.argb(255, 255, 161, 24));
        this.c.setTextSize(30.0f);
        this.c.setTypeface(createFromAsset);
        this.c.setShadowLayer(4.0f, 2.0f, 2.0f, -16777216);
        this.e = (CheckBox) findViewById(C0000R.id.fps_button);
        this.e.setOnClickListener(this);
        this.e.setTextColor(Color.argb(255, 255, 161, 24));
        this.e.setTextSize(30.0f);
        this.e.setTypeface(createFromAsset);
        this.e.setShadowLayer(4.0f, 2.0f, 2.0f, -16777216);
        this.f = (CheckBox) findViewById(C0000R.id.cutscene_button);
        this.f.setOnClickListener(this);
        this.f.setTextColor(Color.argb(255, 255, 161, 24));
        this.f.setTextSize(30.0f);
        this.f.setTypeface(createFromAsset);
        this.f.setShadowLayer(4.0f, 2.0f, 2.0f, -16777216);
        this.h = (ImageButton) findViewById(C0000R.id.settings_back);
        this.h.setOnClickListener(this);
        this.g = (Button) findViewById(C0000R.id.reset_levels);
        this.g.setOnClickListener(this);
        this.f50a = getSharedPreferences("OfficeRagePreferences", 0);
        this.i = new Dialog(this, 16973913);
        WindowManager.LayoutParams attributes = this.i.getWindow().getAttributes();
        attributes.dimAmount = 0.5f;
        this.i.getWindow().setAttributes(attributes);
        this.i.getWindow().addFlags(2);
    }

    public void onPause() {
        super.onPause();
        SharedPreferences.Editor edit = this.f50a.edit();
        edit.putBoolean("Vibration", this.b.isChecked());
        edit.putBoolean("SoundFX", this.d.isChecked());
        edit.putBoolean("Music", this.c.isChecked());
        edit.putBoolean("Cutscene", this.f.isChecked());
        edit.putBoolean("FPS", this.e.isChecked());
        edit.commit();
    }

    public void onResume() {
        super.onResume();
        this.b.setChecked(this.f50a.getBoolean("Vibration", true));
        this.d.setChecked(this.f50a.getBoolean("SoundFX", true));
        this.c.setChecked(this.f50a.getBoolean("Music", true));
        this.f.setChecked(this.f50a.getBoolean("Cutscene", true));
        this.e.setChecked(this.f50a.getBoolean("FPS", false));
    }
}
