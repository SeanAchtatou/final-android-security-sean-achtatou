package com.ElekaSoftware.OfficeRage;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.graphics.Rect;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.os.Vibrator;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import com.ElekaSoftware.OfficeRage.a.ae;
import com.ElekaSoftware.OfficeRage.a.af;
import com.ElekaSoftware.OfficeRage.a.ag;
import com.ElekaSoftware.OfficeRage.a.ah;
import com.ElekaSoftware.OfficeRage.a.ai;
import com.ElekaSoftware.OfficeRage.a.ak;
import com.ElekaSoftware.OfficeRage.a.al;
import com.ElekaSoftware.OfficeRage.a.ao;
import com.ElekaSoftware.OfficeRage.a.ar;
import com.ElekaSoftware.OfficeRage.a.ay;
import com.ElekaSoftware.OfficeRage.a.e;
import com.ElekaSoftware.OfficeRage.a.f;
import com.ElekaSoftware.OfficeRage.a.g;
import com.ElekaSoftware.OfficeRage.a.i;
import com.ElekaSoftware.OfficeRage.a.l;
import com.ElekaSoftware.OfficeRage.a.m;
import com.ElekaSoftware.OfficeRage.a.n;
import com.ElekaSoftware.OfficeRage.a.p;
import com.ElekaSoftware.OfficeRage.a.s;
import com.ElekaSoftware.OfficeRage.a.x;
import com.ElekaSoftware.OfficeRage.b.b;
import com.ElekaSoftware.OfficeRage.b.c;
import com.ElekaSoftware.OfficeRage.c.a;
import com.ElekaSoftware.OfficeRage.c.d;
import com.ElekaSoftware.OfficeRage.d.j;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.TreeMap;
import java.util.Vector;
import java.util.concurrent.ConcurrentHashMap;
import org.xml.sax.SAXException;

public class h extends SurfaceView implements SurfaceHolder.Callback, View.OnTouchListener {
    private static Vibrator O;
    private static final long[] P = {100, 100, 100};
    private static Context ab;
    private static float aq;
    /* access modifiers changed from: private */
    public static MediaPlayer ar;
    private static boolean n;
    private float[] A;
    private int B = 0;
    private int C;
    private int[] D;
    private int E = 0;
    private int F;
    private int[] G;
    private int H = 0;
    private int I;
    private int[] J;
    private int K = 0;
    private int L;
    private Rect[] M;
    private final Random N;
    private Enumeration Q;
    private l R;
    private int S;
    private Activity T;
    private SoundPool U;
    private HashMap V;
    private int W = 1;
    private int X = 2;
    private int Y = 3;
    private int Z = 4;

    /* renamed from: a  reason: collision with root package name */
    public Point[] f119a;
    private TreeMap aa;
    private int ac;
    private int ad;
    /* access modifiers changed from: private */
    public long ae;
    private int af = 0;
    private int ag;
    private int ah;
    private float ai = 1000.0f;
    private float aj;
    private float ak;
    private int al;
    private int am;
    private float an;
    private float ao;
    private float ap;
    private AudioManager as;
    public j b;
    public ConcurrentHashMap c;
    public ConcurrentHashMap d;
    public af e;
    public r f;
    public int g;
    public d h;
    public int i;
    public int j;
    public Rect k;
    public float l;
    public float m;
    private boolean o;
    /* access modifiers changed from: private */
    public boolean p;
    private boolean q;
    /* access modifiers changed from: private */
    public ad r;
    /* access modifiers changed from: private */
    public e s;
    private int t;
    private int u;
    private int[] v;
    private int[] w;
    private List x;
    private int y;
    private int z = 0;

    public h(Context context, Activity activity, int i2) {
        super(context);
        b bVar;
        ab = context;
        this.T = activity;
        SurfaceHolder holder = getHolder();
        holder.addCallback(this);
        this.ac = i2;
        Display defaultDisplay = activity.getWindowManager().getDefaultDisplay();
        this.i = defaultDisplay.getWidth();
        this.j = defaultDisplay.getHeight();
        DisplayMetrics displayMetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        switch (displayMetrics.densityDpi) {
            case 120:
                this.g = 120;
                break;
            case 160:
                this.g = 160;
                break;
            case 240:
                this.g = 240;
                break;
        }
        int i3 = this.i;
        int i4 = this.j;
        this.M = new Rect[6];
        this.M[0] = new Rect(0, 0, i3 / 2, i4 / 3);
        this.M[1] = new Rect(0, i4 / 3, i3 / 2, (i4 * 2) / 3);
        this.M[2] = new Rect(0, (i4 * 2) / 3, i3 / 2, i4);
        this.M[3] = new Rect(i3 / 2, 0, i3, i4 / 3);
        this.M[4] = new Rect(i3 / 2, i4 / 3, i3, (i4 * 2) / 3);
        this.M[5] = new Rect(i3 / 2, (i4 * 2) / 3, i3, i4);
        float f2 = 0.6f * ((float) i3);
        f2 = f2 > ((float) i4) ? (float) i4 : f2;
        float f3 = (((float) i3) - f2) / 2.0f;
        float f4 = (((float) i4) - f2) / 2.0f;
        this.k = new Rect((int) f3, (int) f4, ((int) f3) + ((int) f2), ((int) f4) + ((int) f2));
        this.f119a = new Point[6];
        this.f119a[0] = new Point(this.k.left, this.k.top + (this.k.height() / 6));
        this.f119a[1] = new Point(this.k.left, this.k.top + (this.k.height() / 2));
        this.f119a[2] = new Point(this.k.left, this.k.top + ((this.k.height() * 5) / 6));
        this.f119a[3] = new Point(this.k.right, this.k.top + (this.k.height() / 6));
        this.f119a[4] = new Point(this.k.right, this.k.top + (this.k.height() / 2));
        this.f119a[5] = new Point(this.k.right, this.k.top + ((this.k.height() * 5) / 6));
        this.ag = (int) Math.pow((double) (((float) this.k.height()) / 5.0f), 2.0d);
        this.ah = (int) Math.pow((double) (((float) this.k.height()) / 10.0f), 2.0d);
        this.aj = (float) (this.f119a[3].x - this.f119a[0].x);
        this.l = ((float) this.f119a[0].x) - (0.5f * this.aj);
        this.m = ((float) this.f119a[4].x) + (0.5f * this.aj);
        O = (Vibrator) ab.getSystemService("vibrator");
        new c();
        try {
            bVar = c.a(ab, "levels/level" + Integer.toString(i2) + ".xml");
        } catch (SAXException e2) {
            e2.printStackTrace();
            bVar = null;
        }
        this.ad = bVar.b;
        this.af = bVar.g;
        this.N = new Random();
        this.c = new ConcurrentHashMap();
        this.d = new ConcurrentHashMap();
        this.t = bVar.i.size();
        this.v = new int[this.t];
        for (int i5 = 0; i5 < this.t; i5++) {
            this.v[i5] = ((Integer) bVar.i.get(i5)).intValue();
        }
        this.aa = new TreeMap();
        this.x = new ArrayList();
        for (int i6 = 0; i6 < this.t; i6++) {
            if (!this.x.contains(Integer.valueOf(this.v[i6]))) {
                this.x.add(Integer.valueOf(this.v[i6]));
            }
        }
        g();
        int i7 = bVar.h;
        this.w = new int[((this.t * 10) + i7)];
        this.u = (this.t * 10) + i7;
        int i8 = 0;
        for (int i9 = 0; i9 < i7; i9++) {
            this.w[i8] = 99;
            i8++;
        }
        int i10 = 0;
        while (i7 < this.u) {
            if (i10 >= this.t) {
                i10 = 0;
            }
            this.w[i7] = this.v[i10];
            i10++;
            i7++;
        }
        for (int i11 = 0; i11 < this.u; i11++) {
            int i12 = this.w[i11];
            int nextInt = this.N.nextInt(this.u);
            this.w[i11] = this.w[nextInt];
            this.w[nextInt] = i12;
        }
        for (int i13 = 0; i13 < this.x.size(); i13++) {
            this.aa.put((Integer) this.x.get(i13), 0);
        }
        setOnTouchListener(this);
        this.b = new j(getContext(), this);
        this.e = new af(this, ab, this.i);
        this.f = new r(ab, this, bVar.c, bVar.d, bVar.e);
        this.f.b();
        this.h = new d(this, bVar.f100a);
        this.r = new ad(this, holder, this.i, this.j, bVar.f);
        this.s = new e(this);
        this.U = new SoundPool(4, 3, 100);
        this.V = new HashMap();
        this.V.put(Integer.valueOf(this.W), Integer.valueOf(this.U.load(ab, C0000R.raw.honk, 1)));
        this.V.put(Integer.valueOf(this.X), Integer.valueOf(this.U.load(ab, C0000R.raw.plop, 1)));
        this.V.put(Integer.valueOf(this.Y), Integer.valueOf(this.U.load(ab, C0000R.raw.powerup, 1)));
        this.V.put(Integer.valueOf(this.Z), Integer.valueOf(this.U.load(ab, C0000R.raw.pills, 1)));
        this.as = (AudioManager) getContext().getSystemService("audio");
        this.an = (float) this.as.getStreamVolume(3);
        this.ao = (float) this.as.getStreamMaxVolume(3);
        this.ap = this.an / this.ao;
        aq = this.ap * 1.8f;
        i();
        h();
        MediaPlayer create = MediaPlayer.create(ab, (int) C0000R.raw.pelimusa);
        ar = create;
        create.setVolume(aq, aq);
    }

    public static final void c() {
        ar.seekTo(0);
        ar.start();
    }

    public static void c(int i2) {
        if (n) {
            switch (i2) {
                case 0:
                    O.vibrate(P, -1);
                    return;
                case 1:
                    O.vibrate(50);
                    return;
                case 2:
                    O.vibrate(500);
                    return;
                default:
                    return;
            }
        }
    }

    private l d(int i2) {
        switch (i2) {
            case 0:
                return new f(ab, this);
            case 1:
                return new i(ab, this);
            case 2:
            default:
                return null;
            case 3:
                return new ak(ab, this);
            case 4:
                return new ay(ab, this);
            case 5:
                return new al(ab, this);
            case 6:
                return new s(ab, this);
            case 7:
                return new com.ElekaSoftware.OfficeRage.a.h(ab, this);
            case 8:
                return new p(ab, this);
            case 9:
                return new x(ab, this);
            case 10:
                return new ae(ab, this);
            case 11:
                return new g(ab, this);
            case 12:
                return new m(ab, this);
            case 13:
                return new ao(ab, this);
            case 14:
                return new af(ab, this);
            case 15:
                return new ai(ab, this);
            case 16:
                return new e(ab, this);
            case 17:
                return new ah(ab, this);
            case 18:
                return new ar(ab, this);
            case 19:
                return new n(ab, this);
            case 20:
                return new ag(ab, this);
        }
    }

    private a e(int i2) {
        switch (i2) {
            case 0:
                return new d(ab, this, this.g);
            case 1:
                return new com.ElekaSoftware.OfficeRage.c.e(ab, this, this.g);
            case 2:
                return new com.ElekaSoftware.OfficeRage.c.c(ab, this, this.g);
            case 3:
                return new com.ElekaSoftware.OfficeRage.c.b(ab, this, this.g);
            case 4:
                return new com.ElekaSoftware.OfficeRage.c.f(ab, this, this.g);
            default:
                return null;
        }
    }

    /* JADX WARNING: CFG modification limit reached, blocks count: 134 */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x00bd, code lost:
        if (r0.isVisible() != false) goto L_0x0056;
     */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x003b  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x005c A[EDGE_INSN: B:18:0x005c->B:19:0x0078 ?: BREAK  ] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0098 A[LOOP:0: B:10:0x0027->B:27:0x0098, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00ad  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.ElekaSoftware.OfficeRage.a.l f() {
        /*
            r5 = this;
            r4 = 9
            r3 = 8
            r2 = 0
            int r0 = r5.z
            int r1 = r5.u
            if (r0 < r1) goto L_0x000d
            r5.z = r2
        L_0x000d:
            int[] r0 = r5.w
            int r1 = r5.z
            r0 = r0[r1]
            r5.y = r0
            int r0 = r5.y
            if (r0 == r3) goto L_0x001d
            int r0 = r5.y
            if (r0 != r4) goto L_0x0027
        L_0x001d:
            int r0 = r5.af
            if (r0 <= 0) goto L_0x0079
            int r0 = r5.af
            r1 = 1
            int r0 = r0 - r1
            r5.af = r0
        L_0x0027:
            int r0 = r5.y
            r1 = 99
            if (r0 == r1) goto L_0x0098
            int r0 = r5.z
            int r0 = r0 + 1
            r5.z = r0
            java.util.concurrent.ConcurrentHashMap r0 = r5.c
            int r0 = r0.size()
            if (r0 != 0) goto L_0x004a
            java.util.concurrent.ConcurrentHashMap r0 = r5.c
            java.lang.Integer r1 = java.lang.Integer.valueOf(r2)
            int r2 = r5.y
            com.ElekaSoftware.OfficeRage.a.l r2 = r5.d(r2)
            r0.put(r1, r2)
        L_0x004a:
            java.util.concurrent.ConcurrentHashMap r0 = r5.c
            int r1 = r0.size()
            java.util.concurrent.ConcurrentHashMap r0 = r5.c
            java.util.Enumeration r2 = r0.elements()
        L_0x0056:
            boolean r0 = r2.hasMoreElements()
            if (r0 != 0) goto L_0x00ad
            java.util.concurrent.ConcurrentHashMap r0 = r5.c
            java.lang.Integer r2 = java.lang.Integer.valueOf(r1)
            int r3 = r5.y
            com.ElekaSoftware.OfficeRage.a.l r3 = r5.d(r3)
            r0.put(r2, r3)
            java.util.concurrent.ConcurrentHashMap r0 = r5.c
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)
            java.lang.Object r5 = r0.get(r1)
            com.ElekaSoftware.OfficeRage.a.l r5 = (com.ElekaSoftware.OfficeRage.a.l) r5
            r0 = r5
        L_0x0078:
            return r0
        L_0x0079:
            int r0 = r5.z
            int r0 = r0 + 1
            r5.z = r0
            int r0 = r5.z
            int r1 = r5.u
            if (r0 < r1) goto L_0x0087
            r5.z = r2
        L_0x0087:
            int[] r0 = r5.w
            int r1 = r5.z
            r0 = r0[r1]
            r5.y = r0
            int r0 = r5.y
            if (r0 == r3) goto L_0x0079
            int r0 = r5.y
            if (r0 == r4) goto L_0x0079
            goto L_0x0027
        L_0x0098:
            com.ElekaSoftware.OfficeRage.e r0 = r5.s
            r0.a()
            int r0 = r5.z
            int r0 = r0 + 1
            r5.z = r0
            int[] r0 = r5.w
            int r1 = r5.z
            r0 = r0[r1]
            r5.y = r0
            goto L_0x0027
        L_0x00ad:
            java.lang.Object r0 = r2.nextElement()
            com.ElekaSoftware.OfficeRage.a.l r0 = (com.ElekaSoftware.OfficeRage.a.l) r0
            int r3 = r0.c
            int r4 = r5.y
            if (r3 != r4) goto L_0x0056
            boolean r3 = r0.isVisible()
            if (r3 != 0) goto L_0x0056
            goto L_0x0078
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ElekaSoftware.OfficeRage.h.f():com.ElekaSoftware.OfficeRage.a.l");
    }

    private void g() {
        int i2 = 0;
        int size = this.x.size();
        int i3 = 0;
        while (i2 < size) {
            this.c.put(Integer.valueOf(i3), d(((Integer) this.x.get(i2)).intValue()));
            int i4 = i3 + 1;
            this.c.put(Integer.valueOf(i4), d(((Integer) this.x.get(i2)).intValue()));
            i2++;
            i3 = i4 + 1;
        }
    }

    private void h() {
        Vector vector = new Vector();
        for (int i2 = 0; i2 < this.x.size(); i2++) {
            l d2 = d(((Integer) this.x.get(i2)).intValue());
            if (!vector.contains(Integer.valueOf(d2.s))) {
                vector.add(Integer.valueOf(d2.s));
            }
        }
        for (int i3 = 0; i3 < vector.size(); i3++) {
            if (((Integer) vector.get(i3)).intValue() == 1 || ((Integer) vector.get(i3)).intValue() == 4) {
                this.d.put(Integer.valueOf(this.d.size()), e(((Integer) vector.get(i3)).intValue()));
                this.d.put(Integer.valueOf(this.d.size()), e(((Integer) vector.get(i3)).intValue()));
                this.d.put(Integer.valueOf(this.d.size()), e(((Integer) vector.get(i3)).intValue()));
                if (((Integer) vector.get(i3)).intValue() == 4) {
                    this.d.put(Integer.valueOf(this.d.size()), e(((Integer) vector.get(i3)).intValue()));
                    this.d.put(Integer.valueOf(this.d.size()), e(((Integer) vector.get(i3)).intValue()));
                }
            }
            this.d.put(Integer.valueOf(this.d.size()), e(((Integer) vector.get(i3)).intValue()));
            this.d.put(Integer.valueOf(this.d.size()), e(((Integer) vector.get(i3)).intValue()));
            this.d.put(Integer.valueOf(this.d.size()), e(((Integer) vector.get(i3)).intValue()));
        }
    }

    private void i() {
        this.A = new float[50];
        this.C = this.A.length;
        for (int i2 = 0; i2 < this.C; i2++) {
            if (this.N.nextBoolean()) {
                this.A[i2] = ((float) (this.N.nextInt(6) + 1)) * -45.0f;
            } else {
                this.A[i2] = ((float) (this.N.nextInt(6) + 1)) * 45.0f;
            }
        }
        this.D = new int[71];
        this.F = this.D.length;
        for (int i3 = 0; i3 < this.F; i3++) {
            this.D[i3] = this.N.nextInt(3);
        }
        this.G = new int[62];
        this.I = this.G.length;
        for (int i4 = 0; i4 < this.I; i4++) {
            this.G[i4] = this.N.nextInt(6);
        }
        this.J = new int[30];
        this.L = this.J.length;
        for (int i5 = 0; i5 < this.L; i5++) {
            this.J[i5] = (this.N.nextInt(8) * 50) + 650;
        }
    }

    public synchronized void a() {
        this.al = this.G[this.H];
        this.ai = (float) this.J[this.K];
        if (this.al < 3) {
            this.am = this.D[this.E] + 3;
            this.ak = (float) (this.f119a[this.am].y - this.f119a[this.al].y);
            l f2 = f();
            f2.f78a = this.l;
            f2.b = ((float) this.f119a[this.al].y) - (this.ak * 0.5f);
            f2.m = this.aj / this.ai;
            f2.n = this.ak / this.ai;
            f2.setVisible(true, true);
            f2.q = true;
            f2.b(this.A[this.B]);
        } else {
            this.am = this.D[this.E];
            this.ak = (float) (this.f119a[this.am].y - this.f119a[this.al].y);
            l f3 = f();
            f3.f78a = this.m;
            f3.b = ((float) this.f119a[this.al].y) - (this.ak * 0.5f);
            f3.m = (-this.aj) / this.ai;
            f3.n = this.ak / this.ai;
            f3.setVisible(true, true);
            f3.q = true;
            f3.b(this.A[this.B]);
        }
        this.B++;
        if (this.B >= this.C) {
            this.B = 0;
        }
        this.E++;
        if (this.E >= this.F) {
            this.E = 0;
        }
        this.H++;
        if (this.H >= this.I) {
            this.H = 0;
        }
        this.K++;
        if (this.K >= this.L) {
            this.K = 0;
        }
    }

    public void a(int i2) {
        this.Q = this.c.elements();
        int i3 = 0;
        while (this.Q.hasMoreElements()) {
            this.R = (l) this.Q.nextElement();
            if (this.R.isVisible() && this.R.q && this.M[i2].contains((int) this.R.f78a, (int) this.R.b)) {
                this.S = (int) (Math.pow((double) (this.R.f78a - ((float) this.f119a[i2].x)), 2.0d) + Math.pow((double) (this.R.b - ((float) this.f119a[i2].y)), 2.0d));
                if (this.S < this.ag) {
                    this.aa.put(Integer.valueOf(this.R.c), Integer.valueOf(((Integer) this.aa.get(Integer.valueOf(this.R.c))).intValue() + 1));
                    a(this.R);
                    if (this.S < this.ah) {
                        int i4 = this.R.f < 3 ? i3 + 1 : i3;
                        this.R.a(true);
                        i3 = i4;
                    } else {
                        this.R.a(false);
                    }
                }
            }
        }
        if (i3 == 0) {
            this.e.c();
            return;
        }
        for (int i5 = 0; i5 < i3; i5++) {
            this.e.d();
        }
    }

    public void a(l lVar) {
        a aVar;
        if (lVar.t > 0) {
            for (int i2 = 0; i2 < lVar.t; i2++) {
                int i3 = lVar.s;
                if (this.d.size() == 0) {
                    this.d.put(0, e(i3));
                }
                int size = this.d.size();
                Enumeration elements = this.d.elements();
                while (true) {
                    if (elements.hasMoreElements()) {
                        aVar = (a) elements.nextElement();
                        if (aVar.h == i3 && !aVar.isVisible()) {
                            break;
                        }
                    } else {
                        this.d.put(Integer.valueOf(size), e(i3));
                        aVar = (a) this.d.get(Integer.valueOf(size));
                        break;
                    }
                }
                aVar.a(lVar);
            }
        }
    }

    public void a(boolean z2, boolean z3, boolean z4, boolean z5) {
        n = z2;
        this.o = z3;
        this.p = z4;
        this.q = z5;
    }

    public void b() {
        int i2;
        ArrayList arrayList = new ArrayList();
        int i3 = 0;
        while (true) {
            int i4 = i3;
            if (i4 >= this.x.size()) {
                break;
            }
            int intValue = ((Integer) this.x.get(i4)).intValue();
            int intValue2 = ((Integer) this.aa.get(Integer.valueOf(intValue))).intValue();
            if (intValue2 > 0) {
                l d2 = d(intValue);
                int i5 = d2.j;
                int i6 = d2.u;
                if (i5 != 0) {
                    if (d2.j > 0) {
                        arrayList.add(new ScoreItem(intValue, intValue2, i5, i6, 0, false));
                    } else {
                        arrayList.add(new ScoreItem(intValue, intValue2, i5, i6, 0, true));
                    }
                } else if (d2.f == 4) {
                    arrayList.add(new ScoreItem(intValue, intValue2, i5, i6, 1, true));
                } else {
                    arrayList.add(new ScoreItem(intValue, intValue2, i5, i6, 1, false));
                }
            }
            i3 = i4 + 1;
        }
        int[] b2 = this.e.b();
        int i7 = 0;
        while (true) {
            int i8 = i7;
            if (i8 >= b2.length) {
                if (this.e.a() >= 0) {
                    arrayList.add(new ScoreItem(100, "Longest combo: " + this.e.a(), this.e.a() * 100));
                }
                Intent intent = new Intent(this.T, ScoreActivity.class);
                intent.putExtra("totalscore", this.e.f92a + (this.e.a() * 100));
                intent.putExtra("level", this.ac);
                intent.putExtra("levelupscore", this.ad);
                intent.putExtra("comboreadout", this.e.b);
                intent.putExtra("totalgametime", (float) (System.currentTimeMillis() - this.ae));
                intent.putParcelableArrayListExtra("scoreitems", arrayList);
                intent.putExtra("longestCombo", this.e.a());
                this.T.startActivity(intent);
                this.T.finish();
                return;
            }
            int i9 = i8 + 101;
            int i10 = b2[i8];
            if (i10 > 0) {
                int identifier = getResources().getIdentifier("score_combo_" + Integer.toString(i8 + 1) + "x", "drawable", this.T.getPackageName());
                switch (i8) {
                    case 0:
                        i2 = 20;
                        break;
                    case 1:
                        i2 = 40;
                        break;
                    case 2:
                        i2 = 60;
                        break;
                    case 3:
                        i2 = 100;
                        break;
                    case 4:
                        i2 = 150;
                        break;
                    case 5:
                        i2 = 200;
                        break;
                    default:
                        i2 = 0;
                        break;
                }
                arrayList.add(new ScoreItem(i9, i10, i2, identifier, 0, false));
            }
            i7 = i8 + 1;
        }
    }

    public void b(int i2) {
        if (this.o) {
            this.U.play(((Integer) this.V.get(Integer.valueOf(i2))).intValue(), this.ap, this.ap, 1, 0, 1.0f);
        }
    }

    public final void d() {
        new i(this).execute(Float.valueOf(aq));
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        int i2;
        if (motionEvent.getAction() == 0) {
            int x2 = (int) motionEvent.getX();
            int y2 = (int) motionEvent.getY();
            int i3 = 0;
            while (true) {
                if (i3 >= 6) {
                    break;
                } else if (this.M[i3].contains(x2, y2)) {
                    if (this.b.m) {
                        i2 = this.G[this.H];
                        this.H++;
                        if (this.H >= this.I) {
                            this.H = 0;
                        }
                    } else {
                        i2 = i3;
                    }
                    this.b.c(i2);
                } else {
                    i3++;
                }
            }
        }
        try {
            Thread.sleep(16);
        } catch (InterruptedException e2) {
        }
        return false;
    }

    public void surfaceChanged(SurfaceHolder surfaceHolder, int i2, int i3, int i4) {
    }

    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        g gVar = new g(this);
        this.r.b(this.q);
        this.r.a(true);
        this.r.start();
        gVar.start();
    }

    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        boolean z2 = true;
        if (this.p) {
            ar.stop();
            ar.release();
        }
        this.U.release();
        this.r.a(false);
        this.s.a(false);
        while (z2) {
            try {
                this.r.join();
                this.s.join();
                z2 = false;
            } catch (InterruptedException e2) {
            }
        }
        setOnTouchListener(null);
        this.b = null;
        this.r = null;
        this.s = null;
        this.Q = null;
        this.R = null;
        this.h = null;
        this.e = null;
    }
}
