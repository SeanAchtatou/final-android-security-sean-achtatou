package com.ElekaSoftware.OfficeRage;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

public class Tutorial extends Activity implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private Typeface f51a;

    public void onClick(View view) {
        if (view.getId() == C0000R.id.tutorial_back) {
            finish();
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        setContentView((int) C0000R.layout.tutorial_layout);
        ((ImageButton) findViewById(C0000R.id.tutorial_back)).setOnClickListener(this);
        this.f51a = Typeface.createFromAsset(getResources().getAssets(), "coolvetica.ttf");
        ((TextView) findViewById(C0000R.id.tutorial_header)).setTypeface(this.f51a);
        ((TextView) findViewById(C0000R.id.tutorial_text1)).setTypeface(this.f51a);
        ((TextView) findViewById(C0000R.id.tutorial_text2)).setTypeface(this.f51a);
        ((TextView) findViewById(C0000R.id.tutorial_text3)).setTypeface(this.f51a);
        ((TextView) findViewById(C0000R.id.tutorial_end)).setTypeface(this.f51a);
    }
}
