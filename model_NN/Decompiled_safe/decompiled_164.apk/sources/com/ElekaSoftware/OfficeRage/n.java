package com.ElekaSoftware.OfficeRage;

import android.widget.Toast;
import com.openfeint.api.a.j;
import com.openfeint.api.a.z;
import com.openfeint.internal.w;

final class n extends z {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ScoreActivity f125a;

    n(ScoreActivity scoreActivity) {
        this.f125a = scoreActivity;
    }

    public final void a(String str) {
        Toast.makeText(this.f125a, "Error (" + str + ") posting score.", 0).show();
    }

    public final void a(boolean z) {
        this.f125a.d = z;
        if (z) {
            new j(ScoreActivity.e[this.f125a.b]).a(w.a().d(), new ac(this));
            new j(ScoreActivity.e[0]).a(w.a().d(), new ab(this));
        }
    }
}
