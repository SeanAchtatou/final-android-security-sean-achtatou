package com.ElekaSoftware.OfficeRage;

import android.os.AsyncTask;

final class i extends AsyncTask {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ h f120a;

    /* synthetic */ i(h hVar) {
        this(hVar, (byte) 0);
    }

    private i(h hVar, byte b) {
        this.f120a = hVar;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public Void doInBackground(Float... fArr) {
        float floatValue = fArr[0].floatValue() / 20.0f;
        int i = 0;
        while (i < 20) {
            publishProgress(Float.valueOf(fArr[0].floatValue() - (((float) i) * floatValue)));
            try {
                Thread.sleep(100);
                i++;
            } catch (Exception e) {
            }
        }
        h.ar.setVolume(0.0f, 0.0f);
        return null;
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void onProgressUpdate(Object... objArr) {
        Float[] fArr = (Float[]) objArr;
        h.ar.setVolume(fArr[0].floatValue(), fArr[0].floatValue());
    }
}
