package com.ElekaSoftware.OfficeRage;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;

public final class q extends Drawable {

    /* renamed from: a  reason: collision with root package name */
    private final int f128a;
    private final int b;
    private final Bitmap c;
    private final int d;
    private final int e;
    private int f;
    private int g;
    private int h;
    private int i;
    private Rect j;
    private Rect k;
    private Rect l;
    private Rect m;
    private Rect n;
    private Rect o;
    private Rect p;
    private float q = 0.0f;
    private float r = 0.36f;

    public q(Context context, int i2, int i3) {
        this.c = BitmapFactory.decodeResource(context.getResources(), C0000R.drawable.scoreroller_numbersheet);
        this.d = i2;
        this.e = i3;
        this.f = 0;
        this.g = 0;
        this.f128a = this.c.getWidth() / 10;
        this.b = this.c.getHeight();
        this.k = new Rect(this.f * this.f128a, 0, (this.f + 1) * this.f128a, this.b);
        this.j = new Rect(this.f * this.f128a, 0, (this.f + 1) * this.f128a, this.b);
        this.l = new Rect(this.f * this.f128a, 0, (this.f + 1) * this.f128a, this.b);
        this.m = new Rect(this.d, (this.e - this.b) + ((int) this.q), this.d + this.f128a, (this.e - this.b) + ((int) this.q) + this.b);
        this.n = new Rect(this.d, this.e, this.d + this.f128a, this.e + this.b);
        this.o = new Rect(this.d, this.e + this.b + ((int) this.q), this.d + this.f128a, this.e + this.b + ((int) this.q) + this.b);
        this.p = new Rect(this.d, this.e, this.d + this.f128a, this.e + this.b);
        this.r = (8.0f * ((float) this.b)) / 1000.0f;
    }

    public final void a(float f2) {
        if (this.g != this.f) {
            this.q += this.r * f2;
            if (this.q > ((float) this.b)) {
                this.g++;
                this.q -= (float) this.b;
            }
            if (this.g > 9) {
                this.g = 0;
            }
            if (this.g == this.f) {
                this.q = 0.0f;
            }
            this.i = this.g - 1;
            this.h = this.g + 1;
            if (this.i < 0) {
                this.i = 9;
            }
            if (this.h > 9) {
                this.h = 0;
            }
            this.k.set(this.h * this.f128a, 0, (this.h + 1) * this.f128a, this.b);
            this.j.set(this.g * this.f128a, 0, (this.g + 1) * this.f128a, this.b);
            this.l.set(this.i * this.f128a, 0, (this.i + 1) * this.f128a, this.b);
            this.m.set(this.d, (this.e - this.b) + ((int) this.q), this.d + this.f128a, (this.e - this.b) + ((int) this.q) + this.b);
            this.n.set(this.d, this.e + ((int) this.q), this.d + this.f128a, this.e + ((int) this.q) + this.b);
            this.o.set(this.d, this.e + this.b + ((int) this.q), this.d + this.f128a, this.e + this.b + ((int) this.q) + this.b);
        }
    }

    public final void a(int i2) {
        this.f = i2;
    }

    public final void draw(Canvas canvas) {
        canvas.save();
        canvas.clipRect(this.p);
        canvas.drawBitmap(this.c, this.k, this.m, (Paint) null);
        canvas.drawBitmap(this.c, this.j, this.n, (Paint) null);
        canvas.drawBitmap(this.c, this.l, this.o, (Paint) null);
        canvas.restore();
    }

    public final int getOpacity() {
        return 0;
    }

    public final void setAlpha(int i2) {
    }

    public final void setColorFilter(ColorFilter colorFilter) {
    }
}
