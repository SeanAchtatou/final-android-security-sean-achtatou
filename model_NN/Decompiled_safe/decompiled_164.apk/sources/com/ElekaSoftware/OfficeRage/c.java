package com.ElekaSoftware.OfficeRage;

import android.content.SharedPreferences;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

final class c implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ Settings f101a;

    c(Settings settings) {
        this.f101a = settings;
    }

    public final void onClick(View view) {
        SharedPreferences.Editor edit = this.f101a.f50a.edit();
        edit.putInt("levelplayed", 0);
        edit.commit();
        f fVar = new f(this.f101a.getApplicationContext());
        try {
            fVar.b();
            fVar.c();
            fVar.close();
        } catch (Exception e) {
            Log.d("SelectorView", "EXCEPTION!");
        }
        Toast.makeText(this.f101a.getApplicationContext(), "Levels reset.", 0).show();
        this.f101a.i.dismiss();
    }
}
