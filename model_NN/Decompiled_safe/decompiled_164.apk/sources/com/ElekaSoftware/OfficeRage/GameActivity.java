package com.ElekaSoftware.OfficeRage;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;

public class GameActivity extends Activity {

    /* renamed from: a  reason: collision with root package name */
    private SharedPreferences f38a;
    private h b;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        getWindow().setFormat(1);
        this.f38a = getSharedPreferences("OfficeRagePreferences", 0);
        this.b = new h(this, this, getIntent().getExtras().getInt("level"));
        this.b.a(this.f38a.getBoolean("Vibration", true), this.f38a.getBoolean("SoundFX", true), this.f38a.getBoolean("Music", true), this.f38a.getBoolean("FPS", false));
        setContentView(this.b);
        this.b.requestFocus();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.b.c.clear();
        this.b.d.clear();
    }
}
