package com.ElekaSoftware.OfficeRage.b;

import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

public final class a extends DefaultHandler {

    /* renamed from: a  reason: collision with root package name */
    private b f99a;

    public final b a() {
        return this.f99a;
    }

    public final void characters(char[] cArr, int i, int i2) {
    }

    public final void endDocument() {
    }

    public final void endElement(String str, String str2, String str3) {
        if (!str2.equals("level") && !str2.equals("attributes") && !str2.equals("background-image") && !str2.equals("gameitems")) {
            str2.equals("item");
        }
    }

    public final void startDocument() {
        this.f99a = new b();
    }

    public final void startElement(String str, String str2, String str3, Attributes attributes) {
        int i;
        if (str2.equals("level")) {
            this.f99a.f100a = attributes.getValue("name");
            this.f99a.f = Integer.parseInt(attributes.getValue("id"));
        } else if (str2.equals("attributes")) {
            this.f99a.b = Integer.parseInt(attributes.getValue("levelupscore"));
            this.f99a.c = Integer.parseInt(attributes.getValue("gametime"));
            this.f99a.d = Integer.parseInt(attributes.getValue("throwintervalstart"));
            this.f99a.e = Integer.parseInt(attributes.getValue("throwintervalfinish"));
        } else if (str2.equals("gameitems")) {
            this.f99a.g = Integer.parseInt(attributes.getValue("maxtimebonusitems"));
            this.f99a.h = Integer.parseInt(attributes.getValue("itemrushcount"));
        } else if (str2.equals("item")) {
            int parseInt = Integer.parseInt(attributes.getValue("count"));
            for (int i2 = 0; i2 < parseInt; i2++) {
                String value = attributes.getValue("gameitem");
                if (!value.equalsIgnoreCase("computer")) {
                    if (value.equalsIgnoreCase("cactus")) {
                        i = 1;
                    } else if (value.equalsIgnoreCase("banana")) {
                        i = 2;
                    } else if (value.equalsIgnoreCase("mushroom")) {
                        i = 3;
                    } else if (value.equalsIgnoreCase("pills")) {
                        i = 4;
                    } else if (value.equalsIgnoreCase("anvil")) {
                        i = 5;
                    } else if (value.equalsIgnoreCase("coffeemug")) {
                        i = 6;
                    } else if (value.equalsIgnoreCase("smartphone")) {
                        i = 7;
                    } else if (value.equalsIgnoreCase("cola")) {
                        i = 8;
                    } else if (value.equalsIgnoreCase("candybar")) {
                        i = 9;
                    } else if (value.equalsIgnoreCase("keyboard")) {
                        i = 10;
                    } else if (value.equalsIgnoreCase("box")) {
                        i = 11;
                    } else if (value.equalsIgnoreCase("folder")) {
                        i = 12;
                    } else if (value.equalsIgnoreCase("lcd")) {
                        i = 13;
                    } else if (value.equalsIgnoreCase("printer")) {
                        i = 14;
                    } else if (value.equalsIgnoreCase("calculator")) {
                        i = 15;
                    } else if (value.equalsIgnoreCase("clock")) {
                        i = 16;
                    } else if (value.equalsIgnoreCase("briefcase")) {
                        i = 17;
                    } else if (value.equalsIgnoreCase("coffeemaker")) {
                        i = 18;
                    } else if (value.equalsIgnoreCase("cigarbox")) {
                        i = 19;
                    } else if (value.equalsIgnoreCase("fan")) {
                        i = 20;
                    }
                    this.f99a.i.add(Integer.valueOf(i));
                }
                i = 0;
                this.f99a.i.add(Integer.valueOf(i));
            }
        }
    }
}
