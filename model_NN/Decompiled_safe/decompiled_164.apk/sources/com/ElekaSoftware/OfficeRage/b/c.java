package com.ElekaSoftware.OfficeRage.b;

import android.content.Context;
import java.io.BufferedInputStream;
import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

public final class c {
    public static b a(Context context, String str) {
        try {
            XMLReader xMLReader = SAXParserFactory.newInstance().newSAXParser().getXMLReader();
            a aVar = new a();
            xMLReader.setContentHandler(aVar);
            xMLReader.parse(new InputSource(new BufferedInputStream(context.getAssets().open(str))));
            return aVar.a();
        } catch (IOException | ParserConfigurationException | SAXException e) {
            return null;
        }
    }
}
