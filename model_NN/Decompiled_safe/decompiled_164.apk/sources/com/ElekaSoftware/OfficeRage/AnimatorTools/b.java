package com.ElekaSoftware.OfficeRage.AnimatorTools;

import android.content.Context;
import android.graphics.Rect;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.View;
import com.ElekaSoftware.OfficeRage.a.l;
import com.ElekaSoftware.OfficeRage.d.j;
import com.ElekaSoftware.OfficeRage.h;

public final class b extends h {
    public j n;
    private String o = "AnimationToolsView";
    private a p;
    private Rect[] q;
    private Long r = 0L;
    private int s;
    private int t;
    private boolean u;
    private float v;
    private int w;
    private int x;

    public b(Context context, AnimationToolsActivity animationToolsActivity) {
        super(context, animationToolsActivity, 1);
        SurfaceHolder holder = getHolder();
        holder.addCallback(this);
        Display defaultDisplay = animationToolsActivity.getWindowManager().getDefaultDisplay();
        Log.d(this.o, Integer.toString(defaultDisplay.getWidth()));
        this.w = defaultDisplay.getWidth();
        Log.d(this.o, Integer.toString(defaultDisplay.getHeight()));
        this.x = defaultDisplay.getHeight();
        this.u = false;
        this.q = new Rect[9];
        this.q[0] = new Rect(0, 0, 80, 80);
        this.q[1] = new Rect(0, 100, 80, 180);
        this.q[2] = new Rect(0, 190, 80, 280);
        this.q[3] = new Rect(710, 0, 800, 80);
        this.q[4] = new Rect(710, 100, 800, 180);
        this.q[5] = new Rect(710, 190, 800, 280);
        this.q[6] = new Rect(710, 280, 800, 370);
        this.q[7] = new Rect(0, 288, 80, 360);
        this.q[8] = new Rect(0, 380, 80, 460);
        setOnTouchListener(this);
        this.n = new j(getContext(), this);
        this.p = new a(this, holder, this.w, this.x);
        this.s = 0;
        this.t = 0;
        this.v = 0.0f;
    }

    private static String a(float f) {
        return String.valueOf(Float.toString(((float) Math.round(f * 100.0f)) / 100.0f)) + "f,";
    }

    private void b(float f) {
        switch (this.s) {
            case 0:
                this.n.b.c += f;
                return;
            case 1:
                this.n.c.f106a += f;
                return;
            case 2:
                this.n.d.f109a += f;
                return;
            case 3:
                this.n.e.f108a += f;
                return;
            case 4:
                this.n.f.f114a += f;
                return;
            case 5:
                this.n.g.f104a += f;
                return;
            case 6:
                this.n.h.f112a += f;
                return;
            case 7:
                this.n.h.f.f111a += f;
                return;
            case 8:
                this.n.i.f105a += f;
                return;
            case 9:
                this.n.i.f.f110a += f;
                return;
            default:
                return;
        }
    }

    public final synchronized void a() {
    }

    public final void a(int i) {
    }

    public final synchronized void a(l lVar) {
    }

    public final void a(boolean z, boolean z2, boolean z3, boolean z4) {
    }

    public final void b() {
    }

    public final void b(int i) {
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        float x2 = motionEvent.getX();
        float y = motionEvent.getY();
        if (motionEvent.getAction() == 0) {
            this.v = x2;
            this.r = Long.valueOf(System.currentTimeMillis());
            int i = 0;
            while (true) {
                if (i >= 9) {
                    break;
                } else if (this.q[i].contains((int) x2, (int) y)) {
                    this.u = true;
                    switch (i) {
                        case 0:
                            Log.d(this.o, "do magic");
                            float width = (this.n.b.f107a - this.n.l[0]) / ((float) this.k.width());
                            float height = (this.n.b.b - this.n.l[1]) / ((float) this.k.height());
                            Log.d(this.o, "****** Animation Output ******");
                            Log.d(this.o, "Torso x: " + width + " Torso y: " + height);
                            Log.d(this.o, "Torso rotation: " + this.n.b.c);
                            Log.d(this.o, "Head rotation: " + this.n.b.e.f106a);
                            Log.d(this.o, "LeftUpperArm rotation: " + this.n.b.h.f109a);
                            Log.d(this.o, "LeftLowerArm rotation: " + this.n.b.h.b.f108a);
                            Log.d(this.o, "RightUpperArm rotation: " + this.n.b.k.f114a);
                            Log.d(this.o, "RightLowerArm rotation: " + this.n.b.k.b.f104a);
                            Log.d(this.o, "LeftUpperLeg rotation: " + this.n.b.n.f112a);
                            Log.d(this.o, "LeftLowerLeg rotation: " + this.n.b.n.f.f111a);
                            Log.d(this.o, "RightUpperLeg rotation: " + this.n.b.q.f105a);
                            Log.d(this.o, "RightLowerLeg rotation: " + this.n.b.q.f.f110a);
                            Log.d(this.o, "{" + a(width) + a(height) + a(this.n.b.c) + a(this.n.b.e.f106a) + a(this.n.b.h.f109a) + a(this.n.b.h.b.f108a) + a(this.n.b.k.f114a) + a(this.n.b.k.b.f104a) + a(this.n.b.n.f112a) + a(this.n.b.n.f.f111a) + a(this.n.b.q.f105a) + Float.toString(((float) Math.round(this.n.b.q.f.f110a * 10.0f)) / 10.0f) + "f" + "}");
                            break;
                        case 1:
                            Log.d(this.o, "rotate ccw");
                            b(-1.0f);
                            break;
                        case 2:
                            Log.d(this.o, "rotate cw");
                            b(1.0f);
                            break;
                        case 3:
                            Log.d(this.o, "ylï¿½s");
                            this.n.b.b -= 3.0f;
                            break;
                        case 4:
                            Log.d(this.o, "alas");
                            this.n.b.b += 3.0f;
                            break;
                        case 5:
                            Log.d(this.o, "vasemmalle");
                            this.n.b.f107a -= 3.0f;
                            break;
                        case 6:
                            Log.d(this.o, "oikealle");
                            this.n.b.f107a += 3.0f;
                            break;
                        case 7:
                            Log.d(this.o, "next bodypart");
                            this.t = this.s;
                            this.s++;
                            if (this.s > 9) {
                                this.s = 0;
                            }
                            switch (this.s) {
                                case 0:
                                    this.n.b.t = true;
                                    break;
                                case 1:
                                    this.n.c.b = true;
                                    break;
                                case 2:
                                    this.n.d.f = true;
                                    break;
                                case 3:
                                    this.n.e.b = true;
                                    break;
                                case 4:
                                    this.n.f.f = true;
                                    break;
                                case 5:
                                    this.n.g.b = true;
                                    break;
                                case 6:
                                    this.n.h.c = true;
                                    break;
                                case 7:
                                    this.n.k.b = true;
                                    break;
                                case 8:
                                    this.n.i.c = true;
                                    break;
                                case 9:
                                    this.n.j.b = true;
                                    break;
                            }
                            switch (this.t) {
                                case 0:
                                    this.n.b.t = false;
                                    break;
                                case 1:
                                    this.n.c.b = false;
                                    break;
                                case 2:
                                    this.n.d.f = false;
                                    break;
                                case 3:
                                    this.n.e.b = false;
                                    break;
                                case 4:
                                    this.n.f.f = false;
                                    break;
                                case 5:
                                    this.n.g.b = false;
                                    break;
                                case 6:
                                    this.n.h.c = false;
                                    break;
                                case 7:
                                    this.n.k.b = false;
                                    break;
                                case 8:
                                    this.n.i.c = false;
                                    break;
                                case 9:
                                    this.n.j.b = false;
                                    break;
                            }
                        case 8:
                            Log.d(this.o, "prev bodypart");
                            this.t = this.s;
                            this.s--;
                            if (this.s < 0) {
                                this.s = 9;
                            }
                            switch (this.s) {
                                case 0:
                                    this.n.b.t = true;
                                    break;
                                case 1:
                                    this.n.c.b = true;
                                    break;
                                case 2:
                                    this.n.d.f = true;
                                    break;
                                case 3:
                                    this.n.e.b = true;
                                    break;
                                case 4:
                                    this.n.f.f = true;
                                    break;
                                case 5:
                                    this.n.g.b = true;
                                    break;
                                case 6:
                                    this.n.h.c = true;
                                    break;
                                case 7:
                                    this.n.k.b = true;
                                    break;
                                case 8:
                                    this.n.i.c = true;
                                    break;
                                case 9:
                                    this.n.j.b = true;
                                    break;
                            }
                            switch (this.t) {
                                case 0:
                                    this.n.b.t = false;
                                    break;
                                case 1:
                                    this.n.c.b = false;
                                    break;
                                case 2:
                                    this.n.d.f = false;
                                    break;
                                case 3:
                                    this.n.e.b = false;
                                    break;
                                case 4:
                                    this.n.f.f = false;
                                    break;
                                case 5:
                                    this.n.g.b = false;
                                    break;
                                case 6:
                                    this.n.h.c = false;
                                    break;
                                case 7:
                                    this.n.k.b = false;
                                    break;
                                case 8:
                                    this.n.i.c = false;
                                    break;
                                case 9:
                                    this.n.j.b = false;
                                    break;
                            }
                    }
                } else {
                    i++;
                }
            }
        }
        if (motionEvent.getAction() == 2 && System.currentTimeMillis() - this.r.longValue() > 20 && !this.u) {
            this.r = Long.valueOf(System.currentTimeMillis());
            b((x2 - this.v) / 10.0f);
            this.v = x2;
        }
        if (motionEvent.getAction() == 1) {
            this.u = false;
        }
        return true;
    }

    public final void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i2, int i3) {
        Log.d(this.o, "Surface changed");
    }

    public final void surfaceCreated(SurfaceHolder surfaceHolder) {
        Log.v(this.o, "Surface created");
        this.p.a(true);
        this.p.start();
    }

    public final void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        Log.v(this.o, "Surface destroyed");
        boolean z = true;
        this.p.a(false);
        while (z) {
            try {
                this.p.join();
                z = false;
            } catch (InterruptedException e) {
            }
        }
    }
}
