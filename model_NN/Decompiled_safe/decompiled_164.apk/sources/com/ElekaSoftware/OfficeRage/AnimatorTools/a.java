package com.ElekaSoftware.OfficeRage.AnimatorTools;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.SurfaceHolder;
import com.ElekaSoftware.OfficeRage.C0000R;

public final class a extends Thread {

    /* renamed from: a  reason: collision with root package name */
    private boolean f37a;
    private SurfaceHolder b;
    private long c = 0;
    private int d = 0;
    private int e = 0;
    private int f = 0;
    private Paint g;
    private Bitmap h;
    private Rect i;
    private Rect j;
    private int k;
    private int l;
    private b m;

    public a(b bVar, SurfaceHolder surfaceHolder, int i2, int i3) {
        this.b = surfaceHolder;
        this.k = i2;
        this.l = i3;
        this.g = new Paint();
        this.g.setARGB(255, 255, 255, 255);
        this.g.setTextSize(32.0f);
        this.m = bVar;
        this.h = a();
        this.i = new Rect(0, 0, this.h.getWidth(), this.h.getHeight());
        this.j = new Rect(0, 0, 800, 480);
    }

    private Bitmap a() {
        Bitmap createBitmap = Bitmap.createBitmap(this.k, this.l, Bitmap.Config.RGB_565);
        Bitmap decodeResource = BitmapFactory.decodeResource(this.m.getResources(), C0000R.drawable.animatorbackground);
        Canvas canvas = new Canvas(createBitmap);
        canvas.drawBitmap(decodeResource, new Rect(0, 0, decodeResource.getWidth(), decodeResource.getHeight()), new Rect(0, 0, this.k, this.l), (Paint) null);
        Paint paint = new Paint();
        paint.setColor(-1);
        for (int i2 = 0; i2 < 6; i2++) {
            canvas.drawCircle((float) this.m.f119a[i2].x, (float) this.m.f119a[i2].y, 5.0f, paint);
        }
        return createBitmap;
    }

    public final void a(boolean z) {
        this.f37a = z;
    }

    public final void run() {
        Canvas canvas;
        while (this.f37a) {
            try {
                Canvas lockCanvas = this.b.lockCanvas();
                try {
                    synchronized (this.b) {
                        this.m.n.a();
                        long currentTimeMillis = System.currentTimeMillis();
                        if (this.c != 0) {
                            this.e = ((int) (currentTimeMillis - this.c)) + this.e;
                            this.d++;
                            if (this.d == 10) {
                                this.f = 10000 / this.e;
                                this.e = 0;
                                this.d = 0;
                            }
                        }
                        this.c = currentTimeMillis;
                        lockCanvas.drawBitmap(this.h, this.i, this.j, (Paint) null);
                        this.m.n.draw(lockCanvas);
                        lockCanvas.drawText(String.valueOf(this.f) + " fps", (float) (lockCanvas.getWidth() / 2), (float) (lockCanvas.getHeight() / 2), this.g);
                    }
                    if (lockCanvas != null) {
                        this.b.unlockCanvasAndPost(lockCanvas);
                    }
                } catch (Throwable th) {
                    Throwable th2 = th;
                    canvas = lockCanvas;
                    th = th2;
                }
            } catch (Throwable th3) {
                th = th3;
                canvas = null;
            }
        }
        return;
        if (canvas != null) {
            this.b.unlockCanvasAndPost(canvas);
        }
        throw th;
    }
}
