package com.ElekaSoftware.OfficeRage;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

public class About extends Activity implements View.OnClickListener {
    public void onClick(View view) {
        switch (view.getId()) {
            case C0000R.id.about_tutorial_button /*2131361792*/:
                startActivity(new Intent(this, Tutorial.class));
                return;
            case C0000R.id.about_devel_text /*2131361793*/:
            case C0000R.id.about_eleka_logo /*2131361794*/:
            default:
                return;
            case C0000R.id.about_back /*2131361795*/:
                finish();
                return;
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        setContentView((int) C0000R.layout.about_layout);
        ((Button) findViewById(C0000R.id.about_tutorial_button)).setOnClickListener(this);
        ((ImageButton) findViewById(C0000R.id.about_back)).setOnClickListener(this);
    }
}
