package com.ElekaSoftware.OfficeRage;

import android.os.Parcel;
import android.os.Parcelable;

final class ae implements Parcelable.Creator {
    ae() {
    }

    public final /* bridge */ /* synthetic */ Object createFromParcel(Parcel parcel) {
        return new ScoreItem(parcel);
    }

    public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
        return new ScoreItem[i];
    }
}
