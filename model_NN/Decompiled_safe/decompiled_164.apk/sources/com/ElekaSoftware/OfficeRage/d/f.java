package com.ElekaSoftware.OfficeRage.d;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.LightingColorFilter;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.util.FloatMath;
import com.ElekaSoftware.OfficeRage.C0000R;

public final class f extends Drawable {

    /* renamed from: a  reason: collision with root package name */
    public float f109a;
    public final e b;
    protected float c;
    protected float d;
    protected d e;
    public boolean f;
    private final Bitmap g;
    private float h;
    private float i;
    private float j;
    private float k;
    private Paint l;
    private LightingColorFilter m;
    private int n;
    private final int o = ((int) (((float) this.g.getWidth()) * 0.073f));
    private final int p = ((int) (((float) this.g.getHeight()) * 0.5f));

    public f(d dVar, float f2, float f3, float f4) {
        this.g = dVar.a(C0000R.drawable.player_arm_upper_left);
        this.h = f2;
        this.i = f3;
        this.j = f2 - ((float) this.o);
        this.k = f3 - ((float) this.p);
        this.f109a = f4;
        this.e = dVar;
        this.f = false;
        this.l = new Paint();
        this.m = new LightingColorFilter(200, 112);
        this.l.setColorFilter(this.m);
        this.n = (int) (0.1875f * dVar.u);
        this.c = (((float) this.n) * FloatMath.cos(this.f109a * 0.01745278f)) + f2;
        this.d = (((float) this.n) * FloatMath.sin(this.f109a * 0.01745278f)) + f3;
        this.b = new e(this, this.c, this.d, dVar.d.l[5]);
    }

    public final void a() {
        this.h = this.e.i;
        this.i = this.e.j;
        this.j = this.h - ((float) this.o);
        this.k = this.i - ((float) this.p);
        this.c = this.h + (((float) this.n) * FloatMath.cos(this.f109a * 0.01745278f));
        this.d = this.i + (((float) this.n) * FloatMath.sin(this.f109a * 0.01745278f));
        this.b.a();
    }

    public final void draw(Canvas canvas) {
        this.b.draw(canvas);
        canvas.save();
        canvas.rotate(this.f109a, this.h, this.i);
        if (this.f) {
            canvas.drawBitmap(this.g, this.j, this.k, this.l);
        } else {
            canvas.drawBitmap(this.g, this.j, this.k, (Paint) null);
        }
        canvas.restore();
    }

    public final int getOpacity() {
        return 0;
    }

    public final void setAlpha(int i2) {
    }

    public final void setColorFilter(ColorFilter colorFilter) {
    }
}
