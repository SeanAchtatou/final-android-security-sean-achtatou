package com.ElekaSoftware.OfficeRage.d;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.LightingColorFilter;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import com.ElekaSoftware.OfficeRage.C0000R;

public final class c extends Drawable {

    /* renamed from: a  reason: collision with root package name */
    public float f106a;
    public boolean b;
    private Bitmap c = this.d;
    private final Bitmap d;
    private final Bitmap e;
    private final Bitmap f;
    private float g;
    private float h;
    private float i;
    private float j;
    private d k;
    private Paint l;
    private LightingColorFilter m;
    private boolean n;
    private Paint o;
    private LightingColorFilter p;
    private int q = (this.c.getWidth() / 2);
    private int r = this.c.getHeight();

    public c(d dVar, float f2, float f3, float f4) {
        this.d = dVar.a(C0000R.drawable.player_head1);
        this.e = dVar.a(C0000R.drawable.player_head2);
        this.f = dVar.a(C0000R.drawable.player_head3);
        this.i = f2 - ((float) (this.c.getWidth() / 2));
        this.j = f3 - ((float) this.c.getHeight());
        this.g = f2;
        this.h = f3;
        this.f106a = f4;
        this.k = dVar;
        this.b = false;
        this.l = new Paint();
        this.m = new LightingColorFilter(200, 112);
        this.l.setColorFilter(this.m);
        this.n = false;
        this.o = new Paint();
        this.p = new LightingColorFilter(-1, 8192);
        this.o.setColorFilter(this.p);
    }

    public final void a() {
        this.g = this.k.f;
        this.h = this.k.g;
        this.i = this.g - ((float) this.q);
        this.j = this.h - ((float) this.r);
    }

    public final void a(int i2) {
        switch (i2) {
            case 1:
                this.c = this.d;
                break;
            case 2:
                this.c = this.e;
                break;
            case 3:
                this.c = this.f;
                break;
        }
        this.q = this.c.getWidth() / 2;
        this.r = this.c.getHeight();
    }

    public final void b() {
        this.n = true;
    }

    public final void c() {
        this.n = false;
    }

    public final void draw(Canvas canvas) {
        canvas.save();
        canvas.rotate(this.f106a, this.g, this.h);
        if (this.b) {
            canvas.drawBitmap(this.c, this.i, this.j, this.l);
        } else if (!this.n) {
            canvas.drawBitmap(this.c, this.i, this.j, (Paint) null);
        } else {
            canvas.drawBitmap(this.c, this.i, this.j, this.o);
        }
        canvas.restore();
    }

    public final int getOpacity() {
        return 0;
    }

    public final void setAlpha(int i2) {
    }

    public final void setColorFilter(ColorFilter colorFilter) {
    }
}
