package com.ElekaSoftware.OfficeRage.d;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.LightingColorFilter;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import com.ElekaSoftware.OfficeRage.C0000R;

public final class g extends Drawable {

    /* renamed from: a  reason: collision with root package name */
    public float f110a;
    public boolean b;
    private final Bitmap c;
    private float d;
    private float e;
    private float f;
    private float g;
    private b h;
    private final Paint i;
    private final LightingColorFilter j;
    private final int k = ((int) (((float) this.c.getWidth()) / 10.0f));
    private final int l = ((int) (((float) this.c.getHeight()) * 0.612f));

    public g(b bVar, float f2, float f3, float f4) {
        this.c = bVar.b.a(C0000R.drawable.player_leg_lower_right);
        this.d = f2;
        this.e = f3;
        this.f = f2 - ((float) this.k);
        this.g = f3 - ((float) this.l);
        this.f110a = f4;
        this.h = bVar;
        this.b = false;
        this.i = new Paint();
        this.j = new LightingColorFilter(200, 112);
        this.i.setColorFilter(this.j);
    }

    public final void a() {
        this.d = this.h.d;
        this.e = this.h.e;
        this.f = this.d - ((float) this.k);
        this.g = this.e - ((float) this.l);
    }

    public final void draw(Canvas canvas) {
        canvas.save();
        canvas.rotate(this.f110a, this.d, this.e);
        if (this.b) {
            canvas.drawBitmap(this.c, this.f, this.g, this.i);
        } else {
            canvas.drawBitmap(this.c, this.f, this.g, (Paint) null);
        }
        canvas.restore();
    }

    public final int getOpacity() {
        return 0;
    }

    public final void setAlpha(int i2) {
    }

    public final void setColorFilter(ColorFilter colorFilter) {
    }
}
