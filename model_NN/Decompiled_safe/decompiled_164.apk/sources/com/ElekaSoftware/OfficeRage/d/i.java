package com.ElekaSoftware.OfficeRage.d;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.LightingColorFilter;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.util.FloatMath;
import com.ElekaSoftware.OfficeRage.C0000R;

public final class i extends Drawable {

    /* renamed from: a  reason: collision with root package name */
    public float f112a;
    public final d b;
    public boolean c;
    protected float d;
    protected float e;
    public final h f;
    private Bitmap g;
    private float h;
    private float i;
    private float j;
    private float k;
    private final Paint l;
    private final LightingColorFilter m;
    private final int n = ((int) (((float) this.g.getWidth()) / 10.0f));
    private final int o = ((int) (((float) this.g.getHeight()) / 2.0f));
    private int p;

    public i(d dVar, float f2, float f3, float f4) {
        this.g = dVar.a(C0000R.drawable.player_leg_upper_left);
        this.h = f2;
        this.i = f3;
        this.j = f2 - ((float) this.n);
        this.k = f3 - ((float) this.o);
        this.f112a = f4;
        this.b = dVar;
        this.c = false;
        this.l = new Paint();
        this.m = new LightingColorFilter(200, 112);
        this.l.setColorFilter(this.m);
        this.p = (int) (0.15f * dVar.u);
        this.d = (((float) this.p) * FloatMath.cos(this.f112a * 0.01745278f)) + f2;
        this.e = (((float) this.p) * FloatMath.sin(this.f112a * 0.01745278f)) + f3;
        this.f = new h(this, this.d, this.e, dVar.d.l[5]);
    }

    public final void a() {
        this.h = this.b.o;
        this.i = this.b.p;
        this.d = this.h + (((float) this.p) * FloatMath.cos(this.f112a * 0.01745278f));
        this.e = this.i + (((float) this.p) * FloatMath.sin(this.f112a * 0.01745278f));
        this.f.a();
        this.j = this.h - ((float) this.n);
        this.k = this.i - ((float) this.o);
    }

    public final void draw(Canvas canvas) {
        this.f.draw(canvas);
        canvas.save();
        canvas.rotate(this.f112a, this.h, this.i);
        if (this.c) {
            canvas.drawBitmap(this.g, this.j, this.k, this.l);
        } else {
            canvas.drawBitmap(this.g, this.j, this.k, (Paint) null);
        }
        canvas.restore();
    }

    public final int getOpacity() {
        return 0;
    }

    public final void setAlpha(int i2) {
    }

    public final void setColorFilter(ColorFilter colorFilter) {
    }
}
