package com.ElekaSoftware.OfficeRage.d;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.drawable.Drawable;
import com.ElekaSoftware.OfficeRage.h;

public final class j extends Drawable {

    /* renamed from: a  reason: collision with root package name */
    h f113a;
    public final d b;
    public final c c;
    public final f d;
    public final e e;
    public final k f;
    public final a g;
    public final i h;
    public final b i;
    public final g j;
    public final h k;
    public final float[] l;
    public boolean m;
    private int n;
    private int o = 180;
    private int p = 33;
    private boolean q = false;
    private int r;
    private Long s;
    private float t;
    private float[] u;
    private boolean v;
    private l w;
    private long x;
    private int y;
    private float z;

    public j(Context context, h hVar) {
        this.f113a = hVar;
        this.n = 0;
        this.o = this.f113a.k.centerX();
        this.p = this.f113a.k.bottom - (this.f113a.k.height() / 4);
        this.l = new float[]{(float) this.o, (float) this.p, 0.0f, 0.0f, 124.4f, 87.2f, 49.5f, 97.6f, 100.0f, 80.0f, 94.0f, 94.0f};
        this.b = new d(context, this, this.l[0], this.l[1], this.l[2]);
        this.c = this.b.e;
        this.d = this.b.h;
        this.e = this.b.h.b;
        this.f = this.b.k;
        this.g = this.b.k.b;
        this.h = this.b.n;
        this.i = this.b.q;
        this.k = this.b.n.f;
        this.j = this.b.q.f;
        this.v = false;
        this.w = new l(this.f113a.k.width(), this.f113a.k.height());
        this.m = false;
    }

    public final void a() {
        if (this.q) {
            this.t = (float) (System.currentTimeMillis() - this.s.longValue());
            if (this.t < 125.0f) {
                this.z = this.t / 125.0f;
                this.b.f107a = (this.z * this.u[0]) + this.l[0];
                this.b.b = (this.z * this.u[1]) + this.l[1];
                this.b.c = (this.z * (this.u[2] - this.l[2])) + this.l[2];
                this.c.f106a = (this.z * (this.u[3] - this.l[3])) + this.l[3];
                this.d.f109a = (this.z * (this.u[4] - this.l[4])) + this.l[4];
                this.e.f108a = (this.z * (this.u[5] - this.l[5])) + this.l[5];
                this.f.f114a = this.l[6] + (this.z * (this.u[6] - this.l[6]));
                this.g.f104a = this.l[7] + (this.z * (this.u[7] - this.l[7]));
                this.h.f112a = (this.z * (this.u[8] - this.l[8])) + this.l[8];
                this.k.f111a = (this.z * (this.u[9] - this.l[9])) + this.l[9];
                this.i.f105a = this.l[10] + (this.z * (this.u[10] - this.l[10]));
                this.j.f110a = this.l[11] + (this.z * (this.u[11] - this.l[11]));
            } else if (this.t < 300.0f) {
                if (!this.v) {
                    this.f113a.a(this.r);
                    this.v = true;
                }
                this.z = (300.0f - this.t) / 175.0f;
                this.b.f107a = (this.z * this.u[0]) + this.l[0];
                this.b.b = (this.z * this.u[1]) + this.l[1];
                this.b.c = (this.z * (this.u[2] - this.l[2])) + this.l[2];
                this.c.f106a = (this.z * (this.u[3] - this.l[3])) + this.l[3];
                this.d.f109a = (this.z * (this.u[4] - this.l[4])) + this.l[4];
                this.e.f108a = (this.z * (this.u[5] - this.l[5])) + this.l[5];
                this.f.f114a = this.l[6] + (this.z * (this.u[6] - this.l[6]));
                this.g.f104a = this.l[7] + (this.z * (this.u[7] - this.l[7]));
                this.h.f112a = (this.z * (this.u[8] - this.l[8])) + this.l[8];
                this.k.f111a = (this.z * (this.u[9] - this.l[9])) + this.l[9];
                this.i.f105a = this.l[10] + (this.z * (this.u[10] - this.l[10]));
                this.j.f110a = this.l[11] + (this.z * (this.u[11] - this.l[11]));
            } else {
                this.v = false;
                if (this.n > 0) {
                    this.u = this.w.a(this.n);
                    this.r = this.n;
                    this.n = 0;
                    this.s = Long.valueOf(System.currentTimeMillis());
                } else {
                    this.b.f107a = this.l[0];
                    this.b.b = this.l[1];
                    this.b.c = this.l[2];
                    this.c.f106a = this.l[3];
                    this.d.f109a = this.l[4];
                    this.e.f108a = this.l[5];
                    this.f.f114a = this.l[6];
                    this.g.f104a = this.l[7];
                    this.h.f112a = this.l[8];
                    this.k.f111a = this.l[9];
                    this.i.f105a = this.l[10];
                    this.j.f110a = this.l[11];
                    this.q = false;
                }
            }
        }
        if (this.m && System.currentTimeMillis() - this.x > ((long) this.y)) {
            this.c.c();
            this.m = false;
        }
        this.b.a();
    }

    public final void a(int i2) {
        this.c.a(i2);
    }

    public final void b(int i2) {
        this.x = System.currentTimeMillis();
        this.y = i2 * 1000;
        this.m = true;
        this.c.b();
    }

    public final void c(int i2) {
        if (this.q) {
            this.n = i2;
            return;
        }
        this.q = true;
        this.s = Long.valueOf(System.currentTimeMillis());
        this.r = i2;
        this.u = this.w.a(i2);
        this.v = false;
    }

    public final void draw(Canvas canvas) {
        this.b.draw(canvas);
    }

    public final int getOpacity() {
        return 0;
    }

    public final void setAlpha(int i2) {
    }

    public final void setColorFilter(ColorFilter colorFilter) {
    }
}
