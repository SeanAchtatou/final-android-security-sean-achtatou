package com.ElekaSoftware.OfficeRage.d;

import java.util.Random;

public final class l {

    /* renamed from: a  reason: collision with root package name */
    private Random f115a = new Random();
    private int[] b = new int[33];
    private int c = 0;
    private int d = this.b.length;
    private float[][] e = {new float[]{-0.06f, 0.0f, -13.77f, -10.78f, 208.72f, 225.81f, 39.04f, 68.19f, 118.72f, 87.2f, 51.41f, 87.2f}, new float[]{-0.1f, -0.04f, -11.45f, -6.63f, 193.78f, 222.2f, 38.79f, 65.52f, 100.0f, 87.2f, 80.0f, 87.2f}};
    private float[][] f = {new float[]{0.0f, 0.0625f, -20.0f, -25.8f, 148.8f, 205.8f, 33.5f, 62.0f, 134.3f, 87.2f, 55.8f, 101.6f}, new float[]{-0.15375f, -0.01875f, 45.5f, 12.1f, 124.4f, 206.3f, 60.9f, 162.4f, 224.8f, 227.3f, 94.8f, 87.2f}};
    private float[][] g = {new float[]{-0.09375f, 0.01875f, 18.2f, -11.4f, 140.9f, 116.4f, 72.8f, 97.6f, 172.5f, 153.6f, 47.9f, 125.5f}, new float[]{0.0f, 0.0375f, -47.7f, -36.2f, 102.9f, 138.6f, 15.1f, 48.8f, 147.6f, 87.2f, 97.7f, 57.6f}};
    private float[][] h = {new float[]{0.019f, -0.019f, 22.7f, 12.8f, 145.2f, 117.8f, -27.7f, -58.8f, 113.8f, 106.3f, 90.4f, 87.2f}, new float[]{0.03f, -0.069f, 24.0f, 0.0f, 165.0f, 151.7f, -14.8f, -53.7f, 142.4f, 77.3f, 43.7f, 123.1f}};
    private float[][] i = {new float[]{0.03f, 0.01f, 15.85f, 13.34f, 133.61f, 108.95f, 45.91f, -6.2f, 86.34f, 115.36f, 56.22f, 108.1f}, new float[]{0.15f, -0.03f, -42.8f, -32.6f, 117.3f, 11.3f, 14.3f, 21.4f, 100.0f, 87.2f, -30.7f, -49.9f}};
    private float[][] j = {new float[]{0.21f, 0.0f, -23.77f, -18.09f, 124.4f, 87.2f, 31.31f, 48.84f, 122.22f, 87.2f, 22.34f, 5.9f}, new float[]{0.09f, 0.09f, 33.85f, 24.32f, 158.76f, 124.47f, 72.24f, 62.69f, 64.66f, 125.63f, 20.6f, 87.2f}};

    public l(int i2, int i3) {
        for (int i4 = 0; i4 < this.d; i4++) {
            this.b[i4] = this.f115a.nextInt(2);
        }
        for (int i5 = 0; i5 < 2; i5++) {
            this.e[i5][0] = this.e[i5][0] * ((float) i2);
            this.e[i5][1] = this.e[i5][1] * ((float) i3);
        }
        for (int i6 = 0; i6 < 2; i6++) {
            this.f[i6][0] = this.f[i6][0] * ((float) i2);
            this.f[i6][1] = this.f[i6][1] * ((float) i3);
        }
        for (int i7 = 0; i7 < 2; i7++) {
            this.g[i7][0] = this.g[i7][0] * ((float) i2);
            this.g[i7][1] = this.g[i7][1] * ((float) i3);
        }
        for (int i8 = 0; i8 < 2; i8++) {
            this.h[i8][0] = this.h[i8][0] * ((float) i2);
            this.h[i8][1] = this.h[i8][1] * ((float) i3);
        }
        for (int i9 = 0; i9 < 2; i9++) {
            this.i[i9][0] = this.i[i9][0] * ((float) i2);
            this.i[i9][1] = this.i[i9][1] * ((float) i3);
        }
        for (int i10 = 0; i10 < 2; i10++) {
            this.j[i10][0] = this.j[i10][0] * ((float) i2);
            this.j[i10][1] = this.j[i10][1] * ((float) i3);
        }
    }

    public final float[] a(int i2) {
        this.c++;
        if (this.c >= this.d) {
            this.c = 0;
        }
        switch (i2) {
            case 0:
                return this.e[this.b[this.c]];
            case 1:
                return this.f[this.b[this.c]];
            case 2:
                return this.g[this.b[this.c]];
            case 3:
                return this.h[this.b[this.c]];
            case 4:
                return this.i[this.b[this.c]];
            case 5:
                return this.j[this.b[this.c]];
            default:
                return null;
        }
    }
}
