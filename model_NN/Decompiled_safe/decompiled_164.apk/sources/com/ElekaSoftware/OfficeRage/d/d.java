package com.ElekaSoftware.OfficeRage.d;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.LightingColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.FloatMath;
import com.ElekaSoftware.OfficeRage.C0000R;

public final class d extends Drawable {
    private final LightingColorFilter A;
    private float B;
    private final int C;
    private final int D;
    private final int E;
    private final int F;
    private final int G;
    private Paint H = new Paint();

    /* renamed from: a  reason: collision with root package name */
    public float f107a;
    public float b;
    public float c;
    protected final j d;
    public final c e;
    protected float f;
    protected float g;
    public final f h;
    protected float i;
    protected float j;
    public final k k;
    protected float l;
    protected float m;
    public final i n;
    protected float o;
    protected float p;
    public final b q;
    protected float r;
    protected float s;
    public boolean t;
    protected float u;
    private final Bitmap v;
    private Context w;
    private float x;
    private float y;
    private final Paint z;

    public d(Context context, j jVar, float f2, float f3, float f4) {
        this.d = jVar;
        this.w = context;
        Bitmap decodeResource = BitmapFactory.decodeResource(context.getResources(), C0000R.drawable.player_torso);
        this.u = (float) jVar.f113a.k.height();
        this.B = (0.5f * this.u) / ((float) decodeResource.getHeight());
        this.H.setFilterBitmap(true);
        this.H.setAntiAlias(true);
        this.H.setDither(false);
        this.v = a(C0000R.drawable.player_torso);
        this.f107a = f2;
        this.b = f3;
        this.C = this.v.getWidth() / 2;
        this.D = (int) ((9.0f * ((float) this.v.getHeight())) / 10.0f);
        this.x = f2 - ((float) this.C);
        this.y = f3 - ((float) this.D);
        this.t = false;
        this.z = new Paint();
        this.A = new LightingColorFilter(200, 112);
        this.z.setColorFilter(this.A);
        this.c = f4;
        this.E = (int) (0.398f * this.u);
        this.f = f2 - (((float) this.E) * FloatMath.cos((f4 + 90.0f) * 0.01745278f));
        this.g = f3 - (((float) this.E) * FloatMath.sin((f4 + 90.0f) * 0.01745278f));
        this.e = new c(this, this.f, this.g, jVar.l[3]);
        this.F = (int) (0.06f * this.u);
        this.i = this.f - (((float) this.F) * FloatMath.cos(f4 * 0.01745278f));
        this.j = this.g - (((float) this.F) * FloatMath.sin(f4 * 0.01745278f));
        this.h = new f(this, this.i, this.j, jVar.l[4]);
        this.l = this.f + (((float) this.F) * FloatMath.cos(f4 * 0.01745278f));
        this.m = this.g + (((float) this.F) * FloatMath.sin(f4 * 0.01745278f));
        this.k = new k(this, this.l, this.m, jVar.l[6]);
        this.G = (int) (0.05f * this.u);
        this.o = f2 - (((float) this.G) * FloatMath.cos(f4 * 0.01745278f));
        this.p = f3 - (((float) this.G) * FloatMath.sin(f4 * 0.01745278f));
        this.n = new i(this, this.o, this.p, jVar.l[8]);
        this.r = (((float) this.G) * FloatMath.cos(f4 * 0.01745278f)) + f2;
        this.s = (((float) this.G) * FloatMath.sin(f4 * 0.01745278f)) + f3;
        this.q = new b(this, this.r, this.s, jVar.l[9]);
    }

    /* access modifiers changed from: protected */
    public final Bitmap a(int i2) {
        Bitmap decodeResource = BitmapFactory.decodeResource(this.w.getResources(), i2);
        Bitmap createBitmap = Bitmap.createBitmap((int) (this.B * ((float) decodeResource.getWidth())), (int) (this.B * ((float) decodeResource.getHeight())), Bitmap.Config.ARGB_8888);
        new Canvas(createBitmap).drawBitmap(decodeResource, new Rect(0, 0, decodeResource.getWidth(), decodeResource.getHeight()), new Rect(0, 0, createBitmap.getWidth(), createBitmap.getHeight()), this.H);
        return createBitmap;
    }

    public final void a() {
        this.x = this.f107a - ((float) this.C);
        this.y = this.b - ((float) this.D);
        this.f = this.f107a - (((float) this.E) * FloatMath.cos((this.c + 90.0f) * 0.01745278f));
        this.g = this.b - (((float) this.E) * FloatMath.sin((this.c + 90.0f) * 0.01745278f));
        this.e.a();
        this.i = this.f - (((float) this.F) * FloatMath.cos(this.c * 0.01745278f));
        this.j = this.g - (((float) this.F) * FloatMath.sin(this.c * 0.01745278f));
        this.h.a();
        this.l = this.f + (((float) this.F) * FloatMath.cos(this.c * 0.01745278f));
        this.m = this.g + (((float) this.F) * FloatMath.sin(this.c * 0.01745278f));
        this.k.a();
        this.o = this.f107a - (((float) this.G) * FloatMath.cos(this.c * 0.01745278f));
        this.p = this.b - (((float) this.G) * FloatMath.sin(this.c * 0.01745278f));
        this.n.a();
        this.r = this.f107a + (((float) this.G) * FloatMath.cos(this.c * 0.01745278f));
        this.s = this.b + (((float) this.G) * FloatMath.sin(this.c * 0.01745278f));
        this.q.a();
    }

    public final void draw(Canvas canvas) {
        this.h.draw(canvas);
        this.k.draw(canvas);
        canvas.save();
        canvas.rotate(this.c, this.f107a, this.b);
        if (this.t) {
            canvas.drawBitmap(this.v, this.x, this.y, this.z);
        } else {
            canvas.drawBitmap(this.v, this.x, this.y, (Paint) null);
        }
        canvas.restore();
        this.n.draw(canvas);
        this.q.draw(canvas);
        this.e.draw(canvas);
    }

    public final int getOpacity() {
        return 0;
    }

    public final void setAlpha(int i2) {
    }

    public final void setColorFilter(ColorFilter colorFilter) {
    }
}
