package com.ElekaSoftware.OfficeRage.d;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.LightingColorFilter;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.util.FloatMath;
import com.ElekaSoftware.OfficeRage.C0000R;

public final class k extends Drawable {

    /* renamed from: a  reason: collision with root package name */
    public float f114a;
    public final a b;
    protected float c;
    protected float d;
    protected final d e;
    public boolean f;
    private final Bitmap g;
    private float h;
    private float i;
    private float j;
    private float k;
    private final Paint l;
    private final LightingColorFilter m;
    private final int n = ((int) (((float) this.g.getWidth()) * 0.073f));
    private final int o = ((int) (((float) this.g.getHeight()) * 0.5f));
    private int p;

    public k(d dVar, float f2, float f3, float f4) {
        this.g = dVar.a(C0000R.drawable.player_arm_upper_right);
        this.h = f2;
        this.i = f3;
        this.j = f2 - ((float) this.n);
        this.k = f3 - ((float) this.o);
        this.f114a = f4;
        this.e = dVar;
        this.f = false;
        this.l = new Paint();
        this.m = new LightingColorFilter(200, 112);
        this.l.setColorFilter(this.m);
        this.p = (int) (0.1875f * dVar.u);
        this.c = (((float) this.p) * FloatMath.cos(this.f114a * 0.01745278f)) + f2;
        this.d = (((float) this.p) * FloatMath.sin(this.f114a * 0.01745278f)) + f3;
        this.b = new a(this, this.c, this.d, dVar.d.l[7]);
    }

    public final void a() {
        this.h = this.e.l;
        this.i = this.e.m;
        this.j = this.h - ((float) this.n);
        this.k = this.i - ((float) this.o);
        this.c = this.h + (((float) this.p) * FloatMath.cos(this.f114a * 0.01745278f));
        this.d = this.i + (((float) this.p) * FloatMath.sin(this.f114a * 0.01745278f));
        this.b.a();
    }

    public final void draw(Canvas canvas) {
        this.b.draw(canvas);
        canvas.save();
        canvas.rotate(this.f114a, this.h, this.i);
        if (this.f) {
            canvas.drawBitmap(this.g, this.j, this.k, this.l);
        } else {
            canvas.drawBitmap(this.g, this.j, this.k, (Paint) null);
        }
        canvas.restore();
    }

    public final int getOpacity() {
        return 0;
    }

    public final void setAlpha(int i2) {
    }

    public final void setColorFilter(ColorFilter colorFilter) {
    }
}
