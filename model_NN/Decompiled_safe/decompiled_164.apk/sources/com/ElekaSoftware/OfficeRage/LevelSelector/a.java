package com.ElekaSoftware.OfficeRage.LevelSelector;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import com.ElekaSoftware.OfficeRage.C0000R;
import com.ElekaSoftware.OfficeRage.b.b;
import com.ElekaSoftware.OfficeRage.b.c;
import com.ElekaSoftware.OfficeRage.f;
import java.util.Vector;
import org.xml.sax.SAXException;

public final class a extends View implements View.OnTouchListener {
    private int[] A;
    private Rect B;
    private Paint C;
    private ColorMatrix D;
    private ColorMatrixColorFilter E;
    private String F;
    private Paint G;
    private Typeface H;
    private int I;
    private int J;
    private Paint K;
    private Paint L;
    private String M;
    private int N;
    private boolean O = false;

    /* renamed from: a  reason: collision with root package name */
    private final String f47a = "SelectorView";
    private Context b;
    private SelectorActivity c;
    private int d;
    private int e;
    private int f;
    private int g;
    private int h;
    private Rect i;
    private Rect j;
    private int k;
    private float l;
    private float m;
    private float n;
    private boolean o;
    private int p;
    private int q;
    private int r;
    private Bitmap s;
    private Vector t;
    private Vector u;
    private Bitmap v;
    private Bitmap w;
    private Bitmap x;
    private Bitmap y;
    private Rect[] z;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Paint.setShadowLayer(float, float, float, int):void}
     arg types: [int, int, int, int]
     candidates:
      ClspMth{android.graphics.Paint.setShadowLayer(float, float, float, long):void}
      ClspMth{android.graphics.Paint.setShadowLayer(float, float, float, int):void} */
    public a(Context context, SelectorActivity selectorActivity, int i2) {
        super(context);
        b bVar;
        this.b = context;
        this.c = selectorActivity;
        setOnTouchListener(this);
        Display defaultDisplay = selectorActivity.getWindowManager().getDefaultDisplay();
        this.d = defaultDisplay.getWidth();
        this.e = defaultDisplay.getHeight();
        this.h = i2;
        this.f = 8;
        this.g = i2;
        this.q = this.e / 6;
        this.p = (this.e - this.q) / 2;
        if (this.f == this.g) {
            Log.d("SelectorView", "total=completed");
            this.M = "";
            this.O = true;
        } else {
            new c();
            try {
                bVar = c.a(context, "levels/level" + Integer.toString(this.h + 1) + ".xml");
            } catch (SAXException e2) {
                e2.printStackTrace();
                bVar = null;
            }
            this.M = Integer.toString(bVar.b);
        }
        this.A = c();
        this.t = new Vector();
        this.u = new Vector();
        Bitmap decodeResource = BitmapFactory.decodeResource(getResources(), C0000R.drawable.levelselector_level1);
        this.r = decodeResource.getWidth() * (this.p / decodeResource.getHeight());
        this.y = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(getResources(), C0000R.drawable.button_back), this.q, this.q, true);
        this.v = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(getResources(), C0000R.drawable.levelselector_play), this.r, this.p, true);
        this.w = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(getResources(), C0000R.drawable.levelselector_unavailable), this.r, this.p, true);
        this.x = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(getResources(), C0000R.drawable.levelselector_lock), this.r, this.p, true);
        this.t.add(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(getResources(), C0000R.drawable.levelselector_level1), this.r, this.p, true));
        this.t.add(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(getResources(), C0000R.drawable.levelselector_level2), this.r, this.p, true));
        this.t.add(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(getResources(), C0000R.drawable.levelselector_level3), this.r, this.p, true));
        this.t.add(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(getResources(), C0000R.drawable.levelselector_level4), this.r, this.p, true));
        this.t.add(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(getResources(), C0000R.drawable.levelselector_level5), this.r, this.p, true));
        this.t.add(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(getResources(), C0000R.drawable.levelselector_level6), this.r, this.p, true));
        this.t.add(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(getResources(), C0000R.drawable.levelselector_level7), this.r, this.p, true));
        this.t.add(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(getResources(), C0000R.drawable.levelselector_level8), this.r, this.p, true));
        this.z = b();
        this.B = new Rect(0, this.p * 2, this.q, (this.p * 2) + this.q);
        this.C = new Paint();
        this.D = new ColorMatrix();
        this.D.setSaturation(0.0f);
        this.E = new ColorMatrixColorFilter(this.D);
        this.C.setColorFilter(this.E);
        for (int i3 = 0; i3 < this.f; i3++) {
            if (i3 > this.g) {
                this.u.add(a((Bitmap) this.t.elementAt(i3), true));
            } else {
                this.u.add(a((Bitmap) this.t.elementAt(i3), false));
            }
        }
        int width = ((Bitmap) this.t.get(0)).getWidth();
        this.k = (int) (((float) (width / 2)) + ((((float) this.f) / 2.0f) * ((float) width)));
        Log.d("SelectorView", "canvas width" + this.k);
        if (this.k < this.d) {
            this.k = this.d;
        }
        this.s = Bitmap.createBitmap(this.k, this.e, Bitmap.Config.RGB_565);
        this.j = new Rect(0, 0, this.d, this.e);
        this.i = new Rect(0, 0, this.d, this.e);
        this.G = new Paint();
        this.G.setColor(Color.argb(255, 255, 161, 24));
        this.G.setTextSize(0.13f * ((float) this.e));
        this.H = Typeface.createFromAsset(this.c.getResources().getAssets(), "coolvetica.ttf");
        this.G.setTypeface(this.H);
        this.G.setTextAlign(Paint.Align.CENTER);
        this.G.setAntiAlias(true);
        this.I = this.d / 2;
        if (this.O) {
            this.F = "You did it!";
        } else {
            this.F = "High Score: " + Integer.toString(this.A[this.h]);
        }
        Paint.FontMetrics fontMetrics = this.G.getFontMetrics();
        this.J = (this.e - ((this.q - ((int) (fontMetrics.descent + fontMetrics.ascent))) / 2)) + (this.q / 2);
        this.K = new Paint();
        this.K.setColor(Color.argb(255, 255, 161, 24));
        this.K.setTextSize(0.08f * ((float) this.e));
        this.K.setTypeface(this.H);
        this.K.setTextAlign(Paint.Align.CENTER);
        this.K.setAntiAlias(true);
        this.K.setShadowLayer(4.0f, 2.0f, 2.0f, -16777216);
        Paint.FontMetrics fontMetrics2 = this.K.getFontMetrics();
        this.N = (int) (fontMetrics2.top + fontMetrics2.bottom);
        this.L = new Paint();
        this.L.setColor(Color.argb(255, 255, 161, 24));
        this.L.setTextSize(0.12f * ((float) this.e));
        this.L.setTypeface(this.H);
        this.L.setTextAlign(Paint.Align.CENTER);
        this.L.setAntiAlias(true);
        this.L.setShadowLayer(4.0f, 2.0f, 2.0f, -16777216);
        a();
    }

    private Bitmap a(Bitmap bitmap, boolean z2) {
        Bitmap createBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(createBitmap);
        canvas.drawBitmap(bitmap, 0.0f, 0.0f, this.C);
        if (z2) {
            canvas.drawBitmap(this.w, 0.0f, 0.0f, (Paint) null);
            canvas.drawBitmap(this.x, 0.0f, 0.0f, (Paint) null);
        }
        return createBitmap;
    }

    private void a() {
        int i2;
        int i3;
        Canvas canvas = new Canvas(this.s);
        canvas.drawColor(-16777216);
        for (int i4 = 1; i4 < this.f + 1; i4++) {
            if (i4 % 2 != 0) {
                i2 = 0;
                i3 = (int) (Math.floor(((double) i4) / 2.0d) * ((double) this.r));
            } else {
                int floor = (int) ((Math.floor(((double) i4) / 2.0d) - 0.5d) * ((double) this.r));
                i2 = this.p;
                i3 = floor;
            }
            if (i4 - 1 == this.h) {
                canvas.drawBitmap((Bitmap) this.t.elementAt(i4 - 1), (float) i3, (float) i2, (Paint) null);
                canvas.drawBitmap(this.v, (float) i3, (float) i2, (Paint) null);
            } else {
                canvas.drawBitmap((Bitmap) this.u.elementAt(i4 - 1), (float) i3, (float) i2, (Paint) null);
            }
            if (i4 == this.g + 2) {
                canvas.drawText(this.M, (float) ((this.r / 2) + i3), (float) (((this.p / 2) + i2) - this.N), this.L);
                canvas.drawText("Score to Unlock", (float) ((this.r / 2) + i3), (float) ((this.p / 2) + i2 + this.N), this.K);
            }
            if (this.g == this.f - 1 && i4 == this.f) {
                Log.d("SelectorView", "draw final scoreupneeded message");
                canvas.drawText(this.M, (float) ((this.r / 2) + i3), (float) (((this.p / 2) + i2) - this.N), this.L);
                canvas.drawText("Score to Finish", (float) (i3 + (this.r / 2)), (float) (i2 + (this.p / 2) + this.N), this.K);
            }
        }
    }

    private Rect[] b() {
        int floor;
        int i2;
        Rect[] rectArr = new Rect[this.f];
        for (int i3 = 1; i3 < this.f + 1; i3++) {
            if (i3 % 2 != 0) {
                floor = (int) (Math.floor(((double) i3) / 2.0d) * ((double) this.r));
                i2 = 0;
            } else {
                floor = (int) ((Math.floor(((double) i3) / 2.0d) - 0.5d) * ((double) this.r));
                i2 = this.p;
            }
            int i4 = i2;
            int i5 = floor;
            int i6 = i4;
            rectArr[i3 - 1] = new Rect(i5, i6, this.r + i5, this.p + i6);
        }
        return rectArr;
    }

    private int[] c() {
        int[] iArr = new int[this.f];
        f fVar = new f(this.b);
        try {
            fVar.b();
            for (int i2 = 0; i2 < this.f; i2++) {
                iArr[i2] = fVar.a(i2 + 1);
            }
            fVar.close();
        } catch (Exception e2) {
            Log.d("SelectorView", "EXCEPTION!");
        }
        return iArr;
    }

    public final void draw(Canvas canvas) {
        canvas.drawBitmap(this.s, this.i, this.j, (Paint) null);
        canvas.drawText(this.F, (float) this.I, (float) this.J, this.G);
        canvas.drawBitmap(this.y, 0.0f, (float) (this.p * 2), (Paint) null);
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        if (motionEvent.getAction() == 0) {
            this.l = motionEvent.getX();
            this.n = this.l;
            this.o = false;
        } else if (motionEvent.getAction() == 2) {
            if (!this.o) {
                this.l = motionEvent.getX();
                if (Math.abs(this.n - this.l) > 20.0f) {
                    this.o = true;
                }
            } else {
                this.m = this.l - motionEvent.getX();
                this.i.offset((int) this.m, 0);
                if (this.i.left < 0) {
                    this.i.offsetTo(0, 0);
                }
                if (this.i.left > this.k - this.d) {
                    this.i.offsetTo(this.k - this.d, 0);
                }
                this.l = motionEvent.getX();
                invalidate();
            }
        } else if (motionEvent.getAction() == 1 && !this.o) {
            if (this.B.contains((int) motionEvent.getX(), (int) motionEvent.getY())) {
                this.c.finish();
            }
            int i2 = 0;
            while (true) {
                if (i2 >= this.f) {
                    break;
                } else if (this.z[i2].contains(((int) motionEvent.getX()) + this.i.left, (int) motionEvent.getY())) {
                    Log.d("SelectorView", "Level: " + (i2 + 1) + " selected.");
                    if (this.h == i2) {
                        this.c.a(this.h + 1);
                    } else {
                        if (i2 > this.g) {
                            this.h = this.g;
                            i2 = this.g;
                        } else {
                            this.h = i2;
                        }
                        this.F = "High Score: " + Integer.toString(this.A[i2]);
                        a();
                        invalidate();
                    }
                } else {
                    i2++;
                }
            }
        }
        return true;
    }
}
