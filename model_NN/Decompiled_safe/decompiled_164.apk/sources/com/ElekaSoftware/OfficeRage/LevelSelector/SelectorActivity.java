package com.ElekaSoftware.OfficeRage.LevelSelector;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import com.ElekaSoftware.OfficeRage.GameActivity;
import com.ElekaSoftware.OfficeRage.GameAnimations.GameAnimationActivity;

public class SelectorActivity extends Activity {

    /* renamed from: a  reason: collision with root package name */
    private a f46a;
    private boolean b;

    /* access modifiers changed from: protected */
    public final void a(int i) {
        if (!this.b) {
            Intent intent = new Intent(this, GameActivity.class);
            intent.putExtra("level", i);
            startActivity(intent);
        } else if (i == 1 || i == 2 || i == 4 || i == 6) {
            Intent intent2 = new Intent(this, GameAnimationActivity.class);
            intent2.putExtra("animation", i);
            startActivity(intent2);
        } else {
            Intent intent3 = new Intent(this, GameActivity.class);
            intent3.putExtra("level", i);
            startActivity(intent3);
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
    }

    public void onDestroy() {
        super.onDestroy();
        this.f46a.setOnTouchListener(null);
    }

    public void onResume() {
        super.onResume();
        SharedPreferences sharedPreferences = getSharedPreferences("OfficeRagePreferences", 0);
        this.b = sharedPreferences.getBoolean("Cutscene", true);
        Log.d("SelectorView", "SelectorActivity: levelplayed = " + sharedPreferences.getInt("levelplayed", 0));
        this.f46a = new a(this, this, sharedPreferences.getInt("levelplayed", 0));
        setContentView(this.f46a);
        this.f46a.requestFocus();
    }
}
