package a.a.a;

import a.a.a.a.e;
import a.a.a.b.f;
import a.a.a.b.m;
import a.a.a.c.a;
import a.a.a.d.g;
import a.a.a.d.h;
import java.io.InputStream;
import java.io.Writer;
import java.lang.ref.SoftReference;

public final class n {

    /* renamed from: a  reason: collision with root package name */
    private static int f34a = e.a();
    private static int b = j.a();
    private static ThreadLocal c = new ThreadLocal();
    private g d;
    private h e;
    private d f;
    private int g;
    private int h;

    private n() {
        this.d = g.a();
        this.e = h.a();
        this.g = f34a;
        this.h = b;
        this.f = null;
    }

    public n(byte b2) {
        this();
    }

    private static a a(Object obj, boolean z) {
        e eVar;
        SoftReference softReference = (SoftReference) c.get();
        e eVar2 = softReference == null ? null : (e) softReference.get();
        if (eVar2 == null) {
            e eVar3 = new e();
            if (softReference == null) {
                c.set(new SoftReference(eVar3));
            }
            eVar = eVar3;
        } else {
            eVar = eVar2;
        }
        return new a(eVar, obj, z);
    }

    public final f a(Writer writer) {
        return new m(a(writer, false), this.h, this.f, writer);
    }

    public final h a(InputStream inputStream) {
        return new f(a(inputStream, false), inputStream).a(this.g, this.f, this.e, this.d);
    }

    public final h a(byte[] bArr) {
        return new f(a(bArr, true), bArr, bArr.length).a(this.g, this.f, this.e, this.d);
    }
}
