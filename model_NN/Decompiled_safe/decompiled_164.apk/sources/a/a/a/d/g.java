package a.a.a.d;

import java.util.Arrays;

public final class g {

    /* renamed from: a  reason: collision with root package name */
    private static g f24a = new g();
    private g b;
    private boolean c;
    private boolean d;
    private String[] e;
    private i[] f;
    private int g;
    private int h;
    private int i;
    private boolean j;

    private g() {
        this.d = true;
        this.c = true;
        this.j = true;
        c();
    }

    private g(g gVar, boolean z, boolean z2, String[] strArr, i[] iVarArr, int i2) {
        this.b = gVar;
        this.d = z;
        this.c = z2;
        this.e = strArr;
        this.f = iVarArr;
        this.g = i2;
        int length = strArr.length;
        this.h = length - (length >> 2);
        this.i = length - 1;
        this.j = false;
    }

    private static int a(String str) {
        char charAt = str.charAt(0);
        int length = str.length();
        int i2 = charAt;
        for (int i3 = 1; i3 < length; i3++) {
            i2 = (i2 * 31) + str.charAt(i3);
        }
        return i2;
    }

    private static int a(char[] cArr, int i2) {
        int i3 = cArr[0];
        for (int i4 = 1; i4 < i2; i4++) {
            i3 = (i3 * 31) + cArr[i4];
        }
        return i3;
    }

    public static g a() {
        g gVar = f24a;
        return new g(null, true, true, gVar.e, gVar.f, gVar.g);
    }

    private synchronized void a(g gVar) {
        if (gVar.g > 12000) {
            c();
        } else if (gVar.g > this.g) {
            this.e = gVar.e;
            this.f = gVar.f;
            this.g = gVar.g;
            this.h = gVar.h;
            this.i = gVar.i;
        }
        this.j = false;
    }

    private void c() {
        this.e = new String[64];
        this.f = new i[32];
        this.i = 63;
        this.g = 0;
        this.h = 48;
    }

    private void d() {
        int length = this.e.length;
        int i2 = length + length;
        if (i2 > 65536) {
            this.g = 0;
            Arrays.fill(this.e, (Object) null);
            Arrays.fill(this.f, (Object) null);
            this.j = true;
            return;
        }
        String[] strArr = this.e;
        i[] iVarArr = this.f;
        this.e = new String[i2];
        this.f = new i[(i2 >> 1)];
        this.i = i2 - 1;
        this.h += this.h;
        int i3 = 0;
        for (int i4 = 0; i4 < length; i4++) {
            String str = strArr[i4];
            if (str != null) {
                i3++;
                int a2 = a(str) & this.i;
                if (this.e[a2] == null) {
                    this.e[a2] = str;
                } else {
                    int i5 = a2 >> 1;
                    this.f[i5] = new i(str, this.f[i5]);
                }
            }
        }
        int i6 = length >> 1;
        int i7 = 0;
        int i8 = i3;
        while (i7 < i6) {
            int i9 = i8;
            for (i iVar = iVarArr[i7]; iVar != null; iVar = iVar.b()) {
                i9++;
                String a3 = iVar.a();
                int a4 = a(a3) & this.i;
                if (this.e[a4] == null) {
                    this.e[a4] = a3;
                } else {
                    int i10 = a4 >> 1;
                    this.f[i10] = new i(a3, this.f[i10]);
                }
            }
            i7++;
            i8 = i9;
        }
        if (i8 != this.g) {
            throw new Error("Internal error on SymbolTable.rehash(): had " + this.g + " entries; now have " + i8 + ".");
        }
    }

    public final synchronized g a(boolean z, boolean z2) {
        return new g(this, z, z2, this.e, this.f, this.g);
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x002a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.String a(char[] r7, int r8, int r9, int r10) {
        /*
            r6 = this;
            r5 = 0
            if (r9 > 0) goto L_0x0006
            java.lang.String r0 = ""
        L_0x0005:
            return r0
        L_0x0006:
            boolean r0 = r6.d
            if (r0 != 0) goto L_0x0010
            java.lang.String r0 = new java.lang.String
            r0.<init>(r7, r8, r9)
            goto L_0x0005
        L_0x0010:
            int r0 = r6.i
            r0 = r0 & r10
            java.lang.String[] r1 = r6.e
            r1 = r1[r0]
            if (r1 == 0) goto L_0x0042
            int r2 = r1.length()
            if (r2 != r9) goto L_0x0032
            r2 = r5
        L_0x0020:
            char r3 = r1.charAt(r2)
            int r4 = r8 + r2
            char r4 = r7[r4]
            if (r3 != r4) goto L_0x002e
            int r2 = r2 + 1
            if (r2 < r9) goto L_0x0020
        L_0x002e:
            if (r2 != r9) goto L_0x0032
            r0 = r1
            goto L_0x0005
        L_0x0032:
            a.a.a.d.i[] r1 = r6.f
            int r2 = r0 >> 1
            r1 = r1[r2]
            if (r1 == 0) goto L_0x0042
            java.lang.String r1 = r1.a(r7, r8, r9)
            if (r1 == 0) goto L_0x0042
            r0 = r1
            goto L_0x0005
        L_0x0042:
            boolean r1 = r6.j
            if (r1 != 0) goto L_0x0082
            java.lang.String[] r1 = r6.e
            int r2 = r1.length
            java.lang.String[] r3 = new java.lang.String[r2]
            r6.e = r3
            java.lang.String[] r3 = r6.e
            java.lang.System.arraycopy(r1, r5, r3, r5, r2)
            a.a.a.d.i[] r1 = r6.f
            int r2 = r1.length
            a.a.a.d.i[] r3 = new a.a.a.d.i[r2]
            r6.f = r3
            a.a.a.d.i[] r3 = r6.f
            java.lang.System.arraycopy(r1, r5, r3, r5, r2)
            r1 = 1
            r6.j = r1
        L_0x0061:
            int r1 = r6.g
            int r1 = r1 + 1
            r6.g = r1
            java.lang.String r1 = new java.lang.String
            r1.<init>(r7, r8, r9)
            boolean r2 = r6.c
            if (r2 == 0) goto L_0x0076
            a.a.a.a.c r2 = a.a.a.a.c.f2a
            java.lang.String r1 = r2.a(r1)
        L_0x0076:
            java.lang.String[] r2 = r6.e
            r2 = r2[r0]
            if (r2 != 0) goto L_0x0093
            java.lang.String[] r2 = r6.e
            r2[r0] = r1
        L_0x0080:
            r0 = r1
            goto L_0x0005
        L_0x0082:
            int r1 = r6.g
            int r2 = r6.h
            if (r1 < r2) goto L_0x0061
            r6.d()
            int r0 = a(r7, r9)
            int r1 = r6.i
            r0 = r0 & r1
            goto L_0x0061
        L_0x0093:
            int r0 = r0 >> 1
            a.a.a.d.i[] r2 = r6.f
            a.a.a.d.i r3 = new a.a.a.d.i
            a.a.a.d.i[] r4 = r6.f
            r4 = r4[r0]
            r3.<init>(r1, r4)
            r2[r0] = r3
            goto L_0x0080
        */
        throw new UnsupportedOperationException("Method not decompiled: a.a.a.d.g.a(char[], int, int, int):java.lang.String");
    }

    public final void b() {
        if (this.j && this.b != null) {
            this.b.a(this);
            this.j = false;
        }
    }
}
