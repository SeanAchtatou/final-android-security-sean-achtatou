package a.a.a.d;

public final class c extends f {

    /* renamed from: a  reason: collision with root package name */
    private int f20a;
    private int b;

    c(String str, int i, int i2, int i3) {
        super(str, i);
        this.f20a = i2;
        this.b = i3;
    }

    public final boolean a(int i) {
        return false;
    }

    public final boolean a(int i, int i2) {
        return i == this.f20a && i2 == this.b;
    }

    public final boolean a(int[] iArr, int i) {
        return i == 2 && iArr[0] == this.f20a && iArr[1] == this.b;
    }
}
