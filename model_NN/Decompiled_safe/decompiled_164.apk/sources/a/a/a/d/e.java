package a.a.a.d;

final class e {

    /* renamed from: a  reason: collision with root package name */
    final f f22a;
    final e b;

    e(f fVar, e eVar) {
        this.f22a = fVar;
        this.b = eVar;
    }

    public final f a(int i, int i2, int i3) {
        if (this.f22a.hashCode() == i && this.f22a.a(i2, i3)) {
            return this.f22a;
        }
        for (e eVar = this.b; eVar != null; eVar = eVar.b) {
            f fVar = eVar.f22a;
            if (fVar.hashCode() == i && fVar.a(i2, i3)) {
                return fVar;
            }
        }
        return null;
    }
}
