package a.a.a.d;

public final class b extends f {

    /* renamed from: a  reason: collision with root package name */
    private static b f19a = new b("", 0, 0);
    private int b;

    b(String str, int i, int i2) {
        super(str, i);
        this.b = i2;
    }

    static final b a() {
        return f19a;
    }

    public final boolean a(int i) {
        return i == this.b;
    }

    public final boolean a(int i, int i2) {
        return i == this.b && i2 == 0;
    }

    public final boolean a(int[] iArr, int i) {
        return i == 1 && iArr[0] == this.b;
    }
}
