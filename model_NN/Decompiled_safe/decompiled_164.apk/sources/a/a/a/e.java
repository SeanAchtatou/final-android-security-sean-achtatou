package a.a.a;

public enum e {
    AUTO_CLOSE_SOURCE(true),
    ALLOW_COMMENTS(false),
    ALLOW_UNQUOTED_FIELD_NAMES(false),
    ALLOW_SINGLE_QUOTES(false),
    ALLOW_UNQUOTED_CONTROL_CHARS(false),
    INTERN_FIELD_NAMES(true),
    CANONICALIZE_FIELD_NAMES(true);
    
    private boolean h;

    private e(boolean z) {
        this.h = z;
    }

    public static int a() {
        int i2 = 0;
        for (e eVar : values()) {
            if (eVar.h) {
                i2 |= 1 << eVar.ordinal();
            }
        }
        return i2;
    }

    public final boolean a(int i2) {
        return ((1 << ordinal()) & i2) != 0;
    }

    public final int b() {
        return 1 << ordinal();
    }
}
