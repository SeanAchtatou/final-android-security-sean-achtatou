package a.a.a;

public enum b {
    UTF8("UTF-8", false),
    UTF16_BE("UTF-16BE", true),
    UTF16_LE("UTF-16LE", false),
    UTF32_BE("UTF-32BE", true),
    UTF32_LE("UTF-32LE", false);
    
    private String f;
    private boolean g;

    private b(String str, boolean z) {
        this.f = str;
        this.g = z;
    }

    public final String a() {
        return this.f;
    }

    public final boolean b() {
        return this.g;
    }
}
