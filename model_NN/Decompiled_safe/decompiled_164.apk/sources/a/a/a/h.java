package a.a.a;

import java.io.Closeable;

public abstract class h implements Closeable {
    protected int q;
    protected m r;

    protected h() {
    }

    public final boolean a(e eVar) {
        return (this.q & eVar.b()) != 0;
    }

    public abstract int d();

    /* access modifiers changed from: protected */
    public final c d(String str) {
        return new c(str, l());
    }

    public abstract long e();

    public abstract float f();

    public abstract double g();

    public abstract m i();

    public abstract h j();

    public abstract g k();

    public abstract g l();

    public abstract String m();

    public final m q() {
        return this.r;
    }
}
