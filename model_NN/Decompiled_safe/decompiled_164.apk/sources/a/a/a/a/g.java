package a.a.a.a;

public enum g {
    TOKEN_BUFFER(2000),
    CONCAT_BUFFER(2000),
    TEXT_BUFFER(200),
    NAME_COPY_BUFFER(200);
    
    private final int e;

    private g(int i) {
        this.e = i;
    }
}
