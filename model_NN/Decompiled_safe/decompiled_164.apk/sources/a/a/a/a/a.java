package a.a.a.a;

import java.io.OutputStream;
import java.util.LinkedList;

public final class a extends OutputStream {

    /* renamed from: a  reason: collision with root package name */
    private static final byte[] f0a = new byte[0];
    private final e b = null;
    private final LinkedList c = new LinkedList();
    private int d;
    private byte[] e = new byte[500];
    private int f;

    private a() {
    }

    private void a() {
        this.d += this.e.length;
        int max = Math.max(this.d >> 1, 1000);
        if (max > 262144) {
            max = 262144;
        }
        this.c.add(this.e);
        this.e = new byte[max];
        this.f = 0;
    }

    public final void close() {
    }

    public final void flush() {
    }

    public final void write(int i) {
        byte b2 = (byte) i;
        if (this.f >= this.e.length) {
            a();
        }
        byte[] bArr = this.e;
        int i2 = this.f;
        this.f = i2 + 1;
        bArr[i2] = b2;
    }

    public final void write(byte[] bArr) {
        write(bArr, 0, bArr.length);
    }

    public final void write(byte[] bArr, int i, int i2) {
        int i3 = i2;
        int i4 = i;
        while (true) {
            int min = Math.min(this.e.length - this.f, i3);
            if (min > 0) {
                System.arraycopy(bArr, i4, this.e, this.f, min);
                i4 += min;
                this.f += min;
                i3 -= min;
            }
            if (i3 > 0) {
                a();
            } else {
                return;
            }
        }
    }
}
