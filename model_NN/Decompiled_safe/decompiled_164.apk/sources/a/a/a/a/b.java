package a.a.a.a;

import java.util.Arrays;

public final class b {

    /* renamed from: a  reason: collision with root package name */
    private static char[] f1a = "0123456789ABCDEF".toCharArray();
    private static int[] b;
    private static int[] c;
    private static int[] d;
    private static int[] e;
    private static int[] f = new int[256];
    private static int[] g;
    private static int[] h;

    static {
        int[] iArr = new int[256];
        for (int i = 0; i < 32; i++) {
            iArr[i] = -1;
        }
        iArr[34] = 1;
        iArr[92] = 1;
        b = iArr;
        int[] iArr2 = new int[iArr.length];
        System.arraycopy(b, 0, iArr2, 0, b.length);
        for (int i2 = 128; i2 < 256; i2++) {
            iArr2[i2] = (i2 & 224) == 192 ? 2 : (i2 & 240) == 224 ? 3 : (i2 & 248) == 240 ? 4 : -1;
        }
        c = iArr2;
        int[] iArr3 = new int[256];
        Arrays.fill(iArr3, -1);
        for (int i3 = 33; i3 < 256; i3++) {
            if (Character.isJavaIdentifierPart((char) i3)) {
                iArr3[i3] = 0;
            }
        }
        d = iArr3;
        int[] iArr4 = new int[256];
        System.arraycopy(d, 0, iArr4, 0, d.length);
        Arrays.fill(iArr4, 128, 128, 0);
        e = iArr4;
        System.arraycopy(c, 128, f, 128, 128);
        Arrays.fill(f, 0, 32, -1);
        f[9] = 0;
        f[10] = 10;
        f[13] = 13;
        f[42] = 42;
        int[] iArr5 = new int[256];
        for (int i4 = 0; i4 < 32; i4++) {
            iArr5[i4] = -(i4 + 1);
        }
        iArr5[34] = 34;
        iArr5[92] = 92;
        iArr5[8] = 98;
        iArr5[9] = 116;
        iArr5[12] = 102;
        iArr5[10] = 110;
        iArr5[13] = 114;
        g = iArr5;
        int[] iArr6 = new int[128];
        h = iArr6;
        Arrays.fill(iArr6, -1);
        for (int i5 = 0; i5 < 10; i5++) {
            h[i5 + 48] = i5;
        }
        for (int i6 = 0; i6 < 6; i6++) {
            h[i6 + 97] = i6 + 10;
            h[i6 + 65] = i6 + 10;
        }
    }

    public static int a(int i) {
        if (i > 127) {
            return -1;
        }
        return h[i];
    }

    public static void a(StringBuilder sb, String str) {
        int[] iArr = g;
        int length = iArr.length;
        int length2 = str.length();
        for (int i = 0; i < length2; i++) {
            char charAt = str.charAt(i);
            if (charAt >= length || iArr[charAt] == 0) {
                sb.append(charAt);
            } else {
                sb.append('\\');
                int i2 = iArr[charAt];
                if (i2 < 0) {
                    sb.append('u');
                    sb.append('0');
                    sb.append('0');
                    int i3 = -(i2 + 1);
                    sb.append(f1a[i3 >> 4]);
                    sb.append(f1a[i3 & 15]);
                } else {
                    sb.append((char) i2);
                }
            }
        }
    }

    public static final int[] a() {
        return b;
    }

    public static final int[] b() {
        return c;
    }

    public static final int[] c() {
        return d;
    }

    public static final int[] d() {
        return e;
    }

    public static final int[] e() {
        return f;
    }

    public static final int[] f() {
        return g;
    }
}
