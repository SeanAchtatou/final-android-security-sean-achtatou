package a.a.a.a;

public final class e {

    /* renamed from: a  reason: collision with root package name */
    private byte[][] f4a = new byte[d.values().length][];
    private char[][] b = new char[g.values().length][];

    public final void a(d dVar, byte[] bArr) {
        this.f4a[dVar.ordinal()] = bArr;
    }

    public final void a(g gVar, char[] cArr) {
        this.b[gVar.ordinal()] = cArr;
    }

    public final byte[] a(d dVar) {
        int ordinal = dVar.ordinal();
        byte[] bArr = this.f4a[ordinal];
        if (bArr == null) {
            return new byte[d.a(dVar)];
        }
        this.f4a[ordinal] = null;
        return bArr;
    }

    public final char[] a(g gVar) {
        return a(gVar, 0);
    }

    public final char[] a(g gVar, int i) {
        int a2 = g.a(gVar) > i ? g.a(gVar) : i;
        int ordinal = gVar.ordinal();
        char[] cArr = this.b[ordinal];
        if (cArr == null || cArr.length < a2) {
            return new char[a2];
        }
        this.b[ordinal] = null;
        return cArr;
    }
}
