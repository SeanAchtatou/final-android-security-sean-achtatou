package a.a.a.c;

import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;

abstract class b extends Reader {

    /* renamed from: a  reason: collision with root package name */
    protected InputStream f14a;
    protected byte[] b;
    protected int c;
    protected int d;
    private a e;
    private char[] f = null;

    protected b(a aVar, InputStream inputStream, byte[] bArr, int i, int i2) {
        this.e = aVar;
        this.f14a = inputStream;
        this.b = bArr;
        this.c = i;
        this.d = i2;
    }

    protected static void b() {
        throw new IOException("Strange I/O stream, returned 0 bytes on read");
    }

    public final void a() {
        byte[] bArr = this.b;
        if (bArr != null) {
            this.b = null;
            this.e.a(bArr);
        }
    }

    public void close() {
        InputStream inputStream = this.f14a;
        if (inputStream != null) {
            this.f14a = null;
            a();
            inputStream.close();
        }
    }

    public int read() {
        if (this.f == null) {
            this.f = new char[1];
        }
        if (read(this.f, 0, 1) <= 0) {
            return -1;
        }
        return this.f[0];
    }
}
