package a.a.a.c;

public final class e {

    /* renamed from: a  reason: collision with root package name */
    private static int f17a = 1000000;
    private static int b = 1000000000;
    private static long c = 10000000000L;
    private static long d = 1000;
    private static long e = -2147483648L;
    private static long f = 2147483647L;
    private static String g = String.valueOf(Long.MIN_VALUE);
    private static char[] h = new char[4000];
    private static char[] i = new char[4000];
    private static String[] j = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10"};
    private static String[] k = {"-1", "-2", "-3", "-4", "-5", "-6", "-7", "-8", "-9", "-10"};

    static {
        int i2 = 0;
        int i3 = 0;
        while (i2 < 10) {
            char c2 = (char) (i2 + 48);
            char c3 = i2 == 0 ? 0 : c2;
            int i4 = i3;
            int i5 = 0;
            while (i5 < 10) {
                char c4 = (char) (i5 + 48);
                char c5 = (i2 == 0 && i5 == 0) ? 0 : c4;
                int i6 = i4;
                for (int i7 = 0; i7 < 10; i7++) {
                    char c6 = (char) (i7 + 48);
                    h[i6] = c3;
                    h[i6 + 1] = c5;
                    h[i6 + 2] = c6;
                    i[i6] = c2;
                    i[i6 + 1] = c4;
                    i[i6 + 2] = c6;
                    i6 += 4;
                }
                i5++;
                i4 = i6;
            }
            i2++;
            i3 = i4;
        }
    }

    public static int a(int i2, char[] cArr, int i3) {
        int i4;
        int i5;
        if (i2 >= 0) {
            i4 = i3;
            i5 = i2;
        } else if (i2 == Integer.MIN_VALUE) {
            return a((long) i2, cArr, i3);
        } else {
            i4 = i3 + 1;
            cArr[i3] = '-';
            i5 = -i2;
        }
        if (i5 >= f17a) {
            boolean z = i5 >= b;
            if (z) {
                i5 -= b;
                if (i5 >= b) {
                    i5 -= b;
                    cArr[i4] = '2';
                    i4++;
                } else {
                    cArr[i4] = '1';
                    i4++;
                }
            }
            int i6 = i5 / 1000;
            int i7 = i6 / 1000;
            return c(i5 - (i6 * 1000), cArr, c(i6 - (i7 * 1000), cArr, z ? c(i7, cArr, i4) : b(i7, cArr, i4)));
        } else if (i5 >= 1000) {
            int i8 = i5 / 1000;
            return c(i5 - (i8 * 1000), cArr, b(i8, cArr, i4));
        } else if (i5 >= 10) {
            return b(i5, cArr, i4);
        } else {
            cArr[i4] = (char) (i5 + 48);
            return i4 + 1;
        }
    }

    public static int a(long j2, char[] cArr, int i2) {
        int i3;
        long j3;
        if (j2 < 0) {
            if (j2 > e) {
                return a((int) j2, cArr, i2);
            }
            if (j2 == Long.MIN_VALUE) {
                int length = g.length();
                g.getChars(0, length, cArr, i2);
                return length + i2;
            }
            i3 = i2 + 1;
            cArr[i2] = '-';
            j3 = -j2;
        } else if (j2 <= f) {
            return a((int) j2, cArr, i2);
        } else {
            i3 = i2;
            j3 = j2;
        }
        int i4 = 10;
        for (long j4 = c; j3 >= j4 && i4 != 19; j4 = (j4 << 1) + (j4 << 3)) {
            i4++;
        }
        int i5 = i3 + i4;
        long j5 = j3;
        int i6 = i5;
        while (j5 > f) {
            i6 -= 3;
            long j6 = j5 / d;
            c((int) (j5 - (d * j6)), cArr, i6);
            j5 = j6;
        }
        int i7 = i6;
        int i8 = (int) j5;
        while (i8 >= 1000) {
            i7 -= 3;
            int i9 = i8 / 1000;
            c(i8 - (i9 * 1000), cArr, i7);
            i8 = i9;
        }
        b(i8, cArr, i3);
        return i5;
    }

    private static int b(int i2, char[] cArr, int i3) {
        int i4;
        int i5 = i2 << 2;
        int i6 = i5 + 1;
        char c2 = h[i5];
        if (c2 != 0) {
            cArr[i3] = c2;
            i4 = i3 + 1;
        } else {
            i4 = i3;
        }
        int i7 = i6 + 1;
        char c3 = h[i6];
        if (c3 != 0) {
            cArr[i4] = c3;
            i4++;
        }
        int i8 = i4 + 1;
        cArr[i4] = h[i7];
        return i8;
    }

    private static int c(int i2, char[] cArr, int i3) {
        int i4 = i2 << 2;
        int i5 = i3 + 1;
        int i6 = i4 + 1;
        cArr[i3] = i[i4];
        int i7 = i5 + 1;
        cArr[i5] = i[i6];
        int i8 = i7 + 1;
        cArr[i7] = i[i6 + 1];
        return i8;
    }
}
