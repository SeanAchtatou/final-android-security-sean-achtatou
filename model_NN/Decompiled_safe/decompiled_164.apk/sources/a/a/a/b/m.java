package a.a.a.b;

import a.a.a.c.a;
import a.a.a.c.e;
import a.a.a.d;
import a.a.a.j;
import java.io.Writer;

public final class m extends k {
    private static char[] d = "0123456789ABCDEF".toCharArray();
    private a e;
    private Writer f;
    private char[] g;
    private int h = 0;
    private int i = 0;
    private int j;
    private char[] k;

    public m(a aVar, int i2, d dVar, Writer writer) {
        super(i2, dVar);
        this.e = aVar;
        this.f = writer;
        this.g = aVar.g();
        this.j = this.g.length;
    }

    private static void a(int i2, char[] cArr, int i3) {
        if (i2 < 0) {
            int i4 = -(i2 + 1);
            cArr[i3] = '\\';
            int i5 = i3 + 1;
            cArr[i5] = 'u';
            int i6 = i5 + 1;
            cArr[i6] = '0';
            int i7 = i6 + 1;
            cArr[i7] = '0';
            int i8 = i7 + 1;
            cArr[i8] = d[i4 >> 4];
            cArr[i8 + 1] = d[i4 & 15];
            return;
        }
        cArr[i3] = '\\';
        cArr[i3 + 1] = (char) i2;
    }

    private void b(int i2) {
        char[] cArr = this.k;
        if (cArr == null) {
            cArr = new char[6];
            cArr[0] = '\\';
            cArr[2] = '0';
            cArr[3] = '0';
        }
        if (i2 < 0) {
            int i3 = -(i2 + 1);
            cArr[1] = 'u';
            cArr[4] = d[i3 >> 4];
            cArr[5] = d[i3 & 15];
            this.f.write(cArr, 0, 6);
            return;
        }
        cArr[1] = (char) i2;
        this.f.write(cArr, 0, 2);
    }

    private void e(String str) {
        int length = str.length();
        int i2 = this.j - this.i;
        if (i2 == 0) {
            j();
            i2 = this.j - this.i;
        }
        if (i2 >= length) {
            str.getChars(0, length, this.g, this.i);
            this.i = length + this.i;
            return;
        }
        int i3 = this.j - this.i;
        str.getChars(0, i3, this.g, this.i);
        this.i += i3;
        j();
        int length2 = str.length() - i3;
        while (length2 > this.j) {
            int i4 = this.j;
            str.getChars(i3, i3 + i4, this.g, 0);
            this.h = 0;
            this.i = i4;
            j();
            i3 += i4;
            length2 -= i4;
        }
        str.getChars(i3, i3 + length2, this.g, 0);
        this.h = 0;
        this.i = length2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:41:0x009a, code lost:
        r3 = r13.i - r13.h;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x009f, code lost:
        if (r3 <= 0) goto L_0x00aa;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00a1, code lost:
        r13.f.write(r13.g, r13.h, r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x00aa, code lost:
        r3 = r1[r13.g[r13.i]];
        r13.i++;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00b8, code lost:
        if (r3 >= 0) goto L_0x00c7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00ba, code lost:
        r4 = 6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x00bd, code lost:
        if (r4 <= r13.i) goto L_0x00c9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x00bf, code lost:
        r13.h = r13.i;
        b(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x00c7, code lost:
        r4 = 2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x00c9, code lost:
        r4 = r13.i - r4;
        r13.h = r4;
        a(r3, r13.g, r4);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void f(java.lang.String r14) {
        /*
            r13 = this;
            r12 = 6
            r11 = 2
            r10 = 0
            int r0 = r14.length()
            int r1 = r13.j
            if (r0 <= r1) goto L_0x0068
            r13.j()
            int r0 = r14.length()
            r1 = r10
        L_0x0013:
            int r2 = r13.j
            int r3 = r1 + r2
            if (r3 <= r0) goto L_0x001b
            int r2 = r0 - r1
        L_0x001b:
            int r3 = r1 + r2
            char[] r4 = r13.g
            r14.getChars(r1, r3, r4, r10)
            int[] r3 = a.a.a.a.b.f()
            int r4 = r3.length
            r5 = r10
        L_0x0028:
            if (r5 < r2) goto L_0x002e
        L_0x002a:
            int r1 = r1 + r2
            if (r1 < r0) goto L_0x0013
        L_0x002d:
            return
        L_0x002e:
            r6 = r5
        L_0x002f:
            char[] r7 = r13.g
            char r7 = r7[r6]
            if (r7 >= r4) goto L_0x0039
            r7 = r3[r7]
            if (r7 != 0) goto L_0x003d
        L_0x0039:
            int r6 = r6 + 1
            if (r6 < r2) goto L_0x002f
        L_0x003d:
            int r7 = r6 - r5
            if (r7 <= 0) goto L_0x004a
            java.io.Writer r8 = r13.f
            char[] r9 = r13.g
            r8.write(r9, r5, r7)
            if (r6 >= r2) goto L_0x002a
        L_0x004a:
            char[] r5 = r13.g
            char r5 = r5[r6]
            r5 = r3[r5]
            int r6 = r6 + 1
            if (r5 >= 0) goto L_0x005e
            r7 = r12
        L_0x0055:
            int r8 = r13.i
            if (r7 <= r8) goto L_0x0060
            r13.b(r5)
            r5 = r6
            goto L_0x0028
        L_0x005e:
            r7 = r11
            goto L_0x0055
        L_0x0060:
            int r6 = r6 - r7
            char[] r7 = r13.g
            a(r5, r7, r6)
            r5 = r6
            goto L_0x0028
        L_0x0068:
            int r1 = r13.i
            int r1 = r1 + r0
            int r2 = r13.j
            if (r1 <= r2) goto L_0x0072
            r13.j()
        L_0x0072:
            char[] r1 = r13.g
            int r2 = r13.i
            r14.getChars(r10, r0, r1, r2)
            int r1 = r13.i
            int r0 = r0 + r1
            int[] r1 = a.a.a.a.b.f()
            int r2 = r1.length
        L_0x0081:
            int r3 = r13.i
            if (r3 >= r0) goto L_0x002d
        L_0x0085:
            char[] r3 = r13.g
            int r4 = r13.i
            char r3 = r3[r4]
            if (r3 >= r2) goto L_0x0091
            r3 = r1[r3]
            if (r3 != 0) goto L_0x009a
        L_0x0091:
            int r3 = r13.i
            int r3 = r3 + 1
            r13.i = r3
            if (r3 < r0) goto L_0x0085
            goto L_0x002d
        L_0x009a:
            int r3 = r13.i
            int r4 = r13.h
            int r3 = r3 - r4
            if (r3 <= 0) goto L_0x00aa
            java.io.Writer r4 = r13.f
            char[] r5 = r13.g
            int r6 = r13.h
            r4.write(r5, r6, r3)
        L_0x00aa:
            char[] r3 = r13.g
            int r4 = r13.i
            char r3 = r3[r4]
            r3 = r1[r3]
            int r4 = r13.i
            int r4 = r4 + 1
            r13.i = r4
            if (r3 >= 0) goto L_0x00c7
            r4 = r12
        L_0x00bb:
            int r5 = r13.i
            if (r4 <= r5) goto L_0x00c9
            int r4 = r13.i
            r13.h = r4
            r13.b(r3)
            goto L_0x0081
        L_0x00c7:
            r4 = r11
            goto L_0x00bb
        L_0x00c9:
            int r5 = r13.i
            int r4 = r5 - r4
            r13.h = r4
            char[] r5 = r13.g
            a(r3, r5, r4)
            goto L_0x0081
        */
        throw new UnsupportedOperationException("Method not decompiled: a.a.a.b.m.f(java.lang.String):void");
    }

    private void j() {
        int i2 = this.i - this.h;
        if (i2 > 0) {
            int i3 = this.h;
            this.h = 0;
            this.i = 0;
            this.f.write(this.g, i3, i2);
        }
    }

    public final void a(double d2) {
        if (this.b || ((Double.isNaN(d2) || Double.isInfinite(d2)) && a(j.QUOTE_NON_NUMERIC_NUMBERS))) {
            b(String.valueOf(d2));
            return;
        }
        c("write number");
        e(String.valueOf(d2));
    }

    public final void a(float f2) {
        if (this.b || ((Float.isNaN(f2) || Float.isInfinite(f2)) && a(j.QUOTE_NON_NUMERIC_NUMBERS))) {
            b(String.valueOf(f2));
            return;
        }
        c("write number");
        e(String.valueOf(f2));
    }

    public final void a(int i2) {
        c("write number");
        if (this.i + 11 >= this.j) {
            j();
        }
        if (this.b) {
            if (this.i + 13 >= this.j) {
                j();
            }
            char[] cArr = this.g;
            int i3 = this.i;
            this.i = i3 + 1;
            cArr[i3] = '\"';
            this.i = e.a(i2, this.g, this.i);
            char[] cArr2 = this.g;
            int i4 = this.i;
            this.i = i4 + 1;
            cArr2[i4] = '\"';
            return;
        }
        this.i = e.a(i2, this.g, this.i);
    }

    public final void a(long j2) {
        c("write number");
        if (this.b) {
            if (this.i + 23 >= this.j) {
                j();
            }
            char[] cArr = this.g;
            int i2 = this.i;
            this.i = i2 + 1;
            cArr[i2] = '\"';
            this.i = e.a(j2, this.g, this.i);
            char[] cArr2 = this.g;
            int i3 = this.i;
            this.i = i3 + 1;
            cArr2[i3] = '\"';
            return;
        }
        if (this.i + 21 >= this.j) {
            j();
        }
        this.i = e.a(j2, this.g, this.i);
    }

    /* access modifiers changed from: protected */
    public final void a(String str, boolean z) {
        if (this.f28a == null) {
            if (this.i + 1 >= this.j) {
                j();
            }
            if (z) {
                char[] cArr = this.g;
                int i2 = this.i;
                this.i = i2 + 1;
                cArr[i2] = ',';
            }
            if (!a(j.QUOTE_FIELD_NAMES)) {
                f(str);
                return;
            }
            char[] cArr2 = this.g;
            int i3 = this.i;
            this.i = i3 + 1;
            cArr2[i3] = '\"';
            f(str);
            if (this.i >= this.j) {
                j();
            }
            char[] cArr3 = this.g;
            int i4 = this.i;
            this.i = i4 + 1;
            cArr3[i4] = '\"';
        } else if (a(j.QUOTE_FIELD_NAMES)) {
            if (this.i >= this.j) {
                j();
            }
            char[] cArr4 = this.g;
            int i5 = this.i;
            this.i = i5 + 1;
            cArr4[i5] = '\"';
            f(str);
            if (this.i >= this.j) {
                j();
            }
            char[] cArr5 = this.g;
            int i6 = this.i;
            this.i = i6 + 1;
            cArr5[i6] = '\"';
        } else {
            f(str);
        }
    }

    public final void a(boolean z) {
        int i2;
        c("write boolean value");
        if (this.i + 5 >= this.j) {
            j();
        }
        int i3 = this.i;
        char[] cArr = this.g;
        if (z) {
            cArr[i3] = 't';
            int i4 = i3 + 1;
            cArr[i4] = 'r';
            int i5 = i4 + 1;
            cArr[i5] = 'u';
            i2 = i5 + 1;
            cArr[i2] = 'e';
        } else {
            cArr[i3] = 'f';
            int i6 = i3 + 1;
            cArr[i6] = 'a';
            int i7 = i6 + 1;
            cArr[i7] = 'l';
            int i8 = i7 + 1;
            cArr[i8] = 's';
            i2 = i8 + 1;
            cArr[i2] = 'e';
        }
        this.i = i2 + 1;
    }

    public final void b(String str) {
        c("write text value");
        if (str == null) {
            if (this.i + 4 >= this.j) {
                j();
            }
            int i2 = this.i;
            char[] cArr = this.g;
            cArr[i2] = 'n';
            int i3 = i2 + 1;
            cArr[i3] = 'u';
            int i4 = i3 + 1;
            cArr[i4] = 'l';
            int i5 = i4 + 1;
            cArr[i5] = 'l';
            this.i = i5 + 1;
            return;
        }
        if (this.i >= this.j) {
            j();
        }
        char[] cArr2 = this.g;
        int i6 = this.i;
        this.i = i6 + 1;
        cArr2[i6] = '\"';
        f(str);
        if (this.i >= this.j) {
            j();
        }
        char[] cArr3 = this.g;
        int i7 = this.i;
        this.i = i7 + 1;
        cArr3[i7] = '\"';
    }

    /* access modifiers changed from: protected */
    public final void c(String str) {
        char c;
        int a2 = this.c.a();
        if (a2 == 5) {
            d("Can not " + str + ", expecting field name");
        }
        if (this.f28a == null) {
            switch (a2) {
                case 1:
                    c = ',';
                    break;
                case 2:
                    c = ':';
                    break;
                case 3:
                    c = ' ';
                    break;
                default:
                    return;
            }
            if (this.i >= this.j) {
                j();
            }
            this.g[this.i] = c;
            this.i++;
            return;
        }
        switch (a2) {
            case 0:
                if (this.c.b() || this.c.d()) {
                }
                return;
            case 1:
            case 2:
            case 3:
                return;
            default:
                throw new RuntimeException("Internal error: should never end up through this code path");
        }
    }

    public final void close() {
        super.close();
        if (this.g != null && a(j.AUTO_CLOSE_JSON_CONTENT)) {
            while (true) {
                h e2 = e();
                if (!e2.b()) {
                    if (!e2.d()) {
                        break;
                    }
                    d();
                } else {
                    b();
                }
            }
        }
        j();
        if (this.e.c() || a(j.AUTO_CLOSE_TARGET)) {
            this.f.close();
        } else {
            this.f.flush();
        }
        char[] cArr = this.g;
        if (cArr != null) {
            this.g = null;
            this.e.b(cArr);
        }
    }

    /* access modifiers changed from: protected */
    public final void f() {
        if (this.i >= this.j) {
            j();
        }
        char[] cArr = this.g;
        int i2 = this.i;
        this.i = i2 + 1;
        cArr[i2] = '[';
    }

    /* access modifiers changed from: protected */
    public final void g() {
        if (this.i >= this.j) {
            j();
        }
        char[] cArr = this.g;
        int i2 = this.i;
        this.i = i2 + 1;
        cArr[i2] = ']';
    }

    /* access modifiers changed from: protected */
    public final void h() {
        if (this.i >= this.j) {
            j();
        }
        char[] cArr = this.g;
        int i2 = this.i;
        this.i = i2 + 1;
        cArr[i2] = '{';
    }

    /* access modifiers changed from: protected */
    public final void i() {
        if (this.i >= this.j) {
            j();
        }
        char[] cArr = this.g;
        int i2 = this.i;
        this.i = i2 + 1;
        cArr[i2] = '}';
    }
}
