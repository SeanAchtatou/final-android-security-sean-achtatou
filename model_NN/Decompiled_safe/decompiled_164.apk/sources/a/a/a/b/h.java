package a.a.a.b;

import a.a.a.k;

public abstract class h extends k {
    private h c;
    private h d = null;
    private h e = null;

    protected h(int i, h hVar) {
        super(i);
        this.c = hVar;
    }

    public abstract int a();

    public abstract int a(String str);

    /* access modifiers changed from: protected */
    public abstract void a(StringBuilder sb);

    public final h g() {
        h hVar = this.d;
        if (hVar == null) {
            d dVar = new d(this);
            this.d = dVar;
            return dVar;
        }
        hVar.b = -1;
        return hVar;
    }

    public final h h() {
        h hVar = this.e;
        if (hVar == null) {
            n nVar = new n(this);
            this.e = nVar;
            return nVar;
        }
        hVar.b = -1;
        return hVar;
    }

    public final h i() {
        return this.c;
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder(64);
        a(sb);
        return sb.toString();
    }
}
