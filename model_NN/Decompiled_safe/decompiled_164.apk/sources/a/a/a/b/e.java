package a.a.a.b;

import a.a.a.c.a;
import java.io.IOException;
import java.io.Reader;

public abstract class e extends b {

    /* renamed from: a  reason: collision with root package name */
    protected char[] f11a;
    private Reader s;

    protected e(a aVar, int i, Reader reader) {
        super(aVar, i);
        this.s = reader;
        this.f11a = aVar.f();
    }

    /* access modifiers changed from: protected */
    public final boolean a() {
        this.e += (long) this.d;
        this.g -= this.d;
        if (this.s != null) {
            int read = this.s.read(this.f11a, 0, this.f11a.length);
            if (read > 0) {
                this.c = 0;
                this.d = read;
                return true;
            }
            b();
            if (read == 0) {
                throw new IOException("Reader returned 0 characters when trying to read " + this.d);
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public final void b() {
        if (this.s != null) {
            if (this.b.c() || a(a.a.a.e.AUTO_CLOSE_SOURCE)) {
                this.s.close();
            }
            this.s = null;
        }
    }

    /* access modifiers changed from: protected */
    public final void c() {
        super.c();
        char[] cArr = this.f11a;
        if (cArr != null) {
            this.f11a = null;
            this.b.a(cArr);
        }
    }

    /* access modifiers changed from: protected */
    public final char e(String str) {
        if (this.c >= this.d && !a()) {
            b(str);
        }
        char[] cArr = this.f11a;
        int i = this.c;
        this.c = i + 1;
        return cArr[i];
    }
}
