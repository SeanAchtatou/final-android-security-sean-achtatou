package a.a.a.b;

import a.a.a.b;
import a.a.a.c.a;
import java.io.CharConversionException;
import java.io.InputStream;

public final class f {
    private static /* synthetic */ int[] j;

    /* renamed from: a  reason: collision with root package name */
    private a f12a;
    private InputStream b;
    private byte[] c;
    private int d;
    private int e;
    private final boolean f;
    private int g;
    private boolean h = true;
    private int i = 0;

    public f(a aVar, InputStream inputStream) {
        this.f12a = aVar;
        this.b = inputStream;
        this.c = aVar.e();
        this.d = 0;
        this.e = 0;
        this.g = 0;
        this.f = true;
    }

    public f(a aVar, byte[] bArr, int i2) {
        this.f12a = aVar;
        this.b = null;
        this.c = bArr;
        this.d = 0;
        this.e = i2 + 0;
        this.g = 0;
        this.f = false;
    }

    private static void a(String str) {
        throw new CharConversionException("Unsupported UCS-4 endianness (" + str + ") detected");
    }

    private boolean a(int i2) {
        if ((65280 & i2) == 0) {
            this.h = true;
        } else if ((i2 & 255) != 0) {
            return false;
        } else {
            this.h = false;
        }
        this.i = 2;
        return true;
    }

    private static /* synthetic */ int[] a() {
        int[] iArr = j;
        if (iArr == null) {
            iArr = new int[b.values().length];
            try {
                iArr[b.UTF16_BE.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[b.UTF16_LE.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[b.UTF32_BE.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[b.UTF32_LE.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[b.UTF8.ordinal()] = 1;
            } catch (NoSuchFieldError e6) {
            }
            j = iArr;
        }
        return iArr;
    }

    private boolean b(int i2) {
        int i3 = this.e - this.d;
        while (i3 < i2) {
            int read = this.b == null ? -1 : this.b.read(this.c, this.e, this.c.length - this.e);
            if (read <= 0) {
                return false;
            }
            this.e += read;
            i3 += read;
        }
        return true;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x00a5  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00d6  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x00fc  */
    /* JADX WARNING: Removed duplicated region for block: B:6:0x0041  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final a.a.a.h a(int r12, a.a.a.d r13, a.a.a.d.h r14, a.a.a.d.g r15) {
        /*
            r11 = this;
            r6 = 2
            r5 = 4
            r4 = 0
            r3 = 1
            boolean r0 = r11.b(r5)
            if (r0 == 0) goto L_0x0107
            byte[] r0 = r11.c
            int r1 = r11.d
            byte r0 = r0[r1]
            int r0 = r0 << 24
            byte[] r1 = r11.c
            int r2 = r11.d
            int r2 = r2 + 1
            byte r1 = r1[r2]
            r1 = r1 & 255(0xff, float:3.57E-43)
            int r1 = r1 << 16
            r0 = r0 | r1
            byte[] r1 = r11.c
            int r2 = r11.d
            int r2 = r2 + 2
            byte r1 = r1[r2]
            r1 = r1 & 255(0xff, float:3.57E-43)
            int r1 = r1 << 8
            r0 = r0 | r1
            byte[] r1 = r11.c
            int r2 = r11.d
            int r2 = r2 + 3
            byte r1 = r1[r2]
            r1 = r1 & 255(0xff, float:3.57E-43)
            r0 = r0 | r1
            switch(r0) {
                case -16842752: goto L_0x009f;
                case -131072: goto L_0x008e;
                case 65279: goto L_0x0082;
                case 65534: goto L_0x009a;
                default: goto L_0x003a;
            }
        L_0x003a:
            int r1 = r0 >>> 16
            r2 = 65279(0xfeff, float:9.1475E-41)
            if (r1 != r2) goto L_0x00a5
            int r1 = r11.d
            int r1 = r1 + 2
            r11.d = r1
            r11.i = r6
            r11.h = r3
            r1 = r3
        L_0x004c:
            if (r1 == 0) goto L_0x00cb
            r0 = r3
        L_0x004f:
            if (r0 != 0) goto L_0x012b
            a.a.a.b r0 = a.a.a.b.UTF8
        L_0x0053:
            a.a.a.c.a r1 = r11.f12a
            r1.a(r0)
            a.a.a.e r1 = a.a.a.e.CANONICALIZE_FIELD_NAMES
            boolean r7 = r1.a(r12)
            a.a.a.e r1 = a.a.a.e.INTERN_FIELD_NAMES
            boolean r8 = r1.a(r12)
            a.a.a.b r1 = a.a.a.b.UTF8
            if (r0 != r1) goto L_0x0153
            if (r7 == 0) goto L_0x0153
            a.a.a.d.h r5 = r14.a(r8)
            a.a.a.b.p r0 = new a.a.a.b.p
            a.a.a.c.a r1 = r11.f12a
            java.io.InputStream r3 = r11.b
            byte[] r6 = r11.c
            int r7 = r11.d
            int r8 = r11.e
            boolean r9 = r11.f
            r2 = r12
            r4 = r13
            r0.<init>(r1, r2, r3, r4, r5, r6, r7, r8, r9)
        L_0x0081:
            return r0
        L_0x0082:
            r11.h = r3
            int r1 = r11.d
            int r1 = r1 + 4
            r11.d = r1
            r11.i = r5
            r1 = r3
            goto L_0x004c
        L_0x008e:
            int r1 = r11.d
            int r1 = r1 + 4
            r11.d = r1
            r11.i = r5
            r11.h = r4
            r1 = r3
            goto L_0x004c
        L_0x009a:
            java.lang.String r1 = "2143"
            a(r1)
        L_0x009f:
            java.lang.String r1 = "3412"
            a(r1)
            goto L_0x003a
        L_0x00a5:
            r2 = 65534(0xfffe, float:9.1833E-41)
            if (r1 != r2) goto L_0x00b6
            int r1 = r11.d
            int r1 = r1 + 2
            r11.d = r1
            r11.i = r6
            r11.h = r4
            r1 = r3
            goto L_0x004c
        L_0x00b6:
            int r1 = r0 >>> 8
            r2 = 15711167(0xefbbbf, float:2.2016034E-38)
            if (r1 != r2) goto L_0x00c9
            int r1 = r11.d
            int r1 = r1 + 3
            r11.d = r1
            r11.i = r3
            r11.h = r3
            r1 = r3
            goto L_0x004c
        L_0x00c9:
            r1 = r4
            goto L_0x004c
        L_0x00cb:
            int r1 = r0 >> 8
            if (r1 != 0) goto L_0x00d9
            r11.h = r3
        L_0x00d1:
            r11.i = r5
            r1 = r3
        L_0x00d4:
            if (r1 == 0) goto L_0x00fc
            r0 = r3
            goto L_0x004f
        L_0x00d9:
            r1 = 16777215(0xffffff, float:2.3509886E-38)
            r1 = r1 & r0
            if (r1 != 0) goto L_0x00e2
            r11.h = r4
            goto L_0x00d1
        L_0x00e2:
            r1 = -16711681(0xffffffffff00ffff, float:-1.714704E38)
            r1 = r1 & r0
            if (r1 != 0) goto L_0x00ee
            java.lang.String r1 = "3412"
            a(r1)
            goto L_0x00d1
        L_0x00ee:
            r1 = -65281(0xffffffffffff00ff, float:NaN)
            r1 = r1 & r0
            if (r1 != 0) goto L_0x00fa
            java.lang.String r1 = "2143"
            a(r1)
            goto L_0x00d1
        L_0x00fa:
            r1 = r4
            goto L_0x00d4
        L_0x00fc:
            int r0 = r0 >>> 16
            boolean r0 = r11.a(r0)
            if (r0 == 0) goto L_0x01ca
            r0 = r3
            goto L_0x004f
        L_0x0107:
            boolean r0 = r11.b(r6)
            if (r0 == 0) goto L_0x01ca
            byte[] r0 = r11.c
            int r1 = r11.d
            byte r0 = r0[r1]
            r0 = r0 & 255(0xff, float:3.57E-43)
            int r0 = r0 << 8
            byte[] r1 = r11.c
            int r2 = r11.d
            int r2 = r2 + 1
            byte r1 = r1[r2]
            r1 = r1 & 255(0xff, float:3.57E-43)
            r0 = r0 | r1
            boolean r0 = r11.a(r0)
            if (r0 == 0) goto L_0x01ca
            r0 = r3
            goto L_0x004f
        L_0x012b:
            int r0 = r11.i
            if (r0 != r6) goto L_0x013b
            boolean r0 = r11.h
            if (r0 == 0) goto L_0x0137
            a.a.a.b r0 = a.a.a.b.UTF16_BE
            goto L_0x0053
        L_0x0137:
            a.a.a.b r0 = a.a.a.b.UTF16_LE
            goto L_0x0053
        L_0x013b:
            int r0 = r11.i
            if (r0 != r5) goto L_0x014b
            boolean r0 = r11.h
            if (r0 == 0) goto L_0x0147
            a.a.a.b r0 = a.a.a.b.UTF32_BE
            goto L_0x0053
        L_0x0147:
            a.a.a.b r0 = a.a.a.b.UTF32_LE
            goto L_0x0053
        L_0x014b:
            java.lang.RuntimeException r0 = new java.lang.RuntimeException
            java.lang.String r1 = "Internal error"
            r0.<init>(r1)
            throw r0
        L_0x0153:
            a.a.a.b.i r9 = new a.a.a.b.i
            a.a.a.c.a r10 = r11.f12a
            a.a.a.c.a r0 = r11.f12a
            a.a.a.b r6 = r0.b()
            int[] r0 = a()
            int r1 = r6.ordinal()
            r0 = r0[r1]
            switch(r0) {
                case 1: goto L_0x019a;
                case 2: goto L_0x019a;
                case 3: goto L_0x019a;
                case 4: goto L_0x0172;
                case 5: goto L_0x0172;
                default: goto L_0x016a;
            }
        L_0x016a:
            java.lang.RuntimeException r0 = new java.lang.RuntimeException
            java.lang.String r1 = "Internal error"
            r0.<init>(r1)
            throw r0
        L_0x0172:
            a.a.a.c.f r0 = new a.a.a.c.f
            a.a.a.c.a r1 = r11.f12a
            java.io.InputStream r2 = r11.b
            byte[] r3 = r11.c
            int r4 = r11.d
            int r5 = r11.e
            a.a.a.c.a r6 = r11.f12a
            a.a.a.b r6 = r6.b()
            boolean r6 = r6.b()
            r0.<init>(r1, r2, r3, r4, r5, r6)
            r3 = r0
        L_0x018c:
            a.a.a.d.g r5 = r15.a(r7, r8)
            r0 = r9
            r1 = r10
            r2 = r12
            r4 = r13
            r0.<init>(r1, r2, r3, r4, r5)
            r0 = r9
            goto L_0x0081
        L_0x019a:
            java.io.InputStream r2 = r11.b
            if (r2 != 0) goto L_0x01b4
            java.io.ByteArrayInputStream r0 = new java.io.ByteArrayInputStream
            byte[] r1 = r11.c
            int r2 = r11.d
            int r3 = r11.e
            r0.<init>(r1, r2, r3)
        L_0x01a9:
            java.io.InputStreamReader r1 = new java.io.InputStreamReader
            java.lang.String r2 = r6.a()
            r1.<init>(r0, r2)
            r3 = r1
            goto L_0x018c
        L_0x01b4:
            int r0 = r11.d
            int r1 = r11.e
            if (r0 >= r1) goto L_0x01c8
            a.a.a.c.c r0 = new a.a.a.c.c
            a.a.a.c.a r1 = r11.f12a
            byte[] r3 = r11.c
            int r4 = r11.d
            int r5 = r11.e
            r0.<init>(r1, r2, r3, r4, r5)
            goto L_0x01a9
        L_0x01c8:
            r0 = r2
            goto L_0x01a9
        L_0x01ca:
            r0 = r4
            goto L_0x004f
        */
        throw new UnsupportedOperationException("Method not decompiled: a.a.a.b.f.a(int, a.a.a.d, a.a.a.d.h, a.a.a.d.g):a.a.a.h");
    }
}
