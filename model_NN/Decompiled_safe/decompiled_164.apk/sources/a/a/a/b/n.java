package a.a.a.b;

final class n extends h {
    private String c = null;
    private boolean d = false;

    public n(h hVar) {
        super(2, hVar);
    }

    public final int a() {
        if (this.c == null) {
            return 5;
        }
        this.c = null;
        this.b++;
        return 2;
    }

    public final int a(String str) {
        if (this.c != null) {
            return 4;
        }
        this.c = str;
        return this.b < 0 ? 0 : 1;
    }

    /* access modifiers changed from: protected */
    public final void a(StringBuilder sb) {
        sb.append('{');
        if (this.c != null) {
            sb.append('\"');
            sb.append(this.c);
            sb.append('\"');
        } else {
            sb.append('?');
        }
        sb.append(']');
    }
}
