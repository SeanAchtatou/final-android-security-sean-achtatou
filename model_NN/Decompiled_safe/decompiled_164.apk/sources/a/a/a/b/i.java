package a.a.a.b;

import a.a.a.a.b;
import a.a.a.a.f;
import a.a.a.c.a;
import a.a.a.d;
import a.a.a.d.g;
import a.a.a.e;
import a.a.a.m;
import java.io.Reader;

public final class i extends g {
    private d s;
    private g t;

    public i(a aVar, int i, Reader reader, d dVar, g gVar) {
        super(aVar, i, reader);
        this.s = dVar;
        this.t = gVar;
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x005c  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0090  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.String a(int r9, int r10, int r11) {
        /*
            r8 = this;
            r6 = 92
            a.a.a.a.f r0 = r8.n
            char[] r1 = r8.f11a
            int r2 = r8.c
            int r2 = r2 - r9
            r0.a(r1, r9, r2)
            a.a.a.a.f r0 = r8.n
            char[] r0 = r0.h()
            a.a.a.a.f r1 = r8.n
            int r1 = r1.j()
            r2 = r0
            r0 = r1
            r1 = r10
        L_0x001b:
            int r3 = r8.c
            int r4 = r8.d
            if (r3 < r4) goto L_0x0040
            boolean r3 = r8.a()
            if (r3 != 0) goto L_0x0040
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r4 = ": was expecting closing '"
            r3.<init>(r4)
            char r4 = (char) r11
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r4 = "' for name"
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r3 = r3.toString()
            r8.b(r3)
        L_0x0040:
            char[] r3 = r8.f11a
            int r4 = r8.c
            int r5 = r4 + 1
            r8.c = r5
            char r3 = r3[r4]
            if (r3 > r6) goto L_0x0074
            if (r3 != r6) goto L_0x0067
            char r4 = r8.v()
        L_0x0052:
            int r1 = r1 * 31
            int r1 = r1 + r3
            int r3 = r0 + 1
            r2[r0] = r4
            int r0 = r2.length
            if (r3 < r0) goto L_0x0090
            a.a.a.a.f r0 = r8.n
            char[] r0 = r0.k()
            r2 = 0
            r7 = r2
            r2 = r0
            r0 = r7
            goto L_0x001b
        L_0x0067:
            if (r3 > r11) goto L_0x0074
            if (r3 == r11) goto L_0x0076
            r4 = 32
            if (r3 >= r4) goto L_0x0074
            java.lang.String r4 = "name"
            r8.c(r3, r4)
        L_0x0074:
            r4 = r3
            goto L_0x0052
        L_0x0076:
            a.a.a.a.f r2 = r8.n
            r2.a(r0)
            a.a.a.a.f r0 = r8.n
            char[] r2 = r0.d()
            int r3 = r0.c()
            int r0 = r0.b()
            a.a.a.d.g r4 = r8.t
            java.lang.String r0 = r4.a(r2, r3, r0, r1)
            return r0
        L_0x0090:
            r0 = r3
            goto L_0x001b
        */
        throw new UnsupportedOperationException("Method not decompiled: a.a.a.b.i.a(int, int, int):java.lang.String");
    }

    private void a(m mVar) {
        String a2 = mVar.a();
        int length = a2.length();
        for (int i = 1; i < length; i++) {
            if (this.c >= this.d && !a()) {
                b(" in a value");
            }
            if (this.f11a[this.c] != a2.charAt(i)) {
                StringBuilder sb = new StringBuilder(a2.substring(0, i));
                while (true) {
                    if (this.c >= this.d && !a()) {
                        break;
                    }
                    char c = this.f11a[this.c];
                    if (!Character.isJavaIdentifierPart(c)) {
                        break;
                    }
                    this.c++;
                    sb.append(c);
                }
                c("Unrecognized token '" + sb.toString() + "': was expecting 'null', 'true' or 'false'");
            }
            this.c++;
        }
    }

    private String d(int i) {
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        if (i == 34) {
            int i7 = this.c;
            int i8 = this.d;
            if (i7 < i8) {
                int[] a2 = b.a();
                int length = a2.length;
                int i9 = i7;
                i3 = 0;
                while (true) {
                    char c = this.f11a[i9];
                    if (c >= length || a2[c] == 0) {
                        i3 = (i3 * 31) + c;
                        i9++;
                        if (i9 >= i8) {
                            break;
                        }
                    } else if (c == '\"') {
                        int i10 = this.c;
                        this.c = i9 + 1;
                        return this.t.a(this.f11a, i10, i9 - i10, i3);
                    }
                }
                i2 = i9;
            } else {
                i2 = i7;
                i3 = 0;
            }
            int i11 = this.c;
            this.c = i2;
            return a(i11, i3, 34);
        } else if (i != 39 || !a(e.ALLOW_SINGLE_QUOTES)) {
            if (!a(e.ALLOW_UNQUOTED_FIELD_NAMES)) {
                b(i, "was expecting double-quote to start field name");
            }
            int[] c2 = b.c();
            int length2 = c2.length;
            if (!(i < length2 ? c2[i] == 0 && (i < 48 || i > 57) : Character.isJavaIdentifierPart((char) i))) {
                b(i, "was expecting either valid name character (for unquoted name) or double-quote (for quoted) to start field name");
            }
            int i12 = this.c;
            int i13 = this.d;
            if (i12 < i13) {
                int i14 = i12;
                int i15 = 0;
                do {
                    char c3 = this.f11a[i14];
                    if (c3 < length2) {
                        if (c2[c3] != 0) {
                            int i16 = this.c - 1;
                            this.c = i14;
                            return this.t.a(this.f11a, i16, i14 - i16, i15);
                        }
                    } else if (!Character.isJavaIdentifierPart((char) c3)) {
                        int i17 = this.c - 1;
                        this.c = i14;
                        return this.t.a(this.f11a, i17, i14 - i17, i15);
                    }
                    i15 = (i15 * 31) + c3;
                    i14++;
                } while (i14 < i13);
                i4 = i15;
                i12 = i14;
            } else {
                i4 = 0;
            }
            int i18 = this.c - 1;
            this.c = i12;
            this.n.a(this.f11a, i18, this.c - i18);
            char[] h = this.n.h();
            int j = this.n.j();
            int length3 = c2.length;
            int i19 = j;
            int i20 = i4;
            int i21 = i19;
            while (true) {
                if (this.c >= this.d && !a()) {
                    break;
                }
                char c4 = this.f11a[this.c];
                if (c4 > length3) {
                    if (!Character.isJavaIdentifierPart(c4)) {
                        break;
                    }
                } else if (c2[c4] != 0) {
                    break;
                }
                this.c++;
                i20 = (i20 * 31) + c4;
                int i22 = i21 + 1;
                h[i21] = c4;
                if (i22 >= h.length) {
                    h = this.n.k();
                    i21 = 0;
                } else {
                    i21 = i22;
                }
            }
            this.n.a(i21);
            f fVar = this.n;
            return this.t.a(fVar.d(), fVar.c(), fVar.b(), i20);
        } else {
            int i23 = this.c;
            int i24 = this.d;
            if (i23 < i24) {
                int[] a3 = b.a();
                int length4 = a3.length;
                int i25 = i23;
                i6 = 0;
                do {
                    char c5 = this.f11a[i25];
                    if (c5 != '\'') {
                        if (c5 < length4 && a3[c5] != 0) {
                            break;
                        }
                        i6 = (i6 * 31) + c5;
                        i25++;
                    } else {
                        int i26 = this.c;
                        this.c = i25 + 1;
                        return this.t.a(this.f11a, i26, i25 - i26, i6);
                    }
                } while (i25 < i24);
                i5 = i25;
            } else {
                i5 = i23;
                i6 = 0;
            }
            int i27 = this.c;
            this.c = i5;
            return a(i27, i6, 39);
        }
    }

    private m e(int i) {
        if (i != 39 || !a(e.ALLOW_SINGLE_QUOTES)) {
            b(i, "expected a valid value (number, String, array, object, 'true', 'false' or 'null')");
        }
        char[] i2 = this.n.i();
        int j = this.n.j();
        while (true) {
            if (this.c >= this.d && !a()) {
                b(": was expecting closing quote for a string value");
            }
            char[] cArr = this.f11a;
            int i3 = this.c;
            this.c = i3 + 1;
            char c = cArr[i3];
            if (c <= '\\') {
                if (c == '\\') {
                    c = v();
                } else if (c <= '\'') {
                    if (c == '\'') {
                        this.n.a(j);
                        return m.VALUE_STRING;
                    } else if (c < ' ') {
                        c(c, "string value");
                    }
                }
            }
            if (j >= i2.length) {
                i2 = this.n.k();
                j = 0;
            }
            i2[j] = c;
            j++;
        }
    }

    private void r() {
        if ((this.c < this.d || a()) && this.f11a[this.c] == 10) {
            this.c++;
        }
        this.f++;
        this.g = this.c;
    }

    private void s() {
        this.f++;
        this.g = this.c;
    }

    private final int t() {
        while (true) {
            if (this.c < this.d || a()) {
                char[] cArr = this.f11a;
                int i = this.c;
                this.c = i + 1;
                char c = cArr[i];
                if (c > ' ') {
                    if (c != '/') {
                        return c;
                    }
                    u();
                } else if (c != ' ') {
                    if (c == 10) {
                        s();
                    } else if (c == 13) {
                        r();
                    } else if (c != 9) {
                        a(c);
                    }
                }
            } else {
                throw d("Unexpected end-of-input within/between " + this.k.e() + " entries");
            }
        }
    }

    private final void u() {
        if (!a(e.ALLOW_COMMENTS)) {
            b(47, "maybe a (non-standard) comment? (not recognized as one since Feature 'ALLOW_COMMENTS' not enabled for parser)");
        }
        if (this.c >= this.d && !a()) {
            b(" in a comment");
        }
        char[] cArr = this.f11a;
        int i = this.c;
        this.c = i + 1;
        char c = cArr[i];
        if (c == '/') {
            while (true) {
                if (this.c < this.d || a()) {
                    char[] cArr2 = this.f11a;
                    int i2 = this.c;
                    this.c = i2 + 1;
                    char c2 = cArr2[i2];
                    if (c2 < ' ') {
                        if (c2 == 10) {
                            s();
                            return;
                        } else if (c2 == 13) {
                            r();
                            return;
                        } else if (c2 != 9) {
                            a(c2);
                        }
                    }
                } else {
                    return;
                }
            }
        } else if (c == '*') {
            while (true) {
                if (this.c >= this.d && !a()) {
                    break;
                }
                char[] cArr3 = this.f11a;
                int i3 = this.c;
                this.c = i3 + 1;
                char c3 = cArr3[i3];
                if (c3 <= '*') {
                    if (c3 == '*') {
                        if (this.c >= this.d && !a()) {
                            break;
                        } else if (this.f11a[this.c] == '/') {
                            this.c++;
                            return;
                        }
                    } else if (c3 < ' ') {
                        if (c3 == 10) {
                            s();
                        } else if (c3 == 13) {
                            r();
                        } else if (c3 != 9) {
                            a(c3);
                        }
                    }
                }
            }
            b(" in a comment");
        } else {
            b(c, "was expecting either '*' or '/' for a comment");
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    private char v() {
        if (this.c >= this.d && !a()) {
            b(" in character escape sequence");
        }
        char[] cArr = this.f11a;
        int i = this.c;
        this.c = i + 1;
        char c = cArr[i];
        switch (c) {
            case '\"':
            case '/':
            case '\\':
                return c;
            case 'b':
                return 8;
            case 'f':
                return 12;
            case 'n':
                return 10;
            case 'r':
                return 13;
            case 't':
                return 9;
            case 'u':
                break;
            default:
                c("Unrecognized character escape " + b(c));
                break;
        }
        int i2 = 0;
        for (int i3 = 0; i3 < 4; i3++) {
            if (this.c >= this.d && !a()) {
                b(" in character escape sequence");
            }
            char[] cArr2 = this.f11a;
            int i4 = this.c;
            this.c = i4 + 1;
            char c2 = cArr2[i4];
            int a2 = b.a(c2);
            if (a2 < 0) {
                b(c2, "expected a hex-digit for character escape sequence");
            }
            i2 = (i2 << 4) | a2;
        }
        return (char) i2;
    }

    public final void close() {
        super.close();
        this.t.b();
    }

    /* access modifiers changed from: protected */
    public final void h() {
        int i = this.c;
        int i2 = this.d;
        if (i < i2) {
            int[] a2 = b.a();
            int length = a2.length;
            while (true) {
                char c = this.f11a[i];
                if (c >= length || a2[c] == 0) {
                    i++;
                    if (i >= i2) {
                        break;
                    }
                } else if (c == '\"') {
                    this.n.a(this.f11a, this.c, i - this.c);
                    this.c = i + 1;
                    return;
                }
            }
        }
        this.n.b(this.f11a, this.c, i - this.c);
        this.c = i;
        char[] h = this.n.h();
        int j = this.n.j();
        while (true) {
            if (this.c >= this.d && !a()) {
                b(": was expecting closing quote for a string value");
            }
            char[] cArr = this.f11a;
            int i3 = this.c;
            this.c = i3 + 1;
            char c2 = cArr[i3];
            if (c2 <= '\\') {
                if (c2 == '\\') {
                    c2 = v();
                } else if (c2 <= '\"') {
                    if (c2 == '\"') {
                        this.n.a(j);
                        return;
                    } else if (c2 < ' ') {
                        c(c2, "string value");
                    }
                }
            }
            if (j >= h.length) {
                h = this.n.k();
                j = 0;
            }
            h[j] = c2;
            j++;
        }
    }

    /* JADX WARN: Failed to insert an additional move for type inference into block B:98:0x008b */
    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v6, resolved type: char} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v7, resolved type: char} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v51, resolved type: char} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v52, resolved type: char} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v53, resolved type: char} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v54, resolved type: char} */
    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.b.c.a(int, char):void
     arg types: [char, int]
     candidates:
      a.a.a.b.b.a(int, java.lang.String):void
      a.a.a.b.c.a(int, char):void */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final a.a.a.m i() {
        /*
            r10 = this;
            r8 = 34
            r7 = 1
            r2 = 0
            r6 = 0
            r5 = 32
            a.a.a.m r0 = r10.r
            a.a.a.m r1 = a.a.a.m.FIELD_NAME
            if (r0 != r1) goto L_0x0037
            r10.o = r2
            a.a.a.m r0 = r10.l
            r10.l = r6
            a.a.a.m r1 = a.a.a.m.START_ARRAY
            if (r0 != r1) goto L_0x0026
            a.a.a.b.o r1 = r10.k
            int r2 = r10.i
            int r3 = r10.j
            a.a.a.b.o r1 = r1.a(r2, r3)
            r10.k = r1
        L_0x0023:
            r10.r = r0
        L_0x0025:
            return r0
        L_0x0026:
            a.a.a.m r1 = a.a.a.m.START_OBJECT
            if (r0 != r1) goto L_0x0023
            a.a.a.b.o r1 = r10.k
            int r2 = r10.i
            int r3 = r10.j
            a.a.a.b.o r1 = r1.b(r2, r3)
            r10.k = r1
            goto L_0x0023
        L_0x0037:
            boolean r0 = r10.m
            if (r0 == 0) goto L_0x007b
            r10.m = r2
            int r0 = r10.c
            int r1 = r10.d
            char[] r2 = r10.f11a
            r9 = r1
            r1 = r0
            r0 = r9
        L_0x0046:
            if (r1 < r0) goto L_0x005c
            r10.c = r1
            boolean r0 = r10.a()
            if (r0 != 0) goto L_0x0055
            java.lang.String r0 = ": was expecting closing quote for a string value"
            r10.b(r0)
        L_0x0055:
            int r0 = r10.c
            int r1 = r10.d
            r9 = r1
            r1 = r0
            r0 = r9
        L_0x005c:
            int r3 = r1 + 1
            char r1 = r2[r1]
            r4 = 92
            if (r1 > r4) goto L_0x01d4
            r4 = 92
            if (r1 != r4) goto L_0x0075
            r10.c = r3
            r10.v()
            int r0 = r10.c
            int r1 = r10.d
            r9 = r1
            r1 = r0
            r0 = r9
            goto L_0x0046
        L_0x0075:
            if (r1 > r8) goto L_0x01d4
            if (r1 != r8) goto L_0x0094
            r10.c = r3
        L_0x007b:
            int r0 = r10.c
            int r1 = r10.d
            if (r0 < r1) goto L_0x009f
            boolean r0 = r10.a()
            if (r0 != 0) goto L_0x009f
            r10.o()
            r0 = -1
        L_0x008b:
            if (r0 >= 0) goto L_0x00cd
            r10.close()
            r10.r = r6
            r0 = r6
            goto L_0x0025
        L_0x0094:
            if (r1 >= r5) goto L_0x01d4
            r10.c = r3
            java.lang.String r4 = "string value"
            r10.c(r1, r4)
            r1 = r3
            goto L_0x0046
        L_0x009f:
            char[] r0 = r10.f11a
            int r1 = r10.c
            int r2 = r1 + 1
            r10.c = r2
            char r0 = r0[r1]
            if (r0 <= r5) goto L_0x00b3
            r1 = 47
            if (r0 != r1) goto L_0x008b
            r10.u()
            goto L_0x007b
        L_0x00b3:
            if (r0 == r5) goto L_0x007b
            r1 = 10
            if (r0 != r1) goto L_0x00bd
            r10.s()
            goto L_0x007b
        L_0x00bd:
            r1 = 13
            if (r0 != r1) goto L_0x00c5
            r10.r()
            goto L_0x007b
        L_0x00c5:
            r1 = 9
            if (r0 == r1) goto L_0x007b
            r10.a(r0)
            goto L_0x007b
        L_0x00cd:
            long r1 = r10.e
            int r3 = r10.c
            long r3 = (long) r3
            long r1 = r1 + r3
            r3 = 1
            long r1 = r1 - r3
            r10.h = r1
            int r1 = r10.f
            r10.i = r1
            int r1 = r10.c
            int r2 = r10.g
            int r1 = r1 - r2
            int r1 = r1 - r7
            r10.j = r1
            r10.p = r6
            r1 = 93
            if (r0 != r1) goto L_0x0105
            a.a.a.b.o r1 = r10.k
            boolean r1 = r1.b()
            if (r1 != 0) goto L_0x00f7
            r1 = 125(0x7d, float:1.75E-43)
            r10.a(r0, r1)
        L_0x00f7:
            a.a.a.b.o r0 = r10.k
            a.a.a.b.o r0 = r0.g()
            r10.k = r0
            a.a.a.m r0 = a.a.a.m.END_ARRAY
            r10.r = r0
            goto L_0x0025
        L_0x0105:
            r1 = 125(0x7d, float:1.75E-43)
            if (r0 != r1) goto L_0x0124
            a.a.a.b.o r1 = r10.k
            boolean r1 = r1.d()
            if (r1 != 0) goto L_0x0116
            r1 = 93
            r10.a(r0, r1)
        L_0x0116:
            a.a.a.b.o r0 = r10.k
            a.a.a.b.o r0 = r0.g()
            r10.k = r0
            a.a.a.m r0 = a.a.a.m.END_OBJECT
            r10.r = r0
            goto L_0x0025
        L_0x0124:
            a.a.a.b.o r1 = r10.k
            boolean r1 = r1.h()
            if (r1 == 0) goto L_0x0152
            r1 = 44
            if (r0 == r1) goto L_0x014e
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r2 = "was expecting comma to separate "
            r1.<init>(r2)
            a.a.a.b.o r2 = r10.k
            java.lang.String r2 = r2.e()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = " entries"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r10.b(r0, r1)
        L_0x014e:
            int r0 = r10.t()
        L_0x0152:
            a.a.a.b.o r1 = r10.k
            boolean r1 = r1.d()
            if (r1 == 0) goto L_0x0178
            java.lang.String r0 = r10.d(r0)
            a.a.a.b.o r2 = r10.k
            r2.a(r0)
            a.a.a.m r0 = a.a.a.m.FIELD_NAME
            r10.r = r0
            int r0 = r10.t()
            r2 = 58
            if (r0 == r2) goto L_0x0174
            java.lang.String r2 = "was expecting a colon to separate field name and value"
            r10.b(r0, r2)
        L_0x0174:
            int r0 = r10.t()
        L_0x0178:
            switch(r0) {
                case 34: goto L_0x0187;
                case 45: goto L_0x01cb;
                case 48: goto L_0x01cb;
                case 49: goto L_0x01cb;
                case 50: goto L_0x01cb;
                case 51: goto L_0x01cb;
                case 52: goto L_0x01cb;
                case 53: goto L_0x01cb;
                case 54: goto L_0x01cb;
                case 55: goto L_0x01cb;
                case 56: goto L_0x01cb;
                case 57: goto L_0x01cb;
                case 91: goto L_0x018c;
                case 93: goto L_0x01ae;
                case 102: goto L_0x01bb;
                case 110: goto L_0x01c3;
                case 116: goto L_0x01b3;
                case 123: goto L_0x019d;
                case 125: goto L_0x01ae;
                default: goto L_0x017b;
            }
        L_0x017b:
            a.a.a.m r0 = r10.e(r0)
        L_0x017f:
            if (r1 == 0) goto L_0x01d0
            r10.l = r0
            a.a.a.m r0 = r10.r
            goto L_0x0025
        L_0x0187:
            r10.m = r7
            a.a.a.m r0 = a.a.a.m.VALUE_STRING
            goto L_0x017f
        L_0x018c:
            if (r1 != 0) goto L_0x019a
            a.a.a.b.o r0 = r10.k
            int r2 = r10.i
            int r3 = r10.j
            a.a.a.b.o r0 = r0.a(r2, r3)
            r10.k = r0
        L_0x019a:
            a.a.a.m r0 = a.a.a.m.START_ARRAY
            goto L_0x017f
        L_0x019d:
            if (r1 != 0) goto L_0x01ab
            a.a.a.b.o r0 = r10.k
            int r2 = r10.i
            int r3 = r10.j
            a.a.a.b.o r0 = r0.b(r2, r3)
            r10.k = r0
        L_0x01ab:
            a.a.a.m r0 = a.a.a.m.START_OBJECT
            goto L_0x017f
        L_0x01ae:
            java.lang.String r2 = "expected a value"
            r10.b(r0, r2)
        L_0x01b3:
            a.a.a.m r0 = a.a.a.m.VALUE_TRUE
            r10.a(r0)
            a.a.a.m r0 = a.a.a.m.VALUE_TRUE
            goto L_0x017f
        L_0x01bb:
            a.a.a.m r0 = a.a.a.m.VALUE_FALSE
            r10.a(r0)
            a.a.a.m r0 = a.a.a.m.VALUE_FALSE
            goto L_0x017f
        L_0x01c3:
            a.a.a.m r0 = a.a.a.m.VALUE_NULL
            r10.a(r0)
            a.a.a.m r0 = a.a.a.m.VALUE_NULL
            goto L_0x017f
        L_0x01cb:
            a.a.a.m r0 = r10.c(r0)
            goto L_0x017f
        L_0x01d0:
            r10.r = r0
            goto L_0x0025
        L_0x01d4:
            r1 = r3
            goto L_0x0046
        */
        throw new UnsupportedOperationException("Method not decompiled: a.a.a.b.i.i():a.a.a.m");
    }
}
