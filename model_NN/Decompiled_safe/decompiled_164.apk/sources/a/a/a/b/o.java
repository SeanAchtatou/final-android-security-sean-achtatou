package a.a.a.b;

import a.a.a.a.b;
import a.a.a.g;
import a.a.a.k;

public final class o extends k {
    private o c;
    private int d;
    private int e;
    private String f;
    private o g = null;

    public o(o oVar, int i, int i2, int i3) {
        super(i);
        this.c = oVar;
        this.d = i2;
        this.e = i3;
    }

    private void a(int i, int i2, int i3) {
        this.f31a = i;
        this.b = -1;
        this.d = i2;
        this.e = i3;
        this.f = null;
    }

    public final o a(int i, int i2) {
        o oVar = this.g;
        if (oVar == null) {
            o oVar2 = new o(this, 1, i, i2);
            this.g = oVar2;
            return oVar2;
        }
        oVar.a(1, i, i2);
        return oVar;
    }

    public final g a(Object obj) {
        return new g(obj, -1, this.d, this.e, (byte) 0);
    }

    public final String a() {
        return this.f;
    }

    public final void a(String str) {
        this.f = str;
    }

    public final o b(int i, int i2) {
        o oVar = this.g;
        if (oVar == null) {
            o oVar2 = new o(this, 2, i, i2);
            this.g = oVar2;
            return oVar2;
        }
        oVar.a(2, i, i2);
        return oVar;
    }

    public final o g() {
        return this.c;
    }

    public final boolean h() {
        int i = this.b + 1;
        this.b = i;
        return this.f31a != 0 && i > 0;
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder(64);
        switch (this.f31a) {
            case 0:
                sb.append("/");
                break;
            case 1:
                sb.append('[');
                sb.append(f());
                sb.append(']');
                break;
            case 2:
                sb.append('{');
                if (this.f != null) {
                    sb.append('\"');
                    b.a(sb, this.f);
                    sb.append('\"');
                } else {
                    sb.append('?');
                }
                sb.append(']');
                break;
        }
        return sb.toString();
    }
}
