package a.a.a;

public abstract class k {

    /* renamed from: a  reason: collision with root package name */
    protected int f31a;
    protected int b = -1;

    public k(int i) {
        this.f31a = i;
    }

    public final boolean b() {
        return this.f31a == 1;
    }

    public final boolean c() {
        return this.f31a == 0;
    }

    public final boolean d() {
        return this.f31a == 2;
    }

    public final String e() {
        switch (this.f31a) {
            case 0:
                return "ROOT";
            case 1:
                return "ARRAY";
            case 2:
                return "OBJECT";
            default:
                return "?";
        }
    }

    public final int f() {
        if (this.b < 0) {
            return 0;
        }
        return this.b;
    }
}
