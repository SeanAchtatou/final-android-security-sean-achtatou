package a.b.a.a.a;

import java.io.UnsupportedEncodingException;

public final class a {
    public static String a(byte[] bArr, String str) {
        if (bArr == null) {
            return null;
        }
        try {
            return new String(bArr, str);
        } catch (UnsupportedEncodingException e) {
            throw new IllegalStateException(String.valueOf(str) + ": " + e);
        }
    }
}
