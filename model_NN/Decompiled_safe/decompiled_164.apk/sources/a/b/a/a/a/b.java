package a.b.a.a.a;

public final class b {

    /* renamed from: a  reason: collision with root package name */
    private static byte[] f35a = {13, 10};
    private static final byte[] b = {65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 43, 47};
    private static final byte[] c = {65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 45, 95};
    private static final byte[] d;
    private final byte[] e;
    private final int f;
    private final byte[] g;
    private final int h;
    private final int i;
    private byte[] j;
    private int k;
    private int l;
    private int m;
    private int n;
    private boolean o;
    private int p;

    static {
        byte[] bArr = new byte[123];
        bArr[0] = -1;
        bArr[1] = -1;
        bArr[2] = -1;
        bArr[3] = -1;
        bArr[4] = -1;
        bArr[5] = -1;
        bArr[6] = -1;
        bArr[7] = -1;
        bArr[8] = -1;
        bArr[9] = -1;
        bArr[10] = -1;
        bArr[11] = -1;
        bArr[12] = -1;
        bArr[13] = -1;
        bArr[14] = -1;
        bArr[15] = -1;
        bArr[16] = -1;
        bArr[17] = -1;
        bArr[18] = -1;
        bArr[19] = -1;
        bArr[20] = -1;
        bArr[21] = -1;
        bArr[22] = -1;
        bArr[23] = -1;
        bArr[24] = -1;
        bArr[25] = -1;
        bArr[26] = -1;
        bArr[27] = -1;
        bArr[28] = -1;
        bArr[29] = -1;
        bArr[30] = -1;
        bArr[31] = -1;
        bArr[32] = -1;
        bArr[33] = -1;
        bArr[34] = -1;
        bArr[35] = -1;
        bArr[36] = -1;
        bArr[37] = -1;
        bArr[38] = -1;
        bArr[39] = -1;
        bArr[40] = -1;
        bArr[41] = -1;
        bArr[42] = -1;
        bArr[43] = 62;
        bArr[44] = -1;
        bArr[45] = 62;
        bArr[46] = -1;
        bArr[47] = 63;
        bArr[48] = 52;
        bArr[49] = 53;
        bArr[50] = 54;
        bArr[51] = 55;
        bArr[52] = 56;
        bArr[53] = 57;
        bArr[54] = 58;
        bArr[55] = 59;
        bArr[56] = 60;
        bArr[57] = 61;
        bArr[58] = -1;
        bArr[59] = -1;
        bArr[60] = -1;
        bArr[61] = -1;
        bArr[62] = -1;
        bArr[63] = -1;
        bArr[64] = -1;
        bArr[66] = 1;
        bArr[67] = 2;
        bArr[68] = 3;
        bArr[69] = 4;
        bArr[70] = 5;
        bArr[71] = 6;
        bArr[72] = 7;
        bArr[73] = 8;
        bArr[74] = 9;
        bArr[75] = 10;
        bArr[76] = 11;
        bArr[77] = 12;
        bArr[78] = 13;
        bArr[79] = 14;
        bArr[80] = 15;
        bArr[81] = 16;
        bArr[82] = 17;
        bArr[83] = 18;
        bArr[84] = 19;
        bArr[85] = 20;
        bArr[86] = 21;
        bArr[87] = 22;
        bArr[88] = 23;
        bArr[89] = 24;
        bArr[90] = 25;
        bArr[91] = -1;
        bArr[92] = -1;
        bArr[93] = -1;
        bArr[94] = -1;
        bArr[95] = 63;
        bArr[96] = -1;
        bArr[97] = 26;
        bArr[98] = 27;
        bArr[99] = 28;
        bArr[100] = 29;
        bArr[101] = 30;
        bArr[102] = 31;
        bArr[103] = 32;
        bArr[104] = 33;
        bArr[105] = 34;
        bArr[106] = 35;
        bArr[107] = 36;
        bArr[108] = 37;
        bArr[109] = 38;
        bArr[110] = 39;
        bArr[111] = 40;
        bArr[112] = 41;
        bArr[113] = 42;
        bArr[114] = 43;
        bArr[115] = 44;
        bArr[116] = 45;
        bArr[117] = 46;
        bArr[118] = 47;
        bArr[119] = 48;
        bArr[120] = 49;
        bArr[121] = 50;
        bArr[122] = 51;
        d = bArr;
    }

    private b() {
        this(76, f35a);
    }

    private b(int i2, byte[] bArr) {
        byte[] bArr2;
        int i3;
        if (bArr == null) {
            bArr2 = f35a;
            i3 = 0;
        } else {
            bArr2 = bArr;
            i3 = i2;
        }
        this.f = i3 > 0 ? (i3 / 4) * 4 : 0;
        this.g = new byte[bArr2.length];
        System.arraycopy(bArr2, 0, this.g, 0, bArr2.length);
        if (i3 > 0) {
            this.i = bArr2.length + 4;
        } else {
            this.i = 4;
        }
        this.h = this.i - 1;
        if (b(bArr2)) {
            throw new IllegalArgumentException("lineSeperator must not contain base64 characters: [" + a.a(bArr2, "UTF-8") + "]");
        }
        this.e = b;
    }

    private static long a(byte[] bArr, int i2, byte[] bArr2) {
        int i3 = (i2 / 4) * 4;
        long length = (long) ((bArr.length * 4) / 3);
        long j2 = length % 4;
        if (j2 != 0) {
            length += 4 - j2;
        }
        if (i3 <= 0) {
            return length;
        }
        boolean z = length % ((long) i3) == 0;
        long length2 = length + ((length / ((long) i3)) * ((long) bArr2.length));
        return !z ? length2 + ((long) bArr2.length) : length2;
    }

    private void a() {
        if (this.j == null) {
            this.j = new byte[8192];
            this.k = 0;
            this.l = 0;
            return;
        }
        byte[] bArr = new byte[(this.j.length * 2)];
        System.arraycopy(this.j, 0, bArr, 0, this.j.length);
        this.j = bArr;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v3, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v34, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v35, resolved type: byte} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(byte[] r8, int r9, int r10) {
        /*
            r7 = this;
            r4 = 61
            r6 = 0
            boolean r0 = r7.o
            if (r0 == 0) goto L_0x0008
        L_0x0007:
            return
        L_0x0008:
            if (r10 >= 0) goto L_0x00d1
            r0 = 1
            r7.o = r0
            byte[] r0 = r7.j
            if (r0 == 0) goto L_0x001b
            byte[] r0 = r7.j
            int r0 = r0.length
            int r1 = r7.k
            int r0 = r0 - r1
            int r1 = r7.i
            if (r0 >= r1) goto L_0x001e
        L_0x001b:
            r7.a()
        L_0x001e:
            int r0 = r7.n
            switch(r0) {
                case 1: goto L_0x0040;
                case 2: goto L_0x0083;
                default: goto L_0x0023;
            }
        L_0x0023:
            int r0 = r7.f
            if (r0 <= 0) goto L_0x0007
            int r0 = r7.k
            if (r0 <= 0) goto L_0x0007
            byte[] r0 = r7.g
            byte[] r1 = r7.j
            int r2 = r7.k
            byte[] r3 = r7.g
            int r3 = r3.length
            java.lang.System.arraycopy(r0, r6, r1, r2, r3)
            int r0 = r7.k
            byte[] r1 = r7.g
            int r1 = r1.length
            int r0 = r0 + r1
            r7.k = r0
            goto L_0x0007
        L_0x0040:
            byte[] r0 = r7.j
            int r1 = r7.k
            int r2 = r1 + 1
            r7.k = r2
            byte[] r2 = r7.e
            int r3 = r7.p
            int r3 = r3 >> 2
            r3 = r3 & 63
            byte r2 = r2[r3]
            r0[r1] = r2
            byte[] r0 = r7.j
            int r1 = r7.k
            int r2 = r1 + 1
            r7.k = r2
            byte[] r2 = r7.e
            int r3 = r7.p
            int r3 = r3 << 4
            r3 = r3 & 63
            byte r2 = r2[r3]
            r0[r1] = r2
            byte[] r0 = r7.e
            byte[] r1 = a.b.a.a.a.b.b
            if (r0 != r1) goto L_0x0023
            byte[] r0 = r7.j
            int r1 = r7.k
            int r2 = r1 + 1
            r7.k = r2
            r0[r1] = r4
            byte[] r0 = r7.j
            int r1 = r7.k
            int r2 = r1 + 1
            r7.k = r2
            r0[r1] = r4
            goto L_0x0023
        L_0x0083:
            byte[] r0 = r7.j
            int r1 = r7.k
            int r2 = r1 + 1
            r7.k = r2
            byte[] r2 = r7.e
            int r3 = r7.p
            int r3 = r3 >> 10
            r3 = r3 & 63
            byte r2 = r2[r3]
            r0[r1] = r2
            byte[] r0 = r7.j
            int r1 = r7.k
            int r2 = r1 + 1
            r7.k = r2
            byte[] r2 = r7.e
            int r3 = r7.p
            int r3 = r3 >> 4
            r3 = r3 & 63
            byte r2 = r2[r3]
            r0[r1] = r2
            byte[] r0 = r7.j
            int r1 = r7.k
            int r2 = r1 + 1
            r7.k = r2
            byte[] r2 = r7.e
            int r3 = r7.p
            int r3 = r3 << 2
            r3 = r3 & 63
            byte r2 = r2[r3]
            r0[r1] = r2
            byte[] r0 = r7.e
            byte[] r1 = a.b.a.a.a.b.b
            if (r0 != r1) goto L_0x0023
            byte[] r0 = r7.j
            int r1 = r7.k
            int r2 = r1 + 1
            r7.k = r2
            r0[r1] = r4
            goto L_0x0023
        L_0x00d1:
            r0 = r6
            r1 = r9
        L_0x00d3:
            if (r0 >= r10) goto L_0x0007
            byte[] r2 = r7.j
            if (r2 == 0) goto L_0x00e3
            byte[] r2 = r7.j
            int r2 = r2.length
            int r3 = r7.k
            int r2 = r2 - r3
            int r3 = r7.i
            if (r2 >= r3) goto L_0x00e6
        L_0x00e3:
            r7.a()
        L_0x00e6:
            int r2 = r7.n
            int r2 = r2 + 1
            r7.n = r2
            int r2 = r2 % 3
            r7.n = r2
            int r2 = r1 + 1
            byte r1 = r8[r1]
            if (r1 >= 0) goto L_0x00f8
            int r1 = r1 + 256
        L_0x00f8:
            int r3 = r7.p
            int r3 = r3 << 8
            int r1 = r1 + r3
            r7.p = r1
            int r1 = r7.n
            if (r1 != 0) goto L_0x0177
            byte[] r1 = r7.j
            int r3 = r7.k
            int r4 = r3 + 1
            r7.k = r4
            byte[] r4 = r7.e
            int r5 = r7.p
            int r5 = r5 >> 18
            r5 = r5 & 63
            byte r4 = r4[r5]
            r1[r3] = r4
            byte[] r1 = r7.j
            int r3 = r7.k
            int r4 = r3 + 1
            r7.k = r4
            byte[] r4 = r7.e
            int r5 = r7.p
            int r5 = r5 >> 12
            r5 = r5 & 63
            byte r4 = r4[r5]
            r1[r3] = r4
            byte[] r1 = r7.j
            int r3 = r7.k
            int r4 = r3 + 1
            r7.k = r4
            byte[] r4 = r7.e
            int r5 = r7.p
            int r5 = r5 >> 6
            r5 = r5 & 63
            byte r4 = r4[r5]
            r1[r3] = r4
            byte[] r1 = r7.j
            int r3 = r7.k
            int r4 = r3 + 1
            r7.k = r4
            byte[] r4 = r7.e
            int r5 = r7.p
            r5 = r5 & 63
            byte r4 = r4[r5]
            r1[r3] = r4
            int r1 = r7.m
            int r1 = r1 + 4
            r7.m = r1
            int r1 = r7.f
            if (r1 <= 0) goto L_0x0177
            int r1 = r7.f
            int r3 = r7.m
            if (r1 > r3) goto L_0x0177
            byte[] r1 = r7.g
            byte[] r3 = r7.j
            int r4 = r7.k
            byte[] r5 = r7.g
            int r5 = r5.length
            java.lang.System.arraycopy(r1, r6, r3, r4, r5)
            int r1 = r7.k
            byte[] r3 = r7.g
            int r3 = r3.length
            int r1 = r1 + r3
            r7.k = r1
            r7.m = r6
        L_0x0177:
            int r0 = r0 + 1
            r1 = r2
            goto L_0x00d3
        */
        throw new UnsupportedOperationException("Method not decompiled: a.b.a.a.a.b.a(byte[], int, int):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:27:0x009b, code lost:
        if (r0.l >= r0.k) goto L_0x009d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static byte[] a(byte[] r7) {
        /*
            r6 = 0
            r5 = 0
            if (r7 == 0) goto L_0x0007
            int r0 = r7.length
            if (r0 != 0) goto L_0x0009
        L_0x0007:
            r0 = r7
        L_0x0008:
            return r0
        L_0x0009:
            r0 = 76
            byte[] r1 = a.b.a.a.a.b.f35a
            long r0 = a(r7, r0, r1)
            r2 = 2147483647(0x7fffffff, double:1.060997895E-314)
            int r2 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r2 <= 0) goto L_0x003a
            java.lang.IllegalArgumentException r2 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r4 = "Input array too big, the output array would be bigger ("
            r3.<init>(r4)
            java.lang.StringBuilder r0 = r3.append(r0)
            java.lang.String r1 = ") than the specified maxium size of "
            java.lang.StringBuilder r0 = r0.append(r1)
            r1 = 2147483647(0x7fffffff, float:NaN)
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            r2.<init>(r0)
            throw r2
        L_0x003a:
            a.b.a.a.a.b r0 = new a.b.a.a.a.b
            byte[] r1 = a.b.a.a.a.b.f35a
            r0.<init>(r5, r1)
            r0.j = r6
            r0.k = r5
            r0.l = r5
            r0.m = r5
            r0.n = r5
            r0.o = r5
            if (r7 == 0) goto L_0x0052
            int r1 = r7.length
            if (r1 != 0) goto L_0x0054
        L_0x0052:
            r0 = r7
            goto L_0x0008
        L_0x0054:
            int r1 = r0.f
            byte[] r2 = r0.g
            long r1 = a(r7, r1, r2)
            int r1 = (int) r1
            byte[] r1 = new byte[r1]
            int r2 = r1.length
            int r3 = r1.length
            if (r3 != r2) goto L_0x0069
            r0.j = r1
            r0.k = r5
            r0.l = r5
        L_0x0069:
            int r2 = r7.length
            r0.a(r7, r5, r2)
            r2 = -1
            r0.a(r7, r5, r2)
            byte[] r2 = r0.j
            if (r2 == r1) goto L_0x009f
            int r2 = r1.length
            byte[] r3 = r0.j
            if (r3 == 0) goto L_0x009f
            byte[] r3 = r0.j
            if (r3 == 0) goto L_0x00b9
            int r3 = r0.k
            int r4 = r0.l
            int r3 = r3 - r4
        L_0x0083:
            int r2 = java.lang.Math.min(r3, r2)
            byte[] r3 = r0.j
            if (r3 == r1) goto L_0x009d
            byte[] r3 = r0.j
            int r4 = r0.l
            java.lang.System.arraycopy(r3, r4, r1, r5, r2)
            int r3 = r0.l
            int r2 = r2 + r3
            r0.l = r2
            int r2 = r0.l
            int r3 = r0.k
            if (r2 < r3) goto L_0x009f
        L_0x009d:
            r0.j = r6
        L_0x009f:
            byte[] r2 = r0.e
            byte[] r3 = a.b.a.a.a.b.c
            if (r2 != r3) goto L_0x00bb
            r2 = 1
        L_0x00a6:
            if (r2 == 0) goto L_0x00bd
            int r2 = r0.k
            int r3 = r1.length
            if (r2 >= r3) goto L_0x00bd
            int r2 = r0.k
            byte[] r2 = new byte[r2]
            int r0 = r0.k
            java.lang.System.arraycopy(r1, r5, r2, r5, r0)
            r0 = r2
            goto L_0x0008
        L_0x00b9:
            r3 = r5
            goto L_0x0083
        L_0x00bb:
            r2 = r5
            goto L_0x00a6
        L_0x00bd:
            r0 = r1
            goto L_0x0008
        */
        throw new UnsupportedOperationException("Method not decompiled: a.b.a.a.a.b.a(byte[]):byte[]");
    }

    private static boolean b(byte[] bArr) {
        for (int i2 = 0; i2 < bArr.length; i2++) {
            byte b2 = bArr[i2];
            if (b2 == 61 || (b2 >= 0 && b2 < d.length && d[b2] != -1)) {
                return true;
            }
        }
        return false;
    }
}
