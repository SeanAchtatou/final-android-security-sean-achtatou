package a.b.a.a.a;

public final class c {

    /* renamed from: a  reason: collision with root package name */
    private static final char[] f36a = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
    private static final char[] b = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
    private final String c = "UTF-8";

    public static char[] a(byte[] bArr) {
        int i = 0;
        char[] cArr = f36a;
        int length = bArr.length;
        char[] cArr2 = new char[(length << 1)];
        for (int i2 = 0; i2 < length; i2++) {
            int i3 = i + 1;
            cArr2[i] = cArr[(bArr[i2] & 240) >>> 4];
            i = i3 + 1;
            cArr2[i3] = cArr[bArr[i2] & 15];
        }
        return cArr2;
    }

    public final String toString() {
        return String.valueOf(super.toString()) + "[charsetName=" + this.c + "]";
    }
}
