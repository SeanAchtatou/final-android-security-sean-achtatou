package softkos.woolhanks;

import java.util.ArrayList;
import java.util.Timer;

public class Vars {
    static Vars instance = null;
    public int GO_TO_MOBILSOFT = 2;
    public int RESET_LEVELS = 3;
    public int START_GAME = 1;
    AppState appState;
    ArrayList<gButton> buttonList = null;
    int currLevel = 0;
    int screenHeight;
    int screenWidth;
    Timer updateTimer = new Timer();

    public enum AppState {
        Menu,
        About,
        Game
    }

    public void setLevel(int l) {
        this.currLevel = l;
    }

    public int getLevel() {
        return this.currLevel;
    }

    public void nextLevel() {
        if (this.currLevel < Levels.getInstance().getLevelCount() - 1) {
            this.currLevel++;
            Levels.getInstance().initLevel(this.currLevel);
            FileWR.getInstance().saveGame();
        }
    }

    public void setFirstUnsolvedLevel() {
        int i = 0;
        while (i < Levels.getInstance().getLevelCount()) {
            if (Levels.getInstance().isSolved(i) == 0) {
                this.currLevel = i;
                return;
            } else {
                Levels.getInstance().initLevel(this.currLevel);
                i++;
            }
        }
    }

    public void prevLevel() {
        if (this.currLevel > 0) {
            this.currLevel--;
            Levels.getInstance().initLevel(this.currLevel);
            FileWR.getInstance().saveGame();
        }
    }

    public void setAppState(AppState as) {
        this.appState = as;
    }

    public AppState getAppState() {
        return this.appState;
    }

    public void setScreenSize(int w, int h) {
        this.screenWidth = w;
        this.screenHeight = h;
    }

    public int getScreenWidth() {
        return this.screenWidth;
    }

    public int getScreenHeight() {
        return this.screenHeight;
    }

    public Vars() {
        this.updateTimer.schedule(new UpdateTimer(), 10, 50);
        this.buttonList = new ArrayList<>();
        this.buttonList.clear();
    }

    public void addButton(gButton b) {
        this.buttonList.add(b);
    }

    public ArrayList<gButton> getButtonList() {
        return this.buttonList;
    }

    /* Debug info: failed to restart local var, previous not found, register: 1 */
    public gButton getButton(int b) {
        if (b < 0 || b >= this.buttonList.size()) {
            return null;
        }
        return this.buttonList.get(b);
    }

    public static Vars getInstance() {
        if (instance == null) {
            instance = new Vars();
        }
        return instance;
    }
}
