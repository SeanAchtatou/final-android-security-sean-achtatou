package softkos.woolhanks;

import android.app.Activity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import softkos.woolhanks.Vars;

public class MainActivity extends Activity {
    static MainActivity instance = null;
    GameCanvas gameCanvas = null;
    LinearLayout gameLayout;
    MenuCanvas menuCanvas = null;

    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        instance = this;
        requestWindowFeature(1);
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        getWindow().setFlags(1024, 1024);
        Vars.getInstance().setScreenSize(dm.widthPixels, dm.heightPixels);
        Vars.getInstance();
        FileWR.getInstance().loadGame();
        showMenu();
    }

    public void showMenu() {
        if (this.menuCanvas == null) {
            this.menuCanvas = new MenuCanvas(this);
        }
        Vars.getInstance().setAppState(Vars.AppState.Menu);
        setContentView(this.menuCanvas);
    }

    public void showGame() {
        if (this.gameCanvas == null) {
            this.gameLayout = new LinearLayout(this);
            this.gameLayout.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
            this.gameLayout.setOrientation(1);
            this.gameCanvas = new GameCanvas(this);
            AdView adView = new AdView(this, AdSize.BANNER, "a14dddfae42b3b5");
            adView.loadAd(new AdRequest());
            adView.setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
            this.gameCanvas.setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
            this.gameLayout.addView(adView);
            this.gameLayout.addView(this.gameCanvas);
        }
        Vars.getInstance().setFirstUnsolvedLevel();
        Vars.getInstance().setAppState(Vars.AppState.Game);
        setContentView(this.gameLayout);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4 && Vars.getInstance().getAppState() == Vars.AppState.Game) {
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyUp(keyCode, event);
        }
        if (Vars.getInstance().getAppState() == Vars.AppState.Game) {
            showMenu();
        }
        return true;
    }

    public static MainActivity getInstance() {
        return instance;
    }
}
