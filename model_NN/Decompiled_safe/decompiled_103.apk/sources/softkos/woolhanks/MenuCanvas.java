package softkos.woolhanks;

import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.net.Uri;
import android.view.MotionEvent;
import android.view.View;

public class MenuCanvas extends View {
    static MenuCanvas instance = null;
    gButton mobilsoftBtn = null;
    Paint paint = new Paint();
    PaintManager paintMgr;
    gButton resetLevelsBtn = null;
    gButton startGame = null;
    Vars vars;

    public MenuCanvas(Context c) {
        super(c);
        instance = this;
        this.vars = Vars.getInstance();
        this.paintMgr = PaintManager.getInstance();
        initUI();
    }

    public void initUI() {
        this.startGame = new gButton();
        this.startGame.setSize(260, 40);
        this.startGame.setId(Vars.getInstance().START_GAME);
        this.startGame.setText("Start");
        this.startGame.show();
        Vars.getInstance().addButton(this.startGame);
        this.mobilsoftBtn = new gButton();
        this.mobilsoftBtn.setSize(260, 40);
        this.mobilsoftBtn.setId(Vars.getInstance().GO_TO_MOBILSOFT);
        this.mobilsoftBtn.setText("More apps");
        this.mobilsoftBtn.show();
        Vars.getInstance().addButton(this.mobilsoftBtn);
        this.resetLevelsBtn = new gButton();
        this.resetLevelsBtn.setSize(260, 40);
        this.resetLevelsBtn.setId(Vars.getInstance().RESET_LEVELS);
        this.resetLevelsBtn.setText("Untangle Me Extreme");
        this.resetLevelsBtn.show();
        Vars.getInstance().addButton(this.resetLevelsBtn);
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int w, int h, int oldw, int oldh) {
        layoutUI();
    }

    public void layoutUI() {
        this.resetLevelsBtn.setPosition((getWidth() / 2) - (this.startGame.getWidth() / 2), getHeight() - ((int) (((double) this.startGame.getHeight()) * 1.05d)));
        this.mobilsoftBtn.setPosition((getWidth() / 2) - (this.startGame.getWidth() / 2), getHeight() - ((int) (((double) this.startGame.getHeight()) * 2.1d)));
        this.startGame.setPosition((getWidth() / 2) - (this.startGame.getWidth() / 2), getHeight() - ((int) (((double) this.startGame.getHeight()) * 3.15d)));
    }

    public void mouseDown(int x, int y) {
        int pressed = 0;
        for (int i = 0; i < Vars.getInstance().getButtonList().size(); i++) {
            pressed |= Vars.getInstance().getButton(i).mouseDown(x, y);
            if (pressed != 0) {
                invalidate();
                return;
            }
        }
    }

    public void mouseUp(int x, int y) {
        int pressed = 0;
        for (int i = 0; i < Vars.getInstance().getButtonList().size(); i++) {
            pressed |= Vars.getInstance().getButton(i).mouseUp(x, y);
            if (pressed != 0) {
                handleCommand(Vars.getInstance().getButton(i).getId());
                invalidate();
                return;
            }
        }
    }

    public void mouseDrag(int x, int y) {
        int pressed = 0;
        for (int i = 0; i < Vars.getInstance().getButtonList().size(); i++) {
            pressed |= Vars.getInstance().getButton(i).mouseDrag(x, y);
            if (pressed != 0) {
                invalidate();
                return;
            }
        }
    }

    public void handleCommand(int id) {
        if (id == Vars.getInstance().START_GAME) {
            MainActivity.getInstance().showGame();
        } else if (id == Vars.getInstance().GO_TO_MOBILSOFT) {
            MainActivity.getInstance().startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://www.mobilsoft.pl")));
        } else if (id == Vars.getInstance().RESET_LEVELS) {
            MainActivity.getInstance().startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=softkos.untanglemeextreme")));
        }
    }

    public boolean onTouchEvent(MotionEvent event) {
        int x = (int) event.getX();
        int y = (int) event.getY();
        if (event.getAction() == 0) {
            mouseDown(x, y);
        } else if (2 == event.getAction()) {
            mouseDrag(x, y);
        } else if (1 == event.getAction()) {
            mouseUp(x, y);
        }
        invalidate();
        return true;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        this.paint.setAntiAlias(true);
        this.paintMgr.drawImage(canvas, "gameback", 0, 0, getWidth(), getHeight());
        this.paintMgr.drawImage(canvas, "menuimage", 0, 0, getWidth(), getWidth());
        Canvas canvas2 = canvas;
        this.paintMgr.drawImage(canvas2, "instructions", 0, (int) (((double) getWidth()) * 0.6d), getWidth(), getWidth() / 2);
        for (int i = 0; i < Vars.getInstance().getButtonList().size(); i++) {
            Vars.getInstance().getButton(i).draw(canvas, this.paint);
        }
    }

    public static MenuCanvas getInstance() {
        return instance;
    }
}
