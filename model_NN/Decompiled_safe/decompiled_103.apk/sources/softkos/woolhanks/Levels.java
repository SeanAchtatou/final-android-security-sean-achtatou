package softkos.woolhanks;

import android.graphics.Point;
import java.util.ArrayList;

public class Levels {
    static Levels instance = null;
    int currLevel = 0;
    ArrayList<Line> lineList = new ArrayList<>();
    ArrayList<Point> pointList = new ArrayList<>();
    int screenH = 0;
    int screenW = 0;
    int[] solved_levels = new int[27];

    public void setScreenSize(int w, int h) {
        this.screenW = w;
        this.screenH = h;
    }

    public int getLevelCount() {
        return this.solved_levels.length;
    }

    public Point getPoint(int index) {
        return this.pointList.get(index);
    }

    public int getPointCount() {
        return this.pointList.size();
    }

    public void addPoint(Point p) {
        this.pointList.add(p);
    }

    public boolean isLevelSolved() {
        if (getLinesCount() == 0) {
            return false;
        }
        for (int i = 0; i < getLinesCount(); i++) {
            if (getLine(i).isIntersected()) {
                return false;
            }
        }
        this.solved_levels[this.currLevel] = 1;
        return true;
    }

    public int isSolved(int i) {
        return this.solved_levels[i];
    }

    public void setSolved(int i) {
        this.solved_levels[i] = 1;
    }

    public int getSolvedLevel(int lev) {
        return this.solved_levels[lev];
    }

    public void addLine(Line l) {
        this.lineList.add(l);
    }

    public int getLinesCount() {
        return this.lineList.size();
    }

    public void resetSolved(int i) {
        this.solved_levels[i] = 0;
    }

    public Line getLine(int index) {
        return this.lineList.get(index);
    }

    public double dist(double x1, double y1, double x2, double y2) {
        return Math.sqrt(((x1 - x2) * (x1 - x2)) + ((y1 - y2) * (y1 - y2)));
    }

    public void checkIntersection() {
        for (int i = 0; i < getLinesCount(); i++) {
            getLine(i).setIntersected(false);
            double x1 = (double) getPoint(getLine(i).start_index).x;
            double y1 = (double) getPoint(getLine(i).start_index).y;
            double x2 = (double) getPoint(getLine(i).end_index).x;
            double y2 = (double) getPoint(getLine(i).end_index).y;
            for (int k = 0; k < getLinesCount(); k++) {
                if (k != i) {
                    double u1 = (double) getPoint(getLine(k).start_index).x;
                    double v1 = (double) getPoint(getLine(k).start_index).y;
                    double u2 = (double) getPoint(getLine(k).end_index).x;
                    double v2 = (double) getPoint(getLine(k).end_index).y;
                    double b1 = (y2 - y1) / (x2 - x1);
                    double b2 = (v2 - v1) / (u2 - u1);
                    double a1 = y1 - (b1 * x1);
                    double xi = (-(a1 - (v1 - (b2 * u1)))) / (b1 - b2);
                    double yi = a1 + (b1 * xi);
                    if (dist(xi, yi, x1, y1) > 1.0d && dist(xi, yi, x2, y2) > 1.0d && dist(xi, yi, u1, v1) > 1.0d && dist(xi, yi, u2, v2) > 1.0d && (x1 - xi) * (xi - x2) >= 0.0d && (u1 - xi) * (xi - u2) >= 0.0d && (y1 - yi) * (yi - y2) >= 0.0d && (v1 - yi) * (yi - v2) >= 0.0d) {
                        getLine(i).setIntersected(true);
                    }
                }
            }
        }
    }

    public void initLevel(int lev) {
        this.pointList.clear();
        this.lineList.clear();
        if (this.screenW == 0) {
            this.screenW = 320;
            this.screenH = 480;
        }
        this.currLevel = lev;
        if (lev == 0) {
            Point point = new Point(this.screenW / 4, this.screenH / 4);
            Point point2 = new Point(this.screenW / 4, this.screenH / 2);
            Point point3 = new Point((this.screenW / 4) + (this.screenW / 2), this.screenH / 4);
            Point point4 = new Point((this.screenW / 4) + (this.screenW / 2), this.screenH / 2);
            addPoint(point);
            addPoint(point2);
            addPoint(point3);
            addPoint(point4);
            addLine(new Line(0, 1));
            addLine(new Line(1, 3));
            addLine(new Line(2, 3));
            addLine(new Line(1, 2));
            addLine(new Line(0, 3));
        }
        if (lev == 1 || lev == 2) {
            Point[] p = {new Point(this.screenW / 2, this.screenH / 4), new Point(this.screenW / 5, this.screenH / 3), new Point((int) (((double) this.screenW) * 0.3d), (int) (((double) this.screenH) / 1.5d)), new Point((int) (((double) this.screenW) * 0.7d), (int) (((double) this.screenH) / 1.5d)), new Point((int) (((double) this.screenW) * 0.8d), this.screenH / 3)};
            if (lev == 1) {
                addLevelData(p, new Line[]{new Line(0, 1), new Line(1, 2), new Line(2, 3), new Line(3, 4), new Line(4, 0), new Line(0, 2), new Line(0, 3), new Line(1, 3), new Line(2, 4)});
            }
            if (lev == 2) {
                addLevelData(p, new Line[]{new Line(1, 4), new Line(1, 2), new Line(3, 4), new Line(0, 2), new Line(0, 3), new Line(1, 3), new Line(2, 4), new Line(0, 1), new Line(0, 4)});
            }
        }
        if (lev == 3) {
            Point[] p2 = {new Point(this.screenW / 2, this.screenH / 4), new Point((int) (((double) this.screenW) * 0.2d), (this.screenH / 3) + 1), new Point(((int) (((double) this.screenW) * 0.2d)) + 1, ((int) (((double) this.screenH) / 1.5d)) + 1), new Point((this.screenW / 2) + 1, (int) (((double) this.screenH) * 0.7d)), new Point(((int) (((double) this.screenW) * 0.8d)) - 1, (int) (((double) this.screenH) / 1.5d)), new Point((int) (((double) this.screenW) * 0.8d), this.screenH / 3)};
            if (lev == 3) {
                addLevelData(p2, new Line[]{new Line(0, 1), new Line(2, 3), new Line(3, 4), new Line(4, 5), new Line(5, 0), new Line(0, 2), new Line(0, 4), new Line(1, 3), new Line(1, 4), new Line(1, 5), new Line(2, 5)});
            }
        }
        if (lev == 4 || lev == 5) {
            Point[] p3 = {new Point(this.screenW / 2, this.screenH / 5), new Point((int) (((double) this.screenW) * 0.2d), this.screenH / 4), new Point(((int) (((double) this.screenW) * 0.2d)) + 1, this.screenH / 2), new Point(this.screenW / 3, (int) (((double) this.screenH) * 0.7d)), new Point((this.screenW * 2) / 3, (int) (((double) this.screenH) * 0.7d)), new Point(((int) (((double) this.screenW) * 0.8d)) - 1, this.screenH / 2), new Point((int) (((double) this.screenW) * 0.8d), this.screenH / 4)};
            if (lev == 4) {
                addLevelData(p3, new Line[]{new Line(0, 1), new Line(2, 3), new Line(3, 4), new Line(4, 5), new Line(5, 6), new Line(0, 6), new Line(1, 6), new Line(2, 6), new Line(3, 5), new Line(0, 3), new Line(0, 4), new Line(0, 5)});
            }
            if (lev == 5) {
                addLevelData(p3, new Line[]{new Line(0, 1), new Line(2, 3), new Line(3, 4), new Line(4, 5), new Line(5, 6), new Line(0, 6), new Line(1, 6), new Line(2, 6), new Line(3, 5), new Line(0, 3), new Line(0, 4), new Line(0, 5), new Line(1, 3)});
            }
        }
        if (lev == 6 || lev == 7) {
            int xw = this.screenW / (lev - 1);
            int y1 = this.screenH / 3;
            int y2 = (this.screenH * 2) / 3;
            Point[] p4 = new Point[((lev - 2) * 2)];
            int index = 0;
            for (int x = 0; x < lev - 2; x++) {
                int index2 = index + 1;
                p4[index] = new Point((xw * x) + xw, y1);
                index = index2 + 1;
                p4[index2] = new Point((xw * x) + xw, y2);
            }
            if (lev == 6) {
                addLevelData(p4, new Line[]{new Line(0, 1), new Line(1, 2), new Line(0, 3), new Line(2, 4), new Line(3, 5), new Line(3, 4), new Line(2, 5), new Line(4, 7), new Line(5, 6), new Line(6, 7)});
            }
            if (lev == 7) {
                addLevelData(p4, new Line[]{new Line(0, 1), new Line(1, 2), new Line(0, 3), new Line(2, 4), new Line(3, 5), new Line(3, 4), new Line(2, 5), new Line(4, 7), new Line(5, 6), new Line(4, 6), new Line(5, 7), new Line(6, 9), new Line(7, 8), new Line(8, 9)});
            }
        }
        if (lev == 8 || lev == 9) {
            int yw = this.screenH / (lev - 1);
            int x1 = this.screenW / 3;
            int x2 = (this.screenW * 2) / 3;
            Point[] p5 = new Point[((lev - 2) * 2)];
            int index3 = 0;
            for (int y = 0; y < lev - 2; y++) {
                int index4 = index3 + 1;
                p5[index3] = new Point(x1, (yw * y) + yw);
                index3 = index4 + 1;
                p5[index4] = new Point(x2, (yw * y) + yw);
            }
            if (lev == 8) {
                addLevelData(p5, new Line[]{new Line(0, 1), new Line(1, 2), new Line(0, 3), new Line(2, 4), new Line(3, 5), new Line(3, 4), new Line(2, 5), new Line(4, 7), new Line(5, 6), new Line(6, 9), new Line(7, 8), new Line(6, 8), new Line(7, 9), new Line(8, 11), new Line(9, 10), new Line(10, 11)});
            }
            if (lev == 9) {
                addLevelData(p5, new Line[]{new Line(0, 1), new Line(1, 2), new Line(2, 3), new Line(1, 3), new Line(2, 6), new Line(4, 5), new Line(0, 5), new Line(6, 10), new Line(4, 7), new Line(5, 12), new Line(9, 10), new Line(8, 9), new Line(12, 13)});
            }
        }
        if (lev == 10) {
            addLevelData(getPoints(0, 6), new Line[]{new Line(0, 2), new Line(1, 4), new Line(1, 3), new Line(2, 3), new Line(2, 4), new Line(2, 5), new Line(3, 4), new Line(5, 0)});
        }
        if (lev == 11) {
            addLevelData(getPoints(0, 7), new Line[]{new Line(0, 2), new Line(1, 4), new Line(1, 3), new Line(2, 3), new Line(2, 4), new Line(2, 5), new Line(3, 4), new Line(5, 0), new Line(6, 0), new Line(6, 3)});
        }
        if (lev == 12) {
            addLevelData(getPoints(0, 8), new Line[]{new Line(0, 2), new Line(1, 4), new Line(1, 3), new Line(2, 3), new Line(2, 4), new Line(2, 5), new Line(3, 4), new Line(5, 0), new Line(6, 2), new Line(6, 1), new Line(7, 0), new Line(7, 6)});
        }
        if (lev == 13) {
            addLevelData(getPoints(0, 10), new Line[]{new Line(0, 1), new Line(0, 4), new Line(1, 4), new Line(2, 5), new Line(2, 7), new Line(3, 5), new Line(3, 6), new Line(3, 9), new Line(4, 5), new Line(5, 9), new Line(6, 9), new Line(7, 8), new Line(9, 0)});
        }
        if (lev == 14) {
            addLevelData(getPoints(0, 11), new Line[]{new Line(0, 1), new Line(0, 4), new Line(1, 4), new Line(2, 5), new Line(2, 7), new Line(3, 5), new Line(3, 6), new Line(3, 9), new Line(4, 5), new Line(5, 9), new Line(6, 9), new Line(7, 8), new Line(9, 0), new Line(10, 9), new Line(10, 7)});
        }
        if (lev == 15) {
            addLevelData(getPoints(0, 12), new Line[]{new Line(0, 1), new Line(0, 4), new Line(1, 4), new Line(2, 5), new Line(2, 7), new Line(3, 5), new Line(3, 6), new Line(3, 9), new Line(4, 5), new Line(5, 9), new Line(6, 9), new Line(7, 8), new Line(9, 0), new Line(11, 10), new Line(10, 4), new Line(11, 3), new Line(10, 6)});
        }
        if (lev == 16) {
            addLevelData(getPoints(0, 13), new Line[]{new Line(0, 1), new Line(0, 4), new Line(1, 4), new Line(2, 5), new Line(2, 7), new Line(3, 5), new Line(3, 6), new Line(3, 9), new Line(4, 5), new Line(5, 9), new Line(6, 9), new Line(7, 8), new Line(9, 0), new Line(10, 9), new Line(10, 0), new Line(11, 7), new Line(11, 5), new Line(12, 0), new Line(12, 2)});
        }
        if (lev == 17) {
            addLevelData(getPoints(0, 14), new Line[]{new Line(0, 1), new Line(0, 4), new Line(1, 4), new Line(2, 5), new Line(2, 7), new Line(3, 5), new Line(3, 6), new Line(3, 9), new Line(4, 5), new Line(5, 9), new Line(6, 9), new Line(7, 8), new Line(9, 0), new Line(10, 5), new Line(10, 8), new Line(11, 9), new Line(11, 0), new Line(12, 0), new Line(12, 3), new Line(12, 5), new Line(13, 7), new Line(13, 4)});
        }
        if (lev == 18) {
            addLevelData(getPoints(1, 12), new Line[]{new Line(0, 1), new Line(0, 4), new Line(1, 4), new Line(2, 5), new Line(2, 7), new Line(3, 5), new Line(3, 6), new Line(3, 9), new Line(4, 5), new Line(5, 9), new Line(6, 9), new Line(7, 8), new Line(9, 0), new Line(10, 5), new Line(10, 8), new Line(11, 9), new Line(11, 0)});
        }
        if (lev == 19) {
            addLevelData(getPoints(0, 15), new Line[]{new Line(0, 1), new Line(0, 4), new Line(1, 4), new Line(2, 5), new Line(2, 7), new Line(3, 5), new Line(3, 6), new Line(3, 9), new Line(5, 9), new Line(6, 9), new Line(7, 8), new Line(9, 0), new Line(10, 5), new Line(10, 8), new Line(11, 9), new Line(11, 0), new Line(12, 0), new Line(12, 3), new Line(12, 5), new Line(13, 7), new Line(13, 4), new Line(0, 14), new Line(3, 14)});
        }
        if (lev == 20) {
            addLevelData(getPoints(1, 16), new Line[]{new Line(0, 1), new Line(0, 4), new Line(1, 4), new Line(2, 5), new Line(2, 7), new Line(3, 5), new Line(3, 6), new Line(3, 9), new Line(5, 9), new Line(6, 9), new Line(7, 8), new Line(9, 0), new Line(10, 5), new Line(10, 8), new Line(11, 9), new Line(11, 0), new Line(12, 0), new Line(12, 3), new Line(12, 5), new Line(13, 7), new Line(13, 4), new Line(0, 14), new Line(3, 14), new Line(14, 15), new Line(11, 15)});
        }
        if (lev == 21) {
            addLevelData(getPoints(0, 16), new Line[]{new Line(0, 1), new Line(0, 4), new Line(1, 4), new Line(2, 5), new Line(2, 7), new Line(3, 5), new Line(3, 6), new Line(3, 9), new Line(5, 9), new Line(6, 9), new Line(7, 8), new Line(9, 0), new Line(10, 5), new Line(10, 8), new Line(11, 9), new Line(11, 0), new Line(12, 0), new Line(12, 3), new Line(12, 5), new Line(13, 7), new Line(13, 4), new Line(0, 14), new Line(3, 14), new Line(0, 15), new Line(14, 15), new Line(14, 0)});
        }
        if (lev == 22) {
            addLevelData(getPoints(0, 15), new Line[]{new Line(0, 2), new Line(0, 3), new Line(0, 5), new Line(0, 7), new Line(1, 3), new Line(1, 4), new Line(1, 10), new Line(2, 9), new Line(2, 12), new Line(3, 7), new Line(3, 10), new Line(4, 14), new Line(5, 8), new Line(5, 9), new Line(5, 10), new Line(6, 7), new Line(6, 12), new Line(7, 12), new Line(8, 13), new Line(8, 14), new Line(9, 11), new Line(9, 13), new Line(10, 14), new Line(11, 13)});
        }
        if (lev == 23) {
            addLevelData(getPoints(0, 15), new Line[]{new Line(0, 2), new Line(0, 5), new Line(0, 6), new Line(1, 2), new Line(1, 4), new Line(1, 4), new Line(1, 9), new Line(1, 11), new Line(2, 9), new Line(3, 4), new Line(3, 13), new Line(4, 11), new Line(4, 13), new Line(5, 8), new Line(5, 11), new Line(6, 8), new Line(7, 10), new Line(8, 11), new Line(9, 10), new Line(9, 12), new Line(10, 12), new Line(12, 13), new Line(12, 14), new Line(13, 14)});
        }
        if (lev == 24) {
            addLevelData(getPoints(1, 16), new Line[]{new Line(0, 2), new Line(0, 3), new Line(0, 5), new Line(0, 7), new Line(1, 3), new Line(1, 4), new Line(1, 10), new Line(2, 9), new Line(2, 12), new Line(3, 7), new Line(3, 10), new Line(4, 14), new Line(5, 8), new Line(5, 9), new Line(5, 10), new Line(6, 7), new Line(6, 12), new Line(7, 12), new Line(8, 13), new Line(8, 14), new Line(9, 11), new Line(9, 13), new Line(10, 14), new Line(11, 13), new Line(15, 0), new Line(15, 2)});
        }
        if (lev == 25) {
            addLevelData(getPoints(0, 19), new Line[]{new Line(0, 2), new Line(0, 7), new Line(0, 9), new Line(1, 2), new Line(1, 11), new Line(1, 5), new Line(1, 12), new Line(2, 5), new Line(2, 7), new Line(2, 16), new Line(3, 4), new Line(3, 12), new Line(3, 18), new Line(5, 0), new Line(5, 1), new Line(6, 10), new Line(6, 12), new Line(7, 8), new Line(7, 16), new Line(8, 10), new Line(8, 15), new Line(9, 16), new Line(10, 2), new Line(11, 13), new Line(11, 14), new Line(12, 13), new Line(13, 11), new Line(14, 17), new Line(16, 17)});
        }
        if (lev == 26) {
            addLevelData(getPoints(0, 21), new Line[]{new Line(0, 2), new Line(0, 7), new Line(0, 9), new Line(1, 2), new Line(1, 11), new Line(1, 5), new Line(1, 12), new Line(2, 5), new Line(2, 7), new Line(2, 16), new Line(3, 4), new Line(3, 12), new Line(3, 18), new Line(5, 19), new Line(5, 20), new Line(6, 10), new Line(6, 12), new Line(7, 8), new Line(7, 16), new Line(8, 10), new Line(8, 15), new Line(9, 16), new Line(10, 19), new Line(11, 13), new Line(11, 14), new Line(12, 19), new Line(13, 11), new Line(14, 17), new Line(16, 17)});
        }
        checkIntersection();
    }

    public Point[] getPoints(int type, int count) {
        Point[] p = new Point[count];
        if (type == 0) {
            int angle = 360 / count;
            int curr_angle = 0;
            int r = this.screenW / 3;
            int ry = this.screenH / 3;
            int xc = this.screenW / 2;
            int yc = this.screenH / 2;
            for (int i = 0; i < count; i++) {
                double rad = (((double) curr_angle) * 3.141592653589793d) / 180.0d;
                p[i] = new Point(((int) (((double) r) * Math.cos(rad))) + xc, ((int) (((double) ry) * Math.sin(rad))) + yc);
                curr_angle += angle;
            }
        }
        if (type == 1) {
            int columns = (int) Math.sqrt((double) count);
            int xspacing = this.screenW / (columns + 1);
            int xstart = xspacing;
            int ystart = xspacing;
            for (int i2 = 0; i2 < count; i2++) {
                p[i2] = new Point(xstart + ((i2 % columns) * xspacing), ystart + ((i2 / columns) * xspacing));
            }
        }
        return p;
    }

    public void addLevelData(Point[] p, Line[] l) {
        for (Point addPoint : p) {
            addPoint(addPoint);
        }
        for (Line addLine : l) {
            addLine(addLine);
        }
    }

    public static Levels getInstance() {
        if (instance == null) {
            instance = new Levels();
            instance.initLevel(0);
        }
        return instance;
    }
}
