package softkos.woolhanks;

import java.util.TimerTask;

public class UpdateTimer extends TimerTask {
    public void run() {
        if (GameCanvas.getInstance() != null) {
            GameCanvas.getInstance().postInvalidate();
        }
    }
}
