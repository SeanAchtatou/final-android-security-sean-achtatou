package softkos.woolhanks;

import android.graphics.drawable.Drawable;

public class ImageLoader {
    static ImageLoader instance = null;
    String[] image_names = {"gameback", "button_off", "button_on", "bug", "menuimage", "solved", "arrow_right", "arrow_left", "instructions"};
    Drawable[] images;
    int[] img_id = {R.drawable.gameback, R.drawable.button_off, R.drawable.button_on, R.drawable.bug, R.drawable.menuimage, R.drawable.solved, R.drawable.arrow_right, R.drawable.arrow_left, R.drawable.instructions};

    public static ImageLoader getInstance() {
        if (instance == null) {
            instance = new ImageLoader();
        }
        return instance;
    }

    public ImageLoader() {
        LoadImages();
    }

    public void LoadImages() {
        int len = this.img_id.length;
        this.images = new Drawable[this.image_names.length];
        for (int i = 0; i < len; i++) {
            this.images[i] = MainActivity.getInstance().getResources().getDrawable(this.img_id[i]);
        }
    }

    public Drawable getImage(String img) {
        for (int i = 0; i < this.image_names.length; i++) {
            if (img.equals(this.image_names[i])) {
                return this.images[i];
            }
        }
        return null;
    }
}
