package softkos.woolhanks;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.view.MotionEvent;
import android.view.View;

public class GameCanvas extends View {
    static GameCanvas instance = null;
    int bugDragged = -1;
    int bug_size = 35;
    Paint paint = new Paint();
    PaintManager paintMgr;
    Vars vars;

    public GameCanvas(Context c) {
        super(c);
        instance = this;
        this.vars = Vars.getInstance();
        this.paintMgr = PaintManager.getInstance();
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        Levels.getInstance().setScreenSize(getWidth(), getHeight());
    }

    public void mouseDown(int x, int y) {
        Levels lev = Levels.getInstance();
        int i = 0;
        while (i < lev.getPointCount()) {
            Point p = lev.getPoint(i);
            if (p.x <= x - (this.bug_size / 2) || p.x >= (this.bug_size / 2) + x || p.y <= y - (this.bug_size / 2) || p.y >= (this.bug_size / 2) + y) {
                i++;
            } else {
                this.bugDragged = i;
                return;
            }
        }
    }

    public void mouseUp(int x, int y) {
        this.bugDragged = -1;
        Levels.getInstance().checkIntersection();
        Levels.getInstance().isLevelSolved();
        int w = ImageLoader.getInstance().getImage("arrow_left").getMinimumWidth();
        int h = ImageLoader.getInstance().getImage("arrow_left").getMinimumHeight();
        if (x > 0 && x < w && y > getHeight() - h) {
            this.vars.prevLevel();
        }
        if (x > getWidth() - w && y > getHeight() - h) {
            this.vars.nextLevel();
        }
    }

    public void mouseDrag(int x, int y) {
        if (this.bugDragged > -1) {
            Levels.getInstance().getPoint(this.bugDragged).x = x;
            Levels.getInstance().getPoint(this.bugDragged).y = y;
        }
        Levels.getInstance().checkIntersection();
        invalidate();
    }

    public boolean onTouchEvent(MotionEvent event) {
        int x = (int) event.getX();
        int y = (int) event.getY();
        if (event.getAction() == 0) {
            mouseDown(x, y);
        } else if (2 == event.getAction()) {
            mouseDrag(x, y);
        } else if (1 == event.getAction()) {
            mouseUp(x, y);
        }
        invalidate();
        return true;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        this.paint.setAntiAlias(true);
        this.paintMgr.drawImage(canvas, "gameback", 0, 0, getWidth(), getHeight());
        Levels lev = Levels.getInstance();
        for (int i = 0; i < lev.getLinesCount(); i++) {
            Point p1 = lev.getPoint(lev.getLine(i).start_index);
            Point p2 = lev.getPoint(lev.getLine(i).end_index);
            this.paintMgr.setLineWidth(3);
            this.paintMgr.setColor(Color.argb(100, 50, 200, 50));
            if (lev.getLine(i).isIntersected()) {
                this.paintMgr.setColor(Color.argb(100, 200, 50, 50));
            }
            this.paintMgr.drawLine(canvas, p1.x, p1.y, p2.x, p2.y);
            this.paintMgr.setLineWidth(1);
            this.paintMgr.setColor(-16711936);
            if (lev.getLine(i).isIntersected()) {
                this.paintMgr.setColor(-65536);
            }
            this.paintMgr.drawLine(canvas, p1.x, p1.y, p2.x, p2.y);
        }
        for (int i2 = 0; i2 < lev.getPointCount(); i2++) {
            this.paintMgr.drawImage(canvas, "bug", lev.getPoint(i2).x - (this.bug_size / 2), lev.getPoint(i2).y - (this.bug_size / 2), this.bug_size, this.bug_size);
        }
        if (Levels.getInstance().getSolvedLevel(this.vars.getLevel()) > 0) {
            this.paintMgr.drawImage(canvas, "solved", 0, getHeight() / 4, getWidth(), getWidth() / 2);
        }
        int w = ImageLoader.getInstance().getImage("arrow_left").getMinimumWidth();
        int h = ImageLoader.getInstance().getImage("arrow_left").getMinimumHeight();
        this.paintMgr.drawImage(canvas, "arrow_left", 0, getHeight() - h, w, h);
        this.paintMgr.drawImage(canvas, "arrow_right", getWidth() - w, getHeight() - h, w, h);
        this.paintMgr.setColor(-16777216);
        this.paintMgr.setTextSize(22);
        this.paintMgr.drawString(canvas, "Level: " + (this.vars.getLevel() + 1), 0, getHeight() - h, getWidth(), h, PaintManager.STR_CENTER);
    }

    public static GameCanvas getInstance() {
        return instance;
    }
}
