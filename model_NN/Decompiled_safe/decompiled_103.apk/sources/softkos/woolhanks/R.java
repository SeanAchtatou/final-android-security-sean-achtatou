package softkos.woolhanks;

public final class R {

    public static final class attr {
    }

    public static final class drawable {
        public static final int arrow_left = 2130837504;
        public static final int arrow_right = 2130837505;
        public static final int bug = 2130837506;
        public static final int button_off = 2130837507;
        public static final int button_on = 2130837508;
        public static final int gameback = 2130837509;
        public static final int icon = 2130837510;
        public static final int instructions = 2130837511;
        public static final int menuimage = 2130837512;
        public static final int solved = 2130837513;
    }

    public static final class layout {
        public static final int main = 2130903040;
    }

    public static final class string {
        public static final int app_name = 2130968576;
    }
}
