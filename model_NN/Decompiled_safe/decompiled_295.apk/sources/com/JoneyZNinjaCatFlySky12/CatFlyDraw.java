package com.JoneyZNinjaCatFlySky12;

import android.graphics.Bitmap;
import java.util.Date;

public class CatFlyDraw {
    protected Bitmap[] bitmaps;
    protected int catFlyTime = 0;
    protected int catFlyType = 0;
    private int cat_up_or_down = 0;
    protected long duration;
    protected Long lastBitmapTime;
    protected boolean repeat;
    protected int step;
    protected float x;
    protected float y;

    public CatFlyDraw(float x2, float y2, Bitmap bitmap, long duration2) {
        this.x = x2;
        this.y = y2;
        this.bitmaps = new Bitmap[]{bitmap};
        this.duration = duration2;
        this.repeat = true;
        this.lastBitmapTime = null;
        this.step = 0;
        this.cat_up_or_down = 0;
        this.catFlyTime = 0;
    }

    public CatFlyDraw(float x2, float y2, Bitmap[] bitmaps2, long duration2, boolean repeat2, int myFlyType) {
        this.x = x2;
        this.y = y2;
        this.bitmaps = bitmaps2;
        this.duration = duration2;
        this.repeat = repeat2;
        this.lastBitmapTime = null;
        this.step = 0;
        this.catFlyType = myFlyType;
        this.cat_up_or_down = 0;
        this.catFlyTime = 0;
    }

    public Bitmap nextFrame() {
        if (this.step >= this.bitmaps.length) {
            if (!this.repeat) {
                return null;
            }
            this.lastBitmapTime = null;
        }
        if (this.lastBitmapTime == null) {
            this.lastBitmapTime = Long.valueOf(new Date().getTime());
            Bitmap[] bitmapArr = this.bitmaps;
            this.step = 0;
            return bitmapArr[0];
        }
        long nowTime = System.currentTimeMillis();
        if (nowTime - this.lastBitmapTime.longValue() <= this.duration) {
            return this.bitmaps[this.step];
        }
        this.lastBitmapTime = Long.valueOf(nowTime);
        this.catFlyTime++;
        if (this.catFlyType == 1) {
            if (this.catFlyTime <= 8) {
                this.y += 6.0f;
            } else if (this.catFlyTime > 11 && this.catFlyTime <= 18) {
                this.y -= 6.0f;
            } else if ((this.catFlyTime <= 8 || this.catFlyTime > 10) && this.catFlyTime == 19) {
                return null;
            }
        } else if (this.cat_up_or_down == 0) {
            this.y -= 12.0f;
            if (this.y < 20.0f) {
                this.cat_up_or_down = 1;
            }
        } else {
            this.y += 8.0f;
            if (this.y > 190.0f) {
                return null;
            }
        }
        Bitmap[] bitmapArr2 = this.bitmaps;
        int i = this.step;
        this.step = i + 1;
        return bitmapArr2[i];
    }

    public Bitmap nextFrameDontMove() {
        if (this.step >= this.bitmaps.length) {
            if (!this.repeat) {
                return null;
            }
            this.lastBitmapTime = null;
        }
        if (this.lastBitmapTime == null) {
            this.lastBitmapTime = Long.valueOf(new Date().getTime());
            Bitmap[] bitmapArr = this.bitmaps;
            this.step = 0;
            return bitmapArr[0];
        }
        long nowTime = System.currentTimeMillis();
        if (nowTime - this.lastBitmapTime.longValue() <= this.duration) {
            return this.bitmaps[this.step];
        }
        this.lastBitmapTime = Long.valueOf(nowTime);
        Bitmap[] bitmapArr2 = this.bitmaps;
        int i = this.step;
        this.step = i + 1;
        return bitmapArr2[i];
    }

    public float getX() {
        return this.x;
    }

    public float getY() {
        return this.y;
    }
}
