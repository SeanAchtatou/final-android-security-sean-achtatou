package com.JoneyZNinjaCatFlySky12;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.SurfaceHolder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class DrawRunning implements Runnable {
    public static int CAT_TYPE_CAT_CHUGOU = 6;
    public static int CAT_TYPE_CAT_FALL = 3;
    public static int CAT_TYPE_CAT_HUAXING = 4;
    public static int CAT_TYPE_CAT_JUMP = 2;
    public static int CAT_TYPE_CAT_ROTE = 5;
    public static int CAT_TYPE_CAT_RUN = 1;
    MediaPlayer BigSound;
    private List<AnimationDraw> animationDraws;
    public String[] arr;
    private Bitmap background;
    private Bitmap[] background_cat;
    private Bitmap[] banner;
    private List<AnimationDraw> buffers;
    public boolean canDoDraw = true;
    public int catBg_type = 1;
    public Bitmap[] catFall;
    private List<CatFallDraw> catFallAnimationDraws;
    private List<CatFallDraw> catFallBuffers;
    private List<CatFlyDraw> catFlyAnimationDraws;
    private List<CatFlyDraw> catFlyBuffers;
    private List<CatJumpDraw> catJumpAnimationDraws;
    private List<CatJumpDraw> catJumpBuffers;
    private List<CatRunDraw> catRunAnimationDraws;
    private List<CatRunDraw> catRunBuffers;
    public int cat_type = 1;
    public int cat_under_foot = 1;
    public int cat_x;
    public int cat_y = 190;
    public Bitmap[] coin;
    public int coinNum = 25;
    public float coinPoint = 24.0f;
    private String[] coinState;
    public String[] coin_arr;
    public int drawBgTime = 0;
    public Bitmap[] explote;
    private Context gameContext;
    private Bitmap[] high_number_bitmap;
    public Bitmap[] houseBitmaps;
    public int houseNum = 0;
    public float housePoint = -155.0f;
    private Handler mHandler;
    public Bitmap[] ninjaCat;
    public Bitmap[] ninjaCatJump;
    public int ninjaCatJumpTime = 0;
    public Bitmap[] ninjaCat_Fly;
    public Bitmap[] ninjaCat_Rote;
    public float nowCoinOffset = 0.0f;
    public float nowHouseOffset = 0.0f;
    private Bitmap[] number_bitmap;
    public int offSpeed = 6;
    public boolean pasueButtonState = false;
    public boolean running;
    public int score = 0;
    public SoundPool soundPool;
    public HashMap<Integer, Integer> soundPoolMap;
    public Bitmap[] star_g;
    public Bitmap[] star_s;
    private int[] state_1;
    int streamVolume;
    private SurfaceHolder surfaceHolder;

    public void PlayBgSound() {
        this.BigSound = MediaPlayer.create(this.gameContext, (int) R.raw.snd_gamebg);
        this.BigSound.setVolume(0.8f, 0.8f);
        if (this.BigSound != null) {
            if (!this.BigSound.isPlaying()) {
                this.BigSound.setLooping(true);
                this.BigSound.start();
            }
        } else if (!this.BigSound.isPlaying()) {
            this.BigSound.setLooping(true);
            this.BigSound.start();
        }
    }

    public void StopBgSound() {
        if (this.BigSound != null && this.BigSound.isPlaying()) {
            this.BigSound.stop();
        }
    }

    public void play(int sound, int uLoop) {
        this.soundPool.play(this.soundPoolMap.get(Integer.valueOf(sound)).intValue(), (float) this.streamVolume, (float) this.streamVolume, 1, uLoop, 1.0f);
    }

    public DrawRunning(SurfaceHolder surfaceHolder2, Handler handler, Context context) {
        this.surfaceHolder = surfaceHolder2;
        this.animationDraws = new ArrayList();
        this.buffers = new ArrayList();
        this.catRunAnimationDraws = new ArrayList();
        this.catRunBuffers = new ArrayList();
        this.catJumpAnimationDraws = new ArrayList();
        this.catJumpBuffers = new ArrayList();
        this.catFlyAnimationDraws = new ArrayList();
        this.catFlyBuffers = new ArrayList();
        this.catFallAnimationDraws = new ArrayList();
        this.catFallBuffers = new ArrayList();
        this.mHandler = handler;
        this.gameContext = context;
        this.soundPool = new SoundPool(100, 3, 100);
        this.soundPoolMap = new HashMap<>();
        this.BigSound = MediaPlayer.create(context, R.raw.snd_gamebg);
        this.BigSound.setVolume(1.0f, 1.0f);
        this.streamVolume = ((AudioManager) context.getSystemService("audio")).getStreamVolume(3) / 2;
        this.soundPoolMap.put(1, Integer.valueOf(this.soundPool.load(context, R.raw.snd_silvercoin, 1)));
        this.soundPoolMap.put(2, Integer.valueOf(this.soundPool.load(context, R.raw.snd_goldcoin, 1)));
        this.soundPoolMap.put(3, Integer.valueOf(this.soundPool.load(context, R.raw.snd_run, 1)));
        this.soundPoolMap.put(4, Integer.valueOf(this.soundPool.load(context, R.raw.snd_ropeup, 1)));
        this.soundPoolMap.put(5, Integer.valueOf(this.soundPool.load(context, R.raw.snd_jumpdown, 1)));
        this.soundPoolMap.put(6, Integer.valueOf(this.soundPool.load(context, R.raw.snd_shoot, 1)));
        this.soundPoolMap.put(7, Integer.valueOf(this.soundPool.load(context, R.raw.snd_gameover, 1)));
        this.soundPoolMap.put(8, Integer.valueOf(this.soundPool.load(context, R.raw.snd_ropecatch, 1)));
        this.soundPoolMap.put(9, Integer.valueOf(this.soundPool.load(context, R.raw.snd_speedup, 1)));
        this.soundPoolMap.put(10, Integer.valueOf(this.soundPool.load(context, R.raw.snd_btn, 1)));
        initCoin();
        Log.e("state_1[0]", "=" + this.state_1.length);
        this.background = BitmapFactory.decodeResource(this.gameContext.getResources(), R.drawable.bg_game);
        this.background_cat = new Bitmap[6];
        this.background_cat[0] = BitmapFactory.decodeResource(this.gameContext.getResources(), R.drawable.cat_body);
        this.background_cat[1] = BitmapFactory.decodeResource(this.gameContext.getResources(), R.drawable.cat_bg);
        this.background_cat[2] = BitmapFactory.decodeResource(this.gameContext.getResources(), R.drawable.cat_bg_good);
        this.background_cat[3] = BitmapFactory.decodeResource(this.gameContext.getResources(), R.drawable.cat_bg_speedup);
        this.background_cat[4] = BitmapFactory.decodeResource(this.gameContext.getResources(), R.drawable.cat_bg_lose);
        this.background_cat[5] = BitmapFactory.decodeResource(this.gameContext.getResources(), R.drawable.cat_bg_hand);
        this.houseBitmaps = new Bitmap[2];
        this.houseBitmaps[0] = BitmapFactory.decodeResource(this.gameContext.getResources(), R.drawable.house1);
        this.houseBitmaps[1] = BitmapFactory.decodeResource(this.gameContext.getResources(), R.drawable.house2);
        this.ninjaCat = new Bitmap[5];
        this.ninjaCat[0] = Bitmap.createBitmap(BitmapFactory.decodeResource(this.gameContext.getResources(), R.drawable.cat1), 0, 0, 60, 52);
        this.ninjaCat[1] = Bitmap.createBitmap(BitmapFactory.decodeResource(this.gameContext.getResources(), R.drawable.cat2), 0, 0, 60, 52);
        this.ninjaCat[2] = Bitmap.createBitmap(BitmapFactory.decodeResource(this.gameContext.getResources(), R.drawable.cat3), 0, 0, 60, 52);
        this.ninjaCat[3] = Bitmap.createBitmap(BitmapFactory.decodeResource(this.gameContext.getResources(), R.drawable.cat4), 0, 0, 60, 52);
        this.ninjaCat[4] = Bitmap.createBitmap(BitmapFactory.decodeResource(this.gameContext.getResources(), R.drawable.cat5), 0, 0, 60, 52);
        this.catFall = new Bitmap[2];
        this.catFall[0] = BitmapFactory.decodeResource(this.gameContext.getResources(), R.drawable.cat_fall1);
        this.catFall[1] = BitmapFactory.decodeResource(this.gameContext.getResources(), R.drawable.cat_fall2);
        this.ninjaCat_Fly = new Bitmap[18];
        this.ninjaCat_Fly[0] = BitmapFactory.decodeResource(this.gameContext.getResources(), R.drawable.cat_fly1);
        this.ninjaCat_Fly[1] = BitmapFactory.decodeResource(this.gameContext.getResources(), R.drawable.cat_fly1);
        this.ninjaCat_Fly[2] = BitmapFactory.decodeResource(this.gameContext.getResources(), R.drawable.cat_fly2);
        this.ninjaCat_Fly[3] = BitmapFactory.decodeResource(this.gameContext.getResources(), R.drawable.cat_fly2);
        this.ninjaCat_Fly[4] = BitmapFactory.decodeResource(this.gameContext.getResources(), R.drawable.cat_fly3);
        this.ninjaCat_Fly[5] = BitmapFactory.decodeResource(this.gameContext.getResources(), R.drawable.cat_fly3);
        this.ninjaCat_Fly[6] = BitmapFactory.decodeResource(this.gameContext.getResources(), R.drawable.cat_fly4);
        this.ninjaCat_Fly[7] = BitmapFactory.decodeResource(this.gameContext.getResources(), R.drawable.cat_fly4);
        this.ninjaCat_Fly[8] = BitmapFactory.decodeResource(this.gameContext.getResources(), R.drawable.cat_fly5);
        this.ninjaCat_Fly[9] = BitmapFactory.decodeResource(this.gameContext.getResources(), R.drawable.cat_fly5);
        this.ninjaCat_Fly[10] = BitmapFactory.decodeResource(this.gameContext.getResources(), R.drawable.cat_fly6);
        this.ninjaCat_Fly[11] = BitmapFactory.decodeResource(this.gameContext.getResources(), R.drawable.cat_fly6);
        this.ninjaCat_Fly[12] = BitmapFactory.decodeResource(this.gameContext.getResources(), R.drawable.cat_fly7);
        this.ninjaCat_Fly[13] = BitmapFactory.decodeResource(this.gameContext.getResources(), R.drawable.cat_fly7);
        this.ninjaCat_Fly[14] = BitmapFactory.decodeResource(this.gameContext.getResources(), R.drawable.cat_fly8);
        this.ninjaCat_Fly[15] = BitmapFactory.decodeResource(this.gameContext.getResources(), R.drawable.cat_fly8);
        this.ninjaCat_Fly[16] = BitmapFactory.decodeResource(this.gameContext.getResources(), R.drawable.cat_fly9);
        this.ninjaCat_Fly[17] = BitmapFactory.decodeResource(this.gameContext.getResources(), R.drawable.cat_fly9);
        this.ninjaCat_Rote = new Bitmap[8];
        this.ninjaCat_Rote[0] = BitmapFactory.decodeResource(this.gameContext.getResources(), R.drawable.cat_rote1);
        this.ninjaCat_Rote[1] = BitmapFactory.decodeResource(this.gameContext.getResources(), R.drawable.cat_rote2);
        this.ninjaCat_Rote[2] = BitmapFactory.decodeResource(this.gameContext.getResources(), R.drawable.cat_rote3);
        this.ninjaCat_Rote[3] = BitmapFactory.decodeResource(this.gameContext.getResources(), R.drawable.cat_rote4);
        this.ninjaCat_Rote[4] = BitmapFactory.decodeResource(this.gameContext.getResources(), R.drawable.cat_rote5);
        this.ninjaCat_Rote[5] = BitmapFactory.decodeResource(this.gameContext.getResources(), R.drawable.cat_rote6);
        this.ninjaCat_Rote[6] = BitmapFactory.decodeResource(this.gameContext.getResources(), R.drawable.cat_rote7);
        this.ninjaCat_Rote[7] = BitmapFactory.decodeResource(this.gameContext.getResources(), R.drawable.cat_rote8);
        this.explote = new Bitmap[4];
        this.explote[0] = BitmapFactory.decodeResource(this.gameContext.getResources(), R.drawable.explote_1);
        this.explote[1] = BitmapFactory.decodeResource(this.gameContext.getResources(), R.drawable.explote_2);
        this.explote[2] = BitmapFactory.decodeResource(this.gameContext.getResources(), R.drawable.explote_3);
        this.explote[3] = BitmapFactory.decodeResource(this.gameContext.getResources(), R.drawable.explote_4);
        this.star_s = new Bitmap[4];
        this.star_s[0] = BitmapFactory.decodeResource(this.gameContext.getResources(), R.drawable.star_s1);
        this.star_s[1] = BitmapFactory.decodeResource(this.gameContext.getResources(), R.drawable.star_s2);
        this.star_s[2] = BitmapFactory.decodeResource(this.gameContext.getResources(), R.drawable.star_s3);
        this.star_s[3] = BitmapFactory.decodeResource(this.gameContext.getResources(), R.drawable.star_s4);
        this.star_g = new Bitmap[4];
        this.star_g[0] = BitmapFactory.decodeResource(this.gameContext.getResources(), R.drawable.star_g1);
        this.star_g[1] = BitmapFactory.decodeResource(this.gameContext.getResources(), R.drawable.star_g2);
        this.star_g[2] = BitmapFactory.decodeResource(this.gameContext.getResources(), R.drawable.star_g3);
        this.star_g[3] = BitmapFactory.decodeResource(this.gameContext.getResources(), R.drawable.star_g4);
        this.number_bitmap = new Bitmap[10];
        this.number_bitmap[0] = BitmapFactory.decodeResource(this.gameContext.getResources(), R.drawable.number0);
        this.number_bitmap[1] = BitmapFactory.decodeResource(this.gameContext.getResources(), R.drawable.number1);
        this.number_bitmap[2] = BitmapFactory.decodeResource(this.gameContext.getResources(), R.drawable.number2);
        this.number_bitmap[3] = BitmapFactory.decodeResource(this.gameContext.getResources(), R.drawable.number3);
        this.number_bitmap[4] = BitmapFactory.decodeResource(this.gameContext.getResources(), R.drawable.number4);
        this.number_bitmap[5] = BitmapFactory.decodeResource(this.gameContext.getResources(), R.drawable.number5);
        this.number_bitmap[6] = BitmapFactory.decodeResource(this.gameContext.getResources(), R.drawable.number6);
        this.number_bitmap[7] = BitmapFactory.decodeResource(this.gameContext.getResources(), R.drawable.number7);
        this.number_bitmap[8] = BitmapFactory.decodeResource(this.gameContext.getResources(), R.drawable.number8);
        this.number_bitmap[9] = BitmapFactory.decodeResource(this.gameContext.getResources(), R.drawable.number9);
        this.high_number_bitmap = new Bitmap[10];
        this.high_number_bitmap[0] = BitmapFactory.decodeResource(this.gameContext.getResources(), R.drawable.high_number0);
        this.high_number_bitmap[1] = BitmapFactory.decodeResource(this.gameContext.getResources(), R.drawable.high_number1);
        this.high_number_bitmap[2] = BitmapFactory.decodeResource(this.gameContext.getResources(), R.drawable.high_number2);
        this.high_number_bitmap[3] = BitmapFactory.decodeResource(this.gameContext.getResources(), R.drawable.high_number3);
        this.high_number_bitmap[4] = BitmapFactory.decodeResource(this.gameContext.getResources(), R.drawable.high_number4);
        this.high_number_bitmap[5] = BitmapFactory.decodeResource(this.gameContext.getResources(), R.drawable.high_number5);
        this.high_number_bitmap[6] = BitmapFactory.decodeResource(this.gameContext.getResources(), R.drawable.high_number6);
        this.high_number_bitmap[7] = BitmapFactory.decodeResource(this.gameContext.getResources(), R.drawable.high_number7);
        this.high_number_bitmap[8] = BitmapFactory.decodeResource(this.gameContext.getResources(), R.drawable.high_number8);
        this.high_number_bitmap[9] = BitmapFactory.decodeResource(this.gameContext.getResources(), R.drawable.high_number9);
        this.ninjaCatJump = new Bitmap[1];
        this.ninjaCatJump[0] = Bitmap.createBitmap(BitmapFactory.decodeResource(this.gameContext.getResources(), R.drawable.cat_jump), 0, 0, 60, 52);
        this.coin = new Bitmap[2];
        this.coin[0] = BitmapFactory.decodeResource(this.gameContext.getResources(), R.drawable.coin_s);
        this.coin[1] = BitmapFactory.decodeResource(this.gameContext.getResources(), R.drawable.coin_g);
        this.houseNum = 6;
        this.banner = new Bitmap[2];
        this.banner[0] = BitmapFactory.decodeResource(this.gameContext.getResources(), R.drawable.banner);
        this.banner[1] = BitmapFactory.decodeResource(this.gameContext.getResources(), R.drawable.banner_cat);
    }

    public void run() {
        while (this.running) {
            synchronized (this.surfaceHolder) {
                Canvas canvas = null;
                try {
                    canvas = this.surfaceHolder.lockCanvas(null);
                    doDraw(canvas);
                } finally {
                    if (canvas != null) {
                        this.surfaceHolder.unlockCanvasAndPost(canvas);
                    }
                }
            }
        }
    }

    private void doDraw(Canvas canvas) {
        Bitmap nextFrame;
        if (this.pasueButtonState) {
            setState(5, "");
            this.pasueButtonState = false;
        }
        canvas.drawBitmap(this.background, 0.0f, 0.0f, (Paint) null);
        canvas.drawBitmap(this.coin[1], 10.0f, 20.0f, (Paint) null);
        String a = new StringBuilder().append(this.score).toString();
        for (int i = 0; i < a.length(); i++) {
            String b = a.substring(i, i + 1);
            if ("0".equals(b)) {
                canvas.drawBitmap(this.number_bitmap[0], (float) ((i * 15) + 35), 23.0f, (Paint) null);
            } else if ("1".equals(b)) {
                canvas.drawBitmap(this.number_bitmap[1], (float) ((i * 15) + 35), 23.0f, (Paint) null);
            } else if ("2".equals(b)) {
                canvas.drawBitmap(this.number_bitmap[2], (float) ((i * 15) + 35), 23.0f, (Paint) null);
            } else if ("3".equals(b)) {
                canvas.drawBitmap(this.number_bitmap[3], (float) ((i * 15) + 35), 23.0f, (Paint) null);
            } else if ("4".equals(b)) {
                canvas.drawBitmap(this.number_bitmap[4], (float) ((i * 15) + 35), 23.0f, (Paint) null);
            } else if ("5".equals(b)) {
                canvas.drawBitmap(this.number_bitmap[5], (float) ((i * 15) + 35), 23.0f, (Paint) null);
            } else if ("6".equals(b)) {
                canvas.drawBitmap(this.number_bitmap[6], (float) ((i * 15) + 35), 23.0f, (Paint) null);
            } else if ("7".equals(b)) {
                canvas.drawBitmap(this.number_bitmap[7], (float) ((i * 15) + 35), 23.0f, (Paint) null);
            } else if ("8".equals(b)) {
                canvas.drawBitmap(this.number_bitmap[8], (float) ((i * 15) + 35), 23.0f, (Paint) null);
            } else if ("9".equals(b)) {
                canvas.drawBitmap(this.number_bitmap[9], (float) ((i * 15) + 35), 23.0f, (Paint) null);
            }
        }
        if (this.catBg_type == 1) {
            canvas.drawBitmap(this.background_cat[1], 215.0f, 117.0f, (Paint) null);
        } else if (this.catBg_type == 2) {
            canvas.drawBitmap(this.background_cat[2], 215.0f, 117.0f, (Paint) null);
            this.drawBgTime = this.drawBgTime - 1;
            if (this.drawBgTime == 0) {
                this.catBg_type = 1;
            }
        } else if (this.catBg_type == 3) {
            if (this.drawBgTime % 2 == 0) {
                canvas.drawBitmap(this.background_cat[3], 213.0f, 120.0f, (Paint) null);
            } else {
                canvas.drawBitmap(this.background_cat[3], 217.0f, 120.0f, (Paint) null);
            }
            this.drawBgTime = this.drawBgTime - 1;
            if (this.drawBgTime == 0) {
                this.catBg_type = 1;
            }
        } else {
            if (this.drawBgTime % 2 == 0) {
                canvas.drawBitmap(this.background_cat[4], 215.0f, 117.0f, (Paint) null);
            } else {
                canvas.drawBitmap(this.background_cat[4], 215.0f, 120.0f, (Paint) null);
            }
            this.drawBgTime = this.drawBgTime - 1;
        }
        canvas.drawBitmap(this.background_cat[5], 115.0f, 80.0f, (Paint) null);
        canvas.drawBitmap(this.background_cat[0], 110.0f, 80.0f, (Paint) null);
        if (this.cat_type != CAT_TYPE_CAT_FALL && this.canDoDraw) {
            this.nowHouseOffset = this.nowHouseOffset + ((float) this.offSpeed);
            this.nowCoinOffset = this.nowCoinOffset + ((float) this.offSpeed);
        }
        for (int i2 = this.houseNum - 6; i2 < this.houseNum; i2++) {
            if (this.state_1[i2] == 1) {
                canvas.drawBitmap(this.houseBitmaps[0], ((float) ((((i2 + 6) - this.houseNum) * 115) - 40)) - this.nowHouseOffset, 238.0f, (Paint) null);
            } else if (this.state_1[i2] == 2) {
                canvas.drawBitmap(this.houseBitmaps[1], ((float) ((((i2 + 6) - this.houseNum) * 115) - 40)) - this.nowHouseOffset, 25.0f, (Paint) null);
            } else {
                int i3 = this.state_1[i2];
            }
        }
        if (-40.0f - this.nowHouseOffset <= this.housePoint) {
            this.houseNum = this.houseNum + 1;
            if (this.houseNum % 25 == 0) {
                this.catBg_type = 2;
                this.drawBgTime = 50;
            }
            if (this.houseNum % 80 == 0) {
                this.catBg_type = 3;
                this.drawBgTime = 100;
                this.offSpeed = this.offSpeed + 1;
                play(9, 0);
            }
            this.nowHouseOffset = (this.nowHouseOffset + 40.0f) - 155.0f;
            this.cat_under_foot = this.state_1[this.houseNum - 5];
        } else if (this.cat_type == 1) {
            if (this.nowHouseOffset > 100.0f) {
                this.cat_under_foot = this.state_1[this.houseNum - 4];
            }
        } else if (this.nowHouseOffset > 75.0f) {
            this.cat_under_foot = this.state_1[this.houseNum - 4];
        }
        canvas.drawBitmap(this.banner[0], 2.0f, 290.0f, (Paint) null);
        canvas.drawBitmap(this.banner[1], (float) (this.houseNum + 2), 292.0f, (Paint) null);
        if (this.coinNum != 0) {
            for (int i4 = this.coinNum - 24; i4 < this.coinNum; i4++) {
                if (!this.coinState[i4].equals("0")) {
                    if (this.coinState[i4].equals("1")) {
                        canvas.drawBitmap(this.coin[0], ((float) (((i4 + 24) - this.coinNum) * 25)) - this.nowCoinOffset, 210.0f, (Paint) null);
                        if (((i4 + 24) - this.coinNum) * 25 < 150 && ((i4 + 24) - this.coinNum) * 25 > 85 && 220 > this.cat_y && 220 < this.cat_y + 50) {
                            if (i4 % 2 == 0) {
                                play(1, 0);
                            }
                            this.score = this.score + 15;
                            this.coinState[i4] = "0";
                            addAnimationDraw(new AnimationDraw(75.0f, 200.0f, this.star_s, 20, false, 5));
                        }
                    } else if (this.coinState[i4].equals("2")) {
                        canvas.drawBitmap(this.coin[0], ((float) (((i4 + 24) - this.coinNum) * 25)) - this.nowCoinOffset, 190.0f, (Paint) null);
                        if (((i4 + 24) - this.coinNum) * 25 < 150 && ((i4 + 24) - this.coinNum) * 25 > 85 && 200 > this.cat_y && 200 < this.cat_y + 50) {
                            if (i4 % 2 == 0) {
                                play(1, 0);
                            }
                            this.score = this.score + 15;
                            this.coinState[i4] = "0";
                            addAnimationDraw(new AnimationDraw(75.0f, 180.0f, this.star_s, 20, false, 5));
                        }
                    } else if (this.coinState[i4].equals("3")) {
                        canvas.drawBitmap(this.coin[0], ((float) (((i4 + 24) - this.coinNum) * 25)) - this.nowCoinOffset, 170.0f, (Paint) null);
                        if (((i4 + 24) - this.coinNum) * 25 < 150 && ((i4 + 24) - this.coinNum) * 25 > 85 && 180 > this.cat_y && 180 < this.cat_y + 50) {
                            if (i4 % 2 == 0) {
                                play(1, 0);
                            }
                            this.score = this.score + 15;
                            this.coinState[i4] = "0";
                            addAnimationDraw(new AnimationDraw(75.0f, 160.0f, this.star_s, 20, false, 5));
                        }
                    } else if (this.coinState[i4].equals("4")) {
                        canvas.drawBitmap(this.coin[0], ((float) (((i4 + 24) - this.coinNum) * 25)) - this.nowCoinOffset, 150.0f, (Paint) null);
                        if (((i4 + 24) - this.coinNum) * 25 < 150 && ((i4 + 24) - this.coinNum) * 25 > 85 && 160 > this.cat_y && 160 < this.cat_y + 50) {
                            if (i4 % 2 == 0) {
                                play(1, 0);
                            }
                            this.score = this.score + 15;
                            this.coinState[i4] = "0";
                            addAnimationDraw(new AnimationDraw(75.0f, 140.0f, this.star_s, 20, false, 5));
                        }
                    } else if (this.coinState[i4].equals("5")) {
                        canvas.drawBitmap(this.coin[0], ((float) (((i4 + 24) - this.coinNum) * 25)) - this.nowCoinOffset, 130.0f, (Paint) null);
                        if (((i4 + 24) - this.coinNum) * 25 < 150 && ((i4 + 24) - this.coinNum) * 25 > 85 && 140 > this.cat_y && 140 < this.cat_y + 50) {
                            if (i4 % 2 == 0) {
                                play(1, 0);
                            }
                            this.score = this.score + 15;
                            this.coinState[i4] = "0";
                            addAnimationDraw(new AnimationDraw(75.0f, 120.0f, this.star_s, 20, false, 5));
                        }
                    } else if (this.coinState[i4].equals("6")) {
                        canvas.drawBitmap(this.coin[0], ((float) (((i4 + 24) - this.coinNum) * 25)) - this.nowCoinOffset, 110.0f, (Paint) null);
                        if (((i4 + 24) - this.coinNum) * 25 < 150 && ((i4 + 24) - this.coinNum) * 25 > 85 && 120 > this.cat_y && 120 < this.cat_y + 50) {
                            if (i4 % 2 == 0) {
                                play(1, 0);
                            }
                            this.score = this.score + 15;
                            this.coinState[i4] = "0";
                            addAnimationDraw(new AnimationDraw(75.0f, 100.0f, this.star_s, 20, false, 5));
                        }
                    } else if (this.coinState[i4].equals("a")) {
                        canvas.drawBitmap(this.coin[1], ((float) (((i4 + 24) - this.coinNum) * 25)) - this.nowCoinOffset, 210.0f, (Paint) null);
                        if (((i4 + 24) - this.coinNum) * 25 < 150 && ((i4 + 24) - this.coinNum) * 25 > 85 && 220 > this.cat_y && 220 < this.cat_y + 50) {
                            if (i4 % 2 == 0) {
                                play(2, 0);
                            }
                            this.score = this.score + 20;
                            this.coinState[i4] = "0";
                            addAnimationDraw(new AnimationDraw(75.0f, 200.0f, this.star_g, 20, false, 5));
                        }
                    } else if (this.coinState[i4].equals("b")) {
                        canvas.drawBitmap(this.coin[1], ((float) (((i4 + 24) - this.coinNum) * 25)) - this.nowCoinOffset, 190.0f, (Paint) null);
                        if (((i4 + 24) - this.coinNum) * 25 < 150 && ((i4 + 24) - this.coinNum) * 25 > 85 && 200 > this.cat_y && 200 < this.cat_y + 50) {
                            if (i4 % 2 == 0) {
                                play(2, 0);
                            }
                            this.score = this.score + 20;
                            this.coinState[i4] = "0";
                            addAnimationDraw(new AnimationDraw(75.0f, 180.0f, this.star_g, 20, false, 5));
                        }
                    } else if (this.coinState[i4].equals("c")) {
                        canvas.drawBitmap(this.coin[1], ((float) (((i4 + 24) - this.coinNum) * 25)) - this.nowCoinOffset, 170.0f, (Paint) null);
                        if (((i4 + 24) - this.coinNum) * 25 < 150 && ((i4 + 24) - this.coinNum) * 25 > 85 && 180 > this.cat_y && 180 < this.cat_y + 50) {
                            if (i4 % 2 == 0) {
                                play(2, 0);
                            }
                            this.score = this.score + 20;
                            this.coinState[i4] = "0";
                            addAnimationDraw(new AnimationDraw(75.0f, 160.0f, this.star_g, 20, false, 5));
                        }
                    } else if (this.coinState[i4].equals("d")) {
                        canvas.drawBitmap(this.coin[1], ((float) (((i4 + 24) - this.coinNum) * 25)) - this.nowCoinOffset, 150.0f, (Paint) null);
                        if (((i4 + 24) - this.coinNum) * 25 < 150 && ((i4 + 24) - this.coinNum) * 25 > 85 && 160 > this.cat_y && 160 < this.cat_y + 50) {
                            if (i4 % 2 == 0) {
                                play(2, 0);
                            }
                            this.score = this.score + 20;
                            this.coinState[i4] = "0";
                            addAnimationDraw(new AnimationDraw(75.0f, 140.0f, this.star_g, 20, false, 5));
                        }
                    } else if (this.coinState[i4].equals("e")) {
                        canvas.drawBitmap(this.coin[1], ((float) (((i4 + 24) - this.coinNum) * 25)) - this.nowCoinOffset, 130.0f, (Paint) null);
                        if (((i4 + 24) - this.coinNum) * 25 < 150 && ((i4 + 24) - this.coinNum) * 25 > 85 && 140 > this.cat_y && 140 < this.cat_y + 50) {
                            if (i4 % 2 == 0) {
                                play(2, 0);
                            }
                            this.score = this.score + 20;
                            this.coinState[i4] = "0";
                            addAnimationDraw(new AnimationDraw(75.0f, 120.0f, this.star_g, 20, false, 5));
                        }
                    } else if (this.coinState[i4].equals("f")) {
                        canvas.drawBitmap(this.coin[1], ((float) (((i4 + 24) - this.coinNum) * 25)) - this.nowCoinOffset, 110.0f, (Paint) null);
                        if (((i4 + 24) - this.coinNum) * 25 < 150 && ((i4 + 24) - this.coinNum) * 25 > 85 && 120 > this.cat_y && 120 < this.cat_y + 50) {
                            if (i4 % 2 == 0) {
                                play(2, 0);
                            }
                            this.score = this.score + 20;
                            this.coinState[i4] = "0";
                            addAnimationDraw(new AnimationDraw(75.0f, 100.0f, this.star_g, 20, false, 5));
                        }
                    }
                }
            }
        }
        if (this.nowCoinOffset >= this.coinPoint) {
            this.coinNum = this.coinNum + 1;
            this.nowCoinOffset = this.nowCoinOffset - this.coinPoint;
        }
        synchronized (this) {
            if (!this.buffers.isEmpty()) {
                this.animationDraws.addAll(this.buffers);
                this.buffers.clear();
            }
        }
        Iterator<AnimationDraw> bombIt = this.animationDraws.iterator();
        while (bombIt.hasNext()) {
            AnimationDraw bomb = bombIt.next();
            Bitmap nextFrame2 = bomb.nextFrame();
            if (nextFrame2 == null) {
                bombIt.remove();
            } else {
                if (bomb.type == 1) {
                    bomb.pichuaiziTime++;
                    if (bomb.pichuaiziTime < 10) {
                        if (this.canDoDraw) {
                            bomb.x += 15.0f;
                            bomb.y -= 15.0f;
                        }
                        if (bomb.y < 80.0f && bomb.y > 60.0f) {
                            int i5 = 0;
                            while (i5 < 6) {
                                if (this.state_1[(this.houseNum - 6) + i5] == 2 && bomb.pichuaiziTime != 100 && ((float) ((i5 * 115) - 40)) - this.nowHouseOffset <= bomb.x && (((float) ((i5 * 115) - 40)) - this.nowHouseOffset) + 160.0f > bomb.x && this.cat_type != CAT_TYPE_CAT_FALL) {
                                    Log.d("pichuanzi==", "guazaiqiangshang");
                                    this.cat_type = CAT_TYPE_CAT_HUAXING;
                                    addCatFlyAnimationDraw(new CatFlyDraw(100.0f, (float) this.cat_y, this.ninjaCat_Fly, 10, true, 1));
                                    i5 = 6;
                                    bomb.pichuaiziTime = 100;
                                    play(8, 0);
                                    addAnimationDraw(new AnimationDraw(bomb.x - 5.0f, bomb.y - 16.0f, this.explote, 100, false, 3));
                                }
                                i5++;
                            }
                        }
                    } else if (bomb.pichuaiziTime < 10 || bomb.pichuaiziTime >= 20) {
                        if (bomb.pichuaiziTime < 100 || bomb.pichuaiziTime > 130) {
                            bombIt.remove();
                        } else if (this.canDoDraw) {
                            bomb.x -= (float) this.offSpeed;
                        }
                    } else if (this.canDoDraw) {
                        bomb.y += 15.0f;
                        bomb.x -= 15.0f;
                    }
                }
                if (bomb.type == 3 && this.canDoDraw) {
                    bomb.x -= (float) this.offSpeed;
                }
                canvas.drawBitmap(nextFrame2, bomb.getX(), bomb.getY(), (Paint) null);
            }
        }
        synchronized (this) {
            if (!this.catRunBuffers.isEmpty()) {
                this.catRunAnimationDraws.addAll(this.catRunBuffers);
                this.catRunBuffers.clear();
            }
        }
        Iterator<CatRunDraw> CatRunDrawIt = this.catRunAnimationDraws.iterator();
        while (CatRunDrawIt.hasNext()) {
            CatRunDraw catRun = CatRunDrawIt.next();
            Bitmap nextFrame3 = catRun.nextFrame();
            if (nextFrame3 == null) {
                CatRunDrawIt.remove();
            } else if (this.cat_type != CAT_TYPE_CAT_RUN) {
                CatRunDrawIt.remove();
            } else if (this.cat_under_foot == 0) {
                this.cat_type = CAT_TYPE_CAT_FALL;
                setState(3, "");
                Log.d("cat Run ", "cat_under_foot==0");
                StopBgSound();
                play(7, 0);
                this.catBg_type = 4;
                addCatFallAnimationDraw(new CatFallDraw(100.0f, 190.0f, this.catFall, 50, true));
                canvas.drawBitmap(nextFrame3, catRun.getX(), catRun.getY(), (Paint) null);
                CatRunDrawIt.remove();
            } else {
                canvas.drawBitmap(nextFrame3, catRun.getX(), catRun.getY(), (Paint) null);
            }
        }
        synchronized (this) {
            if (!this.catJumpBuffers.isEmpty()) {
                this.catJumpAnimationDraws.addAll(this.catJumpBuffers);
                this.catJumpBuffers.clear();
            }
        }
        Iterator<CatJumpDraw> CatJumpDrawIt = this.catJumpAnimationDraws.iterator();
        while (CatJumpDrawIt.hasNext()) {
            CatJumpDraw catJump = CatJumpDrawIt.next();
            if (this.canDoDraw) {
                this.ninjaCatJumpTime = this.ninjaCatJumpTime + 1;
            }
            if (this.cat_type == CAT_TYPE_CAT_JUMP || this.cat_type != CAT_TYPE_CAT_HUAXING) {
                if (this.ninjaCatJumpTime <= 12) {
                    if (this.canDoDraw) {
                        catJump.y -= 7.0f;
                        this.cat_y = (int) catJump.y;
                    }
                } else if (this.ninjaCatJumpTime <= 12 || this.ninjaCatJumpTime >= 16) {
                    if (this.canDoDraw) {
                        catJump.y += 9.0f;
                        this.cat_y = (int) catJump.y;
                    }
                    if (this.cat_y < 190 || this.cat_y > 200) {
                        if (this.cat_y > 290) {
                            this.cat_type = CAT_TYPE_CAT_FALL;
                            setState(3, "");
                            Log.d("cat Jump ", "cat_y>250");
                            StopBgSound();
                            play(7, 0);
                            this.catBg_type = 4;
                            addCatFallAnimationDraw(new CatFallDraw(catJump.x, catJump.y, this.catFall, 50, true));
                            canvas.drawBitmap(catJump.bitmaps[0], catJump.getX(), catJump.getY(), (Paint) null);
                            CatJumpDrawIt.remove();
                        }
                    } else if (this.cat_under_foot == 1) {
                        Log.d("catJump(cat_y>=190&&cat_y<=200)", "cat_under_foot=1++++addCatRunAnimationDraw");
                        this.cat_type = CAT_TYPE_CAT_RUN;
                        this.ninjaCatJumpTime = 0;
                        play(5, 0);
                        addCatRunAnimationDraw(new CatRunDraw(100.0f, 190.0f, this.ninjaCat, 50, true));
                        canvas.drawBitmap(catJump.bitmaps[0], catJump.getX(), catJump.getY(), (Paint) null);
                        CatJumpDrawIt.remove();
                    }
                }
                canvas.drawBitmap(catJump.bitmaps[0], catJump.getX(), catJump.getY(), (Paint) null);
            } else {
                canvas.drawBitmap(catJump.bitmaps[0], catJump.getX(), catJump.getY(), (Paint) null);
                CatJumpDrawIt.remove();
            }
        }
        synchronized (this) {
            if (!this.catFlyBuffers.isEmpty()) {
                this.catFlyAnimationDraws.addAll(this.catFlyBuffers);
                this.catFlyBuffers.clear();
            }
        }
        Iterator<CatFlyDraw> CatFlyDrawIt = this.catFlyAnimationDraws.iterator();
        while (CatFlyDrawIt.hasNext()) {
            CatFlyDraw catFly = CatFlyDrawIt.next();
            if (this.canDoDraw) {
                nextFrame = catFly.nextFrame();
            } else {
                nextFrame = catFly.nextFrameDontMove();
            }
            if (catFly.catFlyType == 1) {
                if (this.cat_type != CAT_TYPE_CAT_HUAXING) {
                    CatFlyDrawIt.remove();
                    if (this.cat_type == CAT_TYPE_CAT_RUN) {
                        this.cat_type = CAT_TYPE_CAT_RUN;
                        addCatRunAnimationDraw(new CatRunDraw(100.0f, 190.0f, this.ninjaCat, 50, true));
                    }
                }
            } else if (this.cat_type != CAT_TYPE_CAT_ROTE) {
                CatFlyDrawIt.remove();
            }
            this.cat_y = (int) catFly.y;
            if (nextFrame == null) {
                if (catFly.catFlyType == 1) {
                    CatFlyDrawIt.remove();
                    this.cat_type = CAT_TYPE_CAT_ROTE;
                    Log.d("catFly", "fanhui=null&&___addninjaCat_Rote");
                    Log.e("catFly", "catFly.y=" + catFly.y);
                    addCatFlyAnimationDraw(new CatFlyDraw(100.0f, catFly.y, this.ninjaCat_Rote, 10, true, 2));
                }
                if (catFly.catFlyType == 2) {
                    Log.d("catFly", "fanhui=null___addCatRunAnimationDraw");
                    if (this.cat_under_foot == 1) {
                        CatFlyDrawIt.remove();
                        this.cat_type = CAT_TYPE_CAT_RUN;
                        play(5, 0);
                        addCatRunAnimationDraw(new CatRunDraw(100.0f, 190.0f, this.ninjaCat, 50, true));
                    }
                }
            } else {
                canvas.drawBitmap(nextFrame, catFly.getX(), catFly.getY(), (Paint) null);
            }
        }
        synchronized (this) {
            if (!this.catFallBuffers.isEmpty()) {
                this.catFallAnimationDraws.addAll(this.catFallBuffers);
                this.catFallBuffers.clear();
            }
        }
        if (!this.catFallAnimationDraws.isEmpty()) {
            Iterator<CatFallDraw> CatFallDrawIt = this.catFallAnimationDraws.iterator();
            while (CatFallDrawIt.hasNext()) {
                CatFallDraw catFall2 = CatFallDrawIt.next();
                Bitmap nextFrame4 = catFall2.nextFrame();
                if (this.canDoDraw) {
                    catFall2.y += 2.0f;
                }
                if (nextFrame4 == null) {
                    CatFallDrawIt.remove();
                } else {
                    canvas.drawBitmap(nextFrame4, catFall2.getX(), catFall2.getY(), (Paint) null);
                }
            }
        }
    }

    public void addAnimationDraw(AnimationDraw bomb) {
        synchronized (this) {
            this.buffers.add(bomb);
        }
    }

    public void addCatRunAnimationDraw(CatRunDraw catRun) {
        synchronized (this) {
            this.catRunBuffers.add(catRun);
        }
    }

    public void addCatJumpAnimationDraw(CatJumpDraw catJump) {
        synchronized (this) {
            this.catJumpBuffers.add(catJump);
        }
    }

    public void addCatFallAnimationDraw(CatFallDraw catFall2) {
        synchronized (this) {
            this.catFallBuffers.add(catFall2);
        }
    }

    public void addCatFlyAnimationDraw(CatFlyDraw catFly) {
        synchronized (this) {
            this.catFlyBuffers.add(catFly);
        }
    }

    public void stopDrawing() {
        this.running = false;
    }

    public void setState(int mode, CharSequence message) {
        synchronized (this.surfaceHolder) {
            if (message != null) {
                CharSequence str = ((Object) message) + "\n" + ((Object) "");
            }
            if (mode == 1) {
                Message msg = this.mHandler.obtainMessage();
                msg.setData(new Bundle());
                this.mHandler.sendMessage(msg);
            } else if (mode == 2) {
                Message msg2 = this.mHandler.obtainMessage();
                Bundle b = new Bundle();
                b.putInt("dialog", 4);
                msg2.setData(b);
                this.mHandler.sendMessage(msg2);
            } else if (mode == 3) {
                Message msg3 = this.mHandler.obtainMessage();
                Bundle b2 = new Bundle();
                b2.putString("hasCtach", "win");
                b2.putInt("type", 3);
                b2.putInt("dialog", 0);
                b2.putInt("next", 4);
                b2.putInt("playagain", 0);
                msg3.setData(b2);
                this.mHandler.sendMessage(msg3);
            } else if (mode == 4) {
                Message msg4 = this.mHandler.obtainMessage();
                Bundle b3 = new Bundle();
                b3.putInt("type", 4);
                b3.putInt("dialog", 0);
                b3.putInt("next", 0);
                b3.putInt("playagain", 0);
                msg4.setData(b3);
                this.mHandler.sendMessage(msg4);
            } else if (mode == 5) {
                Log.d("sendMessage", "5");
                Message msg5 = this.mHandler.obtainMessage();
                Bundle b4 = new Bundle();
                b4.putInt("type", 5);
                b4.putInt("dialog", 0);
                b4.putInt("next", 0);
                b4.putInt("playagain", 0);
                msg5.setData(b4);
                this.mHandler.sendMessage(msg5);
            }
        }
    }

    public void initCoin() {
        this.arr = this.gameContext.getResources().getStringArray(R.array.mission);
        this.coin_arr = this.gameContext.getResources().getStringArray(R.array.coinString);
        int coinNowWeizhi = 0;
        this.coinState = new String[(this.coin_arr.length * this.coin_arr[0].length())];
        Log.d("coin_arr.length", "=" + this.coin_arr.length);
        for (int i = 0; i < this.coin_arr.length; i++) {
            for (int j = 0; j < this.coin_arr[i].length(); j++) {
                this.coinState[coinNowWeizhi] = this.coin_arr[i].substring(j, j + 1);
                coinNowWeizhi++;
            }
        }
        int stateWeizhi = 0;
        this.state_1 = new int[(this.arr.length * this.arr[0].length())];
        for (int i2 = 0; i2 < this.arr.length; i2++) {
            for (int j2 = 0; j2 < this.arr[i2].length(); j2++) {
                this.state_1[stateWeizhi] = Integer.parseInt(this.arr[i2].substring(j2, j2 + 1));
                stateWeizhi++;
            }
        }
        play(10, 0);
    }

    public Bundle saveState(Bundle outState) {
        synchronized (this.surfaceHolder) {
            if (outState != null) {
                outState.putInt("houseNum", this.houseNum);
                outState.putInt("score", this.score);
                outState.putInt("drawBgTime", this.drawBgTime);
                outState.putInt("offSpeed", this.offSpeed);
                outState.putInt("cat_under_foot", this.cat_under_foot);
                outState.putInt("ninjaCatJumpTime", this.ninjaCatJumpTime);
                outState.putInt("cat_y", this.cat_y);
                outState.putInt("cat_x", this.cat_x);
                outState.putFloat("nowCoinOffset", this.nowCoinOffset);
                outState.putFloat("nowHouseOffset", this.nowHouseOffset);
                outState.putInt("catBg_type", this.catBg_type);
                outState.putInt("coinNum", this.coinNum);
                outState.putInt("cat_type", this.cat_type);
            }
        }
        return outState;
    }
}
