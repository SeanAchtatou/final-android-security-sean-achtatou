package com.JoneyZNinjaCatFlySky12;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import com.admob.android.ads.AdView;
import com.admob.android.ads.SimpleAdListener;

public class SurfaceAnimationView extends SurfaceView {
    private static String ICICLE_KEY = "nanja_cat_fly";
    /* access modifiers changed from: private */
    public DrawRunning drawRunning;
    /* access modifiers changed from: private */
    public SurfaceHolder houseHolder;
    /* access modifiers changed from: private */
    public RelativeLayout intro;
    /* access modifiers changed from: private */
    public AdView mAd;
    /* access modifiers changed from: private */
    public Bundle mSavedInstanceState;
    /* access modifiers changed from: private */
    public RelativeLayout maDia;
    public Context myContext;
    private ImageButton myPasueButton;
    /* access modifiers changed from: private */
    public ImageButton myPlayAgainButton;
    /* access modifiers changed from: private */
    public ImageButton myResumeButton;

    public void setMyDialog(RelativeLayout dailog, ImageButton button1, ImageButton button2, RelativeLayout myintro, ImageButton button3) {
        this.maDia = dailog;
        this.myPlayAgainButton = button1;
        this.myResumeButton = button2;
        this.myPasueButton = button3;
        this.myPasueButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (SurfaceAnimationView.this.drawRunning.cat_type != 3) {
                    Log.e("myPasueButton", "myPasueButtonClick");
                    SurfaceAnimationView.this.drawRunning.pasueButtonState = true;
                }
            }
        });
        this.intro = myintro;
    }

    public SurfaceAnimationView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.myContext = context;
        Log.e("context", "=" + context);
        Log.e("myContext", "=" + this.myContext);
        final Bitmap[] ninjaCat = {Bitmap.createBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.cat1), 0, 0, 60, 52), Bitmap.createBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.cat2), 0, 0, 60, 52), Bitmap.createBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.cat3), 0, 0, 60, 52), Bitmap.createBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.cat4), 0, 0, 60, 52), Bitmap.createBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.cat5), 0, 0, 60, 52)};
        final Bitmap[] ninjaCatJump = {Bitmap.createBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.cat_jump), 0, 0, 60, 52)};
        final Bitmap[] piChuaiZiJump = {BitmapFactory.decodeResource(getResources(), R.drawable.pichuaizi)};
        getHolder().addCallback(new SurfaceHolder.Callback() {
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
                holder.setFixedSize(width, height);
            }

            public void surfaceCreated(SurfaceHolder holder) {
                Log.e("surfaceCreated", "surfaceCreated");
                Log.e("I want Know", "mSavedInstanceState is =" + SurfaceAnimationView.this.mSavedInstanceState);
                if (SurfaceAnimationView.this.mSavedInstanceState == null) {
                    SurfaceAnimationView.this.maDia.setVisibility(4);
                    SurfaceAnimationView.this.myPlayAgainButton.setVisibility(4);
                    SurfaceAnimationView surfaceAnimationView = SurfaceAnimationView.this;
                    final Bitmap[] bitmapArr = ninjaCat;
                    surfaceAnimationView.drawRunning = new DrawRunning(holder, new Handler() {
                        public void handleMessage(Message m) {
                            m.getData().getInt("dialog");
                            Log.e("recive", "recive");
                            if (m.getData().getInt("type") == 3) {
                                SurfaceAnimationView.this.maDia.setBackgroundResource(R.drawable.end_bg);
                                SurfaceAnimationView.this.maDia.setVisibility(0);
                                SurfaceAnimationView.this.myPlayAgainButton.setVisibility(0);
                                SurfaceAnimationView.this.myResumeButton.setVisibility(4);
                                SurfaceAnimationView.this.mAd.requestFreshAd();
                                ImageButton access$4 = SurfaceAnimationView.this.myPlayAgainButton;
                                final Bitmap[] bitmapArr = bitmapArr;
                                access$4.setOnClickListener(new View.OnClickListener() {
                                    public void onClick(View v) {
                                        SurfaceAnimationView.this.maDia.setVisibility(4);
                                        SurfaceAnimationView.this.myPlayAgainButton.setVisibility(4);
                                        SurfaceAnimationView.this.myResumeButton.setVisibility(4);
                                        SurfaceAnimationView.this.myPlayAgainButton.setBackgroundResource(R.drawable.fish2_retry);
                                        SurfaceAnimationView.this.drawRunning.play(8, 0);
                                        SurfaceAnimationView.this.drawRunning.houseNum = 6;
                                        SurfaceAnimationView.this.drawRunning.score = 0;
                                        SurfaceAnimationView.this.drawRunning.drawBgTime = 0;
                                        SurfaceAnimationView.this.drawRunning.offSpeed = 6;
                                        SurfaceAnimationView.this.drawRunning.cat_under_foot = 1;
                                        SurfaceAnimationView.this.drawRunning.ninjaCatJumpTime = 0;
                                        SurfaceAnimationView.this.drawRunning.cat_y = 190;
                                        SurfaceAnimationView.this.drawRunning.cat_x = 100;
                                        SurfaceAnimationView.this.drawRunning.nowCoinOffset = 0.0f;
                                        SurfaceAnimationView.this.drawRunning.nowHouseOffset = 0.0f;
                                        SurfaceAnimationView.this.drawRunning.PlayBgSound();
                                        SurfaceAnimationView.this.drawRunning.catBg_type = 1;
                                        SurfaceAnimationView.this.drawRunning.coinNum = 0;
                                        SurfaceAnimationView.this.drawRunning.initCoin();
                                        SurfaceAnimationView.this.drawRunning.coinNum = 25;
                                        SurfaceAnimationView.this.drawRunning.cat_type = 1;
                                        SurfaceAnimationView.this.drawRunning.addCatRunAnimationDraw(new CatRunDraw(100.0f, 190.0f, bitmapArr, 50, true));
                                        SurfaceAnimationView.this.myPlayAgainButton.setBackgroundResource(R.drawable.fish1_retry);
                                    }
                                });
                            } else if (m.getData().getInt("type") == 5) {
                                SurfaceAnimationView.this.maDia.setBackgroundResource(R.drawable.resume_bg);
                                SurfaceAnimationView.this.maDia.setVisibility(0);
                                SurfaceAnimationView.this.myPlayAgainButton.setVisibility(0);
                                SurfaceAnimationView.this.myResumeButton.setVisibility(0);
                                SurfaceAnimationView.this.mAd.requestFreshAd();
                                SurfaceAnimationView.this.drawRunning.canDoDraw = false;
                                SurfaceAnimationView.this.myPlayAgainButton.setOnClickListener(new View.OnClickListener() {
                                    public void onClick(View v) {
                                        SurfaceAnimationView.this.maDia.setVisibility(4);
                                        SurfaceAnimationView.this.myPlayAgainButton.setVisibility(4);
                                        SurfaceAnimationView.this.myResumeButton.setVisibility(4);
                                        SurfaceAnimationView.this.myPlayAgainButton.setBackgroundResource(R.drawable.fish2_retry);
                                        SurfaceAnimationView.this.drawRunning.play(8, 0);
                                        SurfaceAnimationView.this.drawRunning.houseNum = 6;
                                        SurfaceAnimationView.this.drawRunning.score = 0;
                                        SurfaceAnimationView.this.drawRunning.drawBgTime = 0;
                                        SurfaceAnimationView.this.drawRunning.offSpeed = 6;
                                        SurfaceAnimationView.this.drawRunning.cat_under_foot = 1;
                                        SurfaceAnimationView.this.drawRunning.ninjaCatJumpTime = 0;
                                        SurfaceAnimationView.this.drawRunning.cat_y = 190;
                                        SurfaceAnimationView.this.drawRunning.cat_x = 100;
                                        SurfaceAnimationView.this.drawRunning.nowCoinOffset = 0.0f;
                                        SurfaceAnimationView.this.drawRunning.nowHouseOffset = 0.0f;
                                        SurfaceAnimationView.this.drawRunning.catBg_type = 1;
                                        SurfaceAnimationView.this.drawRunning.coinNum = 0;
                                        SurfaceAnimationView.this.drawRunning.initCoin();
                                        SurfaceAnimationView.this.drawRunning.coinNum = 25;
                                        SurfaceAnimationView.this.drawRunning.cat_type = 1;
                                        SurfaceAnimationView.this.drawRunning.canDoDraw = true;
                                        SurfaceAnimationView.this.myPlayAgainButton.setBackgroundResource(R.drawable.fish1_retry);
                                    }
                                });
                                SurfaceAnimationView.this.myResumeButton.setOnClickListener(new View.OnClickListener() {
                                    public void onClick(View v) {
                                        SurfaceAnimationView.this.drawRunning.play(8, 0);
                                        SurfaceAnimationView.this.maDia.setVisibility(4);
                                        SurfaceAnimationView.this.myPlayAgainButton.setVisibility(4);
                                        SurfaceAnimationView.this.myResumeButton.setVisibility(4);
                                        SurfaceAnimationView.this.drawRunning.canDoDraw = true;
                                    }
                                });
                            }
                        }
                    }, SurfaceAnimationView.this.myContext);
                    SurfaceAnimationView.this.houseHolder = holder;
                    SurfaceAnimationView.this.drawRunning.addCatRunAnimationDraw(new CatRunDraw(100.0f, 190.0f, ninjaCat, 50, true));
                    Log.d("drawRunning", "++++++++++++++start");
                    Log.d("houseThreaddrawRunning", "++++++++++++++start");
                    Log.w(getClass().getName(), "SIS is null");
                    return;
                }
                SurfaceAnimationView.this.maDia.setVisibility(4);
                SurfaceAnimationView.this.myPlayAgainButton.setVisibility(4);
                SurfaceAnimationView.this.intro.setVisibility(4);
                SurfaceAnimationView surfaceAnimationView2 = SurfaceAnimationView.this;
                final Bitmap[] bitmapArr2 = ninjaCat;
                final Bitmap[] bitmapArr3 = ninjaCatJump;
                surfaceAnimationView2.drawRunning = new DrawRunning(holder, new Handler() {
                    public void handleMessage(Message m) {
                        m.getData().getInt("dialog");
                        if (m.getData().getInt("type") == 3) {
                            SurfaceAnimationView.this.maDia.setBackgroundResource(R.drawable.end_bg);
                            SurfaceAnimationView.this.maDia.setVisibility(0);
                            SurfaceAnimationView.this.myPlayAgainButton.setVisibility(0);
                            SurfaceAnimationView.this.myResumeButton.setVisibility(4);
                            SurfaceAnimationView.this.mAd.requestFreshAd();
                            ImageButton access$4 = SurfaceAnimationView.this.myPlayAgainButton;
                            final Bitmap[] bitmapArr = bitmapArr2;
                            access$4.setOnClickListener(new View.OnClickListener() {
                                public void onClick(View v) {
                                    SurfaceAnimationView.this.maDia.setVisibility(4);
                                    SurfaceAnimationView.this.myPlayAgainButton.setVisibility(4);
                                    SurfaceAnimationView.this.myResumeButton.setVisibility(4);
                                    SurfaceAnimationView.this.myPlayAgainButton.setBackgroundResource(R.drawable.fish2_retry);
                                    SurfaceAnimationView.this.drawRunning.play(8, 0);
                                    SurfaceAnimationView.this.drawRunning.houseNum = 6;
                                    SurfaceAnimationView.this.drawRunning.score = 0;
                                    SurfaceAnimationView.this.drawRunning.drawBgTime = 0;
                                    SurfaceAnimationView.this.drawRunning.offSpeed = 6;
                                    SurfaceAnimationView.this.drawRunning.cat_under_foot = 1;
                                    SurfaceAnimationView.this.drawRunning.ninjaCatJumpTime = 0;
                                    SurfaceAnimationView.this.drawRunning.cat_y = 190;
                                    SurfaceAnimationView.this.drawRunning.cat_x = 100;
                                    SurfaceAnimationView.this.drawRunning.nowCoinOffset = 0.0f;
                                    SurfaceAnimationView.this.drawRunning.nowHouseOffset = 0.0f;
                                    SurfaceAnimationView.this.drawRunning.PlayBgSound();
                                    SurfaceAnimationView.this.drawRunning.catBg_type = 1;
                                    SurfaceAnimationView.this.drawRunning.coinNum = 0;
                                    SurfaceAnimationView.this.drawRunning.initCoin();
                                    SurfaceAnimationView.this.drawRunning.coinNum = 25;
                                    SurfaceAnimationView.this.drawRunning.cat_type = 1;
                                    SurfaceAnimationView.this.drawRunning.addCatRunAnimationDraw(new CatRunDraw(100.0f, 190.0f, bitmapArr, 50, true));
                                    SurfaceAnimationView.this.myPlayAgainButton.setBackgroundResource(R.drawable.fish1_retry);
                                }
                            });
                        } else if (m.getData().getInt("type") == 5) {
                            SurfaceAnimationView.this.maDia.setBackgroundResource(R.drawable.resume_bg);
                            SurfaceAnimationView.this.maDia.setVisibility(0);
                            SurfaceAnimationView.this.myPlayAgainButton.setVisibility(0);
                            SurfaceAnimationView.this.myResumeButton.setVisibility(0);
                            SurfaceAnimationView.this.mAd.requestFreshAd();
                            SurfaceAnimationView.this.drawRunning.canDoDraw = false;
                            SurfaceAnimationView.this.myPlayAgainButton.setOnClickListener(new View.OnClickListener() {
                                public void onClick(View v) {
                                    SurfaceAnimationView.this.maDia.setVisibility(4);
                                    SurfaceAnimationView.this.myPlayAgainButton.setVisibility(4);
                                    SurfaceAnimationView.this.myResumeButton.setVisibility(4);
                                    SurfaceAnimationView.this.myPlayAgainButton.setBackgroundResource(R.drawable.fish2_retry);
                                    SurfaceAnimationView.this.drawRunning.play(8, 0);
                                    SurfaceAnimationView.this.drawRunning.houseNum = 6;
                                    SurfaceAnimationView.this.drawRunning.score = 0;
                                    SurfaceAnimationView.this.drawRunning.drawBgTime = 0;
                                    SurfaceAnimationView.this.drawRunning.offSpeed = 6;
                                    SurfaceAnimationView.this.drawRunning.cat_under_foot = 1;
                                    SurfaceAnimationView.this.drawRunning.ninjaCatJumpTime = 0;
                                    SurfaceAnimationView.this.drawRunning.cat_y = 190;
                                    SurfaceAnimationView.this.drawRunning.cat_x = 100;
                                    SurfaceAnimationView.this.drawRunning.nowCoinOffset = 0.0f;
                                    SurfaceAnimationView.this.drawRunning.nowHouseOffset = 0.0f;
                                    SurfaceAnimationView.this.drawRunning.catBg_type = 1;
                                    SurfaceAnimationView.this.drawRunning.coinNum = 0;
                                    SurfaceAnimationView.this.drawRunning.initCoin();
                                    SurfaceAnimationView.this.drawRunning.coinNum = 25;
                                    SurfaceAnimationView.this.drawRunning.cat_type = 1;
                                    SurfaceAnimationView.this.drawRunning.canDoDraw = true;
                                    SurfaceAnimationView.this.myPlayAgainButton.setBackgroundResource(R.drawable.fish1_retry);
                                }
                            });
                            SurfaceAnimationView.this.myResumeButton.setOnClickListener(new View.OnClickListener() {
                                public void onClick(View v) {
                                    SurfaceAnimationView.this.drawRunning.play(8, 0);
                                    SurfaceAnimationView.this.maDia.setVisibility(4);
                                    SurfaceAnimationView.this.myPlayAgainButton.setVisibility(4);
                                    SurfaceAnimationView.this.myResumeButton.setVisibility(4);
                                    SurfaceAnimationView.this.drawRunning.canDoDraw = true;
                                }
                            });
                        } else {
                            Log.e("recive", "recive");
                            SurfaceAnimationView.this.drawRunning.cat_type = 3;
                            SurfaceAnimationView.this.maDia.setBackgroundResource(R.drawable.resume_bg);
                            SurfaceAnimationView.this.maDia.setVisibility(0);
                            SurfaceAnimationView.this.myPlayAgainButton.setVisibility(0);
                            SurfaceAnimationView.this.myResumeButton.setVisibility(0);
                            SurfaceAnimationView.this.mAd.requestFreshAd();
                            ImageButton access$5 = SurfaceAnimationView.this.myResumeButton;
                            final Bitmap[] bitmapArr2 = bitmapArr3;
                            access$5.setOnClickListener(new View.OnClickListener() {
                                public void onClick(View v) {
                                    SurfaceAnimationView.this.drawRunning.play(8, 0);
                                    SurfaceAnimationView.this.maDia.setVisibility(4);
                                    SurfaceAnimationView.this.myPlayAgainButton.setVisibility(4);
                                    SurfaceAnimationView.this.myResumeButton.setVisibility(4);
                                    SurfaceAnimationView.this.myPlayAgainButton.setBackgroundResource(R.drawable.fish2_retry);
                                    SurfaceAnimationView.this.drawRunning.ninjaCatJumpTime = 0;
                                    SurfaceAnimationView.this.drawRunning.addCatJumpAnimationDraw(new CatJumpDraw(100.0f, 190.0f, bitmapArr2, 50, true));
                                    SurfaceAnimationView.this.drawRunning.cat_type = DrawRunning.CAT_TYPE_CAT_JUMP;
                                    SurfaceAnimationView.this.drawRunning.play(4, 0);
                                    SurfaceAnimationView.this.drawRunning.PlayBgSound();
                                    SurfaceAnimationView.this.drawRunning.initCoin();
                                    SurfaceAnimationView.this.myPlayAgainButton.setBackgroundResource(R.drawable.fish1_retry);
                                }
                            });
                            ImageButton access$42 = SurfaceAnimationView.this.myPlayAgainButton;
                            final Bitmap[] bitmapArr3 = bitmapArr2;
                            access$42.setOnClickListener(new View.OnClickListener() {
                                public void onClick(View v) {
                                    SurfaceAnimationView.this.maDia.setVisibility(4);
                                    SurfaceAnimationView.this.myPlayAgainButton.setVisibility(4);
                                    SurfaceAnimationView.this.myResumeButton.setVisibility(4);
                                    SurfaceAnimationView.this.myPlayAgainButton.setBackgroundResource(R.drawable.fish2_retry);
                                    SurfaceAnimationView.this.drawRunning.play(8, 0);
                                    SurfaceAnimationView.this.drawRunning.houseNum = 6;
                                    SurfaceAnimationView.this.drawRunning.score = 0;
                                    SurfaceAnimationView.this.drawRunning.drawBgTime = 0;
                                    SurfaceAnimationView.this.drawRunning.offSpeed = 6;
                                    SurfaceAnimationView.this.drawRunning.cat_under_foot = 1;
                                    SurfaceAnimationView.this.drawRunning.ninjaCatJumpTime = 0;
                                    SurfaceAnimationView.this.drawRunning.cat_y = 190;
                                    SurfaceAnimationView.this.drawRunning.cat_x = 100;
                                    SurfaceAnimationView.this.drawRunning.nowCoinOffset = 0.0f;
                                    SurfaceAnimationView.this.drawRunning.nowHouseOffset = 0.0f;
                                    SurfaceAnimationView.this.drawRunning.PlayBgSound();
                                    SurfaceAnimationView.this.drawRunning.catBg_type = 1;
                                    SurfaceAnimationView.this.drawRunning.coinNum = 0;
                                    SurfaceAnimationView.this.drawRunning.initCoin();
                                    SurfaceAnimationView.this.drawRunning.coinNum = 25;
                                    SurfaceAnimationView.this.drawRunning.cat_type = 1;
                                    SurfaceAnimationView.this.drawRunning.addCatRunAnimationDraw(new CatRunDraw(100.0f, 190.0f, bitmapArr3, 50, true));
                                    SurfaceAnimationView.this.myPlayAgainButton.setBackgroundResource(R.drawable.fish1_retry);
                                }
                            });
                        }
                    }
                }, SurfaceAnimationView.this.myContext);
                SurfaceAnimationView.this.houseHolder = holder;
                Log.d("BeginRestoreState", "restoreState");
                SurfaceAnimationView.this.drawRunning.running = true;
                SurfaceAnimationView.this.restoreState(SurfaceAnimationView.this.mSavedInstanceState);
                new Thread(SurfaceAnimationView.this.drawRunning).start();
                SurfaceAnimationView.this.drawRunning.addCatRunAnimationDraw(new CatRunDraw(100.0f, 190.0f, ninjaCat, 50, true));
                Log.w(getClass().getName(), "SIS is nonnull");
                SurfaceAnimationView.this.setFocusable(true);
            }

            public void surfaceDestroyed(SurfaceHolder holder) {
                SurfaceAnimationView.this.drawRunning.stopDrawing();
                SurfaceAnimationView.this.drawRunning.StopBgSound();
                synchronized (holder) {
                }
            }
        });
        setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == 0 && SurfaceAnimationView.this.drawRunning != null) {
                    if (!SurfaceAnimationView.this.drawRunning.running) {
                        SurfaceAnimationView.this.intro.setVisibility(4);
                        SurfaceAnimationView.this.drawRunning.running = true;
                        SurfaceAnimationView.this.drawRunning.play(10, 0);
                        SurfaceAnimationView.this.drawRunning.PlayBgSound();
                        new Thread(SurfaceAnimationView.this.drawRunning).start();
                    }
                    if (SurfaceAnimationView.this.drawRunning.cat_type == DrawRunning.CAT_TYPE_CAT_RUN) {
                        SurfaceAnimationView.this.drawRunning.ninjaCatJumpTime = 0;
                        SurfaceAnimationView.this.drawRunning.addCatJumpAnimationDraw(new CatJumpDraw(100.0f, 190.0f, ninjaCatJump, 50, true));
                        SurfaceAnimationView.this.drawRunning.cat_type = DrawRunning.CAT_TYPE_CAT_JUMP;
                        SurfaceAnimationView.this.drawRunning.play(4, 0);
                        return true;
                    } else if (SurfaceAnimationView.this.drawRunning.cat_type == DrawRunning.CAT_TYPE_CAT_JUMP) {
                        SurfaceAnimationView.this.drawRunning.cat_type = DrawRunning.CAT_TYPE_CAT_CHUGOU;
                        SurfaceAnimationView.this.drawRunning.play(6, 0);
                        SurfaceAnimationView.this.drawRunning.addAnimationDraw(new AnimationDraw(150.0f, (float) SurfaceAnimationView.this.drawRunning.cat_y, piChuaiZiJump[0], -1, 1));
                        return true;
                    } else if (SurfaceAnimationView.this.drawRunning.cat_type == DrawRunning.CAT_TYPE_CAT_ROTE) {
                        SurfaceAnimationView.this.drawRunning.addAnimationDraw(new AnimationDraw(150.0f, (float) SurfaceAnimationView.this.drawRunning.cat_y, piChuaiZiJump[0], -1, 1));
                        SurfaceAnimationView.this.drawRunning.ninjaCatJumpTime = 18;
                        SurfaceAnimationView.this.drawRunning.addCatJumpAnimationDraw(new CatJumpDraw(100.0f, (float) SurfaceAnimationView.this.drawRunning.cat_y, ninjaCatJump, 50, true));
                        SurfaceAnimationView.this.drawRunning.cat_type = DrawRunning.CAT_TYPE_CAT_CHUGOU;
                        SurfaceAnimationView.this.drawRunning.play(6, 0);
                        return true;
                    }
                }
                return true;
            }
        });
    }

    public DrawRunning getThread() {
        Log.e("DrawRunningIn", "=" + this.drawRunning);
        return this.drawRunning;
    }

    public void setAd() {
        AlphaAnimation animation = new AlphaAnimation(0.0f, 1.0f);
        animation.setDuration(400);
        animation.setFillAfter(true);
        animation.setInterpolator(new AccelerateInterpolator());
        this.mAd.startAnimation(animation);
    }

    public void setNoAd() {
        this.mAd.setVisibility(8);
    }

    public void setTextView(AdView adView) {
        this.mAd = adView;
        this.mAd.setVisibility(0);
        this.mAd.setAdListener(new LunarLanderListener(this, null));
    }

    private class LunarLanderListener extends SimpleAdListener {
        private LunarLanderListener() {
        }

        /* synthetic */ LunarLanderListener(SurfaceAnimationView surfaceAnimationView, LunarLanderListener lunarLanderListener) {
            this();
        }

        public void onFailedToReceiveAd(AdView adView) {
            super.onFailedToReceiveAd(adView);
            Log.d("surviewad", "=" + SurfaceAnimationView.this.mAd);
            SurfaceAnimationView.this.mAd.requestFreshAd();
            Log.d("Lunar", "onFailedToReceiveAd");
        }

        public void onFailedToReceiveRefreshedAd(AdView adView) {
            super.onFailedToReceiveRefreshedAd(adView);
            SurfaceAnimationView.this.mAd.requestFreshAd();
            Log.d("surviewadLunar", "onFailedToReceiveRefreshedAd");
        }

        public void onReceiveAd(AdView adView) {
            super.onReceiveAd(adView);
            Log.d("surviewadLunar", "onReceiveAd");
            SurfaceAnimationView.this.setAd();
            SurfaceAnimationView.this.mAd.setRequestInterval(60);
        }

        public void onReceiveRefreshedAd(AdView adView) {
            super.onReceiveRefreshedAd(adView);
            Log.d("surviewadLunar", "onReceiveRefreshedAd");
            SurfaceAnimationView.this.mAd.requestFreshAd();
            SurfaceAnimationView.this.setAd();
        }
    }

    public void setSaved(Bundle InstanceState) {
        this.mSavedInstanceState = InstanceState;
    }

    public Bundle saveState(Bundle outState) {
        Bundle map = new Bundle();
        synchronized (this.houseHolder) {
            if (map != null) {
                map.putInt("houseNum", this.drawRunning.houseNum);
                map.putInt("score", this.drawRunning.score);
                map.putInt("drawBgTime", this.drawRunning.drawBgTime);
                map.putInt("offSpeed", this.drawRunning.offSpeed);
                map.putInt("cat_under_foot", this.drawRunning.cat_under_foot);
                map.putInt("ninjaCatJumpTime", this.drawRunning.ninjaCatJumpTime);
                map.putInt("cat_y", this.drawRunning.cat_y);
                map.putInt("cat_x", this.drawRunning.cat_x);
                map.putFloat("nowCoinOffset", this.drawRunning.nowCoinOffset);
                map.putFloat("nowHouseOffset", this.drawRunning.nowHouseOffset);
                map.putInt("catBg_type", this.drawRunning.catBg_type);
                map.putInt("coinNum", this.drawRunning.coinNum);
                map.putInt("cat_type", this.drawRunning.cat_type);
            }
        }
        return map;
    }

    public synchronized void restoreState(Bundle savedState) {
        synchronized (this.houseHolder) {
            this.drawRunning.houseNum = savedState.getInt("houseNum");
            this.drawRunning.score = savedState.getInt("score");
            this.drawRunning.drawBgTime = savedState.getInt("drawBgTime");
            this.drawRunning.offSpeed = savedState.getInt("offSpeed");
            this.drawRunning.cat_under_foot = savedState.getInt("cat_under_foot");
            this.drawRunning.ninjaCatJumpTime = savedState.getInt("ninjaCatJumpTime");
            this.drawRunning.cat_y = savedState.getInt("cat_y");
            this.drawRunning.cat_x = savedState.getInt("cat_x");
            this.drawRunning.nowCoinOffset = savedState.getFloat("nowCoinOffset");
            this.drawRunning.nowHouseOffset = savedState.getFloat("nowHouseOffset");
            this.drawRunning.catBg_type = savedState.getInt("catBg_type");
            this.drawRunning.coinNum = savedState.getInt("coinNum");
            this.drawRunning.cat_type = savedState.getInt("cat_type");
            this.drawRunning.setState(4, "");
        }
    }
}
