package com.JoneyZNinjaCatFlySky12;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import com.admob.android.ads.AdManager;
import com.admob.android.ads.AdView;
import com.admob.android.ads.SimpleAdListener;

public class NinjaCatActivity extends Activity {
    public AdView ad;
    private Bundle mSavedInstanceState;
    private DrawRunning nanjaCatDraw;
    private SurfaceAnimationView newSurface;
    private View.OnFocusChangeListener onFocusC = new View.OnFocusChangeListener() {
        public void onFocusChange(View v, boolean state) {
            Log.e("OnonFocusChange", "=" + v + state);
        }
    };

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mSavedInstanceState = savedInstanceState;
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        setContentView((int) R.layout.main);
        this.newSurface = (SurfaceAnimationView) findViewById(R.id.ninjaCatFlySkyView);
        Log.e("nanjaCatDraw", "nanjaCatDraw=" + this.nanjaCatDraw);
        this.newSurface.setMyDialog((RelativeLayout) findViewById(R.id.done_game), (ImageButton) findViewById(R.id.playAgagin_Button), (ImageButton) findViewById(R.id.resume_Button), (RelativeLayout) findViewById(R.id.intro_game), (ImageButton) findViewById(R.id.pasue_Button));
        this.ad = (AdView) findViewById(R.id.admobad);
        AdManager.setPublisherId("a14e1c002d54274");
        this.newSurface.setTextView(this.ad);
        setAd();
        this.ad.setAdListener(new LunarLanderListener(this, null));
        if (this.mSavedInstanceState == null) {
            Log.w(getClass().getName(), "SIS is null");
        } else {
            this.newSurface.setSaved(this.mSavedInstanceState);
            Log.w(getClass().getName(), "SIS is notnull");
        }
        this.newSurface.setOnFocusChangeListener(this.onFocusC);
    }

    public void setAd() {
        this.ad.setVisibility(0);
        AlphaAnimation animation = new AlphaAnimation(0.0f, 1.0f);
        animation.setDuration(400);
        animation.setFillAfter(true);
        animation.setInterpolator(new AccelerateInterpolator());
        this.ad.startAnimation(animation);
    }

    private class LunarLanderListener extends SimpleAdListener {
        private LunarLanderListener() {
        }

        /* synthetic */ LunarLanderListener(NinjaCatActivity ninjaCatActivity, LunarLanderListener lunarLanderListener) {
            this();
        }

        public void onFailedToReceiveAd(AdView adView) {
            super.onFailedToReceiveAd(adView);
            Log.d("ad", "=" + NinjaCatActivity.this.ad);
            NinjaCatActivity.this.ad.requestFreshAd();
            Log.d("Lunar", "onFailedToReceiveAd");
        }

        public void onFailedToReceiveRefreshedAd(AdView adView) {
            super.onFailedToReceiveRefreshedAd(adView);
            NinjaCatActivity.this.ad.requestFreshAd();
            Log.d("Lunar", "onFailedToReceiveRefreshedAd");
        }

        public void onReceiveAd(AdView adView) {
            super.onReceiveAd(adView);
            Log.d("Lunar", "onReceiveAd");
            NinjaCatActivity.this.setAd();
            NinjaCatActivity.this.ad.requestFreshAd();
        }

        public void onReceiveRefreshedAd(AdView adView) {
            super.onReceiveRefreshedAd(adView);
            NinjaCatActivity.this.setAd();
            NinjaCatActivity.this.ad.requestFreshAd();
            Log.d("Lunar", "onReceiveRefreshedAd");
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.e(getClass().getName(), "nanjaCatDraw=" + this.nanjaCatDraw);
        if (this.nanjaCatDraw != null) {
            this.mSavedInstanceState = this.nanjaCatDraw.saveState(outState);
            return;
        }
        this.nanjaCatDraw = this.newSurface.getThread();
        this.nanjaCatDraw.running = false;
        this.mSavedInstanceState = this.nanjaCatDraw.saveState(outState);
        Log.w(getClass().getName(), "SIS called");
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        Log.d(getClass().getName(), "onPause");
        this.nanjaCatDraw = this.newSurface.getThread();
        this.newSurface.setSaved(this.mSavedInstanceState);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        Log.d(getClass().getName(), "onDestroy");
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        Log.e(getClass().getName(), "onStop");
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        super.onRestart();
        Log.e(getClass().getName(), "onRestart");
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        Log.e(getClass().getName(), "onStart");
        this.newSurface.setSaved(this.mSavedInstanceState);
    }
}
