package com.JoneyZNinjaCatFlySky12;

import android.app.Activity;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.widget.ImageButton;
import com.admob.android.ads.AdManager;
import com.admob.android.ads.AdView;
import com.admob.android.ads.SimpleAdListener;
import java.util.HashMap;

public class EntryActivity extends Activity {
    MediaPlayer BigSound;
    public AdView ad;
    private DisplayMetrics dm;
    private View.OnClickListener enter_game = new View.OnClickListener() {
        public void onClick(View v) {
            EntryActivity.this.StopBgSound();
            EntryActivity.this.play(1, 0);
            Intent intent = new Intent();
            intent.setClass(EntryActivity.this, NinjaCatActivity.class);
            EntryActivity.this.startActivity(intent);
        }
    };
    private ImageButton playButton;
    public SoundPool soundPool;
    public HashMap<Integer, Integer> soundPoolMap;
    int streamVolume;
    private float width = 480.0f;

    public void PlayBgSound() {
        this.BigSound = MediaPlayer.create(this, (int) R.raw.snd_titlebg);
        if (this.BigSound == null) {
            this.BigSound = MediaPlayer.create(this, (int) R.raw.snd_titlebg);
            if (!this.BigSound.isPlaying()) {
                this.BigSound.setVolume(0.8f, 0.8f);
                this.BigSound.setLooping(true);
                this.BigSound.start();
            }
        } else if (!this.BigSound.isPlaying()) {
            this.BigSound.setVolume(0.8f, 0.8f);
            this.BigSound.setLooping(true);
            this.BigSound.start();
        }
    }

    public void StopBgSound() {
        if (this.BigSound != null && this.BigSound.isPlaying()) {
            this.BigSound.stop();
        }
    }

    public void play(int sound, int uLoop) {
        this.soundPool.play(this.soundPoolMap.get(Integer.valueOf(sound)).intValue(), (float) this.streamVolume, (float) this.streamVolume, 1, uLoop, 1.0f);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        setContentView((int) R.layout.entry);
        this.playButton = (ImageButton) findViewById(R.id.entry_game);
        this.dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(this.dm);
        this.width = (float) this.dm.widthPixels;
        Log.e("width", "=" + this.width);
        Log.e("width", "=" + (this.width / 480.0f));
        if (((double) this.width) == 480.0d) {
            Log.e("width", "=480.0true");
        } else {
            this.playButton.setPadding(((int) (((this.width / 480.0f) * 275.0f) - 275.0f)) + 10, 0, 0, 0);
        }
        this.playButton.setOnClickListener(this.enter_game);
        this.BigSound = MediaPlayer.create(this, (int) R.raw.snd_titlebg);
        this.BigSound.setVolume(1.0f, 1.0f);
        PlayBgSound();
        this.soundPool = new SoundPool(100, 3, 100);
        this.soundPoolMap = new HashMap<>();
        this.streamVolume = ((AudioManager) getSystemService("audio")).getStreamVolume(3);
        this.soundPoolMap.put(1, Integer.valueOf(this.soundPool.load(this, R.raw.snd_ropecatch, 1)));
        this.ad = (AdView) findViewById(R.id.ad);
        AdManager.setPublisherId("a14e1c002d54274");
        setAd();
        this.ad.setAdListener(new LunarLanderListener(this, null));
    }

    public void setAd() {
        this.ad.setVisibility(0);
        AlphaAnimation animation = new AlphaAnimation(0.0f, 1.0f);
        animation.setDuration(400);
        animation.setFillAfter(true);
        animation.setInterpolator(new AccelerateInterpolator());
        this.ad.startAnimation(animation);
    }

    private class LunarLanderListener extends SimpleAdListener {
        private LunarLanderListener() {
        }

        /* synthetic */ LunarLanderListener(EntryActivity entryActivity, LunarLanderListener lunarLanderListener) {
            this();
        }

        public void onFailedToReceiveAd(AdView adView) {
            super.onFailedToReceiveAd(adView);
            Log.d("ad", "=" + EntryActivity.this.ad);
            EntryActivity.this.ad.requestFreshAd();
            Log.d("Lunar", "onFailedToReceiveAd");
        }

        public void onFailedToReceiveRefreshedAd(AdView adView) {
            super.onFailedToReceiveRefreshedAd(adView);
            EntryActivity.this.ad.requestFreshAd();
            Log.d("Lunar", "onFailedToReceiveRefreshedAd");
        }

        public void onReceiveAd(AdView adView) {
            super.onReceiveAd(adView);
            Log.d("Lunar", "onReceiveAd");
            EntryActivity.this.setAd();
            EntryActivity.this.ad.requestFreshAd();
        }

        public void onReceiveRefreshedAd(AdView adView) {
            super.onReceiveRefreshedAd(adView);
            EntryActivity.this.setAd();
            EntryActivity.this.ad.requestFreshAd();
            Log.d("Lunar", "onReceiveRefreshedAd");
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        Log.e(getClass().getName(), "onDestroy");
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        StopBgSound();
        Log.e(getClass().getName(), "onPause");
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        Log.e(getClass().getName(), "onResume");
    }
}
