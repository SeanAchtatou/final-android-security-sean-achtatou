package com.JoneyZNinjaCatFlySky12;

import android.graphics.Bitmap;
import java.util.Date;

public class CatFallDraw {
    protected Bitmap[] bitmaps;
    protected long duration;
    protected Long lastBitmapTime;
    protected boolean repeat;
    protected int step;
    protected float x;
    protected float y;

    public CatFallDraw(float x2, float y2, Bitmap bitmap, long duration2) {
        this.x = x2;
        this.y = y2;
        this.bitmaps = new Bitmap[]{bitmap};
        this.duration = duration2;
        this.repeat = true;
        this.lastBitmapTime = null;
        this.step = 0;
    }

    public CatFallDraw(float x2, float y2, Bitmap[] bitmaps2, long duration2, boolean repeat2) {
        this.x = x2;
        this.y = y2;
        this.bitmaps = bitmaps2;
        this.duration = duration2;
        this.repeat = repeat2;
        this.lastBitmapTime = null;
        this.step = 0;
    }

    public Bitmap nextFrame() {
        if (this.step >= this.bitmaps.length) {
            if (!this.repeat) {
                return null;
            }
            this.lastBitmapTime = null;
        }
        if (this.lastBitmapTime == null) {
            this.lastBitmapTime = Long.valueOf(new Date().getTime());
            Bitmap[] bitmapArr = this.bitmaps;
            this.step = 0;
            return bitmapArr[0];
        }
        long nowTime = System.currentTimeMillis();
        if (nowTime - this.lastBitmapTime.longValue() <= this.duration) {
            return this.bitmaps[this.step];
        }
        this.lastBitmapTime = Long.valueOf(nowTime);
        Bitmap[] bitmapArr2 = this.bitmaps;
        int i = this.step;
        this.step = i + 1;
        return bitmapArr2[i];
    }

    public float getX() {
        return this.x;
    }

    public float getY() {
        return this.y;
    }
}
