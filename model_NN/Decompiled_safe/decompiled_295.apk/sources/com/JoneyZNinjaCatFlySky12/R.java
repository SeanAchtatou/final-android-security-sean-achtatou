package com.JoneyZNinjaCatFlySky12;

public final class R {

    public static final class array {
        public static final int coinString = 2131099649;
        public static final int mission = 2131099648;
    }

    public static final class attr {
        public static final int backgroundColor = 2130771969;
        public static final int backgroundTransparent = 2130771975;
        public static final int keywords = 2130771972;
        public static final int primaryTextColor = 2130771970;
        public static final int refreshInterval = 2130771973;
        public static final int secondaryTextColor = 2130771971;
        public static final int textColor = 2130771974;
        public static final int tileSize = 2130771968;
    }

    public static final class drawable {
        public static final int banner = 2130837504;
        public static final int banner_cat = 2130837505;
        public static final int bg = 2130837506;
        public static final int bg_cat = 2130837507;
        public static final int bg_game = 2130837508;
        public static final int cat1 = 2130837509;
        public static final int cat2 = 2130837510;
        public static final int cat3 = 2130837511;
        public static final int cat4 = 2130837512;
        public static final int cat5 = 2130837513;
        public static final int cat_bg = 2130837514;
        public static final int cat_bg_good = 2130837515;
        public static final int cat_bg_hand = 2130837516;
        public static final int cat_bg_lose = 2130837517;
        public static final int cat_bg_speedup = 2130837518;
        public static final int cat_body = 2130837519;
        public static final int cat_fall1 = 2130837520;
        public static final int cat_fall2 = 2130837521;
        public static final int cat_fly1 = 2130837522;
        public static final int cat_fly2 = 2130837523;
        public static final int cat_fly3 = 2130837524;
        public static final int cat_fly4 = 2130837525;
        public static final int cat_fly5 = 2130837526;
        public static final int cat_fly6 = 2130837527;
        public static final int cat_fly7 = 2130837528;
        public static final int cat_fly8 = 2130837529;
        public static final int cat_fly9 = 2130837530;
        public static final int cat_jump = 2130837531;
        public static final int cat_rote1 = 2130837532;
        public static final int cat_rote2 = 2130837533;
        public static final int cat_rote3 = 2130837534;
        public static final int cat_rote4 = 2130837535;
        public static final int cat_rote5 = 2130837536;
        public static final int cat_rote6 = 2130837537;
        public static final int cat_rote7 = 2130837538;
        public static final int cat_rote8 = 2130837539;
        public static final int coin_g = 2130837540;
        public static final int coin_s = 2130837541;
        public static final int end_bg = 2130837542;
        public static final int explote_1 = 2130837543;
        public static final int explote_2 = 2130837544;
        public static final int explote_3 = 2130837545;
        public static final int explote_4 = 2130837546;
        public static final int first_bg = 2130837547;
        public static final int first_button_play = 2130837548;
        public static final int fish1_resume = 2130837549;
        public static final int fish1_retry = 2130837550;
        public static final int fish2_retry = 2130837551;
        public static final int high_number0 = 2130837552;
        public static final int high_number1 = 2130837553;
        public static final int high_number2 = 2130837554;
        public static final int high_number3 = 2130837555;
        public static final int high_number4 = 2130837556;
        public static final int high_number5 = 2130837557;
        public static final int high_number6 = 2130837558;
        public static final int high_number7 = 2130837559;
        public static final int high_number8 = 2130837560;
        public static final int high_number9 = 2130837561;
        public static final int house1 = 2130837562;
        public static final int house2 = 2130837563;
        public static final int icon = 2130837564;
        public static final int intro = 2130837565;
        public static final int number0 = 2130837566;
        public static final int number1 = 2130837567;
        public static final int number2 = 2130837568;
        public static final int number3 = 2130837569;
        public static final int number4 = 2130837570;
        public static final int number5 = 2130837571;
        public static final int number6 = 2130837572;
        public static final int number7 = 2130837573;
        public static final int number8 = 2130837574;
        public static final int number9 = 2130837575;
        public static final int pause = 2130837576;
        public static final int pichuaizi = 2130837577;
        public static final int resume_bg = 2130837578;
        public static final int star_g1 = 2130837579;
        public static final int star_g2 = 2130837580;
        public static final int star_g3 = 2130837581;
        public static final int star_g4 = 2130837582;
        public static final int star_s1 = 2130837583;
        public static final int star_s2 = 2130837584;
        public static final int star_s3 = 2130837585;
        public static final int star_s4 = 2130837586;
        public static final int xiaoxue = 2130837587;
    }

    public static final class id {
        public static final int ad = 2131165185;
        public static final int admobad = 2131165189;
        public static final int done_game = 2131165188;
        public static final int entry_game = 2131165184;
        public static final int intro_game = 2131165192;
        public static final int intro_text = 2131165193;
        public static final int ninjaCatFlySkyView = 2131165186;
        public static final int pasue_Button = 2131165187;
        public static final int playAgagin_Button = 2131165191;
        public static final int resume_Button = 2131165190;
    }

    public static final class layout {
        public static final int entry = 2130903040;
        public static final int main = 2130903041;
    }

    public static final class raw {
        public static final int snd_allclear = 2130968576;
        public static final int snd_best = 2130968577;
        public static final int snd_btn = 2130968578;
        public static final int snd_click = 2130968579;
        public static final int snd_coinbonus = 2130968580;
        public static final int snd_fadeinout = 2130968581;
        public static final int snd_gamebg = 2130968582;
        public static final int snd_gameover = 2130968583;
        public static final int snd_goldcoin = 2130968584;
        public static final int snd_jumpdown = 2130968585;
        public static final int snd_ropecatch = 2130968586;
        public static final int snd_ropedown = 2130968587;
        public static final int snd_ropeup = 2130968588;
        public static final int snd_run = 2130968589;
        public static final int snd_shoot = 2130968590;
        public static final int snd_silvercoin = 2130968591;
        public static final int snd_speedup = 2130968592;
        public static final int snd_spin = 2130968593;
        public static final int snd_titlebg = 2130968594;
    }

    public static final class string {
        public static final int app_name = 2131034113;
        public static final int hello = 2131034112;
        public static final int intro = 2131034114;
    }

    public static final class styleable {
        public static final int[] TileView = {R.attr.tileSize};
        public static final int TileView_tileSize = 0;
        public static final int[] com_admob_android_ads_AdView = {R.attr.backgroundColor, R.attr.primaryTextColor, R.attr.secondaryTextColor, R.attr.keywords, R.attr.refreshInterval};
        public static final int com_admob_android_ads_AdView_backgroundColor = 0;
        public static final int com_admob_android_ads_AdView_keywords = 3;
        public static final int com_admob_android_ads_AdView_primaryTextColor = 1;
        public static final int com_admob_android_ads_AdView_refreshInterval = 4;
        public static final int com_admob_android_ads_AdView_secondaryTextColor = 2;
        public static final int[] net_youmi_android_AdView = {R.attr.textColor, R.attr.backgroundTransparent};
        public static final int net_youmi_android_AdView_backgroundTransparent = 1;
        public static final int net_youmi_android_AdView_textColor = 0;
    }
}
