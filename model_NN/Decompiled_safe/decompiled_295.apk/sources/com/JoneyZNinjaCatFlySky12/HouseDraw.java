package com.JoneyZNinjaCatFlySky12;

import android.graphics.Bitmap;
import java.util.Date;

public class HouseDraw {
    protected Bitmap[] bitmaps;
    protected long duration;
    protected int house_type;
    protected Long lastBitmapTime;
    protected boolean repeat;
    protected Bitmap singleBitmaps;
    protected int step;
    protected float x;
    protected float y;

    public HouseDraw(float x2, float y2, Bitmap bitmap, int type, long duration2) {
        this.x = x2;
        this.y = y2;
        this.singleBitmaps = bitmap;
        this.repeat = true;
        this.lastBitmapTime = null;
        this.duration = duration2;
        this.house_type = type;
        this.step = 0;
    }

    public boolean nextMove() {
        if (this.lastBitmapTime == null) {
            this.lastBitmapTime = Long.valueOf(new Date().getTime());
            return false;
        }
        long nowTime = System.currentTimeMillis();
        if (nowTime - this.lastBitmapTime.longValue() <= this.duration) {
            return false;
        }
        this.lastBitmapTime = Long.valueOf(nowTime);
        this.x -= 4.0f;
        return true;
    }

    public HouseDraw(float x2, float y2, Bitmap[] bitmaps2, long duration2, boolean repeat2) {
        this.x = x2;
        this.y = y2;
        this.bitmaps = bitmaps2;
        this.duration = duration2;
        this.repeat = repeat2;
        this.lastBitmapTime = null;
        this.step = 0;
    }

    public Bitmap nextFrame() {
        if (this.step >= this.bitmaps.length) {
            if (!this.repeat) {
                return null;
            }
            this.lastBitmapTime = null;
        }
        if (this.lastBitmapTime == null) {
            this.lastBitmapTime = Long.valueOf(new Date().getTime());
            Bitmap[] bitmapArr = this.bitmaps;
            this.step = 0;
            return bitmapArr[0];
        }
        long nowTime = System.currentTimeMillis();
        if (nowTime - this.lastBitmapTime.longValue() <= this.duration) {
            return this.bitmaps[this.step];
        }
        this.lastBitmapTime = Long.valueOf(nowTime);
        Bitmap[] bitmapArr2 = this.bitmaps;
        int i = this.step;
        this.step = i + 1;
        return bitmapArr2[i];
    }

    public float getX() {
        return this.x;
    }

    public float getY() {
        return this.y;
    }

    public Bitmap[] getBitmaps() {
        return this.bitmaps;
    }

    public void setBitmaps(Bitmap[] bitmaps2) {
        this.bitmaps = bitmaps2;
    }

    public void setX(float x2) {
        this.x = x2;
    }

    public void setY(float y2) {
        this.y = y2;
    }

    public Bitmap getSingleBitmaps() {
        return this.singleBitmaps;
    }

    public void setSingleBitmaps(Bitmap singleBitmaps2) {
        this.singleBitmaps = singleBitmaps2;
    }
}
