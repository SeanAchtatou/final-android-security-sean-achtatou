package org.onepointzero.hexacon;

import java.util.ArrayList;
import java.util.Random;
import java.util.Stack;

public class GameData {
    private static GameData INSTANCE = new GameData();
    public static final int MAX_UNITS = 72;
    private ArrayList<Cell> data;
    private int points;

    public GameData() {
        this.data = null;
        this.points = 0;
        this.data = new ArrayList<>();
        for (int i = 0; i < 72; i++) {
            this.data.add(new Cell(i));
        }
        connectCells();
    }

    public void initGame() {
        emptyData();
        this.points = 0;
        for (int count = 0; count < 7; count++) {
            addCell();
        }
    }

    public int getEmptyCells() {
        int ret = 0;
        for (int i = 0; i < 72; i++) {
            if (this.data.get(i).isEmpty()) {
                ret++;
            }
        }
        return ret;
    }

    public boolean isComplete() {
        for (int i = 0; i < 72; i++) {
            if (this.data.get(i).isEmpty()) {
                return false;
            }
        }
        return true;
    }

    public boolean isCellEmpty(int i) {
        if (i >= 72 || i == -1) {
            return false;
        }
        return this.data.get(i).isEmpty();
    }

    public int addCell() {
        return addCell(new Random().nextInt(6) + 1);
    }

    public int addCell(int color) {
        Random rnd = new Random();
        boolean isCreated = false;
        if (isComplete()) {
            return -1;
        }
        int newId = -1;
        while (!isCreated) {
            newId = rnd.nextInt(72);
            Cell c = this.data.get(newId);
            if (c.isEmpty()) {
                c.setColor(color);
                isCreated = true;
                this.data.set(newId, c);
            }
        }
        return newId;
    }

    public void emptyData() {
        for (int i = 0; i < this.data.size(); i++) {
            Cell c = this.data.get(i);
            c.empty();
            this.data.set(i, c);
        }
    }

    public int getStatus(int id) {
        Cell c = this.data.get(id);
        int status = c.getStatus();
        if (status > 0) {
            c.setStatus(status - 1);
            this.data.set(id, c);
        }
        return c.getStatus();
    }

    public void resetCell(int id) {
        Cell c = this.data.get(id);
        int status = c.getStatus();
        c.setStatus(10);
        c.setCountdown(-1);
        c.setColor(0);
        this.data.set(id, c);
    }

    public void resetStatus(int id) {
        Cell c = this.data.get(id);
        int status = c.getStatus();
        c.setStatus(10);
        this.data.set(id, c);
    }

    public static GameData getInstance() {
        return INSTANCE;
    }

    public ArrayList<Cell> getData() {
        return this.data;
    }

    public void setData(ArrayList<Cell> data2) {
        this.data = data2;
    }

    public int getPoints() {
        return this.points;
    }

    public void resetPoints() {
        this.points = 0;
    }

    public void connectCells() {
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        int i7;
        int i8;
        int i9;
        int i10;
        int i11;
        for (int i12 = 0; i12 < 72; i12++) {
            Cell c = this.data.get(i12);
            int[] connections = new int[6];
            if ((i12 / 4) % 2 == 0) {
                if (i12 % 8 != 0) {
                    if (i12 - 5 >= 0) {
                        i10 = i12 - 5;
                    } else {
                        i10 = -1;
                    }
                    connections[0] = i10;
                    if (i12 + 3 < 72) {
                        i11 = i12 + 3;
                    } else {
                        i11 = -1;
                    }
                    connections[5] = i11;
                } else {
                    connections[0] = -1;
                    connections[5] = -1;
                }
                if (i12 - 8 >= 0) {
                    i6 = i12 - 8;
                } else {
                    i6 = -1;
                }
                connections[1] = i6;
                if (i12 - 4 >= 0) {
                    i7 = i12 - 4;
                } else {
                    i7 = -1;
                }
                connections[2] = i7;
                if (i12 + 4 < 72) {
                    i8 = i12 + 4;
                } else {
                    i8 = -1;
                }
                connections[3] = i8;
                if (i12 + 8 < 72) {
                    i9 = i12 + 8;
                } else {
                    i9 = -1;
                }
                connections[4] = i9;
            } else {
                connections[0] = i12 - 4 >= 0 ? i12 - 4 : -1;
                if (i12 - 8 >= 0) {
                    i = i12 - 8;
                } else {
                    i = -1;
                }
                connections[1] = i;
                if ((i12 + 1) % 8 != 0) {
                    if (i12 - 3 >= 0) {
                        i4 = i12 - 3;
                    } else {
                        i4 = -1;
                    }
                    connections[2] = i4;
                    if (i12 + 5 < 72) {
                        i5 = i12 + 5;
                    } else {
                        i5 = -1;
                    }
                    connections[3] = i5;
                } else {
                    connections[2] = -1;
                    connections[3] = -1;
                }
                if (i12 + 8 < 72) {
                    i2 = i12 + 8;
                } else {
                    i2 = -1;
                }
                connections[4] = i2;
                if (i12 + 4 < 72) {
                    i3 = i12 + 4;
                } else {
                    i3 = -1;
                }
                connections[5] = i3;
            }
            c.setConnections(connections);
            this.data.set(i12, c);
        }
    }

    public void refreshGame() {
        int countDownCell = -1;
        int i = 0;
        while (true) {
            if (i >= 72) {
                break;
            } else if (this.data.get(i).getCountdown() > -1) {
                countDownCell = i;
                break;
            } else {
                i++;
            }
        }
        if (countDownCell != -1) {
            Cell c = this.data.get(countDownCell);
            if (c.getCountdown() - 1 > 0) {
                c.setCountdown(c.getCountdown() - 1);
            } else if (c.getStatus() == 10) {
                c.setCountdown(-1);
                c.setColor(7);
            }
            this.data.set(countDownCell, c);
        } else if (this.points > -1) {
            Random rnd = new Random();
            while (0 == 0) {
                int _cId = rnd.nextInt(72);
                Cell c2 = this.data.get(_cId);
                if (!c2.isEmpty() && !c2.isBlocked()) {
                    c2.setCountdown(8);
                    this.data.set(_cId, c2);
                    return;
                }
            }
        }
    }

    public Stack<Integer> checkGoal(int item) {
        return checkGoal(item, false);
    }

    public Stack<Integer> checkGoal(int item, boolean autoInsert) {
        Stack<Integer> items = new Stack<>();
        _checkGoal(items, item);
        if (items.size() <= 5) {
            return new Stack<>();
        }
        if (!autoInsert) {
            this.points += items.size();
        }
        for (int i = 0; i < items.size(); i++) {
            int _id = items.get(i).intValue();
            Cell _c = this.data.get(_id);
            _c.setCountdown(-1);
            this.data.set(_id, _c);
        }
        return items;
    }

    public void _checkGoal(Stack items, int item) {
        Cell c = this.data.get(item);
        int[] connections = c.getConnections();
        for (int i = 0; i < 6; i++) {
            if (connections[i] != -1) {
                Cell _c = this.data.get(connections[i]);
                if (!_c.isBlocked() && _c.getColor() == c.getColor() && items.indexOf(Integer.valueOf(connections[i])) == -1) {
                    items.push(Integer.valueOf(connections[i]));
                    _checkGoal(items, connections[i]);
                }
            }
        }
    }
}
