package org.onepointzero.hexacon;

public final class R {

    public static final class anim {
        public static final int flipinnext = 2130968576;
        public static final int flipinprev = 2130968577;
        public static final int flipoutnext = 2130968578;
        public static final int flipoutprev = 2130968579;
    }

    public static final class attr {
    }

    public static final class drawable {
        public static final int app_background = 2130837504;
        public static final int btn_main_normal = 2130837505;
        public static final int btn_main_play = 2130837506;
        public static final int btn_main_selected = 2130837507;
        public static final int btn_settings = 2130837508;
        public static final int cell_empty = 2130837509;
        public static final int fill_blue = 2130837510;
        public static final int fill_green = 2130837511;
        public static final int fill_orange = 2130837512;
        public static final int fill_red = 2130837513;
        public static final int fill_rock = 2130837514;
        public static final int fill_white = 2130837515;
        public static final int fill_yellow = 2130837516;
        public static final int game_background = 2130837517;
        public static final int help1 = 2130837518;
        public static final int help2 = 2130837519;
        public static final int help3 = 2130837520;
        public static final int help4 = 2130837521;
        public static final int help5 = 2130837522;
        public static final int hexagon = 2130837523;
        public static final int hexagon_selected = 2130837524;
        public static final int icon = 2130837525;
        public static final int logotopbar = 2130837526;
        public static final int main_logo = 2130837527;
        public static final int separator = 2130837528;
        public static final int settings = 2130837529;
        public static final int topbar = 2130837530;
        public static final int topbar_background = 2130837531;
    }

    public static final class id {
        public static final int btn_credits = 2131165206;
        public static final int exit = 2131165199;
        public static final int exitapp = 2131165207;
        public static final int gamePanel = 2131165193;
        public static final int gameSettings = 2131165189;
        public static final int gameover = 2131165195;
        public static final int header = 2131165186;
        public static final int help_flipper = 2131165200;
        public static final int howtoplay = 2131165205;
        public static final int labelScore = 2131165190;
        public static final int label_newrecord = 2131165196;
        public static final int labelrecord = 2131165192;
        public static final int layout_root = 2131165184;
        public static final int logo = 2131165187;
        public static final int mainlogo = 2131165203;
        public static final int newGame = 2131165198;
        public static final int next = 2131165201;
        public static final int options = 2131165194;
        public static final int pager = 2131165202;
        public static final int play = 2131165204;
        public static final int points = 2131165188;
        public static final int record = 2131165191;
        public static final int resume = 2131165197;
        public static final int survival = 2131165208;
        public static final int text = 2131165185;
        public static final int timeattack = 2131165209;
    }

    public static final class layout {
        public static final int credits_dialog = 2130903040;
        public static final int game = 2130903041;
        public static final int help = 2130903042;
        public static final int main = 2130903043;
    }

    public static final class string {
        public static final int app_name = 2131099648;
        public static final int btn_back = 2131099662;
        public static final int btn_credits = 2131099660;
        public static final int btn_exit = 2131099651;
        public static final int btn_exit_game = 2131099654;
        public static final int btn_help_next = 2131099669;
        public static final int btn_help_return = 2131099670;
        public static final int btn_howto = 2131099652;
        public static final int btn_new_game = 2131099657;
        public static final int btn_play = 2131099649;
        public static final int btn_play_survival = 2131099658;
        public static final int btn_play_timeattack = 2131099659;
        public static final int btn_resume = 2131099650;
        public static final int credits_label = 2131099661;
        public static final int credits_text = 2131099663;
        public static final int helptext1 = 2131099664;
        public static final int helptext2 = 2131099665;
        public static final int helptext3 = 2131099666;
        public static final int helptext4 = 2131099667;
        public static final int helptext5 = 2131099668;
        public static final int label_game_over = 2131099656;
        public static final int label_record = 2131099655;
        public static final int label_score = 2131099653;
    }

    public static final class style {
        public static final int btn_commons = 2131034112;
        public static final int btn_help = 2131034115;
        public static final int btn_play = 2131034113;
        public static final int help_text = 2131034114;
    }
}
