package org.onepointzero.hexacon;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.ViewFlipper;

public class HexaConActivity extends Activity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.main);
        ViewFlipper viewFlipper = (ViewFlipper) findViewById(R.id.help_flipper);
    }

    public void onClickPlay(View v) {
        ViewFlipper vf = (ViewFlipper) findViewById(R.id.help_flipper);
        vf.setInAnimation(AnimationUtils.loadAnimation(this, R.anim.flipinnext));
        vf.setOutAnimation(AnimationUtils.loadAnimation(this, R.anim.flipoutnext));
        vf.showNext();
    }

    public void onClickPlaySurvival(View v) {
        GameData.getInstance().initGame();
        Intent contentIntent = new Intent(this, GameActivity.class);
        contentIntent.putExtra("mode", 0);
        startActivityForResult(contentIntent, 0);
    }

    public void onClickPlayTimeAttack(View v) {
        GameData.getInstance().initGame();
        Intent contentIntent = new Intent(this, GameActivity.class);
        contentIntent.putExtra("mode", 1);
        startActivityForResult(contentIntent, 0);
    }

    public void onClickBackToMain(View v) {
        ViewFlipper vf = (ViewFlipper) findViewById(R.id.help_flipper);
        vf.setInAnimation(AnimationUtils.loadAnimation(this, R.anim.flipinprev));
        vf.setOutAnimation(AnimationUtils.loadAnimation(this, R.anim.flipoutprev));
        vf.showPrevious();
    }

    public void onClickExit(View v) {
        finish();
    }

    public void onClickHowToPlay(View v) {
        startActivityForResult(new Intent(this, HelpActivity.class), 0);
    }

    public void onClickShowCredits(View v) {
        Dialog dialog = new Dialog(this);
        dialog.setContentView((int) R.layout.credits_dialog);
        dialog.setTitle(getString(R.string.credits_label));
        dialog.setCancelable(true);
        dialog.show();
    }
}
