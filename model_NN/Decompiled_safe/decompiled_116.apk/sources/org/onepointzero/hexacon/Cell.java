package org.onepointzero.hexacon;

class Cell {
    private int color = 0;
    private int[] connections = null;
    private int countdown = -1;
    private int id = -1;
    private int status = 10;

    public Cell(int id2) {
        this.id = id2;
    }

    public int getId() {
        return this.id;
    }

    public int getColor() {
        return this.color;
    }

    public void setColor(int color2) {
        this.color = color2;
    }

    public boolean isEmpty() {
        return this.color == 0;
    }

    public boolean isBlocked() {
        return this.color == 7;
    }

    public void empty() {
        this.color = 0;
        this.countdown = -1;
    }

    public void setConnections(int[] connections2) {
        this.connections = connections2;
    }

    public int[] getConnections() {
        return this.connections;
    }

    public int getStatus() {
        return this.status;
    }

    public void setStatus(int status2) {
        this.status = status2;
    }

    public void setCountdown(int value) {
        this.countdown = value;
    }

    public int getCountdown() {
        return this.countdown;
    }
}
