package org.onepointzero.hexacon;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Stack;

class GamePath {
    GamePath() {
    }

    public Stack<Integer> getPath(int src, int dst) {
        Stack<Integer> result = new Stack<>();
        ArrayList<Integer> visited = new ArrayList<>();
        HashMap<Integer, Integer> parent = new HashMap<>();
        HashMap<Integer, Integer> distance = new HashMap<>();
        GameData gameData = GameData.getInstance();
        ArrayList<Cell> data = gameData.getData();
        for (int k = 0; k < 72; k++) {
            parent.put(Integer.valueOf(k), -1);
            distance.put(Integer.valueOf(k), 9999);
        }
        ArrayList<Integer> queue = new ArrayList<>();
        queue.add(Integer.valueOf(src));
        visited.add(Integer.valueOf(src));
        distance.put(Integer.valueOf(src), null);
        while (queue.size() > 0) {
            Integer i = (Integer) queue.get(0);
            queue.remove(0);
            if (i.intValue() == dst) {
                Stack<Integer> _aux = new Stack<>();
                for (int _node = dst; _node != src; _node = ((Integer) parent.get(Integer.valueOf(_node))).intValue()) {
                    _aux.push(Integer.valueOf(_node));
                }
                _aux.push(Integer.valueOf(src));
                while (true) {
                    try {
                        result.push(_aux.pop());
                    } catch (Exception e) {
                    }
                }
            } else {
                int[] cellConnections = data.get(i.intValue()).getConnections();
                for (int j = 0; j < cellConnections.length; j++) {
                    if (cellConnections[j] != -1 && visited.indexOf(Integer.valueOf(cellConnections[j])) == -1 && gameData.isCellEmpty(cellConnections[j])) {
                        int _distance = ((Integer) distance.get(i)).intValue() + 1;
                        if (((Integer) distance.get(Integer.valueOf(cellConnections[j]))).intValue() > _distance) {
                            distance.put(Integer.valueOf(cellConnections[j]), Integer.valueOf(_distance));
                            parent.put(Integer.valueOf(cellConnections[j]), i);
                            queue.add(Integer.valueOf(cellConnections[j]));
                        }
                    }
                }
                visited.add(i);
            }
        }
        return result;
    }
}
