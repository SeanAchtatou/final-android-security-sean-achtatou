package org.onepointzero.hexacon;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Random;
import java.util.Stack;

public class GameView extends View {
    public static int HEXAGONSIZE = 66;
    public static final int MAX_TIME = 60;
    public static final int MODE_SURVIVAL = 0;
    public static final int MODE_TIMEATTACK = 1;
    public static final int STATUS_PAUSE = 1;
    public static final int STATUS_PLAY = 0;
    private ArrayList<Bitmap> colorCells = null;
    private int gameMode = 0;
    ArrayList<Integer> goals = null;
    Bitmap hexagon;
    Bitmap hexagonEmpty;
    Bitmap hexagonSelected;
    private Listener listener;
    /* access modifiers changed from: private */
    public Handler mHandler = new Handler();
    /* access modifiers changed from: private */
    public Runnable mUpdateTimeTask = new Runnable() {
        public void run() {
            GameView.this.mHandler.postDelayed(GameView.this.mUpdateTimeTask, 500);
            int initX = GameView.this.getWidth() - 100;
            int initY = GameView.this.getHeight() - 100;
            GameView.this.postInvalidate(initX, initY, initX + 100, initY + 100);
        }
    };
    Stack<Integer> nextItems = null;
    /* access modifiers changed from: private */
    public int offsetX = 30;
    /* access modifiers changed from: private */
    public int offsetY = 45;
    private Paint paint;
    Stack<Integer> pathQueue = null;
    private int predictionMargin = 15;
    Bitmap[][] reducedHexagons = null;
    int selectedItem = -1;
    long startTime = 0;
    int status = 0;

    public interface Listener {
        void gameOver();

        void setPoints(String str);
    }

    public GameView(Context context) {
        super(context);
    }

    public GameView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public GameView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void setGameMode(int mode) {
        this.gameMode = mode;
    }

    public void setListener(Listener listener2) {
        this.listener = listener2;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        int lineOffset;
        boolean invalidate = false;
        GameData gameData = GameData.getInstance();
        ArrayList<Cell> data = gameData.getData();
        if (gameData.isComplete()) {
            this.listener.gameOver();
            return;
        }
        int itemWidth = this.hexagon.getWidth();
        int itemHeight = this.hexagon.getHeight();
        int _selectedX = -1;
        int _selectedY = -1;
        if (this.goals.size() > 5) {
            boolean clearGoals = false;
            for (int i = 0; i < this.goals.size(); i++) {
                Cell c = data.get(this.goals.get(i).intValue());
                if (gameData.getStatus(c.getId()) == 0) {
                    c.empty();
                    data.set(c.getId(), c);
                    clearGoals = true;
                }
            }
            gameData.setData(data);
            if (clearGoals) {
                this.goals.clear();
            }
        }
        if (this.pathQueue.size() > 1) {
            Cell cellSrc = data.get(this.pathQueue.get(0).intValue());
            Cell cellDst = data.get(this.pathQueue.get(1).intValue());
            cellDst.setColor(cellSrc.getColor());
            cellDst.setCountdown(cellSrc.getCountdown());
            cellSrc.empty();
            data.set(this.pathQueue.get(0).intValue(), cellSrc);
            data.set(this.pathQueue.get(1).intValue(), cellDst);
            gameData.setData(data);
            this.selectedItem = -1;
            this.pathQueue.remove(0);
            invalidate = true;
        }
        if (this.pathQueue.size() == 1) {
            this.goals.addAll(gameData.checkGoal(this.pathQueue.get(0).intValue()));
            this.pathQueue.clear();
            if (this.goals.size() < 6) {
                int _nextItemsSize = this.nextItems.size();
                int i2 = 0;
                while (i2 < _nextItemsSize) {
                    if (gameData.isComplete()) {
                        this.listener.gameOver();
                        return;
                    } else {
                        gameData.addCell(this.nextItems.pop().intValue());
                        i2++;
                    }
                }
                _fillNextItemsQueue();
            }
            gameData.refreshGame();
        }
        for (int i3 = 0; i3 < 72; i3++) {
            int cellSpace = (itemWidth * 185) / 390;
            if ((i3 / 4) % 2 == 0) {
                lineOffset = 0;
            } else {
                lineOffset = (itemWidth / 4) * 3;
            }
            int x = ((i3 % 4) * (itemWidth + cellSpace)) + lineOffset + this.offsetX;
            int y = ((i3 / 4) * (itemHeight / 2)) + this.offsetY;
            if (this.selectedItem == i3) {
                _selectedX = x;
                _selectedY = y;
            }
            boolean onRemove = false;
            Cell c2 = data.get(i3);
            canvas.drawBitmap(this.hexagonEmpty, (float) x, (float) y, this.paint);
            if (!c2.isEmpty()) {
                Bitmap colorCell = this.colorCells.get(c2.getColor() - 1);
                if (c2.getStatus() < 10) {
                    Bitmap reducedHexagon = _getReducedBitmapHexagon(c2.getColor() - 1, c2.getStatus());
                    canvas.drawBitmap(reducedHexagon, (float) (x + ((colorCell.getWidth() - reducedHexagon.getWidth()) / 2)), (float) (y + ((colorCell.getHeight() - reducedHexagon.getHeight()) / 2)), this.paint);
                    onRemove = true;
                    if (c2.getStatus() == 0) {
                        gameData.resetStatus(c2.getId());
                    }
                    if (this.goals.size() == 0) {
                        gameData.resetCell(c2.getId());
                    }
                } else {
                    canvas.drawBitmap(colorCell, (float) x, (float) y, this.paint);
                    if (c2.getCountdown() > -1) {
                        Paint _paint = new Paint();
                        _paint.setColor(-16777216);
                        canvas.drawRect((float) (x + 27), (float) (y + 18), (float) (x + 42), (float) (y + 35), _paint);
                        _paint.setColor(-1);
                        canvas.drawText(String.valueOf(c2.getCountdown()), (float) (x + 31), (float) (y + 31), _paint);
                    }
                }
            } else if (c2.getStatus() < 10) {
                gameData.resetCell(c2.getId());
            }
            if (onRemove) {
                invalidate = true;
            }
            canvas.drawBitmap(this.hexagon, (float) x, (float) y, this.paint);
        }
        if (!(_selectedX == -1 || _selectedY == -1)) {
            canvas.drawBitmap(this.hexagonSelected, (float) _selectedX, (float) _selectedY, this.paint);
        }
        for (int i4 = 0; i4 < this.nextItems.size(); i4++) {
            Bitmap hexagonPrediction = _getReducedBitmapHexagon(this.nextItems.get(i4).intValue() - 1, 5);
            canvas.drawBitmap(hexagonPrediction, (float) (((hexagonPrediction.getWidth() + 5) * i4) + 20), (float) (getHeight() - (hexagonPrediction.getHeight() + this.predictionMargin)), this.paint);
        }
        this.listener.setPoints(String.valueOf(gameData.getPoints()));
        if (this.gameMode == 1) {
            Paint _paint2 = new Paint();
            _paint2.setColor(1711276287);
            canvas.drawRect((float) (getWidth() - 80), (float) (getHeight() - 60), (float) getWidth(), (float) getHeight(), _paint2);
            _paint2.setColor(-1);
            _paint2.setTextSize(28.0f);
            long _diff = 60 - ((Calendar.getInstance().getTimeInMillis() - this.startTime) / 1000);
            if (_diff <= 0) {
                this.listener.gameOver();
                this.mHandler.removeCallbacks(this.mUpdateTimeTask);
                _diff = 0;
            }
            canvas.drawText(Long.toString(_diff), (float) (getWidth() - 60), (float) (getHeight() - 20), _paint2);
        }
        if (invalidate) {
            try {
                Thread.sleep(5);
            } catch (Exception e) {
                e.printStackTrace();
            }
            postInvalidate();
        }
    }

    private void _fillNextItemsQueue() {
        GameData gameData = GameData.getInstance();
        int points = gameData.getPoints();
        int emptyCells = gameData.getEmptyCells();
        if (points < 50 || emptyCells < 20) {
            _fillNextItemsQueue(3);
        } else if (emptyCells <= 30) {
            _fillNextItemsQueue(4);
        } else if (points < 100) {
            _fillNextItemsQueue(4);
        } else {
            _fillNextItemsQueue(5);
        }
    }

    private void _fillNextItemsQueue(int maxItems) {
        Random rnd = new Random();
        for (int i = 0; i < maxItems; i++) {
            this.nextItems.push(Integer.valueOf(rnd.nextInt(6) + 1));
        }
    }

    public Bitmap _getReducedBitmapHexagon(int color, int status2) {
        return this.reducedHexagons[color][status2];
    }

    public void init() {
        this.startTime = Calendar.getInstance().getTimeInMillis();
        if (this.gameMode == 1) {
            this.mHandler.removeCallbacks(this.mUpdateTimeTask);
            this.mHandler.postDelayed(this.mUpdateTimeTask, 100);
        }
        setWillNotDraw(false);
        this.paint = new Paint();
        if (getWidth() <= 320) {
            HEXAGONSIZE = 36;
            this.offsetX = 20;
            this.offsetY = 10;
            this.predictionMargin = 2;
        }
        if (getWidth() >= 800) {
            HEXAGONSIZE = 110;
        }
        this.nextItems = new Stack<>();
        this.pathQueue = new Stack<>();
        this.goals = new ArrayList<>();
        _fillNextItemsQueue(3);
        this.hexagon = BitmapFactory.decodeResource(getResources(), R.drawable.hexagon);
        this.hexagonSelected = BitmapFactory.decodeResource(getResources(), R.drawable.hexagon_selected);
        this.hexagonEmpty = BitmapFactory.decodeResource(getResources(), R.drawable.cell_empty);
        Bitmap hexagonRed = BitmapFactory.decodeResource(getResources(), R.drawable.fill_red);
        Bitmap hexagonGreen = BitmapFactory.decodeResource(getResources(), R.drawable.fill_green);
        Bitmap hexagonYellow = BitmapFactory.decodeResource(getResources(), R.drawable.fill_yellow);
        Bitmap hexagonBlue = BitmapFactory.decodeResource(getResources(), R.drawable.fill_blue);
        Bitmap hexagonOrange = BitmapFactory.decodeResource(getResources(), R.drawable.fill_orange);
        Bitmap hexagonWhite = BitmapFactory.decodeResource(getResources(), R.drawable.fill_white);
        Bitmap hexagonRock = BitmapFactory.decodeResource(getResources(), R.drawable.fill_rock);
        this.colorCells = new ArrayList<>();
        this.colorCells.add(hexagonRed);
        this.colorCells.add(hexagonGreen);
        this.colorCells.add(hexagonYellow);
        this.colorCells.add(hexagonBlue);
        this.colorCells.add(hexagonOrange);
        this.colorCells.add(hexagonWhite);
        this.colorCells.add(hexagonRock);
        this.reducedHexagons = (Bitmap[][]) Array.newInstance(Bitmap.class, 6, 10);
        for (int i = 0; i < 6; i++) {
            Bitmap colorCell = this.colorCells.get(i);
            int j = 0;
            while (j < 10) {
                int _cellSize = j > 0 ? (HEXAGONSIZE * j) / 10 : 1;
                if (_cellSize < 1) {
                    _cellSize = 1;
                }
                this.reducedHexagons[i][j] = Bitmap.createScaledBitmap(colorCell, _cellSize, _cellSize, true);
                j++;
            }
        }
        GameData instance = GameData.getInstance();
        setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if (!GameView.this.isOnPlay()) {
                    return true;
                }
                int posX = (int) event.getX();
                int posY = (int) event.getY();
                Bitmap hexagon = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.hexagon);
                int itemWidth = hexagon.getWidth();
                int itemHeight = hexagon.getHeight();
                int _selectedItem = -1;
                int i = 0;
                while (true) {
                    if (i < 72) {
                        int x = ((i % 4) * ((itemWidth / 2) + itemWidth)) + ((i / 4) % 2 == 0 ? 0 : ((itemWidth / 4) * 3) + 1) + GameView.this.offsetX;
                        int y = ((i / 4) * (itemHeight / 2)) + GameView.this.offsetY;
                        if (posX > x && posX < x + itemWidth && posY > y && posY < y + itemHeight) {
                            _selectedItem = i;
                            break;
                        }
                        i++;
                    } else {
                        break;
                    }
                }
                GameData data = GameData.getInstance();
                if (_selectedItem != -1) {
                    Cell c = data.getData().get(_selectedItem);
                    if (c.isBlocked()) {
                        return false;
                    }
                    if (c.isEmpty() && GameView.this.selectedItem == -1) {
                        _selectedItem = -1;
                    }
                }
                if (GameView.this.selectedItem == -1 || _selectedItem == -1) {
                    GameView.this.selectedItem = _selectedItem;
                    GameView.this.invalidate();
                } else {
                    GameView.this.moveCell(GameView.this.selectedItem, _selectedItem);
                }
                return false;
            }
        });
    }

    public boolean moveCell(int src, int dst) {
        ArrayList<Cell> data = GameData.getInstance().getData();
        Cell cell = data.get(src);
        Cell cellDst = data.get(dst);
        if (!cellDst.isEmpty()) {
            this.selectedItem = cellDst.getId();
            invalidate();
            return false;
        } else if (!getPath(src, dst)) {
            return false;
        } else {
            invalidate();
            return true;
        }
    }

    public boolean getPath(int src, int dst) {
        this.pathQueue = new GamePath().getPath(src, dst);
        if (this.pathQueue.size() > 0) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int w, int h, int oldw, int oldh) {
        init();
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        this.mHandler.removeCallbacks(this.mUpdateTimeTask);
    }

    public void stopCounter() {
        this.mHandler.removeCallbacks(this.mUpdateTimeTask);
    }

    public void resetGame() {
        this.startTime = Calendar.getInstance().getTimeInMillis();
        this.nextItems.clear();
        _fillNextItemsQueue(3);
        this.selectedItem = -1;
        if (this.gameMode == 1) {
            this.mHandler.removeCallbacks(this.mUpdateTimeTask);
            this.mHandler.postDelayed(this.mUpdateTimeTask, 100);
        }
        GameData.getInstance().initGame();
        invalidate();
    }

    public void setStatus(int status2) {
        this.status = status2;
    }

    public boolean isOnPlay() {
        return this.status == 0;
    }
}
