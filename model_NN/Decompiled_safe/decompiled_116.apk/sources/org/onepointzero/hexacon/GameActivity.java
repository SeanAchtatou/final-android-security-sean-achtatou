package org.onepointzero.hexacon;

import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import org.onepointzero.hexacon.GameView;

public class GameActivity extends Activity {
    int gameMode = 0;
    boolean isGameOver = false;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.game);
        this.isGameOver = false;
        SharedPreferences settings = getSharedPreferences("hexaconPreferences", 0);
        this.gameMode = getIntent().getExtras().getInt("mode");
        ((TextView) findViewById(R.id.record)).setText(settings.getString("record_" + String.valueOf(this.gameMode), "0"));
        GameView panel = (GameView) findViewById(R.id.gamePanel);
        panel.setGameMode(this.gameMode);
        panel.setListener(new GameView.Listener() {
            public void setPoints(String moreInfo) {
                ((TextView) GameActivity.this.findViewById(R.id.points)).setText(moreInfo);
            }

            public void gameOver() {
                if (!GameActivity.this.isGameOver) {
                    GameActivity.this.isGameOver = true;
                    ((GameView) GameActivity.this.findViewById(R.id.gamePanel)).setStatus(1);
                    ((RelativeLayout) GameActivity.this.findViewById(R.id.options)).setVisibility(0);
                    ((TextView) GameActivity.this.findViewById(R.id.gameover)).setVisibility(0);
                    TextView txtRecord = (TextView) GameActivity.this.findViewById(R.id.label_newrecord);
                    txtRecord.setVisibility(8);
                    ((Button) GameActivity.this.findViewById(R.id.resume)).setVisibility(8);
                    Button btnNewGame = (Button) GameActivity.this.findViewById(R.id.newGame);
                    btnNewGame.setVisibility(8);
                    ((RelativeLayout.LayoutParams) btnNewGame.getLayoutParams()).addRule(3, R.id.gameover);
                    int _record = Integer.parseInt(((TextView) GameActivity.this.findViewById(R.id.points)).getText().toString());
                    SharedPreferences settings = GameActivity.this.getSharedPreferences("hexaconPreferences", 0);
                    TextView record = (TextView) GameActivity.this.findViewById(R.id.record);
                    if (_record > Integer.parseInt(settings.getString("record_" + String.valueOf(GameActivity.this.gameMode), "0").toString())) {
                        SharedPreferences.Editor editor = settings.edit();
                        editor.putString("record_" + String.valueOf(GameActivity.this.gameMode), String.valueOf(_record));
                        editor.commit();
                        txtRecord.setVisibility(0);
                        txtRecord.setText("new record: " + String.valueOf(_record));
                        txtRecord.setTextColor(Color.parseColor("#FFFF0000"));
                        record.setText(String.valueOf(_record));
                    } else {
                        txtRecord.setVisibility(0);
                        txtRecord.setTextColor(Color.parseColor("#FFFFFFFF"));
                        txtRecord.setText("score: " + String.valueOf(_record));
                    }
                    try {
                        btnNewGame.setVisibility(0);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        GameView panel = (GameView) findViewById(R.id.gamePanel);
        panel.resetGame();
        panel.stopCounter();
        super.onDestroy();
    }

    public void onClickPause(View v) {
        ((TextView) findViewById(R.id.gameover)).setVisibility(8);
        ((TextView) findViewById(R.id.label_newrecord)).setVisibility(8);
        ((Button) findViewById(R.id.resume)).setVisibility(0);
        ((RelativeLayout.LayoutParams) ((Button) findViewById(R.id.newGame)).getLayoutParams()).addRule(3, R.id.resume);
        ((GameView) findViewById(R.id.gamePanel)).setStatus(1);
        ((RelativeLayout) findViewById(R.id.options)).setVisibility(0);
    }

    public void onClickResume(View v) {
        ((GameView) findViewById(R.id.gamePanel)).setStatus(0);
        ((RelativeLayout) findViewById(R.id.options)).setVisibility(8);
    }

    public void onClickNewGame(View v) {
        this.isGameOver = false;
        GameView panel = (GameView) findViewById(R.id.gamePanel);
        panel.setStatus(0);
        panel.resetGame();
        ((RelativeLayout) findViewById(R.id.options)).setVisibility(8);
    }

    public void onClickExit(View v) {
        finish();
    }
}
