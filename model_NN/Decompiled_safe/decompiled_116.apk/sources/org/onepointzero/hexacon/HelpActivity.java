package org.onepointzero.hexacon;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.TextView;
import android.widget.ViewFlipper;

public class HelpActivity extends Activity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.help);
    }

    public void onClickExit(View v) {
        finish();
    }

    public void onClickNext(View v) {
        ViewFlipper vf = (ViewFlipper) findViewById(R.id.help_flipper);
        vf.setInAnimation(AnimationUtils.loadAnimation(this, R.anim.flipinnext));
        vf.setOutAnimation(AnimationUtils.loadAnimation(this, R.anim.flipoutnext));
        vf.showNext();
        ((TextView) findViewById(R.id.pager)).setText(String.valueOf(vf.getDisplayedChild() + 1) + "/5");
    }
}
