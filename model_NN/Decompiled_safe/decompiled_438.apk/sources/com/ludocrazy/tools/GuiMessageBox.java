package com.ludocrazy.tools;

import android.content.Context;
import javax.microedition.khronos.opengles.GL10;

public class GuiMessageBox extends GuiButton {
    protected float ANIMATIONBLENDFACTOR = 0.2f;
    protected Context m_Context = null;
    protected float m_IconRotationSide = 120.0f;
    protected float m_IconRotationTargetSide = 120.0f;
    protected float m_IconRotationTargetTop = -90.0f;
    protected float m_IconRotationTop = -90.0f;
    protected float m_IconScale = 1.0f;
    protected Sproxel m_IconSproxel = null;

    public GuiMessageBox(Context context, LogOutput DebugLogOutput, PhoneAbilities phoneAbilities, float texture_square_size_pixels, float animation_blend_factor, Font textboxfont, boolean textCentered, GuiMeshBatch guiMeshBatch, GuiBase parent, float left, float top, float width, float height, float boxZ, float alpha) throws Exception {
        super(DebugLogOutput, phoneAbilities, texture_square_size_pixels, textboxfont, textCentered, guiMeshBatch, parent, left, top, width, height, boxZ, alpha, true);
        this.m_Context = context;
        this.ANIMATIONBLENDFACTOR = animation_blend_factor;
    }

    public boolean hide() {
        if (!this.m_VisibleAndUseable) {
            return false;
        }
        setVisibleAndUseable(false);
        setRedrawNeeded();
        return true;
    }

    public boolean show() {
        if (this.m_VisibleAndUseable) {
            return false;
        }
        setVisibleAndUseable(true);
        setRedrawNeeded();
        return true;
    }

    public void resetToNewText(String text) {
        clearTextStack();
        addTextToStack(text);
    }

    public void resetToNewText_Displaying(String text) {
        resetToNewText(text);
        setVisibleAndUseable(true);
    }

    public void setIconSproxel(String sproxel_filename, float scale) {
        this.m_IconScale = scale;
        this.m_IconSproxel = new Sproxel();
        this.m_IconSproxel.loadCubeKingdomFile(this.m_Context, sproxel_filename, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.ludocrazy.tools.PointSpritesMesh.draw(javax.microedition.khronos.opengles.GL10, boolean):int
     arg types: [javax.microedition.khronos.opengles.GL10, int]
     candidates:
      com.ludocrazy.tools.PointSpritesMesh.draw(javax.microedition.khronos.opengles.GL10, int):int
      com.ludocrazy.tools.PointSpritesMesh.draw(javax.microedition.khronos.opengles.GL10, boolean):int */
    public boolean drawSproxelIcon(GL10 gl, float POINTSPRITESIZE) {
        if (this.m_VisibleAndUseable) {
            gl.glPushMatrix();
            float xTrans = this.m_Left + ((this.m_Right - this.m_Left) * 0.75f);
            float yTrans = this.m_Top + ((this.m_Bottom - this.m_Top) * 0.5f);
            float zTrans = this.m_BoxZ + 0.5f;
            gl.glScalef(this.m_IconScale, this.m_IconScale, this.m_IconScale);
            gl.glTranslatef(xTrans / this.m_IconScale, (yTrans / this.m_IconScale) - ((this.m_IconSproxel.m_Zmax * 0.5f) * this.m_IconScale), zTrans);
            if (Math.abs(this.m_IconRotationTop - this.m_IconRotationTargetTop) < 0.01f) {
                this.m_IconRotationTargetTop = Tools.random(-90.0f, -65.0f);
            }
            if (Math.abs(this.m_IconRotationSide - this.m_IconRotationTargetSide) < 0.01f) {
                this.m_IconRotationTargetSide = Tools.random(120.0f, 140.0f);
            }
            this.m_IconRotationTop = (this.m_IconRotationTop * (1.0f - this.ANIMATIONBLENDFACTOR)) + (this.ANIMATIONBLENDFACTOR * this.m_IconRotationTargetTop);
            this.m_IconRotationSide = (this.m_IconRotationSide * (1.0f - this.ANIMATIONBLENDFACTOR)) + (this.ANIMATIONBLENDFACTOR * this.m_IconRotationTargetSide);
            gl.glRotatef(this.m_IconRotationTop, 1.0f, 0.0f, 0.0f);
            gl.glRotatef(this.m_IconRotationSide, 0.0f, 0.0f, 1.0f);
            gl.glPointSize(this.m_IconScale * POINTSPRITESIZE * 0.75f);
            this.m_IconSproxel.draw(gl, false);
            gl.glPopMatrix();
        }
        return false;
    }
}
