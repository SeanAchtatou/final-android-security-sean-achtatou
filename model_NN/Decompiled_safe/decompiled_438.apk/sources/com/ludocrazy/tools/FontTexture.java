package com.ludocrazy.tools;

import android.graphics.Bitmap;
import android.opengl.GLUtils;
import android.util.Log;
import javax.microedition.khronos.opengles.GL10;

public class FontTexture {
    private int height = -1;
    private GL10 m_gl = null;
    private int textureHandle = -1;
    private int width = -1;

    public FontTexture(GL10 gl, int ExternalTextureHandle, int ExternalWidht, int ExternalHeight) {
        this.m_gl = gl;
        this.textureHandle = ExternalTextureHandle;
        this.width = ExternalWidht;
        this.height = ExternalHeight;
    }

    public void setGLonResume(GL10 gl) {
        this.m_gl = gl;
    }

    public FontTexture(GL10 gl, int width2, int height2, boolean convert_abgr_to_argb) throws Exception {
        Bitmap image = Bitmap.createBitmap(width2, height2, Bitmap.Config.ARGB_8888);
        this.m_gl = gl;
        int[] textures = new int[1];
        gl.glGenTextures(1, textures, 0);
        this.textureHandle = textures[0];
        this.width = image.getWidth();
        this.height = image.getHeight();
        gl.glBindTexture(3553, this.textureHandle);
        gl.glMatrixMode(5890);
        gl.glLoadIdentity();
        TextureManager.buildMipmap_Manually(gl, image, convert_abgr_to_argb);
        image.recycle();
        gl.glTexParameterf(3553, 10242, 10497.0f);
        gl.glTexParameterf(3553, 10243, 10497.0f);
        gl.glTexParameterf(3553, 10240, 9729.0f);
        gl.glTexParameterf(3553, 10241, 9985.0f);
    }

    public void bind() {
        this.m_gl.glBindTexture(3553, this.textureHandle);
    }

    public void dispose() {
        this.m_gl.glDeleteTextures(1, new int[]{this.textureHandle}, 0);
        this.textureHandle = 0;
    }

    public int getHeight() {
        return this.height;
    }

    public int getWidth() {
        return this.width;
    }

    public void drawOnMipMaps(PhoneAbilities phoneAbilities, Bitmap bitmap, int x, int y) {
        boolean useGLUtilsTexSubImage2D = false;
        try {
            useGLUtilsTexSubImage2D = phoneAbilities.isSupported(2);
        } catch (Exception e) {
            new ErrorOutput("FontTexture.drawOnMipMaps", "Problem with PhoneAbilities:", e);
        }
        this.m_gl.glBindTexture(3553, this.textureHandle);
        int level = 0;
        int height2 = bitmap.getHeight();
        int width2 = bitmap.getWidth();
        while (height2 >= 1 && width2 >= 1 && level < 5) {
            if (useGLUtilsTexSubImage2D) {
                GLUtils.texSubImage2D(3553, level, x, y, bitmap);
            } else {
                TextureManager.imageToTexture2D_SubTexture(this.m_gl, bitmap, level, x, y);
            }
            checkForGlErrors(this.m_gl, "FontTexture.draw() after texSubImage2D for level:" + level + " x:" + x + " y:" + y);
            if (height2 > 1 && width2 > 1) {
                level++;
                if (height2 > 1) {
                    height2 /= 2;
                }
                if (width2 > 1) {
                    width2 /= 2;
                }
                x /= 2;
                y /= 2;
                bitmap = Bitmap.createScaledBitmap(bitmap, width2, height2, true);
            } else {
                return;
            }
        }
    }

    public static void checkForGlErrors(GL10 gl, String msg) {
        int error = gl.glGetError();
        if (error != 0) {
            Log.e("com.ludocrazy.misc", "FontTexture OpenGL Error: " + glErrorToString(error) + " at: " + msg);
        }
    }

    private static String glErrorToString(int error) {
        switch (error) {
            case 0:
                return String.valueOf(error) + " GL_NO_ERROR";
            case 1280:
                return String.valueOf(error) + " GL_INVALID_ENUM";
            case 1281:
                return String.valueOf(error) + " GL_INVALID_VALUE";
            case 1282:
                return String.valueOf(error) + " GL_INVALID_OPERATION";
            case 1283:
                return String.valueOf(error) + " GL_STACK_OVERFLOW";
            case 1284:
                return String.valueOf(error) + " GL_STACK_UNDERFLOW";
            case 1285:
                return String.valueOf(error) + " GL_OUT_OF_MEMORY";
            default:
                return String.valueOf(error) + " UNKNOWN error number!";
        }
    }
}
