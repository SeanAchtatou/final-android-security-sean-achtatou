package com.ludocrazy.tools;

import java.util.Vector;

public class PointsPath {
    private int m_CurrentIndex = 0;
    private Vector3D m_PathOffset = new Vector3D();
    private Vector<PathPoint> m_PathPoints = new Vector<>();
    private PathPoint m_StartPoint = null;

    public class PathPoint {
        public Vector3D pos = new Vector3D();

        public PathPoint() {
        }
    }

    public void addPointToPath(int x, int y, int z, int id) throws Exception {
        PathPoint newStep = new PathPoint();
        newStep.pos.x = (float) x;
        newStep.pos.y = (float) y;
        newStep.pos.z = (float) z;
        switch (id) {
            case 0:
                this.m_PathPoints.add(newStep);
                return;
            case 1:
                this.m_StartPoint = newStep;
                return;
            default:
                throw new Exception("addPathVoxel(): unknown textured voxel id given: " + id);
        }
    }

    public void prepareSortedPath() throws Exception {
        Vector<PathPoint> SortedPathPoints = new Vector<>();
        PathPoint currentPoint = this.m_StartPoint;
        int initialCount = this.m_PathPoints.size();
        if (currentPoint == null) {
            throw new Exception("PointsPath.prepareSortedPath(): no startpoint has been given!");
        }
        for (int c = 0; c < initialCount; c++) {
            int nearestId = 0;
            float nearestDistance = 99999.9f;
            int count = this.m_PathPoints.size();
            for (int i = 0; i < count; i++) {
                float distance = this.m_PathPoints.elementAt(i).pos.getDistance(currentPoint.pos);
                if (distance < nearestDistance) {
                    nearestDistance = distance;
                    nearestId = i;
                }
            }
            currentPoint = this.m_PathPoints.elementAt(nearestId);
            SortedPathPoints.add(currentPoint);
            this.m_PathPoints.remove(nearestId);
        }
        this.m_PathPoints = SortedPathPoints;
    }

    public void setPathOffset(float x, float y, float z) {
        this.m_PathOffset.x = x;
        this.m_PathOffset.y = y;
        this.m_PathOffset.z = z;
    }

    public Vector3D getNextPathPoint() {
        this.m_CurrentIndex++;
        if (this.m_CurrentIndex >= this.m_PathPoints.size()) {
            this.m_CurrentIndex = 0;
        }
        return this.m_PathPoints.elementAt(this.m_CurrentIndex).pos.calculate_offset(this.m_PathOffset);
    }

    public PointAtPath advancePointAlongPath_Linear(PointAtPath point, float speed) {
        point.currentPosition += speed;
        Vector3D currentPointPos = this.m_PathPoints.elementAt(point.currentPointId).pos;
        Vector3D nextPointPos = this.m_PathPoints.elementAt(point.nextPointId).pos;
        point.currentDistance = currentPointPos.getDistance(nextPointPos);
        point.currentFactor = point.currentPosition / point.currentDistance;
        if (((double) point.currentFactor) > 1.0d) {
            point.currentPosition -= point.currentDistance;
            point.currentPointId = point.nextPointId;
            point.nextPointId++;
            if (point.nextPointId >= this.m_PathPoints.size() - 1) {
                point.nextPointId = 0;
            }
            currentPointPos = this.m_PathPoints.elementAt(point.currentPointId).pos;
            nextPointPos = this.m_PathPoints.elementAt(point.nextPointId).pos;
            point.currentDistance = currentPointPos.getDistance(nextPointPos);
            point.currentFactor = point.currentPosition / point.currentDistance;
        }
        if (((double) point.currentFactor) > 1.0d) {
            point.currentFactor = 1.0f;
        }
        point.currentPos3D.copyFrom(currentPointPos);
        point.currentPos3D.interpolate_linear(nextPointPos, point.currentFactor);
        point.currentPos3D.add(this.m_PathOffset);
        return point;
    }

    public PointAtPath advancePointAlongPath_CatmullRom(PointAtPath point, float speed) {
        point.currentFactor += speed / 2.5f;
        while (((double) point.currentFactor) > 1.0d) {
            point.currentFactor -= 1.0f;
            point.currentPointId = point.nextPointId;
            point.nextPointId++;
            if (point.nextPointId >= this.m_PathPoints.size() - 1) {
                point.nextPointId = 0;
            }
        }
        Vector3D currentPointPos = this.m_PathPoints.elementAt(point.currentPointId).pos;
        Vector3D nextPointPos = this.m_PathPoints.elementAt(point.nextPointId).pos;
        point.currentPos3D.copyFrom(currentPointPos);
        int prePreIndex = point.currentPointId - 2;
        int preIndex = point.currentPointId - 1;
        if (prePreIndex < 0) {
            prePreIndex = 0;
        }
        if (preIndex < 0) {
            preIndex = 0;
        }
        point.currentPos3D.interpolate_catmullrom(this.m_PathPoints.elementAt(prePreIndex).pos, this.m_PathPoints.elementAt(preIndex).pos, nextPointPos, point.currentFactor);
        point.currentPos3D.add(this.m_PathOffset);
        point.totalPercentage = ((float) point.currentPointId) / ((float) this.m_PathPoints.size());
        return point;
    }
}
