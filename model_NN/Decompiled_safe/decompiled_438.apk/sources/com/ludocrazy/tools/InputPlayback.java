package com.ludocrazy.tools;

import android.content.Context;
import android.os.Environment;
import android.util.Log;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.InputStreamReader;

public class InputPlayback {
    private BufferedReader m_BufferedFileReader = null;
    private float m_LastEventTime = 0.0f;
    private SimpleEvent m_NextEvent = null;

    public class SimpleEvent {
        public int action;
        public float gametime_seconds;
        public float x_pos;
        public float y_pos;

        public SimpleEvent() {
        }
    }

    public InputPlayback(Context context_if_to_be_loaded_from_assets, String filename, float start_game_time) {
        try {
            this.m_LastEventTime = start_game_time;
            if (context_if_to_be_loaded_from_assets == null) {
                this.m_BufferedFileReader = new BufferedReader(new FileReader(new File(Environment.getExternalStorageDirectory(), String.valueOf(filename) + ".txt")));
            } else {
                this.m_BufferedFileReader = new BufferedReader(new InputStreamReader(context_if_to_be_loaded_from_assets.getAssets().open(filename, 2)));
            }
            readNextEvent();
        } catch (Exception e) {
            Log.d("com.ludocrazy.misc", "InputPlayback() Exception:" + e.toString());
        }
    }

    private void readNextEvent() throws Exception {
        String line = this.m_BufferedFileReader.readLine();
        if (line != null) {
            String[] elements = line.split(",");
            if (elements.length >= 5) {
                this.m_NextEvent = new SimpleEvent();
                this.m_LastEventTime += Float.parseFloat(elements[1].split(":")[1].trim());
                this.m_NextEvent.gametime_seconds = this.m_LastEventTime;
                this.m_NextEvent.x_pos = Float.parseFloat(elements[2].split(":")[1].trim());
                this.m_NextEvent.y_pos = Float.parseFloat(elements[3].split(":")[1].trim());
                this.m_NextEvent.action = Integer.parseInt(elements[4].split(":")[1].trim());
                return;
            }
            this.m_NextEvent = null;
            return;
        }
        this.m_NextEvent = null;
    }

    public boolean noFurtherEventsLeft() {
        return this.m_NextEvent == null;
    }

    public SimpleEvent getEventFromPlayback(float current_gametime_seconds) throws Exception {
        if (this.m_NextEvent == null || current_gametime_seconds < this.m_NextEvent.gametime_seconds) {
            return null;
        }
        SimpleEvent current_event = this.m_NextEvent;
        readNextEvent();
        return current_event;
    }
}
