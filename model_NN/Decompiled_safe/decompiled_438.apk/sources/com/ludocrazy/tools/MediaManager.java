package com.ludocrazy.tools;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import java.util.Vector;

public class MediaManager {
    public static final int MAX_MULTI_LAYERS = 7;
    /* access modifiers changed from: private */
    public LogOutput DebugLogOutput = null;
    /* access modifiers changed from: private */
    public AudioManager m_AudioManager = null;
    private Context m_Context = null;
    private String m_CurrentSong;
    private float m_EffectsVolume = 1.0f;
    /* access modifiers changed from: private */
    public float m_FadeVolume = 1.0f;
    /* access modifiers changed from: private */
    public long m_LastStartTime_Seconds = 0;
    /* access modifiers changed from: private */
    public MediaPlayer m_MediaPlayer = null;
    private MediaPlayer m_MediaPlayer1 = null;
    private MediaPlayer m_MediaPlayer2 = null;
    private MediaPlayer m_MediaPlayer3 = null;
    private MediaPlayer[] m_MediaPlayerLayer = new MediaPlayer[7];
    private int m_MediaPlayerLayerCount = 0;
    private boolean m_MusicDisabled = false;
    /* access modifiers changed from: private */
    public float m_MusicVolume = 1.0f;
    private Vector<SoundEffect> m_SoundEffects = new Vector<>();
    private SoundPool m_SoundPool = null;
    private boolean m_SoundsDisabled = false;
    /* access modifiers changed from: private */
    public boolean m_StreamedMusic_DelayedLoopsThread_CanRun = true;
    /* access modifiers changed from: private */
    public boolean m_StreamedMusic_DelayedLoopsThread_Running = false;

    private class SoundEffect {
        String filename;
        int sound_id;

        SoundEffect(String file_name, int sound_identifier) {
            this.filename = file_name;
            this.sound_id = sound_identifier;
        }
    }

    public MediaManager(Context context) {
        this.m_Context = context;
        this.m_AudioManager = (AudioManager) this.m_Context.getSystemService("audio");
    }

    public void setLogOutput(LogOutput pDebugLogOutput) {
        this.DebugLogOutput = pDebugLogOutput;
    }

    private int addSoundEffect(String filename) {
        int soundId = -1;
        try {
            soundId = this.m_SoundPool.load(this.m_Context.getAssets().openFd(filename), 1);
            this.m_SoundEffects.add(new SoundEffect(filename, soundId));
            return soundId;
        } catch (Exception e) {
            new ErrorOutput("MediaManager::addSoundEffect()", "Exception", e);
            return soundId;
        }
    }

    private int findOrAddSoundEffect(String filename) {
        int size = this.m_SoundEffects.size();
        int found = -1;
        for (int i = 0; i < size; i++) {
            if (this.m_SoundEffects.get(i).filename.matches(filename)) {
                found = this.m_SoundEffects.get(i).sound_id;
            }
        }
        if (found == -1) {
            return addSoundEffect(filename);
        }
        return found;
    }

    private int prepareSound(String filename) {
        if (this.m_SoundsDisabled || filename.length() <= 0) {
            return -1;
        }
        if (this.m_SoundPool == null) {
            this.m_SoundPool = new SoundPool(4, 3, 0);
        }
        return findOrAddSoundEffect(filename);
    }

    private float prepareVolume() {
        return (((float) this.m_AudioManager.getStreamVolume(3)) / ((float) this.m_AudioManager.getStreamMaxVolume(3))) * this.m_EffectsVolume;
    }

    public void stopSoundEffeect(int playSoundEffect_returned_streamID) {
        try {
            if (!this.m_SoundsDisabled && this.m_SoundPool != null && playSoundEffect_returned_streamID != 0) {
                this.m_SoundPool.stop(playSoundEffect_returned_streamID);
            }
        } catch (Exception e) {
            new ErrorOutput("MediaManager::stopSoundEffeect()", "Exception", e);
        }
    }

    public int playSoundEffect(int res_strings_id) {
        return playSoundEffect(res_strings_id, 1.0f);
    }

    public int playSoundEffect(String filename) {
        return playSoundEffect(filename, 1.0f);
    }

    public int playSoundEffect(int res_strings_id, float pitch_half_to_double) {
        return playSoundEffect(this.m_Context.getString(res_strings_id), pitch_half_to_double);
    }

    public int playSoundEffect(String filename, float pitch_half_to_double) {
        int soundId;
        int result;
        try {
            if (this.m_SoundsDisabled || filename.length() <= 0 || (soundId = prepareSound("sound/" + filename)) <= -1) {
                return 0;
            }
            float volume = prepareVolume();
            int millisecondsTime = 0;
            do {
                if (this.m_SoundPool != null) {
                    Tools.clamp(pitch_half_to_double, 0.5f, 2.0f);
                    result = this.m_SoundPool.play(soundId, volume, volume, 1, 0, pitch_half_to_double);
                } else {
                    result = -1;
                }
                if (result == 0) {
                    Thread.sleep(100);
                    millisecondsTime += 100;
                    if (millisecondsTime > 4000) {
                        throw new Exception("Couldn't SoundPool.play soundId" + soundId + ", waited 4 seconds. See Log for status");
                    }
                }
            } while (result == 0);
            return result;
        } catch (Exception e) {
            new ErrorOutput("MediaManager::playSoundEffect()", "Exception", e);
            return 0;
        }
    }

    public void playStreamedMusic(int res_strings_id, boolean loop, float fade_duration_seconds) {
        playStreamedMusic(this.m_Context.getString(res_strings_id), loop, fade_duration_seconds);
    }

    public void playStreamedMusic(final String filename, final boolean loop, final float fade_duration_seconds) {
        if (this.m_MusicDisabled) {
            return;
        }
        if (this.m_CurrentSong != null && this.m_CurrentSong.equals(filename) && (this.m_MediaPlayer == null || this.m_MediaPlayer.isPlaying())) {
            return;
        }
        if (this.m_MediaPlayer == null) {
            this.m_MediaPlayer = new MediaPlayer();
            this.m_FadeVolume = ((float) this.m_AudioManager.getStreamVolume(3)) / ((float) this.m_AudioManager.getStreamMaxVolume(3));
            startMusicStream(this.m_MediaPlayer, "music/" + filename, loop, 1.0f);
            return;
        }
        this.m_FadeVolume = ((float) this.m_AudioManager.getStreamVolume(3)) / ((float) this.m_AudioManager.getStreamMaxVolume(3));
        if (fade_duration_seconds <= 0.0f || !this.m_MediaPlayer.isPlaying()) {
            startMusicStream(this.m_MediaPlayer, "music/" + filename, loop, 1.0f);
        } else {
            new Thread() {
                public void run() {
                    float fade_increment = (MediaManager.this.m_FadeVolume / fade_duration_seconds) / ((float) 10);
                    float fade = MediaManager.this.m_FadeVolume;
                    while (fade >= 0.0f) {
                        if (fade < 0.0f) {
                            fade = 0.0f;
                        }
                        if (MediaManager.this.m_MediaPlayer == null) {
                            break;
                        }
                        try {
                            if (MediaManager.this.m_MediaPlayer.isPlaying()) {
                                MediaManager.this.m_MediaPlayer.setVolume(MediaManager.this.m_MusicVolume * fade, MediaManager.this.m_MusicVolume * fade);
                            }
                            Thread.sleep((long) (1000 / 10));
                        } catch (Exception e) {
                        }
                        fade -= fade_increment;
                    }
                    MediaManager.this.startMusicStream(MediaManager.this.m_MediaPlayer, "music/" + filename, loop, 1.0f);
                }
            }.start();
        }
    }

    public void playStreamedMusic_withDelayBetweenLoops(String filename, float fade_duration_seconds, int seconds_till_loop_restart, float volume_reduction_stepping, float min_volume_to_stop_at) throws Exception {
        if (!this.m_MusicDisabled) {
            if (this.m_StreamedMusic_DelayedLoopsThread_Running) {
                stopStreamedMusic_DelayedLoopsThread(false);
                Thread.sleep(1000);
            }
            this.m_StreamedMusic_DelayedLoopsThread_CanRun = true;
            this.m_LastStartTime_Seconds = 0;
            if (this.m_CurrentSong == null || !this.m_CurrentSong.equals(filename) || (this.m_MediaPlayer != null && !this.m_MediaPlayer.isPlaying())) {
                final String str = filename;
                final float f = fade_duration_seconds;
                final int i = seconds_till_loop_restart;
                final float f2 = volume_reduction_stepping;
                final float f3 = min_volume_to_stop_at;
                new Thread() {
                    private void startPlaying(float current_volume_factor) {
                        if (MediaManager.this.m_MediaPlayer == null) {
                            MediaManager.this.m_MediaPlayer = new MediaPlayer();
                            MediaManager.this.m_FadeVolume = ((float) MediaManager.this.m_AudioManager.getStreamVolume(3)) / ((float) MediaManager.this.m_AudioManager.getStreamMaxVolume(3));
                            MediaManager.this.startMusicStream(MediaManager.this.m_MediaPlayer, "music/" + str, false, current_volume_factor);
                            return;
                        }
                        MediaManager.this.m_FadeVolume = ((float) MediaManager.this.m_AudioManager.getStreamVolume(3)) / ((float) MediaManager.this.m_AudioManager.getStreamMaxVolume(3));
                        if (f <= 0.0f || !MediaManager.this.m_MediaPlayer.isPlaying()) {
                            MediaManager.this.startMusicStream(MediaManager.this.m_MediaPlayer, "music/" + str, false, current_volume_factor);
                            return;
                        }
                        float fade_increment = (MediaManager.this.m_FadeVolume / f) / ((float) 10);
                        float fade = MediaManager.this.m_FadeVolume;
                        while (fade >= 0.0f) {
                            if (fade < 0.0f) {
                                fade = 0.0f;
                            }
                            if (MediaManager.this.m_MediaPlayer == null) {
                                break;
                            }
                            try {
                                if (MediaManager.this.m_MediaPlayer.isPlaying()) {
                                    MediaManager.this.m_MediaPlayer.setVolume(MediaManager.this.m_MusicVolume * fade, MediaManager.this.m_MusicVolume * fade);
                                }
                                Thread.sleep((long) (1000 / 10));
                            } catch (Exception e) {
                            }
                            fade -= fade_increment;
                        }
                        MediaManager.this.startMusicStream(MediaManager.this.m_MediaPlayer, "music/" + str, false, current_volume_factor);
                    }

                    public void run() {
                        float current_volume_factor = 1.0f;
                        while (MediaManager.this.m_StreamedMusic_DelayedLoopsThread_CanRun) {
                            try {
                                MediaManager.this.m_StreamedMusic_DelayedLoopsThread_Running = true;
                                if (MediaManager.this.m_MediaPlayer == null || !MediaManager.this.m_MediaPlayer.isPlaying() || MediaManager.this.m_LastStartTime_Seconds == 0) {
                                    long current_time_seconds = System.currentTimeMillis() / 1000;
                                    long duration = current_time_seconds - MediaManager.this.m_LastStartTime_Seconds;
                                    MediaManager.this.DebugLogOutput.log(2, "MediaManager.playStreamedMusic_withDelayBetweenLoops() volume:" + current_volume_factor + ", duration:" + duration + ", last:" + MediaManager.this.m_LastStartTime_Seconds + ", current:" + current_time_seconds);
                                    if (duration >= ((long) i)) {
                                        MediaManager.this.m_LastStartTime_Seconds = current_time_seconds;
                                        startPlaying(current_volume_factor);
                                        current_volume_factor -= f2;
                                        if (current_volume_factor < f3) {
                                            break;
                                        }
                                    } else {
                                        continue;
                                    }
                                }
                                Thread.sleep(500, 0);
                            } catch (Exception e) {
                            }
                        }
                        MediaManager.this.m_StreamedMusic_DelayedLoopsThread_Running = false;
                        MediaManager.this.m_StreamedMusic_DelayedLoopsThread_CanRun = true;
                    }
                }.start();
            }
        }
    }

    public void stopStreamedMusic_DelayedLoopsThread(boolean stop_music) {
        this.m_StreamedMusic_DelayedLoopsThread_CanRun = false;
        if (stop_music && this.m_MediaPlayer != null) {
            this.m_MediaPlayer.stop();
        }
    }

    private void prepareMusicStream(MediaPlayer player, String filename, boolean looping, float current_volume_factor) {
        if (player != null) {
            try {
                AssetFileDescriptor descriptor = this.m_Context.getAssets().openFd(filename);
                this.m_CurrentSong = filename;
                player.reset();
                player.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
                player.prepare();
                player.setLooping(looping);
                player.setVolume(this.m_FadeVolume * this.m_MusicVolume * current_volume_factor, this.m_FadeVolume * this.m_MusicVolume * current_volume_factor);
            } catch (Exception e) {
                new ErrorOutput("MediaManager::prepareMusicStream()", "Can't play: " + filename + ". Exception", e);
            }
        }
    }

    /* access modifiers changed from: private */
    public void startMusicStream(MediaPlayer player, String filename, boolean looping, float current_volume_factor) {
        if (player != null) {
            try {
                prepareMusicStream(player, filename, looping, current_volume_factor);
                player.start();
            } catch (Exception e) {
                new ErrorOutput("MediaManager::startMusicStream()", "Can't play: " + filename + ". Exception", e);
            }
        }
    }

    public void release() {
        stopStreamedMusic_DelayedLoopsThread(true);
        if (this.m_MediaPlayer != null) {
            this.m_MediaPlayer.release();
            this.m_MediaPlayer = null;
        }
        if (this.m_MediaPlayer1 != null) {
            this.m_MediaPlayer1.release();
            this.m_MediaPlayer1 = null;
        }
        if (this.m_MediaPlayer2 != null) {
            this.m_MediaPlayer2.release();
            this.m_MediaPlayer2 = null;
        }
        if (this.m_MediaPlayer3 != null) {
            this.m_MediaPlayer3.release();
            this.m_MediaPlayer3 = null;
        }
        unloadMultiMusic();
        if (this.m_SoundPool != null) {
            this.m_SoundPool.release();
            this.m_SoundPool = null;
            this.m_SoundEffects.clear();
        }
    }

    /* access modifiers changed from: package-private */
    public float getCurrentEffectsVolume() {
        return this.m_EffectsVolume;
    }

    public void setEffectsVolume(float new_volume) {
        if (new_volume < 0.0f) {
            new_volume = 0.0f;
        } else if (new_volume > 1.0f) {
            new_volume = 1.0f;
        }
        this.m_EffectsVolume = new_volume;
    }

    public void setMusicVolume(float new_volume) {
        if (new_volume < 0.0f) {
            new_volume = 0.0f;
        } else if (new_volume > 1.0f) {
            new_volume = 1.0f;
        }
        this.m_MusicVolume = new_volume;
        if (this.m_MediaPlayer != null) {
            this.m_MediaPlayer.setVolume(new_volume, new_volume);
        }
    }

    public void loadMultiMusicLoops(String asset_filename_and_path) {
        if (this.m_MediaPlayerLayerCount < 7) {
            try {
                this.m_MediaPlayerLayer[this.m_MediaPlayerLayerCount] = new MediaPlayer();
                prepareMusicStream(this.m_MediaPlayerLayer[this.m_MediaPlayerLayerCount], asset_filename_and_path, true, 1.0f);
                this.m_MediaPlayerLayerCount++;
            } catch (Exception e) {
                new ErrorOutput("MediaManager::loadMultiMusicLoops()", "Exception while preparing to play: " + asset_filename_and_path + " as musicstream loop layer no. " + this.m_MediaPlayerLayerCount, e);
            }
        } else {
            new ErrorOutput("MediaManager::loadMultiMusicLoops()", "Already MAX_LAYERS: 7 loaded. Can not load further: " + asset_filename_and_path + ".");
        }
    }

    public void playMultiMusic() {
        for (int i = 0; i < this.m_MediaPlayerLayerCount; i++) {
            try {
                this.m_MediaPlayerLayer[i].start();
            } catch (Exception e) {
                new ErrorOutput("MediaManager::playMultiMusic()", "Exception trying to start layer " + i, e);
            }
        }
    }

    public void setMultiMusicVolume(int layer, float new_volume) {
        if (layer >= 0 && layer < this.m_MediaPlayerLayerCount) {
            float new_volume2 = new_volume * this.m_MusicVolume;
            if (new_volume2 < 0.0f) {
                new_volume2 = 0.0f;
            } else if (new_volume2 > 1.0f) {
                new_volume2 = 1.0f;
            }
            if (this.m_MediaPlayerLayer[layer] != null) {
                try {
                    if (this.m_MediaPlayerLayer[layer].isPlaying()) {
                        this.m_MediaPlayerLayer[layer].setVolume(new_volume2, new_volume2);
                    }
                } catch (Exception e) {
                }
            } else {
                new ErrorOutput("MediaManager::setMultiMusicVolume()", "Can't set volume for layer " + layer + "! MediaPlayerLayer is null!");
            }
        }
    }

    public void unloadMultiMusic() {
        for (int i = 0; i < 7; i++) {
            if (this.m_MediaPlayerLayer[i] != null) {
                this.m_MediaPlayerLayer[i].stop();
                this.m_MediaPlayerLayer[i].release();
                this.m_MediaPlayerLayer[i] = null;
            }
        }
        this.m_MediaPlayerLayerCount = 0;
    }

    public void playMultiMusic_TEST(LogOutput DebugLogOutput2, String file1, String file2, String file3, boolean loop) {
        if (file1 != null) {
            try {
                this.m_MediaPlayer1 = new MediaPlayer();
                prepareMusicStream(this.m_MediaPlayer1, file1, loop, 1.0f);
                DebugLogOutput2.log(1, "playMultiMusic_TEST() preparing to play: " + file1 + " as musicstream.");
            } catch (Exception e) {
                new ErrorOutput("MediaManager::playMultiMusic_TEST()", "Exception", e);
                return;
            }
        }
        if (file2 != null) {
            this.m_MediaPlayer2 = new MediaPlayer();
            prepareMusicStream(this.m_MediaPlayer2, file2, loop, 1.0f);
            DebugLogOutput2.log(1, "playMultiMusic_TEST() preparing to play: " + file2 + " as musicstream.");
        }
        if (file3 != null) {
            this.m_MediaPlayer3 = new MediaPlayer();
            prepareMusicStream(this.m_MediaPlayer3, file3, loop, 1.0f);
            DebugLogOutput2.log(1, "playMultiMusic_TEST() preparing to play: " + file3 + " as musicstream.");
        }
        if (this.m_MediaPlayer1 != null) {
            this.m_MediaPlayer1.start();
        }
        if (this.m_MediaPlayer2 != null) {
            this.m_MediaPlayer2.start();
        }
        if (this.m_MediaPlayer3 != null) {
            this.m_MediaPlayer3.start();
        }
    }

    public void playMultiSound_TEST(LogOutput DebugLogOutput2, String file1, String file2, String file3, boolean loop) {
        try {
            int soundId1 = prepareSound(file1);
            int soundId2 = prepareSound(file2);
            int soundId3 = prepareSound(file3);
            Thread.sleep(2000);
            float volume = prepareVolume();
            int loop_int = 0;
            if (loop) {
                loop_int = -1;
            }
            int result1 = this.m_SoundPool.play(soundId1, volume, volume, 1, loop_int, 1.0f);
            int result2 = this.m_SoundPool.play(soundId2, volume, volume, 1, loop_int, 1.0f);
            int result3 = this.m_SoundPool.play(soundId3, volume, volume, 1, loop_int, 1.0f);
            DebugLogOutput2.log(1, "playMultiSound_TEST() playing: " + file1 + " as soundid: " + soundId1 + " resulted in: " + result1);
            DebugLogOutput2.log(1, "playMultiSound_TEST() playing: " + file2 + " as soundid: " + soundId2 + " resulted in: " + result2);
            DebugLogOutput2.log(1, "playMultiSound_TEST() playing: " + file3 + " as soundid: " + soundId3 + " resulted in: " + result3);
        } catch (Exception e) {
            new ErrorOutput("MediaManager::playMultiSound_TEST()", "Exception", e);
        }
    }
}
