package com.ludocrazy.tools;

import android.content.Context;
import java.nio.FloatBuffer;
import java.util.Vector;

public class SproxelManager {
    private LogOutput DebugLogOutput = null;
    private Context m_Context = null;
    private Vector<Sproxel> m_Sproxels = new Vector<>();

    public SproxelManager(LogOutput debugLogOutput, Context context) {
        this.DebugLogOutput = debugLogOutput;
        this.m_Context = context;
    }

    public float[] getSproxelVertices_Translated(String sproxelFilename, float translate_x, float translate_y, float translate_z, int rotation, float scale_x, float scale_y, float scale_z) {
        return findSproxel(sproxelFilename).getTranslatedVertexBuffer(translate_x, translate_y, translate_z, rotation, scale_x, scale_y, scale_z);
    }

    public float[] getSproxelVertices_TranslatedInterleavedArray(String sproxelFilename, float translate_x, float translate_y, float translate_z, int rotation, float scale_x, float scale_y, float scale_z) {
        return findSproxel(sproxelFilename).getTranslatedInterleavedArray(translate_x, translate_y, translate_z, rotation, scale_x, scale_y, scale_z);
    }

    public FloatBuffer getSproxelColors(String sproxelFilename) {
        return findSproxel(sproxelFilename).getColorBuffer_PositionAtZero();
    }

    public Sproxel findSproxel(String sproxelFilename) {
        Sproxel found = null;
        int i = 0;
        while (true) {
            if (i >= this.m_Sproxels.size()) {
                break;
            } else if (this.m_Sproxels.get(i).m_SproxelFilename.equalsIgnoreCase(sproxelFilename)) {
                found = this.m_Sproxels.get(i);
                break;
            } else {
                i++;
            }
        }
        if (found != null) {
            return found;
        }
        Sproxel newSproxel = new Sproxel();
        newSproxel.loadCubeKingdomFile(this.m_Context, sproxelFilename, false);
        this.m_Sproxels.add(newSproxel);
        Sproxel found2 = this.m_Sproxels.lastElement();
        this.DebugLogOutput.log(1, "SproxelManager.findSproxel() loaded sproxel #" + this.m_Sproxels.size() + " from file" + sproxelFilename);
        return found2;
    }
}
