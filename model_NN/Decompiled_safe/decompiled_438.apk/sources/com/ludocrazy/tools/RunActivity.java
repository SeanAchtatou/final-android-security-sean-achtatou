package com.ludocrazy.tools;

import android.app.Activity;
import android.app.KeyguardManager;
import android.hardware.SensorListener;
import android.hardware.SensorManager;
import android.os.Bundle;

public class RunActivity extends Activity implements SensorListener {
    protected LogOutput DebugLogOutput = null;
    protected KeyguardManager.KeyguardLock m_Lock = null;
    protected MediaManager m_MediaManager = null;
    protected SensorManager m_SensorManager = null;
    protected GameView m_ViewRenderer = null;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            setVolumeControlStream(3);
            this.m_Lock = ((KeyguardManager) getSystemService("keyguard")).newKeyguardLock("keyguard");
            this.m_Lock.disableKeyguard();
            this.m_SensorManager = (SensorManager) getSystemService("sensor");
            this.m_MediaManager = new MediaManager(this);
            getWindow().addFlags(128);
        } catch (Exception e) {
            new ErrorOutput("RunActivity::onCreate()", "Exception", e);
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        try {
            super.onResume();
            if (this.m_SensorManager != null) {
                this.m_SensorManager.registerListener(this, 3, 0);
            }
            if (this.m_ViewRenderer != null) {
                this.m_ViewRenderer.onResume();
            }
            this.m_Lock.disableKeyguard();
        } catch (Exception e) {
            new ErrorOutput("RunActivity::onResume()", "Exception", e);
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        try {
            super.onPause();
            this.DebugLogOutput.log(2, "RunActivity::onPause() was called.");
            if (this.m_SensorManager != null) {
                this.m_SensorManager.unregisterListener(this);
            }
            if (this.m_ViewRenderer != null) {
                this.m_ViewRenderer.onPause();
            }
            this.m_MediaManager.release();
            this.m_Lock.reenableKeyguard();
        } catch (Exception e) {
            new ErrorOutput("RunActivity::onPause()", "Exception", e);
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }

    public void onSensorChanged(int sensor, float[] values) {
        synchronized (this) {
            if (sensor == 1) {
                if (this.m_ViewRenderer != null) {
                    this.m_ViewRenderer.setSensorOrientation(values[0], values[1], values[2]);
                }
            }
            if (sensor == 2 && this.m_ViewRenderer != null) {
                this.m_ViewRenderer.setSensorAcceleration(values[0], values[1], values[2]);
            }
        }
    }

    public void onAccuracyChanged(int sensor, int accuracy) {
    }
}
