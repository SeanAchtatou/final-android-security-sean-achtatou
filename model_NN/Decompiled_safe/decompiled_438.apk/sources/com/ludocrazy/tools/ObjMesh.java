package com.ludocrazy.tools;

import android.content.Context;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Vector;

public class ObjMesh extends GuiMesh {
    private static final String OBJFILEPATH = "objmesh/";
    public float m_BoundingSphereRadius = 0.5f;
    private Vector<Coords> m_Coords = new Vector<>();
    private Vector<FaceIndices> m_Faces = new Vector<>();
    private Vector<Coords> m_Normals = new Vector<>();
    private float m_ScaleFactor = 1.0f;
    private Vector<UvCoords> m_UvCoords = new Vector<>();
    private float m_max_x = -99999.0f;
    private float m_max_y = -99999.0f;
    private float m_max_z = -99999.0f;
    private float m_min_x = 99999.0f;
    private float m_min_y = 99999.0f;
    private float m_min_z = 99999.0f;

    private class FaceIndices {
        public int vert1_normal;
        public int vert1_pos;
        public int vert1_uv;
        public int vert2_normal;
        public int vert2_pos;
        public int vert2_uv;
        public int vert3_normal;
        public int vert3_pos;
        public int vert3_uv;

        private FaceIndices() {
        }

        /* synthetic */ FaceIndices(ObjMesh objMesh, FaceIndices faceIndices) {
            this();
        }
    }

    public ObjMesh(LogOutput DEBUGLOG_to_use, PhoneAbilities phoneAbilities) {
        super(DEBUGLOG_to_use, phoneAbilities);
    }

    public void loadFromObjFile(Context context, String objFilename, float scale_factor) {
        int linenumber = 0;
        this.m_ScaleFactor = scale_factor;
        this.m_BoundingSphereRadius = scale_factor / 2.0f;
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(context.getAssets().open(OBJFILEPATH + objFilename, 2)), 16384);
            int unknownlines = 0;
            int ignoredlines = 0;
            int commentlines = 0;
            int emptylines = 0;
            String line = br.readLine();
            do {
                linenumber++;
                if (line.length() > 0) {
                    switch (line.charAt(0)) {
                        case '#':
                            commentlines++;
                            break;
                        case 'f':
                            readFace(line);
                            break;
                        case 'g':
                        case 's':
                            ignoredlines++;
                            break;
                        case 'v':
                            switch (line.charAt(1)) {
                                case ' ':
                                    readVertex(line);
                                    break;
                                case 'n':
                                    readNormal(line);
                                    break;
                                case 't':
                                    readUvCoords(line);
                                    break;
                            }
                        default:
                            unknownlines++;
                            break;
                    }
                } else {
                    emptylines++;
                }
                line = br.readLine();
            } while (line != null);
        } catch (Exception e) {
            new ErrorOutput("ObjMesh.loadFromObjFile()", "Exception when reading line: " + linenumber + ".", e);
        }
        scaleMesh();
        createMeshFromReadFaces();
    }

    private void readVertex(String line) {
        String[] line_split = line.split(" ");
        Coords coords = new Coords();
        coords.x = Float.parseFloat(line_split[1]);
        coords.y = Float.parseFloat(line_split[2]);
        coords.z = Float.parseFloat(line_split[3]);
        if (coords.x < this.m_min_x) {
            this.m_min_x = coords.x;
        }
        if (coords.x > this.m_max_x) {
            this.m_max_x = coords.x;
        }
        if (coords.y < this.m_min_y) {
            this.m_min_y = coords.y;
        }
        if (coords.y > this.m_max_y) {
            this.m_max_y = coords.y;
        }
        if (coords.z < this.m_min_z) {
            this.m_min_z = coords.z;
        }
        if (coords.z > this.m_max_z) {
            this.m_max_z = coords.z;
        }
        this.m_Coords.add(coords);
    }

    private void readNormal(String line) {
        String[] line_split = line.split(" ");
        Coords coords = new Coords();
        coords.x = Float.parseFloat(line_split[1]);
        coords.y = Float.parseFloat(line_split[2]);
        coords.z = Float.parseFloat(line_split[3]);
        this.m_Normals.add(coords);
    }

    private void readUvCoords(String line) {
        String[] line_split = line.split(" ");
        UvCoords uvcoords = new UvCoords();
        uvcoords.u = Float.parseFloat(line_split[1]);
        uvcoords.v = Float.parseFloat(line_split[2]);
        this.m_UvCoords.add(uvcoords);
    }

    private void readFace(String line) {
        String[] face_split = line.split(" ");
        String[] face1_split = face_split[1].split("/");
        String[] face2_split = face_split[2].split("/");
        String[] face3_split = face_split[3].split("/");
        FaceIndices face = new FaceIndices(this, null);
        face.vert1_pos = Integer.parseInt(face1_split[0]) - 1;
        face.vert1_uv = Integer.parseInt(face1_split[1]) - 1;
        face.vert1_normal = Integer.parseInt(face1_split[2]) - 1;
        face.vert2_pos = Integer.parseInt(face2_split[0]) - 1;
        face.vert2_uv = Integer.parseInt(face2_split[1]) - 1;
        face.vert2_normal = Integer.parseInt(face2_split[2]) - 1;
        face.vert3_pos = Integer.parseInt(face3_split[0]) - 1;
        face.vert3_uv = Integer.parseInt(face3_split[1]) - 1;
        face.vert3_normal = Integer.parseInt(face3_split[2]) - 1;
        this.m_Faces.add(face);
    }

    private void scaleMesh() {
        float x_span = this.m_max_x - this.m_min_x;
        float y_span = this.m_max_y - this.m_min_y;
        float z_span = this.m_max_z - this.m_min_z;
        float largest_span = x_span;
        if (y_span > largest_span) {
            largest_span = y_span;
        }
        if (z_span > largest_span) {
            largest_span = z_span;
        }
        this.m_ScaleFactor /= largest_span;
        int count = this.m_Coords.size();
        for (int i = 0; i < count; i++) {
            this.m_Coords.elementAt(i).scale(this.m_ScaleFactor);
        }
    }

    private void createMeshFromReadFaces() {
        try {
            int count = this.m_Faces.size();
            setupArraysTriangleFaces(count);
            for (int i = 0; i < count; i++) {
                addTriangleFace_textured(this.m_Coords.elementAt(this.m_Faces.elementAt(i).vert1_pos), this.m_UvCoords.elementAt(this.m_Faces.elementAt(i).vert1_uv), this.m_Coords.elementAt(this.m_Faces.elementAt(i).vert2_pos), this.m_UvCoords.elementAt(this.m_Faces.elementAt(i).vert2_uv), this.m_Coords.elementAt(this.m_Faces.elementAt(i).vert3_pos), this.m_UvCoords.elementAt(this.m_Faces.elementAt(i).vert3_uv));
            }
            convertArraysToBuffer();
        } catch (Exception e) {
            new ErrorOutput("ObjMesh.createMeshFromReadFaces()", "Exception:", e);
        }
    }
}
