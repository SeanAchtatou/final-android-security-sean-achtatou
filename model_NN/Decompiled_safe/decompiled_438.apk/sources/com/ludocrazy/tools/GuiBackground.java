package com.ludocrazy.tools;

public class GuiBackground extends GuiMesh {
    GuiBackground(LogOutput DEBUGLOG_to_use, PhoneAbilities phoneAbilities, float texture_square_size_pixels, float startX, float startY, float width, float height, float bgZ, int pixelWidth, int pixelHeight, GuiBackgroundPattern pattern, GuiBackgroundColors colors) {
        super(DEBUGLOG_to_use, phoneAbilities);
        try {
            float patternWidth = (float) pattern.width;
            float patternHeight = (float) pattern.height;
            float uLeft = ((float) pattern.left) / texture_square_size_pixels;
            float vTop = ((float) pattern.top) / texture_square_size_pixels;
            float uWidth = patternWidth / texture_square_size_pixels;
            float vHeight = patternHeight / texture_square_size_pixels;
            float patternWidth2 = ((patternWidth * 1.0f) / ((float) pixelWidth)) * width;
            float patternHeight2 = ((patternHeight * 1.0f) / ((float) pixelHeight)) * height;
            float endX = startX + width;
            float endY = startY + height;
            int xCount = ((int) (width / patternWidth2)) + 1;
            int yCount = ((int) (height / patternHeight2)) + 1;
            setupArraysQuadFaces(xCount * yCount * 4);
            ColorFloats upperCurCol = new ColorFloats();
            ColorFloats lowerCurCol = new ColorFloats();
            for (int y = 0; y < yCount; y++) {
                ColorFloats colorFloats = colors.bottomColor;
                ColorFloats colorFloats2 = colors.topColor;
                upperCurCol.blendResult(colorFloats, colorFloats2, ((float) y) / ((float) yCount));
                ColorFloats colorFloats3 = colors.bottomColor;
                ColorFloats colorFloats4 = colors.topColor;
                lowerCurCol.blendResult(colorFloats3, colorFloats4, (((float) y) + 1.0f) / ((float) yCount));
                float ybegin = startY + (((float) y) * patternHeight2);
                float yend = ybegin + patternHeight2;
                float vBottom = vTop + vHeight;
                if (yend > endY) {
                    vBottom = vTop + (vHeight * ((endY - ybegin) / patternHeight2));
                    yend = endY;
                }
                for (int x = 0; x < xCount; x++) {
                    float xbegin = startX + (((float) x) * patternWidth2);
                    float xend = xbegin + patternWidth2;
                    float uRight = uLeft + uWidth;
                    if (xend > endX) {
                        uRight = uLeft + (uWidth * ((endX - xbegin) / patternWidth2));
                        xend = endX;
                    }
                    addQuad(xbegin, ybegin, bgZ, xend, yend, uLeft, vTop, uRight, vBottom);
                    addColorsForLastQuad(upperCurCol, upperCurCol, lowerCurCol, lowerCurCol);
                }
            }
            convertArraysToBuffer();
        } catch (Exception e) {
            new ErrorOutput("GuiBackground::GuiBackground()", "Exception", e);
        }
    }
}
