package com.ludocrazy.tools;

import android.graphics.Bitmap;
import android.os.Environment;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.nio.IntBuffer;
import javax.microedition.khronos.opengles.GL10;

public class ScreenshotOpenGLES {
    public static final int QUALITY_USE_PNG = -1;
    private static final String SERVER_IP_LAN = "192.168.0.2";
    public static final int SERVER_PORT_LAN = 16001;

    private static Bitmap SavePixels_FastButWrong(int x, int y, int w, int h, GL10 gl) throws Exception {
        if (w == 0 || h == 0) {
            throw new Exception("ScreenshotOpenGLES::SavePixels uncorrect paramters given. width:" + w + " height:" + h);
        }
        int[] in_pixels = new int[(w * h)];
        IntBuffer intbuffer = IntBuffer.wrap(in_pixels);
        intbuffer.position(0);
        gl.glReadPixels(x, y, w, h, 6408, 5121, intbuffer);
        return Bitmap.createBitmap(in_pixels, w, h, Bitmap.Config.ARGB_8888);
    }

    /* JADX INFO: Multiple debug info for r9v1 int: [D('p1' int), D('p1_b' int)] */
    /* JADX INFO: Multiple debug info for r10v2 int: [D('p2' int), D('p3_g' int)] */
    /* JADX INFO: Multiple debug info for r11v1 int: [D('p4_r' int), D('p3' int)] */
    /* JADX INFO: Multiple debug info for r12v1 int: [D('p4_b' int), D('p4' int)] */
    /* JADX INFO: Multiple debug info for r11v3 int: [D('p1_r' int), D('p4_r' int)] */
    /* JADX INFO: Multiple debug info for r10v5 int: [D('p3_g' int), D('p1_g' int)] */
    private static int filterPixels(int p1, int p2, int p3, int p4) {
        int p1_r = p1 & 255;
        int p1_g = 65280 & p1;
        int p2_r = p2 & 255;
        int p2_g = p2 & 65280;
        int p2_b = 16711680 & p2;
        int p3_r = p3 & 255;
        int p3_b = 16711680 & p3;
        return ((((((p1 & 16711680) + p2_b) + p3_b) + (p4 & 16711680)) / 4) >> 16) | ((((65280 & p3) + (p1_g + p2_g)) + (65280 & p4)) / 4) | ((((p4 & 255) + ((p1_r + p2_r) + p3_r)) / 4) << 16) | -16777216;
    }

    /* JADX INFO: Multiple debug info for r10v9 int: [D('i' int), D('k' int)] */
    /* JADX INFO: Multiple debug info for r9v15 int: [D('j' int), D('current_pixel' int)] */
    /* JADX INFO: Multiple debug info for r9v31 int: [D('dest_pix' int), D('x' int)] */
    public static Bitmap SavePixels_SlowAndCorrect(int x_start, int y_start, int w, int h, GL10 gl, boolean shrink_to_50_percent) throws Exception {
        int[] out_pixels;
        if (w == 0 || h == 0) {
            throw new Exception("ScreenshotOpenGLES::SavePixels uncorrect paramters given. width:" + w + " height:" + h);
        }
        int[] in_pixels = new int[(w * h)];
        IntBuffer intbuffer = IntBuffer.wrap(in_pixels);
        intbuffer.position(0);
        gl.glReadPixels(x_start, y_start, w, h, 6408, 5121, intbuffer);
        if (shrink_to_50_percent) {
            int[] out_pixels2 = new int[((w / 2) * (h / 2))];
            for (int y = 0; y < h; y += 2) {
                for (int x = 0; x < w; x += 2) {
                    int src_pix = (y * w) + x;
                    out_pixels2[(((((h - y) - 1) / 2) * w) / 2) + (x / 2)] = filterPixels(in_pixels[src_pix], in_pixels[src_pix + 1], in_pixels[src_pix + w], in_pixels[src_pix + w + 1]);
                }
            }
            w /= 2;
            h /= 2;
            out_pixels = out_pixels2;
        } else {
            int[] out_pixels3 = new int[(w * h)];
            int k = 0;
            for (int k2 = 0; k2 < h; k2++) {
                for (int j = 0; j < w; j++) {
                    int current_pixel = in_pixels[(k2 * w) + j];
                    out_pixels3[(((h - k) - 1) * w) + j] = (current_pixel & -16711936) | ((current_pixel << 16) & 16711680) | ((current_pixel >> 16) & 255);
                }
                k++;
            }
            out_pixels = out_pixels3;
        }
        return Bitmap.createBitmap(out_pixels, w, h, Bitmap.Config.ARGB_8888);
    }

    public static void SaveBitmap_ToServer(LogOutput DebugLog, OutputStream tcp_os, Bitmap bmp, int jpeg_quality) {
        if (jpeg_quality == -1) {
            try {
                bmp.compress(Bitmap.CompressFormat.PNG, 100, tcp_os);
            } catch (Exception e) {
                new ErrorOutput("SaveBitmap_ToServer", "Exception", e);
            }
        } else {
            bmp.compress(Bitmap.CompressFormat.JPEG, jpeg_quality, tcp_os);
        }
    }

    public static void SaveBitmap_ForVideo(LogOutput DebugLog, Bitmap bmp, int jpeg_quality) throws Exception {
        TCPClient tcp_c = new TCPClient(DebugLog, 2, SERVER_IP_LAN, 16001);
        OutputStream tcp_os = tcp_c.connect();
        if (jpeg_quality == -1) {
            bmp.compress(Bitmap.CompressFormat.PNG, 100, tcp_os);
        } else {
            bmp.compress(Bitmap.CompressFormat.JPEG, jpeg_quality, tcp_os);
        }
        tcp_c.disconnect();
    }

    public static void SaveScreenshot_ToServer(LogOutput DebugLog, OutputStream tcp_os, GL10 gl, int x, int y, int w, int h, int jpeg_quality) throws Exception {
        SaveBitmap_ToServer(DebugLog, tcp_os, SavePixels_SlowAndCorrect(x, y, w, h, gl, false), jpeg_quality);
    }

    public static void SaveScreenshot_ForVideo(LogOutput DebugLog, GL10 gl, int x, int y, int w, int h, int jpeg_quality) throws Exception {
        SaveBitmap_ForVideo(DebugLog, SavePixels_FastButWrong(x, y, w, h, gl), jpeg_quality);
    }

    /* JADX INFO: Multiple debug info for r6v2 android.graphics.Bitmap: [D('DebugLog' com.ludocrazy.tools.LogOutput), D('bmp' android.graphics.Bitmap)] */
    /* JADX INFO: Multiple debug info for r7v2 java.io.File: [D('gl' javax.microedition.khronos.opengles.GL10), D('root' java.io.File)] */
    /* JADX INFO: Multiple debug info for r7v4 java.io.FileOutputStream: [D('fos' java.io.FileOutputStream), D('root' java.io.File)] */
    public static void SaveScreenshot_ToSD(LogOutput DebugLog, GL10 gl, int x, int y, int w, int h, String filename, int jpeg_quality) {
        try {
            Bitmap bmp = SavePixels_SlowAndCorrect(x, y, w, h, gl, false);
            File root = Environment.getExternalStorageDirectory();
            if (root.canWrite() != 0) {
                FileOutputStream fos = new FileOutputStream(new File(root, String.valueOf(filename) + ".png"));
                if (jpeg_quality == -1) {
                    bmp.compress(Bitmap.CompressFormat.PNG, 100, fos);
                } else {
                    bmp.compress(Bitmap.CompressFormat.JPEG, jpeg_quality, fos);
                }
                fos.flush();
                fos.close();
                return;
            }
            throw new Exception("Can't write to SD card! Probably none inserted, or if emulator none created, or if attached to usb locked for pc-connection.");
        } catch (Exception e) {
            new ErrorOutput("SaveScreenshot_ToSD", "Exception on file:" + filename, e);
        }
    }
}
