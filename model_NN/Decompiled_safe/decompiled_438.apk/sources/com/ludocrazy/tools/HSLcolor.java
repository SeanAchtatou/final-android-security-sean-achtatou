package com.ludocrazy.tools;

public class HSLcolor {
    public float h = 320.0f;
    public float l = 0.5f;
    public float s = 1.0f;

    public HSLcolor(float _h, float _s, float _l) {
        this.h = _h;
        this.s = _s;
        this.l = _l;
    }

    public HSLcolor(HSLcolor copy_constructor) {
        this.h = copy_constructor.h;
        this.s = copy_constructor.s;
        this.l = copy_constructor.l;
    }

    public HSLcolor() {
    }

    public void setHue(float hue) {
        this.h = Tools.clamp(hue, 0.0f, 360.0f);
    }

    public void setSaturation(float sat) {
        this.s = Tools.clamp(sat, 0.0f, 1.0f);
    }

    public void setBrightness(float luminance) {
        this.l = Tools.clamp(luminance, 0.0f, 1.0f);
    }

    public void shiftHueByRandom(float min_offset, float max_offset) {
        this.h = Tools.array_modulo_float(this.h + Tools.random(min_offset, max_offset), 360.0f);
    }

    public ColorFloats convertToRGB() {
        float sat_temp_r;
        float sat_temp_g;
        float sat_temp_b;
        ColorFloats convertedColor = new ColorFloats();
        while (this.h < 0.0f) {
            this.h += 360.0f;
        }
        while (this.h > 360.0f) {
            this.h -= 360.0f;
        }
        if (this.h < 120.0f) {
            sat_temp_r = (120.0f - this.h) / 60.0f;
            sat_temp_g = this.h / 60.0f;
            sat_temp_b = 0.0f;
        } else if (this.h < 240.0f) {
            sat_temp_r = 0.0f;
            sat_temp_g = (240.0f - this.h) / 60.0f;
            sat_temp_b = (this.h - 120.0f) / 60.0f;
        } else {
            sat_temp_r = (this.h - 240.0f) / 60.0f;
            sat_temp_g = 0.0f;
            sat_temp_b = (360.0f - this.h) / 60.0f;
        }
        if (sat_temp_r > 1.0f) {
            sat_temp_r = 1.0f;
        }
        if (sat_temp_g > 1.0f) {
            sat_temp_g = 1.0f;
        }
        if (sat_temp_b > 1.0f) {
            sat_temp_b = 1.0f;
        }
        float sat_temp_r2 = (this.s * 2.0f * sat_temp_r) + (1.0f - this.s);
        float sat_temp_g2 = (this.s * 2.0f * sat_temp_g) + (1.0f - this.s);
        float sat_temp_b2 = (this.s * 2.0f * sat_temp_b) + (1.0f - this.s);
        if (((double) this.l) < 0.5d) {
            convertedColor.red = this.l * sat_temp_r2;
            convertedColor.green = this.l * sat_temp_g2;
            convertedColor.blue = this.l * sat_temp_b2;
        } else {
            convertedColor.red = (((1.0f - this.l) * sat_temp_r2) + (this.l * 2.0f)) - 1.0f;
            convertedColor.green = (((1.0f - this.l) * sat_temp_g2) + (this.l * 2.0f)) - 1.0f;
            convertedColor.blue = (((1.0f - this.l) * sat_temp_b2) + (this.l * 2.0f)) - 1.0f;
        }
        return convertedColor;
    }
}
