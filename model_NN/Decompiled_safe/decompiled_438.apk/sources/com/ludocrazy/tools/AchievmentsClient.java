package com.ludocrazy.tools;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import java.io.OutputStream;
import javax.microedition.khronos.opengles.GL10;

public class AchievmentsClient {
    public static final int CONST_SOURCEID_TOTEMS_REWARDS = 201;
    public static final int CONST_SOURCEID_TOTEMS_SCREENSHOTS = 200;
    private static final int LOGLEVEL = 2;
    private static final String SERVER_IP_IKATCH = "62.75.186.197";
    public static final int SERVER_PORT_IKATCH = 16001;

    public static void takeAndUploadScreenshot(Activity ActivityAndContext, LogOutput DebugLog, GL10 gl, int x, int y, int w, int h, Bitmap use_this_already_taken_screenshot__null_if_taking_desired, int image_source_id) throws Exception {
        try {
            TCPClient tcp_c = new TCPClient(DebugLog, 2, SERVER_IP_IKATCH, 16001);
            OutputStream tcp_os = tcp_c.connect();
            if (tcp_os != null) {
                int user_id = getUserId(ActivityAndContext, DebugLog, tcp_c);
                tcp_c.sendMessage("sP_" + Integer.toString(user_id) + "_" + Integer.toString(image_source_id));
                if (use_this_already_taken_screenshot__null_if_taking_desired == null) {
                    ScreenshotOpenGLES.SaveScreenshot_ToServer(DebugLog, tcp_os, gl, x, y, w, h, -1);
                } else {
                    ScreenshotOpenGLES.SaveBitmap_ToServer(DebugLog, tcp_os, use_this_already_taken_screenshot__null_if_taking_desired, -1);
                }
                tcp_c.disconnect();
                openBrowserWithURL(ActivityAndContext, "http://mogu.ikatch.de/picserv/index.php?s=" + user_id + "&f=1");
            }
        } catch (Exception e) {
            if (use_this_already_taken_screenshot__null_if_taking_desired == null) {
                new ErrorOutput("AchievmentsClient.takeAndUploadScreenshot", "Exception", e);
                return;
            }
            throw e;
        }
    }

    protected static int getUserId(Context context, LogOutput DebugLog, TCPClient tcp_c) throws Exception {
        SharedPreferences prefs = context.getSharedPreferences("com.ludocrazy.achievments", 1);
        int userid = prefs.getInt("UserId", -1);
        DebugLog.log(2, "ScreenshotAchievment.getUserId(): locally saved userid: " + userid);
        if (userid < 0) {
            tcp_c.sendMessage("cU");
            DebugLog.log(2, "ScreenshotAchievment.getUserId(): send user id request!");
            String user_id = tcp_c.receiveMessage();
            if (user_id.length() > 0) {
                userid = Integer.parseInt(user_id);
                if (userid < 0) {
                    throw new Exception("UserId received must be greater zero! is:" + userid);
                }
                DebugLog.log(2, "ScreenshotAchievment.getUserId(): received user id:" + userid);
                SharedPreferences.Editor editor = prefs.edit();
                editor.putInt("UserId", userid);
                editor.commit();
            } else {
                throw new Exception("No UserId received!");
            }
        }
        return userid;
    }

    protected static void openBrowserWithURL(Activity ActivityAndContext, String url) {
        Intent i = new Intent("android.intent.action.VIEW");
        i.setData(Uri.parse(url));
        ActivityAndContext.startActivity(i);
    }
}
