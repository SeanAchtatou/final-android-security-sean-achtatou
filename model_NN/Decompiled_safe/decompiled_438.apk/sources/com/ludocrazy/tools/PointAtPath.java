package com.ludocrazy.tools;

public class PointAtPath {
    public float currentDistance;
    public float currentFactor;
    public int currentPointId;
    public Vector3D currentPos3D = new Vector3D();
    public float currentPosition;
    public int nextPointId;
    public float totalPercentage;

    public PointAtPath(int start_offset) {
        this.currentPointId = start_offset;
        this.nextPointId = this.currentPointId + 1;
        this.currentPosition = 0.0f;
    }
}
