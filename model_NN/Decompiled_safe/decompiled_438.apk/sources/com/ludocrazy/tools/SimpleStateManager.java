package com.ludocrazy.tools;

import javax.microedition.khronos.opengles.GL10;

public class SimpleStateManager {
    private SimpleState m_CurrentState = new SimpleState(this, null);

    private class SimpleState {
        ColorFloats color_for_all_vertices;
        boolean vertices_suply_colors;

        private SimpleState() {
            this.vertices_suply_colors = true;
            this.color_for_all_vertices = new ColorFloats(1.0f, 1.0f, 1.0f, 1.0f);
        }

        /* synthetic */ SimpleState(SimpleStateManager simpleStateManager, SimpleState simpleState) {
            this();
        }
    }

    public void enableVerticesSuplyColors(GL10 gl) {
        if (!this.m_CurrentState.vertices_suply_colors) {
            this.m_CurrentState.vertices_suply_colors = true;
            gl.glEnableClientState(32886);
        }
    }

    public void disableVerticesSuplyColors(GL10 gl, ColorFloats color_for_all_vertices) {
        boolean changed = false;
        if (this.m_CurrentState.vertices_suply_colors) {
            this.m_CurrentState.vertices_suply_colors = false;
            gl.glDisableClientState(32886);
            changed = true;
        }
        if (changed || !this.m_CurrentState.color_for_all_vertices.identical_color_parts(color_for_all_vertices)) {
            this.m_CurrentState.color_for_all_vertices = color_for_all_vertices;
            gl.glColor4f(this.m_CurrentState.color_for_all_vertices.red, this.m_CurrentState.color_for_all_vertices.green, this.m_CurrentState.color_for_all_vertices.blue, this.m_CurrentState.color_for_all_vertices.alpha);
        }
    }
}
