package com.ludocrazy.tools;

public class GuiBackgroundColors {
    ColorFloats bottomColor = null;
    ColorFloats topColor = null;

    public GuiBackgroundColors(float topr, float topg, float topb, float bottomr, float bottomg, float bottomb) {
        this.topColor = new ColorFloats(topr, topg, topb);
        this.bottomColor = new ColorFloats(bottomr, bottomg, bottomb);
    }

    /* access modifiers changed from: package-private */
    public void setAlpha(float alpha) {
        this.topColor.alpha = alpha;
        this.bottomColor.alpha = alpha;
    }
}
