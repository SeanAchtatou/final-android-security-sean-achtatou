package com.ludocrazy.tools;

import android.content.Context;
import android.content.SharedPreferences;
import java.util.Vector;

public class AchievmentsStorage {
    private LogOutput DebugLogOutput = null;
    protected Vector<String> m_Achievments = new Vector<>();
    protected Context m_Context = null;
    private String m_CountString = null;
    private int m_CurrentAchievment = 0;
    private String m_KeyPreString = null;
    private SharedPreferences m_Prefs = null;

    public AchievmentsStorage(Context context, LogOutput DebugLog, String sharedprefs_name, String count_string, String key_pre_string) {
        this.DebugLogOutput = DebugLog;
        this.m_Context = context;
        this.m_Prefs = this.m_Context.getSharedPreferences(sharedprefs_name, 1);
        this.m_CountString = count_string;
        this.m_KeyPreString = key_pre_string;
        this.m_CurrentAchievment = this.m_Prefs.getInt(count_string, this.m_CurrentAchievment);
        for (int i = 1; i <= this.m_CurrentAchievment; i++) {
            String key = String.valueOf(key_pre_string) + i;
            String achievment_data = this.m_Prefs.getString(key, "none");
            this.m_Achievments.add(achievment_data);
            this.DebugLogOutput.log(2, "AchievmentsManager found: " + key + ", containing: " + achievment_data);
        }
    }

    public void resetAndRemoveAllPreferences() {
        SharedPreferences.Editor editor = this.m_Prefs.edit();
        editor.clear();
        editor.commit();
    }

    public int getAchievmentsCount() {
        return this.m_Achievments.size();
    }

    /* access modifiers changed from: protected */
    public boolean addAchievment(String type_and_data, boolean allow_duplicates) {
        if (!allow_duplicates) {
            for (int i = 0; i < this.m_Achievments.size(); i++) {
                if (this.m_Achievments.elementAt(i).compareTo(type_and_data) == 0) {
                    return false;
                }
            }
        }
        this.m_CurrentAchievment++;
        this.m_Achievments.add(type_and_data);
        SharedPreferences.Editor editor = this.m_Prefs.edit();
        editor.putString(String.valueOf(this.m_KeyPreString) + this.m_CurrentAchievment, type_and_data);
        editor.putInt(this.m_CountString, this.m_CurrentAchievment);
        editor.commit();
        this.DebugLogOutput.log(1, "AchievmentsManager addAchievment: " + this.m_CurrentAchievment + ", containing: " + type_and_data);
        return true;
    }
}
