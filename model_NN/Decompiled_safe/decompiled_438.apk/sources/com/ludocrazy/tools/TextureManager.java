package com.ludocrazy.tools;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.GLUtils;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.IntBuffer;
import java.util.Vector;
import javax.microedition.khronos.opengles.GL10;

public class TextureManager {
    private LogOutput DebugLogOutput = null;
    private Context m_Context = null;
    private PhoneAbilities m_PhoneAbilities = null;
    Vector<TextureStruct> m_Textures = new Vector<>();

    private class TextureStruct {
        int m_Height;
        int m_RessourceId;
        int[] m_TextureId = new int[1];
        int m_Width;

        TextureStruct(int res_id, int width, int height) {
            this.m_RessourceId = res_id;
            this.m_Width = width;
            this.m_Height = height;
        }

        public int getSquareSize() {
            if (this.m_Width == this.m_Height) {
                return this.m_Width;
            }
            return -1;
        }
    }

    public TextureManager(Context context, LogOutput debugLogOutput, PhoneAbilities phoneAbilities) {
        this.m_Context = context;
        this.DebugLogOutput = debugLogOutput;
        this.m_PhoneAbilities = phoneAbilities;
    }

    /* Debug info: failed to restart local var, previous not found, register: 5 */
    public int getTextureHandle(int ressource_id) throws Exception {
        int size = this.m_Textures.size();
        for (int i = 0; i < size; i++) {
            if (this.m_Textures.get(i).m_RessourceId == ressource_id) {
                return this.m_Textures.get(i).m_TextureId[0];
            }
        }
        throw new Exception("TextureManager: Couldn't find texture for ressource_id: " + ressource_id);
    }

    private int findTexture(int ressource_id) {
        for (int i = 0; i < this.m_Textures.size(); i++) {
            if (this.m_Textures.elementAt(i).m_RessourceId == ressource_id) {
                return i;
            }
        }
        return -1;
    }

    /* Debug info: failed to restart local var, previous not found, register: 2 */
    public int getTextureDimension_SquareSize(int ressource_id) {
        int element = findTexture(ressource_id);
        if (element < 0) {
            return -1;
        }
        return this.m_Textures.elementAt(element).getSquareSize();
    }

    public void useTexture(GL10 gl, int ressource_id) throws Exception {
        gl.glBindTexture(3553, getTextureHandle(ressource_id));
    }

    public void addTexture(GL10 gl, int resource_id, boolean mipmapped_filtering, boolean alpha_channel_needed, boolean convert_to_16bit_for_speed) throws Exception {
        gl.glEnable(3553);
        StopWatch stopwatch = new StopWatch();
        InputStream instream = this.m_Context.getResources().openRawResource(resource_id);
        try {
            Bitmap bitmap = BitmapFactory.decodeStream(instream);
            if (bitmap == null) {
                throw new Exception("BitmapFactory.decodeStream couldn't decode Image data for bitmap id:" + Integer.toHexString(resource_id));
            }
            int width = bitmap.getWidth();
            int height = bitmap.getHeight();
            if (width != height) {
                throw new Exception("TextureManager.addTexture() non square texture given! res id:" + Integer.toHexString(resource_id) + " width:" + width + " height:" + height);
            }
            int width2 = this.m_PhoneAbilities.checkMinMaxValue(height, 1);
            if (width2 != height) {
                throw new Exception("TextureManager.addTexture() texture size exceeds what is allowed on device! res id:" + Integer.toHexString(resource_id) + " allowed:" + width2 + " given:" + height);
            }
            if (convert_to_16bit_for_speed && this.m_PhoneAbilities.isSupported(0)) {
                if (alpha_channel_needed) {
                    bitmap = bitmap.copy(Bitmap.Config.ARGB_4444, false);
                    this.DebugLogOutput.log(2, "addTexture() CONVERTED bitmap to 16bit RGBA_4444 with alpha!");
                } else {
                    bitmap = bitmap.copy(Bitmap.Config.RGB_565, false);
                    this.DebugLogOutput.log(2, "addTexture() CONVERTED bitmap to 16bit RGB_565 without alpha!");
                }
            }
            try {
            } catch (Exception e) {
                new ErrorOutput("TextureManager::addTexture()", "Exception closing stream, bitmap id:" + resource_id, e);
            }
            try {
                TextureStruct textureStruct = new TextureStruct(resource_id, width2, height);
                gl.glGenTextures(1, textureStruct.m_TextureId, 0);
                gl.glBindTexture(3553, textureStruct.m_TextureId[0]);
                if (!mipmapped_filtering) {
                    GLUtils.texImage2D(3553, 0, bitmap, 0);
                } else if (this.m_PhoneAbilities.isSupported(1)) {
                    this.DebugLogOutput.log(2, "TextureManager.addTexture() using GENERATE_MIPMAP on texture id: " + resource_id);
                    gl.glTexParameterf(3553, 33169, 1.0f);
                    GLUtils.texImage2D(3553, 0, bitmap, 0);
                } else {
                    buildMipmap_Manually(gl, bitmap, !this.m_PhoneAbilities.isSupported(1));
                }
                gl.glTexParameterf(3553, 10242, 10497.0f);
                gl.glTexParameterf(3553, 10243, 10497.0f);
                gl.glTexParameterf(3553, 10240, 9729.0f);
                if (mipmapped_filtering) {
                    gl.glTexParameterf(3553, 10241, 9985.0f);
                } else {
                    gl.glTexParameterf(3553, 10241, 9728.0f);
                }
                this.m_Textures.add(textureStruct);
                bitmap.recycle();
            } catch (Exception e2) {
                Exception e3 = e2;
                int bitmapwidth = -1;
                int bitmapheight = -1;
                boolean alpha = false;
                if (bitmap != null) {
                    bitmapwidth = bitmap.getWidth();
                    bitmapheight = bitmap.getHeight();
                    alpha = bitmap.hasAlpha();
                }
                new ErrorOutput("TextureManager::addTexture()", "Error loading bitmap id: " + resource_id + ", dimensions: " + bitmapwidth + "x" + bitmapheight + ", has alpha: " + alpha + ". Exception", e3);
            }
            long duration = stopwatch.getElapsedMilliSeconds();
            if (duration > 100) {
                this.DebugLogOutput.log(1, "performance: loading texture " + resource_id + " took " + duration);
            }
        } finally {
            try {
                instream.close();
            } catch (Exception e4) {
                new ErrorOutput("TextureManager::addTexture()", "Exception closing stream, bitmap id:" + resource_id, e4);
            }
        }
    }

    public static void buildMipmap_Manually(GL10 gl, Bitmap bitmap, boolean convert_abgr_to_argb) throws Exception {
        int height = bitmap.getHeight();
        int width = bitmap.getWidth();
        if (height != width) {
            throw new Exception("TextureManager::buildMipmap() given bitmap is not of square size. it's height:" + height + " is not equal to it's width:" + width);
        }
        if (convert_abgr_to_argb) {
            try {
                int[] pixels = new int[(width * height)];
                int stride = width;
                bitmap.getPixels(pixels, 0, stride, 0, 0, width, height);
                for (int i = 0; i < pixels.length; i++) {
                    int pixel = pixels[i];
                    pixels[i] = (pixel & -16711936) | ((pixel & 255) << 16) | ((16711680 & pixel) >> 16);
                }
                bitmap = Bitmap.createBitmap(pixels, 0, stride, width, height, bitmap.getConfig());
            } catch (Exception e) {
                throw new Exception("TextureManager::buildMipmap() Exception while converting ABGR to ARGB: " + e);
            }
        }
        int level = 0;
        while (true) {
            if (height >= 1 || width >= 1) {
                imageToTexture2D(gl, bitmap, level);
                if (height != 1 && width != 1) {
                    int level2 = level + 1;
                    height /= 2;
                    width /= 2;
                    Bitmap bitmap2 = Bitmap.createScaledBitmap(bitmap, width, height, false);
                    bitmap.recycle();
                    bitmap = bitmap2;
                    level = level2;
                } else {
                    return;
                }
            } else {
                return;
            }
        }
    }

    private static void imageToTexture2D(GL10 gl, Bitmap bmp, int level) {
        int width = bmp.getWidth();
        int height = bmp.getHeight();
        ByteBuffer bytebuf = ByteBuffer.allocateDirect(width * height * 4);
        bytebuf.order(ByteOrder.nativeOrder());
        IntBuffer pixelbuf = bytebuf.asIntBuffer();
        int[] pixels = new int[(width * height)];
        bmp.getPixels(pixels, 0, width, 0, 0, width, height);
        pixelbuf.put(pixels);
        pixelbuf.position(0);
        gl.glTexImage2D(3553, level, 6408, width, height, 0, 6408, 5121, pixelbuf);
    }

    public static void imageToTexture2D_SubTexture(GL10 gl, Bitmap bmp, int level, int xoffset, int yoffset) {
        int width = bmp.getWidth();
        int height = bmp.getHeight();
        ByteBuffer bytebuf = ByteBuffer.allocateDirect(width * height * 4);
        bytebuf.order(ByteOrder.nativeOrder());
        IntBuffer pixelbuf = bytebuf.asIntBuffer();
        int[] pixels = new int[(width * height)];
        bmp.getPixels(pixels, 0, width, 0, 0, width, height);
        pixelbuf.put(pixels);
        pixelbuf.position(0);
        gl.glTexSubImage2D(3553, level, xoffset, yoffset, width, height, 6408, 5121, pixelbuf);
    }

    private static void imageToSubTexture2D(GL10 gl, Bitmap bmp, int level, int xoffset, int yoffset) {
        int width = bmp.getWidth();
        int height = bmp.getHeight();
        ByteBuffer bytebuf = ByteBuffer.allocateDirect(width * height * 4);
        bytebuf.order(ByteOrder.nativeOrder());
        IntBuffer pixelbuf = bytebuf.asIntBuffer();
        int[] pixels = new int[(width * height)];
        bmp.getPixels(pixels, 0, width, 0, 0, width, height);
        pixelbuf.put(pixels);
        pixelbuf.position(0);
        gl.glTexSubImage2D(3553, level, xoffset, yoffset, width, height, 6408, 5121, pixelbuf);
    }

    /* JADX INFO: Multiple debug info for r0v7 int: [D('y' int), D('x' int)] */
    private static void imageToTexture2D_Scaling(GL10 gl, Bitmap bmp, int level, int target_width, int target_height) {
        int width = bmp.getWidth();
        int height = bmp.getHeight();
        ByteBuffer bytebuf = ByteBuffer.allocateDirect(target_width * target_height * 4);
        bytebuf.order(ByteOrder.nativeOrder());
        IntBuffer pixelbuf = bytebuf.asIntBuffer();
        int ystepping = height / target_height;
        int xstepping = width / target_width;
        for (int y = 0; y < height; y += ystepping) {
            for (int x = 0; x < width; x += xstepping) {
                pixelbuf.put(bmp.getPixel(x, y));
            }
        }
        pixelbuf.position(0);
        gl.glTexImage2D(3553, level, 6408, target_width, target_height, 0, 6408, 5121, pixelbuf);
    }
}
