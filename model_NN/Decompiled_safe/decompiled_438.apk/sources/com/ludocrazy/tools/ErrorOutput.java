package com.ludocrazy.tools;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

public class ErrorOutput {
    static Context m_Context = null;
    static String m_PackageName = null;

    public ErrorOutput(Context context, String packageName) {
        m_Context = context;
        m_PackageName = packageName;
    }

    public ErrorOutput(String sourceName, String Message) {
        Log.e(m_PackageName, String.valueOf(sourceName) + " " + Message);
    }

    public ErrorOutput(String sourceName, String Message, Exception e) {
        if (e != null) {
            Log.e(m_PackageName, String.valueOf(sourceName) + " " + Message, e);
            try {
                Toast.makeText(m_Context, String.valueOf(sourceName) + " " + Message + ":" + e, 1).show();
            } catch (Exception e2) {
            }
        } else {
            Log.e(m_PackageName, String.valueOf(sourceName) + " " + Message);
            try {
                Toast.makeText(m_Context, String.valueOf(sourceName) + " " + Message, 1).show();
            } catch (Exception e3) {
            }
        }
    }
}
