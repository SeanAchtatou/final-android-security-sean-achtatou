package com.ludocrazy.tools;

import java.util.Vector;
import javax.microedition.khronos.opengles.GL10;

public class GuiBase {
    boolean m_AlwaysVisible = false;
    protected float m_BasisTextScale = 0.017f;
    float m_Bottom = 0.0f;
    Vector<GuiBase> m_Children = new Vector<>();
    float m_Left = 0.0f;
    float m_OrthoCameraMaxX = 0.0f;
    float m_OrthoCameraMaxY = 0.0f;
    float m_OrthoCameraMinX = 0.0f;
    float m_OrthoCameraMinY = 0.0f;
    GuiBase m_Parent = null;
    public boolean m_RedrawNeeded = true;
    float m_Right = 0.0f;
    float m_Top = 0.0f;
    boolean m_Touchable = true;
    boolean m_VisibleAndUseable = true;
    boolean m_VisibleOnOrthoCamera = false;

    public void setBasisTextScale(float new_scale) {
        this.m_BasisTextScale = new_scale;
    }

    public float getBasisTextScale() {
        return this.m_BasisTextScale;
    }

    public void setRedrawNeeded() {
        this.m_RedrawNeeded = true;
        if (this.m_Parent != null) {
            this.m_Parent.setRedrawNeeded();
        }
    }

    public void onResume() {
        int size = this.m_Children.size();
        for (int i = 0; i < size; i++) {
            this.m_Children.elementAt(i).onResume();
        }
    }

    public void addChild(GuiBase newChild) throws Exception {
        if (newChild == null) {
            throw new Exception("GuiBase::addChild() can't add NULL pointer!");
        }
        this.m_Children.add(newChild);
    }

    public boolean checkAndHandleTouchDown(float x_percentage, float y_percentage) {
        boolean touchDown = false;
        if (this.m_VisibleOnOrthoCamera && this.m_Touchable) {
            for (int i = this.m_Children.size() - 1; i >= 0; i--) {
                touchDown = this.m_Children.elementAt(i).checkAndHandleTouchDown(x_percentage, y_percentage);
                if (touchDown) {
                    break;
                }
            }
        }
        return touchDown;
    }

    public boolean checkAndHandleTouchUp(float x_percentage, float y_percentage) {
        boolean touchUp = false;
        if (this.m_VisibleOnOrthoCamera && this.m_Touchable) {
            for (int i = this.m_Children.size() - 1; i >= 0; i--) {
                touchUp = this.m_Children.elementAt(i).checkAndHandleTouchUp(x_percentage, y_percentage);
                if (touchUp) {
                    break;
                }
            }
        }
        return touchUp;
    }

    public boolean draw(GL10 gl) throws Exception {
        this.m_RedrawNeeded = false;
        if (this.m_VisibleOnOrthoCamera) {
            int size = this.m_Children.size();
            for (int i = 0; i < size; i++) {
                this.m_RedrawNeeded = this.m_Children.elementAt(i).draw(gl) || this.m_RedrawNeeded;
            }
        }
        return this.m_RedrawNeeded;
    }

    public boolean updateVisiblity(float orthoCameraMinX, float orthoCameraMinY, float orthoCameraMaxX, float orthoCameraMaxY) {
        this.m_OrthoCameraMinX = orthoCameraMinX;
        this.m_OrthoCameraMinY = orthoCameraMinY;
        this.m_OrthoCameraMaxX = orthoCameraMaxX;
        this.m_OrthoCameraMaxY = orthoCameraMaxY;
        if (!this.m_VisibleAndUseable || (!this.m_AlwaysVisible && (this.m_Left > orthoCameraMaxX || this.m_Right < orthoCameraMinX || this.m_Top > orthoCameraMaxY || this.m_Bottom < orthoCameraMinY))) {
            this.m_VisibleOnOrthoCamera = false;
        } else {
            this.m_VisibleOnOrthoCamera = true;
            int size = this.m_Children.size();
            for (int i = 0; i < size; i++) {
                this.m_Children.elementAt(i).updateVisiblity(orthoCameraMinX, orthoCameraMinY, orthoCameraMaxX, orthoCameraMaxY);
            }
        }
        return this.m_VisibleOnOrthoCamera;
    }

    public boolean isVisibleOnOrthoCamera() {
        return this.m_VisibleOnOrthoCamera;
    }

    public void setAlwaysVisible(boolean visible) {
        this.m_AlwaysVisible = visible;
    }

    public void setVisibleAndUseable(boolean visible) {
        this.m_VisibleAndUseable = visible;
        this.m_VisibleOnOrthoCamera = false;
    }

    public void setTouchable(boolean touchable) {
        this.m_Touchable = touchable;
    }
}
