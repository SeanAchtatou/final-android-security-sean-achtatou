package com.ludocrazy.tools;

import android.content.Context;

public class ShapeMeshLoader extends ShapeFileReader {
    protected int m_AdditionalTriangles = 0;
    protected Coords m_AppendOffset = new Coords(0.0f, 0.0f, 0.0f);
    protected ColorFloats m_ColorPart0 = new ColorFloats(0.05f, 0.05f, 0.05f);
    protected ColorFloats m_ColorPart1 = new ColorFloats(1.0f, 1.0f, 1.0f);
    protected ColorFloats m_ColorPart2 = new ColorFloats(1.0f, 0.75f, 0.0f);
    protected LogOutput m_DebugLog = null;
    protected LineMesh m_OutlineMesh = null;
    protected PhoneAbilities m_PhoneAbilities = null;
    protected BasicMesh m_StaticMesh = null;

    public void setPartColors(ColorFloats part0, ColorFloats part1) {
        this.m_ColorPart0 = part0;
        this.m_ColorPart1 = part1;
    }

    public void prepareToExtendMesh_NotOutlines(int space_to_reserve_for_additional_triangles) {
        this.m_AdditionalTriangles = space_to_reserve_for_additional_triangles;
    }

    public void initMesh(LogOutput debugLog, PhoneAbilities phoneAbilities, Context context, String asset_filename, float scale, BasicMesh mesh_for_append, Coords append_offset) {
        this.m_StaticMesh = mesh_for_append;
        this.m_AppendOffset = append_offset;
        initMesh(debugLog, phoneAbilities, context, asset_filename, scale);
    }

    public void initMesh(LogOutput debugLog, PhoneAbilities phoneAbilities, Context context, String asset_filename, float scale) {
        this.m_DebugLog = debugLog;
        this.m_PhoneAbilities = phoneAbilities;
        readShapeFile(context, asset_filename, scale);
        if (this.m_AdditionalTriangles == 0) {
            convertArray_finalize();
        }
    }

    public void convertArray_finalize() {
        this.m_StaticMesh.convertArraysToBuffer();
        this.m_OutlineMesh.convertArraysToBuffer();
    }

    public BasicMesh getStaticMesh() {
        return this.m_StaticMesh;
    }

    public BasicMesh getOutlineMesh() {
        return this.m_OutlineMesh;
    }

    /* access modifiers changed from: protected */
    public void prepareMeshes(int total_triangles, int[] part_triangle_counts, int count_of_parts, int outline_count) throws Exception {
        if (this.m_StaticMesh == null) {
            this.m_StaticMesh = new BasicMesh(this.m_DebugLog, this.m_PhoneAbilities);
            this.m_StaticMesh.setupArraysTriangleFaces(this.m_AdditionalTriangles + total_triangles);
        }
        this.m_OutlineMesh = new LineMesh(this.m_DebugLog, this.m_PhoneAbilities);
        this.m_OutlineMesh.setupArraysLines(outline_count);
    }

    /* access modifiers changed from: protected */
    public void addTriangle(Coords pos1, Coords pos2, Coords pos3, int current_part) throws Exception {
        ColorFloats cur_col = this.m_ColorPart0;
        if (current_part >= 1) {
            cur_col = this.m_ColorPart1;
        }
        pos1.add(this.m_AppendOffset);
        pos2.add(this.m_AppendOffset);
        pos3.add(this.m_AppendOffset);
        this.m_StaticMesh.addTriangleFace_colored(pos1, cur_col, pos2, cur_col, pos3, cur_col);
    }

    /* access modifiers changed from: protected */
    public void addCircle(int circle_count_of_triangles, Coords pos1, float radius, float starting_angle_degree, float ending_angle_degree, int current_part) throws Exception {
        ColorFloats cur_col = this.m_ColorPart0;
        if (current_part >= 1) {
            cur_col = this.m_ColorPart1;
        }
        pos1.add(this.m_AppendOffset);
        this.m_StaticMesh.addTriangledCircle_colored(circle_count_of_triangles, pos1, radius, starting_angle_degree, ending_angle_degree, cur_col, cur_col);
    }

    /* access modifiers changed from: protected */
    public void addLine(Coords pos1, Coords pos2) throws Exception {
        pos1.add(this.m_AppendOffset);
        pos2.add(this.m_AppendOffset);
        this.m_OutlineMesh.addLine(pos1, pos2);
    }
}
