package com.ludocrazy.tools;

public class Vector2Dint {
    public int x;
    public int y;

    public Vector2Dint(int initial_x, int initial_y) {
        this.x = initial_x;
        this.y = initial_y;
    }

    public void set(int new_x, int new_y) {
        this.x = new_x;
        this.y = new_y;
    }
}
