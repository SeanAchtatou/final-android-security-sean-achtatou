package com.ludocrazy.tools;

import java.util.Vector;

public class WeightedRandomizer {
    protected Vector<WeightedRandomizerEntry> m_Entries = new Vector<>();
    protected int m_TotalWeight = 0;

    public class WeightedRandomizerEntry {
        public int data;
        public int num;
        public int weight;

        public WeightedRandomizerEntry() {
        }
    }

    public void addEntry(int weight, int data) {
        WeightedRandomizerEntry newWRE = new WeightedRandomizerEntry();
        newWRE.weight = weight;
        newWRE.data = data;
        newWRE.num = this.m_Entries.size();
        this.m_Entries.add(newWRE);
        this.m_TotalWeight += weight;
    }

    /* Debug info: failed to restart local var, previous not found, register: 4 */
    public WeightedRandomizerEntry getRandomEntry() {
        int choice = Tools.randomInt(0, this.m_TotalWeight);
        int randomized_result = -1;
        while (choice > 0) {
            randomized_result++;
            choice -= this.m_Entries.elementAt(randomized_result).weight;
        }
        return this.m_Entries.elementAt(randomized_result);
    }

    public int size() {
        return this.m_Entries.size();
    }
}
