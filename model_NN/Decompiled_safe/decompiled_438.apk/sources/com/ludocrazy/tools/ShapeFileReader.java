package com.ludocrazy.tools;

public class ShapeFileReader {
    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x00da, code lost:
        throw new java.lang.Exception("Given parts_count not in range (1 to 9) with: " + r13);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void readShapeFile(android.content.Context r33, java.lang.String r34, float r35) {
        /*
            r32 = this;
            android.content.res.AssetManager r5 = r33.getAssets()     // Catch:{ Exception -> 0x0039 }
            r6 = 2
            r0 = r5
            r1 = r34
            r2 = r6
            java.io.InputStream r19 = r0.open(r1, r2)     // Catch:{ Exception -> 0x0039 }
            java.io.BufferedReader r12 = new java.io.BufferedReader     // Catch:{ Exception -> 0x0039 }
            java.io.InputStreamReader r5 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x0039 }
            r0 = r5
            r1 = r19
            r0.<init>(r1)     // Catch:{ Exception -> 0x0039 }
            r6 = 16384(0x4000, float:2.2959E-41)
            r12.<init>(r5, r6)     // Catch:{ Exception -> 0x0039 }
            r30 = 0
            r13 = 0
            r22 = 0
            int[] r22 = (int[]) r22     // Catch:{ Exception -> 0x0039 }
            r21 = 0
            r27 = -1081199297(0xffffffffbf8e353f, float:-1.111)
            r11 = -1
        L_0x0029:
            java.lang.String r20 = r12.readLine()     // Catch:{ Exception -> 0x0039 }
            if (r20 != 0) goto L_0x0061
        L_0x002f:
            if (r30 != 0) goto L_0x0179
            java.lang.Exception r5 = new java.lang.Exception     // Catch:{ Exception -> 0x0039 }
            java.lang.String r6 = "Trying to read 'Sha file' version tag from header, but wasn't found in header!"
            r5.<init>(r6)     // Catch:{ Exception -> 0x0039 }
            throw r5     // Catch:{ Exception -> 0x0039 }
        L_0x0039:
            r5 = move-exception
            r16 = r5
            com.ludocrazy.tools.ErrorOutput r5 = new com.ludocrazy.tools.ErrorOutput
            java.lang.String r6 = "ShapeFileReader::readShapeFile()"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            java.lang.String r8 = "Error reading shape file at: "
            r7.<init>(r8)
            r0 = r7
            r1 = r34
            java.lang.StringBuilder r7 = r0.append(r1)
            java.lang.String r8 = ". Exception"
            java.lang.StringBuilder r7 = r7.append(r8)
            java.lang.String r7 = r7.toString()
            r0 = r5
            r1 = r6
            r2 = r7
            r3 = r16
            r0.<init>(r1, r2, r3)
        L_0x0060:
            return
        L_0x0061:
            java.lang.String r5 = "# part"
            r0 = r20
            r1 = r5
            boolean r5 = r0.startsWith(r1)     // Catch:{ Exception -> 0x0039 }
            if (r5 == 0) goto L_0x006e
            r11 = 0
            goto L_0x002f
        L_0x006e:
            java.lang.String r5 = "Sha file"
            r0 = r20
            r1 = r5
            boolean r5 = r0.startsWith(r1)     // Catch:{ Exception -> 0x0039 }
            if (r5 == 0) goto L_0x00a6
            r5 = 10
            r0 = r20
            r1 = r5
            java.lang.String r5 = r0.substring(r1)     // Catch:{ Exception -> 0x0039 }
            float r29 = java.lang.Float.parseFloat(r5)     // Catch:{ Exception -> 0x0039 }
            r5 = 1073741824(0x40000000, float:2.0)
            int r5 = (r29 > r5 ? 1 : (r29 == r5 ? 0 : -1))
            if (r5 >= 0) goto L_0x00a4
            java.lang.Exception r5 = new java.lang.Exception     // Catch:{ Exception -> 0x0039 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0039 }
            java.lang.String r7 = "Expecting Sha file version 2.0 or higher, found: "
            r6.<init>(r7)     // Catch:{ Exception -> 0x0039 }
            r0 = r6
            r1 = r29
            java.lang.StringBuilder r6 = r0.append(r1)     // Catch:{ Exception -> 0x0039 }
            java.lang.String r6 = r6.toString()     // Catch:{ Exception -> 0x0039 }
            r5.<init>(r6)     // Catch:{ Exception -> 0x0039 }
            throw r5     // Catch:{ Exception -> 0x0039 }
        L_0x00a4:
            r30 = 1
        L_0x00a6:
            java.lang.String r5 = "parts_count"
            r0 = r20
            r1 = r5
            boolean r5 = r0.startsWith(r1)     // Catch:{ Exception -> 0x0039 }
            if (r5 == 0) goto L_0x00e7
            r5 = 12
            r0 = r20
            r1 = r5
            java.lang.String r5 = r0.substring(r1)     // Catch:{ Exception -> 0x0039 }
            r6 = 10
            int r13 = java.lang.Integer.parseInt(r5, r6)     // Catch:{ Exception -> 0x0039 }
            if (r13 <= 0) goto L_0x00c6
            r5 = 10
            if (r13 < r5) goto L_0x00db
        L_0x00c6:
            java.lang.Exception r5 = new java.lang.Exception     // Catch:{ Exception -> 0x0039 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0039 }
            java.lang.String r7 = "Given parts_count not in range (1 to 9) with: "
            r6.<init>(r7)     // Catch:{ Exception -> 0x0039 }
            java.lang.StringBuilder r6 = r6.append(r13)     // Catch:{ Exception -> 0x0039 }
            java.lang.String r6 = r6.toString()     // Catch:{ Exception -> 0x0039 }
            r5.<init>(r6)     // Catch:{ Exception -> 0x0039 }
            throw r5     // Catch:{ Exception -> 0x0039 }
        L_0x00db:
            r0 = r13
            int[] r0 = new int[r0]     // Catch:{ Exception -> 0x0039 }
            r22 = r0
            r18 = 0
        L_0x00e2:
            r0 = r18
            r1 = r13
            if (r0 < r1) goto L_0x0172
        L_0x00e7:
            r5 = 0
            r6 = 4
            r0 = r20
            r1 = r5
            r2 = r6
            java.lang.String r5 = r0.substring(r1, r2)     // Catch:{ Exception -> 0x0039 }
            java.lang.String r6 = "part"
            boolean r5 = r5.startsWith(r6)     // Catch:{ Exception -> 0x0039 }
            if (r5 == 0) goto L_0x013f
            r5 = 4
            r6 = 5
            r0 = r20
            r1 = r5
            r2 = r6
            java.lang.String r5 = r0.substring(r1, r2)     // Catch:{ Exception -> 0x0039 }
            java.lang.String r6 = "s"
            boolean r5 = r5.startsWith(r6)     // Catch:{ Exception -> 0x0039 }
            if (r5 != 0) goto L_0x013f
            r5 = 5
            r6 = 12
            r0 = r20
            r1 = r5
            r2 = r6
            java.lang.String r5 = r0.substring(r1, r2)     // Catch:{ Exception -> 0x0039 }
            java.lang.String r6 = "_count="
            boolean r5 = r5.startsWith(r6)     // Catch:{ Exception -> 0x0039 }
            if (r5 == 0) goto L_0x013f
            r5 = 4
            r6 = 5
            r0 = r20
            r1 = r5
            r2 = r6
            java.lang.String r5 = r0.substring(r1, r2)     // Catch:{ Exception -> 0x0039 }
            r6 = 10
            int r23 = java.lang.Integer.parseInt(r5, r6)     // Catch:{ Exception -> 0x0039 }
            r5 = 12
            r0 = r20
            r1 = r5
            java.lang.String r5 = r0.substring(r1)     // Catch:{ Exception -> 0x0039 }
            r6 = 10
            int r5 = java.lang.Integer.parseInt(r5, r6)     // Catch:{ Exception -> 0x0039 }
            r22[r23] = r5     // Catch:{ Exception -> 0x0039 }
        L_0x013f:
            java.lang.String r5 = "outline_count"
            r0 = r20
            r1 = r5
            boolean r5 = r0.startsWith(r1)     // Catch:{ Exception -> 0x0039 }
            if (r5 == 0) goto L_0x0159
            r5 = 14
            r0 = r20
            r1 = r5
            java.lang.String r5 = r0.substring(r1)     // Catch:{ Exception -> 0x0039 }
            r6 = 10
            int r21 = java.lang.Integer.parseInt(r5, r6)     // Catch:{ Exception -> 0x0039 }
        L_0x0159:
            java.lang.String r5 = "scale"
            r0 = r20
            r1 = r5
            boolean r5 = r0.startsWith(r1)     // Catch:{ Exception -> 0x0039 }
            if (r5 == 0) goto L_0x0029
            r5 = 6
            r0 = r20
            r1 = r5
            java.lang.String r5 = r0.substring(r1)     // Catch:{ Exception -> 0x0039 }
            float r27 = java.lang.Float.parseFloat(r5)     // Catch:{ Exception -> 0x0039 }
            goto L_0x0029
        L_0x0172:
            r5 = -1
            r22[r18] = r5     // Catch:{ Exception -> 0x0039 }
            int r18 = r18 + 1
            goto L_0x00e2
        L_0x0179:
            r18 = 0
        L_0x017b:
            r0 = r18
            r1 = r13
            if (r0 < r1) goto L_0x018f
            r5 = -1081199297(0xffffffffbf8e353f, float:-1.111)
            int r5 = (r27 > r5 ? 1 : (r27 == r5 ? 0 : -1))
            if (r5 != 0) goto L_0x01b4
            java.lang.Exception r5 = new java.lang.Exception     // Catch:{ Exception -> 0x0039 }
            java.lang.String r6 = "Trying to read 'scale', but wasn't found in header!"
            r5.<init>(r6)     // Catch:{ Exception -> 0x0039 }
            throw r5     // Catch:{ Exception -> 0x0039 }
        L_0x018f:
            r5 = r22[r18]     // Catch:{ Exception -> 0x0039 }
            if (r5 >= 0) goto L_0x01b1
            java.lang.Exception r5 = new java.lang.Exception     // Catch:{ Exception -> 0x0039 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0039 }
            java.lang.String r7 = "Trying to read 'part"
            r6.<init>(r7)     // Catch:{ Exception -> 0x0039 }
            r0 = r6
            r1 = r18
            java.lang.StringBuilder r6 = r0.append(r1)     // Catch:{ Exception -> 0x0039 }
            java.lang.String r7 = "_count', but was <0 or wasn't found in header!"
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x0039 }
            java.lang.String r6 = r6.toString()     // Catch:{ Exception -> 0x0039 }
            r5.<init>(r6)     // Catch:{ Exception -> 0x0039 }
            throw r5     // Catch:{ Exception -> 0x0039 }
        L_0x01b1:
            int r18 = r18 + 1
            goto L_0x017b
        L_0x01b4:
            if (r11 >= 0) goto L_0x01be
            java.lang.Exception r5 = new java.lang.Exception     // Catch:{ Exception -> 0x0039 }
            java.lang.String r6 = "Trying to read '# part', but wasn't found at end of header!"
            r5.<init>(r6)     // Catch:{ Exception -> 0x0039 }
            throw r5     // Catch:{ Exception -> 0x0039 }
        L_0x01be:
            r28 = 0
            r18 = 0
        L_0x01c2:
            r0 = r18
            r1 = r13
            if (r0 < r1) goto L_0x021c
            r0 = r32
            r1 = r28
            r2 = r22
            r3 = r13
            r4 = r21
            r0.prepareMeshes(r1, r2, r3, r4)     // Catch:{ Exception -> 0x0039 }
            float r27 = r27 * r35
            com.ludocrazy.tools.Coords r7 = new com.ludocrazy.tools.Coords     // Catch:{ Exception -> 0x0039 }
            r7.<init>()     // Catch:{ Exception -> 0x0039 }
            com.ludocrazy.tools.Coords r24 = new com.ludocrazy.tools.Coords     // Catch:{ Exception -> 0x0039 }
            r24.<init>()     // Catch:{ Exception -> 0x0039 }
            com.ludocrazy.tools.Coords r25 = new com.ludocrazy.tools.Coords     // Catch:{ Exception -> 0x0039 }
            r25.<init>()     // Catch:{ Exception -> 0x0039 }
            r31 = 0
            r5 = 1065353216(0x3f800000, float:1.0)
            float r17 = r31 - r5
            r14 = 0
            r15 = 0
        L_0x01ec:
            java.lang.String r20 = r12.readLine()     // Catch:{ Exception -> 0x0039 }
            if (r20 == 0) goto L_0x0060
            r0 = r14
            r1 = r28
            if (r0 <= r1) goto L_0x0223
            java.lang.Exception r5 = new java.lang.Exception     // Catch:{ Exception -> 0x0039 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0039 }
            java.lang.String r7 = "File does not end when expected after reading number of parts: "
            r6.<init>(r7)     // Catch:{ Exception -> 0x0039 }
            r0 = r6
            r1 = r28
            java.lang.StringBuilder r6 = r0.append(r1)     // Catch:{ Exception -> 0x0039 }
            java.lang.String r7 = ". last line read is: "
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x0039 }
            r0 = r6
            r1 = r20
            java.lang.StringBuilder r6 = r0.append(r1)     // Catch:{ Exception -> 0x0039 }
            java.lang.String r6 = r6.toString()     // Catch:{ Exception -> 0x0039 }
            r5.<init>(r6)     // Catch:{ Exception -> 0x0039 }
            throw r5     // Catch:{ Exception -> 0x0039 }
        L_0x021c:
            r5 = r22[r18]     // Catch:{ Exception -> 0x0039 }
            int r28 = r28 + r5
            int r18 = r18 + 1
            goto L_0x01c2
        L_0x0223:
            java.lang.String r5 = "# part"
            r0 = r20
            r1 = r5
            boolean r5 = r0.startsWith(r1)     // Catch:{ Exception -> 0x0039 }
            if (r5 == 0) goto L_0x0258
            int r15 = r14 - r15
            r5 = r22[r11]     // Catch:{ Exception -> 0x0039 }
            if (r15 >= r5) goto L_0x0255
            java.lang.Exception r5 = new java.lang.Exception     // Catch:{ Exception -> 0x0039 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0039 }
            java.lang.String r7 = "Current part has only "
            r6.<init>(r7)     // Catch:{ Exception -> 0x0039 }
            java.lang.StringBuilder r6 = r6.append(r15)     // Catch:{ Exception -> 0x0039 }
            java.lang.String r7 = " parts, but not expectedd "
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x0039 }
            r7 = r22[r11]     // Catch:{ Exception -> 0x0039 }
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x0039 }
            java.lang.String r6 = r6.toString()     // Catch:{ Exception -> 0x0039 }
            r5.<init>(r6)     // Catch:{ Exception -> 0x0039 }
            throw r5     // Catch:{ Exception -> 0x0039 }
        L_0x0255:
            int r11 = r11 + 1
            goto L_0x01ec
        L_0x0258:
            java.lang.String r5 = "triangle"
            r0 = r20
            r1 = r5
            boolean r5 = r0.startsWith(r1)     // Catch:{ Exception -> 0x0039 }
            if (r5 == 0) goto L_0x02d7
            java.lang.String r5 = r12.readLine()     // Catch:{ Exception -> 0x0039 }
            float r5 = java.lang.Float.parseFloat(r5)     // Catch:{ Exception -> 0x0039 }
            float r5 = r5 * r27
            r7.x = r5     // Catch:{ Exception -> 0x0039 }
            java.lang.String r5 = r12.readLine()     // Catch:{ Exception -> 0x0039 }
            float r5 = java.lang.Float.parseFloat(r5)     // Catch:{ Exception -> 0x0039 }
            float r5 = r5 * r27
            r7.y = r5     // Catch:{ Exception -> 0x0039 }
            r0 = r31
            r1 = r7
            r1.z = r0     // Catch:{ Exception -> 0x0039 }
            java.lang.String r5 = r12.readLine()     // Catch:{ Exception -> 0x0039 }
            float r5 = java.lang.Float.parseFloat(r5)     // Catch:{ Exception -> 0x0039 }
            float r5 = r5 * r27
            r0 = r5
            r1 = r24
            r1.x = r0     // Catch:{ Exception -> 0x0039 }
            java.lang.String r5 = r12.readLine()     // Catch:{ Exception -> 0x0039 }
            float r5 = java.lang.Float.parseFloat(r5)     // Catch:{ Exception -> 0x0039 }
            float r5 = r5 * r27
            r0 = r5
            r1 = r24
            r1.y = r0     // Catch:{ Exception -> 0x0039 }
            r0 = r31
            r1 = r24
            r1.z = r0     // Catch:{ Exception -> 0x0039 }
            java.lang.String r5 = r12.readLine()     // Catch:{ Exception -> 0x0039 }
            float r5 = java.lang.Float.parseFloat(r5)     // Catch:{ Exception -> 0x0039 }
            float r5 = r5 * r27
            r0 = r5
            r1 = r25
            r1.x = r0     // Catch:{ Exception -> 0x0039 }
            java.lang.String r5 = r12.readLine()     // Catch:{ Exception -> 0x0039 }
            float r5 = java.lang.Float.parseFloat(r5)     // Catch:{ Exception -> 0x0039 }
            float r5 = r5 * r27
            r0 = r5
            r1 = r25
            r1.y = r0     // Catch:{ Exception -> 0x0039 }
            r0 = r31
            r1 = r25
            r1.z = r0     // Catch:{ Exception -> 0x0039 }
            r0 = r32
            r1 = r7
            r2 = r24
            r3 = r25
            r4 = r11
            r0.addTriangle(r1, r2, r3, r4)     // Catch:{ Exception -> 0x0039 }
            int r14 = r14 + 1
            goto L_0x01ec
        L_0x02d7:
            java.lang.String r5 = "circle"
            r0 = r20
            r1 = r5
            boolean r5 = r0.startsWith(r1)     // Catch:{ Exception -> 0x0039 }
            if (r5 == 0) goto L_0x032b
            java.lang.String r5 = r12.readLine()     // Catch:{ Exception -> 0x0039 }
            r6 = 10
            int r6 = java.lang.Integer.parseInt(r5, r6)     // Catch:{ Exception -> 0x0039 }
            java.lang.String r5 = r12.readLine()     // Catch:{ Exception -> 0x0039 }
            float r26 = java.lang.Float.parseFloat(r5)     // Catch:{ Exception -> 0x0039 }
            java.lang.String r5 = r12.readLine()     // Catch:{ Exception -> 0x0039 }
            float r5 = java.lang.Float.parseFloat(r5)     // Catch:{ Exception -> 0x0039 }
            float r5 = r5 * r27
            r7.x = r5     // Catch:{ Exception -> 0x0039 }
            java.lang.String r5 = r12.readLine()     // Catch:{ Exception -> 0x0039 }
            float r5 = java.lang.Float.parseFloat(r5)     // Catch:{ Exception -> 0x0039 }
            float r5 = r5 * r27
            r7.y = r5     // Catch:{ Exception -> 0x0039 }
            r0 = r17
            r1 = r7
            r1.z = r0     // Catch:{ Exception -> 0x0039 }
            java.lang.String r5 = r12.readLine()     // Catch:{ Exception -> 0x0039 }
            float r9 = java.lang.Float.parseFloat(r5)     // Catch:{ Exception -> 0x0039 }
            java.lang.String r5 = r12.readLine()     // Catch:{ Exception -> 0x0039 }
            float r10 = java.lang.Float.parseFloat(r5)     // Catch:{ Exception -> 0x0039 }
            float r8 = r27 * r26
            r5 = r32
            r5.addCircle(r6, r7, r8, r9, r10, r11)     // Catch:{ Exception -> 0x0039 }
            int r14 = r14 + r6
            goto L_0x01ec
        L_0x032b:
            java.lang.String r5 = "line"
            r0 = r20
            r1 = r5
            boolean r5 = r0.startsWith(r1)     // Catch:{ Exception -> 0x0039 }
            if (r5 == 0) goto L_0x0381
            java.lang.String r5 = r12.readLine()     // Catch:{ Exception -> 0x0039 }
            float r5 = java.lang.Float.parseFloat(r5)     // Catch:{ Exception -> 0x0039 }
            float r5 = r5 * r27
            r7.x = r5     // Catch:{ Exception -> 0x0039 }
            java.lang.String r5 = r12.readLine()     // Catch:{ Exception -> 0x0039 }
            float r5 = java.lang.Float.parseFloat(r5)     // Catch:{ Exception -> 0x0039 }
            float r5 = r5 * r27
            r7.y = r5     // Catch:{ Exception -> 0x0039 }
            r0 = r31
            r1 = r7
            r1.z = r0     // Catch:{ Exception -> 0x0039 }
            java.lang.String r5 = r12.readLine()     // Catch:{ Exception -> 0x0039 }
            float r5 = java.lang.Float.parseFloat(r5)     // Catch:{ Exception -> 0x0039 }
            float r5 = r5 * r27
            r0 = r5
            r1 = r24
            r1.x = r0     // Catch:{ Exception -> 0x0039 }
            java.lang.String r5 = r12.readLine()     // Catch:{ Exception -> 0x0039 }
            float r5 = java.lang.Float.parseFloat(r5)     // Catch:{ Exception -> 0x0039 }
            float r5 = r5 * r27
            r0 = r5
            r1 = r24
            r1.y = r0     // Catch:{ Exception -> 0x0039 }
            r0 = r31
            r1 = r24
            r1.z = r0     // Catch:{ Exception -> 0x0039 }
            r0 = r32
            r1 = r7
            r2 = r24
            r0.addLine(r1, r2)     // Catch:{ Exception -> 0x0039 }
            goto L_0x01ec
        L_0x0381:
            java.lang.Exception r5 = new java.lang.Exception     // Catch:{ Exception -> 0x0039 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0039 }
            java.lang.String r8 = "Unknown line in file: "
            r6.<init>(r8)     // Catch:{ Exception -> 0x0039 }
            r0 = r6
            r1 = r20
            java.lang.StringBuilder r6 = r0.append(r1)     // Catch:{ Exception -> 0x0039 }
            java.lang.String r6 = r6.toString()     // Catch:{ Exception -> 0x0039 }
            r5.<init>(r6)     // Catch:{ Exception -> 0x0039 }
            goto L_0x01ec
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ludocrazy.tools.ShapeFileReader.readShapeFile(android.content.Context, java.lang.String, float):void");
    }

    /* access modifiers changed from: protected */
    public void prepareMeshes(int total_triangles, int[] part_triangle_counts, int count_of_parts, int outline_count) throws Exception {
    }

    /* access modifiers changed from: protected */
    public void addTriangle(Coords pos1, Coords pos2, Coords pos3, int current_part) throws Exception {
    }

    /* access modifiers changed from: protected */
    public void addCircle(int circle_count_of_triangles, Coords pos1, float radius, float starting_angle_degree, float ending_angle_degree, int current_part) throws Exception {
    }

    /* access modifiers changed from: protected */
    public void addLine(Coords pos1, Coords pos2) throws Exception {
    }
}
