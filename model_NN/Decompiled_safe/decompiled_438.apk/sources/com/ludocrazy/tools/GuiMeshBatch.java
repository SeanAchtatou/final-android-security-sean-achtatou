package com.ludocrazy.tools;

import javax.microedition.khronos.opengles.GL10;

public class GuiMeshBatch {
    private LogOutput DebugLogOutput = null;
    private GuiMesh m_BatchMesh = null;
    private GuiMesh m_BatchMesh_LastBuffer = null;
    private PhoneAbilities m_PhoneAbilities = null;

    public GuiMeshBatch(LogOutput DEBUGLOG_to_use, PhoneAbilities phoneAbilities) {
        this.DebugLogOutput = DEBUGLOG_to_use;
        this.m_PhoneAbilities = phoneAbilities;
    }

    public void addGuiMesh(GuiMesh newToAddMesh) throws Exception {
        if (this.m_BatchMesh != null) {
            this.m_BatchMesh_LastBuffer = this.m_BatchMesh;
            this.m_BatchMesh = new GuiMesh(this.DebugLogOutput, this.m_PhoneAbilities);
            this.m_BatchMesh.setupArraysQuadFaces(this.m_BatchMesh_LastBuffer.getQuadCount() + newToAddMesh.getQuadCount());
            this.m_BatchMesh.addBasicMesh(this.m_BatchMesh_LastBuffer, null);
            this.m_BatchMesh.addBasicMesh(newToAddMesh, null);
        } else {
            this.m_BatchMesh = new GuiMesh(this.DebugLogOutput, this.m_PhoneAbilities);
            this.m_BatchMesh.setupArraysQuadFaces(newToAddMesh.getQuadCount());
            this.m_BatchMesh.addBasicMesh(newToAddMesh, null);
        }
        this.m_BatchMesh.convertArraysToBuffer();
    }

    public void draw(GL10 gl) throws Exception {
        this.m_BatchMesh.draw(gl);
    }

    public void onResume() {
        this.m_BatchMesh.onResume();
    }
}
