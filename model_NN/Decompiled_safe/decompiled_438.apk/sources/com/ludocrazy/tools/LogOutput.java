package com.ludocrazy.tools;

import android.content.Context;
import android.util.Log;

public class LogOutput {
    static Context m_Context = null;
    static String m_PackageName = null;
    int DEBUGLOG_LEVEL = 1;

    public LogOutput(Context context, String packageName) {
        m_Context = context;
        m_PackageName = packageName;
    }

    public void setCurrentLogLevel(int logLevel_to_use) {
        this.DEBUGLOG_LEVEL = logLevel_to_use;
    }

    public boolean isCurrentLogLevelAtleast(int logllevel_to_use) {
        return this.DEBUGLOG_LEVEL >= logllevel_to_use;
    }

    public void log(int log_level_priority, String message) {
        if (log_level_priority <= this.DEBUGLOG_LEVEL) {
            Log.d(m_PackageName, "[" + log_level_priority + "] " + message);
        }
    }
}
