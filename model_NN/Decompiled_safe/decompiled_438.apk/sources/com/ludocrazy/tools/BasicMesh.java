package com.ludocrazy.tools;

import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.opengles.GL11;

public class BasicMesh {
    protected static final int VERTICES_PER_QUAD = 4;
    protected LogOutput DebugLogOutput = null;
    protected boolean USEVBOS = true;
    protected byte[] colors = null;
    protected int colors_bytes = 0;
    protected short[] indices = null;
    protected BoundingBox m_BoundingBox = new BoundingBox();
    protected boolean m_BoundingBoxUpToDate = false;
    protected boolean m_ColorBufferUsed = false;
    private int m_IBOhandle = 0;
    protected ShortBuffer m_IndexBuffer = null;
    private FloatBuffer m_InterleavedBuffer = null;
    protected PhoneAbilities m_PhoneAbilities = null;
    protected int m_RenderPrimitiveType = 4;
    protected boolean m_UvBufferUsed = false;
    private int m_VBOhandle = 0;
    private boolean m_VBOprepared = false;
    protected int m_VerticesPerFace = -1;
    protected ByteBuffer m_colorBuffer = null;
    protected int m_currentIndex = 0;
    protected int m_currentVertex = 0;
    protected int m_indices_count = 0;
    protected FloatBuffer m_uvBuffer = null;
    protected FloatBuffer m_vertexBuffer = null;
    protected int m_vertices_count = 0;
    protected float[] uvCoords = null;
    protected int uvCoords_floats = 0;
    protected int vertex_floats = 0;
    protected float[] vertices = null;

    private static native synchronized void copyFloatsIntoBufferFastViaJNI(float[] fArr, Buffer buffer, int i, int i2);

    public BasicMesh(LogOutput DEBUGLOG_to_use, PhoneAbilities phoneAbilities) {
        this.DebugLogOutput = DEBUGLOG_to_use;
        this.m_PhoneAbilities = phoneAbilities;
    }

    public void onResume() {
        this.m_VBOprepared = false;
    }

    public void setupArraysQuadFaces(int quadCount) {
        setupArrays(quadCount, 6, 4);
        this.m_RenderPrimitiveType = 4;
    }

    public void setupArraysTriangleFaces(int triangleCount) {
        setupArrays(triangleCount, 3, 3);
        this.m_RenderPrimitiveType = 4;
    }

    public float[] getVerticesArray() {
        return this.vertices;
    }

    public float[] getUvCoordsArray() {
        return this.uvCoords;
    }

    public byte[] getColorsArray() {
        return this.colors;
    }

    public short[] getIndicesArray() {
        return this.indices;
    }

    public void resetMesh_ForNextDynamicUpdate() {
        reset(true);
    }

    public void convertMesh_AfterDynamicUpdate() {
        convertArraysToBuffer();
    }

    public void reset(boolean dynamicUse) {
        this.m_currentVertex = 0;
        this.m_currentIndex = 0;
        this.m_VBOprepared = false;
        if (dynamicUse) {
            this.USEVBOS = false;
        }
    }

    /* access modifiers changed from: protected */
    public void setupArrays(int faceCount, int indices_per_face, int vertices_per_face) {
        reset(false);
        this.m_VerticesPerFace = vertices_per_face;
        this.m_indices_count = indices_per_face * faceCount;
        this.m_vertices_count = vertices_per_face * faceCount;
        this.vertex_floats = this.m_vertices_count * 3;
        this.uvCoords_floats = this.m_vertices_count * 2;
        this.colors_bytes = this.m_vertices_count * 4;
        this.vertices = new float[this.vertex_floats];
        this.uvCoords = new float[this.uvCoords_floats];
        this.colors = new byte[this.colors_bytes];
        this.indices = new short[this.m_indices_count];
        ByteBuffer byteBuf = ByteBuffer.allocateDirect(this.vertex_floats * 4);
        byteBuf.order(ByteOrder.nativeOrder());
        this.m_vertexBuffer = byteBuf.asFloatBuffer();
        ByteBuffer byteBuf2 = ByteBuffer.allocateDirect(this.uvCoords_floats * 4);
        byteBuf2.order(ByteOrder.nativeOrder());
        this.m_uvBuffer = byteBuf2.asFloatBuffer();
        ByteBuffer byteBuf3 = ByteBuffer.allocateDirect(this.m_indices_count * 2);
        byteBuf3.order(ByteOrder.nativeOrder());
        this.m_IndexBuffer = byteBuf3.asShortBuffer();
        this.m_colorBuffer = ByteBuffer.allocateDirect(this.colors_bytes * 1);
        this.m_colorBuffer.order(ByteOrder.nativeOrder());
    }

    public void convertArraysToBuffer() {
        copyFloatsIntoFloatBufferFast(this.vertices, this.m_vertexBuffer);
        if (this.m_UvBufferUsed) {
            copyFloatsIntoFloatBufferFast(this.uvCoords, this.m_uvBuffer);
        }
        this.m_IndexBuffer.put(this.indices);
        this.m_IndexBuffer.position(0);
        if (this.m_ColorBufferUsed) {
            this.m_colorBuffer.put(this.colors);
            this.m_colorBuffer.position(0);
        }
        this.m_BoundingBoxUpToDate = false;
    }

    /* access modifiers changed from: package-private */
    public void setupVBO(GL10 gl) {
        try {
            this.USEVBOS = this.USEVBOS && this.m_PhoneAbilities.isSupported(3);
            if (!this.USEVBOS) {
                this.DebugLogOutput.log(2, "BasicMesh::setupVBO() called, but not using VBOs.");
            } else if (this.m_vertices_count >= 3) {
                StopWatch stopwatch = new StopWatch();
                GL11 gl11 = (GL11) gl;
                if (this.m_VBOhandle == 0) {
                    int[] handle = new int[1];
                    gl11.glGenBuffers(1, handle, 0);
                    this.m_VBOhandle = handle[0];
                    gl11.glGenBuffers(1, handle, 0);
                    this.m_IBOhandle = handle[0];
                }
                int needed_bytes_vbo = this.vertex_floats * 4;
                if (this.m_UvBufferUsed) {
                    needed_bytes_vbo += this.uvCoords_floats * 4;
                }
                if (this.m_ColorBufferUsed) {
                    needed_bytes_vbo += this.colors_bytes * 1;
                }
                ByteBuffer byteBuf = ByteBuffer.allocateDirect(needed_bytes_vbo);
                byteBuf.order(ByteOrder.nativeOrder());
                this.m_InterleavedBuffer = byteBuf.asFloatBuffer();
                FloatBuffer color_floats_packed = this.m_colorBuffer.asFloatBuffer();
                for (int i = 0; i < this.m_vertices_count; i++) {
                    this.m_InterleavedBuffer.put(this.vertices, i * 3, 3);
                    if (this.m_UvBufferUsed) {
                        this.m_InterleavedBuffer.put(this.uvCoords, i * 2, 2);
                    }
                    if (this.m_ColorBufferUsed) {
                        this.m_InterleavedBuffer.put(color_floats_packed.get(i));
                    }
                }
                this.m_InterleavedBuffer.position(0);
                gl11.glBindBuffer(34962, this.m_VBOhandle);
                gl11.glBufferData(34962, needed_bytes_vbo, this.m_InterleavedBuffer, 35044);
                gl11.glBindBuffer(34963, this.m_IBOhandle);
                gl11.glBufferData(34963, this.m_IndexBuffer.capacity() * 2, this.m_IndexBuffer, 35044);
                this.m_VBOprepared = true;
                long dur = stopwatch.getElapsedMilliSeconds();
                if (dur > 500) {
                    this.DebugLogOutput.log(1, "performance: GuiMesh::setupVBO() prepared VBO, filling interleaved took:" + dur);
                }
            } else {
                throw new Exception("Mesh has (currently) less than 3 vertices! (OPENGL_ERROR OUTOFMEMORY will occur with 0 vertices)");
            }
        } catch (Exception e) {
            new ErrorOutput("BasicMesh::setupVBO()", "Exception", e);
        }
    }

    /* access modifiers changed from: package-private */
    public void drawVBO(GL10 gl, int appearCount) {
        if (this.USEVBOS) {
            GL11 gl11 = (GL11) gl;
            gl11.glBindBuffer(34962, this.m_VBOhandle);
            gl11.glBindBuffer(34963, this.m_IBOhandle);
            int stride_bytes = 12;
            if (this.m_UvBufferUsed) {
                stride_bytes = 12 + 8;
            }
            if (this.m_ColorBufferUsed) {
                stride_bytes += 4;
            }
            gl11.glVertexPointer(3, 5126, stride_bytes, 0);
            int further_offset = 3;
            if (this.m_UvBufferUsed) {
                gl11.glTexCoordPointer(2, 5126, stride_bytes, 3 * 4);
                further_offset = 3 + 2;
            }
            if (this.m_ColorBufferUsed) {
                gl11.glColorPointer(4, 5121, stride_bytes, further_offset * 4);
            }
            gl11.glDrawElements(this.m_RenderPrimitiveType, appearCount, 5123, 0);
            gl11.glBindBuffer(34963, 0);
            gl11.glBindBuffer(34962, 0);
        }
    }

    public int draw(GL10 gl) throws Exception {
        return draw(gl, this.m_currentIndex);
    }

    public void setupMesh(GL10 gl) {
        if (!this.m_VBOprepared && this.USEVBOS) {
            setupVBO(gl);
        }
    }

    public int draw(GL10 gl, int appearCount) throws Exception {
        if (this.m_vertexBuffer != null) {
            if (appearCount > this.m_currentIndex) {
                appearCount = this.m_currentIndex;
            } else if (appearCount < 0) {
                appearCount = 0;
            }
            setupMesh(gl);
            if (!this.m_VBOprepared || !this.USEVBOS) {
                gl.glVertexPointer(3, 5126, 0, this.m_vertexBuffer);
                if (this.m_UvBufferUsed) {
                    gl.glTexCoordPointer(2, 5126, 0, this.m_uvBuffer);
                }
                if (this.m_ColorBufferUsed) {
                    gl.glColorPointer(4, 5121, 0, this.m_colorBuffer);
                }
                gl.glDrawElements(this.m_RenderPrimitiveType, appearCount, 5123, this.m_IndexBuffer);
            } else {
                drawVBO(gl, appearCount);
            }
            return appearCount / 3;
        }
        throw new Exception("BasicMesh.draw(): VertexBuffer is null. Seems like convertArraysToBuffer() has not been called! m_indices_count:" + this.m_indices_count);
    }

    public void addSmallDot(Coords center_pos, float radius) throws Exception {
        ColorFloats black = new ColorFloats(0.0f, 0.0f, 0.0f, 1.0f);
        addTriangledCircle_colored(8, center_pos, radius, 0.0f, 360.0f, black, black);
    }

    public void addTriangledCircle_colored(int count_of_triangles, Coords center_pos, float radius, float starting_angle_degree, float ending_angle_degree, ColorFloats center_col, ColorFloats outer_col) throws Exception {
        Coords segment_point_a = new Coords();
        Coords segment_point_b = new Coords();
        float angle_stepping = Tools.degToRad(Math.abs(ending_angle_degree - starting_angle_degree) / ((float) count_of_triangles));
        float current_angle = Tools.degToRad(starting_angle_degree);
        segment_point_b.x = (((float) Math.sin((double) current_angle)) * radius) + center_pos.x;
        segment_point_b.y = (((float) Math.cos((double) current_angle)) * radius) + center_pos.y;
        segment_point_b.z = center_pos.z;
        for (int i = 0; i < count_of_triangles; i++) {
            segment_point_a.set(segment_point_b);
            current_angle += angle_stepping;
            segment_point_b.x = (((float) Math.sin((double) current_angle)) * radius) + center_pos.x;
            segment_point_b.y = (((float) Math.cos((double) current_angle)) * radius) + center_pos.y;
            addTriangleFace_colored(center_pos, center_col, segment_point_a, outer_col, segment_point_b, outer_col);
        }
    }

    public void addTriangledRing_colored(int count_of_quads, Coords inner_ring_center_pos, Coords outer_ring_center_pos, float inner_radius, float outer_radius, float starting_angle_degree, float ending_angle_degree, ColorFloats inner_col, ColorFloats outer_col) throws Exception {
        Coords segment_point_inner_a = new Coords();
        Coords segment_point_outer_a = new Coords();
        Coords segment_point_inner_b = new Coords();
        Coords segment_point_outer_b = new Coords();
        float angle_stepping = Tools.degToRad(Math.abs(ending_angle_degree - starting_angle_degree) / ((float) count_of_quads));
        float current_angle = Tools.degToRad(starting_angle_degree);
        segment_point_inner_b.x = (((float) Math.sin((double) current_angle)) * inner_radius) + inner_ring_center_pos.x;
        segment_point_inner_b.y = (((float) Math.cos((double) current_angle)) * inner_radius) + inner_ring_center_pos.y;
        segment_point_inner_b.z = inner_ring_center_pos.z;
        segment_point_outer_b.x = (((float) Math.sin((double) current_angle)) * outer_radius) + outer_ring_center_pos.x;
        segment_point_outer_b.y = (((float) Math.cos((double) current_angle)) * outer_radius) + outer_ring_center_pos.y;
        segment_point_outer_b.z = outer_ring_center_pos.z;
        for (int i = 0; i < count_of_quads; i++) {
            segment_point_inner_a.set(segment_point_inner_b);
            segment_point_outer_a.set(segment_point_outer_b);
            current_angle += angle_stepping;
            segment_point_inner_b.x = (((float) Math.sin((double) current_angle)) * inner_radius) + inner_ring_center_pos.x;
            segment_point_inner_b.y = (((float) Math.cos((double) current_angle)) * inner_radius) + inner_ring_center_pos.y;
            segment_point_outer_b.x = (((float) Math.sin((double) current_angle)) * outer_radius) + outer_ring_center_pos.x;
            segment_point_outer_b.y = (((float) Math.cos((double) current_angle)) * outer_radius) + outer_ring_center_pos.y;
            addTriangleFace_colored(segment_point_outer_a, outer_col, segment_point_outer_b, outer_col, segment_point_inner_a, inner_col);
            addTriangleFace_colored(segment_point_outer_b, outer_col, segment_point_inner_b, inner_col, segment_point_inner_a, inner_col);
        }
    }

    public void addTriangleFace_colored(Coords pos1, ColorFloats col1, Coords pos2, ColorFloats col2, Coords pos3, ColorFloats col3) throws Exception {
        if (this.m_currentVertex <= this.m_vertices_count - 3) {
            int firstVertex = this.m_currentVertex;
            this.vertices[(this.m_currentVertex * 3) + 0] = pos1.x;
            this.vertices[(this.m_currentVertex * 3) + 1] = pos1.y;
            this.vertices[(this.m_currentVertex * 3) + 2] = pos1.z;
            this.m_currentVertex++;
            this.vertices[(this.m_currentVertex * 3) + 0] = pos2.x;
            this.vertices[(this.m_currentVertex * 3) + 1] = pos2.y;
            this.vertices[(this.m_currentVertex * 3) + 2] = pos2.z;
            this.m_currentVertex++;
            this.vertices[(this.m_currentVertex * 3) + 0] = pos3.x;
            this.vertices[(this.m_currentVertex * 3) + 1] = pos3.y;
            this.vertices[(this.m_currentVertex * 3) + 2] = pos3.z;
            this.m_currentVertex++;
            this.m_ColorBufferUsed = true;
            this.m_currentVertex -= 3;
            this.colors[(this.m_currentVertex * 4) + 0] = (byte) ((int) (col1.red * 255.0f));
            this.colors[(this.m_currentVertex * 4) + 1] = (byte) ((int) (col1.green * 255.0f));
            this.colors[(this.m_currentVertex * 4) + 2] = (byte) ((int) (col1.blue * 255.0f));
            this.colors[(this.m_currentVertex * 4) + 3] = (byte) ((int) (col1.alpha * 255.0f));
            this.m_currentVertex++;
            this.colors[(this.m_currentVertex * 4) + 0] = (byte) ((int) (col2.red * 255.0f));
            this.colors[(this.m_currentVertex * 4) + 1] = (byte) ((int) (col2.green * 255.0f));
            this.colors[(this.m_currentVertex * 4) + 2] = (byte) ((int) (col2.blue * 255.0f));
            this.colors[(this.m_currentVertex * 4) + 3] = (byte) ((int) (col2.alpha * 255.0f));
            this.m_currentVertex++;
            this.colors[(this.m_currentVertex * 4) + 0] = (byte) ((int) (col3.red * 255.0f));
            this.colors[(this.m_currentVertex * 4) + 1] = (byte) ((int) (col3.green * 255.0f));
            this.colors[(this.m_currentVertex * 4) + 2] = (byte) ((int) (col3.blue * 255.0f));
            this.colors[(this.m_currentVertex * 4) + 3] = (byte) ((int) (col3.alpha * 255.0f));
            this.m_currentVertex++;
            this.indices[this.m_currentIndex] = (short) (firstVertex + 0);
            this.m_currentIndex++;
            this.indices[this.m_currentIndex] = (short) (firstVertex + 1);
            this.m_currentIndex++;
            this.indices[this.m_currentIndex] = (short) (firstVertex + 2);
            this.m_currentIndex++;
            return;
        }
        throw new Exception("GuiMesh: Can't call addTriangleFace_colored(), not enough Triangles reserved by SetupArray(): " + (this.m_vertices_count / 3));
    }

    public void addHeightfieldQuad(float x_start, float x_end, float y_high_start, float y_high_end, float y_ground_start, float y_ground_end, float z_depth) throws Exception {
        if (this.m_currentVertex <= this.m_vertices_count - 4) {
            int firstVertex = this.m_currentVertex;
            this.vertices[(this.m_currentVertex * 3) + 0] = x_start;
            this.vertices[(this.m_currentVertex * 3) + 1] = y_ground_start;
            this.vertices[(this.m_currentVertex * 3) + 2] = z_depth;
            this.m_currentVertex++;
            this.vertices[(this.m_currentVertex * 3) + 0] = x_start;
            this.vertices[(this.m_currentVertex * 3) + 1] = y_high_start;
            this.vertices[(this.m_currentVertex * 3) + 2] = z_depth;
            this.m_currentVertex++;
            this.vertices[(this.m_currentVertex * 3) + 0] = x_end;
            this.vertices[(this.m_currentVertex * 3) + 1] = y_ground_end;
            this.vertices[(this.m_currentVertex * 3) + 2] = z_depth;
            this.m_currentVertex++;
            this.vertices[(this.m_currentVertex * 3) + 0] = x_end;
            this.vertices[(this.m_currentVertex * 3) + 1] = y_high_end;
            this.vertices[(this.m_currentVertex * 3) + 2] = z_depth;
            this.m_currentVertex++;
            this.indices[this.m_currentIndex] = (short) (firstVertex + 0);
            this.m_currentIndex++;
            this.indices[this.m_currentIndex] = (short) (firstVertex + 1);
            this.m_currentIndex++;
            this.indices[this.m_currentIndex] = (short) (firstVertex + 2);
            this.m_currentIndex++;
            this.indices[this.m_currentIndex] = (short) (firstVertex + 2);
            this.m_currentIndex++;
            this.indices[this.m_currentIndex] = (short) (firstVertex + 1);
            this.m_currentIndex++;
            this.indices[this.m_currentIndex] = (short) (firstVertex + 3);
            this.m_currentIndex++;
            return;
        }
        throw new Exception("GuiMesh: Can't call addHeightfieldQuad(), not enough Quads reserved by SetupArray(): " + (this.m_vertices_count / 4));
    }

    public void addColorForLastQuad(float r, float g, float b, float a) {
        addColorsForLastQuad(r, g, b, a, r, g, b, a, r, g, b, a, r, g, b, a);
    }

    public void addColorsForLastQuad(ColorFloats vertex1, ColorFloats vertex2, ColorFloats vertex3, ColorFloats vertex4) {
        addColorsForLastQuad(vertex1.red, vertex1.green, vertex1.blue, vertex1.alpha, vertex2.red, vertex2.green, vertex2.blue, vertex2.alpha, vertex3.red, vertex3.green, vertex3.blue, vertex3.alpha, vertex4.red, vertex4.green, vertex4.blue, vertex4.alpha);
    }

    public void addColorsForLastQuad(float r1, float g1, float b1, float a1, float r2, float g2, float b2, float a2, float r3, float g3, float b3, float a3, float r4, float g4, float b4, float a4) {
        this.m_ColorBufferUsed = true;
        this.m_currentVertex -= 4;
        this.colors[(this.m_currentVertex * 4) + 0] = (byte) ((int) (255.0f * r1));
        this.colors[(this.m_currentVertex * 4) + 1] = (byte) ((int) (255.0f * g1));
        this.colors[(this.m_currentVertex * 4) + 2] = (byte) ((int) (255.0f * b1));
        this.colors[(this.m_currentVertex * 4) + 3] = (byte) ((int) (255.0f * a1));
        this.m_currentVertex++;
        this.colors[(this.m_currentVertex * 4) + 0] = (byte) ((int) (255.0f * r2));
        this.colors[(this.m_currentVertex * 4) + 1] = (byte) ((int) (255.0f * g2));
        this.colors[(this.m_currentVertex * 4) + 2] = (byte) ((int) (255.0f * b2));
        this.colors[(this.m_currentVertex * 4) + 3] = (byte) ((int) (255.0f * a2));
        this.m_currentVertex++;
        this.colors[(this.m_currentVertex * 4) + 0] = (byte) ((int) (255.0f * r4));
        this.colors[(this.m_currentVertex * 4) + 1] = (byte) ((int) (255.0f * g4));
        this.colors[(this.m_currentVertex * 4) + 2] = (byte) ((int) (255.0f * b4));
        this.colors[(this.m_currentVertex * 4) + 3] = (byte) ((int) (255.0f * a4));
        this.m_currentVertex++;
        this.colors[(this.m_currentVertex * 4) + 0] = (byte) ((int) (255.0f * r3));
        this.colors[(this.m_currentVertex * 4) + 1] = (byte) ((int) (255.0f * g3));
        this.colors[(this.m_currentVertex * 4) + 2] = (byte) ((int) (255.0f * b3));
        this.colors[(this.m_currentVertex * 4) + 3] = (byte) ((int) (255.0f * a3));
        this.m_currentVertex++;
    }

    public void addBasicMesh(BasicMesh partner_to_copy_from, Vector3D vertex_translation) throws Exception {
        partner_to_copy_from.copyIntoBasicMesh(this, vertex_translation);
    }

    public void copyIntoBasicMesh(BasicMesh target_to_copy_into, Vector3D vertex_translation) throws Exception {
        if (target_to_copy_into == null) {
            throw new Exception("BasicMesh.copyIntoBasicMesh() given target_to_copy_into is null!");
        }
        float[] copy_uvs = null;
        byte[] copy_colors = null;
        if (this.m_UvBufferUsed) {
            copy_uvs = this.uvCoords;
        }
        if (this.m_ColorBufferUsed) {
            copy_colors = this.colors;
        }
        target_to_copy_into.addArrays(this.vertices, copy_uvs, copy_colors, this.indices, this.m_currentVertex, vertex_translation);
    }

    public void addArrays(float[] newVertices, float[] newUvCoords, byte[] newColors, short[] newIndices, int vertices_count, Vector3D vertex_translation) {
        if (newVertices != null) {
            if (vertex_translation == null) {
                for (int i = 0; i < newVertices.length; i++) {
                    this.vertices[(this.m_currentVertex * 3) + i] = newVertices[i];
                }
            } else {
                for (int i2 = 0; i2 < newVertices.length; i2 += 3) {
                    this.vertices[(this.m_currentVertex * 3) + i2 + 0] = newVertices[i2 + 0] + vertex_translation.x;
                    this.vertices[(this.m_currentVertex * 3) + i2 + 1] = newVertices[i2 + 1] + vertex_translation.y;
                    this.vertices[(this.m_currentVertex * 3) + i2 + 2] = newVertices[i2 + 2] + vertex_translation.z;
                }
            }
        }
        if (newUvCoords != null) {
            this.m_UvBufferUsed = true;
            for (int i3 = 0; i3 < newUvCoords.length; i3++) {
                this.uvCoords[(this.m_currentVertex * 2) + i3] = newUvCoords[i3];
            }
        }
        if (newColors != null) {
            this.m_ColorBufferUsed = true;
            for (int i4 = 0; i4 < newColors.length; i4++) {
                this.colors[(this.m_currentVertex * 4) + i4] = newColors[i4];
            }
        }
        if (newIndices != null) {
            for (int i5 = 0; i5 < newIndices.length; i5++) {
                this.indices[this.m_currentIndex + i5] = (short) (this.m_currentVertex + newIndices[i5]);
            }
            this.m_currentIndex += newIndices.length;
        }
        if (newVertices != null) {
            this.m_currentVertex += newVertices.length / 3;
        }
    }

    public void updateBoundingBox() {
        this.m_BoundingBox.reset();
        for (int i = 0; i < this.m_currentVertex; i++) {
            this.m_BoundingBox.addPoint(this.vertices[(i * 3) + 0], this.vertices[(i * 3) + 1], this.vertices[(i * 3) + 2]);
        }
        this.m_BoundingBoxUpToDate = true;
    }

    public BoundingBox getBoundingBox() {
        if (!this.m_BoundingBoxUpToDate) {
            updateBoundingBox();
        }
        return this.m_BoundingBox;
    }

    public int getFacesCount() {
        return this.m_currentVertex / this.m_VerticesPerFace;
    }

    public int getVerticesCount() {
        return this.m_currentVertex;
    }

    public int getMaxAppearCount() {
        return this.m_currentIndex;
    }

    static {
        try {
            System.loadLibrary("dripFXjni");
        } catch (Exception e) {
            new ErrorOutput("BasicMesh", "Exception loading Library 'dripFXjni':", e);
        }
    }

    public static void copyFloatsIntoFloatBufferFast(float[] sourceArray, Buffer targetBuffer) {
        int count_of_floats_to_be_coped = sourceArray.length;
        copyFloatsIntoBufferFastViaJNI(sourceArray, targetBuffer, count_of_floats_to_be_coped, 0);
        targetBuffer.position(0);
        targetBuffer.limit(count_of_floats_to_be_coped);
    }

    private void copyFloatsIntoFloatBufferSlow(float[] sourceArray, FloatBuffer targetBuffer) {
        targetBuffer.put(sourceArray);
        targetBuffer.position(0);
    }
}
