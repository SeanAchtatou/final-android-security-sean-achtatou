package com.ludocrazy.tools;

import android.os.Environment;
import android.util.Log;
import android.view.MotionEvent;
import java.io.File;
import java.io.FileWriter;

public class InputRecorder {
    private int m_CurrentEvent = 0;
    private FileWriter m_FileWriter = null;
    private float m_LastEventTime = 0.0f;

    public InputRecorder(String filename) {
        try {
            File root = Environment.getExternalStorageDirectory();
            if (root.canWrite()) {
                this.m_FileWriter = new FileWriter(new File(root, String.valueOf(filename) + ".txt"));
                return;
            }
            throw new Exception("Can't write to SD card! Probably none inserted, or if emulator none created, or if attached to usb, its locked for pc-connection.");
        } catch (Exception e) {
            Log.d("com.ludocrazy.misc", "InputRecorder() Exception:" + e.toString());
        }
    }

    public void finalize() {
        try {
            this.m_FileWriter.close();
        } catch (Exception e) {
            Log.d("com.ludocrazy.misc", e.toString());
        }
    }

    public void writeTouchEventToFile(float gametime_seconds, MotionEvent event) {
        float time_since_last_event = gametime_seconds - this.m_LastEventTime;
        this.m_LastEventTime = gametime_seconds;
        int event_action = event.getAction();
        if (event_action == 0 || event_action == 1) {
            try {
                this.m_CurrentEvent++;
                this.m_FileWriter.write("event: " + this.m_CurrentEvent + ", seconds: " + time_since_last_event + ", x_pos: " + event.getX() + ", y_pos: " + event.getY() + ", action: " + event_action + "\n");
                this.m_FileWriter.flush();
            } catch (Exception e) {
                Log.d("com.ludocrazy.misc", "InputRecorder Exception:" + e.toString());
            }
        }
    }
}
