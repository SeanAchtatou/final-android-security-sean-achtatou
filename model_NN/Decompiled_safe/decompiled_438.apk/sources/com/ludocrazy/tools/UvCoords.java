package com.ludocrazy.tools;

public class UvCoords {
    public float u;
    public float v;

    public UvCoords() {
        this.u = 0.0f;
        this.v = 0.0f;
    }

    public UvCoords(float _u, float _v) {
        this.u = _u;
        this.v = _v;
    }
}
