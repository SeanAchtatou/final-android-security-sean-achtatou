package com.ludocrazy.tools;

public class GuiBackgroundPattern {
    int height;
    int left;
    int top;
    int width;

    public GuiBackgroundPattern(int _left, int _top, int _width, int _height) {
        this.left = _left;
        this.top = _top;
        this.width = _width;
        this.height = _height;
    }
}
