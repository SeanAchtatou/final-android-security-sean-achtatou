package com.ludocrazy.tools;

import javax.microedition.khronos.opengles.GL10;

public class GuiGroup extends GuiBase {
    private LogOutput DebugLogOutput = null;
    float m_BackgroundAlpha;
    int m_BackgroundColor = -1;
    int m_BackgroundPattern = -1;
    GuiMesh m_BaseMesh = null;
    boolean m_BaseMeshIsPattern = false;
    boolean m_BaseMeshPattern_DoDraw = true;
    float m_BoxZ;
    boolean m_ChildrenHidden = false;
    boolean m_FrameVisible = true;
    GuiMeshBatch m_GuiMeshBatch = null;
    private PhoneAbilities m_PhoneAbilities = null;
    float m_TextureSquareSizePixels = 512.0f;

    public GuiGroup(LogOutput DEBUGLOG_to_use, PhoneAbilities phoneAbilities, float texture_square_size_pixels, GuiMeshBatch guiMeshBatch, GuiBase parent, float left, float top, float width, float height, float boxZ, float background_alpha, boolean hasBorder) throws Exception {
        this.DebugLogOutput = DEBUGLOG_to_use;
        this.m_PhoneAbilities = phoneAbilities;
        this.m_TextureSquareSizePixels = texture_square_size_pixels;
        this.m_GuiMeshBatch = guiMeshBatch;
        if (parent == null) {
            throw new Exception("GuiGroup::GuiGroup() passed GuiBase-parent-parameter is NULL!");
        }
        this.m_Parent = parent;
        this.m_BasisTextScale = parent.m_BasisTextScale;
        this.m_Left = this.m_Parent.m_Left + left;
        this.m_Top = this.m_Parent.m_Top + top;
        this.m_Right = this.m_Left + width;
        this.m_Bottom = this.m_Top + height;
        this.m_BoxZ = boxZ;
        if (background_alpha < 0.0f) {
            background_alpha = 0.0f;
        } else if (background_alpha > 1.0f) {
            background_alpha = 1.0f;
        }
        this.m_BackgroundAlpha = background_alpha;
        if (hasBorder) {
            createBorderFrame();
        }
    }

    public void onResume() {
        if (this.m_BaseMesh != null) {
            this.m_BaseMesh.onResume();
        }
        super.onResume();
    }

    public float col(int color_shade_up_to_255) {
        return ((float) color_shade_up_to_255) / 255.0f;
    }

    public void setupBackgroundPattern_PrincessNuriko(int pixelWidth, int pixelHeight, int pattern, int color, boolean opaque) throws Exception {
        GuiBackgroundColors currentColors;
        GuiBackgroundPattern currentPattern;
        float alpha = this.m_BackgroundAlpha;
        if (opaque) {
            alpha = 1.0f;
        }
        switch (color % 10) {
            case 1:
                currentColors = new GuiBackgroundColors(0.5f, 0.2f, 0.8f, 0.4f, 0.25f, 0.05f);
                break;
            case PhoneAbilities.ABILITY_GLUTILS_TEXSUBIMAGE2D_NONSQUARE /*2*/:
                currentColors = new GuiBackgroundColors(0.4f, 0.7f, 0.2f, 0.3f, 0.1f, 0.4f);
                break;
            case PhoneAbilities.ABILITY_VBO_GL11 /*3*/:
                currentColors = new GuiBackgroundColors(0.65f, 0.35f, 0.55f, 0.2f, 0.25f, 0.0f);
                break;
            case 4:
                currentColors = new GuiBackgroundColors(0.1f, 0.6f, 0.5f, 0.4f, 0.15f, 0.1f);
                break;
            case PhoneAbilities.ABILITY_FRAMEBUFFER_OES /*5*/:
                currentColors = new GuiBackgroundColors(0.8f, 0.5f, 0.3f, 0.0f, 0.1f, 0.3f);
                break;
            case PhoneAbilities.ABILITY_POINT_SIZE_ARRAY_OES /*6*/:
                currentColors = new GuiBackgroundColors(col(190), col(160), col(25), 0.0f, col(80), col(100));
                break;
            case MediaManager.MAX_MULTI_LAYERS /*7*/:
                currentColors = new GuiBackgroundColors(col(167), col(205), col(11), col(43), col(62), col(78));
                break;
            case 8:
                currentColors = new GuiBackgroundColors(col(190), col(150), col(210), col(100), col(50), col(25));
                break;
            case 9:
                currentColors = new GuiBackgroundColors(col(32), col(190), col(170), col(50), col(30), col(100));
                break;
            default:
                currentColors = new GuiBackgroundColors(0.2f, 0.6f, 0.8f, 0.4f, 0.2f, 0.0f);
                break;
        }
        currentColors.setAlpha(alpha);
        switch (pattern % 6) {
            case 1:
                currentPattern = new GuiBackgroundPattern(104, 217, 90, 90);
                break;
            case PhoneAbilities.ABILITY_GLUTILS_TEXSUBIMAGE2D_NONSQUARE /*2*/:
                currentPattern = new GuiBackgroundPattern(0, 211, 104, 96);
                break;
            case PhoneAbilities.ABILITY_VBO_GL11 /*3*/:
                currentPattern = new GuiBackgroundPattern(194, 199, 128, 108);
                break;
            case 4:
                currentPattern = new GuiBackgroundPattern(419, 229, 90, 78);
                break;
            case PhoneAbilities.ABILITY_FRAMEBUFFER_OES /*5*/:
                currentPattern = new GuiBackgroundPattern(398, 129, 111, 96);
                break;
            default:
                currentPattern = new GuiBackgroundPattern(322, 225, 97, 82);
                break;
        }
        setupBackgroundPattern(pixelWidth, pixelHeight, currentPattern, currentColors);
    }

    public void setupBackgroundPattern_PuzzleMage(int pixelWidth, int pixelHeight, int pattern, int color, boolean opaque) throws Exception {
        GuiBackgroundPattern currentPattern;
        float alpha = this.m_BackgroundAlpha;
        if (opaque) {
            alpha = 1.0f;
        }
        GuiBackgroundColors currentColors = new GuiBackgroundColors(0.85f, 0.85f, 0.85f, 0.15f, 0.15f, 0.15f);
        currentColors.setAlpha(alpha);
        switch (pattern % 6) {
            case 1:
                currentPattern = new GuiBackgroundPattern(123, 366, 120, 100);
                break;
            case PhoneAbilities.ABILITY_GLUTILS_TEXSUBIMAGE2D_NONSQUARE /*2*/:
                currentPattern = new GuiBackgroundPattern(497, 257, 154, 87);
                break;
            case PhoneAbilities.ABILITY_VBO_GL11 /*3*/:
                currentPattern = new GuiBackgroundPattern(1, 258, 124, 106);
                break;
            case 4:
                currentPattern = new GuiBackgroundPattern(127, 241, 162, 123);
                break;
            case PhoneAbilities.ABILITY_FRAMEBUFFER_OES /*5*/:
                currentPattern = new GuiBackgroundPattern(291, 194, 136, 272);
                break;
            default:
                currentPattern = new GuiBackgroundPattern(1, 366, 120, 100);
                break;
        }
        setupBackgroundPattern(pixelWidth, pixelHeight, currentPattern, currentColors);
    }

    public void setupBackgroundPattern(int pixelWidth, int pixelHeight, GuiBackgroundPattern pattern, GuiBackgroundColors colors) throws Exception {
        this.m_BaseMesh = new GuiBackground(this.DebugLogOutput, this.m_PhoneAbilities, this.m_TextureSquareSizePixels, this.m_Left, this.m_Top, this.m_Right - this.m_Left, this.m_Bottom - this.m_Top, this.m_BoxZ - 0.5f, pixelWidth, pixelHeight, pattern, colors);
        this.m_BaseMeshIsPattern = true;
        if (this.m_GuiMeshBatch != null) {
            this.m_GuiMeshBatch.addGuiMesh(this.m_BaseMesh);
            this.m_BaseMesh = null;
        }
    }

    public void setupBackgroundImage(float pixel_u_start, float pixel_v_start, float pixel_u_width, float pixel_v_height, boolean opaque, ColorFloats color_ignoring_alpha) throws Exception {
        if (color_ignoring_alpha == null) {
            color_ignoring_alpha = new ColorFloats(1.0f, 1.0f, 1.0f, 1.0f);
        }
        color_ignoring_alpha.alpha = this.m_BackgroundAlpha;
        if (opaque) {
            color_ignoring_alpha.alpha = 1.0f;
        }
        this.m_BaseMesh = new GuiImage(this.DebugLogOutput, this.m_PhoneAbilities, this.m_TextureSquareSizePixels, this.m_Left, this.m_Top, this.m_Right - this.m_Left, this.m_Bottom - this.m_Top, this.m_BoxZ - 0.5f, pixel_u_start, pixel_v_start, pixel_u_width, pixel_v_height, color_ignoring_alpha);
        if (this.m_GuiMeshBatch != null) {
            this.m_GuiMeshBatch.addGuiMesh(this.m_BaseMesh);
            this.m_BaseMesh = null;
        }
    }

    public void setupBackgroundGradientImage(int pixel_u_start, float pixel_v_start, float pixel_u_width, float pixel_v_height, boolean opaque, ColorFloats color_ignoring_alpha, int repeat_x) throws Exception {
        if (color_ignoring_alpha == null) {
            color_ignoring_alpha = new ColorFloats(1.0f, 1.0f, 1.0f, 1.0f);
        }
        color_ignoring_alpha.alpha = this.m_BackgroundAlpha;
        if (opaque) {
            color_ignoring_alpha.alpha = 1.0f;
        }
        this.m_BaseMesh = new GuiGradientImage(this.DebugLogOutput, this.m_PhoneAbilities, this.m_TextureSquareSizePixels, this.m_Left, this.m_Top, this.m_Right - this.m_Left, this.m_Bottom - this.m_Top, this.m_BoxZ - 0.5f, pixel_u_start, pixel_v_start, pixel_u_width, pixel_v_height, color_ignoring_alpha, repeat_x);
        if (this.m_GuiMeshBatch != null) {
            this.m_GuiMeshBatch.addGuiMesh(this.m_BaseMesh);
            this.m_BaseMesh = null;
        }
    }

    private void createBorderFrame() throws Exception {
        this.m_BaseMesh = new GuiMesh(this.DebugLogOutput, this.m_PhoneAbilities);
        this.m_BaseMesh.setupArraysQuadFaces(9);
        float boxBgZ = this.m_BoxZ - 0.2f;
        float tileSize = 60.0f / this.m_TextureSquareSizePixels;
        float edgeTileSize = tileSize * 0.12f;
        ColorFloats colorFloats = new ColorFloats(0.0f, 0.0f, 0.0f, this.m_BackgroundAlpha);
        this.m_BaseMesh.addQuad(this.m_Left, this.m_Top, boxBgZ, this.m_Right, this.m_Bottom, 0.98535156f, 0.98339844f, 1.0f, 1.0f);
        this.m_BaseMesh.addColorsForLastQuad(colorFloats, colorFloats, colorFloats, colorFloats);
        ColorFloats colorFloats2 = new ColorFloats(1.0f, 1.0f, 1.0f, 1.0f);
        this.m_BaseMesh.addQuad(this.m_Left, this.m_Top, this.m_BoxZ, this.m_Left + 0.114f, this.m_Top + 0.114f, 0.0f, 1.0f - tileSize, edgeTileSize, (1.0f - tileSize) + edgeTileSize);
        this.m_BaseMesh.addColorsForLastQuad(colorFloats2, colorFloats2, colorFloats2, colorFloats2);
        this.m_BaseMesh.addQuad(this.m_Right - 0.114f, this.m_Top, this.m_BoxZ, this.m_Right, this.m_Top + 0.114f, tileSize - edgeTileSize, 1.0f - tileSize, tileSize, (1.0f - tileSize) + edgeTileSize);
        this.m_BaseMesh.addColorsForLastQuad(colorFloats2, colorFloats2, colorFloats2, colorFloats2);
        this.m_BaseMesh.addQuad(this.m_Left, this.m_Bottom - 0.114f, this.m_BoxZ, this.m_Left + 0.114f, this.m_Bottom, 0.0f, 1.0f - edgeTileSize, edgeTileSize, 1.0f);
        this.m_BaseMesh.addColorsForLastQuad(colorFloats2, colorFloats2, colorFloats2, colorFloats2);
        this.m_BaseMesh.addQuad(this.m_Right - 0.114f, this.m_Bottom - 0.114f, this.m_BoxZ, this.m_Right, this.m_Bottom, tileSize - edgeTileSize, 1.0f - edgeTileSize, tileSize, 1.0f);
        this.m_BaseMesh.addColorsForLastQuad(colorFloats2, colorFloats2, colorFloats2, colorFloats2);
        this.m_BaseMesh.addQuad(this.m_Left + 0.114f, this.m_Top, this.m_BoxZ, this.m_Right - 0.114f, this.m_Top + 0.114f, 0.0f + edgeTileSize, 1.0f - tileSize, tileSize - edgeTileSize, (1.0f - tileSize) + edgeTileSize);
        this.m_BaseMesh.addColorsForLastQuad(colorFloats2, colorFloats2, colorFloats2, colorFloats2);
        this.m_BaseMesh.addQuad(this.m_Left, this.m_Top + 0.114f, this.m_BoxZ, this.m_Left + 0.114f, this.m_Bottom - 0.114f, 0.0f, (1.0f - tileSize) + edgeTileSize, edgeTileSize, 1.0f - edgeTileSize);
        this.m_BaseMesh.addColorsForLastQuad(colorFloats2, colorFloats2, colorFloats2, colorFloats2);
        this.m_BaseMesh.addQuad(this.m_Right - 0.114f, this.m_Top + 0.114f, this.m_BoxZ, this.m_Right, this.m_Bottom - 0.114f, tileSize - edgeTileSize, (1.0f - tileSize) + edgeTileSize, tileSize, 1.0f - edgeTileSize);
        this.m_BaseMesh.addColorsForLastQuad(colorFloats2, colorFloats2, colorFloats2, colorFloats2);
        this.m_BaseMesh.addQuad(this.m_Left + 0.114f, this.m_Bottom - 0.114f, this.m_BoxZ, this.m_Right - 0.114f, this.m_Bottom, 0.0f + edgeTileSize, 1.0f - edgeTileSize, tileSize - edgeTileSize, 1.0f);
        this.m_BaseMesh.addColorsForLastQuad(colorFloats2, colorFloats2, colorFloats2, colorFloats2);
        this.m_BaseMesh.convertArraysToBuffer();
        if (this.m_GuiMeshBatch != null) {
            this.m_GuiMeshBatch.addGuiMesh(this.m_BaseMesh);
            this.m_BaseMesh = null;
        }
    }

    public void setDrawBaseMeshIfPattern(boolean drawing_enabled) {
        this.m_BaseMeshPattern_DoDraw = drawing_enabled;
    }

    public boolean draw(GL10 gl) throws Exception {
        this.m_RedrawNeeded = false;
        if (this.m_VisibleOnOrthoCamera) {
            if (this.m_BaseMesh != null) {
                if (!this.m_BaseMeshIsPattern) {
                    this.m_BaseMesh.draw(gl);
                } else if (this.m_BaseMeshPattern_DoDraw) {
                    gl.glDisable(3042);
                    this.m_BaseMesh.draw(gl);
                    gl.glEnable(3042);
                }
            }
            if (!this.m_ChildrenHidden) {
                this.m_RedrawNeeded = super.draw(gl);
            }
        }
        return this.m_RedrawNeeded;
    }

    public void setChildrenHidden(boolean hidden) {
        this.m_ChildrenHidden = hidden;
    }

    public Vector3D getTopLeft() {
        return new Vector3D(this.m_Left, this.m_Top, this.m_BoxZ);
    }

    public Vector3D getBottomRight() {
        return new Vector3D(this.m_Right, this.m_Bottom, this.m_BoxZ);
    }
}
