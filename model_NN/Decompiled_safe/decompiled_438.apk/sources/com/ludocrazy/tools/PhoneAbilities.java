package com.ludocrazy.tools;

import android.os.Build;
import java.util.Vector;
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.opengles.GL11;

public class PhoneAbilities {
    public static final int ABILITY_CONVERT_TEXTURES_16BIT = 0;
    static final int ABILITY_COUNT = 7;
    public static final int ABILITY_FRAMEBUFFER_OES = 5;
    public static final int ABILITY_GLUTILS_TEXSUBIMAGE2D_NONSQUARE = 2;
    public static final int ABILITY_MIPMAP_AUTO_GENERATE = 1;
    public static final int ABILITY_POINT_SIZE_ARRAY_OES = 6;
    public static final int ABILITY_TEXTURE_POINTSPRITES = 4;
    public static final int ABILITY_VBO_GL11 = 3;
    public static final int MINMAX_ALIASED_POINT_SIZE = 0;
    static final int MINMAX_COUNT = 2;
    public static final int MINMAX_TEXTURESIZE = 1;
    private int DEBUGLOGOUTPUTLEVEL = 1;
    private LogOutput DebugLogOutput = null;
    private boolean isGL11_Api = false;
    private boolean isGL11_ApiAndVersion = false;
    private boolean isGL11_Reliable = false;
    private int m_BitDepth = 0;
    private Vector<String> m_BuildLogOutput = new Vector<>();
    private String m_DeviceString = "not yet loaded";
    private int[] m_MinMax_MaxValue = new int[2];
    private int[] m_MinMax_MinValue = new int[2];
    private Vector<String> m_OpenGLBasicsLogOutput = new Vector<>();
    private Vector<String> m_OpenGLExtensionsLogOutput = new Vector<>();
    private Vector<String> m_OpenGLMaxLogOutput = new Vector<>();
    private String m_ProductString = "not yet loaded";
    private boolean[] m_SupportedAbilities = new boolean[7];
    private int m_ViewHeight = 0;
    private int m_ViewWidth = 0;

    public int getViewSmallerSize() {
        int size = this.m_ViewHeight;
        if (this.m_ViewWidth < size) {
            return this.m_ViewWidth;
        }
        return size;
    }

    public boolean isSupported(int ABILITY_id) throws Exception {
        if (ABILITY_id >= 0 && ABILITY_id < 7) {
            return this.m_SupportedAbilities[ABILITY_id];
        }
        throw new Exception("PhoneAbilities.isSupported() unknown id given: " + ABILITY_id);
    }

    public int checkMinMaxValue(int value_to_check, int MINMAX_id) throws Exception {
        if (MINMAX_id < 0 || MINMAX_id >= 2) {
            throw new Exception("PhoneAbilities.checkMinMaxValue() unknown id given: " + MINMAX_id);
        } else if (value_to_check > this.m_MinMax_MaxValue[MINMAX_id]) {
            return this.m_MinMax_MaxValue[MINMAX_id];
        } else {
            if (value_to_check < this.m_MinMax_MinValue[MINMAX_id]) {
                return this.m_MinMax_MinValue[MINMAX_id];
            }
            return value_to_check;
        }
    }

    public String getAbilityStrings(int page) {
        Vector<String> logoutput;
        String retval = new String("");
        switch (page) {
            case 1:
                logoutput = this.m_BuildLogOutput;
                break;
            case 2:
                logoutput = this.m_OpenGLBasicsLogOutput;
                break;
            case ABILITY_VBO_GL11 /*3*/:
                logoutput = this.m_OpenGLMaxLogOutput;
                break;
            default:
                logoutput = this.m_OpenGLExtensionsLogOutput;
                break;
        }
        for (int i = 0; i < logoutput.size(); i++) {
            if (logoutput == this.m_OpenGLExtensionsLogOutput) {
                retval = String.valueOf(retval) + logoutput.elementAt(i).replaceAll("GL_", "").replaceAll("OES_", "").replaceAll("_", " ").toLowerCase() + ", ";
            } else {
                retval = String.valueOf(retval) + logoutput.elementAt(i) + "\n";
            }
        }
        return retval;
    }

    public PhoneAbilities(LogOutput debugLogOutput, int use_output_level) {
        this.DebugLogOutput = debugLogOutput;
        this.DEBUGLOGOUTPUTLEVEL = use_output_level;
        this.m_BuildLogOutput.add("BOARD: " + Build.BOARD);
        this.m_BuildLogOutput.add("BRAND: " + Build.BRAND);
        this.m_BuildLogOutput.add("CPU_ABI: " + Build.CPU_ABI);
        this.m_DeviceString = Build.DEVICE;
        this.m_BuildLogOutput.add("DEVICE: " + this.m_DeviceString);
        this.m_BuildLogOutput.add("DISPLAY: " + Build.DISPLAY);
        this.m_BuildLogOutput.add("ID: " + Build.ID);
        this.m_BuildLogOutput.add("MANUFACTURER: " + Build.MANUFACTURER);
        this.m_BuildLogOutput.add("MODEL: " + Build.MODEL);
        this.m_ProductString = Build.PRODUCT;
        this.m_BuildLogOutput.add("PRODUCT: " + this.m_ProductString);
        this.m_BuildLogOutput.add("TAGS: " + Build.TAGS);
        this.m_BuildLogOutput.add("TYPE: " + Build.TYPE);
    }

    public void determineGLabilities(GL10 gl, int view_width, int view_height) {
        this.m_ViewWidth = view_width;
        this.m_ViewHeight = view_height;
        this.m_OpenGLBasicsLogOutput.add("View Width: " + this.m_ViewWidth + " Height: " + this.m_ViewHeight);
        if (gl == null) {
            this.m_OpenGLBasicsLogOutput.add("OpenGL ES API: not used/present");
        } else {
            DebugLogOutput_checkForGlErrors(gl, this.m_OpenGLBasicsLogOutput, "OpenGL ES API", "1.0 available");
            String vendorString = gl.glGetString(7936);
            String rendererString = gl.glGetString(7937);
            String versionString = gl.glGetString(7938);
            this.isGL11_Api = gl instanceof GL11;
            this.isGL11_ApiAndVersion = this.isGL11_Api;
            if (this.isGL11_Api) {
                this.isGL11_ApiAndVersion = versionString.contains("1.1");
                this.isGL11_Reliable = true;
            }
            this.m_SupportedAbilities[0] = true;
            this.m_SupportedAbilities[2] = true;
            this.m_SupportedAbilities[4] = true;
            if (rendererString.contains("MSM7500")) {
                this.isGL11_Reliable = false;
                this.DebugLogOutput.log(this.DEBUGLOGOUTPUTLEVEL, "PhoneAbilities Custom: Renderer==MSM7500");
            }
            if (this.m_DeviceString.contains("GT-I5700")) {
                this.isGL11_Reliable = false;
                this.DebugLogOutput.log(this.DEBUGLOGOUTPUTLEVEL, "PhoneAbilities Custom: Device==GT-I5700");
            }
            if (rendererString.contains("PixelFlinger")) {
                this.isGL11_Reliable = false;
                this.DebugLogOutput.log(this.DEBUGLOGOUTPUTLEVEL, "PhoneAbilities Custom: Renderer==PixelFlinger (WARNING: very slow!)");
                this.m_SupportedAbilities[4] = false;
            }
            if (this.m_DeviceString.contains("SPH M910") || this.m_DeviceString.contains("SPH-M910")) {
                this.isGL11_Reliable = false;
                this.DebugLogOutput.log(this.DEBUGLOGOUTPUTLEVEL, "PhoneAbilities Custom: Device==SPH M910");
            }
            this.m_SupportedAbilities[1] = this.isGL11_Reliable && this.isGL11_ApiAndVersion;
            this.m_SupportedAbilities[3] = this.isGL11_Reliable && this.isGL11_ApiAndVersion;
            if (this.isGL11_Api) {
                if (!this.isGL11_Reliable) {
                    DebugLogOutput_checkForGlErrors(gl, this.m_OpenGLBasicsLogOutput, "OpenGL ES API", "1.1 available, but has been tagged as not reliable on this phone!");
                } else if (!this.isGL11_ApiAndVersion) {
                    DebugLogOutput_checkForGlErrors(gl, this.m_OpenGLBasicsLogOutput, "OpenGL ES API", "1.1 available, but GL Version is not 1.1!");
                } else {
                    DebugLogOutput_checkForGlErrors(gl, this.m_OpenGLBasicsLogOutput, "OpenGL ES API", "1.1 available");
                }
            }
            DebugLogOutput_checkForGlErrors(gl, this.m_OpenGLBasicsLogOutput, "GL Vendor", vendorString);
            DebugLogOutput_checkForGlErrors(gl, this.m_OpenGLBasicsLogOutput, "GL Renderer", rendererString);
            DebugLogOutput_checkForGlErrors(gl, this.m_OpenGLBasicsLogOutput, "GL Version", versionString);
            int[] params = new int[2];
            gl.glGetIntegerv(3413, params, 0);
            this.m_BitDepth += params[0];
            DebugLogOutput_checkForGlErrors(gl, this.m_OpenGLBasicsLogOutput, "Alpha Bits", params[0]);
            gl.glGetIntegerv(3410, params, 0);
            this.m_BitDepth += params[0];
            DebugLogOutput_checkForGlErrors(gl, this.m_OpenGLBasicsLogOutput, "Red Bits", params[0]);
            gl.glGetIntegerv(3411, params, 0);
            this.m_BitDepth += params[0];
            DebugLogOutput_checkForGlErrors(gl, this.m_OpenGLBasicsLogOutput, "Green Bits", params[0]);
            gl.glGetIntegerv(3412, params, 0);
            this.m_BitDepth += params[0];
            DebugLogOutput_checkForGlErrors(gl, this.m_OpenGLBasicsLogOutput, "Blue Bits", params[0]);
            gl.glGetIntegerv(3414, params, 0);
            DebugLogOutput_checkForGlErrors(gl, this.m_OpenGLBasicsLogOutput, "Depth Bits", params[0]);
            gl.glGetIntegerv(3415, params, 0);
            DebugLogOutput_checkForGlErrors(gl, this.m_OpenGLBasicsLogOutput, "Stencil Bits", params[0]);
            gl.glGetIntegerv(33901, params, 0);
            this.m_MinMax_MinValue[0] = params[0];
            this.m_MinMax_MaxValue[0] = params[1];
            if (rendererString.contains("PixelFlinger")) {
                this.m_MinMax_MaxValue[0] = 6;
            }
            DebugLogOutput_checkForGlErrors(gl, this.m_OpenGLMaxLogOutput, "Aliased Point Size Range", String.valueOf(params[0]) + " to " + params[1]);
            gl.glGetIntegerv(33902, params, 0);
            DebugLogOutput_checkForGlErrors(gl, this.m_OpenGLMaxLogOutput, "Aliased Line Width Range", String.valueOf(params[0]) + " to " + params[1]);
            gl.glGetIntegerv(2834, params, 0);
            DebugLogOutput_checkForGlErrors(gl, this.m_OpenGLMaxLogOutput, "Smooth Point Size Range", String.valueOf(params[0]) + " to " + params[1]);
            gl.glGetIntegerv(2850, params, 0);
            DebugLogOutput_checkForGlErrors(gl, this.m_OpenGLMaxLogOutput, "Smooth Line Width Range", String.valueOf(params[0]) + " to " + params[1]);
            gl.glGetIntegerv(3377, params, 0);
            DebugLogOutput_checkForGlErrors(gl, this.m_OpenGLMaxLogOutput, "Max Lights", params[0]);
            gl.glGetIntegerv(3382, params, 0);
            DebugLogOutput_checkForGlErrors(gl, this.m_OpenGLMaxLogOutput, "Max Modelview Stack Depth", params[0]);
            gl.glGetIntegerv(3384, params, 0);
            DebugLogOutput_checkForGlErrors(gl, this.m_OpenGLMaxLogOutput, "Max Projection Stack Depth", params[0]);
            gl.glGetIntegerv(3379, params, 0);
            this.m_MinMax_MinValue[1] = 2;
            this.m_MinMax_MaxValue[1] = params[0];
            DebugLogOutput_checkForGlErrors(gl, this.m_OpenGLMaxLogOutput, "Max Texture Size", params[0]);
            gl.glGetIntegerv(3385, params, 0);
            DebugLogOutput_checkForGlErrors(gl, this.m_OpenGLMaxLogOutput, "Max Texture Stack Depth", params[0]);
            gl.glGetIntegerv(34018, params, 0);
            DebugLogOutput_checkForGlErrors(gl, this.m_OpenGLMaxLogOutput, "Max Texture Units", params[0]);
            gl.glGetIntegerv(3386, params, 0);
            DebugLogOutput_checkForGlErrors(gl, this.m_OpenGLMaxLogOutput, "Max Viewport Dimensions", params[0]);
            if (this.isGL11_Api) {
                gl.glGetIntegerv(3378, params, 0);
                DebugLogOutput_checkForGlErrors(gl, this.m_OpenGLMaxLogOutput, "GL11 MAX_CLIP_PLANES", params[0]);
            }
            String[] extensions = gl.glGetString(7939).split(" ");
            for (String DebugLogOutput_checkForGlErrors : extensions) {
                DebugLogOutput_checkForGlErrors(gl, this.m_OpenGLExtensionsLogOutput, (String) null, DebugLogOutput_checkForGlErrors);
            }
        }
        if (this.DebugLogOutput.isCurrentLogLevelAtleast(this.DEBUGLOGOUTPUTLEVEL)) {
            for (int i = 0; i < this.m_BuildLogOutput.size(); i++) {
                this.DebugLogOutput.log(this.DEBUGLOGOUTPUTLEVEL, "PhoneAbilities | " + this.m_BuildLogOutput.elementAt(i));
            }
            for (int i2 = 0; i2 < this.m_OpenGLBasicsLogOutput.size(); i2++) {
                this.DebugLogOutput.log(this.DEBUGLOGOUTPUTLEVEL, "PhoneAbilities | " + this.m_OpenGLBasicsLogOutput.elementAt(i2));
            }
            for (int i3 = 0; i3 < this.m_OpenGLMaxLogOutput.size(); i3++) {
                this.DebugLogOutput.log(this.DEBUGLOGOUTPUTLEVEL, "PhoneAbilities | " + this.m_OpenGLMaxLogOutput.elementAt(i3));
            }
            for (int i4 = 0; i4 < this.m_OpenGLExtensionsLogOutput.size(); i4++) {
                this.DebugLogOutput.log(this.DEBUGLOGOUTPUTLEVEL, "PhoneAbilities | GL_Extension " + (i4 + 1) + ": " + this.m_OpenGLExtensionsLogOutput.elementAt(i4));
            }
        }
    }

    public void DebugLogOutput_checkForGlErrors(GL10 gl, Vector<String> logoutput, String msg_title, int msg_result) {
        DebugLogOutput_checkForGlErrors(gl, logoutput, msg_title, new StringBuilder().append(msg_result).toString());
    }

    public void DebugLogOutput_checkForGlErrors(GL10 gl, Vector<String> logoutput, String msg_title, String msg_result) {
        int error = gl.glGetError();
        if (error == 0) {
            if (msg_title != null) {
                logoutput.add(String.valueOf(msg_title) + ": " + msg_result);
            } else {
                logoutput.add(msg_result);
            }
        } else if (msg_title != null) {
            logoutput.add("OpenGL Error while retrieving " + msg_title + ": " + glErrorToString(error));
        } else {
            logoutput.add("OpenGL Error while retrieving " + glErrorToString(error));
        }
    }

    private String glErrorToString(int error) {
        switch (error) {
            case 0:
                return String.valueOf(error) + " GL_NO_ERROR";
            case 1280:
                return String.valueOf(error) + " GL_INVALID_ENUM";
            case 1281:
                return String.valueOf(error) + " GL_INVALID_VALUE";
            case 1282:
                return String.valueOf(error) + " GL_INVALID_OPERATION";
            case 1283:
                return String.valueOf(error) + " GL_STACK_OVERFLOW";
            case 1284:
                return String.valueOf(error) + " GL_STACK_UNDERFLOW";
            case 1285:
                return String.valueOf(error) + " GL_OUT_OF_MEMORY";
            default:
                return String.valueOf(error) + " UNKNOWN error number!";
        }
    }
}
