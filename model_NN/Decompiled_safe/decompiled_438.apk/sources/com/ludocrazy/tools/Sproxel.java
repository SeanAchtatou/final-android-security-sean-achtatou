package com.ludocrazy.tools;

import android.content.Context;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.nio.FloatBuffer;

public class Sproxel extends PointSpritesMesh {
    private static final String SPROXELPATH = "sproxel/";
    public static final int SPROXELS_PER_FIELD = 10;
    public String m_SproxelFilename = null;
    private boolean m_WorldCoordinatesUsed = false;
    private float m_WorldOffsetX = 0.0f;
    private float m_WorldOffsetY = 0.0f;
    private float m_WorldOffsetZ = 0.0f;
    private float m_WorldScaleX = 1.0f;
    private float m_WorldScaleY = 1.0f;
    private float m_WorldScaleZ = 1.0f;
    public float m_Zmax = 1.0f;

    public void preLoad_setWorldCoordinates(float scale_x, float scale_y, float scale_z, float offset_x, float offset_y, float offset_z) {
        this.m_WorldCoordinatesUsed = true;
        this.m_WorldScaleX = scale_x;
        this.m_WorldScaleY = scale_y;
        this.m_WorldScaleZ = scale_z;
        this.m_WorldOffsetX = offset_x;
        this.m_WorldOffsetY = offset_y;
        this.m_WorldOffsetZ = offset_z;
    }

    public void loadCubeKingdomFile(Context context, String sproxelFilename, boolean swap_y_and_z) {
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(context.getAssets().open(SPROXELPATH + sproxelFilename, 2)), 4096);
            String fileformat = bufferedReader.readLine();
            if (!fileformat.matches("CubeKingdom")) {
                throw new Exception("Error: Trying to read 'CubeKingdom' from first line of file, but got: " + fileformat);
            } else if (!bufferedReader.readLine().split(" ")[0].equals("1")) {
                throw new Exception(".cko File must contain colored voxel defintions! Read aborted");
            } else {
                int voxels = Integer.parseInt(bufferedReader.readLine());
                setupArrays(voxels);
                float x_min = 99999.9f;
                float y_min = 99999.9f;
                float z_min = 99999.9f;
                float x_max = -99999.9f;
                float y_max = -99999.9f;
                float z_max = -99999.9f;
                int i = 0;
                while (i < voxels) {
                    String line = bufferedReader.readLine();
                    if (line == null) {
                        throw new Exception(".cko File has less entries (" + i + ") than header claims (" + voxels + ")! Read aborted");
                    }
                    String[] voxel = line.split(" ");
                    float x = (float) Integer.parseInt(voxel[0]);
                    float z = (float) Integer.parseInt(voxel[1]);
                    float y = (float) Integer.parseInt(voxel[2]);
                    float a = ((float) Integer.parseInt(voxel[3].substring(0, 2), 16)) / 255.0f;
                    float r = ((float) Integer.parseInt(voxel[3].substring(2, 4), 16)) / 255.0f;
                    float g = ((float) Integer.parseInt(voxel[3].substring(4, 6), 16)) / 255.0f;
                    float b = ((float) Integer.parseInt(voxel[3].substring(6, 8), 16)) / 255.0f;
                    if (x < x_min) {
                        x_min = x;
                    }
                    if (y < y_min) {
                        y_min = y;
                    }
                    if (z < z_min) {
                        z_min = z;
                    }
                    if (x > x_max) {
                        x_max = x;
                    }
                    if (y > y_max) {
                        y_max = y;
                    }
                    if (z > z_max) {
                        z_max = z;
                    }
                    this.m_vertices[(i * 3) + 0] = x;
                    this.m_vertices[(i * 3) + 1] = y;
                    this.m_vertices[(i * 3) + 2] = z;
                    this.m_colors[(i * 4) + 0] = r;
                    this.m_colors[(i * 4) + 1] = g;
                    this.m_colors[(i * 4) + 2] = b;
                    this.m_colors[(i * 4) + 3] = a;
                    i++;
                }
                int POINTSPRITESCOUNT = i;
                float x_size = Math.abs(x_max - x_min) + 1.0f;
                float y_size = Math.abs(y_max - y_min) + 1.0f;
                float scaleFactor = x_size;
                if (y_size > scaleFactor) {
                    scaleFactor = y_size;
                }
                if (scaleFactor < 10.0f) {
                    scaleFactor = 10.0f;
                }
                this.m_Zmax = (z_max - z_min) / scaleFactor;
                for (int i2 = 0; i2 < POINTSPRITESCOUNT; i2++) {
                    this.m_vertices[(i2 * 3) + 0] = (((this.m_vertices[(i2 * 3) + 0] - x_min) + 0.5f) / scaleFactor) - 0.5f;
                    this.m_vertices[(i2 * 3) + 1] = (((this.m_vertices[(i2 * 3) + 1] - y_min) + 0.5f) / scaleFactor) - 0.5f;
                    this.m_vertices[(i2 * 3) + 2] = (this.m_vertices[(i2 * 3) + 2] - z_min) / scaleFactor;
                    if (swap_y_and_z) {
                        float temp = this.m_vertices[(i2 * 3) + 1];
                        this.m_vertices[(i2 * 3) + 1] = this.m_vertices[(i2 * 3) + 2];
                        this.m_vertices[(i2 * 3) + 2] = temp;
                    }
                    if (this.m_WorldCoordinatesUsed) {
                        this.m_vertices[(i2 * 3) + 0] = this.m_WorldOffsetX + (this.m_WorldScaleX * this.m_vertices[(i2 * 3) + 0]);
                        this.m_vertices[(i2 * 3) + 1] = this.m_WorldOffsetY + (this.m_WorldScaleY * this.m_vertices[(i2 * 3) + 1]);
                        this.m_vertices[(i2 * 3) + 2] = this.m_WorldOffsetZ + (this.m_WorldScaleZ * this.m_vertices[(i2 * 3) + 2]);
                    }
                }
                convertArrays();
                this.m_SproxelFilename = sproxelFilename;
            }
        } catch (Exception e) {
            new ErrorOutput("Sproxel::Sproxel()", "Error reading CubeKingdom file: " + sproxelFilename + ". Exception:", e);
        }
    }

    /* access modifiers changed from: package-private */
    public float[] getTranslatedInterleavedArray(float translation_x, float translation_y, float translation_z, int rotation, float scale_x, float scale_y, float scale_z) {
        float[] vertices = new float[(this.vertexBuffer.capacity() + this.colorBuffer.capacity())];
        int x_offset = 0;
        int y_offset = 1;
        if (rotation == SproxelInfo.ROTATE_EAST || (rotation == SproxelInfo.ROTATE_RANDOM && Tools.random() < 0.5f)) {
            x_offset = 1;
            y_offset = 0;
        }
        if (scale_x <= SproxelInfo.SCALE_RANDOM) {
            scale_x = 1.0f;
            if (Tools.random() < 0.5f) {
                scale_x = -1.0f;
            }
        }
        if (scale_y <= SproxelInfo.SCALE_RANDOM) {
            scale_y = 1.0f;
            if (Tools.random() < 0.5f) {
                scale_y = -1.0f;
            }
        }
        for (int i = 0; i < this.m_PointSpriteVertexCount; i++) {
            vertices[(i * 7) + 0] = (this.vertexBuffer.get((i * 3) + x_offset) * scale_x) + translation_x;
            vertices[(i * 7) + 1] = (this.vertexBuffer.get((i * 3) + y_offset) * scale_y) + translation_y;
            vertices[(i * 7) + 2] = (this.vertexBuffer.get((i * 3) + 2) * scale_z) + translation_z;
            vertices[(i * 7) + 3] = this.colorBuffer.get((i * 4) + 0);
            vertices[(i * 7) + 4] = this.colorBuffer.get((i * 4) + 1);
            vertices[(i * 7) + 5] = this.colorBuffer.get((i * 4) + 2);
            vertices[(i * 7) + 6] = this.colorBuffer.get((i * 4) + 3);
        }
        return vertices;
    }

    /* access modifiers changed from: package-private */
    public float[] getTranslatedVertexBuffer(float translation_x, float translation_y, float translation_z, int rotation, float scale_x, float scale_y, float scale_z) {
        int size = this.vertexBuffer.capacity();
        float[] vertices = new float[size];
        int x_offset = 0;
        int y_offset = 1;
        if (rotation == SproxelInfo.ROTATE_EAST || (rotation == SproxelInfo.ROTATE_RANDOM && Tools.random() < 0.5f)) {
            x_offset = 1;
            y_offset = 0;
        }
        if (scale_x <= SproxelInfo.SCALE_RANDOM) {
            scale_x = 1.0f;
            if (Tools.random() < 0.5f) {
                scale_x = -1.0f;
            }
        }
        if (scale_y <= SproxelInfo.SCALE_RANDOM) {
            scale_y = 1.0f;
            if (Tools.random() < 0.5f) {
                scale_y = -1.0f;
            }
        }
        for (int i = 0; i < size; i += 3) {
            vertices[i + 0] = (this.vertexBuffer.get(i + x_offset) * scale_x) + translation_x;
            vertices[i + 1] = (this.vertexBuffer.get(i + y_offset) * scale_y) + translation_y;
            vertices[i + 2] = (this.vertexBuffer.get(i + 2) * scale_z) + translation_z;
        }
        return vertices;
    }

    /* access modifiers changed from: package-private */
    public FloatBuffer getColorBuffer_PositionAtZero() {
        this.colorBuffer.position(0);
        return this.colorBuffer;
    }
}
