package com.ludocrazy.tools;

import com.ludocrazy.tools.Font;
import java.util.Vector;
import javax.microedition.khronos.opengles.GL10;

public class GuiButton extends GuiGroup {
    protected LogOutput DebugLogOutput = null;
    public int m_CurrentText = -1;
    protected Font m_Font = null;
    protected GuiMesh m_HightLightMesh = null;
    public int m_LastText = -1;
    protected int m_LineLength = 30;
    public boolean m_RebuildNeeded = false;
    protected Font.Text m_Text = null;
    protected Vector<String> m_Texts = new Vector<>();
    float m_TextureSquareSizePixels = 512.0f;
    protected boolean m_TouchDown = false;

    public GuiButton(LogOutput DEBUGLOG_to_use, PhoneAbilities phoneAbilities, float texture_square_size_pixels, Font textboxfont, boolean textCentered, GuiMeshBatch guiMeshBatch, GuiBase parent, float left, float top, float width, float height, float boxZ, float alpha, boolean hasBorder) throws Exception {
        super(DEBUGLOG_to_use, phoneAbilities, texture_square_size_pixels, guiMeshBatch, parent, left, top, width, height, boxZ, alpha, hasBorder);
        this.m_TextureSquareSizePixels = texture_square_size_pixels;
        this.m_HightLightMesh = new GuiMesh(DEBUGLOG_to_use, phoneAbilities);
        this.m_Font = textboxfont;
        this.m_Text = this.m_Font.newText(DEBUGLOG_to_use, this.m_GuiMeshBatch, phoneAbilities);
        this.m_Text.setTextArea(width, height);
        this.m_Text.setScale(parent.getBasisTextScale());
        float textZ = this.m_BoxZ + 0.2f;
        float centerX = this.m_Left + (width / 2.0f);
        float centerY = this.m_Top + (height / 2.0f);
        if (!textCentered) {
            this.m_Text.setPosition(this.m_Left + 0.25f, this.m_Bottom - 0.4f, textZ);
            this.m_Text.setHorizontalAlign(Font.HorizontalAlign.Left);
            this.m_Text.setVerticalAlign(Font.VerticalAlign.Top);
        } else {
            this.m_Text.setPosition(centerX, centerY, textZ);
            this.m_Text.setHorizontalAlign(Font.HorizontalAlign.Center);
            this.m_Text.setVerticalAlign(Font.VerticalAlign.Center);
        }
        this.m_HightLightMesh.setupArraysQuadFaces(1);
        float SIZE = 16.0f / this.m_TextureSquareSizePixels;
        this.m_HightLightMesh.addQuad(this.m_Left, this.m_Top, 0.1f + boxZ, this.m_Right, this.m_Bottom, 1.0f - SIZE, 1.0f - SIZE, 1.0f, 1.0f - (SIZE / 2.0f));
        ColorFloats colorFloats = new ColorFloats(1.0f, 0.8f, 0.0f, 0.35f);
        this.m_HightLightMesh.addColorsForLastQuad(colorFloats, colorFloats, colorFloats, colorFloats);
        this.m_HightLightMesh.convertArraysToBuffer();
        this.DebugLogOutput = DEBUGLOG_to_use;
    }

    public void setTextColor(float r, float g, float b, float a) {
        this.m_Text.setTextColor(r, g, b, a);
    }

    public void onResume() {
        this.m_HightLightMesh.onResume();
        super.onResume();
    }

    public void setLineLength(int break_after_this_many_characters) {
        this.m_LineLength = break_after_this_many_characters;
    }

    public void clearTextStack() {
        this.m_Texts.clear();
        this.m_LastText = -1;
        this.m_CurrentText = -1;
    }

    public void addTextToStack(String text) {
        this.m_Texts.add(Tools.splitStringIntoLines(text, this.m_LineLength));
        if (this.m_CurrentText == -1) {
            advanceText();
        }
    }

    public int getCurrenTextIndex() {
        return this.m_CurrentText;
    }

    public int getTextMaxIndex() {
        return this.m_Texts.size() - 1;
    }

    public boolean advanceText() {
        boolean endReached = false;
        this.m_CurrentText++;
        if (this.m_CurrentText >= this.m_Texts.size()) {
            this.m_CurrentText = 0;
            endReached = true;
        }
        if (this.m_CurrentText != this.m_LastText) {
            this.m_RebuildNeeded = true;
            this.m_LastText = this.m_CurrentText;
            setRedrawNeeded();
        }
        return endReached;
    }

    public boolean draw(GL10 gl) throws Exception {
        if (this.m_VisibleOnOrthoCamera) {
            this.m_RedrawNeeded = super.draw(gl);
            if (this.m_TouchDown) {
                this.m_HightLightMesh.draw(gl);
            }
            if (this.m_Texts.size() > 0) {
                if (this.m_RebuildNeeded) {
                    this.m_Text.setText_rebuildingMesh(this.m_Texts.elementAt(this.m_CurrentText));
                    this.m_RebuildNeeded = false;
                }
                this.m_RedrawNeeded = this.m_Text.render(gl) || this.m_RedrawNeeded;
            }
        } else {
            this.m_RedrawNeeded = false;
        }
        return this.m_RedrawNeeded;
    }

    public boolean checkAndHandleTouchDown(float x_percentage, float y_percentage) {
        this.m_TouchDown = super.checkAndHandleTouchDown(x_percentage, y_percentage);
        if (!this.m_TouchDown && this.m_Touchable && this.m_VisibleOnOrthoCamera) {
            float x = this.m_OrthoCameraMinX + ((this.m_OrthoCameraMaxX - this.m_OrthoCameraMinX) * x_percentage);
            float y = this.m_OrthoCameraMinY + ((this.m_OrthoCameraMaxY - this.m_OrthoCameraMinY) * y_percentage);
            if (x >= this.m_Left && x <= this.m_Right && y >= this.m_Top && y <= this.m_Bottom) {
                this.m_TouchDown = true;
                setRedrawNeeded();
            }
        }
        return this.m_TouchDown;
    }

    public boolean checkAndHandleTouchUp(float x_percentage, float y_percentage) {
        int i;
        this.m_TouchDown = false;
        boolean touchUp = super.checkAndHandleTouchUp(x_percentage, y_percentage);
        if (this.DebugLogOutput.isCurrentLogLevelAtleast(1)) {
            String name = "NOTEXT " + this.m_Left + ", " + this.m_Right + ", " + this.m_Top + ", " + this.m_Bottom;
            if (this.m_Texts.size() > 0) {
                String elementAt = this.m_Texts.elementAt(0);
                if (this.m_Texts.elementAt(0).length() < 20) {
                    i = this.m_Texts.elementAt(0).length();
                } else {
                    i = 20;
                }
                name = elementAt.substring(0, i);
            }
            this.DebugLogOutput.log(3, "touchup: " + name + " - super (children):" + touchUp);
        }
        if (touchUp || !this.m_Touchable || !this.m_VisibleOnOrthoCamera) {
            return touchUp;
        }
        float x = this.m_OrthoCameraMinX + ((this.m_OrthoCameraMaxX - this.m_OrthoCameraMinX) * x_percentage);
        float y = this.m_OrthoCameraMinY + ((this.m_OrthoCameraMaxY - this.m_OrthoCameraMinY) * y_percentage);
        this.DebugLogOutput.log(3, "touchup - checking...");
        if (x < this.m_Left || x > this.m_Right || y < this.m_Top || y > this.m_Bottom) {
            return touchUp;
        }
        this.DebugLogOutput.log(2, "touched up!");
        setRedrawNeeded();
        advanceText();
        handleTouchUp();
        return true;
    }

    public void handleTouchUp() {
    }
}
