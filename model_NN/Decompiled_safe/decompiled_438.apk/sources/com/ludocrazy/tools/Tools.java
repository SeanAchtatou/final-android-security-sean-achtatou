package com.ludocrazy.tools;

import android.os.Debug;
import android.util.Log;
import java.util.Random;

public class Tools {
    public static final float PI = 3.1415927f;
    public static Random m_RandomGenerator = new Random(1980);

    public static void setupRandomSeed(long seed) {
        m_RandomGenerator.setSeed(seed);
    }

    public static void testRandom() {
        StopWatch watch = new StopWatch();
        long sum = 0;
        for (long i = 0; i < 500000; i++) {
            sum = (long) (((double) sum) + (Math.random() * 20.0d));
        }
        Log.d("com.ludocrazy.misc", "1mio math.random took" + watch.getElapsedMilliSeconds() + "ms. average sum=" + (((float) sum) / ((float) 500000)));
        watch.restart();
        long sum2 = 0;
        for (long i2 = 0; i2 < 500000; i2++) {
            sum2 = (long) (((double) sum2) + (m_RandomGenerator.nextDouble() * 20.0d));
        }
        Log.d("com.ludocrazy.misc", "1mio Random.nextDouble took" + watch.getElapsedMilliSeconds() + "ms. average sum=" + (((float) sum2) / ((float) 500000)));
        watch.restart();
        long sum3 = 0;
        for (long i3 = 0; i3 < 500000; i3++) {
            sum3 = (long) (((float) sum3) + (m_RandomGenerator.nextFloat() * 20.0f));
        }
        Log.d("com.ludocrazy.misc", "1mio Random.nextFloat took" + watch.getElapsedMilliSeconds() + "ms. average sum=" + (((float) sum3) / ((float) 500000)));
        watch.restart();
        long sum4 = 0;
        for (long i4 = 0; i4 < 500000; i4++) {
            sum4 += (long) randomInt(5, 15);
        }
        Log.d("com.ludocrazy.misc", "1mio randomInt(min,max) took" + watch.getElapsedMilliSeconds() + "ms. average sum=" + (((float) sum4) / ((float) 500000)));
        watch.restart();
        long sum5 = 0;
        for (long i5 = 0; i5 < 500000; i5++) {
            sum5 += (long) (m_RandomGenerator.nextInt() % 20);
        }
        Log.d("com.ludocrazy.misc", "1mio Random.nextInt took" + watch.getElapsedMilliSeconds() + "ms. average sum=" + (((float) sum5) / ((float) 500000)));
    }

    public static int randomInt(int min, int max) {
        if (max < min) {
            return max;
        }
        return m_RandomGenerator.nextInt((max + 1) - min) + min;
    }

    public static float random(float min, float max) {
        return (m_RandomGenerator.nextFloat() * (max - min)) + min;
    }

    public static float random() {
        return m_RandomGenerator.nextFloat();
    }

    public static float randomizeNearLowerBound(float min, float max, float bound_percentage) {
        return (m_RandomGenerator.nextFloat() * (max - min) * bound_percentage) + min;
    }

    public static float clamp(float value, float min, float max) {
        if (value < min) {
            return min;
        }
        if (value > max) {
            return max;
        }
        return value;
    }

    public static float dist(float x1, float y1, float x2, float y2) {
        float xdiff = x2 - x1;
        float ydiff = y2 - y1;
        return (float) Math.sqrt((double) ((xdiff * xdiff) + (ydiff * ydiff)));
    }

    public static int array_modulo(int value, int divisor) {
        while (value < 0) {
            value += divisor;
        }
        return value % divisor;
    }

    public static float array_modulo_float(float value, float divisor) {
        while (value < 0.0f) {
            value += divisor;
        }
        return value % divisor;
    }

    public static float radToDeg(float radians_angle) {
        return (radians_angle / 3.1415927f) * 180.0f;
    }

    public static float degToRad(float degree_angle) {
        return (degree_angle / 180.0f) * 3.1415927f;
    }

    public static float animateBlink_useReturnValueAbsolute(float value, float step) {
        float value2 = value + step;
        if (value2 >= 1.0f) {
            return -1.0f;
        }
        return value2;
    }

    public static String splitStringIntoLines(String infoText, int max_characters_per_line) {
        int currentLineLength;
        boolean manualWrap;
        String returnLines = "";
        int currentLineLength2 = -1;
        int lastwordStart = 0;
        if (infoText.length() <= max_characters_per_line) {
            currentLineLength2 = 0;
        }
        int c = 0;
        while (c < infoText.length()) {
            currentLineLength++;
            if (infoText.charAt(c) == '~' || infoText.charAt(c) == 10) {
                manualWrap = true;
            } else {
                manualWrap = false;
            }
            if (currentLineLength > max_characters_per_line || manualWrap) {
                if (manualWrap) {
                    lastwordStart = c;
                }
                int lineStart = c - currentLineLength;
                if (lineStart > 0) {
                    lineStart++;
                }
                try {
                    returnLines = String.valueOf(returnLines) + infoText.substring(lineStart, lineStart + (lastwordStart - lineStart)) + "\n";
                } catch (Exception e) {
                }
                currentLineLength = c - lastwordStart;
            }
            if (infoText.charAt(c) == ' ') {
                lastwordStart = c;
            }
            c++;
        }
        int lineStart2 = c - currentLineLength;
        return String.valueOf(returnLines) + infoText.substring(lineStart2, lineStart2 + (infoText.length() - lineStart2));
    }

    public static float shortenFloatDigits_SLOW(float value, int sub_digits) {
        float push = (float) Math.pow(10.0d, (double) sub_digits);
        return ((float) ((int) (value * push))) / push;
    }

    public static String getMemoryStats() {
        float NatHeapUsed = shortenFloatDigits_SLOW(((float) Debug.getNativeHeapAllocatedSize()) * 9.536743E-7f, 1);
        float NatHeapSize = shortenFloatDigits_SLOW(((float) Debug.getNativeHeapSize()) * 9.536743E-7f, 1);
        float RunTotalMem = shortenFloatDigits_SLOW(((float) Runtime.getRuntime().totalMemory()) * 9.536743E-7f, 1);
        return " MemUsed: " + RunTotalMem + " MB / " + shortenFloatDigits_SLOW(((float) Runtime.getRuntime().maxMemory()) * 9.536743E-7f, 1) + " MB, Native: " + NatHeapUsed + " MB (" + NatHeapSize + " MB)";
    }
}
