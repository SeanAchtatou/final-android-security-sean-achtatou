package com.ludocrazy.tools;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;

public class TCPClient {
    private LogOutput DebugLog = null;
    private int m_LogLevel = 2;
    private String m_ServerIp = "192.168.0.5";
    private int m_ServerPort = 16001;
    private Socket m_Socket = null;
    private InputStream m_SocketInputStream = null;
    private PrintWriter m_SocketOutputPrintWriter = null;
    private OutputStream m_SocketOutputStream = null;

    public TCPClient(LogOutput debugLog, int loglevel, String server_ip, int server_port) {
        this.DebugLog = debugLog;
        this.m_LogLevel = loglevel;
        this.m_ServerIp = server_ip;
        this.m_ServerPort = server_port;
    }

    public OutputStream connect() throws Exception {
        try {
            this.DebugLog.log(this.m_LogLevel, "TCPclient.connect():  getting Adress");
            InetAddress serverAddr = InetAddress.getByName(this.m_ServerIp);
            this.DebugLog.log(this.m_LogLevel, "TCPclient.connect():  creating new socket on port");
            this.m_Socket = new Socket(serverAddr, this.m_ServerPort);
            this.DebugLog.log(this.m_LogLevel, "TCPclient.connect():  retrieving streams");
            this.m_SocketOutputStream = this.m_Socket.getOutputStream();
            this.m_SocketInputStream = this.m_Socket.getInputStream();
            this.m_SocketOutputPrintWriter = new PrintWriter(this.m_SocketOutputStream);
            return this.m_SocketOutputStream;
        } catch (Exception e) {
            Exception e2 = e;
            this.m_SocketOutputStream = null;
            this.m_SocketInputStream = null;
            this.m_Socket = null;
            throw e2;
        }
    }

    public void sendMessage(String message) throws Exception {
        if (this.m_Socket == null) {
            throw new Exception("Can't send message: socket not connected!");
        } else if (!this.m_Socket.isConnected()) {
            throw new Exception("Socket isConnected returns false!");
        } else {
            int length = message.length();
            if (length > 255) {
                throw new Exception("Can't send message: length must be less than 256 characters! is: " + length);
            }
            this.DebugLog.log(this.m_LogLevel, "TCPclient.sendMessage(): sending message with length: " + length + " content:" + message);
            this.m_SocketOutputPrintWriter.write(length);
            this.m_SocketOutputPrintWriter.write(message);
            this.m_SocketOutputPrintWriter.flush();
        }
    }

    public String receiveMessage() throws Exception {
        if (this.m_Socket == null) {
            throw new Exception("Can't receive message: socket not connected!");
        } else if (!this.m_Socket.isConnected()) {
            throw new Exception("Socket isConnected returns false!");
        } else {
            this.DebugLog.log(this.m_LogLevel, "TCPclient.receiveMessage(): waiting for message length ...");
            int messagelength = this.m_SocketInputStream.read();
            this.DebugLog.log(this.m_LogLevel, "TCPclient.receiveMessage(): receiving message of length" + messagelength);
            if (messagelength <= 0) {
                return "";
            }
            int message_offset = 0;
            byte[] message = new byte[messagelength];
            while (message_offset < messagelength) {
                int read_length = this.m_SocketInputStream.read(message, message_offset, messagelength - message_offset);
                if (read_length <= 0) {
                    throw new Exception("Error on receiving message: no further data / end of stream before end of message!");
                }
                message_offset += read_length;
            }
            String message_string = new String(message);
            this.DebugLog.log(this.m_LogLevel, "TCPclient.receiveMessage(): received message:" + message_string);
            return message_string;
        }
    }

    public void disconnect() throws IOException {
        if (this.m_Socket != null) {
            if (this.m_SocketInputStream != null) {
                this.m_SocketInputStream.close();
            }
            if (this.m_SocketOutputPrintWriter != null) {
                this.m_SocketOutputPrintWriter.close();
            }
            if (this.m_SocketOutputStream != null) {
                this.m_SocketOutputStream.close();
            }
            this.m_Socket.close();
        }
        this.m_Socket = null;
    }
}
