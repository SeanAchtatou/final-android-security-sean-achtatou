package com.ludocrazy.tools;

import android.content.Context;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.nio.ShortBuffer;
import javax.microedition.khronos.opengles.GL10;

public class ParticleManager {
    private LogOutput DebugLogOutput = null;
    public final int MAXPARTICLESRENDERABLE = 1500;
    private Context m_Context = null;
    private FloatBuffer m_InterleavedBuffer = null;
    private ShortBuffer m_QuadsIndexBuffer = null;
    private FloatBuffer m_QuadsInterleavedBuffer = null;

    private native synchronized int LoadEffectFile(String str, String str2);

    public native synchronized int CreateParticleFromPosition(String str, float f, float f2, float f3, float f4, float f5, float f6, float f7, float f8, float f9, float f10);

    public native synchronized int CreateParticleFromPositionById(int i, float f, float f2, float f3, float f4, float f5, float f6, float f7, float f8, float f9, float f10);

    public native synchronized int GetRenderableParticlesAsNonSizedPointSprites(FloatBuffer floatBuffer);

    public native synchronized int GetRenderableParticlesAsSizedPointSprites(FloatBuffer floatBuffer);

    public native synchronized void GetStatusInfoCounts(IntBuffer intBuffer);

    public native synchronized int GetVisibleParticlesAsTriangledQuads(FloatBuffer floatBuffer, ShortBuffer shortBuffer);

    public native synchronized int SetEffectEmitZone(int i, float f, float f2, float f3, float f4, float f5, float f6);

    public native synchronized int SetEffectPosition(int i, float f, float f2, float f3);

    public native synchronized int StartLoadedEffect(String str, float f, float f2, float f3, float f4, float f5, float f6, float f7, float f8, float f9, float f10);

    public native synchronized int StartLoadedEffectById(int i, float f, float f2, float f3, float f4, float f5, float f6, float f7, float f8, float f9, float f10);

    public native synchronized int StartLoadedEffectMoving(String str, float f, float f2, float f3, float f4, float f5, float f6, float f7, float f8, float f9, float f10, float f11, float f12, float f13);

    public native synchronized int StartLoadedEffectMovingById(int i, float f, float f2, float f3, float f4, float f5, float f6, float f7, float f8, float f9, float f10, float f11, float f12, float f13);

    public native synchronized int StopAllRunningEmitters();

    public native synchronized int StopLoadedEffect(int i);

    public native synchronized int UpdateEffects(float f);

    static {
        try {
            System.loadLibrary("dripFXjni");
        } catch (Exception e) {
            new ErrorOutput("ParticleManager", "Exception loading Library 'dripFXjni':", e);
        }
    }

    public ParticleManager(Context context, LogOutput DebugLogOutput_to_use) {
        this.m_Context = context;
        ByteBuffer b = ByteBuffer.allocateDirect(42000);
        b.order(ByteOrder.nativeOrder());
        this.m_InterleavedBuffer = b.asFloatBuffer();
        ByteBuffer blarge = ByteBuffer.allocateDirect(216000);
        blarge.order(ByteOrder.nativeOrder());
        this.m_QuadsInterleavedBuffer = blarge.asFloatBuffer();
        ByteBuffer bshorts = ByteBuffer.allocateDirect(18000);
        bshorts.order(ByteOrder.nativeOrder());
        this.m_QuadsIndexBuffer = bshorts.asShortBuffer();
        this.DebugLogOutput = DebugLogOutput_to_use;
    }

    public int LoadEffectFileFromRes(int resId) throws Exception {
        String filename = this.m_Context.getResources().getString(resId);
        InputStream is = this.m_Context.getResources().openRawResource(resId);
        StringBuilder sb = new StringBuilder();
        BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"), 4096);
        do {
            String line = reader.readLine();
            if (line == null) {
                break;
            }
            sb.append(line).append("\n");
        } while (sb.length() <= 5000);
        is.close();
        int loadedEffectId = LoadEffectFile(sb.toString(), filename);
        this.DebugLogOutput.log(2, "ParticleManager.LoadEffectFileFromRes() effect load result in id: " + loadedEffectId);
        return loadedEffectId;
    }

    public synchronized int drawTest() {
        return GetRenderableParticlesAsNonSizedPointSprites(this.m_InterleavedBuffer);
    }

    public synchronized int drawNonSizedPointSprites(GL10 gl) {
        int renderableParticlesCount;
        renderableParticlesCount = GetRenderableParticlesAsNonSizedPointSprites(this.m_InterleavedBuffer);
        int stride_bytes = 4 * 7;
        this.m_InterleavedBuffer.position(0);
        gl.glVertexPointer(3, 5126, stride_bytes, this.m_InterleavedBuffer);
        this.m_InterleavedBuffer.position(3);
        gl.glColorPointer(4, 5126, stride_bytes, this.m_InterleavedBuffer);
        gl.glDrawArrays(0, 0, renderableParticlesCount);
        return renderableParticlesCount;
    }

    public synchronized int drawSizedPointSprites(GL10 gl) {
        int renderableParticlesCount;
        renderableParticlesCount = GetRenderableParticlesAsSizedPointSprites(this.m_InterleavedBuffer);
        int stride_bytes = 4 * 8;
        this.m_InterleavedBuffer.position(0);
        gl.glVertexPointer(3, 5126, stride_bytes, this.m_InterleavedBuffer);
        this.m_InterleavedBuffer.position(3);
        gl.glColorPointer(4, 5126, stride_bytes, this.m_InterleavedBuffer);
        gl.glDrawArrays(0, 0, renderableParticlesCount);
        return renderableParticlesCount;
    }

    public synchronized int drawTriangledQuads(GL10 gl) {
        int renderableParticlesCount;
        renderableParticlesCount = GetVisibleParticlesAsTriangledQuads(this.m_QuadsInterleavedBuffer, this.m_QuadsIndexBuffer);
        this.m_QuadsInterleavedBuffer.position(0);
        gl.glVertexPointer(3, 5126, 36, this.m_QuadsInterleavedBuffer);
        this.m_QuadsInterleavedBuffer.position(3);
        gl.glColorPointer(4, 5126, 36, this.m_QuadsInterleavedBuffer);
        this.m_QuadsInterleavedBuffer.position(7);
        gl.glTexCoordPointer(2, 5126, 36, this.m_QuadsInterleavedBuffer);
        this.m_QuadsIndexBuffer.position(0);
        gl.glDrawElements(4, renderableParticlesCount * 6, 5123, this.m_QuadsIndexBuffer);
        return renderableParticlesCount;
    }
}
