package com.ludocrazy.tools;

import java.lang.reflect.Array;
import javax.microedition.khronos.opengles.GL10;

public class CameraPerspective {
    public static final float CAMERABLENDFACTOR = 0.05f;
    public Vector3D m_CameraCurrentSide = new Vector3D(1.0f, 0.0f, 0.0f);
    private Vector3D m_CameraLookAt = new Vector3D();
    private Vector3D m_CameraPos = new Vector3D();
    private Vector3D m_CameraPosTarget = new Vector3D();
    public Vector3D m_CameraUp = new Vector3D(0.0f, 1.0f, 0.0f);
    public Vector3D m_Eye = new Vector3D();
    private float[][] m_FrustumPlanes = ((float[][]) Array.newInstance(Float.TYPE, 6, 4));
    public float[] m_ProjectionMatrix = new float[16];
    public float[] m_ViewMatrix = new float[16];
    private float m_ViewPortHeight = 480.0f;
    private float m_ViewPortWidth = 854.0f;

    public class PickRay {
        public Vector3D direction = new Vector3D();
        public Vector3D origin = new Vector3D();

        public PickRay() {
        }
    }

    public void setInitialPos(float camera_pos_x, float camera_pos_y, float camera_pos_z, float camera_pos_target_x, float camera_pos_target_y, float camera_pos_target_z) {
        this.m_CameraPos.x = camera_pos_x;
        this.m_CameraPos.y = camera_pos_y;
        this.m_CameraPos.z = camera_pos_z;
        this.m_CameraPosTarget.x = camera_pos_target_x;
        this.m_CameraPosTarget.y = camera_pos_target_y;
        this.m_CameraPosTarget.z = camera_pos_target_z;
    }

    public void setupViewPort(float width, float height) {
        this.m_ViewPortWidth = width;
        this.m_ViewPortHeight = height;
    }

    public void setCamera(Vector3D pos, Vector3D look_at) {
        setInitialPos(pos.x, pos.y, pos.z, pos.x, pos.y, pos.z);
        this.m_CameraLookAt.x = look_at.x;
        this.m_CameraLookAt.y = look_at.y;
        this.m_CameraLookAt.z = look_at.z;
    }

    public void setupProjectionMatrix_ViaPerspectiveCamera_UpdatingCameraTargetBlend(GL10 gl) throws Exception {
        this.m_CameraPos.interpolate_linear(this.m_CameraPosTarget, 0.05f);
        setupProjectionMatrix_ViaPerspectiveCamera(gl);
    }

    public void setupProjectionMatrix_ViaPerspectiveCamera(GL10 gl) throws Exception {
        gl.glMatrixMode(5889);
        gl.glLoadIdentity();
        setupProjectionMatrixViaPerspective(gl, 90.0f, this.m_ViewPortWidth / this.m_ViewPortHeight, 0.01f, 1700.0f);
        gl.glMatrixMode(5888);
        gl.glLoadIdentity();
        setupViewMatrixViaLookAt(gl, this.m_CameraPos.x, this.m_CameraPos.y, this.m_CameraPos.z, this.m_CameraLookAt.x, this.m_CameraLookAt.y, this.m_CameraLookAt.z, this.m_CameraUp.x, this.m_CameraUp.y, this.m_CameraUp.z);
    }

    public void setupProjectionMatrixViaPerspective(GL10 gl, float fov, float aspect, float zNear, float zFar) throws Exception {
        float radians = (float) ((((double) (fov / 2.0f)) * 3.141592653589793d) / 180.0d);
        float deltaZ = zFar - zNear;
        double sine = Math.sin((double) radians);
        if (deltaZ == 0.0f) {
            throw new Exception("Projection Error: zNear and zFar are identical!");
        } else if (aspect == 0.0f) {
            throw new Exception("Projection Error: Incorrect aspect, may not be zero!");
        } else if (sine == 0.0d) {
            throw new Exception("Projection Error: sinus(fov) is zero!");
        } else {
            float cotangent = (float) (Math.cos((double) radians) / sine);
            this.m_ProjectionMatrix[0] = cotangent / aspect;
            this.m_ProjectionMatrix[4] = 0.0f;
            this.m_ProjectionMatrix[8] = 0.0f;
            this.m_ProjectionMatrix[12] = 0.0f;
            this.m_ProjectionMatrix[1] = 0.0f;
            this.m_ProjectionMatrix[5] = cotangent;
            this.m_ProjectionMatrix[9] = 0.0f;
            this.m_ProjectionMatrix[13] = 0.0f;
            this.m_ProjectionMatrix[2] = 0.0f;
            this.m_ProjectionMatrix[6] = 0.0f;
            this.m_ProjectionMatrix[10] = (-(zFar + zNear)) / deltaZ;
            this.m_ProjectionMatrix[14] = ((-2.0f * zNear) * zFar) / deltaZ;
            this.m_ProjectionMatrix[3] = 0.0f;
            this.m_ProjectionMatrix[7] = 0.0f;
            this.m_ProjectionMatrix[11] = -1.0f;
            this.m_ProjectionMatrix[15] = 0.0f;
            gl.glMultMatrixf(this.m_ProjectionMatrix, 0);
        }
    }

    public void setupViewMatrixViaLookAt(GL10 gl, float eyex, float eyey, float eyez, float centerx, float centery, float centerz, float upx, float upy, float upz) {
        float[] fArr = new float[3];
        float[] forward = {centerx - eyex, centery - eyey, centerz - eyez};
        normalize(forward);
        float[] side = crossProduct(forward, new float[]{upx, upy, upz});
        normalize(side);
        this.m_CameraCurrentSide.x = side[0];
        this.m_CameraCurrentSide.y = side[1];
        this.m_CameraCurrentSide.z = side[2];
        float[] up = crossProduct(side, forward);
        this.m_ViewMatrix[0] = side[0];
        this.m_ViewMatrix[4] = side[1];
        this.m_ViewMatrix[8] = side[2];
        this.m_ViewMatrix[12] = 0.0f;
        this.m_ViewMatrix[1] = up[0];
        this.m_ViewMatrix[5] = up[1];
        this.m_ViewMatrix[9] = up[2];
        this.m_ViewMatrix[13] = 0.0f;
        this.m_ViewMatrix[2] = -forward[0];
        this.m_ViewMatrix[6] = -forward[1];
        this.m_ViewMatrix[10] = -forward[2];
        this.m_ViewMatrix[14] = 0.0f;
        this.m_ViewMatrix[3] = 0.0f;
        this.m_ViewMatrix[7] = 0.0f;
        this.m_ViewMatrix[11] = 0.0f;
        this.m_ViewMatrix[15] = 1.0f;
        gl.glMultMatrixf(this.m_ViewMatrix, 0);
        gl.glTranslatef(-eyex, -eyey, -eyez);
        this.m_Eye.x = eyex;
        this.m_Eye.y = eyey;
        this.m_Eye.z = eyez;
    }

    public PickRay getPickingRay(float screen_x_percentage, float screen_y_percentage) throws Exception {
        PickRay ray = new PickRay();
        float[] in = {screen_x_percentage, screen_y_percentage, 1.0f, 1.0f};
        in[0] = (in[0] * 2.0f) - 1.0f;
        in[1] = (in[1] * 2.0f) - 1.0f;
        in[2] = (in[2] * 2.0f) - 1.0f;
        float[] inverseMatrix = invertMatrix(multMatrices(this.m_ViewMatrix, this.m_ProjectionMatrix));
        float[] out = new float[4];
        for (int i = 0; i < 4; i++) {
            out[i] = (in[0] * inverseMatrix[i + 0]) + (in[1] * inverseMatrix[i + 4]) + (in[2] * inverseMatrix[i + 8]) + (in[3] * inverseMatrix[i + 12]);
        }
        if (((double) out[3]) == 0.0d) {
            throw new Exception("w is zero. No correct PickRay can be calculated.");
        }
        out[0] = out[0] / out[3];
        out[1] = out[1] / out[3];
        out[2] = out[2] / out[3];
        ray.direction.x = out[0];
        ray.direction.y = out[1];
        ray.direction.z = out[2];
        ray.origin = this.m_Eye;
        return ray;
    }

    public float isSphereVisible_OnCurrentFrustum_GetDistance(float x, float y, float z, float size_radius) {
        float x2 = x - this.m_Eye.x;
        float y2 = y - this.m_Eye.y;
        float z2 = z - this.m_Eye.z;
        float dist = 0.0f;
        for (int p = 0; p < 6; p++) {
            dist = (this.m_FrustumPlanes[p][0] * x2) + (this.m_FrustumPlanes[p][1] * y2) + (this.m_FrustumPlanes[p][2] * z2) + this.m_FrustumPlanes[p][3];
            if (dist < (-size_radius)) {
                return 0.0f;
            }
        }
        return dist;
    }

    public boolean isBoundingBoxVisible_OnCurrentFrustum(Vector3D boxLowerEdge, Vector3D boxHigherEdge, Vector3D worldSpaceOffset) {
        float lowx = boxLowerEdge.x + worldSpaceOffset.x;
        float lowy = boxLowerEdge.y + worldSpaceOffset.y;
        float lowz = boxLowerEdge.z + worldSpaceOffset.z;
        float highx = boxHigherEdge.x + worldSpaceOffset.x;
        float highy = boxHigherEdge.y + worldSpaceOffset.y;
        float highz = boxHigherEdge.z + worldSpaceOffset.z;
        for (int p = 0; p < 6; p++) {
            if ((this.m_FrustumPlanes[p][0] * lowx) + (this.m_FrustumPlanes[p][1] * lowy) + (this.m_FrustumPlanes[p][2] * lowz) + this.m_FrustumPlanes[p][3] <= 0.0f && (this.m_FrustumPlanes[p][0] * highx) + (this.m_FrustumPlanes[p][1] * lowy) + (this.m_FrustumPlanes[p][2] * lowz) + this.m_FrustumPlanes[p][3] <= 0.0f && (this.m_FrustumPlanes[p][0] * lowx) + (this.m_FrustumPlanes[p][1] * highy) + (this.m_FrustumPlanes[p][2] * lowz) + this.m_FrustumPlanes[p][3] <= 0.0f && (this.m_FrustumPlanes[p][0] * highx) + (this.m_FrustumPlanes[p][1] * highy) + (this.m_FrustumPlanes[p][2] * lowz) + this.m_FrustumPlanes[p][3] <= 0.0f && (this.m_FrustumPlanes[p][0] * lowx) + (this.m_FrustumPlanes[p][1] * lowy) + (this.m_FrustumPlanes[p][2] * highz) + this.m_FrustumPlanes[p][3] <= 0.0f && (this.m_FrustumPlanes[p][0] * highx) + (this.m_FrustumPlanes[p][1] * lowy) + (this.m_FrustumPlanes[p][2] * highz) + this.m_FrustumPlanes[p][3] <= 0.0f && (this.m_FrustumPlanes[p][0] * lowx) + (this.m_FrustumPlanes[p][1] * highy) + (this.m_FrustumPlanes[p][2] * highz) + this.m_FrustumPlanes[p][3] <= 0.0f && (this.m_FrustumPlanes[p][0] * highx) + (this.m_FrustumPlanes[p][1] * highy) + (this.m_FrustumPlanes[p][2] * highz) + this.m_FrustumPlanes[p][3] <= 0.0f) {
                return false;
            }
        }
        return true;
    }

    public void CalculateCurrentFrustumPlanes() {
        float[] clip = {(this.m_ViewMatrix[0] * this.m_ProjectionMatrix[0]) + (this.m_ViewMatrix[1] * this.m_ProjectionMatrix[4]) + (this.m_ViewMatrix[2] * this.m_ProjectionMatrix[8]) + (this.m_ViewMatrix[3] * this.m_ProjectionMatrix[12]), (this.m_ViewMatrix[0] * this.m_ProjectionMatrix[1]) + (this.m_ViewMatrix[1] * this.m_ProjectionMatrix[5]) + (this.m_ViewMatrix[2] * this.m_ProjectionMatrix[9]) + (this.m_ViewMatrix[3] * this.m_ProjectionMatrix[13]), (this.m_ViewMatrix[0] * this.m_ProjectionMatrix[2]) + (this.m_ViewMatrix[1] * this.m_ProjectionMatrix[6]) + (this.m_ViewMatrix[2] * this.m_ProjectionMatrix[10]) + (this.m_ViewMatrix[3] * this.m_ProjectionMatrix[14]), (this.m_ViewMatrix[0] * this.m_ProjectionMatrix[3]) + (this.m_ViewMatrix[1] * this.m_ProjectionMatrix[7]) + (this.m_ViewMatrix[2] * this.m_ProjectionMatrix[11]) + (this.m_ViewMatrix[3] * this.m_ProjectionMatrix[15]), (this.m_ViewMatrix[4] * this.m_ProjectionMatrix[0]) + (this.m_ViewMatrix[5] * this.m_ProjectionMatrix[4]) + (this.m_ViewMatrix[6] * this.m_ProjectionMatrix[8]) + (this.m_ViewMatrix[7] * this.m_ProjectionMatrix[12]), (this.m_ViewMatrix[4] * this.m_ProjectionMatrix[1]) + (this.m_ViewMatrix[5] * this.m_ProjectionMatrix[5]) + (this.m_ViewMatrix[6] * this.m_ProjectionMatrix[9]) + (this.m_ViewMatrix[7] * this.m_ProjectionMatrix[13]), (this.m_ViewMatrix[4] * this.m_ProjectionMatrix[2]) + (this.m_ViewMatrix[5] * this.m_ProjectionMatrix[6]) + (this.m_ViewMatrix[6] * this.m_ProjectionMatrix[10]) + (this.m_ViewMatrix[7] * this.m_ProjectionMatrix[14]), (this.m_ViewMatrix[4] * this.m_ProjectionMatrix[3]) + (this.m_ViewMatrix[5] * this.m_ProjectionMatrix[7]) + (this.m_ViewMatrix[6] * this.m_ProjectionMatrix[11]) + (this.m_ViewMatrix[7] * this.m_ProjectionMatrix[15]), (this.m_ViewMatrix[8] * this.m_ProjectionMatrix[0]) + (this.m_ViewMatrix[9] * this.m_ProjectionMatrix[4]) + (this.m_ViewMatrix[10] * this.m_ProjectionMatrix[8]) + (this.m_ViewMatrix[11] * this.m_ProjectionMatrix[12]), (this.m_ViewMatrix[8] * this.m_ProjectionMatrix[1]) + (this.m_ViewMatrix[9] * this.m_ProjectionMatrix[5]) + (this.m_ViewMatrix[10] * this.m_ProjectionMatrix[9]) + (this.m_ViewMatrix[11] * this.m_ProjectionMatrix[13]), (this.m_ViewMatrix[8] * this.m_ProjectionMatrix[2]) + (this.m_ViewMatrix[9] * this.m_ProjectionMatrix[6]) + (this.m_ViewMatrix[10] * this.m_ProjectionMatrix[10]) + (this.m_ViewMatrix[11] * this.m_ProjectionMatrix[14]), (this.m_ViewMatrix[8] * this.m_ProjectionMatrix[3]) + (this.m_ViewMatrix[9] * this.m_ProjectionMatrix[7]) + (this.m_ViewMatrix[10] * this.m_ProjectionMatrix[11]) + (this.m_ViewMatrix[11] * this.m_ProjectionMatrix[15]), (this.m_ViewMatrix[12] * this.m_ProjectionMatrix[0]) + (this.m_ViewMatrix[13] * this.m_ProjectionMatrix[4]) + (this.m_ViewMatrix[14] * this.m_ProjectionMatrix[8]) + (this.m_ViewMatrix[15] * this.m_ProjectionMatrix[12]), (this.m_ViewMatrix[12] * this.m_ProjectionMatrix[1]) + (this.m_ViewMatrix[13] * this.m_ProjectionMatrix[5]) + (this.m_ViewMatrix[14] * this.m_ProjectionMatrix[9]) + (this.m_ViewMatrix[15] * this.m_ProjectionMatrix[13]), (this.m_ViewMatrix[12] * this.m_ProjectionMatrix[2]) + (this.m_ViewMatrix[13] * this.m_ProjectionMatrix[6]) + (this.m_ViewMatrix[14] * this.m_ProjectionMatrix[10]) + (this.m_ViewMatrix[15] * this.m_ProjectionMatrix[14]), (this.m_ViewMatrix[12] * this.m_ProjectionMatrix[3]) + (this.m_ViewMatrix[13] * this.m_ProjectionMatrix[7]) + (this.m_ViewMatrix[14] * this.m_ProjectionMatrix[11]) + (this.m_ViewMatrix[15] * this.m_ProjectionMatrix[15])};
        this.m_FrustumPlanes[0][0] = clip[3] - clip[0];
        this.m_FrustumPlanes[0][1] = clip[7] - clip[4];
        this.m_FrustumPlanes[0][2] = clip[11] - clip[8];
        this.m_FrustumPlanes[0][3] = clip[15] - clip[12];
        this.m_FrustumPlanes[1][0] = clip[3] + clip[0];
        this.m_FrustumPlanes[1][1] = clip[7] + clip[4];
        this.m_FrustumPlanes[1][2] = clip[11] + clip[8];
        this.m_FrustumPlanes[1][3] = clip[15] + clip[12];
        this.m_FrustumPlanes[2][0] = clip[3] + clip[1];
        this.m_FrustumPlanes[2][1] = clip[7] + clip[5];
        this.m_FrustumPlanes[2][2] = clip[11] + clip[9];
        this.m_FrustumPlanes[2][3] = clip[15] + clip[13];
        this.m_FrustumPlanes[3][0] = clip[3] - clip[1];
        this.m_FrustumPlanes[3][1] = clip[7] - clip[5];
        this.m_FrustumPlanes[3][2] = clip[11] - clip[9];
        this.m_FrustumPlanes[3][3] = clip[15] - clip[13];
        this.m_FrustumPlanes[4][0] = clip[3] - clip[2];
        this.m_FrustumPlanes[4][1] = clip[7] - clip[6];
        this.m_FrustumPlanes[4][2] = clip[11] - clip[10];
        this.m_FrustumPlanes[4][3] = clip[15] - clip[14];
        this.m_FrustumPlanes[5][0] = clip[3] + clip[2];
        this.m_FrustumPlanes[5][1] = clip[7] + clip[6];
        this.m_FrustumPlanes[5][2] = clip[11] + clip[10];
        this.m_FrustumPlanes[5][3] = clip[15] + clip[14];
        for (int i = 0; i < 6; i++) {
            float t = (float) Math.sqrt((double) ((this.m_FrustumPlanes[i][0] * this.m_FrustumPlanes[i][0]) + (this.m_FrustumPlanes[i][1] * this.m_FrustumPlanes[i][1]) + (this.m_FrustumPlanes[i][2] * this.m_FrustumPlanes[i][2])));
            float[] fArr = this.m_FrustumPlanes[i];
            fArr[0] = fArr[0] / t;
            float[] fArr2 = this.m_FrustumPlanes[i];
            fArr2[1] = fArr2[1] / t;
            float[] fArr3 = this.m_FrustumPlanes[i];
            fArr3[2] = fArr3[2] / t;
            float[] fArr4 = this.m_FrustumPlanes[i];
            fArr4[3] = fArr4[3] / t;
        }
    }

    public boolean targetReached() {
        return Math.abs(this.m_CameraPos.getDistance(this.m_CameraPosTarget)) <= 0.01f;
    }

    public void setNextTarget(Vector3D next) {
        this.m_CameraPosTarget.x = next.x;
        this.m_CameraPosTarget.y = next.y;
        this.m_CameraPosTarget.z = next.z;
    }

    private float[] normalize(float[] v) {
        double r = Math.sqrt((double) ((v[0] * v[0]) + (v[1] * v[1]) + (v[2] * v[2])));
        if (r != 0.0d) {
            v[0] = (float) (((double) v[0]) / r);
            v[1] = (float) (((double) v[1]) / r);
            v[2] = (float) (((double) v[2]) / r);
        }
        return v;
    }

    private float[] crossProduct(float[] v1, float[] v2) {
        return new float[]{(v1[1] * v2[2]) - (v1[2] * v2[1]), (v1[2] * v2[0]) - (v1[0] * v2[2]), (v1[0] * v2[1]) - (v1[1] * v2[0])};
    }

    /* access modifiers changed from: package-private */
    public float[] multMatrices(float[] a, float[] b) {
        float[] result = new float[16];
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                result[(i * 4) + j] = (a[(i * 4) + 0] * b[j + 0]) + (a[(i * 4) + 1] * b[j + 4]) + (a[(i * 4) + 2] * b[j + 8]) + (a[(i * 4) + 3] * b[j + 12]);
            }
        }
        return result;
    }

    /* access modifiers changed from: package-private */
    public float[] invertMatrix(float[] m) throws Exception {
        float[] inv = new float[16];
        inv[0] = ((((((m[5] * m[10]) * m[15]) - ((m[5] * m[11]) * m[14])) - ((m[9] * m[6]) * m[15])) + ((m[9] * m[7]) * m[14])) + ((m[13] * m[6]) * m[11])) - ((m[13] * m[7]) * m[10]);
        inv[4] = (((((((-m[4]) * m[10]) * m[15]) + ((m[4] * m[11]) * m[14])) + ((m[8] * m[6]) * m[15])) - ((m[8] * m[7]) * m[14])) - ((m[12] * m[6]) * m[11])) + (m[12] * m[7] * m[10]);
        inv[8] = ((((((m[4] * m[9]) * m[15]) - ((m[4] * m[11]) * m[13])) - ((m[8] * m[5]) * m[15])) + ((m[8] * m[7]) * m[13])) + ((m[12] * m[5]) * m[11])) - ((m[12] * m[7]) * m[9]);
        inv[12] = (((((((-m[4]) * m[9]) * m[14]) + ((m[4] * m[10]) * m[13])) + ((m[8] * m[5]) * m[14])) - ((m[8] * m[6]) * m[13])) - ((m[12] * m[5]) * m[10])) + (m[12] * m[6] * m[9]);
        inv[1] = (((((((-m[1]) * m[10]) * m[15]) + ((m[1] * m[11]) * m[14])) + ((m[9] * m[2]) * m[15])) - ((m[9] * m[3]) * m[14])) - ((m[13] * m[2]) * m[11])) + (m[13] * m[3] * m[10]);
        inv[5] = ((((((m[0] * m[10]) * m[15]) - ((m[0] * m[11]) * m[14])) - ((m[8] * m[2]) * m[15])) + ((m[8] * m[3]) * m[14])) + ((m[12] * m[2]) * m[11])) - ((m[12] * m[3]) * m[10]);
        inv[9] = (((((((-m[0]) * m[9]) * m[15]) + ((m[0] * m[11]) * m[13])) + ((m[8] * m[1]) * m[15])) - ((m[8] * m[3]) * m[13])) - ((m[12] * m[1]) * m[11])) + (m[12] * m[3] * m[9]);
        inv[13] = ((((((m[0] * m[9]) * m[14]) - ((m[0] * m[10]) * m[13])) - ((m[8] * m[1]) * m[14])) + ((m[8] * m[2]) * m[13])) + ((m[12] * m[1]) * m[10])) - ((m[12] * m[2]) * m[9]);
        inv[2] = ((((((m[1] * m[6]) * m[15]) - ((m[1] * m[7]) * m[14])) - ((m[5] * m[2]) * m[15])) + ((m[5] * m[3]) * m[14])) + ((m[13] * m[2]) * m[7])) - ((m[13] * m[3]) * m[6]);
        inv[6] = (((((((-m[0]) * m[6]) * m[15]) + ((m[0] * m[7]) * m[14])) + ((m[4] * m[2]) * m[15])) - ((m[4] * m[3]) * m[14])) - ((m[12] * m[2]) * m[7])) + (m[12] * m[3] * m[6]);
        inv[10] = ((((((m[0] * m[5]) * m[15]) - ((m[0] * m[7]) * m[13])) - ((m[4] * m[1]) * m[15])) + ((m[4] * m[3]) * m[13])) + ((m[12] * m[1]) * m[7])) - ((m[12] * m[3]) * m[5]);
        inv[14] = (((((((-m[0]) * m[5]) * m[14]) + ((m[0] * m[6]) * m[13])) + ((m[4] * m[1]) * m[14])) - ((m[4] * m[2]) * m[13])) - ((m[12] * m[1]) * m[6])) + (m[12] * m[2] * m[5]);
        inv[3] = (((((((-m[1]) * m[6]) * m[11]) + ((m[1] * m[7]) * m[10])) + ((m[5] * m[2]) * m[11])) - ((m[5] * m[3]) * m[10])) - ((m[9] * m[2]) * m[7])) + (m[9] * m[3] * m[6]);
        inv[7] = ((((((m[0] * m[6]) * m[11]) - ((m[0] * m[7]) * m[10])) - ((m[4] * m[2]) * m[11])) + ((m[4] * m[3]) * m[10])) + ((m[8] * m[2]) * m[7])) - ((m[8] * m[3]) * m[6]);
        inv[11] = (((((((-m[0]) * m[5]) * m[11]) + ((m[0] * m[7]) * m[9])) + ((m[4] * m[1]) * m[11])) - ((m[4] * m[3]) * m[9])) - ((m[8] * m[1]) * m[7])) + (m[8] * m[3] * m[5]);
        inv[15] = ((((((m[0] * m[5]) * m[10]) - ((m[0] * m[6]) * m[9])) - ((m[4] * m[1]) * m[10])) + ((m[4] * m[2]) * m[9])) + ((m[8] * m[1]) * m[6])) - ((m[8] * m[2]) * m[5]);
        float det = (m[0] * inv[0]) + (m[1] * inv[4]) + (m[2] * inv[8]) + (m[3] * inv[12]);
        if (det == 0.0f) {
            throw new Exception("determinat is zero, can't invert matrix!");
        }
        float det2 = 1.0f / det;
        for (int i = 0; i < 16; i++) {
            m[i] = inv[i] * det2;
        }
        return m;
    }
}
