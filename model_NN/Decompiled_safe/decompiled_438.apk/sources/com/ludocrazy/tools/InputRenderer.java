package com.ludocrazy.tools;

import java.util.Vector;
import javax.microedition.khronos.opengles.GL10;

public class InputRenderer {
    private float EVENT_VIS_DURATION = 0.2f;
    private float EVENT_VIS_RADIUS = 20.0f;
    private final int EVENT_VIS_TRI_COUNT = 16;
    private Vector<SimpleEvent> m_TouchEvents = new Vector<>();

    private class SimpleEvent {
        public float gametime_seconds;
        public float x_pos;
        public float y_pos;

        private SimpleEvent() {
        }

        /* synthetic */ SimpleEvent(InputRenderer inputRenderer, SimpleEvent simpleEvent) {
            this();
        }
    }

    public InputRenderer(float radius, float duration) {
        this.EVENT_VIS_RADIUS = radius;
        this.EVENT_VIS_DURATION = duration;
    }

    public void addNewEvent(float gametime_seconds, float x_pos, float y_pos, int event_action) {
        if (event_action == 1) {
            SimpleEvent newEvent = new SimpleEvent(this, null);
            newEvent.gametime_seconds = gametime_seconds;
            newEvent.x_pos = x_pos;
            newEvent.y_pos = y_pos;
            this.m_TouchEvents.add(newEvent);
        }
    }

    public void draw(GL10 gl, float gametime_seconds, BasicMesh dynamic_gui_mesh) throws Exception {
        int i = 0;
        while (i < this.m_TouchEvents.size()) {
            SimpleEvent event = this.m_TouchEvents.elementAt(i);
            float duration = gametime_seconds - event.gametime_seconds;
            if (duration < this.EVENT_VIS_DURATION) {
                float duration_percentage = duration / this.EVENT_VIS_DURATION;
                dynamic_gui_mesh.addTriangledCircle_colored(16, new Coords(event.x_pos, event.y_pos, 0.0f), this.EVENT_VIS_RADIUS * (1.0f - duration_percentage), 0.0f, 360.0f, new ColorFloats(1.0f, 1.0f, 1.0f, duration_percentage), new ColorFloats(0.75f, 0.75f, 0.75f, 0.75f * duration_percentage));
            } else {
                this.m_TouchEvents.remove(i);
                i--;
            }
            i++;
        }
    }
}
