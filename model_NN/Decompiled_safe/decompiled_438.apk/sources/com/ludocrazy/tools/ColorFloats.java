package com.ludocrazy.tools;

public class ColorFloats {
    public float alpha;
    public float blue;
    public float green;
    public float red;

    public ColorFloats() {
        this.red = 1.0f;
        this.green = 1.0f;
        this.blue = 1.0f;
        this.alpha = 1.0f;
        this.red = 1.0f;
        this.green = 1.0f;
        this.blue = 1.0f;
        this.alpha = 1.0f;
    }

    public ColorFloats(ColorFloats to_copy) {
        this.red = 1.0f;
        this.green = 1.0f;
        this.blue = 1.0f;
        this.alpha = 1.0f;
        this.red = to_copy.red;
        this.green = to_copy.green;
        this.blue = to_copy.blue;
        this.alpha = to_copy.alpha;
    }

    public ColorFloats(float _red, float _green, float _blue) {
        this.red = 1.0f;
        this.green = 1.0f;
        this.blue = 1.0f;
        this.alpha = 1.0f;
        this.red = min0max1(_red);
        this.green = min0max1(_green);
        this.blue = min0max1(_blue);
        this.alpha = 1.0f;
    }

    public ColorFloats(float _red, float _green, float _blue, float _alpha) {
        this.red = 1.0f;
        this.green = 1.0f;
        this.blue = 1.0f;
        this.alpha = 1.0f;
        this.red = min0max1(_red);
        this.green = min0max1(_green);
        this.blue = min0max1(_blue);
        this.alpha = min0max1(_alpha);
    }

    public void blendResult(ColorFloats source, ColorFloats dest, float blend_to_dest) {
        this.red = source.red;
        this.green = source.green;
        this.blue = source.blue;
        this.alpha = source.alpha;
        blendSelfTowards(dest, blend_to_dest);
    }

    public void blendSelfTowards(ColorFloats partner, float blend_to_partner) {
        float blend_to_partner2 = min0max1(blend_to_partner);
        float anti_blend = 1.0f - blend_to_partner2;
        this.red = (this.red * anti_blend) + (partner.red * blend_to_partner2);
        this.green = (this.green * anti_blend) + (partner.green * blend_to_partner2);
        this.blue = (this.blue * anti_blend) + (partner.blue * blend_to_partner2);
        this.alpha = (this.alpha * anti_blend) + (partner.alpha * blend_to_partner2);
    }

    public float min0max1(float input) {
        if (input > 1.0f) {
            return 1.0f;
        }
        if (input < 0.0f) {
            return 0.0f;
        }
        return input;
    }

    public void multiply_rgb(float factor) {
        this.red *= factor;
        this.green *= factor;
        this.blue *= factor;
    }

    public boolean identical_color_parts(ColorFloats compare_partner) {
        return this.red == compare_partner.red && this.green == compare_partner.green && this.blue == compare_partner.blue && this.alpha == compare_partner.alpha;
    }
}
