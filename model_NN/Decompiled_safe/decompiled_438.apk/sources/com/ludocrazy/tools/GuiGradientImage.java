package com.ludocrazy.tools;

public class GuiGradientImage extends GuiMesh {
    GuiGradientImage(LogOutput DEBUGLOG_to_use, PhoneAbilities phoneAbilities, float texture_square_size_pixels, float screenX, float screenY, float screenWidth, float screenHeight, float screenZ, int pixel_u_start, float pixel_v_start, float pixel_u_width, float pixel_v_height, ColorFloats color_including_alpha, int repeat_x) {
        super(DEBUGLOG_to_use, phoneAbilities);
        try {
            setupArraysQuadFaces(repeat_x);
            float repeat_width = screenWidth / ((float) repeat_x);
            float xend = screenX + repeat_width;
            float yend = screenY + screenHeight;
            float TEXTUREWIDTH = texture_square_size_pixels;
            float TEXTUREHEIGHT = texture_square_size_pixels;
            float uLeft = ((float) pixel_u_start) / TEXTUREWIDTH;
            float vTop = pixel_v_start / TEXTUREHEIGHT;
            float uRight = uLeft + (pixel_u_width / TEXTUREWIDTH);
            float vBottom = vTop + (pixel_v_height / TEXTUREHEIGHT);
            for (int i = 0; i < repeat_x; i++) {
                addQuad(screenX, screenY, screenZ, xend, yend, uLeft, vBottom, uRight, vTop);
                addColorsForLastQuad(color_including_alpha, color_including_alpha, color_including_alpha, color_including_alpha);
                screenX += repeat_width;
                xend += repeat_width;
            }
            convertArraysToBuffer();
        } catch (Exception e) {
            new ErrorOutput("GuiImage::GuiImage()", "Exception", e);
        }
    }
}
