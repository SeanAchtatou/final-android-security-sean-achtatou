package com.ludocrazy.tools;

public class Vector3D {
    public float x;
    public float y;
    public float z;

    public Vector3D() {
        this.x = 0.0f;
        this.y = 0.0f;
        this.z = 0.0f;
        this.x = 0.0f;
        this.y = 0.0f;
        this.z = 0.0f;
    }

    public Vector3D(float _x, float _y, float _z) {
        this.x = 0.0f;
        this.y = 0.0f;
        this.z = 0.0f;
        this.x = _x;
        this.y = _y;
        this.z = _z;
    }

    public Vector3D(Vector3D to_be_copied) {
        this.x = 0.0f;
        this.y = 0.0f;
        this.z = 0.0f;
        copyFrom(to_be_copied);
    }

    public void copyFrom(Vector3D to_be_copied) {
        this.x = to_be_copied.x;
        this.y = to_be_copied.y;
        this.z = to_be_copied.z;
    }

    public float getDistanceSquared(Vector3D target) {
        float dist_x = this.x - target.x;
        float dist_y = this.y - target.y;
        float dist_z = this.z - target.z;
        return (dist_x * dist_x) + (dist_y * dist_y) + (dist_z * dist_z);
    }

    public float getDistance(Vector3D target) {
        return (float) Math.sqrt((double) getDistanceSquared(target));
    }

    public Vector3D calculate_offset(Vector3D offset) {
        return new Vector3D(offset.x + this.x, offset.y + this.y, offset.z + this.z);
    }

    public void add(Vector3D offset) {
        this.x += offset.x;
        this.y += offset.y;
        this.z += offset.z;
    }

    public void substract(Vector3D offset) {
        this.x -= offset.x;
        this.y -= offset.y;
        this.z -= offset.z;
    }

    public void addScaled(Vector3D offset, float offset_scale) {
        this.x += offset.x * offset_scale;
        this.y += offset.y * offset_scale;
        this.z += offset.z * offset_scale;
    }

    public void scale(float scale) {
        this.x *= scale;
        this.y *= scale;
        this.z *= scale;
    }

    public void normalize() {
        scale(1.0f / length());
    }

    public float length() {
        return (float) Math.sqrt((double) ((this.x * this.x) + (this.y * this.y) + (this.z * this.z)));
    }

    public float squaredLength() {
        return (this.x * this.x) + (this.y * this.y) + (this.z * this.z);
    }

    public float dotProduct(Vector3D partner) {
        return (this.x * partner.x) + (this.y * partner.y) + (this.z * partner.z);
    }

    public void interpolate_linear(Vector3D target, float blend_factor) {
        float inv = 1.0f - blend_factor;
        this.x = (this.x * inv) + (target.x * blend_factor);
        this.y = (this.y * inv) + (target.y * blend_factor);
        this.z = (this.z * inv) + (target.z * blend_factor);
    }

    public void interpolate_catmullrom(Vector3D prePrevious, Vector3D previous, Vector3D target, float blend_factor) {
        float prePreviousBasis = ((((((-blend_factor) + 3.0f) * blend_factor) - 3.0f) * blend_factor) + 1.0f) / 6.0f;
        float previousBasis = (((((3.0f * blend_factor) - 6.0f) * blend_factor) * blend_factor) + 4.0f) / 6.0f;
        float thisBasis = ((((((-3.0f * blend_factor) + 3.0f) * blend_factor) + 3.0f) * blend_factor) + 1.0f) / 6.0f;
        float targetBasis = ((blend_factor * blend_factor) * blend_factor) / 6.0f;
        float resultx = prePreviousBasis * prePrevious.x;
        float resulty = prePreviousBasis * prePrevious.y;
        float resultz = prePreviousBasis * prePrevious.z;
        float resultx2 = resultx + (previous.x * previousBasis);
        float resulty2 = resulty + (previous.y * previousBasis);
        float resultz2 = resultz + (previous.z * previousBasis);
        float resultx3 = resultx2 + (this.x * thisBasis);
        float resulty3 = resulty2 + (this.y * thisBasis);
        float resultz3 = resultz2 + (this.z * thisBasis);
        float resultx4 = resultx3 + (target.x * targetBasis);
        float resulty4 = resulty3 + (target.y * targetBasis);
        this.x = resultx4;
        this.y = resulty4;
        this.z = resultz3 + (target.z * targetBasis);
    }
}
