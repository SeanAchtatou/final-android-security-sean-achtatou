package com.ludocrazy.tools;

public class GuiMesh extends BasicMesh {
    public GuiMesh(LogOutput DEBUGLOG_to_use, PhoneAbilities phoneAbilities) {
        super(DEBUGLOG_to_use, phoneAbilities);
    }

    /* access modifiers changed from: package-private */
    public int getQuadCount() {
        return this.m_vertices_count / 4;
    }

    public void addTriangleFace_textured(Coords pos1, UvCoords uv1, Coords pos2, UvCoords uv2, Coords pos3, UvCoords uv3, byte gray_shade, byte alpha) throws Exception {
        addTriangleFace_textured(pos1.x, pos1.y, pos1.z, uv1.u, uv1.v, pos2.x, pos2.y, pos2.z, uv2.u, uv2.v, pos3.x, pos3.y, pos3.z, uv3.u, uv3.v, gray_shade, gray_shade, gray_shade, alpha);
    }

    public void addTriangleFace_textured(float pos1_x, float pos1_y, float pos1_z, float uv1_u, float uv1_v, float pos2_x, float pos2_y, float pos2_z, float uv2_u, float uv2_v, float pos3_x, float pos3_y, float pos3_z, float uv3_u, float uv3_v, byte red, byte green, byte blue, byte alpha) throws Exception {
        if (this.m_currentVertex <= this.m_vertices_count - 3) {
            int firstVertex = this.m_currentVertex;
            this.vertices[(this.m_currentVertex * 3) + 0] = pos1_x;
            this.vertices[(this.m_currentVertex * 3) + 1] = pos1_y;
            this.vertices[(this.m_currentVertex * 3) + 2] = pos1_z;
            this.m_currentVertex++;
            this.vertices[(this.m_currentVertex * 3) + 0] = pos2_x;
            this.vertices[(this.m_currentVertex * 3) + 1] = pos2_y;
            this.vertices[(this.m_currentVertex * 3) + 2] = pos2_z;
            this.m_currentVertex++;
            this.vertices[(this.m_currentVertex * 3) + 0] = pos3_x;
            this.vertices[(this.m_currentVertex * 3) + 1] = pos3_y;
            this.vertices[(this.m_currentVertex * 3) + 2] = pos3_z;
            this.m_currentVertex++;
            this.m_UvBufferUsed = true;
            this.m_currentVertex -= 3;
            this.uvCoords[(this.m_currentVertex * 2) + 0] = uv1_u;
            this.uvCoords[(this.m_currentVertex * 2) + 1] = uv1_v;
            this.m_currentVertex++;
            this.uvCoords[(this.m_currentVertex * 2) + 0] = uv2_u;
            this.uvCoords[(this.m_currentVertex * 2) + 1] = uv2_v;
            this.m_currentVertex++;
            this.uvCoords[(this.m_currentVertex * 2) + 0] = uv3_u;
            this.uvCoords[(this.m_currentVertex * 2) + 1] = uv3_v;
            this.m_currentVertex++;
            this.m_ColorBufferUsed = true;
            this.m_currentVertex -= 3;
            this.colors[(this.m_currentVertex * 4) + 0] = red;
            this.colors[(this.m_currentVertex * 4) + 1] = green;
            this.colors[(this.m_currentVertex * 4) + 2] = blue;
            this.colors[(this.m_currentVertex * 4) + 3] = alpha;
            this.m_currentVertex++;
            this.colors[(this.m_currentVertex * 4) + 0] = red;
            this.colors[(this.m_currentVertex * 4) + 1] = green;
            this.colors[(this.m_currentVertex * 4) + 2] = blue;
            this.colors[(this.m_currentVertex * 4) + 3] = alpha;
            this.m_currentVertex++;
            this.colors[(this.m_currentVertex * 4) + 0] = red;
            this.colors[(this.m_currentVertex * 4) + 1] = green;
            this.colors[(this.m_currentVertex * 4) + 2] = blue;
            this.colors[(this.m_currentVertex * 4) + 3] = alpha;
            this.m_currentVertex++;
            this.indices[this.m_currentIndex] = (short) (firstVertex + 0);
            this.m_currentIndex++;
            this.indices[this.m_currentIndex] = (short) (firstVertex + 1);
            this.m_currentIndex++;
            this.indices[this.m_currentIndex] = (short) (firstVertex + 2);
            this.m_currentIndex++;
            return;
        }
        throw new Exception("GuiMesh: Can't call addTriangleFace_textured(), not enough Triangles reserved by SetupArray(): " + (this.m_vertices_count / 3));
    }

    public void addTriangleFace_textured(Coords pos1, UvCoords uv1, Coords pos2, UvCoords uv2, Coords pos3, UvCoords uv3) throws Exception {
        if (this.m_currentVertex <= this.m_vertices_count - 3) {
            int firstVertex = this.m_currentVertex;
            this.vertices[(this.m_currentVertex * 3) + 0] = pos1.x;
            this.vertices[(this.m_currentVertex * 3) + 1] = pos1.y;
            this.vertices[(this.m_currentVertex * 3) + 2] = pos1.z;
            this.m_currentVertex++;
            this.vertices[(this.m_currentVertex * 3) + 0] = pos2.x;
            this.vertices[(this.m_currentVertex * 3) + 1] = pos2.y;
            this.vertices[(this.m_currentVertex * 3) + 2] = pos2.z;
            this.m_currentVertex++;
            this.vertices[(this.m_currentVertex * 3) + 0] = pos3.x;
            this.vertices[(this.m_currentVertex * 3) + 1] = pos3.y;
            this.vertices[(this.m_currentVertex * 3) + 2] = pos3.z;
            this.m_currentVertex++;
            this.m_UvBufferUsed = true;
            this.m_currentVertex -= 3;
            this.uvCoords[(this.m_currentVertex * 2) + 0] = uv1.u;
            this.uvCoords[(this.m_currentVertex * 2) + 1] = uv1.v;
            this.m_currentVertex++;
            this.uvCoords[(this.m_currentVertex * 2) + 0] = uv2.u;
            this.uvCoords[(this.m_currentVertex * 2) + 1] = uv2.v;
            this.m_currentVertex++;
            this.uvCoords[(this.m_currentVertex * 2) + 0] = uv3.u;
            this.uvCoords[(this.m_currentVertex * 2) + 1] = uv3.v;
            this.m_currentVertex++;
            this.m_ColorBufferUsed = true;
            this.m_currentVertex -= 3;
            this.colors[(this.m_currentVertex * 4) + 0] = -1;
            this.colors[(this.m_currentVertex * 4) + 1] = -1;
            this.colors[(this.m_currentVertex * 4) + 2] = -1;
            this.colors[(this.m_currentVertex * 4) + 3] = -1;
            this.m_currentVertex++;
            this.colors[(this.m_currentVertex * 4) + 0] = -1;
            this.colors[(this.m_currentVertex * 4) + 1] = -1;
            this.colors[(this.m_currentVertex * 4) + 2] = -1;
            this.colors[(this.m_currentVertex * 4) + 3] = -1;
            this.m_currentVertex++;
            this.colors[(this.m_currentVertex * 4) + 0] = -1;
            this.colors[(this.m_currentVertex * 4) + 1] = -1;
            this.colors[(this.m_currentVertex * 4) + 2] = -1;
            this.colors[(this.m_currentVertex * 4) + 3] = -1;
            this.m_currentVertex++;
            this.indices[this.m_currentIndex] = (short) (firstVertex + 0);
            this.m_currentIndex++;
            this.indices[this.m_currentIndex] = (short) (firstVertex + 1);
            this.m_currentIndex++;
            this.indices[this.m_currentIndex] = (short) (firstVertex + 2);
            this.m_currentIndex++;
            return;
        }
        throw new Exception("GuiMesh: Can't call addTriangleFace_textured(), not enough Triangles reserved by SetupArray(): " + (this.m_vertices_count / 3));
    }

    public void addQuad_plain(float leftX, float topY, float Z, float rightX, float bottomY) throws Exception {
        if (this.m_currentVertex <= this.m_vertices_count - 4) {
            int firstVertex = this.m_currentVertex;
            this.vertices[(this.m_currentVertex * 3) + 0] = leftX;
            this.vertices[(this.m_currentVertex * 3) + 1] = topY;
            this.vertices[(this.m_currentVertex * 3) + 2] = Z;
            this.m_currentVertex++;
            this.vertices[(this.m_currentVertex * 3) + 0] = rightX;
            this.vertices[(this.m_currentVertex * 3) + 1] = topY;
            this.vertices[(this.m_currentVertex * 3) + 2] = Z;
            this.m_currentVertex++;
            this.vertices[(this.m_currentVertex * 3) + 0] = leftX;
            this.vertices[(this.m_currentVertex * 3) + 1] = bottomY;
            this.vertices[(this.m_currentVertex * 3) + 2] = Z;
            this.m_currentVertex++;
            this.vertices[(this.m_currentVertex * 3) + 0] = rightX;
            this.vertices[(this.m_currentVertex * 3) + 1] = bottomY;
            this.vertices[(this.m_currentVertex * 3) + 2] = Z;
            this.m_currentVertex++;
            this.indices[this.m_currentIndex] = (short) (firstVertex + 0);
            this.m_currentIndex++;
            this.indices[this.m_currentIndex] = (short) (firstVertex + 1);
            this.m_currentIndex++;
            this.indices[this.m_currentIndex] = (short) (firstVertex + 2);
            this.m_currentIndex++;
            this.indices[this.m_currentIndex] = (short) (firstVertex + 2);
            this.m_currentIndex++;
            this.indices[this.m_currentIndex] = (short) (firstVertex + 1);
            this.m_currentIndex++;
            this.indices[this.m_currentIndex] = (short) (firstVertex + 3);
            this.m_currentIndex++;
            return;
        }
        throw new Exception("GuiMesh: Can't call addQuad(), not enough Quads reserved by SetupArray(): " + (this.m_vertices_count / 4));
    }

    public void addDebugLine(float startX, float startY, float Z, float endX, float endY, float uWhitePixel, float vWhitePixel, byte grey_shade, byte alpha, float end_thickness) throws Exception {
        UvCoords uvCoords = new UvCoords(uWhitePixel, vWhitePixel);
        addTriangleFace_textured(new Coords(startX, startY, Z), uvCoords, new Coords(endX, endY, Z), uvCoords, new Coords(endX + end_thickness, endY + end_thickness, Z), uvCoords, grey_shade, alpha);
    }

    public void addQuad(float leftX, float topY, float Z, float rightX, float bottomY, float uLeft, float vTop, float uRight, float vBottom) throws Exception {
        this.m_UvBufferUsed = true;
        if (this.m_PhoneAbilities.getViewSmallerSize() < 480) {
            uLeft -= 2048.0f;
            uRight -= 2048.0f;
            vTop -= 2048.0f;
            vBottom -= 2048.0f;
        }
        if (this.m_currentVertex <= this.m_vertices_count - 4) {
            int firstVertex = this.m_currentVertex;
            this.vertices[(this.m_currentVertex * 3) + 0] = leftX;
            this.vertices[(this.m_currentVertex * 3) + 1] = topY;
            this.vertices[(this.m_currentVertex * 3) + 2] = Z;
            this.uvCoords[(this.m_currentVertex * 2) + 0] = uLeft;
            this.uvCoords[(this.m_currentVertex * 2) + 1] = vTop;
            this.m_currentVertex++;
            this.vertices[(this.m_currentVertex * 3) + 0] = rightX;
            this.vertices[(this.m_currentVertex * 3) + 1] = topY;
            this.vertices[(this.m_currentVertex * 3) + 2] = Z;
            this.uvCoords[(this.m_currentVertex * 2) + 0] = uRight;
            this.uvCoords[(this.m_currentVertex * 2) + 1] = vTop;
            this.m_currentVertex++;
            this.vertices[(this.m_currentVertex * 3) + 0] = leftX;
            this.vertices[(this.m_currentVertex * 3) + 1] = bottomY;
            this.vertices[(this.m_currentVertex * 3) + 2] = Z;
            this.uvCoords[(this.m_currentVertex * 2) + 0] = uLeft;
            this.uvCoords[(this.m_currentVertex * 2) + 1] = vBottom;
            this.m_currentVertex++;
            this.vertices[(this.m_currentVertex * 3) + 0] = rightX;
            this.vertices[(this.m_currentVertex * 3) + 1] = bottomY;
            this.vertices[(this.m_currentVertex * 3) + 2] = Z;
            this.uvCoords[(this.m_currentVertex * 2) + 0] = uRight;
            this.uvCoords[(this.m_currentVertex * 2) + 1] = vBottom;
            this.m_currentVertex++;
            this.indices[this.m_currentIndex] = (short) (firstVertex + 0);
            this.m_currentIndex++;
            this.indices[this.m_currentIndex] = (short) (firstVertex + 1);
            this.m_currentIndex++;
            this.indices[this.m_currentIndex] = (short) (firstVertex + 2);
            this.m_currentIndex++;
            this.indices[this.m_currentIndex] = (short) (firstVertex + 2);
            this.m_currentIndex++;
            this.indices[this.m_currentIndex] = (short) (firstVertex + 1);
            this.m_currentIndex++;
            this.indices[this.m_currentIndex] = (short) (firstVertex + 3);
            this.m_currentIndex++;
            return;
        }
        throw new Exception("GuiMesh: Can't call addQuad(), not enough Quads reserved by SetupArray(): " + (this.m_vertices_count / 4));
    }

    public void addQuad_uvUsedRotatedToRight(float leftX, float topY, float Z, float rightX, float bottomY, float uLeft, float vTop, float uRight, float vBottom) throws Exception {
        this.m_UvBufferUsed = true;
        if (this.m_PhoneAbilities.getViewSmallerSize() < 480) {
            uLeft -= 2048.0f;
            uRight -= 2048.0f;
            vTop -= 2048.0f;
            vBottom -= 2048.0f;
        }
        if (this.m_currentVertex <= this.m_vertices_count - 4) {
            int firstVertex = this.m_currentVertex;
            this.vertices[(this.m_currentVertex * 3) + 0] = leftX;
            this.vertices[(this.m_currentVertex * 3) + 1] = topY;
            this.vertices[(this.m_currentVertex * 3) + 2] = Z;
            this.uvCoords[(this.m_currentVertex * 2) + 0] = uLeft;
            this.uvCoords[(this.m_currentVertex * 2) + 1] = vBottom;
            this.m_currentVertex++;
            this.vertices[(this.m_currentVertex * 3) + 0] = rightX;
            this.vertices[(this.m_currentVertex * 3) + 1] = topY;
            this.vertices[(this.m_currentVertex * 3) + 2] = Z;
            this.uvCoords[(this.m_currentVertex * 2) + 0] = uLeft;
            this.uvCoords[(this.m_currentVertex * 2) + 1] = vTop;
            this.m_currentVertex++;
            this.vertices[(this.m_currentVertex * 3) + 0] = leftX;
            this.vertices[(this.m_currentVertex * 3) + 1] = bottomY;
            this.vertices[(this.m_currentVertex * 3) + 2] = Z;
            this.uvCoords[(this.m_currentVertex * 2) + 0] = uRight;
            this.uvCoords[(this.m_currentVertex * 2) + 1] = vBottom;
            this.m_currentVertex++;
            this.vertices[(this.m_currentVertex * 3) + 0] = rightX;
            this.vertices[(this.m_currentVertex * 3) + 1] = bottomY;
            this.vertices[(this.m_currentVertex * 3) + 2] = Z;
            this.uvCoords[(this.m_currentVertex * 2) + 0] = uRight;
            this.uvCoords[(this.m_currentVertex * 2) + 1] = vTop;
            this.m_currentVertex++;
            this.indices[this.m_currentIndex] = (short) (firstVertex + 0);
            this.m_currentIndex++;
            this.indices[this.m_currentIndex] = (short) (firstVertex + 1);
            this.m_currentIndex++;
            this.indices[this.m_currentIndex] = (short) (firstVertex + 2);
            this.m_currentIndex++;
            this.indices[this.m_currentIndex] = (short) (firstVertex + 2);
            this.m_currentIndex++;
            this.indices[this.m_currentIndex] = (short) (firstVertex + 1);
            this.m_currentIndex++;
            this.indices[this.m_currentIndex] = (short) (firstVertex + 3);
            this.m_currentIndex++;
            return;
        }
        throw new Exception("GuiMesh: Can't call addQuad_uvUsedRotatedToRight(), not enough Quads reserved by SetupArray(): " + (this.m_vertices_count / 4));
    }
}
