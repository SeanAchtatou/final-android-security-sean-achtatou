package com.ludocrazy.tools;

public class LineMesh extends BasicMesh {
    public LineMesh(LogOutput DEBUGLOG_to_use, PhoneAbilities phoneAbilities) {
        super(DEBUGLOG_to_use, phoneAbilities);
    }

    public void setupArraysLines(int lineCount) {
        setupArrays(lineCount, 2, 2);
        this.m_RenderPrimitiveType = 1;
    }

    public void addLine(Coords pos1, Coords pos2) throws Exception {
        if (this.m_currentVertex <= this.m_vertices_count - 2) {
            int firstVertex = this.m_currentVertex;
            this.vertices[(this.m_currentVertex * 3) + 0] = pos1.x;
            this.vertices[(this.m_currentVertex * 3) + 1] = pos1.y;
            this.vertices[(this.m_currentVertex * 3) + 2] = pos1.z;
            this.m_currentVertex++;
            this.vertices[(this.m_currentVertex * 3) + 0] = pos2.x;
            this.vertices[(this.m_currentVertex * 3) + 1] = pos2.y;
            this.vertices[(this.m_currentVertex * 3) + 2] = pos2.z;
            this.m_currentVertex++;
            this.indices[this.m_currentIndex] = (short) (firstVertex + 0);
            this.m_currentIndex++;
            this.indices[this.m_currentIndex] = (short) (firstVertex + 1);
            this.m_currentIndex++;
            return;
        }
        throw new Exception("LineMesh: Can't call addLine(), not enough Vertices reserved by SetupArray(): " + (this.m_vertices_count / 2));
    }
}
