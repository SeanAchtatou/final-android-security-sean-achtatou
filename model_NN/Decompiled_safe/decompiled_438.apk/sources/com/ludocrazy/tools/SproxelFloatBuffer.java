package com.ludocrazy.tools;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

public class SproxelFloatBuffer {
    public FloatBuffer m_Buffer = null;
    public int m_current_element_count = 0;
    int m_element_size_factor = 0;
    int m_total_element_count = 0;

    public SproxelFloatBuffer(int initial_size_count, int element_size_factor) {
        this.m_element_size_factor = element_size_factor;
        expandCreateBuffer(initial_size_count);
    }

    public void expandCreateBuffer(int new_total_element_count_to_add) {
        this.m_total_element_count += new_total_element_count_to_add;
        ByteBuffer byteBuf = ByteBuffer.allocateDirect(this.m_total_element_count * this.m_element_size_factor * 4);
        byteBuf.order(ByteOrder.nativeOrder());
        FloatBuffer newBuffer = byteBuf.asFloatBuffer();
        if (this.m_Buffer != null) {
            newBuffer.position(0);
            this.m_Buffer.position(0);
            newBuffer.put(this.m_Buffer);
        }
        this.m_Buffer = newBuffer;
        this.m_Buffer.position(0);
    }

    public void appendBuffer(FloatBuffer to_be_appended) {
        int append_element_count = to_be_appended.capacity() / this.m_element_size_factor;
        if (this.m_current_element_count + append_element_count > this.m_total_element_count) {
            expandCreateBuffer(append_element_count * 8);
        }
        to_be_appended.position(0);
        this.m_Buffer.position(this.m_current_element_count * this.m_element_size_factor);
        this.m_Buffer.put(to_be_appended);
        this.m_Buffer.position(0);
        this.m_current_element_count += append_element_count;
    }

    public void appendArray(float[] to_be_appended) {
        int append_element_count = to_be_appended.length / this.m_element_size_factor;
        if (this.m_current_element_count + append_element_count > this.m_total_element_count) {
            expandCreateBuffer(append_element_count * 8);
        }
        this.m_Buffer.position(this.m_current_element_count * this.m_element_size_factor);
        this.m_Buffer.put(to_be_appended);
        this.m_Buffer.position(0);
        this.m_current_element_count += append_element_count;
    }

    public void reset_as_empty() {
        this.m_current_element_count = 0;
    }

    public int getByteSize() {
        return this.m_current_element_count * this.m_element_size_factor * 4;
    }
}
