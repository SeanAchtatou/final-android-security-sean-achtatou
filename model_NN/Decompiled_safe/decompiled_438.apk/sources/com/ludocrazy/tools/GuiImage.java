package com.ludocrazy.tools;

public class GuiImage extends GuiMesh {
    public GuiImage(LogOutput DEBUGLOG_to_use, PhoneAbilities phoneAbilities, float texture_square_size_pixels, float screenX, float screenY, float screenWidth, float screenHeight, float screenZ, float pixel_u_start, float pixel_v_start, float pixel_u_width, float pixel_v_height, ColorFloats color_including_alpha) {
        super(DEBUGLOG_to_use, phoneAbilities);
        try {
            setupArraysQuadFaces(1);
            float TEXTUREWIDTH = texture_square_size_pixels;
            float TEXTUREHEIGHT = texture_square_size_pixels;
            float uLeft = pixel_u_start / TEXTUREWIDTH;
            float vTop = pixel_v_start / TEXTUREHEIGHT;
            float f = screenX;
            float f2 = screenY;
            float f3 = screenZ;
            addQuad(f, f2, f3, screenX + screenWidth, screenY + screenHeight, uLeft, vTop + (pixel_v_height / TEXTUREHEIGHT), uLeft + (pixel_u_width / TEXTUREWIDTH), vTop);
            addColorsForLastQuad(color_including_alpha, color_including_alpha, color_including_alpha, color_including_alpha);
            convertArraysToBuffer();
        } catch (Exception e) {
            new ErrorOutput("GuiImage::GuiImage()", "Exception", e);
        }
    }
}
