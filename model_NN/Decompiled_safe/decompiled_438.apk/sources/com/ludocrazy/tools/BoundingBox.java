package com.ludocrazy.tools;

public class BoundingBox {
    public Coords max = new Coords(-9999.9f, -9999.9f, -9999.9f);
    public Coords min = new Coords(9999.9f, 9999.9f, 9999.9f);

    /* access modifiers changed from: package-private */
    public void reset() {
        this.min.x = 9999.9f;
        this.min.y = 9999.9f;
        this.min.z = 9999.9f;
        this.max.x = -9999.9f;
        this.max.y = -9999.9f;
        this.max.z = -9999.9f;
    }

    /* access modifiers changed from: package-private */
    public void addPoint(float x, float y, float z) {
        if (x < this.min.x) {
            this.min.x = x;
        }
        if (y < this.min.y) {
            this.min.y = y;
        }
        if (z < this.min.z) {
            this.min.z = z;
        }
        if (x > this.max.x) {
            this.max.x = x;
        }
        if (y > this.max.y) {
            this.max.y = y;
        }
        if (z > this.max.z) {
            this.max.z = z;
        }
    }

    public boolean isPointInside(float x, float y) {
        if (x < this.min.x) {
            return false;
        }
        if (x > this.max.x) {
            return false;
        }
        if (y < this.min.y) {
            return false;
        }
        if (y > this.max.y) {
            return false;
        }
        return true;
    }

    public boolean isBoxOverlapping_Xonly(float x, float range) {
        if (x + range < this.min.x) {
            return false;
        }
        if (x - range > this.max.x) {
            return false;
        }
        return true;
    }

    public boolean isBoxOverlapping_XYonly(float x, float y, float radius) {
        if (x + radius < this.min.x) {
            return false;
        }
        if (y + radius < this.min.y) {
            return false;
        }
        if (x - radius > this.max.x) {
            return false;
        }
        if (y - radius > this.max.y) {
            return false;
        }
        return true;
    }

    public float getHeightPositiveY() {
        return Math.abs(this.max.y - this.min.y);
    }

    public float getWidth() {
        return this.max.x - this.min.x;
    }

    public Coords getMiddlePoint() {
        return new Coords((this.min.x + this.max.x) / 2.0f, (this.min.y + this.max.y) / 2.0f, (this.min.z + this.max.z) / 2.0f);
    }

    public Coords getRandomPosInside() {
        Coords pos = new Coords();
        pos.x = Tools.random(this.min.x, this.max.x);
        pos.y = Tools.random(this.min.y, this.max.y);
        pos.z = Tools.random(this.min.z, this.max.z);
        return pos;
    }
}
