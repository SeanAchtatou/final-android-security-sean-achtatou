package com.ludocrazy.tools;

import javax.microedition.khronos.opengles.GL10;

public class PointSpritesCloud extends PointSpritesMesh {
    private HSLcolor m_ColorInBetween = new HSLcolor();
    private HSLcolor m_ColorRangeArea = null;
    private HSLcolor m_ColorRangeStart = null;
    private int m_DrawCount = 0;
    private int m_DrawIndex = -1;

    public PointSpritesCloud(int points_count, HSLcolor colorRangeStart, HSLcolor colorRangeEnd) {
        setupArrays(points_count);
        this.m_ColorRangeStart = colorRangeStart;
        this.m_ColorRangeArea = new HSLcolor(colorRangeEnd.h - colorRangeStart.h, colorRangeEnd.s - colorRangeStart.s, colorRangeEnd.l - colorRangeStart.l);
    }

    public void createBox(Vector3D boundingBox_lowerEdge, Vector3D boundingBox_size) {
        for (int i = 0; i < this.m_PointSpriteVertexCount; i++) {
            this.m_vertices[(i * 3) + 0] = boundingBox_lowerEdge.x + (boundingBox_size.x * Tools.random());
            this.m_vertices[(i * 3) + 1] = boundingBox_lowerEdge.y + (boundingBox_size.y * Tools.random());
            this.m_vertices[(i * 3) + 2] = boundingBox_lowerEdge.z + (boundingBox_size.z * Tools.random());
            randomizeColorInRange(i);
        }
        convertArrays();
    }

    public void createPeterDeJongAttractor3D(Vector3D boundingBox_lowerEdge, Vector3D boundingBox_size) {
        float[] f = new float[9];
        for (int iF = 0; iF < 9; iF++) {
            f[iF] = ((Tools.random() * 2.0f) * 3.1415927f) - 3.1415927f;
        }
        for (int i = 0; i < this.m_PointSpriteVertexCount; i++) {
            float new_x = (float) ((Math.cos((double) (f[0] * 0.0f)) + Math.sin((double) (f[1] * 0.0f))) - Math.sin((double) (f[2] * 0.0f)));
            float new_y = (float) ((Math.sin((double) (f[3] * 0.0f)) - Math.cos((double) (f[4] * 0.0f))) + Math.sin((double) (f[5] * 0.0f)));
            float new_z = (float) ((-Math.cos((double) (f[6] * 0.0f))) + Math.cos((double) (f[7] * 0.0f)) + Math.cos((double) (f[8] * 0.0f)));
            this.m_vertices[(i * 3) + 0] = boundingBox_lowerEdge.x + (boundingBox_size.x * ((3.0f + new_x) / 6.0f));
            this.m_vertices[(i * 3) + 1] = boundingBox_lowerEdge.y + (boundingBox_size.y * ((3.0f + new_y) / 6.0f));
            this.m_vertices[(i * 3) + 2] = boundingBox_lowerEdge.z + (boundingBox_size.z * ((3.0f + new_z) / 6.0f));
            randomizeColorInRange(i);
        }
        convertArrays();
    }

    private void randomizeColorInRange(int i) {
        this.m_ColorInBetween.h = this.m_ColorRangeStart.h + (this.m_ColorRangeArea.h * Tools.random());
        this.m_ColorInBetween.s = this.m_ColorRangeStart.s + (this.m_ColorRangeArea.s * Tools.random());
        this.m_ColorInBetween.l = this.m_ColorRangeStart.l + (this.m_ColorRangeArea.l * Tools.random());
        convertHSLtoRGB_addingIntoColorArray(this.m_ColorInBetween, i);
    }

    private void convertHSLtoRGB_addingIntoColorArray(HSLcolor c1, int i) {
        ColorFloats color = c1.convertToRGB();
        this.m_colors[(i * 4) + 0] = color.red;
        this.m_colors[(i * 4) + 1] = color.green;
        this.m_colors[(i * 4) + 2] = color.blue;
    }

    public int drawAnimated(GL10 gl) {
        this.m_DrawIndex++;
        while (this.m_DrawIndex >= this.m_PointSpriteVertexCount) {
            this.m_DrawIndex -= this.m_PointSpriteVertexCount;
        }
        int draw_end = this.m_DrawIndex + ((int) (((float) this.m_PointSpriteVertexCount) * 0.25f));
        while (draw_end >= this.m_PointSpriteVertexCount) {
            draw_end = this.m_PointSpriteVertexCount - 1;
        }
        int draw_count = draw_end - this.m_DrawIndex;
        if (this.m_DrawCount > draw_count) {
            this.m_DrawCount = draw_count;
        } else if (this.m_DrawCount < draw_count) {
            this.m_DrawCount += ((int) (((float) this.m_PointSpriteVertexCount) * 0.005f)) + 1;
        }
        this.colorBuffer.position(0);
        this.vertexBuffer.position(0);
        gl.glVertexPointer(3, 5126, 0, this.vertexBuffer);
        gl.glColorPointer(4, 5126, 0, this.colorBuffer);
        gl.glDrawArrays(0, this.m_DrawIndex, this.m_DrawCount);
        return this.m_DrawCount;
    }
}
