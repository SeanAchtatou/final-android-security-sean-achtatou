package com.ludocrazy.tools;

public class Vector2D {
    public float x;
    public float y;

    public Vector2D(float initial_x, float initial_y) {
        this.x = initial_x;
        this.y = initial_y;
    }

    public void copyFrom(Vector2Dint copy_from) {
        this.x = (float) copy_from.x;
        this.y = (float) copy_from.y;
    }

    public boolean shrinkTowardsZero(float amount_to_shrink) {
        boolean both_zero = true;
        if (Math.abs(this.x) > 0.0f) {
            both_zero = false;
            float sign_x = Math.signum(this.x);
            this.x -= sign_x * amount_to_shrink;
            if ((sign_x > 0.0f && this.x < 0.0f) || (sign_x < 0.0f && this.x > 0.0f)) {
                this.x = 0.0f;
            }
        }
        if (Math.abs(this.y) > 0.0f) {
            both_zero = false;
            float sign_y = Math.signum(this.y);
            this.y -= sign_y * amount_to_shrink;
            if ((sign_y > 0.0f && this.y < 0.0f) || (sign_y < 0.0f && this.y > 0.0f)) {
                this.y = 0.0f;
            }
        }
        return both_zero;
    }
}
