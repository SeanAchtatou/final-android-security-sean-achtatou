package com.ludocrazy.tools;

public class FpsMeasure {
    private float m_AveragedFps = 30.0f;
    private float m_GameTimeSeconds = 0.0f;
    private float m_LastFrameDurationSeconds = 1.0f;
    private long m_LastMeasureTime = System.nanoTime();
    private float m_LastOutputGameTimeSeconds = 0.0f;
    private double m_SummedFrameTimeSeconds = 0.0d;
    private long m_SummedFramesCount = 0;

    public float getGameTimeSeconds() {
        return this.m_GameTimeSeconds;
    }

    public float getLastFrameDurationSeconds() {
        return this.m_LastFrameDurationSeconds;
    }

    public float getAveragedFps() {
        return this.m_AveragedFps;
    }

    public float update(LogOutput DebugLogOutput_if_output_wanted__else_null) {
        long current = System.nanoTime();
        this.m_LastMeasureTime = current;
        double duration_seconds = ((((double) (current - this.m_LastMeasureTime)) / 1000.0d) / 1000.0d) / 1000.0d;
        this.m_LastFrameDurationSeconds = (float) duration_seconds;
        this.m_GameTimeSeconds += this.m_LastFrameDurationSeconds;
        float current_fps = 1.0f / this.m_LastFrameDurationSeconds;
        this.m_AveragedFps = (0.975f * this.m_AveragedFps) + (0.025f * current_fps);
        this.m_SummedFrameTimeSeconds += duration_seconds;
        this.m_SummedFramesCount++;
        if (DebugLogOutput_if_output_wanted__else_null != null && this.m_GameTimeSeconds - this.m_LastOutputGameTimeSeconds > 10.0f) {
            DebugLogOutput_if_output_wanted__else_null.log(1, "FpsMeasure() Last " + ((int) this.m_SummedFrameTimeSeconds) + " seconds: " + this.m_SummedFramesCount + " frames =fps:" + (((double) this.m_SummedFramesCount) / this.m_SummedFrameTimeSeconds) + ", average:" + this.m_AveragedFps + ", latest:" + current_fps);
            this.m_LastOutputGameTimeSeconds = this.m_GameTimeSeconds;
            this.m_SummedFrameTimeSeconds = 0.0d;
            this.m_SummedFramesCount = 0;
        }
        return this.m_LastFrameDurationSeconds;
    }
}
