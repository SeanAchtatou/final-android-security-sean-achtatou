package com.ludocrazy.tools;

public class StopWatch {
    private long start = 0;

    public StopWatch() {
        restart();
    }

    public void restart() {
        this.start = System.currentTimeMillis();
    }

    public long getElapsedMilliSeconds() {
        return System.currentTimeMillis() - this.start;
    }
}
