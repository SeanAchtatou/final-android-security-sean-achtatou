package com.ludocrazy.tools;

import javax.microedition.khronos.opengles.GL10;

public class CameraOrtho {
    public static final float CAMERABLENDFACTOR = 0.25f;
    public static float[] m_ProjectionMatrix = new float[16];
    private float SCREENFIELDSHEIGHT = 14.2f;
    private float SCREENFIELDSWIDTH = 8.0f;
    public boolean m_CameraNeedsRedraw = true;
    private Vector3D m_CameraPos = new Vector3D();
    private Vector3D m_CameraPosTarget = new Vector3D();
    private boolean m_ProjectionNeeded = true;

    public class CameraOrthoExtends {
        public float orthoCameraMaxX;
        public float orthoCameraMaxY;
        public float orthoCameraMinX;
        public float orthoCameraMinY;

        public CameraOrthoExtends() {
        }
    }

    public void setOrthoCamera_ScreenSize(float width, float height) {
        this.SCREENFIELDSWIDTH = width;
        this.SCREENFIELDSHEIGHT = height;
    }

    public void setOrthoCamera_TargetScreen(int x, int y) {
        this.m_CameraPosTarget.x = (this.SCREENFIELDSWIDTH * ((float) x)) + ((float) x);
        this.m_CameraPosTarget.y = (this.SCREENFIELDSHEIGHT * ((float) y)) + ((float) y);
        this.m_CameraPosTarget.z = this.m_CameraPos.z;
        this.m_CameraNeedsRedraw = true;
    }

    public boolean isOrthoCamera_OnScreen(int x, int y) {
        boolean yCorrect;
        if (Math.abs(this.m_CameraPos.y - ((this.SCREENFIELDSHEIGHT * ((float) y)) + ((float) y))) <= 0.001f) {
            yCorrect = true;
        } else {
            yCorrect = false;
        }
        if (!yCorrect) {
            return false;
        }
        return Math.abs(this.m_CameraPos.x - ((this.SCREENFIELDSWIDTH * ((float) x)) + ((float) x))) <= 0.001f;
    }

    public Vector3D getCameraPositionTarget() {
        return this.m_CameraPosTarget;
    }

    public void onSurfaceChanged(GL10 gl, int width, int height) {
        this.m_ProjectionNeeded = true;
        setupProjectionMatrix_ViaOrthoCamera_UpdatingCamera(gl);
    }

    private boolean isDistanceLargerEpsilon(float value1, float value2) {
        return Math.abs(value1 - value2) > 0.01f;
    }

    public void setOrthoCamera_initialPos(float camera_pos_x, float camera_pos_y, float camera_pos_target_x, float camera_pos_target_y) {
        this.m_CameraPos.x = camera_pos_x;
        this.m_CameraPos.y = camera_pos_y;
        this.m_CameraPos.z = 0.0f;
        this.m_CameraPosTarget.x = camera_pos_target_x;
        this.m_CameraPosTarget.y = camera_pos_target_y;
        this.m_CameraPosTarget.z = 0.0f;
    }

    public static void setupProjectionMatrixViaOrtho(GL10 gl, float left, float right, float bottom, float top, float near, float far, boolean use_negative_z_factor_code) {
        float minus_two = -2.0f;
        if (!use_negative_z_factor_code) {
            minus_two = 2.0f;
        }
        m_ProjectionMatrix[0] = 2.0f / (right - left);
        m_ProjectionMatrix[4] = 0.0f;
        m_ProjectionMatrix[8] = 0.0f;
        m_ProjectionMatrix[12] = (-(right + left)) / (right - left);
        m_ProjectionMatrix[1] = 0.0f;
        m_ProjectionMatrix[5] = 2.0f / (top - bottom);
        m_ProjectionMatrix[9] = 0.0f;
        m_ProjectionMatrix[13] = (-(top + bottom)) / (top - bottom);
        m_ProjectionMatrix[2] = 0.0f;
        m_ProjectionMatrix[6] = 0.0f;
        m_ProjectionMatrix[10] = minus_two / (far - near);
        m_ProjectionMatrix[14] = (-(far + near)) / (far - near);
        m_ProjectionMatrix[3] = 0.0f;
        m_ProjectionMatrix[7] = 0.0f;
        m_ProjectionMatrix[11] = 0.0f;
        m_ProjectionMatrix[15] = 1.0f;
        gl.glMultMatrixf(m_ProjectionMatrix, 0);
    }

    public CameraOrthoExtends setupProjectionMatrix_ViaOrthoCamera_UpdatingCamera(GL10 gl) {
        boolean z;
        if (isDistanceLargerEpsilon(this.m_CameraPos.x, this.m_CameraPosTarget.x) || isDistanceLargerEpsilon(this.m_CameraPos.y, this.m_CameraPosTarget.y)) {
            z = true;
        } else {
            z = false;
        }
        this.m_CameraNeedsRedraw = z;
        this.m_CameraPos.interpolate_linear(this.m_CameraPosTarget, 0.25f);
        if (this.m_CameraNeedsRedraw && !isDistanceLargerEpsilon(this.m_CameraPos.x, this.m_CameraPosTarget.x) && !isDistanceLargerEpsilon(this.m_CameraPos.y, this.m_CameraPosTarget.y)) {
            this.m_CameraPos.x = this.m_CameraPosTarget.x;
            this.m_CameraPos.y = this.m_CameraPosTarget.y;
        }
        CameraOrthoExtends extents = new CameraOrthoExtends();
        extents.orthoCameraMinX = ((-this.SCREENFIELDSWIDTH) / 2.0f) + this.m_CameraPos.x;
        extents.orthoCameraMaxX = (this.SCREENFIELDSWIDTH / 2.0f) + this.m_CameraPos.x;
        extents.orthoCameraMinY = ((-this.SCREENFIELDSHEIGHT) / 2.0f) + this.m_CameraPos.y;
        extents.orthoCameraMaxY = (this.SCREENFIELDSHEIGHT / 2.0f) + this.m_CameraPos.y;
        if (this.m_ProjectionNeeded || this.m_CameraNeedsRedraw) {
            gl.glMatrixMode(5889);
            gl.glLoadIdentity();
            setupProjectionMatrixViaOrtho(gl, extents.orthoCameraMinX, extents.orthoCameraMaxX, extents.orthoCameraMinY, extents.orthoCameraMaxY, (-this.SCREENFIELDSHEIGHT) * 2.0f, 2.0f * this.SCREENFIELDSHEIGHT, true);
            gl.glMatrixMode(5888);
            gl.glLoadIdentity();
            this.m_ProjectionNeeded = false;
        }
        return extents;
    }
}
