package com.ludocrazy.tools;

public class Coords {
    public float x;
    public float y;
    public float z;

    public Coords() {
    }

    public Coords(float _x, float _y, float _z) {
        this.x = _x;
        this.y = _y;
        this.z = _z;
    }

    public Coords(Coords partner_to_copy_from) {
        this.x = partner_to_copy_from.x;
        this.y = partner_to_copy_from.y;
        this.z = partner_to_copy_from.z;
    }

    public void set(float _x, float _y, float _z) {
        this.x = _x;
        this.y = _y;
        this.z = _z;
    }

    public void set(Coords copy_from_here) {
        this.x = copy_from_here.x;
        this.y = copy_from_here.y;
        this.z = copy_from_here.z;
    }

    public void scale(float factor) {
        this.x *= factor;
        this.y *= factor;
        this.z *= factor;
    }

    public void add(Coords partner) {
        this.x += partner.x;
        this.y += partner.y;
        this.z += partner.z;
    }

    public void blend(Coords partner, float amount) {
        float invamount = 1.0f - amount;
        this.x = (this.x * invamount) + (partner.x * amount);
        this.y = (this.y * invamount) + (partner.y * amount);
        this.z = (this.z * invamount) + (partner.z * amount);
    }
}
