package com.ludocrazy.tools;

public class GradientMesh extends GuiMesh {
    public GradientMesh(LogOutput DEBUGLOG_to_use, PhoneAbilities phoneAbilities, float start_x, float end_x, float start_y, float end_y, float z, ColorFloats top_color, ColorFloats bottom_color) throws Exception {
        super(DEBUGLOG_to_use, phoneAbilities);
        setupArraysQuadFaces(1);
        addQuad_plain(start_x, start_y, z, end_x, end_y);
        addColorsForLastQuad(top_color, top_color, bottom_color, bottom_color);
        convertArraysToBuffer();
    }
}
