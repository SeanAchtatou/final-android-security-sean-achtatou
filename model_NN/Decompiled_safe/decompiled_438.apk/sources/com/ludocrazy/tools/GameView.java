package com.ludocrazy.tools;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.opengl.GLSurfaceView;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

public class GameView extends GLSurfaceView implements GLSurfaceView.Renderer {
    public static final float ANIMATIONBLENDFACTOR = 0.2f;
    public static boolean DEMOVERSION = false;
    protected static final int GUI_TRIANGLE_MAX = 250;
    public static final float ROTATIONBLENDFACTOR = 0.3f;
    public static final float ROTATIONBLENDFACTOR_SLOW = 0.1f;
    public LogOutput DebugLogOutput = null;
    protected int m_3dEnabled = 0;
    protected boolean m_AllHasBeenLoaded = false;
    private FloatBuffer m_ClearColorBuffer = null;
    private FloatBuffer m_ClearVertexBuffer = null;
    protected int m_CurrentLoadingStep = 0;
    protected BasicMesh m_DynamicGuiMesh = null;
    protected float m_GameTimeSeconds = 0.0f;
    protected int m_LogoDrawCount = -1;
    protected boolean m_LogoSoundStarted = false;
    protected Sproxel m_LogoSproxel = null;
    /* access modifiers changed from: protected */
    public Activity m_ParentActivityAndContext = null;
    protected ParticleManager m_ParticleManager = null;
    public PhoneAbilities m_PhoneAbilities;
    protected int m_ScreenHeightPixels;
    protected int m_ScreenWidthPixels;
    protected float[] m_Sensor_Gravity = new float[3];
    protected float[] m_Sensor_LinearAcceleration = new float[3];
    protected TextureManager m_TextureManager = null;
    /* access modifiers changed from: protected */
    public MediaManager m_pMediaManager;
    protected boolean m_resumeNeedsRedraw = false;
    protected float m_xrot;
    protected float m_yrot;
    protected float m_zrot;

    public GameView(Activity activity_context, MediaManager mm, LogOutput debug_log_output, boolean is_demo_version) {
        super(activity_context);
        DEMOVERSION = is_demo_version;
        try {
            this.DebugLogOutput = debug_log_output;
            this.m_ParentActivityAndContext = activity_context;
            this.m_pMediaManager = mm;
            this.m_PhoneAbilities = new PhoneAbilities(this.DebugLogOutput, 2);
            setRenderer(this);
            requestFocus();
            setFocusableInTouchMode(true);
            StopWatch stopwatch = new StopWatch();
            this.m_ParticleManager = new ParticleManager(this.m_ParentActivityAndContext, this.DebugLogOutput);
            this.DebugLogOutput.log(1, "performance: loading particlemanager library took " + stopwatch.getElapsedMilliSeconds() + " ms.");
            stopwatch.restart();
            setupClearVertexBuffer();
            this.m_TextureManager = new TextureManager(this.m_ParentActivityAndContext, this.DebugLogOutput, this.m_PhoneAbilities);
        } catch (Exception e) {
            new ErrorOutput("GameView::GameView()", "Exception", e);
        }
    }

    /* access modifiers changed from: protected */
    public void createLogo(float scale, float screenfieldsheight) {
        this.m_LogoSproxel = new Sproxel();
        this.m_LogoSproxel.preLoad_setWorldCoordinates(scale * 6.9f, scale * 6.9f, 6.9f * scale, 0.0f, 2.2f * screenfieldsheight, 0.0f);
        this.m_LogoSproxel.loadCubeKingdomFile(this.m_ParentActivityAndContext, "sproxel_ludocrazy_logo.cko", true);
        this.m_LogoDrawCount = this.m_LogoSproxel.m_PointSpriteVertexCount;
    }

    /* access modifiers changed from: protected */
    public void convertLogoToParticleEffect() {
        this.DebugLogOutput.log(2, "creating pfx from logo...");
        int size = this.m_LogoSproxel.vertexBuffer.capacity() / 3;
        for (int i = 0; i < size; i++) {
            float startPosX = this.m_LogoSproxel.vertexBuffer.get((i * 3) + 0);
            float startPosY = -this.m_LogoSproxel.vertexBuffer.get((i * 3) + 1);
            float startPosZ = this.m_LogoSproxel.vertexBuffer.get((i * 3) + 2);
            this.m_ParticleManager.CreateParticleFromPosition("logo_effect_fire", startPosX, startPosY, startPosZ, this.m_GameTimeSeconds, 1.0f, 1.0f, 1.0f, 1.0f, 0.05f, 0.0f);
            this.m_ParticleManager.CreateParticleFromPosition("logo_effect_fire", startPosX, startPosY, startPosZ, this.m_GameTimeSeconds, 1.0f, 1.0f, 1.0f, 1.0f, 0.05f, 0.0f);
            this.m_ParticleManager.CreateParticleFromPosition("logo_effect_rain", startPosX, startPosY, startPosZ, this.m_GameTimeSeconds, 1.0f, 1.0f, 1.0f, 1.0f, 0.05f, 0.0f);
            this.m_ParticleManager.CreateParticleFromPosition("logo_effect_rain", startPosX, startPosY, startPosZ, this.m_GameTimeSeconds, 1.0f, 1.0f, 1.0f, 1.0f, 0.05f, 0.0f);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.ludocrazy.tools.MediaManager.playStreamedMusic(java.lang.String, boolean, float):void
     arg types: [java.lang.String, int, int]
     candidates:
      com.ludocrazy.tools.MediaManager.playStreamedMusic(int, boolean, float):void
      com.ludocrazy.tools.MediaManager.playStreamedMusic(java.lang.String, boolean, float):void */
    /* access modifiers changed from: protected */
    public void startLogoSoundEffect(int prefs_Music_Volume, String logo_music_filename) {
        this.m_LogoSoundStarted = true;
        if (prefs_Music_Volume > 0) {
            this.m_pMediaManager.playStreamedMusic(logo_music_filename, false, 1.5f);
        }
    }

    /* access modifiers changed from: protected */
    public int getSmallerScreenSidePixels(int screenWidth, int screenHeight) {
        int smallerScreenSideSizePixels = screenHeight;
        if (smallerScreenSideSizePixels > screenWidth) {
            return screenWidth;
        }
        return smallerScreenSideSizePixels;
    }

    /* access modifiers changed from: protected */
    public int getLargerScreenSidePixels(int screenWidth, int screenHeight) {
        int largerScreenSideSizePixels = screenWidth;
        if (largerScreenSideSizePixels < screenHeight) {
            return screenHeight;
        }
        return largerScreenSideSizePixels;
    }

    /* access modifiers changed from: protected */
    public float blend_rotation(float start, float end, float percentage) {
        if (Math.abs(start - end) < 180.0f) {
            return (end * percentage) + ((1.0f - percentage) * start);
        }
        if (start > end) {
            end += 360.0f;
        } else {
            start += 360.0f;
        }
        return ((end * percentage) + ((1.0f - percentage) * start)) % 360.0f;
    }

    /* access modifiers changed from: protected */
    public void setupClearVertexBuffer() {
        ByteBuffer byteBuf = ByteBuffer.allocateDirect(48);
        byteBuf.order(ByteOrder.nativeOrder());
        this.m_ClearVertexBuffer = byteBuf.asFloatBuffer();
        this.m_ClearVertexBuffer.put(new float[]{5.0f, -10.0f, 0.0f, -5.0f, -10.0f, 0.0f, 5.0f, 10.0f, 0.0f, -5.0f, 10.0f, 0.0f});
        this.m_ClearVertexBuffer.position(0);
        ByteBuffer byteBuf2 = ByteBuffer.allocateDirect(64);
        byteBuf2.order(ByteOrder.nativeOrder());
        this.m_ClearColorBuffer = byteBuf2.asFloatBuffer();
    }

    public void clearAndSetupLoadingScreen(GL10 gl) {
        gl.glMatrixMode(5889);
        gl.glLoadIdentity();
        CameraOrtho.setupProjectionMatrixViaOrtho(gl, -0.8f, 0.8f, 1.0f, 0.0f, 0.0f, 1.0f, false);
        gl.glMatrixMode(5888);
        gl.glLoadIdentity();
        gl.glClear(16640);
    }

    /* access modifiers changed from: protected */
    public void doClear(GL10 gl) {
        float alpha = 1.0f;
        if (1.0f >= 1.0f) {
            gl.glClear(16640);
            return;
        }
        gl.glClear(256);
        gl.glDisable(2929);
        if (1.0f < 0.0f) {
            alpha = 0.0f;
        }
        float[] colors = new float[16];
        for (int i = 0; i < 4; i++) {
            colors[(i * 4) + 0] = 0.0f;
            colors[(i * 4) + 1] = 0.0f;
            colors[(i * 4) + 2] = 0.0f;
            colors[(i * 4) + 3] = alpha;
        }
        this.m_ClearColorBuffer.position(0);
        this.m_ClearColorBuffer.put(colors);
        this.m_ClearColorBuffer.position(0);
        gl.glEnable(3042);
        gl.glBlendFunc(770, 771);
        gl.glEnableClientState(32884);
        gl.glEnableClientState(32886);
        gl.glDisableClientState(32888);
        gl.glDisable(3553);
        gl.glVertexPointer(3, 5126, 0, this.m_ClearVertexBuffer);
        gl.glColorPointer(4, 5126, 0, this.m_ClearColorBuffer);
        gl.glDrawArrays(5, 0, 4);
        gl.glDisableClientState(32886);
        gl.glDisableClientState(32884);
        gl.glDisable(3042);
        gl.glEnable(3553);
        gl.glEnable(2929);
    }

    /* access modifiers changed from: protected */
    public boolean isRotationSmallerEpsilon(float rotation) {
        if (Math.abs(rotation) <= 0.001f) {
            return true;
        }
        if (Math.abs(360.0f - rotation) <= 0.001f) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public void openBrowserWithURL(String url) {
        Intent i = new Intent("android.intent.action.VIEW");
        i.setData(Uri.parse(url));
        this.m_ParentActivityAndContext.startActivity(i);
    }

    public static void checkForGlErrors(GL10 gl, String msg) {
        int error = gl.glGetError();
        if (error != 0) {
            throw new RuntimeException("OpenGL Error: " + glErrorToString(error) + " at:" + msg);
        }
    }

    private static String glErrorToString(int error) {
        switch (error) {
            case 0:
                return String.valueOf(error) + " GL_NO_ERROR";
            case 1280:
                return String.valueOf(error) + " GL_INVALID_ENUM";
            case 1281:
                return String.valueOf(error) + " GL_INVALID_VALUE";
            case 1282:
                return String.valueOf(error) + " GL_INVALID_OPERATION";
            case 1283:
                return String.valueOf(error) + " GL_STACK_OVERFLOW";
            case 1284:
                return String.valueOf(error) + " GL_STACK_UNDERFLOW";
            case 1285:
                return String.valueOf(error) + " GL_OUT_OF_MEMORY";
            default:
                return String.valueOf(error) + " UNKNOWN error number!";
        }
    }

    public void onResume() {
        super.onResume();
        this.m_resumeNeedsRedraw = true;
    }

    public void setSensorOrientation(float x, float y, float z) {
        if (this.m_3dEnabled == -3) {
            this.m_xrot = blend_rotation(this.m_xrot, y, 0.3f);
            this.m_yrot = blend_rotation(this.m_yrot, -z, 0.3f);
            this.m_zrot = blend_rotation(this.m_zrot, x, 0.3f);
        }
    }

    public void setSensorAcceleration(float x, float y, float z) {
        this.m_Sensor_Gravity[0] = (this.m_Sensor_Gravity[0] * 0.85f) + (0.14999998f * x);
        this.m_Sensor_Gravity[1] = (this.m_Sensor_Gravity[1] * 0.85f) + (0.14999998f * y);
        this.m_Sensor_Gravity[2] = (this.m_Sensor_Gravity[2] * 0.85f) + (0.14999998f * z);
        this.m_Sensor_LinearAcceleration[0] = x - this.m_Sensor_Gravity[0];
        this.m_Sensor_LinearAcceleration[1] = y - this.m_Sensor_Gravity[1];
        this.m_Sensor_LinearAcceleration[2] = z - this.m_Sensor_Gravity[2];
    }

    public void onDrawFrame(GL10 gl) {
    }

    public void onSurfaceChanged(GL10 gl, int width, int height) {
    }

    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
    }
}
