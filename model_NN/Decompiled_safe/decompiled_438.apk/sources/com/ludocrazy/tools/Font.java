package com.ludocrazy.tools;

import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import java.util.HashMap;
import javax.microedition.khronos.opengles.GL10;

public class Font {
    /* access modifiers changed from: private */
    public int FONT_TEXTURE_SIZE = 512;
    private Typeface font;
    private final HashMap<Character, GlyphBitmap> glyphBitmaps = new HashMap<>();
    private int glyphX = 0;
    private int glyphY = 0;
    private final HashMap<Character, Glyph> glyphs = new HashMap<>();
    /* access modifiers changed from: private */
    public boolean m_ExternalTexture = false;
    /* access modifiers changed from: private */
    public int m_FakeboldExtraDrawCounts = 0;
    private float m_FontScale_BaseSize = 1.0f;
    /* access modifiers changed from: private */
    public int m_Outline_Width = 0;
    /* access modifiers changed from: private */
    public PhoneAbilities m_PhoneAbilities = null;
    /* access modifiers changed from: private */
    public Paint.FontMetrics metrics;
    /* access modifiers changed from: private */
    public Paint paint;
    /* access modifiers changed from: private */
    public final FontTexture texture;

    public enum FontStyle {
        Plain,
        Bold,
        Italic,
        BoldItalic
    }

    public enum HorizontalAlign {
        Left,
        Center,
        Right
    }

    public enum VerticalAlign {
        Top,
        Center,
        Bottom
    }

    public class Rectangle {
        public float height;
        public float width;
        public float x;
        public float y;

        public Rectangle() {
            this.x = 0.0f;
            this.y = 0.0f;
            this.width = 0.0f;
            this.height = 0.0f;
        }

        public Rectangle(float x2, float y2, float width2, float height2) {
            this.x = x2;
            this.y = y2;
            this.width = width2;
            this.height = height2;
        }
    }

    public Font(PhoneAbilities phoneAbilities, int texture_square_size_pixels, GL10 gl, String fontName, int size, int scale_base_size, FontStyle style, int fakebold_extradraw_counts, int outline_width) throws Exception {
        boolean convert_abgr_to_argb;
        this.m_PhoneAbilities = phoneAbilities;
        this.FONT_TEXTURE_SIZE = texture_square_size_pixels;
        this.m_FakeboldExtraDrawCounts = fakebold_extradraw_counts;
        this.m_Outline_Width = outline_width;
        this.font = Typeface.create(fontName, getFontStyle(style));
        if (this.m_PhoneAbilities.isSupported(1)) {
            convert_abgr_to_argb = false;
        } else {
            convert_abgr_to_argb = true;
        }
        this.texture = new FontTexture(gl, this.FONT_TEXTURE_SIZE, this.FONT_TEXTURE_SIZE, convert_abgr_to_argb);
        FontConstructorContinued(size, scale_base_size, style);
    }

    public Font(PhoneAbilities phoneAbilities, int texture_square_size_pixels, GL10 gl, AssetManager assets, String file, int size, int scale_base_size, FontStyle style, int ExternalFontHandle, int fakebold_extradraw_counts, int outline_width) {
        this.m_PhoneAbilities = phoneAbilities;
        this.FONT_TEXTURE_SIZE = texture_square_size_pixels;
        this.m_FakeboldExtraDrawCounts = fakebold_extradraw_counts;
        this.m_Outline_Width = outline_width;
        this.font = Typeface.createFromAsset(assets, file);
        this.m_ExternalTexture = true;
        this.texture = new FontTexture(gl, ExternalFontHandle, this.FONT_TEXTURE_SIZE, this.FONT_TEXTURE_SIZE);
        FontConstructorContinued(size, scale_base_size, style);
    }

    private void FontConstructorContinued(int size, int scale_base_size, FontStyle style) {
        this.m_FontScale_BaseSize = ((float) scale_base_size) / ((float) size);
        this.paint = new Paint();
        this.paint.setTypeface(this.font);
        this.paint.setTextSize((float) size);
        this.paint.setAntiAlias(true);
        this.metrics = this.paint.getFontMetrics();
    }

    private int getFontStyle(FontStyle style) {
        if (style == FontStyle.Bold) {
            return 1;
        }
        if (style == FontStyle.BoldItalic) {
            return 3;
        }
        if (style == FontStyle.Italic) {
            return 2;
        }
        if (style == FontStyle.Plain) {
            return 0;
        }
        return 0;
    }

    public int getGlyphAdvance(char character) {
        float[] width = new float[1];
        this.paint.getTextWidths(new StringBuilder().append(character).toString(), width);
        return (int) Math.ceil((double) width[0]);
    }

    public int getLineGap() {
        return (int) Math.ceil((double) this.metrics.leading);
    }

    public int getLineHeight() {
        return (int) Math.ceil((double) (Math.abs(this.metrics.ascent) + Math.abs(this.metrics.descent)));
    }

    public int getLineMaxHeight() {
        return (int) Math.ceil((double) (Math.abs(this.metrics.top) + Math.abs(this.metrics.bottom)));
    }

    public int getStringWidth(String text) {
        Rect rect = new Rect();
        this.paint.getTextBounds(text, 0, text.length(), rect);
        return rect.width();
    }

    public Text newText(LogOutput DEBUGLOG_to_use, GuiMeshBatch guiMeshBatch, PhoneAbilities phoneAbilities) {
        return new Text(DEBUGLOG_to_use, guiMeshBatch, phoneAbilities, this.m_FontScale_BaseSize);
    }

    /* access modifiers changed from: protected */
    public FontTexture getTexture() {
        return this.texture;
    }

    public void preCreateGlyphsFromString(String text) {
        char[] buffer = new char[1];
        for (int i = 0; i < text.length(); i++) {
            text.getChars(i, i + 1, buffer, 0);
            getGlyph(buffer[0]);
        }
    }

    public void preCreateSingleGlyph(char character) {
        getGlyph(character);
    }

    public void onResumeRerenderGlyphsIntoTexture(GL10 gl) {
        for (Character character_key : this.glyphs.keySet()) {
            Glyph current = this.glyphs.get(character_key);
            GlyphBitmap glyphBitmap = getGlyphBitmap(character_key.charValue());
            this.texture.setGLonResume(gl);
            this.texture.drawOnMipMaps(this.m_PhoneAbilities, glyphBitmap.bitmap, (int) (current.u * ((float) this.FONT_TEXTURE_SIZE)), (int) (current.v * ((float) this.FONT_TEXTURE_SIZE)));
        }
    }

    /* access modifiers changed from: protected */
    public Glyph getGlyph(char character) {
        Glyph glyph = this.glyphs.get(Character.valueOf(character));
        if (glyph != null) {
            return glyph;
        }
        Glyph glyph2 = createGlyph(character);
        this.glyphs.put(Character.valueOf(character), glyph2);
        return glyph2;
    }

    private Glyph createGlyph(char character) {
        GlyphBitmap glyphBitmap = getGlyphBitmap(character);
        if (((float) this.glyphX) + glyphBitmap.rect.width >= ((float) this.FONT_TEXTURE_SIZE)) {
            this.glyphX = 0;
            this.glyphY += getLineGap() + getLineMaxHeight();
        }
        this.texture.drawOnMipMaps(this.m_PhoneAbilities, glyphBitmap.bitmap, this.glyphX, this.glyphY);
        Glyph glyph = new Glyph(getGlyphAdvance(character), (int) glyphBitmap.rect.width, (int) glyphBitmap.rect.height, ((float) this.glyphX) / ((float) this.FONT_TEXTURE_SIZE), ((float) this.glyphY) / ((float) this.FONT_TEXTURE_SIZE), glyphBitmap.rect.width / ((float) this.FONT_TEXTURE_SIZE), glyphBitmap.rect.height / ((float) this.FONT_TEXTURE_SIZE));
        this.glyphX = (int) (((float) this.glyphX) + glyphBitmap.rect.width);
        return glyph;
    }

    public GlyphBitmap getGlyphBitmap(char character) {
        GlyphBitmap glyphBitmap = this.glyphBitmaps.get(Character.valueOf(character));
        if (glyphBitmap != null) {
            return glyphBitmap;
        }
        GlyphBitmap glyphBitmap2 = new GlyphBitmap(character);
        this.glyphBitmaps.put(Character.valueOf(character), glyphBitmap2);
        return glyphBitmap2;
    }

    private class GlyphBitmap {
        Bitmap bitmap = null;
        Rectangle rect = null;

        public GlyphBitmap(char character) {
            int i;
            Rect boundsRect = new Rect();
            Font.this.paint.getTextBounds(new StringBuilder().append(character).toString(), 0, 1, boundsRect);
            this.rect = new Rectangle();
            this.rect.width = (float) (Font.this.m_Outline_Width + boundsRect.width() + 8 + Font.this.m_FakeboldExtraDrawCounts);
            int height = Font.this.m_Outline_Width + Font.this.getLineMaxHeight();
            this.rect.height = (float) height;
            if (((int) this.rect.width) == 0) {
                i = 1;
            } else {
                i = (int) this.rect.width;
            }
            this.bitmap = Bitmap.createBitmap(i, height, Bitmap.Config.ARGB_8888);
            Canvas g = new Canvas(this.bitmap);
            Font.this.paint.setColor(0);
            Font.this.paint.setStyle(Paint.Style.FILL);
            g.drawRect(new Rect(0, 0, boundsRect.width() + 5, height), Font.this.paint);
            float y = (-Font.this.metrics.top) + ((float) Font.this.m_Outline_Width);
            Font.this.paint.setColor(-16777216);
            Font.this.paint.setStyle(Paint.Style.STROKE);
            Font.this.paint.setStrokeWidth((float) (Font.this.m_Outline_Width * 2));
            g.drawText(new StringBuilder().append(character).toString(), 0.0f, y, Font.this.paint);
            Font.this.paint.setColor(-1);
            Font.this.paint.setStyle(Paint.Style.FILL);
            g.drawText(new StringBuilder().append(character).toString(), 0.0f, y, Font.this.paint);
            for (int i2 = 0; i2 < Font.this.m_FakeboldExtraDrawCounts; i2++) {
                g.drawText(new StringBuilder().append(character).toString(), (float) (i2 + 0 + 1), y, Font.this.paint);
            }
        }
    }

    private class Glyph {
        public int advance;
        public float u;
        public float uWidth;
        public float v;
        public float vHeight;
        public int width;

        public Glyph(int advance2, int width2, int height, float u2, float v2, float uWidth2, float vHeight2) {
            this.advance = advance2;
            this.width = width2;
            this.u = u2;
            this.v = v2;
            this.uWidth = uWidth2;
            this.vHeight = vHeight2;
        }
    }

    public class Text {
        private HorizontalAlign hAlign;
        private float height;
        private String[] lines;
        private boolean m_FlippedTextY = false;
        private GuiMesh m_FontMesh = null;
        private float m_FontScale_BaseSize;
        private GuiMeshBatch m_GuiMeshBatch = null;
        private float m_ScaleFactorX = 1.0f;
        private float m_ScaleFactorY = 1.0f;
        private int m_TextAppearCounter = 0;
        private ColorFloats m_TextColor = new ColorFloats(1.0f, 1.0f, 1.0f, 1.0f);
        private float posX;
        private float posY;
        private float posZ;
        private String text = "";
        private VerticalAlign vAlign;
        private float width;
        private int[] widths;

        Text(LogOutput DEBUGLOG_to_use, GuiMeshBatch guiMeshBatch, PhoneAbilities phoneAbilities, float fontScale_BaseSize) {
            this.m_FontMesh = new GuiMesh(DEBUGLOG_to_use, phoneAbilities);
            this.m_GuiMeshBatch = guiMeshBatch;
            this.m_FontScale_BaseSize = fontScale_BaseSize;
        }

        public int getStringCharactersCount() {
            return this.text.length();
        }

        public BoundingBox getBoundingBox() {
            return this.m_FontMesh.getBoundingBox();
        }

        public void setTextArea(float width2, float height2) {
            this.width = width2;
            this.height = height2;
        }

        public void setHorizontalAlign(HorizontalAlign hAlign2) {
            this.hAlign = hAlign2;
        }

        public void setVerticalAlign(VerticalAlign vAlign2) {
            this.vAlign = vAlign2;
        }

        public void setScale(float sizeFactor) {
            this.m_ScaleFactorX = sizeFactor;
            this.m_ScaleFactorY = sizeFactor;
        }

        public void setPosition(float x, float y, float z) {
            this.posX = x;
            this.posY = y;
            this.posZ = z;
        }

        public void flipTextVerticalyForNextRebuild() {
            this.m_FlippedTextY = true;
        }

        public void setTextColor(float r, float g, float b, float a) {
            this.m_TextColor.red = r;
            this.m_TextColor.green = g;
            this.m_TextColor.blue = b;
            this.m_TextColor.alpha = a;
        }

        private boolean setText(String text2) {
            if (this.text.equals(text2)) {
                return false;
            }
            if (text2 == null) {
                text2 = "";
            }
            this.text = text2;
            this.lines = text2.split("\n");
            this.widths = new int[this.lines.length];
            for (int i = 0; i < this.lines.length; i++) {
                this.widths[i] = Font.this.getStringWidth(this.lines[i]);
            }
            return true;
        }

        public void setText_rebuildingMesh(String text2) {
            try {
                if (setText(text2)) {
                    rebuild();
                    this.m_TextAppearCounter = 0;
                }
            } catch (Exception e) {
                new ErrorOutput("Text::setText_rebuildingMesh()", "Exception", e);
            }
        }

        public float placeText_onGivenMesh(String text2, GuiMesh mesh, float x_pos, float y_pos, float z_pos, float text_area_width, float text_area_height, float scale, HorizontalAlign halign, VerticalAlign valign) {
            setTextArea(text_area_width, text_area_height);
            setScale(scale);
            setPosition(x_pos, y_pos, z_pos);
            setHorizontalAlign(halign);
            setVerticalAlign(valign);
            return setText_rebuildingDynamicMesh(text2, mesh);
        }

        public float setText_rebuildingDynamicMesh(String text2, GuiMesh dynamic_quad_mesh) {
            try {
                setText(text2);
                if (dynamic_quad_mesh == null) {
                    new Exception("Given Dynamic-Mesh is NULL!");
                }
                return rebuildMesh(dynamic_quad_mesh);
            } catch (Exception e) {
                new ErrorOutput("Text::setText_rebuildingDynamicMesh()", "Exception", e);
                return 0.0f;
            }
        }

        private float rebuildMesh(GuiMesh font_mesh) throws Exception {
            float y;
            float scale_x = this.m_ScaleFactorX * this.m_FontScale_BaseSize;
            float scale_y = this.m_ScaleFactorY * this.m_FontScale_BaseSize;
            float right_most = 0.0f;
            int lineHeight = Font.this.getLineHeight();
            for (int i = 0; i < this.lines.length; i++) {
                String line = this.lines[i];
                float x = 0.0f;
                float y2 = this.height;
                if (this.hAlign == HorizontalAlign.Left) {
                    x = 0.0f;
                }
                if (this.hAlign == HorizontalAlign.Center) {
                    x = (this.width / 2.0f) - ((float) (this.widths[i] / 2));
                }
                if (this.hAlign == HorizontalAlign.Right) {
                    x = this.width - ((float) this.widths[i]);
                }
                if (this.vAlign == VerticalAlign.Top) {
                    y2 = this.height;
                }
                if (this.vAlign == VerticalAlign.Center) {
                    y2 = (this.height / 2.0f) + ((float) ((this.lines.length * (Font.this.getLineHeight() + Font.this.getLineGap())) / 2));
                }
                if (this.vAlign == VerticalAlign.Bottom) {
                    y2 = (float) (this.lines.length * (Font.this.getLineHeight() + Font.this.getLineGap()));
                }
                if (this.m_FlippedTextY) {
                    y = y2 + ((float) ((Font.this.getLineHeight() + Font.this.getLineGap()) * i));
                } else {
                    y = y2 - ((float) ((Font.this.getLineHeight() + Font.this.getLineGap()) * i));
                }
                for (int j = 0; j < line.length(); j++) {
                    Glyph glyph = Font.this.getGlyph(line.charAt(j));
                    float texture_size = (float) Font.this.FONT_TEXTURE_SIZE;
                    float offset_u = -0.5f / texture_size;
                    float offset_v = 0.5f / texture_size;
                    if (Font.this.m_PhoneAbilities.getViewSmallerSize() < 480) {
                        offset_u += -0.5f / (texture_size / 2.0f);
                        offset_v += 0.5f / (texture_size / 2.0f);
                    }
                    float right = this.posX + ((((float) glyph.width) + x) * scale_x);
                    if (right > right_most) {
                        right_most = right;
                    }
                    if (this.m_FlippedTextY) {
                        font_mesh.addQuad((x * scale_x) + this.posX, (y * scale_y) + this.posY, this.posZ, right, ((y - ((float) lineHeight)) * scale_y) + this.posY, glyph.u + offset_u, glyph.vHeight + glyph.v + offset_v, glyph.uWidth + glyph.u + offset_u, glyph.v + offset_v);
                    } else {
                        font_mesh.addQuad((x * scale_x) + this.posX, (y * scale_y) + this.posY, this.posZ, right, ((y - ((float) lineHeight)) * scale_y) + this.posY, glyph.u + offset_u, glyph.v + offset_v, glyph.uWidth + glyph.u + offset_u, glyph.vHeight + glyph.v + offset_v);
                    }
                    font_mesh.addColorForLastQuad(this.m_TextColor.red, this.m_TextColor.green, this.m_TextColor.blue, this.m_TextColor.alpha);
                    x += (float) glyph.advance;
                }
            }
            return right_most;
        }

        private void rebuild() throws Exception {
            this.m_FontMesh.setupArraysQuadFaces(this.text.length());
            rebuildMesh(this.m_FontMesh);
            this.m_FontMesh.convertArraysToBuffer();
            if (this.m_GuiMeshBatch != null) {
                this.m_GuiMeshBatch.addGuiMesh(this.m_FontMesh);
                this.m_FontMesh = null;
            }
        }

        public int getFontMeshFacesCount() {
            if (this.m_FontMesh != null) {
                return this.m_FontMesh.getFacesCount();
            }
            return 0;
        }

        public boolean render(GL10 gl) throws Exception {
            boolean redrawNeeded = false;
            if (this.m_FontMesh != null) {
                if (!Font.this.m_ExternalTexture) {
                    Font.this.texture.bind();
                }
                int textVerticesCount = this.text.length() * 2 * 3;
                if (this.m_TextAppearCounter < textVerticesCount) {
                    this.m_TextAppearCounter += 6;
                    redrawNeeded = true;
                } else {
                    this.m_TextAppearCounter = textVerticesCount;
                }
                if (this.m_TextAppearCounter > textVerticesCount) {
                    this.m_TextAppearCounter = textVerticesCount;
                }
                gl.glDepthMask(false);
                this.m_FontMesh.draw(gl, this.m_TextAppearCounter);
                gl.glDepthMask(true);
            }
            return redrawNeeded;
        }
    }

    public void dispose() {
        this.texture.dispose();
    }
}
