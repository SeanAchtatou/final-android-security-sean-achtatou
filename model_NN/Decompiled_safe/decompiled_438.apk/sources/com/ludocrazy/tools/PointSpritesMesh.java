package com.ludocrazy.tools;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import javax.microedition.khronos.opengles.GL10;

public class PointSpritesMesh {
    protected FloatBuffer colorBuffer;
    private int m_CurrentDrawCount = -1;
    public int m_PointSpriteVertexCount = 0;
    protected float[] m_colors = null;
    protected float[] m_vertices = null;
    public FloatBuffer vertexBuffer;

    /* access modifiers changed from: protected */
    public void setupArrays(int vertexCount) {
        this.m_PointSpriteVertexCount = vertexCount;
        this.m_vertices = new float[(this.m_PointSpriteVertexCount * 3)];
        this.m_colors = new float[(this.m_PointSpriteVertexCount * 4)];
    }

    /* access modifiers changed from: protected */
    public void convertArrays() {
        ByteBuffer byteBuf = ByteBuffer.allocateDirect(this.m_PointSpriteVertexCount * 3 * 4);
        byteBuf.order(ByteOrder.nativeOrder());
        this.vertexBuffer = byteBuf.asFloatBuffer();
        this.vertexBuffer.put(this.m_vertices);
        this.vertexBuffer.position(0);
        ByteBuffer byteBuf2 = ByteBuffer.allocateDirect(this.m_PointSpriteVertexCount * 4 * 4);
        byteBuf2.order(ByteOrder.nativeOrder());
        this.colorBuffer = byteBuf2.asFloatBuffer();
        this.colorBuffer.put(this.m_colors);
        this.colorBuffer.position(0);
    }

    public int draw(GL10 gl, boolean appearing) {
        if (appearing) {
            this.m_CurrentDrawCount++;
        }
        return draw(gl, this.m_CurrentDrawCount);
    }

    public int draw(GL10 gl, int draw_count) {
        this.m_CurrentDrawCount = draw_count;
        if (this.m_CurrentDrawCount < 0 || this.m_CurrentDrawCount > this.vertexBuffer.capacity() / 3) {
            this.m_CurrentDrawCount = this.vertexBuffer.capacity() / 3;
        }
        this.colorBuffer.position(0);
        this.vertexBuffer.position(0);
        gl.glVertexPointer(3, 5126, 0, this.vertexBuffer);
        gl.glColorPointer(4, 5126, 0, this.colorBuffer);
        gl.glDrawArrays(0, 0, this.m_CurrentDrawCount);
        return this.m_CurrentDrawCount;
    }
}
