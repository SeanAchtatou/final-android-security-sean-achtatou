package com.ludocrazy.excit_demo;

import android.os.Bundle;
import com.ludocrazy.excit.GameViewExcit;
import com.ludocrazy.tools.ErrorOutput;
import com.ludocrazy.tools.LogOutput;
import com.ludocrazy.tools.RunActivity;

public class RunExcit_demo extends RunActivity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        new ErrorOutput(this, "com.ludocrazy.excit_demo");
        try {
            super.onCreate(savedInstanceState);
            this.DebugLogOutput = new LogOutput(this, "com.ludocrazy.excit_demo");
            this.DebugLogOutput.setCurrentLogLevel(0);
            this.DebugLogOutput.log(1, "Starting Excit Demo --------------------------------------------------------");
            this.m_MediaManager.setLogOutput(this.DebugLogOutput);
            this.m_ViewRenderer = new GameViewExcit(this, this.m_MediaManager, this.DebugLogOutput, true);
            setContentView(this.m_ViewRenderer);
        } catch (Exception e) {
            new ErrorOutput("RunExcit::onCreate()", "Exception", e);
        }
    }
}
