package com.ludocrazy.excit;

import android.content.Context;
import android.content.SharedPreferences;
import com.ludocrazy.tools.ErrorOutput;
import com.ludocrazy.tools.LogOutput;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.reflect.Array;

public class MapLogic {
    public MapField[][] m_Map;
    public int m_MapHeight = 0;
    public int m_MapWidth = 0;
    public int m_UncompletedLevelsCount = 99999;

    public class MapField {
        int best_moves;
        int best_pickups;
        String charFieldId;
        boolean completed = false;
        int difficulty_icon = 99;
        boolean empty = true;
        boolean has_pickups;
        boolean in_demo;
        String levelFilename;
        String level_name;
        String musicFilename;
        int music_loop_restart_seconds;
        String solution;
        int solution_moves;
        boolean visible = false;

        MapField(SharedPreferences prefs, String[] details) throws Exception {
            boolean z;
            if (prefs != null && details != null) {
                this.charFieldId = details[0];
                this.difficulty_icon = Integer.parseInt(details[3]);
                this.has_pickups = Integer.parseInt(details[6]) > 0;
                this.level_name = details[13];
                this.levelFilename = details[14];
                this.musicFilename = details[15];
                this.music_loop_restart_seconds = Integer.parseInt(details[16]);
                if (details[17].compareTo("demo") == 0) {
                    z = true;
                } else {
                    z = false;
                }
                this.in_demo = z;
                this.solution = details[18];
                this.solution_moves = this.solution.length();
                this.best_moves = prefs.getInt("BestMoves_" + this.charFieldId, 9999);
                this.best_pickups = prefs.getInt("BestPickups_" + this.charFieldId, 0);
                this.completed = prefs.getBoolean("LevelDone_" + this.charFieldId, false);
                this.visible = false;
                this.empty = false;
                if (!this.completed) {
                    MapLogic.this.m_UncompletedLevelsCount++;
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public String[] clearStrings(String[] elements) {
        for (int i = 0; i < elements.length; i++) {
            if (elements[i].length() > 0 && elements[i].charAt(0) == '\"') {
                elements[i] = elements[i].substring(1, elements[i].length() - 1);
            }
        }
        return elements;
    }

    MapLogic(LogOutput DebugLogOutput, Context context, String mapFileName) {
        int map_x;
        int map_y;
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(context.getAssets().open(mapFileName, 2)), 4096);
            String[] format_elements = clearStrings(bufferedReader.readLine().split(","));
            if (!format_elements[0].equals("EXCIT")) {
                throw new Exception("Error: Trying to read 'EXCIT' from first line of " + mapFileName + " file, but got: " + format_elements[0]);
            }
            int x_width = Integer.parseInt(format_elements[1]);
            int y_height = Integer.parseInt(format_elements[2]);
            this.m_Map = (MapField[][]) Array.newInstance(MapField.class, x_width, y_height);
            this.m_MapWidth = x_width;
            this.m_MapHeight = y_height;
            SharedPreferences prefs = context.getSharedPreferences(GameViewExcit.SHAREDPREFSNAME, 1);
            this.m_UncompletedLevelsCount = 0;
            bufferedReader.readLine();
            bufferedReader.readLine();
            int lineNum = 2;
            while (true) {
                String line = bufferedReader.readLine();
                if (line == null) {
                    this.m_Map[0][3].visible = true;
                    for (int x = 0; x < this.m_MapWidth; x++) {
                        for (int y = 0; y < this.m_MapHeight; y++) {
                            if (this.m_Map[x][y] != null && this.m_Map[x][y].completed) {
                                setVisiblity(x - 1, y);
                                setVisiblity(x + 1, y);
                                setVisiblity(x, y - 1);
                                setVisiblity(x, y + 1);
                            }
                        }
                    }
                    return;
                }
                lineNum++;
                String[] details = clearStrings(line.split(","));
                if (details.length == 19) {
                    map_x = Integer.parseInt(details[1]);
                    map_y = Integer.parseInt(details[2]);
                    if (map_x <= this.m_MapWidth && map_y <= this.m_MapHeight && map_x >= 1 && map_y >= 1) {
                        int map_x2 = map_x - 1;
                        int map_y2 = this.m_MapHeight - map_y;
                        if (this.m_Map[map_x2][map_y2] == null) {
                            this.m_Map[map_x2][map_y2] = new MapField(prefs, details);
                        } else {
                            DebugLogOutput.log(0, "WARNING: ignoring line " + (lineNum + 1) + " as it would overwrite existing field!");
                        }
                    }
                } else if (details.length > 1) {
                    throw new Exception("Expected line to be empty OR have exactly 19 elements! But found " + details.length + " elements:\n" + line);
                }
            }
            throw new Exception("Given map coordinates x:" + map_x + ", y:" + map_y + " are out of range. Map size is x:" + this.m_MapWidth + ", y:" + this.m_MapHeight);
        } catch (Exception e) {
            new ErrorOutput("MapLogic::MapLogic()", "Error reading " + mapFileName + " file. Error on line: " + (-1 + 1) + ".  Exception", e);
        }
    }

    private void setVisiblity(int x, int y) {
        if (x >= 0 && x < this.m_MapWidth && y >= 0 && y < this.m_MapHeight && this.m_Map[x][y] != null && !this.m_Map[x][y].empty) {
            this.m_Map[x][y].visible = true;
        }
    }

    public void resetAllLevels(Context context) {
        SharedPreferences.Editor editor = context.getSharedPreferences(GameViewExcit.SHAREDPREFSNAME, 1).edit();
        for (int x = 0; x < this.m_MapWidth; x++) {
            for (int y = 0; y < this.m_MapHeight; y++) {
                if (this.m_Map[x][y] != null) {
                    this.m_Map[x][y].completed = false;
                    editor.putBoolean("LevelDone_" + this.m_Map[x][y].charFieldId, false);
                    editor.putInt("BestMoves_" + this.m_Map[x][y].charFieldId, 9999);
                    editor.putInt("BestPickups_" + this.m_Map[x][y].charFieldId, 0);
                }
            }
        }
        editor.commit();
    }
}
