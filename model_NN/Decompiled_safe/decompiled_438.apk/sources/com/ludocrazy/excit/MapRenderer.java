package com.ludocrazy.excit;

import android.content.Context;
import com.ludocrazy.excit.MapLogic;
import com.ludocrazy.excit_demo.R;
import com.ludocrazy.tools.Font;
import com.ludocrazy.tools.GameView;
import com.ludocrazy.tools.GuiMesh;
import com.ludocrazy.tools.LogOutput;
import com.ludocrazy.tools.PhoneAbilities;
import com.ludocrazy.tools.Tools;
import com.ludocrazy.tools.UvCoords;
import com.ludocrazy.tools.Vector2Dint;
import java.util.HashMap;
import java.util.Map;
import javax.microedition.khronos.opengles.GL10;

public class MapRenderer extends GuiMesh {
    private static final float ITEM_PREVIEW_SPACING_HEIGHT = 0.7f;
    private static final float ITEM_PREVIEW_SPACING_WIDTH = 0.9f;
    public static final float LOGO_HEIGHT = 0.18554688f;
    private static final float LOGO_LEFT_SPACING_FACTOR = 0.1f;
    private static final float LOGO_SCALE = 32.0f;
    public static final float LOGO_START_U = 0.50097656f;
    public static final float LOGO_START_V = 0.81347656f;
    private static final float LOGO_TOP = 2.86f;
    public static final float LOGO_WIDTH = 0.4736328f;
    private static final float MAP_X_OFFSET = 18.1875f;
    private static final float MAP_Y_OFFSET = -9.35f;
    private static final int MEDALS_COUNT = 6;
    public static final UvCoords MEDAL_UV_SIZE = new UvCoords(0.047851562f, 0.046875f);
    public static final UvCoords MEDAL_UV_START = new UvCoords(0.10449219f, 0.9511719f);
    private static final float QUAD_Z = -0.15f;
    private static final float QUAD_Z_DIFFICULTY_OFFSET = 0.05f;
    private static final float QUAD_Z_TEXT_OFFSET = 0.25f;
    private static final float SCROLL_SPEED_FACTOR = 1.8f;
    private static final float m_MapScrollX_Min = -18.92f;
    private LogOutput DebugLogOutput = null;
    private float ITEM_SCREEN_HEIGHT = 5.0f;
    private float ITEM_SCREEN_WIDTH = 6.306667f;
    private Context m_Context = null;
    private Font m_Font = null;
    private float m_Highres = 0.0f;
    private Map<String, Integer> m_MapPreviewIndex = new HashMap();
    private float m_MapScrollX_Current = -1.1352f;
    private float m_MapScrollX_Max = 0.0f;
    private float m_MapScrollX_Target = m_MapScrollX_Min;
    private int m_Statistics_LevelsCompleted = 0;
    private int m_Statistics_LevelsVisible = 0;
    private int m_Statistics_MaxPossibleCompletion = 0;
    private int[] m_Statistics_Medal = new int[6];
    private int m_Statistics_TotalBestMoves = 0;
    private int m_Statistics_TotalCompletion = 0;
    private int m_Statistics_TotalPickupsCollected = 0;
    private Font.Text m_Text = null;

    public MapRenderer(LogOutput DEBUGLOG_to_use, PhoneAbilities phoneAbilities, Context pContext, Font pFont, boolean use_highres_font_colors) {
        super(DEBUGLOG_to_use, phoneAbilities);
        this.DebugLogOutput = DEBUGLOG_to_use;
        this.m_Context = pContext;
        this.USEVBOS = false;
        if (use_highres_font_colors) {
            this.m_Highres = 1.0f;
        } else {
            this.m_Highres = 0.0f;
        }
        this.m_Font = pFont;
        this.m_Text = this.m_Font.newText(this.DebugLogOutput, null, this.m_PhoneAbilities);
        this.m_MapPreviewIndex.put(new String("t_bas1"), new Integer(0));
        this.m_MapPreviewIndex.put(new String("e1o_r3"), new Integer(1));
        this.m_MapPreviewIndex.put(new String("t_bas4"), new Integer(2));
        this.m_MapPreviewIndex.put(new String("t_pick3"), new Integer(3));
        this.m_MapPreviewIndex.put(new String("e1o_j0"), new Integer(4));
        this.m_MapPreviewIndex.put(new String("t_pick2"), new Integer(5));
        this.m_MapPreviewIndex.put(new String("e2L_bn"), new Integer(6));
        this.m_MapPreviewIndex.put(new String("e2L_l5"), new Integer(7));
        this.m_MapPreviewIndex.put(new String("e2L_l9"), new Integer(8));
        this.m_MapPreviewIndex.put(new String("e1o_j1"), new Integer(9));
        this.m_MapPreviewIndex.put(new String("e1o_r4"), new Integer(10));
        this.m_MapPreviewIndex.put(new String("t_beam2"), new Integer(11));
        this.m_MapPreviewIndex.put(new String("e1o_j3"), new Integer(12));
        this.m_MapPreviewIndex.put(new String("e2o_fen"), new Integer(13));
        this.m_MapPreviewIndex.put(new String("e1o_r7"), new Integer(14));
        this.m_MapPreviewIndex.put(new String("e2o_t2"), new Integer(15));
        this.m_MapPreviewIndex.put(new String("e1o_j2"), new Integer(16));
        this.m_MapPreviewIndex.put(new String("e1L_k2"), new Integer(17));
        this.m_MapPreviewIndex.put(new String("t_deflect"), new Integer(18));
        this.m_MapPreviewIndex.put(new String("e1L_k6"), new Integer(19));
        this.m_MapPreviewIndex.put(new String("e2L_l6"), new Integer(20));
        this.m_MapPreviewIndex.put(new String("e2o_r14"), new Integer(21));
        this.m_MapPreviewIndex.put(new String("E2K_37"), new Integer(22));
        this.m_MapPreviewIndex.put(new String("e2L_e1b1"), new Integer(23));
        this.m_MapPreviewIndex.put(new String("E2K_38"), new Integer(24));
        this.m_MapPreviewIndex.put(new String("e2o_o9"), new Integer(25));
        this.m_MapPreviewIndex.put(new String("e2o_o6"), new Integer(26));
        this.m_MapPreviewIndex.put(new String("e2L_l7"), new Integer(27));
        this.m_MapPreviewIndex.put(new String("t_door3"), new Integer(28));
        this.m_MapPreviewIndex.put(new String("t_door1"), new Integer(29));
        this.m_MapPreviewIndex.put(new String("e1o_r8"), new Integer(30));
        this.m_MapPreviewIndex.put(new String("e2L_sci"), new Integer(31));
        this.m_MapPreviewIndex.put(new String("e2o_a8"), new Integer(32));
        this.m_MapPreviewIndex.put(new String("E2K_34"), new Integer(33));
        this.m_MapPreviewIndex.put(new String("e2L_cb"), new Integer(34));
        this.m_MapPreviewIndex.put(new String("E2K_50"), new Integer(35));
        this.m_MapPreviewIndex.put(new String("e2L_logo"), new Integer(36));
        this.m_MapPreviewIndex.put(new String("e2L_cc"), new Integer(37));
        this.m_MapPreviewIndex.put(new String("t_onew3"), new Integer(38));
        this.m_MapPreviewIndex.put(new String("e1L_f2"), new Integer(39));
        this.m_MapPreviewIndex.put(new String("t_onew1"), new Integer(40));
        this.m_MapPreviewIndex.put(new String("E2K_54"), new Integer(41));
        this.m_MapPreviewIndex.put(new String("E2K_57"), new Integer(42));
        this.m_MapPreviewIndex.put(new String("e1L_f3"), new Integer(43));
        this.m_MapPreviewIndex.put(new String("e1o_r16"), new Integer(44));
        this.m_MapPreviewIndex.put(new String("e1L_k5"), new Integer(45));
        this.m_MapPreviewIndex.put(new String("e2L_four"), new Integer(46));
        this.m_MapPreviewIndex.put(new String("e2L_ok"), new Integer(47));
        this.m_MapPreviewIndex.put(new String("E2K_60"), new Integer(48));
        this.m_MapPreviewIndex.put(new String("E2K_45"), new Integer(49));
        this.m_MapPreviewIndex.put(new String("e2L_lfive"), new Integer(50));
        this.m_MapPreviewIndex.put(new String("E2K_44"), new Integer(51));
        this.m_MapPreviewIndex.put(new String("t_arro3"), new Integer(52));
        this.m_MapPreviewIndex.put(new String("E2K_33"), new Integer(53));
        this.m_MapPreviewIndex.put(new String("t_arro2"), new Integer(54));
        this.m_MapPreviewIndex.put(new String("e1L_b1"), new Integer(55));
        this.m_MapPreviewIndex.put(new String("E2K_52"), new Integer(56));
        this.m_MapPreviewIndex.put(new String("e1L_b2"), new Integer(57));
        this.m_MapPreviewIndex.put(new String("e1L_b0"), new Integer(58));
        this.m_MapPreviewIndex.put(new String("E2K_32"), new Integer(59));
        this.m_MapPreviewIndex.put(new String("e1L_dl"), new Integer(60));
        this.m_MapPreviewIndex.put(new String("E2K_35"), new Integer(61));
        this.m_MapPreviewIndex.put(new String("E2K_46"), new Integer(62));
        this.m_MapPreviewIndex.put(new String("e2L_l8k"), new Integer(63));
        this.m_MapPreviewIndex.put(new String("e2L_c"), new Integer(64));
        this.m_MapPreviewIndex.put(new String("E2K_59"), new Integer(65));
        this.m_MapPreviewIndex.put(new String("e1L_k7"), new Integer(66));
        this.m_MapPreviewIndex.put(new String("e1L_n1"), new Integer(67));
        this.m_MapPreviewIndex.put(new String("E2K_36"), new Integer(68));
        this.m_MapPreviewIndex.put(new String("e1L_k1"), new Integer(69));
        this.m_MapPreviewIndex.put(new String("e2L_n61"), new Integer(70));
        this.m_MapPreviewIndex.put(new String("E2K_49"), new Integer(71));
        this.m_MapPreviewIndex.put(new String("e1L_f1"), new Integer(72));
        this.m_MapPreviewIndex.put(new String("e1L_k3"), new Integer(73));
        this.m_MapPreviewIndex.put(new String("E2K_31"), new Integer(74));
        this.m_MapPreviewIndex.put(new String("E2K_51"), new Integer(75));
        this.m_MapPreviewIndex.put(new String("e1L_k0"), new Integer(76));
        this.m_MapPreviewIndex.put(new String("E2K_39"), new Integer(77));
        this.m_MapPreviewIndex.put(new String("E2K_42"), new Integer(78));
        this.m_MapPreviewIndex.put(new String("E2K_47"), new Integer(79));
        this.m_MapPreviewIndex.put(new String("E2K_48"), new Integer(80));
        this.m_MapPreviewIndex.put(new String("E2K_40"), new Integer(81));
        this.m_MapPreviewIndex.put(new String("E2K_55"), new Integer(82));
        this.m_MapPreviewIndex.put(new String("E2K_56"), new Integer(83));
        this.m_MapPreviewIndex.put(new String("E2K_41"), new Integer(84));
        this.m_MapPreviewIndex.put(new String("E2K_53"), new Integer(85));
        this.m_MapPreviewIndex.put(new String("e2L_n62"), new Integer(86));
        this.m_MapPreviewIndex.put(new String("e1L_k4"), new Integer(87));
        this.m_MapPreviewIndex.put(new String("e2L_ca"), new Integer(88));
        this.m_MapPreviewIndex.put(new String("E2K_58"), new Integer(89));
        this.m_MapPreviewIndex.put(new String("E2K_43"), new Integer(90));
    }

    public void createMapGui(MapLogic loadedWorldmap, boolean mapDecoVisible, boolean achievments_layout) throws Exception {
        if (achievments_layout) {
            this.ITEM_SCREEN_WIDTH = 4.73f;
            this.ITEM_SCREEN_HEIGHT = 4.0f;
        }
        this.m_Statistics_LevelsCompleted = 0;
        this.m_Statistics_LevelsVisible = 0;
        this.m_Statistics_TotalCompletion = 0;
        this.m_Statistics_MaxPossibleCompletion = 0;
        for (int i = 0; i < 6; i++) {
            this.m_Statistics_Medal[i] = 0;
        }
        this.m_Statistics_TotalPickupsCollected = 0;
        this.m_Statistics_TotalBestMoves = 0;
        int mapHeight = loadedWorldmap.m_MapHeight;
        int mapWidth = loadedWorldmap.m_MapWidth;
        this.m_MapScrollX_Max = -0.7325001f - (((float) mapWidth) * this.ITEM_SCREEN_WIDTH);
        int i2 = 6 + 16;
        setupArraysQuadFaces((mapWidth * mapHeight * 22) + 1 + 70 + 100);
        int achievments_index = -1;
        for (int mx = 0; mx < mapWidth; mx++) {
            for (int my = 0; my < mapHeight; my++) {
                MapLogic.MapField field = loadedWorldmap.m_Map[mx][my];
                if (!(field == null || field.levelFilename == null)) {
                    int x = mx;
                    int y = my;
                    if (achievments_layout) {
                        x = (int) Math.floor(((double) achievments_index) / 5.0d);
                        if (x > -1) {
                            y = achievments_index - (x * 5);
                        } else {
                            y = 0;
                        }
                        achievments_index++;
                    }
                    this.DebugLogOutput.log(2, "MapRenderer.createMapGui() adding field:" + x + ", " + y);
                    GradeStruct grade = calculateGrade(field.completed, field.has_pickups, field.best_pickups, field.best_moves, field.solution_moves);
                    addMapField_previewQuad((float) x, (float) y, QUAD_Z, field, this.m_MapPreviewIndex.get(field.charFieldId).intValue());
                    if ((field.visible && mapDecoVisible) || (GameViewExcit.DEMOVERSION && !field.in_demo)) {
                        addMapField_difficultyQuads((float) x, (float) y, QUAD_Z, field);
                    }
                    if (field.visible) {
                        addMapField_solvedQuads((float) x, (float) y, QUAD_Z, field, grade, mapDecoVisible, achievments_layout);
                    }
                    sumStatistics(field, grade);
                }
            }
        }
        createLogoAreaQuads(achievments_layout);
        createStatisticsQuads(achievments_layout);
        convertArraysToBuffer();
    }

    private void sumStatistics(MapLogic.MapField field, GradeStruct grade) {
        if (field.completed) {
            this.m_Statistics_LevelsCompleted++;
        }
        if (field.visible) {
            this.m_Statistics_LevelsVisible++;
        }
        this.m_Statistics_TotalCompletion += grade.percentage;
        this.m_Statistics_MaxPossibleCompletion += 100;
        if (grade.level >= 0) {
            int[] iArr = this.m_Statistics_Medal;
            int i = grade.level;
            iArr[i] = iArr[i] + 1;
        }
        this.m_Statistics_TotalPickupsCollected += field.best_pickups;
        if (field.best_moves > 0 && field.best_moves < 9999) {
            this.m_Statistics_TotalBestMoves += field.best_moves;
        }
        this.DebugLogOutput.log(2, "level:" + field.levelFilename + " completed:" + field.completed + " percentage:" + grade.percentage);
    }

    private void createLogoAreaQuads(boolean achievments_layout) throws Exception {
        super.addQuad(1.515625f, LOGO_TOP, QUAD_Z, 16.671875f, 8.7975f, 0.50097656f, 0.99902344f, 0.9746094f, 0.81347656f);
        super.addColorForLastQuad(1.0f, 1.0f, 1.0f, 1.0f);
        float font_scale_normal = 0.025f;
        this.m_Text.setTextColor(0.0f, 0.0f, 0.0f, 1.0f);
        String textstring = this.m_Context.getResources().getString(R.string.sub_title);
        if (GameView.DEMOVERSION) {
            textstring = this.m_Context.getResources().getString(R.string.sub_title_demo);
        }
        this.m_Text.placeText_onGivenMesh(textstring, this, 15.9140625f, 2.56f, 0.25f, 15.15625f, 10.0f, 0.025f, Font.HorizontalAlign.Right, Font.VerticalAlign.Top);
        if (achievments_layout) {
            font_scale_normal = 0.035f;
            this.m_Text.placeText_onGivenMesh(this.m_Context.getResources().getString(R.string.achievments_website), this, 1.515625f, 9.86f, 0.25f, 15.15625f, 10.0f, 0.035f, Font.HorizontalAlign.Left, Font.VerticalAlign.Top);
        }
        if (GameViewExcit.DEMOVERSION) {
            String levels_not_in_demo = this.m_Context.getResources().getString(R.string.levels_not_in_demo);
            float text_x = (2.0f * this.ITEM_SCREEN_WIDTH) + MAP_X_OFFSET;
            float text_y = (3.22f * this.ITEM_SCREEN_HEIGHT) + MAP_Y_OFFSET;
            this.m_Text.setTextColor(1.0f, 0.6f, 0.0f, 1.0f);
            this.m_Text.placeText_onGivenMesh(levels_not_in_demo, this, text_x, text_y, 0.25f, 10.0f, 10.0f, font_scale_normal * 1.45f, Font.HorizontalAlign.Left, Font.VerticalAlign.Top);
        }
    }

    private void createStatisticsQuads(boolean achievments_layout) throws Exception {
        float text_x = 1.515625f;
        float text_y = 0.26f;
        if (achievments_layout) {
            text_y = 0.26f + 1.0f;
        }
        this.m_Text.setTextColor(1.0f, 1.0f, 1.0f, 1.0f);
        float total_completion = Tools.shortenFloatDigits_SLOW((((float) this.m_Statistics_TotalCompletion) / ((float) this.m_Statistics_MaxPossibleCompletion)) * 100.0f, 1);
        this.m_Text.placeText_onGivenMesh(String.valueOf(total_completion) + this.m_Context.getResources().getString(R.string.statistics_percentage_game_completed), this, 1.515625f, text_y, 0.25f, 10.0f, 10.0f, 0.07f, Font.HorizontalAlign.Left, Font.VerticalAlign.Top);
        float text_y2 = text_y - 2.2f;
        if (this.m_Statistics_TotalPickupsCollected > 0) {
            text_x = this.m_Text.placeText_onGivenMesh(Integer.toString(this.m_Statistics_TotalPickupsCollected), this, 1.515625f, text_y2, 0.25f, 12.613334f, 5.5f, 0.15f, Font.HorizontalAlign.Left, Font.VerticalAlign.Top);
        }
        this.m_Text.setTextColor(this.m_Highres * 1.0f, this.m_Highres * 1.0f, this.m_Highres * 1.0f, 1.0f);
        float text_y3 = text_y2 - 0.3f;
        float f = text_x;
        float f2 = text_y3;
        this.m_Text.placeText_onGivenMesh(String.valueOf(this.m_Context.getResources().getString(R.string.statistics_levels)) + " " + this.m_Statistics_LevelsVisible, this, f, f2, 0.25f, 10.0f, 10.0f, 0.035f, Font.HorizontalAlign.Left, Font.VerticalAlign.Top);
        float text_y4 = text_y3 - 1.1f;
        float f3 = text_x;
        float f4 = text_y4;
        this.m_Text.placeText_onGivenMesh(String.valueOf(this.m_Context.getResources().getString(R.string.statistics_levels_solved)) + " " + this.m_Statistics_LevelsCompleted, this, f3, f4, 0.25f, 10.0f, 10.0f, 0.035f, Font.HorizontalAlign.Left, Font.VerticalAlign.Top);
        float text_y5 = text_y4 - 1.1f;
        float f5 = text_x;
        float f6 = text_y5;
        this.m_Text.placeText_onGivenMesh(String.valueOf(this.m_Context.getResources().getString(R.string.statistics_moves)) + " " + this.m_Statistics_TotalBestMoves, this, f5, f6, 0.25f, 10.0f, 10.0f, 0.035f, Font.HorizontalAlign.Left, Font.VerticalAlign.Top);
        float text_x2 = 1.515625f;
        if (this.m_Statistics_TotalPickupsCollected > 0) {
            text_y5 -= 1.0f;
            this.m_Text.placeText_onGivenMesh(this.m_Context.getResources().getString(R.string.statistics_pickups), this, 1.515625f, text_y5, 0.25f, 10.0f, 10.0f, 0.035f, Font.HorizontalAlign.Left, Font.VerticalAlign.Top);
        }
        float text_y6 = text_y5 - 2.0f;
        float medal_size = this.ITEM_SCREEN_WIDTH * ITEM_PREVIEW_SPACING_WIDTH * 0.15f;
        for (int i = 0; i < 6; i++) {
            if (this.m_Statistics_Medal[i] > 0) {
                UvCoords uvCoords = new UvCoords(0.10449219f + (0.047851562f * ((float) i)), 0.9511719f);
                UvCoords uvCoords2 = new UvCoords(uvCoords.u + 0.047851562f, uvCoords.v + 0.046875f);
                super.addQuad(text_x2, text_y6, 0.25f, text_x2 + medal_size, text_y6 - medal_size, uvCoords.u, uvCoords.v, uvCoords2.u, uvCoords2.v);
                super.addColorForLastQuad(1.0f, 1.0f, 1.0f, 1.0f);
                text_x2 = this.m_Text.placeText_onGivenMesh(Integer.toString(this.m_Statistics_Medal[i]), this, text_x2 + (medal_size * 1.5f), text_y6, 0.25f, 12.613334f, 5.5f, 0.035f, Font.HorizontalAlign.Left, Font.VerticalAlign.Top) + medal_size;
                if (achievments_layout && text_x2 > 15.0f * medal_size) {
                    text_x2 = 1.515625f;
                    text_y6 -= 1.5f * medal_size;
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void addMapField_previewQuad(float offsetX, float offsetY, float offsetZ, MapLogic.MapField field, int preview_index) throws Exception {
        float offsetX2 = (this.ITEM_SCREEN_WIDTH * offsetX) + MAP_X_OFFSET;
        float offsetY2 = (this.ITEM_SCREEN_HEIGHT * offsetY) + MAP_Y_OFFSET;
        int previews_per_row = GameViewExcit.TEXTUREIDGUI_PIXEL_SQUARE_SIZE / (66 + 3);
        int preview_y = preview_index / previews_per_row;
        int i = 66 + 3;
        int i2 = 60 + 3;
        int i3 = 1 * 2;
        int i4 = 1 * 2;
        UvCoords uvCoords = new UvCoords(((float) ((((preview_index - (preview_y * previews_per_row)) * 69) + 3) - 1)) / 1024.0f, ((float) (((preview_y * 63) + 386) - 1)) / 1024.0f);
        UvCoords uvCoords2 = new UvCoords(uvCoords.u + (((float) (66 + 2)) / 1024.0f), uvCoords.v + (((float) (60 + 2)) / 1024.0f));
        super.addQuad(offsetX2, offsetY2 + (this.ITEM_SCREEN_HEIGHT * 0.3f), offsetZ, offsetX2 + (this.ITEM_SCREEN_WIDTH * ITEM_PREVIEW_SPACING_WIDTH), offsetY2 + this.ITEM_SCREEN_HEIGHT, uvCoords.u, uvCoords2.v, uvCoords2.u, uvCoords.v);
        float alpha = 1.0f;
        if (!field.visible) {
            alpha = 0.25f;
        }
        if (!GameViewExcit.DEMOVERSION || field.in_demo) {
            super.addColorForLastQuad(1.0f, 1.0f, 1.0f, alpha);
        } else {
            super.addColorForLastQuad(1.0f, 0.6f, 0.0f, 0.5f);
        }
    }

    public void addMapField_difficultyQuads(float offsetX, float offsetY, float offsetZ, MapLogic.MapField field) throws Exception {
        float offsetX2 = (this.ITEM_SCREEN_WIDTH * offsetX) + MAP_X_OFFSET;
        float offsetY2 = (this.ITEM_SCREEN_HEIGHT * offsetY) + MAP_Y_OFFSET;
        UvCoords uvCoords = new UvCoords(0.064453125f, 0.9589844f);
        UvCoords diff_end = new UvCoords(uvCoords.u + 0.041015625f, uvCoords.v + 0.0390625f);
        float size = this.ITEM_SCREEN_HEIGHT * 0.24f;
        float top = offsetY2 + this.ITEM_SCREEN_HEIGHT;
        float bottom = top - size;
        float left = ((this.ITEM_SCREEN_WIDTH * ITEM_PREVIEW_SPACING_WIDTH) + offsetX2) - size;
        for (int i = field.difficulty_icon - 1; i >= 0; i--) {
            float left_start = left - ((0.4f * size) * ((float) i));
            offsetZ += 0.05f;
            super.addQuad(left_start, top, offsetZ, left_start + size, bottom, uvCoords.u, uvCoords.v, diff_end.u, diff_end.v);
            super.addColorForLastQuad(1.0f, 1.0f, 1.0f, 1.0f);
        }
    }

    public void addMapField_solvedQuads(float offsetX, float offsetY, float offsetZ, MapLogic.MapField field, GradeStruct grade, boolean mapDecoVisible, boolean achievments_layout) throws Exception {
        float offsetX2 = (this.ITEM_SCREEN_WIDTH * offsetX) + MAP_X_OFFSET;
        float offsetY2 = (this.ITEM_SCREEN_HEIGHT * offsetY) + MAP_Y_OFFSET;
        float offsetZ2 = offsetZ + 0.25f;
        if (!achievments_layout && (!field.completed || (field.completed && mapDecoVisible))) {
            this.m_Text.setTextColor(this.m_Highres * 0.5f, this.m_Highres * 0.5f, this.m_Highres * 0.5f, 1.0f);
            String textstring = field.level_name;
            if (field.completed && mapDecoVisible) {
                textstring = this.m_Context.getResources().getString(R.string.statistics_level_percentage);
            }
            this.m_Text.placeText_onGivenMesh(textstring, this, offsetX2 + 0.0f, offsetY2 + (this.ITEM_SCREEN_HEIGHT * 0.28f), offsetZ2, this.ITEM_SCREEN_WIDTH, this.ITEM_SCREEN_HEIGHT, 0.025f, Font.HorizontalAlign.Left, Font.VerticalAlign.Top);
        }
        if (field.completed) {
            this.m_Text.setTextColor(this.m_Highres * 1.0f, this.m_Highres * 1.0f, this.m_Highres * 1.0f, 1.0f);
            float f = offsetZ2;
            this.m_Text.placeText_onGivenMesh(String.valueOf(grade.percentage) + "%", this, offsetX2 + 0.0f + (this.ITEM_SCREEN_WIDTH * ITEM_PREVIEW_SPACING_WIDTH * 0.97f), offsetY2 + (this.ITEM_SCREEN_HEIGHT * 0.28f), f, this.ITEM_SCREEN_WIDTH, this.ITEM_SCREEN_HEIGHT, 0.035f, Font.HorizontalAlign.Right, Font.VerticalAlign.Top);
            if (mapDecoVisible) {
                float size = this.ITEM_SCREEN_WIDTH * ITEM_PREVIEW_SPACING_WIDTH * 0.4f;
                float left = offsetX2 + (this.ITEM_SCREEN_WIDTH * ITEM_PREVIEW_SPACING_WIDTH * 0.03f);
                float top = offsetY2 + (this.ITEM_SCREEN_HEIGHT * 0.7f * 0.75f);
                float medal_start_u = MEDAL_UV_START.u + (0.047851562f * ((float) grade.level));
                super.addQuad(left, top, offsetZ2 - 0.05f, left + size, top + size, medal_start_u, MEDAL_UV_START.v + MEDAL_UV_SIZE.v, medal_start_u + MEDAL_UV_SIZE.u, MEDAL_UV_START.v);
                super.addColorForLastQuad(1.0f, 1.0f, 1.0f, 1.0f);
                float text_x_centered_medal = ((0.5f * size) + left) - grade.letter_pos;
                float text_y = top + (this.ITEM_SCREEN_HEIGHT * 0.16f);
                this.m_Text.setTextColor(0.0f, 0.0f, 0.0f, 1.0f);
                this.m_Text.placeText_onGivenMesh(grade.letter, this, text_x_centered_medal, text_y, offsetZ2, 10.0f, 10.0f, 0.06f, Font.HorizontalAlign.Left, Font.VerticalAlign.Center);
            }
        }
    }

    protected class GradeStruct {
        String letter;
        float letter_pos;
        int level;
        int percentage;

        public GradeStruct(int _level, int _percentage, String _letter, float _letter_pos) {
            this.level = _level;
            this.percentage = _percentage;
            this.letter = _letter;
            this.letter_pos = _letter_pos;
        }
    }

    /* access modifiers changed from: package-private */
    public GradeStruct calculateGrade(boolean completed, boolean has_pickups, int best_pickups, int best_moves, int solution_moves) {
        if (!completed) {
            return new GradeStruct(-1, 0, "F", 0.0f);
        }
        if (!has_pickups) {
            if (best_moves > solution_moves) {
                return new GradeStruct(3, 100, "A", this.ITEM_SCREEN_WIDTH * 0.1f);
            }
            return new GradeStruct(4, 100, "A", 0.105f * this.ITEM_SCREEN_WIDTH);
        } else if (best_pickups == 0) {
            return new GradeStruct(0, 25, "D", this.ITEM_SCREEN_WIDTH * 0.09f);
        } else {
            if (best_pickups == 1) {
                return new GradeStruct(1, 50, "C", this.ITEM_SCREEN_WIDTH * 0.11f);
            }
            if (best_pickups == 2) {
                return new GradeStruct(2, 75, "B", this.ITEM_SCREEN_WIDTH * 0.11f);
            }
            if (best_moves > solution_moves + 3) {
                return new GradeStruct(3, 100, "A", this.ITEM_SCREEN_WIDTH * 0.095f);
            }
            if (best_moves >= solution_moves) {
                return new GradeStruct(4, 100, "A", this.ITEM_SCREEN_WIDTH * 0.108f);
            }
            return new GradeStruct(5, 101, "A", 0.105f * this.ITEM_SCREEN_WIDTH);
        }
    }

    public Vector2Dint getTouchedMapField(float x_screen_percentage, float y_screen_percentage) {
        float x_screen_percentage2 = x_screen_percentage * (37.84f / this.ITEM_SCREEN_WIDTH);
        float y_screen_percentage2 = (1.0f - y_screen_percentage) * (22.0f / this.ITEM_SCREEN_HEIGHT);
        float x_screen_percentage3 = (x_screen_percentage2 - (MAP_X_OFFSET / this.ITEM_SCREEN_WIDTH)) - ((this.m_MapScrollX_Current - m_MapScrollX_Min) / this.ITEM_SCREEN_WIDTH);
        this.DebugLogOutput.log(2, "converted into fields precentage x:" + x_screen_percentage3 + ", y:" + y_screen_percentage2);
        return new Vector2Dint((int) x_screen_percentage3, 3 - ((int) y_screen_percentage2));
    }

    public void scrollMap(float dx_screen_percentage, float dy_screen_percentage) {
        this.m_MapScrollX_Target += 37.84f * dx_screen_percentage * SCROLL_SPEED_FACTOR;
    }

    public int draw(GL10 gl, int appearCount) throws Exception {
        this.m_MapScrollX_Current = (this.m_MapScrollX_Current * ITEM_PREVIEW_SPACING_WIDTH) + (0.1f * this.m_MapScrollX_Target);
        if (this.m_MapScrollX_Current > m_MapScrollX_Min) {
            this.m_MapScrollX_Target = m_MapScrollX_Min;
        } else if (this.m_MapScrollX_Current < this.m_MapScrollX_Max) {
            this.m_MapScrollX_Target = this.m_MapScrollX_Max;
        }
        gl.glTranslatef(this.m_MapScrollX_Current, 0.0f, 0.0f);
        int retval = super.draw(gl, appearCount);
        gl.glTranslatef(-this.m_MapScrollX_Current, 0.0f, 0.0f);
        return retval;
    }

    public int draw(GL10 gl) throws Exception {
        return draw(gl, this.m_currentIndex);
    }

    public int getCurrentDrawCount() {
        return super.getVerticesCount();
    }
}
