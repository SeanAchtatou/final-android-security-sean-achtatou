package com.ludocrazy.excit;

import android.app.Activity;
import android.content.SharedPreferences;
import android.view.KeyEvent;
import android.view.MotionEvent;
import com.ludocrazy.excit.MapLogic;
import com.ludocrazy.excit_demo.R;
import com.ludocrazy.tools.BasicMesh;
import com.ludocrazy.tools.CameraOrtho;
import com.ludocrazy.tools.ColorFloats;
import com.ludocrazy.tools.Coords;
import com.ludocrazy.tools.ErrorOutput;
import com.ludocrazy.tools.Font;
import com.ludocrazy.tools.GameView;
import com.ludocrazy.tools.GuiBase;
import com.ludocrazy.tools.GuiButton;
import com.ludocrazy.tools.GuiGroup;
import com.ludocrazy.tools.GuiMessageBox;
import com.ludocrazy.tools.LogOutput;
import com.ludocrazy.tools.MediaManager;
import com.ludocrazy.tools.PhoneAbilities;
import com.ludocrazy.tools.Sproxel;
import com.ludocrazy.tools.StopWatch;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

public class GameViewExcit extends GameView {
    private static final int INTENDEDPOINTSPRITESIZE = 7;
    public static String MAPFILENAME = "level_map.csv";
    private static final float MENUALPHA = 0.35f;
    private static final float MENU_Z = 0.5f;
    public static float PIXEL_HEIGHT = 0.09166667f;
    public static float PIXEL_WIDTH = 0.11825f;
    public static final float PLAYFIELDOFFSET_X = -17.274782f;
    public static final float PLAYFIELDOFFSET_Y = -9.0f;
    public static final int PLAYFIELD_HEIGHT = 20;
    public static final float PLAYFIELD_LAYOUT_HEIGHT = 22.0f;
    public static final float PLAYFIELD_LAYOUT_WIDTH = 23.0f;
    public static final int PLAYFIELD_WIDTH = 22;
    public static final float SCREENFIELDSHEIGHT = 22.0f;
    public static final float SCREENFIELDSWIDTH = 37.84f;
    public static String SHAREDPREFSNAME = "com.ludocrazy.excit";
    private static final int TEXTUREIDGUI = 2130837514;
    public static final int TEXTUREIDGUI_PIXEL_SQUARE_SIZE = 1024;
    private static final int TEXTUREIDLEVEL = 2130837515;
    private int MAX_LOADING_STEPS = 19;
    private int POINTSPRITESIZE = 7;
    private GuiButton m_ButtonControls_Drag = null;
    /* access modifiers changed from: private */
    public GuiButton m_ButtonControls_TouchPosition = null;
    /* access modifiers changed from: private */
    public GuiButton m_ButtonControls_TouchSides = null;
    /* access modifiers changed from: private */
    public float m_ButtonLeft = 0.0f;
    /* access modifiers changed from: private */
    public float m_ButtonTop = 0.0f;
    /* access modifiers changed from: private */
    public CameraOrtho m_Camera = new CameraOrtho();
    private boolean m_ClickDown = false;
    /* access modifiers changed from: private */
    public boolean m_ControlsEnabled_Cursor = true;
    /* access modifiers changed from: private */
    public boolean m_ControlsEnabled_Drag = true;
    /* access modifiers changed from: private */
    public boolean m_ControlsEnabled_IJKL = true;
    private boolean m_ControlsEnabled_Shake = false;
    /* access modifiers changed from: private */
    public boolean m_ControlsEnabled_TouchPosition = false;
    /* access modifiers changed from: private */
    public boolean m_ControlsEnabled_TouchSides = false;
    /* access modifiers changed from: private */
    public boolean m_ControlsEnabled_WASD = true;
    private int m_Controls_DoingSensorMove = 0;
    private int m_CurrentRulesNumber = 0;
    private int m_DisplayingOutroText = 0;
    private int m_DoubleBufferReDrawNeeded = -2;
    /* access modifiers changed from: private */
    public Font m_Font = null;
    /* access modifiers changed from: private */
    public boolean m_ForceNewMap = false;
    private float m_FramesPerSecondAverage = 0.0f;
    /* access modifiers changed from: private */
    public GameLevel m_GameLevel = null;
    /* access modifiers changed from: private */
    public GameMap m_GameMap = null;
    /* access modifiers changed from: private */
    public GuiBase m_GuiBaseRoot = null;
    /* access modifiers changed from: private */
    public GuiGroup m_GuiGroupIntro = null;
    /* access modifiers changed from: private */
    public GuiGroup m_GuiGroupMainLevel = null;
    /* access modifiers changed from: private */
    public GuiGroup m_GuiGroupMainMap = null;
    private GuiGroup m_GuiGroupRulesBasics = null;
    private GuiGroup m_GuiGroupRulesMedals = null;
    /* access modifiers changed from: private */
    public GuiGroup m_GuiGroupSettings = null;
    private boolean m_GuiMessageBoxIconNeedsRedraw = true;
    private boolean m_Highres1024orMore = false;
    private long m_LastFrameTimeMilliSeconds = System.currentTimeMillis();
    private long m_LastOutputMilliSeconds = 0;
    private int m_LastTimeParticlesDrawn = -1;
    private boolean m_LeaveLevelClicked = false;
    private boolean m_LeaveLevelNOWclicked = false;
    public GuiButton m_LogoButton = null;
    /* access modifiers changed from: private */
    public boolean m_MapDecoVisible = true;
    private MapRenderer m_MapRenderer = null;
    private boolean m_MapVisible = true;
    /* access modifiers changed from: private */
    public int m_ResetAllLevels = 0;
    private GuiButton m_SolutionButton = null;
    private GuiMessageBox m_StoryMessageBox = null;
    private boolean m_TexturesConvertTo16Bit = true;
    private float oldX = 0.0f;
    private float oldY = 0.0f;

    public GameViewExcit(Activity activity_context, MediaManager mm, LogOutput debug_log_output, boolean is_demo_version) {
        super(activity_context, mm, debug_log_output, is_demo_version);
        if (is_demo_version) {
            MAPFILENAME = "level_map.csv";
            SHAREDPREFSNAME = "com.ludocrazy.excit_demo";
        }
        try {
            this.m_Camera.setOrthoCamera_ScreenSize(37.84f, 22.0f);
            this.m_Camera.setOrthoCamera_initialPos(0.0f, 66.0f, 0.0f, 46.0f);
            SharedPreferences prefs = this.m_ParentActivityAndContext.getSharedPreferences(SHAREDPREFSNAME, 1);
            startLogoSoundEffect(prefs.getInt("MusicVolume", 1), getResources().getString(R.string.music_logo_sound));
            this.m_MapDecoVisible = prefs.getBoolean("MapDecoVisible", true);
            this.m_ControlsEnabled_Cursor = prefs.getBoolean("ControlsEnabled_Cursor", true);
            this.m_ControlsEnabled_WASD = prefs.getBoolean("ControlsEnabled_WASD", true);
            this.m_ControlsEnabled_IJKL = prefs.getBoolean("ControlsEnabled_IJKL", true);
            this.m_ControlsEnabled_Drag = prefs.getBoolean("ControlsEnabled_Drag", true);
            this.m_ControlsEnabled_TouchSides = prefs.getBoolean("ControlsEnabled_TouchSides", false);
            this.m_ControlsEnabled_TouchPosition = prefs.getBoolean("ControlsEnabled_TouchPosition", false);
            this.m_ControlsEnabled_Shake = prefs.getBoolean("ControlsEnabled_Shake", false);
        } catch (Exception e) {
            new ErrorOutput("GameView::GameView()", "Exception", e);
        }
    }

    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        try {
            this.m_PhoneAbilities.determineGLabilities(gl, getWidth(), getHeight());
            gl.glEnable(3553);
            gl.glDisable(3024);
            gl.glDisable(2884);
            gl.glClearDepthf(1.0f);
            gl.glEnable(2929);
            gl.glDepthFunc(515);
            gl.glDepthMask(true);
            gl.glEnable(3042);
            gl.glBlendFunc(770, 771);
            gl.glDisable(3008);
            gl.glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
            gl.glClearColor(1.0f, 1.0f, 1.0f, 0.0f);
            if (this.m_AllHasBeenLoaded) {
                this.m_TextureManager.addTexture(gl, R.drawable.texture_splotch_no_alpha, false, false, this.m_TexturesConvertTo16Bit);
                this.m_TextureManager.addTexture(gl, R.drawable.texture_gui, true, true, this.m_TexturesConvertTo16Bit);
                this.m_Font.onResumeRerenderGlyphsIntoTexture(gl);
                setupGui();
                onResumeAfterOnSurfaceCreated();
            }
            checkForGlErrors(gl, "GameView.onSurfaceCreated() reached end.");
        } catch (Exception e) {
            new ErrorOutput("GameView::onSurfaceCreated()", "Exception", e);
        }
    }

    public void LoadingFunction(GL10 gl) throws Exception {
        this.m_CurrentLoadingStep++;
        switch (this.m_CurrentLoadingStep) {
            case 1:
                this.m_DynamicGuiMesh = new BasicMesh(this.DebugLogOutput, this.m_PhoneAbilities);
                this.m_DynamicGuiMesh.setupArraysTriangleFaces(250);
                this.m_DynamicGuiMesh.reset(true);
                return;
            case PhoneAbilities.ABILITY_GLUTILS_TEXSUBIMAGE2D_NONSQUARE /*2*/:
                createLogo(3.0f, 19.8f);
                return;
            case PhoneAbilities.ABILITY_VBO_GL11 /*3*/:
                this.m_ParticleManager.LoadEffectFileFromRes(R.drawable.effect_cherry_blossoms);
                this.m_ParticleManager.LoadEffectFileFromRes(R.drawable.logo_effect_fire);
                this.m_ParticleManager.LoadEffectFileFromRes(R.drawable.logo_effect_rain);
                this.m_ParticleManager.LoadEffectFileFromRes(R.drawable.effect_outro);
                return;
            case 4:
                this.m_ParticleManager.LoadEffectFileFromRes(R.drawable.beamer_outline);
                this.m_ParticleManager.LoadEffectFileFromRes(R.drawable.beamer_player);
                this.m_ParticleManager.LoadEffectFileFromRes(R.drawable.speedup_player);
                this.m_ParticleManager.LoadEffectFileFromRes(R.drawable.speedup_wallhit);
                return;
            case PhoneAbilities.ABILITY_FRAMEBUFFER_OES /*5*/:
                this.m_TextureManager.addTexture(gl, R.drawable.texture_splotch_no_alpha, false, false, this.m_TexturesConvertTo16Bit);
                return;
            case PhoneAbilities.ABILITY_POINT_SIZE_ARRAY_OES /*6*/:
                this.m_TextureManager.addTexture(gl, R.drawable.texture_gui, true, true, this.m_TexturesConvertTo16Bit);
                return;
            case 7:
                prepareFont(gl);
                return;
            case 8:
                preCreateFontGlyphs(0.1f, 0.2f);
                return;
            case 9:
                preCreateFontGlyphs(0.2f, 0.3f);
                return;
            case Sproxel.SPROXELS_PER_FIELD /*10*/:
                preCreateFontGlyphs(0.3f, 0.4f);
                return;
            case 11:
                preCreateFontGlyphs(0.4f, 0.5f);
                return;
            case 12:
                preCreateFontGlyphs(0.5f, 0.6f);
                return;
            case 13:
                preCreateFontGlyphs(0.6f, 0.7f);
                return;
            case 14:
                preCreateFontGlyphs(0.7f, 0.8f);
                return;
            case 15:
                preCreateFontGlyphs(0.8f, 0.9f);
                return;
            case 16:
                preCreateFontGlyphs(0.9f, 1.0f);
                return;
            case 17:
                this.m_MapRenderer = new MapRenderer(this.DebugLogOutput, this.m_PhoneAbilities, this.m_ParentActivityAndContext, this.m_Font, this.m_Highres1024orMore);
                return;
            case 18:
                setupGui();
                return;
            case 19:
                onResumeAfterOnSurfaceCreated();
                this.m_AllHasBeenLoaded = true;
                return;
            default:
                return;
        }
    }

    private void prepareFont(GL10 gl) throws Exception {
        this.m_Font = new Font(this.m_PhoneAbilities, TEXTUREIDGUI_PIXEL_SQUARE_SIZE, gl, this.m_ParentActivityAndContext.getAssets(), "Sansation_Regular.ttf", 53, 30, Font.FontStyle.Plain, this.m_TextureManager.getTextureHandle(R.drawable.texture_gui), 0, 1);
    }

    private void preCreateFontGlyphs(float start_percentage, float end_percentage) {
        StopWatch stopwatch = new StopWatch();
        String glyphs = " abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789,.:!()?%".substring((int) (((float) " abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789,.:!()?%".length()) * start_percentage), (int) (((float) " abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789,.:!()?%".length()) * end_percentage));
        this.m_Font.preCreateGlyphsFromString(glyphs);
        long duration = stopwatch.getElapsedMilliSeconds();
        if (duration > 100) {
            this.DebugLogOutput.log(1, "performance: precreate font glyphs for: __" + glyphs + "__ took " + duration);
        }
    }

    private void setupGui() {
        try {
            this.m_GuiBaseRoot = new GuiBase();
            this.m_GuiBaseRoot.setAlwaysVisible(true);
            this.m_GuiBaseRoot.setBasisTextScale(0.035f);
            this.m_ButtonLeft = 13.262857f;
            this.m_GuiBaseRoot.addChild(new GuiButton() {
                {
                    LogOutput logOutput = GameViewExcit.this.DebugLogOutput;
                    PhoneAbilities phoneAbilities = GameViewExcit.this.m_PhoneAbilities;
                    Font access$0 = GameViewExcit.this.m_Font;
                    GuiBase access$1 = GameViewExcit.this.m_GuiBaseRoot;
                    float access$2 = GameViewExcit.this.m_ButtonLeft;
                    addTextToStack(GameViewExcit.this.getResources().getString(R.string.button_label_scroll_story));
                    setAlwaysVisible(true);
                }

                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: com.ludocrazy.tools.MediaManager.playStreamedMusic(java.lang.String, boolean, float):void
                 arg types: [java.lang.String, int, int]
                 candidates:
                  com.ludocrazy.tools.MediaManager.playStreamedMusic(int, boolean, float):void
                  com.ludocrazy.tools.MediaManager.playStreamedMusic(java.lang.String, boolean, float):void */
                public void handleTouchUp() {
                    GameViewExcit.this.m_Camera.setOrthoCamera_TargetScreen(0, 1);
                    if (GameViewExcit.this.m_ParentActivityAndContext.getSharedPreferences(GameViewExcit.SHAREDPREFSNAME, 1).getInt("MusicVolume", 1) > 0) {
                        GameViewExcit.this.m_pMediaManager.playStreamedMusic(GameViewExcit.this.getResources().getString(R.string.music_menu), false, 1.0f);
                    }
                }
            });
            this.m_GuiGroupIntro = new GuiGroup(this.DebugLogOutput, this.m_PhoneAbilities, 1024.0f, null, this.m_GuiBaseRoot, -18.92f, 12.0f, 37.84f, 22.0f, 0.5f, MENUALPHA, false);
            this.m_GuiBaseRoot.addChild(this.m_GuiGroupIntro);
            this.m_GuiGroupRulesBasics = new GuiGroup(this.DebugLogOutput, this.m_PhoneAbilities, 1024.0f, null, this.m_GuiBaseRoot, 0.0f, 0.0f, 37.84f, 22.0f, 0.5f, MENUALPHA, false);
            this.m_GuiBaseRoot.addChild(this.m_GuiGroupRulesBasics);
            this.m_GuiGroupRulesMedals = new GuiGroup(this.DebugLogOutput, this.m_PhoneAbilities, 1024.0f, null, this.m_GuiBaseRoot, 0.0f, 0.0f, 37.84f, 22.0f, 0.5f, MENUALPHA, false);
            this.m_GuiBaseRoot.addChild(this.m_GuiGroupRulesMedals);
            this.m_ButtonLeft = 32.182858f;
            this.m_GuiGroupIntro.addChild(new GuiButton() {
                {
                    LogOutput logOutput = GameViewExcit.this.DebugLogOutput;
                    PhoneAbilities phoneAbilities = GameViewExcit.this.m_PhoneAbilities;
                    Font access$0 = GameViewExcit.this.m_Font;
                    GuiGroup access$6 = GameViewExcit.this.m_GuiGroupIntro;
                    float access$2 = GameViewExcit.this.m_ButtonLeft;
                    addTextToStack(GameViewExcit.this.getResources().getString(R.string.button_label_scroll_story));
                }

                public void handleTouchUp() {
                    GameViewExcit.this.m_Camera.setOrthoCamera_TargetScreen(0, 0);
                }
            });
            this.m_ButtonLeft = this.m_ButtonLeft - 8.014285f;
            this.m_GuiGroupRulesBasics.addChild(new GuiButton() {
                {
                    LogOutput logOutput = GameViewExcit.this.DebugLogOutput;
                    PhoneAbilities phoneAbilities = GameViewExcit.this.m_PhoneAbilities;
                    Font access$0 = GameViewExcit.this.m_Font;
                    GuiGroup access$6 = GameViewExcit.this.m_GuiGroupIntro;
                    float access$2 = GameViewExcit.this.m_ButtonLeft;
                    addTextToStack(GameViewExcit.this.getResources().getString(R.string.button_label_scroll_help2));
                }

                public void handleTouchUp() {
                    GameViewExcit.this.showRulesPage(1);
                }
            });
            this.m_StoryMessageBox = new GuiMessageBox() {
                {
                    Activity access$4 = GameViewExcit.this.m_ParentActivityAndContext;
                    LogOutput logOutput = GameViewExcit.this.DebugLogOutput;
                    PhoneAbilities phoneAbilities = GameViewExcit.this.m_PhoneAbilities;
                    Font access$0 = GameViewExcit.this.m_Font;
                    GuiGroup access$6 = GameViewExcit.this.m_GuiGroupIntro;
                }
            };
            showRulesPage(0);
            this.m_GuiGroupIntro.addChild(this.m_StoryMessageBox);
            GuiGroup excit_logo_image = new GuiGroup(this.DebugLogOutput, this.m_PhoneAbilities, 1024.0f, null, this.m_GuiGroupIntro, 22.704f, 15.4f, 13.2439995f, 0.39175257f * 13.2439995f, 1.25f, 1.0f, true);
            excit_logo_image.setupBackgroundImage(513.0f, 833.0f, 485.0f, 190.0f, true, new ColorFloats(1.0f, 1.0f, 1.0f, 1.0f));
            this.m_GuiGroupRulesBasics.addChild(excit_logo_image);
            GuiGroup exit_example_image = new GuiGroup(this.DebugLogOutput, this.m_PhoneAbilities, 1024.0f, null, this.m_GuiGroupIntro, 29.136799f, 4.4f, 5.676f, 5.676f * 0.5833333f, 1.25f, 1.0f, true);
            exit_example_image.setupBackgroundImage(363.0f, 836.0f, 144.0f, 84.0f, true, new ColorFloats(1.0f, 1.0f, 1.0f, 1.0f));
            this.m_GuiGroupRulesBasics.addChild(exit_example_image);
            GuiGroup cursor_example_image = new GuiGroup(this.DebugLogOutput, this.m_PhoneAbilities, 1024.0f, null, this.m_GuiGroupIntro, 22.704f, 4.62f, 5.2976003f, 5.2976003f * (41.0f / 84.0f), 1.25f, 1.0f, true);
            cursor_example_image.setupBackgroundImage(498.0f, 777.0f, 84.0f, 41.0f, true, new ColorFloats(1.0f, 1.0f, 1.0f, 1.0f));
            this.m_GuiGroupRulesBasics.addChild(cursor_example_image);
            for (int y = 0; y < 2; y++) {
                for (int x = 0; x < 5; x++) {
                    int[] pixels = LevelObjectPickup.getRandomFormula();
                    GuiGroup formula_image = new GuiGroup(this.DebugLogOutput, this.m_PhoneAbilities, 1024.0f, null, this.m_GuiGroupIntro, 37.84f * ((0.08f * ((float) x)) + 0.55f), 22.0f * (0.88f - (0.1f * ((float) y))), 2.2704f, 2.2704f * (((float) pixels[3]) / ((float) pixels[2])), 1.25f, 1.0f, true);
                    formula_image.setupBackgroundImage((float) pixels[0], (float) pixels[1], (float) pixels[2], (float) pixels[3], true, new ColorFloats(1.0f, 1.0f, 1.0f, 1.0f));
                    this.m_GuiGroupRulesMedals.addChild(formula_image);
                }
            }
            float image_height = 1.8920001f * (MapRenderer.MEDAL_UV_SIZE.v / MapRenderer.MEDAL_UV_SIZE.u);
            for (int i = 0; i < 6; i++) {
                GuiGroup medal_image = new GuiGroup(this.DebugLogOutput, this.m_PhoneAbilities, 1024.0f, null, this.m_GuiGroupIntro, 37.84f * ((0.06f * ((float) i)) + 0.55f), 22.0f * (0.48f - (0.06f * ((float) i))), 1.8920001f, image_height, 1.25f, 1.0f, true);
                medal_image.setupBackgroundImage((MapRenderer.MEDAL_UV_START.u + (0.047851562f * ((float) i))) * 1024.0f, MapRenderer.MEDAL_UV_START.v * 1024.0f, MapRenderer.MEDAL_UV_SIZE.u * 1024.0f, MapRenderer.MEDAL_UV_SIZE.v * 1024.0f, true, new ColorFloats(1.0f, 1.0f, 1.0f, 1.0f));
                this.m_GuiGroupRulesMedals.addChild(medal_image);
            }
            this.m_GuiGroupMainLevel = new GuiGroup(this.DebugLogOutput, this.m_PhoneAbilities, 1024.0f, null, this.m_GuiBaseRoot, -18.92f, -11.0f, 37.84f, 22.0f, 0.5f, MENUALPHA, false);
            this.m_GuiGroupMainLevel.setVisibleAndUseable(false);
            this.m_GuiBaseRoot.addChild(this.m_GuiGroupMainLevel);
            this.m_ButtonTop = 20.9f;
            this.m_ButtonLeft = 32.96857f;
            this.m_GuiGroupMainLevel.addChild(new GuiButton() {
                {
                    LogOutput logOutput = GameViewExcit.this.DebugLogOutput;
                    PhoneAbilities phoneAbilities = GameViewExcit.this.m_PhoneAbilities;
                    Font access$0 = GameViewExcit.this.m_Font;
                    GuiGroup access$8 = GameViewExcit.this.m_GuiGroupMainLevel;
                    float access$2 = GameViewExcit.this.m_ButtonLeft;
                    float access$9 = GameViewExcit.this.m_ButtonTop;
                    addTextToStack(GameViewExcit.this.getResources().getString(R.string.button_label_level_leave));
                }

                public void handleTouchUp() {
                    GameViewExcit.this.m_GameLevel.m_CancelLevel = true;
                    GameViewExcit.this.m_pMediaManager.stopStreamedMusic_DelayedLoopsThread(false);
                    GameViewExcit.this.m_pMediaManager.playSoundEffect(GameViewExcit.this.getResources().getString(R.string.sound_map_level_canceled));
                }
            });
            this.m_ButtonLeft = this.m_ButtonLeft - 4.8714285f;
            this.m_GuiGroupMainLevel.addChild(new GuiButton() {
                {
                    LogOutput logOutput = GameViewExcit.this.DebugLogOutput;
                    PhoneAbilities phoneAbilities = GameViewExcit.this.m_PhoneAbilities;
                    Font access$0 = GameViewExcit.this.m_Font;
                    GuiGroup access$8 = GameViewExcit.this.m_GuiGroupMainLevel;
                    float access$2 = GameViewExcit.this.m_ButtonLeft;
                    float access$9 = GameViewExcit.this.m_ButtonTop;
                    addTextToStack(GameViewExcit.this.getResources().getString(R.string.button_label_level_restart));
                }

                public void handleTouchUp() {
                    GameViewExcit.this.m_GameLevel.restartLevel();
                }
            });
            this.m_ButtonLeft = this.m_ButtonLeft - 4.8714285f;
            this.m_GuiGroupMainLevel.addChild(new GuiButton() {
                {
                    LogOutput logOutput = GameViewExcit.this.DebugLogOutput;
                    PhoneAbilities phoneAbilities = GameViewExcit.this.m_PhoneAbilities;
                    Font access$0 = GameViewExcit.this.m_Font;
                    GuiGroup access$8 = GameViewExcit.this.m_GuiGroupMainLevel;
                    float access$2 = GameViewExcit.this.m_ButtonLeft;
                    float access$9 = GameViewExcit.this.m_ButtonTop;
                    addTextToStack(GameViewExcit.this.getResources().getString(R.string.button_label_scroll_main_settings));
                }

                public void handleTouchUp() {
                    GameViewExcit.this.m_Camera.setOrthoCamera_TargetScreen(0, -1);
                }
            });
            this.m_ButtonLeft = this.m_ButtonLeft - 8.014285f;
            this.m_SolutionButton = new GuiButton() {
                {
                    LogOutput logOutput = GameViewExcit.this.DebugLogOutput;
                    PhoneAbilities phoneAbilities = GameViewExcit.this.m_PhoneAbilities;
                    Font access$0 = GameViewExcit.this.m_Font;
                    GuiGroup access$8 = GameViewExcit.this.m_GuiGroupMainLevel;
                    float access$2 = GameViewExcit.this.m_ButtonLeft;
                    float access$9 = GameViewExcit.this.m_ButtonTop;
                    addTextToStack(GameViewExcit.this.getResources().getString(R.string.button_label_level_auotplay));
                }

                public void handleTouchUp() {
                    GameViewExcit.this.m_GameLevel.doAutoplayCurrentLevel();
                }
            };
            this.m_GuiGroupMainLevel.addChild(this.m_SolutionButton);
            this.m_GuiGroupMainMap = new GuiGroup(this.DebugLogOutput, this.m_PhoneAbilities, 1024.0f, null, this.m_GuiBaseRoot, -18.92f, -11.0f, 37.84f, 22.0f, 0.5f, MENUALPHA, false);
            this.m_GuiBaseRoot.addChild(this.m_GuiGroupMainMap);
            this.m_ButtonLeft = 34.38286f;
            this.m_GuiGroupMainMap.addChild(new GuiButton() {
                {
                    LogOutput logOutput = GameViewExcit.this.DebugLogOutput;
                    PhoneAbilities phoneAbilities = GameViewExcit.this.m_PhoneAbilities;
                    Font access$0 = GameViewExcit.this.m_Font;
                    GuiGroup access$11 = GameViewExcit.this.m_GuiGroupMainMap;
                    float access$2 = GameViewExcit.this.m_ButtonLeft;
                    addTextToStack(GameViewExcit.this.getResources().getString(R.string.button_label_exit));
                }

                public void handleTouchUp() {
                    GameViewExcit.this.m_ParentActivityAndContext.finish();
                }
            });
            this.m_ButtonLeft = this.m_ButtonLeft - 5.6571426f;
            this.m_GuiGroupMainMap.addChild(new GuiButton() {
                {
                    LogOutput logOutput = GameViewExcit.this.DebugLogOutput;
                    PhoneAbilities phoneAbilities = GameViewExcit.this.m_PhoneAbilities;
                    Font access$0 = GameViewExcit.this.m_Font;
                    GuiGroup access$11 = GameViewExcit.this.m_GuiGroupMainMap;
                    float access$2 = GameViewExcit.this.m_ButtonLeft;
                    addTextToStack(GameViewExcit.this.getResources().getString(R.string.button_label_scroll_main_settings));
                }

                public void handleTouchUp() {
                    GameViewExcit.this.m_Camera.setOrthoCamera_TargetScreen(0, -1);
                }
            });
            this.m_ButtonLeft = this.m_ButtonLeft - 4.085714f;
            this.m_GuiGroupMainMap.addChild(new GuiButton() {
                {
                    LogOutput logOutput = GameViewExcit.this.DebugLogOutput;
                    PhoneAbilities phoneAbilities = GameViewExcit.this.m_PhoneAbilities;
                    Font access$0 = GameViewExcit.this.m_Font;
                    GuiGroup access$11 = GameViewExcit.this.m_GuiGroupMainMap;
                    float access$2 = GameViewExcit.this.m_ButtonLeft;
                    addTextToStack(GameViewExcit.this.getResources().getString(R.string.button_label_scroll_main_rules));
                }

                public void handleTouchUp() {
                    GameViewExcit.this.ScrollToRules();
                }
            });
            if (DEMOVERSION) {
                this.m_ButtonLeft = this.m_ButtonLeft - 6.442857f;
                this.m_GuiGroupMainMap.addChild(new GuiButton() {
                    {
                        LogOutput logOutput = GameViewExcit.this.DebugLogOutput;
                        PhoneAbilities phoneAbilities = GameViewExcit.this.m_PhoneAbilities;
                        Font access$0 = GameViewExcit.this.m_Font;
                        GuiGroup access$11 = GameViewExcit.this.m_GuiGroupMainMap;
                        float access$2 = GameViewExcit.this.m_ButtonLeft;
                        this.m_Text.setTextColor(1.0f, 0.6f, 0.0f, 1.0f);
                        addTextToStack(GameViewExcit.this.getResources().getString(R.string.button_label_buy));
                    }

                    public void handleTouchUp() {
                        GameViewExcit.this.openBrowserWithURL(GameViewExcit.this.getResources().getString(R.string.market_full_link_url));
                    }
                });
            }
            this.m_ButtonLeft = 0.1f;
            GuiButton selectLabel = new GuiButton(this.DebugLogOutput, this.m_PhoneAbilities, 1024.0f, this.m_Font, true, null, this.m_GuiGroupMainMap, this.m_ButtonLeft, 0.1f, 12.571428f, 1.5714285f, 0.5f, 1.0f, false);
            selectLabel.addTextToStack(getResources().getString(R.string.button_label_select_level));
            selectLabel.setTouchable(false);
            selectLabel.setTextColor(0.3f, 0.3f, 0.3f, 1.0f);
            this.m_GuiGroupMainMap.addChild(selectLabel);
            this.m_GuiGroupSettings = new GuiGroup(this.DebugLogOutput, this.m_PhoneAbilities, 1024.0f, null, this.m_GuiBaseRoot, -18.92f, -34.0f, 37.84f, 22.0f, 0.5f, MENUALPHA, false);
            this.m_GuiBaseRoot.addChild(this.m_GuiGroupSettings);
            this.m_ButtonLeft = 34.38286f;
            this.m_ButtonTop = 20.27143f;
            this.m_GuiGroupSettings.addChild(new GuiButton() {
                {
                    LogOutput logOutput = GameViewExcit.this.DebugLogOutput;
                    PhoneAbilities phoneAbilities = GameViewExcit.this.m_PhoneAbilities;
                    Font access$0 = GameViewExcit.this.m_Font;
                    GuiGroup access$14 = GameViewExcit.this.m_GuiGroupSettings;
                    float access$2 = GameViewExcit.this.m_ButtonLeft;
                    float access$9 = GameViewExcit.this.m_ButtonTop;
                    addTextToStack(GameViewExcit.this.getResources().getString(R.string.button_label_scroll_settings));
                }

                public void handleTouchUp() {
                    GameViewExcit.this.m_Camera.setOrthoCamera_TargetScreen(0, 0);
                    GameViewExcit.this.m_ResetAllLevels = 0;
                }
            });
            this.m_ButtonLeft = 0.15714286f;
            this.m_ButtonTop = this.m_ButtonTop - 1.7285714f;
            this.m_GuiGroupSettings.addChild(new GuiButton() {
                {
                    LogOutput logOutput = GameViewExcit.this.DebugLogOutput;
                    PhoneAbilities phoneAbilities = GameViewExcit.this.m_PhoneAbilities;
                    Font access$0 = GameViewExcit.this.m_Font;
                    GuiGroup access$14 = GameViewExcit.this.m_GuiGroupSettings;
                    float access$2 = GameViewExcit.this.m_ButtonLeft;
                    float access$9 = GameViewExcit.this.m_ButtonTop;
                    String[] labelTexts = GameViewExcit.this.getResources().getStringArray(R.array.button_label_sound_settings_texts);
                    for (int i = 0; i < labelTexts.length; i++) {
                        addTextToStack(labelTexts[i]);
                    }
                    this.m_CurrentText = GameViewExcit.this.m_ParentActivityAndContext.getSharedPreferences(GameViewExcit.SHAREDPREFSNAME, 1).getInt("SoundVolume", labelTexts.length - 1);
                    GameViewExcit.this.m_pMediaManager.setEffectsVolume(((float) this.m_CurrentText) / ((float) getTextMaxIndex()));
                }

                public void handleTouchUp() {
                    this.m_RebuildNeeded = true;
                    GameViewExcit.this.m_pMediaManager.setEffectsVolume(((float) this.m_CurrentText) / ((float) getTextMaxIndex()));
                    SharedPreferences.Editor editor = GameViewExcit.this.m_ParentActivityAndContext.getSharedPreferences(GameViewExcit.SHAREDPREFSNAME, 1).edit();
                    editor.putInt("SoundVolume", this.m_CurrentText);
                    editor.commit();
                }
            });
            this.m_ButtonTop = this.m_ButtonTop - 1.7285714f;
            this.m_GuiGroupSettings.addChild(new GuiButton() {
                {
                    LogOutput logOutput = GameViewExcit.this.DebugLogOutput;
                    PhoneAbilities phoneAbilities = GameViewExcit.this.m_PhoneAbilities;
                    Font access$0 = GameViewExcit.this.m_Font;
                    GuiGroup access$14 = GameViewExcit.this.m_GuiGroupSettings;
                    float access$2 = GameViewExcit.this.m_ButtonLeft;
                    float access$9 = GameViewExcit.this.m_ButtonTop;
                    String[] labelTexts = GameViewExcit.this.getResources().getStringArray(R.array.button_label_music_settings_texts);
                    for (int i = 0; i < labelTexts.length; i++) {
                        addTextToStack(labelTexts[i]);
                    }
                    this.m_CurrentText = GameViewExcit.this.m_ParentActivityAndContext.getSharedPreferences(GameViewExcit.SHAREDPREFSNAME, 1).getInt("MusicVolume", labelTexts.length - 1);
                    GameViewExcit.this.m_pMediaManager.setMusicVolume(((float) this.m_CurrentText) / ((float) getTextMaxIndex()));
                }

                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: com.ludocrazy.tools.MediaManager.playStreamedMusic(java.lang.String, boolean, float):void
                 arg types: [java.lang.String, int, int]
                 candidates:
                  com.ludocrazy.tools.MediaManager.playStreamedMusic(int, boolean, float):void
                  com.ludocrazy.tools.MediaManager.playStreamedMusic(java.lang.String, boolean, float):void */
                public void handleTouchUp() {
                    this.m_RebuildNeeded = true;
                    GameViewExcit.this.m_pMediaManager.setMusicVolume(((float) this.m_CurrentText) / ((float) getTextMaxIndex()));
                    GameViewExcit.this.m_pMediaManager.playStreamedMusic(GameViewExcit.this.getResources().getString(R.string.music_menu), false, 0.5f);
                    SharedPreferences.Editor editor = GameViewExcit.this.m_ParentActivityAndContext.getSharedPreferences(GameViewExcit.SHAREDPREFSNAME, 1).edit();
                    editor.putInt("MusicVolume", this.m_CurrentText);
                    editor.commit();
                }
            });
            this.m_ButtonTop = this.m_ButtonTop - 1.7285714f;
            this.m_GuiGroupSettings.addChild(new GuiButton() {
                {
                    LogOutput logOutput = GameViewExcit.this.DebugLogOutput;
                    PhoneAbilities phoneAbilities = GameViewExcit.this.m_PhoneAbilities;
                    Font access$0 = GameViewExcit.this.m_Font;
                    GuiGroup access$14 = GameViewExcit.this.m_GuiGroupSettings;
                    float access$2 = GameViewExcit.this.m_ButtonLeft;
                    float access$9 = GameViewExcit.this.m_ButtonTop;
                    String[] labelTexts = GameViewExcit.this.getResources().getStringArray(R.array.button_label_map_deco_texts);
                    for (int i = 0; i < labelTexts.length; i++) {
                        addTextToStack(labelTexts[i]);
                    }
                    this.m_CurrentText = GameViewExcit.this.m_MapDecoVisible ? 0 : 1;
                }

                public void handleTouchUp() {
                    int i;
                    GameViewExcit.this.m_MapDecoVisible = !GameViewExcit.this.m_MapDecoVisible;
                    if (GameViewExcit.this.m_MapDecoVisible) {
                        i = 0;
                    } else {
                        i = 1;
                    }
                    this.m_CurrentText = i;
                    this.m_RebuildNeeded = true;
                    GameViewExcit.this.SharedPreferences_Editor_SaveBool("MapDecoVisible", GameViewExcit.this.m_MapDecoVisible);
                    try {
                        GameViewExcit.this.m_GameMap = null;
                        GameViewExcit.this.m_ForceNewMap = true;
                    } catch (Exception e) {
                        new ErrorOutput("MapDecoSettingsButton.handleTouchUp()", "can't create new GameMap: ", e);
                    }
                }
            });
            this.m_ButtonTop = this.m_ButtonTop - 1.7285714f;
            GuiButton controlsLabel = new GuiButton(this.DebugLogOutput, this.m_PhoneAbilities, 1024.0f, this.m_Font, false, null, this.m_GuiGroupSettings, this.m_ButtonLeft, this.m_ButtonTop, 18.4436f, 1.5714285f, 0.5f, 1.0f, false);
            controlsLabel.addTextToStack(getResources().getString(R.string.button_label_controls));
            controlsLabel.setTouchable(false);
            this.m_GuiGroupSettings.addChild(controlsLabel);
            this.m_ButtonTop = this.m_ButtonTop - 1.2571429f;
            this.m_ButtonControls_Drag = new GuiButton() {
                {
                    LogOutput logOutput = GameViewExcit.this.DebugLogOutput;
                    PhoneAbilities phoneAbilities = GameViewExcit.this.m_PhoneAbilities;
                    Font access$0 = GameViewExcit.this.m_Font;
                    GuiGroup access$14 = GameViewExcit.this.m_GuiGroupSettings;
                    float access$2 = GameViewExcit.this.m_ButtonLeft;
                    float access$9 = GameViewExcit.this.m_ButtonTop;
                    String labelText = GameViewExcit.this.getResources().getString(R.string.button_label_controls_drag);
                    addTextToStack(String.valueOf(labelText) + " " + GameViewExcit.this.getResources().getString(R.string.button_label_controls_enabled));
                    addTextToStack(String.valueOf(labelText) + " " + GameViewExcit.this.getResources().getString(R.string.button_label_controls_disabled));
                    this.m_CurrentText = GameViewExcit.this.m_ControlsEnabled_Drag ? 0 : 1;
                }

                public void handleTouchUp() {
                    int i;
                    GameViewExcit.this.m_ControlsEnabled_Drag = !GameViewExcit.this.m_ControlsEnabled_Drag;
                    if (GameViewExcit.this.m_ControlsEnabled_Drag) {
                        GameViewExcit.this.m_ControlsEnabled_TouchSides = false;
                        GameViewExcit.this.m_ButtonControls_TouchSides.m_CurrentText = 1;
                        GameViewExcit.this.m_ButtonControls_TouchSides.m_RebuildNeeded = true;
                        GameViewExcit.this.m_ControlsEnabled_TouchPosition = false;
                        GameViewExcit.this.m_ButtonControls_TouchPosition.m_CurrentText = 1;
                        GameViewExcit.this.m_ButtonControls_TouchPosition.m_RebuildNeeded = true;
                    }
                    if (GameViewExcit.this.m_ControlsEnabled_Drag) {
                        i = 0;
                    } else {
                        i = 1;
                    }
                    this.m_CurrentText = i;
                    this.m_RebuildNeeded = true;
                    GameViewExcit.this.SharedPreferences_Editor_SaveBool("ControlsEnabled_Drag", GameViewExcit.this.m_ControlsEnabled_Drag);
                }
            };
            this.m_GuiGroupSettings.addChild(this.m_ButtonControls_Drag);
            this.m_ButtonTop = this.m_ButtonTop - 2.5142858f;
            this.m_GuiGroupSettings.addChild(new GuiButton() {
                {
                    LogOutput logOutput = GameViewExcit.this.DebugLogOutput;
                    PhoneAbilities phoneAbilities = GameViewExcit.this.m_PhoneAbilities;
                    Font access$0 = GameViewExcit.this.m_Font;
                    GuiGroup access$14 = GameViewExcit.this.m_GuiGroupSettings;
                    float access$2 = GameViewExcit.this.m_ButtonLeft;
                    float access$9 = GameViewExcit.this.m_ButtonTop;
                    String labelText = GameViewExcit.this.getResources().getString(R.string.button_label_controls_cursor);
                    addTextToStack(String.valueOf(labelText) + " " + GameViewExcit.this.getResources().getString(R.string.button_label_controls_enabled));
                    addTextToStack(String.valueOf(labelText) + " " + GameViewExcit.this.getResources().getString(R.string.button_label_controls_disabled));
                    this.m_CurrentText = GameViewExcit.this.m_ControlsEnabled_Cursor ? 0 : 1;
                }

                public void handleTouchUp() {
                    int i;
                    GameViewExcit.this.m_ControlsEnabled_Cursor = !GameViewExcit.this.m_ControlsEnabled_Cursor;
                    if (GameViewExcit.this.m_ControlsEnabled_Cursor) {
                        i = 0;
                    } else {
                        i = 1;
                    }
                    this.m_CurrentText = i;
                    this.m_RebuildNeeded = true;
                    GameViewExcit.this.SharedPreferences_Editor_SaveBool("ControlsEnabled_Cursor", GameViewExcit.this.m_ControlsEnabled_Cursor);
                }
            });
            this.m_ButtonTop = this.m_ButtonTop - 1.7285714f;
            this.m_GuiGroupSettings.addChild(new GuiButton() {
                {
                    LogOutput logOutput = GameViewExcit.this.DebugLogOutput;
                    PhoneAbilities phoneAbilities = GameViewExcit.this.m_PhoneAbilities;
                    Font access$0 = GameViewExcit.this.m_Font;
                    GuiGroup access$14 = GameViewExcit.this.m_GuiGroupSettings;
                    float access$2 = GameViewExcit.this.m_ButtonLeft;
                    float access$9 = GameViewExcit.this.m_ButtonTop;
                    String labelText = GameViewExcit.this.getResources().getString(R.string.button_label_controls_wasd);
                    addTextToStack(String.valueOf(labelText) + " " + GameViewExcit.this.getResources().getString(R.string.button_label_controls_enabled));
                    addTextToStack(String.valueOf(labelText) + " " + GameViewExcit.this.getResources().getString(R.string.button_label_controls_disabled));
                    this.m_CurrentText = GameViewExcit.this.m_ControlsEnabled_WASD ? 0 : 1;
                }

                public void handleTouchUp() {
                    int i;
                    GameViewExcit.this.m_ControlsEnabled_WASD = !GameViewExcit.this.m_ControlsEnabled_WASD;
                    if (GameViewExcit.this.m_ControlsEnabled_WASD) {
                        i = 0;
                    } else {
                        i = 1;
                    }
                    this.m_CurrentText = i;
                    this.m_RebuildNeeded = true;
                    GameViewExcit.this.SharedPreferences_Editor_SaveBool("ControlsEnabled_WASD", GameViewExcit.this.m_ControlsEnabled_WASD);
                }
            });
            this.m_ButtonTop = this.m_ButtonTop - 1.7285714f;
            this.m_GuiGroupSettings.addChild(new GuiButton() {
                {
                    LogOutput logOutput = GameViewExcit.this.DebugLogOutput;
                    PhoneAbilities phoneAbilities = GameViewExcit.this.m_PhoneAbilities;
                    Font access$0 = GameViewExcit.this.m_Font;
                    GuiGroup access$14 = GameViewExcit.this.m_GuiGroupSettings;
                    float access$2 = GameViewExcit.this.m_ButtonLeft;
                    float access$9 = GameViewExcit.this.m_ButtonTop;
                    String labelText = GameViewExcit.this.getResources().getString(R.string.button_label_controls_ijkl);
                    addTextToStack(String.valueOf(labelText) + " " + GameViewExcit.this.getResources().getString(R.string.button_label_controls_enabled));
                    addTextToStack(String.valueOf(labelText) + " " + GameViewExcit.this.getResources().getString(R.string.button_label_controls_disabled));
                    this.m_CurrentText = GameViewExcit.this.m_ControlsEnabled_IJKL ? 0 : 1;
                }

                public void handleTouchUp() {
                    int i;
                    GameViewExcit.this.m_ControlsEnabled_IJKL = !GameViewExcit.this.m_ControlsEnabled_IJKL;
                    if (GameViewExcit.this.m_ControlsEnabled_IJKL) {
                        i = 0;
                    } else {
                        i = 1;
                    }
                    this.m_CurrentText = i;
                    this.m_RebuildNeeded = true;
                    GameViewExcit.this.SharedPreferences_Editor_SaveBool("ControlsEnabled_IJKL", GameViewExcit.this.m_ControlsEnabled_IJKL);
                }
            });
            this.m_ButtonLeft = 19.1964f;
            this.m_ButtonTop = 7.542856f;
            this.m_GuiGroupSettings.addChild(new GuiButton() {
                {
                    LogOutput logOutput = GameViewExcit.this.DebugLogOutput;
                    PhoneAbilities phoneAbilities = GameViewExcit.this.m_PhoneAbilities;
                    Font access$0 = GameViewExcit.this.m_Font;
                    GuiGroup access$14 = GameViewExcit.this.m_GuiGroupSettings;
                    float access$2 = GameViewExcit.this.m_ButtonLeft;
                    float access$9 = GameViewExcit.this.m_ButtonTop;
                    setLineLength(35);
                    setTouchable(false);
                    addTextToStack(GameViewExcit.this.getResources().getString(R.string.credits_texts));
                }

                public void handleTouchUp() {
                }
            });
            this.m_ButtonTop = this.m_ButtonTop - 4.085714f;
            this.m_GuiGroupSettings.addChild(new GuiButton() {
                {
                    LogOutput logOutput = GameViewExcit.this.DebugLogOutput;
                    PhoneAbilities phoneAbilities = GameViewExcit.this.m_PhoneAbilities;
                    Font access$0 = GameViewExcit.this.m_Font;
                    GuiGroup access$14 = GameViewExcit.this.m_GuiGroupSettings;
                    float access$2 = GameViewExcit.this.m_ButtonLeft;
                    float access$9 = GameViewExcit.this.m_ButtonTop;
                    setLineLength(32);
                    addTextToStack(GameViewExcit.this.getResources().getString(R.string.credits_link_text));
                }

                public void handleTouchUp() {
                    GameViewExcit.this.openBrowserWithURL(GameViewExcit.this.getResources().getString(R.string.credits_link_url));
                }
            });
            this.m_ButtonTop = this.m_ButtonTop - 3.2999997f;
            this.m_GuiGroupSettings.addChild(new GuiButton() {
                {
                    LogOutput logOutput = GameViewExcit.this.DebugLogOutput;
                    PhoneAbilities phoneAbilities = GameViewExcit.this.m_PhoneAbilities;
                    Font access$0 = GameViewExcit.this.m_Font;
                    GuiGroup access$14 = GameViewExcit.this.m_GuiGroupSettings;
                    float access$2 = GameViewExcit.this.m_ButtonLeft;
                    float access$9 = GameViewExcit.this.m_ButtonTop;
                    setLineLength(35);
                    addTextToStack(GameViewExcit.this.getResources().getString(R.string.button_label_reset_levels));
                    addTextToStack(GameViewExcit.this.getResources().getString(R.string.button_label_reset_confirm));
                    addTextToStack(GameViewExcit.this.getResources().getString(R.string.button_label_reset_confirm2));
                }

                public void handleTouchUp() {
                    GameViewExcit gameViewExcit = GameViewExcit.this;
                    gameViewExcit.m_ResetAllLevels = gameViewExcit.m_ResetAllLevels + 1;
                    if (GameViewExcit.this.m_ResetAllLevels == 3) {
                        GameViewExcit.this.m_GameMap.resetAllLevels();
                        GameViewExcit.this.m_GameMap = null;
                        GameViewExcit.this.m_pMediaManager.playSoundEffect(GameViewExcit.this.getResources().getString(R.string.sound_map_reset_all));
                        GameViewExcit.this.m_ResetAllLevels = 0;
                    }
                }
            });
        } catch (Exception e) {
            new ErrorOutput("GameView::setupGui()", "Exception", e);
        }
    }

    private void onResumeAfterOnSurfaceCreated() throws Exception {
        if (this.m_resumeNeedsRedraw) {
            if (this.m_MapRenderer != null) {
                this.m_MapRenderer.onResume();
            }
            if (this.m_GuiBaseRoot != null) {
                this.m_GuiBaseRoot.onResume();
            }
            setupGuiForMapOrLevelUse();
            if (this.m_GuiGroupIntro != null) {
                this.m_GuiGroupIntro.setupBackgroundGradientImage(971, 383.0f, 52.0f, 444.0f, true, new ColorFloats(1.0f, 1.0f, 1.0f, 1.0f), 12);
            }
            if (this.m_GuiGroupSettings != null) {
                this.m_GuiGroupSettings.setupBackgroundGradientImage(971, 383.0f, 52.0f, 444.0f, true, new ColorFloats(1.0f, 1.0f, 1.0f, 1.0f), 12);
            }
            if (this.m_GuiGroupMainMap != null) {
                this.m_GuiGroupMainMap.setupBackgroundGradientImage(971, 383.0f, 52.0f, 444.0f, true, new ColorFloats(1.0f, 1.0f, 1.0f, 1.0f), 12);
            }
        }
    }

    public void setupGuiForMapOrLevelUse() {
        if (this.m_GuiGroupMainMap != null) {
            this.m_GuiGroupMainMap.setVisibleAndUseable(this.m_MapVisible);
        }
        if (this.m_GuiGroupMainLevel != null) {
            this.m_GuiGroupMainLevel.setChildrenHidden(this.m_MapVisible);
            this.m_GuiGroupMainLevel.setVisibleAndUseable(!this.m_MapVisible);
        }
    }

    /* access modifiers changed from: package-private */
    public void doStartOfLevelTasks() throws Exception {
        MapLogic.MapField nextLevel = this.m_GameMap.getNextLevel();
        if (nextLevel != null) {
            this.m_SolutionButton.setVisibleAndUseable(nextLevel.solution_moves > 2);
            this.m_MapVisible = false;
            this.m_GameMap = null;
            this.m_GameLevel = new GameLevel(this.DebugLogOutput, this.m_PhoneAbilities, this.m_ParentActivityAndContext, this.m_pMediaManager, this.m_Font, this.m_ParticleManager, this.m_GameTimeSeconds, nextLevel, 37.84f, 22.0f);
            setupGuiForMapOrLevelUse();
            this.m_LeaveLevelClicked = false;
            this.m_LeaveLevelNOWclicked = false;
        }
    }

    private void doEndOfLevelTasks() throws Exception {
        if (this.m_GameMap == null) {
            this.m_MapVisible = true;
            setupGuiForMapOrLevelUse();
            this.DebugLogOutput.log(2, "GameViewExcit.doEndOfLevelTasks() stopped (set as dead) " + this.m_ParticleManager.StopAllRunningEmitters() + " running emitters.");
            this.m_GameLevel = null;
            StopWatch watch = new StopWatch();
            if (this.m_MapRenderer == null) {
                this.m_MapRenderer = new MapRenderer(this.DebugLogOutput, this.m_PhoneAbilities, this.m_ParentActivityAndContext, this.m_Font, this.m_Highres1024orMore);
            }
            this.m_GameMap = new GameMap(this.DebugLogOutput, this.m_PhoneAbilities, this.m_ParentActivityAndContext, this.m_MapRenderer, this.m_pMediaManager, MAPFILENAME, this.m_MapDecoVisible, false);
            this.DebugLogOutput.log(2, "creating renderer and map: " + watch.getElapsedMilliSeconds() + "ms.");
            this.m_LeaveLevelClicked = false;
            this.m_LeaveLevelNOWclicked = false;
            this.m_ResetAllLevels = 0;
            if (this.m_GameMap.areAllLevelsCompleted() && !this.m_ForceNewMap && this.m_DisplayingOutroText <= 0) {
                this.m_DisplayingOutroText = 1;
            }
        }
    }

    public void drawLoadingGraphics(GL10 gl, float load_percentage) throws Exception {
        if (this.m_DynamicGuiMesh != null) {
            clearAndSetupLoadingScreen(gl);
            gl.glDisable(3042);
            gl.glDisable(3553);
            gl.glDisableClientState(32888);
            gl.glEnableClientState(32884);
            gl.glEnableClientState(32886);
            this.m_DynamicGuiMesh.resetMesh_ForNextDynamicUpdate();
            Coords inner_ring_center_pos = new Coords(0.0f, 0.5f, 0.0f);
            ColorFloats inner_col = new ColorFloats(1.0f - load_percentage, 1.0f - load_percentage, 0.0f, 1.0f);
            this.m_DynamicGuiMesh.addTriangledRing_colored(100, inner_ring_center_pos, inner_ring_center_pos, 0.0625f, 0.3125f, 0.0f, 360.0f * load_percentage, inner_col, inner_col);
            this.m_DynamicGuiMesh.convertMesh_AfterDynamicUpdate();
            this.m_DynamicGuiMesh.draw(gl);
        }
    }

    public void onDrawFrame(GL10 gl) {
        boolean redrawNeeded;
        try {
            if (!this.m_AllHasBeenLoaded) {
                LoadingFunction(gl);
                drawLoadingGraphics(gl, ((float) this.m_CurrentLoadingStep) / ((float) this.MAX_LOADING_STEPS));
                return;
            }
            long frame_time_now_milliseconds = System.currentTimeMillis();
            float frame_duration_milliseconds = (float) (frame_time_now_milliseconds - this.m_LastFrameTimeMilliSeconds);
            if (frame_duration_milliseconds > 0.0f) {
                this.m_GameTimeSeconds = this.m_GameTimeSeconds + (frame_duration_milliseconds / 1000.0f);
                this.m_LastFrameTimeMilliSeconds = frame_time_now_milliseconds;
                this.m_FramesPerSecondAverage = (0.9f * this.m_FramesPerSecondAverage) + (0.1f * (1000.0f / frame_duration_milliseconds));
                if (frame_time_now_milliseconds - this.m_LastOutputMilliSeconds > 8000) {
                    this.m_LastOutputMilliSeconds = frame_time_now_milliseconds;
                    if (this.DebugLogOutput.isCurrentLogLevelAtleast(1)) {
                        String message = "average fps:" + this.m_FramesPerSecondAverage + " particles drawn:" + this.m_LastTimeParticlesDrawn;
                        if (this.m_GameLevel != null) {
                            message = String.valueOf(message) + " visible vertices (level):" + this.m_GameLevel.getCurrentDrawCount();
                        } else if (this.m_GameMap != null) {
                            message = String.valueOf(message) + " visible vertices (map):" + this.m_GameMap.getCurrentDrawCount();
                        }
                        this.DebugLogOutput.log(1, message);
                    }
                }
            }
            if (this.m_GameLevel == null && this.m_GameMap != null) {
                doStartOfLevelTasks();
            } else if ((this.m_GameMap == null && this.m_GameLevel == null) || this.m_GameLevel.m_CancelLevel || ((this.m_GameLevel.canBeUnloaded() && this.m_LeaveLevelClicked) || this.m_LeaveLevelNOWclicked || this.m_ForceNewMap)) {
                doEndOfLevelTasks();
                this.m_ForceNewMap = false;
            }
            boolean redrawNeeded2 = 0 != 0 || this.m_resumeNeedsRedraw;
            this.m_resumeNeedsRedraw = false;
            boolean redrawNeeded3 = (((redrawNeeded2 || this.m_3dEnabled < 0) || this.m_GuiBaseRoot.m_RedrawNeeded) || this.m_Camera.m_CameraNeedsRedraw) || this.m_GuiMessageBoxIconNeedsRedraw;
            if (this.m_ParticleManager.UpdateEffects(this.m_GameTimeSeconds) > 1) {
                redrawNeeded3 = true;
            } else {
                this.m_LastTimeParticlesDrawn = 0;
            }
            if (this.m_MapVisible) {
                if (this.m_GameMap != null) {
                    if (redrawNeeded || this.m_GameMap.m_reDrawNeeded) {
                        redrawNeeded = true;
                    } else {
                        redrawNeeded = false;
                    }
                }
            } else if (this.m_GameLevel != null) {
                redrawNeeded = redrawNeeded || this.m_GameLevel.m_reDrawNeeded || this.m_GameLevel.isLevelCompleted();
            }
            if (doubleBufferRedrawNeeded(redrawNeeded)) {
                CameraOrtho.CameraOrthoExtends extents = this.m_Camera.setupProjectionMatrix_ViaOrthoCamera_UpdatingCamera(gl);
                this.m_GuiBaseRoot.updateVisiblity(extents.orthoCameraMinX, extents.orthoCameraMinY, extents.orthoCameraMaxX, extents.orthoCameraMaxY);
                doClear(gl);
                gl.glLoadIdentity();
                if (this.m_PhoneAbilities.isSupported(4)) {
                    this.m_TextureManager.useTexture(gl, R.drawable.texture_splotch_no_alpha);
                } else {
                    gl.glDisable(3553);
                }
                gl.glEnable(34913);
                gl.glPointSize((float) this.POINTSPRITESIZE);
                gl.glTexEnvx(34913, 34914, 1);
                gl.glEnableClientState(32884);
                gl.glEnableClientState(32886);
                boolean logo_screen_visible = this.m_Camera.isOrthoCamera_OnScreen(0, 2);
                if (logo_screen_visible && this.m_LogoDrawCount > 0) {
                    this.m_LogoDrawCount = this.m_LogoDrawCount - 1;
                    this.m_LogoSproxel.draw(gl, this.m_LogoSproxel.m_PointSpriteVertexCount - this.m_LogoDrawCount);
                    if (!this.m_LogoSoundStarted) {
                        startLogoSoundEffect(this.m_ParentActivityAndContext.getSharedPreferences(SHAREDPREFSNAME, 1).getInt("MusicVolume", 1), getResources().getString(R.string.music_logo_sound));
                    }
                    if (this.m_LogoDrawCount <= 0) {
                        convertLogoToParticleEffect();
                    }
                }
                gl.glLoadIdentity();
                this.m_GuiMessageBoxIconNeedsRedraw = false;
                if (!this.m_PhoneAbilities.isSupported(4)) {
                    gl.glEnable(3553);
                }
                gl.glDisable(34913);
                gl.glEnableClientState(32884);
                gl.glEnableClientState(32888);
                gl.glEnableClientState(32886);
                gl.glEnable(3553);
                this.m_TextureManager.useTexture(gl, R.drawable.texture_gui);
                gl.glEnable(3042);
                gl.glLoadIdentity();
                gl.glTranslatef(0.0f, 0.0f, -9.0f);
                if (this.m_GuiBaseRoot != null) {
                    this.m_GuiBaseRoot.draw(gl);
                }
                gl.glLoadIdentity();
                if (this.m_GuiGroupMainLevel.isVisibleOnOrthoCamera() && this.m_GameLevel != null) {
                    this.m_GameLevel.draw(gl, this.m_GameTimeSeconds);
                }
                if (this.m_MapVisible && this.m_GuiGroupMainMap != null && this.m_GuiGroupMainMap.isVisibleOnOrthoCamera() && this.m_GameMap != null && this.m_MapRenderer != null && this.m_GameLevel == null) {
                    this.m_MapRenderer.draw(gl);
                }
                this.m_LastTimeParticlesDrawn = this.m_ParticleManager.drawTriangledQuads(gl);
                gl.glDisableClientState(32888);
                if (logo_screen_visible) {
                    gl.glLoadIdentity();
                    this.m_TextureManager.useTexture(gl, R.drawable.texture_splotch_no_alpha);
                    gl.glEnable(34913);
                    gl.glPointSize((float) this.POINTSPRITESIZE);
                    gl.glTexEnvx(34913, 34914, 1);
                    gl.glEnableClientState(32884);
                    gl.glEnableClientState(32886);
                    if (logo_screen_visible) {
                        gl.glEnable(3042);
                    }
                    this.m_ParticleManager.drawNonSizedPointSprites(gl);
                    gl.glDisable(3042);
                    return;
                }
                return;
            }
            Thread.sleep(250);
            this.DebugLogOutput.log(2, "currently no redraw needed!");
        } catch (Exception e) {
            new ErrorOutput("GameView::onDrawFrame()", "Exception", e);
        }
    }

    public void onSurfaceChanged(GL10 gl, int width, int height) {
        boolean z;
        this.DebugLogOutput.log(2, "GameViewExcit.onSurfaceChanged() width:" + width + " height:" + height);
        if (height == 0) {
            height = 1;
        }
        try {
            gl.glViewport(0, 0, width, height);
            this.m_ScreenWidthPixels = width;
            this.m_ScreenHeightPixels = height;
            PIXEL_WIDTH = 37.84f / ((float) this.m_ScreenWidthPixels);
            PIXEL_HEIGHT = 22.0f / ((float) this.m_ScreenHeightPixels);
            float smallerScreenSideSizePixels = (float) getSmallerScreenSidePixels(this.m_ScreenWidthPixels, this.m_ScreenHeightPixels);
            float squarePixelsPerField = smallerScreenSideSizePixels / 37.84f;
            if (getLargerScreenSidePixels(this.m_ScreenWidthPixels, this.m_ScreenHeightPixels) >= 1024) {
                z = true;
            } else {
                z = false;
            }
            this.m_Highres1024orMore = z;
            this.POINTSPRITESIZE = Math.round((squarePixelsPerField / 12.684989f) * 7.0f);
            int possiblePOINTSPRITESIZE = this.m_PhoneAbilities.checkMinMaxValue(this.POINTSPRITESIZE, 0);
            if (possiblePOINTSPRITESIZE != this.POINTSPRITESIZE) {
                this.DebugLogOutput.log(1, "POINTSPRITESIZE can't be intended:" + this.POINTSPRITESIZE + " setting to possible:" + possiblePOINTSPRITESIZE + " smallerScreenSideSizePixels:" + smallerScreenSideSizePixels);
                this.POINTSPRITESIZE = possiblePOINTSPRITESIZE;
            } else {
                this.DebugLogOutput.log(2, "POINTSPRITESIZE is now:" + this.POINTSPRITESIZE + " smallerScreenSideSizePixels:" + smallerScreenSideSizePixels);
            }
            this.m_Camera.onSurfaceChanged(gl, width, height);
        } catch (Exception e) {
            new ErrorOutput("GameView.onSurfaceChanged()", " catched Exception:", e);
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        boolean eventHasBeenHandled = false;
        try {
            if (this.m_MapVisible) {
                switch (keyCode) {
                    case 4:
                        if (this.m_Camera.getCameraPositionTarget().y != 0.0f) {
                            this.m_Camera.setOrthoCamera_TargetScreen(0, 0);
                            eventHasBeenHandled = true;
                            this.m_pMediaManager.playSoundEffect(getResources().getString(R.string.sound_gui_button));
                            break;
                        }
                        break;
                    case 82:
                        if (this.m_Camera.isOrthoCamera_OnScreen(0, 1)) {
                            this.m_StoryMessageBox.resetToNewText_Displaying(this.m_PhoneAbilities.getAbilityStrings(this.m_CurrentRulesNumber));
                        } else {
                            this.m_Camera.setOrthoCamera_TargetScreen(0, -1);
                        }
                        eventHasBeenHandled = true;
                        this.m_pMediaManager.playSoundEffect(getResources().getString(R.string.sound_gui_button));
                        break;
                    case 84:
                        if (this.m_Camera.isOrthoCamera_OnScreen(0, 1)) {
                            this.m_StoryMessageBox.resetToNewText_Displaying(this.m_PhoneAbilities.getAbilityStrings(this.m_CurrentRulesNumber));
                        } else {
                            ScrollToRules();
                        }
                        eventHasBeenHandled = true;
                        this.m_pMediaManager.playSoundEffect(getResources().getString(R.string.sound_gui_button));
                        break;
                }
                return eventHasBeenHandled;
            } else if (this.m_GameLevel != null) {
                eventHasBeenHandled = this.m_GameLevel.onKeyDown(keyCode, this.m_ControlsEnabled_Cursor, this.m_ControlsEnabled_WASD, this.m_ControlsEnabled_IJKL);
                if (!(this.m_GameLevel == null || !this.m_GameLevel.isLevelCompleted() || this.m_GuiGroupMainLevel == null)) {
                    this.m_GuiGroupMainLevel.setChildrenHidden(true);
                }
            }
            switch (keyCode) {
                case 24:
                case 25:
                    this.m_pMediaManager.playSoundEffect(getResources().getString(R.string.sound_volume_changed));
                    break;
            }
        } catch (Exception e) {
            new ErrorOutput("GameView::onKeyUp()", "Exception", e);
        }
        return eventHasBeenHandled;
    }

    public void setSensorAcceleration(float x, float y, float z) {
        try {
            super.setSensorAcceleration(x, y, z);
            if (this.m_GameLevel != null && this.m_ControlsEnabled_Shake) {
                float[] fArr = this.m_Sensor_LinearAcceleration;
                fArr[0] = fArr[0] / 3.5f;
                float[] fArr2 = this.m_Sensor_LinearAcceleration;
                fArr2[1] = fArr2[1] / 3.5f;
                float x_strength = Math.abs(this.m_Sensor_LinearAcceleration[0]);
                float y_strength = Math.abs(this.m_Sensor_LinearAcceleration[1]);
                if (x_strength > 1.0f || y_strength > 1.0f) {
                    if (this.m_Controls_DoingSensorMove <= 0) {
                        this.DebugLogOutput.log(2, "Sensor x:" + this.m_Sensor_LinearAcceleration[0] + ", y:" + this.m_Sensor_LinearAcceleration[1]);
                        if (x_strength > y_strength) {
                            this.m_GameLevel.doMove(((int) this.m_Sensor_LinearAcceleration[0]) * -1, 0);
                        } else {
                            this.m_GameLevel.doMove(0, (int) this.m_Sensor_LinearAcceleration[1]);
                        }
                        this.m_Controls_DoingSensorMove = 8;
                    }
                } else if (this.m_Controls_DoingSensorMove > 0) {
                    this.m_Controls_DoingSensorMove--;
                }
            }
        } catch (Exception e) {
            new ErrorOutput("GameViewExcit.setSensorAcceleration()", "Exception", e);
        }
    }

    public boolean onTouchEvent(MotionEvent event) {
        try {
            float x = event.getX();
            float y = event.getY();
            float x_percentage = x / ((float) getWidth());
            float y_percentage = 1.0f - (y / ((float) getHeight()));
            if (event.getAction() == 0) {
                this.m_ClickDown = true;
                this.m_GuiBaseRoot.checkAndHandleTouchDown(x_percentage, y_percentage);
            }
            if (event.getAction() == 2) {
                float dx_pixels = x - this.oldX;
                float dy_pixels = y - this.oldY;
                if (Math.abs(dx_pixels) + Math.abs(dy_pixels) > 4.0f) {
                    if (!(this.m_GameMap == null || this.m_MapRenderer == null)) {
                        this.m_MapRenderer.scrollMap(dx_pixels / ((float) getWidth()), dy_pixels / ((float) getHeight()));
                        this.m_ClickDown = false;
                    }
                    if (this.m_GameLevel != null && this.m_ControlsEnabled_Drag) {
                        if (dx_pixels < -30.0f) {
                            this.m_GameLevel.doMove(-1, 0);
                        }
                        if (dx_pixels > 30.0f) {
                            this.m_GameLevel.doMove(1, 0);
                        }
                        if (dy_pixels < -30.0f) {
                            this.m_GameLevel.doMove(0, -1);
                        }
                        if (dy_pixels > 30.0f) {
                            this.m_GameLevel.doMove(0, 1);
                        }
                        this.m_ClickDown = false;
                    }
                }
            }
            if (event.getAction() == 1 && this.m_ClickDown) {
                if (this.m_GameLevel == null || !this.m_GameLevel.isLevelCompleted()) {
                    if (this.m_GuiBaseRoot.checkAndHandleTouchUp(x_percentage, y_percentage)) {
                        this.m_pMediaManager.playSoundEffect(getResources().getString(R.string.sound_gui_button));
                    } else if (this.m_Camera.getCameraPositionTarget().y == 0.0f) {
                        if (this.m_GameMap != null) {
                            this.m_GameMap.fieldClick(x_percentage, y_percentage - 0.022727273f);
                        } else if (this.m_GameLevel != null) {
                            if (this.m_ControlsEnabled_TouchSides) {
                                if (x_percentage < 0.25f) {
                                    this.m_GameLevel.doMove(-1, 0);
                                }
                                if (x_percentage > 0.75f) {
                                    this.m_GameLevel.doMove(1, 0);
                                }
                                if (y_percentage > 0.75f) {
                                    this.m_GameLevel.doMove(0, -1);
                                }
                                if (y_percentage < 0.25f) {
                                    this.m_GameLevel.doMove(0, 1);
                                }
                            }
                            this.m_GameLevel.onTouch(x_percentage, y_percentage - 0.022727273f, this.m_ControlsEnabled_TouchPosition);
                            if (this.m_GameLevel.isLevelCompleted()) {
                                this.m_GuiGroupMainLevel.setChildrenHidden(true);
                            }
                        }
                    }
                } else if (!this.m_LeaveLevelClicked) {
                    this.m_LeaveLevelClicked = true;
                } else {
                    this.m_LeaveLevelNOWclicked = true;
                }
                this.m_ClickDown = false;
            }
            this.oldX = x;
            this.oldY = y;
            return true;
        } catch (Exception e) {
            new ErrorOutput("GameView::onTouchEvent()", "Exception", e);
            return true;
        }
    }

    private boolean doubleBufferRedrawNeeded(boolean redrawNeeded) {
        if (redrawNeeded) {
            this.m_DoubleBufferReDrawNeeded = -2;
        } else if (this.m_DoubleBufferReDrawNeeded < 0) {
            this.m_DoubleBufferReDrawNeeded++;
        }
        return this.m_DoubleBufferReDrawNeeded < 0;
    }

    /* access modifiers changed from: private */
    public void showRulesPage(int page_id) {
        this.m_CurrentRulesNumber = page_id % 3;
        this.m_StoryMessageBox.setLineLength(44);
        this.m_StoryMessageBox.setTouchable(false);
        switch (this.m_CurrentRulesNumber) {
            case 0:
                this.m_StoryMessageBox.resetToNewText_Displaying(getResources().getString(R.string.intro_text));
                this.m_GuiGroupRulesBasics.setVisibleAndUseable(true);
                this.m_GuiGroupRulesMedals.setVisibleAndUseable(false);
                return;
            case 1:
                this.m_StoryMessageBox.resetToNewText_Displaying(getResources().getString(R.string.rules_medals));
                this.m_GuiGroupRulesBasics.setVisibleAndUseable(false);
                this.m_GuiGroupRulesMedals.setVisibleAndUseable(true);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: private */
    public void ScrollToRules() {
        showRulesPage(0);
        this.m_Camera.setOrthoCamera_TargetScreen(0, 1);
    }

    /* access modifiers changed from: private */
    public void SharedPreferences_Editor_SaveBool(String pref_name, boolean pref) {
        SharedPreferences.Editor editor = this.m_ParentActivityAndContext.getSharedPreferences(SHAREDPREFSNAME, 1).edit();
        editor.putBoolean(pref_name, pref);
        editor.commit();
    }
}
