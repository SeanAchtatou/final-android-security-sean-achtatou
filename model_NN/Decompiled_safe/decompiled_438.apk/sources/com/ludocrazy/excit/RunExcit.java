package com.ludocrazy.excit;

import android.os.Bundle;
import com.ludocrazy.tools.ErrorOutput;
import com.ludocrazy.tools.LogOutput;
import com.ludocrazy.tools.RunActivity;

public class RunExcit extends RunActivity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        new ErrorOutput(this, "com.ludocrazy.excit");
        try {
            super.onCreate(savedInstanceState);
            this.DebugLogOutput = new LogOutput(this, "com.ludocrazy.excit");
            this.DebugLogOutput.setCurrentLogLevel(1);
            this.DebugLogOutput.log(1, "Starting Excit -----------------------------------------------------------");
            this.m_MediaManager.setLogOutput(this.DebugLogOutput);
            this.m_ViewRenderer = new GameViewExcit(this, this.m_MediaManager, this.DebugLogOutput, false);
            setContentView(this.m_ViewRenderer);
        } catch (Exception e) {
            new ErrorOutput("RunExcit::onCreate()", "Exception", e);
        }
    }
}
