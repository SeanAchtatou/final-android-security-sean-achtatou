package com.ludocrazy.excit;

import android.content.Context;
import android.content.SharedPreferences;
import com.ludocrazy.excit.MapLogic;
import com.ludocrazy.excit_demo.R;
import com.ludocrazy.tools.ColorFloats;
import com.ludocrazy.tools.ErrorOutput;
import com.ludocrazy.tools.Font;
import com.ludocrazy.tools.GuiMesh;
import com.ludocrazy.tools.LogOutput;
import com.ludocrazy.tools.MediaManager;
import com.ludocrazy.tools.ParticleManager;
import com.ludocrazy.tools.PhoneAbilities;
import com.ludocrazy.tools.Vector2Dint;
import java.lang.reflect.Array;
import javax.microedition.khronos.opengles.GL10;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class GameLevel {
    public static final float FIELD_HEIGHT = 1.0f;
    public static final float FIELD_WIDTH = 1.6452174f;
    private static final int PICKUPS_COUNT = 12;
    public static final int PICKUPS_PER_CLUSTER = 4;
    public static final float Z_GRID = 0.65f;
    public static final float Z_LEVELOBJECT = 0.5f;
    public static final float Z_LEVELOBJECT_CLOSER = 0.55f;
    public static final float Z_LEVELOBJECT_CLOSEST = 0.6f;
    public static final float Z_PFX = 0.95f;
    public static final float Z_PICKUP = 0.75f;
    public static final float Z_PLAYER_OUTLINES = 0.8f;
    public static final float Z_PLAYER_SHADOW = 0.7f;
    public static final float Z_PLAYER_SHAPE = 0.85f;
    private LogOutput DebugLogOutput = null;
    public boolean m_CancelLevel = false;
    private Context m_Context = null;
    private boolean m_CurrentLevelAutoplay = false;
    private int m_CurrentLevelAutoplayIndex = 0;
    private float m_CurrentLevelAutoplayWait = 0.0f;
    private MapLogic.MapField m_CurrentLevelSettings = null;
    private GuiMesh m_DynamicPlayfieldMesh = null;
    public boolean m_LevelCompleted = false;
    private int m_PickupIndex = 0;
    private LevelObjectPickup[] m_PickupObject = new LevelObjectPickup[PICKUPS_COUNT];
    private PlayerObject m_Player = null;
    private LevelObject[][] m_Playfield = null;
    private MediaManager m_pMediaManager = null;
    public boolean m_reDrawNeeded = true;

    public class TeleporterTargetType {
        ColorFloats color;
        Vector2Dint pos;

        public TeleporterTargetType() {
        }
    }

    public void addPickupObject(LevelObjectPickup new_pickup_vis) {
        if (this.m_PickupIndex < PICKUPS_COUNT) {
            this.m_PickupObject[this.m_PickupIndex] = new_pickup_vis;
            this.m_PickupIndex++;
        }
    }

    GameLevel(LogOutput debug_log_output, PhoneAbilities phoneAbilities, Context context, MediaManager pMediaManager, Font pFont, ParticleManager pParticleManager, float gameTimeSeconds, MapLogic.MapField levelSettings, float screenFieldsWidth, float screenFieldsHeight) throws Exception {
        this.DebugLogOutput = debug_log_output;
        this.m_Context = context;
        this.m_pMediaManager = pMediaManager;
        this.m_CurrentLevelSettings = levelSettings;
        this.m_pMediaManager.playStreamedMusic_withDelayBetweenLoops(this.m_CurrentLevelSettings.musicFilename, 2.5f, this.m_CurrentLevelSettings.music_loop_restart_seconds, 0.25f, 0.4f);
        readLevel(this.m_CurrentLevelSettings.levelFilename, pParticleManager, gameTimeSeconds);
        this.m_DynamicPlayfieldMesh = new GuiMesh(debug_log_output, phoneAbilities);
        int i = 16 * 440;
        int i2 = 40 + 7040;
        int i3 = 50 + 7080;
        this.m_DynamicPlayfieldMesh.setupArraysQuadFaces(50 + 7130);
    }

    /* JADX INFO: Multiple debug info for r6v4 org.w3c.dom.Document: [D('is' java.io.InputStream), D('dom' org.w3c.dom.Document)] */
    /* JADX INFO: Multiple debug info for r6v5 org.w3c.dom.Element: [D('dom' org.w3c.dom.Document), D('root' org.w3c.dom.Element)] */
    /* JADX INFO: Multiple debug info for r6v6 org.w3c.dom.NodeList: [D('items' org.w3c.dom.NodeList), D('root' org.w3c.dom.Element)] */
    /* JADX INFO: Multiple debug info for r6v7 org.w3c.dom.NamedNodeMap: [D('items' org.w3c.dom.NodeList), D('attributes' org.w3c.dom.NamedNodeMap)] */
    public static int[][] readLevel_forPreview(Context context, String levelFilename, int map_width, int map_height) {
        int[][] map = (int[][]) Array.newInstance(Integer.TYPE, map_width, map_height);
        try {
            Node level_node = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(context.getAssets().open("level/" + levelFilename, 2)).getDocumentElement().getElementsByTagName("level").item(0);
            NamedNodeMap attributes = level_node.getAttributes();
            int level_width = Integer.parseInt(attributes.getNamedItem("width").getNodeValue());
            int level_height = Integer.parseInt(attributes.getNamedItem("height").getNodeValue());
            int offset_x = 0;
            Node offsetnode = attributes.getNamedItem("offsetx");
            if (offsetnode != null) {
                offset_x = Integer.parseInt(offsetnode.getNodeValue());
            }
            if (level_width > 22 || level_height > 20) {
                throw new Exception("Error: Requeted Size to large!");
            }
            for (int x = 0; x < 22; x++) {
                for (int y = 0; y < 20; y++) {
                    map[x][y] = -1;
                }
            }
            NodeList items = level_node.getChildNodes();
            for (int i = 0; i < items.getLength(); i++) {
                Node item = items.item(i);
                if (item.getNodeName().equalsIgnoreCase("tile")) {
                    String tile_name = item.getFirstChild().getNodeValue();
                    NamedNodeMap attributes2 = item.getAttributes();
                    int pos_x = Integer.parseInt(attributes2.getNamedItem("x").getNodeValue()) + offset_x;
                    int pos_y = Integer.parseInt(attributes2.getNamedItem("y").getNodeValue());
                    if (tile_name.equals("exit")) {
                        map[pos_x][pos_y] = -16746752;
                    } else if (tile_name.startsWith("mis")) {
                        map[pos_x][pos_y] = -21897;
                    } else if (tile_name.equals("key")) {
                        map[pos_x][pos_y] = -103;
                    } else if (tile_name.startsWith("beamer")) {
                        map[pos_x][pos_y] = -5570561;
                    } else if (tile_name.startsWith("arrow") || tile_name.startsWith("deflector") || tile_name.startsWith("oneway")) {
                        map[pos_x][pos_y] = -986896;
                    } else {
                        map[pos_x][pos_y] = -16777216;
                    }
                }
            }
            return map;
        } catch (Exception e) {
            new ErrorOutput("GameLevel.readLevel_forPreview()", "Exception parsing xml file: " + levelFilename, e);
        }
    }

    private void readLevel(String levelFilename, ParticleManager pParticleManager, float gameTimeSeconds) {
        try {
            Node level_node = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(this.m_Context.getAssets().open("level/" + levelFilename, 2)).getDocumentElement().getElementsByTagName("level").item(0);
            NamedNodeMap attributes = level_node.getAttributes();
            int level_width = Integer.parseInt(attributes.getNamedItem("width").getNodeValue());
            int level_height = Integer.parseInt(attributes.getNamedItem("height").getNodeValue());
            int offset_x = 0;
            Node offsetnode = attributes.getNamedItem("offsetx");
            if (offsetnode != null) {
                offset_x = Integer.parseInt(offsetnode.getNodeValue());
            }
            this.DebugLogOutput.log(2, "loading level " + levelFilename + " with width:" + level_width + " height:" + level_height);
            if (level_width > 22 || level_height > 20) {
                throw new Exception("Error: Requeted Size to large!");
            }
            this.m_Playfield = (LevelObject[][]) Array.newInstance(LevelObject.class, 22, 20);
            for (int x = 0; x < 22; x++) {
                for (int y = 0; y < 20; y++) {
                    this.m_Playfield[x][y] = null;
                }
            }
            NodeList items = level_node.getChildNodes();
            for (int i = 0; i < items.getLength(); i++) {
                Node item = items.item(i);
                if (item.getNodeName().equalsIgnoreCase("tile")) {
                    String tile_name = item.getFirstChild().getNodeValue();
                    NamedNodeMap attributes2 = item.getAttributes();
                    int pos_x = offset_x + Integer.parseInt(attributes2.getNamedItem("x").getNodeValue());
                    int pos_y = Integer.parseInt(attributes2.getNamedItem("y").getNodeValue());
                    this.DebugLogOutput.log(2, "x:" + pos_x + " y:" + pos_y + " =" + tile_name);
                    int object_category = 0;
                    if (tile_name.equals("deflector left up")) {
                        object_category = -12;
                    }
                    if (tile_name.equals("deflector left down")) {
                        object_category = -11;
                    }
                    if (tile_name.equals("deflector right up")) {
                        object_category = -10;
                    }
                    if (tile_name.equals("deflector right down")) {
                        object_category = -9;
                    }
                    if (tile_name.equals("arrow left")) {
                        object_category = -8;
                    }
                    if (tile_name.equals("arrow right")) {
                        object_category = -7;
                    }
                    if (tile_name.equals("arrow up")) {
                        object_category = -6;
                    }
                    if (tile_name.equals("arrow down")) {
                        object_category = -5;
                    }
                    if (tile_name.equals("oneway left")) {
                        object_category = -4;
                    }
                    if (tile_name.equals("oneway right")) {
                        object_category = -3;
                    }
                    if (tile_name.equals("oneway up")) {
                        object_category = -2;
                    }
                    if (tile_name.equals("oneway down")) {
                        object_category = -1;
                    }
                    if (tile_name.equals("beamer1")) {
                        object_category = 1;
                    }
                    if (tile_name.equals("beamer2")) {
                        object_category = 2;
                    }
                    if (tile_name.equals("beamer3")) {
                        object_category = 3;
                    }
                    if (tile_name.equals("beamer4")) {
                        object_category = 4;
                    }
                    if (tile_name.equals("beamer5")) {
                        object_category = 5;
                    }
                    if (tile_name.equals("mis1")) {
                        object_category = -18;
                    }
                    if (tile_name.equals("mis2")) {
                        object_category = -18;
                    }
                    if (tile_name.equals("mis3")) {
                        object_category = -18;
                    }
                    if (tile_name.equals("exit")) {
                        object_category = -17;
                    }
                    if (tile_name.equals("wall")) {
                        object_category = -14;
                    }
                    if (tile_name.equals("key")) {
                        object_category = -15;
                    }
                    if (tile_name.equals("door")) {
                        object_category = -16;
                    }
                    if (tile_name.equals("start")) {
                        this.m_Player = new PlayerObject(this.DebugLogOutput, this.m_pMediaManager, pParticleManager, this, pos_x, pos_y, this.m_Playfield);
                    } else {
                        this.m_Playfield[pos_x][pos_y] = new LevelObject(this.DebugLogOutput, pParticleManager, gameTimeSeconds, this, pos_x, pos_y, object_category, null);
                    }
                }
            }
            for (int x2 = 1; x2 < 21; x2++) {
                for (int y2 = 1; y2 < 20; y2++) {
                    if (this.m_Playfield[x2][y2] != null && this.m_Playfield[x2][y2].isWall()) {
                        this.m_Playfield[x2][y2].setNeighbourFields(this.m_Playfield[x2 - 1][y2 - 1], this.m_Playfield[x2][y2 - 1], this.m_Playfield[x2 - 1][y2], this.m_Playfield[x2 + 1][y2 - 1]);
                    }
                }
            }
            for (int x3 = 1; x3 < 22; x3++) {
                if (this.m_Playfield[x3][0] != null && this.m_Playfield[x3][0].isWall()) {
                    this.m_Playfield[x3][0].setNeighbourFields(null, null, this.m_Playfield[x3 - 1][0], null);
                }
            }
            for (int y3 = 1; y3 < 20; y3++) {
                if (this.m_Playfield[0][y3] != null && this.m_Playfield[0][y3].isWall()) {
                    this.m_Playfield[0][y3].setNeighbourFields(null, this.m_Playfield[0][y3 - 1], null, this.m_Playfield[1][y3 - 1]);
                }
                if (this.m_Playfield[21][y3] != null && this.m_Playfield[21][y3].isWall()) {
                    this.m_Playfield[21][y3].setNeighbourFields(null, this.m_Playfield[21][y3 - 1], null, null);
                }
            }
        } catch (Exception e) {
            new ErrorOutput("GameLevel.readLevel()", "Exception parsing xml file: " + levelFilename, e);
        }
    }

    public void restartLevel() {
        if (!this.m_CurrentLevelAutoplay) {
            this.m_pMediaManager.playSoundEffect((int) R.string.sound_level_restartLevel);
        }
        for (int x = 0; x < 22; x++) {
            for (int y = 0; y < 20; y++) {
                if (this.m_Playfield[x][y] != null) {
                    this.m_Playfield[x][y].resetToStart();
                }
            }
        }
        for (int i = 0; i < this.m_PickupIndex; i++) {
            this.m_PickupObject[i].resetToStart();
        }
        this.m_Player.resetToStart();
    }

    public void doAutoplayCurrentLevel() {
        if (!this.m_LevelCompleted) {
            this.m_CurrentLevelAutoplay = true;
            restartLevel();
            this.m_Player.setPlayerSpeed(true);
        }
    }

    public void doMove(int x_change, int y_change) {
        if (!this.m_LevelCompleted) {
            this.m_Player.doMove(x_change, y_change);
        }
    }

    public boolean hasPlayerKeys() {
        return this.m_Player.getKeyCount() > 0;
    }

    public void resetPlayerBurning() {
        this.m_Player.resetBurning();
    }

    /* access modifiers changed from: package-private */
    public TeleporterTargetType findTeleporterPartner(int teleporter_start_x, int teleporter_start_y) {
        ColorFloats color;
        int teleporter_id = this.m_Playfield[teleporter_start_x][teleporter_start_y].getCategory();
        for (int x = 0; x < 22; x++) {
            int y = 0;
            while (y < 20) {
                if (this.m_Playfield[x][y] == null || ((x == teleporter_start_x && y == teleporter_start_y) || (color = this.m_Playfield[x][y].isTeleporterPartner_getColor(teleporter_id)) == null)) {
                    y++;
                } else {
                    TeleporterTargetType target = new TeleporterTargetType();
                    target.color = color;
                    target.pos = new Vector2Dint(x, y);
                    return target;
                }
            }
        }
        return null;
    }

    private void simulate(float duration) {
        if (this.m_CurrentLevelAutoplay && !this.m_Player.isMoving()) {
            this.m_CurrentLevelAutoplayWait += duration;
            if (this.m_CurrentLevelAutoplayWait > 0.4f) {
                this.m_CurrentLevelAutoplayWait = 0.0f;
                if (this.m_CurrentLevelAutoplayIndex < this.m_CurrentLevelSettings.solution.length()) {
                    this.m_Player.doMove(this.m_CurrentLevelSettings.solution.charAt(this.m_CurrentLevelAutoplayIndex));
                }
                this.m_CurrentLevelAutoplayIndex++;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void draw(GL10 gl, float gameTimeSeconds) {
        try {
            simulate(0.02f);
            this.m_DynamicPlayfieldMesh.reset(true);
            float end_u = 0.107421875f + 0.05859375f;
            float end_v = 0.8496094f + 0.05859375f;
            for (int x = 0; x < 22; x++) {
                float posx = -17.274782f + (1.6452174f * ((float) x));
                this.m_DynamicPlayfieldMesh.addQuad(posx, -9.0f, 0.65f, GameViewExcit.PIXEL_WIDTH + posx, 11.0f, end_u, end_v, 0.107421875f, 0.8496094f);
                this.m_DynamicPlayfieldMesh.addColorForLastQuad(0.8f, 0.8f, 0.8f, 1.0f);
            }
            for (int y = 0; y < 20; y++) {
                float posy = (1.0f * ((float) y)) - 0.46875f;
                this.m_DynamicPlayfieldMesh.addQuad(-17.274782f, posy, 0.65f, 18.92f, posy + GameViewExcit.PIXEL_HEIGHT, end_u, end_v, 0.107421875f, 0.8496094f);
                this.m_DynamicPlayfieldMesh.addColorForLastQuad(0.8f, 0.8f, 0.8f, 1.0f);
            }
            for (int x2 = 0; x2 < 22; x2++) {
                for (int y2 = 0; y2 < 20; y2++) {
                    if (this.m_Playfield[x2][y2] != null) {
                        this.m_Playfield[x2][y2].draw(this.m_DynamicPlayfieldMesh);
                    }
                }
            }
            for (int i = 0; i < this.m_PickupIndex; i++) {
                this.m_PickupObject[i].draw(this.m_DynamicPlayfieldMesh);
            }
            this.m_Player.draw(this.m_DynamicPlayfieldMesh, gameTimeSeconds);
            this.m_DynamicPlayfieldMesh.convertArraysToBuffer();
            gl.glScalef(1.0f, -1.0f, 1.0f);
            this.m_DynamicPlayfieldMesh.draw(gl);
            this.m_reDrawNeeded = true;
        } catch (Exception e) {
            new ErrorOutput("GameLevel.draw()", "Exception", e);
        }
    }

    /* access modifiers changed from: package-private */
    public void onTouch(float x_percentage, float y_percentage, boolean controlsEnabled_TouchPosition) throws Exception {
        if (controlsEnabled_TouchPosition) {
            this.m_Player.doMoveToField(Math.round(23.0f * x_percentage) - 1, Math.round(22.0f * (1.0f - y_percentage)) - 1);
        }
    }

    /* access modifiers changed from: package-private */
    public boolean onKeyDown(int keyCode, boolean controlsEnabled_Cursor, boolean controlsEnabled_WASD, boolean controlsEnabled_IJKL) throws Exception {
        boolean eventHasBeenHandled = false;
        switch (keyCode) {
            case 4:
                eventHasBeenHandled = true;
                this.m_CancelLevel = true;
                this.m_pMediaManager.playSoundEffect(this.m_Context.getResources().getString(R.string.sound_map_level_canceled));
                break;
        }
        if (controlsEnabled_Cursor) {
            switch (keyCode) {
                case 19:
                    doMove(0, -1);
                    break;
                case GameViewExcit.PLAYFIELD_HEIGHT /*20*/:
                    doMove(0, 1);
                    break;
                case 21:
                    doMove(-1, 0);
                    break;
                case GameViewExcit.PLAYFIELD_WIDTH /*22*/:
                    doMove(1, 0);
                    break;
            }
        }
        if (controlsEnabled_WASD) {
            switch (keyCode) {
                case 29:
                    doMove(-1, 0);
                    break;
                case 32:
                    doMove(1, 0);
                    break;
                case 47:
                    doMove(0, 1);
                    break;
                case 51:
                    doMove(0, -1);
                    break;
            }
        }
        if (controlsEnabled_IJKL) {
            switch (keyCode) {
                case 37:
                    doMove(0, -1);
                    break;
                case 38:
                    doMove(-1, 0);
                    break;
                case 39:
                    doMove(0, 1);
                    break;
                case 40:
                    doMove(1, 0);
                    break;
            }
        }
        return eventHasBeenHandled;
    }

    public int getCurrentDrawCount() {
        if (this.m_DynamicPlayfieldMesh != null) {
            return this.m_DynamicPlayfieldMesh.getVerticesCount();
        }
        return 0;
    }

    /* access modifiers changed from: package-private */
    public boolean canBeUnloaded() {
        return this.m_CancelLevel || isLevelCompleted();
    }

    /* access modifiers changed from: package-private */
    public boolean isLevelCompleted() {
        return this.m_LevelCompleted;
    }

    public void setAsCompleted(int pickupsCollected, int movesMade) {
        this.m_LevelCompleted = true;
        if (!this.m_CurrentLevelAutoplay) {
            String current_level_id = this.m_CurrentLevelSettings.charFieldId;
            SharedPreferences prefs = this.m_Context.getSharedPreferences(GameViewExcit.SHAREDPREFSNAME, 1);
            int best_moves = prefs.getInt("BestMoves_" + current_level_id, 9999);
            int best_pickups = prefs.getInt("BestPickups_" + current_level_id, 0);
            if (pickupsCollected > best_pickups) {
                best_moves = movesMade;
                best_pickups = pickupsCollected;
            } else if (pickupsCollected == best_pickups && movesMade < best_moves) {
                best_moves = movesMade;
            }
            SharedPreferences.Editor editor = prefs.edit();
            editor.putBoolean("LevelDone_" + current_level_id, true);
            editor.putInt("BestMoves_" + current_level_id, best_moves);
            editor.putInt("BestPickups_" + current_level_id, best_pickups);
            editor.commit();
            this.DebugLogOutput.log(2, "GameLevel.setAsCompleted() current moves:" + movesMade + " pickups:" + pickupsCollected + ", best moves:" + best_moves + " pickups:" + best_pickups + ". saved preferences for level:" + current_level_id);
            this.m_pMediaManager.stopStreamedMusic_DelayedLoopsThread(true);
        }
    }
}
