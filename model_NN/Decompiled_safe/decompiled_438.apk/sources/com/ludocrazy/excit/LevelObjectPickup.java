package com.ludocrazy.excit;

import com.ludocrazy.tools.GuiMesh;
import com.ludocrazy.tools.MediaManager;
import com.ludocrazy.tools.PhoneAbilities;
import com.ludocrazy.tools.Sproxel;
import com.ludocrazy.tools.Tools;
import com.ludocrazy.tools.UvCoords;
import com.ludocrazy.tools.Vector3D;

public class LevelObjectPickup {
    private static final int POS_COUNT = 5;
    private float m_Alpha = 0.0f;
    private float m_AlphaTarget = 1.0f;
    private float m_FieldPosX = 0.0f;
    private float m_FieldPosY = 0.0f;
    private float m_ObjectHalfSizeX = 1.0f;
    private float m_ObjectHalfSizeY = 1.0f;
    private boolean m_PickedUp = false;
    private Vector3D m_Pos = new Vector3D();
    private float m_PositionAnimationBlendFactor = 0.0f;
    private Vector3D[] m_Positions = new Vector3D[5];
    private int m_StartPosX = 0;
    private int m_StartPosY = 0;
    private UvCoords m_UvCoordsEnd = new UvCoords(0.18554688f, 0.9277344f);
    private UvCoords m_UvCoordsStart = new UvCoords(0.087890625f, 0.8300781f);

    public static int[] getRandomFormula() {
        int pixel_start_x = 0;
        int pixel_start_y = 0;
        int pixel_size_x = 0;
        int pixel_size_y = 0;
        switch (Tools.randomInt(0, 12)) {
            case 0:
                pixel_start_x = 469;
                pixel_start_y = 939;
                pixel_size_x = 39;
                pixel_size_y = 15;
                break;
            case 1:
                pixel_start_x = 429;
                pixel_start_y = 933;
                pixel_size_x = 41;
                pixel_size_y = 26;
                break;
            case PhoneAbilities.ABILITY_GLUTILS_TEXSUBIMAGE2D_NONSQUARE /*2*/:
                pixel_start_x = 391;
                pixel_start_y = 936;
                pixel_size_x = 39;
                pixel_size_y = 19;
                break;
            case PhoneAbilities.ABILITY_VBO_GL11 /*3*/:
                pixel_start_x = 351;
                pixel_start_y = 935;
                pixel_size_x = 40;
                pixel_size_y = 22;
                break;
            case 4:
                pixel_start_x = 312;
                pixel_start_y = 933;
                pixel_size_x = 39;
                pixel_size_y = 25;
                break;
            case 5:
                pixel_start_x = 274;
                pixel_start_y = 936;
                pixel_size_x = 39;
                pixel_size_y = 19;
                break;
            case PhoneAbilities.ABILITY_POINT_SIZE_ARRAY_OES /*6*/:
                pixel_start_x = 232;
                pixel_start_y = 935;
                pixel_size_x = 42;
                pixel_size_y = 23;
                break;
            case MediaManager.MAX_MULTI_LAYERS /*7*/:
                pixel_start_x = 292;
                pixel_start_y = 893;
                pixel_size_x = 59;
                pixel_size_y = 40;
                break;
            case 8:
                pixel_start_x = 234;
                pixel_start_y = 899;
                pixel_size_x = 58;
                pixel_size_y = 27;
                break;
            case 9:
                pixel_start_x = 293;
                pixel_start_y = 857;
                pixel_size_x = 56;
                pixel_size_y = 35;
                break;
            case Sproxel.SPROXELS_PER_FIELD /*10*/:
                pixel_start_x = 233;
                pixel_start_y = 862;
                pixel_size_x = 61;
                pixel_size_y = 28;
                break;
            case 11:
                pixel_start_x = 295;
                pixel_start_y = 833;
                pixel_size_x = 53;
                pixel_size_y = 24;
                break;
            case 12:
                pixel_start_x = 235;
                pixel_start_y = 833;
                pixel_size_x = 56;
                pixel_size_y = 24;
                break;
        }
        return new int[]{pixel_start_x, pixel_start_y, pixel_size_x, pixel_size_y};
    }

    public LevelObjectPickup(int start_x, int start_y) {
        this.m_StartPosX = start_x;
        this.m_StartPosY = start_y;
        this.m_FieldPosX = (1.6452174f * (((float) start_x) + 0.5f)) - 0.24004076f;
        this.m_FieldPosY = (1.0f * (((float) start_y) + 0.5f)) - 0.46875f;
        for (int i = 0; i < 5; i++) {
            this.m_Positions[i] = new Vector3D();
            createRandomPoint(this.m_Positions[i]);
        }
        int[] pixels = getRandomFormula();
        int pixel_start_x = pixels[0];
        int pixel_start_y = pixels[1];
        int pixel_size_x = pixels[2];
        int pixel_size_y = pixels[3];
        this.m_UvCoordsStart.u = ((float) pixel_start_x) / 1024.0f;
        this.m_UvCoordsStart.v = ((float) pixel_start_y) / 1024.0f;
        this.m_UvCoordsEnd.u = this.m_UvCoordsStart.u + (((float) pixel_size_x) / 1024.0f);
        this.m_UvCoordsEnd.v = this.m_UvCoordsStart.v + (((float) pixel_size_y) / 1024.0f);
        float scale = 39.0f / ((float) pixel_size_x);
        this.m_ObjectHalfSizeX = (((0.5f * scale) * 1.6452174f) * ((float) pixel_size_x)) / 37.0f;
        this.m_ObjectHalfSizeY = (((0.5f * scale) * 1.0f) * ((float) pixel_size_y)) / 21.0f;
    }

    public void setAsPickedUp(int movementDirection_x, int movementDirection_y, int index) {
        this.m_PickedUp = true;
        this.m_Positions[0].copyFrom(this.m_Positions[1]);
        this.m_Positions[1].copyFrom(this.m_Positions[2]);
        this.m_Positions[2].copyFrom(this.m_Pos);
        this.m_PositionAnimationBlendFactor = 0.0f;
        this.m_Positions[3].x = (float) (this.m_StartPosX + ((index + 1) * movementDirection_x));
        this.m_Positions[3].y = (float) (this.m_StartPosY + ((index + 1) * movementDirection_y));
        for (int i = 4; i < 5; i++) {
            this.m_Positions[i].x = this.m_Positions[3].x + ((float) Tools.randomInt(-1, 1)) + ((float) (Tools.randomInt(1, i - 2) * movementDirection_x));
            this.m_Positions[i].y = this.m_Positions[3].y + ((float) Tools.randomInt(-1, 1)) + ((float) (Tools.randomInt(1, i - 2) * movementDirection_y));
        }
        for (int i2 = 3; i2 < 5; i2++) {
            if (this.m_Positions[i2].x < 0.0f) {
                this.m_Positions[i2].x = Math.abs(this.m_Positions[i2].x);
            } else if (this.m_Positions[i2].x >= 22.0f) {
                this.m_Positions[i2].x = (22.0f - (this.m_Positions[i2].x - 22.0f)) - 1.0f;
            }
            if (this.m_Positions[i2].y < 0.0f) {
                this.m_Positions[i2].y = Math.abs(this.m_Positions[i2].y);
            } else if (this.m_Positions[i2].y >= 20.0f) {
                this.m_Positions[i2].y = (20.0f - (this.m_Positions[i2].y - 20.0f)) - 1.0f;
            }
            this.m_Positions[i2].x = (1.6452174f * (this.m_Positions[i2].x + 0.5f)) - 0.24004076f;
            this.m_Positions[i2].y = ((this.m_Positions[i2].y + 0.5f) * 1.0f) - 0.46875f;
        }
    }

    public void resetToStart() {
        this.m_PickedUp = false;
        this.m_AlphaTarget = 1.0f;
        this.m_Positions[0].copyFrom(this.m_Positions[1]);
        this.m_Positions[1].copyFrom(this.m_Positions[2]);
        this.m_Positions[2].copyFrom(this.m_Pos);
        this.m_PositionAnimationBlendFactor = 0.0f;
        this.m_Positions[3].x = this.m_FieldPosX;
        this.m_Positions[3].y = this.m_FieldPosY;
        for (int i = 4; i < 5; i++) {
            createRandomPoint(this.m_Positions[i]);
        }
    }

    private void createRandomPoint(Vector3D point_to_be_modified) {
        point_to_be_modified.x = this.m_FieldPosX + Tools.random(-1.0f * 0.2f * 1.6452174f, 1.6452174f * 0.2f);
        point_to_be_modified.y = this.m_FieldPosY + Tools.random(-1.0f * 0.2f * 1.0f, 0.2f * 1.0f);
    }

    private void simulate(float duration) {
        this.m_PositionAnimationBlendFactor += duration / 0.5f;
        if (this.m_PositionAnimationBlendFactor >= 1.0f) {
            this.m_PositionAnimationBlendFactor -= (float) ((int) this.m_PositionAnimationBlendFactor);
            Vector3D temp = this.m_Positions[0];
            for (int i = 0; i < 4; i++) {
                this.m_Positions[i] = this.m_Positions[i + 1];
            }
            this.m_Positions[4] = temp;
            if (!this.m_PickedUp) {
                createRandomPoint(this.m_Positions[4]);
            } else {
                this.m_Positions[4].copyFrom(this.m_Positions[3]);
                this.m_AlphaTarget -= 0.2f;
                if (this.m_AlphaTarget < 0.0f) {
                    this.m_AlphaTarget = 0.0f;
                }
            }
        }
        float duration2 = duration * 2.0f;
        this.m_Alpha = (this.m_Alpha * (1.0f - duration2)) + (this.m_AlphaTarget * duration2);
        this.m_Pos.copyFrom(this.m_Positions[2]);
        this.m_Pos.interpolate_catmullrom(this.m_Positions[0], this.m_Positions[1], this.m_Positions[3], this.m_PositionAnimationBlendFactor);
    }

    public void draw(GuiMesh dynamic_playfield_mesh) throws Exception {
        if (this.m_Alpha > 0.01f || this.m_AlphaTarget > 0.0f) {
            simulate(0.02f);
            dynamic_playfield_mesh.addQuad(this.m_Pos.x - this.m_ObjectHalfSizeX, this.m_Pos.y - this.m_ObjectHalfSizeY, 0.75f, this.m_ObjectHalfSizeX + this.m_Pos.x, this.m_ObjectHalfSizeY + this.m_Pos.y, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
            dynamic_playfield_mesh.addColorForLastQuad(1.0f, 1.0f, 1.0f, this.m_Alpha);
        }
    }
}
