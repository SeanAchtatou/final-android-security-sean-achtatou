package com.ludocrazy.excit;

import com.ludocrazy.excit.GameLevel;
import com.ludocrazy.excit_demo.R;
import com.ludocrazy.tools.ColorFloats;
import com.ludocrazy.tools.GuiMesh;
import com.ludocrazy.tools.LogOutput;
import com.ludocrazy.tools.MediaManager;
import com.ludocrazy.tools.ParticleManager;
import com.ludocrazy.tools.UvCoords;
import com.ludocrazy.tools.Vector2D;
import com.ludocrazy.tools.Vector2Dint;

public class PlayerObject {
    private static final float PLAYER_SPEED_NORMAL = 35.0f;
    private static final float PLAYER_SPEED_SLOW = 25.0f;
    private LogOutput DebugLogOutput = null;
    private float m_Appear = 0.0f;
    private int m_BurnSoundStreamId = 0;
    private boolean m_Burning = false;
    private Vector2D m_DisplayPos = new Vector2D(0.0f, 0.0f);
    private Vector2Dint m_FieldPos = new Vector2Dint(0, 0);
    private int m_KeysCount = 0;
    private Vector2D m_MoveAnimation = new Vector2D(0.0f, 0.0f);
    private Vector2Dint m_MovementDirection = new Vector2Dint(0, 0);
    private int m_MovesMade = 0;
    private boolean m_Moving = false;
    private int m_ParticleEmitterId = -999;
    private ParticleManager m_ParticleManager = null;
    private int m_PickupsCollected = 0;
    private int m_PlayMoveSound = 0;
    private float m_PlayerSpeed = PLAYER_SPEED_NORMAL;
    private ColorFloats m_ShapeColor = new ColorFloats(1.0f, 1.0f, 1.0f, 1.0f);
    private float m_SpeedFactor = 0.0f;
    private float m_SpeedFactorTarget = 0.0f;
    private Vector2Dint m_StartPos = new Vector2Dint(0, 0);
    private boolean m_StopParticleEffect = false;
    private GameLevel.TeleporterTargetType m_TeleportTarget = null;
    private UvCoords m_UvCoordsEnd = new UvCoords(0.99902344f, 0.98828125f);
    private UvCoords m_UvCoordsStart = new UvCoords(0.98535156f, 0.97753906f);
    private GameLevel m_pGameLevel = null;
    private MediaManager m_pMediaManager = null;
    private LevelObject[][] m_pPlayfield = null;

    PlayerObject(LogOutput debugLog, MediaManager pMediaManager, ParticleManager pParticleManager, GameLevel pGameLevel, int start_x, int start_y, LevelObject[][] pPlayfield) {
        this.DebugLogOutput = debugLog;
        this.m_pMediaManager = pMediaManager;
        this.m_pGameLevel = pGameLevel;
        this.m_pPlayfield = pPlayfield;
        this.m_ParticleManager = pParticleManager;
        this.m_Appear = 0.0f;
        this.m_StartPos.x = start_x;
        this.m_StartPos.y = start_y;
        resetToStart();
    }

    public void resetToStart() {
        stopMovement();
        resetBurning();
        this.m_KeysCount = 0;
        this.m_PickupsCollected = 0;
        this.m_FieldPos.x = this.m_StartPos.x;
        this.m_FieldPos.y = this.m_StartPos.y;
        this.m_DisplayPos.x = (float) this.m_StartPos.x;
        this.m_DisplayPos.y = (float) this.m_StartPos.y;
    }

    public int getKeyCount() {
        return this.m_KeysCount;
    }

    public boolean isMoving() {
        return this.m_Moving;
    }

    public void doMoveToField(int to_field_pos_x, int to_field_pos_y) {
        if (Math.abs(this.m_FieldPos.x - to_field_pos_x) < Math.abs(this.m_FieldPos.y - to_field_pos_y)) {
            if (to_field_pos_y < this.m_FieldPos.y) {
                doMove(0, -1);
            } else if (to_field_pos_y > this.m_FieldPos.y) {
                doMove(0, 1);
            }
        } else if (to_field_pos_x < this.m_FieldPos.x) {
            doMove(-1, 0);
        } else if (to_field_pos_x > this.m_FieldPos.x) {
            doMove(1, 0);
        }
    }

    public void doMove(char direction) {
        switch (direction) {
            case 'D':
                doMove(0, 1);
                return;
            case 'L':
                doMove(-1, 0);
                return;
            case 'R':
                doMove(1, 0);
                return;
            case 'U':
                doMove(0, -1);
                return;
            default:
                return;
        }
    }

    public void doMove(int x_change, int y_change) {
        if (!this.m_Moving) {
            if (x_change > 1) {
                x_change = 1;
            }
            if (x_change < -1) {
                x_change = -1;
            }
            if (y_change > 1) {
                y_change = 1;
            }
            if (y_change < -1) {
                y_change = -1;
            }
            this.m_MovementDirection.x = x_change;
            this.m_MovementDirection.y = y_change;
            this.m_SpeedFactorTarget = 1.0f;
            this.m_Moving = true;
            this.m_PlayMoveSound = 1;
            return;
        }
        this.DebugLogOutput.log(2, "PlayerObject.doMove() can't do new move, because currently already moving!");
    }

    private void stopMovement() {
        this.m_MoveAnimation.x = (float) this.m_MovementDirection.x;
        this.m_MoveAnimation.y = (float) this.m_MovementDirection.y;
        this.m_Moving = false;
        this.m_MovementDirection.x = 0;
        this.m_MovementDirection.y = 0;
        this.m_DisplayPos.x = (float) this.m_FieldPos.x;
        this.m_DisplayPos.y = (float) this.m_FieldPos.y;
        this.m_SpeedFactorTarget = 0.0f;
    }

    public void resetBurning() {
        if (this.m_Burning) {
            this.m_pMediaManager.playSoundEffect((int) R.string.sound_level_burnWall);
            this.m_pMediaManager.stopSoundEffeect(this.m_BurnSoundStreamId);
        }
        this.m_BurnSoundStreamId = 0;
        this.m_StopParticleEffect = true;
        this.m_Burning = false;
    }

    public void setPlayerSpeed(boolean replay_slow) {
        if (replay_slow) {
            this.m_PlayerSpeed = PLAYER_SPEED_SLOW;
        } else {
            this.m_PlayerSpeed = PLAYER_SPEED_NORMAL;
        }
    }

    private void simulate(float duration, float gameTimeSeconds) {
        this.m_MoveAnimation.shrinkTowardsZero(10.0f * duration);
        this.m_Appear = this.m_Appear + 0.075f;
        if (this.m_Appear > 1.0f) {
            this.m_Appear = 1.0f;
        }
        if (this.m_TeleportTarget != null) {
            this.m_DisplayPos.x = (this.m_DisplayPos.x * 0.85f) + (0.15f * ((float) this.m_TeleportTarget.pos.x));
            this.m_DisplayPos.y = (this.m_DisplayPos.y * 0.85f) + (0.15f * ((float) this.m_TeleportTarget.pos.y));
            if (this.m_TeleportTarget.pos.x == Math.round(this.m_DisplayPos.x) && this.m_TeleportTarget.pos.y == Math.round(this.m_DisplayPos.y)) {
                this.m_FieldPos.x = this.m_TeleportTarget.pos.x;
                this.m_FieldPos.y = this.m_TeleportTarget.pos.y;
                this.m_TeleportTarget = null;
                this.m_StopParticleEffect = true;
            }
        } else if (this.m_Moving) {
            this.m_SpeedFactor = (this.m_SpeedFactor * 0.9f) + (0.1f * this.m_SpeedFactorTarget);
            float distance_moved = this.m_SpeedFactor * duration * this.m_PlayerSpeed;
            boolean new_field_reached = false;
            int potentialFieldPos_x = Math.round(this.m_DisplayPos.x + (((float) this.m_MovementDirection.x) * distance_moved));
            int potentialFieldPos_y = Math.round(this.m_DisplayPos.y + (((float) this.m_MovementDirection.y) * distance_moved));
            if (!(potentialFieldPos_x == this.m_FieldPos.x && potentialFieldPos_y == this.m_FieldPos.y)) {
                new_field_reached = true;
            }
            pickupPossibleObject(this.m_FieldPos.x, this.m_FieldPos.y, gameTimeSeconds);
            int new_field_x = this.m_FieldPos.x;
            int new_field_y = this.m_FieldPos.y;
            while (true) {
                if (distance_moved < 1.0f && !new_field_reached) {
                    break;
                }
                new_field_reached = false;
                distance_moved -= 1.0f;
                new_field_x += this.m_MovementDirection.x;
                new_field_y += this.m_MovementDirection.y;
                if (!hasLeftPlayfield_doRestart(new_field_x, new_field_y)) {
                    if (!moveToField_ifPossible(new_field_x, new_field_y, gameTimeSeconds)) {
                        stopMovement();
                        distance_moved = 0.0f;
                        break;
                    }
                    if (this.m_PlayMoveSound == 1) {
                        this.m_MovesMade = this.m_MovesMade + 1;
                        if (this.m_MovementDirection.x == -1) {
                            this.DebugLogOutput.log(2, "PlayerObject doMove direction: L");
                        } else if (this.m_MovementDirection.x == 1) {
                            this.DebugLogOutput.log(2, "PlayerObject doMove direction: R");
                        } else if (this.m_MovementDirection.y == -1) {
                            this.DebugLogOutput.log(2, "PlayerObject doMove direction: U");
                        } else if (this.m_MovementDirection.y == 1) {
                            this.DebugLogOutput.log(2, "PlayerObject doMove direction: D");
                        }
                        this.m_pMediaManager.playSoundEffect((int) R.string.sound_level_doMove);
                        this.m_PlayMoveSound = 2;
                    }
                    boolean state_changed = changeState_ifPossible(new_field_x, new_field_y, gameTimeSeconds);
                    pickupPossibleObject(new_field_x, new_field_y, gameTimeSeconds);
                    this.m_FieldPos.x = new_field_x;
                    this.m_FieldPos.y = new_field_y;
                    this.m_DisplayPos.x = (float) new_field_x;
                    this.m_DisplayPos.y = (float) new_field_y;
                    if (state_changed) {
                        break;
                    }
                } else {
                    return;
                }
            }
            if (distance_moved > 0.0f) {
                this.m_DisplayPos.x += ((float) this.m_MovementDirection.x) * distance_moved;
                this.m_DisplayPos.y += ((float) this.m_MovementDirection.y) * distance_moved;
            }
        }
        if (this.m_ParticleEmitterId > -999) {
            this.m_ParticleManager.SetEffectPosition(this.m_ParticleEmitterId, ((1.6452174f * this.m_DisplayPos.x) - 0.24004076f) + 0.8226087f, ((1.0f * this.m_DisplayPos.y) - 0.46875f) + 0.5f, 0.95f);
            if (this.m_StopParticleEffect) {
                this.m_ParticleManager.StopLoadedEffect(this.m_ParticleEmitterId);
                this.m_ParticleEmitterId = -999;
                this.m_StopParticleEffect = false;
            }
        }
    }

    private boolean hasLeftPlayfield_doRestart(int new_field_x, int new_field_y) {
        boolean playfield_left = new_field_x < 0 || new_field_y < 0 || new_field_x >= 22 || new_field_y >= 20;
        if (playfield_left) {
            this.m_pGameLevel.restartLevel();
        }
        return playfield_left;
    }

    private boolean isFieldSet(int field_x, int field_y) {
        return this.m_pPlayfield[field_x][field_y] != null;
    }

    private boolean moveToField_ifPossible(int field_x, int field_y, float gameTimeSeconds) {
        boolean move_is_possible = true;
        if (isFieldSet(field_x, field_y)) {
            move_is_possible = this.m_pPlayfield[field_x][field_y].moveToField_ifPossible(this.m_MovementDirection.x, this.m_MovementDirection.y, this.m_Burning, gameTimeSeconds, this.m_BurnSoundStreamId, this.m_pMediaManager);
            if (this.m_pPlayfield[field_x][field_y].isDoor_isVisible()) {
                if (!this.m_pPlayfield[field_x][field_y].mustBeDoor_openIfPossible(this.m_KeysCount, this.m_MovementDirection.x, this.m_MovementDirection.y)) {
                    move_is_possible = false;
                } else {
                    this.m_KeysCount--;
                    this.m_pMediaManager.playSoundEffect((int) R.string.sound_level_openDoor);
                }
            }
            if (this.m_pPlayfield[field_x][field_y].isDeflector() && (move_is_possible = this.m_pPlayfield[field_x][field_y].mustBeDeflector_moveIfPossible_changeDirection(this.m_MovementDirection))) {
                this.m_pMediaManager.playSoundEffect((int) R.string.sound_level_deflector);
            }
            if (!move_is_possible) {
                if (this.m_Burning) {
                    resetBurning();
                } else if (this.m_PlayMoveSound == 2) {
                    this.m_pMediaManager.playSoundEffect((int) R.string.sound_level_hitWall);
                    this.m_PlayMoveSound = 0;
                }
            }
        }
        return move_is_possible;
    }

    private void pickupPossibleObject(int field_x, int field_y, float gameTimeSeconds) {
        if (!isFieldSet(field_x, field_y)) {
            return;
        }
        if (this.m_pPlayfield[field_x][field_y].pickupPickup(this.m_MovementDirection.x, this.m_MovementDirection.y)) {
            this.m_PickupsCollected++;
            this.m_pMediaManager.playSoundEffect((int) R.string.sound_level_collectPickup);
        } else if (this.m_pPlayfield[field_x][field_y].pickupKey()) {
            this.m_KeysCount++;
            this.m_pMediaManager.playSoundEffect((int) R.string.sound_level_collectKey);
        } else if (!this.m_Burning) {
            this.m_Burning = this.m_pPlayfield[field_x][field_y].pickupArrow(this.m_MovementDirection.x, this.m_MovementDirection.y);
            if (this.m_Burning) {
                this.m_pMediaManager.playSoundEffect((int) R.string.sound_level_burnStart);
                this.m_BurnSoundStreamId = this.m_pMediaManager.playSoundEffect((int) R.string.sound_level_burningLoop);
                this.m_ParticleEmitterId = this.m_ParticleManager.StartLoadedEffect("speedup_player", ((1.6452174f * this.m_DisplayPos.x) - 0.24004076f) + 0.8226087f, ((this.m_DisplayPos.y * 1.0f) - 0.46875f) + 0.5f, 0.95f, gameTimeSeconds, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f);
                this.m_StopParticleEffect = false;
            }
        }
    }

    private boolean changeState_ifPossible(int field_x, int field_y, float gameTimeSeconds) {
        boolean state_changed = false;
        if (!isFieldSet(field_x, field_y)) {
            return false;
        }
        if (this.m_pPlayfield[field_x][field_y].isExit_startAnimation()) {
            stopMovement();
            this.m_pGameLevel.setAsCompleted(this.m_PickupsCollected, this.m_MovesMade);
            state_changed = true;
            this.m_pMediaManager.playSoundEffect((int) R.string.sound_level_enterExit);
        }
        if (!this.m_pPlayfield[field_x][field_y].isTeleporter()) {
            return state_changed;
        }
        this.m_TeleportTarget = this.m_pGameLevel.findTeleporterPartner(field_x, field_y);
        float X = ((1.6452174f * this.m_DisplayPos.x) - 0.24004076f) + 0.8226087f;
        float Y = ((1.0f * this.m_DisplayPos.y) - 0.46875f) + 0.5f;
        this.m_ParticleEmitterId = this.m_ParticleManager.StartLoadedEffect("beamer_player", X, Y, 0.95f, gameTimeSeconds, this.m_TeleportTarget.color.red, this.m_TeleportTarget.color.green, this.m_TeleportTarget.color.blue, this.m_TeleportTarget.color.alpha, 1.2f, 0.0f);
        this.m_StopParticleEffect = false;
        this.m_pMediaManager.playSoundEffect((int) R.string.sound_level_teleport);
        return true;
    }

    public void draw(GuiMesh dynamic_playfield_mesh, float gameTimeSeconds) throws Exception {
        simulate(0.02f, gameTimeSeconds);
        float center_left = ((1.6452174f * this.m_DisplayPos.x) - 0.24004076f) + 0.8226087f;
        float center_top = ((1.0f * this.m_DisplayPos.y) - 0.46875f) + 0.5f;
        float squashed_x = (float) Math.sin(((double) this.m_MoveAnimation.x) * 3.141592653589793d);
        float squashed_y = (float) Math.sin(((double) this.m_MoveAnimation.y) * 3.141592653589793d);
        float x_scale = 1.0f - (Math.abs(squashed_x) * 0.4f);
        float y_scale = 1.0f - (Math.abs(squashed_y) * 0.4f);
        float center_left2 = center_left + (0.41130435f * squashed_x);
        float center_top2 = center_top + (0.41130435f * squashed_y);
        if (this.m_Moving) {
            drawCursor(dynamic_playfield_mesh, center_left2, center_top2, x_scale, y_scale, 0.5f);
            drawCursor(dynamic_playfield_mesh, center_left2 + (((float) this.m_MovementDirection.x) * GameViewExcit.PIXEL_WIDTH * 2.0f), center_top2 + (((float) this.m_MovementDirection.y) * GameViewExcit.PIXEL_HEIGHT * 2.0f), x_scale, y_scale, 0.5f);
            return;
        }
        drawCursor(dynamic_playfield_mesh, center_left2, center_top2, x_scale, y_scale, 1.0f);
    }

    private void drawCursor(GuiMesh dynamic_playfield_mesh, float center_left, float center_top, float x_scale, float y_scale, float alpha) throws Exception {
        if (this.m_Burning) {
            this.m_ShapeColor.red = 1.0f;
            this.m_ShapeColor.green = 0.75f;
            this.m_ShapeColor.blue = 0.0f;
            this.m_ShapeColor.alpha = this.m_Appear * alpha;
        } else if (this.m_TeleportTarget == null) {
            this.m_ShapeColor.red = 1.0f;
            this.m_ShapeColor.green = 1.0f;
            this.m_ShapeColor.blue = 1.0f;
            this.m_ShapeColor.alpha = this.m_Appear * alpha;
        } else {
            return;
        }
        float horizontal_left = center_left - (0.34898552f * x_scale);
        float horizontal_top = center_top - (0.11111111f * y_scale);
        float horizontal_right = horizontal_left + (0.5982609f * x_scale);
        float horizontal_bottom = horizontal_top + (0.22222222f * y_scale);
        float vertical_left = center_left - (0.14956522f * x_scale);
        float vertical_top = center_top - (0.33333334f * y_scale);
        float vertical_right = vertical_left + (0.1994203f * x_scale);
        float vertical_bottom = vertical_top + (0.6666667f * y_scale);
        dynamic_playfield_mesh.addQuad(horizontal_left, horizontal_top, 0.85f, horizontal_right, horizontal_bottom, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
        dynamic_playfield_mesh.addColorForLastQuad(this.m_ShapeColor.red, this.m_ShapeColor.green, this.m_ShapeColor.blue, this.m_ShapeColor.alpha);
        dynamic_playfield_mesh.addQuad(vertical_left, vertical_top, 0.85f, vertical_right, vertical_bottom, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
        dynamic_playfield_mesh.addColorForLastQuad(this.m_ShapeColor.red, this.m_ShapeColor.green, this.m_ShapeColor.blue, this.m_ShapeColor.alpha);
        float horizontal_left2 = horizontal_left - GameViewExcit.PIXEL_WIDTH;
        float horizontal_top2 = horizontal_top - GameViewExcit.PIXEL_HEIGHT;
        float horizontal_right2 = horizontal_right + GameViewExcit.PIXEL_WIDTH;
        float horizontal_bottom2 = horizontal_bottom + GameViewExcit.PIXEL_HEIGHT;
        float vertical_left2 = vertical_left - GameViewExcit.PIXEL_WIDTH;
        float vertical_top2 = vertical_top - GameViewExcit.PIXEL_HEIGHT;
        float vertical_right2 = vertical_right + GameViewExcit.PIXEL_WIDTH;
        float vertical_bottom2 = vertical_bottom + GameViewExcit.PIXEL_HEIGHT;
        dynamic_playfield_mesh.addQuad(horizontal_left2, horizontal_top2, 0.8f, horizontal_right2, horizontal_bottom2, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
        dynamic_playfield_mesh.addColorForLastQuad(0.0f, 0.0f, 0.0f, this.m_ShapeColor.alpha);
        dynamic_playfield_mesh.addQuad(vertical_left2, vertical_top2, 0.8f, vertical_right2, vertical_bottom2, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
        dynamic_playfield_mesh.addColorForLastQuad(0.0f, 0.0f, 0.0f, this.m_ShapeColor.alpha);
        float horizontal_left3 = horizontal_left2 + GameViewExcit.PIXEL_WIDTH;
        float horizontal_top3 = horizontal_top2 + GameViewExcit.PIXEL_HEIGHT;
        float horizontal_right3 = horizontal_right2 + GameViewExcit.PIXEL_WIDTH;
        float horizontal_bottom3 = horizontal_bottom2 + GameViewExcit.PIXEL_HEIGHT;
        float vertical_left3 = vertical_left2 + GameViewExcit.PIXEL_WIDTH;
        float vertical_top3 = vertical_top2 + GameViewExcit.PIXEL_HEIGHT;
        float vertical_right3 = vertical_right2 + GameViewExcit.PIXEL_WIDTH;
        float vertical_bottom3 = vertical_bottom2 + GameViewExcit.PIXEL_HEIGHT;
        dynamic_playfield_mesh.addQuad(horizontal_left3, horizontal_top3, 0.7f, horizontal_right3, horizontal_bottom3, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
        dynamic_playfield_mesh.addColorForLastQuad(0.0f, 0.0f, 0.0f, this.m_ShapeColor.alpha);
        dynamic_playfield_mesh.addQuad(vertical_left3, vertical_top3, 0.7f, vertical_right3, vertical_bottom3, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
        dynamic_playfield_mesh.addColorForLastQuad(0.0f, 0.0f, 0.0f, this.m_ShapeColor.alpha);
    }
}
