package com.ludocrazy.excit;

import android.util.FloatMath;
import com.ludocrazy.excit_demo.R;
import com.ludocrazy.tools.BasicMesh;
import com.ludocrazy.tools.ColorFloats;
import com.ludocrazy.tools.Coords;
import com.ludocrazy.tools.GuiMesh;
import com.ludocrazy.tools.LogOutput;
import com.ludocrazy.tools.MediaManager;
import com.ludocrazy.tools.ParticleManager;
import com.ludocrazy.tools.PhoneAbilities;
import com.ludocrazy.tools.Tools;
import com.ludocrazy.tools.UvCoords;
import com.ludocrazy.tools.Vector2D;
import com.ludocrazy.tools.Vector2Dint;
import java.lang.reflect.Array;

public class LevelObject {
    public static final float DEFLECTANIMATION_SPEEDUP = 5.0f;
    public static final float DOORCLOSEANIMATION_SPEEDUP = 10.0f;
    public static final float EXIT_HEIGHT = 0.08203125f;
    public static final float EXIT_START_U = 0.3544922f;
    public static final float EXIT_START_V = 0.81640625f;
    public static final float EXIT_WIDTH = 0.140625f;
    public static final int OBJECT_ARROW_DOWN = -5;
    public static final int OBJECT_ARROW_LEFT = -8;
    public static final int OBJECT_ARROW_RIGHT = -7;
    public static final int OBJECT_ARROW_UP = -6;
    public static final int OBJECT_BEAMER = 0;
    public static final int OBJECT_DEFLECTOR_LEFT_DOWN = -11;
    public static final int OBJECT_DEFLECTOR_LEFT_UP = -12;
    public static final int OBJECT_DEFLECTOR_RIGHT_DOWN = -9;
    public static final int OBJECT_DEFLECTOR_RIGHT_UP = -10;
    public static final int OBJECT_DOOR = -16;
    public static final int OBJECT_EXIT = -17;
    public static final int OBJECT_KEY = -15;
    public static final int OBJECT_ONEWAY_DOWN = -1;
    public static final int OBJECT_ONEWAY_LEFT = -4;
    public static final int OBJECT_ONEWAY_RIGHT = -3;
    public static final int OBJECT_ONEWAY_UP = -2;
    public static final int OBJECT_PICKUP = -18;
    public static final int OBJECT_START = -13;
    public static final int OBJECT_WALL = -14;
    public static final float ONEWAYANIMATION_SPEEDUP = 6.0f;
    public static final float WALLHITANIMATION_DISTANCE = 0.25f;
    public static final float WALLHITANIMATION_SPEEDUP = 10.0f;
    private LogOutput DebugLogOutput = null;
    private float m_AnimationLoop = 0.0f;
    private float m_Appear = 0.0f;
    private ColorFloats m_BeamerColor = null;
    private float m_BeamerParticleAnimation = Tools.random(0.0f, 1.0f);
    private int m_BeamerParticleEmitterId = -999;
    private float m_DeflectAnimation = 0.0f;
    private Vector2D m_DoorOpenAnimation = new Vector2D(1.0f, 1.0f);
    private Vector2D m_DoorOpenAnimationTarget = new Vector2D(1.0f, 1.0f);
    private int m_FieldPosX = 0;
    private int m_FieldPosY = 0;
    private boolean[][] m_Neighbours = ((boolean[][]) Array.newInstance(Boolean.TYPE, 3, 3));
    private int m_ObjectCategory = 0;
    private float m_OneWayAnimation = 0.0f;
    private ParticleManager m_ParticleManager = null;
    private LevelObjectPickup[] m_PickupObject = new LevelObjectPickup[4];
    private boolean m_PlayAnimation_Exit = false;
    private float m_PosX = 0.0f;
    private float m_PosY = 0.0f;
    private UvCoords m_UvCoordsEnd = new UvCoords(0.18554688f, 0.9277344f);
    private UvCoords m_UvCoordsStart = new UvCoords(0.087890625f, 0.8300781f);
    private boolean m_Visible = true;
    private Vector2D m_WallHitAnimation = new Vector2D(0.0f, 0.0f);
    private GameLevel m_pGameLevel = null;

    LevelObject(LogOutput debugLog, ParticleManager pParticleManager, float gameTimeSeconds, GameLevel pGameLevel, int start_x, int start_y, int object_category, BasicMesh pShapeMesh) {
        this.DebugLogOutput = debugLog;
        this.m_pGameLevel = pGameLevel;
        this.m_FieldPosX = start_x;
        this.m_FieldPosY = start_y;
        this.m_PosX = (float) start_x;
        this.m_PosY = (float) start_y;
        this.m_ParticleManager = pParticleManager;
        this.m_ObjectCategory = object_category;
        this.m_Appear = 0.0f;
        for (int y = 0; y < 3; y++) {
            for (int x = 0; x < 3; x++) {
                this.m_Neighbours[x][y] = false;
            }
        }
        if (this.m_ObjectCategory == -18) {
            for (int i = 0; i < 4; i++) {
                this.m_PickupObject[i] = new LevelObjectPickup(start_x, start_y);
                pGameLevel.addPickupObject(this.m_PickupObject[i]);
            }
        } else if (this.m_ObjectCategory >= 0) {
            switch (this.m_ObjectCategory) {
                case 1:
                    this.m_BeamerColor = new ColorFloats(0.8f, 1.0f, 0.75f, 1.0f);
                    break;
                case PhoneAbilities.ABILITY_GLUTILS_TEXSUBIMAGE2D_NONSQUARE /*2*/:
                    this.m_BeamerColor = new ColorFloats(0.7f, 0.9f, 0.8f, 1.0f);
                    break;
                case PhoneAbilities.ABILITY_VBO_GL11 /*3*/:
                    this.m_BeamerColor = new ColorFloats(0.5f, 0.9f, 0.95f, 1.0f);
                    break;
                case 4:
                    this.m_BeamerColor = new ColorFloats(0.5f, 0.7f, 0.9f, 1.0f);
                    break;
                case PhoneAbilities.ABILITY_FRAMEBUFFER_OES /*5*/:
                    this.m_BeamerColor = new ColorFloats(0.8f, 0.7f, 0.9f, 1.0f);
                    break;
            }
            float X = (1.6452174f * this.m_PosX) - 0.24004076f;
            float Y = (1.0f * this.m_PosY) - 0.46875f;
            this.m_BeamerParticleEmitterId = this.m_ParticleManager.StartLoadedEffect("beamer_outline", X, Y, 0.95f, gameTimeSeconds, this.m_BeamerColor.red, this.m_BeamerColor.green, this.m_BeamerColor.blue, this.m_BeamerColor.alpha, 0.9f, 0.0f);
            this.DebugLogOutput.log(2, "Starting PFX at x:" + X + ", y:" + Y + ", emitter id:" + this.m_BeamerParticleEmitterId);
        }
    }

    public void setNeighbourFields(LevelObject potential_top_left_neighbour, LevelObject potential_top_neighbour, LevelObject potential_left_neighbour, LevelObject potential_top_right_neighbour) {
        if (potential_top_left_neighbour != null && potential_top_left_neighbour.isWall()) {
            potential_top_left_neighbour.getNeighbours()[2][2] = true;
            this.m_Neighbours[0][0] = true;
        }
        if (potential_top_neighbour != null && potential_top_neighbour.isWall()) {
            potential_top_neighbour.getNeighbours()[1][2] = true;
            this.m_Neighbours[1][0] = true;
        }
        if (potential_left_neighbour != null && potential_left_neighbour.isWall()) {
            potential_left_neighbour.getNeighbours()[2][1] = true;
            this.m_Neighbours[0][1] = true;
        }
        if (potential_top_right_neighbour != null && potential_top_right_neighbour.isWall()) {
            potential_top_right_neighbour.getNeighbours()[0][2] = true;
            this.m_Neighbours[2][0] = true;
        }
    }

    public boolean[][] getNeighbours() {
        return this.m_Neighbours;
    }

    public int getCategory() {
        return this.m_ObjectCategory;
    }

    public boolean isWall() {
        return this.m_ObjectCategory == -14;
    }

    public boolean isTeleporter() {
        return this.m_ObjectCategory >= 0;
    }

    public ColorFloats isTeleporterPartner_getColor(int teleporter_id) {
        if (this.m_ObjectCategory == teleporter_id) {
            return this.m_BeamerColor;
        }
        return null;
    }

    public boolean moveToField_ifPossible(int movementDirection_x, int movementDirection_y, boolean burning, float gameTimeSeconds, int burnSoundStreamId, MediaManager pMediaManager) {
        boolean move_is_possible = true;
        if (this.m_Visible) {
            if (this.m_ObjectCategory != -14) {
                boolean play_oneway_sound = false;
                if (this.m_ObjectCategory == -4) {
                    move_is_possible = movementDirection_x < 0;
                    play_oneway_sound = move_is_possible;
                } else if (this.m_ObjectCategory == -3) {
                    move_is_possible = movementDirection_x > 0;
                    play_oneway_sound = move_is_possible;
                } else if (this.m_ObjectCategory == -2) {
                    move_is_possible = movementDirection_y < 0;
                    play_oneway_sound = move_is_possible;
                } else if (this.m_ObjectCategory == -1) {
                    move_is_possible = movementDirection_y > 0;
                    play_oneway_sound = move_is_possible;
                }
                if (play_oneway_sound) {
                    this.m_OneWayAnimation = 1.0f;
                    pMediaManager.playSoundEffect(R.string.sound_level_oneway);
                }
            } else if (!burning) {
                move_is_possible = false;
            } else {
                move_is_possible = true;
                this.m_pGameLevel.resetPlayerBurning();
                this.m_Visible = false;
                this.m_ParticleManager.StartLoadedEffect("speedup_wallhit", (1.6452174f * (this.m_PosX + 0.5f)) - 0.24004076f, (1.0f * (this.m_PosY + 0.5f)) - 0.46875f, 0.95f, gameTimeSeconds, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f);
                pMediaManager.stopSoundEffeect(burnSoundStreamId);
                pMediaManager.playSoundEffect(R.string.sound_level_burnWall);
            }
        }
        if (!move_is_possible) {
            this.m_WallHitAnimation.x = (float) movementDirection_x;
            this.m_WallHitAnimation.y = (float) movementDirection_y;
        }
        return move_is_possible;
    }

    public boolean isDoor_isVisible() {
        return this.m_ObjectCategory == -16 && this.m_Visible;
    }

    public boolean mustBeDoor_openIfPossible(int current_KeysCount, int movementDirection_x, int movementDirection_y) {
        if (current_KeysCount > 0) {
            this.m_DoorOpenAnimationTarget.x = (float) Math.abs(movementDirection_x);
            this.m_DoorOpenAnimationTarget.y = (float) Math.abs(movementDirection_y);
            return true;
        }
        this.m_WallHitAnimation.x = (float) movementDirection_x;
        this.m_WallHitAnimation.y = (float) movementDirection_y;
        return false;
    }

    public boolean isDeflector() {
        return this.m_Visible && (this.m_ObjectCategory == -12 || this.m_ObjectCategory == -11 || this.m_ObjectCategory == -10 || this.m_ObjectCategory == -9);
    }

    public boolean mustBeDeflector_moveIfPossible_changeDirection(Vector2Dint movementDirection) {
        boolean move_is_possible = true;
        if (this.m_ObjectCategory == -12) {
            if (movementDirection.x > 0) {
                movementDirection.set(0, -1);
            } else if (movementDirection.y > 0) {
                movementDirection.set(-1, 0);
            } else {
                move_is_possible = false;
            }
        } else if (this.m_ObjectCategory == -11) {
            if (movementDirection.x > 0) {
                movementDirection.set(0, 1);
            } else if (movementDirection.y < 0) {
                movementDirection.set(-1, 0);
            } else {
                move_is_possible = false;
            }
        } else if (this.m_ObjectCategory == -10) {
            if (movementDirection.x < 0) {
                movementDirection.set(0, -1);
            } else if (movementDirection.y > 0) {
                movementDirection.set(1, 0);
            } else {
                move_is_possible = false;
            }
        } else if (this.m_ObjectCategory == -9) {
            if (movementDirection.x < 0) {
                movementDirection.set(0, 1);
            } else if (movementDirection.y < 0) {
                movementDirection.set(1, 0);
            } else {
                move_is_possible = false;
            }
        }
        if (!move_is_possible) {
            this.m_WallHitAnimation.copyFrom(movementDirection);
        } else {
            this.m_DeflectAnimation = 1.0f;
        }
        return move_is_possible;
    }

    public boolean pickupArrow(int movementDirection_x, int movementDirection_y) {
        boolean burning = false;
        if (this.m_Visible) {
            burning = (movementDirection_x < 0 && this.m_ObjectCategory == -8) || (movementDirection_x > 0 && this.m_ObjectCategory == -7) || ((movementDirection_y < 0 && this.m_ObjectCategory == -6) || (movementDirection_y > 0 && this.m_ObjectCategory == -5));
            if (burning) {
                this.m_Visible = false;
            }
        }
        return burning;
    }

    public boolean pickupKey() {
        boolean is_key = false;
        if (this.m_Visible) {
            is_key = this.m_ObjectCategory == -15;
            if (is_key) {
                this.m_Visible = false;
            }
        }
        return is_key;
    }

    public boolean pickupPickup(int movementDirection_x, int movementDirection_y) {
        boolean is_pickup = false;
        if (this.m_Visible) {
            is_pickup = this.m_ObjectCategory == -18;
            if (is_pickup) {
                this.m_Visible = false;
                for (int i = 0; i < 4; i++) {
                    this.m_PickupObject[i].setAsPickedUp(movementDirection_x, movementDirection_y, i);
                }
            }
        }
        return is_pickup;
    }

    public boolean isExit_startAnimation() {
        boolean isExit = this.m_ObjectCategory == -17;
        if (isExit) {
            this.m_PlayAnimation_Exit = true;
        }
        return isExit;
    }

    public void resetToStart() {
        this.m_Visible = true;
        if (this.m_ObjectCategory == -16) {
            this.m_DoorOpenAnimationTarget.x = 1.0f;
            this.m_DoorOpenAnimationTarget.y = 1.0f;
            this.m_DoorOpenAnimation.x = 1.0f;
            this.m_DoorOpenAnimation.y = 1.0f;
        }
    }

    private void simulate(float duration) {
        this.m_WallHitAnimation.shrinkTowardsZero(10.0f * duration);
        if (this.m_DoorOpenAnimation.x > this.m_DoorOpenAnimationTarget.x) {
            this.m_DoorOpenAnimation.x -= 10.0f * duration;
        }
        if (this.m_DoorOpenAnimation.y > this.m_DoorOpenAnimationTarget.y) {
            this.m_DoorOpenAnimation.y -= 10.0f * duration;
        }
        if (this.m_DoorOpenAnimation.x <= 0.0f || this.m_DoorOpenAnimation.y <= 0.0f) {
            this.m_Visible = false;
        }
        if (this.m_DeflectAnimation > 0.0f) {
            this.m_DeflectAnimation -= duration * 5.0f;
            if (this.m_DeflectAnimation < 0.0f) {
                this.m_DeflectAnimation = 0.0f;
            }
        }
        if (this.m_OneWayAnimation > 0.0f) {
            this.m_OneWayAnimation -= 6.0f * duration;
            if (this.m_OneWayAnimation < 0.0f) {
                this.m_OneWayAnimation = 0.0f;
            }
        }
        if (this.m_BeamerParticleEmitterId >= 0) {
            this.m_BeamerParticleAnimation += duration * 0.3f;
            if (this.m_BeamerParticleAnimation > 1.0f) {
                this.m_BeamerParticleAnimation -= (float) ((int) this.m_BeamerParticleAnimation);
            }
            float leftX = (1.6452174f * this.m_PosX) - 0.24004076f;
            float topY = (this.m_PosY * 1.0f) - 0.46875f;
            float left = leftX - GameViewExcit.PIXEL_WIDTH;
            float top = topY - GameViewExcit.PIXEL_HEIGHT;
            float width = 1.6452174f + (GameViewExcit.PIXEL_WIDTH * 2.0f);
            float height = 1.0f + (GameViewExcit.PIXEL_HEIGHT * 2.0f);
            if (this.m_BeamerParticleAnimation < 0.3f) {
                this.m_ParticleManager.SetEffectPosition(this.m_BeamerParticleEmitterId, (this.m_BeamerParticleAnimation * width * 3.3333333f) + left, top, 0.95f);
            } else if (this.m_BeamerParticleAnimation < 0.5f) {
                this.m_ParticleManager.SetEffectPosition(this.m_BeamerParticleEmitterId, left + width, ((this.m_BeamerParticleAnimation - 0.3f) * height * 5.0f) + top, 0.95f);
            } else if (this.m_BeamerParticleAnimation < 0.8f) {
                this.m_ParticleManager.SetEffectPosition(this.m_BeamerParticleEmitterId, (left + width) - (((this.m_BeamerParticleAnimation - 0.5f) * width) * 3.3333333f), top + height, 0.95f);
            } else if (this.m_BeamerParticleAnimation < 1.0f) {
                this.m_ParticleManager.SetEffectPosition(this.m_BeamerParticleEmitterId, left, (top + height) - (((this.m_BeamerParticleAnimation - 0.8f) * height) * 5.0f), 0.95f);
            }
        }
    }

    public void draw(GuiMesh dynamic_playfield_mesh) throws Exception {
        simulate(0.02f);
        if (this.m_Visible) {
            this.m_Appear += 0.075f;
            if (this.m_Appear > 1.0f) {
                this.m_Appear = 1.0f;
            }
            this.m_AnimationLoop += 0.075f;
            if (this.m_AnimationLoop > 1.0f) {
                this.m_AnimationLoop -= 1.0f;
            }
            this.m_PosX = (this.m_PosX * 0.8f) + (((float) this.m_FieldPosX) * 0.2f);
            this.m_PosY = (this.m_PosY * 0.8f) + (((float) this.m_FieldPosY) * 0.2f);
            drawExcelStyle(dynamic_playfield_mesh, ((1.6452174f * this.m_PosX) - 0.24004076f) + (((float) Math.sin(((double) this.m_WallHitAnimation.x) * 3.141592653589793d)) * 0.41130435f), ((this.m_PosY * 1.0f) - 0.46875f) + (((float) Math.sin(((double) this.m_WallHitAnimation.y) * 3.141592653589793d)) * 0.41130435f), 0.5f, 0.55f, 0.6f);
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    private void drawExcelStyle(GuiMesh dynamic_playfield_mesh, float leftX, float topY, float Z, float Z_close, float Z_closest) throws Exception {
        float left = leftX - GameViewExcit.PIXEL_WIDTH;
        float top = topY - GameViewExcit.PIXEL_HEIGHT;
        float right = 1.6452174f + leftX + (GameViewExcit.PIXEL_WIDTH * 2.0f);
        float bottom = 1.0f + topY + (GameViewExcit.PIXEL_HEIGHT * 2.0f);
        float border_width = GameViewExcit.PIXEL_WIDTH * 3.0f;
        float border_height = GameViewExcit.PIXEL_HEIGHT * 3.0f;
        float inner_width = GameViewExcit.PIXEL_WIDTH * 4.0f;
        float inner_height = GameViewExcit.PIXEL_HEIGHT * 4.0f;
        float deflect_factor = 0.3f + (0.4f * ((float) Math.sin(((double) this.m_DeflectAnimation) * 3.141592653589793d * 3.0d)));
        switch (this.m_ObjectCategory) {
            case OBJECT_PICKUP /*-18*/:
                return;
            case OBJECT_EXIT /*-17*/:
                GuiMesh guiMesh = dynamic_playfield_mesh;
                float f = leftX;
                float f2 = topY;
                float f3 = Z;
                guiMesh.addQuad(f, f2, f3, leftX + 1.6452174f, topY + 1.0f, 0.3544922f, 0.81640625f, 0.3544922f + 0.140625f, 0.81640625f + 0.08203125f);
                if (!this.m_PlayAnimation_Exit) {
                    dynamic_playfield_mesh.addColorForLastQuad(1.0f, 1.0f, 1.0f, this.m_Appear);
                } else {
                    dynamic_playfield_mesh.addColorForLastQuad(0.5f, 0.5f, 0.5f, this.m_AnimationLoop);
                }
                float u_end = 0.58691406f + 0.34179688f;
                float v_end = 0.7626953f + 0.009765625f;
                float left2 = leftX - (GameViewExcit.PIXEL_WIDTH * 2.0f);
                float top2 = topY - (GameViewExcit.PIXEL_HEIGHT * 2.0f);
                float right2 = 1.6452174f + leftX + (GameViewExcit.PIXEL_WIDTH * 2.0f);
                float bottom2 = 1.0f + topY + (GameViewExcit.PIXEL_HEIGHT * 2.0f);
                dynamic_playfield_mesh.addQuad(left2, top2, Z_close, right2, top2 + GameViewExcit.PIXEL_HEIGHT, 0.58691406f, 0.7626953f, u_end, v_end);
                dynamic_playfield_mesh.addColorForLastQuad(1.0f, 1.0f, 1.0f, 1.0f);
                dynamic_playfield_mesh.addQuad(left2, bottom2, Z_close, right2, bottom2 + GameViewExcit.PIXEL_HEIGHT, 0.58691406f, 0.7626953f, u_end, v_end);
                dynamic_playfield_mesh.addColorForLastQuad(1.0f, 1.0f, 1.0f, 1.0f);
                float u_end2 = 0.58691406f + ((350.0f * 0.5813953f) / 1024.0f);
                float v_end2 = 0.7626953f + ((10.0f * 0.5813953f) / 1024.0f);
                dynamic_playfield_mesh.addQuad_uvUsedRotatedToRight(left2, top2, Z_close, left2 + GameViewExcit.PIXEL_WIDTH, bottom2, 0.58691406f, 0.7626953f, u_end2, v_end2);
                dynamic_playfield_mesh.addColorForLastQuad(1.0f, 1.0f, 1.0f, 1.0f);
                dynamic_playfield_mesh.addQuad_uvUsedRotatedToRight(right2, top2, Z_close, right2 + GameViewExcit.PIXEL_WIDTH, bottom2, 0.58691406f, 0.7626953f, u_end2, v_end2);
                dynamic_playfield_mesh.addColorForLastQuad(1.0f, 1.0f, 1.0f, 1.0f);
                return;
            case OBJECT_DOOR /*-16*/:
                drawLockBitmapAndDoor(dynamic_playfield_mesh, leftX, topY, 1.6452174f, 1.0f, Z_close, Z, this.m_pGameLevel.hasPlayerKeys(), this.m_DoorOpenAnimation);
                return;
            case OBJECT_KEY /*-15*/:
                drawKeyBitmap(dynamic_playfield_mesh, leftX, topY, 1.6452174f, 1.0f, Z_close);
                return;
            case OBJECT_WALL /*-14*/:
                if (!this.m_Neighbours[1][0]) {
                    GuiMesh guiMesh2 = dynamic_playfield_mesh;
                    float f4 = left;
                    float f5 = top;
                    float f6 = Z_close;
                    float f7 = right;
                    guiMesh2.addQuad(f4, f5, f6, f7, top + border_height, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
                    dynamic_playfield_mesh.addColorForLastQuad(0.0f, 0.0f, 0.0f, 1.0f);
                }
                if (!this.m_Neighbours[1][2]) {
                    GuiMesh guiMesh3 = dynamic_playfield_mesh;
                    float f8 = left;
                    float f9 = bottom;
                    float f10 = Z_close;
                    float f11 = right;
                    guiMesh3.addQuad(f8, f9, f10, f11, bottom - border_height, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
                    dynamic_playfield_mesh.addColorForLastQuad(0.0f, 0.0f, 0.0f, 1.0f);
                }
                if (!this.m_Neighbours[0][1]) {
                    GuiMesh guiMesh4 = dynamic_playfield_mesh;
                    float f12 = left;
                    float f13 = top;
                    float f14 = Z_close;
                    guiMesh4.addQuad(f12, f13, f14, left + border_width, bottom, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
                    dynamic_playfield_mesh.addColorForLastQuad(0.0f, 0.0f, 0.0f, 1.0f);
                }
                if (!this.m_Neighbours[2][1]) {
                    GuiMesh guiMesh5 = dynamic_playfield_mesh;
                    float f15 = right;
                    float f16 = top;
                    float f17 = Z_close;
                    guiMesh5.addQuad(f15, f16, f17, right - border_width, bottom, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
                    dynamic_playfield_mesh.addColorForLastQuad(0.0f, 0.0f, 0.0f, 1.0f);
                }
                drawInnerWall(dynamic_playfield_mesh, left, right, top, bottom, inner_width, inner_height, Z);
                return;
            case OBJECT_START /*-13*/:
                GuiMesh guiMesh6 = dynamic_playfield_mesh;
                float f18 = leftX;
                float f19 = topY;
                float f20 = Z;
                guiMesh6.addQuad(f18, f19, f20, leftX + 1.6452174f, topY + 1.0f, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
                dynamic_playfield_mesh.addColorForLastQuad(0.7f, 0.7f, 0.7f, this.m_Appear);
                return;
            case OBJECT_DEFLECTOR_LEFT_UP /*-12*/:
                Coords coords = new Coords((1.6452174f + leftX) - border_width, (1.0f + topY) - border_height, Z);
                Coords coords2 = new Coords(0.5429218f + leftX + border_width, (1.0f + topY) - border_height, Z);
                Coords coords3 = new Coords((1.6452174f + leftX) - border_width, topY + border_height, Z);
                Coords coords4 = new Coords(coords2);
                coords4.blend(coords3, 0.33f);
                coords4.blend(coords, deflect_factor);
                Coords coords5 = new Coords(coords2);
                coords5.blend(coords3, 0.66f);
                coords5.blend(coords, deflect_factor);
                dynamic_playfield_mesh.addTriangleFace_textured(coords2, this.m_UvCoordsStart, coords4, this.m_UvCoordsStart, coords, this.m_UvCoordsStart, (byte) 0, (byte) -1);
                dynamic_playfield_mesh.addTriangleFace_textured(coords4, this.m_UvCoordsStart, coords5, this.m_UvCoordsStart, coords, this.m_UvCoordsStart, (byte) 0, (byte) -1);
                dynamic_playfield_mesh.addTriangleFace_textured(coords5, this.m_UvCoordsStart, coords3, this.m_UvCoordsStart, coords, this.m_UvCoordsStart, (byte) 0, (byte) -1);
                dynamic_playfield_mesh.addQuad(left, bottom, Z_closest, right, bottom - border_height, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
                dynamic_playfield_mesh.addColorForLastQuad(0.3f, 0.3f, 0.3f, 1.0f);
                dynamic_playfield_mesh.addQuad(right, top, Z_closest, right - border_width, bottom, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
                dynamic_playfield_mesh.addColorForLastQuad(0.3f, 0.3f, 0.3f, 1.0f);
                return;
            case OBJECT_DEFLECTOR_LEFT_DOWN /*-11*/:
                Coords coords6 = new Coords((1.6452174f + leftX) - border_width, topY + border_height, Z);
                Coords coords7 = new Coords(0.5429218f + leftX + border_width, topY + border_height, Z);
                Coords coords8 = new Coords((1.6452174f + leftX) - border_width, (1.0f + topY) - border_height, Z);
                Coords coords9 = new Coords(coords7);
                coords9.blend(coords8, 0.33f);
                coords9.blend(coords6, deflect_factor);
                Coords coords10 = new Coords(coords7);
                coords10.blend(coords8, 0.66f);
                coords10.blend(coords6, deflect_factor);
                dynamic_playfield_mesh.addTriangleFace_textured(coords7, this.m_UvCoordsStart, coords9, this.m_UvCoordsStart, coords6, this.m_UvCoordsStart, (byte) 0, (byte) -1);
                dynamic_playfield_mesh.addTriangleFace_textured(coords9, this.m_UvCoordsStart, coords10, this.m_UvCoordsStart, coords6, this.m_UvCoordsStart, (byte) 0, (byte) -1);
                dynamic_playfield_mesh.addTriangleFace_textured(coords10, this.m_UvCoordsStart, coords8, this.m_UvCoordsStart, coords6, this.m_UvCoordsStart, (byte) 0, (byte) -1);
                dynamic_playfield_mesh.addQuad(left, top, Z_closest, right, top + border_height, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
                dynamic_playfield_mesh.addColorForLastQuad(0.3f, 0.3f, 0.3f, 1.0f);
                dynamic_playfield_mesh.addQuad(right, top, Z_closest, right - border_width, bottom, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
                dynamic_playfield_mesh.addColorForLastQuad(0.3f, 0.3f, 0.3f, 1.0f);
                return;
            case OBJECT_DEFLECTOR_RIGHT_UP /*-10*/:
                Coords coords11 = new Coords(leftX + border_width, (1.0f + topY) - border_height, Z);
                Coords coords12 = new Coords(leftX + border_width, topY + border_height, Z);
                Coords coords13 = new Coords((1.0858436f + leftX) - border_width, (1.0f + topY) - border_height, Z);
                Coords coords14 = new Coords(coords12);
                coords14.blend(coords13, 0.33f);
                coords14.blend(coords11, deflect_factor);
                Coords coords15 = new Coords(coords12);
                coords15.blend(coords13, 0.66f);
                coords15.blend(coords11, deflect_factor);
                dynamic_playfield_mesh.addTriangleFace_textured(coords12, this.m_UvCoordsStart, coords14, this.m_UvCoordsStart, coords11, this.m_UvCoordsStart, (byte) 0, (byte) -1);
                dynamic_playfield_mesh.addTriangleFace_textured(coords14, this.m_UvCoordsStart, coords15, this.m_UvCoordsStart, coords11, this.m_UvCoordsStart, (byte) 0, (byte) -1);
                dynamic_playfield_mesh.addTriangleFace_textured(coords15, this.m_UvCoordsStart, coords13, this.m_UvCoordsStart, coords11, this.m_UvCoordsStart, (byte) 0, (byte) -1);
                dynamic_playfield_mesh.addQuad(left, bottom, Z_closest, right, bottom - border_height, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
                dynamic_playfield_mesh.addColorForLastQuad(0.3f, 0.3f, 0.3f, 1.0f);
                dynamic_playfield_mesh.addQuad(left, top, Z_closest, left + border_width, bottom, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
                dynamic_playfield_mesh.addColorForLastQuad(0.3f, 0.3f, 0.3f, 1.0f);
                return;
            case OBJECT_DEFLECTOR_RIGHT_DOWN /*-9*/:
                Coords coords16 = new Coords(leftX + border_width, topY + border_height, Z);
                Coords coords17 = new Coords((1.0858436f + leftX) - border_width, topY + border_height, Z);
                Coords coords18 = new Coords(leftX + border_width, (1.0f + topY) - border_height, Z);
                Coords coords19 = new Coords(coords17);
                coords19.blend(coords18, 0.33f);
                coords19.blend(coords16, deflect_factor);
                Coords coords20 = new Coords(coords17);
                coords20.blend(coords18, 0.66f);
                coords20.blend(coords16, deflect_factor);
                dynamic_playfield_mesh.addTriangleFace_textured(coords17, this.m_UvCoordsStart, coords19, this.m_UvCoordsStart, coords16, this.m_UvCoordsStart, (byte) 0, (byte) -1);
                dynamic_playfield_mesh.addTriangleFace_textured(coords19, this.m_UvCoordsStart, coords20, this.m_UvCoordsStart, coords16, this.m_UvCoordsStart, (byte) 0, (byte) -1);
                dynamic_playfield_mesh.addTriangleFace_textured(coords20, this.m_UvCoordsStart, coords18, this.m_UvCoordsStart, coords16, this.m_UvCoordsStart, (byte) 0, (byte) -1);
                dynamic_playfield_mesh.addQuad(left, top, Z_closest, right, top + border_height, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
                dynamic_playfield_mesh.addColorForLastQuad(0.3f, 0.3f, 0.3f, 1.0f);
                dynamic_playfield_mesh.addQuad(left, top, Z_closest, left + border_width, bottom, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
                dynamic_playfield_mesh.addColorForLastQuad(0.3f, 0.3f, 0.3f, 1.0f);
                return;
            case OBJECT_ARROW_LEFT /*-8*/:
                drawArrowsBitmap(dynamic_playfield_mesh, true, false, leftX, topY, 1.6452174f, 1.0f, Z);
                return;
            case OBJECT_ARROW_RIGHT /*-7*/:
                drawArrowsBitmap(dynamic_playfield_mesh, false, false, leftX, topY, 1.6452174f, 1.0f, Z);
                return;
            case OBJECT_ARROW_UP /*-6*/:
                drawArrowsBitmap(dynamic_playfield_mesh, true, true, leftX, topY, 1.6452174f, 1.0f, Z);
                return;
            case OBJECT_ARROW_DOWN /*-5*/:
                drawArrowsBitmap(dynamic_playfield_mesh, false, true, leftX, topY, 1.6452174f, 1.0f, Z);
                return;
            case OBJECT_ONEWAY_LEFT /*-4*/:
                drawOnewayArrow(false, false, dynamic_playfield_mesh, left, top, bottom, right, Z_closest, border_width, border_height);
                return;
            case OBJECT_ONEWAY_RIGHT /*-3*/:
                drawOnewayArrow(true, false, dynamic_playfield_mesh, left, top, bottom, right, Z_closest, border_width, border_height);
                return;
            case OBJECT_ONEWAY_UP /*-2*/:
                drawOnewayArrow(false, true, dynamic_playfield_mesh, left, top, bottom, right, Z_closest, border_width, border_height);
                return;
            case -1:
                drawOnewayArrow(true, true, dynamic_playfield_mesh, left, top, bottom, right, Z_closest, border_width, border_height);
                return;
            default:
                if (this.m_ObjectCategory >= 0) {
                    dynamic_playfield_mesh.addQuad(leftX, topY, Z, leftX + 1.6452174f, topY + 1.0f, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
                    switch (this.m_ObjectCategory) {
                        case 1:
                            dynamic_playfield_mesh.addColorForLastQuad(0.8f, 1.0f, 0.75f, this.m_Appear);
                            return;
                        case PhoneAbilities.ABILITY_GLUTILS_TEXSUBIMAGE2D_NONSQUARE /*2*/:
                            dynamic_playfield_mesh.addColorForLastQuad(0.7f, 0.9f, 0.8f, this.m_Appear);
                            return;
                        case PhoneAbilities.ABILITY_VBO_GL11 /*3*/:
                            dynamic_playfield_mesh.addColorForLastQuad(0.5f, 0.9f, 0.95f, this.m_Appear);
                            return;
                        case 4:
                            dynamic_playfield_mesh.addColorForLastQuad(0.5f, 0.7f, 0.9f, this.m_Appear);
                            return;
                        case PhoneAbilities.ABILITY_FRAMEBUFFER_OES /*5*/:
                            dynamic_playfield_mesh.addColorForLastQuad(0.8f, 0.7f, 0.9f, this.m_Appear);
                            break;
                    }
                }
                return;
        }
    }

    private void drawKeyBitmap(GuiMesh mesh, float x_start, float y_start, float x_field_width, float y_field_height, float Z) throws Exception {
        float x_start2 = x_start + (0.27272728f * x_field_width);
        float y_start2 = y_start + (0.22222222f * y_field_height);
        GuiMesh guiMesh = mesh;
        float f = x_start2;
        float f2 = y_start2;
        float f3 = Z;
        guiMesh.addQuad(f, f2, f3, x_start2 + (0.5151515f * x_field_width), y_start2 + (0.6666667f * y_field_height), 0.89941406f, 0.7949219f, 0.89941406f + 0.016601562f, 0.7949219f + 0.01171875f);
        mesh.addColorForLastQuad(1.0f, 1.0f, 1.0f, 1.0f);
    }

    private void drawLockBitmapAndDoor(GuiMesh mesh, float x_start, float y_start, float x_field_width, float y_field_height, float Z_close, float Z, boolean keys_available, Vector2D open_scale) throws Exception {
        float left = x_start - GameViewExcit.PIXEL_WIDTH;
        float top = y_start - GameViewExcit.PIXEL_HEIGHT;
        float right = x_start + ((1.6452174f + (GameViewExcit.PIXEL_WIDTH * 2.0f)) * open_scale.x);
        float bottom = y_start + ((1.0f + (GameViewExcit.PIXEL_HEIGHT * 2.0f)) * open_scale.y);
        float border_width = GameViewExcit.PIXEL_WIDTH * 3.0f;
        float border_height = GameViewExcit.PIXEL_HEIGHT * 3.0f;
        GuiMesh guiMesh = mesh;
        float f = Z_close;
        guiMesh.addQuad(left, top, f, right, top + border_height, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
        mesh.addColorForLastQuad(0.0f, 0.0f, 0.0f, 1.0f);
        mesh.addQuad(left, bottom, Z_close, right, bottom - border_height, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
        mesh.addColorForLastQuad(0.0f, 0.0f, 0.0f, 1.0f);
        mesh.addQuad(left, top, Z_close, left + border_width, bottom, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
        mesh.addColorForLastQuad(0.0f, 0.0f, 0.0f, 1.0f);
        mesh.addQuad(right, top, Z_close, right - border_width, bottom, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
        mesh.addColorForLastQuad(0.0f, 0.0f, 0.0f, 1.0f);
        float inner_width = GameViewExcit.PIXEL_WIDTH * 4.0f;
        float inner_height = GameViewExcit.PIXEL_HEIGHT * 4.0f;
        mesh.addQuad(left + inner_width, top + inner_height, Z, right - inner_width, bottom - inner_height, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
        mesh.addColorForLastQuad(0.0f, 0.0f, 0.0f, 1.0f);
        float x_start2 = x_start + (0.27272728f * x_field_width * open_scale.x);
        float y_start2 = y_start + (0.11111111f * y_field_height * open_scale.y);
        GuiMesh guiMesh2 = mesh;
        float f2 = x_start2;
        float f3 = y_start2;
        float f4 = Z;
        guiMesh2.addQuad(f2, f3, f4, x_start2 + (0.4848485f * x_field_width * open_scale.x), y_start2 + (0.8333333f * y_field_height * open_scale.y), 0.88183594f, 0.79296875f, 0.88183594f + 0.015625f, 0.79296875f + 0.0146484375f);
        if (!keys_available) {
            mesh.addColorForLastQuad(1.0f, 1.0f, 1.0f, 1.0f);
        } else {
            mesh.addColorForLastQuad(1.0f, 0.925f, 0.1f, 1.0f);
        }
    }

    private void drawArrowsBitmap(GuiMesh mesh, boolean flip_direction, boolean rotate, float x_start, float y_start, float x_field_width, float y_field_height, float Z) throws Exception {
        float uLeft = 0.92285156f;
        float uRight = 0.92285156f + 0.020507812f;
        float vBottom = 0.7949219f + 0.0107421875f;
        float x_offset = 7.0f;
        if (flip_direction) {
            uLeft = uRight;
            uRight = 0.92285156f;
            x_offset = 6.0f;
        }
        if (!rotate) {
            float x_start2 = x_start + ((x_offset / 33.0f) * x_field_width);
            float y_start2 = y_start + (0.22222222f * y_field_height);
            mesh.addQuad(x_start2, y_start2, Z, x_start2 + (0.6363636f * x_field_width), y_start2 + (0.6111111f * y_field_height), uLeft, 0.7949219f, uRight, vBottom);
        } else {
            float x_start3 = x_start + (0.36363637f * x_field_width);
            float y_start3 = y_start + (-0.055555556f * y_field_height);
            mesh.addQuad_uvUsedRotatedToRight(x_start3, y_start3, Z, x_start3 + (0.33333334f * x_field_width), y_start3 + (1.1666666f * y_field_height), uLeft, 0.7949219f, uRight, vBottom);
        }
        mesh.addColorForLastQuad(1.0f, 1.0f, 1.0f, 1.0f);
    }

    private void drawOnewayArrow(boolean flip_x, boolean flip_xy, GuiMesh dynamic_playfield_mesh, float left, float top, float bottom, float right, float Z_closest, float border_width, float border_height) throws Exception {
        float OneWayAnimation_closed = 1.0f - ((float) Math.sin(((double) this.m_OneWayAnimation) * 3.141592653589793d));
        if (!flip_xy) {
            float FULL_WIDTH = right - left;
            float FULL_HEIGHT = bottom - top;
            float side_front = 0.4848485f * FULL_WIDTH;
            float side_back = 0.7878788f * FULL_WIDTH;
            float arrow_forward_front_x = border_width;
            float arrow_forward_front_y_up = 0.475f * FULL_HEIGHT * OneWayAnimation_closed;
            float arrow_forward_front_y_down = FULL_HEIGHT - ((0.475f * FULL_HEIGHT) * OneWayAnimation_closed);
            float arrow_forward_back_x = border_width * 1.75f;
            float arrow_forward_back_y_up = border_height + (((0.475f * FULL_HEIGHT) - border_height) * OneWayAnimation_closed);
            float arrow_forward_back_y_down = (FULL_HEIGHT - border_height) - (((0.475f * FULL_HEIGHT) - border_height) * OneWayAnimation_closed);
            float arrow_backward_front_x = -1.5f * border_width;
            float arrow_backward_back_x = -0.5f * border_width;
            float arrow_backward_inner_y_up = border_height;
            float arrow_backward_inner_y_down = FULL_HEIGHT - border_height;
            float arrow_backward_outer_y_down = FULL_HEIGHT;
            if (flip_x) {
                side_front = FULL_WIDTH - side_front;
                side_back = FULL_WIDTH - side_back;
                arrow_forward_front_x = FULL_WIDTH - arrow_forward_front_x;
                arrow_forward_back_x = FULL_WIDTH - arrow_forward_back_x;
                arrow_backward_front_x *= -1.0f;
                arrow_backward_back_x *= -1.0f;
            }
            GuiMesh guiMesh = dynamic_playfield_mesh;
            float f = top;
            float f2 = Z_closest;
            guiMesh.addQuad(left + side_front, f, f2, left + side_back, top + border_height, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
            dynamic_playfield_mesh.addColorForLastQuad(0.3f, 0.3f, 0.3f, 1.0f);
            GuiMesh guiMesh2 = dynamic_playfield_mesh;
            float f3 = bottom;
            float f4 = Z_closest;
            guiMesh2.addQuad(left + side_front, f3, f4, left + side_back, bottom - border_height, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
            dynamic_playfield_mesh.addColorForLastQuad(0.3f, 0.3f, 0.3f, 1.0f);
            dynamic_playfield_mesh.addTriangleFace_textured(left + arrow_forward_front_x, top + arrow_forward_front_y_up, Z_closest, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, left + side_front + arrow_backward_front_x, top + 0.0f, Z_closest, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, left + arrow_forward_back_x, top + arrow_forward_back_y_up, Z_closest, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v, (byte) -1, Byte.MIN_VALUE, (byte) 0, (byte) -1);
            dynamic_playfield_mesh.addTriangleFace_textured(left + arrow_forward_back_x, top + arrow_forward_back_y_up, Z_closest, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, left + side_front + arrow_backward_front_x, top + 0.0f, Z_closest, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, left + side_front + arrow_backward_back_x, top + arrow_backward_inner_y_up, Z_closest, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v, (byte) -1, Byte.MIN_VALUE, (byte) 0, (byte) -1);
            dynamic_playfield_mesh.addTriangleFace_textured(left + side_front + arrow_backward_front_x, top + 0.0f, Z_closest, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, left + side_front + arrow_backward_back_x, top + 0.0f, Z_closest, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, left + side_front + arrow_backward_back_x, top + arrow_backward_inner_y_up, Z_closest, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v, (byte) -1, Byte.MIN_VALUE, (byte) 0, (byte) -1);
            dynamic_playfield_mesh.addTriangleFace_textured(left + arrow_forward_front_x, top + arrow_forward_front_y_down, Z_closest, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, left + side_front + arrow_backward_front_x, top + arrow_backward_outer_y_down, Z_closest, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, left + arrow_forward_back_x, top + arrow_forward_back_y_down, Z_closest, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v, (byte) -1, Byte.MIN_VALUE, (byte) 0, (byte) -1);
            dynamic_playfield_mesh.addTriangleFace_textured(left + arrow_forward_back_x, top + arrow_forward_back_y_down, Z_closest, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, left + side_front + arrow_backward_front_x, top + arrow_backward_outer_y_down, Z_closest, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, left + side_front + arrow_backward_back_x, top + arrow_backward_inner_y_down, Z_closest, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v, (byte) -1, Byte.MIN_VALUE, (byte) 0, (byte) -1);
            dynamic_playfield_mesh.addTriangleFace_textured(left + side_front + arrow_backward_front_x, top + arrow_backward_outer_y_down, Z_closest, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, left + side_front + arrow_backward_back_x, top + arrow_backward_outer_y_down, Z_closest, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, left + side_front + arrow_backward_back_x, top + arrow_backward_inner_y_down, Z_closest, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v, (byte) -1, Byte.MIN_VALUE, (byte) 0, (byte) -1);
            return;
        }
        float FULL_WIDTH2 = (right - left) * 0.7f;
        float left2 = left + ((right - left) * 0.15f);
        float FULL_HEIGHT2 = bottom - top;
        float side_front2 = 0.6969697f * FULL_HEIGHT2;
        float side_back2 = 1.0f * FULL_HEIGHT2;
        float arrow_forward_front_x2 = 0.0f;
        float arrow_forward_front_y_up2 = 0.475f * FULL_WIDTH2 * OneWayAnimation_closed;
        float arrow_forward_front_y_down2 = FULL_WIDTH2 - ((0.475f * FULL_WIDTH2) * OneWayAnimation_closed);
        float arrow_forward_back_x2 = border_height * 1.75f;
        float arrow_forward_back_y_up2 = border_width + (((0.475f * FULL_WIDTH2) - border_width) * OneWayAnimation_closed);
        float arrow_forward_back_y_down2 = (FULL_WIDTH2 - border_width) - (((0.475f * FULL_WIDTH2) - border_width) * OneWayAnimation_closed);
        float arrow_backward_front_x2 = -1.5f * border_height;
        float arrow_backward_back_x2 = -0.5f * border_height;
        float arrow_backward_inner_y_up2 = border_width;
        float arrow_backward_inner_y_down2 = FULL_WIDTH2 - border_width;
        float arrow_backward_outer_y_down2 = FULL_WIDTH2;
        if (flip_x) {
            side_front2 = FULL_HEIGHT2 - side_front2;
            side_back2 = FULL_HEIGHT2 - side_back2;
            arrow_forward_front_x2 = FULL_HEIGHT2 - 0.0f;
            arrow_forward_back_x2 = FULL_HEIGHT2 - arrow_forward_back_x2;
            arrow_backward_front_x2 *= -1.0f;
            arrow_backward_back_x2 *= -1.0f;
        }
        GuiMesh guiMesh3 = dynamic_playfield_mesh;
        float f5 = left2;
        float f6 = Z_closest;
        guiMesh3.addQuad(f5, top + side_front2, f6, left2 + border_width, top + side_back2, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
        dynamic_playfield_mesh.addColorForLastQuad(0.3f, 0.3f, 0.3f, 1.0f);
        GuiMesh guiMesh4 = dynamic_playfield_mesh;
        float f7 = Z_closest;
        guiMesh4.addQuad(left2 + FULL_WIDTH2, top + side_front2, f7, (left2 + FULL_WIDTH2) - border_width, top + side_back2, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
        dynamic_playfield_mesh.addColorForLastQuad(0.3f, 0.3f, 0.3f, 1.0f);
        dynamic_playfield_mesh.addTriangleFace_textured(left2 + arrow_forward_front_y_up2, top + arrow_forward_front_x2, Z_closest, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, left2 + 0.0f, top + side_front2 + arrow_backward_front_x2, Z_closest, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, left2 + arrow_forward_back_y_up2, top + arrow_forward_back_x2, Z_closest, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v, (byte) -1, Byte.MIN_VALUE, (byte) 0, (byte) -1);
        dynamic_playfield_mesh.addTriangleFace_textured(left2 + arrow_forward_back_y_up2, top + arrow_forward_back_x2, Z_closest, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, left2 + 0.0f, top + side_front2 + arrow_backward_front_x2, Z_closest, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, left2 + arrow_backward_inner_y_up2, top + side_front2 + arrow_backward_back_x2, Z_closest, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v, (byte) -1, Byte.MIN_VALUE, (byte) 0, (byte) -1);
        dynamic_playfield_mesh.addTriangleFace_textured(left2 + 0.0f, top + side_front2 + arrow_backward_front_x2, Z_closest, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, left2 + 0.0f, top + side_front2 + arrow_backward_back_x2, Z_closest, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, left2 + arrow_backward_inner_y_up2, top + side_front2 + arrow_backward_back_x2, Z_closest, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v, (byte) -1, Byte.MIN_VALUE, (byte) 0, (byte) -1);
        dynamic_playfield_mesh.addTriangleFace_textured(left2 + arrow_forward_front_y_down2, top + arrow_forward_front_x2, Z_closest, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, left2 + arrow_backward_outer_y_down2, top + side_front2 + arrow_backward_front_x2, Z_closest, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, left2 + arrow_forward_back_y_down2, top + arrow_forward_back_x2, Z_closest, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v, (byte) -1, Byte.MIN_VALUE, (byte) 0, (byte) -1);
        dynamic_playfield_mesh.addTriangleFace_textured(left2 + arrow_forward_back_y_down2, top + arrow_forward_back_x2, Z_closest, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, left2 + arrow_backward_outer_y_down2, top + side_front2 + arrow_backward_front_x2, Z_closest, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, left2 + arrow_backward_inner_y_down2, top + side_front2 + arrow_backward_back_x2, Z_closest, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v, (byte) -1, Byte.MIN_VALUE, (byte) 0, (byte) -1);
        dynamic_playfield_mesh.addTriangleFace_textured(left2 + arrow_backward_outer_y_down2, top + side_front2 + arrow_backward_front_x2, Z_closest, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, left2 + arrow_backward_outer_y_down2, top + side_front2 + arrow_backward_back_x2, Z_closest, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, left2 + arrow_backward_inner_y_down2, top + side_front2 + arrow_backward_back_x2, Z_closest, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v, (byte) -1, Byte.MIN_VALUE, (byte) 0, (byte) -1);
    }

    /* JADX WARN: Type inference failed for: r11v0, types: [com.ludocrazy.excit.LevelObject] */
    private void drawInnerWall(GuiMesh dynamic_playfield_mesh, float left, float right, float top, float bottom, float inner_width, float inner_height, float Z) throws Exception {
        boolean no_col = false;
        if (this.m_Neighbours[0][0] && this.m_Neighbours[1][0] && this.m_Neighbours[2][0] && this.m_Neighbours[0][1] && this.m_Neighbours[2][1] && this.m_Neighbours[0][2] && this.m_Neighbours[1][2] && this.m_Neighbours[2][2]) {
            dynamic_playfield_mesh.addQuad(left, top, Z, right, bottom, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
        } else if (!this.m_Neighbours[1][0] && !this.m_Neighbours[0][1] && !this.m_Neighbours[2][1] && !this.m_Neighbours[1][2]) {
            GuiMesh guiMesh = dynamic_playfield_mesh;
            float f = Z;
            guiMesh.addQuad(left + inner_width, top + inner_height, f, right - inner_width, bottom - inner_height, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
        } else if (this.m_Neighbours[1][0] && !this.m_Neighbours[0][1] && !this.m_Neighbours[2][1] && !this.m_Neighbours[1][2]) {
            GuiMesh guiMesh2 = dynamic_playfield_mesh;
            float f2 = top;
            float f3 = Z;
            guiMesh2.addQuad(left + inner_width, f2, f3, right - inner_width, bottom - inner_height, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
        } else if (!this.m_Neighbours[1][0] && !this.m_Neighbours[0][1] && this.m_Neighbours[2][1] && !this.m_Neighbours[1][2]) {
            GuiMesh guiMesh3 = dynamic_playfield_mesh;
            float f4 = Z;
            float f5 = right;
            guiMesh3.addQuad(left + inner_width, top + inner_height, f4, f5, bottom - inner_height, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
        } else if (!this.m_Neighbours[1][0] && !this.m_Neighbours[0][1] && !this.m_Neighbours[2][1] && this.m_Neighbours[1][2]) {
            GuiMesh guiMesh4 = dynamic_playfield_mesh;
            float f6 = Z;
            guiMesh4.addQuad(left + inner_width, top + inner_height, f6, right - inner_width, bottom, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
        } else if (!this.m_Neighbours[1][0] && this.m_Neighbours[0][1] && !this.m_Neighbours[2][1] && !this.m_Neighbours[1][2]) {
            GuiMesh guiMesh5 = dynamic_playfield_mesh;
            float f7 = left;
            float f8 = Z;
            guiMesh5.addQuad(f7, top + inner_height, f8, right - inner_width, bottom - inner_height, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
        } else if (this.m_Neighbours[1][0] && !this.m_Neighbours[2][0] && !this.m_Neighbours[0][1] && this.m_Neighbours[2][1] && !this.m_Neighbours[1][2]) {
            GuiMesh guiMesh6 = dynamic_playfield_mesh;
            float f9 = top;
            float f10 = Z;
            guiMesh6.addQuad(left + inner_width, f9, f10, right - inner_width, bottom - inner_height, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
            dynamic_playfield_mesh.addColorForLastQuad(0.0f, 0.0f, 0.0f, this.m_Appear);
            GuiMesh guiMesh7 = dynamic_playfield_mesh;
            float f11 = Z;
            float f12 = right;
            guiMesh7.addQuad(left + inner_width, top + inner_height, f11, f12, bottom - inner_height, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
        } else if (!this.m_Neighbours[1][0] && !this.m_Neighbours[0][1] && this.m_Neighbours[2][1] && this.m_Neighbours[1][2] && !this.m_Neighbours[2][2]) {
            GuiMesh guiMesh8 = dynamic_playfield_mesh;
            float f13 = Z;
            float f14 = right;
            guiMesh8.addQuad(left + inner_width, top + inner_height, f13, f14, bottom - inner_height, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
            dynamic_playfield_mesh.addColorForLastQuad(0.0f, 0.0f, 0.0f, this.m_Appear);
            GuiMesh guiMesh9 = dynamic_playfield_mesh;
            float f15 = Z;
            guiMesh9.addQuad(left + inner_width, top + inner_height, f15, right - inner_width, bottom, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
        } else if (!this.m_Neighbours[1][0] && this.m_Neighbours[0][1] && !this.m_Neighbours[2][1] && !this.m_Neighbours[0][2] && this.m_Neighbours[1][2]) {
            GuiMesh guiMesh10 = dynamic_playfield_mesh;
            float f16 = Z;
            guiMesh10.addQuad(left + inner_width, top + inner_height, f16, right - inner_width, bottom, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
            dynamic_playfield_mesh.addColorForLastQuad(0.0f, 0.0f, 0.0f, this.m_Appear);
            GuiMesh guiMesh11 = dynamic_playfield_mesh;
            float f17 = left;
            float f18 = Z;
            guiMesh11.addQuad(f17, top + inner_height, f18, right - inner_width, bottom - inner_height, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
        } else if (!this.m_Neighbours[0][0] && this.m_Neighbours[1][0] && this.m_Neighbours[0][1] && !this.m_Neighbours[2][1] && !this.m_Neighbours[1][2]) {
            GuiMesh guiMesh12 = dynamic_playfield_mesh;
            float f19 = left;
            float f20 = Z;
            guiMesh12.addQuad(f19, top + inner_height, f20, right - inner_width, bottom - inner_height, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
            dynamic_playfield_mesh.addColorForLastQuad(0.0f, 0.0f, 0.0f, this.m_Appear);
            GuiMesh guiMesh13 = dynamic_playfield_mesh;
            float f21 = top;
            float f22 = Z;
            guiMesh13.addQuad(left + inner_width, f21, f22, right - inner_width, bottom - inner_height, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
        } else if (this.m_Neighbours[1][0] && !this.m_Neighbours[0][1] && !this.m_Neighbours[2][1] && this.m_Neighbours[1][2]) {
            GuiMesh guiMesh14 = dynamic_playfield_mesh;
            float f23 = top;
            float f24 = Z;
            guiMesh14.addQuad(left + inner_width, f23, f24, right - inner_width, bottom, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
        } else if (!this.m_Neighbours[1][0] && this.m_Neighbours[0][1] && this.m_Neighbours[2][1] && !this.m_Neighbours[1][2]) {
            GuiMesh guiMesh15 = dynamic_playfield_mesh;
            float f25 = left;
            float f26 = Z;
            float f27 = right;
            guiMesh15.addQuad(f25, top + inner_height, f26, f27, bottom - inner_height, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
        } else if (!this.m_Neighbours[1][0] && this.m_Neighbours[0][1] && this.m_Neighbours[2][1] && !this.m_Neighbours[0][2] && this.m_Neighbours[1][2] && !this.m_Neighbours[2][2]) {
            GuiMesh guiMesh16 = dynamic_playfield_mesh;
            float f28 = left;
            float f29 = Z;
            float f30 = right;
            guiMesh16.addQuad(f28, top + inner_height, f29, f30, bottom - inner_height, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
            dynamic_playfield_mesh.addColorForLastQuad(0.0f, 0.0f, 0.0f, this.m_Appear);
            GuiMesh guiMesh17 = dynamic_playfield_mesh;
            float f31 = Z;
            guiMesh17.addQuad(left + inner_width, top + inner_height, f31, right - inner_width, bottom, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
        } else if (!this.m_Neighbours[0][0] && this.m_Neighbours[1][0] && this.m_Neighbours[0][1] && !this.m_Neighbours[2][1] && !this.m_Neighbours[0][2] && this.m_Neighbours[1][2]) {
            GuiMesh guiMesh18 = dynamic_playfield_mesh;
            float f32 = top;
            float f33 = Z;
            guiMesh18.addQuad(left + inner_width, f32, f33, right - inner_width, bottom, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
            dynamic_playfield_mesh.addColorForLastQuad(0.0f, 0.0f, 0.0f, this.m_Appear);
            GuiMesh guiMesh19 = dynamic_playfield_mesh;
            float f34 = left;
            float f35 = Z;
            guiMesh19.addQuad(f34, top + inner_height, f35, right - inner_width, bottom - inner_height, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
        } else if (!this.m_Neighbours[0][0] && this.m_Neighbours[1][0] && !this.m_Neighbours[2][0] && this.m_Neighbours[0][1] && this.m_Neighbours[2][1] && !this.m_Neighbours[1][2]) {
            GuiMesh guiMesh20 = dynamic_playfield_mesh;
            float f36 = left;
            float f37 = Z;
            float f38 = right;
            guiMesh20.addQuad(f36, top + inner_height, f37, f38, bottom - inner_height, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
            dynamic_playfield_mesh.addColorForLastQuad(0.0f, 0.0f, 0.0f, this.m_Appear);
            GuiMesh guiMesh21 = dynamic_playfield_mesh;
            float f39 = top;
            float f40 = Z;
            guiMesh21.addQuad(left + inner_width, f39, f40, right - inner_width, bottom - inner_height, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
        } else if (this.m_Neighbours[1][0] && !this.m_Neighbours[2][0] && !this.m_Neighbours[0][1] && this.m_Neighbours[2][1] && this.m_Neighbours[1][2] && !this.m_Neighbours[2][2]) {
            GuiMesh guiMesh22 = dynamic_playfield_mesh;
            float f41 = top;
            float f42 = Z;
            guiMesh22.addQuad(left + inner_width, f41, f42, right - inner_width, bottom, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
            dynamic_playfield_mesh.addColorForLastQuad(0.0f, 0.0f, 0.0f, this.m_Appear);
            GuiMesh guiMesh23 = dynamic_playfield_mesh;
            float f43 = Z;
            float f44 = right;
            guiMesh23.addQuad(left + inner_width, top + inner_height, f43, f44, bottom - inner_height, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
        } else if (!this.m_Neighbours[0][0] && this.m_Neighbours[1][0] && !this.m_Neighbours[2][0] && this.m_Neighbours[0][1] && this.m_Neighbours[2][1] && !this.m_Neighbours[0][2] && this.m_Neighbours[1][2] && !this.m_Neighbours[2][2]) {
            GuiMesh guiMesh24 = dynamic_playfield_mesh;
            float f45 = top;
            float f46 = Z;
            guiMesh24.addQuad(left + inner_width, f45, f46, right - inner_width, bottom, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
            dynamic_playfield_mesh.addColorForLastQuad(0.0f, 0.0f, 0.0f, this.m_Appear);
            GuiMesh guiMesh25 = dynamic_playfield_mesh;
            float f47 = left;
            float f48 = Z;
            float f49 = right;
            guiMesh25.addQuad(f47, top + inner_height, f48, f49, bottom - inner_height, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
        } else if (this.m_Neighbours[0][0] && this.m_Neighbours[1][0] && !this.m_Neighbours[2][0] && this.m_Neighbours[0][1] && this.m_Neighbours[2][1] && !this.m_Neighbours[0][2] && this.m_Neighbours[1][2] && !this.m_Neighbours[2][2]) {
            GuiMesh guiMesh26 = dynamic_playfield_mesh;
            float f50 = left;
            float f51 = top;
            float f52 = Z;
            guiMesh26.addQuad(f50, f51, f52, right - inner_width, bottom - inner_height, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
            dynamic_playfield_mesh.addColorForLastQuad(0.0f, 0.0f, 0.0f, this.m_Appear);
            GuiMesh guiMesh27 = dynamic_playfield_mesh;
            float f53 = left;
            float f54 = Z;
            float f55 = right;
            guiMesh27.addQuad(f53, top + inner_height, f54, f55, bottom - inner_height, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
            dynamic_playfield_mesh.addColorForLastQuad(0.0f, 0.0f, 0.0f, this.m_Appear);
            GuiMesh guiMesh28 = dynamic_playfield_mesh;
            float f56 = top;
            float f57 = Z;
            guiMesh28.addQuad(left + inner_width, f56, f57, right - inner_width, bottom, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
        } else if (!this.m_Neighbours[0][0] && this.m_Neighbours[1][0] && this.m_Neighbours[2][0] && this.m_Neighbours[0][1] && this.m_Neighbours[2][1] && !this.m_Neighbours[0][2] && this.m_Neighbours[1][2] && !this.m_Neighbours[2][2]) {
            GuiMesh guiMesh29 = dynamic_playfield_mesh;
            float f58 = top;
            float f59 = Z;
            float f60 = right;
            guiMesh29.addQuad(left + inner_width, f58, f59, f60, bottom - inner_height, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
            dynamic_playfield_mesh.addColorForLastQuad(0.0f, 0.0f, 0.0f, this.m_Appear);
            GuiMesh guiMesh30 = dynamic_playfield_mesh;
            float f61 = left;
            float f62 = Z;
            float f63 = right;
            guiMesh30.addQuad(f61, top + inner_height, f62, f63, bottom - inner_height, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
            dynamic_playfield_mesh.addColorForLastQuad(0.0f, 0.0f, 0.0f, this.m_Appear);
            GuiMesh guiMesh31 = dynamic_playfield_mesh;
            float f64 = top;
            float f65 = Z;
            guiMesh31.addQuad(left + inner_width, f64, f65, right - inner_width, bottom, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
        } else if (!this.m_Neighbours[0][0] && this.m_Neighbours[1][0] && !this.m_Neighbours[2][0] && this.m_Neighbours[0][1] && this.m_Neighbours[2][1] && !this.m_Neighbours[0][2] && this.m_Neighbours[1][2] && this.m_Neighbours[2][2]) {
            GuiMesh guiMesh32 = dynamic_playfield_mesh;
            guiMesh32.addQuad(left + inner_width, top + inner_height, Z, right, bottom, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
            dynamic_playfield_mesh.addColorForLastQuad(0.0f, 0.0f, 0.0f, this.m_Appear);
            GuiMesh guiMesh33 = dynamic_playfield_mesh;
            float f66 = left;
            float f67 = Z;
            float f68 = right;
            guiMesh33.addQuad(f66, top + inner_height, f67, f68, bottom - inner_height, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
            dynamic_playfield_mesh.addColorForLastQuad(0.0f, 0.0f, 0.0f, this.m_Appear);
            GuiMesh guiMesh34 = dynamic_playfield_mesh;
            float f69 = top;
            float f70 = Z;
            guiMesh34.addQuad(left + inner_width, f69, f70, right - inner_width, bottom, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
        } else if (!this.m_Neighbours[0][0] && this.m_Neighbours[1][0] && !this.m_Neighbours[2][0] && this.m_Neighbours[0][1] && this.m_Neighbours[2][1] && this.m_Neighbours[0][2] && this.m_Neighbours[1][2] && !this.m_Neighbours[2][2]) {
            GuiMesh guiMesh35 = dynamic_playfield_mesh;
            float f71 = left;
            float f72 = Z;
            guiMesh35.addQuad(f71, top + inner_height, f72, right - inner_width, bottom, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
            dynamic_playfield_mesh.addColorForLastQuad(0.0f, 0.0f, 0.0f, this.m_Appear);
            GuiMesh guiMesh36 = dynamic_playfield_mesh;
            float f73 = left;
            float f74 = Z;
            float f75 = right;
            guiMesh36.addQuad(f73, top + inner_height, f74, f75, bottom - inner_height, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
            dynamic_playfield_mesh.addColorForLastQuad(0.0f, 0.0f, 0.0f, this.m_Appear);
            GuiMesh guiMesh37 = dynamic_playfield_mesh;
            float f76 = top;
            float f77 = Z;
            guiMesh37.addQuad(left + inner_width, f76, f77, right - inner_width, bottom, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
        } else if (this.m_Neighbours[0][0] && this.m_Neighbours[1][0] && !this.m_Neighbours[2][0] && this.m_Neighbours[0][1] && this.m_Neighbours[2][1] && this.m_Neighbours[0][2] && this.m_Neighbours[1][2] && !this.m_Neighbours[2][2]) {
            GuiMesh guiMesh38 = dynamic_playfield_mesh;
            float f78 = left;
            float f79 = Z;
            float f80 = right;
            guiMesh38.addQuad(f78, top + inner_height, f79, f80, bottom - inner_height, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
            dynamic_playfield_mesh.addColorForLastQuad(0.0f, 0.0f, 0.0f, this.m_Appear);
            GuiMesh guiMesh39 = dynamic_playfield_mesh;
            float f81 = left;
            float f82 = top;
            float f83 = Z;
            guiMesh39.addQuad(f81, f82, f83, right - inner_width, bottom, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
        } else if (this.m_Neighbours[0][0] && this.m_Neighbours[1][0] && this.m_Neighbours[2][0] && this.m_Neighbours[0][1] && this.m_Neighbours[2][1] && !this.m_Neighbours[0][2] && this.m_Neighbours[1][2] && !this.m_Neighbours[2][2]) {
            GuiMesh guiMesh40 = dynamic_playfield_mesh;
            float f84 = top;
            float f85 = Z;
            guiMesh40.addQuad(left + inner_width, f84, f85, right - inner_width, bottom, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
            dynamic_playfield_mesh.addColorForLastQuad(0.0f, 0.0f, 0.0f, this.m_Appear);
            GuiMesh guiMesh41 = dynamic_playfield_mesh;
            float f86 = left;
            float f87 = top;
            float f88 = Z;
            float f89 = right;
            guiMesh41.addQuad(f86, f87, f88, f89, bottom - inner_height, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
        } else if (!this.m_Neighbours[0][0] && this.m_Neighbours[1][0] && this.m_Neighbours[2][0] && this.m_Neighbours[0][1] && this.m_Neighbours[2][1] && !this.m_Neighbours[0][2] && this.m_Neighbours[1][2] && this.m_Neighbours[2][2]) {
            GuiMesh guiMesh42 = dynamic_playfield_mesh;
            float f90 = left;
            float f91 = Z;
            float f92 = right;
            guiMesh42.addQuad(f90, top + inner_height, f91, f92, bottom - inner_height, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
            dynamic_playfield_mesh.addColorForLastQuad(0.0f, 0.0f, 0.0f, this.m_Appear);
            GuiMesh guiMesh43 = dynamic_playfield_mesh;
            guiMesh43.addQuad(left + inner_width, top, Z, right, bottom, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
        } else if (!this.m_Neighbours[0][0] && this.m_Neighbours[1][0] && !this.m_Neighbours[2][0] && this.m_Neighbours[0][1] && this.m_Neighbours[2][1] && this.m_Neighbours[0][2] && this.m_Neighbours[1][2] && this.m_Neighbours[2][2]) {
            GuiMesh guiMesh44 = dynamic_playfield_mesh;
            float f93 = top;
            float f94 = Z;
            guiMesh44.addQuad(left + inner_width, f93, f94, right - inner_width, bottom, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
            dynamic_playfield_mesh.addColorForLastQuad(0.0f, 0.0f, 0.0f, this.m_Appear);
            GuiMesh guiMesh45 = dynamic_playfield_mesh;
            float f95 = left;
            guiMesh45.addQuad(f95, top + inner_height, Z, right, bottom, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
        } else if (this.m_Neighbours[0][0] && this.m_Neighbours[1][0] && this.m_Neighbours[2][0] && this.m_Neighbours[0][1] && this.m_Neighbours[2][1] && this.m_Neighbours[0][2] && this.m_Neighbours[1][2] && !this.m_Neighbours[2][2]) {
            GuiMesh guiMesh46 = dynamic_playfield_mesh;
            float f96 = left;
            float f97 = top;
            float f98 = Z;
            guiMesh46.addQuad(f96, f97, f98, right - inner_width, bottom, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
            dynamic_playfield_mesh.addColorForLastQuad(0.0f, 0.0f, 0.0f, this.m_Appear);
            GuiMesh guiMesh47 = dynamic_playfield_mesh;
            float f99 = left;
            float f100 = top;
            float f101 = Z;
            float f102 = right;
            guiMesh47.addQuad(f99, f100, f101, f102, bottom - inner_height, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
        } else if (this.m_Neighbours[0][0] && this.m_Neighbours[1][0] && this.m_Neighbours[2][0] && this.m_Neighbours[0][1] && this.m_Neighbours[2][1] && !this.m_Neighbours[0][2] && this.m_Neighbours[1][2] && this.m_Neighbours[2][2]) {
            GuiMesh guiMesh48 = dynamic_playfield_mesh;
            guiMesh48.addQuad(left + inner_width, top, Z, right, bottom, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
            dynamic_playfield_mesh.addColorForLastQuad(0.0f, 0.0f, 0.0f, this.m_Appear);
            GuiMesh guiMesh49 = dynamic_playfield_mesh;
            float f103 = left;
            float f104 = top;
            float f105 = Z;
            float f106 = right;
            guiMesh49.addQuad(f103, f104, f105, f106, bottom - inner_height, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
        } else if (!this.m_Neighbours[0][0] && this.m_Neighbours[1][0] && this.m_Neighbours[2][0] && this.m_Neighbours[0][1] && this.m_Neighbours[2][1] && this.m_Neighbours[0][2] && this.m_Neighbours[1][2] && this.m_Neighbours[2][2]) {
            GuiMesh guiMesh50 = dynamic_playfield_mesh;
            float f107 = left;
            guiMesh50.addQuad(f107, top + inner_height, Z, right, bottom, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
            dynamic_playfield_mesh.addColorForLastQuad(0.0f, 0.0f, 0.0f, this.m_Appear);
            GuiMesh guiMesh51 = dynamic_playfield_mesh;
            guiMesh51.addQuad(left + inner_width, top, Z, right, bottom, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
        } else if (this.m_Neighbours[0][0] && this.m_Neighbours[1][0] && !this.m_Neighbours[2][0] && this.m_Neighbours[0][1] && this.m_Neighbours[2][1] && this.m_Neighbours[0][2] && this.m_Neighbours[1][2] && this.m_Neighbours[2][2]) {
            GuiMesh guiMesh52 = dynamic_playfield_mesh;
            float f108 = left;
            float f109 = top;
            float f110 = Z;
            guiMesh52.addQuad(f108, f109, f110, right - inner_width, bottom, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
            dynamic_playfield_mesh.addColorForLastQuad(0.0f, 0.0f, 0.0f, this.m_Appear);
            GuiMesh guiMesh53 = dynamic_playfield_mesh;
            float f111 = left;
            guiMesh53.addQuad(f111, top + inner_height, Z, right, bottom, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
        } else if (!this.m_Neighbours[1][0] && this.m_Neighbours[0][1] && this.m_Neighbours[2][1] && this.m_Neighbours[0][2] && this.m_Neighbours[1][2] && this.m_Neighbours[2][2]) {
            GuiMesh guiMesh54 = dynamic_playfield_mesh;
            float f112 = left;
            guiMesh54.addQuad(f112, top + inner_height, Z, right, bottom, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
        } else if (this.m_Neighbours[1][0] && this.m_Neighbours[2][0] && !this.m_Neighbours[0][1] && this.m_Neighbours[2][1] && this.m_Neighbours[1][2] && this.m_Neighbours[2][2]) {
            GuiMesh guiMesh55 = dynamic_playfield_mesh;
            guiMesh55.addQuad(left + inner_width, top, Z, right, bottom, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
        } else if (this.m_Neighbours[0][0] && this.m_Neighbours[1][0] && this.m_Neighbours[0][1] && !this.m_Neighbours[2][1] && this.m_Neighbours[0][2] && this.m_Neighbours[1][2]) {
            GuiMesh guiMesh56 = dynamic_playfield_mesh;
            float f113 = left;
            float f114 = top;
            float f115 = Z;
            guiMesh56.addQuad(f113, f114, f115, right - inner_width, bottom, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
        } else if (this.m_Neighbours[0][0] && this.m_Neighbours[1][0] && this.m_Neighbours[2][0] && this.m_Neighbours[0][1] && this.m_Neighbours[2][1] && !this.m_Neighbours[1][2]) {
            GuiMesh guiMesh57 = dynamic_playfield_mesh;
            float f116 = left;
            float f117 = top;
            float f118 = Z;
            float f119 = right;
            guiMesh57.addQuad(f116, f117, f118, f119, bottom - inner_height, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
        } else if (!this.m_Neighbours[1][0] && !this.m_Neighbours[0][1] && this.m_Neighbours[2][1] && this.m_Neighbours[1][2] && this.m_Neighbours[2][2]) {
            GuiMesh guiMesh58 = dynamic_playfield_mesh;
            guiMesh58.addQuad(left + inner_width, top + inner_height, Z, right, bottom, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
        } else if (!this.m_Neighbours[1][0] && this.m_Neighbours[0][1] && !this.m_Neighbours[2][1] && this.m_Neighbours[0][2] && this.m_Neighbours[1][2]) {
            GuiMesh guiMesh59 = dynamic_playfield_mesh;
            float f120 = left;
            float f121 = Z;
            guiMesh59.addQuad(f120, top + inner_height, f121, right - inner_width, bottom, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
        } else if (this.m_Neighbours[0][0] && this.m_Neighbours[1][0] && this.m_Neighbours[0][1] && !this.m_Neighbours[2][1] && !this.m_Neighbours[1][2]) {
            GuiMesh guiMesh60 = dynamic_playfield_mesh;
            float f122 = left;
            float f123 = top;
            float f124 = Z;
            guiMesh60.addQuad(f122, f123, f124, right - inner_width, bottom - inner_height, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
        } else if (this.m_Neighbours[1][0] && this.m_Neighbours[2][0] && !this.m_Neighbours[0][1] && this.m_Neighbours[2][1] && !this.m_Neighbours[1][2]) {
            GuiMesh guiMesh61 = dynamic_playfield_mesh;
            float f125 = top;
            float f126 = Z;
            float f127 = right;
            guiMesh61.addQuad(left + inner_width, f125, f126, f127, bottom - inner_height, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
        } else if (this.m_Neighbours[0][0] && this.m_Neighbours[1][0] && !this.m_Neighbours[2][0] && this.m_Neighbours[0][1] && this.m_Neighbours[2][1] && !this.m_Neighbours[0][2] && this.m_Neighbours[1][2] && this.m_Neighbours[2][2]) {
            GuiMesh guiMesh62 = dynamic_playfield_mesh;
            float f128 = left;
            float f129 = top;
            float f130 = Z;
            guiMesh62.addQuad(f128, f129, f130, right - inner_width, bottom - inner_height, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
            dynamic_playfield_mesh.addColorForLastQuad(0.0f, 0.0f, 0.0f, this.m_Appear);
            GuiMesh guiMesh63 = dynamic_playfield_mesh;
            guiMesh63.addQuad(left + inner_width, top + inner_height, Z, right, bottom, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
        } else if (!this.m_Neighbours[0][0] && this.m_Neighbours[1][0] && this.m_Neighbours[2][0] && this.m_Neighbours[0][1] && this.m_Neighbours[2][1] && this.m_Neighbours[0][2] && this.m_Neighbours[1][2] && !this.m_Neighbours[2][2]) {
            GuiMesh guiMesh64 = dynamic_playfield_mesh;
            float f131 = top;
            float f132 = Z;
            float f133 = right;
            guiMesh64.addQuad(left + inner_width, f131, f132, f133, bottom - inner_height, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
            dynamic_playfield_mesh.addColorForLastQuad(0.0f, 0.0f, 0.0f, this.m_Appear);
            GuiMesh guiMesh65 = dynamic_playfield_mesh;
            float f134 = left;
            float f135 = Z;
            guiMesh65.addQuad(f134, top + inner_height, f135, right - inner_width, bottom, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
        } else if (this.m_Neighbours[0][0] && this.m_Neighbours[1][0] && this.m_Neighbours[0][1] && !this.m_Neighbours[2][1] && !this.m_Neighbours[0][2] && this.m_Neighbours[1][2]) {
            GuiMesh guiMesh66 = dynamic_playfield_mesh;
            float f136 = left;
            float f137 = top;
            float f138 = Z;
            guiMesh66.addQuad(f136, f137, f138, right - inner_width, bottom - inner_height, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
            dynamic_playfield_mesh.addColorForLastQuad(0.0f, 0.0f, 0.0f, this.m_Appear);
            GuiMesh guiMesh67 = dynamic_playfield_mesh;
            float f139 = top;
            float f140 = Z;
            guiMesh67.addQuad(left + inner_width, f139, f140, right - inner_width, bottom, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
        } else if (!this.m_Neighbours[1][0] && this.m_Neighbours[0][1] && this.m_Neighbours[2][1] && !this.m_Neighbours[0][2] && this.m_Neighbours[1][2] && this.m_Neighbours[2][2]) {
            GuiMesh guiMesh68 = dynamic_playfield_mesh;
            guiMesh68.addQuad(left + inner_width, top + inner_height, Z, right, bottom, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
            dynamic_playfield_mesh.addColorForLastQuad(0.0f, 0.0f, 0.0f, this.m_Appear);
            GuiMesh guiMesh69 = dynamic_playfield_mesh;
            float f141 = left;
            float f142 = Z;
            float f143 = right;
            guiMesh69.addQuad(f141, top + inner_height, f142, f143, bottom - inner_height, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
        } else if (!this.m_Neighbours[0][0] && this.m_Neighbours[1][0] && this.m_Neighbours[2][0] && this.m_Neighbours[0][1] && this.m_Neighbours[2][1] && !this.m_Neighbours[1][2]) {
            GuiMesh guiMesh70 = dynamic_playfield_mesh;
            float f144 = top;
            float f145 = Z;
            float f146 = right;
            guiMesh70.addQuad(left + inner_width, f144, f145, f146, bottom - inner_height, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
            dynamic_playfield_mesh.addColorForLastQuad(0.0f, 0.0f, 0.0f, this.m_Appear);
            GuiMesh guiMesh71 = dynamic_playfield_mesh;
            float f147 = left;
            float f148 = Z;
            float f149 = right;
            guiMesh71.addQuad(f147, top + inner_height, f148, f149, bottom - inner_height, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
        } else if (this.m_Neighbours[1][0] && !this.m_Neighbours[2][0] && !this.m_Neighbours[0][1] && this.m_Neighbours[2][1] && this.m_Neighbours[1][2] && this.m_Neighbours[2][2]) {
            GuiMesh guiMesh72 = dynamic_playfield_mesh;
            guiMesh72.addQuad(left + inner_width, top + inner_height, Z, right, bottom, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
            dynamic_playfield_mesh.addColorForLastQuad(0.0f, 0.0f, 0.0f, this.m_Appear);
            GuiMesh guiMesh73 = dynamic_playfield_mesh;
            float f150 = top;
            float f151 = Z;
            guiMesh73.addQuad(left + inner_width, f150, f151, right - inner_width, bottom, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
        } else if (!this.m_Neighbours[0][0] && this.m_Neighbours[1][0] && this.m_Neighbours[0][1] && !this.m_Neighbours[2][1] && this.m_Neighbours[0][2] && this.m_Neighbours[1][2]) {
            GuiMesh guiMesh74 = dynamic_playfield_mesh;
            float f152 = left;
            float f153 = Z;
            guiMesh74.addQuad(f152, top + inner_height, f153, right - inner_width, bottom, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
            dynamic_playfield_mesh.addColorForLastQuad(0.0f, 0.0f, 0.0f, this.m_Appear);
            GuiMesh guiMesh75 = dynamic_playfield_mesh;
            float f154 = top;
            float f155 = Z;
            guiMesh75.addQuad(left + inner_width, f154, f155, right - inner_width, bottom, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
        } else if (!this.m_Neighbours[1][0] && this.m_Neighbours[0][1] && this.m_Neighbours[2][1] && this.m_Neighbours[0][2] && this.m_Neighbours[1][2] && !this.m_Neighbours[2][2]) {
            GuiMesh guiMesh76 = dynamic_playfield_mesh;
            float f156 = left;
            float f157 = Z;
            guiMesh76.addQuad(f156, top + inner_height, f157, right - inner_width, bottom, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
            dynamic_playfield_mesh.addColorForLastQuad(0.0f, 0.0f, 0.0f, this.m_Appear);
            GuiMesh guiMesh77 = dynamic_playfield_mesh;
            float f158 = left;
            float f159 = Z;
            float f160 = right;
            guiMesh77.addQuad(f158, top + inner_height, f159, f160, bottom - inner_height, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
        } else if (this.m_Neighbours[0][0] && this.m_Neighbours[1][0] && !this.m_Neighbours[2][0] && this.m_Neighbours[0][1] && this.m_Neighbours[2][1] && !this.m_Neighbours[1][2]) {
            GuiMesh guiMesh78 = dynamic_playfield_mesh;
            float f161 = left;
            float f162 = top;
            float f163 = Z;
            guiMesh78.addQuad(f161, f162, f163, right - inner_width, bottom - inner_height, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
            dynamic_playfield_mesh.addColorForLastQuad(0.0f, 0.0f, 0.0f, this.m_Appear);
            GuiMesh guiMesh79 = dynamic_playfield_mesh;
            float f164 = left;
            float f165 = Z;
            float f166 = right;
            guiMesh79.addQuad(f164, top + inner_height, f165, f166, bottom - inner_height, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
        } else if (!this.m_Neighbours[1][0] || !this.m_Neighbours[2][0] || this.m_Neighbours[0][1] || !this.m_Neighbours[2][1] || !this.m_Neighbours[1][2] || this.m_Neighbours[2][2]) {
            no_col = true;
            GuiMesh guiMesh80 = dynamic_playfield_mesh;
            float f167 = Z;
            guiMesh80.addQuad(left + inner_width, top + inner_height, f167, right - inner_width, bottom - inner_height, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
            dynamic_playfield_mesh.addColorForLastQuad(1.0f, 0.0f, 0.0f, this.m_Appear);
        } else {
            GuiMesh guiMesh81 = dynamic_playfield_mesh;
            float f168 = top;
            float f169 = Z;
            float f170 = right;
            guiMesh81.addQuad(left + inner_width, f168, f169, f170, bottom - inner_height, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
            dynamic_playfield_mesh.addColorForLastQuad(0.0f, 0.0f, 0.0f, this.m_Appear);
            GuiMesh guiMesh82 = dynamic_playfield_mesh;
            float f171 = top;
            float f172 = Z;
            guiMesh82.addQuad(left + inner_width, f171, f172, right - inner_width, bottom, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
        }
        if (!no_col) {
            dynamic_playfield_mesh.addColorForLastQuad(0.0f, 0.0f, 0.0f, this.m_Appear);
        }
    }

    private void drawPlainStyle(GuiMesh dynamic_playfield_mesh, float leftX, float topY, float Z, float Z_close) throws Exception {
        switch (this.m_ObjectCategory) {
            case OBJECT_PICKUP /*-18*/:
                dynamic_playfield_mesh.addQuad(leftX + (1.6452174f * 0.15f), topY + (1.0f * 0.15f), Z, leftX + ((1.0f - 0.15f) * 1.6452174f), topY + ((1.0f - 0.15f) * 1.0f), this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
                dynamic_playfield_mesh.addColorForLastQuad(0.9f, 0.5f, 0.0f, this.m_Appear);
                return;
            case OBJECT_EXIT /*-17*/:
                GuiMesh guiMesh = dynamic_playfield_mesh;
                float f = leftX;
                float f2 = topY;
                float f3 = Z;
                guiMesh.addQuad(f, f2, f3, leftX + 1.6452174f, topY + 1.0f, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
                if (!this.m_PlayAnimation_Exit) {
                    dynamic_playfield_mesh.addColorForLastQuad(0.0f, 0.5f, 0.0f, this.m_Appear);
                    return;
                }
                dynamic_playfield_mesh.addColorForLastQuad(0.0f, 0.5f, 0.0f, this.m_AnimationLoop);
                return;
            case OBJECT_DOOR /*-16*/:
                GuiMesh guiMesh2 = dynamic_playfield_mesh;
                float f4 = leftX;
                float f5 = topY;
                float f6 = Z;
                guiMesh2.addQuad(f4, f5, f6, leftX + 1.6452174f, topY + 1.0f, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
                dynamic_playfield_mesh.addColorForLastQuad(0.0f, 0.0f, 0.0f, this.m_Appear);
                dynamic_playfield_mesh.addQuad(leftX + (1.6452174f * 0.25f), topY + (1.0f * 0.25f), Z_close, leftX + ((1.0f - 0.25f) * 1.6452174f), topY + ((1.0f - 0.25f) * 1.0f), this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
                if (!this.m_pGameLevel.hasPlayerKeys()) {
                    dynamic_playfield_mesh.addColorForLastQuad(1.0f, 1.0f, 1.0f, this.m_Appear);
                    return;
                }
                dynamic_playfield_mesh.addColorForLastQuad(1.0f, 1.0f, 0.0f, this.m_Appear);
                return;
            case OBJECT_KEY /*-15*/:
                dynamic_playfield_mesh.addQuad(leftX + (1.6452174f * 0.25f), topY + (1.0f * 0.25f), Z, leftX + ((1.0f - 0.25f) * 1.6452174f), topY + ((1.0f - 0.25f) * 1.0f), this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
                dynamic_playfield_mesh.addColorForLastQuad(0.9f, 0.9f, 0.0f, this.m_Appear);
                return;
            case OBJECT_WALL /*-14*/:
                GuiMesh guiMesh3 = dynamic_playfield_mesh;
                float f7 = leftX;
                float f8 = topY;
                float f9 = Z;
                guiMesh3.addQuad(f7, f8, f9, leftX + 1.6452174f, topY + 1.0f, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
                dynamic_playfield_mesh.addColorForLastQuad(0.0f, 0.0f, 0.0f, this.m_Appear);
                return;
            case OBJECT_START /*-13*/:
                GuiMesh guiMesh4 = dynamic_playfield_mesh;
                float f10 = leftX;
                float f11 = topY;
                float f12 = Z;
                guiMesh4.addQuad(f10, f11, f12, leftX + 1.6452174f, topY + 1.0f, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
                dynamic_playfield_mesh.addColorForLastQuad(0.7f, 0.7f, 0.7f, this.m_Appear);
                return;
            case OBJECT_DEFLECTOR_LEFT_UP /*-12*/:
                GuiMesh guiMesh5 = dynamic_playfield_mesh;
                guiMesh5.addTriangleFace_textured(new Coords(leftX, 1.0f + topY, Z), this.m_UvCoordsStart, new Coords(1.6452174f + leftX, topY, Z), this.m_UvCoordsStart, new Coords(1.6452174f + leftX, 1.0f + topY, Z), this.m_UvCoordsStart, (byte) 0, (byte) -1);
                return;
            case OBJECT_DEFLECTOR_LEFT_DOWN /*-11*/:
                GuiMesh guiMesh6 = dynamic_playfield_mesh;
                guiMesh6.addTriangleFace_textured(new Coords(leftX, topY, Z), this.m_UvCoordsStart, new Coords(1.6452174f + leftX, topY, Z), this.m_UvCoordsStart, new Coords(1.6452174f + leftX, 1.0f + topY, Z), this.m_UvCoordsStart, (byte) 0, (byte) -1);
                return;
            case OBJECT_DEFLECTOR_RIGHT_UP /*-10*/:
                GuiMesh guiMesh7 = dynamic_playfield_mesh;
                guiMesh7.addTriangleFace_textured(new Coords(leftX, topY, Z), this.m_UvCoordsStart, new Coords(leftX, 1.0f + topY, Z), this.m_UvCoordsStart, new Coords(1.6452174f + leftX, 1.0f + topY, Z), this.m_UvCoordsStart, (byte) 0, (byte) -1);
                return;
            case OBJECT_DEFLECTOR_RIGHT_DOWN /*-9*/:
                GuiMesh guiMesh8 = dynamic_playfield_mesh;
                guiMesh8.addTriangleFace_textured(new Coords(leftX, topY, Z), this.m_UvCoordsStart, new Coords(1.6452174f + leftX, topY, Z), this.m_UvCoordsStart, new Coords(leftX, 1.0f + topY, Z), this.m_UvCoordsStart, (byte) 0, (byte) -1);
                return;
            case OBJECT_ARROW_LEFT /*-8*/:
                GuiMesh guiMesh9 = dynamic_playfield_mesh;
                guiMesh9.addTriangleFace_textured(new Coords((1.6452174f * (1.0f - 0.25f)) + leftX, (1.0f * 0.25f) + topY, Z), this.m_UvCoordsStart, new Coords((1.6452174f * 0.25f) + leftX, 0.5f + topY, Z), this.m_UvCoordsStart, new Coords((1.6452174f * (1.0f - 0.25f)) + leftX, (1.0f * (1.0f - 0.25f)) + topY, Z), this.m_UvCoordsStart, Byte.MIN_VALUE, (byte) -1);
                return;
            case OBJECT_ARROW_RIGHT /*-7*/:
                GuiMesh guiMesh10 = dynamic_playfield_mesh;
                guiMesh10.addTriangleFace_textured(new Coords((1.6452174f * 0.25f) + leftX, (1.0f * 0.25f) + topY, Z), this.m_UvCoordsStart, new Coords((1.6452174f * (1.0f - 0.25f)) + leftX, 0.5f + topY, Z), this.m_UvCoordsStart, new Coords((1.6452174f * 0.25f) + leftX, (1.0f * (1.0f - 0.25f)) + topY, Z), this.m_UvCoordsStart, Byte.MIN_VALUE, (byte) -1);
                return;
            case OBJECT_ARROW_UP /*-6*/:
                GuiMesh guiMesh11 = dynamic_playfield_mesh;
                guiMesh11.addTriangleFace_textured(new Coords((1.6452174f * 0.25f) + leftX, (1.0f * (1.0f - 0.25f)) + topY, Z), this.m_UvCoordsStart, new Coords(0.8226087f + leftX, (1.0f * 0.25f) + topY, Z), this.m_UvCoordsStart, new Coords((1.6452174f * (1.0f - 0.25f)) + leftX, (1.0f * (1.0f - 0.25f)) + topY, Z), this.m_UvCoordsStart, Byte.MIN_VALUE, (byte) -1);
                return;
            case OBJECT_ARROW_DOWN /*-5*/:
                GuiMesh guiMesh12 = dynamic_playfield_mesh;
                guiMesh12.addTriangleFace_textured(new Coords((1.6452174f * 0.25f) + leftX, (1.0f * 0.25f) + topY, Z), this.m_UvCoordsStart, new Coords(0.8226087f + leftX, (1.0f * (1.0f - 0.25f)) + topY, Z), this.m_UvCoordsStart, new Coords((1.6452174f * (1.0f - 0.25f)) + leftX, (1.0f * 0.25f) + topY, Z), this.m_UvCoordsStart, Byte.MIN_VALUE, (byte) -1);
                return;
            case OBJECT_ONEWAY_LEFT /*-4*/:
                dynamic_playfield_mesh.addQuad(leftX, topY, Z, leftX + 1.6452174f, topY + 1.0f, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
                dynamic_playfield_mesh.addColorsForLastQuad(0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f);
                return;
            case OBJECT_ONEWAY_RIGHT /*-3*/:
                dynamic_playfield_mesh.addQuad(leftX, topY, Z, leftX + 1.6452174f, topY + 1.0f, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
                dynamic_playfield_mesh.addColorsForLastQuad(0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f);
                return;
            case OBJECT_ONEWAY_UP /*-2*/:
                dynamic_playfield_mesh.addQuad(leftX, topY, Z, leftX + 1.6452174f, topY + 1.0f, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
                dynamic_playfield_mesh.addColorsForLastQuad(0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f);
                return;
            case -1:
                dynamic_playfield_mesh.addQuad(leftX, topY, Z, leftX + 1.6452174f, topY + 1.0f, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
                dynamic_playfield_mesh.addColorsForLastQuad(0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f);
                return;
            default:
                if (this.m_ObjectCategory >= 0) {
                    dynamic_playfield_mesh.addQuad(leftX, topY, Z, leftX + 1.6452174f, topY + 1.0f, this.m_UvCoordsStart.u, this.m_UvCoordsStart.v, this.m_UvCoordsEnd.u, this.m_UvCoordsEnd.v);
                    float alpha_anim = 0.5f + (0.5f * FloatMath.sin(this.m_BeamerParticleAnimation * 4.0f * 2.0f * 3.1415927f));
                    dynamic_playfield_mesh.addColorForLastQuad(this.m_BeamerColor.red, this.m_BeamerColor.green, this.m_BeamerColor.blue, this.m_Appear * alpha_anim);
                    return;
                }
                return;
        }
    }
}
