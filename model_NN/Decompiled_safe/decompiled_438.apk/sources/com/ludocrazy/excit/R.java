package com.ludocrazy.excit;

public final class R {

    public static final class array {
        public static final int button_label_background_settings_texts = 2130968579;
        public static final int button_label_map_deco_texts = 2130968578;
        public static final int button_label_music_settings_texts = 2130968577;
        public static final int button_label_sound_settings_texts = 2130968576;
    }

    public static final class attr {
    }

    public static final class drawable {
        public static final int beamer_outline = 2130837504;
        public static final int beamer_player = 2130837505;
        public static final int effect_cherry_blossoms = 2130837506;
        public static final int effect_outro = 2130837507;
        public static final int icon = 2130837508;
        public static final int icon_demo = 2130837509;
        public static final int logo_effect_fire = 2130837510;
        public static final int logo_effect_rain = 2130837511;
        public static final int speedup_player = 2130837512;
        public static final int speedup_wallhit = 2130837513;
        public static final int texture_gui = 2130837514;
        public static final int texture_splotch_no_alpha = 2130837515;
    }

    public static final class string {
        public static final int achievments_website = 2130903053;
        public static final int app_name = 2130903040;
        public static final int app_name_demo = 2130903041;
        public static final int author = 2130903042;
        public static final int button_label_achievments = 2130903067;
        public static final int button_label_buy = 2130903066;
        public static final int button_label_controls = 2130903072;
        public static final int button_label_controls_cursor = 2130903075;
        public static final int button_label_controls_disabled = 2130903074;
        public static final int button_label_controls_drag = 2130903078;
        public static final int button_label_controls_enabled = 2130903073;
        public static final int button_label_controls_ijkl = 2130903077;
        public static final int button_label_controls_wasd = 2130903076;
        public static final int button_label_exit = 2130903065;
        public static final int button_label_level_auotplay = 2130903081;
        public static final int button_label_level_leave = 2130903079;
        public static final int button_label_level_restart = 2130903080;
        public static final int button_label_reset_confirm = 2130903070;
        public static final int button_label_reset_confirm2 = 2130903071;
        public static final int button_label_reset_levels = 2130903069;
        public static final int button_label_scroll_help2 = 2130903059;
        public static final int button_label_scroll_help3 = 2130903060;
        public static final int button_label_scroll_main_rules = 2130903063;
        public static final int button_label_scroll_main_settings = 2130903064;
        public static final int button_label_scroll_settings = 2130903061;
        public static final int button_label_scroll_story = 2130903058;
        public static final int button_label_select_level = 2130903062;
        public static final int credits_link_text = 2130903055;
        public static final int credits_link_url = 2130903056;
        public static final int credits_text_further = 2130903054;
        public static final int credits_texts = 2130903082;
        public static final int intro_text = 2130903083;
        public static final int levels_not_in_demo = 2130903068;
        public static final int market_full_link_url = 2130903057;
        public static final int music_logo_sound = 2130903090;
        public static final int music_menu = 2130903091;
        public static final int rules_medals = 2130903084;
        public static final int sound_gui_button = 2130903086;
        public static final int sound_level_burnStart = 2130903096;
        public static final int sound_level_burnWall = 2130903094;
        public static final int sound_level_burningLoop = 2130903095;
        public static final int sound_level_collectKey = 2130903097;
        public static final int sound_level_collectPickup = 2130903098;
        public static final int sound_level_deflector = 2130903099;
        public static final int sound_level_doMove = 2130903100;
        public static final int sound_level_enterExit = 2130903101;
        public static final int sound_level_hitWall = 2130903102;
        public static final int sound_level_oneway = 2130903093;
        public static final int sound_level_openDoor = 2130903103;
        public static final int sound_level_restartLevel = 2130903104;
        public static final int sound_level_teleport = 2130903105;
        public static final int sound_map_level_canceled = 2130903087;
        public static final int sound_map_level_entering = 2130903088;
        public static final int sound_map_level_notEntering = 2130903089;
        public static final int sound_map_reset_all = 2130903092;
        public static final int sound_volume_changed = 2130903085;
        public static final int statistics_level_percentage = 2130903052;
        public static final int statistics_levels = 2130903048;
        public static final int statistics_levels_solved = 2130903049;
        public static final int statistics_moves = 2130903050;
        public static final int statistics_percentage_game_completed = 2130903047;
        public static final int statistics_pickups = 2130903051;
        public static final int sub_title = 2130903045;
        public static final int sub_title_demo = 2130903046;
        public static final int title = 2130903044;
        public static final int website = 2130903043;
    }
}
