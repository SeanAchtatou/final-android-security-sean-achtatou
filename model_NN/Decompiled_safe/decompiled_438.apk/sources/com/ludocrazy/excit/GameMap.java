package com.ludocrazy.excit;

import android.content.Context;
import com.ludocrazy.excit.MapLogic;
import com.ludocrazy.excit_demo.R;
import com.ludocrazy.tools.LogOutput;
import com.ludocrazy.tools.MediaManager;
import com.ludocrazy.tools.PhoneAbilities;
import com.ludocrazy.tools.Vector2Dint;

public class GameMap {
    private LogOutput DebugLogOutput = null;
    private Context m_Context = null;
    private MapLogic m_MapLogic = null;
    private MapRenderer m_MapRenderer = null;
    private MapLogic.MapField m_NextLevel = null;
    private MediaManager m_pMediaManager = null;
    public boolean m_reDrawNeeded = true;

    public void recreateMapRenderer(boolean mapDecoVisible, boolean achievments_layout) throws Exception {
        this.m_MapRenderer.createMapGui(this.m_MapLogic, mapDecoVisible, achievments_layout);
    }

    GameMap(LogOutput debug_log_output, PhoneAbilities phoneAbilities, Context context, MapRenderer guiRenderer, MediaManager pMediaManager, String mapFileName, boolean mapDecoVisible, boolean achievments_layout) throws Exception {
        this.m_Context = context;
        this.m_pMediaManager = pMediaManager;
        this.DebugLogOutput = debug_log_output;
        this.m_MapLogic = new MapLogic(debug_log_output, this.m_Context, mapFileName);
        this.m_MapRenderer = guiRenderer;
        recreateMapRenderer(mapDecoVisible, achievments_layout);
    }

    /* access modifiers changed from: package-private */
    public void fieldClick(float x_percentage, float y_percentage) throws Exception {
        Vector2Dint mapfield = this.m_MapRenderer.getTouchedMapField(x_percentage, y_percentage);
        fieldClick(mapfield.x, mapfield.y);
    }

    private void fieldClick(int mx, int my) throws Exception {
        if (mx >= 0 && my >= 0 && mx < this.m_MapLogic.m_MapWidth && my < this.m_MapLogic.m_MapHeight) {
            if (this.m_MapLogic.m_Map[mx][my] == null || !this.m_MapLogic.m_Map[mx][my].visible || (GameViewExcit.DEMOVERSION && !this.m_MapLogic.m_Map[mx][my].in_demo)) {
                this.m_pMediaManager.playSoundEffect(this.m_Context.getResources().getString(R.string.sound_map_level_notEntering));
                return;
            }
            this.m_NextLevel = this.m_MapLogic.m_Map[mx][my];
            this.m_pMediaManager.playSoundEffect(this.m_Context.getResources().getString(R.string.sound_map_level_entering));
        }
    }

    /* access modifiers changed from: package-private */
    public MapLogic.MapField getNextLevel() {
        return this.m_NextLevel;
    }

    public int getCurrentDrawCount() {
        return this.m_MapRenderer.getCurrentDrawCount();
    }

    public void resetAllLevels() {
        this.m_MapLogic.resetAllLevels(this.m_Context);
    }

    public boolean areAllLevelsCompleted() {
        this.DebugLogOutput.log(2, "uncompleted levels: " + this.m_MapLogic.m_UncompletedLevelsCount);
        return this.m_MapLogic.m_UncompletedLevelsCount == 0;
    }
}
