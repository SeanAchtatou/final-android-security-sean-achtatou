package com.badlogic.gdx.physics.box2d;

import com.badlogic.gdx.math.Vector2;

public class Transform {
    public static final int COL1_X = 2;
    public static final int COL1_Y = 3;
    public static final int COL2_X = 4;
    public static final int COL2_Y = 5;
    public static final int POS_X = 0;
    public static final int POS_Y = 1;
    private Vector2 position = new Vector2();
    public float[] vals = new float[6];

    public Transform() {
    }

    public Transform(Vector2 position2, float angle) {
        setPosition(position2);
        setRotation(angle);
    }

    public Vector2 mul(Vector2 v) {
        v.x = this.vals[0] + (this.vals[2] * v.x) + (this.vals[4] * v.y);
        v.y = this.vals[1] + (this.vals[3] * v.x) + (this.vals[5] * v.y);
        return v;
    }

    public Vector2 getPosition() {
        return this.position.set(this.vals[0], this.vals[1]);
    }

    public void setRotation(float angle) {
        float c = (float) Math.cos((double) angle);
        float s = (float) Math.sin((double) angle);
        this.vals[2] = c;
        this.vals[4] = -s;
        this.vals[3] = s;
        this.vals[5] = c;
    }

    public void setPosition(Vector2 pos) {
        this.vals[0] = pos.x;
        this.vals[1] = pos.y;
    }
}
