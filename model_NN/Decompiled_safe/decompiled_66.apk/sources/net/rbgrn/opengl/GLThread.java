package net.rbgrn.opengl;

import android.opengl.GLSurfaceView;
import android.view.SurfaceHolder;
import java.util.ArrayList;
import org.anddev.andengine.opengl.view.GLSurfaceView;

class GLThread extends Thread {
    public static final int DEBUG_CHECK_GL_ERROR = 1;
    public static final int DEBUG_LOG_GL_CALLS = 2;
    private static final boolean LOG_THREADS = false;
    public boolean mDone = false;
    private final GLSurfaceView.EGLConfigChooser mEGLConfigChooser;
    private final GLSurfaceView.EGLContextFactory mEGLContextFactory;
    private final GLSurfaceView.EGLWindowSurfaceFactory mEGLWindowSurfaceFactory;
    private EglHelper mEglHelper;
    /* access modifiers changed from: private */
    public GLThread mEglOwner;
    private final ArrayList<Runnable> mEventQueue = new ArrayList<>();
    private boolean mEventsWaiting;
    private final GLSurfaceView.GLWrapper mGLWrapper;
    private boolean mHasSurface;
    private boolean mHaveEgl;
    private int mHeight = 0;
    public SurfaceHolder mHolder;
    private boolean mPaused;
    private int mRenderMode = 1;
    private final GLSurfaceView.Renderer mRenderer;
    private boolean mRequestRender = true;
    private boolean mSizeChanged = true;
    private boolean mWaitingForSurface;
    private int mWidth = 0;
    private final GLThreadManager sGLThreadManager = new GLThreadManager(this, null);

    GLThread(GLSurfaceView.Renderer renderer, GLSurfaceView.EGLConfigChooser chooser, GLSurfaceView.EGLContextFactory contextFactory, GLSurfaceView.EGLWindowSurfaceFactory surfaceFactory, GLSurfaceView.GLWrapper wrapper) {
        this.mRenderer = renderer;
        this.mEGLConfigChooser = chooser;
        this.mEGLContextFactory = contextFactory;
        this.mEGLWindowSurfaceFactory = surfaceFactory;
        this.mGLWrapper = wrapper;
    }

    public void run() {
        setName("GLThread " + getId());
        try {
            guardedRun();
        } catch (InterruptedException e) {
        } finally {
            this.sGLThreadManager.threadExiting(this);
        }
    }

    private void stopEglLocked() {
        if (this.mHaveEgl) {
            this.mHaveEgl = false;
            this.mEglHelper.destroySurface();
            this.mEglHelper.finish();
            this.sGLThreadManager.releaseEglSurface(this);
        }
    }

    /* JADX INFO: finally extract failed */
    /* JADX WARN: Type inference failed for: r11v12, types: [javax.microedition.khronos.opengles.GL] */
    /* JADX WARNING: Code restructure failed: missing block: B:101:?, code lost:
        r4 = r0.mEglHelper.createSurface(r0.mHolder);
        r8 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:102:0x0178, code lost:
        if (r9 == false) goto L_0x018a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:103:0x017a, code lost:
        r0.mRenderer.onSurfaceCreated(r4, r0.mEglHelper.mEglConfig);
        r9 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:104:0x018a, code lost:
        if (r8 == false) goto L_0x0195;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:105:0x018c, code lost:
        r0.mRenderer.onSurfaceChanged(r4, r10, r5);
        r8 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:106:0x0195, code lost:
        if (r10 <= 0) goto L_0x0023;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:107:0x0197, code lost:
        if (r5 <= 0) goto L_0x0023;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:108:0x0199, code lost:
        r0.mRenderer.onDrawFrame(r4);
        r0.mEglHelper.swap();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:121:0x0023, code lost:
        continue;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:136:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00b8, code lost:
        if (r3 == false) goto L_0x015f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:?, code lost:
        r7 = getEvent();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x00be, code lost:
        if (r7 == null) goto L_0x0023;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x00c0, code lost:
        r7.run();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x00c7, code lost:
        if (isDone() == false) goto L_0x00ba;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x00c9, code lost:
        r11 = r0.sGLThreadManager;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x00ce, code lost:
        monitor-enter(r11);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:?, code lost:
        stopEglLocked();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x00d2, code lost:
        monitor-exit(r11);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x010c, code lost:
        r2 = r0.mSizeChanged;
        r10 = r0.mWidth;
        r5 = r0.mHeight;
        r1.mSizeChanged = false;
        r1.mRequestRender = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:0x012c, code lost:
        if (r0.mHasSurface == false) goto L_0x00b7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:0x0133, code lost:
        if (r0.mWaitingForSurface == false) goto L_0x00b7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:0x0135, code lost:
        r2 = true;
        r1.mWaitingForSurface = false;
        r0.sGLThreadManager.notifyAll();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:96:0x015f, code lost:
        if (r6 == false) goto L_0x0163;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:97:0x0161, code lost:
        r9 = true;
        r2 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:98:0x0163, code lost:
        if (r2 == false) goto L_0x0178;
     */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void guardedRun() throws java.lang.InterruptedException {
        /*
            r17 = this;
            r16 = 1
            net.rbgrn.opengl.EglHelper r11 = new net.rbgrn.opengl.EglHelper
            r0 = r17
            android.opengl.GLSurfaceView$EGLConfigChooser r0 = r0.mEGLConfigChooser
            r12 = r0
            r0 = r17
            android.opengl.GLSurfaceView$EGLContextFactory r0 = r0.mEGLContextFactory
            r13 = r0
            r0 = r17
            android.opengl.GLSurfaceView$EGLWindowSurfaceFactory r0 = r0.mEGLWindowSurfaceFactory
            r14 = r0
            r0 = r17
            android.opengl.GLSurfaceView$GLWrapper r0 = r0.mGLWrapper
            r15 = r0
            r11.<init>(r12, r13, r14, r15)
            r0 = r11
            r1 = r17
            r1.mEglHelper = r0
            r4 = 0
            r9 = 1
            r8 = 1
        L_0x0023:
            boolean r11 = r17.isDone()     // Catch:{ all -> 0x0149 }
            if (r11 == 0) goto L_0x0034
            r0 = r17
            net.rbgrn.opengl.GLThread$GLThreadManager r0 = r0.sGLThreadManager
            r11 = r0
            monitor-enter(r11)
            r17.stopEglLocked()     // Catch:{ all -> 0x01ae }
            monitor-exit(r11)     // Catch:{ all -> 0x01ae }
        L_0x0033:
            return
        L_0x0034:
            r10 = 0
            r5 = 0
            r2 = 0
            r6 = 0
            r3 = 0
            r0 = r17
            net.rbgrn.opengl.GLThread$GLThreadManager r0 = r0.sGLThreadManager     // Catch:{ all -> 0x0149 }
            r11 = r0
            monitor-enter(r11)     // Catch:{ all -> 0x0149 }
        L_0x003f:
            r0 = r17
            boolean r0 = r0.mPaused     // Catch:{ all -> 0x0146 }
            r12 = r0
            if (r12 == 0) goto L_0x0049
            r17.stopEglLocked()     // Catch:{ all -> 0x0146 }
        L_0x0049:
            r0 = r17
            boolean r0 = r0.mHasSurface     // Catch:{ all -> 0x0146 }
            r12 = r0
            if (r12 != 0) goto L_0x007e
            r0 = r17
            boolean r0 = r0.mWaitingForSurface     // Catch:{ all -> 0x0146 }
            r12 = r0
            if (r12 != 0) goto L_0x0068
            r17.stopEglLocked()     // Catch:{ all -> 0x0146 }
            r12 = 1
            r0 = r12
            r1 = r17
            r1.mWaitingForSurface = r0     // Catch:{ all -> 0x0146 }
            r0 = r17
            net.rbgrn.opengl.GLThread$GLThreadManager r0 = r0.sGLThreadManager     // Catch:{ all -> 0x0146 }
            r12 = r0
            r12.notifyAll()     // Catch:{ all -> 0x0146 }
        L_0x0068:
            r0 = r17
            boolean r0 = r0.mDone     // Catch:{ all -> 0x0146 }
            r12 = r0
            if (r12 == 0) goto L_0x00a9
            monitor-exit(r11)     // Catch:{ all -> 0x0146 }
            r0 = r17
            net.rbgrn.opengl.GLThread$GLThreadManager r0 = r0.sGLThreadManager
            r11 = r0
            monitor-enter(r11)
            r17.stopEglLocked()     // Catch:{ all -> 0x007b }
            monitor-exit(r11)     // Catch:{ all -> 0x007b }
            goto L_0x0033
        L_0x007b:
            r12 = move-exception
            monitor-exit(r11)     // Catch:{ all -> 0x007b }
            throw r12
        L_0x007e:
            r0 = r17
            boolean r0 = r0.mHaveEgl     // Catch:{ all -> 0x0146 }
            r12 = r0
            if (r12 != 0) goto L_0x0068
            r0 = r17
            net.rbgrn.opengl.GLThread$GLThreadManager r0 = r0.sGLThreadManager     // Catch:{ all -> 0x0146 }
            r12 = r0
            r0 = r12
            r1 = r17
            boolean r12 = r0.tryAcquireEglSurface(r1)     // Catch:{ all -> 0x0146 }
            if (r12 == 0) goto L_0x0068
            r12 = 1
            r0 = r12
            r1 = r17
            r1.mHaveEgl = r0     // Catch:{ all -> 0x0146 }
            r0 = r17
            net.rbgrn.opengl.EglHelper r0 = r0.mEglHelper     // Catch:{ all -> 0x0146 }
            r12 = r0
            r12.start()     // Catch:{ all -> 0x0146 }
            r12 = 1
            r0 = r12
            r1 = r17
            r1.mRequestRender = r0     // Catch:{ all -> 0x0146 }
            r6 = 1
            goto L_0x0068
        L_0x00a9:
            r0 = r17
            boolean r0 = r0.mEventsWaiting     // Catch:{ all -> 0x0146 }
            r12 = r0
            if (r12 == 0) goto L_0x00d8
            r3 = 1
            r12 = 0
            r0 = r12
            r1 = r17
            r1.mEventsWaiting = r0     // Catch:{ all -> 0x0146 }
        L_0x00b7:
            monitor-exit(r11)     // Catch:{ all -> 0x0146 }
            if (r3 == 0) goto L_0x015f
        L_0x00ba:
            java.lang.Runnable r7 = r17.getEvent()     // Catch:{ all -> 0x0149 }
            if (r7 == 0) goto L_0x0023
            r7.run()     // Catch:{ all -> 0x0149 }
            boolean r11 = r17.isDone()     // Catch:{ all -> 0x0149 }
            if (r11 == 0) goto L_0x00ba
            r0 = r17
            net.rbgrn.opengl.GLThread$GLThreadManager r0 = r0.sGLThreadManager
            r11 = r0
            monitor-enter(r11)
            r17.stopEglLocked()     // Catch:{ all -> 0x00d5 }
            monitor-exit(r11)     // Catch:{ all -> 0x00d5 }
            goto L_0x0033
        L_0x00d5:
            r12 = move-exception
            monitor-exit(r11)     // Catch:{ all -> 0x00d5 }
            throw r12
        L_0x00d8:
            r0 = r17
            boolean r0 = r0.mPaused     // Catch:{ all -> 0x0146 }
            r12 = r0
            if (r12 != 0) goto L_0x0155
            r0 = r17
            boolean r0 = r0.mHasSurface     // Catch:{ all -> 0x0146 }
            r12 = r0
            if (r12 == 0) goto L_0x0155
            r0 = r17
            boolean r0 = r0.mHaveEgl     // Catch:{ all -> 0x0146 }
            r12 = r0
            if (r12 == 0) goto L_0x0155
            r0 = r17
            int r0 = r0.mWidth     // Catch:{ all -> 0x0146 }
            r12 = r0
            if (r12 <= 0) goto L_0x0155
            r0 = r17
            int r0 = r0.mHeight     // Catch:{ all -> 0x0146 }
            r12 = r0
            if (r12 <= 0) goto L_0x0155
            r0 = r17
            boolean r0 = r0.mRequestRender     // Catch:{ all -> 0x0146 }
            r12 = r0
            if (r12 != 0) goto L_0x010c
            r0 = r17
            int r0 = r0.mRenderMode     // Catch:{ all -> 0x0146 }
            r12 = r0
            r0 = r12
            r1 = r16
            if (r0 != r1) goto L_0x0155
        L_0x010c:
            r0 = r17
            boolean r0 = r0.mSizeChanged     // Catch:{ all -> 0x0146 }
            r2 = r0
            r0 = r17
            int r0 = r0.mWidth     // Catch:{ all -> 0x0146 }
            r10 = r0
            r0 = r17
            int r0 = r0.mHeight     // Catch:{ all -> 0x0146 }
            r5 = r0
            r12 = 0
            r0 = r12
            r1 = r17
            r1.mSizeChanged = r0     // Catch:{ all -> 0x0146 }
            r12 = 0
            r0 = r12
            r1 = r17
            r1.mRequestRender = r0     // Catch:{ all -> 0x0146 }
            r0 = r17
            boolean r0 = r0.mHasSurface     // Catch:{ all -> 0x0146 }
            r12 = r0
            if (r12 == 0) goto L_0x00b7
            r0 = r17
            boolean r0 = r0.mWaitingForSurface     // Catch:{ all -> 0x0146 }
            r12 = r0
            if (r12 == 0) goto L_0x00b7
            r2 = 1
            r12 = 0
            r0 = r12
            r1 = r17
            r1.mWaitingForSurface = r0     // Catch:{ all -> 0x0146 }
            r0 = r17
            net.rbgrn.opengl.GLThread$GLThreadManager r0 = r0.sGLThreadManager     // Catch:{ all -> 0x0146 }
            r12 = r0
            r12.notifyAll()     // Catch:{ all -> 0x0146 }
            goto L_0x00b7
        L_0x0146:
            r12 = move-exception
            monitor-exit(r11)     // Catch:{ all -> 0x0146 }
            throw r12     // Catch:{ all -> 0x0149 }
        L_0x0149:
            r11 = move-exception
            r0 = r17
            net.rbgrn.opengl.GLThread$GLThreadManager r0 = r0.sGLThreadManager
            r12 = r0
            monitor-enter(r12)
            r17.stopEglLocked()     // Catch:{ all -> 0x01ab }
            monitor-exit(r12)     // Catch:{ all -> 0x01ab }
            throw r11
        L_0x0155:
            r0 = r17
            net.rbgrn.opengl.GLThread$GLThreadManager r0 = r0.sGLThreadManager     // Catch:{ all -> 0x0146 }
            r12 = r0
            r12.wait()     // Catch:{ all -> 0x0146 }
            goto L_0x003f
        L_0x015f:
            if (r6 == 0) goto L_0x0163
            r9 = 1
            r2 = 1
        L_0x0163:
            if (r2 == 0) goto L_0x0178
            r0 = r17
            net.rbgrn.opengl.EglHelper r0 = r0.mEglHelper     // Catch:{ all -> 0x0149 }
            r11 = r0
            r0 = r17
            android.view.SurfaceHolder r0 = r0.mHolder     // Catch:{ all -> 0x0149 }
            r12 = r0
            javax.microedition.khronos.opengles.GL r11 = r11.createSurface(r12)     // Catch:{ all -> 0x0149 }
            r0 = r11
            javax.microedition.khronos.opengles.GL10 r0 = (javax.microedition.khronos.opengles.GL10) r0     // Catch:{ all -> 0x0149 }
            r4 = r0
            r8 = 1
        L_0x0178:
            if (r9 == 0) goto L_0x018a
            r0 = r17
            org.anddev.andengine.opengl.view.GLSurfaceView$Renderer r0 = r0.mRenderer     // Catch:{ all -> 0x0149 }
            r11 = r0
            r0 = r17
            net.rbgrn.opengl.EglHelper r0 = r0.mEglHelper     // Catch:{ all -> 0x0149 }
            r12 = r0
            javax.microedition.khronos.egl.EGLConfig r12 = r12.mEglConfig     // Catch:{ all -> 0x0149 }
            r11.onSurfaceCreated(r4, r12)     // Catch:{ all -> 0x0149 }
            r9 = 0
        L_0x018a:
            if (r8 == 0) goto L_0x0195
            r0 = r17
            org.anddev.andengine.opengl.view.GLSurfaceView$Renderer r0 = r0.mRenderer     // Catch:{ all -> 0x0149 }
            r11 = r0
            r11.onSurfaceChanged(r4, r10, r5)     // Catch:{ all -> 0x0149 }
            r8 = 0
        L_0x0195:
            if (r10 <= 0) goto L_0x0023
            if (r5 <= 0) goto L_0x0023
            r0 = r17
            org.anddev.andengine.opengl.view.GLSurfaceView$Renderer r0 = r0.mRenderer     // Catch:{ all -> 0x0149 }
            r11 = r0
            r11.onDrawFrame(r4)     // Catch:{ all -> 0x0149 }
            r0 = r17
            net.rbgrn.opengl.EglHelper r0 = r0.mEglHelper     // Catch:{ all -> 0x0149 }
            r11 = r0
            r11.swap()     // Catch:{ all -> 0x0149 }
            goto L_0x0023
        L_0x01ab:
            r11 = move-exception
            monitor-exit(r12)     // Catch:{ all -> 0x01ab }
            throw r11
        L_0x01ae:
            r12 = move-exception
            monitor-exit(r11)     // Catch:{ all -> 0x01ae }
            throw r12
        */
        throw new UnsupportedOperationException("Method not decompiled: net.rbgrn.opengl.GLThread.guardedRun():void");
    }

    private boolean isDone() {
        boolean z;
        synchronized (this.sGLThreadManager) {
            z = this.mDone;
        }
        return z;
    }

    public void setRenderMode(int renderMode) {
        if (renderMode < 0 || renderMode > 1) {
            throw new IllegalArgumentException("renderMode");
        }
        synchronized (this.sGLThreadManager) {
            this.mRenderMode = renderMode;
            if (renderMode == 1) {
                this.sGLThreadManager.notifyAll();
            }
        }
    }

    public int getRenderMode() {
        int i;
        synchronized (this.sGLThreadManager) {
            i = this.mRenderMode;
        }
        return i;
    }

    public void requestRender() {
        synchronized (this.sGLThreadManager) {
            this.mRequestRender = true;
            this.sGLThreadManager.notifyAll();
        }
    }

    public void surfaceCreated(SurfaceHolder holder) {
        this.mHolder = holder;
        synchronized (this.sGLThreadManager) {
            this.mHasSurface = true;
            this.sGLThreadManager.notifyAll();
        }
    }

    public void surfaceDestroyed() {
        synchronized (this.sGLThreadManager) {
            this.mHasSurface = false;
            this.sGLThreadManager.notifyAll();
            while (!this.mWaitingForSurface && isAlive() && !this.mDone) {
                try {
                    this.sGLThreadManager.wait();
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                }
            }
        }
    }

    public void onPause() {
        synchronized (this.sGLThreadManager) {
            this.mPaused = true;
            this.sGLThreadManager.notifyAll();
        }
    }

    public void onResume() {
        synchronized (this.sGLThreadManager) {
            this.mPaused = false;
            this.mRequestRender = true;
            this.sGLThreadManager.notifyAll();
        }
    }

    public void onWindowResize(int w, int h) {
        synchronized (this.sGLThreadManager) {
            this.mWidth = w;
            this.mHeight = h;
            this.mSizeChanged = true;
            this.sGLThreadManager.notifyAll();
        }
    }

    public void requestExitAndWait() {
        synchronized (this.sGLThreadManager) {
            this.mDone = true;
            this.sGLThreadManager.notifyAll();
        }
        try {
            join();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }

    public void queueEvent(Runnable r) {
        synchronized (this) {
            this.mEventQueue.add(r);
            synchronized (this.sGLThreadManager) {
                this.mEventsWaiting = true;
                this.sGLThreadManager.notifyAll();
            }
        }
    }

    private Runnable getEvent() {
        synchronized (this) {
            if (this.mEventQueue.size() <= 0) {
                return null;
            }
            Runnable remove = this.mEventQueue.remove(0);
            return remove;
        }
    }

    private class GLThreadManager {
        private GLThreadManager() {
        }

        /* synthetic */ GLThreadManager(GLThread gLThread, GLThreadManager gLThreadManager) {
            this();
        }

        public synchronized void threadExiting(GLThread thread) {
            thread.mDone = true;
            if (GLThread.this.mEglOwner == thread) {
                GLThread.this.mEglOwner = null;
            }
            notifyAll();
        }

        public synchronized boolean tryAcquireEglSurface(GLThread thread) {
            boolean z;
            if (GLThread.this.mEglOwner == thread || GLThread.this.mEglOwner == null) {
                GLThread.this.mEglOwner = thread;
                notifyAll();
                z = true;
            } else {
                z = false;
            }
            return z;
        }

        public synchronized void releaseEglSurface(GLThread thread) {
            if (GLThread.this.mEglOwner == thread) {
                GLThread.this.mEglOwner = null;
            }
            notifyAll();
        }
    }
}
