package org.anddev.andengine.util;

import android.util.FloatMath;

public class Transformation {
    private float a = 1.0f;
    private float b;
    private float c;
    private float d = 1.0f;
    private float tx;
    private float ty;

    public String toString() {
        return "Transformation{[" + this.a + ", " + this.c + ", " + this.tx + "][" + this.b + ", " + this.d + ", " + this.ty + "][0.0, 0.0, 1.0]}";
    }

    public void reset() {
        setToIdentity();
    }

    public void setToIdentity() {
        this.a = 1.0f;
        this.d = 1.0f;
        this.b = 0.0f;
        this.c = 0.0f;
        this.tx = 0.0f;
        this.ty = 0.0f;
    }

    public void preTranslate(float pX, float pY) {
        Transformation transformation = TransformationPool.obtain();
        preConcat(transformation.setToTranslate(pX, pY));
        TransformationPool.recycle(transformation);
    }

    public void postTranslate(float pX, float pY) {
        Transformation transformation = TransformationPool.obtain();
        postConcat(transformation.setToTranslate(pX, pY));
        TransformationPool.recycle(transformation);
    }

    public Transformation setToTranslate(float pX, float pY) {
        this.a = 1.0f;
        this.b = 0.0f;
        this.c = 0.0f;
        this.d = 1.0f;
        this.tx = pX;
        this.ty = pY;
        return this;
    }

    public void preScale(float pScaleX, float pScaleY) {
        Transformation transformation = TransformationPool.obtain();
        preConcat(transformation.setToScale(pScaleX, pScaleY));
        TransformationPool.recycle(transformation);
    }

    public void postScale(float pScaleX, float pScaleY) {
        Transformation transformation = TransformationPool.obtain();
        postConcat(transformation.setToScale(pScaleX, pScaleY));
        TransformationPool.recycle(transformation);
    }

    public Transformation setToScale(float pScaleX, float pScaleY) {
        this.a = pScaleX;
        this.b = 0.0f;
        this.c = 0.0f;
        this.d = pScaleY;
        this.tx = 0.0f;
        this.ty = 0.0f;
        return this;
    }

    public void preRotate(float pAngle) {
        Transformation transformation = TransformationPool.obtain();
        preConcat(transformation.setToRotate(pAngle));
        TransformationPool.recycle(transformation);
    }

    public void postRotate(float pAngle) {
        Transformation transformation = TransformationPool.obtain();
        postConcat(transformation.setToRotate(pAngle));
        TransformationPool.recycle(transformation);
    }

    public Transformation setToRotate(float pAngle) {
        float angleRad = MathUtils.degToRad(pAngle);
        float sin = FloatMath.sin(angleRad);
        float cos = FloatMath.cos(angleRad);
        this.a = cos;
        this.b = sin;
        this.c = -sin;
        this.d = cos;
        this.tx = 0.0f;
        this.ty = 0.0f;
        return this;
    }

    public void postConcat(Transformation pTransformation) {
        float a1 = this.a;
        float a2 = pTransformation.a;
        float b1 = this.b;
        float b2 = pTransformation.b;
        float c1 = this.c;
        float c2 = pTransformation.c;
        float d1 = this.d;
        float d2 = pTransformation.d;
        float tx1 = this.tx;
        float tx2 = pTransformation.tx;
        float ty1 = this.ty;
        float ty2 = pTransformation.ty;
        this.a = (a1 * a2) + (b1 * c2);
        this.b = (a1 * b2) + (b1 * d2);
        this.c = (c1 * a2) + (d1 * c2);
        this.d = (c1 * b2) + (d1 * d2);
        this.tx = (tx1 * a2) + (ty1 * c2) + tx2;
        this.ty = (tx1 * b2) + (ty1 * d2) + ty2;
    }

    public void preConcat(Transformation pTransformation) {
        float a1 = pTransformation.a;
        float a2 = this.a;
        float b1 = pTransformation.b;
        float b2 = this.b;
        float c1 = pTransformation.c;
        float c2 = this.c;
        float d1 = pTransformation.d;
        float d2 = this.d;
        float tx1 = pTransformation.tx;
        float tx2 = this.tx;
        float ty1 = pTransformation.ty;
        float ty2 = this.ty;
        this.a = (a1 * a2) + (b1 * c2);
        this.b = (a1 * b2) + (b1 * d2);
        this.c = (c1 * a2) + (d1 * c2);
        this.d = (c1 * b2) + (d1 * d2);
        this.tx = (tx1 * a2) + (ty1 * c2) + tx2;
        this.ty = (tx1 * b2) + (ty1 * d2) + ty2;
    }

    public void transform(float[] pVertices) {
        int count = pVertices.length / 2;
        int j = 0;
        int i = 0;
        while (true) {
            count--;
            if (count >= 0) {
                int i2 = i + 1;
                float x = pVertices[i];
                i = i2 + 1;
                float y = pVertices[i2];
                int j2 = j + 1;
                pVertices[j] = (this.a * x) + (this.c * y) + this.tx;
                j = j2 + 1;
                pVertices[j2] = (this.b * x) + (this.d * y) + this.ty;
            } else {
                return;
            }
        }
    }
}
