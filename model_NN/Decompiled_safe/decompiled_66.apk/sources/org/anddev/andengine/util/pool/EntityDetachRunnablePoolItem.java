package org.anddev.andengine.util.pool;

import org.anddev.andengine.entity.IEntity;

public class EntityDetachRunnablePoolItem extends RunnablePoolItem {
    protected IEntity mEntity;
    protected IEntity mParent;

    public void setEntity(IEntity pEntity) {
        this.mEntity = pEntity;
    }

    public void setParent(IEntity pParent) {
        this.mParent = pParent;
    }

    public void set(IEntity pEntity, IEntity pParent) {
        this.mEntity = pEntity;
        this.mParent = pParent;
    }

    public void run() {
        this.mParent.detachChild(this.mEntity);
    }
}
