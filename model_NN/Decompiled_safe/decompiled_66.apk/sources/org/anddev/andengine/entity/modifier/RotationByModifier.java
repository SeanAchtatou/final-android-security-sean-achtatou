package org.anddev.andengine.entity.modifier;

import org.anddev.andengine.entity.IEntity;

public class RotationByModifier extends SingleValueChangeShapeModifier {
    public RotationByModifier(float pDuration, float pRotation) {
        super(pDuration, pRotation);
    }

    protected RotationByModifier(RotationByModifier pRotationByModifier) {
        super(pRotationByModifier);
    }

    public RotationByModifier clone() {
        return new RotationByModifier(this);
    }

    /* access modifiers changed from: protected */
    public void onChangeValue(IEntity pEntity, float pValue) {
        pEntity.setRotation(pEntity.getRotation() + pValue);
    }
}
