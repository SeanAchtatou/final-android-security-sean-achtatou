package org.anddev.andengine.entity.layer;

import org.anddev.andengine.entity.Entity;

public class Layer extends Entity {
    public Layer() {
        super(0.0f, 0.0f);
    }

    public Layer(float pX, float pY) {
        super(pX, pY);
    }
}
