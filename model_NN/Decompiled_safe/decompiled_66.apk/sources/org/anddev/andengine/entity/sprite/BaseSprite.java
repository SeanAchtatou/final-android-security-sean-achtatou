package org.anddev.andengine.entity.sprite;

import javax.microedition.khronos.opengles.GL10;
import org.anddev.andengine.entity.primitive.BaseRectangle;
import org.anddev.andengine.opengl.texture.region.BaseTextureRegion;
import org.anddev.andengine.opengl.util.GLHelper;
import org.anddev.andengine.opengl.vertex.RectangleVertexBuffer;

public abstract class BaseSprite extends BaseRectangle {
    protected final BaseTextureRegion mTextureRegion;

    public BaseSprite(float pX, float pY, float pWidth, float pHeight, BaseTextureRegion pTextureRegion) {
        super(pX, pY, pWidth, pHeight);
        this.mTextureRegion = pTextureRegion;
        initBlendFunction();
    }

    public BaseSprite(float pX, float pY, float pWidth, float pHeight, BaseTextureRegion pTextureRegion, RectangleVertexBuffer pRectangleVertexBuffer) {
        super(pX, pY, pWidth, pHeight, pRectangleVertexBuffer);
        this.mTextureRegion = pTextureRegion;
        initBlendFunction();
    }

    public BaseTextureRegion getTextureRegion() {
        return this.mTextureRegion;
    }

    public void reset() {
        super.reset();
        initBlendFunction();
    }

    /* access modifiers changed from: protected */
    public void onInitDraw(GL10 pGL) {
        super.onInitDraw(pGL);
        GLHelper.enableTextures(pGL);
        GLHelper.enableTexCoordArray(pGL);
    }

    /* access modifiers changed from: protected */
    public void onApplyTransformations(GL10 pGL) {
        super.onApplyTransformations(pGL);
        this.mTextureRegion.onApply(pGL);
    }

    private void initBlendFunction() {
        if (this.mTextureRegion.getTexture().getTextureOptions().mPreMultipyAlpha) {
            setBlendFunction(1, 771);
        }
    }
}
