package org.anddev.andengine.ui.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import java.util.concurrent.Callable;
import org.anddev.andengine.util.AsyncCallable;
import org.anddev.andengine.util.Callback;
import org.anddev.andengine.util.Debug;
import org.anddev.andengine.util.progress.IProgressListener;
import org.anddev.andengine.util.progress.ProgressCallable;

public abstract class BaseActivity extends Activity {

    public static class CancelledException extends Exception {
        private static final long serialVersionUID = -78123211381435596L;
    }

    /* access modifiers changed from: protected */
    public <T> void doAsync(int pTitleResID, int pMessageResID, Callable<T> pCallable, Callback<T> pCallback) {
        doAsync(pTitleResID, pMessageResID, pCallable, pCallback, (Callback<Exception>) null);
    }

    /* access modifiers changed from: protected */
    public <T> void doAsync(int pTitleResID, int pMessageResID, Callable<T> pCallable, Callback<T> pCallback, Callback<Exception> pExceptionCallback) {
        final int i = pTitleResID;
        final int i2 = pMessageResID;
        final Callable<T> callable = pCallable;
        final Callback<T> callback = pCallback;
        final Callback<Exception> callback2 = pExceptionCallback;
        new AsyncTask<Void, Void, T>() {
            private Exception mException = null;
            private ProgressDialog mPD;

            public void onPreExecute() {
                this.mPD = ProgressDialog.show(BaseActivity.this, BaseActivity.this.getString(i), BaseActivity.this.getString(i2));
                super.onPreExecute();
            }

            public T doInBackground(Void... params) {
                try {
                    return callable.call();
                } catch (Exception e) {
                    this.mException = e;
                    return null;
                }
            }

            public void onPostExecute(T result) {
                try {
                    this.mPD.dismiss();
                } catch (Exception e) {
                    Debug.e("Error", e);
                }
                if (isCancelled()) {
                    this.mException = new CancelledException();
                }
                if (this.mException == null) {
                    callback.onCallback(result);
                } else if (callback2 == null) {
                    Debug.e("Error", this.mException);
                } else {
                    callback2.onCallback(this.mException);
                }
                super.onPostExecute(result);
            }
        }.execute((Object[]) null);
    }

    /* access modifiers changed from: protected */
    public <T> void doProgressAsync(int pTitleResID, ProgressCallable<T> pCallable, Callback<T> pCallback) {
        doProgressAsync(pTitleResID, pCallable, pCallback, null);
    }

    /* access modifiers changed from: protected */
    public <T> void doProgressAsync(int pTitleResID, ProgressCallable<T> pCallable, Callback<T> pCallback, Callback<Exception> pExceptionCallback) {
        final int i = pTitleResID;
        final ProgressCallable<T> progressCallable = pCallable;
        final Callback<T> callback = pCallback;
        final Callback<Exception> callback2 = pExceptionCallback;
        new AsyncTask<Void, Integer, T>() {
            private Exception mException = null;
            private ProgressDialog mPD;

            public void onPreExecute() {
                this.mPD = new ProgressDialog(BaseActivity.this);
                this.mPD.setTitle(i);
                this.mPD.setIcon(17301582);
                this.mPD.setIndeterminate(false);
                this.mPD.setProgressStyle(1);
                this.mPD.show();
                super.onPreExecute();
            }

            public T doInBackground(Void... params) {
                try {
                    return progressCallable.call(new IProgressListener() {
                        public void onProgressChanged(int pProgress) {
                            AnonymousClass2.this.onProgressUpdate(Integer.valueOf(pProgress));
                        }
                    });
                } catch (Exception e) {
                    this.mException = e;
                    return null;
                }
            }

            public void onProgressUpdate(Integer... values) {
                this.mPD.setProgress(values[0].intValue());
            }

            public void onPostExecute(T result) {
                try {
                    this.mPD.dismiss();
                } catch (Exception e) {
                    Debug.e("Error", e);
                }
                if (isCancelled()) {
                    this.mException = new CancelledException();
                }
                if (this.mException == null) {
                    callback.onCallback(result);
                } else if (callback2 == null) {
                    Debug.e("Error", this.mException);
                } else {
                    callback2.onCallback(this.mException);
                }
                super.onPostExecute(result);
            }
        }.execute((Object[]) null);
    }

    /* access modifiers changed from: protected */
    public <T> void doAsync(int pTitleResID, int pMessageResID, AsyncCallable<T> pAsyncCallable, final Callback<T> pCallback, Callback<Exception> pExceptionCallback) {
        final ProgressDialog pd = ProgressDialog.show(this, getString(pTitleResID), getString(pMessageResID));
        pAsyncCallable.call(new Callback<T>() {
            public void onCallback(T result) {
                try {
                    pd.dismiss();
                } catch (Exception e) {
                    Debug.e("Error", e);
                }
                pCallback.onCallback(result);
            }
        }, pExceptionCallback);
    }
}
