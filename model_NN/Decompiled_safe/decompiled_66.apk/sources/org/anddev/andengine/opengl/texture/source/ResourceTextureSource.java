package org.anddev.andengine.opengl.texture.source;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

public class ResourceTextureSource implements ITextureSource {
    private final Context mContext;
    private final int mDrawableResourceID;
    private final int mHeight;
    private final int mWidth;

    public ResourceTextureSource(Context pContext, int pDrawableResourceID) {
        this.mContext = pContext;
        this.mDrawableResourceID = pDrawableResourceID;
        BitmapFactory.Options decodeOptions = new BitmapFactory.Options();
        decodeOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(pContext.getResources(), pDrawableResourceID, decodeOptions);
        this.mWidth = decodeOptions.outWidth;
        this.mHeight = decodeOptions.outHeight;
    }

    protected ResourceTextureSource(Context pContext, int pDrawableResourceID, int pWidth, int pHeight) {
        this.mContext = pContext;
        this.mDrawableResourceID = pDrawableResourceID;
        this.mWidth = pWidth;
        this.mHeight = pHeight;
    }

    public ResourceTextureSource clone() {
        return new ResourceTextureSource(this.mContext, this.mDrawableResourceID, this.mWidth, this.mHeight);
    }

    public int getHeight() {
        return this.mHeight;
    }

    public int getWidth() {
        return this.mWidth;
    }

    public Bitmap onLoadBitmap() {
        BitmapFactory.Options decodeOptions = new BitmapFactory.Options();
        decodeOptions.inPreferredConfig = Bitmap.Config.ARGB_8888;
        return BitmapFactory.decodeResource(this.mContext.getResources(), this.mDrawableResourceID, decodeOptions);
    }

    public String toString() {
        return String.valueOf(getClass().getSimpleName()) + "(" + this.mDrawableResourceID + ")";
    }
}
