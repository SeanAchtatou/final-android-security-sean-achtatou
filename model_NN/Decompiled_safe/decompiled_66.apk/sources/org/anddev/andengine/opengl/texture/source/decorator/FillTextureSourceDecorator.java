package org.anddev.andengine.opengl.texture.source.decorator;

import android.graphics.Paint;
import org.anddev.andengine.opengl.texture.source.ITextureSource;
import org.anddev.andengine.opengl.texture.source.decorator.BaseTextureSourceDecorator;
import org.anddev.andengine.opengl.texture.source.decorator.shape.ITextureSourceDecoratorShape;

public class FillTextureSourceDecorator extends BaseShapeTextureSourceDecorator {
    protected final int mFillColor;

    public FillTextureSourceDecorator(ITextureSource pTextureSource, ITextureSourceDecoratorShape pTextureSourceDecoratorShape, int pFillColor) {
        this(pTextureSource, pTextureSourceDecoratorShape, pFillColor, null);
    }

    public FillTextureSourceDecorator(ITextureSource pTextureSource, ITextureSourceDecoratorShape pTextureSourceDecoratorShape, int pFillColor, BaseTextureSourceDecorator.TextureSourceDecoratorOptions pTextureSourceDecoratorOptions) {
        super(pTextureSource, pTextureSourceDecoratorShape, pTextureSourceDecoratorOptions);
        this.mFillColor = pFillColor;
        this.mPaint.setStyle(Paint.Style.FILL);
        this.mPaint.setColor(pFillColor);
    }

    public FillTextureSourceDecorator clone() {
        return new FillTextureSourceDecorator(this.mTextureSource, this.mTextureSourceDecoratorShape, this.mFillColor, this.mTextureSourceDecoratorOptions);
    }
}
