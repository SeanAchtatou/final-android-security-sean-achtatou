package org.anddev.andengine.opengl.texture.region;

import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.opengles.GL11;
import org.anddev.andengine.opengl.buffer.BufferObjectManager;
import org.anddev.andengine.opengl.texture.Texture;
import org.anddev.andengine.opengl.texture.region.buffer.BaseTextureRegionBuffer;
import org.anddev.andengine.opengl.util.GLHelper;

public abstract class BaseTextureRegion {
    protected int mHeight;
    protected final Texture mTexture;
    protected int mTexturePositionX;
    protected int mTexturePositionY;
    protected final BaseTextureRegionBuffer mTextureRegionBuffer = onCreateTextureRegionBuffer();
    protected int mWidth;

    /* access modifiers changed from: protected */
    public abstract BaseTextureRegionBuffer onCreateTextureRegionBuffer();

    public BaseTextureRegion(Texture pTexture, int pTexturePositionX, int pTexturePositionY, int pWidth, int pHeight) {
        this.mTexture = pTexture;
        this.mTexturePositionX = pTexturePositionX;
        this.mTexturePositionY = pTexturePositionY;
        this.mWidth = pWidth;
        this.mHeight = pHeight;
        BufferObjectManager.getActiveInstance().loadBufferObject(this.mTextureRegionBuffer);
        initTextureBuffer();
    }

    /* access modifiers changed from: protected */
    public void initTextureBuffer() {
        updateTextureRegionBuffer();
    }

    public int getWidth() {
        return this.mWidth;
    }

    public int getHeight() {
        return this.mHeight;
    }

    public void setWidth(int pWidth) {
        this.mWidth = pWidth;
        updateTextureRegionBuffer();
    }

    public void setHeight(int pHeight) {
        this.mHeight = pHeight;
        updateTextureRegionBuffer();
    }

    public void setTexturePosition(int pX, int pY) {
        this.mTexturePositionX = pX;
        this.mTexturePositionY = pY;
        updateTextureRegionBuffer();
    }

    public int getTexturePositionX() {
        return this.mTexturePositionX;
    }

    public int getTexturePositionY() {
        return this.mTexturePositionY;
    }

    public Texture getTexture() {
        return this.mTexture;
    }

    public BaseTextureRegionBuffer getTextureBuffer() {
        return this.mTextureRegionBuffer;
    }

    public boolean isFlippedHorizontal() {
        return this.mTextureRegionBuffer.isFlippedHorizontal();
    }

    public void setFlippedHorizontal(boolean pFlippedHorizontal) {
        this.mTextureRegionBuffer.setFlippedHorizontal(pFlippedHorizontal);
    }

    public boolean isFlippedVertical() {
        return this.mTextureRegionBuffer.isFlippedVertical();
    }

    public void setFlippedVertical(boolean pFlippedVertical) {
        this.mTextureRegionBuffer.setFlippedVertical(pFlippedVertical);
    }

    /* access modifiers changed from: protected */
    public void updateTextureRegionBuffer() {
        this.mTextureRegionBuffer.update();
    }

    public void onApply(GL10 pGL) {
        if (GLHelper.EXTENSIONS_VERTEXBUFFEROBJECTS) {
            GL11 gl11 = (GL11) pGL;
            this.mTextureRegionBuffer.selectOnHardware(gl11);
            GLHelper.bindTexture(pGL, this.mTexture.getHardwareTextureID());
            GLHelper.texCoordZeroPointer(gl11);
            return;
        }
        GLHelper.bindTexture(pGL, this.mTexture.getHardwareTextureID());
        GLHelper.texCoordPointer(pGL, this.mTextureRegionBuffer.getFloatBuffer());
    }
}
