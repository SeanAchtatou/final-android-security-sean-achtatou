package org.anddev.andengine.audio;

import java.util.ArrayList;
import org.anddev.andengine.audio.IAudioEntity;

public abstract class BaseAudioManager<T extends IAudioEntity> implements IAudioManager<T> {
    protected final ArrayList<T> mAudioEntities = new ArrayList<>();
    protected float mMasterVolume = 1.0f;

    public float getMasterVolume() {
        return this.mMasterVolume;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public void setMasterVolume(float r6) {
        /*
            r5 = this;
            r5.mMasterVolume = r6
            java.util.ArrayList<T> r0 = r5.mAudioEntities
            int r3 = r0.size()
            r4 = 1
            int r2 = r3 - r4
        L_0x000b:
            if (r2 >= 0) goto L_0x000e
            return
        L_0x000e:
            java.lang.Object r1 = r0.get(r2)
            org.anddev.andengine.audio.IAudioEntity r1 = (org.anddev.andengine.audio.IAudioEntity) r1
            r1.onMasterVolumeChanged(r6)
            int r2 = r2 + -1
            goto L_0x000b
        */
        throw new UnsupportedOperationException("Method not decompiled: org.anddev.andengine.audio.BaseAudioManager.setMasterVolume(float):void");
    }

    public void add(T pAudioEntity) {
        this.mAudioEntities.add(pAudioEntity);
    }

    public void releaseAll() {
        ArrayList<T> audioEntities = this.mAudioEntities;
        for (int i = audioEntities.size() - 1; i >= 0; i--) {
            T audioEntity = (IAudioEntity) audioEntities.get(i);
            audioEntity.stop();
            audioEntity.release();
        }
    }
}
