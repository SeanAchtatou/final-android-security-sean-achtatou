package org.anddev.andengine.sensor.accelerometer;

import java.util.Arrays;
import org.anddev.andengine.sensor.BaseSensorData;

public class AccelerometerData extends BaseSensorData {
    public AccelerometerData() {
        super(3);
    }

    public float getX() {
        return this.mValues[0];
    }

    public float getY() {
        return this.mValues[1];
    }

    public float getZ() {
        return this.mValues[2];
    }

    public void setX(float pX) {
        this.mValues[0] = pX;
    }

    public void setY(float pY) {
        this.mValues[1] = pY;
    }

    public void setZ(float pZ) {
        this.mValues[2] = pZ;
    }

    public String toString() {
        return "Accelerometer: " + Arrays.toString(this.mValues);
    }
}
