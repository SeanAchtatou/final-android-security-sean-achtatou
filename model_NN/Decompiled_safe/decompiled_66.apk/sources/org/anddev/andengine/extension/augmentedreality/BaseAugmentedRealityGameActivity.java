package org.anddev.andengine.extension.augmentedreality;

import android.os.Bundle;
import android.view.ViewGroup;
import org.anddev.andengine.opengl.view.ComponentSizeChooser;
import org.anddev.andengine.opengl.view.RenderSurfaceView;
import org.anddev.andengine.ui.activity.BaseGameActivity;

public abstract class BaseAugmentedRealityGameActivity extends BaseGameActivity {
    private CameraPreviewSurfaceView mCameraPreviewSurfaceView;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mCameraPreviewSurfaceView = new CameraPreviewSurfaceView(this);
        addContentView(this.mCameraPreviewSurfaceView, new ViewGroup.LayoutParams(-2, -2));
        this.mRenderSurfaceView.bringToFront();
    }

    /* access modifiers changed from: protected */
    public void onSetContentView() {
        this.mRenderSurfaceView = new RenderSurfaceView(this, this.mEngine);
        this.mRenderSurfaceView.setEGLConfigChooser(new ComponentSizeChooser(4, 4, 4, 4, 16, 0));
        this.mRenderSurfaceView.getHolder().setFormat(-3);
        this.mRenderSurfaceView.applyRenderer();
        setContentView(this.mRenderSurfaceView, createSurfaceViewLayoutParams());
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }
}
