package org.anddev.andengine.extension.multiplayer.protocol.adt.message.client;

import org.anddev.andengine.extension.multiplayer.protocol.adt.message.Message;

public abstract class ClientMessage extends Message implements IClientMessage {
}
