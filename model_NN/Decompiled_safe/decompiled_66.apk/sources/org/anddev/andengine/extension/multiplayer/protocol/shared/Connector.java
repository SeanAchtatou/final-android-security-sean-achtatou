package org.anddev.andengine.extension.multiplayer.protocol.shared;

import org.anddev.andengine.extension.multiplayer.protocol.shared.Connection;

public abstract class Connector<C extends Connection> implements Connection.IConnectionListener {
    protected final C mConnection;
    protected IConnectorListener<? extends Connector<C>> mConnectorListener;

    public interface IConnectorListener<C extends Connector<?>> {
        void onConnected(Connector connector);

        void onDisconnected(Connector connector);
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: C
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public Connector(C r2) {
        /*
            r1 = this;
            r1.<init>()
            r1.mConnection = r2
            C r0 = r1.mConnection
            r0.setConnectionListener(r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.anddev.andengine.extension.multiplayer.protocol.shared.Connector.<init>(org.anddev.andengine.extension.multiplayer.protocol.shared.Connection):void");
    }

    public C getConnection() {
        return this.mConnection;
    }

    public boolean hasConnectorListener() {
        return this.mConnectorListener != null;
    }

    public IConnectorListener<? extends Connector<C>> getConnectorListener() {
        return this.mConnectorListener;
    }

    /* access modifiers changed from: protected */
    public void setConnectorListener(IConnectorListener<? extends Connector<C>> pConnectorListener) {
        this.mConnectorListener = pConnectorListener;
    }
}
