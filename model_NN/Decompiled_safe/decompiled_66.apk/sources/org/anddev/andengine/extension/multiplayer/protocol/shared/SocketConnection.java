package org.anddev.andengine.extension.multiplayer.protocol.shared;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import org.anddev.andengine.util.SocketUtils;

public class SocketConnection extends Connection {
    private final Socket mSocket;

    public SocketConnection(Socket pSocket) throws IOException {
        super(new DataInputStream(pSocket.getInputStream()), new DataOutputStream(pSocket.getOutputStream()));
        this.mSocket = pSocket;
    }

    public Socket getSocket() {
        return this.mSocket;
    }

    /* access modifiers changed from: protected */
    public void onClosed() {
        SocketUtils.closeSocket(this.mSocket);
    }
}
