package org.anddev.andengine.extension.multiplayer.protocol.adt.message.client;

import org.anddev.andengine.extension.multiplayer.protocol.adt.message.IMessage;
import org.anddev.andengine.extension.multiplayer.protocol.util.constants.ClientMessageFlags;

public interface IClientMessage extends IMessage, ClientMessageFlags {
}
