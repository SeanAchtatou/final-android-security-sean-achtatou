package org.anddev.andengine.extension.multiplayer.protocol.adt.message.client.connection;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.anddev.andengine.extension.multiplayer.protocol.adt.message.client.ClientMessage;

public class ConnectionEstablishClientMessage extends ClientMessage {
    private int mProtocolVersion;

    @Deprecated
    public ConnectionEstablishClientMessage() {
    }

    public ConnectionEstablishClientMessage(int pProtocolVersion) {
        this.mProtocolVersion = pProtocolVersion;
    }

    public int getProtocolVersion() {
        return this.mProtocolVersion;
    }

    public short getFlag() {
        return Short.MIN_VALUE;
    }

    public void onReadTransmissionData(DataInputStream pDataInputStream) throws IOException {
        this.mProtocolVersion = pDataInputStream.readInt();
    }

    public void onWriteTransmissionData(DataOutputStream pDataOutputStream) throws IOException {
        pDataOutputStream.writeInt(this.mProtocolVersion);
    }

    /* access modifiers changed from: protected */
    public void onAppendTransmissionDataForToString(StringBuilder pStringBuilder) {
        pStringBuilder.append(", getProtocolVersion()=").append(this.mProtocolVersion);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        ConnectionEstablishClientMessage other = (ConnectionEstablishClientMessage) obj;
        return getFlag() == other.getFlag() && getProtocolVersion() == other.getProtocolVersion();
    }
}
