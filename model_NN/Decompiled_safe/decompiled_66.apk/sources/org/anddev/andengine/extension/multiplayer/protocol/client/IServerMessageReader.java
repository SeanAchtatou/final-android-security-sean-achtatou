package org.anddev.andengine.extension.multiplayer.protocol.client;

import java.io.DataInputStream;
import java.io.IOException;
import org.anddev.andengine.extension.multiplayer.protocol.adt.message.IMessage;
import org.anddev.andengine.extension.multiplayer.protocol.adt.message.client.connection.ConnectionPongClientMessage;
import org.anddev.andengine.extension.multiplayer.protocol.adt.message.server.IServerMessage;
import org.anddev.andengine.extension.multiplayer.protocol.adt.message.server.connection.ConnectionAcceptedServerMessage;
import org.anddev.andengine.extension.multiplayer.protocol.adt.message.server.connection.ConnectionCloseServerMessage;
import org.anddev.andengine.extension.multiplayer.protocol.adt.message.server.connection.ConnectionPingServerMessage;
import org.anddev.andengine.extension.multiplayer.protocol.adt.message.server.connection.ConnectionPongServerMessage;
import org.anddev.andengine.extension.multiplayer.protocol.adt.message.server.connection.ConnectionRefusedServerMessage;
import org.anddev.andengine.extension.multiplayer.protocol.client.connector.ServerConnector;
import org.anddev.andengine.extension.multiplayer.protocol.shared.Connection;
import org.anddev.andengine.extension.multiplayer.protocol.shared.Connector;
import org.anddev.andengine.extension.multiplayer.protocol.shared.IMessageHandler;
import org.anddev.andengine.extension.multiplayer.protocol.shared.IMessageReader;
import org.anddev.andengine.extension.multiplayer.protocol.shared.MessageReader;
import org.anddev.andengine.extension.multiplayer.protocol.util.constants.ServerMessageFlags;

public interface IServerMessageReader<C extends Connection> extends IMessageReader<C, ServerConnector<C>, IServerMessage> {
    void handleMessage(ServerConnector serverConnector, IServerMessage iServerMessage) throws IOException;

    IServerMessage readMessage(DataInputStream dataInputStream) throws IOException;

    void recycleMessage(IServerMessage iServerMessage);

    void registerMessage(short s, Class<? extends IServerMessage> cls);

    void registerMessage(short s, Class<? extends IServerMessage> cls, IMessageHandler<C, ServerConnector<C>, IServerMessage> iMessageHandler);

    void registerMessageHandler(short s, IMessageHandler<C, ServerConnector<C>, IServerMessage> iMessageHandler);

    public static class ServerMessageReader<C extends Connection> extends MessageReader<C, ServerConnector<C>, IServerMessage> implements ServerMessageFlags, IServerMessageReader<C> {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: org.anddev.andengine.extension.multiplayer.protocol.shared.MessageReader.handleMessage(org.anddev.andengine.extension.multiplayer.protocol.shared.Connector, org.anddev.andengine.extension.multiplayer.protocol.adt.message.IMessage):void
         arg types: [org.anddev.andengine.extension.multiplayer.protocol.client.connector.ServerConnector, org.anddev.andengine.extension.multiplayer.protocol.adt.message.server.IServerMessage]
         candidates:
          org.anddev.andengine.extension.multiplayer.protocol.client.IServerMessageReader.ServerMessageReader.handleMessage(org.anddev.andengine.extension.multiplayer.protocol.client.connector.ServerConnector, org.anddev.andengine.extension.multiplayer.protocol.adt.message.server.IServerMessage):void
          org.anddev.andengine.extension.multiplayer.protocol.client.IServerMessageReader.handleMessage(org.anddev.andengine.extension.multiplayer.protocol.client.connector.ServerConnector, org.anddev.andengine.extension.multiplayer.protocol.adt.message.server.IServerMessage):void
          org.anddev.andengine.extension.multiplayer.protocol.shared.MessageReader.handleMessage(org.anddev.andengine.extension.multiplayer.protocol.shared.Connector, org.anddev.andengine.extension.multiplayer.protocol.adt.message.IMessage):void */
        public /* bridge */ /* synthetic */ void handleMessage(ServerConnector serverConnector, IServerMessage iServerMessage) throws IOException {
            handleMessage((Connector) serverConnector, (IMessage) iServerMessage);
        }

        public /* bridge */ /* synthetic */ IServerMessage readMessage(DataInputStream dataInputStream) throws IOException {
            return (IServerMessage) readMessage(dataInputStream);
        }

        public /* bridge */ /* synthetic */ void recycleMessage(IServerMessage iServerMessage) {
            recycleMessage((IMessage) iServerMessage);
        }

        public static class DefaultServerMessageReader<C extends Connection> extends ServerMessageReader<C> implements IServerMessageHandler<C> {
            public DefaultServerMessageReader() {
                registerMessage(Short.MIN_VALUE, ConnectionAcceptedServerMessage.class, this);
                registerMessage(-32767, ConnectionRefusedServerMessage.class, this);
                registerMessage(-32766, ConnectionCloseServerMessage.class, this);
                registerMessage(-32765, ConnectionPingServerMessage.class, this);
                registerMessage(ServerMessageFlags.FLAG_MESSAGE_SERVER_CONNECTION_PONG, ConnectionPongServerMessage.class, this);
            }

            public void onHandleMessage(ServerConnector<C> pServerConnector, IServerMessage pServerMessage) throws IOException {
                switch (pServerMessage.getFlag()) {
                    case Short.MIN_VALUE:
                        onHandleConnectionAcceptedServerMessage(pServerConnector, (ConnectionAcceptedServerMessage) pServerMessage);
                        return;
                    case -32767:
                        onHandleConnectionRefusedServerMessage(pServerConnector, (ConnectionRefusedServerMessage) pServerMessage);
                        return;
                    case -32766:
                        onHandleConnectionCloseServerMessage(pServerConnector, (ConnectionCloseServerMessage) pServerMessage);
                        return;
                    case -32765:
                        onHandleConnectionPingServerMessage(pServerConnector, (ConnectionPingServerMessage) pServerMessage);
                        return;
                    case -32764:
                        onHandleConnectionPongServerMessage(pServerConnector, (ConnectionPongServerMessage) pServerMessage);
                        return;
                    default:
                        return;
                }
            }

            /* access modifiers changed from: protected */
            public void onHandleConnectionRefusedServerMessage(ServerConnector<C> serverConnector, ConnectionRefusedServerMessage pConnectionRefusedServerMessage) {
            }

            /* access modifiers changed from: protected */
            public void onHandleConnectionAcceptedServerMessage(ServerConnector<C> serverConnector, ConnectionAcceptedServerMessage pConnectionAcceptedServerMessage) {
            }

            /* access modifiers changed from: protected */
            public void onHandleConnectionPingServerMessage(ServerConnector<C> pServerConnector, ConnectionPingServerMessage pConnectionPingServerMessage) throws IOException {
                pServerConnector.sendClientMessage(new ConnectionPongClientMessage(pConnectionPingServerMessage));
            }

            /* access modifiers changed from: protected */
            public void onHandleConnectionPongServerMessage(ServerConnector<C> serverConnector, ConnectionPongServerMessage pConnectionPongServerMessage) {
            }

            /* access modifiers changed from: protected */
            public void onHandleConnectionCloseServerMessage(ServerConnector<C> pServerConnector, ConnectionCloseServerMessage pConnectionCloseServerMessage) {
                if (pServerConnector.hasConnectorListener()) {
                    pServerConnector.getConnectorListener().onDisconnected((ServerConnector) pServerConnector);
                }
            }
        }
    }
}
