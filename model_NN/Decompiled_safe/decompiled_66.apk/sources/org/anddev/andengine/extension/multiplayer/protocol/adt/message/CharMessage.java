package org.anddev.andengine.extension.multiplayer.protocol.adt.message;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public abstract class CharMessage extends Message {
    protected char mCharacter;

    public CharMessage(char pCharacter) {
        this.mCharacter = pCharacter;
    }

    public CharMessage(DataInputStream pDataInputStream) throws IOException {
        read(pDataInputStream);
    }

    public char getCharacter() {
        return this.mCharacter;
    }

    public void read(DataInputStream pDataInputStream) throws IOException {
        this.mCharacter = pDataInputStream.readChar();
    }

    /* access modifiers changed from: protected */
    public void onAppendTransmissionDataForToString(StringBuilder pStringBuilder) {
        pStringBuilder.append(", getCharacter()=").append('\'').append(getCharacter()).append('\'');
    }

    public void onWriteTransmissionData(DataOutputStream pDataOutputStream) throws IOException {
        pDataOutputStream.writeChar(getCharacter());
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        CharMessage other = (CharMessage) obj;
        return getFlag() == other.getFlag() && getCharacter() == other.getCharacter();
    }
}
