package jp.Adlantis.Android;

public class SDKVersion {
    public static final String BUILDDATE = "2011-02-17 21:42:04";
    public static final String BUILDNUMBER = "114";
    public static final String SVNVERSION = "8154M";
    public static final String VERSION = "1.2.2";
}
