package com.google.gson;

import java.lang.reflect.Type;

final class JsonSerializationContextDefault implements JsonSerializationContext {
    private final MemoryRefStack ancestors = new MemoryRefStack();
    private final FieldNamingStrategy2 fieldNamingPolicy;
    private final ObjectNavigator objectNavigator;
    private final boolean serializeNulls;
    private final ParameterizedTypeHandlerMap<JsonSerializer<?>> serializers;

    JsonSerializationContextDefault(ObjectNavigator objectNavigator2, FieldNamingStrategy2 fieldNamingPolicy2, boolean serializeNulls2, ParameterizedTypeHandlerMap<JsonSerializer<?>> serializers2) {
        this.objectNavigator = objectNavigator2;
        this.fieldNamingPolicy = fieldNamingPolicy2;
        this.serializeNulls = serializeNulls2;
        this.serializers = serializers2;
    }

    public JsonElement serialize(Object src) {
        if (src == null) {
            return JsonNull.createJsonNull();
        }
        return serialize(src, src.getClass(), false);
    }

    public JsonElement serialize(Object src, Type typeOfSrc) {
        return serialize(src, typeOfSrc, true);
    }

    /* access modifiers changed from: package-private */
    public JsonElement serialize(Object src, Type typeOfSrc, boolean preserveType) {
        if (src == null) {
            return JsonNull.createJsonNull();
        }
        JsonSerializationVisitor visitor = new JsonSerializationVisitor(this.objectNavigator, this.fieldNamingPolicy, this.serializeNulls, this.serializers, this, this.ancestors);
        this.objectNavigator.accept(new ObjectTypePair(src, typeOfSrc, preserveType), visitor);
        return visitor.getJsonElement();
    }
}
