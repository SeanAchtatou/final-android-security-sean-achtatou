package com.crossfd.android.twitter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.KeyEvent;
import cross.field.StickMan.TitleActivity;
import oauth.signpost.OAuth;
import oauth.signpost.OAuthConsumer;
import oauth.signpost.OAuthProvider;
import oauth.signpost.commonshttp.CommonsHttpOAuthConsumer;
import oauth.signpost.commonshttp.CommonsHttpOAuthProvider;

public class PrepareRequestTokenActivity extends Activity {
    final String TAG = getClass().getName();
    private OAuthConsumer consumer;
    private boolean finish_flag = false;
    private OAuthProvider provider;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            this.consumer = new CommonsHttpOAuthConsumer(Constants.CONSUMER_KEY, Constants.CONSUMER_SECRET);
            this.provider = new CommonsHttpOAuthProvider(Constants.REQUEST_URL, Constants.ACCESS_URL, Constants.AUTHORIZE_URL);
        } catch (Exception e) {
            Log.e(this.TAG, "Error creating consumer / provider", e);
        }
        Log.i(this.TAG, "Starting task to retrieve request token.");
        new OAuthRequestTokenTask(this, this.consumer, this.provider).execute(new Void[0]);
    }

    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        Uri uri = intent.getData();
        if (uri != null && uri.getScheme().equals(Constants.OAUTH_CALLBACK_SCHEME)) {
            Log.i(this.TAG, "Callback received : " + uri);
            Log.i(this.TAG, "Retrieving Access Token");
            new RetrieveAccessTokenTask(this, this.consumer, this.provider, prefs).execute(uri);
            finish();
        }
    }

    public class RetrieveAccessTokenTask extends AsyncTask<Uri, Void, Void> {
        private OAuthConsumer consumer;
        private Context context;
        private SharedPreferences prefs;
        private OAuthProvider provider;

        public RetrieveAccessTokenTask(Context context2, OAuthConsumer consumer2, OAuthProvider provider2, SharedPreferences prefs2) {
            this.context = context2;
            this.consumer = consumer2;
            this.provider = provider2;
            this.prefs = prefs2;
        }

        /* access modifiers changed from: protected */
        public Void doInBackground(Uri... params) {
            try {
                this.provider.retrieveAccessToken(this.consumer, params[0].getQueryParameter(OAuth.OAUTH_VERIFIER));
                SharedPreferences.Editor edit = this.prefs.edit();
                edit.putString(OAuth.OAUTH_TOKEN, this.consumer.getToken());
                edit.putString(OAuth.OAUTH_TOKEN_SECRET, this.consumer.getTokenSecret());
                edit.commit();
                this.consumer.setTokenWithSecret(this.prefs.getString(OAuth.OAUTH_TOKEN, ""), this.prefs.getString(OAuth.OAUTH_TOKEN_SECRET, ""));
                this.context.startActivity(new Intent(this.context, TitleActivity.class));
                Log.i(PrepareRequestTokenActivity.this.TAG, "OAuth - Access Token Retrieved");
                return null;
            } catch (Exception e) {
                Log.e(PrepareRequestTokenActivity.this.TAG, "OAuth - Access Token Retrieval Error", e);
                return null;
            }
        }
    }

    public void onResume() {
        super.onResume();
        if (this.finish_flag) {
            finish();
        }
        if (!this.finish_flag) {
            this.finish_flag = true;
        }
    }

    public void onDestroy() {
        super.onDestroy();
    }

    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getAction() == 0) {
            switch (event.getKeyCode()) {
                case 4:
                    finish();
                    break;
            }
        }
        return super.dispatchKeyEvent(event);
    }
}
