package com.crossfd.android.twitter;

import android.content.SharedPreferences;
import oauth.signpost.OAuth;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.http.AccessToken;

public class TwitterUtils {
    public static boolean isAuthenticated(SharedPreferences prefs) {
        AccessToken a = new AccessToken(prefs.getString(OAuth.OAUTH_TOKEN, ""), prefs.getString(OAuth.OAUTH_TOKEN_SECRET, ""));
        Twitter twitter = new TwitterFactory().getInstance();
        twitter.setOAuthConsumer(Constants.CONSUMER_KEY, Constants.CONSUMER_SECRET);
        twitter.setOAuthAccessToken(a);
        try {
            twitter.getAccountSettings();
            return true;
        } catch (TwitterException e) {
            return false;
        }
    }

    public static void sendTweet(SharedPreferences prefs, String msg) throws Exception {
        AccessToken a = new AccessToken(prefs.getString(OAuth.OAUTH_TOKEN, ""), prefs.getString(OAuth.OAUTH_TOKEN_SECRET, ""));
        Twitter twitter = new TwitterFactory().getInstance();
        twitter.setOAuthConsumer(Constants.CONSUMER_KEY, Constants.CONSUMER_SECRET);
        twitter.setOAuthAccessToken(a);
        twitter.updateStatus(msg);
    }
}
