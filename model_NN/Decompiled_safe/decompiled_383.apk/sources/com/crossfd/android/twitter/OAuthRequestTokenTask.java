package com.crossfd.android.twitter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import oauth.signpost.OAuthConsumer;
import oauth.signpost.OAuthProvider;

public class OAuthRequestTokenTask extends AsyncTask<Void, Void, Void> {
    final String TAG = getClass().getName();
    private OAuthConsumer consumer;
    private Context context;
    private OAuthProvider provider;

    public OAuthRequestTokenTask(Context context2, OAuthConsumer consumer2, OAuthProvider provider2) {
        this.context = context2;
        this.consumer = consumer2;
        this.provider = provider2;
    }

    /* access modifiers changed from: protected */
    public Void doInBackground(Void... params) {
        try {
            Log.i(this.TAG, "Retrieving request token from Google servers");
            String url = this.provider.retrieveRequestToken(this.consumer, Constants.OAUTH_CALLBACK_URL);
            Log.i(this.TAG, "Popping a browser with the authorize URL : " + url);
            this.context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(url)).setFlags(1610612740));
            return null;
        } catch (Exception e) {
            Log.e(this.TAG, "Error during OAUth retrieve request token", e);
            return null;
        }
    }
}
