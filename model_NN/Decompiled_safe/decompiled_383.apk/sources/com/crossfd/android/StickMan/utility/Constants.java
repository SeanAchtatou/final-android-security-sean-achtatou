package com.crossfd.android.StickMan.utility;

public class Constants {
    public static final String RANKING_CATEGORY_ALL = "all";
    public static final String RANKING_CATEGORY_MONTHLY = "monthly";
    public static final String RANKING_CATEGORY_TODAY = "today";
    public static final String RANKING_CATEGORY_WEEKLY = "weekly";
    public static final String RANKING_CATEGORY_YESTERDAY = "yesterday";
    public static final String RANKING_COUNTRY_DOMESTIC = "JP";
    public static final String RANKING_COUNTRY_LOCAL = "JP";
    public static final String RANKING_COUNTRY_WORLD = "world";
    public static final String URL_GET_RANKING = "http://111.68.211.144:8080/stickmanweb/get_ranking.rs";
    public static final String URL_INSER_POINT = "http://111.68.211.144:8080/stickmanweb/submit_point.rs";
}
