package twitter4j;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import twitter4j.internal.http.HttpParameter;

public final class Query implements Serializable {
    public static final String KILOMETERS = "km";
    public static final String MILES = "mi";
    public static final String MIXED = "mixed";
    public static final String POPULAR = "popular";
    public static final String RECENT = "recent";
    private static final long serialVersionUID = -8108425822233599808L;
    private String geocode = null;
    private String lang = null;
    private String locale = null;
    private long maxId = -1;
    private int page = -1;
    private String query = null;
    private String resultType = null;
    private int rpp = -1;
    private String since = null;
    private long sinceId = -1;
    private String until = null;

    public Query() {
    }

    public Query(String query2) {
        this.query = query2;
    }

    public String getQuery() {
        return this.query;
    }

    public void setQuery(String query2) {
        this.query = query2;
    }

    public Query query(String query2) {
        setQuery(query2);
        return this;
    }

    public String getLang() {
        return this.lang;
    }

    public void setLang(String lang2) {
        this.lang = lang2;
    }

    public Query lang(String lang2) {
        setLang(lang2);
        return this;
    }

    public String getLocale() {
        return this.locale;
    }

    public void setLocale(String locale2) {
        this.locale = locale2;
    }

    public Query locale(String locale2) {
        setLocale(locale2);
        return this;
    }

    public long getMaxId() {
        return this.maxId;
    }

    public void setMaxId(long maxId2) {
        this.maxId = maxId2;
    }

    public Query maxId(long maxId2) {
        setMaxId(maxId2);
        return this;
    }

    public int getRpp() {
        return this.rpp;
    }

    public void setRpp(int rpp2) {
        this.rpp = rpp2;
    }

    public Query rpp(int rpp2) {
        setRpp(rpp2);
        return this;
    }

    public int getPage() {
        return this.page;
    }

    public void setPage(int page2) {
        this.page = page2;
    }

    public Query page(int page2) {
        setPage(page2);
        return this;
    }

    public String getSince() {
        return this.since;
    }

    public void setSince(String since2) {
        this.since = since2;
    }

    public Query since(String since2) {
        setSince(since2);
        return this;
    }

    public long getSinceId() {
        return this.sinceId;
    }

    public void setSinceId(long sinceId2) {
        this.sinceId = sinceId2;
    }

    public Query sinceId(long sinceId2) {
        setSinceId(sinceId2);
        return this;
    }

    public String getGeocode() {
        return this.geocode;
    }

    public void setGeoCode(GeoLocation location, double radius, String unit) {
        this.geocode = new StringBuffer().append(location.getLatitude()).append(",").append(location.getLongitude()).append(",").append(radius).append(unit).toString();
    }

    public Query geoCode(GeoLocation location, double radius, String unit) {
        setGeoCode(location, radius, unit);
        return this;
    }

    public String getUntil() {
        return this.until;
    }

    public void setUntil(String until2) {
        this.until = until2;
    }

    public Query until(String until2) {
        setUntil(until2);
        return this;
    }

    public String getResultType() {
        return this.resultType;
    }

    public void setResultType(String resultType2) {
        this.resultType = resultType2;
    }

    public Query resultType(String resultType2) {
        setResultType(resultType2);
        return this;
    }

    /* access modifiers changed from: package-private */
    public HttpParameter[] asHttpParameterArray() {
        ArrayList<HttpParameter> params = new ArrayList<>();
        appendParameter("q", this.query, params);
        appendParameter("lang", this.lang, params);
        appendParameter("locale", this.locale, params);
        appendParameter("max_id", this.maxId, params);
        appendParameter("rpp", (long) this.rpp, params);
        appendParameter("page", (long) this.page, params);
        appendParameter("since", this.since, params);
        appendParameter("since_id", this.sinceId, params);
        appendParameter("geocode", this.geocode, params);
        appendParameter("until", this.until, params);
        appendParameter("resultType", this.resultType, params);
        return (HttpParameter[]) params.toArray(new HttpParameter[params.size()]);
    }

    private void appendParameter(String name, String value, List<HttpParameter> params) {
        if (value != null) {
            params.add(new HttpParameter(name, value));
        }
    }

    private void appendParameter(String name, long value, List<HttpParameter> params) {
        if (0 <= value) {
            params.add(new HttpParameter(name, String.valueOf(value)));
        }
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Query query1 = (Query) o;
        if (this.maxId != query1.maxId) {
            return false;
        }
        if (this.page != query1.page) {
            return false;
        }
        if (this.rpp != query1.rpp) {
            return false;
        }
        if (this.sinceId != query1.sinceId) {
            return false;
        }
        if (this.geocode == null ? query1.geocode != null : !this.geocode.equals(query1.geocode)) {
            return false;
        }
        if (this.lang == null ? query1.lang != null : !this.lang.equals(query1.lang)) {
            return false;
        }
        if (this.locale == null ? query1.locale != null : !this.locale.equals(query1.locale)) {
            return false;
        }
        if (this.query == null ? query1.query != null : !this.query.equals(query1.query)) {
            return false;
        }
        if (this.since == null ? query1.since != null : !this.since.equals(query1.since)) {
            return false;
        }
        if (this.until == null ? query1.until != null : !this.until.equals(query1.until)) {
            return false;
        }
        return this.resultType == null ? query1.resultType == null : this.resultType.equals(query1.resultType);
    }

    public int hashCode() {
        int result;
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        if (this.query != null) {
            result = this.query.hashCode();
        } else {
            result = 0;
        }
        int i7 = result * 31;
        if (this.lang != null) {
            i = this.lang.hashCode();
        } else {
            i = 0;
        }
        int i8 = (i7 + i) * 31;
        if (this.locale != null) {
            i2 = this.locale.hashCode();
        } else {
            i2 = 0;
        }
        int i9 = (((((((i8 + i2) * 31) + ((int) (this.maxId ^ (this.maxId >>> 32)))) * 31) + this.rpp) * 31) + this.page) * 31;
        if (this.since != null) {
            i3 = this.since.hashCode();
        } else {
            i3 = 0;
        }
        int i10 = (((i9 + i3) * 31) + ((int) (this.sinceId ^ (this.sinceId >>> 32)))) * 31;
        if (this.geocode != null) {
            i4 = this.geocode.hashCode();
        } else {
            i4 = 0;
        }
        int i11 = (i10 + i4) * 31;
        if (this.until != null) {
            i5 = this.until.hashCode();
        } else {
            i5 = 0;
        }
        int i12 = (i11 + i5) * 31;
        if (this.resultType != null) {
            i6 = this.resultType.hashCode();
        } else {
            i6 = 0;
        }
        return i12 + i6;
    }

    public String toString() {
        return new StringBuffer().append("Query{query='").append(this.query).append('\'').append(", lang='").append(this.lang).append('\'').append(", locale='").append(this.locale).append('\'').append(", maxId=").append(this.maxId).append(", rpp=").append(this.rpp).append(", page=").append(this.page).append(", since='").append(this.since).append('\'').append(", sinceId=").append(this.sinceId).append(", geocode='").append(this.geocode).append('\'').append(", until='").append(this.until).append('\'').append(", resultType='").append(this.resultType).append('\'').append('}').toString();
    }
}
