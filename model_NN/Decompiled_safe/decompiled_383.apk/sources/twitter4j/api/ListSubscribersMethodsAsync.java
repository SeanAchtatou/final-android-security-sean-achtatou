package twitter4j.api;

public interface ListSubscribersMethodsAsync {
    void checkUserListSubscription(String str, int i, int i2);

    void getUserListSubscribers(String str, int i, long j);

    void subscribeUserList(String str, int i);

    void unsubscribeUserList(String str, int i);
}
