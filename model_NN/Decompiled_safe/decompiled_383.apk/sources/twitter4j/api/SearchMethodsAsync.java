package twitter4j.api;

import twitter4j.Query;

public interface SearchMethodsAsync {
    void search(Query query);
}
