package twitter4j.api;

import twitter4j.ProfileImage;

public interface UserMethodsAsync {
    void getFollowersStatuses();

    void getFollowersStatuses(int i);

    void getFollowersStatuses(int i, long j);

    void getFollowersStatuses(long j);

    void getFollowersStatuses(String str);

    void getFollowersStatuses(String str, long j);

    void getFriendsStatuses();

    void getFriendsStatuses(int i);

    void getFriendsStatuses(int i, long j);

    void getFriendsStatuses(long j);

    void getFriendsStatuses(String str);

    void getFriendsStatuses(String str, long j);

    void getMemberSuggestions(String str);

    void getProfileImage(String str, ProfileImage.ImageSize imageSize);

    void getSuggestedUserCategories();

    void getUserSuggestions(String str);

    void lookupUsers(int[] iArr);

    void lookupUsers(String[] strArr);

    void searchUsers(String str, int i);

    void showUser(int i);

    void showUser(String str);
}
