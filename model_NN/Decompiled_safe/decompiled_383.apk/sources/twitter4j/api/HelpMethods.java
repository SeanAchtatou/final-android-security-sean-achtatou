package twitter4j.api;

import twitter4j.TwitterException;

public interface HelpMethods {
    boolean test() throws TwitterException;
}
