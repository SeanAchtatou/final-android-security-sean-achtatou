package twitter4j.api;

import twitter4j.Paging;

public interface TimelineMethodsAsync {
    void getFriendsTimeline();

    void getFriendsTimeline(Paging paging);

    void getHomeTimeline();

    void getHomeTimeline(Paging paging);

    void getMentions();

    void getMentions(Paging paging);

    void getPublicTimeline();

    void getRetweetedByMe();

    void getRetweetedByMe(Paging paging);

    void getRetweetedByUser(int i, Paging paging);

    void getRetweetedByUser(String str, Paging paging);

    void getRetweetedToMe();

    void getRetweetedToMe(Paging paging);

    void getRetweetedToUser(int i, Paging paging);

    void getRetweetedToUser(String str, Paging paging);

    void getRetweetsOfMe();

    void getRetweetsOfMe(Paging paging);

    void getUserTimeline();

    void getUserTimeline(int i);

    void getUserTimeline(int i, Paging paging);

    void getUserTimeline(String str);

    void getUserTimeline(String str, Paging paging);

    void getUserTimeline(Paging paging);
}
