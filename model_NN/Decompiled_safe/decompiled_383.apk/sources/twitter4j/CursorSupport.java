package twitter4j;

public interface CursorSupport {
    long getNextCursor();

    long getPreviousCursor();

    boolean hasNext();

    boolean hasPrevious();
}
