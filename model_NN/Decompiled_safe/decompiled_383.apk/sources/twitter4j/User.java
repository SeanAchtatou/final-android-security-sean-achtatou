package twitter4j;

import java.io.Serializable;
import java.net.URL;
import java.util.Date;

public interface User extends Comparable<User>, TwitterResponse, Serializable {
    Date getCreatedAt();

    String getDescription();

    int getFavouritesCount();

    int getFollowersCount();

    int getFriendsCount();

    int getId();

    String getLang();

    int getListedCount();

    String getLocation();

    String getName();

    String getProfileBackgroundColor();

    String getProfileBackgroundImageUrl();

    URL getProfileImageURL();

    String getProfileLinkColor();

    String getProfileSidebarBorderColor();

    String getProfileSidebarFillColor();

    String getProfileTextColor();

    String getScreenName();

    Status getStatus();

    Date getStatusCreatedAt();

    long getStatusId();

    String getStatusInReplyToScreenName();

    long getStatusInReplyToStatusId();

    int getStatusInReplyToUserId();

    String getStatusSource();

    String getStatusText();

    int getStatusesCount();

    String getTimeZone();

    URL getURL();

    int getUtcOffset();

    boolean isContributorsEnabled();

    boolean isFollowRequestSent();

    boolean isGeoEnabled();

    boolean isProfileBackgroundTiled();

    boolean isProfileUseBackgroundImage();

    boolean isProtected();

    boolean isShowAllInlineMedia();

    boolean isStatusFavorited();

    boolean isStatusTruncated();

    boolean isTranslator();

    boolean isVerified();
}
