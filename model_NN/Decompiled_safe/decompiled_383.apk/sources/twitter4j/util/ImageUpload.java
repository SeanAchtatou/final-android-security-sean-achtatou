package twitter4j.util;

import java.io.File;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import oauth.signpost.OAuth;
import org.apache.http.client.methods.HttpGet;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.conf.PropertyConfiguration;
import twitter4j.http.Authorization;
import twitter4j.http.BasicAuthorization;
import twitter4j.http.OAuthAuthorization;
import twitter4j.internal.http.HttpClientWrapper;
import twitter4j.internal.http.HttpParameter;
import twitter4j.internal.http.HttpResponse;
import twitter4j.internal.org.json.JSONException;
import twitter4j.internal.org.json.JSONObject;

public abstract class ImageUpload {
    public static String DEFAULT_TWITPIC_API_KEY = null;

    public abstract String upload(File file) throws TwitterException;

    public abstract String upload(File file, String str) throws TwitterException;

    public abstract String upload(String str, InputStream inputStream) throws TwitterException;

    public abstract String upload(String str, InputStream inputStream, String str2) throws TwitterException;

    static HttpParameter[] access$000(HttpParameter[] x0, HttpParameter[] x1) {
        return appendHttpParameters(x0, x1);
    }

    public static ImageUpload getTwitpicUploader(Twitter twitter) throws TwitterException {
        Authorization auth = twitter.getAuthorization();
        if (auth instanceof OAuthAuthorization) {
            return getTwitpicUploader(DEFAULT_TWITPIC_API_KEY, (OAuthAuthorization) auth);
        }
        ensureBasicEnabled(auth);
        return getTwitpicUploader((BasicAuthorization) auth);
    }

    public static ImageUpload getTwitpicUploader(BasicAuthorization auth) {
        return new TwitpicBasicAuthUploader(auth);
    }

    public static ImageUpload getTwitpicUploader(String twitpicAPIKey, OAuthAuthorization auth) {
        return new TwitpicOAuthUploader(twitpicAPIKey, auth);
    }

    public static ImageUpload getTweetPhotoUploader(String tweetPhotoAPIKey, OAuthAuthorization auth) {
        return new TweetPhotoOAuthUploader(tweetPhotoAPIKey, auth);
    }

    public static ImageUpload getYFrogUploader(Twitter twitter) throws TwitterException {
        Authorization auth = twitter.getAuthorization();
        if (auth instanceof OAuthAuthorization) {
            return getYFrogUploader(twitter.getScreenName(), (OAuthAuthorization) auth);
        }
        ensureBasicEnabled(auth);
        return getYFrogUploader((BasicAuthorization) auth);
    }

    public static ImageUpload getYFrogUploader(BasicAuthorization auth) {
        return new YFrogBasicAuthUploader(auth);
    }

    public static ImageUpload getYFrogUploader(String userId, OAuthAuthorization auth) {
        return new YFrogOAuthUploader(userId, auth);
    }

    public static ImageUpload getImgLyUploader(OAuthAuthorization auth) {
        return new ImgLyOAuthUploader(auth);
    }

    public static ImageUpload getTwitgooUploader(OAuthAuthorization auth) {
        return new TwitgooOAuthUploader(auth);
    }

    public static ImageUpload getTwippleUploader(OAuthAuthorization auth) {
        return new TwippleUploader(auth);
    }

    private static void ensureBasicEnabled(Authorization auth) {
        if (!(auth instanceof BasicAuthorization)) {
            throw new IllegalStateException("user ID/password combination not supplied");
        }
    }

    private static class YFrogOAuthUploader extends ImageUpload {
        private static final String TWITTER_VERIFY_CREDENTIALS = "https://api.twitter.com/1/account/verify_credentials.xml";
        private static final String YFROG_UPLOAD_URL = "https://yfrog.com/api/upload";
        private OAuthAuthorization auth;
        private String user;

        public YFrogOAuthUploader(String user2, OAuthAuthorization auth2) {
            this.user = user2;
            this.auth = auth2;
        }

        public String upload(File image) throws TwitterException {
            return upload(new HttpParameter[]{new HttpParameter("media", image)});
        }

        public String upload(File image, String message) throws TwitterException {
            return upload(new HttpParameter[]{new HttpParameter("media", image), new HttpParameter("message", message)});
        }

        public String upload(String imageFileName, InputStream imageBody) throws TwitterException {
            return upload(new HttpParameter[]{new HttpParameter("media", imageFileName, imageBody)});
        }

        public String upload(String imageFileName, InputStream imageBody, String message) throws TwitterException {
            try {
                message = URLEncoder.encode(message, OAuth.ENCODING);
            } catch (UnsupportedEncodingException e) {
            }
            return upload(new HttpParameter[]{new HttpParameter("media", imageFileName, imageBody), new HttpParameter("message", message)});
        }

        private String upload(HttpParameter[] additionalParams) throws TwitterException {
            HttpResponse httpResponse = new HttpClientWrapper().post(YFROG_UPLOAD_URL, ImageUpload.access$000(new HttpParameter[]{new HttpParameter("auth", "oauth"), new HttpParameter("username", this.user), new HttpParameter("verify_url", generateSignedVerifyCredentialsURL())}, additionalParams));
            if (httpResponse.getStatusCode() != 200) {
                throw new TwitterException("YFrog image upload returned invalid status code", httpResponse);
            }
            String response = httpResponse.asString();
            if (-1 != response.indexOf("<rsp stat=\"fail\">")) {
                throw new TwitterException(new StringBuffer().append("YFrog image upload failed with this error message: ").append(response.substring(response.indexOf("msg") + 5, response.lastIndexOf("\""))).toString(), httpResponse);
            } else if (-1 != response.indexOf("<rsp stat=\"ok\">")) {
                return response.substring(response.indexOf("<mediaurl>") + "<mediaurl>".length(), response.indexOf("</mediaurl>"));
            } else {
                throw new TwitterException("Unknown YFrog response", httpResponse);
            }
        }

        private String generateSignedVerifyCredentialsURL() {
            return new StringBuffer().append("https://api.twitter.com/1/account/verify_credentials.xml?").append(OAuthAuthorization.encodeParameters(this.auth.generateOAuthSignatureHttpParams(HttpGet.METHOD_NAME, TWITTER_VERIFY_CREDENTIALS))).toString();
        }
    }

    private static class YFrogBasicAuthUploader extends ImageUpload {
        private static final String YFROG_UPLOAD_URL = "https://yfrog.com/api/upload";
        private BasicAuthorization auth;

        public YFrogBasicAuthUploader(BasicAuthorization auth2) {
            this.auth = auth2;
        }

        public String upload(File image) throws TwitterException {
            return upload(new HttpParameter[]{new HttpParameter("media", image)});
        }

        public String upload(File image, String message) throws TwitterException {
            try {
                message = URLEncoder.encode(message, OAuth.ENCODING);
            } catch (UnsupportedEncodingException e) {
            }
            return upload(new HttpParameter[]{new HttpParameter("media", image), new HttpParameter("message", message)});
        }

        public String upload(String imageFileName, InputStream imageBody) throws TwitterException {
            return upload(new HttpParameter[]{new HttpParameter("media", imageFileName, imageBody)});
        }

        public String upload(String imageFileName, InputStream imageBody, String message) throws TwitterException {
            try {
                message = URLEncoder.encode(message, OAuth.ENCODING);
            } catch (UnsupportedEncodingException e) {
            }
            return upload(new HttpParameter[]{new HttpParameter("media", imageFileName, imageBody), new HttpParameter("message", message)});
        }

        private String upload(HttpParameter[] additionalParams) throws TwitterException {
            HttpResponse httpResponse = new HttpClientWrapper().post(YFROG_UPLOAD_URL, ImageUpload.access$000(new HttpParameter[]{new HttpParameter("username", this.auth.getUserId()), new HttpParameter((String) PropertyConfiguration.PASSWORD, this.auth.getPassword())}, additionalParams));
            if (httpResponse.getStatusCode() != 200) {
                throw new TwitterException("YFrog image upload returned invalid status code", httpResponse);
            }
            String response = httpResponse.asString();
            if (-1 != response.indexOf("<rsp stat=\"fail\">")) {
                throw new TwitterException(new StringBuffer().append("YFrog image upload failed with this error message: ").append(response.substring(response.indexOf("msg") + 5, response.lastIndexOf("\""))).toString(), httpResponse);
            } else if (-1 != response.indexOf("<rsp stat=\"ok\">")) {
                return response.substring(response.indexOf("<mediaurl>") + "<mediaurl>".length(), response.indexOf("</mediaurl>"));
            } else {
                throw new TwitterException("Unknown YFrog response", httpResponse);
            }
        }
    }

    private static class TwitpicOAuthUploader extends ImageUpload {
        private static final String TWITPIC_UPLOAD_URL = "https://twitpic.com/api/2/upload.json";
        private static final String TWITTER_VERIFY_CREDENTIALS = "https://api.twitter.com/1/account/verify_credentials.json";
        private OAuthAuthorization auth;
        private String twitpicAPIKey;

        public TwitpicOAuthUploader(String twitpicAPIKey2, OAuthAuthorization auth2) {
            if (twitpicAPIKey2 == null || "".equals(twitpicAPIKey2)) {
                throw new IllegalArgumentException("The Twitpic API Key supplied to the OAuth image uploader can't be null or empty");
            }
            this.twitpicAPIKey = twitpicAPIKey2;
            this.auth = auth2;
        }

        public String upload(File image) throws TwitterException {
            return upload(new HttpParameter[]{new HttpParameter("media", image)});
        }

        public String upload(File image, String message) throws TwitterException {
            return upload(new HttpParameter[]{new HttpParameter("media", image), new HttpParameter("message", message)});
        }

        public String upload(String imageFileName, InputStream imageBody) throws TwitterException {
            return upload(new HttpParameter[]{new HttpParameter("media", imageFileName, imageBody)});
        }

        public String upload(String imageFileName, InputStream imageBody, String message) throws TwitterException {
            return upload(new HttpParameter[]{new HttpParameter("media", imageFileName, imageBody), new HttpParameter("message", message)});
        }

        private String upload(HttpParameter[] additionalParams) throws TwitterException {
            String verifyCredentialsAuthorizationHeader = generateVerifyCredentialsAuthorizationHeader();
            Map<String, String> headers = new HashMap<>();
            headers.put("X-Auth-Service-Provider", TWITTER_VERIFY_CREDENTIALS);
            headers.put("X-Verify-Credentials-Authorization", verifyCredentialsAuthorizationHeader);
            HttpResponse httpResponse = new HttpClientWrapper().post(TWITPIC_UPLOAD_URL, ImageUpload.access$000(new HttpParameter[]{new HttpParameter("key", this.twitpicAPIKey)}, additionalParams), headers);
            if (httpResponse.getStatusCode() != 200) {
                throw new TwitterException("Twitpic image upload returned invalid status code", httpResponse);
            }
            String response = httpResponse.asString();
            try {
                JSONObject json = new JSONObject(response);
                if (!json.isNull("url")) {
                    return json.getString("url");
                }
                throw new TwitterException("Unknown Twitpic response", httpResponse);
            } catch (JSONException e) {
                throw new TwitterException(new StringBuffer().append("Invalid Twitpic response: ").append(response).toString(), e);
            }
        }

        private String generateVerifyCredentialsAuthorizationHeader() {
            return new StringBuffer().append("OAuth realm=\"http://api.twitter.com/\",").append(OAuthAuthorization.encodeParameters(this.auth.generateOAuthSignatureHttpParams(HttpGet.METHOD_NAME, TWITTER_VERIFY_CREDENTIALS), ",", true)).toString();
        }
    }

    private static class TwitpicBasicAuthUploader extends ImageUpload {
        private static final String TWITPIC_UPLOAD_URL = "https://twitpic.com/api/upload";
        private BasicAuthorization auth;

        public TwitpicBasicAuthUploader(BasicAuthorization auth2) {
            this.auth = auth2;
        }

        public String upload(File image) throws TwitterException {
            return upload(new HttpParameter[]{new HttpParameter("media", image)});
        }

        public String upload(File image, String message) throws TwitterException {
            return upload(new HttpParameter[]{new HttpParameter("media", image), new HttpParameter("message", message)});
        }

        public String upload(String imageFileName, InputStream imageBody) throws TwitterException {
            return upload(new HttpParameter[]{new HttpParameter("media", imageFileName, imageBody)});
        }

        public String upload(String imageFileName, InputStream imageBody, String message) throws TwitterException {
            return upload(new HttpParameter[]{new HttpParameter("media", imageFileName, imageBody), new HttpParameter("message", message)});
        }

        private String upload(HttpParameter[] additionalParams) throws TwitterException {
            HttpResponse httpResponse = new HttpClientWrapper().post(TWITPIC_UPLOAD_URL, ImageUpload.access$000(new HttpParameter[]{new HttpParameter("username", this.auth.getUserId()), new HttpParameter((String) PropertyConfiguration.PASSWORD, this.auth.getPassword())}, additionalParams));
            if (httpResponse.getStatusCode() != 200) {
                throw new TwitterException("Twitpic image upload returned invalid status code", httpResponse);
            }
            String response = httpResponse.asString();
            if (-1 != response.indexOf("<rsp stat=\"fail\">")) {
                throw new TwitterException(new StringBuffer().append("Twitpic image upload failed with this error message: ").append(response.substring(response.indexOf("msg") + 5, response.lastIndexOf("\""))).toString(), httpResponse);
            } else if (-1 != response.indexOf("<rsp stat=\"ok\">")) {
                return response.substring(response.indexOf("<mediaurl>") + "<mediaurl>".length(), response.indexOf("</mediaurl>"));
            } else {
                throw new TwitterException("Unknown Twitpic response", httpResponse);
            }
        }
    }

    public static class TweetPhotoOAuthUploader extends ImageUpload {
        private static final String TWEETPHOTO_UPLOAD_URL = "http://tweetphotoapi.com/api/upload.aspx";
        private static final String TWITTER_VERIFY_CREDENTIALS = "https://api.twitter.com/1/account/verify_credentials.xml";
        private OAuthAuthorization auth;
        private String tweetPhotoAPIKey;

        public TweetPhotoOAuthUploader(String tweetPhotoAPIKey2, OAuthAuthorization auth2) {
            if (tweetPhotoAPIKey2 == null || "".equals(tweetPhotoAPIKey2)) {
                throw new IllegalArgumentException("The TweetPhoto API Key supplied to the OAuth image uploader can't be null or empty");
            }
            this.tweetPhotoAPIKey = tweetPhotoAPIKey2;
            this.auth = auth2;
        }

        public String upload(File image) throws TwitterException {
            return upload(new HttpParameter[]{new HttpParameter("media", image)});
        }

        public String upload(File image, String message) throws TwitterException {
            return upload(new HttpParameter[]{new HttpParameter("media", image), new HttpParameter("message", message)});
        }

        public String upload(String imageFileName, InputStream imageBody) throws TwitterException {
            return upload(new HttpParameter[]{new HttpParameter("media", imageFileName, imageBody)});
        }

        public String upload(String imageFileName, InputStream imageBody, String message) throws TwitterException {
            return upload(new HttpParameter[]{new HttpParameter("media", imageFileName, imageBody), new HttpParameter("message", message)});
        }

        private String upload(HttpParameter[] additionalParams) throws TwitterException {
            String verifyCredentialsAuthorizationHeader = generateVerifyCredentialsAuthorizationHeader();
            Map<String, String> headers = new HashMap<>();
            headers.put("X-Auth-Service-Provider", TWITTER_VERIFY_CREDENTIALS);
            headers.put("X-Verify-Credentials-Authorization", verifyCredentialsAuthorizationHeader);
            HttpResponse httpResponse = new HttpClientWrapper().post(TWEETPHOTO_UPLOAD_URL, ImageUpload.access$000(new HttpParameter[]{new HttpParameter("api_key", this.tweetPhotoAPIKey)}, additionalParams), headers);
            if (httpResponse.getStatusCode() != 201) {
                throw new TwitterException("Twitpic image upload returned invalid status code", httpResponse);
            }
            String response = httpResponse.asString();
            if (-1 != response.indexOf("<Error><ErrorCode>")) {
                throw new TwitterException(new StringBuffer().append("TweetPhoto image upload failed with this error message: ").append(response.substring(response.indexOf("<ErrorCode>") + "<ErrorCode>".length(), response.lastIndexOf("</ErrorCode>"))).toString(), httpResponse);
            } else if (-1 != response.indexOf("<Status>OK</Status>")) {
                return response.substring(response.indexOf("<MediaUrl>") + "<MediaUrl>".length(), response.indexOf("</MediaUrl>"));
            } else {
                throw new TwitterException("Unknown TweetPhoto response", httpResponse);
            }
        }

        private String generateVerifyCredentialsAuthorizationHeader() {
            return new StringBuffer().append("OAuth realm=\"http://api.twitter.com/\",").append(OAuthAuthorization.encodeParameters(this.auth.generateOAuthSignatureHttpParams(HttpGet.METHOD_NAME, TWITTER_VERIFY_CREDENTIALS), ",", true)).toString();
        }
    }

    public static class ImgLyOAuthUploader extends ImageUpload {
        private static final String IMGLY_UPLOAD_URL = "http://img.ly/api/2/upload.json";
        private static final String TWITTER_VERIFY_CREDENTIALS = "https://api.twitter.com/1/account/verify_credentials.json";
        private OAuthAuthorization auth;

        public ImgLyOAuthUploader(OAuthAuthorization auth2) {
            this.auth = auth2;
        }

        public String upload(File image) throws TwitterException {
            return upload(new HttpParameter[]{new HttpParameter("media", image)});
        }

        public String upload(File image, String message) throws TwitterException {
            return upload(new HttpParameter[]{new HttpParameter("media", image), new HttpParameter("message", message)});
        }

        public String upload(String imageFileName, InputStream imageBody) throws TwitterException {
            return upload(new HttpParameter[]{new HttpParameter("media", imageFileName, imageBody)});
        }

        public String upload(String imageFileName, InputStream imageBody, String message) throws TwitterException {
            return upload(new HttpParameter[]{new HttpParameter("media", imageFileName, imageBody), new HttpParameter("message", message)});
        }

        private String upload(HttpParameter[] additionalParams) throws TwitterException {
            String verifyCredentialsAuthorizationHeader = generateVerifyCredentialsAuthorizationHeader();
            Map<String, String> headers = new HashMap<>();
            headers.put("X-Auth-Service-Provider", TWITTER_VERIFY_CREDENTIALS);
            headers.put("X-Verify-Credentials-Authorization", verifyCredentialsAuthorizationHeader);
            HttpResponse httpResponse = new HttpClientWrapper().post(IMGLY_UPLOAD_URL, ImageUpload.access$000(new HttpParameter[0], additionalParams), headers);
            if (httpResponse.getStatusCode() != 200) {
                throw new TwitterException("ImgLy image upload returned invalid status code", httpResponse);
            }
            String response = httpResponse.asString();
            try {
                JSONObject json = new JSONObject(response);
                if (!json.isNull("url")) {
                    return json.getString("url");
                }
                throw new TwitterException("Unknown ImgLy response", httpResponse);
            } catch (JSONException e) {
                throw new TwitterException(new StringBuffer().append("Invalid ImgLy response: ").append(response).toString(), e);
            }
        }

        private String generateVerifyCredentialsAuthorizationHeader() {
            return new StringBuffer().append("OAuth realm=\"http://api.twitter.com/\",").append(OAuthAuthorization.encodeParameters(this.auth.generateOAuthSignatureHttpParams(HttpGet.METHOD_NAME, TWITTER_VERIFY_CREDENTIALS), ",", true)).toString();
        }
    }

    public static class TwitgooOAuthUploader extends ImageUpload {
        private static final String TWITGOO_UPLOAD_URL = "http://twitgoo.com/api/uploadAndPost";
        private static final String TWITTER_VERIFY_CREDENTIALS = "https://api.twitter.com/1/account/verify_credentials.json";
        private OAuthAuthorization auth;

        public TwitgooOAuthUploader(OAuthAuthorization auth2) {
            this.auth = auth2;
        }

        public String upload(File image) throws TwitterException {
            return upload(new HttpParameter[]{new HttpParameter("media", image)});
        }

        public String upload(File image, String message) throws TwitterException {
            return upload(new HttpParameter[]{new HttpParameter("media", image), new HttpParameter("message", message)});
        }

        public String upload(String imageFileName, InputStream imageBody) throws TwitterException {
            return upload(new HttpParameter[]{new HttpParameter("media", imageFileName, imageBody)});
        }

        public String upload(String imageFileName, InputStream imageBody, String message) throws TwitterException {
            return upload(new HttpParameter[]{new HttpParameter("media", imageFileName, imageBody), new HttpParameter("message", message)});
        }

        private String upload(HttpParameter[] additionalParams) throws TwitterException {
            int i;
            int j;
            int j2;
            String verifyCredentialsAuthorizationHeader = generateVerifyCredentialsAuthorizationHeader();
            Map<String, String> headers = new HashMap<>();
            headers.put("X-Auth-Service-Provider", TWITTER_VERIFY_CREDENTIALS);
            headers.put("X-Verify-Credentials-Authorization", verifyCredentialsAuthorizationHeader);
            HttpResponse httpResponse = new HttpClientWrapper().post(TWITGOO_UPLOAD_URL, ImageUpload.access$000(new HttpParameter[]{new HttpParameter("no_twitter_post", "1")}, additionalParams), headers);
            if (httpResponse.getStatusCode() != 200) {
                throw new TwitterException("Twitgoo image upload returned invalid status code", httpResponse);
            }
            String response = httpResponse.asString();
            if (-1 != response.indexOf("<rsp status=\"ok\">")) {
                int i2 = response.indexOf("<mediaurl>");
                if (!(i2 == -1 || (j2 = response.indexOf("</mediaurl>", "<mediaurl>".length() + i2)) == -1)) {
                    return response.substring("<mediaurl>".length() + i2, j2);
                }
            } else if (!(-1 == response.indexOf("<rsp status=\"fail\">") || (i = response.indexOf("msg=\"")) == -1 || (j = response.indexOf("\"", "msg=\"".length() + i)) == -1)) {
                throw new TwitterException(new StringBuffer().append("Invalid Twitgoo response: ").append(response.substring("msg=\"".length() + i, j)).toString());
            }
            throw new TwitterException("Unknown Twitgoo response", httpResponse);
        }

        private String generateVerifyCredentialsAuthorizationHeader() {
            return new StringBuffer().append("OAuth realm=\"http://api.twitter.com/\",").append(OAuthAuthorization.encodeParameters(this.auth.generateOAuthSignatureHttpParams(HttpGet.METHOD_NAME, TWITTER_VERIFY_CREDENTIALS), ",", true)).toString();
        }
    }

    private static class TwippleUploader extends ImageUpload {
        private static final String TWIPPLE_UPLOAD_URL = "http://p.twipple.jp/api/upload";
        private static final String TWITTER_VERIFY_CREDENTIALS = "https://api.twitter.com/1/account/verify_credentials.xml";
        private OAuthAuthorization auth;

        public TwippleUploader(OAuthAuthorization auth2) {
            this.auth = auth2;
        }

        public String upload(File image) throws TwitterException {
            return upload(new HttpParameter[]{new HttpParameter("media", image)});
        }

        public String upload(File image, String message) throws TwitterException {
            return upload(new HttpParameter[]{new HttpParameter("media", image)});
        }

        public String upload(String imageFileName, InputStream imageBody) throws TwitterException {
            return upload(new HttpParameter[]{new HttpParameter("media", imageFileName, imageBody)});
        }

        public String upload(String imageFileName, InputStream imageBody, String message) throws TwitterException {
            return upload(new HttpParameter[]{new HttpParameter("media", imageFileName, imageBody)});
        }

        private String upload(HttpParameter[] additionalParams) throws TwitterException {
            HttpResponse httpResponse = new HttpClientWrapper().post(TWIPPLE_UPLOAD_URL, ImageUpload.access$000(new HttpParameter[]{new HttpParameter("verify_url", generateSignedVerifyCredentialsURL())}, additionalParams));
            if (httpResponse.getStatusCode() != 200) {
                throw new TwitterException("Twipple image upload returned invalid status code", httpResponse);
            }
            String response = httpResponse.asString();
            if (-1 != response.indexOf("<rsp stat=\"fail\">")) {
                throw new TwitterException(new StringBuffer().append("Twipple image upload failed with this error message: ").append(response.substring(response.indexOf("msg") + 5, response.lastIndexOf("\""))).toString(), httpResponse);
            } else if (-1 != response.indexOf("<rsp stat=\"ok\">")) {
                return response.substring(response.indexOf("<mediaurl>") + "<mediaurl>".length(), response.indexOf("</mediaurl>"));
            } else {
                throw new TwitterException("Unknown Twipple response", httpResponse);
            }
        }

        private String generateSignedVerifyCredentialsURL() {
            return new StringBuffer().append("https://api.twitter.com/1/account/verify_credentials.xml?").append(OAuthAuthorization.encodeParameters(this.auth.generateOAuthSignatureHttpParams(HttpGet.METHOD_NAME, TWITTER_VERIFY_CREDENTIALS))).toString();
        }
    }

    private static HttpParameter[] appendHttpParameters(HttpParameter[] src, HttpParameter[] dst) {
        int srcLen = src.length;
        int dstLen = dst.length;
        HttpParameter[] ret = new HttpParameter[(srcLen + dstLen)];
        for (int i = 0; i < srcLen; i++) {
            ret[i] = src[i];
        }
        for (int i2 = 0; i2 < dstLen; i2++) {
            ret[srcLen + i2] = dst[i2];
        }
        return ret;
    }
}
