package twitter4j;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import twitter4j.conf.Configuration;
import twitter4j.http.AccessToken;
import twitter4j.http.Authorization;
import twitter4j.http.NullAuthorization;
import twitter4j.http.OAuthAuthorization;
import twitter4j.http.OAuthSupport;
import twitter4j.http.RequestToken;
import twitter4j.internal.http.HttpClientWrapper;
import twitter4j.internal.http.HttpResponse;
import twitter4j.internal.http.HttpResponseCode;
import twitter4j.internal.http.HttpResponseEvent;
import twitter4j.internal.http.HttpResponseListener;

abstract class TwitterOAuthSupportBase extends TwitterBase implements HttpResponseCode, HttpResponseListener, OAuthSupport, Serializable {
    private static final long serialVersionUID = 6960663978976449394L;
    protected transient HttpClientWrapper http;
    protected RateLimitStatusListener rateLimitStatusListener = null;

    public abstract AccessToken getOAuthAccessToken() throws TwitterException;

    public abstract AccessToken getOAuthAccessToken(String str) throws TwitterException;

    public abstract AccessToken getOAuthAccessToken(String str, String str2) throws TwitterException;

    public abstract AccessToken getOAuthAccessToken(String str, String str2, String str3) throws TwitterException;

    public abstract AccessToken getOAuthAccessToken(RequestToken requestToken) throws TwitterException;

    public abstract AccessToken getOAuthAccessToken(RequestToken requestToken, String str) throws TwitterException;

    public abstract RequestToken getOAuthRequestToken() throws TwitterException;

    public abstract RequestToken getOAuthRequestToken(String str) throws TwitterException;

    public abstract void setOAuthAccessToken(String str, String str2);

    public abstract void setOAuthAccessToken(AccessToken accessToken);

    public abstract void setOAuthConsumer(String str, String str2);

    TwitterOAuthSupportBase(Configuration conf) {
        super(conf);
        init();
    }

    TwitterOAuthSupportBase(Configuration conf, String screenName, String password) {
        super(conf, screenName, password);
        init();
    }

    TwitterOAuthSupportBase(Configuration conf, Authorization auth) {
        super(conf, auth);
        init();
    }

    private void init() {
        if (this.auth instanceof NullAuthorization) {
            String consumerKey = this.conf.getOAuthConsumerKey();
            String consumerSecret = this.conf.getOAuthConsumerSecret();
            if (!(consumerKey == null || consumerSecret == null)) {
                OAuthAuthorization oauth2 = new OAuthAuthorization(this.conf, consumerKey, consumerSecret);
                String accessToken = this.conf.getOAuthAccessToken();
                String accessTokenSecret = this.conf.getOAuthAccessTokenSecret();
                if (!(accessToken == null || accessTokenSecret == null)) {
                    oauth2.setOAuthAccessToken(new AccessToken(accessToken, accessTokenSecret));
                }
                this.auth = oauth2;
            }
        }
        this.http = new HttpClientWrapper(this.conf);
        this.http.setHttpResponseListener(this);
    }

    public void shutdown() {
        if (this.http != null) {
            this.http.shutdown();
        }
    }

    private void writeObject(ObjectOutputStream out) throws IOException {
        if (this.rateLimitStatusListener instanceof Serializable) {
            out.writeObject(this.rateLimitStatusListener);
        } else {
            out.writeObject(null);
        }
    }

    private void readObject(ObjectInputStream stream) throws IOException, ClassNotFoundException {
        this.rateLimitStatusListener = (RateLimitStatusListener) stream.readObject();
        this.http = new HttpClientWrapper(this.conf);
        this.http.setHttpResponseListener(this);
    }

    public boolean isOAuthEnabled() {
        return (this.auth instanceof OAuthAuthorization) && this.auth.isEnabled();
    }

    public void setRateLimitStatusListener(RateLimitStatusListener listener) {
        this.rateLimitStatusListener = listener;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TwitterOAuthSupportBase)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        TwitterOAuthSupportBase that = (TwitterOAuthSupportBase) o;
        return this.rateLimitStatusListener == null ? that.rateLimitStatusListener == null : this.rateLimitStatusListener.equals(that.rateLimitStatusListener);
    }

    public int hashCode() {
        return (super.hashCode() * 31) + (this.rateLimitStatusListener != null ? this.rateLimitStatusListener.hashCode() : 0);
    }

    public void httpResponseReceived(HttpResponseEvent event) {
        RateLimitStatus rateLimitStatus;
        int statusCode;
        if (this.rateLimitStatusListener != null) {
            HttpResponse res = event.getResponse();
            TwitterException te = event.getTwitterException();
            if (te != null) {
                rateLimitStatus = te.getRateLimitStatus();
                statusCode = te.getStatusCode();
            } else {
                rateLimitStatus = RateLimitStatusJSONImpl.createFromResponseHeader(res);
                statusCode = res.getStatusCode();
            }
            if (rateLimitStatus != null) {
                RateLimitStatusEvent statusEvent = new RateLimitStatusEvent(this, rateLimitStatus, event.isAuthenticated());
                if (statusCode == 420 || statusCode == 503) {
                    this.rateLimitStatusListener.onRateLimitStatus(statusEvent);
                    this.rateLimitStatusListener.onRateLimitReached(statusEvent);
                    return;
                }
                this.rateLimitStatusListener.onRateLimitStatus(statusEvent);
            }
        }
    }
}
