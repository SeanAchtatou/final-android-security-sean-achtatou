package twitter4j;

import java.io.Serializable;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationContext;
import twitter4j.http.AccessToken;
import twitter4j.http.Authorization;
import twitter4j.http.AuthorizationFactory;
import twitter4j.http.BasicAuthorization;
import twitter4j.http.OAuthAuthorization;

public final class TwitterFactory implements Serializable {
    private static final long serialVersionUID = 5193900138477709155L;
    private final Configuration conf;

    public TwitterFactory() {
        this(ConfigurationContext.getInstance());
    }

    public TwitterFactory(Configuration conf2) {
        if (conf2 == null) {
            throw new NullPointerException("configuration cannot be null");
        }
        this.conf = conf2;
    }

    public TwitterFactory(String configTreePath) {
        this(ConfigurationContext.getInstance(configTreePath));
    }

    public Twitter getInstance() {
        return getInstance(this.conf, AuthorizationFactory.getInstance(this.conf, true));
    }

    public Twitter getInstance(AccessToken accessToken) {
        String consumerKey = this.conf.getOAuthConsumerKey();
        String consumerSecret = this.conf.getOAuthConsumerSecret();
        if (consumerKey == null && consumerSecret == null) {
            throw new IllegalStateException("Consumer key and Consumer secret not supplied.");
        }
        return getInstance(this.conf, new OAuthAuthorization(this.conf, consumerKey, consumerSecret, accessToken));
    }

    public Twitter getInstance(String screenName, String password) {
        return getInstance(this.conf, new BasicAuthorization(screenName, password));
    }

    public Twitter getInstance(Authorization auth) {
        return getInstance(this.conf, auth);
    }

    public Twitter getOAuthAuthorizedInstance(String consumerKey, String consumerSecret) {
        if (consumerKey == null && consumerSecret == null) {
            throw new IllegalStateException("Consumer key and Consumer secret not supplied.");
        }
        return getInstance(this.conf, new OAuthAuthorization(this.conf, consumerKey, consumerSecret));
    }

    public Twitter getOAuthAuthorizedInstance(String consumerKey, String consumerSecret, AccessToken accessToken) {
        if (consumerKey == null && consumerSecret == null) {
            throw new IllegalStateException("Consumer key and Consumer secret not supplied.");
        }
        OAuthAuthorization oauth2 = new OAuthAuthorization(this.conf, consumerKey, consumerSecret);
        oauth2.setOAuthAccessToken(accessToken);
        return getInstance(this.conf, oauth2);
    }

    public Twitter getOAuthAuthorizedInstance(AccessToken accessToken) {
        return getInstance(accessToken);
    }

    private Twitter getInstance(Configuration conf2, Authorization auth) {
        return new Twitter(conf2, auth);
    }
}
