package twitter4j.conf;

import java.io.File;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import twitter4j.internal.util.StringUtil;

public final class PropertyConfiguration extends ConfigurationBase implements Serializable {
    public static final String ASYNC_DISPATCHER_IMPL = "async.dispatherImpl";
    public static final String ASYNC_NUM_THREADS = "async.numThreads";
    public static final String CLIENT_URL = "clientURL";
    public static final String CLIENT_VERSION = "clientVersion";
    public static final String DEBUG = "debug";
    public static final String HTTP_CONNECTION_TIMEOUT = "http.connectionTimeout";
    public static final String HTTP_DEFAULT_MAX_PER_ROUTE = "http.defaultMaxPerRoute";
    public static final String HTTP_MAX_TOTAL_CONNECTIONS = "http.maxTotalConnections";
    public static final String HTTP_PRETTY_DEBUG = "http.prettyDebug";
    public static final String HTTP_PROXY_HOST = "http.proxyHost";
    public static final String HTTP_PROXY_HOST_FALLBACK = "http.proxyHost";
    public static final String HTTP_PROXY_PASSWORD = "http.proxyPassword";
    public static final String HTTP_PROXY_PORT = "http.proxyPort";
    public static final String HTTP_PROXY_PORT_FALLBACK = "http.proxyPort";
    public static final String HTTP_PROXY_USER = "http.proxyUser";
    public static final String HTTP_READ_TIMEOUT = "http.readTimeout";
    public static final String HTTP_RETRY_COUNT = "http.retryCount";
    public static final String HTTP_RETRY_INTERVAL_SECS = "http.retryIntervalSecs";
    public static final String HTTP_STREAMING_READ_TIMEOUT = "http.streamingReadTimeout";
    public static final String HTTP_USER_AGENT = "http.userAgent";
    public static final String HTTP_USE_SSL = "http.useSSL";
    public static final String INCLUDE_ENTITIES = "includeEntities";
    public static final String INCLUDE_RTS = "includeRTs";
    public static final String MEDIA_PROVIDER = "media.provier";
    public static final String MEDIA_PROVIDER_API_KEY = "media.provierAPIKey";
    public static final String OAUTH_ACCESS_TOKEN = "oauth.accessToken";
    public static final String OAUTH_ACCESS_TOKEN_SECRET = "oauth.accessTokenSecret";
    public static final String OAUTH_ACCESS_TOKEN_URL = "oauth.accessTokenURL";
    public static final String OAUTH_AUTHENTICATION_URL = "oauth.authenticationURL";
    public static final String OAUTH_AUTHORIZATION_URL = "oauth.authorizationURL";
    public static final String OAUTH_CONSUMER_KEY = "oauth.consumerKey";
    public static final String OAUTH_CONSUMER_SECRET = "oauth.consumerSecret";
    public static final String OAUTH_REQUEST_TOKEN_URL = "oauth.requestTokenURL";
    public static final String PASSWORD = "password";
    public static final String REST_BASE_URL = "restBaseURL";
    public static final String SEARCH_BASE_URL = "searchBaseURL";
    public static final String SITE_STREAM_BASE_URL = "siteStreamBaseURL";
    public static final String SOURCE = "source";
    public static final String STREAM_BASE_URL = "streamBaseURL";
    public static final String STREAM_USER_REPLIES_ALL = "stream.user.repliesAll";
    public static final String USER = "user";
    public static final String USER_STREAM_BASE_URL = "userStreamBaseURL";
    static Class class$twitter4j$conf$Configuration = null;
    private static final long serialVersionUID = 6458764415636588373L;

    public String getDispatcherImpl() {
        return super.getDispatcherImpl();
    }

    public int getHttpStreamingReadTimeout() {
        return super.getHttpStreamingReadTimeout();
    }

    public String getMediaProvider() {
        return super.getMediaProvider();
    }

    public String getMediaProviderAPIKey() {
        return super.getMediaProviderAPIKey();
    }

    public String getOAuthAccessToken() {
        return super.getOAuthAccessToken();
    }

    public String getOAuthAccessTokenSecret() {
        return super.getOAuthAccessTokenSecret();
    }

    public String getOAuthAccessTokenURL() {
        return super.getOAuthAccessTokenURL();
    }

    public String getOAuthAuthenticationURL() {
        return super.getOAuthAuthenticationURL();
    }

    public String getOAuthAuthorizationURL() {
        return super.getOAuthAuthorizationURL();
    }

    public String getOAuthRequestTokenURL() {
        return super.getOAuthRequestTokenURL();
    }

    public Map getRequestHeaders() {
        return super.getRequestHeaders();
    }

    public String getRestBaseURL() {
        return super.getRestBaseURL();
    }

    public String getSearchBaseURL() {
        return super.getSearchBaseURL();
    }

    public String getSiteStreamBaseURL() {
        return super.getSiteStreamBaseURL();
    }

    public String getStreamBaseURL() {
        return super.getStreamBaseURL();
    }

    public String getUserStreamBaseURL() {
        return super.getUserStreamBaseURL();
    }

    public int hashCode() {
        return super.hashCode();
    }

    public boolean isIncludeEntitiesEnabled() {
        return super.isIncludeEntitiesEnabled();
    }

    public boolean isIncludeRTsEnabled() {
        return super.isIncludeRTsEnabled();
    }

    public boolean isPrettyDebugEnabled() {
        return super.isPrettyDebugEnabled();
    }

    public boolean isUserStreamRepliesAllEnabled() {
        return super.isUserStreamRepliesAllEnabled();
    }

    public String toString() {
        return super.toString();
    }

    public PropertyConfiguration(InputStream is) {
        Properties props = new Properties();
        loadProperties(props, is);
        setFieldsWithTreePath(props, "/");
    }

    public PropertyConfiguration(Properties props) {
        this(props, "/");
    }

    public PropertyConfiguration(Properties props, String treePath) {
        setFieldsWithTreePath(props, treePath);
    }

    PropertyConfiguration(String treePath) {
        Properties props;
        Class cls;
        Class cls2;
        try {
            props = (Properties) System.getProperties().clone();
            normalize(props);
        } catch (SecurityException e) {
            props = new Properties();
        }
        loadProperties(props, new StringBuffer().append(".").append(File.separatorChar).append("twitter4j.properties").toString());
        if (class$twitter4j$conf$Configuration == null) {
            cls = class$("twitter4j.conf.Configuration");
            class$twitter4j$conf$Configuration = cls;
        } else {
            cls = class$twitter4j$conf$Configuration;
        }
        loadProperties(props, cls.getResourceAsStream("/twitter4j.properties"));
        if (class$twitter4j$conf$Configuration == null) {
            cls2 = class$("twitter4j.conf.Configuration");
            class$twitter4j$conf$Configuration = cls2;
        } else {
            cls2 = class$twitter4j$conf$Configuration;
        }
        loadProperties(props, cls2.getResourceAsStream("/WEB-INF/twitter4j.properties"));
        setFieldsWithTreePath(props, treePath);
    }

    static Class class$(String x0) {
        try {
            return Class.forName(x0);
        } catch (ClassNotFoundException x1) {
            throw new NoClassDefFoundError().initCause(x1);
        }
    }

    PropertyConfiguration() {
        this("/");
    }

    private boolean notNull(Properties props, String prefix, String name) {
        return props.getProperty(new StringBuffer().append(prefix).append(name).toString()) != null;
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x0034 A[SYNTHETIC, Splitter:B:20:0x0034] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x003d A[SYNTHETIC, Splitter:B:25:0x003d] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean loadProperties(java.util.Properties r6, java.lang.String r7) {
        /*
            r5 = this;
            r1 = 0
            java.io.File r0 = new java.io.File     // Catch:{ Exception -> 0x0031, all -> 0x003a }
            r0.<init>(r7)     // Catch:{ Exception -> 0x0031, all -> 0x003a }
            boolean r3 = r0.exists()     // Catch:{ Exception -> 0x0031, all -> 0x003a }
            if (r3 == 0) goto L_0x002a
            boolean r3 = r0.isFile()     // Catch:{ Exception -> 0x0031, all -> 0x003a }
            if (r3 == 0) goto L_0x002a
            java.io.FileInputStream r2 = new java.io.FileInputStream     // Catch:{ Exception -> 0x0031, all -> 0x003a }
            r2.<init>(r0)     // Catch:{ Exception -> 0x0031, all -> 0x003a }
            java.io.FileInputStream r3 = new java.io.FileInputStream     // Catch:{ Exception -> 0x004a, all -> 0x0047 }
            r3.<init>(r0)     // Catch:{ Exception -> 0x004a, all -> 0x0047 }
            r6.load(r3)     // Catch:{ Exception -> 0x004a, all -> 0x0047 }
            r5.normalize(r6)     // Catch:{ Exception -> 0x004a, all -> 0x0047 }
            r3 = 1
            if (r2 == 0) goto L_0x0028
            r2.close()     // Catch:{ IOException -> 0x0041 }
        L_0x0028:
            r1 = r2
        L_0x0029:
            return r3
        L_0x002a:
            if (r1 == 0) goto L_0x002f
            r1.close()     // Catch:{ IOException -> 0x0043 }
        L_0x002f:
            r3 = 0
            goto L_0x0029
        L_0x0031:
            r3 = move-exception
        L_0x0032:
            if (r1 == 0) goto L_0x002f
            r1.close()     // Catch:{ IOException -> 0x0038 }
            goto L_0x002f
        L_0x0038:
            r3 = move-exception
            goto L_0x002f
        L_0x003a:
            r3 = move-exception
        L_0x003b:
            if (r1 == 0) goto L_0x0040
            r1.close()     // Catch:{ IOException -> 0x0045 }
        L_0x0040:
            throw r3
        L_0x0041:
            r4 = move-exception
            goto L_0x0028
        L_0x0043:
            r3 = move-exception
            goto L_0x002f
        L_0x0045:
            r4 = move-exception
            goto L_0x0040
        L_0x0047:
            r3 = move-exception
            r1 = r2
            goto L_0x003b
        L_0x004a:
            r3 = move-exception
            r1 = r2
            goto L_0x0032
        */
        throw new UnsupportedOperationException("Method not decompiled: twitter4j.conf.PropertyConfiguration.loadProperties(java.util.Properties, java.lang.String):boolean");
    }

    private boolean loadProperties(Properties props, InputStream is) {
        try {
            props.load(is);
            normalize(props);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private void normalize(Properties props) {
        Set<String> keys = props.keySet();
        ArrayList<String> toBeNormalized = new ArrayList<>(10);
        for (String keyStr : keys) {
            if (-1 != keyStr.indexOf("twitter4j.")) {
                toBeNormalized.add(keyStr);
            }
        }
        Iterator i$ = toBeNormalized.iterator();
        while (i$.hasNext()) {
            String keyStr2 = (String) i$.next();
            String property = props.getProperty(keyStr2);
            int index = keyStr2.indexOf("twitter4j.");
            props.setProperty(new StringBuffer().append(keyStr2.substring(0, index)).append(keyStr2.substring(index + 10)).toString(), property);
        }
    }

    private void setFieldsWithTreePath(Properties props, String treePath) {
        setFieldsWithPrefix(props, "");
        String prefix = null;
        for (String split : StringUtil.split(treePath, "/")) {
            if (!"".equals(split)) {
                if (prefix == null) {
                    prefix = new StringBuffer().append(split).append(".").toString();
                } else {
                    prefix = new StringBuffer().append(prefix).append(split).append(".").toString();
                }
                setFieldsWithPrefix(props, prefix);
            }
        }
    }

    private void setFieldsWithPrefix(Properties props, String prefix) {
        if (notNull(props, prefix, DEBUG)) {
            setDebug(getBoolean(props, prefix, DEBUG));
        }
        if (notNull(props, prefix, SOURCE)) {
            setSource(getString(props, prefix, SOURCE));
        }
        if (notNull(props, prefix, USER)) {
            setUser(getString(props, prefix, USER));
        }
        if (notNull(props, prefix, PASSWORD)) {
            setPassword(getString(props, prefix, PASSWORD));
        }
        if (notNull(props, prefix, HTTP_USE_SSL)) {
            setUseSSL(getBoolean(props, prefix, HTTP_USE_SSL));
        } else if (notNull(props, prefix, USER) && notNull(props, prefix, PASSWORD)) {
            setUseSSL(true);
        }
        if (notNull(props, prefix, HTTP_PRETTY_DEBUG)) {
            setPrettyDebugEnabled(getBoolean(props, prefix, HTTP_PRETTY_DEBUG));
        }
        if (notNull(props, prefix, "http.proxyHost")) {
            setHttpProxyHost(getString(props, prefix, "http.proxyHost"));
        } else if (notNull(props, prefix, "http.proxyHost")) {
            setHttpProxyHost(getString(props, prefix, "http.proxyHost"));
        }
        if (notNull(props, prefix, HTTP_PROXY_USER)) {
            setHttpProxyUser(getString(props, prefix, HTTP_PROXY_USER));
        }
        if (notNull(props, prefix, HTTP_PROXY_PASSWORD)) {
            setHttpProxyPassword(getString(props, prefix, HTTP_PROXY_PASSWORD));
        }
        if (notNull(props, prefix, "http.proxyPort")) {
            setHttpProxyPort(getIntProperty(props, prefix, "http.proxyPort"));
        } else if (notNull(props, prefix, "http.proxyPort")) {
            setHttpProxyPort(getIntProperty(props, prefix, "http.proxyPort"));
        }
        if (notNull(props, prefix, HTTP_CONNECTION_TIMEOUT)) {
            setHttpConnectionTimeout(getIntProperty(props, prefix, HTTP_CONNECTION_TIMEOUT));
        }
        if (notNull(props, prefix, HTTP_READ_TIMEOUT)) {
            setHttpReadTimeout(getIntProperty(props, prefix, HTTP_READ_TIMEOUT));
        }
        if (notNull(props, prefix, HTTP_STREAMING_READ_TIMEOUT)) {
            setHttpStreamingReadTimeout(getIntProperty(props, prefix, HTTP_STREAMING_READ_TIMEOUT));
        }
        if (notNull(props, prefix, HTTP_RETRY_COUNT)) {
            setHttpRetryCount(getIntProperty(props, prefix, HTTP_RETRY_COUNT));
        }
        if (notNull(props, prefix, HTTP_RETRY_INTERVAL_SECS)) {
            setHttpRetryIntervalSeconds(getIntProperty(props, prefix, HTTP_RETRY_INTERVAL_SECS));
        }
        if (notNull(props, prefix, HTTP_MAX_TOTAL_CONNECTIONS)) {
            setHttpMaxTotalConnections(getIntProperty(props, prefix, HTTP_MAX_TOTAL_CONNECTIONS));
        }
        if (notNull(props, prefix, HTTP_DEFAULT_MAX_PER_ROUTE)) {
            setHttpDefaultMaxPerRoute(getIntProperty(props, prefix, HTTP_DEFAULT_MAX_PER_ROUTE));
        }
        if (notNull(props, prefix, OAUTH_CONSUMER_KEY)) {
            setOAuthConsumerKey(getString(props, prefix, OAUTH_CONSUMER_KEY));
        }
        if (notNull(props, prefix, OAUTH_CONSUMER_SECRET)) {
            setOAuthConsumerSecret(getString(props, prefix, OAUTH_CONSUMER_SECRET));
        }
        if (notNull(props, prefix, OAUTH_ACCESS_TOKEN)) {
            setOAuthAccessToken(getString(props, prefix, OAUTH_ACCESS_TOKEN));
        }
        if (notNull(props, prefix, OAUTH_ACCESS_TOKEN_SECRET)) {
            setOAuthAccessTokenSecret(getString(props, prefix, OAUTH_ACCESS_TOKEN_SECRET));
        }
        if (notNull(props, prefix, ASYNC_NUM_THREADS)) {
            setAsyncNumThreads(getIntProperty(props, prefix, ASYNC_NUM_THREADS));
        }
        if (notNull(props, prefix, ASYNC_DISPATCHER_IMPL)) {
            setDispatcherImpl(getString(props, prefix, ASYNC_DISPATCHER_IMPL));
        }
        if (notNull(props, prefix, CLIENT_VERSION)) {
            setClientVersion(getString(props, prefix, CLIENT_VERSION));
        }
        if (notNull(props, prefix, CLIENT_URL)) {
            setClientURL(getString(props, prefix, CLIENT_URL));
        }
        if (notNull(props, prefix, HTTP_USER_AGENT)) {
            setUserAgent(getString(props, prefix, HTTP_USER_AGENT));
        }
        if (notNull(props, prefix, OAUTH_REQUEST_TOKEN_URL)) {
            setOAuthRequestTokenURL(getString(props, prefix, OAUTH_REQUEST_TOKEN_URL));
        }
        if (notNull(props, prefix, OAUTH_AUTHORIZATION_URL)) {
            setOAuthAuthorizationURL(getString(props, prefix, OAUTH_AUTHORIZATION_URL));
        }
        if (notNull(props, prefix, OAUTH_ACCESS_TOKEN_URL)) {
            setOAuthAccessTokenURL(getString(props, prefix, OAUTH_ACCESS_TOKEN_URL));
        }
        if (notNull(props, prefix, OAUTH_AUTHENTICATION_URL)) {
            setOAuthAuthenticationURL(getString(props, prefix, OAUTH_AUTHENTICATION_URL));
        }
        if (notNull(props, prefix, REST_BASE_URL)) {
            setRestBaseURL(getString(props, prefix, REST_BASE_URL));
        }
        if (notNull(props, prefix, SEARCH_BASE_URL)) {
            setSearchBaseURL(getString(props, prefix, SEARCH_BASE_URL));
        }
        if (notNull(props, prefix, STREAM_BASE_URL)) {
            setStreamBaseURL(getString(props, prefix, STREAM_BASE_URL));
        }
        if (notNull(props, prefix, USER_STREAM_BASE_URL)) {
            setUserStreamBaseURL(getString(props, prefix, USER_STREAM_BASE_URL));
        }
        if (notNull(props, prefix, SITE_STREAM_BASE_URL)) {
            setSiteStreamBaseURL(getString(props, prefix, SITE_STREAM_BASE_URL));
        }
        if (notNull(props, prefix, INCLUDE_RTS)) {
            setIncludeRTsEnbled(getBoolean(props, prefix, INCLUDE_RTS));
        }
        if (notNull(props, prefix, INCLUDE_ENTITIES)) {
            setIncludeEntitiesEnbled(getBoolean(props, prefix, INCLUDE_ENTITIES));
        }
        if (notNull(props, prefix, STREAM_USER_REPLIES_ALL)) {
            setUserStreamRepliesAllEnabled(getBoolean(props, prefix, STREAM_USER_REPLIES_ALL));
        }
        if (notNull(props, prefix, MEDIA_PROVIDER)) {
            setMediaProvider(getString(props, prefix, MEDIA_PROVIDER));
        }
        if (notNull(props, prefix, MEDIA_PROVIDER_API_KEY)) {
            setMediaProviderAPIKey(getString(props, prefix, MEDIA_PROVIDER_API_KEY));
        }
    }

    /* access modifiers changed from: protected */
    public boolean getBoolean(Properties props, String prefix, String name) {
        return Boolean.valueOf(props.getProperty(new StringBuffer().append(prefix).append(name).toString())).booleanValue();
    }

    /* access modifiers changed from: protected */
    public int getIntProperty(Properties props, String prefix, String name) {
        try {
            return Integer.parseInt(props.getProperty(new StringBuffer().append(prefix).append(name).toString()));
        } catch (NumberFormatException e) {
            return -1;
        }
    }

    /* access modifiers changed from: protected */
    public String getString(Properties props, String prefix, String name) {
        return props.getProperty(new StringBuffer().append(prefix).append(name).toString());
    }
}
