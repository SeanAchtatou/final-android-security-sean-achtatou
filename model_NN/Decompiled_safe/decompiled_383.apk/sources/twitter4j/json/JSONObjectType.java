package twitter4j.json;

import java.io.Serializable;
import org.apache.http.client.methods.HttpDelete;
import twitter4j.internal.logging.Logger;
import twitter4j.internal.org.json.JSONException;
import twitter4j.internal.org.json.JSONObject;

public final class JSONObjectType implements Serializable {
    public static final JSONObjectType BLOCK = new JSONObjectType("BLOCK");
    public static final JSONObjectType DELETE = new JSONObjectType(HttpDelete.METHOD_NAME);
    public static final JSONObjectType DIRECT_MESSAGE = new JSONObjectType("DIRECT_MESSAGE");
    public static final JSONObjectType FAVORITE = new JSONObjectType("FAVORITE");
    public static final JSONObjectType FOLLOW = new JSONObjectType("FOLLOW");
    public static final JSONObjectType FRIENDS = new JSONObjectType("FRIENDS");
    public static final JSONObjectType LIMIT = new JSONObjectType("LIMIT");
    public static final JSONObjectType RETWEET = new JSONObjectType("RETWEET");
    public static final JSONObjectType SCRUB_GEO = new JSONObjectType("SCRUB_GEO");
    public static final JSONObjectType SENDER = new JSONObjectType("SENDER");
    public static final JSONObjectType STATUS = new JSONObjectType("STATUS");
    public static final JSONObjectType UNBLOCK = new JSONObjectType("UNBLOCK");
    public static final JSONObjectType UNFAVORITE = new JSONObjectType("UNFAVORITE");
    public static final JSONObjectType UNFOLLOW = new JSONObjectType("UNFOLLOW");
    public static final JSONObjectType USER_LIST_CREATED = new JSONObjectType("USER_LIST_CREATED");
    public static final JSONObjectType USER_LIST_DESTROYED = new JSONObjectType("USER_LIST_DESTROYED");
    public static final JSONObjectType USER_LIST_MEMBER_ADDED = new JSONObjectType("USER_LIST_MEMBER_ADDED");
    public static final JSONObjectType USER_LIST_MEMBER_DELETED = new JSONObjectType("USER_LIST_MEMBER_DELETED");
    public static final JSONObjectType USER_LIST_SUBSCRIBED = new JSONObjectType("USER_LIST_SUBSCRIBED");
    public static final JSONObjectType USER_LIST_UNSUBSCRIBED = new JSONObjectType("USER_LIST_UNSUBSCRIBED");
    public static final JSONObjectType USER_LIST_UPDATED = new JSONObjectType("USER_LIST_UPDATED");
    public static final JSONObjectType USER_UPDATE = new JSONObjectType("USER_UPDATE");
    static Class class$twitter4j$json$JSONObjectType = null;
    private static final Logger logger;
    private static final long serialVersionUID = -4487565183481849892L;
    private final String name;

    static {
        Class cls;
        if (class$twitter4j$json$JSONObjectType == null) {
            cls = class$("twitter4j.json.JSONObjectType");
            class$twitter4j$json$JSONObjectType = cls;
        } else {
            cls = class$twitter4j$json$JSONObjectType;
        }
        logger = Logger.getLogger(cls);
    }

    static Class class$(String x0) {
        try {
            return Class.forName(x0);
        } catch (ClassNotFoundException x1) {
            throw new NoClassDefFoundError().initCause(x1);
        }
    }

    private JSONObjectType() {
        throw new AssertionError();
    }

    private JSONObjectType(String name2) {
        this.name = name2;
    }

    public String getName() {
        return this.name;
    }

    public static JSONObjectType determine(JSONObject json) {
        if (!json.isNull("sender")) {
            return SENDER;
        }
        if (!json.isNull("text")) {
            return STATUS;
        }
        if (!json.isNull("direct_message")) {
            return DIRECT_MESSAGE;
        }
        if (!json.isNull("delete")) {
            return DELETE;
        }
        if (!json.isNull("limit")) {
            return LIMIT;
        }
        if (!json.isNull("scrub_geo")) {
            return SCRUB_GEO;
        }
        if (!json.isNull("friends")) {
            return FRIENDS;
        }
        if (!json.isNull("event")) {
            try {
                String event = json.getString("event");
                if ("favorite".equals(event)) {
                    return FAVORITE;
                }
                if ("unfavorite".equals(event)) {
                    return UNFAVORITE;
                }
                if ("retweet".equals(event)) {
                    return RETWEET;
                }
                if ("follow".equals(event)) {
                    return FOLLOW;
                }
                if ("unfollow".equals(event)) {
                    return UNFOLLOW;
                }
                if (event.startsWith("list")) {
                    if ("list_member_added".equals(event)) {
                        return USER_LIST_MEMBER_ADDED;
                    }
                    if ("list_member_removed".equals(event)) {
                        return USER_LIST_MEMBER_DELETED;
                    }
                    if ("list_user_subscribed".equals(event)) {
                        return USER_LIST_SUBSCRIBED;
                    }
                    if ("list_user_unsubscribed".equals(event)) {
                        return USER_LIST_UNSUBSCRIBED;
                    }
                    if ("list_created".equals(event)) {
                        return USER_LIST_CREATED;
                    }
                    if ("list_updated".equals(event)) {
                        return USER_LIST_UPDATED;
                    }
                    if ("list_destroyed".equals(event)) {
                        return USER_LIST_DESTROYED;
                    }
                } else if ("user_update".equals(event)) {
                    return USER_UPDATE;
                } else {
                    if ("block".equals(event)) {
                        return BLOCK;
                    }
                    if ("unblock".equals(event)) {
                        return UNBLOCK;
                    }
                }
            } catch (JSONException e) {
                try {
                    logger.warn("Failed to get event element: ", json.toString(2));
                } catch (JSONException e2) {
                }
            }
        }
        return null;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        JSONObjectType that = (JSONObjectType) o;
        return this.name == null ? that.name == null : this.name.equals(that.name);
    }

    public int hashCode() {
        if (this.name != null) {
            return this.name.hashCode();
        }
        return 0;
    }

    public String toString() {
        return this.name;
    }
}
