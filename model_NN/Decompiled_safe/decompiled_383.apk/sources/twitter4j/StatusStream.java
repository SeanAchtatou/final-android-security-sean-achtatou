package twitter4j;

import java.io.IOException;

public interface StatusStream extends StreamImplementation {
    void close() throws IOException;

    void next(StatusListener statusListener) throws TwitterException;
}
