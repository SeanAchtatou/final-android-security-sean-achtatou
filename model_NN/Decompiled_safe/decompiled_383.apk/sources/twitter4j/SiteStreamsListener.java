package twitter4j;

public interface SiteStreamsListener extends StreamListener {
    void onBlock(int i, User user, User user2);

    void onDeletionNotice(int i, int i2, int i3);

    void onDeletionNotice(int i, StatusDeletionNotice statusDeletionNotice);

    void onDirectMessage(int i, DirectMessage directMessage);

    void onException(Exception exc);

    void onFavorite(int i, User user, User user2, Status status);

    void onFollow(int i, User user, User user2);

    void onFriendList(int i, int[] iArr);

    void onStatus(int i, Status status);

    void onUnblock(int i, User user, User user2);

    void onUnfavorite(int i, User user, User user2, Status status);

    void onUnfollow(int i, User user, User user2);

    void onUserListCreation(int i, User user, UserList userList);

    void onUserListDeletion(int i, User user, UserList userList);

    void onUserListMemberAddition(int i, User user, User user2, UserList userList);

    void onUserListMemberDeletion(int i, User user, User user2, UserList userList);

    void onUserListSubscription(int i, User user, User user2, UserList userList);

    void onUserListUnsubscription(int i, User user, User user2, UserList userList);

    void onUserListUpdate(int i, User user, UserList userList);

    void onUserProfileUpdate(int i, User user);
}
