package twitter4j;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import oauth.signpost.OAuth;
import twitter4j.conf.Configuration;
import twitter4j.conf.PropertyConfiguration;
import twitter4j.internal.async.Dispatcher;
import twitter4j.internal.http.HttpResponse;
import twitter4j.internal.json.DataObjectFactoryUtil;
import twitter4j.internal.logging.Logger;
import twitter4j.internal.org.json.JSONArray;
import twitter4j.internal.org.json.JSONException;
import twitter4j.internal.org.json.JSONObject;
import twitter4j.json.JSONObjectType;

abstract class AbstractStreamImplementation {
    static Class class$twitter4j$StatusStreamImpl;
    protected static final Logger logger;
    private final Configuration CONF;
    private BufferedReader br;
    protected final Dispatcher dispatcher;
    private InputStream is;
    private HttpResponse response;
    private boolean streamAlive;

    /* access modifiers changed from: package-private */
    public abstract void next(StreamListener[] streamListenerArr) throws TwitterException;

    static Configuration access$000(AbstractStreamImplementation x0) {
        return x0.CONF;
    }

    static {
        Class cls;
        if (class$twitter4j$StatusStreamImpl == null) {
            cls = class$("twitter4j.StatusStreamImpl");
            class$twitter4j$StatusStreamImpl = cls;
        } else {
            cls = class$twitter4j$StatusStreamImpl;
        }
        logger = Logger.getLogger(cls);
    }

    static Class class$(String x0) {
        try {
            return Class.forName(x0);
        } catch (ClassNotFoundException x1) {
            throw new NoClassDefFoundError().initCause(x1);
        }
    }

    AbstractStreamImplementation(Dispatcher dispatcher2, InputStream stream, Configuration conf) throws IOException {
        this.streamAlive = true;
        this.is = stream;
        this.br = new BufferedReader(new InputStreamReader(stream, OAuth.ENCODING));
        this.dispatcher = dispatcher2;
        this.CONF = conf;
    }

    AbstractStreamImplementation(Dispatcher dispatcher2, HttpResponse response2, Configuration conf) throws IOException {
        this(dispatcher2, response2.asStream(), conf);
        this.response = response2;
    }

    /* access modifiers changed from: protected */
    public String parseLine(String line) {
        return line;
    }

    abstract class StreamEvent implements Runnable {
        String line;
        private final AbstractStreamImplementation this$0;

        StreamEvent(AbstractStreamImplementation abstractStreamImplementation, String line2) {
            this.this$0 = abstractStreamImplementation;
            this.line = line2;
        }
    }

    /* access modifiers changed from: protected */
    public void handleNextElement() throws TwitterException {
        if (!this.streamAlive) {
            throw new IllegalStateException("Stream already closed.");
        }
        try {
            String line = this.br.readLine();
            if (line == null) {
                throw new IOException("the end of the stream has been reached");
            }
            this.dispatcher.invokeLater(new StreamEvent(this, line) {
                private final AbstractStreamImplementation this$0;

                {
                    this.this$0 = r1;
                }

                public void run() {
                    this.line = this.this$0.parseLine(this.line);
                    if (this.line.length() > 0) {
                        try {
                            JSONObject json = new JSONObject(this.line);
                            JSONObjectType jsonObjectType = JSONObjectType.determine(json);
                            if (AbstractStreamImplementation.logger.isDebugEnabled()) {
                                AbstractStreamImplementation.logger.debug("Received:", AbstractStreamImplementation.access$000(this.this$0).isPrettyDebugEnabled() ? json.toString(1) : json.toString());
                            }
                            if (JSONObjectType.SENDER == jsonObjectType) {
                                this.this$0.onSender(json);
                            } else if (JSONObjectType.STATUS == jsonObjectType) {
                                this.this$0.onStatus(json);
                            } else if (JSONObjectType.DIRECT_MESSAGE == jsonObjectType) {
                                this.this$0.onDirectMessage(json);
                            } else if (JSONObjectType.DELETE == jsonObjectType) {
                                this.this$0.onDelete(json);
                            } else if (JSONObjectType.LIMIT == jsonObjectType) {
                                this.this$0.onLimit(json);
                            } else if (JSONObjectType.SCRUB_GEO == jsonObjectType) {
                                this.this$0.onScrubGeo(json);
                            } else if (JSONObjectType.FRIENDS == jsonObjectType) {
                                this.this$0.onFriends(json);
                            } else if (JSONObjectType.FAVORITE == jsonObjectType) {
                                this.this$0.onFavorite(json.getJSONObject(PropertyConfiguration.SOURCE), json.getJSONObject("target"), json.getJSONObject("target_object"));
                            } else if (JSONObjectType.UNFAVORITE == jsonObjectType) {
                                this.this$0.onUnfavorite(json.getJSONObject(PropertyConfiguration.SOURCE), json.getJSONObject("target"), json.getJSONObject("target_object"));
                            } else if (JSONObjectType.RETWEET == jsonObjectType) {
                                this.this$0.onRetweet(json.getJSONObject(PropertyConfiguration.SOURCE), json.getJSONObject("target"), json.getJSONObject("target_object"));
                            } else if (JSONObjectType.FOLLOW == jsonObjectType) {
                                this.this$0.onFollow(json.getJSONObject(PropertyConfiguration.SOURCE), json.getJSONObject("target"));
                            } else if (JSONObjectType.UNFOLLOW == jsonObjectType) {
                                this.this$0.onUnfollow(json.getJSONObject(PropertyConfiguration.SOURCE), json.getJSONObject("target"));
                            } else if (JSONObjectType.USER_LIST_MEMBER_ADDED == jsonObjectType) {
                                this.this$0.onUserListMemberAddition(json.getJSONObject("target"), json.getJSONObject(PropertyConfiguration.SOURCE), json.getJSONObject("target_object"));
                            } else if (JSONObjectType.USER_LIST_MEMBER_DELETED == jsonObjectType) {
                                this.this$0.onUserListMemberDeletion(json.getJSONObject("target"), json.getJSONObject(PropertyConfiguration.SOURCE), json.getJSONObject("target_object"));
                            } else if (JSONObjectType.USER_LIST_SUBSCRIBED == jsonObjectType) {
                                this.this$0.onUserListSubscription(json.getJSONObject(PropertyConfiguration.SOURCE), json.getJSONObject("target"), json.getJSONObject("target_object"));
                            } else if (JSONObjectType.USER_LIST_UNSUBSCRIBED == jsonObjectType) {
                                this.this$0.onUserListUnsubscription(json.getJSONObject(PropertyConfiguration.SOURCE), json.getJSONObject("target"), json.getJSONObject("target_object"));
                            } else if (JSONObjectType.USER_LIST_CREATED == jsonObjectType) {
                                this.this$0.onUserListCreation(json.getJSONObject(PropertyConfiguration.SOURCE), json.getJSONObject("target"));
                            } else if (JSONObjectType.USER_LIST_UPDATED == jsonObjectType) {
                                this.this$0.onUserListUpdated(json.getJSONObject(PropertyConfiguration.SOURCE), json.getJSONObject("target"));
                            } else if (JSONObjectType.USER_LIST_DESTROYED == jsonObjectType) {
                                this.this$0.onUserListDestroyed(json.getJSONObject(PropertyConfiguration.SOURCE), json.getJSONObject("target"));
                            } else if (JSONObjectType.USER_UPDATE == jsonObjectType) {
                                this.this$0.onUserUpdate(json.getJSONObject(PropertyConfiguration.SOURCE), json.getJSONObject("target"));
                            } else if (JSONObjectType.BLOCK == jsonObjectType) {
                                this.this$0.onBlock(json.getJSONObject(PropertyConfiguration.SOURCE), json.getJSONObject("target"));
                            } else if (JSONObjectType.UNBLOCK == jsonObjectType) {
                                this.this$0.onUnblock(json.getJSONObject(PropertyConfiguration.SOURCE), json.getJSONObject("target"));
                            } else {
                                AbstractStreamImplementation.logger.warn("Received unknown event:", AbstractStreamImplementation.access$000(this.this$0).isPrettyDebugEnabled() ? json.toString(1) : json.toString());
                            }
                        } catch (Exception e) {
                            this.this$0.onException(e);
                        }
                    }
                }
            });
        } catch (IOException e) {
            IOException ioe = e;
            try {
                this.is.close();
            } catch (IOException e2) {
            }
            boolean isUnexpectedException = this.streamAlive;
            this.streamAlive = false;
            if (isUnexpectedException) {
                throw new TwitterException("Stream closed.", ioe);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onSender(JSONObject json) throws TwitterException {
        logger.warn("Unhandled event: onSender");
    }

    /* access modifiers changed from: protected */
    public void onStatus(JSONObject json) throws TwitterException {
        logger.warn("Unhandled event: onStatus");
    }

    /* access modifiers changed from: protected */
    public void onDirectMessage(JSONObject json) throws TwitterException, JSONException {
        logger.warn("Unhandled event: onDirectMessage");
    }

    /* access modifiers changed from: protected */
    public void onDelete(JSONObject json) throws TwitterException, JSONException {
        logger.warn("Unhandled event: onDelete");
    }

    /* access modifiers changed from: protected */
    public void onLimit(JSONObject json) throws TwitterException, JSONException {
        logger.warn("Unhandled event: onLimit");
    }

    /* access modifiers changed from: protected */
    public void onScrubGeo(JSONObject json) throws TwitterException, JSONException {
        logger.warn("Unhandled event: onScrubGeo");
    }

    /* access modifiers changed from: protected */
    public void onFriends(JSONObject json) throws TwitterException, JSONException {
        logger.warn("Unhandled event: onFriends");
    }

    /* access modifiers changed from: protected */
    public void onFavorite(JSONObject source, JSONObject target, JSONObject targetObject) throws TwitterException {
        logger.warn("Unhandled event: onFavorite");
    }

    /* access modifiers changed from: protected */
    public void onUnfavorite(JSONObject source, JSONObject target, JSONObject targetObject) throws TwitterException {
        logger.warn("Unhandled event: onUnfavorite");
    }

    /* access modifiers changed from: protected */
    public void onRetweet(JSONObject source, JSONObject target, JSONObject targetObject) throws TwitterException {
        logger.warn("Unhandled event: onRetweet");
    }

    /* access modifiers changed from: protected */
    public void onFollow(JSONObject source, JSONObject target) throws TwitterException {
        logger.warn("Unhandled event: onFollow");
    }

    /* access modifiers changed from: protected */
    public void onUnfollow(JSONObject source, JSONObject target) throws TwitterException {
        logger.warn("Unhandled event: onUnfollow");
    }

    /* access modifiers changed from: protected */
    public void onUserListMemberAddition(JSONObject addedMember, JSONObject owner, JSONObject userList) throws TwitterException, JSONException {
        logger.warn("Unhandled event: onUserListMemberAddition");
    }

    /* access modifiers changed from: protected */
    public void onUserListMemberDeletion(JSONObject deletedMember, JSONObject owner, JSONObject userList) throws TwitterException, JSONException {
        logger.warn("Unhandled event: onUserListMemberDeletion");
    }

    /* access modifiers changed from: protected */
    public void onUserListSubscription(JSONObject source, JSONObject owner, JSONObject userList) throws TwitterException, JSONException {
        logger.warn("Unhandled event: onUserListSubscription");
    }

    /* access modifiers changed from: protected */
    public void onUserListUnsubscription(JSONObject source, JSONObject owner, JSONObject userList) throws TwitterException, JSONException {
        logger.warn("Unhandled event: onUserListUnsubscription");
    }

    /* access modifiers changed from: protected */
    public void onUserListCreation(JSONObject source, JSONObject userList) throws TwitterException, JSONException {
        logger.warn("Unhandled event: onUserListCreation");
    }

    /* access modifiers changed from: protected */
    public void onUserListUpdated(JSONObject source, JSONObject userList) throws TwitterException, JSONException {
        logger.warn("Unhandled event: onUserListUpdated");
    }

    /* access modifiers changed from: protected */
    public void onUserListDestroyed(JSONObject source, JSONObject userList) throws TwitterException {
        logger.warn("Unhandled event: onUserListDestroyed");
    }

    /* access modifiers changed from: protected */
    public void onUserUpdate(JSONObject source, JSONObject target) throws TwitterException {
        logger.warn("Unhandled event: onUserUpdate");
    }

    /* access modifiers changed from: protected */
    public void onBlock(JSONObject source, JSONObject target) throws TwitterException {
        logger.warn("Unhandled event: onBlock");
    }

    /* access modifiers changed from: protected */
    public void onUnblock(JSONObject source, JSONObject target) throws TwitterException {
        logger.warn("Unhandled event: onUnblock");
    }

    /* access modifiers changed from: protected */
    public void onException(Exception e) {
        logger.warn("Unhandled event: ", e.getMessage());
    }

    public void close() throws IOException {
        this.streamAlive = false;
        this.is.close();
        this.br.close();
        if (this.response != null) {
            this.response.disconnect();
        }
    }

    /* access modifiers changed from: protected */
    public Status asStatus(JSONObject json) throws TwitterException {
        Status status = new StatusJSONImpl(json);
        DataObjectFactoryUtil.registerJSONObject(status, json);
        return status;
    }

    /* access modifiers changed from: protected */
    public DirectMessage asDirectMessage(JSONObject json) throws TwitterException {
        try {
            DirectMessage directMessage = new DirectMessageJSONImpl(json.getJSONObject("direct_message"));
            DataObjectFactoryUtil.registerJSONObject(directMessage, json);
            return directMessage;
        } catch (JSONException e) {
            throw new TwitterException(e);
        }
    }

    /* access modifiers changed from: protected */
    public int[] asFriendList(JSONObject json) throws TwitterException {
        try {
            JSONArray friends = json.getJSONArray("friends");
            int[] friendIds = new int[friends.length()];
            for (int i = 0; i < friendIds.length; i++) {
                friendIds[i] = friends.getInt(i);
            }
            return friendIds;
        } catch (JSONException e) {
            throw new TwitterException(e);
        }
    }

    /* access modifiers changed from: protected */
    public User asUser(JSONObject json) throws TwitterException {
        User user = new UserJSONImpl(json);
        DataObjectFactoryUtil.registerJSONObject(user, json);
        return user;
    }

    /* access modifiers changed from: protected */
    public UserList asUserList(JSONObject json) throws TwitterException {
        UserList userList = new UserListJSONImpl(json);
        DataObjectFactoryUtil.registerJSONObject(userList, json);
        return userList;
    }
}
