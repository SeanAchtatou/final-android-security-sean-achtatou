package twitter4j.http;

import java.io.Serializable;
import javax.crypto.spec.SecretKeySpec;
import oauth.signpost.OAuth;
import twitter4j.TwitterException;
import twitter4j.internal.http.HttpResponse;
import twitter4j.internal.util.StringUtil;

abstract class OAuthToken implements Serializable {
    private static final long serialVersionUID = 3891133932519746686L;
    String[] responseStr;
    private transient SecretKeySpec secretKeySpec;
    private String token;
    private String tokenSecret;

    public OAuthToken(String token2, String tokenSecret2) {
        this.responseStr = null;
        this.token = token2;
        this.tokenSecret = tokenSecret2;
    }

    OAuthToken(HttpResponse response) throws TwitterException {
        this(response.asString());
    }

    OAuthToken(String string) {
        this.responseStr = null;
        this.responseStr = StringUtil.split(string, "&");
        this.tokenSecret = getParameter(OAuth.OAUTH_TOKEN_SECRET);
        this.token = getParameter(OAuth.OAUTH_TOKEN);
    }

    public String getToken() {
        return this.token;
    }

    public String getTokenSecret() {
        return this.tokenSecret;
    }

    /* access modifiers changed from: package-private */
    public void setSecretKeySpec(SecretKeySpec secretKeySpec2) {
        this.secretKeySpec = secretKeySpec2;
    }

    /* access modifiers changed from: package-private */
    public SecretKeySpec getSecretKeySpec() {
        return this.secretKeySpec;
    }

    public String getParameter(String parameter) {
        for (String str : this.responseStr) {
            if (str.startsWith(new StringBuffer().append(parameter).append('=').toString())) {
                return StringUtil.split(str, "=")[1].trim();
            }
        }
        return null;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof OAuthToken)) {
            return false;
        }
        OAuthToken that = (OAuthToken) o;
        if (!this.token.equals(that.token)) {
            return false;
        }
        return this.tokenSecret.equals(that.tokenSecret);
    }

    public int hashCode() {
        return (this.token.hashCode() * 31) + this.tokenSecret.hashCode();
    }

    public String toString() {
        return new StringBuffer().append("OAuthToken{token='").append(this.token).append('\'').append(", tokenSecret='").append(this.tokenSecret).append('\'').append(", secretKeySpec=").append(this.secretKeySpec).append('}').toString();
    }
}
