package twitter4j.http;

public interface AuthorizationConfiguration {
    String getOAuthAccessToken();

    String getOAuthAccessTokenSecret();

    String getOAuthConsumerKey();

    String getOAuthConsumerSecret();

    String getPassword();

    String getUser();
}
