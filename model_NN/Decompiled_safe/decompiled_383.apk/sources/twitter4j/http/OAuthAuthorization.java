package twitter4j.http;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import oauth.signpost.OAuth;
import twitter4j.TwitterException;
import twitter4j.conf.Configuration;
import twitter4j.internal.http.BASE64Encoder;
import twitter4j.internal.http.HttpClientWrapper;
import twitter4j.internal.http.HttpParameter;
import twitter4j.internal.http.HttpRequest;
import twitter4j.internal.logging.Logger;
import twitter4j.internal.util.StringUtil;

public class OAuthAuthorization implements Authorization, Serializable, OAuthSupport {
    private static final String HMAC_SHA1 = "HmacSHA1";
    private static final HttpParameter OAUTH_SIGNATURE_METHOD = new HttpParameter(OAuth.OAUTH_SIGNATURE_METHOD, "HMAC-SHA1");
    private static Random RAND = new Random();
    static Class class$twitter4j$http$OAuthAuthorization = null;
    private static transient HttpClientWrapper http = null;
    private static final Logger logger;
    private static final long serialVersionUID = -4368426677157998618L;
    private final Configuration conf;
    private String consumerKey;
    private String consumerSecret;
    private OAuthToken oauthToken;
    private String realm;

    static {
        Class cls;
        if (class$twitter4j$http$OAuthAuthorization == null) {
            cls = class$("twitter4j.http.OAuthAuthorization");
            class$twitter4j$http$OAuthAuthorization = cls;
        } else {
            cls = class$twitter4j$http$OAuthAuthorization;
        }
        logger = Logger.getLogger(cls);
    }

    static Class class$(String x0) {
        try {
            return Class.forName(x0);
        } catch (ClassNotFoundException x1) {
            throw new NoClassDefFoundError().initCause(x1);
        }
    }

    public OAuthAuthorization(Configuration conf2) {
        this(conf2, conf2.getOAuthConsumerKey(), conf2.getOAuthConsumerSecret(), new AccessToken(conf2.getOAuthAccessToken(), conf2.getOAuthAccessTokenSecret()));
    }

    public OAuthAuthorization(Configuration conf2, String consumerKey2, String consumerSecret2) {
        this.consumerKey = "";
        this.realm = null;
        this.oauthToken = null;
        this.conf = conf2;
        init(consumerKey2, consumerSecret2);
    }

    public OAuthAuthorization(Configuration conf2, String consumerKey2, String consumerSecret2, AccessToken accessToken) {
        this.consumerKey = "";
        this.realm = null;
        this.oauthToken = null;
        this.conf = conf2;
        init(consumerKey2, consumerSecret2, accessToken);
    }

    private void init(String consumerKey2, String consumerSecret2) {
        http = new HttpClientWrapper(this.conf);
        setConsumerKey(consumerKey2);
        setConsumerSecret(consumerSecret2);
    }

    private void init(String consumerKey2, String consumerSecret2, AccessToken accessToken) {
        init(consumerKey2, consumerSecret2);
        setOAuthAccessToken(accessToken);
    }

    public String getAuthorizationHeader(HttpRequest req) {
        return generateAuthorizationHeader(req.getMethod().name(), req.getURL(), req.getParameters(), this.oauthToken);
    }

    private void ensureTokenIsAvailable() {
        if (this.oauthToken == null) {
            throw new IllegalStateException("No Token available.");
        }
    }

    public boolean isEnabled() {
        return this.oauthToken != null && (this.oauthToken instanceof AccessToken);
    }

    public RequestToken getOAuthRequestToken() throws TwitterException {
        return getOAuthRequestToken(null);
    }

    /* Debug info: failed to restart local var, previous not found, register: 5 */
    public RequestToken getOAuthRequestToken(String callbackURL) throws TwitterException {
        HttpParameter[] params;
        if (this.oauthToken instanceof AccessToken) {
            throw new IllegalStateException("Access token already available.");
        }
        if (callbackURL != null) {
            params = new HttpParameter[]{new HttpParameter((String) OAuth.OAUTH_CALLBACK, callbackURL)};
        } else {
            params = new HttpParameter[0];
        }
        this.oauthToken = new RequestToken(http.post(this.conf.getOAuthRequestTokenURL(), params, this), this);
        return (RequestToken) this.oauthToken;
    }

    /* Debug info: failed to restart local var, previous not found, register: 3 */
    public AccessToken getOAuthAccessToken() throws TwitterException {
        ensureTokenIsAvailable();
        if (this.oauthToken instanceof AccessToken) {
            return (AccessToken) this.oauthToken;
        }
        this.oauthToken = new AccessToken(http.post(this.conf.getOAuthAccessTokenURL(), this));
        return (AccessToken) this.oauthToken;
    }

    public AccessToken getOAuthAccessToken(String oauthVerifier) throws TwitterException {
        ensureTokenIsAvailable();
        this.oauthToken = new AccessToken(http.post(this.conf.getOAuthAccessTokenURL(), new HttpParameter[]{new HttpParameter((String) OAuth.OAUTH_VERIFIER, oauthVerifier)}, this));
        return (AccessToken) this.oauthToken;
    }

    public AccessToken getOAuthAccessToken(RequestToken requestToken) throws TwitterException {
        this.oauthToken = requestToken;
        return getOAuthAccessToken();
    }

    public AccessToken getOAuthAccessToken(RequestToken requestToken, String oauthVerifier) throws TwitterException {
        this.oauthToken = requestToken;
        return getOAuthAccessToken(oauthVerifier);
    }

    public AccessToken getOAuthAccessToken(String screenName, String password) throws TwitterException {
        try {
            String url = this.conf.getOAuthAccessTokenURL();
            if (url.indexOf("http://") == 0) {
                url = new StringBuffer().append("https://").append(url.substring(7)).toString();
            }
            this.oauthToken = new AccessToken(http.post(url, new HttpParameter[]{new HttpParameter("x_auth_username", screenName), new HttpParameter("x_auth_password", password), new HttpParameter("x_auth_mode", "client_auth")}, this));
            return (AccessToken) this.oauthToken;
        } catch (TwitterException e) {
            TwitterException te = e;
            throw new TwitterException("The screen name / password combination seems to be invalid.", te, te.getStatusCode());
        }
    }

    public void setOAuthAccessToken(AccessToken accessToken) {
        this.oauthToken = accessToken;
    }

    public void setOAuthRealm(String realm2) {
        this.realm = realm2;
    }

    /* access modifiers changed from: package-private */
    public String generateAuthorizationHeader(String method, String url, HttpParameter[] params, String nonce, String timestamp, OAuthToken otoken) {
        if (params == null) {
            params = new HttpParameter[0];
        }
        List<HttpParameter> oauthHeaderParams = new ArrayList<>(5);
        oauthHeaderParams.add(new HttpParameter(OAuth.OAUTH_CONSUMER_KEY, this.consumerKey));
        oauthHeaderParams.add(OAUTH_SIGNATURE_METHOD);
        oauthHeaderParams.add(new HttpParameter(OAuth.OAUTH_TIMESTAMP, timestamp));
        oauthHeaderParams.add(new HttpParameter(OAuth.OAUTH_NONCE, nonce));
        oauthHeaderParams.add(new HttpParameter(OAuth.OAUTH_VERSION, OAuth.VERSION_1_0));
        if (otoken != null) {
            oauthHeaderParams.add(new HttpParameter(OAuth.OAUTH_TOKEN, otoken.getToken()));
        }
        List<HttpParameter> signatureBaseParams = new ArrayList<>(oauthHeaderParams.size() + params.length);
        signatureBaseParams.addAll(oauthHeaderParams);
        if (!HttpParameter.containsFile(params)) {
            signatureBaseParams.addAll(toParamList(params));
        }
        parseGetParameters(url, signatureBaseParams);
        StringBuffer base = new StringBuffer(method).append("&").append(HttpParameter.encode(constructRequestURL(url))).append("&");
        base.append(HttpParameter.encode(normalizeRequestParameters(signatureBaseParams)));
        String oauthBaseString = base.toString();
        logger.debug("OAuth base string: ", oauthBaseString);
        String signature = generateSignature(oauthBaseString, otoken);
        logger.debug("OAuth signature: ", signature);
        oauthHeaderParams.add(new HttpParameter(OAuth.OAUTH_SIGNATURE, signature));
        if (this.realm != null) {
            oauthHeaderParams.add(new HttpParameter("realm", this.realm));
        }
        return new StringBuffer().append("OAuth ").append(encodeParameters(oauthHeaderParams, ",", true)).toString();
    }

    private void parseGetParameters(String url, List<HttpParameter> signatureBaseParams) {
        int queryStart = url.indexOf("?");
        if (-1 != queryStart) {
            try {
                for (String query : StringUtil.split(url.substring(queryStart + 1), "&")) {
                    String[] split = StringUtil.split(query, "=");
                    if (split.length == 2) {
                        signatureBaseParams.add(new HttpParameter(URLDecoder.decode(split[0], OAuth.ENCODING), URLDecoder.decode(split[1], OAuth.ENCODING)));
                    } else {
                        signatureBaseParams.add(new HttpParameter(URLDecoder.decode(split[0], OAuth.ENCODING), ""));
                    }
                }
            } catch (UnsupportedEncodingException e) {
            }
        }
    }

    /* access modifiers changed from: package-private */
    public String generateAuthorizationHeader(String method, String url, HttpParameter[] params, OAuthToken token) {
        long timestamp = System.currentTimeMillis() / 1000;
        return generateAuthorizationHeader(method, url, params, String.valueOf(timestamp + ((long) RAND.nextInt())), String.valueOf(timestamp), token);
    }

    public List<HttpParameter> generateOAuthSignatureHttpParams(String method, String url) {
        long timestamp = System.currentTimeMillis() / 1000;
        long nonce = timestamp + ((long) RAND.nextInt());
        List<HttpParameter> oauthHeaderParams = new ArrayList<>(5);
        oauthHeaderParams.add(new HttpParameter(OAuth.OAUTH_CONSUMER_KEY, this.consumerKey));
        oauthHeaderParams.add(OAUTH_SIGNATURE_METHOD);
        oauthHeaderParams.add(new HttpParameter(OAuth.OAUTH_TIMESTAMP, timestamp));
        oauthHeaderParams.add(new HttpParameter(OAuth.OAUTH_NONCE, nonce));
        oauthHeaderParams.add(new HttpParameter(OAuth.OAUTH_VERSION, OAuth.VERSION_1_0));
        if (this.oauthToken != null) {
            oauthHeaderParams.add(new HttpParameter(OAuth.OAUTH_TOKEN, this.oauthToken.getToken()));
        }
        List<HttpParameter> signatureBaseParams = new ArrayList<>(oauthHeaderParams.size());
        signatureBaseParams.addAll(oauthHeaderParams);
        parseGetParameters(url, signatureBaseParams);
        StringBuffer base = new StringBuffer(method).append("&").append(HttpParameter.encode(constructRequestURL(url))).append("&");
        base.append(HttpParameter.encode(normalizeRequestParameters(signatureBaseParams)));
        oauthHeaderParams.add(new HttpParameter(OAuth.OAUTH_SIGNATURE, generateSignature(base.toString(), this.oauthToken)));
        return oauthHeaderParams;
    }

    /* access modifiers changed from: package-private */
    public String generateSignature(String data, OAuthToken token) {
        SecretKeySpec spec;
        byte[] byteHMAC = null;
        try {
            Mac mac = Mac.getInstance(HMAC_SHA1);
            if (token == null) {
                spec = new SecretKeySpec(new StringBuffer().append(HttpParameter.encode(this.consumerSecret)).append("&").toString().getBytes(), HMAC_SHA1);
            } else {
                spec = token.getSecretKeySpec();
                if (spec == null) {
                    spec = new SecretKeySpec(new StringBuffer().append(HttpParameter.encode(this.consumerSecret)).append("&").append(HttpParameter.encode(token.getTokenSecret())).toString().getBytes(), HMAC_SHA1);
                    token.setSecretKeySpec(spec);
                }
            }
            mac.init(spec);
            byteHMAC = mac.doFinal(data.getBytes());
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e2) {
        }
        return BASE64Encoder.encode(byteHMAC);
    }

    /* access modifiers changed from: package-private */
    public String generateSignature(String data) {
        return generateSignature(data, null);
    }

    public static String normalizeRequestParameters(HttpParameter[] params) {
        return normalizeRequestParameters(toParamList(params));
    }

    public static String normalizeRequestParameters(List<HttpParameter> params) {
        Collections.sort(params);
        return encodeParameters(params);
    }

    public static String normalizeAuthorizationHeaders(List<HttpParameter> params) {
        Collections.sort(params);
        return encodeParameters(params);
    }

    public static List<HttpParameter> toParamList(HttpParameter[] params) {
        List<HttpParameter> paramList = new ArrayList<>(params.length);
        paramList.addAll(Arrays.asList(params));
        return paramList;
    }

    public static String encodeParameters(List<HttpParameter> httpParams) {
        return encodeParameters(httpParams, "&", false);
    }

    public static String encodeParameters(List<HttpParameter> httpParams, String splitter, boolean quot) {
        StringBuffer buf = new StringBuffer();
        for (HttpParameter param : httpParams) {
            if (!param.isFile()) {
                if (buf.length() != 0) {
                    if (quot) {
                        buf.append("\"");
                    }
                    buf.append(splitter);
                }
                buf.append(HttpParameter.encode(param.getName())).append("=");
                if (quot) {
                    buf.append("\"");
                }
                buf.append(HttpParameter.encode(param.getValue()));
            }
        }
        if (buf.length() != 0 && quot) {
            buf.append("\"");
        }
        return buf.toString();
    }

    public static String constructRequestURL(String url) {
        int index = url.indexOf("?");
        if (-1 != index) {
            url = url.substring(0, index);
        }
        int slashIndex = url.indexOf("/", 8);
        String baseURL = url.substring(0, slashIndex).toLowerCase();
        int colonIndex = baseURL.indexOf(":", 8);
        if (-1 != colonIndex) {
            if (baseURL.startsWith("http://") && baseURL.endsWith(":80")) {
                baseURL = baseURL.substring(0, colonIndex);
            } else if (baseURL.startsWith("https://") && baseURL.endsWith(":443")) {
                baseURL = baseURL.substring(0, colonIndex);
            }
        }
        return new StringBuffer().append(baseURL).append(url.substring(slashIndex)).toString();
    }

    private void setConsumerKey(String consumerKey2) {
        this.consumerKey = consumerKey2 != null ? consumerKey2 : "";
    }

    private void setConsumerSecret(String consumerSecret2) {
        this.consumerSecret = consumerSecret2 != null ? consumerSecret2 : "";
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof OAuthSupport)) {
            return false;
        }
        OAuthAuthorization that = (OAuthAuthorization) o;
        if (this.consumerKey == null ? that.consumerKey != null : !this.consumerKey.equals(that.consumerKey)) {
            return false;
        }
        if (this.consumerSecret == null ? that.consumerSecret != null : !this.consumerSecret.equals(that.consumerSecret)) {
            return false;
        }
        return this.oauthToken == null ? that.oauthToken == null : this.oauthToken.equals(that.oauthToken);
    }

    public int hashCode() {
        int result;
        int i;
        int i2;
        if (this.consumerKey != null) {
            result = this.consumerKey.hashCode();
        } else {
            result = 0;
        }
        int i3 = result * 31;
        if (this.consumerSecret != null) {
            i = this.consumerSecret.hashCode();
        } else {
            i = 0;
        }
        int i4 = (i3 + i) * 31;
        if (this.oauthToken != null) {
            i2 = this.oauthToken.hashCode();
        } else {
            i2 = 0;
        }
        return i4 + i2;
    }

    public String toString() {
        return new StringBuffer().append("OAuthAuthorization{consumerKey='").append(this.consumerKey).append('\'').append(", consumerSecret='******************************************'").append(", oauthToken=").append(this.oauthToken).append('}').toString();
    }
}
