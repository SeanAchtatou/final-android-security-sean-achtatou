package twitter4j;

import java.io.Serializable;
import java.util.Date;
import twitter4j.internal.http.HttpResponse;
import twitter4j.internal.json.DataObjectFactoryUtil;
import twitter4j.internal.org.json.JSONObject;
import twitter4j.internal.util.ParseUtil;

final class RateLimitStatusJSONImpl implements RateLimitStatus, Serializable {
    private static final long serialVersionUID = 832355052293658614L;
    private int hourlyLimit;
    private int remainingHits;
    private Date resetTime;
    private int resetTimeInSeconds;
    private int secondsUntilReset;

    private RateLimitStatusJSONImpl(int hourlyLimit2, int remainingHits2, int resetTimeInSeconds2, Date resetTime2) {
        this.hourlyLimit = hourlyLimit2;
        this.remainingHits = remainingHits2;
        this.resetTime = resetTime2;
        this.resetTimeInSeconds = resetTimeInSeconds2;
        this.secondsUntilReset = (int) ((resetTime2.getTime() - System.currentTimeMillis()) / 1000);
    }

    RateLimitStatusJSONImpl(HttpResponse res) throws TwitterException {
        JSONObject json = res.asJSONObject();
        init(json);
        DataObjectFactoryUtil.clearThreadLocalMap();
        DataObjectFactoryUtil.registerJSONObject(this, json);
    }

    RateLimitStatusJSONImpl(JSONObject json) throws TwitterException {
        init(json);
    }

    /* access modifiers changed from: package-private */
    public void init(JSONObject json) throws TwitterException {
        this.hourlyLimit = ParseUtil.getInt("hourly_limit", json);
        this.remainingHits = ParseUtil.getInt("remaining_hits", json);
        this.resetTime = ParseUtil.getDate("reset_time", json, "EEE MMM d HH:mm:ss Z yyyy");
        this.resetTimeInSeconds = ParseUtil.getInt("reset_time_in_seconds", json);
        this.secondsUntilReset = (int) ((this.resetTime.getTime() - System.currentTimeMillis()) / 1000);
    }

    static RateLimitStatus createFromResponseHeader(HttpResponse res) {
        if (res == null) {
            return null;
        }
        String limit = res.getResponseHeader("X-RateLimit-Limit");
        if (limit == null) {
            return null;
        }
        int hourlyLimit2 = Integer.parseInt(limit);
        String remaining = res.getResponseHeader("X-RateLimit-Remaining");
        if (remaining == null) {
            return null;
        }
        int remainingHits2 = Integer.parseInt(remaining);
        String reset = res.getResponseHeader("X-RateLimit-Reset");
        if (reset == null) {
            return null;
        }
        long longReset = Long.parseLong(reset);
        return new RateLimitStatusJSONImpl(hourlyLimit2, remainingHits2, (int) (longReset / 1000), new Date(longReset * 1000));
    }

    static RateLimitStatus createFeatureSpecificRateLimitStatusFromResponseHeader(HttpResponse res) {
        if (res == null) {
            return null;
        }
        String limit = res.getResponseHeader("X-FeatureRateLimit-Limit");
        if (limit == null) {
            return null;
        }
        int hourlyLimit2 = Integer.parseInt(limit);
        String remaining = res.getResponseHeader("X-FeatureRateLimit-Remaining");
        if (remaining == null) {
            return null;
        }
        int remainingHits2 = Integer.parseInt(remaining);
        String reset = res.getResponseHeader("X-FeatureRateLimit-Reset");
        if (reset == null) {
            return null;
        }
        long longReset = Long.parseLong(reset);
        return new RateLimitStatusJSONImpl(hourlyLimit2, remainingHits2, (int) (longReset / 1000), new Date(longReset * 1000));
    }

    public int getRemainingHits() {
        return this.remainingHits;
    }

    public int getHourlyLimit() {
        return this.hourlyLimit;
    }

    public int getResetTimeInSeconds() {
        return this.resetTimeInSeconds;
    }

    public int getSecondsUntilReset() {
        return this.secondsUntilReset;
    }

    public Date getResetTime() {
        return this.resetTime;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RateLimitStatusJSONImpl)) {
            return false;
        }
        RateLimitStatusJSONImpl that = (RateLimitStatusJSONImpl) o;
        if (this.hourlyLimit != that.hourlyLimit) {
            return false;
        }
        if (this.remainingHits != that.remainingHits) {
            return false;
        }
        if (this.resetTimeInSeconds != that.resetTimeInSeconds) {
            return false;
        }
        if (this.secondsUntilReset != that.secondsUntilReset) {
            return false;
        }
        return this.resetTime == null ? that.resetTime == null : this.resetTime.equals(that.resetTime);
    }

    public int hashCode() {
        return (((((((this.remainingHits * 31) + this.hourlyLimit) * 31) + this.resetTimeInSeconds) * 31) + this.secondsUntilReset) * 31) + (this.resetTime != null ? this.resetTime.hashCode() : 0);
    }

    public String toString() {
        return new StringBuffer().append("RateLimitStatusJSONImpl{remainingHits=").append(this.remainingHits).append(", hourlyLimit=").append(this.hourlyLimit).append(", resetTimeInSeconds=").append(this.resetTimeInSeconds).append(", secondsUntilReset=").append(this.secondsUntilReset).append(", resetTime=").append(this.resetTime).append('}').toString();
    }
}
