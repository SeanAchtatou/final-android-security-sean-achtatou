package twitter4j;

import java.io.Serializable;
import java.util.Date;

public interface DirectMessage extends TwitterResponse, Serializable {
    Date getCreatedAt();

    long getId();

    User getRecipient();

    int getRecipientId();

    String getRecipientScreenName();

    User getSender();

    int getSenderId();

    String getSenderScreenName();

    String getText();
}
