package twitter4j;

import java.io.Serializable;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationContext;
import twitter4j.http.AccessToken;
import twitter4j.http.Authorization;
import twitter4j.http.AuthorizationFactory;
import twitter4j.http.BasicAuthorization;
import twitter4j.http.OAuthAuthorization;

public final class AsyncTwitterFactory implements Serializable {
    private static final long serialVersionUID = -2565686715640816219L;
    private final Configuration conf;
    private final TwitterListener listener;

    public AsyncTwitterFactory() {
        this(ConfigurationContext.getInstance());
    }

    public AsyncTwitterFactory(Configuration conf2) {
        if (conf2 == null) {
            throw new NullPointerException("configuration cannot be null");
        }
        this.conf = conf2;
        this.listener = new TwitterAdapter();
    }

    public AsyncTwitterFactory(Configuration conf2, TwitterListener listener2) {
        if (conf2 == null) {
            throw new NullPointerException("configuration cannot be null");
        }
        this.conf = conf2;
        this.listener = listener2;
    }

    public AsyncTwitterFactory(TwitterListener listener2) {
        this.conf = ConfigurationContext.getInstance();
        this.listener = listener2;
    }

    public AsyncTwitterFactory(String configTreePath, TwitterListener listener2) {
        this.conf = ConfigurationContext.getInstance(configTreePath);
        this.listener = listener2;
    }

    public AsyncTwitter getInstance() {
        return getInstance(this.conf);
    }

    public AsyncTwitter getInstance(String screenName, String password) {
        return getInstance(this.conf, new BasicAuthorization(screenName, password));
    }

    public AsyncTwitter getInstance(AccessToken accessToken) {
        String consumerKey = this.conf.getOAuthConsumerKey();
        String consumerSecret = this.conf.getOAuthConsumerSecret();
        if (consumerKey == null && consumerSecret == null) {
            throw new IllegalStateException("Consumer key and Consumer secret not supplied.");
        }
        return getInstance(this.conf, new OAuthAuthorization(this.conf, consumerKey, consumerSecret, accessToken));
    }

    public AsyncTwitter getInstance(Authorization auth) {
        return getInstance(this.conf, auth);
    }

    public AsyncTwitter getOAuthAuthorizedInstance(String consumerKey, String consumerSecret) {
        if (consumerKey == null && consumerSecret == null) {
            throw new IllegalStateException("Consumer key and Consumer secret not supplied.");
        }
        return getInstance(this.conf, new OAuthAuthorization(this.conf, consumerKey, consumerSecret));
    }

    public AsyncTwitter getOAuthAuthorizedInstance(String consumerKey, String consumerSecret, AccessToken accessToken) {
        if (consumerKey == null && consumerSecret == null) {
            throw new IllegalStateException("Consumer key and Consumer secret not supplied.");
        }
        OAuthAuthorization oauth2 = new OAuthAuthorization(this.conf, consumerKey, consumerSecret);
        oauth2.setOAuthAccessToken(accessToken);
        return getInstance(this.conf, oauth2);
    }

    public AsyncTwitter getOAuthAuthorizedInstance(AccessToken accessToken) {
        return getInstance(accessToken);
    }

    private AsyncTwitter getInstance(Configuration conf2, Authorization auth) {
        return new AsyncTwitter(conf2, auth, this.listener);
    }

    private AsyncTwitter getInstance(Configuration conf2) {
        return new AsyncTwitter(conf2, AuthorizationFactory.getInstance(conf2, true), this.listener);
    }
}
