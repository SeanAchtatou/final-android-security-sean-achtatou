package twitter4j.internal.logging;

public abstract class Logger {
    private static final LoggerFactory LOGGER_FACTORY;
    private static final String LOGGER_FACTORY_IMPLEMENTATION = "twitter4j.loggerFactory";
    static Class class$twitter4j$internal$logging$Logger;

    public abstract void debug(String str);

    public abstract void debug(String str, String str2);

    public abstract void info(String str);

    public abstract void info(String str, String str2);

    public abstract boolean isDebugEnabled();

    public abstract boolean isInfoEnabled();

    public abstract boolean isWarnEnabled();

    public abstract void warn(String str);

    public abstract void warn(String str, String str2);

    /* JADX WARN: Type inference failed for: r4v16, types: [java.lang.Object] */
    /* JADX WARNING: Multi-variable type inference failed */
    static {
        /*
            r2 = 0
            java.lang.String r4 = "twitter4j.loggerFactory"
            java.lang.String r3 = java.lang.System.getProperty(r4)     // Catch:{ NoClassDefFoundError -> 0x0085, ClassNotFoundException -> 0x0083, InstantiationException -> 0x0072, IllegalAccessException -> 0x0081, SecurityException -> 0x007f }
            if (r3 == 0) goto L_0x0015
            java.lang.Class r4 = java.lang.Class.forName(r3)     // Catch:{ NoClassDefFoundError -> 0x0085, ClassNotFoundException -> 0x0083, InstantiationException -> 0x0072, IllegalAccessException -> 0x0081, SecurityException -> 0x007f }
            java.lang.Object r4 = r4.newInstance()     // Catch:{ NoClassDefFoundError -> 0x0085, ClassNotFoundException -> 0x0083, InstantiationException -> 0x0072, IllegalAccessException -> 0x0081, SecurityException -> 0x007f }
            r0 = r4
            twitter4j.internal.logging.LoggerFactory r0 = (twitter4j.internal.logging.LoggerFactory) r0     // Catch:{ NoClassDefFoundError -> 0x0085, ClassNotFoundException -> 0x0083, InstantiationException -> 0x0072, IllegalAccessException -> 0x0081, SecurityException -> 0x007f }
            r2 = r0
        L_0x0015:
            if (r2 != 0) goto L_0x0024
            java.lang.String r4 = "org.slf4j.impl.StaticLoggerBinder"
            java.lang.Class.forName(r4)     // Catch:{ ClassNotFoundException -> 0x007d }
            java.lang.String r4 = "org.slf4j.Logger"
            java.lang.String r5 = "twitter4j.internal.logging.SLF4JLoggerFactory"
            twitter4j.internal.logging.LoggerFactory r2 = getLoggerFactory(r4, r5)     // Catch:{ ClassNotFoundException -> 0x007d }
        L_0x0024:
            if (r2 != 0) goto L_0x002e
            java.lang.String r4 = "org.apache.commons.logging.Log"
            java.lang.String r5 = "twitter4j.internal.logging.CommonsLoggingLoggerFactory"
            twitter4j.internal.logging.LoggerFactory r2 = getLoggerFactory(r4, r5)
        L_0x002e:
            if (r2 != 0) goto L_0x0038
            java.lang.String r4 = "org.apache.log4j.Logger"
            java.lang.String r5 = "twitter4j.internal.logging.Log4JLoggerFactory"
            twitter4j.internal.logging.LoggerFactory r2 = getLoggerFactory(r4, r5)
        L_0x0038:
            if (r2 != 0) goto L_0x003f
            twitter4j.internal.logging.StdOutLoggerFactory r2 = new twitter4j.internal.logging.StdOutLoggerFactory
            r2.<init>()
        L_0x003f:
            twitter4j.internal.logging.Logger.LOGGER_FACTORY = r2
            java.lang.Class r4 = twitter4j.internal.logging.Logger.class$twitter4j$internal$logging$Logger
            if (r4 != 0) goto L_0x007a
            java.lang.String r4 = "twitter4j.internal.logging.Logger"
            java.lang.Class r4 = class$(r4)
            twitter4j.internal.logging.Logger.class$twitter4j$internal$logging$Logger = r4
        L_0x004d:
            twitter4j.internal.logging.Logger r4 = r2.getLogger(r4)
            java.lang.StringBuffer r5 = new java.lang.StringBuffer
            r5.<init>()
            java.lang.String r6 = "Will use "
            java.lang.StringBuffer r5 = r5.append(r6)
            java.lang.Class r6 = r2.getClass()
            java.lang.StringBuffer r5 = r5.append(r6)
            java.lang.String r6 = " as logging factory."
            java.lang.StringBuffer r5 = r5.append(r6)
            java.lang.String r5 = r5.toString()
            r4.debug(r5)
            return
        L_0x0072:
            r4 = move-exception
            r1 = r4
            java.lang.AssertionError r4 = new java.lang.AssertionError
            r4.<init>(r1)
            throw r4
        L_0x007a:
            java.lang.Class r4 = twitter4j.internal.logging.Logger.class$twitter4j$internal$logging$Logger
            goto L_0x004d
        L_0x007d:
            r4 = move-exception
            goto L_0x0024
        L_0x007f:
            r4 = move-exception
            goto L_0x0015
        L_0x0081:
            r4 = move-exception
            goto L_0x0015
        L_0x0083:
            r4 = move-exception
            goto L_0x0015
        L_0x0085:
            r4 = move-exception
            goto L_0x0015
        */
        throw new UnsupportedOperationException("Method not decompiled: twitter4j.internal.logging.Logger.<clinit>():void");
    }

    static Class class$(String x0) {
        try {
            return Class.forName(x0);
        } catch (ClassNotFoundException x1) {
            throw new NoClassDefFoundError().initCause(x1);
        }
    }

    private static LoggerFactory getLoggerFactory(String checkClassName, String implementationClass) {
        try {
            Class.forName(checkClassName);
            return (LoggerFactory) Class.forName(implementationClass).newInstance();
        } catch (ClassNotFoundException e) {
            return null;
        } catch (InstantiationException e2) {
            throw new AssertionError(e2);
        } catch (IllegalAccessException e3) {
            throw new AssertionError(e3);
        }
    }

    public static Logger getLogger(Class clazz) {
        return LOGGER_FACTORY.getLogger(clazz);
    }
}
