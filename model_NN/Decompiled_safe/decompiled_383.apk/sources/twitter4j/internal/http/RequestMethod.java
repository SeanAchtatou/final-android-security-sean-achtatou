package twitter4j.internal.http;

import java.io.ObjectStreamException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpHead;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;

public final class RequestMethod implements Serializable {
    public static final RequestMethod DELETE = new RequestMethod(HttpDelete.METHOD_NAME);
    public static final RequestMethod GET = new RequestMethod(HttpGet.METHOD_NAME);
    public static final RequestMethod HEAD = new RequestMethod(HttpHead.METHOD_NAME);
    public static final RequestMethod POST = new RequestMethod(HttpPost.METHOD_NAME);
    public static final RequestMethod PUT = new RequestMethod(HttpPut.METHOD_NAME);
    private static final Map<String, RequestMethod> instances = new HashMap(5);
    private static final long serialVersionUID = -4399222582680270381L;
    private final String name;

    private RequestMethod(String name2) {
        this.name = name2;
        instances.put(name2, this);
    }

    public final String name() {
        return this.name;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RequestMethod)) {
            return false;
        }
        return this.name.equals(((RequestMethod) o).name);
    }

    public int hashCode() {
        return this.name.hashCode();
    }

    public String toString() {
        return new StringBuffer().append("RequestMethod{name='").append(this.name).append('\'').append('}').toString();
    }

    private static RequestMethod getInstance(String name2) {
        return instances.get(name2);
    }

    private Object readResolve() throws ObjectStreamException {
        return getInstance(this.name);
    }
}
