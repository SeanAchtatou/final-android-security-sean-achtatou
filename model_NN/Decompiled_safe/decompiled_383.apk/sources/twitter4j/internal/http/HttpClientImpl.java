package twitter4j.internal.http;

import java.io.BufferedInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.Authenticator;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.PasswordAuthentication;
import java.net.Proxy;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import oauth.signpost.OAuth;
import twitter4j.TwitterException;
import twitter4j.conf.ConfigurationContext;
import twitter4j.internal.logging.Logger;
import twitter4j.internal.org.json.HTTP;
import twitter4j.internal.util.StringUtil;

public class HttpClientImpl implements HttpClient, HttpResponseCode, Serializable {
    static Class class$twitter4j$internal$http$HttpClientImpl = null;
    private static final Map<HttpClientConfiguration, HttpClient> instanceMap = new HashMap(1);
    private static boolean isJDK14orEarlier = false;
    private static final Logger logger;
    private static final long serialVersionUID = -8819171414069621503L;
    private final HttpClientConfiguration CONF;

    static HttpClientConfiguration access$000(HttpClientImpl x0) {
        return x0.CONF;
    }

    static {
        Class cls;
        boolean z;
        if (class$twitter4j$internal$http$HttpClientImpl == null) {
            cls = class$("twitter4j.internal.http.HttpClientImpl");
            class$twitter4j$internal$http$HttpClientImpl = cls;
        } else {
            cls = class$twitter4j$internal$http$HttpClientImpl;
        }
        logger = Logger.getLogger(cls);
        isJDK14orEarlier = false;
        try {
            String versionStr = System.getProperty("java.specification.version");
            if (versionStr != null) {
                if (1.5d > Double.parseDouble(versionStr)) {
                    z = true;
                } else {
                    z = false;
                }
                isJDK14orEarlier = z;
            }
            if (ConfigurationContext.getInstance().isDalvik()) {
                System.setProperty("http.keepAlive", "false");
            }
        } catch (SecurityException e) {
            isJDK14orEarlier = true;
        }
    }

    static Class class$(String x0) {
        try {
            return Class.forName(x0);
        } catch (ClassNotFoundException x1) {
            throw new NoClassDefFoundError().initCause(x1);
        }
    }

    public HttpClientImpl() {
        this.CONF = ConfigurationContext.getInstance();
    }

    public HttpClientImpl(HttpClientConfiguration conf) {
        this.CONF = conf;
        if (isProxyConfigured() && isJDK14orEarlier) {
            logger.warn("HTTP Proxy is not supported on JDK1.4 or earlier. Try twitter4j-httpclient-supoprt artifact");
        }
    }

    public void shutdown() {
    }

    public static HttpClient getInstance(HttpClientConfiguration conf) {
        HttpClient client = instanceMap.get(conf);
        if (client != null) {
            return client;
        }
        HttpClient client2 = new HttpClientImpl(conf);
        instanceMap.put(conf, client2);
        return client2;
    }

    public HttpResponse get(String url) throws TwitterException {
        return request(new HttpRequest(RequestMethod.GET, url, null, null, null));
    }

    public HttpResponse post(String url, HttpParameter[] params) throws TwitterException {
        return request(new HttpRequest(RequestMethod.POST, url, params, null, null));
    }

    public HttpResponse request(HttpRequest req) throws TwitterException {
        int retry = this.CONF.getHttpRetryCount() + 1;
        HttpResponseImpl httpResponseImpl = null;
        int retriedCount = 0;
        while (true) {
            HttpResponse res = httpResponseImpl;
            if (retriedCount >= retry) {
                return res;
            }
            int responseCode = -1;
            OutputStream os = null;
            try {
                HttpURLConnection con = getConnection(req.getURL());
                con.setDoInput(true);
                setHeaders(req, con);
                con.setRequestMethod(req.getMethod().name());
                if (req.getMethod() == RequestMethod.POST) {
                    if (HttpParameter.containsFile(req.getParameters())) {
                        String boundary = new StringBuffer().append("----Twitter4J-upload").append(System.currentTimeMillis()).toString();
                        con.setRequestProperty("Content-Type", new StringBuffer().append("multipart/form-data; boundary=").append(boundary).toString());
                        String boundary2 = new StringBuffer().append("--").append(boundary).toString();
                        con.setDoOutput(true);
                        os = con.getOutputStream();
                        DataOutputStream dataOutputStream = new DataOutputStream(os);
                        for (HttpParameter param : req.getParameters()) {
                            if (param.isFile()) {
                                write(dataOutputStream, new StringBuffer().append(boundary2).append(HTTP.CRLF).toString());
                                write(dataOutputStream, new StringBuffer().append("Content-Disposition: form-data; name=\"").append(param.getName()).append("\"; filename=\"").append(param.getFile().getName()).append("\"\r\n").toString());
                                write(dataOutputStream, new StringBuffer().append("Content-Type: ").append(param.getContentType()).append("\r\n\r\n").toString());
                                BufferedInputStream bufferedInputStream = new BufferedInputStream(param.hasFileBody() ? param.getFileBody() : new FileInputStream(param.getFile()));
                                while (true) {
                                    int buff = bufferedInputStream.read();
                                    if (buff == -1) {
                                        break;
                                    }
                                    dataOutputStream.write(buff);
                                }
                                write(dataOutputStream, HTTP.CRLF);
                                bufferedInputStream.close();
                            } else {
                                write(dataOutputStream, new StringBuffer().append(boundary2).append(HTTP.CRLF).toString());
                                write(dataOutputStream, new StringBuffer().append("Content-Disposition: form-data; name=\"").append(param.getName()).append("\"\r\n").toString());
                                write(dataOutputStream, "Content-Type: text/plain; charset=UTF-8\r\n\r\n");
                                logger.debug(param.getValue());
                                dataOutputStream.write(param.getValue().getBytes(OAuth.ENCODING));
                                write(dataOutputStream, HTTP.CRLF);
                            }
                        }
                        write(dataOutputStream, new StringBuffer().append(boundary2).append("--\r\n").toString());
                        write(dataOutputStream, HTTP.CRLF);
                    } else {
                        con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                        String postParam = HttpParameter.encodeParameters(req.getParameters());
                        logger.debug("Post Params: ", postParam);
                        byte[] bytes = postParam.getBytes(OAuth.ENCODING);
                        con.setRequestProperty("Content-Length", Integer.toString(bytes.length));
                        con.setDoOutput(true);
                        os = con.getOutputStream();
                        os.write(bytes);
                    }
                    os.flush();
                    os.close();
                }
                httpResponseImpl = new HttpResponseImpl(con, this.CONF);
                try {
                    responseCode = con.getResponseCode();
                    if (logger.isDebugEnabled()) {
                        logger.debug("Response: ");
                        Map<String, List<String>> responseHeaders = con.getHeaderFields();
                        for (String key : responseHeaders.keySet()) {
                            for (String value : responseHeaders.get(key)) {
                                if (key != null) {
                                    logger.debug(new StringBuffer().append(key).append(": ").append(value).toString());
                                } else {
                                    logger.debug(value);
                                }
                            }
                        }
                    }
                    if (responseCode >= 200 && (responseCode == 302 || 300 > responseCode)) {
                        try {
                            os.close();
                            return httpResponseImpl;
                        } catch (Exception e) {
                            return httpResponseImpl;
                        }
                    } else if (responseCode != 420 && responseCode != 400 && responseCode >= 500 && retriedCount != this.CONF.getHttpRetryCount()) {
                        try {
                            os.close();
                        } catch (Exception e2) {
                        }
                        try {
                            if (logger.isDebugEnabled() && httpResponseImpl != null) {
                                httpResponseImpl.asString();
                            }
                            logger.debug(new StringBuffer().append("Sleeping ").append(this.CONF.getHttpRetryIntervalSeconds()).append(" seconds until the next retry.").toString());
                            Thread.sleep((long) (this.CONF.getHttpRetryIntervalSeconds() * 1000));
                        } catch (InterruptedException e3) {
                        }
                        retriedCount++;
                    }
                } catch (Throwable th) {
                    th = th;
                    try {
                        os.close();
                    } catch (Exception e4) {
                    }
                    try {
                        throw th;
                    } catch (IOException e5) {
                        IOException ioe = e5;
                        if (retriedCount == this.CONF.getHttpRetryCount()) {
                            throw new TwitterException(ioe.getMessage(), ioe, responseCode);
                        }
                    }
                }
            } catch (Throwable th2) {
                th = th2;
                httpResponseImpl = res;
            }
        }
        throw new TwitterException(httpResponseImpl.asString(), httpResponseImpl);
    }

    private void write(DataOutputStream out, String outStr) throws IOException {
        out.writeBytes(outStr);
        logger.debug(outStr);
    }

    public static String encode(String str) {
        try {
            return URLEncoder.encode(str, OAuth.ENCODING);
        } catch (UnsupportedEncodingException e) {
            throw new AssertionError("will never happen");
        }
    }

    private void setHeaders(HttpRequest req, HttpURLConnection connection) {
        String authorizationHeader;
        if (logger.isDebugEnabled()) {
            logger.debug("Request: ");
            logger.debug(new StringBuffer().append(req.getMethod().name()).append(" ").toString(), req.getURL());
        }
        if (!(req.getAuthorization() == null || (authorizationHeader = req.getAuthorization().getAuthorizationHeader(req)) == null)) {
            if (logger.isDebugEnabled()) {
                logger.debug("Authorization: ", StringUtil.maskString(authorizationHeader));
            }
            connection.addRequestProperty("Authorization", authorizationHeader);
        }
        if (req.getRequestHeaders() != null) {
            for (String key : req.getRequestHeaders().keySet()) {
                connection.addRequestProperty(key, req.getRequestHeaders().get(key));
                logger.debug(new StringBuffer().append(key).append(": ").append(req.getRequestHeaders().get(key)).toString());
            }
        }
    }

    private HttpURLConnection getConnection(String url) throws IOException {
        HttpURLConnection con;
        if (!isProxyConfigured() || isJDK14orEarlier) {
            con = (HttpURLConnection) new URL(url).openConnection();
        } else {
            if (this.CONF.getHttpProxyUser() != null && !this.CONF.getHttpProxyUser().equals("")) {
                if (logger.isDebugEnabled()) {
                    logger.debug(new StringBuffer().append("Proxy AuthUser: ").append(this.CONF.getHttpProxyUser()).toString());
                    logger.debug(new StringBuffer().append("Proxy AuthPassword: ").append(StringUtil.maskString(this.CONF.getHttpProxyPassword())).toString());
                }
                Authenticator.setDefault(new Authenticator(this) {
                    private final HttpClientImpl this$0;

                    {
                        this.this$0 = r1;
                    }

                    /* access modifiers changed from: protected */
                    public PasswordAuthentication getPasswordAuthentication() {
                        if (getRequestorType().equals(Authenticator.RequestorType.PROXY)) {
                            return new PasswordAuthentication(HttpClientImpl.access$000(this.this$0).getHttpProxyUser(), HttpClientImpl.access$000(this.this$0).getHttpProxyPassword().toCharArray());
                        }
                        return null;
                    }
                });
            }
            Proxy proxy = new Proxy(Proxy.Type.HTTP, InetSocketAddress.createUnresolved(this.CONF.getHttpProxyHost(), this.CONF.getHttpProxyPort()));
            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuffer().append("Opening proxied connection(").append(this.CONF.getHttpProxyHost()).append(":").append(this.CONF.getHttpProxyPort()).append(")").toString());
            }
            con = (HttpURLConnection) new URL(url).openConnection(proxy);
        }
        if (this.CONF.getHttpConnectionTimeout() > 0 && !isJDK14orEarlier) {
            con.setConnectTimeout(this.CONF.getHttpConnectionTimeout());
        }
        if (this.CONF.getHttpReadTimeout() > 0 && !isJDK14orEarlier) {
            con.setReadTimeout(this.CONF.getHttpReadTimeout());
        }
        con.setInstanceFollowRedirects(false);
        return con;
    }

    private boolean isProxyConfigured() {
        return this.CONF.getHttpProxyHost() != null && !this.CONF.getHttpProxyHost().equals("");
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        HttpClientImpl that = (HttpClientImpl) o;
        return this.CONF == null ? that.CONF == null : this.CONF.equals(that.CONF);
    }

    public int hashCode() {
        if (this.CONF != null) {
            return this.CONF.hashCode();
        }
        return 0;
    }

    public String toString() {
        return new StringBuffer().append("HttpClientImpl{CONF=").append(this.CONF).append('}').toString();
    }
}
