package twitter4j.internal.http;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Map;
import oauth.signpost.OAuth;
import twitter4j.TwitterException;
import twitter4j.conf.ConfigurationContext;
import twitter4j.internal.logging.Logger;
import twitter4j.internal.org.json.JSONException;
import twitter4j.internal.org.json.JSONObject;
import twitter4j.internal.org.json.JSONTokener;

public abstract class HttpResponse {
    static Class class$twitter4j$internal$http$HttpResponseImpl;
    private static final Logger logger;
    protected final HttpClientConfiguration CONF;
    protected InputStream is;
    private JSONObject json;
    protected String responseAsString;
    protected int statusCode;
    private boolean streamConsumed;

    public abstract void disconnect() throws IOException;

    public abstract String getResponseHeader(String str);

    public abstract Map<String, List<String>> getResponseHeaderFields();

    static {
        Class cls;
        if (class$twitter4j$internal$http$HttpResponseImpl == null) {
            cls = class$("twitter4j.internal.http.HttpResponseImpl");
            class$twitter4j$internal$http$HttpResponseImpl = cls;
        } else {
            cls = class$twitter4j$internal$http$HttpResponseImpl;
        }
        logger = Logger.getLogger(cls);
    }

    static Class class$(String x0) {
        try {
            return Class.forName(x0);
        } catch (ClassNotFoundException x1) {
            throw new NoClassDefFoundError().initCause(x1);
        }
    }

    HttpResponse() {
        this.responseAsString = null;
        this.streamConsumed = false;
        this.json = null;
        this.CONF = ConfigurationContext.getInstance();
    }

    public HttpResponse(HttpClientConfiguration conf) {
        this.responseAsString = null;
        this.streamConsumed = false;
        this.json = null;
        this.CONF = conf;
    }

    public final int getStatusCode() {
        return this.statusCode;
    }

    public final InputStream asStream() {
        if (!this.streamConsumed) {
            return this.is;
        }
        throw new IllegalStateException("Stream has already been consumed.");
    }

    public final String asString() throws TwitterException {
        if (this.responseAsString == null) {
            InputStream stream = null;
            try {
                InputStream stream2 = asStream();
                if (stream2 == null) {
                    if (stream2 != null) {
                        try {
                            stream2.close();
                        } catch (IOException e) {
                        }
                    }
                    disconnectForcibly();
                    return null;
                }
                BufferedReader br = new BufferedReader(new InputStreamReader(stream2, OAuth.ENCODING));
                StringBuffer buf = new StringBuffer();
                while (true) {
                    String line = br.readLine();
                    if (line == null) {
                        break;
                    }
                    buf.append(line).append("\n");
                }
                this.responseAsString = buf.toString();
                logger.debug(this.responseAsString);
                stream2.close();
                this.streamConsumed = true;
                if (stream2 != null) {
                    try {
                        stream2.close();
                    } catch (IOException e2) {
                    }
                }
                disconnectForcibly();
            } catch (NullPointerException e3) {
                NullPointerException npe = e3;
                throw new TwitterException(npe.getMessage(), npe);
            } catch (IOException e4) {
                IOException ioe = e4;
                throw new TwitterException(ioe.getMessage(), ioe);
            } catch (Throwable th) {
                if (stream != null) {
                    try {
                        stream.close();
                    } catch (IOException e5) {
                    }
                }
                disconnectForcibly();
                throw th;
            }
        }
        return this.responseAsString;
    }

    public final JSONObject asJSONObject() throws TwitterException {
        if (this.json == null) {
            InputStreamReader reader = null;
            try {
                if (!logger.isDebugEnabled()) {
                    reader = asReader();
                    this.json = new JSONObject(new JSONTokener(reader));
                } else if (this.CONF.isPrettyDebugEnabled()) {
                    reader = asReader();
                    this.json = new JSONObject(new JSONTokener(reader));
                    logger.debug(this.json.toString(1));
                } else {
                    this.json = new JSONObject(asString());
                }
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (IOException e) {
                    }
                }
                disconnectForcibly();
            } catch (JSONException e2) {
                JSONException jsone = e2;
                if (logger.isDebugEnabled()) {
                    throw new TwitterException(new StringBuffer().append(jsone.getMessage()).append(":").append(this.responseAsString).toString(), jsone);
                }
                throw new TwitterException(jsone.getMessage(), jsone);
            } catch (Throwable th) {
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (IOException e3) {
                    }
                }
                disconnectForcibly();
                throw th;
            }
        }
        return this.json;
    }

    /* JADX WARNING: Removed duplicated region for block: B:24:0x0059 A[Catch:{ all -> 0x007c }] */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x007f A[SYNTHETIC, Splitter:B:28:0x007f] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0086 A[SYNTHETIC, Splitter:B:32:0x0086] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final twitter4j.internal.org.json.JSONArray asJSONArray() throws twitter4j.TwitterException {
        /*
            r7 = this;
            r0 = 0
            r3 = 0
            twitter4j.internal.logging.Logger r4 = twitter4j.internal.http.HttpResponse.logger     // Catch:{ JSONException -> 0x004f }
            boolean r4 = r4.isDebugEnabled()     // Catch:{ JSONException -> 0x004f }
            if (r4 == 0) goto L_0x003f
            twitter4j.internal.http.HttpClientConfiguration r4 = r7.CONF     // Catch:{ JSONException -> 0x004f }
            boolean r4 = r4.isPrettyDebugEnabled()     // Catch:{ JSONException -> 0x004f }
            if (r4 == 0) goto L_0x0034
            java.io.InputStreamReader r3 = r7.asReader()     // Catch:{ JSONException -> 0x004f }
            twitter4j.internal.org.json.JSONArray r1 = new twitter4j.internal.org.json.JSONArray     // Catch:{ JSONException -> 0x004f }
            twitter4j.internal.org.json.JSONTokener r4 = new twitter4j.internal.org.json.JSONTokener     // Catch:{ JSONException -> 0x004f }
            r4.<init>(r3)     // Catch:{ JSONException -> 0x004f }
            r1.<init>(r4)     // Catch:{ JSONException -> 0x004f }
            twitter4j.internal.logging.Logger r4 = twitter4j.internal.http.HttpResponse.logger     // Catch:{ JSONException -> 0x0097, all -> 0x0094 }
            r5 = 1
            java.lang.String r5 = r1.toString(r5)     // Catch:{ JSONException -> 0x0097, all -> 0x0094 }
            r4.debug(r5)     // Catch:{ JSONException -> 0x0097, all -> 0x0094 }
            r0 = r1
        L_0x002b:
            if (r3 == 0) goto L_0x0030
            r3.close()     // Catch:{ IOException -> 0x0090 }
        L_0x0030:
            r7.disconnectForcibly()
            return r0
        L_0x0034:
            twitter4j.internal.org.json.JSONArray r1 = new twitter4j.internal.org.json.JSONArray     // Catch:{ JSONException -> 0x004f }
            java.lang.String r4 = r7.asString()     // Catch:{ JSONException -> 0x004f }
            r1.<init>(r4)     // Catch:{ JSONException -> 0x004f }
            r0 = r1
            goto L_0x002b
        L_0x003f:
            java.io.InputStreamReader r3 = r7.asReader()     // Catch:{ JSONException -> 0x004f }
            twitter4j.internal.org.json.JSONArray r1 = new twitter4j.internal.org.json.JSONArray     // Catch:{ JSONException -> 0x004f }
            twitter4j.internal.org.json.JSONTokener r4 = new twitter4j.internal.org.json.JSONTokener     // Catch:{ JSONException -> 0x004f }
            r4.<init>(r3)     // Catch:{ JSONException -> 0x004f }
            r1.<init>(r4)     // Catch:{ JSONException -> 0x004f }
            r0 = r1
            goto L_0x002b
        L_0x004f:
            r4 = move-exception
            r2 = r4
        L_0x0051:
            twitter4j.internal.logging.Logger r4 = twitter4j.internal.http.HttpResponse.logger     // Catch:{ all -> 0x007c }
            boolean r4 = r4.isDebugEnabled()     // Catch:{ all -> 0x007c }
            if (r4 == 0) goto L_0x0086
            twitter4j.TwitterException r4 = new twitter4j.TwitterException     // Catch:{ all -> 0x007c }
            java.lang.StringBuffer r5 = new java.lang.StringBuffer     // Catch:{ all -> 0x007c }
            r5.<init>()     // Catch:{ all -> 0x007c }
            java.lang.String r6 = r2.getMessage()     // Catch:{ all -> 0x007c }
            java.lang.StringBuffer r5 = r5.append(r6)     // Catch:{ all -> 0x007c }
            java.lang.String r6 = ":"
            java.lang.StringBuffer r5 = r5.append(r6)     // Catch:{ all -> 0x007c }
            java.lang.String r6 = r7.responseAsString     // Catch:{ all -> 0x007c }
            java.lang.StringBuffer r5 = r5.append(r6)     // Catch:{ all -> 0x007c }
            java.lang.String r5 = r5.toString()     // Catch:{ all -> 0x007c }
            r4.<init>(r5, r2)     // Catch:{ all -> 0x007c }
            throw r4     // Catch:{ all -> 0x007c }
        L_0x007c:
            r4 = move-exception
        L_0x007d:
            if (r3 == 0) goto L_0x0082
            r3.close()     // Catch:{ IOException -> 0x0092 }
        L_0x0082:
            r7.disconnectForcibly()
            throw r4
        L_0x0086:
            twitter4j.TwitterException r4 = new twitter4j.TwitterException     // Catch:{ all -> 0x007c }
            java.lang.String r5 = r2.getMessage()     // Catch:{ all -> 0x007c }
            r4.<init>(r5, r2)     // Catch:{ all -> 0x007c }
            throw r4     // Catch:{ all -> 0x007c }
        L_0x0090:
            r4 = move-exception
            goto L_0x0030
        L_0x0092:
            r5 = move-exception
            goto L_0x0082
        L_0x0094:
            r4 = move-exception
            r0 = r1
            goto L_0x007d
        L_0x0097:
            r4 = move-exception
            r2 = r4
            r0 = r1
            goto L_0x0051
        */
        throw new UnsupportedOperationException("Method not decompiled: twitter4j.internal.http.HttpResponse.asJSONArray():twitter4j.internal.org.json.JSONArray");
    }

    public final InputStreamReader asReader() {
        try {
            return new InputStreamReader(this.is, OAuth.ENCODING);
        } catch (UnsupportedEncodingException e) {
            return new InputStreamReader(this.is);
        }
    }

    private void disconnectForcibly() {
        try {
            disconnect();
        } catch (Exception e) {
        }
    }

    public String toString() {
        return new StringBuffer().append("HttpResponse{statusCode=").append(this.statusCode).append(", responseAsString='").append(this.responseAsString).append('\'').append(", is=").append(this.is).append(", streamConsumed=").append(this.streamConsumed).append('}').toString();
    }
}
