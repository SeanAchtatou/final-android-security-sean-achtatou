package twitter4j.internal.org.json;

import java.util.Iterator;

public class JSONML {
    /* JADX WARNING: Code restructure failed: missing block: B:87:0x014e, code lost:
        throw r13.syntaxError("Reserved attribute.");
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.Object parse(twitter4j.internal.org.json.XMLTokener r13, boolean r14, twitter4j.internal.org.json.JSONArray r15) throws twitter4j.internal.org.json.JSONException {
        /*
            r12 = 91
            r11 = 45
            r3 = 0
            r5 = 0
            r6 = 0
            r7 = 0
        L_0x0008:
            java.lang.Object r8 = r13.nextContent()
            java.lang.Character r9 = twitter4j.internal.org.json.XML.LT
            if (r8 != r9) goto L_0x01da
            java.lang.Object r8 = r13.nextToken()
            boolean r9 = r8 instanceof java.lang.Character
            if (r9 == 0) goto L_0x00c4
            java.lang.Character r9 = twitter4j.internal.org.json.XML.SLASH
            if (r8 != r9) goto L_0x0054
            java.lang.Object r8 = r13.nextToken()
            boolean r9 = r8 instanceof java.lang.String
            if (r9 != 0) goto L_0x0043
            twitter4j.internal.org.json.JSONException r9 = new twitter4j.internal.org.json.JSONException
            java.lang.StringBuffer r10 = new java.lang.StringBuffer
            r10.<init>()
            java.lang.String r11 = "Expected a closing name instead of '"
            java.lang.StringBuffer r10 = r10.append(r11)
            java.lang.StringBuffer r10 = r10.append(r8)
            java.lang.String r11 = "'."
            java.lang.StringBuffer r10 = r10.append(r11)
            java.lang.String r10 = r10.toString()
            r9.<init>(r10)
            throw r9
        L_0x0043:
            java.lang.Object r9 = r13.nextToken()
            java.lang.Character r10 = twitter4j.internal.org.json.XML.GT
            if (r9 == r10) goto L_0x0052
            java.lang.String r9 = "Misshaped close tag"
            twitter4j.internal.org.json.JSONException r9 = r13.syntaxError(r9)
            throw r9
        L_0x0052:
            r9 = r8
        L_0x0053:
            return r9
        L_0x0054:
            java.lang.Character r9 = twitter4j.internal.org.json.XML.BANG
            if (r8 != r9) goto L_0x00b2
            char r2 = r13.next()
            if (r2 != r11) goto L_0x006d
            char r9 = r13.next()
            if (r9 != r11) goto L_0x0069
            java.lang.String r9 = "-->"
            r13.skipPast(r9)
        L_0x0069:
            r13.back()
            goto L_0x0008
        L_0x006d:
            if (r2 != r12) goto L_0x0093
            java.lang.Object r8 = r13.nextToken()
            java.lang.String r9 = "CDATA"
            boolean r9 = r8.equals(r9)
            if (r9 == 0) goto L_0x008c
            char r9 = r13.next()
            if (r9 != r12) goto L_0x008c
            if (r15 == 0) goto L_0x0008
            java.lang.String r9 = r13.nextCDATA()
            r15.put(r9)
            goto L_0x0008
        L_0x008c:
            java.lang.String r9 = "Expected 'CDATA['"
            twitter4j.internal.org.json.JSONException r9 = r13.syntaxError(r9)
            throw r9
        L_0x0093:
            r4 = 1
        L_0x0094:
            java.lang.Object r8 = r13.nextMeta()
            if (r8 != 0) goto L_0x00a1
            java.lang.String r9 = "Missing '>' after '<!'."
            twitter4j.internal.org.json.JSONException r9 = r13.syntaxError(r9)
            throw r9
        L_0x00a1:
            java.lang.Character r9 = twitter4j.internal.org.json.XML.LT
            if (r8 != r9) goto L_0x00ab
            int r4 = r4 + 1
        L_0x00a7:
            if (r4 > 0) goto L_0x0094
            goto L_0x0008
        L_0x00ab:
            java.lang.Character r9 = twitter4j.internal.org.json.XML.GT
            if (r8 != r9) goto L_0x00a7
            int r4 = r4 + -1
            goto L_0x00a7
        L_0x00b2:
            java.lang.Character r9 = twitter4j.internal.org.json.XML.QUEST
            if (r8 != r9) goto L_0x00bd
            java.lang.String r9 = "?>"
            r13.skipPast(r9)
            goto L_0x0008
        L_0x00bd:
            java.lang.String r9 = "Misshaped tag"
            twitter4j.internal.org.json.JSONException r9 = r13.syntaxError(r9)
            throw r9
        L_0x00c4:
            boolean r9 = r8 instanceof java.lang.String
            if (r9 != 0) goto L_0x00e6
            java.lang.StringBuffer r9 = new java.lang.StringBuffer
            r9.<init>()
            java.lang.String r10 = "Bad tagName '"
            java.lang.StringBuffer r9 = r9.append(r10)
            java.lang.StringBuffer r9 = r9.append(r8)
            java.lang.String r10 = "'."
            java.lang.StringBuffer r9 = r9.append(r10)
            java.lang.String r9 = r9.toString()
            twitter4j.internal.org.json.JSONException r9 = r13.syntaxError(r9)
            throw r9
        L_0x00e6:
            r0 = r8
            java.lang.String r0 = (java.lang.String) r0
            r7 = r0
            twitter4j.internal.org.json.JSONArray r5 = new twitter4j.internal.org.json.JSONArray
            r5.<init>()
            twitter4j.internal.org.json.JSONObject r6 = new twitter4j.internal.org.json.JSONObject
            r6.<init>()
            if (r14 == 0) goto L_0x010f
            r5.put(r7)
            if (r15 == 0) goto L_0x00fe
            r15.put(r5)
        L_0x00fe:
            r8 = 0
        L_0x00ff:
            if (r8 != 0) goto L_0x01ed
            java.lang.Object r8 = r13.nextToken()
            r1 = r8
        L_0x0106:
            if (r1 != 0) goto L_0x011a
            java.lang.String r9 = "Misshaped tag"
            twitter4j.internal.org.json.JSONException r9 = r13.syntaxError(r9)
            throw r9
        L_0x010f:
            java.lang.String r9 = "tagName"
            r6.put(r9, r7)
            if (r15 == 0) goto L_0x00fe
            r15.put(r6)
            goto L_0x00fe
        L_0x011a:
            boolean r9 = r1 instanceof java.lang.String
            if (r9 != 0) goto L_0x013c
            if (r14 == 0) goto L_0x0129
            int r9 = r6.length()
            if (r9 <= 0) goto L_0x0129
            r5.put(r6)
        L_0x0129:
            java.lang.Character r9 = twitter4j.internal.org.json.XML.SLASH
            if (r1 != r9) goto L_0x0181
            java.lang.Object r9 = r13.nextToken()
            java.lang.Character r10 = twitter4j.internal.org.json.XML.GT
            if (r9 == r10) goto L_0x0177
            java.lang.String r9 = "Misshaped tag"
            twitter4j.internal.org.json.JSONException r9 = r13.syntaxError(r9)
            throw r9
        L_0x013c:
            java.lang.String r1 = (java.lang.String) r1
            if (r14 != 0) goto L_0x014f
            java.lang.String r9 = "tagName"
            if (r1 == r9) goto L_0x0148
            java.lang.String r9 = "childNode"
            if (r1 != r9) goto L_0x014f
        L_0x0148:
            java.lang.String r9 = "Reserved attribute."
            twitter4j.internal.org.json.JSONException r9 = r13.syntaxError(r9)
            throw r9
        L_0x014f:
            java.lang.Object r8 = r13.nextToken()
            java.lang.Character r9 = twitter4j.internal.org.json.XML.EQ
            if (r8 != r9) goto L_0x0171
            java.lang.Object r8 = r13.nextToken()
            boolean r9 = r8 instanceof java.lang.String
            if (r9 != 0) goto L_0x0166
            java.lang.String r9 = "Missing value"
            twitter4j.internal.org.json.JSONException r9 = r13.syntaxError(r9)
            throw r9
        L_0x0166:
            java.lang.String r8 = (java.lang.String) r8
            java.lang.Object r9 = twitter4j.internal.org.json.XML.stringToValue(r8)
            r6.accumulate(r1, r9)
            r8 = 0
            goto L_0x00ff
        L_0x0171:
            java.lang.String r9 = ""
            r6.accumulate(r1, r9)
            goto L_0x00ff
        L_0x0177:
            if (r15 != 0) goto L_0x0008
            if (r14 == 0) goto L_0x017e
            r9 = r5
            goto L_0x0053
        L_0x017e:
            r9 = r6
            goto L_0x0053
        L_0x0181:
            java.lang.Character r9 = twitter4j.internal.org.json.XML.GT
            if (r1 == r9) goto L_0x018c
            java.lang.String r9 = "Misshaped tag"
            twitter4j.internal.org.json.JSONException r9 = r13.syntaxError(r9)
            throw r9
        L_0x018c:
            java.lang.Object r3 = parse(r13, r14, r5)
            java.lang.String r3 = (java.lang.String) r3
            if (r3 == 0) goto L_0x0008
            boolean r9 = r3.equals(r7)
            if (r9 != 0) goto L_0x01c2
            java.lang.StringBuffer r9 = new java.lang.StringBuffer
            r9.<init>()
            java.lang.String r10 = "Mismatched '"
            java.lang.StringBuffer r9 = r9.append(r10)
            java.lang.StringBuffer r9 = r9.append(r7)
            java.lang.String r10 = "' and '"
            java.lang.StringBuffer r9 = r9.append(r10)
            java.lang.StringBuffer r9 = r9.append(r3)
            java.lang.String r10 = "'"
            java.lang.StringBuffer r9 = r9.append(r10)
            java.lang.String r9 = r9.toString()
            twitter4j.internal.org.json.JSONException r9 = r13.syntaxError(r9)
            throw r9
        L_0x01c2:
            r7 = 0
            if (r14 != 0) goto L_0x01d0
            int r9 = r5.length()
            if (r9 <= 0) goto L_0x01d0
            java.lang.String r9 = "childNodes"
            r6.put(r9, r5)
        L_0x01d0:
            if (r15 != 0) goto L_0x0008
            if (r14 == 0) goto L_0x01d7
            r9 = r5
            goto L_0x0053
        L_0x01d7:
            r9 = r6
            goto L_0x0053
        L_0x01da:
            if (r15 == 0) goto L_0x0008
            boolean r9 = r8 instanceof java.lang.String
            if (r9 == 0) goto L_0x01eb
            java.lang.String r8 = (java.lang.String) r8
            java.lang.Object r9 = twitter4j.internal.org.json.XML.stringToValue(r8)
        L_0x01e6:
            r15.put(r9)
            goto L_0x0008
        L_0x01eb:
            r9 = r8
            goto L_0x01e6
        L_0x01ed:
            r1 = r8
            goto L_0x0106
        */
        throw new UnsupportedOperationException("Method not decompiled: twitter4j.internal.org.json.JSONML.parse(twitter4j.internal.org.json.XMLTokener, boolean, twitter4j.internal.org.json.JSONArray):java.lang.Object");
    }

    public static JSONArray toJSONArray(String string) throws JSONException {
        return toJSONArray(new XMLTokener(string));
    }

    public static JSONArray toJSONArray(XMLTokener x) throws JSONException {
        return (JSONArray) parse(x, true, null);
    }

    public static JSONObject toJSONObject(XMLTokener x) throws JSONException {
        return (JSONObject) parse(x, false, null);
    }

    public static JSONObject toJSONObject(String string) throws JSONException {
        return toJSONObject(new XMLTokener(string));
    }

    public static String toString(JSONArray ja) throws JSONException {
        int i;
        StringBuffer sb = new StringBuffer();
        String tagName = ja.getString(0);
        XML.noSpace(tagName);
        String tagName2 = XML.escape(tagName);
        sb.append('<');
        sb.append(tagName2);
        Object object = ja.opt(1);
        if (object instanceof JSONObject) {
            i = 2;
            JSONObject jo = (JSONObject) object;
            Iterator keys = jo.keys();
            while (keys.hasNext()) {
                String key = keys.next().toString();
                XML.noSpace(key);
                String value = jo.optString(key);
                if (value != null) {
                    sb.append(' ');
                    sb.append(XML.escape(key));
                    sb.append('=');
                    sb.append('\"');
                    sb.append(XML.escape(value));
                    sb.append('\"');
                }
            }
        } else {
            i = 1;
        }
        int length = ja.length();
        if (i >= length) {
            sb.append('/');
            sb.append('>');
        } else {
            sb.append('>');
            do {
                Object object2 = ja.get(i);
                i++;
                if (object2 != null) {
                    if (object2 instanceof String) {
                        sb.append(XML.escape(object2.toString()));
                        continue;
                    } else if (object2 instanceof JSONObject) {
                        sb.append(toString((JSONObject) object2));
                        continue;
                    } else if (object2 instanceof JSONArray) {
                        sb.append(toString((JSONArray) object2));
                        continue;
                    } else {
                        continue;
                    }
                }
            } while (i < length);
            sb.append('<');
            sb.append('/');
            sb.append(tagName2);
            sb.append('>');
        }
        return sb.toString();
    }

    public static String toString(JSONObject jo) throws JSONException {
        StringBuffer sb = new StringBuffer();
        String tagName = jo.optString("tagName");
        if (tagName == null) {
            return XML.escape(jo.toString());
        }
        XML.noSpace(tagName);
        String tagName2 = XML.escape(tagName);
        sb.append('<');
        sb.append(tagName2);
        Iterator keys = jo.keys();
        while (keys.hasNext()) {
            String key = keys.next().toString();
            if (!key.equals("tagName") && !key.equals("childNodes")) {
                XML.noSpace(key);
                String value = jo.optString(key);
                if (value != null) {
                    sb.append(' ');
                    sb.append(XML.escape(key));
                    sb.append('=');
                    sb.append('\"');
                    sb.append(XML.escape(value));
                    sb.append('\"');
                }
            }
        }
        JSONArray ja = jo.optJSONArray("childNodes");
        if (ja == null) {
            sb.append('/');
            sb.append('>');
        } else {
            sb.append('>');
            int length = ja.length();
            for (int i = 0; i < length; i++) {
                Object object = ja.get(i);
                if (object != null) {
                    if (object instanceof String) {
                        sb.append(XML.escape(object.toString()));
                    } else if (object instanceof JSONObject) {
                        sb.append(toString((JSONObject) object));
                    } else if (object instanceof JSONArray) {
                        sb.append(toString((JSONArray) object));
                    }
                }
            }
            sb.append('<');
            sb.append('/');
            sb.append(tagName2);
            sb.append('>');
        }
        return sb.toString();
    }
}
