package twitter4j.internal.async;

public interface DispatcherConfiguration {
    int getAsyncNumThreads();

    String getDispatcherImpl();
}
