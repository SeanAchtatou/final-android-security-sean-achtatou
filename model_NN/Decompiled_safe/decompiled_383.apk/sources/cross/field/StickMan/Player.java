package cross.field.StickMan;

public class Player {
    public static final int ANM_SPEED = 1;
    public static final float GRAVITY = 30.0f;
    public static final int JUMP_COUNT = 1;
    public static final float JUMP_SPEED = -50.0f;
    public int anm_cnt = 0;
    private Graphics g;
    public int gx = 0;
    public int h;
    public int image_id;
    public int jump_count = 0;
    private int scene;
    public int w;
    public int x;
    public int xhit;
    public int xoff = 0;
    public float xspeed = 0.0f;
    public int y;
    public int yhit;
    public int yoff = 0;
    public float yspeed = 0.0f;

    public Player(Graphics g2, int id, int x2, int y2, int w2, int h2) {
        this.g = g2;
        this.scene = 0;
        this.x = x2;
        this.y = y2;
        this.w = w2;
        this.h = h2;
        this.xoff = (w2 / -2) + ((int) (((double) w2) * -0.2d));
        this.yoff = (int) (((double) h2) * -1.0d);
        this.xhit = (int) ((((double) w2) * 1.0d) / 2.0d);
        this.yhit = 0;
        this.xspeed = 0.0f;
        this.yspeed = 0.0f;
        g2.getClass();
        this.image_id = 1;
    }

    public void action() {
    }

    public void draw() {
        animation();
        this.g.drawImage(this.image_id, this.x + this.xoff, this.y + this.yoff, this.w, this.h);
        if (this.yspeed == 0.0f) {
            int i = this.image_id;
            this.g.getClass();
            if (i == 1) {
                this.jump_count = 1;
            }
        }
    }

    public void animation() {
        if (this.scene == 1) {
            this.anm_cnt++;
        }
        int i = this.image_id;
        this.g.getClass();
        if (i != 3) {
            int i2 = this.image_id;
            this.g.getClass();
            if (i2 != 4 && this.anm_cnt > 1) {
                int i3 = this.image_id;
                this.g.getClass();
                if (i3 < 1) {
                    this.g.getClass();
                    this.image_id = 1;
                }
                this.anm_cnt = 0;
                this.image_id++;
                int i4 = this.image_id;
                this.g.getClass();
                if (i4 > 2) {
                    this.g.getClass();
                    this.image_id = 1;
                }
            }
        }
        if (3 == this.scene) {
            this.g.getClass();
            this.image_id = 3;
        }
        if (this.yhit > 0) {
            this.g.getClass();
            this.image_id = 4;
        }
    }

    public void sceneChange(int scene2) {
        this.scene = scene2;
    }

    public void move() {
        this.x = (int) (((float) this.x) + this.xspeed);
    }

    public void fall() {
        this.yspeed += 30.0f;
        this.y = (int) (((float) this.y) + this.yspeed);
    }

    public void jump() {
        this.yspeed = -50.0f;
        this.y = (int) (((float) this.y) + this.yspeed);
        this.g.getClass();
        this.image_id = 3;
    }

    public void orientation(float dx, float dy) {
        this.xspeed = (float) ((int) (((double) dy) * 3.0d));
    }

    public void warp() {
        if (this.x < this.xoff) {
            this.x = this.g.disp_width - 1;
        } else if (this.g.disp_width < this.x + this.xoff) {
            this.x = this.xoff + 1;
        }
    }
}
